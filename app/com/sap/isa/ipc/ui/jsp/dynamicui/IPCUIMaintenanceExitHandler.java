package com.sap.isa.ipc.ui.jsp.dynamicui;

import java.util.Iterator;
import java.util.List;
import java.util.Map;

import com.sap.isa.core.TechKey;
import com.sap.isa.core.UserSessionData;
import com.sap.isa.core.businessobject.management.MetaBusinessObjectManager;
import com.sap.isa.core.util.RequestParser;
import com.sap.isa.ipc.ui.jsp.dynamicui.renderer.IPCUIRendererManager;
import com.sap.isa.maintenanceobject.action.ActionConstants;
import com.sap.isa.maintenanceobject.backend.boi.DynamicUIMaintenanceExitData;
import com.sap.isa.maintenanceobject.backend.boi.PropertyData;
import com.sap.isa.maintenanceobject.backend.boi.UIElementBaseData;
import com.sap.isa.maintenanceobject.backend.boi.UIElementData;
import com.sap.isa.maintenanceobject.backend.boi.UIElementGroupData;
import com.sap.isa.maintenanceobject.businessobject.DynamicUI;
import com.sap.isa.maintenanceobject.businessobject.Property;
import com.sap.isa.maintenanceobject.businessobject.PropertyMaintenance;
import com.sap.isa.maintenanceobject.businessobject.UIElementGroup;
import com.sap.isa.maintenanceobject.businessobject.UIPage;
import com.sap.isa.maintenanceobject.ui.renderer.UIRendererManager;
import com.sap.isa.user.util.UserInitHandler;

public class IPCUIMaintenanceExitHandler implements DynamicUIMaintenanceExitData {

	public void adjustPropertyMaintenance(PropertyData property, PropertyMaintenance propertyMaintenance) {
		
		List properties = propertyMaintenance.getPropertyList("uiTypeExpand");

		IPCProperty ipcProperty = (IPCProperty)property;
		
		String ipcUiType = ipcProperty.getUiType();

		if (properties != null) {
			Property uiProperty = (Property)properties.get(0);
			
			if (uiProperty != null) {
				uiProperty.setHidden(true);
				if (ipcUiType.equals(UIRendererManager.UI_TYPE_RADIO_BUTTON)){
					uiProperty.setHidden(false);
					uiProperty.setValue("uitype_expand");
				} 
				else if (ipcUiType.equals(UIRendererManager.UI_TYPE_CHECKBOX_LIST)){
					uiProperty.setHidden(false);
					uiProperty.setValue("uitype_expand");
				}
				else if (ipcUiType.equals(IPCUIRendererManager.IPC_CHECKBOXES_INPUT)){
					uiProperty.setHidden(false);
					uiProperty.setValue("uitype_expand");
				}
				else if (ipcUiType.equals(IPCUIRendererManager.IPC_RADIO_BUTTONS_INPUT)){
					uiProperty.setHidden(false);
					uiProperty.setValue("uitype_expand");
				}
				else if (ipcUiType.equals(UIRendererManager.UI_TYPE_DDLB)){
					uiProperty.setHidden(false);
					uiProperty.setValue("uitype_collapse");
				}
				else if (ipcUiType.equals(UIRendererManager.UI_TYPE_DDLB_LIST)){
					uiProperty.setHidden(false);
					uiProperty.setValue("uitype_collapse");
				}
				else if (ipcUiType.equals(IPCUIRendererManager.IPC_MULTI_DDLB_INPUT)){
					uiProperty.setHidden(false);
					uiProperty.setValue("uitype_collapse");
				}
				else if (ipcUiType.equals(IPCUIRendererManager.IPC_SINGLE_DDLB_INPUT)){
					uiProperty.setHidden(false);
					uiProperty.setValue("uitype_collapse");
				}
			}
		}	

		
		boolean addDocumentAvailable = ipcProperty.getAdditionalDocumentURL() != null && ipcProperty.getAdditionalDocumentURL().length() > 0; 
		properties = propertyMaintenance.getPropertyList("displayDocumentLink");
		if (properties != null) {
			Property uiProperty = (Property)properties.get(0);
			uiProperty.setHidden(!addDocumentAvailable);
		}	
		
		boolean imageAvailable = ipcProperty.getImageURL() != null && ipcProperty.getImageURL().length() > 0; 
		properties = propertyMaintenance.getPropertyList("displayImageType");
		if (properties != null) {
			Property uiProperty = (Property)properties.get(0);
			uiProperty.setHidden(!imageAvailable);
		}	

	}


	public void parseAllowedValues(List allowedValues, RequestParser requestParser) {
		
		Iterator allowIter = allowedValues.iterator();
		

		RequestParser.Parameter displayImageTypeParameter = requestParser.getParameter("ipc-dt[]");
		RequestParser.Parameter  displayDocumentParameter = requestParser.getParameter("ipc-dd[]");
				
		while (allowIter.hasNext()){
			IPCAllowedValue allowedValue = (IPCAllowedValue)allowIter.next();

			if (allowedValue.getValue().length() > 0) {
				String displayImageType = displayImageTypeParameter.getValue(allowedValue.getValue()).getString();
				boolean displayDocument= displayDocumentParameter.getValue(allowedValue.getValue()).getBoolean();
			
				allowedValue.setDisplayImageType(displayImageType);
				allowedValue.setDisplayDocumentLink(displayDocument);
			}
		}
	}

	
	public void maintainProperty(PropertyData property, PropertyMaintenance propertyMaintenance) {

		// get ipc property
		List uiType = propertyMaintenance.getPropertyList("uiType");
		Property ipcProperty = null;
		String propertyType = "";
		if (uiType != null) {
			ipcProperty = (Property)uiType.get(0);
			propertyType = ipcProperty.getString();
		}	
		
		// get FRW property
		List uiTypeFrw = propertyMaintenance.getPropertyList("uiTypeExpand");
		
		
		if (uiType != null) {
			Property uiProperty = (Property)uiTypeFrw.get(0);
			
			if (uiProperty != null) {
				if (propertyType.equals(UIRendererManager.UI_TYPE_RADIO_BUTTON) && uiProperty.getValue().equals("uitype_collapse") ){
					ipcProperty.setValue(UIRendererManager.UI_TYPE_DDLB);
				} 
				else if (propertyType.equals(UIRendererManager.UI_TYPE_CHECKBOX_LIST) && uiProperty.getValue().equals("uitype_collapse")){
					ipcProperty.setValue(UIRendererManager.UI_TYPE_DDLB_LIST);
				}
				else if (propertyType.equals(IPCUIRendererManager.IPC_CHECKBOXES_INPUT) && uiProperty.getValue().equals("uitype_collapse")){
					ipcProperty.setValue(IPCUIRendererManager.IPC_MULTI_DDLB_INPUT);
				}
				else if (propertyType.equals(IPCUIRendererManager.IPC_RADIO_BUTTONS_INPUT) && uiProperty.getValue().equals("uitype_collapse")){
					ipcProperty.setValue(IPCUIRendererManager.IPC_SINGLE_DDLB_INPUT);
				}
				else if (propertyType.equals(UIRendererManager.UI_TYPE_DDLB) && uiProperty.getValue().equals("uitype_expand")){
					ipcProperty.setValue(UIRendererManager.UI_TYPE_RADIO_BUTTON);
				}
				else if (propertyType.equals(UIRendererManager.UI_TYPE_DDLB_LIST)&& uiProperty.getValue().equals("uitype_expand")){
					ipcProperty.setValue(UIRendererManager.UI_TYPE_CHECKBOX_LIST);
				}
				else if (propertyType.equals(IPCUIRendererManager.IPC_MULTI_DDLB_INPUT)&& uiProperty.getValue().equals("uitype_expand")){
					ipcProperty.setValue(IPCUIRendererManager.IPC_CHECKBOXES_INPUT);
				}
				else if (propertyType.equals(IPCUIRendererManager.IPC_SINGLE_DDLB_INPUT)&& uiProperty.getValue().equals("uitype_expand")){
					ipcProperty.setValue(IPCUIRendererManager.IPC_RADIO_BUTTONS_INPUT);
				}
			}
		}	
		
	}
	
	
	
	public void parseDesignerRequest(RequestParser requestParser, DynamicUI dynamicUI){
		if (requestParser.getParameter("ipc-du-expand").isSet()) {
			String expand = requestParser.getParameter("ipc-du-expand").getValue().getString();
			if (expand.length() > 0) {
				UIPage page = (UIPage)dynamicUI.getActivePage();
				RequestParser.Parameter checkboxes = requestParser.getParameter(ActionConstants.RC_CHECKBOX);
				RequestParser.Value[] keys = checkboxes.getValues();
				if (page != null) {
					Iterator iter = page.propertyIterator();
					while (iter.hasNext()) {
						IPCProperty property = (IPCProperty) iter.next();
						String propertyType = property.getUiType();
						String propKey = property.getTechKey().getIdAsString();
						
						for (int i = 0; i < keys.length; i++) {
							if (keys[i].getString().equals(propKey)) {
								if (propertyType.equals(UIRendererManager.UI_TYPE_RADIO_BUTTON) && expand.equals("collapse") ){
									property.setUiType(UIRendererManager.UI_TYPE_DDLB);
								} 
								else if (propertyType.equals(UIRendererManager.UI_TYPE_CHECKBOX_LIST) && expand.equals("collapse")){
									property.setUiType(UIRendererManager.UI_TYPE_DDLB_LIST);
								}
								else if (propertyType.equals(IPCUIRendererManager.IPC_CHECKBOXES_INPUT) && expand.equals("collapse")){
									property.setUiType(IPCUIRendererManager.IPC_MULTI_DDLB_INPUT);
								}
								else if (propertyType.equals(IPCUIRendererManager.IPC_RADIO_BUTTONS_INPUT) && expand.equals("collapse")){
									property.setUiType(IPCUIRendererManager.IPC_SINGLE_DDLB_INPUT);
								}
								else if (propertyType.equals(UIRendererManager.UI_TYPE_DDLB) && expand.equals("expand")){
									property.setUiType(UIRendererManager.UI_TYPE_RADIO_BUTTON);
								}
								else if (propertyType.equals(UIRendererManager.UI_TYPE_DDLB_LIST)&& expand.equals("expand")){
									property.setUiType(UIRendererManager.UI_TYPE_CHECKBOX_LIST);
								}								
								else if (propertyType.equals(IPCUIRendererManager.IPC_MULTI_DDLB_INPUT)&& expand.equals("expand")){
									property.setUiType(IPCUIRendererManager.IPC_CHECKBOXES_INPUT);
								}								
								else if (propertyType.equals(IPCUIRendererManager.IPC_SINGLE_DDLB_INPUT)&& expand.equals("expand")){
									property.setUiType(IPCUIRendererManager.IPC_RADIO_BUTTONS_INPUT);
								}								
							}
						}
					}
				}
			}
		}
		
	}
	
	/**
	 * <p>Overwrites/Implements the method <code>isDeletionAllowed</code> 
	 * of the <code>IPCUIMaintenanceExitHandler</code>. </p>
	 * <p>The method checks if the element can be found also complete on an other page. This 
	 * means that for a component or a group all includes elements exit also on other pages! </p>
	 * 
	 * @param dynamicUI ui model
	 * @param element element to delete
	 * @param parent pararent element
	 * @return <code>true</code> when the element could be found completly on an other page.
	 * 
	 * 
	 * @see com.sap.isa.maintenanceobject.backend.boi.DynamicUIMaintenanceExitData#isDeletionAllowed(com.sap.isa.maintenanceobject.businessobject.DynamicUI, com.sap.isa.maintenanceobject.backend.boi.UIElementBaseData, com.sap.isa.maintenanceobject.backend.boi.UIElementGroupData)
	 */
	public boolean isDeletionAllowed(DynamicUI dynamicUI, UIElementBaseData element, UIElementGroupData parent){
		boolean ret = true;
		
		// has to be implemented for IPC objects
		if (dynamicUI instanceof IPCUIModel) {
			IPCUIModel ipcModel = (IPCUIModel)dynamicUI;  
			if (element instanceof IPCUIPage) {
				IPCUIPage page = (IPCUIPage)element;
				Iterator iter = page.iterator();
				ret = true;
				while (iter.hasNext()) {
					if (!checkIPCComponent((IPCUIComponent) iter.next(),ipcModel)) {
						return false;
					}
				}
			}
			else if (element instanceof IPCUIComponent) {
				ret = checkIPCComponent(element, ipcModel);
			}
			else if (element instanceof IPCPropertyGroup) {
				List list = getComponentCopies(ipcModel, parent);	
				ret = false;
				if (list != null && list.size() > 0) {
					ret = true;
					// check for all attributes if the exit in the other components.
					Iterator iter = ((IPCPropertyGroup)element).propertyIterator();
					while (iter.hasNext()) {
						if (!findAttributeInComponents(list,((UIElementBaseData)iter.next()).getName())){
							return false;
						}
					}
				}
			}
			else if (element instanceof IPCProperty) {
				UIElementBaseData component = findComponent(ipcModel,parent.getTechKey());
				List list = getComponentCopies(ipcModel, component);	
				ret= false;
				if (list != null && list.size() > 0) {
					ret = findAttributeInComponents(list,element.getName());
				}		
			}
		}
		return ret;
	}


	protected boolean checkIPCComponent(UIElementBaseData element, IPCUIModel ipcModel) {
		boolean ret;
		List list = getComponentCopies(ipcModel, element);
		ret = false;
		if (list != null && list.size() > 0) {
			ret = true;
			// check for all attributes if the exit in the other components.
			Iterator iter = ((IPCUIComponent)element).propertyIterator();
			while (iter.hasNext()) {
				if (!findAttributeInComponents(list,((UIElementBaseData)iter.next()).getName())){
					return false;
				}
			}
		}
		return ret;
	}	

	
	/**
	 * <p> The method getComponentList returns a list of copies of the given component.</p>
	 * 
	 * @param ipcModel ui model 
	 * @param component compoenent
	 * @return list of component copies on other pages.
	 */
	protected List getComponentCopies(IPCUIModel ipcModel, UIElementBaseData component) {
		IPCConfigModelKey  modelKey = new IPCConfigModelKey();
		modelKey.setInstanceName(component.getName());
		List list = ipcModel.getIPCUIComponents(modelKey);
		if (list != null) {
			int index = -1;
			int i = 0;
			Iterator iter = list.iterator();
			while (iter.hasNext()) {
				if (((UIElementBaseData)iter.next()).getTechKey().equals(component.getTechKey())) {
					index = i; 
				}
				i++;
			}
			list.remove(index);
		}
		return list;
	}
		


	/**
	 * <p> The method findAttributeInComponents search for the attribute with the given name.</p>
	 * 
	 * @param componentList list with components with possible entries
	 * @param attributeName name of the attribute
	 * @return <code>true</code> if the attribute could be found.
	 */
	protected boolean findAttributeInComponents(List componentList, String attributeName ) {
		
		Iterator iter = componentList.iterator();
		while (iter.hasNext()) {
			Iterator propertyIterator = ((IPCUIComponent) iter.next()).propertyIterator();
			while (propertyIterator.hasNext()) {
				if (((UIElementBaseData) propertyIterator.next()).getName().equals(attributeName)){
					return true;
				}
			}
		}
			
		return false;
	}

	
	/**
	 * <p> The method findComponent the component which includes the given group </p>
	 * 
	 * @param ipcModel ui model.
	 * @param groupKey key of the searched group.
	 * @return the found component
	 */
	protected UIElementBaseData findComponent(IPCUIModel ipcModel,TechKey groupKey) { 
		
		Iterator pageIterator = ipcModel.pageIterator();
		while (pageIterator.hasNext()) {
			UIElementGroupData page = (UIElementGroupData) pageIterator.next();

			Iterator componentIterator = page.iterator();
			while (componentIterator.hasNext()) {
				UIElementGroupData component = (UIElementGroupData) componentIterator.next();
				Iterator groupIterator = component.iterator();
				while (groupIterator.hasNext()) {
					UIElementGroupData group = (UIElementGroupData) groupIterator.next();
					// search for selected element
					if (group.getTechKey().equals(groupKey)) {
						return component;
					}
				}
			}
		}
		return null;
	}	
	
	
	
	
	//TODO docu
	/**
	 * <p>Overwrites/Implements the method <code>extendedElementMoveDown</code> 
	 * of the <code>IPCUIMaintenanceExitHandler</code>. </p>
	 * 
	 * @param dynamicUI
	 * @param parent
	 * @param element
	 * 
	 * 
	 * @see com.sap.isa.maintenanceobject.backend.boi.DynamicUIMaintenanceExitData#extendedElementMoveDown(com.sap.isa.maintenanceobject.businessobject.DynamicUI, com.sap.isa.maintenanceobject.backend.boi.UIElementGroupData, com.sap.isa.maintenanceobject.backend.boi.UIElementBaseData)
	 */
	public void extendedElementMoveDown(DynamicUI dynamicUI, UIElementGroupData parent, UIElementBaseData element) {
		
		if (parent instanceof IPCPropertyGroup && element!= null) {
			UIElementGroupData component = dynamicUI.getParent(parent.getTechKey());
			if (component != null) {
				List elementList = component.getElementList();
				int index = elementList.indexOf(parent);
				
				if (index>0){
					UIElementGroupData targetGroup = (UIElementGroupData)elementList.get(index-1);
					targetGroup.addElement((UIElementData)element);
					parent.deleteElement((UIElementData)element);
					component.expandGroup(targetGroup.getName());
				}
			}
		}
	}


	//TODO docu
	/**
	 * <p>Overwrites/Implements the method <code>extendedElementMoveUp</code> 
	 * of the <code>IPCUIMaintenanceExitHandler</code>. </p>
	 * 
	 * @param dynamicUI
	 * @param parent
	 * @param element
	 * 
	 * 
	 * @see com.sap.isa.maintenanceobject.backend.boi.DynamicUIMaintenanceExitData#extendedElementMoveUp(com.sap.isa.maintenanceobject.businessobject.DynamicUI, com.sap.isa.maintenanceobject.backend.boi.UIElementGroupData, com.sap.isa.maintenanceobject.backend.boi.UIElementBaseData)
	 */
	public void extendedElementMoveUp(DynamicUI dynamicUI, UIElementGroupData parent, UIElementBaseData element) {

		if (parent instanceof IPCPropertyGroup && element != null) {
			UIElementGroupData component = dynamicUI.getParent(parent.getTechKey());
			if (component != null) {
				List elementList = component.getElementList();
				int index = elementList.indexOf(parent);
				
				if (index>=0 && index<elementList.size()-1 ){
					UIElementGroup targetGroup = (UIElementGroup)elementList.get(index+1);
					targetGroup.addElement(0,(UIElementData)element);
					parent.deleteElement((UIElementData)element);
					component.expandGroup(targetGroup.getName());
				}
			}
		}
	}


	public void adjustAllowedProperties(PropertyData property, PropertyMaintenance propertyMaintenance) {
	}


	/**
	 * <p> The method getLanguageMap provides the language map for description maintenance</p>
	 * 
	 * @param userSessionData
	 * @param mbom
	 * @return Map with languages.
	 */
	public Map getLanguageMap(UserSessionData userSessionData, 
            MetaBusinessObjectManager mbom) {
		return UserInitHandler.getLanguages();
	}


	/**
	 * <p> The method getBOType returns the BO type to get the backend object</p>
	 * 
	 * @return boType for the dynamicUI object.
	 */
	public String getBOType() {
		return IPCUIModel.BO_TYPE;
	}

	
}
