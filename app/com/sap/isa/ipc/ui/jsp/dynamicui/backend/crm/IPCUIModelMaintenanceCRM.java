/*****************************************************************************
    Class:        IPCUIModelMaintenanceCRM
    Copyright (c) 2008, SAP AG, Germany, All rights reserved.
    Author:       SAP AG
    Created:      19.04.2008
    Version:      1.0

    $Revision: #13 $
    $Date: 2003/05/06 $ 
*****************************************************************************/

package com.sap.isa.ipc.ui.jsp.dynamicui.backend.crm;

import java.util.Iterator;
import java.util.Map;

import com.sap.isa.backend.JCoHelper;
import com.sap.isa.core.TechKey;
import com.sap.isa.ipc.ui.jsp.dynamicui.IPCAllowedValue;
import com.sap.isa.ipc.ui.jsp.dynamicui.IPCProperty;
import com.sap.isa.ipc.ui.jsp.dynamicui.IPCUIPage;
import com.sap.isa.maintenanceobject.backend.boi.AllowedValueData;
import com.sap.isa.maintenanceobject.backend.boi.DescriptionData;
import com.sap.isa.maintenanceobject.backend.boi.DescriptionListData;
import com.sap.isa.maintenanceobject.backend.boi.MaintenanceObjectData;
import com.sap.isa.maintenanceobject.backend.boi.PropertyData;
import com.sap.isa.maintenanceobject.backend.boi.UIElementBaseData;
import com.sap.isa.maintenanceobject.backend.boi.UIElementGroupData;
import com.sap.isa.maintenanceobject.backend.crm.DynamicUIMaintenanceCRM;
import com.sap.isa.maintenanceobject.businessobject.Description;
import com.sap.isa.maintenanceobject.businessobject.DynamicUI;
import com.sap.mw.jco.JCO;
import com.sap.mw.jco.JCO.Table;


public class IPCUIModelMaintenanceCRM extends DynamicUIMaintenanceCRM {
	
	
    /**
     * <p> The method fillPropertyTable </p>
     * 
     * @param propertyMaintenance
     * @param uiElement
     * @param jcoTables
     */
    protected void fillPropertyTable(MaintenanceObjectData propertyMaintenance,
    								 PropertyData uiElement,
    								 JCOTables jcoTables) {
    	
		super.fillPropertyTable(propertyMaintenance,uiElement,jcoTables);
		
    	JCO.Table propertyDescription = jcoTables.getPropertyDescription();
		
		if (uiElement instanceof IPCProperty) {
			DescriptionListData descriptionList = ((IPCProperty)uiElement).getHelpLinkLabelList();
			Iterator iter = descriptionList.iterator();
			while (iter.hasNext()) {
				DescriptionData description = (DescriptionData) iter.next();
	        	propertyDescription.appendRow();
	        	propertyDescription.setValue(uiElement.getTechKey().getIdAsString(), "PROPERTY_GUID");
	        	propertyDescription.setValue(description.getDescription(), "DESCRIPTION");
	        	propertyDescription.setValue(description.getLanguage(), "LANGU");
			}
		}
    }    	

    
    
	/**
	 * Overwrites the method fillAllowedValueTableExit. <br>
	 * 
	 * @param value
	 * @param valueDetail
	 * 
	 * 
	 * @see com.sap.isa.maintenanceobject.backend.crm.DynamicUIMaintenanceCRM#fillAllowedValueTableExit(com.sap.isa.maintenanceobject.backend.boi.AllowedValueData, com.sap.mw.jco.JCO.Table)
	 */
	protected void fillAllowedValueTableExit(AllowedValueData value, Table valueDetail) {
		
    	valueDetail.setValue(((IPCAllowedValue)value).getDisplayImageType(), "DISPLAY_IMAGE");
    	JCoHelper.setValue(valueDetail,((IPCAllowedValue)value).isDisplayDocumentLink(), "DISP_LNK_ADD_DOC");
	}



	//TODO docu
	/**
	 * Overwrites the method buildValueExit. <br>
	 * 
	 * @param valueDetail
	 * @param value
	 * 
	 * 
	 * @see com.sap.isa.maintenanceobject.backend.crm.DynamicUICRM#buildValueExit(com.sap.mw.jco.JCO.Table, com.sap.isa.maintenanceobject.backend.boi.AllowedValueData)
	 */
	protected void buildValueExit(Table valueDetail, AllowedValueData value) {
		
		((IPCAllowedValue)value).setDisplayImageType(valueDetail.getString("DISPLAY_IMAGE"));
		((IPCAllowedValue)value).setDisplayDocumentLink(JCoHelper.getBoolean(valueDetail, "DISP_LNK_ADD_DOC"));
	}



    

}
