package com.sap.isa.ipc.ui.jsp.dynamicui.renderer;

import com.sap.isa.maintenanceobject.ui.UITypeRendererData;
import com.sap.isa.maintenanceobject.ui.handler.UITypeRendererHandler;

public class IPCUITypeRendererMultiDDLBInput implements UITypeRendererData {


	protected static String JSP_INCLUDE_NAME = "uitypeIPC_multiddlbinput";

    public IPCUITypeRendererMultiDDLBInput() {
    }

    public String getJSPIncludeName() {
		return UITypeRendererHandler.getJSPInclude(JSP_INCLUDE_NAME);
    }

}
