package com.sap.isa.ipc.ui.jsp.dynamicui;

import com.sap.isa.ipc.ui.jsp.dynamicui.renderer.IPCUIRendererManager;
import com.sap.isa.maintenanceobject.ui.renderer.UIRendererManager;
import com.sap.spc.remote.client.object.imp.BaseGroup;

/**
 * Constants used within the DynamicUI
 */
public interface IPCDynamicUIConstant {

	public static final String COMPONENT_NAME_SEPPERATOR = " > ";
	public static final String DEFAULT_PAGE_NAME = "DEFAULT PAGE";
	public static final String IPC_NO_RENDER_GROUP_NAME = "IPC_SYSTEM_GROUP_NO_RENDER";
	public static final String SESSION_DYNAMIC_UI = "IPC_DYNAMIC_UI";
    public static final String BASE_GROUP_NAME = BaseGroup.BASE_GROUP_NAME;

	/**
	 * All UITypes that are available for the IPC. 
	 * So all Framework UITypes as well as IPC specific UITypes
	 */
	public interface UITypes {
		
		// Framework:
		public static final String PAGE = UIRendererManager.UI_TYPE_PAGE;
		public static final String ASSIGNMENT_BLOCK = UIRendererManager.UI_TYPE_ASSIGNMENT_BLOCK;
		public static final String PROPERTY_GROUP = UIRendererManager.UI_TYPE_PROPERTY_GROUP;
		public static final String SINGLE_DDLB = UIRendererManager.UI_TYPE_DDLB;                              // U05
		public static final String MULTI_DDLB = UIRendererManager.UI_TYPE_DDLB_LIST;                          // U07
		public static final String INPUT = UIRendererManager.UI_TYPE_INPUT;                                   // U09
		public static final String RADIO_BUTTONS = UIRendererManager.UI_TYPE_RADIO_BUTTON;                    // U01
		public static final String CHECKBOX_SINGLE = UIRendererManager.UI_TYPE_CHECKBOX;
		public static final String CHECKBOX = UIRendererManager.UI_TYPE_CHECKBOX_LIST;                        // U03
		public static final String DATE = UIRendererManager.UI_TYPE_DATE;                                     // U10

		// IPC
		public static final String IPC_NO_RENDER_GROUP = UIRendererManager.UI_TYPE_PROPERTY_GROUP;
		public static final String IPC_NO_RENDER_GROUP_NAME = IPCUIRendererManager.IPC_NO_RENDER_GROUP_NAME;
		public static final String IPC_MULTI_DDLB_INPUT = IPCUIRendererManager.IPC_MULTI_DDLB_INPUT;          // U08
		public static final String IPC_SINGLE_DDLB_INPUT = IPCUIRendererManager.IPC_SINGLE_DDLB_INPUT;        // U06
		public static final String IPC_CHECKBOXES_INPUT = IPCUIRendererManager.IPC_CHECKBOXES_INPUT;          // U04
		public static final String IPC_RADIO_BUTTONS_INPUT = IPCUIRendererManager.IPC_RADIO_BUTTONS_INPUT;    // U02
		public static final String IPC_MESSAGE_CSTIC = IPCUIRendererManager.IPC_MESSAGE_CSTIC;                // U11
		
	}
	
	/**
	 * Resource keys for the available characteristic statuses.
	 * Style names for the property statuses. 
	 */
	public interface Status{
		// Resource keys for characteristic statuses
		// Characteristics
		public static final String STATUS_CSTIC_COMPLETE = "ipc.cstic.complete";
		public static final String STATUS_CSTIC_READONLY = "ipc.cstic.readonly";
		public static final String STATUS_CSTIC_REQUIRED = "ipc.cstic.required";
		public static final String STATUS_CSTIC_INCONSISTENT = "ipc.cstic.inconsistent";
		// Groups and components
		public static final String STATUS_CONFIG_COMPLETE = "ipc.config.complete";
		public static final String STATUS_CONFIG_INCOMPLETE = "ipc.config.incomplete";
		public static final String STATUS_CONFIG_INCONSISTENT = "ipc.config.inconsistent";
		public static final String STATUS_GROUP_EMPTY = "ipc.group.empty";
		
		// Style names for the property statuses
		// Properties (Characteristics)
		public static final String STYLE_CSTIC_COMPLETE = "cstic_status_complete";
		public static final String STYLE_CSTIC_READONLY = "cstic_status_readonly";
		public static final String STYLE_CSTIC_REQUIRED = "cstic_status_required";
		public static final String STYLE_CSTIC_INCONSISTENT = "cstic_status_inconsistent";
		public static final String STYLE_CSTIC_FORMATERROR = "cstic_status_formaterror";
		// Groups and components
		public static final String STYLE_CONFIG_COMPLETE = "config_status_complete";
		public static final String STYLE_CONFIG_INCOMPLETE = "config_status_incomplete";
		public static final String STYLE_CONFIG_INCONSISTENT = "config_status_inconsistent";
		public static final String STYLE_GROUP_EMPTY = "group_status_empty";
	} //public interface IPCStatus{
	
	/**
	 * Paths to images. 
	 */
	public interface Images{
		// Nova Style
		public static final String IMG_NOVA_LED_GREEN = "ipc/mimes/images/nova/GreenLed.gif";
		public static final String IMG_NOVA_LED_YELLOW = "ipc/mimes/images/nova/YellowLed.gif";
		public static final String IMG_NOVA_LED_RED = "ipc/mimes/images/nova/RedLed.gif";
		public static final String IMG_NOVA_LED_INACTIVE = "ipc/mimes/images/nova/InactiveLed.gif";
		public static final String IMG_NOVA_INCOMPLETE = "ipc/mimes/images/nova/MissingInformation.gif";
				
		// IPC
		public static final String IMG_SOURCE_DOT_GREEN = "ipc/mimes/images/table/dot_green.gif";
		public static final String IMG_SOURCE_DOT_YELLOW = "ipc/mimes/images/table/dot_yellow.gif";
		public static final String IMG_SOURCE_DOT_RED = "ipc/mimes/images/table/dot_red.gif";
		public static final String IMG_SOURCE_DOT_GRAY = "ipc/mimes/images/table/dot_gray.gif";
		public static final String IMG_SOURCE_TRAFFIC_LIGHT = "inconsipc/mimes/images/sys/ipc_incomp_and_inconsist.gif";

		
	}//public interface IPCImages{

}
