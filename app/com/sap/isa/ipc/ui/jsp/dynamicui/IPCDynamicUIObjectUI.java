package com.sap.isa.ipc.ui.jsp.dynamicui;

import javax.servlet.jsp.PageContext;

import com.sap.isa.maintenanceobject.action.ActionConstants;
import com.sap.isa.ui.uiclass.DynamicUIObjectUI;

public class IPCDynamicUIObjectUI extends DynamicUIObjectUI {

	protected IPCUIModel ipcUIModel;

    /**
     *
     */

    public IPCDynamicUIObjectUI(PageContext pageContext) {
        super(pageContext);
    }

    /**
     *
     */

    public IPCDynamicUIObjectUI() {
        super();
    }

    protected void determineDynamicUI() {
		ipcUIModel = (IPCUIModel)request.getAttribute(IPCDynamicUIConstant.SESSION_DYNAMIC_UI);
		request.setAttribute(ActionConstants.RC_DYNAMIC_UI, ipcUIModel);
		object = ipcUIModel;
		object.checkObject();
		object.determineVisibility();
    }

    protected void determineReadOnly() {

    }

    protected void determineFormName() {
		formName = "currentForm";
    }

    protected void determineHelpAction() {

    }
    
	protected void determinKeyPressEventFunction() {
		keyPressEventFunction = "onkeypress=\"javascript:returnKeyForCurrentFormDefault(event);\"";
	}

	/**
	 * <p> The method getPageDescription returns the description for a page.</p>
	 * 
	 * @param pageIndex index of the page
	 * @return description of the page-
	 */
	public String getPageDescription(int pageIndex) {
		String superDescription = super.getPageDescription(pageIndex);
		IPCUIPage page = (IPCUIPage)pageList.get(pageIndex);
		String description = null;
		if (page != null) {
			description = page.getDescription(getLanguage());
		}
				
		if (description == null || description.length() == 0) {
			description = superDescription;
		}
		
		return description;
	}	
		
	
}
