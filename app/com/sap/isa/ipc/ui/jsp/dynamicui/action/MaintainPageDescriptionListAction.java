/*****************************************************************************
    Class:        MaintainPageDescriptionListAction
    Copyright (c) 2008, SAP AG, Germany, All rights reserved.
    Author:       SAP AG
    Created:      18.04.2008
    Version:      1.0
*****************************************************************************/

package com.sap.isa.ipc.ui.jsp.dynamicui.action;

import com.sap.isa.core.UserSessionData;
import com.sap.isa.core.businessobject.management.MetaBusinessObjectManager;
import com.sap.isa.core.util.EditObjectHandler;
import com.sap.isa.ipc.ui.jsp.action.SessionAttributeConstants;
import com.sap.isa.ipc.ui.jsp.dynamicui.IPCDynamicUIConstant;
import com.sap.isa.ipc.ui.jsp.dynamicui.IPCUIModel;
import com.sap.isa.ipc.ui.jsp.dynamicui.IPCUIPage;
import com.sap.isa.ipc.ui.jsp.uiclass.UIContext;
import com.sap.isa.maintenanceobject.action.MaintainDescriptionListBaseAction;
import com.sap.isa.maintenanceobject.backend.boi.DescriptionListData;
import com.sap.isa.maintenanceobject.businessobject.DescriptionList;

public class MaintainPageDescriptionListAction extends
		MaintainDescriptionListBaseAction {

	protected void setDescriptionList(UserSessionData userSessionData,
			MetaBusinessObjectManager mbom,
			EditObjectHandler editHandler,
			DescriptionListData descriptionListData) {
		
		// dynmic UI object finden
		UIContext uiContext = (UIContext)userSessionData.getAttribute(SessionAttributeConstants.UICONTEXT);
		IPCUIModel ui = (IPCUIModel) uiContext.getProperty(IPCDynamicUIConstant.SESSION_DYNAMIC_UI);
		
		// pagename aus edit Handler
		
		String pageName = (String)editHandler.getAttribute("PAGE_NAME");
		
		IPCUIPage page = (IPCUIPage)ui.getPage(pageName);
		if (page!= null) {
			page.setDescriptionList((DescriptionList)descriptionListData);
		}
		
		
		// descriptionlist setzen
 
		
	}

}
