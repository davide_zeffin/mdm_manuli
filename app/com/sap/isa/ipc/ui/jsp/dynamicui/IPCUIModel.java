/*****************************************************************************
    Class:        IPCUIModel
    Copyright (c) 2008, SAP AG, Germany, All rights reserved.
    Author:       SAP AG
    Created:      05.03.2008
    Version:      1.0

    $Revision: #13 $
    $Date: 2003/05/06 $ 
*****************************************************************************/

package com.sap.isa.ipc.ui.jsp.dynamicui;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import com.sap.isa.core.TechKey;
import com.sap.isa.core.businessobject.BackendAware;
import com.sap.isa.core.eai.BackendException;
import com.sap.isa.core.eai.BackendObjectManager;
import com.sap.isa.core.util.Message;
import com.sap.isa.ipc.ui.jsp.action.RequestParameterConstants;
import com.sap.isa.ipc.ui.jsp.beans.CharacteristicUIBean;
import com.sap.isa.ipc.ui.jsp.beans.ConfigUIBean;
import com.sap.isa.ipc.ui.jsp.beans.GroupUIBean;
import com.sap.isa.ipc.ui.jsp.beans.InstanceUIBean;
import com.sap.isa.ipc.ui.jsp.uiclass.UIContext;
import com.sap.isa.maintenanceobject.backend.boi.DynamicUIBackend;
import com.sap.isa.maintenanceobject.backend.boi.DynamicUIExitData;
import com.sap.isa.maintenanceobject.backend.boi.DynamicUIMaintenanceExitData;
import com.sap.isa.maintenanceobject.backend.boi.DynamicUIProviderData;
import com.sap.isa.maintenanceobject.backend.boi.PropertyData;
import com.sap.isa.maintenanceobject.backend.boi.UIElementBaseData;
import com.sap.isa.maintenanceobject.backend.boi.UIElementData;
import com.sap.isa.maintenanceobject.backend.boi.UIElementGroupData;
import com.sap.isa.maintenanceobject.businessobject.DynamicUI;
import com.sap.isa.maintenanceobject.businessobject.Property;
import com.sap.isa.maintenanceobject.businessobject.UIElementGroup;
import com.sap.isa.maintenanceobject.businessobject.UIOptimizeElementGroup;
import com.sap.isa.maintenanceobject.ui.UITypeRendererData;
import com.sap.spc.remote.client.object.OriginalRequestParameterConstants;
import com.sap.spc.remote.client.object.imp.DefaultCharacteristicGroup;

/**
 * The class IPCUIModel describes the UI model of a configuration model. <br>
 * <p>The model consists of one or more pages. The pages itself consists of components. 
 * Within the component there are serval groups, which includes the attribute with it valuse.
 * </p>
 * 
 * @author  SAP AG
 * @version 1.0
 */
public class IPCUIModel extends DynamicUI implements BackendAware {

    public static final String BO_TYPE = "IPCDynamicUI";

    final static public String IPC_RENDERER = "IPCUIRendererManager";

    /**
     * <p>The <code>pageGroup</code> hold the pages for the configuration </p>
     */
    protected PageGroup pageGroup = new PageGroup();
    
    protected HashMap invisibleParts = new HashMap();
    
    protected DynamicUIBackend dynamicUIMaintenanceBackend = null;

    protected BackendObjectManager bem;
    
    private String[] availableParts;
    

    /**
     * <p>Standard constructor. </p>
     * <p><strong>Note: </strong>The UI provider will be created within the constructor.</p>
     */
    public IPCUIModel() {
        super();
        initializeAvailableParts();
        uiProvider = new UIProvider();
    }
    
    
    /**
     * fill an array with all available parts
     */
    private void initializeAvailableParts() {
        availableParts = new String[5]; // number of parts
        availableParts[0] = UITypeRendererData.THUMBNAIL;       
        availableParts[1] = UITypeRendererData.ADD_DOC_LINKS;
        availableParts[2] = UITypeRendererData.RESET_VALUE;
        availableParts[3] = UITypeRendererData.DISPLAY_ALL_OPTION;
        availableParts[4] = UITypeRendererData.DISPLAY_HELP_LINK;
    }


    /**
     * Returns a list for the screen groups.
     *
     * @return list of the screen groups
     */
    public UIElementGroupData getPageGroup() {
        return uiProvider.getPageGroup();
    }

    /**
     * Hide all invisible Parts
     */
    public void hideAllInvisibleParts() {
        ((IPCUIModel.UIProvider)uiProvider).hideAllInvisibleParts();
    }

    /**
     * Hide invisible part
     */
    public void hideInvisiblePart(String partName) {
        ((IPCUIModel.UIProvider)uiProvider).hideInvisiblePart(partName);
    }

    /**
     * Display invisible Parts
     */
    public void displayInvisiblePart(String partName) {
        ((IPCUIModel.UIProvider)uiProvider).displayInvisiblePart(partName);
    }

    /**
     * @return first page of the model
     */
    public IPCUIPage getFirstPage(){
        IPCUIPage firstPage = null;
        PageGroup pageGroup = (PageGroup) getPageGroup();
        ArrayList pages = (ArrayList) pageGroup.getElementList();
        if (pages.size()>0){
            firstPage = (IPCUIPage)pages.get(0);
        }
        return firstPage;
    }

    /**
     * <p>The method getUIElement returns the UI element for the given UI element key 
     * ({@link IPCUIElementKey}) . </p>
     * 
     * @param key key of the ui element ( {@link IPCUIElementKey} )
     * @return The found UI element or <code>null</code> if nothing can't be found.
     */
    public UIElementBaseData getUIElement(IPCUIElementKey key) {

        UIElementBaseData element = null;
        String elementName = key.getPage();
        if (elementName != null) {
            element = pageGroup.getElement(elementName);
        }
        elementName = key.getComponent();
        if (elementName != null && element != null) {
            element = ((UIElementGroupData) element).getElement(elementName);
        }

        elementName = key.getGroup();
        if (elementName != null && element != null) {
            element = ((UIElementGroupData) element).getElement(elementName);
        }

        elementName = key.getProperty();
        if (elementName != null && element != null) {
            element = ((UIElementGroupData) element).getElement(elementName);
        }

        return (UIElementBaseData) element;
    }

    /**
     * This method will return a list of IPCUIComponents that correspond
     * to the given config Model key. Or an empty list if nothing can be found.
     * Group name and ctsic name will be ignored.
     * @param key instance name is mandatory
     * @return  a list of IPCUIComponents, may be empty
     */
    public List getIPCUIComponents(IPCConfigModelKey key) {
        ArrayList comps = new ArrayList();
        // instance name is mandatory
        if (key.getInstanceName() == null) {
            return comps;
        }
        // search on all pages for components with the given instance name
        List pages = pageGroup.getElementList();
        Iterator itr = pages.iterator();
        while (itr.hasNext()) {
            UIElementGroupData page = (UIElementGroupData) itr.next();
            UIElementBaseData element = page.getElement(key.getInstanceName());
            if (element != null) {
                comps.add(element);
            }

        }
        return comps;
    }

    /**
     * This method will return a list of IPCUIGroups that correspond
     * to the given config Model key. Or an empty list if nothing can be found
     * The cstic name is ignored
     * @param key instance name and group anme are madatory
     * @return a list of IPCUIGroups, may be empty
     */
    public List getIPCUIPropertyGroups(IPCConfigModelKey key) {
        List groups = new ArrayList();
        // Instance and Group name are mandatory
        if (key.getInstanceName() == null || key.getGroupName() == null) {
            return groups;
        }
        // Get a list of components in which to search for the given group name
        List comps = getIPCUIComponents(key);

        // now search in the components for the given group
        Iterator itr = comps.iterator();
        while (itr.hasNext()) {
            UIElementGroupData comp = (UIElementGroupData) itr.next();
            UIElementBaseData element = comp.getElement(key.getGroupName());
            if (element != null) {
                groups.add(element);
            }
        }
        return groups;
    }



    /**
     * This method will return a list of IPCUIproperties that correspond
     * to the given config Model key. Or an empty list if nothing can be found.
     * An instance name has to be given.
     * If a group name is given only cstcis taht are part of a group with this name are returned.
     * The cstic name is ignored
     * @param key: instance name and cstic name are mandatory
     * @return list of IPCUIproperties, may be empty
     */
    public List getIPCUIProperties(IPCConfigModelKey key) {
        // Instance and cstic name are mandatory
        List cstics = new ArrayList();
        if (key.getInstanceName() == null
            || key.getCharacteristicName() == null) {
            return cstics;
        }
        // Get list of groups in which to search for given cstic
        List groups;
        if (key.getGroupName() != null) {
            // Group name specified, so get only groups with this name
            groups = getIPCUIPropertyGroups(key);
        } else {
            // No group name given so get all groups for the given component name
            groups = new ArrayList();
            List comps = getIPCUIComponents(key);
            Iterator itr = comps.iterator();
            while (itr.hasNext()) {
                UIElementGroupData comp = (UIElementGroupData) itr.next();
                List tmpGrp = comp.getElementList();
                if (tmpGrp != null) {
                    groups.addAll(tmpGrp);
                }
            }
        }
        // now search in groups for the given ctstic name
        Iterator itr = groups.iterator();
        while (itr.hasNext()) {
            UIElementGroupData group = (UIElementGroupData) itr.next();
            UIElementBaseData element = group.getElement(key.getCharacteristicName());
                if (element != null) {
                    cstics.add(element);
                }
        }
        return cstics;
    }

    
    /**
     * Pass a ConfigModelKey for a characteristic for which you want to get the
     * first occurance of the IPCProperty.
     * Returns a list with 
     * <li>the first IPCUIPage (key: page)
     * <li>the first IPCUIComponent (key: component) 
     * <li>the first IPCPropertyGroup (key: group)
     * <li>the first IPCProperty (key: property)
     * @param key configModelKey 
     * @return
     */
    public HashMap getFirstPageComponentGroupProperty(IPCConfigModelKey key) {
        HashMap result = new HashMap();
        boolean propFound = false;
        List pages = pageGroup.getElementList();
        Iterator pagesItr = pages.iterator();
        while (pagesItr.hasNext()) {
            IPCUIPage page = (IPCUIPage) pagesItr.next();
            page.getElement(key.getCharacteristicName());
            Iterator components = page.iterator();
            while (components.hasNext()){
                IPCUIComponent component = (IPCUIComponent)components.next();
                if (component.getName().equals(key.getInstanceName())){
                    Iterator groups = component.iterator();
                    while(groups.hasNext()){
                        IPCPropertyGroup group = (IPCPropertyGroup)groups.next();
                        if (group.getName().equals(key.getGroupName())){
                            Iterator properties = group.iterator();
                            while (properties.hasNext()){
                                IPCProperty property = (IPCProperty)properties.next();
                                if (property.getName().equals(key.getCharacteristicName())){
                                    result.put("property", property);
                                    propFound = true;
                                    break;                                  
                                }
                            }
                            result.put("group", group);
                            break;
                        }
                    }
                    result.put("component", component);
                    break;
                }
            }
            result.put("page", page);
            break;
        }
        return result;
    }

    
    
    /**
     * This method tries to get information on the instances that should be displayed
     * on the next page from the DynamicUI framework.
     * It returns an ArrayList that contains an ArrayList for each relevant instance.
     * The ArrayList of the relevant instance contains 
     * <li>the instance-id (at position 0) 
     * <li>a HashMap of relevant groups (at position 1).
     * This information is used to create the correct UIBeans, i.e. only for relevant groups
     * CharacteristicUIBeans and ValueUIBeans are created.
     * @param ui
     * @param uiContext
     * @param config
     * @param searchSetGroupName the name of the group to which the found cstic belongs to (only if searchSet-feature is used)
     * @return ArrayList list of ui model info
     */
    public static ArrayList prefetch(IPCUIModel ui, UIContext uiContext, ConfigUIBean config, String searchSetGroupName, boolean showGroups) {
        ArrayList modelInfo = new ArrayList();
        if (ui != null){
            IPCUIPage page = (IPCUIPage)ui.getActivePage();
            boolean showGroupsModified = uiContext.getPropertyAsBoolean(RequestParameterConstants.USE_GROUP_INFORMATION_MODIFIED);
			boolean resetConfigDone = uiContext.getPropertyAsBoolean(RequestParameterConstants.RESET_CONFIG_DONE);
            if (page == null || showGroupsModified || resetConfigDone){
                // default UI modelInfo
                return modelInfo = createDefaultUIModelInfo(uiContext, config, ui);
            }
            ArrayList instances = (ArrayList) page.getElementList();
            for (int i=0; i<instances.size(); i++){
                IPCUIComponent instance = (IPCUIComponent)instances.get(i);
                // check if instance is currently expanded
                if (instance.isExpanded()) { 
                    ArrayList relevantInstance = new ArrayList();
                    HashMap relevantGroups = new HashMap();
                    relevantInstance.add(0, instance.getName());
                    relevantInstance.add(1, relevantGroups);
                    modelInfo.add(relevantInstance);
                    ArrayList groups = (ArrayList) instance.getElementList();
                    for (int j=0; j<groups.size(); j++){
                        IPCPropertyGroup group = (IPCPropertyGroup) groups.get(j);
                        // Add the group to relevant groups if it is expanded or
                        // if the uiModel has been loaded from database (in order to determine
                        // the correct group-status also if the group is collapsed).
						if (group.isExpanded() || ui.isLoadedFromDB() || (!showGroups && ui.getPageList().size() == 1 )){	
                            relevantGroups.put(group.getName(), group.getName());
                        }
                        else {
                            // if the group is not expanded we check whether the current group
                            // is the group of the searchSet cstic, if yes we add it to the relevant groups
                            if ((searchSetGroupName != null) && (group.getName().equals(searchSetGroupName))){
                                relevantGroups.put(group.getName(), group.getName());
                            }
                        }
                    }
                    if (groups.size() == 0){
                        // there are no groups in the list but the instance is expanded:
                        // by default the first group of config-model is used
                        if (!ui.isLoadedFromDB()) {
                            String groupName = getFirstGroupFromConfigModel(instance.getName(), uiContext, config);
                            if ((!groupName.equals(""))) {
                                relevantGroups.put(groupName, groupName);
                            }
                            
                        }
                        // if uiModel is loaded from database all groups are relevant (to determine the correct group-status)
                        else {
                            InstanceUIBean curInst = config.getInstance(instance.getName());
                            List groups2 = curInst.getBusinessObject().getCharacteristicGroups(uiContext.getShowInvisibleCharacteristics(), uiContext.getPropertyAsBoolean(RequestParameterConstants.SHOW_GENERAL_TAB_FIRST));
                            for (int j=0; j<groups2.size(); j++){
                                DefaultCharacteristicGroup group = (DefaultCharacteristicGroup) groups2.get(j);
                                relevantGroups.put(group.getName(), group.getName());
                            }
                        }
                    }
                } 
            }
        }
        else {
            // no UI model exists: default case -> root instance and first group
            modelInfo = createDefaultUIModelInfo(uiContext, config, null);
        }
        return modelInfo;
    }
    
    /**
     * Creates default UI model info (used if no UIModel exists).
     * Which objects are included in the UI model Info by default 
     * depends on the mode (runtime-/designer-mode, model loaded from db).
     * In runtime-mode the following is included by default:
     * It returns the first instance (root-instance) and its first
     * group that has visible characteristics as relevant.
     * In designer-mode the following is expanded by default: 
     * It returns the first instance (root-instance) and all groups
     * as relevant because in designer-mode all groups of the root-instance
     * are expanded at start (therefore we also need the appropriate UIBeans).
     * If the uiModel has been loaded from database the following is expanded by default: 
     * It returns the first instance (root-instance) and all groups
     * as relevant because if the model has been loaded from database we need all groups
     * to determine the correct group-status even if the group is collapsed.
     * @param uiContext
     * @param config
     * @param uiModel
     * @return ArrayList list of ui model info
     */
    private static ArrayList createDefaultUIModelInfo(
        UIContext uiContext,
        ConfigUIBean config,
        IPCUIModel uiModel) {
            
        ArrayList modelInfo = new ArrayList();
        InstanceUIBean root = config.getRootInstance();
        ArrayList relevantInstance = new ArrayList();
        HashMap relevantGroups = new HashMap();
        relevantInstance.add(0, root.getId());
        relevantInstance.add(1, relevantGroups);
        modelInfo.add(relevantInstance);
        // return relevant groups depending on mode
        if (uiContext.isDesignerMode() || uiModel.isLoadedFromDB() || !uiContext.getPropertyAsBoolean(RequestParameterConstants.USE_GROUP_INFORMATION)){
            // all groups
            List groups = root.getBusinessObject().getCharacteristicGroups(uiContext.getShowInvisibleCharacteristics(), uiContext.getPropertyAsBoolean(RequestParameterConstants.SHOW_GENERAL_TAB_FIRST));
            for (int i=0; i<groups.size(); i++){
                DefaultCharacteristicGroup group = (DefaultCharacteristicGroup) groups.get(i);
                relevantGroups.put(group.getName(), group.getName());
            }
        }
        else {
            // get the first group that has visible cstics
            String groupName = getFirstGroupFromConfigModel(root.getId(), uiContext, config);
            relevantGroups.put(groupName, groupName);
        }
        return modelInfo;   
    }   
    
    /**
     * This method returns the name of the first group of a given instance from the config-model.
     * It returns the groupname only if the group has cstics that are currently visible.
     * Otherwise it returns the next group with visible cstics.
     * @param instId current instance id that for which the first group should be found
     * @param uiContext
     * @param config ConfigUIBean
     * @return name of first group of the given instance of the config model
     */
    private static String getFirstGroupFromConfigModel(String instId, UIContext uiContext, ConfigUIBean config){
        String groupName =  "";
        InstanceUIBean curInst = config.getInstance(instId);
        List groups = curInst.getBusinessObject().getCharacteristicGroups(uiContext.getShowInvisibleCharacteristics(), uiContext.getPropertyAsBoolean(RequestParameterConstants.SHOW_GENERAL_TAB_FIRST));
        if (groups.size()>0){
            // get the first group that has characteristics
            for (int i=0; i<groups.size(); i++){
                DefaultCharacteristicGroup group = (DefaultCharacteristicGroup) groups.get(i);
                List cstics = group.getCharacteristics(uiContext.getShowInvisibleCharacteristics());
                if (cstics != null && cstics.size()>0){
                    groupName = group.getName();
                    break;  
                }
            }
        }   
        
        return groupName;
    }

    /**
     * Resets the data belonging to the UIModel in the uiContext.
     * Should be used if the same session is used for displaying differen UIModels (e.g. ECO).
     * Otherwise the UIModel would interfere.
     */
    public static void resetUIModelContextData(UIContext uic){
        if (uic != null){
            uic.removeProperty(IPCDynamicUIConstant.SESSION_DYNAMIC_UI);
            uic.removeProperty(OriginalRequestParameterConstants.DESIGNER_MODE);
            uic.setInitialMerge(true);
            uic.resetModelInfo();
        }
    }
    
    /**
     * Creates full UI model info. It returns all instances/groups that
     * are currently available in the config model. 
     * @param uiContext
     * @param config
     * @return ArrayList list of ui model info
     */
    private static ArrayList createFullUIModelInfo(
        UIContext uiContext,
        ConfigUIBean config) {
            
        ArrayList modelInfo = new ArrayList();
        ArrayList instances = (ArrayList)config.getInstances();
        for (int i=0; i<instances.size(); i++){
            InstanceUIBean curInst = (InstanceUIBean) instances.get(i);
            ArrayList relevantInstance = new ArrayList();
            HashMap relevantGroups = new HashMap();
            relevantInstance.add(0, curInst.getId());
            relevantInstance.add(1, relevantGroups);
            modelInfo.add(relevantInstance);
            List groups = curInst.getBusinessObject().getCharacteristicGroups(uiContext.getShowInvisibleCharacteristics(), uiContext.getPropertyAsBoolean(RequestParameterConstants.SHOW_GENERAL_TAB_FIRST));
            for (int j=0; j<groups.size(); j++){
                String groupName = ((DefaultCharacteristicGroup)groups.get(j)).getName();
                relevantGroups.put(groupName, groupName);
            }
        }
        return modelInfo;   
    }
    
    /**
     * This methods triggers the default expanding of components and groups.
     * Which objects are expanded by default depends on the mode (runtime-/designer-mode).
     * In runtime-mode the following is expanded by default:
     * The first component and its first group of the first page.
     * In designer-mode the following is expanded by default: 
     * The first component of the first page and all groups of this component.
     */
    public void triggerDefaultExpanding(UIContext uiContext){
		 IPCUIPage page = (IPCUIPage) getPage(activePage);	
         if (page != null){
            // first all components get collapsed
            page.collapseElementsOnPage();
            IPCUIComponent component = page.getFirstComponent();
            // expand the first component
            page.expandGroup(component.getName());
            
            // see int. message 3908856 2009: also in UI Designer mode characteristics and also groups should 
            // be expanded dependant on the XCM setting. Therefore the following if-block got removed!
             
            // expand groups depending on mode
//            if (uiContext.isDesignerMode()){
//                // all groups
//                Iterator groups = component.iterator();
//                while (groups.hasNext()){
//                    IPCPropertyGroup group = (IPCPropertyGroup) groups.next();
//                    component.expandGroup(group.getName());
//                }
//            }
//            else {
                // only the first group
                IPCPropertyGroup group = component.getFirstGroup(uiContext.showEmptyGroups());
                if (group!=null){
                    component.expandGroup(group.getName());
                }                
//            }
        }
    }


    /**
     * Sets the BackendObjectManager for the basket object. This method is used
     * by the object manager to allow the user object interaction with
     * the backend logic. This method is normally not called
     * by classes other than BusinessObjectManager.
     *
     * @param bem BackendObjectManager to be used
     */
    public void setBackendObjectManager(BackendObjectManager bem) {
        this.bem = bem;
    }
    
    
    //TODO docu
    /**
     * <p>Overwrites/Implements the method <code>getDynamicUIBackendService</code> 
     * of the <code>IPCUIModel</code>. </p>
     * 
     * @return
     * @throws BackendException
     * 
     * 
     * @see com.sap.isa.maintenanceobject.businessobject.DynamicUI#getDynamicUIBackendService()
     */
    protected DynamicUIBackend getDynamicUIBackendService() throws BackendException {

        if (dynamicUIMaintenanceBackend == null) {
            // get the Backend from the Backend Manager
            dynamicUIMaintenanceBackend = 
                    (DynamicUIBackend) bem.createBackendBusinessObject(BO_TYPE, null);
        }
        return dynamicUIMaintenanceBackend;     

    }

    public void addFormatErrorMessage(CharacteristicUIBean bean, IPCProperty property){
        String errorKey = property.getFormatErrorKey(bean);
        this.addMessage(new Message(Message.ERROR,
                    errorKey,
                    null,
                    property.getName()));
    }
    
    /**
     * @return true if the UIModel is loaded from database.
     */
    public boolean isLoadedFromDB(){
        // if the TechKey is initial it is not loaded from DB.
        return !techKey.isInitial();
    }

    public IPCUIPage determineActivePage(GroupUIBean searchSetGroup, CharacteristicUIBean searchSetCstic) {
        // search-set: trigger page navigation if necessary
        determinePageForSearchSet(searchSetGroup, searchSetCstic); // sets the active page in the DynamicUI-frw.
        
        // get active Page
        IPCUIPage currentPage = (IPCUIPage)this.getActivePage();
        if (currentPage == null) {
            currentPage = new IPCUIPage();
            currentPage.setName(IPCDynamicUIConstant.DEFAULT_PAGE_NAME);
            this.getPageGroup().addElement(currentPage);
            this.setActivePageName(currentPage.getName());
        }
        return currentPage;
    }

    /**
     * This method checks for the right page if the search-set feature has been triggered.
     * It checks on which page the first occurrence of the cstic is located that has been found via the search.
     * If it finds a page it sets as active page. If it doesn't find a page it just does nothing.
     */
    private void determinePageForSearchSet(GroupUIBean searchSetGroup, CharacteristicUIBean searchSetCstic) {
        // only search for the page if a cstic has been found via search/set
        if (searchSetCstic != null){
            // create configModelKey
            IPCConfigModelKey key = new IPCConfigModelKey();
            key.setInstanceName(searchSetCstic.getInstanceId());
            key.setGroupName(searchSetGroup.getName());
            key.setCharacteristicName(searchSetCstic.getName());
            Iterator pages = this.getPageGroup().iterator();
            // loop over the pages and search for the first page with the given cstic
            while (pages.hasNext()){
                IPCUIPage currentPage = (IPCUIPage)pages.next();
                boolean pageFound = currentPage.hasIPCProperty(key);
                if (pageFound){
                    this.setActivePageName(currentPage.getName());
                    break;
                }
            }
        }
    }


    protected boolean checkRequiredPropertiesForPropertyIteratorIgnoreVisibility(Iterator properyIterator) {
        final String METHOD = "checkRequiredPropertiesForPropertyIterator()";
        log.entering(METHOD);
        boolean ret = true;
        
        while (properyIterator.hasNext()) {
            Property property = (Property)properyIterator.next();

            if (property.isRequired() && !property.isDisabled()) {
                if ((property.getType().equals(Property.TYPE_STRING)
                    || property.getType().equals(Property.TYPE_INTEGER)) &&
                        property.getString().length() == 0 ) {
                    ret = false;
                }
            }
        }
        log.exiting();
        return ret;
    }

    public boolean checkRequiredPropertiesForElementGroupIgnoreVisibility(UIElementGroup elementGroup) {
        final String METHOD = "checkRequiredPropertiesForElementGroup()";
        log.entering(METHOD);
        boolean ret = true;
        
        if (!checkRequiredPropertiesForPropertyIteratorIgnoreVisibility(elementGroup.propertyIterator())) {
            ret = false;                                
        }
        
        log.exiting();       
        return ret;
    }


    private class PageGroup extends UIOptimizeElementGroup {

        /**
         * Overwrites the method checkGroupInstance. <br>
         * <p> This methods avoid to add other elements than {@link IPCUIPage}.</p>
         * 
         * @param element
         * @return Only <code>true</code> if an instance of {@link IPCUIPage} is added.
         * 
         * @see com.sap.isa.maintenanceobject.businessobject.UIElementGroup#checkGroupInstance(com.sap.isa.maintenanceobject.backend.boi.UIElementBaseData)
         */
        protected boolean checkGroupInstance(UIElementBaseData element) {
            return element instanceof IPCUIPage;
        }

    }

    
    //TODO docu
    /**
     * <p>The class UIProvider . </p>
     *
     * @author  SAP AG
     * @version 1.0
     */
    private class UIProvider implements DynamicUIProviderData {


        public UIProvider() {
            super();
            hideAllInvisibleParts();
        }

        DynamicUIMaintenanceExitData maintenanceExitHandler;
        DynamicUIExitData exitHandler;

        //TODO docu
        /**
         * Overwrites the method . <br>
         * 
         * @return
         * 
         * @see com.sap.isa.maintenanceobject.backend.boi.DynamicUIProviderData#getPropertyIterator()
         */
        public Iterator getPropertyIterator() {
            return pageGroup.propertyIterator();
        }

        //TODO docu
        /**
         * <p>Overwrites the method getInvisibleParts. </p>
         * 
         * @return
         * 
         * 
         * @see com.sap.isa.maintenanceobject.backend.boi.DynamicUIProviderData#getInvisibleParts()
         */
        public Map getInvisibleParts() {
            return invisibleParts;
        }
        
        public void hideAllInvisibleParts(){
            // we add all available invisible parts to the HashMap thus they are hidden
            if (invisibleParts != null){
                for (int i=0; i<availableParts.length; i++){
                    invisibleParts.put(availableParts[i], availableParts[i]);
                }
            }           
        }
        
        public void displayInvisiblePart(String partName){
            if (invisibleParts != null) {
                if (invisibleParts.containsKey(partName)){
                    invisibleParts.remove(partName);
                }
            }
        }
        
        public void hideInvisiblePart(String partName){
            if (invisibleParts != null) {
                if (!invisibleParts.containsKey(partName)){
                    invisibleParts.put(partName, partName);
                }
            }
        }       

        /**
         * <p>Implements the method getUIRendererManagerName. </p>
         * <p> Return the name of the UI Renderer for the IPC UI.</p>
         * 
         * @return {@link IPCUIModel#IPC_RENDERER} as the name of the UI Renderer.  
         * 
         * @see com.sap.isa.maintenanceobject.backend.boi.DynamicUIProviderData#getUIRendererManagerName()
         */
        public String getUIRendererManagerName() {
            return IPCUIModel.IPC_RENDERER;
        }

        //TODO docu
        /**
         * <p>Overwrites the method getElementGroupList. </p>
         * 
         * @param elementName
         * @return
         * 
         * 
         * @see com.sap.isa.maintenanceobject.backend.boi.DynamicUIProviderData#getElementGroupList(java.lang.String)
         */
        public List getElementGroupList(String elementName) {
            return pageGroup.getElementList(elementName, new Boolean(true));
        }

        //TODO docu
        /**
         * <p>Overwrites the method getPageGroup. </p>
         * 
         * @return
         * 
         * @see com.sap.isa.maintenanceobject.backend.boi.DynamicUIProviderData#getPageGroup()
         */
        public UIElementGroupData getPageGroup() {
            return pageGroup;
        }

        //TODO docu
        /**
         * <p>Overwrites the method getPropertyList. </p>
         * 
         * @param elementName
         * @return
         * 
         * 
         * @see com.sap.isa.maintenanceobject.backend.boi.DynamicUIProviderData#getPropertyList(java.lang.String)
         */
        public List getPropertyList(String elementName) {
            return pageGroup.getElementList(elementName, new Boolean(false));
        }

        //TODO docu
        /**
         * Overwrites the method . <br>
         * 
         * @param key
         * @return
         * 
         * 
         * @see com.sap.isa.maintenanceobject.backend.boi.DynamicUIProviderData#getElementGroup(com.sap.isa.core.TechKey)
         */
        public UIElementGroupData getElementGroup(TechKey key) {
            UIElementBaseData element = pageGroup.getElement(key);
            if (element instanceof UIElementGroupData) {
                return (UIElementGroupData) element;
            }
            return null;
        }


        //TODO docu
        /**
         * Overwrites the method . <br>
         * 
         * @param key
         * @return
         * 
         * 
         * @see com.sap.isa.maintenanceobject.backend.boi.DynamicUIProviderData#getProperty(com.sap.isa.core.TechKey)
         */
        public PropertyData getProperty(TechKey key) {
            UIElementBaseData element = pageGroup.getElement(key);
            if (element instanceof PropertyData) {
                return (PropertyData) element;
            }
            return null;
        }
        
        //TODO docu
        /**
         * <p>Overwrites/Implements the method <code>getParent</code> 
         * of the <code>UIProvider</code>. </p>
         * 
         * @param key
         * @return
         * 
         * 
         * @see com.sap.isa.maintenanceobject.backend.boi.DynamicUIProviderData#getParent(com.sap.isa.core.TechKey)
         */
        public UIElementGroupData getParent(TechKey key) {
            return pageGroup.getParent(key);
        }
        

        //TODO docu
        /**
         * <p>Overwrites/Implements the method <code>addElement</code> 
         * of the <code>UIProvider</code>. </p>
         * 
         * @param newElement
         * @param parentGroup
         * 
         * 
         * @see com.sap.isa.maintenanceobject.backend.boi.DynamicUIProviderData#addElement(com.sap.isa.maintenanceobject.backend.boi.UIElementBaseData, com.sap.isa.maintenanceobject.backend.boi.UIElementGroupData)
         */
        public void addElement(UIElementBaseData newElement, UIElementGroupData parentGroup) {

            if (parentGroup != null) {
                if (newElement instanceof UIElementGroupData) {
                    parentGroup.addElement((UIElementGroupData)newElement);
                }
                else if (newElement instanceof UIElementData) {
                    parentGroup.addElement((UIElementData)newElement);
                }
            }
        }

        //TODO docu
        /**
         * <p>Overwrites/Implements the method <code>deleteAllPages</code> 
         * of the <code>UIProvider</code>. </p>
         * 
         * 
         * 
         * @see com.sap.isa.maintenanceobject.backend.boi.DynamicUIProviderData#deleteAllPages()
         */
        public void deleteAllPages() {
            pageGroup.clear();          
        }
        
        //TODO docu
        /**
         * <p>Delete pages of dynamic UI. </p> 
         */
        public void deletePage(UIElementGroupData page){
            pageGroup.deleteElement((UIElementGroupData)page);
        }
        
//    TODO docu    
        /**
         */
        public void deleteElement(UIElementBaseData element, UIElementGroupData group){
            if (element instanceof UIElementData) {
                group.deleteElement((UIElementData)element);
            }
            else if (element instanceof UIElementGroupData) {
                group.deleteElement((UIElementGroupData)element);
            }
        }


//      TODO docu
        public UIElementGroupData createPage() {
            return new IPCUIPage();
}
        
        

        
        /**
         * <p>Overwrites/Implements the method <code>moveElement</code> 
         * of the <code>UIProvider</code> interface. </p>
         * <p>Please be aware that also all children will be copied.</p>
         * 
         * @param elementKey key of the element which should be moved.
         * @param page target page to which the element will be moved.
         * 
         * @see com.sap.isa.maintenanceobject.backend.boi.DynamicUIProviderData#moveElement(com.sap.isa.core.TechKey, com.sap.isa.maintenanceobject.backend.boi.UIElementGroupData)
         */
        public void moveElement(TechKey elementKey, UIElementGroupData page) {
            
            UIElementGroupData groupForDelete = null;
            UIElementBaseData elementForDelete = null;
            boolean found = false;

            Iterator pageIterator = pageIterator();
            while (pageIterator.hasNext() && !found) {
                UIElementGroupData sourcePage = (UIElementGroupData) pageIterator.next();

                Iterator componentIterator = sourcePage.iterator();
                while (componentIterator.hasNext() && !found) {
                    UIElementGroupData component = (UIElementGroupData) componentIterator.next();
                    // search for selected element
                    if (component.getTechKey().equals(elementKey)) {
                        // add a copy of selected element to target page if it is not already there
                        if (page.getElement(component.getName()) == null) {
                            page.addElement(component);
                        }                       
                        groupForDelete = sourcePage;
                        elementForDelete = component;
                        found = true;
                    }
                    Iterator groupIterator = component.iterator();
                    while (groupIterator.hasNext() && !found) {
                        UIElementGroupData group = (UIElementGroupData) groupIterator.next();
                        // search for selected element
                        if (group.getTechKey().equals(elementKey)) {
                            // check if compoment alreads exists
                            UIElementGroupData targetComponent = (UIElementGroupData) page.getElement(component.getName());
                            // copy element to target component
                            if (targetComponent == null) {
                                try {
                                    targetComponent = (UIElementGroupData) component.cloneWithoutElements();
                                    page.addElement(targetComponent);
                                } catch (CloneNotSupportedException e) {
                                    log.error(e);
                                }
                            }
                            if (targetComponent.getElement(group.getName()) == null) {
                                targetComponent.addElement(group);
                                groupForDelete = component;
                                elementForDelete = group;
                                found = true;
                                
                            }
                        }
                        Iterator elementIterator = group.iterator();
                        while (elementIterator.hasNext() && !found){
                            UIElementData element = (UIElementData) elementIterator.next();
                            // search for selected element
                            if (element.getTechKey().equals(elementKey)){
                                // check if element already exists
                                UIElementGroupData targetComponent = (UIElementGroupData) page.getElement(component.getName());
                                if (targetComponent == null){
                                    try{
                                        targetComponent = (UIElementGroupData) component.cloneWithoutElements();
                                        page.addElement(targetComponent);
                                    } catch (CloneNotSupportedException e) {
                                        log.error(e);
                                    }
                                }
                                UIElementGroupData targetGroup = (UIElementGroupData) targetComponent.getElement(group.getName());
                                if (targetGroup == null){
                                    try {
                                        targetGroup = (UIElementGroupData) group.cloneWithoutElements(); 
                                        targetComponent.addElement(targetGroup);
                                    } catch (CloneNotSupportedException e) {
                                        log.error(e);                                   }
                                }
                                UIElementData targetElement = (UIElementData) targetGroup.getElement(element.getName());
                                if (targetElement == null){
                                    targetGroup.addElement(element);
                                    groupForDelete = group;
                                    elementForDelete = element;
                                    found = true;
                                    //group.deleteElement(element);
                                }   
                            }
                            
                        }
                    }
                }
            }       
            
            if (groupForDelete != null && elementForDelete != null) {
                if (elementForDelete instanceof UIElementData) {
                    groupForDelete.deleteElement((UIElementData)elementForDelete);
                }
                else if (elementForDelete instanceof UIElementGroupData) {
                    groupForDelete.deleteElement((UIElementGroupData)elementForDelete);
                } 
            }
        }

        
        
        
        
        /**
         * <p>Overwrites/Implements the method <code>copyElement</code> 
         * of the <code>UIProvider</code> interface. </p>
         * <p>Please be aware that also all children will be copied.</p>
         * 
         * @param elementKey key of the element which should be copied.
         * @param page target page to which the element will be copied.
         * 
         * @see com.sap.isa.maintenanceobject.backend.boi.DynamicUIProviderData#copyElement(com.sap.isa.core.TechKey, com.sap.isa.maintenanceobject.backend.boi.UIElementGroupData)
         */
        public void copyElement(TechKey elementKey, UIElementGroupData page) {

            Iterator pageIterator = pageIterator();
            while (pageIterator.hasNext()) {
                UIElementGroupData sourcePage = (UIElementGroupData) pageIterator.next();

                Iterator componentIterator = sourcePage.iterator();
                while (componentIterator.hasNext()) {
                    UIElementGroupData component = (UIElementGroupData) componentIterator.next();
                    // search for selected element
                    if (component.getTechKey().equals(elementKey)) {
                        // add a copy of selected element to target page if it is not already there
                        if (page.getElement(component.getName()) == null) {
                            try {
                                page.addElement((UIElementGroupData) component.clone());
                            } catch (CloneNotSupportedException e) {
                                log.error(e);
                            }
                        }
                    }
                    Iterator groupIterator = component.iterator();
                    while (groupIterator.hasNext()) {
                        UIElementGroupData group = (UIElementGroupData) groupIterator.next();
                        // search for selected element
                        if (group.getTechKey().equals(elementKey)) {
                            // check if compoment alreads exists
                            UIElementGroupData targetComponent = (UIElementGroupData) page.getElement(component.getName());
                            // copy element to target component
                            if (targetComponent == null) {
                                try {
                                    targetComponent = (UIElementGroupData) component.cloneWithoutElements();
                                    page.addElement(targetComponent);
                                } catch (CloneNotSupportedException e) {
                                    log.error(e);
                                }
                            }
                            if (targetComponent.getElement(group.getName()) == null) {
                                try {
                                    targetComponent.addElement((UIElementGroupData) group.clone());
                                } catch (CloneNotSupportedException e) {
                                    log.error(e);                                   }
                            }
                        }
                        Iterator elementIterator = group.iterator();
                        while (elementIterator.hasNext()){
                            UIElementData element = (UIElementData) elementIterator.next();
                            // search for selected element
                            if (element.getTechKey().equals(elementKey)){
                                // check if element already exists
                                UIElementGroupData targetComponent = (UIElementGroupData) page.getElement(component.getName());
                                if (targetComponent == null){
                                    try{
                                        targetComponent = (UIElementGroupData) component.cloneWithoutElements();
                                        page.addElement(targetComponent);
                                    } catch (CloneNotSupportedException e) {
                                        log.error(e);
                                    }
                                }
                                UIElementGroupData targetGroup = (UIElementGroupData) targetComponent.getElement(group.getName());
                                if (targetGroup == null){
                                    try {
                                        targetGroup = (UIElementGroupData) group.cloneWithoutElements(); 
                                        targetComponent.addElement(targetGroup);
                                    } catch (CloneNotSupportedException e) {
                                        log.error(e);                                   }
                                }
                                UIElementData targetElement = (UIElementData) targetGroup.getElement(element.getName());
                                if (targetElement == null){
                                    try {
                                        targetGroup.addElement((UIElementData) element.clone());
                                    } catch (CloneNotSupportedException e) {
                                        log.error(e);                                   }
                                }                   
                            }
                            
                        }
                    }
                }

            }
        }

        
        
        /**
         * <p>Overwrites/Implements the method <code>getMaintenanceExitData</code> 
         * of the <code>UIProvider</code> interface. </p>
         * 
         * @return an instance of the class {@link IPCUIMaintenanceExitHandler}
         * 
         * @see com.sap.isa.maintenanceobject.backend.boi.DynamicUIProviderData#getMaintenanceExitData()
         */
        public DynamicUIMaintenanceExitData getMaintenanceExitData() {
            // TODO Auto-generated method stub
            if (maintenanceExitHandler == null) {
                maintenanceExitHandler = new IPCUIMaintenanceExitHandler();
            }
            return maintenanceExitHandler;
        }
        
        /**
         * <p> The method getDynamicUIExitsHandler provides exits which can be used for Dynamic UI
         * implementation </p>
         * 
         * @return instance of {@link IPCUIExitHandler}
         */
        public DynamicUIExitData getDynamicUIExitData(){
            if (exitHandler == null) {
                exitHandler = new IPCUIExitHandler();
            }
            return exitHandler;
        }
        
    }

}
