package com.sap.isa.ipc.ui.jsp.dynamicui.renderer;

import com.sap.isa.ipc.ui.jsp.beans.CharacteristicUIBean;
import com.sap.isa.ipc.ui.jsp.dynamicui.IPCProperty;
import com.sap.isa.ipc.ui.jsp.dynamicui.IPCDynamicUIConstant.UITypes;
import com.sap.isa.ipc.ui.jsp.uiclass.UIContext;
import com.sap.spc.remote.client.ResourceAccessor;
import com.sap.tc.logging.Category;
import com.sap.tc.logging.Location;
import com.sap.tc.logging.Severity;

public class IPCUITypeDetermination {

    private static final Category category = ResourceAccessor.category;
    private static final Location location = ResourceAccessor.getLocation(IPCUITypeDetermination.class);

    /**
     * Determines the correct uiType using a given CharacteristicUIBean, IPCProperty
     * and csticStatusChange-information.
     * The decision is made between the following uiTypes:
     * <ol>
     * <li>UiType 1: Radio buttons only
     * <li>UiType 2: Radion buttons and input field
     * <li>UiType 3: Checkboxes only
     * <li>UiType 4: Checkboxes and input field
     * <li>UiType 5: DDLB only
     * <li>UiType 6: DDLB and input field
     * <li>UiType 7: Multi-DDLB only
     * <li>UiType 8: Multi-DDLB and input field
     * <li>UiType 9: Input field only
     * <li>UiType 10: Input field with calendar control
     * <li>UiType 11: Message characteristic
     * <li>Error: This combination of attributes should never occur.
     * </ol>
     * The following attributes are taken into account:
     * <ul>
     * <li>isMessageCstic: The cstic is a message cstic and the feature is enalbed in XCM. In this case it is rendered not as a cstic with value but as a message.
     * <li>showCal: The calendar-control should be displayed. This is calculated in CharacteristicUIBean.showCalendarControl(). It takes the cstic-type and the XCM setting into account.
     * <li>hasValues: The characteristic has values, i.e. method getValues returns a list with a size > 0)
     * <li>isExpanded: The values are displayed expanded, e.g. as checkboxes or radiobuttons.
     * <li>isDomainConstrained: The values of this cstic are constrained by a dynamic domain. Also if the cstic only has a static domain this returns T. 
     * <li>hasIntervalDomain: whether the characteristic has an interval as domain 
     * <li>allowsMultipleValues: is it allowed to assign more than one value
     * <li>allowsAdditionalValues: The user can enter any value (i.e. also values that were not specified in the domain)
     * <li>hasDomainValues: At least one value of the cstic belongs to the static domain.
     * <li>hasAssignedValues: The user or the system already assigned at least one value to this cstic.
     * </ul>
     * The decision table:
     * <table border="1">                                                                                                                                                                                      
     * <tr><th>    isMessageCstic  </th><th>   showCal </th><th>   hasValues   </th><th>   isExpanded  </th><th>   isDomainConstrained </th><th>   hasIntervalAsDomain </th><th>   allowMultiValues    </th><th>   allowAdditionalValues   </th><th>   hasDomainValues </th><th>   hasAssignedValues   </th><th>   integer </th><th>   U01 </th><th>   U02 </th><th>   U03 </th><th>   U04 </th><th>   U05 </th><th>   U06 </th><th>   U07 </th><th>   U08 </th><th>   U09 </th><th>   U10 </th><th>   U11 </th><th>   Error   </th></tr>
     * <tr><td>    0   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>       </td><td>       </td><td>       </td></tr>
     * <tr><td>    0   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>       </td><td>       </td><td>   X   </td></tr>
     * <tr><td>    0   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   2   </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td></tr>
     * <tr><td>    0   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   3   </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td></tr>
     * <tr><td>    0   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   4   </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>       </td><td>       </td><td>       </td></tr>
     * <tr><td>    0   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   5   </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>       </td><td>       </td><td>   X   </td></tr>
     * <tr><td>    0   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   6   </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td></tr>
     * <tr><td>    0   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   7   </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td></tr>
     * <tr><td>    0   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   8   </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>       </td><td>       </td><td>       </td></tr>
     * <tr><td>    0   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   9   </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>       </td><td>       </td><td>       </td><td>   X   </td></tr>
     * <tr><td>    0   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   10  </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>       </td><td>       </td><td>       </td><td>   X   </td></tr>
     * <tr><td>    0   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   11  </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>       </td><td>       </td><td>       </td><td>   X   </td></tr>
     * <tr><td>    0   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   12  </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>       </td><td>       </td><td>       </td></tr>
     * <tr><td>    0   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   13  </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>       </td><td>       </td><td>       </td><td>   X   </td></tr>
     * <tr><td>    0   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   14  </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>       </td><td>       </td><td>       </td><td>       </td></tr>
     * <tr><td>    0   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   15  </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>       </td><td>       </td><td>       </td><td>   X   </td></tr>
     * <tr><td>    0   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   16  </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td></tr>
     * <tr><td>    0   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   17  </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td></tr>
     * <tr><td>    0   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   18  </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td></tr>
     * <tr><td>    0   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   19  </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td></tr>
     * <tr><td>    0   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   20  </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td></tr>
     * <tr><td>    0   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   21  </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td></tr>
     * <tr><td>    0   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   22  </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td></tr>
     * <tr><td>    0   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   23  </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td></tr>
     * <tr><td>    0   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   24  </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>       </td><td>       </td><td>       </td><td>   X   </td></tr>
     * <tr><td>    0   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   25  </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>       </td><td>       </td><td>       </td><td>   X   </td></tr>
     * <tr><td>    0   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   26  </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>       </td><td>       </td><td>       </td><td>   X   </td></tr>
     * <tr><td>    0   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   27  </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>       </td><td>       </td><td>       </td><td>   X   </td></tr>
     * <tr><td>    0   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   28  </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>       </td><td>       </td><td>       </td></tr>
     * <tr><td>    0   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   29  </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>       </td><td>       </td><td>   X   </td></tr>
     * <tr><td>    0   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   30  </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>       </td><td>       </td><td>       </td></tr>
     * <tr><td>    0   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   31  </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>       </td><td>       </td><td>   X   </td></tr>
     * <tr><td>    0   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   32  </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td></tr>
     * <tr><td>    0   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   33  </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td></tr>
     * <tr><td>    0   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   34  </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td></tr>
     * <tr><td>    0   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   35  </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td></tr>
     * <tr><td>    0   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   36  </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td></tr>
     * <tr><td>    0   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   37  </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td></tr>
     * <tr><td>    0   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   38  </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td></tr>
     * <tr><td>    0   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   39  </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td></tr>
     * <tr><td>    0   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   40  </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td></tr>
     * <tr><td>    0   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   41  </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td></tr>
     * <tr><td>    0   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   42  </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td></tr>
     * <tr><td>    0   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   43  </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td></tr>
     * <tr><td>    0   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   44  </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>       </td><td>       </td><td>       </td><td>   X   </td></tr>
     * <tr><td>    0   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   45  </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>       </td><td>       </td><td>       </td><td>   X   </td></tr>
     * <tr><td>    0   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   46  </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td></tr>
     * <tr><td>    0   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   47  </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td></tr>
     * <tr><td>    0   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   48  </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>       </td><td>       </td><td>       </td></tr>
     * <tr><td>    0   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   49  </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>       </td><td>       </td><td>   X   </td></tr>
     * <tr><td>    0   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   50  </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td></tr>
     * <tr><td>    0   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   51  </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>       </td><td>       </td><td>   X   </td></tr>
     * <tr><td>    0   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   52  </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>       </td><td>       </td><td>       </td></tr>
     * <tr><td>    0   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   53  </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>       </td><td>       </td><td>   X   </td></tr>
     * <tr><td>    0   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   54  </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td></tr>
     * <tr><td>    0   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   55  </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td></tr>
     * <tr><td>    0   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   56  </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>       </td><td>       </td><td>       </td></tr>
     * <tr><td>    0   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   57  </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>       </td><td>       </td><td>       </td><td>   X   </td></tr>
     * <tr><td>    0   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   58  </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>       </td><td>       </td><td>       </td><td>       </td></tr>
     * <tr><td>    0   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   59  </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>       </td><td>       </td><td>       </td><td>   X   </td></tr>
     * <tr><td>    0   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   60  </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>       </td><td>       </td><td>       </td></tr>
     * <tr><td>    0   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   61  </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>       </td><td>       </td><td>       </td><td>   X   </td></tr>
     * <tr><td>    0   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   62  </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>       </td><td>       </td><td>       </td><td>       </td></tr>
     * <tr><td>    0   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   63  </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>       </td><td>       </td><td>       </td><td>   X   </td></tr>
     * <tr><td>    0   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   64  </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>       </td><td>       </td><td>       </td></tr>
     * <tr><td>    0   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   65  </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>       </td><td>       </td><td>   X   </td></tr>
     * <tr><td>    0   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   66  </td><td>       </td><td>   X   </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td></tr>
     * <tr><td>    0   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   67  </td><td>       </td><td>   X   </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td></tr>
     * <tr><td>    0   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   68  </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>       </td><td>       </td><td>       </td></tr>
     * <tr><td>    0   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   69  </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>       </td><td>       </td><td>   X   </td></tr>
     * <tr><td>    0   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   70  </td><td>       </td><td>   X   </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td></tr>
     * <tr><td>    0   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   71  </td><td>       </td><td>   X   </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td></tr>
     * <tr><td>    0   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   72  </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>       </td><td>       </td><td>       </td></tr>
     * <tr><td>    0   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   73  </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td></tr>
     * <tr><td>    0   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   74  </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td></tr>
     * <tr><td>    0   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   75  </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td></tr>
     * <tr><td>    0   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   76  </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>       </td><td>       </td><td>       </td></tr>
     * <tr><td>    0   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   77  </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td></tr>
     * <tr><td>    0   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   78  </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td></tr>
     * <tr><td>    0   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   79  </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td></tr>
     * <tr><td>    0   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   80  </td><td>       </td><td>   X   </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td></tr>
     * <tr><td>    0   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   81  </td><td>       </td><td>   X   </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td></tr>
     * <tr><td>    0   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   82  </td><td>       </td><td>   X   </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td></tr>
     * <tr><td>    0   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   83  </td><td>       </td><td>   X   </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td></tr>
     * <tr><td>    0   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   84  </td><td>       </td><td>   X   </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td></tr>
     * <tr><td>    0   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   85  </td><td>       </td><td>   X   </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td></tr>
     * <tr><td>    0   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   86  </td><td>       </td><td>   X   </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td></tr>
     * <tr><td>    0   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   87  </td><td>       </td><td>   X   </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td></tr>
     * <tr><td>    0   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   88  </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td></tr>
     * <tr><td>    0   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   89  </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td></tr>
     * <tr><td>    0   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   90  </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td></tr>
     * <tr><td>    0   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   91  </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td></tr>
     * <tr><td>    0   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   92  </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>       </td><td>       </td><td>       </td></tr>
     * <tr><td>    0   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   93  </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>       </td><td>       </td><td>   X   </td></tr>
     * <tr><td>    0   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   94  </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>       </td><td>       </td><td>       </td></tr>
     * <tr><td>    0   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   95  </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>       </td><td>       </td><td>   X   </td></tr>
     * <tr><td>    0   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   96  </td><td>   X   </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td></tr>
     * <tr><td>    0   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   97  </td><td>   X   </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td></tr>
     * <tr><td>    0   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   98  </td><td>   X   </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td></tr>
     * <tr><td>    0   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   99  </td><td>   X   </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td></tr>
     * <tr><td>    0   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   100 </td><td>       </td><td>   X   </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td></tr>
     * <tr><td>    0   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   101 </td><td>       </td><td>   X   </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td></tr>
     * <tr><td>    0   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   102 </td><td>   X   </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td></tr>
     * <tr><td>    0   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   103 </td><td>   X   </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td></tr>
     * <tr><td>    0   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   104 </td><td>       </td><td>       </td><td>   X   </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td></tr>
     * <tr><td>    0   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   105 </td><td>       </td><td>       </td><td>   X   </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td></tr>
     * <tr><td>    0   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   106 </td><td>       </td><td>       </td><td>   X   </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td></tr>
     * <tr><td>    0   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   107 </td><td>       </td><td>       </td><td>   X   </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td></tr>
     * <tr><td>    0   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   108 </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td></tr>
     * <tr><td>    0   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   109 </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td></tr>
     * <tr><td>    0   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   110 </td><td>       </td><td>       </td><td>   X   </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td></tr>
     * <tr><td>    0   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   111 </td><td>       </td><td>       </td><td>   X   </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td></tr>
     * <tr><td>    0   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   112 </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>       </td><td>       </td><td>       </td></tr>
     * <tr><td>    0   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   113 </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>       </td><td>       </td><td>   X   </td></tr>
     * <tr><td>    0   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   114 </td><td>       </td><td>   X   </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td></tr>
     * <tr><td>    0   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   115 </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>       </td><td>       </td><td>   X   </td></tr>
     * <tr><td>    0   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   116 </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>       </td><td>       </td><td>       </td></tr>
     * <tr><td>    0   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   117 </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>       </td><td>       </td><td>   X   </td></tr>
     * <tr><td>    0   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   118 </td><td>       </td><td>   X   </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td></tr>
     * <tr><td>    0   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   119 </td><td>       </td><td>   X   </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td></tr>
     * <tr><td>    0   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   120 </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>       </td><td>       </td><td>       </td></tr>
     * <tr><td>    0   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   121 </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td></tr>
     * <tr><td>    0   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   122 </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td></tr>
     * <tr><td>    0   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   123 </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td></tr>
     * <tr><td>    0   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   124 </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>       </td><td>       </td><td>       </td></tr>
     * <tr><td>    0   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   125 </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td></tr>
     * <tr><td>    0   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   126 </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td></tr>
     * <tr><td>    0   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   127 </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td></tr>
     * <tr><td>    0   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   128 </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>       </td><td>       </td><td>       </td></tr>
     * <tr><td>    0   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   129 </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>       </td><td>       </td><td>       </td></tr>
     * <tr><td>    0   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   130 </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td></tr>
     * <tr><td>    0   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   131 </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td></tr>
     * <tr><td>    0   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   132 </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>       </td><td>       </td><td>       </td></tr>
     * <tr><td>    0   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   133 </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>       </td><td>       </td><td>       </td></tr>
     * <tr><td>    0   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   134 </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td></tr>
     * <tr><td>    0   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   135 </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td></tr>
     * <tr><td>    0   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   136 </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>       </td><td>       </td><td>       </td></tr>
     * <tr><td>    0   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   137 </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>       </td><td>       </td><td>       </td><td>       </td></tr>
     * <tr><td>    0   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   138 </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>       </td><td>       </td><td>       </td><td>   X   </td></tr>
     * <tr><td>    0   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   139 </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>       </td><td>       </td><td>       </td><td>   X   </td></tr>
     * <tr><td>    0   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   140 </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>       </td><td>       </td><td>       </td></tr>
     * <tr><td>    0   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   141 </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>       </td><td>       </td><td>       </td><td>       </td></tr>
     * <tr><td>    0   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   142 </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>       </td><td>       </td><td>       </td><td>       </td></tr>
     * <tr><td>    0   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   143 </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>       </td><td>       </td><td>       </td><td>       </td></tr>
     * <tr><td>    0   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   144 </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>       </td><td>       </td><td>   X   </td></tr>
     * <tr><td>    0   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   145 </td><td>       </td><td>   X   </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td></tr>
     * <tr><td>    0   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   146 </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>       </td><td>       </td><td>   X   </td></tr>
     * <tr><td>    0   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   147 </td><td>       </td><td>   X   </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td></tr>
     * <tr><td>    0   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   148 </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>       </td><td>       </td><td>   X   </td></tr>
     * <tr><td>    0   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   149 </td><td>       </td><td>   X   </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td></tr>
     * <tr><td>    0   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   150 </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>       </td><td>       </td><td>   X   </td></tr>
     * <tr><td>    0   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   151 </td><td>       </td><td>   X   </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td></tr>
     * <tr><td>    0   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   152 </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>       </td><td>       </td><td>   X   </td></tr>
     * <tr><td>    0   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   153 </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td></tr>
     * <tr><td>    0   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   154 </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>       </td><td>       </td><td>   X   </td></tr>
     * <tr><td>    0   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   155 </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td></tr>
     * <tr><td>    0   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   156 </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>       </td><td>       </td><td>       </td></tr>
     * <tr><td>    0   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   157 </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>       </td><td>       </td><td>       </td></tr>
     * <tr><td>    0   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   158 </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>       </td><td>       </td><td>       </td></tr>
     * <tr><td>    0   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   159 </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>       </td><td>       </td><td>       </td></tr>
     * <tr><td>    0   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   160 </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td></tr>
     * <tr><td>    0   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   161 </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td></tr>
     * <tr><td>    0   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   162 </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td></tr>
     * <tr><td>    0   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   163 </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td></tr>
     * <tr><td>    0   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   164 </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td></tr>
     * <tr><td>    0   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   165 </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td></tr>
     * <tr><td>    0   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   166 </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td></tr>
     * <tr><td>    0   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   167 </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td></tr>
     * <tr><td>    0   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   168 </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td></tr>
     * <tr><td>    0   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   169 </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td></tr>
     * <tr><td>    0   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   170 </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td></tr>
     * <tr><td>    0   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   171 </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td></tr>
     * <tr><td>    0   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   172 </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>       </td><td>       </td><td>       </td><td>   X   </td></tr>
     * <tr><td>    0   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   173 </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>       </td><td>       </td><td>       </td><td>   X   </td></tr>
     * <tr><td>    0   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   174 </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td></tr>
     * <tr><td>    0   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   175 </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td></tr>
     * <tr><td>    0   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   176 </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>       </td><td>       </td><td>       </td></tr>
     * <tr><td>    0   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   177 </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>       </td><td>       </td><td>       </td></tr>
     * <tr><td>    0   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   178 </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>       </td><td>       </td><td>       </td></tr>
     * <tr><td>    0   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   179 </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>       </td><td>       </td><td>       </td></tr>
     * <tr><td>    0   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   180 </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>       </td><td>       </td><td>       </td></tr>
     * <tr><td>    0   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   181 </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>       </td><td>       </td><td>       </td></tr>
     * <tr><td>    0   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   182 </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>       </td><td>       </td><td>       </td></tr>
     * <tr><td>    0   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   183 </td><td>       </td><td>   X   </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td></tr>
     * <tr><td>    0   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   184 </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>       </td><td>       </td><td>       </td></tr>
     * <tr><td>    0   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   185 </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td></tr>
     * <tr><td>    0   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   186 </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>       </td><td>       </td><td>       </td></tr>
     * <tr><td>    0   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   187 </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td></tr>
     * <tr><td>    0   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   188 </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>       </td><td>       </td><td>       </td></tr>
     * <tr><td>    0   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   189 </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td></tr>
     * <tr><td>    0   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   190 </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>       </td><td>       </td><td>       </td></tr>
     * <tr><td>    0   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   191 </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td></tr>
     * <tr><td>    0   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   192 </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>       </td><td>       </td><td>       </td></tr>
     * <tr><td>    0   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   193 </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>       </td><td>       </td><td>       </td></tr>
     * <tr><td>    0   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   194 </td><td>       </td><td>   X   </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td></tr>
     * <tr><td>    0   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   195 </td><td>       </td><td>   X   </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td></tr>
     * <tr><td>    0   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   196 </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>       </td><td>       </td><td>       </td></tr>
     * <tr><td>    0   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   197 </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>       </td><td>       </td><td>       </td></tr>
     * <tr><td>    0   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   198 </td><td>       </td><td>   X   </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td></tr>
     * <tr><td>    0   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   199 </td><td>       </td><td>   X   </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td></tr>
     * <tr><td>    0   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   200 </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>       </td><td>       </td><td>       </td></tr>
     * <tr><td>    0   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   201 </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td></tr>
     * <tr><td>    0   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   202 </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td></tr>
     * <tr><td>    0   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   203 </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td></tr>
     * <tr><td>    0   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   204 </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>       </td><td>       </td><td>       </td></tr>
     * <tr><td>    0   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   205 </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td></tr>
     * <tr><td>    0   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   206 </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td></tr>
     * <tr><td>    0   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   207 </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td></tr>
     * <tr><td>    0   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   208 </td><td>       </td><td>   X   </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td></tr>
     * <tr><td>    0   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   209 </td><td>       </td><td>   X   </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td></tr>
     * <tr><td>    0   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   210 </td><td>       </td><td>   X   </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td></tr>
     * <tr><td>    0   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   211 </td><td>       </td><td>   X   </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td></tr>
     * <tr><td>    0   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   212 </td><td>       </td><td>   X   </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td></tr>
     * <tr><td>    0   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   213 </td><td>       </td><td>   X   </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td></tr>
     * <tr><td>    0   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   214 </td><td>       </td><td>   X   </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td></tr>
     * <tr><td>    0   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   215 </td><td>       </td><td>   X   </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td></tr>
     * <tr><td>    0   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   216 </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td></tr>
     * <tr><td>    0   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   217 </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td></tr>
     * <tr><td>    0   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   218 </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td></tr>
     * <tr><td>    0   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   219 </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td></tr>
     * <tr><td>    0   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   220 </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>       </td><td>       </td><td>       </td></tr>
     * <tr><td>    0   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   221 </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>       </td><td>       </td><td>       </td></tr>
     * <tr><td>    0   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   222 </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>       </td><td>       </td><td>       </td></tr>
     * <tr><td>    0   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   223 </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>       </td><td>       </td><td>       </td></tr>
     * <tr><td>    0   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   224 </td><td>   X   </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td></tr>
     * <tr><td>    0   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   225 </td><td>   X   </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td></tr>
     * <tr><td>    0   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   226 </td><td>   X   </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td></tr>
     * <tr><td>    0   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   227 </td><td>   X   </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td></tr>
     * <tr><td>    0   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   228 </td><td>       </td><td>   X   </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td></tr>
     * <tr><td>    0   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   229 </td><td>       </td><td>   X   </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td></tr>
     * <tr><td>    0   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   230 </td><td>   X   </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td></tr>
     * <tr><td>    0   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   231 </td><td>   X   </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td></tr>
     * <tr><td>    0   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   232 </td><td>       </td><td>       </td><td>   X   </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td></tr>
     * <tr><td>    0   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   233 </td><td>       </td><td>       </td><td>   X   </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td></tr>
     * <tr><td>    0   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   234 </td><td>       </td><td>       </td><td>   X   </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td></tr>
     * <tr><td>    0   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   235 </td><td>       </td><td>       </td><td>   X   </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td></tr>
     * <tr><td>    0   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   236 </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td></tr>
     * <tr><td>    0   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   237 </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td></tr>
     * <tr><td>    0   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   238 </td><td>       </td><td>       </td><td>   X   </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td></tr>
     * <tr><td>    0   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   239 </td><td>       </td><td>       </td><td>   X   </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td></tr>
     * <tr><td>    0   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   240 </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>       </td><td>       </td><td>       </td></tr>
     * <tr><td>    0   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   241 </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>       </td><td>       </td><td>       </td></tr>
     * <tr><td>    0   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   242 </td><td>       </td><td>   X   </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td></tr>
     * <tr><td>    0   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   243 </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>       </td><td>       </td><td>       </td></tr>
     * <tr><td>    0   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   244 </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>       </td><td>       </td><td>       </td></tr>
     * <tr><td>    0   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   245 </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>       </td><td>       </td><td>       </td></tr>
     * <tr><td>    0   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   246 </td><td>       </td><td>   X   </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td></tr>
     * <tr><td>    0   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   247 </td><td>       </td><td>   X   </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td></tr>
     * <tr><td>    0   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   248 </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>       </td><td>       </td><td>       </td></tr>
     * <tr><td>    0   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   249 </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td></tr>
     * <tr><td>    0   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   250 </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td></tr>
     * <tr><td>    0   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   251 </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td></tr>
     * <tr><td>    0   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   252 </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>       </td><td>       </td><td>       </td></tr>
     * <tr><td>    0   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   253 </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td></tr>
     * <tr><td>    0   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   254 </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td></tr>
     * <tr><td>    0   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   255 </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td></tr>
     * <tr><td>    0   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   256 </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>       </td><td>       </td></tr>
     * <tr><td>    0   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   257 </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>       </td><td>   X   </td></tr>
     * <tr><td>    0   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   258 </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td></tr>
     * <tr><td>    0   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   259 </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td></tr>
     * <tr><td>    0   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   260 </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>       </td><td>       </td></tr>
     * <tr><td>    0   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   261 </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>       </td><td>   X   </td></tr>
     * <tr><td>    0   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   262 </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td></tr>
     * <tr><td>    0   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   263 </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td></tr>
     * <tr><td>    0   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   264 </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>       </td><td>       </td></tr>
     * <tr><td>    0   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   265 </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>       </td><td>       </td><td>       </td><td>   X   </td></tr>
     * <tr><td>    0   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   266 </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>       </td><td>       </td><td>       </td><td>   X   </td></tr>
     * <tr><td>    0   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   267 </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>       </td><td>       </td><td>       </td><td>   X   </td></tr>
     * <tr><td>    0   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   268 </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>       </td><td>       </td></tr>
     * <tr><td>    0   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   269 </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>       </td><td>       </td><td>       </td><td>   X   </td></tr>
     * <tr><td>    0   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   270 </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>       </td><td>       </td><td>       </td><td>       </td></tr>
     * <tr><td>    0   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   271 </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>       </td><td>       </td><td>       </td><td>   X   </td></tr>
     * <tr><td>    0   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   272 </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td></tr>
     * <tr><td>    0   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   273 </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td></tr>
     * <tr><td>    0   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   274 </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td></tr>
     * <tr><td>    0   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   275 </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td></tr>
     * <tr><td>    0   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   276 </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td></tr>
     * <tr><td>    0   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   277 </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td></tr>
     * <tr><td>    0   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   278 </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td></tr>
     * <tr><td>    0   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   279 </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td></tr>
     * <tr><td>    0   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   280 </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>       </td><td>       </td><td>       </td><td>   X   </td></tr>
     * <tr><td>    0   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   281 </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>       </td><td>       </td><td>       </td><td>   X   </td></tr>
     * <tr><td>    0   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   282 </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>       </td><td>       </td><td>       </td><td>   X   </td></tr>
     * <tr><td>    0   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   283 </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>       </td><td>       </td><td>       </td><td>   X   </td></tr>
     * <tr><td>    0   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   284 </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>       </td><td>       </td></tr>
     * <tr><td>    0   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   285 </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>       </td><td>   X   </td></tr>
     * <tr><td>    0   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   286 </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>       </td><td>       </td></tr>
     * <tr><td>    0   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   287 </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>       </td><td>   X   </td></tr>
     * <tr><td>    0   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   288 </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td></tr>
     * <tr><td>    0   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   289 </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td></tr>
     * <tr><td>    0   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   290 </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td></tr>
     * <tr><td>    0   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   291 </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td></tr>
     * <tr><td>    0   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   292 </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td></tr>
     * <tr><td>    0   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   293 </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td></tr>
     * <tr><td>    0   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   294 </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td></tr>
     * <tr><td>    0   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   295 </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td></tr>
     * <tr><td>    0   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   296 </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td></tr>
     * <tr><td>    0   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   297 </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td></tr>
     * <tr><td>    0   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   298 </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td></tr>
     * <tr><td>    0   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   299 </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td></tr>
     * <tr><td>    0   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   300 </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>       </td><td>       </td><td>       </td><td>   X   </td></tr>
     * <tr><td>    0   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   301 </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>       </td><td>       </td><td>       </td><td>   X   </td></tr>
     * <tr><td>    0   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   302 </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td></tr>
     * <tr><td>    0   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   303 </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td></tr>
     * <tr><td>    0   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   304 </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>       </td><td>       </td></tr>
     * <tr><td>    0   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   305 </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>       </td><td>   X   </td></tr>
     * <tr><td>    0   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   306 </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td></tr>
     * <tr><td>    0   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   307 </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>       </td><td>   X   </td></tr>
     * <tr><td>    0   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   308 </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>       </td><td>       </td></tr>
     * <tr><td>    0   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   309 </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>       </td><td>   X   </td></tr>
     * <tr><td>    0   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   310 </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td></tr>
     * <tr><td>    0   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   311 </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td></tr>
     * <tr><td>    0   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   312 </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>       </td><td>       </td></tr>
     * <tr><td>    0   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   313 </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>       </td><td>       </td><td>       </td><td>   X   </td></tr>
     * <tr><td>    0   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   314 </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>       </td><td>       </td><td>       </td><td>       </td></tr>
     * <tr><td>    0   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   315 </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>       </td><td>       </td><td>       </td><td>   X   </td></tr>
     * <tr><td>    0   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   316 </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>       </td><td>       </td></tr>
     * <tr><td>    0   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   317 </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>       </td><td>       </td><td>       </td><td>   X   </td></tr>
     * <tr><td>    0   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   318 </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>       </td><td>       </td><td>       </td><td>       </td></tr>
     * <tr><td>    0   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   319 </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>       </td><td>       </td><td>       </td><td>   X   </td></tr>
     * <tr><td>    0   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   320 </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>       </td><td>       </td></tr>
     * <tr><td>    0   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   321 </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>       </td><td>   X   </td></tr>
     * <tr><td>    0   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   322 </td><td>       </td><td>   X   </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td></tr>
     * <tr><td>    0   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   323 </td><td>       </td><td>   X   </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td></tr>
     * <tr><td>    0   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   324 </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>       </td><td>       </td></tr>
     * <tr><td>    0   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   325 </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>       </td><td>   X   </td></tr>
     * <tr><td>    0   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   326 </td><td>       </td><td>   X   </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td></tr>
     * <tr><td>    0   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   327 </td><td>       </td><td>   X   </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td></tr>
     * <tr><td>    0   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   328 </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>       </td><td>       </td></tr>
     * <tr><td>    0   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   329 </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td></tr>
     * <tr><td>    0   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   330 </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td></tr>
     * <tr><td>    0   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   331 </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td></tr>
     * <tr><td>    0   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   332 </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>       </td><td>       </td></tr>
     * <tr><td>    0   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   333 </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td></tr>
     * <tr><td>    0   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   334 </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td></tr>
     * <tr><td>    0   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   335 </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td></tr>
     * <tr><td>    0   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   336 </td><td>       </td><td>   X   </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td></tr>
     * <tr><td>    0   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   337 </td><td>       </td><td>   X   </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td></tr>
     * <tr><td>    0   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   338 </td><td>       </td><td>   X   </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td></tr>
     * <tr><td>    0   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   339 </td><td>       </td><td>   X   </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td></tr>
     * <tr><td>    0   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   340 </td><td>       </td><td>   X   </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td></tr>
     * <tr><td>    0   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   341 </td><td>       </td><td>   X   </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td></tr>
     * <tr><td>    0   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   342 </td><td>       </td><td>   X   </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td></tr>
     * <tr><td>    0   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   343 </td><td>       </td><td>   X   </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td></tr>
     * <tr><td>    0   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   344 </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td></tr>
     * <tr><td>    0   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   345 </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td></tr>
     * <tr><td>    0   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   346 </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td></tr>
     * <tr><td>    0   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   347 </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td></tr>
     * <tr><td>    0   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   348 </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>       </td><td>       </td></tr>
     * <tr><td>    0   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   349 </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>       </td><td>   X   </td></tr>
     * <tr><td>    0   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   350 </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>       </td><td>       </td></tr>
     * <tr><td>    0   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   351 </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>       </td><td>   X   </td></tr>
     * <tr><td>    0   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   352 </td><td>   X   </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td></tr>
     * <tr><td>    0   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   353 </td><td>   X   </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td></tr>
     * <tr><td>    0   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   354 </td><td>   X   </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td></tr>
     * <tr><td>    0   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   355 </td><td>   X   </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td></tr>
     * <tr><td>    0   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   356 </td><td>       </td><td>   X   </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td></tr>
     * <tr><td>    0   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   357 </td><td>       </td><td>   X   </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td></tr>
     * <tr><td>    0   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   358 </td><td>   X   </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td></tr>
     * <tr><td>    0   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   359 </td><td>   X   </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td></tr>
     * <tr><td>    0   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   360 </td><td>       </td><td>       </td><td>   X   </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td></tr>
     * <tr><td>    0   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   361 </td><td>       </td><td>       </td><td>   X   </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td></tr>
     * <tr><td>    0   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   362 </td><td>       </td><td>       </td><td>   X   </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td></tr>
     * <tr><td>    0   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   363 </td><td>       </td><td>       </td><td>   X   </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td></tr>
     * <tr><td>    0   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   364 </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td></tr>
     * <tr><td>    0   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   365 </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td></tr>
     * <tr><td>    0   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   366 </td><td>       </td><td>       </td><td>   X   </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td></tr>
     * <tr><td>    0   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   367 </td><td>       </td><td>       </td><td>   X   </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td></tr>
     * <tr><td>    0   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   368 </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>       </td><td>       </td></tr>
     * <tr><td>    0   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   369 </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>       </td><td>   X   </td></tr>
     * <tr><td>    0   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   370 </td><td>       </td><td>   X   </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td></tr>
     * <tr><td>    0   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   371 </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>       </td><td>   X   </td></tr>
     * <tr><td>    0   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   372 </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>       </td><td>       </td></tr>
     * <tr><td>    0   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   373 </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>       </td><td>   X   </td></tr>
     * <tr><td>    0   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   374 </td><td>       </td><td>   X   </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td></tr>
     * <tr><td>    0   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   375 </td><td>       </td><td>   X   </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td></tr>
     * <tr><td>    0   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   376 </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>       </td><td>       </td></tr>
     * <tr><td>    0   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   377 </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td></tr>
     * <tr><td>    0   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   378 </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td></tr>
     * <tr><td>    0   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   379 </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td></tr>
     * <tr><td>    0   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   380 </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>       </td><td>       </td></tr>
     * <tr><td>    0   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   381 </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td></tr>
     * <tr><td>    0   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   382 </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td></tr>
     * <tr><td>    0   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   383 </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td></tr>
     * <tr><td>    0   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   384 </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>       </td><td>       </td></tr>
     * <tr><td>    0   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   385 </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>       </td><td>       </td></tr>
     * <tr><td>    0   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   386 </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td></tr>
     * <tr><td>    0   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   387 </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td></tr>
     * <tr><td>    0   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   388 </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>       </td><td>       </td></tr>
     * <tr><td>    0   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   389 </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>       </td><td>       </td></tr>
     * <tr><td>    0   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   390 </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td></tr>
     * <tr><td>    0   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   391 </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td></tr>
     * <tr><td>    0   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   392 </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>       </td><td>       </td></tr>
     * <tr><td>    0   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   393 </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>       </td><td>       </td><td>       </td><td>       </td></tr>
     * <tr><td>    0   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   394 </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>       </td><td>       </td><td>       </td><td>   X   </td></tr>
     * <tr><td>    0   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   395 </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>       </td><td>       </td><td>       </td><td>   X   </td></tr>
     * <tr><td>    0   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   396 </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>       </td><td>       </td></tr>
     * <tr><td>    0   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   397 </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>       </td><td>       </td><td>       </td><td>       </td></tr>
     * <tr><td>    0   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   398 </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>       </td><td>       </td><td>       </td><td>       </td></tr>
     * <tr><td>    0   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   399 </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>       </td><td>       </td><td>       </td><td>       </td></tr>
     * <tr><td>    0   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   400 </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>       </td><td>   X   </td></tr>
     * <tr><td>    0   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   401 </td><td>       </td><td>   X   </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td></tr>
     * <tr><td>    0   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   402 </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>       </td><td>   X   </td></tr>
     * <tr><td>    0   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   403 </td><td>       </td><td>   X   </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td></tr>
     * <tr><td>    0   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   404 </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>       </td><td>   X   </td></tr>
     * <tr><td>    0   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   405 </td><td>       </td><td>   X   </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td></tr>
     * <tr><td>    0   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   406 </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>       </td><td>   X   </td></tr>
     * <tr><td>    0   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   407 </td><td>       </td><td>   X   </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td></tr>
     * <tr><td>    0   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   408 </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>       </td><td>   X   </td></tr>
     * <tr><td>    0   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   409 </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td></tr>
     * <tr><td>    0   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   410 </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>       </td><td>   X   </td></tr>
     * <tr><td>    0   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   411 </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td></tr>
     * <tr><td>    0   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   412 </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>       </td><td>       </td></tr>
     * <tr><td>    0   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   413 </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>       </td><td>       </td></tr>
     * <tr><td>    0   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   414 </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>       </td><td>       </td></tr>
     * <tr><td>    0   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   415 </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>       </td><td>       </td></tr>
     * <tr><td>    0   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   416 </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td></tr>
     * <tr><td>    0   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   417 </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td></tr>
     * <tr><td>    0   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   418 </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td></tr>
     * <tr><td>    0   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   419 </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td></tr>
     * <tr><td>    0   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   420 </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td></tr>
     * <tr><td>    0   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   421 </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td></tr>
     * <tr><td>    0   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   422 </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td></tr>
     * <tr><td>    0   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   423 </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td></tr>
     * <tr><td>    0   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   424 </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td></tr>
     * <tr><td>    0   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   425 </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td></tr>
     * <tr><td>    0   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   426 </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td></tr>
     * <tr><td>    0   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   427 </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td></tr>
     * <tr><td>    0   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   428 </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>       </td><td>       </td><td>       </td><td>   X   </td></tr>
     * <tr><td>    0   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   429 </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>       </td><td>       </td><td>       </td><td>   X   </td></tr>
     * <tr><td>    0   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   430 </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td></tr>
     * <tr><td>    0   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   431 </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td></tr>
     * <tr><td>    0   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   432 </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>       </td><td>       </td></tr>
     * <tr><td>    0   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   433 </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>       </td><td>       </td></tr>
     * <tr><td>    0   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   434 </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>       </td><td>       </td></tr>
     * <tr><td>    0   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   435 </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>       </td><td>       </td></tr>
     * <tr><td>    0   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   436 </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>       </td><td>       </td></tr>
     * <tr><td>    0   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   437 </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>       </td><td>       </td></tr>
     * <tr><td>    0   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   438 </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>       </td><td>       </td></tr>
     * <tr><td>    0   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   439 </td><td>       </td><td>   X   </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td></tr>
     * <tr><td>    0   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   440 </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>       </td><td>       </td></tr>
     * <tr><td>    0   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   441 </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td></tr>
     * <tr><td>    0   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   442 </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>       </td><td>       </td></tr>
     * <tr><td>    0   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   443 </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td></tr>
     * <tr><td>    0   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   444 </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>       </td><td>       </td></tr>
     * <tr><td>    0   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   445 </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td></tr>
     * <tr><td>    0   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   446 </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>       </td><td>       </td></tr>
     * <tr><td>    0   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   447 </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td></tr>
     * <tr><td>    0   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   448 </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>       </td><td>       </td></tr>
     * <tr><td>    0   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   449 </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>       </td><td>       </td></tr>
     * <tr><td>    0   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   450 </td><td>       </td><td>   X   </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td></tr>
     * <tr><td>    0   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   451 </td><td>       </td><td>   X   </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td></tr>
     * <tr><td>    0   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   452 </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>       </td><td>       </td></tr>
     * <tr><td>    0   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   453 </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>       </td><td>       </td></tr>
     * <tr><td>    0   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   454 </td><td>       </td><td>   X   </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td></tr>
     * <tr><td>    0   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   455 </td><td>       </td><td>   X   </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td></tr>
     * <tr><td>    0   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   456 </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>       </td><td>       </td></tr>
     * <tr><td>    0   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   457 </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td></tr>
     * <tr><td>    0   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   458 </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td></tr>
     * <tr><td>    0   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   459 </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td></tr>
     * <tr><td>    0   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   460 </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>       </td><td>       </td></tr>
     * <tr><td>    0   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   461 </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td></tr>
     * <tr><td>    0   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   462 </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td></tr>
     * <tr><td>    0   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   463 </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td></tr>
     * <tr><td>    0   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   464 </td><td>       </td><td>   X   </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td></tr>
     * <tr><td>    0   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   465 </td><td>       </td><td>   X   </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td></tr>
     * <tr><td>    0   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   466 </td><td>       </td><td>   X   </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td></tr>
     * <tr><td>    0   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   467 </td><td>       </td><td>   X   </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td></tr>
     * <tr><td>    0   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   468 </td><td>       </td><td>   X   </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td></tr>
     * <tr><td>    0   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   469 </td><td>       </td><td>   X   </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td></tr>
     * <tr><td>    0   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   470 </td><td>       </td><td>   X   </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td></tr>
     * <tr><td>    0   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   471 </td><td>       </td><td>   X   </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td></tr>
     * <tr><td>    0   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   472 </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td></tr>
     * <tr><td>    0   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   473 </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td></tr>
     * <tr><td>    0   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   474 </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td></tr>
     * <tr><td>    0   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   475 </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td></tr>
     * <tr><td>    0   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   476 </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>       </td><td>       </td></tr>
     * <tr><td>    0   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   477 </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>       </td><td>       </td></tr>
     * <tr><td>    0   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   478 </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>       </td><td>       </td></tr>
     * <tr><td>    0   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   479 </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>       </td><td>       </td></tr>
     * <tr><td>    0   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   480 </td><td>   X   </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td></tr>
     * <tr><td>    0   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   481 </td><td>   X   </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td></tr>
     * <tr><td>    0   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   482 </td><td>   X   </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td></tr>
     * <tr><td>    0   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   483 </td><td>   X   </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td></tr>
     * <tr><td>    0   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   484 </td><td>       </td><td>   X   </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td></tr>
     * <tr><td>    0   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   485 </td><td>       </td><td>   X   </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td></tr>
     * <tr><td>    0   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   486 </td><td>   X   </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td></tr>
     * <tr><td>    0   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   487 </td><td>   X   </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td></tr>
     * <tr><td>    0   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   488 </td><td>       </td><td>       </td><td>   X   </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td></tr>
     * <tr><td>    0   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   489 </td><td>       </td><td>       </td><td>   X   </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td></tr>
     * <tr><td>    0   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   490 </td><td>       </td><td>       </td><td>   X   </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td></tr>
     * <tr><td>    0   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   491 </td><td>       </td><td>       </td><td>   X   </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td></tr>
     * <tr><td>    0   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   492 </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td></tr>
     * <tr><td>    0   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   493 </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td></tr>
     * <tr><td>    0   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   494 </td><td>       </td><td>       </td><td>   X   </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td></tr>
     * <tr><td>    0   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   495 </td><td>       </td><td>       </td><td>   X   </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td></tr>
     * <tr><td>    0   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   496 </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>       </td><td>       </td></tr>
     * <tr><td>    0   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   497 </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>       </td><td>       </td></tr>
     * <tr><td>    0   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   498 </td><td>       </td><td>   X   </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td></tr>
     * <tr><td>    0   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   499 </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>       </td><td>       </td></tr>
     * <tr><td>    0   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   500 </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>       </td><td>       </td></tr>
     * <tr><td>    0   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   501 </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>       </td><td>       </td></tr>
     * <tr><td>    0   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   502 </td><td>       </td><td>   X   </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td></tr>
     * <tr><td>    0   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   503 </td><td>       </td><td>   X   </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td></tr>
     * <tr><td>    0   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   504 </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>       </td><td>       </td></tr>
     * <tr><td>    0   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   505 </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td></tr>
     * <tr><td>    0   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   506 </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td></tr>
     * <tr><td>    0   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   507 </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td></tr>
     * <tr><td>    0   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   508 </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>       </td><td>       </td></tr>
     * <tr><td>    0   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   509 </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td></tr>
     * <tr><td>    0   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   510 </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td></tr>
     * <tr><td>    0   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   511 </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td></tr>
     * <tr><td>    1   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   512 </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>       </td></tr>
     * <tr><td>    1   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   513 </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>   X   </td></tr>
     * <tr><td>    1   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   514 </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>   X   </td></tr>
     * <tr><td>    1   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   515 </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>   X   </td></tr>
     * <tr><td>    1   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   516 </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>       </td></tr>
     * <tr><td>    1   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   517 </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>   X   </td></tr>
     * <tr><td>    1   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   518 </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>       </td></tr>
     * <tr><td>    1   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   519 </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>   X   </td></tr>
     * <tr><td>    1   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   520 </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>       </td></tr>
     * <tr><td>    1   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   521 </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>   X   </td></tr>
     * <tr><td>    1   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   522 </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>   X   </td></tr>
     * <tr><td>    1   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   523 </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>   X   </td></tr>
     * <tr><td>    1   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   524 </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>       </td></tr>
     * <tr><td>    1   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   525 </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>   X   </td></tr>
     * <tr><td>    1   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   526 </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>       </td></tr>
     * <tr><td>    1   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   527 </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>   X   </td></tr>
     * <tr><td>    1   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   528 </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>   X   </td></tr>
     * <tr><td>    1   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   529 </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>   X   </td></tr>
     * <tr><td>    1   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   530 </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>   X   </td></tr>
     * <tr><td>    1   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   531 </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>   X   </td></tr>
     * <tr><td>    1   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   532 </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>   X   </td></tr>
     * <tr><td>    1   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   533 </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>   X   </td></tr>
     * <tr><td>    1   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   534 </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>   X   </td></tr>
     * <tr><td>    1   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   535 </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>   X   </td></tr>
     * <tr><td>    1   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   536 </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>   X   </td></tr>
     * <tr><td>    1   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   537 </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>   X   </td></tr>
     * <tr><td>    1   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   538 </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>   X   </td></tr>
     * <tr><td>    1   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   539 </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>   X   </td></tr>
     * <tr><td>    1   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   540 </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>       </td></tr>
     * <tr><td>    1   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   541 </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>   X   </td></tr>
     * <tr><td>    1   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   542 </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>       </td></tr>
     * <tr><td>    1   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   543 </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>   X   </td></tr>
     * <tr><td>    1   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   544 </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>       </td></tr>
     * <tr><td>    1   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   545 </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>   X   </td></tr>
     * <tr><td>    1   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   546 </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>       </td></tr>
     * <tr><td>    1   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   547 </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>   X   </td></tr>
     * <tr><td>    1   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   548 </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>   X   </td></tr>
     * <tr><td>    1   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   549 </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>   X   </td></tr>
     * <tr><td>    1   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   550 </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>       </td></tr>
     * <tr><td>    1   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   551 </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>   X   </td></tr>
     * <tr><td>    1   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   552 </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>       </td></tr>
     * <tr><td>    1   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   553 </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>   X   </td></tr>
     * <tr><td>    1   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   554 </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>       </td></tr>
     * <tr><td>    1   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   555 </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>   X   </td></tr>
     * <tr><td>    1   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   556 </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>   X   </td></tr>
     * <tr><td>    1   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   557 </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>   X   </td></tr>
     * <tr><td>    1   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   558 </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>       </td></tr>
     * <tr><td>    1   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   559 </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>   X   </td></tr>
     * <tr><td>    1   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   560 </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>       </td></tr>
     * <tr><td>    1   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   561 </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>   X   </td></tr>
     * <tr><td>    1   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   562 </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>       </td></tr>
     * <tr><td>    1   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   563 </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>   X   </td></tr>
     * <tr><td>    1   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   564 </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>       </td></tr>
     * <tr><td>    1   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   565 </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>   X   </td></tr>
     * <tr><td>    1   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   566 </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>       </td></tr>
     * <tr><td>    1   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   567 </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>   X   </td></tr>
     * <tr><td>    1   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   568 </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>       </td></tr>
     * <tr><td>    1   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   569 </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>   X   </td></tr>
     * <tr><td>    1   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   570 </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>       </td></tr>
     * <tr><td>    1   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   571 </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>   X   </td></tr>
     * <tr><td>    1   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   572 </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>       </td></tr>
     * <tr><td>    1   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   573 </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>   X   </td></tr>
     * <tr><td>    1   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   574 </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>       </td></tr>
     * <tr><td>    1   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   575 </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>   X   </td></tr>
     * <tr><td>    1   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   576 </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>       </td></tr>
     * <tr><td>    1   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   577 </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>   X   </td></tr>
     * <tr><td>    1   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   578 </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>       </td></tr>
     * <tr><td>    1   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   579 </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>   X   </td></tr>
     * <tr><td>    1   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   580 </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>       </td></tr>
     * <tr><td>    1   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   581 </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>   X   </td></tr>
     * <tr><td>    1   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   582 </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>       </td></tr>
     * <tr><td>    1   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   583 </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>   X   </td></tr>
     * <tr><td>    1   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   584 </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>       </td></tr>
     * <tr><td>    1   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   585 </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>   X   </td></tr>
     * <tr><td>    1   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   586 </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>   X   </td></tr>
     * <tr><td>    1   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   587 </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>   X   </td></tr>
     * <tr><td>    1   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   588 </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>       </td></tr>
     * <tr><td>    1   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   589 </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>   X   </td></tr>
     * <tr><td>    1   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   590 </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>       </td></tr>
     * <tr><td>    1   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   591 </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>   X   </td></tr>
     * <tr><td>    1   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   592 </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>   X   </td></tr>
     * <tr><td>    1   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   593 </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>   X   </td></tr>
     * <tr><td>    1   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   594 </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>   X   </td></tr>
     * <tr><td>    1   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   595 </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>   X   </td></tr>
     * <tr><td>    1   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   596 </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>   X   </td></tr>
     * <tr><td>    1   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   597 </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>   X   </td></tr>
     * <tr><td>    1   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   598 </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>   X   </td></tr>
     * <tr><td>    1   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   599 </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>   X   </td></tr>
     * <tr><td>    1   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   600 </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>   X   </td></tr>
     * <tr><td>    1   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   601 </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>   X   </td></tr>
     * <tr><td>    1   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   602 </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>   X   </td></tr>
     * <tr><td>    1   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   603 </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>   X   </td></tr>
     * <tr><td>    1   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   604 </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>       </td></tr>
     * <tr><td>    1   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   605 </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>   X   </td></tr>
     * <tr><td>    1   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   606 </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>       </td></tr>
     * <tr><td>    1   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   607 </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>   X   </td></tr>
     * <tr><td>    1   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   608 </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>       </td></tr>
     * <tr><td>    1   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   609 </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>   X   </td></tr>
     * <tr><td>    1   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   610 </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>       </td></tr>
     * <tr><td>    1   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   611 </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>   X   </td></tr>
     * <tr><td>    1   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   612 </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>   X   </td></tr>
     * <tr><td>    1   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   613 </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>   X   </td></tr>
     * <tr><td>    1   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   614 </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>       </td></tr>
     * <tr><td>    1   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   615 </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>   X   </td></tr>
     * <tr><td>    1   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   616 </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>       </td></tr>
     * <tr><td>    1   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   617 </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>   X   </td></tr>
     * <tr><td>    1   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   618 </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>       </td></tr>
     * <tr><td>    1   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   619 </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>   X   </td></tr>
     * <tr><td>    1   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   620 </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>   X   </td></tr>
     * <tr><td>    1   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   621 </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>   X   </td></tr>
     * <tr><td>    1   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   622 </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>       </td></tr>
     * <tr><td>    1   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   623 </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>   X   </td></tr>
     * <tr><td>    1   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   624 </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>       </td></tr>
     * <tr><td>    1   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   625 </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>   X   </td></tr>
     * <tr><td>    1   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   626 </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>       </td></tr>
     * <tr><td>    1   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   627 </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>   X   </td></tr>
     * <tr><td>    1   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   628 </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>       </td></tr>
     * <tr><td>    1   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   629 </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>   X   </td></tr>
     * <tr><td>    1   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   630 </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>       </td></tr>
     * <tr><td>    1   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   631 </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>   X   </td></tr>
     * <tr><td>    1   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   632 </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>       </td></tr>
     * <tr><td>    1   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   633 </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>   X   </td></tr>
     * <tr><td>    1   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   634 </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>       </td></tr>
     * <tr><td>    1   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   635 </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>   X   </td></tr>
     * <tr><td>    1   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   636 </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>       </td></tr>
     * <tr><td>    1   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   637 </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>   X   </td></tr>
     * <tr><td>    1   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   638 </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>       </td></tr>
     * <tr><td>    1   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   639 </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>   X   </td></tr>
     * <tr><td>    1   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   640 </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>       </td></tr>
     * <tr><td>    1   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   641 </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>       </td></tr>
     * <tr><td>    1   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   642 </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>   X   </td></tr>
     * <tr><td>    1   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   643 </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>   X   </td></tr>
     * <tr><td>    1   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   644 </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>       </td></tr>
     * <tr><td>    1   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   645 </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>       </td></tr>
     * <tr><td>    1   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   646 </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>       </td></tr>
     * <tr><td>    1   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   647 </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>       </td></tr>
     * <tr><td>    1   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   648 </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>       </td></tr>
     * <tr><td>    1   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   649 </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>       </td></tr>
     * <tr><td>    1   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   650 </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>   X   </td></tr>
     * <tr><td>    1   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   651 </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>   X   </td></tr>
     * <tr><td>    1   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   652 </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>       </td></tr>
     * <tr><td>    1   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   653 </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>       </td></tr>
     * <tr><td>    1   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   654 </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>       </td></tr>
     * <tr><td>    1   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   655 </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>       </td></tr>
     * <tr><td>    1   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   656 </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>   X   </td></tr>
     * <tr><td>    1   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   657 </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>   X   </td></tr>
     * <tr><td>    1   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   658 </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>   X   </td></tr>
     * <tr><td>    1   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   659 </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>   X   </td></tr>
     * <tr><td>    1   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   660 </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>   X   </td></tr>
     * <tr><td>    1   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   661 </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>   X   </td></tr>
     * <tr><td>    1   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   662 </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>   X   </td></tr>
     * <tr><td>    1   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   663 </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>   X   </td></tr>
     * <tr><td>    1   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   664 </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>   X   </td></tr>
     * <tr><td>    1   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   665 </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>   X   </td></tr>
     * <tr><td>    1   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   666 </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>   X   </td></tr>
     * <tr><td>    1   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   667 </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>   X   </td></tr>
     * <tr><td>    1   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   668 </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>       </td></tr>
     * <tr><td>    1   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   669 </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>       </td></tr>
     * <tr><td>    1   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   670 </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>       </td></tr>
     * <tr><td>    1   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   671 </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>       </td></tr>
     * <tr><td>    1   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   672 </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>       </td></tr>
     * <tr><td>    1   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   673 </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>       </td></tr>
     * <tr><td>    1   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   674 </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>       </td></tr>
     * <tr><td>    1   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   675 </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>       </td></tr>
     * <tr><td>    1   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   676 </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>   X   </td></tr>
     * <tr><td>    1   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   677 </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>   X   </td></tr>
     * <tr><td>    1   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   678 </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>       </td></tr>
     * <tr><td>    1   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   679 </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>       </td></tr>
     * <tr><td>    1   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   680 </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>       </td></tr>
     * <tr><td>    1   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   681 </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>       </td></tr>
     * <tr><td>    1   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   682 </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>       </td></tr>
     * <tr><td>    1   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   683 </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>       </td></tr>
     * <tr><td>    1   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   684 </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>   X   </td></tr>
     * <tr><td>    1   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   685 </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>   X   </td></tr>
     * <tr><td>    1   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   686 </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>       </td></tr>
     * <tr><td>    1   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   687 </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>       </td></tr>
     * <tr><td>    1   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   688 </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>       </td></tr>
     * <tr><td>    1   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   689 </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>       </td></tr>
     * <tr><td>    1   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   690 </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>       </td></tr>
     * <tr><td>    1   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   691 </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>       </td></tr>
     * <tr><td>    1   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   692 </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>       </td></tr>
     * <tr><td>    1   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   693 </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>       </td></tr>
     * <tr><td>    1   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   694 </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>       </td></tr>
     * <tr><td>    1   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   695 </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>       </td></tr>
     * <tr><td>    1   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   696 </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>       </td></tr>
     * <tr><td>    1   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   697 </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>       </td></tr>
     * <tr><td>    1   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   698 </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>       </td></tr>
     * <tr><td>    1   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   699 </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>       </td></tr>
     * <tr><td>    1   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   700 </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>       </td></tr>
     * <tr><td>    1   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   701 </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>       </td></tr>
     * <tr><td>    1   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   702 </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>       </td></tr>
     * <tr><td>    1   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   703 </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>       </td></tr>
     * <tr><td>    1   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   704 </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>       </td></tr>
     * <tr><td>    1   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   705 </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>       </td></tr>
     * <tr><td>    1   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   706 </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>   X   </td></tr>
     * <tr><td>    1   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   707 </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>   X   </td></tr>
     * <tr><td>    1   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   708 </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>       </td></tr>
     * <tr><td>    1   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   709 </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>       </td></tr>
     * <tr><td>    1   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   710 </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>       </td></tr>
     * <tr><td>    1   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   711 </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>       </td></tr>
     * <tr><td>    1   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   712 </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>       </td></tr>
     * <tr><td>    1   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   713 </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>       </td></tr>
     * <tr><td>    1   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   714 </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>   X   </td></tr>
     * <tr><td>    1   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   715 </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>   X   </td></tr>
     * <tr><td>    1   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   716 </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>       </td></tr>
     * <tr><td>    1   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   717 </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>       </td></tr>
     * <tr><td>    1   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   718 </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>       </td></tr>
     * <tr><td>    1   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   719 </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>       </td></tr>
     * <tr><td>    1   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   720 </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>   X   </td></tr>
     * <tr><td>    1   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   721 </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>   X   </td></tr>
     * <tr><td>    1   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   722 </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>   X   </td></tr>
     * <tr><td>    1   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   723 </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>   X   </td></tr>
     * <tr><td>    1   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   724 </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>   X   </td></tr>
     * <tr><td>    1   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   725 </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>   X   </td></tr>
     * <tr><td>    1   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   726 </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>   X   </td></tr>
     * <tr><td>    1   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   727 </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>   X   </td></tr>
     * <tr><td>    1   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   728 </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>   X   </td></tr>
     * <tr><td>    1   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   729 </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>   X   </td></tr>
     * <tr><td>    1   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   730 </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>   X   </td></tr>
     * <tr><td>    1   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   731 </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>   X   </td></tr>
     * <tr><td>    1   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   732 </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>       </td></tr>
     * <tr><td>    1   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   733 </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>       </td></tr>
     * <tr><td>    1   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   734 </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>       </td></tr>
     * <tr><td>    1   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   735 </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>       </td></tr>
     * <tr><td>    1   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   736 </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>       </td></tr>
     * <tr><td>    1   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   737 </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>       </td></tr>
     * <tr><td>    1   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   738 </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>       </td></tr>
     * <tr><td>    1   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   739 </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>       </td></tr>
     * <tr><td>    1   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   740 </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>   X   </td></tr>
     * <tr><td>    1   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   741 </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>   X   </td></tr>
     * <tr><td>    1   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   742 </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>       </td></tr>
     * <tr><td>    1   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   743 </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>       </td></tr>
     * <tr><td>    1   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   744 </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>       </td></tr>
     * <tr><td>    1   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   745 </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>       </td></tr>
     * <tr><td>    1   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   746 </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>       </td></tr>
     * <tr><td>    1   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   747 </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>       </td></tr>
     * <tr><td>    1   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   748 </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>   X   </td></tr>
     * <tr><td>    1   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   749 </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>   X   </td></tr>
     * <tr><td>    1   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   750 </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>       </td></tr>
     * <tr><td>    1   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   751 </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>       </td></tr>
     * <tr><td>    1   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   752 </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>       </td></tr>
     * <tr><td>    1   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   753 </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>       </td></tr>
     * <tr><td>    1   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   754 </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>       </td></tr>
     * <tr><td>    1   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   755 </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>       </td></tr>
     * <tr><td>    1   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   756 </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>       </td></tr>
     * <tr><td>    1   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   757 </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>       </td></tr>
     * <tr><td>    1   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   758 </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>       </td></tr>
     * <tr><td>    1   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   759 </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>       </td></tr>
     * <tr><td>    1   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   760 </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>       </td></tr>
     * <tr><td>    1   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   761 </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>       </td></tr>
     * <tr><td>    1   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   762 </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>       </td></tr>
     * <tr><td>    1   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   763 </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>       </td></tr>
     * <tr><td>    1   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   764 </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>       </td></tr>
     * <tr><td>    1   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   765 </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>       </td></tr>
     * <tr><td>    1   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   766 </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>       </td></tr>
     * <tr><td>    1   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   767 </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>       </td></tr>
     * <tr><td>    1   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   768 </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>       </td></tr>
     * <tr><td>    1   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   769 </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>   X   </td></tr>
     * <tr><td>    1   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   770 </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>   X   </td></tr>
     * <tr><td>    1   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   771 </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>   X   </td></tr>
     * <tr><td>    1   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   772 </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>       </td></tr>
     * <tr><td>    1   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   773 </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>   X   </td></tr>
     * <tr><td>    1   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   774 </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>       </td></tr>
     * <tr><td>    1   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   775 </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>   X   </td></tr>
     * <tr><td>    1   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   776 </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>       </td></tr>
     * <tr><td>    1   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   777 </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>   X   </td></tr>
     * <tr><td>    1   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   778 </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>   X   </td></tr>
     * <tr><td>    1   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   779 </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>   X   </td></tr>
     * <tr><td>    1   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   780 </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>       </td></tr>
     * <tr><td>    1   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   781 </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>   X   </td></tr>
     * <tr><td>    1   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   782 </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>       </td></tr>
     * <tr><td>    1   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   783 </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>   X   </td></tr>
     * <tr><td>    1   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   784 </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>   X   </td></tr>
     * <tr><td>    1   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   785 </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>   X   </td></tr>
     * <tr><td>    1   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   786 </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>   X   </td></tr>
     * <tr><td>    1   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   787 </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>   X   </td></tr>
     * <tr><td>    1   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   788 </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>   X   </td></tr>
     * <tr><td>    1   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   789 </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>   X   </td></tr>
     * <tr><td>    1   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   790 </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>   X   </td></tr>
     * <tr><td>    1   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   791 </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>   X   </td></tr>
     * <tr><td>    1   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   792 </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>   X   </td></tr>
     * <tr><td>    1   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   793 </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>   X   </td></tr>
     * <tr><td>    1   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   794 </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>   X   </td></tr>
     * <tr><td>    1   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   795 </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>   X   </td></tr>
     * <tr><td>    1   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   796 </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>       </td></tr>
     * <tr><td>    1   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   797 </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>   X   </td></tr>
     * <tr><td>    1   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   798 </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>       </td></tr>
     * <tr><td>    1   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   799 </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>   X   </td></tr>
     * <tr><td>    1   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   800 </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>       </td></tr>
     * <tr><td>    1   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   801 </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>   X   </td></tr>
     * <tr><td>    1   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   802 </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>       </td></tr>
     * <tr><td>    1   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   803 </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>   X   </td></tr>
     * <tr><td>    1   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   804 </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>   X   </td></tr>
     * <tr><td>    1   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   805 </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>   X   </td></tr>
     * <tr><td>    1   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   806 </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>       </td></tr>
     * <tr><td>    1   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   807 </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>   X   </td></tr>
     * <tr><td>    1   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   808 </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>       </td></tr>
     * <tr><td>    1   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   809 </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>   X   </td></tr>
     * <tr><td>    1   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   810 </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>       </td></tr>
     * <tr><td>    1   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   811 </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>   X   </td></tr>
     * <tr><td>    1   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   812 </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>   X   </td></tr>
     * <tr><td>    1   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   813 </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>   X   </td></tr>
     * <tr><td>    1   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   814 </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>       </td></tr>
     * <tr><td>    1   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   815 </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>   X   </td></tr>
     * <tr><td>    1   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   816 </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>       </td></tr>
     * <tr><td>    1   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   817 </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>   X   </td></tr>
     * <tr><td>    1   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   818 </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>       </td></tr>
     * <tr><td>    1   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   819 </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>   X   </td></tr>
     * <tr><td>    1   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   820 </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>       </td></tr>
     * <tr><td>    1   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   821 </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>   X   </td></tr>
     * <tr><td>    1   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   822 </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>       </td></tr>
     * <tr><td>    1   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   823 </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>   X   </td></tr>
     * <tr><td>    1   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   824 </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>       </td></tr>
     * <tr><td>    1   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   825 </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>   X   </td></tr>
     * <tr><td>    1   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   826 </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>       </td></tr>
     * <tr><td>    1   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   827 </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>   X   </td></tr>
     * <tr><td>    1   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   828 </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>       </td></tr>
     * <tr><td>    1   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   829 </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>   X   </td></tr>
     * <tr><td>    1   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   830 </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>       </td></tr>
     * <tr><td>    1   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   831 </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>   X   </td></tr>
     * <tr><td>    1   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   832 </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>       </td></tr>
     * <tr><td>    1   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   833 </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>   X   </td></tr>
     * <tr><td>    1   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   834 </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>   X   </td></tr>
     * <tr><td>    1   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   835 </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>   X   </td></tr>
     * <tr><td>    1   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   836 </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>       </td></tr>
     * <tr><td>    1   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   837 </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>   X   </td></tr>
     * <tr><td>    1   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   838 </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>       </td></tr>
     * <tr><td>    1   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   839 </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>   X   </td></tr>
     * <tr><td>    1   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   840 </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>       </td></tr>
     * <tr><td>    1   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   841 </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>   X   </td></tr>
     * <tr><td>    1   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   842 </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>   X   </td></tr>
     * <tr><td>    1   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   843 </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>   X   </td></tr>
     * <tr><td>    1   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   844 </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>       </td></tr>
     * <tr><td>    1   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   845 </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>   X   </td></tr>
     * <tr><td>    1   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   846 </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>       </td></tr>
     * <tr><td>    1   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   847 </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>   X   </td></tr>
     * <tr><td>    1   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   848 </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>   X   </td></tr>
     * <tr><td>    1   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   849 </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>   X   </td></tr>
     * <tr><td>    1   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   850 </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>   X   </td></tr>
     * <tr><td>    1   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   851 </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>   X   </td></tr>
     * <tr><td>    1   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   852 </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>   X   </td></tr>
     * <tr><td>    1   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   853 </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>   X   </td></tr>
     * <tr><td>    1   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   854 </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>   X   </td></tr>
     * <tr><td>    1   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   855 </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>   X   </td></tr>
     * <tr><td>    1   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   856 </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>   X   </td></tr>
     * <tr><td>    1   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   857 </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>   X   </td></tr>
     * <tr><td>    1   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   858 </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>   X   </td></tr>
     * <tr><td>    1   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   859 </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>   X   </td></tr>
     * <tr><td>    1   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   860 </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>       </td></tr>
     * <tr><td>    1   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   861 </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>   X   </td></tr>
     * <tr><td>    1   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   862 </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>       </td></tr>
     * <tr><td>    1   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   863 </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>   X   </td></tr>
     * <tr><td>    1   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   864 </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>       </td></tr>
     * <tr><td>    1   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   865 </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>   X   </td></tr>
     * <tr><td>    1   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   866 </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>       </td></tr>
     * <tr><td>    1   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   867 </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>   X   </td></tr>
     * <tr><td>    1   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   868 </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>   X   </td></tr>
     * <tr><td>    1   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   869 </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>   X   </td></tr>
     * <tr><td>    1   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   870 </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>       </td></tr>
     * <tr><td>    1   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   871 </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>   X   </td></tr>
     * <tr><td>    1   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   872 </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>       </td></tr>
     * <tr><td>    1   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   873 </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>   X   </td></tr>
     * <tr><td>    1   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   874 </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>       </td></tr>
     * <tr><td>    1   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   875 </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>   X   </td></tr>
     * <tr><td>    1   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   876 </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>   X   </td></tr>
     * <tr><td>    1   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   877 </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>   X   </td></tr>
     * <tr><td>    1   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   878 </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>       </td></tr>
     * <tr><td>    1   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   879 </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>   X   </td></tr>
     * <tr><td>    1   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   880 </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>       </td></tr>
     * <tr><td>    1   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   881 </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>   X   </td></tr>
     * <tr><td>    1   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   882 </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>       </td></tr>
     * <tr><td>    1   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   883 </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>   X   </td></tr>
     * <tr><td>    1   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   884 </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>       </td></tr>
     * <tr><td>    1   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   885 </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>   X   </td></tr>
     * <tr><td>    1   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   886 </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>       </td></tr>
     * <tr><td>    1   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   887 </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>   X   </td></tr>
     * <tr><td>    1   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   888 </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>       </td></tr>
     * <tr><td>    1   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   889 </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>   X   </td></tr>
     * <tr><td>    1   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   890 </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>       </td></tr>
     * <tr><td>    1   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   891 </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>   X   </td></tr>
     * <tr><td>    1   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   892 </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>       </td></tr>
     * <tr><td>    1   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   893 </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>   X   </td></tr>
     * <tr><td>    1   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   894 </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>       </td></tr>
     * <tr><td>    1   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   895 </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>   X   </td></tr>
     * <tr><td>    1   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   896 </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>       </td></tr>
     * <tr><td>    1   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   897 </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>       </td></tr>
     * <tr><td>    1   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   898 </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>   X   </td></tr>
     * <tr><td>    1   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   899 </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>   X   </td></tr>
     * <tr><td>    1   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   900 </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>       </td></tr>
     * <tr><td>    1   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   901 </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>       </td></tr>
     * <tr><td>    1   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   902 </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>       </td></tr>
     * <tr><td>    1   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   903 </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>       </td></tr>
     * <tr><td>    1   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   904 </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>       </td></tr>
     * <tr><td>    1   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   905 </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>       </td></tr>
     * <tr><td>    1   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   906 </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>   X   </td></tr>
     * <tr><td>    1   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   907 </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>   X   </td></tr>
     * <tr><td>    1   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   908 </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>       </td></tr>
     * <tr><td>    1   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   909 </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>       </td></tr>
     * <tr><td>    1   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   910 </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>       </td></tr>
     * <tr><td>    1   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   911 </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>       </td></tr>
     * <tr><td>    1   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   912 </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>   X   </td></tr>
     * <tr><td>    1   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   913 </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>   X   </td></tr>
     * <tr><td>    1   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   914 </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>   X   </td></tr>
     * <tr><td>    1   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   915 </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>   X   </td></tr>
     * <tr><td>    1   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   916 </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>   X   </td></tr>
     * <tr><td>    1   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   917 </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>   X   </td></tr>
     * <tr><td>    1   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   918 </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>   X   </td></tr>
     * <tr><td>    1   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   919 </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>   X   </td></tr>
     * <tr><td>    1   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   920 </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>   X   </td></tr>
     * <tr><td>    1   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   921 </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>   X   </td></tr>
     * <tr><td>    1   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   922 </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>   X   </td></tr>
     * <tr><td>    1   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   923 </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>   X   </td></tr>
     * <tr><td>    1   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   924 </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>       </td></tr>
     * <tr><td>    1   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   925 </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>       </td></tr>
     * <tr><td>    1   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   926 </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>       </td></tr>
     * <tr><td>    1   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   927 </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>       </td></tr>
     * <tr><td>    1   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   928 </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>       </td></tr>
     * <tr><td>    1   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   929 </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>       </td></tr>
     * <tr><td>    1   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   930 </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>       </td></tr>
     * <tr><td>    1   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   931 </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>       </td></tr>
     * <tr><td>    1   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   932 </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>   X   </td></tr>
     * <tr><td>    1   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   933 </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>   X   </td></tr>
     * <tr><td>    1   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   934 </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>       </td></tr>
     * <tr><td>    1   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   935 </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>       </td></tr>
     * <tr><td>    1   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   936 </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>       </td></tr>
     * <tr><td>    1   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   937 </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>       </td></tr>
     * <tr><td>    1   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   938 </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>       </td></tr>
     * <tr><td>    1   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   939 </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>       </td></tr>
     * <tr><td>    1   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   940 </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>   X   </td></tr>
     * <tr><td>    1   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   941 </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>   X   </td></tr>
     * <tr><td>    1   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   942 </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>       </td></tr>
     * <tr><td>    1   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   943 </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>       </td></tr>
     * <tr><td>    1   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   944 </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>       </td></tr>
     * <tr><td>    1   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   945 </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>       </td></tr>
     * <tr><td>    1   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   946 </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>       </td></tr>
     * <tr><td>    1   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   947 </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>       </td></tr>
     * <tr><td>    1   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   948 </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>       </td></tr>
     * <tr><td>    1   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   949 </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>       </td></tr>
     * <tr><td>    1   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   950 </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>       </td></tr>
     * <tr><td>    1   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   951 </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>       </td></tr>
     * <tr><td>    1   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   952 </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>       </td></tr>
     * <tr><td>    1   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   953 </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>       </td></tr>
     * <tr><td>    1   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   954 </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>       </td></tr>
     * <tr><td>    1   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   955 </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>       </td></tr>
     * <tr><td>    1   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   956 </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>       </td></tr>
     * <tr><td>    1   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   957 </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>       </td></tr>
     * <tr><td>    1   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   958 </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>       </td></tr>
     * <tr><td>    1   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   959 </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>       </td></tr>
     * <tr><td>    1   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   960 </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>       </td></tr>
     * <tr><td>    1   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   961 </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>       </td></tr>
     * <tr><td>    1   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   962 </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>   X   </td></tr>
     * <tr><td>    1   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   963 </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>   X   </td></tr>
     * <tr><td>    1   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   964 </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>       </td></tr>
     * <tr><td>    1   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   965 </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>       </td></tr>
     * <tr><td>    1   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   966 </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>       </td></tr>
     * <tr><td>    1   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   967 </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>       </td></tr>
     * <tr><td>    1   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   968 </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>       </td></tr>
     * <tr><td>    1   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   969 </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>       </td></tr>
     * <tr><td>    1   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   970 </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>   X   </td></tr>
     * <tr><td>    1   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   971 </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>   X   </td></tr>
     * <tr><td>    1   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   972 </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>       </td></tr>
     * <tr><td>    1   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   973 </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>       </td></tr>
     * <tr><td>    1   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   974 </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>       </td></tr>
     * <tr><td>    1   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   975 </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>       </td></tr>
     * <tr><td>    1   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   976 </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>   X   </td></tr>
     * <tr><td>    1   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   977 </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>   X   </td></tr>
     * <tr><td>    1   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   978 </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>   X   </td></tr>
     * <tr><td>    1   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   979 </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>   X   </td></tr>
     * <tr><td>    1   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   980 </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>   X   </td></tr>
     * <tr><td>    1   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   981 </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>   X   </td></tr>
     * <tr><td>    1   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   982 </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>   X   </td></tr>
     * <tr><td>    1   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   983 </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>   X   </td></tr>
     * <tr><td>    1   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   984 </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>   X   </td></tr>
     * <tr><td>    1   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   985 </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>   X   </td></tr>
     * <tr><td>    1   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   986 </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>   X   </td></tr>
     * <tr><td>    1   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   987 </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>   X   </td></tr>
     * <tr><td>    1   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   988 </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>       </td></tr>
     * <tr><td>    1   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   989 </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>       </td></tr>
     * <tr><td>    1   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   990 </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>       </td></tr>
     * <tr><td>    1   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   991 </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>       </td></tr>
     * <tr><td>    1   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   992 </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>       </td></tr>
     * <tr><td>    1   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   993 </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>       </td></tr>
     * <tr><td>    1   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   994 </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>       </td></tr>
     * <tr><td>    1   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   995 </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>       </td></tr>
     * <tr><td>    1   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   996 </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>   X   </td></tr>
     * <tr><td>    1   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   997 </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>   X   </td></tr>
     * <tr><td>    1   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   998 </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>       </td></tr>
     * <tr><td>    1   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   999 </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>       </td></tr>
     * <tr><td>    1   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   1000    </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>       </td></tr>
     * <tr><td>    1   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   1001    </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>       </td></tr>
     * <tr><td>    1   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   1002    </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>       </td></tr>
     * <tr><td>    1   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   1003    </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>       </td></tr>
     * <tr><td>    1   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   1004    </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>   X   </td></tr>
     * <tr><td>    1   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   1005    </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>   X   </td></tr>
     * <tr><td>    1   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   1006    </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>       </td></tr>
     * <tr><td>    1   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   1007    </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>       </td></tr>
     * <tr><td>    1   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   1008    </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>       </td></tr>
     * <tr><td>    1   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   1009    </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>       </td></tr>
     * <tr><td>    1   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   1010    </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>       </td></tr>
     * <tr><td>    1   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   1011    </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>       </td></tr>
     * <tr><td>    1   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   1012    </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>       </td></tr>
     * <tr><td>    1   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   1013    </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>       </td></tr>
     * <tr><td>    1   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   1014    </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>       </td></tr>
     * <tr><td>    1   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   1015    </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>       </td></tr>
     * <tr><td>    1   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   0   </td><td>   1016    </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>       </td></tr>
     * <tr><td>    1   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   1   </td><td>   1017    </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>       </td></tr>
     * <tr><td>    1   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   0   </td><td>   1018    </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>       </td></tr>
     * <tr><td>    1   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   1   </td><td>   1019    </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>       </td></tr>
     * <tr><td>    1   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   0   </td><td>   1020    </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>       </td></tr>
     * <tr><td>    1   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   1   </td><td>   1021    </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>       </td></tr>
     * <tr><td>    1   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   0   </td><td>   1022    </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>       </td></tr>
     * <tr><td>    1   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   1   </td><td>   1023    </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>       </td><td>   X   </td><td>       </td></tr>
     * </table>                                                                                                                                                                                         
     * @param prop current IPCProperty
     * @param bean current CharacteristicUIBean
     * @param csticStatusChange Key of the characteristic for which the expand/collapse status should be changed
     * @param uiContext
     * @return uiType 
     */    
    public static String determineUIType(IPCProperty prop, CharacteristicUIBean bean, String csticStatusChange, UIContext uiContext) {
        boolean isExpandedCfg = bean.isExpanded(); // expand state from cfg-model
        // was the previous uiType (e.g. from UIModel or "Display Options" link) expanded?
        boolean isExpanded = prop.determineExpandState(isExpandedCfg, csticStatusChange, bean, uiContext);
        int uiTypeCase = calculateUiTypeCase(isExpanded, bean);
        String uiType = "";
        switch (uiTypeCase) {
            case 96:
            case 97:
            case 98:
            case 99:
            case 102:
            case 103:
            case 224:
            case 225:
            case 226:
            case 227:
            case 230:
            case 231:
            case 352:
            case 353:
            case 354:
            case 355:
            case 358:
            case 359:
            case 480:
            case 481:
            case 482:
            case 483:
            case 486:
            case 487:
                // UiType 1: Radio buttons only
                uiType = UITypes.RADIO_BUTTONS;
                break;
            case 66:
            case 67:
            case 70:
            case 71:
            case 80:
            case 81:
            case 82:
            case 83:
            case 84:
            case 85:
            case 86:
            case 87:
            case 100:
            case 101:
            case 114:
            case 118:
            case 119:
            case 145:
            case 147:
            case 149:
            case 151:
            case 183:
            case 194:
            case 195:
            case 198:
            case 199:
            case 208:
            case 209:
            case 210:
            case 211:
            case 212:
            case 213:
            case 214:
            case 215:
            case 228:
            case 229:
            case 242:
            case 246:
            case 247:
            case 322:
            case 323:
            case 326:
            case 327:
            case 336:
            case 337:
            case 338:
            case 339:
            case 340:
            case 341:
            case 342:
            case 343:
            case 356:
            case 357:
            case 370:
            case 374:
            case 375:
            case 401:
            case 403:
            case 405:
            case 407:
            case 439:
            case 450:
            case 451:
            case 454:
            case 455:
            case 464:
            case 465:
            case 466:
            case 467:
            case 468:
            case 469:
            case 470:
            case 471:
            case 484:
            case 485:
            case 498:
            case 502:
            case 503:
                // UiType 2: Radion buttons and input field
                uiType = UITypes.IPC_RADIO_BUTTONS_INPUT;
                break;
            case 104:
            case 105:
            case 106:
            case 107:
            case 110:
            case 111:
            case 232:
            case 233:
            case 234:
            case 235:
            case 238:
            case 239:
            case 360:
            case 361:
            case 362:
            case 363:
            case 366:
            case 367:
            case 488:
            case 489:
            case 490:
            case 491:
            case 494:
            case 495:
                // UiType 3: Checkboxes only
                uiType = UITypes.CHECKBOX;
                break;
            case 73:
            case 74:
            case 75:
            case 77:
            case 78:
            case 79:
            case 88:
            case 89:
            case 90:
            case 91:
            case 108:
            case 109:
            case 121:
            case 122:
            case 123:
            case 125:
            case 126:
            case 127:
            case 153:
            case 155:
            case 185:
            case 187:
            case 189:
            case 191:
            case 201:
            case 202:
            case 203:
            case 205:
            case 206:
            case 207:
            case 216:
            case 217:
            case 218:
            case 219:
            case 236:
            case 237:
            case 249:
            case 250:
            case 251:
            case 253:
            case 254:
            case 255:
            case 329:
            case 330:
            case 331:
            case 333:
            case 334:
            case 335:
            case 344:
            case 345:
            case 346:
            case 347:
            case 364:
            case 365:
            case 377:
            case 378:
            case 379:
            case 381:
            case 382:
            case 383:
            case 409:
            case 411:
            case 441:
            case 443:
            case 445:
            case 447:
            case 457:
            case 458:
            case 459:
            case 461:
            case 462:
            case 463:
            case 472:
            case 473:
            case 474:
            case 475:
            case 492:
            case 493:
            case 505:
            case 506:
            case 507:
            case 509:
            case 510:
            case 511:
                // UiType 4: Checkboxes and input field
                uiType = UITypes.IPC_CHECKBOXES_INPUT;
                break;
            case 32:
            case 33:
            case 34:
            case 35:
            case 38:
            case 39:
            case 160:
            case 161:
            case 162:
            case 163:
            case 166:
            case 167:
            case 288:
            case 289:
            case 290:
            case 291:
            case 294:
            case 295:
            case 416:
            case 417:
            case 418:
            case 419:
            case 422:
            case 423:
                // UiType 5: DDLB only
                uiType = UITypes.SINGLE_DDLB;
                break;
            case 2:
            case 3:
            case 6:
            case 7:
            case 16:
            case 17:
            case 18:
            case 19:
            case 20:
            case 21:
            case 22:
            case 23:
            case 36:
            case 37:
            case 50:
            case 54:
            case 55:
            case 130:
            case 131:
            case 134:
            case 135:
            case 164:
            case 165:
            case 258:
            case 259:
            case 262:
            case 263:
            case 272:
            case 273:
            case 274:
            case 275:
            case 276:
            case 277:
            case 278:
            case 279:
            case 292:
            case 293:
            case 306:
            case 310:
            case 311:
            case 386:
            case 387:
            case 390:
            case 391:
            case 420:
            case 421:
                // UiType 6: DDLB and input field
                uiType = UITypes.IPC_SINGLE_DDLB_INPUT; 
                break;                
            case 40:
            case 41:
            case 42:
            case 43:
            case 46:
            case 47:
            case 168:
            case 169:
            case 170:
            case 171:
            case 174:
            case 175:
            case 296:
            case 297:
            case 298:
            case 299:
            case 302:
            case 303:
            case 424:
            case 425:
            case 426:
            case 427:
            case 430:
            case 431:
                // UiType 7: Multi-DDLB only
                uiType = UITypes.MULTI_DDLB; 
                break;                
            case 9:
            case 10:
            case 11:
            case 13:
            case 14:
            case 15:
            case 24:
            case 25:
            case 26:
            case 27:
            case 44:
            case 45:
            case 57:
            case 58:
            case 59:
            case 61:
            case 62:
            case 63:
            case 137:
            case 138:
            case 139:
            case 141:
            case 142:
            case 143:
            case 172:
            case 173:
            case 265:
            case 266:
            case 267:
            case 269:
            case 270:
            case 271:
            case 280:
            case 281:
            case 282:
            case 283:
            case 300:
            case 301:
            case 313:
            case 314:
            case 315:
            case 317:
            case 318:
            case 319:
            case 393:
            case 394:
            case 395:
            case 397:
            case 398:
            case 399:
            case 428:
            case 429:
                // UiType 8: Multi-DDLB and input field
                uiType = UITypes.IPC_MULTI_DDLB_INPUT; 
                break;                
            case 0:
            case 1:
            case 4:
            case 5:
            case 8:
            case 12:
            case 28:
            case 29:
            case 30:
            case 31:
            case 48:
            case 49:
            case 51:
            case 52:
            case 53:
            case 56:
            case 60:
            case 64:
            case 65:
            case 68:
            case 69:
            case 72:
            case 76:
            case 92:
            case 93:
            case 94:
            case 95:
            case 112:
            case 113:
            case 115:
            case 116:
            case 117:
            case 120:
            case 124:
            case 128:
            case 129:
            case 132:
            case 133:
            case 136:
            case 140:
            case 144:
            case 146:
            case 148:
            case 150:
            case 152:
            case 154:
            case 156:
            case 157:
            case 158:
            case 159:
            case 176:
            case 177:
            case 178:
            case 179:
            case 180:
            case 181:
            case 182:
            case 184:
            case 186:
            case 188:
            case 190:
            case 192:
            case 193:
            case 196:
            case 197:
            case 200:
            case 204:
            case 220:
            case 221:
            case 222:
            case 223:
            case 240:
            case 241:
            case 243:
            case 244:
            case 245:
            case 248:
            case 252:
                // UiType 9: Input field only
                uiType = UITypes.INPUT; 
                break;                
            case 256:
            case 257:
            case 260:
            case 261:
            case 264:
            case 268:
            case 284:
            case 285:
            case 286:
            case 287:
            case 304:
            case 305:
            case 307:
            case 308:
            case 309:
            case 312:
            case 316:
            case 320:
            case 321:
            case 324:
            case 325:
            case 328:
            case 332:
            case 348:
            case 349:
            case 350:
            case 351:
            case 368:
            case 369:
            case 371:
            case 372:
            case 373:
            case 376:
            case 380:
            case 384:
            case 385:
            case 388:
            case 389:
            case 392:
            case 396:
            case 400:
            case 402:
            case 404:
            case 406:
            case 408:
            case 410:
            case 412:
            case 413:
            case 414:
            case 415:
            case 432:
            case 433:
            case 434:
            case 435:
            case 436:
            case 437:
            case 438:
            case 440:
            case 442:
            case 444:
            case 446:
            case 448:
            case 449:
            case 452:
            case 453:
            case 456:
            case 460:
            case 476:
            case 477:
            case 478:
            case 479:
            case 496:
            case 497:
            case 499:
            case 500:
            case 501:
            case 504:
            case 508:
                // UiType 10: Input field with calendar control
                uiType = UITypes.DATE; 
                break;                
            case 512:
            case 513:
            case 514:
            case 515:
            case 516:
            case 517:
            case 518:
            case 519:
            case 520:
            case 521:
            case 522:
            case 523:
            case 524:
            case 525:
            case 526:
            case 527:
            case 528:
            case 529:
            case 530:
            case 531:
            case 532:
            case 533:
            case 534:
            case 535:
            case 536:
            case 537:
            case 538:
            case 539:
            case 540:
            case 541:
            case 542:
            case 543:
            case 544:
            case 545:
            case 546:
            case 547:
            case 548:
            case 549:
            case 550:
            case 551:
            case 552:
            case 553:
            case 554:
            case 555:
            case 556:
            case 557:
            case 558:
            case 559:
            case 560:
            case 561:
            case 562:
            case 563:
            case 564:
            case 565:
            case 566:
            case 567:
            case 568:
            case 569:
            case 570:
            case 571:
            case 572:
            case 573:
            case 574:
            case 575:
            case 576:
            case 577:
            case 578:
            case 579:
            case 580:
            case 581:
            case 582:
            case 583:
            case 584:
            case 585:
            case 586:
            case 587:
            case 588:
            case 589:
            case 590:
            case 591:
            case 592:
            case 593:
            case 594:
            case 595:
            case 596:
            case 597:
            case 598:
            case 599:
            case 600:
            case 601:
            case 602:
            case 603:
            case 604:
            case 605:
            case 606:
            case 607:
            case 608:
            case 609:
            case 610:
            case 611:
            case 612:
            case 613:
            case 614:
            case 615:
            case 616:
            case 617:
            case 618:
            case 619:
            case 620:
            case 621:
            case 622:
            case 623:
            case 624:
            case 625:
            case 626:
            case 627:
            case 628:
            case 629:
            case 630:
            case 631:
            case 632:
            case 633:
            case 634:
            case 635:
            case 636:
            case 637:
            case 638:
            case 639:
            case 640:
            case 641:
            case 642:
            case 643:
            case 644:
            case 645:
            case 646:
            case 647:
            case 648:
            case 649:
            case 650:
            case 651:
            case 652:
            case 653:
            case 654:
            case 655:
            case 656:
            case 657:
            case 658:
            case 659:
            case 660:
            case 661:
            case 662:
            case 663:
            case 664:
            case 665:
            case 666:
            case 667:
            case 668:
            case 669:
            case 670:
            case 671:
            case 672:
            case 673:
            case 674:
            case 675:
            case 676:
            case 677:
            case 678:
            case 679:
            case 680:
            case 681:
            case 682:
            case 683:
            case 684:
            case 685:
            case 686:
            case 687:
            case 688:
            case 689:
            case 690:
            case 691:
            case 692:
            case 693:
            case 694:
            case 695:
            case 696:
            case 697:
            case 698:
            case 699:
            case 700:
            case 701:
            case 702:
            case 703:
            case 704:
            case 705:
            case 706:
            case 707:
            case 708:
            case 709:
            case 710:
            case 711:
            case 712:
            case 713:
            case 714:
            case 715:
            case 716:
            case 717:
            case 718:
            case 719:
            case 720:
            case 721:
            case 722:
            case 723:
            case 724:
            case 725:
            case 726:
            case 727:
            case 728:
            case 729:
            case 730:
            case 731:
            case 732:
            case 733:
            case 734:
            case 735:
            case 736:
            case 737:
            case 738:
            case 739:
            case 740:
            case 741:
            case 742:
            case 743:
            case 744:
            case 745:
            case 746:
            case 747:
            case 748:
            case 749:
            case 750:
            case 751:
            case 752:
            case 753:
            case 754:
            case 755:
            case 756:
            case 757:
            case 758:
            case 759:
            case 760:
            case 761:
            case 762:
            case 763:
            case 764:
            case 765:
            case 766:
            case 767:
            case 768:
            case 769:
            case 770:
            case 771:
            case 772:
            case 773:
            case 774:
            case 775:
            case 776:
            case 777:
            case 778:
            case 779:
            case 780:
            case 781:
            case 782:
            case 783:
            case 784:
            case 785:
            case 786:
            case 787:
            case 788:
            case 789:
            case 790:
            case 791:
            case 792:
            case 793:
            case 794:
            case 795:
            case 796:
            case 797:
            case 798:
            case 799:
            case 800:
            case 801:
            case 802:
            case 803:
            case 804:
            case 805:
            case 806:
            case 807:
            case 808:
            case 809:
            case 810:
            case 811:
            case 812:
            case 813:
            case 814:
            case 815:
            case 816:
            case 817:
            case 818:
            case 819:
            case 820:
            case 821:
            case 822:
            case 823:
            case 824:
            case 825:
            case 826:
            case 827:
            case 828:
            case 829:
            case 830:
            case 831:
            case 832:
            case 833:
            case 834:
            case 835:
            case 836:
            case 837:
            case 838:
            case 839:
            case 840:
            case 841:
            case 842:
            case 843:
            case 844:
            case 845:
            case 846:
            case 847:
            case 848:
            case 849:
            case 850:
            case 851:
            case 852:
            case 853:
            case 854:
            case 855:
            case 856:
            case 857:
            case 858:
            case 859:
            case 860:
            case 861:
            case 862:
            case 863:
            case 864:
            case 865:
            case 866:
            case 867:
            case 868:
            case 869:
            case 870:
            case 871:
            case 872:
            case 873:
            case 874:
            case 875:
            case 876:
            case 877:
            case 878:
            case 879:
            case 880:
            case 881:
            case 882:
            case 883:
            case 884:
            case 885:
            case 886:
            case 887:
            case 888:
            case 889:
            case 890:
            case 891:
            case 892:
            case 893:
            case 894:
            case 895:
            case 896:
            case 897:
            case 898:
            case 899:
            case 900:
            case 901:
            case 902:
            case 903:
            case 904:
            case 905:
            case 906:
            case 907:
            case 908:
            case 909:
            case 910:
            case 911:
            case 912:
            case 913:
            case 914:
            case 915:
            case 916:
            case 917:
            case 918:
            case 919:
            case 920:
            case 921:
            case 922:
            case 923:
            case 924:
            case 925:
            case 926:
            case 927:
            case 928:
            case 929:
            case 930:
            case 931:
            case 932:
            case 933:
            case 934:
            case 935:
            case 936:
            case 937:
            case 938:
            case 939:
            case 940:
            case 941:
            case 942:
            case 943:
            case 944:
            case 945:
            case 946:
            case 947:
            case 948:
            case 949:
            case 950:
            case 951:
            case 952:
            case 953:
            case 954:
            case 955:
            case 956:
            case 957:
            case 958:
            case 959:
            case 960:
            case 961:
            case 962:
            case 963:
            case 964:
            case 965:
            case 966:
            case 967:
            case 968:
            case 969:
            case 970:
            case 971:
            case 972:
            case 973:
            case 974:
            case 975:
            case 976:
            case 977:
            case 978:
            case 979:
            case 980:
            case 981:
            case 982:
            case 983:
            case 984:
            case 985:
            case 986:
            case 987:
            case 988:
            case 989:
            case 990:
            case 991:
            case 992:
            case 993:
            case 994:
            case 995:
            case 996:
            case 997:
            case 998:
            case 999:
            case 1000:
            case 1001:
            case 1002:
            case 1003:
            case 1004:
            case 1005:
            case 1006:
            case 1007:
            case 1008:
            case 1009:
            case 1010:
            case 1011:
            case 1012:
            case 1013:
            case 1014:
            case 1015:
            case 1016:
            case 1017:
            case 1018:
            case 1019:
            case 1020:
            case 1021:
            case 1022:
            case 1023:            
                // UiType 11: Message characteristic
                uiType = UITypes.IPC_MESSAGE_CSTIC; 
                break;
            default:
                // Error: This combination of attributes should never occur.
                uiType = UITypes.IPC_SINGLE_DDLB_INPUT;
                // logging
                break;

        }
        boolean valid = checkUITypeCaseValidity(uiTypeCase);
        if (!valid){
            category.logT(Severity.WARNING, location, "uiTypeCase " + uiTypeCase + " is not valid! Please check uiType-determination.");            
        }
        category.logT(Severity.DEBUG, location, "UiType for Property " + prop.getName() + ": "+ uiType);
        return uiType;
    }
    
    /**
     * Checks the validity of a given uiTypeCase.
     * A uiTypeCase is invalid if the combination of attributes in the decision table that lead to this
     * uiTypeCase is not consistent.  
     * E.g. if hasValues=F and hasAssignedValues=T. 
     * @param uiTypeCase
     * @return false if uiTypeCase is invalid.
     */
    private static boolean checkUITypeCaseValidity(int uiTypeCase) {
        boolean valid = true;
        switch(uiTypeCase){
            case 1:
            case 2:
            case 3:
            case 5:
            case 7:
            case 9:
            case 10:
            case 11:
            case 13:
            case 15:
            case 16:
            case 17:
            case 18:
            case 19:
            case 20:
            case 21:
            case 22:
            case 23:
            case 24:
            case 25:
            case 26:
            case 27:
            case 29:
            case 31:
            case 33:
            case 35:
            case 36:
            case 37:
            case 39:
            case 41:
            case 43:
            case 44:
            case 45:
            case 47:
            case 49:
            case 51:
            case 53:
            case 55:
            case 57:
            case 59:
            case 61:
            case 63:
            case 65:
            case 67:
            case 69:
            case 71:
            case 73:
            case 74:
            case 75:
            case 77:
            case 79:
            case 80:
            case 81:
            case 82:
            case 83:
            case 84:
            case 85:
            case 86:
            case 87:
            case 88:
            case 89:
            case 90:
            case 91:
            case 93:
            case 95:
            case 97:
            case 99:
            case 100:
            case 101:
            case 103:
            case 105:
            case 107:
            case 108:
            case 109:
            case 111:
            case 113:
            case 115:
            case 117:
            case 119:
            case 121:
            case 123:
            case 125:
            case 127:
            case 130:
            case 131:
            case 138:
            case 139:
            case 144:
            case 145:
            case 146:
            case 147:
            case 148:
            case 149:
            case 150:
            case 151:
            case 152:
            case 153:
            case 154:
            case 155:
            case 164:
            case 165:
            case 172:
            case 173:
            case 194:
            case 195:
            case 202:
            case 203:
            case 208:
            case 209:
            case 210:
            case 211:
            case 212:
            case 213:
            case 214:
            case 215:
            case 216:
            case 217:
            case 218:
            case 219:
            case 228:
            case 229:
            case 236:
            case 237:
            case 257:
            case 258:
            case 259:
            case 261:
            case 263:
            case 265:
            case 266:
            case 267:
            case 269:
            case 271:
            case 272:
            case 273:
            case 274:
            case 275:
            case 276:
            case 277:
            case 278:
            case 279:
            case 280:
            case 281:
            case 282:
            case 283:
            case 285:
            case 287:
            case 289:
            case 291:
            case 292:
            case 293:
            case 295:
            case 297:
            case 299:
            case 300:
            case 301:
            case 303:
            case 305:
            case 307:
            case 309:
            case 311:
            case 313:
            case 315:
            case 317:
            case 319:
            case 321:
            case 322:
            case 323:
            case 325:
            case 327:
            case 329:
            case 330:
            case 331:
            case 333:
            case 335:
            case 336:
            case 337:
            case 338:
            case 339:
            case 340:
            case 341:
            case 342:
            case 343:
            case 344:
            case 345:
            case 346:
            case 347:
            case 349:
            case 351:
            case 353:
            case 355:
            case 356:
            case 357:
            case 359:
            case 361:
            case 363:
            case 364:
            case 365:
            case 367:
            case 369:
            case 371:
            case 373:
            case 375:
            case 377:
            case 379:
            case 381:
            case 383:
            case 386:
            case 387:
            case 394:
            case 395:
            case 400:
            case 401:
            case 402:
            case 403:
            case 404:
            case 405:
            case 406:
            case 407:
            case 408:
            case 409:
            case 410:
            case 411:
            case 420:
            case 421:
            case 428:
            case 429:
            case 450:
            case 451:
            case 458:
            case 459:
            case 464:
            case 465:
            case 466:
            case 467:
            case 468:
            case 469:
            case 470:
            case 471:
            case 472:
            case 473:
            case 474:
            case 475:
            case 484:
            case 485:
            case 492:
            case 493:
            case 513:
            case 514:
            case 515:
            case 517:
            case 519:
            case 521:
            case 522:
            case 523:
            case 525:
            case 527:
            case 528:
            case 529:
            case 530:
            case 531:
            case 532:
            case 533:
            case 534:
            case 535:
            case 536:
            case 537:
            case 538:
            case 539:
            case 541:
            case 543:
            case 545:
            case 547:
            case 548:
            case 549:
            case 551:
            case 553:
            case 555:
            case 556:
            case 557:
            case 559:
            case 561:
            case 563:
            case 565:
            case 567:
            case 569:
            case 571:
            case 573:
            case 575:
            case 577:
            case 579:
            case 581:
            case 583:
            case 585:
            case 586:
            case 587:
            case 589:
            case 591:
            case 592:
            case 593:
            case 594:
            case 595:
            case 596:
            case 597:
            case 598:
            case 599:
            case 600:
            case 601:
            case 602:
            case 603:
            case 605:
            case 607:
            case 609:
            case 611:
            case 612:
            case 613:
            case 615:
            case 617:
            case 619:
            case 620:
            case 621:
            case 623:
            case 625:
            case 627:
            case 629:
            case 631:
            case 633:
            case 635:
            case 637:
            case 639:
            case 642:
            case 643:
            case 650:
            case 651:
            case 656:
            case 657:
            case 658:
            case 659:
            case 660:
            case 661:
            case 662:
            case 663:
            case 664:
            case 665:
            case 666:
            case 667:
            case 676:
            case 677:
            case 684:
            case 685:
            case 706:
            case 707:
            case 714:
            case 715:
            case 720:
            case 721:
            case 722:
            case 723:
            case 724:
            case 725:
            case 726:
            case 727:
            case 728:
            case 729:
            case 730:
            case 731:
            case 740:
            case 741:
            case 748:
            case 749:
            case 769:
            case 770:
            case 771:
            case 773:
            case 775:
            case 777:
            case 778:
            case 779:
            case 781:
            case 783:
            case 784:
            case 785:
            case 786:
            case 787:
            case 788:
            case 789:
            case 790:
            case 791:
            case 792:
            case 793:
            case 794:
            case 795:
            case 797:
            case 799:
            case 801:
            case 803:
            case 804:
            case 805:
            case 807:
            case 809:
            case 811:
            case 812:
            case 813:
            case 815:
            case 817:
            case 819:
            case 821:
            case 823:
            case 825:
            case 827:
            case 829:
            case 831:
            case 833:
            case 834:
            case 835:
            case 837:
            case 839:
            case 841:
            case 842:
            case 843:
            case 845:
            case 847:
            case 848:
            case 849:
            case 850:
            case 851:
            case 852:
            case 853:
            case 854:
            case 855:
            case 856:
            case 857:
            case 858:
            case 859:
            case 861:
            case 863:
            case 865:
            case 867:
            case 868:
            case 869:
            case 871:
            case 873:
            case 875:
            case 876:
            case 877:
            case 879:
            case 881:
            case 883:
            case 885:
            case 887:
            case 889:
            case 891:
            case 893:
            case 895:
            case 898:
            case 899:
            case 906:
            case 907:
            case 912:
            case 913:
            case 914:
            case 915:
            case 916:
            case 917:
            case 918:
            case 919:
            case 920:
            case 921:
            case 922:
            case 923:
            case 932:
            case 933:
            case 940:
            case 941:
            case 962:
            case 963:
            case 970:
            case 971:
            case 976:
            case 977:
            case 978:
            case 979:
            case 980:
            case 981:
            case 982:
            case 983:
            case 984:
            case 985:
            case 986:
            case 987:
            case 996:
            case 997:
            case 1004:
            case 1005:
                valid = false;
        }
        return valid;
    }
    
    /**
     * Calculates an integer value that represents the individual case for which 
     * a uiType has to be determined.
     * According to the uiType decision table the attributes have the following integer
     * representation:
     * --------------------------------------------------
     * attribute                | binary        | integer
     * -------------------------------------------------
     * isMessageCstic           | 1000000000    | 512
     * showCalendarControl      | 0100000000    | 256
     * hasValues                | 0010000000    | 128
     * isExpanded               | 0001000000    |  64    
     * isDomainConstrained      | 0000100000    |  32
     * hasIntervalDomain        | 0000010000    |  16
     * allowsMultipleValues     | 0000001000    |   8
     * allowsAdditionalValues   | 0000000100    |   4
     * hasDomainValues          | 0000000010    |   2
     * hasAssignedValues        | 0000000001    |   1
     * ----------------------------------------------
     * @return int value that represents the individual case for which a uiType has to be determined. 
     */
    private static int calculateUiTypeCase(boolean isExpanded, CharacteristicUIBean bean){
        location.entering("calculateUiTypeCase(boolean isExpanded, CharacteristicUIBean bean)");
        int uiTypeCase = 0;
        category.logT(Severity.DEBUG, location, "uiTypeCase-calculation for characteristic "+ bean.getName() + ":");        
        if (bean.isMessageCstic()) {
            uiTypeCase += 512;
            category.logT(Severity.DEBUG, location, "isMessageCstic == true");
        }
        if (bean.showCalendarControl()) {
            uiTypeCase += 256;
            category.logT(Severity.DEBUG, location, "showCalendarControl == true");
        }                
        if (bean.getValues().size() > 0) {
            uiTypeCase += 128;
            category.logT(Severity.DEBUG, location, "hasValues == true");
        }
        if (isExpanded) {
            uiTypeCase += 64;
            category.logT(Severity.DEBUG, location, "isExpanded == true");
        }
        if (bean.getBusinessObject().isDomainConstrained()) {
            uiTypeCase += 32;
            category.logT(Severity.DEBUG, location, "isDomainConstrained == true");
        }
        if (bean.hasIntervalAsDomain()) {
            uiTypeCase += 16;
            category.logT(Severity.DEBUG, location, "hasIntervalAsDomain == true");
        }
        if (bean.allowsMultipleValues()) {
            uiTypeCase += 8;
            category.logT(Severity.DEBUG, location, "allowsMultipleValues == true");
        }
        if (bean.getBusinessObject().allowsAdditionalValues()) {
            uiTypeCase += 4;
            category.logT(Severity.DEBUG, location, "allowsAdditionalValues == true");
        }
        if (bean.hasDomainValues()) {
            uiTypeCase += 2;
            category.logT(Severity.DEBUG, location, "hasDomainValues == true");
        }
        if (bean.hasAssignedValues()) {
            uiTypeCase += 1;
            category.logT(Severity.DEBUG, location, "hasAssignedValues() == true");
        }
        category.logT(Severity.DEBUG, location, "uiTypeCase is "+ uiTypeCase);
        location.exiting();
        return uiTypeCase;
    }
    
    
}
