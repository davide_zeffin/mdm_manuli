/*****************************************************************************
    Class:        IPCUIExitHandler
    Copyright (c) 2008, SAP AG, Germany, All rights reserved.
    Author:       SAP AG
    Created:      11.04.2008
    Version:      1.0
*****************************************************************************/

package com.sap.isa.ipc.ui.jsp.dynamicui;

import com.sap.isa.core.util.RequestParser;
import com.sap.isa.maintenanceobject.backend.boi.DynamicUIExitData;
import com.sap.isa.maintenanceobject.businessobject.DynamicUI;
import com.sap.isa.maintenanceobject.businessobject.Property;

public class IPCUIExitHandler implements DynamicUIExitData {

	/**
	 * <p> The method parseProperty read the data from an additional input field.</p>
	 * 
	 * @param requestParser request parser allows the access to the request
	 * @param dynamicUI the dynamic ui object
	 * @param parameter parameter for the parsed property
	 * @param property the parsed property
	 */
	public boolean parseProperty(RequestParser requestParser, 
                              DynamicUI dynamicUI, 
                              RequestParser.Parameter parameter,
                              Property property) {
		boolean isPropertyParsed = false;
        if (!property.isHidden() && !property.isDisabled()) {
        	String parameterName =  property.getRequestParameterName() +"[]";
        	RequestParser.Parameter newParameter = requestParser.getParameter(parameterName);
        	if (newParameter.isSet()) {
        		isPropertyParsed = true;
        		String value = newParameter.getValue(0).getString();
        		if (value.length()>0) {
		            if (property.getType().equals(Property.TYPE_STRING)) {
		                property.setValue(value);
		            }
		            else if (property.getType().equals(Property.TYPE_LIST)) {
		                property.setValue(value);
		            }
        		}        
        	}
        }
        return isPropertyParsed;
	}
	
}
