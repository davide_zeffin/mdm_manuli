package com.sap.isa.ipc.ui.jsp.dynamicui.renderer;

import com.sap.isa.maintenanceobject.ui.UITypeRendererData;
import com.sap.isa.maintenanceobject.ui.renderer.UIRendererManager;

public class IPCUIRendererManager extends UIRendererManager {

	public static final String IPC_NO_RENDER_GROUP = "uitypeIPC_norendergroup";
	public static final String IPC_MULTI_DDLB_INPUT = "uitypeIPC_multiddlbinput";
	public static final String IPC_SINGLE_DDLB_INPUT = "uitypeIPC_singleddlbinput";		
	public static final String IPC_CHECKBOXES_INPUT = "uitypeIPC_checkboxinput";
	public static final String IPC_RADIO_BUTTONS_INPUT = "uitypeIPC_radiobuttoninput";
	public static final String IPC_MESSAGE_CSTIC = "uitypeIPC_messagecstic";
	public static final String IPC_NO_RENDER_GROUP_NAME = "uitypeIPC_groupWithoutName";

    public IPCUIRendererManager() {
        super();
    }

	/**
	 * Returns the corresponding UITypeRenderer for the given uiType.
	 * 
	 * @param uiType
	 * @return UITypeRenderer
	 */
	public UITypeRendererData getUIRenderer(String uiType) {
		if (uiType.equals(IPC_SINGLE_DDLB_INPUT)) {
			if (renderer.get(uiType) == null) {
				IPCUITypeRendererSingleDDLBInput rendererSingleDdlbInput = new IPCUITypeRendererSingleDDLBInput();
				renderer.put(uiType, rendererSingleDdlbInput);
			}
			return (UITypeRendererData) renderer.get(uiType);
		}
		else if (uiType.equals(IPC_MULTI_DDLB_INPUT)) {
			if (renderer.get(uiType) == null) {
				IPCUITypeRendererMultiDDLBInput rendererMultiDdlbInput = new IPCUITypeRendererMultiDDLBInput();
				renderer.put(uiType, rendererMultiDdlbInput);
			}
			return (UITypeRendererData) renderer.get(uiType);
		}
		else if (uiType.equals(IPC_RADIO_BUTTONS_INPUT)) {
			if (renderer.get(uiType) == null) {
				IPCUITypeRendererRadioButtonInput rendererRadioButtonInput = new IPCUITypeRendererRadioButtonInput();
				renderer.put(uiType, rendererRadioButtonInput);
			}
			return (UITypeRendererData) renderer.get(uiType);
		}
		else if (uiType.equals(IPC_CHECKBOXES_INPUT)) {
			if (renderer.get(uiType) == null) {
				IPCUITypeRendererCheckboxInput rendererCheckboxInput = new IPCUITypeRendererCheckboxInput();
				renderer.put(uiType, rendererCheckboxInput);
			}
			return (UITypeRendererData) renderer.get(uiType);
		}
		else if (uiType.equals(IPC_MESSAGE_CSTIC)) {
			if (renderer.get(uiType) == null) {
				IPCUITypeRendererMessageCharacteristic rendererMessageCstic = new IPCUITypeRendererMessageCharacteristic();
				renderer.put(uiType, rendererMessageCstic);
			}
			return (UITypeRendererData) renderer.get(uiType);
		}
		else if (uiType.equals(IPC_NO_RENDER_GROUP_NAME)) {
			if (renderer.get(uiType) == null) {
				IPCUITypeRendererGroupWithoutName rendererGroupWithoutName = new IPCUITypeRendererGroupWithoutName();
				renderer.put(uiType, rendererGroupWithoutName);
			}
			return (UITypeRendererData) renderer.get(uiType);
		}
		else {
			return super.getUIRenderer(uiType); 
		}
	}


}
