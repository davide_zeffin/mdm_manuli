package com.sap.isa.ipc.ui.jsp.action;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.sap.isa.core.UserSessionData;
import com.sap.isa.core.businessobject.management.MetaBusinessObjectManager;
import com.sap.isa.core.util.RequestParser;

public class DispatcherConfigTypeAction extends IPCBaseAction implements ActionForwardConstants{
	
	private final String CONFIG_TYPE_USER = "configtype";
	private final String CONFIG_TYPE = "configType";
	private final String GRID = "G";

	public ActionForward ecomPerform(ActionMapping mapping,
		ActionForm form,
		HttpServletRequest request,
		HttpServletResponse response,
		UserSessionData userSessionData,
		RequestParser requestParser,
		MetaBusinessObjectManager mbom,
		boolean multipleInvocation,
		boolean browserBack)
			throws IOException, ServletException {
			
			String forwardTo = ActionForwardConstants.SUCCESS;
			
			if(request.getParameter(CONFIG_TYPE) != null && request.getParameter(CONFIG_TYPE).equals(GRID)){
				forwardTo = ActionForwardConstants.GRID;
			}
			else if(userSessionData.getAttribute(CONFIG_TYPE_USER) != null && ((String)userSessionData.getAttribute(CONFIG_TYPE_USER)).equals(GRID)){
				forwardTo = ActionForwardConstants.GRID;
			}
		
			
			// TODO Auto-generated method stub
			return processForward(mapping, request, forwardTo);
			//mapping.findForward(forwardTo);
		}

}
