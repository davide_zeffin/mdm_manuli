package com.sap.isa.ipc.ui.jsp.action;

public interface ActionForwardConstants{
    public static final String DYNAMIC_UI = "dynamicUI";
	public static final String SUCCESS = "success";
    public static final String EMPTY = "empty";
    public static final String INPUT_PARAMETER_ERROR = "inputParameterError";
    public static final String INTERNAL_ERROR = "internalError";
    public static final String ILLEGAL_COMMAND_ERROR = "illegalCommandError";

	// parameters for the command/action layer (-> forwards)
    public static final String CHANGE_INSTANCE = "/ipc/changeInstance.do";
    public static final String EXPAND_INSTANCE = "/ipc/expandInstance.do";
	public static final String EXPAND_CHARACTERISTIC = "/ipc/expandCharacteristic.do";
    public static final String LOCK_SESSION = "/ipc/lockSession.do";
    public static final String RELEASE_SESSION = "/ipc/releaseSession.do";
	public static final String INITIALIZE = "initialize";
	public static final String INITIALIZE_PRODUCT_SIMULATION = "initializeProductSimulation";
	public static final String INITIALIZE_ORDER_MAINTAIN = "initializeOrderMaintain";
	public static final String START_CONFIGURATION = "startConfiguration";
	public static final String SHOW_CONFIGURATION = "showConfiguration";
	public static final String NO_CONFLICTS = "noConflicts";
	public static final String SHORTCUTS = "shortcuts";
	public static final String NO_SHORTCUTS = "noShortcuts";
	public static final String CHECK_MESSAGES = "checkMessages";
//	public static final String STANDARD_MESSAGE_OPTION_SELECTION = "standardMessageOptionSelection";
	public static final String STANDARD_MESSAGE_CONFLICT = "standardMessageConflict";
	public static final String STANDARD_MESSAGE_SOLVED_CONFLICT = "standardMessageSolvedConflict";
	public static final String SINGLE_LEVEL = "singleLevel";
	public static final String MULTI_LEVEL = "multiLevel";
	public static final String DISPLAY_LAYOUT = "displayLayout";
	public static final String GROUPS = "groups"; //forward of displayLayout
	public static final String NOGROUPS = "nogroups"; //forward of displayLayout
	public static final String SEARCH_SET = "searchset";
    public static final String EXPORT = "export";
    public static final String MIME_POPUP = "mimePopUp";
    public static final String COMPARISON_SCREEN = "comparisonScreen";
    public static final String COMPARISON_SCREEN_CSTICS = "comparisonScreenLayoutCstics";
    public static final String COMPARISON_SCREEN_GROUPS = "comparisonScreenLayoutGroups";
    public static final String COMPARISON_SCREEN_DYNAMICUI = "comparisonScreenDynamicUI";
    public static final String COMPARE_TO_SNAP = "compareToSnapshot";
    public static final String COMPARE_TO_STORED = "compareToStored";
    public static final String TAKE_SNAPSHOT = "takeSnapshot";
    public static final String NO_STORED = "noStored";
    public static final String NO_SNAPSHOT = "noSnapshot";
    public static final String COMPARISON_ERROR = "comparisonError";
    public static final String MERGE = "merge";
	public static final String DESIGNER = "uiDesigner";
	public static final String SHOW_PREVIEW = "showPreview";
    
	//layout constants
	public static final String MAIN = "main";
	public static final String SINGLE_LEVEL_MFA = "singlelevelMfa";
	public static final String MULTI_LEVEL_MFA = "multilevelMfa";
	
    public static final String SHOW_MIMES = "showMimes";
    public static final String NO_MIMES = "noMimes";
    public static final String FORM_TARGET = "ipc";
    
	//tabs of the multifunctional area
	static final String CUSTOMIZATION_LIST = "customizationList";
	static final String PRODUCT_VARIANTS = "productVariants";
	static final String DYNAMIC_PICTURE = "dynamicPicture";
	static final String CUSTOMER_TAB = "customerTab";
	
	
	//forwards for DispatchOnCallerAction
	static final String CRMORDERMAINTAIN = "crmordermaintain";
	static final String CRMORDERMAINTAIN_ERP_LRD = "crmordermaintain_erp_lrd";
	static final String CRMPRODUCTSIMULATION = "crmproductsimulation";
	static final String CRM_PRODUCT_CATALOG = "crmproductcatalog";
	static final String MSAORDERMAINTAIN = "msaordermaintain";
	static final String MSAPRODUCTSIMULATION = "msaproductsimulation";
	static final String MSAONLYCONFIG = "msaonlyconfig";
	static final String VEHICLEMANAGER = "vehiclemanager";
	static final String ERP_ORDER = "erpordermaintain";
	static final String ERP_PRODUCT_SIMULATION = "erpproductsimulation";
	static final String ERP_BASE = "erpbase"; //other configurations in ERP (product variants, purchase order, equipment, procuction order, ...
	static final String THIRD_PARTY = "thirdparty";
	static final String ONLYCONFIG = "onlyconfig";
	static final String CALLER_B2B_BASKET = "b2b_basket";
	static final String CALLER_B2C_BASKET = "b2c_basket";
	static final String CALLER_B2C_ORDERSTATUS = "b2c_orderstatus";
	static final String CALLER_B2B_ORDERSTATUS = "orderstatuscloseconfig";
	static final String CALLER_B2B_ORDER = "b2b_order";
	static final String FORWARD_CLOSE_CONFIGURATION = "contractCloseConfiguration";
	public final static String FW_BACK_TO_CATALOG      = "catalog";
    public final static String CALLER_B2C_CATALOG = "b2c_catalog";
	
	//forwards for all grid process handling
	static final String GRID = "grid";
	static final String READ_GRID_VARIANTS = "readGridVariants";

}