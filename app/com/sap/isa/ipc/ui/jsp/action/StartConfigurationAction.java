package com.sap.isa.ipc.ui.jsp.action;

import java.io.IOException;
import java.util.HashMap;
import java.util.Hashtable;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.sap.isa.businessobject.CommunicationException;
import com.sap.isa.core.UserSessionData;
import com.sap.isa.core.businessobject.management.MetaBusinessObjectManager;
import com.sap.isa.core.util.RequestParser;
import com.sap.isa.ipc.ui.jsp.uiclass.UIContext;
import com.sap.spc.remote.client.object.IPCException;
import com.sap.spc.remote.client.util.ext_configuration;
import com.sap.spc.remote.shared.ConfigException;
import com.sap.util.monitor.jarm.IMonitor;

/**
 *                ----------+---------|-----------
 *  table         |  object | request |  request |
 *  Inputparams   |---------+---------|----------|
 *                |         |  host   |          |
 *                | itemRef |  port   | documentid-----------------------------
 *  caller        |         | sessionid   itemid | with pricing enable        |
 *  --------------+---------+---------|----------+----------------------------|
 *  ISA           |   yes   |   no    |   no     |   yes                      |
 *  --------------+---------+---------+----------+-----------------------------
 *  CRMOrder      |   no    |   yes   |   yes    |   yes                      |
 *  --------------+---------+---------+----------+-----------------------------
 *  CRMSimulate   |   no    |   yes   |   no     |   no                       |
 *  --------------+---------+---------+----------+-----------------------------
 *  VehicleManager|   no    |   no    |   no     |   yes, create doc and item |
 *  --------------+---------+---------+----------+-----------------------------
 *  ThirdParty    |   no    |   no    |   no     |   yes                      |
 *  --------------+---------+---------+----------+-----------------------------
 *  ConfigureOnly |   no    |   no    |   no     |   no                       |
 *  --------------+---------+---------+----------+-----------------------------
 * @author Klaus Warth
 */
public class StartConfigurationAction
	extends IPCBaseAction
	implements RequestParameterConstants, ActionForwardConstants {

	//BD 24062003: Changelist 42773 IPC 4.0 SP2 SAT
	//new-->
	IMonitor _monitor = null;

	protected String SAT_STRING = "";
	//<--

	public StartConfigurationAction() {
		super();
		if (log.isDebugEnabled()) {
			log.debug("New instance for the action: " + this.getClass().getName() + " created.");
		}
	}


	/*
	 * @return a external configuration object
	 */
	protected ext_configuration getExternalConfiguration(HttpServletRequest request) {

		ext_configuration externalConfiguration = null;

		// is an external config is passed?
		String[] configs = getParameters(request, EXTERNAL_CONFIGURATION_ID);
		if (configs == null)
			return null;

		String language =
			IPCBaseAction.getParameterOrScenarioParameter(
				request,
				LANGUAGE);
		SimpleRequestWrapper simpleRequestWrapper =
			new SimpleRequestWrapper(request);
		try {
			externalConfiguration =
				ExternalConfigConverter.createConfig(
					simpleRequestWrapper,
					null,
					null,
					null,
					null,
					language,
					null);
		} catch (ConfigException configException) {
			log.fatal(this, configException);
			setExtendedRequestAttribute(request,
				IPC_EXCEPTION,
				new IPCException(configException.getMessage()));
			return null;
		}

		return externalConfiguration;
	}

	/*
	 * @return an array of parameter values
	 */
	protected String[] getParameters(HttpServletRequest request, String key) {
		SimpleRequestWrapper simpleRequestWrapper =
			new SimpleRequestWrapper(request);
		String[] values = simpleRequestWrapper.getParameterValues(key);
		return values;
	}

	/*
	 * @return a Hashtable with all the input stuff
	 */
	protected Hashtable getHashtable(String[] params, String[] values) {
		if (params == null || values == null)
			return null;
		Hashtable ht = new Hashtable(params.length);
		for (int i = 0; i < params.length; i++) {
			ht.put(params[i], values[i]);
		}
		return ht;
	}


	/*
	 * @return a Hashtable with all the input stuff
	 */
	protected HashMap makeHashMap(
		HttpServletRequest request,
		String paramsKey,
		String valuesKey) {
		String[] params = getParameters(request, paramsKey);
		String[] values = getParameters(request, valuesKey);
		HashMap hm = getHashMap(params, values);
		return hm;
	}

	/*
	 * @return a HashMap with all the input stuff
	 */
	private HashMap getHashMap(String[] params, String[] values) {
		if (params == null || values == null)
			return null;
		HashMap hm = new HashMap(params.length);
		for (int i = 0; i < params.length; i++) {
			hm.put(params[i], values[i]);
		}
		return hm;
	}


	/* (non-Javadoc)
	 * @see com.sap.isa.isacore.action.EComBaseAction#ecomPerform(org.apache.struts.action.ActionMapping, org.apache.struts.action.ActionForm, javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse, com.sap.isa.core.UserSessionData, com.sap.isa.core.util.RequestParser, com.sap.isa.core.businessobject.management.MetaBusinessObjectManager, boolean, boolean)
	 */
	public ActionForward ecomPerform(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response, UserSessionData userSessionData, RequestParser requestParser, MetaBusinessObjectManager mbom, boolean multipleInvocation, boolean browserBack) throws IOException, ServletException, CommunicationException {
		// TODO Auto-generated method stub
		return null;
	}

}
