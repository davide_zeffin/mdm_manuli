package com.sap.isa.ipc.ui.jsp.action;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.sap.isa.businessobject.ipc.IPCBOManager;
import com.sap.isa.core.UserSessionData;
import com.sap.isa.core.businessobject.management.MetaBusinessObjectManager;
import com.sap.isa.core.util.RequestParser;
import com.sap.isa.core.util.RequestParser.Parameter;
import com.sap.isa.ipc.ui.jsp.uiclass.UIContext;
import com.sap.spc.remote.client.object.Configuration;
import com.sap.spc.remote.client.object.IPCClient;
import com.sap.spc.remote.client.object.IPCException;
import com.sap.spc.remote.client.object.IPCItem;
import com.sap.spc.remote.client.object.IPCItemReference;
import com.sap.spc.remote.client.object.IPCSession;
import com.sap.spc.remote.client.object.imp.rfc.RFCIPCItemReference;

/**
 * @author 
 * Requires the following http request parameter:
 * IPC_HOST   {String}
 * IPC_PORT   {String representing integer number}
 * DISPATCHER {"true|false"}
 * IPC_SESSION, DOCUMENTID, ITEMID {String representing integer number} 
 *
 */
public class StartCRMOnlineConfigurationAction
	extends StartConfigurationAction {

	public ActionForward ecomPerform(
		ActionMapping mapping,
		ActionForm form,
		HttpServletRequest request,
		HttpServletResponse response,
		UserSessionData userSessionData,
		RequestParser requestParser,
		MetaBusinessObjectManager mbom,
		boolean multipleInvocation,
		boolean browserBack)
		throws IOException, ServletException {
			

			UIContext uiContext = IPCBaseAction.getUIContext(request);
			if (uiContext == null) { //the uiContext has not been initialized
				ActionForward forward = processScenarioParameter(mapping, request);
				if (!forward.getName().equals(ActionForwardConstants.SUCCESS)) return forward;
				uiContext = IPCBaseAction.getUIContext(request); //uiContext created in processScenarioParameter
			}
			
			super.htmlTrace(request);
			Parameter tokenIdParam = requestParser.getFromContext(IPC_TOKENID);
			if (!tokenIdParam.isSet()) return parameterError(mapping, request, RequestParameterConstants.IPC_TOKENID, getClass().getName());
			Parameter documentIdParam = requestParser.getFromContext(DOCUMENTID);
			if (!documentIdParam.isSet()) return parameterError(mapping, request, RequestParameterConstants.DOCUMENTID, getClass().getName());
			Parameter itemIdParam = requestParser.getFromContext(ITEMID);
			if (!itemIdParam.isSet()) return parameterError(mapping, request, RequestParameterConstants.ITEMID, getClass().getName());
			Parameter callerParam = requestParser.getFromContext(CALLER);
			if (!callerParam.isSet()) return parameterError(mapping, request, RequestParameterConstants.CALLER, getClass().getName());
			uiContext.setCaller(callerParam.getValue().getString());
			Parameter displayModeParam = requestParser.getFromContext(RequestParameterConstants.DISPLAY_MODE);
			boolean displayMode;
			if ("T".equals(displayModeParam.getValue().getString())) {
				displayMode = true;
			}else {
				displayMode = false;
			}
			
			IPCItemReference itemReference = new RFCIPCItemReference();
			itemReference.setDocumentId(documentIdParam.getValue().getString());
			itemReference.setItemId(itemIdParam.getValue().getString());
			if (log.isDebugEnabled()) {
				log.debug("Documentid: " + itemReference.getDocumentId());
				log.debug("Itemid: " + itemReference.getItemId());						
			}

    		IPCBOManager ipcBoManager =
    			(IPCBOManager) userSessionData.getBOM(IPCBOManager.NAME);
    		if (ipcBoManager == null) {
    			log.error("No IPC BOM in session data!");
    			return (mapping.findForward(INTERNAL_ERROR));
    		}
    
            String configId = "I" + itemReference.getDocumentId() + "/" + itemReference.getItemId();
			
			IPCClient ipcClient;
//          21Sep2005 tm Potential performance improvement? Using an existing IPCClient from the session.
//          However, CRM online publishs a new configuration with each call that need to be bound and the
//          client needs to be reinitialized. Establishing the connection is not so expensive compared to
//          the client state initialization.
//			ipcClient = (IPCClient)uiContext.getProperty(InternalRequestParameterConstants.IPC_CLIENT);
//			if (ipcClient != null) { //is this call in an existing session?
//				//want to reuse the same session in order to get the lock status of the client session
//				//otherwise, the lock might remain and a lock is tried again.
//				ipcClient.getIPCSession().syncWithServer(); //the session state could have changed on the server
//			}else {

			Boolean logonFallback = (Boolean) request.getAttribute(LOGON_FALLBACK);
			
			// if the SSO Logon failed, the generic JCO user stored in XCM should be used
			// rather then displaying the a login screen.
			// by passing the language to the ipcClient, we will trigger proper intilisation of
			// the connection. This includes setting of the Appserver, client and sysnumber
			// which are parsed by ISA framework and stored in backend context.	
			if(logonFallback != null && logonFallback.booleanValue()){
				// No Logon happened, so there is no SSO ticket to read the language, so we
				// take language that is set in request
				Parameter languageParam = requestParser.getFromContext(LANGUAGE);
				if (!languageParam.isSet()) return parameterError(mapping, request, RequestParameterConstants.LANGUAGE, getClass().getName());
					ipcClient =
						ipcBoManager.createIPCClient(
						languageParam.getValue().getString(),
						tokenIdParam.getValue().getString(),
						configId);
			}else{
				// during logon, language was read from the SSO ticket and set into the
				// connection. no need to pass it here.
				ipcClient =
					ipcBoManager.createIPCClient(
						tokenIdParam.getValue().getString(),
						configId);
			}
//			}
          

			IPCSession session = ipcClient.getIPCSession();
			if (!displayMode && !session.isLocked()) {
				ipcClient.getIPCSession().lock();
			}
			//item must exist
			IPCItem ipcItem = ipcClient.getIPCItem(itemReference, true);
			if (ipcItem == null) {
				IPCException ipcException =
					new IPCException(
						"The item "
							+ itemReference.toString()
							+ " could not be found on the IPC server!");
				setExtendedRequestAttribute(request, IPC_EXCEPTION, ipcException);
				log.error(this, ipcException);
				return mapping.findForward(INTERNAL_ERROR);
			}
            
            // If item is product variant set the productVariant mode to true.
            setProductVariantMode(uiContext, ipcItem);

			Configuration configuration = ipcItem.getConfiguration();
			if (configuration == null) {
				IPCException ipcException =
					new IPCException(
						"No configuration associated to item "
							+ ipcItem
							+ "!");
				setExtendedRequestAttribute(request, InternalRequestParameterConstants.IPC_EXCEPTION, ipcException);
				return mapping.findForward(ActionForwardConstants.INTERNAL_ERROR);
			}

		customerExit(mapping, form, request, response, userSessionData, requestParser, mbom, multipleInvocation, browserBack);

		uiContext.setCurrentConfiguration(configuration);
		//initialize the context
		setInitialContextValues(request, configuration);
		uiContext.setCaller(callerParam.getValue().getString());
		uiContext.setProperty(InternalRequestParameterConstants.IPC_CLIENT, ipcClient);
		
		return mapping.findForward(ActionForwardConstants.SUCCESS);
	}

}
