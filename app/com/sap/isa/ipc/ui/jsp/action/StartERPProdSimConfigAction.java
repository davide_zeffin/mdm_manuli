/*
 * Created on 09.05.2005
 *
 */
package com.sap.isa.ipc.ui.jsp.action;

import java.io.IOException;
import java.util.Hashtable;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.sap.isa.businessobject.ipc.IPCBOManager;
import com.sap.isa.core.UserSessionData;
import com.sap.isa.core.businessobject.management.MetaBusinessObjectManager;
import com.sap.isa.core.util.RequestParser;
import com.sap.isa.core.util.RequestParser.Parameter;
import com.sap.isa.ipc.ui.jsp.uiclass.UIContext;
import com.sap.spc.remote.client.object.Configuration;
import com.sap.spc.remote.client.object.IPCClient;
import com.sap.spc.remote.client.util.ext_configuration;

/**
 * @author 
 *
 */
public class StartERPProdSimConfigAction extends StartConfigurationAction {

	public ActionForward ecomPerform(
		ActionMapping mapping,
		ActionForm form,
		HttpServletRequest request,
		HttpServletResponse response,
		UserSessionData userSessionData,
		RequestParser requestParser,
		MetaBusinessObjectManager mbom,
		boolean multipleInvocation,
		boolean browserBack)
		throws IOException, ServletException {
		
			UIContext uiContext = IPCBaseAction.getUIContext(request);
			if (uiContext == null) { //the uiContext has not been initialized
			ActionForward forward = processScenarioParameter(mapping, request);
			if (!forward.getName().equals(ActionForwardConstants.SUCCESS)) return forward;
				uiContext = IPCBaseAction.getUIContext(request); //uiContext created in processScenarioParameter
			}
		
			super.htmlTrace(request);
			IPCBOManager ipcBoManager =
				(IPCBOManager) userSessionData.getBOM(IPCBOManager.NAME);
			if (ipcBoManager == null) {
				log.error("No IPC BOM in session data!");
				return (mapping.findForward(INTERNAL_ERROR));
			}
			
			Parameter callerParam = requestParser.getParameter(CALLER);
			if (!callerParam.isSet()) return parameterError(mapping, request, RequestParameterConstants.CALLER, getClass().getName());
			uiContext.setCaller(callerParam.getValue().getString());
			
			Parameter language = requestParser.getParameter(RequestParameterConstants.LANGUAGE);
			if (!language.isSet()) {
				return parameterError(mapping, request, RequestParameterConstants.LANGUAGE, getClass().getName());
			}
			Parameter productId = requestParser.getParameter(RequestParameterConstants.PRODUCT_ID_ERP);
			if (!productId.isSet()) {
				return parameterError(mapping, request, RequestParameterConstants.PRODUCT_ID_ERP, getClass().getName());
			}
			Parameter kbName = requestParser.getParameter(RequestParameterConstants.KB_NAME);
			if (!kbName.isSet()) {
				return parameterError(mapping, request, RequestParameterConstants.KB_NAME, getClass().getName());
			}
			Parameter kbVersion = requestParser.getParameter(RequestParameterConstants.KB_VERSION);
			if (!kbVersion.isSet()) {
				return parameterError(mapping, request, RequestParameterConstants.KB_VERSION, getClass().getName());
			}
			Parameter kbLogsys = requestParser.getParameter(RequestParameterConstants.KB_LOGSYS);
			if (!kbLogsys.isSet()) {
				return parameterError(mapping, request, RequestParameterConstants.KB_LOGSYS, getClass().getName());
			}
			Parameter kbProfile = requestParser.getParameter(RequestParameterConstants.KB_PROFILE);
			if (!kbProfile.isSet()) {
				return parameterError(mapping, request, RequestParameterConstants.KB_PROFILE, getClass().getName());
			}
			Parameter kbDate = requestParser.getParameter(RequestParameterConstants.KB_DATE);
			if (!kbDate.isSet()) {
				return parameterError(mapping, request, RequestParameterConstants.KB_DATE, getClass().getName());
			}
			Parameter displayMode = requestParser.getParameter(RequestParameterConstants.DISPLAY_MODE);
			if (!displayMode.isSet() || !displayMode.getValue().getBoolean()) {
				uiContext.setDisplayMode(true);
			}else if (displayMode.getValue().getBoolean()){
				uiContext.setDisplayMode(false);
			}
			
			customerExit(mapping, form, request, response, userSessionData, requestParser, mbom, multipleInvocation, browserBack);

			IPCClient ipcClient = ipcBoManager.createIPCClient(language.getValue().getString());
			
			ext_configuration extConfig = getExternalConfiguration(request);
			// context information
			String[] contextNames = getParameters(request, CONTEXT_NAME);
			if (contextNames == null) contextNames = new String[] {};
			String[] contextValues = getParameters(request, CONTEXT_VALUE);
			if (contextValues == null) contextValues = new String[] {};
					
			Configuration config;
			if (extConfig != null) {
				config = ipcClient.createConfiguration(extConfig, contextNames, contextValues, kbDate.getValue().getString());
			}else {
			
				config =
					ipcClient.createConfiguration(
						productId.getValue().getString(),
						language.getValue().getString(),
						kbDate.getValue().getString(),
						kbLogsys.getValue().getString(),
						kbName.getValue().getString(),
						kbVersion.getValue().getString(),
						kbProfile.getValue().getString(),
						contextNames,
						contextValues);
			}
					
			uiContext.setCurrentConfiguration(config);
			//initialize the context
			setInitialContextValues(request, config);
					
					
			return mapping.findForward(ActionForwardConstants.SUCCESS);		
		
		}
		
}