package com.sap.isa.ipc.ui.jsp.action;

import java.io.IOException;
import java.util.Iterator;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.sap.isa.core.UserSessionData;
import com.sap.isa.core.businessobject.management.MetaBusinessObjectManager;
import com.sap.isa.core.util.RequestParser;
import com.sap.spc.remote.client.object.IPCClient;
import com.sap.spc.remote.client.object.IPCException;
import com.sap.spc.remote.client.object.KnowledgeBase;

public class GetProfilAction
    extends IPCBaseAction
    implements RequestParameterConstants, ActionForwardConstants {

		public ActionForward ecomPerform(ActionMapping mapping,
			ActionForm form,
			HttpServletRequest request,
			HttpServletResponse response,
			UserSessionData userSessionData,
			RequestParser requestParser,
			MetaBusinessObjectManager mbom,
			boolean multipleInvocation,
			boolean browserBack)
				throws IOException, ServletException {


        String kbName = getParameter(request, KB_NAME);
        if (kbName == null || kbName.equals("") || kbName.equals("null")) {
            return (mapping.findForward(INTERNAL_ERROR));
        }
        
        IPCClient ipcClient = getIPCClient(request);

        String kbVersion = getParameter(request, KB_VERSION);
        try {
            List knowledgeBases = ipcClient.getKnowledgeBases(kbName);

            for (Iterator kbIt = knowledgeBases.iterator(); kbIt.hasNext();) {
                KnowledgeBase kb = (KnowledgeBase) kbIt.next();
                if (kb.getVersion().equals(kbVersion)) {
                    request.setAttribute(KNOWLEDGEBASE, kb);
                    break;
                }
            }
        } catch (IPCException e) {
            request.setAttribute(RequestParameterConstants.IPC_EXCEPTION, e);
            return mapping.findForward(INTERNAL_ERROR);
        }

		customerExit(mapping, form, request, response, userSessionData, requestParser, mbom, multipleInvocation, browserBack);

        return processForward(mapping, request, ActionForwardConstants.SUCCESS);
    }

    private String getParameter(HttpServletRequest request, String key) {
        String value = null;
        value = request.getParameter(key);
        if (value == null)
            return (String) request.getAttribute(key);
        return value;
    }

}
