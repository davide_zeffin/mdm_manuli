/**
 * The action GetInstancesAction returns a list of instances
 * If no instances are found you get an empty list of instances
 * The list of instances is stored in the request;
 * In the jsp page you can access the the list of instances
 * using the following method:
 *
 * List instances = (List)request.getAttribute(RequestParameterConstants.INSTANCES);
 *
 * The action also adds the root instance as currentInstance
 * to the UIContext.
 */
package com.sap.isa.ipc.ui.jsp.action;

import java.io.IOException;
import java.util.Hashtable;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.sap.isa.core.Constants;
import com.sap.isa.core.UserSessionData;
import com.sap.isa.core.businessobject.management.MetaBusinessObjectManager;
import com.sap.isa.core.util.RequestParser;
import com.sap.isa.ipc.ui.jsp.uiclass.UIContext;

/**
 * Precondition: StartConfiguration was called in the same http session in order
 * to fill the uiContext with the configuration.
 * Takes an instance id from the request, gets the associated Instance from the
 * Configuration and puts it to the request as an Attribute.
 * Puts all Instances of the configuration into the request as attribute.
 */
public class ExpandInstanceAction extends IPCBaseAction implements RequestParameterConstants,
                                                              ActionForwardConstants {

	public ActionForward ecomPerform(ActionMapping mapping,
		ActionForm form,
		HttpServletRequest request,
		HttpServletResponse response,
		UserSessionData userSessionData,
		RequestParser requestParser,
		MetaBusinessObjectManager mbom,
		boolean multipleInvocation,
		boolean browserBack)
			throws IOException, ServletException {

        UIContext uiContext = getUIContext(request);
        
		//selected instance is the one that has the "selected" status on the display.
		//the user entered values need to be buffered for this.
		String selectedInstanceId = request.getParameter(Constants.SELECTED_INSTANCE_ID);
		if (!isSet(selectedInstanceId)) {
			parameterError(mapping, request, Constants.SELECTED_INSTANCE_ID, getClass().getName());
		}
        
        if (!uiContext.getOnlineEvaluate() && !browserBack) {
            ActionForward forward = bufferCharacteristicValues(mapping, request, selectedInstanceId);
            if (!forward.getName().equals(SUCCESS)) {
                return forward;
            }
        }
        
        changeContextValues(request);
        
        // The collapse may lead to a change of the instance (e.g. user is on a sub-instance and clicks
        // the collapse icon on parent instance -> the sub-instance will disappear and the UI will change
        // to the parent instance).
        // In this case the context values for groups have to be changed to the values of the current instance.
        // If the values would not be changed this would lead to an ArrayIndexOutOfBoundsException because the
        // "old" groups may not exist on the "new" (current) instance.
        
        // Naming "current instance": this is the instance which should be displayed next in the UI.
        // I.e. the user clicks on an instance which he wants to get displayed next. This instance
        // is the "currentInstance" processed in this action.
        
        String instanceId = request.getParameter(Constants.CURRENT_INSTANCE_ID);
        if (!isSet(instanceId)) {
            return parameterError(mapping, request, Constants.CURRENT_INSTANCE_ID, getClass().getName());
        }
        // Initialize the group context-values only if there was a change in the instances.
        if (!selectedInstanceId.equals(instanceId)){
            setInitialGroupContextValues(request, uiContext.getCurrentConfiguration().getInstance(instanceId));
        }
 
		String instanceTreeStatusChange = request.getParameter(Constants.INSTANCE_TREE_STATUS_CHANGE);
		Hashtable instanceTreeStatus = (Hashtable) uiContext.getProperty(Constants.INSTANCE_TREE_STATUS_CHANGE);
		if (instanceTreeStatus == null)
		{
		  instanceTreeStatus = new Hashtable();
		  uiContext.setProperty(Constants.INSTANCE_TREE_STATUS_CHANGE,instanceTreeStatus);
		}

		// This piece of code will fill the hashtable with the instance names of
		// collapsed nodes
		//The parameter INSTANCE_TREE_STATUS_CHANGE is only sent by the instances.jsp
		//so no problems with the other frames
		if (instanceTreeStatusChange != null)
		{
		  if (instanceTreeStatus.get(instanceTreeStatusChange) != null)
		  {
			instanceTreeStatus.remove(instanceTreeStatusChange);
		  }
		  else
		  {
			instanceTreeStatus.put(instanceTreeStatusChange,instanceTreeStatusChange);
		  }
		}

		customerExit(mapping, form, request, response, userSessionData, requestParser, mbom, multipleInvocation, browserBack);

        return processForward(mapping, request, ActionForwardConstants.SUCCESS);       
    }
    
}

