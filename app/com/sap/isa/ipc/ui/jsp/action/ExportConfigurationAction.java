package com.sap.isa.ipc.ui.jsp.action;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Vector;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.sap.isa.core.Constants;
import com.sap.isa.core.UserSessionData;
import com.sap.isa.core.businessobject.management.MetaBusinessObjectManager;
import com.sap.isa.core.util.RequestParser;
import com.sap.isa.ipc.ui.jsp.beans.MessageUIBean;
import com.sap.isa.ipc.ui.jsp.beans.UIBeanFactory;
import com.sap.isa.ipc.ui.jsp.uiclass.UIContext;
import com.sap.spc.remote.client.object.Configuration;
import com.sap.spc.remote.client.util.imp.c_ext_cfg_imp;

/**
 * This action is responsible to create a String representation of the current
 * configuration. This string is added to the UIcontext with the parameter "EXPORT_DATA".
 * Also the name of the file is added to the UIContext with the parameter "EXPORT_DATA_FILENAME".
 * This action also creates a status message of the export. This message is but to the 
 * message list which is hold by the UIContext with the parameter "MESSAGES". 
 */
public class ExportConfigurationAction extends IPCBaseAction implements RequestParameterConstants,
																	ActionForwardConstants {                                                                        
	public ActionForward ecomPerform(ActionMapping mapping,
		ActionForm form,
		HttpServletRequest request,
		HttpServletResponse response,
		UserSessionData userSessionData,
		RequestParser requestParser,
		MetaBusinessObjectManager mbom,
		boolean multipleInvocation,
		boolean browserBack)
			throws IOException, ServletException {

        UIContext uiContext = getUIContext(request);
		String currentInstId = request.getParameter(Constants.CURRENT_INSTANCE_ID);
		if (isSet(currentInstId)) {
			changeContextValue(request, Constants.CURRENT_INSTANCE_ID, currentInstId);		
		}
        String currentGroupName = request.getParameter(Constants.CURRENT_CHARACTERISTIC_GROUP_NAME);
        if (isSet(currentGroupName)) {
            setContextValue(request, Constants.CURRENT_CHARACTERISTIC_GROUP_NAME, currentGroupName);
        }
        String curScrollGroupName = request.getParameter(Constants.CURRENT_SCROLL_CHARACTERISTIC_GROUP_NAME);
        if (isSet(curScrollGroupName)) {
            setContextValue(request, Constants.CURRENT_SCROLL_CHARACTERISTIC_GROUP_NAME, curScrollGroupName);
        }
        String currentCharacteristicName = request.getParameter(Constants.CURRENT_CHARACTERISTIC_NAME);
        if (isSet(currentCharacteristicName)) {
            setContextValue(request, Constants.CURRENT_CHARACTERISTIC_NAME, currentCharacteristicName);
        }
        String selectedMfaTabName = request.getParameter(Constants.SELECTED_MFA_TAB_NAME);
        if (isSet(selectedMfaTabName)) {
            setContextValue(request, Constants.SELECTED_MFA_TAB_NAME, selectedMfaTabName);
        }
        
		if (!uiContext.getOnlineEvaluate() && !browserBack) {
			ActionForward forward = bufferCharacteristicValues(mapping, request, currentInstId);
			if (!forward.getName().equals(SUCCESS)) {
				return forward;
			}
		}
        List messages = (List) uiContext.getProperty(MESSAGES);
        MessageUIBean messageBean = UIBeanFactory.getUIBeanFactory().newMessageUIBean();
        String securityLevel = (String)uiContext.getProperty(SECURITY_LEVEL);
        try {
            if (messages == null){
                messages = new Vector();    
            }
            // check security level
            if (securityLevel!=null && securityLevel.equals("0")){                
                // just to ensure that uiContext is empty
                // also should be done by exportConfiguration.jsp
                uiContext.removeProperty(EXPORT_DATA);
                uiContext.removeProperty(EXPORT_DATA_FILENAME);
        		Configuration currentConfiguration = uiContext.getCurrentConfiguration();
        		c_ext_cfg_imp ext_config = (c_ext_cfg_imp) currentConfiguration.toExternal();
                
        		String data = ext_config.cfg_ext_to_xml_string();
                
                if (data != null && !data.equals("")){
                    // create filename 
                    StringBuffer fileName = new StringBuffer();
                    fileName.append(currentConfiguration.getRootInstance().getName());
                    fileName.append("_");
                    SimpleDateFormat fmt = new SimpleDateFormat();
                    fmt.applyPattern("yyyyMMdd'_'hhmmss");
                    fmt.format( new Date(System.currentTimeMillis()));
                    fileName.append(fmt.format( new Date(System.currentTimeMillis())));
                    fileName.append(".cfg");
                    
                    // put data to request, uiContext
					setExtendedRequestAttribute(request, EXPORT_START, T);
                    uiContext.setProperty(EXPORT_DATA, data);
                    uiContext.setProperty(EXPORT_DATA_FILENAME, fileName.toString());
                    // create message
                    if (currentConfiguration.isConsistent()){
                        // success message that export was successful
                        // not displayed because of popup download window
                        // otherwise message is shown before user downloaded the file
                        messageBean = null; 
                        //messageBean.setMessageStatus(MessageUIBean.SUCCESS);
                        //messageBean.setMessageTranslateKey("ipc.message.export.success");
                    } else {
                        messageBean.setMessageStatus(MessageUIBean.WARNING); 
                        messageBean.setMessageTranslateKey("ipc.message.export.warning");
                    }
                } else {
                    messageBean.setMessageStatus(MessageUIBean.ERROR);
                    messageBean.setMessageTranslateKey("ipc.message.export.error");
                }
            } else {
                messageBean.setMessageStatus(MessageUIBean.ERROR);
                messageBean.setMessageTranslateKey("ipc.message.export.security");
            }
        } catch (Exception e) {
            messageBean.setMessageStatus(MessageUIBean.ERROR);
            messageBean.setMessageTranslateKey("ipc.message.export.error");
        }
        // add message to uiContext
        messages.add(messageBean);
        uiContext.setProperty(MESSAGES, messages);

		customerExit(mapping, form, request, response, userSessionData, requestParser, mbom, multipleInvocation, browserBack);
        
        return (mapping.findForward(SUCCESS));
    }
}
