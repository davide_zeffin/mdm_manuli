/*
 * Created on 04.01.2005
 *
 */
package com.sap.isa.ipc.ui.jsp.action;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.sap.isa.core.Constants;
import com.sap.isa.core.UserSessionData;
import com.sap.isa.core.businessobject.management.MetaBusinessObjectManager;
import com.sap.isa.core.util.RequestParser;
import com.sap.isa.ipc.ui.jsp.uiclass.UIContext;

/**
 * @author 
 *
 */
public class GotoConflictTraceAction extends IPCBaseAction {

	public ActionForward ecomPerform(ActionMapping mapping,
		ActionForm form,
		HttpServletRequest request,
		HttpServletResponse response,
		UserSessionData userSessionData,
		RequestParser requestParser,
		MetaBusinessObjectManager mbom,
		boolean multipleInvocation,
		boolean browserBack)
			throws IOException, ServletException {

		UIContext uiContext =
			getUIContext(request);
			String currentInstId = request.getParameter(Constants.CURRENT_INSTANCE_ID);
			if (isSet(currentInstId)) {
				changeContextValue(request, Constants.CURRENT_INSTANCE_ID, currentInstId);		
			}
		if (!uiContext.getOnlineEvaluate() && !browserBack) {
			ActionForward forward =
				bufferCharacteristicValues(mapping, request, currentInstId);
			if (!forward.getName().equals(ActionForwardConstants.SUCCESS)) {
				return forward;
			}
		}
	
		//Set all context parameter
	    String selectedInstanceId = request.getParameter(Constants.SELECTED_INSTANCE_ID);
	    this.changeContextValue(request, Constants.SELECTED_INSTANCE_ID, selectedInstanceId);
	    
		String currentGroupName = request.getParameter(Constants.CURRENT_CHARACTERISTIC_GROUP_NAME);
		if (isSet(currentGroupName)) {
			setContextValue(request, Constants.CURRENT_CHARACTERISTIC_GROUP_NAME, currentGroupName);
		}
		String curScrollGroupName = request.getParameter(Constants.CURRENT_SCROLL_CHARACTERISTIC_GROUP_NAME);
		if (isSet(curScrollGroupName)) {
			setContextValue(request, Constants.CURRENT_SCROLL_CHARACTERISTIC_GROUP_NAME, curScrollGroupName);
		}
		String currentCharacteristicName = request.getParameter(Constants.CURRENT_CHARACTERISTIC_NAME);
		if (isSet(currentCharacteristicName)) {
			setContextValue(request, Constants.CURRENT_CHARACTERISTIC_NAME, currentCharacteristicName);
		}
		String selectedMfaTabName = request.getParameter(Constants.SELECTED_MFA_TAB_NAME);
		if (isSet(selectedMfaTabName)) {
			setContextValue(request, Constants.SELECTED_MFA_TAB_NAME, selectedMfaTabName);
		}
	
		customerExit(mapping, form, request, response, userSessionData, requestParser, mbom, multipleInvocation, browserBack);
	
		return mapping.findForward(ActionForwardConstants.SUCCESS);
	}

}
