package com.sap.isa.ipc.ui.jsp.action;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.sap.isa.core.UserSessionData;
import com.sap.isa.core.businessobject.management.MetaBusinessObjectManager;
import com.sap.isa.core.util.RequestParser;
import com.sap.spc.remote.client.object.IPCClient;
import com.sap.util.monitor.jarm.IMonitor;
//<--

/**
 * Title:        
 * Description:  JSP UI for the IPC.
 * Copyright:    Copyright (c) 2001
 * Company:      
 * @author 
 * @version 1.0
 */

/**
 * Checks the heartbeat of the IPC.
 * Forwards to {@link com.sap.isa.ipc.ui.jsp.action.ActionForwardConstants#SUCCESS}
 * if everything is o.k.
 * Forwards to {@link com.sap.isa.ipc.ui.jsp.action.ActionForwardConstants#INTERNAL_ERROR}
 * in case of error and puts the following 
 * attributes to the request.
 * {@link  com.sap.isa.businessobject.ipc.IPCClient#SERVER_CODE},
 * {@link  com.sap.isa.businessobject.ipc.IPCClient#DB_CODE},
 * {@link  com.sap.isa.businessobject.ipc.IPCClient#PRICING_CODE},
 * {@link  com.sap.isa.businessobject.ipc.IPCClient#CONFIG_CODE},
 * possible values:
 *  {@link  com.sap.isa.businessobject.ipc.IPCClient#OK},
 *  {@link  com.sap.isa.businessobject.ipc.IPCClient#UNKNOWN_ERROR},
 *  {@link  com.sap.isa.businessobject.ipc.IPCClient#UNABLE_TO_DETERMINE},
 *  {@link  com.sap.isa.businessobject.ipc.IPCClient#SPC_SESSION_MISSING},
 *  {@link  com.sap.isa.businessobject.ipc.IPCClient#SCE_DATABASE_MISSING},
 *  {@link  com.sap.isa.businessobject.ipc.IPCClient#NO_KNOWLEDGEBASE_AVAILABLE},
 *  {@link  com.sap.isa.businessobject.ipc.IPCClient#PRICING_ENGINE_MISSING},
 *  {@link  com.sap.isa.businessobject.ipc.IPCClient#CONVERSION_ENGINE_MISSING},
 *  {@link  com.sap.isa.businessobject.ipc.IPCClient#PRICING_ENGINE_NOT_INITIALIZED},
 *  {@link  com.sap.isa.businessobject.ipc.IPCClient#PRICING_ENGINE_WRONG_CLIENT},
 */
public class CheckHeartbeatAction extends IPCBaseAction {

	//BD 25052003: Changelist 42773 IPC 4.0 SP2 SAT
	//new-->
	//The monitor for the SAT 
	IMonitor _monitor = null;
	//<--

    public CheckHeartbeatAction() {
    }
	
    /* Reads the IPCClient businessobject from the session and calls the checkHeartbeat() method.
     * @see com.sap.isa.core.BaseAction#doPerform(org.apache.struts.action.ActionMapping, org.apache.struts.action.ActionForm, javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse)
     */
	public ActionForward ecomPerform(ActionMapping mapping,
		ActionForm form,
		HttpServletRequest request,
		HttpServletResponse response,
		UserSessionData userSessionData,
		RequestParser requestParser,
		MetaBusinessObjectManager mbom,
		boolean multipleInvocation,
		boolean browserBack)
			throws IOException, ServletException {

		IPCClient ipcClient = this.getIPCClient(request);

        if(ipcClient == null)
        {
        	return (mapping.findForward(ActionForwardConstants.INTERNAL_ERROR)); 
        } 
		
		//BD 25052003: Changelist 42773 IPC 4.0 SP2 SAT
		//new-->
		ipcClient.setJARMMonitor(_monitor);
		//<--

        //TODO Heartbeat: check of IPC server was removed -- implement a new check of the web-app        			
		/**
        String[][] result = ipcClient.checkHeartbeat();
		//check if all component result codes are OK
		boolean everythingOk = true;
		for (int i = 0; i < result.length; i++) {
			if (!result[i][1].equals(Heartbeat.OK)) {
				request.setAttribute(result[i][0], result[i][1]);
				everythingOk = false;
			}
		}
        **/
        
        boolean everythingOk = true;
		if (everythingOk) {
			return mapping.findForward(ActionForwardConstants.SUCCESS);
		}else {
			return mapping.findForward(ActionForwardConstants.INTERNAL_ERROR);
		}
    }
}