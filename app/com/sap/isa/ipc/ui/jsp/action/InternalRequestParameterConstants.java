package com.sap.isa.ipc.ui.jsp.action;

/**
 * Title:
 * Description:  Definitions of application internal request parameter constants.
 * These constants define request parameter names for those parameter that are sent
 * from the html form to the actions.
 * The constants define also parts for request attribute names that are sent from
 * the actions to the jsp pages. In order to avoid naming conflicts for the request
 * attributes each components (usually 1:1 with an action) concatenates the name of
 * the component to the attribute name. The names of the components are defined in
 * ComponentRequestParameterConstants.
 * The names of the component request attribute names are defined in the
 * <component name>RequestParameterConstants interfaces.
 * Request attribute names that are not used in more than one component do not need to be
 * defined here, but directly in the <component name>RequestParameterConstants interfaces.
 * Copyright:    Copyright (c) 2001
 * Company:
 * @author
 * @version 1.0
 */

public interface InternalRequestParameterConstants {
	

	// action parameter constants, internally used only
    static final String CURRENT_CONFIGURATION = "cConfig";
	static final String CURRENT_INSTANCE = "cInst";
	static final String IPC_CLIENT = "ipcClient";
	static final String LOGON_FALLBACK = "ipcLogonFallback";
	//in the online evaluate false case, an additional field points the
	//BufferCharacteristicsValuesAction to the instance that needs to be buffered.
	static final String CURRENT_CHARACTERISTIC_GROUP = "cCharGroup";
	static final String CURRENT_CHARACTERISTIC = "cChar";
    static final String CURRENT_VALUE = "cVal";
	static final String CURRENT_SCROLL_CHARACTERISTIC_GROUP = "cScrollCharGroup";
	static final String VALUE_LANGUAGE_DEPENDENT_NAME = "valueLanguageDependentName";
	static final String CONFLICT_TRACE_INFO = "conflicttrace.info";//"conflictTraceInfo";
	static final String CURRENT_MIME_OBJECT = "cMimeObj";
	static final String CHARACTERISTICS = "characteristics";
	static final String VALUES = "values";
    static final String VALUES_BY_CSTIC = "valuesByCstic";
//	static final String INSTANCES = "instances";
	static final String CHARACTERISTIC_GROUPS = "characteristicGroups";
	static final String CHARACTERISTIC_GROUPS_SELECTION = "characteristicGroupsSelection";
//	static final String PRODUCT_VARIANTS = "productVariants";
	static final String PRODUCT_VARIANTS_TO_COMPARE = "productVariantsToCompare";
	static final String PRODUCT_VARIANTS_BY_ID = "productVariantsById";
	static final String FORWARD_DESTINATION = "forwardDestination";
	static final String EXTERNAL_CONFIGURATION = "externalConfiguration";
    static final String CSTICS_BY_GROUP = "CSTICSBYGROUP";
    static final String CSTICS_BY_INSTANCE = "CSTICSBYINSTANCE";
    static final String CUSTOMER_PARAMS = "CUSTOMER_PARAMS";
	/**
	 * A list of currently expanded instances, shown on the
	 * CustomizationList.
	 */
	static final String CUSTOMIZATIONLIST_EXPANDED_INSTANCES = "customizationlistInstancesexpanded";//"customizationListExpandedInstances";
    static final String CUSTOMIZATIONLIST_KNOWN_INSTANCES = "customizationlistKnownInstances";
	static final String CONFLICTS = "conflicts";
	static final String DESCRIPTIVE_CONFLICTS = "descriptiveConflicts";
	static final String CONFLICT_TRACE = "conflictTrace";
	static final String CONFLICT_HANDLING_SHORTCUTS = "conflictHandlingShortcuts";
	static final String CONFLICT_VALUE = "conflictValue";
	static final String IPC_HAS_MESSAGES = "ipcHasMessages";
	static final String SOLVED_CONFLICT = "solvedConflict";
	static final String SOLVED_FIRST_CONFLICT = "solvedFirstConflict";
//	static final String SCENARIO_PARAMETERS = "scenarioParameters";
	static final String UINAME_PARAMETERS = "uiNameParameters";
	static final String UINAME = "uiName";
	static final String IPC_XCM_SCENARIO = "ipc_scenario";


    /**
     * UIBean name constants
     */
    static final String CHARACTERISTIC_UIBEAN = "characteristicUIBean";
   /**
    * Tabnames in the multifunctional area.
    */
   static final String CUSTOMIZATION_LIST = "customizationList";
   static final String PRODUCT_VARIANTS = "productVariants";
   static final String DYNAMIC_PICTURE = "dynamicPicture";
   static final String CUSTOMER_TAB = "customerTab";

	/**
	 * Internal parameter indicating whether the user commited or cancelled
	 * his changes in the "settings" page
	 */
	static final String PARAMETER_VALID = "parameterValid";
	/**
	 * Which version has the ipc jsp ui?
	 */
	static final String IPC_UI_VERSION = "ipcUiVersion";
    /**
     * CUSTOM is used to specify the initial character sequence for customer
     * properties. You can specify a property as "custom.XXX" in the web.xml
     * where XXX specifies the property.
     */
    static final String CUSTOM = "custom";
	static final String CHARACTERISTIC_VALUE_BUFFER = "characteristicValueBuffer";
	static final String KNOWLEDGEBASES = "knowledgeBases";
	static final String KNOWLEDGEBASE = "knowledgeBase";
	//parameters for the exception handling
	static final String IPC_EXCEPTION = "ipcException";
	static final String SESSION_TIMEOUT ="sessionTimeout";

    // this parameter tells the setCharacteristicsValuesAction to process the values
    // of a cstic even if there are no values.
    static final String VALIDATE_CSTIC_VALUES = "validateCsticValues";

	static final String XCM_IPC_CONFIGURATION_FILE = "ipc-config";
	static final String XCM_INTERACTION_CONFIG_FILE = "interaction-config";
	static final String DYNAMIC_PRODUCT_PICTURE_ASSIGNED_VALUES = "dynamicProductPictureAssignedValues";


    static final String ACCESSKEY_BUTTONS="accesskey.buttons"; 
    static final String ACCESSKEY_CSTICS="accesskey.cstics";
    static final String ACCESSKEY_GROUPLIST="accesskey.grouplist";
    static final String ACCESSKEY_GROUPS="accesskey.groups";
    static final String ACCESSKEY_INSTANCES="accesskey.instances";
    static final String ACCESSKEY_MFA="accesskey.mfa";
    static final String ACCESSKEY_SEARCH="accesskey.search";
    static final String ACCESSKEY_ACCEPTBUTTON="accesskey.acceptbutton";
    static final String ACCESSKEY_CANCELBUTTON="accesskey.cancelbutton";
    static final String ACCESSKEY_RESETBUTTON="accesskey.resetbutton";
    static final String ACCESSKEY_LASTFOCUSEDCSTIC="accesskey.lastfocused";
    static final String TABORDER_SEARCH="taborder.search";
    static final String TABORDER_CSTICS="taborder.cstics";
    static final String TABORDER_GROUPS="taborder.groups";
    static final String TABORDER_GROUPLIST="taborder.grouplist";
    static final String TABORDER_BUTTONS="taborder.buttons";
    static final String TABORDER_INSTANCES="taborder.instances";
    static final String TABORDER_MFA="taborder.mfa";
    static final String GROUP_CHANGED="groupChanged";
    static final String INSTANCE_CHANGED="instanceChanged";
    static final String FORM_TARGET="ipc";
    
    // constants for import/export
    public static final String IMPORT_FLAG = "importFlag";
    public static final String IMPORT_XCM = "import";
    public static final String IMPORT_EXPORT_XCM = "importexport";
    public static final String EXPORT_XCM = "export";
    public static final String EXPORT_DATA = "exportData";
    public static final String EXPORT_DATA_FILENAME = "exportDataFileName";
    public static final String EXPORT_START = "exportStart";
    public static final String EXPORT_FLAG = "exportFlag";
    
    // constants for search/set functionality
    public static final String SEARCH_SET_FLAG          = "ssFlag";
    public static final String SEARCH_SET_VALUE         = "ssValue";
    public static final String SEARCH_SET_XCM_DISABLED      = "disabled";
    public static final String SEARCH_SET_XCM_CSTIC     = "cstic";
    public static final String SEARCH_SET_XCM_CSTIC_AND_VALUE = "csticvalue";
    public static final String SEARCH_SET_XCM_VALUE = "value";
    public static final String SEARCH_SET_INPUT_RETURN = "ssInputReturn";
    //public static final String SEARCH_SET_INPUT = "ssInput";
    public static final String SEARCH_SET_FOUND_CSTIC   = "foundCstic";
    public static final String SEARCH_SET_FOUND_GROUP   = "foundGroup";
    
    
    // constants for messages
    public static final String MESSAGES      = "messages";
    
    // constants for snapshots
    public static final String SNAPSHOT_INITIAL = "snapshot-initial";
    public static final String SNAPSHOT_STORED  = "snapshot-stored";
    public static final String SNAPSHOT         = "snapshot";
    public static final String SNAPSHOT_ACTION  = "takeSnapshot";    
    
    // constants for configuration comparisons
    public static final String COMPARISON_RESULT = "comparisonResult";
    public static final String COMPARISON_SNAP1 = "comparisonSnap1";    
    public static final String COMPARISON_SNAP2 = "comparisonSnap2";
    public static final String COMPARISON_SHOW_ALL_ASSIGNED = "comparisonShowAll";
    public static final String COMPARISON_FILTER= "comparisonFilter";
    public static final String COMPARISON_ACTION = "comparisonAction";
    public static final String COMPARISON_ACTION_SNAP = "compareToSnapshot";
    public static final String COMPARISON_ACTION_STORED = "compareToStored";
    public static final String LOADING_MESSAGES = "loadingMessages";
    
    // constants needed for the product variant mode
    public static final String PRODUCT_VARIANT_MODE = "productVariantMode";
    public static final String ORIGINAL_DISPLAY_MODE = "originalDisplayMode"; 
       
	// constants for preview page
	public static final String PREVIEW_ACTION_STORED = "previewPage";
	public static final String SWITCH_ADD_INFO       = "switchAddInfo";
	public static final String EXPAND_ADD_INFO       = "expandAddInfo";
	public static final String SWITCH_PROD_VAR       = "switchProdVar";
	public static final String EXPAND_PROD_VAR       = "expandProdVar";
	public static final String SWITCH_CUST_TAB       = "switchCustTab";
	public static final String EXPAND_CUST_TAB       = "expandCustTab";
	// Default behaviour for Additional Info AB (expand or collapse)
	public static final boolean DEFAULT_ADD_INFO       = false;
	public static final boolean DEFAULT_PROD_VAR       = true;
	public static final boolean DEFAULT_CUST_TAB       = true;
    public static final String TECHKEY                 = "ipcTechKey";



}
