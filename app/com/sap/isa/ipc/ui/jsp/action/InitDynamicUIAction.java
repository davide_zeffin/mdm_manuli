package com.sap.isa.ipc.ui.jsp.action;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.sap.isa.businessobject.CommunicationException;
import com.sap.isa.core.TechKey;
import com.sap.isa.core.UserSessionData;
import com.sap.isa.core.businessobject.management.MetaBusinessObjectManager;
import com.sap.isa.core.util.RequestParser;
import com.sap.isa.ipc.ui.jsp.dynamicui.IPCDynamicUIConstant;
import com.sap.isa.ipc.ui.jsp.dynamicui.IPCUIModel;
import com.sap.isa.ipc.ui.jsp.uiclass.UIContext;
import com.sap.isa.maintenanceobject.backend.boi.UIElementGroupData;
import com.sap.isa.maintenanceobject.businessobject.DynamicUIMaintenance;
import com.sap.isa.maintenanceobject.businessobject.DynamicUIMaintenanceAware;

public class InitDynamicUIAction
    extends IPCBaseAction
    implements RequestParameterConstants, ActionForwardConstants {

    /* (non-Javadoc)
     * @see com.sap.isa.isacore.action.EComBaseAction#ecomPerform(org.apache.struts.action.ActionMapping, org.apache.struts.action.ActionForm, javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse, com.sap.isa.core.UserSessionData, com.sap.isa.core.util.RequestParser, com.sap.isa.core.businessobject.management.MetaBusinessObjectManager, boolean, boolean)
     */
    public ActionForward ecomPerform(
        ActionMapping mapping,
        ActionForm form,
        HttpServletRequest request,
        HttpServletResponse response,
        UserSessionData userSessionData,
        RequestParser requestParser,
        MetaBusinessObjectManager mbom,
        boolean multipleInvocation,
        boolean browserBack)
        throws IOException, ServletException, CommunicationException {

			log.entering("ecomPerform()");
			UIContext uiContext = getUIContext(request);
			IPCUIModel ui = (IPCUIModel) uiContext.getProperty(IPCDynamicUIConstant.SESSION_DYNAMIC_UI);
			// initialize if UIModel is null
			if (ui == null){
				// TODO remove dirty hack to get backend object manager!!!!!
		    	DynamicUIMaintenanceAware dynamicUIMaintenanceAware = (DynamicUIMaintenanceAware)mbom.getBOMByType(DynamicUIMaintenanceAware.class);
		        DynamicUIMaintenance dynamicUIMaintenance = dynamicUIMaintenanceAware.getDynamicUIMaintenance();
		        if(dynamicUIMaintenance == null) {
		        	dynamicUIMaintenance = dynamicUIMaintenanceAware.createDynamicUIMaintenance();   
		        }
				
				ui = new IPCUIModel();
				// TODO remove dirty hack to get backend object manager!!!!!
				ui.setBackendObjectManager(dynamicUIMaintenance.getBem());				
				uiContext.setProperty(IPCDynamicUIConstant.SESSION_DYNAMIC_UI, ui);
			}
			
			// set UIModel GUID at UIModel
			String guid = uiContext.getPropertyAsString(UIMODEL_GUID);
			//TODO: check for invalid guid
			if (guid != null && guid.length()>0 && !guid.equals(UIMODEL_GUID_INITIAL)){
				ui.setTechKey(new TechKey(guid));
				ui.read();
				if (ui.getPageList()!= null) {
					if (ui.getPageList().size()>0){
						UIElementGroupData page = (UIElementGroupData)ui.getPageList().get(0);
						if (page != null) {
							ui.setActivePageName(page.getName());
						}
					}	
				}
			}

			// if the ConfigUI is started in Designer-Mode we forward to Designer-Module
			if (uiContext.isDesignerMode()){
				log.exiting();
				return mapping.findForward(ActionForwardConstants.DESIGNER);
			}

			log.exiting();
			return mapping.findForward(ActionForwardConstants.SUCCESS);
    }

}
