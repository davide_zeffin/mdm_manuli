/**
 * Is called if the user clicks on "Display Options".
 * Currently nothing to do here. The parameter
 * InternalRequestParameterConstants.CHARACTERISTIC_STATUS_CHANGE
 * is treated in GetCharacteristicsAction.
 */
package com.sap.isa.ipc.ui.jsp.action;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.sap.isa.core.Constants;
import com.sap.isa.core.UserSessionData;
import com.sap.isa.core.businessobject.management.MetaBusinessObjectManager;
import com.sap.isa.core.util.RequestParser;

/**
 * 
 * @author 
 * No default function. May be used for customer modifications.
 */
public class ExpandCharacteristicAction extends IPCBaseAction {

	public ActionForward ecomPerform(ActionMapping mapping,
		ActionForm form,
		HttpServletRequest request,
		HttpServletResponse response,
		UserSessionData userSessionData,
		RequestParser requestParser,
		MetaBusinessObjectManager mbom,
		boolean multipleInvocation,
		boolean browserBack)
			throws IOException, ServletException {
				
        String characteristicName = request.getParameter(Constants.CURRENT_CHARACTERISTIC_NAME);
        if (!isSet(characteristicName)) {
        	return parameterError(mapping, request, Constants.CURRENT_CHARACTERISTIC_NAME, getClass().getName());
        }
        String instanceId = request.getParameter(Constants.CURRENT_INSTANCE_ID);
        String techKey = request.getParameter(InternalRequestParameterConstants.TECHKEY);
        if (!isSet(techKey)) {
            return parameterError(mapping, request, InternalRequestParameterConstants.TECHKEY, getClass().getName());
        }
        
		customerExit(mapping, form, request, response, userSessionData, requestParser, mbom, multipleInvocation, browserBack);

        changeContextValue(request, Constants.CHARACTERISTIC_STATUS_CHANGE, instanceId + "." + characteristicName + "." + techKey);
        
        return mapping.findForward(ActionForwardConstants.SUCCESS);
        
    }
    
}

