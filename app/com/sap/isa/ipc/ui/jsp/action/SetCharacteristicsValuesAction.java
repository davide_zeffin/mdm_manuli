/************************************************************************

    Copyright (c) 2001 by SAPMarkets Europe GmbH, Germany, SAP AG

    All rights to both implementation and design are reserved.

    Use and copying of this software and preparation of derivative works
    based upon this software are not permitted.

    Distribution of this software is restricted. SAP does not make any
    warranty about the software, its performance or its conformity to any
    specification.

**************************************************************************/

package com.sap.isa.ipc.ui.jsp.action;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Vector;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.sap.isa.businessobject.CommunicationException;
import com.sap.isa.core.Constants;
import com.sap.isa.core.ContextConst;
import com.sap.isa.core.UserSessionData;
import com.sap.isa.core.businessobject.management.MetaBusinessObjectManager;
import com.sap.isa.core.logging.LogUtil;
import com.sap.isa.core.util.RequestParser;
import com.sap.isa.core.util.RequestParser.Parameter;
import com.sap.isa.ipc.ui.jsp.beans.CharacteristicUIBean;
import com.sap.isa.ipc.ui.jsp.beans.MessageUIBean;
import com.sap.isa.ipc.ui.jsp.beans.UIBeanFactory;
import com.sap.isa.ipc.ui.jsp.dynamicui.IPCDynamicUIConstant;
import com.sap.isa.ipc.ui.jsp.dynamicui.IPCProperty;
import com.sap.isa.ipc.ui.jsp.dynamicui.IPCPropertyGroup;
import com.sap.isa.ipc.ui.jsp.dynamicui.IPCUIComponent;
import com.sap.isa.ipc.ui.jsp.dynamicui.IPCUIModel;
import com.sap.isa.ipc.ui.jsp.dynamicui.IPCUIPage;
import com.sap.isa.ipc.ui.jsp.uiclass.UIContext;
import com.sap.isa.ipc.ui.jsp.util.FormatValidator;
import com.sap.isa.maintenanceobject.action.DynamicUIActionHelper;
import com.sap.isa.maintenanceobject.backend.boi.UIElementGroupData;
import com.sap.spc.remote.client.object.Characteristic;
import com.sap.spc.remote.client.object.CharacteristicValue;
import com.sap.spc.remote.client.object.IPCException;
import com.sap.spc.remote.client.object.Instance;
import com.sap.spc.remote.client.object.imp.IPCSetValuesException;


public class SetCharacteristicsValuesAction
    extends IPCBaseAction
    implements RequestParameterConstants, ActionForwardConstants {

	public ActionForward ecomPerform(ActionMapping mapping,
		ActionForm form,
		HttpServletRequest request,
		HttpServletResponse response,
		UserSessionData userSessionData,
		RequestParser requestParser,
		MetaBusinessObjectManager mbom,
		boolean multipleInvocation,
		boolean browserBack)
			throws IOException, ServletException {

		if (!browserBack) {
			UIContext uiContext = getUIContext(request);

			IPCUIModel ui = (IPCUIModel) uiContext.getProperty(IPCDynamicUIConstant.SESSION_DYNAMIC_UI);
			
            if (ui != null){
                ui.clearMessages();
            }
            
			// update the IPCUIModel with the information from the request
			try {
				DynamicUIActionHelper.parseRequest(requestParser, ui);
			}
			catch (CommunicationException e) {
				// Catch communication exceptions and log them
				// log error
				log.error(LogUtil.APPS_COMMON_INFRASTRUCTURE, "exception.communication", e);
				// store exception in context for the error pages
				request.setAttribute(ContextConst.EXCEPTION, e);
				return mapping.findForward(Constants.FORWARD_BACKEND_ERROR);
			}

			// get the pages
			UIElementGroupData pageGroup = ui.getPageGroup();

			// process the ui model and identify values that need to be changed at the config-model
        	ArrayList valuesToSet = determineValuesToSet(ui, pageGroup);

			// pass the values to the config-model
            ActionForward fwd = passValuesToConfigModel(mapping, request, uiContext, valuesToSet);
			
			if (fwd != null){
				// error occured during processing of passValuesToConfigModel()
				// forward accordingly
				return fwd; 
			}
			else {
				// no errors in passValuesToConfigModel(): continue...
			}

			customerExit(mapping, form, request, response, userSessionData, requestParser, mbom, multipleInvocation, browserBack);

			// Now flush the configuration to make sure that everything has been submitted
			// to the server
			try {
				uiContext.getCurrentConfiguration().flush();
			}
			catch (IPCSetValuesException ipcSVEx){
				// create error messages for values that cannot be set at instances
				List messages = (List) uiContext.getProperty(MESSAGES);
				if (messages == null){
					messages = new Vector();    
				}
				HashMap valuesByInstance = ipcSVEx.getValuesByInstance();
				Iterator iter = valuesByInstance.keySet().iterator();
				while (iter.hasNext()){
					String key = (String)iter.next();
					ArrayList valueList = (ArrayList)valuesByInstance.get(key);
					StringBuffer notSetValuesSB = new StringBuffer();
					for (int i=0; i<valueList.size(); i++){
						if (i != 0){
							// don't add the separator if it is the first value
							notSetValuesSB.append("; ");
						}
						notSetValuesSB.append(valueList.get(i));
					}
					String notSetValues = notSetValuesSB.toString();
					String[] args = {key, notSetValues};
					MessageUIBean msg = UIBeanFactory.getInstance().newMessageUIBean("ipc.message.values.not.set", args);
					msg.setMessageStatus(MessageUIBean.ERROR);
					messages.add(msg);                    
				}
				uiContext.setProperty(MESSAGES, messages);
			}

		}
		//  store switch values of additional Info AB in uiContext
		// (there might be a redirect before getAdditionalInfoAction, so otherwise i might be lost) 
		UIContext uiContext = (UIContext)userSessionData.getAttribute(SessionAttributeConstants.UICONTEXT);
		Parameter param;
		param = requestParser.getFromContext(InternalRequestParameterConstants.SWITCH_ADD_INFO);
		uiContext.setProperty(InternalRequestParameterConstants.SWITCH_ADD_INFO, param.getValue().getBoolean());
		param = requestParser.getFromContext(InternalRequestParameterConstants.SWITCH_PROD_VAR);
		uiContext.setProperty(InternalRequestParameterConstants.SWITCH_PROD_VAR, param.getValue().getBoolean());
		param = requestParser.getFromContext(InternalRequestParameterConstants.SWITCH_CUST_TAB);
		uiContext.setProperty(InternalRequestParameterConstants.SWITCH_CUST_TAB, param.getValue().getBoolean());
        
        // handle further processing
		if (isSet(requestParser.getParameter(InternalRequestParameterConstants.SEARCH_SET_FLAG).getValue().getString())) {
			return mapping.findForward(ActionForwardConstants.SEARCH_SET);
		}
		else if (isSet(requestParser.getParameter(InternalRequestParameterConstants.EXPORT_FLAG).getValue().getString())) {
			return mapping.findForward(ActionForwardConstants.EXPORT);
		}        
		else if (isSet(requestParser.getParameter(InternalRequestParameterConstants.COMPARISON_ACTION_SNAP).getValue().getString())) {
			return mapping.findForward(ActionForwardConstants.COMPARE_TO_SNAP);
		}        
		else if (isSet(requestParser.getParameter(InternalRequestParameterConstants.COMPARISON_ACTION_STORED).getValue().getString())) {
			return mapping.findForward(ActionForwardConstants.COMPARE_TO_STORED);
		}        
		else if (isSet(requestParser.getParameter(InternalRequestParameterConstants.SNAPSHOT_ACTION).getValue().getString())) {
			return mapping.findForward(ActionForwardConstants.TAKE_SNAPSHOT);
		}
		else if (isSet(requestParser.getParameter(InternalRequestParameterConstants.PREVIEW_ACTION_STORED).getValue().getString())) {
			return mapping.findForward(ActionForwardConstants.SHOW_PREVIEW);        
		}        
		else {
			return mapping.findForward(ActionForwardConstants.SUCCESS);
		}
	}

    private ActionForward passValuesToConfigModel(
        ActionMapping mapping,
        HttpServletRequest request,
        UIContext uiContext,
        ArrayList valuesToSet) {
        ActionForward forward = null;
        
		boolean showInvisibleCstics = uiContext.getPropertyAsBoolean(SHOW_INVISIBLE_CHARACTERISTICS, false);
		boolean showMessageCstics = uiContext.getPropertyAsBoolean(ProcessRequestParameterAction.SHOW_MESSAGE_CSTICS);
		String prefix = uiContext.getPropertyAsString(ProcessRequestParameterAction.MESSAGE_CSTICS_PREFIX);

        for (int i=0; i<valuesToSet.size(); i++){
        	ArrayList currentEntry = (ArrayList)valuesToSet.get(i);
        	// get the csticName
        	String csticName = (String)currentEntry.get(0);
        	// get the instance the cstic belongs to
        	String currentInstId = (String)currentEntry.get(3);
        	// get the instance BO
        	Instance currentInstance = uiContext.getCurrentConfiguration().getInstance(currentInstId);
        	if (currentInstance == null) {
        		return parameterError(
        			mapping,
        			request,
        			Constants.CURRENT_INSTANCE_ID,
        			this.getClass().getName());
        	}
        	// get the cstic BO
        	Characteristic cstic = currentInstance.getCharacteristic(csticName,	showInvisibleCstics);
        	if (cstic == null) {
        		setExtendedRequestAttribute(request,
        			IPC_EXCEPTION,
        			new IPCException(
        				"characteristic " + csticName + " not found!"));
        		return mapping.findForward(INTERNAL_ERROR);
        	}
        	// get the values
        	String[] charValueNames = (String[])currentEntry.get(1);
        	// different handling if the cstic has an input-field
        	String uiType = (String) currentEntry.get(2);
        	if (IPCProperty.uiTypeHasOnlyInputField(uiType)){
        		// handling for input value
                handleInputValues(
                    request,
                    uiContext,
                    showMessageCstics,
                    prefix,
                    csticName,
                    cstic,
                    charValueNames);
        	}
        	else if (IPCProperty.uiTypeHasInputAndSelection(uiType)){
        		// if the property has selection list and input field both have to be checked
        		handleValues(cstic, charValueNames);
        		handleInputValues(
        			request,
        			uiContext,
        			showMessageCstics,
        			prefix,
        			csticName,
        			cstic,
        			charValueNames);
        	}
        	else {
        		// handling for non-input values
                handleValues(cstic, charValueNames);
        	}
        }
        return forward;
    }

    /**
     * This method returns an ArrayList that contains an ArrayList for each cstic for which values need to 
     * be updated in the config-model (the decision whether the value really needs to be set at the 
     * cfg-engine is done in the client-object-layer).
	 * The ArrayList of a cstic contains 
	 * <li>the property (cstic) name (at position 0)
	 * <li>an array of values that need to be set (at position 1)
	 * <li>the uiType of the property (at position 2): needed for handling of values from input-fields
	 * <li>the component (instance) id to which the property belongs to (at position 3)
     * @param ui IPCUIModel
     * @param pageGroup list of pages
     * @return
     */
    private ArrayList determineValuesToSet(IPCUIModel ui, UIElementGroupData pageGroup) {
		ArrayList valuesToSet = new ArrayList();
        Iterator pages = pageGroup.iterator();
        while (pages.hasNext()){
			IPCUIPage page = (IPCUIPage)pages.next();
			Iterator components = page.iterator();
            while (components.hasNext()){
        		IPCUIComponent component = (IPCUIComponent)components.next();
        	    Iterator groups = component.iterator();
        	    while(groups.hasNext()){
        		    IPCPropertyGroup group = (IPCPropertyGroup)groups.next();
        		    Iterator properties = group.iterator();
        		    while (properties.hasNext()){
        			    IPCProperty property = (IPCProperty)properties.next();
        			    // Handle the property only if it can be changed and the value has changed.
                        // Furthermore if the value was erroneous before we need to send it again to 
                        // the engine even if the value has not changed. Otherwise the erroneous value
                        // would vanish magically.
        			    if (!property.isReadOnly() && (property.isValueChanged() || property.getHasErroneousValue()) ){
                            // The value needs to be sent to the engine. Therefore we need to clear 
                            // the "formattedString" (used for input-fields).
                            // It will be set in the MergeVisitor with the changed value.
                            property.setFormattedString(null);
        			 	    String[] vals = null;
        				    // prop is single-valued
        				    if (!property.allowsMultipleValues()) {
        						String val = property.getString();
        					    vals = new String[1];
        					    vals[0] = val;								
        				    }
        				    // prop is multi-valued
        				    else {
        						ArrayList values = (ArrayList)property.getList();
        					    vals = new String[1];
        					    // Use toArray() method only if size of ArrayList is > 0. 
        					    // Otherwise vals would be filled with "null" which leads to exception during setting
        					    // of values
        					    if (values.size()>0){
        							vals = (String[])values.toArray(vals);
        					   	}
        					    else {
        							vals[0] = "";
        					    }
        				    }
        				    ArrayList entry = new ArrayList();
        				    entry.add(property.getName());
        				    entry.add(vals);
        				    entry.add(property.getUiType());
        				    entry.add(component.getName());  
        				    valuesToSet.add(entry);
        				    // finally we clear the changed-flag
        				    property.setValueChanged(false);
        			    }
        		    }
        	    }
            }
        }
        return valuesToSet;
    }
	
    private void handleInputValues(
        HttpServletRequest request,
        UIContext uiContext,
        boolean showMessageCstics,
        String prefix,
        String csticName,
        Characteristic cstic,
        String[] charValueNames) {
			
		for (int j=0; j<charValueNames.length; j++){
			String inputValue = charValueNames[j];
			CharacteristicUIBean csticUIBean;
			if (showMessageCstics && cstic.getName().startsWith(prefix)){
				csticUIBean = CharacteristicUIBean.getInstance(request, cstic, uiContext, true);
			} 
			else {
				csticUIBean = CharacteristicUIBean.getInstance(request, cstic, uiContext);
			}

			//handle freetext input Values
			if (!inputValue.equals("")) {
				FormatValidator.FormatError validationError =
					FormatValidator.validate(
						cstic,
						inputValue,
						uiContext.getLocale());
				if (!validationError.isOk()) {
					csticUIBean.setValidationError(validationError);
					csticUIBean.setErroneousValue(inputValue);
				}
			}
        
			 if (inputValue!=null) { 
	               if (csticUIBean.getValidationError().isOk()) {
                     // dependent whether the value is already existing it will be added to the list and assigned or just assigned
                     /*if (inputValue.length()==0 && cstic.allowsMultipleValues() && cstic.hasAssignedValues()) {
                     }
                     else {
					   addOrAssignValue(uiContext, inputValue, cstic);
	                   }*/
	            	   
	            	   /* Modified the code above to fix multiple value cstics
	            	    * with free text input
	            	    */
	            	   if (inputValue.length() != 0) {
	            		   //if ((cstic.allowsMultipleValues() && !cstic.hasAssignedValues()) || (!cstic.allowsMultipleValues())) {
								addOrAssignValue(uiContext, inputValue, cstic);				
						}else{ 
							//user presses backspace for single value free text input, blank value should be set
							//No assignment for single value with additional value
							//Blank value assignment should only happen for single value cstic with free text input
							if(!(!cstic.allowsMultipleValues() && cstic.allowsAdditionalValues())){
								if ((cstic.allowsMultipleValues() && !cstic.hasAssignedValues()) || (!cstic.allowsMultipleValues() && cstic.hasAssignedValues())) {
									addOrAssignValue(uiContext, inputValue, cstic);
								}
							}
						}
	                 
	               } else {
	                   log.debug("Value "
	                        + inputValue
	                        + " for characteristic "
	                        + csticName
	                        + " had format errors. Not set.");
	               }
			    }  
              }
           }
    
    private void handleValues(Characteristic cstic, String[] charValueNames) {
        boolean result = cstic.setValues(charValueNames);
        if (result == false) {
        	StringBuffer errormessage = new StringBuffer();
        	errormessage.append("Could not set characteristic values:");
        	for (int valueindex = 0; valueindex < charValueNames.length; valueindex++) {
        		errormessage.append(charValueNames[valueindex]);
        		if (valueindex < charValueNames.length - 1) {
        			errormessage.append("; ");
        		}
        	}
        	errormessage.append("for characteristic:" + cstic.getName());
        	log.error(errormessage.toString());
        }
    }
    
    
    /**
     * Helper Method to decide whether a value, that was entered by the user has to be added 
     * or has to be assigned.
     * In case that the value is already existing in the list of values it will be assigned.
     * In case the value is not existing it will be added to the list of values and assigned.
     * @param uiContext 
     * @param inputValue The value the user has entered.
     * @param cstic The characteristic to which the value should be added.
     */
    private void addOrAssignValue(UIContext uiContext, String inputValue, Characteristic cstic) {
        String internalInputValue = inputValue;
        CharacteristicValue valueInList = null;
        if ((cstic.getValueType() == Characteristic.TYPE_INTEGER) || (cstic.getValueType() == Characteristic.TYPE_FLOAT)){
            // if it is a numerical cstic we have to convert the inputValue to the 
            // internal representation (in order that the check for existance work properly).
            internalInputValue = FormatValidator.convertNumericValueToIndependent(cstic, inputValue, uiContext.getLocale());
            // check for existance (using internal representation)
            valueInList = cstic.getValue(internalInputValue);
        }
        // dependent on getShowLanguageDependentNames() we check for the existance of the language-dependent name or the internal name
        else {
            if (uiContext.getShowLanguageDependentNames()) {
                // we assume the user enters the value language-dependent (if he wants to set it)
                valueInList = cstic.getValueFromLanguageDependentName(inputValue);    
            }
            else {
                // if the language independent names are shown, the user has to enter the internal name
                valueInList = cstic.getValue(inputValue);
            }
        }
        if (valueInList == null) {
            // value is not existing, add value and assign it (inside the addValue-method)
            cstic.addValue(inputValue);    
        }
        else {
            // value is existing, assign it
            cstic.assignValue(valueInList);
        }
    }
}
