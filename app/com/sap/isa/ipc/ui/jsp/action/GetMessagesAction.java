/**
 *  GetMessagesAction returns the configuration messages
 */
package com.sap.isa.ipc.ui.jsp.action;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.util.MessageResources;

import sun.rmi.transport.Connection;

import com.sap.isa.core.UserSessionData;
import com.sap.isa.core.businessobject.management.MetaBusinessObjectManager;
import com.sap.isa.core.util.RequestParser;
import com.sap.isa.ipc.ui.jsp.beans.ConfigUIBean;
import com.sap.isa.ipc.ui.jsp.beans.ConflictHandlingShortcutUIBean;
import com.sap.isa.ipc.ui.jsp.beans.MessageUIBean;
import com.sap.isa.ipc.ui.jsp.beans.UIBeanFactory;
import com.sap.isa.ipc.ui.jsp.constants.MessagesRequestParameterConstants;
import com.sap.isa.ipc.ui.jsp.uiclass.UIContext;
import com.sap.spc.remote.client.object.Configuration;
import com.sap.spc.remote.client.object.ConflictHandlingShortcut;
import com.sap.spc.remote.client.object.DescriptiveConflict;
import com.sap.spc.remote.client.object.DescriptiveConflictType;
import com.sap.spc.remote.client.object.ExplanationTool;

/**
 * Has currently no function except to enable the component /components/message.jsp
 * to be displayed independently from other components.
 * If the other actions did not place any messages, it creates an empty container for
 * messages an places it into the uiContext.
 * @author 
 *
 * To change the template for this generated type comment go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
public class GetMessagesAction
	extends IPCBaseAction
	implements RequestParameterConstants, ActionForwardConstants {

	public ActionForward ecomPerform(ActionMapping mapping,
		ActionForm form,
		HttpServletRequest request,
		HttpServletResponse response,
		UserSessionData userSessionData,
		RequestParser requestParser,
		MetaBusinessObjectManager mbom,
		boolean multipleInvocation,
		boolean browserBack)
			throws IOException, ServletException {

		UIContext uiContext =
			getUIContext(request);
		ConfigUIBean currentConfiguration =
			this.getCurrentConfiguration(uiContext);
	    
        //check if other actions (e.g. SearchSetAction) have set messages already as request attribute
	    Object messagesObj = request.getAttribute(MessagesRequestParameterConstants.MESSAGES);
		List messages;
	    if (messagesObj == null) {
			messages = new ArrayList();
	    }else {
	    	messages = (List)messagesObj;
	    }
        
        //check if other action (e.g. ImportConfigurationAction) have set messages already in the uiContext
        Object uiContextMessagesObj = uiContext.getProperty(InternalRequestParameterConstants.MESSAGES);
        if (uiContextMessagesObj != null) {
            Iterator uiContextMessages = ((List)uiContextMessagesObj).iterator();
            while (uiContextMessages.hasNext()) {
                messages.add(uiContextMessages.next());
            }
            // delete the message of the uiContext (otherwise they will show up every round-time)
            uiContext.removeProperty(InternalRequestParameterConstants.MESSAGES);
        }
                
		//check for conflict value messages
		if (uiContext.getPropertyAsBoolean(USE_EXPLANATION_TOOL)
			&& uiContext.getPropertyAsBoolean(USE_CONFLICT_HANDLING_SHORTCUTS)
			&& uiContext.getPropertyAsBoolean(USE_SHORTCUT_VALUE_CONFLICT)) {
			boolean consistent = currentConfiguration.isConsistent();
			if (!consistent) {
				ConflictHandlingShortcut conflictShortcut =
					getShortcut(uiContext, this.getResources(request));
				if (conflictShortcut != null) {
					ConflictHandlingShortcutUIBean conflictShortcutUIBean =
					UIBeanFactory.getUIBeanFactory().newConflictHandlingShortcutUIBean(
							conflictShortcut,
							uiContext);
					messages.add(conflictShortcutUIBean);
				}else { //the conflict does not have a shortcut
					MessageUIBean conflictMessage = UIBeanFactory.getUIBeanFactory().newMessageUIBean();
					conflictMessage.setMessageStatus(MessageUIBean.ERROR);
					conflictMessage.setMessageTranslateKey("ipc.config.inconsistent");
					String actionUrl = "/ipc/showConflictDetails.do"; 
					conflictMessage.addAction("ipc.conflict.tt.handle", "javascript:openConflictSolver", actionUrl);
					messages.add(conflictMessage);
				}
			}
		}
		
		customerExit(mapping, form, request, response, userSessionData, requestParser, mbom, multipleInvocation, browserBack);
		//TODO Add other messages here (standard conflict messages, search/set messages).

		request.setAttribute(
			MessagesRequestParameterConstants.MESSAGES,
			messages);

		return mapping.findForward(SUCCESS);
	}

	private ConflictHandlingShortcut getShortcut(
		UIContext uiContext,
		MessageResources resources) {

		ExplanationTool explanationTool =
			uiContext.getCurrentConfiguration().getExplanationTool();
		List descriptiveConflicts =
			explanationTool
				.getDescriptiveConflicts(
					uiContext.getConflictExplanationLevel(),
					uiContext.getConflictExplanationTextBlockTag(),
					uiContext.getConflictExplanationTextLineTag(),
					uiContext.showConflictSolutionRate(),
					uiContext.getLocale(),
					resources,
			//TODO        uiContext.getApplication(),
		uiContext.useConflictHandlingShortcuts(),
			uiContext.useValueConflictShortcut(),
			uiContext.useExplanationTool());

		if (uiContext
			.getPropertyAsBoolean(
				RequestParameterConstants.USE_SHORTCUT_VALUE_CONFLICT)
			&& (descriptiveConflicts.size() == 1)) {
			DescriptiveConflict descriptiveConflict =
				(DescriptiveConflict) descriptiveConflicts.get(0);
			if (!descriptiveConflict.hasShortcut()
				|| (descriptiveConflict
					.getConflictTypes(
						uiContext.showConflictSolutionRate(),
						uiContext.getLocale(),
						resources,
			//TODO              uiContext.getApplication(),
			uiContext
					.getConflictExplanationTextLineTag(),
						uiContext.useConflictHandlingShortcuts(),
						uiContext.useValueConflictShortcut(),
						uiContext.useExplanationTool())
					.size()
					!= 1))
				return null;
			DescriptiveConflictType conflictType =
				(DescriptiveConflictType) descriptiveConflict.getConflictTypes(
							uiContext.showConflictSolutionRate(),
							uiContext.getLocale(),
							resources,
							uiContext.getConflictExplanationTextLineTag(),
							uiContext.useConflictHandlingShortcuts(),
							uiContext.useValueConflictShortcut(),
							uiContext.useExplanationTool()
				).get(0);
			
			if (conflictType.getNumberOfMembers() == 2
				&& conflictType.getType().equals("conflict_value")) {
				return conflictType.getShortcut();
			}
		}
		return null;
	}
    
    /**
     * Determines whether to display the messages area or not.
     * @return true if messages area should be displayed
     */
    public static boolean displayMessagesArea(HttpServletRequest request, UIContext uiContext, Configuration currentConfiguration){
        boolean displayArea = false;
        //check if other actions (e.g. SearchSetAction) have set messages already as request attribute
        Object messagesObj = request.getAttribute(MessagesRequestParameterConstants.MESSAGES);
        if (messagesObj != null) {
            displayArea = true;
        }
        //check if other action (e.g. ImportConfigurationAction) have set messages already in the uiContext
        Object uiContextMessagesObj = uiContext.getProperty(InternalRequestParameterConstants.MESSAGES);
        if (uiContextMessagesObj != null) {
            displayArea = true;
        }
        //check for conflict value messages
        if (uiContext.getPropertyAsBoolean(USE_EXPLANATION_TOOL)
            && uiContext.getPropertyAsBoolean(USE_CONFLICT_HANDLING_SHORTCUTS)
            && uiContext.getPropertyAsBoolean(USE_SHORTCUT_VALUE_CONFLICT)) {
            boolean consistent = currentConfiguration.isConsistent();
            if (!consistent) {
                displayArea = true;
            }
        }
        return displayArea;
    }
}