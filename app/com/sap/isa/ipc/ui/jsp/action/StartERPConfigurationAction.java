package com.sap.isa.ipc.ui.jsp.action;

import java.io.IOException;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.sap.isa.businessobject.ipc.IPCBOManager;
import com.sap.isa.core.UserSessionData;
import com.sap.isa.core.businessobject.management.MetaBusinessObjectManager;
import com.sap.isa.core.util.RequestParser;
import com.sap.isa.core.util.RequestParser.Parameter;
import com.sap.isa.ipc.ui.jsp.uiclass.UIContext;
import com.sap.spc.remote.client.object.Configuration;
import com.sap.spc.remote.client.object.IPCClient;
import com.sap.spc.remote.client.object.IPCDocument;
import com.sap.spc.remote.client.object.IPCDocumentProperties;
import com.sap.spc.remote.client.object.IPCException;
import com.sap.spc.remote.client.object.IPCItem;
import com.sap.spc.remote.client.object.IPCItemProperties;
import com.sap.spc.remote.client.object.IPCItemReference;
import com.sap.spc.remote.client.object.OrderConfiguration;
import com.sap.spc.remote.client.object.OriginalRequestParameterConstants;
import com.sap.spc.remote.client.object.imp.DimensionalValue;
import com.sap.spc.remote.client.object.imp.rfc.DefaultIPCR3DocumentProperties;
import com.sap.spc.remote.client.util.ext_configuration;

public class StartERPConfigurationAction extends StartConfigurationAction {

	public ActionForward ecomPerform(
		ActionMapping mapping,
		ActionForm form,
		HttpServletRequest request,
		HttpServletResponse response,
		UserSessionData userSessionData,
		RequestParser requestParser,
		MetaBusinessObjectManager mbom,
		boolean multipleInvocation,
		boolean browserBack)
		throws IOException, ServletException {


		//We have to decide whether we are in 'changeable product variant mode' or not. 
		//ecomPerfom will set this flag according to the IPC instance, so we have to 
		//re-set this attribute afterwards (from a request parameter)
			
		//We need the IPCItem as container for the 'changeable product variant mode' because
		//ECO uses the IPCItem as transfer mechanism	
			
		//Request parameter have priority over request attributes
		
		//Currently an ERP calling application will not set this parameter. It is used 
		//for testing only
		String requestConstant = OriginalRequestParameterConstants.CHANGEABLE_PRODUCT_VARIANT_MODE;
		String requestParameter = "";
		if (request.getParameter(requestConstant) != null
			&& !request.getParameter(requestConstant).equals("")) {
			requestParameter = request.getParameter(requestConstant);
		}else if (request.getAttribute(requestConstant) != null) {
			requestParameter = (String) request.getAttribute(requestConstant);
			}
			
		boolean changeableProductVariant = requestParameter.equals("T");
			
		UIContext uiContext = IPCBaseAction.getUIContext(request);
		if (uiContext == null) { //the uiContext has not been initialized
		ActionForward forward = processScenarioParameter(mapping, request);
		if (!forward.getName().equals(ActionForwardConstants.SUCCESS)) return forward;
			uiContext = IPCBaseAction.getUIContext(request); //uiContext created in processScenarioParameter
		}
		if (uiContext != null) { 			  
		 uiContext.setChangeableProductVariantMode(changeableProductVariant);
		}		
		Parameter callerParam = requestParser.getParameter(CALLER);
		if (!callerParam.isSet()) return parameterError(mapping, request, RequestParameterConstants.CALLER, getClass().getName());
		uiContext.setCaller(callerParam.getValue().getString());
			
		IPCBOManager ipcBoManager =
			(IPCBOManager) userSessionData.getBOM(IPCBOManager.NAME);
		if (ipcBoManager == null) {
			log.error("No IPC BOM in session data!");
			return (mapping.findForward(INTERNAL_ERROR));
		}

        IPCDocumentProperties docProp = getDocumentProperties(request);

        if (uiContext.isPriceAnalysisEnabled()) {
			docProp.setPerformPricingAnalysis(true);
        }

        IPCClient ipcClient =
			ipcBoManager.createIPCClient(docProp.getLanguage());

		// create document+item if
		IPCDocument ipcDocument = ipcClient.createIPCDocument(docProp);
		if (ipcDocument == null) {
			IPCException ipcException =
				new IPCException("IPCDocument == null in StartConfigurationAction!");
			setExtendedRequestAttribute(request, IPC_EXCEPTION, ipcException);
			log.error(this, ipcException);
			return mapping.findForward(INTERNAL_ERROR);
		}

		IPCItemProperties itemProp = getItemProperties(request);
		IPCItem ipcItem = ipcClient.createIPCItem(ipcDocument.getId(), itemProp);
		if (ipcItem == null) {
			IPCException ipcException =
				new IPCException("IPCItem == null in StartConfigurationAction!");
			setExtendedRequestAttribute(request, IPC_EXCEPTION, ipcException);
			log.error(this, ipcException);
			return mapping.findForward(INTERNAL_ERROR);
		}
        
        // If item is product variant set the productVariant mode to true.
        setProductVariantMode(uiContext, ipcItem);

		String kbDate = request.getParameter(KB_DATE);
		// mapping table between guids and instances available?
		HashMap posToInst = makeHashMap(request, POS_POS_GUID, POS_INST_ID);
		// external Configuration
		ext_configuration extConfig = getExternalConfiguration(request);
		if (extConfig != null)
			ipcItem.setConfig(
				new OrderConfiguration(extConfig, posToInst, kbDate));

		// context information
		String[] contextNames = getParameters(request, CONTEXT_NAME);
		String[] contextValues = getParameters(request, CONTEXT_VALUE);
		Hashtable context = getHashtable(contextNames, contextValues);
		if (context != null)
			ipcItem.setContext(context);

		// additional customizing parameters
		// - subItemsAllowed
		String subItemsAllowed = uiContext.getPropertyAsString(SUB_ITEMS_ALLOWED);

		if (isSet(subItemsAllowed)) {
            boolean flag = false;
            if (subItemsAllowed.equals("Y")) {
                flag = true;
            }
            ipcItem.setSubItemsAllowed(flag);
		}

		Configuration configuration = ipcItem.getConfiguration();


		customerExit(mapping, form, request, response, userSessionData, requestParser, mbom, multipleInvocation, browserBack);

		// get used itemReference and set documentId and itemId
		// -> used f.e. in ReturnConfigurationStandaloneAction
		IPCItemReference itemReference = ipcClient.getItemReference();
		itemReference.setDocumentId(ipcDocument.getId());
		itemReference.setItemId(ipcItem.getItemId());
		
        uiContext.setItemReference(itemReference);
		uiContext.setCurrentConfiguration(configuration);
		//initialize the context
		setInitialContextValues(request, configuration);
		uiContext.setCaller(VEHICLE_MANAGER);
		
		return mapping.findForward(ActionForwardConstants.SUCCESS);
	}

    /*
     * fill document properties from the request into an IPCDocumentProperties object
     * @return a IPCDocumentProperties
     */
    protected IPCDocumentProperties getDocumentProperties(HttpServletRequest request) {
        IPCDocumentProperties p = new DefaultIPCR3DocumentProperties();

        p.setLanguage(request.getParameter(LANGUAGE));
        p.setCountry(request.getParameter(COUNTRY));
        p.setDepartureCountry(request.getParameter(DEPARTURE_COUNTRY));
        p.setDistributionChannel(request.getParameter(DISTRIBUTION_CHANNEL));
        p.setDocumentCurrency(request.getParameter(DOCUMENT_CURRENCY_UNIT));
        p.setLocalCurrency(request.getParameter(LOCAL_CURRENCY_UNIT));
        p.setSalesOrganisation(request.getParameter(SALES_ORGANISATION));
        p.setDistributionChannel(request.getParameter(DISTRIBUTION_CHANNEL));
        p.setPricingProcedure(request.getParameter(PROCEDURE_NAME));

        String pricingUsage = request.getParameter(USAGE);
        if (isSet(pricingUsage)){
            p.setUsage(pricingUsage);
        }

        String pricingApplication = request.getParameter(PRICING_APPLICATION);
        if (isSet(pricingApplication)){
            p.setApplication(pricingApplication);
        }

        // pricing issue:
        // setting of further document attributes
        HashMap docAttributes = p.getHeaderAttributes();
        Enumeration parameterNames = request.getParameterNames();
        while (parameterNames.hasMoreElements()) {
            String nextParam = (String) parameterNames.nextElement();
            if (nextParam.startsWith(GENERIC_DOC_ATTR)) {
            String value = request.getParameter(nextParam);
				if (value != null) {
            // extract attribute name
					String name =
                nextParam.substring(
                    GENERIC_DOC_ATTR.length(),
                    nextParam.length());
            p.addCreationCommandParameter(name, value);
        }
            }
			if (nextParam.startsWith(OriginalRequestParameterConstants.PRC_HDR_ATTR)) {
				String value = request.getParameter(nextParam);
				if (value != null) {
					// extract attribute name
					String name =
						nextParam.substring(
							OriginalRequestParameterConstants
								.PRC_HDR_ATTR
								.length(),
							nextParam.length());
					docAttributes.put(name, value);
				}
			}
        }
        p.setHeaderAttributes(docAttributes);
        return p;
    }

    /*
     * fill item properties from the request into an IPCItemProperties object
     * @return a IPCItemProperties
     */
    protected IPCItemProperties getItemProperties(HttpServletRequest request) {
        IPCItemProperties p = factory.newIPCItemProperties();
        HashMap itemAttributes = new HashMap();
        try {

            // the request parameter "productIdERP" is used in ERP to submit the productId
            String productIdERP = request.getParameter(PRODUCT_ID_ERP);
            p.setProductId(productIdERP);
            
            itemAttributes.put("PMATN", productIdERP);
            
//              IPCBaseAction.getParameterAttributeOrScenarioParameter(
//                  request,
//                  PRODUCT_ID,
//                  ""));
			String baseQuantityValue = request.getParameter(BASE_QUANTITY_VALUE);
			String baseQuantityUnit = request.getParameter(BASE_QUANTITY_UNIT);

            String salesQuantityValue = request.getParameter(SALES_QUANTITY_VALUE);
            String salesQuantityUnit = request.getParameter(SALES_QUANTITY_UNIT);

			if (isSet(baseQuantityValue)
				&& isSet(baseQuantityUnit)) {
				p.setBaseQuantity(
					new DimensionalValue(
						baseQuantityValue,
						baseQuantityUnit));
			}

            if (isSet(salesQuantityValue)
                && isSet(salesQuantityUnit)) {
                p.setSalesQuantity(
                    new DimensionalValue(
                        salesQuantityValue,
                        salesQuantityUnit));
            }
            
            String productGuid = request.getParameter(PRODUCT_GUID);
            if (isSet(productGuid)){
                p.setProductGuid(productGuid);
            }
            
            String pricingDate = request.getParameter(PRICING_DATE);
            if (isSet(pricingDate)) {
                    p.setDate(pricingDate);
            }else {
                log.warn("Parameter " + PRICING_DATE + " not passed!"); 
            }
            
			String pricingRelevant = request.getParameter(PRICING_RELEVANT);
			if (isSet(pricingRelevant)){
				p.setPricingRelevant(pricingRelevant.equals("T") ? Boolean.TRUE : Boolean.FALSE);
				itemAttributes.put("PRSFD", (pricingRelevant.equals("T") ? "X": ""));
			}else {
				itemAttributes.put("PRSFD", "X");				
			}
			
			String performPricingAnanlysis = request.getParameter(PERFORM_PRICING_ANALYSIS);
			if (isSet(performPricingAnanlysis)){
				p.setPerformPricingAnalysis(performPricingAnanlysis.equals("T") ? true : false);
			}

//              IPCBaseAction.getParameterAttributeOrScenarioParameter(
//                  request,
//                  PRICING_DATE,
//                  ""));

            String kbLogsys = request.getParameter(KB_LOGSYS);
            if (isSet(kbLogsys)) {
                p.setKbLogSys(kbLogsys);
            }
            String kbName = request.getParameter(KB_NAME);
            if (isSet(kbName)) {
                p.setKbName(kbName);
            }
            String kbVersion = request.getParameter(KB_VERSION);
            if (isSet(kbVersion)) {
                p.setKbVersion(kbVersion);
            }
            String kbProfile = request.getParameter(KB_PROFILE);
            if (isSet(kbProfile)) {
                p.setKbProfile(kbProfile);
            }

            // pricing issue:
            // setting of header and item attributes AND
            // setting of further pricing/taxing attributes
            Enumeration parameterNames = request.getParameterNames();
            Map headerAttributes = new HashMap();
            while (parameterNames.hasMoreElements()) {
                String nextParam = (String) parameterNames.nextElement();
                if (!nextParam.startsWith(PRC_HDR_ATTR)
                    && !nextParam.startsWith(PRC_ITM_ATTR)
                    && !nextParam.startsWith(GENERIC_ITM_ATTR))
                    continue;
                String value = request.getParameter(nextParam);
                // get attribute value
                if (value == null)
                    continue;
                // extract attribute name
                String name = null;
                // header
                if (nextParam.startsWith(PRC_HDR_ATTR)) {
                    name =
                        nextParam.substring(
                            PRC_HDR_ATTR.length(),
                            nextParam.length());
                    headerAttributes.put(name, value);
                    // item
                } else if (nextParam.startsWith(PRC_ITM_ATTR)) {
                    name =
                        nextParam.substring(
                            PRC_ITM_ATTR.length(),
                            nextParam.length());
                    itemAttributes.put(name, value);
                    // generic item attribute
                } else if (nextParam.startsWith(GENERIC_ITM_ATTR)) {
                    name =
                        nextParam.substring(
                            GENERIC_ITM_ATTR.length(),
                            nextParam.length());
                    p.addCreationCommandParameter(name, value);
                }
            }
            if (headerAttributes.size() > 0)
                p.setHeaderAttributes(headerAttributes);
            if (itemAttributes.size() > 0)
                p.setItemAttributes(itemAttributes);

        } catch (IPCException e) {
            log.fatal(this, e);
            setExtendedRequestAttribute(request, IPC_EXCEPTION, e);
            return null;
        }
        return p;
    }

}
