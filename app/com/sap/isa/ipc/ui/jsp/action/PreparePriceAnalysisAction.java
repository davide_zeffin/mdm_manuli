/*****************************************************************************
    Class         PrepareIsaPriceAnalysisAction
    Copyright (c) 2000, SAP AG, Germany, All rights reserved.
    Description:  Action to prepare the display of the price analysis in ISA
    Author:       SAP
    Created:      October 2005
    Version:      1.0

    $Revision: #1 $
    $Date: 2005/10/05 $
*****************************************************************************/
package com.sap.isa.ipc.ui.jsp.action;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.sap.isa.core.UserSessionData;
import com.sap.isa.core.businessobject.management.MetaBusinessObjectManager;
import com.sap.isa.core.util.RequestParser;
import com.sap.isa.ipc.ui.jsp.uiclass.UIContext;
import com.sap.spc.remote.client.object.Configuration;
import com.sap.spc.remote.client.object.IPCItem;
import com.sap.spc.remote.client.object.IPCItemReference;
import com.sap.spc.remote.client.object.Instance;

/**
 * Class to initialize the price analysis in the E-selling application.
 * The <code>isaPerform</code> method uses the 
 *   - connectionKey : key of the IPC connection,
 *   - IPC-documentId : ID of the IPC pricing document,
 *   - IPC-itemId : ID of the IPC prcing item
 * to instanciate a new IPCItemReference object.   
 *
 * @author SAP
 * @version 1.0
 */
public class PreparePriceAnalysisAction extends IPCBaseAction {

	/**
	 * Implement this method to add functionality to your action.
	 *
	 * @param mapping           The <code>ActionMapping</code> passed by struts
	 * @param form              The <code>FormBean</code> specified in the
	 *                          config.xml file for this action
	 * @param request           The request object
	 * @param response          The response object
	 * @param userSessionData   Object wrapping the session
	 * @param requestParser     Parser to simple retrieve data from the request
	 * @param bom               Reference to the BusinessObjectManager
	 * @param log               Reference to the IsaLocation, needed for logging
	 * @return Forward to another action or page
	 */
	public ActionForward ecomPerform(
		ActionMapping mapping,
		ActionForm form,
		HttpServletRequest request,
		HttpServletResponse response,
		UserSessionData userSessionData,
		RequestParser requestParser,
		MetaBusinessObjectManager mbom,
		boolean multipleInvocation,
		boolean browserBack)
		throws IOException, ServletException {

		final String METHOD_NAME = "PreparePriceAnalysisAction.ecomPerform";
		log.entering(METHOD_NAME);
		String mode = "";
		
		UIContext uiContext = getUIContext(request);
		Configuration currentConfiguration = uiContext.getCurrentConfiguration();
		if (currentConfiguration == null) return mapping.findForward(ActionForwardConstants.INPUT_PARAMETER_ERROR); 
		Instance instance = currentConfiguration.getRootInstance();
		if (instance == null) return mapping.findForward(ActionForwardConstants.INPUT_PARAMETER_ERROR);
		IPCItem item = instance.getIpcItem();
		if (item == null) return mapping.findForward(ActionForwardConstants.INPUT_PARAMETER_ERROR);
		IPCItemReference ipcItemReference = item.getItemReference();

		// update the request: add the ipcItemReference
		request.setAttribute(RequestParameterConstants.IPC_ITEM_REFERENCE, ipcItemReference);
		request.setAttribute("MODE", mode);
		request.setAttribute("showBackButton", Boolean.TRUE);

		log.exiting();
		return mapping.findForward(ActionForwardConstants.SUCCESS);

	}

}
