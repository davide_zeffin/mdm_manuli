package com.sap.isa.ipc.ui.jsp.action;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.sap.isa.core.UserSessionData;
import com.sap.isa.core.businessobject.management.MetaBusinessObjectManager;
import com.sap.isa.core.interaction.InteractionConfig;
import com.sap.isa.core.interaction.InteractionConfigContainer;
import com.sap.isa.core.util.RequestParser;
import com.sap.isa.ipc.ui.jsp.uiclass.UIContext;
import com.sap.spc.remote.client.object.Configuration;
import com.sap.spc.remote.client.object.IPCException;
import com.sap.spc.remote.client.object.OriginalRequestParameterConstants;
/**
 * @author 
 *
 * To change this generated comment edit the template variable "typecomment":
 * Window>Preferences>Java>Templates.
 * To enable and disable the creation of type comments go to
 * Window>Preferences>Java>Code Generation.
 */
public class ProcessUINameParameterAction extends IPCBaseAction {

    /**
     * @see com.sap.isa.core.BaseAction#doPerform(ActionMapping, ActionForm, HttpServletRequest, HttpServletResponse)
     */
	public ActionForward ecomPerform(ActionMapping mapping,
		ActionForm form,
		HttpServletRequest request,
		HttpServletResponse response,
		UserSessionData userSessionData,
		RequestParser requestParser,
		MetaBusinessObjectManager mbom,
		boolean multipleInvocation,
		boolean browserBack)
			throws IOException, ServletException {

        Map uiNameParams = null;

        //		String uiName = (String)request.getAttribute(UINAME);
        UIContext uiContext =
            getUIContext(request);
        Configuration currentConfiguration =
            uiContext.getCurrentConfiguration();
        try {
            String uiName =
                currentConfiguration.getActiveKBProfile().getUIName();
            if (uiName == null || uiName.equals("") || uiName.equals("null")) {
                uiNameParams = new HashMap();
                request.setAttribute(InternalRequestParameterConstants.UINAME_PARAMETERS, uiNameParams);
            } else {
                InteractionConfigContainer interactionConfigContainer =
                    getInteractionConfig(request);
                InteractionConfig interactionConfig =
                    interactionConfigContainer.getConfig(uiName);
                if (interactionConfig != null) {
                    uiNameParams = interactionConfig.getParams();
                    request.setAttribute(InternalRequestParameterConstants.UINAME_PARAMETERS, uiNameParams);
                } else {
                    request.setAttribute(InternalRequestParameterConstants.UINAME_PARAMETERS, new HashMap());
                }
            }
        } catch (IPCException e) {
			setExtendedRequestAttribute(request, RequestParameterConstants.IPC_EXCEPTION, e);
            return mapping.findForward(ActionForwardConstants.INTERNAL_ERROR);
        }

		customerExit(mapping, form, request, response, userSessionData, requestParser, mbom, multipleInvocation, browserBack);

		// check for designerMode
		String designerParam = "F"; 
		if (request.getParameter(OriginalRequestParameterConstants.DESIGNER_MODE) != null && !request.getParameter(OriginalRequestParameterConstants.DESIGNER_MODE).equals("")) {
			designerParam = request.getParameter(OriginalRequestParameterConstants.DESIGNER_MODE);
		}else if (request.getAttribute(OriginalRequestParameterConstants.DESIGNER_MODE) != null) {
			designerParam = (String) request.getAttribute(OriginalRequestParameterConstants.DESIGNER_MODE);
		}
		if (designerParam != null && designerParam.equals("T")){
			return processForward(mapping, request, ActionForwardConstants.DESIGNER);
		}

        return processForward(mapping, request, ActionForwardConstants.SUCCESS);
    }

    public static void main(String[] args) {
    }
}
