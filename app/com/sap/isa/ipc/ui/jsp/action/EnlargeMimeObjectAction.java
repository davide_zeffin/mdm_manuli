package com.sap.isa.ipc.ui.jsp.action;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.sap.isa.core.UserSessionData;
import com.sap.isa.core.businessobject.management.MetaBusinessObjectManager;
import com.sap.isa.core.util.RequestParser;
import com.sap.isa.ipc.ui.jsp.beans.MimeUIBean;
import com.sap.isa.ipc.ui.jsp.beans.UIBeanFactory;
import com.sap.isa.ipc.ui.jsp.uiclass.UIContext;
import com.sap.spc.remote.client.object.MimeObject;

/**
 * Title:
 * Description:  JSP UI for the IPC.
 * Copyright:    Copyright (c) 2001
 * Company:
 * @author
 * @version 1.0
 */

public class EnlargeMimeObjectAction extends IPCBaseAction implements RequestParameterConstants, ActionForwardConstants {

	public ActionForward ecomPerform(ActionMapping mapping,
		ActionForm form,
		HttpServletRequest request,
		HttpServletResponse response,
		UserSessionData userSessionData,
		RequestParser requestParser,
		MetaBusinessObjectManager mbom,
		boolean multipleInvocation,
		boolean browserBack)
			throws IOException, ServletException {


    UIContext uiContext = getUIContext(request);

    int parameterIndex = 0;
    MimeObject mimeBO = factory.newMimeObject(parameterIndex, request.getQueryString());
    
    // if it was not possible to get the mime-object from the query string try
    // to get it from the request
    if (mimeBO.getURL() == null || mimeBO.getURL().equals("")) {
        if (request.getParameter(MimeObject.MIME_TYPE + parameterIndex) != null && request.getParameter(MimeObject.MIME_URL + parameterIndex) != null) {
            mimeBO = factory.newMimeObject(request.getParameter(MimeObject.MIME_URL + parameterIndex),request.getParameter(MimeObject.MIME_TYPE + parameterIndex));
        }    
    }
    mimeBO.setParameter(MimeObject.IS_ENLARGED,MimeObject.YES);
    if (mimeBO.getType().equals(MimeObject.O2C)) {
        mimeBO.setParameter("width","640");
        mimeBO.setParameter("height","480");
    }
    MimeUIBean mimeBean = UIBeanFactory.getUIBeanFactory().newMimeUIBean(mimeBO, uiContext);

	customerExit(mapping, form, request, response, userSessionData, requestParser, mbom, multipleInvocation, browserBack);

	setExtendedRequestAttribute(request, CURRENT_MIME_OBJECT, mimeBean);
    if (uiContext.getPropertyAsBoolean(RequestParameterConstants.SHOW_MIMES_IN_NEW_WINDOW)) {
        return mapping.findForward(MIME_POPUP);
    }
    else {
        return mapping.findForward(SUCCESS);
    }
  }
}
