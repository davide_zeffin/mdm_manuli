package com.sap.isa.ipc.ui.jsp.action;

import java.util.ArrayList;
import java.util.Enumeration;

import javax.servlet.http.HttpServletRequest;

import com.sap.spc.remote.shared.SimpleRequest;

/**
 * Title:
 * Description:
 * Copyright:    Copyright (c) 2000
 * Company:
 * @author
 * @version 1.0
 */

public class SimpleRequestWrapper implements SimpleRequest {
	private HttpServletRequest request;

	public SimpleRequestWrapper(HttpServletRequest request) {
		this.request = request;
	}

	public String getParameterValue(String name) {
		String[] parameterValues = getParameterValues(name);
		if (parameterValues == null) {
			return null;
		}else if (parameterValues.length > 1) {
			return null;
		}else {
			return parameterValues[0];
		}
	}

	public String getParameterValue(String name, String defaultValue) {
		String result = getParameterValue(name);
		if (result != null) {
			return result;
		}else {
			return defaultValue;
		}
	}

	/**
	 * returns request parameters in the format String param[1], String param[2], ...
	 * into String[] param
	 */
	public String[] getParameterValues(String name) {
		String[] values = request.getParameterValues(name);
		if (values != null) {
			return values;
		}else {
			ArrayList parameterValues = new ArrayList();
			for (Enumeration e = request.getParameterNames(); e.hasMoreElements(); ) {
				String parameterName = (String)e.nextElement();
				if (parameterName.indexOf(name + '[') != -1) {
					int bracketOpenPosition = parameterName.lastIndexOf('[');
					int bracketClosePosition = parameterName.lastIndexOf(']');
					String strIndex = parameterName.substring(bracketOpenPosition + 1, bracketClosePosition);
					if (strIndex != null && !strIndex.equalsIgnoreCase("")) {
						int index = Integer.parseInt(strIndex);
						String parameterValue = request.getParameter(parameterName);
						if (parameterValue != null) {
							parameterValues.ensureCapacity(index);
							if (parameterValues.size() < index) {
								for (int i = parameterValues.size(); i < index; i++) {
									parameterValues.add(null);
								}
							}
							parameterValues.set(index - 1, parameterValue);
						}
					}
				}
			}
			if (parameterValues.size() > 0) {
				String[] objects = new String[parameterValues.size()];
				return (String[])(parameterValues.toArray(objects));
			}else {
				return null;
			}
		}
	}

	public String[] getParameterValues(String name, String[] defaultValues) {
		String results[];
		results = getParameterValues(name);
		if (results == null) {
			return null;
		}else {
			for (int i = 0; i < results.length; i++) {
				if (results[i] == null) {
					results[i] = defaultValues[i];
				}
			}
			return results;
		}
	}
}