/*
 * Created on 29.12.2004
 *
 * To change the template for this generated file go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
package com.sap.isa.ipc.ui.jsp.action;

import java.io.IOException;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.Vector;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.sap.isa.core.Constants;
import com.sap.isa.core.UserSessionData;
import com.sap.isa.core.businessobject.management.MetaBusinessObjectManager;
import com.sap.isa.core.util.RequestParser;
import com.sap.isa.ipc.ui.jsp.beans.CharacteristicUIBean;
import com.sap.isa.ipc.ui.jsp.beans.ConfigUIBean;
import com.sap.isa.ipc.ui.jsp.beans.GroupUIBean;
import com.sap.isa.ipc.ui.jsp.beans.InstanceUIBean;
import com.sap.isa.ipc.ui.jsp.beans.ProductVariantUIBean;
import com.sap.isa.ipc.ui.jsp.beans.UIBeanFactory;
import com.sap.isa.ipc.ui.jsp.beans.ValueUIBean;
import com.sap.isa.ipc.ui.jsp.constants.ProductVariantComparisonRequestParameterConstants;
import com.sap.isa.ipc.ui.jsp.uiclass.UIContext;
import com.sap.spc.remote.client.object.Characteristic;
import com.sap.spc.remote.client.object.CharacteristicGroup;
import com.sap.spc.remote.client.object.CharacteristicValue;
import com.sap.spc.remote.client.object.Instance;

/**
 * @author 
 *
 * To change the template for this generated type comment go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
public class GetProductVariantComparisonAction extends IPCBaseAction {

	/* (non-Javadoc)
	 * @see com.sap.isa.core.BaseAction#doPerform(org.apache.struts.action.ActionMapping, org.apache.struts.action.ActionForm, javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse)
	 */
	public ActionForward ecomPerform(ActionMapping mapping,
		ActionForm form,
		HttpServletRequest request,
		HttpServletResponse response,
		UserSessionData userSessionData,
		RequestParser requestParser,
		MetaBusinessObjectManager mbom,
		boolean multipleInvocation,
		boolean browserBack)
			throws IOException, ServletException {
			UIContext uiContext = getUIContext(request);

			String[] productVariantIds = (String[])request.getAttribute(InternalRequestParameterConstants.PRODUCT_VARIANTS_TO_COMPARE);
			ConfigUIBean config = this.getCurrentConfiguration(uiContext);
			//product variants only single level config
			String rootInstanceId = config.getBusinessObject().getRootInstance().getId();
			InstanceUIBean configInstanceUIBean = this.getCurrentInstance(uiContext, config, rootInstanceId, request);


			String	curCsticName = getContextValue(request, Constants.CURRENT_CHARACTERISTIC_NAME);

			String curCharGroupName = getContextValue(request, Constants.CURRENT_CHARACTERISTIC_GROUP_NAME);
			
			String	currentScrollGroupName = getContextValue(request, Constants.CURRENT_SCROLL_CHARACTERISTIC_GROUP_NAME);
			
			String groupStatusChangeName = getContextValue(request, Constants.GROUP_STATUS_CHANGE);

			String csticStatusChange = getContextValue(request, Constants.CHARACTERISTIC_STATUS_CHANGE);
			
			Hashtable groupStatus =
				(Hashtable) uiContext.getProperty(
					Constants.GROUP_STATUS_CHANGE);

			GroupUIBean curCsticGroup = getCharacteristicGroups(
					uiContext,
				    configInstanceUIBean,
					curCharGroupName,
					currentScrollGroupName,
					groupStatusChangeName,
					groupStatus,
					request);
					

			String instanceChangedParameter =
				(String)request.getAttribute(
					InternalRequestParameterConstants.INSTANCE_CHANGED);
			String groupChangedParameter =
				(String)request.getAttribute(
					InternalRequestParameterConstants.GROUP_CHANGED);

            CharacteristicUIBean currentCsticUIBean =
            GetCustomizationListAction.getCustCharacteristics(
                    uiContext,
                    configInstanceUIBean,
                    instanceChangedParameter,
                    groupChangedParameter,
                    curCsticName,
                    true);

			if (currentCsticUIBean == null); //currentCsticUIBean currently not used, avoid compiler warning

			GetCustomizationListAction.getCustCharacteristicValues(
				uiContext,
				configInstanceUIBean,
				log);

            boolean showMessageCstic = uiContext.getPropertyAsBoolean(ProcessRequestParameterAction.SHOW_MESSAGE_CSTICS);
            String prefix = uiContext.getPropertyAsString(ProcessRequestParameterAction.MESSAGE_CSTICS_PREFIX);

			Vector productVariantsToCompare = new Vector();
			if (productVariantIds != null) {
				Hashtable productVariantsById = (Hashtable)uiContext.getProperty(InternalRequestParameterConstants.PRODUCT_VARIANTS_BY_ID);
				for (int i = 0; i < productVariantIds.length; i++) {
					ProductVariantUIBean currentProductVariant = (ProductVariantUIBean)productVariantsById.get(productVariantIds[i]);
					//Create the hierarchy of uibeans for the configuration of the product variant
					//create InstanceUIBeans with the Configuration RootInstance Business Objects and
					//associate them with the ProductVariantUIBean
					//In order to get separate bean objects for each product variants, the objects are
					//created with call to their constructors instead of .getInstance(...)
					Instance rootInstance = currentProductVariant.getConfiguration().getRootInstance();
					InstanceUIBean instanceUIBean = UIBeanFactory.getUIBeanFactory().newInstanceUIBean(rootInstance, uiContext);
					currentProductVariant.setInstance(instanceUIBean);
					for (Iterator groupIt = rootInstance.getCharacteristicGroups(uiContext.getShowInvisibleCharacteristics(), uiContext.getPropertyAsBoolean(RequestParameterConstants.SHOW_GENERAL_TAB_FIRST)).iterator(); groupIt.hasNext(); ) {
						//create GroupUIBeans with the rootInstance CharacteristicGroup's and associate to InstanceUIBean
						CharacteristicGroup group = (CharacteristicGroup)groupIt.next();
						GroupUIBean groupUIBean =  UIBeanFactory.getUIBeanFactory().newGroupUIBean(group, uiContext);
						instanceUIBean.addGroup(groupUIBean);
						for (Iterator csticIt = group.getCharacteristics(uiContext.getShowInvisibleCharacteristics()).iterator(); csticIt.hasNext(); ) {
							Characteristic cstic = (Characteristic)csticIt.next();
                            // check if show_message_cstics is true (In-Screen Messaging is enbaled) AND cstic name begins with the prefix defined in the XCM
                            CharacteristicUIBean csticUIBean; 
                            if (showMessageCstic && cstic.getName().startsWith(prefix)  ){
                                continue;  //ignore this cstic, because it is a message cstic
                            }
                            else {
    							csticUIBean = UIBeanFactory.getUIBeanFactory().newCharacteristicUIBean(cstic, uiContext);
                            }
							instanceUIBean.addCharacteristic(csticUIBean);
							groupUIBean.addCharacteristic(csticUIBean);
							for (Iterator valueIt = cstic.getValues().iterator(); valueIt.hasNext(); ) {
								CharacteristicValue value = (CharacteristicValue)valueIt.next();
								//ValueUIBean valueUIBean = ValueUIBean.getInstance(request, value, uiContext);
								ValueUIBean valueUIBean = UIBeanFactory.getUIBeanFactory().newValueUIBean (value, uiContext);
								csticUIBean.addValue(valueUIBean);
							}
						}
					}
					productVariantsToCompare.add(currentProductVariant);
					if (log.isDebugEnabled()) {
						log.debug("currentProductVariant:" + currentProductVariant);
					}
				}
			}
			customerExit(mapping, form, request, response, userSessionData, requestParser, mbom, multipleInvocation, browserBack);

			request.setAttribute(ProductVariantComparisonRequestParameterConstants.CURRENT_INSTANCE, configInstanceUIBean);
			request.setAttribute(ProductVariantComparisonRequestParameterConstants.PRODUCT_VARIANTS_TO_COMPARE, productVariantsToCompare);

			return processForward(mapping, request, ActionForwardConstants.SUCCESS);
		}

}
