package com.sap.isa.ipc.ui.jsp.action;

/**
 * GetDynamicProductPictureAction reads in all dynamic porduct pictures and
 * puts them into the request object.
 */
import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.sap.isa.core.UserSessionData;
import com.sap.isa.core.businessobject.management.MetaBusinessObjectManager;
import com.sap.isa.core.util.RequestParser;
import com.sap.isa.ipc.ui.jsp.container.DynamicProductList;
import com.sap.isa.ipc.ui.jsp.uiclass.UIContext;
import com.sap.spc.remote.client.object.OriginalRequestParameterConstants;

public class GetDynamicProductsAction extends IPCBaseAction implements RequestParameterConstants,
                                                                    ActionForwardConstants {
    /**
     * <p>name of the file with the maintained list of dynamic products
     * (using Dynamic product picture)</p>
     */
    static final String DYNAMIC_PRODUCT_PICTURE_XMLFILE  = "/WEB-INF/ipc/dynamicProducts.xml";
    /**
     * <p>A list of dynamic products (using Dynamic product picture)</p>
     */
    static final String DYNAMIC_PRODUCTS  = "dynamicProducts";


    public ActionForward ecomPerform(ActionMapping mapping,
        ActionForm form,
        HttpServletRequest request,
        HttpServletResponse response,
        UserSessionData userSessionData,
        RequestParser requestParser,
        MetaBusinessObjectManager mbom,
        boolean multipleInvocation,
        boolean browserBack)
            throws IOException, ServletException {

        UIContext uiContext = getUIContext(request);

        if(uiContext == null)
            return (mapping.findForward(INTERNAL_ERROR));

        // if dynamic product picture should NOT be displayed forward to next action
        if (!uiContext.getPropertyAsBoolean(OriginalRequestParameterConstants.SHOW_DYNAMIC_PRODUCT_PICTURE)){
            return processForward(mapping, request, ActionForwardConstants.SUCCESS);
        }

        // parse the xml and put the product list in the request
        DynamicProductList dynamicProductList = new DynamicProductList();


        // read /ipc/templates/dynamicProducts.xml
        InputStream input = getServlet().getServletContext().getResourceAsStream(DYNAMIC_PRODUCT_PICTURE_XMLFILE);
        dynamicProductList.parseStream(input);
        HashMap dynamicProducts = dynamicProductList.getProductHashMap();

		customerExit(mapping, form, request, response, userSessionData, requestParser, mbom, multipleInvocation, browserBack);

        uiContext.setDynamicProducts(dynamicProducts);
        log.debug(dynamicProducts);

        return processForward(mapping, request, ActionForwardConstants.SUCCESS);
    }

}