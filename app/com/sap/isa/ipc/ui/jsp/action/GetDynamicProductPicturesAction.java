package com.sap.isa.ipc.ui.jsp.action;

/**
 * GetDynamicProductPictureAction reads in all dynamic porduct pictures and
 * puts them into the request object.
 */
import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.sap.isa.core.Constants;
import com.sap.isa.core.UserSessionData;
import com.sap.isa.core.businessobject.management.MetaBusinessObjectManager;
import com.sap.isa.core.util.RequestParser;
import com.sap.isa.ipc.ui.jsp.beans.CharacteristicUIBean;
import com.sap.isa.ipc.ui.jsp.beans.InstanceUIBean;
import com.sap.isa.ipc.ui.jsp.beans.UIBeanFactory;
import com.sap.isa.ipc.ui.jsp.beans.ValueUIBean;
import com.sap.isa.ipc.ui.jsp.constants.DynamicProductPictureRequestParameterConstants;
import com.sap.isa.ipc.ui.jsp.uiclass.UIContext;
import com.sap.spc.remote.client.object.Characteristic;
import com.sap.spc.remote.client.object.CharacteristicValue;
import com.sap.spc.remote.client.object.Instance;

public class GetDynamicProductPicturesAction extends IPCBaseAction implements RequestParameterConstants,
                                                                    ActionForwardConstants {
    /**
     * <p>A list of assigned values (using Dynamic product picture)</p>
     */

    public ActionForward ecomPerform(ActionMapping mapping,
        ActionForm form,
        HttpServletRequest request,
        HttpServletResponse response,
        UserSessionData userSessionData,
        RequestParser requestParser,
        MetaBusinessObjectManager mbom,
        boolean multipleInvocation,
        boolean browserBack)
            throws IOException, ServletException {

        UIContext uiContext = getUIContext(request);

        if(uiContext == null)
            return (mapping.findForward(INTERNAL_ERROR));


        // String curInstId = getContextValue(request, Constants.CURRENT_INSTANCE_ID);
        //if (!isSet(curInstId)) {
        //    return parameterError(mapping, request, Constants.CURRENT_INSTANCE_ID, this.getClass().getName());
        //}
        // new UI, no current inst, use root instead
        Instance instance = uiContext.getCurrentConfiguration().getRootInstance();
        InstanceUIBean instanceUIBean = UIBeanFactory.getUIBeanFactory().newInstanceUIBean(instance, uiContext);
        // fill the instanceUIBean
        getCharacteristicsAndAssignedValues(uiContext, instanceUIBean);       

		customerExit(mapping, form, request, response, userSessionData, requestParser, mbom, multipleInvocation, browserBack);

        this.setExtendedRequestAttribute(request, DynamicProductPictureRequestParameterConstants.CURRENT_INSTANCE, instanceUIBean);

        return processForward(mapping, request, ActionForwardConstants.SUCCESS);
    }
    
    /**
     * Fills the instanceUIBean with characteristicUIBeans and with valueUIBeans for the assigned values.
     * @param uiContext
     * @param instanceUIBean
     */
    private static void getCharacteristicsAndAssignedValues(UIContext uiContext, InstanceUIBean instanceUIBean){
        boolean showInvisibleCstics = uiContext.getShowInvisibleCharacteristics();
        Instance instance = instanceUIBean.getBusinessObject();
        List characteristics = instance.getCharacteristics(showInvisibleCstics);
        boolean showMessageCstic = uiContext.getPropertyAsBoolean(ProcessRequestParameterAction.SHOW_MESSAGE_CSTICS);
        String prefix = uiContext.getPropertyAsString(ProcessRequestParameterAction.MESSAGE_CSTICS_PREFIX);

        for (int i=0; i < characteristics.size(); i++) {
            Characteristic cstic = (Characteristic) characteristics.get(i);
            // check if show_message_cstics is true (In-Screen Messaging is enbaled) AND cstic name begins with the prefix defined in the XCM
            CharacteristicUIBean csticUIBean; 
            if (showMessageCstic && cstic.getName().startsWith(prefix)  ){
                continue;  //ignore this cstic, because it is a message cstic
            }
            else {
                csticUIBean = UIBeanFactory.getUIBeanFactory().newCharacteristicUIBean(cstic, uiContext);
            }
            List assignedValues = cstic.getAssignedValues();
            for (int k=0; k<assignedValues.size(); k++){
                CharacteristicValue value = (CharacteristicValue)assignedValues.get(k);
                ValueUIBean valueBean = UIBeanFactory.getUIBeanFactory().newValueUIBean(value, uiContext);
                csticUIBean.addValue(valueBean);
            }
            instanceUIBean.addCharacteristic(csticUIBean);                
        }
    }
}
