/*
 * Created on 07.04.2008
 *
 * To change the template for this generated file go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
package com.sap.isa.ipc.ui.jsp.action;

import java.io.IOException;
import java.util.Enumeration;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.sap.isa.businessobject.CommunicationException;
import com.sap.isa.businessobject.ipc.IPCBOManager;
import com.sap.isa.core.SessionConst;
import com.sap.isa.core.UserSessionData;
import com.sap.isa.core.businessobject.management.MetaBusinessObjectManager;
import com.sap.isa.core.util.RequestParser;
import com.sap.isa.core.util.StartupParameter;
import com.sap.isa.core.util.RequestParser.Parameter;
import com.sap.isa.ipc.ui.jsp.uiclass.UIContext;
import com.sap.spc.remote.client.object.IPCClient;

/**
 * @author D048393
 *
 * To change the template for this generated type comment go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
public class PreLoginAction
	extends IPCBaseAction
	implements RequestParameterConstants, ActionForwardConstants {

	/* (non-Javadoc)
	 * @see com.sap.isa.isacore.action.EComBaseAction#ecomPerform(org.apache.struts.action.ActionMapping, org.apache.struts.action.ActionForm, javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse, com.sap.isa.core.UserSessionData, com.sap.isa.core.util.RequestParser, com.sap.isa.core.businessobject.management.MetaBusinessObjectManager, boolean, boolean)
	 */
	public ActionForward ecomPerform(
		ActionMapping mapping,
		ActionForm form,
		HttpServletRequest request,
		HttpServletResponse response,
		UserSessionData userSessionData,
		RequestParser requestParser,
		MetaBusinessObjectManager mbom,
		boolean multipleInvocation,
		boolean browserBack)
		throws IOException, ServletException, CommunicationException {

		// prepare StartUpParamList
		StartupParameter startupParameter = (StartupParameter) userSessionData.getAttribute(SessionConst.STARTUP_PARAMETER);

		// get list of params
		Enumeration requestParams = request.getParameterNames();

		// loop over list of parameter
		while (requestParams.hasMoreElements()) {
			String paramName = (String) requestParams.nextElement();
			// add Parameter
			startupParameter.addParameter(paramName, request.getParameter(paramName), true);

		}
		// get the caller from request (set in dispatch on Caller and also store him)
		startupParameter.addParameter(RequestParameterConstants.CALLER, (String)request.getAttribute(RequestParameterConstants.CALLER), true);
		
		
		// prepare IPC backend, since this is required by the Login
		// The login will swith this connection for a generic user to the loggedin user		
					
		return mapping.findForward(SUCCESS);

	}
}
