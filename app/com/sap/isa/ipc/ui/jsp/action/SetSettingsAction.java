package com.sap.isa.ipc.ui.jsp.action;

import java.io.IOException;
import java.util.Hashtable;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.sap.isa.core.Constants;
import com.sap.isa.core.UserSessionData;
import com.sap.isa.core.businessobject.management.MetaBusinessObjectManager;
import com.sap.isa.core.util.RequestParser;
import com.sap.isa.ipc.ui.jsp.uiclass.UIContext;

/**
 * SetSettings - Action reads in all settings (which might have been changed
 * by the user) and stores them in the ui context
 */
public class SetSettingsAction
	extends IPCBaseAction
	implements RequestParameterConstants, ActionForwardConstants {

	public ActionForward ecomPerform(ActionMapping mapping,
		ActionForm form,
		HttpServletRequest request,
		HttpServletResponse response,
		UserSessionData userSessionData,
		RequestParser requestParser,
		MetaBusinessObjectManager mbom,
		boolean multipleInvocation,
		boolean browserBack)
			throws IOException, ServletException {

		UIContext uiContext =
			getUIContext(request);

		// transfer all settings from the request to the ui context if the user didn't
		// select cancel (in that case, PARAMETER_VALID would be set to "F"
		if (request.getParameter(PARAMETER_VALID).equals(T)) {
			uiContext.setProperty(
				SHOW_INVISIBLE_CHARACTERISTICS,
				(translatePseudoBoolean(request,
					SHOW_INVISIBLE_CHARACTERISTICS)));
			//      uiContext.setCheckPricing(translatePseudoBoolean(request,CHECK_PRICING,false)); not used anymore according to MIZ param doc
			uiContext.setProperty(
				ONLINE_EVALUATE,
				(translatePseudoBoolean(request, ONLINE_EVALUATE)));
			//      uiContext.setPermanentCompletenessCheck(translatePseudoBoolean(request,PERMANENT_COMPLETENESS_CHECK,false));
			uiContext.setProperty(
				SHOW_LANGUAGE_DEPENDENT_NAMES,
				(translatePseudoBoolean(request,
					SHOW_LANGUAGE_DEPENDENT_NAMES)));

	        boolean showGroups = translatePseudoBoolean(request, USE_GROUP_INFORMATION);
			if (showGroups != uiContext.getPropertyAsBoolean(USE_GROUP_INFORMATION)) {
				uiContext.setInitialMerge(true);
				uiContext.setProperty(USE_GROUP_INFORMATION, showGroups);
     			uiContext.setProperty(USE_GROUP_INFORMATION_MODIFIED, showGroups);
			}
			
			
			boolean csticsExpanded = translatePseudoBoolean(request, SHOW_CSTIC_VALUES_EXPANDED);
			if (csticsExpanded != uiContext.getPropertyAsBoolean(SHOW_CSTIC_VALUES_EXPANDED)) {
                // "Display All Option" setting has been changed:
                // Change it in the uiContext. 
                uiContext.setProperty(SHOW_CSTIC_VALUES_EXPANDED, csticsExpanded);
                // Set the flag that it has been modified. This is evaluated in MergeVisitor. 
                uiContext.setProperty(CSTIC_VALUES_EXPANDED_MODIFIED, true);                
			}
		}
		String currentInstanceId = request.getParameter(Constants.CURRENT_INSTANCE_ID);
		if (!isSet(currentInstanceId)) {
			return parameterError(mapping, request, Constants.CURRENT_INSTANCE_ID, getClass().getName());
		}else {
			setContextValue(request, Constants.CURRENT_INSTANCE_ID, currentInstanceId);
		}
        String curScrollGroupName = request.getParameter(Constants.CURRENT_SCROLL_CHARACTERISTIC_GROUP_NAME);
        if (isSet(curScrollGroupName)) {
            setContextValue(request, Constants.CURRENT_SCROLL_CHARACTERISTIC_GROUP_NAME, curScrollGroupName);
        }
        String curGroupName = request.getParameter(Constants.CURRENT_CHARACTERISTIC_GROUP_NAME); 
        if (isSet(curGroupName)) {
            setContextValue(request, Constants.CURRENT_CHARACTERISTIC_GROUP_NAME, curGroupName);
        }
        //had the user manipulated a characcteristic before going to settings?
        String curCstic = request.getParameter(Constants.CURRENT_CHARACTERISTIC_NAME);
        if (isSet(curCstic)) {
        	setContextValue(request, Constants.CURRENT_CHARACTERISTIC_NAME, curCstic);
        }else {
        	setContextValue(request, Constants.CURRENT_CHARACTERISTIC_NAME, "");
        }

		customerExit(mapping, form, request, response, userSessionData, requestParser, mbom, multipleInvocation, browserBack);
        
		return processForward(mapping, request, ActionForwardConstants.SUCCESS);
	}

	/**
	 * This method reads the attribute given by the attribName and returns the
	 * corresponding boolean value. In case that the attribute doesn't exist in
	 * the request, a default value will be used.
	 */
	private boolean translatePseudoBoolean(
		HttpServletRequest request,
		String attribName) {
		String value = request.getParameter(attribName);
		return (value == null) ? false : true;
	}

}