/**
 *  GetConflictHandlingShortcutAction returns the Shortcut for conflictHandling
 *  In the jsp page you can access the the shortcut using
 *  the following method:
 *  List conflictHandlingShortcuts = (List)request.getAttribute(RequestParameterConstants.);
 */
package com.sap.isa.ipc.ui.jsp.action;

import java.io.IOException;
import java.util.List;
import java.util.Vector;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.sap.isa.core.UserSessionData;
import com.sap.isa.core.businessobject.management.MetaBusinessObjectManager;
import com.sap.isa.core.util.RequestParser;
import com.sap.isa.ipc.ui.jsp.uiclass.UIContext;
import com.sap.spc.remote.client.object.Configuration;
import com.sap.spc.remote.client.object.ConflictHandlingShortcut;
import com.sap.spc.remote.client.object.DescriptiveConflictType;


public class GetConflictHandlingShortcutsAction extends IPCBaseAction implements RequestParameterConstants,
                                                              ActionForwardConstants{

	public ActionForward ecomPerform(ActionMapping mapping,
		ActionForm form,
		HttpServletRequest request,
		HttpServletResponse response,
		UserSessionData userSessionData,
		RequestParser requestParser,
		MetaBusinessObjectManager mbom,
		boolean multipleInvocation,
		boolean browserBack)
			throws IOException, ServletException {

		UIContext uiContext = getUIContext(request);

        Configuration currentConfiguration = uiContext.getCurrentConfiguration();
        if(currentConfiguration == null)
          return (mapping.findForward(INTERNAL_ERROR));
		List shortcuts = new Vector();
		List messages = (List) uiContext.getProperty(MESSAGES);
		if (messages == null) {
			messages = new Vector();
		}
//      check for value_shortcut
		DescriptiveConflictType conflictType = (DescriptiveConflictType)request.getAttribute(CONFLICT_VALUE);
		if(conflictType != null){
			ConflictHandlingShortcut shortcut = conflictType.getShortcut();
			shortcuts.add(shortcut);
			request.removeAttribute(CONFLICT_VALUE);
		}

		customerExit(mapping, form, request, response, userSessionData, requestParser, mbom, multipleInvocation, browserBack);

		uiContext.setProperty(MESSAGES, shortcuts);
//		request.setAttribute(CONFLICT_HANDLING_SHORTCUTS, shortcuts);

        return processForward(mapping, request, ActionForwardConstants.SUCCESS);
	}
}