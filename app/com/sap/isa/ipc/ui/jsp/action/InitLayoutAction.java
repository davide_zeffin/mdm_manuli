/*
 * Created on 15.01.2005
 *
 */
package com.sap.isa.ipc.ui.jsp.action;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.sap.isa.businessobject.CommunicationException;
import com.sap.isa.core.Constants;
import com.sap.isa.core.UserSessionData;
import com.sap.isa.core.businessobject.management.MetaBusinessObjectManager;
import com.sap.isa.core.util.RequestParser;
import com.sap.isa.ipc.ui.jsp.uiclass.UIContext;
import com.sap.spc.remote.client.object.Configuration;
import com.sap.spc.remote.client.object.Instance;

/**
 * @author 
 *
 */
public class InitLayoutAction extends IPCBaseAction {

	/* (non-Javadoc)
	 * @see com.sap.isa.isacore.action.EComBaseAction#ecomPerform(org.apache.struts.action.ActionMapping, org.apache.struts.action.ActionForm, javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse, com.sap.isa.core.UserSessionData, com.sap.isa.core.util.RequestParser, com.sap.isa.core.businessobject.management.MetaBusinessObjectManager, boolean, boolean)
	 */
	public ActionForward ecomPerform(
		ActionMapping mapping,
		ActionForm form,
		HttpServletRequest request,
		HttpServletResponse response,
		UserSessionData userSessionData,
		RequestParser requestParser,
		MetaBusinessObjectManager mbom,
		boolean multipleInvocation,
		boolean browserBack)
		throws IOException, ServletException, CommunicationException {
			UIContext uiContext = getUIContext(request);
			
			Configuration currentConfiguration = uiContext.getCurrentConfiguration();
			String currentInstanceId = getContextValue(request, Constants.CURRENT_INSTANCE_ID);
			if (!isSet(currentInstanceId)) {
				currentInstanceId = currentConfiguration.getRootInstance().getId();
			}

			setInitialContextValues(request, currentConfiguration);

			customerExit(mapping, form, request, response, userSessionData, requestParser, mbom, multipleInvocation, browserBack);

			// we always forward to the dynamicUI layout
			return mapping.findForward(ActionForwardConstants.DYNAMIC_UI);

	}
    
    public static boolean showLayoutWithGroups(
        UIContext uiContext,
        Configuration currentConfiguration,
        String currentInstanceId) {
        Instance currentInstance = currentConfiguration.getInstance(currentInstanceId);
        List groups = currentInstance.getCharacteristicGroups();
        boolean layoutWithGroups = false;
        //check if the model contains characteristic groups and the customizing is set to display groups
        if (groups != null && !groups.isEmpty() && uiContext.getUseGroupInformation()) {
        	layoutWithGroups = true;
        }else {
        	layoutWithGroups = false;
        }
        return layoutWithGroups;
    }

}
