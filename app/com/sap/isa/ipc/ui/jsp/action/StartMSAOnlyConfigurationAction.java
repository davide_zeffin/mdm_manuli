package com.sap.isa.ipc.ui.jsp.action;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.sap.isa.businessobject.ipc.IPCBOManager;
import com.sap.isa.core.UserSessionData;
import com.sap.isa.core.businessobject.management.MetaBusinessObjectManager;
import com.sap.isa.core.util.RequestParser;
import com.sap.isa.core.util.RequestParser.Parameter;
import com.sap.isa.ipc.ui.jsp.uiclass.UIContext;
import com.sap.spc.remote.client.object.Configuration;
import com.sap.spc.remote.client.object.IPCClient;
import com.sap.spc.remote.client.object.IPCConfigReference;
import com.sap.spc.remote.client.object.imp.IllegalClassException;
import com.sap.spc.remote.client.object.imp.tcp.TCPIPCConfigReference;
import com.sap.spc.remote.client.util.ext_configuration;

/**
 * @author 
 *
 */
public class StartMSAOnlyConfigurationAction 
    extends StartConfigurationAction {

    public ActionForward ecomPerform(
        ActionMapping mapping,
        ActionForm form,
        HttpServletRequest request,
        HttpServletResponse response,
        UserSessionData userSessionData,
        RequestParser requestParser,
        MetaBusinessObjectManager mbom,
        boolean multipleInvocation,
        boolean browserBack)
        throws IOException, ServletException {

            UIContext uiContext = IPCBaseAction.getUIContext(request);
            if (uiContext == null) { //the uiContext has not been initialized
            ActionForward forward = processScenarioParameter(mapping, request);
                if (!forward.getName().equals(ActionForwardConstants.SUCCESS)) return forward;
                uiContext = IPCBaseAction.getUIContext(request); //uiContext created in processScenarioParameter
            }
            super.htmlTrace(request);
            
            Parameter hostParam = requestParser.getParameter(IPC_HOST);
            if (!hostParam.isSet()) return this.parameterError(mapping, request, IPC_HOST, this.getClass().getName());;
            Parameter portParam = requestParser.getParameter(IPC_PORT);
            if (!portParam.isSet()) return this.parameterError(mapping, request, IPC_PORT, this.getClass().getName());;
            Parameter clientParam = requestParser.getParameter(RequestParameterConstants.CLIENT);
            if (!clientParam.isSet()) return this.parameterError(mapping, request, RequestParameterConstants.CLIENT, this.getClass().getName());
            Parameter typeParam = requestParser.getParameter(RequestParameterConstants.TYPE);
            String type;
            if (!typeParam.isSet()) type = RequestParameterConstants.ISA; else type = typeParam.getValue().getString();
            Parameter productIdParam = requestParser.getParameter(PRODUCT_ID);
            if (!productIdParam.isSet()) return this.parameterError(mapping, request, PRODUCT_ID, this.getClass().getName());
			Parameter callerParam = requestParser.getParameter(CALLER);
			if (!callerParam.isSet()) return parameterError(mapping, request, RequestParameterConstants.CALLER, getClass().getName());
			uiContext.setCaller(callerParam.getValue().getString());

            String kbDate = request.getParameter(KB_DATE);
            String language = request.getParameter(LANGUAGE);
            String kbLogsys = request.getParameter(KB_LOGSYS);
            String kbName = request.getParameter(KB_NAME);
            String kbVersion = request.getParameter(KB_VERSION);
            String kbProfile = request.getParameter(KB_PROFILE);
            String productType = request.getParameter(PRODUCT_TYPE);
            if ( productType != null && productType.equals("") ) productType = null;
            if ( kbDate != null && kbDate.equals("") ) kbDate = null;
            if ( kbLogsys != null && kbLogsys.equals("") ) kbLogsys = null;
            if ( kbName != null && kbName.equals("") ) kbName = null;
            if ( kbVersion != null && kbVersion.equals("") ) kbVersion = null;
            if ( kbProfile != null && kbProfile.equals("") ) kbProfile = null;


            // external configuration available?
            ext_configuration extConfig = getExternalConfiguration(request);
            // context information
            String[] contextNames = getParameters(request, CONTEXT_NAME);
            String[] contextValues = getParameters(request, CONTEXT_VALUE);
            IPCConfigReference configReference = factory.newIPCConfigReference();
            IPCClient ipcClient = null;
            if (!(configReference instanceof TCPIPCConfigReference)) {
                throw new IllegalClassException(configReference, TCPIPCConfigReference.class);
            }

			customerExit(mapping, form, request, response, userSessionData, requestParser, mbom, multipleInvocation, browserBack);


            ((TCPIPCConfigReference)configReference).setHost(hostParam.getValue().getString());
            ((TCPIPCConfigReference)configReference).setPort(portParam.getValue().getString());
            ((TCPIPCConfigReference)configReference).setDispatcher(false);
        
            if (log.isDebugEnabled()) {
                log.debug("Dispatcher: " + ((TCPIPCConfigReference)configReference).isDispatcher());
                log.debug("Host: " + ((TCPIPCConfigReference)configReference).getHost());
                log.debug("Port: " + ((TCPIPCConfigReference)configReference).getPort());
//TODO                log.debug("Sessionid: " + configReference.getSessionId());
                log.debug("Configid: " + configReference.getConfigId());
            }

            IPCBOManager ipcBoManager =
                (IPCBOManager) userSessionData.getBOM(IPCBOManager.NAME);
            if (ipcBoManager == null) {
                log.error("No IPC BOM in session data!");
                return (mapping.findForward(INTERNAL_ERROR));
            }
                    
            ipcClient = ipcBoManager.createIPCClient(
                clientParam.getValue().getString(),
                type,
                configReference);
            Configuration configuration;
            if ( extConfig != null ) 
            {
                configuration =
                    ipcClient.createConfiguration(
                        language,
                        extConfig,
                        contextNames,
                        contextValues,
                        kbDate);
            }
            else {
                configuration = ipcClient.createConfiguration(
                    productIdParam.getValue().getString(),
                    productType,
                    language,
                    kbDate,
                    kbLogsys,
                    kbName,
                    kbVersion,
                    kbProfile,
                    contextNames, contextValues);
            }
                    
//          UIContext uiContext = createUIContext(request);
            uiContext.setCurrentConfiguration(configuration);
			//initialize the context
			setInitialContextValues(request, configuration);
           

            return mapping.findForward(ActionForwardConstants.SUCCESS);
    }
}
