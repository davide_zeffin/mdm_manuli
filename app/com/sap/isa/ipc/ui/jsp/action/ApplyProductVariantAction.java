package com.sap.isa.ipc.ui.jsp.action;

/**
 * Title:
 * Description:  JSP UI for the IPC.
 * Copyright:    Copyright (c) 2001
 * Company:
 * @author
 * @version 1.0
 */
import java.io.IOException;
import java.util.ArrayList;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.sap.isa.core.Constants;
import com.sap.isa.core.UserSessionData;
import com.sap.isa.core.businessobject.management.MetaBusinessObjectManager;
import com.sap.isa.core.util.RequestParser;
import com.sap.isa.ipc.ui.jsp.beans.InstanceUIBean;
import com.sap.isa.ipc.ui.jsp.beans.MessageUIBean;
import com.sap.isa.ipc.ui.jsp.beans.ProductVariantUIBean;
import com.sap.isa.ipc.ui.jsp.beans.UIBeanFactory;
import com.sap.isa.ipc.ui.jsp.constants.MessagesRequestParameterConstants;
import com.sap.isa.ipc.ui.jsp.uiclass.UIContext;
import com.sap.spc.remote.client.object.Configuration;
import com.sap.spc.remote.client.object.IPCException;
import com.sap.spc.remote.client.object.IPCItem;
import com.sap.spc.remote.client.object.IPCItemProperties;
import com.sap.spc.remote.client.object.Instance;
import com.sap.spc.remote.client.object.ProductVariant;
import com.sap.spc.remote.client.object.imp.DefaultIPCDocument;

/* 
 * Expects the parameter @see CURRENT_PRODUCT_VARIANT_ID and replaces the root instance of the
 * current configuration @see uiContext.getCurrentConfiguration() with the associated product.
 * @see com.sap.isa.core.BaseAction#doPerform(org.apache.struts.action.ActionMapping, org.apache.struts.action.ActionForm, javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse)
 */
public class ApplyProductVariantAction
    extends IPCBaseAction
    implements RequestParameterConstants, ActionForwardConstants {

    /* 
     * @see com.sap.isa.core.BaseAction#doPerform(org.apache.struts.action.ActionMapping, org.apache.struts.action.ActionForm, javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse)
     */
	 public ActionForward ecomPerform(ActionMapping mapping,
	 ActionForm form,
	 HttpServletRequest request,
	 HttpServletResponse response,
	 UserSessionData userSessionData,
	 RequestParser requestParser,
	 MetaBusinessObjectManager mbom,
	 boolean multipleInvocation,
	 boolean browserBack)
		 throws IOException, ServletException {

        UIContext uiContext =
            getUIContext(request);

        String currentGroupName = request.getParameter(Constants.CURRENT_CHARACTERISTIC_GROUP_NAME);
        if (isSet(currentGroupName)) {
            changeContextValue(request, Constants.CURRENT_CHARACTERISTIC_GROUP_NAME, currentGroupName);
        }


        //add the list of instances as attribute to the request
        Configuration currentConfiguration =
            uiContext.getCurrentConfiguration();
        if (currentConfiguration == null)
            return mapping.findForward(INTERNAL_ERROR);
        Instance rootInstance = currentConfiguration.getRootInstance();
        if (rootInstance == null)
            return mapping.findForward(INTERNAL_ERROR);
        String productId = request.getParameter(Constants.CURRENT_PRODUCT_VARIANT_ID);
        //call the command to apply the product variant
        if (!isSet(productId))
            return parameterError(mapping, request, Constants.CURRENT_PRODUCT_VARIANT_ID, getClass().getName());

        // obtain a reference to the config's item, if possible
        IPCItem item = rootInstance.getIpcItem();
        try {
            customerExit(mapping,
            form,
            request,
            response,
            productId);
            // success does not mean anything valuable (the command does not return this value)
            boolean success = rootInstance.applyProductVariant(productId);
            if (!success) {
            	IPCException ipcException = new IPCException("Error applying productvariant!");
				setExtendedRequestAttribute(request, InternalRequestParameterConstants.IPC_EXCEPTION, ipcException);
            	return mapping.findForward(ActionForwardConstants.INTERNAL_ERROR);
            }
            if (item != null) {
                // get the product variant object
                Hashtable productVariantsById = (Hashtable)uiContext.getProperty(InternalRequestParameterConstants.PRODUCT_VARIANTS_BY_ID);
                ProductVariantUIBean currentProductVariantBean = (ProductVariantUIBean)productVariantsById.get(productId);
                if (currentProductVariantBean != null){
                    ProductVariant productVariant = currentProductVariantBean.getBusinessObject();                    
                    // read the properties of the KMAT
                    IPCItemProperties[] itemProperties = item.getPropertiesFromServer(new String[] {item.getItemId()});
                    String application = ((DefaultIPCDocument)item.getDocument()).getApplication();
                    // get the original item attributes; they will be replaced by the attributes from variant's product master
                    Map itemAttributes = null;
                    if (itemProperties != null && itemProperties[0] != null ){
						itemAttributes = itemProperties[0].getItemAttributes();
                    }
                    // get the item attributes for the variant from the product master
                    Map itemAttributesFromProductMaster = productVariant.getAttributesFromProductMaster(application);
                    // replace the original item attributes with the attributes that were returned from the variant's product master
                    if (itemAttributes != null){
                        Iterator iter = itemAttributesFromProductMaster.keySet().iterator();
                        while (iter.hasNext()) {
                            String key = (String)iter.next();
                            String value = (String)itemAttributesFromProductMaster.get(key);
                            itemAttributes.put(key, value);
                        }
                        // the the attributes at the item properties
                        itemProperties[0].setItemAttributes(itemAttributes);
						// set the attributes at the item (triggers ChangeItem command)
						item.setItemAttributes(itemAttributes);                        
                    }
                }
                
                // the configuration has changed completely
                Configuration newConfig = item.getConfiguration();
                uiContext.setCurrentConfiguration(newConfig);
                Instance newRootInstance = newConfig.getRootInstance();
                changeContextValue(request, Constants.CURRENT_INSTANCE_ID, newRootInstance.getId());
                InstanceUIBean newRootInstanceBean =
                    UIBeanFactory.getUIBeanFactory().newInstanceUIBean(newRootInstance, uiContext);
				setExtendedRequestAttribute(request, CURRENT_INSTANCE, newRootInstanceBean);
                // switch from default-mode to product variant mode
                uiContext.setProperty(PRODUCT_VARIANT_MODE, true);
                // We have to store also the original display mode that it is possible to find out 
                // the original display mode when switching back to the KMAT (i.e. whether the button
                // to switch to KMAT should be displayed).
                uiContext.setProperty(ORIGINAL_DISPLAY_MODE, uiContext.getDisplayMode());
                // set display-mode to read-only
                uiContext.setDisplayMode(true);
                // add message
                String newProductId = item.getProductId();
                List  messages = new ArrayList();
                String[] messageArguments = {newProductId};
                MessageUIBean productVariantMessage = UIBeanFactory.getUIBeanFactory().newMessageUIBean(MessageUIBean.SUCCESS, "", "ipc.message.variant.apply", messageArguments);
                messages.add(productVariantMessage);
                setExtendedRequestAttribute(request, MessagesRequestParameterConstants.MESSAGES, messages);
            } else {
                log.error(
                    "No Item object for "
                        + productId
                        + "existing. This is either an application error or you use the application in a configuration only mode without associated  order functionality.");
                // a session wide config! Applying product variants is not supported here!
            }
        } catch (IPCException e) {
            log.fatal(this, e);
			setExtendedRequestAttribute(request, IPC_EXCEPTION, e);
            return mapping.findForward(INTERNAL_ERROR);
        }
        
        return processForward(mapping, request, ActionForwardConstants.SUCCESS);

    }
    
    /**
     * Enables subclasses to replace the productId that identifies the productVariant.
     * @param request
     * @param productId
     */
    protected void customerExit(
    ActionMapping mapping,
    ActionForm form,
    HttpServletRequest request,
    HttpServletResponse response,
    String productId) {
    	//avoid compiler warnings about unused local variables
    	if(mapping == null);
    	if(form == null);
    	if(request == null);
    	if (response == null);
    	if (productId == null);
    }
}
