/*
 * Created on 09.08.2004
 *
 * To change the template for this generated file go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
package com.sap.isa.ipc.ui.jsp.action;

import java.io.IOException;
import java.util.Hashtable;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.sap.isa.core.Constants;
import com.sap.isa.core.UserSessionData;
import com.sap.isa.core.businessobject.management.MetaBusinessObjectManager;
import com.sap.isa.core.util.RequestParser;
import com.sap.isa.ipc.ui.jsp.beans.ConfigUIBean;
import com.sap.isa.ipc.ui.jsp.beans.GroupUIBean;
import com.sap.isa.ipc.ui.jsp.beans.InstanceUIBean;
import com.sap.isa.ipc.ui.jsp.constants.HeaderRequestParameterConstants;
import com.sap.isa.ipc.ui.jsp.uiclass.UIContext;

/**
 * @author 
 *
 * To change the template for this generated type comment go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
public class GetHeaderAction extends IPCBaseAction {

    /* (non-Javadoc)
     * @see com.sap.isa.core.BaseAction#doPerform(org.apache.struts.action.ActionMapping, org.apache.struts.action.ActionForm, javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse)
     */
	public ActionForward ecomPerform(ActionMapping mapping,
		ActionForm form,
		HttpServletRequest request,
		HttpServletResponse response,
		UserSessionData userSessionData,
		RequestParser requestParser,
		MetaBusinessObjectManager mbom,
		boolean multipleInvocation,
		boolean browserBack)
			throws IOException, ServletException {
            UIContext uiContext = getUIContext(request);
            ConfigUIBean configUIBean = this.getCurrentConfiguration(uiContext);
			String currentInstanceId = getContextValue(request, Constants.CURRENT_INSTANCE_ID);
//			if (!isSet(currentInstanceId)) {
//				currentInstanceId = getContextValue(request, Constants.CURRENT_INSTANCE_ID);
//			}
			if (!isSet(currentInstanceId)) {
				currentInstanceId = uiContext.getCurrentConfiguration().getRootInstance().getId();
			}
            InstanceUIBean currentInstanceUIBean = this.getCurrentInstance(uiContext, configUIBean, currentInstanceId, request);
            
			String curCharGroupName = getContextValue(request, Constants.CURRENT_CHARACTERISTIC_GROUP_NAME);
//			if (!isSet(curCharGroupName)) {
//				curCharGroupName = getContextValue(request, Constants.CURRENT_CHARACTERISTIC_GROUP_NAME);
//			}
			String curScrollGroupName = curScrollGroupName = getContextValue(request, Constants.CURRENT_SCROLL_CHARACTERISTIC_GROUP_NAME);
//			if (!isSet(curScrollGroupName)) {
//				curScrollGroupName = getContextValue(request, Constants.CURRENT_SCROLL_CHARACTERISTIC_GROUP_NAME);
//			}
			String groupStatusChangeName = getContextValue(request, Constants.GROUP_STATUS_CHANGE);
//			if (!isSet(groupStatusChangeName)) {
//				groupStatusChangeName = getContextValue(request, Constants.GROUP_STATUS_CHANGE);
//			}
			Hashtable groupStatus = (Hashtable) uiContext.getProperty(Constants.GROUP_STATUS_CHANGE);
			GroupUIBean currentGroupUIBean = this.getCharacteristicGroups(uiContext, currentInstanceUIBean, curCharGroupName, curScrollGroupName, groupStatusChangeName, groupStatus, request);
            
			customerExit(mapping, form, request, response, userSessionData, requestParser, mbom, multipleInvocation, browserBack);

			request.setAttribute(HeaderRequestParameterConstants.CURRENT_CONFIGURATION, configUIBean);
			request.setAttribute(HeaderRequestParameterConstants.CURRENT_INSTANCE, currentInstanceUIBean);
			request.setAttribute(HeaderRequestParameterConstants.CURRENT_CHARACTERISTIC_GROUP, currentGroupUIBean);
            return mapping.findForward(ActionForwardConstants.SUCCESS);
        //return processForward(mapping, request, ActionForwardConstants.SUCCESS);
    }

    public static void main(String[] args) {
    }

}
