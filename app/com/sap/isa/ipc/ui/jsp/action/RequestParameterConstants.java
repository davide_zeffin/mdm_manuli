

package com.sap.isa.ipc.ui.jsp.action;

/**
 * Contains request parameter constants.
 * New parameter names (not the constants) should be added in Java notation.
 * (showOneToOneConditions instead of show_one_to_one_conditions)
 */
public interface RequestParameterConstants extends com.sap.spc.remote.client.object.OriginalRequestParameterConstants,InternalRequestParameterConstants {
  /**
   *   All constants have been moved to the interface com.sap.spc.remote.client.object.RequestParameterConstants,
   *   since the UIContext in the spc layer needs to access the constants. This behaviour is
   *   dirty and has to be changed in version 3.1. The UIContext should become
   *   part of the ipcclient!
   */
}
