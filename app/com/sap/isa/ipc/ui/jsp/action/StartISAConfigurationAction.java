package com.sap.isa.ipc.ui.jsp.action;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.sap.isa.businessobject.ipc.IPCBOManager;
import com.sap.isa.core.UserSessionData;
import com.sap.isa.core.businessobject.management.MetaBusinessObjectManager;
import com.sap.isa.core.util.RequestParser;
import com.sap.isa.core.util.RequestParser.Parameter;
import com.sap.isa.ipc.ui.jsp.dynamicui.IPCUIModel;
import com.sap.isa.ipc.ui.jsp.uiclass.UIContext;
import com.sap.spc.remote.client.object.Configuration;
import com.sap.spc.remote.client.object.IPCClient;
import com.sap.spc.remote.client.object.IPCException;
import com.sap.spc.remote.client.object.IPCItem;
import com.sap.spc.remote.client.object.OriginalRequestParameterConstants;
import com.sap.spc.remote.client.object.imp.rfc.RFCIPCItemReference;

public class StartISAConfigurationAction extends StartConfigurationAction {

    public ActionForward ecomPerform(
        ActionMapping mapping,
        ActionForm form,
        HttpServletRequest request,
        HttpServletResponse response,
        UserSessionData userSessionData,
        RequestParser requestParser,
        MetaBusinessObjectManager mbom,
        boolean multipleInvocation,
        boolean browserBack)
        throws IOException, ServletException {
        
        UIContext uiContext = IPCBaseAction.getUIContext(request);
        if (uiContext == null) { //the uiContext has not been initialized
            ActionForward forward = processScenarioParameter(mapping, request);
            if (!forward.getName().equals(ActionForwardConstants.SUCCESS)) return forward;
            uiContext = IPCBaseAction.getUIContext(request); //uiContext created in processScenarioParameter
            // save the ipc_scenario to identify a switch of the scenario
            String ipcScenarioName = (String)request.getAttribute(InternalRequestParameterConstants.IPC_XCM_SCENARIO); 
            if (ipcScenarioName != null) {
                uiContext.setProperty(InternalRequestParameterConstants.IPC_XCM_SCENARIO, ipcScenarioName);
            }
        }
        else {
            // Check whether the scenario changed since last time (switch between catalog and basket).
            // If there was a switch the scenario parameters have to be re-read.
            String oldScenarioName = uiContext.getPropertyAsString(InternalRequestParameterConstants.IPC_XCM_SCENARIO);
            String newScenarioName = (String)request.getAttribute(InternalRequestParameterConstants.IPC_XCM_SCENARIO);
            if ((oldScenarioName != null) && (!oldScenarioName.equals(newScenarioName))) {
                // scenario has changed re-read the scenario parameters
                ActionForward forward = processScenarioParameter(mapping, request);
                if (!forward.getName().equals(ActionForwardConstants.SUCCESS)) return forward;
                uiContext = IPCBaseAction.getUIContext(request); //uiContext created in processScenarioParameter
                // save the changed scenario name
                if (newScenarioName != null) {
                    uiContext.setProperty(InternalRequestParameterConstants.IPC_XCM_SCENARIO, newScenarioName);
                }
            }
        }
            
        // reset the client -> the items will reload
        // if caller equals catalog it's not necessary to reload all static
        // items once again. All others which change the list of items
        // (order, basket) must reload.
        Parameter callerParam = requestParser.getAttribute(CALLER);
        uiContext.setCaller(callerParam.getValue().getString());

        RFCIPCItemReference itemReference =
            (RFCIPCItemReference) request.getAttribute(IPC_ITEM_REFERENCE);
        if (itemReference == null) {
                IPCException ipcException =
                    new IPCException("IPCItemReference == null in StartConfigurationAction!");
            setExtendedRequestAttribute(request, IPC_EXCEPTION, ipcException);
                log.error(this, ipcException);
                return (mapping.findForward(INTERNAL_ERROR));
        }

        IPCBOManager ipcBoManager =
            (IPCBOManager) userSessionData.getBOM(IPCBOManager.NAME);
        if (ipcBoManager == null) {
            log.error("No IPC BOM in session data!");
            return (mapping.findForward(INTERNAL_ERROR));
        }

        String client = request.getParameter(CLIENT);

        customerExit(mapping, form, request, response, userSessionData, requestParser, mbom, multipleInvocation, browserBack);

        IPCClient ipcClient;
        String connectionKey = itemReference.getConnectionKey();
        //the caller of the configuration requests the client to use the same connection
        ipcClient = ipcBoManager.createIPCClientUsingConnection(connectionKey);
            
        // item must exist
        IPCItem ipcItem = ipcClient.getIPCItem(itemReference);
        if (ipcItem == null) {
            IPCException ipcException =
                new IPCException(
                    "The item "
                        + itemReference.toString()
                        + " could not be found on the IPC server!");
            setExtendedRequestAttribute(request, IPC_EXCEPTION, ipcException);
            log.error(this, ipcException);
            return mapping.findForward(INTERNAL_ERROR);
        }
        
        // If item is product variant set the productVariant mode to true.
        setProductVariantMode(uiContext, ipcItem);
        
        Configuration configuration = ipcItem.getConfiguration();
        if (configuration == null) {
            IPCException ipcException =
                new IPCException(
                    "No configuration associated to item " + ipcItem + "!");
            setExtendedRequestAttribute(request,
                InternalRequestParameterConstants.IPC_EXCEPTION,
                ipcException);
            return mapping.findForward(ActionForwardConstants.INTERNAL_ERROR);
        }

        // In the ECO-scenario it is possible to view several items in the same session.
        // Therefore the UIModel data needs to be reset otherwise it would interfere with
        // UIModel data from another item.
        IPCUIModel.resetUIModelContextData(uiContext);

        // find UI-model GUID
        String scenario = getUIModelScenario(callerParam.getValue().getString());
        String uiModelGUID = "";
        try {
            uiModelGUID = ipcClient.getUIModelGUID(configuration.getKnowledgeBase(), scenario, "");
        }
        catch (IPCException e){
            log.error("Problems during execution of UI-Model-Find: " + e.getMessage()); 
        }
        request.setAttribute(UIMODEL_GUID, uiModelGUID);



        uiContext.setCaller(callerParam.getValue().getString());
        uiContext.setCurrentConfiguration(configuration);
        //initialize the context
        setInitialContextValues(request, configuration);
        
        return mapping.findForward(ActionForwardConstants.SUCCESS);
    }

    /**
     * @param string
     * @return
     */
    private String getUIModelScenario(String caller) {
        String scenario = "";
        if (caller != null){
            if ( caller.equals(OriginalRequestParameterConstants.CALLER_B2B_BASKET)
                || caller.equals(OriginalRequestParameterConstants.CALLER_B2B_ORDERSTATUS)
                || caller.equals(OriginalRequestParameterConstants.CALLER_B2B_ORDER)
                || caller.equals(OriginalRequestParameterConstants.CALLER_BACK_TO_CATALOG)){
                scenario = OriginalRequestParameterConstants.UIMODEL_SCENARIO_B2B;
            }
            else if (caller.equals(OriginalRequestParameterConstants.CALLER_B2C_BASKET)
                || caller.equals(OriginalRequestParameterConstants.CALLER_B2C_ORDERSTATUS)
                || caller.equals(OriginalRequestParameterConstants.CALLER_B2C_CATALOG)){
                scenario = OriginalRequestParameterConstants.UIMODEL_SCENARIO_B2C;
            }
            else{
            }
        }
        return scenario;
    }

}
