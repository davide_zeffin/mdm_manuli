/*****************************************************************************
  Class:        LogoffAction
  Copyright (c) 2001, SAPMarkets Europe GmbH, Germany, All rights reserved.
  Author:       Franz-Dieter Berger
  Created:      21.05.2001
  Version:      1.0

  $Revision: #1 $
  $Date: 2001/06/26 $
*****************************************************************************/

package com.sap.isa.ipc.ui.jsp.action;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.sap.isa.core.UserSessionData;
import com.sap.isa.core.businessobject.management.MetaBusinessObjectManager;
import com.sap.isa.core.util.RequestParser;
import com.sap.isa.ipc.ui.jsp.uiclass.UIContext;
import com.sap.spc.remote.client.object.IPCClient;
import com.sap.spc.remote.client.object.IPCSession;

/**
 * Logs off, by invalidating the session.
 *
 * @author Franz-Dieter Berger
 * @version 1.0
 */
public class LogoffAction extends IPCBaseAction {

    /**
     * Overriden <em>ecomPerform</em> method of <em>EcomBaseAction</em>.
     */
	public ActionForward ecomPerform(ActionMapping mapping,
		ActionForm form,
		HttpServletRequest request,
		HttpServletResponse response,
		UserSessionData userSessionData,
		RequestParser requestParser,
		MetaBusinessObjectManager mbom,
		boolean multipleInvocation,
		boolean browserBack)
			throws IOException, ServletException {
				
	final String METHOD_NAME = "ecomPerform()";
	log.entering(METHOD_NAME);
      HttpSession session = request.getSession();

	  UIContext uiContext = getUIContext(request);

	  if(uiContext == null)
		  return (mapping.findForward(ActionForwardConstants.INTERNAL_ERROR));
		  
	  IPCClient client = (IPCClient)uiContext.getProperty(InternalRequestParameterConstants.IPC_CLIENT);
	  if (client != null) {
		IPCSession ipcsession = client.getIPCSession();
		if (ipcsession != null) {
			client.getIPCSession().close();
		}
	  }
	  
	  session.invalidate();

     
	  log.exiting();
      return mapping.findForward(ActionForwardConstants.SUCCESS);
  }
}
