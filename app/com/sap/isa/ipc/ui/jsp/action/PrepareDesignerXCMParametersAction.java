package com.sap.isa.ipc.ui.jsp.action;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.sap.isa.businessobject.CommunicationException;
import com.sap.isa.core.UserSessionData;
import com.sap.isa.core.businessobject.management.MetaBusinessObjectManager;
import com.sap.isa.core.util.RequestParser;

public class PrepareDesignerXCMParametersAction
    extends IPCBaseAction
    implements RequestParameterConstants, ActionForwardConstants {

    /* (non-Javadoc)
     * @see com.sap.isa.isacore.action.EComBaseAction#ecomPerform(org.apache.struts.action.ActionMapping, org.apache.struts.action.ActionForm, javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse, com.sap.isa.core.UserSessionData, com.sap.isa.core.util.RequestParser, com.sap.isa.core.businessobject.management.MetaBusinessObjectManager, boolean, boolean)
     */
    public ActionForward ecomPerform(
        ActionMapping mapping,
        ActionForm form,
        HttpServletRequest request,
        HttpServletResponse response,
        UserSessionData userSessionData,
        RequestParser requestParser,
        MetaBusinessObjectManager mbom,
        boolean multipleInvocation,
        boolean browserBack)
        throws IOException, ServletException, CommunicationException {

			log.entering("ecomPerform()"); 

			request.setAttribute(SHOW_PICTURE, "T");
			request.setAttribute(SHOW_EMPTY_GROUPS, "T");
			request.setAttribute(SHOW_DETAILS, "T");
			request.setAttribute(SHOW_INVISIBLE_CHARACTERISTICS, "T");
			request.setAttribute(SHOW_LINK_TO_ADD_CSTIC_MIMES, "T");
			request.setAttribute(ASSIGNABLE_VALUES_ONLY, "F");
			request.setAttribute(SHOW_LINK_TO_ADD_VALUE_MIMES, "T");
			request.setAttribute(SHOW_MULTI_FUNCTIONALITY_AREA, "T");
			request.setAttribute(ENABLE_VARIANT_SEARCH, "T");
			
			log.exiting();
			return mapping.findForward(ActionForwardConstants.SUCCESS);

    }

	

}
