
package com.sap.isa.ipc.ui.jsp.action;

import java.io.IOException;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.sap.isa.core.Constants;
import com.sap.isa.core.SessionConst;
import com.sap.isa.core.UserSessionData;
import com.sap.isa.core.businessobject.management.MetaBusinessObjectManager;
import com.sap.isa.core.util.RequestParser;
import com.sap.isa.core.util.StartupParameter;
import com.sap.isa.ipc.ui.jsp.uiclass.UIContext;

public class ProcessRequestParameterAction extends IPCBaseAction
			  implements RequestParameterConstants, ActionForwardConstants {

	public ActionForward ecomPerform(ActionMapping mapping,
		ActionForm form,
		HttpServletRequest request,
		HttpServletResponse response,
		UserSessionData userSessionData,
		RequestParser requestParser,
		MetaBusinessObjectManager mbom,
		boolean multipleInvocation,
		boolean browserBack)
			throws IOException, ServletException {

		HttpSession session = request.getSession(false);
		if (session == null) {
			log.fatal("Could not get HttpSession in IPCBaseAction!");
		}

		// read businessobjectmanager from Session 
		UserSessionData userData = UserSessionData.getUserSessionData(session);
		UIContext uiContext = getUIContext(request);

		Map uiNameParameters =
			(Map)request.getAttribute(UINAME_PARAMETERS);
		if (uiNameParameters == null) {
			uiNameParameters = (Map)uiContext.getProperty(UINAME_PARAMETERS);
		}else {
			uiContext.setProperty(UINAME_PARAMETERS, uiNameParameters);
		}
		if (uiNameParameters == null) {
			uiNameParameters = new HashMap();
		}

	   // read all paramters for customizing the IPC UI
		getParameter(request,SHOW_APPLY_BUTTON, uiContext, uiNameParameters);
		getParameter(request,APPLY_BUTTON_ACTION, uiContext, uiNameParameters);
		getParameter(request,SHOW_BACK_BUTTON, uiContext, uiNameParameters);
		getParameter(request,BACK_BUTTON_ACTION, uiContext, uiNameParameters);
		getParameter(request,TOPFRAME, uiContext, uiNameParameters);
		getParameter(request, LIMIT_OF_PRODUCT_VARIANTS, uiContext, uiNameParameters);
		getParameter(request, QUALITY_OF_PRODUCT_VARIANTS, uiContext, uiNameParameters);
		getParameter(request,IPC_SCENARIO, uiContext, uiNameParameters);
		getParameter(request,STYLESHEET, uiContext, uiNameParameters);
		getParameter(request,CALLER_MESSAGE, uiContext, uiNameParameters);
		getParameter(request,HOOK_URL, uiContext, uiNameParameters);

		// true or false parameters
		getParameter(request,CHECK_PRICING,uiContext, uiNameParameters);
		getParameter(request,DISPLAY_MODE,uiContext, uiNameParameters);
		getParameter(request,CHANGEABLE_PRODUCT_VARIANT_MODE,uiContext, uiNameParameters);
		getParameter(request,DESIGNER_MODE, uiContext, uiNameParameters);
		getParameter(request,UIMODEL_GUID, uiContext, uiNameParameters);
		
        // If the UI is running in "product variant mode" we set the display-mode to T.
        if (uiContext.getPropertyAsBoolean(PRODUCT_VARIANT_MODE, false) ){
        	// We have to store also the original display mode that it is possible to find out 
            // the original display mode when switching to the KMAT (i.e. whether the button
            // to switch to KMAT should be displayed).
            uiContext.setProperty(ORIGINAL_DISPLAY_MODE, uiContext.getDisplayMode());
			              
            //only for fix variants
            if (!uiContext.getChangeableProductVariantMode()){
        		uiContext.setDisplayMode(true);
			}
        }
		getParameter(request,ENABLE_SETTINGS,uiContext, uiNameParameters);
		getParameter(request,HIDE_PRICES,uiContext, uiNameParameters);
		getParameter(request,ONLINE_EVALUATE,uiContext, uiNameParameters);
		getParameter(request,SHOW_PICTURE,uiContext, uiNameParameters);
		getParameter(request,PRICING_STATISTICAL,uiContext, uiNameParameters); 
		
		// check value of SHOW_CONFIG_PICTURES -> DynamicProductPicture enable/disable
		getParameter(request,SHOW_DYNAMIC_PRODUCT_PICTURE,uiContext, uiNameParameters);
		getParameter(request,SHOW_CUSTOMIZATION_LIST,uiContext, uiNameParameters);
		getParameter(request,SHOW_PRICES_IN_CUSTOMIZATION_LIST, uiContext, uiNameParameters);
		getParameter(request,CUSTOMIZATION_LIST_ROOT_INSTANCE_PRICES, uiContext, uiNameParameters);
		getParameter(request,CUSTOMIZATION_LIST_SUB_INSTANCES_PRICES, uiContext, uiNameParameters);
		getParameter(request,CUSTOMIZATION_LIST_TOTAL_PRICES, uiContext, uiNameParameters);
		//initialize container of price display customizing, this container prepares the data for convenient access
		uiContext.setPriceDisplayCustomizing(uiContext.getPropertyAsString(RequestParameterConstants.CUSTOMIZATION_LIST_ROOT_INSTANCE_PRICES, ""),
		                                     uiContext.getPropertyAsString(RequestParameterConstants.CUSTOMIZATION_LIST_SUB_INSTANCES_PRICES, ""),
		                                     uiContext.getPropertyAsString(RequestParameterConstants.CUSTOMIZATION_LIST_TOTAL_PRICES, ""));
		getParameter(request,SHOW_ONE_TO_ONE_CONDITIONS, uiContext, uiNameParameters);
		getParameter(request,SHOW_CHARACTERISTIC_VALUE_3D_PICTURE, uiContext, uiNameParameters);
		getParameter(request,CHARACTERISTICS_DESCRIPTION_TYPE, uiContext, uiNameParameters);
		getParameter(request,VALUES_DESCRIPTION_TYPE, uiContext, uiNameParameters);
		getParameter(request,CHARACTERISTICS_MAX_DESCRIPTION_LENGTH, uiContext, uiNameParameters);
		getParameter(request,VALUES_MAX_DESCRIPTION_LENGTH, uiContext, uiNameParameters);
		
		getParameter(request,SHOW_INVISIBLE_CHARACTERISTICS, uiContext, uiNameParameters);
		getParameter(request,SHOW_GENERAL_TAB_FIRST, uiContext, uiNameParameters);
		getParameter(request,SHOW_DETAILS, uiContext, uiNameParameters);
		getParameter(request,SHOW_DETAILS_IN_NEW_WINDOW, uiContext, uiNameParameters);
		getParameter(request,SHOW_MULTI_FUNCTIONALITY_AREA, uiContext, uiNameParameters);
		getParameter(request,USE_GROUP_INFORMATION, uiContext, uiNameParameters);
		getParameter(request,SHOW_EMPTY_GROUPS,uiContext, uiNameParameters);
		getParameter(request,SHOW_CSTIC_VALUES_EXPANDED, uiContext, uiNameParameters);
		getParameter(request,ASSIGNABLE_VALUES_ONLY, uiContext, uiNameParameters);
		getParameter(request,SHOW_LANGUAGE_DEPENDENT_NAMES, uiContext, uiNameParameters);
		getParameter(request,SHOW_STATUS_LIGHTS, uiContext, uiNameParameters);
		getParameter(request,USE_EXPLANATION_TOOL, uiContext, uiNameParameters);
		getParameter(request,USE_CONFLICT_SOLVER, uiContext, uiNameParameters);
		getParameter(request,CONFLICT_SOLVER_SHOW_CONFLICT_SOLUTION_RATE, uiContext, uiNameParameters);
		getParameter(request,USE_CONFLICT_EXPLANATION, uiContext, uiNameParameters);
		getParameter(request,CONFLICT_EXPLANATION_LEVEL, uiContext, uiNameParameters);
		getParameter(request,CONFLICT_EXPLANATION_TEXT_BLOCK_TAG, uiContext, uiNameParameters);
		getParameter(request,CONFLICT_EXPLANATION_TEXT_LINE_TAG,  uiContext, uiNameParameters);
		getParameter(request,SHOW_MESSAGE_AREA, uiContext, uiNameParameters);
		getParameter(request,SHOW_STANDARD_MESSAGE_IN_MESSAGE_AREA, uiContext, uiNameParameters);
		getParameter(request,USE_CONFLICT_HANDLING_SHORTCUTS, uiContext, uiNameParameters);
		getParameter(request,USE_SHORTCUT_VALUE_CONFLICT, uiContext, uiNameParameters);
		getParameter(request,ENABLE_VARIANT_SEARCH,uiContext, uiNameParameters);
		getParameter(request,CHANGEABLE_PRODUCT_VARIANT_MODE,uiContext, uiNameParameters);
		getParameter(request,SHOW_MIMES_IN_NEW_WINDOW, uiContext, uiNameParameters);
		getParameter(request,SHOW_INNER_SPACES_IN_LANGUAGE_DEPENDENT_NAMES, uiContext, uiNameParameters);
		getParameter(request,SHOW_INNER_SPACES_IN_LANGUAGE_INDEPENDENT_NAMES, uiContext, uiNameParameters);
		getParameter(request,SHOW_CUSTOMIZATIONLIST_INSTANCES_EXPANDED, uiContext, uiNameParameters);
		getParameter(request,SHOW_CONFLICT_TRACE,uiContext, uiNameParameters);
		getParameter(request,SHOW_CUSTOMER_TAB,uiContext, uiNameParameters);
		getParameter(request,MFA_TAB_ORDER, uiContext, uiNameParameters);
        getParameter(request,INPUTFIELD_MASK, uiContext, uiNameParameters);
        getParameter(request,CALENDAR_CONTROL,uiContext, uiNameParameters);
        getParameter(request, CALENDAR_CONTROL_EMBEDDED, uiContext, uiNameParameters);
        getParameter(request, SHOW_PROGRESS_INDICATOR, uiContext, uiNameParameters);        
        getParameter(request, ENABLE_HELP_POPUP, uiContext, uiNameParameters);
        getParameter(request, SHOW_LINK_TO_ADD_VALUE_MIMES, uiContext, uiNameParameters);
        getParameter(request, SHOW_LINK_TO_ADD_CSTIC_MIMES, uiContext, uiNameParameters);
        getParameter(request, MULITIPLE_VALUES_THRESHOLD, uiContext, uiNameParameters);
        getParameter(request, CSTIC_SHOW_EXPAND_LINK, uiContext, uiNameParameters);
        getParameter(request, MIMES_MASTERDATA, uiContext, uiNameParameters);
        getParameter(request, SHOW_GROUP_NAME_IN_WORK_AREA, uiContext, uiNameParameters);
		getParameter(request, AUTOMATIC_COLLAPSE_COMPONENTS, uiContext, uiNameParameters);
		getParameter(request, AUTOMATIC_COLLAPSE_GROUPS, uiContext, uiNameParameters);
		
		// import/export configuration
		getParameter(request,IMPORT_EXPORT_MODE, uiContext, uiNameParameters);
        
        // search/set 
        getParameter(request,SEARCH_SET_MODE, uiContext, uiNameParameters);
        getParameter(request,SEARCH_SET_DELIMITER, uiContext, uiNameParameters);
        
		getParameter(request, MATCH_POINTS_OF_PRODUCT_VARIANTS, uiContext, uiNameParameters);
		getParameter(request, ONLY_VAR_FIND, uiContext, uiNameParameters);

		// The following code reads all attributes for the customer buttons
		getParameter(request,SHOW_CUSTOMER_BUTTON1, uiContext, uiNameParameters);
		getParameter(request,SHOW_CUSTOMER_BUTTON2, uiContext, uiNameParameters);
		getParameter(request,SHOW_CUSTOMER_BUTTON3, uiContext, uiNameParameters);

		getParameter(request,CUSTOMER_BUTTON1_LABEL, uiContext, uiNameParameters);
		getParameter(request,CUSTOMER_BUTTON2_LABEL, uiContext, uiNameParameters);
		getParameter(request,CUSTOMER_BUTTON3_LABEL, uiContext, uiNameParameters);

		getParameter(request,CUSTOMER_BUTTON1_AVAILABLE_IF_COMPLETE_ONLY, uiContext, uiNameParameters);
		getParameter(request,CUSTOMER_BUTTON2_AVAILABLE_IF_COMPLETE_ONLY, uiContext, uiNameParameters);
		getParameter(request,CUSTOMER_BUTTON3_AVAILABLE_IF_COMPLETE_ONLY, uiContext, uiNameParameters);

		getParameter(request,CUSTOMER_BUTTON1_ACTION, uiContext, uiNameParameters);
		getParameter(request,CUSTOMER_BUTTON2_ACTION, uiContext, uiNameParameters);
		getParameter(request,CUSTOMER_BUTTON3_ACTION, uiContext, uiNameParameters);

		getParameter(request,CUSTOMER_BUTTON1_FORWARD, uiContext, uiNameParameters);
		getParameter(request,CUSTOMER_BUTTON2_FORWARD, uiContext, uiNameParameters);
		getParameter(request,CUSTOMER_BUTTON3_FORWARD, uiContext, uiNameParameters);

		getParameter(request,CUSTOMER_BUTTON1_TARGET, uiContext, uiNameParameters);
		getParameter(request,CUSTOMER_BUTTON2_TARGET, uiContext, uiNameParameters);
		getParameter(request,CUSTOMER_BUTTON3_TARGET, uiContext, uiNameParameters);
        
        // accesskey parameters
        this.getParameter(request, ACCESSKEY_BUTTONS,  uiContext, uiNameParameters);
        this.getParameter(request, ACCESSKEY_CSTICS,  uiContext, uiNameParameters);
        this.getParameter(request, ACCESSKEY_GROUPLIST,  uiContext, uiNameParameters);
        this.getParameter(request, ACCESSKEY_GROUPS,  uiContext, uiNameParameters);
        this.getParameter(request, ACCESSKEY_INSTANCES,  uiContext, uiNameParameters);
        this.getParameter(request, ACCESSKEY_MFA,  uiContext, uiNameParameters);
        this.getParameter(request, ACCESSKEY_SEARCH,  uiContext, uiNameParameters);
        this.getParameter(request, ACCESSKEY_ACCEPTBUTTON,  uiContext, uiNameParameters);
        this.getParameter(request, ACCESSKEY_CANCELBUTTON, uiContext, uiNameParameters);
        this.getParameter(request, ACCESSKEY_RESETBUTTON,  uiContext, uiNameParameters);
        this.getParameter(request, ACCESSKEY_LASTFOCUSEDCSTIC,  uiContext, uiNameParameters);

        // taborder parameters
        this.getParameter(request, TABORDER_BUTTONS,  uiContext, uiNameParameters);
        this.getParameter(request, TABORDER_CSTICS,  uiContext, uiNameParameters);
        this.getParameter(request, TABORDER_GROUPLIST,  uiContext, uiNameParameters);
        this.getParameter(request, TABORDER_GROUPS,  uiContext, uiNameParameters);
        this.getParameter(request, TABORDER_INSTANCES,  uiContext, uiNameParameters);
        this.getParameter(request, TABORDER_MFA,  uiContext, uiNameParameters);
        this.getParameter(request, TABORDER_SEARCH,  uiContext, uiNameParameters);

        // loading message parameters
        this.getParameter(request, SHOW_LOADING_MESSAGES, uiContext, uiNameParameters);
        this.getParameter(request, LOADING_MESSAGES_AUTOMATIC_FORWARD, uiContext, uiNameParameters);

        // comparison parameters
        this.getParameter(request, ENABLE_SNAPSHOT, uiContext, uiNameParameters);
        this.getParameter(request, ENABLE_COMPARISON, uiContext, uiNameParameters);
		
        // in-screen messaging parameters
        this.getParameter(request, SHOW_MESSAGE_CSTICS, uiContext, uiNameParameters);
        this.getParameter(request, MESSAGE_CSTICS_PREFIX, uiContext, uiNameParameters);
        this.getParameter(request, MESSAGE_CSTICS_SHOW_ID, uiContext, uiNameParameters);
        
        if (uiContext.getLocale() == null) {
			uiContext.setLocale(userData.getLocale());
		}

		String[] charact = this.getParameters(request, EXT_ATTR_CHARC);
		if (charact != null) {
			String[] visible = this.getParameters(request, EXT_ATTR_VISIBLE);
			uiContext.setCharacteristicsVisibleView(charact, visible);

			String[] readonly = this.getParameters(request, EXT_ATTR_READONLY);
			uiContext.setCharacteristicsReadonlyView(charact, readonly);
		}

		// Now check for all custom properties
		Enumeration attributeNames = request.getAttributeNames();
		while (attributeNames.hasMoreElements()) {
		  String attributeName = (String) attributeNames.nextElement();
		  if (attributeName.startsWith(CUSTOM + ".")) {
			uiContext.setCustomerProperty(attributeName,request.getAttribute(attributeName));
		  }
		}
        
        // check whether the UI runs in accessibility mode
        boolean isAccessible = false;
        if (userSessionData != null) {
            StartupParameter startupParameter = (StartupParameter)userSessionData.getAttribute(SessionConst.STARTUP_PARAMETER);
            if (startupParameter.getParameterValue(Constants.ACCESSIBILITY).equalsIgnoreCase("X")) {
                isAccessible = true;
            }
        }
        uiContext.setProperty(Constants.ACCESSIBILITY, isAccessible);

		customerExit(mapping, form, request, response, userSessionData, requestParser, mbom, multipleInvocation, browserBack);

        return processForward(mapping, request, ActionForwardConstants.SUCCESS);	
    }

	/*
	 * @return an array of parameter values
	 */
	private String[] getParameters(HttpServletRequest request, String key) {
		SimpleRequestWrapper simpleRequestWrapper = new SimpleRequestWrapper(request);
		String[] values = simpleRequestWrapper.getParameterValues(key);
		return values;
	}

	/**
	 * This method automatically searches for a parameter in the web.xml and
	 * if it doesn't exist there, it tries to fetch it from the request.
	 * If it's able to find the parameter in one of both ways, it sets it directely
	 * in the UI context. Optionally you can specify a default value that will
	 * be used in case that no parameter could be found.
	 * The return value is the value that was set in the ui context. However if
	 * no value has been set, the returned value will be null.
	 */
	private void getParameter(
		HttpServletRequest request,
		String parameterName,
		UIContext uiContext,
		Map uiNameParameters) {
		String stringParameter = null;

		//request parameter have priority over request attributes
		if (request.getParameter(parameterName) != null
			&& !request.getParameter(parameterName).equals("")) {
			stringParameter = request.getParameter(parameterName);
		//request attributes have prio over uiNameParameters
		}else if (request.getAttribute(parameterName) != null) {
			stringParameter = (String) request.getAttribute(parameterName);
		//uiNameParameters have prio over scenarioParameter
		} else if (uiNameParameters != null && uiNameParameters.get(parameterName) != null) {
			stringParameter = (String) uiNameParameters.get(parameterName);
		//tm 27.01.2005 scenario parameters written directly to uiContext instead of 
		//request Attribute in IPCBaseAction.processScenarioParameters.
//		} else if (scenarioParameters != null && scenarioParameters.get(parameterName) != null) {
//			stringParameter = (String) scenarioParameters.get(parameterName);
//		} else if (defaultValue != null) {
//			if (log.isDebugEnabled())
//				log.debug("Using default value " + defaultValue + " for parameter "
//				 + parameterName + " from " + this.getClass().getName());
//			stringParameter = defaultValue;
		}
		// assign value to uiContext
		if (stringParameter != null) {
			if (uiContext.getProperty(parameterName) != null) {
				//overwrite scenario parameter with request parameter
				uiContext.removeProperty(parameterName);
			}
			uiContext.setProperty(parameterName, stringParameter);
		}
	}
}
