/*
 * Created on 13.01.2005
 *
 */
package com.sap.isa.ipc.ui.jsp.action;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.sap.isa.businessobject.ipc.IPCBOManager;
import com.sap.isa.core.UserSessionData;
import com.sap.isa.core.businessobject.management.MetaBusinessObjectManager;
import com.sap.isa.core.util.RequestParser;
import com.sap.isa.core.util.RequestParser.Parameter;
import com.sap.isa.ipc.ui.jsp.uiclass.UIContext;
import com.sap.spc.remote.client.object.Configuration;
import com.sap.spc.remote.client.object.IPCClient;
import com.sap.spc.remote.client.object.IPCConfigReference;
import com.sap.spc.remote.client.object.IPCException;
import com.sap.spc.remote.client.object.KnowledgeBase;
import com.sap.spc.remote.client.object.imp.DefaultKnowledgeBase;
import com.sap.spc.remote.client.object.imp.IllegalClassException;
import com.sap.spc.remote.client.object.imp.rfc.RFCIPCConfigReference;
import com.sap.spc.remote.client.object.imp.rfc.RfcDefaultIPCClient;
import com.sap.spc.remote.client.util.ext_configuration;

/**
 * @author 
 *
 */
public class StartOnlyConfigurationAction
	extends StartConfigurationAction {

	public ActionForward ecomPerform(
		ActionMapping mapping,
		ActionForm form,
		HttpServletRequest request,
		HttpServletResponse response,
		UserSessionData userSessionData,
		RequestParser requestParser,
		MetaBusinessObjectManager mbom,
		boolean multipleInvocation,
		boolean browserBack)
		throws IOException, ServletException {

			UIContext uiContext = IPCBaseAction.getUIContext(request);
			if (uiContext == null) { //the uiContext has not been initialized
			ActionForward forward = processScenarioParameter(mapping, request);
				if (!forward.getName().equals(ActionForwardConstants.SUCCESS)) return forward;
				uiContext = IPCBaseAction.getUIContext(request); //uiContext created in processScenarioParameter
			}
			super.htmlTrace(request);
			
//			Parameter typeParam = requestParser.getFromContext(RequestParameterConstants.TYPE);
//			String type;
//			if (!typeParam.isSet()) type = RequestParameterConstants.ISA; else type = typeParam.getValue().getString();
			Parameter productIdParam = requestParser.getFromContext(PRODUCT_ID);
			if (!productIdParam.isSet()) return this.parameterError(mapping, request, PRODUCT_ID, this.getClass().getName());
			Parameter callerParam = requestParser.getFromContext(CALLER);
			if (!callerParam.isSet()) return parameterError(mapping, request, RequestParameterConstants.CALLER, getClass().getName());
			uiContext.setCaller(callerParam.getValue().getString());
            Parameter kbLogSysParam = requestParser.getFromContext(KB_LOGSYS);
            if (!kbLogSysParam.isSet() || kbLogSysParam.getValue().getString().equals("")) return parameterError(mapping, request, RequestParameterConstants.KB_LOGSYS, getClass().getName());

			String kbDate = requestParser.getFromContext(KB_DATE).getValue().getString();
			String language = requestParser.getFromContext(LANGUAGE).getValue().getString();
			String kbName = requestParser.getFromContext(KB_NAME).getValue().getString();
			String kbVersion = requestParser.getFromContext(KB_VERSION).getValue().getString();
			String kbProfile = requestParser.getFromContext(KB_PROFILE).getValue().getString();
			String productType = requestParser.getFromContext(PRODUCT_TYPE).getValue().getString();
			if ( productType != null && productType.equals("") ) productType = null;
			if ( kbDate != null && kbDate.equals("") ) kbDate = null;

			if ( kbName != null && kbName.equals("") ) kbName = null;
			if ( kbVersion != null && kbVersion.equals("") ) kbVersion = null;
			if ( kbProfile != null && kbProfile.equals("") ) kbProfile = null;


			// external configuration available?
			ext_configuration extConfig = getExternalConfiguration(request);
			// context information
			String[] contextNames = getParameters(request, CONTEXT_NAME);
			String[] contextValues = getParameters(request, CONTEXT_VALUE);
            IPCConfigReference configReference = factory.newIPCConfigReference();
            IPCClient ipcClient = null;            
            if (!(configReference instanceof RFCIPCConfigReference)) {
                throw new IllegalClassException(configReference, RFCIPCConfigReference.class);
            }

			customerExit(mapping, form, request, response, userSessionData, requestParser, mbom, multipleInvocation, browserBack);

            if (log.isDebugEnabled()) {
//TODO                log.debug("Sessionid: " + configReference.getSessionId());
                log.debug("Configid: " + configReference.getConfigId());
            }
            IPCBOManager ipcBoManager =
                (IPCBOManager) userSessionData.getBOM(IPCBOManager.NAME);
            if (ipcBoManager == null) {
                log.error("No IPC BOM in session data!");
                return (mapping.findForward(INTERNAL_ERROR));
            }    

			// optional parameter
			// TODO:This is now handled by the Logon Module (crm/tc/user)
		    // if you want to connect to a speciffic server you can set thie in xcm jco config
		    // or you can use the Logon module for dynamic set		
			/**
			Parameter clientParam = requestParser.getParameter(RequestParameterConstants.CLIENT);
			Parameter appServerParam = requestParser.getParameter(IPC_HOST);
			Parameter sysNrParam = requestParser.getParameter(RequestParameterConstants.IPC_SYSNR);
			if (clientParam.isSet() && appServerParam.isSet() && sysNrParam.isSet()) {
				ipcClient = ipcBoManager.createIPCClient(clientParam.getValue().getString(),
				                                         language,
				                                         appServerParam.getValue().getString(),
				                                         sysNrParam.getValue().getString());
			}else {**/
				ipcClient = ipcBoManager.createIPCClient(language);
			/**
			}
			**/

            
            // kbName and kbVersion are necessary for the 5.0 fm
            // if kbName or kbVersion is not passed we request a list of kbs from the server
            if((kbName==null) || (kbVersion == null)){
                log.debug("kbName or kbVersion not passed. Requesting list of kbs for product " + productIdParam.getValue().getString() + " from server.");
                List listOfKBs = ((RfcDefaultIPCClient)ipcClient).getKnowledgeBasesForProduct(productIdParam.getValue().getString(), kbLogSysParam.getValue().getString());
                // by default we use the latest kb of this product.
                if (listOfKBs.size() > 0) {
                    KnowledgeBase kb = (KnowledgeBase)listOfKBs.get(0);
                    kbName = kb.getName();
                    kbVersion = kb.getVersion();
                    log.debug("Using kbName \"" + kbName + "\" and kbVersion \"" + kbVersion + "\"");
                }
                else {
                	String logMessage = "No knowledgebase found for product " + productIdParam.getValue().getString();
                    log.debug(logMessage);
                    //TODO forward to error page without dump
                    throw new IPCException(logMessage);           
                }
            }

			Configuration configuration;
			if ( extConfig != null ) 
			{
				configuration =
					ipcClient.createConfiguration(
						language,
						extConfig,
						contextNames,
						contextValues,
						kbDate);
			}
			else {
				configuration =	ipcClient.createConfiguration(
				    productIdParam.getValue().getString(),
				    productType,
				    language,
				    kbDate,
					kbLogSysParam.getValue().getString(),
					kbName,
					kbVersion,
					kbProfile,
					contextNames, contextValues);
			}


			// check for uimodel-guid
			String uiModelGUID = requestParser.getFromContext(UIMODEL_GUID).getValue().getString();
			if (uiModelGUID == null || uiModelGUID.equals("")){
				// no uiModelGuid has been passed: find UI model
				String scenario = requestParser.getFromContext(UIMODEL_SCENARIO).getValue().getString();
				String roleKey = requestParser.getFromContext(UIMODEL_ROLEKEY).getValue().getString();
				// we build our own kb-object to avoid an unnecessary call
				DefaultKnowledgeBase kb = factory.newKnowledgeBase(ipcClient, kbLogSysParam.getValue().getString(), kbName, kbVersion, "");
				try {
					uiModelGUID = ipcClient.getUIModelGUID(kb, scenario, roleKey);
				}
				catch (IPCException e){
					log.error("Problems during execution of UI-Model-Find: " + e.getMessage()); 
				}
				request.setAttribute(UIMODEL_GUID, uiModelGUID);
			}
								
			uiContext.setCurrentConfiguration(configuration);
			//initialize the context
			setInitialContextValues(request, configuration);

			return mapping.findForward(ActionForwardConstants.SUCCESS);
	}
}
