package com.sap.isa.ipc.ui.jsp.action;

import java.io.IOException;
import java.util.Hashtable;
import java.util.StringTokenizer;
import java.util.Vector;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.sap.isa.core.Constants;
import com.sap.isa.core.UserSessionData;
import com.sap.isa.core.businessobject.management.MetaBusinessObjectManager;
import com.sap.isa.core.ui.layout.UIArea;
import com.sap.isa.core.ui.layout.UILayout;
import com.sap.isa.core.ui.layout.UILayoutManager;
import com.sap.isa.core.util.RequestParser;
import com.sap.isa.ipc.ui.jsp.beans.ConfigUIBean;
import com.sap.isa.ipc.ui.jsp.beans.InstanceUIBean;
import com.sap.isa.ipc.ui.jsp.constants.MFATabRequestParameterConstants;
import com.sap.isa.ipc.ui.jsp.container.MfaTab;
import com.sap.isa.ipc.ui.jsp.uiclass.UIContext;

/**
 * This action finds the selected tabrider for the multi functionality
 * area. The tabs are read from the UIContext. If no tabs are contained
 * MfaTab-objects are created according to the parameters contained in web.xml.
 */
public class GetMFATabAction
	extends IPCBaseAction
	implements RequestParameterConstants, ActionForwardConstants {
	public ActionForward ecomPerform(
		ActionMapping mapping,
		ActionForm form,
		HttpServletRequest request,
		HttpServletResponse response,
		UserSessionData userSessionData,
		RequestParser requestParser,
		MetaBusinessObjectManager mbom,
		boolean multipleInvocation,
		boolean browserBack)
		throws IOException, ServletException {
		UIContext uiContext = getUIContext(request);
		//		if (!uiContext.getOnlineEvaluate() && !browserBack) {
		//			ActionForward forward =
		//				bufferCharacteristicValues(mapping, request);
		//			if (!forward.getName().equals(SUCCESS)) {
		//				return forward;
		//			}
		//		}

		//		String selectedTabName =
		//			request.getParameter(
		//			Constants.SELECTED_MFA_TAB_NAME);
		//		if (!isSet(selectedTabName)) {
		String selectedTabName =
			getContextValue(request, Constants.SELECTED_MFA_TAB_NAME);
		//		}
		MfaTab selectedTab;

		//tab-objects are generated here. If more Tab-Objects are needed, they
		//must be generated here
		//for each tab one object is needed.
		Vector tablist = new Vector(4);
		Hashtable tabmap = new Hashtable(4);
		//if the tabs should appear in a different order, change the order
		//of the if-commands
		if (uiContext
			.getPropertyAsBoolean(
				RequestParameterConstants.SHOW_CUSTOMIZATION_LIST)) {
			MfaTab tab =
				new MfaTab(
					InternalRequestParameterConstants.CUSTOMIZATION_LIST,
					"ipc.customization_tab",
					"/ipc/gotoCustomizationList.do");
			tabmap.put(tab.getName(), tab);
			tablist.add(tab);
		}

		if (uiContext
			.getPropertyAsBoolean(
				RequestParameterConstants.SHOW_DYNAMIC_PRODUCT_PICTURE)) {

			String currentInstanceId =
				getContextValue(request, Constants.CURRENT_INSTANCE_ID);
			//			if (!isSet(currentInstanceId)) {
			//				currentInstanceId = getContextValue(request, Constants.CURRENT_INSTANCE_ID);
			//			}
			ConfigUIBean currentConfig =
				this.getCurrentConfiguration(uiContext);
			InstanceUIBean instance =
				this.getCurrentInstance(
					uiContext,
					currentConfig,
					currentInstanceId,
					request);
			String productName = instance.getName();
			boolean productIsReleased =
				uiContext.isDynamicProductReleased(productName);
			if (productIsReleased) {
				MfaTab tab =
					new MfaTab(
						InternalRequestParameterConstants.DYNAMIC_PICTURE,
						"ipc.picture_tab",
						"/ipc/gotoDynamicPicture.do");
				tabmap.put(tab.getName(), tab);
				tablist.add(tab);
			}
		}

		if (uiContext
			.getPropertyAsBoolean(
				RequestParameterConstants.ENABLE_VARIANT_SEARCH)) {
			MfaTab tab =
				new MfaTab(
					InternalRequestParameterConstants.PRODUCT_VARIANTS,
					"ipc.product_variants",
					"/ipc/gotoProductVariants.do");
			tabmap.put(tab.getName(), tab);
			tablist.add(tab);
		}

		if (uiContext
			.getPropertyAsBoolean(
				RequestParameterConstants.SHOW_CUSTOMER_TAB)) {
			MfaTab tab =
				new MfaTab(
					InternalRequestParameterConstants.CUSTOMER_TAB,
					"ipc.customer_tab",
					"/ipc/gotoCustomerTab.do");
			tabmap.put(tab.getName(), tab);
			tablist.add(tab);
		}

		//find the correct order for the tabs
		String order =
			uiContext.getPropertyAsString(
				RequestParameterConstants.MFA_TAB_ORDER);
		if (order != null && order.length() != 0) {
			tablist.clear();
			StringTokenizer st = new StringTokenizer(order, ",");
			while (st.hasMoreElements()) {
				MfaTab tab = (MfaTab) tabmap.get(st.nextToken());
				if (tab != null)
					tablist.add(tab);
			}
		}

		//there is one xcm setting which could lead to an empty tablist:
		//mfaarea.display = T and customizationlist, customertab, productvariants, dynamic product picture) = F
		if (tablist.size() > 0) {

			//tabs were contained in the uiContext
			if (isSet(selectedTabName)) {
				selectedTab = (MfaTab) tabmap.get(selectedTabName);
			} else {
				//set the selected tab to the currently displayed component in the controlled area mfaarea
				UILayoutManager layoutManager =
					UILayoutManager.getManagerFromRequest(request);
				UILayout uiLayout = layoutManager.getCurrentUILayout();
				UIArea uiArea = uiLayout.getUiArea("multifunctionalarea");
				String componentName = uiArea.getComponent().getName();
				selectedTab = (MfaTab) tabmap.get(componentName);
				if (selectedTab == null) {
					// the component could not be found so try to set the first available tab			
					selectedTab = (MfaTab) tablist.get(0);
				}
			}

			customerExit(
				mapping,
				form,
				request,
				response,
				userSessionData,
				requestParser,
				mbom,
				multipleInvocation,
				browserBack);

			request.setAttribute(
				MFATabRequestParameterConstants.SELECTED_MFA_TAB_NAME,
				selectedTab.getName());
			request.setAttribute(
				MFATabRequestParameterConstants.MFA_TAB_LIST,
				tablist);

			return mapping.findForward(ActionForwardConstants.SUCCESS);
		} else {
			return mapping.findForward(ActionForwardConstants.EMPTY);
		}
	}
}
