/*
 * Created on 23.11.2004
 *
 * To change the template for this generated file go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
package com.sap.isa.ipc.ui.jsp.action;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.sap.isa.core.Constants;
import com.sap.isa.core.UserSessionData;
import com.sap.isa.core.businessobject.management.MetaBusinessObjectManager;
import com.sap.isa.core.util.RequestParser;
import com.sap.isa.ipc.ui.jsp.uiclass.UIContext;

/**
 * @author 
 * Is executed when the user clicks on the "Details" link of a characteristic.
 * Forwards to the current layout with the component characteristicDetails in the
 * configworkarea.
 * To change the template for this generated type comment go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
public class CompareProductVariantsAction extends IPCBaseAction {

	/* (non-Javadoc)
	 * @see com.sap.isa.core.BaseAction#doPerform(org.apache.struts.action.ActionMapping, org.apache.struts.action.ActionForm, javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse)
	 */
	public ActionForward ecomPerform(ActionMapping mapping,
		ActionForm form,
		HttpServletRequest request,
		HttpServletResponse response,
		UserSessionData userSessionData,
		RequestParser requestParser,
		MetaBusinessObjectManager mbom,
		boolean multipleInvocation,
		boolean browserBack)
			throws IOException, ServletException {
		UIContext uiContext = getUIContext(request);
		String curInstId = request.getParameter(Constants.CURRENT_INSTANCE_ID);
		if (isSet(curInstId)) {
			changeContextValue(request, Constants.CURRENT_INSTANCE_ID, curInstId);
		}
		
		// Buffer is now handled by dynamicUI Framework, so no need to buffer here anymore.
		/**
		if (!uiContext.getOnlineEvaluate() && !browserBack) {
			ActionForward forward = bufferCharacteristicValues(mapping, request, curInstId);
			if (!forward.getName().equals(ActionForwardConstants.SUCCESS)) {
				return forward;
			}
		}**/
		String curCsticName =
			request.getParameter(
				Constants.CURRENT_CHARACTERISTIC_NAME);
		if (isSet(curCsticName)) {
			changeContextValue(request, Constants.CURRENT_CHARACTERISTIC_NAME, curCsticName);
		}

		String curCharGroupName =
			(String) request.getParameter(
				Constants
					.CURRENT_CHARACTERISTIC_GROUP_NAME);
		if (isSet(curCharGroupName)) {
			changeContextValue(request, Constants.CURRENT_CHARACTERISTIC_GROUP_NAME, curCharGroupName);
		}
			
		String currentScrollGroupName =
			(String) request.getParameter(
				Constants
					.CURRENT_SCROLL_CHARACTERISTIC_GROUP_NAME);
		if (isSet(currentScrollGroupName)) {
			changeContextValue(request, Constants.CURRENT_SCROLL_CHARACTERISTIC_GROUP_NAME, currentScrollGroupName);
		}
			
		String groupStatusChangeName =
			(String) request.getParameter(
				Constants.GROUP_STATUS_CHANGE);
		if (isSet(groupStatusChangeName)) {
			changeContextValue(request, Constants.GROUP_STATUS_CHANGE, groupStatusChangeName);
		}


		String[] productVariantIds = request.getParameterValues(InternalRequestParameterConstants.PRODUCT_VARIANTS_TO_COMPARE);
		if (productVariantIds == null) {
            // the user did not select at least one checkbox
            productVariantIds = new String[0];
		}

		customerExit(mapping, form, request, response, userSessionData, requestParser, mbom, multipleInvocation, browserBack);

		setExtendedRequestAttribute(request, InternalRequestParameterConstants.PRODUCT_VARIANTS_TO_COMPARE, productVariantIds);

		return mapping.findForward(ActionForwardConstants.SUCCESS);
	}

}
