package com.sap.isa.ipc.ui.jsp.action;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;


import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.sap.isa.businessobject.CommunicationException;
import com.sap.isa.core.UserSessionData;
import com.sap.isa.core.businessobject.management.MetaBusinessObjectManager;
import com.sap.isa.core.util.RequestParser;
import com.sap.isa.ipc.ui.jsp.beans.MessageUIBean;
import com.sap.isa.ipc.ui.jsp.beans.UIBeanFactory;
import com.sap.isa.ipc.ui.jsp.uiclass.UIContext;
import com.sap.spc.remote.client.object.Configuration;
import com.sap.spc.remote.client.object.ConfigurationSnapshot;
import com.sap.spc.remote.client.object.IPCItem;
import com.sap.spc.remote.client.object.Instance;
import com.sap.spc.remote.client.object.LoadingStatus;
import com.sap.spc.remote.client.object.imp.DefaultConfigurationSnapshot;

public class CompareToStoredConfigurationAction 
    extends CompareConfigurationsAction
    implements RequestParameterConstants, ActionForwardConstants { 

    /* (non-Javadoc)
     * @see com.sap.isa.isacore.action.EComBaseAction#ecomPerform(org.apache.struts.action.ActionMapping, org.apache.struts.action.ActionForm, javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse, com.sap.isa.core.UserSessionData, com.sap.isa.core.util.RequestParser, com.sap.isa.core.businessobject.management.MetaBusinessObjectManager, boolean, boolean)
     */
    public ActionForward ecomPerform(
        ActionMapping mapping,
        ActionForm form,
        HttpServletRequest request,
        HttpServletResponse response,
        UserSessionData userSessionData,
        RequestParser requestParser,
        MetaBusinessObjectManager mbom,
        boolean multipleInvocation,
        boolean browserBack)
        throws IOException, ServletException, CommunicationException {

        UIContext uiContext = getUIContext(request);
        // save the state of the ConfigUI
        changeContextValues(request);
        // store the showAll-flag in the UIContext
        storeShowAll(request, uiContext);
        // store the showCritical-flag in the UIContext
        storeFilterFlag(request, uiContext);

        // get the list of messages from the uiContext
        List messages = (List) uiContext.getProperty(MESSAGES);
        if (messages == null){
            messages = new ArrayList();    
        }

        Configuration currentConfig = uiContext.getCurrentConfiguration();
        LoadingStatus status = currentConfig.getLoadingStatus();

        // check for loading messages
        if(status.hasLoadingMessages()){
            ArrayList loadingMessages = (ArrayList) status.getLoadingMessages();
            uiContext.setProperty(LOADING_MESSAGES, loadingMessages);
        }

        // check whether feature is disabled
        // Exception if there are loading messages we nevertheless use the comparison screen to display the messages
        if (!uiContext.getPropertyAsBoolean(ENABLE_COMPARISON, false) && !status.hasLoadingMessages()) {
            // create an error message
            MessageUIBean messageBean = UIBeanFactory.getUIBeanFactory().newMessageUIBean();
            messageBean.setMessageStatus(MessageUIBean.ERROR);
            messageBean.setMessageTranslateKey("ipc.message.compare.disabled");
            messages.add(messageBean);
            uiContext.setProperty(MESSAGES, messages);            
            return (mapping.findForward(COMPARISON_ERROR));
        }

        // check whether there is an initial configuration
        IPCItem item = currentConfig.getRootInstance().getIpcItem();
        ConfigurationSnapshot initialConfig = null;
        if (item != null){
            initialConfig = item.getInitialConfiguration();
        }
        if (initialConfig == null){
            // no initial config: create message and return to ConfigUI
            MessageUIBean errorMsg = UIBeanFactory.getUIBeanFactory().newMessageUIBean();
            errorMsg.setMessageStatus(MessageUIBean.ERROR);
            errorMsg.setMessageTranslateKey("ipc.message.compare.no.initial");
            messages.add(errorMsg);
            uiContext.setProperty(MESSAGES, messages);
            uiContext.removeProperty(LOADING_MESSAGES);
            return mapping.findForward(ActionForwardConstants.NO_STORED);
        }
        
        // retrieve the snapshot of the current configuration
        Instance rootInstance = currentConfig.getRootInstance();
        String rootInstancePrice = rootInstance.getPrice();
        DefaultConfigurationSnapshot currentSnap = factory.newConfigurationSnapshot(currentConfig, SNAPSHOT, rootInstancePrice, false);
        
        // compare the snapshots
        messages = compareConfigurations(initialConfig, currentSnap, request);
        if (messages.size()>0){
            // errors occured during comparison -> add messages to uiContext and return to ConfigUI
            uiContext.setProperty(MESSAGES, messages);
            return mapping.findForward(ActionForwardConstants.COMPARISON_ERROR);
        }
        
        customerExit(mapping, form, request, response, userSessionData, requestParser, mbom, multipleInvocation, browserBack);
        // save the action that has been called for comparison
        uiContext.setProperty(COMPARISON_ACTION, COMPARISON_ACTION_STORED);
        return mapping.findForward(ActionForwardConstants.COMPARISON_SCREEN);
    }
}
