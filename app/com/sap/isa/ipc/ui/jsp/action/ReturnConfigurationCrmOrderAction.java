/* Generated by Together */

package com.sap.isa.ipc.ui.jsp.action;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.sap.isa.core.UserSessionData;
import com.sap.isa.core.businessobject.management.MetaBusinessObjectManager;
import com.sap.isa.core.util.RequestParser;
import com.sap.isa.ipc.ui.jsp.uiclass.UIContext;
import com.sap.spc.remote.client.object.Configuration;
import com.sap.spc.remote.client.object.IPCClient;
import com.sap.spc.remote.client.object.IPCSession;

/**
 * Returns the ExternalConfiguration to the calling application. Does not end the current configuration.
 */
public class ReturnConfigurationCrmOrderAction extends IPCBaseAction implements RequestParameterConstants,
                                                                    ActionForwardConstants {
		
		private final String CONFIG_TYPE = "configType";
		private final String GRID = "G";
	
	public ActionForward ecomPerform(ActionMapping mapping,
		ActionForm form,
		HttpServletRequest request,
		HttpServletResponse response,
		UserSessionData userSessionData,
		RequestParser requestParser,
		MetaBusinessObjectManager mbom,
		boolean multipleInvocation,
		boolean browserBack)
			throws IOException, ServletException {

		UIContext uiContext = getUIContext(request);

        if(uiContext == null)
            return (mapping.findForward(INTERNAL_ERROR));

        Configuration currentConfiguration = uiContext.getCurrentConfiguration();
        if(currentConfiguration == null)
          return (mapping.findForward(INTERNAL_ERROR));
          
        
        IPCClient client = (IPCClient)uiContext.getProperty(InternalRequestParameterConstants.IPC_CLIENT);
        IPCSession session = client.getIPCSession();
        session.publish();
        if (session.isLocked()) {
	        client.getIPCSession().unlock();
		}
        
        client.getIPCSession().close();

		if(request.getParameter(CONFIG_TYPE) != null && request.getParameter(CONFIG_TYPE).equals(GRID)){
			return mapping.findForward(ActionForwardConstants.GRID);
		}

		customerExit(mapping, form, request, response, userSessionData, requestParser, mbom, multipleInvocation, browserBack);

	    return mapping.findForward(ActionForwardConstants.SUCCESS);
    }
}
