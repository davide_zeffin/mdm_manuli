/*
 * Created on 07.04.2008
 *
 * To change the template for this generated file go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
package com.sap.isa.ipc.ui.jsp.action;

import java.io.IOException;
import java.util.Iterator;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.sap.isa.businessobject.CommunicationException;
import com.sap.isa.core.SessionConst;
import com.sap.isa.core.UserSessionData;
import com.sap.isa.core.businessobject.management.MetaBusinessObjectManager;
import com.sap.isa.core.util.RequestParser;
import com.sap.isa.core.util.StartupParameter;
import com.sap.isa.core.util.StartupParameter.Parameter;
import com.sap.spc.remote.client.object.IPCException;

/**
 * @author D048393
 *
 * To change the template for this generated type comment go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
public class PostLoginAction extends IPCBaseAction {

	/* (non-Javadoc)
	 * @see com.sap.isa.isacore.action.EComBaseAction#ecomPerform(org.apache.struts.action.ActionMapping, org.apache.struts.action.ActionForm, javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse, com.sap.isa.core.UserSessionData, com.sap.isa.core.util.RequestParser, com.sap.isa.core.businessobject.management.MetaBusinessObjectManager, boolean, boolean)
	 */
	public ActionForward ecomPerform(
		ActionMapping mapping,
		ActionForm form,
		HttpServletRequest request,
		HttpServletResponse response,
		UserSessionData userSessionData,
		RequestParser requestParser,
		MetaBusinessObjectManager mbom,
		boolean multipleInvocation,
		boolean browserBack)
		throws IOException, ServletException, CommunicationException {

		// obtain StartUpParamList
		StartupParameter startupParameter = (StartupParameter) userSessionData.getAttribute(SessionConst.STARTUP_PARAMETER);
		Iterator itr = startupParameter.iterator();
		// set stored request paramter attribute
		while (itr.hasNext()) {
			Parameter param = (Parameter) itr.next();
			request.setAttribute(param.getName(), param.getValue());
		}
		
		// where to go next
		String caller = startupParameter.getParameter(RequestParameterConstants.CALLER).getValue();
		
		
		ActionForward fwd = DispatchOnCallerAction.findForwardForCaller(mapping, caller);
		if (fwd != null) {
			return fwd;
		}
        
        // we should never get here because this is also checked in DispatchOnCaller
		String errormsg = "invalid value caller: " + caller;
		log.error(errormsg);
		IPCException ipcException = new IPCException(errormsg);
		setExtendedRequestAttribute(
			request,
			InternalRequestParameterConstants.IPC_EXCEPTION,
			ipcException);
		return mapping.findForward(ActionForwardConstants.INTERNAL_ERROR);

	}

}
