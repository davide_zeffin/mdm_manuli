package com.sap.isa.ipc.ui.jsp.action;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.sap.isa.businessobject.CommunicationException;
import com.sap.isa.core.UserSessionData;
import com.sap.isa.core.businessobject.management.MetaBusinessObjectManager;
import com.sap.isa.core.util.RequestParser;
import com.sap.isa.ipc.ui.jsp.uiclass.UIContext;
import com.sap.spc.remote.client.object.Configuration;
import com.sap.spc.remote.client.object.Instance;
import com.sap.spc.remote.client.object.imp.DefaultConfigurationSnapshot;

public class TakeInitialSnapshotAction
    extends IPCBaseAction
    implements RequestParameterConstants, ActionForwardConstants {

    /* (non-Javadoc)
     * @see com.sap.isa.isacore.action.EComBaseAction#ecomPerform(org.apache.struts.action.ActionMapping, org.apache.struts.action.ActionForm, javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse, com.sap.isa.core.UserSessionData, com.sap.isa.core.util.RequestParser, com.sap.isa.core.businessobject.management.MetaBusinessObjectManager, boolean, boolean)
     */
    public ActionForward ecomPerform(
        ActionMapping mapping,
        ActionForm form,
        HttpServletRequest request,
        HttpServletResponse response,
        UserSessionData userSessionData,
        RequestParser requestParser,
        MetaBusinessObjectManager mbom,
        boolean multipleInvocation,
        boolean browserBack)
        throws IOException, ServletException, CommunicationException {

        UIContext uiContext = getUIContext(request);
        
        // remove an old snapshot
        uiContext.removeProperty(SNAPSHOT);

        Configuration currentConfig = uiContext.getCurrentConfiguration();
        Instance rootInstance = currentConfig.getRootInstance();
        String rootInstancePrice = rootInstance.getPrice();
        // take the snapshot
        DefaultConfigurationSnapshot snap = factory.newConfigurationSnapshot(currentConfig, "initialConfiguration", rootInstancePrice, true);
        // add it to the uiContext
        uiContext.setProperty(SNAPSHOT_INITIAL, snap);

        customerExit(mapping, form, request, response, userSessionData, requestParser, mbom, multipleInvocation, browserBack);
        
        return (mapping.findForward(SUCCESS));
    }

}
