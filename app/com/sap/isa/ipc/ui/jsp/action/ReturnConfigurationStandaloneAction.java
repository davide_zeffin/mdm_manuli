package com.sap.isa.ipc.ui.jsp.action;

import java.io.IOException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Vector;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.sap.isa.core.UserSessionData;
import com.sap.isa.core.businessobject.management.MetaBusinessObjectManager;
import com.sap.isa.core.util.RequestParser;
import com.sap.isa.ipc.ui.jsp.uiclass.UIContext;
import com.sap.spc.remote.client.object.Configuration;
import com.sap.spc.remote.client.object.IPCClient;
import com.sap.spc.remote.client.object.IPCException;
import com.sap.spc.remote.client.object.IPCItem;
import com.sap.spc.remote.client.object.IPCItemReference;
import com.sap.spc.remote.client.util.imp.c_ext_cfg_imp;
import com.sap.spc.remote.shared.ParameterSet;

/**
 * Returns the ExternalConfiguration to the calling application. Does not end the current configuration.
 */
public class ReturnConfigurationStandaloneAction
    extends IPCBaseAction
    implements RequestParameterConstants, ActionForwardConstants {
	public ActionForward ecomPerform(ActionMapping mapping,
		ActionForm form,
		HttpServletRequest request,
		HttpServletResponse response,
		UserSessionData userSessionData,
		RequestParser requestParser,
		MetaBusinessObjectManager mbom,
		boolean multipleInvocation,
		boolean browserBack)
			throws IOException, ServletException {

        IPCClient ipcClient =
            this.getIPCClient(request);
        UIContext uiContext =
            getUIContext(request);

        HashMap map = new HashMap();

        try {
            // configuration data
            Configuration currentConfiguration =
                uiContext.getCurrentConfiguration();
            if (currentConfiguration == null)
                return (mapping.findForward(INTERNAL_ERROR));
            c_ext_cfg_imp ext_config = (c_ext_cfg_imp) currentConfiguration.toExternal();
            Vector ext_configs = new Vector(1);
            ext_configs.addElement(ext_config);
            // returns the ext_config with BAPI-naming (config-part only)
            ExternalConfigConverter.getConfigs(ext_configs,null,0,1,new _MapParameterSet(map), true);
            //map = currentConfiguration.getExternalConfigurationStandalone();
//            if (map == null)
//                return (mapping.findForward(INTERNAL_ERROR));
            setRequestAttributes(request, map);

            IPCItemReference itemReference = uiContext.getItemReference();
            IPCItem item = ipcClient.getIPCItem(itemReference);

            // pricing values
            map = item.getExternalPricingInfo();
            if (map != null) {
                setRequestAttributes(request, map);
            }

            // pricing conditions
            map = item.getExternalPricingConditions();
            if (map != null) {
                setRequestAttributes(request, map);
            }
        } catch (IPCException ipcException) {
            log.fatal(this, ipcException);
			setExtendedRequestAttribute(request, IPC_EXCEPTION, ipcException);
            return (mapping.findForward(INTERNAL_ERROR));
        }


		customerExit(mapping, form, request, response, userSessionData, requestParser, mbom, multipleInvocation, browserBack);

        return processForward(mapping, request, ActionForwardConstants.SUCCESS);
    }

    private void setRequestAttributes(
        HttpServletRequest request,
        HashMap map) {
        String key = null;
        String value = null;
        Iterator iter = null;

        iter = map.keySet().iterator();
        while (iter.hasNext()) {
            key = (String) iter.next();
            value = (String) map.get(key);
            request.setAttribute(key, value);
        }
    }
    
    protected static class _MapParameterSet implements ParameterSet {
        private HashMap _data;

        public _MapParameterSet(HashMap data) {             
            _data = data;
        }

        public void setValue(String key, String value) {
            _data.put(key,value);
        }

        public void setValue(String key, int index, String value) {          
            _data.put(key+"["+Integer.toString(index)+"]",value);
        }
    }
    
}
