/**
 * The action GetInstancesAction returns a list of instances
 * If no instances are found you get an empty list of instances
 * The list of instances is stored in the request;
 * In the jsp page you can access the the list of instances
 * using the following method:
 *
 * List instances = (List)request.getAttribute(RequestParameterConstants.INSTANCES);
 *
 * The action also adds the root instance as currentInstance
 * to the UIContext.
 */
package com.sap.isa.ipc.ui.jsp.action;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.sap.isa.core.Constants;
import com.sap.isa.core.UserSessionData;
import com.sap.isa.core.businessobject.management.MetaBusinessObjectManager;
import com.sap.isa.core.util.RequestParser;

/**
 * Currently just forwards to the display of the Layout.
 * Customers might add functionality here.
 */
public class GotoProductVariantsAction extends IPCBaseAction {

	public ActionForward ecomPerform(ActionMapping mapping,
		ActionForm form,
		HttpServletRequest request,
		HttpServletResponse response,
		UserSessionData userSessionData,
		RequestParser requestParser,
		MetaBusinessObjectManager mbom,
		boolean multipleInvocation,
		boolean browserBack)
			throws IOException, ServletException {

        String instanceId = request.getParameter(Constants.CURRENT_INSTANCE_ID);
        if (!isSet(instanceId)) {
            return parameterError(mapping, request, Constants.CURRENT_INSTANCE_ID, getClass().getName());
        }
        changeContextValue(request, Constants.CURRENT_INSTANCE_ID, instanceId);

        String groupName = request.getParameter(Constants.CURRENT_CHARACTERISTIC_GROUP_NAME);
        if (!isSet(groupName)) {
            return parameterError(mapping, request, Constants.CURRENT_CHARACTERISTIC_GROUP_NAME, getClass().getName());
        }
        changeContextValue(request, Constants.CURRENT_CHARACTERISTIC_GROUP_NAME, groupName);

		customerExit(mapping, form, request, response, userSessionData, requestParser, mbom, multipleInvocation, browserBack);

        return mapping.findForward(ActionForwardConstants.SUCCESS);       
    }
    
}

