/************************************************************************

	Copyright (c) 2004 by SAP AG

	All rights to both implementation and design are reserved.

	Use and copying of this software and preparation of derivative works
	based upon this software are not permitted.

	Distribution of this software is restricted. SAP does not make any
	warranty about the software, its performance or its conformity to any
	specification.

**************************************************************************/

package com.sap.isa.ipc.ui.jsp.action;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.sap.isa.businessobject.ipc.IPCBOManager;
import com.sap.isa.core.UserSessionData;
import com.sap.isa.core.businessobject.management.MetaBusinessObjectManager;
import com.sap.isa.core.util.DataValidator;
import com.sap.isa.core.util.RequestParser;
import com.sap.isa.ipc.ui.jsp.container.GridLayout;
import com.sap.isa.ipc.ui.jsp.uiclass.UIContext;
import com.sap.spc.remote.client.object.IPCClient;
import com.sap.spc.remote.client.object.IPCException;
import com.sap.spc.remote.client.object.IPCItem;
import com.sap.spc.remote.client.object.IPCItemProperties;
import com.sap.spc.remote.client.object.imp.DefaultIPCDocument;
import com.sap.spc.remote.client.object.imp.DimensionalValue;
import com.sap.spc.remote.client.object.imp.rfc.RFCIPCItemReference;

/************************************************************************

	Grid Process:
	
	After the user made his entries in the quantity fields for each 
	variant, subitems are updated accordingly.
	3 results are possible for each variant:
		- the quantity was changed to 0 or blank -> subitem has to be 
		  removed
		- quantity was changed to a new values, different from 0 or 
		  blank -> subitem quantity will be adapted
		- a new quantity is entered -> a new subitem will be created
	
	Example:
	- item structure first run:
		<ITEM_1>
	- grid screen
	 _____  ______	______
	/ RED |/ Blue |/ Black|
	==========================================
	|    |     30    |     32    |     34    |
	| 30 | variant_1 |           |           |
	| 32 | variant_2 |           |           |
	| 34 |           |           |           |
	========================================== 
	
	- variant_1 quantity of 10 is entered and variant_2 gets the quantity of 20
	- item structure second run:
		<ITEM_1>
			|-<VARIANT_1> (10)
			|-<VARIANT_2> (20)
	- now variant_1 is changed to 0, variant_2 is changed to 15
	- item structure third run:
		<ITEM_1>
			|-<VARIANT_2> (15)
	
	
	After the adjustment of subitems the Action Flow leads back to 
	ReadGridVariantsAction

**************************************************************************/

public class CreateGridItemsAction extends IPCBaseAction {
	

	private final String DOCUMENT_ID = "documentId";
	private final String ITEM_IDS_FOR_REMOVAL = "itemIdsForRemoval";
	private final String ENTRIES_NOT_VALID = "WRONG INPUT";
	private final String CRM_BACKEND = "CRM";
	
	/*
	* **************************************************************************************
	* Start of Action
	* **************************************************************************************
	*/
	
	public ActionForward ecomPerform(ActionMapping mapping,
		ActionForm form,
		HttpServletRequest request,
		HttpServletResponse response,
		UserSessionData userSessionData,
		RequestParser requestParser,
		MetaBusinessObjectManager mbom,
		boolean multipleInvocation,
		boolean browserBack)
			throws IOException, ServletException {
			
			UIContext uiContext = getUIContext(request);
			IPCItem ipcItem = uiContext.getCurrentConfiguration().getRootInstance().getIpcItem();
			String caller = uiContext.getPropertyAsString(RequestParameterConstants.CALLER);
			boolean isValid = true;
			
			
			//IPCClient ipcClient = getIPCClient(request); 
			IPCClient ipcClient = getIPCClient(ipcItem, userSessionData);
			
			//get information that is necessary for the items creation call. highLevelItemId is needed
			//to tell the backend, that it should create subitems instead of main items. In fact the
			//highLevelItemId is nothing but the itemId of the main item
			DefaultIPCDocument ipcDoc = (DefaultIPCDocument)uiContext.getCurrentConfiguration().getRootInstance().getIpcDocument();
			String documentId = uiContext.getCurrentConfiguration().getRootInstance().getIpcDocument().getId();
			String highLevelItemId = uiContext.getCurrentConfiguration().getRootInstance().getIpcItem().getItemId();
			
			String callingAppln = ipcDoc.getApplication();
			//Lock the session for CRM backend only to have consistent data.
			if (callingAppln.equals(CRM_BACKEND)){
				ipcClient.getIPCSession().lock();
			}
			//Get the Jsp data (qty and Variant Ids) and validate entries
			//each field on the Jsp defines a certain variant and its quantity
			String[] qty = getParameters(request, "qty");
			for(int i=0; i<qty.length; i++){
				if(qty[i].length() > 0 && !qty[i].equals("")){
					isValid = DataValidator.isInt(qty[i]);
				}
				
			}
			String[] prodVar = getParameters(request, "varIds");
			String unit = ipcItem.getBaseQuantity().getUnit();
			
			//Get the gridLayout created
			GridLayout gridLayout = (GridLayout) (request.getSession()).getAttribute("gridLayout");
			
			//set error message if entries were not validated successfully
			if(!isValid){
				gridLayout.error.put("3", "X");
				(request.getSession()).removeAttribute("gridLayout");
				(request.getSession()).setAttribute("gridLayout", gridLayout); 
			}
			else{
				
				int counter = 0;
				//Required to create the productVariants array for actual quantity entered.
				//in that step the Jsp input is analyzed, for each filled qty field we need to
				//identify the correct variant object. therfore we first retrieve the amount of variants
				//and then select the correct ids from the request
				//the productVariants[] will contain all thos variantIds for which a quantity different to
				//0 or blank exists
				for (int z=0;z<qty.length;z++){
					if (qty[z] != null && !qty[z].equals("") && !qty[z].equals("0") && !qty[z].equals(" ")) 
						counter++;
				}
				
				String [] varIds = gridLayout.getVariantIds();
				String productVariants[] = new String[counter];
				String variantQty[] = new String[counter];
				
				counter = 0;
				for (int i=0;i<qty.length;i++){
					if (qty[i] != null && !qty[i].equals("") && !qty[i].equals("0") && !qty[i].equals(" ")) {
						variantQty[counter]=qty[i];
						for(int j=0;j<varIds.length;j++){
							if (prodVar[i].equals(varIds[j])){
								productVariants[counter]=varIds[j];
								counter++;
							}	
						}
					}	
				}
							
				
				//since we can have 3 possible results for each variant (see description above) we collect
				//equal results in one List
				ArrayList itemsForRemoval = new ArrayList();
				ArrayList itemsForChange = new ArrayList();
				ArrayList itemsForAdding = new ArrayList();
				ArrayList changeQtys = new ArrayList(1);
				ArrayList addQtys = new ArrayList(1);
				
				
				// if no subitem exists, none can be deleted or changed, so all will be added
				// if already subitems exists we have to identify those who should be deleted or
				// changed and those who should be added
				// Example:
				// ==========================================================
				// |            | productVariant[i] exists | subitem exists |
				// | addItem    |             X            |                |
				// | removeItem |                          |        X       |
				// | changeItem |             X            |        X       |
				// ==========================================================
				if(ipcItem.getChildren()!= null && ipcItem.getChildren().length > 0){
					
					// check for deletion and changing
					for(int i=0; i<ipcItem.getChildren().length;i++){
						boolean delItem = true; // assume all items have to be deleted
						boolean changeItem = false; // --> if all items are deleted, none has to be changed
						
						for(int j=0; j<productVariants.length; j++){
							if(ipcItem.getChildren()[i].getProductId().equals(productVariants[j])){
								delItem = false; // item was found so it should not be deleted 
								// but maybe changed
								if(!ipcItem.getChildren()[i].getSalesQuantity().getValueAsString().equals(variantQty[j])){
									changeItem = true;
									changeQtys.add(variantQty[j]); 
								}
							}
						}
						if(delItem){
							itemsForRemoval.add(ipcItem.getChildren()[i]);
						}
						if(changeItem){
							itemsForChange.add(ipcItem.getChildren()[i]);
						}
					}
					
					//check for those that need to be added
					for(int i=0; i<productVariants.length; i++){
						boolean addItem = true; // assume all items have to be added
						
						for(int j=0; j<ipcItem.getChildren().length; j++){
							if(productVariants[i].equals(ipcItem.getChildren()[j].getProductId())){
								addItem = false; // item already exists
							}
						}
						if(addItem){
							itemsForAdding.add(productVariants[i]);
							addQtys.add(variantQty[i]);
						}
					}
				}
				else{
					for(int i=0; i<productVariants.length; i++){
						itemsForAdding.add(productVariants[i]);
						addQtys.add(variantQty[i]);
					}
				}
				
				// create parameter list for deleting subitems
				if(itemsForRemoval != null & itemsForRemoval.size() > 0){
					
					IPCItem[] removeItems = new IPCItem[itemsForRemoval.size()];
					for(int i=0; i<itemsForRemoval.size(); i++){
						removeItems[i] = (IPCItem)itemsForRemoval.get(i);
					}
					ipcDoc.removeItems(removeItems);
				}
				
				// create parameter list for updating subitems
				if(itemsForChange != null && itemsForChange.size() > 0){
					for(int i=0; i<itemsForChange.size(); i++){
						((IPCItem)itemsForChange.get(i)).setBaseQuantity(new DimensionalValue((String)changeQtys.get(i), unit));
					}
				}
				
				// create item properties for the new or existing items
				if(itemsForAdding != null && itemsForAdding.size() > 0){
					
					//1.Read main IPC Item from server to get IPCItemProperties with all the pricing related information along with Taxation details  
					//2.Create the sub-items using the properties obtained from server to pass all the pricing related information
					//Note 1103590 - Grid qty not getting tranferred to order from Grid screen
 					//Note 1103590 is a correction note for Note 1069550
				    //Note 1108797 - is correction note for Note 1103590
				    
					IPCItemProperties mainItemProp = null;
					IPCItemProperties[] itemProps = new IPCItemProperties[itemsForAdding.size()];
					if (callingAppln.equals(CRM_BACKEND)){
						String[] ipcItemIds = new String[1];
						ipcItemIds[0] = ipcItem.getItemId();
						IPCItemProperties[] mainItemPropArr = (IPCItemProperties[])ipcItem.getPropertiesFromServer(ipcItemIds);
						mainItemProp = mainItemPropArr[0];
					}
					for(int i=0; i<itemsForAdding.size(); i++){					
						itemProps[i] = factory.newIPCItemProperties();
						if (callingAppln.equals(CRM_BACKEND)){	
							itemProps[i].setItemAttributes(mainItemProp.getItemAttributes());
						}					
						itemProps[i].setProductId((String)itemsForAdding.get(i));
						itemProps[i].setHighLevelItemId(highLevelItemId);
						itemProps[i].setBaseQuantity(new DimensionalValue((String)addQtys.get(i), unit));
						//itemProps[i].setSalesQuantity(new DimensionalValue((String)addQtys.get(i), unit));
					}			
					factory.newIPCItems(ipcDoc, itemProps);
				}
			}
			
			return mapping.findForward(ActionForwardConstants.SUCCESS);

	}
	
	/**
	 * Method to get the ipcClient from ipcBoManager
	 * 
	 * @param ipcItem
	 * @param userSessionData
	 * @return
	 */
	private IPCClient getIPCClient(IPCItem ipcItem, UserSessionData userSessionData) {
		
		IPCClient ipcClient = null;
		
		//Get the ipcBoManager
		IPCBOManager ipcBoManager =
			  (IPCBOManager) userSessionData.getBOM(IPCBOManager.NAME);
		if (ipcBoManager == null) {
		   log.error("No IPC BOM in session data! - CreateGridItemsAction");
		   return null;
		}
		//Get the ipcClient
		if (ipcBoManager.getIPCClient() == null){ //might be in ISA case
		
			RFCIPCItemReference itemReference = (RFCIPCItemReference)ipcItem.getItemReference();
			if (itemReference == null) {
				  IPCException ipcException =
					  new IPCException("IPCItemReference == null in CreateGridItemsAction!");
				  log.error(this, ipcException);
				  return null;
			}
			
			String connectionKey = itemReference.getConnectionKey();
			//the caller of the configuration requests the client to use the same connection
			ipcClient = ipcBoManager.createIPCClientUsingConnection(connectionKey);
			
		} else{ 
			ipcClient = ipcBoManager.getIPCClient();
		}
		
		return ipcClient;
	}	
	
	/*
	* **************************************************************************************
	* Service methods
	* **************************************************************************************
	*/
	
	private String[] getParameters(HttpServletRequest request, String key) {
	  SimpleRequestWrapper simpleRequestWrapper = new SimpleRequestWrapper(request);
	  String[] values = simpleRequestWrapper.getParameterValues(key);
	  return values;
	}
	
	private String getParameter(HttpServletRequest request, String key) {
		  SimpleRequestWrapper simpleRequestWrapper = new SimpleRequestWrapper(request);
		  String value = simpleRequestWrapper.getParameterValue(key);
		  return value;
		}
	
	
}