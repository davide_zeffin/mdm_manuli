package com.sap.isa.ipc.ui.jsp.action;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.sap.isa.businessobject.CommunicationException;
import com.sap.isa.core.Constants;
import com.sap.isa.core.UserSessionData;
import com.sap.isa.core.businessobject.management.MetaBusinessObjectManager;
import com.sap.isa.core.util.RequestParser;
import com.sap.isa.ipc.ui.jsp.beans.InstanceUIBean;
import com.sap.isa.ipc.ui.jsp.beans.MessageUIBean;
import com.sap.isa.ipc.ui.jsp.beans.UIBeanFactory;
import com.sap.isa.ipc.ui.jsp.constants.MessagesRequestParameterConstants;
import com.sap.isa.ipc.ui.jsp.uiclass.UIContext;
import com.sap.spc.remote.client.object.Configuration;
import com.sap.spc.remote.client.object.IPCException;
import com.sap.spc.remote.client.object.IPCItem;
import com.sap.spc.remote.client.object.IPCItemProperties;
import com.sap.spc.remote.client.object.Instance;
import com.sap.spc.remote.client.object.ProductKey;


public class ChangeToConfigProductAction
    extends IPCBaseAction
    implements RequestParameterConstants, ActionForwardConstants {

    /* (non-Javadoc)
     * @see com.sap.isa.isacore.action.EComBaseAction#ecomPerform(org.apache.struts.action.ActionMapping, org.apache.struts.action.ActionForm, javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse, com.sap.isa.core.UserSessionData, com.sap.isa.core.util.RequestParser, com.sap.isa.core.businessobject.management.MetaBusinessObjectManager, boolean, boolean)
     */
    public ActionForward ecomPerform(
        ActionMapping mapping,
        ActionForm form,
        HttpServletRequest request,
        HttpServletResponse response,
        UserSessionData userSessionData,
        RequestParser requestParser,
        MetaBusinessObjectManager mbom,
        boolean multipleInvocation,
        boolean browserBack)
        throws IOException, ServletException, CommunicationException {

        UIContext uiContext = getUIContext(request);

        String currentGroupName = request.getParameter(Constants.CURRENT_CHARACTERISTIC_GROUP_NAME);
        if (isSet(currentGroupName)) {
            changeContextValue(request, Constants.CURRENT_CHARACTERISTIC_GROUP_NAME, currentGroupName);
        }

        Configuration currentConfiguration = uiContext.getCurrentConfiguration();
        if (currentConfiguration == null)
            return mapping.findForward(INTERNAL_ERROR);
        Instance rootInstance = currentConfiguration.getRootInstance();
        if (rootInstance == null)
            return mapping.findForward(INTERNAL_ERROR);

        // Try to get the item.
        IPCItem currentItem = rootInstance.getIpcItem();        
        if (currentItem != null){
            try {
                // Check whether it is a product variant
                if (!currentItem.isProductVariant()){
                    log.error("Item " + currentItem.getItemId() + " is not a product variant!");
                    setExtendedRequestAttribute(request,
                        IPC_EXCEPTION,
                        new IPCException(
                            "Item " + currentItem.getItemId() + " is not a product variant!"));
                    return mapping.findForward(INTERNAL_ERROR);
                }
                // We need the guid of the KMAT so we call GetInstances to get the id.
                // TODO: Should be changed as soon as AP provides a function module for this (giving the KMAT for a variant)
                String[] instNames = currentConfiguration.getInstanceNames(); 
                if(instNames.length>0){
                    // the first instance name is the root instance
                    String kmatId = instNames[0]; 
                    ProductKey pk = currentItem.getProductKey();
                    // convert the productId of the KMAT to the productGUID
                    String kmatGuid = currentItem.getProductGuid(kmatId, pk.getProductType(), pk.getProductLogSys());
                    // get the original item attributes; they will be replaced by the attributes from KMAT's product master
                    IPCItemProperties[] itemProperties = currentItem.getPropertiesFromServer(new String[] {currentItem.getItemId()});
                    Map itemAttributes = null;
                    if (itemProperties.length>0){
                    	if (itemProperties[0] != null){
							itemAttributes = itemProperties[0].getItemAttributes();   
                    	}
                    }
                    // get the item attributes for the KMAT from the product master
                    Map itemAttributesFromProductMaster = currentItem.getAttributesFromProductMaster(kmatGuid);
                    // replace the original item attributes with the attributes that were returned from the KMAT's product master
                    if (itemAttributes != null){
                        Iterator iter = itemAttributesFromProductMaster.keySet().iterator();
                        while (iter.hasNext()) {
                            String key = (String)iter.next();
                            String value = (String)itemAttributesFromProductMaster.get(key);
                            itemAttributes.put(key, value);
                        }
                        // set the attributes at the item (triggers ChangeItem command)
                        currentItem.setItemAttributes(itemAttributes);
                    }
                    // change item product (triggers ChangeItemProduct)
                    currentItem.changeProduct(kmatId); // TODO: At the moment the fm only accepts the productId not the productGUID. Change this later.
                    // get new config and instance
                    Configuration newConfig = currentItem.getConfiguration();
                    uiContext.setCurrentConfiguration(newConfig);
                    Instance newRootInstance = newConfig.getRootInstance();
                    changeContextValue(request, Constants.CURRENT_INSTANCE_ID, newRootInstance.getId());
                    InstanceUIBean newRootInstanceBean =
                        UIBeanFactory.getUIBeanFactory().newInstanceUIBean(newRootInstance, uiContext);
                    setExtendedRequestAttribute(request, CURRENT_INSTANCE, newRootInstanceBean);
                    // switch from product variant mode to default-mode
                    uiContext.setProperty(PRODUCT_VARIANT_MODE, false);
                    // set the display mode to the original display mode (i.e. if the original display-mode was read-only they UI will remain read-only)
                    uiContext.setDisplayMode(uiContext.getPropertyAsBoolean(ORIGINAL_DISPLAY_MODE));
                    // add message
                    List  messages = new ArrayList();
                    String[] messageArguments = {pk.getProductId(), kmatId};
                    MessageUIBean productVariantMessage = UIBeanFactory.getUIBeanFactory().newMessageUIBean(MessageUIBean.SUCCESS, "", "ipc.message.kmat.apply", messageArguments);
                    messages.add(productVariantMessage);
                    setExtendedRequestAttribute(request, MessagesRequestParameterConstants.MESSAGES, messages);                    
                }
            } catch (IPCException e) {
                log.fatal(this, e);
                setExtendedRequestAttribute(request, IPC_EXCEPTION, e);
                return mapping.findForward(INTERNAL_ERROR);
            }                
        }
        else {
            setExtendedRequestAttribute(request,
                IPC_EXCEPTION,
                new IPCException(
                    "Item is null!"));
            return mapping.findForward(INTERNAL_ERROR);
        }
        return processForward(mapping, request, ActionForwardConstants.SUCCESS);
    }
}
