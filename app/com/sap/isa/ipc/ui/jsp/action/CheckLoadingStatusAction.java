package com.sap.isa.ipc.ui.jsp.action;

import java.io.IOException;
import java.util.List;
import java.util.Vector;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.sap.isa.businessobject.CommunicationException;
import com.sap.isa.core.UserSessionData;
import com.sap.isa.core.businessobject.management.MetaBusinessObjectManager;
import com.sap.isa.core.util.RequestParser;
import com.sap.isa.ipc.ui.jsp.beans.MessageUIBean;
import com.sap.isa.ipc.ui.jsp.beans.UIBeanFactory;
import com.sap.isa.ipc.ui.jsp.uiclass.UIContext;
import com.sap.spc.remote.client.object.Configuration;
import com.sap.spc.remote.client.object.LoadingStatus;

public class CheckLoadingStatusAction
    extends IPCBaseAction
    implements RequestParameterConstants, ActionForwardConstants {

    private static final String MSG_NEW_KB = "ipc.message.load.newKB";
    private static final String MSG_DETAILS = "ipc.message.details";
    private static final String MSG_LOADING_EXISTS = "ipc.message.load.exist";
    
    /* (non-Javadoc)
     * @see com.sap.isa.isacore.action.EComBaseAction#ecomPerform(org.apache.struts.action.ActionMapping, org.apache.struts.action.ActionForm, javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse, com.sap.isa.core.UserSessionData, com.sap.isa.core.util.RequestParser, com.sap.isa.core.businessobject.management.MetaBusinessObjectManager, boolean, boolean)
     */
    public ActionForward ecomPerform(
        ActionMapping mapping,
        ActionForm form,
        HttpServletRequest request,
        HttpServletResponse response,
        UserSessionData userSessionData,
        RequestParser requestParser,
        MetaBusinessObjectManager mbom,
        boolean multipleInvocation,
        boolean browserBack)
        throws IOException, ServletException, CommunicationException {
     
        UIContext uiContext = getUIContext(request);
        Configuration config = uiContext.getCurrentConfiguration();
        boolean showLoadingMessages = uiContext.getPropertyAsBoolean(RequestParameterConstants.SHOW_LOADING_MESSAGES, true);
        String automaticForward = uiContext.getPropertyAsString(RequestParameterConstants.LOADING_MESSAGES_AUTOMATIC_FORWARD, RequestParameterConstants.ON_MESSAGES);

        String comparisonScreenActionForward = ActionForwardConstants.COMPARISON_SCREEN_DYNAMICUI;
        
        // We have to differantiate the following cases of parameter automaticForward 
        // 1. "always": it always forwards to the comparison screen even if no messages exist.
        // 2. "onmessages": it forwards automatically to the comparison screen if loading messages exist.
        // 3. "F": no forward, it only shows static messages in the messagearea with links 
        //    to the comaprison screen.
        if (automaticForward.equals(RequestParameterConstants.ALWAYS)){
            log.debug("forward: " +  automaticForward);
            // 1. always forward (don't check for loading messages (this can be done later on request)
            return mapping.findForward(comparisonScreenActionForward);
        }
        else if (automaticForward.equals(RequestParameterConstants.ON_MESSAGES)){
            log.debug("forward: " +  automaticForward);
            // 2. forward on messages
            if (showLoadingMessages){
                log.debug("showLoadingMessages: " +  showLoadingMessages);
                // retrieve the loading status
                LoadingStatus status = config.getLoadingStatus();

                if (status.hasLoadingMessages()){
                    log.debug("hasNewKB: " +  status.hasNewKB());
                    log.debug("hasLoadingMessages: " +  status.hasLoadingMessages());
                    if (status.hasNewKB()) {
                        // get the list of messages from the uicontext
                        List messages = (List) uiContext.getProperty(MESSAGES);
                        if (messages == null) {
                            messages = new Vector();
                            uiContext.setProperty(MESSAGES, messages);
                        }                
                        // create message that new KB has been applied
                        MessageUIBean newKBMessage = UIBeanFactory.getUIBeanFactory().newMessageUIBean(MSG_NEW_KB);
                        messages.add(newKBMessage);
                    }                   
                    // loading messages occurred during loading -> forward to comparison scrren
                    return mapping.findForward(comparisonScreenActionForward);
                }
                else {
                    log.debug("hasNewKB: " +  status.hasNewKB());
                    log.debug("hasLoadingMessages: " +  status.hasLoadingMessages());
                    // new KB has been applied -> we don't forward but create a message
                    if (status.hasNewKB()) {
                        // get the list of messages from the uicontext
                        List messages = (List) uiContext.getProperty(MESSAGES);
                        if (messages == null) {
                            messages = new Vector();
                            uiContext.setProperty(MESSAGES, messages);
                        }                
                        MessageUIBean newKBMessage = UIBeanFactory.getUIBeanFactory().newMessageUIBean(MSG_NEW_KB);
                        messages.add(newKBMessage);                        
                    }
                    // no loading messages -> don't forward
                }
            }
            else {
                log.debug("showLoadingMessages: " +  showLoadingMessages);
                // no loading messages should be displayed -> don't forward
            }
        }
        else {
            // 3. no foward
            log.debug("forward: " +  automaticForward);
            if (showLoadingMessages){
                log.debug("showLoadingMessages: " +  showLoadingMessages);
                // retrieve the loading status
                LoadingStatus status = config.getLoadingStatus();                
                
                // get the list of messages from the uicontext
                List messages = (List) uiContext.getProperty(MESSAGES);
                if (messages == null) {
                    messages = new Vector();
                    uiContext.setProperty(MESSAGES, messages);
                }                
                if (status.hasNewKB()){
                    log.debug("hasNewKB: " +  status.hasNewKB());                                       
                    // create message that new KB has been applied
                    MessageUIBean newKBMessage = UIBeanFactory.getUIBeanFactory().newMessageUIBean(MSG_NEW_KB);
                    newKBMessage.addAction(MSG_DETAILS, "javascript:submitForm", "/ipc/compareToStored.do");
                    messages.add(newKBMessage);
                }
                if (status.hasLoadingMessages()){
                    log.debug("hasLoadingMessages: " +  status.hasLoadingMessages());                                        
                    // create message that loading messages occurred
                    MessageUIBean hasLoadingMessagesMessage = UIBeanFactory.getUIBeanFactory().newMessageUIBean(MSG_LOADING_EXISTS);
                    hasLoadingMessagesMessage.addAction(MSG_DETAILS, "javascript:submitForm", "/ipc/compareToStored.do");
                    messages.add(hasLoadingMessagesMessage);
                }
            }
            else {
                log.debug("showLoadingMessages: " +  showLoadingMessages);                
                // do nothing 
            }
        }
        
        customerExit(mapping, form, request, response, userSessionData, requestParser, mbom, multipleInvocation, browserBack);
        
        return mapping.findForward(ActionForwardConstants.SUCCESS);
    }

}
