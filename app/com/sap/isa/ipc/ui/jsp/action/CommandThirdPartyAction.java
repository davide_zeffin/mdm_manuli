package com.sap.isa.ipc.ui.jsp.action;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.sap.isa.core.UserSessionData;
import com.sap.isa.core.businessobject.management.MetaBusinessObjectManager;
import com.sap.isa.core.util.RequestParser;

/**
 * Title:
 * Description:  Initializes the specific parameter for ThirdParty Applications
 * calling the UI.
 * Copyright:    Copyright (c) 2001
 * Company:
 * @author
 * @version 1.0
 */

public class CommandThirdPartyAction extends IPCBaseAction implements RequestParameterConstants {
	public ActionForward ecomPerform(ActionMapping mapping,
		ActionForm form,
		HttpServletRequest request,
		HttpServletResponse response,
		UserSessionData userSessionData,
		RequestParser requestParser,
		MetaBusinessObjectManager mbom,
		boolean multipleInvocation,
		boolean browserBack)
			throws IOException, ServletException {


		if (!requestParser.getParameter(CALLER).isSet()) {
		    setExtendedRequestAttribute(request, IPC_SCENARIO, THIRD_PARTY);
			setExtendedRequestAttribute(request, CALLER, THIRD_PARTY);
		}

		//Customers may add processing of specific parameter here.
		//e.g. transformation of external configuration format,
		//mapping of parameter names, etc.


		RequestParser.Parameter commandParameter = requestParser.getParameter(COMMAND_EXECUTE);
		if (commandParameter.isSet()) {
			String commandExecute = commandParameter.getValue().getString();

			if (commandExecute.equalsIgnoreCase(CRM_START_CONFIGURATION)) {
				return (mapping.findForward(ActionForwardConstants.START_CONFIGURATION));
			}
			else if (commandExecute.equalsIgnoreCase(CRM_SHOW_CONFIGURATION)) {
				return (mapping.findForward(ActionForwardConstants.SHOW_CONFIGURATION));
			}
			else
			{
				return (mapping.findForward(ActionForwardConstants.ILLEGAL_COMMAND_ERROR));
			}
		}

		//return findForward.....
		return (mapping.findForward(ActionForwardConstants.ILLEGAL_COMMAND_ERROR));

	}
}