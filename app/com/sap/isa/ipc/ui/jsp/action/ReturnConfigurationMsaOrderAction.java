/*
 * Created on 27.07.2005
 *
 * To change the template for this generated file go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
package com.sap.isa.ipc.ui.jsp.action;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.sap.isa.core.UserSessionData;
import com.sap.isa.core.businessobject.management.MetaBusinessObjectManager;
import com.sap.isa.core.util.RequestParser;
import com.sap.isa.ipc.ui.jsp.uiclass.UIContext;
import com.sap.spc.remote.client.object.Configuration;

/**
 * @author d031177
 *
 * To change the template for this generated type comment go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
public class ReturnConfigurationMsaOrderAction extends IPCBaseAction implements RequestParameterConstants,
																	ActionForwardConstants {
		
		private final String CONFIG_TYPE = "configType";
		private final String GRID = "G";
	
	public ActionForward ecomPerform(ActionMapping mapping,
		ActionForm form,
		HttpServletRequest request,
		HttpServletResponse response,
		UserSessionData userSessionData,
		RequestParser requestParser,
		MetaBusinessObjectManager mbom,
		boolean multipleInvocation,
		boolean browserBack)
			throws IOException, ServletException {

		UIContext uiContext = getUIContext(request);

		if(uiContext == null)
			return (mapping.findForward(INTERNAL_ERROR));

		Configuration currentConfiguration = uiContext.getCurrentConfiguration();
		if(currentConfiguration == null)
		  return (mapping.findForward(INTERNAL_ERROR));
        

		if(request.getParameter(CONFIG_TYPE) != null && request.getParameter(CONFIG_TYPE).equals(GRID)){
			return mapping.findForward(ActionForwardConstants.GRID);
		}

		customerExit(mapping, form, request, response, userSessionData, requestParser, mbom, multipleInvocation, browserBack);


		return mapping.findForward(ActionForwardConstants.SUCCESS);
	}
}
