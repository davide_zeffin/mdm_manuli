package com.sap.isa.ipc.ui.jsp.action;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Vector;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.digester.Digester;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.sap.isa.businessobject.ipc.IPCBOManager;
import com.sap.isa.core.Constants;
import com.sap.isa.core.ContextConst;
import com.sap.isa.core.FrameworkConfigManager;
import com.sap.isa.core.RequestProcessor;
import com.sap.isa.core.UserSessionData;
import com.sap.isa.core.businessobject.management.MetaBusinessObjectManager;
import com.sap.isa.core.interaction.InteractionConfig;
import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.core.ui.context.GlobalContextManager;
import com.sap.isa.core.util.RequestParser;
import com.sap.isa.core.xcm.ConfigContainer;
import com.sap.isa.core.xcm.ExtendedConfigManager;
import com.sap.isa.core.xcm.init.ExtendedConfigInitHandler;
import com.sap.isa.core.xcm.scenario.ScenarioConfigException;
import com.sap.isa.ipc.ui.jsp.IPCDebugMessages;
import com.sap.isa.ipc.ui.jsp.IPCExceptionConstants;
import com.sap.isa.ipc.ui.jsp.beans.CharacteristicUIBean;
import com.sap.isa.ipc.ui.jsp.beans.ConfigUIBean;
import com.sap.isa.ipc.ui.jsp.beans.GroupUIBean;
import com.sap.isa.ipc.ui.jsp.beans.InstanceUIBean;
import com.sap.isa.ipc.ui.jsp.beans.UIBeanFactory;
import com.sap.isa.ipc.ui.jsp.beans.ValueUIBean;
import com.sap.isa.ipc.ui.jsp.dynamicui.IPCDynamicUIConstant;
import com.sap.isa.ipc.ui.jsp.dynamicui.IPCUIModel;
import com.sap.isa.ipc.ui.jsp.uiclass.UIContext;
import com.sap.isa.ipc.ui.jsp.util.HtmlTraceGenerator;
import com.sap.isa.isacore.action.EComBaseAction;
import com.sap.spc.remote.client.object.Characteristic;
import com.sap.spc.remote.client.object.CharacteristicGroup;
import com.sap.spc.remote.client.object.CharacteristicValue;
import com.sap.spc.remote.client.object.Configuration;
import com.sap.spc.remote.client.object.IPCClient;
import com.sap.spc.remote.client.object.IPCException;
import com.sap.spc.remote.client.object.IPCItem;
import com.sap.spc.remote.client.object.Instance;
import com.sap.spc.remote.client.object.imp.IPCClientObjectFactory;
import com.sap.spc.remote.client.object.imp.BaseGroup;
/**
 * @author 
 *
 * To change the template for this generated type comment go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
//<--

/**
 * Title:
 * Description:  JSP UI for the IPC.
 * Copyright:    Copyright (c) 2001
 * Company:
 * @author
 * @version 1.0
 */

public abstract class IPCBaseAction extends EComBaseAction {
		
	static {
		GlobalContextManager.registerValue(Constants.CURRENT_CHARACTERISTIC_GROUP_NAME, true, false);
		GlobalContextManager.registerValue(Constants.CURRENT_CHARACTERISTIC_NAME, true, false);
		GlobalContextManager.registerValue(Constants.CHARACTERISTIC_STATUS_CHANGE, true, false);
		GlobalContextManager.registerValue(Constants.CURRENT_CONFLICT_ID, true, false);
		GlobalContextManager.registerValue(Constants.CURRENT_CONFLICT_PARTICIPANT_ID, true, false);
		GlobalContextManager.registerValue(Constants.CURRENT_INSTANCE_ID, true, false);
		GlobalContextManager.registerValue(Constants.INSTANCE_TREE_STATUS_CHANGE, true, false);
		GlobalContextManager.registerValue(Constants.CURRENT_PRODUCT_VARIANT_ID, true, false);
		GlobalContextManager.registerValue(Constants.CURRENT_SCROLL_CHARACTERISTIC_GROUP_NAME, true, false);
        GlobalContextManager.registerValue(Constants.SELECTED_MFA_TAB_NAME, true, false);
        GlobalContextManager.registerValue(Constants.CURRENT_VALUE_NAME, true, false);
	}
	private static final String mFileName = 
		ExtendedConfigInitHandler.getFilesConfig().getPath(InternalRequestParameterConstants.XCM_IPC_CONFIGURATION_FILE);
		
	protected static IsaLocation log = IsaLocation.getInstance(IPCBaseAction.class.getName());
    
	protected IPCClientObjectFactory factory =
		IPCClientObjectFactory.getInstance();
		
	protected IPCClient getIPCClient(HttpServletRequest request) {

		// read businessobjectmanager from Session
		UserSessionData userData = getUserSessionData(request);

		IPCBOManager ipcBoManager = (IPCBOManager)userData.getBOM(IPCBOManager.NAME);
		if (ipcBoManager == null) {
			setExtendedRequestAttribute(request, InternalRequestParameterConstants.IPC_EXCEPTION, new IPCException("Couldn't get the Businessobject manager!\n"));
			log.fatal(userData, new IPCException("Couldn't get the Businessobject manager!\n"));
			return null;
		}
		IPCClient ipcClient = ipcBoManager.getIPCClient();

		if (ipcClient == null) {
			setExtendedRequestAttribute(request, InternalRequestParameterConstants.IPC_EXCEPTION, new IPCException("Couldn't connect to IPC-Server!\n" +
																 "Probably the server is down or not started\n" +
																 "or the settings in \n" +
																 "\"<web-app>\\WEB-INF\\classes\\properties\\client.properties\" " +
																 "are wrong"));
			log.fatal(ipcBoManager, new IPCException("Could not determine the IPCClient from the IPCBOManager!"));
			return null;
		}
		return ipcClient;
	}

	private static HttpSession getHttpSession(HttpServletRequest request) {
		HttpSession session = request.getSession(false);
		if (session == null) {
			//TODO use msgkey
			log.fatal("Could not get HttpSession in IPCBaseAction!");
		}
		return session;        
	}
	private static UserSessionData getUserSessionData(HttpServletRequest request) {
		HttpSession session = getHttpSession(request);
		UserSessionData userData = UserSessionData.getUserSessionData(session);
		if (userData == null) {
//			TODO use msgkey
			RequestProcessor.setExtendedRequestAttribute(request,
			InternalRequestParameterConstants.IPC_EXCEPTION,
				new IPCException(IPCExceptionConstants.IPCEX19));
			log.fatal(
				session,
				new IPCException(IPCExceptionConstants.IPCEX19));
			return null;
		}
		return userData;
	}
    
	public static UIContext createUIContext(HttpServletRequest request) {
		UserSessionData userSessionData = getUserSessionData(request);
		Object uiContextObject = userSessionData.getAttribute(SessionAttributeConstants.UICONTEXT);
		if (uiContextObject != null) {
			log.error(IPCExceptionConstants.IPCEX20);
		}
		UIContext uiContext = new UIContext();
		userSessionData.setAttribute(SessionAttributeConstants.UICONTEXT, uiContext);
		if (log.isDebugEnabled()) {
			log.debug(IPCDebugMessages.MSG2);
		}
		return uiContext;
	}
    
	protected void removeUIContext(HttpServletRequest request) {
		UserSessionData userSessionData = getUserSessionData(request);
		Object uiContextObject = userSessionData.getAttribute(SessionAttributeConstants.UICONTEXT);
		if (uiContextObject == null) {
			log.error(IPCExceptionConstants.IPCEX18);
		}else {
			userSessionData.removeAttribute(SessionAttributeConstants.UICONTEXT);
			if (log.isDebugEnabled()) {
				log.debug(IPCDebugMessages.MSG1);
			}
		}
	}
    
	public static UIContext getUIContext(
		HttpServletRequest request) {
            
		UserSessionData userData = getUserSessionData(request);
        
        
		UIContext uiContext = (UIContext)userData.getAttribute(SessionAttributeConstants.UICONTEXT);
		return uiContext;
	}

	protected ActionForward parameterError(ActionMapping mapping, HttpServletRequest request, String parameterName, String actionName) {
		IPCException ipcException = new IPCException("Required requestParameter " + parameterName +
		 " not found in " + actionName + "!");
		setExtendedRequestAttribute(request, InternalRequestParameterConstants.IPC_EXCEPTION, ipcException);
		 return mapping.findForward(ActionForwardConstants.INTERNAL_ERROR);
	}
	
	/**
	 * Forwards to the destination given in the request parameter forwardDestination or if no request parameter
	 * forwardDestination exists to the forward given in the parameter forward.
	 * @param mapping
	 * @param request
	 * @param forward
	 * @return
	 */
	protected ActionForward processForward(ActionMapping mapping, HttpServletRequest request, String forward) {
		//search for a forward in the request
		//if nothing found, forward to succes
		if (isSet(request.getParameter(RequestParameterConstants.FORWARD_DESTINATION)))
		  return mapping.findForward(request.getParameter(RequestParameterConstants.FORWARD_DESTINATION));
		if(isSet((String)request.getAttribute(RequestParameterConstants.FORWARD_DESTINATION)))
		return mapping.findForward((String)request.getAttribute(RequestParameterConstants.FORWARD_DESTINATION));
		return (mapping.findForward(forward));
	}

	protected static String getParameterOrScenarioParameter(HttpServletRequest request, String parameterName) {
		UIContext uiContext = IPCBaseAction.getUIContext(request);
		String parameterValue = request.getParameter(parameterName);
		if (parameterValue != null && !parameterValue.equals("") && !parameterValue.equals("null")) {
			return parameterValue;
		} else if ((parameterValue = (String)uiContext.getProperty(parameterName)) != null) {
			return parameterValue;
		}else {
			return null;
		}

	}
    
	protected ActionForward bufferCharacteristicValues(
		ActionMapping mapping,
		HttpServletRequest request,
		String instanceId)
		throws IPCException {
			UIContext uiContext;
		uiContext =
			getUIContext(request);

		Hashtable characteristicBuffer = uiContext.getCharacteristicsBuffer();
		Hashtable valueBuffer; // = new Hashtable(1);

		Configuration currentConfiguration =
			uiContext.getCurrentConfiguration();
		if (currentConfiguration == null)
			return (mapping.findForward(ActionForwardConstants.INTERNAL_ERROR));
            
		Instance instance =
			uiContext.getCurrentConfiguration().getInstance(instanceId);
		if (instance == null) {
			return parameterError(
				mapping,
				request,
				Constants.SELECTED_INSTANCE_ID,
				this.getClass().getName());
		}

		String currentCharacteristicGroupName =
			request.getParameter(Constants.SELECTED_CHARACTERISTIC_GROUP_NAME);
		if (!isSet(currentCharacteristicGroupName)) {
			currentCharacteristicGroupName =
				((CharacteristicGroup) instance
					.getCharacteristicGroups(uiContext.getShowInvisibleCharacteristics(), uiContext.getPropertyAsBoolean(RequestParameterConstants.SHOW_GENERAL_TAB_FIRST))
					.get(0))
					.getName();
		}
		CharacteristicGroup currentCharacteristicGroup =
			instance.getCharacteristicGroup(
				currentCharacteristicGroupName);
		if (currentCharacteristicGroup == null) {
			return parameterError(
				mapping,
				request,
			Constants.CURRENT_CHARACTERISTIC_GROUP_NAME,
				this.getClass().getName());
		}

		boolean showInvisibleCstics =
		uiContext.getShowInvisibleCharacteristics();

		// Now search for input fields (additional values)
		Enumeration e = request.getParameterNames();
		while (e.hasMoreElements()) {
			String parameterName = (String) e.nextElement();
			int index = parameterName.indexOf("input_");
			if (index != -1
				&& parameterName.substring(0, "input_".length()).equals(
					"input_")) {
				String characteristicName =
					parameterName.substring(index + "input_".length());
				String characteristicValue =
					request.getParameter(parameterName);
				Characteristic cstic =
					instance.getCharacteristic(
						characteristicName,
						showInvisibleCstics);
				String characteristicsKey =
					instance.getId() + "." + cstic.getName();
				//buffer the value if the cstic exists and the input field contains smth <> "" and the value is not yet set
				if ((cstic != null && characteristicValue.length() > 0)
				   &&
				   (!uiContext.getShowLanguageDependentNames() && cstic.getValueFromLanguageDependentName(characteristicValue) == null
				   || //currently freetext fields use the language-dependend name always regardless the setting of uiContext
				    uiContext.getShowLanguageDependentNames() && cstic.getValueFromLanguageDependentName(characteristicValue) == null)) {
					valueBuffer =
						(Hashtable) characteristicBuffer.get(
							characteristicsKey);
					if (valueBuffer == null) {
						valueBuffer = new Hashtable();
						characteristicBuffer.put(
							characteristicsKey,
							valueBuffer);
					} else {
						valueBuffer.clear();
					}

					// Copy the content of the original characteristic into this cache!
					List origValues = cstic.getValues();
					for (int i = 0; i < origValues.size(); i++) {
						CharacteristicValue origValue =
							(CharacteristicValue) origValues.get(i);
						valueBuffer.put(origValue.getName(), origValue);
					}
					// Finally add the temporary cstic value
					valueBuffer.put(characteristicValue, characteristicValue);
				}
			} else {
				index = parameterName.indexOf("characteristic_");
				if (index != -1
					&& parameterName.substring(
						0,
						"characteristic_".length()).equals(
						"characteristic_")) {
					String characteristicName =
						parameterName.substring(
							index + "characteristic_".length());
					Characteristic characteristic =
						instance.getCharacteristic(
							characteristicName,
							showInvisibleCstics);
					if (characteristic == null) {
						setExtendedRequestAttribute(request,
							RequestParameterConstants.IPC_EXCEPTION,
							new IPCException(
								"characteristic "
									+ characteristicName
									+ " not found!"));
						return mapping.findForward(ActionForwardConstants.INTERNAL_ERROR);
					}
					String[] charValueNames =
						request.getParameterValues(parameterName);
					if (request
						.getParameter(
						InternalRequestParameterConstants.VALIDATE_CSTIC_VALUES
								+ "_"
								+ characteristic.getName())
						!= null) {
						boolean result =
							characteristic.setValues(charValueNames);
						if (result); //avoid compiler warning about unused variable
                        
					}
				}
			}
		}
		return mapping.findForward(ActionForwardConstants.SUCCESS);
	}
    
    
	protected void initializeInstanceUIBeans(HttpServletRequest request, InstanceUIBean rootInstanceUIBean, List instanceUIBeans, Hashtable instanceUIBeansById, UIContext uiContext) {
		instanceUIBeans.add(rootInstanceUIBean);
		instanceUIBeansById.put(rootInstanceUIBean.getId(), rootInstanceUIBean);
		customerExit(request, rootInstanceUIBean, uiContext);
		List children = rootInstanceUIBean.getBusinessObject().getChildren();
		if (children != null && children.size() > 0) {
			for (Iterator it = children.iterator(); it.hasNext(); ) {
				Instance instance = (Instance)it.next();
				InstanceUIBean instanceUIBean = UIBeanFactory.getUIBeanFactory().newInstanceUIBean(instance, uiContext);
				instanceUIBean.setParent(rootInstanceUIBean);
				rootInstanceUIBean.addChild(instanceUIBean);
				initializeInstanceUIBeans(request, instanceUIBean, instanceUIBeans, instanceUIBeansById, uiContext);
			}
		}else {
			return;
		}
	}
	
	protected ConfigUIBean getCurrentConfiguration(
		UIContext uiContext)
	{
			Configuration currentConfiguration = uiContext.getCurrentConfiguration();
            
			ConfigUIBean configUIBean = UIBeanFactory.getUIBeanFactory().newConfigUIBean(currentConfiguration, uiContext);
			return configUIBean;
	}
	
	/**
	 * Initializes the InstanceUIBean objects and associates them to the currentConfiguration.
	 * @param uiContext
	 * @param currentConfiguration
	 * @param currentInstanceId
	 * @param request
	 * @return Reference to the InstanceUIBean identified by currentInstanceId.
	 * Reference to the root instance of the configuration, if the currentInstanceId is null.
	 */
	protected InstanceUIBean getCurrentInstance(
				 UIContext uiContext,
				 ConfigUIBean currentConfiguration,
				 String currentInstanceId,
				 HttpServletRequest request)
		{

        
		Instance currentInstance;
		if (isSet(currentInstanceId)) {
			currentInstance = uiContext.getCurrentConfiguration().getInstance(currentInstanceId);
		}else {
			currentInstance = uiContext.getCurrentConfiguration().getRootInstance();
			currentInstanceId = currentInstance.getId();
		}
		if (currentInstance == null) {
			return null;
		}

		List instances = currentConfiguration.getBusinessObject().getInstances();
		Instance rootInstance = currentConfiguration.getBusinessObject().getRootInstance();
		InstanceUIBean rootInstanceUIBean = UIBeanFactory.getUIBeanFactory().newInstanceUIBean(rootInstance, uiContext);
		List instanceUIBeans = new ArrayList(instances.size());
		Hashtable instanceUIBeansById = new Hashtable(instances.size());
		initializeInstanceUIBeans(request, rootInstanceUIBean, instanceUIBeans, instanceUIBeansById, uiContext);
		//initialize the instances of the ConfigUIBean
		currentConfiguration.setInstances(instanceUIBeans);
		currentConfiguration.setRootInstance(rootInstanceUIBean);


		Hashtable instanceTreeStatus = (Hashtable) uiContext.getProperty(Constants.INSTANCE_TREE_STATUS_CHANGE);
		if (instanceTreeStatus == null)
		{
		  instanceTreeStatus = new Hashtable();
		  uiContext.setProperty(Constants.INSTANCE_TREE_STATUS_CHANGE,instanceTreeStatus);
		}

		InstanceUIBean currentInstanceUIBean = (InstanceUIBean)instanceUIBeansById.get(currentInstanceId);
        
		return currentInstanceUIBean;

	}
	
	/**
	 * Iterates through the groups of the business object of currentInstanceUIBean.
	 * Creates UIBean objects for the groups to display and associates them to currentInstanceUIBean.
	 * Returns a reference to the UIBean of the currently active characteristicGroup.
	 * @param uiContext
	 * @param currentInstanceUIBean
	 * @param request
	 * @return
	 * @throws IOException
	 * @throws ServletException
	 */
	public GroupUIBean getCharacteristicGroups(
				 UIContext uiContext,
				 InstanceUIBean currentInstanceUIBean,
				 String curCharGroupName,
				 String currentScrollGroupName,
				 String groupStatusChangeName,
				 Hashtable groupStatus,
				 HttpServletRequest request
				 ) {
	    
//		List characteristicGroups = null;
		GroupUIBean currentCharacteristicGroup = null;

		if (groupStatus == null)
		{
		  groupStatus = new Hashtable();
		  uiContext.setProperty(Constants.GROUP_STATUS_CHANGE,groupStatus);;
		}

		// This piece of code will fill the hashtable with the instance names of
		// collapsed nodes
		if (groupStatusChangeName != null)
		{
		  if (groupStatus.get(groupStatusChangeName) != null)
		  {
			groupStatus.remove(groupStatusChangeName);
		  }
		  else
		  {
			groupStatus.put(groupStatusChangeName,groupStatusChangeName);
		  }
		}

		// first get a list of completely all available characteristicGroups
		List groupBusinessObjects = currentInstanceUIBean.getBusinessObject().getCharacteristicGroups(uiContext.getShowInvisibleCharacteristics(), uiContext.getPropertyAsBoolean(RequestParameterConstants.SHOW_GENERAL_TAB_FIRST));
		List groups = new Vector();
		for (int i = 0; i < groupBusinessObjects.size(); i++) {
			CharacteristicGroup group =
				(CharacteristicGroup) groupBusinessObjects.get(i);
			// if the parameter showEmptyGroups = false (default), then show only
			//the groups which have visible characteristicValue
			if (!uiContext.showEmptyGroups() && !hasVisibleCharacteristics(uiContext, group)) {
				//do not add the group to the groups to be displayed
			}else {
				GroupUIBean groupUIBean =
				UIBeanFactory.getUIBeanFactory().newGroupUIBean(group, uiContext);
				groups.add(groupUIBean);
				//associate with instanceUIBean
				currentInstanceUIBean.addGroup(groupUIBean);
			}
		}

		// modification for search/set
		GroupUIBean searchSetGroup = (GroupUIBean) request.getAttribute(InternalRequestParameterConstants.SEARCH_SET_FOUND_GROUP);
		if (isSet(curCharGroupName) && searchSetGroup == null) {
            // get the current group from the list of generated groupUIBeans
            currentCharacteristicGroup  = currentInstanceUIBean.getGroup(curCharGroupName);
            // If this was not successful display the first available group.
            // (This could happen if curCharGroupName points to a group that is empty and empty groups should not be displayed).
            if (currentCharacteristicGroup == null && !groups.isEmpty()){
                currentCharacteristicGroup = (GroupUIBean)groups.get(0);
				curCharGroupName = currentCharacteristicGroup.getName();
				currentScrollGroupName = curCharGroupName;
            }
            // if this still fails we try to get the group from the list of group business objects
            if (currentCharacteristicGroup == null){
			currentCharacteristicGroup = UIBeanFactory.getUIBeanFactory().newGroupUIBean(currentInstanceUIBean.getBusinessObject().getCharacteristicGroup(curCharGroupName), uiContext);
            }            
		} else if (searchSetGroup != null) {
			currentCharacteristicGroup = searchSetGroup;            
		}else {
			// Select the first group. Note: There has to be at least one group!
			if (!groups.isEmpty()) {
				//these are the groups wich contain at least one characteristic
				currentCharacteristicGroup = (GroupUIBean)groups.get(0);
			}
			else {
				//TODO: What should happen if no group with visible cstics exists, but
				//the user decided not to see empty groups
				//these are all groups including the empty ones
				currentCharacteristicGroup = UIBeanFactory.getUIBeanFactory().newGroupUIBean((CharacteristicGroup)currentInstanceUIBean.getBusinessObject().getCharacteristicGroups(uiContext.getShowInvisibleCharacteristics(), uiContext.getPropertyAsBoolean(RequestParameterConstants.SHOW_GENERAL_TAB_FIRST)).get(0), uiContext);
			}
		}

		// fill the empty List characteristicGroupsChoose with the number of groups defined
		// by the parameter GROUP_QUANTITY
		List characteristicGroupsChoose = new Vector();
		// int groupQuantity = uiContext.getPropertyAsInteger(RequestParameterConstants.GROUP_QUANTITY);
		int groupQuantity = 3;
		int groupPosition = 0;
		//the number of groups in the configuration is higher than the number of groups
		//that customizing defines to display 
		if (groupQuantity > 0 && groups.size() > groupQuantity) {
			// if groupQuantity > 0, then show only as many groups at once as defined by the parameter
			if (IPCBaseAction.isSet(currentScrollGroupName)) {
				//CharacteristicGroup currentScroll = (CharacteristicGroup) uiContext.getProperty(Constants.CURRENT_SCROLL_CHARACTERISTIC_GROUP_NAME);
				GroupUIBean currentScroll = currentInstanceUIBean.getGroup(currentScrollGroupName);
				groupPosition = groups.indexOf(currentScroll);
			} else {
				if (uiContext.getSelectedCharacteristicGroup() != null) {
					// set the groupPosition not only on the position of the selectedCharacteristicGroup,
					// but on the value of int i. This is to avoid that in case SHOW_EMPTY_GROUPS = F the
					// first group isn't shown.
					for (int i=0; i<groups.size(); i++) {
						if (uiContext.getSelectedCharacteristicGroup().equals(groups.get(i))) {
							groupPosition = i;
						}
					}
				}
			}
			for (int i = groupPosition; i < groups.size(); i++) {
				if ((i-groupPosition) < groupQuantity) {
					Object group = groups.get(i);
					characteristicGroupsChoose.add(group);
				}
			}
			//TODO performance: is it really necessary to iterate through all groups again?
			// if the List of the chosen groups is empty or smaller than the number of groups
			// defined by GROUP_QUANTITY, then fill the list again.
			if (characteristicGroupsChoose.size() < groupQuantity && groupPosition > (groups.size() - groupQuantity)) {
				characteristicGroupsChoose.clear();
				for (int j = (groups.size()-groupQuantity); j < groups.size(); j++) {
					Object group = groups.get(j);
					characteristicGroupsChoose.add(group);
				}
			}
			
			//TODO optimize for performance
			//set the scroll flag for all groups that are visible in the scroll area
			for (Iterator i = characteristicGroupsChoose.iterator(); i.hasNext(); ) {
				GroupUIBean curScrollGroup = (GroupUIBean)i.next();
				curScrollGroup.setInScrollArea(true);
			}
            
			// set the flag for the first group in the scroll-area
			GroupUIBean firstScrollGroup = (GroupUIBean)characteristicGroupsChoose.get(0);
			firstScrollGroup.setFirstInScrollArea(true);
            
		} else {
			//TODO optimize for performance
			// no groups or all groups visible at the same time, no scrolling
			for (Iterator i = groups.iterator(); i.hasNext(); ) {
				GroupUIBean curScrollGroup = (GroupUIBean)i.next();
				curScrollGroup.setInScrollArea(true);
			}
            
			// set the flag for the first group in the scroll-area
			GroupUIBean firstScrollGroup;
			if (groups.size() > 0) {
				firstScrollGroup = (GroupUIBean)groups.get(0);
				firstScrollGroup.setFirstInScrollArea(true);            
			}else {
				log.debug("No characteristic groups found in IPCBaseAction.getCharacteristicGroups");
			}
		}
		uiContext.setSelectedCharacteristicGroup(currentCharacteristicGroup);

		return currentCharacteristicGroup;
	}
	
	protected boolean hasVisibleCharacteristics(
		UIContext uiContext,
		CharacteristicGroup group) {
		List characteristics =
			group.getCharacteristics(
				uiContext.getShowInvisibleCharacteristics(),
				uiContext.getPropertyAsBoolean(
					RequestParameterConstants.ASSIGNABLE_VALUES_ONLY,
					false));
		if (characteristics.isEmpty()) {
			return false;
		} else if (uiContext.isCharacteristicsVisibleViewSet() && !uiContext.getShowInvisibleCharacteristics()) {
			//restrictions for visibility passed via http request?
			for (int j = 0; j < characteristics.size(); j++) {
				Characteristic charac = (Characteristic) characteristics.get(j);
				// check whether the characteristic view is restricted
				// manually (VehicleManager)
				if (!uiContext.isCharacteristicVisibleRestricted(charac.getName()) && charac.isVisible()) {
					return true;
				}					
			}
			return false;
		}
		return true;
	}

	/**
	 * Iterates through the characteristics associated with the business object of currentInstanceUIBean
	 * or currentGroupUIBean.
	 * Creates ui bean objects for characteristics to display and associates them to the currentInstanceUIBean
	 * and to the currentGroupUIBean.
	 * Returns the reference to the currently active characteristic, that is the characteristic the page
	 * should scroll to.
	 * @param uiContext
	 * @param currentInstanceUIBean
	 * @param instanceChangedParameter
	 * @param currentGroupUIBean
	 * @param groupChangedParameter
	 * @param curCsticName
	 * @param csticStatusChange
	 * @param searchSetCstic
	 * @return
	 */
	protected CharacteristicUIBean getCharacteristics(UIContext uiContext,
									  InstanceUIBean currentInstanceUIBean,
									  String instanceChangedParameter,
									  GroupUIBean currentGroupUIBean,
									  String groupChangedParameter,
									  String curCsticName,
									  String csticStatusChange,
									  CharacteristicUIBean searchSetCstic,
									  HttpServletRequest request) {
		List characteristics;
		if(!uiContext.getPropertyAsBoolean(RequestParameterConstants.USE_GROUP_INFORMATION)){
			//get all characteristics independent of their groups
			characteristics = currentInstanceUIBean.getBusinessObject().getCharacteristics(uiContext.getShowInvisibleCharacteristics());
		}else{
			//get all characteristics of one special group
			characteristics = currentGroupUIBean.getBusinessObject().getCharacteristics(uiContext.getShowInvisibleCharacteristics());
		}
        
		// restrict the attributes 'readonly' and 'visible' dependent of the
		// business scenario (f.e. VehicleManager)
		if (uiContext.isCharacteristicsReadonlyViewSet() ||
			uiContext.isCharacteristicsVisibleViewSet() ) {

			for (int i=0; i < characteristics.size(); i++) {
				Characteristic cstic = (Characteristic) characteristics.get(i);
				
				//possibility to influence the properties of a cstic independent from the engine (VMS)
				//@TODO move into a seperate action
				if (uiContext.isCharacteristicVisibleRestricted( cstic.getName() ) ) {
					if ( cstic.isVisible() ) cstic.setVisible(false);
				}
				//possibility to influence the properties of a cstic independent from the engine (VMS)
				//@TODO move into a seperate action
				if (uiContext.isCharacteristicReadonlyRestricted( cstic.getName() ) ) {
					if ( ! cstic.isReadOnly() ) { 
						cstic.setReadOnly(true);
					}
					
				}
				// remove cstic if not required to display
				if (!cstic.isVisible() && !uiContext.getShowInvisibleCharacteristics()){
					characteristics.remove(i);
				}
			}
		}
        
        boolean showMessageCstics = uiContext.getPropertyAsBoolean(ProcessRequestParameterAction.SHOW_MESSAGE_CSTICS);
        String prefix = uiContext.getPropertyAsString(ProcessRequestParameterAction.MESSAGE_CSTICS_PREFIX);
        
		ArrayList characteristicUIBeans = new ArrayList();
		for (Iterator it = characteristics.iterator(); it.hasNext(); ) {
			Characteristic characteristic = (Characteristic)it.next();
            CharacteristicUIBean characteristicUIBean;
            if (showMessageCstics && characteristic.getName().startsWith(prefix)){
                characteristicUIBean = CharacteristicUIBean.getInstance(request, characteristic, uiContext, true);
            } 
            else {
            	characteristicUIBean = CharacteristicUIBean.getInstance(request, characteristic, uiContext);
            }

            characteristicUIBeans.add(characteristicUIBean);
			currentGroupUIBean.addCharacteristic(characteristicUIBean);
			currentInstanceUIBean.addCharacteristic(characteristicUIBean);
		}
                        
		// delete former currentCsticName if group or instance has changed
		boolean groupOrInstanceChanged = false;
		if ((groupChangedParameter != null && groupChangedParameter.equals("true")) 
			|| (instanceChangedParameter != null && instanceChangedParameter.equals("true"))) {
			groupOrInstanceChanged = true;
		}
		if (groupOrInstanceChanged) {
			curCsticName = "";
		}
        
		// modification search/set
		CharacteristicUIBean curCstic = searchSetCstic;
		if (isSet(curCsticName) && curCstic == null) {
					curCstic = currentInstanceUIBean.getCharacteristic(curCsticName);
			if (curCstic != null) {
				curCstic.setLastFocused(true);
			}
		} else if (curCstic != null){
			curCstic.setLastFocused(true);
		} else if (!currentGroupUIBean.getCharacteristics().isEmpty()){
			//set the current characteristic to the first characteristic
			//of the current group if the group is not empty
			curCstic = (CharacteristicUIBean)currentGroupUIBean.getCharacteristics().get(0);
			if (curCstic != null) {
				curCstic.setLastFocused(true);
			}
		}
        
		//------------------------- changing expand/collapse of characteristics -----------------------------------//
		//TODO: That would be the task of a separate action
		// Now handle the status of the characteristics - a single klick on the
		// characteristic should collapse them
		Hashtable csticStatus = (Hashtable) uiContext.getProperty(Constants.CHARACTERISTIC_STATUS_CHANGE);
		if (csticStatus == null) {
		  csticStatus = new Hashtable();
		  uiContext.setProperty(Constants.CHARACTERISTIC_STATUS_CHANGE,csticStatus);
		}

		// This piece of code will fill the hashtable with the cstic names of
		// collapsed nodes
		//TODO move this to ExpandCharacteristicAction
		if (isSet(csticStatusChange)) {
			//intended for scrolling to the characteristic that is to be expanded, but does not work in a frameless ui
			//curCstic = currentInstanceUIBean.getCharacteristic(csticStatusChange.substring(csticStatusChange.indexOf('.')+1));
			String csticStatusKey = currentInstanceUIBean.getId() + "." + csticStatusChange;
		  if (csticStatus.get(csticStatusKey) != null) {
			csticStatus.remove(csticStatusKey);
		  }
		  else {
			csticStatus.put(csticStatusKey, csticStatusChange);
		  }
		}
        
		//------------------------ end changing expand/collapse of characteristics ----------------------------------//
		return curCstic;
	}
	
	
	
	
	/**
	 * Creates UIBeans for the characteristic values and associates them to the passed
	 * InstanceUIBean or GroupUIBean.
	 * @param request
	 * @param uiContext
	 * @param currentInstanceUIBean
	 * @param group
	 * @throws IOException
	 * @throws ServletException
	 */
	protected void getCharacteristicValues(
								 HttpServletRequest request,
								 UIContext uiContext,
								 InstanceUIBean currentInstanceUIBean,
								 GroupUIBean group 
								 )
	{


	  Hashtable csticsBuffer = null;
	  if (!uiContext.getPropertyAsBoolean(RequestParameterConstants.ONLINE_EVALUATE)) {
		csticsBuffer = uiContext.getCharacteristicsBuffer();
	  }
	  Hashtable csticValues = new Hashtable();

	  List csticList;
	  if (uiContext.getUseGroupInformation()) {
		  csticList = group.getCharacteristics();
	  } else {
		  csticList = currentInstanceUIBean.getCharacteristics();
	  }
	  Iterator csticListIt = csticList.iterator();
	  while (csticListIt.hasNext()) {
		  Vector returnedValues;
		  CharacteristicUIBean currentCharacteristic = (CharacteristicUIBean) csticListIt.next();
		  List allValues = currentCharacteristic.getBusinessObject().getValues();
		  if (csticsBuffer != null && csticsBuffer.size() > 0) {
			  String csticKey = currentCharacteristic.getBusinessObject().getInstance().getId()+"."+currentCharacteristic.getName();
			  Hashtable valueBuffer = (Hashtable) csticsBuffer.get(csticKey);
			  // check if there are buffered values, which would overwrite the real values!
			  if (valueBuffer != null) {
				  returnedValues = new Vector(valueBuffer.size());
				  // Please do not call Vector(valueBuffer.values()) because the elements will be in the
				  // wrong order!!!
				  for (Iterator valuesIt = allValues.iterator(); valuesIt.hasNext();) {
					  CharacteristicValue iValue = (CharacteristicValue) valuesIt.next();
					  CharacteristicValue bufValue = (CharacteristicValue) valueBuffer.get(iValue.getName());
					  if (bufValue != null) { 
					  	ValueUIBean valueUIBean = UIBeanFactory.getUIBeanFactory().newValueUIBean(bufValue, uiContext);
					  	returnedValues.add(valueUIBean);
					  	currentCharacteristic.addValue(valueUIBean);
					  } else {
					  	log.debug("Unexpected error in " + this.getClass().getName() + ".getCharacteristicValues(). " + iValue.getName() + " not found in buffer!");
				      }
				  }
			  }else {
				  returnedValues = new Vector(allValues.size());
				  for (Iterator valuesIt = allValues.iterator(); valuesIt.hasNext(); ) {
					  CharacteristicValue value = (CharacteristicValue)valuesIt.next();
					  ValueUIBean valueUIBean = UIBeanFactory.getUIBeanFactory().newValueUIBean(value, uiContext);
					  returnedValues.add(valueUIBean);
					  currentCharacteristic.addValue(valueUIBean);
				  }
			  }
			  csticValues.put(currentCharacteristic.getName(), returnedValues);
		  }else {
			  returnedValues = new Vector(allValues.size());
			  for (Iterator valuesIt = allValues.iterator(); valuesIt.hasNext(); ) {
				  CharacteristicValue value = (CharacteristicValue)valuesIt.next();
				  ValueUIBean valueUIBean = UIBeanFactory.getUIBeanFactory().newValueUIBean(value, uiContext);
				  returnedValues.add(valueUIBean);
				  currentCharacteristic.addValue(valueUIBean);
			  }
			  csticValues.put(currentCharacteristic.getName(), returnedValues);
		  }
		  // set flag whether any characteristic has mimes or value mimes for the work area
		  // this is done during instantiation-process because of performance-reasons
		  if (currentCharacteristic.hasMimesForWorkArea() || currentCharacteristic.hasValueMimesForWorkArea()) {
			  currentInstanceUIBean.setAnyMimesForWorkArea(true);
			  group.setAnyMimesForWorkArea(true);
	  }

	  }
	  log.debug(csticValues);
	}
	
	protected ActionForward processScenarioParameter(
		ActionMapping mapping,
		HttpServletRequest request) {
	    	
		String method = "processScenarioParameter";
		log.entering(method);
	    	
		ConfigContainer xcmConfigContainer = null;
		String scenarioName;
		if (request.getParameter(Constants.XCM_SCENARIO_RP) != null) {
			xcmConfigContainer = this.getXCMConfigContainer(request);
		}else if ((scenarioName = (String)request.getAttribute(InternalRequestParameterConstants.IPC_XCM_SCENARIO)) != null) {
            if (FrameworkConfigManager.XCM.isXCMScenario(scenarioName)) {
    			ExtendedConfigManager extendedConfigManager = ExtendedConfigManager.getExtConfigManager();
    	   		xcmConfigContainer = extendedConfigManager.getConfig(scenarioName);
            }
            else {
                String errMsg = "The [XCM application configuration]='" + scenarioName + "' does not exist. Check XCM configuration."; 
                log.error(errMsg);
                IPCException ipcException = new IPCException(errMsg);
                setExtendedRequestAttribute(request, InternalRequestParameterConstants.IPC_EXCEPTION, ipcException);
                return mapping.findForward(ActionForwardConstants.INTERNAL_ERROR);
            }
		}else {
			//use the default scenario
			xcmConfigContainer = getXCMConfigContainer(request);
		}
		if (xcmConfigContainer == null) {
			log.error("XCM configuration container not found, XCM might not be enabled!");
			return mapping.findForward(ActionForwardConstants.INTERNAL_ERROR);
		}else if (mFileName != null) {
			InputStream is =
				xcmConfigContainer.getConfigAsStream(mFileName);
			//Digester is not multithreading capable, so create a new Instance here
			Digester digester = new Digester();
	
			try {
				UIContext uiContext = IPCBaseAction.createUIContext(request);
				Map scenarioParameters = uiContext.getInternalProperties();
	
				if (is != null) {
					digester.push(scenarioParameters);
					digester.addCallMethod("ipc/configs/component/params/param", "put", 2, new Class[] {Object.class, Object.class});
					digester.addCallParam("ipc/configs/component/params/param", 0, "name");
					digester.addCallParam("ipc/configs/component/params/param", 1, "value");
					digester.parse(is);
						
					if (log.isDebugEnabled() && scenarioParameters != null) {
						log.debug("Scenario Configuration:");
						for (Iterator parameterIt = scenarioParameters.keySet().iterator(); parameterIt.hasNext(); ) {
							String parameterName = (String)parameterIt.next();
							String parameterValue = (String)scenarioParameters.get(parameterName);
							log.debug("Parametername: " + parameterName + " + Parametervalue: " + parameterValue);
							if (parameterName.length() > 32) {
								log.debug("Parameter too long: " + parameterName);
							}
						}
					}
					
					if (scenarioParameters == null) {
						IPCException ipce = new IPCException("Error reading the scenario parameter from file " + mFileName);
						setExtendedRequestAttribute(request, InternalRequestParameterConstants.IPC_EXCEPTION, ipce);
						mapping.findForward(ActionForwardConstants.INTERNAL_ERROR);
					}
	
					InteractionConfig f = FrameworkConfigManager.Servlet.getInteractionConfig(request).getConfig("ui");
					if( f != null) {
					  String enablePricingAnalysis = f.getValue(ContextConst.IC_ENABLE_PRICE_ANALYSIS);
					  if (enablePricingAnalysis != null) {
					  	uiContext.setProperty(ContextConst.IC_ENABLE_PRICE_ANALYSIS, enablePricingAnalysis);
					  }
					}
					
	
				} else
					throw new ScenarioConfigException(
						"Error reading configuration information\n"
							+ "No input stream or file name set");
			} catch (Exception ex) {
				log.fatal(
					"Error reading configuration information\n" + ex.toString());
			} finally {
				try {
					if (is != null)
						is.close();
				} catch (IOException ioex) {
					log.error(
						"system.xcm.init.read.smconf",
						new Object[] { mFileName },
						ioex);
				}
			}
			
		}else {
			log.error(
				"system.xcm.init.read.smconf",
				new Object[] { mFileName }, new RuntimeException());
		}
		
		log.exiting();
		return mapping.findForward(ActionForwardConstants.SUCCESS);		
	}
	
	protected static boolean isSet(String parameter) {
		if (parameter != null && !parameter.equals("") && !parameter.equals("null")) {
			return true;
		}else {
			return false;
		}
	}
	
	
	/**
	 * Is called when a html trace has to be written in the trace file
	 * This trace file contains all the parameters passed in the http request
	 * @param request
	 */
	protected void htmlTrace(HttpServletRequest request) {
		HtmlTraceGenerator htmlGen=new HtmlTraceGenerator();
		htmlGen.trace(request);
		
	}
	
	
    
	/**
	 * Is called for each instance of the configuration.
	 * Customer may subclass this action and owerwrite the customerExit method.
	 * @param request
	 * @param instanceUIBean
	 * @param uiContext
	 */
	protected void customerExit(HttpServletRequest request, InstanceUIBean instanceUIBean, UIContext uiContext) {
		//avoid compiler warnings about unused local variables
		if (request == null);
		if (instanceUIBean == null);
		if (uiContext == null);

	}
	
	/**
	 * Enables customer to modify the action behavior.
	 * @param request
	 * @param productId
	 */
	protected void customerExit(
	ActionMapping mapping,
	ActionForm form,
	HttpServletRequest request,
	HttpServletResponse response,
	UserSessionData userSessionData,
	RequestParser requestParser,
	MetaBusinessObjectManager mbom,
	boolean multipleInvocation,
	boolean browserBack) {
		//avoid compiler warnings about unused local variables
		if(mapping == null);
		if(form == null);
		if(request == null);
		if (response == null);
		if (userSessionData == null);
		if (requestParser == null);
		if (mbom == null);
		if (multipleInvocation);
		if (browserBack);
	}

	/**
	 * Reads the relevant context parameter 
	 * Constants.CURRENT_INSTANCE_ID
	 * Constants.SELECTED_INSTANCE_ID
	 * Constants.CURRENT_SCROLL_CHARACTERISTIC_GROUP_NAME
	 * Constants.CURRENT_CHARACTERISTIC_GROUP_NAME
	 * Constants.CURRENT_CHARACTERISTIC_NAME
	 * Constants.CURRENT_CONFLICT_ID
	 * from the request and sets them to the 
	 * context for usage after redirect and browser back.
	 * @param request
	 */
	public void setContextValues(HttpServletRequest request) {
		String instanceId = request.getParameter(Constants.CURRENT_INSTANCE_ID);
		if (isSet(instanceId)) {
			setContextValue(request, Constants.CURRENT_INSTANCE_ID, instanceId);
		}
		
		String sInstanceId = request.getParameter(Constants.SELECTED_INSTANCE_ID);
		if (isSet(sInstanceId)) {
			setContextValue(request, Constants.SELECTED_INSTANCE_ID, sInstanceId);
		}

		String curScrollGroupName = request.getParameter(Constants.CURRENT_SCROLL_CHARACTERISTIC_GROUP_NAME);
		if (isSet(curScrollGroupName)) {
			setContextValue(request, Constants.CURRENT_SCROLL_CHARACTERISTIC_GROUP_NAME, curScrollGroupName);
		}
		String curGroupName = request.getParameter(Constants.CURRENT_CHARACTERISTIC_GROUP_NAME); 
		if (isSet(curGroupName)) {
			setContextValue(request, Constants.CURRENT_CHARACTERISTIC_GROUP_NAME, curGroupName);
		}        
        
		String curCsticName = request.getParameter(Constants.CURRENT_CHARACTERISTIC_NAME);
		if (isSet(curCsticName)) {
			setContextValue(request, Constants.CURRENT_CHARACTERISTIC_NAME, curCsticName);
		}
        
		String curConflictId = request.getParameter(Constants.CURRENT_CONFLICT_ID);
		if (isSet(curCsticName)) {
			setContextValue(request, Constants.CURRENT_CONFLICT_ID, curConflictId);
		}
        
		String curConflictPartId = request.getParameter(Constants.CURRENT_CONFLICT_PARTICIPANT_ID);
		if (isSet(curConflictPartId)) {
			setContextValue(request, Constants.CURRENT_CONFLICT_PARTICIPANT_ID, curConflictPartId);
		}
		
	}

	/**
	 * Reads the relevant context parameter 
	 * Constants.CURRENT_INSTANCE_ID
	 * Constants.SELECTED_INSTANCE_ID
	 * Constants.CURRENT_SCROLL_CHARACTERISTIC_GROUP_NAME
	 * Constants.CURRENT_CHARACTERISTIC_GROUP_NAME
	 * Constants.CURRENT_CHARACTERISTIC_NAME
	 * Constants.CURRENT_CONFLICT_ID
	 * from the request and sets them to the old
	 * context for usage after redirect and browser back.
	 * @param request
	 */
	public void changeContextValues(HttpServletRequest request) {
		String instanceId = request.getParameter(Constants.CURRENT_INSTANCE_ID);
		if (isSet(instanceId)) {
			changeContextValue(request, Constants.CURRENT_INSTANCE_ID, instanceId);
		}
		
		String sInstanceId = request.getParameter(Constants.SELECTED_INSTANCE_ID);
		if (isSet(sInstanceId)) {
			changeContextValue(request, Constants.SELECTED_INSTANCE_ID, sInstanceId);
		}

		String curScrollGroupName = request.getParameter(Constants.CURRENT_SCROLL_CHARACTERISTIC_GROUP_NAME);
		if (isSet(curScrollGroupName)) {
			changeContextValue(request, Constants.CURRENT_SCROLL_CHARACTERISTIC_GROUP_NAME, curScrollGroupName);
		}
		String curGroupName = request.getParameter(Constants.CURRENT_CHARACTERISTIC_GROUP_NAME); 
		if (isSet(curGroupName)) {
			changeContextValue(request, Constants.CURRENT_CHARACTERISTIC_GROUP_NAME, curGroupName);
		}        
        
		String curCsticName = request.getParameter(Constants.CURRENT_CHARACTERISTIC_NAME);
		if (isSet(curCsticName)) {
			changeContextValue(request, Constants.CURRENT_CHARACTERISTIC_NAME, curCsticName);
		}
        
		String curConflictId = request.getParameter(Constants.CURRENT_CONFLICT_ID);
		if (isSet(curCsticName)) {
			changeContextValue(request, Constants.CURRENT_CONFLICT_ID, curConflictId);
		}
        
		String curConflictPartId = request.getParameter(Constants.CURRENT_CONFLICT_PARTICIPANT_ID);
		if (isSet(curConflictPartId)) {
			changeContextValue(request, Constants.CURRENT_CONFLICT_PARTICIPANT_ID, curConflictPartId);
		}
		
	}

	/**
	 * At the beginning of the configuration process the focus should be on the root instance,
	 * the first group of the root instance and the first characteristic of this first group.
	 * The method assumes that there is always at least one group, the BaseGroup.
	 * If not, it will not be able to set initial context values.
	 * @param request
	 * @param configuration
	 */
	public void setInitialContextValues(HttpServletRequest request, Configuration configuration) {
		if (configuration != null) {
			Instance rootInstance = configuration.getRootInstance();
			if (rootInstance != null) {
				UIContext uiContext = getUIContext(request);
				changeContextValue(request, Constants.CURRENT_INSTANCE_ID, rootInstance.getId());
				changeContextValue(request, Constants.SELECTED_INSTANCE_ID, rootInstance.getId());
				setInitialGroupContextValues(request, rootInstance);
			} else {
				log.debug("Cannot set initial context value for rootinstance!");
			}
		}
	}
	
	public void setInitialGroupContextValues(
		HttpServletRequest request,
		Instance instance) {
		UIContext uiContext = getUIContext(request);
		List groups =
			instance.getCharacteristicGroups(
				uiContext.getShowInvisibleCharacteristics(),
				uiContext.getPropertyAsBoolean(
					RequestParameterConstants.SHOW_GENERAL_TAB_FIRST));
		Iterator groupsIt;
		if (groups != null) {
			groupsIt = groups.iterator();
			if (groupsIt.hasNext()) {
				while (groupsIt.hasNext()) {
					CharacteristicGroup firstGroup =
						(CharacteristicGroup) groupsIt.next();
					List characteristics =
						firstGroup.getCharacteristics(
							uiContext.getShowInvisibleCharacteristics(),
							uiContext.getAssignableValuesOnly());
					if (!characteristics.isEmpty()) {
						changeContextValue(
							request,
							Constants.CURRENT_CHARACTERISTIC_GROUP_NAME,
							firstGroup.getName());
						changeContextValue(
							request,
							Constants.SELECTED_CHARACTERISTIC_GROUP_NAME,
							firstGroup.getName());
						changeContextValue(
							request,
							Constants.CURRENT_SCROLL_CHARACTERISTIC_GROUP_NAME,
							firstGroup.getName());
						setInitialCharacteristicContextValues(
							request,
							firstGroup);
						break; //first non-empty group
					}
				}
			}else {
				log.debug(
					"Cannot set initial context value for group! Instance "
						+ instance.getId()
						+ " has empty group list.");
			}
		}else {
			log.debug(
				"Cannot set initial context value for group! Instance "
					+ instance.getId()
					+ " has null groups.");
		}
		// fallback: If the context-value is not set we set it to $BASE_GROUP to avoid exception
		String currentCsticGroupName = getContextValue(request, Constants.CURRENT_CHARACTERISTIC_GROUP_NAME);
		if ((currentCsticGroupName == null) || (currentCsticGroupName.equals(""))){
			changeContextValue(
				request,
				Constants.CURRENT_CHARACTERISTIC_GROUP_NAME,
				BaseGroup.BASE_GROUP_NAME);			
		}
	}
	
	public void setInitialCharacteristicContextValues(HttpServletRequest request, CharacteristicGroup group) {
		UIContext uiContext = getUIContext(request);
		List characteristics = group.getCharacteristics(uiContext.getShowInvisibleCharacteristics(), uiContext.getAssignableValuesOnly());
		Iterator characteristicsIt;
		if (characteristics != null) {
			characteristicsIt = characteristics.iterator();
			if (characteristicsIt.hasNext()) {
				Characteristic firstCharacteristic = (Characteristic)characteristicsIt.next();
				changeContextValue(request, Constants.CURRENT_CHARACTERISTIC_NAME, firstCharacteristic.getName());
			}else {
				log.debug("Cannot set initial context value for characteristic. Group " + group.getName() + " has no visible characteristics in the current uiContext.");
			}
		}else {
			log.debug("Cannot set initial context value for characteristic. Group " + group.getName() + " has null characteristics in the current uiContext.");
		}		
	}
    
    /**
     * Checks whether the UI has to be displayed in product variant mode.
     * If the item is a product variant the UI is set into "product variant mode".
     * 
     * Checks whether the IPC item corresponds to a changeable product variant
     * @param uiContext
     * @param item
     */
    public void setProductVariantMode(UIContext uiContext, IPCItem item){
        if (item != null){
            if (item.isProductVariant()){
                log.debug("setProductVariantMode(): Item is product variant. Switching to PV-mode.");
                uiContext.setProperty(InternalRequestParameterConstants.PRODUCT_VARIANT_MODE,true);
            }
            else {
                uiContext.setProperty(InternalRequestParameterConstants.PRODUCT_VARIANT_MODE,false);
                log.debug("setProductVariantMode(): No variant.");
            }
            if (item.isChangeableProductVariant()){
				log.debug("setProductVariantMode(): Item is changeable product variant");            	
				uiContext.setChangeableProductVariantMode(true);
            }
            else {            	
				log.debug("setProductVariantMode(): Item is not a changeable product variant");            	
				uiContext.setChangeableProductVariantMode(false);
            }

        }
        else {
            log.error("setProductVariantMode: item is null!");
        }
    }

	/**
	 * Initializes the InstanceUIBean objects and associates them to the currentConfiguration.
	 * @param uiContext
	 * @param currentConfiguration
	 * @param request
	 * @return Reference to the root InstanceUIBean 
	 */
	protected InstanceUIBean createInstanceUIBeans(
				 UIContext uiContext,
				 ConfigUIBean currentConfiguration,
				 HttpServletRequest request) {
        
		List instances = currentConfiguration.getBusinessObject().getInstances();
		Instance rootInstance = currentConfiguration.getBusinessObject().getRootInstance();
		InstanceUIBean rootInstanceUIBean = UIBeanFactory.getUIBeanFactory().newInstanceUIBean(rootInstance, uiContext);
		List instanceUIBeans = new ArrayList(instances.size());
		Hashtable instanceUIBeansById = new Hashtable(instances.size());
		initializeInstanceUIBeans(request, rootInstanceUIBean, instanceUIBeans, instanceUIBeansById, uiContext);
		//initialize the instances of the ConfigUIBean
		currentConfiguration.setInstances(instanceUIBeans);
		currentConfiguration.setInstancesByID(instanceUIBeansById);
		currentConfiguration.setRootInstance(rootInstanceUIBean);
        
		return rootInstanceUIBean;

	}

	/**
	 * Iterates through the groups of the business object of currentInstanceUIBean.
	 * Creates UIBean objects for the groups to display and associates them to currentInstanceUIBean.
	 * @param uiContext
	 * @param currentInstanceUIBean
	 * @param relevantGroups
	 * @param request
	 * @throws IOException
	 * @throws ServletException
	 */
	public void createGroupUIBeans(
				 UIContext uiContext,
				 InstanceUIBean currentInstanceUIBean,
				 HashMap relevantGroups,
				 HttpServletRequest request
				 ) {
	    if (currentInstanceUIBean == null){
	    	// it might happen that in the UIModel an instance is given that is not (yet)
	    	// existing in the config model. 
	    	// Thus currentInstanceUIBean  might be null.
	    	return;
	    }
		GroupUIBean currentCharacteristicGroup = null;
		// first get a list of completely all available characteristicGroups
		List groupBusinessObjects = currentInstanceUIBean.getBusinessObject().getCharacteristicGroups(uiContext.getShowInvisibleCharacteristics(), uiContext.getPropertyAsBoolean(RequestParameterConstants.SHOW_GENERAL_TAB_FIRST));
		List groups = new Vector();
		List characteristics;
		IPCUIModel ui =	(IPCUIModel) uiContext.getProperty(IPCDynamicUIConstant.SESSION_DYNAMIC_UI);
		// if no group should be displayed we call the create-method only once for all characteristics.
		if(!uiContext.getPropertyAsBoolean(RequestParameterConstants.USE_GROUP_INFORMATION) && ui.getPageList().size() == 1){  
			//get all characteristics independent of their groups
			characteristics = currentInstanceUIBean.getBusinessObject().getCharacteristics(uiContext.getShowInvisibleCharacteristics());
			createCharacteristicUIBeans(uiContext, currentInstanceUIBean, null, characteristics, request);
		}else{
			//get all characteristics of the groups
			
			for (int i = 0; i < groupBusinessObjects.size(); i++) {
				CharacteristicGroup group = (CharacteristicGroup) groupBusinessObjects.get(i);
				// if the parameter showEmptyGroups = false (default), then show only
				// the groups which have visible characteristicValue
				if (!uiContext.showEmptyGroups() && !hasVisibleCharacteristics(uiContext, group)) {
					//do not add the group to the groups to be displayed
				}else {
					GroupUIBean groupUIBean = UIBeanFactory.getUIBeanFactory().newGroupUIBean(group, uiContext);
					groups.add(groupUIBean);
					//associate with instanceUIBean
					currentInstanceUIBean.addGroup(groupUIBean);
					// if the group is relevant (i.e. it is in the modelInfo),
					// we create CharacteristicUIBeans and ValueUIBeans for it 
					if (relevantGroups.get(groupUIBean.getName()) != null) {
						characteristics = groupUIBean.getBusinessObject().getCharacteristics(uiContext.getShowInvisibleCharacteristics());
						createCharacteristicUIBeans(uiContext, currentInstanceUIBean, groupUIBean, characteristics, request);
					}
				}
			}
		}
	}

	/**
	 * Iterates through the characteristics associated with the business object of currentInstanceUIBean
	 * or currentGroupUIBean.
	 * Creates ui bean objects for characteristics to display and associates them to the currentInstanceUIBean
	 * and to the currentGroupUIBean.
	 * Returns the reference to the currently active characteristic, that is the characteristic the page
	 * should scroll to.
	 * @param uiContext
	 * @param currentInstanceUIBean
	 * @param instanceChangedParameter
	 * @param currentGroupUIBean
	 * @param groupChangedParameter
	 * @param curCsticName
	 * @param csticStatusChange
	 * @param searchSetCstic
	 * @return
	 */
	protected void createCharacteristicUIBeans(UIContext uiContext,
									  InstanceUIBean currentInstanceUIBean,
									  GroupUIBean currentGroupUIBean,
									  List characteristics,
									  HttpServletRequest request) {

		// restrict the attributes 'readonly' and 'visible' dependent of the
		// business scenario (f.e. VehicleManager)
		if (uiContext.isCharacteristicsReadonlyViewSet() ||
			uiContext.isCharacteristicsVisibleViewSet() ) {

			for (int i=0; i < characteristics.size(); i++) {
				Characteristic cstic = (Characteristic) characteristics.get(i);
				
				//possibility to influence the properties of a cstic independent from the engine (VMS)
				//@TODO move into a seperate action
				if (uiContext.isCharacteristicVisibleRestricted( cstic.getName() ) ) {
					if ( cstic.isVisible() ) cstic.setVisible(false);
				}
				//possibility to influence the properties of a cstic independent from the engine (VMS)
				//@TODO move into a seperate action
				if (uiContext.isCharacteristicReadonlyRestricted( cstic.getName() ) ) {
					if ( ! cstic.isReadOnly() ) { 
						cstic.setReadOnly(true);
					}
					
				}
				// remove cstic if not required to display
				if (!cstic.isVisible() && !uiContext.getShowInvisibleCharacteristics()){
					characteristics.remove(i);
				}
			}
		}
        
		boolean showMessageCstics = uiContext.getPropertyAsBoolean(ProcessRequestParameterAction.SHOW_MESSAGE_CSTICS);
		String prefix = uiContext.getPropertyAsString(ProcessRequestParameterAction.MESSAGE_CSTICS_PREFIX);
        
		ArrayList characteristicUIBeans = new ArrayList();
		for (Iterator it = characteristics.iterator(); it.hasNext(); ) {
			Characteristic characteristic = (Characteristic)it.next();
			CharacteristicUIBean characteristicUIBean;
			if (showMessageCstics && characteristic.getName().startsWith(prefix)){
				characteristicUIBean = CharacteristicUIBean.getInstance(request, characteristic, uiContext, true);
			} 
			else {
				characteristicUIBean = CharacteristicUIBean.getInstance(request, characteristic, uiContext);
			}

			characteristicUIBeans.add(characteristicUIBean);
			// only add it to the group if existing
			if (currentGroupUIBean != null){
				currentGroupUIBean.addCharacteristic(characteristicUIBean);
			}
			currentInstanceUIBean.addCharacteristic(characteristicUIBean);
		}
		// now create the value-beans
		createValueUIBeans(request, uiContext, currentInstanceUIBean, currentGroupUIBean);
	}
 
	/**
	 * Creates UIBeans for the characteristic values and associates them to the passed
	 * InstanceUIBean or GroupUIBean.
	 * @param request
	 * @param uiContext
	 * @param currentInstanceUIBean
	 * @param group
	 * @throws IOException
	 * @throws ServletException
	 */
	protected void createValueUIBeans(
								 HttpServletRequest request,
								 UIContext uiContext,
								 InstanceUIBean currentInstanceUIBean,
								 GroupUIBean group){

	  Hashtable csticsBuffer = null;
	  if (!uiContext.getPropertyAsBoolean(RequestParameterConstants.ONLINE_EVALUATE)) {
		csticsBuffer = uiContext.getCharacteristicsBuffer();
	  }
	  Hashtable csticValues = new Hashtable();

	  List csticList;
	  if (uiContext.getUseGroupInformation() && group != null ) {
		  csticList = group.getCharacteristics();
	  } else {
		  csticList = currentInstanceUIBean.getCharacteristics();
	  }
	  Iterator csticListIt = csticList.iterator();
	  while (csticListIt.hasNext()) {
		  Vector returnedValues;
		  CharacteristicUIBean currentCharacteristic = (CharacteristicUIBean) csticListIt.next();
		  List allValues = currentCharacteristic.getBusinessObject().getValues();
		  if (csticsBuffer != null && csticsBuffer.size() > 0) {
			  String csticKey = currentCharacteristic.getBusinessObject().getInstance().getId()+"."+currentCharacteristic.getName();
			  Hashtable valueBuffer = (Hashtable) csticsBuffer.get(csticKey);
			  // check if there are buffered values, which would overwrite the real values!
			  if (valueBuffer != null) {
				  returnedValues = new Vector(valueBuffer.size());
				  // Please do not call Vector(valueBuffer.values()) because the elements will be in the
				  // wrong order!!!
				  for (Iterator valuesIt = allValues.iterator(); valuesIt.hasNext();) {
					  CharacteristicValue iValue = (CharacteristicValue) valuesIt.next();
					  CharacteristicValue bufValue = (CharacteristicValue) valueBuffer.get(iValue.getName());
					  ValueUIBean valueUIBean = UIBeanFactory.getUIBeanFactory().newValueUIBean(bufValue, uiContext);
					  returnedValues.add(valueUIBean);
					  currentCharacteristic.addValue(valueUIBean);
				  }
			  }else {
				  returnedValues = new Vector(allValues.size());
				  for (Iterator valuesIt = allValues.iterator(); valuesIt.hasNext(); ) {
					  CharacteristicValue value = (CharacteristicValue)valuesIt.next();
					  ValueUIBean valueUIBean = UIBeanFactory.getUIBeanFactory().newValueUIBean(value, uiContext);
					  returnedValues.add(valueUIBean);
					  currentCharacteristic.addValue(valueUIBean);
				  }
			  }
			  csticValues.put(currentCharacteristic.getName(), returnedValues);
		  }else {
			  returnedValues = new Vector(allValues.size());
			  for (Iterator valuesIt = allValues.iterator(); valuesIt.hasNext(); ) {
				  CharacteristicValue value = (CharacteristicValue)valuesIt.next();
				  ValueUIBean valueUIBean = UIBeanFactory.getUIBeanFactory().newValueUIBean(value, uiContext);
				  returnedValues.add(valueUIBean);
				  currentCharacteristic.addValue(valueUIBean);
			  }
			  csticValues.put(currentCharacteristic.getName(), returnedValues);
		  }
		  // set flag whether any characteristic has mimes or value mimes for the work area
		  // this is done during instantiation-process because of performance-reasons
		  if (currentCharacteristic.hasMimesForWorkArea() || currentCharacteristic.hasValueMimesForWorkArea()) {
			  currentInstanceUIBean.setAnyMimesForWorkArea(true);
			  if (group != null){
				  group.setAnyMimesForWorkArea(true);
			  }
	  }

	  }
	  log.debug(csticValues);
	}
    
    public static void main(String[] args) {
    }
    
}
