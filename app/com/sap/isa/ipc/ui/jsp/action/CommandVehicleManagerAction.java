package com.sap.isa.ipc.ui.jsp.action;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.sap.isa.core.RequestProcessor;
import com.sap.isa.core.SharedConst;
import com.sap.isa.core.util.RequestParser;
import com.sap.isa.core.util.VersionGet;
import com.sap.isa.ipc.ui.jsp.util.vms.HttpRequestParamMapper;
import com.sap.util.monitor.jarm.IMonitor;
//<--

public class CommandVehicleManagerAction extends Action
			  implements RequestParameterConstants, ActionForwardConstants {

	//BD 23052003: Changelist 42773 IPC 4.0 SP2 SAT
	//new-->
	IMonitor _monitor = null;
	private String SAT_STRING = "";
	//<--

	public CommandVehicleManagerAction() {
		super();
	}

	public ActionForward execute(ActionMapping mapping,
				 ActionForm form,
				 HttpServletRequest request,
				 HttpServletResponse response)
		throws IOException, ServletException{

		//BD 23052003: Changelist 42773 IPC 4.0 SP2 SAT
		//add the SAT Check handler to the session if SAT is turned of
		//new-->
		_monitor = (IMonitor)request.getAttribute(SharedConst.SAT_MONITOR);
		SAT_STRING = VersionGet.getSATRequestPrefix() + getClass().getName();
		startComponentIPCCLient("");
		//<--


		// see also
		//		command.do -> IPC Interface 3.0 and higher
		//      OnlineStore.do -> IPC Interface 2.0b for OnlineStore


		// first call:
		// http://server:port/ipc/ipc/VehicleManager.do!~initialTemplate=ipc_createConfig
		//                                              |--------------| |--------------|
		//                                                COMMAND_INIT    CRM_START_CONFIGURATION
		// second call:
		// http://server:port/ipc/ipc/VehicleManager.do!~template=ipc_websce
		//                                              |--------||--------------|
		//                                        COMMAND_EXECUTE  CRM_SHOW_CONFIGURATION


//        HttpSession session = request.getSession(false);
		RequestProcessor.setExtendedRequestAttribute(request, CALLER, VEHICLE_MANAGER);
		RequestProcessor.setExtendedRequestAttribute(request, IPC_SCENARIO, VEHICLE_MANAGER);
		HttpRequestParamMapper.mapParameterNames(request);

		// read all paramters for customizing the IPC UI
        RequestParser requestParser = new RequestParser(request);
		RequestParser.Parameter commandParameter = requestParser.getParameter(COMMAND_INIT);
		if (commandParameter.isSet()) {
//			String commandInit = commandParameter.getValue().getString();
			//BD 23052003: Changelist 42773 IPC 4.0 SP2 SAT
			//add the SAT Check handler to the session if SAT is turned of
			//new-->
			endComponentIPCCLient("");
			//<--
			return (mapping.findForward(INITIALIZE));
		}

		commandParameter = requestParser.getParameter(COMMAND_EXECUTE);
		if (commandParameter.isSet()) {
			String commandExecute = commandParameter.getValue().getString();

			if (commandExecute.equalsIgnoreCase(CRM_START_CONFIGURATION)) {
				//BD 23052003: Changelist 42773 IPC 4.0 SP2 SAT
				//add the SAT Check handler to the session if SAT is turned of
				//new-->
				endComponentIPCCLient("");
				//<--
				return (mapping.findForward(START_CONFIGURATION));
			}
			else if (commandExecute.equalsIgnoreCase(CRM_SHOW_CONFIGURATION)) {
				//BD 23052003: Changelist 42773 IPC 4.0 SP2 SAT
				//add the SAT Check handler to the session if SAT is turned of
				//new-->
				endComponentIPCCLient("");
				//<--
				return (mapping.findForward(SHOW_CONFIGURATION));
			}
			else
			{	
				//BD 23052003: Changelist 42773 IPC 4.0 SP2 SAT
				//add the SAT Check handler to the session if SAT is turned of
				//new-->
				endComponentIPCCLient("");
				//<--
				return (mapping.findForward(ILLEGAL_COMMAND_ERROR));
			}
		}

		//BD 23052003: Changelist 42773 IPC 4.0 SP2 SAT
		//add the SAT Check handler to the session if SAT is turned of
		//new-->
		endComponentIPCCLient("");
		//<--

		//return findForward.....
		return (mapping.findForward(ILLEGAL_COMMAND_ERROR));
	}
	
	//BD 25052003: Changelist 42773 IPC 4.0 SP2 SAT
	//new-->
	/*
	 * This method should simplify using the SAT-mechanismus startComponent(...) in 
	 * nested java-command-blocks. The Method use the SAT-method startComponent(...).
	 * 
	 * @param recordString : the String which you want record in the tracefile
	 */
	protected void startComponentIPCCLient(String recordString)
	{
		if(_monitor != null)
			_monitor.startComponent(SAT_STRING+recordString);
	}

	/*
	 * This method should simplify using the SAT-mechanismus startComponent(...) in 
	 * nested java-command-blocks. The Method use the SAT-method startComponent(...).
	 * 
	 * @param recordString : the String which you want record in the tracefile
	 */
	protected void endComponentIPCCLient(String recordString)
	{
		if(_monitor != null)
			_monitor.endComponent(SAT_STRING+recordString);
	}
	//<--
}
