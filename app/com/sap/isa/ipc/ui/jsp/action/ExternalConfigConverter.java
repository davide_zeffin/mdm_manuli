package com.sap.isa.ipc.ui.jsp.action;

import java.util.Enumeration;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.Vector;

import com.sap.msasrv.socket.shared.ErrorConstants;
import com.sap.spc.remote.client.util.cfg_ext_cstic_val;
import com.sap.spc.remote.client.util.cfg_ext_cstic_val_seq;
import com.sap.spc.remote.client.util.cfg_ext_inst;
import com.sap.spc.remote.client.util.cfg_ext_part;
import com.sap.spc.remote.client.util.cfg_ext_part_seq;
import com.sap.spc.remote.client.util.cfg_ext_price_key;
import com.sap.spc.remote.client.util.cfg_ext_price_key_seq;
import com.sap.spc.remote.client.util.ext_configuration;
import com.sap.spc.remote.client.util.imp.c_ext_cfg_imp;
import com.sap.spc.remote.client.util.imp.c_ext_cfg_inst_seq_imp;
import com.sap.spc.remote.shared.ConfigException;
import com.sap.spc.remote.shared.ParameterSet;
import com.sap.spc.remote.shared.SimpleRequest;
import com.sap.spc.remote.shared.command.SCECommand;

/**
 * Provides methods that convert external configurations to parameter name/value pairs 
 * and backward.
 * This ExternalConfigConverter uses the BAPI-Naming (like CFG-ROOT_ID, PRT-PARENT_ID, INS-INST_ID, etc.)
 * There are also other ExternalConfigConverters for other naming formats:
 * @see com.sap.spc.remote.client.rfc.ExternalConfigConverter (RFC 50 naming)
 * @see com.sap.spc.remote.client.tcp.ExternalConfigConverter (default naming)
 * It is possible to convert from each naming to each other naming via the getConfigs() method that returns 
 * ext_configuration object which can be used with the createConfig() methods. 
 */
public class ExternalConfigConverter implements RequestParameterConstants{


	public static ext_configuration createConfig(SimpleRequest request, String client,
										  String kbName, String kbVersion, String kbProfile,
										  String kbLanguage, String defaultSceVersion) throws ConfigException {

		if (kbName == null) {
			kbName = request.getParameterValue(EXT_CONFIG_KB_NAME);
		}
		if (kbVersion == null) {
			kbVersion = request.getParameterValue(EXT_CONFIG_KB_VERSION);
		}
		if (kbProfile == null) {
			kbProfile = request.getParameterValue(EXT_CONFIG_KB_PROFILE, "");
		}
		if (kbLanguage == null) {
			kbLanguage = request.getParameterValue(EXT_CONFIG_KB_LANGUAGE, "");
		}
		c_ext_cfg_imp writable = new c_ext_cfg_imp(request.getParameterValue(EXT_CONFIG_NAME,kbName),
																			  request.getParameterValue(EXT_CONFIG_SCE_VERSION,defaultSceVersion),
																			  client,
																			  kbName,
																			  kbVersion,
																			  Integer.parseInt(request.getParameterValue(EXT_CONFIG_KB_BUILD,"1").trim()),
																			  kbProfile,
																			  // 20001127-kha: the KB language is now always ignored. Instead, the language of the document
																			  // is used. This makes sense: If I configure in English, leave the system, come back in German
																			  // to continue my configuration I don't want the saved language for the language dependent
																			  // texts but the current document language that is consistent with the UI texts.
																			  //request.getParameterValue(EXT_CONFIG_KB_LANGUAGE,kbLanguage),
																			  kbLanguage,
																			  new Integer(getRequiredParameter(request,EXT_CONFIG_ROOT_ID).trim()),
																			  request.getParameterValue(EXT_CONFIG_CONSISTENT,SCECommand.TRUE).equals(SCECommand.TRUE),
																			  request.getParameterValue(EXT_CONFIG_COMPLETE,SCECommand.TRUE).equals(SCECommand.TRUE)
																			  );
		String configInfo = request.getParameterValue(EXT_CONFIG_INFO);
		if (configInfo != null)
			writable.set_cfg_info(configInfo);

		// add instance info
		String[] instIds = getRequiredParameters(request, EXT_INST_ID);
		String[] instObjType = getRequiredParameters(request, EXT_INST_OBJECT_TYPE);
		String[] instClassType = getRequiredParameters(request, EXT_INST_CLASS_TYPE);
		String[] instObjKey = getRequiredParameters(request, EXT_INST_OBJECT_KEY);
		String[] instObjText = getRequiredParameters(request, EXT_INST_OBJECT_TEXT);
		String[] instAuthor = request.getParameterValues(EXT_INST_AUTHOR, constantStringArray(instIds.length, " "));
		String[] instQuantity = getRequiredParameters(request, EXT_INST_QUANTITY);
		String[] instConsistent = request.getParameterValues(EXT_INST_CONSISTENT, constantStringArray(instIds.length, SCECommand.TRUE));
		String[] instComplete = request.getParameterValues(EXT_INST_COMPLETE, constantStringArray(instIds.length, SCECommand.TRUE));

		// quantity units are optional with "ST" as default
		String[] instQuantityUnits = request.getParameterValues(EXT_INST_QUANTITY_UNIT, constantStringArray(instIds.length, "ST"));

		checkLength(new int[] { instIds.length, instObjType.length, instClassType.length, instObjKey.length,
					instObjText.length, instAuthor.length, instQuantity.length,
					instConsistent.length, instComplete.length, instQuantityUnits.length },
					new String[] { "inst id", "object type", "class type", "object key", "object text",
						"author", "quantity", "consistent", "complete", "quantity unit" } );

		for (int i=0; i<instIds.length; i++) {
			if (instAuthor[i] == null || instAuthor[i].length() == 0)
				instAuthor[i] = " ";

			writable.add_inst(new Integer(instIds[i].trim()),
							  instObjType[i], instClassType[i],
							  instObjKey[i], instObjText[i],
							  instAuthor[i], instQuantity[i],
							  instQuantityUnits[i],
							  instConsistent[i].equals(SCECommand.TRUE),
							  instComplete[i].equals(SCECommand.TRUE));
		}

		// add part info (if available)
		String[] partParentId = request.getParameterValues(EXT_PART_PARENT_ID);
		if (partParentId != null) {
			String[] partInstId = getRequiredParameters(request,EXT_PART_INST_ID);
			String[] partPosNr = getRequiredParameters(request,EXT_PART_POS_NR);
			String[] partObjType = getRequiredParameters(request,EXT_PART_OBJECT_TYPE);
			String[] partClassType = getRequiredParameters(request,EXT_PART_CLASS_TYPE);
			String[] partObjKey = getRequiredParameters(request,EXT_PART_OBJECT_KEY);
			String[] partAuthor = request.getParameterValues(EXT_PART_AUTHOR, constantStringArray(partParentId.length, " "));
			// the sales relevant array is not part of the BAPI interface, so we make it
			// optional. External configs and CRM provide it, BAPI will use the default
			String[] partSalesRelevant = request.getParameterValues(EXT_PART_SALES_RELEVANT,constantStringArray(partParentId.length, " "));

			checkLength(new int[] { partParentId.length, partInstId.length, partPosNr.length,
						partObjType.length, partClassType.length, partObjKey.length,
						partAuthor.length, partSalesRelevant.length },
						new String[] { "part parent id", "part inst id", "part posnr", "part object type",
							"part class type", "part object key", "part author", "part sales relevant" } );

			for (int i=0; i<partParentId.length; i++) {
				if (partAuthor[i] == null || partAuthor[i].length() == 0)
					partAuthor[i] = " ";
				writable.add_part(new Integer(partParentId[i].trim()),
								  new Integer(partInstId[i].trim()),
								  partPosNr[i],
								  partObjType[i], partClassType[i],
								  partObjKey[i], partAuthor[i],
								  partSalesRelevant[i].equals(SCECommand.SALES_RELEVANT));
			}
		}

		// add cstic value info (if available)
		String[] valueInstId = request.getParameterValues(EXT_VALUE_INSTANCE_ID);
		if (valueInstId != null) {
			String[] valueCsticName = getRequiredParameters(request,EXT_VALUE_CSTIC_NAME);
			String[] valueCsticLName = getRequiredParameters(request,EXT_VALUE_CSTIC_LNAME);
			String[] valueName = getRequiredParameters(request,EXT_VALUE_NAME);
			String[] valueLName = getRequiredParameters(request,EXT_VALUE_LNAME);
			String[] valueAuthor = request.getParameterValues(EXT_VALUE_AUTHOR, constantStringArray(valueCsticName.length, " "));

			checkLength(new int[] { valueInstId.length, valueCsticName.length,
						valueCsticLName.length, valueName.length,
						valueLName.length, valueAuthor.length }, new String[] { "value inst id",
							"cstic name", "cstic lname", "value name", "value lname", "value author" } );

			for (int i=0; i<valueInstId.length; i++) {
				if (valueAuthor[i] == null || valueAuthor[i].length() == 0)
					valueAuthor[i] = " ";
				writable.add_cstic_value(new Integer(valueInstId[i].trim()),
										 valueCsticName[i], valueCsticLName[i],
										 valueName[i], valueLName[i],
										 valueAuthor[i]);
			}
		}

		// add price keys (if available)
		String[] priceInstId = request.getParameterValues(EXT_PRICE_INSTANCE_ID);
		if (priceInstId != null) {
			String[] priceKey = getRequiredParameters(request,EXT_PRICE_KEY);
			String[] priceFactor = getRequiredParameters(request,EXT_PRICE_FACTOR);

			checkLength(new int[] { priceInstId.length, priceKey.length, priceFactor.length },
						new String[] { "price inst id", "price key", "price factor" }
						);

			for (int i=0; i<priceInstId.length; i++) {
				                     
					String factor = priceFactor[i].trim();                     
					int index = factor.toUpperCase().indexOf("E ");            
					if (index >= 0){                                           
						String number = factor.substring(0, index);            
						String exponent = factor.substring(index+2);           
						factor = number + "E+" + exponent;                     
					}                                                          
				writable.add_price_key(new Integer(priceInstId[i].trim()),
									   priceKey[i],
								   Double.valueOf(factor).doubleValue());      
				                                                         

			}
		}

		return writable;
	}


	private static String[] getRequiredParameters(SimpleRequest request, String name) throws ConfigException {
		String[] value = request.getParameterValues(name);
		if (value == null) {
			throw new ConfigException(ErrorConstants.REQUIRED_PARAMETER_NOT_FOUND, new String[] { name });
		}
		else {
		    return value;
		}
	}


	private static String getRequiredParameter(SimpleRequest request, String name) throws ConfigException {
		String value = request.getParameterValue(name);
		if (value == null) {
			throw new ConfigException(ErrorConstants.REQUIRED_PARAMETER_NOT_FOUND, new String[] { name });
		}
		else {
		    return value;
		}
	}

	private static void checkLength(int[] lengths, String[] ids) throws ConfigException {
		if (lengths.length < 2)
			return;
		int length = lengths[0];
		for (int i=1; i<lengths.length; i++)
			if (length != lengths[i])
				throw new ConfigException(ErrorConstants.PARAMETER_ARRAY_LENGTH_MISMATCH,
									      new String[] { ids[0], ids[i] });
	}

	private static String[] constantStringArray(int length, String value) {
		String[] a = new String[length];
		for (int i=0; i<length; i++)
			a[i] = value;
		return a;
	}
    /**
     * Updates param with all IPC parameter name/value pairs that are needed to represent all external configurations
     * in extCfgs. Only to be used by server components!
     * @param   extCfg  Vector of ext_configuration
     * @param   instIdToItemIdTables Vector of (Hashtable mapping String instids to String itemIds). May be null if no item mapping is needed
     * @param   posExInt initial config position number (eg. 1)
     * @param   cfgId initial config id (eg. 1)
     * @param   something implementing ParameterSet that will be updated by this method.
     * @return  nothing - param will be updated.
     */
    public static void getConfigs(Vector extCfgs, Vector instIdToItemIdTables, int posExInt, int cfgId, ParameterSet param) {
        getConfigs(extCfgs, instIdToItemIdTables, posExInt, cfgId, param, true);
    }


    /**
     * Updates param with all IPC parameter name/value pairs that are needed to represent all external configurations
     * in extCfgs.
     * Uses BAPI-naming.
     * @param   extCfg  Vector of ext_configuration
     * @param   instIdToItemIdTables Vector of (Hashtable mapping String instids to String itemIds). May be null if no item mapping is needed
     * @param   posExInt initial config position number (eg. 1)
     * @param   cfgId initial config id (eg. 1)
     * @param   something implementing ParameterSet that will be updated by this method.
     * @param   useIndices attach a numeric index [<number>] to each parameter name. If you want to use the result
     * in a server response, use true here, if you want to use this in a request, use false.
     * @return  nothing - param will be updated.
     */
    public static void getConfigs(Vector extCfgs, Vector instIdToItemIdTables, int posExInt, int cfgId, ParameterSet param, boolean useIndices) {

        //BAPICUCFG
        int initialCfgId = cfgId;

        Vector parts = new Vector();
        Vector insts = new Vector();
        Vector csticVals = new Vector();
        Vector conditions = new Vector();

        for (Enumeration e = extCfgs.elements(); e.hasMoreElements();) {
            ext_configuration extCfg = (ext_configuration)e.nextElement();

            setParameterValue(param, EXT_CONFIG_POSITION,    useIndices, cfgId, Integer.toString(posExInt));
            posExInt++;
            setParameterValue(param, EXT_CONFIG_NAME,          useIndices, cfgId, Integer.toString(cfgId));
            setParameterValue(param, EXT_CONFIG_ROOT_ID,     useIndices, cfgId, ""+extCfg.get_root_id());
            setParameterValue(param, EXT_CONFIG_SCE_VERSION,    useIndices, cfgId, "1");
            setParameterValue(param, EXT_CONFIG_KB_NAME,     useIndices, cfgId, extCfg.get_kb_name());
            setParameterValue(param, EXT_CONFIG_KB_VERSION,  useIndices, cfgId, extCfg.get_kb_version());
            setParameterValue(param, EXT_CONFIG_KB_PROFILE,  useIndices, cfgId, extCfg.get_kb_profile_name());
            setParameterValue(param, EXT_CONFIG_KB_BUILD,    useIndices, cfgId, ""+extCfg.get_kb_build());
            setParameterValue(param, EXT_CONFIG_INFO,        useIndices, cfgId, extCfg.get_cfg_info());

            setParameterValue(param, EXT_CONFIG_KB_LANGUAGE, useIndices, cfgId, extCfg.get_language());
            setParameterValue(param, EXT_CONFIG_NAME,        useIndices, cfgId, "");
            setParameterValue(param, EXT_CONFIG_COMPLETE,    useIndices, cfgId, extCfg.is_complete_p()? T: F);

            setParameterValue(param, EXT_CONFIG_CONSISTENT,  useIndices, cfgId, extCfg.is_consistent_p()? T: F);

            setParameterValue(param, EXT_CONFIG_SCE_VERSION, useIndices, cfgId, extCfg.get_sce_version());

            parts.addElement(extCfg.get_parts());
            insts.addElement(extCfg.get_insts());
            csticVals.addElement(extCfg.get_cstics_values());
            conditions.addElement(extCfg.get_price_keys());
            cfgId++;
        }

        _getConfigParts(parts, initialCfgId, param, useIndices);
        _getConfigInsts(insts, initialCfgId, param, useIndices);
        if (instIdToItemIdTables != null) {
            _getConfigItems(insts, instIdToItemIdTables, initialCfgId, param, useIndices);
        }
        _getConfigCsticsVals(csticVals, initialCfgId, param, useIndices);
        _getConfigPrices(conditions, initialCfgId, param, useIndices);
    }
    
    private static void setParameterValue(ParameterSet set, String param, boolean useIndices, int index, String value) {
        if (useIndices) {
            set.setValue(param, index, value);
        }
        else {
            set.setValue(param, value);
        }
    }

    private static void _getConfigInsts(Vector instances, int cfgId, ParameterSet param, boolean useIndices) {

        int index = 0;

        for (Enumeration e=instances.elements(); e.hasMoreElements();) {
            c_ext_cfg_inst_seq_imp insts = (c_ext_cfg_inst_seq_imp)e.nextElement();
            for (Enumeration enumeration= insts.elements(); enumeration.hasMoreElements();) {
                cfg_ext_inst inst = (cfg_ext_inst)enumeration.nextElement();
                index++;

                setParameterValue(param, EXT_INST_CONFIG_ID,        useIndices, index, Integer.toString(cfgId));
                setParameterValue(param, EXT_INST_ID,               useIndices, index, ""+inst.get_inst_id());
                setParameterValue(param, EXT_INST_OBJECT_TYPE,      useIndices, index, inst.get_obj_type());
                setParameterValue(param, EXT_INST_CLASS_TYPE,       useIndices, index, inst.get_class_type());
                setParameterValue(param, EXT_INST_OBJECT_KEY,       useIndices, index, inst.get_obj_key());
                setParameterValue(param, EXT_INST_OBJECT_TEXT,      useIndices, index, inst.get_obj_txt());
                setParameterValue(param, EXT_INST_QUANTITY,         useIndices, index, inst.get_quantity());
                setParameterValue(param, EXT_INST_QUANTITY_UNIT,    useIndices, index, inst.get_quantity_unit());
                setParameterValue(param, EXT_INST_AUTHOR,           useIndices, index, inst.get_author());
                setParameterValue(param, EXT_INST_COMPLETE,         useIndices, index, inst.is_complete_p() ? T : F);
                setParameterValue(param, EXT_INST_CONSISTENT,       useIndices, index, inst.is_consistent_p() ? T : F);
            }
            cfgId++;
        }
    }



    private static void _getConfigItems(Vector instances, Vector instIdToItemIdTables, int cfgId, ParameterSet param, boolean useIndices) {

        int index = 0;
        Enumeration tables = instIdToItemIdTables.elements();
        for (Enumeration e=instances.elements(); e.hasMoreElements();) {
            c_ext_cfg_inst_seq_imp insts = (c_ext_cfg_inst_seq_imp)e.nextElement();
            Hashtable instIdToItemIdTable = (Hashtable)tables.nextElement();
            for (Enumeration enumeration= insts.elements(); enumeration.hasMoreElements();) {
                cfg_ext_inst inst = (cfg_ext_inst)enumeration.nextElement();

                String instId = Integer.toString(inst.get_inst_id().intValue());
                String itemId = (String)instIdToItemIdTable.get(instId);

                index++;
                setParameterValue(param, POS_CONFIG_ID, useIndices, index, Integer.toString(cfgId));
                setParameterValue(param, POS_INST_ID,   useIndices, index, instId);
                setParameterValue(param, POS_POS_GUID,   useIndices, index, itemId);
            }
            cfgId++;
        }
    }



    private static void _getConfigParts(Vector partsVec, int cfgId, ParameterSet param, boolean useIndices) {

        int index = 0;

        for (Enumeration e=partsVec.elements(); e.hasMoreElements();) {
            cfg_ext_part_seq parts = (cfg_ext_part_seq)e.nextElement();
            for (Iterator enumeration= parts.iterator(); enumeration.hasNext();) {
                cfg_ext_part part = (cfg_ext_part)enumeration.next();

                index++;
                setParameterValue(param, EXT_PART_CONFIG_ID,       useIndices, index, Integer.toString(cfgId));
                setParameterValue(param, EXT_PART_PARENT_ID,       useIndices, index, ""+part.get_parent_id());
                setParameterValue(param, EXT_PART_INST_ID,         useIndices, index, ""+part.get_inst_id());
                setParameterValue(param, EXT_PART_POS_NR,          useIndices, index, part.get_pos_nr());
                setParameterValue(param, EXT_PART_OBJECT_TYPE,     useIndices, index, part.get_obj_type());
                setParameterValue(param, EXT_PART_CLASS_TYPE,      useIndices, index, part.get_class_type());
                setParameterValue(param, EXT_PART_OBJECT_KEY,      useIndices, index, part.get_obj_key());
                setParameterValue(param, EXT_PART_AUTHOR,          useIndices, index, part.get_author());
                setParameterValue(param, EXT_PART_SALES_RELEVANT,  useIndices, index, part.is_sales_relevant_p() ? "X":"");
            }
            cfgId++;
        }
    }



    private static void _getConfigPrices(Vector priceSeqs, int cfgId, ParameterSet param, boolean useIndices) {

        int index = 0;
        for (Enumeration enum=priceSeqs.elements(); enum.hasMoreElements();) {
            cfg_ext_price_key_seq seq = (cfg_ext_price_key_seq)enum.nextElement();

            for (Iterator e = seq.iterator(); e.hasNext();) {
                cfg_ext_price_key key = (cfg_ext_price_key)e.next();
                index++;
                //a3-349tl: priceFactor is cut to have at most 15 digits
                double factor = key.get_factor();
                String priceFactor = key.getFactorAsChar15();
                setParameterValue(param, EXT_VALUE_CONFIG_ID,   useIndices, index, Integer.toString(cfgId));
                setParameterValue(param, EXT_PRICE_INSTANCE_ID, useIndices, index, key.get_inst_id().toString());
                setParameterValue(param, EXT_PRICE_FACTOR,      useIndices, index, priceFactor);
                setParameterValue(param, EXT_PRICE_KEY,         useIndices, index, key.get_key());
            }
            cfgId++;
        }
    }

    private static void _getConfigCsticsVals(Vector csticVals, int cfgId, ParameterSet param, boolean useIndices) {

        int index = 0;

        for (Enumeration e=csticVals.elements(); e.hasMoreElements();) {
            cfg_ext_cstic_val_seq seq = (cfg_ext_cstic_val_seq)e.nextElement();
            for (Iterator enumeration= seq.iterator(); enumeration.hasNext();) {
                index++;

                cfg_ext_cstic_val item = (cfg_ext_cstic_val)enumeration.next();

                setParameterValue(param, EXT_VALUE_CONFIG_ID,   useIndices, index, Integer.toString(cfgId));
                setParameterValue(param, EXT_VALUE_INSTANCE_ID, useIndices, index, ""+item.get_inst_id());
                setParameterValue(param, EXT_VALUE_CSTIC_NAME,  useIndices, index, item.get_charc());
                setParameterValue(param, EXT_VALUE_CSTIC_LNAME, useIndices, index, item.get_charc_txt());
                setParameterValue(param, EXT_VALUE_NAME,        useIndices, index, item.get_value());
                setParameterValue(param, EXT_VALUE_LNAME,       useIndices, index, item.get_value_txt());
                setParameterValue(param, EXT_VALUE_AUTHOR,      useIndices, index, item.get_author());
            }
            cfgId++;
        }
    }
    
}