/**
 *  GetConflictsAction returns a list of conflicts
 *  In the jsp page you can access the list of conflicts using
 *  the following method:
 *  List conflicts = (List)request.getAttribute(RequestParameterConstants.DESCRIPTIVE_CONFLICTS);
 */
package com.sap.isa.ipc.ui.jsp.action;

import java.io.IOException;
import java.util.List;
import java.util.Vector;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.sap.isa.core.UserSessionData;
import com.sap.isa.core.businessobject.management.MetaBusinessObjectManager;
import com.sap.isa.core.util.RequestParser;
import com.sap.isa.ipc.ui.jsp.uiclass.UIContext;
import com.sap.spc.remote.client.object.Configuration;
import com.sap.spc.remote.client.object.DescriptiveConflict;
import com.sap.spc.remote.client.object.DescriptiveConflictType;
import com.sap.spc.remote.client.object.ExplanationTool;
import com.sap.spc.remote.client.object.IPCException;

public class GetDescriptiveConflictsAction extends IPCBaseAction implements RequestParameterConstants,
                                                              ActionForwardConstants{

	public ActionForward ecomPerform(ActionMapping mapping,
		ActionForm form,
		HttpServletRequest request,
		HttpServletResponse response,
		UserSessionData userSessionData,
		RequestParser requestParser,
		MetaBusinessObjectManager mbom,
		boolean multipleInvocation,
		boolean browserBack)
			throws IOException, ServletException {

		UIContext uiContext = getUIContext(request);

        Configuration currentConfiguration = uiContext.getCurrentConfiguration();
        if(currentConfiguration == null)
          return (mapping.findForward(INTERNAL_ERROR));

		List descriptiveConflicts = new Vector();
//		DescriptiveConflict conflict;
		ExplanationTool explanationTool = currentConfiguration.getExplanationTool();
//		String currentInstanceId = request.getParameter(RequestParameterConstants.CURRENT_INSTANCE_ID);
//      Instance currentInstance = currentConfiguration.getInstance(currentInstanceId);
		descriptiveConflicts = explanationTool.getDescriptiveConflicts(uiContext.getConflictExplanationLevel(),
        uiContext.getConflictExplanationTextBlockTag(),
        uiContext.getConflictExplanationTextLineTag(),
        uiContext.showConflictSolutionRate(),
        uiContext.getLocale(),
        this.getResources(request),
//TODO        uiContext.getApplication(),
        uiContext.useConflictHandlingShortcuts(),
        uiContext.useValueConflictShortcut(),
        uiContext.useExplanationTool());
		if(descriptiveConflicts == null)
		descriptiveConflicts = new Vector();

		customerExit(mapping, form, request, response, userSessionData, requestParser, mbom, multipleInvocation, browserBack);

		request.setAttribute(DESCRIPTIVE_CONFLICTS, descriptiveConflicts);

//      check where to go
		if(request.getParameter(FORWARD_DESTINATION) == null &&
			request.getAttribute(FORWARD_DESTINATION) == null)
			return mapping.findForward(SUCCESS);

		String forwardDestination = request.getParameter(FORWARD_DESTINATION);
		if(forwardDestination == null)
			forwardDestination = (String)request.getAttribute(FORWARD_DESTINATION);
		if(forwardDestination.length() == 0)
			return mapping.findForward(SUCCESS);
//      check whether to go to shortcuts
		if(forwardDestination.equals(SHORTCUTS))
			if(checkShortcuts(request, uiContext))
			    return mapping.findForward(SHORTCUTS);
			else
				return mapping.findForward(NO_SHORTCUTS);

//      check whether it is a check for messages
		if(forwardDestination.equals(CHECK_MESSAGES)){
		    if(checkShortcuts(request, uiContext))
				request.setAttribute(IPC_HAS_MESSAGES, new Boolean(true));
			else if(uiContext.getPropertyAsBoolean(SHOW_STANDARD_MESSAGE_IN_MESSAGE_AREA))
				request.setAttribute(IPC_HAS_MESSAGES, new Boolean(true));
			else request.setAttribute(IPC_HAS_MESSAGES, new Boolean(false));
			return mapping.findForward(CHECK_MESSAGES);
		}

  	    return mapping.findForward(forwardDestination);

	}

	private boolean checkShortcuts(HttpServletRequest request, UIContext uiContext){
		List descriptiveConflicts = (List)request.getAttribute(DESCRIPTIVE_CONFLICTS);
		try{
			if(uiContext.getPropertyAsBoolean(RequestParameterConstants.USE_SHORTCUT_VALUE_CONFLICT) &&
				(descriptiveConflicts.size() == 1)){
				DescriptiveConflict descriptiveConflict =
								(DescriptiveConflict)descriptiveConflicts.get(0);
				if(!descriptiveConflict.hasShortcut() ||
					(descriptiveConflict.getConflictTypes(
                    uiContext.showConflictSolutionRate(),
                    uiContext.getLocale(),
                    this.getResources(request),
//TODO              uiContext.getApplication(),
                    uiContext.getConflictExplanationTextLineTag(),
                    uiContext.useConflictHandlingShortcuts(),
                    uiContext.useValueConflictShortcut(),
                    uiContext.useExplanationTool()).size() != 1))
					return false;
				DescriptiveConflictType conflictType =
					(DescriptiveConflictType)descriptiveConflict.getConflictTypes(
                    uiContext.showConflictSolutionRate(),
                    uiContext.getLocale(),
                    getResources(request),
//                  uiContext.getApplication(),
                    uiContext.getConflictExplanationTextLineTag(),
                    uiContext.useConflictHandlingShortcuts(),
                    uiContext.useValueConflictShortcut(),
                    uiContext.useExplanationTool()).get(0);
				if(conflictType.getNumberOfMembers() != 2)
					return false;
				if(conflictType.getType().equals("conflict_value")){
					request.setAttribute(CONFLICT_VALUE, conflictType);
					return true;
				}
			}
		}catch(IPCException ex){
			log.fatal(this, ex);
			request.setAttribute(IPC_EXCEPTION, ex);
		}
	    return false;
	}
}