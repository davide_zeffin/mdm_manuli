/**
 * The action GetInstancesAction returns a list of instances
 * If no instances are found you get an empty list of instances
 * The list of instances is stored in the request;
 * In the jsp page you can access the the list of instances
 * using the following method:
 *
 * List instances = (List)request.getAttribute(RequestParameterConstants.INSTANCES);
 *
 * The action also adds the root instance as currentInstance
 * to the UIContext.
 */
package com.sap.isa.ipc.ui.jsp.action;

import java.io.IOException;
import java.util.Hashtable;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.sap.isa.core.Constants;
import com.sap.isa.core.UserSessionData;
import com.sap.isa.core.businessobject.management.MetaBusinessObjectManager;
import com.sap.isa.core.util.RequestParser;
import com.sap.isa.ipc.ui.jsp.beans.ConfigUIBean;
import com.sap.isa.ipc.ui.jsp.beans.GroupUIBean;
import com.sap.isa.ipc.ui.jsp.beans.InstanceUIBean;
import com.sap.isa.ipc.ui.jsp.constants.InstancesRequestParameterConstants;
import com.sap.isa.ipc.ui.jsp.uiclass.UIContext;

/**
 * Precondition: StartConfiguration was called in the same http session in order
 * to fill the uiContext with the configuration.
 * Takes an instance id from the request, gets the associated Instance from the
 * Configuration and puts it to the request as an Attribute.
 * Puts all Instances of the configuration into the request as attribute.
 */
public class GetInstancesAction extends IPCBaseAction {

	public ActionForward ecomPerform(ActionMapping mapping,
		ActionForm form,
		HttpServletRequest request,
		HttpServletResponse response,
		UserSessionData userSessionData,
		RequestParser requestParser,
		MetaBusinessObjectManager mbom,
		boolean multipleInvocation,
		boolean browserBack)
			throws IOException, ServletException {

        UIContext uiContext = getUIContext(request);
        
        ConfigUIBean currentConfiguration = this.getCurrentConfiguration(uiContext);
        if(currentConfiguration == null)
          return this.parameterError(mapping, request, InternalRequestParameterConstants.CURRENT_CONFIGURATION, this.getClass().getName());
		String currentInstanceId = getContextValue(request, Constants.CURRENT_INSTANCE_ID);
//		if (!isSet(currentInstanceId)) {
//			currentInstanceId = getContextValue(request, Constants.CURRENT_INSTANCE_ID);
//		}
		//if not in request nor in context, reset to the root instance id (e.g. when starting config)
		if (!isSet(currentInstanceId)) {
			currentInstanceId = uiContext.getCurrentConfiguration().getRootInstance().getId();
		}

        InstanceUIBean currentInstanceUIBean = this.getCurrentInstance(uiContext, currentConfiguration, currentInstanceId, request);
        
		String curCharGroupName = getContextValue(request, Constants.CURRENT_CHARACTERISTIC_GROUP_NAME);
//		if (!isSet(curCharGroupName)) {
//			curCharGroupName = getContextValue(request, Constants.CURRENT_CHARACTERISTIC_GROUP_NAME);
//		}
		String curScrollGroupName = getContextValue(request, Constants.CURRENT_SCROLL_CHARACTERISTIC_GROUP_NAME);
//		if (!isSet(curScrollGroupName)) {
//			curScrollGroupName = getContextValue(request, Constants.CURRENT_SCROLL_CHARACTERISTIC_GROUP_NAME);
//		}
		String groupStatusChangeName = getContextValue(request, Constants.GROUP_STATUS_CHANGE);
//		if (!isSet(groupStatusChangeName)) {
//			groupStatusChangeName = getContextValue(request, Constants.GROUP_STATUS_CHANGE);
//		}
		Hashtable groupStatus = (Hashtable) uiContext.getProperty(Constants.GROUP_STATUS_CHANGE);
        GroupUIBean currentGroupUIBean = this.getCharacteristicGroups(uiContext, currentInstanceUIBean, curCharGroupName, curScrollGroupName, groupStatusChangeName, groupStatus, request);

		customerExit(mapping, form, request, response, userSessionData, requestParser, mbom, multipleInvocation, browserBack);

		request.setAttribute(InstancesRequestParameterConstants.CURRENT_CONFIGURATION, currentConfiguration);
        request.setAttribute(InstancesRequestParameterConstants.CURRENT_INSTANCE, currentInstanceUIBean);
		request.setAttribute(InstancesRequestParameterConstants.CURRENT_CHARACTERISTIC_GROUP, currentGroupUIBean);
        
        return mapping.findForward(ActionForwardConstants.SUCCESS);


    }
    
}

