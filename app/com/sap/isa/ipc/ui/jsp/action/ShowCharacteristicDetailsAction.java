/*
 * Created on 23.11.2004
 *
 * To change the template for this generated file go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
package com.sap.isa.ipc.ui.jsp.action;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.sap.isa.businessobject.CommunicationException;
import com.sap.isa.core.Constants;
import com.sap.isa.core.ContextConst;
import com.sap.isa.core.logging.LogUtil;
import com.sap.isa.core.UserSessionData;
import com.sap.isa.core.businessobject.management.MetaBusinessObjectManager;
import com.sap.isa.core.util.RequestParser;
import com.sap.isa.ipc.ui.jsp.dynamicui.IPCDynamicUIConstant;
import com.sap.isa.ipc.ui.jsp.dynamicui.IPCUIModel;
import com.sap.isa.ipc.ui.jsp.uiclass.UIContext;
import com.sap.isa.maintenanceobject.action.DynamicUIActionHelper;

/**
 * @author 
 * Is executed when the user clicks on the "Details" link of a characteristic.
 * Forwards to the current layout with the component characteristicDetails in the
 * configworkarea.
 * To change the template for this generated type comment go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
public class ShowCharacteristicDetailsAction extends IPCBaseAction {

	/* (non-Javadoc)
	 * @see com.sap.isa.core.BaseAction#doPerform(org.apache.struts.action.ActionMapping, org.apache.struts.action.ActionForm, javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse)
	 */
	public ActionForward ecomPerform(ActionMapping mapping,
		ActionForm form,
		HttpServletRequest request,
		HttpServletResponse response,
		UserSessionData userSessionData,
		RequestParser requestParser,
		MetaBusinessObjectManager mbom,
		boolean multipleInvocation,
		boolean browserBack)
			throws IOException, ServletException {
		UIContext uiContext = getUIContext(request);
		String curInstId = request.getParameter(Constants.CURRENT_INSTANCE_ID);
		if (isSet(curInstId)) {
			changeContextValue(request, Constants.CURRENT_INSTANCE_ID, curInstId);
		}
//		if (!uiContext.getOnlineEvaluate() && !browserBack) {
//			ActionForward forward = bufferCharacteristicValues(mapping, request, curInstId);
//			if (!forward.getName().equals(ActionForwardConstants.SUCCESS)) {
//				return forward;
//			}
//		}

		String groupName = request.getParameter(Constants.CURRENT_CHARACTERISTIC_GROUP_NAME);
		if (!isSet(groupName)) {
			return parameterError(mapping, request, Constants.CURRENT_CHARACTERISTIC_GROUP_NAME, getClass().getName());
		}
		changeContextValue(request, Constants.CURRENT_CHARACTERISTIC_GROUP_NAME, groupName);

		String curCsticName =
			request.getParameter(
				Constants.CURRENT_CHARACTERISTIC_NAME);
		if (isSet(curCsticName)) {
			changeContextValue(request, Constants.CURRENT_CHARACTERISTIC_NAME, curCsticName);
		}

        // value name is necessary for message-cstic details page
        String curValueName = request.getParameter(Constants.CURRENT_VALUE_NAME);
        if (isSet(curValueName)) {
            changeContextValue(request, Constants.CURRENT_VALUE_NAME, curValueName);
        }

        // value buffering using DynamicUI-framework
        if (!uiContext.getOnlineEvaluate()){
            IPCUIModel ui = (IPCUIModel) uiContext.getProperty(IPCDynamicUIConstant.SESSION_DYNAMIC_UI);
    
            // update the IPCUIModel with the information from the request
            try {
                DynamicUIActionHelper.parseRequest(requestParser, ui);
            }
            catch (CommunicationException e) {
                // Catch communication exceptions and log them
                // log error
                log.error(LogUtil.APPS_COMMON_INFRASTRUCTURE, "exception.communication", e);
                // store exception in context for the error pages
                request.setAttribute(ContextConst.EXCEPTION, e);
                return mapping.findForward(Constants.FORWARD_BACKEND_ERROR);
            }
        }        

		customerExit(mapping, form, request, response, userSessionData, requestParser, mbom, multipleInvocation, browserBack);

		return mapping.findForward(ActionForwardConstants.SUCCESS);
	}

}
