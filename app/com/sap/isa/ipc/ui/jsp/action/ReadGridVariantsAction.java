/************************************************************************

	Copyright (c) 2004 by SAP AG

	All rights to both implementation and design are reserved.

	Use and copying of this software and preparation of derivative works
	based upon this software are not permitted.

	Distribution of this software is restricted. SAP does not make any
	warranty about the software, its performance or its conformity to any
	specification.

**************************************************************************/

package com.sap.isa.ipc.ui.jsp.action;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.sap.isa.businessobject.ipc.IPCBOManager;
import com.sap.isa.core.UserSessionData;
import com.sap.isa.core.businessobject.management.MetaBusinessObjectManager;
import com.sap.isa.core.util.RequestParser;
import com.sap.isa.ipc.ui.jsp.container.GridLayout;
import com.sap.isa.ipc.ui.jsp.container.GridVariant;
import com.sap.isa.ipc.ui.jsp.uiclass.UIContext;
import com.sap.spc.remote.client.object.IPCClient;
import com.sap.spc.remote.client.object.IPCDocument;
import com.sap.spc.remote.client.object.IPCDocumentProperties;
import com.sap.spc.remote.client.object.IPCException;
import com.sap.spc.remote.client.object.IPCItem;
import com.sap.spc.remote.client.object.IPCItemProperties;
import com.sap.spc.remote.client.object.Characteristic;
import com.sap.spc.remote.client.object.CharacteristicGroup;
import com.sap.spc.remote.client.object.imp.DefaultIPCDocument;
import com.sap.spc.remote.client.object.imp.DimensionalValue;
import com.sap.spc.remote.client.object.imp.rfc.RFCIPCItemReference;


/************************************************************************

	Grid Process:
	
	Information of all valid variants of a grid product is done in 2 steps:
	
	Step 1:
	-------
	All valid configuration combinations are read from the variant table.
	Example:
			====================================================
			| SIZE | LENGTH | COLOR | SAP_RESTRICTION | SPREAD |
			====================================================
			|  30  |   30   |  red  |     000101      |   10   |
			|  30  |   32   |  red  |     000101      |   20   |
			|  30  |   32   |  blue |     000101      |   20   |
			|  32  |   32   |  blue |     000101      |   20   |
			|  34  |   34   |  black|     000101      |   30   |
			|  30  |   32   |  red  |   SAP_DEFAULT   |   30   |
			|  32  |   32   |  red  |   SAP_DEFAULT   |   45   |
			|  34  |   32   |  red  |   SAP_DEFAULT   |   45   |
			====================================================
	The first up to 3 coloumns represent the dimensions of a grid product.
	The Restriction coloumn is to distinguish between several combinations
	that are only valid for one certain sales area (R/3 SalesOrg(0001) and
	DistributionChannel (01)). The Spread coloumn represents a percentage
	value for one combination. The sum of all values for all combination
	within a certain sales area must be 100%. This reflects a distribute
	profile for the quantities to easily distribute a main quantity among
	all variands.
	The result is stored in 2 String[]:
	
	{[SIZE.5][LENGTH][COLOR][SPREAD]}
	{[30][30][30][32][34][30][32][32][32][34][red][red][blue][blue][black][10][20][20][20][30]}
	
	The number added to the first Array represents the amount of valid
	combinations.
	
	Step 2:
	-------
	All variants with a combination of the result of Step 1 are read from the
	backend.
	The result is returned as several String[] and String[][]. This
	information is used to create GridVariant Objetcs. Each of these objects
	represent one variant.
	
	Example:
	
			======================
			|     Variant_1      |
			======================
			| String variantId   |
			| String parentId    |
			| String spreadValue |
			| String qty         |
			| String[] cNames    |
			| HashMap csticsMap  |
			| String indicator   |
			| String price       |
			======================
	
	With this information a GridLayout is created that provides all necessary
methods for the JSP Layout.

**************************************************************************/

public class ReadGridVariantsAction extends IPCBaseAction implements ActionForwardConstants{

	// constants for different display mode
	// pure grid, grid with variant prices, grid including stock indicator or both
	private final String UI_MODE_USER = "uimode";
	private final String UI_MODE = "uiMode";
	private final String UI_MODE_GRID = "G";
	private final String UI_MODE_PRICE = "P";
	private final String UI_MODE_STOCK = "S";
	private final String UI_MODE_PRICE_STOCK = "C";

	// constants for server command parameters
	private final String CSTICS_NAMES = "csticsNames";
	private final String CSTICS_VALUES = "csticsValues";
	private final String groupId = "DIMENSION";

	// constants for variants attributes
	private final String VARIANT_IDS = "variantId";
	private final String PARENT_IDS = "parentId";
	private final String SPREAD_VALUES = "spreadValue";
	//private final String CSTIC_NAMES = "csticName";
	//private final String CSTIC_VALUES = "csticValue";
	private final String INDICATOR_VARIANT_IDS = "varIds";
	private final String INDICATORS = "indicators";
	private final String DEFAULT_INDICATOR = "A";
	
	//constants for backend connected to ISA( ERP or CRM)
	private final String CRM_BACKEND = "CRM";
	//private final String ERP_BACKEND = "V";
	private final String CRM_SIZE 	 = "AFS_SIZE";
	private final String ERP_SIZE	 = "J_3ASIZE";

	// forwards
	// since a different navigation/layout is provide for ISA, we need to distinguish
	// between ISA an all other calling applications
	//private final String FORWARD_ISA = "isa";
	private final String FORWARD_COMMON = "common";


	/**
	* **************************************************************************************
	* Start of Action
	* **************************************************************************************
	*/

	public ActionForward ecomPerform(ActionMapping mapping, 
		ActionForm form,
		HttpServletRequest request,
		HttpServletResponse response,
		UserSessionData userSessionData,
		RequestParser requestParser,
		MetaBusinessObjectManager mbom,
		boolean multipleInvocation,
		boolean browserBack)
			throws IOException, ServletException {

		ArrayList gridVariants = new ArrayList();

		UIContext uiContext = getUIContext(request);
		checkNotNull(uiContext, "UiContext Object not initialized");

		IPCItem ipcItem = uiContext.getCurrentConfiguration().getRootInstance().getIpcItem();
		checkNotNull(ipcItem, "No IPC Item found");

		IPCClient ipcClient = getIPCClient(ipcItem, userSessionData);
		checkNotNull(ipcClient, "No IPC Client found");

		String documentId = uiContext.getCurrentConfiguration().getRootInstance().getIpcDocument().getId();
		setLogInfo("DocumentId:" + documentId);

		String itemId = ipcItem.getItemId();
		setLogInfo("ItemId:" + itemId);

		String productId = ipcItem.getProductId();
		setLogInfo("ProductId:" + productId);

		//String caller = uiContext.getPropertyAsString(RequestParameterConstants.CALLER);

		boolean spread = false;

		//get user data
		HttpSession session = request.getSession();
		UserSessionData userData = UserSessionData.getUserSessionData(session);

		//get values from the request
		//if the grid including stock indicator is requested, the calling application
		//adds to String[] to the call as a kind of variant <-> indicator pair
		//indicator b -> yellow; c -> red; e -> no information
		//all variants that are not included a -> green is defaulted
		String[] indVariantIds = getParameters(request, INDICATOR_VARIANT_IDS);
		String[] indicators = getParameters(request,INDICATORS);

		String uiMode = getParameter(request, UI_MODE);

		//set sales grid values for variant table call, see Step 1 description above
		String[] contextNames = uiContext.getCurrentConfiguration().getContextNames();
		String[] contextValues = uiContext.getCurrentConfiguration().getContextValues();

		String salesOrgR3 = "";
		String distrChannel = "";

		if(contextNames != null && contextNames.length > 0){
			for(int i=0; i<contextNames.length; i++){
				//if non-ERP applications calling
				if(contextNames[i].equals("DOCUMENT_SALES_ORG_R3")) salesOrgR3 = contextValues[i];
				if(contextNames[i].equals("DOCUMENT_DISTRIBUTION_CHANNEL")) distrChannel = contextValues[i];
				
				//if ERP application calling
				if(contextNames[i].equals("VBAP-VKORG")) salesOrgR3 = contextValues[i];
				if(contextNames[i].equals("VBAP-VTWEG")) distrChannel = contextValues[i];
			}
		}

		String filterValue = salesOrgR3 + distrChannel;
		setLogInfo("Filter value for sales grif in variant table:" + filterValue);		

		// get parameters from the user session data, if the request has not set them yet
		// ISA for example puts all information in the UserSessionData instead of the request
		if(uiMode == null || uiMode.length() == 0) uiMode = (String)userData.getAttribute(UI_MODE_USER);
		if(indVariantIds == null || indVariantIds.length == 0) indVariantIds = (String[])userData.getAttribute(INDICATOR_VARIANT_IDS);
		if(indicators == null || indicators.length == 0) indicators = (String[])userData.getAttribute(INDICATORS);

		// initialize server command return values
		HashMap varTableMap = new HashMap();
		HashMap variantsMap = new HashMap();
		//List paramsList = new ArrayList();
		
		//Note 1388787   ----- Start ------
         List csticGroupList = uiContext.getCurrentConfiguration().getRootInstance().getCharacteristicGroups();
         int count = 0;
		 CharacteristicGroup csticGroup;
		 String[] cstics = null;
		 
         if (csticGroupList != null){
         	count = csticGroupList.size();
         	setLogInfo ( "Number of groups in the csticGroupList: " + count );
         	for (int i=0; i<count; i++){
         		csticGroup = (CharacteristicGroup)csticGroupList.get(i);
         		
         		if (csticGroup != null && ( csticGroup.getName().equals(groupId) || csticGroup.getLanguageDependentName().equals(groupId) )){
					setLogInfo ("Characteristic Group Name: " + csticGroup.getName() );
					List csticsList = csticGroup.getCharacteristics();
					cstics = new String[csticsList.size()];
					if(csticsList != null && csticsList.size()>0){
						for(int j=0; j<csticsList.size(); j++){
							setLogInfo ("Characteristic Name: " + ((Characteristic)csticsList.get(j)).getName() );
							cstics[j] = ((Characteristic)csticsList.get(j)).getName();
						}
					}					
         		}else{
         			setLogInfo("Characateristic Group is NULL !!!");       		
         		}
         	}        	
         }else{
         	setLogInfo("Characteristic Group list csticGroupList is NULL !!!");
         }
		//Note 1388787  ----- End ------		
		//read all valid combinations of product variant characteristics, see Step 1		
		varTableMap = ipcClient.getVarTable(documentId, itemId, cstics, filterValue);

		String[] csticsNames = (String[])varTableMap.get(CSTICS_NAMES);
		String[] csticsValues = (String[])varTableMap.get(CSTICS_VALUES);

		setLogInfo("End GetVarTable \n" +
					"Returned Values from the GetVarTable call:" +
							"Characteristic names:" + Integer.toString(csticsNames.length) +
							"Characteristic values:" + Integer.toString(csticsNames.length));

		//initialize parameters that will be filled by the server command
		String[] variantIds = null;
		String[] parentIds = null;
		String[] spreadValues = null;
		String[][] cNames = null;
		String[][] cValues = null;

		//only call server command for variants if valid entries in the variant table were found
		//see Step 2 in the initial description
		if(csticsValues != null){
			setLogInfo("Start GetAllValidGridVariants \n"+
						"Characteristic names[0]:" + csticsNames[0]);

			String logsys = uiContext.getCurrentConfiguration().getKnowledgeBase().getLogsys();
			
			
			// read all valid product variant, see Step 2 above
			variantsMap = ipcClient.getAllValidGridVariants(productId, logsys, csticsNames, csticsValues);

			// get values from the server
			variantIds = (String[])variantsMap.get(VARIANT_IDS);
			parentIds = (String[])variantsMap.get(PARENT_IDS);
			spreadValues = (String[])variantsMap.get(SPREAD_VALUES);
			cNames = (String[][])variantsMap.get(CSTICS_NAMES);
			cValues = (String[][])variantsMap.get(CSTICS_VALUES);

			setLogInfo("End GetAllValidGridVariants \n"+
									"VariantIds:" + Integer.toString(variantIds == null?0:variantIds.length)+
									"ParentIds:" + Integer.toString(parentIds == null?0:parentIds.length)+
									"SpreadValues:" + Integer.toString(spreadValues == null?0:spreadValues.length));
		}
		else{
			setLogInfo("GetAllValidGridVariants was not called, no variants available");
		}


		// if UI_MODE is null, use grid as default
		if(uiMode == null) uiMode = UI_MODE_GRID;
		setLogInfo("UiMode:" + uiMode);

		//only move on if valid variants were found
		if(variantIds != null){
			// start price routine to get the prices for the variants, if requested
			String[] prices = new String[variantIds.length];
			if(uiMode.equals(UI_MODE_PRICE) || uiMode.equals(UI_MODE_PRICE_STOCK)){
					prices = getPrices(uiContext, ipcClient, documentId, ipcItem, variantIds, cValues);
			}

			// calculate spread
			// provides a customer exit, if an additional calculation routine is
			// needed. The calculated quantities will directly be filled to the
			// qty of each variant
			String[] spreadQtys = new String[variantIds.length];
			if((ipcItem.getChildren() != null || ipcItem.getChildren().length != 0)){
					spread = true;
					spreadQtys = calcSpreadQuantity(ipcItem.getSalesQuantity().getValueAsString(), spreadValues);
				}

			// create grid variants - including ids, configuration, quantities, indicator, prices
			gridVariants = createGridVariants(variantIds,
												parentIds,
												spreadQtys,
												spreadValues,
												cNames,
												cValues,
												indVariantIds,
												indicators,
												prices,
												ipcItem,
												uiMode);
		}

		checkNotNull(variantIds, "No valid variants found");

		// create matrixLayout object
		GridLayout gridLayout = new GridLayout(ipcItem, uiContext, gridVariants, uiMode, spread);


		customerExit(mapping, form, request, response, userSessionData, requestParser, mbom, multipleInvocation, browserBack);

		// cache object in session
		session.setAttribute("gridLayout",gridLayout);

		if(false){
			return mapping.findForward("error");
		}
		else{
			return mapping.findForward(FORWARD_COMMON);
		}
	}

	/*
	 * **************************************************************************************
	 * Methods for creating and filling GridVariant Objects
	 * **************************************************************************************
	 */

	/**
	 * Method to get the ipcClient from ipcBoManager
	 * 
	 * @param ipcItem
	 * @param userSessionData
	 * @return
	 */
	private IPCClient getIPCClient(IPCItem ipcItem, UserSessionData userSessionData) {
		
		IPCClient ipcClient = null;
		
		//Get the ipcBoManager
		IPCBOManager ipcBoManager =
			  (IPCBOManager) userSessionData.getBOM(IPCBOManager.NAME);
		if (ipcBoManager == null) {
		   log.error("No IPC BOM in session data! - ReadGridVariantsAction");
		   return null;
		}
		//Get the ipcClient
		if (ipcBoManager.getIPCClient() == null){ //might be in ISA case
		
			RFCIPCItemReference itemReference = (RFCIPCItemReference)ipcItem.getItemReference();
			if (itemReference == null) {
				  IPCException ipcException =
					  new IPCException("IPCItemReference == null in ReadGridVariantsAction!");
				  log.error(this, ipcException);
				  return null;
			}
			
			String connectionKey = itemReference.getConnectionKey();
			//the caller of the configuration requests the client to use the same connection
			ipcClient = ipcBoManager.createIPCClientUsingConnection(connectionKey);
			
		} else{ 
			ipcClient = ipcBoManager.getIPCClient();
		}
		
		return ipcClient;
	}

	/**
	 * main method
	 * @param variantIds
	 * @param parentIds
	 * @param spreadQtys
	 * @param spreadValues
	 * @param cNames
	 * @param cValues
	 * @param indVarIds
	 * @param indicators
	 * @param prices
	 * @param ipcItem
	 * @param uiMode
	 * @return
	 */
	private ArrayList createGridVariants(String[] variantIds,
											 String[] parentIds,
											 String[] spreadQtys,
											 String[] spreadValues,
											 String[][] cNames,
											 String[][] cValues,
											 String[] indVarIds,
											 String[] indicators,
											 String[] prices,
											 IPCItem ipcItem,
											 String uiMode){

		HashMap csticsMap = new HashMap();
		ArrayList gridVariants = new ArrayList();


		for(int i=0; i<variantIds.length; i++){
			for(int j=0; j<cNames[i].length;j++){
				csticsMap.put(cNames[i][j], cValues[i][j]);
			}

			GridVariant gridVariant = new GridVariant(variantIds[i], parentIds[i], spreadValues[i], cNames[i], csticsMap);
			gridVariant.setQuantity(fillQuantity(ipcItem, variantIds[i], spreadQtys[i]));

			if(uiMode.equals(UI_MODE_STOCK) || uiMode.equals(UI_MODE_PRICE_STOCK)){
				gridVariant.setStockIndicator(setIndicator(variantIds[i], indVarIds, indicators));
			}
			if(uiMode.equals(UI_MODE_PRICE) || uiMode.equals(UI_MODE_PRICE_STOCK)){
				gridVariant.setPrice(prices[i]);
			}

			gridVariants.add(gridVariant);
		}
		return gridVariants;
	}
	// main method ends

	/**
	 * if a variant was already added as a subitem before, we read the current quantity
	 * of the subitem and fill the variant object accordingly
	 */
	private String fillQuantity(IPCItem item, String variantId, String spreadValue){
		String qty = "";
		if(item.getChildren() != null && item.getChildren().length != 0){
			for(int i=0; i<item.getChildren().length; i++){
				IPCItem current = item.getChildren()[i];
				if(current.getProductId() != null &&
				   current.getProductId().equals(variantId)){
					qty = current.getSalesQuantity().getValueAsString();
				}
			}
		}
		else if(item.getSalesQuantity().getValueAsString() != null &&
				!item.getSalesQuantity().getValueAsString().equals("")&&
				!item.getSalesQuantity().getValueAsString().equals("1") &&
				spreadValue != null){
				qty = spreadValue;
		}
		return qty;
	}

	/**
	 *  retrieves information from the customer exit
	 */
	private String[] calcSpreadQuantity(String mainQty, String[] spreadValues){
		String[] spreadQtys = new String[spreadValues.length];
		spreadQtys = customerExitForSpreadCalc(mainQty, spreadValues);

		return spreadQtys;
	}

	/**
	 * sets the indicator for a variant accordinlgy to the information from the request
	 * or UserSessionData, for each variant that no indicator is set, a is defaulted
	 */
	private String setIndicator(String variandId, String[] indVarIds, String[] indicators){
		String indVal = DEFAULT_INDICATOR;
		if(indVarIds != null){
			for(int i=0; i<indVarIds.length; i++){
				if(indVarIds[i].equals(variandId)){
					if(indicators[i] != null && indicators[i].length()>0){
						indVal = indicators[i];
					}
				}
			}
		}
		return indVal;
	}
	/**
	 *  to display prices for variants a temporarily document is created. To that document
	 * the main item and all possible variants are added with the quantity of 1. Then the
	 * document is priced and the prices are stored in a String[]
	 */
	private String[] getPrices(UIContext uiContext, IPCClient client, String documentId, IPCItem item, String[] variantIds, String[][] cValues){
		String[] prices = new String[variantIds.length];

		//Required to set GRIDVALUE to different keys for CRM & ERP Backends.
		String afsString = ERP_SIZE;
		DefaultIPCDocument document = (DefaultIPCDocument)client.getIPCDocument(documentId); 
		if(document.getApplication() != null &&
			document.getApplication().equals(CRM_BACKEND)){
				afsString = CRM_SIZE;
		}
		
		// necessary since SalesOrg and DC is not copied (correctly)
		IPCDocumentProperties ipcDocProps = client.getIPCDocument(documentId).getPropertiesFromServer();
		
		IPCDocument tempDoc = client.createIPCDocument(ipcDocProps);
		IPCItemProperties itemProperties = (item.getPropertiesFromServer(new String[] {item.getItemId()}))[0];
		itemProperties.setProductId(item.getProductId());
		
		IPCItem tempItem = tempDoc.newItem(itemProperties);
		tempItem.setBaseQuantity(new DimensionalValue("1", itemProperties.getBaseQuantity().getUnit()));
		setLogInfo("uiContext.getStatisticalPricing():"+uiContext.getStatisticalPricing());
		if(uiContext.getStatisticalPricing()){
			IPCItemProperties[] tempProp = new IPCItemProperties[variantIds.length];
			itemProperties.setStatistical(false);
			for(int i=0; i<variantIds.length; i++){
				tempProp[i] = factory.newIPCItemProperties(itemProperties);
				tempProp[i].setProductId(variantIds[i]);
				tempProp[i].setConfig(null);
				tempProp[i].setBaseQuantity(new DimensionalValue("1", itemProperties.getBaseQuantity().getUnit()));
				tempProp[i].setSalesQuantity(new DimensionalValue("1", itemProperties.getSalesQuantity().getUnit()));
				tempProp[i].setPricingRelevant(new Boolean(true));
				tempProp[i].setHighLevelItemId(tempItem.getItemId());
				
				//Add the field AFS_SIZE with value to itemAttributes.
				//This is required to get the prices for product variants.
				String gridValue = "";
				for (int x=0;x<cValues[i].length;x++){
					if (cValues[i][x] != null)
						gridValue+=cValues[i][x];
				}
				setLogInfo("gridValue:"+gridValue);
				Map tempItemAttributes = itemProperties.getItemAttributes();
				tempItemAttributes.put(afsString, gridValue);
				tempProp[i].setItemAttributes(tempItemAttributes);
			}
			factory.newIPCItems(tempDoc, tempProp);

			IPCItem[] tempItemChilds = tempItem.getChildren();
			for(int i=0; i<variantIds.length; i++){
				for(int j=0; j<tempItemChilds.length; j++){
					if(variantIds[i].equals(tempItemChilds[j].getProductId())){
						prices[i] = tempItemChilds[j].getNetValue().getValueAsString();
						setLogInfo(variantIds[i] + ":sku:" + prices[i]);
					}
				}
			}
		}
		else{
			for(int i=0; i<variantIds.length; i++){
				prices[i] = tempItem.getNetValue().getValueAsString();
				setLogInfo(variantIds[i] + ":main:" + prices[i]);
			}
		}
		client.getIPCSession().removeDocument(tempDoc);
		return prices;
	}

	/**
	* **************************************************************************************
	* Customer Exits
	* **************************************************************************************
	*/

	public String[] customerExitForSpreadCalc(String mainQty, String[] spreadValues){
		if (mainQty == null); //avoid compiler warning of unused argument
		String[] spreadQtys = new String[spreadValues.length];
		return spreadQtys;
	}


	/**
	* **************************************************************************************
	* Service methods
	* **************************************************************************************
	*/

	private String[] getParameters(HttpServletRequest request, String key) {
		SimpleRequestWrapper simpleRequestWrapper = new SimpleRequestWrapper(request);
		String[] values = simpleRequestWrapper.getParameterValues(key);
		return values;
	}

	private String getParameter(HttpServletRequest request, String key) {
			SimpleRequestWrapper simpleRequestWrapper = new SimpleRequestWrapper(request);
			String value = simpleRequestWrapper.getParameterValue(key);
			return value;
	}

	private void setLogInfo(String debugInfo){
		if (log.isDebugEnabled()) {
						log.debug(debugInfo);
		}
	}

	private void checkNotNull (Object o, String errorInfo){
		if(o == null){
			log.error(errorInfo);
		}
	}
}
