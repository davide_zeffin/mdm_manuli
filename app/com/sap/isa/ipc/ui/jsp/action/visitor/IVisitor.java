package com.sap.isa.ipc.ui.jsp.action.visitor;

import com.sap.isa.ipc.ui.jsp.beans.CharacteristicUIBean;
import com.sap.isa.ipc.ui.jsp.beans.ConfigUIBean;
import com.sap.isa.ipc.ui.jsp.beans.GroupUIBean;
import com.sap.isa.ipc.ui.jsp.beans.InstanceUIBean;
import com.sap.isa.ipc.ui.jsp.beans.MimeUIBean;
import com.sap.isa.ipc.ui.jsp.beans.StatusImage;
import com.sap.isa.ipc.ui.jsp.beans.ValueUIBean;

/**
 * @author D048393
 *
 * This Interface is part of the Visitor Design Pattern
 */
public interface IVisitor {
    
    /**
     * Will analyse a ConfigUIBean
     * @param bean
     */
    public void visit (ConfigUIBean bean);
    
    /**
     * Will analyse a InstanceUIBean.
     * This method processes all logic of the instance level.
     * It contains the following steps:
     * 1. Obtain the IPCUIComponent for the current configModelKey
     * 2. Analyze InstanceUIBean
     * 3. Update the frw-objects
     * 4. Process children: sub-instance and of current instance groups
     * 5. Handle deleted groups in the cfg-model
     * 6. Determine status of IPCUIComponents
     * @param bean
     */
    public void visit (InstanceUIBean bean);
    
    /**
     * Will analyse a GroupUIBean
     * This method processes all logic of the group level.
     * It contains the following steps:
     * 1. Obtain the IPCPropertyGroups for the current configModelKey
     * 2. Analyze GroupUIBean
     * 3. Update the frw-objects
     * 4. Process children: characteristics
     * 5. Handling of search-set feature (expand found group if necessary)
     * 6. Determine the status of the IPCPropertyGroups
     * 7. Determine the group visibility
     * @param bean
     */
    public void visit (GroupUIBean bean);
    
    /**
     * Will analyse a CharacteristicUIBean.
     * This method processes all logic of the cstic level.
     * It contains the following steps:
     * 1. Obtain the IPCProperties for the current configModelKey
     * 2. Analyze CharacteristicUIBean
     * 3. Update the frw-objects
     * 4. Process children: values
     * 5. Handling of erroneous values: highlight the properties if necessary
     * @param bean CharacteristicUIBean
     */
    public void visit (CharacteristicUIBean bean);
    
	/**
	 * Will analyse a MimeUIBean
	 * @param bean
	 */
	public void visit (MimeUIBean bean);
    
	/**
	 * Will analyse a StatusImage
	 * @param bean
	 */
	public void visit (StatusImage bean);
	
    /**
     * Will analyse a ValueUIBean
     * This method processes all logic of the value level.
     * It contains the following steps:
     * 1. Analyze ValueUIBean
     * 2. Obtain the IPCProperties the current value belongs to.
     * 3. Get the IPCAllowedValue object.
     * 4. Update the IPCAllowedValue-object
     * 5. Set value as assigned if necessary.
     * 6. Set mimes.
     * 7. Set the status for message cstic if the current characteristic is a message cstic.
     * @param bean ValueUIBean
     */
    public void visit (ValueUIBean bean);

    /**
      * @return
      */
     public CharacteristicUIBean getSearchSetCstic();

     /**
      * @return
      */
     public GroupUIBean getSearchSetGroup();

     /**
      * @param bean
      */
     public void setSearchSetCstic(CharacteristicUIBean bean);

     /**
      * @param bean
      */
     public void setSearchSetGroup(GroupUIBean bean);

    /**
     * @return
     */
    public String getCsticStatusChange();

    /**
     * @param bean
     */
    public void setCsticStatusChange(String string);


}