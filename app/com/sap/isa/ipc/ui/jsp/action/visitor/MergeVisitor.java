
/*****************************************************************************
    Class:        MergeVisitor
    Copyright (c) 2008, SAP AG, Germany, All rights reserved.
    Author:       SAP AG
    Created:      ${date}
    Version:      1.0
*****************************************************************************/

package com.sap.isa.ipc.ui.jsp.action.visitor;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.servlet.jsp.PageContext;
import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.core.util.WebUtil;
import com.sap.isa.ipc.ui.jsp.action.RequestParameterConstants;
import com.sap.isa.ipc.ui.jsp.beans.CharacteristicUIBean;
import com.sap.isa.ipc.ui.jsp.beans.ConfigUIBean;
import com.sap.isa.ipc.ui.jsp.beans.GroupUIBean;
import com.sap.isa.ipc.ui.jsp.beans.InstanceUIBean;
import com.sap.isa.ipc.ui.jsp.beans.MimeUIBean;
import com.sap.isa.ipc.ui.jsp.beans.StatusImage;
import com.sap.isa.ipc.ui.jsp.beans.ValueUIBean;
import com.sap.isa.ipc.ui.jsp.dynamicui.IPCAllowedValue;
import com.sap.isa.ipc.ui.jsp.dynamicui.IPCConfigModelKey;
import com.sap.isa.ipc.ui.jsp.dynamicui.IPCDynamicUIConstant;
import com.sap.isa.ipc.ui.jsp.dynamicui.IPCProperty;
import com.sap.isa.ipc.ui.jsp.dynamicui.IPCPropertyGroup;
import com.sap.isa.ipc.ui.jsp.dynamicui.IPCUIComponent;
import com.sap.isa.ipc.ui.jsp.dynamicui.IPCUIElementKey;
import com.sap.isa.ipc.ui.jsp.dynamicui.IPCUIModel;
import com.sap.isa.ipc.ui.jsp.dynamicui.IPCUIPage;
import com.sap.isa.ipc.ui.jsp.dynamicui.renderer.IPCUITypeDetermination;
import com.sap.isa.ipc.ui.jsp.uiclass.UIArea;
import com.sap.isa.ipc.ui.jsp.uiclass.UIContext;
import com.sap.isa.ipc.ui.jsp.uiclass.UIElement;
import com.sap.isa.ipc.ui.jsp.util.MasterdataUtil;
import com.sap.isa.maintenanceobject.backend.boi.IPCImageLinkData;
import com.sap.isa.maintenanceobject.backend.boi.UIElementGroupData;
import com.sap.isa.maintenanceobject.backend.boi.UIElementStatus;
import com.sap.isa.maintenanceobject.businessobject.UIElementGroup;
import com.sap.isa.maintenanceobject.ui.UITypeRendererData;

//@TODO: refactor visit methods concerning length

/**
 * This Visitor is able to handle the merge of the config model with DynamicUI-framework model (also
 * referred to as "frw-model").
 * The Visitor Design Pattern is applied, so that this visitor will iterate over the IPC-UIBeans and
 * update the DynamicUI framework objects according to the UIBeans.
 */
public class MergeVisitor implements IVisitor, IPCDynamicUIConstant {

    /**
     * Key of the parent in the ConfigModel.
     */
    private IPCConfigModelKey parentConfigModelKey;

    /**
     * Key of the parent UIModel element.
     * Can be used to obtain the corresponding Object from the UI model.
     */
    private IPCUIElementKey parentUIModelKey;

    /**
     * Reference to the DynamicUI object that should get updated by this visitor
     */
    private IPCUIModel uiModel;

    /**
     * Reference to the UIConext: For exmaple XCM settings can be obtained from there. 
     */
    private UIContext uiContext;

    private PageContext pageContext;
    
    protected static IsaLocation log = IsaLocation.getInstance("com.sap.isa.ipc.ui.jsp.action.visitor.MergeVisitor");
    
    /**
     * The characteristic that has been found via searchSet feature
     */
    private CharacteristicUIBean searchSetCstic = null;
    
    /**
     * The group of the searchSet characteristic.
     */
    private GroupUIBean searchSetGroup = null;
    
    /**
     * Flag is set to true in visit(CharacteristicUIBean) if the cstic has been found in this group
     */
    private boolean searchSetCsticFound = false;
    
	/**
	 * Flag is set to true in visit(InstanceUIBean) if we have a uiModel with more than one pages
	 * but the parameter showGroups is set to false
	 */
	private boolean groupsOffWithSeveralPages = false;
    
    /**
     * The IPCProperty group the searchSet cstic belongs to.
     */
    private IPCPropertyGroup searchSetUIGroup;
    
    /**
     * Flag whether the focus for the searchSet cstic has been set.
     * The focus should be set only at the first occurence of the searchSet focus.
     */
    private boolean searchSetFocusSet = false;
    
    /**
     * The characteristic for which the expand/collapse status should be changed.
     * This is triggerd by "Display Options" Link.
     * The string contains instance-id, cstic name and tech-key., e.g.
     * "1.SIZE.119B7D4455D0000024000A140A8C0000".
     */
    private String csticStatusChange = null;

    /**
     * Constructor Both parameters are mandatory. If one is null, an IlleagalArgumentException
     * will be thrown
     * @param ui The UI object that the visitor should synachronize via analysing the UIBeans
     * @param uc reference to the ui context
     */
    public MergeVisitor(
        IPCUIModel uiModel,
        UIContext uiContext,
        PageContext pageContext) {
        
        //protected static IsaLocation log = IsaLocation.getInstance(MergeVisitor.class.getName());
        
        // both params are mandatory So if one is null throw an IllegalArgumentException
        
        if ((uiModel == null) 
            || (uiContext == null)) {
            throw new IllegalArgumentException("Parameters uiModel and uiContext are not allowed to be null");
        } else if (pageContext == null){
            log.debug("Page Context is null");
        }
        this.uiModel = uiModel;
        this.uiContext = uiContext;
        this.pageContext = pageContext;

    }

    /* (non-Javadoc)
     * @see com.sap.isa.ipc.ui.jsp.action.visitor.IVisitor#visit(com.sap.isa.ipc.ui.jsp.beans.ConfigUIBean)
     */
    public void visit(ConfigUIBean bean) {

        // settings valid for the whole configModel, may get overwritten by uiModel
        displayPart(true, UITypeRendererData.RESET_VALUE);
        displayPart(false, UITypeRendererData.ADD_DOC_LINKS);
        displayPart(false, UITypeRendererData.THUMBNAIL);
        
        IPCUIPage currentPage = uiModel.determineActivePage(searchSetGroup, searchSetCstic);

        // set keys
        parentUIModelKey = new IPCUIElementKey();
        parentUIModelKey.setPage(currentPage.getName());
        parentConfigModelKey = new IPCConfigModelKey();

        // Objects in the config-model can (dis)appear dynamically throughout
        // the session. 
        // Hide all elements to get rid of elements that are not existing in 
        // the config-model for the current round-trip.
        currentPage.hideAllElements();
        
        // Clear all messages (they will be set during the merge).
        // Do this in DynamicUINavigationAction instead
        //uiModel.clearMessages();

        // visit root instance
        ((IVisitableObject) bean.getRootInstance()).accept(this);

        // For newly expanded components we have to trigger 
        // the default expanding (= expanding of the first group)
        // and we save the state of expanded components afterwards.
        currentPage.triggerDefaultExpandingOnStateChange();

    }



    /* (non-Javadoc)
     * @see com.sap.isa.ipc.ui.jsp.action.visitor.IVisitor#visit(com.sap.isa.ipc.ui.jsp.beans.InstanceUIBean)
     */
    public void visit(InstanceUIBean instanceBean) {

        // construct keys
        IPCConfigModelKey currentInstanceKey = new IPCConfigModelKey(parentConfigModelKey);
        currentInstanceKey.setInstanceName(instanceBean.getId());
        
        IPCUIElementKey currentUIModelKey = new IPCUIElementKey(parentUIModelKey);
        currentUIModelKey.setComponent(instanceBean.getId());

        // get Parent Element in UI
        UIElementGroupData uiPage = (UIElementGroupData) uiModel.getUIElement(parentUIModelKey);

        // obtain components (or if non exists create new one)
        IPCUIComponent comp = null;
        List uiComponentList = uiModel.getIPCUIComponents(currentInstanceKey);
        if (uiComponentList.size() == 0) {
            comp = new IPCUIComponent();
            comp.setName(currentUIModelKey.getComponent());
            comp.setExpanded(false);
            uiPage.addElement(comp);
            uiComponentList.add(comp);
        }

        // analyse instance bean
        // description (frw) corresponds to languageDependentName (cfg)
        String componentDescription = "";
        ArrayList ancestors = new ArrayList();
        instanceBean.getLanguageDependentAncestorsNames(ancestors);
        for (int i=0; i<ancestors.size(); i++){
            componentDescription = componentDescription + (String)ancestors.get(i) + COMPONENT_NAME_SEPPERATOR;           
        }
        componentDescription = componentDescription + instanceBean.getLanguageDependentName();
            
        // do we have to show groups? 
        boolean showGroups =
            uiContext.getPropertyAsBoolean(
                RequestParameterConstants.USE_GROUP_INFORMATION);
        
        // do we have several pages (more than the default page)   
        if (!showGroups && uiModel.getPageList().size() > 1) {		
			groupsOffWithSeveralPages = true;						
        }															

        // Expand/Collapse mode of the components (assignement blocks)
        boolean onlyOneComponentExpanded = uiContext.getPropertyAsBoolean(RequestParameterConstants.AUTOMATIC_COLLAPSE_COMPONENTS);
        
        IPCPropertyGroup group = null;
        if (!showGroups && !groupsOffWithSeveralPages) {   			
            // TODO: implement UIRenderer for uiType IPC_NO_RENDER_GROUP
            group = new IPCPropertyGroup();
//            group.setName(IPC_NO_RENDER_GROUP_NAME);
            group.setName(BASE_GROUP_NAME);
            group.setUiType(UITypes.IPC_NO_RENDER_GROUP);
            group.setRessourceKeyUsed(false);
            currentUIModelKey.setGroup(group.getName());
        }
        // an elemnt form the Config model may occure several times in the UI model, update all
        Iterator itr = uiComponentList.iterator();
        while (itr.hasNext()) {
            // set data to ui model components
            comp = (IPCUIComponent) itr.next();
            comp.setDescription(componentDescription);
            comp.setRessourceKeyUsed(false);
            //non-configurable bom items should not get a hyperlink nor be "toggleable"
            if (instanceBean.hasVisibleCharacteristics()) {
            comp.setToggleAllowed(true);
            } else {
            	comp.setToggleAllowed(false);
            }
            comp.setHidden(false);
            if (comp.isExpanded() == false){
                // If the component is currently collapsed we set this flag in order 
                // to display also components that are currently empty (e.g. a component that is collapsed).
                // Otherwise the framework would not display the element because from the frw-perspective it is "empty".
                comp.setDisplayEmptyGroups(true);
            }
            comp.setOnlyOneGroupExpanded(onlyOneComponentExpanded);
            if (!showGroups && !groupsOffWithSeveralPages){
           		comp.clear();
                comp.addElement(group);
            }

        }

        // Recursive call: process children -> child instances
        // sub-instances are children of current instance in cfg-model but 
        // in the ui-model they are on the same level (they are children of the page) 
        visitList(instanceBean.getChildren());

        // set keys before processing childs
        parentUIModelKey = currentUIModelKey;
        parentConfigModelKey = currentInstanceKey;  

        // process groups
        if (showGroups || groupsOffWithSeveralPages) {
            // visit all sub groups
            visitList(instanceBean.getGroups());
        } else {
            // visit characteristic beans and ignore groups beans
            visitList(instanceBean.getCharacteristics());
        }

        // Handle groups that have been deleted in the cfg-model.
        IPCUIComponent.handleDeletedGroups(uiModel, instanceBean, uiComponentList);

        // Determine the status of the IPCUIComponents.
        determineStatus(uiComponentList, instanceBean.getStatusImage());
    }

    /* (non-Javadoc)
     * @see com.sap.isa.ipc.ui.jsp.action.visitor.IVisitor#visit(com.sap.isa.ipc.ui.jsp.beans.GroupUIBean)
     */
    public void visit(GroupUIBean bean) {
        // construct keys
        IPCConfigModelKey currentConfigModelKey =
            new IPCConfigModelKey(parentConfigModelKey);
        IPCUIElementKey currentUIModelKey =
            new IPCUIElementKey(parentUIModelKey);
        currentConfigModelKey.setGroupName(bean.getName());
        currentUIModelKey.setGroup(bean.getName());

        // get Parent Element in UI
        UIElementGroupData uiParent =
            (UIElementGroupData) uiModel.getUIElement(parentUIModelKey);
        
    	if (uiContext.getPropertyAsBoolean(RequestParameterConstants.USE_GROUP_INFORMATION_MODIFIED) == true) {
            // after switching back to "Show Groups" (USE_GROUP_INFORMATION_MODIFIED = true), 
    		// the first group within the uimodel is still the Base Group (with all cstics)
            // This leads to the effect that this Base Group stays at position one and so
            // issues may occur with products that have ungrouped cstics in a Base group ("General"). 
    		// Therefore all elements (=groups) in the uiParent object are cleared before adding the new groups
    		uiParent.clear();
        	// now reset the USE_GROUP_INFORMATION_MODIFIED 
    	   	// (default expanding will of course be performed as the initial merge flag 
    	   	// is still set in the first roundtrip after switching back to "Show groups").
    	   	// Otherwise the default UI modelInfo would be
	        // created in the prefetch method even if it is not the first time
	        // and in the GetCharacteristicsAction the relevant instances would not contain
	        // the correct relevant groups if another group was expanded by the user (then no 
	        // characteristics for this newly expanded group would be rendered) 
	        uiContext.setProperty(RequestParameterConstants.USE_GROUP_INFORMATION_MODIFIED, false);
    	}

        // obtain components (or if non exists create new one)
        IPCPropertyGroup group;
        List list = uiModel.getIPCUIPropertyGroups(currentConfigModelKey);
        if (list.size() == 0) {
            group = new IPCPropertyGroup();
            group.setName(currentUIModelKey.getGroup());
            group.setExpanded(false);
            uiParent.addElement(group);
            list.add(group);
        }

        // analyse group bean
        // description (frw) corresponds to languageDependentName (cfg)
        String description = bean.getLanguageDependentName(pageContext);

        // Expand/Collapse mode of the components (assignement blocks)
        boolean onlyOneGroupExpanded = uiContext.getPropertyAsBoolean(RequestParameterConstants.AUTOMATIC_COLLAPSE_GROUPS);
        
        // an elemnt form the Config model may occure several times in the UI model, update all
        Iterator itr = list.iterator();
        while (itr.hasNext()) {
            // set data to ui model components
            group = (IPCPropertyGroup) itr.next();
            group.setDescription(description);
            group.setRessourceKeyUsed(false);
            group.setToggleAllowed(true);
            group.setHidden(false);
            // Set this flag in order to display also elements that are currently empty (e.g. a group that is collapsed).
            // Otherwise the framework would not display the element because from the frw-perspective it is "empty".
            group.setDisplayEmptyGroups(true);          
            group.setOnlyOneGroupExpanded(onlyOneGroupExpanded);
            
            // special case (see int. message 512912 2009): several pages with groups are defined in the UI Editor, 
			// but groups are turned off, then we keep the cstics on the pages as groups were turned on but 
			// the rendering of the group name itself will not be done (new UI Type with new JSP)
            if (groupsOffWithSeveralPages) {						
	            group.setUiType(UITypes.IPC_NO_RENDER_GROUP_NAME);  
	            group.setExpanded(true);
	            uiContext.setInitialMerge(false);
            }														
            else {
				group.setUiType(UITypes.PROPERTY_GROUP);
            }
        }

        // set keys before processing childs
        parentUIModelKey = currentUIModelKey;
        parentConfigModelKey = currentConfigModelKey;

        
        ArrayList cStics;
        cStics = bean.getCharacteristics();
        if (cStics.size() == 0) { 
            // set visibility depending on XCM settings
            
        } else { 
            //  process children - cstic
            visitList(bean.getCharacteristics());
        } //if (cStics.size() == 0)
        
        // search set:
        // if the searchSetCstic has been found in this group we have to expand it
        if (searchSetCsticFound){
            Iterator itr2 = list.iterator();
            while (itr2.hasNext()) {
                group = (IPCPropertyGroup) itr2.next();
                if (group.equals(searchSetUIGroup)){
                    group.setExpanded(true);
                }
            }
            // reset the flag & the searchSetUiGroup
            searchSetCsticFound = false;
            searchSetUIGroup = null;
        }
        
        // Determine the status of the IPCPropertyGroups.
        determineStatus(list, bean.getStatusImage());
        
        // Determine the group visibility
        IPCPropertyGroup.determineGroupVisibility(list, uiModel, uiContext);

    }

    /**
     * Determine the status of the UIElementGroups.
     * The determination  depends on the fact whether a UIModel exists or not.
     * Reason: It might be possible that a required property has been
     * moved from one page to another or from one group to antoher . In this case the former 
     * IPCUIComponent/IPCPropertyGroup should not be  marked "incomplete" if there is not 
     * another required property inside it.
     *
     * The decision whether the status information from cfg-engine can be used or not follows this logic:
     * The status might differ if the user has changed the objects (e.g. moving a required
     * property to another page or group -> the cfg-engine does not know about this so the status
     * from cfg-engine might be incorrect).
     * The following three cases have to be taken into account:
     *
     * 1. If the user has changed and saved the uiModel it is persistent on the DB. 
     * That means the uiModel has a GUID and the changed IPCUIComponent/IPCPropertyGroup has elements.
     * In this case we use the frw-objects to determine the status.
     *
     * 2. If only a UIModel has been created in PME but no one has changed the objects so far,
     * then no elements exists below IPCUIComponent/IPCPropertyGroup level but the uiModel has a GUID.
     * In this case we still use the status from cfg-engine as this is still valid.
     * 
     * 3. If the UIModel has not been loaded from database (i.e. it does not have a GUID).
     * we use the status from cfg-engine.
     *
     * @param list list of UIElementGroups (IPCUIComponents or IPCPropertyGroups)
     * @param statusImage the StatusImage of the bean
     */
    private void determineStatus(List list, StatusImage statusImage) {
        Iterator iter = list.iterator();
        while (iter.hasNext()){
            boolean addAsterisk = false;
            UIElementGroup grp = (UIElementGroup) iter.next();
            UIElementStatus status = null;
            // We determine the status from cfg-engine first.
            if (grp instanceof IPCUIComponent) {
                status = ((IPCUIComponent)grp).determineStatus(statusImage); ;
            }
            else if (grp instanceof IPCPropertyGroup){
                status = ((IPCPropertyGroup)grp).determineStatus(statusImage); ;
            }
            
            if (status != null){
                String statusKey = status.getDescription();
                // Check whether we have to re-determine the status using the frw-objects.
                boolean redetermine = false;
                if (uiModel.isLoadedFromDB() && grp.getElementList().size() > 0){
                    redetermine = true;
                }
                if (redetermine){
                    // Re-determine the status of the UIElementGroup.
                    boolean complete = uiModel.checkRequiredPropertiesForElementGroupIgnoreVisibility(grp);;
                    // We check whether the status from cfg-object different from the status that has been 
                    // determined via frw-objects (we only change the status if it is complete or incomplete).
                    if (statusKey.equals(IPCDynamicUIConstant.Status.STATUS_CONFIG_COMPLETE) 
                        && !complete ) {

                        // It is different -> change it.
                        status.setDescription(IPCDynamicUIConstant.Status.STATUS_CONFIG_INCOMPLETE);
                        status.setStyle(IPCDynamicUIConstant.Status.STYLE_CONFIG_INCOMPLETE);
                        status.setImageURL(IPCDynamicUIConstant.Images.IMG_NOVA_INCOMPLETE);
                        addAsterisk = true;
                    }
                    else if (statusKey.equals(IPCDynamicUIConstant.Status.STATUS_CONFIG_INCOMPLETE)
                        && complete) {

                        // It is different -> change it.
                        status.setDescription(IPCDynamicUIConstant.Status.STATUS_CONFIG_COMPLETE);
                        status.setStyle(IPCDynamicUIConstant.Status.STYLE_CONFIG_COMPLETE);
                        status.setImageURL(IPCDynamicUIConstant.Images.IMG_NOVA_LED_GREEN);
                    }
                    else if (statusKey.equals(IPCDynamicUIConstant.Status.STATUS_CONFIG_INCOMPLETE)
                        && !complete) {

                        // Status is the same (incomplete) but we have to add the asterisk if 
                        // no status lights should be displayed
                        addAsterisk = true;
                    }
                }
                else {
                    // No re-determination necessary but in case of incompleteness we need to add the asterisk
                    // (also depending on appearance.showstatuslights, see below) 
                    if (statusKey.equals(IPCDynamicUIConstant.Status.STATUS_CONFIG_INCOMPLETE)){
                        addAsterisk = true;
                    }
                }
            }
            // Depending on XCM setting (appearance.showstatuslights) we set the status or manipulate the description.
            if (uiContext.getShowStatusLights()){
                grp.setStatus(status);
            }
            else {
                if (addAsterisk){
                    String descr = checkDescription(grp.getDescription(), false);
                    grp.setDescription(descr);
                }
            }
        }
    }


    /* (non-Javadoc)
     * @see com.sap.isa.ipc.ui.jsp.action.visitor.IVisitor#visit(com.sap.isa.ipc.ui.jsp.beans.CharacteristicUIBean)
     */ 
    public void visit(CharacteristicUIBean bean) {
        // construct keys
        IPCConfigModelKey currentConfigModelKey = new IPCConfigModelKey(parentConfigModelKey);
        IPCUIElementKey currentUIModelKey = new IPCUIElementKey(parentUIModelKey);
        currentConfigModelKey.setCharacteristicName(bean.getName());
        // If the UIModel has been loaded from DB we need to reset the groupName in the configModelKey.
        // Reason: It might be possible that an IPCProperty has been moved to another group by the UIDesigner.
        // If we search with a configModelKey that has the groupName set, only the group with that name
        // is used for the search. Thus the IPCProperty cannot be found if it has been moved to another group.
        // For the same reason we don't remove the groupName in all cases: If the UIModel is not loaded from DB,
        // the IPCProperties are always in the same group as defined in the cfg-model. So we can restrict
        // the search to one group only (performance!).  
        if (uiModel.isLoadedFromDB()){
            currentConfigModelKey.setGroupName(null);
        }
        currentUIModelKey.setProperty(bean.getName());

        // get parent element in UI
        UIElementGroupData uiParent = (UIElementGroupData) uiModel.getUIElement(parentUIModelKey);
        IPCProperty prop;
        
        // 1. Obtain the IPCProperties for the current configModelKey.
        
        // For one object in the config-model more than one object in the frw-model might
        // exist (e.g. if a cstic has been copied). 
        List list = uiModel.getIPCUIProperties(currentConfigModelKey);
        boolean isPropNew = false;
        if (list.size() == 0) {
            // No object in the frw-model exists for the current configModelKey: create a new frw-object.
            isPropNew = true;
            prop = new IPCProperty();
            prop.setName(currentUIModelKey.getProperty());
			boolean showGroups =
						uiContext.getPropertyAsBoolean(
							RequestParameterConstants.USE_GROUP_INFORMATION);
			int characteristicPosition;
			if (showGroups) {
				characteristicPosition = bean.getCharacteristicGroupPosition();
			}else {
				characteristicPosition = bean.getCharacteristicInstancePosition();
			}
			//avoid ArrayIndexOutOfBoundException when index larger than size
			if (characteristicPosition > uiParent.getElementList().size()) {
				//add to the end of the list
				uiParent.addElement(prop);
			}else {
				((UIElementGroup)uiParent).addElement(characteristicPosition, prop);
			}
            
            
            if (characteristicPosition > list.size()) {
				list.add(prop);
            }else {
            	list.add(characteristicPosition, prop);
            }
            prop.setType(bean.allowsMultipleValues());
            // Display help link? 
            if (uiContext.getShowDetails()){
                prop.setDisplayHelpLink(true);
            }
            // Image display Type: default: nothing (set it only during init, later frw will control this)
            prop.setDisplayImageType(IPCImageLinkData.IMAGE_LINK_DISPLAY_TYPE_NOTHING);
            // Set the default label for the help link.
            prop.setHelpLinkLabel(WebUtil.translate(uiContext.getLocale(), "ipc.details", null));
        }

        // 2. Analyze CharacteristicUIBean
        
        // Description attribute in frw-object corresponds to displayName of cfg-object.
        StringBuffer descr = new StringBuffer(bean.getDisplayName());
        if (bean.hasUnit()) {
            // Add the unit to the description if one exists.
            descr.append(" ");
            descr.append(bean.getUnit());
        }
        String description = descr.toString();
        // For message cstics the description is handled differently. 
        description = IPCProperty.determineMessageCsticDescription(bean, description);
        // Determine whether the property needs to be set to read-only.
        boolean isReadOnly = false; 
        if (uiContext.getDisplayMode() || bean.isReadOnly()){
            isReadOnly = true;
        }
        boolean isRequired = bean.isRequired();
        int maxLength = bean.getMaxLength();
        int size = bean.getTypeLength();
        // For the tab-index the UIArea is needed.
        UIArea csticsArea = new UIArea("cstics", uiContext);
        String tabindex = csticsArea.getTabIndex();
        // UIElement is needed for shortcut if the cstic was the last focused.
        String lastFocusedAccessKey = "";
        if (bean.isLastFocused()){
            UIElement lastFocusedCstic = new UIElement("lastfocused", uiContext);
            lastFocusedAccessKey = lastFocusedCstic.getShortcutKey();
        }
        // SearchSet-feature: check if the current characteristic has been found with searchSet feature.
        if ((searchSetCstic != null) && (bean.getName().equals(searchSetCstic.getName()))){
            // Further check for correct instance.
            if (bean.getInstanceId().equals(searchSetCstic.getInstanceId())){
                // Set flag to true -> vist(GroupUIBean) will use this for expand & 
                // it is later used in this method to decide whether to set the focus to the 
                // property.
                searchSetCsticFound = true;
                searchSetUIGroup = (IPCPropertyGroup)uiParent;
            }
        }
        boolean visible = bean.isVisible();
        
        // Reset link
        String resetURL = IPCProperty.determineResetURL(uiContext, bean, pageContext);
        // Help link
        String helpURL = IPCProperty.determineHelpURL(uiContext, bean, pageContext);
        // Help link label
        String helpLinkLabelDefault = WebUtil.translate(uiContext.getLocale(), "ipc.details", null);
        // Interval display
        String interval = IPCProperty.determineInterval(uiContext, bean);
        // Input pattern
        String pattern = "";
        if (pageContext != null){
            pattern = bean.getInputFieldMask(pageContext);
        }
        else{
            log.debug( "Unit test environment, no page context available. Set pattern to blank");
        }
        // Date-format for calendar control
        String dateFormat = IPCProperty.determineFormat(bean);
        // Exit field function: used for online_evaluate=T mode (function is triggerd if a value is changed).
        String exitFieldFunction = IPCProperty.determineExitFieldFunction(uiContext, bean, pageContext);
        // Second line for longtext if necessary (see documentation of method IPCProperty.determineSecondLineForLongText())
        String secondLineForLongText = IPCProperty.determineSecondLineForLongText(uiContext, bean);
        // Key of characteristic for which the expand/collapse status has been changed by "Display Options" link
        String csticStatusChange = getCsticStatusChange();
        
        // 3. Update the frw-objects.
        
        // For one object in the config-model more than one object in the frw-model might
        // exist. Update all.
        Iterator itr = list.iterator();
        while (itr.hasNext()) {
            prop = (IPCProperty) itr.next();
            prop.setDescription(description);
            prop.setReadOnly(isReadOnly);
            prop.setDisabled(isReadOnly); // Disabled flag controls whether a request-parameter is parsed.  
                                          // This should be avoided if a property is read-only.
            prop.setRequired(isRequired);
            prop.setTabIndex(tabindex);
            // UiType handling: set uiType depending on layout-case, UI-model and user interaction (csticStatusChange) 
            // The uiType can change during runtime.
            prop.setUiType(IPCUITypeDetermination.determineUIType(prop, bean, csticStatusChange, uiContext));
            if (lastFocusedAccessKey.length()>0){
                prop.setAccessKey(lastFocusedAccessKey);
            }
            searchSetFocusSet = prop.setSearchSetFocus(uiModel, searchSetCsticFound, searchSetFocusSet);
            prop.setMaxLength(maxLength);
            prop.setSize(size);
            prop.setHidden(!visible);
            prop.setResetURL(resetURL);
            prop.setHelpLinkURL(helpURL);
            if (prop.getHelpLinkLabel() == ""){
                prop.setHelpLinkLabel(helpLinkLabelDefault);
            }
            // If at least on frw-object want to display the help link, ensure that part for the  
            // help-link is visible even if it was set invisible at config level because of global setting.
            if (prop.isDisplayHelpLink()){
                displayPart(true, UITypeRendererData.DISPLAY_HELP_LINK);
            }
            // Set display options link (and make part for "display options" link visible)
            String displayOptionsURL = prop.determineDisplayOptionsURL(uiContext, bean, pageContext);                    
            prop.setAllOptionsLink(displayOptionsURL);
            displayPart(displayOptionsURL, UITypeRendererData.DISPLAY_ALL_OPTION );
            prop.setInputInterval(interval);
            prop.setLongText(secondLineForLongText);
            prop.setInputPattern(pattern);
            // Calendar control?
            if (!dateFormat.equals("")){
                prop.setDefaultDateFormat(dateFormat);
            }
            if (!exitFieldFunction.equals("")){
                prop.setExitField(true);
            }
            else {
                prop.setExitField(false);
            }
            prop.setExitFieldFunction(exitFieldFunction);
            // Check if all changed values have been communicated to the config-model.
            if (prop.isValueChanged()){
                // Cfg-model and frw-model are NOT in sync:
                // We keep the values of the framework otherwise un-submitted values would be lost.
                // This is the buffering of un-submitted values.
            }
            else {
                // Cfg-model and frw-model are in sync: 
                // We clear the set values and set them in visit(ValueUIBean) anew.
                prop.clearAssignedValues();
            }
            // Reset the formattedString. It would be set again in visit(ValueUIBean) if necessary.
            prop.setFormattedString(null);
            prop.addEmptyLineForDDLB(); 
            prop.setSizeForMultiDDLB(this.uiContext);
            // The cfg-object does not use resource-keys for the short- and longtexts. The texts are already
            // in the right language.
            prop.setRessourceKeyUsed(false);
            // Set the status-icon according to the cfg-object if enabled.
            // Otherwise set status to null.
            if (uiContext.getShowStatusLights()) {
                prop.setStatus(bean.getStatusImage());
            }
            else {
                prop.setStatus((StatusImage)null);
            }
            // Handle Images and additional documents         
            handlePropertyMimes(bean, prop, isPropNew);
        }

        // Set keys before processing children
        parentUIModelKey = currentUIModelKey;
        parentConfigModelKey = currentConfigModelKey;

        // 4. Process children: values
        
        visitList(bean.getValues());
        
        // 5. Handling of erroneous values.
        
        // If there are values that have errors due to a user-input, an error-message
        // is added at the property and the style is changed to mark the property.
        // This has to be done for all occurrences of the property.
        // It cannot be done in the loop of step 3 because the children have to be 
        // processed before (which is done in step 4).
        Iterator itr2 = list.iterator();
        while (itr2.hasNext()) {
            prop = (IPCProperty) itr2.next();
            boolean hasError = prop.addErroneousValue(bean);
            if (hasError){
                // Add the error message
                uiModel.addFormatErrorMessage(bean, prop);
                // Change style to highlight the property.
                prop.setFormatErrorStatus(prop.getFormatErrorKey(bean));                
            }
        }   
    }
    
    private void handlePropertyMimes(CharacteristicUIBean bean, IPCProperty prop, boolean isPropNew) {
        MimeUIBean mime = bean.getMimeForWorkArea(false);         
        if(!mime.getType().equals(MimeUIBean.UNKNOWN)){
            if(uiContext.getPropertyAsBoolean(RequestParameterConstants.SHOW_PICTURE)) {
                if(mime.getType().equalsIgnoreCase(MimeUIBean.ICON)) {
                    prop.setThumbnailURL(buildURL(mime.getURL()));
                    prop.setImageURL(getEnlargedMimeURL(mime, "", ""));
                    prop.setThumbnailHeight(MimeUIBean.ICON_DEFAULT_HEIGHT);
                    prop.setThumbnailWidth(MimeUIBean.ICON_DEFAULT_WIDTH);
                    if(isPropNew){
                        prop.setDisplayImageType(IPCImageLinkData.IMAGE_LINK_DISPLAY_TYPE_THUMBNAIL);
                    }
                }else if(mime.getType().equalsIgnoreCase(MimeUIBean.IMAGE)) {
                    prop.setImageURL(getEnlargedMimeURL(mime, "", ""));
                    if(isPropNew){
                        prop.setDisplayImageType(IPCImageLinkData.IMAGE_LINK_DISPLAY_TYPE_HYPERLINK);
                    }
                }   
            } else if(mime.getType().equalsIgnoreCase(MimeUIBean.SOUND)) {
                prop.setSoundURL(buildURL(mime.getURL()));
            } else if(mime.getType().equalsIgnoreCase(MimeUIBean.O2C)) {
                if( uiContext.getPropertyAsBoolean( RequestParameterConstants.SHOW_CHARACTERISTIC_VALUE_3D_PICTURE, true ) ){
                    prop.setO2cImageURL(buildURL(mime.getURL()));                        
                }
            } 
        }
        // ensure that part is active, when required
        if (prop.getDisplayImageType() != IPCImageLinkData.IMAGE_LINK_DISPLAY_TYPE_NOTHING){
            uiModel.displayInvisiblePart(UITypeRendererData.THUMBNAIL); 
        }
        
        
        // Handle additional documents
        String mimeURL = bean.getLinkToAdditionalMime();
        if(!(mimeURL).equals("") && mimeURL != null){
            prop.setAdditionalDocumentURL(buildURL(mimeURL));               
            // Initial settings for new property
            if (isPropNew) {
                prop.setDisplayDocumentLink(true);
            }
            // Make sure part is visble, if there is the need to show the link.
            if(prop.isDisplayDocumentLink()){
                displayPart(true, UITypeRendererData.ADD_DOC_LINKS);    
            }
        }
    } 
        
    /* (non-Javadoc)
     * @see com.sap.isa.ipc.ui.jsp.action.visitor.IVisitor#visit(com.sap.isa.ipc.ui.jsp.beans.MimeUIBean)
     * In the Maintenance Object, mimes aren't modeled as seperate object,
     * but are modeled as attributes of the IPCProperty and IPCAllowedValue.
     * Hence they are set in the visit(CharateristicUIBean) or visit(ValueUIBean).
     */
    public void visit(MimeUIBean bean) {
        // auto generated method
  
    }

    /* (non-Javadoc)
     * @see com.sap.isa.ipc.ui.jsp.action.visitor.IVisitor#visit(com.sap.isa.ipc.ui.jsp.beans.StatusImage)
     * In the Maintenance Object, the status isn't modeled as seperate object,
     * but is modeled as attributes of the IPCComponent, IPCGroup, IPCProperty or IPCAllowedValue respectivly.
     * Hence the status is processed in the method visit() for the corresponding object.
     */
    public void visit(StatusImage bean) {
        // auto generated sub
    } //public void visit(StatusImage bean) {
    
    
    /* (non-Javadoc)
     * @see com.sap.isa.ipc.ui.jsp.action.visitor.IVisitor#visit(com.sap.isa.ipc.ui.jsp.beans.ValueUIBean)
     */
    public void visit(ValueUIBean bean) {
        
        // 1. Analyze ValueUIBean
        
        String valueName = bean.getName();
        boolean isAssigned = bean.isAssigned();
        // Determine read-only state of the value. It is read-only if
        //   - we are running in display-mode OR
        //   - cstic is read-only OR
        //   - value is not assignable OR
        //   - value is assigned by system OR
        boolean readOnly = false;
        if (uiContext.getDisplayMode()
                || bean.isCsticReadOnly()
                || !bean.isAssignable()
                || bean.isAssignedBySystem()) {
            readOnly = true;
        }
        // If the values has a price (1:1 condition) we add it to the description.
        String price = bean.getPrice();
        StringBuffer priceBuffer = new StringBuffer();
        if (uiContext.getShowOneToOneConditions()
            && (price != null)
            && (price != "")) {
            priceBuffer.append(" (");
            priceBuffer.append(
                WebUtil.translate(
                    uiContext.getLocale(),
                    "ipc.surcharge",
                    null));
            priceBuffer.append(" ");
            priceBuffer.append(price);
            priceBuffer.append(")");
        }
        // Second column for longtext if necessary (see documentation of method IPCAllowedValue.determineSecondColumnForLongText()).
        String longText = IPCAllowedValue.determineSecondColumnForLongText(uiContext, bean);

        // 2. Obtain the IPCProperties the current value belongs to.
        List list = uiModel.getIPCUIProperties(parentConfigModelKey);
        Iterator itr = list.iterator();
        
        // Loop over list of IPCProperties and handle the value.
        while (itr.hasNext()) {
            IPCProperty prop = (IPCProperty) itr.next();
            // 3. Get the IPCAllowedValue object.
            // @TODO Refactoring necessary: use prop.getAllowedValue(valueName, bean, uiContext) instead and remove isValueNew attribute (changes in additional-document-handling and mimes-handling necessary!)
            IPCAllowedValue aVal = prop.getAllowedValue(valueName);
            boolean isValueNew = false;
            if (aVal == null){
                isValueNew = true;
                // 3.1 value does not exist: create it
                aVal = new IPCAllowedValue();
                aVal.setValue(valueName);
                // 3.2 add the value
                prop.addAllowedValueUITypeDepending(bean, aVal, uiContext);
            }
            
            // 4.  Update the IPCAllowedValue-object.
            
            StringBuffer descr = new StringBuffer(IPCAllowedValue.determineDescription(bean, prop));
            descr.append(priceBuffer);
            aVal.setDescription(descr.toString());
            aVal.setRessourceKeyUsed(false);
            // Visibility handling of the value.
            prop.determineAllowedValueVisibilty(bean, aVal, uiContext);
            aVal.setReadOnly(readOnly);
            aVal.setLongText(longText);
            
            // 5. Set value as assigned if necessary.
            if (isAssigned) {
                // Check if all changed values have been communicated to the config-model.
                if (prop.isValueChanged()){
                    // Cfg-model and frw-model are NOT in sync:
                    // We don't set values from the config-model, we just keep the values of the frw-model.
                }
                else {
                    // Cfg-model and frw-model are in sync:
                    // Previously set values have been cleared in visit(CharacteristicUIBean).
                    // We just set values anew.
                    prop.setValue(valueName);
                }
            }
            // 6. Set mime
            // Handle Images and additional documents         
            handleValueMimes(bean, aVal, isValueNew);
            
            // 7. Set the status for message cstic if the current characteristic is a message cstic.
            if (IPCProperty.uiTypeIsForMessageCstic(prop.getUiType())){
                aVal.setStatus(bean);
            }
        } 
    }
    private void handleValueMimes(ValueUIBean bean, IPCAllowedValue aVal, boolean isValueNew) {
        MimeUIBean mime = bean.getMimeForWorkArea();
        if (isValueNew) {
            aVal.setDisplayImageType(IPCImageLinkData.IMAGE_LINK_DISPLAY_TYPE_NOTHING);
        }
        if(!mime.getType().equals(MimeUIBean.UNKNOWN)){
            if(uiContext.getPropertyAsBoolean(RequestParameterConstants.SHOW_PICTURE)) {
                if(mime.getType().equalsIgnoreCase(MimeUIBean.ICON)) {
                    aVal.setThumbnailURL(buildURL(mime.getURL()));
                    aVal.setImageURL(getEnlargedMimeURL(mime, "", ""));
                    aVal.setThumbnailHeight(MimeUIBean.ICON_DEFAULT_HEIGHT);
                    aVal.setThumbnailWidth(MimeUIBean.ICON_DEFAULT_WIDTH);
                    if (isValueNew) {
                        aVal.setDisplayImageType(IPCImageLinkData.IMAGE_LINK_DISPLAY_TYPE_THUMBNAIL);
                    }   
                }else if(mime.getType().equalsIgnoreCase(MimeUIBean.IMAGE)) {
                    aVal.setImageURL(getEnlargedMimeURL(mime, "", ""));
                    if (isValueNew) {
                        aVal.setDisplayImageType(IPCImageLinkData.IMAGE_LINK_DISPLAY_TYPE_HYPERLINK);
                    }   
                }   
            } else if(mime.getType().equalsIgnoreCase(MimeUIBean.SOUND)) {
                aVal.setSoundURL(buildURL(mime.getURL()));
            } else if(mime.getType().equalsIgnoreCase(MimeUIBean.O2C)) {
                if( uiContext.getPropertyAsBoolean( RequestParameterConstants.SHOW_CHARACTERISTIC_VALUE_3D_PICTURE, true ) ){
                    aVal.setO2cImageURL(buildURL(mime.getURL()));
                }
            } 
        } 
        // ensure that part is active, when required
        if (aVal.getDisplayImageType() != IPCImageLinkData.IMAGE_LINK_DISPLAY_TYPE_NOTHING){
            uiModel.displayInvisiblePart(UITypeRendererData.THUMBNAIL); 
        }
        
        // Set additional document
        String mimeURL = bean.getLinkToAdditionalMime();
        if(!(mimeURL).equals("") && mimeURL != null){
            aVal.setAdditionalDocumentURL(buildURL(mimeURL));
            // initial settings for new vakues
            if (isValueNew) {
                aVal.setDisplayDocumentLink(true);
            }
            // make sure part is visble, if there is the need to show the link
            if(aVal.isDisplayDocumentLink()){
                displayPart(true, UITypeRendererData.ADD_DOC_LINKS);    
            }
        
        } 
    } 
    
    /**
     * This method will iterate over the given list of vsistable objects to visit each object.
     * 
     * @param list of VisistableObjects to visit
     */
    private void visitList(List list) {
        Iterator itr = list.iterator();
        while (itr.hasNext()) {
            // if the list contains NON-VisitableObjects, we ignore them
            Object obj = itr.next();
            if (obj instanceof IVisitableObject) {
                IVisitableObject bean = (IVisitableObject) obj;

                // remeber the parent keys before analysing the childs
                IPCUIElementKey parentUIModelKey = this.parentUIModelKey;
                IPCConfigModelKey parentConfigModelKey =
                    this.parentConfigModelKey;

                // trigger analysis of childs
                bean.accept(this);

                // restore parent keys
                this.parentUIModelKey = parentUIModelKey;
                this.parentConfigModelKey = parentConfigModelKey;
            }
        }//while (itr.hasNext()) {
    }// private void visitList(List list) {

    /**
     * Builds the complete URL to the mime 
     * @param linkToAdditionalMime: relative link of the mime
     * @return complete url to the mime
     */
    private String buildURL(String url) {
        if(url.equals("") || url == null || pageContext == null) {
            return "";
        } else {
            String masterdataURL = uiContext.getMasterdataMimesURL();
            String mimeURL = MasterdataUtil.getMimeURL(pageContext, masterdataURL, null, null, url);
            return mimeURL;
        }
    }
    
    private String getEnlargedMimeURL(MimeUIBean mime, String groupName, String instanceId){
        String url = new String();
        if( uiContext.getPropertyAsBoolean( RequestParameterConstants.SHOW_MIMES_IN_NEW_WINDOW ) ){
            String imageUrl = WebUtil.getAppsURL(pageContext,
                                            null,
                                            "/ipc/enlargeMimeObject.do",
                                            null,
                                            null,
                                            false);
            imageUrl = imageUrl + "?" + mime.getURLEncodedRepresentation(0);
        } else {
            String action = WebUtil.getAppsURL( pageContext, null, "/ipc/enlargeMimeObject.do",
                                                null, null,false );
            String addUrl = "?" + mime.getURLEncodedRepresentation(0);
            url = "javascript:openMimeInSameWindow('" + action + "', '" + addUrl + "', '" + groupName + "', '" + instanceId + "');";
        }
       return url;
    }
    
    /**
     * Removes the part from the list of invisible objects, when the condition is met. 
     * 
     * @param condition: condtion which has to be fullfilled.
     * @param part:      Part to be removed from the list of invisble parts.
     */
    private void displayPart(boolean condition, String part){
        if(condition){
            uiModel.displayInvisiblePart(part);
        }else{
            uiModel.hideInvisiblePart(part);        
        }
    }
    
    /**
     * Removes the part from the list of invisible objects, when the condition is not an empty string. 
     * 
     * @param condition: condtion which has to be fullfilled.
     * @param part:      Part to be removed from the list of invisble parts.
     */
    private void displayPart(String condition, String part){
        if(condition != null && !condition.equals("")){
            uiModel.displayInvisiblePart(part);
        }       
    }
    
    /**
     * In case no status lights are shown and the configuration is incomplete 
     * the corresponding property group as well as the instance (assignment block)
     * should be highlighted by *.
     * This method checks appends the * if applicable.
     * 
     * @param description: description to be checked
     * @param isComplete:  identifier, whether the configuration is complete/consistent. 
     * @return
     */
    private String checkDescription(String description, boolean isComplete){
        StringBuffer descrBuffer = new StringBuffer(description);
        if (!uiContext.getShowStatusLights() && !isComplete) {
            descrBuffer.append(" (*)");
        }
        return descrBuffer.toString();
    }
    
    /**
     * There's no method to directly check the status of a characteristic group, 
     * but the status could be accassed via the StatusImage assigned
     * to the Characteristic Group. 
     * 
     * @param description: description to be checked
     * @param statusImage: StatusImage attached to the characteristic group
     * @return
     */
    private String checkDescription(String description, StatusImage statusImage){
        String descr = description;
        if(statusImage != null ){
            String statusKey = statusImage.getResourceKey();
            if( statusKey.equals(IPCDynamicUIConstant.Status.STATUS_CONFIG_INCOMPLETE)
                ||statusKey.equals(IPCDynamicUIConstant.Status.STATUS_CONFIG_INCONSISTENT)){
                
                descr = checkDescription(descr, false); 
            } 
        }
        return descr;
    }
    
    
    /**
     * @return the CharacteristicUIBean found by the search/set functionality
     */
    public CharacteristicUIBean getSearchSetCstic() {
        return searchSetCstic;
    }

    /**
     * @return the GroupUIBean found by the search/set functionality
     */
    public GroupUIBean getSearchSetGroup() {
        return searchSetGroup;
    }

    /**
     * @param bean
     */
    public void setSearchSetCstic(CharacteristicUIBean bean) {
        searchSetCstic = bean;
    }

    /**
     * @param bean
     */
    public void setSearchSetGroup(GroupUIBean bean) {
        searchSetGroup = bean;
    }


    /**
     * Returns the characteristic for which the expand/collapse status should be changed.
     * Format: <instanceId>.<csticName>.<techKey>
     * @return Key of the characteristic for which the expand/collapse status should be changed. 
     */
    public String getCsticStatusChange() {
        return csticStatusChange;
}
    /**
     * Set the characteristic for which the expand/collapse status should be changed.
     * Format: <instanceId>.<csticName>.<techKey>
     * @param Key of the characteristic for which the expand/collapse status should be changed.
     */
    public void setCsticStatusChange(String string) {
        csticStatusChange = string;
    }

}