/*
 * Created on 28.11.2004
 *
 * To change the template for this generated file go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
package com.sap.isa.ipc.ui.jsp.action;

import java.io.IOException;
import java.util.Hashtable;
import java.util.Iterator;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.sap.isa.core.Constants;
import com.sap.isa.core.UserSessionData;
import com.sap.isa.core.businessobject.management.MetaBusinessObjectManager;
import com.sap.isa.core.util.RequestParser;
import com.sap.isa.ipc.ui.jsp.uiclass.UIContext;
import com.sap.spc.remote.client.object.Configuration;
import com.sap.spc.remote.client.object.Instance;

/**
 * @author 
 *
 * To change the template for this generated type comment go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
public class ExpandCustomizationListInstance extends IPCBaseAction {

	/* (non-Javadoc)
	 * @see com.sap.isa.core.BaseAction#doPerform(org.apache.struts.action.ActionMapping, org.apache.struts.action.ActionForm, javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse)
	 */
	public ActionForward ecomPerform(ActionMapping mapping,
		ActionForm form,
		HttpServletRequest request,
		HttpServletResponse response,
		UserSessionData userSessionData,
		RequestParser requestParser,
		MetaBusinessObjectManager mbom,
		boolean multipleInvocation,
		boolean browserBack)
			throws IOException, ServletException {
			UIContext uiContext = getUIContext(request);
			Configuration config = uiContext.getCurrentConfiguration();
            
            String instanceId = request.getParameter(Constants.CURRENT_INSTANCE_ID);
            if (!isSet(instanceId)) {
                return parameterError(mapping, request, Constants.CURRENT_INSTANCE_ID, getClass().getName());
            }
            changeContextValue(request, Constants.CURRENT_INSTANCE_ID, instanceId);

            String groupName = request.getParameter(Constants.CURRENT_CHARACTERISTIC_GROUP_NAME);
            if (!isSet(groupName)) {
                return parameterError(mapping, request, Constants.CURRENT_CHARACTERISTIC_GROUP_NAME, getClass().getName());
            }
            changeContextValue(request, Constants.CURRENT_CHARACTERISTIC_GROUP_NAME, groupName);
            
			boolean expanded = uiContext.getPropertyAsBoolean(RequestParameterConstants.SHOW_CUSTOMIZATIONLIST_INSTANCES_EXPANDED);
			boolean add = false;
			Hashtable expandedNodes = (Hashtable)uiContext.getProperty(InternalRequestParameterConstants.CUSTOMIZATIONLIST_EXPANDED_INSTANCES);
			if (expandedNodes == null) {
				add = true;
				expandedNodes = new Hashtable();
			}
			// find the assigned values for all expanded instances which belong to the configuration
			for (Iterator instIt = config.getInstances().iterator(); instIt.hasNext(); ) {
				Instance inst = (Instance) instIt.next();
				if (add && expanded) {
					expandedNodes.put(inst.getId(), inst.getId());
				}
		
//				groupList = inst.getGroups().iterator();
//				// Iterate through all characteristc groups
//				while (groupList.hasNext()) {
//					GroupUIBean group = (GroupUIBean)groupList.next();
//					csticList = group.getCharacteristics().iterator();
//					// Iterate through all characteristics to find all the values assigned by the
//					// user
//					while (csticList.hasNext()) {
//						  CharacteristicUIBean cstic = (CharacteristicUIBean) csticList.next();
//						  //check whether the visibility was restricted by scenario (VehicleManager)
//						  if(!uiContext.getShowInvisibleCharacteristics() &&
//							uiContext.isCharacteristicsVisibleViewSet() &&
//							uiContext.isCharacteristicVisibleRestricted(cstic.getName()))
//							continue;
//						  boolean assigneds = cstic.hasAssignedValues();
//						  if (assigneds && (cstic.isVisible() ||
//							uiContext.getShowInvisibleCharacteristics())) {
//							customizationList.add(cstic);
//						  }
//					}
//				}
			}
			String changedInstId = request.getParameter(InternalRequestParameterConstants.CUSTOMIZATIONLIST_EXPANDED_INSTANCES);
			if (isSet(changedInstId)) {
				if (expandedNodes.get(changedInstId) == null) {
					expandedNodes.put(changedInstId, changedInstId);
				}
				else {
					expandedNodes.remove(changedInstId);
				}
			}
			
		uiContext.setProperty(InternalRequestParameterConstants.CUSTOMIZATIONLIST_EXPANDED_INSTANCES,expandedNodes);
		
		customerExit(mapping, form, request, response, userSessionData, requestParser, mbom, multipleInvocation, browserBack);

		return mapping.findForward(ActionForwardConstants.SUCCESS);
	}

}
