package com.sap.isa.ipc.ui.jsp.action;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.sap.isa.businessobject.ipc.IPCBOManager;
import com.sap.isa.core.UserSessionData;
import com.sap.isa.core.businessobject.management.MetaBusinessObjectManager;
import com.sap.isa.core.util.RequestParser;
import com.sap.isa.core.util.RequestParser.Parameter;
import com.sap.isa.ipc.ui.jsp.uiclass.UIContext;
import com.sap.spc.remote.client.object.Configuration;
import com.sap.spc.remote.client.object.IPCClient;
import com.sap.spc.remote.client.object.IPCException;
import com.sap.spc.remote.client.object.KnowledgeBase;
import com.sap.spc.remote.client.object.imp.rfc.RfcDefaultIPCClient;
import com.sap.spc.remote.client.util.ext_configuration; 

public class StartCRMCatalogConfigAction extends StartConfigurationAction {

    private static final String PRODUCT_LOGSYS = "productLogSys";

    public ActionForward ecomPerform(
        ActionMapping mapping,
        ActionForm form,
        HttpServletRequest request,
        HttpServletResponse response,
        UserSessionData userSessionData,
        RequestParser requestParser,
        MetaBusinessObjectManager mbom,
        boolean multipleInvocation,
        boolean browserBack)
        throws IOException, ServletException {
        
            UIContext uiContext = IPCBaseAction.getUIContext(request);
            if (uiContext == null) { //the uiContext has not been initialized
            ActionForward forward = processScenarioParameter(mapping, request);
            if (!forward.getName().equals(ActionForwardConstants.SUCCESS)) return forward;
                uiContext = IPCBaseAction.getUIContext(request); //uiContext created in processScenarioParameter
            }
        
            super.htmlTrace(request);
            IPCBOManager ipcBoManager =
                (IPCBOManager) userSessionData.getBOM(IPCBOManager.NAME);
            if (ipcBoManager == null) {
                log.error("No IPC BOM in session data!");
                return (mapping.findForward(INTERNAL_ERROR));
            }
            
            Parameter callerParam = requestParser.getParameter(CALLER);
            if (!callerParam.isSet()) return parameterError(mapping, request, RequestParameterConstants.CALLER, getClass().getName());
            uiContext.setCaller(callerParam.getValue().getString());
            
            Parameter language = requestParser.getParameter(RequestParameterConstants.LANGUAGE);
            if (!language.isSet()) {
                return parameterError(mapping, request, RequestParameterConstants.LANGUAGE, getClass().getName());
            }
            Parameter productId = requestParser.getParameter(RequestParameterConstants.PRODUCT_ID);
            if (!productId.isSet()) {
                return parameterError(mapping, request, RequestParameterConstants.PRODUCT_ID, getClass().getName());
            }
            Parameter kbName = requestParser.getParameter(RequestParameterConstants.KB_NAME);
            String kbNameValue = null;
            if (kbName.isSet() && !kbName.getValue().getString().equals("")){
                kbNameValue = kbName.getValue().getString();
            }
            Parameter kbVersion = requestParser.getParameter(RequestParameterConstants.KB_VERSION);
            String kbVersionValue = null;
            if (kbVersion.isSet() && !kbVersion.getValue().getString().equals("")){
                kbVersionValue = kbVersion.getValue().getString();
            }

            Parameter kbLogsys = requestParser.getParameter(RequestParameterConstants.KB_LOGSYS);
            Parameter productLogsys = requestParser.getParameter(PRODUCT_LOGSYS);
            
            if ((!kbLogsys.isSet()) || kbLogsys.getValue().getString().equals("")) {
                // we use the productLogsys instead
                kbLogsys = productLogsys;
            }
            
            if ((!kbLogsys.isSet()) || kbLogsys.getValue().getString().equals("")) {
                // both kbLogsys and product logsys are not set
                return parameterError(mapping, request, RequestParameterConstants.KB_LOGSYS, getClass().getName());
            }
            Parameter kbProfile = requestParser.getParameter(RequestParameterConstants.KB_PROFILE);
            Parameter kbDate = requestParser.getParameter(RequestParameterConstants.KB_DATE);
            if (!kbDate.isSet()) {
                return parameterError(mapping, request, RequestParameterConstants.KB_DATE, getClass().getName());
            }

            Parameter displayMode = requestParser.getParameter(RequestParameterConstants.DISPLAY_MODE);
            if (!displayMode.isSet() || !displayMode.getValue().getBoolean()) {
                uiContext.setDisplayMode(true);
            }else if (displayMode.getValue().getBoolean()){
                uiContext.setDisplayMode(false);
            }

            customerExit(mapping, form, request, response, userSessionData, requestParser, mbom, multipleInvocation, browserBack);

            IPCClient ipcClient = ipcBoManager.createIPCClient(language.getValue().getString());

            

            // kbName and kbVersion are necessary for the 5.0 fm
            // if kbName or kbVersion is not passed we request a list of kbs from the server
            if((kbNameValue==null) || (kbVersionValue == null)){
                log.debug("kbName or kbVersion not passed. Requesting list of kbs for product " + productId.getValue().getString() + " from server.");
                List listOfKBs = ((RfcDefaultIPCClient)ipcClient).getKnowledgeBasesForProduct(productId.getValue().getString(), kbLogsys.getValue().getString());
                // by default we use the latest kb of this product.
                if (listOfKBs.size() > 0) {
                    KnowledgeBase kb = (KnowledgeBase)listOfKBs.get(0);
                    kbNameValue = kb.getName();
                    kbVersionValue = kb.getVersion();
                    log.debug("Using kbName \"" + kbName + "\" and kbVersion \"" + kbVersion + "\"");
                }
                else {
                    String logMessage = "No knowledgebase found for product " + productId.getValue().getString();
                    log.debug(logMessage);
                    //TODO forward to error page without dump
                    throw new IPCException(logMessage);           
                }
            }
            
            ext_configuration extConfig = getExternalConfiguration(request);
            Configuration config;
            if (extConfig != null) {
                config = ipcClient.createConfiguration(extConfig, new String[] {}, new String[] {}, kbDate.getValue().getString());
            }else {
            
                config =
                    ipcClient.createConfiguration(
                        productId.getValue().getString(),
                        language.getValue().getString(),
                        kbDate.getValue().getString(),
                        kbLogsys.getValue().getString(),
                        kbNameValue,
                        kbVersionValue,
                        kbProfile.getValue().getString(),
                        new String[] {},
                        new String[] {});
            }
                    
            uiContext.setCurrentConfiguration(config);
            //initialize the context
            setInitialContextValues(request, config);
                    
                    
            return mapping.findForward(ActionForwardConstants.SUCCESS);     
        
        }



}
