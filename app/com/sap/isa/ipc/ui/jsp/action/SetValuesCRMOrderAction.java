package com.sap.isa.ipc.ui.jsp.action;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.sap.isa.core.UserSessionData;
import com.sap.isa.core.businessobject.management.MetaBusinessObjectManager;
import com.sap.isa.core.util.RequestParser;
//import com.sap.isa.ipc.ui.jsp.uiclass.UIContext;
//import com.sap.spc.remote.client.object.IPCClient;
//import com.sap.spc.remote.client.object.IPCSession;

/**
 * Every state change of the configuration needs to result in a lock to the session.
 * This lock guarantees that the order does not read the configuaration state before it
 * has been published and unlocked by the JSP UI.
 */
public class SetValuesCRMOrderAction extends SetCharacteristicsValuesAction {

	public ActionForward ecomPerform(ActionMapping mapping,
		ActionForm form,
		HttpServletRequest request,
		HttpServletResponse response,
		UserSessionData userSessionData,
		RequestParser requestParser,
		MetaBusinessObjectManager mbom,
		boolean multipleInvocation,
		boolean browserBack)
			throws IOException, ServletException {

//locking is done in StartCRMONlineConfigurationAction
//				UIContext uiContext = getUIContext(request);
//
//				if(uiContext == null)
//					return (mapping.findForward(INTERNAL_ERROR));
//        
//				IPCClient client = (IPCClient)uiContext.getProperty(InternalRequestParameterConstants.IPC_CLIENT);
//				IPCSession session = client.getIPCSession();
//				if (!session.isLocked()) {
//					client.getIPCSession().lock();
//				}

				return super.ecomPerform(
					mapping,
					form,
					request,
					response,
					userSessionData,
					requestParser,
					mbom,
					multipleInvocation,
					browserBack);
			}
}
