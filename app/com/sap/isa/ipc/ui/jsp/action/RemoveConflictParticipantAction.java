package com.sap.isa.ipc.ui.jsp.action;

/**
 * Title:
 * Description:  JSP UI for the IPC.
 * Copyright:    Copyright (c) 2001
 * Company:
 * @author
 * @version 1.0
 */
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.sap.isa.core.Constants;
import com.sap.isa.core.UserSessionData;
import com.sap.isa.core.businessobject.management.MetaBusinessObjectManager;
import com.sap.isa.core.util.RequestParser;
import com.sap.isa.ipc.ui.jsp.beans.UIBeanFactory;
import com.sap.isa.ipc.ui.jsp.constants.MessagesRequestParameterConstants;
import com.sap.isa.ipc.ui.jsp.uiclass.UIContext;
import com.sap.spc.remote.client.object.Configuration;
import com.sap.spc.remote.client.object.Conflict;
import com.sap.spc.remote.client.object.ConflictParticipant;
import com.sap.spc.remote.client.object.ExplanationTool;
import com.sap.spc.remote.client.object.IPCException;
import com.sap.spc.remote.client.object.Instance;
import com.sap.spc.remote.client.object.imp.DefaultIPCItem;
import com.sap.spc.remote.client.object.imp.DefaultInstance;
import com.sap.spc.remote.client.object.imp.GroupCache;

public class RemoveConflictParticipantAction
	extends IPCBaseAction
	implements RequestParameterConstants, ActionForwardConstants {

		public ActionForward ecomPerform(ActionMapping mapping,
			ActionForm form,
			HttpServletRequest request,
			HttpServletResponse response,
			UserSessionData userSessionData,
			RequestParser requestParser,
			MetaBusinessObjectManager mbom,
			boolean multipleInvocation,
			boolean browserBack)
				throws IOException, ServletException {

		UIContext uiContext =
			getUIContext(request);
            
		setContextValues(request);

		String conflictId =
			request.getParameter(Constants.CURRENT_CONFLICT_ID);
		String participantId =
			request.getParameter("ipc.conflict_" + conflictId);
		if (participantId == null)
			participantId =
				request.getParameter(
					Constants.CURRENT_CONFLICT_PARTICIPANT_ID);
		if ((conflictId == null) || (participantId == null))
			return (mapping.findForward(SUCCESS));
		Configuration currentConfiguration =
			uiContext.getCurrentConfiguration();
		if (currentConfiguration == null)
			return (mapping.findForward(INTERNAL_ERROR));
		ExplanationTool explanationTool =
			currentConfiguration.getExplanationTool();
		try {
			Conflict conflict =
				explanationTool.getConflict(
					conflictId,
					uiContext.showConflictSolutionRate(),
					uiContext.getLocale(),
					getResources(request));
			if (conflict == null)
				return (mapping.findForward(SUCCESS));
			ConflictParticipant participant =
				conflict.getParticipant(participantId);
			if (participant == null)
				return (mapping.findForward(SUCCESS));
			participant.remove();
			
			
			//refresh all buffers
			List instances = currentConfiguration.getInstances();         // note 987856: getInstances() sets cacheDirty to false therefore 
		    currentConfiguration.setCacheDirty(true);                     // we have to call it before we set it to true
			currentConfiguration.setInstanceCacheDirty(true);
			if (instances != null){
				for (Iterator instIt = instances.iterator(); instIt.hasNext(); ) {
					Instance currentInstance = (Instance)instIt.next();
					DefaultIPCItem ipcItem = (DefaultIPCItem)currentInstance.getIpcItem();
					if (ipcItem != null) {
						ipcItem.setCacheDirty(); //flush() sends some SET or DELETE commands, hence make Item cache also dirty.
					}
					HashMap groupCaches = ((DefaultInstance)currentInstance).groupCaches;
					if (groupCaches != null) {
						Iterator gCacheIt = groupCaches.values().iterator();
						while (gCacheIt.hasNext()) {
							GroupCache gCache = (GroupCache) gCacheIt.next();
							gCache.setRequiredRefresh(GroupCache.REFRESH_REQUIRED_FOR_ALL_CSTICS);
						}
					}
					((DefaultInstance)currentInstance).setCacheIsDirty(true);
				}
			}
			
			
		} catch (IPCException e) {
			setExtendedRequestAttribute(request, RequestParameterConstants.IPC_EXCEPTION, e);
			return mapping.findForward(INTERNAL_ERROR);
		}
		//information for messaging that first conflict was solved
		Object messageObj = request.getAttribute(MessagesRequestParameterConstants.MESSAGES);
		if (messageObj != null) {
			List messages = (List)messageObj;
			messages.add(UIBeanFactory.getUIBeanFactory().newMessageUIBean("ipc.message.info4"));
		}else {
			List messages = new ArrayList();
			messages.add(UIBeanFactory.getUIBeanFactory().newMessageUIBean("ipc.message.info4"));
			setExtendedRequestAttribute(request, MessagesRequestParameterConstants.MESSAGES, messages);
		}

		customerExit(mapping, form, request, response, userSessionData, requestParser, mbom, multipleInvocation, browserBack);

		if (currentConfiguration.isConsistent()) {
			return mapping.findForward(ActionForwardConstants.NO_CONFLICTS);
		}else {
			return mapping.findForward(ActionForwardConstants.SUCCESS);
		}
		
	}

}