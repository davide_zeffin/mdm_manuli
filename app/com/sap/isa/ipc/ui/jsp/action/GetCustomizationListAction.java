package com.sap.isa.ipc.ui.jsp.action;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.List;
import java.util.TreeSet;
import java.util.Vector;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.sap.isa.core.Constants;
import com.sap.isa.core.UserSessionData;
import com.sap.isa.core.businessobject.management.MetaBusinessObjectManager;
import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.core.util.RequestParser;
import com.sap.isa.ipc.ui.jsp.beans.CharacteristicUIBean;
import com.sap.isa.ipc.ui.jsp.beans.ConditionUIBean;
import com.sap.isa.ipc.ui.jsp.beans.ConfigUIBean;
import com.sap.isa.ipc.ui.jsp.beans.GroupUIBean;
import com.sap.isa.ipc.ui.jsp.beans.InstanceUIBean;
import com.sap.isa.ipc.ui.jsp.beans.UIBeanFactory;
import com.sap.isa.ipc.ui.jsp.beans.ValueUIBean;
import com.sap.isa.ipc.ui.jsp.constants.CustomizationListRequestParameterConstants;
import com.sap.isa.ipc.ui.jsp.container.PricingConditionContainer;
import com.sap.isa.ipc.ui.jsp.uiclass.UIContext;
import com.sap.spc.remote.client.object.Characteristic;
import com.sap.spc.remote.client.object.CharacteristicGroup;
import com.sap.spc.remote.client.object.CharacteristicValue;
import com.sap.spc.remote.client.object.IPCItem;
import com.sap.spc.remote.client.object.IPCPricingCondition;
import com.sap.spc.remote.client.object.IPCPricingConditionSet;

public class GetCustomizationListAction
	extends IPCBaseAction
	implements RequestParameterConstants, ActionForwardConstants {

	public ActionForward ecomPerform(
		ActionMapping mapping,
		ActionForm form,
		HttpServletRequest request,
		HttpServletResponse response,
		UserSessionData userSessionData,
		RequestParser requestParser,
		MetaBusinessObjectManager mbom,
		boolean multipleInvocation,
		boolean browserBack)
		throws IOException, ServletException {

		UIContext uiContext = getUIContext(request);

		//construct the hierarchy of bean objects for the configuration
		ConfigUIBean configUIBean = this.getCurrentConfiguration(uiContext);
		String currentInstanceId = getContextValue(request, Constants.CURRENT_INSTANCE_ID);
		if (!isSet(currentInstanceId)) {
			currentInstanceId =
				uiContext.getCurrentConfiguration().getRootInstance().getId();
		}
		InstanceUIBean currentInstanceUIBean =
			this.getCurrentInstance(
				uiContext,
				configUIBean,
				currentInstanceId,
				request);
		String	curCharGroupName = getContextValue(request, Constants.CURRENT_CHARACTERISTIC_GROUP_NAME);
		String	currentScrollGroupName = getContextValue(request, Constants.CURRENT_SCROLL_CHARACTERISTIC_GROUP_NAME);
		String	groupStatusChangeName = getContextValue(request, Constants.GROUP_STATUS_CHANGE);
		Hashtable groupStatus =
			(Hashtable) uiContext.getProperty(
				Constants.GROUP_STATUS_CHANGE);

		GroupUIBean currentGroupUIBean =
			getCustCharacteristicGroups(
				uiContext,
				currentInstanceUIBean,
				curCharGroupName,
				groupStatusChangeName,
				groupStatus,
                configUIBean,
				request);

		String	curCsticName = getContextValue(request, Constants.CURRENT_CHARACTERISTIC_NAME);
		String instanceChangedParameter =
			(String)request.getAttribute(
				InternalRequestParameterConstants.INSTANCE_CHANGED);
		String groupChangedParameter =
			(String)request.getAttribute(
				InternalRequestParameterConstants.GROUP_CHANGED);

        CharacteristicUIBean currentCsticUIBean = null;        
        // get all cstics and all values for all instances
        Iterator instanceIt = configUIBean.getInstances().iterator();
        while (instanceIt.hasNext()){
            InstanceUIBean loopedInstance = (InstanceUIBean) instanceIt.next();
    		currentCsticUIBean =
    			getCustCharacteristics(
    				uiContext,
    				loopedInstance,
    				instanceChangedParameter,
    				groupChangedParameter,
    				curCsticName,
                    false);
    
    		getCustCharacteristicValues(
    			uiContext,
    			loopedInstance,
    			log);
        }

		IPCItem pricingItem =
			currentInstanceUIBean.getBusinessObject().getIpcItem();
		if (pricingItem != null && uiContext.isShowPricesInCustomizationList()) {
			String pricingProcedure =
				pricingItem.getDocument().getPricingProcedure();
			if (pricingProcedure == null || pricingProcedure.equals("")) {
				//in the crm online scenario, the document has not been created via the IPCClient,
				//but from the CRM order. Therefore the IPCClient does not know the pricing procedure.
				//It has to use an alternative API to read the pricing procedure from the server.
				pricingProcedure = pricingItem.getDocument().getPropertiesFromServer().getPricingProcedure();
			}
			getPricingConditions(pricingProcedure, configUIBean, uiContext);
		}

		customerExit(mapping, form, request, response, userSessionData, requestParser, mbom, multipleInvocation, browserBack);

		request.setAttribute(
			CustomizationListRequestParameterConstants.CURRENT_CONFIGURATION,
			configUIBean);
		request.setAttribute(
			CustomizationListRequestParameterConstants.CURRENT_INSTANCE,
			currentInstanceUIBean);
		request.setAttribute(
			CustomizationListRequestParameterConstants
				.CURRENT_CHARACTERISTIC_GROUP,
			currentGroupUIBean);
		request.setAttribute(
			CustomizationListRequestParameterConstants.CURRENT_CHARACTERISTIC,
			currentCsticUIBean);


        Hashtable expandedNodes = (Hashtable)uiContext.getProperty(InternalRequestParameterConstants.CUSTOMIZATIONLIST_EXPANDED_INSTANCES);

        // In case of customizationlist.instancesexpanded=T we need to keep a list of the known instances,
        // because if a new instance is added this has to be expanded initially, so it has to be added to 
        // the list of expandedNodes. If we would not do this a new instances would always appear collapsed 
        // initially.
        // The list knownInstances helps to find out whether a particular instance was added
        HashMap knownInstances = (HashMap)uiContext.getProperty(InternalRequestParameterConstants.CUSTOMIZATIONLIST_KNOWN_INSTANCES);
        
        // initialize the container for the expanded instances
        // empty Hashtable means no expanded instances
        if (expandedNodes == null) {
                //user did not expand any instances so far
                expandedNodes = new Hashtable();
                knownInstances = new HashMap(); // to find out later whether new instances have been added
                if (uiContext.getPropertyAsBoolean(RequestParameterConstants.SHOW_CUSTOMIZATIONLIST_INSTANCES_EXPANDED)) {
                    //customizing that all instances should be expanded upon startup
                    for (Iterator instIt = configUIBean.getInstances().iterator(); instIt.hasNext(); ) {
                        InstanceUIBean inst = (InstanceUIBean) instIt.next();
                            expandedNodes.put(inst.getId(), inst.getId());
                            knownInstances.put(inst.getId(), inst.getId());
                    }
                }else {
                    //no instances expanded upon startup
                }
                uiContext.setProperty(
                    InternalRequestParameterConstants
                        .CUSTOMIZATIONLIST_EXPANDED_INSTANCES,
                    expandedNodes);
                uiContext.setProperty(InternalRequestParameterConstants.CUSTOMIZATIONLIST_KNOWN_INSTANCES, knownInstances);
        }
        else{
            // In case of customizationlist.instancesexpanded=T we have to check whether new instances were added.
            // If this is the case we have to add them to expandedNodes so that they will be displayed expanded initially.  
            if (uiContext.getPropertyAsBoolean(RequestParameterConstants.SHOW_CUSTOMIZATIONLIST_INSTANCES_EXPANDED)) {
                for (Iterator instIt = configUIBean.getInstances().iterator(); instIt.hasNext(); ) {
                    InstanceUIBean inst = (InstanceUIBean) instIt.next();
                    String initialInstID = (String)knownInstances.get(inst.getId());
                    // if it is a new one it cannot be found in the knownInstances list -> then add this to the expandedNodes
                    if (initialInstID == null){
                        expandedNodes.put(inst.getId(), inst.getId());
                        knownInstances.put(inst.getId(), inst.getId());
                    }
                }
            }
            // if customizationlist.instancesexpanded=F we don't have to do anything here
        }


		//search for a forward in the request
		//if nothing found, forward to succes
		return processForward(mapping, request, ActionForwardConstants.SUCCESS);
	}

	private void getPricingConditions(
	    String pricingProcedure,
		ConfigUIBean config,
		UIContext uiContext) {

		if (pricingProcedure != null) {
			for (Iterator instIt = config.getInstances().iterator();
				instIt.hasNext();
				) {
				InstanceUIBean currentInstance = (InstanceUIBean)instIt.next();
				IPCItem pricingItem =
					currentInstance.getBusinessObject().getIpcItem();
				if (pricingItem == null) {
					continue;
				}
				//associate conditions to root instance and sub instances according to customizing
				IPCPricingConditionSet pricingConditionSet =
					pricingItem.getPricingConditions();
				IPCPricingCondition[] pricingConditionArr =
					pricingConditionSet.getPricingConditions();
				if (pricingConditionArr == null) {
					continue;
				}
				PricingConditionContainer variantConditions =
					new PricingConditionContainer(pricingConditionArr.length);
				PricingConditionContainer nonVariantConditions =
					new PricingConditionContainer(pricingConditionArr.length);
				splitVariantConditionsFromNonVariantConditions(
					pricingConditionArr,
					variantConditions,
					nonVariantConditions);
	
				remove1To1Conditions(
					currentInstance,
					variantConditions,
					uiContext);
				if (currentInstance.isRootInstance()) {
				//add conditions that are to be displayed at root instance level
				TreeSet rootInstanceSteps =
						uiContext
							.getPriceDisplayCustomizing()
							.getRootInstanceProcSteps(
						pricingProcedure);
					for (Iterator it = rootInstanceSteps.iterator();
						it.hasNext();
						) {
					Integer stepNo = (Integer) it.next();
					//has non 1:1 variant conditions
						TreeSet conditions =
							variantConditions.getConditionsForStep(stepNo);
					if (conditions != PricingConditionContainer.C_EMPTY) {
							addInstanceConditions(
								config.getRootInstance(),
								conditions,
								uiContext);
					}
					//has other conditions
				    conditions =
						nonVariantConditions.getConditionsForStep(stepNo);
					if (conditions != PricingConditionContainer.C_EMPTY) {
							addInstanceConditions(
								config.getRootInstance(),
								conditions,
								uiContext);
					}
				}
	
					//add conditions that are to be displayed at total level to root instance
					TreeSet totalSteps =
						uiContext
							.getPriceDisplayCustomizing()
							.getTotalsProcSteps(
						pricingProcedure);
					for (Iterator it = totalSteps.iterator(); it.hasNext();) {
						Integer stepNo = (Integer) it.next();
					//has non 1:1 variant conditions
						TreeSet conditions =
							variantConditions.getConditionsForStep(stepNo);
					if (conditions != PricingConditionContainer.C_EMPTY) {
							addInstanceConditions(
								config.getRootInstance(),
								conditions,
								uiContext);
					}
					//has other conditions
					conditions =
						nonVariantConditions.getConditionsForStep(stepNo);
					if (conditions != PricingConditionContainer.C_EMPTY) {
							addTotalConditions(config, conditions, uiContext);
					}
				}
				} else {
	
					//add conditions that are to be displayed at sub instance level
					TreeSet subInstanceSteps =
						uiContext
							.getPriceDisplayCustomizing()
							.getSubInstancesProcSteps(
							pricingProcedure);
					for (Iterator subInstStepsit = subInstanceSteps.iterator();
						subInstStepsit.hasNext();
						) {
						Integer stepNo = (Integer) subInstStepsit.next();
					//has non 1:1 variant conditions
						TreeSet conditions =
							variantConditions.getConditionsForStep(stepNo);
					if (conditions != PricingConditionContainer.C_EMPTY) {
							addInstanceConditions(
								currentInstance,
								conditions,
								uiContext);
					}
					//has other conditions
						conditions =
							nonVariantConditions.getConditionsForStep(stepNo);
					if (conditions != PricingConditionContainer.C_EMPTY) {
							addInstanceConditions(
								currentInstance,
								conditions,
								uiContext);
					}
				}
			}
		}
	}
	}

	/**
	 * @param bean
	 * @param conditions
	 */
	private void addInstanceConditions(InstanceUIBean instance, TreeSet conditions, UIContext uiContext) {
		for (Iterator cit = conditions.iterator(); cit.hasNext(); ) {
			IPCPricingCondition condition = (IPCPricingCondition)cit.next();
			ConditionUIBean conditionUIBean =
			UIBeanFactory.getUIBeanFactory().newConditionUIBean(
					condition,
					uiContext);
			instance.addCondition(conditionUIBean);
		}		
	}

	/**
	 * @param bean
	 * @param conditions
	 */
	private void addTotalConditions(ConfigUIBean config, TreeSet conditions, UIContext uiContext) {
		for (Iterator cit = conditions.iterator(); cit.hasNext(); ) {
			IPCPricingCondition condition = (IPCPricingCondition)cit.next();
			ConditionUIBean conditionUIBean =
			UIBeanFactory.getUIBeanFactory().newConditionUIBean(
					condition,
					uiContext);
			config.addCondition(conditionUIBean);
		}		
	}

	/**
	* @param pricingConditionArr
	* @param variantConditions
	* @param nonVariantConditions
	*/
	private void splitVariantConditionsFromNonVariantConditions(
		IPCPricingCondition[] pricingConditionArr,
		PricingConditionContainer variantConditions,
		PricingConditionContainer nonVariantConditions) {
		for (int i = 0; i < pricingConditionArr.length; i++) {
			IPCPricingCondition condition = pricingConditionArr[i];
			if (condition.getVariantConditionKey() != null
				&& !condition.getVariantConditionKey().equals("")) {
				variantConditions.add(condition);
			} else {
				nonVariantConditions.add(condition);
			}
		}
	}

	//removes the 1:1 conditions from variantConditions, creates a ConditionUIBean object for each condition
	/**
	 * Searches for 1:1 conditions (variant conditions directly associated to characteristic value)
	 * in variantConditions.
	 * Creates ConditionUIBean object for each 1:1 condition found and adds this ConditionUIBean object
	 * to the characteristicValue.
	 * Removes the found 1:1 condition from variantConditions.
	 */
	private void remove1To1Conditions(
		InstanceUIBean currentInstance,
		PricingConditionContainer variantConditions,
		UIContext uiContext) {
		//find 1:1 conditions attached to the characteristic values
		for (Iterator csticIt = currentInstance.getCharacteristics().iterator();
			csticIt.hasNext();
			) {
			CharacteristicUIBean currentCharacteristic =
				(CharacteristicUIBean) csticIt.next();
			for (Iterator valueIt =
				currentCharacteristic.getAssignedValues().iterator();
				valueIt.hasNext();
				) {
				ValueUIBean currentValue = (ValueUIBean) valueIt.next();
				String conditionKey = currentValue.getConditionKey();
				IPCPricingCondition variantCondition =
					variantConditions.getConditionForKey(conditionKey);
				if (variantCondition != null) {
					ConditionUIBean variantConditionUIBean =
					UIBeanFactory.getUIBeanFactory().newConditionUIBean(variantCondition, uiContext);
					currentValue.setVariantCondition(variantConditionUIBean);
					variantConditions.remove(conditionKey);
				} else {
					log.error(
						"Condition "
							+ conditionKey
							+ " of value "
							+ currentValue.getName()
							+ " not found in conditions of instance "
							+ currentInstance.getName());
				}
			}
		}

	}

	/**
	 * Iterates through the characteristics associated with the business object of currentInstanceUIBean
	 * or currentGroupUIBean.
	 * Creates ui bean objects for characteristics to display and associates them to the currentInstanceUIBean
	 * and to the groups.
	 * Returns the reference to the currently active characteristic, that is the characteristic the page
	 * should scroll to.
	 * Reimplements getCharacteristics from IPCBaseAction,
	 * since IPCBaseAction.getCharacteristics returns the characteristics only for currentGroupUIBean.
	 * The customization list however displays the characteristics for all groups.
	 * Performance is better if the customization list is not to be displayed and grouping
	 * is used. In this case only the characteristics for the currently displayed group are
	 * to be shown.
	 * @param uiContext
	 * @param currentInstanceUIBean
	 * @param instanceChangedParameter
	 * @param currentGroupUIBean
	 * @param groupChangedParameter
	 * @param curCsticName
	 * @param csticStatusChange
	 * @param searchSetCstic
	 * @return
	 * TODO: optimize for customizationList; no csticStatusChange nor searchSetCstic needed
	 */
	protected static CharacteristicUIBean getCustCharacteristics(
		UIContext uiContext,
		InstanceUIBean currentInstanceUIBean,
		String instanceChangedParameter,
		String groupChangedParameter,
		String curCsticName,
        boolean allCstics) {
		List characteristics;

		for (Iterator groupIt = currentInstanceUIBean.getGroups().iterator();
			groupIt.hasNext();
			) {
			GroupUIBean group = (GroupUIBean) groupIt.next();

			//get all characteristics independent of their groups
			characteristics =
				group.getBusinessObject().getCharacteristics(
					uiContext.getShowInvisibleCharacteristics());

			// restrict the attributes 'readonly' and 'visible' dependent of the
			// business scenario (f.e. VehicleManager)
			if (uiContext.isCharacteristicsReadonlyViewSet()
				|| uiContext.isCharacteristicsVisibleViewSet()) {

				for (int i = 0; i < characteristics.size(); i++) {
					Characteristic cstic =
						(Characteristic) characteristics.get(i);

					//possibility to influence the properties of a cstic independent from the engine (VMS)
					//@TODO move into a seperate action
					if (uiContext
						.isCharacteristicVisibleRestricted(cstic.getName())) {
						if (cstic.isVisible())
							cstic.setVisible(false);
					}
					//possibility to influence the properties of a cstic independent from the engine (VMS)
					//@TODO move into a seperate action
					if (uiContext
						.isCharacteristicReadonlyRestricted(cstic.getName())) {
						if (!cstic.isReadOnly()) {
							cstic.setReadOnly(true);
						}

					}
					// remove cstic if not required to display
					if (!cstic.isVisible()
						&& !uiContext.getShowInvisibleCharacteristics()) {
						characteristics.remove(i);
					}
				}
			}

			ArrayList characteristicUIBeans = new ArrayList();
			for (Iterator it = characteristics.iterator(); it.hasNext();) {
				Characteristic characteristic = (Characteristic) it.next();
                // If flag "allCstics" == false: 
                // Add characteristic only if it has assigned values because in the customizationlist
                // only characterstics with values are displayed
                if (!allCstics){
                    if (characteristic.hasAssignedValues()) {
                        // check if show_message_cstics is true (In-Screen Messaging is enbaled) AND cstic name begins with the prefix defined in the XCM 
                        if (uiContext.getPropertyAsBoolean(ProcessRequestParameterAction.SHOW_MESSAGE_CSTICS) && 
                              characteristic.getName().startsWith(uiContext.getPropertyAsString(ProcessRequestParameterAction.MESSAGE_CSTICS_PREFIX))  ){
                            continue;  //ignore this cstic, because it is a message cstic
                        }
                        else {
                  				CharacteristicUIBean characteristicUIBean =
            					UIBeanFactory.getUIBeanFactory().newCharacteristicUIBean(characteristic, uiContext);
                				characteristicUIBeans.add(characteristicUIBean);
                				group.addCharacteristic(characteristicUIBean);
                				currentInstanceUIBean.addCharacteristic(characteristicUIBean);
                        }
                    }
                }
                else {
                    // Add all cstics
                    // check if show_message_cstics is true (In-Screen Messaging is enbaled) AND cstic name begins with the prefix defined in the XCM 
                    if (uiContext.getPropertyAsBoolean(ProcessRequestParameterAction.SHOW_MESSAGE_CSTICS) && 
                          characteristic.getName().startsWith(uiContext.getPropertyAsString(ProcessRequestParameterAction.MESSAGE_CSTICS_PREFIX))  ){
                        continue;  //ignore this cstic, because it is a message cstic
                    }
                    else {
                            CharacteristicUIBean characteristicUIBean =
                            UIBeanFactory.getUIBeanFactory().newCharacteristicUIBean(characteristic, uiContext);
                            characteristicUIBeans.add(characteristicUIBean);
                            group.addCharacteristic(characteristicUIBean);
                            currentInstanceUIBean.addCharacteristic(characteristicUIBean);
                    }
                }
            }
        }

		// delete former currentCsticName if group or instance has changed
		if ((groupChangedParameter != null
			&& groupChangedParameter.equals("true"))
			|| (instanceChangedParameter != null
				&& instanceChangedParameter.equals("true"))) {
					curCsticName = "";
		}

//		//------------------------- changing visibility of characteristics -----------------------------------//
//		//TODO: That would be the task of a separate action
//		// Now handle the status of the characteristics - a single klick on the
//		// characteristics should make them (in-)visible
//		Hashtable csticStatus =
//			(Hashtable) uiContext.getProperty(
//				Constants.CHARACTERISTIC_STATUS_CHANGE);
//		if (csticStatus == null) {
//			csticStatus = new Hashtable();
//			uiContext.setProperty(
//				Constants.CHARACTERISTIC_STATUS_CHANGE,
//				csticStatus);
//		}
//
//		//------------------------ end changing visibility of characteristics ----------------------------------//

		CharacteristicUIBean currentCsticUIBean = null;
		if (curCsticName != null
			&& !curCsticName.equals("")
			&& !curCsticName.equals("null")) {
			currentCsticUIBean =
				currentInstanceUIBean.getCharacteristic(curCsticName);
		}
		return currentCsticUIBean;
	}

	/**
	 * Creates UIBeans for the characteristic values and associates them to the passed
	 * InstanceUIBean or GroupUIBean. Overwrites getCharacteristicValues from IPCBaseAction,
	 * since IPCBaseAction.getCharacteristicValues returns the values only for group.
	 * The customization list however displays the values for all groups.
	 * Performance is better if the customization list is not to be displayed and grouping
	 * is used. In this case only the characteristics for the currently displayed group are
	 * to be shown.
	 * @param request
	 * @param uiContext
	 * @param currentInstanceUIBean
	 * @param group
	 * @throws IOException
	 * @throws ServletException
	 */
	protected static void getCustCharacteristicValues(
		UIContext uiContext,
		InstanceUIBean currentInstanceUIBean,
		IsaLocation log) {

		Hashtable csticValues = new Hashtable();

		List csticList;
		csticList = currentInstanceUIBean.getCharacteristics();
		Iterator csticListIt = csticList.iterator();
		while (csticListIt.hasNext()) {
			Vector returnedValues;
			CharacteristicUIBean currentCharacteristic =
				(CharacteristicUIBean) csticListIt.next();
			List allValues =
				currentCharacteristic.getBusinessObject().getValues();
				returnedValues = new Vector(allValues.size());
				for (Iterator valuesIt = allValues.iterator();
					valuesIt.hasNext();
					) {
					CharacteristicValue value =
						(CharacteristicValue) valuesIt.next();
					ValueUIBean valueUIBean = UIBeanFactory.getUIBeanFactory().newValueUIBean(value, uiContext);
					returnedValues.add(valueUIBean);
					currentCharacteristic.addValue(valueUIBean);
				}
				csticValues.put(
					currentCharacteristic.getName(),
					returnedValues);
		}
		log.debug(csticValues);
	}
	
    /**
     * Iterates through the groups of the business objects of all instances of the ConfigUIBean.
     * Creates UIBean objects for the groups to display and associates them to their InstanceUIBeans.
     * Returns a reference to the UIBean of the currently active characteristicGroup.
     * This is a special implementation of the getCharacteristicGroups() of IPCBaseAction,
     * since IPCBaseAction.getCharacteristicGroups() returns the groups only for one instance.
     * The customization list however displays the instances for all groups.
     * Performance is better if the customization list is not to be displayed 
     * @param uiContext
     * @param currentInstanceUIBean
     * @param curCharGroupName
     * @param groupStatusChangeName
     * @param groupStatus
     * @param currentConfig
     * @param request
     * @return
     * @throws IOException
     * @throws ServletException
     */
    protected GroupUIBean getCustCharacteristicGroups(
                 UIContext uiContext,
                 InstanceUIBean currentInstanceUIBean,
                 String curCharGroupName,
                 String groupStatusChangeName,
                 Hashtable groupStatus,
                 ConfigUIBean currentConfig,
                 HttpServletRequest request
                 ) {
        
        GroupUIBean currentCharacteristicGroup = null;

        if (groupStatus == null)
        {
          groupStatus = new Hashtable();
          uiContext.setProperty(Constants.GROUP_STATUS_CHANGE,groupStatus);;
        }

        // This piece of code will fill the hashtable with the instance names of
        // collapsed nodes
        if (groupStatusChangeName != null)
        {
          if (groupStatus.get(groupStatusChangeName) != null)
          {
            groupStatus.remove(groupStatusChangeName);
          }
          else
          {
            groupStatus.put(groupStatusChangeName,groupStatusChangeName);
          }
        }

        // loop over all instances of the current configuration
        Iterator instanceIt = currentConfig.getInstances().iterator();
        while (instanceIt.hasNext()){
            InstanceUIBean loopedInstance = (InstanceUIBean) instanceIt.next();
            
            // first get a list of completely all available characteristicGroups
            List groupBusinessObjects = loopedInstance.getBusinessObject().getCharacteristicGroups(uiContext.getShowInvisibleCharacteristics(), uiContext.getPropertyAsBoolean(RequestParameterConstants.SHOW_GENERAL_TAB_FIRST));
            List groups = new Vector();
            for (int i = 0; i < groupBusinessObjects.size(); i++) {
                CharacteristicGroup group = (CharacteristicGroup) groupBusinessObjects.get(i);
                // if the parameter showEmptyGroups = false (default), then show only
                // the groups which have visible characteristicValue
                if (!uiContext.showEmptyGroups() && !hasVisibleCharacteristics(uiContext, group)) {
                    //do not add the group to the groups to be displayed
                }else {
                    GroupUIBean groupUIBean = UIBeanFactory.getUIBeanFactory().newGroupUIBean(group, uiContext);
                    groups.add(groupUIBean);
                    //associate with instanceUIBean
                    loopedInstance.addGroup(groupUIBean);
                }
            }
        }
        
        // get the currentCharacteristicGroup...        
        GroupUIBean searchSetGroup = (GroupUIBean) request.getAttribute(InternalRequestParameterConstants.SEARCH_SET_FOUND_GROUP);
        if (isSet(curCharGroupName) && searchSetGroup == null) {
            currentCharacteristicGroup = UIBeanFactory.getUIBeanFactory().newGroupUIBean(currentInstanceUIBean.getBusinessObject().getCharacteristicGroup(curCharGroupName), uiContext);
        } else if (searchSetGroup != null) {
            currentCharacteristicGroup = searchSetGroup;            
        }else {
            // Select the first group. Note: There has to be at least one group!
            if (!currentInstanceUIBean.getGroups().isEmpty()) {
                //these are the groups wich contain at least one characteristic
                currentCharacteristicGroup = (GroupUIBean)(currentInstanceUIBean.getGroups().get(0));
            }
            else {
                //TODO: What should happen if no group with visible cstics exists, but
                //the user decided not to see empty groups
                //these are all groups including the empty ones
                currentCharacteristicGroup = UIBeanFactory.getUIBeanFactory().newGroupUIBean((CharacteristicGroup)currentInstanceUIBean.getBusinessObject().getCharacteristicGroups(uiContext.getShowInvisibleCharacteristics(), uiContext.getPropertyAsBoolean(RequestParameterConstants.SHOW_GENERAL_TAB_FIRST)).get(0), uiContext);
            }
        }

        return currentCharacteristicGroup;
    }
    
    
}
