package com.sap.isa.ipc.ui.jsp.action;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.List;
import java.util.StringTokenizer;
import java.util.Vector;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.sap.isa.core.Constants;
import com.sap.isa.core.UserSessionData;
import com.sap.isa.core.businessobject.management.MetaBusinessObjectManager;
import com.sap.isa.core.util.RequestParser;
import com.sap.isa.ipc.ui.jsp.beans.CharacteristicUIBean;
import com.sap.isa.ipc.ui.jsp.beans.GroupUIBean;
import com.sap.isa.ipc.ui.jsp.beans.MessageUIBean;
import com.sap.isa.ipc.ui.jsp.beans.UIBeanFactory;
import com.sap.isa.ipc.ui.jsp.constants.MessagesRequestParameterConstants;
import com.sap.isa.ipc.ui.jsp.dynamicui.IPCConfigModelKey;
import com.sap.isa.ipc.ui.jsp.dynamicui.IPCDynamicUIConstant;
import com.sap.isa.ipc.ui.jsp.dynamicui.IPCUIComponent;
import com.sap.isa.ipc.ui.jsp.dynamicui.IPCUIModel;
import com.sap.isa.ipc.ui.jsp.dynamicui.IPCUIPage;
import com.sap.isa.ipc.ui.jsp.uiclass.UIContext;
import com.sap.spc.remote.client.object.Characteristic;
import com.sap.spc.remote.client.object.CharacteristicGroup;
import com.sap.spc.remote.client.object.CharacteristicValue;
import com.sap.spc.remote.client.object.Configuration;
import com.sap.spc.remote.client.object.Instance;

/**
 * This action reads the request parameter SEARCH_SET_VALUE. 
 * Depending on the XCM setting behavior.searchset.mode it tries to find a 
 * characteristic or set characteristic value(s). It also add a message to the 
 * message list with is put to the UIContext with the parameter MESSAGES.
 */
public class SearchSetAction extends IPCBaseAction implements RequestParameterConstants,
																	ActionForwardConstants {                                                                        
	public ActionForward ecomPerform(ActionMapping mapping,
		ActionForm form,
		HttpServletRequest request,
		HttpServletResponse response,
		UserSessionData userSessionData,
		RequestParser requestParser,
		MetaBusinessObjectManager mbom,
		boolean multipleInvocation,
		boolean browserBack)
			throws IOException, ServletException {

        UIContext uiContext = getUIContext(request);

		IPCUIModel ui = (IPCUIModel) uiContext.getProperty(IPCDynamicUIConstant.SESSION_DYNAMIC_UI);

		List  messages = new ArrayList();    
        try{        
            String searchString = request.getParameter(SEARCH_SET_VALUE);
            if (searchString == null || searchString.equals("")){
                MessageUIBean ssMessage = UIBeanFactory.getUIBeanFactory().newMessageUIBean();
                ssMessage.setMessageStatus(MessageUIBean.ERROR);
                ssMessage.setMessageTranslateKey("ipc.searchset.parameter.missing");
                messages.add(ssMessage);
				setExtendedRequestAttribute(request, MessagesRequestParameterConstants.MESSAGES, messages);   
                return (mapping.findForward(SUCCESS));            
            }
            // id are always upper case
            searchString = searchString.toUpperCase();
            Configuration currentConfiguration = uiContext.getCurrentConfiguration();
            String currentInstanceId = null;
            // get the current expanded instance from the UIModel
            // if more than one is expanded only the first is taken
            IPCUIPage page = (IPCUIPage)ui.getActivePage();
            if (page != null) {
            	IPCUIComponent comp = page.getFirstExpandedComponent();
            	if (comp != null){
            		currentInstanceId = comp.getName(); 
            	}
            }
            if (!isSet(currentInstanceId)) {
                currentInstanceId = uiContext.getCurrentConfiguration().getRootInstance().getId();
            }
            Instance currentInstance = uiContext.getCurrentConfiguration().getInstance(currentInstanceId);
            setContextValue(request, Constants.CURRENT_INSTANCE_ID, currentInstanceId);
			String selectedInstanceId = request.getParameter(Constants.SELECTED_INSTANCE_ID);
			if (isSet(selectedInstanceId)) {
				this.setContextValue(request, Constants.SELECTED_INSTANCE_ID, selectedInstanceId);
			}
			String groupName = request.getParameter(Constants.CURRENT_CHARACTERISTIC_GROUP_NAME);
			if (isSet(groupName)) {
				this.setContextValue(request, Constants.CURRENT_CHARACTERISTIC_GROUP_NAME, groupName);
			}
			String currentScrollGroup = request.getParameter(Constants.CURRENT_SCROLL_CHARACTERISTIC_GROUP_NAME);
			if (isSet(currentScrollGroup)) {
				this.setContextValue(request, Constants.CURRENT_SCROLL_CHARACTERISTIC_GROUP_NAME, currentScrollGroup);
			}
			// scrollGroup needs to be set also to the uiContext because this is a session-wide information 
			// and it is not set by the js-functions (e.g. onCsticValueChange, etc.)
			uiContext.setProperty(Constants.CURRENT_SCROLL_CHARACTERISTIC_GROUP_NAME,currentScrollGroup);
                            
            String mode = uiContext.getPropertyAsString(SEARCH_SET_MODE);
            boolean whiteSpace = uiContext.getPropertyAsBoolean(SHOW_INNER_SPACES_IN_LANGUAGE_INDEPENDENT_NAMES, false);
            boolean showIvisibleCstics = uiContext.getShowInvisibleCharacteristics();
            String delimiter = uiContext.getPropertyAsString(SEARCH_SET_DELIMITER);
            int containsDelim = searchString.indexOf(delimiter);
            
            // if delimiter then only set values
            if (containsDelim == -1 && (mode.equals(SEARCH_SET_XCM_CSTIC) || mode.equals(SEARCH_SET_XCM_CSTIC_AND_VALUE))){
                Characteristic curCstic = currentInstance.getCharacteristic(searchString, showIvisibleCstics);
                MessageUIBean csticMessage = UIBeanFactory.getUIBeanFactory().newMessageUIBean();
                if (curCstic != null) {
                    // cstic found do not perform value set
                    CharacteristicUIBean csticBean = CharacteristicUIBean.getInstance(request, curCstic, uiContext);
                    String groupId = curCstic.getGroup().getName();
                    CharacteristicGroup group;
                    if (groupId == null || groupId.equals("")){
                        group = (CharacteristicGroup)currentInstance.getCharacteristicGroups(uiContext.getShowInvisibleCharacteristics(), uiContext.getPropertyAsBoolean(RequestParameterConstants.SHOW_GENERAL_TAB_FIRST)).get(0);
                    } else {
                        group =  currentInstance.getCharacteristicGroup(groupId);
                    }
                    GroupUIBean groupUIBean = UIBeanFactory.getUIBeanFactory().newGroupUIBean(group, uiContext);
                    csticMessage.setMessageStatus(MessageUIBean.SUCCESS);
                    csticMessage.setMessageTranslateKey("ipc.searchset.cstic.found");
                    csticMessage.setMessageShortText(curCstic.getName() + " - " + curCstic.getLanguageDependentName());
                    setExtendedRequestAttribute(request, SEARCH_SET_FOUND_CSTIC, csticBean);
					setExtendedRequestAttribute(request, SEARCH_SET_FOUND_GROUP, groupUIBean);
                    messages.add(csticMessage);    
					setExtendedRequestAttribute(request, MessagesRequestParameterConstants.MESSAGES, messages); 
                    return (mapping.findForward(SUCCESS));
                } else if (curCstic == null && mode.equals(SEARCH_SET_XCM_CSTIC)){
                    // only cstic search but no cstic found 
                    csticMessage.setMessageStatus(MessageUIBean.WARNING);
                    csticMessage.setMessageTranslateKey("ipc.searchset.cstic.notfound");
                    csticMessage.setMessageShortText(searchString);
                    messages.add(csticMessage);
					setExtendedRequestAttribute(request, MessagesRequestParameterConstants.MESSAGES, messages);   
                    uiContext.setProperty(SEARCH_SET_VALUE, searchString);
                    return (mapping.findForward(SUCCESS));
                }
            }
            // perform  value set
            // all values which should be set
            List setValues = new Vector();
            // all values which are not unique
            List valuesNotUnique = new Vector();
            // all unknown paramter
            List unknownCsticOrValue = new Vector();
            // all assigned Values
            List valuesAssigned = new Vector();
            // all values who should be assigned but couldn't
            List valuesAssignedFailed = new Vector();
            // all values which where found but are not assignable (invisible or not assignable)
            List valuesNotAssignable = new Vector();
            Hashtable value_csticPairs = new Hashtable();
            if (mode.equals(SEARCH_SET_XCM_VALUE) || mode.equals(SEARCH_SET_XCM_CSTIC_AND_VALUE)){
                 searchCsticValues(searchString, delimiter, whiteSpace, currentInstance, showIvisibleCstics, setValues, valuesNotUnique, unknownCsticOrValue, value_csticPairs);
                 for (int i=0; i<setValues.size();i++){
                    String valueName = (String)setValues.get(i);
                    List csticNames = (List) value_csticPairs.get(valueName);
                    // check whether value is unique
                    if (csticNames.size()>1){
                        valuesNotUnique.add(valueName);
                        continue;
                    }
                    String csticName = (String) csticNames.get(0);
                    Characteristic csticObject = currentInstance.getCharacteristic(csticName, showIvisibleCstics);
                    if (csticObject != null){
                        CharacteristicValue theValue = csticObject.getValue(valueName);
                        if (theValue!=null && theValue.isAssignable() && theValue.isVisible()){
                            List valuesToSet = new Vector();
                            if (csticObject.allowsMultipleValues()){
                                // Check all values whether they are marked as assigned.
                                // Using getAssignableValues() does not work because this only returns the values that are
                                // already set on the server (not the values that are only marked as assigned on client-side).
                                List allValues = csticObject.getValues();
                                for (int j=0; j<allValues.size(); j++){
                                    CharacteristicValue currentValue = (CharacteristicValue)allValues.get(j);
                                    if (currentValue.isAssignedByUser()) {
                                        valuesToSet.add(currentValue.getName());
                                    }
                                }
                            }
                            valuesToSet.add(valueName);
                            String [] array = new String [valuesToSet.size()];
                            for (int j=0; j<valuesToSet.size(); j++){
                                array[j] = (String)valuesToSet.get(j);
                            }
                            if (csticObject.setValues(array)){
                                valuesAssigned.add(valueName);
                            }else{
                                valuesAssignedFailed.add(valueName);
                            }
                        } else {
                            valuesNotAssignable.add(valueName);
                        }
                    } else {
                       unknownCsticOrValue.add(valueName);
                    }
                    // only scroll to group and cstic if single cstic value set
                    if (setValues.size()==1){
                        // change group and cstic for scrolling 
//                        CharacteristicUIBean csticBean = UIBeanFactory.getUIBeanFactory().newCharacteristicUIBean(csticObject, uiContext);
                        CharacteristicUIBean csticBean = CharacteristicUIBean.getInstance(request, csticObject, uiContext);
						setExtendedRequestAttribute(request, SEARCH_SET_FOUND_CSTIC, csticBean);
                        String groupId = csticObject.getGroup().getName();
                        if (groupId != null){
                            CharacteristicGroup group =  currentInstance.getCharacteristicGroup(groupId);
                            GroupUIBean groupUIBean = UIBeanFactory.getUIBeanFactory().newGroupUIBean(group, uiContext);
							setExtendedRequestAttribute(request, SEARCH_SET_FOUND_GROUP, groupUIBean);
                        }
                    } 
                } 
            }
            // message for set values
            if (valuesAssigned.size()>0){
                MessageUIBean setMessage = UIBeanFactory.getUIBeanFactory().newMessageUIBean();
                setMessage.setMessageStatus(MessageUIBean.SUCCESS);
                StringBuffer setSB = new StringBuffer();
                for (int i=0; i<valuesAssigned.size();i++){
                    String aVString = (String)valuesAssigned.get(i);
                    setSB.append(aVString);
                    if (i<valuesAssigned.size()-1){
                        setSB.append(", ");
                    }
                    if (valuesAssigned.size()== 1){
                        List csticNames = (List) value_csticPairs.get(aVString);
                        if (csticNames.size()==1){
                            String csticName = (String) csticNames.get(0);
                            Characteristic csticObject = currentInstance.getCharacteristic(csticName, showIvisibleCstics);
                            CharacteristicValue theValue = csticObject.getValue(aVString);
                            setSB.append(" - " + theValue.getLanguageDependentName());
                        }
                    }

                }
                setMessage.setMessageShortText(setSB.toString());
                setMessage.setMessageTranslateKey("ipc.searchset.values.set");           
                messages.add(setMessage);
            }
            // holds values which should be display in search/set field after action
            StringBuffer returnParam = new StringBuffer();
            // message for not assigned values
            if (valuesAssignedFailed.size()>0){
                MessageUIBean notAssignedMessage = UIBeanFactory.getUIBeanFactory().newMessageUIBean();
                notAssignedMessage.setMessageStatus(MessageUIBean.ERROR);
                StringBuffer notAssignedSB = new StringBuffer();
                for (int i=0; i<valuesAssignedFailed.size();i++){
                    String uPString = (String)valuesAssignedFailed.get(i);
                    notAssignedSB.append(uPString);
                    returnParam.append(uPString);
                    if (i<valuesAssignedFailed.size()-1){
                        notAssignedSB.append(",");
                        returnParam.append(delimiter);
                    }
                }
                notAssignedMessage.setMessageShortText(notAssignedSB.toString());   
                notAssignedMessage.setMessageTranslateKey("ipc.searchset.values.notSet");        
                messages.add(notAssignedMessage);                
                
            }
            // message for not assignable or invisible values
            if (valuesNotAssignable.size()>0){
                MessageUIBean notAssigableMessage = UIBeanFactory.getUIBeanFactory().newMessageUIBean();
                notAssigableMessage.setMessageStatus(MessageUIBean.ERROR);
                StringBuffer notAssigneabledSB = new StringBuffer();
                for (int i=0; i<valuesNotAssignable.size();i++){
                    String uPString = (String)valuesNotAssignable.get(i);
                    notAssigneabledSB.append(uPString);
                    returnParam.append(uPString);
                    if (i<valuesNotUnique.size()-1){
                        notAssigneabledSB.append(",");
                        returnParam.append(delimiter);
                    }
                }
                notAssigableMessage.setMessageShortText(notAssigneabledSB.toString());   
                notAssigableMessage.setMessageTranslateKey("ipc.searchset.values.notassignab");        
                messages.add(notAssigableMessage);
                // add delimiter if next message contains values 
                if (valuesNotUnique.size()>0){
                    returnParam.append(delimiter);               
                }                
            }
            // message for non unique values
            if (valuesNotUnique.size()>0){
                MessageUIBean uniqueMessage = UIBeanFactory.getUIBeanFactory().newMessageUIBean();
                uniqueMessage.setMessageStatus(MessageUIBean.ERROR);
                StringBuffer notUniqueSB = new StringBuffer();
                for (int i=0; i<valuesNotUnique.size();i++){
                    String uPString = (String)valuesNotUnique.get(i);
                    notUniqueSB.append(uPString);
                    returnParam.append(uPString);
                    notUniqueSB.append("(");
                    List nUCstics = (List) value_csticPairs.get(uPString);
                    for (int cNU=0; cNU<nUCstics.size();cNU++){
                        String nUcstic = (String)nUCstics.get(cNU);
                        notUniqueSB.append(nUcstic);
                        if (cNU<nUCstics.size()-1){
                            notUniqueSB.append(",");
                        }
                    }
                    notUniqueSB.append(")");
                    if (i<valuesNotUnique.size()-1){
                        notUniqueSB.append(",");
                        returnParam.append(delimiter);
                    }
                }
                uniqueMessage.setMessageShortText(notUniqueSB.toString());   
                uniqueMessage.setMessageTranslateKey("ipc.searchset.values.notUnique");        
                messages.add(uniqueMessage);
                // add delimiter if next message contains values 
                if (unknownCsticOrValue.size()>0){
                    returnParam.append(delimiter);               
                }
            }
            // message for unknown cstic and values
            if (unknownCsticOrValue.size()>0){
                MessageUIBean unknownMessage = UIBeanFactory.getUIBeanFactory().newMessageUIBean();
                unknownMessage.setMessageStatus(MessageUIBean.WARNING);
                StringBuffer unknownSB = new StringBuffer();
                for (int i=0; i<unknownCsticOrValue.size();i++){
                    String uPString = (String)unknownCsticOrValue.get(i);
                    unknownSB.append(uPString);
                    returnParam.append(uPString);
                    if (i<unknownCsticOrValue.size()-1){
                        unknownSB.append(",");
                        returnParam.append(delimiter);
                    }
                }
                if (mode.equals(SEARCH_SET_XCM_VALUE)){
                    unknownMessage.setMessageTranslateKey("ipc.searchset.values.notfound");
                } else {
                    unknownMessage.setMessageTranslateKey("ipc.searchset.unknown");                    
                }
                unknownMessage.setMessageShortText(unknownSB.toString());  
                messages.add(unknownMessage);
            }
            uiContext.setProperty(SEARCH_SET_VALUE, returnParam.toString());         
            currentConfiguration.flush();

			customerExit(mapping, form, request, response, userSessionData, requestParser, mbom, multipleInvocation, browserBack);

			setExtendedRequestAttribute(request, MessagesRequestParameterConstants.MESSAGES, messages);   
            return (mapping.findForward(SUCCESS));
        } catch (Exception e){
            MessageUIBean errorMessage = UIBeanFactory.getUIBeanFactory().newMessageUIBean();
            errorMessage.setMessageStatus(MessageUIBean.ERROR);
            errorMessage.setMessageTranslateKey("ipc.message.searchset.error");
            errorMessage.setMessageLongText(e.toString());
            messages.add(errorMessage);
			setExtendedRequestAttribute(request, MessagesRequestParameterConstants.MESSAGES, messages);   
            return (mapping.findForward(SUCCESS));            
        }
    }
    
    /**
     * 
     * @param searchString: the search sting from the browser
     * @param delimiter: the delimiter from the XCM settings
     * @param whiteSpaces: indicates whether id can contain white spaces. (from XCM settings)
     * @param currentInstance: the current instance where the search will be executed
     * @param showInvisibleCstics: UI property if invisible characteristics are shown
     * @param valuesSet: a list where all values are stored which could be set
     * @param valuesNotUnique: a list where all values are stored that are not unique
     * @param unknownParameter: a list where all values are stored which could not be found in the instance
     * @param valueCsticDependencies: a hashtable which hold a list of characteristic id by the key of the value. 
     *                          This hashtable is used find the dependencies between the values and the characteristics. 
     */
    private void searchCsticValues(String searchString, String delimiter, boolean whiteSpaces, Instance currentInstance, boolean showIvisibleCstics, List valuesSet, List valuesNotUnique, List unknownValue, Hashtable valueCsticDependencies) {
        List cstics = currentInstance.getCharacteristics(showIvisibleCstics);
        Hashtable uniqueValues = new Hashtable();
        Hashtable nonUniqueValues = new Hashtable();
        // loop over all cstics and values and create lists with unique and non unique values
        // also create a hashtable with the value-cstic dependencies
        for (int i=0; i<cstics.size(); i++){
            Characteristic cstic = (Characteristic)cstics.get(i);
            List values = cstic.getValues();
            for (int j=0; j<values.size(); j++){
                CharacteristicValue value = (CharacteristicValue)values.get(j);
				// initially value is unique, but maybe later same value is found again
                if (!nonUniqueValues.containsKey(value.getName()) && !uniqueValues.containsKey(value.getName())){
                    uniqueValues.put(value.getName(), value.getName());
                } else if (!nonUniqueValues.containsKey(value.getName()) && uniqueValues.containsKey(value.getName())){
                    uniqueValues.remove(value.getName());
                    nonUniqueValues.put(value.getName(), value.getName());
                }
                // add relation between values and cstics
                List csticsFromHash = (List)valueCsticDependencies.get(value.getName());
                if (csticsFromHash== null){
                    csticsFromHash = new Vector();
                    valueCsticDependencies.put(value.getName(), csticsFromHash);
                }
                csticsFromHash.add(cstic.getName());
            }
        }
        // loop over the search string and decide which list has to be filled
        StringTokenizer tokens = new StringTokenizer(searchString, delimiter);
        while (tokens.hasMoreElements()){
            String token = tokens.nextToken();
            if (!whiteSpaces) {
                token.trim();
            }
            if (uniqueValues.containsKey(token)){
                valuesSet.add(token);
            } else if (nonUniqueValues.containsKey(token)) {
                valuesNotUnique.add(token);
            } else {
                unknownValue.add(token);
            }
        }
    }    
}
