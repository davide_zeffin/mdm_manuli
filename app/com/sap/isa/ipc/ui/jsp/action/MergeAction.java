/*
 * Created on 07.03.2008
 *
 * To change the template for this generated file go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
package com.sap.isa.ipc.ui.jsp.action;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.jsp.JspFactory;
import javax.servlet.jsp.PageContext;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.sap.isa.businessobject.CommunicationException;
import com.sap.isa.core.Constants;
import com.sap.isa.core.UserSessionData;
import com.sap.isa.core.businessobject.management.MetaBusinessObjectManager;
import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.core.util.RequestParser;
import com.sap.isa.ipc.ui.jsp.action.visitor.IVisitableObject;
import com.sap.isa.ipc.ui.jsp.action.visitor.IVisitor;
import com.sap.isa.ipc.ui.jsp.action.visitor.MergeVisitor;
import com.sap.isa.ipc.ui.jsp.beans.CharacteristicUIBean;
import com.sap.isa.ipc.ui.jsp.beans.ConfigUIBean;
import com.sap.isa.ipc.ui.jsp.beans.GroupUIBean;
import com.sap.isa.ipc.ui.jsp.constants.CharacteristicsRequestParameterConstants;
import com.sap.isa.ipc.ui.jsp.dynamicui.IPCDynamicUIConstant;
import com.sap.isa.ipc.ui.jsp.dynamicui.IPCUIModel;
import com.sap.isa.ipc.ui.jsp.uiclass.UIContext;

/**
 * @author D048393
 *
 * This Action will trigger the Merge the Config UI Model with the UIModel.
 */
public class MergeAction
	extends IPCBaseAction
	implements RequestParameterConstants, ActionForwardConstants {

	IsaLocation log = IsaLocation.getInstance(MergeAction.class.getName());

	/* (non-Javadoc)
	 * @see com.sap.isa.isacore.action.EComBaseAction#ecomPerform(org.apache.struts.action.ActionMapping, org.apache.struts.action.ActionForm, javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse, com.sap.isa.core.UserSessionData, com.sap.isa.core.util.RequestParser, com.sap.isa.core.businessobject.management.MetaBusinessObjectManager, boolean, boolean)
	 */
	public ActionForward ecomPerform(
		ActionMapping mapping,
		ActionForm form,
		HttpServletRequest request,
		HttpServletResponse response,
		UserSessionData userSessionData,
		RequestParser requestParser,
		MetaBusinessObjectManager mbom,
		boolean multipleInvocation,
		boolean browserBack)
		throws IOException, ServletException, CommunicationException {

		UIContext uiContext = getUIContext(request);
		IPCUIModel ui =	(IPCUIModel) uiContext.getProperty(IPCDynamicUIConstant.SESSION_DYNAMIC_UI);

		// check if reset has been triggered -> clear the "old" instances in the UIModel
		if (uiContext.getPropertyAsBoolean(RequestParameterConstants.RESET_CONFIG_DONE)	== true) {
//			ui.getActivePage().clear();
			// reset the flag
			uiContext.setProperty(RESET_CONFIG_DONE, false);
		}

		// get relevant data from session
		IVisitableObject cBean = (IVisitableObject) request.getAttribute(CharacteristicsRequestParameterConstants.CURRENT_CONFIGURATION);

		// create page context (some Methods of UIBeans require page context)
		JspFactory factory = JspFactory.getDefaultFactory();
		PageContext pageContext = null;
		if (factory == null) {
			log.debug("Action is running in test mode as JspFactory is initial!");
		} else {
				pageContext = 
				factory.getPageContext(
					this.servlet, 
					request, 
					response, 
					null, // errorPageURL
					false,// needsSession
					8192, // bufferSize; todo: who defines this values normally can it be changed depending on the engine
					true); // autoFlush
		}

		//create merge visitor
		IVisitor visitor = new MergeVisitor(ui, uiContext, pageContext);

		// searchSet: pass group and cstic to visitor 
		GroupUIBean searchSetGroup = (GroupUIBean) request.getAttribute(InternalRequestParameterConstants.SEARCH_SET_FOUND_GROUP);
		if (searchSetGroup != null) {
			visitor.setSearchSetGroup(searchSetGroup);
			CharacteristicUIBean searchSetCstic = (CharacteristicUIBean) request.getAttribute(InternalRequestParameterConstants.SEARCH_SET_FOUND_CSTIC);
			if (searchSetCstic != null) {
				visitor.setSearchSetCstic(searchSetCstic);
			}
		}

		// handle expand/collapse status triggered via "Display Options" link
		String csticStatusChange = getContextValue(request, Constants.CHARACTERISTIC_STATUS_CHANGE);
		// pass name of changed cstic to visitor
		visitor.setCsticStatusChange(csticStatusChange);

		// trigger merge
		cBean.accept(visitor);

		// All pageContext-relevant data has been handed over to the DynamicUI framework-objects.
		if (factory != null) {
			factory.releasePageContext(pageContext);
		}

		// if this is the first merge we trigger the default expanding
		if ((ui != null) && (uiContext.isInitialMerge())) {
			ui.triggerDefaultExpanding(uiContext);
			uiContext.setInitialMerge(false);
		}

		// set IPCUIModel in request
		request.setAttribute(IPCDynamicUIConstant.SESSION_DYNAMIC_UI, ui);

		// trigger further processing	
		return mapping.findForward(ActionForwardConstants.SUCCESS);
	}

}
