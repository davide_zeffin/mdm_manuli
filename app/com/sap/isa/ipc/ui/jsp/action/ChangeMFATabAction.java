/**
 * The action GetInstancesAction returns a list of instances
 * If no instances are found you get an empty list of instances
 * The list of instances is stored in the request;
 * In the jsp page you can access the the list of instances
 * using the following method:
 *
 * List instances = (List)request.getAttribute(RequestParameterConstants.INSTANCES);
 *
 * The action also adds the root instance as currentInstance
 * to the UIContext.
 */
package com.sap.isa.ipc.ui.jsp.action;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.sap.isa.core.Constants;
import com.sap.isa.core.UserSessionData;
import com.sap.isa.core.businessobject.management.MetaBusinessObjectManager;
import com.sap.isa.core.util.RequestParser;
import com.sap.isa.ipc.ui.jsp.uiclass.UIContext;

/**
 * Currently just forwards to the display of the Layout.
 * Customers might add functionality here.
 */
public class ChangeMFATabAction extends IPCBaseAction implements RequestParameterConstants,
                                                              ActionForwardConstants {

	public ActionForward ecomPerform(ActionMapping mapping,
		ActionForm form,
		HttpServletRequest request,
		HttpServletResponse response,
		UserSessionData userSessionData,
		RequestParser requestParser,
		MetaBusinessObjectManager mbom,
		boolean multipleInvocation,
		boolean browserBack)
			throws IOException, ServletException {

        UIContext uiContext = getUIContext(request);
        
		String instanceId = request.getParameter(Constants.CURRENT_INSTANCE_ID);
		if (!isSet(instanceId)) {
			return parameterError(mapping, request, Constants.CURRENT_INSTANCE_ID, getClass().getName());
		}else {
			changeContextValue(request, Constants.CURRENT_INSTANCE_ID, instanceId);
		}
		
        if (!uiContext.getOnlineEvaluate() && !browserBack) {
            ActionForward forward = bufferCharacteristicValues(mapping, request, instanceId);
            if (!forward.getName().equals(SUCCESS)) {
                return forward;
            }
        }

		String selectedTabName = request.getParameter(Constants.SELECTED_MFA_TAB_NAME);
		if (!isSet(selectedTabName)) {
			return parameterError(mapping, request, Constants.SELECTED_MFA_TAB_NAME, getClass().getName());
		}
		changeContextValue(request, Constants.SELECTED_MFA_TAB_NAME, selectedTabName);

		customerExit(mapping, form, request, response, userSessionData, requestParser, mbom, multipleInvocation, browserBack);

        return processForward(mapping, request, ActionForwardConstants.SUCCESS);       
    }
    
}

