/*
 * Created on 13.01.2005
 *
 */
package com.sap.isa.ipc.ui.jsp.action;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.sap.isa.businessobject.ipc.IPCBOManager;
import com.sap.isa.core.UserSessionData;
import com.sap.isa.core.businessobject.management.MetaBusinessObjectManager;
import com.sap.isa.core.util.RequestParser;
import com.sap.isa.core.util.RequestParser.Parameter;
import com.sap.isa.ipc.ui.jsp.uiclass.UIContext;
import com.sap.spc.remote.client.object.Configuration;
import com.sap.spc.remote.client.object.IPCClient;
import com.sap.spc.remote.client.object.IPCConfigReference;
import com.sap.spc.remote.client.object.IPCSession;

/**
 * @author 
 *
 */
public class StartCRMProdSimConfigAction extends StartConfigurationAction {
	public ActionForward ecomPerform(ActionMapping mapping,
		ActionForm form,
		HttpServletRequest request,
		HttpServletResponse response,
		UserSessionData userSessionData,
		RequestParser requestParser,
		MetaBusinessObjectManager mbom,
		boolean multipleInvocation,
		boolean browserBack)
			throws IOException, ServletException {
				
				UIContext uiContext = IPCBaseAction.getUIContext(request);
				if (uiContext == null) { //the uiContext has not been initialized
					ActionForward forward = processScenarioParameter(mapping, request);
					if (!forward.getName().equals(ActionForwardConstants.SUCCESS)) return forward;
					uiContext = IPCBaseAction.getUIContext(request); //uiContext created in processScenarioParameter
				}
				super.htmlTrace(request);
			
				Parameter tokenIdParam = requestParser.getFromContext(IPC_TOKENID);
				if (!tokenIdParam.isSet()) return parameterError(mapping, request, RequestParameterConstants.IPC_TOKENID, getClass().getName());
				Parameter callerParam = requestParser.getFromContext(CALLER);
				if (!callerParam.isSet()) return parameterError(mapping, request, RequestParameterConstants.CALLER, getClass().getName());
				uiContext.setCaller(callerParam.getValue().getString());
				Parameter configIdParam = requestParser.getFromContext(CONFIG_ID);
			
				
				IPCBOManager ipcBoManager =
					(IPCBOManager) userSessionData.getBOM(IPCBOManager.NAME);
				if (ipcBoManager == null) {
					log.error("No IPC BOM in session data!");
					return (mapping.findForward(INTERNAL_ERROR));
				}
				
				Boolean logonFallback = (Boolean) request.getAttribute(LOGON_FALLBACK);
				IPCClient ipcClient;
				
				// if the SSO Logon failed, the generic JCO user stored in XCM should be used
				// rather then displaying the a login screen.
				// by passing the language to the ipcClient, we will trigger proper intilisation of
				// the connection. This includes setting of the Appserver, client and sysnumber
				// which are parsed by ISA framework and stored in backend context.
				if(logonFallback != null && logonFallback.booleanValue()){
					// No Logon happened, so there is no SSO ticket to read the language, so we
					// take language that is set in request
					Parameter languageParam = requestParser.getFromContext(LANGUAGE);
					if (!languageParam.isSet()) return parameterError(mapping, request, RequestParameterConstants.LANGUAGE, getClass().getName());
					ipcClient =
						ipcBoManager.createIPCClient(
							languageParam.getValue().getString(),
							tokenIdParam.getValue().getString(),
							configIdParam.getValue().getString());
				}else{
					// during logon, language was read from the SSO ticket and set into the
					// connection. no need to pass it here.
					ipcClient =
						ipcBoManager.createIPCClient(
							tokenIdParam.getValue().getString(),
							configIdParam.getValue().getString());
				}
			
				IPCConfigReference configReference = factory.newIPCConfigReference();
				configReference.setConfigId(configIdParam.getValue().getString());
				
				IPCSession session = ipcClient.getIPCSession();
				Configuration configuration = session.getConfiguration(configIdParam.getValue().getString());
          
				customerExit(mapping, form, request, response, userSessionData, requestParser, mbom, multipleInvocation, browserBack);

					
				uiContext.setCaller(callerParam.getValue().getString());
				uiContext.setCurrentConfiguration(configuration);
				uiContext.setProperty(InternalRequestParameterConstants.IPC_CLIENT, ipcClient);
				//initialize the context
				setInitialContextValues(request, configuration);
				
				return mapping.findForward(ActionForwardConstants.SUCCESS);
			}

}
