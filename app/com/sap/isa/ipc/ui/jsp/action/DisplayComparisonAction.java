package com.sap.isa.ipc.ui.jsp.action;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.sap.isa.businessobject.CommunicationException;
import com.sap.isa.core.Constants;
import com.sap.isa.core.UserSessionData;
import com.sap.isa.core.businessobject.management.MetaBusinessObjectManager;
import com.sap.isa.core.util.RequestParser;
import com.sap.isa.ipc.ui.jsp.beans.CharacteristicDeltaUIBean;
import com.sap.isa.ipc.ui.jsp.beans.CharacteristicUIBean;
import com.sap.isa.ipc.ui.jsp.beans.ComparisonResultUIBean;
import com.sap.isa.ipc.ui.jsp.beans.GroupDeltaUIBean;
import com.sap.isa.ipc.ui.jsp.beans.GroupUIBean;
import com.sap.isa.ipc.ui.jsp.beans.InstanceDeltaUIBean;
import com.sap.isa.ipc.ui.jsp.beans.InstanceUIBean;
import com.sap.isa.ipc.ui.jsp.beans.LoadingMessageUIBean;
import com.sap.isa.ipc.ui.jsp.beans.UIBeanFactory;
import com.sap.isa.ipc.ui.jsp.beans.ValueDeltaUIBean;
import com.sap.isa.ipc.ui.jsp.constants.ComparisonRequestParameterConstants;
import com.sap.isa.ipc.ui.jsp.uiclass.UIContext;
import com.sap.spc.remote.client.object.Characteristic;
import com.sap.spc.remote.client.object.CharacteristicGroup;
import com.sap.spc.remote.client.object.CharacteristicValue;
import com.sap.spc.remote.client.object.ComparisonResult;
import com.sap.spc.remote.client.object.Configuration;
import com.sap.spc.remote.client.object.Instance;
import com.sap.spc.remote.client.object.InstanceDelta;
import com.sap.spc.remote.client.object.LoadingMessage;
import com.sap.spc.remote.client.object.ValueDelta;
import com.sap.spc.remote.client.object.imp.DefaultInstanceDelta;
import com.sap.spc.remote.client.util.cfg_ext_cstic_val;
import com.sap.spc.remote.client.util.cfg_ext_cstic_val_seq;
import com.sap.spc.remote.client.util.cfg_ext_inst;
import com.sap.spc.remote.client.util.cfg_ext_part;
import com.sap.spc.remote.client.util.cfg_ext_part_seq;
import com.sap.spc.remote.client.util.ext_configuration;

public class DisplayComparisonAction
    extends IPCBaseAction
    implements RequestParameterConstants, ActionForwardConstants {

    UIBeanFactory beanFactory = UIBeanFactory.getInstance();

    /* (non-Javadoc)
     * @see com.sap.isa.isacore.action.EComBaseAction#ecomPerform(org.apache.struts.action.ActionMapping, org.apache.struts.action.ActionForm, javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse, com.sap.isa.core.UserSessionData, com.sap.isa.core.util.RequestParser, com.sap.isa.core.businessobject.management.MetaBusinessObjectManager, boolean, boolean)
     */
    public ActionForward ecomPerform(
        ActionMapping mapping,
        ActionForm form,
        HttpServletRequest request,
        HttpServletResponse response,
        UserSessionData userSessionData,
        RequestParser requestParser,
        MetaBusinessObjectManager mbom,
        boolean multipleInvocation,
        boolean browserBack)
        throws IOException, ServletException, CommunicationException {

        log.entering("ecomPerform()");
        UIContext uiContext = getUIContext(request);
        Configuration currentConfig = uiContext.getCurrentConfiguration();

        //required parameter for return to the configuration page
        //the comparison page will store the ids in hidden input fields
        //and submit them when returning to the configuration page
        String curInstId = getContextValue(request, Constants.CURRENT_INSTANCE_ID);
        if (!isSet(curInstId)) {
            return parameterError(
                mapping,
                request,
                Constants.CURRENT_INSTANCE_ID,
                this.getClass().getName());
        }
        Instance curInst = uiContext.getCurrentConfiguration().getInstance(curInstId);
        InstanceUIBean curInstUIBean = UIBeanFactory.getUIBeanFactory().newInstanceUIBean(curInst, uiContext);
        request.removeAttribute(CURRENT_INSTANCE);
        request.setAttribute(CURRENT_INSTANCE, curInstUIBean);
        String curCharGroupName = getContextValue(request, Constants.CURRENT_CHARACTERISTIC_GROUP_NAME);
        if (!isSet(curCharGroupName)) {
            return parameterError(
                mapping,
                request,
                Constants.CURRENT_CHARACTERISTIC_GROUP_NAME,
                this.getClass().getName());
        }
        GroupUIBean curCharGroup = curInstUIBean.getGroup(curCharGroupName);
        request.setAttribute(CURRENT_CHARACTERISTIC_GROUP, curCharGroup);
        String curCharName = getContextValue(request, Constants.CURRENT_CHARACTERISTIC_NAME);
        if (isSet(curCharName)) {
            request.setAttribute(
                CURRENT_CHARACTERISTIC,
                curInstUIBean.getCharacteristic(curCharName));
        }

        boolean showLoadingMessages = 
            uiContext.getPropertyAsBoolean(RequestParameterConstants.SHOW_LOADING_MESSAGES, true); // this flag controls 
                                                                                                   // whether loading messages
                                                                                                   // should be displayed at all

        boolean showOnlyCritical = 
            uiContext.getPropertyAsBoolean(COMPARISON_FILTER, false);   // this flag controls whether only critical 
                                                                        // messages should be displayed (i.e. only 
                                                                        // loading messages)  
        // retrieve the list of loading messages
        ArrayList loadingMessages = (ArrayList)uiContext.getProperty(LOADING_MESSAGES);
        
        HashMap loadingMessagesByInstId;
        // if the loading messages should be displayed...
        if (showLoadingMessages){
            log.debug("showLoadingMessages = "+ showLoadingMessages);
            // create LoadingMessageUIBeans and sort them by instance and config object key
            loadingMessagesByInstId = initLoadingMessagesByInstId(loadingMessages, uiContext);
        }
        else {
            log.debug("showLoadingMessages = "+ showLoadingMessages);
            // create only an empty HashMap
            loadingMessagesByInstId = new HashMap();
        }
        
        ComparisonResult resultBO = (ComparisonResult) uiContext.getProperty(COMPARISON_RESULT);
        if (resultBO == null){
            return parameterError(
                mapping,
                request,
                COMPARISON_RESULT,
                this.getClass().getName());
        }        
        ComparisonResultUIBean resultBean = beanFactory.newComparisonResultUIBean(resultBO, uiContext);
        createResultUIBean(resultBO, resultBean, loadingMessagesByInstId, currentConfig, uiContext);

        if (showOnlyCritical){
            log.debug("showOnlyCritical = "+ showOnlyCritical);
            applyFilterForCriticalMessages(resultBean);
        }

        request.setAttribute(ComparisonRequestParameterConstants.COMPARISON_RESULT, resultBean);

        customerExit(mapping, form, request, response, userSessionData, requestParser, mbom, multipleInvocation, browserBack);

        log.exiting();
        return mapping.findForward(SUCCESS);
    }

    /**
     * This method modifies the resultBean so that only critical messages (i.e.
     * loading messages) are part of it.<br>
     * Other deltas will be deleted.
     * @param resultBean resultBean that will be filtered
     */
    private void applyFilterForCriticalMessages(ComparisonResultUIBean resultBean) {
        log.entering("applyFilterForCriticalMessages(ComparisonResultUIBean resultBean)");
        List instDeltas = resultBean.getInstanceDeltas();
        ArrayList instsToBeRemoved = new ArrayList();
        for (int i=0; i<instDeltas.size(); i++){
            InstanceDeltaUIBean instDelta = (InstanceDeltaUIBean)instDeltas.get(i);
            List csticsOfInstance = instDelta.getCharacteristics();
            if (!instDelta.hasLoadingMessagesForCstics()) {
                // none of its cstics has a loading message
                if(!instDelta.hasLoadingMessages()){
                    // also the instance itself has no loading message
                    // -> remove this instance from the list of instances
                    instsToBeRemoved.add(instDelta); 
                    log.debug("Critical-Filter: Instance "+ instDelta.getId()+" removed.");
                }
                else{
                    // the instance itself has loading messages
                    // -> remove only groups and cstics of this instance
                    instDelta.setCharacteristics(new ArrayList());
                    instDelta.setGroups(new ArrayList());
                }
            }
            else {
                // instance has loading messages for cstics -> walk through and delete 
                List groupDeltas = instDelta.getGroups();
                ArrayList groupsToBeRemoved = new ArrayList();
                for (int j=0; j<groupDeltas.size(); j++){
                    GroupDeltaUIBean groupDelta = (GroupDeltaUIBean) groupDeltas.get(j);
                    List csticDeltas = groupDelta.getCharacteristics();
                    ArrayList csticsToBeRemoved = new ArrayList();
                    for (int k=0; k<csticDeltas.size(); k++){
                        CharacteristicDeltaUIBean csticDelta = (CharacteristicDeltaUIBean)csticDeltas.get(k);
                        if (!csticDelta.hasLoadingMessages()){
                            // no loading message -> mark for removal
                            csticsToBeRemoved.add(csticDelta);
                            log.debug("Critical-Filter: Cstic "+ csticDelta.getName()+" removed.");
                        }
                    }
                    csticDeltas.removeAll(csticsToBeRemoved);
                    csticsOfInstance.removeAll(csticsToBeRemoved);
                    
                    if (!groupDelta.hasCharacteristicDeltas()){
                        // group is empty -> remove it
                        groupsToBeRemoved.add(groupDelta);
                        log.debug("Critical-Filter: Group "+ groupDelta.getName()+" removed.");
                    }
                }
                groupDeltas.removeAll(groupsToBeRemoved);
            }
        }
        instDeltas.removeAll(instsToBeRemoved);
        log.exiting();
    }

    /**
     * Creates the hierarchy of delta UIBeans.
     * @param resultBO the comparison result business object
     * @param resultBean the ComparisonResultUIBean
     * @param loadingMessagesByInstId
     * @param currentConfig currentConfiguration
     * 
     */
    private void createResultUIBean(
        ComparisonResult resultBO,
        ComparisonResultUIBean resultBean, 
        HashMap loadingMessagesByInstId, 
        Configuration currentConfig,
        UIContext uiContext){
        log.entering("createResultUIBean()");
        boolean showInvisible = uiContext.getShowInvisibleCharacteristics();
        boolean showAllCsticsWithAssignedValues = 
            uiContext.getPropertyAsBoolean(COMPARISON_SHOW_ALL_ASSIGNED, false);   // this flag controls, whether all cstics  
                                                                                   // with assigned values should be displayed   
                                                                                   // even if they don't have deltas 
        List instances = currentConfig.getInstances();
        // loop over instances
        for (int i=0; i<instances.size(); i++){
            Instance currentInst = (Instance)instances.get(i);

            // the loading messages of this instance
            HashMap loadingMessagesOfInstance = (HashMap)loadingMessagesByInstId.get(currentInst.getId());
            
            // try to get the instanceDelta for this instance
            InstanceDelta instDelta = (InstanceDelta)resultBO.getInstanceDeltasById2().get(currentInst.getId());  
            if (instDelta == null){
                instDelta = DefaultInstanceDelta.C_NULL;
            }
            InstanceDeltaUIBean instUIBean =  beanFactory.newInstanceDeltaUIBean(instDelta, currentInst, uiContext);            

            List groups = currentInst.getCharacteristicGroups();
            // loop over the groups to find out which have to be displayed
            for (int j=0; j<groups.size(); j++){
                CharacteristicGroup currentGroup = (CharacteristicGroup)groups.get(j);
                createGroupDeltaUIBean(
                        resultBO,
                        loadingMessagesOfInstance,
                        uiContext,
                        instUIBean,
                        currentGroup,
                        instDelta);                
            }
            // check for loading messages of this instance
            if (loadingMessagesOfInstance != null){
                ArrayList loadingMessages = (ArrayList)loadingMessagesOfInstance.get(currentInst.getId());
                if (loadingMessages != null){
                    log.debug("There are " + loadingMessages.size() + " loading messages for instance "+ currentInst.getId() +".");
                    // only add the messages to instUIBean and the instUIBean to the result if there are messages
                    instUIBean.setLoadingMessages(loadingMessages);
                    // remove this loading message from the HashMap loadingMessagesOfInstance 
                    // (in order to find out which loadingMessages have been created)
                    if (loadingMessagesOfInstance != null){
                        loadingMessagesOfInstance.remove(currentInst.getId());
                    }                       
                }
                
                // capture remaining loading messages of this instance
                captureRemainingLoadingMessagesOfInstance(loadingMessagesOfInstance, instUIBean);

                ArrayList messages = instUIBean.getLoadingMessages();
                if (messages != null){
                    for (int j=0; j<messages.size(); j++){
                        LoadingMessageUIBean currentMsg = (LoadingMessageUIBean) messages.get(j);
                        // If it is a message for a deleted cstic (msg no. 241) then we try to 
                        // add additional information (set values of the deleted cstic)                       
                        addInfoForMsg241(resultBO, currentInst, currentMsg);
                    }
                }
                
                // all loading messages for this instance should have been created so delete the entry in the map
                loadingMessagesByInstId.remove(currentInst.getId());
            }
            
            // only add the instUIBean to the result if ... 
            //  - there are messages OR
            //  - there are UIBeans (cstics/csticDeltas) OR 
            //  - it is an inserted instance (the user should see that it has been added
            //    also if it does not have any assigned values at the moment) OR
            //  - showAllCsticsWithAssignedValues == true (all instances are displayed anyway)
            if (instUIBean.hasLoadingMessages() 
                || instUIBean.getCharacteristics().size()>0
                || instUIBean.getType().equals(InstanceDelta.INSERTED)
                || showAllCsticsWithAssignedValues) {
                    
                log.debug("Add InstanceDeltaUIBean "+ instUIBean.getId() +" to result.");
                resultBean.addInstanceDelta(instUIBean);
                // set flag to true if one of the instance's cstics has a loading message or 
                // if the instance itself has a loading message 
                if (instUIBean.hasLoadingMessagesForCstics() || instUIBean.hasLoadingMessages()){
                    resultBean.setAnyLoadingMessagesFlag(true);
                }
                // set flag to true if the instance has value deltas or characteristics respectively
                if (instUIBean.getCharacteristics().size()>0){
                    resultBean.setAnyValueDeltasFlag(true); 
            	}                
        	}
        }

        // Add the DELETED instances.
        // Remark: by definition a deleted instance should not have loading messages -> it was not loaded anymore 
        List deletedInstances = resultBO.getDeletedInstances();
        // We want to add the deleted instances at the right position (if possible).
        // Therefore we need the information to which parent the instances in extConfig1 belonged to.
        ext_configuration extConfig1 = resultBO.getConfigOne();
        HashMap parentForChild = createChildParentAssociations(deletedInstances, extConfig1);
                
        for (int i=0; i<deletedInstances.size(); i++){
            InstanceDelta deletedInst = (InstanceDelta) deletedInstances.get(i);
            InstanceDeltaUIBean instUIBean = createDeletedInstanceDeltaUIBean(resultBO, uiContext, deletedInst);
            // add the instanceDeltaUIBean to the result
            if (instUIBean != null){
                // try to add the deleted instance to the resultBean at the right position         
                addDeletedInstance(resultBean, parentForChild, instUIBean);
                // set flag to true if one of the instance's cstics has a loading message or 
                // if the instance itself has a loading message 
                if (instUIBean.hasLoadingMessagesForCstics() || instUIBean.hasLoadingMessages()){
                    resultBean.setAnyLoadingMessagesFlag(true);
                }                
            }
        }
        
        // Finally we have to check the loadingMessagesByInstId map whether there are still messages that  
        // have not been associated to a deltaUIBean.       
        captureUnassociatedLoadingMessages(resultBean, loadingMessagesByInstId);
        log.exiting();
    }

    /**
     * Message no. 241 is used if a cstic has been deleted. In this case we want to
     * add the values that have been set for this cstic as additional information for
     * the user.<br>
     * Therefore we use the external configuration one (i.e. the old config) where
     * the values exist for this cstic.<br>
     * Only the internal names are available because on the database only the these are
     * stored. 
     * @param resultBO result BO (contains the extConfig1)
     * @param currentInst 
     * @param currentMsg
     */
    private void addInfoForMsg241(
        ComparisonResult resultBO,
        Instance currentInst,
        LoadingMessageUIBean currentMsg) {
        if (currentMsg != null && currentMsg.getNumber() == 241) {
            // get the old configuration
            ext_configuration oldConfig = resultBO.getConfigOne();
            // get the instance data from the old configuration
            Integer instId = new Integer(currentInst.getId());
            if (oldConfig != null){
                cfg_ext_inst extInst = oldConfig.get_inst(instId);
                // get the cstic name from the loading message 
                String deletedCsticName = currentMsg.getArguments()[0];
                if (deletedCsticName != null && !deletedCsticName.equals("")){
                    ArrayList oldValues = getExtVals(deletedCsticName, extInst);
                    currentMsg.setOldValues(oldValues); 
                }
            }
        }
    }

    /**
     * Helper Method.<br>
     * Tries to add the deleted instance at the right position.<br>
     * Therefore it takes the child->parent associations of extConfig1 into account.<br>
     * In a first step it retrieves the instance id of the parent. Then it loops over the 
     * already existing instance deltas of the resultBean and tries to find the position of
     * parent. If it was successful and the parent is not the last element, it adds it to the 
     * right position. Otherwise it just adds the deleted instance at the end.<br>
     * Remark:<br>
     * This works fine for configuration comparisons of snapshots (during the same session) but
     * it could be faulty for configuration comparisons to stored configurations.<br>
     * Example:<br>
     * If the instance ABC has the ID "3" in the stored config one can not assume that instance ABC
     * has also instance ID "3" in the current configuration. If this is not the case the deleted 
     * instance would be displayed at the wrong position.<br>
     * Possible workaround that could be implemented:<br>
     * Only search for the right position of the deleted instance if it was a comparison to snapshots 
     * (same session). If it was a comparison to a stored configuration always put deleted instances at 
     * the end. But this is not implemented up to now.
     * @param resultBean
     * @param parentForChild
     * @param instUIBean
     */
    private void addDeletedInstance(
        ComparisonResultUIBean resultBean,
        HashMap parentForChild,
        InstanceDeltaUIBean instUIBean) {
        String deletedInstId = instUIBean.getId();
        String parentInstId = (String) parentForChild.get(deletedInstId);
        log.entering("addDeletedInstance");
        int parentPosition = -1;
        if (parentInstId != null){
            // find the position of the parent
            for (int j=0; j<resultBean.getInstanceDeltas().size(); j++){
                InstanceDeltaUIBean currentInstDeltaBean = (InstanceDeltaUIBean) resultBean.getInstanceDeltas().get(j);
                if (currentInstDeltaBean.getId().equals(parentInstId)){
                    // this is the parent
                    parentPosition = j;
                    break;
                }
            }
        }
        if (parentPosition == -1 || (parentPosition+1 == resultBean.getInstanceDeltas().size())){
            // parent couldn't be found or the parent is at the end of the list -> just add it at the end
            resultBean.addInstanceDelta(instUIBean);                    
        }
        else {
            // parent could be found -> add the deleted instance one position after the parent
            resultBean.addInstanceDelta(instUIBean, parentPosition+1);
        }
        log.exiting();
    }

    /**
     * Helper method.<br>
     * Creates a <code>HashMap</code> with child->parent associations on basis of external config 1.<br>
     * That means that you get a <code>HashMap</code> that has the children Ids as keys and the parent IDs
     * as values.
     * @param deletedInstances list of deleted instances
     * @param extConfig1 the external configuration 1
     * @return <code>HashMap</code> with child->parent associations
     */
    private HashMap createChildParentAssociations(
        List deletedInstances,
        ext_configuration extConfig1) {

        HashMap parentForChild = new HashMap();
        if (deletedInstances.size()>0){
            // get the part information
            cfg_ext_part_seq parts = extConfig1.get_parts();
            Iterator partsIter = parts.iterator();
            while (partsIter.hasNext()){
                cfg_ext_part part = (cfg_ext_part) partsIter.next();
                String childInstId = part.get_inst_id().toString();
                String parentInstId = part.get_parent_id().toString();
                // child->parent association
                parentForChild.put(childInstId, parentInstId);
            }
        }
        return parentForChild;
    }

    /**
     * Collects the loading messages that could not associated to an appropriate 
     * config object (instance, cstic) so far.<br>
     * It adds the messages to the list of loading messages of the resultBean.
     * @param resultBean 
     * @param loadingMessagesByInstId
     */
    private void captureUnassociatedLoadingMessages(
            ComparisonResultUIBean resultBean,
            HashMap loadingMessagesByInstId) {

            log.entering("captureUnassociatedLoadingMessages()");
            if (loadingMessagesByInstId != null && loadingMessagesByInstId.size()>0){
                log.debug("There are loading message that could not associated to an appropriate config object. We add them to the resultBean");
                //Iterator instIds = loadingMessagesByInstId.keySet().iterator();

                //Sorting the keyset using collections
                List sortedKeySet = new ArrayList(loadingMessagesByInstId.keySet());
                Collections.sort(sortedKeySet,new Comparator() {

        			public int compare(Object left, Object right) {

        				int leftKey = Integer.parseInt((String)left);

        				int rightKey = Integer.parseInt((String)right);

        				return (leftKey<rightKey)?0:1;

        			}

        		});

                Iterator instIds = sortedKeySet.iterator();
                while (instIds.hasNext()){
                    String currentInstId = (String) instIds.next();
                    HashMap messagesForInstId = (HashMap) loadingMessagesByInstId.get(currentInstId);
                    Iterator keys = messagesForInstId.keySet().iterator();
                    while (keys.hasNext()){
                        String key = (String) keys.next();
                        ArrayList messagesForKey = (ArrayList) messagesForInstId.get(key);
                        for (int p=0; p<messagesForKey.size(); p++){
                            LoadingMessageUIBean msg = (LoadingMessageUIBean)messagesForKey.get(p);
                            resultBean.addLoadingMessage(msg);
                        }
                    }
                }
            }
            log.exiting();
        }

    /**
     * This method adds the remaining LoadingMessageUIBeans to the instanceDeltaUIBean which haven't been 
     * associated to a delta bean so far.
     * @param loadingMessagesOfInstance HashMap containing the loading messages which haven't been associated to a delta bean 
     * @param instUIBean instanceDeltaUIBean 
     */
    private void captureRemainingLoadingMessagesOfInstance(
        HashMap loadingMessagesOfInstance,
        InstanceDeltaUIBean instUIBean) {

        log.entering("captureRemainingLoadingMessagesOfInstance()");            
        if (loadingMessagesOfInstance != null && loadingMessagesOfInstance.size()>0){
            // if there are still loading messages for this instance we add them here
            log.debug("There are still loading messages for instance "+ instUIBean.getId()+". We add them.");

            Iterator loadingMessagesKeys = loadingMessagesOfInstance.keySet().iterator();
            
            ArrayList messageKeys = new ArrayList();
            
            while (loadingMessagesKeys.hasNext()){
              String key = (String) loadingMessagesKeys.next();
              
              messageKeys.add(key);
              
              ArrayList messagesForKey = (ArrayList) loadingMessagesOfInstance.get(key);
              for (int p=0; p<messagesForKey.size(); p++){
                  LoadingMessageUIBean msg = (LoadingMessageUIBean)messagesForKey.get(p);
                  instUIBean.addLoadingMessage(msg);
              }
            }              
            for(int i = 0 ; i < messageKeys.size();i++ ){
            	String key = (String) messageKeys.get(i);
            	loadingMessagesOfInstance.remove(key);
            } 
        }
        log.exiting();
    }

    /**
     * Creates csticDeltaUIBean in the case there are no deltas for the current instance.<br>
     * Why? If the cstic has a loading message or if the flag showAllCsticsWithAssignedValues is
     * true we have to create a csticDeltaUIBean for the cstic even if it does not have a delta.
     * @param loadingMessagesOfInstance
     * @param uiContext
     * @param showAllCsticsWithAssignedValues
     * @param instUIBean
     * @param groupDeltaUIBean
     * @param currentCstic
     */
    private void createCsticDeltaUIBeanForLoadingMessages(
        HashMap loadingMessagesOfInstance,
        UIContext uiContext,
        boolean showAllCsticsWithAssignedValues,
        InstanceDeltaUIBean instUIBean,
        GroupDeltaUIBean groupDeltaUIBean,
        Characteristic currentCstic) {
            
        log.entering("createCsticDeltaUIBeanForLoadingMessages()");
        CharacteristicDeltaUIBean csticUIBean = beanFactory.newCharacteristicDeltaUIBean(currentCstic, uiContext);
        // check for loading messages for this cstic
        String cfgObjKey = generateConfigObjectKey(currentCstic);
        if (loadingMessagesOfInstance != null){
            ArrayList loadingMessages = (ArrayList)loadingMessagesOfInstance.get(cfgObjKey);
            if (loadingMessages != null){
                // it has loading messages
                // add the loading messages to the bean
                log.debug(cfgObjKey + " has loading messages.");
                csticUIBean.setLoadingMessages(loadingMessages);
                // set the flag at the instanceDeltaUIBean to true
                instUIBean.setLoadingMessagesForCstics(true);                
                // remove this loading message from the HashMap loadingMessagesOfInstance 
                // (in order to find out which loadingMessages have been created)
                if (loadingMessagesOfInstance != null){
                    loadingMessagesOfInstance.remove(cfgObjKey);
                }
            }
        }
        if (showAllCsticsWithAssignedValues == true){
            // cstics with assigned values should be displayed even if they don't have deltas
            List assignedValues = currentCstic.getAssignedValues();
            // loop over assigned values of this cstic
            for (int m=0; m<assignedValues.size(); m++){
                log.debug(cfgObjKey + " has assigned values and showAll-Flag is " + showAllCsticsWithAssignedValues);
                CharacteristicValue val = (CharacteristicValue) assignedValues.get(m);
                ValueDeltaUIBean valueUIBean = beanFactory.newValueDeltaUIBean(val, uiContext);
                csticUIBean.addValue(valueUIBean);                            
            }
        }                        
        // only add the csticUIBean if it has loading messages or values assigned
        if(csticUIBean.hasLoadingMessages() || csticUIBean.getValues().size()>0){
            groupDeltaUIBean.addCharacteristic(csticUIBean);
            instUIBean.addCharacteristic(csticUIBean);
        }
        log.exiting();
    }


    /**
     * This method creates a CharacteristicDeltaUIBean for a passed cstic BO.<br>
     * It checks for loading messages for this cstic and adds them.<br>
     * It adds the UIBean to the groupUIBean and the instanceUIBean.<br>
     * If there are no deltas/loading messages for this cstic BO and also cstics with no deltas should
     * not be displayed the bean would not be added.
     * @param uiContext
     * @param instUIBean 
     * @param extInst
     * @param loadingMessagesByConfigObjKey HashMap of loadingMessages sorted by config object key
     * @param loadingMessagesOfInstance
     * @param currentInst
     * @param groupDeltaUIBean
     * @param currentCstic current cstic business object
     */
    private void createCsticDeltaUIBeanForInsertedInstance(
        UIContext uiContext,
        InstanceDeltaUIBean instUIBean,
        cfg_ext_inst extInst,
        HashMap loadingMessagesOfInstance,
        GroupDeltaUIBean groupDeltaUIBean,
        Characteristic currentCstic) {
        
        log.entering(" createCsticDeltaUIBeanForInsertedInstance()");
        CharacteristicDeltaUIBean csticUIBean = null;   
        if (currentCstic.hasAssignedValues()){
            log.debug(currentCstic.getName() + " has assigned values.");
            csticUIBean = beanFactory.newCharacteristicDeltaUIBean(currentCstic, uiContext);
            List assignedValues = currentCstic.getAssignedValues();
            for (int l=0; l<assignedValues.size(); l++){
                CharacteristicValue val = (CharacteristicValue) assignedValues.get(l);
                // create an "inserted" valueDelta for this value
                String csticName = currentCstic.getName();
                String valueName = val.getName();
                String configObjKey = generateConfigObjectKey(currentCstic);
                // get the author information from extInst
                cfg_ext_cstic_val extVal = findExtVal(csticName, valueName, extInst);
                String author = ""; 
                if (extVal != null){
                    author = extVal.get_author();
                }
                String type = ""; 
                ValueDelta insertedValueDelta = factory.newValueDelta(configObjKey, csticName, valueName, type, author, ValueDelta.INSERTED);
                ValueDeltaUIBean valueUIBean = beanFactory.newValueDeltaUIBean(insertedValueDelta, val, uiContext);
                csticUIBean.addValue(valueUIBean);
            }
        }
        else {
            // skip this cstic
        }        
        // check for loading messages for this cstic
        String cfgObjKey = generateConfigObjectKey(currentCstic);
        if (loadingMessagesOfInstance != null){
            ArrayList loadingMessages = (ArrayList)loadingMessagesOfInstance.get(cfgObjKey);
            if (loadingMessages != null){
                log.debug(cfgObjKey + " has loading messages.");
                // it has loading messages
                // add the loading messages to the bean
                if (csticUIBean == null){
                    csticUIBean =  beanFactory.newCharacteristicDeltaUIBean(currentCstic, uiContext);
                }
                csticUIBean.setLoadingMessages(loadingMessages);
                // set the flag at the instanceDeltaUIBean to true
                instUIBean.setLoadingMessagesForCstics(true);                
                // remove this loading message from the HashMap loadingMessagesOfInstance 
                // (in order to find out which loadingMessages have been created)
                if (loadingMessagesOfInstance != null){
                    loadingMessagesOfInstance.remove(cfgObjKey);
                }            
            }
        }

        // only add the csticUIBean if it has been generated
        if(csticUIBean != null){
            groupDeltaUIBean.addCharacteristic(csticUIBean);
            instUIBean.addCharacteristic(csticUIBean);
        }
        log.exiting();
    }

    /**
     * Find the cfg_ext_cstic_val object in the extInst for a given csticName and valueName.
     * @param csticName
     * @param valueName
     * @param extInst
     * @return Found cfg_ext_cstic_val or null if not found
     */
    private cfg_ext_cstic_val findExtVal(String csticName, String valueName, cfg_ext_inst extInst) {
        cfg_ext_cstic_val returnVal = null;
        cfg_ext_cstic_val_seq values = extInst.get_cstics_values();
        for (int i=0; i<values.size(); i++){
            cfg_ext_cstic_val extVal = (cfg_ext_cstic_val)values.get(i);
            if (extVal.get_charc().equals(csticName) && extVal.get_value().equals(valueName)){
                returnVal = extVal;
                break;
            }
        }
        return returnVal;
    }

    /**
     * Find the values in the extInst for a given csticName.
     * @param csticName
     * @param extInst
     * @return ArrayList with values (internal names only)
     */
    private ArrayList getExtVals(String csticName, cfg_ext_inst extInst) {
        ArrayList returnValues = new ArrayList();
        cfg_ext_cstic_val_seq values = extInst.get_cstics_values();
        for (int i=0; i<values.size(); i++){
            cfg_ext_cstic_val extVal = (cfg_ext_cstic_val)values.get(i);
            if (extVal.get_charc().equals(csticName)){
                returnValues.add(extVal.get_value());
            }
        }
        return returnValues;
    }

    /**
     * Creates an InstanceDeltaUIBean for a given "deleted" InstanceDelta.
     * It generates the CsticDeltaUIBeans and ValueDeltaUIBeans.
     * Special treatment for "deleted" instanceDeltas is necessary because
     * AP-CFG does not return ValueDeltas for deleted instances (because all 
     * values have been deleted by definition). We have to create them on our
     * own. Master data is not available anymore, so only the internal names are 
     * displayed (also no group information is available).
     * @param resultBO result business object
     * @param uiContext
     * @param deletedInst InstanceDelta BO of the deleted instance
     * @return InstanceDeltaUIBean for the deleted instance delta BO
     */
    private InstanceDeltaUIBean createDeletedInstanceDeltaUIBean(
        ComparisonResult resultBO,
        UIContext uiContext,
        InstanceDelta deletedInst) {

        log.entering("createDeletedInstanceDeltaUIBean()");
        // AP-CFG does not return ValueDeltas for deleted instances (because all values have been deleted
        // by definition).
        // We have to loop over the external configuration 1. Master data is not available anymore, 
        // so only the internal names are displayed (also no group information is available).
        Integer instId = new Integer(deletedInst.getInstId1());
        ext_configuration extConfig1 = resultBO.getConfigOne();
        // retrieve the information of the deleted instance
        cfg_ext_inst extInst = extConfig1.get_inst(instId);
        String internalName = extInst.get_obj_key();        
        String internalLName = extInst.get_obj_txt();
        InstanceDeltaUIBean instUIBean = beanFactory.newInstanceDeltaUIBean(deletedInst, internalName, internalLName, uiContext);
        // create a GroupDeltaUIBean with no BO
        GroupDeltaUIBean groupDeltaUIBean = beanFactory.newGroupDeltaUIBean(uiContext);
        
        // get the cstics with assigned values
        cfg_ext_cstic_val_seq values = extInst.get_cstics_values();
        CharacteristicDeltaUIBean previousCstic = null;
        for (int j=0; j<values.size(); j++){
            cfg_ext_cstic_val value = (cfg_ext_cstic_val) values.get(j);
            String csticName = value.get_charc();
            String csticLName = value.get_charc_txt();
            String valueName = value.get_value();
            String valueLName = value.get_value_txt();
            if ((previousCstic != null) && (previousCstic.getName().equals(csticName))){
                // cstic exists, just add a new value delta
                // create new ValueDeltaUIBean with no BO
                ValueDeltaUIBean valueDeltaUIBean = beanFactory.newValueDeltaUIBean(valueName, valueLName, uiContext);
                // add the value delta to the csticDelta
                previousCstic.addValue(valueDeltaUIBean);
            }
            else {
                // cstic not existing: create a new CsticDeltaUIBean with no BO
                CharacteristicDeltaUIBean csticDeltaUIBean = beanFactory.newCharacteristicDeltaUIBean(csticName, csticLName, uiContext);
                // create new ValueDeltaUIBean with no BO
                ValueDeltaUIBean valueDeltaUIBean = beanFactory.newValueDeltaUIBean(valueName, valueLName, uiContext);
                // add the value delta to the csticDelta
                csticDeltaUIBean.addValue(valueDeltaUIBean);
                // add the cstic delta to the instance Delta and the group delta
                instUIBean.addCharacteristic(csticDeltaUIBean);
                groupDeltaUIBean.addCharacteristic(csticDeltaUIBean);
                previousCstic = csticDeltaUIBean;
            }
        }
        if (groupDeltaUIBean.hasCharacteristicDeltas()){
            instUIBean.addGroup(groupDeltaUIBean);
        }
        log.exiting();
        return instUIBean;
    }

    /**
     * This method creates a GroupDeltaUIBean for the current group BO (business object).
     * It only adds the UIBean to the instanceUIBean if it contains Characteristic(Delta)UIBeans.
     * @param resultBO result business object
     * @param loadingMessagesByInstId HashMap of loadingMessages of this instance
     * @param uiContext 
     * @param instUIBean instanceDeltaUIBean the group belongs to
     * @param currentGroup current group BO
     * @param instDelta instance delta BO
     */
    private void createGroupDeltaUIBean(
        ComparisonResult resultBO,
        HashMap loadingMessagesOfInstance,
        UIContext uiContext,
        InstanceDeltaUIBean instUIBean,
        CharacteristicGroup currentGroup,
        InstanceDelta instDelta) {

        log.entering("createGroupDeltaUIBean()");
        boolean showInvisible = uiContext.getShowInvisibleCharacteristics();
        boolean showAllCsticsWithAssignedValues = 
            uiContext.getPropertyAsBoolean(COMPARISON_SHOW_ALL_ASSIGNED, false);   // this flag controls, whether all cstics  
                                                                                   // with assigned values should be displayed   
                                                                                   // even if they don't have deltas 
        
        boolean showMessageCstics = uiContext.getPropertyAsBoolean(ProcessRequestParameterAction.SHOW_MESSAGE_CSTICS);
        String prefix = uiContext.getPropertyAsString(ProcessRequestParameterAction.MESSAGE_CSTICS_PREFIX);
        
        log.debug("showInvisible: " + showInvisible + "; showAllCsticsWithAssignedValues: " + showAllCsticsWithAssignedValues);
        Instance currentInst = instUIBean.getBusinessObject();
        GroupDeltaUIBean groupDeltaUIBean = beanFactory.newGroupDeltaUIBean(currentGroup, uiContext);                
        // list of cstic BOs
        List cstics = currentGroup.getCharacteristics(showInvisible);
        // loop over cstics to find out which have to be displayed
        for (int k=0; k<cstics.size(); k++){
            Characteristic currentCstic = (Characteristic)cstics.get(k);
            if (showMessageCstics && currentCstic.getName().startsWith(prefix)) {
                continue; //ignore this cstic, because it is a message cstic
            }
            else{
                // we have to differentiate between the different types of instance deltas
                if ((instDelta == DefaultInstanceDelta.C_NULL)){
                    log.debug("instDelta type: DefaultInstanceDelta.C_NULL");
                    // there are NO DELTAS for this instance 
                    // (but maybe we have to create beans because loadingMessages exist or 
                    // the flag showAllCsticsWithAssignedValues is true) 
                    createCsticDeltaUIBeanForLoadingMessages(
                        loadingMessagesOfInstance,
                        uiContext,
                        showAllCsticsWithAssignedValues,
                        instUIBean,
                        groupDeltaUIBean,
                        currentCstic);
                }
                else if (instDelta.getType().equals(InstanceDelta.INSERTED)) {
                    log.debug("instDelta type: INSERTED");
                    // Instance has been INSERTED.
                    // AP-CFG does not return ValueDeltas for inserted instances (because all values have been 
                    // inserted by definition).
                    // We have to loop over the assigned values of this instance and create "inserted" ValueUIBeans.
    
                    // we have to retrieve the author information of the value deltas from the extConfig2
                    Integer instId = new Integer(currentInst.getId());
                    ext_configuration extConfig2 = resultBO.getConfigTwo();        
                    cfg_ext_inst extInst = extConfig2.get_inst(instId);
                    createCsticDeltaUIBeanForInsertedInstance(
                        uiContext,
                        instUIBean,
                        extInst,
                        loadingMessagesOfInstance,
                        groupDeltaUIBean,
                        currentCstic);
                }
                else {
                    log.debug("instDelta type: UPDATED");
                    // Instance has been UPDATED
                    // We have to walk through the hierarchy to create deltaUIBeans or normal UIBeans
                    createCsticDeltaUIBean(
                        resultBO,
                        loadingMessagesOfInstance,
                        uiContext,
                        showAllCsticsWithAssignedValues,
                        instUIBean,
                        groupDeltaUIBean,
                        currentCstic);
                }
            }
        }                
        
        // only add the groupDeltaUIBean to the instanceDeltaUIBean if it has Cstic(Delta)UIBeans
        if (groupDeltaUIBean.getCharacteristics().size()>0){
            instUIBean.addGroup(groupDeltaUIBean);
        }
        log.exiting();
    }

    
    /**
     * This method creates a CharacteristicUIBean or CharacteristicDeltaUIBean depending on 
     * the passed cstic BO (business object).<br>
     * It only adds the UIBean if:
     * <ul>
     * <li>the cstic BO has deltas or loading messages (CharacteristicDeltaUIBean is created).
     * <li>the cstic BO does not have deltas/loading messages and if the flag "showAllCsticsWithAssignedValues"
     * is true (CharacteristicUIBean is created).
     * </ul>
     * If there are no deltas/loading messages for this cstic BO and also cstics with no deltas should
     * not be displayed (flag "showAllCsticsWithAssignedValues" is false) this method would not add
     * a UIBean to the GroupDeltaUIBean and the InstanceDeltaUIBean. 
     * return <code>null</code>.
     * @param resultBO business object of the comparison result
     * @param loadingMessagesByConfigObjKey HashMap of loadingMessages sorted by config object key
     * @param loadingMessagesOfInstance 
     * @param uiContext 
     * @param showAllCsticsWithAssignedValues flag whether cstics with assigned values should 
     * be displayed even if they don't have deltas 
     * @param currentCstic current cstic business object
     */
    private void createCsticDeltaUIBean(
        ComparisonResult resultBO,
        HashMap loadingMessagesOfInstance,
        UIContext uiContext,
        boolean showAllCsticsWithAssignedValues,
        InstanceDeltaUIBean instUIBean,
        GroupDeltaUIBean groupDeltaUIBean,
        Characteristic currentCstic) {

        log.entering("createCsticDeltaUIBean()");
        CharacteristicUIBean csticUIBean = null;
        // 1. get the value deltas for this cstic
        String cfgObjKey = generateConfigObjectKey(currentCstic);
        log.debug("cfgObjKey: " + cfgObjKey);
        ArrayList vDeltas = (ArrayList) resultBO.getValueDeltasByConfigObjectKey().get(cfgObjKey);
        if (vDeltas != null){
            // it has value deltas
            log.debug(cfgObjKey + " has value deltas"); 
            // create a csticDeltaUIBean
            csticUIBean =  beanFactory.newCharacteristicDeltaUIBean(currentCstic, uiContext);
            // create the valueDeltaUIBeans
            condenseValueDeltas((CharacteristicDeltaUIBean)csticUIBean, vDeltas, uiContext);
        }
        // 2. get loading messages for this cstic
        if (loadingMessagesOfInstance != null) {
            ArrayList loadingMessages = (ArrayList)loadingMessagesOfInstance.get(cfgObjKey);
            if (loadingMessages != null){
                // it has loading messages
                log.debug(cfgObjKey + " has loading messages");
                // create a csticDeltaUIBean if it has not been created in step 1.
                if (csticUIBean == null){
                    csticUIBean =  beanFactory.newCharacteristicDeltaUIBean(currentCstic, uiContext);
                }
                // add the loading messages to the bean
                ((CharacteristicDeltaUIBean)csticUIBean).setLoadingMessages(loadingMessages);
                // set the flag at the instanceDeltaUIBean to true
                instUIBean.setLoadingMessagesForCstics(true);
                // remove this loading message from the HashMap loadingMessagesOfInstance 
                // (in order to find out which loadingMessages have been created)
                if (loadingMessagesOfInstance != null){
                    loadingMessagesOfInstance.remove(cfgObjKey);
                }                       
                
                // if all cstics with assigned values should be displayed (even if they don't 
                // have deltas) -> create the valueUIBean(s), but only if no valueBeans have been
                // created in step 1 (otherwise an inserted deltaValue would be added twice)
                if (showAllCsticsWithAssignedValues && csticUIBean.getValues().size()<1){
                    List assignedValues = currentCstic.getAssignedValues();
                    for (int i=0; i<assignedValues.size(); i++){
                        CharacteristicValue val = (CharacteristicValue) assignedValues.get(i);
                        ValueDeltaUIBean valueUIBean = beanFactory.newValueDeltaUIBean(val, uiContext);
                        csticUIBean.addValue(valueUIBean);
                    }                
                }
            }
        }
        // 3. if csticUIBean is still null and if all cstics with assigned values should 
        //     be displayed (even if they don't have deltas/loadingMessages) 
        //     -> create a csticDeltaUIBean
        if ((csticUIBean == null) && (showAllCsticsWithAssignedValues)){
            // only create the csticUIBean if the current cstic has assigned values
            if (currentCstic.hasAssignedValues()){
                log.debug(cfgObjKey + " has assigned values and showAll-Flag is " + showAllCsticsWithAssignedValues);
                csticUIBean = beanFactory.newCharacteristicDeltaUIBean(currentCstic, uiContext);
                List assignedValues = currentCstic.getAssignedValues();
                for (int i=0; i<assignedValues.size(); i++){
                    CharacteristicValue val = (CharacteristicValue) assignedValues.get(i);
                    ValueDeltaUIBean valueUIBean = beanFactory.newValueDeltaUIBean(val, uiContext);
                    csticUIBean.addValue(valueUIBean);
                }                
            }
            else {
                // don't create the csticUIBean because the current cstic has no assigned values
            }
        }
        else {
            // nothing has to be done, because csticUIBean has already been initialized 
            // and therefore it will be displayed
        }
        // only add the csticUIBean if it has been generated
        if(csticUIBean != null){
            groupDeltaUIBean.addCharacteristic(csticUIBean);
            instUIBean.addCharacteristic(csticUIBean);
        }
        log.exiting();
    }


    /**
     * This method creates ValueDeltaUIBeans and add them to the csticDeltaUIBean.
     * It checks whether it is possible to condense the value deltas.
     * Deltas for single-valued characteristics could be condensed. That means
     * if a single-valued characteristic has two deltas. One is a "delete"-delta,
     * the other one an "insert"-delta. This information could be condensed to a
     * "change"-delta.
     * So, instead of stating: "Value A has been deleted, then value B has been 
     * inserted". It will state: "The value has been changed from A to B."
     * @param csticUIBean the csticDeltaUIBean to which the value deltas belong to
     * @param vDeltas list of value delta BOs
     * @param uiContext
     */
    private void condenseValueDeltas(CharacteristicDeltaUIBean csticDeltaUIBean, ArrayList vDeltas, UIContext uiContext) {
        log.entering("condenseValueDeltas()");
        Characteristic csticBO = csticDeltaUIBean.getBusinessObject();
        log.debug("Cstic: " + csticBO.getName());
        // make sure that there are deltas
        if(vDeltas.size()>0){
            if (csticDeltaUIBean.allowsMultipleValues()){
                log.debug("Cstic allows multiple values.");
                // multi-valued: do not try to condense and create also ValueUIBeans 
                // for values that did not change
                ValueDeltaUIBean valueUIBean;
                // list with all values of the cstic's domain
                List allValues = csticBO.getValues();
                // This hashMap tracks for which valueDeltas UIBeans have been created
                HashMap createdValueDeltas = new HashMap();
                // loop over all values of this cstic
                for (int i=0; i<allValues.size(); i++){
                    CharacteristicValue val = (CharacteristicValue) allValues.get(i);
                    ValueDelta deltaBO = findValueDeltaBO(vDeltas, val);
                    if (deltaBO != null){
                        // a delta exists for this value
                        log.debug("Delta existis for value " + val.getName());
                        valueUIBean = beanFactory.newValueDeltaUIBean(deltaBO, val, uiContext);
                        csticDeltaUIBean.addValue(valueUIBean);
                        // add an entry to the HashMap in order that we know later that we created a bean for this delta
                        createdValueDeltas.put(deltaBO.getValue(),deltaBO.getValue());
                    }
                    else {
                        // no delta exists for this value (= unchanged)
                        log.debug("No delta existis for value " + val.getName());
                        // check whether it is assigned
                        if (val.isAssigned()){
                            // create a ValueDeltaUIBean for an unchanged value of a multi-valued cstic
                            log.debug("Value " + val.getName() + " is assigned, so we create a ValueDelta.");
                            valueUIBean = beanFactory.newValueDeltaUIBean(val, uiContext);
                            csticDeltaUIBean.addValue(valueUIBean);                            
                        }
                        else{
                            // skip it
                        }
                    }
                }
                // Now we have to check whether beans for all deltas have been created.
                // In the following case it could happen that we miss to create a bean:
                // If a value has been deleted that was not part of the domain (i.e. an additional value).
                // In this case we would not cover it with looping over all values of the cstic's domain 
                // as done above.
                if (vDeltas.size() != createdValueDeltas.size()){
                    // we only have to loop over the deltas if the size of vDeltas is different 
                    // from the size of the HashMap with the names of created valueDeltas
                    for (int i=0; i<vDeltas.size(); i++){
                        // check for each valueDelta whether a bean has been created
                        ValueDelta deltaBO = (ValueDelta)vDeltas.get(i);
                        String value = deltaBO.getValue();
                        if (createdValueDeltas.get(value) == null){
                            // there is no entry for the currentVDelta in the HashMap -> it has not been created!
                            log.debug("Create valueDelta for "+ value);
                            valueUIBean = beanFactory.newValueDeltaUIBean(deltaBO, null, uiContext);
                            csticDeltaUIBean.addValue(valueUIBean); 
                        }
                    }
                }
            }
            else {
                // single-valued
                log.debug("Cstic is single-valued.");
                ValueDeltaUIBean valueUIBean;
                // if the size of vDeltas is 2 -> condense it
                if (vDeltas.size() == 2){
                    ValueDelta delta1; 
                    ValueDelta delta2;
                    CharacteristicValue value1;
                    CharacteristicValue value2;
                    log.debug("There are 2 valueDeltas for this value: we condense it");
                    // We have to find out which one of the two deltas is the "deleted" delta.
                    // The "deleted" delta will be "delta1" ("value before"). The "inserted" delta
                    // will be "delta2" ("value after");
                    if(((ValueDelta)vDeltas.get(0)).getType().equals(ValueDelta.DELETED)){
                        // delta at index 0 is the deleted delta
                        delta1 = (ValueDelta)vDeltas.get(0);
                        value1 = csticBO.getValue(delta1.getValue());
                        delta2 = (ValueDelta)vDeltas.get(1);
                        value2 = csticBO.getValue(delta2.getValue());
                    }
                    else {
                        // delta at index 0 is not the deleted delta
                        delta2 = (ValueDelta)vDeltas.get(0);
                        value2 = csticBO.getValue(delta2.getValue());                    
                        delta1 = (ValueDelta)vDeltas.get(1);
                        value1 = csticBO.getValue(delta1.getValue());                        
                    }
                    if (value1 == null && value2 == null && csticBO.getValueType() == Characteristic.TYPE_DATE){
                        // If it is a date-cstic we have to convert the values of the deltas.
                        // This is a work-around because AP returns dates differently in comparison FM.
                        String convertedDate1 = convertDateValue(delta1.getValue());  
                        value1 = csticBO.getValue(convertedDate1);
                        String convertedDate2 = convertDateValue(delta2.getValue());  
                        value2 = csticBO.getValue(convertedDate2);
                    }                                                            
                    valueUIBean = beanFactory.newValueDeltaUIBean(delta1, delta2, value1, value2, uiContext);
                    if (value1 == null){
                        // pass csticBO to provide additional information (e.g. for formatting)
                        valueUIBean.setCsticBO(csticBO);
                    }                    
                    csticDeltaUIBean.addValue(valueUIBean); 
                }
                else {
                    log.debug("There is only one valueDelta for this value.");
                    // there should be only one delta in this list (more than two would be an error)
                    ValueDelta delta1 = (ValueDelta)vDeltas.get(0);
                    CharacteristicValue value1 = csticBO.getValue(delta1.getValue());
                    if (value1 == null && csticBO.getValueType() == Characteristic.TYPE_DATE){
                        // If it is a date-cstic we have to convert the value of the delta.
                        // This is a work-around because AP returns dates differently in comparison FM.
                        String convertedDate = convertDateValue(delta1.getValue());  
                        value1 = csticBO.getValue(convertedDate);
                    }
                    valueUIBean = beanFactory.newValueDeltaUIBean(delta1, value1, uiContext);
                    if (value1 == null){
                        // pass csticBO to provide additional information (e.g. for formatting)
                        valueUIBean.setCsticBO(csticBO);
                    }
                    csticDeltaUIBean.addValue(valueUIBean);
                }
            }
        }
        log.exiting();
    }

    /**
     * This method converts the date value returned by FM CFG_API_COMPARE_CONFIGURATIONS.
     * The reason is that AP function modules return the date in different formats.
     * CFG_API_GET_FILTERED_CSTICS returns it as YYYYMMDD (e.g. 20060615) whereas 
     * CFG_API_COMPARE_CONFIGURATIONS returns it as Y.YYYMMDDE7 (e.g 2.0060615E7). This
     * leads to the problem that using the value from the comparison FM we can't find the
     * correct value business object because this is stored with format YYYYMMDD.   
     * @param originalDate (Y.YYYMMDDE7)
     * @return the converted date (YYYYMMDD)
     */
    public static String convertDateValue(String originalDate) {
        // check whether the string contains "E7" (indicates the format of CFG_API_COMPARE_CONFIGURATIONS).
        if (originalDate.indexOf("E7") == -1){
            return originalDate; // return the original value
        }
        int dotPosition = originalDate.indexOf(".");
        String firstPart = originalDate.substring(0, dotPosition);
        int eSevenPosition = originalDate.indexOf("E7");
        String secondPart = originalDate.substring(dotPosition+1, eSevenPosition);
        String converted = converted = firstPart + secondPart;
        // check whether the value has the correct length (it could be incorrect if day is 
        // 10, 20 or 30, then the exponential format is cut of at the end).
        if (converted.length() < 8){
            converted = converted + "0";
        }
        return converted;
    }

    /**
     * Find a value delta for a given value BO.
     * @param vDeltas the list of value deltas
     * @param val the characteristic value BO
     * @return the found value delta or null if nothing has been found
     */
    private ValueDelta findValueDeltaBO(ArrayList vDeltas, CharacteristicValue val) {
        ValueDelta deltaBO = null;
        for (int j=0; j<vDeltas.size(); j++){
            ValueDelta vd = (ValueDelta) vDeltas.get(j);
            if (vd.getValue().equals(val.getName())){
                deltaBO = vd;
                break;
            }
        }
        return deltaBO;
    }

    /**
     * Generates the configuration object key for a <code>Characteristic</code>.
     * The pattern is: "<instId>.<csticName>"
     * @param currentCstic characteristic for which the config object key should be generated
     * @return config object key
     */
    private String generateConfigObjectKey(Characteristic currentCstic) {
        StringBuffer cfgObjKeySB = new StringBuffer();
        cfgObjKeySB.append(currentCstic.getInstance().getId());
        cfgObjKeySB.append(".");
        cfgObjKeySB.append(currentCstic.getName());
        String cfgObjKey = cfgObjKeySB.toString();
        return cfgObjKey;
    }

    /**
     * Creates LoadingMessageUIBeans, sorts them by their configObject keys and by the 
     * instance they belong to.<br> 
     * For each configuration object an <code>ArrayList</code> object is generated and 
     * added to the <code>HashMap</code> of its instance with the configObjectKey as key 
     * (pattern: "<instId>.<csticName>").<br>
     * This <code>HashMap</code> for the loading messages of the instance is added to 
     * another <code>HashMap</code> with the instance id as key.<br>
     * If you want to access the loading messages of a specific configuration object, you first have
     * to retrieve the <code>HashMap</code> of its instance and then the list of messages using
     * the configuration object key.
     * @param loading messages list of loading message objects
     * @param uic UIContext
     * @return <code>HashMap</code> with a <code>HashMap</code> for each instance and in turn
     * an <code>ArrayList</code> for each configuration object that has loadinge messages.
     */
    private HashMap initLoadingMessagesByInstId(List loadingMessages, UIContext uic) {
        HashMap lmByInstId = new HashMap();    
        if (loadingMessages != null){            
            for (int i=0; i<loadingMessages.size(); i++ ){
                LoadingMessage lm = (LoadingMessage)loadingMessages.get(i);
                LoadingMessageUIBean lmBean = beanFactory.newLoadingMessageUIBean(lm, uic);
                // try to get the HashMap for lmBean's instance
                HashMap lmOfInstance = (HashMap)lmByInstId.get(lmBean.getInstId());
                if (lmByInstId.get(lmBean.getInstId()) == null){
                    // no map existing for this inst -> create new map
                    lmOfInstance = new HashMap();
                    // put the map for this instance into lmByInstId
                    lmByInstId.put(lmBean.getInstId(), lmOfInstance);
                }
                if (lmOfInstance.get(lmBean.getConfigObjKey()) != null) {
                    // there is already a list for this configObject in the map
                    ArrayList lmList = (ArrayList)lmOfInstance.get(lmBean.getConfigObjKey());
                    // add the bean to the list
                    lmList.add(lmBean);
                }
                else {
                    // create a new list for this configObject
                    ArrayList lmList = new ArrayList();
                    // put it into the map
                    lmOfInstance.put(lmBean.getConfigObjKey(), lmList);
                    // add the bean to the list
                    lmList.add(lmBean);
                }
            }
        }
        return lmByInstId;
    }
}
