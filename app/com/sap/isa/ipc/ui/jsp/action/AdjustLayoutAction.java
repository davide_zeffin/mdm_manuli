/**
 * Dispatches depending on the level the configuration has:
 * single level or multi level
 */
package com.sap.isa.ipc.ui.jsp.action;

import java.io.IOException;
import java.util.HashMap;


import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;

import com.sap.isa.businessobject.CommunicationException;
import com.sap.isa.core.Constants;
import com.sap.isa.core.UserSessionData;
import com.sap.isa.core.businessobject.management.MetaBusinessObjectManager;
import com.sap.isa.core.ui.layout.UIArea;
import com.sap.isa.core.ui.layout.UILayout;
import com.sap.isa.ipc.ui.jsp.dynamicui.IPCDynamicUIConstant;
import com.sap.isa.ipc.ui.jsp.dynamicui.IPCUIModel;
import com.sap.isa.ipc.ui.jsp.dynamicui.IPCUIPage;
import com.sap.isa.ipc.ui.jsp.uiclass.UIContext;
import com.sap.isa.isacore.action.ControlLayoutAction;
import com.sap.spc.remote.client.object.Configuration;
import com.sap.spc.remote.client.object.Instance;
import com.sap.spc.remote.client.object.OriginalRequestParameterConstants;


/**
 * Displays or hides layout areas according to customization settings or
 * model data.
 * Chooses the style ids for the layout components, if dynamically changed.
 */
/* style definitions for different combinations */
/* | message area        | multifunctional area | workarea style          | */
/* |         0           |          0           | 0: configworkarea       | */
/* |         0           |          1           | 1: configworkareamfa    | */
/* |         1           |          0           | 2: configworkareamsg    | */
/* |         1           |          1           | 3: configworkareamfgmsg | */
public class AdjustLayoutAction extends ControlLayoutAction implements RequestParameterConstants,
                                                              ActionForwardConstants{

    private static final int MULTIFUNCTIONALAREA = 1;
    private static final int MESSAGES = 2; 
    
    private static final String MFAAREANAME = "multifunctionalarea";
	private static final String MESSAGESARESNAME = "configmessagesarea";
    private static final String WORKAREANAME = "configworkarea";

    private static final String CONFIGWORKAREA_STYLE = "configworkarea";
    private static final String CONFIGWORKAREA_MFA_STYLE = "configworkareamfa";
    private static final String CONFIGWORKAREA_MSG_STYLE = "configworkareamsg";
    private static final String CONFIGWORKAREA_MFA_MSG_STYLE = "configworkareamfamsg";


    public void controlLayout(
	ActionMapping mapping,
	ActionForm form,
	HttpServletRequest request,
	HttpServletResponse response,
	UserSessionData userSessionData,
	MetaBusinessObjectManager mbom,
	UILayout layout)
	throws IOException, ServletException, CommunicationException {


		UIContext uiContext = IPCBaseAction.getUIContext(request);

        // change the css-class for the body due to display mode (default is display-mode:F -> ipcBody)
        if(uiContext.getDisplayMode()){  
            // read-only mode -> change css-class for the body
            layout.setCssClassName("ipcBodyReadOnly");
        }
		Configuration currentConfiguration = uiContext.getCurrentConfiguration();
        String currentInstanceId = getContextValue(request, Constants.CURRENT_INSTANCE_ID);
        if (!IPCBaseAction.isSet(currentInstanceId)) {
            currentInstanceId = currentConfiguration.getRootInstance().getId();
        }
        Instance currentInstance = currentConfiguration.getInstance(currentInstanceId);
        IPCUIModel ui = (IPCUIModel) uiContext.getProperty(IPCDynamicUIConstant.SESSION_DYNAMIC_UI);

        boolean displayPictures = displayPictureArea(uiContext, currentInstance, ui);
        boolean displayMessages = GetMessagesAction.displayMessagesArea(request, uiContext, currentConfiguration);
	    
        int layoutCase = 0;
        if (displayPictures){
            layoutCase += MULTIFUNCTIONALAREA; 
        }
        if (displayMessages){
            layoutCase += MESSAGES;
        }
        	
		String uiarea_not_found = "UIArea &1 not found";
		UIArea mfaarea = layout.getUiArea(MFAAREANAME);
		if (mfaarea == null) {
			log.warn(uiarea_not_found, new String[]{MFAAREANAME}, null);
		}
		UIArea messagesarea = layout.getUiArea(MESSAGESARESNAME);
		if (messagesarea == null) {
			log.warn(uiarea_not_found, new String[]{MESSAGESARESNAME}, null);
		}
		UIArea workarea = layout.getUiArea(WORKAREANAME);
		if (workarea == null) {
			log.warn(uiarea_not_found, new String[]{WORKAREANAME}, null);
		}
	    switch (layoutCase) {
	    	case 0:
	    	    mfaarea.setHidden(true);
                messagesarea.setHidden(true);
	    	    workarea.setCssIdName(CONFIGWORKAREA_STYLE);
	    	    break;
	    	case 1:
	    	    mfaarea.setHidden(false);
                messagesarea.setHidden(true);
	    	    workarea.setCssIdName(CONFIGWORKAREA_MFA_STYLE);
	    	    break;
	    	case 2:
	    	    mfaarea.setHidden(true);
                messagesarea.setHidden(false);
	    	    workarea.setCssIdName(CONFIGWORKAREA_MSG_STYLE);
	    	    break;
	    	case 3:
	    	    mfaarea.setHidden(false);
                messagesarea.setHidden(false);
	    	    workarea.setCssIdName(CONFIGWORKAREA_MFA_MSG_STYLE);
	    	    break;
        }
	}
    
    private boolean displayPictureArea(UIContext uiContext, Instance instance, IPCUIModel ui){
        boolean displayPictureArea = false;
        boolean hasDynamic = false;
        boolean hasPagePic = false;
        // check if dynamicProductPicture-feature is enabled
        if (uiContext.getPropertyAsBoolean(OriginalRequestParameterConstants.SHOW_DYNAMIC_PRODUCT_PICTURE)){
            // check wether product is in the list of dynamic products
            HashMap dynProds = uiContext.getDynamicProducts();
            if (dynProds != null && instance != null){
                hasDynamic = dynProds.containsKey(instance.getName());
            }
        }
        // check for picture at active page
        if (ui != null){
            IPCUIPage page = (IPCUIPage)ui.getActivePage();
            if (page != null){
                String url = page.getImageURL();
                if (url != null && !url.equals("")){
                    hasPagePic = true;
                }
            }
            
        }        
        if (hasDynamic || hasPagePic){
            displayPictureArea = true;
        }
        return displayPictureArea;
    }
    
}