package com.sap.isa.ipc.ui.jsp.action;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.sap.isa.core.UserSessionData;
import com.sap.isa.core.businessobject.management.MetaBusinessObjectManager;
import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.core.util.RequestParser;
import com.sap.isa.ipc.ui.jsp.uiclass.UIContext;
import com.sap.spc.remote.client.object.OriginalRequestParameterConstants;

/**
 * Starting configuration in the CRM context when CRM accesses the ERP order
 * via the 'Lean Order' API.
 * 
 * Requires the following http request parameter:
 * IPC_HOST   {String}
 * IPC_PORT   {String representing integer number}
 * DISPATCHER {"true|false"}
 * IPC_SESSION, DOCUMENTID, ITEMID {String representing integer number} 
 *
 */
public class StartCRMERPLRDConfigurationAction extends StartCRMOnlineConfigurationAction {
	private static IsaLocation log = IsaLocation.getInstance(
				StartCRMERPLRDConfigurationAction.class.getName());
				
	public ActionForward ecomPerform(
		ActionMapping mapping,
		ActionForm form,
		HttpServletRequest request,
		HttpServletResponse response,
		UserSessionData userSessionData,
		RequestParser requestParser,
		MetaBusinessObjectManager mbom,
		boolean multipleInvocation,
		boolean browserBack)
		throws IOException, ServletException {
			
			log.debug("ecomPerform start");
			
			//We have to decide whether we are in 'changeable product variant mode' or not. 
			//ecomPerfom will set this flag according to the IPC instance, so we have to 
			//re-set this attribute afterwards (from a request parameter)
			
			//We need the IPCItem as container for the 'changeable product variant mode' because
			//ECO uses the IPCItem as transfer mechanism	
			
			//Request parameter have priority over request attributes
			String requestConstant = OriginalRequestParameterConstants.CHANGEABLE_PRODUCT_VARIANT_MODE;
			String requestParameter = "";
			if (request.getParameter(requestConstant) != null
				&& !request.getParameter(requestConstant).equals("")) {
				requestParameter = request.getParameter(requestConstant);
			}else if (request.getAttribute(requestConstant) != null) {
				requestParameter = (String) request.getAttribute(requestConstant);
				}
			
			boolean changeableProductVariant = requestParameter.equals("T");
			
			//in the super call, the attribute will be overvwritten
			ActionForward forward = super.ecomPerform( 
									  mapping,
									  form,
									  request,
									  response,
									  userSessionData,
									  requestParser,
									  mbom,
									  multipleInvocation,
									  browserBack);
									  
									  
			UIContext uiContext = IPCBaseAction.getUIContext(request);
			
			//Here we can assume that the uiContext is properly initialized
			if (uiContext != null) { 			  
			 uiContext.setChangeableProductVariantMode(changeableProductVariant);
			}
			
			return forward;									  
									  
		}				
}
