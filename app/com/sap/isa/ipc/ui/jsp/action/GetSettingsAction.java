package com.sap.isa.ipc.ui.jsp.action;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.sap.isa.core.Constants;
import com.sap.isa.core.SessionConst;
import com.sap.isa.core.UserSessionData;
import com.sap.isa.core.businessobject.management.MetaBusinessObjectManager;
import com.sap.isa.core.util.RequestParser;
import com.sap.isa.core.xcm.ConfigContainer;
import com.sap.isa.ipc.ui.jsp.beans.GroupUIBean;
import com.sap.isa.ipc.ui.jsp.beans.InstanceUIBean;
import com.sap.isa.ipc.ui.jsp.beans.UIBeanFactory;
import com.sap.isa.ipc.ui.jsp.uiclass.UIContext;
import com.sap.spc.remote.client.object.IPCClient;
import com.sap.spc.remote.client.object.IPCException;
import com.sap.spc.remote.client.object.Instance;
import com.sap.spc.remote.client.object.KnowledgeBase;
//remove for new stubs
//import com.sap.sxe.sys.sys_const;
//<--

/**
 * The GetSettings - Action copies all settings into the request for further
 * processing.
 */
public class GetSettingsAction
    extends IPCBaseAction
    implements RequestParameterConstants, ActionForwardConstants {

		public ActionForward ecomPerform(ActionMapping mapping,
			ActionForm form,
			HttpServletRequest request,
			HttpServletResponse response,
			UserSessionData userSessionData,
			RequestParser requestParser,
			MetaBusinessObjectManager mbom,
			boolean multipleInvocation,
			boolean browserBack)
				throws IOException, ServletException {

        IPCClient ipcClient =
            this.getIPCClient(request);
        UIContext uiContext =
            getUIContext(request);

        //required parameter for return to the configuration page
        //the settings page will store the ids in hidden input fields
        //and submitt them when returning to the configuration page
        String curInstId = getContextValue(request, Constants.CURRENT_INSTANCE_ID);
		if (isSet(curInstId)) {
        	Instance curInst = uiContext.getCurrentConfiguration().getInstance(curInstId);
			InstanceUIBean curInstUIBean =
			UIBeanFactory.getUIBeanFactory().newInstanceUIBean(curInst, uiContext);
			request.removeAttribute(CURRENT_INSTANCE);
			request.setAttribute(CURRENT_INSTANCE, curInstUIBean);

	        String curCharGroupName = getContextValue(request, Constants.CURRENT_CHARACTERISTIC_GROUP_NAME);
			if (isSet(curCharGroupName)) {
	        	GroupUIBean curCharGroup = curInstUIBean.getGroup(curCharGroupName);
	        	request.setAttribute(CURRENT_CHARACTERISTIC_GROUP, curCharGroup);
			}

	        String curCharName = getContextValue(request, Constants.CURRENT_CHARACTERISTIC_NAME);
	        if (isSet(curCharName)) {
	            request.setAttribute(
	                CURRENT_CHARACTERISTIC,
	                curInstUIBean.getCharacteristic(curCharName));
	        }
		}

        KnowledgeBase knowledgeBase =
            uiContext.getCurrentConfiguration().getKnowledgeBase();

		customerExit(mapping, form, request, response, userSessionData, requestParser, mbom, multipleInvocation, browserBack);

        try {
            // transfer all settings from the ui context to corresponding request parameters
            request.setAttribute(
                SHOW_INVISIBLE_CHARACTERISTICS,
                translateBoolean(
                    uiContext.getPropertyAsBoolean(
                        SHOW_INVISIBLE_CHARACTERISTICS)));
            request.setAttribute(
                ONLINE_EVALUATE,
                translateBoolean(
                    uiContext.getPropertyAsBoolean(ONLINE_EVALUATE)));
            request.setAttribute(
                SHOW_LANGUAGE_DEPENDENT_NAMES,
                translateBoolean(
                    uiContext.getPropertyAsBoolean(
                        SHOW_LANGUAGE_DEPENDENT_NAMES)));
            request.setAttribute(
                USE_GROUP_INFORMATION,
                translateBoolean(
                    uiContext.getPropertyAsBoolean(USE_GROUP_INFORMATION)));
			String xcmScenario = "";
			if (userSessionData != null){
				ConfigContainer xcmConfig = (ConfigContainer) userSessionData.getAttribute(SessionConst.XCM_CONFIG);
				if (xcmConfig != null){
					xcmScenario = xcmConfig.getScenarioName();
				}
			}                        
			request.setAttribute(IPC_UI_VERSION, xcmScenario);
            request.setAttribute(
                SHOW_CSTIC_VALUES_EXPANDED,
                translateBoolean(
                    uiContext.getPropertyAsBoolean(
                        SHOW_CSTIC_VALUES_EXPANDED)));
            request.setAttribute(KB_NAME, knowledgeBase.getName());
            request.setAttribute(KB_VERSION, knowledgeBase.getVersion());
            request.setAttribute(
                KB_PROFILE,
                uiContext
                    .getCurrentConfiguration()
                    .getActiveKBProfile()
                    .getName());
            request.setAttribute(
                KB_BUILD_NUMBER,
                knowledgeBase.getBuildNumber());
        } catch (IPCException e) {
            request.setAttribute(RequestParameterConstants.IPC_EXCEPTION, e);
            return mapping.findForward(INTERNAL_ERROR);
        }

        return processForward(mapping, request, ActionForwardConstants.SUCCESS);
    }

    /**
     * This method returns T for "true" and F for "false"
     */
    private String translateBoolean(boolean source) {
        return (source) ? T : F;
    }
}
