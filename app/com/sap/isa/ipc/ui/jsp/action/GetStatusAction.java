/*
 * Created on 09.08.2004
 *
 * To change the template for this generated file go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
package com.sap.isa.ipc.ui.jsp.action;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.sap.isa.core.UserSessionData;
import com.sap.isa.core.businessobject.management.MetaBusinessObjectManager;
import com.sap.isa.core.util.RequestParser;
import com.sap.isa.ipc.ui.jsp.beans.ConfigUIBean;
import com.sap.isa.ipc.ui.jsp.beans.InstanceUIBean;
import com.sap.isa.ipc.ui.jsp.beans.UIBeanFactory;
import com.sap.isa.ipc.ui.jsp.uiclass.UIContext;
import com.sap.spc.remote.client.object.Configuration;
import com.sap.spc.remote.client.object.Instance;

/**
 * @author 
 *
 * To change the template for this generated type comment go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
public class GetStatusAction extends IPCBaseAction {

    /* (non-Javadoc)
     * @see com.sap.isa.core.BaseAction#doPerform(org.apache.struts.action.ActionMapping, org.apache.struts.action.ActionForm, javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse)
     */
	public ActionForward ecomPerform(ActionMapping mapping,
		ActionForm form,
		HttpServletRequest request,
		HttpServletResponse response,
		UserSessionData userSessionData,
		RequestParser requestParser,
		MetaBusinessObjectManager mbom,
		boolean multipleInvocation,
		boolean browserBack)
			throws IOException, ServletException {
            UIContext uiContext = getUIContext(request);
            Configuration currentConfiguration = uiContext.getCurrentConfiguration();
			Instance rootInstance = currentConfiguration.getRootInstance();
            
            ConfigUIBean configUIBean = UIBeanFactory.getUIBeanFactory().newConfigUIBean(currentConfiguration, uiContext);
			InstanceUIBean rootInstanceUIBean = UIBeanFactory.getUIBeanFactory().newInstanceUIBean(rootInstance, uiContext);
			configUIBean.setRootInstance(rootInstanceUIBean);
            
			customerExit(mapping, form, request, response, userSessionData, requestParser, mbom, multipleInvocation, browserBack);

            request.setAttribute(InternalRequestParameterConstants.CURRENT_CONFIGURATION, configUIBean);
            return mapping.findForward(ActionForwardConstants.SUCCESS);
        //return processForward(mapping, request, ActionForwardConstants.SUCCESS);
    }

    public static void main(String[] args) {
    }
}
