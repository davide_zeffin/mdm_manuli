package com.sap.isa.ipc.ui.jsp.action;
/**
 * The action GetProductVariantsAction returns a list of product variants
 * of the root instance. If no product variants would be found,
 * the action would return an empty list.
 * In the jsp page you can access the the list of product variants
 * using the following method:
 *
 * List productVariants = (List)request.getAttribute(ProductVariantsRequestParameterConstants.PRODUCT_VARIANTS);
 */
import java.io.IOException;
import java.util.ArrayList;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.sap.isa.core.Constants;
import com.sap.isa.core.UserSessionData;
import com.sap.isa.core.businessobject.management.MetaBusinessObjectManager;
import com.sap.isa.core.util.RequestParser;
import com.sap.isa.ipc.ui.jsp.beans.ConfigUIBean;
import com.sap.isa.ipc.ui.jsp.beans.InstanceUIBean;
import com.sap.isa.ipc.ui.jsp.beans.ProductVariantUIBean;
import com.sap.isa.ipc.ui.jsp.beans.UIBeanFactory;
import com.sap.isa.ipc.ui.jsp.constants.ProductVariantsRequestParameterConstants;
import com.sap.isa.ipc.ui.jsp.uiclass.UIContext;
import com.sap.spc.remote.client.object.Configuration;
import com.sap.spc.remote.client.object.IPCClient;
import com.sap.spc.remote.client.object.IPCDocument;
import com.sap.spc.remote.client.object.IPCDocumentProperties;
import com.sap.spc.remote.client.object.IPCException;
import com.sap.spc.remote.client.object.IPCItem;
import com.sap.spc.remote.client.object.IPCItemProperties;
import com.sap.spc.remote.client.object.ProductVariant;
import com.sap.spc.remote.client.object.ProductKey;
import com.sap.spc.remote.client.object.imp.DefaultIPCDocument;

public class GetProductVariantsAction
	extends IPCBaseAction
	implements RequestParameterConstants, ActionForwardConstants {

		public ActionForward ecomPerform(ActionMapping mapping,
			ActionForm form,
			HttpServletRequest request,
			HttpServletResponse response,
			UserSessionData userSessionData,
			RequestParser requestParser,
			MetaBusinessObjectManager mbom,
			boolean multipleInvocation,
			boolean browserBack)
				throws IOException, ServletException {

		HttpSession session = request.getSession(false);
		if (session == null) {
			log.fatal("Could not get HttpSession in IPCBaseAction!");
		}

		// read businessobjectmanager from Session
		UserSessionData userData = UserSessionData.getUserSessionData(session);
		IPCClient ipcClient =
			this.getIPCClient(request);
		UIContext uiContext =
			getUIContext(request);

		ConfigUIBean configUIBean = this.getCurrentConfiguration(uiContext);

		String curInstId = getContextValue(request, Constants.CURRENT_INSTANCE_ID);
		InstanceUIBean curInstUIBean =
			this.getCurrentInstance(
				uiContext,
				configUIBean,
				curInstId,
				request);

		List productVariants;
		try {
            //the 5.0 release only supports incomplete search (not fuzzy search)
            //therefore the parameters LIMIT_OF_PRODUCT_VARIANTS, QUALITY_OF_PRODUCT_VARIANTS
            //and MATCH_POINTS_OF_PRODUCT_VARIANTS have no affect.
			productVariants =
				configUIBean
					.getRootInstance()
					.getBusinessObject()
					.getProductVariants(
					uiContext.getPropertyAsString(LIMIT_OF_PRODUCT_VARIANTS, "5"),
					uiContext.getPropertyAsString(
						QUALITY_OF_PRODUCT_VARIANTS,
						"0.25"),
					uiContext.getPropertyAsString(
						MATCH_POINTS_OF_PRODUCT_VARIANTS,
						"5"));
		} catch (IPCException e) {
			request.setAttribute(InternalRequestParameterConstants.IPC_EXCEPTION, e);
			return mapping.findForward(ActionForwardConstants.INTERNAL_ERROR);
		}

		ArrayList productVariantUIBeans = new ArrayList();

		Hashtable productVariantsById = new Hashtable(productVariants.size());
		uiContext.setProperty(PRODUCT_VARIANTS_BY_ID, productVariantsById);
		//if no product variants found, just exit (performance)
		if (!productVariants.isEmpty()) {
			IPCItem configProductItem =
				configUIBean.getRootInstance().getBusinessObject().getIpcItem();
			if (configProductItem != null && !uiContext.getHidePrices()) {
				IPCDocument currentDocument = configProductItem.getDocument();
				IPCItemProperties[] itemProperties =
					configProductItem.getPropertiesFromServer(new String[] {configProductItem.getItemId()});
				// generate the productKey (id, logsys, type) of the KMAT
				ProductKey productKey = configProductItem.getProductKey();
                IPCDocumentProperties documentProperties = null;
				if (currentDocument != null) {
					documentProperties = currentDocument.getPropertiesFromServer();
				} else {
					log.debug(configProductItem);
				}
                
				if (documentProperties != null) {
					IPCDocument prodVarDoc = null;
					prodVarDoc = ipcClient.createIPCDocument(documentProperties);
                    String application = ((DefaultIPCDocument)currentDocument).getApplication();
					IPCItemProperties[] prodVarItemProperties =
						new IPCItemProperties[productVariants.size()];
					int i = 0;
					for (Iterator it = productVariants.iterator();
						it.hasNext();
						i++) {
						ProductVariant currentProductVariant =
							(ProductVariant) it.next();
						prodVarItemProperties[i] =
							factory.newIPCItemProperties(itemProperties[0]);
						prodVarItemProperties[i].setProductGuid(null);
						prodVarItemProperties[i].setProductId(
							currentProductVariant.getId());
						prodVarItemProperties[i].setConfig(null);
						// use the product type of the KMAT
                        currentProductVariant.setProductType(productKey.getProductType());
                        // use the product logsys of the KMAT
                        currentProductVariant.setProductLogSys(productKey.getProductLogSys());
                        // get the original item attributes; they will be replaced by the attributes from variant's product master
                        Map itemAttributes = prodVarItemProperties[i].getItemAttributes();
                        // get the itemAttributes for the variant from the product master
                        Map itemAttributesFromProductMaster = currentProductVariant.getAttributesFromProductMaster(application);
                        // replace the original item attributes with the attributes that were returned from the variant's product master
                        if (itemAttributes != null){
                            Iterator iter = itemAttributesFromProductMaster.keySet().iterator();
                            while (iter.hasNext()) {
                                String key = (String)iter.next();
                                String value = (String)itemAttributesFromProductMaster.get(key);
                                itemAttributes.put(key, value);
                            }
                            // set the attributes at the properties
                            prodVarItemProperties[i].setItemAttributes(itemAttributes);
                        }
                                                                        
						ProductVariantUIBean productVariantUIBean =
						UIBeanFactory.getUIBeanFactory().newProductVariantUIBean(
								currentProductVariant,
								uiContext);
						productVariantsById.put(
							productVariantUIBean.getId(),
							productVariantUIBean);
						productVariantUIBeans.add(productVariantUIBean);
					}
					// create new items for the product variants
					prodVarDoc.newItems(prodVarItemProperties);

					IPCItem[] prodVarItems = prodVarDoc.getAllItems();
					for (int j = 0; j < prodVarItems.length; j++) {
						IPCItem currentItem = prodVarItems[j];
						ProductVariantUIBean currentProductVariant =
							(ProductVariantUIBean) productVariantsById.get(
								currentItem.getProductId());
						if (currentProductVariant == null) {
							log.fatal(
								"No product variant found for item "
									+ currentItem.getProductId()
									+ " !");
						} else {
							currentProductVariant.setItem(currentItem);
							currentProductVariant.setConfiguration(
								currentItem.getConfiguration());
						}
					}
				}
			} else { //no pricing document and item available, config only
				for (Iterator i = productVariants.iterator(); i.hasNext();) {
					ProductVariant currentProductVariant =
						(ProductVariant) i.next();
					ProductVariantUIBean productVariantUIBean =
					UIBeanFactory.getUIBeanFactory().newProductVariantUIBean(currentProductVariant, uiContext);
					productVariantsById.put(
						productVariantUIBean.getId(),
						productVariantUIBean);
					productVariantUIBeans.add(productVariantUIBean);
						Configuration configuration =
							ipcClient
								.createConfiguration(
									currentProductVariant.getId(),
									userData.getSAPLanguage(),
									null,
							//kbDate is redundant if logsys, name, version is available
							uiContext
								.getCurrentConfiguration()
								.getKnowledgeBase()
								.getLogsys(),
								uiContext
									.getCurrentConfiguration()
									.getKnowledgeBase()
									.getName(),
								uiContext
									.getCurrentConfiguration()
									.getKnowledgeBase()
									.getVersion(),
								uiContext
									.getCurrentConfiguration()
									.getActiveKBProfile()
									.getName(),
								uiContext
									.getCurrentConfiguration()
									.getContextNames(),
								uiContext
									.getCurrentConfiguration()
									.getContextValues());
							currentProductVariant.setConfiguration(
								configuration);
					if (log.isDebugEnabled()) {
						log.debug("currentProductVariant:" + currentProductVariant);
					}
				}
			}
		}else {
			if (log.isDebugEnabled()) {
				log.debug("No product variant found!");
			}
		}

		customerExit(mapping, form, request, response, userSessionData, requestParser, mbom, multipleInvocation, browserBack);

		request.setAttribute(ProductVariantsRequestParameterConstants.CURRENT_INSTANCE, curInstUIBean);
		request.setAttribute(
			ProductVariantsRequestParameterConstants.PRODUCT_VARIANTS,
			productVariantUIBeans);
			
		// trigger further processing	
		return GetAdditionalInfoAction.determineForward(mapping, this.getClass().getName(), uiContext);

	}
}