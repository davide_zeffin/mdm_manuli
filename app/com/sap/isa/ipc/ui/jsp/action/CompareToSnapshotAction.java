package com.sap.isa.ipc.ui.jsp.action;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.sap.isa.businessobject.CommunicationException;
import com.sap.isa.core.UserSessionData;
import com.sap.isa.core.businessobject.management.MetaBusinessObjectManager;
import com.sap.isa.core.util.RequestParser;
import com.sap.isa.ipc.ui.jsp.beans.MessageUIBean;
import com.sap.isa.ipc.ui.jsp.beans.UIBeanFactory;
import com.sap.isa.ipc.ui.jsp.uiclass.UIContext;
import com.sap.spc.remote.client.object.Configuration;
import com.sap.spc.remote.client.object.ConfigurationSnapshot;
import com.sap.spc.remote.client.object.Instance;
import com.sap.spc.remote.client.object.imp.DefaultConfigurationSnapshot;

public class CompareToSnapshotAction
    extends CompareConfigurationsAction
    implements RequestParameterConstants, ActionForwardConstants {

    /* (non-Javadoc)
     * @see com.sap.isa.isacore.action.EComBaseAction#ecomPerform(org.apache.struts.action.ActionMapping, org.apache.struts.action.ActionForm, javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse, com.sap.isa.core.UserSessionData, com.sap.isa.core.util.RequestParser, com.sap.isa.core.businessobject.management.MetaBusinessObjectManager, boolean, boolean)
     */
    public ActionForward ecomPerform(
        ActionMapping mapping,
        ActionForm form,
        HttpServletRequest request,
        HttpServletResponse response,
        UserSessionData userSessionData,
        RequestParser requestParser,
        MetaBusinessObjectManager mbom,
        boolean multipleInvocation,
        boolean browserBack)
        throws IOException, ServletException, CommunicationException {
        
        UIContext uiContext = getUIContext(request);
        // save the state of the ConfigUI
        changeContextValues(request);
        // store the showAll-flag in the UIContext
        storeShowAll(request, uiContext);
        // store the showCritical-flag in the UIContext
        storeFilterFlag(request, uiContext);

        // get the list of messages from the uiContext
        List messages = (List) uiContext.getProperty(MESSAGES);
        if (messages == null){
            messages = new ArrayList();    
        }
        
        // check whether feature is disabled
        if (!uiContext.getPropertyAsBoolean(ENABLE_COMPARISON, false)) {
            // create an error message
            MessageUIBean messageBean = UIBeanFactory.getUIBeanFactory().newMessageUIBean();
            messageBean.setMessageStatus(MessageUIBean.ERROR);
            messageBean.setMessageTranslateKey("ipc.message.compare.disabled");
            messages.add(messageBean);
            uiContext.setProperty(MESSAGES, messages);            
            return (mapping.findForward(COMPARISON_ERROR));
        }

        // save the action that has been called for comparison
        uiContext.setProperty(COMPARISON_ACTION, COMPARISON_ACTION_SNAP);
        
        Configuration currentConfig = uiContext.getCurrentConfiguration();
        
        ConfigurationSnapshot snap = (ConfigurationSnapshot)uiContext.getProperty(SNAPSHOT);
        // check whether there is a snapshot
        if(snap == null){
            // no snapshot, use the initial snapshot instead
            snap = (ConfigurationSnapshot)uiContext.getProperty(SNAPSHOT_INITIAL);
            if (snap == null){
                // no initial snapshot available -> create message and return to ConfigUI 
                MessageUIBean errorMsg = UIBeanFactory.getUIBeanFactory().newMessageUIBean();
                errorMsg.setMessageStatus(MessageUIBean.ERROR);
                errorMsg.setMessageTranslateKey("ipc.message.compare.no.snapshot");
                messages.add(errorMsg);
                uiContext.setProperty(MESSAGES, messages);
                return mapping.findForward(ActionForwardConstants.NO_SNAPSHOT);                
            }
        }
        // retrieve the snapshot of the current configuration
        Instance rootInstance = currentConfig.getRootInstance();
        String rootInstancePrice = rootInstance.getPrice();
        DefaultConfigurationSnapshot currentSnap = factory.newConfigurationSnapshot(currentConfig, "", rootInstancePrice, false);
        // compare the snapshots
        messages = compareConfigurations(snap, currentSnap, request);
        if (messages.size()>0){
            // errors occured during comparison -> add messages to uiContext and return to ConfigUI
            uiContext.setProperty(MESSAGES, messages);
            return mapping.findForward(ActionForwardConstants.COMPARISON_ERROR);
        }
        
        customerExit(mapping, form, request, response, userSessionData, requestParser, mbom, multipleInvocation, browserBack);
        return mapping.findForward(ActionForwardConstants.COMPARISON_SCREEN);    
    }

}
