/*
 * ChangeCharacteristicGroupAction:
 * 
 */
package com.sap.isa.ipc.ui.jsp.action;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.sap.isa.core.Constants;
import com.sap.isa.core.UserSessionData;
import com.sap.isa.core.businessobject.management.MetaBusinessObjectManager;
import com.sap.isa.core.util.RequestParser;
import com.sap.isa.ipc.ui.jsp.uiclass.UIContext;

public class ChangeCharacteristicGroupAction extends IPCBaseAction implements RequestParameterConstants,
                                                                        ActionForwardConstants {
	public ActionForward ecomPerform(ActionMapping mapping,
		ActionForm form,
		HttpServletRequest request,
		HttpServletResponse response,
		UserSessionData userSessionData,
		RequestParser requestParser,
		MetaBusinessObjectManager mbom,
		boolean multipleInvocation,
		boolean browserBack)
			throws IOException, ServletException {

		String currentInstanceId = request.getParameter(Constants.CURRENT_INSTANCE_ID);
		if (isSet(currentInstanceId)) {
			this.changeContextValue(request, Constants.CURRENT_INSTANCE_ID, currentInstanceId);
		}else {
			return parameterError(mapping, request, Constants.CURRENT_INSTANCE_ID, getClass().getName());
		}

        UIContext uiContext = getUIContext(request);
        if (!uiContext.getOnlineEvaluate() && !browserBack) {
            ActionForward forward = bufferCharacteristicValues(mapping, request,currentInstanceId);
            if (!forward.getName().equals(SUCCESS)) {
                return forward;
            }
        }
        
		String groupName = request.getParameter(Constants.CURRENT_CHARACTERISTIC_GROUP_NAME);
		if (!isSet(groupName)) {
			return parameterError(mapping, request, Constants.CURRENT_CHARACTERISTIC_GROUP_NAME, getClass().getName());
		}
		this.changeContextValue(request, Constants.CURRENT_CHARACTERISTIC_GROUP_NAME, groupName);
        
        String currentScrollGroup = request.getParameter(Constants.CURRENT_SCROLL_CHARACTERISTIC_GROUP_NAME);
        if (!isSet(currentScrollGroup)) {
            return parameterError(mapping, request, Constants.CURRENT_SCROLL_CHARACTERISTIC_GROUP_NAME, getClass().getName());
        }
        this.changeContextValue(request, Constants.CURRENT_SCROLL_CHARACTERISTIC_GROUP_NAME, currentScrollGroup);
        // scrollGroup needs to be set also to the uiContext because this is a session-wide information 
        // and it is not set by the js-functions (e.g. onCsticValueChange, etc.)
        uiContext.setProperty(Constants.CURRENT_SCROLL_CHARACTERISTIC_GROUP_NAME,currentScrollGroup);
                
		customerExit(mapping, form, request, response, userSessionData, requestParser, mbom, multipleInvocation, browserBack);

		setExtendedRequestAttribute(request, GROUP_CHANGED, "true");
        return processForward(mapping, request, ActionForwardConstants.SUCCESS);       
    }

}
