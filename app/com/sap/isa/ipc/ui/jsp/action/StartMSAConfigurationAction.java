/*
 * Created on 13.01.2005
 *
 */
package com.sap.isa.ipc.ui.jsp.action;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.sap.isa.businessobject.ipc.IPCBOManager;
import com.sap.isa.core.UserSessionData;
import com.sap.isa.core.businessobject.management.MetaBusinessObjectManager;
import com.sap.isa.core.util.RequestParser;
import com.sap.isa.core.util.RequestParser.Parameter;
import com.sap.isa.ipc.ui.jsp.uiclass.UIContext;
import com.sap.spc.remote.client.object.Configuration;
import com.sap.spc.remote.client.object.IPCClient;
import com.sap.spc.remote.client.object.IPCException;
import com.sap.spc.remote.client.object.IPCItem;
import com.sap.spc.remote.client.object.imp.tcp.TCPIPCItemReference;

/**
 * @author 
 *
 */
public class StartMSAConfigurationAction
	extends StartCRMOnlineConfigurationAction {

	public ActionForward ecomPerform(
		ActionMapping mapping,
		ActionForm form,
		HttpServletRequest request,
		HttpServletResponse response,
		UserSessionData userSessionData,
		RequestParser requestParser,
		MetaBusinessObjectManager mbom,
		boolean multipleInvocation,
		boolean browserBack)
		throws IOException, ServletException {

			UIContext uiContext = IPCBaseAction.getUIContext(request);
			if (uiContext == null) { //the uiContext has not been initialized
			ActionForward forward = processScenarioParameter(mapping, request);
						if (!forward.getName().equals(ActionForwardConstants.SUCCESS)) return forward;
				uiContext = IPCBaseAction.getUIContext(request); //uiContext created in processScenarioParameter
			}
						super.htmlTrace(request);

						//UIContext uiContext = IPCBaseAction.getUIContext(request);
						//mandatory parameter
						Parameter hostParam = requestParser.getParameter(IPC_HOST);
						if (!hostParam.isSet()) return parameterError(mapping, request, RequestParameterConstants.IPC_HOST, getClass().getName());
						Parameter portParam = requestParser.getParameter(IPC_PORT);
						if (!portParam.isSet()) return parameterError(mapping, request, RequestParameterConstants.IPC_PORT, getClass().getName());
						String dispatcher = request.getParameter(DISPATCHER);
						if (dispatcher == null || dispatcher.equals("")) return parameterError(mapping, request, RequestParameterConstants.DISPATCHER, getClass().getName());
						//Parameter sessionIdParam = requestParser.getParameter(IPC_SESSION);
						//if (!sessionIdParam.isSet()) return parameterError(mapping, request, RequestParameterConstants.IPC_SESSION, getClass().getName());
						Parameter documentIdParam = requestParser.getParameter(DOCUMENTID);
						if (!documentIdParam.isSet()) return parameterError(mapping, request, RequestParameterConstants.DOCUMENTID, getClass().getName());
						Parameter itemIdParam = requestParser.getParameter(ITEMID);
						if (!itemIdParam.isSet()) return parameterError(mapping, request, RequestParameterConstants.ITEMID, getClass().getName());
						Parameter callerParam = requestParser.getParameter(CALLER);
						if (!callerParam.isSet()) return parameterError(mapping, request, RequestParameterConstants.CALLER, getClass().getName());
						uiContext.setCaller(callerParam.getValue().getString());

						//parameter that is taken from the customizing, if not part of the request
						String encoding = "UnicodeLittle";
			
						TCPIPCItemReference itemReference;
						itemReference = new TCPIPCItemReference();
						itemReference.setHost(hostParam.getValue().getString());
						itemReference.setPort(portParam.getValue().getString());
						itemReference.setDispatcher(Boolean.valueOf(dispatcher).booleanValue());
						itemReference.setEncoding(encoding);
						//itemReference.setSessionId(sessionIdParam.getValue().getString());
						itemReference.setDocumentId(documentIdParam.getValue().getString());
						itemReference.setItemId(itemIdParam.getValue().getString());
						if (log.isDebugEnabled()) {
							log.debug("Host: " + itemReference.getHost());
							log.debug("Port: " + itemReference.getPort());
							log.debug("Dispatcher: " + itemReference.isDispatcher());
							log.debug("Encoding: " + itemReference.getEncoding());
//TODO							log.debug("Sessionid: " + itemReference.getSessionId());
							log.debug("Documentid: " + itemReference.getDocumentId());
							log.debug("Itemid: " + itemReference.getItemId());						
						}

					IPCBOManager ipcBoManager =
						(IPCBOManager) userSessionData.getBOM(IPCBOManager.NAME);
					if (ipcBoManager == null) {
						log.error("No IPC BOM in session data!");
						return (mapping.findForward(INTERNAL_ERROR));
					}

					String client = request.getParameter(CLIENT);
					String type = request.getParameter(TYPE);

					customerExit(mapping, form, request, response, userSessionData, requestParser, mbom, multipleInvocation, browserBack);

					
					IPCClient ipcClient = ipcBoManager.createIPCClient(client, type, itemReference);
					// item must exist
					IPCItem ipcItem = ipcClient.getIPCItem(itemReference, false);
					if (ipcItem == null) {
						IPCException ipcException =
							new IPCException(
									"The item "
									+ itemReference.toString()
									+ " could not be found on the IPC server!");
						request.setAttribute(IPC_EXCEPTION, ipcException);
						log.error(this, ipcException);
						return mapping.findForward(INTERNAL_ERROR);
					}
                    
                    // If item is product variant set the productVariant mode to true.
                    setProductVariantMode(uiContext, ipcItem);
                    
					Configuration configuration = ipcItem.getConfiguration();
					if (configuration == null) {
						IPCException ipcException =
							new IPCException(
									"No configuration associated to item "
									+ ipcItem
									+ "!");
						request.setAttribute(InternalRequestParameterConstants.IPC_EXCEPTION, ipcException);
						return mapping.findForward(ActionForwardConstants.INTERNAL_ERROR);
					}
					uiContext.setCurrentConfiguration(configuration);
                    //initialize the context
                    setInitialContextValues(request, configuration);
					uiContext.setCaller(callerParam.getValue().getString());
		
					return mapping.findForward(ActionForwardConstants.SUCCESS);
			
	}
}
