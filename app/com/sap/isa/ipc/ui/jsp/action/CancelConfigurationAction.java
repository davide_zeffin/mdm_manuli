
package com.sap.isa.ipc.ui.jsp.action;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.sap.isa.businessobject.CommunicationException;
import com.sap.isa.core.UserSessionData;
import com.sap.isa.core.businessobject.management.MetaBusinessObjectManager;
import com.sap.isa.core.util.RequestParser;
import com.sap.isa.core.util.RequestParser.Parameter;
import com.sap.isa.ipc.ui.jsp.uiclass.UIContext;
import com.sap.spc.remote.client.object.Configuration;
import com.sap.spc.remote.client.object.ConfigurationSnapshot;
import com.sap.spc.remote.client.object.IPCException;

public class CancelConfigurationAction
    extends ApplySnapshotAction
    implements RequestParameterConstants, ActionForwardConstants {

    /* (non-Javadoc)
     * @see com.sap.isa.isacore.action.EComBaseAction#ecomPerform(org.apache.struts.action.ActionMapping, org.apache.struts.action.ActionForm, javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse, com.sap.isa.core.UserSessionData, com.sap.isa.core.util.RequestParser, com.sap.isa.core.businessobject.management.MetaBusinessObjectManager, boolean, boolean)
     */
    public ActionForward ecomPerform(
        ActionMapping mapping,
        ActionForm form,
        HttpServletRequest request,
        HttpServletResponse response,
        UserSessionData userSessionData,
        RequestParser requestParser,
        MetaBusinessObjectManager mbom,
        boolean multipleInvocation,
        boolean browserBack)
        throws IOException, ServletException, CommunicationException {

        UIContext uiContext = getUIContext(request);
        ConfigurationSnapshot snap = (ConfigurationSnapshot)uiContext.getProperty(SNAPSHOT_INITIAL);
    
        if (snap != null){
            // replace the current config with the initial snapshot
            Configuration initialConfig = applySnapshot(request, uiContext, snap); 
        }
        else {
            // don't replace the configuration
        }

        // find the right forward for the caller
        String caller;
        Parameter callerParam = requestParser.getParameter(RequestParameterConstants.CALLER);
        if (!callerParam.isSet()) {
            callerParam = requestParser.getAttribute(RequestParameterConstants.CALLER);
        }
        if (!callerParam.isSet()) {
            caller = uiContext.getCaller();
        }else {
            caller = null;
        }
        if (!callerParam.isSet() && caller == null) {
            return parameterError(mapping, request, RequestParameterConstants.CALLER, getClass().getName());
        }
        if (callerParam.isSet()) caller = callerParam.getValue().getString();
        ActionForward fwd = DispatchOnCallerAction.findForwardForCaller(mapping, caller);
        
		if (caller.equals(RequestParameterConstants.VEHICLE_MANAGER)) {
			fwd = mapping.findForward(ActionForwardConstants.VEHICLEMANAGER);
		}					

		//Note 1168930
		//START==>
        if( caller.equals(RequestParameterConstants.CALLER_B2B_ORDER) )
            fwd = mapping.findForward( RequestParameterConstants.CALLER_B2B_ORDER );
		//<==END
		
        if (fwd != null){
            return fwd;
        }
        

        String errormsg = "invalid value caller: " + caller;
        log.error(errormsg);
        IPCException ipcException = new IPCException(errormsg);
        setExtendedRequestAttribute(request, InternalRequestParameterConstants.IPC_EXCEPTION, ipcException);
        return mapping.findForward(ActionForwardConstants.INTERNAL_ERROR);
    }

}
