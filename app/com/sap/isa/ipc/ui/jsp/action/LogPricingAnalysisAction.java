package com.sap.isa.ipc.ui.jsp.action;

import java.io.IOException;
import java.util.Iterator;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.sap.isa.businessobject.CommunicationException;
import com.sap.isa.core.UserSessionData;
import com.sap.isa.core.businessobject.management.MetaBusinessObjectManager;
import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.core.util.RequestParser;
import com.sap.isa.ipc.ui.jsp.uiclass.UIContext;
import com.sap.isa.isacore.action.EComBaseAction;
import com.sap.spc.remote.client.object.Configuration;
import com.sap.spc.remote.client.object.IPCItem;
import com.sap.spc.remote.client.object.Instance;

public class LogPricingAnalysisAction extends EComBaseAction {

	protected static IsaLocation log = IsaLocation.getInstance(LogPricingAnalysisAction.class.getName());

	/* (non-Javadoc)
	 * @see com.sap.isa.isacore.action.EComBaseAction#ecomPerform(org.apache.struts.action.ActionMapping, org.apache.struts.action.ActionForm, javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse, com.sap.isa.core.UserSessionData, com.sap.isa.core.util.RequestParser, com.sap.isa.core.businessobject.management.MetaBusinessObjectManager, boolean, boolean)
	 */
	public ActionForward ecomPerform(
		ActionMapping mapping,
		ActionForm form,
		HttpServletRequest request,
		HttpServletResponse response,
		UserSessionData userSessionData,
		RequestParser requestParser,
		MetaBusinessObjectManager mbom,
		boolean multipleInvocation,
		boolean browserBack)
		throws IOException, ServletException, CommunicationException {
			
			UIContext uiContext = IPCBaseAction.getUIContext(request);
			Configuration config = uiContext.getCurrentConfiguration();
			Instance rootInstance = config.getRootInstance();
			IPCItem item = rootInstance.getIpcItem();
				String analysis = item.getPricingAnalysisData();
				log.debug(analysis);				
			Map extPrcConditions = item.getExternalPricingConditions();
			for (Iterator i = extPrcConditions.keySet().iterator(); i.hasNext();) {
				String key = (String)i.next();
				String value = (String)extPrcConditions.get(key);
				log.debug(key + ":" + value);
			}
			
			return mapping.findForward(ActionForwardConstants.SUCCESS);

	}

}
