/*
 * Created on 11.04.2008
 *
 * To change the template for this generated file go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
package com.sap.isa.ipc.ui.jsp.action;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.sap.isa.businessobject.CommunicationException;
import com.sap.isa.core.SessionConst;
import com.sap.isa.core.UserSessionData;
import com.sap.isa.core.businessobject.management.MetaBusinessObjectManager;
import com.sap.isa.core.util.RequestParser;
import com.sap.isa.core.util.RequestParser.Parameter;
import com.sap.isa.ipc.ui.jsp.uiclass.UIContext;

/**
 * @author D048393
 *
 * To change the template for this generated type comment go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
public class GetAdditionalInfoAction extends IPCBaseAction implements  RequestParameterConstants,
																		ActionForwardConstants{

	/* (non-Javadoc)
	 * @see com.sap.isa.isacore.action.EComBaseAction#ecomPerform(org.apache.struts.action.ActionMapping, org.apache.struts.action.ActionForm, javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse, com.sap.isa.core.UserSessionData, com.sap.isa.core.util.RequestParser, com.sap.isa.core.businessobject.management.MetaBusinessObjectManager, boolean, boolean)
	 */
	public ActionForward ecomPerform(
		ActionMapping mapping,
		ActionForm form,
		HttpServletRequest request,
		HttpServletResponse response,
		UserSessionData userSessionData,
		RequestParser requestParser,
		MetaBusinessObjectManager mbom,
		boolean multipleInvocation,
		boolean browserBack)
		throws IOException, ServletException, CommunicationException {
			
		UIContext uiContext = (UIContext)userSessionData.getAttribute(SessionAttributeConstants.UICONTEXT);
		Parameter param;
		boolean switchFlag;
		boolean value;
		
		// Add Info AB
		value = uiContext.getPropertyAsBoolean(EXPAND_ADD_INFO, DEFAULT_ADD_INFO);
		param = requestParser.getFromContext(SWITCH_ADD_INFO);
		switchFlag = param.getValue().getBoolean();
		if(!switchFlag){
			switchFlag = uiContext.getPropertyAsBoolean((SWITCH_ADD_INFO));
		}
		if(switchFlag){
			value = !value;
		}
		uiContext.setProperty(SWITCH_ADD_INFO, false);
		uiContext.setProperty(EXPAND_ADD_INFO, value);
		
		// Prod Var Group
		value = uiContext.getPropertyAsBoolean(EXPAND_PROD_VAR, DEFAULT_PROD_VAR);
		param = requestParser.getFromContext(SWITCH_PROD_VAR);
		switchFlag = param.getValue().getBoolean();
		if(!switchFlag){
			switchFlag = uiContext.getPropertyAsBoolean((SWITCH_PROD_VAR));
			uiContext.setProperty(SWITCH_ADD_INFO, false);
		}
		if(switchFlag){
			value = !value;
		}
		uiContext.setProperty(SWITCH_PROD_VAR, false);
		uiContext.setProperty(EXPAND_PROD_VAR, value);
		
		// Customer Tab group
		value = uiContext.getPropertyAsBoolean(EXPAND_CUST_TAB, DEFAULT_CUST_TAB);
		param = requestParser.getFromContext(SWITCH_CUST_TAB);
		switchFlag = param.getValue().getBoolean();
		if(!switchFlag){
			switchFlag = uiContext.getPropertyAsBoolean((SWITCH_CUST_TAB));
		}
		if(switchFlag){
			value = !value;
		}
		uiContext.setProperty(SWITCH_CUST_TAB, false);
		uiContext.setProperty(EXPAND_CUST_TAB, value);
		
		// trigger further processing	
		return determineForward(mapping, this.getClass().getName(), uiContext);	
	}


	/**
	 * Determines which actions has to be processed next, so that the data that is required by the additional Info Assignment Block will be there
	 * @param mapping
	 * @param source name of caller class, get via this.getClass().getName()
	 * @param uiContext
	 * @return
	 */
	public static ActionForward determineForward(ActionMapping mapping, String source, UIContext uiContext){		
		ActionForward fwd;
		// get xcm settings for Additional Info AB, former Multifuctional Area
		boolean showMFA = uiContext.getPropertyAsBoolean(RequestParameterConstants.SHOW_MULTI_FUNCTIONALITY_AREA)
									&& uiContext.getPropertyAsBoolean(RequestParameterConstants.EXPAND_ADD_INFO);
		boolean showPV = uiContext.getPropertyAsBoolean(RequestParameterConstants.ENABLE_VARIANT_SEARCH)
									&& uiContext.getPropertyAsBoolean(RequestParameterConstants.EXPAND_PROD_VAR);
		boolean showCT = uiContext.getPropertyAsBoolean(RequestParameterConstants.SHOW_CUSTOMER_TAB)
									&& uiContext.getPropertyAsBoolean(RequestParameterConstants.EXPAND_CUST_TAB);
		
		if ( !showPV && !showCT){
			// nothing to show
			showMFA = false; 							
		}
		
		
		if (source.equals(GetAdditionalInfoAction.class.getName())) {
			if(showMFA){
				if(showPV){
					fwd = mapping.findForward(ActionForwardConstants.PRODUCT_VARIANTS);
				}else if(showCT){
					fwd = mapping.findForward(ActionForwardConstants.CUSTOMER_TAB);
				}else{
					fwd = mapping.findForward(ActionForwardConstants.SUCCESS);
				}		
			}else{
				fwd = mapping.findForward(ActionForwardConstants.SUCCESS);	
			}
		   
		}else if (source.equals(GetProductVariantsAction.class.getName())) {
			if(showCT){
				fwd = mapping.findForward(ActionForwardConstants.CUSTOMER_TAB);
			}else{
				fwd = mapping.findForward(ActionForwardConstants.SUCCESS);	
			}
		} else {
			fwd = mapping.findForward(ActionForwardConstants.SUCCESS);
		}
		return fwd;
	}
}
