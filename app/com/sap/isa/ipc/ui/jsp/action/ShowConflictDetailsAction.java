/*
 * Created on 23.11.2004
 *
 * To change the template for this generated file go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
package com.sap.isa.ipc.ui.jsp.action;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.sap.isa.core.UserSessionData;
import com.sap.isa.core.businessobject.management.MetaBusinessObjectManager;
import com.sap.isa.core.util.RequestParser;

/**
 * @author 
 * Is executed when the user clicks on the "Details" link of a characteristic.
 * Forwards to the current layout with the component characteristicDetails in the
 * configworkarea.
 * To change the template for this generated type comment go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
public class ShowConflictDetailsAction extends IPCBaseAction {

	/* (non-Javadoc)
	 * @see com.sap.isa.core.BaseAction#doPerform(org.apache.struts.action.ActionMapping, org.apache.struts.action.ActionForm, javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse)
	 */
	public ActionForward ecomPerform(ActionMapping mapping,
		ActionForm form,
		HttpServletRequest request,
		HttpServletResponse response,
		UserSessionData userSessionData,
		RequestParser requestParser,
		MetaBusinessObjectManager mbom,
		boolean multipleInvocation,
		boolean browserBack)
			throws IOException, ServletException {
				
		setContextValues(request);

//		Navigating into the conflict handler should not cause a buffering of values on the form.
//      The conflict handler changes the state of the configuration, so that the buffer may contain
//      values for characteristics that are not part of the configuration anymore.
//		if (!uiContext.getOnlineEvaluate() && !browserBack) {
//			ActionForward forward = bufferCharacteristicValues(mapping, request, curInstId);
//			if (!forward.getName().equals(ActionForwardConstants.SUCCESS)) {
//				return forward;
//			}
//		}

		customerExit(mapping, form, request, response, userSessionData, requestParser, mbom, multipleInvocation, browserBack);

		return mapping.findForward(ActionForwardConstants.SUCCESS);
	}

}
