/**
 * The action GetInstancesAction returns a list of instances
 * If no instances are found you get an empty list of instances
 * The list of instances is stored in the request;
 * In the jsp page you can access the the list of instances
 * using the following method:
 *
 * List instances = (List)request.getAttribute(RequestParameterConstants.INSTANCES);
 *
 * The action also adds the root instance as currentInstance
 * to the UIContext.
 */
package com.sap.isa.ipc.ui.jsp.action;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.sap.isa.core.Constants;
import com.sap.isa.core.UserSessionData;
import com.sap.isa.core.businessobject.management.MetaBusinessObjectManager;
import com.sap.isa.core.util.RequestParser;
import com.sap.isa.ipc.ui.jsp.uiclass.UIContext;

/**
 * Precondition: StartConfiguration was called in the same http session in order
 * to fill the uiContext with the configuration.
 * Takes an instance id from the request, gets the associated Instance from the
 * Configuration and puts it to the request as an Attribute.
 * Puts all Instances of the configuration into the request as attribute.
 */
public class ChangeInstanceAction extends IPCBaseAction implements RequestParameterConstants,
                                                              ActionForwardConstants {

	public ActionForward ecomPerform(ActionMapping mapping,
		ActionForm form,
		HttpServletRequest request,
		HttpServletResponse response,
		UserSessionData userSessionData,
		RequestParser requestParser,
		MetaBusinessObjectManager mbom,
		boolean multipleInvocation,
		boolean browserBack)
			throws IOException, ServletException {

        UIContext uiContext = getUIContext(request);
        
		//selected instance is the one that has the "selected" status on the display.
		//the user entered values need to be buffered for this.
		String selectedInstanceId = request.getParameter(Constants.SELECTED_INSTANCE_ID);
		if (!isSet(selectedInstanceId)) {
			parameterError(mapping, request, Constants.SELECTED_INSTANCE_ID, getClass().getName());
		}
        
        if (!uiContext.getOnlineEvaluate() && !browserBack) {
            ActionForward forward = bufferCharacteristicValues(mapping, request, selectedInstanceId);
            if (!forward.getName().equals(SUCCESS)) {
                return forward;
            }
        }
        
        // naming "current instance": this is the instance which should be displayed next in the UI.
        // I.e. the user clicks on an instance which he wants to get displayed next. This instance
        // is the "currentInstance" processed in this action.
        String instanceId = request.getParameter(Constants.CURRENT_INSTANCE_ID);
        if (!isSet(instanceId)) {
        	return parameterError(mapping, request, Constants.CURRENT_INSTANCE_ID, getClass().getName());
        }
		changeContextValue(request, Constants.CURRENT_INSTANCE_ID, instanceId);
		setInitialGroupContextValues(request, uiContext.getCurrentConfiguration().getInstance(instanceId));
        //TODO save and restore aktiv group and characteristic dependent on instance
//        Instance instance = uiContext.getCurrentConfiguration().getInstance(instanceId);
//        List groupsList = instance.getCharacteristicGroups(uiContext.getShowInvisibleCharacteristics(), uiContext.getPropertyAsBoolean(RequestParameterConstants.SHOW_GENERAL_TAB_FIRST));
//		Iterator groupsIt = groupsList.iterator();
//        if (groupsIt.hasNext()) {
//        	CharacteristicGroup group = (CharacteristicGroup)groupsIt.next();
//        }
		customerExit(mapping, form, request, response, userSessionData, requestParser, mbom, multipleInvocation, browserBack);


		setExtendedRequestAttribute(request, INSTANCE_CHANGED, "true");
        return processForward(mapping, request, ActionForwardConstants.SUCCESS);       
    }
    
}

