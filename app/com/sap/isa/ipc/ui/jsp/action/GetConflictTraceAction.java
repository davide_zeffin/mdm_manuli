/**
 *  GetConflictsAction returns a list of conflicts
 *  In the jsp page you can access the list of conflicts using
 *  the following method:
 *  List conflicts = (List)request.getAttribute(RequestParameterConstants.CONFLICTS);
 */
package com.sap.isa.ipc.ui.jsp.action;

import java.io.IOException;
import java.util.List;
import java.util.Vector;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.sap.isa.core.UserSessionData;
import com.sap.isa.core.businessobject.management.MetaBusinessObjectManager;
import com.sap.isa.core.util.RequestParser;
import com.sap.isa.ipc.ui.jsp.uiclass.UIContext;
import com.sap.spc.remote.client.object.Configuration;
import com.sap.spc.remote.client.object.IPCException;
import com.sap.spc.remote.client.object.OriginalRequestParameterConstants;


public class GetConflictTraceAction extends IPCBaseAction implements RequestParameterConstants,
                                                              ActionForwardConstants{

	public ActionForward ecomPerform(ActionMapping mapping,
		ActionForm form,
		HttpServletRequest request,
		HttpServletResponse response,
		UserSessionData userSessionData,
		RequestParser requestParser,
		MetaBusinessObjectManager mbom,
		boolean multipleInvocation,
		boolean browserBack)
			throws IOException, ServletException {

		UIContext uiContext = getUIContext(request);

        Configuration currentConfiguration = uiContext.getCurrentConfiguration();
        if(currentConfiguration == null)
          return (mapping.findForward(INTERNAL_ERROR));
		List conflictInfos;
		try{
			if (uiContext.getPropertyAsBoolean(OriginalRequestParameterConstants.SHOW_CONFLICT_TRACE)) {
				conflictInfos = currentConfiguration.getConflictInfos(); 
			}else {
				conflictInfos = new Vector(); 
			}
		}catch (IPCException e){
			setExtendedRequestAttribute(request, IPC_EXCEPTION, e);
			return mapping.findForward(INTERNAL_ERROR);
		}

		customerExit(mapping, form, request, response, userSessionData, requestParser, mbom, multipleInvocation, browserBack);

		request.setAttribute(CONFLICT_TRACE, conflictInfos);

      	return (mapping.findForward(SUCCESS));

	}
}