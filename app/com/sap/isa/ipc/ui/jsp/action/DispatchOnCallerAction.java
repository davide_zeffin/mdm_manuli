/*
 * Created on 13.01.2005
 *
 */
package com.sap.isa.ipc.ui.jsp.action;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.sap.isa.businessobject.CommunicationException;
import com.sap.isa.core.UserSessionData;
import com.sap.isa.core.businessobject.management.MetaBusinessObjectManager;
import com.sap.isa.core.util.RequestParser;
import com.sap.isa.core.util.RequestParser.Parameter;
import com.sap.isa.ipc.ui.jsp.uiclass.UIContext;
import com.sap.spc.remote.client.object.IPCException;

/**
 * @author 
 *
 */
public class DispatchOnCallerAction extends IPCBaseAction {

	/* (non-Javadoc)
	 * @see com.sap.isa.isacore.action.EComBaseAction#ecomPerform(org.apache.struts.action.ActionMapping, org.apache.struts.action.ActionForm, javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse, com.sap.isa.core.UserSessionData, com.sap.isa.core.util.RequestParser, com.sap.isa.core.businessobject.management.MetaBusinessObjectManager, boolean, boolean)
	 */
	public ActionForward ecomPerform(
		ActionMapping mapping,
		ActionForm form,
		HttpServletRequest request,
		HttpServletResponse response,
		UserSessionData userSessionData,
		RequestParser requestParser,
		MetaBusinessObjectManager mbom,
		boolean multipleInvocation,
		boolean browserBack)
		throws IOException, ServletException, CommunicationException {
			
			String caller;
			UIContext uiContext = IPCBaseAction.getUIContext(request);
			Parameter callerParam = requestParser.getParameter(RequestParameterConstants.CALLER);
			if (!callerParam.isSet()) {
				callerParam = requestParser.getAttribute(RequestParameterConstants.CALLER);
			}
			if (!callerParam.isSet()) {
				caller = uiContext.getCaller();
			}else {
				caller = null;
			}
			if (!callerParam.isSet() && caller == null) {
				return parameterError(mapping, request, RequestParameterConstants.CALLER, getClass().getName());
			}
			if (callerParam.isSet()) caller = callerParam.getValue().getString();
			request.setAttribute(RequestParameterConstants.CALLER, caller);
            ActionForward fwd = findForwardForCaller(mapping, caller);

            if (fwd != null){
                return fwd;
            }

					    
		    String errormsg = "invalid value caller: " + caller;
		    log.error(errormsg);
		    IPCException ipcException = new IPCException(errormsg);
			setExtendedRequestAttribute(request, InternalRequestParameterConstants.IPC_EXCEPTION, ipcException);
		    return mapping.findForward(ActionForwardConstants.INTERNAL_ERROR);
	}
    
    /**
     * Returns the ActionForward for the given caller.
     * @param mapping ActionMapping
     * @param caller caller
     * @return action forward for given caller
     */
    public static ActionForward findForwardForCaller(ActionMapping mapping, String caller) {
        ActionForward fwd;
        if (caller.equals(RequestParameterConstants.ORDER_MAINTAIN)) {
        	fwd = mapping.findForward(ActionForwardConstants.CRMORDERMAINTAIN);
        }else if (caller.equals(RequestParameterConstants.ORDER_MAINTAIN_ERP_LRD)) {
			fwd = mapping.findForward(ActionForwardConstants.CRMORDERMAINTAIN_ERP_LRD);
        }else if (caller.equals(RequestParameterConstants.PRODUCT_SIMULATION)) {
        	fwd = mapping.findForward(ActionForwardConstants.CRMPRODUCTSIMULATION);
        }else if (caller.equals(RequestParameterConstants.CRM_PRODUCT_CATALOG)) {
        	fwd = mapping.findForward(ActionForwardConstants.CRM_PRODUCT_CATALOG);
        }else if (caller.equals(RequestParameterConstants.MSA_ORDER)) {
        	fwd = mapping.findForward(ActionForwardConstants.MSAORDERMAINTAIN);
        }else if (caller.equals(RequestParameterConstants.MSA_PRODUCT_SIMULATION)) {
        	fwd = mapping.findForward(ActionForwardConstants.MSAPRODUCTSIMULATION);
        }else if (caller.equals(RequestParameterConstants.MSAONLYCONFIG)) {
        	fwd = mapping.findForward(ActionForwardConstants.MSAONLYCONFIG);
        }else if (caller.equals(RequestParameterConstants.VEHICLE_MANAGER)) {
        	fwd = mapping.findForward(ActionForwardConstants.VEHICLEMANAGER);
        }else if (caller.equals(RequestParameterConstants.ERP_ORDER)) {
        	fwd = mapping.findForward(ActionForwardConstants.ERP_ORDER);
        }else if (caller.equals(RequestParameterConstants.ERP_PRODUCT_SIMULATION)) {
        	fwd = mapping.findForward(ActionForwardConstants.ERP_PRODUCT_SIMULATION);
        }else if (caller.equals(RequestParameterConstants.ERP_BASE)) {
        	fwd = mapping.findForward(ActionForwardConstants.ERP_BASE);
        }else if (caller.equals(RequestParameterConstants.THIRD_PARTY)) {
        	fwd = mapping.findForward(ActionForwardConstants.THIRD_PARTY);
        }else if (caller.equals(RequestParameterConstants.ONLY_CONFIG)) {
        	fwd = mapping.findForward(ActionForwardConstants.ONLYCONFIG);
        }else if (caller.equals(RequestParameterConstants.CALLER_B2B_BASKET)) {
        	fwd = mapping.findForward(ActionForwardConstants.CALLER_B2B_BASKET);
        }else if (caller.equals(RequestParameterConstants.CALLER_B2B_ORDER)) {
        	fwd = mapping.findForward(ActionForwardConstants.CALLER_B2B_ORDER);
        }else if (caller.equals(RequestParameterConstants.CALLER_B2B_ORDERSTATUS)) {
        	fwd = mapping.findForward(ActionForwardConstants.CALLER_B2B_ORDERSTATUS);
        }else if (caller.equals(RequestParameterConstants.CALLER_B2C_BASKET)) {
        	fwd = mapping.findForward(ActionForwardConstants.CALLER_B2C_BASKET);
        }else if (caller.equals(RequestParameterConstants.CALLER_B2C_ORDERSTATUS)) {
        	fwd = mapping.findForward(ActionForwardConstants.CALLER_B2C_ORDERSTATUS);
        }else if (caller.equals(RequestParameterConstants.CALLER_CLOSE_CONFIGURATION)) {
        	fwd = mapping.findForward(ActionForwardConstants.FORWARD_CLOSE_CONFIGURATION);
        }else if (caller.equals(RequestParameterConstants.CALLER_BACK_TO_CATALOG)) {
        	fwd = mapping.findForward(ActionForwardConstants.FW_BACK_TO_CATALOG);
        }else if (caller.equals(RequestParameterConstants.CALLER_B2C_CATALOG)) {
            fwd = mapping.findForward(ActionForwardConstants.CALLER_B2C_CATALOG);
        } else {
            fwd = null;
        }
        return fwd;
    }

}
