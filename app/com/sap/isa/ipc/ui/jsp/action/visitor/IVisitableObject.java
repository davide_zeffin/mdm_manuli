/*
 * Created on 07.03.2008
 *
 * To change the template for this generated file go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
package com.sap.isa.ipc.ui.jsp.action.visitor;

/**
 * @author D048393
 *
 * IVisitableObject Interface according to Visitor Design Pattern
 */
public interface IVisitableObject {
	
	
	/**
	 * Accept method according to Visitor Design Pattern
	 * @param visitor
	 */
	public void accept(IVisitor visitor);

}
