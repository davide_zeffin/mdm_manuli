package com.sap.isa.ipc.ui.jsp.action;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;


import com.sap.isa.core.Constants;
import com.sap.isa.ipc.ui.jsp.beans.MessageUIBean;
import com.sap.isa.ipc.ui.jsp.beans.UIBeanFactory;
import com.sap.isa.ipc.ui.jsp.uiclass.UIContext;
import com.sap.spc.remote.client.object.ComparisonResult;
import com.sap.spc.remote.client.object.Configuration;
import com.sap.spc.remote.client.object.imp.DefaultConfiguration;
import com.sap.spc.remote.client.object.ConfigurationSnapshot;
import com.sap.spc.remote.client.util.ext_configuration;

import com.sap.spc.remote.client.util.imp.c_ext_cfg_imp;

public abstract class CompareConfigurationsAction
    extends IPCBaseAction
    implements RequestParameterConstants, ActionForwardConstants {

    public List compareConfigurations(
        ConfigurationSnapshot snap1, 
        ConfigurationSnapshot snap2,
        HttpServletRequest request){

        List errorMessages = new ArrayList();
        
        if (snap1 == null ||snap2 == null){
            // at least one of the snapshots is null
            MessageUIBean messageBean = UIBeanFactory.getUIBeanFactory().newMessageUIBean();
            messageBean.setMessageStatus(MessageUIBean.ERROR);
            messageBean.setMessageTranslateKey("ipc.message.compare.snap.null");
            errorMessages.add(messageBean);
            return errorMessages;
        }
        
        ext_configuration extConfig1 = snap1.getConfig();
        ext_configuration extConfig2 = snap2.getConfig();
        if (log.isDebugEnabled()) {
            String xml1 = ((c_ext_cfg_imp)extConfig1).cfg_ext_to_xml_string();
            String xml2 = ((c_ext_cfg_imp)extConfig2).cfg_ext_to_xml_string();        
            log.debug("Snap1 (initial/manual snap):");
            log.debug(xml1);
            log.debug("Snap2 (current config):");
            log.debug(xml2);
        }
        
        UIContext uiContext = getUIContext(request);
        Configuration currentConfiguration = uiContext.getCurrentConfiguration();
        
        ComparisonResult result = currentConfiguration.compareConfigurations(extConfig1, extConfig2);
        
        if (result.hasErrorsDuringComparison()){
            // there have been errors during comparison
            List errorKeys = result.getComparisonErrors();
            for (int i=0; i<errorKeys.size(); i++){
                String errorKey = (String)errorKeys.get(i);
                if (errorKey.equals(DefaultConfiguration.EXT_CONFIGS_NULL)){
                    MessageUIBean messageBean = UIBeanFactory.getUIBeanFactory().newMessageUIBean();
                    messageBean.setMessageStatus(MessageUIBean.ERROR);
                    messageBean.setMessageTranslateKey("ipc.message.compare.snap.null");
                    errorMessages.add(messageBean);                    
                }
                else {
                    MessageUIBean messageBean = UIBeanFactory.getUIBeanFactory().newMessageUIBean();
                    messageBean.setMessageStatus(MessageUIBean.ERROR);
                    messageBean.setMessageTranslateKey("ipc.message.compare.error");
                    errorMessages.add(messageBean);                                        
                }
            }
            
        }

        uiContext.setProperty(COMPARISON_RESULT, result);
        uiContext.setProperty(COMPARISON_SNAP1, snap1);
        uiContext.setProperty(COMPARISON_SNAP2, snap2);
       
        return errorMessages;
    }
    
    /**
     * Stores the showAll-flag in the UIContext.
     * @param request
     * @param uiContext
     */
    protected void storeShowAll(HttpServletRequest request, UIContext uiContext) {
        String showAll = request.getParameter(Constants.COMPARISON_SHOW_ALL); 
        if (showAll == null || (showAll != null && showAll.equals(""))){
            // nothing has been passed via the request -> is it in the uiContext?
            showAll = uiContext.getPropertyAsString(COMPARISON_SHOW_ALL_ASSIGNED);
            if (showAll == null || (showAll != null && showAll.equals(""))){
                // it is also not in the uiContext, set the flag to F
                showAll = "F";
            }
        }
        uiContext.setProperty(COMPARISON_SHOW_ALL_ASSIGNED, showAll);
    }
    
    /**
     * Stores the filter-flag in the UIContext.
     * @param request
     * @param uiContext
     */
    protected void storeFilterFlag(HttpServletRequest request, UIContext uiContext) {
        String critical = request.getParameter(Constants.COMPARISON_FILTER_FLAG); 
        if (critical == null || (critical != null && critical.equals(""))){
            // nothing has been passed via the request -> is it in the uiContext?
            critical = uiContext.getPropertyAsString(COMPARISON_FILTER);
            if (critical == null || (critical != null && critical.equals(""))){
                // it is also not in the uiContext, set the flag to F
                critical = "F";
            }
        }
        uiContext.setProperty(COMPARISON_FILTER, critical);
    }


}
