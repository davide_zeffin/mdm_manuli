package com.sap.isa.ipc.ui.jsp.action;

import java.io.IOException;
import java.util.List;
import java.util.Vector;

import javax.servlet.ServletException;
import javax.servlet.ServletInputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.sap.isa.core.Constants;
import com.sap.isa.core.UserSessionData;
import com.sap.isa.core.businessobject.management.MetaBusinessObjectManager;
import com.sap.isa.core.util.RequestParser;
import com.sap.isa.ipc.ui.jsp.beans.MessageUIBean;
import com.sap.isa.ipc.ui.jsp.beans.UIBeanFactory;
import com.sap.isa.ipc.ui.jsp.uiclass.UIContext;
import com.sap.isa.ipc.ui.jsp.util.NumericalIdUtil;
import com.sap.spc.remote.client.object.Configuration;
import com.sap.spc.remote.client.object.IPCClient;
import com.sap.spc.remote.client.object.IPCItem;
import com.sap.spc.remote.client.object.IPCItemReference;
import com.sap.spc.remote.client.object.ImportConfiguration;
import com.sap.spc.remote.client.util.cfg_ext_inst;
import com.sap.spc.remote.client.util.ext_configuration;
import com.sap.spc.remote.client.util.imp.c_ext_cfg_imp;
import com.sap.spc.remote.shared.ConfigIdManager;

/**
 * This action is responsible for the import of a configuration from a file
 * of the local file system. Therefore the Action reads the input stream of the 
 * request at tries to find the section where the file data are stored. 
 * Then a external configuration is created out of this data. 
 * If the product id of the current configuration equals the id of the external 
 * the current configuration is replaced by the external. 
 */
public class ImportConfigurationAction extends IPCBaseAction implements RequestParameterConstants,
																	ActionForwardConstants {
	public ActionForward ecomPerform(ActionMapping mapping,
		ActionForm form,
		HttpServletRequest request,
		HttpServletResponse response,
		UserSessionData userSessionData,
		RequestParser requestParser,
		MetaBusinessObjectManager mbom,
		boolean multipleInvocation,
		boolean browserBack)
			throws IOException, ServletException {
        
        MessageUIBean messageBean = UIBeanFactory.getUIBeanFactory().newMessageUIBean();
        UIContext uiContext = getUIContext(request);
// Import starts a complete new configuration, so buffering the old values does not seem to make sense,
// unless we want to save the state for the case that the import fails and the user whishes to continue
// with his old config. This requirese some extra effort however since the request does not have the
// instance id, nor group, nor characteristic. These would to be added to the import form.        
//		String currentInstId = request.getParameter(Constants.CURRENT_INSTANCE_ID);
//		if (isSet(currentInstId)) {
//			changeContextValue(request, Constants.CURRENT_INSTANCE_ID, currentInstId);		
//		}
//		if (!uiContext.getOnlineEvaluate() && !browserBack) {
//			ActionForward forward = bufferCharacteristicValues(mapping, request, currentInstId);
//			if (!forward.getName().equals(SUCCESS)) {
//				return forward;
//			}
//		}

        List messages = (List) uiContext.getProperty(MESSAGES);
        String securityLevel = (String)uiContext.getProperty(SECURITY_LEVEL);
        // get the old configuration                  
        Configuration oldConfiguration = uiContext.getCurrentConfiguration();
        // initial context values have to be set                                
        setInitialContextValues(request, oldConfiguration);                 

        try {
            if (messages == null){
                messages = new Vector();    
            }
            // check security level: this feature works only if security level = 0
            if (securityLevel!=null && securityLevel.equals("0")){
                String configData = readConfigFromRequest(request);
                if (configData != null && !configData.equals("")){
                    c_ext_cfg_imp extConfig = (c_ext_cfg_imp) c_ext_cfg_imp.cfg_ext_read_data_from_string(configData);
                    Integer rootId = extConfig.get_root_id();
                    cfg_ext_inst rootInstance = extConfig.get_inst(rootId);
                    String instanceNameNew = rootInstance.get_obj_key();
                    String instanceNameOld = oldConfiguration.getRootInstance().getName();
                    boolean areIdsEqual = false;
                    // is one of the ids a numerical id?
                    if (NumericalIdUtil.isNumerical(instanceNameNew)){
                        log.debug("Id "+ instanceNameNew + "is numerical.");
                        // perform equality check for numerical ids
                        areIdsEqual = NumericalIdUtil.compareNumericalIds(instanceNameNew, instanceNameOld);
                    }
                    else{
                        // perform check for non-numerical ids
                        areIdsEqual = instanceNameNew.equals(instanceNameOld);
                    }
                    if (areIdsEqual){
                        IPCClient ipcClient = this.getIPCClient(request);
                        String kbNameOld = oldConfiguration.getKnowledgeBase().getName();
                        String kbVersionOld = oldConfiguration.getKnowledgeBase().getVersion();
                        extConfig.set_kb_name(kbNameOld);
                        extConfig.set_kb_version(kbVersionOld);
                        ext_configuration extConfigCast = (ext_configuration)extConfig;
                        ImportConfiguration newConfiguration = ipcClient.createImportConfiguration(extConfigCast, 
                                                           oldConfiguration.getContextNames(),
                                                           oldConfiguration.getContextValues());
                                                                               
                        if (newConfiguration != null) {
                            Configuration loadedConfig = (Configuration)newConfiguration;
                            setContextValue(request, Constants.CURRENT_INSTANCE_ID, loadedConfig.getRootInstance().getId());
                            // Try to get the documentId from the old configuration.
                            String documentId = ConfigIdManager.getDocumentId(oldConfiguration.getId());
                            if (documentId!=null && !documentId.equals("")){
                                // replace old configuration at item if existing
                                IPCItemReference itemRef = factory.newIPCItemReference();
                                // initialize the new itemRef with the data from the old one (needed for TCP impl.: host, port, etc.)
                                itemRef.init(ipcClient.getItemReference());                                
                                itemRef.setDocumentId(documentId);
                                String itemId = ConfigIdManager.getItemId(oldConfiguration.getId());
                                itemRef.setItemId(itemId);
                                IPCItem ipcItem = ipcClient.getIPCItem(itemRef, false);
                                if (ipcItem != null){
                                    // set the loaded configuration at the item
                                    ipcItem.setConfig(loadedConfig.toExternal());
                                    log.debug("Set loaded configuration at item " + itemId + "of document" + documentId);
                                    // Now get the configuration from the item.
                                    // This is necessary that the configId is the right one. It has to fit to the
                                    // item. Otherwise it would be the sesssion-wide configId (e.g. S1).
                                    loadedConfig = ipcItem.getConfiguration();
                                }
                                else {
                                    log.error("Item " + itemId + "could not be found in document "+ documentId + "!");
                                }
                            }
                            else {
                                log.debug("No document existing.");
                            }
                            // replace the old configuration in UIContext with the loaded one
                            uiContext.setCurrentConfiguration(loadedConfig);
                            // initial context values have to be set for the loaded configuration                        
                            setInitialContextValues(request, loadedConfig);                            
                            
                            // check warning protocol
                            String protocol = newConfiguration.getWarningProtocol();
                            if (protocol == null || protocol.equals("")){
                                messageBean.setMessageStatus(MessageUIBean.SUCCESS);
                                messageBean.setMessageTranslateKey("ipc.message.import.success");
                            } else {
                                messageBean.setMessageStatus(MessageUIBean.WARNING);
                                messageBean.setMessageTranslateKey("ipc.message.import.warning");
                                messageBean.setMessageLongText(protocol);
                            }
                        } else {
                            log.error("Error during creation of imported configuration.");
                            messageBean.setMessageStatus(MessageUIBean.ERROR);
                            messageBean.setMessageTranslateKey("ipc.message.import.error");
                        }
                    } else {
                        messageBean.setMessageStatus(MessageUIBean.ERROR);
                        messageBean.setMessageTranslateKey("ipc.message.import.wrongproduct");
                    }
                } else {
                    messageBean.setMessageStatus(MessageUIBean.ERROR);
                    messageBean.setMessageTranslateKey("ipc.message.import.missingdata");            
                }
            } else {
                log.error("Import/Export feature not allowed if ConfigUI runs in secure-mode.");
                messageBean.setMessageStatus(MessageUIBean.ERROR);
                messageBean.setMessageTranslateKey("ipc.message.import.security");
            }                
        } catch (Exception e) {
            log.error("Exception during import of configuration: " +  e.toString());
            messageBean.setMessageStatus(MessageUIBean.ERROR);
            messageBean.setMessageTranslateKey("ipc.message.import.error");
        }
        messages.add(messageBean);                
        uiContext.setProperty(MESSAGES, messages);

		customerExit(mapping, form, request, response, userSessionData, requestParser, mbom, multipleInvocation, browserBack);

        setExtendedRequestAttribute(request, IMPORT_FLAG, "T");
        return (mapping.findForward(SUCCESS));
    }

    /**
     * @param request
     * @return
     */
    private String readConfigFromRequest(HttpServletRequest request) throws IOException {
        ServletInputStream in = request.getInputStream();
        StringBuffer config = new StringBuffer();

        byte[] line = new byte[128];
        int i = in.readLine(line, 0, 128);
        int boundaryLength = i - 2;
        String boundary = new String(line, 0, boundaryLength); //-2 discards the newline character
        while (i != -1) {
            String newLine = new String(line, 0, i);
            if (newLine.indexOf("Content-Type") != -1) {
                //this is line with the file content-type -> skip it 
                i = in.readLine(line, 0, 128);
                while (i != -1 && !newLine.startsWith(boundary)) {
                    // the problem is the last line of the file content
                    // contains the new line character.
                    // So, we need to check if the current line is
                    // the last line.
                    i = in.readLine(line, 0, 128);
                    newLine = new String(line, 0, i);
                    // found boundary -> so input file is finished
                    if ((i==boundaryLength+2 || i==boundaryLength+4) && (new String(line, 0, i).startsWith(boundary))){
                      return config.toString();
                    }
                    // we don't want to add a line that has only "\r\n"
                    else if (!newLine.equals("\r\n")){
                        config.append(newLine);
                    }
                }
            }
            i = in.readLine(line, 0, 128);
        } // end while
        return config.toString();
    }
}
