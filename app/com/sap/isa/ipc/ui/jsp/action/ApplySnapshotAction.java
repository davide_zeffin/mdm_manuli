
package com.sap.isa.ipc.ui.jsp.action;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Hashtable;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.sap.isa.businessobject.CommunicationException;
import com.sap.isa.core.UserSessionData;
import com.sap.isa.core.businessobject.management.MetaBusinessObjectManager;
import com.sap.isa.core.util.RequestParser;
import com.sap.isa.ipc.ui.jsp.beans.MessageUIBean;
import com.sap.isa.ipc.ui.jsp.beans.UIBeanFactory;
import com.sap.isa.ipc.ui.jsp.uiclass.UIContext;
import com.sap.spc.remote.client.object.Configuration;
import com.sap.spc.remote.client.object.ConfigurationSnapshot;
import com.sap.spc.remote.client.object.IPCClient;
import com.sap.spc.remote.client.object.IPCItem;
import com.sap.spc.remote.client.util.ext_configuration;

public class ApplySnapshotAction
    extends IPCBaseAction
    implements RequestParameterConstants, ActionForwardConstants {

    /* (non-Javadoc)
     * @see com.sap.isa.isacore.action.EComBaseAction#ecomPerform(org.apache.struts.action.ActionMapping, org.apache.struts.action.ActionForm, javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse, com.sap.isa.core.UserSessionData, com.sap.isa.core.util.RequestParser, com.sap.isa.core.businessobject.management.MetaBusinessObjectManager, boolean, boolean)
     */
    public ActionForward ecomPerform(
        ActionMapping mapping,
        ActionForm form,
        HttpServletRequest request,
        HttpServletResponse response,
        UserSessionData userSessionData,
        RequestParser requestParser,
        MetaBusinessObjectManager mbom,
        boolean multipleInvocation,
        boolean browserBack)
        throws IOException, ServletException, CommunicationException {

        UIContext uiContext = getUIContext(request);
        List messages = (List) uiContext.getProperty(MESSAGES);
        if (messages == null){
            messages = new ArrayList();    
        }
        ConfigurationSnapshot snap = (ConfigurationSnapshot)uiContext.getProperty(SNAPSHOT);
        // check whether there is a snapshot
        if(snap == null){
            // no snapshot, use the initial snapshot instead
            snap = (ConfigurationSnapshot)uiContext.getProperty(SNAPSHOT_INITIAL);
            if (snap == null){
                // no initial snapshot available -> create message and return to ConfigUI 
                MessageUIBean errorMsg = UIBeanFactory.getUIBeanFactory().newMessageUIBean();
                errorMsg.setMessageStatus(MessageUIBean.ERROR);
                errorMsg.setMessageTranslateKey("ipc.message.apply.snap.failed");
                messages.add(errorMsg);
                uiContext.setProperty(MESSAGES, messages);
                return mapping.findForward(ActionForwardConstants.SUCCESS);                
            }
        }

        Configuration loadedConfig = applySnapshot(request, uiContext, snap); 
        if (loadedConfig == null){
            MessageUIBean errorMsg = UIBeanFactory.getUIBeanFactory().newMessageUIBean();
            errorMsg.setMessageStatus(MessageUIBean.ERROR);
            errorMsg.setMessageTranslateKey("ipc.message.apply.snap.failed");
            messages.add(errorMsg);
        }
        else {
            MessageUIBean successMsg = UIBeanFactory.getUIBeanFactory().newMessageUIBean();
            successMsg.setMessageStatus(MessageUIBean.SUCCESS);
            successMsg.setMessageTranslateKey("ipc.message.apply.snap.success");
            messages.add(successMsg);
        }
        
        uiContext.setProperty(MESSAGES, messages);        
        return mapping.findForward(ActionForwardConstants.SUCCESS);        
    }
    
    public Configuration applySnapshot(
        HttpServletRequest request,
        UIContext uiContext,
        ConfigurationSnapshot snap) {
                
        // get the old configuration  
        Configuration oldConfiguration = uiContext.getCurrentConfiguration();
        
        //TODO
        //Get old context
        String[] contextNames = uiContext.getCurrentConfiguration().getContextNames();
		String[] contextValues = uiContext.getCurrentConfiguration().getContextValues();
		Hashtable context = new Hashtable(contextNames.length);
		for (int i = 0; i < contextNames.length; i++) {
			context.put(contextNames[i], contextValues[i]);
		}
        // get the new configuration
        ext_configuration extConfig = snap.getConfig();

        Configuration loadedConfig = null;
        // Try to get the item from the old configuration
        IPCItem ipcItem = oldConfiguration.getRootInstance().getIpcItem();
                
        if (ipcItem != null) {
        	// set the context
        	// set the external configuration at the item
            ipcItem.setConfig(extConfig, context);
            log.debug("Set loaded configuration at item " + ipcItem.getItemId() + "of document" + ipcItem.getDocument().getId());
            // Now get the configuration from the item.
            // This is necessary that the configId is the right one. It has to fit to the
            // item. Otherwise it would be the sesssion-wide configId (e.g. S1).
            loadedConfig = ipcItem.getConfiguration();
        }
        else {
            log.debug("No document existing. Create a standalone config.");
            IPCClient ipcClient = this.getIPCClient(request);            
            loadedConfig = ipcClient.createConfiguration(extConfig, 
                                oldConfiguration.getContextNames(),
                                oldConfiguration.getContextValues());
        }
        if (loadedConfig != null){
            loadedConfig.setContextNames(oldConfiguration.getContextNames());
            loadedConfig.setContextValues(oldConfiguration.getContextValues());
            // replace the old configuration in UIContext with the loaded one
            uiContext.setCurrentConfiguration(loadedConfig);
            // initial context values have to be set for the loaded configuration (to avoid
            // a NullPointerException if an instance or group is not existing anymore)                              
            setInitialContextValues(request, loadedConfig);                                        
        }
        return loadedConfig;
    }

}
