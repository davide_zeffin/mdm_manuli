/**
 * The action GetCharacteristicsAction returns a list of characteristics
 * of one instance. The http request you pass to the action
 * should contain the id of the instance as parameter with the
 * name RequestParameterConstants.CURRENT_INSTANCE_ID.
 * Should the request contain no such parameter,
 * the action will return an empty list of characteristics
 * In the jsp page you can access the the list of characteristics
 * using the following method:
 *
 * List charcateristics = (List)request.getAttribute(RequestParameterConstants.CHARACTERISTICS);
 *
 * The action also adds the current instance you sent in your
 * request to the UIContext.
 */
package com.sap.isa.ipc.ui.jsp.action;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.sap.isa.core.UserSessionData;
import com.sap.isa.core.businessobject.management.MetaBusinessObjectManager;
import com.sap.isa.core.util.RequestParser;
import com.sap.isa.ipc.ui.jsp.beans.CharacteristicUIBean;
import com.sap.isa.ipc.ui.jsp.beans.ConfigUIBean;
import com.sap.isa.ipc.ui.jsp.beans.GroupUIBean;
import com.sap.isa.ipc.ui.jsp.beans.InstanceUIBean;
import com.sap.isa.ipc.ui.jsp.constants.CharacteristicsRequestParameterConstants;
import com.sap.isa.ipc.ui.jsp.uiclass.UIContext;
import com.sap.isa.ipc.ui.jsp.dynamicui.IPCDynamicUIConstant;
import com.sap.isa.ipc.ui.jsp.dynamicui.IPCPropertyGroup;
import com.sap.isa.ipc.ui.jsp.dynamicui.IPCUIComponent;
import com.sap.isa.ipc.ui.jsp.dynamicui.IPCUIModel;
import com.sap.isa.ipc.ui.jsp.dynamicui.IPCUIPage;
import com.sap.isa.maintenanceobject.businessobject.UIElementGroup;


public class GetCharacteristicsAction extends IPCBaseAction {
	
	/**
     * Creates a dummy IPCUIModel for test purposes.
     */
    private IPCUIModel createDummyModel(){
		IPCUIModel ui =  new IPCUIModel();
		// page
		UIElementGroup currentPage = new IPCUIPage();
		currentPage.setName("DEFAULT PAGE");
		ui.getPageGroup().addElement(currentPage);
		ui.setActivePageName("DEFAULT PAGE");
		// components
		// comp no.1
		IPCUIComponent comp = new IPCUIComponent();
		comp.setName("1");
		currentPage.addElement(comp);
		comp.setDescription("Utility Contract");
		comp.setRessourceKeyUsed(false);
		// groups for no.1
		// group Customer
		IPCPropertyGroup group = new IPCPropertyGroup();
		group.setName("Customer");
		comp.addElement(group);
		group.setDescription("Customer");
		group.setRessourceKeyUsed(false);
		// group Products
		IPCPropertyGroup group2 = new IPCPropertyGroup();
		group2.setName("Products");
		comp.addElement(group2);
		group2.setDescription("Products");
		group2.setRessourceKeyUsed(false);
		// group Contract
		IPCPropertyGroup group3 = new IPCPropertyGroup();
		group3.setName("Contract");
		comp.addElement(group3);
		group3.setDescription("Contract");
		group3.setRessourceKeyUsed(false);
		// group $BASE_GROUP
		IPCPropertyGroup group4 = new IPCPropertyGroup();
		group4.setName("$BASE_GROUP");
		comp.addElement(group4);
		group4.setDescription("$BASE_GROUP");
		group4.setRessourceKeyUsed(false);
		// comp no.2
		IPCUIComponent comp2 = new IPCUIComponent();
		comp2.setName("2");
		currentPage.addElement(comp2);
		comp2.setDescription("Electricity Contract");
		comp2.setRessourceKeyUsed(false);
		// groups for no.2
		// group $BASE_GROUP
		IPCPropertyGroup group5 = new IPCPropertyGroup();
		group5.setName("$BASE_GROUP");
		comp2.addElement(group5);
		group5.setDescription("$BASE_GROUP");
		group5.setRessourceKeyUsed(false);
		// comp no.3
		IPCUIComponent comp3 = new IPCUIComponent();
		comp3.setName("3");
		currentPage.addElement(comp3);
		comp3.setDescription("Gas Contract");
		comp3.setRessourceKeyUsed(false);
		// groups for no.3
		// group Contract
		IPCPropertyGroup group6 = new IPCPropertyGroup();
		group6.setName("Contract");
		comp3.addElement(group6);
		group6.setDescription("Contract");
		group6.setRessourceKeyUsed(false);

		return ui;
		
	}


    /**
     * Enables subclasses to modify the list of characteristicUIBeans
     * retrieved by this action and added to the request. Allows
     * to add additional data to the http request.
     * @param form
     * @param request
     * @param uiContext
     * @param currentInstance
     * @param currentCharGroup
     * @return
     */
    protected String customerExitParseRequest(
        ActionForm form,
        HttpServletRequest request,
        UIContext uiContext,
        InstanceUIBean currentInstanceUIBean,
        GroupUIBean currentGroupUIBean) {
        	//avoid compiler warnings of unused parameter
        	if (form == null);
        	if (request == null);
        	if (uiContext == null);
        	if (currentInstanceUIBean == null);
        	if (currentGroupUIBean == null);
            return null;
    }

	/**
	 * Enables subclasses to modify the list of characteristicUIBeans
	 * retrieved by this action and added to the request. Allows
	 * to add additional data to the http request.
	 * @param form
	 * @param request
	 * @param uiContext
	 * @param currentConfiguration
	 * @param currentCharGroup
	 * @return
	 */
	protected String customerExitParseRequest(
		ActionForm form,
		HttpServletRequest request,
		UIContext uiContext,
		ConfigUIBean currentConfiguration) {
			//avoid compiler warnings of unused parameter
			if (form == null);
			if (request == null);
			if (uiContext == null);
			if (currentConfiguration == null);
			return null;
	}

    /**
     * @param form
     * @param request
     * @param uiContext
     * @param currentInstance
     * @param currentCharGroup
     * @param currentCharacteristic
     */
    protected void customerExitGetCharacteristics(
        ActionForm form,
        HttpServletRequest request,
        UIContext uiContext,
        InstanceUIBean currentInstance,
        GroupUIBean currentCharGroup,
        CharacteristicUIBean currentCharacteristic
        ) {
			//avoid compiler warnings of unused parameter
			if (form == null);
			if (request == null);
			if (uiContext == null);
			if (currentInstance == null);
			if (currentCharGroup == null);
			if (currentCharacteristic == null);
    }

	/**
	 * @param form
	 * @param request
	 * @param uiContext
	 * @param currentConfiguration
	 */
	protected void customerExitGetCharacteristics(
		ActionForm form,
		HttpServletRequest request,
		UIContext uiContext,
		ConfigUIBean currentConfiguration
		) {
			//avoid compiler warnings of unused parameter
			if (form == null);
			if (request == null);
			if (uiContext == null);
			if (currentConfiguration == null);
	}
    
    /**
     * @param form
     * @param request
     * @param uiContext
     * @param currentInstance
     * @param currentCharGroup
     * @return
     */
    protected String customerExitDetermineForward(
        ActionForm form,
        HttpServletRequest request,
        UIContext uiContext,
        InstanceUIBean currentInstanceUIBean,
        GroupUIBean currentGroupUIBean) {
			if (form == null);
			if (request == null);
			if (uiContext == null);
			if (currentInstanceUIBean == null);
			if (currentGroupUIBean == null);
            return null;
    }

	/**
	 * @param form
	 * @param request
	 * @param uiContext
	 * @param currentConfig
	 * @return
	 */
	protected String customerExitDetermineForward(
		ActionForm form,
		HttpServletRequest request,
		UIContext uiContext,
		ConfigUIBean currentConfigUIBean) {
			if (form == null);
			if (request == null);
			if (uiContext == null);
			if (currentConfigUIBean == null);
			return null;
	}

	public ActionForward ecomPerform(ActionMapping mapping,
			ActionForm form,
			HttpServletRequest request,
			HttpServletResponse response,
			UserSessionData userSessionData,
			RequestParser requestParser,
			MetaBusinessObjectManager mbom,
			boolean multipleInvocation,
			boolean browserBack)
				throws IOException, ServletException {
		
			UIContext uiContext = getUIContext(request);
			if(uiContext == null)
				return (mapping.findForward(ActionForwardConstants.INTERNAL_ERROR));

			ConfigUIBean currentConfigUIBean = getCurrentConfiguration(uiContext);

			// initialize the hierarchy of InstanceUIBeans
			InstanceUIBean rootInstance = createInstanceUIBeans(uiContext, currentConfigUIBean, request);

			// searchSet handling: 
			// group is used for prefetch, cstic is used to set lastFocused accesskey
			GroupUIBean searchSetGroup = (GroupUIBean) request.getAttribute(InternalRequestParameterConstants.SEARCH_SET_FOUND_GROUP);
			String searchSetGroupName = null;
			if (searchSetGroup != null){
				searchSetGroupName = searchSetGroup.getName();
				CharacteristicUIBean searchSetCstic = (CharacteristicUIBean) request.getAttribute(InternalRequestParameterConstants.SEARCH_SET_FOUND_CSTIC);
				if (searchSetCstic != null){
					searchSetCstic.setLastFocused(true);				
				}
			}

			// Try to get the IPCUIModel from uicontext. It might be null!
			IPCUIModel ui = (IPCUIModel) uiContext.getProperty(IPCDynamicUIConstant.SESSION_DYNAMIC_UI); 

	        // do we have to show groups? 
	        boolean showGroups =
	            uiContext.getPropertyAsBoolean(
	                RequestParameterConstants.USE_GROUP_INFORMATION);
			
			// prefetch info from DynamicUI framework 
        	ArrayList relevantInstances = IPCUIModel.prefetch(ui, uiContext, currentConfigUIBean, searchSetGroupName, showGroups);

			// create the beans only for relevant instances
			for (int i=0; i<relevantInstances.size(); i++){
				ArrayList relInst = (ArrayList)relevantInstances.get(i);
				InstanceUIBean currentInst = currentConfigUIBean.getInstance((String)relInst.get(0));
				HashMap relevantGroups = (HashMap)relInst.get(1);
				createGroupUIBeans(uiContext, currentInst, relevantGroups, request);
							
			}



			String customerForward =
				customerExitParseRequest(
					form,
					request,
					uiContext,
					currentConfigUIBean);

			//customer exit
			customerExitGetCharacteristics(
				form,
				request,
				uiContext,
				currentConfigUIBean);   
	                     
			request.setAttribute(CharacteristicsRequestParameterConstants.CURRENT_CONFIGURATION, currentConfigUIBean);			                     

			customerForward =
				customerExitDetermineForward(
					form,
					request,
					uiContext,
					currentConfigUIBean);
			
			//search for a forward in the request
			//if nothing found, forward to succes
			if (customerForward != null) {
				return mapping.findForward(customerForward);
			}else {
				return processForward(mapping, request, ActionForwardConstants.MERGE);
			}
	}
}

