package com.sap.isa.ipc.ui.jsp.action;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import java.util.ArrayList;
import com.sap.isa.core.Constants;
import com.sap.isa.core.UserSessionData;
import com.sap.isa.core.businessobject.management.MetaBusinessObjectManager;
import com.sap.isa.core.util.RequestParser;
import com.sap.isa.ipc.ui.jsp.beans.CharacteristicUIBean;
import com.sap.isa.ipc.ui.jsp.beans.ConfigUIBean;
import com.sap.isa.ipc.ui.jsp.beans.GroupUIBean;
import com.sap.isa.ipc.ui.jsp.beans.InstanceUIBean;
import com.sap.isa.ipc.ui.jsp.beans.UIBeanFactory;
import com.sap.isa.ipc.ui.jsp.beans.ValueUIBean;
import com.sap.isa.ipc.ui.jsp.constants.CharacteristicDetailsRequestParameterConstants;
import com.sap.isa.ipc.ui.jsp.dynamicui.IPCDynamicUIConstant;
import com.sap.isa.ipc.ui.jsp.dynamicui.IPCProperty;
import com.sap.isa.ipc.ui.jsp.dynamicui.IPCUIComponent;
import com.sap.isa.ipc.ui.jsp.dynamicui.IPCUIModel;
import com.sap.isa.ipc.ui.jsp.dynamicui.IPCUIPage;
import com.sap.isa.ipc.ui.jsp.uiclass.UIContext;
import com.sap.spc.remote.client.object.Characteristic;
import com.sap.spc.remote.client.object.CharacteristicGroup;

public class GetDetailsAction extends IPCBaseAction {

	public ActionForward ecomPerform(ActionMapping mapping,
		ActionForm form,
		HttpServletRequest request,
		HttpServletResponse response,
		UserSessionData userSessionData,
		RequestParser requestParser,
		MetaBusinessObjectManager mbom,
		boolean multipleInvocation,
		boolean browserBack)
			throws IOException, ServletException {

		UIContext uiContext = getUIContext(request);
//		if (!uiContext.getOnlineEvaluate() && !browserBack) {
//			ActionForward forward =
//				bufferCharacteristicValues(mapping, request);
//			if (!forward.getName().equals(ActionForwardConstants.SUCCESS)) {
//				return forward;
//			}
//		}

		ConfigUIBean currentConfigUIBean =
			this.getCurrentConfiguration(uiContext);

		// Do this to transfer the current instance and characteristic into the ui context
		//Instance currentInstance = ObjectFinder.getCurrentInstance(request,uiContext);
		String currentInstanceId =
			(String) request.getParameter(
				Constants.CURRENT_INSTANCE_ID);
		if (!isSet(currentInstanceId)) {
			currentInstanceId = getContextValue(request, Constants.CURRENT_INSTANCE_ID);
		}
		InstanceUIBean currentInstanceUIBean;
		if (currentInstanceId == null) {
			currentInstanceUIBean =
				this.getCurrentInstance(
					uiContext,
					currentConfigUIBean,
					currentConfigUIBean
						.getBusinessObject()
						.getRootInstance()
						.getId(),
					request);
		} else {
			currentInstanceUIBean =
				this.getCurrentInstance(
					uiContext,
					currentConfigUIBean,
					currentInstanceId,
					request);
		}

		String currentCharacteristicGroupName =
			(String) request.getParameter(
				Constants
					.CURRENT_CHARACTERISTIC_GROUP_NAME);
		if (!isSet(currentCharacteristicGroupName)) {
			currentCharacteristicGroupName = getContextValue(request, Constants.CURRENT_CHARACTERISTIC_GROUP_NAME);
		}
		if (currentCharacteristicGroupName != null) {
    		CharacteristicGroup currentCharacteristicGroup =
    			currentInstanceUIBean.getBusinessObject().getCharacteristicGroup(
    				currentCharacteristicGroupName);
    		GroupUIBean currentGroupBean =
    		UIBeanFactory.getUIBeanFactory().newGroupUIBean(
    				currentCharacteristicGroup,
    				uiContext);
    		currentInstanceUIBean.addGroup(currentGroupBean);
    
    		String currentCharacteristicName =
    			(String) request.getParameter(
    				Constants.CURRENT_CHARACTERISTIC_NAME);
    		if (!isSet(currentCharacteristicName)) {
    			currentCharacteristicName = getContextValue(request, Constants.CURRENT_CHARACTERISTIC_NAME);
    		}
    		if (currentCharacteristicName == null) {
    			return parameterError(
    				mapping,
    				request,
    				Constants.CURRENT_CHARACTERISTIC_NAME,
    				this.getClass().getName());
    		}
    		boolean showInvisibleCstics =
    			uiContext.getShowInvisibleCharacteristics();
    		Characteristic currentCharacteristic =
    			currentInstanceUIBean.getBusinessObject().getCharacteristic(
    				currentCharacteristicName,
    				showInvisibleCstics);
            
            boolean showMessageCstic = uiContext.getPropertyAsBoolean(ProcessRequestParameterAction.SHOW_MESSAGE_CSTICS);                
            String prefix = uiContext.getPropertyAsString(ProcessRequestParameterAction.MESSAGE_CSTICS_PREFIX);
            CharacteristicUIBean characteristicUIBean;
            if (showMessageCstic && currentCharacteristic.getName().startsWith(prefix)){
                characteristicUIBean = CharacteristicUIBean.getInstance(request, currentCharacteristic, uiContext, true);
            } 
            else {
                characteristicUIBean = CharacteristicUIBean.getInstance(request, currentCharacteristic, uiContext);
            }                        
    		currentInstanceUIBean.addCharacteristic(characteristicUIBean);
    		currentGroupBean.addCharacteristic(characteristicUIBean);
    
    		getCharacteristicValues(request, uiContext, currentInstanceUIBean, currentGroupBean);
    
            // We need to select the right values if values have been changed without updating the configuration.
            checkBufferedValues(
                uiContext,
                currentInstanceId,
                characteristicUIBean);
    
    		customerExit(mapping, form, request, response, userSessionData, requestParser, mbom, multipleInvocation, browserBack);
    		
    		request.setAttribute(
    		    CharacteristicDetailsRequestParameterConstants.CURRENT_INSTANCE, currentInstanceUIBean);
    		request.setAttribute(
    			CharacteristicDetailsRequestParameterConstants
    				.CURRENT_CHARACTERISTIC_GROUP,
    			currentGroupBean);
    		request.setAttribute(
    			CharacteristicDetailsRequestParameterConstants
    				.CURRENT_CHARACTERISTIC,
    			characteristicUIBean);
                
            // necessary for message-cstics
            String curValueName = getContextValue(request, Constants.CURRENT_VALUE_NAME);        
            request.setAttribute(CharacteristicDetailsRequestParameterConstants.CURRENT_VALUE, curValueName);
        }            
		return mapping.findForward(ActionForwardConstants.SUCCESS);
	}

    /**
     * If there are buffered values that have not been sent to the engine we need to update the ValueUIBeans 
     * with the correct "selected" information.
     * Example: 
     * XCM-parameter behavior.onlineevaluate is set to F.
     * The users changes a value but does not submit this value to the cfg-engine (pressing return-key or check-
     * button). Then he navigates to the details-page. He would expect that the value that he selected on the 
     * main screen is also selected on the details page. But the details-page only uses the UIBean, which reflect
     * the state of the cfg-engine. In such a case the "selected" information needs to be adapted to the information
     * in the DynamicUI-framework buffer.
     * @param uiContext
     * @param currentInstanceId
     * @param characteristicUIBean
     */
    private void checkBufferedValues(
        UIContext uiContext,
        String currentInstanceId,
        CharacteristicUIBean characteristicUIBean) {
        if (!uiContext.getOnlineEvaluate()){
            IPCUIModel ui = (IPCUIModel) uiContext.getProperty(IPCDynamicUIConstant.SESSION_DYNAMIC_UI);
            if (ui != null){
                IPCUIPage activePage = (IPCUIPage) ui.getActivePage();
                IPCUIComponent comp = (IPCUIComponent)activePage.getElement(currentInstanceId);
                if (comp != null){
                    IPCProperty property = comp.getIPCProperty(characteristicUIBean.getName());
                    if (property != null){
                        if (property.isValueChanged()){
                            // Values have been changed in frw-object but not yet in the config-object
                            // Loop over the values of config-object and set the "selected" flag according to the buffer.
                            ArrayList values = characteristicUIBean.getValues();
                            for (int i=0; i<values.size(); i++){
                                ValueUIBean val = (ValueUIBean)values.get(i);
                                boolean selected = property.isValueAlreadyAssigned(val.getName());
                                if (val.isAssigned()){
                                    if (selected){
                                        // frw-object and cfg-object are both "selected": nothing to do
                                    }
                                    else {
                                        // frw-object is not selected but cfg-object: set selectedBuffer flag to "not selected"
                                        // (buffer overwrites config-model-state!) 
                                        val.setBufferedSelected(ValueUIBean.BUFFER_NOT_SELECTED);
                                    }
                                }
                                else {
                                    if (selected){
                                        // frw-object is selected but not cfg-object: set selectedBuffer flag to "selected"
                                        // (buffer overwrites config-model-state!) 
                                        val.setBufferedSelected(ValueUIBean.BUFFER_SELECTED);
                                    }
                                    else {
                                        // frw-object and cfg-object are both "selected": nothing to do
                                    }
                                }
                            }
                        }
                    }
                }                    
            }
        }
    }
}
