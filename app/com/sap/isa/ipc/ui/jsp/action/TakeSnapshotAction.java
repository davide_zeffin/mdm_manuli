package com.sap.isa.ipc.ui.jsp.action;

import java.io.IOException;
import java.util.List;
import java.util.Vector;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.sap.isa.businessobject.CommunicationException;
import com.sap.isa.core.Constants;
import com.sap.isa.core.UserSessionData;
import com.sap.isa.core.businessobject.management.MetaBusinessObjectManager;
import com.sap.isa.core.util.RequestParser;
import com.sap.isa.ipc.ui.jsp.beans.MessageUIBean;
import com.sap.isa.ipc.ui.jsp.beans.UIBeanFactory;
import com.sap.isa.ipc.ui.jsp.uiclass.UIContext;
import com.sap.spc.remote.client.object.Configuration;
import com.sap.spc.remote.client.object.Instance;
import com.sap.spc.remote.client.object.imp.DefaultConfigurationSnapshot;

public class TakeSnapshotAction
    extends IPCBaseAction
    implements RequestParameterConstants, ActionForwardConstants {

    /* (non-Javadoc)
     * @see com.sap.isa.isacore.action.EComBaseAction#ecomPerform(org.apache.struts.action.ActionMapping, org.apache.struts.action.ActionForm, javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse, com.sap.isa.core.UserSessionData, com.sap.isa.core.util.RequestParser, com.sap.isa.core.businessobject.management.MetaBusinessObjectManager, boolean, boolean)
     */
    public ActionForward ecomPerform(
        ActionMapping mapping,
        ActionForm form,
        HttpServletRequest request,
        HttpServletResponse response,
        UserSessionData userSessionData,
        RequestParser requestParser,
        MetaBusinessObjectManager mbom,
        boolean multipleInvocation,
        boolean browserBack)
        throws IOException, ServletException, CommunicationException {

        UIContext uiContext = getUIContext(request);
        
        // store current*-/selected*-parameters in order to keep the current UI-state
        String currentInstId = request.getParameter(Constants.CURRENT_INSTANCE_ID);
        if (isSet(currentInstId)) {
            changeContextValue(request, Constants.CURRENT_INSTANCE_ID, currentInstId);      
        }
        String currentGroupName = request.getParameter(Constants.CURRENT_CHARACTERISTIC_GROUP_NAME);
        if (isSet(currentGroupName)) {
            setContextValue(request, Constants.CURRENT_CHARACTERISTIC_GROUP_NAME, currentGroupName);
        }
        String curScrollGroupName = request.getParameter(Constants.CURRENT_SCROLL_CHARACTERISTIC_GROUP_NAME);
        if (isSet(curScrollGroupName)) {
            setContextValue(request, Constants.CURRENT_SCROLL_CHARACTERISTIC_GROUP_NAME, curScrollGroupName);
        }
        String currentCharacteristicName = request.getParameter(Constants.CURRENT_CHARACTERISTIC_NAME);
        if (isSet(currentCharacteristicName)) {
            setContextValue(request, Constants.CURRENT_CHARACTERISTIC_NAME, currentCharacteristicName);
        }
        String selectedMfaTabName = request.getParameter(Constants.SELECTED_MFA_TAB_NAME);
        if (isSet(selectedMfaTabName)) {
            setContextValue(request, Constants.SELECTED_MFA_TAB_NAME, selectedMfaTabName);
        }

        List messages = (List) uiContext.getProperty(MESSAGES);
        if (messages == null){
            messages = new Vector();    
            uiContext.setProperty(MESSAGES, messages);
        }
        
        MessageUIBean messageBean = UIBeanFactory.getUIBeanFactory().newMessageUIBean();
        messages.add(messageBean);
        // check whether feature is disabled
        if (!uiContext.getPropertyAsBoolean(ENABLE_SNAPSHOT, false)) {
            // create an error message
            messageBean.setMessageStatus(MessageUIBean.ERROR);
            messageBean.setMessageTranslateKey("ipc.message.snapshot.disabled");
            return (mapping.findForward(SUCCESS));
        }

        Configuration currentConfig = uiContext.getCurrentConfiguration();
        Instance rootInstance = currentConfig.getRootInstance();
        String rootInstancePrice = rootInstance.getPrice();
        // take the snapshot
        DefaultConfigurationSnapshot snap = factory.newConfigurationSnapshot(currentConfig, "", rootInstancePrice, false);
        // add it to the uiContext
        uiContext.setProperty(SNAPSHOT, snap);
        // create a message
        messageBean.setMessageStatus(MessageUIBean.SUCCESS);
        messageBean.setMessageTranslateKey("ipc.message.snapshot.success");
        
        customerExit(mapping, form, request, response, userSessionData, requestParser, mbom, multipleInvocation, browserBack);
        
        return (mapping.findForward(SUCCESS));


    }

}
