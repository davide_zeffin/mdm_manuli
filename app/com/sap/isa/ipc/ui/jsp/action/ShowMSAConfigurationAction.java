package com.sap.isa.ipc.ui.jsp.action;


import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.sap.isa.core.Constants;
import com.sap.isa.core.UserSessionData;
import com.sap.isa.core.businessobject.management.MetaBusinessObjectManager;
import com.sap.isa.core.util.RequestParser;
import com.sap.isa.ipc.ui.jsp.uiclass.UIContext;
import com.sap.spc.remote.client.object.Configuration;
import com.sap.spc.remote.client.object.IPCClient;
import com.sap.spc.remote.client.object.IPCException;
import com.sap.spc.remote.client.object.IPCItem;
import com.sap.spc.remote.client.object.IPCItemReference;
import com.sap.spc.remote.client.object.imp.tcp.TCPIPCItemReference;

public class ShowMSAConfigurationAction extends ShowConfigurationAction {

    public ActionForward ecomPerform(ActionMapping mapping,
        ActionForm form,
        HttpServletRequest request,
        HttpServletResponse response,
        UserSessionData userSessionData,
        RequestParser requestParser,
        MetaBusinessObjectManager mbom,
        boolean multipleInvocation,
        boolean browserBack)
            throws IOException, ServletException {

        IPCClient ipcClient = this.getIPCClient(request);
        UIContext uiContext = getUIContext(request);

        /*
         *  First search for an ipcItemReference or try to create one.
         */
        IPCItemReference ipcItemReference = (IPCItemReference) request.getAttribute(IPC_ITEM_REFERENCE);
        if (ipcItemReference != null){
            // so we are in scenario 1 and the work will be done by the caller
        }
        else{

            // so, now we are in scenario 2, 3 or 4

            String host = getParameter(request, IPC_HOST);
            String port = getParameter(request, IPC_PORT);
            String tokenId = getParameter(request, IPC_TOKENID);
            String documentId = getParameter(request, DOCUMENTID);
            String itemId = getParameter(request, ITEMID);


            if(host != null && !host.equals("") &&
               port != null && !port.equals("") &&
               documentId != null && !documentId.equals("") &&
               itemId != null && !itemId.equals("")){

                // scenario 3, MSA
                ipcItemReference = new TCPIPCItemReference();
                ((TCPIPCItemReference)ipcItemReference).setHost(host);
                ((TCPIPCItemReference)ipcItemReference).setPort(port);
                ipcItemReference.setDocumentId(documentId);
                ipcItemReference.setItemId(itemId);
                ((TCPIPCItemReference)ipcItemReference).setEncoding(UNICODE_LITTLE);
                ((TCPIPCItemReference)ipcItemReference).setDispatcher(true);

                request.setAttribute(IPC_ITEM_REFERENCE, ipcItemReference);
            }
        }

        try{

            IPCItem ipcItem = ipcClient.getIPCItem(ipcItemReference);
            if (ipcItem == null) return(mapping.findForward(INTERNAL_ERROR));

            Configuration configuration = ipcItem.getConfiguration();
            if (configuration == null) return(mapping.findForward(INTERNAL_ERROR));

            uiContext.setCurrentConfiguration(configuration);
            //initialize the context with the id of the root instance
            setContextValue(request, Constants.CURRENT_INSTANCE_ID, configuration.getRootInstance().getId());
            uiContext.setCurrentProductVariant(null);
            uiContext.setCurrentIpcException(null);
            uiContext.setCurrentMimeObjectContainer(null);

        }
        catch (IPCException ipcException){
            log.fatal(this, ipcException);
            request.setAttribute(IPC_EXCEPTION, ipcException);
            return (mapping.findForward(INTERNAL_ERROR));
        }


		customerExit(mapping, form, request, response, userSessionData, requestParser, mbom, multipleInvocation, browserBack);

        return processForward(mapping, request, ActionForwardConstants.SUCCESS);    }

    private String getParameter(HttpServletRequest request, String key){
      String value = null;
      value = request.getParameter(key);
      if(value == null)
        return (String)request.getAttribute(key);
      return value;
    }

}
