/*
 * Created on 14.07.2004
 *
 * To change the template for this generated file go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
package com.sap.isa.ipc.ui.jsp.uiclass;

import java.io.IOException;
import java.util.Hashtable;
import java.util.List;
import java.util.Stack;

import javax.servlet.ServletException;
import javax.servlet.jsp.PageContext;

import com.sap.isa.core.logging.IsaLocation;
import com.sap.spc.remote.client.object.IPCException;

/**
 * @author 
 *
 * To change the template for this generated type comment go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
public class ConflictSolverUI extends IPCBaseUI {
    
    //helper instance in order to create the parameter objects
    private static ConflictSolverUI conflictSolverUI = new ConflictSolverUI();
    private static IsaLocation loc = IsaLocation.getInstance(ConflictUI.class.getName());
    
    public static void include(
        PageContext pageContext,
        String conflictSolverPage,
        UIContext ipcBaseUI,
        List conflicts,
        Hashtable customerParams) {

            IncludeParams includeParams =
                conflictSolverUI.new IncludeParams(
                    ipcBaseUI,
                    conflicts,
                    customerParams);
            Stack paramStack = IPCBaseUI.getParamStack(pageContext);
            paramStack.push(includeParams);
            try {
                pageContext.include(conflictSolverPage);
            } catch (ServletException e) {
                loc.error(IPCEX70 + conflictSolverPage, e.getRootCause());
                throw new RuntimeException(IPCEX50, e.getRootCause());
//TODO:Exceptionhandling                throw new IPCException(IPCEX, null, e);
            } catch (IOException e) {
                loc.error(IPCEX70 + conflictSolverPage, e);
                throw new RuntimeException(IPCEX51, e);
//TODO:Exceptionhandling                throw new IPCException(IPCEX, null, e);
            }
            paramStack.pop();
    }
    
    public static IncludeParams getIncludeParams(PageContext pageContext) {
        IncludeParams includeParams = null;
        try{
            includeParams = (IncludeParams)IPCBaseUI.getBaseIncludeParams(pageContext);
        }catch (ClassCastException e) {
            throw new IPCException(IPCEX52);
        }
        return includeParams;
    }
    
    public class IncludeParams extends IPCBaseUI.IncludeParams {
        //TODO: remove access to business object
        private List conflicts;

        /**
         * @param ipcBaseUI
         */
        IncludeParams(
            UIContext ipcBaseUI,
            List conflicts,
            Hashtable customerParams) {
            super(ipcBaseUI, customerParams);
            this.conflicts = conflicts;
        }
        
		/**
		 * @return
		 */
		public List getConflicts() {
			return conflicts;
		}

    }
    
}
