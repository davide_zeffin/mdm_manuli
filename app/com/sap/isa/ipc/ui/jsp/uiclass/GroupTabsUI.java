/*
 * Created on 14.07.2004
 *
 * To change the template for this generated file go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
package com.sap.isa.ipc.ui.jsp.uiclass;

import java.io.IOException;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.List;
import java.util.Stack;

import javax.servlet.ServletException;
import javax.servlet.jsp.PageContext;

import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.ipc.ui.jsp.beans.GroupUIBean;
import com.sap.isa.ipc.ui.jsp.beans.InstanceUIBean;
import com.sap.spc.remote.client.object.IPCException;

/**
 * @author 
 *
 * To change the template for this generated type comment go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
public class GroupTabsUI extends IPCBaseUI {
    
    //helper instance in order to create the parameter objects
    private static GroupTabsUI characteristicUI = new GroupTabsUI();
    private static IsaLocation loc = IsaLocation.getInstance(GroupTabsUI.class.getName());
    
    public static void include(
        PageContext pageContext,
        String groupTabsPage,
        UIContext ipcBaseUI,
        InstanceUIBean instance,
        GroupUIBean group,
        GroupUIBean scrollGroup,
        Hashtable customerParams) {

            IncludeParams includeParams =
                characteristicUI.new IncludeParams(
                    ipcBaseUI,
                    instance,
                    group,
                    scrollGroup,
                    customerParams);
            Stack paramStack = IPCBaseUI.getParamStack(pageContext);
            paramStack.push(includeParams);
            try {
                pageContext.include(groupTabsPage);
            } catch (ServletException e) {
                loc.error(IPCEX70 + groupTabsPage, e.getRootCause());
                throw new RuntimeException(IPCEX, e.getRootCause());
//TODO:Exceptionhandling                throw new IPCException(IPCEX, null, e);
            } catch (IOException e) {
                loc.error(IPCEX70 + groupTabsPage, e);
                throw new RuntimeException(IPCEX, e);
//TODO:Exceptionhandling                throw new IPCException(IPCEX, null, e);
            }
            paramStack.pop();
    }
    
    public static IncludeParams getIncludeParams(PageContext pageContext) {
        IncludeParams includeParams = null;
        try{
            includeParams = (IncludeParams)IPCBaseUI.getBaseIncludeParams(pageContext);
        }catch (ClassCastException e) {
            throw new IPCException(IPCEX);
        }
        return includeParams;
    }
    
    /**
     * Returns the first characteristic group of the scroll area, i.e. the first group that 
     * is currently shown in the list of tabs.
     * @param characteristicGroups List of all characteristic groups
     * @return GroupUIBean of the first group in the scroll area.
     */
    public static GroupUIBean getFirstGroupInScrollArea(List characteristicGroups){
        GroupUIBean firstScrollGroup = null;
        Iterator groups = characteristicGroups.iterator();
        while (groups.hasNext()){
            GroupUIBean group = (GroupUIBean) groups.next();
            if (group.isFirstInScrollArea()){
                return group;
            }
        }
        return firstScrollGroup;
        
    }
    
    /**
     * Returns information for the first part of the tabs that is shown (e.g. the starting triangle
     * or the connection between two tabs if the tabs have been already scrolled).
     * The information is encapsulated in a hashmap. It contains the following information (with the 
     * following keys):
     * "width": Width that the first table-cell of the shown tabs has to have.
     * "title": Title for the table-cell (needed for accessibility).
     * "imageName": Name of the image used. (e.g. starting-triangle image, 
     * connection-between-tabs image, etc.)  
     * @param characteristicGroups List of all characteristic groups
     * @param firstGroupInScrollArea First group in the scroll area
     * @param characteristicGroup Current characteristic group that is displayed in the characteristic area (i.e. the currently selected group) 
     * @param uiContext The uiContext
     * @return HashMap with the information for the first table-cell of the tabs. See above for keys.
     */
    public static HashMap getStartInfoOfTabs(List characteristicGroups, 
                                            GroupUIBean firstGroupInScrollArea,
                                            GroupUIBean characteristicGroup,
                                            UIContext uiContext){
        
        HashMap firstTabInfo = new HashMap();
        String tabSelectionKey = "";
        String width = "20";
        String imageName = "fron.gif";
        
        // check if CharacteristicGroup has left neighbour who is not displayed
        boolean leftNeighbour = false;
        int leftNeighbourIndex = 0;
        if (characteristicGroups.indexOf(firstGroupInScrollArea) > 0) {
            leftNeighbourIndex = characteristicGroups.indexOf(firstGroupInScrollArea) - 1;
            leftNeighbour = true;
        }
        
        
        if (!leftNeighbour){ 
            if (firstGroupInScrollArea.isConsistent() || uiContext.getShowStatusLights()) { 
                if (characteristicGroup == null 
                    || firstGroupInScrollArea.getName().equals(characteristicGroup.getName())) { 
                        tabSelectionKey = "access.tab.selected";
                        width = "17";
                        imageName = "fron.gif";
                }
                else { 
                    tabSelectionKey = "access.tab.unselected";
                    width = "17";
                    imageName = "froff.gif";
                } 
            } 
            else { 
                tabSelectionKey = "access.tab.unselected";
                width = "17";
                imageName = "frconflict.gif";
            } 
        }
        // depending of neighbour status display connection Image not first Tab Image
        else { 
            GroupUIBean leftNeighbourGroup = (GroupUIBean) characteristicGroups.get(leftNeighbourIndex);
            if (firstGroupInScrollArea.isConsistent() || uiContext.getShowStatusLights()) { 
                if (characteristicGroup == null 
                    || firstGroupInScrollArea.getName().equals(characteristicGroup.getName())) { 
                    if (leftNeighbourGroup.isConsistent() || uiContext.getShowStatusLights()) {
                        tabSelectionKey = "access.tab.selected";
                        imageName = "offon.gif";
                    }
                    else {
                        tabSelectionKey = "access.tab.selected";
                        imageName = "offconflon.gif";
                    }
                }
                else { 
                    if (leftNeighbourGroup.isConsistent() || uiContext.getShowStatusLights()) {
                        if (leftNeighbourGroup.getName().equals(characteristicGroup.getName())) {
                            tabSelectionKey = "access.tab.unselected";
                            imageName = "onoff.gif";
                        }
                        else {
                            tabSelectionKey = "access.tab.unselected";
                            imageName = "offoff.gif";
                        }
                    }
                    else {
                        if (leftNeighbourGroup.getName().equals( characteristicGroup.getName())) {
                            tabSelectionKey = "access.tab.unselected";
                            imageName = "onconfloff.gif";
                        }
                        else {
                            tabSelectionKey = "access.tab.unselected";
                            imageName = "offconfloff.gif";
                        }
                    }
                } 
            }
            else { 
                if (characteristicGroup == null 
                    || firstGroupInScrollArea.getName().equals(characteristicGroup.getName())) { 
                    if (leftNeighbourGroup.isConsistent() || uiContext.getShowStatusLights()) {
                        tabSelectionKey = "access.tab.selected";
                        imageName = "offonconfl.gif";
                    }
                    else {
                        tabSelectionKey = "access.tab.selected";
                        imageName = "offconfl_onconfl.gif";
                    }
                }
                else { 
                    if (leftNeighbourGroup.isConsistent() || uiContext.getShowStatusLights()) {
                        if (leftNeighbourGroup.getName().equals(characteristicGroup.getName())) {
                            tabSelectionKey = "access.tab.unselected";
                            imageName = "onoffconfl.gif";
                        }
                        else {
                            tabSelectionKey = "access.tab.unselected";
                            imageName = "offoffconfl.gif";
                        }
                    }
                    else {
                        if (leftNeighbourGroup.getName().equals(characteristicGroup.getName())) {
                            tabSelectionKey = "access.tab.unselected";
                            imageName = "onconfloffconfl.gif";
                        }
                        else {
                            tabSelectionKey = "access.tab.unselected";
                            imageName = "offconfl_offconfl.gif";
                        }
                    }
                } 
            } 
        } 
        
        firstTabInfo.put("width", width);
        firstTabInfo.put("title", tabSelectionKey);
        firstTabInfo.put("imageName", imageName);
        return firstTabInfo;
    }


    /**
     * Returns the image at the end of a single tab. This could be either an connection image, i.e. 
     * an image that represents the connection between two tabs. Or it could be an end image, i.e. 
     * the currently processed group is the last one and has no right neighbour.
     * In both cases it differentiates between active/inactive tabs and conflicting/not conflicting
     * tabs.
     * The information is encapsulated in a hashmap. It contains the following information (with the 
     * following keys):
     * "width": Width that the first table-cell of the shown tabs has to have.
     * "title": Title for the table-cell (needed for accessibility).
     * "imageName": Name of the image used. (e.g. starting-triangle image, 
     * connection-between-tabs image, etc.)  
     * @param characteristicGroups List of all characteristic groups
     * @param currentProcessingGroup Group that is currently processed, i.e. the group for that the tab design is currently generated
     * @param characteristicGroup Current characteristic group that is displayed in the characteristic area (i.e. the currently selected group) 
     * @param uiContext The uiContext
     * @return HashMap with the information for the first table-cell of the tabs. See above for keys.
     */
    public static HashMap getConnectionInfoOfTabs(List characteristicGroups, 
                                            GroupUIBean currentProcessingGroup,
                                            GroupUIBean characteristicGroup,
                                            UIContext uiContext){
        
        HashMap connectionInfo = new HashMap();
        String tabSelectionKey = "";
        String width = "20"; 
        String imageName = "bkoff.gif";        
        
        // check if there are more groups. if yes: display connection image, else display end image
        boolean rightNeighbour = false;
        int rightNeighbourIndex = 0;
        
        if ((characteristicGroups.indexOf(currentProcessingGroup) + 1) < characteristicGroups.size()) {
            rightNeighbour = true;
            rightNeighbourIndex = characteristicGroups.indexOf(currentProcessingGroup) + 1;
        }
        else {
            rightNeighbour = false;
        }

        // no more Groups in GroupList: display end-image
        if (!rightNeighbour) { 
            if (currentProcessingGroup.isConsistent() || uiContext.getShowStatusLights()) {
                if (characteristicGroup != null 
                    && currentProcessingGroup.getName().equals(characteristicGroup.getName())) {
                        tabSelectionKey = "access.tab.selected";
                        width = "12";
                        imageName = "bkon.gif";
                }
                else {
                    tabSelectionKey = "access.tab.unselected";
                    width = "16";
                    imageName = "bkoff.gif";
                }
            }
            else {
                if (characteristicGroup != null 
                    && currentProcessingGroup.getName().equals(characteristicGroup.getName())){
                        tabSelectionKey = "access.tab.selected";
                }
                else {
                    tabSelectionKey = "access.tab.unselected";
                }
                width = "16";
                imageName = "bkconflict.gif";
            }
        }
        // there are more groups: display connection-image
        else { 
            GroupUIBean rightNeighbourGroup = (GroupUIBean) characteristicGroups.get(rightNeighbourIndex);
            // current group is consistent
            if (currentProcessingGroup.isConsistent() || uiContext.getShowStatusLights()) { 
                // current group is consistent and active
                if (currentProcessingGroup.getName().equals( characteristicGroup.getName())){ 
                    tabSelectionKey = "access.tab.selected";
                    // current group is consistent, active and right neighbour is consistent
                    if (rightNeighbourGroup.isConsistent() || uiContext.getShowStatusLights()) {
                        width = "19";
                        imageName = "onoff.gif";
                    }
                    // current group is consistent, active and right neighbour is not consistent
                    else {
                        width = "19";
                        imageName = "onoffconfl.gif";
                    }
                }
                // current group is consistent and not active
                else { 
                    tabSelectionKey = "access.tab.unselected";
                    // current group is consistent, not active and right neighbour is active
                    if (rightNeighbourGroup.getName().equals( characteristicGroup.getName())) {
                        // current group is consistent, not active and right neighbour is active and consistent
                        if (rightNeighbourGroup.isConsistent() || uiContext.getShowStatusLights()){
                            imageName = "offon.gif";
                        }
                        // current group is consistent, not active and right neighbour is active and not consistent
                        else {
                            imageName = "offonconfl.gif";
                        }
                    }
                    // current group is consistent, not active and right neighbour is not active
                    else {
                        // current group is consistent, not active and right neighbour is not active and consistent
                        if (rightNeighbourGroup.isConsistent() || uiContext.getShowStatusLights()) {
                            imageName = "offoff.gif";
                        }
                        // current group is consistent, not active and right neighbour is not active and not consistent
                        else {
                            tabSelectionKey = "access.tab.unselected";
                            imageName = "offoffconfl.gif";
                        }
                    }
                } 
            } 
            // current group is not consistent
            else {
                // current group is not consistent and active
                if (currentProcessingGroup.getName().equals( characteristicGroup.getName())) { 
                    // current group is not consistent, active and rigth neighbour is consistent
                    if (rightNeighbourGroup.isConsistent()) {
                        tabSelectionKey = "access.tab.selected";
                        imageName = "onconfloff.gif";
                    }
                    // current group is not consistent, active and rigth neighbour is not consistent
                    else {
                        tabSelectionKey = "access.tab.selected";
                        imageName = "onconfloffconfl.gif";
                    }
                }
                // current group is not consistent and not active
                else { 
                    // current group is not consistent, not active and right neighbour is active
                    if (rightNeighbourGroup.getName().equals( characteristicGroup.getName())) { 
                        // current group is not consistent, not active and right neighbour is active and consistent
                        if (rightNeighbourGroup.isConsistent()) {
                            tabSelectionKey = "access.tab.unselected";
                            imageName = "offconflon.gif";
                        }
                        // current group is not consistent, not active and right neighbour is active and not consistent
                        else {
                            tabSelectionKey = "access.tab.unselected";
                            imageName = "offconfl_onconfl.gif";
                        }
                    }
                    // current group is not consistent, not active and right neighbour is not active
                    else { 
                        // current group is not consistent, not active and right neighbour is not active and consistent
                        if (rightNeighbourGroup.isConsistent()) {
                            tabSelectionKey = "access.tab.unselected";
                            imageName = "offconfloff.gif";
                        }
                        // current group is not consistent, not active and right neighbour is not active and not consistent
                        else {
                            tabSelectionKey = "access.tab.unselected";
                            imageName = "offconfl_offconfl.gif";
                        }
                    } 
                } 
            } 
        } 

        connectionInfo.put("width", width);
        connectionInfo.put("title", tabSelectionKey);
        connectionInfo.put("imageName", imageName);
        return connectionInfo;
    }    
    
    public class IncludeParams extends IPCBaseUI.IncludeParams {
    	private InstanceUIBean instance;
    	private GroupUIBean group;
    	private GroupUIBean scrollGroup;

        /**
         * @param ipcBaseUI
         */
        IncludeParams(
            UIContext ipcBaseUI,
            InstanceUIBean instance,
            GroupUIBean group,
            GroupUIBean scrollGroup,
            Hashtable customerParams) {
            super(ipcBaseUI, customerParams);
            this.instance = instance;
            this.group = group;
            this.scrollGroup = scrollGroup;
        }
        


		/**
		 * @return
		 */
		public GroupUIBean getGroup() {
			return group;
		}

		/**
		 * @return
		 */
		public InstanceUIBean getInstance() {
			return instance;
		}


		/**
		 * @return
		 */
		public GroupUIBean getScrollGroup() {
			return scrollGroup;
		}

    }
    
}
