package com.sap.isa.ipc.ui.jsp.uiclass;

import java.io.IOException;
import java.util.Hashtable;
import java.util.Stack;

import javax.servlet.ServletException;
import javax.servlet.jsp.PageContext;

import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.ipc.ui.jsp.beans.LoadingMessageUIBean;
import com.sap.spc.remote.client.object.IPCException;

public class ComparisonTblGenLoadMsgUI extends IPCBaseUI {
    
    //helper instance in order to create the parameter objects
    private static ComparisonTblGenLoadMsgUI comparisonTblGenLoadMsgUI = new ComparisonTblGenLoadMsgUI();
    private static IsaLocation loc = IsaLocation.getInstance(ComparisonTblGenLoadMsgUI.class.getName());
    
    public static void include(
        PageContext pageContext,
        String comparisionTblGenLoadMsgPage,
        UIContext ipcBaseUI,
        LoadingMessageUIBean messageBean,
        boolean titleDisplayed,
        Hashtable customerParams) {

            IncludeParams includeParams =
                comparisonTblGenLoadMsgUI.new IncludeParams(
                    ipcBaseUI,
                    messageBean,
                    titleDisplayed,
                    customerParams);
            Stack paramStack = IPCBaseUI.getParamStack(pageContext);
            paramStack.push(includeParams);
            try {
                pageContext.include(comparisionTblGenLoadMsgPage);
            } catch (ServletException e) {
                loc.error(IPCEX70 + comparisionTblGenLoadMsgPage, e.getRootCause());
                throw new RuntimeException(IPCEX57, e.getRootCause());
            } catch (IOException e) {
                loc.error(IPCEX70 + comparisionTblGenLoadMsgPage, e);
                throw new RuntimeException(IPCEX58, e);
            }
            paramStack.pop();
    }
    
    public static IncludeParams getIncludeParams(PageContext pageContext) {
        IncludeParams includeParams = null;
        try{
            includeParams = (IncludeParams)IPCBaseUI.getBaseIncludeParams(pageContext);
        }catch (ClassCastException e) {
            throw new IPCException(IPCEX59);
        }
        return includeParams;
    }
    
    public class IncludeParams extends IPCBaseUI.IncludeParams {
        private LoadingMessageUIBean messageBean;
        private boolean titleDisplayed; 

        /**
         * @param ipcBaseUI
         */
        IncludeParams(
            UIContext ipcBaseUI,
            LoadingMessageUIBean messageBean,
            boolean titleDisplayed,
            Hashtable customerParams) {
            super(ipcBaseUI, customerParams);
            this.messageBean = messageBean;
            this.titleDisplayed = titleDisplayed;
        }
       
        /**
         * @return
         */
        public LoadingMessageUIBean getMessageBean() {
            return messageBean;
        }

        /**
         * @return
         */
        public boolean isTitleDisplayed() {
            return titleDisplayed;
        }
    }
}