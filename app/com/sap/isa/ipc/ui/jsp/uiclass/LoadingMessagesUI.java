package com.sap.isa.ipc.ui.jsp.uiclass;

import java.io.IOException;
import java.util.Hashtable;
import java.util.List;
import java.util.Stack;

import javax.servlet.ServletException;
import javax.servlet.jsp.PageContext;

import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.ipc.ui.jsp.beans.CharacteristicUIBean;
import com.sap.spc.remote.client.object.IPCException;

public class LoadingMessagesUI extends IPCBaseUI {
    //helper instance in order to create the parameter objects
    private static LoadingMessagesUI loadingMessagesUI = new LoadingMessagesUI();
    private static IsaLocation loc = IsaLocation.getInstance(LoadingMessagesUI.class.getName());
    
    public static void include(
        PageContext pageContext,
        String loadingMessagePage,
        UIContext ipcBaseUI,
        List messages,
        CharacteristicUIBean csticDelta,
        Hashtable customerParams) {

            IncludeParams includeParams =
                loadingMessagesUI.new IncludeParams(
                    ipcBaseUI,
                    messages,
                    csticDelta,
                    customerParams);
            Stack paramStack = IPCBaseUI.getParamStack(pageContext);
            paramStack.push(includeParams);
            try {
                pageContext.include(loadingMessagePage);
            } catch (ServletException e) {
                loc.error(IPCEX70 + loadingMessagePage, e.getRootCause());
                throw new RuntimeException(IPCEX70 + loadingMessagePage, e.getRootCause());
            } catch (IOException e) {
                loc.error(IPCEX70 + loadingMessagePage, e);
                throw new RuntimeException(IPCEX70 + loadingMessagePage, e);
            }
            paramStack.pop();
    }
    
    public static IncludeParams getIncludeParams(PageContext pageContext) {
        IncludeParams includeParams = null;
        try{
            includeParams = (IncludeParams)IPCBaseUI.getBaseIncludeParams(pageContext);
        }catch (ClassCastException e) {
            throw new IPCException(IPCEX66);
        }
        return includeParams;
    }
    
    public class IncludeParams extends IPCBaseUI.IncludeParams {
        private List messages;
        private CharacteristicUIBean csticDelta;

        /**
         * @param ipcBaseUI
         * @param message
         * @param customerParams
         */
        IncludeParams(
            UIContext ipcBaseUI,
            List messages,
            CharacteristicUIBean csticDelta,
            Hashtable customerParams) {
            super(ipcBaseUI, customerParams);
            this.messages = messages;
        }

        /**
         * @return List of LoadingMessageUIBean that should be displayed
         */
        public List getMessages() {
            return messages;
        }

        /**
         * @return CharacteristicUIBean to which the LoadingMessage belongs to
         */
        public CharacteristicUIBean getCsticDelta() {
            return csticDelta;
        }

    }
}
