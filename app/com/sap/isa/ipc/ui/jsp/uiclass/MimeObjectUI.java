/*
 * Created on 14.07.2004
 *
 * To change the template for this generated file go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
package com.sap.isa.ipc.ui.jsp.uiclass;

import java.io.IOException;
import java.util.Hashtable;
import java.util.List;
import java.util.Stack;

import javax.servlet.ServletException;
import javax.servlet.jsp.PageContext;

import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.ipc.ui.jsp.beans.GroupUIBean;
import com.sap.isa.ipc.ui.jsp.beans.InstanceUIBean;
import com.sap.isa.ipc.ui.jsp.beans.MimeUIBean;

/**
 * @author 
 *
 * To change the template for this generated type comment go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
public class MimeObjectUI extends IPCBaseUI {
    private static MimeObjectUI mimeObjectUI = new MimeObjectUI();
    private static IsaLocation loc = IsaLocation.getInstance(MimeObjectUI.class.getName());
    
    public static void include(
        PageContext pageContext,
        String mimeObjectPage,
        UIContext uiContext,
        List mimes,
        MimeUIBean mime,
        MimeUIBean enlargedMime,
        InstanceUIBean instance,
        GroupUIBean charGroup,
        Hashtable customerParams) {
        IncludeParams includeParams =
            mimeObjectUI.new IncludeParams(
                uiContext,
                mimeObjectPage,
                mimes,
                mime,
                enlargedMime,
                instance,
                charGroup,
                customerParams);
        Stack paramStack = IPCBaseUI.getParamStack(pageContext);
        paramStack.push(includeParams);
        try {
            pageContext.include(mimeObjectPage);
        } catch (ServletException e) {
            loc.error(IPCEX70 + mimeObjectPage, e.getRootCause());
            throw new RuntimeException(IPCEX33, e.getRootCause());
//TODO: Exceptionhandling            throw new IPCException(IPCEX15, null, e);
        } catch (IOException e) {
            loc.error(IPCEX70 + mimeObjectPage, e);
            throw new RuntimeException(IPCEX34, e);
//TODO: Exceptionhandling            throw new IPCException(IPCEX16, null, e);
        }
        paramStack.pop();
    }

    public static IncludeParams getIncludeParams(PageContext pageContext) {
        IncludeParams includeParams = null;
        includeParams = (IncludeParams)IPCBaseUI.getBaseIncludeParams(pageContext);
        return includeParams;
    }
    
    public class IncludeParams extends IPCBaseUI.IncludeParams {
        private String mimeObjectPage;
        private List mimes;
        private MimeUIBean mime;
        private MimeUIBean enlargedMime;
        private InstanceUIBean instance;
        private GroupUIBean charGroup;
        public IncludeParams(UIContext uiContext,
                             String mimeObjectPage,
                             List mimes,
                             MimeUIBean mime,
                             MimeUIBean enlargedMime,
                             InstanceUIBean instance,
                             GroupUIBean charGroup,
                             Hashtable customerParams) {
                                 super(uiContext, customerParams);
                                 this.mimeObjectPage = mimeObjectPage;
                                 this.mimes = mimes;
                                 this.mime = mime;
                                 this.enlargedMime = enlargedMime;
                                 this.instance = instance;
                                 this.charGroup = charGroup; 
                             }
        /**
         * @return mime object that has been passed from the calling JSP
         */
        public MimeUIBean getMime() {
            return mime;
        }

        /**
         * @return page that has been passed from the calling JSP
         */
        public String getMimeObjectPage() {
            return mimeObjectPage;
        }

        /**
         * @return list of Mimes that has been passed from the calling JSP
         */
        public List getMimes() {
            return mimes;
        }
        
        /**
         * @return enlarged Mime object that has been passed from the calling JSP
         */
        public MimeUIBean getEnlargedMime() {
            return enlargedMime;
        }

        /**
         * @return current instance
         */
        public InstanceUIBean getInstance() {
            return instance;
        }
        
        /**
         * @return current characteristic group
         */
        public GroupUIBean getCharGroup() {
            return charGroup;
        }



    }

}
