/*
 * Created on 14.07.2004
 *
 * To change the template for this generated file go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
package com.sap.isa.ipc.ui.jsp.uiclass;

import java.io.IOException;
import java.util.Hashtable;
import java.util.Stack;

import javax.servlet.ServletException;
import javax.servlet.jsp.PageContext;

import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.ipc.ui.jsp.beans.InstanceUIBean;
import com.sap.spc.remote.client.object.IPCException;

/**
 * @author 
 *
 * To change the template for this generated type comment go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
public class DynamicProductPictureShowUI extends IPCBaseUI {
    
    //helper instance in order to create the parameter objects
    private static DynamicProductPictureShowUI characteristicUI = new DynamicProductPictureShowUI();
    private static IsaLocation loc = IsaLocation.getInstance(DynamicProductPictureShowUI.class.getName());
    
    public static void include(
        PageContext pageContext,
        String dynamicProductPictureShowPage,
	    UIContext ipcBaseUI,
        InstanceUIBean instance,
		String[] imgIds,
		String[] imgSizes,
		String[] imgSrcs,
		String[] imgStyles,       
        Hashtable customerParams) {

            IncludeParams includeParams =
                characteristicUI.new IncludeParams(
                    ipcBaseUI,
                    dynamicProductPictureShowPage,
                    instance,
                    imgIds,
                    imgSizes,
                    imgSrcs,
                    imgStyles,
                    customerParams);
            Stack paramStack = IPCBaseUI.getParamStack(pageContext);
            paramStack.push(includeParams);
            try {
                pageContext.include(dynamicProductPictureShowPage);
            } catch (ServletException e) {
                loc.error(IPCEX70 + dynamicProductPictureShowPage, e);
                throw new RuntimeException(IPCEX38, e);
//TODO:Exceptionhandling                throw new IPCException(IPCEX, null, e);
            } catch (IOException e) {
                loc.error(IPCEX70 + dynamicProductPictureShowPage, e);
                throw new RuntimeException(IPCEX39, e);
//TODO:Exceptionhandling                throw new IPCException(IPCEX, null, e);
            }
            paramStack.pop();
    }
    
    public static IncludeParams getIncludeParams(PageContext pageContext) {
        IncludeParams includeParams = null;
        try{
            includeParams = (IncludeParams)IPCBaseUI.getBaseIncludeParams(pageContext);
        }catch (ClassCastException e) {
            throw new IPCException(IPCEX40);
        }
        return includeParams;
    }
    
    public class IncludeParams extends IPCBaseUI.IncludeParams {
        private String dynamicProductPictureShowPage;
        private InstanceUIBean instance;
        private String[] imgIds,imgSrcs,imgStyles,imgSizes;

        /**
         * @param ipcBaseUI
         */
        IncludeParams(
            UIContext ipcBaseUI,
            String dynamicProductPictureShowPage,
            InstanceUIBean instance,
            String[] imgIds,
            String[] imgSizes,
            String[] imgSrcs,
            String[] imgStyles,
            Hashtable customerParams) {
            super(ipcBaseUI, customerParams);
            this.dynamicProductPictureShowPage = dynamicProductPictureShowPage;
            this.instance = instance;
            this.imgIds = imgIds;
            this.imgSizes = imgSizes;
            this.imgSrcs = imgSrcs;
            this.imgStyles = imgStyles;
        }
        


        /**
         * @return
         */
        public String getDynamicProductPictureShowPage() {
            return dynamicProductPictureShowPage;
        }


		/**
		 * @return
		 */
		public String[] getImgIds() {
			return imgIds;
		}

		/**
		 * @return
		 */
		public String[] getImgSizes() {
			return imgSizes;
		}

		/**
		 * @return
		 */
		public String[] getImgSrcs() {
			return imgSrcs;
		}

		/**
		 * @return
		 */
		public String[] getImgStyles() {
			return imgStyles;
		}

		/**
		 * @return
		 */
		public InstanceUIBean getInstance() {
			return instance;
		}

    }
    
}
