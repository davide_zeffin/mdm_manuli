/*
 * Created on 27.10.2004
 *
 * To change the template for this generated file go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
package com.sap.isa.ipc.ui.jsp.uiclass;

import java.io.IOException;
import java.util.Hashtable;
import java.util.List;
import java.util.Stack;

import javax.servlet.ServletException;
import javax.servlet.jsp.PageContext;

import com.sap.isa.core.logging.IsaLocation;
import com.sap.spc.remote.client.object.IPCException;

/**
 * @author 
 *
 * To change the template for this generated type comment go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
public class ProductVariantsUI extends IPCBaseUI {
    
	//helper instance in order to create the parameter objects
	private static ProductVariantsUI productVariantsUI = new ProductVariantsUI();
	private static IsaLocation loc = IsaLocation.getInstance(ProductVariantUI.class.getName());
    
	public static void include(
		PageContext pageContext,
		String productVariantsPage,
		UIContext ipcBaseUI,
		List productVariants,
		Hashtable customerParams) {

			IncludeParams includeParams =
				productVariantsUI.new IncludeParams(
					ipcBaseUI,
					productVariants,
					customerParams);
			Stack paramStack = IPCBaseUI.getParamStack(pageContext);
			paramStack.push(includeParams);
			try {
				pageContext.include(productVariantsPage);
			} catch (ServletException e) {
				loc.error(IPCEX70 + productVariantsPage, e.getRootCause());
				throw new RuntimeException(IPCEX57, e.getRootCause());
//TODO:Exceptionhandling                throw new IPCException(IPCEX, null, e);
			} catch (IOException e) {
				loc.error(IPCEX70 + productVariantsPage, e);
				throw new RuntimeException(IPCEX58, e);
//TODO:Exceptionhandling                throw new IPCException(IPCEX, null, e);
			}
			paramStack.pop();
	}
    
	public static IncludeParams getIncludeParams(PageContext pageContext) {
		IncludeParams includeParams = null;
		try{
			includeParams = (IncludeParams)IPCBaseUI.getBaseIncludeParams(pageContext);
		}catch (ClassCastException e) {
			throw new IPCException(IPCEX59);
		}
		return includeParams;
	}
    
	public class IncludeParams extends IPCBaseUI.IncludeParams {
		private List productVariants;

		/**
		 * @param ipcBaseUI
		 */
		IncludeParams(
			UIContext ipcBaseUI,
			List productVariants,
			Hashtable customerParams) {
			super(ipcBaseUI, customerParams);
			this.productVariants = productVariants;
		}
        


		/**
		 * @return
		 */
		public List getProductVariants() {
			return productVariants;
		}

	}
}
