/*
 * Created on 14.07.2004
 *
 * To change the template for this generated file go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
package com.sap.isa.ipc.ui.jsp.uiclass;

import java.io.IOException;
import java.util.Hashtable;
import java.util.List;
import java.util.Stack;

import javax.servlet.ServletException;
import javax.servlet.jsp.PageContext;

import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.ipc.ui.jsp.beans.CharacteristicUIBean;
import com.sap.isa.ipc.ui.jsp.beans.GroupUIBean;
import com.sap.isa.ipc.ui.jsp.beans.InstanceUIBean;
import com.sap.isa.ipc.ui.jsp.beans.ValueUIBean;
import com.sap.spc.remote.client.object.IPCException;

/**
 * @author 
 *
 * To change the template for this generated type comment go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
public class CharacteristicValuesUI extends IPCBaseUI {
    
    //helper instance in order to create the parameter objects
    private static CharacteristicValuesUI characteristicUI = new CharacteristicValuesUI();
    private static IsaLocation loc = IsaLocation.getInstance(CharacteristicValuesUI.class.getName());
    
    public static void include(
        PageContext pageContext,
        String layout,
        String valuesTile,
        String freeTextTile,
        String intervalTile,
        UIContext ipcBaseUI,
        InstanceUIBean currentInstance,
        GroupUIBean currentGroup,
        CharacteristicUIBean currentCharacteristic,
        List values,
        boolean showDescriptions,
        UIArea uiArea, 
        boolean readOnlyMode,
        boolean detailsMode,       
        Hashtable customerParams) {

            IncludeParams includeParams =
                characteristicUI.new IncludeParams(
                    ipcBaseUI,
                    layout,
                    valuesTile,
                    freeTextTile,
                    intervalTile,
                    currentInstance,
                    currentGroup,
                    currentCharacteristic,
                    values,
                    showDescriptions,
                    uiArea,
                    readOnlyMode,
                    detailsMode,
                    customerParams);
            Stack paramStack = IPCBaseUI.getParamStack(pageContext);
            paramStack.push(includeParams);
            try {
                pageContext.include(layout);
            } catch (ServletException e) {
                loc.error(IPCEX70 + layout, e);
                throw new RuntimeException(IPCEX35, e);
//TODO:Exceptionhandling                throw new IPCException(IPCEX13, null, e);
            } catch (IOException e) {
                loc.error(IPCEX70 + layout, e);
                throw new RuntimeException(IPCEX36, e);
//TODO:Exceptionhandling                throw new IPCException(IPCEX14, null, e);
            }
            paramStack.pop();
    }
    
    public static IncludeParams getIncludeParams(PageContext pageContext) {
        IncludeParams includeParams = null;
        try{
            includeParams = (IncludeParams)IPCBaseUI.getBaseIncludeParams(pageContext);
        }catch (ClassCastException e) {
            throw new IPCException(IPCEX37);
        }
        return includeParams;
    }
    
    
    /**
     * Returns true if an input field should be displayed.
     * An input field should NOT be displayed if at least one of the following conditions is true:
     * <li>The UI is currently in display-mode 
     * <li>The current characteristic is read-only
     * <li>The assigned value is assigned by system
     * So this means: If the UI is not in display-mode and the cstic is not read-only we show
     * an input field (with the exception that if the assigned value is system-assigned we don't show
     * an input field). 
     * @param displayMode Is UI in display mode (T means that it is read-only)
     * @param currentCharacteristic 
     * @return true if an input field should be displayed.
     */
    public static boolean showInputField(boolean displayMode, CharacteristicUIBean currentCharacteristic){
        boolean showInputField = false;
        if (!displayMode && !currentCharacteristic.isReadOnly()) {
            // we show an input field
            showInputField = true;
            if (currentCharacteristic.hasAssignedValues()){
                ValueUIBean value = (ValueUIBean)currentCharacteristic.getAssignedValues().get(0);
                // exception: if characteristic has a value and it is assigned by system -> no input field
                if (value.isAssignedBySystem()){
                    showInputField = false;
                }
            }
        }
        return showInputField;
    }
    
    public class IncludeParams extends IPCBaseUI.IncludeParams {
        private String layout, valuesTile, freeTextTile, intervalTile;
        private InstanceUIBean instance;
        private GroupUIBean group;
        private CharacteristicUIBean currentCharacteristic;
        private List values;
        private boolean showDescriptions;
        private UIArea uiArea;
        private boolean readOnlyMode;
        private boolean detailsMode;

        /**
         * @param ipcBaseUI
         */
        IncludeParams(
            UIContext ipcBaseUI,
            String layout,
            String valuesTile,
            String freeTextTile,
            String intervalTile,
            InstanceUIBean instance,
            GroupUIBean group,
            CharacteristicUIBean currentCharacteristic,
            List values,
            boolean showDescriptions,
            UIArea uiArea,
            boolean readOnlyMode,
            boolean detailsMode,
            Hashtable customerParams) {
            super(ipcBaseUI, customerParams);
            this.layout = layout;
            this.valuesTile = valuesTile;
            this.freeTextTile = freeTextTile;
            this.intervalTile = intervalTile;
            this.instance = instance;
            this.group = group;
            this.currentCharacteristic = currentCharacteristic;
            this.values = values;
            this.showDescriptions = showDescriptions;
            this.uiArea = uiArea;
            this.readOnlyMode = readOnlyMode;
            this.detailsMode = detailsMode;
        }
        

        /**
         * @return
         */
        public CharacteristicUIBean getCurrentCharacteristic() {
            return currentCharacteristic;
        }

        /**
         * @return
         */
        public boolean isShowDescriptions() {
            return showDescriptions;
        }

        /**
         * @return
         */
        public List getValues() {
            return values;
        }

        /**
         * @return tile for input text field
         */
        public String getFreeTextTile() {
            return freeTextTile;
        }

        /**
         * @return tile for interval domains
         */
        public String getIntervalTile() {
            return intervalTile;
        }

        /**
         * @return
         */
        public String getLayout() {
            return layout;
        }

        /**
         * @return tile for selection list (drop-down or single-option)
         */
        public String getValuesTile() {
            return valuesTile;
        }

        /**
         * @return
         */
        public UIArea getUiArea() {
            return uiArea;
        }

		/**
		 * @return
		 */
		public boolean isReadOnlyMode() {
			return readOnlyMode;
		}

        /**
         * @return
         */
        public boolean isDetailsMode() {
            return detailsMode;
        }

		/**
		 * @return
		 */
		public GroupUIBean getGroup() {
			return group;
		}

		/**
		 * @return
		 */
		public InstanceUIBean getInstance() {
			return instance;
		}

    }
    
}
