/*
 * UIArea:
 * Represents an area of the UI.
 * Provides shortcutkey and tabindex for UIArea.
 */
package com.sap.isa.ipc.ui.jsp.uiclass;

import com.sap.isa.core.Constants;

public class UIArea extends UIElement {

    private String tabIndex;
    private static final String TABORDER_PREFIX = "taborder."; //$NON-NLS-1$

    public String test;
    public UIArea(String name, UIContext uiContext) {
        super(name, uiContext);
        String keyForTabIndex = TABORDER_PREFIX + name;
        // set tabIndex only if UI is not in accessibility mode otherwise set tabindex to ""
        if (uiContext.getPropertyAsBoolean(Constants.ACCESSIBILITY)){
            this.tabIndex = "";
        }
        else {
            this.tabIndex = uiContext.getPropertyAsString(keyForTabIndex, ""); //$NON-NLS-1$
        }
    }

    public String getTabIndex() {
        return this.tabIndex;
    }

    public String toString() {
        String returnString = new String();
        returnString += UIArea.class.getName();
        returnString += "\nname: " + this.getName(); //$NON-NLS-1$
        returnString += "\nkey: " + this.getShortcutKey(); //$NON-NLS-1$
        returnString += "\ntabindex: " + this.getTabIndex(); //$NON-NLS-1$
        return returnString;
    }

    public static void main(String[] args) {
        UIContext myContext = new UIContext();
        UIArea testArea = new UIArea("testarea", myContext); //$NON-NLS-1$
        System.out.println(testArea);
    }
}
