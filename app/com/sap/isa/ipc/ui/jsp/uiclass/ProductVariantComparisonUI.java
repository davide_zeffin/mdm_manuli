/*
 * Created on 27.10.2004
 *
 * To change the template for this generated file go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
package com.sap.isa.ipc.ui.jsp.uiclass;

import java.io.IOException;
import java.util.Hashtable;
import java.util.List;
import java.util.Stack;

import javax.servlet.ServletException;
import javax.servlet.jsp.PageContext;

import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.ipc.ui.jsp.beans.InstanceUIBean;
import com.sap.spc.remote.client.object.IPCException;

/**
 * @author 
 *
 * To change the template for this generated type comment go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
public class ProductVariantComparisonUI extends IPCBaseUI {
    
	//helper instance in order to create the parameter objects
	private static ProductVariantComparisonUI productVariantsUI = new ProductVariantComparisonUI();
	private static IsaLocation loc = IsaLocation.getInstance(ProductVariantComparisonUI.class.getName());
    
	public static void include(
		PageContext pageContext,
		String productVariantComparisionPage,
		UIContext ipcBaseUI,
		InstanceUIBean configInstance,
		List productVariantsToCompare,
		Hashtable customerParams) {

			IncludeParams includeParams =
				productVariantsUI.new IncludeParams(
					ipcBaseUI,
					configInstance,
					productVariantsToCompare,
					customerParams);
			Stack paramStack = IPCBaseUI.getParamStack(pageContext);
			paramStack.push(includeParams);
			try {
				pageContext.include(productVariantComparisionPage);
			} catch (ServletException e) {
				loc.error(IPCEX70 + productVariantComparisionPage, e.getRootCause());
				throw new RuntimeException(IPCEX57, e.getRootCause());
//TODO:Exceptionhandling                throw new IPCException(IPCEX, null, e);
			} catch (IOException e) {
				loc.error(IPCEX70 + productVariantComparisionPage, e);
				throw new RuntimeException(IPCEX58, e);
//TODO:Exceptionhandling                throw new IPCException(IPCEX, null, e);
			}
			paramStack.pop();
	}
    
	public static IncludeParams getIncludeParams(PageContext pageContext) {
		IncludeParams includeParams = null;
		try{
			includeParams = (IncludeParams)IPCBaseUI.getBaseIncludeParams(pageContext);
		}catch (ClassCastException e) {
			throw new IPCException(IPCEX59);
		}
		return includeParams;
	}
    
	public class IncludeParams extends IPCBaseUI.IncludeParams {
		private InstanceUIBean configInstance; //instance of the configurable product
		private List productVariantsToCompare; //the product variants

		/**
		 * @param ipcBaseUI
		 */
		IncludeParams(
			UIContext ipcBaseUI,
			InstanceUIBean configInstance,
			List productVariantsToCompare,
			Hashtable customerParams) {
			super(ipcBaseUI, customerParams);
			this.configInstance = configInstance;
			this.productVariantsToCompare = productVariantsToCompare;
		}
        

		/**
		 * @return
		 */
		public InstanceUIBean getConfigInstance() {
			return configInstance;
		}

		/**
		 * @return
		 */
		public List getProductVariantsToCompare() {
			return productVariantsToCompare;
		}
	}

}
