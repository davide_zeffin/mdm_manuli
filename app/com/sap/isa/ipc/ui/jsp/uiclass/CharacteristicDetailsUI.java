/*
 * Created on 27.10.2004
 *
 * To change the template for this generated file go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
package com.sap.isa.ipc.ui.jsp.uiclass;

import java.io.IOException;
import java.util.Hashtable;
import java.util.Stack;

import javax.servlet.ServletException;
import javax.servlet.jsp.PageContext;

import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.ipc.ui.jsp.beans.CharacteristicUIBean;
import com.sap.isa.ipc.ui.jsp.beans.GroupUIBean;
import com.sap.isa.ipc.ui.jsp.beans.InstanceUIBean;
import com.sap.spc.remote.client.object.IPCException;

/**
 * @author 
 *
 * To change the template for this generated type comment go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
public class CharacteristicDetailsUI extends IPCBaseUI {
	private static CharacteristicDetailsUI characteristicDetailsUI = new CharacteristicDetailsUI();
	private static IsaLocation loc = IsaLocation.getInstance(CharacteristicUI.class.getName());
    
	public static void include(
		PageContext pageContext,
		String detailPage,
	    UIContext uiContext,
	    InstanceUIBean instance,
		GroupUIBean charGroup,
		CharacteristicUIBean cstic,
		UIArea uiArea,
		Hashtable customerParams) {
		IncludeParams includeParams =
			characteristicDetailsUI.new IncludeParams(
				uiContext,
				detailPage,
				instance,
				charGroup,
				cstic,
				uiArea,
				customerParams);
		Stack paramStack = IPCBaseUI.getParamStack(pageContext);
		paramStack.push(includeParams);
		try {
			pageContext.include(detailPage);
		} catch (ServletException e) {
			loc.error(IPCEX70 + detailPage, e);
			throw new RuntimeException(IPCEX15, e);
//TODO: Exceptionhandling            throw new IPCException(IPCEX15, null, e);
		} catch (IOException e) {
			loc.error(IPCEX70 + detailPage, e);
			throw new RuntimeException(IPCEX16, e);
//TODO: Exceptionhandling            throw new IPCException(IPCEX16, null, e);
		}
		paramStack.pop();
	}

	public static IncludeParams getIncludeParams(PageContext pageContext) {
		IncludeParams includeParams = null;
		try{
			includeParams = (IncludeParams)IPCBaseUI.getBaseIncludeParams(pageContext);
		}catch (ClassCastException e) {
			throw new IPCException(IPCEX14);
		}
		return includeParams;
	}
    
	public class IncludeParams extends IPCBaseUI.IncludeParams {
		private String detailPage;
		private InstanceUIBean instance;
		private GroupUIBean charGroup;
		private CharacteristicUIBean cstic;
		private UIArea uiArea;
        
		public IncludeParams(UIContext uiContext,
							 String detailPage,
							 InstanceUIBean instance,
							 GroupUIBean charGroup,
							 CharacteristicUIBean cstic,
							 UIArea uiArea,
							 Hashtable customerParams) {
								 super(uiContext, customerParams);
								 this.detailPage = detailPage;
								 this.instance = instance;
								 this.charGroup = charGroup;
								 this.cstic = cstic;
								 this.uiArea = uiArea;
							 }

		/**
		 * @return
		 */
		public GroupUIBean getCharGroup() {
			return charGroup;
		}

		/**
		 * @return
		 */
		public CharacteristicUIBean getCstic() {
			return cstic;
		}

		/**
		 * @return
		 */
		public UIArea getUiArea() {
			return uiArea;
		}

		/**
		 * @return
		 */
		public InstanceUIBean getInstance() {
			return instance;
		}

	}


}
