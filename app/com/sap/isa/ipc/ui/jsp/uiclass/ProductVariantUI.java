/*
 * Created on 14.07.2004
 *
 * To change the template for this generated file go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
package com.sap.isa.ipc.ui.jsp.uiclass;

import java.io.IOException;
import java.util.Hashtable;
import java.util.Stack;

import javax.servlet.ServletException;
import javax.servlet.jsp.PageContext;

import com.sap.spc.remote.client.object.IPCException;
import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.ipc.ui.jsp.beans.ProductVariantUIBean;

/**
 * @author 
 *
 * To change the template for this generated type comment go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
public class ProductVariantUI extends IPCBaseUI {
    
    //helper instance in order to create the parameter objects
    private static ProductVariantUI productVariantUI = new ProductVariantUI();
    private static IsaLocation loc = IsaLocation.getInstance(ProductVariantUI.class.getName());
    
    public static void include(
        PageContext pageContext,
        String productVariantPage,
        UIContext ipcBaseUI,
        ProductVariantUIBean productVariant,
        Hashtable customerParams) {

            IncludeParams includeParams =
                productVariantUI.new IncludeParams(
                    ipcBaseUI,
                    productVariant,
                    customerParams);
            Stack paramStack = IPCBaseUI.getParamStack(pageContext);
            paramStack.push(includeParams);
            try {
                pageContext.include(productVariantPage);
            } catch (ServletException e) {
                loc.error(IPCEX70 + productVariantPage, e.getRootCause());
                throw new RuntimeException(IPCEX57, e.getRootCause());
//TODO:Exceptionhandling                throw new IPCException(IPCEX, null, e);
            } catch (IOException e) {
                loc.error(IPCEX70 + productVariantPage, e);
                throw new RuntimeException(IPCEX58, e);
//TODO:Exceptionhandling                throw new IPCException(IPCEX, null, e);
            }
            paramStack.pop();
    }
    
    public static IncludeParams getIncludeParams(PageContext pageContext) {
        IncludeParams includeParams = null;
        try{
            includeParams = (IncludeParams)IPCBaseUI.getBaseIncludeParams(pageContext);
        }catch (ClassCastException e) {
            throw new IPCException(IPCEX59);
        }
        return includeParams;
    }
    
    public class IncludeParams extends IPCBaseUI.IncludeParams {
        private ProductVariantUIBean productVariant;

        /**
         * @param ipcBaseUI
         */
        IncludeParams(
            UIContext ipcBaseUI,
            ProductVariantUIBean productVariant,
            Hashtable customerParams) {
            super(ipcBaseUI, customerParams);
            this.productVariant = productVariant;
        }
        


        /**
         * @return
         */
        public ProductVariantUIBean getProductVariant() {
            return productVariant;
        }

    }
    
}
