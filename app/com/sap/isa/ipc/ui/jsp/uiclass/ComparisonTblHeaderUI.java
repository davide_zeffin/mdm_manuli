package com.sap.isa.ipc.ui.jsp.uiclass;

import java.io.IOException;
import java.util.Hashtable;
import java.util.Stack;

import javax.servlet.ServletException;
import javax.servlet.jsp.PageContext;

import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.ipc.ui.jsp.beans.ComparisonResultUIBean;
import com.sap.spc.remote.client.object.IPCException;

public class ComparisonTblHeaderUI extends IPCBaseUI {
    
    //helper instance in order to create the parameter objects
    private static ComparisonTblHeaderUI comparisonTblHeaderUI = new ComparisonTblHeaderUI();
    private static IsaLocation loc = IsaLocation.getInstance(ComparisonTblHeaderUI.class.getName());
    
    public static void include(
        PageContext pageContext,
        String comparisionTblHeaderPage,
        UIContext ipcBaseUI,
        ComparisonResultUIBean resultBean,
        Hashtable customerParams) {

            IncludeParams includeParams =
                comparisonTblHeaderUI.new IncludeParams(
                    ipcBaseUI,
                    resultBean,
                    customerParams);
            Stack paramStack = IPCBaseUI.getParamStack(pageContext);
            paramStack.push(includeParams);
            try {
                pageContext.include(comparisionTblHeaderPage);
            } catch (ServletException e) {
                loc.error(IPCEX70 + comparisionTblHeaderPage, e.getRootCause());
                throw new RuntimeException(IPCEX57, e.getRootCause());
            } catch (IOException e) {
                loc.error(IPCEX70 + comparisionTblHeaderPage, e);
                throw new RuntimeException(IPCEX58, e);
            }
            paramStack.pop();
    }
    
    public static IncludeParams getIncludeParams(PageContext pageContext) {
        IncludeParams includeParams = null;
        try{
            includeParams = (IncludeParams)IPCBaseUI.getBaseIncludeParams(pageContext);
        }catch (ClassCastException e) {
            throw new IPCException(IPCEX59);
        }
        return includeParams;
    }
    
    public class IncludeParams extends IPCBaseUI.IncludeParams {
        private ComparisonResultUIBean resultBean;

        /**
         * @param ipcBaseUI
         */
        IncludeParams(
            UIContext ipcBaseUI,
            ComparisonResultUIBean resultBean,
            Hashtable customerParams) {
            super(ipcBaseUI, customerParams);
            this.resultBean = resultBean;
        }
       
        /**
         * @return
         */
        public ComparisonResultUIBean getResultBean() {
            return resultBean;
        }
    }
}
