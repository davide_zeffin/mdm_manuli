/*
 * Created on 14.07.2004
 *
 * To change the template for this generated file go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
package com.sap.isa.ipc.ui.jsp.uiclass;

import java.io.IOException;
import java.util.Hashtable;
import java.util.Stack;

import javax.servlet.ServletException;
import javax.servlet.jsp.PageContext;

import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.ipc.ui.jsp.beans.MessageUIBean;
import com.sap.spc.remote.client.object.IPCException;

/**
 * @author 
 *
 * To change the template for this generated type comment go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
public class MessagesUI extends IPCBaseUI {
    
    //helper instance in order to create the parameter objects
    private static MessagesUI messagesUI = new MessagesUI();
    private static IsaLocation loc = IsaLocation.getInstance(MessagesUI.class.getName());
    
    public static void include(
        PageContext pageContext,
        String messagesPage,
        UIContext ipcBaseUI,
        MessageUIBean message,
        Hashtable customerParams) {

            IncludeParams includeParams =
                messagesUI.new IncludeParams(
                    ipcBaseUI,
                    message,
                    customerParams);
            Stack paramStack = IPCBaseUI.getParamStack(pageContext);
            paramStack.push(includeParams);
            try {
                pageContext.include(messagesPage);
            } catch (ServletException e) {
                loc.error(IPCEX70 + messagesPage, e.getRootCause());
                throw new RuntimeException(IPCEX70 + messagesPage, e.getRootCause());
//TODO:Exceptionhandling                throw new IPCException(IPCEX, null, e);
            } catch (IOException e) {
                loc.error(IPCEX70 + messagesPage, e);
                throw new RuntimeException(IPCEX70 + messagesPage, e);
//TODO:Exceptionhandling                throw new IPCException(IPCEX, null, e);
            }
            paramStack.pop();
    }
    
    public static IncludeParams getIncludeParams(PageContext pageContext) {
        IncludeParams includeParams = null;
        try{
            includeParams = (IncludeParams)IPCBaseUI.getBaseIncludeParams(pageContext);
        }catch (ClassCastException e) {
            throw new IPCException(IPCEX66);
        }
        return includeParams;
    }
    
    public class IncludeParams extends IPCBaseUI.IncludeParams {
    	private MessageUIBean message;

        /**
         * @param ipcBaseUI
         */
        IncludeParams(
            UIContext ipcBaseUI,
            MessageUIBean message,
            Hashtable customerParams) {
            super(ipcBaseUI, customerParams);
            this.message = message;
        }
        


		/**
		 * @return
		 */
		public MessageUIBean getMessage() {
			return message;
		}

    }
    
}
