package com.sap.isa.ipc.ui.jsp.uiclass;

import java.io.IOException;
import java.util.Hashtable;
import java.util.Stack;

import javax.servlet.ServletException;
import javax.servlet.jsp.PageContext;

import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.ipc.ui.jsp.beans.GroupDeltaUIBean;
import com.sap.spc.remote.client.object.IPCException;

public class ComparisonTblGroupUI extends IPCBaseUI {

    //helper instance in order to create the parameter objects
    private static ComparisonTblGroupUI comparisonTblGroupUI = new ComparisonTblGroupUI();
    private static IsaLocation loc = IsaLocation.getInstance(ComparisonTblGroupUI.class.getName());
    
    public static void include(
        PageContext pageContext,
        String comparisionTblGroupPage,
        UIContext ipcBaseUI,
        GroupDeltaUIBean groupBean,
        Hashtable customerParams) {

            IncludeParams includeParams =
                comparisonTblGroupUI.new IncludeParams(
                    ipcBaseUI,
                    groupBean,
                    customerParams);
            Stack paramStack = IPCBaseUI.getParamStack(pageContext);
            paramStack.push(includeParams);
            try {
                pageContext.include(comparisionTblGroupPage);
            } catch (ServletException e) {
                loc.error(IPCEX70 + comparisionTblGroupPage, e.getRootCause());
                throw new RuntimeException(IPCEX57, e.getRootCause());
            } catch (IOException e) {
                loc.error(IPCEX70 + comparisionTblGroupPage, e);
                throw new RuntimeException(IPCEX58, e);
            }
            paramStack.pop();
    }
    
    public static IncludeParams getIncludeParams(PageContext pageContext) {
        IncludeParams includeParams = null;
        try{
            includeParams = (IncludeParams)IPCBaseUI.getBaseIncludeParams(pageContext);
        }catch (ClassCastException e) {
            throw new IPCException(IPCEX59);
        }
        return includeParams;
    }
    
    public class IncludeParams extends IPCBaseUI.IncludeParams {
        private GroupDeltaUIBean groupBean; 

        /**
         * @param ipcBaseUI
         */
        IncludeParams(
            UIContext ipcBaseUI,
            GroupDeltaUIBean groupBean,
            Hashtable customerParams) {
            super(ipcBaseUI, customerParams);
            this.groupBean = groupBean;
        }
        

        /**
         * @return
         */
        public GroupDeltaUIBean getGroupDeltaBean() {
            return groupBean;
        }
    }
}