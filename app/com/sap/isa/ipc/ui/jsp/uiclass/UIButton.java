/*
 * UIButton:
 * Represents a button on the UI.
 * Provides shortcutkey.
 */
package com.sap.isa.ipc.ui.jsp.uiclass;


public class UIButton extends UIElement {

    public UIButton(String name, UIContext uiContext) {
        super(name, uiContext);
    }

    public String toString() {
        String returnString = new String();
        returnString += UIButton.class.getName();
        returnString += "\nname: " + this.getName(); //$NON-NLS-1$
        returnString += "\nkey: " + this.getShortcutKey(); //$NON-NLS-1$
        return returnString;
    }

    public static void main(String[] args) {
        UIContext myContext = new UIContext();
        UIButton testButton = new UIButton("testbutton", myContext); //$NON-NLS-1$
        System.out.println(testButton);
    }

}
