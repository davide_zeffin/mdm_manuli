/*
 * Created on 14.07.2004
 *
 * To change the template for this generated file go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
package com.sap.isa.ipc.ui.jsp.uiclass;

import java.io.IOException;
import java.util.Hashtable;
import java.util.Stack;

import javax.servlet.ServletException;
import javax.servlet.jsp.PageContext;

import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.ipc.ui.jsp.beans.CharacteristicUIBean;
import com.sap.isa.ipc.ui.jsp.beans.GroupUIBean;
import com.sap.isa.ipc.ui.jsp.beans.InstanceUIBean;
import com.sap.spc.remote.client.object.IPCException;

/**
 * @author 
 *
 * To change the template for this generated type comment go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
public class CharacteristicUI extends IPCBaseUI {
    public static boolean showIndicatorForRequiredCstic(UIContext uiContext, CharacteristicUIBean characteristicUIBean) {
        return !uiContext.getShowStatusLights() && characteristicUIBean.getBusinessObject().isRequired();
    }
    public static boolean showStatusLightForRequiredCstic() {
        return false;
    }
    public static boolean showClearValuesLink(
        UIContext uiContext,
        CharacteristicUIBean cstic) {
        if (!uiContext.getDisplayMode()
            && !cstic.getBusinessObject().isReadOnly()
            && cstic.getBusinessObject().hasAssignedValuesByUser()) {
            return true;
        } else {
            return false;
        }
    
    }
    private static CharacteristicUI characteristicsUI = new CharacteristicUI();
    private static IsaLocation loc = IsaLocation.getInstance(CharacteristicUI.class.getName());
    
    public static void include(
        PageContext pageContext,
        String characteristicController,
        UIContext uiContext,
        InstanceUIBean instance,
        GroupUIBean charGroup,
        CharacteristicUIBean cstic,
        UIArea uiArea,
        Hashtable customerParams) {
        IncludeParams includeParams =
            characteristicsUI.new IncludeParams(
                uiContext,
                characteristicController,
                instance,
                charGroup,
                cstic,
                uiArea,
                customerParams);
        Stack paramStack = IPCBaseUI.getParamStack(pageContext);
        paramStack.push(includeParams);
        try {
            pageContext.include(characteristicController);
        } catch (ServletException e) {
            loc.error(IPCEX70 + characteristicController, e.getRootCause());
            throw new RuntimeException(IPCEX70 + characteristicController, e.getRootCause());
//TODO: Exceptionhandling            throw new IPCException(IPCEX15, null, e);
        } catch (IOException e) {
            loc.error(IPCEX70 + characteristicController, e);
            throw new RuntimeException(IPCEX70 + characteristicController, e);
//TODO: Exceptionhandling            throw new IPCException(IPCEX16, null, e);
        }
        paramStack.pop();
    }

    public static IncludeParams getIncludeParams(PageContext pageContext) {
        IncludeParams includeParams = null;
        try{
            includeParams = (IncludeParams)IPCBaseUI.getBaseIncludeParams(pageContext);
        }catch (ClassCastException e) {
            throw new IPCException(IPCEX14);
        }
        return includeParams;
    }
    

    /**
     * Decides whether the values has to be indented on characteristic-level.
     *
     * The decision whether to indent depends on:
     * <li>has the current characteristic a cstic-mime
     * <li>has the current characteristic any value that has a mime that should be shown in the work-area
     * <li>has any other characteristic of this group characteristic-mimes or value-mimes to be shown in the work area
     * <li>value of XCM-parameter characteristics.indentation. If it is set to false the values of the 
     * current characteristic won't be indented even if any other characteristic has mimes
     * <li>is the current characteristic expanded 
     * 
     * The decision table:
     * a b c d e  x
     * 0 0 0 0 0  0
     * 0 0 0 0 1  0
     * 0 0 0 1 0  0
     * 0 0 0 1 1  0       
     * 0 0 1 0 0  0 
     * 0 0 1 0 1  0        
     * 0 0 1 1 0  1   (integer: 6)
     * 0 0 1 1 1  1   (integer: 7)     
     * 0 1 0 0 0  0
     * 0 1 0 0 1  0
     * 0 1 0 1 0  0
     * 0 1 0 1 1  0
     * 0 1 1 0 0  0
     * 0 1 1 0 1  0
     * 0 1 1 1 0  1   (integer: 14) Explenation: Although value-mimes are available, indentation on cstic-level is done because cstic is collapsed (indentation on value-level won't work) 
     * 0 1 1 1 1  0
     * 1 0 0 0 0  0
     * 1 0 0 0 1  0
     * 1 0 0 1 0  0
     * 1 0 0 1 1  0       
     * 1 0 1 0 0  1   (integer: 20)
     * 1 0 1 0 1  1   (integer: 21)    
     * 1 0 1 1 0  1   (integer: 22)
     * 1 0 1 1 1  1   (integer: 23)       
     * 1 1 0 0 0  0
     * 1 1 0 0 1  0
     * 1 1 0 1 0  0
     * 1 1 0 1 1  0
     * 1 1 1 0 0  1   (integer: 28) Explenation: Although value-mimes are available, the cstic-mime is shown because cstic is collapsed
     * 1 1 1 0 1  0
     * 1 1 1 1 0  1   (integer: 30) Explenation: Although value-mimes are available, the cstic-mime is shown because cstic is collapsed 
     * 1 1 1 1 1  0
     * 
     * a = cstic.hasMimeForWorkArea
     * b = cstic.hasValueMimesForWorkArea
     * c = group.hasAnyMimesForWorkArea 
     *       (remark: even if groups were deactivated/not available the groupUIBean 
     *       will exist and then contain all characteristics of the instance)
     * d = value of XCM-parameter characteristics.indentation (true = values should be indented if there are other cstics with mimes)
     * e = expanded: is current characteristic displayed as expanded or collapsed
     * x = return (indentValues)
     *
     *  
     * Further decision for indentation on value-level is done in CharacteristicValueUI.indentValues()
     * 
     * @param groupUiBean of the current group
     * @param characteristicUIBean of the current characteristic
     * @param value of XCM-parameter characteristics.indentation
     * @param is current characteristic expanded
     * @return true if values should be indented
     */
    public static boolean indentValues(GroupUIBean groupBean, CharacteristicUIBean csticBean, boolean indentation, boolean expanded) {
        boolean indent = false;
        int indentationDecision = 0;
        // calculate indentationDecision due to decision table
        if (csticBean.hasMimesForWorkArea()) {
            indentationDecision += 16;
        }
        if (csticBean.hasValueMimesForWorkArea()) {
            indentationDecision += 8;
        }
        if (groupBean.hasAnyMimesForWorkArea()) {
            indentationDecision += 4;
        }
        if (indentation) {
            indentationDecision += 2;
        }
        if (expanded) {
            indentationDecision += 1;
        }

        switch (indentationDecision) {
            case 6:
            case 7:
            case 14:
            case 20:
            case 21:
            case 22:
            case 23:
            case 28:
            case 30:                        
                indent = true;
                break;
            case 16:
            case 17:
            case 18:
            case 19:
            case 24:
            case 25:
            case 26:
            case 27:
                // should never happen: csticBean.hasMimesForWorkArea()==true implies
                // that also groupBean.hasAnyMimesForWorkArea() is true (in these cases it is false)
                indent = false;
                break;
            default:
                indent = false;
                break;                        
        }
        // old-determination begin
        // because of above decision table the decision is:
        /*
        if (!csticBean.hasValueMimesForWorkArea() && groupBean.hasAnyMimesForWorkArea() && indentation && !expanded      // 0110
            || !csticBean.hasValueMimesForWorkArea() && groupBean.hasAnyMimesForWorkArea() && indentation && expanded    // 0111
            || csticBean.hasValueMimesForWorkArea() && groupBean.hasAnyMimesForWorkArea() && indentation && !expanded) { // 1110
            
            indent = true;
        } */           
        // old-end
        return indent;
    }    
    
    public class IncludeParams extends IPCBaseUI.IncludeParams {
        private String characteristicController;
        private InstanceUIBean instance;
        private GroupUIBean charGroup;
        private CharacteristicUIBean cstic;
        private UIArea uiArea;
        
        public IncludeParams(UIContext uiContext,
                             String characteristicController,
                             InstanceUIBean instance,
                             GroupUIBean charGroup,
                             CharacteristicUIBean cstic,
                             UIArea uiArea,
                             Hashtable customerParams) {
                                 super(uiContext, customerParams);
                                 this.characteristicController = characteristicController;
                                 this.instance = instance;
                                 this.charGroup = charGroup;
                                 this.cstic = cstic;
                                 this.uiArea = uiArea;
                             }
        /**
         * @return
         */
        public String getCharacteristicController() {
            return characteristicController;
        }

        /**
         * @return
         */
        public GroupUIBean getCharGroup() {
            return charGroup;
        }

        /**
         * @return
         */
        public CharacteristicUIBean getCstic() {
            return cstic;
        }

        /**
         * @return
         */
        public InstanceUIBean getInstance() {
            return instance;
        }

        /**
         * @return
         */
        public UIArea getUiArea() {
            return uiArea;
        }

    }

}
