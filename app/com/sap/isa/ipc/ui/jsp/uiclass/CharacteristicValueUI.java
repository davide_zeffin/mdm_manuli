/*
 * Created on 01.07.2004
 *
 * To change the template for this generated file go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
package com.sap.isa.ipc.ui.jsp.uiclass;

import java.io.IOException;
import java.util.Hashtable;
import java.util.Stack;

import javax.servlet.ServletException;
import javax.servlet.jsp.PageContext;

import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.ipc.ui.jsp.beans.CharacteristicUIBean;
import com.sap.isa.ipc.ui.jsp.beans.GroupUIBean;
import com.sap.isa.ipc.ui.jsp.beans.InstanceUIBean;
import com.sap.isa.ipc.ui.jsp.beans.ValueUIBean;
import com.sap.spc.remote.client.object.IPCException;

/**
 * @author 
 *
 * To change the template for this generated type comment go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
public class CharacteristicValueUI extends IPCBaseUI {

    private static IsaLocation loc =
        IsaLocation.getInstance(CharacteristicValueUI.class.getName());
    //helper instance in order to create the parameter objects
    private static CharacteristicValueUI csticValueUI =
        new CharacteristicValueUI();

    /**
     * Includes the paged given by @see layout into the current page.
     * @param pageContext
     * @param layout
     * @param valuesTile
     * @param freeTextTile
     * @param ipcBaseUI
     * @param characteristic
     * @param showDescriptions
     * @param showDescriptions
     * @param customerParams
     */
    public static void include(
        PageContext pageContext,
        String valuePage,
        UIContext ipcBaseUI,
        InstanceUIBean instance,
        GroupUIBean group,
        CharacteristicUIBean characteristic,
        ValueUIBean value,
        boolean showDescriptions,
        UIArea uiArea,
        boolean readOnlyMode,
        boolean detailsMode,
        Hashtable customerParams) {
        if (valuePage == null) {
            loc.error("Required parameter valuePage missing!"); //$NON-NLS-1$
        }
        if (characteristic == null) {
            loc.error("Required parameter characteristic missing!"); //$NON-NLS-1$
        }
        if (value == null) {
            loc.error("Required parameter value missing!"); //$NON-NLS-1$
        }

        IncludeParams includeParams =
            csticValueUI.new IncludeParams(
                ipcBaseUI,
                instance,
                group,
                characteristic,
                value,
                showDescriptions,
                uiArea,
                readOnlyMode,
                detailsMode,
                customerParams);
        Stack paramStack = IPCBaseUI.getParamStack(pageContext);
        paramStack.push(includeParams);
        try {
            pageContext.include(valuePage);
        } catch (ServletException e) {
            loc.error(IPCEX70 + valuePage, e);
            throw new RuntimeException(IPCEX6, e);
            //TODO:                throw new IPCException(IPCEX6, null, e);
        } catch (IOException e) {
            loc.error(IPCEX70 + valuePage, e);
            throw new RuntimeException(IPCEX7, e);
            //TODO:                throw new IPCException(IPCEX7, null, e);
        }
        paramStack.pop();
    }

    public static IncludeParams getIncludeParams(PageContext pageContext) {
        IncludeParams includeParams = null;
        try {
            includeParams =
                (IncludeParams) IPCBaseUI.getBaseIncludeParams(pageContext);
        } catch (ClassCastException e) {
            throw new IPCException(IPCEX2);
        }
        return includeParams;
    }

    
    /**
     * Decides whether the values has to be indented.
     * 
     * The decision whether to indent depends on:
     * <li>has the current characteristic any value that has a mime that should be shown in the work-area
     * <li>has any other characteristic of this group characteristic-mimes or value-mimes to be shown in the work area
     * 
     * The decision table:
     * a b  x
     * 0 0  0
     * 0 1  0
     * 1 0  1
     * 1 1  1
     * 
     * a = cstic.hasValueMimesForWorkArea
     * b = group.hasAnyMimesForWorkArea 
     *       (remark: even if groups were deactivated/not available the groupUIBean 
     *       will exist and then contain all characteristics of the instance)
     * x = return (indentValues)
     * 
     * @param groupUiBean of the current group
     * @param characteristicUIBean of the current characteristic
     * @return true if values should be indented
     */
    public static boolean indentValues(GroupUIBean groupBean, CharacteristicUIBean csticBean) {
    	if (groupBean == null); //groupBean is part of the decision table but not used in
    	//the simplified expression, this line is to avoid compiler warnings
        boolean indent = false;
        // because of above decision table the decision can be simplified:
        if (csticBean.hasValueMimesForWorkArea()) {
            indent = true;
        }            
        return indent;
    }

    public CharacteristicValueUI() {
    }

    /**
     * @author 
     *
     * To change the template for this generated type comment go to
     * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
     */
    public class IncludeParams extends IPCBaseUI.IncludeParams {
        private InstanceUIBean instance;
        private GroupUIBean group;
        private CharacteristicUIBean characteristic;
        private ValueUIBean value;
        private boolean showDescriptions;
        private UIArea uiArea;
        private boolean readOnlyMode;
        private boolean detailsMode;
        /**
         * @param ipcBaseUI
         */
        IncludeParams(
            UIContext ipcBaseUI,
            InstanceUIBean instance,
            GroupUIBean group,
            CharacteristicUIBean characteristic,
            ValueUIBean value,
            boolean showDescriptions,
            UIArea uiArea,
            boolean readOnlyMode,
            boolean detailsMode,
            Hashtable customerParams) {
            super(ipcBaseUI, customerParams);
            this.instance = instance;
            this.group = group;
            this.characteristic = characteristic;
            this.value = value;
            this.showDescriptions = showDescriptions;
            this.uiArea = uiArea;
            this.readOnlyMode = readOnlyMode;
            this.detailsMode = detailsMode;
        }

        /**
         * @return
         */
        public CharacteristicUIBean getCharacteristic() {
            return characteristic;
        }

        /**
         * @return
         */
        public ValueUIBean getValue() {
            return value;
        }

        /**
         * @return
         */
        public boolean isShowDescriptions() {
            return showDescriptions;
        }

        /**
         * @return
         */
        public UIArea getUiArea() {
            return uiArea;
        }

		/**
		 * @return
		 */
		public boolean isReadOnlyMode() {
			return readOnlyMode;
		}

        /**
         * @return
         */
        public boolean isDetailsMode() {
            return detailsMode;
        }


		/**
		 * @return
		 */
		public GroupUIBean getGroup() {
			return group;
		}

		/**
		 * @return
		 */
		public InstanceUIBean getInstance() {
			return instance;
		}

    }
}
