/*
 * Created on 14.07.2004
 *
 * To change the template for this generated file go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
package com.sap.isa.ipc.ui.jsp.uiclass;

import java.util.List;
import java.io.IOException;
import java.util.Hashtable;
import java.util.Stack;

import javax.servlet.ServletException;
import javax.servlet.jsp.PageContext;

import com.sap.spc.remote.client.object.IPCException;
import com.sap.isa.core.logging.IsaLocation;

/**
 * @author 
 *
 * To change the template for this generated type comment go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
public class ConflictTraceUI extends IPCBaseUI {
    
    //helper instance in order to create the parameter objects
    private static ConflictTraceUI conflictUI = new ConflictTraceUI();
    private static IsaLocation loc = IsaLocation.getInstance(ConflictTraceUI.class.getName());
    
    public static void include(
        PageContext pageContext,
        String conflictTracePage,
        UIContext ipcBaseUI,
        List conflictTrace,
        Hashtable customerParams) {

            IncludeParams includeParams =
                conflictUI.new IncludeParams(
                    ipcBaseUI,
                    conflictTrace,
                    customerParams);
            Stack paramStack = IPCBaseUI.getParamStack(pageContext);
            paramStack.push(includeParams);
            try {
                pageContext.include(conflictTracePage);
            } catch (ServletException e) {
                loc.error(IPCEX70 + conflictTracePage, e.getRootCause());
                throw new RuntimeException(IPCEX50, e.getRootCause());
//TODO:Exceptionhandling                throw new IPCException(IPCEX, null, e);
            } catch (IOException e) {
                loc.error(IPCEX70 + conflictTracePage, e);
                throw new RuntimeException(IPCEX51, e);
//TODO:Exceptionhandling                throw new IPCException(IPCEX, null, e);
            }
            paramStack.pop();
    }
    
    public static IncludeParams getIncludeParams(PageContext pageContext) {
        IncludeParams includeParams = null;
        try{
            includeParams = (IncludeParams)IPCBaseUI.getBaseIncludeParams(pageContext);
        }catch (ClassCastException e) {
            throw new IPCException(IPCEX52);
        }
        return includeParams;
    }
    
    public class IncludeParams extends IPCBaseUI.IncludeParams {
        //TODO: remove access to business object
        private List conflictTrace;

        /**
         * @param ipcBaseUI
         */
        IncludeParams(
            UIContext ipcBaseUI,
            List conflictTrace,
            Hashtable customerParams) {
            super(ipcBaseUI, customerParams);
            this.conflictTrace = conflictTrace;
        }
        


		/**
		 * @return
		 */
		public List getConflictTrace() {
			return conflictTrace;
		}

    }
    
}
