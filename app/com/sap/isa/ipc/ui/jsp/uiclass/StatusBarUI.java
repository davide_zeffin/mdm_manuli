/*
 * Created on 14.07.2004
 *
 * To change the template for this generated file go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
package com.sap.isa.ipc.ui.jsp.uiclass;

import java.io.IOException;
import java.util.Hashtable;
import java.util.Stack;

import javax.servlet.ServletException;
import javax.servlet.jsp.PageContext;

import com.sap.spc.remote.client.object.IPCException;
import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.ipc.ui.jsp.beans.ConfigUIBean;

/**
 * @author 
 *
 * To change the template for this generated type comment go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
public class StatusBarUI extends IPCBaseUI {
    
    //helper instance in order to create the parameter objects
    private static StatusBarUI characteristicUI = new StatusBarUI();
    private static IsaLocation loc = IsaLocation.getInstance(StatusBarUI.class.getName());
    
    public static void include(
        PageContext pageContext,
        String statusBarPage,
        UIContext ipcBaseUI,
        ConfigUIBean configuration,
        Hashtable customerParams) {

            IncludeParams includeParams =
                characteristicUI.new IncludeParams(
                    ipcBaseUI,
                    configuration,
                    customerParams);
            Stack paramStack = IPCBaseUI.getParamStack(pageContext);
            paramStack.push(includeParams);
            try {
                pageContext.include(statusBarPage);
            } catch (ServletException e) {
                loc.error(IPCEX70 + statusBarPage, e.getRootCause());
                throw new RuntimeException(IPCEX44, e.getRootCause());
//TODO:Exceptionhandling                throw new IPCException(IPCEX, null, e);
            } catch (IOException e) {
                loc.error(IPCEX70 + statusBarPage, e);
                throw new RuntimeException(IPCEX45, e);
//TODO:Exceptionhandling                throw new IPCException(IPCEX, null, e);
            }
            paramStack.pop();
    }
    
    public static IncludeParams getIncludeParams(PageContext pageContext) {
        IncludeParams includeParams = null;
        try{
            includeParams = (IncludeParams)IPCBaseUI.getBaseIncludeParams(pageContext);
        }catch (ClassCastException e) {
            throw new IPCException(IPCEX46);
        }
        return includeParams;
    }
    
    public class IncludeParams extends IPCBaseUI.IncludeParams {
        private ConfigUIBean configuration;

        /**
         * @param ipcBaseUI
         */
        IncludeParams(
            UIContext ipcBaseUI,
            ConfigUIBean configuration,
            Hashtable customerParams) {
            super(ipcBaseUI, customerParams);
            this.configuration = configuration;
        }
        


        /**
         * @return
         */
        public ConfigUIBean getConfiguration() {
            return configuration;
        }

    }
    
}
