/*
 * Created on 14.07.2004
 *
 * To change the template for this generated file go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
package com.sap.isa.ipc.ui.jsp.uiclass;

import java.io.IOException;
import java.util.Hashtable;
import java.util.Stack;

import javax.servlet.ServletException;
import javax.servlet.jsp.PageContext;

import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.ipc.ui.jsp.beans.CharacteristicUIBean;
import com.sap.isa.ipc.ui.jsp.beans.ConfigUIBean;
import com.sap.isa.ipc.ui.jsp.beans.GroupUIBean;
import com.sap.isa.ipc.ui.jsp.beans.InstanceUIBean;
import com.sap.spc.remote.client.object.IPCException;

/**
 * @author 
 *
 * To change the template for this generated type comment go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
public class CurrentConfigurationUI extends IPCBaseUI {
    
    //helper instance in order to create the parameter objects
    private static CurrentConfigurationUI characteristicUI = new CurrentConfigurationUI();
    private static IsaLocation loc = IsaLocation.getInstance(CurrentConfigurationUI.class.getName());
    
    public static void include(
        PageContext pageContext,
        String configPage,
        UIContext ipcBaseUI,
        ConfigUIBean config,
        InstanceUIBean instance,
        GroupUIBean group,
        CharacteristicUIBean characteristic,
        boolean fromConflicts,
        Hashtable customerParams) {
            if (config == null) {
                loc.error(IPCEX60);
            }

            IncludeParams includeParams =
                characteristicUI.new IncludeParams(
                    ipcBaseUI,
                    config,
                    instance,
                    group,
                    characteristic,
                    fromConflicts,
                    customerParams);
            Stack paramStack = IPCBaseUI.getParamStack(pageContext);
            paramStack.push(includeParams);
            try {
                pageContext.include(configPage);
            } catch (ServletException e) {
                loc.error(IPCEX70 + configPage, e.getRootCause());
                throw new RuntimeException(IPCEX47, e.getRootCause());
//TODO:Exceptionhandling                throw new IPCException(IPCEX, null, e);
            } catch (IOException e) {
                loc.error(IPCEX70 + configPage, e);
                throw new RuntimeException(IPCEX48, e);
//TODO:Exceptionhandling                throw new IPCException(IPCEX, null, e);
            }
            paramStack.pop();
    }
    
    public static IncludeParams getIncludeParams(PageContext pageContext) {
        IncludeParams includeParams = null;
        try{
            includeParams = (IncludeParams)IPCBaseUI.getBaseIncludeParams(pageContext);
        }catch (ClassCastException e) {
            throw new IPCException(IPCEX49);
        }
        return includeParams;
    }
    
    public class IncludeParams extends IPCBaseUI.IncludeParams {
        private ConfigUIBean config;
        private InstanceUIBean instance;
        private GroupUIBean group;
        private CharacteristicUIBean characteristic;
        private boolean fromConflicts;

        /**
         * @param ipcBaseUI
         */
        IncludeParams(
            UIContext ipcBaseUI,
            ConfigUIBean config,
            InstanceUIBean instance,
            GroupUIBean group,
            CharacteristicUIBean characteristic,
            boolean fromConflicts,
            Hashtable customerParams) {
            super(ipcBaseUI, customerParams);
            this.config = config;
            this.instance = instance;
            this.group = group;
            this.characteristic = characteristic;
            this.fromConflicts = fromConflicts;
        }
        


        /**
         * @return
         */
        public ConfigUIBean getConfig() {
            return config;
        }

        /**
         * @return
         */
        public boolean isFromConflicts() {
            return fromConflicts;
        }

		/**
		 * @return
		 */
		public GroupUIBean getGroup() {
			return group;
		}

		/**
		 * @return
		 */
		public InstanceUIBean getInstance() {
			return instance;
		}

		/**
		 * @return
		 */
		public CharacteristicUIBean getCharacteristic() {
			return characteristic;
		}

    }
    
}
