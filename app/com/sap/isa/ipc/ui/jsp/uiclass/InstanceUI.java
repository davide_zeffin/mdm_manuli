/*
 * Created on 14.07.2004
 *
 * To change the template for this generated file go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
package com.sap.isa.ipc.ui.jsp.uiclass;

import java.io.IOException;
import java.util.Hashtable;
import java.util.Stack;

import javax.servlet.ServletException;
import javax.servlet.jsp.PageContext;

import com.sap.spc.remote.client.object.IPCException;
import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.ipc.ui.jsp.beans.GroupUIBean;
import com.sap.isa.ipc.ui.jsp.beans.InstanceUIBean;

/**
 * @author 
 *
 * To change the template for this generated type comment go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
public class InstanceUI extends IPCBaseUI {
    
    //helper instance in order to create the parameter objects
    private static InstanceUI characteristicUI = new InstanceUI();
    private static IsaLocation loc = IsaLocation.getInstance(InstanceUI.class.getName());
    
    public static void include(
        PageContext pageContext,
        String instancePage,
        UIContext ipcBaseUI,
        InstanceUIBean instance,
        GroupUIBean group,
        UIArea uiArea,
        Hashtable customerParams) {

            IncludeParams includeParams =
                characteristicUI.new IncludeParams(
                    ipcBaseUI,
                    instance,
                    group,
                    uiArea,
                    customerParams);
            Stack paramStack = IPCBaseUI.getParamStack(pageContext);
            paramStack.push(includeParams);
            try {
                pageContext.include(instancePage);
            } catch (ServletException e) {
                loc.error(IPCEX70 + instancePage, e.getRootCause());
                throw new RuntimeException(IPCEX54, e.getRootCause());
//TODO:Exceptionhandling                throw new IPCException(IPCEX, null, e);
            } catch (IOException e) {
                loc.error(IPCEX70 + instancePage, e);
                throw new RuntimeException(IPCEX55, e);
//TODO:Exceptionhandling                throw new IPCException(IPCEX, null, e);
            }
            paramStack.pop();
    }
    
    public static IncludeParams getIncludeParams(PageContext pageContext) {
        IncludeParams includeParams = null;
        try{
            includeParams = (IncludeParams)IPCBaseUI.getBaseIncludeParams(pageContext);
        }catch (ClassCastException e) {
            throw new IPCException(IPCEX56);
        }
        return includeParams;
    }
    
    public class IncludeParams extends IPCBaseUI.IncludeParams {
        private InstanceUIBean instance;
        private GroupUIBean group;
        private UIArea uiArea;

        /**
         * @param ipcBaseUI
         */
        IncludeParams(
            UIContext ipcBaseUI,
            InstanceUIBean instance,
            GroupUIBean group,
            UIArea uiArea,
            Hashtable customerParams) {
            super(ipcBaseUI, customerParams);
            this.instance = instance;
            this.group = group;
            this.uiArea = uiArea;
        }
        


        /**
         * @return
         */
        public GroupUIBean getGroup() {
            return group;
        }

        /**
         * @return
         */
        public InstanceUIBean getInstance() {
            return instance;
        }

        /**
         * @return
         */
        public UIArea getUiArea() {
            return uiArea;
        }

    }
    
}
