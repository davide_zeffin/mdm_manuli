/*
 * Created on 14.07.2004
 *
 * To change the template for this generated file go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
package com.sap.isa.ipc.ui.jsp.uiclass;

import java.io.IOException;
import java.util.Hashtable;
import java.util.Stack;

import javax.servlet.ServletException;
import javax.servlet.jsp.PageContext;

import com.sap.spc.remote.client.object.IPCException;
import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.ipc.ui.jsp.beans.GroupUIBean;
import com.sap.isa.ipc.ui.jsp.beans.InstanceUIBean;

/**
 * @author 
 *
 * To change the template for this generated type comment go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
public class CharacteristicsUI extends IPCBaseUI {
    private static CharacteristicsUI characteristicsUI = new CharacteristicsUI();
    private static IsaLocation loc = IsaLocation.getInstance(CharacteristicsUI.class.getName());
    
    public static void include(
        PageContext pageContext,
        String characteristicController,
        UIContext uiContext,
        InstanceUIBean instance,
        GroupUIBean charGroup,
        UIArea uiArea,
        Hashtable customerParams) {
        IncludeParams includeParams =
            characteristicsUI.new IncludeParams(
                uiContext,
                characteristicController,
                instance,
                charGroup,
                uiArea,
                customerParams);
        Stack paramStack = IPCBaseUI.getParamStack(pageContext);
        paramStack.push(includeParams);
        try {
            pageContext.include(characteristicController);
        } catch (ServletException e) {
            loc.error(IPCEX70 + characteristicController, e);
            throw new RuntimeException(IPCEX70 + characteristicController, e.getRootCause());
//TODO: Exceptionhandling            throw new IPCException(IPCEX15, null, e);
        } catch (IOException e) {
            loc.error(IPCEX70 + characteristicController, e);
            throw new RuntimeException(IPCEX70 + characteristicController, e);
//TODO: Exceptionhandling            throw new IPCException(IPCEX16, null, e);
        }
        paramStack.pop();
    }

    public static IncludeParams getIncludeParams(PageContext pageContext) {
        IncludeParams includeParams = null;
        try{
            includeParams = (IncludeParams)IPCBaseUI.getBaseIncludeParams(pageContext);
        }catch (ClassCastException e) {
            throw new IPCException(IPCEX14);
        }
        return includeParams;
    }
    
    public class IncludeParams extends IPCBaseUI.IncludeParams {
        private String characteristicController;
        private InstanceUIBean instance;
        private GroupUIBean charGroup;
        private UIArea uiArea;
        public IncludeParams(UIContext uiContext,
                             String characteristicController,
                             InstanceUIBean instance,
                             GroupUIBean charGroup,
                             UIArea uiArea,
                             Hashtable customerParams) {
                                 super(uiContext, customerParams);
                                 this.characteristicController = characteristicController;
                                 this.instance = instance;
                                 this.charGroup = charGroup;
                                 this.uiArea = uiArea;
                             }
        /**
         * @return
         */
        public String getCharacteristicController() {
            return characteristicController;
        }

        /**
         * @return
         */
        public GroupUIBean getCharGroup() {
            return charGroup;
        }

        /**
         * @return
         */
        public InstanceUIBean getInstance() {
            return instance;
        }

        /**
         * @return
         */
        public UIArea getUiArea() {
            return uiArea;
        }

    }

}
