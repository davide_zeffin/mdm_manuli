/*
 * Created on 14.07.2004
 *
 * To change the template for this generated file go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
package com.sap.isa.ipc.ui.jsp.uiclass;

import java.io.IOException;
import java.util.Hashtable;
import java.util.Stack;

import javax.servlet.ServletException;
import javax.servlet.jsp.PageContext;

import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.ipc.ui.jsp.beans.GroupUIBean;
import com.sap.isa.ipc.ui.jsp.beans.InstanceUIBean;

/**
 * @author 
 *
 * To change the template for this generated type comment go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
public class CharacteristicGroupsUI extends IPCBaseUI {
    private static CharacteristicGroupsUI characteristicsUI = new CharacteristicGroupsUI();
    private static IsaLocation loc = IsaLocation.getInstance(CharacteristicGroupsUI.class.getName());
    
    public static void include(
        PageContext pageContext,
        String groupsPage,
        UIContext uiContext,
        InstanceUIBean instance,
        GroupUIBean charGroup,
        Hashtable customerParams) {
        IncludeParams includeParams =
            characteristicsUI.new IncludeParams(
                uiContext,
                groupsPage,
                instance,
                charGroup,
                customerParams);
        Stack paramStack = IPCBaseUI.getParamStack(pageContext);
        paramStack.push(includeParams);
        try {
            pageContext.include(groupsPage);
        } catch (ServletException e) {
            loc.error(IPCEX70 + groupsPage, e.getRootCause());
            throw new RuntimeException(IPCEX30, e.getRootCause());
//TODO: Exceptionhandling            throw new IPCException(IPCEX15, null, e);
        } catch (IOException e) {
            loc.error(IPCEX70 + groupsPage, e);
            throw new RuntimeException(IPCEX31, e);
//TODO: Exceptionhandling            throw new IPCException(IPCEX16, null, e);
        }
        paramStack.pop();
    }

    public static IncludeParams getIncludeParams(PageContext pageContext) {
        IncludeParams includeParams = null;
        includeParams = (IncludeParams)IPCBaseUI.getBaseIncludeParams(pageContext);
        return includeParams;
    }
    
    public class IncludeParams extends IPCBaseUI.IncludeParams {
        private String groupsPage;
        private InstanceUIBean instance;
        private GroupUIBean charGroup;
        public IncludeParams(UIContext uiContext,
                             String groupsPage,
                             InstanceUIBean instance,
                             GroupUIBean charGroup,
                             Hashtable customerParams) {
                                 super(uiContext, customerParams);
                                 this.groupsPage = groupsPage;
                                 this.instance = instance;
                                 this.charGroup = charGroup;
                             }
        /**
         * @return
         */
        public GroupUIBean getCharGroup() {
            return charGroup;
        }

        /**
         * @return
         */
        public InstanceUIBean getInstance() {
            return instance;
        }

        /**
         * @return
         */
        public String getGroupsPage() {
            return groupsPage;
        }

    }

}
