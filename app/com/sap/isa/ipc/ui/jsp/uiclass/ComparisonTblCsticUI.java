package com.sap.isa.ipc.ui.jsp.uiclass;

import java.io.IOException;
import java.util.Hashtable;
import java.util.Stack;

import javax.servlet.ServletException;
import javax.servlet.jsp.PageContext;

import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.ipc.ui.jsp.beans.CharacteristicDeltaUIBean;
import com.sap.spc.remote.client.object.IPCException;

public class ComparisonTblCsticUI extends IPCBaseUI {
    
    //helper instance in order to create the parameter objects
    private static ComparisonTblCsticUI comparisonTblCsticUI = new ComparisonTblCsticUI();
    private static IsaLocation loc = IsaLocation.getInstance(ComparisonTblCsticUI.class.getName());
    
    public static void include(
        PageContext pageContext,
        String comparisionTblCsticPage,
        UIContext ipcBaseUI,
        CharacteristicDeltaUIBean csticBean,
        Hashtable customerParams) {

            IncludeParams includeParams =
                comparisonTblCsticUI.new IncludeParams(
                    ipcBaseUI,
                    csticBean,
                    customerParams);
            Stack paramStack = IPCBaseUI.getParamStack(pageContext);
            paramStack.push(includeParams);
            try {
                pageContext.include(comparisionTblCsticPage);
            } catch (ServletException e) {
                loc.error(IPCEX70 + comparisionTblCsticPage, e.getRootCause());
                throw new RuntimeException(IPCEX57, e.getRootCause());
            } catch (IOException e) {
                loc.error(IPCEX70 + comparisionTblCsticPage, e);
                throw new RuntimeException(IPCEX58, e);
            }
            paramStack.pop();
    }
    
    public static IncludeParams getIncludeParams(PageContext pageContext) {
        IncludeParams includeParams = null;
        try{
            includeParams = (IncludeParams)IPCBaseUI.getBaseIncludeParams(pageContext);
        }catch (ClassCastException e) {
            throw new IPCException(IPCEX59);
        }
        return includeParams;
    }
    
    public class IncludeParams extends IPCBaseUI.IncludeParams {
        private CharacteristicDeltaUIBean csticBean; 

        /**
         * @param ipcBaseUI
         */
        IncludeParams(
            UIContext ipcBaseUI,
            CharacteristicDeltaUIBean csticBean,
            Hashtable customerParams) {
            super(ipcBaseUI, customerParams);
            this.csticBean = csticBean;
        }
        

        /**
         * @return
         */
        public CharacteristicDeltaUIBean getCharacteristicDeltaBean() {
            return csticBean;
        }
    }
}
