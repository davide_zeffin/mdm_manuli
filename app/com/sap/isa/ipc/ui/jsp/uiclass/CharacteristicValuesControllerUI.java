/*
 * Created on 14.07.2004
 *
 * To change the template for this generated file go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
package com.sap.isa.ipc.ui.jsp.uiclass;

import java.io.IOException;
import java.util.Hashtable;
import java.util.Stack;

import javax.servlet.ServletException;
import javax.servlet.jsp.PageContext;

import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.ipc.ui.jsp.beans.CharacteristicUIBean;
import com.sap.isa.ipc.ui.jsp.beans.GroupUIBean;
import com.sap.isa.ipc.ui.jsp.beans.InstanceUIBean;
import com.sap.spc.remote.client.object.IPCException;

/**
 * @author 
 *
 * To change the template for this generated type comment go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
public class CharacteristicValuesControllerUI extends IPCBaseUI {
    
    //helper instance in order to create the parameter objects
    private static CharacteristicValuesControllerUI characteristicUI = new CharacteristicValuesControllerUI();
    private static IsaLocation loc = IsaLocation.getInstance(CharacteristicValuesControllerUI.class.getName());
    
    public static void include(
        PageContext pageContext,
        String characteristicValuesController,
        UIContext ipcBaseUI,
        InstanceUIBean instance,
        GroupUIBean group,
        CharacteristicUIBean currentCharacteristic,
        boolean showExpandMode,
        boolean showDescriptions,
        UIArea uiArea,
        boolean readOnlyMode,
        boolean detailsMode,
        Hashtable customerParams) {

            IncludeParams includeParams =
                characteristicUI.new IncludeParams(
                    ipcBaseUI,
                    characteristicValuesController,
                    instance,
                    group,
                    currentCharacteristic,
                    showExpandMode,
                    showDescriptions,
                    uiArea,
                    readOnlyMode,
                    detailsMode,
                    customerParams);
            Stack paramStack = IPCBaseUI.getParamStack(pageContext);
            paramStack.push(includeParams);
            try {
                pageContext.include(characteristicValuesController);
            } catch (ServletException e) {
                loc.error(IPCEX70 + characteristicValuesController, e);
                throw new RuntimeException(IPCEX13, e);
//TODO:Exceptionhandling                throw new IPCException(IPCEX13, null, e);
            } catch (IOException e) {
                loc.error(IPCEX70 + characteristicValuesController, e);
                throw new RuntimeException(IPCEX14, e);
//TODO:Exceptionhandling                throw new IPCException(IPCEX14, null, e);
            }
            paramStack.pop();
    }
    
    public static IncludeParams getIncludeParams(PageContext pageContext) {
        IncludeParams includeParams = null;
        try{
            includeParams = (IncludeParams)IPCBaseUI.getBaseIncludeParams(pageContext);
        }catch (ClassCastException e) {
            throw new IPCException(IPCEX12);
        }
        return includeParams;
    }
    
    public class IncludeParams extends IPCBaseUI.IncludeParams {
        private String characteristicValuesController;
        private InstanceUIBean instance;
        private GroupUIBean group;
        private CharacteristicUIBean currentCharacteristic;
        private boolean showExpandMode;
        private boolean showDescriptions;
        private UIArea uiArea;
        private boolean readOnlyMode;
        private boolean detailsMode;

        /**
         * @param ipcBaseUI
         */
        IncludeParams(
            UIContext ipcBaseUI,
            String characteristicValuesController,
            InstanceUIBean instance,
            GroupUIBean group,
            CharacteristicUIBean currentCharacteristic,
            boolean showExpandMode,
            boolean showDescriptions,
            UIArea uiArea,
            boolean readOnlyMode,
            boolean detailsMode,
            Hashtable customerParams) {
            super(ipcBaseUI, customerParams);
            this.instance = instance;
            this.group = group;
            this.currentCharacteristic = currentCharacteristic;
            this.showExpandMode = showExpandMode;
            this.showDescriptions = showDescriptions;
            this.characteristicValuesController = characteristicValuesController;
            this.uiArea = uiArea;
            this.readOnlyMode = readOnlyMode;
            this.detailsMode = detailsMode;
        }
        

        /**
         * @return
         */
        public String getCharacteristicValuesController() {
            return this.characteristicValuesController;
        }

        /**
         * @return
         */
        public CharacteristicUIBean getCurrentCharacteristic() {
            return currentCharacteristic;
        }

        /**
         * @return
         */
        public boolean isShowDescriptions() {
            return showDescriptions;
        }

        /**
         * @return
         */
        public boolean isShowExpandMode() {
            return showExpandMode;
        }

        /**
         * @return
         */
        public UIArea getUiArea() {
            return uiArea;
        }

		/**
		 * @return
		 */
		public boolean isReadOnlyMode() {
			return readOnlyMode;
		}

        /**
         * @return
         */
        public boolean isDetailsMode() {
            return detailsMode;
        }

		/**
		 * @return
		 */
		public GroupUIBean getGroup() {
			return group;
		}

		/**
		 * @return
		 */
		public InstanceUIBean getInstance() {
			return instance;
		}

    }
    
}
