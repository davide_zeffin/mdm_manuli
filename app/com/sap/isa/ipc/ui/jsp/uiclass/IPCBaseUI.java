/*
 * Created on 01.07.2004
 *
 * To change the template for this generated file go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
package com.sap.isa.ipc.ui.jsp.uiclass;

import java.util.Hashtable;
import java.util.Stack;

import javax.servlet.jsp.PageContext;

import com.sap.spc.remote.client.object.IPCException;
import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.ipc.ui.jsp.IPCExceptionConstants;

/**
 * @author 
 *
 * To change the template for this generated type comment go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
public class IPCBaseUI implements IPCExceptionConstants {
    private static String INCLUDE_PARAMS = "JSP_INCLUDE_PARAMS"; //$NON-NLS-1$
    static IsaLocation loc = IsaLocation.getInstance(IPCBaseUI.class.getName());
    
    
    /**
     * @author 
     *
     * To change the template for this generated type comment go to
     * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
     */
    public class IncludeParams {
        private UIContext ipcBaseUI;
        private Hashtable customerParams;
        
        IncludeParams(UIContext ipcBaseUI, Hashtable customerParams) {
            if (ipcBaseUI == null) {
                loc.error("Required parameter ipcBaseUI missing!"); //$NON-NLS-1$
            }
            this.ipcBaseUI = ipcBaseUI;
            this.customerParams = customerParams;
            
        }

        /**
         * @return
         */
        public UIContext getIpcBaseUI() {
            return ipcBaseUI;
        }

        /**
         * @return
         */
        public Hashtable getCustomerParams() {
            return customerParams;
        }

    }
    
    public static Stack getParamStack(PageContext pageContext) {
        Object paramStackObject = pageContext.getRequest().getAttribute(INCLUDE_PARAMS);
        if (paramStackObject == null) {
            paramStackObject = new Stack();
            pageContext.getRequest().setAttribute(INCLUDE_PARAMS, paramStackObject);
        }
        Stack paramStack;
        try {
            paramStack = (Stack)paramStackObject;
        } catch (ClassCastException e) {
            throw new IPCException(IPCEX3, null, e);
        }
        return paramStack;
    }
    
    protected static IncludeParams getBaseIncludeParams(PageContext pageContext) {
        Stack paramStack = getParamStack(pageContext);
        Object includeParamsObject = null;
        try {
            includeParamsObject = paramStack.peek();
        }catch (RuntimeException rte) {
            throw new IPCException(IPCEX8, null, rte);
        }
        if (includeParamsObject == null) {
            throw new IPCException(IPCEX4);
        }
        IncludeParams includeParams;
        try {
            includeParams = (IncludeParams)includeParamsObject;
        }catch (ClassCastException e) {
            throw new IPCException(IPCEX5, null, e);
        }
        return includeParams;
    }

}
