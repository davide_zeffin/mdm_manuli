/*
 * Created on 14.07.2004
 *
 * To change the template for this generated file go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
package com.sap.isa.ipc.ui.jsp.uiclass;

import java.io.IOException;
import java.util.Hashtable;
import java.util.Stack;

import javax.servlet.ServletException;
import javax.servlet.jsp.PageContext;

import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.ipc.ui.jsp.beans.ConflictHandlingShortcutUIBean;
import com.sap.spc.remote.client.object.IPCException;

/**
 * @author 
 *
 * To change the template for this generated type comment go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
public class ConflictHandlingShortcutsUI extends IPCBaseUI {
    
    //helper instance in order to create the parameter objects
    private static ConflictHandlingShortcutsUI conflictHandlingShortcutsUI = new ConflictHandlingShortcutsUI();
    private static IsaLocation loc = IsaLocation.getInstance(ConflictHandlingShortcutsUI.class.getName());
    
    public static void include(
        PageContext pageContext,
        String messagesPage,
        UIContext ipcBaseUI,
        ConflictHandlingShortcutUIBean conflictHandlingShortcut,
        Hashtable customerParams) {

            IncludeParams includeParams =
			conflictHandlingShortcutsUI.new IncludeParams(
                    ipcBaseUI,
                    conflictHandlingShortcut,
                    customerParams);
            Stack paramStack = IPCBaseUI.getParamStack(pageContext);
            paramStack.push(includeParams);
            try {
                pageContext.include(messagesPage);
            } catch (ServletException e) {
                loc.error(IPCEX70 + messagesPage, e.getRootCause());
                throw new RuntimeException(IPCEX68, e.getRootCause());
//TODO:Exceptionhandling                throw new IPCException(IPCEX, null, e);
            } catch (IOException e) {
                loc.error(IPCEX70 + messagesPage, e);
                throw new RuntimeException(IPCEX69, e);
//TODO:Exceptionhandling                throw new IPCException(IPCEX, null, e);
            }
            paramStack.pop();
    }
    
    public static IncludeParams getIncludeParams(PageContext pageContext) {
        IncludeParams includeParams = null;
        try{
            includeParams = (IncludeParams)IPCBaseUI.getBaseIncludeParams(pageContext);
        }catch (ClassCastException e) {
            throw new IPCException(IPCEX66);
        }
        return includeParams;
    }
    
    public class IncludeParams extends IPCBaseUI.IncludeParams {
    	
    	private ConflictHandlingShortcutUIBean conflictHandlingShortcut;

        /**
         * @param ipcBaseUI
         */
        IncludeParams(
            UIContext ipcBaseUI,
            ConflictHandlingShortcutUIBean conflictHandlingShortcut,
            Hashtable customerParams) {
            super(ipcBaseUI, customerParams);
            this.conflictHandlingShortcut = conflictHandlingShortcut;
        }
        


		/**
		 * @return
		 */
		public ConflictHandlingShortcutUIBean getConflictHandlingShortcut() {
			return conflictHandlingShortcut;
		}

    }
    
}
