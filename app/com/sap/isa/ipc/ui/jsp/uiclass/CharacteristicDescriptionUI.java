/*
 * Created on 14.03.2006
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package com.sap.isa.ipc.ui.jsp.uiclass;

import java.io.IOException;
import java.util.Hashtable;
import java.util.Stack;

import javax.servlet.ServletException;
import javax.servlet.jsp.PageContext;

import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.ipc.ui.jsp.beans.CharacteristicUIBean;
import com.sap.isa.ipc.ui.jsp.beans.GroupUIBean;
import com.sap.isa.ipc.ui.jsp.beans.InstanceUIBean;
import com.sap.isa.ipc.ui.jsp.uiclass.CharacteristicDescriptionUI.IncludeParams;
import com.sap.spc.remote.client.object.IPCException;
import com.sap.spc.remote.client.object.OriginalRequestParameterConstants;

/**
 * @author d035406
 *
 * TODO To change the template for this generated type comment go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
public class CharacteristicDescriptionUI extends IPCBaseUI {
	private static CharacteristicDescriptionUI characteristicDescriptionUI = new CharacteristicDescriptionUI();
	private static IsaLocation loc = IsaLocation.getInstance(CharacteristicUI.class.getName());
    
	public static void include(
		PageContext pageContext,
		String descriptionPage,
	    UIContext uiContext,
		CharacteristicUIBean cstic,
		UIArea uiArea,
		Hashtable customerParams) {
		IncludeParams includeParams =
		    characteristicDescriptionUI.new IncludeParams(
				uiContext,
				descriptionPage,
				cstic,
				uiArea,
				customerParams);
		Stack paramStack = IPCBaseUI.getParamStack(pageContext);
		paramStack.push(includeParams);
		try {
			pageContext.include(descriptionPage);
		} catch (ServletException e) {
			loc.error(IPCEX70 + descriptionPage, e);
			throw new RuntimeException(IPCEX15, e);
//TODO: Exceptionhandling            throw new IPCException(IPCEX15, null, e);
		} catch (IOException e) {
			loc.error(IPCEX70 + descriptionPage, e);
			throw new RuntimeException(IPCEX16, e);
//TODO: Exceptionhandling            throw new IPCException(IPCEX16, null, e);
		}
		paramStack.pop();
	}

	public static IncludeParams getIncludeParams(PageContext pageContext) {
		IncludeParams includeParams = null;
		try{
			includeParams = (IncludeParams)IPCBaseUI.getBaseIncludeParams(pageContext);
		}catch (ClassCastException e) {
			throw new IPCException(IPCEX14);
		}
		return includeParams;
	}
    
	public class IncludeParams extends IPCBaseUI.IncludeParams {
		private String descriptionPage;
		private CharacteristicUIBean cstic;
		private UIArea uiArea;
		
		public IncludeParams(UIContext uiContext,
							 String descriptionPage,
							 CharacteristicUIBean cstic,
							 UIArea uiArea,
							 Hashtable customerParams) {
								 super(uiContext, customerParams);
								 this.descriptionPage = descriptionPage;
								 this.cstic = cstic;
								 this.uiArea = uiArea;
							 }


		/**
		 * @return
		 */
		public CharacteristicUIBean getCstic() {
			return cstic;
		}

		/**
		 * @return
		 */
		public UIArea getUiArea() {
			return uiArea;
		}
	}
}
