package com.sap.isa.ipc.ui.jsp.uiclass;

import java.io.IOException;
import java.util.Hashtable;
import java.util.List;
import java.util.Stack;

import javax.servlet.ServletException;
import javax.servlet.jsp.PageContext;

import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.core.util.WebUtil;
import com.sap.isa.ipc.ui.jsp.action.InternalRequestParameterConstants;
import com.sap.isa.ipc.ui.jsp.beans.CharacteristicDeltaUIBean;
import com.sap.isa.ipc.ui.jsp.beans.ComparisonResultUIBean;
import com.sap.isa.ipc.ui.jsp.beans.GroupDeltaUIBean;
import com.sap.isa.ipc.ui.jsp.beans.InstanceDeltaUIBean;
import com.sap.spc.remote.client.object.IPCException;


public class ComparisonUI extends IPCBaseUI {
    
    //helper instance in order to create the parameter objects
    private static ComparisonUI comparisonUI = new ComparisonUI();
    private static IsaLocation loc = IsaLocation.getInstance(ComparisonUI.class.getName());
    
    public static void include(
        PageContext pageContext,
        String comparisionPage,
        UIContext ipcBaseUI,
        ComparisonResultUIBean resultBean,
        Hashtable customerParams) {

            IncludeParams includeParams =
                comparisonUI.new IncludeParams(
                    ipcBaseUI,
                    resultBean,
                    customerParams);
            Stack paramStack = IPCBaseUI.getParamStack(pageContext);
            paramStack.push(includeParams);
            try {
                pageContext.include(comparisionPage);
            } catch (ServletException e) {
                loc.error(IPCEX70 + comparisionPage, e.getRootCause());
                throw new RuntimeException(IPCEX57, e.getRootCause());
            } catch (IOException e) {
                loc.error(IPCEX70 + comparisionPage, e);
                throw new RuntimeException(IPCEX58, e);
            }
            paramStack.pop();
    }
    
    public static IncludeParams getIncludeParams(PageContext pageContext) {
        IncludeParams includeParams = null;
        try{
            includeParams = (IncludeParams)IPCBaseUI.getBaseIncludeParams(pageContext);
        }catch (ClassCastException e) {
            throw new IPCException(IPCEX59);
        }
        return includeParams;
    }
    
    /**
     * Returns the correct text for the show all/show different link<br>
     * I.e. if the showAllFlag is F at the moment the link will be labeled with "Show all" 
     * to switch to the "showAll-mode".<br>
     * If showAllFlag is T the link will be labled "Show differences" in order to switch to
     * "showOnlyDifferences-mode". 
     * @param pageContext
     * @return actionPath
     */
    public static String getShowAllLinkText(PageContext pageContext, UIContext uiContext){
        String linkText = "";
        if (uiContext.getPropertyAsBoolean(InternalRequestParameterConstants.COMPARISON_SHOW_ALL_ASSIGNED)){
            linkText = WebUtil.translate(pageContext,"ipc.compare.show.differences", null);
        }
        else {
            linkText = WebUtil.translate(pageContext,"ipc.compare.show.all", null);
        }
        return linkText;
    }

    /**
     * Returns the correct text for the set filtert link<br>
     * I.e. if the filter is set to "none" at the moment the link will be labeled with 
     * "set filter: critical" to switch to the filtered mode.<br>
     * If the filter is set to "critical" the link will be labled "set filter: none"
     * in order to switch to none-filtered mode. 
     * @param pageContext
     * @return actionPath
     */
    public static String getFilterLinkText(PageContext pageContext, UIContext uiContext){
        String linkText = "";
        if (uiContext.getPropertyAsBoolean(InternalRequestParameterConstants.COMPARISON_FILTER)){
            linkText = WebUtil.translate(pageContext,"ipc.compare.show.none", null);
        }
        else {
            linkText = WebUtil.translate(pageContext,"ipc.compare.show.critical", null);
        }
        return linkText;
    }

        
    /**
     * @return inverse showAll-Flag (used to switch this flag via the link)
     */
    public static String getInverseShowAllFlag(UIContext uiContext){
        String inverseFlag;  
        if (uiContext.getPropertyAsBoolean(InternalRequestParameterConstants.COMPARISON_SHOW_ALL_ASSIGNED)){
            inverseFlag = "F";
        }
        else {
            inverseFlag = "T";
        }
        return inverseFlag;   
    }


    /**
     * @return inverse filter-Flag (used to switch this flag via the link)
     */
    public static String getInverseFilterFlag(UIContext uiContext){
        String inverseFlag;  
        if (uiContext.getPropertyAsBoolean(InternalRequestParameterConstants.COMPARISON_FILTER)){
            inverseFlag = "F";
        }
        else {
            inverseFlag = "T";
        }
        return inverseFlag;   
    }
    
    public static String[] getTableInfo(ComparisonResultUIBean resultBean){
        int rows = 0;
        int size = resultBean.getLoadingMessages().size();
        if (size>0){
            size += 1; // if there are general loading messages we also have a separator line
        }
        rows += size;
        List insts = resultBean.getInstanceDeltas();
        size = insts.size();
        if (size>0){ 
            size = size + (size-1); // we add the number of separator lines (the no of separator lines
                                    // is inst.size()-1)
        }
        rows += size;
        for (int i=0; i<insts.size(); i++){
            InstanceDeltaUIBean instDelta = (InstanceDeltaUIBean) insts.get(i);
            size = instDelta.getLoadingMessages().size();
            rows += size;
            if (size>0){
                rows -= 1; // we subtract 1 because the first loading message is 
                           // shown in the same line as the instance name
            }
            List groups = instDelta.getGroups();
            rows += groups.size(); // add the number of groups
            for (int j=0; j<groups.size(); j++){
                GroupDeltaUIBean groupDelta = (GroupDeltaUIBean) groups.get(j);
                List cstics = groupDelta.getCharacteristics();
                for (int k=0; k<cstics.size(); k++){
                    CharacteristicDeltaUIBean csticDelta = (CharacteristicDeltaUIBean) cstics.get(k);
                    rows += csticDelta.getValues().size(); // add the number of values
                    size = csticDelta.getLoadingMessages().size();
                    rows += size; // add the number of loading messages
                    if (size>0){
                        rows -= 1; // we subtract 1 because the first loading message is 
                                   // shown in the same line as the cstic name
                    }
                }
            }
        }
        String[] tableInfo = new String[4];
        tableInfo[0] =  Integer.toString(rows); // rowNumber
        tableInfo[1] = "5"; // columnNumber
        tableInfo[2] = "1"; // firstRow
        tableInfo[3] = Integer.toString(rows); // lastRow              
        return tableInfo;
    }
    
    
    public class IncludeParams extends IPCBaseUI.IncludeParams {
        private ComparisonResultUIBean resultBean;

        /**
         * @param ipcBaseUI
         */
        IncludeParams(
            UIContext ipcBaseUI,
            ComparisonResultUIBean resultBean,
            Hashtable customerParams) {
            super(ipcBaseUI, customerParams);
            this.resultBean = resultBean;
        }
        

        /**
         * @return
         */
        public ComparisonResultUIBean getResultBean() {
            return resultBean;
        }
    }

}
