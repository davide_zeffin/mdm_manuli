/*
 * Created on 09.07.2004
 *
 * To change the template for this generated file go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
package com.sap.isa.ipc.ui.jsp.uiclass;

import java.io.IOException;
import java.util.Hashtable;
import java.util.List;
import java.util.Stack;

import javax.servlet.ServletException;
import javax.servlet.jsp.PageContext;

import com.sap.spc.remote.client.object.IPCException;
import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.ipc.ui.jsp.beans.GroupUIBean;
import com.sap.isa.ipc.ui.jsp.beans.InstanceUIBean;

/**
 * @author 
 *
 * To change the template for this generated type comment go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
public class InstancesUI extends IPCBaseUI {

    //helper instance in order to create the parameter objects
    private static InstancesUI instancesUI = new InstancesUI();
    private static IsaLocation loc = IsaLocation.getInstance(InstancesUI.class.getName());
    
    public static void include(
        PageContext pageContext,
        String instancePage,
        UIContext ipcBaseUI,
        List instances,
        InstanceUIBean instance,
        GroupUIBean group,
        Hashtable customerParams) {
            if (instancePage == null || instancePage.equals("")) { //$NON-NLS-1$
                loc.error(IPCEX21);
            }
            if (instances == null) {
                loc.error(IPCEX53);
            }
            if (instance == null) {
                loc.error(IPCEX22);
            }
            if (group == null) {
                loc.error(IPCEX23);
            }
            IncludeParams includeParams = instancesUI.new IncludeParams(ipcBaseUI, instances, instance, group, customerParams);
            Stack paramStack = IPCBaseUI.getParamStack(pageContext);
            paramStack.push(includeParams);
            try {
                pageContext.include(instancePage);
            } catch (ServletException e) {
                loc.error(IPCEX70 + instancePage, e.getRootCause());
                throw new RuntimeException(IPCEX9, e.getRootCause());
//TODO: Exceptionhandling                throw new IPCException(IPCEX9, null, e);
            } catch (IOException e) {
                loc.error(IPCEX70 + instancePage, e);
                throw new RuntimeException(IPCEX10, e);
//TODO: Exceptionhandling                throw new IPCException(IPCEX10, null, e);
            }
            paramStack.pop();
    }

    public static IncludeParams getIncludeParams(PageContext pageContext) {
        IncludeParams includeParams = null;
        try{
            includeParams = (IncludeParams)IPCBaseUI.getBaseIncludeParams(pageContext);
        }catch (ClassCastException e) {
            throw new IPCException(IPCEX11);
        }
        return includeParams;
    }
    
    public class IncludeParams extends IPCBaseUI.IncludeParams {
        private List instances;
        private InstanceUIBean instance;
        private GroupUIBean group;

        /**
         * @param ipcBaseUI
         */
        IncludeParams(
            UIContext ipcBaseUI,
            List instances,
            InstanceUIBean instance,
            GroupUIBean group,
            Hashtable customerParams) {
            super(ipcBaseUI, customerParams);
            this.instances = instances;
            this.instance = instance;
            this.group = group;
        }
        
        /**
         * @return
         */
        public GroupUIBean getGroup() {
            return group;
        }

        /**
         * @return
         */
        public InstanceUIBean getInstance() {
            return instance;
        }

        /**
         * @return
         */
        public List getInstances() {
            return instances;
        }

    }

}
