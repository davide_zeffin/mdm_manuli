/*
 * Created on 14.07.2004
 *
 * To change the template for this generated file go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
package com.sap.isa.ipc.ui.jsp.uiclass;

import java.io.IOException;
import java.util.Hashtable;
import java.util.Stack;

import javax.servlet.ServletException;
import javax.servlet.jsp.PageContext;

import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.ipc.ui.jsp.action.RequestParameterConstants;
import com.sap.isa.ipc.ui.jsp.beans.ConfigUIBean;
import com.sap.isa.ipc.ui.jsp.beans.GroupUIBean;
import com.sap.isa.ipc.ui.jsp.beans.InstanceUIBean;
import com.sap.spc.remote.client.object.ConfigurationSnapshot;
import com.sap.spc.remote.client.object.IPCException;
import com.sap.spc.remote.client.object.IPCItem;
import com.sap.spc.remote.client.object.LoadingStatus;

/**
 * @author 
 *
 * To change the template for this generated type comment go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
public class HeaderUI extends IPCBaseUI {
    
    //helper instance in order to create the parameter objects
    private static HeaderUI characteristicUI = new HeaderUI();
    private static IsaLocation loc = IsaLocation.getInstance(HeaderUI.class.getName());
    
    public static void include(
        PageContext pageContext,
        String headerPage,
        UIContext ipcBaseUI,
        ConfigUIBean configuration,
        InstanceUIBean instance,
        GroupUIBean group,
        Hashtable customerParams) {

            IncludeParams includeParams =
                characteristicUI.new IncludeParams(
                    ipcBaseUI,
                    configuration,
                    instance,
                    group,
                    customerParams);
            Stack paramStack = IPCBaseUI.getParamStack(pageContext);
            paramStack.push(includeParams);
            try {
                pageContext.include(headerPage);
            } catch (ServletException e) {
                loc.error(IPCEX70 + headerPage, e);
                throw new RuntimeException(IPCEX61, e);
//TODO:Exceptionhandling                throw new IPCException(IPCEX, null, e);
            } catch (IOException e) {
                loc.error(IPCEX70 + headerPage, e);
                throw new RuntimeException(IPCEX62, e);
//TODO:Exceptionhandling                throw new IPCException(IPCEX, null, e);
            }
            paramStack.pop();
    }
    
    public static IncludeParams getIncludeParams(PageContext pageContext) {
        IncludeParams includeParams = null;
        try{
            includeParams = (IncludeParams)IPCBaseUI.getBaseIncludeParams(pageContext);
        }catch (ClassCastException e) {
            throw new IPCException(IPCEX63);
        }
        return includeParams;
    }
    
    /**
     * Helper method.<br>
     * Determines whether the "compare to stored" link has to be displayed.<br>
     * We check the loading status of the configuration first. If the configuration 
     * has been loaded from a given external configuration (i.e. hasInitialConfiguration()
     * returns true) we check whether there is also an initial configuration available by
     * check getInitialConfiguration(). Only if there is really an initial configuration 
     * returned by the engine we display the link (otherwise it does not make sense).<br>
     * An initial configuration is only available if it has been set with function module
     * SPC_SET_INITIAL_CONFIGURATION.<br>
     * If the feature is disabled but there are loading messages we nevertheless show the 
     * link that user has the possibility to display the loading messages.
     * @param uic
     * @param config
     * @return
     */
    public static boolean displayCompareToStoredLink(UIContext uic, ConfigUIBean config){
        boolean displayCompareToStoredLink = false;
        LoadingStatus status = config.getBusinessObject().getLoadingStatus();
        if ( (!uic.getPropertyAsBoolean(RequestParameterConstants.ENABLE_COMPARISON) && !status.hasLoadingMessages())
           || (!uic.getPropertyAsBoolean(RequestParameterConstants.ENABLE_COMPARISON) && !uic.getPropertyAsBoolean(RequestParameterConstants.SHOW_LOADING_MESSAGES))) {
            
            // feature is disabled and there are no loading messages (or loading messages are disabled)
            return displayCompareToStoredLink;
        }                             
        boolean initialConfig = config.loadedFromInitialConfiguration();
        if (initialConfig){
            // we check whether an initial configuration is available
            IPCItem item = config.getBusinessObject().getRootInstance().getIpcItem();
            if (item != null){
                ConfigurationSnapshot initialSnap = item.getInitialConfiguration();
                if (initialSnap != null){
                    displayCompareToStoredLink = true;
                }
            }
            else {
                // there is no item and therefore no initial configuration -> return false
                displayCompareToStoredLink = false;
            }
        }
        return displayCompareToStoredLink;
    }
    
    public class IncludeParams extends IPCBaseUI.IncludeParams {
        private ConfigUIBean configuration;
        private InstanceUIBean instance;
        private GroupUIBean group;

        /**
         * @param ipcBaseUI
         */
        IncludeParams(
            UIContext ipcBaseUI,
            ConfigUIBean configuration,
            InstanceUIBean instance,
            GroupUIBean group,
            Hashtable customerParams) {
            super(ipcBaseUI, customerParams);
            this.configuration = configuration;
            this.instance = instance;
            this.group = group;
        }

        /**
         * @return
         */
        public ConfigUIBean getConfiguration() {
            return configuration;
        }

		/**
		 * @return
		 */
		public GroupUIBean getGroup() {
			return group;
		}

		/**
		 * @return
		 */
		public InstanceUIBean getInstance() {
			return instance;
		}

    }
    
}
