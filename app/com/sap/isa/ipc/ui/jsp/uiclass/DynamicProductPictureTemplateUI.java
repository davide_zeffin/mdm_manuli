/*
 * Created on 14.07.2004
 *
 * To change the template for this generated file go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
package com.sap.isa.ipc.ui.jsp.uiclass;

import java.io.IOException;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Stack;

import javax.servlet.ServletException;
import javax.servlet.jsp.PageContext;

import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.ipc.ui.jsp.beans.InstanceUIBean;
import com.sap.spc.remote.client.object.IPCException;
import com.sap.spc.remote.client.object.OriginalRequestParameterConstants;

/**
 * @author 
 *
 * To change the template for this generated type comment go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
public class DynamicProductPictureTemplateUI extends IPCBaseUI {
    
    //helper instance in order to create the parameter objects
    private static DynamicProductPictureTemplateUI characteristicUI = new DynamicProductPictureTemplateUI();
    private static IsaLocation loc = IsaLocation.getInstance(DynamicProductPictureTemplateUI.class.getName());
    
    public static void include(
        PageContext pageContext,
        String templatePage,
        UIContext ipcBaseUI,
        InstanceUIBean instance,
        Hashtable customerParams) {

            IncludeParams includeParams =
                characteristicUI.new IncludeParams(
                    ipcBaseUI,
                    templatePage,
                    instance,
                    customerParams);
            Stack paramStack = IPCBaseUI.getParamStack(pageContext);
            paramStack.push(includeParams);
            try {
                pageContext.include(templatePage);
            } catch (ServletException e) {
                loc.error(IPCEX70 + templatePage, e);
                throw new RuntimeException(IPCEX41, e);
//TODO:Exceptionhandling                throw new IPCException(IPCEX, null, e);
            } catch (IOException e) {
                loc.error(IPCEX70 + templatePage, e);
                throw new RuntimeException(templatePage, e);
//TODO:Exceptionhandling                throw new IPCException(IPCEX, null, e);
            }
            paramStack.pop();
    }
    
    public static IncludeParams getIncludeParams(PageContext pageContext) {
        IncludeParams includeParams = null;
        try{
            includeParams = (IncludeParams)IPCBaseUI.getBaseIncludeParams(pageContext);
        }catch (ClassCastException e) {
            throw new IPCException(IPCEX43);
        }
        return includeParams;
    }
    
    public static boolean instanceHasDynamicProductPicture(UIContext uiContext, InstanceUIBean instance){
    	boolean hasDynamic = false;
    	// check if feature is enabled
		if (uiContext.getPropertyAsBoolean(OriginalRequestParameterConstants.SHOW_DYNAMIC_PRODUCT_PICTURE)){
			// check wether product is in the list of dynamic products
			HashMap dynProds = uiContext.getDynamicProducts();
			if (dynProds != null && instance != null){
				hasDynamic = dynProds.containsKey(instance.getName());
			}
		}
    	
    	return hasDynamic;
    	
    }
    
    public class IncludeParams extends IPCBaseUI.IncludeParams {
        private String templatePage;
        private InstanceUIBean instance;

        /**
         * @param ipcBaseUI
         */
        IncludeParams(
            UIContext ipcBaseUI,
            String templatePage,
            InstanceUIBean instance,
            Hashtable customerParams) {
            super(ipcBaseUI, customerParams);
            this.templatePage = templatePage;
            this.instance = instance;
        }
        


		/**
		 * @return
		 */
		public InstanceUIBean getInstance() {
			return instance;
		}

    }
    
    
    
}
