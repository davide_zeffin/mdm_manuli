package com.sap.isa.ipc.ui.jsp.uiclass;

import java.io.IOException;
import java.util.Hashtable;
import java.util.Stack;

import javax.servlet.ServletException;
import javax.servlet.jsp.PageContext;

import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.ipc.ui.jsp.beans.InstanceDeltaUIBean;
import com.sap.spc.remote.client.object.IPCException;

public class ComparisonTblInstanceUI extends IPCBaseUI {
    
    //helper instance in order to create the parameter objects
    private static ComparisonTblInstanceUI comparisonTblInstanceUI = new ComparisonTblInstanceUI();
    private static IsaLocation loc = IsaLocation.getInstance(ComparisonTblInstanceUI.class.getName());
    
    public static void include(
        PageContext pageContext,
        String comparisionTblInstancePage,
        UIContext ipcBaseUI,
        InstanceDeltaUIBean instanceBean,
        Hashtable customerParams) {

            IncludeParams includeParams =
                comparisonTblInstanceUI.new IncludeParams(
                    ipcBaseUI,
                    instanceBean,
                    customerParams);
            Stack paramStack = IPCBaseUI.getParamStack(pageContext);
            paramStack.push(includeParams);
            try {
                pageContext.include(comparisionTblInstancePage);
            } catch (ServletException e) {
                loc.error(IPCEX70 + comparisionTblInstancePage, e.getRootCause());
                throw new RuntimeException(IPCEX57, e.getRootCause());
            } catch (IOException e) {
                loc.error(IPCEX70 + comparisionTblInstancePage, e);
                throw new RuntimeException(IPCEX58, e);
            }
            paramStack.pop();
    }
    
    public static IncludeParams getIncludeParams(PageContext pageContext) {
        IncludeParams includeParams = null;
        try{
            includeParams = (IncludeParams)IPCBaseUI.getBaseIncludeParams(pageContext);
        }catch (ClassCastException e) {
            throw new IPCException(IPCEX59);
        }
        return includeParams;
    }
    
    public class IncludeParams extends IPCBaseUI.IncludeParams {
        private InstanceDeltaUIBean instanceBean; 

        /**
         * @param ipcBaseUI
         */
        IncludeParams(
            UIContext ipcBaseUI,
            InstanceDeltaUIBean instanceBean,
            Hashtable customerParams) {
            super(ipcBaseUI, customerParams);
            this.instanceBean = instanceBean;
        }
       
        /**
         * @return
         */
        public InstanceDeltaUIBean getInstanceDeltaBean() {
            return instanceBean;
        }
    }
}