/*
 * UIElement:
 * 
 */
package com.sap.isa.ipc.ui.jsp.uiclass;

public class UIElement {
    
    private String name;
    private UIContext uiContext;
    private String shortcutKey;
    private static final String ACCESSKEY_PREFIX = "accesskey."; //$NON-NLS-1$

    public UIElement(String name, UIContext uiContext) {
        this.name = name;
        this.uiContext = uiContext;
        String keyForAccesskey = ACCESSKEY_PREFIX + name;
        this.shortcutKey = uiContext.getPropertyAsString(keyForAccesskey, ""); //$NON-NLS-1$
    }

    public String getShortcutKey() {
        return this.shortcutKey;
    }

    public String getName() {
        return this.name;
    }

    public String toString() {
        String returnString = new String();
        returnString += UIElement.class.getName();
        returnString += "\nname: " + this.getName(); //$NON-NLS-1$
        returnString += "\nkey: " + this.getShortcutKey(); //$NON-NLS-1$
        return returnString;
    }

    public static void main(String[] args) {
        UIContext myContext = new UIContext();
        UIElement testElement = new UIElement("testelement", myContext); //$NON-NLS-1$
        System.out.println(testElement);
    }
}