/*
 * Created on 21.01.2005
 *
 */
package com.sap.isa.ipc.ui.jsp.uiclass;

import java.io.IOException;
import java.util.Hashtable;
import java.util.Stack;

import javax.servlet.ServletException;
import javax.servlet.jsp.PageContext;

import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.ipc.ui.jsp.container.ConditionUIBeanSet;
import com.sap.spc.remote.client.object.IPCException;

/**
 * @author 
 *
 */
public class ConditionsUI extends IPCBaseUI {
	
	private static ConditionsUI conditionsUI = new ConditionsUI();
	private static IsaLocation loc = IsaLocation.getInstance(ConditionsUI.class.getName());


	public static void include(
		PageContext pageContext,
		String conditionsPage,
		UIContext ipcBaseUI,
		ConditionUIBeanSet conditions,
		Hashtable customerParams) {

			IncludeParams includeParams =
				conditionsUI.new IncludeParams(
					ipcBaseUI,
					conditions,
					customerParams);
			Stack paramStack = IPCBaseUI.getParamStack(pageContext);
			paramStack.push(includeParams);
			try {
				pageContext.include(conditionsPage);
			} catch (ServletException e) {
				loc.error(IPCEX70 + conditionsPage, e.getRootCause());
				throw new RuntimeException(IPCEX61, e.getRootCause());
//TODO:Exceptionhandling                throw new IPCException(IPCEX, null, e);
			} catch (IOException e) {
				loc.error(IPCEX70 + conditionsPage, e);
				throw new RuntimeException(IPCEX62, e);
//TODO:Exceptionhandling                throw new IPCException(IPCEX, null, e);
			}
			paramStack.pop();
	}
    
	public static IncludeParams getIncludeParams(PageContext pageContext) {
		IncludeParams includeParams = null;
		try{
			includeParams = (IncludeParams)IPCBaseUI.getBaseIncludeParams(pageContext);
		}catch (ClassCastException e) {
			throw new IPCException(IPCEX63);
		}
		return includeParams;
	}
    
	public class IncludeParams extends IPCBaseUI.IncludeParams {
		private ConditionUIBeanSet conditions;

		/**
		 * @param ipcBaseUI
		 */
		IncludeParams(
			UIContext ipcBaseUI,
			ConditionUIBeanSet conditions,
			Hashtable customerParams) {
			super(ipcBaseUI, customerParams);
			this.conditions = conditions;
		}


		/**
		 * @return
		 */
		public ConditionUIBeanSet getConditions() {
			return conditions;
		}

	}

}
