/*
 * Created on 04.12.2004
 *
 * To change the template for this generated file go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
package com.sap.isa.ipc.ui.jsp.beans;

import com.sap.isa.ipc.ui.jsp.uiclass.UIContext;
import com.sap.spc.remote.client.object.ConflictHandlingShortcut;

/**
 * @author 
 *
 * To change the template for this generated type comment go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
public class ConflictHandlingShortcutUIBean {
	protected ConflictHandlingShortcut conflictHandlingshortcutBO;
	protected UIContext uiContext;
	
	protected ConflictHandlingShortcutUIBean(ConflictHandlingShortcut conflictHandlingShortcut, UIContext uiContext) {
		this.conflictHandlingshortcutBO = conflictHandlingShortcut;
		this.uiContext = uiContext;
	}
	
	/**
	 * In order to give customers the possibility access business objects in JSP pages.
	 * This is useful especially for customer extension data of business objects.
	 * @return
	 */
	public ConflictHandlingShortcut getBusinessObject() {
		return conflictHandlingshortcutBO;
	}


	/**
	 * @return
	 */
	public String getConflictId() {
		return conflictHandlingshortcutBO.getConflictId();
	}

	/**
	 * @return
	 */
	public String getConflictParticipantId() {
		return conflictHandlingshortcutBO.getConflictParticipantId();
	}

	/**
	 * @return
	 */
	public String getExplanation() {
		return conflictHandlingshortcutBO.getExplanation();
	}

	/**
	 * @return
	 */
	public String getKey() {
		return conflictHandlingshortcutBO.getKey();
	}

	/**
	 * @return
	 */
	public String getObservable() {
		return conflictHandlingshortcutBO.getObservable();
	}

	/**
	 * @return
	 */
	public int getType() {
		return conflictHandlingshortcutBO.getType();
	}

	/**
	 * @return
	 */
	public String getValue() {
		return conflictHandlingshortcutBO.getValue();
	}

	/**
	 * @return
	 */
	public String getValueToDrop() {
		return conflictHandlingshortcutBO.getValueToDrop();
	}

	/**
	 * @return
	 */
	public String getValueToKeep() {
		return conflictHandlingshortcutBO.getValueToKeep();
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	public int hashCode() {
		return conflictHandlingshortcutBO.hashCode();
	}
    
    /* (non-Javadoc)
     * @see java.lang.Object#equals(java.lang.Object)
     */
    public boolean equals(Object o) {
        return conflictHandlingshortcutBO.equals(o);
    }

	/**
	 * @return
	 */
	public boolean isClosed() {
		return conflictHandlingshortcutBO.isClosed();
	}

	/**
	 * @return
	 */
	public boolean isOfTypeDrop() {
		return conflictHandlingshortcutBO.isOfTypeDrop();
	}

	/**
	 * @param commandName
	 * @return
	 */
	public boolean isRelevant(String commandName) {
		return conflictHandlingshortcutBO.isRelevant(commandName);
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	public String toString() {
		return conflictHandlingshortcutBO.toString();
	}

}
