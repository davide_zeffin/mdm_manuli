/*
 * Created on 18.01.2005
 *
 */
package com.sap.isa.ipc.ui.jsp.beans;

import com.sap.isa.ipc.ui.jsp.uiclass.UIContext;
import com.sap.spc.remote.client.object.IPCPricingCondition;

/**
 * @author 
 *
 */
public class ConditionUIBean implements Comparable {
	protected IPCPricingCondition conditionBo;
	protected UIContext uiContext;

	/**
	 * 
	 */
	protected ConditionUIBean(IPCPricingCondition conditionBo, UIContext uiContext) {
		this.conditionBo = conditionBo;
		this.uiContext = uiContext;
	}
	
	/**
	 * In order to give customers the possibility access business objects in JSP pages.
	 * This is useful especially for customer extension data of business objects.
	 * @return
	 */
	public IPCPricingCondition getBusinessObject() {
		return conditionBo;
	}
	
	/**
	 * 
	 */
	public String getDescription() {
		if (uiContext.getShowLanguageDependentNames()) {
			String description;
			if (conditionBo.isVariantCondition()){
				description = conditionBo.getVariantConditionDescription();
				if (description == null || description.equals("")) {
					//no variant condition description available,
					//then better show "Variant" than ""
					description = conditionBo.getDescription();
				}
			}else {
				description = conditionBo.getDescription();
			} 
			if (description != null) {
				return description;
			}else {
				return "";
			} 
		}else {
			String typeName;
			if(conditionBo.isVariantCondition()) {
				typeName = conditionBo.getVariantConditionKey();
			}else {
				typeName = conditionBo.getConditionTypeName();
			}
			if (typeName != null) {
				return typeName; 
			}else {
				return "";
			}
		}
	}
	/**
	 * 
	 */
	public String getValue() {
		String internalConditionValue = conditionBo.getConditionValue();
		if (uiContext.getShowLanguageDependentNames()) {
			String externalConditionValue = conditionBo.getExternalConditionValue(uiContext.getDecimalSeparator(), uiContext.getGroupingSeparator());
			if (externalConditionValue != null && !externalConditionValue.equals("")) {
				return externalConditionValue;
			}else {
				return internalConditionValue;
			}
		}else {
			return internalConditionValue;
		}
	}
	/**
	 * @return
	 */
	public String getCurrency() {
		return conditionBo.getConditionCurrency();
	}
	
	public String getDocumentCurrency() {
		return conditionBo.getDocumentCurrency();
	}
	
	/* (non-Javadoc)
	 * @see java.lang.Comparable#compareTo(java.lang.Object)
	 */
	public int compareTo(Object o) {
		ConditionUIBean co = (ConditionUIBean)o;
		String coKey = co.getStepNo() + co.getCounter();
		String key = this.getStepNo() + this.getCounter();
		// TODO Auto-generated method stub
		return key.compareTo(coKey);
	}
	/**
	 * 
	 */
	protected String getCounter() {
		return this.conditionBo.getCounter();
		
	}
	/**
	 * 
	 */
	protected String getStepNo() {
		return this.conditionBo.getStepNo();
		
	}

}
