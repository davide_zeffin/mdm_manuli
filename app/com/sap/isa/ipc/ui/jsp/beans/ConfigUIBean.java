/*
 * Created on 06.08.2004
 *
 * To change the template for this generated file go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
package com.sap.isa.ipc.ui.jsp.beans;

import java.util.Hashtable;
import java.util.Iterator;
import java.util.List;
import java.util.TreeSet;

import com.sap.isa.ipc.ui.jsp.action.visitor.IVisitableObject;
import com.sap.isa.ipc.ui.jsp.action.visitor.IVisitor;
import com.sap.isa.ipc.ui.jsp.container.ConditionUIBeanSet;
import com.sap.isa.ipc.ui.jsp.uiclass.UIContext;
import com.sap.spc.remote.client.object.Configuration;
import com.sap.spc.remote.client.object.IPCException;

/**
 * @author 
 *
 * To change the template for this generated type comment go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
public class ConfigUIBean implements IVisitableObject{

	protected Configuration configBO;
	protected UIContext uiContext;
	protected InstanceUIBean rootInstance;
	protected List instances;
	protected Hashtable instanceUIBeansById;
	protected Boolean assignedValues;
	protected ConditionUIBeanSet conditions;

    protected ConfigUIBean(Configuration config, UIContext uiContext) {
        this.configBO = config;
        this.uiContext = uiContext;
    }

    public StatusImage getStatusImage() {
        if (!configBO.isConsistent()) {
            return UIBeanFactory.getUIBeanFactory().newStatusImage(
                "ipc.config.inconsistent",
                "ipc/mimes/images/sys/ipc_incomp_and_inconsist.gif");
        } else {
            if (configBO.isComplete()) {
                return UIBeanFactory.getUIBeanFactory().newStatusImage(
                    "ipc.config.complete",
                    "ipc/mimes/images/sys/ipc_completeness.gif");
            } else {
                return UIBeanFactory.getUIBeanFactory().newStatusImage(
                    "ipc.config.incomplete",
                    "ipc/mimes/images/sys/ipc_incompleteness.gif");
            }
        }
    }

    public String getStatusText() {
        if (!configBO.isConsistent()) {
            return "ipc.config.inconsistent";
        } else {
            if (configBO.isComplete()) {
                return "ipc.config.complete";
            } else {
                return "ipc.config.incomplete";
            }
        }
    }

    public static void main(String[] args) {
    }
    /**
     * @return
     */
    public InstanceUIBean getRootInstance() {
        return rootInstance;
    }

    /**
     * @param bean
     */
    public void setRootInstance(InstanceUIBean bean) {
        rootInstance = bean;
    }

	/**
	 * In order to give customers the possibility access business objects in JSP pages.
	 * This is useful especially for customer extension data of business objects.
	 * @return
	 */
    public Configuration getBusinessObject() {
        return configBO;
    }

    /**
     * @return
     */
    public List getInstances() {
        return instances;
    }
    
    public InstanceUIBean getInstance(String id){
    	return (InstanceUIBean) instanceUIBeansById.get(id);
    }

    /**
     * @param list
     */
    public void setInstances(List list) {
        instances = list;
    }

	public void setInstancesByID(Hashtable instById){
		instanceUIBeansById = instById;
	}

    /**
     * @return
     * @throws IPCException
     */
    public boolean isComplete() throws IPCException {
        return configBO.isComplete();
    }

    /**
     * @return
     * @throws IPCException
     */
    public boolean isConsistent() throws IPCException {
        return configBO.isConsistent();
    }

	/**
	 * Checks if at least one instance of the configuration has values assigned to
	 * its characteristics.
	 * @return
	 */
	public boolean hasAssignedValues() {
		if (assignedValues == null) {
			for (Iterator it = instances.iterator(); it.hasNext(); ) {
				InstanceUIBean instance = (InstanceUIBean)it.next();
				if (instance.hasAssignedValues()) {
					assignedValues = Boolean.TRUE;
					return true;
				}
			}
			assignedValues = Boolean.FALSE;
			return false;
		}
		return assignedValues.booleanValue();
	}


	/**
	 * @param conditionUIBean
	 */
	public void addCondition(ConditionUIBean conditionUIBean) {
		if (conditions == null) {
			conditions = new ConditionUIBeanSet();
		}
		conditions.add(conditionUIBean);
	}

	/**
	 * @return
	 */
	public ConditionUIBeanSet getConditions() {
		if (conditions != null) {
			return conditions;
		}else {
			return ConditionUIBeanSet.C_EMPTY;
		}
		
	}

	/**
	 * @param set
	 */
	public void addConditions(TreeSet set) {
		if (conditions == null) {
			conditions = new ConditionUIBeanSet();
		}
		for (Iterator it = set.iterator(); it.hasNext(); ) {
			ConditionUIBean condition = (ConditionUIBean)it.next();
			conditions.add(condition); 
		}
	}
    
    public boolean loadedFromInitialConfiguration(){
        return configBO.getLoadingStatus().loadedFromInitialConfiguration();
    }

	/* (non-Javadoc)
	 * @see com.sap.isa.ipc.ui.jsp.action.visitor.IVisitableObject#accept(com.sap.isa.ipc.ui.jsp.action.visitor.IVisitor)
	 */
	public void accept(IVisitor visitor) {
		visitor.visit(this);
	}
}
