package com.sap.isa.ipc.ui.jsp.beans;

import javax.servlet.jsp.PageContext;

import com.sap.isa.core.util.JspUtil;
import com.sap.isa.core.util.WebUtil;
import com.sap.isa.ipc.ui.jsp.action.DisplayComparisonAction;
import com.sap.isa.ipc.ui.jsp.uiclass.UIContext;
import com.sap.isa.ipc.ui.jsp.util.FormatValidator;
import com.sap.isa.ipc.ui.jsp.util.StringUtil;
import com.sap.spc.remote.client.object.Characteristic;
import com.sap.spc.remote.client.object.CharacteristicValue;
import com.sap.spc.remote.client.object.ValueDelta;

/**
 * Wrapper around the ValueDelta, that is specialized for HTML usage.
 */
public class ValueDeltaUIBean extends ValueUIBean {

    public static final String CHANGED = "C";
    public static final String DELETED = "D";
    public static final String INSERTED = "I";
    public static final String NODELTA = "N"; 
    private static final String AUTHOR_USER = "";        // author for user-set values can be represented
                                                         // by an empty string... (if it is returned by FM)
    private static final String AUTHOR_USER_SPACE = " "; // ... or by space (if it is taken from extConfig)      
    
    private boolean change = true;   // indicates whether this delta represents a real change 
                                     // usually true, only false if this ValueDeltaUIBean is 
                                     // used for an unchanged value of a multi-valued characteristic
    
    private String internalName = ""; // This is used if this deltaUIBean has been created for
                                      // a cstic of an deleted instance. In this case there is no
                                      // business object.
    private String internalLName = "";
    private String internalName2 = "";
    private String internalLName2 = "";
                                      
    private boolean deltaForDeletedInstance = false; // Indicates whether this ValueDeltaUIBean has been
                                                     // created for a deleted instance. If this is the case
                                                     // there would be no delta business object so the 
                                                     // language (in)dependent name has to be taken from 
                                                     // the "internalName" attribute.                                                                                       
    private ValueDelta deltaBO;
    private ValueDelta deltaBO2;
    private CharacteristicValue valueBO2;
    private Characteristic csticBO; // Can be set to provide additional information for the value if no
                                    // valueBO exists. (e.g. deleted additional value -> did not belong to 
                                    // the domain, so no valueBO1 exists and also no valueBO2 because it has
                                    // been deleted.
                                    // Has to be set explicitely via set-method.
    
    private boolean condensed;

    /**
     * Creates a ValueDeltaUIBean that represents an unchanged value either of a multi-valued 
     * characteristic or in case showAll=T.
     * @param valueBO business object of the value
     * @param uiContext
     */
    protected ValueDeltaUIBean(CharacteristicValue valueBO,  UIContext uiContext) {
        super(valueBO, uiContext);
        this.change = false;
        this.condensed = false;
    }

    /**
     * Create a ValueDeltaUIBean for a deleted instance (in this case no BO is available).
     * @param valueName internal name of the value
     * @param internalLName internal language dependent name of the value (pass null or empty string if not available) 
     * @param uiContext
     */
    protected ValueDeltaUIBean(String valueName, String internalLName,  UIContext uiContext) {
        super(null, uiContext);
        this.internalName = valueName;
        this.internalLName = internalLName; 
        // this is a ValueDeltaUIBean for a deleted instance
        this.deltaForDeletedInstance = true;
        this.condensed = false;
    }

    /**
     * Create a ValueDeltaUIBean based on one delta business object.
     * @param deltaBO business object of the delta
     * @param valueBO business object of the value
     * @param uiContext
     */
    protected ValueDeltaUIBean(ValueDelta deltaBO1, CharacteristicValue valueBO, UIContext uiContext) {
        super(valueBO, uiContext);
        if (valueBO == null){
            // Special case: no valueBO has been passed.
            // This happens if this valueDelta belongs to a characteristic with no domain but with additional values
            // allowed. If the value is deleted then no BO for the old one is existing because it is not
            // part of the domain of the characteristic.
            // Solution: We take the internal value name of the deltaBO and set it as "internalName".
            // If this ValueDeltaUIBean will be asked for its "name1" the internal name will be returned.
            this.internalName = deltaBO1.getValue();
        }        
        this.deltaBO = deltaBO1;
        this.condensed = false;
    }

    /**
     * Create a ValueDeltaUIBean based on two delta business objects.<br>
     * This could be used to condense a delta information in case of a single-valued characteristic.
     * Deltas for single-valued characteristic could be condensed. That means
     * if a single-valued characteristic has two deltas. One is a "delete"-delta,
     * the other one an "insert"-delta. This information could be condensed to a
     * "change"-delta.
     * So, instead of stating: "Value A has been deleted, then value B has been 
     * inserted". It will state: "The value has been changed from A to B."
     * @param deltaBO business object of the delta
     * @param deltaBO2 business object of the second delta that should be represented by this bean 
     * (only possible for single-valued characteristics) 
     * @param valueBO business object of the value
     * @param valueBO2 business object of the second value that should be represented by this bean
     * @param uiContext
     */
    protected ValueDeltaUIBean(ValueDelta deltaBO, 
        ValueDelta deltaBO2,
        CharacteristicValue valueBO,
        CharacteristicValue valueBO2,
        UIContext uiContext) {
        
        super(valueBO, uiContext);
        if (valueBO == null){
            // Special case: no valueBO has been passed.
            // This happens if this valueDelta belongs to a characteristic with no domain but with additional values
            // allowed. If the value is changed then no BO for the old one is existing because it is not
            // part of the domain of the characteristic.
            // Solution: We take the internal value name of the deltaBO and set it as "internalName".
            // If this ValueDeltaUIBean will be asked for its "name1" the internal name will be returned.
            this.internalName = deltaBO.getValue();
        }
        this.deltaBO = deltaBO;
        this.deltaBO2 = deltaBO2;
        this.valueBO2 = valueBO2;
        if (valueBO2 == null){
            this.internalName2 = deltaBO2.getValue();
        }
        this.condensed = true;
    }


    /**
     * @return language-independent value of the delta
     */
    public String getValue(){
        return deltaBO.getValue();
    }
    
    /**
     * If this value is a condensed delta this method will return the second delta value that
     * is underlying this ValueDeltaUIBean.
     * If it is not a condensed delta it returns null.
     * @return language-independent value of the second delta
     */
    public String getValue2(){
        return deltaBO2.getValue();
    }
    
    /**
     * @return resource key for the remark of this delta.
     */
    public String getRemark(){
        String remark;
        if (getType().equals(ValueDeltaUIBean.CHANGED)) {
            if (isSetByUser(getAuthor())) {
                remark = "ipc.compare.val.changed.user";   
            }
            else {
                remark = "ipc.compare.val.changed.system"; 
            }
        }
        else if (getType().equals(ValueDeltaUIBean.DELETED)) {
            remark = "ipc.compare.val.deleted";
        }
        else if (getType().equals(ValueDeltaUIBean.INSERTED)) {
            if (isSetByUser(getAuthor())) {
                remark = "ipc.compare.val.set.user";   
            }
            else {
                remark = "ipc.compare.val.set.system"; 
            }   
        }
        else {
            remark = "";
        }
        return remark;
    }
    
    /**
     * The differnt types of deltas a represented by different status icons:
     * <ul>
     * <li>green LED: no delta
     * <li>yellow LED: change (delete, insert, change)
     * </ul>
     * @return path to a status icon for this delta
     */
    public StatusImage getStatusImage(){
        String sourceFile = "ipc/mimes/images/sys/w_s_ledy.gif";
        String statusDescription = "ipc.compare.tt.icon.change";
        
        if (getType().equals(NODELTA)){
            sourceFile = "ipc/mimes/images/sys/w_s_ledg.gif";
            statusDescription = "ipc.compare.tt.icon.nochange";
        }
      
        return UIBeanFactory.getUIBeanFactory().newStatusImage(statusDescription, sourceFile);
    }
    
    /**
     * Returns the type of value delta.<br>
     * This could be different from the type of the underlying business object e.g. if
     * the delta information has been condensed.
     * The following types can be returned:
     * <ul>
     * <li><code>D</code>: deletedelta
     * <li><code>I</code>: insert delta
     * <li><code>C</code>: changed delta (condensed)
     * <li><code>N</code>: no delta, this UIBean is for an unchanged value of a multi-valued characteristic
     * </ul>
     * @return type of value delta
     */
    public String getType(){
        String type;
        if (deltaForDeletedInstance){
            // it is a "deleted" value delta for an deleted instance (no BO available)
            type = DELETED;
        }
        else if (condensed){
            type = CHANGED;
        }
        else if(!change){
            type = NODELTA;
        }
        else {
            if (deltaBO.getType().equals(ValueDelta.DELETED)){
                type = DELETED;
            }
            else {
                type = INSERTED;
            }
        }
        return type;
    }
    
    /**
     * @return delta business object
     */
    public ValueDelta getDeltaBusinessObject(){
        return deltaBO;
    }
    
    /**
     * If this value is a condensed delta this method will return the second delta
     * business object that is underlying this ValueDeltaUIBean.
     * If it is not a condensed delta it returns null.
     * @return delta business object 2
     */
    public ValueDelta getDeltaBusinessObject2(){
        return deltaBO2;
    }

    /**
     * If this value is a condensed delta this method will return the second 
     * business object that is underlying this ValueDeltaUIBean.
     * If it is not a condensed delta it returns null.
     * @return business object 2
     */
    public CharacteristicValue getBusinessObject2() {
        return valueBO2;
    }


    /**
     * Deltas for single-valued characteristics could be condensed. That means
     * if a single-valued characteristic has two deltas. One is a "delete"-delta,
     * the other one an "insert"-delta. This information could be condensed to a
     * "change"-delta.
     * So, instead of stating: "Value A has been deleted, then value B has been 
     * inserted". It will state: "The value has been changed from A to B."
     * @return flag whether the delta is condensed
     */
    public boolean isCondensed() {
        return condensed;
    }

    /**
     * 
     * @param set to true if this is a condensed value
     */
    public void setCondensed(boolean b) {
        condensed = b;
    }
    
    /**
     * Returns the author of this value delta.<br>
     * If this is a condensed delta it returns the author of
     * deltaBO2.
     * If this is this UIBean is for an unchanged value, the author information
     * empty (no change, so no author). 
     * @return author of value delta
     */
    public String getAuthor(){
        String author;
        if (condensed){
            author = deltaBO2.getAuthor();
        }
        else if (change == false){
            // this UIBean is for an unchanged value, the author information is empty 
            author = "";
        }
        else {
            author = deltaBO.getAuthor();
        }
        return author;
    }    

    /**
     * Which name would be returned depends on the type of the delta.
     * <ul>
     * <li>deleted: name of BO1 (if BO1 is available, otherwise it returns the internalName)
     * <li>inserted: static text "not selected"
     * <li>changed: name of BO1 (if BO1 is available, otherwise it returns the internalName)
     * <li>noDelta: name of BO1
     * </ul>
     * Remark:<br>
     * It could happen that this bean does not have an associated BO value if
     * this bean has been created for a "deleted" instance.    
     * In this case it returns the internal name (which has to be set before).
     * @return the language independent name of the characteristic value
     */
    public String getName(PageContext pageContext) {
        String name;
        if (getType().equals(INSERTED)){
            if (pageContext != null){
                name = WebUtil.translate(pageContext, "ipc.compare.val.notselected", null);
            }
            else {
                name = "ipc.compare.val.notselected";
            }
        }
        else if (getType().equals(DELETED) || getType().equals(CHANGED)){
            if (super.getBusinessObject() == null){
                // no BO -> return the internal name
                name = JspUtil.replaceSpecialCharacters(internalName);
            }
            else{
                name = super.getName();
            }
        }
        else {
            name = super.getName();
        }
        return name;
    }

    /**
     * Which name would be returned depends on the type of the delta.
     * <ul>
     * <li>deleted: static text "not selected"
     * <li>inserted: name of BO1
     * <li>changed: name of BO2
     * <li>noDelta: name of BO1  
     * </ul>
     * @return the language independent name of the characteristic value 2
     */
    public String getName2(PageContext pageContext) {
        String name;
        if(getType().equals(DELETED)){
            if (pageContext != null){
                name = WebUtil.translate(pageContext, "ipc.compare.val.notselected", null);
            }
            else {
                name = "ipc.compare.val.notselected";
            }
        }
        else if (getType().equals(NODELTA)){
            name = super.getName();
        }
        else if (getType().equals(INSERTED)){
            name = super.getName();
        }
        else{
            name = valueBO2.getName();
        }
        return name;
    }


    /**
     * Which name would be returned depends on the type of the delta.
     * <ul>
     * <li>deleted: name of BO1 (if BO1 is available, otherwise it returns the internalName)
     * <li>inserted: static text "not selected"
     * <li>changed: name of BO1 (if BO1 is available, otherwise it returns the internalName)
     * <li>noDelta: name of BO1 (if BO1 is available, otherwise it returns the internalName)
     * </ul>
     * Remark:<br>
     * It could happen that this bean does not have an associated value BO if
     * this bean has been created for a "deleted" instance.    
     * In this case it returns the internal name (which has to be set before).
     * @return the language dependent name of the characteristic value
     */
    public String getLanguageDependentName(PageContext pageContext) {
        String lName;
        if (getType().equals(INSERTED)){
            if (pageContext != null){
                lName = WebUtil.translate(pageContext, "ipc.compare.val.notselected", null);
            }
            else {
                lName = "ipc.compare.val.notselected";
            }
        }
        else {
            if (super.getBusinessObject() == null){
                // no BO -> return the internal lName (if available, otherwise the internal name)
                if ((internalLName != null) && (!internalLName.equals(""))){
                    lName = JspUtil.replaceSpecialCharacters(internalLName);
                }
                else {
                    // Last chance:
                    // If the value belongs to a numerical or a date cstic we try to format it 
                    // otherwise we return the internal name.
                    lName = formatInternalValue(internalName);
                }
            }
            else{
                lName = super.getLanguageDependentName();
            }
        }
        return lName;
    }

    /**
     * If the value belongs to a numerical or a date cstic we try to format it 
     * otherwise we return the internal name.
     * @param internalName the internal value that should be formatted (if possible)
     */
    private String formatInternalValue(String internalValue) {
        String lName;
        boolean formatNumericalValue = isNumerical();
        boolean formatDateValue = isDate();
        if (formatNumericalValue){
            // format the value
            String formatted = FormatValidator.convertNumericValueToLocalized(csticBO, internalValue, uiContext.getLocale());
            lName = JspUtil.replaceSpecialCharacters(formatted);
        }
        else if (formatDateValue){
            // convert the value from exponential to YYYYMMDD format
            String converted = DisplayComparisonAction.convertDateValue(internalValue);
            // format the value
            String formatted = FormatValidator.convertDateValueToLocalized(csticBO, converted, uiContext.getLocale());
            lName = JspUtil.replaceSpecialCharacters(formatted);
        }
        else {
            lName = JspUtil.replaceSpecialCharacters(internalValue);
        }
        return lName;
    }

    /**
     * @return true if the value belongs to a numerical cstic
     */
    private boolean isNumerical() {
        boolean numerical = false;

        // 1. We try to get the cstic of the value.
        Characteristic currentCstic = null;
        if (valueBO2 != null){
            // we try to use the information from valueBO2
            currentCstic = valueBO2.getCharacteristic();
        }
        if (currentCstic == null) {
            // we try to use the information of attribute csticBO
            currentCstic = csticBO;
        }
        
        // 2. We try to find out whether value is numerical.
        int type = Characteristic.TYPE_UNDEFINED; // initially the type is undefined
        if (currentCstic != null){
            type = currentCstic.getValueType();
        }
        if (type == Characteristic.TYPE_INTEGER ||
            type == Characteristic.TYPE_FLOAT ||
            type == Characteristic.TYPE_CURRENCY ||
            type == Characteristic.TYPE_TIME){
            
            numerical = true;
        }
        
        csticBO = currentCstic; // keep the information of the cstic
        return numerical;
    }


    /**
     * @return true if the value belongs to a date cstic
     */
    private boolean isDate() {
        boolean date = false;

        // 1. We try to get the cstic of the value.
        Characteristic currentCstic = null;
        if (valueBO2 != null){
            // we try to use the information from valueBO2
            currentCstic = valueBO2.getCharacteristic();
        }
        if (currentCstic == null) {
            // we try to use the information of attribute csticBO
            currentCstic = csticBO;
        }
        
        // 2. We try to find out whether value is a date value.
        int type = Characteristic.TYPE_UNDEFINED; // initially the type is undefined
        if (currentCstic != null){
            type = currentCstic.getValueType();
        }
        if (type == Characteristic.TYPE_DATE){
            date = true;
        }
        
        csticBO = currentCstic; // keep the information of the cstic
        return date;
    }


    /**
     * Which name would be returned depends on the type of the delta.
     * <ul>
     * <li>deleted: static text "not selected"
     * <li>inserted: name of BO1 (there is only one)
     * <li>changed: name of BO2
     * <li>noDelta: name of BO1  
     * </ul>
     * @return the language dependent name of the characteristic value 2
     */
    public String getLanguageDependentName2(PageContext pageContext) {
        String lName;
        if(getType().equals(DELETED)){
            if (pageContext != null){
                lName = WebUtil.translate(pageContext, "ipc.compare.val.notselected", null);
            }
            else {
                lName = "ipc.compare.val.notselected";
            }
        }
        else if (getType().equals(CHANGED)){
            if (valueBO2 == null){
                // no BO -> return the internal lName2  (if available, otherwise the internal name2)
                if ((internalLName2 != null) && (!internalLName2.equals(""))){
                    lName = JspUtil.replaceSpecialCharacters(internalLName2);
                }
                else {
                    // Last chance:
                    // If the value belongs to a numerical or a date cstic we try to format it 
                    // otherwise we return the internal name.
                    lName = formatInternalValue(internalName2);
                }
            }
            else {
                // return the language-dependent name if uiContext.getShowLanguageDependent returns true
                // or if the characteristic has the type integer, float or date (in theses cases displaying
                // a language-independent view doesn't make sense                    
                if ((uiContext.getShowLanguageDependentNames() ||
                    valueBO2.getCharacteristic().getValueType() == Characteristic.TYPE_INTEGER ||
                    valueBO2.getCharacteristic().getValueType() == Characteristic.TYPE_FLOAT ||
                    valueBO2.getCharacteristic().getValueType() == Characteristic.TYPE_DATE)
                    && valueBO2.getLanguageDependentName() != null) {
                        lName = JspUtil.replaceSpecialCharacters(valueBO2.getLanguageDependentName());
                } else {
                        lName = JspUtil.replaceSpecialCharacters(valueBO2.getName());
                }
            }            
        }       
        else {  // NODELTA or INSERTED
            if (super.getBusinessObject() == null){
                // no BO -> return the internal lName (if available, otherwise the internal name)
                if ((internalLName != null) && (!internalLName.equals(""))){
                    lName = JspUtil.replaceSpecialCharacters(internalLName);
                }
                else {
                    // Last chance:
                    // If the value belongs to a numerical or a date cstic we try to format it 
                    // otherwise we return the internal name.
                    lName = formatInternalValue(internalName);
                }
            }
            else{
                lName = super.getLanguageDependentName();
            }
        }
        return lName;
    }

    /* (non-Javadoc)
     * @see com.sap.isa.ipc.ui.jsp.beans.ValueUIBean#isAssignable()
     */
    public boolean isAssignable() {
        if (super.getBusinessObject() == null){
            // no BO -> do not access BO, just return false
            return false;
        }
        else {
            return super.isAssignable();
        }
    }

    /* (non-Javadoc)
     * @see com.sap.isa.ipc.ui.jsp.beans.ValueUIBean#isAssigned()
     */
    public boolean isAssigned() {
        if (super.getBusinessObject() == null){
            // no BO -> do not access BO, just return false
            return false;
        }
        else {
            return super.isAssigned();
        }
    }

    /* (non-Javadoc)
     * @see com.sap.isa.ipc.ui.jsp.beans.ValueUIBean#getDescription()
     */
    public String getDescription() {
        if (super.getBusinessObject() == null){
            // no BO -> return the internal lName (if available, otherwise the internal name)
            if ((internalLName != null) && (!internalLName.equals(""))){
                return StringUtil.convertStringToHtml(JspUtil.replaceSpecialCharacters(internalLName));
            }
            else {
                return JspUtil.replaceSpecialCharacters(internalName);
            }
        }
        else {
            return super.getDescription();    
        }
    }

    /* (non-Javadoc)
     * @see com.sap.isa.ipc.ui.jsp.beans.ValueUIBean#isAssignedBySystem()
     */
    public boolean isAssignedBySystem() {
        if (super.getBusinessObject() == null){
            // no BO -> do not access BO, just return false
            return false;
        }
        else {
            return super.isAssignedBySystem();
        }
    }

    /* (non-Javadoc)
     * @see com.sap.isa.ipc.ui.jsp.beans.ValueUIBean#isAssignedByUser()
     */
    public boolean isAssignedByUser() {
        if (super.getBusinessObject() == null){
            // no BO -> do not access BO, just return false
            return false;
        }
        else {
            return super.isAssignedByUser();
        }
    }

    /* (non-Javadoc)
     * @see com.sap.isa.ipc.ui.jsp.beans.ValueUIBean#isInDomain()
     */
    public boolean isInDomain() {
        if (super.getBusinessObject() == null){
            // no BO -> do not access BO, just return false
            return false;
        }
        else {
            return super.isInDomain();
        }
        
    }

    /* (non-Javadoc)
     * @see com.sap.isa.ipc.ui.jsp.beans.ValueUIBean#getConditionKey()
     */
    public String getConditionKey() {
        if (super.getBusinessObject() == null){
            // no BO -> do not access BO, just return empty String
            return "";
        }
        else {
            return super.getConditionKey();
        }
    }

    /* (non-Javadoc)
     * @see com.sap.isa.ipc.ui.jsp.beans.ValueUIBean#getPrice()
     */
    public String getPrice() {
        if (super.getBusinessObject() == null){
            // no BO -> do not access BO, just return empty String
            return "";
        }
        else {
            return super.getPrice();
        }
    }
    
    /**
     * Dependent on the type of delta this method returns a start tag for formating the
     * first value.<br>
     * For example if it is an insert delta the value 1 (in this case "not selected") 
     * will be displayed italic. The start tag would be "<i>".
     * @return start tag for name1 
     */
    public String getFormatStartTagName1(){
        String startTag = "";
        if (getType().equals(INSERTED)) {
            startTag = "<i>";
        }
        return startTag;
    }

    /**
     * Dependent on the type of delta this method returns an end tag for formating the
     * first value.<br>
     * For example if it is an insert delta the value 1 (in this case "not selected") 
     * will be displayed italic. The end tag would be "</i>".
     * @return end tag for name1 
     */
    public String getFormatEndTagName1(){
        String endTag = "";
        if (getType().equals(INSERTED)) {
            endTag = "</i>";
        }
        return endTag;
    }
    
    /**
     * Dependent on the type of delta this method returns a start tag for formating the
     * second value.<br>
     * For example if it is a "deleted" delta the value 2 (in this case "not selected") 
     * will be displayed italic. The start tag would be "<i>".
     * @return start tag for name1 
     */
    public String getFormatStartTagName2(){
        String startTag = "";
        if (getType().equals(DELETED)) {
            startTag = "<i>";
        }
        return startTag;
    }

    /**
     * Dependent on the type of delta this method returns an end tag for formating the
     * second value.<br>
     * For example if it is a "deleted" delta the value 2 (in this case "not selected") 
     * will be displayed italic. The end tag would be "</i>".
     * @return end tag for name1 
     */
    public String getFormatEndTagName2(){
        String endTag = "";
        if (getType().equals(DELETED)) {
            endTag = "</i>";
        }
        return endTag;
    }
    
    public String toString(){
        StringBuffer sb = new StringBuffer();
        sb.append("ValueDeltaUIBean <TYPE>");
        sb.append(this.getType());
        sb.append("</TYPE>\n");
        sb.append("<VAL1>");
        sb.append(getName(null));
        sb.append("</VAL1>\n");       
        sb.append("<VAL2>");
        sb.append(getName2(null));
        sb.append("</VAL2>\n");
        sb.append("<REMARK>");
        sb.append(getRemark());
        sb.append("</REMARK>");       

        return sb.toString();
    }

    /**
     * Helper Method.<br>
     * The author for user-set values can be represented by an empty string ("") or 
     * or by space (" ").<br> 
     * The comparison FM returns the author for user-set values as empty string.<br>
     * But the author information for user-set values stored in an external configuration 
     * (e.g. as XML) is space.<br>
     * This method is for convenience. It checks whether the author is "" or " ". If this 
     * is the case it returns true. 
     * @param author author-flag
     * @return true if the value is set by user
     */
    private boolean isSetByUser(String author){
        if (author.equals(AUTHOR_USER) || author.equals(AUTHOR_USER_SPACE)){
            return true;
        }
        else {
            return false;
        }
    }
    /**
     * @return the cstic business object of this value delta. Has to be set explicitly!
     */
    public Characteristic getCsticBO() {
        return csticBO;
    }

    /**
     * Set a cstic BO for this value delta. Can be used to provide additional information for the value if no
     * valueBO exists. (e.g. deleted additional value -> did not belong to the domain, so no valueBO1 exists 
     * and also no valueBO2 because it has been deleted.
     * @param characteristic business object
     */
    public void setCsticBO(Characteristic characteristic) {
        csticBO = characteristic;
    }

}
