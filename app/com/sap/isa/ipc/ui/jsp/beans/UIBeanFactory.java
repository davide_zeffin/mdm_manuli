/*
 * Created on 23.09.2005
 *
 * To change the template for this generated file go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
package com.sap.isa.ipc.ui.jsp.beans;

import java.util.ArrayList;

import com.sap.isa.core.util.GenericFactory;
import com.sap.isa.ipc.ui.jsp.uiclass.UIContext;
import com.sap.spc.remote.client.ResourceAccessor;
import com.sap.spc.remote.client.object.Characteristic;
import com.sap.spc.remote.client.object.CharacteristicGroup;
import com.sap.spc.remote.client.object.CharacteristicValue;
import com.sap.spc.remote.client.object.ComparisonResult;
import com.sap.spc.remote.client.object.Configuration;
import com.sap.spc.remote.client.object.ConflictHandlingShortcut;
import com.sap.spc.remote.client.object.IPCPricingCondition;
import com.sap.spc.remote.client.object.Instance;
import com.sap.spc.remote.client.object.InstanceDelta;
import com.sap.spc.remote.client.object.LoadingMessage;
import com.sap.spc.remote.client.object.MimeObject;
import com.sap.spc.remote.client.object.ProductVariant;
import com.sap.spc.remote.client.object.ValueDelta;
import com.sap.tc.logging.Category;
import com.sap.tc.logging.Location;

/**
 * @author 
 *
 * To change the template for this generated type comment go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
public class UIBeanFactory {

	// logging
	private static final Category category = ResourceAccessor.category;
	private static final Location location = ResourceAccessor.getLocation(UIBeanFactory.class);
	protected static final String IPC_UIBEAN_FACTORY_NAME =
		"IPCUIBeanFactory";
	// used instance (Singeleton Pattern)
	public static UIBeanFactory instance = new UIBeanFactory();

	/**
	 * Return the only instance of the class (Singleton). <br>
	 * 
	 * @return Factory object. Depending on the scenario either RfcIPCClientObjectFactory or TcpIPCClientObjectFactory.
	 */
	public static UIBeanFactory getInstance() {
		return instance;
	}
	
//	TODO docu
	public static UIBeanFactory getUIBeanFactory() {

		UIBeanFactory factory = (UIBeanFactory)GenericFactory.getInstance(IPC_UIBEAN_FACTORY_NAME);

		if (factory == null) {
			location.errorT("entry [" + IPC_UIBEAN_FACTORY_NAME + "] missing in factory-config.xml");
		}

		return factory;		
	}

	public CharacteristicUIBean newCharacteristicUIBean(Characteristic cstic, UIContext uiContext) {
		return new CharacteristicUIBean(cstic, uiContext);
	}
    
    public CharacteristicUIBean newCharacteristicUIBean(Characteristic cstic, UIContext uiContext, boolean isMessageCstic) {
        return new CharacteristicUIBean(cstic, uiContext, isMessageCstic);
    }
    
	public ConditionUIBean newConditionUIBean(IPCPricingCondition conditionBo, UIContext uiContext) {
		return new ConditionUIBean(conditionBo, uiContext);
	}
	
	public ConfigUIBean newConfigUIBean(Configuration config, UIContext uiContext) {
		return new ConfigUIBean(config, uiContext);
	}
	
	public ConflictHandlingShortcutUIBean newConflictHandlingShortcutUIBean(ConflictHandlingShortcut conflictHandlingShortcut, UIContext uiContext) {
		return new ConflictHandlingShortcutUIBean(conflictHandlingShortcut, uiContext);
	}
	
	public GroupUIBean newGroupUIBean(CharacteristicGroup groupBO, UIContext uiContext) {
		return new GroupUIBean(groupBO, uiContext);
	}
	
	public InstanceUIBean newInstanceUIBean(Instance instanceBO, UIContext uiContext) {
		return new InstanceUIBean(instanceBO, uiContext);
	}

	public MessageUIBean newMessageUIBean(String status, String shortText){
		return new MessageUIBean(status, shortText);
	}
    
	public MessageUIBean newMessageUIBean(){
		return new MessageUIBean();
	}
    
	public MessageUIBean newMessageUIBean(String messageTranslateKey) {
		return new MessageUIBean(messageTranslateKey);
	}
    
    public MessageUIBean newMessageUIBean(String messageTranslateKey, String[] messageArguments) {
        return new MessageUIBean(messageTranslateKey, messageArguments);
    }
     
    public MessageUIBean newMessageUIBean(String status, String shortText, String messageTranslateKey, String[] messageArguments){
        return new MessageUIBean(status, shortText, messageTranslateKey, messageArguments);
    }
	
	public MimeUIBean newMimeUIBean(MimeObject mimeBO, UIContext uiContext) {
		return new MimeUIBean(mimeBO, uiContext);
	}
	
	public ProductVariantUIBean newProductVariantUIBean(ProductVariant productVariantBO, UIContext uiContext) {
		return new ProductVariantUIBean(productVariantBO, uiContext);
	}
	
	public StatusImage newStatusImage(String resourceKey, String imagePath) {
		return new StatusImage(resourceKey, imagePath);
	}
	
	public ValueUIBean newValueUIBean(CharacteristicValue valueBO, UIContext uiContext) {
		return new ValueUIBean(valueBO, uiContext);
	}

    public LoadingMessageUIBean newLoadingMessageUIBean(LoadingMessage bo, UIContext uic) {
        return new LoadingMessageUIBean(bo, uic);
    }
    
    /**
     * Creates a ValueDeltaUIBean based on one delta business object.
     * @param deltaBO business object of the delta
     * @param valueBO business object of the value
     * @param uiContext
     */
    public ValueDeltaUIBean newValueDeltaUIBean(ValueDelta deltaBO1, CharacteristicValue valueBO, UIContext uiContext) {
        return new ValueDeltaUIBean(deltaBO1, valueBO, uiContext);
    }

    /**
     * Creates a ValueDeltaUIBean that represents an unchanged value either of a multi-valued 
     * characteristic or in case showAll=T.
     * @param valueBO business object of the value
     * @param uiContext
     */
    public ValueDeltaUIBean newValueDeltaUIBean(CharacteristicValue valueBO, UIContext uiContext) {
        return new ValueDeltaUIBean(valueBO, uiContext); 
    }
     
    /**
     * Creates a ValueDeltaUIBean for a deleted instance (in this case no BO is available).
     * @param valueName internal name of the value
     * @param internalLName internal language dependent name of the value (pass null or empty string if not available)
     * @param uiContext
     */
    public ValueDeltaUIBean newValueDeltaUIBean(String valueName,  String internalLName, UIContext uiContext) {
        return new ValueDeltaUIBean(valueName, internalLName, uiContext);
    }
     
        
    /**
     * Creates a ValueDeltaUIBean based on two delta business objects.<br>
     * This could be used to condense a delta information in case of a single-valued characteristic.
     * Deltas for single-valued characteristic could be condensed. That means
     * if a single-valued characteristic has two deltas. One is a "delete"-delta,
     * the other one an "insert"-delta. This information could be condensed to a
     * "change"-delta.
     * So, instead of stating: "Value A has been deleted, then value B has been 
     * inserted". It will state: "The value has been changed from A to B."
     * @param deltaBO business object of the delta
     * @param deltaBO2 business object of the second delta that should be represented by this bean 
     * (only possible for single-valued characteristics) 
     * @param valueBO business object of the value
     * @param uiContext
     */
    public ValueDeltaUIBean newValueDeltaUIBean(ValueDelta deltaBO, 
        ValueDelta deltaBO2,
        CharacteristicValue valueBO, 
        CharacteristicValue valueBO2,
        UIContext uiContext) {
        return new ValueDeltaUIBean(deltaBO, deltaBO2, valueBO, valueBO2, uiContext);
    }            
    
    /**
     * Creates a CharacteristicDeltaUIBean for a deleted instance (in this case no BO is available)
     * @param csticName internal name of the characteristic
     * @param internalLName internal language dependent name of the characteristic (pass null or empty string if not available)
     * @param uiContext
     */
    public CharacteristicDeltaUIBean newCharacteristicDeltaUIBean(String csticName, String internalLName, UIContext uiContext) {
        return new CharacteristicDeltaUIBean(csticName, internalLName, uiContext);
    }
    
    /**
     * Creates a CharacteristicDeltaUIBean.
     * @param characteristicBO  business object of the characteristic
     * @param uiContext
     */
    public CharacteristicDeltaUIBean newCharacteristicDeltaUIBean(Characteristic characteristicBO, UIContext uiContext) {
        return new CharacteristicDeltaUIBean(characteristicBO, uiContext);
    }

    /**
     * Creates a CharacteristicDeltaUIBean.
     * @param characteristicBO business object of the characteristic
     * @param uiContext
     * @param valueDeltas list of value deltas
     * @param loadingMessages list of loading messages
     */
    public CharacteristicDeltaUIBean newCharacteristicDeltaUIBean(Characteristic characteristicBO, UIContext uiContext, ArrayList valueDeltas, ArrayList loadingMessages) {
        return new CharacteristicDeltaUIBean(characteristicBO, uiContext, valueDeltas, loadingMessages);
    }

    /**
     * Creates a GroupDeltaUIBean
     * @param groupBO business object of the group
     * @param uiContext
     */
    public GroupDeltaUIBean newGroupDeltaUIBean(CharacteristicGroup groupBO, UIContext uiContext) {
        return new GroupDeltaUIBean(groupBO, uiContext);
    }
    
    /**
     * Creates a GroupDeltaUIBean
     * @param groupBO business object of the group
     * @param uiContext
     * @param csticDeltas list of cstic deltas
     */
    public GroupDeltaUIBean newGroupDeltaUIBean(CharacteristicGroup groupBO, UIContext uiContext, ArrayList csticDeltas) {
        return new GroupDeltaUIBean(groupBO, uiContext, csticDeltas);
    }
    
    /**
     * Creates InstanceDeltaUIBean for a deleted instance.
     * @param instDeltaBO business object of the instance delta
     * @param internalName internal name of the instance
     * @param internalLName internal language dependent name of the instance (pass null or empty string if not available)
     * @param uiContext
     */
    public InstanceDeltaUIBean newInstanceDeltaUIBean(InstanceDelta instDeltaBO, String internalName, String internalLName, UIContext uiContext) {
        return new InstanceDeltaUIBean(instDeltaBO, internalName, internalLName, uiContext);
    }
    
    /**
     * Creates InstanceDeltaUIBean.
     * @param instDeltaBO business object of the instance delta
     * @param instanceBO business object of the instance
     * @param uiContext
     */
    public InstanceDeltaUIBean newInstanceDeltaUIBean(InstanceDelta instDeltaBO, Instance instanceBO, UIContext uiContext) {
        return new InstanceDeltaUIBean(instDeltaBO, instanceBO, uiContext);
    }

    /**
     * Creates InstanceDeltaUIBean.
     * @param instDeltaBO business object of the instance delta
     * @param instanceBO business object of the instance
     * @param loadingMessages list of loading messages for this instance 
     * @param uiContext
     */
    public InstanceDeltaUIBean newInstanceDeltaUIBean(InstanceDelta instDeltaBO, Instance instanceBO, ArrayList loadingMessages, UIContext uiContext) {
        return new InstanceDeltaUIBean(instDeltaBO, instanceBO, loadingMessages, uiContext);
    }


    /**
     * Creates ComparisonResultUIBean
     * @param resultBO business object of the comparison result
     * @param uiContext
     */
    public ComparisonResultUIBean newComparisonResultUIBean(ComparisonResult resultBO, UIContext uiContext) {
        return new ComparisonResultUIBean(resultBO, uiContext);
    }

    /**
     * Creates a GroupDeltaUIBean for a deleted instance (in this case no BO is available).
     * @param uiContext
     */
    public GroupDeltaUIBean newGroupDeltaUIBean(UIContext uiContext) {
        return new GroupDeltaUIBean(uiContext);
    }
    
   
    
}
