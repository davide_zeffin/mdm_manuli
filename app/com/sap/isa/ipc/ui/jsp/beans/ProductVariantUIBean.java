 /*
 * Created on 11.08.2004
 *
 * To change the template for this generated file go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
package com.sap.isa.ipc.ui.jsp.beans;

import com.sap.isa.ipc.ui.jsp.uiclass.UIContext;
import com.sap.spc.remote.client.object.Configuration;
import com.sap.spc.remote.client.object.IPCItem;
import com.sap.spc.remote.client.object.ProductVariant;

/**
 * @author 
 *
 * To change the template for this generated type comment go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
public class ProductVariantUIBean {
    
	protected ProductVariant productVariantBO;
	protected UIContext uiContext;
	protected InstanceUIBean instance;
    
    protected ProductVariantUIBean(ProductVariant productVariantBO, UIContext uiContext) {
        this.productVariantBO = productVariantBO;
        this.uiContext = uiContext;
    }

	/**
	 * In order to give customers the possibility access business objects in JSP pages.
	 * This is useful especially for customer extension data of business objects.
	 * @return
	 */
    public ProductVariant getBusinessObject() {
    	return productVariantBO;
    }
    
    public static void main(String[] args) {
    }
    /**
     * @return
     */
    public String getId() {
        return productVariantBO.getId();
    }
    
    public String getPrice() {
        return productVariantBO.getConfiguration().getRootInstance().getPrice();
    }
    
    public IPCItem getItem() {
        return productVariantBO.getConfiguration().getRootInstance().getIpcItem();
    }
    
    public InstanceUIBean getInstance() {
        return instance;
    }
    
    public void setInstance(InstanceUIBean instance) {
        this.instance = instance;
    }

	/**
	 * @param configuration
	 */
	public void setConfiguration(Configuration configuration) {
		productVariantBO.setConfiguration(configuration);
	}

	/**
	 * @param ipcItem
	 */
	public void setItem(IPCItem ipcItem) {
		productVariantBO.setItem(ipcItem);
	}

	/**
	 * @return
	 */
	public String getMatchPoints() {
		return productVariantBO.getMatchPoints();
	}

	/**
	 * @return
	 */
	public String getMaxMatchPoints() {
		return productVariantBO.getMaxMatchPoints();
	}

	/**
	 * @return
	 */
	public Configuration getConfiguration() {
		return productVariantBO.getConfiguration();
	}

}
