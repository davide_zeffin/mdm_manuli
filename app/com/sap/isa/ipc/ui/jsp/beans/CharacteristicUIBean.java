/*
 * Created on 02.06.2004
 *
 * To change the template for this generated file go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
package com.sap.isa.ipc.ui.jsp.beans;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.jsp.PageContext;

import com.sap.isa.core.RequestProcessor;
import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.core.util.JspUtil;
import com.sap.isa.core.util.WebUtil;
import com.sap.isa.ipc.ui.jsp.action.visitor.IVisitableObject;
import com.sap.isa.ipc.ui.jsp.action.visitor.IVisitor;
import com.sap.isa.ipc.ui.jsp.uiclass.UIContext;
import com.sap.isa.ipc.ui.jsp.util.FormatValidator;
import com.sap.isa.ipc.ui.jsp.util.LocalizedEntryFieldMask;
import com.sap.isa.ipc.ui.jsp.util.StringUtil;
import com.sap.spc.remote.client.object.Characteristic;
import com.sap.spc.remote.client.object.CharacteristicGroup;
import com.sap.spc.remote.client.object.Instance;
import com.sap.spc.remote.client.object.MimeObject;
import com.sap.spc.remote.client.object.MimeObjectContainer;
import com.sap.spc.remote.client.object.OriginalRequestParameterConstants;

/**
 * @author 
 *
 * To change the template for this generated type comment go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
public class CharacteristicUIBean implements IVisitableObject{
    protected String erroneousValue;
	protected FormatValidator.FormatError validationError;
	protected Characteristic characteristicBO = null;
	protected UIContext uiContext = null;

    private static IsaLocation location = IsaLocation.getInstance(CharacteristicUIBean.class.getName());
    private static final CharacteristicUIBean helperInstance =
        new CharacteristicUIBean();
    public static final String CHARACTERISTIC_UI_BEANS = "CSTICUIBEANS"; //$NON-NLS-1$
    public static final int LAYOUT_1 = 1;
    public static final int LAYOUT_2 = 2;
    public static final int LAYOUT_3 = 3;
    public static final int LAYOUT_4 = 4;
    public static final int LAYOUT_5 = 5;
    public static final int LAYOUT_6 = 6;
    public static final int LAYOUT_ERROR = 0;
    
    protected ArrayList values = new ArrayList();
    protected ArrayList assignableValues = new ArrayList();
	protected ArrayList assignedValues = new ArrayList();
	protected ArrayList mimes = new ArrayList();
	protected HashMap mimesByType = new HashMap();
	protected boolean valueMimesForWorkArea = false;
	protected boolean mimesForWorkArea = false;
	protected boolean additionalMimes = false;
	protected String description;
	protected boolean lastFocused = false;
	protected boolean messageCstic = false;

    class Key {
        protected String instanceId;
		protected String csticName;

        Key(String instanceId, String csticName) {
            this.instanceId = instanceId;
            this.csticName = csticName;
        }

        public boolean equals(Object o) {
            if (o instanceof Key) {
                Key key = (Key) o;
                if (key.instanceId.equals(this.instanceId)
                    && key.csticName.equals(this.csticName)) {
                    return true;
                }
            }
            return false;
        }

        public int hashCode() {
            return instanceId.hashCode() ^ csticName.hashCode();
        }
        
        public String toString() {
            StringBuffer result = new StringBuffer();
            result.append("Instance:"); //$NON-NLS-1$
            result.append(instanceId);
            result.append(";"); //$NON-NLS-1$
            result.append("Cstic:"); //$NON-NLS-1$
            result.append(csticName);
            return result.toString();
        }

		public String getCsticName() {
			return csticName;
    }

		public String getInstanceId() {
			return instanceId;
		}

    }

    /**
     * @param request
     * @param cstic
     * @param uiContext
     * @return An instance of type CharacteristicUIBean.
     * This instance is first searched in the request identified by cstic.getInstance().getInstanceId() and
     * cstic.getCsticName().
     * If the instance is not in the request, it is created and added to the request.
     */
    public static CharacteristicUIBean getInstance(
        HttpServletRequest request,
        Characteristic cstic,
        UIContext uiContext) {
        HashMap container;
        Object containerObject = request.getAttribute(CHARACTERISTIC_UI_BEANS);
        if (containerObject == null) {
            container = new HashMap();
            RequestProcessor.setExtendedRequestAttribute(request, CHARACTERISTIC_UI_BEANS, container);
            if (location.isDebugEnabled()) {
                location.debug("Creating HashMap for " + CHARACTERISTIC_UI_BEANS + " and adding to request!"); //$NON-NLS-1$ //$NON-NLS-2$
            }
        } else {
            container = (HashMap) containerObject;
            if (location.isDebugEnabled()) {
                location.debug("Retrieved HashMap for " + CHARACTERISTIC_UI_BEANS + " from request!"); //$NON-NLS-1$ //$NON-NLS-2$
            }
        }
        Key key =
            helperInstance.new Key(
                cstic.getInstance().getId(),
                cstic.getName());
        Object uiBeanObject = container.get(key);
        if (uiBeanObject == null) {
            if (location.isDebugEnabled()){
                location.debug("No CharacteristicUIBean instance for " + key + " found! Creating one and adding to container."); //$NON-NLS-1$ //$NON-NLS-2$
            }
            CharacteristicUIBean csticUIBean =
                UIBeanFactory.getUIBeanFactory().newCharacteristicUIBean(cstic, uiContext);
            container.put(key, csticUIBean);
            return csticUIBean;
        } else {
            if (location.isDebugEnabled()){
                location.debug("CharacteristicUIBean instance for " + key + " found."); //$NON-NLS-1$ //$NON-NLS-2$
            }
            return (CharacteristicUIBean) uiBeanObject;
        }
    }
    
    public static CharacteristicUIBean getInstance(
        HttpServletRequest request,
        Characteristic cstic,
        UIContext uiContext,
        boolean messageCstic){
        HashMap container;
        Object containerObject = request.getAttribute(CHARACTERISTIC_UI_BEANS);
        if (containerObject == null) {
            container = new HashMap();
            RequestProcessor.setExtendedRequestAttribute(request, CHARACTERISTIC_UI_BEANS, container);
            if (location.isDebugEnabled()) {
                location.debug("Creating HashMap for " + CHARACTERISTIC_UI_BEANS + " and adding to request!"); //$NON-NLS-1$ //$NON-NLS-2$
            }
        } else {
            container = (HashMap) containerObject;
            if (location.isDebugEnabled()) {
                location.debug("Retrieved HashMap for " + CHARACTERISTIC_UI_BEANS + " from request!"); //$NON-NLS-1$ //$NON-NLS-2$
            }
        }
        Key key =
            helperInstance.new Key(
                cstic.getInstance().getId(),
                cstic.getName());
        Object uiBeanObject = container.get(key);
        if (uiBeanObject == null) {
            if (location.isDebugEnabled()){
                location.debug("No CharacteristicUIBean instance for " + key + " found! Creating one and adding to container."); //$NON-NLS-1$ //$NON-NLS-2$
            }
            CharacteristicUIBean csticUIBean;
            if (messageCstic){
                // message cstic
                csticUIBean = UIBeanFactory.getUIBeanFactory().newCharacteristicUIBean(cstic, uiContext, true);
            }
            else{
                // normal cstic
                csticUIBean = UIBeanFactory.getUIBeanFactory().newCharacteristicUIBean(cstic, uiContext);
            }
            container.put(key, csticUIBean);
            return csticUIBean;
        } else {
            if (location.isDebugEnabled()){
                location.debug("CharacteristicUIBean instance for " + key + " found."); //$NON-NLS-1$ //$NON-NLS-2$
            }
            return (CharacteristicUIBean) uiBeanObject;
        }
    }
    
    
    protected CharacteristicUIBean() {
    }

    protected CharacteristicUIBean(
        Characteristic characteristicBO,
        UIContext uiContext) {
        this.characteristicBO = characteristicBO;
        this.uiContext = uiContext;
        this.validationError = FormatValidator.getInstance().new FormatError(FormatValidator.FormatError.OK);
        // check for null required because of initialisation of static member helperInstance = new CharacteristicUIBean(null, null);
        if (characteristicBO != null) {
            MimeObjectContainer mimeObjectContainer = this.characteristicBO.getMimeObjects();
            // generate mimeUIBeans only if necessary
            if (mimeObjectContainer != null) {
                Iterator mimeBOList = mimeObjectContainer.getMimeObjects().iterator();
                while (mimeBOList.hasNext()) {
                    MimeObject mimeBO = (MimeObject) mimeBOList.next();
                    MimeUIBean mimeBean = UIBeanFactory.getUIBeanFactory().newMimeUIBean(mimeBO, this.uiContext);
                    addMime(mimeBean); 
                }
            }
        }
    }
   
    protected CharacteristicUIBean(
            Characteristic characteristicBO,
            UIContext uiContext,
            boolean messageCstic) {
            this( characteristicBO, uiContext );
            this.messageCstic = messageCstic;
        }

	/**
	 * In order to give customers the possibility access business objects in JSP pages.
	 * This is useful especially for customer extension data of business objects.
	 * @return
	 */
    public Characteristic getBusinessObject() {
        return characteristicBO;
    }

    /**
     * 
     * @return The language independent name of the associated business object characteristic.
     */
    public String getName() {
        return JspUtil.replaceSpecialCharacters(characteristicBO.getName());
    }
    /**
     * 
     * @return returns the description restricted to the length specified in XCM parameter 
     */
    public String getLengthLimitedDescription()
    {
        String maxLength = uiContext.getCharacteristicsMaxDescriptionLength();
        int maxLengthI = 80;
        if( maxLength != null)
        {
            maxLengthI = Integer.parseInt(maxLength);
        }
        else
        {
            if(location.isDebugEnabled())
                location.debug("XCM value characteristics.maxdescriptionlength was not resolved. Use default length 80 instead");
        }
        String descr= getDescription();
        if( descr != null && descr.length() > maxLengthI)
        {
         /*   descr= descr.substring(0, maxLengthI);
            return  descr.replaceAll("\\s[^\\s]*$","");*/
			descr = descr.substring(0, maxLengthI).replaceAll("\\s[^\\s]*$","");
		    descr = descr + "...";
		    return descr;
        }
        else 
            return descr;        
    }
	

    /**
     * The display name of a characteristic depends on two XCM parameters:<br/>
     * 1. characteristic.descriptionlength<br/>
     * 2. appearance.showlangdepnames<br/>
     * If <code>appearance.showlangdepnames</code> is false, the ID of the characteristic will be used
     * instead of the language dependand name.<br/>
     * If characteristic.descriptionlength is configured to hold the shorttext and the ID, both
     * values will be displayed by using the resource key "ipc.cstic.descr.delimiter".
     * 
     * @return The display name
     */
    public String getDisplayName() {
        String name = null;
        // Check if only the Name is to be displayed in the shortname field
        if(uiContext.getCharacteristicsDescriptionType().equals(OriginalRequestParameterConstants.iDTextId) ||
           uiContext.getCharacteristicsDescriptionType().equals(OriginalRequestParameterConstants.iDAndLongDescriptionTextId))
            return characteristicBO.getName();
        name=getLanguageDependentName();
        if(uiContext.getCharacteristicsDescriptionType().equals(OriginalRequestParameterConstants.iDAndShortDescriptionTextId) ||
                uiContext.getCharacteristicsDescriptionType().equals(OriginalRequestParameterConstants.iDAndShortAndLongDescriptionTextId))
        {
             return WebUtil.translate(uiContext.getLocale(),"ipc.cstic.descr.delimiter", new String[]{name, characteristicBO.getName()} );
        }
        if(uiContext.getCharacteristicsDescriptionType().equals(OriginalRequestParameterConstants.longDescriptionTextId))
        {
           // name = getDescriptionFirstLine();
		   	  name = StringUtil.getDescriptionFirstLine(uiContext.getCharacteristicsMaxDescriptionLength(), getDescription(), getLengthLimitedDescription(), getLanguageDependentName(), location);
            if( name== null || name.length()==0)
                return getLanguageDependentName();
        }
        return name;
    }


     /**
     *  
     * @return The language dependent name of the associated business object characteristic depending on the uiContext.
     * 
     */
    public String getLanguageDependentName() {
        String name = null;
        // Check if only the Name is to be displayed in the shortname field
        if (uiContext.getShowLanguageDependentNames()) {
            if (characteristicBO.getLanguageDependentName() == null) {
                name = characteristicBO.getName();
            } else {
                name =
                    JspUtil.replaceSpecialCharacters(
                        characteristicBO.getLanguageDependentName());
            }
        } else {
            name =
            JspUtil.replaceSpecialCharacters(
                    characteristicBO.getName());
        }
        return name;
    }

    /**
     * A characteristic is visible if the user has permission to see invisible characteristics
     * or the characteristic is visible.
     * @return
     */
    public boolean isVisible() {
        return characteristicBO.isVisible() || uiContext.getShowInvisibleCharacteristics();
    }

    /**
     * 
     * @return The relative URL to the status image of the characteristic
     * ipc/mimes/images/table/dot_green.gif = consistent and complete
     * ipc/mimes/images/table/dot_gray.gif = required
     * ipc/mimes/images/table/dot_yellow.gif = incomplete
     * ipc/mimes/images/table/dot_red.gif = inconsistent
     */
    public StatusImage getStatusImage() {
        String sourceFile = "ipc/mimes/images/table/dot_green.gif";
        String statusDescription = "ipc.cstic.complete";

        if (characteristicBO.isReadOnly()) {
            sourceFile = "ipc/mimes/images/table/dot_gray.gif";
            statusDescription = "ipc.cstic.readonly";
        }

        if (characteristicBO.isRequired()
            && !characteristicBO.hasAssignedValues()) {
            sourceFile = "ipc/mimes/images/table/dot_yellow.gif";
            statusDescription = "ipc.cstic.required";
        }

        if (!characteristicBO.isConsistent()) {
            sourceFile = "ipc/mimes/images/table/dot_red.gif";
            statusDescription = "ipc.cstic.inconsistent";
        }

        return UIBeanFactory.getUIBeanFactory().newStatusImage(statusDescription, sourceFile);
    }

    /**
     * 
     * @return the name of the style-class of the table
     */
    public String getTableStyleClass() {
        String characteristic_tableStyleClass = "ipcCharacteristicBar";
        if (!uiContext.getShowStatusLights()
            && !characteristicBO.isConsistent())
            characteristic_tableStyleClass = "ipcCharacteristicBarConflict";
        return characteristic_tableStyleClass;
    }

	/**
	* Please modify this in conjunction with the method: getTableStyleClass()
	* @return the tooltip of the style-class of the table
	*/
   public String getTableStyleTooltip() {
	   String tooltip = "ipc.tt.cstic";
	   if (!uiContext.getShowStatusLights()
		   && !characteristicBO.isConsistent())
		   tooltip = "ipc.tt.cstic.conflict";
	   return tooltip;
   }

    /**
     * 
     * @return true if input field is required
     */
    public boolean requiresFreeTextInputField() {
        //mk 05.12.03
        //excluded the entry !currentCharacteristic.isDomainConstrained()
        //because for restrictable cstics without intervalls and
        //without additional values allowed the text field was shown      
        //tm 13.01.2004 reverted this change since cstics with an unconstraint domain, without intervals
        //and without additional values did not have an input field anymore.
        //We have to live with the text field for the case mk describes above since we do not have a flag
        //in the object layer isRestrictable like in the EAL layer. Would require extension of the command
        //GetFilteredCsticsAndValues
        if (!uiContext.getDisplayMode()
            && !characteristicBO.isReadOnly()
            && characteristicBO.isVisible()
            && (characteristicBO.allowsAdditionalValues()
                || !characteristicBO.isDomainConstrained()
                || characteristicBO.hasIntervalAsDomain())) {
            return true;
        } else {
            return false;
        }
    }

    public boolean hasUnit() {
        return !characteristicBO.getUnit().equals("");
    }

    /**
     * @return the measurement unit of the characteristic if one was assigned
     */
    public String getUnit() {
        String unit = null;
        if (uiContext.getShowLanguageDependentNames()) {
            unit = characteristicBO.getLanguageDependentUnit();
        } else {
            unit = characteristicBO.getUnit();
        }
        return unit;
    }

    /**
     * @return true if the characteristic has a least one value
      * assigned
     */
    public boolean hasAssignedValues() {
        return characteristicBO.hasAssignedValues();
    }

    /**
     * @return true if the domain of the characteristic may contain an interval (1 - 10).
     */
    public boolean hasIntervalAsDomain() {
        return characteristicBO.hasIntervalAsDomain();
    }

    /**
     * @return true if the characteristic can only be read
     * but cannot be changed
     */
    public boolean isReadOnly() {
        return characteristicBO.isReadOnly();
    }

    /**
     * @return true if you can assign multiple values
     */
    public boolean allowsMultipleValues() {
        return characteristicBO.allowsMultipleValues();
    }

    /**
     * 
     * @param validationError - the validationError that is assigned to this characateristic
     */
    public void setValidationError(FormatValidator.FormatError validationError) {
        this.validationError = validationError;
    }

    /**
     * 
     * @return the validationError of the characteristic
     */
    public FormatValidator.FormatError getValidationError() {
        return validationError;
    }

    /**
     * 
     * @param erroneousValue - the value of the characteristic that lead to the validationError
     */
    public void setErroneousValue(String erroneousValue) {
        this.erroneousValue = erroneousValue;
    }

    /**
     * 
     * @return the value of the characteristic that lead to the validationError
     */
    public String getErroneousValue() {
        return erroneousValue;
    }

	protected void addAssignableValue(ValueUIBean assignableValue) {
        assignableValues.add(assignableValue);
    }
    /**
     * @return the values that can be assigned to
     * this characteristic.
     */
    public List getAssignableValues() {
        return assignableValues;
    }

	protected void addAssignedValue(ValueUIBean assignedValue) {
        assignedValues.add(assignedValue);
    }
    /**
     * @return the values that have been assigned
     * If no characterist values have been assigned you will
     * get an empty list
     */
    public List getAssignedValues() {
        return assignedValues;
    }

    /**
     * @return true if the characteristic has a least one value
     * assigned by the user
     */
    public boolean hasAssignedValuesByUser() {
        return characteristicBO.hasAssignedValuesByUser();
    }

	/**
	 * @return true if the characteristic has a least one value
	 * assigned by the 
	 */
//	public boolean hasAssignedValuesByUser() {
//		return characteristicBO.hasAssignedValuesByUser();
//	}


    /**
     * @return The maximum value input length for this characteristic.
     */
    public int getMaxLength() {
        if (characteristicBO.getValueType() == Characteristic.TYPE_STRING) {
            return characteristicBO.getTypeLength();
        } else if (characteristicBO.getValueType() == Characteristic.TYPE_DATE){
			if (characteristicBO.getEntryFieldMask() != null
				&& !characteristicBO.getEntryFieldMask().equals("")
				&& !characteristicBO.getEntryFieldMask().equals("null")) {
				return characteristicBO.getEntryFieldMask().length();
			}
			else {
				// CRM products might not have an entry field mask for date cstics
				return 10;
			}

		} else if (
            characteristicBO.getEntryFieldMask() != null
                && !characteristicBO.getEntryFieldMask().equals("")
                && !characteristicBO.getEntryFieldMask().equals("null")) {
            return characteristicBO.getEntryFieldMask().length();
        } else {
            return 0;
        }

    }

    /**
     * 
     * @param pageContext - current pageContext object
     * @return the input field mask (template)
     */
    public String getInputFieldMask(PageContext pageContext) {
        if (uiContext.getShowInputFieldMask() && pageContext != null) {
            String patternSign = WebUtil.translate(pageContext, "ipc.pattern.sign", null);
            String monthAbbreviation = WebUtil.translate(pageContext, "ipc.pattern.month", null);
            String dayAbbreviation = WebUtil.translate(pageContext, "ipc.pattern.day", null);
            String yearAbbreviation = WebUtil.translate(pageContext, "ipc.pattern.year", null);
            return LocalizedEntryFieldMask.generate(
                characteristicBO,
                uiContext.getLocale(),
                patternSign,
                monthAbbreviation,
                dayAbbreviation,
                yearAbbreviation);
        } else {
            return "";
        }
    }
    
    /**
     * Returns the date format for the calendar control. Unlike the method getInputFieldMask() this
     * method returns the date format always with the following abbreviations:
     * M for month
     * d for day
     * y for year
     * And the language-dependent pattern separator. 
     * The calendar control accepts only these abbreviations and not the language-dependent ones that
     * are returned by getInputFieldMask(). 
     * 
     * @return the date format for the calendar control
     */
    public String getDateFormatForCalendarControl() {
        return LocalizedEntryFieldMask.generate(
                characteristicBO,
                uiContext.getLocale(),
                "", // we pass nothing as pattern-sign because this is not needed here 
                "M",
                "d",
                "y");
    }

    /**
     * @return the length (in characters) a value of this characteristic may not
     * exceed.
     * Please note that in case of numeric values, particularly floating point
     * values, the length only counts digits, not separators or leading "-".
     */
    public int getTypeLength() {
        return characteristicBO.getTypeLength();
    }

    /**
     * @return the description of chartacteristic business object
     */
    public String getDescription() {
    	String description = characteristicBO.getDescription();
    	if (uiContext.getShowLanguageDependentNames() && description != null) {
    		return StringUtil.convertStringToHtml(JspUtil.replaceSpecialCharacters(description));
    	} else {
    		return JspUtil.replaceSpecialCharacters(getLanguageDependentName());
    	}
    }
    
    /**
     * @return a list of all MIME objects (images, sounds, etc.)
     * attached to the characteristic.
     */
    public ArrayList getMimes() {
        return this.mimes;
    }
    
    /**
     * Returns all Mimes of this characteristic and the given type.
     * @param Type of the mimes
     * @return List of mimes of this type
     */
    public ArrayList getMimesByType(String type) {
        ArrayList listOfMimes = (ArrayList) mimesByType.get(type.toLowerCase());
        if (listOfMimes == null) {
            listOfMimes = new ArrayList();        
        }
        return listOfMimes;        
    }

    /**
     * Add a MIME object (MimeUIBean) to the list of MIMEs
     * @param MimeUIBean to be added to this characteristic
     */
    public void addMime(MimeUIBean mime) {
        mimes.add(mime);        
        // try to receive the ArrayList of the given type
        String mimeType = mime.getType();
        ArrayList mimesOfSpecificType = (ArrayList) mimesByType.get(mimeType.toLowerCase());
        if (mimesOfSpecificType == null) {
            mimesOfSpecificType = new ArrayList();
            mimesByType.put(mime.getType().toLowerCase(), mimesOfSpecificType);
        }
        mimesOfSpecificType.add(mime);
        // set flag if mime is of type icon, o2c or sound
        // this is done during instantiation-process because of performance-reasons
        if (mimeType.equalsIgnoreCase(MimeUIBean.ICON) 
            || mimeType.equalsIgnoreCase(MimeUIBean.O2C)
            || mimeType.equalsIgnoreCase(MimeUIBean.SOUND)) {
            
            this.mimesForWorkArea = true;                
        }
        // set flag if mime is of type Application or Unknown
        if (mimeType.equalsIgnoreCase(MimeUIBean.APPLICATION) 
            || mimeType.equalsIgnoreCase(MimeUIBean.UNKNOWN)) {

            this.additionalMimes = true;                
        }                
    }
    

    /**
     * @return the list of the characteristic values
     */
    public ArrayList getValues() {
        return values;
    }

    /**
     * @param value add a valu (ValueUIBean) to the list of values 
     */
    public void addValue(ValueUIBean value) {
        values.add(value);
        if (value.isAssignable()) {
            addAssignableValue(value);
        }
        if (value.isAssigned()) {
            addAssignedValue(value);
        }
        // set flag if value has a mime of type icon, o2c or sound 
        // this is done during instantiation-process because of performance-reasons
        if (value.hasMimesForWorkArea()) { 
            this.valueMimesForWorkArea = true;                
        }        
    }

    /**
     * TODO: set the description in GetLongtextAction
     * @param string the description
     */
    public void setDescription(String string) {
        description = string;
    }

    /**
     * @return true if the characteristic must be evaluated
     */
    public boolean isRequired() {
        return characteristicBO.isRequired();
    }

    /**
     * @return true if the characteristic is consistent
     */
    public boolean isConsistent() {
        return characteristicBO.isConsistent();
    }

	/**
	 * 
	 * @return true if the characteristic to be displayed is in "expanded" mode,
	 * that is with the allowed values visible.
	 * This decision is made by the attibute of the characteristic business object,
	 * the global setting "show cstic values expanded" and also the XCM customization parameter is 
     * taken into account.
     * The fact that the user manually expanded or collapsed the cstic with the "Display Options"
     * is NOT taken into account here. This is handled in the MergeVisitor. 
	 */
	public boolean isExpanded() {

		//Get the expand value from the UI context
		int expandValue=uiContext.getCharacteristicValuesExpanded();
		//Get the backend value of Characteristic Expanded 
		boolean backEndExpValue=false;
		boolean returnVal=false;
    	
		switch(expandValue) {
			case OriginalRequestParameterConstants.CSTIC_SHOW_EXPAND_CSTIC_DEP:
								backEndExpValue=getBusinessObject().isExpanded();
								returnVal=backEndExpValue;
								break;
			case OriginalRequestParameterConstants.CSTIC_SHOW_EXPAND_FALSE:
								returnVal=false;
								break;
			case OriginalRequestParameterConstants.CSTIC_SHOW_EXPAND_TRUE:
								returnVal=true;
								break;
		}
		return returnVal;
	}

    /**
     * @return true if the characteristic is the one that had the focus before the server roundtrip
     */
    public boolean isLastFocused() {
        return lastFocused;
    }

    /**
     * @param lastFocused - true if the characteristic is the one that had the focus before the server roundtrip
     */
    public void setLastFocused(boolean lastFocused) {
        this.lastFocused = lastFocused;
    }
    
    /**
     * @return true if a link to the calendar control should be displayed.
     */
    public boolean showCalendarControl(){
        boolean showCalendarControl = false;
        if (uiContext.getShowCalendarControl() && (characteristicBO.getValueType() == Characteristic.TYPE_DATE)) {
            showCalendarControl = true;
        }
        return showCalendarControl;
    }
    
    /**
     * @return true if at least one of the assigned values is part of the static domain.
     */
    public boolean hasDomainValues(){
        Iterator valueIterator = values.iterator();
        boolean hasDomainValues = false;      
        while (valueIterator.hasNext()) {
            ValueUIBean value = (ValueUIBean) valueIterator.next();
            if (value.isInDomain()) {
                hasDomainValues = true;
                break;
            }
        }
        return hasDomainValues;
    }
    
    /**
     * Determines the layout.
     * For the relevant attributes and the decision table see method determineLayoutNumber().
     * In the case of an invalid combination of the attributes we display 
     * "selection list, input field and interval domain" instead of an error .
     * @return the determined layout  
     */
    public String determineLayout() {
        String layout = "";
        int layoutNumber = determineLayoutNumber();
        switch (layoutNumber) {
            case LAYOUT_1:
                // Layout 1: Selection list only
                layout = uiContext.getJSPInclude("layouts.selectionList.jsp");
                break;
            case LAYOUT_2:
                // Layout 2: Selection list and input field
                layout = uiContext.getJSPInclude("layouts.selectionListWithInputField.jsp");
                break;
            case LAYOUT_3:
                // Layout 3: Input field only
                layout = uiContext.getJSPInclude("layouts.inputField.jsp");
                break;
            case LAYOUT_4:
                // Layout 4: Selection list, input field and interval domain
                layout = uiContext.getJSPInclude("layouts.selectionListWithInputFieldAndInterval.jsp");
                break;
            case LAYOUT_5:
                // Layout 5: Input field and interval domain
                layout = uiContext.getJSPInclude("layouts.inputFieldWithInterval.jsp");
                break;           
            case LAYOUT_6:
                // Layout 6: Selection list and input field for characteristics with suggested values (i.e. static domain plus allowsAdditionalValues=T)
                layout = uiContext.getJSPInclude("layouts.selectionListForSuggestedValuesWithInputField.jsp"); 
                break;                
            default:
                // Error: This combination of attributes should never occur.
                // In this case we display "selection list, input field and interval domain" instead of an error
                layout = uiContext.getJSPInclude("layouts.selectionListWithInputFieldAndInterval.jsp");
                break;
        }

        return layout;
    }
    
    /**
     * Determines Layout and returns the layout as number.
     * The decision is made between the following layouts:
     * <ol>
     * <li>Selection list only (Layout-Number: 1)
     * <li>Selection list and input field (Layout-Number: 2)
     * <li>Input field only (Layout-Number: 3)
     * <li>Selection list, input field and interval domain (Layout-Number: 4)
     * <li>Input field and interval domain (Layout-Number: 5)
     * <li>Selection list and input field for characteristics with suggested values (i.e. static domain plus allowsAdditionalValues=T) (Layout-Number: 6)
     * <li>Error: This combination of attributes should never occur. (Layout-Error (number 0)
     * </ol>
     * The following attributes were taken into account:
     * <ul>
     * <li>isDomainConstrained: The values of this cstic are constrained by a dynamic domain. Also if the cstic only has a static domain this returns T. 
     * <li>hasIntervalDomain: whether the characteristic has an interval as domain 
     * <li>allowsMultipleValues: is it allowed to assign more than one value
     * <li>allowsAdditionalValues: The user can enter any value (i.e. also values that were not specified in the domain)
     * <li>hasDomainValues: At least one value of the cstic belongs to the static domain.
     * <li>hasAssignedValues: The user or the system already assigned at least one value to this cstic.
     * </ul>
     * The decision table:
     * <table border="1">
     * <tr><th>isDomainConstrained</th> <th>hasIntervalDomain</th>  <th>allowsMultipleValues</th>   <th>allowsAdditionalValues</th> <th>hasDomainValues</th><th>hasAssignedValues</th>  <th>int</th><th>L1</th>     <th>L2</th> <th>L3</th> <th>L4</th> <th>L5</th> <th>L6</th> <th>L-Error</th> </tr>
     * <tr><td>0</td><td>0</td><td>0</td><td>0</td><td>0</td><td>0</td><td>0 </td><td>&nbsp;</td><td>&nbsp;</td><td>X</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>  
     * <tr><td>0</td><td>0</td><td>0</td><td>0</td><td>0</td><td>1</td><td>1 </td><td>&nbsp;</td><td>&nbsp;</td><td>X</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>  
     * <tr><td>0</td><td>0</td><td>0</td><td>0</td><td>1</td><td>0</td><td>2 </td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>X</td></tr>  
     * <tr><td>0</td><td>0</td><td>0</td><td>0</td><td>1</td><td>1</td><td>3 </td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>X</td></tr>  
     * <tr><td>0</td><td>0</td><td>0</td><td>1</td><td>0</td><td>0</td><td>4 </td><td>&nbsp;</td><td>&nbsp;</td><td>X</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>  
     * <tr><td>0</td><td>0</td><td>0</td><td>1</td><td>0</td><td>1</td><td>5 </td><td>&nbsp;</td><td>&nbsp;</td><td>X</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr> 
     * <tr><td>0</td><td>0</td><td>0</td><td>1</td><td>1</td><td>0</td><td>6 </td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>X</td><td>&nbsp;</td></tr>  
     * <tr><td>0</td><td>0</td><td>0</td><td>1</td><td>1</td><td>1</td><td>7 </td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>X</td><td>&nbsp;</td></tr>  
     * <tr><td>0</td><td>0</td><td>1</td><td>0</td><td>0</td><td>0</td><td>8 </td><td>&nbsp;</td><td>&nbsp;</td><td>X</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>  
     * <tr><td>0</td><td>0</td><td>1</td><td>0</td><td>0</td><td>1</td><td>9 </td><td>&nbsp;</td><td>X</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>  
     * <tr><td>0</td><td>0</td><td>1</td><td>0</td><td>1</td><td>0</td><td>10</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>X</td></tr>  
     * <tr><td>0</td><td>0</td><td>1</td><td>0</td><td>1</td><td>1</td><td>11</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>X</td></tr>  
     * <tr><td>0</td><td>0</td><td>1</td><td>1</td><td>0</td><td>0</td><td>12</td><td>&nbsp;</td><td>&nbsp;</td><td>X</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>  
     * <tr><td>0</td><td>0</td><td>1</td><td>1</td><td>0</td><td>1</td><td>13</td><td>&nbsp;</td><td>X</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>  
     * <tr><td>0</td><td>0</td><td>1</td><td>1</td><td>1</td><td>0</td><td>14</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>X</td><td>&nbsp;</td></tr>  
     * <tr><td>0</td><td>0</td><td>1</td><td>1</td><td>1</td><td>1</td><td>15</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>X</td><td>&nbsp;</td></tr>
     * <tr><td>0</td><td>1</td><td>0</td><td>0</td><td>0</td><td>0</td><td>16</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>X</td></tr>
     * <tr><td>0</td><td>1</td><td>0</td><td>0</td><td>0</td><td>1</td><td>17</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>X</td></tr>
     * <tr><td>0</td><td>1</td><td>0</td><td>0</td><td>1</td><td>0</td><td>18</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>X</td></tr>
     * <tr><td>0</td><td>1</td><td>0</td><td>0</td><td>1</td><td>1</td><td>19</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>X</td></tr>
     * <tr><td>0</td><td>1</td><td>0</td><td>1</td><td>0</td><td>0</td><td>20</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>X</td></tr>
     * <tr><td>0</td><td>1</td><td>0</td><td>1</td><td>0</td><td>1</td><td>21</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>X</td></tr>
     * <tr><td>0</td><td>1</td><td>0</td><td>1</td><td>1</td><td>0</td><td>22</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>X</td></tr>
     * <tr><td>0</td><td>1</td><td>0</td><td>1</td><td>1</td><td>1</td><td>23</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>X</td></tr>
     * <tr><td>0</td><td>1</td><td>1</td><td>0</td><td>0</td><td>0</td><td>24</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>X</td></tr>
     * <tr><td>0</td><td>1</td><td>1</td><td>0</td><td>0</td><td>1</td><td>25</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>X</td></tr>
     * <tr><td>0</td><td>1</td><td>1</td><td>0</td><td>1</td><td>0</td><td>26</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>X</td></tr>
     * <tr><td>0</td><td>1</td><td>1</td><td>0</td><td>1</td><td>1</td><td>27</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>X</td></tr>
     * <tr><td>0</td><td>1</td><td>1</td><td>1</td><td>0</td><td>0</td><td>28</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>X</td><td>&nbsp;</td><td>&nbsp;</td></tr>
     * <tr><td>0</td><td>1</td><td>1</td><td>1</td><td>0</td><td>1</td><td>29</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>X</td><td>&nbsp;</td><td>&nbsp;</td></tr>
     * <tr><td>0</td><td>1</td><td>1</td><td>1</td><td>1</td><td>0</td><td>30</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>X</td><td>&nbsp;</td><td>&nbsp;</td></tr>
     * <tr><td>0</td><td>1</td><td>1</td><td>1</td><td>1</td><td>1</td><td>31</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>X</td><td>&nbsp;</td><td>&nbsp;</td></tr>
     * <tr><td>1</td><td>0</td><td>0</td><td>0</td><td>0</td><td>0</td><td>32</td><td>X</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
     * <tr><td>1</td><td>0</td><td>0</td><td>0</td><td>0</td><td>1</td><td>33</td><td>X</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
     * <tr><td>1</td><td>0</td><td>0</td><td>0</td><td>1</td><td>0</td><td>34</td><td>X</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
     * <tr><td>1</td><td>0</td><td>0</td><td>0</td><td>1</td><td>1</td><td>35</td><td>X</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
     * <tr><td>1</td><td>0</td><td>0</td><td>1</td><td>0</td><td>0</td><td>36</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>X</td></tr>
     * <tr><td>1</td><td>0</td><td>0</td><td>1</td><td>0</td><td>1</td><td>37</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>X</td></tr>
     * <tr><td>1</td><td>0</td><td>0</td><td>1</td><td>1</td><td>0</td><td>38</td><td>X</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
     * <tr><td>1</td><td>0</td><td>0</td><td>1</td><td>1</td><td>1</td><td>39</td><td>X</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
     * <tr><td>1</td><td>0</td><td>1</td><td>0</td><td>0</td><td>0</td><td>40</td><td>X</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
     * <tr><td>1</td><td>0</td><td>1</td><td>0</td><td>0</td><td>1</td><td>41</td><td>X</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
     * <tr><td>1</td><td>0</td><td>1</td><td>0</td><td>1</td><td>0</td><td>42</td><td>X</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
     * <tr><td>1</td><td>0</td><td>1</td><td>0</td><td>1</td><td>1</td><td>43</td><td>X</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
     * <tr><td>1</td><td>0</td><td>1</td><td>1</td><td>0</td><td>0</td><td>44</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>X</td></tr>
     * <tr><td>1</td><td>0</td><td>1</td><td>1</td><td>0</td><td>1</td><td>45</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>X</td></tr>
     * <tr><td>1</td><td>0</td><td>1</td><td>1</td><td>1</td><td>0</td><td>46</td><td>X</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
     * <tr><td>1</td><td>0</td><td>1</td><td>1</td><td>1</td><td>1</td><td>47</td><td>X</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
     * <tr><td>1</td><td>1</td><td>0</td><td>0</td><td>0</td><td>0</td><td>48</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>X</td><td>&nbsp;</td><td>&nbsp;</td></tr>
     * <tr><td>1</td><td>1</td><td>0</td><td>0</td><td>0</td><td>1</td><td>49</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>X</td><td>&nbsp;</td><td>&nbsp;</td></tr>
     * <tr><td>1</td><td>1</td><td>0</td><td>0</td><td>1</td><td>0</td><td>50</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>X</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
     * <tr><td>1</td><td>1</td><td>0</td><td>0</td><td>1</td><td>1</td><td>51</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>X</td><td>&nbsp;</td><td>&nbsp;</td></tr>
     * <tr><td>1</td><td>1</td><td>0</td><td>1</td><td>0</td><td>0</td><td>52</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>X</td><td>&nbsp;</td><td>&nbsp;</td></tr>
     * <tr><td>1</td><td>1</td><td>0</td><td>1</td><td>0</td><td>1</td><td>53</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>X</td><td>&nbsp;</td><td>&nbsp;</td></tr>
     * <tr><td>1</td><td>1</td><td>0</td><td>1</td><td>1</td><td>0</td><td>54</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>X</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
     * <tr><td>1</td><td>1</td><td>0</td><td>1</td><td>1</td><td>1</td><td>55</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>X</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
     * <tr><td>1</td><td>1</td><td>1</td><td>0</td><td>0</td><td>0</td><td>56</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>X</td><td>&nbsp;</td><td>&nbsp;</td></tr>
     * <tr><td>1</td><td>1</td><td>1</td><td>0</td><td>0</td><td>1</td><td>57</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>X</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
     * <tr><td>1</td><td>1</td><td>1</td><td>0</td><td>1</td><td>0</td><td>58</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>X</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
     * <tr><td>1</td><td>1</td><td>1</td><td>0</td><td>1</td><td>1</td><td>59</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>X</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
     * <tr><td>1</td><td>1</td><td>1</td><td>1</td><td>0</td><td>0</td><td>60</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>X</td><td>&nbsp;</td><td>&nbsp;</td></tr>
     * <tr><td>1</td><td>1</td><td>1</td><td>1</td><td>0</td><td>1</td><td>61</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>X</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
     * <tr><td>1</td><td>1</td><td>1</td><td>1</td><td>1</td><td>0</td><td>62</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>X</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
     * <tr><td>1</td><td>1</td><td>1</td><td>1</td><td>1</td><td>1</td><td>63</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>X</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
     * </table>
     * 
     * @return Number of layout
     */
    public int determineLayoutNumber(){
        int layoutNumber = LAYOUT_ERROR;
        int layoutCase = calculateLayoutCase();
        switch (layoutCase) {
            case 32:
            case 33:
            case 34:
            case 35:
            case 38:
            case 39:
            case 40:
            case 41:
            case 42:
            case 43:
            case 46:
            case 47:
                // Layout 1: Selection list only
                layoutNumber = LAYOUT_1;
                break;
            case 9:
            case 13:
                // Layout 2: Selection list and input field
                layoutNumber = LAYOUT_2;
                break;
            case 0:
            case 1:
            case 4:
            case 5:
            case 8:
            case 12:
                // Layout 3: Input field only
                layoutNumber = LAYOUT_3;
                break;
            case 50:
            case 54:
            case 55:
            case 57:
            case 58:
            case 59:
            case 61:
            case 62:
            case 63:
                // Layout 4: Selection list, input field and interval domain
                layoutNumber = LAYOUT_4;
                break;
            case 28:
            case 29:
            case 30:
            case 31:            
            case 48:
            case 49:
            case 51:            
            case 52:
            case 53:
            case 56:
            case 60:
                // Layout 5: Input field and interval domain
                layoutNumber = LAYOUT_5;
                break;
            case 6:
            case 7:
            case 14:            
            case 15:
                // Layout 6: Selection list and input field for characteristics with suggested values (i.e. static domain plus allowsAdditionalValues=T)
                layoutNumber = LAYOUT_6; 
                break;                
            default:
                // Error: This combination of attributes should never occur.
                layoutNumber = LAYOUT_ERROR;
                break;
        }
        return layoutNumber;
    }
    
    /**
     * Calculates an integer value that represents the individual case for which 
     * a layout has to be determined.
     * According to the layout decision table the attributes have the following integer
     * representation:
     * ----------------------------------------------
     * attribute                | binary    | integer
     * ----------------------------------------------
     * isDomainConstrained      | 100000    | 32
     * hasIntervalDomain        | 010000    | 16
     * allowsMultipleValues     | 001000    |  8
     * allowsAdditionalValues   | 000100    |  4
     * hasDomainValues          | 000010    |  2
     * hasAssignedValues        | 000001    |  1
     * ----------------------------------------------
     * @return int value that represents the individual case for which a layout has to be determined. 
     */
	protected int calculateLayoutCase(){
        int layoutCase = 0;
        if (characteristicBO.isDomainConstrained()) {
            layoutCase += 32;
        }
        if (this.hasIntervalAsDomain()) {
            layoutCase += 16;
        }
        if (this.allowsMultipleValues()) {
            layoutCase += 8;
        }
        if (characteristicBO.allowsAdditionalValues()) {
            layoutCase += 4;
        }
        if (this.hasDomainValues()) {
            layoutCase += 2;
        }
        if (this.hasAssignedValues()) {
            layoutCase += 1;
        }
        return layoutCase;
    }

    /**
     * @return the first Mime of type "icon"
     */
    public MimeUIBean getIcon() {
        ArrayList listOfMimes = getMimesByType(MimeUIBean.ICON);
        if (!listOfMimes.isEmpty()) {
            return (MimeUIBean) listOfMimes.get(0);
        }
        else {
            return MimeUIBean.C_NULL;
        }
    }
    
    /**
     * @return the first Mime of type "image"
     */
    public MimeUIBean getImage() {
        ArrayList listOfMimes = getMimesByType(MimeUIBean.IMAGE);
        if (!listOfMimes.isEmpty()) {
            return (MimeUIBean) listOfMimes.get(0);
        }
        else {
            return MimeUIBean.C_NULL;
        }
    }
    
    /**
     * @return the first Mime of type "o2c"
     */
    public MimeUIBean getO2CObject() {
        ArrayList listOfMimes = getMimesByType(MimeUIBean.O2C);
        if (!listOfMimes.isEmpty()) {
            return (MimeUIBean) listOfMimes.get(0);
        }
        else {
            return MimeUIBean.C_NULL;
        }
    }
    
    /**
     * @return the first Mime of type "Application" (e.g. pdf-documents or word-documents, etc.)
     */
    public MimeUIBean getApplicationMime() {
        ArrayList listOfMimes = getMimesByType(MimeUIBean.APPLICATION);
        if (!listOfMimes.isEmpty()) {
            return (MimeUIBean) listOfMimes.get(0);
        }
        else {
            return MimeUIBean.C_NULL;
        }
    }
    

    /**
     * @return the first Mime of type "sound"
     */
    public MimeUIBean getSoundObject() {
        ArrayList listOfMimes = getMimesByType(MimeUIBean.SOUND);
        if (!listOfMimes.isEmpty()) {
            return (MimeUIBean) listOfMimes.get(0);
        }
        else {
            return MimeUIBean.C_NULL;
        }
    }
    
   
    /**
     * @return the first Mime of type "unknown"
     */
    public MimeUIBean getUnknownMime() {
        ArrayList listOfMimes = getMimesByType(MimeUIBean.UNKNOWN);
        if (!listOfMimes.isEmpty()) {
            return (MimeUIBean) listOfMimes.get(0);
        }
        else {
            return MimeUIBean.C_NULL;
        }
    }

    /**
     * Returns the cstic-mime for the work-area.
     * In the work-area (list of cstics and values) not all mimes can be included.
     * Only the following types will be included directly in the work-area:
     * <li>Icon
     * <li>O2C
     * <li>Sound
     * 
     * Beside this another rule comes into play: If one of the values of this characteristic
     * has also a mime, the mime of the cstic itself will NOT be displayed. Instead the mimes for
     * the values will be displayed.
     * Motivation: it is assumed that as soon as value-mimes were added to the cstic these mimes 
     * have a higher level of detail then the general mime of the cstic. If a customer wants to
     * change this he can do it by overwriting this method. 
     * 
     * Exception: If the characteristic is collapsed then the cstic-mime will be displayed although
     * value-mimes are available. Reason: In this case the value mimes can't be displayed (drop-down-box)
     * and therefore there is space for the cstic-mime.
     * 
     * This method returns the appropriate mime in the above order (i.e. first it checks
     * for an icon, if this is not available it will try to return an O2C-mime, and so on) and
     * checks whether any value of this characteristic has also a mime (if it is the case the 
     * mime of the cstic will not be displayed (but see also exception above)).
     * 
     * @param is characteristic expanded
     * @return the mime for the work-area
     */
    public MimeUIBean getMimeForWorkArea(boolean expanded) {
        MimeUIBean mimeForWorkArea = MimeUIBean.C_NULL;
        // check whether value-mimes exist for this cstic (see explanation of exception above)
        if (this.hasValueMimesForWorkArea() && expanded) {
            return mimeForWorkArea;
        }
        if (this.hasMimesForWorkArea()) {
            mimeForWorkArea = getIcon();
            if (mimeForWorkArea == MimeUIBean.C_NULL) {
                mimeForWorkArea = getO2CObject();
                if (mimeForWorkArea == MimeUIBean.C_NULL) {
                    mimeForWorkArea = getSoundObject();
                }
            }
        }
        return mimeForWorkArea; 
    }

    /**
     * In the worka-area (list of cstics and values) not all Mimes can be included.
     * Only the following types will be included directly in the work-area:
     * <li>Icon
     * <li>O2C
     * <li>Sound
     * If the characteristic has at least one of these Mimes the method will return true.
     * 
     * @return true if the characteristic has at least one Mime for the work area
     */
    public boolean hasMimesForWorkArea() {
        return this.mimesForWorkArea; 
    }

    /**
     * In the worka-area (list of cstics and values) not all Mimes can be included.
     * Only the following types will be included directly in the work-area:
     * <li>Icon
     * <li>O2C
     * <li>Sound
     * If at least one value of the characteristic has one of these Mimes the method will return true.
     * 
     * @return true if at least one value of this characteristic has Mimes for the work area. 
     */
    public boolean hasValueMimesForWorkArea() {
        return this.valueMimesForWorkArea; 
    }
    
    
    /**
     * Returns the link (URL) to an additional mime of this characteristic.
     * 
     * Only the following types are taken into account for the additional mime link determination 
     * <li>Application
     * <li>Unknown
     * 
     * This method returns the appropriate link in the above order (i.e. first it checks
     * for a mime with type "Application" (e.g. pdf or doc), if this is not available it will 
     * try to return a link to a mime of type "Unknown".
     * If there is no additional mime for this characteristic. This method will return
     * an empty string ("");
     *
     * @return URL to the additional mime, empty string if no additional mime is available
     */
    public String getLinkToAdditionalMime() {
        String linkToAdditionalMime = "";
        MimeUIBean additionalMime = MimeUIBean.C_NULL;
        if (this.hasAdditionalMimes()) {
            additionalMime = getApplicationMime();
            if (additionalMime == MimeUIBean.C_NULL) {
                additionalMime = getUnknownMime();
            }
            linkToAdditionalMime = additionalMime.getURL();
        }
        return linkToAdditionalMime;
    }
    
     /**     
     * Additional Mimes are from type: 
     * <li>Application
     * <li>Unknown
     * If the characteristic has at least one of these Mimes the method will return true.
     * 
     * @return true if the characteristic has at least one additional mime
     */
    public boolean hasAdditionalMimes() {
        return this.additionalMimes; 
    }   

    /**
     * Returns the enlarged Mime object. 
     * The object that is return is calculated as follows:
     * It tries to return the first Mime of type "Image". If this was not possible it returns
     * the first Mime of type "Icon" (i.e. the icon itself is returned as Enlarged Mime if there 
     * is no Mime of type "Image"). 
     * 
     * @return MimeUIBean for enlarged Mime
     */
    public MimeUIBean getEnlargedMime() {
        MimeUIBean enlargedMime = getImage();
        if (enlargedMime == MimeUIBean.C_NULL) {
            enlargedMime = getIcon();
        }
        return enlargedMime;
    }

    public String toString() {
        StringBuffer sb = new StringBuffer();
        sb.append("CharacteristicUIBean <NAME>");
        sb.append(getName());
        sb.append("</NAME><VAL-ERROR>");
        sb.append(getValidationError().toString());
        sb.append("</VAL-ERROR><HAS-VALUE-MIMES>");
        sb.append(hasValueMimesForWorkArea());
        sb.append("</HAS-VALUE-MIMES><HAS-MIMES>");
        sb.append(hasMimesForWorkArea());
        sb.append("</HAS-MIMES>");
        
        return sb.toString();
        
    }
    /**
     * @return true if the cstic is a message-cstic
     */
    public boolean isMessageCstic() {
        return messageCstic;
    }

    /**
     * @param b flag that indicates whether this cstic is a message-cstic
     */
    public void setMessageCstic(boolean b) {
        messageCstic = b;
    }

	/* (non-Javadoc)
	 * @see com.sap.isa.ipc.ui.jsp.action.visitor.IVisitableObject#accept(com.sap.isa.ipc.ui.jsp.action.visitor.IVisitor)
	 */
	public void accept(IVisitor visitor) {
		visitor.visit(this);
	}

	public String getGroupName(){
		String groupName = "";
		if (characteristicBO != null){
			CharacteristicGroup group = characteristicBO.getGroup();
			if (group != null){
				groupName = group.getName();
			}
		}
		return groupName;
	}

	
	public String getInstanceId(){
		String instId = "";
		if (characteristicBO != null){
			Instance inst = characteristicBO.getInstance();
			if (inst != null){
				instId = inst.getId();
			}
		}
		return instId;
	}

	public int getCharacteristicGroupPosition() {
		return characteristicBO.getCharacteristicGroupPosition();
	}

	public int getCharacteristicInstancePosition() {
		return characteristicBO.getCharacteristicInstancePosition();
	}

}