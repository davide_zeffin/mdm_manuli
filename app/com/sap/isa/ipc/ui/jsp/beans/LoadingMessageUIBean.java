package com.sap.isa.ipc.ui.jsp.beans;

import java.util.ArrayList;

import com.sap.isa.core.util.JspUtil;
import com.sap.isa.ipc.ui.jsp.uiclass.UIContext;
import com.sap.spc.remote.client.object.LoadingMessage;

/**
 * UIBean for LoadingMessages.
 */
public class LoadingMessageUIBean {

    private LoadingMessage loadingMessageBO;
    private UIContext uiContext;
    private String toolTip;
    private ArrayList oldValues;
    
    protected LoadingMessageUIBean(LoadingMessage bo, UIContext uic) {
        this.loadingMessageBO = bo;
        this.uiContext = uic;
    }
    
    /**
     * @param bo LoadingMessage businesss object for this bean.
     * @param uic UIContext
     * @param tt A tooltip that should be displayed additionally to the message text.
     */
    protected LoadingMessageUIBean(LoadingMessage bo, UIContext uic, String tt) {
        this.loadingMessageBO = bo;
        this.uiContext = uic;
        this.toolTip = tt;
    }    
    
    /**
     * In order to give customers the possibility access business objects in JSP pages.
     * This is useful especially for customer extension data of business objects.
     * @return Loading Message business object
     */
    public LoadingMessage getBusinessObject() {
        return loadingMessageBO;
    }
    
    /* (non-Javadoc)
     * @see com.sap.spc.remote.client.object.LoadingMessage#getConfigObjKey()
     */
    public String getConfigObjKey() {
        return loadingMessageBO.getConfigObjKey();
    }

    /* (non-Javadoc)
     * @see com.sap.spc.remote.client.object.LoadingMessage#getInstId()
     */
    public String getInstId() {
        return loadingMessageBO.getInstId();
    }


    /* (non-Javadoc)
     * @see com.sap.spc.remote.client.object.LoadingMessage#getNumber()
     */
    public int getNumber() {
        return loadingMessageBO.getNumber();
    }

    /* (non-Javadoc)
     * @see com.sap.spc.remote.client.object.LoadingMessage#getSeverity()
     */
    public int getSeverity() {
        return loadingMessageBO.getSeverity();
    }

    /* (non-Javadoc)
     * @see com.sap.spc.remote.client.object.LoadingMessage#isErrorMessage()
     */
    public boolean isErrorMessage() {
        return loadingMessageBO.isErrorMessage();
    }

    /* (non-Javadoc)
     * @see com.sap.spc.remote.client.object.LoadingMessage#isWarningMessage()
     */
    public boolean isWarningMessage() {
        return loadingMessageBO.isWarningMessage();
    }

    /* (non-Javadoc)
     * @see com.sap.spc.remote.client.object.LoadingMessage#isInfoMessage()
     */
    public boolean isInfoMessage() {
        return loadingMessageBO.isInfoMessage();
    }

    /* (non-Javadoc)
     * @see com.sap.spc.remote.client.object.LoadingMessage#getText()
     */
    public String getText() {
        return JspUtil.replaceSpecialCharacters(loadingMessageBO.getText());
    }

    /* (non-Javadoc)
     * @see com.sap.spc.remote.client.object.LoadingMessage#getArguments()
     */
    public String[] getArguments() {
        return loadingMessageBO.getArguments();
    }    

    /**
     * @return A tooltip that should be displayed additionally to the message text.
     */
    public String getToolTip(){
        return toolTip;
    }

    public StatusImage getStatusImage() {
        // At the moment we only return the red LED for loading messages because
        // we only have one type of loading messages.
        String sourceFile = "ipc/mimes/images/sys/w_s_ledr.gif";
        String statusDescription = "ipc.compare.tt.icon.loadingmsg";

        if (isInfoMessage()) {
            statusDescription = "ipc.compare.tt.icon.loadingmsg";
        }

        if (isWarningMessage()) {
            statusDescription = "ipc.compare.tt.icon.loadingmsg";
        }
        if (isErrorMessage()) {
            statusDescription = "ipc.compare.tt.icon.loadingmsg";
        }
        return UIBeanFactory.getUIBeanFactory().newStatusImage(statusDescription, sourceFile);
    }
    
    /**
     * @return Old values (e.g. of a deleted cstic) if available.
     */
    public ArrayList getOldValues() {
        return oldValues;
    }

    /**
     * @param list List with old values (e.g. of a deleted cstic).
     */
    public void setOldValues(ArrayList list) {
        oldValues = list;
    }

}
