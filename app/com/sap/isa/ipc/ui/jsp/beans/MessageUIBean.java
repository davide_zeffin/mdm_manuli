package com.sap.isa.ipc.ui.jsp.beans;

import java.util.ArrayList;
import java.util.List;

/**
 * @author C5042316
 *
 * To change the template for this generated type comment go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
public class MessageUIBean {

    public static final String SUCCESS = "success";
    public static final String WARNING = "warning";
    public static final String ERROR = "error";
	protected String messageShortText;
	protected String messageLongText;
	protected String messageStatus;
	protected String messageTranslateKey;
	protected List messageActions;
	protected String[] messageArguments;
    
    protected MessageUIBean(String status, String shortText){
        this.messageStatus = status;
        this.messageShortText = shortText;
        this.messageLongText = "";
        this.messageTranslateKey = "";
        this.messageArguments = new String[0];
    }
    
    protected MessageUIBean(){
        this.messageShortText = "";
        this.messageStatus = "";
        this.messageTranslateKey = "";
        this.messageArguments = new String[0];
    }
    
    protected MessageUIBean(String messageTranslateKey) {
    	this.messageShortText = "";
    	this.messageStatus = "";
		this.messageTranslateKey = messageTranslateKey;
        this.messageArguments = new String[0];
    }

    protected MessageUIBean(String messageTranslateKey, String[] messageArguments) {
        this.messageShortText = "";
        this.messageStatus = "";
        this.messageTranslateKey = messageTranslateKey;
        this.messageArguments = messageArguments;
    }

    protected MessageUIBean(String status, String shortText, String messageTranslateKey, String[] messageArguments) {
        this.messageShortText = shortText;
        this.messageStatus = status;
        this.messageTranslateKey = messageTranslateKey;
        this.messageArguments = messageArguments;
    }
    
    public void setMessageTranslateKey(String translateKey){
        this.messageTranslateKey = translateKey;
    }
    
    public String getMessageTranslateKey (){
        return this.messageTranslateKey;
    }
    
    public void setMessageStatus (String status){
        this.messageStatus = status; 
    }
    
    public String getMessageStatus (){
        return this.messageStatus;
    }
    
    public void setMessageShortText (String shortText){
        this.messageShortText = shortText;
    }
    
    public String getMessageShortText (){
        return this.messageShortText;
    }
    
    public void setMessageLongText(String longText){
        this.messageLongText = longText;        
    }
    
    public String getMessageLongText(){
        return this.messageLongText;
    }
    public StatusImage getStatusImage() {
        String sourceFile = "ipc/mimes/images/sys/ico12_success.gif";
        String statusDescription = "ipc.message.tt.success";

        if (messageStatus.equals(SUCCESS)) {
            sourceFile = "ipc/mimes/images/sys/ico12_success.gif";
            statusDescription = "ipc.message.tt.success";
        }

        if (messageStatus.equals(WARNING)) {
            sourceFile = "ipc/mimes/images/sys/ico12_warning.gif";
            statusDescription = "ipc.message.tt.warning";
        }
        if (messageStatus.equals(ERROR)) {
            sourceFile = "ipc/mimes/images/sys/ico12_error.gif";
            statusDescription = "ipc.message.tt.error";
        }

        return UIBeanFactory.getUIBeanFactory().newStatusImage(statusDescription, sourceFile);
    }
    
    public void addAction(MessageAction action) {
    	if (messageActions == null) {
    		messageActions = new ArrayList();
    	}
    	messageActions.add(action);
    }
    
    /**
     * 
     * @return the List of message actions or an empty List.
     */
    public List getActions() {
		if (messageActions == null) {
			messageActions = new ArrayList();
		}
    	return messageActions;
    }

    public void setMessageArguments(String[] messageArguments){
        this.messageArguments = messageArguments;
    }


    public String[] getMessageArguments(){
        return messageArguments;
    }

    //messages may contain some links to actions to react on the message.
    public class MessageAction {
    	String key;
    	String action;
    	String actionUrl;
    	
    	public MessageAction(String key, String action, String actionUrl) {
    		this.key = key;
    		this.action = action;
    		this.actionUrl = actionUrl;
    	}
		/**
		 * @return
		 */
		public String getAction() {
			return action;
		}

		/**
		 * @return
		 */
		public String getKey() {
			return key;
		}

		/**
		 * @return
		 */
		public String getActionUrl() {
			return actionUrl;
		}

    }
	/**
	 * @param key
	 * @param action
	 */
	public void addAction(String key, String action, String actionUrl) {
		MessageAction mAction = new MessageAction(key, action, actionUrl);
		addAction(mAction);
		
	}
}
