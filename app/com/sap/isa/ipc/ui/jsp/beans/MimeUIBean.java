/*
 * Created on 06.08.2004
 *
 * To change the template for this generated file go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
package com.sap.isa.ipc.ui.jsp.beans;

import com.sap.isa.ipc.ui.jsp.action.visitor.IVisitableObject;
import com.sap.isa.ipc.ui.jsp.action.visitor.IVisitor;
import com.sap.isa.ipc.ui.jsp.uiclass.UIContext;
import com.sap.spc.remote.client.object.MimeObject;
import com.sap.spc.remote.client.object.imp.DefaultMimeObject;

/**
 * @author 
 *
 * To change the template for this generated type comment go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
public class MimeUIBean implements IVisitableObject{
    
	protected MimeObject mimeBO;
	protected UIContext uiContext;
    public static final MimeUIBean C_NULL = new MimeUIBean(DefaultMimeObject.C_NULL, new UIContext());
    
    //constants of the business object
    public static final String UNKNOWN         = MimeObject.UNKNOWN;
    public static final String O2C             = MimeObject.O2C;
    public static final String IMAGE           = MimeObject.IMAGE;
    public static final String ICON            = MimeObject.ICON;
    public static final String SOUND           = MimeObject.SOUND;
    public static final String APPLICATION     = MimeObject.APPLICATION;
    //public static final String ACROBAT_READER  = MimeObject.ACROBAT_READER; // not supported by Engine, "Application" is used instead
    //public static final String DOC             = MimeObject.DOC; // not supported by Engine, "Application" is used instead
    public static final String MIME_URL        = MimeObject.MIME_URL;
    public static final String MIME_TYPE       = MimeObject.MIME_TYPE;
    public static final String MIME_PARAM_NAME = MimeObject.MIME_PARAM_NAME;
    public static final String MIME_PARAM_VALUE= MimeObject.MIME_PARAM_VALUE;
    public static final String WIDTH           = MimeObject.WIDTH;
	public static final String ICON_DEFAULT_WIDTH = "70";
    public static final String HEIGHT          = MimeObject.HEIGHT;
	public static final String ICON_DEFAULT_HEIGHT = "50";
    public static final String IS_ENLARGED     = MimeObject.IS_ENLARGED;
    public static final String YES             = MimeObject.YES;
    public static final String NO              = MimeObject.NO;
    
    protected MimeUIBean(MimeObject mimeBO, UIContext uiContext) {
        this.mimeBO = mimeBO;
        this.uiContext = uiContext;
    }

	/**
	 * In order to give customers the possibility access business objects in JSP pages.
	 * This is useful especially for customer extension data of business objects.
	 * @return
	 */
    public MimeObject getBusinessObject() {
    	return mimeBO;
    }
    
    public static void main(String[] args) {
    }
    /* (non-Javadoc)
     * @see java.lang.Object#equals(java.lang.Object)
     */
    public boolean equals(Object arg0) {
        return mimeBO.equals(arg0);
    }

    /**
     * @param paramName
     * @return
     */
    public String getParameter(String paramName) {
        return mimeBO.getParameter(paramName);
    }

    /**
     * @param paramName
     * @param defaultValue
     * @return
     */
    public String getParameter(String paramName, String defaultValue) {
        return mimeBO.getParameter(paramName, defaultValue);
    }

    /**
     * @return
     */
    public String getType() {
        return mimeBO.getType();
    }

    /**
     * @return
     */
    public String getURL() {
        return mimeBO.getURL();
    }

    /**
     * @param index
     * @return
     */
    public String getURLEncodedRepresentation(int index) {
        return mimeBO.getURLEncodedRepresentation(index);
    }

    /* (non-Javadoc)
     * @see java.lang.Object#hashCode()
     */
    public int hashCode() {
        return mimeBO.hashCode();
    }

    /**
     * @param paramName
     * @param paramValue
     */
    public void setParameter(String paramName, String paramValue) {
        mimeBO.setParameter(paramName, paramValue);
    }
    


    /* (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    public String toString() {
        StringBuffer sb = new StringBuffer();
        sb.append("MimeUIBean <TYPE>");
        sb.append(getType());
        sb.append("</TYPE><URL>");
        sb.append(getURL());
        sb.append("</URL>");
        return sb.toString();
    }

	/* (non-Javadoc)
	 * @see com.sap.isa.ipc.ui.jsp.action.visitor.IVisitableObject#accept(com.sap.isa.ipc.ui.jsp.action.visitor.IVisitor)
	 */
	public void accept(IVisitor visitor) {
		visitor.visit(this);
	}

}
