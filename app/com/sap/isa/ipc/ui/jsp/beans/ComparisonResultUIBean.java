package com.sap.isa.ipc.ui.jsp.beans;

import java.text.DateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.sap.isa.ipc.ui.jsp.action.InternalRequestParameterConstants;
import com.sap.isa.ipc.ui.jsp.uiclass.UIContext;
import com.sap.spc.remote.client.object.ComparisonResult;
import com.sap.spc.remote.client.object.ConfigurationSnapshot;

/**
 * Wrapper around the ComparisonResult that is specialized for HTML usage.
 */
public class ComparisonResultUIBean {

    private ComparisonResult resultBO;
    private UIContext uiContext;
    private ArrayList instDeltas;
    private ArrayList loadingMessages;
    private boolean anyLoadingMessages;
    private boolean anyValueDeltas;

    /**
     * Creates ComparisonResultUIBean
     * @param resultBO business object of the comparison result
     * @param uiContext
     */
    protected ComparisonResultUIBean(ComparisonResult resultBO, UIContext uiContext) {
        this.resultBO = resultBO;
        this.uiContext = uiContext;
        this.instDeltas = new ArrayList();
        this.loadingMessages = new ArrayList();
    }

    /**
     * Add an instance delta to the result.
     * @param instDelta
     */
    public void addInstanceDelta(InstanceUIBean instDelta){
        instDeltas.add(instDelta);
    }

    /**
     * Add an instance delta add specific position to the result.
     * @param instDelta
     */
    public void addInstanceDelta(InstanceUIBean instDelta, int position){
        instDeltas.add(position, instDelta);
    }

    
    /**
     * @return list of instance deltas
     */
    public ArrayList getInstanceDeltas(){
        return instDeltas;
    }
    
    /**
     * @return true if the result has instance deltas
     */
    public boolean hasInstanceDeltas(){
        return (instDeltas.size()>0);
    }    

    /**
     * @return true if the result has header deltas
     */
    public boolean hasHeaderDeltas(){
        return (resultBO.getHeaderDeltas().size()>0);
    }    

    /**
     * @return true if there have been errors during the comparison
     */
    public boolean hasErrors(){
        return resultBO.hasErrorsDuringComparison();
    }
        
    /**
     * Returns a list of errors that occured during comparison.
     * Null is returned if there are no errors.
     * @return list of errors that occured during comparison
     */
    public List getErrors(){
        return resultBO.getComparisonErrors();
    }
    
    /**
     * Returns the list of deleted instances.
     * These instance deltas are also part of the <code>instanceDeltas</code> list.
     * @return list of deleted instances
     */
    public List getDeletedInstances(){
        return resultBO.getDeletedInstances();
    }

    /**
     * @return true if the result has deleted deltas
     */
    public boolean hasDeletedInstance(){
        return (resultBO.getDeletedInstances().size()>0);
    }    

    /**
     * @return the business object of this result
     */
    public ComparisonResult getBusinessObject() {
        return resultBO;
    }
    
    /**
     * @return true if the comparison has differences
     */
    public boolean isDifferent(){
        return resultBO.isDifferent();
    }

    /**
     * @return list of header deltas
     */
    public List getHeaderDeltas(){
        return resultBO.getHeaderDeltas();
    }
    
    /**
     * @return business object of snapshot one.
     */
    public ConfigurationSnapshot getSnap1BO(){
        return (ConfigurationSnapshot)uiContext.getProperty(InternalRequestParameterConstants.COMPARISON_SNAP1);
    }

    /**
     * @return business object of snapshot two.
     */
    public ConfigurationSnapshot getSnap2BO(){
        return (ConfigurationSnapshot)uiContext.getProperty(InternalRequestParameterConstants.COMPARISON_SNAP2);
    }
    
    /**
     * @return the internal name of snapshot one or an empty string if not maintained or snapshot1 is null.
     */
    public String getSnap1Name(){
        String snap1Name = "";
        ConfigurationSnapshot snap = (ConfigurationSnapshot)uiContext.getProperty(InternalRequestParameterConstants.COMPARISON_SNAP1);
        if (snap != null){
            snap1Name = snap.getName();
        }
        return snap1Name; 
    }

    /**
     * @return the internal name of snapshot two or an empty string if not maintained or snapshot2 is null.
     */
    public String getSnap2Name(){
        String snap2Name = "";
        ConfigurationSnapshot snap = (ConfigurationSnapshot)uiContext.getProperty(InternalRequestParameterConstants.COMPARISON_SNAP2);
        if (snap != null){
            snap2Name = snap.getName();
        }
        return snap2Name; 
    }

    /**
     * @return the time of snapshot 1 (language depedent)
     */
    public String getSnap1Time(){
        String snap1Time = "";
        ConfigurationSnapshot snap = (ConfigurationSnapshot)uiContext.getProperty(InternalRequestParameterConstants.COMPARISON_SNAP1);        
        if (snap != null){
            long ts = snap.getTimestamp();
            snap1Time = DateFormat.getTimeInstance(DateFormat.MEDIUM, uiContext.getLocale()).format(new Date(ts));
        }
        return snap1Time;
    }

    /**
     * @return the price of snapshot one or an empty string if not maintained or snapshot1 is null.
     */
    public String getSnap1Price(){
        String snap1Price = "";
        ConfigurationSnapshot snap = (ConfigurationSnapshot)uiContext.getProperty(InternalRequestParameterConstants.COMPARISON_SNAP1);
        if (snap != null){
            snap1Price = snap.getPrice();
        }
        return snap1Price; 
    }

    /**
     * @return the price of snapshot two or an empty string if not maintained or snapshot2 is null.
     */
    public String getSnap2Price(){
        String snap2Price = "";
        ConfigurationSnapshot snap = (ConfigurationSnapshot)uiContext.getProperty(InternalRequestParameterConstants.COMPARISON_SNAP2);
        if (snap != null){
            snap2Price = snap.getPrice();
        }
        return snap2Price; 
    }

    /**
     * @return the time of snapshot 2 (language depedent)
     */
    public String getSnap2Time(){
        String snap2Time = "";
        ConfigurationSnapshot snap = (ConfigurationSnapshot)uiContext.getProperty(InternalRequestParameterConstants.COMPARISON_SNAP2);        
        if (snap != null){
            long ts = snap.getTimestamp();
            snap2Time = DateFormat.getTimeInstance(DateFormat.MEDIUM, uiContext.getLocale()).format(new Date(ts));
        }
        return snap2Time;
    }
    
    /**
     * Adds a LoadingMessageUIBean to the resultUIBean that could not be associated to 
     * an instance or characteristic delta.
     * @param msg LoadingMessagesUIBean
     */
    public void addLoadingMessage(LoadingMessageUIBean msg){
        setAnyLoadingMessagesFlag(true);
        loadingMessages.add(msg);
    }

    /**
     * @return true if this result bean has general loading messages
     */
    public boolean hasLoadingMessages(){
        return (loadingMessages.size() > 0);
    }    
    
    /**
     * Returns a list of loading messages for this result bean.<br>
     * These message could not be associated to any other configuration object.<br>
     * If there are no loading messages it will return an empty list.
     * @return list of loading messages
     */
    public ArrayList getLoadingMessages(){
        return loadingMessages;
    }
    
    /**
     * @return true if either one of the instances or one of the cstics or the result 
     * itself has a loading message (has to be set manually!) 
     */
    public boolean hasAnyLoadingMessages() {
        return anyLoadingMessages;
    }

    /**
     * @param flag set to true if either one of the instances or one of the cstics or the result 
     * itself has a loading message
     */
    public void setAnyLoadingMessagesFlag(boolean flag) {
        anyLoadingMessages = flag;
    }

    /**
     * True if if at least one of the instances has value deltas.<br>
     * This information is different from isDifferent(). hasAnyValueDeltas() returns true
     * if value deltas exist but isDifferent() returns also true if only header deltas 
     * (e.g. quantity change) exists.  
     * @return true if at least one of the instances has value deltas (has to be set manually!) 
     */
    public boolean hasAnyValueDeltas() {
        return anyValueDeltas;
    }

    /**
     * @param flag set to true if either one of the instances or one of the cstics or the result 
     * itself has a loading message
     */
    public void setAnyValueDeltasFlag(boolean flag) {
        anyValueDeltas = flag;
    }
  
    public String toString(){
        StringBuffer sb = new StringBuffer();
        sb.append("ComparisonResultUIBean <IS-DIFFERENT>");
        sb.append(this.isDifferent());
        sb.append("</IS-DIFFERENT>\n");
        sb.append("<HAS-INSTANCE-DELTAS>");
        sb.append(this.hasInstanceDeltas());
        sb.append("</HAS-INSTANCE-DELTAS>\n");       
        sb.append("<HAS-LOADING-MESSAGES>");
        sb.append(this.hasLoadingMessages());
        sb.append("</HAS-LOADING-MESSAGES>\n");
        sb.append("<HAS-ANY-LOADING-MESSAGE>");
        sb.append(this.hasAnyLoadingMessages());
        sb.append("</HAS-ANY-LOADING-MESSAGE>\n");
        sb.append("<HAS-DELETED-INSTANCES>");
        sb.append(this.hasDeletedInstance());
        sb.append("</HAS-DELETED-INSTANCES>\n");
        return sb.toString();
    }     

}
