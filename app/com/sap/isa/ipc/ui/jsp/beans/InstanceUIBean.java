/*
 * Created on 09.07.2004
 */
package com.sap.isa.ipc.ui.jsp.beans;

import java.text.DecimalFormat;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.TreeSet;

import com.sap.isa.core.util.JspUtil;
import com.sap.isa.ipc.ui.jsp.action.visitor.IVisitableObject;
import com.sap.isa.ipc.ui.jsp.action.visitor.IVisitor;
import com.sap.isa.ipc.ui.jsp.container.ConditionUIBeanSet;
import com.sap.isa.ipc.ui.jsp.uiclass.UIContext;
import com.sap.isa.ipc.ui.jsp.util.StringUtil;
import com.sap.spc.remote.client.object.Instance;
import com.sap.spc.remote.client.object.CharacteristicGroup;
import com.sap.spc.remote.client.object.MimeObject;
import com.sap.spc.remote.client.object.MimeObjectContainer;

/**
 * @author 
 *
 * To change the template for this generated type comment go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
public class InstanceUIBean implements IVisitableObject{
	protected ConditionUIBeanSet conditions;
	protected Instance instanceBO;
	protected UIContext uiContext;
	protected ArrayList children;
	protected InstanceUIBean parent;
    
	protected boolean selected;
	protected boolean expanded;
	protected boolean closed;
	protected Boolean assignedValues;
	protected ArrayList mimes = new ArrayList();
	protected HashMap mimesByType = new HashMap();    
    protected ArrayList groups = new ArrayList();
	protected Hashtable groupsByName = new Hashtable();
    protected ArrayList characteristics = new ArrayList();
	protected Hashtable characteristicsByName = new Hashtable();
	protected boolean anyMimesForWorkArea = false;

    
    protected InstanceUIBean(Instance instanceBO, UIContext uiContext) {
        this.instanceBO = instanceBO;
        this.uiContext = uiContext;
        this.parent = null;
        if (instanceBO == null){
            this.children = new ArrayList();
        }
        else {
            this.children = new ArrayList(instanceBO.getChildren().size());
        }
        this.selected = false;
        this.closed = false;
        if (instanceBO != null) {
            MimeObjectContainer mimeObjectContainer = this.instanceBO.getMimeObjects();
            // generate mimeUIBeans only if necessary
            if (mimeObjectContainer != null) {
                Iterator mimeBOList = mimeObjectContainer.getMimeObjects().iterator();
                while (mimeBOList.hasNext()) {
                    MimeObject mimeBO = (MimeObject) mimeBOList.next();
                    MimeUIBean mimeBean = UIBeanFactory.getUIBeanFactory().newMimeUIBean(mimeBO, this.uiContext);
                    addMime(mimeBean); 
                }
            }
        }        
    }
    
    public String getId() {
        return instanceBO.getId();
    }
    
    public void addChild(InstanceUIBean child) {
        children.add(child);
    }
    
    public List getChildren() {
        return children;
    }
    
    public void setParent(InstanceUIBean parent) {
        this.parent = parent;
    }
    public InstanceUIBean getParent() {
        return parent;
    }

    /**
     * @return
     */
    public boolean isSelected() {
        return selected;
    }

    /**
     * @param b
     */
    public void setSelected(boolean b) {
        selected = b;
    }
    
	/**
	 * In order to give customers the possibility access business objects in JSP pages.
	 * This is useful especially for customer extension data of business objects.
	 * @return
	 */
    public Instance getBusinessObject() {
        return this.instanceBO;
    }

    /**
     * @return
     */
    public boolean isClosed() {
        return closed;
    }

    /**
     * @param b
     */
    public void setClosed(boolean b) {
        closed = b;
    }
    
    public String getStyleClass() {
        if( !uiContext.getShowStatusLights() && !instanceBO.isConsistent() )
        {
            return "ipcConflict";
        }

        else
        {
            return "";
        }        
    }
    
	public String getStyleTooltip() {
		if( !uiContext.getShowStatusLights() && !instanceBO.isConsistent() )
		{
			return "ipc.tt.conflict";
		}

		else
		{
			return "ipc.tt.no_conflict";
		}        
	}
    
    public boolean hasVisibleCharacteristics() {
        if( instanceBO.getCharacteristics(uiContext.getShowInvisibleCharacteristics()).size() > 0 ) {
            return true;        
        }else{
            return false;
        }
    }
    
	/**
	 * a b c x
	 * 0 0 0 0
	 * 0 0 1 0
	 * 0 1 0 0
	 * 0 1 1 1
	 * 1 0 0 0
	 * 1 0 1 1
	 * 1 1 0 0
	 * 1 1 1 1
	 * a = uiContext.getShowInvisibleCharacteristics()
	 * b = cstic.isVisible()
	 * c = cstic.hasAssignedValues()
	 * x = return
	 * @return
	 */
	public boolean hasAssignedValues() {
		if (assignedValues == null) {
			for (Iterator csticIt = getCharacteristics().iterator();
				csticIt.hasNext();
				) {
				CharacteristicUIBean cstic =
					(CharacteristicUIBean) csticIt.next();
				if (uiContext.getShowInvisibleCharacteristics()
					&& cstic.hasAssignedValues()
					|| cstic.isVisible()
					&& cstic.hasAssignedValues()) {
					assignedValues = Boolean.TRUE;
					return true;
				}
			}
			assignedValues = Boolean.FALSE;
			return false;
		}else {
		    return assignedValues.booleanValue();
		}
	}
    
    
    public String getLanguageDependentNameFallback() {
        if (uiContext.getShowLanguageDependentNames()
            && instanceBO.getLanguageDependentName() != null) {
            return JspUtil.replaceSpecialCharacters(
                instanceBO.getLanguageDependentName());

        } else {

            return StringUtil.encodeLanguageIndependentName(
                instanceBO.getName(),
                uiContext);

        }
    }
    
	/**
	 * Get language dependent name. Read from IPC item if available.
	 */
	public String getLanguageDependentName() {
		
		if ( instanceBO.getIpcItem() != null && isRootInstance()){
			if (uiContext.getShowLanguageDependentNames()
				&& instanceBO.getIpcItem().getProductDescription() != null) {
				String languageDependentName =  JspUtil.replaceSpecialCharacters(instanceBO.getIpcItem().getProductDescription());
				if ( languageDependentName != null && !languageDependentName.equals(""))
				return languageDependentName;
				else return getLanguageDependentNameFallback();

			} else {

				return StringUtil.encodeLanguageIndependentName(
					getName(),
					uiContext);
			}
			
		}
		else return getLanguageDependentNameFallback();
				
	}	
	
	public String getDescription() {
		if (uiContext.getShowLanguageDependentNames()
		    && instanceBO.getDescription() != null) {
		    	return StringUtil.convertStringToHtml(JspUtil.replaceSpecialCharacters(
		    	    instanceBO.getDescription()));
		} else if (uiContext.getShowLanguageDependentNames()
			&& instanceBO.getLanguageDependentName() != null) {
			return JspUtil.replaceSpecialCharacters(
				instanceBO.getLanguageDependentName());

		} else {

			return StringUtil.encodeLanguageIndependentName(
				instanceBO.getName(),
				uiContext);

		}
	}
	
	/**
	 * Get name. Read from IPC item if available.
	 */
	public String getName() {
		if ( instanceBO.getIpcItem() != null){
			String name = instanceBO.getIpcItem().getProductId();
			if (name != null && !name.equals("")){
				return name;			
			}
			else return instanceBO.getName();
		}
		else return instanceBO.getName();
	}	
    
    public StatusImage getStatusImage() {
        String statusText = "ipc.config.complete";
        String sourceFile = "ipc/mimes/images/table/dot_green.gif";

        if (!instanceBO.isComplete()) {
            sourceFile = "ipc/mimes/images/table/dot_yellow.gif";
            statusText = "ipc.config.incomplete"; 
        }

        if (!instanceBO.isConsistent()) {
			statusText = "ipc.config.inconsistent";
            sourceFile = "ipc/mimes/images/table/dot_red.gif";
        }
        return UIBeanFactory.getUIBeanFactory().newStatusImage(statusText, sourceFile);
    }
       
    public String getQuantity() {
        // the following coding is to beautify the instance quantity in the case of integers. 
        // I.e. "1.0" becomes "1" but "1.5" stays "1.5"         
        String quantityBefore = instanceBO.getQuantity();        
        String beautifiedQuantity = "";
        
        // the quantity is always returned from the engine in english format 
        // (i.e. with a dot as decimal separator, like "1.0"), so we use Locale.ENGLISH for conversion.
        DecimalFormat df = (DecimalFormat)DecimalFormat.getInstance(Locale.ENGLISH);
        df.setDecimalSeparatorAlwaysShown(false);
        try {
            Number num = df.parse(quantityBefore);
            beautifiedQuantity = num.toString();
        }
        catch (ParseException pex){
            // we keep the original format
            beautifiedQuantity = quantityBefore;
        }
        return beautifiedQuantity;
    }
    
    /**
     * This method is called to get the instance quantity for the instance tree.
     * There is a special treatment for quantity "0": If the quantity of the instance is
     * "0" it should not be displayed. So this method returns an empty String. 
     * @return instance quantity for the instance tree
     */
    public String getQuantityForInstanceTree(){
        String treeQty = this.getQuantity();
        if ((treeQty != null) && (treeQty.equals("0"))){
            treeQty = "";
        }
        return treeQty;
    }
    
    public String getBOMPosition() {
        return instanceBO.getBOMPosition();
    }
    

    /**
     * @return a list of MIME objects (images, sounds, etc.)
     * attached to the instance.
     */
    public ArrayList getMimes() {
        return this.mimes;
    }
    
    /**
     * Returns all Mimes of this instance and the given type.
     * @param Type of the mimes
     * @return List of mimes of this type
     */
    public ArrayList getMimesByType(String type) {
        ArrayList listOfMimes = (ArrayList) mimesByType.get(type.toLowerCase());
        if (listOfMimes == null) {
            listOfMimes = new ArrayList();        
        }
        return listOfMimes;        
    }

    /**
     * Add a MIME object (MimeUIBean) to the list of MIMEs
     * @param MimeUIBean to be added to this instance
     */
    public void addMime(MimeUIBean mime) {
        mimes.add(mime);        
        // try to receive the ArrayList of the given type
        String mimeType = mime.getType();
        ArrayList mimesOfSpecificType = (ArrayList) mimesByType.get(mimeType.toLowerCase());
        if (mimesOfSpecificType == null) {
            mimesOfSpecificType = new ArrayList();
            mimesByType.put(mime.getType().toLowerCase(), mimesOfSpecificType);
        }
        mimesOfSpecificType.add(mime);
    }

    /**
     * @return
     */
    public String getPrice() {
        return instanceBO.getPrice();
    }

    /**
     * @return
     */
    public ArrayList getGroups() {
        return groups;
    }
    
    public GroupUIBean getGroup(String name) {
        return (GroupUIBean)groupsByName.get(name);
    }

    /**
     * @param list
     */
    public void addGroup(GroupUIBean group) {
        groups.add(group);
        groupsByName.put(group.getName(), group);
    }

    /**
     * @return
     */
    public ArrayList getCharacteristics() {
        return characteristics;
    }

    /**
     * @return
     */
    public CharacteristicUIBean getCharacteristic(String characteristicName) {
        return (CharacteristicUIBean)characteristicsByName.get(characteristicName);
    }

    /**
     * @param list
     */
    public void addCharacteristic(CharacteristicUIBean characteristic) {
        characteristics.add(characteristic);
        characteristicsByName.put(characteristic.getName(), characteristic);
    }


	/**
	 * @return
	 */
	public boolean isExpanded() {
		return expanded;
	}

	/**
	 * @param b
	 */
	public void setExpanded(boolean b) {
		expanded = b;
	}

    /**
     * returns the first Mime of type "icon"
     */
    public MimeUIBean getIcon() {
        ArrayList listOfMimes = getMimesByType(MimeUIBean.ICON);
        if (!listOfMimes.isEmpty()) {
            return (MimeUIBean) listOfMimes.get(0);
        }
        else {
            return MimeUIBean.C_NULL;
        }
    }
    
    /**
     * returns the first Mime of type "image"
     */
    public MimeUIBean getImage() {
        ArrayList listOfMimes = getMimesByType(MimeUIBean.IMAGE);
        if (!listOfMimes.isEmpty()) {
            return (MimeUIBean) listOfMimes.get(0);
        }
        else {
            return MimeUIBean.C_NULL;
        }
    }
    
    /**
     * returns the first Mime of type "o2c"
     */
    public MimeUIBean getO2CObject() {
        ArrayList listOfMimes = getMimesByType(MimeUIBean.O2C);
        if (!listOfMimes.isEmpty()) {
            return (MimeUIBean) listOfMimes.get(0);
        }
        else {
            return MimeUIBean.C_NULL;
        }
    }
    
    /**
     * @return the first Mime of type "Application" (e.g. pdf-documents or word-documents, etc.)
     */
    public MimeUIBean getApplicationMime() {
        ArrayList listOfMimes = getMimesByType(MimeUIBean.APPLICATION);
        if (!listOfMimes.isEmpty()) {
            return (MimeUIBean) listOfMimes.get(0);
        }
        else {
            return MimeUIBean.C_NULL;
        }
    }    
    
    /**
     * returns the first Mime of type "sound"
     */
    public MimeUIBean getSoundObject() {
        ArrayList listOfMimes = getMimesByType(MimeUIBean.SOUND);
        if (!listOfMimes.isEmpty()) {
            return (MimeUIBean) listOfMimes.get(0);
        }
        else {
            return MimeUIBean.C_NULL;
        }
    }
    
    /**
     * returns the first Mime of type "unknown"
     */
    public MimeUIBean getUnknownMime() {
        ArrayList listOfMimes = getMimesByType(MimeUIBean.UNKNOWN);
        if (!listOfMimes.isEmpty()) {
            return (MimeUIBean) listOfMimes.get(0);
        }
        else {
            return MimeUIBean.C_NULL;
        }
    }

    /**
     * In the worka-area (list of cstics and values) not all Mimes can be included.
     * Only the following types will be included directly in the work-area:
     * <li>Icon
     * <li>O2C
     * <li>Sound
     * If either any characteristic or any value of this instance has mimes for the work-area the method 
     * will return true.
     * This is set during instantiation process of cstic-mimes and value-mimes
     * 
     * @return true if either any characteristic or any value of this instance has mimes for the work-area
     */
    public boolean hasAnyMimesForWorkArea() {
        return this.anyMimesForWorkArea;
    }

    /**
     * Set during instantiation process of cstic-mimes and value-mimes
     * @param set to true if any mime for work-area is available
     */
    public void setAnyMimesForWorkArea(boolean anyMimes) {
        this.anyMimesForWorkArea = anyMimes;
    }

    /**
     * Returns the enlarged Mime object. 
     * The object that is return is calculated as follows:
     * It tries to return the first Mime of type "Image". If this was not possible it returns
     * the first Mime of type "Icon" (i.e. the icon itself is returned as Enlarged Mime if there 
     * is no Mime of type "Image"). 
     * 
     * @return MimeUIBean for enlarged Mime
     */
    public MimeUIBean getEnlargedMime() {
        MimeUIBean enlargedMime = getImage();
        if (enlargedMime == MimeUIBean.C_NULL) {
            enlargedMime = getIcon();
        }
        return enlargedMime;
    }

    public String toString() {
        StringBuffer sb = new StringBuffer();
        sb.append("InstanceUIBean <NAME>");
        sb.append(getName());
        sb.append("</NAME><HAS-MIMES>");
        sb.append(hasAnyMimesForWorkArea());
        sb.append("</HAS-MIMES>");
        return sb.toString();
    }

	/**
	 * @return
	 */
	public boolean isRootInstance() {
		return instanceBO.isRootInstance();
	}

    /**
     * @return true if instance is complete
     */
    public boolean isComplete(){
        return instanceBO.isComplete();
    }

	/**
	 * @param conditionUIBean
	 */
	public void addCondition(ConditionUIBean conditionUIBean) {
		if (conditions == null) {
			conditions = new ConditionUIBeanSet();
		}
		conditions.add(conditionUIBean);
	}

	/**
	 * @return
	 */
	public ConditionUIBeanSet getConditions() {
		if (conditions != null) {
			return conditions;
		}else {
			return ConditionUIBeanSet.C_EMPTY;
		}
		
	}

	/**
	 * @param set
	 */
	public void addConditions(TreeSet set) {
		if (conditions == null) {
			conditions = new ConditionUIBeanSet();
		}
		for (Iterator it = set.iterator(); it.hasNext(); ) {
			ConditionUIBean condition = (ConditionUIBean)it.next();
			conditions.add(condition); 
		}
	}
	
	public void getLanguageDependentAncestorsNames(ArrayList ancestors){
		if (ancestors != null){
			if (parent != null){
				ancestors.add(0,parent.getLanguageDependentName());
				parent.getLanguageDependentAncestorsNames(ancestors);
			}
		}
	}


	/* (non-Javadoc)
	 * @see com.sap.isa.ipc.ui.jsp.action.visitor.IVisitableObject#accept(com.sap.isa.ipc.ui.jsp.action.visitor.IVisitor)
	 */
	public void accept(IVisitor visitor) {
		visitor.visit(this);	
	}
    
    /**
     * Returns the list of group-names from the business object. 
     * Depending on the parameter passed also names of invisible groups
     * can be returned.
     * @param 
     * @return a String array containing the names of the groups 
     */
    public String[] getGroupNamesFromBO(boolean includeInvisible){
        String[] names = new String[0]; 
        if (instanceBO != null){
            List grps = instanceBO.getCharacteristicGroups(includeInvisible, true);
            if (grps != null){
                names = new String[grps.size()];
                for (int i=0; i<grps.size(); i++){
                    names[i] = ((CharacteristicGroup)grps.get(i)).getName();
                }
            }
        }
        return names;
    }
    
    
	/**
	 * The configuration object Instance only supports price display and other pricing
	 * related functionality, if the Instance is associated with an @link IPCItem
	 * @return
	 */
	public boolean supportsPricing() {
		if (instanceBO != null && instanceBO.getIpcItem() != null) {
			return true;
		}else {
			return false; 
		}

	}

	public boolean isConfigurable() {
		return instanceBO.isConfigurable();
	}
}
