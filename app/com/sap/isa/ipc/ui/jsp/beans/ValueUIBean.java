/*
 * Created on 02.08.2004
 *
 * To change the template for this generated file go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
package com.sap.isa.ipc.ui.jsp.beans;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;

import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.core.util.JspUtil;
import com.sap.isa.core.util.WebUtil;
import com.sap.isa.ipc.ui.jsp.action.visitor.IVisitableObject;
import com.sap.isa.ipc.ui.jsp.action.visitor.IVisitor;
import com.sap.isa.ipc.ui.jsp.uiclass.UIContext;
import com.sap.isa.ipc.ui.jsp.util.StringUtil;
import com.sap.spc.remote.client.object.Characteristic;
import com.sap.spc.remote.client.object.CharacteristicValue;
import com.sap.spc.remote.client.object.MimeObject;
import com.sap.spc.remote.client.object.MimeObjectContainer;
import com.sap.spc.remote.client.object.OriginalRequestParameterConstants;


/**
 * Wrapper around the CharacteristicValue, that is specialized for HTML usage.
 */
public class ValueUIBean implements IVisitableObject{ 
    
    protected ConditionUIBean variantConditionUIBean;
	protected CharacteristicValue valueBO;
    protected UIContext uiContext;
    private static IsaLocation location = IsaLocation.getInstance(ValueUIBean.class.getName());
    
	protected ArrayList mimes = new ArrayList();
	protected HashMap mimesByType = new HashMap();
	protected boolean mimesForWorkArea = false;
	protected boolean additionalMimes = false;
    protected int bufferedSelected = -1;

    public static final int BUFFER_NOT_SELECTED = 0;
    public static final int BUFFER_SELECTED = 1;
       
    class Key {
		protected String instanceId;
		protected String csticName;
		protected String valueName;
    
        Key(String instanceId, String csticName, String valueName) {
            this.instanceId = instanceId;
            this.csticName = csticName;
            this.valueName = valueName;
        }

        public boolean equals(Object o) {
            if (o instanceof Key) {
                Key key = (Key) o;
                if (key.instanceId.equals(this.instanceId)
                    && key.csticName.equals(this.csticName)
                    && key.valueName.equals(this.valueName)) {
                    return true;
                }
            }
            return false;
        }

        public int hashCode() {
            return instanceId.hashCode() ^ csticName.hashCode() ^ valueName.hashCode();
        }
        
        public String toString() {
            StringBuffer result = new StringBuffer();
            result.append("Instance:");
            result.append(instanceId);
            result.append(";");
            result.append("Cstic:");
            result.append(csticName);
            result.append("Value:");
            result.append(valueName);
            return result.toString();
        }
    
    }

    /**
     * Factory method
     * @param request - HttpServletRequest
     * @param value - BusinessObject of the CharacteristicValue
     * @param uiContext - UIContext object
     * @return
     */
//    public static ValueUIBean getInstance(
//        HttpServletRequest request,
//        CharacteristicValue value,
//        UIContext uiContext) {
//        HashMap container;
//        Object containerObject = request.getAttribute(VALUE_UI_BEANS);
//        if (containerObject == null) {
//            container = new HashMap();
//            request.setAttribute(VALUE_UI_BEANS, container);
//            if (location.isDebugEnabled()) {
//                location.debug("Creating HashMap for " + VALUE_UI_BEANS + " and adding to request!");
//            }
//        } else {
//            container = (HashMap) containerObject;
//            if (location.isDebugEnabled()) {
//                location.debug("Retrieved HashMap for " + VALUE_UI_BEANS + " from request!");
//            }
//        }
//        Key key =
//            helperInstance.new Key(
//                value.getCharacteristic().getInstance().getId(),
//                value.getCharacteristic().getName(),
//                value.getName());
//        Object uiBeanObject = container.get(key);
//        if (uiBeanObject == null) {
//            if (location.isDebugEnabled()){
//                location.debug("No ValueUIBean instance for " + key + " found! Creating one and adding to container.");
//            }
//            ValueUIBean valueUIBean =
//                new ValueUIBean(value, uiContext);
//            container.put(key, valueUIBean);
//            return valueUIBean;
//        } else {
//            if (location.isDebugEnabled()){
//                location.debug("ValueUIBean instance for " + key + " found.");
//            }
//            return (ValueUIBean) uiBeanObject;
//        }
//
//    }
    
    /**
     * Constructor
     * @param BusinessObject of the CharacteristicValue
     * @param uiContext
     */
    protected ValueUIBean(CharacteristicValue valueBO, UIContext uiContext) {
        this.valueBO = valueBO;
        this.uiContext = uiContext;
        if (valueBO != null) {
            MimeObjectContainer mimeObjectContainer = this.valueBO.getMimeObjects();
            // generate mimeUIBeans only if necessary
            if (mimeObjectContainer != null) {
                Iterator mimeBOList = mimeObjectContainer.getMimeObjects().iterator();
                while (mimeBOList.hasNext()) {
                    MimeObject mimeBO = (MimeObject) mimeBOList.next();
                    MimeUIBean mimeBean = UIBeanFactory.getUIBeanFactory().newMimeUIBean(mimeBO, this.uiContext);
                    addMime(mimeBean); 
                }
            }
        }
    }
    
    /**
     * In order to give customers the possibility access business objects in JSP pages.
     * This is useful especially for customer extension data of business objects.
     * @return
     */
    public CharacteristicValue getBusinessObject() {
    	return valueBO;
    }
    
    public static void main(String[] args) {
    }
    
    /**
     * @return the language dependent description of the characteristic value
     */
    public String getDescription() {
    	String description = valueBO.getDescription();
    	if (uiContext.getShowLanguageDependentNames() && description != null) {
    		return StringUtil.convertStringToHtml(JspUtil.replaceSpecialCharacters(description));
    	}else {
    		return JspUtil.replaceSpecialCharacters(getLanguageDependentName());
    	}
    }

    /**
     * The display name of a characteristic valuedepends on two XCM parameters:<br/>
     * 1. values.descriptionlength<br/>
     * 2. appearance.showlangdepnames<br/>
     * If <code>appearance.showlangdepnames</code> is false, the ID of the characteristic value will be used
     * instead of the language dependand name.<br/>
     * If values.descriptionlength is configured to hold the shorttext and the ID, both
     * values will be displayed by using the resource key "ipc.cstic.descr.delimiter".
     * 
     * @return The display name
     */
    public String getDisplayName() {
        String name = null;
        // Check if only the Name is to be displayed in the shortname field
        if(uiContext.getValuesDescriptionType().equals(OriginalRequestParameterConstants.iDTextId) ||
           uiContext.getValuesDescriptionType().equals(OriginalRequestParameterConstants.iDAndLongDescriptionTextId))
            return valueBO.getName();
        name=getLanguageDependentName();
        if(uiContext.getValuesDescriptionType().equals(OriginalRequestParameterConstants.iDAndShortDescriptionTextId) ||
                uiContext.getValuesDescriptionType().equals(OriginalRequestParameterConstants.iDAndShortAndLongDescriptionTextId))
        {
             return WebUtil.translate(uiContext.getLocale(),"ipc.cstic.descr.delimiter", new String[]{name, valueBO.getName()} );
        }
        if(uiContext.getValuesDescriptionType().equals(OriginalRequestParameterConstants.longDescriptionTextId))
        {
            String descr =  getLengthLimitedDescription();
            if( descr== null || descr.length()==0)
                return name;
            else
                return descr;

        }

        return name;
    }

    /**
     * The value-text displayed in a drop-down box might differ from the value-text displayed 
     * for checkboxes/radiobuttons.
     * It is depending on XCM-parameter "values.descriptiontype":
     * ShortDescription: 
     *     Default behavior. The language-dependent name (30 characters) of the value is displayed.
     * LongDescription: 
     *     Instead of the language-dependent name, the long description of the value is displayed up to the first line 
     *     break or up to the threshold (values.maxdescriptionlength), dependent on whatever comes first.
     *     Example: Maximum length is defined as 50 characters but the first line break is at position 25.
     *              The text that will be displayed in the DDLB contains the first 25 characters only. 
     * IDAndShortDescription: 
     *     The ID and the language-dependent name of the value are displayed.  
     * IDAndLongDescription: 
     *     The ID and the long description of the value are displayed in one line up to the first line break 
     *     or up to the threshold, dependent on whatever comes first.
     * ShortAndLongDescription: 
     *     Only the language-dependent name is displayed as it is not possible to display two columns in a DDLB.
     * IDAndShortAndLongDescription: 
     *     Only the language-dependent name is displayed as it is not possible to display two columns in a DDLB.
     * ID: 
     *     The ID is displayed.
     */
    public String getDisplayNameForDropDownBox() {
        String name = null;
        if(uiContext.getValuesDescriptionType().equals(OriginalRequestParameterConstants.iDTextId)) {
            name = getName();
    }
        else if (uiContext.getValuesDescriptionType().equals(OriginalRequestParameterConstants.longDescriptionTextId)){
            name = getDescriptionFirstLine();
        }
        else if (uiContext.getValuesDescriptionType().equals(OriginalRequestParameterConstants.iDAndShortDescriptionTextId)){
            name = WebUtil.translate(uiContext.getLocale(),"ipc.cstic.descr.delimiter", new String[]{getLanguageDependentName(), getName()} );
        }
        else if (uiContext.getValuesDescriptionType().equals(OriginalRequestParameterConstants.iDAndLongDescriptionTextId)){
            name = WebUtil.translate(uiContext.getLocale(),"ipc.cstic.descr.delimiter", new String[]{getDescriptionFirstLine(), getName()} );
        }
        else{
            name = getLanguageDependentName();
        }
        return name;
    }
    
    /**
     * Returns the first line of the description but only up to the maintained threshold (values.maxdescriptionlength).
     * If the line is longer it will be cut off at this length. 
     * If the description starts with a line-break (this might be the case for products modelled in CRM) it removes 
     * the first line-break.
     * If the description is not maintained it returns the language-dependent name
     * @return the first line of the description (up to the threshold)
     */
    private String getDescriptionFirstLine() {
        String name;
        // Get the threshold
        String maxLength = uiContext.getValuesMaxDescriptionLength();
        int maxLengthI = 80;
        if(maxLength != null && maxLength.length()>0) {
            maxLengthI = Integer.parseInt(maxLength);
        }
        else {
            if(location.isDebugEnabled())
                location.debug("XCM value characteristics.maxdescriptionlength was not resolved. Use default length 80 instead");
        }
        // Check whether it contains a line break before the maxlength.
        String descr = getDescription();
        boolean lineBreakBeforeMaxLength= false;;
        int pos = -1;
        if (descr != null){
            // Check whether there is a line-break at the first position (CRM-PME issue): remove it
            pos = descr.indexOf("<br>");
            if (pos == 0){
                if (descr.length() > 3){
                    descr = descr.substring(4, descr.length());
                }
                // check for the position of the next line-break
                pos = descr.indexOf("<br>");
            }
            if ((pos > 0) && (pos <= maxLengthI)){
                lineBreakBeforeMaxLength = true;
            }
        }
        
        if (descr != null && lineBreakBeforeMaxLength){
            name = descr.substring(0, pos);            
        }
        else {
            name = getLengthLimitedDescription(descr);
        }
        // fallback if the description is not maintained:
        if (name != null && name.equals("")){
            name = getLanguageDependentName();
        }
        return name;
    }
    
    /**
     * The name that is displayed for values of a message-characteristic follows this rule:
     * If a description is maintained it is displayed (the length depends on parameter 
     * values.descriptionlength). If no description is maintained the language-dependent name 
     * is used. This is independent of the parameter values.descriptiontype. (Exception: if 
     * the user switches to language-independent display also the values of message-characteristics are 
     * displayed language-independent).
     * If the parameter characteristics.messagecstics.showid is set to T the language independent value name
     * is added in front of the description (in brackets).
     * @return the value for message-characteristics
     */
    public String getDisplayNameForMessageCsticValues() {
        String displayName = null;
        String descr =  getLengthLimitedDescription();
        if( descr== null || descr.length()==0){
            // no description available use language dependent name
           displayName = getLanguageDependentName();
        }
        else {
            // use description
            displayName = descr; 
        }
        if (uiContext.getPropertyAsBoolean(OriginalRequestParameterConstants.MESSAGE_CSTICS_SHOW_ID)){
            displayName = "(" + this.getName() + ") " + displayName; 
        }
        return displayName;
    }
    
    /**
     * Returns the key for the name that may be displayed in the characterstic bar 
     * for values of a message-characteristic. 
     * <ul>
     * <li>&quot;Error Message&quot; (value name starts with "E")</li>
     * <li>&quot;Warning Message&quot; (value name starts with "W")</li>
     * <li>&quot;Information Message&quot; (value name starts with "I"); default</li>
     * </ul>
     * @return resource key for the name displayed in the cstic-bar of a message-characteristic
     */
    public String getDisplayNameForMessageCstic() {
        // by default it is an information message
        String key = "ipc.message.info";
        if (valueBO.getName().startsWith("W")) {
            key = "ipc.message.warning";
        }
        if (valueBO.getName().startsWith("E")) {
            key = "ipc.message.error";
        }
        return key;
    }

    /**
     * @return
     */
    public  String getLengthLimitedDescription() {
        String maxLength = uiContext.getValuesMaxDescriptionLength();
        int maxLengthI = 80;
        if( maxLength != null)
        {
            maxLengthI = Integer.parseInt(maxLength);
        }
        else
        {
            if(location.isDebugEnabled())
                location.debug("XCM value characteristics.maxdescriptionlength was not resolved. Use default length 80 instead");
        }
        String descr= getDescription();
        if( descr != null && descr.length() > maxLengthI)
        {
            descr = descr.substring(0, maxLengthI).replaceAll("\\s[^\\s]*$","");
            descr = descr + "...";
        }
        return descr;
    }

    /**
     * Limits the description that is passed to this method.
     * @return
     */
    public  String getLengthLimitedDescription(String descr) {
        if (descr == null){
            return "";
        }
        String maxLength = uiContext.getValuesMaxDescriptionLength();
        int maxLengthI = 80;
        if( maxLength != null)
        {
            maxLengthI = Integer.parseInt(maxLength);
        }
        else
        {
            if(location.isDebugEnabled())
                location.debug("XCM value characteristics.maxdescriptionlength was not resolved. Use default length 80 instead");
        }
        if( descr != null && descr.length() > maxLengthI)
        {
            descr = descr.substring(0, maxLengthI).replaceAll("\\s[^\\s]*$","");
            descr = descr + "...";
        }
        return descr;
    }


    /**
     * @return the language dependent name of the characteristic value
     */
    public String getLanguageDependentName() {
        return JspUtil.replaceSpecialCharacters(getLanguageDependentNameNoEncoding());
    }

    /**
     * @return the language dependent name of the characteristic value but does not encode special characters.
     */
    public String getLanguageDependentNameNoEncoding() {
        // return the language-dependent name if uiContext.getShowLanguageDependent returns true
        // or if the characteristic has the type integer, float or date (in theses cases displaying
        // a language-independent view doesn't make sense        
        if ((uiContext.getShowLanguageDependentNames() ||
            valueBO.getCharacteristic().getValueType() == Characteristic.TYPE_INTEGER ||
            valueBO.getCharacteristic().getValueType() == Characteristic.TYPE_FLOAT ||
            valueBO.getCharacteristic().getValueType() == Characteristic.TYPE_DATE)
            && valueBO.getLanguageDependentName() != null) {
                return valueBO.getLanguageDependentName();
        } else {
                return valueBO.getName();
        }
    }


    /**
     * @return the language independent name of the characteristic value
     */
    public String getName() {
        return valueBO.getName();
    }

    /**
     * Add a MIME object (MimeUIBean) to the list of MIMEs
     * @param MimeUIBean to be added to this value
     */
    public void addMime(MimeUIBean mime) {
        mimes.add(mime);        
        // try to receive the ArrayList of the given type
        String mimeType = mime.getType();
        ArrayList mimesOfSpecificType = (ArrayList) mimesByType.get(mimeType.toLowerCase());
        if (mimesOfSpecificType == null) {
            mimesOfSpecificType = new ArrayList();
            mimesByType.put(mime.getType().toLowerCase(), mimesOfSpecificType);
        }
        mimesOfSpecificType.add(mime);
        // set flag if mime is of type icon, o2c or sound
        // this is done during instantiation-process because of performance-reasons
        if (mimeType.equalsIgnoreCase(MimeUIBean.ICON) 
            || mimeType.equalsIgnoreCase(MimeUIBean.O2C)
            || mimeType.equalsIgnoreCase(MimeUIBean.SOUND)) {
            
            this.mimesForWorkArea = true;                
        }
        // set flag if mime is of type  Application or Unknown
        if (mimeType.equalsIgnoreCase(MimeUIBean.APPLICATION) 
            || mimeType.equalsIgnoreCase(MimeUIBean.UNKNOWN)) {
            
            this.additionalMimes = true;                
        }                        
    }


    /**
     * @return a list of MIME objects (images, sounds, etc.)
     * attached to the value.
     */
    public ArrayList getMimes() {
        return this.mimes;
    }

    /**
     * Returns all Mimes of this value and the given type.
     * @param Type of the mimes
     * @return List of mimes of this type
     */
    public ArrayList getMimesByType(String type) {
        ArrayList listOfMimes = (ArrayList) mimesByType.get(type.toLowerCase());
        if (listOfMimes == null) {
            listOfMimes = new ArrayList();        
        }
        return listOfMimes;        
    }

    /**
     * @return a value price if a variant condition was assigned to the value. 
     * The price is formatted in the configuration's locale, with the pattern "<Value> <Currency>".
     */
    public String getPrice() {
        return valueBO.getPrice();
    }

    /**
     * @return true if the value has been assigned
     */
    public boolean isAssigned() {
        return valueBO.isAssigned();
    }

    /**
     * @return true if the value has been assigned by the system
     */
    public boolean isAssignedBySystem() {
        return valueBO.isAssignedBySystem();
    }

    /**
     * @return true if the value is in the dynamic domain of the characteristic.
     */
    public boolean isAssignable() {
        return valueBO.isAssignable();
    }
    
    /**
     * 
     * @return true if the value is part of the static domain
     */
    public boolean isInDomain() {
        return valueBO.isInDomain(); 
    }

    /**
     * @return true if the value is the default value
     */
    public boolean isDefaultValue() {
        return valueBO.isDefaultValue();
    }


    /**
     * @return true if the value has been assigned by the user
     */
    public boolean isAssignedByUser() {
        return valueBO.isAssignedByUser();
    }

    /**
     * @return the first Mime of type "icon"
     */
    public MimeUIBean getIcon() {
        ArrayList listOfMimes = getMimesByType(MimeUIBean.ICON);
        if (!listOfMimes.isEmpty()) {
            return (MimeUIBean) listOfMimes.get(0);
        }
        else {
            return MimeUIBean.C_NULL;
        }
    }
    
    /**
     * @return the first Mime of type "image"
     */
    public MimeUIBean getImage() {
        ArrayList listOfMimes = getMimesByType(MimeUIBean.IMAGE);
        if (!listOfMimes.isEmpty()) {
            return (MimeUIBean) listOfMimes.get(0);
        }
        else {
            return MimeUIBean.C_NULL;
        }
    }
    
    /**
     * @return the first Mime of type "o2c"
     */
    public MimeUIBean getO2CObject() {
        ArrayList listOfMimes = getMimesByType(MimeUIBean.O2C);
        if (!listOfMimes.isEmpty()) {
            return (MimeUIBean) listOfMimes.get(0);
        }
        else {
            return MimeUIBean.C_NULL;
        }
    }
    
   
    /**
     * @return the first Mime of type "sound"
     */
    public MimeUIBean getSoundObject() {
        ArrayList listOfMimes = getMimesByType(MimeUIBean.SOUND);
        if (!listOfMimes.isEmpty()) {
            return (MimeUIBean) listOfMimes.get(0);
        }
        else {
            return MimeUIBean.C_NULL;
        }
    }
    
    /**
     * @return the first Mime of type "Application" (e.g. pdf-documents or word-documents, etc.)
     */
    public MimeUIBean getApplicationMime() {
        ArrayList listOfMimes = getMimesByType(MimeUIBean.APPLICATION);
        if (!listOfMimes.isEmpty()) {
            return (MimeUIBean) listOfMimes.get(0);
        }
        else {
            return MimeUIBean.C_NULL;
        }
    }
        
    /**
     * @return the first Mime of type "unknown"
     */
    public MimeUIBean getUnknownMime() {
        ArrayList listOfMimes = getMimesByType(MimeUIBean.UNKNOWN);
        if (!listOfMimes.isEmpty()) {
            return (MimeUIBean) listOfMimes.get(0);
        }
        else {
            return MimeUIBean.C_NULL;
        }
    }
    
    /**
     * In the worka-area (list of cstics and values) not all Mimes can be included.
     * Only the following types will be included directly in the work-area:
     * <li>Icon
     * <li>O2C
     * <li>Sound
     * If the value has at least one of these Mimes the method will return true.
     * 
     * @return true if the value has at least one Mime for the work area
     */
    public boolean hasMimesForWorkArea() {
        return this.mimesForWorkArea; 
    }

    /**
     * Returns the value-mime for the work-area.
     * In the work-area (list of cstics and values) not all Mimes can be included.
     * Only the following types will be included directly in the work-area:
     * <li>Icon
     * <li>O2C
     * <li>Sound
     * This method returns the appropriate mime in the above order (i.e. first it checks
     * for an icon, if this is not available it will try to return an O2C-mime, and so on).
     * 
     * @return the mime for the work-area
     */
    public MimeUIBean getMimeForWorkArea() {
        MimeUIBean mimeForWorkArea = MimeUIBean.C_NULL;
        if (hasMimesForWorkArea()) {
            mimeForWorkArea = getIcon();
            if (mimeForWorkArea == MimeUIBean.C_NULL) {
                mimeForWorkArea = getO2CObject();
                if (mimeForWorkArea == MimeUIBean.C_NULL) {
                    mimeForWorkArea = getSoundObject();
                }
            }
        }
        return mimeForWorkArea; 
    }
    
    /**
     * Returns the link (URL) to an additional mime of this value.
     * 
     * Only the following types are taken into account for the additional mime link determination 
     * <li>Application
     * <li>Unknown
     * 
     * This method returns the appropriate link in the above order (i.e. first it checks
     * for a mime with type "Application" (e.g. pdf or doc), if this is not available it will 
     * try to return a link to a mime of type "Unknown". 
     * If there is no additional mime for this value. This method will return
     * an empty string ("");
     *
     * @return URL to the additional mime, empty string if no additional mime is available
     */
    public String getLinkToAdditionalMime() {
        String linkToAdditionalMime = "";
        MimeUIBean additionalMime = MimeUIBean.C_NULL;
        if (this.hasAdditionalMimes()) {
            additionalMime = getApplicationMime();
            if (additionalMime == MimeUIBean.C_NULL) {
                additionalMime = getUnknownMime();
            }
            linkToAdditionalMime = additionalMime.getURL();
        }
        return linkToAdditionalMime;
    }
    
     /**     
     * Additional Mimes are: 
     * <li>PDF
     * <li>DOC
     * <li>Unknown
     * If the characteristic has at least one of these Mimes the method will return true.
     * 
     * @return true if the characteristic has at least one additional mime
     */
    public boolean hasAdditionalMimes() {
        return this.additionalMimes;
         
    }   
    
    /**
     * Returns the enlarged Mime object. 
     * The object that is return is calculated as follows:
     * It tries to return the first Mime of type "Image". If this was not possible it returns
     * the first Mime of type "Icon" (i.e. the icon itself is returned as Enlarged Mime if there 
     * is no Mime of type "Image"). 
     * 
     * @return MimeUIBean for enlarged Mime
     */
    public MimeUIBean getEnlargedMime() {
        MimeUIBean enlargedMime = getImage();
        if (enlargedMime == MimeUIBean.C_NULL) {
            enlargedMime = getIcon();
        }
        return enlargedMime;
    }    
    
    public String toString(){
        StringBuffer sb = new StringBuffer();
        sb.append("ValueUIBean <NAME>");
        sb.append(this.getName());
        sb.append("</NAME><HAS-MIMES>");
        sb.append(hasMimesForWorkArea());
        sb.append("</HAS-MIMES>");       
        return sb.toString();
    }

	/**
	 * @return
	 */
	public String getConditionKey() {
		return valueBO.getConditionKey();
	}

	/**
	 * @param variantConditionUIBean
	 */
	public void setVariantCondition(ConditionUIBean variantConditionUIBean) {
		this.variantConditionUIBean = variantConditionUIBean;
	}
	
	public ConditionUIBean getVariantCondition() {
		return variantConditionUIBean;
	}
    
    /**
     * Returns the name for the link that is used for highlighting when mouse is over the value.
     * This is used to highlight the value even if the mouse is over the input-field 
     * (radio or checkbox) or over the value name.
     * @return Name for the highlighting link.
     */
    public String getHighlightingName(){
        String csticName = valueBO.getCharacteristic().getName();
        String valueName = valueBO.getName();
        StringBuffer highlightName = new StringBuffer();
        highlightName.append("hover_");
        highlightName.append(csticName);
        highlightName.append(".");
        highlightName.append(valueName); 
        return highlightName.toString();
    }
    
	public UIContext getUiContext() {
		return uiContext;
	}
    
    /**
     * Returns the style class for this value of a message-cstic.
     * There are three different types of styles:
     * <ul>
     * <li>Error (value name starts with "E")</li>
     * <li>Warning (value name starts with "W")</li>
     * <li>Information (value name starts with "I"); default</li>
     * </ul>
     * @return the style class for this value of a message-cstic 
     */
    public String getMessageCsticStyleClass(){
        // by default it is an information message
        String styleClass = "ipcMessageCsticInfo";
        if (valueBO.getName().startsWith("W")){
            styleClass = "ipcMessageCsticWarning";
        }
        else if (valueBO.getName().startsWith("E")){
            styleClass = "ipcMessageCsticError";
        }
        return styleClass;
    }

    /**
     * If the characteristic is a message-characteristic the value has a status image.
     * <ul>
     * <li>Error (value name starts with "E")</li>
     * <li>Warning (value name starts with "W")</li>
     * <li>Information (value name starts with "I"); default</li>
     * </ul>
     * @return The relative URL to the status image of the value 
     */
    public StatusImage getStatusImage() {
        // by default it is an information message
        String sourceFile = "ipc/mimes/images/sys/ico12_success.gif";
        String statusDescription = "ipc.message.tt.info";

        if (valueBO.getName().startsWith("W")) {
            sourceFile = "ipc/mimes/images/sys/ico12_warning.gif";
            statusDescription = "ipc.message.tt.warning";
        }
        if (valueBO.getName().startsWith("E")) {
            sourceFile = "ipc/mimes/images/sys/ico12_error.gif";
            statusDescription = "ipc.message.tt.error";
        }
        return UIBeanFactory.getUIBeanFactory().newStatusImage(statusDescription, sourceFile);
    }

	/* (non-Javadoc)
	 * @see com.sap.isa.ipc.ui.jsp.action.visitor.IVisitableObject#accept(com.sap.isa.ipc.ui.jsp.action.visitor.IVisitor)
	 */
	public void accept(IVisitor visitor) {
		visitor.visit(this);
	}

	/**
     * @return true if the characteristic of this value allows multiple values to be set
     */
    public boolean csticAllowsMultipleValues(){
		boolean allowsMultipleValues = false;
		Characteristic csticBO = this.valueBO.getCharacteristic(); 
		if (csticBO != null){
			allowsMultipleValues = csticBO.allowsMultipleValues();
		}

		return allowsMultipleValues;
	}

	/**
	 * @return true if the characteristic of this value is read only
	 */
	public boolean isCsticReadOnly(){
		boolean readOnly = false;
		Characteristic csticBO = this.valueBO.getCharacteristic(); 
		if (csticBO != null){
			readOnly = csticBO.isReadOnly();
		}
		return readOnly;
	}

	/**
	 * @return true if the characteristic of this value has an interval as domain
	 */
	public boolean hasCsticIntervalAsDomain(){
		boolean intervalDomain = false;
		Characteristic csticBO = this.valueBO.getCharacteristic(); 
		if (csticBO != null){
			intervalDomain = csticBO.hasIntervalAsDomain();
		}
		return intervalDomain;
	}

	/**
	 * @return true if the characteristic of this value has assigned values
	 */
	public boolean hasCsticAssignedValues(){
		boolean assignedValues = false;
		Characteristic csticBO = this.valueBO.getCharacteristic(); 
		if (csticBO != null){
			assignedValues = csticBO.hasAssignedValues();
		}
		return assignedValues;
	}
    
    /**
     * This method can be used to overwrite the assigned-flag if the value is set or un-set
     * but the information has not been sent to the engine (i.e. it is only buffered on client-side).
     * By default bufferedSelected is -1, i.e. the original assigned-flag is used.
     * If the assigned-flag should be overwritten the following values can be passed:
     * <li>0: the value should be unselected
     * <li>1: the value should be selected
     * To read this information method isSelected needs to be called.
     * @param bufferedAssigned
     */
    public void setBufferedSelected(int selected){
        bufferedSelected = selected;        
    }
    
    /**
     * Returns true if the value is selected. This method takes the bufferedSelected information into 
     * account (see method setBufferedSelected().
     * @return true if the value is selected
     */
    public boolean isSelected(){
        if (bufferedSelected == BUFFER_NOT_SELECTED){
            // assigned-flag has been overwritten with "unselected"
            return false;
        }
        else if (bufferedSelected == BUFFER_SELECTED){
            // assigned-flag has been overwritten with "selected"
            return true;
        }
        else {
            // assigned-flag has not been overwritten: use the original assigned-flag
            return this.isAssigned();
        }
    }

    
}
