package com.sap.isa.ipc.ui.jsp.beans;

import com.sap.isa.core.util.JspUtil;
import com.sap.isa.ipc.ui.jsp.action.visitor.IVisitableObject;
import com.sap.isa.ipc.ui.jsp.uiclass.UIContext;
import com.sap.isa.ipc.ui.jsp.util.StringUtil;
import com.sap.spc.remote.client.object.Instance;

/**
 * Instance UI bean which supports a changeable product variant. Difference to the super class:
 * Product ID and description are read from the underlying IPC item, not the Instance.
 * @deprecated
 */
public class ChangeablePVInstanceUIBean extends InstanceUIBean implements IVisitableObject {
	
	protected ChangeablePVInstanceUIBean(Instance instanceBO, UIContext uiContext) {
		super(instanceBO, uiContext);
	}	
	
	
	/**
	 * Get name. Read from IPC item if available.
	 */
	public String getName() {
		if ( instanceBO.getIpcItem() != null){
			String name = instanceBO.getIpcItem().getProductId();
			if (name != null && !name.equals("")){
				return name;			
			}
			else return super.getName();
		}
		else return super.getName();
	}	

	
	
	/**
	 * Get language dependent name. Read from IPC item if available.
	 */
	public String getLanguageDependentName() {
		
		if ( instanceBO.getIpcItem() != null){
			if (uiContext.getShowLanguageDependentNames()
				&& instanceBO.getIpcItem().getProductDescription() != null) {
				String languageDependentName =  JspUtil.replaceSpecialCharacters(instanceBO.getIpcItem().getProductDescription());
				if ( languageDependentName != null && !languageDependentName.equals(""))
				return languageDependentName;
				else return super.getLanguageDependentName();

			} else {

				return StringUtil.encodeLanguageIndependentName(
					getName(),
					uiContext);
			}
			
		}
		else return super.getLanguageDependentName();
				
	}	
}
