/*
 * Created on 06.08.2004
 *
 * To change the template for this generated file go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
package com.sap.isa.ipc.ui.jsp.beans;

import com.sap.isa.ipc.ui.jsp.action.visitor.IVisitableObject;
import com.sap.isa.ipc.ui.jsp.action.visitor.IVisitor;

/**
 * @author 
 *
 * To change the template for this generated type comment go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
public class StatusImage implements IVisitableObject{

	protected String resourceKey;
	protected String imagePath;

    protected StatusImage(String resourceKey, String imagePath) {
        this.resourceKey = resourceKey;
        this.imagePath = imagePath;
    }

    /**
     * @return
     */
    public String getImagePath() {
        return imagePath;
    }

    /**
     * @return
     */
    public String getResourceKey() {
        return resourceKey;
    }

    public static void main(String[] args) {
    }

	/* (non-Javadoc)
	 * @see com.sap.isa.ipc.ui.jsp.action.visitor.IVisitableObject#accept(com.sap.isa.ipc.ui.jsp.action.visitor.IVisitor)
	 */
	public void accept(IVisitor visitor) {
		visitor.visit(this);
	}
}
