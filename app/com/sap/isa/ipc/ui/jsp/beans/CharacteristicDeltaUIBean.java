package com.sap.isa.ipc.ui.jsp.beans;

import java.util.ArrayList;

import com.sap.isa.core.util.JspUtil;
import com.sap.isa.ipc.ui.jsp.uiclass.UIContext;
import com.sap.isa.ipc.ui.jsp.util.StringUtil;
import com.sap.spc.remote.client.object.Characteristic;

public class CharacteristicDeltaUIBean extends CharacteristicUIBean {

    private ArrayList loadingMessages;
    private boolean valueDeltas;
    private String internalName = ""; // This is used if this deltaUIBean has been created for
                                      // a cstic of an deleted instance. In this case there is no
                                      // business object.
    private String internalLName = "";
    
    /**
     * Creates a CharacteristicDeltaUIBean.
     * @param characteristicBO  business object of the characteristic
     * @param uiContext
     */
    protected CharacteristicDeltaUIBean(Characteristic characteristicBO, UIContext uiContext) {
        super(characteristicBO, uiContext);
        loadingMessages = new ArrayList();
    }

    /**
     * Creates a CharacteristicDeltaUIBean for a deleted instance (in this case no BO is available)
     * @param csticName internal name of the characteristic
     * @param internalLName internal language dependent name of the characteristic (pass null or empty string if not available)
     * @param uiContext
     */
    protected CharacteristicDeltaUIBean(String csticName, String internalLName, UIContext uiContext) {
        super(null, uiContext);
        this.internalName = csticName;
        this.internalLName = internalLName;
        loadingMessages = new ArrayList();
    }

    /**
     * Creates a CharacteristicDeltaUIBean.
     * @param characteristicBO business object of the characteristic
     * @param uiContext
     * @param valueDeltas list of value deltas
     * @param loadingMessages list of loading messages
     */
    protected CharacteristicDeltaUIBean(Characteristic characteristicBO, UIContext uiContext, ArrayList valueDeltas, ArrayList loadingMessages) {
        super(characteristicBO, uiContext);
        this.values = valueDeltas;
        this.loadingMessages = loadingMessages;
    }

    /**
     * Add a value delta to this characteristic.
     * @param valueDelta value delta UIBean
     */
    public void addValue(ValueUIBean valueDelta){
        super.addValue(valueDelta);
        valueDeltas = true;
    }
    
    
    /**
     * @return true if this characteristic has value deltas
     */
    public boolean hasValueDeltas(){
        return valueDeltas;
    }
    
    /**
     * Add a loading message to this characteristic.
     * @param loadingMessage
     */
    public void addLoadingMessage(LoadingMessageUIBean loadingMessage){
        loadingMessages.add(loadingMessage);
    }
    
    /**
     * Returns a list of loading messages for this characteristic.
     * If there are no loading messages it will return an empty list.
     * @return list of loading messages
     */
    public ArrayList getLoadingMessages(){
        return loadingMessages;
    }
    
    /**
     * @return true if this characteristic has loading messages
     */
    public boolean hasLoadingMessages(){
        return (loadingMessages.size() > 0);
    }
    
    /**
     * Set a list of loading messages at this characteristic delta.
     * This overwritten an existing list of loading messages.
     * @param list list of loading messages for this characteristic
     */
    public void setLoadingMessages(ArrayList list) {
        loadingMessages = list;
    }

    /**
     * Returns the language independent name of the associated business object characteristic.
     * It could happen that this bean does not have an associated BO characterstic if
     * this bean has been created for a "deleted" instance.    
     * In this case it returns the internal name (which has to be set before).
     * @return The language independent name of the associated business object characteristic.
     */
    public String getName() {
        String name;
        if (super.getBusinessObject() == null){
            // no BO -> return the internal name
            name = JspUtil.replaceSpecialCharacters(internalName);
        }
        else {
            name = super.getName();
        }
        return name;
    }
    
    /**
     * Returns the language dependent name of the associated business object characteristic.
     * It could happen that this bean does not have an associated characterstic BO if
     * this bean has been created for a "deleted" instance.    
     * In this case it returns the internal name (which has to be set before).
     * @return The language dependent name of the associated business object characteristic depending on the uiContext.
     */
    public String getLanguageDependentName() {
        String lName;
        if (super.getBusinessObject() == null){
            // no BO -> return the internal lName (if available, otherwise the internal name)
            if ((internalLName != null) && (!internalLName.equals(""))){
                lName = JspUtil.replaceSpecialCharacters(internalLName);
            }
            else {
                lName = JspUtil.replaceSpecialCharacters(internalName);
            }
        }
        else {
            lName = super.getLanguageDependentName();
        }
        return lName;
    }
    /* (non-Javadoc)
     * @see com.sap.isa.ipc.ui.jsp.beans.CharacteristicUIBean#allowsMultipleValues()
     */
    public boolean allowsMultipleValues() {
        if (super.getBusinessObject() == null){
            // no BO -> do not access BO, just return false
            return false;
        }
        else {
            return super.allowsMultipleValues();
        }
    }

    /* (non-Javadoc)
     * @see com.sap.isa.ipc.ui.jsp.beans.CharacteristicUIBean#getDescription()
     */
    public String getDescription() {
        if (super.getBusinessObject() == null){
            // no BO -> do not access BO, return the internal lName (if available, otherwise the internal name)
            if ((internalLName != null) && (!internalLName.equals(""))){
                return StringUtil.convertStringToHtml(JspUtil.replaceSpecialCharacters(internalLName));
            }
            else {
                return JspUtil.replaceSpecialCharacters(internalName);
            }
        }
        else {
            return super.getDescription();
        }
    }

    /* (non-Javadoc)
     * @see com.sap.isa.ipc.ui.jsp.beans.CharacteristicUIBean#getUnit()
     */
    public String getUnit() {
        if (super.getBusinessObject() == null){
            // no BO -> do not access BO, just return empty string
            return "";
        }
        else {
            return super.getUnit();
        }

    }

    /* (non-Javadoc)
     * @see com.sap.isa.ipc.ui.jsp.beans.CharacteristicUIBean#hasAssignedValues()
     */
    public boolean hasAssignedValues() {
        if (super.getBusinessObject() == null){
            // no BO -> do not access BO, just return false
            return false;
        }
        else {
            return super.hasAssignedValues();
        }
    }

    /* (non-Javadoc)
     * @see com.sap.isa.ipc.ui.jsp.beans.CharacteristicUIBean#hasAssignedValuesByUser()
     */
    public boolean hasAssignedValuesByUser() {
        if (super.getBusinessObject() == null){
            // no BO -> do not access BO, just return false
            return false;
        }
        else {
            return super.hasAssignedValuesByUser();
        }
    }

    /* (non-Javadoc)
     * @see com.sap.isa.ipc.ui.jsp.beans.CharacteristicUIBean#hasIntervalAsDomain()
     */
    public boolean hasIntervalAsDomain() {
        if (super.getBusinessObject() == null){
            // no BO -> do not access BO, just return false
            return false;
        }
        else {
            return super.hasIntervalAsDomain();
        }
    }

    /* (non-Javadoc)
     * @see com.sap.isa.ipc.ui.jsp.beans.CharacteristicUIBean#hasUnit()
     */
    public boolean hasUnit() {
        if (super.getBusinessObject() == null){
            // no BO -> do not access BO, just return false
            return false;
        }
        else {
            return super.hasUnit();
        }
    }

    /* (non-Javadoc)
     * @see com.sap.isa.ipc.ui.jsp.beans.CharacteristicUIBean#isConsistent()
     */
    public boolean isConsistent() {
        if (super.getBusinessObject() == null){
            // no BO -> do not access BO, just return true
            return true;
        }
        else {
            return super.isConsistent();
        }
    }

    /* (non-Javadoc)
     * @see com.sap.isa.ipc.ui.jsp.beans.CharacteristicUIBean#isReadOnly()
     */
    public boolean isReadOnly() {
        if (super.getBusinessObject() == null){
            // no BO -> do not access BO, just return true
            return true;
        }
        else {
            return super.isReadOnly();
        }
    }

    /* (non-Javadoc)
     * @see com.sap.isa.ipc.ui.jsp.beans.CharacteristicUIBean#isRequired()
     */
    public boolean isRequired() {
        if (super.getBusinessObject() == null){
            // no BO -> do not access BO, just return false
            return false;
        }
        else {
            return super.isRequired();
        }
    }

    /* (non-Javadoc)
     * @see com.sap.isa.ipc.ui.jsp.beans.CharacteristicUIBean#isVisible()
     */
    public boolean isVisible() {
        if (super.getBusinessObject() == null){
            // no BO -> do not access BO, just return true
            return true;
        }
        else {
            return super.isVisible();
        }
    }

    public String toString(){
        StringBuffer sb = new StringBuffer();
        sb.append("CharacteristicDeltaUIBean <NAME>");
        sb.append(this.getName());
        sb.append("</NAME>\n");
        sb.append("<HAS-VALUE-DELTAS>");
        sb.append(hasValueDeltas());
        sb.append("</HAS-VALUE-DELTAS>\n");       
        sb.append("<HAS-LOADING-MESSAGES>");
        sb.append(hasLoadingMessages());
        sb.append("</HAS-LOADING-MESSAGES>\n");
        return sb.toString();
    }    


}
