package com.sap.isa.ipc.ui.jsp.beans;

import java.util.ArrayList;
import java.util.List;

import com.sap.isa.core.util.JspUtil;
import com.sap.isa.ipc.ui.jsp.uiclass.UIContext;
import com.sap.spc.remote.client.object.Instance;
import com.sap.spc.remote.client.object.InstanceDelta;
import com.sap.spc.remote.client.object.imp.DefaultInstanceDelta;

/**
 * Wrapper around the InstanceDelta, that is specialized for HTML usage.
 */
public class InstanceDeltaUIBean extends InstanceUIBean {

    public static final String NODELTA = "N";   // type of the delta UIBean if it is only uses 
                                                // as container for loading messages
    private String internalName = ""; // This is used if this deltaUIBean has been created for
                                      // a deleted instance. In this case there is no
                                      // business object. 
    private String internalLName = "";
    private InstanceDelta instDeltaBO;
    private ArrayList loadingMessages;
    private boolean loadingMessagesForCstics;

    /**
     * Creates InstanceDeltaUIBean.
     * @param instDeltaBO business object of the instance delta
     * @param instanceBO business object of the instance
     * @param uiContext
     */
    protected InstanceDeltaUIBean(InstanceDelta instDeltaBO, Instance instanceBO, UIContext uiContext) {
        super(instanceBO, uiContext);
        this.instDeltaBO = instDeltaBO;
        this.loadingMessages = new ArrayList();        
    }

    /**
     * Creates InstanceDeltaUIBean for a deleted instance.
     * @param instDeltaBO business object of the instance delta
     * @param internalName internal name of the instance
     * @param internalLName internal language dependent name of the instance (pass null or empty string if not available)
     * @param uiContext
     */
    protected InstanceDeltaUIBean(InstanceDelta instDeltaBO, String internalName, String internalLName, UIContext uiContext) {
        super(null, uiContext);
        this.instDeltaBO = instDeltaBO;
        this.internalName = internalName;
        this.internalLName = internalLName;
        this.loadingMessages = new ArrayList();        
    }

    /**
     * Creates InstanceDeltaUIBean.
     * @param instDeltaBO business object of the instance delta
     * @param instanceBO business object of the instance
     * @param loadingMessages list of loading messages for this instance
     * @param uiContext
     */
    protected InstanceDeltaUIBean(InstanceDelta instDeltaBO, Instance instanceBO, ArrayList loadingMessages, UIContext uiContext) {
        super(instanceBO, uiContext);
        this.instDeltaBO = instDeltaBO;
        if(loadingMessages == null){
            this.loadingMessages = new ArrayList();
        }
        else{
            this.loadingMessages = loadingMessages;
        }
    }


    /* (non-Javadoc)
     * @see com.sap.isa.ipc.ui.jsp.beans.InstanceUIBean#addCharacteristic(com.sap.isa.ipc.ui.jsp.beans.CharacteristicUIBean)
     */
    public void addCharacteristic(CharacteristicUIBean characteristic) {
        super.addCharacteristic(characteristic);
    }
    
    /**
     * @return true if this instance has characterstic deltas
     */
    public boolean hasCharacteristicDeltas(){
        return (this.characteristics.size()>0);
    }

    /* (non-Javadoc)
     * @see com.sap.isa.ipc.ui.jsp.beans.InstanceUIBean#addGroup(com.sap.isa.ipc.ui.jsp.beans.GroupUIBean)
     */
    public void addGroup(GroupUIBean group) {
        super.addGroup(group);
    }

    
    /**
     * @return true if this instance has group deltas
     */
    public boolean hasGroupDeltas(){
        return (this.groups.size()>0);
    }

    /**
     * @return list of deltas on instance level
     */
    public List getInstanceDeltas(){
        return instDeltaBO.getDeltas();
    }
    
    /**
     * @return list of part of deltas 
     */
    public List getPartOfDeltas(){
        return instDeltaBO.getPartOfDeltas();
    }
    
    /**
     * @return type of instance delta
     */
    public String getType(){
        String type;
        if (instDeltaBO == DefaultInstanceDelta.C_NULL){
            type = NODELTA;
        }
        else {
            type = instDeltaBO.getType();
        }
        return type;
    }
    
    /**
     * Returns business object of instance.
     * Could be null if this instance only contains loading message but no instance delta.
     * @return business object of instance
     */
    public InstanceDelta getDeltaBusinessObject(){
        return instDeltaBO;
    }
    
    /**
     * @return author of instance delta
     */
    public String getAuthor(){
        return instDeltaBO.getAuthor();
    }

    /**
     * Add a single loading message to the list of loading messages.
     * @param loadingMessage
     */
    public void addLoadingMessage(LoadingMessageUIBean loadingMessage){
        if (loadingMessages == null){
            loadingMessages = new ArrayList();
        }
        loadingMessages.add(loadingMessage);
    }
    
    /**
     * @return true if this instance has loading messages
     */
    public boolean hasLoadingMessages(){
        return (loadingMessages.size() > 0);
    }    
    
    /**
     * Returns a list of loading messages for this instance.
     * If there are no loading messages it will return an empty list.
     * @return list of loading messages
     */
    public ArrayList getLoadingMessages(){
        return loadingMessages;
    }

    /**
     * Set a list of loading messages at this instance delta.
     * This overwritten an existing list of loading messages.
     * @param list list of loading messages for this instance
     */
    public void setLoadingMessages(ArrayList list) {
        loadingMessages = list;
    }
    
    /**
     * Returns the language dependent name of the associated business object instance.<br>
     * Remark:<br>
     * It could happen that this bean does not have an associated value BO if
     * this bean has been created for a "deleted" instance.    
     * In this case it returns the internal name (which has to be set before).
     */ 
    public String getLanguageDependentName() {
        String lName;
        if (super.getBusinessObject() == null){
            // no BO -> return the internal lName (if available, otherwise the internal name)
            if ((internalLName != null) && (!internalLName.equals(""))){
                lName = JspUtil.replaceSpecialCharacters(internalLName);
            }
            else {
                lName = JspUtil.replaceSpecialCharacters(internalName);
            }
                        
        }
        else {
            lName = super.getLanguageDependentName();
        }
        return lName;
    }
    
    /**
     * Returns the language independent name of the associated business object instance.<br>
     * Remark:<br>
     * It could happen that this bean does not have an associated value BO if
     * this bean has been created for a "deleted" instance.    
     * In this case it returns the internal name (which has to be set before).
     */ 
    public String getName() {
        String name;
        if (super.getBusinessObject() == null){
            // no BO -> return the internal name
            name = JspUtil.replaceSpecialCharacters(internalName);
        }
        else {
            name = super.getName();
        }
        return name;
    }

    public String getId() {
        String id;
        if (super.getBusinessObject() == null){
            // no BO -> return the id1 of the instance delta
            id = instDeltaBO.getInstId1();            
        }
        else {
            id = super.getId();
        }
        return id;
    }


    /**
     * @return true if one of instance's cstics has a loading message (has to be set manually!) 
     */
    public boolean hasLoadingMessagesForCstics() {
        return loadingMessagesForCstics;
    }

    /**
     * @param flag set to true if one of the instance's cstics has a loading message
     */
    public void setLoadingMessagesForCstics(boolean flag) {
        loadingMessagesForCstics = flag;
    }
    
    /**
     * Overwrite the list of groups.
     * @param groups groups for this instance
     */
    public void setGroups(ArrayList groups){
        this.groups = groups;
    }

    /**
     * Overwrite the list of characteristics.
     * @param characteristics characteristics for this instance.
     */
    public void setCharacteristics(ArrayList characteristics){
        this.characteristics = characteristics;
    }

    /**
     * @return resource key for the remark of this delta.
     */
    public String getRemark(){
        String remark;
        if (getType().equals(InstanceDelta.DELETED)) {
            remark = "ipc.compare.inst.deleted";   
        }
        else if (getType().equals(InstanceDelta.INSERTED)) {
            remark = "ipc.compare.inst.added";
        }
        else {
            remark = "";
        }
        return remark;
    }

    /**
     * The differnt types of deltas a represented by different status icons:
     * <ul>
     * <li>green LED: no delta
     * <li>yellow LED: change (delete, insert, change)
     * </ul>
     * @return path to a status icon for this delta
     */
    public StatusImage getStatusImage(){
        String sourceFile = "";
        String statusDescription = "";
        
        if (getType().equals(NODELTA)){
            sourceFile = "ipc/mimes/images/sys/w_s_ledg.gif";
            statusDescription = "ipc.compare.tt.icon.nochange";
        }
        else if (getType().equals(InstanceDelta.INSERTED) || getType().equals(InstanceDelta.DELETED)){
            sourceFile = "ipc/mimes/images/sys/w_s_ledy.gif";
            statusDescription = "ipc.compare.tt.icon.change";
        }
      
        return UIBeanFactory.getUIBeanFactory().newStatusImage(statusDescription, sourceFile);
    }

    public String toString(){
        StringBuffer sb = new StringBuffer();
        sb.append("InstanceDeltaUIBean <ID>");
        sb.append(this.getId());
        sb.append("</ID>\n");
        sb.append("<NAME>");
        sb.append(this.getName());
        sb.append("</NAME>\n");       
        sb.append("<HAS-LOADING-MESSAGES>");
        sb.append(this.hasLoadingMessages());
        sb.append("</HAS-LOADING-MESSAGES>\n");
        sb.append("<HAS-CSTIC-LOADING-MESSAGES>");
        sb.append(this.hasLoadingMessagesForCstics());
        sb.append("</HAS-CSTIC-LOADING-MESSAGES>\n");
        sb.append("<HAS-GROUP-DELTAS>");
        sb.append(this.hasGroupDeltas());
        sb.append("</HAS-GROUP-DELTAS>\n");
        sb.append("<HAS-CSTIC-DELTAS>");
        sb.append(this.hasCharacteristicDeltas());
        sb.append("</HAS-CSTIC-DELTAS>\n");        
        sb.append("<REMARK>");
        sb.append(this.getRemark());
        sb.append("</REMARK>");       

        return sb.toString();
    }    


}
