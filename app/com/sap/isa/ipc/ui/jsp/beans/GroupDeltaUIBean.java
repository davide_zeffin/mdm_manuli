package com.sap.isa.ipc.ui.jsp.beans;

import java.util.ArrayList;

import javax.servlet.jsp.PageContext;

import com.sap.isa.core.util.WebUtil;
import com.sap.isa.ipc.ui.jsp.uiclass.UIContext;
import com.sap.spc.remote.client.object.CharacteristicGroup;

/**
 * 
 */
public class GroupDeltaUIBean extends GroupUIBean {

    private String internalName = ""; // This is used if this deltaUIBean has been created for
                                      // a group of an deleted instance. In this case there is no
                                      // business object. 

    /**
     * Creates a GroupUIBeanDelta
     * @param groupBO business object of the group
     * @param uiContext
     */
    protected GroupDeltaUIBean(CharacteristicGroup groupBO, UIContext uiContext) {
        super(groupBO, uiContext);
    }

    /**
     * Creates a GroupUIBeanDelta
     * @param groupBO business object of the group
     * @param uiContext
     * @param csticDeltas list of cstic deltas
     */
    protected GroupDeltaUIBean(CharacteristicGroup groupBO, UIContext uiContext, ArrayList csticDeltas) {
        super(groupBO, uiContext);
        this.characteristics = csticDeltas;

    }

    /**
     * Createsa GroupDeltaUIBean for a deleted instance (in this case no BO is available).
     * @param uiContext
     */
    protected GroupDeltaUIBean(UIContext uiContext) {
        super(null, uiContext);
        this.internalName = "ipc.defaultgroup";
    }

    /**
     * @return true if this group has characterstic deltas
     */
    public boolean hasCharacteristicDeltas(){
        return (this.characteristics.size()>0);
    }

    /* (non-Javadoc)
     * @see com.sap.isa.ipc.ui.jsp.beans.GroupUIBean#addCharacteristic(com.sap.isa.ipc.ui.jsp.beans.CharacteristicUIBean)
     */
    public void addCharacteristic(CharacteristicUIBean characteristic) {
        super.addCharacteristic(characteristic);
    }
    
    public String getLanguageDependentName(PageContext pageContext) {
        if (super.getBusinessObject() == null){
            // no BO -> return the internal name
            if (pageContext != null) {
                return WebUtil.translate(pageContext, internalName, null);
            }
            else {
                return internalName;
            }
        }
        else {
            return super.getLanguageDependentName(pageContext);
        }
    }

    public String getName() {
        if (super.getBusinessObject() == null){
            // no BO -> return the internal name
            return internalName;
        }
        else {       
            return super.getName();
        }
    }

    public String toString(){
        StringBuffer sb = new StringBuffer();
        sb.append("GroupDeltaUIBean <NAME>");
        sb.append(this.getName());
        sb.append("</NAME>\n");
        sb.append("<HAS-CSTIC-DELTAS>");
        sb.append(hasCharacteristicDeltas());
        sb.append("</HAS-CSTIC-DELTAS>\n");               
        return sb.toString();
    }    


}
