/*
 * Created on 02.08.2004
 *
 * To change the template for this generated file go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
package com.sap.isa.ipc.ui.jsp.beans;

import java.util.ArrayList;
import java.util.Iterator;

import javax.servlet.jsp.PageContext;

import com.sap.isa.core.util.JspUtil;
import com.sap.isa.core.util.WebUtil;
import com.sap.isa.ipc.ui.jsp.action.visitor.IVisitableObject;
import com.sap.isa.ipc.ui.jsp.action.visitor.IVisitor;
import com.sap.isa.ipc.ui.jsp.uiclass.UIContext;
import com.sap.spc.remote.client.object.CharacteristicGroup;
import com.sap.spc.remote.client.object.OriginalRequestParameterConstants;

/**
 * @author 
 *
 * To change the template for this generated type comment go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
public class GroupUIBean implements IVisitableObject{

//    private static final String GROUP_UI_BEANS = "GROUPUIBEANS";
//    private static final GroupUIBean helperInstance =
//        new GroupUIBean(null, null);
//    private static IsaLocation location =
//        IsaLocation.getInstance(CharacteristicUIBean.class.getName());
    protected CharacteristicGroup groupBO;
	protected UIContext uiContext;
    protected ArrayList characteristics = new ArrayList();
	protected boolean isInScrollArea;
	protected boolean isFirstInScrollArea = false;
	protected boolean anyMimesForWorkArea = false;

    class Key {
		protected String instanceId;
		protected String groupName;

        Key(String instanceId, String groupName) {
            this.instanceId = instanceId;
            this.groupName = groupName;
        }

        public boolean equals(Object o) {
            if (o instanceof Key) {
                Key key = (Key) o;
                if (key.instanceId.equals(this.instanceId)
                    && key.groupName.equals(this.groupName)) {
                    return true;
                }
            }
            return false;
        }

        public int hashCode() {
            return instanceId.hashCode() ^ groupName.hashCode();
        }
        
        public String toString() {
            StringBuffer result = new StringBuffer();
            result.append("Instance:");
            result.append(instanceId);
            result.append(";");
            result.append("Group:");
            result.append(groupName);
            return result.toString();
        }
    }
    
//    public static GroupUIBean getInstance(
//        HttpServletRequest request,
//        CharacteristicGroup group,
//        UIContext uiContext) {
//        HashMap container;
//        Object containerObject = request.getAttribute(GROUP_UI_BEANS);
//        if (containerObject == null) {
//            container = new HashMap();
//            request.setAttribute(GROUP_UI_BEANS, container);
//            if (location.isDebugEnabled()) {
//                location.debug(
//                    "Creating HashMap for "
//                        + GROUP_UI_BEANS
//                        + " and adding to request!");
//            }
//        } else {
//            container = (HashMap) containerObject;
//            if (location.isDebugEnabled()) {
//                location.debug(
//                    "Retrieved HashMap for "
//                        + GROUP_UI_BEANS
//                        + " from request!");
//            }
//        }
//        Key key = helperInstance.new Key(group.getInstance().getId(), group.getName());
//        Object uiBeanObject = container.get(key);
//        if (uiBeanObject == null) {
//            if (location.isDebugEnabled()) {
//                location.debug(
//                    "No GroupUIBean instance for "
//                        + key
//                        + " found! Creating one and adding to container.");
//            }
//            GroupUIBean groupUIBean = new GroupUIBean(group, uiContext);
//            container.put(key, groupUIBean);
//            return groupUIBean;
//        } else {
//            if (location.isDebugEnabled()) {
//                location.debug(
//                    "InstanceUIBean instance for " + key + " found.");
//            }
//            return (GroupUIBean) uiBeanObject;
//        }
//
//    }

    protected GroupUIBean(CharacteristicGroup groupBO, UIContext uiContext) {
        this.groupBO = groupBO;
        this.uiContext = uiContext;
        this.isInScrollArea = false;
    }
    
	/**
	 * In order to give customers the possibility access business objects in JSP pages.
	 * This is useful especially for customer extension data of business objects.
	 * @return
	 */
    public CharacteristicGroup getBusinessObject() {
        return groupBO;
    }
    
	public String getLanguageDependentName(PageContext pageContext) {
		if (groupBO.isBaseGroup()) {
			if (pageContext != null) {
				return WebUtil.translate(pageContext, "ipc.defaultgroup", null);
			} else {
				return "ipc.defaultgroup";
			}
		} else {
			if (uiContext.getShowLanguageDependentNames()
				&& groupBO.getLanguageDependentName() != null) {
				return JspUtil.replaceSpecialCharacters(
					groupBO.getLanguageDependentName());
			} else {
				return JspUtil.replaceSpecialCharacters(groupBO.getName());
			}
		}
	}
    

    public static void main(String[] args) {
    }

    /**
     * @return
     */
    public String getName() {
        return groupBO.getName();
    }

    /**
     * @return
     */
    public boolean isBaseGroup() {
        return groupBO.isBaseGroup();
    }

    /**
     * @return
     */
    public boolean isConsistent() {
        return groupBO.isConsistent();
    }

    /**
     * @return
     */
    public boolean isRequired() {
        return groupBO.isRequired();
    }
    
    public StatusImage getStatusImage() {
        if (!groupBO.getCharacteristics(uiContext.getShowInvisibleCharacteristics()).isEmpty()) { // (id=12)
            if (!groupBO.isRequired() && groupBO.isConsistent()) {
                return UIBeanFactory.getUIBeanFactory().newStatusImage("ipc.config.complete", "ipc/mimes/images/sys/ipc_completeness.gif");
            }
            if (groupBO.isRequired() && groupBO.isConsistent()) {
                return UIBeanFactory.getUIBeanFactory().newStatusImage("ipc.config.incomplete", "ipc/mimes/images/sys/ipc_incompleteness.gif");
            }
            if (!groupBO.isRequired() && !groupBO.isConsistent()) {
                return UIBeanFactory.getUIBeanFactory().newStatusImage("ipc.config.inconsistent", "ipc/mimes/images/sys/ipc_inconsist.gif");
            }
            else {
                return UIBeanFactory.getUIBeanFactory().newStatusImage("ipc.config.inconsistent", "ipc/mimes/images/sys/ipc_incomp_and_inconsist.gif");
            }
        }
        else { // (id=12)
            return UIBeanFactory.getUIBeanFactory().newStatusImage("ipc.group.empty", "ipc/mimes/images/sys/ipc_empty_group.gif");
        } // (id=12)
    }
           
    /**
     * @return
     */
    public ArrayList getCharacteristics() {
        return characteristics;
    }

    /**
     * @param list
     */
    public void addCharacteristic(CharacteristicUIBean characteristic) {
        characteristics.add(characteristic);
    }
    
    /**
     * a b c x
     * 0 0 0 0
     * 0 0 1 0
     * 0 1 0 0
     * 0 1 1 1
     * 1 0 0 0
     * 1 0 1 1
     * 1 1 0 0
     * 1 1 1 1
     * a = uiContext.getShowInvisibleCharacteristics()
     * b = cstic.isVisible()
     * c = cstic.hasAssignedValues()
     * x = return
     * @return
     */
    public boolean hasAssignedValues() {
    	for (Iterator csticIt = getCharacteristics().iterator(); csticIt.hasNext(); ) {
    		CharacteristicUIBean cstic = (CharacteristicUIBean)csticIt.next();
    		if (uiContext.getShowInvisibleCharacteristics() && cstic.hasAssignedValues() ||
    		    cstic.isVisible() && cstic.hasAssignedValues()) {
    			return true;
    		}
    	}
    	return false;
    }

	/**
	 * @return
	 */
	public boolean isInScrollArea() {
		return isInScrollArea;
	}
    
    /**
     * @return true if the group is the first in the scroll area
     */
    public boolean isFirstInScrollArea(){
         return isFirstInScrollArea;
    }
    
    public void setFirstInScrollArea(boolean b){
        isFirstInScrollArea = b;
    }
   

	/**
	 * @param b
	 */
	public void setInScrollArea(boolean b) {
		isInScrollArea = b;
	}
    
    /**
     * In the worka-area (list of cstics and values) not all Mimes can be included.
     * Only the following types will be included directly in the work-area:
     * <li>Icon
     * <li>O2C
     * <li>Sound
     * If either any characteristic or any value of this group has mimes for the work-area the method 
     * will return true.
     * This is set during instantiation process of cstic-mimes and value-mimes
     * 
     * @return true if either any characteristic or any value of this group has mimes for the work-area
     */
    public boolean hasAnyMimesForWorkArea() {
        return this.anyMimesForWorkArea;
    }

    /**
     * Set during instantiation process of cstic-mimes and value-mimes
     * @param set to true if any mime for work-area is available
     */
    public void setAnyMimesForWorkArea(boolean anyMimes) {
        this.anyMimesForWorkArea = anyMimes;
    }

    /**
     * Returns the group name for the work area. <br>
     * The user can customize whether the group name should  be displayed at the beginning 
     * of the work area. This could be necessary if the model has many groups and the user has 
     * the possibility to scroll the group tabs. In this case the user can scroll the current active
     * tab out of the displayed group-tabs. Then the group name in the work area still helps 
     * him to identify the current group that is displayed in the work area.<br>
     * If the model has no groups or only a few (that are always displayed) it might not 
     * make sense to display the group-name in the work area again. In this case it can be 
     * disabled. <br>
     * The XCM parameter "groups.shownameinworkarea" of component "groups" is used to customize the 
     * display of the group name in the work area. The default is "F" (i.e. group name is disabled).
     * @param pageContext
     * @return The group name for the work area. If it is disabled it returns an empty String.
     */
    public String getNameForWorkArea(PageContext pageContext){
        String nameForWorkArea = "";
        boolean showGroupName = uiContext.getPropertyAsBoolean(OriginalRequestParameterConstants.SHOW_GROUP_NAME_IN_WORK_AREA, false);
        if (showGroupName){
            nameForWorkArea = getLanguageDependentName(pageContext);
        }
        return nameForWorkArea;
    }

    public String toString() {
        StringBuffer sb = new StringBuffer();
        sb.append("GroupUIBean <NAME>");
        sb.append(getName());
        sb.append("</NAME><HAS-MIMES>");
        sb.append(hasAnyMimesForWorkArea());
        sb.append("</HAS-MIMES>");
        return sb.toString();
    }

	/* (non-Javadoc)
	 * @see com.sap.isa.ipc.ui.jsp.action.visitor.IVisitableObject#accept(com.sap.isa.ipc.ui.jsp.action.visitor.IVisitor)
	 */
	public void accept(IVisitor visitor) {
		visitor.visit(this);	
	}
}
