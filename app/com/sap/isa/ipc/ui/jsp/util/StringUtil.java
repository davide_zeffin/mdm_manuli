package com.sap.isa.ipc.ui.jsp.util;

import com.sap.isa.ipc.ui.jsp.action.RequestParameterConstants;
import com.sap.isa.ipc.ui.jsp.uiclass.UIContext;
import com.sap.isa.core.logging.IsaLocation;


/**
 * The class contains utility methods to encode white-spaces
 * <blockquote>into non breaking
 * spaces</blockquote>.<br>
 * The ' ' character is encoded to '&nbsp;'<br><br>
 *
 *
 */
public class StringUtil implements RequestParameterConstants {
	/**
	 * defines the characters for a white-space
	 */
	private static final String blank = "&nbsp;&nbsp;";

	/**
	 * Translates a language dependent names' white-spaces into an html <blockquote>non breaking
	 * space</blockquote>. The translation depends on the parameter <code>
	 * RequestParameterConstants.ALLOW_INNER_SPACES_IN_LANGUAGE_DEPENDENT_NAMES
	 * </code>. It's value is checked in the method.<br>
	 * Note: there must be at least 2 whitespaces for replace!
	 *
	 * @param String the String which should be translated
	 * @param UIContext used to check the parameter
	 * @return the encoded String
	 */
	public static String encodeLanguageDependentName(String toEncode, UIContext uiContext) {
		if (!uiContext.getPropertyAsBoolean(SHOW_INNER_SPACES_IN_LANGUAGE_DEPENDENT_NAMES))
			return toEncode;
		else
		    return replaceString(toEncode, "  ");
	}

	/**
	 * Translates a language dependent names' white-spaces into an html <blockquote>non breaking
	 * space</blockquote>. The translation depends on the parameter <code>
	 * RequestParameterConstants.ALLOW_INNER_SPACES_IN_LANGUAGE_INDEPENDENT_NAMES
	 * </code>. It's value is checked in the method.<br>
	 * Note: there must be at least 2 whitespaces for replace!
	 *
	 * @param String the String which should be translated
	 * @param UIContext used to check the parameter
	 * @return the encoded String
	 */
	public static String encodeLanguageIndependentName(String toEncode, UIContext uiContext) {
		if (!uiContext.getPropertyAsBoolean(SHOW_INNER_SPACES_IN_LANGUAGE_INDEPENDENT_NAMES))
		   return toEncode;
		else
			return replaceString(toEncode, "  ");
	}

	/**
	 * Replaces in the String s all occurrences of Stirng search by
	 * '&nbsp;'.  If search is empty, s is returned unchanged.
	 */
	private static String replaceString(String s, String search) {
		   String ret = "";
	       if (search.length() <= 0) {
			  ret = s;
		   } else {
	         int pos;
	         while ((pos = s.indexOf(search)) != -1) {
				   ret += s.substring(0, pos) + blank;
		           s = s.substring(pos + search.length());
			 }
			 ret += s;
			}
	        return ret;
	}

    /**
     * Converts a string to Html representation
     * replaces <,>,&,\n," to right Html code
     */
    public static String convertStringToHtml(String str){
        String sHtmlFormat = str;
        sHtmlFormat = replace(sHtmlFormat, "\n", "<br>");

        return sHtmlFormat;
    }


    private static String replace(String src, String replaceA, String withB){
        String ret=src;
        int pos=0, oldPos=0;
        while (pos >=0){
            pos = ret.indexOf(replaceA, oldPos);
            if (pos < 0) continue;
            ret = ret.substring(0, pos)+ withB + ret.substring(pos+replaceA.length());
            oldPos = pos + withB.length();
        }
        return ret;
    }

	static public String byteToHex(byte b) {
	   // Returns hex String representation of byte b
	   char hexDigit[] = {
		  '0', '1', '2', '3', '4', '5', '6', '7',
		  '8', '9', 'A', 'B', 'C', 'D', 'E', 'F'
	   };
	   char[] array = { hexDigit[(b >> 4) & 0x0f], hexDigit[b & 0x0f] };
	   return new String(array);
	}

	public static StringBuffer getUTF16String(byte[] array) {

	   StringBuffer retString = new StringBuffer();

	   for (int k = 2; k < array.length; k++) 
		   retString.append(byteToHex(array[k]));

	   return retString;
	}

    
	public static String toUTF16String(String value)  {
		
		try {		
			byte utf16Bytes[] = value.getBytes("UTF-16");
			
			String utf16string = new String(utf16Bytes, "UTF-16");
			
			StringBuffer retString = new StringBuffer();			
			retString.append(getUTF16String(utf16Bytes));		

			return retString.toString().toUpperCase();			
		}catch (Exception e) {
		   e.printStackTrace();
		   return "";
		}		
	}   
	/**
			 * Returns the first line of the description but only up to the maintained threshold (values.maxdescriptionlength).
			 * If the line is longer it will be cut off at this length.
			 * If the description starts with a line-break (this might be the case for products modelled in CRM) it removes
			 * the first line-break.
			 * If the description is not maintained it returns the language-dependent name
			 * @return the first line of the description (up to the threshold)
			 */
			public static String getDescriptionFirstLine(String maxLength, String descr, String lengthLimitedDescription, String languageDependentName, IsaLocation location) {
				String name;
				// Get the threshold
				//String maxLength = uiContext.getValuesMaxDescriptionLength();
				int maxLengthI = 80;
				if(maxLength != null && maxLength.length()>0) {
					maxLengthI = Integer.parseInt(maxLength);
				}
				else {
					if(location.isDebugEnabled())
						location.debug("XCM value characteristics.maxdescriptionlength was not resolved. Use default length 80 instead");
				}
				// Check whether it contains a line break before the maxlength.
			//    String descr = getDescription();
				boolean lineBreakBeforeMaxLength= false;
				int pos = -1;
				if (descr != null){
					// Check whether there is a line-break at the first position (CRM-PME issue): remove it
					pos = descr.indexOf("<br>");
					if (pos == 0){
						if (descr.length() > 3){
							descr = descr.substring(4, descr.length());
						}
						// check for the position of the next line-break
						pos = descr.indexOf("<br>");
					}
					if ((pos > 0) && (pos <= maxLengthI)){
						lineBreakBeforeMaxLength = true;
					}
				}

				if (descr != null && lineBreakBeforeMaxLength){
					name = descr.substring(0, pos);
				}
				else {
					name = lengthLimitedDescription;
				}
				// fallback if the description is not maintained:
				if (name != null && name.equals("")){
		//            name = getLanguageDependentName();
					  name = languageDependentName;
				}
				return name;
			}
	
    
}
