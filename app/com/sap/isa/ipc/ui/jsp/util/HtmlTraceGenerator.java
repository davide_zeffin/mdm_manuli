/*
 * Created on Mar 3, 2005
 *
 * To change the template for this generated file go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
package com.sap.isa.ipc.ui.jsp.util;

import java.text.SimpleDateFormat;
import java.util.Enumeration;

import javax.servlet.http.HttpServletRequest;

import com.sap.isa.core.logging.IsaLocation;


/**
 * @author I026583
 *
 * To change the template for this generated type comment go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
public class HtmlTraceGenerator {
	
	//The trace file is divided into different parts HEADER BODY and FOOTER
	private final static int HEADER=0;
	private final static int BODY=1;
	private final static int FOOTER=2;
	
	protected static IsaLocation log = IsaLocation.getInstance(HtmlTraceGenerator.class.getName());
	
	private StringBuffer traceContent;
	
	
	public HtmlTraceGenerator() {
		traceContent=new StringBuffer();
	}
	
	
	private void addHiddenVariable(StringBuffer strbuf,String name,String value) {
		strbuf.append("\t\r\n<input type=\"hidden\" name=\"");
		strbuf.append(name);
		strbuf.append("\" value=\"");
		strbuf.append(value);
		strbuf.append("\" > ");
		
	}
	
	private void buildTraceContent(int area,HttpServletRequest request) {
		String keyName=null;
		String keyValue=null;
		if (traceContent==null || request==null) return;
		switch(area) {
			//Header
			case 0:	traceContent.append("\r\n<!-- start IPC with this HTML-Form at -->");
					traceContent.append("\r\n<HTML>\r\n<BODY>");
					java.util.Date traceDate=new java.util.Date(System.currentTimeMillis());
					SimpleDateFormat dateFormat=new SimpleDateFormat("EEE, d MMM yyyy HH:mm:ss");
					//traceContent.append("\r\n<!--"+new java.util.Date(System.currentTimeMillis()).toLocaleString()+"-->");
					traceContent.append("\r\n<!--"+dateFormat.format(traceDate)+"-->");
					traceContent.append("\r\n<a id=\"hookURL2\" href=\"SAPEVENT:CFG\"> </a>");
					traceContent.append("\r\n<FORM NAME=\"datapipe\"   METHOD=\"POST\"");
					traceContent.append("ACTION=\"");
					StringBuffer url=request.getRequestURL();
					traceContent.append(url);
					traceContent.append("\">");
					break;
			//Body
			case 1: traceContent.append("\r\n");
					Enumeration enum=request.getParameterNames();
					while (enum.hasMoreElements()) {
						keyName=(String) enum.nextElement();
						keyValue=request.getParameter(keyName);
						addHiddenVariable(traceContent,keyName,keyValue);
					}
					break;
			
			//Footer
			 default: 	traceContent.append("\r\n<SCRIPT LANGUAGE=\"JavaScript\">");
							traceContent.append("\r\n\tdocument.datapipe.hookURL.value=document.links[0];");
							traceContent.append("\r\n\tdocument.datapipe.submit();");
							traceContent.append("\r\n</SCRIPT>\r\n");
							traceContent.append("\r\n</FORM>");
							traceContent.append("\r\n</BODY>");
							traceContent.append("\r\n</HTML>\r\n");				
							traceContent.append("\r\n<!-- end of HTML Trace-->");
							break;
		}
	}
	
	
	/**
	 * This method has to be called with a valid request and uicontext.
	 * @param request
	 * @param uicontext
	 */
	public void trace(HttpServletRequest request) {
		if (request==null) {
			//TODO: add a log for this serious problem
			return;
		}
			 //for (int i=0;i<3;i++) buildTraceContent(i,request);
			 buildTraceContent(HEADER,request);
			 buildTraceContent(BODY,request);
			 buildTraceContent(FOOTER,request);
			 log.debug(traceContent);
			 
		}
	

	

}
