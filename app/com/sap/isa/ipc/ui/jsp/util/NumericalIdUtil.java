package com.sap.isa.ipc.ui.jsp.util;

import java.math.BigInteger;

import com.sap.isa.core.logging.IsaLocation;

/**
 * Utilities for numerical Ids.
 */
public class NumericalIdUtil {

    protected static IsaLocation log = IsaLocation.getInstance(NumericalIdUtil.class.getName());


    /**
     * Checks whether a given productId is numerical.
     * @param productId The productId to check.
     * @return true if the id is numerical.
     */
    public static boolean isNumerical(String productId){
        boolean isNumerical = true;
        try {
            BigInteger bigInt = new BigInteger(productId);
        }
        catch(NumberFormatException nfe){
            isNumerical = false;
        }
        return isNumerical;
    }
    
    /**
     * Compares two numerical product ids for numerical equality, i.e. if "00123" and "123" is passed this
     * method will return true because both ids are numerical and numerical equal.
     * If one of the ids is not a numerical id it will return false.
     *  
     * @param idOne First product id as String.
     * @param idTwo Second product id as String.
     * @return true if the ids are equal and both are numerical.
     */
    public static boolean compareNumericalIds(String idOne, String idTwo){
        boolean isEqual = false;        
        try {
            BigInteger bigOne = new BigInteger(idOne);
            BigInteger bigTwo = new BigInteger(idTwo);
            isEqual = bigOne.equals(bigTwo);
        }
        catch(NumberFormatException nfe){
            log.error("Comparison of numerical product ids: one of the two product ids is not numerical!");
        }        
        return isEqual;
    }    
    
    
}
