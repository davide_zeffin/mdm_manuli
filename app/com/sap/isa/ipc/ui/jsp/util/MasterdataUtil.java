package com.sap.isa.ipc.ui.jsp.util;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.jsp.PageContext;

import com.sap.isa.core.util.WebUtil;


/**
 *  Utility class for masterdata MIME documents.
 */
public class MasterdataUtil {

    /**
     *  Generates the mime-URL of the following pattern
     *  %mimeServer%/%theme%/%language%/%name%
     *  If the mimeServer parameter is an empty string the root of the application is used.
     *
     *@param pageContext the current PageContext
     *@param  mimesServer  URL of the server where the MIME documents are stored. Application-root is used if it is an empty string.
     *@param  theme        may be null, in this case it is ignored 
     *@param  language     may be null, in this case it is ignored
     *@param  name         is the resource
     *@return              the complete mime-URL
     */
    public static String getMimeURL(PageContext pageContext, String mimesServer, String theme, String language, String name){
        if (mimesServer == null || mimesServer.trim().length() == 0) {
            HttpServletRequest request = (HttpServletRequest) pageContext.getRequest();
            mimesServer = request.getContextPath();
        }
        return WebUtil.getMimeURL(mimesServer, theme, language, name);
    }

}
