/*
 * Created on 02.10.2005
 *
 * To change the template for this generated file go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
package com.sap.isa.ipc.ui.jsp.util;

import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

import org.apache.commons.digester.Digester;

import com.sap.isa.core.FrameworkConfigManager;
import com.sap.isa.core.init.Initializable;
import com.sap.isa.core.init.InitializationEnvironment;
import com.sap.isa.core.init.InitializeException;
import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.core.logging.LogUtil;
import com.sap.isa.core.xcm.ConfigContainer;

/**
 * @author 
 *
 * To change the template for this generated type comment go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
public class JSPIncludeHandler implements Initializable {

	/**
	 * Reference to the IsaLocation. 
	 *
	 * @see com.sap.isa.core.logging.IsaLocation
	 */
	protected static IsaLocation log = IsaLocation.
		  getInstance(JSPIncludeHandler.class.getName());

	// map with the include mappings
	private static Map includes; 

	public static String getJSPInclude(String paramName) {
		String jspInclude = (String)includes.get(paramName);
		if (jspInclude == null) {
			log.error("jspinclude for " + paramName + "not found in jspinclude-config.xml!");
		}
		return jspInclude;
	}
	
	/* (non-Javadoc)
	 * @see com.sap.isa.core.init.Initializable#initialize(com.sap.isa.core.init.InitializationEnvironment, java.util.Properties)
	 */
	public void initialize(InitializationEnvironment env, Properties props)
		throws InitializeException {
			includes = new HashMap(20);   				
			ConfigContainer configContainer = FrameworkConfigManager.XCM.getXCMScenarioConfig(FrameworkConfigManager.NAME_BOOTSTRAP_SCENARIO);

			if (configContainer != null) {
			
				InputStream inputStream = 
					configContainer.getConfigUsingAliasAsStream("jspinclude-config");
				if (inputStream != null) {	
					parseConfigFile(inputStream);
					try {
						inputStream.close();
					}
					catch (IOException e) {
						log.error(LogUtil.APPS_BUSINESS_LOGIC,"io.exception",e);
					}
				}
				else {
					log.debug("Entry [jspinclude-config] in xcm-config.xml missing"); 
				}
			}
			else {
				log.debug("Scenario [FrameworkConfigManager.NAME_BOOTSTRAP_SCENARIO] missing"); 
			}
	}

	/**
	 * Terminate the Handler . <br>
	 * 
	 * @see com.sap.isa.core.init.Initializable#terminate()
	 */
	public void terminate() {
		includes.clear();
		includes=null;    	
	}

	private static String CRLF = System.getProperty("line.separator");

	/* private method to parse the config-file */
	private void parseConfigFile(InputStream inputStream)
			throws InitializeException {

		// new Struts digester
		Digester digester = new Digester();

		digester.push(this);

		String rootTag = "jspincludes/jspinclude";
		
		digester.push(includes);
		digester.addCallMethod(rootTag, "put", 2, new Class[] {Object.class, Object.class});
		digester.addCallParam("jspincludes/jspinclude", 0, "name");
		digester.addCallParam("jspincludes/jspinclude", 1, "value");

		try {
			digester.parse(inputStream);
		}
		catch (java.lang.Exception ex) {
			String errMsg = "Error reading configuration information" + CRLF + ex.toString();
			throw new InitializeException(errMsg);
		}

	}
}
