/*
 * Created on 13.11.2003
 *
 * To change the template for this generated file go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
package com.sap.isa.ipc.ui.jsp.util.vms;

/**
 * @author 
 * 
 * Contains http request parameter constants for vehicle manager compatibility.
 * @see com.sap.spc.remote.client.object.OriginalRequestParameterConstants
 * @see HttParamMapper
 * Only parameter that differ in their naming from the 4.0 parameter names
 * will be defined here.
 *
 */
public interface HttpRequestParameterConstants {
	/**
	 * display mode on ({@link #T}) or off ({@link #F}). In the display mode you can't modify the
	 * configuration. It is open for display only.
	 */
	public static final String DISPLAY_MODE = "display";
	/**
	 * The IPC server may handle several clients (MANDT). Analog to the login
	 * into a SAP system (R/3 or CRM), you need to specify the client here.
	 */
	public static final String IPC_CLIENT = "IPC_CLIENT";
	/**
	 * Only used in R/3 scenarios.
	 * Simulates an R/3 event in order to tell the calling application that
	 * the configuration process is finished.
	 * The VehicleManager passes the value "SAPEVENT:CFG".
	 */
	public static final String HOOK_URL = "hookURL";
	/**
	 * Activate({@link #T}) finding product variants or not ({@link #F}).
	 */
	public static final String ENABLE_VARIANT_SEARCH = "enableVariantSearch";

}
