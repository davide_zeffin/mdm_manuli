package com.sap.isa.ipc.ui.jsp.util;

import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Locale;
import java.util.regex.PatternSyntaxException;
import java.text.*;

import com.sap.isa.core.logging.IsaLocation;
import com.sap.spc.remote.client.object.Characteristic;
import com.sap.spc.remote.client.object.CharacteristicValue;

/**
 * Format validation for additional value fields.
 */

public class FormatValidator {
    
    private static final FormatValidator formatValidator = new FormatValidator();
	protected static IsaLocation log = IsaLocation.
		   getInstance(FormatValidator.class.getName());
    
    /**
     * Factory for FormatValidator
     * 
     * @return Instance of FormatValidator.
     */
    public static final FormatValidator getInstance() {
        return formatValidator;
    }


    public class FormatError {
        public final static int OK = 0;
        public final static int VALUE_TOO_LONG = 1; //"VALUE_TOO_LONG";
        public final static int INVALID = 2; //"VALUE_INVALID";
        public final static int FRACT_TOO_LONG = 4; //"FRACT_TOO_LONG";
        public final static int INT_TOO_LONG = 8; //"INT_TOO_LONG";
        public final static int DATE_INVALID = 16; //"DATE_INVALID";
        public final static int MONTH_INVALID = 32; //"MONTH_INVALID";
        public final static int DAY_INVALID = 64; //"DAY_INVALID";
        private int errorCode;
        
        public FormatError(int errorCode) {
            this.errorCode = errorCode;
        }

        public String toString(){
            return String.valueOf(this.errorCode);
        }

        /**
         * @return true if validation was successful.
         */
        public boolean isOk() {
            return (errorCode ^ OK) == 0;
        }
        
        /**
         * @return
         */
        public boolean isValueTooLong() {
            return (errorCode & VALUE_TOO_LONG) != 0;
        }

        /**
         * @return
         */
        public boolean isInvalid() {
            return (errorCode & INVALID) != 0;
        }
        /**
         * @return
         */
        public boolean isFractTooLong() {
            return (errorCode & FRACT_TOO_LONG) != 0;
        }

        /**
         * @return
         */
        public boolean isIntTooLong() {
            return (errorCode & INT_TOO_LONG) != 0;
        }

        /**
         * @return
         */
        public boolean isDateInvalid() {
            return (errorCode & DATE_INVALID) != 0;
        }

        /**
         * @return
         */
        public boolean isDayInvalid() {
            return (errorCode & DAY_INVALID) != 0;
        }

        /**
         * @return
         */
        public boolean isMonthInvalid() {
            return (errorCode & MONTH_INVALID) != 0;
        }

    }
    
    public FormatValidator() {
    }

    /**
     * Validates a value. Uses the characteristic object and the locale to retrieve the 
     * needed information.
     * 
     * @param cstic Characteristic of this value. Used to get the right length, etc.
     * @param value Value to be validated.
     * @param locale Current locale used to validate language dependent.
     * @return FormatError depending on the validation process.
     */
    public static FormatError validate(Characteristic cstic, String value, Locale locale) {
        String entryFieldMask = cstic.getEntryFieldMask(); // language independent entry field mask
        int typeLength = cstic.getTypeLength(); // length without thousand-separator, sign, etc. pure number of digits 
        int numberScale = cstic.getNumberScale(); // digits behind decimal separator (fraction)
        String internal = null;
        Locale loc = locale;
        int type = cstic.getValueType();

        String template = entryFieldMask;

        int totalLength, fractionLength, integerLength;
        boolean negativesAllowed;
        if (typeLength == 0 && template != null) {
            // if no lengthVal is given, check the template
            totalLength = template.length();
            fractionLength = 0;
            integerLength = 0;
        }
        else {
            totalLength = typeLength;
            // check for decimal place separator
            if (numberScale > 0) {
                fractionLength = numberScale;
            }
            else {
                fractionLength = 0;
            }
            // calculate the integer-length (digits before decimal separator)
            integerLength = totalLength - fractionLength;
            // check if we need to allow negative numbers
            if (template != null && template.startsWith("-")) {
                totalLength++;
                integerLength++;
                negativesAllowed = true;
            }

            // one char for the "." separator between int and fraction parts
            if (fractionLength > 0) {
                totalLength++;
            }
        }
        // type: string
        if (type == Characteristic.TYPE_STRING) {
            if (value.length() > totalLength) {
                return formatValidator.new FormatError(FormatError.VALUE_TOO_LONG);
            }
            return formatValidator.new FormatError(FormatError.OK);
        }
        // type: integer (1) or float (2)
        else if ((type == Characteristic.TYPE_INTEGER) || (type == Characteristic.TYPE_FLOAT)) {
            // check for valid characters using a regular expression
            boolean containsValueValidCharacters = checkNumberForValidCharacters(value, loc);
            // return error if value contains invalid characters
            if (!containsValueValidCharacters) {
                return formatValidator.new FormatError(FormatError.INVALID);
            }           
            // convert value to language independent format
            try {
                internal = toLanguageIndependent(value, locale);
            } catch (ParseException pe) {
                return formatValidator.new FormatError(FormatError.INVALID);
            }
            /*
             * Check whether it's possible to use language independent validation.
             * E.g. if the integer part is longer than 8 digits, java uses the scientific notation
             * for the language independent notation, so it's not possible to check easily the
             * correct integer and fraction part.
             * In this case (=java is using the scientific notation) the value is currently left unchecked.
             */
            if ((internal.indexOf("E") != -1)) {
                return formatValidator.new FormatError(FormatError.OK);
            }
            else {
                return formatValidator.new FormatError(checkLanguageIndependent(internal, integerLength, fractionLength));
            }
        }
        // type: date (4)
        else if (type == Characteristic.TYPE_DATE) {
            return formatValidator.new FormatError(checkDateFormat(value, locale));
        }
        // other value types than String (0), integer (1), float (2) and date (4) left unchecked
        else {
            return formatValidator.new FormatError(FormatError.OK);
        }
    }


    /**
     * try to convert value to language independent notation
     */
    private static String toLanguageIndependent(String originalValue, Locale loc) throws ParseException {
        String internal = "";
        // get the number format for the locale
        NumberFormat format = NumberFormat.getNumberInstance(loc);
        // thousands separators are supported
        format.setGroupingUsed(true);
        Number num = format.parse(originalValue);
        internal = num.toString();
        return internal;
    }
    
    /**
     * This methods converts a numerical value to the language independent notation.
     * The method is only designed for conversion of numerical values (Characteristic.TYPE_INTEGER) 
     * and Characteristic.TYPE_FLOAT). If the characteristic is not of these types the method returns
     * the passed value unconverted.
     * @param cstic Characteristic to which the value belongs.
     * @param originalValue The value entered by the user that should be converted.
     * @param loc Locale 
     * @return The internal representation of a numerical value (otherwise the original value is returned)
     */
    public static String convertNumericValueToIndependent(Characteristic cstic, String originalValue, Locale loc){
        int type = cstic.getValueType();
		CharacteristicValue internal_value = cstic.getValueFromLanguageDependentName(originalValue);
		if (internal_value != null) {
			return internal_value.getName();
		}
		String internal = originalValue;
        if ((type == Characteristic.TYPE_INTEGER) || (type == Characteristic.TYPE_FLOAT)){
            try {
                internal = toLanguageIndependent(originalValue, loc);
            } catch (ParseException pe) {
                // just don't change internal (then internal == originalValue)
				log.debug(pe.getMessage(), pe);
            }
            // Check whether the number has a fraction part.
            int dotpos = internal.indexOf(".");
            // If it doesn't have a fraction part, we have to add ".0" because the internal representation
            // for integers has always ".0" (e.g. the number "2" is internally represented as "2.0")
            // but we could not automatically cut the fraction part to ".0" using Java NumberFormat class
            // because this would lead to converting e.g. "2.25" always to "2.3". This is not wanted.
            if (dotpos == -1){
                internal = internal + ".0";
            }
        }
        return internal;
    }

    /**
     * Checks the value language independently.
     * 
     * @param internal - language independent value to be validated
     * @param intLength - Length of integer part (before decimal separator)
     * @param fractionLength - Length of fraction part (after decimal separator)
     * @return FormatError code.
     */
    private static int checkLanguageIndependent(String internal, int intLength, int fractionLength) {
        int formatError = 0;
        int dotpos = internal.indexOf(".");
        if (dotpos != -1) {
            // fraction part is too long
            if ((internal.length() - dotpos - 1) > fractionLength) {
                formatError = FormatError.FRACT_TOO_LONG;
            }
            // integer part is too long
            if (dotpos > intLength) {
                formatError = formatError | FormatError.INT_TOO_LONG;
            }
            return formatError;
        }
        else {
            // number is too long
            if (internal.length() > intLength) {
                return FormatError.VALUE_TOO_LONG;
            }
        }
        return formatError;
    }

    /**
     * Is date entered in a valid language-dependent format or at least in  
     * internal format YYYYMMDD?     
     * 
     * @param value -  The date to be validated.
     * @param loc - Locale to use for language dependent validation of the date.
     * @return The FormatError code.
     */
    private static int checkDateFormat(String value, Locale loc) {
        Date result;
        int valid = 0;
        try {
            int number = Integer.parseInt(value);
            if (value.length() != 8) {
                valid = FormatError.DATE_INVALID;
                return valid;
            }
            int year = number / 10000;
            int inter = number % 10000;
            int month = inter / 100;
            int day = inter % 100;
            if (month >12 || month == 0) {
                valid += FormatError.MONTH_INVALID;
            }
            if (day > 31 || day == 0) {
                valid += FormatError.DAY_INVALID;
            }
        }
        catch(NumberFormatException nfe) {
            // not a number => invoke the language-dependent parser
            SimpleDateFormat localizedParser = (SimpleDateFormat)SimpleDateFormat.getDateInstance(DateFormat.SHORT, loc);
            localizedParser.setLenient(false);
            try {
                result = localizedParser.parse(value);
            }
            catch (ParseException pex) {
                valid += FormatError.DATE_INVALID;
            }
        }
        return valid;
    }
    
    /**
     * Check for valid characters in a number using regular expressions.
     * 
     * @param numericalValue - The number to be checked.
     * @param loc - The current locale used for validation.
     * @return true if numerical value contains only valid characters.
     */   
    private static boolean checkNumberForValidCharacters(String numericalValue, Locale loc) {
        boolean valid = false;
        // call method for building regexp
        String regExp = buildRegularExpression(loc);
        try {
            valid = numericalValue.matches(regExp);
        }
        catch (PatternSyntaxException psEx) {
			log.debug("The pattern syntax is not valid", psEx);
            // TODO logging: the pattern syntax is not valid
        }
        return valid;
    }
    
    /**
     * Uses the current locale to build the regular expression.
     * 
     * @param loc - Locale for building the localized regular expression.
     * @return Localized regular expression as String.
     */
    private static String buildRegularExpression(Locale loc) {
        DecimalFormat format = (DecimalFormat) NumberFormat.getInstance(loc);
        char groupingSeparator = format.getDecimalFormatSymbols().getGroupingSeparator();
        char decimalSeparator = format.getDecimalFormatSymbols().getDecimalSeparator();
        // build regExp: [-+0-9{groupingSeparator}{decimalSeparator}E]*
        StringBuffer localizedRegExp = new StringBuffer("");
        localizedRegExp.append("[-+0-9");
        localizedRegExp.append(groupingSeparator);
        localizedRegExp.append(decimalSeparator);
        localizedRegExp.append("E]*");
        return localizedRegExp.toString();
    }
    
    
    
    /**
     * This methods converts a numerical value to the language independent notation.
     * The method is only designed for conversion of numerical values (Characteristic.TYPE_INTEGER),  
     * Characteristic.TYPE_FLOAT, Characteristic.TYPE_TIME andCharacteristic.TYPE_CURRENCY).
     * If the characteristic is not of these types the method returns
     * the passed value unconverted.
     * @param cstic Characteristic to which the value belongs.
     * @param independent The internal value that should be converted.
     * @param loc Locale 
     * @return The localized representation of a numerical value (otherwise the independent value is returned)
     */
    public static String convertNumericValueToLocalized(Characteristic cstic, String independent, Locale loc){
        int type = cstic.getValueType();
        String formatted = independent;
        if (independent == null  ||  independent.equals("") || loc == null){
            return independent;
        }
        if ((type == Characteristic.TYPE_INTEGER) || (type == Characteristic.TYPE_FLOAT) || (type == Characteristic.TYPE_TIME) || (type == Characteristic.TYPE_CURRENCY)){
            int typeLength = cstic.getTypeLength(); // length without thousand-separator, sign, etc. pure number of digits 
            int numberScale = cstic.getNumberScale(); // digits behind decimal separator (fraction)
            String template = cstic.getEntryFieldMask(); // language independent entry field mask
    
            int totalLength, fractionLength, integerLength;
            boolean negativesAllowed;
            if (typeLength == 0 && template != null) {
                // if no lengthVal is given, check the template
                totalLength = template.length();
                fractionLength = 0;
                integerLength = 0;
            }
            else {
                totalLength = typeLength;
                // check for decimal place separator
                if (numberScale > 0) {
                    fractionLength = numberScale;
                }
                else {
                    fractionLength = 0;
                }
                // calculate the integer-length (digits before decimal separator)
                integerLength = totalLength - fractionLength;
                // check if we need to allow negative numbers
                if (template != null && template.startsWith("-")) {
                    totalLength++;
                    integerLength++;
                    negativesAllowed = true;
                }
    
                // one char for the "." separator between int and fraction parts
                if (fractionLength > 0) {
                    totalLength++;
                }
            }        
            
            NumberFormat format = NumberFormat.getNumberInstance(loc);                        
            format.setGroupingUsed(true);
            format.setMaximumIntegerDigits(integerLength);
            format.setMaximumFractionDigits(fractionLength);
            format.setMinimumFractionDigits(fractionLength);
    
            // convert EAL representation to the number
            try {
                double value = Double.valueOf(independent).doubleValue();
                formatted = format.format(value);
                // is there a faster way to find out if the formatting lost data?
                try {
                    // two things may happen: either we got an overflow (ie. our number got cut
                    // off and the result is more than one different than it should be.
                    // The second is underflow, where the number has changed slightly.
                    Number num = format.parse(formatted);
                    double diff = Math.abs(num.doubleValue()-value);
                    if (diff > Double.MIN_VALUE * 65536) {
                        if (diff > 1-Double.MIN_VALUE * 65536) {
                            formatted = "!"+formatted.substring(0,formatted.length()-2)+"!";
                        }
                    }
                }
                catch(ParseException e) {
                    formatted = independent;
                }
            }
            catch(NumberFormatException e) {
                formatted = independent;
            }
        }
        return formatted;
    }

    /**
     * This methods converts a date value from the internal YYYMMDD format to the language independent notation.
     * The method is only designed for conversion of date values (Characteristic.DATE).
     * If the characteristic is not of this type the method returns
     * the passed value unconverted.
     * @param cstic Characteristic to which the value belongs.
     * @param independentValue The internal value that should be converted.
     * @param loc Locale 
     * @return The localized representation of a numerical value (otherwise the independent value is returned)
     * @return
     */
    public static String convertDateValueToLocalized(Characteristic cstic, String independent, Locale loc) {
        int type = cstic.getValueType();
        String formatted = independent;
        if (independent == null  ||  independent.equals("") || loc == null){
            return independent;
        }    
        DateFormat format = DateFormat.getDateInstance(DateFormat.SHORT, loc);
        format.setLenient(false);
        if ((type == Characteristic.TYPE_DATE)){
            try {
                int dateVal = Integer.parseInt(independent); 
                int day = dateVal % 100;
                dateVal = dateVal / 100;
                int month = dateVal % 100;
                int year = dateVal / 100;
                Date d = new GregorianCalendar(year,month-1,day).getTime();
                formatted = format.format(d);
            }
            catch (NumberFormatException nfe){
                // we return the independent format
				log.debug(nfe.getMessage(), nfe);
            }
        }
        return formatted;
    }
    
   public static void main(String[] args) {
        FormatError error = formatValidator.new FormatError(FormatError.DATE_INVALID);
        if (error.isDateInvalid()) System.out.println("Date invalid! OK!"); else System.out.println("Error in isDateInvalid()");
        error = formatValidator.new FormatError(FormatError.DAY_INVALID);
        if (error.isDayInvalid()) System.out.println("Day invalid! OK!"); else System.out.println("Error in isDayInvalid()");
        error = formatValidator.new FormatError(FormatError.FRACT_TOO_LONG);
        if (error.isFractTooLong()) System.out.println("Fract too long! OK!"); else System.out.println("Error in isFractTooLong()");
        error = formatValidator.new FormatError(FormatError.INT_TOO_LONG);
        if (error.isIntTooLong()) System.out.println ("Int too long! OK!"); else System.out.println("Error in isIntTooLong()");
        error = formatValidator.new FormatError(FormatError.INVALID);
        if (error.isInvalid()) System.out.println("Invalid! OK!"); else System.out.println("Error in isInvalid()");
        error = formatValidator.new FormatError(FormatError.MONTH_INVALID);
        if (error.isMonthInvalid()) System.out.println("Month invalid! OK!"); else System.out.println("Error in isMonthInvalid()");
        error = formatValidator.new FormatError(FormatError.OK);
        if (error.isOk()) System.out.println("OK ! OK!"); else System.out.println("Error in isOK()");
        error = formatValidator.new FormatError(FormatError.VALUE_TOO_LONG);
        if (error.isValueTooLong()) System.out.println("Value too long! Ok!"); else System.out.println("Error in isValueTooLong()");
        for (int i = 0; i < 256; i++) {
            error = formatValidator.new FormatError(i);
            System.out.print(i + ":");
            if (error.isDateInvalid()) System.out.print("Date invalid! OK!");
            if (error.isDayInvalid()) System.out.print("Day invalid! OK!");
            if (error.isFractTooLong()) System.out.print("Fract too long! OK!");
            if (error.isIntTooLong()) System.out.print ("Int too long! OK!");
            if (error.isInvalid()) System.out.print("Invalid! OK!");
            if (error.isMonthInvalid()) System.out.print("Month invalid! OK!");
            if (error.isOk()) System.out.print("OK ! OK!");
            if (error.isValueTooLong()) System.out.print("Value too long! Ok!");
            System.out.println();
        }
    }
}
