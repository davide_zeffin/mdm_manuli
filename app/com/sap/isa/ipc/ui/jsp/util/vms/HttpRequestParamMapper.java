/*
 * Created on 13.11.2003
 *
 * To change the template for this generated file go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
package com.sap.isa.ipc.ui.jsp.util.vms;

import java.util.Enumeration;
import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;

import com.sap.isa.ipc.ui.jsp.action.IPCBaseAction;
import com.sap.isa.ipc.ui.jsp.uiclass.UIContext;

/**
 * @author 
 * 
 * Enables the application to run with the 3.0 parameter names.
 * Added for Vehicle Manager (VMS release < 5.0) scenario
 *
 */
public class HttpRequestParamMapper {
	private static HashMap paramMap = new HashMap();
	
	static {
		paramMap.put(HttpRequestParameterConstants.DISPLAY_MODE, com.sap.spc.remote.client.object.OriginalRequestParameterConstants.DISPLAY_MODE);
		paramMap.put(HttpRequestParameterConstants.IPC_CLIENT, com.sap.spc.remote.client.object.OriginalRequestParameterConstants.CLIENT);
		paramMap.put(HttpRequestParameterConstants.HOOK_URL, com.sap.spc.remote.client.object.OriginalRequestParameterConstants.HOOK_URL);
		paramMap.put(HttpRequestParameterConstants.ENABLE_VARIANT_SEARCH, com.sap.spc.remote.client.object.OriginalRequestParameterConstants.ENABLE_VARIANT_SEARCH);
	
	
	}
	
	/**
	 * Searches all request parameter for occurances of values defined
	 * in @see com.sap.isa.ui.jsp.util.vms.HttpRequestParameterConstants
	 * for their equivalent in
	 * @see com.sap.spc.remote.client.object.OriginalRequestParameter
	 * and sets the found value as request Attribute.
	 * @param request
	 */
	public static void mapParameterNames(HttpServletRequest request) {
		UIContext uiContext = IPCBaseAction.getUIContext(request);
		for (Enumeration e = request.getParameterNames(); e.hasMoreElements(); ) {
			String requestParam = (String)e.nextElement();
			String requestParam40 = (String)paramMap.get(requestParam);
			if (requestParam40 != null) {
				uiContext.setProperty(requestParam40, request.getParameter(requestParam));
			}else {
				uiContext.setProperty(requestParam, request.getParameter(requestParam));
			}
		}
	}

	public static void main(String[] args) {
	}
}
