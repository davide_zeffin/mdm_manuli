package com.sap.isa.ipc.ui.jsp.util;

import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.Locale;
import java.util.StringTokenizer;

import com.sap.spc.remote.client.object.Characteristic;

/**
 * Title:
 * Description:  Localized entry field mask for the JSP UI.
 * Copyright:    Copyright (c) 2001
 * Company:
 * @author
 * @version 1.0
 */

public class LocalizedEntryFieldMask {

    /**
     * Generates a localized input field mask (pattern) for a given characteristic and locale.
     * E.g. ###.## or MM/DD/YYYY
     * 
     * @param characteristic Characteristic of this value. Used to get the right length, etc.
     * @param loc Current locale used to validate language dependent.
     * @param patternSign Character to be used in pattern (e.g. #)
     * @param monthAbbreviation abbreviation of "month" used in pattern (e.g. M)
     * @param dayAbbreviation abbreviation of "day" used in pattern (e.g. D)
     * @param yearAbbreviation abbreviation of "year" used in pattern (e.g. Y)
     * @return language and type dependent pattern 
     */
    public static String generate(Characteristic characteristic, Locale loc, String patternSign, String monthAbbreviation, String dayAbbreviation, String yearAbbreviation) {
        int type = characteristic.getValueType();
        // valueType int or float
        if (type == Characteristic.TYPE_INTEGER || type == Characteristic.TYPE_FLOAT ) {
            return assembleNumberEntryFieldMask(characteristic, loc, patternSign);
        }
        // valueType date
        else if (type == Characteristic.TYPE_DATE) {
            return assembleDateEntryFieldMask(loc, monthAbbreviation, dayAbbreviation, yearAbbreviation);
        }
        else {
            return "";
        }
    }

    /**
     * Assemble the entry field mask for a numerical value
     *  
     * @param cstic - characterstic of the numerical value
     * @param loc - locale to be used#
     * @param patternSign - Character to be used in pattern (e.g. #)
     * @return language dependent pattern
     */
    private static String assembleNumberEntryFieldMask(Characteristic cstic, Locale loc, String patternSign) {
        String result;
        int length = cstic.getTypeLength();
        int scale = cstic.getNumberScale();
        // integer part
        int intPartLength;
        if (scale != -1 && length != -1) {
            intPartLength = length - scale;
        } else {
            intPartLength = -1;
        }
        NumberFormat form = NumberFormat.getNumberInstance(loc);
        int groupingSize = ((DecimalFormat)form).getGroupingSize();
        char groupingSeparator = ((DecimalFormat)form).getDecimalFormatSymbols().getGroupingSeparator();
        char decimalSeparator = ((DecimalFormat)form).getDecimalFormatSymbols().getDecimalSeparator();
        String integerPart = generateIntegerPart(intPartLength, groupingSize, groupingSeparator, patternSign);
        String fractionPart = generateFractionPart(scale, patternSign);
        // has the entry field mask of the characteristic a minus sign?
        String csticEFM = cstic.getEntryFieldMask();
        String positivePattern, negativePattern;
        String pattern = ((DecimalFormat)form).toLocalizedPattern();
        char patternSeparator = ((DecimalFormat)form).getDecimalFormatSymbols().getPatternSeparator();
        int patternSeparatorPosition = pattern.indexOf(patternSeparator);
        char minusSign = ((DecimalFormat)form).getDecimalFormatSymbols().getMinusSign();
        if (patternSeparatorPosition != -1) {
            positivePattern = pattern.substring(0, patternSeparatorPosition);
            negativePattern = pattern.substring(patternSeparatorPosition+1, pattern.length());
        }
        else {
            positivePattern = pattern;
            negativePattern = "";
        }
        boolean hasMinus;
        if (csticEFM != null && csticEFM.startsWith("-")) {
            hasMinus = true;
        } else {
            hasMinus = false;
        }
        if (hasMinus) {
            result = addMinusSign(integerPart, fractionPart, negativePattern, minusSign, decimalSeparator);
        }
        else {
            result = integerPart + decimalSeparator + fractionPart;
        }
        if (result.endsWith(new Character(decimalSeparator).toString())) {
            result = result.substring(0,result.length()-1);
        }
        return result;
    }

    /**
     * Generate the integer part (before decimal separator) for a numerical value pattern
     * 
     * @param intPartLength - Length of integer part (before decimal separator)
     * @param groupingSize - digits between the grouping separators
     * @param groupingSeparator -  the grouping separator to be used
     * @param patternSign - Character to be used in pattern (e.g. #) 
     * @return integer part of the pattern
     */
    private static String generateIntegerPart(int intPartLength, int groupingSize, char groupingSeparator, String patternSign) {
        StringBuffer result = new StringBuffer();
        int j = 0;
        for (int i=0; i<intPartLength; i++) {
            if (j==groupingSize) {
                result.insert(0, groupingSeparator);
                j = 0;
            }
            result.insert(0, patternSign);
            j++;
        }
        // if the first character is the grouping separator, we delete it (but we check the length of result before)
        if ((result.length() > 0) && (result.charAt(0) == groupingSeparator)) {
            result.deleteCharAt(0);
        }
        return result.toString();
    }

     /**
      * Generate the fraction part (after decimal separator) for a numerical value pattern
      * 
      * @param scale - Length of fraction part (after decimal separator)
      * @param patternSign - Character to be used in pattern (e.g. #) 
      * @return fraction part of the pattern
      */    
    private static String generateFractionPart(int scale, String patternSign) {
        StringBuffer result = new StringBuffer();
        for (int i=0; i<scale; i++) {
            result.insert(0, patternSign);
        }
        return result.toString();
    }

    /**
     * Add a minus sign to the pattern if necessary
     * 
     * @param integerPart - integer part of the pattern (generated in method generateIntegerPart()
     * @param fractionPart - fraction part of the pattern (generated in method generateFractionPart()
     * @param negativePattern - Negative pattern (in order to determine whether minus sign has to be added at the beginning or at the end of the pattern
     * @param minusSign - Character of the minus sign
     * @param decimalSeparator - decimal separator to be added between integer and fraction part
     * @return returns the pattern 
     */
    private static String addMinusSign(
        String integerPart,
        String fractionPart,
        String negativePattern,
        char minusSign,
        char decimalSeparator) {
        String result = "";
        if (!negativePattern.equals("")) {
            if (negativePattern
                .startsWith(new Character(minusSign).toString())) {
                result =
                    minusSign + integerPart + decimalSeparator + fractionPart;
            } else if (
                negativePattern.endsWith(
                    new Character(minusSign).toString())) {
                result =
                    integerPart + decimalSeparator + fractionPart + minusSign;
            } else if (negativePattern.startsWith("(")) {
                result =
                    "(" + integerPart + decimalSeparator + fractionPart + ")";
            }
        } else {
            result = minusSign + integerPart + decimalSeparator + fractionPart;
        }
        return result;
    }

    /**
     * Assemble the entry field mask for a date value
     * 
     * @param locale - the locale to be used
     * @param m - abbreviation of "month" used in pattern (e.g. M)
     * @param d - abbreviation of "day" used in pattern (e.g. D)
     * @param y - abbreviation of "year" used in pattern (e.g. Y)
     * @return pattern for a date value
     */
    private static String assembleDateEntryFieldMask(Locale locale, String m, String d, String y) {
        StringBuffer result = new StringBuffer();
        SimpleDateFormat sdf = (SimpleDateFormat)SimpleDateFormat.getDateInstance(DateFormat.SHORT, locale);
        String datePattern = sdf.toPattern();
        String dateSeparator = getDateSeparator(datePattern);
        try {
            String dateFields[] = tokenizePattern(datePattern, dateSeparator);
            for (int i=0; i<dateFields.length; i++) {
                switch (dateFields[i].charAt(0)) {
                    case 'y':
                    case 'Y':
                        if (dateFields[i].length() == 4) {
                            // add "YYYY"
                            result.append(y);
                            result.append(y);
                            result.append(y);
                            result.append(y);
                        }
                        else {
                            // add "YY"
                            result.append(y);
                            result.append(y);
                        }
                        break;
                    case 'm':
                    case 'M':
                        // add "MM"
                        result.append(m);
                        result.append(m);
                        break;
                    case 'd':
                    case 'D':
                        // add "DD"
                        result.append(d);
                        result.append(d);
                }
                if (i != dateFields.length-1) {
                    result.append(dateSeparator);
                }
            }
        }
        catch (NullPointerException npe) {
            // return YYYYMMDD
            result.append(y);
            result.append(y);
            result.append(y);
            result.append(y);
            result.append(m);
            result.append(m);
            result.append(d);
            result.append(d);
            return result.toString();
        }
        return result.toString();
    }

    /**
     * Tokenize the pattern to date fields
     */
    private static String[] tokenizePattern(String datePattern, String dateSeparator) {
        String dateFields[] = new String[3];
        StringTokenizer st = new StringTokenizer(datePattern, dateSeparator);
        int i = 0;
        while (st.hasMoreTokens()) {
            dateFields[i] = st.nextToken();
            i++;
        }
        return dateFields;
    }

    /**
     * Returns the separator of a date pattern
     */
    private static String getDateSeparator(String datePattern) {
        String dateSeparator = "";
        if (datePattern.indexOf("/") != -1) {
            dateSeparator = "/";
        } else if (datePattern.indexOf(".") != -1) {
            dateSeparator = ".";
        } else if (datePattern.indexOf("-") != -1) {
            dateSeparator = "-";
        }
        return dateSeparator;
    }
}