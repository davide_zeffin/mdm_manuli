/*****************************************************************************
    Interface:    CatalogServiceAreaData
    Copyright (c) 2008, SAP AG, Germany, All rights reserved.
    Created:      15.05.2008
    Version:      1.0

    $Revision: #1 $
    $Date: 2008/05/15 $
*****************************************************************************/

package com.sap.isa.webservice.catalog.types;
import java.io.Serializable;

/**
 * Class to hold area specific data for a Web Service response
 * 
 * @version 1.0
 */
public class CatalogServiceAreaData implements Serializable {

    public String areaKey;
    public String parentAreaKey;
    public String description;
    /** @link aggregation <{CatalogServiceAreaLOCDef}>
    * @supplierCardinality 0..*
    */
    public CatalogServiceAreaLOCDef[] locData;
    /** @link aggregation <{CatalogServiceTextData}>
    * @supplierCardinality 0..*
    */
    public CatalogServiceTextData[] textData;
    /** @link aggregation <{CatalogServiceMimesData}>
    * @supplierCardinality 0..*
    */
    public CatalogServiceMimesData[] mimeData;
}