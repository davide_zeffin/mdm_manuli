/*****************************************************************************
Class         Log
Copyright (c) 2008, SAP AG, All rights reserved.
Created:      30.4.2008
*****************************************************************************/
package com.sap.isa.webservice.catalog.types;

import java.io.Serializable;

/**
 * Implements the global data type 11.344 Log.
 * 
 * A Log is a sequence of messages that result when an application executes a task. 
 * A Log can be used to transmit log messages with different levels of severity such as warnings and errors.
 * 
 * Example
 * =======
 * <pre>
 * <Log>
 *     <MaximumLogItemSeverityCode>3</MaximumLogItemSeverityCode>
 *     <Item >
 *         <TypeID>001(/CCM/)</TypeID>
 *         <SeverityCode>3</SeverityCode>
 *         <Note>Catalog cameras could not be published</Note>
 *     </Item>
 * </Log>
 * </pre>
 * 
 * Integrity Conditions
 * ====================
 * At least one element must be given.
 * If MaximumLogItemSeverityCode is set, the BusinessDocumentProcessingResultCode and / or LogItem(s) must be given.
 * The maximum occurrence of the element Item is n times.
 *
 */
public class Log implements Serializable {

    private int itemSize = 1;
    private int currentItem = 0;

    /**
     * Codes for log detail level.
     */
    public static final String NO_DETAILS = "1";
    public static final String ABORTIONS = "3";
    public static final String ABORTIONS_AND_ERRORS = "5";
    public static final String ABORTIONS_ERRORS_AND_WARNINGS = "7";
    public static final String ALL_DETAILS = "9";

    /**
     * Result of processing of a business document
     */
    public String BusinessDocumentProcessingResultCode;

    /**
     * Coded representation of the maximum severity of a log message in a given log
     */
    public String MaximumLogItemSeverityCode = LogItem.SEVERITY_INFO;

    /**
     * A coded representation of the level of detail of the information provided in a log.
     */
    public String LogDetailLevelCode = Log.NO_DETAILS;

    /**
     * Individual log message (see GDT LogItem).
     */
    public LogItem[] item;

    public Log() {
        item = new LogItem[itemSize];
    }

    /**
     * Adds a LogItem and updates the maxSeverityCode.
     * 
     * @see LogItem.SEVERTIY_INFO
     * 
     * @param severityCode
     * @param note
     * @return
     */
    public LogItem addLogItem(String severityCode, String note) {

        LogItem newLogItem = new LogItem(severityCode, note);

        checkItemSize();
        item[currentItem++] = newLogItem;

        if (severityCode.compareTo(MaximumLogItemSeverityCode) > 0) {
            MaximumLogItemSeverityCode = severityCode;
            setLogDetailLevel();
        }

        return newLogItem;
    }

    private void setLogDetailLevel() {

        if (LogItem.SEVERITY_ABORTION.equals(MaximumLogItemSeverityCode)) {
            if (LogDetailLevelCode.compareTo(Log.ABORTIONS) <= 0) {
                LogDetailLevelCode = Log.ABORTIONS;
            }
        }

        if (LogItem.SEVERITY_ERROR.equals(MaximumLogItemSeverityCode)) {
            if (LogDetailLevelCode.compareTo(Log.ABORTIONS_AND_ERRORS) <= 0) {
                LogDetailLevelCode = Log.ABORTIONS_AND_ERRORS;
            }
        }

        if (LogItem.SEVERITY_WARNING.equals(MaximumLogItemSeverityCode)) {
            if (LogDetailLevelCode.compareTo(Log.ABORTIONS_ERRORS_AND_WARNINGS) <= 0) {
                LogDetailLevelCode = Log.ABORTIONS_ERRORS_AND_WARNINGS;
            }
        }
    }

    private void checkItemSize() {

        if (currentItem >= itemSize) {
            LogItem[] newItem = new LogItem[currentItem + 1];
            for (int i = 0; i < currentItem; i++) {
                newItem[i] = item[i];
            }

            item = newItem;
        }
    }

    /**
     * @return
     */
    public String getBusinessDocumentProcessingResultCode() {
        return BusinessDocumentProcessingResultCode;
    }

    /**
     * @return
     */
    public LogItem[] getItem() {
        return item;
    }

    /**
     * @return
     */
    public String getMaximumLogItemSeverityCode() {
        return MaximumLogItemSeverityCode;
    }

    /**
     * @param string
     */
    public void setBusinessDocumentProcessingResultCode(String string) {
        BusinessDocumentProcessingResultCode = string;
    }

    /**
     * @param items
     */
    public void setItem(LogItem[] items) {
        item = items;
    }

    /**
     * @param string
     */
    public void setMaximumLogItemSeverityCode(String string) {
        MaximumLogItemSeverityCode = string;
    }

}
