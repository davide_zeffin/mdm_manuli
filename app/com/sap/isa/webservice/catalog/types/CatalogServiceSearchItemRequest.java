/*****************************************************************************
    Interface:    CatalogServiceSearchItemRequest
    Copyright (c) 2008, SAP AG, Germany, All rights reserved.
    Created:      15.05.2008
    Version:      1.0

    $Revision: #1 $
    $Date: 2008/05/15 $
*****************************************************************************/

package com.sap.isa.webservice.catalog.types;
import java.io.Serializable;

/**
 * Class to hold data for an item search Web Service request
 * 
 * @version 1.0
 */
public class CatalogServiceSearchItemRequest implements Serializable {
    public CatalogServiceRequestBase requestBaseData;
    public String areaKey;
    public String searchTerm;
    /** @link aggregation <{CatalogServiceAreaLOCSearch}>
     * @supplierCardinality 0..*
     */
    public CatalogServiceAreaLOCSearch[] locData;
    public String campaignId;
    public int startIdx;
    public int endIdx;
    public boolean returnPrices;
    public boolean returnContracts;
    public boolean areaSearch;
    
    public CatalogServiceSearchItemRequest() {
        requestBaseData = new CatalogServiceRequestBase();
    }
}