/*****************************************************************************
    Interface:    CatalogServiceAreaRequest
    Copyright (c) 2008, SAP AG, Germany, All rights reserved.
    Created:      15.05.2008
    Version:      1.0

    $Revision: #1 $
    $Date: 2008/05/15 $
*****************************************************************************/

package com.sap.isa.webservice.catalog.types;
import java.io.Serializable;

/**
 * Class to hold data for an area specific Web Service request
 * 
 * @version 1.0
 */
public class CatalogServiceAreaRequest implements Serializable {
    public CatalogServiceRequestBase requestBaseData;
    public String areaKey;
    public String campaignId;
    public int startIdx;
    public int endIdx;
    public boolean returnAreaLocs;
    public boolean returnPrices;
    public boolean returnContracts;
    
    public CatalogServiceAreaRequest() {
        requestBaseData = new CatalogServiceRequestBase();
    }
}