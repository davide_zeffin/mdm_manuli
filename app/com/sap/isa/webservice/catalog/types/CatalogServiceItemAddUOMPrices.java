/*****************************************************************************
    Interface:    CatalogServiceItemAddUOMPrices
    Copyright (c) 2008, SAP AG, Germany, All rights reserved.
    Created:      15.05.2008
    Version:      1.0

    $Revision: #1 $
    $Date: 2008/05/15 $
*****************************************************************************/

package com.sap.isa.webservice.catalog.types;
import java.io.Serializable;

/**
 * Class to hold data for an items prices for additional units of measurement 
 * in a Web Service response
 * 
 * @version 1.0
 */
public class CatalogServiceItemAddUOMPrices implements Serializable {

    public String uom;
    public String uomIso;
    public String stdPrice;
    public String currencyStdPrice;
    public String currencyStdPriceIso;
    public String specialPrice;
    public String currencySpecialPrice;
    public String currencySpecialPriceIso;
}