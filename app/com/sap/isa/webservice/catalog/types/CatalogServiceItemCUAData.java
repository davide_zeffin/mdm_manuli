/*****************************************************************************
    Interface:    CatalogServiceItemCUAData
    Copyright (c) 2008, SAP AG, Germany, All rights reserved.
    Created:      15.05.2008
    Version:      1.0

    $Revision: #1 $
    $Date: 2008/05/15 $
*****************************************************************************/

package com.sap.isa.webservice.catalog.types;
import java.io.Serializable;

/**
 * Class to hold data for an items CUA items in a Web Service response
 * 
 * @version 1.0
 */
public class CatalogServiceItemCUAData implements Serializable {

    public String itemKey;
    public String areaKey;
    public String productId;
    public String productGuid;
    public String productDesc;
    public String cuaType;
    public String thumbNail;
    public String price;
    public String priceSimpleFormat;
    public String currency;
    public String currencyIso;
}