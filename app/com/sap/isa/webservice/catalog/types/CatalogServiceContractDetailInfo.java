/*****************************************************************************
    Interface:    CatalogServiceContractDetailInfo
    Copyright (c) 2008, SAP AG, Germany, All rights reserved.
    Created:      15.05.2008
    Version:      1.0

    $Revision: #1 $
    $Date: 2008/05/15 $
*****************************************************************************/

package com.sap.isa.webservice.catalog.types;
import java.io.Serializable;

/**
 * Class to hold an items detail contract data for a Service response
 * 
 * @version 1.0
 */
public class CatalogServiceContractDetailInfo implements Serializable {

    public String contrAttrDesc;
    public String contrAttrValue;
}