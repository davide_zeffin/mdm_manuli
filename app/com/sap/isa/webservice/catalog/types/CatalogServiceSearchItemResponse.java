/*****************************************************************************
    Interface:    CatalogServiceSearchItemResponse
    Copyright (c) 2008, SAP AG, Germany, All rights reserved.
    Created:      15.05.2008
    Version:      1.0

    $Revision: #1 $
    $Date: 2008/05/15 $
*****************************************************************************/

package com.sap.isa.webservice.catalog.types;
import java.io.Serializable;

/**
 * Class to hold data for an item search Web Service response
 * 
 * @version 1.0
 */
public class CatalogServiceSearchItemResponse implements Serializable {
    public CatalogServiceResponseBaseData responseBaseData;
    public String resultNo;
    /** @link aggregation <{CatalogServiceItemData}>
    * @supplierCardinality 0..*
    */
    public CatalogServiceItemData[] items;
    
    public CatalogServiceSearchItemResponse() {
        responseBaseData = new CatalogServiceResponseBaseData();
    }
}