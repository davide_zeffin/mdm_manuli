/*****************************************************************************
Class         LogItem
Copyright (c) 2008, SAP AG, All rights reserved.
Created:      30.4.2008
*****************************************************************************/
package com.sap.isa.webservice.catalog.types;

import java.io.Serializable;

/**
 * Implements global data type 6.253 LogItem 
 * 
 * A LogItem is a log message that is generated when an application is executed
 * 
 * The use of the elements TypeID and WebURI (with or without a specified language) 
 * is optional depending on the business context. It is not useful to use the SeverityCode 
 * for all types of log. The GDT LogItem can therefore be extended in the future by specifying an
 * attack level, for instance, in the area of Internet security, or for user interaction in the 
 * area of e-learning.
 * 
 */
public class LogItem implements Serializable {

    /**
     * Unique identification of the type of a log entry (within the application that generates the log).
     * For example, when a catalog is published, a log can be generated containing several items 
     * concerning the successful publication of a catalog item. Since these log entries are similar, 
     * they all have the same TypeID, although the respective catalog items are inserted dynamically in a text
     * pattern that corresponds to the message type. The LogItemTypeID must not be confused with sequential 
     * numbering for the messages in a log. The LogItemTypeID does not require any of the attributes listed 
     * for the CDT Identifier, since these are taken from the context. 
     */
    private String TypeID;

    /**
     * A category of a log item according to the characteristics of the log message.
     */
    private String CategoryCode;

    /**
     * The severity of the log message
     */
    private String SeverityCode;

    /**
     * A short text for the log message. The LogItemNote restricts the length permitted in the GDT Note.
     */
    private String Note;

    /**
     * The URI for a document available on the Internet that contains more information about the log entry. 
     * The only URI schemas permitted are "http" and "https."
     */
    private String WebURI;

    /**
     * Severity codes.
     */
    public static final String SEVERITY_INFO = "10";
    public static final String SEVERITY_WARNING = "20";
    public static final String SEVERITY_ERROR = "30";
    public static final String SEVERITY_ABORTION = "90";

    /**
     * Constructor
     * 
     * @param severityCode
     * @param note
     */
    public LogItem(String severityCode, String note) {
        this.SeverityCode = severityCode;
        this.Note = note;
    }

    /**
     * @return
     */
    public String getCategoryCode() {
        return CategoryCode;
    }

    /**
     * @return
    */
    public String getNote() {
        return Note;
    }

    /**
     * @return
     */
    public String getSeverityCode() {
        return SeverityCode;
    }

    /**
     * @return
    */
    public String getTypeID() {
        return TypeID;
    }

    /**
     * @return
     */
    public String getWebURI() {
        return WebURI;
    }

    /**
     * @param string
     */
    public void setCategoryCode(String string) {
        CategoryCode = string;
    }

    /**
     * @param string
     */
    public void setNote(String string) {
        Note = string;
    }

    /**
     * @param string
     */
    public void setSeverityCode(String string) {
        SeverityCode = string;
    }

    /**
     * @param string
     */
    public void setTypeID(String string) {
        TypeID = string;
    }

    /**
         * @param string
         */
    public void setWebURI(String string) {
        WebURI = string;
    }

}
