/*****************************************************************************
    Interface:    CatalogServiceItemDetailData
    Copyright (c) 2008, SAP AG, Germany, All rights reserved.
    Created:      15.05.2008
    Version:      1.0

    $Revision: #1 $
    $Date: 2008/05/15 $
*****************************************************************************/

package com.sap.isa.webservice.catalog.types;
import java.io.Serializable;

/**
 * Class to hold data for an item detail in a Web Service response
 * 
 * @version 1.0
 */
public class CatalogServiceItemDetailData implements Serializable {

    public CatalogServiceItemData item;
    /** @link aggregation <{CatalogServiceItemCUAData}>
    * @supplierCardinality 0..*
    */
    public CatalogServiceItemCUAData[] cuaData;
    /** @link aggregation <{CatalogServiceItemAddUOMPrices}>
    * @supplierCardinality 0..*
    */
    public CatalogServiceItemAddUOMPrices[] addUOMPrices;  
    public String  atpAvailQuant;
    public String  atpAvailQuantSimpleFormat;
    public String  atpAvailDate;
    public String  atpAvailDateIso;
}