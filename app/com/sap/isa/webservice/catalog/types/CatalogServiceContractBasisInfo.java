/*****************************************************************************
    Interface:    CatalogServiceContractBasisInfo
    Copyright (c) 2008, SAP AG, Germany, All rights reserved.
    Created:      15.05.2008
    Version:      1.0

    $Revision: #1 $
    $Date: 2008/05/15 $
*****************************************************************************/

package com.sap.isa.webservice.catalog.types;
import java.io.Serializable;

/**
 * Class to hold an items basic contract data for a Service response
 * 
 * @version 1.0
 */
public class CatalogServiceContractBasisInfo implements Serializable {

    public String contractKey;
    public String contractId;
    public String contractItemKey;
    public String contractItemId;
    public String contractPrice;
    public String contractPriceSimpleFormat;
    public String contractCurrency;
    public String contractCurrencyIso;
    /**
    * @link aggregation <{CatalogServiceContractDetailInfo}>
    * @supplierCardinality 0..*
    */    
    public CatalogServiceContractDetailInfo[] contrDetails;
}