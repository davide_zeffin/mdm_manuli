/*****************************************************************************
    Interface:    CatalogServiceItemDetailRequest
    Copyright (c) 2008, SAP AG, Germany, All rights reserved.
    Created:      15.05.2008
    Version:      1.0

    $Revision: #1 $
    $Date: 2008/05/15 $
*****************************************************************************/

package com.sap.isa.webservice.catalog.types;
import java.io.Serializable;

/**
 * Class to hold data for an item detail Web Service request
 * 
 * @version 1.0
 */
public class CatalogServiceItemDetailRequest implements Serializable {
    public CatalogServiceRequestBase requestBaseData;
    public String areaKey;
    public String itemKey;
    public String campaignId;
    public boolean returnPrices;
    public boolean returnContracts;
    public String quantityForATPCheck;
    public String uomForATPCheck;
    public boolean returnATP;
    public boolean returnCUA;
    
    public CatalogServiceItemDetailRequest() {
        requestBaseData = new CatalogServiceRequestBase();
    }
}