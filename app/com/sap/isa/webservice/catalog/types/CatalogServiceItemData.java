/*****************************************************************************
    Interface:    CatalogServiceItemData
    Copyright (c) 2008, SAP AG, Germany, All rights reserved.
    Created:      15.05.2008
    Version:      1.0

    $Revision: #1 $
    $Date: 2008/05/15 $
*****************************************************************************/

package com.sap.isa.webservice.catalog.types;
import java.io.Serializable;

/**
 * Class to hold data for an item in a Web Service response
 * 
 * @version 1.0
 */
public class CatalogServiceItemData implements Serializable {

    /** @link aggregation <{CatalogServiceLOCData}>
    * @supplierCardinality 0..*
    */
    public CatalogServiceLOCData[] locData;
    public String itemKey;
    public String areaKey;
    public String productGuid;
    public String productId;
    public String productDescription;
    public String uom;
    public String uomIso;
    public String quantity;
    public String posNr;
    public String description;
    public String eyeCatcherText;
    public String priceEyeCatcherText;
    public String stdPrice;
    public String currencyStdPrice;
    public String currencyStdPriceIso;
    public String specialPrice;
    public String currencySpecialPrice;
    public String currencySpecialPriceIso;
    public String thumbNail;
    /**
     * @link aggregation <{CatalogServiceTextData}> 
     * @supplierCardinality 0..*
     */
    public CatalogServiceTextData[] textData;
    /**
    * @link aggregation <{CatalogServiceMimesData}>
    * @supplierCardinality 0..*
    */
    public CatalogServiceMimesData[] mimeData;
    /**
    * @link aggregation <{CatalogServiceContractBasisInfo}>
    * @supplierCardinality 0..*
    */    
    public CatalogServiceContractBasisInfo[] contracts;
}