package com.sap.isa.catalog.webcatalog;

import com.sap.isa.core.Iterable;
import com.sap.isa.core.logging.IsaLocation;

import java.util.Iterator;
import java.util.List;

/**
 * Title:        ProductCatalog UI Description: Copyright:    Copyright (c) 2001 Company:      SAPMarkets Europe GmbH
 *
 * @version 1.0
 */
public class WebCatItemPage extends WebCatBusinessObjectBase implements Iterable {
    
    private static IsaLocation log = IsaLocation.getInstance(WebCatItemPage.class.getName());
    
    protected WebCatItemList itemList;
    protected int page;
    protected int pageSize;
    protected int pageSizeMin;
    protected List items;
    protected int totalPages = 1;         //Total number of pages that can be displayed = Number of hits/Items per page
    protected int remainingPartPages = 0; //Remainder of pages after Number of hits%Items per page
    protected int maxPageLinks = 0;       //Number of links per page as configured via XCM
    protected int listType = 0;           //type of the WebCatItemList

    /**
     * Creates a new WebCatItemPage object.
     *
     * @param itemList DOCUMENT ME!
     * @param page DOCUMENT ME!
     * @param pageSize DOCUMENT ME!
     * 
     * @deprecated because constructor extended by new parameter
     */
    public WebCatItemPage(WebCatItemList itemList, int page, int pageSize) {
       
       if (log.isDebugEnabled()) {
           log.debug("WebCatItemPage:WebCatItemPage(WebCatItemList " + itemList + ", int " + page + ", int " + pageSize + ")");
       }

        this.itemList = itemList;
        this.page = page;
        this.pageSize = pageSize;

        if (page <= 0) {
            items = itemList.getItems();
        }
        else {
            int pageStart = (page - 1) * pageSize; // first page is numbered 1, not 0!
            int pageEnd = Math.min(pageStart + pageSize, itemList.size());
            items = itemList.getItems().subList(pageStart, pageEnd);
        }
        
        if (pageSize() != 0) {
            // Calculate how many complete pages can be displayed
            totalPages = getItemList().getNoOfMainItems() / pageSize() ;

            // Check if any incomplete pages remain
            remainingPartPages = getItemList().getNoOfMainItems() % pageSize();
        }

        if (remainingPartPages != 0) {
            totalPages++;
        }

        maxPageLinks = itemList.getParentCatalog().getNumPageLinks(); //Number of links per page as configured via XCM

        if (maxPageLinks > totalPages) {
            maxPageLinks = totalPages;
        }
    }

    /**
     * Creates a new WebCatItemPage object.
     *
     * @param itemList - item list
     * @param page - number of page to be displayed
     * @param pageSize - current page size
     * @param pageSizeMin - minimum page size
     */
    public WebCatItemPage(WebCatItemList itemList, int page, int pageSize, int pageSizeMin) {
       
       if (log.isDebugEnabled()) {
           log.debug("WebCatItemPage:WebCatItemPage(WebCatItemList " + itemList + ", int " + page + ", int " + pageSize + ", int " + pageSizeMin + ")");
       }

        this.itemList = itemList;
        this.page = page;
        this.pageSize = pageSize;
        this.pageSizeMin = pageSizeMin;
        this.listType = itemList.getListType();

        if (page <= 0) {
            items = itemList.getItems();
        }
        else {
            int pageStart = (page - 1) * pageSize; // first page is numbered 1, not 0!
            int pageEnd = Math.min(pageStart + pageSize, itemList.size());
            items = itemList.getItems().subList(pageStart, pageEnd);
        }
        
        if (pageSize() != 0) {
            // Calculate how many complete pages can be displayed
            totalPages = getItemList().getNoOfMainItems() / pageSize() ;

            // Check if any incomplete pages remain
            remainingPartPages = getItemList().getNoOfMainItems() % pageSize();
        }

        if (remainingPartPages != 0) {
            totalPages++;
        }

        maxPageLinks = itemList.getParentCatalog().getNumPageLinks(); //Number of links per page as configured via XCM

        if (maxPageLinks > totalPages) {
            maxPageLinks = totalPages;
        }
    }

    /**
     * Creates a new WebCatItemPage object.
     *
     * @param itemList DOCUMENT ME!
     */
    public WebCatItemPage(WebCatItemList itemList) {
        this(itemList, 0, 0, 0);
    }

    /**
     * DOCUMENT ME!
     *
     * @return DOCUMENT ME!
     */
    public WebCatItemList getItemsAsWebCatItemList() {
        WebCatItemList itemsList = new WebCatItemList(itemList.getParentCatalog());
        itemList.setListType(listType);

        for (int i = 0; i < items.size(); i++) {
            itemsList.addItem((WebCatItem) items.get(i));
        }

        return itemsList;
    }

    /**
     * DOCUMENT ME!
     *
     * @return DOCUMENT ME!
     */
    public int[] getPaginationLinks() {
        
        log.debug("PaginationLinks called");
        
        int paginationArray[];   //Array to hold all the pagination numbers to be displayed

        int currentPage = getPage();        //Current Page
        
        // check for odd or even nuber of pages to be displayed
        int zeroOrOne = maxPageLinks % 2;
        
        // if the page to be displayed is not at least the middle of the 
        // first chunk of pages to be displayed, set it to the middle
        if (currentPage <= maxPageLinks / 2) {
            currentPage = maxPageLinks / 2 + zeroOrOne;
        }
        
        // if the current page would exceed the total number of pages
        // set it to the middle of the last page chunk
        if (currentPage > totalPages - (maxPageLinks / 2)) {
            currentPage = totalPages - (maxPageLinks / 2);
        }

        //Calculate the first and last pagination number that should be displayed.
        int firstLinkNumber = currentPage - ((maxPageLinks / 2) + zeroOrOne) + 1 ;
        int lastLinkNumber = currentPage + (maxPageLinks / 2);
        
        // no make sure we do notstart with an index < 1
        if (firstLinkNumber < 1) {
            firstLinkNumber = 1;
        }

        //Create the array to hold the pagination numbers that will be displayed on the page
        paginationArray = new int[maxPageLinks];
        
        try {
            for (int y = firstLinkNumber; y <= lastLinkNumber; y++) {
                paginationArray[y - firstLinkNumber] = y;
            }
        }
        catch (IndexOutOfBoundsException e) {
        	log.debug(e.getMessage());
        }

        return paginationArray;
    }

    /**
     * DOCUMENT ME!
     *
     * @return DOCUMENT ME!
     */
    public List getItems() {
        return items;
    }
    
    /**
     * Returns true, if the current page is the last page
     * 
     * @return true if the current page is the last page 
     *         false if there are pages available after the current page. 
     */
    public boolean showNextLink() {
        return page != 0 && page < totalPages;
    }
    
    /**
     * Before removing this object, make sure that referenced objects are handled. This is 
     * currently done by calling the releaseReferences() method.
     */
    public void finalize() throws Throwable {
        log.entering("finalize()");
        if (log.isDebugEnabled()) {
            log.debug("Start finalizing WebCatItemPage " + this);
        }

        itemList = null;
        items       = null;
        
        super.finalize();
        log.exiting();
    }

    /**
     * Get the item for the given index
     *
     * @param index the index of the rquested item
     *
     * @return WebCatItem the item for the given index
     */
    public WebCatItem getItem(int index) {
        if (log.isDebugEnabled()) {
            log.debug("WebCatItemPage: getItem(int " + index + ")");
        }

        return (WebCatItem) items.get(index);
    }

    /**
     * Get the underlying itemlist
     *
     * @return WebCatItemList the underlying itemlist
     */
    public WebCatItemList getItemList() {
        log.debug("WebCatItemPage: getItemList()");
        return itemList;
    }

    /**
     * Get the number of the current page
     *
     * @return int Get the number of the current page
     */
    public int getPage() {
        return page;
    }
    
    /**
     * Get the total number of possible pages
     *
     * @return int total number of possible pages
     */
    public int getNoOfTotalPages() {
        return totalPages;
    }

    /**
     * get number of items on this distinct page
     *
     * @return size number of items on this page, can be less than pageSize if this is the last page.
     */
    public int size() {
        if (log.isDebugEnabled()) {
            log.debug("WebCatItemPage:size() " + items.size());
        }

        return items.size();
    }

    /**
     * get number of items to be shown per page
     *
     * @return pageSize number of items to be shown per page (max.)
     */
    public int pageSize() {
        if (log.isDebugEnabled()) {
            log.debug("WebCatItemPage:pageSize() " + pageSize);
        }

        return pageSize;
    }

    /**
     * determines the information if the current page isn't the first and second page.
     *
     * @return boolean value is true if it isn't the 1st and 2nd page
     */
    public boolean showPrevLink() {
        return page > 1;
    }

    /**
     * get iterator for itempage
     *
     * @return iterator iterator of item page
     */
    public Iterator iterator() {
        return items.iterator();
    }

    /**
     * calculates the page size dependent on a rule.
     * 
     * @param  calcRule defines the rule how the proposed page size is calculated
     *         = 0, calcPageSize = 0
     *         = 1, calcPageSize is set to pageSize 
     *         = 2, calcPageSize is set to double of pageSize 
     *         = 3, calcPageSize is set to square product of pageSize
     *         = 4, if pageSize is greater than 10 calcPageSize is set to the double of square product of pageSize
     *              if pageSize is smaller than 10 calcPageSize is set to 10 times the pageSize
     *         > 4, calcPageSize = 0
     * @return calcPageSize delivers the calculated page size
     */
    public int calculatePageSize(int calcRule) {
        final String METHOD = "calculatePageSize()";
        int calcPageSize = 0;
        int calcPageSizePrev = 0;
        
        switch (calcRule) {
            case 1:  calcPageSize = this.pageSizeMin;
                     break;
            case 2:  calcPageSize = this.pageSizeMin * 2;
                     break;
            case 3:  calcPageSizePrev = calculatePageSize(2);
                     calcPageSize = this.pageSizeMin * this.pageSizeMin;
                     if (calcPageSize <= calcPageSizePrev) {
                         calcPageSize = calcPageSizePrev * calcPageSizePrev;
                     }
                     break;
            case 4:  calcPageSizePrev = calculatePageSize(3);
                     calcPageSize = this.pageSizeMin * 10;
                     if (calcPageSize <= calcPageSizePrev) {
                         calcPageSize = calcPageSizePrev * 2;
                     }
                     break;
            default: calcPageSize = 0;
                     break; 
        }
        log.entering(METHOD);
        
        if (log.isDebugEnabled()) {
            log.debug(METHOD + ": calcRule " + calcRule
                      + " / calcPageSize = " + calcPageSize);
        }                 

        return calcPageSize;
    }

    /**
     * Returns the type of the WebCatItemList.
     * The types are defined in the WebCatItemList and ActionConstants.
     * 
     * @return listType
     */
    public int getListType() {
        return this.listType;
    }

}