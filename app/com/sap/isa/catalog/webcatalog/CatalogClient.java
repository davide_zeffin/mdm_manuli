package com.sap.isa.catalog.webcatalog;

import java.util.Locale;

import com.sap.isa.catalog.boi.IClient;

/**
 * DOCUMENT ME!
 *
 * @author $author$
 * @version $Revision$
 */
public class CatalogClient extends com.sap.isa.catalog.impl.CatalogClient implements IClient {

	/**
	 * Creates a new CatalogClient object.
	 *
	 * @param theUserLocale DOCUMENT ME!
	 * @param theUsername DOCUMENT ME!
	 * @param thePassword DOCUMENT ME!
	 */
	public CatalogClient(Locale theUserLocale, String theUsername, String thePassword) {
		log.entering("CatalogClient(Locale theUserLocale, String theUsername, String thePassword)");

		theLocale = theUserLocale;
		theName = theUsername;
		thePWD = thePassword;

		if (log.isDebugEnabled()) {
			log.debug("CatalogClient:CatalogClient(Locale " + theUserLocale + ", Username " + theUsername +
					  ", Password - not shown )");
		}

		log.exiting();
	}

	/**
	 * Constructor.
	 *
	 * @param aLocale the locale of the user
	 * @param aName the user name
	 * @param aPWD the password
	 * @param views the list of views
	 */
	public CatalogClient(Locale aLocale, String aName, String aPWD, String views[]) {
		super(aLocale, aName, aPWD, views);
	}
    
	/**
	 * Constructor.
	 *
	 * @param aLocale the locale of the user
	 * @param aName the user name
	 * @param aPWD the password
	 * @param views the list of views
	 * @param catalogStatus the status of teh catalog to be read
	 */
	public CatalogClient(Locale aLocale, String aName, String aPWD, String views[], int catalogStatus) {
		super(aLocale, aName, aPWD, views);
		log.entering("CatalogClient(Locale aLocale, String aName, String aPWD, String views[], int catalogStatus)");
		this.catalogStatus = catalogStatus;
		log.exiting();
	}

	/**
	 * DOCUMENT ME!
	 *
	 * @return DOCUMENT ME!
	 */
	public String getCookie() {
		return "";
	}

	/**
	 * DOCUMENT ME!
	 *
	 * @return DOCUMENT ME!
	 */
	public String getGuid() {
		return "";
	}

	/**
	 * DOCUMENT ME!
	 *
	 * @return DOCUMENT ME!
	 */
	public java.util.Iterator getPermissions() {
		return null;
	}
}