/*****************************************************************************

    Class:        WebCatItemList
    Copyright (c) 2006, SAP AG, Germany, All rights reserved.
    Created:      2001

*****************************************************************************/
package com.sap.isa.catalog.webcatalog;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import com.sap.isa.businessobject.ProductBaseData;
import com.sap.isa.businessobject.ProductListBaseData;
import com.sap.isa.businessobject.contract.ContractAttributeList;
import com.sap.isa.catalog.boi.CatalogException;
import com.sap.isa.catalog.boi.IItem;
import com.sap.isa.catalog.boi.IQuery;
import com.sap.isa.catalog.boi.WebCatItemData;
import com.sap.isa.catalog.boi.WebCatItemListData;
import com.sap.isa.core.Iterable;
import com.sap.isa.core.TechKey;
import com.sap.isa.core.logging.IsaLocation;

/**
 * Stores a List of WebCatItems.
 */
public class WebCatItemList
    extends WebCatBusinessObjectBase
    implements Iterable, WebCatItemListData, ProductListBaseData {

    // Logging location
    private static IsaLocation log = IsaLocation.getInstance(WebCatItemList.class.getName());

    /**
     * Scenario constants
     */
    public final static int CV_LISTTYPE_BESTSELLER      = 1;
    public final static int CV_LISTTYPE_RECOMMENDATIONS = 2;
    public final static int CV_LISTTYPE_CATALOG_AREA    = 3;
    public final static int CV_LISTTYPE_CATALOG_SEARCH  = 4;
    public final static int CV_LISTTYPE_CUA = 5;
  
    public static boolean populateAll = false; // populate full list? needed for EBP
    private WebCatAreaQuery areaQuery;
    private WebCatArea creatingArea;

    private IQuery creatingQuery;
    private WebCatItemPage currentPage;
    protected boolean includeAccessories = false;
    protected boolean includeSubItems = false;
    protected ArrayList items;
    private HashMap itemsKeyMap;
    private HashMap itemsMap;
    private HashMap itemUnitsMap;
    private WebCatInfo parentCatalog;
    private Iterator resultIterator = null;
    private int resultProcessed = 0;
    private ContractAttributeList contractAttributeList = null;
    
    
    /**
     * should a default sales Bundle explosion be executed during populate()
     */
    protected boolean execDefSlsBdlExpl = true;
    /**
     * type of the WebCatItemList
     */
    private int listType = 0;

    /**
     * Create ItemList
     * 
     * @param theCatalog CatalogInfo referring to the current catalog
     */
    public WebCatItemList(WebCatInfo theCatalog) {

        log.entering("WebCatItemList(WebCatInfo theCatalog)");
        
        if (log.isDebugEnabled()) {
            log.debug("WebCatItemList:WebCatItemList(WebCatInfo " + theCatalog + ")");
        }
        
        parentCatalog = theCatalog;
        creatingQuery = null;
        creatingArea = null;
        items       = new ArrayList(0);
        itemsMap    = new HashMap(0);
        itemsKeyMap = new HashMap(0);
        currentPage = null;
        areaQuery = null;
        itemUnitsMap = null;
        populateAll = false;
        listType = 0;
        
        log.exiting();
    }

    /**
     * Create ItemList from a search
     * 
     * @param theCatalog CatalogInfo referring to the current catalog
     * @param query IQuery from which the ItemList is created
     */
    public WebCatItemList(WebCatInfo theCatalog, IQuery query) {

        this(theCatalog);
        log.entering("WebCatItemList(WebCatInfo theCatalog, IQuery query)");
        setQuery(query);
        
        if (log.isDebugEnabled()) {
            log.debug("WebCatItemList:WebCatItemList(WebCatInfo " + theCatalog 
                      + " , IQuery " + creatingQuery
                      + " , boolean " + includeAccessories
                      + " , populateAll="  + populateAll
                      + ")");
        }  
        
        log.exiting();
    }
    
    /**
     * Create ItemList from a search
     * 
     * @param theCatalog CatalogInfo referring to the current catalog
     * @param query IQuery from which the ItemList is created
     * @param upTo the number of items to populate, if upTo <= 0 just populate() will be executed
     */
    public WebCatItemList(WebCatInfo theCatalog, IQuery query, int upTo) {

        this(theCatalog);
        log.entering("WebCatItemList(WebCatInfo theCatalog, IQuery query)");
        setQuery(query, upTo);
        
        if (log.isDebugEnabled()) {
            log.debug("WebCatItemList:WebCatItemList(WebCatInfo " + theCatalog 
                      + " , IQuery " + creatingQuery
                      + " , boolean " + includeAccessories
                      + " , populateAll="  + populateAll
                      + " , upTo="  + upTo
                      + ")");
        }  
        
        log.exiting();
    }

    /**
     * Create ItemList from a search
     * 
     * @param theCatalog CatalogInfo referring to the current catalog
     * @param query IQuery from which the ItemList is created
     * @param includeAccessories boolean information whether accessories should be
     *        included in itemList
     */
    public WebCatItemList(WebCatInfo theCatalog, IQuery query, boolean includeAccessories) {

        this(theCatalog);
        
        log.entering("WebCatItemList(WebCatInfo theCatalog, IQuery query, boolean includeAccessories)");
        
        this.includeAccessories = includeAccessories;
        setQuery(query);
        
        if (log.isDebugEnabled()) {
            log.debug("WebCatItemList:WebCatItemList(WebCatInfo " + theCatalog 
                      + " , IQuery " + creatingQuery
                      + " , boolean " + includeAccessories
                      + " , populateAll="  + populateAll
                      + ")");
        }  
        
        log.exiting();
    }
    
    /**
     * Create ItemList from a search
     * 
     * @param theCatalog CatalogInfo referring to the current catalog
     * @param query IQuery from which the ItemList is created
     * @param includeAccessories boolean information whether accessories should be
     *        included in itemList
     * @param getAllItems should be set to true if the list should be completly be populated
     * @param execDefSlsBdlExpl should be set to true if the default components should be determined for Sales Bundles
     *                          during population
     */
    public WebCatItemList(WebCatInfo theCatalog, IQuery query, boolean includeAccessories, boolean getAllItems, boolean execDefSlsBdlExpl) {

        this(theCatalog);
        log.entering("WebCatItemList(WebCatInfo theCatalog, IQuery query, boolean includeAccessories, boolean getAllItems, boolean execDefSlsBdlExpl)");
        this.includeAccessories = includeAccessories;
        populateAll = getAllItems;
        setExecDefSlsBdlExpl(execDefSlsBdlExpl);
        setQuery(query);
        if (log.isDebugEnabled()) {
            log.debug("WebCatItemList:WebCatItemList(WebCatInfo " + theCatalog 
                      + " , IQuery " + creatingQuery 
                      + " , includeAccessories " + includeAccessories
                      + " , populateAll="  + populateAll
                      + " , execDefSlsBdlExpl " + execDefSlsBdlExpl 
                      + ")");
        }
        
        log.exiting();
    }

    /**
     * Create ItemList from a search
     * 
     * @param theCatalog CatalogInfo referring to the current catalog
     * @param query IQuery from which the ItemList is created
     * @param includeAccessories boolean information whether accessories should be
     *        included in itemList
     */
    public WebCatItemList(WebCatInfo theCatalog, IQuery query, boolean includeAccessories, boolean getAllItems) {

        
        this(theCatalog);
        log.entering("WebCatItemList(WebCatInfo theCatalog, IQuery query, boolean includeAccessories, boolean getAllItems)");
        this.includeAccessories = includeAccessories;
        populateAll = getAllItems;

        setQuery(query);
        
        if (log.isDebugEnabled()) {
            log.debug(
                "WebCatItemList:WebCatItemList(WebCatInfo " + theCatalog
                    + " , query "  + creatingQuery
                    + ", populateAll="  + populateAll
                    + ")");
        }
        log.exiting();
    }

    /**
     * Create ItemList from catalog Area
     * 
     * @param theCatalog CatalogInfo referring to the current catalog
     * @param newArea Area from which the ItemList is created
     */
    public WebCatItemList(WebCatInfo theCatalog, WebCatArea newArea) {

        this(theCatalog);
        log.entering("WebCatItemList(WebCatInfo theCatalog, WebCatArea newArea)");
        setArea(newArea);
        if (log.isDebugEnabled()) {
            log.debug(
                "WebCatItemList:WebCatItemList(WebCatInfo " + theCatalog
                    + " , WebCatArea " + creatingArea
                    + " , populateAll=" + populateAll
                    + ")");
        }

        log.exiting();
    }
    
    /**
     * Create ItemList from catalog Area
     * 
     * @param theCatalog CatalogInfo referring to the current catalog
     * @param newArea Area from which the ItemList is created
     * @param upTo the number of items to populate, if upTo <= 0 just populate() will be executed
     */
    public WebCatItemList(WebCatInfo theCatalog, WebCatArea newArea, int upTo) {

        this(theCatalog);
        log.entering("WebCatItemList(WebCatInfo theCatalog, WebCatArea newArea, int upTo)");
        setArea(newArea, upTo);
        if (log.isDebugEnabled()) {
            log.debug(
                "WebCatItemList:WebCatItemList(WebCatInfo " + theCatalog
                    + " , WebCatArea " + creatingArea
                    + " , populateAll="  + populateAll
                    + " , upTo=" + upTo
                    + ")");
        }

        log.exiting();
    }
    
    /**
     * Create ItemList from catalog Area, that populates all items.
     * 
     * @param theCatalog CatalogInfo referring to the current catalog
     * @param newArea Area from which the ItemList is created
     * @param getAllItems to create a list with populate all
     */
    public WebCatItemList(WebCatInfo theCatalog, WebCatArea newArea, boolean getAllItems) {

        this(theCatalog);
        log.entering("WebCatItemList(WebCatInfo theCatalog, WebCatArea newArea, boolean getAllItems))");
        populateAll = getAllItems;
        setArea(newArea);
        if (log.isDebugEnabled()) {
            log.debug(
                "WebCatItemList:WebCatItemList(WebCatInfo " + theCatalog
                    + " , WebCatArea " + creatingArea
                    + " , populateAll=" + populateAll
                    + ")");
        }
        log.exiting();
    }

    /**
     * Adds an <code>WebCatItem</code> to the <code>WebCatItemList</code>.
     * 
     * @param item to be added.
     */
    public void addItem(WebCatItem item) {

        final String METHOD_NAME = "addItem(WebCatItem item)";
        log.entering(METHOD_NAME);

        if (items == null) {

            items = new ArrayList();
            itemsMap = new HashMap();
            if (itemsKeyMap == null) {
                itemsKeyMap = new HashMap();
            }
        }

        items.add(item);
        itemsMap.put(item.getItemID(), item);
        itemsKeyMap.put(item.getItemID(), item.getItemKey());

        log.exiting();
    }

    /**
     * Inserts a <code>WebCatItem</code> into the <code>items</code> array at the
     * specified position.
     * 
     * @param   item to be added to this ItemList
     * @param   position in th <code>items</code> array
     */
    public void addItem(WebCatItem item, int position) {
        
        if (log.isDebugEnabled()) {
            log.debug("WebCatItemList:addItem(WebCatItem " + item.getItemID() + ")");
        }
        
        if (items == null) {
            items = new ArrayList();
            itemsMap = new HashMap();
            if (itemsKeyMap == null) {
                itemsKeyMap = new HashMap();
            }
        }

        items.add(position, item);
        itemsMap.put(item.getItemID(), item);
        itemsKeyMap.put(item.getItemID(), item.getItemKey());
    }

    /**
     * Aggregate all units of measurement and corresponding prices.
     * 
     * In case of R/3 backend all units and corresponding prices have to be attached to each 
     * WebCatItem. The method checks, if the aggregation is necessary by itself. 
     *  
     * @param webCatItem
     */
    private void aggregateUnits(WebCatItem webCatItem) {

        // Check, if aggregation is necessary
        String catalogType = parentCatalog.getCatalog().getClass().getName();
        boolean isBackendR3 =
            catalogType.endsWith("R3Catalog") || catalogType.endsWith("TrexManagedCatalog");
        boolean aggregateUnits = isBackendR3 && creatingQuery != null;
        if (!aggregateUnits) {
            return;
        }

        String productId = webCatItem.getProductID();
        HashMap theUnits;
        if (itemUnitsMap == null) {
            itemUnitsMap = new HashMap();
            theUnits = new HashMap();
        }
        else {
            theUnits = (HashMap) itemUnitsMap.get(productId);
            if (theUnits == null) {
                theUnits = new HashMap();
            }
        }

        String theUnit = webCatItem.getUnit();
        //WebCatItemPrice thePrice = webCatItem.readItemPrice();

        // if the same product with the same unit was found in different areas
        // the entry for the unit will always point to the last found WebCatItem
        // due to the fact, that this list is only used for static pricing, this won't harm
        // because even another item is used, the price will be the same.
        theUnits.put(theUnit, webCatItem);

        itemUnitsMap.put(productId, theUnits);
        if (itemUnitsMap != null) {
            webCatItem.setUnits((HashMap) itemUnitsMap.get(webCatItem.getProductID()));
        }
    }

    /**
     * Get the page size dependent on the kind of item list to which will be displayed. 
     * 
     * @param type of displayed item list as int
     * @return page size as int 
     */
    private int getPageSize(int listtype) {
        
        if (log.isDebugEnabled()) {
            log.debug("getPageSize(listtype " + listtype + ")");
        }

        int pageSize = 0;

        switch (listtype) {
            case CV_LISTTYPE_BESTSELLER :
                pageSize = parentCatalog.getPageSizeBestseller();
                break;
            case CV_LISTTYPE_RECOMMENDATIONS :
                pageSize = parentCatalog.getPageSizeRecomm();
                break;
            case CV_LISTTYPE_CATALOG_SEARCH :
                pageSize = parentCatalog.getPageSizeCatSearch();
                break;
            case CV_LISTTYPE_CATALOG_AREA :
                pageSize = parentCatalog.getPageSizeCatArea();
                break;
            default :
                pageSize = parentCatalog.getItemPageSize();
                break;
        }
        return pageSize;
    }

    /**
     * Get the minimum value of page size dependent on the kind of item list to which will be displayed. 
     * 
     * @param type of displayed item list as int
     * @return page size as int 
     */
    private int getPageSizeMin(int listtype) {
        
        if (log.isDebugEnabled()) {
            log.debug("getPageSizeMin(listtype " + listtype + ")");
        }
        
        int pageSize = 0;

        switch (listtype) {
            case CV_LISTTYPE_BESTSELLER :
                pageSize = parentCatalog.getPageSizeBestsellerMin();
                break;
            case CV_LISTTYPE_RECOMMENDATIONS :
                pageSize = parentCatalog.getPageSizeRecommMin();
                break;
            case CV_LISTTYPE_CATALOG_SEARCH :
                pageSize = parentCatalog.getPageSizeCatSearchMin();
                break;
            case CV_LISTTYPE_CATALOG_AREA :
                pageSize = parentCatalog.getPageSizeCatAreaMin();
                break;
            default :
                pageSize = parentCatalog.getItemPageSizeMin();
                break;
        }
        return pageSize;
    }

    /**
     * Creates a new <code>WebCatItem</code>.
     * 
     * @param itemKey the key of the item
     * @param catalogItem the <code>IItem</code> of the new <code>WebCatItem</code>.
     */
    public WebCatItem createNewItem(WebCatItemKey itemKey, IItem catalogItem) {

        return parentCatalog.getItem(itemKey, catalogItem);
    }
    
    /**
     * Before removing this object, make sure that referenced objects are handled. This is 
     * currently done by calling the releaseReferences() method.
     */
    protected void finalize() throws Throwable {
        log.entering("finalize()");
        if (log.isDebugEnabled()) {
            log.debug("Finalize WebCatItemList: " + this);
        }
        parentCatalog = null;
        creatingQuery = null;
        creatingArea = null;
        items       = null;
        itemsMap    = null;
        itemsKeyMap = null;
        currentPage = null;
        areaQuery = null;
        itemUnitsMap = null;
        
        super.finalize();
        
        log.exiting();
    }

    /**
     * Creates the <code>resultIterator</code> for this <code>WebCatItemList</code>.
     * 
     * Based on the <code>creatingArea</code> and the <code>creatingQuery</code> the query
     * is carried out and a <code>resultIterator</code> is created.<br>
     * 
     * The <code>items</code> and the <code>itemsMap</code> are initialzed by this.
     * If <code>creatingArea</code> and the <code>creatingQuery</code> are both null,
     * nothing is done.
     *
     * @return true, if a resultIterator has been created,
     *         false, otherwise.
     */
    private boolean createResultIterator() {

        final String METHOD_NAME = "createResultIterator()";
        log.entering(METHOD_NAME);

        if (resultIterator != null) {
            return true;
        }

        if (null == creatingArea && null == creatingQuery) {
            log.debug("Cannot create resultIterator, because creatingArea and creatingQuery are null");
            log.exiting();
            return false;
        }

        resultProcessed = 0;

        // Initialize the items and itemsMap
        items = new ArrayList();
        itemsMap = new HashMap();
        if (itemsKeyMap == null) {
            itemsKeyMap = new HashMap();
        }

        // creating area is available -> return all items of the area
        if (creatingArea != null) {

            // this is for the shared manufacturer catalog:
            if (parentCatalog instanceof WebCatInfoModifiable) {

                WebCatInfoModifiable catModify = (WebCatInfoModifiable) parentCatalog;
                WebCatItemModifiable[] newItems = catModify.getNewItems(creatingArea.getAreaID());
                for (int i = 0; i < newItems.length; i++) {
                    if (!WebCatModificationStatus
                        .DELETED_OBJECT
                        .equals(newItems[i].getModificationStatus()))
                        addItem(newItems[i]);
                }
            }

            // Check the category of the area
            if (null == creatingArea.getCategory()) {
                log.error(
                    "invalid category",
                    new String[] { creatingArea.getAreaID(), creatingArea.getAreaName()},
                    new Exception("Catalog Exception"));

                log.exiting();
                return false;
            }

            // get the items of the category
            try {
                log.debug("Performing a creatingArea.getCategory().getItems()");
                resultIterator = creatingArea.getCategory().getItems();
            }
            catch (CatalogException e) {
                log.error("system.exception", e);
                log.exiting();
                return false;
            }
        } // if (creatingArea != null) 

        // Perform a query for creatingQuery in all other cases.
        if (null == resultIterator) {
            log.debug("Performing a creatingQuery.submit()");
            try {
                resultIterator = creatingQuery.submit();
            }
            catch (CatalogException e) {
                log.error("system.exception", e);
                log.exiting();
                return false;
            }
        }

        log.exiting();
        return true;
    }

    /**
     * get Area this ItemList was created from
     * @return area Area this ItemList was created from.
     *         null if ItemList was not created from an Area.
     */
    public WebCatArea getArea() {
        return creatingArea;
    }

    /**
     * Returns the <code>WebCatAreaQuery</code>.
     * 
     * @return areaQuery
     */
    public WebCatAreaQuery getAreaQuery() {
        return areaQuery;
    }

    /**
     * Get the last used ItemPage. This is used to display the same items when
     * leaving the catalog and re-entering.
     * 
     * @return itemPage the last visited page of items of this ItemList
     *                  null if no page was visited yet.
     */
    public WebCatItemPage getCurrentItemPage() {
        return currentPage;
    }

    /**
     * Get a distinct item of the ItemList.
     * 
     * @param line line number of the item to be returned.
     * @return item the item at "line" of the itemlist
     */
    public WebCatItem getItem(int line) {

        final String METHOD_NAME = "getItem(int line)";
        log.entering(METHOD_NAME);
        
        if (log.isDebugEnabled()) {
            log.debug("line=" + line);
        }

        if (resultProcessed <= line) {
            populate(line + 1); // ensure that item is already read
        }

        log.exiting();
        return (WebCatItem) items.get(line);
    }

    /**
     * Get a distinct item of the ItemList by item ID.
     * 
     * @param   itemID ID of the item to be returned.
     * 
     * @return  item the item with the given ID of the itemlist, null if not present.
     */
    public WebCatItem getItem(String itemID) {

        if (log.isDebugEnabled()) {
            log.debug("WebCatItemList:getItem(String " + itemID + ")");
        }
        
        WebCatItem item = null;
        WebCatItemKey itemKey = null;
        if (itemsMap == null || itemsKeyMap == null) {
            return null;
        }

        // check whether item is in item map
        item = (WebCatItem) itemsMap.get(itemID);
        if (item == null) {
            // if item not in item map, check whether item key is in item key map.
            // this is the case if items and item map were released for memory
            // saving reasons
            itemKey = (WebCatItemKey) itemsKeyMap.get(itemID);
            if (itemKey != null) {
                item = parentCatalog.getItem(itemKey);
            }
        }
        return item;
    }

    /**
     * Returns the WebCatItemData entry for a given index in the WebCatItemList.
     *  
     * @return the WebCatItemData for the given index.
     */
    public WebCatItemData getItemData(int index) {

        return getItem(index);
    }

    /**
     * Get one page of items.
     * @param page number of the page to be displayed. Number of items per page is
     *             defined in pageSize. Page number of <code>0</code> means last
     *             page!
     * @return itemPage page containing the items to be displayed on the page.
     * 
     * @deprecated due to new constructor with paramter listtype. 
     */
    public WebCatItemPage getItemPage(int page) {

        if (log.isDebugEnabled()) {
            log.debug("WebCatItemList:getItemPage(int " + page + ")");
        }
        
        WebCatItemPage itemPage;
        if ((currentPage != null) && (page == currentPage.getPage())) {
            itemPage = currentPage;
        }
        else {
            int pageSize = parentCatalog.getItemPageSize();
            if (pageSize > 0) {
                if (page == 0) // last page!
                    {
                    populate(-1);
                    page = (items.size() + pageSize - 1) / pageSize;
                }
                else {
                    populate(page * pageSize);
                    // check if selected page is out of range, if so set to last page
                    if (items.size() <= ((page - 1) * pageSize)) {
                        page = (items.size() + pageSize - 1) / pageSize;
                    }
                }
                itemPage = new WebCatItemPage(this, page, pageSize);
            }
            else {
                populate(-1);
                itemPage = new WebCatItemPage(this);
            }
            currentPage = itemPage;
        }
        return itemPage;
    }
    
    /**
     * Get the items for the given indizes (including) 
     */
    public List getItems(int startIdx, int endIdx) {

        if (log.isDebugEnabled()) {
            log.debug("getItems(int " + startIdx + ", " + endIdx + ")");
        }
        
         if (startIdx < 0) {
             startIdx = 0; 
             log.debug("Set startIdx to 0");    
         }
         
        if (endIdx < 0) {
            endIdx = 0;    
            log.debug("Set endIdx to 0"); 
        }
        
        if (endIdx < startIdx) {
            endIdx = startIdx;
            log.debug("Set endIdx to startIdx");
        }

        populate(endIdx);
        
        return items.subList(startIdx, endIdx);
    }

    /**
     * Get one page of items.
     * @param page number of the page to be displayed. Number of items per page is
     *             defined in pageSize. Page number of <code>0</code> means last
     *             page!
     * @param type of item list to be displayed as int. The page size depends on the type of list 
     *             and the type of presentation (block view or list view). 
     * @return itemPage page containing the items to be displayed on the page.
     */
    public WebCatItemPage getItemPage(int page, int listtype) {

        if (log.isDebugEnabled()) {
            log.debug("WebCatItemList:getItemPage(int " + page + "," + listtype + ")");
        }
        
        WebCatItemPage itemPage;
        int pageSize = getPageSize(listtype);
        int pageSizeMin = getPageSizeMin(listtype);
        if (pageSize > 0) {
            if (page == 0) { // last page!
                populate(-1);
                page = (items.size() + pageSize - 1) / pageSize;
            }
            else {
                populate(page * pageSize);
                // check if selected page is out of range, if so set to last page
                if (items.size() <= ((page - 1) * pageSize)) {
                    page = (items.size() + pageSize - 1) / pageSize;
                }
            }
            itemPage = new WebCatItemPage(this, page, pageSize, pageSizeMin);
        }
        else {
            populate(-1);
            itemPage = new WebCatItemPage(this, 0, 0, pageSizeMin);
        }
        currentPage = itemPage;
        return itemPage;
    }

    /**
     * get all Items of this ItemList
     * 
     * @return items all items of the ItemList
     */
    public ArrayList getItems() {

        if (items == null) {
            populate(-1);
        }
        return items;
    }

    public ArrayList getItems(boolean all) {
        if (all) {
            populate(-1);
        }
        else {
            getItems();
        }

        return items;
    }
    
    /**
     * Return only the populated items
     * 
     * @return items popuöated items of the ItemList
     */
    public ArrayList getItemsOnlyPopulated() {
        return items;
    }

    /**
     * @return itemUnitsMap
     */
    public HashMap getItemUnits() {
        return itemUnitsMap;
    }
    /**
     * @return parentCatalog
     */
    public WebCatInfo getParentCatalog() {
        return parentCatalog;
    }

	/**
	 * Get a product item of the ItemList.
	 * 
	 * @param line line number of the item to be returned.
	 * @return item of type ProductBaseData at "line" of the itemlist
	 */
	public ProductBaseData getProduct(int line) {

        if (log.isDebugEnabled()) {
            log.debug("getProduct( " + line + " )");
        }
        
		return (ProductBaseData) getItem(line);
	}

    /**
     * Convenience method to access a <code>WebCatSubItem</code> from the <code>
     * WebCatItemList</code>.<br>
     * 
     * Returns a <code>WebCatSubItem</code> for a specified <code>TechKey</code> 
     * from a <code>WebCatItem</code> of this <code>WebCatItemList</code>. The <code>
     * WebCatItem</code> is specified by the item ID. 
     * 
     * @return webCatSubitem or null in the case of errors.
     */
    public WebCatSubItem getSubItem(String itemID, TechKey webCatSubItemKey) {

        // get the specified WebCatItem
        WebCatItem webCatItem = getItem(itemID);

        // get the WebCatSubItemList of this item
        WebCatSubItemList webCatSubItemList = webCatItem.getWebCatSubItemList();
        if (webCatSubItemList == null) {
            return null;
        }

        // get the WebCatSubItem
        WebCatSubItem webCatSubItem = webCatSubItemList.getItem(webCatSubItemKey);

        return webCatSubItem;
    }

    /**
     * get information about search which created this ItemList
     * @return search Search information.
     *         null if this ItemList was not created from a search.
     */
    public IQuery getQuery() {
        return creatingQuery;
    }

    /**
     * Checks, if subitems should be returned.
     * 
     * @return true, if subitems should be included
     *         false, otherwise
     */
    public boolean isIncludeSubItems() {
        return includeSubItems;
    }

    /**
     * Checks, if an item is relevant for the <code>WebCatItemlist</code></br>
     * 
     * @param   catalogItem to be checked.
     * @return  true, if the item has to be included in the list
     *          false, otherwise.
     */
    public boolean isRelevantForList(WebCatItem item) {
        
        if (log.isDebugEnabled()) {
            log.debug("item " + item.getProduct() + " isMainItem=" + item.isMainItem() + " isAcessory=" + item.isAccessory() + 
                      " isSubComponent=" + item.isSubComponent());
                  
            log.debug("includeAccessories=" + includeAccessories + " isIncludeSubItems()=" + isIncludeSubItems());
        }

        if (item.isMainItem()) {
            return true;
        }

        if (includeAccessories && item.isAccessory()) {
            return true;
        }

        if (isIncludeSubItems() && 
            (item.isSubComponent())) {
            return true;
        }

        return false;
    }

    /**
     * get iterator for itemlist.
     * This method returns an iterator for all items in the item list, no matter
     * whether they have been populated before. Therefore it calls
     * <code>populate(-1)</code> first to ensure that all items of the list are
     * populated. For performance it might be better to call
     * <code>iteratorOnlyPopulated()</code> instead.
     * @return iterator iterator of item list
     */
    public Iterator iterator() {

        log.debug("WebCatItemList:iterator()");
        populate(-1);
        return iteratorOnlyPopulated();
    }

    /**
     * get iterator for itempage that includes only those items which are already
     * populated. In difference to <code>iterator()</code>, this does NOT do a
     * <code>populate(-1)</code> first. This is useful if you only want to see
     * populated items anyway, for example when adding items to basket.
     * @return iterator iterator of item list that only includes already populated
     *         items
     */
    public Iterator iteratorOnlyPopulated() {

        log.debug("WebCatItemList:iteratorOnlyPopulated()");
        if (items == null) {
            return (new ArrayList(0)).iterator();
        }

        return items.iterator();
    }
    
    /**
     * returns as iterator a list of all selected items, it could be a main item or a contract item
     * 
     * @return <code>Iterator</code> if item list that are selected
     */
    public Iterator iteratorOnlySelected() {
        log.debug("iteratorOnlySelected");
        
        ArrayList arrayOfSelected = new ArrayList();
        WebCatItem aWebCatItem = null;
        WebCatItem aWebCatSubItem = null;
        if(items != null ) {
           Iterator iItems = items.iterator();
           while(iItems.hasNext()) {
               aWebCatItem = (WebCatItem)iItems.next();
             //main item?
               if(aWebCatItem.isSelected() ) {
                   arrayOfSelected.add(aWebCatItem);
               }
               else {
                  //contract item ?
                  if(aWebCatItem.getContractItemsArray() != null) {
                      Iterator iContractItems = aWebCatItem.getContractItemsArray().iterator();
                      while( iContractItems.hasNext() ) {
                         aWebCatSubItem = (WebCatItem)iContractItems.next();
                       //is it selected?
                         if(aWebCatSubItem.isSelected()) {
                             arrayOfSelected.add(aWebCatItem);
                             break; //add the item once
                         }
                      }
                   } 
               }
           }
        }
        
        if (log.isDebugEnabled()) {
            log.debug("returned count: " + arrayOfSelected.size());
        }
        
        log.exiting();
        return arrayOfSelected.iterator();
    }

    /**
     * populate list of WebCatItems
     * For memory saving and performance reasons the ArrayList of WebCatItems
     * belonging to this WebCatItemList can be evaluated later or dropped.
     * This method is used to (re-)populate the list
     * for further usage by using a HashMap with WebCatItemKey's which
     * is not dropped.
     * populate() is also used when filling the WebCatItemList the first time.
     * it refers to <code>populate(2)</code> to fill at least the first two items,
     * which is used to check whether the list is empty or contains only one
     * item or contains more items.
     */
    void populate() {

        if (populateAll)
            populate(-1);
        else
            populate(2);
    }

    /**
     * Populates a list of WebCatItems<br>
     * 
     * For memory saving and performance reasons the ArrayList of WebCatItems belonging to this 
     * WebCatItemList can be evaluated later or dropped. This method is used to (re-)populate the 
     * list for further usage by using a HashMap with WebCatItemKey's which is not dropped.
     * 
     * The number of items to be filled into the list has to be provided as parameter. The method 
     * checks, how many items were already fetched and only tries to retrieve the rest. This is 
     * useful to retrieve "enough" items to fill the itemPage.
     * 
     * @param upTo number up to which the itemList should be filled.
     *        negative number means fill all.
     */
    synchronized void populate(int upTo) {

        final String METHOD_NAME = "populate(int upTo)";
        log.entering(METHOD_NAME);
        
        if (log.isDebugEnabled()) {
            log.debug("upTo=" + upTo);
        }

        //if itemList is a "manual" one, i.e. it is not created from a query
        //or catalog area, but by adding Items with the addItem() Method,
        //populate does not make any sense.
        if (resultIterator == null && creatingArea == null && creatingQuery == null) {
            if (log.isDebugEnabled()) {
                log.debug("resultIterator: " + resultIterator + " / creatingArea: " + creatingArea + " / creatingQuery: " + creatingQuery);
            }
            log.exiting();
            return;
        }

        // create a resultIterator (submits the query)
        if (createResultIterator() == false) {
            log.debug("createResultIterator = false");
            log.exiting();
            return; // nothing to do
        }

        int toDo = upTo - resultProcessed;
        if (log.isDebugEnabled()) {
            log.debug("Already read: " + resultProcessed + ", target: " + upTo + " => to do: " + toDo);
        }

        while (((upTo < 0) || (toDo > 0)) && resultIterator.hasNext()) {

            // access catalog item
            IItem catalogItem = (IItem) resultIterator.next();

            // Get or create WebCatItemKey
            WebCatItemKey itemKey = (WebCatItemKey) itemsKeyMap.get(catalogItem.getGuid());
            if (itemKey == null) {
                itemKey = new WebCatItemKey(parentCatalog, catalogItem);
            }

            // Create WebCatItem
            WebCatItem item = createNewItem(itemKey, catalogItem);
            if (log.isDebugEnabled()) {
                log.debug("isExecDefSlsBdlExpl()=" + isExecDefSlsBdlExpl());
            }
				
            // Skip not relevant items
            if (isRelevantForList(item) == false) {
                if (log.isDebugEnabled()) {
                    log.debug("item " + item.getProduct() + " not relevant for list");
                }
                continue; // while
            }
            
            if (isExecDefSlsBdlExpl()) {
                // Create WebCatSubItemList for a package of sales components
                item.createDefSlsCompWebCatSubItemList();
            }
            // adjust counters
            toDo--;
            resultProcessed++;

            // Skip deleted modifiable items
            if (item instanceof WebCatItemModifiable
                && WebCatModificationStatus.DELETED_OBJECT.equals(
                    ((WebCatItemModifiable) item).getModificationStatus())) {

                continue; // while
            }

            // aggregate units, if required
            aggregateUnits(item);
            
            //read the contract duration
            item.populateContractDuration();

            // add item to the list
            addItem(item);
            if (log.isDebugEnabled()) {
                log.debug("added item " + item.getItemID() + " (" + size() + ")");
            }

        } // while
        
        if (log.isDebugEnabled()) {
            log.debug("populate() end, " + items.size() + " items");
        }
        
        log.exiting();
    }

    /**
      * Removes an item from the ItemList.
      * 
      * @param line line number of the item to be returned.
      * @return item that has been removed
      */
    public WebCatItem removeItem(int line) {

        final String METHOD_NAME = "removeItem(int line)";
        log.entering(METHOD_NAME);
        
        if (log.isDebugEnabled()) {
            log.debug("line=" + line);
        }

        // read the item
        WebCatItem item = getItem(line);

        // remove 
        itemsMap.remove(item.getItemID());
        itemsKeyMap.remove(item.getItemID());
        items.remove(line);

        log.exiting();
        return item;
    }

    /**
     * Reset the itemlist.
     */
    public void reset() {

        resetCurrentItemPage();
        items = null;
        itemsMap = null;
        itemsKeyMap = null;
        resultIterator = null;
        populate();
    }

    /**
     * reset last used ItemPage. This is used for example when sorting in different
     * order as items of page change.
     */
    public void resetCurrentItemPage() {

        log.debug("WebCatItemList:resetCurrentItemPage()");
        currentPage = null;
    }

    /**
     * Set area information for the ItemList
     * 
     * @param newArea Area this ItemList belongs to
     */
    public void setArea(WebCatArea newArea) {

        if (log.isDebugEnabled()) {
            log.debug("WebCatItemList:setArea(WebCatArea " + newArea.getAreaID() + ")");
        }
        
        creatingArea = newArea;
        creatingQuery = null;
        setListType(CV_LISTTYPE_CATALOG_AREA);
        if (newArea.getAreaID().equals(WebCatArea.ROOT_AREA)) {
            //area not changed
        }
        else {
            populate();
        }
    }
    
    /**
     * Set area information for the ItemList
     * 
     * @param newArea Area this ItemList belongs to
     * @param upTo the number of items that should be populated
     *        if <= 0 just populate() will be called
     */
    public void setArea(WebCatArea newArea, int upTo) {

        if (log.isDebugEnabled()) {
            log.debug("WebCatItemList:setArea(WebCatArea " + newArea.getAreaID() + " upT0 " + upTo + ")");
        }
        
        creatingArea = newArea;
        creatingQuery = null;
        setListType(CV_LISTTYPE_CATALOG_AREA);
        if (newArea.getAreaID().equals(WebCatArea.ROOT_AREA)) {
            //area not changed
        }
        else {
            if (upTo <= 0) {
                populate();
            }
            else {
                populate(upTo);
            } 
        }
    }

    public void setAreaQuery(WebCatAreaQuery areaQuery) {
        this.areaQuery = areaQuery;
    }
    /**
     * @param includeSubItems
     */
    public void setIncludeSubItems(boolean includeSubItems) {
        this.includeSubItems = includeSubItems;
    }

    /**
     * @param map
     */
    public void setItemUnits(HashMap map) {
        this.itemUnitsMap = map;
    }

    /**
     * Store information about the search which creates this ItemList
     * 
     * @param query Search information
     */
    public void setQuery(IQuery query) {

        creatingArea = null;
        creatingQuery = query;
        setListType(CV_LISTTYPE_CATALOG_SEARCH);
        populate();
    }
    
    /**
     * Store information about the search which creates this ItemList
     * 
     * @param query Search information
     * @param upTo the number of items that should be populated
     *        if <= 0 just populate() will be called
     */
    public void setQuery(IQuery query, int upTo) {

        creatingArea = null;
        creatingQuery = query;
        setListType(CV_LISTTYPE_CATALOG_SEARCH);
        if (upTo <= 0) {
            populate();
        }
        else {
            populate(upTo);
        } 
    }

    /**
     * Get size of ItemList.
     * 
     * @return size number of items in this ItemList.
     */
    public int size() {

        if (itemsKeyMap == null) {
            return 0;
        }

        return itemsKeyMap.size();
    }

    public void sort(String attribute) {
        
        if (log.isDebugEnabled()) {
            log.debug("WebCatItemList:sort(" + attribute + ")");
        }
            
        if (items != null) {
            populate(-1);
            Collections.sort(items, new WebCatItemComparator(attribute));
        }
        currentPage = null;
    }

    /**
     * Get size of ItemList from pcatAPI method data instead of it's own count.
     * 
     * @return size number of items in this ItemList.
     */
    public int totalSize() {

        log.debug("Determine number of items");

        int ret = 0;
        
        if (creatingQuery != null) {
            log.debug("Determine number of items from underlying query");
            ret = creatingQuery.getCount();
        }
        else if (creatingArea != null) {
            log.debug("Determine number of items from underlying area");
            ret = creatingArea.getCategory().getCount();
        }
        else {
            log.debug("Determine number of items from items array");
            ret = size();
        }
        
        if (log.isDebugEnabled()) {
            log.debug("Determined number of items=" + ret);
        } 

        return ret;
    }
    
    /**
     * Get no of main items
     * 
     * @return int no of main
     */
    public int getNoOfMainItems() {
        
        log.debug("Determine number of main items");

        int ret = 0;
        
        if (creatingQuery != null) {
            log.debug("Determine number of main items from underlying query");
            ret = creatingQuery.getMainItemCount();
        }
        else if (creatingArea != null) {
            log.debug("Determine number of main items from underlying area");
            ret = creatingArea.getCategory().getMainItemCount();
        }
        else {
            if (includeAccessories) {
                log.debug("Determine number of main items from items array, includeAccessories = true");
                for (int i = 0; i < items.size(); i++) {
                    if (((WebCatItem) items.get(i)).isMainItem()) {
                        ret++;
                    }
                }                
            } 
            else {
                // Display also the number of Accessories
                log.debug("Determine number of items from items array, includeAccessories = false"); 
                ret = items.size();
            }

        }
        
        if (log.isDebugEnabled()) {
            log.debug("Determined number of main items=" + ret);
        }

        return ret;
    }

    /**
     * Get flag to indicate if WebCatItemList is created by an area query
     * @return isAreaQuery flag 
     */
    public boolean isCreatedByAreaQuery() {
        return (getAreaQuery() != null);
    }
    /**
     * Returns true if the populate Method should execute a default Sales Bundle
     * explosion from the TREX data for every sales bundle prdouct that is populated
     * 
     * @return true if default Sales Bundle explosion will be executed
     *         false else
     */
    public boolean isExecDefSlsBdlExpl() {
        return execDefSlsBdlExpl;
    }

    /**
     * Sets the flag, if the populate Method should execute a default Sales Bundle
     * explosion from the TREX data for every sales bundle prdouct that is populated
     * 
     * @param execDefSlsBdlExpl true if default Sales Bundle explosion should be executed
     */
    public void setExecDefSlsBdlExpl(boolean execDefSlsBdlExpl) {
        this.execDefSlsBdlExpl = execDefSlsBdlExpl;
    }

    /**
     * Returns the type of the WebCatItemList.
     * The types are defined in the WebCatItemList and ActionConstants.
     * 
     * @return listType
     */
    public int getListType() {
        return this.listType;
    }

    /**
     * Sets the type of the WebCatItemList.
     * 
     * @param listType as int 
     */
    public void setListType(int listType) {
        this.listType = listType;
    }

    /**
     * Resets the SC configuration of the items in the WebCatItemList.
     * 
     */
    public void resetToDefCmp(ArrayList itemsForRepricing) {
        WebCatItem item;
        for (int i = 0; i < items.size(); i++) {
            item = (WebCatItem) items.get(i);
            if (item.isMainItem() && item.isExploded()) {
                item.resetToDefCmp();
                itemsForRepricing.add(item);
            }
        }
    }
    
    /**
     * return the contract attribute list associated to this web cat item list
     * @return the contract attribute List if is set
     *         null otherwise
     */
	public ContractAttributeList getContractAttributeList() {
		return contractAttributeList;
	}

	/**
	 * sets the contract attribute list
	 * @param list: the contract attribute list that need to be set
	 */
	public void setContractAttributeList(ContractAttributeList list) {
		contractAttributeList = list;
	}

}
