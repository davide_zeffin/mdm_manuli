package com.sap.isa.catalog.webcatalog;

/**
 * Title:        Catalog UI Area Query
 * Description:  Contains info regarding previous query on area specific search
 * Copyright:    Copyright (c) 2001
 * Company:      SAPMarkets
 * @author
 * @version 1.0
 */

import java.util.ArrayList;

import com.sap.isa.catalog.boi.IFilter;

public class WebCatAreaQuery extends WebCatBusinessObjectBase {

  private ArrayList prevAttribs;
  private IFilter prevFilter;
  private ArrayList prevNames;
  private ArrayList prevValues;

  public WebCatAreaQuery() {
    prevAttribs = null;
    prevNames = null;
    prevValues = null;
    prevFilter = null;
  }
  
  protected void finalize() throws Throwable { 
      if (log.isDebugEnabled()) {
          log.debug("Finalize WebCatAreaQuery: " + this);
      }
        
      prevAttribs = null;
      prevNames = null;
      prevValues = null;
      prevFilter = null;
        
      super.finalize();
  }

  public ArrayList getPrevAttribs() {
      return prevAttribs;
  }

  public void setPrevAttribs(ArrayList prevAttribs) {
      this.prevAttribs = prevAttribs;
  }

  public ArrayList getPrevNames() {
      return prevNames;
  }

  public void setPrevNames(ArrayList prevNames) {
      this.prevNames = prevNames;
  }

  public ArrayList getPrevValues() {
      return prevValues;
  }

  public void setPrevValues(ArrayList prevValues) {
      this.prevValues = prevValues;
  }

  public IFilter getPrevFilter() {
      return prevFilter;
  }

  public void setPrevFilter(IFilter prevFilter) {
      this.prevFilter = prevFilter;
  }

  }
