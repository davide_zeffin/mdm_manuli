/*****************************************************************************

    Class:        WebCatItem
    Copyright (c) 2006, SAP AG, Germany, All rights reserved.
    Created:      2001

*****************************************************************************/
package com.sap.isa.catalog.webcatalog;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Comparator;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.Set;
import java.util.StringTokenizer;
import java.util.Vector;

import com.sap.isa.backend.boi.isacore.contract.ContractReferenceListData;
import com.sap.isa.backend.boi.webcatalog.solutionconfigurator.SolutionConfiguratorData;
import com.sap.isa.backend.boi.webcatalog.views.ViewRelevant;
import com.sap.isa.businessobject.CommunicationException;
import com.sap.isa.businessobject.ProductBaseData;
import com.sap.isa.businessobject.contract.ContractReference;
import com.sap.isa.businessobject.item.ContractDuration;
import com.sap.isa.businessobject.webcatalog.atp.AtpResultList;
import com.sap.isa.businessobject.webcatalog.exchproducts.ExchProductResult;
import com.sap.isa.businessobject.webcatalog.pricing.PriceCalculator;
import com.sap.isa.businessobject.webcatalog.solutionconfigurator.SolutionConfigurator;
import com.sap.isa.catalog.AttributeKeyConstants;
import com.sap.isa.catalog.boi.CatalogException;
import com.sap.isa.catalog.boi.IAttributeValue;
import com.sap.isa.catalog.boi.ICatalog;
import com.sap.isa.catalog.boi.ICategory;
import com.sap.isa.catalog.boi.IFilter;
import com.sap.isa.catalog.boi.IItem;
import com.sap.isa.catalog.boi.IQuery;
import com.sap.isa.catalog.boi.IQueryStatement;
import com.sap.isa.catalog.boi.WebCatInfoData;
import com.sap.isa.catalog.boi.WebCatItemData;
import com.sap.isa.catalog.boi.WebCatSubItemListData;
import com.sap.isa.catalog.filter.CatalogFilterFactory;
import com.sap.isa.core.Iterable;
import com.sap.isa.core.TechKey;
import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.core.util.table.ResultData;
import com.sap.spc.remote.client.object.Configuration;
import com.sap.spc.remote.client.object.IPCException;
import com.sap.spc.remote.client.object.IPCItem;
import com.sap.spc.remote.client.util.cfg_ext_cstic_val;
import com.sap.spc.remote.client.util.cfg_ext_inst;
import com.sap.spc.remote.client.util.cfg_ext_part;
import com.sap.spc.remote.client.util.cfg_ext_price_key;
import com.sap.spc.remote.client.util.ext_configuration;
import com.sap.spc.remote.client.util.imp.c_ext_cfg_imp;
import com.sap.spc.remote.client.util.imp.c_ext_cfg_inst_seq_imp;

/**
 * Represents an item of the web catalog. It is a collection for all the item related
 * data like the data from the catalog, configuration data, contract data etc.
 */
public class WebCatItem
    extends WebCatBusinessObjectBase
    implements ViewRelevant, WebCatItemData, ProductBaseData, Cloneable, Iterable {

    private static IsaLocation log = IsaLocation.getInstance(WebCatItem.class.getName());

    /** central item data (like ID) from which the rest can be redetermined. */
    private WebCatItemKey itemKey = null;

    /** the data coming from the catalog engine for this item. */
    private IItem catalogItem = null;

    /**
     * contract reference data, to be shown in an item list. See customizing of contract data 
     * views in CRM for how to add fields.
     */
    private ContractReference contractRef = null;

    /**
     * contract reference data, to be shown on some item detail page. See customizing of contract 
     * data views in CRM for how to add fields.
     */
    private ContractReference contractDetailedRef = null;

    /**
     * reference to the configuration, to be used by some external configuration engine. Currently, 
     * this is allways the IPC.
     */
    private IPCItem configItemReference = null;

    /** Availability is done in a different way currently, so this part is not used. */
    private Object availability = null;

    /**
     * For each contract which is found for the product, a contract related subitem is created. 
     * The subitems are WebCatItems, too. This contains all these subitems.
     */
    private HashMap contractItems = null;
    private ArrayList contractItemsArray = null;

    /**
     * As prices can be complex, like scale prices or configuration dependant prices, there are 
     * special classes for the price.
     */
    private WebCatItemPrice price = null;

    /** eAuction link (reference?) */
    private String eAuctionLink = null;
    private WebCatAreaAttributes areaAttributes = null;

    /** Unit of Measurement */
    private String unit = null;
    private HashMap units = null;
    private String remanufacturableFlag = null;
    private String exchBusinessFlag = null;
    private ResultData condPurpGroup = null;
    private ExchProductResult exchProdData = null;

    /**
     * Stores the reference to the optionally existing object. This object will be used for 
     * bundles or interlinkaged products to determine their components or sub items. 
     */
    protected SolutionConfigurator solutionConfigurator = null;

    /**
     * Holds a list of all <code>WebCatSubItem</code>s belonging to this item. This list will either be filled 
     * from the SolutionConfigurator object, or with data coming from the TREX, in case the 
     * default bundle configuration is set.
     */
    protected WebCatSubItemList webCatSubItemList = null;

    /**
     * Stores the unique key for the parent item of this item. The attribute will be null by 
     * default. This key can be used to create a hierarchy for the items in conjunction with 
     * itemTechKey.
     */
    protected TechKey parentTechKey = null;

    /**
     * Stores the Guid of the baskets SC document, if the item coming from the basket. This field 
     * is to be set if the details for a basket item are displayed and the SC might be called, so the SC
     * can determine the items SC configuration from the orders SC config document.
     */
    protected TechKey scOrderDocumentGuid = null;

    /**
     * Stores attributes, necessary for the IPC pricing of the sub components. The Values will be 
     * stored as key<->value pair and be used in the PriceCalculator during creation of the IPCItems.
     */
    protected HashMap scRelvIPCAttr = null;

    /**
     * Stores an external configuration, that should be taklen into account during IPCItem creation.
     */
    protected c_ext_cfg_imp extCfg;
    
    /**
     * this array stores the different contract duration for this WebCatItem
     */
    protected ArrayList contractDurationArray;
    
    /**
     * represents the default contract duration for this WebCatItem
     */
    protected ContractDuration defaultContractDuration;
    
    /**
     * represents the ui selected contract duration for this WebCatItem
     */
    protected ContractDuration selectedContractDuration;
    
    /**
     * holds the results of the last atp check
     */
    protected AtpResultList atpResultList = null;
    
    /** Loyalty Management - Points */

    /** 
     * Empty constructor to ensure initialization; not to be used from outside. 
     */
    private WebCatItem() {
        log.entering("WebCatItem()");
        log.exiting();
        // no general initialization by now
    }

    /**
     * Create new WebCatItem from catalog data. <i>Note:</i> Do not use this Constructor directly! 
     * Instead, call
     *
     * @param newCatalog the catalog the data came from
     * @param newCatalogItem the data of the catalog engine for this item
     *
     */
    public WebCatItem(WebCatInfo newCatalog, IItem newCatalogItem) {

        this();
        log.entering("WebCatItem(WebCatInfo newCatalog, IItem newCatalogItem)");
        itemKey = new WebCatItemKey(newCatalog, newCatalogItem);
        catalogItem = newCatalogItem;

        if (log.isDebugEnabled()) {
            log.debug("WebCatItem:WebCatItem(WebCatInfo " + newCatalog + " , IItem " + newCatalogItem + ")");
        }
        log.exiting();
    }

    /**
     * Create WebCatItem from WebCatItemKey. As the itemKey contains the relevant data, the rest 
     * can be rebuild from it. <i>Note:</i> Do not use this Constructor directly! 
     * Instead, call
     *
     * @param itemKey the WebCatItemKey for the item
     */
    WebCatItem(WebCatItemKey itemKey) {
        this();
        log.entering("WebCatItem(WebCatItemKey itemKey)");
        this.itemKey = itemKey;
        log.exiting();
    }

    /**
     * Create WebCatItem from WebCatItemKey and catalog data. Although the WebCatItem can be build 
     * from WebCatItemKey alone, in most cases also the catalog data is at hand; so it saves 
     * performance to use this instead of redetermining from the catalog engine. <i>Note:</i> 
     * Do not use this Constructor directly! Instead, call
     *
     * @param itemKey the WebCatItemKey for the item
     * @param catalogItem the data of the catalog engine for this item
     *
     * @see com.sap.isa.catalog.webcatalog.WebCatInfo#getItem(WebCatItemKey,IItem)
     */
    WebCatItem(WebCatItemKey itemKey, IItem catalogItem) {
        this(itemKey);
        log.entering("WebCatItem(WebCatItemKey itemKey, IItem catalogItem)");
        this.catalogItem = catalogItem;
        log.exiting();
    }

    /**
     * Set the table from shop object into the WebItem object It is used for the pricing related 
     * attributes for exchange products
     *
     * @param condPurpGroup condPurpGroup
     */
    WebCatItem(ResultData condPurpGroup) {
        log.entering("WebCatItem(ResultData condPurpGroup)");
        this.condPurpGroup = condPurpGroup;
        log.exiting();
    }

    /**
     * Clears the scRelvIPCAttr HashMap.
     */
    public void clearScRelvIPCAttr() {
        log.entering("clearScRelvIPCAttr()");

        if (null != scRelvIPCAttr) {
            scRelvIPCAttr.clear();
        }

        log.exiting();
    }

    /**
     * Creates a new WebCatSubItemList for a package of sales components or combined rate plan.
     * The information about the default components is stored on TREX
     * 
     */

    public void createDefSlsCompWebCatSubItemList() {

        String method = "createDefSlsCompWebCatSubItemList()";
        log.entering(method);
        if (log.isDebugEnabled()) {
            log.debug(method + ":" + itemKey.getItemID() + " / " + getProduct());
        }

        String allSubCmp[] = null;

        if (log.isDebugEnabled()) {
            log.debug(method + ": isSalesPackage()=" + isSalesPackage());
            log.debug(method + ": isCombinedRatePlan()=" + isCombinedRatePlan());
        }

        if (isSalesPackage() || isCombinedRatePlan()) {

            // TechKey for main item 
            setTechKey(TechKey.generateKey());

            // Read List of sub components 
            allSubCmp = getAttributeAllValues(AttributeKeyConstants.DEFAULT_SUBCOMP_OF_PCKAGE);

            if (allSubCmp == null || allSubCmp.length == 0) {
                if (log.isDebugEnabled()) {
                    log.debug(method + ": allSubCmp list is null or empty");
                }
                return;
            }
            if (log.isDebugEnabled()) {
                for (int i = 0; i < allSubCmp.length; i++) {
                    log.debug(method + ": allSubCmp[i]=" + allSubCmp[i]);
                }
            }

            webCatSubItemList = new WebCatSubItemList(getWebCatInfo());
            if (webCatSubItemList.populateWithProducts(allSubCmp, getAreaID())) {
                enhanceDefCompWebCatSubItemList(allSubCmp);
            }
            else {
                log.error(method + ": delete webCatSubItemList because atleast one of the items are invalid");
                webCatSubItemList = null;
            }
        }

        log.exiting();
    }

    /**
     * A utility method to encode xml data.
     * The decoding can be done with function module SSFC_BASE64_DECODE in the ABAP backend.
     */
    public static final String encodeB64(String str) {

        String encStr = "";
        char[] base64 =
            {
                'A',
                'B',
                'C',
                'D',
                'E',
                'F',
                'G',
                'H',
                'I',
                'J',
                'K',
                'L',
                'M',
                'N',
                'O',
                'P',
                'Q',
                'R',
                'S',
                'T',
                'U',
                'V',
                'W',
                'X',
                'Y',
                'Z',
                'a',
                'b',
                'c',
                'd',
                'e',
                'f',
                'g',
                'h',
                'i',
                'j',
                'k',
                'l',
                'm',
                'n',
                'o',
                'p',
                'q',
                'r',
                's',
                't',
                'u',
                'v',
                'w',
                'x',
                'y',
                'z',
                '0',
                '1',
                '2',
                '3',
                '4',
                '5',
                '6',
                '7',
                '8',
                '9',
                '+',
                '/' };
        int length = str.length();
        for (int i = 0; i < length; i += 3) {
            encStr += base64[((int) str.charAt(i) >>> 2)];
            if (i + 1 >= length) {
                encStr += "==";
                break;
            }
            encStr += base64[((((int) str.charAt(i) & 0x03) << 4) | (int) str.charAt(i + 1) >>> 4)];
            if (i + 2 >= length) {
                encStr += '=';
                break;
            }
            encStr += base64[((((int) str.charAt(i + 1) & 0x0F) << 2) | (int) str.charAt(i + 2) >>> 6)];
            encStr += base64[((int) str.charAt(i + 2) & 0x3F)];
        }
        return encStr;
    }

    /**
     * Enhances the hierarchy information and pricing relevant attributes for a package of sales components
     * or combined rate plan.
     * 
     * @param allSubCmp is an array of the product id of the sub item
     */

    public void enhanceDefCompWebCatSubItemList(String allSubCmp[]) {

        final String METHOD = "enhanceDefCompWebCatSubItemList()";
        log.entering(METHOD);

        String allHiers[] = null;
        String allIPCAttr[] = null;

        WebCatItemKey lItemKey = null;
        WebCatSubItem subItem = null;
        WebCatSubItem subItemParent = null;
        int parentIdx;

        // Read hierarchy information of sub components
        allHiers = getAttributeAllValues(AttributeKeyConstants.DFL_SUBCOMP_PARENT_IDX);

        // Read pricing relevant information of sub components
        allIPCAttr = getAttributeAllValues(AttributeKeyConstants.DFL_SUBCOMP_PRC_ATTR_VALU);

        int noDeletedItems=0;
        for (int i = 0; i < allSubCmp.length; i++) {
            if (allSubCmp[i] == null || allSubCmp[i].length() == 0) {
                if (log.isDebugEnabled()) {
                    log.debug("No Sub Components found for index " + i);
                }
                continue;
            }
            
            //same as in populateWithProducts from WebCatSubItemList
            if (allSubCmp[i].equals("00000000000000000000000000000000")) {
                if (log.isDebugEnabled()) {
                    log.debug("allSubCmp[" + i + "]== 00000000000000000; ignoring this (it is a group)");
                }
                noDeletedItems++;
                continue;
            }

            if (log.isDebugEnabled()) {
                log.debug(
                    "Sub item: "
                        + allSubCmp[i]
                        + " - Description: "
                        + webCatSubItemList.getItem(i - noDeletedItems).getDescription());
            }
            subItem = (WebCatSubItem) webCatSubItemList.getItem(i-noDeletedItems);

            subItem.setTechKey(TechKey.generateKey());
            subItem.setIsScSelected(true);

            // enhance hierarchy 
            parentIdx = Integer.parseInt(allHiers[i]);
            if (parentIdx == 0) {
                subItem.setParentTechKey(getTechKey());
                subItem.setParentItem(this);
            }
            else {
                subItemParent = (WebCatSubItem) webCatSubItemList.getItem(parentIdx - 1 - noDeletedItems);
                subItem.setParentTechKey(subItemParent.getTechKey());
                subItem.setParentItem((WebCatItem) subItemParent);
            }

            // enhance pricing attributes of sub item
            if (log.isDebugEnabled()) {
                log.debug(
                    "Set price attributes - Attr Name: "
                        + AttributeKeyConstants.PARENT_PRC_PROD
                        + " Value: "
                        + subItem.getParentItem().getProductID());
                log.debug(
                    "Set price attributes - Attr Name: " + AttributeKeyConstants.PROV_PRICE_LEVEL + " Value: " + allIPCAttr[i]);
                log.debug(
                    "Set price attributes - Attr Name: "
                        + AttributeKeyConstants.PAR_PAR_PRC_PROD
                        + " Value: "
                        + subItem.getProductID());
            }

            subItem.putScRelvIPCAttr(AttributeKeyConstants.PARENT_PRC_PROD, subItem.getParentItem().getProductID());
            subItem.putScRelvIPCAttr(AttributeKeyConstants.PROV_PRICE_LEVEL, allIPCAttr[i]);
            subItem.putScRelvIPCAttr(AttributeKeyConstants.PAR_PAR_PRC_PROD, subItem.getProductID());
        }
        log.exiting();
    }

    /**
     * Returns the unique identifier of the item.
     *
     * @return id of the item as String
     */
    public String getItemID() {
        log.entering("getItemID()");
        log.exiting();
        return itemKey.getItemID();
    }

    /**
     * Returns the unique identifier of the item.
     * The method implements ProductBaseData.
     *
     * @return id of the item as String
     */
    public String getItemId() {
        log.entering("getItemId()");
        log.exiting();
        return itemKey.getItemID();
    }

    /**
     * Returns the WebCatItemKey of the item.
     *
     * @return WebCatItemKey of this item
     */
    public WebCatItemKey getItemKey() {
        log.entering("getItemKey()");
        log.exiting();
        return itemKey;
    }

    /**
     * Returns null as parent item of this <code>WebCatItem</code>.
     * The method implements ProductBaseData.
     * 
     * @return parent product of type ProductBaseData 
     */
    public ProductBaseData getParent() {
        String method = "getParent()";
        log.entering(method);
        if (log.isDebugEnabled()) {
            log.debug(method + ": null");
        }
        log.exiting();
        return null;
    }

    /**
     * Returns null as parent item of this <code>WebCatItem</code>.
     * 
     * @return parent item of type WebCatItem
     */
    public WebCatItem getParentItem() {
        String method = "getParentItem()";
        log.entering(method);
        if (log.isDebugEnabled()) {
            log.debug(method + ": null");
        }
        log.exiting();
        return null;
    }

    /**
     * DOCUMENT ME!
     *
     * @param condPurpGroup DOCUMENT ME!
     */
    public void setCondPurpGroup(ResultData condPurpGroup) {
        log.entering("setCondPurpGroup()");
        this.condPurpGroup = condPurpGroup;
        log.exiting();
    }

    /**
     * DOCUMENT ME!
     *
     * @param exchProdData DOCUMENT ME!
     */
    public void setExchProdData(ExchProductResult exchProdData) {
        log.entering("setExchProdData()");
        this.exchProdData = exchProdData;
        log.exiting();
    }

    /**
     * DOCUMENT ME!
     *
     * @return DOCUMENT ME!
     */
    public ExchProductResult getExchProdData() {
        log.entering("getExchProdData()");
        log.exiting();

        return exchProdData;
    }

    /**
     * retrieve the stored object
     *
     * @return condPurpGroup
     */
    public ResultData getCondPurpGroup() {
        log.entering("getCondPurpGroup()");
        log.exiting();
        return condPurpGroup;
    }

    /**
     * Set catalog item data. Sets reference to catalog item. Does NOT set item key as this 
     * method must only be used to re-set reference to catalog item after this was dropped 
     * by "releaseReferences"!
     *
     * @param newCatalogItem catalog item corresponding to this WebCatItem
     */
    public void setCatalogItem(IItem newCatalogItem) {
        log.entering("setCatalogItem()");

        if (!newCatalogItem.getGuid().equals(itemKey.getItemID())) {
            log.fatal("Tried to repopulate WebCatItem with foreign item data");
            return;
        }

        catalogItem = newCatalogItem;

        if (log.isDebugEnabled()) {
            log.debug("setCatalogItem(" + itemKey.getItemID() + ")");
        }
        log.exiting();
    }

    /**
     * Returns catalog item data. Retrieve catalog engine data for this item. If this item was 
     * created only from a WebCatItemKey, calling this method is the easiest way to populate 
     * the catalog data, as the call to the catalog engine will be done automatically in this case.
     *
     * @return catalog item data as IItem
     */
    public IItem getCatalogItem() {
        log.entering("getCatalogItem()");

        if (catalogItem == null) {

            if (log.isDebugEnabled()) {
                log.debug("getCatalogItem(" + itemKey.getItemID() + ")");
            }
            try {
                ICategory category = getWebCatInfo().getCatalog().getCategory(itemKey.getAreaID());

                if (category != null) {
                    catalogItem = category.getItem(itemKey.getItemID());
                }
            }
            catch (CatalogException ex) {
                log.error("Catalog Exception thrown", ex);
                catalogItem = null;
            }
        }

        log.exiting();

        return catalogItem;
    }

    /**
     * Returns the URL for the picture of the product. <br>
     * The method implements ProductBaseData.
     *
     * @return picture
     */
    public String getPicture() {
        String METHOD = "getPicture()";
        log.entering(METHOD);

        String picture = "";

        String pictureFile = getAttribute(AttributeKeyConstants.AN_PICTURE1);
        String imageServer = getItemKey().getParentCatalog().getImageServer();
        if (imageServer == null) {
            imageServer = "";
        }

        String URLPrefix = imageServer + getAttribute(AttributeKeyConstants.URLSeparator);
        if (pictureFile.length() > 0) {
            picture = URLPrefix + pictureFile;
            if (log.isDebugEnabled()) {
                log.debug(METHOD + ": for " + AttributeKeyConstants.AN_PICTURE1 + " : " + picture);
            }
        }

        if (picture.length() == 0) {
            pictureFile = getAttribute(AttributeKeyConstants.AN_PICTURE2);
            if (pictureFile.length() > 0) {
                picture = URLPrefix + pictureFile;
                if (log.isDebugEnabled()) {
                    log.debug(METHOD + ": for " + AttributeKeyConstants.AN_PICTURE2 + " : " + picture);
                }
            }
        }

        if (log.isDebugEnabled()) {
            log.debug("getPicture() = '" + picture + "'");
        }
        log.exiting();

        return (picture);
    }

    /**
     * Returns the URL for the thumbnail of the product. <br>
     * The method implements ProductBaseData.
     *
     * @return thumb
     */
    public String getThumb() {
        String METHOD = "getThumb()";
        log.entering(METHOD);

        String thumb = "";

        String thumbNail = getRelThumbNailPath();
        
        if (thumbNail.length() > 0) {
            
            String imageServer = getItemKey().getParentCatalog().getImageServer();
            if (imageServer == null) {
                imageServer = "";
            }
                
            thumb = imageServer + thumbNail;
        }

        if (log.isDebugEnabled()) {
            log.debug(METHOD + ": thumb='" + thumb + "'");
        }
        log.exiting();

        return (thumb);
    }

    /**
     * get the relative Thumbnail Path
     */
    public String getRelThumbNailPath() {
        
        String thumbNail;
        
        thumbNail = getAttribute(AttributeKeyConstants.AN_THUMBNAIL1);
        
        if (thumbNail.length() > 0) {
            if (log.isDebugEnabled()) {
                log.debug("getRelThumbNailPath : for " + AttributeKeyConstants.AN_THUMBNAIL1 + " : " + thumbNail);
            }
        } else if (thumbNail.length() == 0) {
            thumbNail = getAttribute(AttributeKeyConstants.AN_THUMBNAIL2);
            if (thumbNail.length() > 0) {
                if (log.isDebugEnabled()) {
                    log.debug("getRelThumbNailPath : for " + AttributeKeyConstants.AN_THUMBNAIL2 + " : " + thumbNail);
                }
            }
            else if (thumbNail.length() == 0) {
                 thumbNail = getAttribute(AttributeKeyConstants.AN_THUMBNAIL3);
                 if (thumbNail.length() > 0) {
                     if (log.isDebugEnabled()) {
                         log.debug("getRelThumbNailPath : for " + AttributeKeyConstants.AN_THUMBNAIL3 + " : " + thumbNail);
                     }
                 }
            }
        } 
        
        if (thumbNail.length() > 0) {
            thumbNail = getAttribute(AttributeKeyConstants.URLSeparator) + thumbNail;
        }

        return thumbNail;
    }

    /**
     * Forces re-read of catalog item. This is useful to switch from the item without area 
     * specific attributes, as they are returned by a search, to the item including area specific 
     * data. Implementation is setting catalogItem property to null and calling
     *
     * @see #getCatalogItem()
     */
    public void refreshCatalogItem() {
        log.entering("refreshCatalogItem()");
                
        //if getCatalogItem returns null (catalog with views -- product with no area), we don't remove the catalogItem.        
        IItem tempCatalogItem = getCatalogItem();
        if(tempCatalogItem != null) {
            catalogItem = tempCatalogItem;
        
        //also refresh contract subitems from this...
            if (getContractItemsArray() != null) {
              log.debug("Also refresh contract items.");
              Iterator iter = getContractItemsArray().iterator();
              while (iter.hasNext()) {
                ((WebCatItem) iter.next()).setCatalogItem(catalogItem);
              }
            }
        }

        log.exiting();
    }

    /**
     * Set contract reference for item list view. Sets reference to the contract data of this item. The reference
     * includes the contract data to be shown in a list view of the item. For contract data to be
     * shown in item detail view, see setContractDetailedRef
     *
     * @param contractRef contract reference including data to be shown on item list view
     *
     * @see #setContractDetailedRef
     */
    public void setContractRef(ContractReference contractRef) {
        log.entering("setContractRef()");
        this.contractRef = contractRef;
        log.exiting();
    }

    /**
     * Get contract reference for item list view. Gets the contract reference of this item, 
     * containing contract data to be shown in a list view of the item. For contract data to be 
     * shown in the item detail view, {@link #getContractDetailedRef }
     *
     * @return contract reference including data for list view
     *
     */
    public ContractReference getContractRef() {
        log.entering("getContractRef()");
        log.exiting();
        return contractRef;
    }

    /**
     * Set contract reference for item detail view. Sets reference to the contract data of this item. The reference
     * includes the contract data to be shown in a detail view of the item. For contract data to be shown in item list
     * view, see <code>setContractRef</code>
     *
     * @param contractDetailedRef contract reference including data to be shown on item detail view
     *
     * @see #setContractRef
     */
    public void setContractDetailedRef(ContractReference contractDetailedRef) {
        log.entering("setContractDetailedRef()");
        this.contractDetailedRef = contractDetailedRef;
        log.exiting();
    }
    
    /**
     * Returns true if the item is a contract sub item, that means 
     * has any kind of contract reference data.
     *
     * @return true if the item is a contract sub item, 
     *         false else
     */
    public boolean isContractSubItem() {
        log.entering("isContractSubItem()");
        
        boolean isContractSubItem = contractDetailedRef != null || contractRef != null;
        
        if (log.isDebugEnabled()) {
            log.debug("Result isContractSubItem() = " + isContractSubItem);
        }
        
        log.exiting();
        
        return isContractSubItem;
    }  

    /**
     * Get contract reference for item detail view. Gets the contract reference of this item, containing contract data
     * to be shown in a detail view of the item. For contract data to be shown in the item list view, see
     * <code>getContractRef</code>
     *
     * @return contract reference including data for list view
     *
     * @see #getContractRef
     */
    public ContractReference getContractDetailedRef() {
        log.entering("getContractDetailedRef()");
        log.exiting();
        return contractDetailedRef;
    }

    /**
     * Adds contract data for item detail view. This method adds data to the contract subitems 
     * created by <code>setContractItems</code>, it does not create the subitems itself.
     *
     * @param contractRefs list of contract reference data for item detail view
     */
    public void setContractDetailedItems(ContractReferenceListData contractRefs) {
        log.entering("setContractDetailedItems()");

        if (contractItems == null) {
            log.debug("Contract items are null");
            log.exiting();
            return;
        }

        if (contractRefs != null) {
            if (log.isDebugEnabled()) {
                log.debug(
                    "Item "
                        + this.getProduct()
                        + "/"
                        + this.getProductID()
                        + ", add "
                        + contractRefs.size()
                        + " Contract References");
            }

            Iterator contractRefIterator = contractRefs.iterator();
            while (contractRefIterator.hasNext()) {

                ContractReference contractRef = (ContractReference) contractRefIterator.next();
                WebCatItem subItem =
                    (WebCatItem) contractItems.get(contractRef.getContractKey().toString() + contractRef.getItemKey().toString());
                subItem.setContractDetailedRef(contractRef);

                if (log.isDebugEnabled()) {
                    log.debug("merged Contract Reference" + contractRef.getContractId() + "/" + contractRef.getItemID());
                }
            }
        }
        else {
            if (log.isDebugEnabled()) {
                log.debug("Item " + this.getItemID() + ", no Contract References");
            }
        }

        log.exiting();
    }

    /**
     * Creates contract related subitems. If for a product of the catalog, contract references 
     * are found, for each of these references a subitem is created. The subitems are regular 
     * <code>WebCatItems</code>, additionally they have contract reference data. The main item 
     * keeps references to these subitems.
     * 
     * <p>
     * This method has a List of contract references as parameter and creates a new WebCatItem 
     * for each.</p>
     *
     * @param contractRefs list of contract references for the product of this WebCatItem
     */
    public void setContractItems(ContractReferenceListData contractRefs) {
        log.entering("setContractItems()");

        contractItems = new HashMap();
        contractItemsArray = new ArrayList();

        if (contractRefs != null) {
            if (log.isDebugEnabled()) {
                log.debug(
                    "Item "
                        + this.getProduct()
                        + "/"
                        + this.getProductID()
                        + ", add "
                        + contractRefs.size()
                        + " Contract References");
            }

            Iterator contractRefIterator = contractRefs.iterator();

            while (contractRefIterator.hasNext()) {
                ContractReference contractRef = (ContractReference) contractRefIterator.next();
                WebCatItemKey newItemKey = new WebCatItemKey(getWebCatInfo(), catalogItem);
                WebCatItem subItem = getWebCatInfo().getItem(newItemKey, catalogItem);
                subItem.setContractRef(contractRef);

                if (contractItems.get(contractRef.getContractKey().toString()+ contractRef.getItemKey().toString()) == null) {
                    contractItems.put(contractRef.getContractKey().toString() + contractRef.getItemKey().toString(), subItem);
                    contractItemsArray.add(subItem);
                }

                if (log.isDebugEnabled()) {
                    log.debug("added Contract Reference " + contractRef.getContractId() + "/" + contractRef.getItemID());
                }
            }
        }
        else {
            if (log.isDebugEnabled()) {
                log.debug("Item " + this.getItemID() + ", no Contract References");
            }
        }

        log.exiting();
    }

    /**
     * Checks whether contract reference data is available. Note: This does not trigger contract 
     * data determination.
     *
     * @return true if contract data is available, false otherwise
     */
    public boolean idContractRefs() {
        log.entering("hasContractRefs()");

        boolean hasContractRefs = (contractItems != null && contractItems.size() > 0);

        if (log.isDebugEnabled()) {
            log.debug("hasContractRefs()=" + hasContractRefs);
        }
        log.exiting();
        return hasContractRefs;
    }

    /**
     * Returns the subitem for a given contract.
     *
     * @param contractKey unique identifier for a contract
     * @param contractItemKey unique identifier for a contract item
     *
     * @return the subitem related to the given contract data as WebCatItem if available; this 
     *         WebCatItem, if contract data equals to empty String; null otherwise
     */
    public WebCatItem getContractItem(String contractKey, String contractItemKey) {
        log.entering("getContractItem()");

        WebCatItem contrItem = null;

        if (contractKey == null && contractItemKey == null) {
            log.debug("No Contract keys, return this item");
            contrItem = this;
        }
        else if (contractKey.equals("") && contractItemKey.equals("")) {
            log.debug("Contract keys empty, return this item");
            contrItem = this;
        }
        else if (contractItems == null || contractItems.values().size() == 0) {
            log.debug("No Contract items, return null");
        }
        else if (contractItems.get(contractKey + contractItemKey) != null) {
            if (log.isDebugEnabled()) {
                log.debug("Contract " + contractKey + ", item " + contractItemKey + " selected");
            }
            contrItem = (WebCatItem) contractItems.get(contractKey + contractItemKey);
        }
        else {
            log.debug("unknown error, return null");
        }

        log.exiting();

        return contrItem;
    }

    /**
     * Returns a Collection of contract related WebCatItems.
     *
     * @return all contract related subitems of this item, bundled as Collection
     */
    public Collection getContractItems() {
        log.entering("getContractItems()");

        if (contractItems == null) {
            log.debug("contractItems is null");
            log.exiting();
            return null;
        }

        log.exiting();

        return contractItems.values();
    }

    /**
     * Returns the array of the contract items.
     *
     * @return the array of contract items.
     */
    public ArrayList getContractItemsArray() {
        log.entering("getContractItemsArray()");
        log.exiting();
        return contractItemsArray;
    }

    /**
     * Returns accessories for this item. The accessories are determined from the catalog, that 
     * is only those products will be shown, that are defined and published as accessories in CRM.
     *
     * @return accessories as WebCatItems, bundled in an ArrayList
     */
    public ArrayList getAccesories() {

        String method = "getAccesories()";
        log.entering(method);

        CatalogFilterFactory fact = CatalogFilterFactory.getInstance();

        // "ITEM_GUID" is CRM specific; abort if not available
        if (getCatalogItem().getAttributeValue("ITEM_GUID") == null) {
            if (log.isDebugEnabled()) {
                log.debug(method + ": ITEM_GUID is null");
            }
            log.exiting();
            return null;
        }

        // Test if item has accessories
        if (!getAttribute(AttributeKeyConstants.IS_ITEM_WITH_ACC).equals("X")) {
            if (log.isDebugEnabled()) {
                log.debug(method + ": Item hasn't any accessories; IS_ITEM_WITH_ACC = space");
            }
            log.exiting();
            return null;
        }
        
        IFilter filter = fact.createAttrEqualValue("MAIN_PRODUCTS", getCatalogItem().getAttributeValue("ITEM_GUID").getAsString());
        IFilter filter2 = fact.createAttrEqualValue("AREA_GUID", this.getAreaID());
        filter = fact.createAnd(filter, filter2);
        filter2 = fact.createAttrEqualValue(AttributeKeyConstants.IS_SUBCOMP_OF_ACCESS, "X");
        filter = fact.createAnd(filter, filter2);

        ICatalog catalog = getWebCatInfo().getCatalog();
        Iterator itemListIterator;
        ArrayList resultingList;

        try {
            IQueryStatement queryStmt = catalog.createQueryStatement();
            queryStmt.setStatement(filter, null, null);
            // attributes, sort order?

            IQuery query = catalog.createQuery(queryStmt);

            // create WebCatItemList from this query and return
            resultingList = new ArrayList();

            //    ArrayList tmpList = new ArrayList(); //to be deleted after Wolfgang will change, and 2 more other lines
            itemListIterator = query.submit();
        }
        catch (CatalogException e) {
            log.error("catalog.exception.backend", e);
            log.exiting();

            return null;
        }

        log.debug("Adding Items to result list");

        while (itemListIterator.hasNext()) {
            IItem theItem = (IItem) itemListIterator.next();
            WebCatItem newItem = new WebCatItem(getWebCatInfo(), theItem);
            resultingList.add(newItem);
            if (log.isDebugEnabled()) {
                log.debug("Item " + newItem.getProduct() + " added");
            }
        }

        WebCatItemComparator comparator = new WebCatItemComparator("POS_NR");
        java.util.Collections.sort(resultingList, comparator);

        log.exiting();

        return resultingList;
    }

    /**
     * Sets reference to availability information.
     *
     * @param availability reference to object containing availability information
     */
    public void setAvailability(Object availability) {
        log.entering("setAvailability()");
        this.availability = availability;
        log.exiting();
    }

    /**
     * Returns availibility information
     *
     * @return availability information
     */
    public Object getAvailability() {
        log.entering("getAvailability()");
        log.exiting();
        return availability;
    }

    /**
     * Returns ID of catalog area.
     *
     * @return unique identifier of the catalog area (category) this item belongs to, as String
     */
    public String getAreaID() {
        log.entering("getAreaID()");
        log.exiting();
        return itemKey.getAreaID();
    }

    /**
     * Returns language dependent Name of catalog area.
     *
     * @return identifier of the area which this item belongs to; usually a speaking abbrevation, 
     *         in opposite to <code>getAreaID</code>
     */
    public String getArea() {
        log.entering("getArea()");
        String area = null;
        try {
            if (itemKey != null
                && itemKey.getParentCatalog() != null
                && itemKey.getParentCatalog().getCatalog() != null
                && itemKey.getParentCatalog().getCatalog().getCategory(getAreaID()) != null
                && itemKey.getParentCatalog().getCatalog().getCategory(getAreaID()).getName() != null) {
                // you should get the right information here...sometimes it happend that with the code
                // in else part returns the search string after a product serach instead the area name 
                // so let try to use this one.
                area = itemKey.getParentCatalog().getCatalog().getCategory(getAreaID()).getName();
                if (log.isDebugEnabled()) {
                    log.debug("WebCatItem.getArea()="+area);
                }
            }
            else {
                area = getCatalogItem().getParent().getName();
                log.warn("getArea(): getCatalogItem().getParent().getName() ="+area);
            }
        } 
        catch (Exception e) {
            log.error("catalog.exception.backend",e);
            area = null;
        }
        log.exiting();
        return area;
    }

   /**
    * Returns language independent Name of catalog area.
    * @return identifier of the area which this item belongs to
    */
   public String getAreaLangIndepName() {
       log.entering("getAreaLangIndepName()");
       String area = null;
       try {
            if (itemKey != null
                && itemKey.getParentCatalog() != null
                && itemKey.getParentCatalog().getCatalog() != null
                && itemKey.getParentCatalog().getCatalog().getCategory(getAreaID()) != null
                && itemKey.getParentCatalog().getCatalog().getCategory(getAreaID()).getDetail("AREA") != null
                && itemKey.getParentCatalog().getCatalog().getCategory(getAreaID()).getDetail("AREA").getAsString() != null) {
               area = itemKey.getParentCatalog().getCatalog().getCategory(getAreaID()).getDetail("AREA").getAsString();
           }
           else {
               area = getCatalogItem().getParent().getName();
               log.warn("WebCatItem.getAreaLangIndepName()...the methode returns the language dependend area name!!!");
           }
       }
       catch (Exception e) {
           log.error("catalog.exception.backend",e);
           area = null;
       }
       if (log.isDebugEnabled()) {
           log.debug("WebCatItem.getAreaLangIndepName()="+area);
       }
       return area;
   }
    
    /**
     * Returns the name of the parent area.
     *
     * @return the name of the parent area.
     */
    public String getParentAreaName() {
        log.entering("getParentAreaName()");

        String parentAreaName = null;

        try {
            parentAreaName = getWebCatInfo().getCatalog().getCategory(getAreaID()).getName();
        }
        catch (CatalogException e) {
            log.error("catalog.exception.backend", e);
        }

        if (log.isDebugEnabled()) {
            log.debug("parentAreaName = " + parentAreaName);
        }
        log.exiting();

        return parentAreaName;
    }

    /**
     * Returns ID of catalog.
     *
     * @return unique identifier of the catalog this item belongs to, as String
     */
    public String getCatalogID() {
        log.entering("getCatalogID()");
        log.exiting();
        return getWebCatInfo().getCatalogServer().getCatalogGuid();
    }

    /**
     * Returns id of the product.
     *
     * @return identifier of the product which is included in this item; usually a speaking abbrevation or some product
     *         number, in opposite to <code>getProductID</code>
     */
    public String getId() {
        log.entering("getId()");
        log.exiting();
        return getAttributeByKey(AttributeKeyConstants.PRODUCT);
    }

    /**
     * Returns the items WebCatInfo object.
     *
     * @return the WebCatInfo Object, the item belongs to
     */
    public WebCatInfo getWebCatInfo() {
        log.entering("getWebCatInfo()");
        log.exiting();
        return itemKey.getParentCatalog();
    }

    /**
     * Returns the items WebCatInfoData object.
     *
     * @return the WebCatInfoData Object, the item belongs to
     */
    public WebCatInfoData getWebCatInfoData() {
        log.entering("getWebCatInfoData()");
        log.exiting();
        return itemKey.getParentCatalog();
    }

    /**
     * Returns ID of product.
     *
     * @return unique identifier of the product which is included in this item
     */
    public String getProductID() {
        log.entering("getProductID()");

        String productId = getAttributeByKey(AttributeKeyConstants.PRODUCT_ID);
        if ((productId == null || productId.length() == 0) && getCatalogItem() != null) {

            if (log.isDebugEnabled()) {
                log.debug(
                    "While attempting to getProductID(), with key: "
                        + AttributeKeyConstants.PRODUCT_ID
                        + " I got no relevant answer, so, I'll return getProductGuid(): "
                        + getCatalogItem().getProductGuid());
            }

            productId = getCatalogItem().getProductGuid();
        }

        log.exiting();

        return productId;
    }
    
    /**
     * Returns WebCatItemPrice object if present
     * else null
     *
     * @return WebCatItemPrice object if present
     *         else null
     */
    public WebCatItemPrice getProductPrice() {
        log.entering("getProductPrice()");
        log.exiting();
        return getItemPrice();
    }

    /**
     * Returns name of product.
     *
     * @return identifier of the product which is included in this item; usually a speaking abbrevation or some product
     *         number, in opposite to <code>getProductID</code>
     */
    public String getProduct() {
        log.entering("getProduct()");
        log.exiting();
        return getAttributeByKey(AttributeKeyConstants.PRODUCT);
    }

    /**
     * Returns the product category.
     *
     * @return the product category.
     */
    public String getProductCategory() {
        log.entering("getProductCategory()");
        log.exiting();
        return getAttributeByKey(AttributeKeyConstants.PRODUCT_CATEGORY);
    }

    /**
     * Returns product description.
     * If the product description is empty the product id is returned.
     *
     * @return description from catalog, as String
     */
    public String getDescription() {
        log.entering("getDescription()");
        log.exiting();
        String descr = "";
        descr = getAttributeByKey(AttributeKeyConstants.PRODUCT_DESCRIPTION);
        
        if (descr == null || descr.length() == 0) {
            descr = getId();
    }
        return descr;
    }

    /**
     * Sets the quantity.
     *
     * @param quantity 
     */
    public void setQuantity(String quantity) {
        log.entering("setQuantity()");
        itemKey.setQuantity(quantity);
        log.exiting();
    }
    
    /**
     * Sets the quantity.
     * if the quantity is 0 or hgjds, the quantity will be set. This is not the case in the function setQuantity.
     *
     * @param quantity 
     */
    public void setQuantityWithoutCheck(String quantity){
        log.entering("setQuantityWithoutCheck()");
        itemKey.setQuantityWithoutCheck(quantity);
        log.exiting();
    }
    
    /**
     * Returns quantity. Quantity is not a catalog data, but a property used for the web 
     * visualization to be able to select the quantity before adding products to a shopping basket, 
     * order etc. This quantity is only valid for the lifetime of the itemKey and is not saved
     * anywhere; it should be used when adding this item to some basket etc.
     *
     * @return quantity of this item, as String
     */
    public String getQuantity() {
        log.entering("getQuantity()");
        log.exiting();
        return itemKey.getQuantity();
    }

    /**
	 * Returns quantity associated to the given contract item key.
	 *
	 * @return quantity of this contract item, as String
	 */
	public String getQuantity(String contractItemKey) {
		log.entering("getQuantity(String contractItemKey)");
		String returnedQuantity = null;
		if(contractItemKey != null && !contractItemKey.equals("")){
			Iterator it = getContractItems().iterator();
			WebCatItem contractItem = null;
			if(it != null){
				while(it.hasNext()){
					contractItem = (WebCatItem)it.next();
					if(contractItem.getContractRef().getItemKey().toString().equals(contractItemKey)){
						 returnedQuantity = contractItem.getItemKey().getQuantity();
						 break;
					}
				}
			}
		}
		if(returnedQuantity == null){
			returnedQuantity = getQuantity();
		}
		log.debug("returnedQuantity: "+returnedQuantity);
		log.exiting();
		return returnedQuantity;
	}

    /**
     * Returns quantity. Quantity is not a catalog data, but a property used for the web 
     * visualization to be able to select the quantity before adding products to a shopping basket, 
     * order etc. This quantity is only valid for the lifetime of the itemKey and is not saved
     * anywhere; it should be used when adding this item to some basket etc.
     * The method implements ProductBaseData.
     *
     * @return quantity of this item, as String
     */
    public String getQuantityAsStr() {
        log.entering("getQuantityAsStr()");
        log.exiting();
        return itemKey.getQuantity();
    }

    /**
     * Returns unit of measurement
     * The method implements ProductBaseData.
     *
     * @return unit of measurement, as String
     */
    public String getUnit() {

        log.entering("getUnit()");

        if ((unit == null || unit.equalsIgnoreCase((String) "") || unit.equalsIgnoreCase((String) " "))
            && getUnitsOfMeasurement().length != 0) {
            log.debug("Return first unit from list");
            log.exiting();

            return getUnitsOfMeasurement()[0];
        }
        
        if (unit == null) {
            log.debug("Unit was set to empty String, because it was null");
            unit = "";
        }

        log.exiting();
        return unit;
    }

    /**
     * get flag whether this item is selected via checkbox.
     *
     * @return selected boolean value, represented as String as it needs to be checked for web 
     *         display
     */
    public String isSelectedStr() {
        log.entering("isSelectedStr()");

        String retVal = "";

        if (itemKey.isSelected()) {
        	// W3C conform attribute.
            retVal = "checked=\"checked\"";
        }

        if (log.isDebugEnabled()) {
            log.debug("isSelectedStr=" + retVal);
        }
        log.exiting();

        return retVal;
    }

    /**
     * Adds the given key-value pair to the scRelvIPCAttr HashMap.<br>
     * 
     * The HashMap is created, if it does not exist before.
     * 
     * @param key to be inserted
     * @param value to be inserted
     */
    public void putScRelvIPCAttr(Object key, Object value) {
        log.entering("putScRelvIPCAttr()");

        if (null == scRelvIPCAttr) {
            scRelvIPCAttr = new HashMap();
        }

        scRelvIPCAttr.put(key, value);
        if (log.isDebugEnabled()) {
            log.debug("key: " + key + " value: " + value);
        }

        log.exiting();
    }

    /**
     * Changes selection mark. Selection mark is used for processes like comparison of items or
     * adding several items to a basket in one step.
     *
     * @param selected boolean value representing the new state of the selection mark
     */
    public void setSelected(boolean selected) {
        log.entering("setSelected()");
        itemKey.setSelected(selected);
        log.exiting();
    }

    /**
     * Returns value of selection mark.
     * The method implements ProductBaseData.
     *
     * @return true if this item is selected; false otherwise
     */
    public boolean isSelected() {
        String method = "isSelected()";
        log.entering(method);
        boolean retVal = itemKey.isSelected();
        if (log.isDebugEnabled()) {
            log.debug(method + ": isSelected=" + retVal);
        }
        log.exiting();
        return retVal;
    }

    /**
     * Returns the value of a catalog attribute.
     *
     * @param attrName name of the requested catalog attribute
     *
     * @return value of the requested attribute; empty String (not null!) if not available
     */
    public String getAttribute(String attrName) {

        log.entering("getAttribute()");

        if (log.isDebugEnabled()) {
            log.debug("WebCatItem:getAttribute(String " + attrName + ")");
        }

        if (catalogItem == null) {
            log.debug("Catalog item is null");
            log.exiting();
            return "";
        }

        IAttributeValue attr = catalogItem.getAttributeValue(attrName);

        if (attr == null) {
            log.debug("attr is null");
            log.exiting();
            return ""; // return empty String instead of null for web use
        }

        if (log.isDebugEnabled()) {
            log.debug("attribute value=" + attr.getAsString());
        }
        log.exiting();
        return attr.getAsString();
    }

    /**
     * Returns the value of a catalog attribute, determined using attribute key mapping.
     *
     * @param attrKey key of the requested catalog attribute, which is mapped to the 
     *        corresponding attribute name, and for that the value is determined using
     *        <code>getAttribute</code>
     *
     * @return value of the requested attribute; empty String (not null!) if not available
     */
    public String getAttributeByKey(String attrKey) {

        log.entering("getAttributeByKey()");

        String attrName = getWebCatInfo().getCatalog().getAttributeGuid(attrKey);
        String attrVal = "";

        if (attrName == null || attrName.trim().equals("")) {

            if (log.isDebugEnabled()) {
                log.debug("catalog.message.attrMap.notDef; Key is not defined; key= " + attrKey + " ; We return an empty string");
            }
            log.exiting();
            return "";
        }

        attrVal = getAttribute(attrName);

        if (log.isDebugEnabled()) {
            log.debug("getAttributeByKey: key=" + attrKey + " name=" + attrName + " value=" + attrVal);
        }
        log.exiting();

        return attrVal;
    }

    /**
     * Returns all values of a catalog attribute.
     *
     * @param attrName name of the requested catalog attribute
     *
     * @return values of the requested attribute as array of String; empty array (not null!) 
     *         if not available
     */
    public String[] getAttributeAllValues(String attrName) {

        log.entering("getAttributeAllValues()");

        if (log.isDebugEnabled()) {
            log.debug("WebCatItem:getAttributeAllValues(String " + attrName + ")");
        }

        IAttributeValue attr = catalogItem.getAttributeValue(attrName);

        if (attr == null) {
            log.debug("attr is null");
            log.exiting();
            return new String[0];
        }

        Iterator iter = attr.getAllAsString();
        ArrayList values = new ArrayList();

        log.debug("Start adding values.");

        while (iter.hasNext()) {
            String str = (String) iter.next();
            values.add(str);
            if (log.isDebugEnabled()) {
                log.debug("Value=" + str + " added");
            }
        }

        log.exiting();

        return (String[]) values.toArray(new String[values.size()]);
    }

    /**
     * Method for basket. Returns the HashMap for all the IPC pricing related attributes
     *
     * @return HashMap
     */
    public HashMap getIPCPricingAttributes() {

        log.entering("getIPCPricingAttributes()");

        HashMap attr = new HashMap();
        StringTokenizer st = null;
        String attrName = null;
        String attrValue = null;
        String allAttrs[] = getAttributeAllValues(AttributeKeyConstants.IPC_PRICE_ATTR);

        if (log.isDebugEnabled()) {
            log.debug("Start adding price attributes length: " + allAttrs.length);
        }

        for (int i = 0; i < allAttrs.length; i++) {
            st = new StringTokenizer(allAttrs[i], "=");

            if (st != null && st.hasMoreTokens()) {
                attrName = st.nextToken();

                if (st.hasMoreTokens()) {
                    attrValue = st.nextToken();
                }
                else {
                    attrValue = "";
                }

                attr.put(attrName.toUpperCase(), attrValue);
                if (log.isDebugEnabled()) {
                    log.debug("Adding price attribute: " + attrName.toUpperCase() + " with value: " + attrValue);
                }
            }
        }

        log.exiting();

        return attr;
    }

    /**
     * Returns all attribute values for a given attribute.
     *
     * @param attrKey the attribute, the values are requested for.
     *
     * @return string array of the values for the attribute
     */
    public String[] getAttributeAllValuesByKey(String attrKey) {

        log.entering("getAttributeAllValuesByKey()");

        String attrName = getWebCatInfo().getCatalog().getAttributeGuid(attrKey);

        if (log.isDebugEnabled()) {
            log.debug("WebCatItem:getAttributeAllValues(String " + attrName + ")");
        }
        
        if(catalogItem != null) {
            IAttributeValue attr = catalogItem.getAttributeValue(attrName);

            if (attr == null) {
                log.debug("AttrName is null");
                log.exiting();
                return new String[0];
            }

            Iterator iter = attr.getAllAsString();
            ArrayList values = new ArrayList();
            
            log.debug("Start adding values");
            
            while (iter.hasNext()) {
                String str = (String) iter.next();
                values.add(str);
                if (log.isDebugEnabled()) {
                    log.debug("Value=" + str + " added");
                }
            }
            log.exiting();
            return (String[]) values.toArray(new String[values.size()]);
         }
         else {
            log.debug("catalogItem is null");
            log.exiting();
         }
        
        return new String[] {};
    }

    /**
     * Returns Iterator over the attributes of this item.
     *
     * @return catalog attributes of this item, as Iterator;
     */
    public Iterator getCatalogItemAttributes() {
        log.entering("getCatalogItemAttributes()");
        log.exiting();
        return getCatalogItem().getAttributeValues();
    }

    /**
     * Returns the attribute values of a catalog item for a given comparator.
     *
     * @param aComparator to be used in the query for the attributes.
     *
     * @return DOCUMENT ME!
     */
    public Iterator getCatalogItemAttributes(Comparator aComparator) {
        log.entering("getCatalogItemAttributes()");
        log.exiting();
        return getCatalogItem().getAttributeValues(aComparator);
    }
    
    /**
     * This methods returns the category ids of the WebCatItem if present,
     * otherwise a String array of length 0 is returned.
     */
    public String[] getCategoryIds() {
        log.entering("getCategoryIds()");
        log.exiting();
        return getAttributeAllValuesByKey(AttributeKeyConstants.CATEGORY_ID);
    }
    
    /**
     * This methods returns the category description of the WebCatItem if present,
     * otherwise a String array of length 0 is returned.
     * AN unknown desription will be indetified by -
     */
    public String[] getCategoryDescriptions() {
        log.entering("getCategoryDescriptions()");
        log.exiting();
        return getAttributeAllValuesByKey(AttributeKeyConstants.CATEGORY_DESCR);
    }

    /**
     * equals: two items are equal if they have the same ID
     *
     * @param newItem WebCatItem to compare with
     *
     * @return true if itemID of this and newItem are the same (ignoring case) false otherwise
     */
    public boolean equals(WebCatItem newItem) {
        log.entering("equals(WebCatItem newItem)");
        log.exiting();
        return getItemID().equalsIgnoreCase(newItem.getItemID());
    }

    /**
     * equals: two items are equal if they have the same ID
     *
     * @param obj String containing itemID to compare with
     *
     * @return true if itemID of this and newItemID are the same (ignoring case) false otherwise
     */
    public boolean equals(Object obj) {

        log.entering("equals(Object obj)");

        WebCatItem newItem = null;
        String newItemID = null;

        if (obj instanceof WebCatItem) {
            if (log.isDebugEnabled()) {
                log.debug("Compare with WebcatItem " + obj);
            }
            newItem = (WebCatItem) obj;
            log.exiting();
            return this.equals(newItem);
        }

        if (obj instanceof String) {
            newItemID = (String) obj;
            if (this.getItemID().equals(newItemID)) {
                log.exiting();
                return true;
            }
        }

        log.exiting();

        return false;
    }
    
    /**
     * Executes the explosion process for the WebCatItem.<br>
     * 
     * If the item is not relevant for explosion, nothing will be done.<br>
     * 
     * If the item can be exploded, it will be checked if there is already a solutionConfigurator 
     * assigned to it. If not the catalog will be asked, to create one.<br>
     * 
     * Than the explode() method of the SolutionConfigurator will be called. 
     * The same return values as for the SolutionConfigurator explode() method will be returned.<br>
     *
     * @param boolean storeScDoc boolean flag, to indicate, if the sc document used to execute the explosion 
     *                          should be kept in the backend or not. The document must be kept, if the explosion 
     *                          tree should be copied by refrence, e.g. when the main item is added to the basket.
     * 
     * @return SolutionConfiguratorData.NOT_ELIGIBLE     if item is not relevant for explosion<br>
     *         SolutionConfiguratorData.RESOLVED         if the explosion was successfull<br>
     *         SolutionConfiguratorData.RESOLUTION_ERROR if the explosion has failed<br>
     */
    public int explode(boolean storeScDoc) throws CommunicationException {

        log.entering("explode(boolean storeScDoc)");

        if (log.isDebugEnabled()) {
            log.debug("explode called for item " + getProduct() + "  - storeScDoc = " + storeScDoc);
        }

        // Check, if the item is relevant for explosion
        if (!isRelevantForExplosion()) {
            log.debug("item not relevatn for explosion");
            return SolutionConfiguratorData.NOT_ELIGIBLE;
        }

        // Create a solution configurator, if not yet done.
        if (null == solutionConfigurator) {
            log.debug("Create Solution Configurator at first");
            getWebCatInfo().createSolutionConfigurator(this);
        }

        // Call the explode method of the solution configurator
        int retVal = solutionConfigurator.explode(storeScDoc);

        if (log.isDebugEnabled()) {
            log.debug("Explosion returned: " + retVal);
        }

        log.exiting();

        return retVal;
    }

    /**
     * Executes the explosion process for the WebCatItem.<br>
     * 
     * If the item is not relevant for explosion, nothing will be done.<br>
     * 
     * If the item can be exploded, it will be checked if there is already a solutionConfigurator 
     * assigned to it. If not the catalog will be asked, to create one.<br>
     * 
     * Than the explode() method of the SolutionConfigurator will be called. 
     * The same return values as for the SolutionConfigurator explode() method will be returned.<br>
     * 
     * @return SolutionConfiguratorData.NOT_ELIGIBLE     if item is not relevant for explosion<br>
     *         SolutionConfiguratorData.RESOLVED         if the explosion was successfull<br>
     *         SolutionConfiguratorData.RESOLUTION_ERROR if the explosion has failed<br>
     */
    public int explode() throws CommunicationException {

        log.entering("explode()");

        log.exiting();

        return explode(false);
    }

    /**
     * Hashcode is overwritten and returns the hashcode of the itemId
     *
     * @return Hash code.
     */
    public int hashCode() {
        log.entering("hashCode()");
        log.exiting();
        return getItemID().hashCode();
    }
    
    /**
     * Returns true, if the item has a configurable sub item
     *
     * @return true, if the item has a configurable sub item.
     */
    public boolean hashConfigurableSubItem() {
        log.entering("hashConfigurableSubItem()");
        
        boolean retVal = false;
        
        if (getWebCatSubItemList() != null) { 
            for (int i = 1; i < getWebCatSubItemList().size(); i++) {
                if (getWebCatSubItemList().getItem(i).isConfigurable()) {
                    retVal = true;
                    break;
                }
            }
        }
        
        if (log.isDebugEnabled()) {
            log.debug("hashConfigurableSubItem()=" + retVal);
        }
        
        log.exiting();
        return retVal;
    }

    /**
     * Returns true, if the item has  subitems
     * 
     * @param true if the WebCatSubItemList is not empty
     * 
     */
    public boolean hasSubItems() {
        log.entering("hasSubItems()");
        
        boolean retVal = false;
        
        if (getWebCatSubItemList() != null && getWebCatSubItemList().size() > 0) {
            retVal = true;
        }
        
        log.exiting();
        return retVal;
        
    }
    /**
     * Set reference to a configuration. Configuration is not used by web catalog itself, 
     * but might be important for other processes. Therefore a reference to a configuration 
     * can be kept with the item.
     *
     * @param configItemReference reference to the configuration for this item
     */
    public void setConfigItemReference(Object configItemReference) {
        log.entering("setConfigItemReference()");
        
        if (this.configItemReference != null && !this.configItemReference.equals(configItemReference)) {
            if (log.isDebugEnabled()) {
                log.debug("Delete old ConfigItemreference for -  " + this);
                log.debug("Old ConfigItemreference -  " + this.configItemReference);
            }
            this.configItemReference.getDocument().removeItem(this.configItemReference);
        }

        if (log.isDebugEnabled()) {
            log.debug("set config item reference for " + this);
            log.debug("config item reference " + getConfigItemReference());
        }

        this.configItemReference = (IPCItem) configItemReference;

        log.exiting();
    }

    /**
     * Returns reference to configuration (if available). This does not determine a new reference 
     * if none available! As currently configuration is done through IPC that also does pricing, 
     * call <code>readItemPrice()</code> to determine price and configuration, afterwards this 
     * method will return the reference to configuration.
     *
     * @return reference to configuration
     */
    public IPCItem getConfigItemReference() {
        log.entering("getConfigItemReference()");
        log.exiting();
        return configItemReference;
    }

    /**
     * Gets item price. If not evaluated yet, returns null. To evaluate item price if not 
     * evaluated yet use <code>readItemPrice</code>.
     *
     * @return item price
     */
    public WebCatItemPrice getItemPrice() {
        log.entering("getItemPrice()");
        log.exiting();
        return price;
    }

    /**
     * Evaluates the item price only if it is not already available via the HashMap of Units and 
     * the corresponding price.  In the R/3 case when all units the corresponding WebCatItems are 
     * aggregated so the price would be calculated using the reference to the corresponding 
     * WebCatItem. In the CRM case readItemPrice would be called.
     *
     * @return item price
     */
    public WebCatItemPrice retrieveItemPrice() {

        /* Also in the R/3 case we will have to use readItemPrice, in case the item uses dynamic 
         * pricing either due to the fact, that the catalog uses dynamic prices, or due to the 
         * fact that the item is configurable. Otherwise we might switch to a different item, 
         * because if several items with the same product and unit are found (which might happen 
         * during a search, because the same item might exist in different areas), the units map 
         * will point to the last item with the given product and unit in the list, which must 
         * not be the configured item. So requiredItem will point to a different webCatItem than 
         * the configured one and deu to creating a new WebCatItemPrice, also a new IPCItem would 
         * be created, missing the COnfiguration of the current Item
         *
         * Thus whenever getConfigItemReference is not null, call readItemPrice()
         */

        log.entering("retrieveItemPrice()");

        if (log.isDebugEnabled()) {
            log.debug("Try to find item in units for unit: " + getUnit());
        }

        if (getConfigItemReference() == null && (units != null && !units.isEmpty())) {

            log.debug("configItemReference is null and units are not");

            // Volker if (units != null && !units.isEmpty()){
            WebCatItem requiredItem = (WebCatItem) units.get(getUnit());

            if (requiredItem != null) {
                log.debug("item found in units for unit");
                requiredItem.setQuantity(this.getQuantity());
                setItemPrice(new WebCatItemPrice(requiredItem.getWebCatInfo().getPriceCalculator(), requiredItem));
            }
            else { //unit probably not allowed, return zero price
                if (log.isDebugEnabled()) {
                    log.debug("no item found in units for unit: " + getUnit());
                }
                setItemPrice(new WebCatItemPrice());
            }

            log.exiting();

            return price;
        }
        else {
            log.debug("readItemPrice");
            log.exiting();

            return readItemPrice();
        }
    }

    /**
     * Reads item price. If not evaluated yet, evaluates. To only check whether item price was 
     * evaluated, use <code>getItemPrice</code>.
     *
     * @return item price
     */
    public WebCatItemPrice readItemPrice() {
        log.entering("readItemPrice()");

        if (price == null) {
            log.debug("price is null, read it");
            setItemPrice(new WebCatItemPrice(getWebCatInfo().getPriceCalculator(), this));
        }

        log.exiting();

        return price;
    }

    /**
     * Returns the price calculator for the item.
     *
     * @return price calculator instance
     */
    public PriceCalculator getPriceCalculator() {
        log.entering("getPriceCalculator()");
        log.exiting();
        return getWebCatInfo().getPriceCalculator();
    }

    /**
     * Sets item price. This method is used to externally populate this item with pricing 
     * information, especially when doing mass price calculations for all items of one page at 
     * the same time (for performance reasons). The price itself is not evaluated, but only 
     * stored, so use with care.
     *
     * @param price pricing information for this item
     */
    public void setItemPrice(WebCatItemPrice price) {

        log.entering("setItemPrice()");
        
        this.price = price;

        if (price != null && price.getPriceInfo() != null && price.getPriceInfo().getPricingItemReference() != null) {
            try {
                // due to the fact that we must keep the itemreference also for main 
                // positions that are not configurable, but have configurable subpositions
                // it is not checked anymore, if the IPCitem is configurable
                setConfigItemReference(price.getPriceInfo().getPricingItemReference());
            }
            catch (IPCException ex) {
                log.error("Exception when setting Reference", ex);
                setConfigItemReference(null);
            }
        }

        log.exiting();
    }

    /**
     * DOCUMENT ME!
     *
     * @return DOCUMENT ME!
     */
    public String getRemanufacturableFlag() {
        log.entering("getRemanufacturableFlag()");
        remanufacturableFlag = this.getAttribute("REMAN_ABL");
        log.exiting();
        return remanufacturableFlag;
    }

    /**
     * DOCUMENT ME!
     *
     * @return DOCUMENT ME!
     */
    public String readExchBusinessFlag() {
        log.entering("readExchBusinessFlag()");
        exchBusinessFlag = this.getAttribute("EXCH_BUS");
        log.exiting();
        return exchBusinessFlag;
    }

    /**
     * DOCUMENT ME!
     *
     * @return DOCUMENT ME!
     */
    public Hashtable readExProductItemPrices() {

        log.entering("readExProductItemPrices()");

        String nextParam = null;
        Vector purposeNames = new Vector();
        Hashtable exchPriceAttributes = null;
        exchPriceAttributes = new Hashtable();

		if (price == null){
			readItemPrice();
		}
		if(price != null){
			HashMap exchPrices = price.getExchProdPrices();
			Set keys = exchPrices.keySet();
			Iterator test = keys.iterator();

			while (test.hasNext()) {
			   nextParam = (String) test.next();
			   if (nextParam.startsWith("purposeNames") || nextParam.startsWith("PURPOSENAMES")) {
				   purposeNames.add(nextParam);
				   if (log.isDebugEnabled()) {
					   log.debug("Parameter : " + nextParam + " added");
				   }
			   }
		    }

			if (purposeNames != null) {
			   String condPurpNames[] = new String[purposeNames.size()];
			   purposeNames.copyInto(condPurpNames);

			   ResultData exchData = this.getCondPurpGroup();
			   if (exchData != null) {
				   int num = exchData.getNumRows();

				   for (int i = 0; i < num; i++) {
					   exchData.next();
					   if (log.isDebugEnabled()) {
						   log.debug("the condition purposes are" + exchData.getString("DEST"));
					   }
                      
			           for (int j = 1; j <= condPurpNames.length; j++) {
						   if (exchData.getString("DEST").equalsIgnoreCase((String) exchPrices.get("purposeNames[" + j + "]")))
							   //String pricingData = stringFormat(exchData.getString("DESCRIPTION"));
						   if (log.isDebugEnabled()) {
							   log.debug(
								   "the condition purposes values are(inside readExProductItemPrices())"
											   + exchData.getString("DEST")
											   + ","
											   + exchPrices.get("purposeValues[" + j + "]"));
						   }
						   exchPriceAttributes.put(exchData.getString("DESCRIPTION"), exchPrices.get("purposeValues[" + j + "]"));
					  }
				  }
				  exchData.beforeFirst();
			    }
		   }
		}
      
        log.exiting();

        return exchPriceAttributes;
    }

    /**
     * Returns the currency of this <code>WebCatItem</code>.
     *
     * @return currency of the price as String
     */
    public String getCurrency() {
        log.entering("getCurrency()");
        log.exiting();
        return price.getCurrency();
    }

    /**
     * Release item references to other objects. This is done for memory reasons; WebCatItem is 
     * able to re-determine these objects if needed, as it stores the object keys.
     */
    public void releaseReferences() {

        log.entering("releaseReferences()");
        
        if (log.isDebugEnabled()) {
            log.debug("Start finalizing item " + this);
        }

        //remove IPC items which are no longer needed
        //to improve IPC performance
        if (configItemReference != null) {
            if (log.isDebugEnabled()) {
                log.debug("finalize configItemReference for catalog item ref=" + configItemReference);
            }
            try {
                configItemReference.getDocument().removeItemExisting(configItemReference);
            }
            catch (IPCException e) {
                log.warn("system.eai.exception", e);
            }

            configItemReference = null;
        }
        else {
            log.debug("configItemReference is null");
        }

        //no special handling for the prices!
        //for configurable items, referenced IPC items are handled above.
        //for non-configurable items, IPC items are usually removed directly
        //after reading values. IPC items are only kept for price analysis, which 
        //will not be used in a productive environment, thus no need to care about
        //here.
        if (price != null && price.getAllPriceInfos() != null) {
            IPCItem ipcItem;
            for (int i=0; i < price.getAllPriceInfos().length; i++) {
                ipcItem = (IPCItem) price.getAllPriceInfos()[i] .getPricingItemReference();
                if (ipcItem != null) {
                    if (log.isDebugEnabled()) {
                        log.debug("finalize remove IPCItem from price=" + ipcItem);
                    }
                    ipcItem.getDocument().removeItemExisting(ipcItem);
                }
            }
        }
        
        //no special handling for the catalog item, as this is handled through
        //catalog cache anyway
        catalogItem = null;
        price = null;
        webCatSubItemList = null;
        //contract references are only flat Strings and not "real" references,
        //thus no need to handle them here
        contractRef = null;
        contractDetailedRef = null;
        solutionConfigurator = null;
        //are these currently used at all?
        eAuctionLink = null;

        log.exiting();
    }

    /**
     * Returns the <code>WebCatAreaAttributes</code> of this <code>WebCatItem</code>.
     *
     * @return areaAttributes
     */
    public WebCatAreaAttributes getAreaAttributes() {
        log.entering("getAreaAttributes()");
        log.exiting();
        return areaAttributes;
    }

    /**
     * Sets the <code>WebCatAreaAttributes</code> of this <code>WebCatItem</code>.
     *
     * @param areaAttributes to be set.
     */
    public void setAreaAttributes(WebCatAreaAttributes areaAttributes) {
        log.entering("setAreaAttributes()");
        this.areaAttributes = areaAttributes;
        log.exiting();
    }

    /**
     * Sets the eAuctionLink of this <code>WebCatItem</code>.
     *
     * @param link eAuctionLink as String
     */
    public void setEAuctionLink(String link) {
        log.entering("setEAuctionLink()");
        this.eAuctionLink = link;
        log.exiting();
    }

    /**
     * Returns the eAuctionLink of this <code>WebCatItem</code>.
     *
     * @return eAuctionLink as String
     */
    public String getEAuctionLink() {
        log.entering("getEAuctionLink()");
        log.exiting();
        return this.eAuctionLink;
    }

    /**
     * Determine whether this item's product is configurable. This method checks for the attribute
     * whether product is configurable. In case of IPC usage, it also checks whether an IPC item 
     * is available (otherwise it is configurable, but starting IPC configuration would fail).
     *
     * @return flag indicating whether product is configurable, see constants for allowed values.
     */
    public String getConfigurableFlag() {

        log.entering("getConfigurableFlag()");

        String configFlag = "";

        //first check if product is flagged as configurable
        String productConfigurableFlag = getAttributeByKey(AttributeKeyConstants.PRODUCT_CONFIGURABLE_FLAG);

        if (log.isDebugEnabled()) {
            log.debug("getConfigurableFlag(): productConfigurableFlag=" + productConfigurableFlag);
        }

        if (productConfigurableFlag != null && !productConfigurableFlag.trim().equals("")) {

            // product is configurable, so check if IPC should be used
            String productConfigurableIpc = getAttributeByKey(AttributeKeyConstants.PRODUCT_CONFIGURABLE_IPC_FLAG);

            if (log.isDebugEnabled()) {
                log.debug(
                    "productConfigurableIpc="
                        + productConfigurableIpc
                        + ", ipcItem available ="
                        + (getConfigItemReference() != null));
            }

            if (productConfigurableIpc != null && !productConfigurableIpc.trim().equals("")) {

                // IPC should be used, so check if IPC item could be created
                if (getConfigItemReference() != null) {
                    /* check if it is a grid product or not, grid products have a slightly different behavior
                     * if grid products should be configured, it is mandatory that a basket is created before
                     * and the product is added to basket, since we can't save any configuration information
                     * in the external configuration of an item. We always create sub-items instead of a
                     * configuration and attach them directly to the basket
                     * - DS -
                     */
                    if (productConfigurableFlag.equals("G")) {
                        configFlag = PRODUCT_CONFIGURABLE_IPC_GRID;
                    }
                    else {
                        configFlag = PRODUCT_CONFIGURABLE_IPC;
                    }
                }
                else {

                    // IPC item is not available: this is ok for pricing, as there the 
                    //  IPC item has to be created. 
                    // In catalog JSPs use PRODUCT_CONFIGURABLE_IPC
                    configFlag = PRODUCT_CONFIGURABLE_IPC_NOITEM;
                }
            }
            else {
                // IPC should not be used
                configFlag = PRODUCT_CONFIGURABLE_OTHER;
            }
        }
        else {
            // check if product is actually a product variant
            // (for these, config can be called in read-only mode)
            String productVariantFlag = getAttributeByKey(AttributeKeyConstants.PRODUCT_VARIANT_FLAG);

            if (log.isDebugEnabled()) {
                log.debug("productVariantFlag=" + productVariantFlag);
            }

            if (productVariantFlag != null && !productVariantFlag.trim().equals("")) {
                configFlag = PRODUCT_VARIANT;
            }
            else {
                configFlag = PRODUCT_NOT_CONFIGURABLE;
            }
        }

        if (log.isDebugEnabled()) {
            log.debug("configFlag=" + configFlag);
        }
        log.exiting();

        return configFlag;
    }

    /**
     * Returns the hierarchy level of a WebCatItem.
     * 
     */
    public int getHierarchyLevel() {
        log.entering("getHierarchyLevel()");
        log.exiting();
        return (0);
    }
    
    /**
     * This methods returns the hierarchy ids of the WebCatItem if present,
     * otherwise a String array of length 0 is returned.
     */
    public String[] getHierarchyIds() {
        log.entering("getHierarchyIds()");
        log.exiting();
        return getAttributeAllValuesByKey(AttributeKeyConstants.HIERARCHY_ID);
    }
    
    /**
     * This methods returns the hierarchy description of the WebCatItem if present,
     * otherwise a String array of length 0 is returned.
     * AN unknown desription will be indetified by -
     */
    public String[] getHierarchyDescriptions() {
        log.entering("getHierarchyDescriptions()");
        log.exiting();
        return getAttributeAllValuesByKey(AttributeKeyConstants.HIERARCHY_DESCR);
    }

    /**
     * Checks, if the item has accessories.
     * 
     * @return  true, if the item has accessories,
     *          false, otherwise.
     */
    public boolean isItemWithAccessories() {
        log.entering("isItemWithAccessories()");
        log.exiting();
        return catalogItem.isItemWithAccessories();
    }

    /**
     * Checks if the item is an accessory.<br> 
     * 
     * @return true, if the item is an accessory.<br>
     *         false, otherwise
     */
    public boolean isAccessory() {
        log.entering("isAccessory()");
        log.exiting();
        return catalogItem.isAccessory();
    }

    /**
     * Checks, if the item is classified as a Combined Rate Plan.
     * This is the case, if the attribute PRODUCT_ROLE is set to "C".
     * The method implements ProductBaseData.
     * 
     * @return  true, if the item is a Combined Rate Plan 
     *          false, otherwise.
     */
    public boolean isCombinedRatePlan() {
        final String METHOD = "isCombinedRatePlan()";
        log.entering(METHOD);
        boolean isCombinedRatePlan = catalogItem.isCombinedRatePlan();
        
        if (log.isDebugEnabled()) {
            log.debug(METHOD +": isCombinedRatePlan = " + isCombinedRatePlan);
        }
        log.exiting();
        return isCombinedRatePlan;
    }

    /**
     * Check whether a configuration item is available (only IPC supported!) and if so, whether its configuration is
     * not initial.
     *
     * @return boolean indicating whether the item has a non-initial  configuration
     */
    public boolean isConfigured() {
        log.entering("isConfigured()");

        boolean isConfigured = false;

        try {
            if (getConfigItemReference() != null
                && getConfigItemReference().getConfiguration() != null
                && !getConfigItemReference().getConfiguration().isInitial()) {
                isConfigured = true;
            }
        }
        catch (com.sap.spc.remote.client.object.IPCException ex) {
            log.error("com.sap.spc.remote.client.object.IPCException", ex);
        }

        if (log.isDebugEnabled()) {
            log.debug("isConfigured = " + isConfigured);
        }
        log.exiting();

        return isConfigured;
    }

    /**
     * Check whether the configuration of the item is complete and consistent
     *
     * @return boolean indicating whether the items configuration conatins errors
     */
    public boolean isConfiguredCompletely() {
        log.entering("isConfiguredCompletely()");

        boolean isConfiguredCompletely = true;

        try {
            if (getConfigItemReference() != null && getConfigItemReference().getConfiguration() != null) {
                Configuration conf = getConfigItemReference().getConfiguration();
                isConfiguredCompletely = conf.isComplete() && conf.isConsistent();
            }
        }
        catch (com.sap.spc.remote.client.object.IPCException ex) {
            log.error("com.sap.spc.remote.client.object.IPCException", ex);
        }

        if (log.isDebugEnabled()) {
            log.debug("isConfiguredCompletely = " + isConfiguredCompletely);
        }
        log.exiting();

        return isConfiguredCompletely;
    }

    /**
     * Returns true, if an explosion of the <code>WebCatItem</code> has been performed.
     * 
     * @return  true, if the item is exploded
     *          false, if the item is not exploded
     */
    public boolean isExploded() {
        log.entering("isExploded()");

        boolean isExploded = solutionConfigurator != null && webCatSubItemList != null && webCatSubItemList.size() > 0;

        if (log.isDebugEnabled()) {
            log.debug("isExploded = " + isExploded);
        }
        log.exiting();

        return isExploded;
    }

    /**
     * Checks if the item is just a main item.<br> 
     * 
     * Returns true if an item is directly part of an area (e.g would be false if only be 
     * part of an area because of accessory relations)
     * 
     * @return true, if the item is a main item.<br>
     *         false, otherwise
     */
    public boolean isMainItem() {
        log.entering("isMainItem()");
        log.exiting();
        return catalogItem.isMainItem();
    }

    /**
     * Checks if the item is relevant for explosion with the Solution Configurator.<br> 
     * 
     * This is the case, if the attribute IS_SOL_CONF_EXPLO is set to "X".
     * 
     * @return true, if the item is relevant for explosion.<br>
     *         false, otherwise
     */
    public boolean isRelevantForExplosion() {
        log.entering("isRelevantForExplosion()");
        log.exiting();
        return catalogItem.isRelevantForExplosion();
    }
    
    /**
     * Returns an iterator over the elements contained in the
     * <code>MessageList</code>.
     *
     * @return Iterator for this object
     */
    public Iterator iterator() {
        if (webCatSubItemList == null) {
            return null;
        }
        else {
            return webCatSubItemList.iteratorOnlyPopulated();
        }
    }

    /**
     * Checks if the item is a sub component.
     */
    public boolean isSubComponent() {
        log.entering("isSubComponent()");
        boolean isSubComp = isSalesComponent() || isDependentComponent() || isRatePlanCombination();

        if (log.isDebugEnabled()) {
            log.debug("isSubComponent = " + isSubComp);
        }
        log.exiting();
        return isSubComp;
    }

    /**
     * Before removing this object, make sure that referenced objects are handled. This is 
     * currently done by calling the releaseReferences() method.
     */
    protected void finalize() throws Throwable {
        log.entering("finalize()");
        if (log.isDebugEnabled()) {
            log.debug("Finalize WebCatItem: " + this);
        }
        releaseReferences();
        
        super.finalize();
        
        log.exiting();
    }

    /**
     * Sets the unit of measurement for this WebCatItem.
     *
     * @param unit
     */
    public void setUnit(String unit) {
        log.entering("setUnit()");
        this.unit = unit;
        log.exiting();
    }

    /**
     * Returns all attributes with key <code>AttributeKeyConstants.UNIT_OF_MEASUREMENT</code>.
     *
     * @return array with attributes
     */
    public String[] getUnitsOfMeasurement() {
        log.entering("getUnitsOfMeasurement()");
        String[] uoms = getAttributeAllValuesByKey(AttributeKeyConstants.UNIT_OF_MEASUREMENT);
        if (uoms == null) {
        	log.debug("Unit of measurements is null, so replace it by empty string array");
            uoms = new String[] {
            };
        }
        log.exiting();
        return uoms;
    }

    /**
     * Returns the possible UOMs of this <code>WebCatItem</code>.
     *
     * @return String[]
     */
    public String[] getPossibleUOMs() {

        log.entering("getPossibleUOMs()");

        String[] possibleUoms = null;

        String catalogType = getWebCatInfo().getCatalog().getClass().getName();
        boolean isBackendR3 = catalogType.endsWith("R3Catalog") || catalogType.endsWith("TrexManagedCatalog");

        if (!isBackendR3) {
            log.debug("CRM Catalog");
            possibleUoms = getUnitsOfMeasurement();
        }
        else if (units != null) {
            log.debug("R3 Catalog");
            possibleUoms = (String[]) units.keySet().toArray(new String[0]);
        }

        if (log.isDebugEnabled()) {
            if (possibleUoms != null) {
                for (int i = 1; i < possibleUoms.length; i++) {
                    log.debug("possibleUoms[" + i + "] = " + possibleUoms[i]);
                }
            }
            else {
                log.debug("possibleUoms is null");
            }
        }

        log.exiting();

        return possibleUoms;
    }

    /**
     * Returns the Guid of the baskets SC document, if the item is coming from a basket. This field 
     * is to be set if the details for a basket item are displayed and the SC might be called, so the SC
     * can determine the items SC configuration from the orders SC config document.
     * 
     * @return TechKey scOrderDocumentGuid the belonging SalesDocument SC DocumentGuid, in case the WebCatItem belongs 
     *                                     to a Product of a basket item.
     */
    public TechKey getSCOrderDocumentGuid() {
        log.entering("getSCOrderDocumentGuid()");
        log.exiting();
        return scOrderDocumentGuid;
    }

    /**
     * Returns the HashMap of the solution configurator relevant IPC attributes.<br>
     * 
     * Returns the attributes, that are necessary for the IPC pricing of the sub components. 
     * The Values are stored as key�value pair and can be used in the PriceCalculator during 
     * creation of the IPCItems.
     * 
     * @return HashMap scRelvIPCAttr
     */
    public HashMap getScRelvIPCAttr() {
        log.entering("getScRelvIPCAttr()");
        log.exiting();
        return scRelvIPCAttr;
    }

    /**
     * Returns the <code>SolutionConfigurator</code> of this <code>WebCatItem</code>.
     * 
     * @return solutionConfigurator
     */
    public SolutionConfigurator getSolutionConfigurator() {
        log.entering("getSolutionConfigurator()");
        log.exiting();
        return solutionConfigurator;
    }

    /**
     * Returns the item itself. For WebCatSubItems this method is returning the main WebCatItem. 
     * Due to the fact, that a normal WebCatItem has no parent it returns itself.
     * 
     * @return this <code>WebCatItem</code>
     */
    public WebCatItem getTopParentItem() {
        log.entering("getTopParentItem()");
        log.exiting();
        return this;
    }

    /**
     * Sets the <code>SolutionConfigurator</code> of this <code>WebCatItem</code>.
     * 
     * @param solutionConfigurator
     */
    public void setSolutionConfigurator(SolutionConfigurator solutionConfigurator) {
        log.entering("setSolutionConfigurator()");
        this.solutionConfigurator = solutionConfigurator;
        log.exiting();
    }

    /**
     * Returns the <code>TeckKey</code> of the parent of this <code>WebCatItem</code>.
     * The method implements a method of ProductBaseData.
     * 
     * @return parentTechKey
     */
    public TechKey getParentTechKey() {
        log.entering("getParentTechKey()");
        log.exiting();
        return parentTechKey;
    }

    /**
     *  Returns the <code>HashMap</code> with the units of this <code>WebCatItem</code>.
     *
     * @return units
     */
    public HashMap getUnits() {
        log.entering("getUnits()");
        log.exiting();
        return units;
    }

    /**
     * Sets the <code>HashMap</code> with the units for this <code>WebCatItem</code>.
     *
     * @param units
     */
    public void setUnits(HashMap units) {
        log.entering("setUnits()");
        this.units = units;
        log.exiting();
    }

    /**
     * Searches for the <code>webCatSubItem</code> with the given TechKey.
     * 
     * @param subItemTechKey the TechKEy of the rerquired subItem
     * 
     * @return webCatSubItem teh subItem if found 
     *         null else
     */
    public WebCatSubItem getWebCatSubItem(TechKey subItemTechKey) {
        log.entering("getWebCatSubItem(TechKey)");
        
        WebCatSubItem subItem = null;
        
        if (webCatSubItemList != null) {
            subItem = webCatSubItemList.getItem(subItemTechKey);
        }
        else {
            log.debug("webCatSubItemList is  null");
        }
        
        log.exiting();
        
        return subItem;
    }
    
    /**
     * Searches for the <code>webCatSubItem</code> with the given productId.
     * 
     * @param productId, the productID of the required subItem
     * 
     * @return webCatSubItem the subItem if found 
     *         null else
     */
    public WebCatSubItem getWebCatSubItem(String productId) {
      log.entering("getWebCatSubItem(String productID)");
      
      WebCatSubItem subItem = null; 
      boolean found = false;       
      if (webCatSubItemList != null) {
          Iterator it = webCatSubItemList.iterator();
          
          while(it.hasNext()) {
             subItem = (WebCatSubItem)it.next();
             if(subItem.getProductID().equals(productId)){
                 found = true;
                 break;              
             }
          }
      }
      else {
         log.debug("webCatSubItemList is  null");
      }
        
      log.exiting();
      if(found == true){
          return subItem;
      }
      else {
          return null;
      }
      
   }

    /**
     * Returns the <code>webCatSubItemList</code> of this <code>WebCatItem</code>.
     * 
     * @return webCatSubItemList.
     */
    public WebCatSubItemList getWebCatSubItemList() {
        log.entering("getWebCatSubItemList()");
        log.exiting();
        return webCatSubItemList;
    }

    /**
     * Returns the <code>webCatSubItemList</code> of this <code>WebCatItem</code>.
     * 
     * @see WebCatItemData#getWebCatSubItemListData()
     * 
     * @return the webCatSubItemListData object.
     */
    public WebCatSubItemListData getWebCatSubItemListData() {
        log.entering("getWebCatSubItemListData()");
        log.exiting();
        return webCatSubItemList;
    }

    /**
     * Sets the parent <code>TechKey</code> of this <code>WebCatItem</code>
     * 
     * @param parentTechKey
     */
    public void setParentTechKey(TechKey parentTechKey) {
        log.entering("setParentTechKey()");
        this.parentTechKey = parentTechKey;
        log.exiting();
    }

    /**
     * Sets the Guid of the baskets SC document, if the item is coming from a basket. This field 
     * is to be set if the details for a basket item are displayed and the SC might be called, so the SC
     * can determine the items SC configuration from the orders SC config document.
     * 
     * @param scOrderDocumentGuid the belonging SalesDocument SC DocumentGuid, in case the WebCatItem belongs 
     *                       to a Product of a basket item.
     */
    public void setSCOrderDocumentGuid(TechKey scOrderDocumentGuid) {
        log.entering("setSCOrderDocumentGuid()");
        this.scOrderDocumentGuid = scOrderDocumentGuid;
        log.exiting();
    }

    /**
     * Sets the solution configurator relevant IPC attributes HashMap.
     * 
     * @param scRelvIPCAttr HashMap of the attributes
     */
    public void setScRelvIPCAttr(HashMap scRelvIPCAttr) {
        log.entering("setScRelvIPCAttr()");
        this.scRelvIPCAttr = scRelvIPCAttr;
        log.exiting();
    }

    /**
     * Sets the <code>webCatSubItemList</code> for this <code>WebCatItem</code>
     * 
     * @param webCatSubItemList
     */
    public void setWebCatSubItemList(WebCatSubItemList webCatSubItemList) {
        log.entering("setWebCatSubItemList()");
        this.webCatSubItemList = webCatSubItemList;
        log.exiting();
    }

    /**
     * Sets the <code>webCatSubItemList</code> from a <code>WebCatSubItemListData</code> object.
     * 
     * @param webCatSubItemListData
     * 
     * @see WebCatItemData#setWebCatSubItemListData(WebCatSubItemListData)
     */
    public void setWebCatSubItemListData(WebCatSubItemListData webCatSubItemListData) {
        log.entering("setWebCatSubItemListData()");
        this.webCatSubItemList = (WebCatSubItemList) webCatSubItemListData;
        log.exiting();
    }

    /**
     * Returns the external configuration
     * 
     * @return the external configuration
     */
    public c_ext_cfg_imp getExtCfg() {
        log.entering("getExtCfg()");
        log.exiting();
        return extCfg;
    }

    /**
     * Returns the external configuration xml encoded with basis 64 for
     * transfer via http.
     */
    public String getExtConfigXmlEncodedB64() throws IPCException {
        return encodeB64(getExtConfigXml());
    }

    /**
     * Returns the external configuration as xml.
     * If the item has no configuration, an empty string "" is returned.
     */
    public String getExtConfigXml() {

        String xml = "";

        log.entering("getExtConfigXml()");

        // Get the IPC item
        IPCItem ipcItem = getConfigItemReference();
        if (null == ipcItem) {
            log.exiting();
            return "";
        }
                
        // Get the external configuration from the IPC item
        ext_configuration extConfig = ipcItem.getConfig();
        if (null == extConfig) {
            log.exiting();
            return "";
        }

        c_ext_cfg_imp cfgExtInst =
            new c_ext_cfg_imp(
                extConfig.get_name(),
                extConfig.get_sce_version(),
                extConfig.get_client(),
                extConfig.get_kb_name(),
                extConfig.get_kb_version(),
                extConfig.get_kb_build(),
                extConfig.get_kb_profile_name(),
                extConfig.get_language(),
                extConfig.get_root_id(),
                extConfig.is_consistent_p(),
                extConfig.is_complete_p());
                
        if (log.isDebugEnabled()) {
            log.debug("extConfig.get_name()             : " + extConfig.get_name());
            log.debug("extConfig.get_sce_version()      : " + extConfig.get_sce_version());
            log.debug("extConfig.get_client()           : " + extConfig.get_client());
            log.debug("extConfig.get_kb_name()          : " + extConfig.get_kb_name());
            log.debug("extConfig.get_kb_version()       : " + extConfig.get_kb_version());
            log.debug("extConfig.get_kb_build()         : " + extConfig.get_kb_build());
            log.debug("extConfig.get_kb_profile_name()  : " + extConfig.get_kb_profile_name());
            log.debug("extConfig.get_language()         : " + extConfig.get_language());
            log.debug("extConfig.get_root_id()          : " + extConfig.get_root_id());
            log.debug("extConfig.is_consistent_p()      : " + extConfig.is_consistent_p());
            log.debug("extConfig.is_complete_p()        : " + extConfig.is_complete_p());
        }

        Enumeration insts = ((c_ext_cfg_inst_seq_imp) extConfig.get_insts()).elements();

        while (insts.hasMoreElements()) {

            cfg_ext_inst inst = (cfg_ext_inst) insts.nextElement();

            cfgExtInst.add_inst(
                inst.get_inst_id(),
                inst.get_obj_type(),
                inst.get_class_type(),
                inst.get_obj_key(),
                inst.get_obj_txt(),
                inst.get_author(),
                inst.get_quantity(),
                inst.get_quantity_unit(),
                inst.is_consistent_p(),
                inst.is_complete_p());

            if (log.isDebugEnabled()) {
                log.debug("inst.get_inst_id()       : " + inst.get_inst_id());
                log.debug("inst.get_obj_type()      : " + inst.get_obj_type());
                log.debug("inst.get_class_type()    : " + inst.get_class_type());
                log.debug("inst.get_obj_key()       : " + inst.get_obj_key());
                log.debug("inst.get_obj_txt()       : " + inst.get_obj_txt());
                log.debug("inst.get_author()        : " + inst.get_author());
                log.debug("inst.get_quantity()      : " + inst.get_quantity());
                log.debug("inst.get_quantity_unit() : " + inst.get_quantity_unit());
                log.debug("inst.is_consistent_p()   : " + inst.is_consistent_p());
                log.debug("inst.is_complete_p()     : " + inst.is_complete_p());
            }
        }

        Iterator parts = extConfig.get_parts().iterator();

        while (parts.hasNext()) {
            cfg_ext_part part = (cfg_ext_part) parts.next();

            cfgExtInst.add_part(
                part.get_parent_id(),
                part.get_inst_id(),
                part.get_pos_nr(),
                part.get_obj_type(),
                part.get_class_type(),
                part.get_obj_key(),
                part.get_author(),
                part.is_sales_relevant_p());

            if (log.isDebugEnabled()) {
                log.debug("part.get_parent_id()         : " + part.get_parent_id());
                log.debug("part.get_inst_id()           : " + part.get_inst_id());
                log.debug("part.get_pos_nr()            : " + part.get_pos_nr());
                log.debug("part.get_obj_type()          : " + part.get_obj_type());
                log.debug("part.get_class_type()        : " + part.get_class_type());
                log.debug("part.get_obj_key()           : " + part.get_obj_key());
                log.debug("part.get_author()            : " + part.get_author());
                log.debug("part.is_sales_relevant_p()   : " + part.is_sales_relevant_p());
            }
        }

        Iterator values = extConfig.get_cstics_values().iterator();

        while (values.hasNext()) {
            cfg_ext_cstic_val value = (cfg_ext_cstic_val) values.next();

            cfgExtInst.add_cstic_value(
                value.get_inst_id(),
                value.get_charc(),
                value.get_charc_txt(),
                value.get_value(),
                value.get_value_txt(),
                value.get_author(),
                value.is_invisible_p());

            if (log.isDebugEnabled()) {
                log.debug("value.get_inst_id()      : " + value.get_inst_id());
                log.debug("value.get_charc()        : " + value.get_charc());
                log.debug("value.get_charc_txt()    : " + value.get_charc_txt());
                log.debug("value.get_value()        : " + value.get_value());
                log.debug("value.get_value_txt()    : " + value.get_value_txt());
                log.debug("value.get_author()       : " + value.get_author());
                log.debug("value.is_invisible_p()   : " + value.is_invisible_p());
            }
        }

        Iterator conditions = extConfig.get_price_keys().iterator();

        while (conditions.hasNext()) {
            cfg_ext_price_key condition = (cfg_ext_price_key) conditions.next();

            cfgExtInst.add_price_key(condition.get_inst_id(), condition.get_key(), condition.get_factor());

            if (log.isDebugEnabled()) {
                log.debug("condition.get_inst_id()  : " + condition.get_inst_id());
                log.debug("condition.get_key()      : " + condition.get_key());
                log.debug("condition.get_factor()   : " + condition.get_factor());
            }
        }

        // generate the xml
        xml = cfgExtInst.cfg_ext_to_xml_string();

        if (log.isDebugEnabled()) {
            log.debug("cfgExtInst.toString() : " + cfgExtInst.toString());
            log.debug("xmlConfig : " + ((xml != null) ? xml : "NULL"));
        }

        log.exiting();
        return xml;
    }

    /**
     * Sets the external configuration
     * 
     * @param extCfg the external configuration
     */
    public void setExtCfg(c_ext_cfg_imp extCfg) {
        log.entering("setExtCfg()");
        this.extCfg = extCfg;
        log.exiting();
    }

    /**
     * returns the long item description.
     * 
     * @return long description
     */
    public String getLngDesc() {
        log.entering("getLngDesc()");
        log.exiting();
        return getAttributeByKey(AttributeKeyConstants.LNG_DESC);
    }

    /**
     * returns the item subtitle
     * 
     * @return item subtitle
     */
    public String getSubtitle() {
        log.entering("getSubtitle()");
        log.exiting();
        return getAttributeByKey(AttributeKeyConstants.SUBTITLE);
    }

    /**
     * returns eye catcher text.
     * if it is a main product, the eye catcher value is stored in TEXT_0004
     * if it is a product bundle, the eye catcher value is stored in TEXT_0005
     * 
     * @return
     */
    public String getEyeCatcherText() {

        log.entering("getEyeCatcherText()");
        boolean isSubComponent = isSubComponent();
        if (log.isDebugEnabled()) {
            log.debug("isSubComponent() = " + isSubComponent );
        }

        if (isSubComponent) {
            log.exiting();

            return getAttributeByKey(AttributeKeyConstants.BUNDLE_CPT_EYE_CATCHER);
        }
        else {
            log.exiting();

            return getAttributeByKey(AttributeKeyConstants.MAIN_EYE_CATCHER);
        }
    }

    /**
     * returns an iterator with all selected subitems. 
     * this function is relevant when items are added in the basket.
     */
    public Iterator iteratorOnlySubItemSelected() {

        log.entering("iteratorOnlySubItemSelected()");

        ArrayList arrayList = new ArrayList();
        WebCatSubItem webCatSubItem = null;
        if(webCatSubItemList != null) {
           Iterator subListIterator = webCatSubItemList.iterator();
           while (subListIterator.hasNext()) {
              webCatSubItem = (WebCatSubItem) subListIterator.next();
              if (webCatSubItem.isScSelected()) {
                  arrayList.add(webCatSubItem);
                  if (log.isDebugEnabled()) {
                      log.debug("add subitem " + webCatSubItem.getProduct());
                  }
             }
           }
        }
        log.exiting();

        return arrayList.iterator();
    }

    /**
     * returns true if it is a configurable product, false otherwise
     * The method implements ProductBaseData.
     * 
     * @return true if it is a configurable product, 
     *         false otherwise
     */
    public boolean isConfigurable() {
        String method = "isConfigurable()";
        log.entering(method);
        boolean isConfigurable = false;
        
        isConfigurable =
            getConfigurableFlag().equals(WebCatItem.PRODUCT_CONFIGURABLE_IPC)
                || getConfigurableFlag().equals(WebCatItem.PRODUCT_CONFIGURABLE_IPC_NOITEM)
                         //      reenable if changes for GRID config are activated
                         // || getConfigurableFlag().equals(WebCatItem.PRODUCT_CONFIGURABLE_IPC_GRID)
                         ;
        if (log.isDebugEnabled()) {
            log.debug(method + ": is configurable = " + isConfigurable);
        }

        log.exiting();
        return isConfigurable;
    }

    /**
     * Returns true, if the webCatItem is old fashion configurable
     *
     * @return true   if the item is old fashion configurable false  otherwise
     */
    public boolean isItemOldFashionConfigurable() {

        log.entering("isItemOldFashionConfigurable()");

        boolean oldFashionConfigurable = false;

        if (log.isDebugEnabled()) {
            log.debug("test webCatItem " + getProduct() + " for oldFashionConfigurable");
        }

        String configUseIPC = getAttribute("CONFIG_USE_IPC");
        String configurableFlag = getAttribute("PRODUCT_CONFIGURABLE_FLAG");

        if (log.isDebugEnabled()) {
            log.debug(
                "configUseIPC="
                    + configUseIPC
                    + " getConfigItemReference()="
                    + getConfigItemReference()
                    + " configurableFlag="
                    + configurableFlag);
        }

        /*it is an "old fashion" config*/
        if (configUseIPC != null
            && configUseIPC.equals("X")
            && getConfigItemReference() != null
            && configurableFlag != null
            && (configurableFlag.equals("A")
                || configurableFlag.equals(
                    "X") // reenable if changes for GRID config are activated
                // || configurableFlag.equals("G")
                || configurableFlag.equals("C"))) {
            oldFashionConfigurable = true;
        }

        if (log.isDebugEnabled()) {
            log.debug("test webCatItem " + getProduct() + " for oldFashionConfigurable result: " + oldFashionConfigurable);
        }

        log.exiting();

        return oldFashionConfigurable;
    }

    /**
     * Returns true, if the webCatItem is a product variant
     *
     * @return true   if the item is a product variant false  otherwise
     */
    public boolean isItemProductVariant() {

        log.entering("isItemProductVariant()");

        boolean isVariant = false;

        if (log.isDebugEnabled()) {
            log.debug("test if webCatItem " + getProduct() + " is product variant");
        }

        String productVariantFlag = getAttributeByKey(AttributeKeyConstants.PRODUCT_VARIANT_FLAG);

        /*it is an "old fashion" config*/
        if (productVariantFlag != null && !productVariantFlag.equals("") && getConfigItemReference() != null) {
            isVariant = true;
        }

        if (log.isDebugEnabled()) {
            log.debug("test if webCatItem " + getProduct() + " is product variant: " + isVariant);
        }

        log.exiting();

        return isVariant;
    }

    /**
     * 
     * @param selectedComponent: true : the returned list contains only the selected subitems
     * @return a list with the description of all (or only the selected) subitems which level is 1
     */
    public Iterator getDescriptionOfFirstLevelComponent(boolean selectedComponent) {
        log.entering("getDescriptionOfSelectedFirstLevelComponent()");
        ArrayList arrayList = new ArrayList();
        WebCatSubItem webCatSubItem = null;

        WebCatSubItemList parentSubItemList = this.getParentItem().getWebCatSubItemList();
        if (parentSubItemList != null) {
            Iterator subListIterator = parentSubItemList.iterator();
            while (subListIterator.hasNext()) {
                webCatSubItem = (WebCatSubItem) subListIterator.next();
                if (webCatSubItem.getParentItem().getTechKey().getIdAsString().equals(this.getTechKey().getIdAsString())) {
                    arrayList.add(webCatSubItem.getDescription());
                    if (log.isDebugEnabled()) {
                        log.debug("add subitem " + webCatSubItem.getDescription());
                    }
                }
            }
        }
        log.exiting();
        return arrayList.iterator();
    }

    /**
     * Returns the information, if this <code>WebCatItem</code> is optional. 
     * The method implements ProductBaseData
     * 
     * @return <code>false</code>
     */
    public boolean isOptional() {
        log.entering("isOptional()");
        log.exiting();
        return false;
    }

    /**
     * Returns the information, if this <code>WebCatItem</code> is part of a group. 
     * The method implements ProductBaseData
     * 
     * @return <code>false</code>
     */
    public boolean isPartOfAGroup() {
        log.entering("isPartOfAGroup()");
        log.exiting();
        return false;
    }

    /**
     * Returns the group key.
     * The method implements ProductBaseData
     * 
     * @return group key as <code>String</code>
     * 
     * @see     #setGroupKey(String)
     */
    public String getGroupKey() {
        log.entering("getGroupKey()");
        log.exiting();
        return "";
    }

    /**
     * Returns the information if a product is selected
     * The method implements ProductBaseData
     *
     * @return true
     */
    public boolean getAuthorFlag() {
        String method = "getAuthorFlag()";
        log.entering(method);
        log.exiting();
        return true;
    }

    /**
     * Get Solution configurator selection flag.
     *
     * @return selection flag as String
     */
    public String getAuthor() {
        return "";
    }

    /**
     * Sets the selection flag
     * The method implements ProductBaseData
     *
     * @param selectFlag of type boolean
     */
    public void setAuthorFlag(boolean selectFlag) {
        String method = "setAuthorFlag()";
        log.entering(method);
        log.exiting();
    }

    /**
     * Checks if a product is a product variant
     * The method implements ProductBaseData
     *
     * @param isProductVariant of type boolean, true if product is a product variant,
     *        false else.
     */
    public boolean isProductVariant() {
        String method = "isProductVariant()";
        log.entering(method);
        boolean isProductVariant = false;
        
        String productVariantFlag = getAttributeByKey(AttributeKeyConstants.PRODUCT_VARIANT_FLAG);        
        if ( productVariantFlag != null && ! productVariantFlag.equals("")) { 
            isProductVariant = true;    
        }
        
        if (log.isDebugEnabled()) {
            log.debug(method + ": is product variant" + isProductVariant);
        }
        log.exiting();
        return isProductVariant;
    }
    
    /**
     * Return a String representation of the object
     */
    public String toString() {
        
        log.entering("toString()");

        String retVal = super.toString();
        retVal = retVal  + " product=[" +  getProduct() + "]" ;
        retVal = retVal  + ", ItemKey=[" + ((itemKey == null) ? "NULL" : itemKey.toString()) + "]";
        retVal = retVal  + ", unit=[" + unit + "]";
        retVal = retVal  + ", units=[";
        if (units != null && units.size() > 0) {
            String unit = null;
            for (Iterator iter = units.keySet().iterator(); iter.hasNext();) {
                   unit = (String) iter.next();
                   retVal = retVal  + unit;
                   if (iter.hasNext()) {
                       retVal = retVal  + ", ";
                   }
            }
        }
        retVal = retVal  + "]";
        retVal = retVal  + ", parentTechKey=[" + ((parentTechKey == null) ? "NULL" : parentTechKey.toString()) + "]";
        retVal = retVal  + ", price=[" + price + "]";
        retVal = retVal  + ", solutionConfigurator=[" + solutionConfigurator + "]";
        retVal =
            retVal + ", scOrderDocumentGuid=[" + ((scOrderDocumentGuid == null) ? "NULL" : scOrderDocumentGuid.toString()) + "]";
        retVal = retVal  + ", scRelvIPCAttr=[" + scRelvIPCAttr + "]";
        retVal = retVal  + ", extCfg=[" + extCfg + "]";
        retVal = retVal  + ", exchBusinessFlag=[" + exchBusinessFlag + "]";
        retVal = retVal  + ", exchProdData=[" + exchProdData + "]";
        retVal = retVal  + ", configItemReference=[" + configItemReference + "]";
                 
        log.exiting();

        return retVal;
    }
    
    /**
     * returns the price eye catcher text which is stored on the TREX as TEXT_0006
     * @return the price eye catcher text 
     */
        public String getPriceEyeCatcherText() {
            String method = "getPriceEyeCatcherText()";
            log.entering(method);
            String text = getAttributeByKey(AttributeKeyConstants.PRICE_EYE_CATCHER);
            if (log.isDebugEnabled()) {
                log.debug(method +": returns "+text);
            }
            log.exiting();

            return text;
        }
        
    /**
     * Checks if this item is classified as Dependent Components Product.
     * The method implements ProductBaseData
     * 
     * @return  true, if the item is a dependent components product,
     *          false, otherwise.
     */
    public boolean isDependentComponent() {
        String method = "isDependentComponent()";
        log.entering(method);
        boolean retVal = false; 
        
        if (getTopParentItem().isExploded()) {
            if (WebCatSubItem.SC_ITEM_TYPE_DEPENDENT_COMPONENT.equals(getScItemType())) {
                retVal = true;
            }    
            if (log.isDebugEnabled()) {
                log.debug(method +": isDependentComponent (from Solution Configurator) = " + retVal);
            }
        }
        else {
            ProductBaseData parent = getParent();
            if (parent != null) {
                retVal = !isMainItem() && !parent.isSalesPackage() && !parent.isCombinedRatePlan();
                if (log.isDebugEnabled()) {
                    log.debug(method +": isDependentComponent (from TREX) = " + retVal);
                }
        }
            else {
                if (log.isDebugEnabled()) {
                    log.debug(method +": parent is null");
                }
            }
        }

        log.exiting();
        return retVal;
    }
    
    /**
     * Checks if this item is classified as Rate Plan.
     * The method implements ProductBaseData
     * 
     * @return  true, if the item is a rate plan product,
     *          false, otherwise.
     */
    public boolean isRatePlan() {
        final String METHOD = "isRatePlan()";
        log.entering(METHOD);
        boolean isRatePlan = catalogItem.isRatePlan();
        
        if (log.isDebugEnabled()) {
            log.debug(METHOD +": isRatePlan = " + isRatePlan);
        }
        log.exiting();
        return isRatePlan;
    }
    
    /**
     * Checks if this item is classified as Rate Plan Combination.
     * The method implements ProductBaseData
     * 
     * @return  true, if the item is a rate plan combination product,
     *          false, otherwise.
     */
    public boolean isRatePlanCombination() {
        String method = "isRatePlanCombination()";
        log.entering(method);
        boolean retVal = false; 
        
        if (getTopParentItem().isExploded()) {
            if (WebCatSubItem.SC_ITEM_TYPE_COMB_RATE_PLAN_COMPONENT.equals(getScItemType())) {
                retVal = true;
            }    
            if (log.isDebugEnabled()) {
                log.debug(method +": isRatePlanCombination (from Solution Configurator) = " + retVal);
            }
        }
        else {
            ProductBaseData parent = getParent();
            if (parent != null) {
                retVal = parent.isCombinedRatePlan();
            if (log.isDebugEnabled()) {
                log.debug(method +": isRatePlanCombination (from TREX) = " + retVal);
            }
        }
            else {
                if (log.isDebugEnabled()) {
                    log.debug(method +": parent is null");
                }
            }
        }
        log.exiting();

        return retVal;
    }

    /**
     * Checks if this item is classified as Sales Components Product.
     * The method implements ProductBaseData
     * 
     * @return  true, if the item is a sales components product,
     *          false, otherwise.
     */
    public boolean isSalesComponent() {
        String method = "isSalesComponent()";
        log.entering(method);
        boolean retVal = false; 
        
        if (getTopParentItem().isExploded()) {
            if (WebCatSubItem.SC_ITEM_TYPE_SALES_COMPONENT.equals(getScItemType())) {
                retVal = true;
            }    
            if (log.isDebugEnabled()) {
                log.debug(method +": isSalesComponent (from Solution Configurator) = " + retVal);
            }
        }
        else {
            ProductBaseData parent = getParent();
            if (parent != null) {
                retVal = parent.isSalesPackage();
            if (log.isDebugEnabled()) {
                log.debug(method +": isSalesComponent (from TREX) = " + retVal);
            }
        }
            else {
                log.debug(method +": parent is null");
            }
        }
        log.exiting();
        return retVal;
    }
    
    /**
     * Returns the Solution Configurator item type
     * 
     * @return String the Solution configurator item type
     */
    public String getScItemType() {
        return "";
    }

    /**
     * Checks, if the item is a Sales Package.
     * The method implements ProductBaseData
     * 
     * @return  true, if the item is a Sales Package
     *          false, otherwise.
     */
    public boolean isSalesPackage() {
        final String METHOD = "isSalesPackage()";
        log.entering(METHOD);
        boolean isSalesPackage = catalogItem.isSalesPackage();
        
        if (log.isDebugEnabled()) {
            log.debug(METHOD +": isSalesPackage = " + isSalesPackage);
        }
        log.exiting();
        return isSalesPackage;
    }

    /**
     * returns the default contract duration for this WebCatItem
     * 
     * @return the default contract duration for this WebCatItem
     */
    public ContractDuration getDefaultContractDuration() {
        log.entering("getDefaultContractDuration()");
        log.exiting();
        return defaultContractDuration;
    }

    /**
     * returns the table of all contract duration for this WebCatItem
     * 
     * @return the array with the different contract duration for this WebCatItem
     */
    public ArrayList getContractDurationArray() {
        log.entering("getContractDurationArray()");
        log.exiting();
        return contractDurationArray;
    }

    /**
      * returns the selected contract duration for this WebCatItem. 
      * The selected contract duration is set in the JSP pages.
      * 
      * @return the selected contract duration for this WebCatItem
      */
    public ContractDuration getSelectedContractDuration() {
        log.entering("getSelectedContractDuration()");
        
        if(selectedContractDuration == null){
            populateContractDuration();
        }
        log.exiting();
        return selectedContractDuration;
    }

    /**
     * sets the default contract duration
     * 
     * @param defaultContractDuration : the defaultContractDuration for this WebCatItem
     * 
     */
    public void setDefaultContractDuration(ContractDuration defaultContractDuration) {
        log.entering("setDefaultContractDuration");
        log.exiting();
        this.defaultContractDuration = defaultContractDuration;
    }

    /**
     * sets the contract duration array
     * 
     * @param contractDurationArray : the contractDurationArray for this WebCatItem
     * 
     */
    public void setContractDurationArray(ArrayList contractDurationArray) {
        log.entering("setContractDurationArray");
        log.exiting();
        this.contractDurationArray = contractDurationArray;
    }

    /**
     * sets the selected contract duration
     * 
     * @param selectedContractDuration : the selectedContractDuration for this WebCatItem
     * 
     */
    public void setSelectedContractDuration(ContractDuration selectedContractDuration) {
        log.entering("setSelectedContractDuration");
        //if the selectedContractDuration is null, then the contractDurationArray is null too. 
        //a selectedContractDuration should be found in the contractDurationArray, that's why 
        //we should first call the populateContractDuration function to populate the 
        //defaultContractDuration and the contractDurationArray. 
        if (this.selectedContractDuration == null){
            populateContractDuration();
        }
        
        if (selectedContractDuration != null) {
			// check is given contract duration is valid, if not take default contract duration 
			boolean cntDrFound = false;
			ContractDuration compDur = null;
        
			if (log.isDebugEnabled()) {
                log.debug(
                    "Search for given contract duration : val="
                        + selectedContractDuration.getValue()
                        + " unit="
                        + selectedContractDuration.getUnit());
			}
			for (int i=0; i < contractDurationArray.size() && selectedContractDuration != null; i++) {
				compDur = (ContractDuration) contractDurationArray.get(i);
				if (log.isDebugEnabled()) {
                    log.debug("Compare found contract duration : val=" + compDur.getValue() + " unit=" + compDur.getUnit());
				}
                if (compDur.getUnit().equals(selectedContractDuration.getUnit())
                    && compDur.getValue().equals(selectedContractDuration.getValue())) {
					cntDrFound = true;
					break;
				}
			}
        
			if (cntDrFound) {
				this.selectedContractDuration = selectedContractDuration;
			}
			else {
				if (log.isDebugEnabled()) {
                    log.debug(
                        "Given contract duration : val="
                            + selectedContractDuration.getValue()
                            + " unit="
                            + selectedContractDuration.getUnit()
                            + " is invalid. Set Default Duration instead");
				}
			}
        }
        else {
        	log.debug("Selected contract duration is null");
        }

        log.exiting(); 
    }
    
    public ContractDuration findContractDuration(String theContractDurationToFind) {
        log.entering("findContractDuration");
        
        if(contractDurationArray != null ) {
            Iterator it = contractDurationArray.iterator();
            ContractDuration contractDuration = null;
            while(it.hasNext()) {
                contractDuration = (ContractDuration)it.next();
                if (contractDuration
                    .toString(getItemKey().getParentCatalog().getCatalog().getDefaultLocale())
                    .equals(theContractDurationToFind)) {
                    return contractDuration;
                }
            }
        }
        log.exiting();
        return null;
    }

    /**
     * reads the contract duration information on the TREX and put the different contract duration in the contractDurationArray
     */
    public void populateContractDuration() {
        log.entering("populateContractDuration");
        
        String duration[] = getAttributeAllValuesByKey(AttributeKeyConstants.CONTRACT_DURATION); 
        String duration_unit[] = getAttributeAllValuesByKey(AttributeKeyConstants.CONTRACT_DURATION_UNIT);
        String duration_default[] = getAttributeAllValuesByKey(AttributeKeyConstants.CONTRACT_DURATION_DEFAULT);
        
        int size = duration.length;
        contractDurationArray = new ArrayList();
        ContractDuration contractDuration = null;
        for(int i=0;i<size;i++) {
            contractDuration = new ContractDuration(duration[i], duration_unit[i]);
            contractDurationArray.add(contractDuration);
            if(duration_default[i].equals("X")) {
                defaultContractDuration = contractDuration;
                selectedContractDuration = contractDuration;
            }
        }
        //if no default contract duration is set in the backend, we will select the first.
        if(defaultContractDuration == null && contractDurationArray.size() > 0) {
            defaultContractDuration = (ContractDuration)contractDurationArray.get(0);
            selectedContractDuration = (ContractDuration)contractDurationArray.get(0);
        }
        log.exiting();
    }
    
    /**
     * Returns the information, if this <code>WebCatSubItem</code> is a selected by the 
     * solution configurator.
     * The Method implements ProductBaseData.
     * 
     * @return <code>true</code>, if the item is selected by the solution configurator
     *         <code>false</code>, if not
     * 
     */
    public boolean isScSelected() {
        return false;
    }

    /**
     * Reset to the default constellation.<br> 
     * 
     */
    public void resetToDefCmp() {
        log.entering("resetToDefCmp()");
        if (this.solutionConfigurator != null) {
            // delete the SC explosion
            setSolutionConfigurator(null);
            setWebCatSubItemList(null);
            clearScRelvIPCAttr();
            // read the default constellation
            createDefSlsCompWebCatSubItemList();
        }
        log.exiting();
    }

    /**
     * clone function
     */
    public Object clone() {
        log.entering("clone");
        try {
            WebCatItem myClone = (WebCatItem) super.clone();
            myClone.webCatSubItemList = null;
            WebCatSubItemList clonedSubItemList = new WebCatSubItemList(getWebCatInfo());
            if(getWebCatSubItemList() != null){
                Iterator it = getWebCatSubItemList().iterator();
                while(it.hasNext()) {
                    WebCatSubItem subItem = (WebCatSubItem)it.next();
                    clonedSubItemList.addItem((WebCatSubItem)subItem.clone());
                }
            }
            myClone.webCatSubItemList = clonedSubItemList;
            log.exiting();
            return myClone;
        }
        catch (CloneNotSupportedException e) {
            log.error("clone error: "+e.getMessage());
            log.exiting();
            return null;
        }
    }
    
    /**
     * this function modifies the parenttechkey and itemtechkey for the main item and his children.
     * this function is called after a SC relevant item was add into the basket. We must change
     * the techkeys in order to be able to add this package twice in the basket. If the techkeys
     *   
     */    
    public void changeTechKey(TechKey parentTechKey) {
        log.entering("changeTechKey");
        TechKey techKey = TechKey.generateKey();
        setParentTechKey(parentTechKey);
        setTechKey(techKey);
        if(getWebCatSubItemList() != null){
           Iterator it = getWebCatSubItemList().iterator();
           while(it.hasNext()) {
               ((WebCatItem)it.next()).changeTechKey(techKey);
           }
        }
        log.exiting();
    }
    /**
     * The method checks if at least one message is existing for the main item or the 
     * contract items. 
     * @return isMsgExisting as boolean, true if at least one message is existing,
     *                       false otherwise
     */    
    public boolean hasMessages() {
        final String METHOD = "hasMessages()";
        log.entering(METHOD);
        boolean retval = (getMessageList() != null && getMessageList().size() > 0);
        
        if (!retval && getContractItemsArray() != null) {
            // also check the messages of the contract subitems of this webCatItem
            Iterator iter = getContractItemsArray().iterator();
            while (iter.hasNext() && !retval) {
                retval = ((WebCatItem) iter.next()).hasMessages();
            }
        }
        
        if (log.isDebugEnabled()) {
            log.debug(METHOD + ": item (" + getId() + ") has messages: " + retval);
        }
        log.exiting();
        return retval;
    }
    
    /**
     * Returns all point codes of the product
     * @return point codes, as String[]
     */
    public String[] getLoyPtCodes() {
        log.entering("getLoyPtCodes()");
        String loyPtCodes[] = null;
        int idx = 0;

        IAttributeValue[] attributes = getAttributesByPrefix(AttributeKeyConstants.LOY_POINTS);

        if (attributes != null) {
            loyPtCodes = new String[attributes.length];
            for (int i = 0; i < attributes.length; i++) {
                String attrName = attributes[i].getAttributeName();
                
                String loyPtCodeInt = attrName.substring(AttributeKeyConstants.LOY_POINTS.length());
                if (loyPtCodeInt != null && loyPtCodeInt.length() > 0) {
                    loyPtCodes[idx] = loyPtCodeInt;
                    idx++;
                }
            }
        }
        log.exiting();
        return loyPtCodes;
    }


    /**
     * Returns all points of the product for all point codes
     * @return points, as String[]
     */
    public String[] getLoyPts() {
        log.entering("getLoyPts()");
        String loyPts[] = null;
        int idx = 0;

        IAttributeValue[] attributes = getAttributesByPrefix(AttributeKeyConstants.LOY_POINTS);

        if (attributes != null) {
            loyPts = new String[attributes.length];
            for (int i = 0; i < attributes.length; i++) {
                String attrName = attributes[i].getAttributeName();                 
                String loyPtsTmp[] = getAttributeAllValues(attrName);
                if (loyPtsTmp != null) {
                    for (int j = 0; j < loyPtsTmp.length; j++) {
                        loyPts[idx] = loyPtsTmp[j];
                        idx++;
                    }
                }
            }
        }
        log.exiting();
        return loyPts;
    }

    /**
     * Returns the points of the product for a concrete point code
     * @param loyPtCode, Point Code as String
     * @return Points as String
     */
    public String getPoints(String loyPtCode) {
        log.entering("getPoints()");
        String loyPoints = null;
        String loyProg = "";
        String prog_pt_code = loyProg + "_" + loyPtCode;
        String loyPtCodes[] = getLoyPtCodes();
        String loyPts[] = getLoyPts();
        
        if (loyPtCodes != null && loyPtCode != null) {
            for (int i = 0; i < loyPtCodes.length; i++) {
                if (loyPtCodes[i] == null || !loyPtCodes[i].equals(prog_pt_code)) {
                    continue;
                }
                if (loyPts[i] != null) {
                    loyPoints = loyPts[i];
                }
            }
        }
        log.exiting();
        return loyPoints;
    }

    /**
     * Returns the LoyPointCodeId
     * 
     * @return loyPointCodeId as String
     */
//    public String getLoyPointCodeId() {
//        String ptCodeId = null;
//        if (getWebCatInfo() != null) {
//            ptCodeId = getWebCatInfo().getLoyPointCodeId();
//        } 
//        return ptCodeId;
//    }

    /**
     * Returns the results of the last ATP check
     * 
     * @return atpResultList the results of the last ATP check
     */
    public AtpResultList getAtpResultList() {
        log.entering(" getAtpResultList()");
        log.exiting();
        return atpResultList;
    }

    /**
     * Checks if the items is a points item of a reward area
     * 
     * @return isPtsItem flag as boolean
     */
    public boolean isPtsItem() {
        boolean isPtsItem = false;
        String attr = getAttribute(AttributeKeyConstants.IS_PTS_ITEM);
        if (attr != null && "X".equals(attr)) {
            isPtsItem  = true;
        }
        return isPtsItem;
    }

    /**
     * Sets the results of the last ATP check
     * 
     * @param atpResultList the results of the last ATP check
     */
    public void setAtpResultList(AtpResultList atpResultList) {
        log.entering(" setAtpResultList()");
        this.atpResultList = atpResultList;
        log.exiting();
    }

    /**
     * Checks if the items is a buy points item of a buy points area
     * 
     * @return isBuyPtsItem flag as boolean
     */
    public boolean isBuyPtsItem() {
        boolean isBuyPtsItem = false;
        String attr = getAttribute(AttributeKeyConstants.IS_BUY_PTS_ITEM);
        if (attr != null && "X".equals(attr)) {
            isBuyPtsItem  = true;
        }
        return isBuyPtsItem;
    }

    /**
     * Returns all values of a catalog attribute.
     * @param prefex as String
     * @return values of the requested attribute as array of String; empty array (not null!) 
     *         if not available
     */
    public IAttributeValue[] getAttributesByPrefix(String prefix) {

        log.entering("getAttributeAllValues("+ prefix + ")");

        Iterator iter = catalogItem.getAttributeValues();

        if (iter == null) {
            log.debug("iter is null");
            log.exiting();
            return null;
        }

        ArrayList attributes = new ArrayList();

        while (iter.hasNext()) {
            IAttributeValue attribute = (IAttributeValue ) iter.next();
            String attName = attribute.getAttributeName();
            if (attName.startsWith(prefix)) {
                attributes.add(attribute);
                if (log.isDebugEnabled()) {
                    log.debug("attName=" + attName);
                }
            }
        }

        log.exiting();
        return (IAttributeValue[]) attributes.toArray(new IAttributeValue[attributes.size()]);
    }

}
