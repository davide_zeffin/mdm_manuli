package com.sap.isa.catalog.webcatalog;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Hashtable;

import com.sap.isa.core.logging.IsaLocation;

/**
 * To change this generated comment edit the template variable "typecomment":
 * Window>Preferences>Java>Templates.
 * To enable and disable the creation of type comments go to
 * Window>Preferences>Java>Code Generation.
 */
public class WebCatModifiableObjectsContainer extends WebCatBusinessObjectBase 
{

  private Hashtable items;
  
  private Hashtable areas;
  
  private Hashtable itemListsByArea;
  
  private Hashtable areaListsByParentArea;

  private static IsaLocation log =
      IsaLocation.getInstance(WebCatModifiableObjectsContainer.class.getName());

  /**
   * Constructor for WebCatModifiableObjectsContainer.
   */
  WebCatModifiableObjectsContainer()
  {
    items = new Hashtable();
    itemListsByArea = new Hashtable();
    areas = new Hashtable();
    areaListsByParentArea = new Hashtable();
  }
  
  /* --------------------------- items -------------------------------------- */

  /**
   * Add a modifiable catalog item to the container
   * @param item the catalog item to be added
   */
  public void addItemData(WebCatModificationData modData)
  {
    if (!items.containsKey(modData.getId()))
    {
      items.put(modData.getId(), modData);
    }
    else
    {
      // deleted item, re-added rfu: problem here?
      modData.setModificationStatus(WebCatModificationStatus.CHANGED_OBJECT);
      //((WebCatItemModifiable) items.get(item.getItemID())).undelete();
    }
    if (!getItemDataList(modData.getParentAreaId()).contains(modData))
    {
      getItemDataList(modData.getParentAreaId()).add(modData);
    }
  }
  
  /**
   * Helper method to retrieve items data for an area. This method should not be
   * called from outside. Use getItems(String) instead.
   * @param areaId the catalog area
   * @return ArrayList of items data changed or added for the area
   */
  protected ArrayList getItemDataList(String areaId)
  {
    ArrayList itemList = (ArrayList) itemListsByArea.get(areaId);
    if (itemList == null)
    {
      itemList = new ArrayList();
      itemListsByArea.put(areaId, itemList);
    }
    return itemList;
  }
  
  /**
   * Get all modifiable catalog items for a given area, that are stored in
   * the container
   * @param areaId catalog area
   * @return array of modifiable items
   */
  public WebCatModificationData[] getItemsData(String areaId)
  {
    ArrayList itemList = getItemDataList(areaId);
    return (WebCatModificationData[]) itemList.toArray(new WebCatModificationData[itemList.size()]);
  }
  
  /**
   * Get all <b>new</b> modifiable catalog items for a given area, that are 
   * stored in the container
   * @param areaId catalog area
   * @return array of new modifiable items
   */
  public WebCatModificationData[] getNewItemsData(String areaId)
  {
    ArrayList itemList = getItemDataList(areaId);
    ArrayList newItems = new ArrayList();
    for (int i=0; i<itemList.size(); i++)
    {
      WebCatModificationData item = (WebCatModificationData) itemList.get(i);
      if (item.getModificationStatus() == WebCatModificationStatus.NEW_OBJECT)
        newItems.add(item);
    }
    return (WebCatModificationData[]) newItems.toArray(new WebCatModificationData[newItems.size()]);
  }
  
  /**
   * Get all modifiable catalog items stored in the container
   * @return array of modifiable catalog items
   */
  public WebCatModificationData[] getItemsData()
  {
    Collection allItems = items.values();
    return (WebCatModificationData[]) allItems.toArray(new WebCatModificationData[allItems.size()]);
  }

  /**
   * Get modifiable catalog item by item ID (if available)
   * @return the modifiable catalog item with the given id if available,
   *         <code>null</code> otherwise
   */
  public WebCatModificationData getItemData(String itemId)
  {
    return (WebCatModificationData) items.get(itemId);
  }
  
  /**
   * Remove a newly created item from an area.
   * This method is used only when moving an item which was newly created.
   * In this case, no reference to the old area needs to be kept, as it is not
   * known at all to the backend.
   * Note that for existing items from the catalog, this must <b>not</b> be 
   * called, as we need to keep the &quot;delete&quot; information in the old
   * area as well as the &quot;add&quot; information in the new.
   */
  public void removeItem(WebCatModificationData item, String areaId)
  {
    if (item.getModificationStatus() != WebCatModificationStatus.NEW_OBJECT)
      return;
    boolean removed = getItemDataList(areaId).remove(item);
    if (!removed)
    {
      log.warn("catalog.error", new Object[] { item, areaId }, new Exception("catalog.error"));
    }
  }

  /* --------------------------- areas -------------------------------------- */

  /**
   * Add a modifiable catalog area to the container
   * @param area the catalog area to be added
   */
  public void addAreaData(WebCatModificationData modArea)
  {
    if (!areas.containsKey(modArea.getId()))
    {
      areas.put(modArea.getId(), modArea);
    }
    if (modArea.getParentAreaId() == null)
    {
      if (!getAreaDataList(WebCatArea.ROOT_AREA).contains(modArea))
        getAreaDataList(WebCatArea.ROOT_AREA).add(modArea);
    }
    else 
    {
      if (!getAreaDataList(modArea.getParentAreaId()).contains(modArea))
        getAreaDataList(modArea.getParentAreaId()).add(modArea);
    }
  }
  
  /**
   * Helper method to retrieve areas for a parent area. This method should not 
   * be called from outside. Use getAreas(String) instead.
   * @param parentAreaId the parent catalog area
   * @return ArrayList of areas changed or added for the parent area
   */
  protected ArrayList getAreaDataList(String parentAreaId)
  {
    ArrayList areaList = (ArrayList) areaListsByParentArea.get(parentAreaId);
    if (areaList == null)
    {
      areaList = new ArrayList();
      areaListsByParentArea.put(parentAreaId, areaList);
    }
    return areaList;
  }
  
  /**
   * Get all modifiable catalog areas for a given parent area, that are stored 
   * in the container
   * @param parentAreaId parent catalog area
   * @return array of modifiable areas
   */
  public WebCatModificationData[] getAreasData(String parentAreaId)
  {
    ArrayList areaList = getAreaDataList(parentAreaId);
    return (WebCatModificationData[]) areaList.toArray(new WebCatModificationData[areaList.size()]);
  }
  
  /**
   * Get all <b>new</b> modifiable catalog areas for a given parent area, that 
   * are stored in the container
   * @param parentAreaId parent catalog area
   * @return array of new modifiable areas
   */
  public WebCatModificationData[] getNewAreasData(String parentAreaId)
  {
    if (parentAreaId == null)
      parentAreaId = WebCatArea.ROOT_AREA;
    ArrayList areaList = getAreaDataList(parentAreaId);
    ArrayList newAreas = new ArrayList();
    for (int i=0; i<areaList.size(); i++)
    {
      WebCatModificationData area = (WebCatModificationData) areaList.get(i);
      if ((area.getModificationStatus() == WebCatModificationStatus.NEW_OBJECT) ||
          ((area.getModificationStatus() == WebCatModificationStatus.CHANGED_OBJECT) &&
           (!parentAreaId.equals(area.getParentAreaIdOriginal()))))
        newAreas.add(area);
    }
    return (WebCatModificationData[]) newAreas.toArray(new WebCatModificationData[newAreas.size()]);
  }
  
  /**
   * Get all modifiable catalog areas stored in the container
   * @return array of modifiable catalog areas
   */
  public WebCatModificationData[] getAreasData()
  {
    Collection allAreas = areas.values();
    return (WebCatModificationData[]) allAreas.toArray(new WebCatModificationData[allAreas.size()]);
  }

  /**
   * Get modifiable catalog area by area ID (if available)
   * @return the modifiable catalog area with the given id if available,
   *         <code>null</code> otherwise
   */
  public WebCatModificationData getAreaData(String areaId)
  {
    return (WebCatModificationData) areas.get(areaId);
  }
  
  /**
   * Remove a newly created area from a parent area.
   * This method is used only when moving an area which was newly created.
   * In this case, no reference to the old parent area needs to be kept, as it 
   * is not known at all to the backend.
   * Note that for existing areas from the catalog, this must <b>not</b> be 
   * called, as we need to keep the &quot;delete&quot; information in the old
   * parent area as well as the &quot;add&quot; information in the new.
   */
  public void removeArea(WebCatModificationData area, String parentAreaId)
  {
    if (area.getModificationStatus() != WebCatModificationStatus.NEW_OBJECT)
      return;
    boolean removed = getAreaDataList(parentAreaId).remove(area);
    if (!removed)
    {
      log.warn("catalog.error", new Object[] { area, parentAreaId }, new Exception("catalog.error"));
    }
  }

}
