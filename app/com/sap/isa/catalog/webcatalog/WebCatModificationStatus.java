package com.sap.isa.catalog.webcatalog;

/**
 */
public class WebCatModificationStatus
{

  /**
   * Indicator that this is a newly created object
   */
  public final static WebCatModificationStatus NEW_OBJECT = 
                                    new WebCatModificationStatus("N");
  
  /**
   * Indicator that this is a modified object
   */
  public final static WebCatModificationStatus CHANGED_OBJECT = 
                                    new WebCatModificationStatus("C");
  
  /**
   * Indicator that this object is deleted
   */
  public final static WebCatModificationStatus DELETED_OBJECT = 
                                    new WebCatModificationStatus("D");
  
  /**
   * Indicator that this is an unchanged object
   */
  public final static WebCatModificationStatus UNCHANGED_OBJECT = 
                                    new WebCatModificationStatus("U");
  
  protected String status = null;
  
  /**
   * Constructor for WebCatModificationStatus.
   */
  private WebCatModificationStatus(String status)
  {
    this.status = status;
  }
  
  public static WebCatModificationStatus getStatus(String status)
  {
    if (status == null)
      return null;
    if (status.equalsIgnoreCase("N"))
      return NEW_OBJECT;
    if (status.equalsIgnoreCase("C"))
      return CHANGED_OBJECT;
    if (status.equalsIgnoreCase("D"))
      return DELETED_OBJECT;
    if (status.equalsIgnoreCase("U"))
      return UNCHANGED_OBJECT;
    return null;
  }

}
