/*****************************************************************************

    Class:        WebCatItemPrice
    Copyright (c) 2006, SAP AG, Germany, All rights reserved.
    Created:      03.02.2006 

*****************************************************************************/
package com.sap.isa.catalog.webcatalog;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;

import com.sap.isa.backend.boi.webcatalog.pricing.PriceInfo;
import com.sap.isa.backend.boi.webcatalog.pricing.PriceType;
import com.sap.isa.backend.boi.webcatalog.pricing.Prices;
import com.sap.isa.businessobject.webcatalog.pricing.PriceCalculator;
import com.sap.isa.core.logging.IsaLocation;

/**
 * Stores price information for a <code>WebCatItem</code>.
 */
public class WebCatItemPrice extends WebCatBusinessObjectBase {
    
    public static int SHOW_ALL_PRICES = 0;
    public static int SHOW_SUMS_ONLY = 1;
    public static int SHOW_NO_SUMS = 2;

    private static IsaLocation log = IsaLocation.getInstance(WebCatItemPrice.class.getName());

    /** Constant that defines the separator between the entries of one row of the price list. */
    private final static String SEPARATOR = "&nbsp;";

    private Prices priceInfos = null;

    /**
     * Constructor to create a price instance for a web item.
     *
     * @param priceCalc the calculator to be used for determining the price
     * @param item the item for which the price shall be determined
     */
    public WebCatItemPrice(PriceCalculator priceCalc, WebCatItem item) {
        log.entering("WebCatItemPrice(PriceCalculator priceCalc, WebCatItem item)");
        if (log.isDebugEnabled()) {
            log.debug("Read Price(s) for item \"" + item.getProduct() + "\".");
        }
        if( priceCalc != null ) {
            this.priceInfos = priceCalc.getPrices(item);
        }
        else {
            log.error("PriceCalculator is null");
        }
        
        log.exiting();
    }

    /**
     * Constructor to create a price instance from existing list of priceInfos. This is used 
     * when retrieving prices for more than one item at once, to attach prices to the items.
     *
     * @param priceInfos the calculator to be used for determining the price
     */
    public WebCatItemPrice(Prices priceInfos) {

        if (priceInfos == null) {
            this.priceInfos = new Prices();
            log.debug("Entered NO Prices!");
        }
        else {
            this.priceInfos = priceInfos;
            if (log.isDebugEnabled()) {
                log.debug("Entered " + priceInfos.size() + " Price(s): " + getPrice());
            }
        }
    }

    /**
     * Constructor to create a price instance which represents empty prices information
     */
    public WebCatItemPrice() {
        this(null);
    }

    /**
     * DOCUMENT ME!
     *
     * @return DOCUMENT ME!
     */
    public HashMap getExchProdPrices() {

        HashMap resultMap = new HashMap();
        PriceInfo pi;
        if( ( pi = getPriceInfo()) != null) {
            resultMap = pi.getExchProdPrices();
        }

        if (resultMap == null) {
            log.error("There no pricing related attributes for the exchange Product");
        }

        return resultMap;
    }

    /**
     * DOCUMENT ME!
     *
     * @return DOCUMENT ME!
     */
    public String getCurrency() {
        return this.getPriceInfo().getCurrency();
    }

    /**
     * Gets price information as PriceInfo object. Retrieve the price as defined for this 
     * item. The price may be calculated via IPC or may be a list or scale price. In case 
     * of scale prices, this method only returns the first object. The method does not
     * return recurrent prices.
     *
     * @return PriceInfo for the first price, if there are multiple.
     */
    public PriceInfo getPriceInfo() {

        if ( priceInfos == null || priceInfos.size() == 0) {
            return null;
        }

        return (PriceInfo) priceInfos.getPriceInfo(0);
    }
    
    /**
     * Returns a list of PriceInfo objects for the given PriceTypes. 
     * This list includes all PriceInfos with the given PriceTypes from the priceInfos attribute.
     * With the sumOnly flags it can be steered if only sums of the given PriceType should be
     * returned
     * 
     * @param priceTypes the priceTypes to search for
     * @param sumsOnly should only sums be rturned
     * @param specialPriceLast shoudl the specialPrices be sorted after or before their related default price
     *
     * @return PriceInfo[] all price info objects matching the given criteria.
     */
    public PriceInfo[] getPriceInfosForPriceTypes(PriceType[] priceTypes, int[] priceInfoArray, boolean specialPricesLast) {
        
        log.entering("getPriceInfosForPriceTypes(PriceType[] priceTypes, int[] priceInfoArray, boolean specialPricesLast)");
        
        PriceInfo[] retVal = new PriceInfo[0];
        ArrayList foundPriceInfos = new ArrayList();
        
        PriceInfo currPriceInfo = null;
        PriceInfo compPriceInfo = null;
        int samePriceTypeIdx = -1;
        int priceInfo;
        
        if (priceTypes != null && priceTypes.length > 0) {
            
            for (int i = 0; i < priceTypes.length; i++) {
                priceInfo = SHOW_ALL_PRICES;
                if (priceInfoArray != null &&  i < priceInfoArray.length) { 
                    priceInfo = priceInfoArray[i];
                }
                else {
                    if (log.isDebugEnabled()) {
                        log.debug("priceInfoArray[] is null or has no entry for index: " + i);
                    }
                    priceInfo = SHOW_ALL_PRICES;
                }
                if (log.isDebugEnabled()) {
                    log.debug("Search for priceType: " + priceTypes[i].toString() + " priceInfo =" + priceInfo  +
                              " specialPricesLast =" + specialPricesLast);
                }
                
                for (int j = 0; j < priceInfos.size(); j++) {
                    currPriceInfo = priceInfos.getPriceInfo(j);
                    if (log.isDebugEnabled()) {
                        log.debug("compare priceInfo Type: " + currPriceInfo);
                    }
                    if (currPriceInfo.getPriceType().equals(priceTypes[i]) &&
                        (priceInfo == SHOW_ALL_PRICES ||
                         (currPriceInfo.isSumPrice() && priceInfo == SHOW_SUMS_ONLY) ||
                         (!currPriceInfo.isSumPrice() && priceInfo == SHOW_NO_SUMS))) {
                        log.debug("PriceInfo object is matching the criterias");
                        
                        // start sorting
                        samePriceTypeIdx = -1;
                        compPriceInfo = null;
                        // search for entry with same type, sumFlag, ...
                        for (int k = 0; k < foundPriceInfos.size(); k++) {
                            compPriceInfo = (PriceInfo) foundPriceInfos.get(k);
                            if (compPriceInfo.getPriceType().equals(currPriceInfo.getPriceType()) && 
                                compPriceInfo.isSumPrice() == currPriceInfo.isSumPrice() &&
                                compPriceInfo.getPeriodType() == currPriceInfo.getPeriodType() &&
                                !compPriceInfo.getPromotionalPriceType().equals(currPriceInfo.getPromotionalPriceType())) {
                                samePriceTypeIdx = k;
                                if (log.isDebugEnabled()) {
                                    log.debug("Sort: matching priceInfo found: " + compPriceInfo);
                                }
                                break;
                            }
                        }
                        
                        // if no mathcing entry was foudn add at the end of the list
                        if (samePriceTypeIdx == -1) {
                            log.debug("add at the end of the list");
                            foundPriceInfos.add(currPriceInfo); 
                        }
                        else {
                            // the currPriceInfo price info is a marketing price
                            if (compPriceInfo.getPromotionalPriceType().equals(PriceInfo.NO_PROMOTIONAL_PRICE)) {
                                log.debug("currPriceInfo price info is a marketing price");
                                if (specialPricesLast) {
                                    if (log.isDebugEnabled()) {
                                        log.debug("Price added at position " + (samePriceTypeIdx + 1));
                                    }
                                    foundPriceInfos.add(samePriceTypeIdx + 1, currPriceInfo); 
                                }
                                else {
                                    if (log.isDebugEnabled()) {
                                        log.debug("Price added at position " + samePriceTypeIdx);
                                    }
                                    foundPriceInfos.add(samePriceTypeIdx ,currPriceInfo); 
                                }
                            }
                            // the compPriceInfo price info is a marketing price
                            else {
                                log.debug("compPriceInfo price info is a marketing price");
                                if (specialPricesLast) {
                                    if (log.isDebugEnabled()) {
                                        log.debug("Price added at position " + samePriceTypeIdx);
                                    }
                                    foundPriceInfos.add(samePriceTypeIdx, currPriceInfo); 
                                }
                                else {
                                    if (log.isDebugEnabled()) {
                                        log.debug("Price added at position " +(samePriceTypeIdx + 1));
                                    }
                                    foundPriceInfos.add(samePriceTypeIdx + 1 ,currPriceInfo);
                                }
                            }
                        }
                    }
                } // for (int j, ..
            } // for (int i, ..
            
            retVal = new PriceInfo[foundPriceInfos.size()];
            foundPriceInfos.toArray(retVal);
        }
        else {
            log.debug("priceType is null or empty");
        }
        
        if (log.isDebugEnabled()) {
            log.debug("Returning PriceInfo[] with " + retVal.length + " entries");
        }

        log.exiting();
        return retVal;
    }

    /**
     * Returns an array of <code>PriceInfo</code> objects.
     * 
     * @param  includePromotionalPrices controls, if promotional prices should
     *         be included in the returned array.
     */
    public PriceInfo[] getPriceInfos(boolean includePromotionalPrices) {

        ArrayList prices = new ArrayList();
        for (int i = 0; i < priceInfos.size(); i++) {

            PriceInfo priceInfo = priceInfos.getPriceInfo(i);

            // Skip promotional prices, if not wanted
            if (!priceInfo.isPayablePrice()) {
                if (includePromotionalPrices == false) {
                    continue;
                }
            }

            prices.add(priceInfo);
        }

        return (PriceInfo[]) prices.toArray(new PriceInfo[prices.size()]);
    }

    /**
     * Gets price information. Retrieve the price as defined for this item. The price may 
     * be calculated via IPC or may be a list or scale price. In case of scale prices,
     * this method only returns the first line. Use <code>getPrices()</code> instead for 
     * items with scale prices.
     *
     * @return DOCUMENT ME!
     * 
     * @deprecated due to new concept for promotional prices 
     */
    public String getPrice() {
        return getPriceAsString(getPriceInfo());
    }

    /**
     * Get List of prices Retrieve an array of PriceInfo objects representing the prices 
     * determined for the item. Note that in case of scale price this array contains the 
     * scale information, in case of multiple price types defined in eai-config.xml this 
     * contains all the prices.
     *
     * @return priceInfos array of PriceInfo objects representing the prices for the item
     * 
     * @deprecated  Use getPriceInfos(Boolean includePromotionalPrices) instead.
     */
    public PriceInfo[] getAllPriceInfos() {
        return (priceInfos==null) ? null : priceInfos.getPriceInfos();
    }

    /**
     * Get List of prices.<br>
     * 
     * This is the method usually called for display of prices, for example in product 
     * catalog. Even if more than one price type is defined, this method returns only the 
     * first price, only for scale prices it returns all of the prices. If the item has 
     * no scale price the returned Iterator only has one element which is the same as would
     * be retrieved from <code>getPrice()</code>.
     *  
     * If no prices are available an empty iterator will be returned.<br>
     * 
     * Does not return recurrent prices. The iterated objects are of type <code>String</code>.
     *
     * @return iterator Iterator of the scale prices.
     * 
     * @deprecated due to new concept for promotional prices 
     */
    public Iterator getPrices() {

        ArrayList pricesAsString = new ArrayList();

        if (isScalePrice()) {

            for (int idx = 0; idx < priceInfos.size(); idx++) {

                PriceInfo priceInfo = priceInfos.getPriceInfo(idx);

                // skip recurrent prices
                if (priceInfo.getPeriodType() != PriceInfo.ONE_TIME) {
                    continue;
                }

                pricesAsString.add(getPriceAsString(priceInfo));
            }
        }
        else {
            // recurrent prices are not in the first position
            pricesAsString.add(getPrice());
        }

        return pricesAsString.iterator();
    }

    /**
     * Returns all recurrent prices for an item.
     *
     * @return priceInfos array of PriceInfo objects representing the prices for the item
     */
    public PriceInfo[] getRecurrentPrices() {

        ArrayList recurrentPrices = new ArrayList();
        for (int i = 0; i < priceInfos.size(); i++) {

            PriceInfo priceInfo = priceInfos.getPriceInfo(i);

            // skip one time prices
            if (priceInfo.getPeriodType() == PriceInfo.ONE_TIME) {
                continue;
            }

            recurrentPrices.add(priceInfo);
        }

        return (PriceInfo[]) recurrentPrices.toArray(new PriceInfo[recurrentPrices.size()]);
    }

    /**
     * Returns all recurrent prices for an item for a given period type. Allowed values
     * for <code>periodType</code> are: PriceInfo.PER_YEAR, PriceInfo.PER_MONTH, 
     * PriceInfo.PER_WEEK and PriceInfo.PER_DAY.  For other values, null is returned.
     *
     * @param   periodType filter for the returned recurrent prices
     * @return  priceInfos array of PriceInfo objects representing the prices for the item
     */
    public PriceInfo[] getRecurrentPrices(int periodType) {

        // Check given period type
        if (!(periodType == PriceInfo.PER_YEAR
            || periodType == PriceInfo.PER_MONTH
            || periodType == PriceInfo.PER_WEEK
            || periodType == PriceInfo.PER_DAY)) {

            return null;
        }

        // extract recurrent prices
        ArrayList recurrentPrices = new ArrayList();
        for (int i = 0; i < priceInfos.size(); i++) {

            PriceInfo priceInfo = priceInfos.getPriceInfo(i);

            if (priceInfo.getPeriodType() != PriceInfo.ONE_TIME) {
                recurrentPrices.add(priceInfo);
            }
        }

        PriceInfo[] prices = (PriceInfo[]) recurrentPrices.toArray(new PriceInfo[recurrentPrices.size()]);
        return prices;
    }

    /**
     * determines whether the current price information is a scale price information. Current implementation assumes
     * that if the first PriceInfo has scale price type, than all do.
     *
     * @return DOCUMENT ME!
     */
    public boolean isScalePrice() {

        PriceInfo inf = getPriceInfo();

        if (inf != null && inf.getPriceType() == PriceType.SCALE_VALUE) {
            return true;
        }

        return false;
    }

    /**
     * determines whether the given price information is a scale price information.
     *
     * @param priceInfo DOCUMENT ME!
     *
     * @return DOCUMENT ME!
     */
    public boolean isScalePrice(PriceInfo priceInfo) {

        if (priceInfo != null && priceInfo.getPriceType() == PriceType.SCALE_VALUE) {
            return true;
        }

        return false;
    }

    /**
     * Gets price information as PriceInfo object for the case of both static and dynamic 
     * prices enabled for the shop. This returns the PriceInfo object that came from IPC from 
     * the array of all the PriceInfo objects. In case of multiple price types defined in 
     * eai-config.xml this method returns the first PriceInfo object present in the array. 
     * Should be called only in this shop setting scenario.
     *
     * @return priceInfo the priceInfo object for IPC prices
     * 
     * @deprecated due to new concept for promotional prices 
     */
    public PriceInfo getIPCPriceInfo() {

        PriceInfo inf = null;

        if (priceInfos.size() == 0) {
            return null;
        }

        for (int i = 0; i < priceInfos.size(); i++) {

            inf = priceInfos.getPriceInfo(i);

            if (inf != null
                && (inf.getPriceType() != PriceType.SCALE_VALUE && inf.getPriceType() != PriceType.LIST_VALUE)) {
                break;
            }
        }

        return inf;
    }

    /**
     * Returns the List prices / Scale Prices from the array of PriceInfo objects in case 
     * of both static and dynamic prices being enabled for the shop. Should be called only 
     * in this shop setting scenario.<br>
     * 
     * Only one time prices are returned.
     *
     * @return iterator Iterator of the scale/list prices.
     * 
     * @deprecated due to new concept for promotional prices 
     */
    public Iterator getListPrices() {

        PriceInfo inf;
        ArrayList listPricesAsString = new ArrayList();

        for (int i = 0; i < priceInfos.size(); i++) {

            inf = priceInfos.getPriceInfo(i);

            // skip recurrent prices
            if (inf.getPeriodType() != PriceInfo.ONE_TIME) {
                continue;
            }

            if (inf != null
                && (inf.getPriceType() == PriceType.SCALE_VALUE || inf.getPriceType() == PriceType.LIST_VALUE)) {
                listPricesAsString.add(getPriceAsString(inf));
            }
        }

        return listPricesAsString.iterator();
    }

    /**
     * Returns the List price (coming from CRM) from the array of PriceInfo objects in 
     * case of both static and dynamic prices being enabled for the shop. Should be called 
     * only in this shop setting scenario.<br>
     * 
     * Only one time prices are returned.
     *
     * @return String list price as String.
     * 
     * @deprecated due to new concept for promotional prices 
     */
    public String getListPrice() {

        PriceInfo inf;
        String priceAsString = "";

        if (priceInfos.size() == 0) {
            return "";
        }

        for (int i = 0; i < priceInfos.size(); i++) {

            inf = priceInfos.getPriceInfo(i);

            // skip recurrent prices
            if (inf.getPeriodType() != PriceInfo.ONE_TIME) {
                continue;
            }

            if (inf != null && inf.getPriceType() == PriceType.LIST_VALUE) {
                priceAsString = getPriceAsString(inf);
            }
        }

        return priceAsString;
    }

    /**
     * Returns the IPC price from the array of PriceInfo objects in case of both static 
     * and dynamic prices being enabled for the shop. Should be called only in this shop 
     * setting scenario.<br>
     * 
     * Only one time prices are returned.
     *
     * @return String IPC price as String.
     * 
     * @deprecated due to new concept for promotional prices 
     */
    public String getIPCPrice() {

        PriceInfo inf;

        if (priceInfos.size() == 0) {
            return "";
        }

        for (int i = 0; i < priceInfos.size(); i++) {

            inf = priceInfos.getPriceInfo(i);

            // skip recurrent prices
            if (inf.getPeriodType() != PriceInfo.ONE_TIME) {
                continue;
            }

            if (inf != null
                && (inf.getPriceType() != PriceType.SCALE_VALUE && inf.getPriceType() != PriceType.LIST_VALUE)) {
                return getPriceAsString(inf);
            }
        }

        return "";
    }

    /**
     * In case of both static and dynamic prices being enabled for the shop this method
     * returns true otherwise false.
     *
     * @return boolean
     * 
     * @deprecated due to new concept for promotional prices 
     */
    public boolean isMultiplePrice() {

        PriceInfo inf = null;
        PriceInfo inf0 = null;

        if (priceInfos.size() == 0) {
            return false;
        }

        //First select a priceInfo which is of type SCALE or LIST   
        for (int j = 0; j < priceInfos.size(); j++) {

            inf0 = priceInfos.getPriceInfo(j);

            if (inf0 != null
                && (inf0.getPriceType() == PriceType.SCALE_VALUE || inf0.getPriceType() == PriceType.LIST_VALUE)) {
                break;
            }
        }

        inf = null;

        for (int i = 0; i < priceInfos.size(); i++) {
            inf = priceInfos.getPriceInfo(i);

            if (inf != null && inf0 != null && inf.getPriceType() != inf0.getPriceType()) {
                return true;
            }
        }

        return false;
    }

    /**
     * In case of both static and dynamic prices being enabled for the shop this method
     * returns true if the static PriceInfo objects have PriceType SCALE_TYPE.
     *
     * @return boolean
     * 
     * @deprecated due to new concept for promotional prices 
     */
    public boolean isMultipleAndScalePrice() {

        PriceInfo inf;

        if (isMultiplePrice()) {

            for (int j = 0; j < priceInfos.size(); j++) {

                inf = priceInfos.getPriceInfo(j);

                if (inf != null && inf.getPriceType() == PriceType.SCALE_VALUE) {
                    return true;
                }
                else if (inf != null && inf.getPriceType() == PriceType.LIST_VALUE) {
                    return false;
                }
            }
        }

        return false;
    }

    /**
     * Converts the price info objects to the web catalog specific representation.
     *
     * @param priceInfo a list of price info objects
     *
     *
     *  @return DOCUMENT ME!
     * 
     * @deprecated due to new concept for promotional prices 
     */
    private String getPriceAsString(PriceInfo priceInfo) {

        if (priceInfo == null) {
            return "";
        }

        StringBuffer entry = new StringBuffer();
        entry.append(priceInfo.getPrice());
        entry.append(SEPARATOR);
        entry.append(priceInfo.getCurrency());

        if (isScalePrice(priceInfo)) {

            // not always available!
            String type = priceInfo.getScaletype();

            if (type != null && type.length() != 0) {
                entry.append(SEPARATOR);
                entry.append(type);
            }

            entry.append(SEPARATOR);
            entry.append(priceInfo.getQuantity());
            entry.append(SEPARATOR);
            entry.append(priceInfo.getUnit());
        }

        return entry.toString();
    }
}