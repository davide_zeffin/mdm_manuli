package com.sap.isa.catalog.webcatalog;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.ParsePosition;

import com.sap.isa.catalog.boi.IItem;
import com.sap.isa.catalog.boi.IQueryItem;
import com.sap.isa.core.logging.IsaLocation;


public class WebCatItemKey extends WebCatBusinessObjectBase {

  private WebCatInfo parentCatalog;
  private String     areaID;
  private String     itemID;

  private String     quantity;
  private boolean    selected;
  
  private static IsaLocation log =
      IsaLocation.getInstance(WebCatItemKey.class.getName());

  /**
   * Basis Constructor that sets some data. This one is called by the other
   * constructors.
   */
  private WebCatItemKey(WebCatInfo catalog)
  {
    parentCatalog = catalog;
    quantity = "1";
    selected = false;
  }
  
  /**
   * Usual constructor for existing catalog items.
   */
  public WebCatItemKey(WebCatInfo catalog, IItem catalogItem) {
    this(catalog);
    if (catalogItem != null)
    {
      itemID = catalogItem.getGuid();
      if (catalogItem instanceof IQueryItem)
      {
        areaID = ((IQueryItem) catalogItem).getCategoryGuid();
      }
      else
      {
        areaID = catalogItem.getParent().getGuid();
      }
    }
  }

  /**
   * This constructor should only be used for creating new items.
   */
  public WebCatItemKey(WebCatInfo catalog, String areaId, String productId)
  {
    this(catalog);
    this.areaID = areaId;
    this.itemID = areaId + productId;
  }
  
  protected void finalize() throws Throwable { 
      if (log.isDebugEnabled()) {
          log.debug("Finalize WebCatItemKey: " + this);
      }
        
      parentCatalog = null;
        
      super.finalize();
  }
  
  public String getAreaID() {
    return areaID;
  }

  public String getItemID() {
    return itemID;
  }

  /**
   * This function sets the quantity if the checkQuantity returns true
   */
  public void setQuantity(String newQuantity) {
      if( checkQuantity(newQuantity, parentCatalog)){
          quantity = newQuantity;
      }
      else {
          if (log.isDebugEnabled())
              log.debug(newQuantity+" is not a valid quantity. The original one "+quantity+" remains unchanged");
      }
  }
  
  public void setQuantityWithoutCheck(String newQuantity) {
      quantity = newQuantity;
  }
  
  /**
   * This function checks if the quantity is correct, e.g. is not like 'afaj' or '0'
   */
  public static boolean checkQuantity(String newQuantity, WebCatInfo parentCatalog) {
      
       boolean quantityIsOk = false;
       
       if (newQuantity == null) {
           if (log.isDebugEnabled()) {
               log.debug("Cannot set the quantity, as the desired one is null; returning ...");
           }
                 
           return quantityIsOk;
       } 
         
       if (log.isDebugEnabled()) {
           log.debug("Setting the Quantity to: "+newQuantity);
       }
             
       String patternPref = "###,###,###,###";
       String patternSuf  = "####";
       String pattern = patternPref + "." + patternSuf;
       
       DecimalFormat format = new DecimalFormat(pattern);
       
       DecimalFormatSymbols symbs = format.getDecimalFormatSymbols();

       if (parentCatalog != null && parentCatalog.getPriceCalculator() != null &&
           parentCatalog.getPriceCalculator().getInitData() != null &&
           parentCatalog.getPriceCalculator().getInitData().getDecimalSeparator() != null &&
           parentCatalog.getPriceCalculator().getInitData().getGroupingSeparator() != null) {
           if (parentCatalog.getPriceCalculator().getInitData().getDecimalSeparator().length() > 0) {
               symbs.setDecimalSeparator(parentCatalog.getPriceCalculator().getInitData().getDecimalSeparator().charAt(0));
           }
           else {
               log.debug("::setQuantity() ; initData.getDecimalSeparator() is empty string. Not setting any value; it remains "+symbs.getDecimalSeparator());
           }
                     
           if (parentCatalog.getPriceCalculator().getInitData().getGroupingSeparator().length() > 0) {
               symbs.setGroupingSeparator(parentCatalog.getPriceCalculator().getInitData().getGroupingSeparator().charAt(0));
           }   
           else {
               log.debug("::setQuantity() ; initData.getGroupingSeparator() is empty string. Not setting any value; it remains "+symbs.getGroupingSeparator());
           }
       }
       
       ParsePosition parsePos = new ParsePosition(0); 

       format.setDecimalFormatSymbols(symbs); // Needs to be set, because of getDecicmalFormatSymbols() returns only a clone!
       Number no = format.parse(newQuantity.trim(), parsePos);
       if (log.isDebugEnabled()) {
           if (no == null) {
               log.debug(":: 'no' value is 'null'");
           } else {
               log.debug(":: 'no' double value is '" + no.doubleValue() + "'");
           }
           log.debug(":: format.toPattern() = '" + format.toPattern() + "'");
           log.debug(":: parsePos.getIndex() = '" + parsePos.getIndex() + "'");
           log.debug(":: newQuantity.trim().length() = '" + newQuantity.trim().length() + "'");
       }
       if (no != null && no.doubleValue() > 0 && parsePos.getIndex() == newQuantity.trim().length()) {
           quantityIsOk = true;
       } 
       
       return quantityIsOk;
  }

  public String getQuantity() {
    return quantity;
  }

  public void setSelected(boolean newSelected) {
    selected = newSelected;
  }

  public boolean isSelected() {
    return selected;
  }

  public WebCatInfo getParentCatalog() {
    return parentCatalog;
  }
  
  /**
   * Return a String representation of the object
   */
  public String toString() {
        
      log.entering("toString()");
        
      String retVal = super.toString(); 
      retVal = retVal  +" areaID=[" +  areaID + "]"; 
      retVal = retVal  +", itemID=[" + itemID + "]";
      retVal = retVal  +", selected=[" + selected + "]";
      retVal = retVal  +", quantity=[" + quantity + "]";
      retVal = retVal  +", hashCode=[" + hashCode() + "]";
                 
      log.exiting();

      return retVal;
  }

}
