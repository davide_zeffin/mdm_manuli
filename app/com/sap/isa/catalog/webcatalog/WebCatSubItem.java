/*****************************************************************************

    Class:        WebCatSubItem
    Copyright (c) 2006, SAP AG, Germany, All rights reserved.
    Created:      03.02.2006

*****************************************************************************/
package com.sap.isa.catalog.webcatalog;

import com.sap.isa.backend.boi.webcatalog.solutionconfigurator.SolutionConfiguratorData;
import com.sap.isa.catalog.boi.IItem;
import com.sap.isa.catalog.boi.WebCatItemData;
import com.sap.isa.catalog.boi.WebCatSubItemData;
import com.sap.isa.catalog.boi.WebCatSubItemListData;
import com.sap.isa.businessobject.ProductBaseData;
import com.sap.isa.core.TechKey;
import com.sap.isa.core.logging.IsaLocation;
import com.sap.spc.remote.client.util.imp.c_ext_cfg_imp;

/**
 * Represents an subitem in a <code>WebCatSubItemList</code>.<br>
 *  
 * Extends the <code>WebCatItem</code> that is used in a <code>WebCatItemList</code> by subitem
 * specific data. It is managed by the <code>WebCatSubItemList</code>, it belongs to.
 * 
 */
public class WebCatSubItem extends WebCatItem implements WebCatSubItemData {

    private final static IsaLocation log = IsaLocation.getInstance(WebCatSubItem.class.getName());

    /**
     * This attribute will be used to store the configuration coming from the Solution
     * Configurator and will be used in the PriceCalculator during creation of the IPCItems.
     */
    protected c_ext_cfg_imp extConfig = null;

    /**
     * Contains the id of the group the item belongs to, in case it is part of an option
     * group. All items with the same groupKey make up an option group.
     */
    protected String groupKey = null;

    /**
     * This flag is set to true, if the item is selected by the solution configurator.
     */
    protected boolean isScSelected = false;

    /**
     * This flag is set to true, if the item is part of a mandatory group.
     */
    protected boolean isPartOfMandatoryGroup = false;

    /**
     * This flag is set to true, if the item is part of a Default group.
     */
    protected boolean isPartOfDefaultGroup = false;

    /**
     * This boolean flag indicates, if the item is optional in the sense, that it can
     * be selected but must not be selected.
     * 
     * @see     #isOptional()
     * @see     #setIsOptional(boolean)
     */
    protected boolean isOptional = false;

    /**
     * The parent <code>WebCatItem</code> of this <code>WebCatSubItem</code>.
     * 
     * @see     #setParentItem(WebCatItem)
     * @see     #getParentItem()
     */
    protected WebCatItem parentItem = null;

    /**
     * The selection status for the solution configurator explosion.<br>
     * 
     * The default value for this is <code>NOT_SELECTED_FOR_EXP</code>. For the allowed values see 
     * {@link #setSelectedForRes(String)}.
     *  
     * @see #getSelectedForRes() 
     */
    protected String author = NOT_SELECTED_FOR_EXP;

    /**
     * The sort number of the item for the solution configurator explosion.<br>
     * 
     */
    protected String sortNo = "";

    /**
     * The <code>WebCatSubItemList</code> this <code>WebCatSubItem</code> belongs to.
     */
    protected WebCatSubItemList parentWebCatSubItemList = null;

    /**
     * The Solution Configurator item type
     * 
     * @see     #setParentItem(WebCatItem)
     * @see     #getParentItem()
     */
    protected String scItemType = SC_ITEM_TYPE_UNDEFINED;

    /**
     * Constructor for an empty <code>WebCatSubItem</code> is not allowed.<br>
     *  
     * To support a soft error handling in the <code>WebCatSubItemList</code> it is necessary to support
     * "empty" subitems. They will be used as placeholder in the <code>WebCatSubItemList</code> if there
     * is no valid product information in the replicated product catalog.
     * 
     * For this purpose, create a <code>WebCatItemKey</code> and use  
     * <code>WebCatSubItem(WebCatItemKey)</code>.
     * 
     * @see     WebCatSubItemList#populateWithProducts(String[], String)
     */
    private WebCatSubItem() {
        super((WebCatItemKey) null);
    }

    /**
     * Constructor for a given <code>WebCatItemKey</code>.
     * 
     * @param itemKey a valid <code>WebCatItemKey</code>
     */
    public WebCatSubItem(WebCatItemKey itemKey) {
        super(itemKey);
    }

    /**
     * Constructor for a given <code>WebCatInfo</code> and <code>IItem</code>.
     *
     * @param newCatalog        a valid <code>WebCatInfo</code> catalog.
     * @param newCatalogItem    the data as <code>IItem</code>
     */
    public WebCatSubItem(WebCatInfo newCatalog, IItem newCatalogItem) {
        super(newCatalog, newCatalogItem);
    }

    /**
     * Returns always <code>SolutionConfiguratorData.NOT_ELIGIBLE</code>, because
     * the <code>WebCatSubItem</code> will not be exploded.
     * 
     * @return always <code>SolutionConfiguratorData.NOT_ELIGIBLE</code>
     */
    public int explode() {
        log.entering("explode()");
        log.exiting();
        return SolutionConfiguratorData.NOT_ELIGIBLE;
    }
    
    /**
     * Returns always <code>SolutionConfiguratorData.NOT_ELIGIBLE</code>, because
     * the <code>WebCatSubItem</code> will not be exploded.
     *
     * @param boolean storeScDoc boolean flag, to indicate, if the sc document used to execute the explosion 
     *                          should be kept in the backend or not. The document must be kept, if the explosion 
     *                          tree should be copied by refrence, e.g. when the main item is added to the basket.
     * 
     * @return always <code>SolutionConfiguratorData.NOT_ELIGIBLE</code>
     */
    public int explode(boolean storeScDoc)  {
        log.entering("explode(boolean storeScDoc)");
        log.exiting();
        return SolutionConfiguratorData.NOT_ELIGIBLE;
    }

    /**
     * Returns the product description.
     * 
     * @return description of the product of the <code>WebCatSubItem</code>
     */
    public String getDescription() {

        // A WebCatInfo necessary to call the getDescription() without errors.
        if (null == getWebCatInfo()) {
            return null;
        }

        return super.getDescription();
    }

    /**
     * Returns the group key.
     * 
     * @return group key as <code>String</code>
     * 
     * @see     #setGroupKey(String)
     */
    public String getGroupKey() {
        return groupKey;
    }

    /**
     * Returns the parent item of this <code>WebCatSubItem</code>.
     * Method implements ProductBaseData.
     * 
     * @return parent product of type ProductBaseData 
     */
    public ProductBaseData getParent() {
        String method = "getParent()";
        log.entering(method);
        if (parentItem != null) {
            if (log.isDebugEnabled()) {
                log.debug(method + ": parentItem.ID = " + parentItem.getId());
            }
        }
        log.exiting();
        return parentItem;
    }

    /**
     * Returns the parent item of this <code>WebCatSubItem</code>.
     * 
     * @return parent item
     */
    public WebCatItemData getParentItemData() {
        return parentItem;
    }

    /**
     * Returns the parent item of this <code>WebCatSubItem</code>.
     * 
     * @return parent item of type WebCatItem
     */
    public WebCatItem getParentItem() {
        String method = "getParentItem()";
        log.entering(method);
        if (log.isDebugEnabled()) {
            log.debug(method + ": null");
        }
        log.exiting();
        return parentItem;
    }

    /**
     * Returns the hierarchy level of a WebCatSubItem.
     * 
     */
    public int getHierarchyLevel() {

        if (getParentTechKey() == null) {
            return (0);
        }
        else {
            return (1 + parentItem.getHierarchyLevel());
        }
    }

    /**
     * Returns the selection status of this <code>WebCatSubItem</code>.<p>
     * 
     * The selection status controls, which items are transferred to the solution 
     * configurator explosion. The solution configurator needs to know, which items are
     * currently selected, but also which items have been deselected.<br>
     * 
     * The method returns one of the following values: {@link WebCatSubItemData#SELECTED_FOR_EXP}, 
     * {@link WebCatSubItemData#DESELECTED_FOR_EXP} or {@link WebCatSubItemData#NOT_SELECTED_FOR_EXP}.
     * 
     * @return the current selection status for this item as <code>String</code>
     * 
     * @see     #setAuthor(String)
     */
    public String getAuthor() {
        final String METHOD = "getAuthor()";
        log.entering(METHOD);
        return this.author;
    }

    /**
     * Returns the top item in the parentItem hierarchy for the current item.
     * This means, if the parentItem is empty, the item will return itself. If not
     * it will call the same method on his parent, thus returning the root item of the
     * hierarchy.
     * 
     * @return top parent item or self
     */
    public WebCatItemData getTopParentItemData() {

        final String METHOD_NAME = "getTopParentItemData()";
        log.entering(METHOD_NAME);

        if (null == parentItem) {
            log.exiting();
            return this;
        }

        log.exiting();
        return parentItem.getTopParentItem();
    }

    /**
     * Returns the top item in the parentItem hierarchy for the current item.
     * This means, if the parentItem is empty, the item will return itself. If not
     * it will call the same method on his parent, thus returning the root item of the
     * hierarchy.
     * 
     * @return top parent item or self
     */
    public WebCatItem getTopParentItem() {

        final String METHOD_NAME = "getTopParentItem()";
        log.entering(METHOD_NAME);

        if (null == parentItem) {
            log.exiting();
            return this;
        }

        log.exiting();
        return parentItem.getTopParentItem();
    }

    /**
     * Returns the information, if this <code>WebCatSubItem</code> is a selected by the 
     * solution configurator.
     * The Method implements ProductBaseData.
     * 
     * @return <code>true</code>, if the item is selected by the solution configurator
     *         <code>false</code>, if not
     * 
     * @see     #setIsScSelected(boolean)
     * @see     #setGroupKey(String)
     */
    public boolean isScSelected() {
        return isScSelected;
    }

    /**
     * Returns the information, if this <code>WebCatSubItem</code> is optional in the sense, 
     * that it can be selected but must not be selected.
     * Method implements ProductBaseData.
     * 
     * @return <code>true</code>, if the item is optional.
     *         <code>false</code>, if not
     */
    public boolean isOptional() {
        final String METHOD = "isOptional()";
        log.entering(METHOD);
        if (log.isDebugEnabled()) {
            log.debug(METHOD + ": isOptional= " + isOptional);
        }
        log.exiting();
        return isOptional;
    }

    /**
     * Returns the information, if this <code>WebCatItem</code> is part of a group. 
     * Method implements ProductBaseData.
     * 
     * @return <code>false</code>
     */
    public boolean isPartOfAGroup() {
        final String METHOD = "isPartOfAGroup()";
        log.entering(METHOD);
        boolean retVal = false;

        if (getGroupKey() != null) {
            if (getGroupKey().length() > 0) {
                retVal = true;
            }
        }
        if (log.isDebugEnabled()) {
            log.debug(METHOD + ": return= " + retVal);
        }
        log.exiting();
        return retVal;
    }

    /**
     * Returns the information, if this <code>WebCatSubItem</code> is a part of a default group. 
     * 
     * @return <code>true</code>, if the item is part of a default group
     *         <code>false</code>, if not
     * 
     * @see     #isPartOfDefaultGroup(boolean)
     */
    public boolean isPartOfDefaultGroup() {
        log.entering("isPartOfDefaultGroup()");
        log.exiting();
        return isPartOfDefaultGroup;
    }

    /**
     * Returns the information, if this <code>WebCatSubItem</code> is a part of a mandatory group. 
     * 
     * @return <code>true</code>, if the item is part of a mandatory group
     *         <code>false</code>, if not
     * 
     * @see     #setIsPartOfMandatoryGroup(boolean)
     */
    public boolean isPartOfMandatoryGroup() {
        log.entering("isPartOfMandatoryGroup()");
        log.exiting();
        return this.isPartOfMandatoryGroup;
    }

    /**
     * Sets the identifier of the group the item belongs to, in case it is part of an option
     * group. All items with the same <code>groupKey</code> make up an option group.
     * 
     * @param groupKey a <code>String</code> that is an identifier for the group.
     */
    public void setGroupKey(String groupKey) {
        this.groupKey = groupKey;
    }

    /**
     * Sets the indicator, that a <code>WebCatSubItem</code> is selected by the 
     * solution configurator.
     * 
     * @param isScSelected a <code>boolean</code> value that is set to <code>true</code> if
     *                       the <code>WebCatSubItem</code> is selected by the solution configurator.
     * 
     * @see #setGroupKey(String)
     * @see #isScSelected()
     */
    public void setIsScSelected(boolean isScSelected) {
        this.isScSelected = isScSelected;
    }

    /**
     * Sets the indicator, if this <code>WebCatSubItem</code> is optional in the sense, that it 
     * can be selected but must not be selected.
     * 
     * @param isOptional a <code>boolean</code> value that is set to <code>true</code> if the
     *                   <code>WebCatSubItem</code> is optional.
     */
    public void setIsOptional(boolean isOptional) {
        this.isOptional = isOptional;
    }

    /**
     * Sets the indicator, if this <code>WebCatSubItem</code> is part of a default group.
     * 
     * @param isPartOfMandatoryGroup a <code>boolean</code> value that is set to <code>true</code> if the
     *                   <code>WebCatSubItem</code> is part of a default group.
     * 
     * @see     #setIsPartOfDefaultGroup()
     *
     */
    public void setIsPartOfDefaultGroup(boolean isPartOfDefaultGroup) {
        log.entering("setIsPartOfDefaultGroup()");
        this.isPartOfDefaultGroup = isPartOfDefaultGroup;
        log.exiting();
    }

    /**
     * Sets the indicator, if this <code>WebCatSubItem</code> is part of a mandatory group.
     * 
     * @param isPartOfMandatoryGroup a <code>boolean</code> value that is set to <code>true</code> if the
     *                   <code>WebCatSubItem</code> is part of a mandatory group.
     * 
     * @see     #isPartOfMandatoryGroup()
     *
     */
    public void setIsPartOfMandatoryGroup(boolean isPartOfMandatoryGroup) {
        this.isPartOfMandatoryGroup = isPartOfMandatoryGroup;
    }

    /**
     * Sets the parent <code>WebCatItem</code> of this <code>WebCatSubItem</code>.
     * 
     * @param parentItem the parent <code>WebCatItem</code>
     */
    public void setParentItem(WebCatItem parentItem) {

        final String METHOD_NAME = "setParentItem(WebCatItem parentItem)";
        log.entering(METHOD_NAME);

        if (parentItem == this) {
            if (log.isDebugEnabled()) {
                log.debug(this.toString());
            }
            IllegalArgumentException ex = new IllegalArgumentException(METHOD_NAME + " WebCatSubItem cannot be its own parent.");
            log.throwing(ex);
            throw ex;
        }

        this.parentItem = parentItem;

        log.exiting();
    }

    /**
     * Sets the parent <code>WebCatItem</code> of this <code>WebCatSubItem</code>.
     * 
     * @param parentItem the parent <code>WebCatItem</code>
     */
    public void setParentItem(WebCatItemData parentItem) {
        setParentItem((WebCatItem) parentItem);
    }

    /**
     * Sets the selection status of this <code>WebCatSubItem</code>.<p>
     * 
     * The selection status is used to control, which items are transferred to the solution 
     * configurator explosion. It is necessary to transfer the information, which items are
     * currently selected, but also which items have been deselected. The allowed values for 
     * <code>author</code> are {@link WebCatSubItemData#SELECTED_FOR_EXP}, 
     * {@link WebCatSubItemData#DESELECTED_FOR_EXP} and {@link WebCatSubItemData#NOT_SELECTED_FOR_EXP}.
     * <p>
     * If an invalid value for <code>author</code> is provided, this will be ignored, 
     * that means the status remains as it was before.
     * 
     * @param author selection status as <code>String</code>
     * 
     * @see     #getAuthor()
     */
    public void setAuthor(String selectedForRes) {

        final String METHOD_NAME = "setAuthor(String selectedForRes)";
        log.entering(METHOD_NAME);

        if (isRatePlanCombination()) {
            log.debug("Author flag not changed for a Rate Plan Combination!");
            log.exiting();
            return;
        }

        // Ignore wrong input
        if (null == selectedForRes) {
            log.debug("Input parameter selectedForRes is null, call ignored!");
            log.exiting();
            return;
        }

        // Verify input data
        if (!selectedForRes.equals(WebCatSubItemData.SELECTED_FOR_EXP)
            && !selectedForRes.equals(WebCatSubItemData.DESELECTED_FOR_EXP)
            && !selectedForRes.equals(WebCatSubItemData.NOT_SELECTED_FOR_EXP)
            && !selectedForRes.equals(WebCatSubItemData.GROUP_DESELECTED_FOR_EXP)) {

            if (log.isDebugEnabled()) {
                log.debug("The value " + selectedForRes + " is not allowed!");
            }
            log.exiting();
            return;
        }

        this.author = selectedForRes;
        log.exiting();
    }

    /**
     * Sets the <code>techKey</code> of this <code>WebCatSubItem</code> and notifies its
     * <code>WebCatSubItemList</code> about the change of this.
     * 
     * @param   techKey is the new <code>techKey</code> for this item.
     */
    public void setTechKey(TechKey techKey) {

        // Change the techKey
        super.setTechKey(techKey);

        // Notify the subitemlist about the change, if there is one
        if (parentWebCatSubItemList != null) {
            parentWebCatSubItemList.notifyWebCatSubItemTechKeyChanged(this, techKey);
        }
    }

    /**
     * A <code>WebCatSubItem</code> cannot have a <code>WebCatSubItemList</code>.<br>
     * 
     * Therefore, this method throws always a <code>UnsupportedOperationException</code>.
     * 
     * @param webCatSubItemList
     * 
     * @throws UnsupportedOperationException 
     */
    public void setWebCatSubItemList(WebCatSubItemList webCatSubItemList) {

        final String METHOD_NAME = "setWebCatSubItemList(WebCatSubItemList webCatSubItemList)";

        UnsupportedOperationException ex = new UnsupportedOperationException(METHOD_NAME + " is not supported for WebCatSubItem.");
        log.throwing(ex);
        throw ex;
    }

    /**
     * A <code>WebCatSubItem</code> cannot have a <code>WebCatSubItemList</code>.<br>
     * 
     * Therefore, this method throws always a <code>UnsupportedOperationException</code>.
     * 
     * @param webCatSubItemList
     * 
     * @throws UnsupportedOperationException 
     */
    public void setWebCatSubItemListData(WebCatSubItemListData webCatSubItemListData) {

        final String METHOD_NAME = "setWebCatSubItemListData(WebCatSubItemListData webCatSubItemListData)";

        UnsupportedOperationException ex = new UnsupportedOperationException(METHOD_NAME + " is not supported for WebCatSubItem.");
        log.throwing(ex);
        throw ex;
    }

    /**
     * @return
     */
    public WebCatSubItemList getParentWebCatSubItemList() {
        return parentWebCatSubItemList;
    }

    /**
     * @param list
     */
    public void setParentWebCatSubItemList(WebCatSubItemList list) {
        parentWebCatSubItemList = list;
    }

    /**
     * Returns the Solution Configurator item type
     * 
     * @return String the Solution configurator item type
     */
    public String getScItemType() {
        return scItemType;
    }

    /**
     * Sets the Solution Configurator item type
     * 
     * @param scItemType the Solution configurator item type
     */
    public void setScItemType(String scItemType) {
        this.scItemType = scItemType;
    }

    /**
     * Returns the information if a product is selected for solution configurator.
     * The method implements ProductBaseData.
     *
     * @return retVal of type boolean contains, if product is selected the value true 
     *         will be returned.
     */
    public boolean getAuthorFlag() {
        String method = "getAuthorFlag()";
        log.entering(method);

        boolean retVal = getAuthor().equals(WebCatSubItemData.SELECTED_FOR_EXP);

        if (log.isDebugEnabled()) {
            log.debug(method + ": retVal = " + retVal);
        }
        log.exiting();
        return retVal;
    }

    /**
     * Sets the selection flag for solution configurator
     * The method implements ProductBaseData
     *
     * @param selectFlag of type boolean
     */
    public void setAuthorFlag(boolean selectFlag) {
        String method = "setAuthorFlag()";
        log.entering(method);

        if (selectFlag) {
            setAuthor(WebCatSubItemData.SELECTED_FOR_EXP);
        }
        else {
            setAuthor(WebCatSubItemData.DESELECTED_FOR_EXP);
        }

        if (log.isDebugEnabled()) {
            log.debug(method + ": author = " + this.author);
        }
        log.exiting();
    }

    /**
     * Return a String representation of the object
     */
    public String toString() {

        log.entering("toString()");

        String retVal;

        retVal =
            super.toString()
                + " groupKey=["
                + groupKey
                + "]"
                + ", isScSelected=["
                + isScSelected
                + "]"
                + ", isOptional=["
                + isOptional
                + "]"
                + ", author=["
                + author
                + "]"
                + ", scItemType=["
                + scItemType
                + "]"
                + ", parentWebCatSubItemList=["
                + parentWebCatSubItemList
                + "]";
        if (parentItem != null) {
            retVal =
                retVal
                    + ", parentItem=[product="
                    + parentItem.getProduct()
                    + " itemKey="
                    + parentItem.getItemKey()
                    + " TechKey="
                    + parentItem.getTechKey()
                    + " HashCode="
                    + parentItem.hashCode()
                    + "]";
        }
        else {
            retVal = retVal + ", parentItem=[null]";
        }

        log.exiting();

        return retVal;
    }

    /**
     * Sets the sort number of the item for the solution configurator
     * Method implements WebCatSubItemData 
     * 
     * @param sortNr as <code>String</code> that is a sort number of sub item.
     */
    public void setSortNo(String sortNo) {
        this.sortNo = sortNo;
    }

    /**
     * Returns the sort number of the item for the solution configurator
     * Method implements WebCatSubItemData 
     * 
     * @return sortNr as <code>String</code> that is a sort number of sub item.
     */
    public String getSortNo() {
        return this.sortNo; 
    }

}