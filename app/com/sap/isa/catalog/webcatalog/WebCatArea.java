package com.sap.isa.catalog.webcatalog;

import java.util.ArrayList;
import java.util.Iterator;

import com.sap.isa.backend.boi.webcatalog.views.ViewRelevant;
import com.sap.isa.catalog.AttributeKeyConstants;
import com.sap.isa.catalog.boi.CatalogException;
import com.sap.isa.catalog.boi.ICategory;
import com.sap.isa.catalog.boi.IDetail;
import com.sap.isa.core.logging.IsaLocation;

/**
 * Title:
 * Description:
 * Copyright:    Copyright (c) 2001
 * Company:
 * @author
 * @version 1.0
 */

public class WebCatArea extends WebCatBusinessObjectBase implements ViewRelevant {

    /**
     * The parent area
     */
    protected WebCatArea parentArea;
    /**
     * List of children areas
     */
    private WebCatAreaList children;
    /**
     * List of items in this area
     */
    private WebCatItemList itemList;
    /**
     * language independent identifier for the area
     */
    private String areaID;
    /**
     * language dependent short description of the area
     */
    private String areaName;
    /**
     * reference to the catalog the area belongs to.
     */
    private WebCatInfo theCatalog;
    /**
     * corresponding ICategory of catalog API
     */
    private ICategory theCategory;

    /**
     * selection indicator which is used for example in views
     */
    private boolean selected = false;

    /**
     * identifies the root area for table of content
     */
    public final static String ROOT_AREA = "$ROOT";
    
    /**
     * constant for loyalty managemnt - reward area and buy points
     */
    public final static String AREATYPE_REGULAR_CATALOG = null;
    public final static String AREATYPE_LOY_REWARD_CATALOG = "LORC";
    public final static String AREATYPE_LOY_BUY_POINTS = "LOBP";

    private static IsaLocation log = IsaLocation.getInstance(WebCatArea.class.getName());

    /**
     * create new area from ID
     */
    private ArrayList path = null;
    
    /**
     * type of the areaType
     */
    private String areaType = null;

    /**
     * constructor - create new area 
     */
    public WebCatArea(WebCatInfo newCatalogInfo, String newAreaID) throws CatalogException {

        if (log.isDebugEnabled()) {
            log.debug("WebCatArea:WebCatArea(WebCatInfo " + newCatalogInfo + " , String " + newAreaID + ")");
        }
            
        theCatalog = newCatalogInfo;
        areaID = newAreaID;
        try {
            theCategory = theCatalog.getCatalog().getCategory(newAreaID);
        }
        catch (CatalogException e) {
            log.error("catalog.exception.backend", e);
            throw e;
        }
        if (theCategory != null) {
            setCategoryAttributes();
        }
        itemList = null;
    }

    /**
     * create new area from API category
     */
    public WebCatArea(WebCatInfo newCatalogInfo, ICategory newCategory) {

        if (log.isDebugEnabled()) {
            log.debug("WebCatArea:WebCatArea(WebCatInfo "
                + newCatalogInfo
                + " , ICategory "
                + newCategory
                + ")");
        }

        theCatalog = newCatalogInfo;
        theCategory = newCategory;
        if (theCategory != null) {
            areaID = theCategory.getGuid(); // ID needs to be implemented!
            setCategoryAttributes();
        }
        itemList = null; // determine later
    }

    /**
     * Sets some attributes for this area for performance reasons.
     */
    private void setCategoryAttributes() {
        try {
            areaName = theCategory.getName();
            areaType = getDetailByKey(AttributeKeyConstants.AREA_TYPE);
        }
        catch (Exception e) {
            areaType = null;
        }
    }

    /**
     * get list of children of this area.
     * @return children list of children areas for this area, as AreaList
     */
    public WebCatAreaList getChildren() throws CatalogException {

        if (children == null) {
            Iterator categories = null;
            if (theCategory != null) {
                categories = theCategory.getChildCategories();
            }
            children = new WebCatAreaList(theCatalog, this, categories);
        }

        return children;
    }

    public WebCatInfo getCatalog() {

        return theCatalog;
    }

    public boolean isParentOf(WebCatArea newArea) {

        if (log.isDebugEnabled()) {
            log.debug("WebCatArea:isParentOf(WebCatArea " + newArea + ")");
        }

        if (newArea == null) {
            return false;
        }

        if (theCategory != null) {

            if (newArea.getCategory() != null) {
                if (newArea.getCategory().getParent() == null)
                    return false;
                else
                    return theCategory.getGuid().equals(
                        newArea.getCategory().getParent().getGuid());
            }

            else if (
                (newArea.getParentArea() == null)
                    || (newArea.getParentArea().getCategory() == null))
                return false;

            else
                return theCategory.getGuid().equals(
                    newArea.getParentArea().getCategory().getGuid());

        }

        if (newArea.getParentArea() == null) {
            return false;
        }

        return equals(newArea.getParentArea());
    }

    public boolean isParentOf(String newAreaID) {

        if (log.isDebugEnabled()) {
            log.debug("WebCatArea:isParentOf(String " + newAreaID + ")");
        }

        WebCatArea newArea;
        newArea = theCatalog.getArea(newAreaID);
        return isParentOf(newArea);
    }

    /**
     * Returns the current itemList.
     * 
     * @return
     */
    public WebCatItemList getItemList() {

        if ((itemList == null) && (!getAreaID().equals(ROOT_AREA))) { // no items in index!
            itemList = new WebCatItemList(theCatalog, this);
        }

        return itemList;
    }

    /**
     * Returns the items of this list according to the <code>getAllItems</code> flag.
     * 
     * If <code>getAllItems</code> is set to <code>true</code>, a new item list is created,
     * that contains all items of the area.
     * 
     * If <code>getAllItems</code> is set to <code>false</code>, the <code>getItemList()</code>
     * is called.
     * 
     * @param getAllItems to control, which items are returned.
     * 
     * @return  itemList or null if called for the root area
     *          
     */
    public WebCatItemList getItemList(boolean getAllItems) {

        // no items in ROOT_AREA
        if (getAreaID().equals(ROOT_AREA)) {
            return null;
        }

        // call standard, if asked for
        if (!getAllItems) {
            return getItemList();
        }

        // create new itemlist
        itemList = new WebCatItemList(theCatalog, this, true);

        return itemList;
    }
    
    /**
     * Returns the items of this list according to the <code>upTo</code> attribute.
     * 
     * If <code>upTo</code> is <= 0 all the default populate() will be called
     * 
     * @param upTo number of items to be populated.
     * 
     * @return  itemList or null if called for the root area        
     */
    public WebCatItemList getItemList(int upTo) {

        // no items in ROOT_AREA
        if (getAreaID().equals(ROOT_AREA)) {
            return null;
        }
        
        if (upTo > 0) {
            itemList = new WebCatItemList(theCatalog, this, upTo);
        }

        return itemList;
    }

    public String getAreaID() {
        return areaID;
    }

    public String getAreaName() {
        return areaName;
    }

    public ICategory getCategory() {
        return theCategory;
    }

    public String getDetail(String attributeId) {

        if ((attributeId == null) || attributeId.trim().equals("") || (getCategory() == null))
            return null;

        IDetail detail = null;
        try {
            detail = getCategory().getDetail(attributeId);
        }
        catch (CatalogException e) {
        	log.debug(e.getMessage());
        }

        if (detail != null) {
            return detail.getAsString();
        }

        return null;
    }

    public String getDetailByKey(String attributeKey) {

        String attrName = getCatalog().getCatalog().getAttributeGuid(attributeKey);
        if ( ( attrName == null || attrName.trim().equals("") ) &&
		     ( attributeKey == null || !attributeKey.equals(AttributeKeyConstants.AREA_TYPE) ) ){
            log.error(
                "catalog.message.attrMap.notDef",
                new String[] { attributeKey },
                new Exception("catalog.message.attrMap.notDef"));
        }
        return getDetail(attrName);
    }

    public void setPath(ArrayList path) {
        this.path = path;
    }

    public ArrayList getPath() {

        if (path == null) {
            path = setPath();
        }

        return path;
    }

    private ArrayList setPath() {

        ArrayList tmpArray = new ArrayList();
        WebCatArea theArea = this;
        do {
            tmpArray.add(0, theArea.getAreaID());
            theArea = theArea.getParentArea();
        }
        while (theArea != null && !theArea.getAreaID().equals(WebCatArea.ROOT_AREA));
        tmpArray.add(0, "0");
        return tmpArray;
    }

    public String getPathAsString() {

        if (path == null) {
            path = setPath();
        }

        String pathStr = (String) path.get(0);
        for (int i = 1; i < path.size(); i++)
            pathStr += ("/" + (String) path.get(i));
        return pathStr;
    }

    public void setParentArea(WebCatArea parent) {
        this.parentArea = parent;
    }

    public WebCatArea getParentArea() {

        if (parentArea == null && getCategory() != null && !areaID.equals(ROOT_AREA)) {

            ICategory parentCat = this.getCategory().getParent();
            if (parentCat != null) {
                parentArea = theCatalog.getArea(parentCat);
            }
        }

        return parentArea;
    }

    public void setSelected(boolean selected) {
        this.selected = selected;
    }

    public boolean isSelected() {
        return selected;
    }

    public boolean equals(WebCatArea newArea) {
        return this.getAreaID().equals(newArea.getAreaID());
    }
    
    /**
     * Before removing this object, make sure that referenced objects are handled. This is 
     * currently done by calling the releaseReferences() method.
     */
    protected void finalize() throws Throwable {
        log.entering("finalize()");
        
        if (log.isDebugEnabled()) {
            log.debug("Finalize WebcatArea: " + this);
        }
        
        parentArea = null;
        children = null;
        itemList  = null;
        areaID = null;
        areaName = null;
        theCatalog = null;
        theCategory = null;
        path = null;
        areaType = null;
        
        super.finalize();
        
        log.exiting();
    }

    public boolean equals(Object obj) {

        WebCatArea newArea = null;
        String newAreaID = null;

        if (obj instanceof WebCatArea) {
            newArea = (WebCatArea) obj;
            return this.equals(newArea);
        }
        else if (obj instanceof String) {
            newAreaID = (String) obj;
            if (this.getAreaID().equals(newAreaID))
                return true;
            else
                return false;
        }

        return false;
    }

    /**
     * Hashcode is overwritten and returns the hashcode of the areaId
     */
    public int hashCode() {
        return getAreaID().hashCode();
    }

    public String toString() {
        return getAreaID() + " (" + getAreaName() + ")";
    }

    /**
     * Checks if the type of the category corresponds to the parameter
     * @param allowedAreaType, area type which is allowed
     * @return flag indication if the category has the correct area type 
     */
    public boolean isAreaOfType(String allowedAreaType) {
        boolean isAreaOfType = false;
        if (this.areaType == null || this.areaType.length() == 0) {
            if (allowedAreaType == null || allowedAreaType.length() == 0) {
                isAreaOfType = true;
            }
        }
        else {
            isAreaOfType = this.areaType.equals(allowedAreaType);
        }
        return (isAreaOfType);
    }

    /**
     * Checks if the type of the category is a reward catalog area
     * @return flag indication if the category is a reward catalog area 
     */
    public boolean isRewardCategory() {
        boolean isRewardCategory = AREATYPE_LOY_REWARD_CATALOG.equals(this.areaType);
        return (isRewardCategory);
    }

    /**
     * Checks if the type of the category is a buy points area
     * @return flag indication if the category is a buy points area 
     */
    public boolean isBuyPointsCategory() {
        boolean isBuyPtsCategory = AREATYPE_LOY_BUY_POINTS.equals(this.areaType);
        return (isBuyPtsCategory);
    }

    /**
     * Returns all values of a catalog attribute.
     *
     * @param attrName name of the requested catalog attribute
     *
     * @return values of the requested attribute as array of String; empty array (not null!) 
     *         if not available
     */
    private String[] getAttributeAllValues(String attrKey) {

        log.entering("getAttributeAllValues()");

        String attr = getDetailByKey(attrKey);

        if (attr == null) {
            log.debug("attr is null");
            log.exiting();
            return new String[0];
        }

        String values[] = attr.split(";");

        log.exiting();

        return (values);
    }

    /**
     * get type of this area.
     * @return type of this area, as String
     */
    public String getAreaType() {
        return this.areaType;
    }

}