package com.sap.isa.catalog.webcatalog;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;

import com.sap.isa.catalog.boi.ICategory;
import com.sap.isa.core.Iterable;
import com.sap.isa.core.logging.IsaLocation;

/**
 * Title:        ProductCatalog UI
 * Description:
 * Copyright:    Copyright (c) 2001
 * Company:      SAPMarkets Europe GmbH
 * @version 1.0
 */

public class WebCatAreaList extends WebCatBusinessObjectBase implements Iterable {

  private ArrayList      areas;
  private WebCatArea     parentArea;
  private WebCatAreaPage currentAreaPage;
  private static IsaLocation log =
      IsaLocation.getInstance(WebCatAreaList.class.getName());

  public WebCatAreaList(WebCatArea parent) {
	if (log.isDebugEnabled())
    	log.debug("WebCatAreaList:WebCatAreaList(WebCatArea "+parent+")");
    parentArea = parent;
    areas = new ArrayList();
    currentAreaPage = null;
  }

  public WebCatAreaList(WebCatArea parent, ArrayList newAreas) {
    this(parent);
	if (log.isDebugEnabled())
    	log.debug("WebCatAreaList:WebCatAreaList(WebCatArea "+parent+", ArrayList "+newAreas+")");
    areas = newAreas;
  }

  public WebCatAreaList(WebCatInfo theCatalog, WebCatArea parent, Iterator categories) {
    this(parent);
	if (log.isDebugEnabled())
    	log.debug("WebCatAreaList:WebCatAreaList(WebCatInfo "+theCatalog+", WebCatArea "+parent+", Iterator "+categories+")");
    if (theCatalog instanceof WebCatInfoModifiable)
    {
      WebCatInfoModifiable modCatalog = (WebCatInfoModifiable) theCatalog;
      String parentId = WebCatArea.ROOT_AREA;
      if (parent != null)
        parentId = parent.getAreaID();
      WebCatAreaModifiable[] newAreas = modCatalog.getNewAreas(parentId);
      for (int i=0; i<newAreas.length; i++)
      {
        add(newAreas[i]);
      }
    }
    if (categories != null)
    {
      while (categories.hasNext())
      {
        WebCatArea newArea = theCatalog.getArea((ICategory) categories.next());
        // for modifiable catalog: if selected area is deleted: do not display
        if (newArea instanceof WebCatAreaModifiable)
        {
          WebCatAreaModifiable newAreaMod = (WebCatAreaModifiable) newArea;
          if ((newAreaMod.getModificationStatus() == WebCatModificationStatus.DELETED_OBJECT) ||
              ((newAreaMod.getModificationStatus() == WebCatModificationStatus.CHANGED_OBJECT) &&
               (newAreaMod.getModificationData().isParentAreaChanged())))
          continue;
        }
        // for modifiable catalog: if selected parent differs from parent
        // determined for new area, this means area has moved, so do not
        // display.
        if (newArea.getParentArea() == null)
        {
          if (parent == null)
            areas.add(newArea);
        }
        else if (parent == null || newArea.getParentArea().equals(parent))
          areas.add(newArea);
      }
    }
  }
  
  protected void finalize() throws Throwable { 
      if (log.isDebugEnabled()) {
          log.debug("Finalize WebCatAreaList: " + this);
      }
        
      parentArea = null;
      currentAreaPage = null;
      areas = null;
        
      super.finalize();
  }

  public WebCatArea getParentArea() {
    return parentArea;
  }

  public void add(WebCatArea newArea) {
	if (log.isDebugEnabled())
    	log.debug("WebCatAreaList:add area "+newArea);
    newArea.setParentArea(parentArea);
    if (areas == null)
      areas = new ArrayList();
    areas.add(newArea);
  }

  public ArrayList getAreas() {
    return areas;
  }

  public WebCatArea getArea(int index) {
	if (log.isDebugEnabled())
    	log.debug("WebCatAreaList:getArea(int "+index+")");
    return (WebCatArea) areas.get(index);
  }

  public int size() {
    log.debug("WebCatAreaList:size()");
    if (areas != null)
      return areas.size();
    else
      return 0;
  }

  public Iterator iterator() {
	if (log.isDebugEnabled())
    	log.debug("WebCatAreaList:iterator()");
    if (areas != null)
      return areas.iterator();
    else
      return null;
  }

  public WebCatAreaPage getAreaPage(String page) {
	if (log.isDebugEnabled())
    	log.debug("WebCatAreaList:getAreaPage(String "+page+")");
    WebCatAreaPage areaPage = new WebCatAreaPage(this, page);
    currentAreaPage = areaPage;
    return areaPage;
  }

  public WebCatAreaPage getCurrentAreaPage() {
    return currentAreaPage;
  }

  public int indexOf(Object obj) {
    if (areas == null)
      return -1;
    else
      return areas.indexOf(obj);
  }

  public void sort(String attribute) {
	if (log.isDebugEnabled())
    	log.debug("WebCatAreaList:sort(String "+attribute+")");
    if (areas != null)
      Collections.sort(areas, new WebCatAreaComparator(attribute));
  }

}
