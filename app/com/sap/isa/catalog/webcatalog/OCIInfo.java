package com.sap.isa.catalog.webcatalog;

/**
 * Title:        OCI Info Object
 * Description:  Stores parameters provided by the caller when catalog is
 *               called from procurement System (EBP) using OCI (open catalog interface)
 * Copyright:    Copyright (c) 2001
 * Company:      SAPMarkets
 * @author       RFU
 * @version 1.0
 */

import com.sap.isa.core.logging.IsaLocation;

public class OCIInfo{

  protected static IsaLocation log =
             IsaLocation.getInstance(OCIInfo.class.getName());

  /**
   * OCI_PCAT open catalog interface scenario, only product catalog to be accessed from procurement.
   */
  public final static String OCI_PCAT = "OCI_PCAT";
  /**
   * OCI_ISA open catalog interface scenario, whole internet sales (b2b) accessed from procurement.
   */
  public final static String OCI_ISA  = "OCI_ISA";
  private String hookURL          = "";
  private String fullscreenURL    = null; // null means not provided!
  private String basketTarget     = "";
  private String catalogTarget    = "";
  private String ociVersion       = "";
  private String ociScenario      = "";
  private String ebpCatalogId     = "";
  private String ebpUser          = "";
  private String ebpPo            = "";
  private String stylesheet       = "";

  public OCIInfo() {
	if (log.isDebugEnabled())
    	log.debug("OCIInfo:OCIInfo()");
  }

  /**
   * Stores the hook URL parameter of the request which gives the URL of the calling
   * system. This URL is needed to return and to post items to callers basket.
   */
  public void setHookURL(String newHookURL) {
	if (log.isDebugEnabled())
    	log.debug("OCIInfo:setHookURL(String "+newHookURL+")");
    if (newHookURL != null)
       hookURL = newHookURL;
  }

  /**
   * @return Hook URL (URL of calling system)
   */
  public String getHookURL() {
	if (log.isDebugEnabled())
    	log.debug("OCIInfo:getHookURL() "+hookURL);
    return hookURL;
  }

  /**
   * Stores the fullscreen URL parameter of the request which gives the URL
   * to jump to a fullscreen version of the catalog, provided by calling
   * system. This URL is needed to return and to post items to callers basket.
   */
  public void setFullscreenURL(String fullscreenURL) {
	if (log.isDebugEnabled())
    	log.debug("OCIInfo:setFullscreenURL(String "+fullscreenURL+")");
    if (fullscreenURL != null)
       this.fullscreenURL = fullscreenURL;
  }

  /**
   * @return fullscreen URL (URL to jump to a fullscreen version of catalog)
   */
  public String getFullscreenURL() {
	if (log.isDebugEnabled())
    	log.debug("OCIInfo:getFullscreenURL() "+fullscreenURL);
    return fullscreenURL;
  }

  /**
   * Stores target frame of OCI callers shopping basket.
   * If items are added to basket via OCI, this is the frame the callers basket is
   * displayed in. When posting items this is the target of the form.
   */
  public void setBasketTarget(String newBasketTarget) {
	if (log.isDebugEnabled())
    	log.debug("OCIInfo:setBasketTarget(String "+newBasketTarget+")");
    if (newBasketTarget != null)
       basketTarget = newBasketTarget;
  }

  /**
   * @return frame target which is used by OCI calling basket
   */
  public String getBasketTarget() {
	if (log.isDebugEnabled())
    	log.debug("OCIInfo:getBasketTarget() "+basketTarget);
    return basketTarget;
  }

  /**
   * Stores target frame of catalog within OCI scenario.
   * When catalog is to be displayed in-place within procuremnt UI this is the
   * name of the frame the catalog is displayed in.
   */
  public void setCatalogTarget(String newCatalogTarget) {
	if (log.isDebugEnabled())
    	log.debug("OCIInfo:setCatalogTarget(String "+newCatalogTarget+")");
    if (newCatalogTarget != null)
       catalogTarget = newCatalogTarget;
  }

  /**
   * @return frame target which is used for catalog.
   */
  public String getCatalogTarget() {
	if (log.isDebugEnabled())
    	log.debug("OCIInfo:getCatalogTarget() "+catalogTarget);
    return catalogTarget;
  }

  /**
   * Stores OCI version, needed to determine return format
   * @param version Version of open catalog interface
   */
  public void setOciVersion(String newOciVersion) {
	if (log.isDebugEnabled())
    	log.debug("OCIInfo:setOciVersion(String "+newOciVersion+")");
    if (newOciVersion != null)
       ociVersion = newOciVersion;
  }

  /**
   * @return Version of open catalog interface requested by caller
   */
  public String getOciVersion() {
	if (log.isDebugEnabled())
    	log.debug("OCIInfo:getOciVersion() "+ociVersion);
    return ociVersion;
  }

  /**
   * Stores information whether only catalog was accessed from Procurement
   * or Internet Sales
   * @param ociScenario Scenario, according to constants
   */
  public void setOciScenario(String newOciScenario) {
	if (log.isDebugEnabled())
    	log.debug("OCIInfo:setOciScenario(String "+newOciScenario+")");
    if (newOciScenario != null)
       ociScenario = newOciScenario;
  }

  /**
   * Get scenario information.
   * @return Scenario, according to constants
   */
  public String getOciScenario() {
	if (log.isDebugEnabled())
    	log.debug("OCIInfo:getOciScenario() "+ociScenario);
    return ociScenario;
  }

  public void setEbpCatalogId(String ebpCatalogId) {
    if (ebpCatalogId != null)
      this.ebpCatalogId = ebpCatalogId;
  }
  public String getEbpCatalogId() {
      return ebpCatalogId;
  }

  public void setEbpUser(String ebpUser) {
    if (ebpUser != null)
      this.ebpUser = ebpUser;
  }
  public String getEbpUser() {
      return ebpUser;
  }

  public void setEbpPo(String ebpPo) {
    if (ebpPo != null)
      this.ebpPo = ebpPo;
  }
  public String getEbpPo() {
      return ebpPo;
  }

  public void setStylesheet(String stylesheet) {
      this.stylesheet = stylesheet;
  }

  public String getStylesheet() {
      return stylesheet;
  }
}
