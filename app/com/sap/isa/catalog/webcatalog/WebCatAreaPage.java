package com.sap.isa.catalog.webcatalog;

import java.util.ArrayList;
import java.util.Iterator;

import com.sap.isa.core.Iterable;
import com.sap.isa.core.logging.IsaLocation;

/**
 * Title:        ProductCatalog UI
 * Description:  Includes one page of categories. A page is determined by
 *               the letter the category name starts with!
 * Copyright:    Copyright (c) 2001
 * Company:      SAPMarkets Europe GmbH
 * @version 1.0
 */

public class WebCatAreaPage extends WebCatBusinessObjectBase implements Iterable {

  /**
   * The AreaList this page is created from
   */
  private WebCatAreaList areaList;
  /**
   * The letter the areas of this page start with
   */
  private String         page;
  /**
   * The List of areas of this page
   */
  private ArrayList      areas;
  private static IsaLocation log = IsaLocation.getInstance(WebCatAreaPage.class.getName());

  /**
   * create a page from a list of areas
   * @param areaList List of areas from which the page is derived
   * @param page     the letter the areas of this page have to start with.
   *                 if page is empty all areas of the list are in the page.
   */

  public WebCatAreaPage(WebCatAreaList areaList, String page) {
	if (log.isDebugEnabled())
    	log.debug("WebCatAreaPage:WebCatAreaPage(WebCatAreaList "+areaList+", String "+page+")");
    this.areaList   = areaList;
    this.page       = page;
    if (page.trim().equals(""))
    {
      areas = areaList.getAreas();
    }
    else
    {
      areas = new ArrayList();
      Iterator areaIterator = areaList.iterator();
      while (areaIterator.hasNext())
      {
        WebCatArea area = (WebCatArea) areaIterator.next();
        if (area.getAreaName().startsWith(page))
        {
          areas.add(area);
        }
      }
    }
  }
  
  protected void finalize() throws Throwable { 
      if (log.isDebugEnabled()) {
          log.debug("Finalize WebCatAreaPage: " + this);
      }
        
      areaList = null;
      areas = null;
        
      super.finalize();
  }

  /**
   * create page containing all areas of the list
   */
  public WebCatAreaPage(WebCatAreaList areaList) {
    this(areaList, "");
	if (log.isDebugEnabled())
    	log.debug("WebCatAreaPage:WebCatAreaPage(WebCatAreaList "+areaList+")");
  }

  /**
   * Get list of areas of this page
   * @return areas list of areas of this page
   */
  public ArrayList getAreas() {
    return areas;
  }

  /**
   * Get list of areas of this page, which can be displayed,
   * i.e. whose area.getAreaID() != "0"
   * @return areas list of areas of this page, without the area for which (area.getAreaID() == "0")
   */
  public ArrayList getDisplayableAreas() {
    ArrayList displayableAreas = new ArrayList();
    for (int i=0;i<areas.size();i++) {
        WebCatArea area = (WebCatArea)areas.get(i);
        if (!area.getAreaID().equals("0"))
          displayableAreas.add(area);
    }
    return displayableAreas;
  }
  /**
   * Get areaList this page was created from
   */
  public WebCatAreaList getAreaList() {
    return areaList;
  }

  /**
   * Get page identifier, i.e. the letter all areas of this page start with
   */
  public String getPage() {
	if (log.isDebugEnabled())
    	log.debug("WebCatAreaPage:getPage() "+page);
    return page;
  }

  /**
   * Get area by index
   * @param index int number of area to return
   */
  public WebCatArea getArea(int index) {
	if (log.isDebugEnabled())
    	log.debug("WebCatAreaPage:getArea(int "+index+")");
    return (WebCatArea) areas.get(index);
  }

  /**
   * get number of items on this distinct page
   * @return size number of items on this page, can be less than pageSize if this
   *         is the last page.
   */
  public int size() {
	if (log.isDebugEnabled())
    	log.debug("WebCatAreaPage:size() "+areas.size());
    return areas.size();
  }

  public Iterator iterator() {
    return areas.iterator();
  }

}
