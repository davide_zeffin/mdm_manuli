package com.sap.isa.catalog.webcatalog;

import org.apache.struts.upload.FormFile;

import com.sap.isa.backend.boi.webcatalog.sharedcatalog.WebCatModifiable;
import com.sap.isa.catalog.*;
import com.sap.isa.catalog.boi.CatalogException;
import com.sap.isa.catalog.boi.ICategory;
import com.sap.isa.core.logging.IsaLocation;

/**
 * This class represents a WebCatItem extension that can be modified (normal 
 * WebCatItem is &quot;read only&quot;).
 * This class is used in the environment of the shared manufacturer catalog.
 */
public class WebCatAreaModifiable extends WebCatArea
                                  implements WebCatModifiable
{

  private static IsaLocation log =
      IsaLocation.getInstance(WebCatAreaModifiable.class.getName());

  /**
   * flag to store the area modification status
   */
  protected WebCatModificationStatus changeIndicator;
  
  /* ------------------------- changeable fields ---------------------------- */
  
  /**
   * The id of this area (if it was changed).
   */
  protected String areaId = null;
  /**
   * The name of this area, i.e.
   * the area description (short text), if it was changed.
   */
  protected String areaName = null;

  /**
   * object to store the area modification data
   */
  protected WebCatModificationData modificationData;
  
  /**
   * Constructor for new WebCatItemModifiable, representing a newly created 
   * catalog item.
   * @param newCatalog the catalog this area is created in
   * @param areaId the id of the new area
   * @param description short text for new area
   */
  WebCatAreaModifiable(WebCatInfo newCatalog, String areaId, String description)
  {
    super(newCatalog, (ICategory) null);
    modificationData = new WebCatModificationData();
    modificationData.setModificationStatus(WebCatModificationStatus.NEW_OBJECT);
    String area = areaId.trim().toUpperCase();
    StringBuffer sBuf = new StringBuffer();
    for (int i=0; i<area.length(); i++)
    {
      char ch = area.charAt(i);
      if (((ch < 'A') || (ch > 'Z')) && ((ch < '0') || (ch > '9')) && (ch != '-')) 
        ch = '_';
      sBuf.append(ch);
    }
    modificationData.setId(sBuf.toString());
    modificationData.setDescription(description);
  }

  /**
   * Constructor for WebCatAreaModifiable.
   * @param newCatalogInfo
   * @param newCategory
   */
  WebCatAreaModifiable(WebCatInfo newCatalogInfo, ICategory newCategory)
  {
    super(newCatalogInfo, newCategory);
    modificationData = new WebCatModificationData();
    modificationData.setModificationStatus(WebCatModificationStatus.UNCHANGED_OBJECT);
    modificationData.setId(getAreaID());
    if (newCategory.getParent() != null)
      modificationData.setParentAreaId(newCategory.getParent().getGuid());
  }

  /**
   * Constructor for WebCatAreaModifiable.
   * @param newCatalogInfo
   * @param newAreaID
   * @throws CatalogException
   */
  WebCatAreaModifiable(WebCatInfo newCatalogInfo, String newAreaID)
    throws CatalogException
  {
    super(newCatalogInfo, newAreaID);
    modificationData = new WebCatModificationData();
    modificationData.setModificationStatus(WebCatModificationStatus.UNCHANGED_OBJECT);
    modificationData.setId(getAreaID());
  }

  /**
   * Constructor for WebCatAreaModifiable, representing an existing catalog
   * area that can be changed or deleted later.
   * @param area WebCatArea which is basis for this area
   */
  WebCatAreaModifiable(WebCatArea area, WebCatModificationData modData)
  {
    super(area.getCatalog(), area.getCategory());
    if (modData == null)
    {
      modificationData = new WebCatModificationData();
      modificationData.setModificationStatus(WebCatModificationStatus.UNCHANGED_OBJECT);
      modificationData.setId(getAreaID());
      if (area.getParentArea() != null)
        modificationData.setParentAreaIdOriginal(area.getParentArea().getAreaID());
      else
        modificationData.setParentAreaIdOriginal(WebCatArea.ROOT_AREA);
    }
    else
    {
      // ensure parent-redetermination!
      parentArea = null;
      modificationData = modData;
    }
  }
  
  /**
   * Method getModificationData.
   * @return WebCatModificationData
   */
  public WebCatModificationData getModificationData()
  {
    return modificationData;
  }
  
  /**
   * Method to force reading of modification data from container.
   * This is useful to ensure that allways the current version is used.
   * @return WebCatModificationData
   */
  public WebCatModificationData readModificationData()
  {
    WebCatModificationData modData = ((WebCatInfoModifiable) getCatalog())
                                     .getModifiableObjectsContainer()
                                     .getAreaData(getAreaID());
    if (modData != null)
    {
      modificationData = modData;
      parentArea = null;
    }
    return modificationData;
  }
  

  protected void setModified()
  {
    modificationData.setModified();
    ((WebCatInfoModifiable) getCatalog()).addToModifiedAreas(modificationData);
  }
  
  /**
   * Determine the modification status of this item
   * @return status modification status of this item as one of the defined 
   *                constants.
   */
  public WebCatModificationStatus getModificationStatus()
  {
    return modificationData.getModificationStatus();
  }
  
  public String getDetailByKey(String key)
  {
    if (key == AttributeKeyConstants.AREA_LONG_DESCRIPTION)
      return getLongDescription();
    else if (key == AttributeKeyConstants.AREA_IMAGE)
      return getImage();
    else if (key == AttributeKeyConstants.AREA_THUMBNAIL)
      return getThumbNail();
    else
      return super.getDetailByKey(key);
  }
  
  /**
   * Change long description for this item.
   * @param description new item long description
   */
  public void setLongDescription(String description)
  {
    modificationData.setLongDescription(description);
    setModified();
  }
  
  /**
   * Method getLongDescription.
   * @return String
   */
  public String getLongDescription()
  {
    if (modificationData.getLongDescription() == null)
      return super.getDetailByKey(AttributeKeyConstants.AREA_LONG_DESCRIPTION);
    else
      return modificationData.getLongDescription();
  }
  
  /**
   * get information whether the long description for this area has been
   * modified.
   * @return boolean true, if long description is modified, false otherwise
   */
  public boolean isLongDescriptionModified()
  {
    return (modificationData.getLongDescription() != null);
  }
  
  /**
   * Method setThumbNail.
   * @param thumb
   */
  public void setThumbNail(String thumb)
  {
    modificationData.setThumbNail(thumb);
    setModified();
  }
  
  /**
   * Method getThumbNail.
   * @return String
   */
  public String getThumbNail()
  {
    if (modificationData.getThumbNail() == null)
      return super.getDetailByKey(AttributeKeyConstants.AREA_THUMBNAIL);
    else
      return modificationData.getThumbNail();
  }
  
  /**
   * Method isThumbNailModified.
   * @return boolean
   */
  public boolean isThumbNailModified()
  {
    return (modificationData.getThumbNail() != null);
  }
  
  /**
   * Method setImage.
   * @param image
   */
  public void setImage(String image)
  {
    modificationData.setImage(image);
    setModified();
  }
  
  /**
   * Method getImage.
   * @return String
   */
  public String getImage()
  {
    if (modificationData.getImage() == null)
      return super.getDetailByKey(AttributeKeyConstants.AREA_IMAGE);
    else
      return modificationData.getImage();
  }
  
  /**
   * Method isImageModified.
   * @return boolean
   */
  public boolean isImageModified()
  {
    return (modificationData.getImage() != null);
  }

  /**
   * Method setAreaID.
   * @param areaId
   */
  public void setAreaID(String areaId)
  {
    modificationData.setId(areaId);
    setModified();
  }
  
  /**
   * @see com.sap.isa.catalog.webcatalog.WebCatItem#getAreaID()
   */
  public String getAreaID()
  {
    if (modificationData.getId() == null)
      return super.getAreaID();
    else
      return modificationData.getId();
  }
  
  /**
   * Delete this item (i.e. flag item for later deletion).
   */
  public void delete()
  {
    setModified();
    modificationData.setModificationStatus(WebCatModificationStatus.DELETED_OBJECT);
  }
  
  /**
   * Returns the areaName.
   * @return String
   */
  public String getAreaName()
  {
    if (modificationData.getDescription() == null)
      return super.getAreaName();
    else
      return modificationData.getDescription();
  }

  /**
   * Sets the areaName.
   * @param areaName The areaName to set
   */
  public void setAreaName(String description)
  {
    modificationData.setDescription(description);
  }

  /**
   * @see com.sap.isa.backend.boi.webcatalog.sharedcatalog.WebCatModifiable#addUploadDocument(java.lang.String, org.apache.struts.upload.FormFile)
   */
  public void addUploadDocument(String type, FormFile document)
  {
    if (UPLOAD_DOCUMENT_TYPE_IMAGE.equals(type))
    {
      if (modificationData.getUploadImage() != null)
      modificationData.getUploadImage().destroy();
      modificationData.setUploadImage(document);
    }
    else if (UPLOAD_DOCUMENT_TYPE_THUMB.equals(type))
    {
      if (modificationData.getUploadThumb() != null)
      modificationData.getUploadThumb().destroy();
      modificationData.setUploadThumb(document);
    }
    else
      log.warn("cat.shr.invdocupltyp",new String[] { type },null);
  }

  /**
   * @see com.sap.isa.backend.boi.webcatalog.sharedcatalog.WebCatModifiable#getUploadDocument(java.lang.String)
   */
  public FormFile getUploadDocument(String type)
  {
    if (UPLOAD_DOCUMENT_TYPE_IMAGE.equals(type))
      return modificationData.getUploadImage();
    else if (UPLOAD_DOCUMENT_TYPE_THUMB.equals(type))
      return modificationData.getUploadThumb();
    else
      return null;
  }

  /**
   * @see com.sap.isa.catalog.webcatalog.WebCatArea#setParentArea(com.sap.isa.catalog.webcatalog.WebCatArea)
   */
  public void setParentArea(WebCatArea parent)
  {
    super.setParentArea(parent);
    if (parent != null)
    {
      ((WebCatInfoModifiable) getCatalog()).moveArea(modificationData, parent.getAreaID());
    }
  }

  /**
   * @see com.sap.isa.catalog.webcatalog.WebCatArea#getParentArea()
   */
  public WebCatArea getParentArea()
  {
    if ((parentArea == null) &&
        (modificationData != null) &&
        (modificationData.getParentAreaId() != null) &&
        (!modificationData.getParentAreaId().equals(modificationData.getParentAreaIdOriginal())))
      super.setParentArea(getCatalog().getArea(modificationData.getParentAreaId()));
    return super.getParentArea();
  }

}
