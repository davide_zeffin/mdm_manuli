package com.sap.isa.catalog.webcatalog;

import java.util.ArrayList;

public class WebCatWeightedArea extends WebCatBusinessObjectBase {
  private WebCatArea area;
  private int position;
  private ArrayList pathArray;
  private String path;
  private String parentPath = null;
  private int parentId;
  private int sign; //this means what kind of sign should precede it.

  public static final int MINUS=1;
  public static final int PLUS=2;
  public static final int NOTHING=0;

  public WebCatWeightedArea () {
    area=null;
    position=-1;
    pathArray=new ArrayList();
    parentId=-1;
    sign=0;
  }

  public void setArea(WebCatArea area) {
    this.area=area;
  }
  
  public WebCatArea getArea() {
    return area;
  }
  
  public void setPosition(int position) {
      this.position=position;
  }
  
  public int getPosition() {
    return position;
  }

  public void addPath(String path) {
    pathArray.add(path);
  }

  public void setPath() {
    path = (String) pathArray.get(0);
    parentPath = path;
    for (int i = 1; i < pathArray.size(); i++) {
        path+=("/"+(String)pathArray.get(i));
        if (i == pathArray.size() - 2) {
            parentPath = path;
        }
    }  
  }

  public String getPath() {
    return path;
  }
  
  public String getParentPath() {
    return parentPath;
  }

  public void setParentId(int id) {
    parentId=id;
  }

  public int getParentId() {
    return parentId;
  }

  public void setSign(int sign) {
    this.sign=sign;
  }

  public int getSign() {
    return sign;
  }

}