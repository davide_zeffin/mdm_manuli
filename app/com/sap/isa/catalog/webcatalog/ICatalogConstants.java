package com.sap.isa.catalog.webcatalog;

/**
 * Title:
 * Description:
 * Copyright:    Copyright (c) 2001
 * Company:
 * @author
 * @version 1.0
 */

public interface ICatalogConstants
{
    /**
     * Key to store the catalog in the user session
     */
    final static String PCAT = "PCAT_KEY";

    final static String CLIENT = "CLIENT_KEY";

    final static String SERVER = "SEREVER_KEY";
}