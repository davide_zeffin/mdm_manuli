package com.sap.isa.catalog.webcatalog;

import org.apache.struts.upload.FormFile;

/**
 * File: WebCatModificationData.java
 * To change this generated comment edit the template variable "typecomment":
 * Window>Preferences>Java>Templates.
 * To enable and disable the creation of type comments go to
 * Window>Preferences>Java>Code Generation.
 */
public class WebCatModificationData
{

  /**
   * flag to store the item modification status
   */
  protected WebCatModificationStatus changeIndicator;

  /* ------------------------- changeable fields ---------------------------- */

  /**
   * The product short description (short text). Cannot be changed, but needs
   * to be kept for new items to be able to show the product short text in
   * the product list.
   */
  protected String description = null;

  /**
   * The item long description (long text), if it was changed.
   */
  protected String longDescription = null;

  /**
   * The item thumb nail image (small image), if it was changed.
   */
  protected String thumbNail = null;

  /**
   * The item image (large image), if it was changed.
   */
  protected String image = null;

  /**
   * Uploaded image file (if available).
   */
  protected FormFile uploadImage = null;

  /**
   * Uploaded thumb file (if available).
   */
  protected FormFile uploadThumb = null;

  /**
   * The item / area's id.
   */
  protected String id = null;

  /**
   * The item's product id.
   */
  protected String productId = null;

  /**
   * The item's product.
   */
  protected String product = null;

  /**
   * The catalog area the item belongs to (if it was changed).
   */
  protected String parentAreaId = null;

  /**
   * The catalog area the item originally belonged to (if it was changed).
   */
  protected String parentAreaIdOriginal = null;

  /**
   * Constructor for WebCatModificationData.
   */
  public WebCatModificationData()
  {
    super();
  }

  /**
   * Sets the changeIndicator.
   * @param changeIndicator The changeIndicator to set
   */
  public void setModificationStatus(WebCatModificationStatus changeIndicator)
  {
    // do not switch from status like deleted to changed this way!
    if ((changeIndicator != WebCatModificationStatus.CHANGED_OBJECT) ||
        (this.changeIndicator == WebCatModificationStatus.UNCHANGED_OBJECT))
      this.changeIndicator = changeIndicator;
  }

  protected void setModified()
  {
    if (changeIndicator == WebCatModificationStatus.UNCHANGED_OBJECT)
    {
      changeIndicator = WebCatModificationStatus.CHANGED_OBJECT;
    }
  }
  
  /**
   * Returns the changeIndicator.
   * @return WebCatModificationStatus
   */
  public WebCatModificationStatus getModificationStatus()
  {
    return changeIndicator;
  }

  /**
   * Sets the description.
   * @param description The description to set
   */
  public void setDescription(String description)
  {
    this.description = description;
  }

  /**
   * Returns the description.
   * @return String
   */
  public String getDescription()
  {
    return description;
  }

  /**
   * Sets the image.
   * @param image The image to set
   */
  public void setImage(String image)
  {
    this.image = image;
  }

  /**
   * Returns the image.
   * @return String
   */
  public String getImage()
  {
    return image;
  }

  /**
   * Sets the longDescription.
   * @param longDescription The longDescription to set
   */
  public void setLongDescription(String longDescription)
  {
    if ("".equals(longDescription.trim()))
      this.longDescription = null;
    else
      this.longDescription = longDescription;
  }

  /**
   * Returns the longDescription.
   * @return String
   */
  public String getLongDescription()
  {
    return longDescription;
  }

  /**
   * Sets the parentAreaId.
   * @param parentAreaId The parentAreaId to set
   */
  public void setParentAreaId(String parentAreaId)
  {
    if (parentAreaIdOriginal == null)
      setParentAreaIdOriginal(getParentAreaId());
    this.parentAreaId = parentAreaId;
  }

  /**
   * Returns the parentAreaId.
   * @return String
   */
  public String getParentAreaId()
  {
    return parentAreaId;
  }

  /**
   * Sets the parentAreaIdOriginal.
   * @param parentAreaIdOriginal The parentAreaIdOriginal to set
   */
  public void setParentAreaIdOriginal(String parentAreaIdOriginal)
  {
    this.parentAreaIdOriginal = parentAreaIdOriginal;
  }

  /**
   * Returns the parentAreaIdOriginal.
   * @return String
   */
  public String getParentAreaIdOriginal()
  {
    if (parentAreaIdOriginal == null)
      return getParentAreaId();
    else
      return parentAreaIdOriginal;
  }

  public boolean isParentAreaChanged()
  {
    if (parentAreaIdOriginal == null)
      return (parentAreaId == null);
    else 
      return parentAreaIdOriginal.equals(parentAreaId);
  }
  
  /**
   * Sets the thumbNail.
   * @param thumbNail The thumbNail to set
   */
  public void setThumbNail(String thumbNail)
  {
    this.thumbNail = thumbNail;
  }

  /**
   * Returns the thumbNail.
   * @return String
   */
  public String getThumbNail()
  {
    return thumbNail;
  }

  /**
   * Sets the uploadImage.
   * @param uploadImage The uploadImage to set
   */
  public void setUploadImage(FormFile uploadImage)
  {
    this.uploadImage = uploadImage;
  }

  /**
   * Returns the uploadImage.
   * @return FormFile
   */
  public FormFile getUploadImage()
  {
    return uploadImage;
  }

  /**
   * Sets the uploadThumb.
   * @param uploadThumb The uploadThumb to set
   */
  public void setUploadThumb(FormFile uploadThumb)
  {
    this.uploadThumb = uploadThumb;
  }

  /**
   * Returns the uploadThumb.
   * @return FormFile
   */
  public FormFile getUploadThumb()
  {
    return uploadThumb;
  }

  /**
   * Sets the id.
   * @param id The id to set
   */
  public void setId(String id)
  {
    this.id = id;
  }

  /**
   * Returns the id.
   * @return String
   */
  public String getId()
  {
    return id;
  }

  /**
   * Method setProduct.
   * @param productId
   * @param product
   */
  public void setProduct(String productId, String product)
  {
    this.product = product;
    this.productId = productId;
  }

  /**
   * Method getProductId.
   * @return String
   */
  public String getProductId()
  {
    return productId;
  }

  /**
   * Method getProduct.
   * @return String
   */
  public String getProduct()
  {
    return product;
  }

}
