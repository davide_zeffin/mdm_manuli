/*****************************************************************************

    Class:        WebCatInfo
    Copyright (c) 2006, SAP AG, Germany, All rights reserved.
    Created:      2001

*****************************************************************************/
package com.sap.isa.catalog.webcatalog;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;

import com.sap.isa.backend.BackendTypeConstants;
import com.sap.isa.backend.boi.isacore.marketing.CUAListData;
import com.sap.isa.backend.boi.isacore.marketing.CUAProductData;
import com.sap.isa.businessobject.webcatalog.pricing.PriceCalculator;
import com.sap.isa.businessobject.webcatalog.solutionconfigurator.SolutionConfigurator;
import com.sap.isa.catalog.CatalogFactory;
import com.sap.isa.catalog.actions.ActionConstants;
import com.sap.isa.catalog.boi.CatalogException;
import com.sap.isa.catalog.boi.IAttribute;
import com.sap.isa.catalog.boi.ICatalog;
import com.sap.isa.catalog.boi.ICatalogInfo;
import com.sap.isa.catalog.boi.ICategory;
import com.sap.isa.catalog.boi.IClient;
import com.sap.isa.catalog.boi.IFilter;
import com.sap.isa.catalog.boi.IItem;
import com.sap.isa.catalog.boi.IQuery;
import com.sap.isa.catalog.boi.IQueryStatement;
import com.sap.isa.catalog.boi.IServer;
import com.sap.isa.catalog.boi.IServerEngine;
import com.sap.isa.catalog.boi.ISite;
import com.sap.isa.catalog.boi.WebCatInfoData;
import com.sap.isa.catalog.boi.WebCatSubItemListData;
import com.sap.isa.catalog.boi.IServerEngine.ServerType;
import com.sap.isa.catalog.filter.CatalogFilterFactory;
import com.sap.isa.core.TechKey;
import com.sap.isa.core.businessobject.BackendAware;
import com.sap.isa.core.eai.BackendException;
import com.sap.isa.core.eai.BackendObjectManager;
import com.sap.isa.core.init.InitializationEnvironment;
import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.core.logging.LogUtil;
import com.sap.isa.core.util.table.ResultData;
import com.sap.isa.isacore.action.marketing.ShowCUAUtil;
import com.sap.spc.remote.client.object.IPCException;
import com.sap.spc.remote.client.object.IPCItem;
import com.sap.spc.remote.client.object.IPCItemFilter;

/**
 * Provides the catalog functionality that is needed in b2c or b2b shops.
 */
public class WebCatInfo
    extends WebCatBusinessObjectBase
    implements BackendAware, IPCItemFilter, WebCatInfoData {

	/**
	 * Used mostly by the {@link #setLastVisited(String)} method to show the function
	 * currently displayed.
	 * The constant is used by the function to show a product list in an area.
	 * The value of the constant is <b>areaList</b>. 
	 */
	public final static String AREALIST = "areaList";
	
	/**
	 * Used mostly by the {@link #setLastVisited(String)} method to show the function
	 * currently displayed.
	 * The constant is used by the function to show the details of an area.
	 * The value of the constant is <b>areaDetails</b>. 
	 */
	public final static String AREADETAILS = "areaDetails";
    
    /**
	 * Used mostly by the {@link #setLastVisited(String)} method to show the function
	 * currently displayed.
	 * An old constant used by the function to show a product list from a query.
	 * The value of the constant is <b>itemList</b>. 
	 */
    public final static String ITEMLIST = "itemList";
    
    /**
	 * Used mostly by the {@link #setLastVisited(String)} method to show the function
	 * currently displayed.
	 * The constant is used by the function to show a the details of an item.
	 * The value of the constant is <b>itemDetails</b>. 
	 */
    public final static String ITEMDETAILS = "itemDetails";
    
    /**
	 * Used mostly by the {@link #setLastVisited(String)} method to show the function
	 * currently displayed.
	 * The constant is used by the function to show a the details of an item when coming from 
	 * a quick search query, then selecting one of the cross selling features.
	 * The value of the constant is <b>itemDetailsQuery</b>. 
	 */
    public final static String ITEMDETAILSQUERY = "itemDetailsQuery";
    
    /**
	 * Used mostly by the {@link #setLastVisited(String)} method to show the function
	 * currently displayed.
	 * The constant is used by the function to show compare items.
	 * The value of the constant is <b>compareItems</b>. 
	 */
    public final static String COMPARE = "compareItems";
    
    /**
	 * Used mostly by the {@link #setLastVisited(String)} method to show the function
	 * currently displayed.
	 * The constant is used by the function to show the catalog entry.
	 * The value of the constant is <b>initial</b>. 
	 */
    public final static String INITIAL = "initial";
    
    /**
	 * Used mostly by the {@link #setLastVisited(String)} method to show the function
	 * currently displayed.
	 * The constant is used by the function to show a product list from a query in a category.
	 * The value of the constant is <b>categoryQuery</b>. 
	 */
    public final static String CATEGORY_QUERY = "categoryQuery";
    
    /**
	 * Used mostly by the {@link #setLastVisited(String)} method to show the function
	 * currently displayed.
	 * The constant is used by the function to show a product list from a query like quick search.
	 * The value of the constant is <b>catalogQuery</b>. 
	 */
    public final static String CATALOG_QUERY = ActionConstants.DS_CATALOG_QUERY;
    
    /**
	 * Used mostly by the {@link #setLastVisited(String)} method to show the function
	 * currently displayed.
	 * The constant is used by the function to show a list of contracts.
	 * The value of the constant is <b>contracts</b>. 
	 */
    public final static String CONTRACTS = "contracts";
    /**
	 * Used mostly by the {@link #setLastVisited(String)} method to show the function
	 * currently displayed.
	 * The constant is used by the function to show the bestseller in a shop.
	 * The value of the constant is <b>bestseller</b>. 
	 */
    public final static String BESTSELLER = ActionConstants.DS_BESTSELLER;
    
    /**
	 * Used mostly by the {@link #setLastVisited(String)} method to show the function
	 * currently displayed.
	 * The constant is used by the function to show the recommendations in a shop.
	 * The value of the constant is <b>recommendations</b>. 
	 */
    public final static String RECOMMENDATAIONS = ActionConstants.DS_RECOMMENDATION;

    /**
     * types of WebCatItemList
     */
    public final static int CV_LISTTYPE_BESTSELLER      = 1;
    public final static int CV_LISTTYPE_RECOMMENDATIONS = 2;
    public final static int CV_LISTTYPE_CATALOG_AREA    = 3;
    public final static int CV_LISTTYPE_CATALOG_SEARCH  = 4;
    public final static int CV_LISTTYPE_CUA             = 5;
    /**
     * Staging relevant constants
     */
    public final static int STAGING_OK                  = 0;
    public final static int STAGING_NO_SERVER           = 1;
    public final static int STAGING_NOT_SUPPORTED       = 2;
    
    /**
     * types of product detail item list
     */
	public final static int PROD_DET_LIST_CONTRACTS = 1;
	public final static int PROD_DET_LIST_ACCESSORIES = 2;
	public final static int PROD_DET_LIST_ALTERNATIVES = 3;
	public final static int PROD_DET_LIST_REL_PROD = 4;
	public final static int PROD_DET_LIST_EXCH_PROD = 5;
	

    private static IsaLocation log = IsaLocation.getInstance(WebCatInfo.class.getName());
    private BackendObjectManager theBackendManager;
    private ICatalog theCatalog;
    private IServer catalogServer;
    private IClient catalogClient;
    private IServerEngine serverEngine = null;
    private String lastVisited;
    private String lastUsedDetailScenario;
    private WebCatAreaList currentAreaList;
    private WebCatArea currentArea;
    private WebCatItemList currentItemList;
    private WebCatItemList savedItemList;
    private WebCatItem currentItem;
    private OCIInfo ociInfo;
    private ContractDisplayMode contractDisplayMode;
    private String imageServer;
    private String httpsImageServer;
    private PriceCalculator priceCalculator = null;
    private WebCatItemList currentCUAList = null;
    private CUAListData cuaList;
    private String cuaType;
    private WebCatItem configuredItem;
    private boolean extendedSearch = false;
    private String salesOrganisation;
    private String distributionChannel;
    private String division;
    private ResultData condPurpTable;
    private boolean advisorEnabled;
    protected String language = null;
    protected String languageIso = null;
    private ServerType serverType = null;
    
    /**
     * This variable is used to store the last search term used in quick search.
     */
    private String lastQuickSearchQueryString = null;
	protected String hookUrl = null;
	
	private int productDetailListType = 0;

    /**
     * indicates whether problems have been detected when calling the backend catalog. this should make the application
     * behave accordingly
     */
    protected boolean valid = true;

    /** Number of items to be displayed on one page */
    private int itemPageSize;
    /** Number of items to be displayed on one page - original value */
    private int itemPageSizeMin = 5;

    /** The number of page links to be displayed in the search results page */
    private int numPageLinks;

	/** block view parameters for different types of product lists */
	private int blockViewMaxCols;
    /** block view parameters for catalog area */
	private boolean catAreaShowAsBlockView;
    private int pageSizeCatAreaBV;
    private int pageSizeCatAreaBVMin;
//	private int catAreaMaxRowsBV;
    /** block view parameters for catalog search */
	private boolean catSearchShowAsBlockView;
    private int pageSizeCatSearchBV;
    private int pageSizeCatSearchBVMin;
//	private int catSearchMaxRowsBV;
    /** block view parameters for bestseller */
	private boolean bestsellerShowAsBlockView;
    private int pageSizeBestsellerBV;
    private int pageSizeBestsellerBVMin;
//	private int bestsellerMaxRowsBV;
    /** block view parameters for recommendations */
	private boolean recommShowAsBlockView;
    private int pageSizeRecommBV;
    private int pageSizeRecommBVMin;
//	private int recommendationsMaxRowsBV;
    /** switch to control automatic explosion of packages */
    private boolean automaticPackageExplosion = false;

    /** Campaign Id */
    private String campaignId;
    
	/** The used Campaign Id to compute the CUA recommendation prices */
	private String recommendationCampaignId;
    
	/** The used Campaign Id to compute the CUA bestseller prices */
	private String bestsellerCampaignId;

    /** Campaign Guid */
    private TechKey campaignGuid;

    /** Flag to indicate if the eligibility check for a sold-to was successful. */
    private boolean isSoldToEligible = false;
    private boolean doPricing = false;

    private String debit_credit_strategy;
    
    /** Flag to indicate if the catalog has points items */
    private boolean hasPtsItems = false;

    /**
     * Creates a new instance. The instance has to be initialized via the <code>init</code> method. Before the
     * initialization can be performed the backend object manager has to be set correctly. Never pass a instance of
     * this class to the client before the <code>init</code> has been called.
     */
    public WebCatInfo() {
        lastVisited = INITIAL;
        imageServer = null;
        httpsImageServer = null;
    }

    /**
     * Creates a new instance of the SolutionConfigurator class
     * and assigns it to the given WebCatItem.
     * 
     * @param webCatItem the item, the new SolutionConfigurator instance 
     *                   should be created for and assigned to.
     */
    public void createSolutionConfigurator(WebCatItem webCatItem) {
        log.debug("Try to create a new SolutionConfigurator object");
        SolutionConfigurator sc = new SolutionConfigurator(webCatItem);
        sc.setBackendObjectManager(theBackendManager);
        log.debug("Assign SolutionConfigurator object " + sc + "to the WebCatitem " + webCatItem.getProduct());
        webCatItem.setSolutionConfigurator(sc); 
    }

    /**
     * Creates a <code>WebCatSubItemListData</code> object from a backend class. 
     * 
     * @return a new WebCatSubItemListData instance
     */
    public WebCatSubItemListData createWebCatSubItemListData() {
        return new WebCatSubItemList(this);
    }

    /**
     * DOCUMENT ME!
     *
     * @param bem DOCUMENT ME!
     */
    public void setBackendObjectManager(BackendObjectManager bem) {
        theBackendManager = bem;
    }

    /**
     * DOCUMENT ME!
     *
     * @param aClient DOCUMENT ME!
     * @param aServer DOCUMENT ME!
     * @param environment DOCUMENT ME!
     *
     * @throws BackendException DOCUMENT ME!
     */
    public void init(IClient aClient, IServer aServer, InitializationEnvironment environment)
        throws BackendException {

        log.debug("WebCatInfo Client=" + aClient + ", Server=" + aServer);

        catalogServer = aServer;
        catalogClient = aClient;

        try {
            CatalogFactory factory =
                CatalogFactory.getInstance(
                    theBackendManager,
                    BackendTypeConstants.BO_TYPE_CATALOG_SITE,
                    environment);
            ISite site = factory.getCatalogSite(catalogClient, BackendTypeConstants.BO_TYPE_CATALOG_SITE);

            // Internet Sales currently only uses 1 catalog server -
            // just grab the first one
            serverEngine = (IServerEngine) site.getCatalogServers().next();
            serverType = serverEngine.getType();

            log.debug("selected server " + serverEngine.getGuid() + "(" + serverEngine.getName() + ")");

            if (log.isDebugEnabled()) {
                Iterator catIter = serverEngine.getCatalogInfos();

                while (catIter.hasNext()) {
                    ICatalogInfo catInf = (ICatalogInfo) catIter.next();

                    log.debug("available catalog:" + catInf.getGuid() + "," + catInf.getName() + "," + catInf.getDescription() + ".");
                }
            }
            
            if (log.isDebugEnabled()) {
                log.debug("Requested cataloState=" + catalogClient.getCatalogStatus());
            }
            
            // don't proceed if we should read a inactive staging catalogue and staging is not supported by the backend
            if (catalogClient.getCatalogStatus() == IClient.CATALOG_STATE_INACTIVE) {
                
                log.debug("Staging catalogue requested");
                
                if (!IServerEngine.STAGING_SUPPORTED.equals(serverEngine.getStagingSupport())) {
                    valid = false;
                    log.warn(LogUtil.APPS_COMMON_CONFIGURATION, "catalog.warn.nostagingsupp");
                }
            }
            else {
                
                if (catalogClient.getCatalogStatus() == IClient.CATALOG_STATE_UNDEFINED && 
				    IServerEngine.STAGING_SUPPORTED.equals(serverEngine.getStagingSupport())) {
                    log.debug("Undefined catalogue state requested - change to active");
                    catalogClient.setCatalogStatus(IClient.CATALOG_STATE_ACTIVE);
                }
                
                log.debug("Non staging catalogue requested");
                
                theCatalog = serverEngine.getCatalog(aServer.getCatalogGuid(), catalogClient);
                
                // pcatAPI does not throw an exception if catalog not found,
                // so check now
                if (theCatalog == null) {
                    throw new BackendException("catalog.notFound(" + aServer.getCatalogGuid() + ")");
                }
                
                setCurrentArea(WebCatArea.ROOT_AREA);

                if (!isValid()) {
                    throw new BackendException("Error in catalog initialization");
                }
            }

            itemPageSize = 5;
            
            // check if the catalog has points items and set the attribute hasPtsItems
            if (theCatalog != null) { 
                Iterator iterAttr = theCatalog.getAttributes();
                while ( iterAttr.hasNext() ) {
                    IAttribute attrib = (IAttribute) iterAttr.next();
                    if ("IS_PTS_ITEM".equals(attrib.getName())) {
                        this.hasPtsItems = true;
                        break;
                    }
                }
            }            
        }
        catch (Exception e) {
            log.error("system.exception", new Object[] { "BEM exception " + e.getMessage()}, e);
            throw new BackendException(e);
        }
    }

    /**
     * Store catalog area last visited. Current area is used to re-display the same content after switching to shopping
     * basket or configuration.
     *
     * @param newCurrentArea Area to be stored
     */
    public void setCurrentArea(WebCatArea newCurrentArea) {

        if (newCurrentArea != null) {
            log.debug("Setting current area to (ID) " + newCurrentArea.getAreaID());
        }
        else {
            log.debug("Setting current area to null");
        }

        if (currentArea != null && currentArea.equals(newCurrentArea)) {
            log.debug("Catalog area not changed");
        }
        else {
            if (currentArea != null
                && !newCurrentArea.getAreaID().equals(WebCatArea.ROOT_AREA)
                && currentArea.isParentOf(newCurrentArea)) {
                newCurrentArea.setParentArea(currentArea);
            }

            currentArea = newCurrentArea;
            setCurrentItemList(null);
            setCurrentItem(null);
        }

        setLastVisited(AREADETAILS);
    }

    /**
     * Store catalog area last visited, identified by area ID.
     *
     * @param newCurrentAreaID area ID of area to be stored.
     */
    public void setCurrentArea(String newCurrentAreaID) {

        log.debug("Setting current area to (String) " + newCurrentAreaID);

        if (currentAreaList != null && currentAreaList.indexOf(newCurrentAreaID) >= 0) {
            int idx = currentAreaList.indexOf(newCurrentAreaID);
            setCurrentArea(currentAreaList.getArea(idx));
        }
        else if (currentArea == null) {
            WebCatArea newCurrentArea = getArea(newCurrentAreaID);
            setCurrentArea(newCurrentArea);
        }
        else if (!newCurrentAreaID.equalsIgnoreCase(currentArea.getAreaID())) {
            WebCatArea newCurrentArea = getArea(newCurrentAreaID);

            if (newCurrentArea == null) {
                log.error("catalog.exception.backend");
//                valid = false;

                return;
            }

            if (!newCurrentAreaID.equals(WebCatArea.ROOT_AREA)
                && currentArea.isParentOf(newCurrentArea)) {
                newCurrentArea.setParentArea(currentArea);
            }

            setCurrentArea(newCurrentArea);
        }
    }
    
    /**
     * This method will read the inactive catalog for the given inactiveCatalogId.
     * 
     * To do this the init method of the catalog must have been executed before,
     * so the CatalogClient and CatalogServer are already set. If not execptions will
     * be thrown.
     *
     * @param TechKey inactiveCatatalogKey the unique id of the inactive catalogue that should be read
     *
     * @throws BackendException if CatalogClient, CatalogServer or serverrEngine are
     *                          not set respective execptions will be thrown.
     */
    public void readInactiveCatalog(TechKey stagingCatatalogKey)
        throws BackendException {
            
        log.entering("readStagingCatalog()");
        
        if (log.isDebugEnabled()) {
            log.debug("stagingCatTechkey=" + stagingCatatalogKey);
        }
        
        // check, that all prerequsites are fulfilled
        if (serverEngine == null) {
            throw new BackendException("Staging catalog could not be read: Server engine is not available");
        }
        
        if (!IServerEngine.STAGING_SUPPORTED.equals(serverEngine.getStagingSupport())) {
            throw new BackendException("Staging catalog could not be read: Catalogue Staging is not supported by the backend");
        }
        
        if (catalogClient == null) {
            throw new BackendException("Staging catalog could not be read: CatalogClient is not set");
        }
        
        if (catalogClient.getCatalogStatus() != IClient.CATALOG_STATE_INACTIVE) {
            throw new BackendException("Inactive catalog could not be read: CatalogClient state is not set to inactive, but to " + catalogClient.getCatalogStatus());
        }
        
        catalogClient.setStagingCatalogKey(stagingCatatalogKey);

        if (catalogServer == null) {
            throw new BackendException("Staging catalog could not be read: CatalogServer is not set");
        }
        
        // no try to read the inactive staging catalog

        try {
                theCatalog = serverEngine.getCatalog(catalogServer.getCatalogGuid(), catalogClient);
                
                // pcatAPI does not throw an exception if catalog not found,
                // so check now
                if (theCatalog == null) {
                    throw new BackendException("catalog.notFound(" + catalogServer.getCatalogGuid() + ")");
                }
                
                setCurrentArea(WebCatArea.ROOT_AREA);

                if (!isValid()) {
                    throw new BackendException("Error in catalog initialization");
                }
        }
        catch (Exception e) {
            log.error("system.exception", new Object[] { "BEM exception " + e.getMessage()}, e);
            throw new BackendException(e);
        }
    }


    /**
     * DOCUMENT ME!
     */
    public void resetCurrentArea() {
        //catalogLog.debug("Resetting current area");
        currentArea = null;
        setCurrentItemList(null);
        setCurrentItem(null);
    }

    /**
     * Retrieve current area information.
     *
     * @return DOCUMENT ME!
     */
    public WebCatArea getCurrentArea() {
        return currentArea;
    }
  
    public void setDebit_Credit_Strategy(String debit_credit_strategy) {
  	    this.debit_credit_strategy = debit_credit_strategy;
  	
    }
  
    public String getDebit_Credit_Strategy() {
  	    return this.debit_credit_strategy;
    }
  
    /**
     * Store the Sales organisation information passed from the Shop Object.
     *
     * @param salesOrganisation DOCUMENT ME!
     */
    public void setSalesOrganisation(String salesOrganisation) {
        this.salesOrganisation = salesOrganisation;
    }

    /**
     * Retrieve the sales organisation information
     *
     * @return DOCUMENT ME!
     */
    public String getSalesOrganisation() {
        return salesOrganisation;
    }

    /**
     * Returns the <code>ServerType</code> of the server engine.
     * @return  server type 
     */
    public ServerType getServerType () {
        return serverType; 
    }
    
    /**
     * Returns InactiveCatalogInfos for the current catalog.
     * If no inactive staging catalogs are available, or no catalogserver has been created
     * yet, or staging is not supported in the backend the list might be empty.
     * 
     * @param stagingType the staging type, to search for
     * @param country country as String
     * @return stagingInfos will be set by the method and contain the list of catalog 
     *                      staging infos available for the given staging type
     */
    public ResultData getInactiveCatalogInfos(String stagingType, String country) {

        log.entering("getInactiveCatalogInfos(String stagingType, String country)");

        ResultData stagingInfos = null;
        stagingInfos = serverEngine.getInactiveCatalogInfos(catalogServer.getCatalogGuid(), stagingType, catalogClient, country);
        
        log.exiting();
        
        return stagingInfos; 
    }
    
    /**
     * Checks the status if the server engine is available and supports staging 
     * 
     * @return retCode This value tells about the success of the method call
     *             WebCatInfo.STAGING_OK    success (but list might nevertheless be empty)
     *             WebCatInfo.STAGING_NO_SERVER no serverengine available we could ask
     *             WebCatInfo.STAGING_NOT_SUPPORTED staging is not supported by the backend
     */
    public int checkServerEngine() {

        log.entering("checkServerEngine()");
        
        int retCode = STAGING_OK;
        
        if (serverEngine == null) {
            log.debug("No Server engine");
            retCode = STAGING_NO_SERVER;
        }
        else if (!IServerEngine.STAGING_SUPPORTED.equals(serverEngine.getStagingSupport())) {
            log.debug("Staging not supported");
            retCode = STAGING_NOT_SUPPORTED;
        }
        log.exiting();
        
        return retCode; 
    }
    
    /**
     * Creates a <code>WebCatSubItem</code> for the given data. This method is call during
     * population of a WebCatSubItemList.
     * 
     * @return a new WebCatSubItem object
     */
    public WebCatSubItem getSubItem(WebCatItemKey itemKey, IItem catalogItem) {
        return new WebCatSubItem(this, catalogItem);
    }

    /**
     * Store the distributionChannel information from the Shop Object.
     *
     * @param distributionChannel DOCUMENT ME!
     */
    public void setDistributionChannel(String distributionChannel) {
        this.distributionChannel = distributionChannel;
    }

    /**
     * Retrieve the distributionChannel information
     *
     * @return DOCUMENT ME!
     */
    public String getDistributionChannel() {
        return distributionChannel;
    }

    /**
     * Store the division from the shop object
     *
     * @param division DOCUMENT ME!
     */
    public void setDivision(String division) {
        this.division = division;
    }

    /**
     * DOCUMENT ME!
     *
     * @return division
     */
    public String getDivision() {
        return division;
    }

    /**
     * Set the condition purpose group for exchange products
     *
     * @param condPurpTable DOCUMENT ME!
     */
    public void setCondPurpGroup(ResultData condPurpTable) {
        this.condPurpTable = condPurpTable;
    }

    /**
     * DOCUMENT ME!
     *
     * @return DOCUMENT ME!
     */
    public ResultData getCondPurpGroup() {
        return condPurpTable;
    }

    /**
     * Get array of areas available in catalog.
     *
     * @return DOCUMENT ME!
     *
     * @throws CatalogException DOCUMENT ME!
     *
     * @deprecated use getAllCategories instead
     */
    public WebCatAreaList getAreas() throws CatalogException {
        return new WebCatAreaList(this, null, theCatalog.getCategories());
    }

    /**
     * DOCUMENT ME!
     *
     * @return DOCUMENT ME!
     */
    public CUAListData getCuaList() {
        return cuaList;
    }
    
	/**
	* DOCUMENT ME!
	*
	* @return DOCUMENT ME!
	*/
	public void setCuaList(CUAListData cuaList) {
	   this.cuaList = cuaList;
	}

    /**
     * DOCUMENT ME!
     *
     * @return DOCUMENT ME!
     */
    public String getCuaType() {
        return cuaType;
    }
    
	/**
	 * sets the cuaType for this WebCatInfo
	 *
	 * @param cuaType, the cuaType to set for this WebCatInfo
	 */
	public void setCuaType(String cuaType) {
		this.cuaType = cuaType;
	}

    /**
     * DOCUMENT ME!
     *
     * @param advisorEnabled DOCUMENT ME!
     */
    public void setAdvisorEnabled(boolean advisorEnabled) {
        this.advisorEnabled = advisorEnabled;
    }

    /**
     * DOCUMENT ME!
     *
     * @return DOCUMENT ME!
     */
    public boolean isAdvisorEnabled() {
        return advisorEnabled;
    }

    /**
     * DOCUMENT ME!
     *
     * @param cuaList DOCUMENT ME!
     * @param cuaType DOCUMENT ME!
     */
    public void setCurrentCUAList(CUAListData cuaList, String cuaType) {

        this.cuaList = cuaList;
        this.cuaType = cuaType;
        currentCUAList = new WebCatItemList(this);
        currentCUAList.setListType(CV_LISTTYPE_CUA);

        Iterator iter = cuaList.iterator();

        while (iter.hasNext()) {
            CUAProductData prod = (CUAProductData) iter.next();

            if (ShowCUAUtil.isProductVisible(prod.getCuaType(), cuaType)) {
                WebCatItem wItem = (WebCatItem) prod.getCatalogItemAsObject();
                wItem.readItemPrice();
                currentCUAList.addItem(wItem);
            }
        }
    }

    /**
     * DOCUMENT ME!
     *
     * @return DOCUMENT ME!
     */
    public WebCatItemList getCurrentCUAList() {
        return currentCUAList;
    }

    /**
     * This method resets the list of displayed CUA products in the catalog.
     * The list will be regenerated when such a list is requested.
     *
     */
    public void resetCUAList() {
    	cuaList = null;
    	cuaType = null;
    	currentCUAList = null;
    }
    
    /**
     * DOCUMENT ME!
     *
     * @param areaList DOCUMENT ME!
     */
    public void setCurrentAreaList(WebCatAreaList areaList) {
        currentAreaList = areaList;
    }

    /**
     * DOCUMENT ME!
     *
     * @return DOCUMENT ME!
     */
    public WebCatAreaList getCurrentAreaList() {
        return currentAreaList;
    }

    /**
     * DOCUMENT ME!
     *
     * @return DOCUMENT ME!
     *
     * @throws CatalogException DOCUMENT ME!
     */
    public Iterator getAttributes() throws CatalogException {
        return theCatalog.getAttributes();
    }

    /**
     * Store catalog item last visited. Used for re-display when returning from basket/configuration etc. to product
     * details.
     *
     * @param newCurrentItem Item to be stored
     */
    public void setCurrentItem(WebCatItem newCurrentItem) {
        currentItem = newCurrentItem;
    }

    /**
     * Get catalog item last visited.
     *
     * @return Catalog item. Can be null in the beginning.
     */
    public WebCatItem getCurrentItem() {
        return currentItem;
    }

    /**
     * get a distinct item by areaID and itemID
     *
     * @param areaID ID of the area the item belongs to
     * @param itemID ID of the item
     *
     * @return item item searched for as WebCatItem
     */
    public WebCatItem getItem(String areaID, String itemID) {

        WebCatItem newItem;

        // catch NullPointerException which might occur if one of the parts is
        // not available (catalog, area, item); return null in this case.
        try {
            newItem = new WebCatItem(this, getCatalog().getCategory(areaID).getItem(itemID));
        }
        catch (java.lang.NullPointerException ne) {
            newItem = null;
        }
        catch (CatalogException e) {
            log.error("catalog.exception.backend", e);
            newItem = null;
        }

        return newItem;
    }

    /**
     * ...
     *
     * @param itemKey DOCUMENT ME!
     * @param catalogItem DOCUMENT ME!
     *
     * @return DOCUMENT ME!
     */
    public WebCatItem getItem(WebCatItemKey itemKey, IItem catalogItem) {
        return new WebCatItem(itemKey, catalogItem);
    }

    /**
     * ...
     *
     * @param itemKey DOCUMENT ME!
     *
     * @return DOCUMENT ME!
     */
    public WebCatItem getItem(WebCatItemKey itemKey) {
        return new WebCatItem(itemKey);
    }

    /**
     * Creates a new <code>WebCatArea</code> object, for the <code>ICategory</code> with 
     * the provided <code>areaID</code>.
     *
     * @param areaID ID of the area
     *
     * @return new WebCatArea object
     */
    public WebCatArea getArea(String areaID) {
        
        WebCatArea newArea;

        log.debug("WebCatArea#getArea(" + areaID + ")");

        // catch NullPointerException which might occur if one of the parts is
        // not available (catalog, area); return null in this case.
        try {
            newArea = new WebCatArea(this, areaID);
        }
        catch (CatalogException e) {
            log.warn("catalog.exception.backend", e);
            valid = false;
            newArea = null;
        }

        return newArea;
    }

    /**
     * Factory method for creating a <code>WebCatArea</code>.
     *
     * @param category DOCUMENT ME!
     *
     * @return DOCUMENT ME!
     */
    public WebCatArea getArea(ICategory category) {
        return new WebCatArea(this, category);
    }

    /**
     * DOCUMENT ME!
     *
     * @return DOCUMENT ME!
     *
     * @throws CatalogException DOCUMENT ME!
     */
    public WebCatAreaList getAllCategories() throws CatalogException {
        Iterator categories = getCatalog().getCategories();
        WebCatAreaList children = new WebCatAreaList(this, null, categories);

        return children;
    }

    /**
     * DOCUMENT ME!
     *
     * @return DOCUMENT ME!
     *
     * @throws CatalogException DOCUMENT ME!
     */
    public WebCatAreaList getRootCategories() throws CatalogException {
        Iterator categories = getCatalog().getChildCategories();
        WebCatAreaList children = new WebCatAreaList(this, null, categories);

        return children;
    }

    /**
     * Create WebCatItemList from array of product GUIDs This method can be used to retrieve catalog information for
     * given product ids. To display CUA product info, method <code>getProductMap</code> might be more usefull as from
     * the resulting map items can be retrieved by product id.
     *
     * @param productIDs array of String including the IDs/Guids of the products
     *
     * @return itemList WebCatItemList containing the corresponding items.
     */
    public WebCatItemList getItemList(String productIDs[]) {

        // return empty WebCatItemList if no products provided
        if (productIDs.length == 0) {
            return new WebCatItemList(this);
        }

        // create the query to search for products
        CatalogFilterFactory fact = CatalogFilterFactory.getInstance();
        IFilter productFilter = fact.createAttrEqualValue("OBJECT_GUID", productIDs[0]);
        IFilter filter = productFilter;

        // create the query to search for products
//        CatalogFilterFactory fact = CatalogFilterFactory.getInstance();
//        IFilter productFilter = fact.createAttrEqualValue("OBJECT_GUID", productIDs[0]);
//        IFilter filter = productFilter;
        productFilter = fact.createAttrEqualValue("OBJECT_GUID", productIDs[0]);
        filter = fact.createAnd(productFilter, filter); // AND statement

        for (int i = 1; i < productIDs.length; i++) {
            productFilter = fact.createAttrEqualValue("OBJECT_GUID", productIDs[i]);
            filter = fact.createOr(productFilter, filter); // OR statement
        }

        // Search not in reward catalog
        if (hasPtsItem()) {
            IFilter filter2 = fact.createAttrEqualValue("IS_PTS_ITEM", "X");
            filter2 = fact.createNot(filter2);
            filter = fact.createAnd(filter, filter2);
        }

        IQuery query = null;

        try {
            IQueryStatement queryStmt = theCatalog.createQueryStatement();
            queryStmt.setStatement(filter, null, null); // attributes, sort order?
            query = theCatalog.createQuery(queryStmt);
        }
        catch (CatalogException e) {
            log.error(e.getClass() + " " + e.getMessage());
        }

        // create WebCatItemList from this query (including accessory products) and return
        WebCatItemList itemList = new WebCatItemList(this, query, true);

        return itemList;
    }

    /**
     * Create WebCatItemList from array of product GUIDs This method can be used to retrieve catalog information for
     * given product ids. To display CUA product info, method <code>getProductMap</code> might be more usefull as from
     * the resulting map items can be retrieved by product id.
     *
     * @param productIDs array of String including the IDs/Guids of the products
     * @param areaType as String, type of the area in which the product needs to be searched
     *
     * @return itemList WebCatItemList containing the corresponding items.
     */
    public WebCatItemList getItemList(String productIDs[], String areaType) {

        // return empty WebCatItemList if no products provided
        if (productIDs.length == 0) {
            return new WebCatItemList(this);
        }

        // create the query to search for products
        CatalogFilterFactory fact = CatalogFilterFactory.getInstance();
        
        IFilter filter = fact.createAttrEqualValue("OBJECT_GUID", productIDs[0]);
        for (int i = 1; i < productIDs.length; i++) {
            IFilter productFilter = fact.createAttrEqualValue("OBJECT_GUID", productIDs[i]);
            filter = fact.createOr(productFilter, filter); // OR statement
        }

        // Restrict search to special areas
        if (hasPtsItems) {
            IFilter filter2 = null;
            if ( areaType != null && areaType.equals(WebCatArea.AREATYPE_LOY_REWARD_CATALOG) ) {
                // Search in reward areas of the catalog 
                filter2 = fact.createAttrEqualValue("IS_PTS_ITEM", "X");
            }
//            else if ( areaType != null && areaType.equals(WebCatArea.AREATYPE_LOY_BUY_POINTS) ) {
//                // Search in buy points areas of the catalog 
//                filter2 = fact.createAttrEqualValue("IS_BUY_PTS_ITEM", "X");
//            }
            else {
                // Search not in reward areas of the catalog
                filter2 = fact.createAttrEqualValue("IS_PTS_ITEM", "X");
                filter2 = fact.createNot(filter2);
            }
            filter = fact.createAnd(filter, filter2);
        }

        IQuery query = null;

        try {
            IQueryStatement queryStmt = theCatalog.createQueryStatement();
            queryStmt.setStatement(filter, null, null); // attributes, sort order?
            query = theCatalog.createQuery(queryStmt);
        }
        catch (CatalogException e) {
            log.error(e.getClass() + " " + e.getMessage());
        }

        // create WebCatItemList from this query (including accessory products) and return
        WebCatItemList itemList = new WebCatItemList(this, query, true);

        return itemList;
    }

    /**
     * Create WebCatItemList from array of product IDs This method can be used to retrieve catalog information for
     * given product ids. To display CUA product info, method <code>getProductMap</code> might be more usefull as from
     * the resulting map items can be retrieved by product id.
     *
     * @param productIDs array of String including the IDs of the products
     *
     * @return itemList WebCatItemList containing the corresponding items.
     */
    public WebCatItemList getItemListFromID(String productIDs[]) {

        // return empty WebCatItemList if no products provided
        if (productIDs.length == 0) {
            return new WebCatItemList(this);
        }

        // create the query to search for products
        CatalogFilterFactory fact = CatalogFilterFactory.getInstance();
        IFilter productFilter = fact.createAttrEqualValue("OBJECT_ID", productIDs[0]);
        IFilter filter = productFilter;

        for (int i = 1; i < productIDs.length; i++) {
            productFilter = fact.createAttrEqualValue("OBJECT_ID", productIDs[i]);
            filter = fact.createOr(productFilter, filter); // OR statement
        }
        
        // Search not in reward catalog
        if (hasPtsItem()) {
            IFilter filter2 = fact.createAttrEqualValue("IS_PTS_ITEM", "X");
            filter2 = fact.createNot(filter2);
            filter = fact.createAnd(filter, filter2);
        }

        IQuery query = null;

        try {
            IQueryStatement queryStmt = theCatalog.createQueryStatement();
            queryStmt.setStatement(filter, null, null); // attributes, sort order?
            query = theCatalog.createQuery(queryStmt);
        }
        catch (CatalogException e) {
            log.error(e.getClass() + " " + e.getMessage());
        }

        // create WebCatItemList from this query (including accessory products) and return
        WebCatItemList itemList = new WebCatItemList(this, query, true);

        return itemList;
    }

    /**
     * Create HashMap of Lists from array of product IDs This method can be used to retrieve catalog information for
     * given product ids. The resulting HashMap gives for the Key productID as result an ArrayList of WebCatItems
     * containing this product (as the product might be in the catalog more than one time).
     *
     * @param productIDs array of String including the IDs/Guids of the products
     *
     * @return productMap HashMap containing List of corresponding items for every product.
     */
    public HashMap getProductMap(String productIDs[]) {

        return getProductMap(productIDs, true, null);
    }
    
	/**
	 * Create HashMap of Lists from array of product IDs This method can be used to retrieve catalog information for
	 * given product ids. The resulting HashMap gives for the Key productID as result an ArrayList of WebCatItems
	 * containing this product (as the product might be in the catalog more than one time).
	 *
	 * @param productIDs array of String including the IDs/Guids of the products
	 * @param withoutSubComponent; if false subComponent won't be returned.
	 *
	 * @return productMap HashMap containing List of corresponding items for every product.
	 */
	public HashMap getProductMap(String productIDs[], boolean withSubComponent) {
		
        return getProductMap(productIDs, withSubComponent, null);
    }

    /**
     * Create HashMap of Lists from array of product IDs This method can be used to retrieve catalog information for
     * given product ids. The resulting HashMap gives for the Key productID as result an ArrayList of WebCatItems
     * containing this product (as the product might be in the catalog more than one time).
     *
     * @param productIDs array of String including the IDs/Guids of the products
     * @param withSubComponent; if false subComponent won't be returned.
     * @param areaType as String, type of the area in which the product needs to be searched
     *
     * @return productMap HashMap containing List of corresponding items for every product.
     */
    public HashMap getProductMap(String productIDs[], boolean withSubComponent, String areaType) {
        
        HashMap productMap = new HashMap();
        // create WebCatItemList from these products
        WebCatItemList itemList = this.getItemList(productIDs, areaType);

        // loop at item list to group items with same product id
        Iterator itemIterator = itemList.iterator();

        
        if( !withSubComponent){
            //sort the list; main item and then accessories...
            int size = itemList.size();
            int incrSize = size;
            if(size > 0){
                for(int i=0; i<incrSize; i++){
                    WebCatItem item = itemList.getItem(i);
                    if (item.isSubComponent()){
                        itemList.removeItem(i);
                        continue;
                    }
                    if( item.isAccessory()) {
                        //delete it and insert it at the end.
                        itemList.removeItem(i);
                        itemList.addItem(item,size-1);
                        incrSize --;
                        i--;
                        continue;
                    }
                }
            }
            itemIterator = itemList.iteratorOnlyPopulated();
        }
        
            
        while (itemIterator.hasNext()) {
            WebCatItem item = (WebCatItem) itemIterator.next();

            // check for entry in map for this product id
            ArrayList itemArray = (ArrayList) productMap.get(item.getProductID());

            // if no entry for this product id, create one
            if (itemArray == null) {
                itemArray = new ArrayList();
                productMap.put(item.getProductID(), itemArray);
            }

            // add current item to the item list of the current product id
            itemArray.add(item);
        }

        return productMap;
    }

    /**
     * DOCUMENT ME!
     *
     * @param newCurrentItemList DOCUMENT ME!
     */
    public void setCurrentItemList(WebCatItemList newCurrentItemList) {
        log.entering("setCurrentItemList");
        
        setCurrentItem(null);     
           
        if (configuredItem != null && currentItemList != null && 
            currentItemList.getItem(configuredItem.getItemID()) != null) {
            log.debug("Deleted reference to configuredItem, because it is contained in the itemlist being replaced");
            configuredItem = null;
        }
        
        currentItemList = newCurrentItemList;

        if (currentItemList != null && currentItemList.getArea() != null) {
            currentArea = currentItemList.getArea();
        }

        log.exiting();
    }

    /**
     * DOCUMENT ME!
     *
     * @return DOCUMENT ME!
     */
    public WebCatItemList getCurrentItemList() {

        if (currentItemList == null && currentArea != null) {
            currentItemList = currentArea.getItemList();
        }

        return currentItemList;
    }

    /**
     * DOCUMENT ME!
     *
     * @return DOCUMENT ME!
     */
    public ICatalog getCatalog() {
        return theCatalog;
    }

    /**
     * DOCUMENT ME!
     *
     * @return DOCUMENT ME!
     */
    public IServer getCatalogServer() {
        return catalogServer;
    }

    /**
     * DOCUMENT ME!
     *
     * @param newOciInfo DOCUMENT ME!
     */
    public void setOciInfo(OCIInfo newOciInfo) {
        ociInfo = newOciInfo;
    }

    /**
     * DOCUMENT ME!
     *
     * @return DOCUMENT ME!
     */
    public OCIInfo getOciInfo() {
        return ociInfo;
    }

    /**
     * DOCUMENT ME!
     *
     * @param mode DOCUMENT ME!
     */
    public void setContractDisplayMode(ContractDisplayMode mode) {
        this.contractDisplayMode = mode;
    }

    /**
     * DOCUMENT ME!
     *
     * @return DOCUMENT ME!
     */
    public ContractDisplayMode getContractDisplayMode() {
        return contractDisplayMode;
    }

    /**
     * DOCUMENT ME!
     *
     * @param imageServer DOCUMENT ME!
     */
    public void setImageServer(String imageServer) {

        if (imageServer.endsWith("/")) {
            imageServer = imageServer.substring(0, imageServer.length() - 1);
        }

        this.imageServer = imageServer;
    }

    /**
     * DOCUMENT ME!
     *
     * @param imageServer DOCUMENT ME!
     */
    public void setHttpsImageServer(String httpsImageServer) {

        if (httpsImageServer.endsWith("/")) {
            httpsImageServer = httpsImageServer.substring(0, httpsImageServer.length() - 1);
        }

        this.httpsImageServer = httpsImageServer;
    }

    /**
     * DOCUMENT ME!
     *
     * @return DOCUMENT ME!
     */
    public String getImageServer() {
        return imageServer;
    }

    /**
     * This method returns the httpsImageServer parameter, which shoudl be used if https is used as protocol.
     *
     * @return String the httpsImageServer
     */
    public String getHttpsImageServer() {
        log.entering("getHttpsImageServer");
        log.exiting();
        return httpsImageServer;
    }


    /**
     * set number of items to be displayed on one page
     *
     * @param newItemPageSize DOCUMENT ME!
     */
    public void setItemPageSize(int newItemPageSize) {

        if (newItemPageSize >= 0 && newItemPageSize != itemPageSize) {
            itemPageSize = newItemPageSize;

            if (currentItemList != null) {
                currentItemList.resetCurrentItemPage();
            }
        }
    }

    /**
     * get number of items to be displayed on one page
     *
     * @return DOCUMENT ME!
     */
    public int getItemPageSize() {
        return itemPageSize;
    }

    /**
     * set the original value of number of items to be displayed on one page
     * in list view
     *
     * @param newItemPageSize DOCUMENT ME!
     */
    public void setItemPageSizeMin(int newItemPageSize) {

        if (newItemPageSize >= 0 && newItemPageSize != itemPageSizeMin) {
            itemPageSizeMin = newItemPageSize;

            if (currentItemList != null) {
                currentItemList.resetCurrentItemPage();
            }
        }
    }

    /**
     * get original value of number of items to be displayed on one page in list view
     *
     * @return original item page size as int
     */
    public int getItemPageSizeMin() {
        return itemPageSizeMin;
    }

    /**
     * Sets the function that is shown right now.
     * The values can be the string constants in this class.
     *
     * @param lastVisited A string value of the function currently shown.
     */
    public void setLastVisited(String lastVisited) {
    	this.lastVisited = lastVisited;
    }

    /**
     * Returns the function visited the last time.
     *
     * @return The function visited the last time.
     */
    public String getLastVisited() {
        return lastVisited;
    }

    /**
     * This method returns the detail scenario used in catalog. This is used for if
     * the customer used the refresh button. 
     * @return The function showed before last visit.
     */
    public String getLastUsedDetailScenario() {
    	return lastUsedDetailScenario;
    }
    
    /**
     * This method sets the detail scenario used in catalog. This is used for if
     * the customer used the refresh button. 
     */
    public void setLastUsedDetailScenario(String lastUsedDetailScenario) {
    	this.lastUsedDetailScenario = lastUsedDetailScenario;
    }
    
    /**
     * DOCUMENT ME!
     *
     * @return DOCUMENT ME!
     */
    public PriceCalculator getPriceCalculator() {
        return priceCalculator;
    }

    /**
     * DOCUMENT ME!
     *
     * @param priceCalculator DOCUMENT ME!
     */
    public void setPriceCalculator(PriceCalculator priceCalculator) {
        this.priceCalculator = priceCalculator;
    }

    /**
     * DOCUMENT ME!
     *
     * @param configuredItem DOCUMENT ME!
     */
    public void setConfiguredItem(WebCatItem configuredItem) {
        this.configuredItem = configuredItem;
    }

    /**
     * DOCUMENT ME!
     *
     * @return DOCUMENT ME!
     */
    public WebCatItem getConfiguredItem() {
        return configuredItem;
    }

    /**
     * DOCUMENT ME!
     *
     * @return DOCUMENT ME!
     */
    public boolean isValid() {
        return valid;
    }

    /**
     * DOCUMENT ME!
     *
     * @param extendedSearch DOCUMENT ME!
     */
    public void setExtendedSearch(boolean extendedSearch) {
        this.extendedSearch = extendedSearch;
    }

    /**
     * DOCUMENT ME!
     *
     * @return DOCUMENT ME!
     */
    public boolean isExtendedSearch() {
        return extendedSearch;
    }

    protected void finalize() throws Throwable { 
        if (log.isDebugEnabled()) {
            log.debug("Finalize WebCatInfo: " + this);
        }
        
        theCatalog = null;
        catalogServer = null;
        currentAreaList = null;
        currentArea = null;
        currentItemList = null;
        savedItemList = null;
        currentItem = null;
        ociInfo = null;
        contractDisplayMode = null;
        httpsImageServer = null;
        priceCalculator = null;
        currentCUAList = null;
        cuaList = null;
        configuredItem = null;
        condPurpTable = null;
        serverType = null;
        
        super.finalize();
    }

    /**
     * This method is for interface IPCItemFilter. It is used to restrict product variant determination to the product
     * variants available in the catalog.
     *
     * @param items array of IPCItem's that are found as product variants during configuration
     *
     * @return array of IPCItem's which is the input reduced to those product variants that are included in the catalog
     *         as products
     */
    public IPCItem[] filterItems(IPCItem items[]) {

        log.debug("Filtering " + items.length + " items...");

        String products[] = new String[items.length];
        HashMap itemsMap = new HashMap();

        try {
            for (int i = 0; i < items.length; i++) {
                products[i] = items[i].getProductGUID();
                itemsMap.put(items[i].getProductGUID(), items[i]);
            }

            WebCatItemList prodList = getItemList(products);
            HashSet productSet = new HashSet();

            // loop at item list to group items with same product id
            Iterator itemIterator = prodList.iterator();

            while (itemIterator.hasNext()) {
                WebCatItem item = (WebCatItem) itemIterator.next();
                productSet.add(item.getProductID());
            }

            ArrayList output = new ArrayList(items.length);

            for (int i = 0; i < items.length; i++) {
                if (productSet.contains(items[i].getProductGUID())) {
                    output.add(items[i]);
                }
                else if (log.isDebugEnabled()) {
                    log.debug("IPCItemFilter: Product not in catalog '" + items[i].getProductId() + "' (" + items[i].getProductGUID() + ")");
                }
            }

            return (IPCItem[]) output.toArray(new IPCItem[output.size()]);
        }
        catch (IPCException i) {
            log.error("system.eai.exception", i);

            return null;
        }
    }

    /**
     * DOCUMENT ME!
     *
     * @return numPageLinks
     */
    public int getNumPageLinks() {
        return numPageLinks;
    }

    /**
     * DOCUMENT ME!
     *
     * @param noOfPageLinks
     */
    public void setNumPageLinks(int noOfPageLinks) {
        numPageLinks = noOfPageLinks;
    }

    /**
     * DOCUMENT ME!
     *
     * @return campaignGuid
     */
    public TechKey getCampaignGuid() {
        return campaignGuid;
    }

    /**
     * DOCUMENT ME!
     *
     * @return campaignId
     */
    public String getCampaignId() {
        return campaignId;
    }

    /**
     * DOCUMENT ME!
     *
     * @return doPricing
     */
    public boolean isDoPricing() {
        return doPricing;
    }

    /**
     * DOCUMENT ME!
     *
     * @return isSoldToEligible
     */
    public boolean isSoldToEligible() {
        return isSoldToEligible;
    }

    /**
     * DOCUMENT ME!
     *
     * @param campaignId
     */
    public void setCampaignId(String campaignId) {
	
	log.debug("catalog campaignId set to :" + campaignId);
	
        this.campaignId = campaignId;
        this.campaignGuid = null;
        setDoPricing(true);
    }

    /**
     * DOCUMENT ME!
     *
     * @param doPricing
     */
    private void setDoPricing(boolean doPricing) {
        this.doPricing = doPricing;
    }

    /**
     * DOCUMENT ME!
     *
     * @param isSoldToEligible
     */
    public void setSoldToEligible(boolean isSoldToEligible) {
        this.isSoldToEligible = isSoldToEligible;
    }

    /**
     * DOCUMENT ME!
     *
     * @param campaignGuid
     */
    public void setCampaignGuid(TechKey campaignGuid) {
	if(campaignGuid == null) {
		log.debug("catalog campaignGuid set to : null");
	}
	else {
		log.debug("catalog campaignGuid set to : " + campaignGuid.getIdAsString());
	}
		
        this.campaignGuid = campaignGuid;
    }

    /**
     * DOCUMENT ME!
     *
     * @return boolean 
     */
    public boolean isItemListEmpty() {
        return currentItemList == null;
    }

    /**
     * Returns the language of the web catalog.
     * 
     * @return language as string.
     */
    public String getLanguage() {
        return language;
    }
    
    /**
     * Returns the iso language of the web catalog.
     * 
     * @return iso language as string.
     */
    public String getLanguageIso() {
        return languageIso;
    }

    /**
     * Sets the language of the web catalog.
     * 
     * @param language of the catalog
     */
    public void setLanguage(String language) {
        this.language = language;
    }
    
    /**
     * Sets the iso language of the web catalog.
     * 
     * @param iso language of the catalog
     */
    public void setLanguageIso(String languageIso) {
        this.languageIso = languageIso;
    }

	/**
	 * Sets the maximum number of columns in the block view.
	 * 
	 * @param blockViewMaxCols as int
	 */
	public void setBlockViewMaxCols(int blockViewMaxCols) {
		this.blockViewMaxCols = blockViewMaxCols;
	}

	/**
	 * Returns the maximum number of columns in the block view.
	 * 
	 * @return maximum columns as int.
	 */
	public int getBlockViewMaxCols() {
		return blockViewMaxCols;
	}

	/**
	 * Sets the flag if Catalog Area should be displayed as block view.
	 * 
	 * @param catAreaShowAsBlockView as boolean
	 */
	public void setCatAreaShowAsBlockView(boolean catAreaShowAsBlockView) {
		this.catAreaShowAsBlockView = catAreaShowAsBlockView;
	}

	/**
	 * Returns the flag if Catalog Area should be displayed as block view.
	 * 
	 * @return catAreaShowAsBlockView as boolean.
	 */
	public boolean getCatAreaShowAsBlockView() {
		return catAreaShowAsBlockView;
	}

	/**
	 * Sets the page size displayed for a Catalog Area in the block view.
	 * 
	 * @param newPageSize as int
	 */
	public void setPageSizeCatAreaBV(int newPageSize) {
		this.pageSizeCatAreaBV = newPageSize;
	}

    /**
     * Returns the page size displayed for a Catalog Area.
     * 
     * @return page size as int.
     */
    public int getPageSizeCatArea() {
        int maxRows;
        if (this.catAreaShowAsBlockView) {
            maxRows = this.pageSizeCatAreaBV;
        }
        else {
            maxRows = this.itemPageSize;
        }
        return maxRows;
    }

    /**
     * Sets the minimum page size displayed for a Catalog Area in the block view.
     * 
     * @param newPageSize as int
     */
    public void setPageSizeCatAreaBVMin(int newPageSize) {
        this.pageSizeCatAreaBVMin = newPageSize;
    }

    /**
     * Returns the minimum value of page size displayed for a Catalog Area.
     * 
     * @return Minimum page size as int.
     */
    public int getPageSizeCatAreaMin() {
        int maxRows;
        if (this.catAreaShowAsBlockView) {
            maxRows = this.pageSizeCatAreaBVMin;
        }
        else {
            maxRows = this.itemPageSizeMin;
        }
        return maxRows;
    }

	/**
	 * Sets the flag if Catalog Search should be displayed as block view.
	 * 
	 * @param catSearchShowAsBlockView as boolean
	 */
	public void setCatSearchShowAsBlockView(boolean catSearchShowAsBlockView) {
		this.catSearchShowAsBlockView = catSearchShowAsBlockView;
	}

	/**
	 * Returns the flag if Catalog Search should be displayed as block view.
	 * 
	 * @return catSearchShowAsBlockView as boolean.
	 */
	public boolean getCatSearchShowAsBlockView() {
		return catSearchShowAsBlockView;
	}

    /**
     * Sets the page size displayed for a Catalog Search in the block view.
     * 
     * @param newPageSize as int
     */
    public void setPageSizeCatSearchBV(int newPageSize) {
        this.pageSizeCatSearchBV = newPageSize;
    }

    /**
     * Returns the page size displayed for a Catalog Search.
     * 
     * @return page size as int.
     */
    public int getPageSizeCatSearch() {
        int maxRows;
        if (this.catSearchShowAsBlockView) {
            maxRows = this.pageSizeCatSearchBV;
        }
        else {
            maxRows = this.itemPageSize;
        }
        return maxRows;
    }

    /**
     * Sets the minimum page size displayed for a Catalog Search in the block view.
     * 
     * @param newPageSize as int
     */
    public void setPageSizeCatSearchBVMin(int newPageSize) {
        this.pageSizeCatSearchBVMin = newPageSize;
    }

    /**
     * Returns the minimum value of page size displayed for a Catalog Search.
     * 
     * @return Minimum page size as int.
     */
    public int getPageSizeCatSearchMin() {
        int maxRows;
        if (this.catSearchShowAsBlockView) {
            maxRows = this.pageSizeCatSearchBVMin;
        }
        else {
            maxRows = this.itemPageSizeMin;
        }
        return maxRows;
    }

	/**
	 * Sets the flag if Bestseller should be displayed as block view.
	 * 
	 * @param bestsellerShowAsBlockView as boolean
	 */
	public void setBestsellerShowAsBlockView(boolean bestsellerShowAsBlockView) {
		this.bestsellerShowAsBlockView = bestsellerShowAsBlockView;
	}

	/**
	 * Returns the flag if Bestseller should be displayed as block view.
	 * 
	 * @return bestsellerShowAsBlockView as boolean.
	 */
	public boolean getBestsellerShowAsBlockView() {
		return bestsellerShowAsBlockView;
	}

    /**
     * Sets the page size displayed for a Bestseller in the block view.
     * 
     * @param newPageSize as int
     */
    public void setPageSizeBestsellerBV(int newPageSize) {
        this.pageSizeBestsellerBV = newPageSize;
    }

    /**
     * Returns the page size displayed for a Bestseller.
     * 
     * @return page size as int.
     */
    public int getPageSizeBestseller() {
        int maxRows;
        if (this.bestsellerShowAsBlockView) {
            maxRows = this.pageSizeBestsellerBV;
        }
        else {
            maxRows = this.itemPageSize;
        }
        return maxRows;
    }

    /**
     * Sets the minimum page size displayed for a Bestseller in the block view.
     * 
     * @param newPageSize as int
     */
    public void setPageSizeBestsellerBVMin(int newPageSize) {
        this.pageSizeBestsellerBVMin = newPageSize;
    }

    /**
     * Returns the minimum value of page size displayed for a Bestseller.
     * 
     * @return Minimum page size as int.
     */
    public int getPageSizeBestsellerMin() {
        int maxRows;
        if (this.bestsellerShowAsBlockView) {
            maxRows = this.pageSizeBestsellerBVMin;
        }
        else {
            maxRows = this.itemPageSizeMin;
        }
        return maxRows;
    }

	/**
	 * Sets the flag if Recommendations should be displayed as block view.
	 * 
	 * @param recommShowAsBlockView as boolean
	 */
	public void setRecommShowAsBlockView(boolean recommShowAsBlockView) {
		this.recommShowAsBlockView = recommShowAsBlockView;
	}

	/**
	 * Returns the flag if Recommendations should be displayed as block view.
	 * 
	 * @return recommShowAsBlockView as boolean.
	 */
	public boolean getRecommShowAsBlockView() {
		return recommShowAsBlockView;
	}

    /**
     * Sets the page size displayed for a Recommendation in the block view.
     * 
     * @param newPageSize as int
     */
    public void setPageSizeRecommBV(int newPageSize) {
        this.pageSizeRecommBV = newPageSize;
    }

    /**
     * Returns the page size displayed for a Recommendations.
     * 
     * @return page size as int.
     */
    public int getPageSizeRecomm() {
        int maxRows;
        if (this.recommShowAsBlockView) {
            maxRows = this.pageSizeRecommBV;
        }
        else {
            maxRows = this.itemPageSize;
        }
        return maxRows;
    }

    /**
     * Sets the minimum page size displayed for a Recommendation in the block view.
     * 
     * @param newPageSize as int
     */
    public void setPageSizeRecommBVMin(int newPageSize) {
        this.pageSizeRecommBVMin = newPageSize;
    }

    /**
     * Returns the minimum value of page size displayed for a Recommendations.
     * 
     * @return Minimum page size as int.
     */
    public int getPageSizeRecommMin() {
        int maxRows;
        if (this.recommShowAsBlockView) {
            maxRows = this.pageSizeRecommBVMin;
        }
        else {
            maxRows = this.itemPageSizeMin;
        }
        return maxRows;
    }

    /**
     * Sets the flag if the explosion of a package is done automatically in the product details screen.
     * 
     * @param automaticPackageExplosion as boolean
     */
    public void setAutomaticPackageExplosion(boolean automaticPackageExplosion) {
        this.automaticPackageExplosion = automaticPackageExplosion;
    }

    /**
     * Returns the flag if the explosion of a package is done automatically in the product details screen.
     * 
     * @return automaticPackageExplosion as boolean.
     */
    public boolean getAutomaticPackageExplosion() {
        return automaticPackageExplosion;
    }

    /**
     * set the new page size dependent of the list type
     *
     * @param newPageSize new page size
     * @param listType    type of the list for which the page size will be set
     */
    public void setPageSize(int newPageSize, int listType) {
        switch (listType) {
            case CV_LISTTYPE_BESTSELLER :
                if (this.bestsellerShowAsBlockView) {
                    setPageSizeBestsellerBV(newPageSize);
                }
                else {
                    setItemPageSize(newPageSize);
                }
                break;
            case CV_LISTTYPE_RECOMMENDATIONS :
                if (this.recommShowAsBlockView) {
                    setPageSizeRecommBV(newPageSize);
                }
                else {
                    setItemPageSize(newPageSize);
                }
                break;
            case CV_LISTTYPE_CATALOG_SEARCH :
                if (this.catSearchShowAsBlockView) {
                    setPageSizeCatSearchBV(newPageSize);
                }
                else {
                    setItemPageSize(newPageSize);
                }
                break;
            case CV_LISTTYPE_CATALOG_AREA :
                if (this.catAreaShowAsBlockView) {
                    setPageSizeCatAreaBV(newPageSize);
                }
                else {
                    setItemPageSize(newPageSize);
                }
                break;
            default :
                setItemPageSize(newPageSize);
                break;
        }
    }

    /**
     * get page size to be displayed on one page
     *
     * @param listType    type of the list for which the page size will be set
     * 
     * @return pageSize dependent on the type of list
     */
    public int getPageSize(int listType) {
        final String METHOD = "getPageSize(" + listType + ")";
        
        int pageSize = 0;
        switch (listType) {
            case CV_LISTTYPE_BESTSELLER :
                pageSize = getPageSizeBestseller();
                break;
            case CV_LISTTYPE_RECOMMENDATIONS :
                pageSize = getPageSizeRecomm();
                break;
            case CV_LISTTYPE_CATALOG_SEARCH :
                pageSize = getPageSizeCatSearch();
                break;
            case CV_LISTTYPE_CATALOG_AREA :
                pageSize = getPageSizeCatArea();
                break;
            default :
                pageSize = getItemPageSize();
                break;
        }
        log.debug(METHOD + ": pageSize =" + pageSize);
        return pageSize;
    }

    /**
     * Return the client of the catalog
     * 
     * @return IClient the client of the catalog
     */
    public IClient getCatalogClient() {
        return catalogClient;
    }
    
    /**
     * returns the server engine of the catalog
     * 
     * @return IServerEngine, the server enfine of the catalog.
     */
    public IServerEngine getCatalogServerEngine() {
        return serverEngine;
    }
    
	/**
	 * gets the used campaign id for compute the CUA recommendation prices
	 * 
	 * @return String, the used campaign id for CUA recommendation prices
	 */
	public String getRecommendationCampaignId() {
		return recommendationCampaignId;
	}

	/**
	 * sets the used campaignId for compute the CUA recommendation prices
	 * 
	 * @param string, the used CUA recommendation campaignId.
	 */
	public void setRecommendationCampaignId(String recommendationCampaignId) {
		this.recommendationCampaignId = recommendationCampaignId;
	}

	/**
	 * gets the used campaign id for compute the CUA bestseller prices
	 * 
	 * @return String, the used campaign id for CUA bestseller prices
	 */
	public String getBestsellerCampaignId() {
		return bestsellerCampaignId;
	}

	/**
	 * sets the used campaignId for compute the CUA bestseller prices
	 * 
	 * @param string, the used CUA bestseller campaignId.
	 */
	public void setBestsellerCampaignId(String bestsellerCampaignId) {
		this.bestsellerCampaignId = bestsellerCampaignId;
	}

	/**
     * gets the previous item list in case of Back navigation
     * from related products page
     * 
	 * @return WebCatItemList, 
	 */
	public WebCatItemList getSavedItemList() {
        log.entering("getSavedItemList");
        log.exiting();
		return savedItemList;
    }
    
	/**
     * sets the currentItemList as savedItemList in case of Back navigation
     * from related products page
     * 
	 * @param WebCatItemList, the currentItemList
	 */
	public void setSavedItemList(WebCatItemList currentItemList) {
        log.entering("setSavedItemList");
		this.savedItemList = currentItemList;
        log.exiting();
	}

	/**
	 * Returns the hookURL
	 * 
	 * @return hookURL, the hookURL
	 */
	public String getHookUrl() {
		return hookUrl;
	}

	public void setHookUrl(String hookUrl) {
		this.hookUrl = hookUrl;
	}

	/**
	 * returns the product detail type list.
	 * Following types could be returned:
	 * 0 -> no product detail list should be displayed
	 * 1 -> PROD_DET_LIST_CONTRACTS --> list of contracts
	 * 2 -> PROD_DET_LIST_ACCESSORIES --> list of accessories
	 * 3 -> PROD_DET_LIST_ALTERNATIVES --> list of alternatives
	 * 4 -> PROD_DET_LIST_REL_PROD --> list of related products
	 * 5 -> PROD_DET_LIST_EXCH_PROD --> list of exchanged products
	 * 
	 * @return the product detail list type 
	 */
	public int getProductDetailListType() {
		return productDetailListType;
	}

	/**
	 * sets the product detail list type
	 * 
	 * @param i, the type of the product detail list. Following values can be passed:
	 * 0 -> no product detail list should be displayed
	 * 1 -> PROD_DET_LIST_CONTRACTS --> list of contracts
	 * 2 -> PROD_DET_LIST_ACCESSORIES --> list of accessories
	 * 3 -> PROD_DET_LIST_ALTERNATIVES --> list of alternatives
	 * 4 -> PROD_DET_LIST_REL_PROD --> list of related products
	 * 5 -> PROD_DET_LIST_EXCH_PROD --> list of exchanged products
	 * 
	 */
	public void setProductDetailListType(int i) {
		productDetailListType = i;
	}

    /**
     * returns the flag if points items are in the catalog .
     * 
     * @return hasPtsItem as boolean 
     */
    public boolean hasPtsItem() {
      return this.hasPtsItems;
    }

	/**
	 * Returns the last search term used in quick search. If quick search has not
	 * been used, then an empty string is returned.
	 * @return The last search term used in quick search.
	 */
	public String getLastQuickSearchQueryString() {
		if(lastQuickSearchQueryString != null) {
			return lastQuickSearchQueryString;
		}
		return "";
	}

	/**
	 * Stores the last search term used in quick search.
	 * @param lastQuickSearchQueryString the used search term.
	 */
	public void setLastQuickSearchQueryString(String lastQuickSearchQueryString) {
		this.lastQuickSearchQueryString = lastQuickSearchQueryString;
	}

}
