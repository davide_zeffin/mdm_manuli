/*****************************************************************************

    Class:        WebCatSubItemList
    Copyright (c) 2006, SAP AG, Germany, All rights reserved.
    Created:      13.02.2006

*****************************************************************************/
package com.sap.isa.catalog.webcatalog;

import java.util.HashMap;

import com.sap.isa.catalog.boi.CatalogException;
import com.sap.isa.catalog.boi.IFilter;
import com.sap.isa.catalog.boi.IItem;
import com.sap.isa.catalog.boi.IQuery;
import com.sap.isa.catalog.boi.IQueryStatement;
import com.sap.isa.catalog.boi.ItemGroupData;
import com.sap.isa.catalog.boi.WebCatSubItemData;
import com.sap.isa.catalog.boi.WebCatSubItemListData;
import com.sap.isa.catalog.filter.CatalogFilterFactory;
import com.sap.isa.core.TechKey;
import com.sap.isa.core.logging.IsaLocation;

/**
 * Creates and holds a list of WebCatSubItems. This kind of lists will we used to hold 
 * the subitems that are created by an explosion. 
 */
public class WebCatSubItemList extends WebCatItemList implements WebCatSubItemListData {

    private static IsaLocation log = IsaLocation.getInstance(WebCatSubItemList.class.getName());

    /**
     * Stores key-value pairs of groupID-ItemGroups. The key is stored as <code>String</code>. 
     * 
     * @see     #clearItemGroups()
     * @see     #getItemGroup(String)
     * @see     #getItemGroupText(String)
     * @see     #setItemGroup(ItemGroup)
     */
    protected HashMap itemGroups = null;

    /**
     * Stores a link from the <code>TechKey</code> of the <code>WebCatSubItem</code> to
     * items in the <code>WebCatItemList</code>. This <code>HashMap</code> is updated, when
     * a <code>WebCatSubItem</change> changes its <code>techKey</code>.
     *  
     * @see #notifyWebCatSubItemTechKeyChanged(WebCatSubItem , TechKey)
     */
    protected HashMap itemTechKeyMap = null;

    /**
     * The constructor of the superclass will be overwritten. First it will call its super class 
     * implementation and than set the includeSubItems flag to true.
     * 
     * @param webCatInfo
     */
    public WebCatSubItemList(WebCatInfo webCatInfo) {

        super(webCatInfo);
        super.includeSubItems = true;
        populateAll = true;
    }

    /**
     * The constructor of the superclass will be overwritten. First it will call its super class 
     * implementation and than set the includeSubItems flag to true.
     *      
     * @param theCatalog
     * @param newArea
     */
    public WebCatSubItemList(WebCatInfo theCatalog, WebCatArea newArea) {

        super(theCatalog, newArea);
        super.includeSubItems = true;
        populateAll = true;
    }

    /**
     * The constructor of the superclass will be overwritten. First it will call its super class 
     * implementation and than set the includeSubItems flag to true.
     * 
     * @param theCatalog
     * @param query
     */
    public WebCatSubItemList(WebCatInfo theCatalog, IQuery query) {

        this(theCatalog);
        if (log.isDebugEnabled()) {
            log.debug("WebCatSubItemList:WebCatSubItemList(WebCatInfo " + theCatalog + " , IQuery " + query + ")");
        }
        setQuery(query);
        populateAll = true;
    }

    /**
     * The constructor of the superclass will be overwritten. First it will call its super class 
     * implementation and than set the includeSubItems flag to true.
     * 
     * @param theCatalog
     * @param query
     * @param includeAccessories
     */
    public WebCatSubItemList(WebCatInfo theCatalog, IQuery query, boolean includeAccessories) {

        this(theCatalog);
        this.includeAccessories = includeAccessories;

        if (log.isDebugEnabled()) {
            log.debug("WebCatSubItemList:WebCatSubItemList(WebCatInfo "
                + theCatalog
                + " , IQuery "
                + query
                + " , boolean "
                + includeAccessories
                + ")");
        }

        setQuery(query);
        populateAll = true;
    }

    /**
      * Adds a <code>WebCatSubItem</code> to this <code>WebCatSubItemList</code>. Additionally to
      * the add method of the <code>WebCatItemList</code> this maintains a HashMap with the 
      * <code>TechKey</code> of the items.
      * 
      * @param item is the WebCatSubItem, that has to be added.
      */
    public void addItem(WebCatSubItem item) {

        final String METHOD_NAME = "addItem(WebCatSubItem item)";
        log.entering(METHOD_NAME);

        super.addItem(item);

        // Maintain tech key map for sub items
        if (itemTechKeyMap == null) {
            itemTechKeyMap = new HashMap();
        }

        // items are always entered into itemTechKeyMap, also when this is initial.
        itemTechKeyMap.put(item.getTechKey().getIdAsString(), item);

        // set the subitems parent to this list. This enables the subitem to notify us
        // about changes in the itemTechKey.
        item.setParentWebCatSubItemList(this);

        log.exiting();
    }

    /**
     * Performs a area specific search for productIDs. The returned <code>WebCatSubItemList</code>
     * contains one row for each product. The order of the returned list might not be the same
     * than the order of the provided productIDs.
     * 
     * If productIDs[] is null or has no entries, an empty <code>WebCatSubItemList</code> is returned.
     * 
     * @param   productIDs[]    the product ids to be searched
     * @param   areaID          the area to be searched (category)
     *  
     * @return  unique list of WebCatSubItems
     */
    protected WebCatSubItemList areaSearch(String productIDs[], String areaID) {

        final String METHOD_NAME = "areaSearch(String productIDs[], String areaID)";
        log.entering(METHOD_NAME);
        
        if (log.isDebugEnabled()) {
            log.debug("areaID = " + areaID);
            for (int i = 0; i < productIDs.length; i++) {
                log.debug("productIds : " + productIDs[i]);
            }
        }

        WebCatSubItemList itemList = null;

        // return empty list if product is null
        if (productIDs == null) {
            log.debug("productIDs[] == null provided. Returning empty list.");
            log.exiting();
            return new WebCatSubItemList(getParentCatalog());
        }

        // return empty list if no products provided
        if (productIDs.length == 0) {
            log.debug("No productIDs[] provided. Returning empty list.");
            log.exiting();
            return new WebCatSubItemList(getParentCatalog());
        }

        // return empty list if areaID is null
        if (areaID == null) {
            log.debug("areaID == null provided. Returning empty list.");
            log.exiting();
            return new WebCatSubItemList(getParentCatalog());
        }

        // Create a filter with the object guids of the provided products
        CatalogFilterFactory fact = CatalogFilterFactory.getInstance();
        IFilter productFilter = fact.createAttrEqualValue("OBJECT_GUID", productIDs[0]);
        for (int i = 1; i < productIDs.length; i++) {

            IFilter filter = fact.createAttrEqualValue("OBJECT_GUID", productIDs[i]);
            productFilter = fact.createOr(productFilter, filter); // OR statement
        }
        
        // perform search on the given area only
        IFilter areaFilter = fact.createAttrEqualValue("AREA_GUID", areaID);
        productFilter = fact.createAnd(productFilter, areaFilter); // AND statement

        try {
            IQueryStatement queryStmt = getParentCatalog().getCatalog().createQueryStatement();
            queryStmt.setStatement(productFilter, null, null);

            IQuery query = getParentCatalog().getCatalog().createQuery(queryStmt);
            query.setRequestCategorySpecificAttributes(true);

            // create the list for this query and populate it complete
            itemList = new WebCatSubItemList(getParentCatalog(), query);
        }

        catch (CatalogException ex) {
            if (log.isDebugEnabled()) {
                log.debug(ex.getMessage() + ": Returning empty list.");
            }
            itemList = new WebCatSubItemList(getParentCatalog());
        }

        log.exiting();
        return itemList;
    }

    /**
     * Changes the <code>TechKey</code> of a <code>WebCatSubItem</code> that is a member of
     * this <code>WebCatSubItemList</code>.
     * 
     * The <code>techKey</code> as well as the <code>itemTeckKeyMap</code> is updated.
     * 
     * @param position of the <code>WebCatSubItem</code> in the <code>items</code> array
     * @param techKey the new <code>TechKey</code> of the <code>WebCatSubItem</code>
     * 
     * @return the <code>WebCatSubItem</code> that has been changed.
     * 
     * @see #notifyWebCatSubItemTechKeyChanged(WebCatSubItem , TechKey) 
     */
    public WebCatSubItem setSubItemTechKey(int position, TechKey techKey) {

        final String METHOD_NAME = "setSubItemTechKey(int position, TechKey techKey)";
        log.entering(METHOD_NAME);

        // Read the WebCatSubItem
        WebCatSubItem item = (WebCatSubItem) getItem(position);

        // Remove the old techKey form the TechKeyMap
        if (null != itemTechKeyMap) {
            itemTechKeyMap.remove(item.getTechKey().getIdAsString());
        }

        // Set the new techKey (updates itemTechKeyMap via notify ...
        item.setTechKey(techKey);

        log.exiting();
        return item;
    }

    /**
     * Clears all group texts that are stored in this WebCatSubItemList.
     */
    public void clearItemGroups() {
        
        log.entering("clearItemGroups()");

        if (null != itemGroups) {
            itemGroups.clear();
        }
        
        log.exiting();
    }
    
    /**
     * Cereates and returns a new itemGroupData object<br>
     * 
     * @param groupId the groupId
     * @param groupText teh group text
     * 
     * @retrun ItemGroupData the ItemGroupData oject
     */
    public ItemGroupData createItemGroupData(String groupId, String groupText) {
        return new ItemGroup(groupId, groupText);
    }

    /**
     * Overwrites the WebCatItemList.createNewItem method to generate a WebCatSubItem.
     */

    public WebCatItem createNewItem(WebCatItemKey itemKey, IItem catalogItem) {

        return getParentCatalog().getSubItem(itemKey, catalogItem);
    }
    
    /**
     * Returns the itemGroup for a given groupId.
     * 
     * @param groupId 
     * 
     * @return text for the given groupId, null if there is no text.
     */
    public ItemGroup getItemGroup(String groupId) {
        log.entering("getItemGroup()");
        
        ItemGroup itemGroup = null;

        if (null != itemGroups) {
            itemGroup = (ItemGroup) itemGroups.get(groupId);
        }

        log.exiting();
        return itemGroup;
    }
    
    /**
     * Returns the itemGroupData for a given groupId.
     * 
     * @param groupId 
     * 
     * @return text for the given groupId, null if there is no text.
     */
    public ItemGroupData getItemGroupData(String groupId) {
        log.entering("getItemGroupData()");
        log.exiting();
        return (ItemGroupData) getItemGroup(groupId);
    }


    /**
     * Returns the group text for a given groupId.
     * 
     * @param groupId
     * @return text for the given groupId, null if there is no group.
     */
    public String getItemGroupText(String groupId) {
        
        log.entering("getItemGroupText()");
        
        String text = null;
        ItemGroup itemGroup = getItemGroup(groupId);

        if (null != itemGroup) {
            text =  itemGroup.getGroupText();
            if (log.isDebugEnabled()) {
                log.debug("returned text: " + text);
            }
        }
        else {
            log.debug("itemGroup not found, so no text is returned");
        }

        log.exiting();
        return text;
    }

    /**
     * Returns one of more WebCatItem, because the itemID might not be unique.
     */
    public WebCatItem getItem(String itemID) {

        return super.getItem(itemID);
    }

    /**
     * Returns the WebCatSubItem entry for a given index in the WebCatItemList.
     *  
     * @return the WebCatSubItem for the given index, null if not existing.
     */
    public WebCatSubItem getSubItem(int index) {

        return (WebCatSubItem) super.getItem(index);
    }

    /**
     * Returns the WebCatItem entry for a given index in the WebCatItemList.
     *  
     * @return the WebCatItem for the given index, null if not existing.
     */
    public WebCatSubItemData getSubItemData(int index) {

        return (WebCatSubItemData) getSubItem(index);
    }

    /**
     * Handles the notification from a <code>WebCatSubItem</code>s when it changes its
     * <code>TechKey</code>.
     * 
     * This is necessary, because the <code>WebCatSubItemList</code> maintains an <code>TechKey</code>
     * index for the included <code>WebCatSubItems</code>.
     * 
     * @param webCatSubItem
     * @param techKey
     */
    public void notifyWebCatSubItemTechKeyChanged(WebCatSubItem webCatSubItem, TechKey techKey) {

        final String METHOD_NAME =
            "notifyWebCatSubItemTechKeyChanged(WebCatSubItem webCatSubItem, TechKey techKey)";
        log.entering(METHOD_NAME);

        // Remove the old techKey form the TechKeyMap
        if (null != itemTechKeyMap) {
            itemTechKeyMap.remove(webCatSubItem.getTechKey().getIdAsString());
        }

        // Insert the new techKey in the TechKeyMap
        if (itemTechKeyMap == null) {
            itemTechKeyMap = new HashMap();
        }
        itemTechKeyMap.put(techKey.getIdAsString(), webCatSubItem);

        log.exiting();
    }

    /**
     * Returns the WebCatSubItemData with the given techKey.
     * 
     * @param techKey of the item to be returned.
     * @return WebCatSubItemData object of the item, null if not existing.
     */
    public WebCatSubItemData getSubItemData(TechKey techKey) {

        WebCatSubItem item = null;

        if (itemTechKeyMap == null) {
            return null;
        }

        // check whether item is in item map
        item = (WebCatSubItem) itemTechKeyMap.get(techKey.getIdAsString());

        return item;
    }

    /**
     * Returns the <code>WebCatSubItem</code> with the given <code>techKey</code>.
     * 
     * @param  techKey of the item to be returned.
     * @return WebCatSubItem 
     */
    public WebCatSubItem getItem(TechKey techKey) {

        final String METHOD_NAME = "getItem(TechKey techKey)";
        log.entering(METHOD_NAME);

        if (itemTechKeyMap == null) {
            return null;
        }

        // check whether item is in item map
        WebCatSubItem item = (WebCatSubItem) itemTechKeyMap.get(techKey.getIdAsString());

        log.exiting();
        return item;
    }

    /**
     * Checks, if an item is relevant for the <code>WebCatSubItemlist</code></br>
     * 
     * @param   catalogItem to be checked.
     * @return  true, if the item has to be included in the list
     *          false, otherwise.
     */
    public boolean isRelevantForList(WebCatItem item) {

        if (isIncludeSubItems() ||
            item.isRatePlanCombination() ||
            item.isDependentComponent() ||
            item.isSalesComponent()) {
            return true;
        }

        if (!includeAccessories && item.isAccessory()) {
            return false;
        }

        return true;
    }

    /**
     * Fills the WebCatSubItemList with new WebCatSubItems for the given productIDs[] in the given 
     * order.<br>
     * 
     * If the same product is contained more than once in the input list, it will occur more than 
     * once in the populated WebcatSubItemList. Thus after the method is executed, the item list 
     * of the object holds a WebCatSubItem for every productID in the same sequence as in the 
     * productIDs input parameter.<br>
     * 
     * If there are problems with a single productID, the return value is set to false and an
     * empty WebCatSubItem is inserted in the WebCatItemList. This <code>WebCatSubItem</code>
     * is invalidated (setInvalid()) to indicate this situation.
     *  
     * @param productIDs array with ProductIDs
     * @param areaID     Identifier of the area
     * 
     * @return <code>true</code> if the population could be completed successfully, this means 
     *               for every given productID, a <code>WebCatSubItem</code> could be created.
     *         Otherwise it returns <code>false</code>.
     */
    public boolean populateWithProducts(String[] productIDs, String areaID) {

        final String METHOD_NAME = "populateWithProducts(String[] productIDs, String areaID)";
        log.entering(METHOD_NAME);

        boolean allItemsAreValid = true;

        // Create a unique list of WebCatItems for each productID
        WebCatSubItemList webCatSubItems = areaSearch(productIDs, areaID);

        // If nothing was found, log this situation and create empty result entries.
        if (webCatSubItems.size() <= 0) {
            log.debug("Got no results from areaSearch()");
        }
        if (log.isDebugEnabled()) {
            log.debug("Got " + webCatSubItems.size() + " webCatSubItems from areaSearch(" + areaID + ")");
        }

        // Check input parameter
        if (productIDs == null) {
            allItemsAreValid = false;
            log.debug("productIDs == null. Returning false.");
            log.exiting();
            return allItemsAreValid;
        }

        // Build the WebCatSubItemList in the sequence of the provided productIDs
        for (int i = 0; i < productIDs.length; i++) {

            // Handle empty entries in productIDs[]
            if (productIDs[i] == null) {
                if (log.isDebugEnabled()) {
                    log.debug("productIDs[" + i + "]== null; ignoring this");
                }
                continue;
            }
            
            if (productIDs[i].equals("00000000000000000000000000000000")) {
                if (log.isDebugEnabled()) {
                    log.debug("productIDs[" + i + "]== 00000000000000000; ignoring this (it is a group)");
                }
                continue;
            }

            WebCatSubItem subItem = null;
            WebCatItem webCatItem = null;
            
            // Create an itemKey for direct access of item in the items list.
            WebCatItemKey itemKey = new WebCatItemKey(getParentCatalog(), areaID, productIDs[i]);

            // Find the product in the webCatItems
            webCatItem = (WebCatItem) webCatSubItems.getItem(itemKey.getItemID());


            // If no WebCatItem could be created during area search, an empty WebCatSubItem
            // is returned and the caller has to handle this. If this occurs for any WebCatItem,
            // this method will return false. 
            // This is the case, if the product is not completely replicated into the catalog.
            if (null == webCatItem) {

                if (log.isDebugEnabled()) {
                    log.debug("Could not create a WebCatItem for product '"
                        + productIDs[i]
                        + "' in area '"
                        + areaID
                        + "'");
                    log.debug("Creating an empty WebCatSubItem");
                }

                // Indicate, that there have been problems with this product id.
                allItemsAreValid = false;

                // create an empty WebCatSubItem for this.
                subItem = new WebCatSubItem(itemKey);

                // invalidate this empty item
                subItem.setInvalid();
            }

            // There is a valid WebCatItem for the product
            else {

                // Create a new subitem with the webCatItem data
                subItem =
                    getParentCatalog().getSubItem(
                        webCatItem.getItemKey(),
                        webCatItem.getCatalogItem());

            } // if (null =! webCatItem)

            // Add the subitem to the items
            if (log.isDebugEnabled()) {
                log.debug("adding Subitem " + subItem);
            }
            addItem(subItem);

        } // for (int i = 0; ..-

        log.exiting();
        return allItemsAreValid;
    }

    /**
     * Removes an subitem from the subitem list and from all maintained maps for this list.
     *  
     * @param position is the index of the item in the items array.
     * @return the WebCatSubItem that has been removed.
     */
    public WebCatSubItem removeSubItem(int position) {

        final String METHOD_NAME = "removeSubItem(int position)";
        log.entering(METHOD_NAME);

        WebCatSubItem subItem = null;

        // Remove the WebCatItem
        WebCatItem item = super.removeItem(position);

        // Remove the TechKeyMap entry of the subitem
        if (null != itemTechKeyMap) {
            subItem = (WebCatSubItem) itemTechKeyMap.remove(item.getTechKey().getIdAsString());
        }

        // Remove the link to this list
        subItem.setParentWebCatSubItemList(null);

        log.exiting();
        return subItem;
    }
    
    /**
     * This method has to be called, to rebuidl the TechKeyMap, in case itesm from the
     * list have been removed via another method than removeSubItem(int) (e.h via an 
     * remove on an iterator) 
     * Than the TechKeyMap is created newly from all entries in the list.
     */
    public void rebuildTechKeyMap() {

        final String METHOD_NAME = "rebuildTechKeyMap";
        log.entering(METHOD_NAME);

        // Remove the TechKeyMap entry of the subitem
        if (null != itemTechKeyMap) {
            log.debug("Rebuilding TechKey Map");
            itemTechKeyMap.clear();
            
            for (int i = 0; i < items.size(); i++) {
                itemTechKeyMap.put(((WebCatItem) items.get(i)).getTechKey().getIdAsString(), items.get(i));   
            }
        }

        log.exiting();
    }

    /**
     * Returns the HashMap with the itemGroups.
     * 
     * @return HashMap with groupID / itemGroup entries.
     */
    public HashMap getItemGroups() {
        log.entering("getItemGroups()");
        log.exiting();
        return itemGroups;
    }

    /**
     * Removes the entry for a given group id.
     * 
     * @param groupId of the entry to be deleted.
     */
    public void removeItemGroup(String groupId) {
        
        log.entering("removeItemGroup()");

        if (null != itemGroups) {
            if (log.isDebugEnabled()) {
                log.debug("Remove itemGroup with Id: " + groupId);
            }
            itemGroups.remove(groupId);
        }
        
        log.exiting();
    }
    
    /**
     * Removes the entry for a given group id.
     * 
     * @param groupId of the entry to be deleted.
     */
    public void removeItemGroupData(String groupId) {
        
        log.entering("removeItemGroupData()");

        removeItemGroup(groupId);
        
        log.exiting();
    }

    /**
     * Sets the given group<br>
     * 
     * @param ItemGroupData  the ItemGroupData object to be stored
     */
    public void setItemGroupData(ItemGroupData itemGroup) {
        
        log.entering("setItemGroupData()");
        
        setItemGroup((ItemGroup) itemGroup);

        log.exiting();
    }
    
    /**
     * Sets the given group<br>
     * 
     * @param ItemGroup  the ItemGroupData object to be stored
     */
    public void setItemGroup(ItemGroup itemGroup) {
        
        log.entering("setItemGroup()");
        
        if (itemGroup != null) {
            // Create the HashMap, if not yet done
            if (null == itemGroups) {
                itemGroups = new HashMap();
                log.debug("create itemGroups Map");
            }

            // Insert the given group.
            if (log.isDebugEnabled()) {
                log.debug("Add group with Id: " + itemGroup.getGroupId() + " - text: " +
                           itemGroup.getGroupText() + " -displayCollapsed: " + itemGroup.displayCollapsed());
            }
            
            itemGroups.put(itemGroup.getGroupId(), itemGroup);
        }
        else {
            log.debug("itemGroup is null, nothing to be added.");
        }

        log.exiting();
    }

    /**
     * Sets the HashMap with the itemGroup objects.
     * 
     * @param itemGroups HashMap with groupID / itemGroup entries.
     */
    public void setItemGroups(HashMap itemGroups) {
        log.entering("setItemGroups()");
        this.itemGroups = itemGroups;
        log.exiting();
    }

    /**
     * Returns the number of items in the <code>WebCatSubItemList</code>.<br>
     * This is different from the <code>WebCatItemList.size()</code> because products can be included
     * multiple times in a subitem list.
     * 
     * @return number of items
     */
    public int size() {

        if (items == null) {
            return 0;
        }

        return items.size();
    }
    
	/**
	 * This function returns true if the populate Method should execute
	 * a default Sales Bundle
	 * explosion from the TREX data for every sales bundle prdouct that is populated
	 * 
	 * In the case of webcatsubitemlist, we never want to execute a TREX explosion
	 * 
	 * @return false if default Sales Bundle explosion won't be executed
	 */
	public boolean isExecDefSlsBdlExpl() {
	   return false;
	}

}