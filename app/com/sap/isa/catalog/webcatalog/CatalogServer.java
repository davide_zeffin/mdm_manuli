/*****************************************************************************

    Class:        CatalogServer
    Copyright (c) 2006, SAP AG, Germany, All rights reserved.
    Created:      2001

*****************************************************************************/
package com.sap.isa.catalog.webcatalog;

import java.util.Properties;

import com.sap.isa.catalog.boi.IServer;
import com.sap.isa.core.logging.IsaLocation;

/**
 * Stores connection information for a catalog server.
 */
public class CatalogServer implements IServer {

    private String aURL;
    private int port;
    private String catalogGuid;

    protected static IsaLocation log = IsaLocation.getInstance(CatalogServer.class.getName());

    public CatalogServer(String serverURL, String catalogGuid) {

        log.debug("CatalogServer:CatalogServer(" + serverURL + ", " + catalogGuid + ")");
        
        // Set defaults
        aURL = "";
        port = 0;

        
        if (serverURL != null) {

            int idx = serverURL.lastIndexOf(":");
            String portStr = serverURL.substring(idx + 1);
            try {
                port = Integer.parseInt(portStr);
                aURL = serverURL.substring(0, idx);
            }
            catch (NumberFormatException e) {
                port = 80;
                aURL = serverURL;
            }
        }
        
        this.catalogGuid = catalogGuid;
    }

    public String getURLString() {
        log.debug("CatalogServer:getURLString() " + aURL);
        return aURL;
    }

    public int getPort() {
        log.debug("CatalogServer:getPort() " + port);
        return port;
    }

    public Properties getProperties() {
        log.debug("CatalogServer:getProperties()");
        return new Properties();
    }

    public String getCatalogGuid() {
        log.debug("catalogGuid=\"" + catalogGuid + "\"");
        return catalogGuid;
    }

    // to do: check catalog initialization
    public String getGuid() {
        return getURLString();
    }
}