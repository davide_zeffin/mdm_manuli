package com.sap.isa.catalog.webcatalog;

import java.util.HashMap;
import java.util.Iterator;

import com.sap.isa.core.Iterable;

/**
 * Title:        ItemList
 * Description:  Stores a List of Attribute_Name
 *               Extends <code>java.util.HashMap</code>
 *               Adds more catalog-specific facilities to delete items from the hashMap.
 *
 * Copyright:    Copyright (c) 2001
 * Company:      SAPMarkets Europe GmbH
 * @version      1.0
 */

public class WebCatAttributeList extends HashMap implements Iterable {

      WebCatInfo catalog;

      public WebCatAttributeList(WebCatInfo catalog) {
        super();
        this.catalog = catalog;
      }

      public void deleteByGuid(String guid) {
          if (this.containsKey(guid)) this.remove(guid);
      }

      public void deleteByKey(String key) {
          String guid = catalog.getCatalog().getAttributeGuid(key);
          if (guid != null) deleteByGuid(guid);
      }

      public Iterator iterator() {
          if (this.values() != null ) return this.values().iterator();
          return null;
      }
}
