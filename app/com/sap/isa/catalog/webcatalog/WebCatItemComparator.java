package com.sap.isa.catalog.webcatalog;

import java.util.Comparator;

import com.sap.isa.core.logging.IsaLocation;

/**
 * Title:        WebCatalog
 * Description:  Comparator to compare WebCatalog Items
 * Copyright:    Copyright (c) 2001
 * Company:      SAPMarkets Europe GmbH
 * @version 1.5
 */

public class WebCatItemComparator extends WebCatBusinessObjectBase implements Comparator {

  /**
   * Name of the attribute to be used for compare
   */
  private String attribute;
  protected static IsaLocation log =
      IsaLocation.getInstance(WebCatItemComparator.class.getName());

  /**
   * Create new comparator
   * @param attribute attribute which is key for compare
   */
  public WebCatItemComparator(String attribute) {
    log.debug("WebCatItemComparator:WebCatItemComparator(String "+attribute+")");
    this.attribute = attribute;
  }

  /**
   * compare two WebCatItems regarding <code>attribute</code>
   * @param object1 first object to compare, needs to be of class WebCatItem
   * @param object2 second object to compare, needs to be of class WebCatItem
   * @return result int which is < 0 if object1 < object2, = 0 if they are equal
   *         and >0 if object1 > object2
   */
  public int compare(Object obj1, Object obj2) {
    WebCatItem item1 = (WebCatItem) obj1;
    WebCatItem item2 = (WebCatItem) obj2;
    if (attribute.equals("POS_NR")) {
        Integer int1, int2;
        try {
          int1 = new Integer(item1.getAttribute(attribute));
        } catch (NumberFormatException e) {
          int1 = new Integer(-1);
          log.debug("Product "+item1.getProduct()+", for the attribute "+attribute+",has an invalid value: |"+item1.getAttribute(attribute)+"|");
        }
        try {
          int2 = new Integer(item2.getAttribute(attribute));
        } catch (NumberFormatException e) {
          int2 = new Integer(-1);
          log.debug("Product "+item2.getProduct()+", for the attribute "+attribute+",has an invalid value: |"+item2.getAttribute(attribute)+"|");
        }
      return int1.compareTo(int2);
    }
    else {
      return item1.getAttribute(attribute).compareToIgnoreCase(item2.getAttribute(attribute));
    }

  }
}
