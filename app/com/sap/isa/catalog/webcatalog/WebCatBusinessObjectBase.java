/*
 * WebCatBusinessObjectBase
 *
 * Copyright (c) 2001, SAP AG, Germany, All rights reserved.
 * Created:      2001
 */
package com.sap.isa.catalog.webcatalog;

import com.sap.isa.businessobject.BusinessObjectBase;

/**
 * Base class for all Business Objects related with the Web Catalog
 */
public class WebCatBusinessObjectBase extends BusinessObjectBase {

    public WebCatBusinessObjectBase() {
    }

    public void release() {
    }
}
