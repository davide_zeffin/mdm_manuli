package com.sap.isa.catalog.webcatalog;

import java.util.Comparator;

import com.sap.isa.core.logging.IsaLocation;

/** 
 * Title:        WebCatalog
 * Description:  Comparator for WebCatAreas
 * Copyright:    Copyright (c) 2001
 * Company:      SAPMarkets Europe GmbH
 * @version 1.0
 */

public class WebCatAreaComparator extends WebCatBusinessObjectBase implements Comparator {

  /**
   * the attribute by which comparison is done
   */
  private String attribute;

  private static IsaLocation log =
      IsaLocation.getInstance(WebCatAreaComparator.class.getName());

  /**
   * Create new comparator
   * @param attribute the attribute which is key for the compare
   */
  public WebCatAreaComparator(String attribute) {
	if (log.isDebugEnabled())
    	log.debug("WebCatAreaComparator:WebCatAreaComparator(String "+attribute+")");
    this.attribute = attribute;
  }

  /**
   * Compare two WebCatAreas regarding <code>attribute</code>
   * @param object1 first object to compare, needs to be of class WebCatArea
   * @param object2 second object to compare, needs to be of class WebCatArea
   * @return result int which is < 0 if object1 < object2, = 0 if they are equal
   *         and >0 if object1 > object2
   */
  public int compare(Object obj1, Object obj2) {
    WebCatArea area1 = (WebCatArea) obj1;
    WebCatArea area2 = (WebCatArea) obj2;
    return area1.getAreaName().compareTo(area2.getAreaName());
  }
}
