package com.sap.isa.catalog.webcatalog;

import com.sap.isa.backend.boi.webcatalog.sharedcatalog.WebCatModifiable;
import com.sap.isa.catalog.*;
import com.sap.isa.catalog.boi.ICategory;
import com.sap.isa.catalog.boi.IItem;
import com.sap.isa.core.logging.IsaLocation;

/**
 * This extended WebCatInfo class allows modification of the catalog.
 */
public class WebCatInfoModifiable extends WebCatInfo
{

  /**
   * Value for the last visited information, representing the situation of
   * currently moving an object, i.e. the object has been selected, but the
   * target has to be defined.
   */
  public final static String MOVE_OBJECT     = "moveObject";

  /**
   * Temporary storage for modified objects.
   */
  protected WebCatModifiableObjectsContainer modifiedObjects;

  private static IsaLocation log =
      IsaLocation.getInstance(WebCatInfoModifiable.class.getName());
  
  private WebCatModifiable moveObj = null;
  
  /**
   * Constructor for WebCatInfoModifiable.
   */
  public WebCatInfoModifiable()
  {
    super();
    modifiedObjects = new WebCatModifiableObjectsContainer();
  }
  
  
  /**
   * Method getModifiableObjectsContainer.
   * @return WebCatModifiableObjectsContainer
   */
  public WebCatModifiableObjectsContainer getModifiableObjectsContainer()
  {
    return modifiedObjects;
  }
  
  /**
   * Temporarily store an object that will be moved.
   * @param moveObj
   */
  public void setObjectToMove(WebCatModifiable moveObj)
  {
    this.moveObj = moveObj;
  }
  
  /**
   * Returns the temporarily stored object.
   * @return WebCatModifiable
   */
  public WebCatModifiable getObjectToMove()
  {
    return moveObj;
  }

  
  /* ------------------------------- items ---------------------------------- */
  
  /**
   * Create a new catalog item in this catalog
   * @param areaId area the new item belongs to
   * @param productId the product of the new item
   * @return new modifiable catalog item
   */
  public WebCatItemModifiable createItem(String areaId, String product, String productId, String productDescription)
  {
	if (log.isDebugEnabled())
    	log.debug("create new item for "+product+" in area "+areaId);
    WebCatItemModifiable newItem = new WebCatItemModifiable(this, areaId, product, productId, productDescription);
    modifiedObjects.addItemData(newItem.getModificationData());
	if (log.isDebugEnabled())
    	log.debug("created new item "+newItem);
    return newItem;
  }

  protected WebCatItemModifiable getItem(WebCatModificationData modData)
  {
    if (modData == null)
      return null;
    if (modData.getModificationStatus() == WebCatModificationStatus.NEW_OBJECT)
      return new WebCatItemModifiable(this, modData);
    else
      return new WebCatItemModifiable(super.getItem(modData.getParentAreaIdOriginal(), modData.getId()), modData);
  }
  
  protected WebCatItemModifiable[] getItems(WebCatModificationData[] modData)
  {
    if (modData == null)
      return null;
    WebCatItemModifiable[] items = new WebCatItemModifiable[modData.length];
    for (int i=0; i<modData.length; i++)
    {
      items[i] = getItem(modData[i]);
    }
    return items;
  }
  
  /**
   * Get all new items of a given area. This method is usefull to find the new
   * items of an area, which cannot be retrieved from the backend catalog yet.
   * The method is called in WebCatItemList to extend the list of items of an
   * area.
   * @see com.sap.isa.catalog.webcatalog.WebCatItemList#populate(int)
   * @param areaId the area for which the new items should be determined
   */
  public WebCatItemModifiable[] getNewItems(String areaId)
  {
    return getItems(modifiedObjects.getItemsData(areaId));
  }
  
//  /** rfu: is this method needed?
//   * Get all new items of the catalog. This method is called in the end to
//   * transfer the new items to backend.
//   */
//  public WebCatItemModifiable[] getNewItems()
//  {
//    return getItems(modifiedObjects.getNewItemsData());
//  }
//
  /**
   * Check whether a given WebCatItem represents a modifiable catalog item.
   * If so, return a corresponding WebCatItemModifiable, otherwise return the
   * input item.
   * @param item the WebCatItem to be checked
   * @return corresponding WebCatItemModifiable if item is modifiable, 
   *         the input item otherwise
   */
  private WebCatItem checkModifiable(WebCatItem item)
  {
    // if it is already a WebCatItemModifiable: nothing to do
//    if (item instanceof WebCatItemModifiable) rfu: make sure to allways get same item!
//      return item;
    // if it is a known modified object, return that
    WebCatModificationData modItem = modifiedObjects.getItemData(item.getItemID());
    if (modItem != null)
      return new WebCatItemModifiable(item, modItem);
    // check if modification is allowed
    String modifFlag = item.getAttributeByKey(AttributeKeyConstants.OBJECT_MODIFIABLE_FLAG);
    // if modification is not allowed: return input item
    if ((modifFlag == null) ||
        modifFlag.trim().equals(""))
      return item;
    // otherwise (modification allowed): return new WebCatItemModifiable
    //   created from item
    else
      return new WebCatItemModifiable(item, null);
  }
  
  /**
   * @see com.sap.isa.catalog.webcatalog.WebCatInfo#getItem(String, String)
   */
  public WebCatItem getItem(String areaID, String itemID)
  {
    WebCatItem item = super.getItem(areaID, itemID);
    return checkModifiable(item);
  }

  /**
   * @see com.sap.isa.catalog.webcatalog.WebCatInfo#getItem(WebCatItemKey, IItem)
   */
  public WebCatItem getItem(WebCatItemKey itemKey, IItem catalogItem)
  {
    WebCatItem newItem = super.getItem(itemKey, catalogItem);
    return checkModifiable(newItem);
  }

  /**
   * @see com.sap.isa.catalog.webcatalog.WebCatInfo#getItem(WebCatItemKey)
   */
  public WebCatItem getItem(WebCatItemKey itemKey)
  {
    WebCatItem newItem = super.getItem(itemKey);
    return checkModifiable(newItem);
  }
  
  /**
   * Add a catalog item to the list of modified items
   * @param itemData item that is modified
   */
  void addToModifiedItems(WebCatModificationData itemData)
  {
    modifiedObjects.addItemData(itemData);
  }

  /**
   * Remove a modifiable item from the catalog. If it was a new item, it is
   * completely deleted. If it was an existing item, it is only flagged, as the 
   * removal information needs to be passed to the backend catalog in the end.
   */ 
  public void removeItem(WebCatModificationData itemData)
  {
    if (itemData.getModificationStatus().equals(WebCatModificationStatus.NEW_OBJECT))
    {
      modifiedObjects.removeItem(itemData, itemData.getParentAreaId());
    }
    else
    {
      addToModifiedItems(itemData);
      itemData.setModificationStatus(WebCatModificationStatus.DELETED_OBJECT);
    }
  }
  
  public void moveItem(WebCatModificationData itemData, String newParent)
  {
	if (log.isDebugEnabled())
    	log.debug("move "+itemData.getId()+" to "+newParent);
    itemData.setParentAreaId(newParent);
    addToModifiedItems(itemData);
  }  
  
  /* ------------------------------- areas ---------------------------------- */
    
  /**
   * Create a new catalog area in this catalog
   * @param areaId area the new item belongs to
   * @param productId the product of the new item
   * @return new modifiable catalog item
   */
  public WebCatAreaModifiable createArea(String areaId, 
                                         String description,
                                         WebCatArea parentArea)
  {
    WebCatAreaModifiable newArea = new WebCatAreaModifiable(this, areaId, description);
    if (parentArea != null)
      newArea.getModificationData().setParentAreaIdOriginal(parentArea.getAreaID());
    else
      newArea.getModificationData().setParentAreaIdOriginal(WebCatArea.ROOT_AREA);
    modifiedObjects.addAreaData(newArea.getModificationData());
    return newArea;
  }
  
  protected WebCatAreaModifiable getArea(WebCatModificationData modData)
  {
    if (modData == null)
      return null;
    return new WebCatAreaModifiable(super.getArea(modData.getId()), modData);
  }
  
  protected WebCatAreaModifiable[] getAreas(WebCatModificationData[] modData)
  {
    if (modData == null)
      return null;
    WebCatAreaModifiable[] areas = new WebCatAreaModifiable[modData.length];
    for (int i=0; i<modData.length; i++)
    {
      areas[i] = getArea(modData[i]);
    }
    return areas;
  }
  
  /**
   * Get all new areas of a given parent area. This method is usefull to find 
   * the new areas of a parent area, which cannot be retrieved from the 
   * backend catalog yet.
   * @param parentAreaId the area for which the new items should be determined
   */
  public WebCatAreaModifiable[] getNewAreas(String parentAreaId)
  {
    return getAreas(modifiedObjects.getNewAreasData(parentAreaId));
  }
  
  /**
   * Get all new areas of the catalog. This method is called in the end to
   * transfer the new areas to backend.
   */
  public WebCatAreaModifiable[] getNewAreas()
  {
    return getAreas(modifiedObjects.getNewAreasData(null));
  }

  /**
   * Check whether a given WebCatArea represents a modifiable catalog area.
   * If so, return a corresponding WebCatAreaModifiable, otherwise return the
   * input area.
   * @param area the WebCatArea to be checked
   * @return corresponding WebCatAreaModifiable if area is modifiable, 
   *         the input area otherwise
   */
  private WebCatArea checkModifiable(WebCatArea area)
  {
    // if it is already a WebCatAreaModifiable: nothing to do
    if (area instanceof WebCatAreaModifiable)
      return area;
    // if it is a known modified object, return that
    WebCatModificationData modArea = modifiedObjects.getAreaData(area.getAreaID());
    if (modArea != null)
      return new WebCatAreaModifiable(area, modArea);
    // check if modification is allowed
    String modifFlag = area.getDetailByKey(AttributeKeyConstants.OBJECT_MODIFIABLE_FLAG);
	if (log.isDebugEnabled())
    	log.debug("Area "+area.getAreaName()+", COPY_STATUS = "+modifFlag);
    // if modification is not allowed: return input area
    if ((modifFlag == null) ||
        modifFlag.trim().equals(""))
      return area;
    // otherwise (modification allowed): return new WebCatAreaModifiable
    //   created from area
    else
      return new WebCatAreaModifiable(area, null);
  }
  
  /**
   * @see com.sap.isa.catalog.webcatalog.WebCatInfo#getArea(String)
   */
  public WebCatArea getArea(String areaID)
  {
      // TODO getArea? funktioniert das so?
    WebCatArea area = super.getArea(areaID);
    return checkModifiable(area);
  }

  /**
   * @see com.sap.isa.catalog.webcatalog.WebCatInfo#getArea(ICategory)
   */
  public WebCatArea getArea(ICategory category)
  {
    WebCatArea area = super.getArea(category);
    return checkModifiable(area);
  }

  /**
   * Add a catalog area to the list of modified areas
   * @param area area that is modified
   */
  void addToModifiedAreas(WebCatModificationData areaData)
  {
    modifiedObjects.addAreaData(areaData);
  }

  /**
   * Remove a modifiable area from the catalog. If it was a new area, it is
   * completely deleted. If it was an existing area, it is only flagged, as the 
   * removal information needs to be passed to the backend catalog in the end.
   */ 
  public void removeArea(WebCatModificationData areaData)
  {
    if (areaData.getModificationStatus().equals(WebCatModificationStatus.NEW_OBJECT))
    {
      String parent = WebCatArea.ROOT_AREA;
      if (areaData.getParentAreaId() != null)
        parent = areaData.getParentAreaId();
      modifiedObjects.removeArea(areaData, parent);
    }
    else
    {
      addToModifiedAreas(areaData);
      areaData.setModificationStatus(WebCatModificationStatus.DELETED_OBJECT);
    }
  }
  
  public void moveArea(WebCatModificationData areaData, String newParent)
  {
	if (log.isDebugEnabled())
    	log.debug("move "+areaData.getId()+" to "+newParent);
    areaData.setParentAreaId(newParent);
    addToModifiedAreas(areaData);
  }  
  
  /**
   * This method is overwritten, as the original also sets the last visited 
   * information, which fails for moving items.
   * @see com.sap.isa.catalog.webcatalog.WebCatInfo#setCurrentArea(com.sap.isa.catalog.webcatalog.WebCatArea)
   */
  public void setCurrentArea(WebCatArea newCurrentArea)
  {
    // rfu: overwrite to not change lastVisited
    String lastVisit = getLastVisited();
    super.setCurrentArea(newCurrentArea);
    if (MOVE_OBJECT.equals(lastVisit))
      setLastVisited(MOVE_OBJECT);
  }

}
