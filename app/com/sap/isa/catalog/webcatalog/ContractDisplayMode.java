package com.sap.isa.catalog.webcatalog;

/**
 * Title:        Internet Sales, dev branch
 * Description:
 * Copyright:    Copyright (c) 2001
 * Company:
 * @author
 * @version 1.0
 */

public class ContractDisplayMode {

  public final static ContractDisplayMode UNKNOWN =
                          new ContractDisplayMode("Unknown");
  public final static ContractDisplayMode NO_CONTRACTS =
                          new ContractDisplayMode("No Contracts");
  public final static ContractDisplayMode SINGLE_CONTRACT =
                          new ContractDisplayMode("Display Single Contract");
  public final static ContractDisplayMode ALL_CONTRACTS =
                          new ContractDisplayMode("Display All Contracts");

  private String displayMode = "";

  private ContractDisplayMode(String mode) {
    this.displayMode = mode;
  }

  public ContractDisplayMode() {
    this.displayMode = "Unknown";
  }

  public static ContractDisplayMode getDisplayMode(String modeChr) {
    if (modeChr == null)
      return NO_CONTRACTS;
    String mch = modeChr.trim().toUpperCase();
    if (mch.equals("M"))
      return ALL_CONTRACTS;
    else if (mch.equals("S"))
      return SINGLE_CONTRACT;
    else if (mch.equals(""))
      return NO_CONTRACTS;
    else
      return UNKNOWN;
  }

  public String toString() {
    return displayMode;
  }

  public boolean showNoContracts() {
    return ((this == NO_CONTRACTS) ||
            ((this != SINGLE_CONTRACT) && (this != ALL_CONTRACTS)));
  }
  public boolean showSingleContract() {
    return (this == SINGLE_CONTRACT);
  }
  public boolean showAllContracts() {
    return (this == ALL_CONTRACTS);
  }
  public boolean showContracts() {
    return ((this == SINGLE_CONTRACT) || (this == ALL_CONTRACTS));
  }
}