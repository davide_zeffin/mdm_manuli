package com.sap.isa.catalog.webcatalog;

/**
 * Title:        Catalog UI Area Attribute
 * Description:  Contains info regarding area's specific attributes
 * Copyright:    Copyright (c) 2001
 * Company:      SAPMarkets
 * @author
 * @version 1.0
 */

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import com.sap.isa.catalog.boi.CatalogException;
import com.sap.isa.catalog.boi.IAttribute;
import com.sap.isa.catalog.boi.IDetail;
import com.sap.isa.core.logging.IsaLocation;


public class WebCatAreaAttribute extends WebCatBusinessObjectBase {

  public static final String DROP_DOWN    = "A";
  public static final String RADIO_BUTTON = "B";
  public static final String INPUT_FIELD  = " ";

  private String name;
  private String guid;
  private String description;
  private ArrayList values;
  private int order;
  private String style;
  private static IsaLocation log =
      IsaLocation.getInstance(WebCatAreaAttribute.class.getName());

  public WebCatAreaAttribute(IAttribute attrib) {
      name = attrib.getName();
      guid = attrib.getGuid();
      description = attrib.getDescription();
      values = new ArrayList();
      Iterator valuesIterator = attrib.getFixedValues();
      while (valuesIterator.hasNext()) {
        List defVal = (List)valuesIterator.next();
        if (defVal.size() > 1 && !((String)defVal.get(1)).equals("")) values.add(defVal.get(1));
              else values.add(defVal.get(0));
      }
      IDetail orderObj;
      try
      {
        orderObj = attrib.getDetail("POS_NR");
      }
      catch (CatalogException e)
      {
        log.warn("catalog.exception.backend",e);
        orderObj = null;
      }
      String orderStr;
      if (orderObj == null) orderStr = "-1";
          else orderStr = orderObj.getAsString();
      try {
        order = Integer.parseInt(orderStr);
      }
      catch (Exception e) { order = -1;}
      IDetail styleObj = null;
      try
      {
        styleObj = attrib.getDetail("ENTRYSTYLE");
      }
      catch (CatalogException e)
      {
        log.warn("catalog.exception.backend",e);
        styleObj = null;
      }
      String styleStr;
      if (styleObj == null) styleStr = WebCatAreaAttribute.INPUT_FIELD;
              else styleStr = styleObj.getAsString();
      if (styleStr.equals("A")) style = WebCatAreaAttribute.DROP_DOWN;
          else
          if (styleStr.equals("B")) style = WebCatAreaAttribute.RADIO_BUTTON;
              else
              if (styleStr.equals(" ")) style = WebCatAreaAttribute.INPUT_FIELD;
                else style = WebCatAreaAttribute.INPUT_FIELD;
  }

  public String getName() {
    return name;
  }

  public String getDescription() {
    return description;
  }

  public ArrayList getValues() {
    return values;
  }

  public int getOrder() {
    return order;
  }

  public String getStyle() {
    return style;
  }

  public String getGuid() {
    return guid;
  }
}
