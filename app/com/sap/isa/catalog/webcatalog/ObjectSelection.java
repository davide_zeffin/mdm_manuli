package com.sap.isa.catalog.webcatalog;

/**
 * Simple class that represents an object, identified by an id, and its
 * selection status. This is used within ObjectSelectionForm.
 * You can subclass this for special selections.
 */
public class ObjectSelection {

  private boolean selected;

  private String id;

  public ObjectSelection() {
    selected = false;
    id = null;
  }

  public boolean isSelected() {
    return selected;
  }

  public void setSelected(boolean selected) {
    this.selected = selected;
  }

  public void setId(String id) {
    this.id = id;
  }

  public String getId() {
    return id;
  }
}