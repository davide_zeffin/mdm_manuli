package com.sap.isa.catalog.webcatalog;

import org.apache.struts.upload.FormFile;

import com.sap.isa.backend.boi.webcatalog.sharedcatalog.WebCatModifiable;
import com.sap.isa.catalog.*;
import com.sap.isa.core.logging.IsaLocation;

/**
 * This class represents a WebCatItem extension that can be modified (normal
 * WebCatItem is &quot;read only&quot;).
 * This class is used in the environment of the shared manufacturer catalog.
 */
public class WebCatItemModifiable extends WebCatItem
                                  implements WebCatModifiable
{

  private static IsaLocation log =
      IsaLocation.getInstance(WebCatItemModifiable.class.getName());

  /**
   * object to store the item modification data
   */
  protected WebCatModificationData modificationData;

  /**
   * Constructor for new WebCatItemModifiable, representing a newly created
   * catalog item.
   * @param newCatalog
   * @param areaId
   * @param product
   * @param productId
   * @param productDescription
   */
  WebCatItemModifiable(WebCatInfo newCatalog,
                       String areaId,
                       String product,
                       String productId,
                       String productDescription)
  {
    super(new WebCatItemKey(newCatalog, areaId, productId));
    modificationData = new WebCatModificationData();
    modificationData.setId(productId+areaId);
    modificationData.setParentAreaId(areaId);
    modificationData.setProduct(productId, product);
    modificationData.setDescription(productDescription);
    modificationData.setModificationStatus(WebCatModificationStatus.NEW_OBJECT);
  }

  WebCatItemModifiable(WebCatInfo newCatalog, WebCatModificationData modData)
  {
    super(new WebCatItemKey(newCatalog, modData.getParentAreaId(), modData.getProductId()));
    modificationData = modData;
  }
  
  /**
   * Constructor for WebCatItemModifiable, representing an existing catalog
   * item that can be changed or deleted later.
   * @param item WebCatItem which is basis for this item
   */
  WebCatItemModifiable(WebCatItem item, WebCatModificationData modData)
  {
    super(item.getItemKey(), item.getCatalogItem());
    if (modData == null)
    {
      modificationData = new WebCatModificationData();
      modificationData.setId(item.getItemID());
      modificationData.setParentAreaId(item.getAreaID());
      modificationData.setModificationStatus(WebCatModificationStatus.UNCHANGED_OBJECT);
    }
    else
      modificationData = modData;
  }

  /**
   * Method getModificationData.
   * @return WebCatModificationData
   */
  public WebCatModificationData getModificationData()
  {
    return modificationData;
  }

  /**
   * Method to force reading of modification data from container.
   * This is useful to ensure that allways the current version is used.
   * @return WebCatModificationData
   */
  public WebCatModificationData readModificationData()
  {
    WebCatModificationData modData = ((WebCatInfoModifiable) getItemKey().getParentCatalog())
                                     .getModifiableObjectsContainer()
                                     .getItemData(getItemID());
    if (modData != null)
      modificationData = modData;
    return modificationData;
  }
  
  public void setModified()
  {
    modificationData.setModified();
    ((WebCatInfoModifiable) getItemKey().getParentCatalog()).addToModifiedItems(modificationData);
  }

  /**
   * Determine the modification status of this item
   * @return status modification status of this item as one of the defined
   *                constants.
   */
  public WebCatModificationStatus getModificationStatus()
  {
    return modificationData.getModificationStatus();
  }

  /**
   * Override inherited method to check for modified parameters which can not
   * be read from the original catalog item, but need to be taken from special
   * fields.
   * @see com.sap.isa.catalog.webcatalog.WebCatItem#getAttributeByKey(String)
   */
  public String getAttributeByKey(String key)
  {
    if ((key == AttributeKeyConstants.PRODUCT_ID) &&
        (modificationData.getProductId() != null))
      return modificationData.getProductId();
    else if ((key == AttributeKeyConstants.PRODUCT) &&
             (modificationData.getProduct() != null))
      return modificationData.getProduct();
    else if ((key == AttributeKeyConstants.PRODUCT_DESCRIPTION) &&
             (modificationData.getDescription() != null))
      return modificationData.getDescription();
    else if ((key == AttributeKeyConstants.PRODUCT_LONG_DESCRIPTION) &&
             (modificationData.getLongDescription() != null))
      return modificationData.getLongDescription();
    else if ((key == AttributeKeyConstants.ITEM_THUMBNAIL) &&
             (modificationData.getThumbNail() != null))
      return modificationData.getThumbNail();
    else if ((key == AttributeKeyConstants.ITEM_IMAGE) &&
             (modificationData.getImage() != null))
      return modificationData.getImage();
    else if (getCatalogItem() != null)
      return super.getAttributeByKey(key);
    else
      return null;
  }

  /**
   * @see com.sap.isa.catalog.webcatalog.WebCatItem#getAttribute(String)
   */
  public String getAttribute(String attrName)
  {
    if (getCatalogItem() != null)
      return super.getAttribute(attrName);
    else
      return null;
  }

  /**
   * @see com.sap.isa.catalog.webcatalog.WebCatItem#getAttributeAllValues(String)
   */
  public String[] getAttributeAllValues(String attrName)
  {
    if (getCatalogItem() != null)
      return super.getAttributeAllValues(attrName);
    else
      return new String[0];
  }

  /**
   * Change long description for this item.
   * @param description new item long description
   */
  public void setLongDescription(String description)
  {
    modificationData.setLongDescription(description);
    setModified();
  }

  /**
   * Method getLongDescription.
   * @return String
   */
  public String getLongDescription()
  {
    if (modificationData.getLongDescription() == null)
      return super.getAttributeByKey(AttributeKeyConstants.PRODUCT_LONG_DESCRIPTION);
    else
      return modificationData.getLongDescription();
  }

  /**
   * Method isLongDescriptionModified.
   * @return boolean
   */
  public boolean isLongDescriptionModified()
  {
    return (modificationData.getLongDescription() != null);
  }

  /**
   * Method setThumbNail.
   * @param thumb
   */
  public void setThumbNail(String thumb)
  {
    // rfu: what's this? through UploadDocument?
    modificationData.setThumbNail(thumb);
    setModified();
  }

  /**
   * Method getThumbNail.
   * @return String
   */
  public String getThumbNail()
  {
    return getAttributeByKey(AttributeKeyConstants.ITEM_THUMBNAIL);
  }

  /**
   * Method isThumbNailModified.
   * @return boolean
   */
  public boolean isThumbNailModified()
  {
    return (modificationData.getThumbNail() != null);
  }

  /**
   * Method setImage.
   * @param image
   */
  public void setImage(String image)
  {
    modificationData.setImage(image);
    setModified();
  }

  /**
   * Method getImage.
   * @return String
   */
  public String getImage()
  {
    return getAttributeByKey(AttributeKeyConstants.ITEM_IMAGE);
  }

  /**
   * Method isImageModified.
   * @return boolean
   */
  public boolean isImageModified()
  {
    return (modificationData.getImage() != null);
  }

  /**
   * Method setAreaID.
   * @param areaId
   */
  public void setAreaID(String areaId)
  {
    modificationData.setParentAreaId(areaId);
    setModified();
  }

  /**
   * @see com.sap.isa.catalog.webcatalog.WebCatItem#getAreaID()
   */
  public String getAreaID()
  {
    if (modificationData.getParentAreaId() == null)
      return super.getAreaID();
    else
      return modificationData.getParentAreaId();
  }

  /**
   * Delete this item (i.e. flag item for later deletion).
   */
  public void delete()
  {
    setModified();
    modificationData.setModificationStatus(WebCatModificationStatus.DELETED_OBJECT);
  }

  /**
   * Undelete this item (i.e. change flag back on flagged item).
   */
  public void undelete()
  {
    if (modificationData.getModificationStatus().equals(WebCatModificationStatus.DELETED_OBJECT))
    {
      modificationData.setModificationStatus(WebCatModificationStatus.CHANGED_OBJECT);
    }
    else
    {
      log.warn("sharedcat.err.undelete");
    }
  }

  /**
   * @see com.sap.isa.backend.boi.webcatalog.sharedcatalog.WebCatModifiable#addUploadDocument(java.lang.String, org.apache.struts.upload.FormFile)
   */
  public void addUploadDocument(String type, FormFile document)
  {
    if (UPLOAD_DOCUMENT_TYPE_IMAGE.equals(type))
      modificationData.setUploadImage(document);
    else if (UPLOAD_DOCUMENT_TYPE_THUMB.equals(type))
      modificationData.setUploadThumb(document);
    else
      log.warn("cat.shr.invdocupltyp",new String[] { type },null);
  }

  /**
   * @see com.sap.isa.backend.boi.webcatalog.sharedcatalog.WebCatModifiable#getUploadDocument(java.lang.String)
   */
  public FormFile getUploadDocument(String type)
  {
    if (UPLOAD_DOCUMENT_TYPE_IMAGE.equals(type))
      return modificationData.getUploadImage();
    else if (UPLOAD_DOCUMENT_TYPE_THUMB.equals(type))
      return modificationData.getUploadThumb();
    else
      return null;
  }

}
