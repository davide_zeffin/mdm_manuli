package com.sap.isa.catalog.webcatalog;

/**
 * Title:        Catalog UI Area Attribute
 * Description:  Contains info regarding area's specific attributes
 * Copyright:    Copyright (c) 2001
 * Company:      SAPMarkets
 * @author
 * @version 1.0
 */

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import com.sap.isa.catalog.boi.CatalogException;
import com.sap.isa.catalog.boi.IAttribute;
import com.sap.isa.catalog.boi.ICategory;
import com.sap.isa.core.logging.IsaLocation;

/**
 * This class provides with a HashMap of category specific attributes, along with their
 * default values and default values descriptions
 */


public class WebCatAreaAttributes extends WebCatBusinessObjectBase {

  private static IsaLocation log =
      IsaLocation.getInstance(WebCatAreaAttributes.class.getName());

private class DefValArrayList {

private ArrayList fixedValues;
private ArrayList fixedDescription;
private IAttribute theAttribute;

public DefValArrayList(IAttribute theAttribute) {
    this.theAttribute = theAttribute;
    fixedValues = new ArrayList();
    fixedDescription = new ArrayList();

    Iterator defValIter = theAttribute.getFixedValues();
    if (defValIter == null) return;
    while (defValIter.hasNext()) {
      List thePair = (List)defValIter.next();
      if (thePair.size() > 1) {
        fixedValues.add((String)thePair.get(0));
        if (!((String)thePair.get(1)).equals("")) fixedDescription.add((String)thePair.get(1));
                else fixedDescription.add((String)thePair.get(0));
      }
      if (thePair.size() == 1) {
        fixedValues.add((String)thePair.get(0));
        fixedDescription.add((String)thePair.get(0));
      }
    }
    //do some triks to retrieve the default values and to fill the arrays
}

public String getValue(String description) {
  int i;
  for (i=0;i<fixedDescription.size();i++) {
      if ( ((String)fixedDescription.get(i)).equalsIgnoreCase(description)) {
          break;
      }
  }
  if (i < fixedDescription.size() && i < fixedValues.size()) {
      return (String)fixedValues.get(i);
  } else
    return null;
}


public String getDescription(String value) {
  int i;
  for (i=0;i<fixedValues.size();i++) {
      if ( ((String)fixedValues.get(i)).equalsIgnoreCase(value)) {
          break;
      }
  }
  if (i < fixedValues.size() && i < fixedDescription.size()) {
      return (String)fixedDescription.get(i);
  } else
    return null;
}

}



  private HashMap theMap;

  public WebCatAreaAttributes(ICategory theCategory) {
    theMap = new HashMap();
    Iterator attribIter;
    try
    {
      attribIter = theCategory.getAttributes();
    }
    catch (CatalogException e)
    {
      log.error("catalog.exception.backend",e);
      return;
    }
    if (attribIter == null) return;
    while (attribIter.hasNext()) {
        IAttribute attribute = (IAttribute)attribIter.next();
        DefValArrayList defValArray = new DefValArrayList(attribute);
        theMap.put(attribute.getGuid(),defValArray);
    }
  }
  
  protected void finalize() throws Throwable { 
      if (log.isDebugEnabled()) {
          log.debug("Finalize WebCatAreaAttributes: " + this);
      }
        
      theMap = null;
        
      super.finalize();
  }

  public String getValue(String attrGuid, String desc) {
      if (theMap.get(attrGuid) != null) {
          return (((DefValArrayList)theMap.get(attrGuid)).getValue(desc));
      } else return null;
  }

  public String getDescription(String attrGuid, String value) {
      if (theMap.get(attrGuid) != null) {
          return (((DefValArrayList)theMap.get(attrGuid)).getDescription(value));
      } else return null;
  }

}
