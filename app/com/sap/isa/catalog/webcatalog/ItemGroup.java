/*****************************************************************************

    Class:        ItemGroup
    Copyright (c) 2006, SAP AG, Germany, All rights reserved.
    Created:      28.03.2006 

*****************************************************************************/
package com.sap.isa.catalog.webcatalog;

import com.sap.isa.catalog.boi.ItemGroupData;
import com.sap.isa.core.logging.IsaLocation;

/**
 * Class to store and process item group related informations, like texts
 * etc.
 */
public class ItemGroup implements ItemGroupData {
    
    protected String groupId;
    /** The language dependent group text */
    protected String groupText;
    /** field determines if the group should be displayed in collapsed or unfold mode */
    protected boolean displayCollapsed = true;
    
    private static IsaLocation log = IsaLocation.getInstance(ItemGroup.class.getName());

    /**
     * Create a ItemGroup object;
     */
    public ItemGroup(String groupId, String groupText) {
        super();
        log.entering("ItemGroup()");
        this.groupId = groupId;
        this.groupText = groupText;
        log.exiting();
    }

    /**
     * Returns true if the group should be displayed in collapsed mode
     * 
     * @return true if the group should be displayed in collapsed mode
     *         false otherwise
     */
    public boolean displayCollapsed() {
        log.entering("displayCollapsed()");
        log.exiting();
        return displayCollapsed;
    }

    /**
     * Returns the groupId.
     * 
     * @return String the groupId
     */
    public String getGroupId() {
        log.entering("getGroupId()");
        log.exiting();
        return groupId;
    }
    
    /**
     * Returns the groupText.
     * 
     * @return String the groupText
     */
    public String getGroupText() {
        log.entering("getGroupText()");
        log.exiting();
        return groupText;
    }

    public void setDisplayCollapsed(boolean displayCollapsed) {
        log.entering("setDisplayCollapsed()");
        this.displayCollapsed = displayCollapsed;
        log.exiting();
    }

    /**
     * Sets the groupId.
     * 
     * @param groupId the groupId
     */
    public void setGroupId(String groupId) {
        log.entering("setGroupId()");
        this.groupId = groupId;
        log.exiting();
    }

    /**
     * Sets the groupText.
     * 
     * @param String the groupText
     */
    public void setGroupText(String groupText) {
        log.entering("setGroupText()");
        this.groupText = groupText;
        log.exiting();
    }
}
