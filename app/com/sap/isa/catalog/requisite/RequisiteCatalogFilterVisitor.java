/*****************************************************************************
  Copyright (c) 2000, SAPMarkets Europe GmbH, Germany, All rights reserved.
  Created:      22 March 2001

  $Id: //sap/ESALES_base/30_SP_COR/src/_pcatAPI/java/com/sap/isa/catalog/requisite/RequisiteCatalogFilterVisitor.java#3 $
  $Revision: #3 $
  $Change: 150872 $
  $DateTime: 2003/09/29 09:18:51 $
*****************************************************************************/
package com.sap.isa.catalog.requisite;

// misc imports
import java.util.List;

import org.apache.struts.util.MessageResources;

import com.sap.isa.catalog.ICatalogMessagesKeys;
import com.sap.isa.catalog.boi.IHttpCommand;
import com.sap.isa.catalog.filter.CatalogFilter;
import com.sap.isa.catalog.filter.CatalogFilterAnd;
import com.sap.isa.catalog.filter.CatalogFilterAttrContain;
import com.sap.isa.catalog.filter.CatalogFilterAttrEqual;
import com.sap.isa.catalog.filter.CatalogFilterInvalidException;
import com.sap.isa.catalog.filter.CatalogFilterNot;
import com.sap.isa.catalog.filter.CatalogFilterOr;
import com.sap.isa.catalog.filter.CatalogFilterVisitor;

public class RequisiteCatalogFilterVisitor extends CatalogFilterVisitor
{
    private IHttpCommand theCommand;

    /**
    * Creates a new instance of the Requisite specfic visitor of the filter
    * expression tree.
    *
    * @param messResources  the resources for the error messages
    */
    public RequisiteCatalogFilterVisitor(MessageResources messResources)
    {
        super(messResources);
    }

    void evaluate(CatalogFilter rootFilter,
                List attributes,
                IHttpCommand theCommand)
        throws CatalogFilterInvalidException
    {
        // initialization of state information
        this.theCommand = theCommand;
        // start traversation of expression tree
        this.start(rootFilter, attributes);
        // clear state
        this.theCommand = null;
    }

    protected void visitAttrContainFilter(CatalogFilterAttrContain filterNode)
        throws CatalogFilterInvalidException
    {
        // super call for debugging and static variable check
        super.visitAttrContainFilter(filterNode);
        // transformation
        String theNameOfAttribute = filterNode.getName();
        String thePatternThatIsContained = filterNode.getPattern();
        theCommand.addCommandParameter(ItemsForDescValsCommand.DESCRIPTORS,theNameOfAttribute);
        theCommand.addCommandParameter(ItemsForDescValsCommand.DESCRIPTORVALUES,thePatternThatIsContained);
    }

    protected void visitAttrEqualFilter(CatalogFilterAttrEqual filterNode)
    throws CatalogFilterInvalidException
    {
        // super call for debugging
        super.visitAttrEqualFilter(filterNode);
        // transformation
        String theNameOfAttribute = filterNode.getName();
        String theValue = filterNode.getValue();
        theCommand.addCommandParameter(ItemsForDescValsCommand.DESCRIPTORS,theNameOfAttribute);
        theCommand.addCommandParameter(ItemsForDescValsCommand.DESCRIPTORVALUES,theValue);
    }

    protected void visitNotFilter(CatalogFilterNot filterNode)
        throws CatalogFilterInvalidException
    {
        // super call for debugging and static variable check
        super.visitNotFilter(filterNode);
        // transformation
        //We don't support this!
        String msg =
            getMessageResources().getMessage(ICatalogMessagesKeys.PCAT_FILTER_NOT_SUPP_EXPR,"NOT");
        throw new CatalogFilterInvalidException(msg);
    }

    protected void visitOrFilter(CatalogFilterOr filterNode)
        throws CatalogFilterInvalidException
    {
        // super call for debugging and static variable check
        super.visitOrFilter(filterNode);
        // transformation - drill down
        CatalogFilter lOperand = (CatalogFilter)filterNode.getOperands().get(0);
        lOperand.assign(this);
        CatalogFilter rOperand = (CatalogFilter)filterNode.getOperands().get(1);
        rOperand.assign(this);
    }

    protected void visitAndFilter(CatalogFilterAnd filterNode)
        throws CatalogFilterInvalidException
    {
        // super call for debugging and static variable check
        super.visitAndFilter(filterNode);
        // transformation - drill down
        CatalogFilter lOperand = (CatalogFilter)filterNode.getOperands().get(0);
        lOperand.assign(this);
        CatalogFilter rOperand = (CatalogFilter)filterNode.getOperands().get(1);
        rOperand.assign(this);
    }
}
