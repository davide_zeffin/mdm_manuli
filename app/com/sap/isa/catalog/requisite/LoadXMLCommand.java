/*****************************************************************************
  Copyright (c) 2000, SAPMarkets Europe GmbH, Germany, All rights reserved.
  Created:      22 March 2001

  $Id: //sap/ESALES_base/30_SP_COR/src/_pcatAPI/java/com/sap/isa/catalog/requisite/LoadXMLCommand.java#2 $
  $Revision: #2 $
  $Change: 129221 $
  $DateTime: 2003/05/15 17:20:58 $
*****************************************************************************/
package com.sap.isa.catalog.requisite;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.StringReader;
import java.io.StringWriter;
import java.net.URLEncoder;

import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMResult;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;

import org.apache.struts.util.MessageResources;
import org.w3c.dom.Document;

import com.sap.isa.catalog.boi.CatalogException;
import com.sap.isa.catalog.impl.CatalogClient;

public class LoadXMLCommand extends RequisiteHttpCommand
{
  /*
   *  RequisiteHttpCommand implements Serializable!
   *  Resurrection of concrete template in the readObject method
   */
  private static final String QUERY_LOAD_XML_TEMPLATE =
	"/com/sap/isa/catalog/requisite/requisiteLoadXML.xsl";

  public LoadXMLCommand(CatalogClient aClient,
						RequisiteServerEngine aServer,
						MessageResources aMessageResource)
  {
	super(aClient,aServer,aMessageResource);

	theURLTrunk =
	  new StringBuffer(theURLTrunk)
	  .append("&BugQcmd=loadXML").toString();

	loadFilter(QUERY_LOAD_XML_TEMPLATE);
	isSAXToSAX=false;
  }

  public void processPostParameters()
	throws CatalogException
  {
	Document theChanges =
	  (Document) theValues.get(theKeys.indexOf("loaderXML"));

	Document aseCX =  convertToECX(theChanges);

	StringWriter  stringOut = new StringWriter();

	TransformerFactory tFactory = TransformerFactory.newInstance();

	//build your simple copyAll xsl
	StringBuffer anotherBuffer = new StringBuffer("<?xml version=\"1.0\"?>");
	anotherBuffer.append("<xsl:stylesheet xmlns:xsl=\"http://www.w3.org/1999/XSL/Transform\" version=\"1.0\">");
	anotherBuffer.append("aBuffer.append(\"<xsl:template match=\"/\">");
	anotherBuffer.append("<xsl:copy-of select=\"*\"/>");
	anotherBuffer.append("</xsl:template>");
	anotherBuffer.append("</xsl:stylesheet>");

	try 
	  {
		Transformer transformer = tFactory.newTransformer(new StreamSource(new StringReader(anotherBuffer.toString())));

		transformer.transform(new DOMSource(aseCX), new StreamResult(stringOut));
		 
	  } 
	catch ( TransformerConfigurationException tce) 
	   {
		 throw new CatalogException(tce.getMessage() + tce.toString());
	   } // end of try-catch
	catch ( TransformerException te) 
	  {
		 throw new CatalogException(te.getMessage() + te.toString());		
	  } // end of catch

	theValues.set(theKeys.indexOf("loaderXML"),
				  URLEncoder.encode(stringOut.toString()));
  }

  /*************************** private ******************************************/

  /**
   * Read the templates again!
   */
  private void readObject(ObjectInputStream oo)
	throws IOException, ClassNotFoundException
  {
	oo.defaultReadObject();

	loadFilter(QUERY_LOAD_XML_TEMPLATE);
  }

  private Document convertToECX(Document theChanges)
	throws CatalogException
  {
	Document theChangesIneCX = null;

	try
	  {
		TransformerFactory tFactory = TransformerFactory.newInstance();

		if( tFactory.getFeature(DOMSource.FEATURE) &&
			tFactory.getFeature(DOMResult.FEATURE))
		  {
			// Process the stylesheet DOMSource and generate a Transformer.
			Transformer transformer = theFilter.newTransformer();

			// Use the DOM Document to define a DOMSource object.
			DOMSource xmlDomSource = new DOMSource(theChanges);

			// Set the base URI for the DOMSource so any relative URIs it contains can
			// be resolved.
			xmlDomSource.setSystemId("birds.xml");

			// Create an empty DOMResult for the Result.
			DOMResult domResult = new DOMResult();

			// Perform the transformation, placing the output in the DOMResult.
			transformer.transform(xmlDomSource, domResult);

			theChangesIneCX = domResult.getNode().getOwnerDocument();
		  }
		else
		  {
			throw new CatalogException("DOM node processing not supported!");
		  }
	  }
	catch(TransformerConfigurationException tce)
	  {
		throw new CatalogException(tce.toString());
	  }
	catch(TransformerException te)
	  {
		throw new CatalogException(te.toString());
	  }

	return theChangesIneCX;
  }
}
