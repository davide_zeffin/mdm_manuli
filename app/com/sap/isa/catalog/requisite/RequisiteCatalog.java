/*****************************************************************************
  Copyright (c) 2000, SAPMarkets Europe GmbH, Germany, All rights reserved.
  Created:      22 March 2001

  $Id: //sap/ESALES_base/30_SP_COR/src/_pcatAPI/java/com/sap/isa/catalog/requisite/RequisiteCatalog.java#3 $
  $Revision: #3 $
  $Change: 152353 $
  $DateTime: 2003/10/07 08:53:05 $
*****************************************************************************/
package com.sap.isa.catalog.requisite;

import java.io.InputStream;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.net.URLEncoder;
import java.text.MessageFormat;
import java.util.HashMap;
import java.util.Properties;

import org.apache.struts.util.MessageResources;
import org.w3c.dom.Document;

import com.sap.isa.catalog.ICatalogMessagesKeys;
import com.sap.isa.catalog.boi.CatalogException;
import com.sap.isa.catalog.boi.IClient;
import com.sap.isa.catalog.boi.IHttpCommand;
import com.sap.isa.catalog.boi.IHttpCommandManager;
import com.sap.isa.catalog.boi.IServerEngine;
import com.sap.isa.catalog.boi.IncompleteCommandException;
import com.sap.isa.catalog.impl.Catalog;
import com.sap.isa.catalog.impl.CatalogBuildFailedException;
import com.sap.isa.catalog.impl.CatalogClient;
import com.sap.isa.core.logging.IsaLocation;

public class RequisiteCatalog
    extends Catalog
    implements  IHttpCommandManager
{
    /*
     *  Catalog implements Serializable:
     *  Fields:
     *  static fields       :   no problem!
     *  theCommands         :   the Container implements Serializable!
     *                          The parts implement Serializable them self!
     *  aFormater           :   implements serializable
     */

    final static String BUG_Q_CMD_CATS=
        "com.sap.isa.catalog.requisite.CatsCommand";
    final static String BUG_Q_CMD_ROOTSCHEMA=
        "com.sap.isa.catalog.requisite.RootSchemaCommand";
    final static String BUG_Q_CMD_SCHEMA_FOR_CATS=
        "com.sap.isa.catalog.requisite.SchemaForCatsCommand";
    final static String BUG_Q_CMD_ITEMS_FOR_CATS=
        "com.sap.isa.catalog.requisite.ItemsForCatsCommand";
    final static String BUG_Q_CMD_ITEMS_FOR_DESC_VALS=
        "com.sap.isa.catalog.requisite.ItemsForDescValsCommand";
    final static String BUG_Q_CMD_ITEMS_FOR_BUGS_EYE_SEARCH=
        "com.sap.isa.catalog.requisite.ItemsForBugsEyeSearchCommand";
    final static String BUG_Q_CMD_LOAD_XML=
        "com.sap.isa.catalog.requisite.LoadXMLCommand";

    final static String[] COMMANDS =
    {
        BUG_Q_CMD_CATS,
        BUG_Q_CMD_ROOTSCHEMA,
        BUG_Q_CMD_SCHEMA_FOR_CATS,
        BUG_Q_CMD_ITEMS_FOR_CATS,
        BUG_Q_CMD_ITEMS_FOR_DESC_VALS,
        BUG_Q_CMD_ITEMS_FOR_BUGS_EYE_SEARCH,
		BUG_Q_CMD_LOAD_XML
    };

    private HashMap theCommands = new HashMap();

        // the logging instance
    private static IsaLocation theStaticLocToLog =
        IsaLocation.getInstance(RequisiteCatalog.class.getName());

    private MessageFormat aFormater = new MessageFormat("");

    /**
     * Generates a key for the cache from the passed parameters.<br>
     * This method is called from the {@link com.sap.isa.catalog.cache.CatalogCache}
     * and has to be <code>public static</code>.
     *
     * @param aMetaInfo  meta information from the EAI layer
     * @param aClient    the client spec of the catalog
     * @param aServer    the server spec of the catalog
     */
    public static String generateCacheKey(String theClassName,
                                            IClient aClient,
                                            IServerEngine aServer)
    {
        StringBuffer keyBuffer = new StringBuffer("http://");
        keyBuffer.append(aServer.getURLString())
            .append(':')
            .append(aServer.getPort())
            .append(aServer.getCatalogGuid())
            .append('?')
            .append("name=")
            .append(URLEncoder.encode(aClient.getName()))
            .append("&pwd=")
            .append(URLEncoder.encode(aClient.getPWD()))
            .append(aClient.getLocale().toString());

        return keyBuffer.toString();
    }

    /**
     * Created via Factory
     */
    protected RequisiteCatalog(
        RequisiteServerEngine theServerEngine,
        String theCatalogGuid,
        IClient aClient,
        MessageResources theMessageResources,
        Properties theProperties)
    {
        super(theServerEngine,theCatalogGuid);
        try
        {
                //will be used by logging!
            this.theMessageResources =theMessageResources;
            
            this.theClient = new CatalogClient(aClient);
            
            setCatalogBuilder(new RequisiteBuilder(this));

            initCommands();

            //Build the detail information for the new catalog
            getCatalogBuilder().buildDetails(this);

            isDistributable =
                (theProperties!=null?theProperties.getProperty("distributable","NO").equals("YES"):false);
            if (theProperties!=null &&
                theProperties.getProperty(PROPERTY_MAX_NUMBER_OF_ITEM_LEAFS)!=null) 
            {
                String theMaxNumberAsString =
                    theProperties.getProperty(PROPERTY_MAX_NUMBER_OF_ITEM_LEAFS);
                try //just in case the string can't be parsed
                {
                    int theInt = Integer.parseInt(theMaxNumberAsString);
                    this.theMaxNumberOfItemLeafs=theInt;                 
                } catch (Throwable t) 
                {
					theStaticLocToLog.debug(t.getMessage());
                        //you got your fair chance to set the number
                } // end of try-catch                
            } // end of if ()            
        }
        catch(CatalogBuildFailedException ce)
        {
            theStaticLocToLog.error(ICatalogMessagesKeys.PCAT_EXCEPTION,
                                new Object[]{ce.getLocalizedMessage()},
                                ce);
        }
    }

    public IHttpCommand getCommand(String aCommandKey)
    {
        return (IHttpCommand) theCommands.get(aCommandKey);
    }

    public HashMap getCommands()
    {
        return theCommands;
    }

    /**************************************************************************/

    private void initCommands()
    {
        for(int i=0; i < COMMANDS.length; i++)
        {
            String theKey = COMMANDS[i];
            try
            {
                Class clientClass =
                    Class.forName("com.sap.isa.catalog.impl.CatalogClient");
                Class serverClass =
                    Class.forName("com.sap.isa.catalog.requisite.RequisiteServerEngine");
                Class messageResourcesClass =
                    Class.forName("org.apache.struts.util.MessageResources");

                Class[] theArguments = {clientClass,
                                        serverClass,
                                        messageResourcesClass};
                Class theCommandClass = Class.forName(COMMANDS[i]);
                Constructor theCommandClassConstructor =
                    theCommandClass.getConstructor(theArguments);
                RequisiteHttpCommand theCommand =
                    (RequisiteHttpCommand) theCommandClassConstructor
                    .newInstance(new Object[]{theClient,theServerEngine,theMessageResources});
                this.theCommands.put(theKey,theCommand);
            }
            catch(IllegalAccessException iae)
            {
                theStaticLocToLog.error(ICatalogMessagesKeys.PCAT_EXCEPTION,new Object[]{iae.getLocalizedMessage()},iae);
            }
            catch(InvocationTargetException ite)
            {
                theStaticLocToLog.error(ICatalogMessagesKeys.PCAT_EXCEPTION,new Object[]{ite.getLocalizedMessage()},ite);
            }
            catch(NoSuchMethodException nsme)
            {
                theStaticLocToLog.error(ICatalogMessagesKeys.PCAT_EXCEPTION,new Object[]{nsme.getLocalizedMessage()},nsme);
            }
            catch(ClassNotFoundException cnfe)
            {
                theStaticLocToLog.error(ICatalogMessagesKeys.PCAT_EXCEPTION,new Object[]{cnfe.getLocalizedMessage()},cnfe);
            }
            catch(InstantiationException ie)
            {
                theStaticLocToLog.error(ICatalogMessagesKeys.PCAT_EXCEPTION,new Object[]{ie.getLocalizedMessage()},ie);
            }
        }
    }

	public void commitChangesRemote(Document theChanges)
		throws CatalogException
	{
		IHttpCommand loadXMLCommand = getCommand(BUG_Q_CMD_LOAD_XML);

		loadXMLCommand.setPostParameter("loaderXML",theChanges);

		try
		{
		    InputStream aResult = loadXMLCommand.excecute();
			checkForFailures(aResult);
		}
		catch(IncompleteCommandException icce)
		{
			throw new CatalogException(icce.getMessage() + icce.toString());
		}

		return;
	}

/* ************************* private **************************************** */

	/**
	 * Checks the response of the bugQ command for failures.
	 *
	 * @exception in case of a detected failure it throws an exception
	 */
	private void checkForFailures(InputStream aResult)
		throws CatalogException
	{
		return;
	}
}
