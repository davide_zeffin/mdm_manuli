/*****************************************************************************
  Copyright (c) 2000, SAPMarkets Europe GmbH, Germany, All rights reserved.
  Created:      22 March 2001

  $Id: //sap/ESALES_base/30_SP_COR/src/_pcatAPI/java/com/sap/isa/catalog/requisite/ItemsForCatsCommand.java#2 $
  $Revision: #2 $
  $Change: 129221 $
  $DateTime: 2003/05/15 17:20:58 $
*****************************************************************************/
package com.sap.isa.catalog.requisite;

//catalog imports
import java.io.IOException;
import java.io.ObjectInputStream;

import org.apache.struts.util.MessageResources;

import com.sap.isa.catalog.impl.CatalogClient;

public class ItemsForCatsCommand extends RequisiteHttpCommand
{
    /*
     *  RequisiteHttpCommand implements Serializable!
     *  Resurrection of concrete template in the readObject method
     */

    private static final String CATEGORY_ITEMS_TEMPLATE =
        "/com/sap/isa/catalog/requisite/requisiteCategoryItems.xsl";

    public ItemsForCatsCommand( CatalogClient aClient,
                                RequisiteServerEngine aServer,
                                MessageResources aMessageResource)
    {
        super(aClient,aServer,aMessageResource);

        theURLTrunk =
            new StringBuffer(theURLTrunk).append("&BugQcmd=itemsForCats").toString();

        loadFilter(CATEGORY_ITEMS_TEMPLATE);
    }

/*************************** private ******************************************/

    /**
     * Read the templates again!
     */
    private void readObject(ObjectInputStream oo)
        throws IOException, ClassNotFoundException
    {
        oo.defaultReadObject();

        loadFilter(CATEGORY_ITEMS_TEMPLATE);
    }
}
