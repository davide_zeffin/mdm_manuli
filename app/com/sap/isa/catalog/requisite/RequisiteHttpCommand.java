/*****************************************************************************
  Copyright (c) 2000, SAPMarkets Europe GmbH, Germany, All rights reserved.
  Created:      22 March 2001

  $Id: //sap/ESALES_base/30_SP_COR/src/_pcatAPI/java/com/sap/isa/catalog/requisite/RequisiteHttpCommand.java#2 $
  $Revision: #2 $
  $Change: 127411 $
  $DateTime: 2003/04/30 17:20:32 $
*****************************************************************************/
package com.sap.isa.catalog.requisite;

//catalog imports
import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.io.Serializable;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Locale;

import javax.xml.parsers.SAXParserFactory;
import javax.xml.transform.Templates;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMResult;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.sax.SAXResult;
import javax.xml.transform.sax.SAXSource;
import javax.xml.transform.stream.StreamSource;

import org.apache.struts.util.MessageResources;
import org.xml.sax.ContentHandler;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import com.sap.isa.catalog.ICatalogMessagesKeys;
import com.sap.isa.catalog.boi.CatalogException;
import com.sap.isa.catalog.boi.IHttpCommand;
import com.sap.isa.catalog.impl.CatalogClient;
import com.sap.isa.core.logging.IsaLocation;

public abstract class RequisiteHttpCommand
    implements  IHttpCommand,
        Serializable
{
        /*
         *  Implements Serializable:
         *  Fields:
         *  theClient               :   implements Serializable
         *  theServer               :   implements Serializable
         *  theKeys                 :   theContainer implements Serializable!
         *                              The parts implement Serializable themself!
         *  theValues               :   theContainer implements Serializable!
         *                              The parts implement Serializable themself!
         *  theFilter               :   transient will be resurrected during
         *                              deserialization of the concrete subclasses
         *  isSAXToSAX              :   no problem!
         *  theURLTrunk             :   no problem!
         *  theMessageFormat        :   implements Serializable
         *  theMessageResources     :   implements Serializable
         *  isPost                  :   no problem!
         *  isIdempotent            :   no problem!
         */

        //might become customizable
    public static final String[] theRequisiteLanguages =
    {
        "ar-AE",
        "ar-EG",
        "bg-BG",
        "bn-BD",
        "br-BR",
        "ca-AD",
        "cs-CZ",
        "da-DK",
        "de-DE",
        "el-GR",
        "en-US",
        "en-GB",
        "es-MX",
        "es-ES",
        "et-EE",
        "fi-FI",
        "fr-CA",
        "fr-FR",
        "hr-HR",
        "hu-HN",
        "in-ID",
        "is-IS",
        "it-IT",
        "iw-IL",
        "ja-JP",
        "ko-KR",
        "lt-LT",
        "lv-LV",
        "ms-MY",
        "nl-NL",
        "no-NO",
        "pl-PL",
        "pt-PT",
        "ro-RO",
        "ru-RU",
        "sk-SK",
        "sl-SI",
        "sv-SE",
        "th-TH",
        "tr-TR",
        "uk-UA",
        "vi-VN",
        "zh-CN",
        "zh-TW",
    };

    protected CatalogClient theClient;

    protected RequisiteServerEngine theServer;

    protected ArrayList theKeys = new ArrayList();

    protected ArrayList theValues = new ArrayList();

    protected ArrayList theXSLTKeys = new ArrayList();

    protected ArrayList theXSLTValues = new ArrayList();

    protected ArrayList thePostKeys = new ArrayList();

    protected ArrayList thePostValues = new ArrayList();

    protected transient Templates theFilter;

    protected boolean isSAXToSAX = true;

    protected String theURLTrunk;

    protected MessageFormat aFormater = new MessageFormat("");

    protected MessageResources theMessageResource;

    protected boolean isPost = false;

    protected boolean isIdempotent = true;

        // the logging instance
    private static IsaLocation theStaticLocToLog =
        IsaLocation.getInstance(RequisiteHttpCommand.class.getName());

        // the logging instance for performance issue logging
    private static IsaLocation theStaticPerfLocToLog =
        IsaLocation.getInstance("com.sap.isa.catalog.performance");

    public RequisiteHttpCommand(CatalogClient aClient,
                                RequisiteServerEngine aServer,
                                MessageResources aMessageResource)
    {
        this.theClient=aClient;

        this.theServer=aServer;

        this.theMessageResource=aMessageResource;

        StringBuffer aBuffer = new StringBuffer(theServer.getURLString());

        aBuffer.append(':')
            .append(theServer.getPort())
            .append(theServer.getCatalogGuid())
            .append("?cmd=bugq&username=")
            .append(URLEncoder.encode(theClient.getName()))
            .append("&password=")
            .append(URLEncoder.encode(theClient.getPWD()))
            .append("&language=")
            .append(URLEncoder.encode(determineRequisiteLanguage(theClient.getLocale())));
            //fucking stupid american fuckers
        theURLTrunk = aBuffer.toString();
    }

    public void setParameter(String aKey, Object aValue)
    {
        theKeys.add(aKey);
        theValues.add(aValue);
    }

    public void setPostParameter(String aKey, Object aValue)
    {
        thePostKeys.add(aKey);
        thePostValues.add(aValue);
    }

    public void setXSLTParameter(String aKey, Object aValue)
    {
        theXSLTKeys.add(aKey);
        theXSLTValues.add(aValue);
    }

    public void addCommandParameter(String aKey, Object aValue)
    {
        if(!theKeys.contains(aKey))
        {
            theKeys.add(aKey);
            theValues.add((String) aValue);
        }
        else
        {
            int theIndex = theKeys.indexOf(aKey);
            StringBuffer theCurrentValueAsBuffer =
                new StringBuffer((String)theValues.get(theIndex));
            theCurrentValueAsBuffer.append('|').append((String)aValue);
            theValues.set(theIndex,theCurrentValueAsBuffer.toString());
        }
    }

    public Iterator getKeys()
    {
        return theKeys.iterator();
    }

    public void excecute(ContentHandler aContendHandler)
        throws  SAXException,
        IOException,
        CatalogException
    {
        try
        {
            Transformer aTransformer = theFilter.newTransformer();

            int size = theXSLTKeys.size();
            for (int i = 0; i < size; i++)
            {
                aTransformer.setParameter(
                    (String)theXSLTKeys.get(i),
                    theXSLTValues.get(i));
            } // end of for ()

            InputSource theInputSource = new InputSource(excecute());

            if(theStaticPerfLocToLog.isInfoEnabled())
            {
                long start = System.currentTimeMillis();
                aTransformer.transform(
                    new SAXSource(theInputSource),
                    new SAXResult(aContendHandler));
                long end = System.currentTimeMillis();
                theStaticPerfLocToLog.info(  ICatalogMessagesKeys.PCAT_PERFORMANCE,
                                new Object[] {  "RequisiteHttpCommand.excecute()",
                                                Long.toString(end-start)},
                                null);
            }
            else
            {
                aTransformer.transform(
                    new SAXSource(theInputSource),
                    new SAXResult(aContendHandler));
            }
        }
        catch(TransformerConfigurationException tce)
        {
            theStaticLocToLog.error(ICatalogMessagesKeys.PCAT_EXCEPTION,
                                new Object[]{tce.getLocalizedMessage()},
                                tce);
        }
        catch(TransformerException te)
        {
            theStaticLocToLog.error(ICatalogMessagesKeys.PCAT_EXCEPTION,
                                new Object[]{te.getLocalizedMessage()},
                                te);
        }
        finally
        {
            theXSLTKeys.clear();
            theXSLTValues.clear();
        }

        return;
    }

    public InputStream excecute()
        throws CatalogException
    {
        StringBuffer theURLBuffer = new StringBuffer(theURLTrunk);

        processParameters();

        processPostParameters();

        InputStream theResult = null;

        try
        {
            for(int i = 0; i < theKeys.size(); i++)
                theURLBuffer.append("&").append((String) theKeys.get(i))
                    .append('=')
                    .append(URLEncoder.encode((String) theValues.get(i)));

            if(theStaticLocToLog.isInfoEnabled()&&!isPost)
            {
                theStaticLocToLog.info(ICatalogMessagesKeys.PCAT_SENDING_HTTP,
                                   new Object[] {"REQUISITE_HTTP_QUESTION",theURLBuffer.toString()}
                                   ,null);
            }

            URLConnection connection =
                getPreparedConnection(theURLBuffer.toString());

                //Unfortunately it is not possible to predict over the contentLenght
                //If Requisite will delivere content.
            if(theStaticLocToLog.isDebugEnabled())
            {
                    //log the result in the tempdir if the command is idempotent
                if(isIdempotent)
                    logResult(getPreparedConnection(theURLBuffer.toString()));

                theStaticLocToLog
                    .debug("Encoding: "+ connection.getContentEncoding());
                theStaticLocToLog
                    .debug("ContentLenght: "+ connection.getContentLength());
                theStaticLocToLog
                    .debug("ContentType: "+ connection.getContentType());
                theStaticLocToLog
                    .debug("Date: "+ connection.getDate());
                int counter=1;
                while(connection.getHeaderFieldKey(counter)!=null)
                {
                    String key = connection.getHeaderFieldKey(counter);
                    String value = connection.getHeaderField(counter);
                    theStaticLocToLog.debug("HeaderField: " + key + ':' + value);
                    counter++;
                }
            }
            theResult = connection.getInputStream();
        }
        catch (MalformedURLException murlex)
        {
            theStaticLocToLog.error(ICatalogMessagesKeys.PCAT_EXCEPTION,
                                new Object[]{murlex.getLocalizedMessage()},
                                murlex);
        }
        catch (IOException ioex)
        {
            theStaticLocToLog.error(ICatalogMessagesKeys.PCAT_EXCEPTION,
                                new Object[]{ioex.getLocalizedMessage()},
                                ioex);
        }
        finally
        {
            theKeys.clear();
            theValues.clear();
        }

        return theResult;
    }

    public void loadFilter(String theFilterName)
    {
        try
        {
                // Set the parser to non valiading as default!
                /**@todo later make it a configurable propertiy of the api **/
            SAXParserFactory theSAXParserFactory =
                SAXParserFactory.newInstance();
            if(theSAXParserFactory.isValidating())
                theSAXParserFactory.setValidating(false);

                // Instantiate a TransformerFactory.
            TransformerFactory tFactory = TransformerFactory.newInstance();

                // Determine whether the TransformerFactory supports
                // The use of SAXSource and SAXResult
            if ( isSAXToSAX && !(tFactory.getFeature(SAXSource.FEATURE)
                                 && tFactory.getFeature(SAXResult.FEATURE)))
            {
                String aMsg =
                    theMessageResource.getMessage(
                        ICatalogMessagesKeys.PCAT_XML_TRANS_FEATURE_MISSING);
                theStaticLocToLog.error(ICatalogMessagesKeys.PCAT_EXCEPTION,
                                    new Object[] {aMsg}, null);
            }
                // Determine whether the TransformerFactory supports
                // The use of DOMSource and DOMResult
            if ( !isSAXToSAX  && !(tFactory.getFeature(DOMSource.FEATURE)
                                   && tFactory.getFeature(DOMResult.FEATURE)))
            {
                String aMsg =
                    theMessageResource.getMessage(
                        ICatalogMessagesKeys.PCAT_XML_TRANS_FEATURE_MISSING);
                theStaticLocToLog.error(ICatalogMessagesKeys.PCAT_EXCEPTION,
                                    new Object[] {aMsg}, null);
            }

                // Create an XMLFilter for each stylesheet.
            InputStream templateAsStream =
                this.getClass().getResourceAsStream(theFilterName);

                //Jump out
            if(templateAsStream==null)
            {
                String aMsg =
                    theMessageResource.getMessage(
                        ICatalogMessagesKeys.PCAT_ERR_FILE_NOT_FOUND,
                        theFilterName);
                theStaticLocToLog.error(ICatalogMessagesKeys.PCAT_EXCEPTION,
                                    new Object[] {aMsg}, null);
                return;
            }

            theFilter =
                tFactory.newTemplates(new StreamSource(templateAsStream));

            if(theStaticLocToLog.isDebugEnabled())
            {
                theStaticLocToLog.debug("Initialized xslt filters!");
            }
        }
        catch(TransformerConfigurationException tce)
        {
            String aMsg = theMessageResource.getMessage(
                ICatalogMessagesKeys.PCAT_ERR_INIT_XSLT_PARSER);
            theStaticLocToLog.error(ICatalogMessagesKeys.PCAT_EXCEPTION,
                                new Object[] {aMsg}, tce);
        }
        catch(Throwable t)
        {
            theStaticLocToLog.error(ICatalogMessagesKeys.PCAT_EXCEPTION,
                                new Object[]{t.getLocalizedMessage()},t);
        }
        return;
    }

    public void processParameters()
        throws CatalogException
    {
        return;
    }

    public void processPostParameters()
        throws CatalogException
    {
        return;
    }

    public static String determineRequisiteLanguage(Locale aLocale)
    {
        String theResult = "en-US";

            //determine language
        String theLocaleLanguage = aLocale.getLanguage();
        ArrayList theHits = new ArrayList();
        int size = theRequisiteLanguages.length;
        for ( int i = 0; i < size; i++)
        {
            if (theRequisiteLanguages[i].startsWith(theLocaleLanguage))
            {
                theHits.add(theRequisiteLanguages[i]);
            } // end of if ()
        } // end of for ()

            //determine the country
        String theLocaleCountry = aLocale.getCountry();
        if ( theLocaleCountry.intern()!="".intern())
        {
            Iterator anIter = theHits.iterator();
            while (anIter.hasNext())
            {
                String aRequisiteLanguage = (String)anIter.next();
                if (!aRequisiteLanguage.regionMatches(3, theLocaleCountry, 0, 2) )
                {
                    anIter.remove();
                } // end of if ()
            } // end of while ()

        } // end of if ()

            //determine the subset of requisite languages that match the lanaguage and potentialy the country
        if (theHits.size()>=1)
        {
            theResult = (String)theHits.get(0);
        } // end of if ()

            // if nothing found fall back to en-US
        return theResult;
    }

/*************************** private ******************************************/


    private URLConnection getPreparedConnection(String aURLAsString)
        throws  IOException
    {
        if(theStaticLocToLog.isInfoEnabled()&&!isPost)
        {
            theStaticLocToLog.info(ICatalogMessagesKeys.PCAT_SENDING_HTTP,
                               new Object[] {"REQUISITE_HTTP_QUESTION",aURLAsString}
                               ,null);
        }

        URL url = new URL(aURLAsString);

        URLConnection connection = url.openConnection();

        if(isPost)
        {
            connection.setDoOutput(true);

            PrintWriter aWriter =
                new PrintWriter(connection.getOutputStream());

            for(int i = 0; i < this.thePostKeys.size(); i++)
            {
                String aKey = (String)thePostKeys.get(i);
                String aValue = (String)thePostValues.get(i);
                aWriter.print(aKey);
                aWriter.print("=");
                aWriter.print(URLEncoder.encode(aValue));
                if(i < this.thePostKeys.size()-1)
                    aWriter.print('&');
                else
                    aWriter.print('\n');
            }
            aWriter.close();
        }
        return connection;
    }

    private void logResult(URLConnection aURLConnection)
    {
        try
        {
            InputStream aResultStream = aURLConnection.getInputStream();

                //Make a reader
            InputStreamReader aInputStreamReader =
                new InputStreamReader(aResultStream);

            BufferedInputStream aBufferedInputStream =
                new BufferedInputStream(aResultStream);

                //Make a writer
            StringBuffer aFileNameBuffer =
                new StringBuffer(System.getProperty("java.io.tmpdir"));
            aFileNameBuffer.append(this.getClass().getName())
                .append(".result")
                .append(System.currentTimeMillis())
                .append(".xml");
            File aResultFile = new File(aFileNameBuffer.toString());

            FileOutputStream aFileOutputStream =
                new FileOutputStream(aResultFile);

            BufferedOutputStream aBufferedOutputStream =
                new BufferedOutputStream(aFileOutputStream);

            byte[] anByteArray = new byte[1024];
            int result = 0;
            while((result =
                   aBufferedInputStream.read(anByteArray,0,anByteArray.length))
                  != -1 )
            {
                aBufferedOutputStream.write(anByteArray,0,result);
            }
            aBufferedInputStream.close();
            aBufferedOutputStream.flush();
            aBufferedOutputStream.close();

        }
        catch(FileNotFoundException fne)
        {
            theStaticLocToLog.error(
                ICatalogMessagesKeys.PCAT_ERR_LOGGING,
                fne);
        }
        catch(IOException io)
        {
            theStaticLocToLog.error(
                ICatalogMessagesKeys.PCAT_ERR_LOGGING,
                io);
        }
        return;
    }
}
