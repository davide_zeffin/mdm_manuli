/*****************************************************************************
  Copyright (c) 2000, SAPMarkets Europe GmbH, Germany, All rights reserved.
  Created:      22 March 2001

  $Id: //sap/ESALES_base/30_SP_COR/src/_pcatAPI/java/com/sap/isa/catalog/requisite/RequisiteServerEngine.java#2 $
  $Revision: #2 $
  $Change: 127411 $
  $DateTime: 2003/04/30 17:20:32 $
*****************************************************************************/
package com.sap.isa.catalog.requisite;

import java.util.Properties;

import javax.xml.transform.Source;

import org.apache.struts.util.MessageResources;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.w3c.dom.Text;
import org.xml.sax.InputSource;

import com.sap.isa.catalog.ICatalogMessagesKeys;
import com.sap.isa.catalog.boi.IActor;
import com.sap.isa.catalog.boi.IClient;
import com.sap.isa.catalog.boi.IServer;
import com.sap.isa.catalog.boi.IServerEngine;
import com.sap.isa.catalog.impl.Catalog;
import com.sap.isa.catalog.impl.CatalogServerEngine;
import com.sap.isa.catalog.impl.CatalogSite;
import com.sap.isa.core.eai.BackendBusinessObjectMetaData;
import com.sap.isa.core.eai.BackendBusinessObjectParams;
import com.sap.isa.core.eai.BackendException;
import com.sap.isa.core.logging.IsaLocation;

/**
 * RequisiteServerEngine.java
 *
 *
 * Created: Wed Feb 13 09:27:03 2002
 *
 * @version 1.0
 */

public class RequisiteServerEngine
    extends CatalogServerEngine
{
    private String theProtocol ="http:";
    private String theHost;
    private int thePort;
            // the logging instance
    private static IsaLocation theStaticLocToLog =
        IsaLocation.getInstance(RequisiteServerEngine.class.getName());


        /**
         * Creates a new <code>RequisiteServerEngine</code> instance.
         * Allows creation via BEM mechanism
         *
         */
    public RequisiteServerEngine()
    {
        theType = IServerEngine.ServerType.REQUISITE;        
    }
    
    public RequisiteServerEngine (String theGuid,
                                  CatalogSite theSite,
                                  Document  theConfigFile)
        throws BackendException
    {
        super(theGuid,theSite,theConfigFile);
        theType = IServerEngine.ServerType.REQUISITE;        
    }

        /**
         * Inherited form BackendBussinesObject.
         * Called via the BEM creation mechanism after instantiating the instance.
         * aProperties <code>Properties</code> builded form the parm/value
         * pairs in the eai config file. 
         * @param aProperties a <code>Properties</code>  builded form the eai config file.
         * @param aParams a <code>BackendBusinessObjectParams</code> value
         */
    public void initBackendObject(Properties aPros, BackendBusinessObjectParams aParams)
    {
        CatalogSite.CatalogServerParams theParams =(CatalogSite.CatalogServerParams)aParams;
        theGuid = theParams.getGuid();
        theSite = theParams.getSite();
        theMessageResources = theSite.getMessageResources();
        theActor = theSite.getActor();
        Document aConfigFile = theParams.getDocument();
        this.theConfigDocument=aConfigFile;        
        init(aConfigFile);
        return;
    }

        /**
         * Inherited form BackendBussinesObject.
         * No action is triggered in the requisite case.
         */
    public void destroyBackendObject()
    {
        return;
    }

        /****************************** CachableItem ***************************/
    
    public boolean isCachedItemUptodate()
    {
        return true;
    }

        /**
         * Called from the cache via reflection when calculating the key for specific requisite server.
         *
         * @param metaInfo a <code>BackendBusinessObjectMetaData</code> value
         * @param aUser an <code>IActor</code> value
         * @param aServer an <code>IServer</code> value
         * @return a <code>String</code> value
         */
    public static String generateCacheKey(
        BackendBusinessObjectMetaData metaInfo,
        IActor aUser,
        IServer aServer)
    {
        StringBuffer aBuffer = new StringBuffer(RequisiteServerEngine.class.getName());
        String theServerGUID =
            (aServer!=null?
             aServer.getGuid():
             metaInfo.getBackendBusinessObjectConfig().getType());
        aBuffer.append(theServerGUID);
        aBuffer.append(aUser.getName());
        aBuffer.append(aUser.getPWD());
        return aBuffer.toString();
    }
    
        /**** implementations of abstract methods form CatalogServerEngine *****/

    
        /**
         * Initialize the requisite server via the config file/element provided in the site.xml file
         * for this server.
         * 
         * @param theConfigFile a <code>Document</code> value
         */
    protected void init(Document theConfigFile)
    {
        Element theConfigFileElement = (Element)theConfigFile.getElementsByTagName("ServerConfig").item(0);
        
        Element theHostElement  =
            (Element)theConfigFileElement.getElementsByTagName("requisite:host").item(0);
        theHost = ((Text)theHostElement.getFirstChild()).getData();
        Element thePortElement  =
            (Element)theConfigFileElement.getElementsByTagName("requisite:port").item(0);
        String aPort = ((Text)thePortElement.getFirstChild()).getData();
        thePort = Integer.valueOf(aPort).intValue();
        
        if (theStaticLocToLog.isDebugEnabled()) 
        {
            MessageResources theMessageResources = theSite.getMessageResources();
            StringBuffer aBuffer = new StringBuffer("Server: ");
            aBuffer.append(theHost);
            aBuffer.append(" Port: ");
            aBuffer.append(thePort);
            String theMessage = theMessageResources.getMessage(
                ICatalogMessagesKeys.PCAT_REQUISITE_SRV_INIT_DATA,aBuffer.toString());
            theStaticLocToLog.debug(theMessage);
        } // end of if ()

            //init theCatalogInfo Elements
        NodeList aCatalogList = theConfigFile.getElementsByTagName("Catalog");
        int size = aCatalogList.getLength();
        for (int i =0; i < size; i++) 
        {
            Element aCatlogElement = (Element)aCatalogList.item(i);
            CatalogInfo aCatalogInfo =
                new CatalogInfo(
                    aCatlogElement.getAttribute("ID"),
                    RequisiteCatalog.class.getName());
            theCatalogInfos.add(aCatalogInfo);
            NodeList theDetails = aCatlogElement.getElementsByTagName("Detail");
            int lenght = theDetails.getLength();
            for ( int j = 0; j < lenght; j++)
            {
                Element aDetail = (Element) theDetails.item(j);
                String theID = aDetail.getAttribute("ID");
                NodeList theValueChilds = aDetail.getElementsByTagName("Value");
                int childNum = theValueChilds.getLength();
                for (int k = 0; k < childNum; k++)
                {
                    Element  aValue = (Element) theValueChilds.item(k);
                    String theLocale = aValue.getAttribute("xml:lang").trim();
                    if (theLocale.equals("")) 
                        theLocale=null;
                    Text theContent = (Text)aValue.getFirstChild();
                    aCatalogInfo.setProperty(theID,theLocale,theContent.getData());
                } // end of for ()
            }
        } // end of for ()
    }

    public String getURLString()
    {
        return theProtocol+"//"+theHost;
    }

    public int getPort()
    {
        return thePort;
    }
    
    public Properties getProperties()
    {
        return new Properties();
    }

    public boolean hasServerFeature(IServerEngine.ServerFeature aFeature)
    {
        return false;
    }

    public IServerEngine.ServerFeature[] getServerFeatures()
    {
        return new IServerEngine.ServerFeature[]{};
    }

        /**
         * Instantiate a new catalog instance served on this server.
         * Only called if the catalog is not already cached.
         *
         * @param aCatalogInfo a <code>CatalogServerEngine.CatalogInfo</code> value
         * @param aClient an <code>IClient</code> value
         * @return a <code>Catalog</code> value
         */
    protected Catalog getCatalogRemote(
        CatalogServerEngine.CatalogInfo aCatalogInfo,
        IClient aClient)
    {
        RequisiteCatalog aNewCatalog =
            new RequisiteCatalog(this,
                                 aCatalogInfo.getGuid(),
                                 aClient,
                                 this.theMessageResources,
                                 aCatalogInfo.getProperties());
        return aNewCatalog;   
    }
    
    protected void addCatalogRemote(
        InputSource aSource,
        Source aMapping)
    {
        return;
    }

    protected void addCatalogRemote(Catalog catalog)
    {
        throw new UnsupportedOperationException("Not implemented yet!");
    }

    protected void deleteCatalogRemote(CatalogServerEngine.CatalogInfo aCatalogInfo)
    {
        return;
    }

    protected void deleteCatalogsRemote()
    {
        return;
    }

    protected Catalog createCatalogRemote()
    {
        return null;
    }

    public boolean isCatalogUpToDate(Catalog aCatalog)
    {
        return true;
    }

    protected void buildCatalogInfosRemote(String aCatalogNamePattern)
    {
        //todo: similar coding as in this.init()
    }

}// RequisiteServerEngine
