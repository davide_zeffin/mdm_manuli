/*****************************************************************************
  Copyright (c) 2000, SAPMarkets Europe GmbH, Germany, All rights reserved.
  Created:      22 March 2001

  $Id: //sap/ESALES_base/30_SP_COR/src/_pcatAPI/java/com/sap/isa/catalog/requisite/RequisiteBuilder.java#3 $
  $Revision: #3 $
  $Change: 152353 $
  $DateTime: 2003/10/07 08:53:05 $
*****************************************************************************/
package com.sap.isa.catalog.requisite;

//  misc imports
import java.io.IOException;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.Iterator;

import org.xml.sax.SAXException;

import com.sap.isa.catalog.ICatalogMessagesKeys;
import com.sap.isa.catalog.boi.CatalogException;
import com.sap.isa.catalog.boi.IHttpCommand;
import com.sap.isa.catalog.boi.IncompleteCommandException;
import com.sap.isa.catalog.digester.CallFactoryMethodRule;
import com.sap.isa.catalog.digester.CallMethodRule;
import com.sap.isa.catalog.digester.CallParamRule;
import com.sap.isa.catalog.digester.Digester;
import com.sap.isa.catalog.filter.CatalogFilter;
import com.sap.isa.catalog.filter.CatalogFilterInvalidException;
import com.sap.isa.catalog.impl.Catalog;
import com.sap.isa.catalog.impl.CatalogAttribute;
import com.sap.isa.catalog.impl.CatalogAttributeValue;
import com.sap.isa.catalog.impl.CatalogBuildFailedException;
import com.sap.isa.catalog.impl.CatalogBuilder;
import com.sap.isa.catalog.impl.CatalogCategory;
import com.sap.isa.catalog.impl.CatalogDetail;
import com.sap.isa.catalog.impl.CatalogItem;
import com.sap.isa.catalog.impl.CatalogQuery;
import com.sap.isa.core.logging.IsaLocation;

public class RequisiteBuilder extends CatalogBuilder
{
        /*
         *  CatalogBuilder implements Serializable:
         *  Fields:
         *  static fields       :   no problem!
         *  transient fields    :   no problem!
         *  theCatalogToBuild   :   implements Serializable
         *  aFormater           :   implements Serializable
         */

    private RequisiteCatalog              theCatalogToBuild;

    private static final int  CATALOG_ATTRIBUTES_DIGESTER = 0;

    private static final int  CATEGORIES_DIGESTER = 1;

    private static final int  CATEGORY_ATTRIBUTES_DIGESTER = 2;

    private static final int  ITEMS_DIGESTER = 3;

    private static final int  QUERY_ITEMS_DIGESTER =4;

        // the logging instance
    private static IsaLocation theStaticLocToLog =
        IsaLocation.getInstance(RequisiteBuilder.class.getName());

    private MessageFormat                 aFormater = new MessageFormat("");

    private transient Catalog             theCurrentCatalog = null;

    private transient CatalogCategory     theCurrentCategory = null;

    private transient CatalogQuery        theCurrentQuery = null;

    public RequisiteBuilder(RequisiteCatalog aCatalogToBuild)
    {
        this.theCatalogToBuild = aCatalogToBuild;
    }

    protected void buildDetailsInternal(Catalog aCatalog)
        throws CatalogBuildFailedException
    {
            /**@todo: do it via an HttpCommand*/
        aCatalog.setNameInternal("Requisite Catalog");
        StringBuffer aBuffer = new StringBuffer();
        aBuffer.append("Most catalogs support the concept of details to ");
        aBuffer.append("catalog entites. I.e. metadata that represent the ");
        aBuffer.append("structur of the catalog not the product data ");
        aBuffer.append("(usually). IDetail allows to access these data. ");
        aBuffer.append("Three specialized details: Name, Description and ");
        aBuffer.append("ThumbNail for the Catalog and the Category ");
        aBuffer.append("will be accessible via corresponding getters in the ");
        aBuffer.append("ICatalog and ICategory interface ");
        aBuffer.append("(if available in the corresponding catalog).");
        aCatalog.setDescriptionInternal(aBuffer.toString());

        aCatalog.setThumbNailInternal("mimes/catalog/galaxy.jpg");
        CatalogDetail aDetail = aCatalog.createDetailInternal();
        aDetail.setNameInternal("ADetail");
        aDetail.setAsStringInternal("AValue");
    }

    protected void buildDetailsInternal(CatalogCategory aCategory)
        throws CatalogBuildFailedException
    {
            /**@todo: do it via an HttpCommand*/
        StringBuffer aBuffer = new StringBuffer();
        aBuffer.append("Most catalogs support the concept of details to ");
        aBuffer.append("catalog entites. I.e. metadata that represent the ");
        aBuffer.append("structur of the catalog not the product data ");
        aBuffer.append("(usually). IDetail allows to access these data. ");
        aBuffer.append("Three specialized details: Name, Description and ");
        aBuffer.append("ThumbNail for the Catalog and the Category ");
        aBuffer.append("will be accessible via corresponding getters in the ");
        aBuffer.append("ICatalog and ICategory interface ");
        aBuffer.append("(if available in the corresponding catalog).");
        aCategory.setDescriptionInternal(aBuffer.toString());

        aCategory.setThumbNailInternal("mimes/catalog/galaxy.jpg");
        CatalogDetail aDetail = aCategory.createDetailInternal();
        aDetail.setNameInternal("ADetail");
        aDetail.setAsStringInternal("AValue");
    }

    protected void buildDetailsInternal(CatalogItem aItem)
        throws CatalogBuildFailedException
    {
            /**@todo: do it via an HttpCommand
               aItem.setName("Requiste Item");
               StringBuffer aBuffer = new StringBuffer();
               aBuffer.append("Most catalogs support the concept of details to ");
               aBuffer.append("catalog entites. I.e. metadata that represent the ");
               aBuffer.append("structur of the catalog not the product data ");
               aBuffer.append("(usually). IDetail allows to access these data. ");
               aBuffer.append("Three specialized details: Name, Description and ");
               aBuffer.append("ThumbNail, for the Catalog the Category and the Item ");
               aBuffer.append("will be accessible via corresponding getters in the ");
               aBuffer.append("(if available in the corresponding catalog).");
               aItem.setDescription(aBuffer.toString());

               aItem.setThumbNail("mimes/catalog/galaxy.jpg");
               CatalogDetail aDetail = aItem.createDetail();
               aDetail.setName("ADetail");
               aDetail.setAsString("AValue");
            */
    }

    protected void buildDetailsInternal(CatalogAttribute aAttribute)
        throws CatalogBuildFailedException
    {
            //Currently no details for requisite Attributes
    }

    protected void buildDetailsInternal(CatalogDetail aDetail)
        throws CatalogBuildFailedException
    {
            //Currently no details for requisite Details
    }

    protected void buildDetailsInternal(CatalogAttributeValue aValue)
        throws CatalogBuildFailedException
    {
            //Currently no details for requisite Attribute values
    }

    protected void buildRootCategoriesInternal(Catalog aCatalog)
        throws CatalogBuildFailedException
    {
        super.buildCategories(aCatalog);
    }

    protected void buildAllCategoriesInternal(Catalog aCatalog)
        throws CatalogBuildFailedException
    {
        super.buildCategories(aCatalog);
    }

    protected void buildCategoriesInternal(Catalog aCatalog)
        throws CatalogBuildFailedException
    {
        this.theCurrentCatalog = aCatalog;
		Digester aXMLCategoriesDigester = initDigester(CATEGORIES_DIGESTER);
		aXMLCategoriesDigester.push(aCatalog);
        try
        {
            IHttpCommand theCommand = theCatalogToBuild
                .getCommand(RequisiteCatalog.BUG_Q_CMD_CATS);
            theCommand.excecute(aXMLCategoriesDigester);
        }
        catch (IOException ioex)
        {
            theStaticLocToLog.error(ICatalogMessagesKeys.PCAT_EXCEPTION,
                                new Object[]{ioex.getLocalizedMessage()},ioex);
        }
        catch (SAXException ase)
        {
            theStaticLocToLog.error(ICatalogMessagesKeys.PCAT_EXCEPTION,
                                new Object[]{ase.getLocalizedMessage()},ase);
        }
        catch (IncompleteCommandException ice)
        {
            theStaticLocToLog.error(ICatalogMessagesKeys.PCAT_EXCEPTION,
                                new Object[]{ice.getLocalizedMessage()},ice);
        }
		catch (CatalogException ce)
		{
			throw new CatalogBuildFailedException(ce.getMessage());
		}
        finally
        {
            aXMLCategoriesDigester.pop();
        }
    }

    public void addCategory(CatalogCategory aCategory)
    {
        theCurrentCatalog.addCategory(aCategory);
        return;
    }

    protected void buildCategoriesInternal(CatalogCategory aCategory)
        throws CatalogBuildFailedException
    {
            // nothing to do all done after getRootCategories
            // or getCategories on the root
    }

    protected void buildItemsInternal(CatalogCategory aCategory)
        throws CatalogBuildFailedException
    {
        theCurrentCategory = aCategory;
		Digester aXMLItemsDigester = initDigester(ITEMS_DIGESTER);
		aXMLItemsDigester.push(theCurrentCategory);

        try
        {
                //make sure you got all global attributes first
            super.buildAttributes(theCurrentCategory.getRoot());
                //make sure you got all local attributes first
            super.buildAttributes(theCurrentCategory);

            IHttpCommand theCommand =
                theCatalogToBuild
                .getCommand(RequisiteCatalog.BUG_Q_CMD_ITEMS_FOR_CATS);

                //init the command
            synchronized (theCommand) {
            theCommand.setParameter("categories",aCategory.getGuid());

            theCommand.excecute(aXMLItemsDigester);}
            
        }
        catch (IOException ioex)
        {
            theStaticLocToLog.error(ICatalogMessagesKeys.PCAT_EXCEPTION,
                                new Object[]{ioex.getLocalizedMessage()},ioex);
        }
        catch (SAXException ase)
        {
            theStaticLocToLog.error(ICatalogMessagesKeys.PCAT_EXCEPTION,
                                new Object[]{ase.getLocalizedMessage()},ase);
        }
        catch (IncompleteCommandException ice)
        {
            theStaticLocToLog.error(ICatalogMessagesKeys.PCAT_EXCEPTION,
                                new Object[]{ice.getLocalizedMessage()},ice);
        }
		catch (CatalogException ce)
		{
			throw new CatalogBuildFailedException(ce.getMessage());
		}
		finally
		{
            aXMLItemsDigester.pop();
		}

    }

    protected void buildQueryInternal(CatalogQuery aQuery)
        throws CatalogBuildFailedException
    {
        theCurrentQuery = aQuery;

            //lay the query on top of this in the stack
        Digester aXMLQueryItemsDigester = initDigester(QUERY_ITEMS_DIGESTER);
		aXMLQueryItemsDigester.push(theCurrentQuery);

        try
        {
                //make sure you got all global attributes first
            super.buildAttributes(this.theCatalogToBuild);

                //figure out what type of query is done
            if(aQuery.getStatementAsString()!= null)
            {
                IHttpCommand theCommand =
                    theCatalogToBuild
                    .getCommand(RequisiteCatalog.BUG_Q_CMD_ITEMS_FOR_BUGS_EYE_SEARCH);

                    //init the command
                synchronized (theCommand){
                    theCommand.setParameter("quickSearch",aQuery.getStatementAsString());
                    theCommand.setParameter("isCountMode",aQuery.inCountMode()?"true":"false");                
                        //execute the command
                    theCommand.excecute(aXMLQueryItemsDigester);}
            }
            else
            {
                    //Prepare the list of attributes
                Iterator theAttributes=null;
                if(aQuery.isCategorySpecificSearch())
                {
                    theAttributes = aQuery.getParent().getAttributes();
                }
                else
                {
                    theAttributes = aQuery.getRoot().getAttributes();
                }

                ArrayList theListOfAQttributeKeys = new ArrayList();
                while(theAttributes.hasNext())
                {
                    CatalogAttribute theCurrentAttribute =
                        (CatalogAttribute) theAttributes.next();
                    theListOfAQttributeKeys.add(theCurrentAttribute.getGuid());
                }

                IHttpCommand theCommand =
                    theCatalogToBuild
                    .getCommand(RequisiteCatalog.BUG_Q_CMD_ITEMS_FOR_DESC_VALS);

                CatalogFilter aFilter =
                    aQuery.getCatalogQueryStatement().getFilter();
                
                synchronized(theCommand) { 
                    //init the command
                RequisiteCatalogFilterVisitor aInterpreter =
                    new RequisiteCatalogFilterVisitor(
                        theCatalogToBuild.getMessageResources());
                aInterpreter.evaluate(aFilter,theListOfAQttributeKeys,theCommand);
                    //finalize the initilization of the command
                if(aQuery.isCategorySpecificSearch())
                    theCommand.addCommandParameter("categories",
                                                   aQuery.getParent().getGuid());
                theCommand.setParameter("isCountMode",aQuery.inCountMode()?"true":"false");
                
                    //execute the command
                theCommand.excecute(aXMLQueryItemsDigester);}//end sync
            }
        }
        catch (CatalogException ce)
        {
            theStaticLocToLog.error(ICatalogMessagesKeys.PCAT_EXCEPTION,
                                new Object[]{ce.getLocalizedMessage()},ce);
        }
        catch (IOException ioex)
        {
            theStaticLocToLog.error(ICatalogMessagesKeys.PCAT_EXCEPTION,
                                new Object[]{ioex.getLocalizedMessage()},ioex);
        }
        catch (SAXException ase)
        {
            theStaticLocToLog.error(ICatalogMessagesKeys.PCAT_EXCEPTION,
                                new Object[]{ase.getLocalizedMessage()},ase);
        }
        catch (IncompleteCommandException ice)
        {
            theStaticLocToLog.error(ICatalogMessagesKeys.PCAT_EXCEPTION,
                                new Object[]{ice.getLocalizedMessage()},ice);
        }
        catch (CatalogFilterInvalidException cife)
        {
            theStaticLocToLog.error(ICatalogMessagesKeys.PCAT_EXCEPTION,
                                new Object[]{cife.getLocalizedMessage()},cife);
        }
		finally
		{
            aXMLQueryItemsDigester.pop();
		}
    }

    public CatalogItem createItem()
    {
        return theCurrentCategory.createItemInternal();
    }

    protected void buildAttributesInternal(Catalog aCatalog)
        throws CatalogBuildFailedException
    {
        theCurrentCatalog = aCatalog;
        Digester aXMLCatalogAttributesDigester = initDigester(CATALOG_ATTRIBUTES_DIGESTER);
		aXMLCatalogAttributesDigester.push(aCatalog);

        try
        {
            IHttpCommand theCommand = theCatalogToBuild
                .getCommand(RequisiteCatalog.BUG_Q_CMD_ROOTSCHEMA);
            theCommand.excecute(aXMLCatalogAttributesDigester);
        }
        catch (IOException ioex)
        {
            theStaticLocToLog.error(ICatalogMessagesKeys.PCAT_EXCEPTION,
                                new Object[]{ioex.getLocalizedMessage()},ioex);
        }
        catch (SAXException ase)
        {
            theStaticLocToLog.error(ICatalogMessagesKeys.PCAT_EXCEPTION,
                                new Object[]{ase.getLocalizedMessage()},ase);
        }
        catch (IncompleteCommandException ice)
        {
            theStaticLocToLog.error(ICatalogMessagesKeys.PCAT_EXCEPTION,
                                new Object[]{ice.getLocalizedMessage()},ice);
        }
		catch (CatalogException ce)
		{
			throw new CatalogBuildFailedException(ce.getMessage());
		}
        finally
        {
            aXMLCatalogAttributesDigester.pop();
        }
    }

    protected void buildAttributesInternal(CatalogCategory aCategory)
        throws CatalogBuildFailedException
    {
        theCurrentCategory = aCategory;
        Digester aXMLCategoryAttributesDigester = initDigester(CATEGORY_ATTRIBUTES_DIGESTER);
		aXMLCategoryAttributesDigester.push(aCategory);
        try
        {
            IHttpCommand theCommand =
                theCatalogToBuild
                .getCommand(RequisiteCatalog.BUG_Q_CMD_SCHEMA_FOR_CATS);
            
                //init the command
            synchronized (theCommand) {
            theCommand.setParameter("categories",aCategory.getGuid());

                //execute the command
            theCommand.excecute(aXMLCategoryAttributesDigester);}//end sync
        }
        catch (IOException ioex)
        {
            theStaticLocToLog.error(ICatalogMessagesKeys.PCAT_EXCEPTION,
                                new Object[]{ioex.getLocalizedMessage()},ioex);
        }
        catch (SAXException ase)
        {
            theStaticLocToLog.error(ICatalogMessagesKeys.PCAT_EXCEPTION,
                                new Object[]{ase.getLocalizedMessage()},ase);
        }
        catch (IncompleteCommandException ice)
        {
            theStaticLocToLog.error(ICatalogMessagesKeys.PCAT_EXCEPTION,
                                new Object[]{ice.getLocalizedMessage()},ice);
        }
		catch (CatalogException ce)
		{
			throw new CatalogBuildFailedException(ce.getMessage());
		}
        finally
        {
            aXMLCategoryAttributesDigester.pop();
        }
    }

/************************************ private *********************************/

    private Digester initDigester(int aType)
    {
		Digester aDigester = null;
		switch (aType)
		{
			case CATALOG_ATTRIBUTES_DIGESTER :
                    //the digester for the global attributes
                aDigester = new Digester();
                aDigester.setValidating(false);
                aDigester.push(this);

                CallFactoryMethodRule theRuleToCreateTheCatAtts =
                    new CallFactoryMethodRule(  aDigester,
                                                "createAttributeInternal",
                                                new String[]{"ID", "type", "quantity", "units"});
                aDigester.addRule("*/Attribute",
                                  theRuleToCreateTheCatAtts);

                CallMethodRule theCatAttributeNameSetRule =
                    new CallMethodRule(aDigester,"setNameInternal",1);
                aDigester.addRule("*/Attribute/Name",
                                  theCatAttributeNameSetRule);

                CallParamRule theCatAttributeNameValueRule =
                    new CallParamRule(aDigester,0);
                aDigester.addRule("*/Attribute/Name",
                                  theCatAttributeNameValueRule);
                break;
                
			case CATEGORIES_DIGESTER :
                    //The digester for the categories ( actually all in the reqisite case )
                aDigester = new Digester();
                aDigester.setValidating(false);
                aDigester.push(this);
                CallFactoryMethodRule theRuleToCreateTheCategories =
                    new CallFactoryMethodRule(  aDigester,
                                                "createChildCategoryInternal",
                                                new String[]{"ID"});
                aDigester.addRule("*/Category",
                                  theRuleToCreateTheCategories);

                CallMethodRule theCategoryNameSetRule =
                    new CallMethodRule(aDigester,"setNameInternal",1);
                aDigester.addRule("*/Category/Name",
                                  theCategoryNameSetRule);
                CallParamRule theCategoryNameValueRule =
                    new CallParamRule(aDigester,0);
                aDigester.addRule("*/Category/Name",
                                  theCategoryNameValueRule);
                break;

			case CATEGORY_ATTRIBUTES_DIGESTER :
                    //the digester for the category specific attributes
                aDigester = new Digester();
                aDigester.setValidating(false);
                aDigester.push(this);

                CallFactoryMethodRule theRuleToCreateTheCateAtts =
                    new CallFactoryMethodRule(  aDigester,
                                                "createAttributeInternal",
                                                new String[]{"ID", "type", "quantity", "units"});
                aDigester.addRule("*/Attribute",
                                  theRuleToCreateTheCateAtts);

                CallMethodRule theCateAttributeNameSetRule =
                    new CallMethodRule(aDigester,"setNameInternal",1);
                aDigester.addRule("*/Attribute/Name",
                                  theCateAttributeNameSetRule);

                CallParamRule theCateAttributeNameValueRule =
                    new CallParamRule(aDigester,0);
                aDigester.addRule("*/Attribute/Name",
                                  theCateAttributeNameValueRule);
                break;

			case ITEMS_DIGESTER :
                    // The digester for the items request
                aDigester = new Digester();
                aDigester.setValidating(false);
                aDigester.push(this);

                    //create the items
                CallFactoryMethodRule theRuleToCreateTheItems =
                    new CallFactoryMethodRule(aDigester,
                                              "createItemInternal",
                                              new String[]{"ID"});
                aDigester.addRule("*/Item",
                                  theRuleToCreateTheItems);
                    //create the attributes of the item
                CallFactoryMethodRule theRuleToCreateTheAttributes =
                    new CallFactoryMethodRule(  aDigester,
                                                "createAttributeValueInternal",
                                                new String[] {"attribute"});
                aDigester.addRule("*/Item/AttributeValue",
                                  theRuleToCreateTheAttributes);

                CallMethodRule theAttributeValueSetRule =
                    new CallMethodRule(aDigester,"setAsStringInternal",1);
                aDigester.addRule("*/Item/AttributeValue/Value",
                                  theAttributeValueSetRule);

                CallParamRule theAttributeValueValueRule =
                    new CallParamRule(aDigester,0);
                aDigester.addRule("*/Item/AttributeValue/Value",
                                  theAttributeValueValueRule);
                break;

			case QUERY_ITEMS_DIGESTER :
                    // The digester for the items query
                aDigester = new Digester();
                aDigester.setValidating(false);
                aDigester.push(this);

                    //The digester for the queries
                CallFactoryMethodRule theRuleToCreateTheQueryItems =
                    new CallFactoryMethodRule(  aDigester,
                                                "createQueryItemInternal",
                                                new String[]{"item","category"});
                aDigester.addRule("*/QueryItem",
                                  theRuleToCreateTheQueryItems);
                    //create the attributes of the item
                CallFactoryMethodRule theRuleToCreateTheAttributeValues =
                    new CallFactoryMethodRule(  aDigester,
                                                "createAttributeValueInternal",
                                                new String[]{"attribute"});
                aDigester.addRule("*/QueryItem/AttributeValue",
                                  theRuleToCreateTheAttributeValues);

                CallMethodRule theQueryItemsAttributeValueSetRule =
                    new CallMethodRule(aDigester,"setAsStringInternal",1);
                aDigester.addRule("*/QueryItem/AttributeValue/Value",
                                  theQueryItemsAttributeValueSetRule);

                CallParamRule theQueryItemsAttributeValueValueRule =
                    new CallParamRule(aDigester,0);
                aDigester.addRule("*/QueryItem/AttributeValue/Value",
                                  theQueryItemsAttributeValueValueRule);
                
                    //incase we are in count mode
                CallMethodRule theQueryItemsCountValueSetRule =
                    new CallMethodRule(aDigester,"setCountAsStringInternal",0);
                aDigester.addRule("Count",
                                  theQueryItemsCountValueSetRule);                
                break;
		}
        return aDigester;
    }
}
