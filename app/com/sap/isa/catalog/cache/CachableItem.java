/*****************************************************************************
  Copyright (c) 2000, SAPMarkets Europe GmbH, Germany, All rights reserved.
  Created:      03 April 2001

  $Id: //sap/ESALES_base/30_SP_COR/src/_pcatAPI/java/com/sap/isa/catalog/cache/CachableItem.java#2 $
  $Revision: #2 $
  $Change: 129221 $
  $DateTime: 2003/05/15 17:20:58 $
*****************************************************************************/

package com.sap.isa.catalog.cache;

import java.sql.Timestamp;

/**
 * Interface which all items have to implement that are managed by the
 * <code>CatalogCache</code>. The methods of this interface should only be
 * used inside the catalog package.
 * @version     1.0
 */
public interface CachableItem
{
    /**
     * Returns the time stamp that indicates the time when the item was used
     * for the last time.
     *
     * @return a time stamp that indicates the last access to cached item
     */
    public Timestamp getTimeStamp();

    /**
     * Sets the time stamp that indicates the time when the item was used
     * for the last time.
     */
    public void setTimeStamp();

    /**
     * Tests if the cached item is still valid i.e. if the original item
     * was not changed in the meantime.
     *
     * @return boolean that indicates if the chached item is still uptodate
     */
    public boolean isCachedItemUptodate();
}
