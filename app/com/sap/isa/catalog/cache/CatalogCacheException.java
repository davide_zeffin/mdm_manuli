/*****************************************************************************
  Copyright (c) 2000, SAPMarkets Europe GmbH, Germany, All rights reserved.
  Created:      03 April 2001

  $Id: //sap/ESALES_base/30_SP_COR/src/_pcatAPI/java/com/sap/isa/catalog/cache/CatalogCacheException.java#2 $
  $Revision: #2 $
  $Change: 129221 $
  $DateTime: 2003/05/15 17:20:58 $
*****************************************************************************/
package com.sap.isa.catalog.cache;

import com.sap.isa.catalog.boi.CatalogException;

/**
 * This exception is thrown if an error occurs during the caching of
 * a catalog instance.
 *
 * @version     1.0
 */
public class CatalogCacheException extends CatalogException
{
    /**
     * Creates a new instance of the exception with the given message text.
     *
     * @param msg  text of the exception
     */
    public CatalogCacheException(String msg)
    {
        super(msg);
    }

    public CatalogCacheException(String msg, Throwable aCause)
    {
        super(msg,aCause);
    }
    
}
