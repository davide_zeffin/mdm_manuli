/*****************************************************************************
  Copyright (c) 2000, SAPMarkets Europe GmbH, Germany, All rights reserved.

  $Id: //sap/ESALES_base/30_SP_COR/src/_pcatAPI/java/com/sap/isa/catalog/cache/CatalogCache.java#4 $
  $Revision: #4 $
  $Change: 144038 $
  $DateTime: 2003/08/21 17:57:25 $t
*****************************************************************************/
package com.sap.isa.catalog.cache;

import java.lang.ref.SoftReference;
import java.lang.reflect.Method;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Properties;

import com.sap.isa.catalog.boi.IActor;
import com.sap.isa.catalog.boi.IClient;
import com.sap.isa.catalog.boi.IServer;
import com.sap.isa.catalog.boi.IServerEngine;
import com.sap.isa.catalog.impl.Catalog;
import com.sap.isa.core.eai.BackendBusinessObjectConfig;
import com.sap.isa.core.eai.BackendBusinessObjectMetaData;
import com.sap.isa.core.init.InitializationEnvironment;
import com.sap.isa.core.logging.IsaLocation;

/**
 * This class implements the caching functionality of the catalog API. The
 * class is implemented as a singleton so that in one VM only one cache exists.
 * The catalogs are managed independent of the session context. <BR/>
 *
 * <B>IMPORTANT: </B> The caching strategy is very simple. If the default
 * strategy decides that one catalog can be removed from the cache the reference
 * to this catalog is deleted in the cache. All (unknown) users of this catalog
 * (and of the referenced objects) can still work with this old instance. The
 * references are still valid. But if a new user requests the same catalog he
 * will get a new instance and this new instance is again put into the cache.
 * The old instance is automatically removed from the garbage collector if no
 * user holds a reference to it. This simple strategy is the only way to avoid
 * dangling references, since we do not know who is (still) using the instances
 * that were removed from the cache!!
 *
 * @version     1.0
 */
public class CatalogCache
{
        /**
         * Constant that defines the parameter name for the expiration time.
         */
    public final static String CACHE_EXPIRATION = "expiration";

        /**
         * Constant that defines the parameter name for the removal time.
         */
    public final static String CACHE_REMOVAL = "removal";

    private final static long EXPIRATION_TIME = 24; // default expiration time

    private static CatalogCache theInstance = null;

    private long theExpirationTime = EXPIRATION_TIME; // default
    private long theRemovalTime    = 0;

    private CatalogCacheCleaner theExpCleaner;
    private CatalogCacheCleaner theRemCleaner;

    private Map theCachedItems = Collections.synchronizedMap(new HashMap());
    private Map theCachedItemsAsStrongRef;  //JP: property "cache_hardReferences"
    private boolean cacheHardReferences = false; //JP: property "cache_hardReferences"
    private boolean cacheTracing = false; //JP: property "cache_tracing"

    // the logging instance
    private static IsaLocation theStaticLocToLog =
        IsaLocation.getInstance(CatalogCache.class.getName());

        /**
         * Default constructor for the singleton instance. The cache is instantiated
         * via the initialization environment.
         *
         * @param messResources the message resources from the environment
         * @param aProps        the properties from the initialization environment
         */
    private CatalogCache(Properties aProps)
    {
        //      determine whether cacheTracing should occur
        String cache_tracing = aProps.getProperty("cache_tracing");
        if(cache_tracing != null)
        {
            cacheTracing = Boolean.valueOf(cache_tracing).booleanValue();
            if(cacheTracing) {
                theStaticLocToLog.debug("cacheTracing: activated");
            }
        }
        //      determine whether cache should also use strong references
        String cache_hardReferences = aProps.getProperty("cache_hardReferences");
        if(cache_hardReferences != null)
        {
            cacheHardReferences = Boolean.valueOf(cache_hardReferences).booleanValue();
            if(cacheHardReferences) {
                theCachedItemsAsStrongRef = new HashMap();
                theStaticLocToLog.debug("cacheHardReferences: activated");
                cacheTracing = true;
            }
        }

            // determine the expiration time
        String expiration = aProps.getProperty(CACHE_EXPIRATION);
        if(expiration != null)
        {
            try
            {
                long expTime = Long.parseLong(expiration);
                if(expTime != 0)
                    theExpirationTime = expTime;
            }
            catch(NumberFormatException ex)
            {
                theExpirationTime = EXPIRATION_TIME;
            }
        }

            // convert hours in milliseconds
        theExpirationTime = theExpirationTime * 3600 * 1000;
        if(theStaticLocToLog.isDebugEnabled())
        {
            theStaticLocToLog.debug("Cache expiration time set to " + theExpirationTime + " msec");
        }

            // start the cleaner thread for the expiration time, the time
            // that the lookup is performed is set to one hour
        theExpCleaner = new CatalogCacheCleaner(this,
                                                CACHE_EXPIRATION,
                                                theExpirationTime);
        theExpCleaner.start();

            // determine the removal time; if parameter is missing no thread
            // will be started
        String removal = aProps.getProperty(CACHE_REMOVAL);
        if(removal != null)
        {
            try
            {
                theRemovalTime = Long.parseLong(removal);
            }
            catch(NumberFormatException ex)
            {
                theRemovalTime = 0;
            }

                // start thread only if time was really defined
            if(theRemovalTime != 0)
            {
                theRemovalTime = theRemovalTime * 3600 * 1000;
                if(theStaticLocToLog.isDebugEnabled())
                {
                    theStaticLocToLog.debug("Cache removal time set to " + theRemovalTime + " msec");
                }

                theRemCleaner = new CatalogCacheCleaner(this,
                                                        CACHE_REMOVAL,
                                                        theRemovalTime);
                theRemCleaner.start();
            }
        }
    }

        /**
         * Returns the singleton instance of the cache.
         *
         * @return the singleton instance
         * @exception CatalogCacheException error because cache was not initialized
         * properly
         */
    synchronized public static CatalogCache getInstance() throws CatalogCacheException
    {
        if(theInstance == null)
        {
            throw new CatalogCacheException("The catalog cache was not properly initialized");
        }

        return theInstance;
    }

        /**
         * Reads the initialization properties from the given properties and configures
         * the cache instance accordingly.
         *
         * @param env    the calling environment
         * @param aProps the properties from the initalization process
         * @exception CatalogCacheException error because cache was already initialized
         */
    public static void initialize(InitializationEnvironment env,
                                  Properties aProps) throws CatalogCacheException
    {
        if(theInstance == null)
            theInstance = new CatalogCache(aProps);
        else
        {
            throw new CatalogCacheException("The cache was already initialized");
        }
    }

        /**
         * Handles any cleanup for the threads generated by this instance.
         *
         */
    public static void terminate()
    {
        theInstance.theExpCleaner.stopRequest();
        if(theInstance.theRemCleaner != null)
            theInstance.theRemCleaner.stopRequest();
    }

    /**
     * Generates a key from the passed parameters. This key has to be used if
     * an item is put into the cache. For each catalog/catalogSite/CatalogServer
     * which is supported by the catalog API a unique key is generated.
     * The catalog/catalogSite/catalogServer which wants to be catched
     * has to implement a method with the following name and signature (the
     * parameters are the same as these of this method): <br>
     *
     * <code>public static String generateCacheKey(BackendBusinessObjectMetaData, IActor, IServer)</code>
     *
     * @param aMetaInfo  meta information from the EAI layer; can be requested from
     *                   the BEM for each business object
     * @param aClient    the client spec of the catalog
     * @param aServer    the server spec of the catalog
     * @exception CatalogCacheException error while creating the key
     */
    public static String generateKey(BackendBusinessObjectMetaData aMetaInfo,
                                     IActor aClient,
                                     IServer aServer)
        throws CatalogCacheException
    {
        BackendBusinessObjectConfig config =
            aMetaInfo.getBackendBusinessObjectConfig();
        String className = config.getClassName();
        String key = null;

        try
        {
            Class catClass = Class.forName(className);
            Class[] paramClasses =
                {   Class.forName("com.sap.isa.core.eai.BackendBusinessObjectMetaData"),
                    Class.forName("com.sap.isa.catalog.boi.IActor"),
                    Class.forName("com.sap.isa.catalog.boi.IServer")
                };

            Method keyMethod = catClass.getMethod("generateCacheKey", paramClasses);
            key = (String)keyMethod.invoke(null, new Object[] {aMetaInfo, aClient, aServer});

            if ( theStaticLocToLog.isDebugEnabled())
            {
                theStaticLocToLog.debug("The cache generated the key: " + key);
            } // end of if ()
        }
        catch(Exception ex)
        {
            throw new CatalogCacheException(ex.getLocalizedMessage());
        }

        if(key == null || key.length() == 0)
        {
            throw new CatalogCacheException("No key could be created for the catalog '" + className + "'");
        }

        return key;
    }

        /**
         * Generates a key from the passed parameters. This key has to be used if
         * a catalog is put into the cache. For each catalog
         * which is supported by the catalog API a unique key is generated.
         * The catalog which wants to be catched has to implement a method with the
         * following name and signature  <br>
         *
         * <code>public static String generateCacheKey(String, IClient, IServerEngine)</code>
         *
         * @param aClient    			the client spec of the catalog
         * @param aServerEngine    	the serverengine of the catalog
         * @exception CatalogCacheException error while creating the key
         */
    public static String generateKey(String aClassName, IClient aClient, IServerEngine aServer)
        throws CatalogCacheException
    {
        String key = null;
        try
        {
            Class catClass = Class.forName(aClassName);
            Class[] paramClasses =
                {   Class.forName("java.lang.String"),
                    Class.forName("com.sap.isa.catalog.boi.IClient"),
                    Class.forName("com.sap.isa.catalog.boi.IServerEngine")
                };

            Method keyMethod = catClass.getMethod("generateCacheKey", paramClasses);
            key = (String)keyMethod.invoke(null, new Object[] {aClassName, aClient, aServer});
            if ( theStaticLocToLog.isDebugEnabled())
            {
                theStaticLocToLog.debug("The cache generated the key: " + key);
            } // end of if ()
        }
        catch(Exception ex)
        {
            throw new CatalogCacheException(ex.getLocalizedMessage(),ex);
        }
        if(key == null || key.length() == 0)
        {
            throw new CatalogCacheException("No key could be created for the catalog '" + aClassName + "'");
        }
        return key;
    }

        /**
         * Returns the expiration time in msec.
         *
         * @return the expiration time
         */
    public long getExpirationTime()
    {
        return theExpirationTime;
    }

        /**
         * Returns the removal time in msec.
         *
         * @return the removal time
         */
    public long getRemovalTime()
    {
        return theRemovalTime;
    }

        /**
         * Returns the time when the expiration cleaner was set to sleep.
         *
         * @return the time when the expiration cleaner was set to sleeptestInit
         */
    public long getExpCleanerStartTime()
    {
        return theExpCleaner.getStartTime();
    }

        /**
         * Returns the time when the removal cleaner was set to sleep.
         *
         * @return the time when the removal cleaner was set to sleep
         */
    public long getRemCleanerStartTime()
    {
        return (theRemCleaner==null)?0:theRemCleaner.getStartTime();
    }

        /**
         * Adds a cachable instance to the cache. If the instance was already cached
         * nothing will be done.
         *
         * @param key       unique key for the catalog
         * @param aCachable a cachable instance to be cached
         */
    public void addCachableInstance(String key, CachableItem aCachableItem)
    {
            // a catalog is definitly a cachable instance!
        this.addItem(key, aCachableItem);
        if(theStaticLocToLog.isDebugEnabled())
        {
            theStaticLocToLog.debug("Cachable instance added with key: " + key);
        }
    }

        /**
         * Gets a cachable instance from the cache.
         * If the instance was not found <code>null</code> will
         * be returned. The client of the method knows by the means
         * he used to creat the key which type is returned.
         *
         * @param key the key of the searched item
         * @return the cached instance
         */
    public CachableItem getCachedInstance(String key)
    {
        if(theStaticLocToLog.isDebugEnabled())
        {
            theStaticLocToLog.debug("Cachable instance read access with key: " + key);
        }

        CachableItem item = (CachableItem)this.getItem(key);
        if(item == null && theStaticLocToLog.isDebugEnabled())
        {
            theStaticLocToLog.debug("Cachable instance with key '" + key + "' not found");
        }
        return item;
    }

	/**
         * Gets all cachable instances currently cached.
         * If no instance was not found an empty set will be returned.
         *
         * @return an Iterator of cached items of the given type
         */
    public Iterator getCachedInstances()
    {
        ArrayList theResult = new ArrayList();
            // here synchronization is necessary because of multiple method calls
        synchronized (theCachedItems)
        {
            Iterator aKeyIter = keys();
            while (aKeyIter.hasNext())
            {
                CachableItem aItem = getItem((String)aKeyIter.next());
                if (aItem!=null && (aItem instanceof Catalog))
                {
                    theResult.add(aItem);
                } // end of if ()
            }
        }
        return theResult.iterator();
    }

        /**
         * Removes a cached instance from the cache.
         *
         * @param key the key of the instance
         */
    public void removeInstance(String key)
    {
        this.removeItem(key);
    }

        /**
         * Tests if the item with the given key is already cached.
         *
         * @param key the key of the searched item
         * @return boolean that indicates if the item is cached
         */
    public boolean isItemCached(String key)
    {
        SoftReference ref = (SoftReference)theCachedItems.get(key);

            // if reference was cleared in the meantime delete the soft reference
        if(ref != null && ref.get() == null)
        {
            if(cacheTracing) {
                theStaticLocToLog.debug("cacheTracing: isItemCached() has found pending soft reference for key '" + key + "'");
            }
            this.removeItem(key);
            if(theStaticLocToLog.isDebugEnabled())
            {
                theStaticLocToLog.debug("Entry was removed from the automatic memory cleanup");
            }
        }

        return (ref != null && ref.get() != null)?true:false;
    }

        /**
         * Returns an iterator over all keys of the items which are contained in the cache.
         * This method is for internal use only!
         *
         * @return iterator over the keys of the content of the cache
         */
    public Iterator keys()
    {
        // internally only called by this.getCachedInstances(), which is synchronized(theCachedItems)
        // ATTENTION: externally also called from catalogcache.jsp !!!
        ArrayList list = new ArrayList(this.theCachedItems.keySet());
        return Collections.unmodifiableList(list).iterator();
    }

        /**
         * Adds an item to the cache. If the item was already cached nothing
         * will be done.
         *
         * @param key the key of the searched item
         * @param aItem the item to be cached
         */
    protected void addItem(String key, CachableItem aItem)
    {
            // here synchronization is necessary because of multiple calls
        synchronized(theCachedItems)
        {
                // check if it was cached in the meantime
            SoftReference ref = (SoftReference)theCachedItems.get(key);
            CachableItem item = (ref != null)?(CachableItem)ref.get():null;

            if(item == null) {
                theCachedItems.put(key, new SoftReference(aItem));
                if(cacheHardReferences) {
                    theCachedItemsAsStrongRef.put(key, aItem);
                    theStaticLocToLog.debug("cacheHardReferences: addItem() remembers strong reference for key '" + key + "'");
                } else if(cacheTracing)
                {
                    theStaticLocToLog.debug("cacheTracing: addItem() for key '" + key + "'");
                }
            }
        }
    }

        /**
         * Gets an item from the cache. If the item was not found
         * <code>null</code> will be returned.
         *
         * @param key the key of the searched item
         * @return the cached item
         */
    protected CachableItem getItem(String key)
    {
        CachableItem item = null;

        // deactivated synchronized read access due to performance reasons
        //synchronized(theCachedItems)
        //{
            if(isItemCached(key))
            {
                SoftReference ref = (SoftReference)theCachedItems.get(key);
                item = (CachableItem)ref.get();

                    // check if item was not changed in the meantime
                if(item != null && !item.isCachedItemUptodate())
                {
                    if(cacheTracing)
                        theStaticLocToLog.debug("cacheTracing: getItem() for key '" + key + "' has found update on cached item");
                    this.removeItem(key);
                    // remove item from cache since it was changed
                    item = null;
                }
            }
        //}
        return item;
    }

        /**
         * Removes an item from the cache. If the item was not found nothing
         * will be done.
         *
         * @param key the key of the item to be removed
         */
    protected void removeItem(String key)
    {
        synchronized(theCachedItems)
        {
            if(cacheHardReferences) {
                theStaticLocToLog.debug("cacheHardReferences: removeItem() for key '" + key + "' - remove it from hard reference map");
                theCachedItemsAsStrongRef.remove(key);
            }
            else if(cacheTracing) {
                theStaticLocToLog.debug("cacheTracing: removeItem() for key '" + key + "'");
            }
            theCachedItems.remove(key);
        }
    }

        /**
         * Cleans the cache according to the default strategy. <BR/>
         * <B>Strategy:</B> In the initialization file a expiration time (in hours)
         * can be set. All items which were not used during this time period are
         * removed from the cache.
         */
    public void cleanUp()
    {
        Timestamp stamp =
            new Timestamp(System.currentTimeMillis()-theExpirationTime);

            // here synchronization is necessary because of iteration
        synchronized(theCachedItems)
        {
            if(cacheTracing) {
                theStaticLocToLog.debug("cacheTracing: cleanUp() called");
            }
            Iterator iter = theCachedItems.values().iterator();
            while(iter.hasNext())
            {
                SoftReference ref = (SoftReference)iter.next();
                CachableItem item = (CachableItem)ref.get();
                if(item == null ||
                   item.getTimeStamp().before(stamp) ||
                   !item.isCachedItemUptodate())
                {
                    iter.remove();
                    if(theStaticLocToLog.isDebugEnabled())
                    {
                        String msg = null;
                        if(item != null)
                            msg = "The entry with the time stamp '" + item.getTimeStamp() + "' was deleted";
                        else
                            msg = "Entry was removed from the automatic memory cleanup";

                        theStaticLocToLog.debug(msg);
                    }
                }
            }
                // now run the garbage collector!
            System.gc();
        }
    }

        /**
         * Clears the cache. All entries are removed.
         */
    public void clear()
    {
        if(cacheHardReferences) {
            theStaticLocToLog.debug("cacheHardReferences: clear() *** clearing the hard reference map");
            theCachedItemsAsStrongRef.clear();
        }
        else if(cacheTracing) {
            theStaticLocToLog.debug("cacheTracing: clear() called");
        }
        theCachedItems.clear();
        if(theStaticLocToLog.isDebugEnabled())
        {
            theStaticLocToLog.debug("All entries were deleted");
        }
            // now run the garbage collector!
        System.gc();
    }
}

/**
 * Helper class which cleans the catalog cache. Dependant of the configured time
 * the thread is started and removes the items of the catalog cache.
 * @version     1.0
 */
class CatalogCacheCleaner extends Thread
{
    private long theSleepingTime;
    private long theStartTime;  // time when thread was set to sleep
    private String theCleanerKind;
    private CatalogCache theCatalogCache;
    // the logging instance
    private static IsaLocation theStaticLocToLog =
        IsaLocation.getInstance(CatalogCacheCleaner.class.getName());
    // thread info objects:
    private volatile boolean mStopRequested = false;
    private Thread mRunThread;

        /**
         * Creates a new cleaner instance.
         *
         * @param cache         the corresponding caching instance
         * @param messResources the message resources from the environment
         * @param kind          the kind of thread which is to be instantiated
         * @param time          the sleeping time of the thread
         */
    CatalogCacheCleaner(CatalogCache cache,
                        String kind,
                        long time)
    {
        theCatalogCache = cache;
        theCleanerKind  = kind;
        theSleepingTime = time;

        this.setName("CCC"+kind);	// "CCC" = CatalogCacheCleaner
        theStaticLocToLog.debug("The cleaner thread '" + theCleanerKind + "' was instantiated successfully");
    }

    long getStartTime()
    {
        return theStartTime;
    }

    public void stopRequest()
    {
        mStopRequested = true;
        if(mRunThread != null)
            mRunThread.interrupt();
    }

    public void run()
    {
        if(theStaticLocToLog.isDebugEnabled())
        {
            theStaticLocToLog.debug("The cleaner thread '" + theCleanerKind + "' was started successfully");
        }

        mRunThread = Thread.currentThread();
        while(!mStopRequested)        {
            try
            {
                theStartTime = System.currentTimeMillis();
                Thread.sleep(theSleepingTime);

                if(theStaticLocToLog.isDebugEnabled())
                {
                    theStaticLocToLog.debug("The cleaner thread '" + theCleanerKind + "' was activated");
                }

                    // if it is a expiration cleaner, the entries are removed which
                    // weren't used for the specified time
                if(theCleanerKind.equals(CatalogCache.CACHE_EXPIRATION))
                    theCatalogCache.cleanUp();
                    // if it is a removal cleaner, all entries are removed from the
                    // cache
                else if(theCleanerKind.equals(CatalogCache.CACHE_REMOVAL))
                    theCatalogCache.clear();
            }
            catch(InterruptedException ex)
            {
                // changed to DEBUG logging, since this interrupt is called on each application exit
                theStaticLocToLog.debug("The sleeping cleaner thread '" + theCleanerKind + "' was interrupted");
                if(mStopRequested)
                    Thread.currentThread().interrupt(); // reassert (unneccessary?)
            }
        }
    }
}
