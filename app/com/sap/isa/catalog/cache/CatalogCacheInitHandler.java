/*****************************************************************************
  Copyright (c) 2000-2002, SAPMarkets Europe GmbH, Germany, All rights reserved.
*****************************************************************************/

package com.sap.isa.catalog.cache;

// core imports
import java.util.Properties;

import com.sap.isa.core.init.Initializable;
import com.sap.isa.core.init.InitializationEnvironment;
import com.sap.isa.core.init.InitializeException;
import com.sap.isa.core.logging.IsaLocation;

/**
 * This class is called from the initialization environment to setup the
 * catalog cache. Via the initialization config file the caching can be
 * configured.
 * @version     1.0
 */
public class CatalogCacheInitHandler implements Initializable
{
    private CatalogCache theCache;

    private static IsaLocation theStaticLocToLog = 
        IsaLocation.getInstance(CatalogCacheInitHandler.class.getName());

    /**
     * Default constructor.
     */
    public CatalogCacheInitHandler()
    {
    }

    public void initialize(InitializationEnvironment env, Properties props)
            throws InitializeException
    {
        // initialization of the cache, write a warning to the log if it
        // was already done
        try
        {
            CatalogCache.initialize(env, props);
        }
        catch(CatalogCacheException ex)
        {
            theStaticLocToLog.warn(ex.getLocalizedMessage());
        }

        // Get a reference to the cache so that the garbage collector does not
        // remove the cache instance
        try
        {
            theCache = CatalogCache.getInstance();
        }
        catch(CatalogCacheException ex)
        {
            throw new InitializeException(ex.getLocalizedMessage());
        }
    }

    public void terminate()
    {
        // terminate any CatalogCache objects
        CatalogCache.terminate();

        // Now the application is terminated so the garbage collector is allowed
        // to remove the instance
        theCache = null;
    }
}