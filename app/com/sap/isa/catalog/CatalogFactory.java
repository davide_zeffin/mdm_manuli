/*****************************************************************************
  Copyright (c) 2000, SAPMarkets Europe GmbH, Germany, All rights reserved.

  $Id: //sap/ESALES_base/30_SP_COR/src/_pcatAPI/java/com/sap/isa/catalog/CatalogFactory.java#4 $
  $Revision: #4 $
  $Change: 143928 $
  $DateTime: 2003/08/21 13:33:52 $
*****************************************************************************/
package com.sap.isa.catalog;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Properties;

import com.sap.isa.catalog.boi.CatalogException;
import com.sap.isa.catalog.boi.IActor;
import com.sap.isa.catalog.boi.IEnvironment;
import com.sap.isa.catalog.boi.IServer;
import com.sap.isa.catalog.boi.ISite;
import com.sap.isa.catalog.cache.CatalogCache;
import com.sap.isa.catalog.impl.CatalogSite;
import com.sap.isa.core.SessionConst;
import com.sap.isa.core.eai.BackendBusinessObjectMetaData;
import com.sap.isa.core.eai.BackendBusinessObjectParams;
import com.sap.isa.core.eai.BackendException;
import com.sap.isa.core.eai.BackendObjectManager;
import com.sap.isa.core.eai.BackendObjectManagerImpl;
import com.sap.isa.core.init.InitializationEnvironment;
import com.sap.isa.core.init.InitializeException;
import com.sap.isa.core.init.StandaloneHandler;
import com.sap.isa.core.logging.IsaLocation;

/**
 * The factory interface. There will be different finder-methods. i.e. methods
 * to get the catalog view for a given side, user, etc... anything that might
 * influence the products seen by a given client.
 * Two obvious finder methods are the default catalog finder and the finder
 * method by "primary key".
 * @stereotype abstract factory
 * @version     1.0
 */
public class CatalogFactory
//    implements
//        InitializationEnvironment
{
    private InitializationEnvironment theInitEnvironment;
    public InitializationEnvironment getInitializationEnvironment()
    {
        return theInitEnvironment;
    }

    private static final String DEFAULT_BO_TYPE_CATALOG_SITE ="CATALOG_SITE";

    private static HashMap theFactoryInstances = new HashMap();

    private static IsaLocation theStaticLocToLog = 
        IsaLocation.getInstance(CatalogFactory.class.getName());

    private BackendObjectManager theBEM = null;

        /**
         * Constant that holds the name of the message resource bundle which
         * is used in the catalog API. This constant has <code>public</code> visibility
         * since other classes (IMSAdminConsoleImpl and TrexAdminConsoleImpl)
         * have to use this messages, too.
         */
    public static final String THE_DEFAULT_MESSAGE_RESOURCES_CLASS =
    	"com.sap.isa.catalog.CatalogMessages";

    private static final String THE_DEFAULT_CONFIG_ROOT = ""; // don't use root dir
    private static final String THE_DEFAULT_INITALIZATION_FILE =
        "config/init-config.xml";

        /**
         * Get an instance over the getInstance method!
         * @param confId a <code>String</code> the eai-config id to use  
         * @param confData a <code>String</code> the eai-cofig-data id to use
         *
         * This method should only be called in standalone cases
         * (e.g. for test programs)
         */
    protected CatalogFactory(String confId, String confData)
        throws CatalogException
    {
            String theCurrentDir = System.getProperty("user.dir");
System.out.println("!!! CatalogFactory user.dir property is " + theCurrentDir);
System.out.println("*** CatalogFactory is creating some new instance" +
                   " (hopefully, we are not in a ISA web application!!!)");
System.out.println("*** using confId             '" + confId + "'");
System.out.println("*** using confData           '" + confData + "'");
System.out.println("*** using configRoot         '" + THE_DEFAULT_CONFIG_ROOT + "'");
System.out.println("*** using initConfigFilename '" + THE_DEFAULT_INITALIZATION_FILE + "' (from directory test/local)");

        StandaloneHandler standaloneHandler = new StandaloneHandler();
        standaloneHandler.setClassNameMessageResources(THE_DEFAULT_MESSAGE_RESOURCES_CLASS);
        standaloneHandler.setBaseDir(THE_DEFAULT_CONFIG_ROOT);
        standaloneHandler.setInitConfigPath(THE_DEFAULT_INITALIZATION_FILE);
        standaloneHandler.setBootStrapInitConfigPath("config/bootstrap-config.xml");
        try {
            standaloneHandler.initialize();
        }
        catch(InitializeException ex) {
            theStaticLocToLog.error(ICatalogMessagesKeys.PCAT_EXCEPTION,
                new Object[] {"initializing standaloneHandler failed"}, ex);
            // if initialization failed, there may not be a proper logging configured
            System.out.println("Fatal error in CatalogFactory: "
                + ex.getLocalizedMessage());
            throw new CatalogException("initializing standaloneHandler failed");
        }
        theInitEnvironment = standaloneHandler;

            // create backend object manager
        Properties theBEMProps = new Properties();
        theBEMProps.setProperty(SessionConst.EAI_CONF_ID, confId);
        if ( confData !=null && !confData.equals("")) 
        {
             theBEMProps.setProperty(SessionConst.EAI_CONF_DATA,confData);
        } // end of if ()
        
        theBEM = new BackendObjectManagerImpl(theBEMProps);

    }

        /**
         * Singleton pattern to get a concrete instance of a CatalogFactory.
         * Returns <code>null</code> if the creation of the factory fails.
         *
         * @return the singleton instance
         */
    synchronized public static CatalogFactory getInstance(String confId, String confData)
    {
        CatalogFactory aInstance = (CatalogFactory)theFactoryInstances.get(confId+":"+confData);
        if (aInstance == null)
        {
            try {
                aInstance = new CatalogFactory(confId, confData);
            }
            catch(CatalogException ex) {
                return null;
            }
            theFactoryInstances.put(confId+":"+confData, aInstance);
            // try to log the success of this work to a static logging location:
            if(theStaticLocToLog.isInfoEnabled())
            {
                theStaticLocToLog.info(
                    ICatalogMessagesKeys.PCAT_INIT_CATFACTORY,
                    new Object[] {confId,confData},
                    null);
            }
        }
        return aInstance;
    }

        /**
         * Singleton pattern to get a concrete instance of a CatalogFactory.
         * Calls <code>getInstance("default", "")</code>.
         * Returns <code>null</code> if the creation of the factory fails.
         *
         * @return the singleton instance
         */
    synchronized public static CatalogFactory getInstance()
    {
        return getInstance("default", "");
    }

        /**
         * Get an instance over the getCatalogInstance method!
         * @param confId a <code>String</code> the eai-config id to use  
         * @param confData a <code>String</code> the eai-cofig-data id to use
         *
         * This method should only be called in ESales environment
         */
    protected CatalogFactory(BackendObjectManager bem,
                            InitializationEnvironment environment)
    {
        theInitEnvironment = environment;
        theBEM = bem;
    }

        /**
         * Singleton pattern to get a concrete instance of a CatalogFactory.
         * @param bem an already instantiated <code>BackendObjectManager</code>
         * @param backendType a <code>String</code> which
         *   corresponds to "type" attribute of the "businessObject" tag in eai-config.xml
         * @param environment the <code>InitializationEnvironment</code> of the application
         * Returns <code>null</code> if the creation of the factory fails.
         *
         * @return the singleton instance
         */
    synchronized public static CatalogFactory getInstance(
            BackendObjectManager bem,
            String backendType,
            InitializationEnvironment environment)
    {
        String key = bem.getBackendConfigKey() + backendType;
        CatalogFactory aInstance = (CatalogFactory)theFactoryInstances.get(key);
        if (aInstance == null)
        {
            aInstance = new CatalogFactory(bem, environment);
            theFactoryInstances.put(key, aInstance);
            // try to log the success of this work:
            if(theStaticLocToLog.isDebugEnabled() || theStaticLocToLog.isInfoEnabled())
            {
                theStaticLocToLog.info(
                    ICatalogMessagesKeys.PCAT_INIT_CATFACTORY,
                    new Object[] {key,null},
                    null);
            }
        }
        return aInstance;
    }

        /**
         * Returns a catalog site known to the factory. Depending on the 
         * runtime environment different customizations are necessary.
         * See for details the customization chapter in the documentation.
         * <a href="{@docroot}/com/sap/isa/catalog/doc-files/
         * ProductCatalogAPIDesign.html">Catalog API Design</a>
         * 
         * @param anActor an <code>IActor</code> if  catalog engines require
         * an authorization for requesting the information.
         * @param theSiteType a <code>String</code> value present in the eai config file as a type attribute
         * @return an <code>ISite</code> instances of type
         * {@link com.sap.isa.catalog.boi.ISite}
         * @exception CatalogException if an error occurs
         */
    synchronized public ISite getCatalogSite(IActor anActor, String theSiteType)
        throws CatalogException
    {
        CatalogSite catalogSite = null;

            // first search in the cache
        String key = null;
        CatalogCache cache = CatalogCache.getInstance();
        if(theBEM == null)
            throw new CatalogException("BEM not initialized");
        BackendBusinessObjectMetaData metaInfo =
            theBEM.getBackendBusinessObjectMetaData(theSiteType);
            // try to build a key - if not possible the catalog site won't be cached

        try {
            IServer aServer = null; // this is not used in generateKey()
            key = CatalogCache.generateKey(metaInfo, anActor, aServer);
        }
        catch(/*CatalogCacheException*/ Exception ex) {
            key = null;
            theStaticLocToLog.warn(
                    ICatalogMessagesKeys.PCAT_EXCEPTION,
                    new Object[]{ex.getLocalizedMessage()},
                    null);
        }

        if(key != null && key.length() != 0)
            catalogSite = (CatalogSite) cache.getCachedInstance(key);

            // the catalog site was not cached yet - create new one and cache it now!
        if(catalogSite == null)
        {
            try 
            {
                catalogSite = (CatalogSite)
                    theBEM.createBackendBusinessObject(theSiteType,
                                                       new ActorInfo(anActor,theInitEnvironment));
                if(key != null && key.length() != 0)
                    cache.addCachableInstance(key, catalogSite);
            }
            catch (BackendException bee) 
            {
                String msg =
                    theInitEnvironment.getMessageResources().getMessage(
                        ICatalogMessagesKeys.PCAT_ERR_INIT_INSTANCE,
                        CatalogSite.class, bee.getMessage());
                throw new CatalogException(msg,bee);
            } // end of try-catch
        }
        return catalogSite;
    }

        /**
         * Get the default site.
         * Calls getCatalogSite(anActor,"CATALOG_SITE")
         * where this constant is set to a value that should be
         * used in the eai config file.
         *
         * @param anActor an <code>IActor</code> value
         * @return an <code>ISite</code> value
         * @exception CatalogException if an error occurs
         */
    public ISite getCatalogSite(IActor anActor)
        throws CatalogException
    {
        return getCatalogSite(anActor,CatalogFactory.DEFAULT_BO_TYPE_CATALOG_SITE);
    }
    
    class ActorInfo
    implements
        IActor,
        IEnvironment,
        BackendBusinessObjectParams
    {
        IActor theActor;
        InitializationEnvironment theInitEnvironment;
    
        ActorInfo(IActor aActor,
                  InitializationEnvironment aInitEnvironment)
        {
            theActor=aActor;
            theInitEnvironment=aInitEnvironment;
        }

            /*******  IActor Methods *****************************/
        
        public String getName()
        {
            return theActor.getName();
        }

        public String getGuid()
        {
            return theActor.getName();
        }

        public String getCookie()
        {
            return null;
        }
        public String getPWD()
        {
            return theActor.getPWD();
        }

        public Iterator getPermissions()
        {
            return theActor.getPermissions();
        }
        
            /************************   IEnvironment methods *******************/
        
        public InitializationEnvironment getEnvironment()
        {
            return theInitEnvironment;
        }
    }


        /**
         * Updates the list of currently known sides.
         */
    private void updateSites()
    {
        return;
    }
}
