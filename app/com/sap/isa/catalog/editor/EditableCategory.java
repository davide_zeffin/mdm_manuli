/*****************************************************************************
  Copyright (c) 2000-2001, SAPMarkets Europe GmbH, Germany, All rights reserved.
  Created:      02 August 2001

  $Id: //sap/ESALES_base/30_SP_COR/src/_pcatAPI/java/com/sap/isa/catalog/editor/EditableCategory.java#2 $
  $Revision: #2 $
  $Change: 129221 $
  $DateTime: 2003/05/15 17:20:58 $
*****************************************************************************/
package com.sap.isa.catalog.editor;

//  catalog interfaces
import com.sap.isa.catalog.ICatalogMessagesKeys;
import com.sap.isa.catalog.boi.CatalogException;
import com.sap.isa.catalog.boi.IAttributeCreatedEvent;
import com.sap.isa.catalog.boi.IAttributeDeletedEvent;
import com.sap.isa.catalog.boi.ICategory;
import com.sap.isa.catalog.boi.IChildCreatedEvent;
import com.sap.isa.catalog.boi.IChildDeletedEvent;
import com.sap.isa.catalog.boi.IComponent;
import com.sap.isa.catalog.boi.IComponentChangedEvent;
import com.sap.isa.catalog.boi.IComponentEventListener;
import com.sap.isa.catalog.boi.ICompositeEventListener;
import com.sap.isa.catalog.boi.IDetailCreatedEvent;
import com.sap.isa.catalog.boi.IDetailDeletedEvent;
import com.sap.isa.catalog.boi.IEditableAttribute;
import com.sap.isa.catalog.boi.IEditableCategory;
import com.sap.isa.catalog.boi.IEditableDetail;
import com.sap.isa.catalog.boi.IEditableItem;
import com.sap.isa.catalog.catalogcmd.CatalogCmd;
import com.sap.isa.catalog.catalogcmd.CatalogCmdProcessor;
import com.sap.isa.catalog.impl.Catalog;
import com.sap.isa.catalog.impl.CatalogCategory;
import com.sap.isa.catalog.impl.CatalogCompositeComponent;
import com.sap.isa.catalog.impl.CatalogLockManager;

/**
 *  An editor for a category.
 *
 *  @version     1.0
 */
public class EditableCategory
    extends CatalogCategory
    implements  IEditableCategory,
                IComponentEventListener,
                ICompositeEventListener
{
    /*
     *  EditableCategory implements Serializable:
     *  Fields:
     *  theCateToEdit     :   implements Serializable
     */
    private CatalogCategory theCateToEdit; //null if new

    /**
     * To create new editors for new categories. The construction process
     * via {@link EditableCatalogBuilder}, {@link CatalogCleanerVisitor},
     * {@link EditableCatalog}, {@link EditableCategory} enforces the
     * preconditions beween the parameters. The state after creation is
     * NEW.
     *
     * @param   anEditableParent a catalog or category that is about to
     *          get a new category. i.e. the editable wrapper of the catalog or
     *          category that will finaly get the new category.
     * @param   aGuid a guid that will be used for this. The user has to
     *          ensure that guids that are used here are consistent with the
     *          guids used by the underlying catalog engine.
     */
    public EditableCategory(CatalogCompositeComponent anEditableParent,
                            String aGuid)
    {
        super(anEditableParent,aGuid);
        setState(IComponent.State.NEW);
    }

    /**
     * To create new editors for existing categories. The constructor pulls the
     * internal state from the component to edit. The construction process
     * via {@link EditableCatalogBuilder}, {@link CatalogCleanerVisitor},
     * {@link EditableCategory}, {@link EditableCatalog} enforces the
     * preconditions beween the parameters. The state after creation is
     * LOADED.
     *
     * @param   anEditableParent a catalog or category that is about to be
     *          edited. i.e. the editable wrapper of the parent category or
     *          category of aCateToEdit
     * @param   aCateToEdit the component that is edited via this.
     */
    public EditableCategory(CatalogCompositeComponent anEditableParent,
                            CatalogCategory aCateToEdit)
    {
        super(anEditableParent,aCateToEdit.getGuid());
        this.theCateToEdit=aCateToEdit;
        setState(IComponent.State.LOADED);
        //Init the internal state from the cate to edit
        this.setNameInternal(aCateToEdit.getName());
        this.setDescriptionInternal(aCateToEdit.getDescription());
        this.setThumbNailInternal(aCateToEdit.getThumbNail());

        //listen to your component
        theCateToEdit.addComponentEventListener(this);
        theCateToEdit.addCompositeEventListener(this);
    }

    /**
     * For internal use in the construction process.
     *
     * @return the reference to the item beeing edited.
     */
    CatalogCategory getCategory()
    {
        return theCateToEdit;
    }

    /**
     * For internal use in the construction process.
     *
     * @return the reference to the item beeing edited.
     */
    void setCategory(CatalogCategory aCateRef)
    {
        theCateToEdit=aCateRef;

        //listen to your component
        theCateToEdit.addComponentEventListener(this);
        theCateToEdit.addCompositeEventListener(this);

        return;
    }

    /**
     * Factory method for creating a detail instance. Internal method which
     * can't be called via the public interface.
     *
     * @param name the name of the new detail instance
     * @return a instance of type <code>EditableDetail</code>
     */
    public EditableDetail createEditableDetailInternal(String name)
    {
        EditableDetail aDetail = new EditableDetail(this, name);
        addDetailInternal(aDetail,true);
        return aDetail;
    }

    /**
     * Factory method for creating a category instance. Internal method which
     * can't be called via the public interface.
     *
     * @param aCategoryGuid the guid of the category to be created
     * @return a instance of type <code>EditableCategory</code>
     */
    public EditableCategory createEditableCategoryInternal(String aCategoryGuid)
    {
        EditableCategory child =
            (EditableCategory)getRoot().getCategoryInternal(aCategoryGuid);
        if(child == null)
        {
            child = new EditableCategory(this, aCategoryGuid);
            getRoot().addCategory(child);
        }

        addChildCategoryInternal(child,true);

        return child;
    }

    /**
     * Creates a new attribute. The composite has an association with the instance.
     * Internal method which can't be called via the public interface.
     *
     * @param guid the guid of the attribute
     * @return a instance of type <code>EditableAttribute</code>
     */
    synchronized public
        EditableAttribute createEditableAttributeInternal(String guid)
    {
        EditableAttribute attribute = new EditableAttribute(this, guid);
        addAttributeInternal(attribute,true);
        return attribute;
    }

    /**
     * Factory method for creating an item instance. Internal method which
     * can't be called via the public interface.
     *
     * @param aItemGuid the guid of the item to be created
     * @return a instance of type <code>EditableItem</code>
     */
    synchronized public
        EditableItem createEditableItemInternal(String aItemGuid)
    {
        EditableItem aItem = new EditableItem(this, aItemGuid);
        addItemInternal(aItem,true);
        return aItem;
    }

/* ***************************** IEditableComponent   *********************** */

    public IComponent getComponentReference()
    {
        return this.getCategoryReference();
    }

    public boolean lock()
    {
        if(theCateToEdit!=null)
        {
            //Now get the lock
            //Its definitely of type EditableCatalog
            EditableCatalog theEditor = (EditableCatalog) getRoot();
            CatalogLockManager theLocker =
                CatalogLockManager.getInstanceFor(theEditor.getCatalog());
            return theLocker.getLock(   theCateToEdit,
                                        false,
                                        theEditor.getUser());
        }
        else
            return true;
    }

    public void releaseLock()
    {
        if(theCateToEdit!=null)
        {
            //Now get the lock
            EditableCatalog theEditor = (EditableCatalog)getRoot();
            CatalogLockManager theLocker =
                CatalogLockManager.getInstanceFor(theEditor.getCatalog());
            theLocker.releaseLock(theCateToEdit,
                                  false,
                                  theEditor.getUser());
        }
        return;
    }

/* *********************** IEditableHierarchicalComponent******************** */

    public boolean lockTree()
    {
        if(theCateToEdit!=null)
        {
            //Now get the lock
            EditableCatalog theEditor = (EditableCatalog)getRoot();
            CatalogLockManager theLocker =
                CatalogLockManager.getInstanceFor(theEditor.getCatalog());
            return theLocker.getLock(theCateToEdit,
                              true,
                              theEditor.getUser());
        }
        return true;
    }

    public void releaseTree()
    {
        if(theCateToEdit!=null)
        {
            //Now get the lock
            EditableCatalog theEditor = (EditableCatalog)getRoot();
            CatalogLockManager theLocker =
                CatalogLockManager.getInstanceFor(theEditor.getCatalog());
            theLocker.releaseLock(  theCateToEdit,
                                    true,
                                    theEditor.getUser());
            return;
        }
        return;
    }

/* *********************** IEditableCompositeComponent ********************** */

    public boolean lockChildren()
    {
        if(theCateToEdit!=null)
        {
            //Now get the lock
            EditableCatalog theEditor = (EditableCatalog)getRoot();
            CatalogLockManager theLocker =
                CatalogLockManager.getInstanceFor(theEditor.getCatalog());
            return theLocker.getLock(theCateToEdit,
                              true,
                              theEditor.getUser());
        }
        return true;
    }

    public void releaseChildren()
    {
        if(theCateToEdit!=null)
        {
            //Now get the lock
            EditableCatalog theEditor = (EditableCatalog)getRoot();
            CatalogLockManager theLocker =
                CatalogLockManager.getInstanceFor(theEditor.getCatalog());
            theLocker.releaseLock(  theCateToEdit,
                                    true,
                                    theEditor.getUser());
            return;
        }
        return;
    }

/* *************************  IEditableCategory ***************************** */

    synchronized public ICategory getCategoryReference()
    {
        return theCateToEdit;
    }

    /**
     * Synchronization: To avoid inconsistancies between threads "of
     * the same session"!
     */
    synchronized public void setName(String aNewName)
        throws CatalogException
    {
        if(isDeleted())
        {
            String theMessage = getRoot().getMessageResources().getMessage(
                ICatalogMessagesKeys.PCAT_ERR_IS_DELEDED);
            throw new CatalogException(theMessage);
        }

        //Now get the lock, if new we get ite any way
        if(!lock())
        {
            String theMessage =
                ((EditableCatalog)getRoot()).getCatalog().getMessageResources()
                .getMessage(ICatalogMessagesKeys.PCAT_ERR_LOCK);
            throw new CatalogException(theMessage);
        }

        /********************** without cmd processor **************************
        setNameInternal(aNewName);

        //if not new we are changed now
        setStateChanged();

        return;
        ***********************************************************************/
        EditableCatalog theEditor = (EditableCatalog)getRoot();
        CatalogCmdProcessor processor = theEditor.getCmdProcessor();
        CatalogCmd command =
            processor.createCmdCategoryEdit(this,
                                            aNewName,
                                            CatalogCmd.PROP_NAME);
        if(!processor.doCmd(command))
            throw new CatalogException("");
            /**@todo real text has to be added **/

        return;
    }

    /**
     * Synchronization: To avoid inconsistancies between threads "of
     * the same session"!
     */
    synchronized public void setDescription(String aNewDescription)
        throws CatalogException
    {
        if(isDeleted())
        {
            String theMessage = getRoot().getMessageResources().getMessage(
                ICatalogMessagesKeys.PCAT_ERR_IS_DELEDED);
            throw new CatalogException(theMessage);
        }

        //Now get the lock, if new we get ite any way
        if(!lock())
        {
            String theMessage =
                ((EditableCatalog)getRoot()).getCatalog().getMessageResources()
                .getMessage(ICatalogMessagesKeys.PCAT_ERR_LOCK);
            throw new CatalogException(theMessage);
        }

        /****************** without cmd processor ******************************
        setDescriptionInternal(aNewDescription);

        //if not new we are changed now
        setStateChanged();

        return;
        ***********************************************************************/
        EditableCatalog theEditor = (EditableCatalog)getRoot();
        CatalogCmdProcessor processor = theEditor.getCmdProcessor();
        CatalogCmd command =
            processor.createCmdCategoryEdit(this,
                                            aNewDescription,
                                            CatalogCmd.PROP_DESCRIPTION);
        if(!processor.doCmd(command))
            throw new CatalogException("");
            /**@todo real text has to be added **/

        return;
    }

    /**
     * Synchronization: To avoid inconsistancies between threads "of
     * the same session"!
     */
    synchronized public IEditableAttribute createAttribute(String aGuid)
        throws CatalogException
    {
        EditableCatalog theEditor = (EditableCatalog) getRoot();
        Catalog theCatToEdit = theEditor.getCatalog();
        //unused: IActor theUser = theEditor.getUser();

        /**@todo we should check that we realy get a new guid*/
        if(isDeleted())
        {
            String theMessage = theCatToEdit.getMessageResources().getMessage(
                ICatalogMessagesKeys.PCAT_ERR_IS_DELEDED);
            throw new CatalogException(theMessage);
        }

        //Now get the lock, if new we get ite any way
        if(!lock())
        {
            String theMessage = theCatToEdit.getMessageResources().getMessage(
                ICatalogMessagesKeys.PCAT_ERR_LOCK);
            throw new CatalogException(theMessage);
        }
        /*************************** without cmd processor *********************
        EditableAttribute aNewAttribute = new EditableAttribute(this,aGuid);
        this.addAttributeInternal(aNewAttribute,true);

        //if not new we are changed now
        setStateChanged();

        return aNewAttribute;
        ***********************************************************************/
        CatalogCmdProcessor processor = theEditor.getCmdProcessor();
        CatalogCmd command = processor.createCmdAttributeCreate(this, aGuid);
        if(!processor.doCmd(command))
            throw new CatalogException("");
            /**@todo real text has to be added **/

        return (EditableAttribute)getAttributeInternal(aGuid);
    }

    /**
     * Synchronization: To avoid inconsistancies between threads "of
     * the same session"!
     */
    synchronized public void deleteAttribute(IEditableAttribute aThingToDelete)
        throws CatalogException
    {
        EditableCatalog theEditor = (EditableCatalog) getRoot();
        EditableAttribute theThingToDelete = (EditableAttribute) aThingToDelete;
        Catalog theCatToEdit = theEditor.getCatalog();
        //unused: IActor theUser = theEditor.getUser();

        if(isDeleted())
        {
            String theMessage = theCatToEdit.getMessageResources().getMessage(
                ICatalogMessagesKeys.PCAT_ERR_IS_DELEDED);
            throw new CatalogException(theMessage);
        }

        //If the thing is allready deleted no need to delete it again.
        if(theThingToDelete.isDeleted())
        {
            String theMessage = theCatToEdit.getMessageResources().getMessage(
                ICatalogMessagesKeys.PCAT_ERR_IS_DELEDED);
            throw new CatalogException(theMessage);
        }

        //If we don't own the thing inform the stubborn user with an exception
        if(!theAttributes.contains(theThingToDelete))
        {
            String theMessage = getRoot().getMessageResources().getMessage(
                ICatalogMessagesKeys.PCAT_ERR_OBJ_NOT_OWNED_THIS);
            throw new CatalogException(theMessage);
        }

        //Here we we need the hole tree except we are new
        if( !isNew() && !theEditor.lockTree() )
        {
            String theMessage = theCatToEdit.getMessageResources().getMessage(
                ICatalogMessagesKeys.PCAT_ERR_LOCK);
            throw new CatalogException(theMessage);
        }
        if(!theThingToDelete.lock())
        {
            String theMessage = theCatToEdit.getMessageResources().getMessage(
                ICatalogMessagesKeys.PCAT_ERR_LOCK);
            throw new CatalogException(theMessage);
        }
        /************************ without cmd processor ************************
        //we did it
        theThingToDelete.setState(IComponent.State.DELETED);
        this.deleteAttributeInternal((CatalogAttribute)theThingToDelete);

        //at the latest now we are changed if not new
        setStateChanged();

        return;
        ***********************************************************************/
        CatalogCmdProcessor processor = theEditor.getCmdProcessor();
        CatalogCmd command =
            processor.createCmdAttributeDelete(this, theThingToDelete);
        if(!processor.doCmd(command))
            throw new CatalogException("");
            /**@todo real text has to be added **/
        return;
    }

    /**
     * Synchronization: To avoid inconsistancies between threads "of
     * the same session"!
     */
    synchronized public IEditableCategory createChild(String aGuid)
        throws CatalogException
    {
        EditableCatalog theEditor = (EditableCatalog) getRoot();
        Catalog theCatToEdit = theEditor.getCatalog();
        //unused: IActor theUser = theEditor.getUser();

        if(isDeleted())
        {
            String theMessage = theCatToEdit.getMessageResources().getMessage(
                ICatalogMessagesKeys.PCAT_ERR_IS_DELEDED);
            throw new CatalogException(theMessage);
        }

        //Now get the lock, if new we get ite any way
        if(!lock())
        {
            String theMessage = theCatToEdit.getMessageResources().getMessage(
                ICatalogMessagesKeys.PCAT_ERR_LOCK);
            throw new CatalogException(theMessage);
        }
        /***********************+ without cmd processsor ***********************
        EditableCategory aNewCategory = new EditableCategory(this,aGuid);

        addChildCategoryInternal(aNewCategory,true);

        //add it to the hash table of the catalog
        theEditor.addCategory(aNewCategory);

        //at the latest now we are changed if not new
        setStateChanged();

        return aNewCategory;
        ************************************************************************/

        CatalogCmdProcessor processor = theEditor.getCmdProcessor();
        CatalogCmd command =
            processor.createCmdCategoryCreate(this, aGuid);
        if(!processor.doCmd(command))
            throw new CatalogException("");
            /**@todo real text has to be added **/

        return (EditableCategory)getChildCategory(aGuid);

    }

    /**
     * Synchronization: To avoid inconsistancies between threads "of
     * the same session"!
     */
    synchronized public void deleteChild(IEditableCategory aThingToDelete)
        throws CatalogException
    {
        EditableCategory theThingToDelete =
            (EditableCategory) aThingToDelete;

        EditableCatalog theEditor = (EditableCatalog) getRoot();

        Catalog theCatToEdit = theEditor.getCatalog();

        if(isDeleted())
        {
            String theMessage = theCatToEdit.getMessageResources().getMessage(
                ICatalogMessagesKeys.PCAT_ERR_IS_DELEDED);
            throw new CatalogException(theMessage);
        }

        //If the thing is allready deleted no need to delete it again.
        if(theThingToDelete.isDeleted())
        {
            String theMessage = theCatToEdit.getMessageResources().getMessage(
                ICatalogMessagesKeys.PCAT_ERR_IS_DELEDED);
            throw new CatalogException(theMessage);
        }

        //If we don't own the thing inform the stubborn user with an exception
        if(!theInnerNodes.contains(theThingToDelete))
        {
            String theMessage = getRoot().getMessageResources().getMessage(
                ICatalogMessagesKeys.PCAT_ERR_OBJ_NOT_OWNED_THIS);
            throw new CatalogException(theMessage);
        }

        if(!lock())//Now get the lock, if new we get ite any way
        {
            String theMessage = theCatToEdit.getMessageResources().getMessage(
                ICatalogMessagesKeys.PCAT_ERR_LOCK);
            throw new CatalogException(theMessage);
        }
        if(!theThingToDelete.lockTree())//Now get the lock, if new we get ite any way
        {
            String theMessage = theCatToEdit.getMessageResources().getMessage(
                ICatalogMessagesKeys.PCAT_ERR_LOCK);
            throw new CatalogException(theMessage);
        }
        /****************** without cmd processor ******************************
        theThingToDelete.setStateDeleted();

        deleteChildCategoryInternal(theThingToDelete);

        //remove the association in the hashtable
        theEditor.removeCategory(theThingToDelete);

        //at the latest now we are changed if not new
        setStateChanged();

        return;
        ***********************************************************************/
        CatalogCmdProcessor processor = theEditor.getCmdProcessor();
        CatalogCmd command =
            processor.createCmdCategoryDelete(this, theThingToDelete);
        if(!processor.doCmd(command))
            throw new CatalogException("");
            /**@todo real text has to be added **/

        return;
    }

    /**
     * Synchronization: To avoid inconsistancies between threads "of
     * the same session"!
     */
    synchronized public IEditableItem createItem(String aGuid)
        throws CatalogException
    {
        EditableCatalog theEditor = (EditableCatalog) getRoot();
        Catalog theCatToEdit = theEditor.getCatalog();

        //If we are deleted no way!
        if(isDeleted())
        {
            String theMessage = theCatToEdit.getMessageResources()
                .getMessage(
                ICatalogMessagesKeys.PCAT_ERR_IS_DELEDED);
            throw new CatalogException(theMessage);
        }

        if(!lock())//Now get the lock, if new we get ite any way
        {
            String theMessage = theCatToEdit.getMessageResources().getMessage(
                ICatalogMessagesKeys.PCAT_ERR_LOCK);
            throw new CatalogException(theMessage);
        }

        /******************* without cmd processor *****************************
        EditableItem aNewItem = new EditableItem(this,aGuid);

        //Add something the builder could not build because its new
        this.addItemInternal(aNewItem,true);

        //at the latest now we are changed if not new
        setStateChanged();

        return aNewItem;
        ***********************************************************************/

        CatalogCmdProcessor processor = theEditor.getCmdProcessor();
        CatalogCmd command =
            processor.createCmdItemCreate(this, aGuid);
        if(!processor.doCmd(command))
            throw new CatalogException("");
            /**@todo real text has to be added **/

        return (EditableItem)getItem(aGuid);
    }

    /**
     * Synchronization: To avoid inconsistancies between threads "of
     * the same session"!
     */
    synchronized public void deleteItem(IEditableItem aThingToDelete)
        throws CatalogException
    {
        //unused: EditableItem theThingToDeleted = (EditableItem) aThingToDelete;
        EditableCatalog theEditor = (EditableCatalog) getRoot();
        EditableItem theThingToDelete = (EditableItem) aThingToDelete;
        Catalog theCatToEdit = theEditor.getCatalog();

        //If we are deleted there is no chance to delete any thing over us.
        if(isDeleted())
        {
            String theMessage = theCatToEdit.getMessageResources().getMessage(
                ICatalogMessagesKeys.PCAT_ERR_IS_DELEDED);
            throw new CatalogException(theMessage);
        }

        //If the thing is allready deleted no need to delete it again.
        if(theThingToDelete.isDeleted())
        {
            String theMessage = theCatToEdit.getMessageResources().getMessage(
                ICatalogMessagesKeys.PCAT_ERR_IS_DELEDED);
            throw new CatalogException(theMessage);
        }

        //If we don't own the thing inform the stubborn user with an exception
        if(!theLeafNodes.contains(theThingToDelete))
        {
            String theMessage = getRoot().getMessageResources().getMessage(
                ICatalogMessagesKeys.PCAT_ERR_OBJ_NOT_OWNED_THIS);
            throw new CatalogException(theMessage);
        }

        if(!lock())//If new we get it any way
        {
            String theMessage = theCatToEdit.getMessageResources().getMessage(
                ICatalogMessagesKeys.PCAT_ERR_LOCK);
            throw new CatalogException(theMessage);
        }
        if(!theThingToDelete.lockTree())//If new we get it any way
        {
            String theMessage = theCatToEdit.getMessageResources().getMessage(
                ICatalogMessagesKeys.PCAT_ERR_LOCK);
            throw new CatalogException(theMessage);
        }

        /**************** without cmd processor ********************************
        //we did it
        theThingToDelete.setStateDeleted();

        this.deleteItemInternal((CatalogItem) theThingToDelete);

        //at the latest now we are changed if not new
        setStateChanged();

        return;
        ***********************************************************************/
        CatalogCmdProcessor processor = theEditor.getCmdProcessor();
        CatalogCmd command =
            processor.createCmdItemDelete(this, theThingToDelete);
        if(!processor.doCmd(command))
            throw new CatalogException("");
            /**@todo real text has to be added **/

        return;
    }

    /**
     * Synchronization: To avoid inconsistancies between threads "of
     * the same session"!
     */
    public IEditableDetail createDetail(String aName)
        throws CatalogException
    {
        EditableCatalog theEditor = (EditableCatalog) getRoot();
        Catalog theCatToEdit = theEditor.getCatalog();

        //If we are deleted no way!
        if(isDeleted())
        {
            String theMessage = theCatToEdit.getMessageResources()
                .getMessage(
                ICatalogMessagesKeys.PCAT_ERR_IS_DELEDED);
            throw new CatalogException(theMessage);
        }

        if(!lock())//If new we get it any way
        {
            String theMessage = theCatToEdit.getMessageResources().getMessage(
                ICatalogMessagesKeys.PCAT_ERR_LOCK);
            throw new CatalogException(theMessage);
        }
        /************** without cmd processor **********************************
        EditableDetail aNewDetail = new EditableDetail(this,aName);

        //Add something the builder could not build because its new
        this.addDetailInternal(aNewDetail,true);

        //at the latest now we are changed if not new
        setStateChanged();

        return aNewDetail;
        ***********************************************************************/

        CatalogCmdProcessor processor = theEditor.getCmdProcessor();
        CatalogCmd command = processor.createCmdDetailCreate(this, aName);
        if(!processor.doCmd(command))
            throw new CatalogException("");
            /**@todo real text has to be added **/

        return (EditableDetail)getDetail(aName); // it's definitly of this type
    }

    /**
     * Synchronization: To avoid inconsistancies between threads "of
     * the same session"!
     */
    synchronized public void deleteDetail(IEditableDetail aThingToDelete)
        throws CatalogException
    {
        EditableDetail theThingToDelete = (EditableDetail) aThingToDelete;
        EditableCatalog theEditor = (EditableCatalog) getRoot();
        Catalog theCatToEdit = theEditor.getCatalog();

        //If we are deleted there is no chance to delete any thing over us.
        if(isDeleted())
        {
            String theMessage = theCatToEdit.getMessageResources().getMessage(
                ICatalogMessagesKeys.PCAT_ERR_IS_DELEDED);
            throw new CatalogException(theMessage);
        }

        //If the thing is allready deleted no need to delete it again.
        if(theThingToDelete.isDeleted())
        {
            String theMessage = theCatToEdit.getMessageResources().getMessage(
                ICatalogMessagesKeys.PCAT_ERR_IS_DELEDED);
            throw new CatalogException(theMessage);
        }

        //If we don't own the thing inform the stubborn user with an exception
        if(!theDetails.contains(theThingToDelete))
        {
            String theMessage = getRoot().getMessageResources().getMessage(
                ICatalogMessagesKeys.PCAT_ERR_OBJ_NOT_OWNED_THIS);
            throw new CatalogException(theMessage);
        }

        if(!lock())//If new we get it any way
        {
            String theMessage = theCatToEdit.getMessageResources().getMessage(
                ICatalogMessagesKeys.PCAT_ERR_LOCK);
            throw new CatalogException(theMessage);
        }
        if(!theThingToDelete.lock())
        {
            String theMessage = theCatToEdit.getMessageResources().getMessage(
                ICatalogMessagesKeys.PCAT_ERR_LOCK);
            throw new CatalogException(theMessage);
        }

        /********************************* without cmd processor ***************
        //we did it
        theThingToDelete.setStateDeleted();
        this.deleteDetailInternal(theThingToDelete);

        //at the latest now we are changed if not new
        setStateChanged();

        return;
        ***********************************************************************/
        CatalogCmdProcessor processor = theEditor.getCmdProcessor();
        CatalogCmd command =
            processor.createCmdDetailDelete(this, theThingToDelete);
        if(!processor.doCmd(command))
            throw new CatalogException("");
            /**@todo real text has to be added **/
        return;
    }

/* ************************* IComponentEventListener ************************ */

    synchronized public void componentChanged(IComponentChangedEvent anEvent)
    {
        /**@todo implement it*/
        return;
    }

    synchronized public void detailCreated(IDetailCreatedEvent aNewDetailEvent)
    {
        /**@todo implement it*/
        return;
    }

    synchronized public void detailDeleted(IDetailDeletedEvent anDeleteEvent)
    {
        /**@todo implement it*/
        return;
    }

/* ************************** ICompositeEventListener *********************** */

    public void attributeCreated(
        IAttributeCreatedEvent aNewAttributeEvent)
    {
        /**@todo implement it synchronization!!! */
        return;
    }

    synchronized public void attributeDeleted(
        IAttributeDeletedEvent anDeleteEvent)
    {
        /**@todo implement it*/
        return;
    }

    public void childCreated(
        IChildCreatedEvent aNewChildEvent)
    {
        /**@todo implement it synchronization!!! */
        return;
    }

    synchronized public void childDeleted(
        IChildDeletedEvent anDeleteEvent)
    {
        /**@todo implement it*/
        return;
    }
}
