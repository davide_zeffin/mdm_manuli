/*****************************************************************************
  Copyright (c) 2000-2001, SAPMarkets Europe GmbH, Germany, All rights reserved.
  Created:      02 August 2001

  $Id: //sap/ESALES_base/30_SP_COR/src/_pcatAPI/java/com/sap/isa/catalog/editor/EditableAttribute.java#2 $
  $Revision: #2 $
  $Change: 129221 $
  $DateTime: 2003/05/15 17:20:58 $
*****************************************************************************/
package com.sap.isa.catalog.editor;

//  catalog interfaces
import com.sap.isa.catalog.ICatalogMessagesKeys;
import com.sap.isa.catalog.boi.CatalogException;
import com.sap.isa.catalog.boi.IAttribute;
import com.sap.isa.catalog.boi.IComponent;
import com.sap.isa.catalog.boi.IComponentChangedEvent;
import com.sap.isa.catalog.boi.IComponentEventListener;
import com.sap.isa.catalog.boi.IDetailCreatedEvent;
import com.sap.isa.catalog.boi.IDetailDeletedEvent;
import com.sap.isa.catalog.boi.IEditableAttribute;
import com.sap.isa.catalog.boi.IEditableDetail;
import com.sap.isa.catalog.catalogcmd.CatalogCmd;
import com.sap.isa.catalog.catalogcmd.CatalogCmdProcessor;
import com.sap.isa.catalog.impl.Catalog;
import com.sap.isa.catalog.impl.CatalogAttribute;
import com.sap.isa.catalog.impl.CatalogCompositeComponent;
import com.sap.isa.catalog.impl.CatalogLockManager;

/**
 *  An editor for an attribute.
 *
 *  @version     1.0
 */
public class EditableAttribute
    extends CatalogAttribute
    implements  IEditableAttribute,
                IComponentEventListener
{
    /*
     *  EditableAttribute implements Serializable:
     *  Fields:
     *  theAttToEdit     :   implements Serializable
     */
    private CatalogAttribute theAttToEdit;  //null if new

    /**
     * To create new editors for existing attributes. The constructor pulls the
     * internal state from the component to edit. The construction process
     * via {@link EditableCatalogBuilder}, {@link CatalogCleanerVisitor},
     * {@link EditableCategory}, {@link EditableCatalog} enforces the
     * preconditions beween the parameters. The state after creation is
     * LOADED.
     *
     * @param   anEditableParentComp a catalog or category that is about to be
     *          edited. i.e. the editable wrapper of the parent category or
     *          category of anAttToEdit
     * @param   anAttToEdit the component that is edited via this.
     */
    public EditableAttribute(   CatalogCompositeComponent anEditableParentComp,
                                CatalogAttribute anAttToEdit)
    {
        super(anEditableParentComp);

        theAttToEdit=anAttToEdit;

        setState(IComponent.State.LOADED);
        //pull your state for the thing to edit
        setNameInternal(anAttToEdit.getName());
        setGuidInternal(anAttToEdit.getGuid());
        setTypeInternal(anAttToEdit.getType());
        setQuantityInternal(anAttToEdit.getQuantity());
        setDomainInternal(anAttToEdit.getDomain());

        //take care we are informed of changes
        theAttToEdit.addComponentEventListener(this);
    }

    /**
     * To create new editors for new attributes. The state after creation is
     * NEW.
     *
     * @param   anEditableParentComp a catalog or category that is about to
     *          get a new attribute. i.e. the editable wrapper of the catalog or
     *          category that will finaly get the new attribute.
     * @param   aGuid a guid that will be used for this. The user has to
     *          ensure that guids that are used here are consistent with the
     *          guids used by the underlying catalog engine.
     */
    public EditableAttribute(   CatalogCompositeComponent anEditableParentComp,
                                String aGuid)
    {
        super(anEditableParentComp,aGuid,TYPE_ID_STRING,null,null);
        setState(IComponent.State.NEW);
    }

    /**
     * The attribute we are editing.
     *
     * @return aCatRef we are editing
     */
    CatalogAttribute getAttribute()
    {
        return this.theAttToEdit;
    }

    /**
     * Set the attribute we are editing.
     *
     * @param aCatRef a new ref if we where self new at the start.
     */
    void setAttribute(CatalogAttribute aAttribRef)
    {
        this.theAttToEdit=aAttribRef;

        //take care we are informed of changes
        theAttToEdit.addComponentEventListener(this);
    }

    /**
     * Factory method for creating a detail instance. Internal method which
     * can't be called via the public interface.
     *
     * @param name the name of the new detail instance
     * @return a instance of type <code>EditableDetail</code>
     */
    public EditableDetail createEditableDetailInternal(String name)
    {
        EditableDetail aDetail = new EditableDetail(this, name);
        this.addDetailInternal(aDetail,true);
        return aDetail;
    }

/* *************************** IEditableComponent *************************** */

    public IComponent getComponentReference()
    {
        return this.getAttributeReference();
    }

    public boolean lock()
    {
        if(theAttToEdit!=null)
        {
            //Now get the lock
            EditableCatalog theEditor = (EditableCatalog)getRoot();
            CatalogLockManager theLocker =
                CatalogLockManager.getInstanceFor(theEditor.getCatalog());
            return theLocker.getLock(   theAttToEdit,
                                        false,
                                        ((EditableCatalog)getRoot()).getUser());
        }
        else
            return true;
    }

    public void releaseLock()
    {
        if(theAttToEdit!=null)
        {
            //Now get the lock
            EditableCatalog theEditor = (EditableCatalog)getRoot();
            CatalogLockManager theLocker =
                CatalogLockManager.getInstanceFor(theEditor.getCatalog());
            theLocker.releaseLock(theAttToEdit,
                                  false,
                                  theEditor.getUser());
        }
        return;
    }

/* *************************** IEditableAttribute *************************** */

    synchronized public IAttribute getAttributeReference()
    {
        return theAttToEdit;
    }

    /**
     * Synchronization: To avoid inconsistancies between threads "of
     * the same session"!
     */
    synchronized public void setType(String aNewType)
        throws CatalogException
    {
        if(isDeleted())
        {
            String theMessage = getRoot().getMessageResources().getMessage(
                ICatalogMessagesKeys.PCAT_ERR_IS_DELEDED);
            throw new CatalogException(theMessage);
        }

        /**@todo enforce consistency with existing attribute values*/

        if(!lock())//If new we get it any way
        {
            String theMessage =
                ((EditableCatalog)getRoot()).getCatalog().getMessageResources()
                .getMessage(ICatalogMessagesKeys.PCAT_ERR_LOCK);
            throw new CatalogException(theMessage);
        }

        /************************ Without CmdProcessor ************************
        setTypeInternal(aNewType);

        setStateChanged();
        ***********************************************************************/
        EditableCatalog theEditor = (EditableCatalog)getRoot();

        CatalogCmdProcessor processor = theEditor.getCmdProcessor();
        CatalogCmd command =
            processor.createCmdAttributeEdit(   this,
                                                aNewType,
                                                CatalogCmd.PROP_TYPE);
        if(!processor.doCmd(command))
            throw new CatalogException("");
            /**@todo real text has to be added **/
        return;
    }

    /**
     * Synchronization: To avoid inconsistancies between threads "of
     * the same session"!
     */
    synchronized public void setQuantity(String aQuantityGuid)
        throws CatalogException
    {
        /**@todo implement it*/
    }

    /**
     * Synchronization: To avoid inconsistancies between threads "of
     * the same session"!
     */
    synchronized public void setName(String aNewName)
        throws CatalogException
    {
        if(isDeleted())
        {
            String theMessage = getRoot().getMessageResources().getMessage(
                ICatalogMessagesKeys.PCAT_ERR_IS_DELEDED);
            throw new CatalogException(theMessage);
        }

        if(!lock())//if new we get it any way
        {
            String theMessage =
                getRoot().getMessageResources()
                .getMessage(ICatalogMessagesKeys.PCAT_ERR_LOCK);
            throw new CatalogException(theMessage);
        }

        /************************ Without CmdProcessor ************************
        setNameInternal(aNewName);

        //if not new we are changed now
        setStateChanged();
        ***********************************************************************/
        EditableCatalog theEditor = (EditableCatalog)getRoot();

        CatalogCmdProcessor processor = theEditor.getCmdProcessor();
        CatalogCmd command =
            processor.createCmdAttributeEdit(   this,
                                                aNewName,
                                                CatalogCmd.PROP_NAME);
        if(!processor.doCmd(command))
            throw new CatalogException(""); /**@todo real text has to be added **/

        return;
    }

    /**
     * Synchronization: To avoid inconsistancies between threads "of
     * the same session"!
     */
    synchronized public void setDescription(String aNewDescription)
        throws CatalogException
    {
        if(isDeleted())
        {
            String theMessage = getRoot().getMessageResources().getMessage(
                ICatalogMessagesKeys.PCAT_ERR_IS_DELEDED);
            throw new CatalogException(theMessage);
        }

        if(!lock())//if new we get it any way
        {
            String theMessage = getRoot().getMessageResources().getMessage(
                ICatalogMessagesKeys.PCAT_ERR_LOCK);
            throw new CatalogException(theMessage);
        }

        /************************ Without CmdProcessor ************************
        setDescriptionInternal(aNewDescription);

        //if not new we are changed now
        setStateChanged();
        ***********************************************************************/
        EditableCatalog theEditor = (EditableCatalog)getRoot();
        CatalogCmdProcessor processor = theEditor.getCmdProcessor();
        CatalogCmd command =
            processor.createCmdAttributeEdit(   this,
                                                aNewDescription,
                                                CatalogCmd.PROP_DESCRIPTION);
        if(!processor.doCmd(command))
            throw new CatalogException("");
            /**@todo real text has to be added **/

        return;
    }

    /**
     * Synchronization: To avoid inconsistancies between threads "of
     * the same session"!
     */
    synchronized public IEditableDetail createDetail(String aName)
        throws CatalogException
    {
        EditableCatalog theEditor = (EditableCatalog)getRoot();
        Catalog theCatToEdit = theEditor.getCatalog();

        //If we are deleted no way!
        if(isDeleted())
        {
            String theMessage = theCatToEdit.getMessageResources().getMessage(
                ICatalogMessagesKeys.PCAT_ERR_IS_DELEDED);
            throw new CatalogException(theMessage);
        }

        if(!lock())//if new we get it any way
        {
            String theMessage = theCatToEdit.getMessageResources().getMessage(
                ICatalogMessagesKeys.PCAT_ERR_LOCK);
            throw new CatalogException(theMessage);
        }

        /************************ Without CmdProcessor ************************
        EditableDetail aNewDetail = new EditableDetail(this,aName);

        //Add something the builder could not build because its new
        addDetailInternal(aNewDetail,true);

        //if not new we are changed now
        setStateChanged();
        ***********************************************************************/
        CatalogCmdProcessor processor = theEditor.getCmdProcessor();
        CatalogCmd command =
            processor.createCmdDetailCreate(this, aName);
        if(!processor.doCmd(command))
            throw new CatalogException(""); /**@todo real text has to be added **/

        //return aNewDetail;
        return (EditableDetail)getDetail(aName); // it's definitly of this type
    }

    /**
     * Synchronization: To avoid inconsistancies between threads "of
     * the same session"!
     */
    synchronized public void deleteDetail(IEditableDetail aThingToDelete)
        throws CatalogException
    {
        EditableDetail theThingToDelete = (EditableDetail) aThingToDelete;
        EditableCatalog theEditor = (EditableCatalog)getRoot();
        Catalog theCatToEdit = theEditor.getCatalog();

        //If we are deleted there is no chance to delete any thing over us.
        if(isDeleted())
        {
            String theMessage = theCatToEdit.getMessageResources().getMessage(
                ICatalogMessagesKeys.PCAT_ERR_IS_DELEDED);
            throw new CatalogException(theMessage);
        }

        //If the thing is allready deleted no need to delete it again.
        if(theThingToDelete.isDeleted())
        {
            String theMessage = theCatToEdit.getMessageResources().getMessage(
                ICatalogMessagesKeys.PCAT_ERR_IS_DELEDED);
            throw new CatalogException(theMessage);
        }

        //if the thing does not belong to me inform the stubborn user with an
        //exception
        if(!theDetails.contains(theThingToDelete))
        {
            String theMessage = theCatToEdit.getMessageResources().getMessage(
                ICatalogMessagesKeys.PCAT_ERR_OBJ_NOT_OWNED_THIS);
            throw new CatalogException(theMessage);
        }

        if(!lock())
        {
            String theMessage = theCatToEdit.getMessageResources().getMessage(
                ICatalogMessagesKeys.PCAT_ERR_LOCK);
            throw new CatalogException(theMessage);
        }
        if(!theThingToDelete.lock())
        {
            String theMessage = theCatToEdit.getMessageResources().getMessage(
                ICatalogMessagesKeys.PCAT_ERR_LOCK);
            throw new CatalogException(theMessage);
        }

        /************************ Without CmdProcessor ************************
        //we did it
        theThingToDelete.setStateDeleted();
        this.deleteDetailInternal(theThingToDelete);

        //if not new we are changed now
        setStateChanged();
        ***********************************************************************/
        CatalogCmdProcessor processor = theEditor.getCmdProcessor();
        CatalogCmd command =
            processor.createCmdDetailDelete(this, theThingToDelete);
        if(!processor.doCmd(command))
            throw new CatalogException("");
            /**@todo real text has to be added **/

        return;
    }

/******************************* event listening ******************************/

    synchronized public void componentChanged(IComponentChangedEvent anEvent)
    {
        //changed
        //update your state from the ref
        if(anEvent.getComponent().getState()!=IComponent.State.DELETED)
        {
            setNameInternal(theAttToEdit.getName());
            setGuidInternal(theAttToEdit.getGuid());
            setTypeInternal(theAttToEdit.getType());
            setQuantityInternal(theAttToEdit.getQuantity());
            setDomainInternal(theAttToEdit.getDomain());
        }

        //deleted
        if(anEvent.getComponent().getState()==IComponent.State.DELETED)
        {
            //set your state to deleted the "parent" i.e. the catlog will
            //remove you from his list
            this.setStateDeleted();
        }
    }

    synchronized public void detailCreated(IDetailCreatedEvent p0)
    {
        /**@todo implement listener interface */
        ;
    }

    synchronized public void detailDeleted(IDetailDeletedEvent anDeleteEvent)
    {
        /**@todo implement listener interface */
        ;
    }
}
