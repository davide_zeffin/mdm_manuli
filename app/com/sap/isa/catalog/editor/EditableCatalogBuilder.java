package com.sap.isa.catalog.editor;

import java.util.ArrayList;
import java.util.Iterator;

import com.sap.isa.catalog.boi.CatalogException;
import com.sap.isa.catalog.impl.Catalog;
import com.sap.isa.catalog.impl.CatalogAttribute;
import com.sap.isa.catalog.impl.CatalogAttributeValue;
import com.sap.isa.catalog.impl.CatalogBuildFailedException;
import com.sap.isa.catalog.impl.CatalogBuilder;
import com.sap.isa.catalog.impl.CatalogCategory;
import com.sap.isa.catalog.impl.CatalogDetail;
import com.sap.isa.catalog.impl.CatalogItem;
import com.sap.isa.catalog.impl.CatalogQuery;

public class EditableCatalogBuilder extends CatalogBuilder
{
    private EditableCatalog theEditor;
    private Catalog theCatalogToEdit;

    public EditableCatalogBuilder(EditableCatalog theEditor)
    {
        this.theEditor=theEditor;
        this.theCatalogToEdit=theEditor.getCatalog();
    }

    protected void buildDetailsInternal(CatalogAttributeValue attributeValue)
    {
        //currently we got no details for attributeValues
        return;
    }

    protected void buildDetailsInternal(CatalogDetail aDetail)
    {
        //currently we got no details for details
        return;
    }

    protected void buildDetailsInternal(CatalogItem aItem)
        throws CatalogBuildFailedException
    {
        // Will definitely be from that type
        EditableItem theItemEditor = (EditableItem)aItem;
        CatalogItem theItemReference = theItemEditor.getItem();

        //new category nothing to do for the builder
        if(theItemReference==null)
            return;

        try
        {
            // Get the details we are going to build editors for
            Iterator theDetailsIterator = theItemReference.getDetails();
            while(theDetailsIterator.hasNext())
            {
                CatalogDetail aDetailToEdit =
                    (CatalogDetail)theDetailsIterator.next();

                // The editable detail will pull the internal state from
                // aDetailToEdit
                EditableDetail aEditableDetail =
                    new EditableDetail(theItemEditor,aDetailToEdit);

                //Build the assoziation from parent to child
                theItemEditor.addDetailInternal(aEditableDetail);
            }
        }
        catch(CatalogException ce)
        {
            throw new CatalogBuildFailedException(ce.getMessage());
        }
        return;
    }

    protected void buildCategoriesInternal(Catalog aCatalog)
        throws CatalogBuildFailedException
    {
        //Will definitely be from that type
        EditableCatalog theCataEditor = (EditableCatalog)aCatalog;
        Catalog theCataToEdit = theCataEditor.getCatalog();

        try
        {
            Iterator theCategoryIterator = theCataToEdit.getChildCategories();
            while(theCategoryIterator.hasNext())
            {
                //We builded them already
                if(this.isAllCategoriesBuilt(aCatalog))
                {
                    CatalogCategory aCategoryToEdit =
                        (CatalogCategory)theCategoryIterator.next();
                    EditableCategory aEditableCategory = (EditableCategory)
                        aCatalog.getCategoryInternal(aCategoryToEdit.getGuid());
                    aEditableCategory.setParent(theCataEditor);
                }
                else
                {
                    CatalogCategory aCategoryToEdit =
                        (CatalogCategory)theCategoryIterator.next();

                    EditableCategory aEditableCategory =
                        new EditableCategory(theCataEditor,aCategoryToEdit);
                    theCataEditor.addChildCategoryInternal(aEditableCategory);
                    //add it to the hashtable
                    theCataEditor.addCategory(aEditableCategory);
                }
            }
        }
        catch(CatalogException ce)
        {
            throw new CatalogBuildFailedException(ce.getMessage());
        }
    }

    protected void buildAttributesInternal(CatalogCategory aCategory)
        throws CatalogBuildFailedException
    {
        //Will definitely be from that type
        EditableCategory theCateEditor = (EditableCategory)aCategory;
        CatalogCategory theCateToEdit = theCateEditor.getCategory();

        //new category nothing to do for the builder
        if(theCateToEdit==null)
            return;
        try
        {
            //Get the local attributes
            Iterator theLocalAttributesIter = theCateToEdit.getAttributes();
            while(theLocalAttributesIter.hasNext())
            {
                CatalogAttribute anAttributeToEdit =
                    (CatalogAttribute) theLocalAttributesIter.next();
                EditableAttribute anAttributeEditor =
                    new EditableAttribute(theCateEditor,anAttributeToEdit);
                theCateEditor.addAttributeInternal(anAttributeEditor);
            }

        }
        catch(CatalogException ce)
        {
            throw new CatalogBuildFailedException(ce.getMessage());
        }
    }

    protected void buildAllCategoriesInternal(Catalog aCatalog)
        throws CatalogBuildFailedException
    {
        //Will definitely be from that type
        EditableCatalog theCataEditor = (EditableCatalog)aCatalog;
        Catalog theCataToEdit = theCataEditor.getCatalog();
        try
        {
            Iterator theCategoryIterator = theCataToEdit.getCategories();
            ArrayList editorsWithoutParents = new ArrayList();
            while(theCategoryIterator.hasNext())
            {
                CatalogCategory aCategoryToEdit =
                    (CatalogCategory)theCategoryIterator.next();

                //allread build via browsing
                if(theCataEditor.getCategoryInternal(aCategoryToEdit.getGuid())!=null)
                    continue;

                //We don't know the actual parent right now
                EditableCategory aEditableCategory =
                    new EditableCategory(theCataEditor,aCategoryToEdit);
                    //new EditableCategory(null,aCategoryToEdit);

                //add it to the catalog internal hashTable
                theCataEditor.addCategory(aEditableCategory);
                editorsWithoutParents.add(aEditableCategory);
            }

            /*/Build the tree now
            for(int i = 0; i < editorsWithoutParents.size(); i++)
            {
                EditableCategory aCateEditor =
                    (EditableCategory)editorsWithoutParents.get(i);
                CatalogCategory aRef = aCateEditor.getCategory();
                ICategory theParentOfTheRef = aRef.getParent();
                if(theParentOfTheRef==null)//root
                {
                    aCateEditor.setParent(theCataEditor);
                }
                else
                {
                    EditableCategory aCateEditorParent = (EditableCategory)
                        theCataEditor.getCategoryInternal(theParentOfTheRef.getGuid());
                    aCateEditor.setParent(aCateEditorParent);
                    aCateEditorParent.addChildrenCategory(aCateEditor);
                }
            }*/
        }
        catch(CatalogException ce)
        {
            throw new CatalogBuildFailedException(ce.getMessage());
        }
    }

    protected void buildCategoriesInternal(CatalogCategory aCategory)
        throws CatalogBuildFailedException
    {
        //Will definitely be from that type
        EditableCategory theCateEditor = (EditableCategory)aCategory;
        EditableCatalog theCataEditor = (EditableCatalog)theCateEditor.getRoot();
        CatalogCategory theCateToEdit = theCateEditor.getCategory();

        //new category
        if(theCateToEdit==null)
        {
            return;
        }

        try
        {
            //Get the children categories from the category
            //theCateditor is editing
            Iterator theCategoryIterator =
                theCateToEdit.getChildCategories();
            while(theCategoryIterator.hasNext())
            {
                //We builded them already
                if(this.isAllCategoriesBuilt(theCataEditor))
                {
                    CatalogCategory aCategoryToEdit =
                        (CatalogCategory)theCategoryIterator.next();
                    EditableCategory aEditableCategory = (EditableCategory)
                        theCataEditor.getCategoryInternal(aCategoryToEdit.getGuid());
                    if(aEditableCategory!=null)//probably allready deleted
                    {
                        aEditableCategory.setParent(theCateEditor);
                        theCateEditor.addChildCategoryInternal(aEditableCategory);
                    }
                }
                else
                {
                    CatalogCategory aCategoryToEdit =
                        (CatalogCategory)theCategoryIterator.next();
                    //Create a new Editable category for each child
                    EditableCategory aEditableCategory =
                    new EditableCategory(theCateEditor,aCategoryToEdit);

                    theCateEditor.addChildCategoryInternal(aEditableCategory);

                    //add it to the hashTable
                    theCateEditor.getRoot().addCategory(aEditableCategory);
                }
            }
        }
        catch(CatalogException ce)
        {
            throw new CatalogBuildFailedException(ce.getMessage());
        }

    }

    protected void buildAttributesInternal(Catalog aCatalog)
        throws CatalogBuildFailedException
    {
        //Will definitely be from that type
        EditableCatalog theCataEditor = (EditableCatalog)aCatalog;
        Catalog theCataToEdit = theCataEditor.getCatalog();
        try
        {
            //Get the global attributes
            Iterator theGlobalAttributesIter = theCataToEdit.getAttributes();
            while(theGlobalAttributesIter.hasNext())
            {
                CatalogAttribute anAttributeToEdit =
                    (CatalogAttribute) theGlobalAttributesIter.next();
                EditableAttribute anAttributeEditor =
                    new EditableAttribute(theCataEditor,anAttributeToEdit);
                theCataEditor.addAttributeInternal(anAttributeEditor);
                anAttributeEditor.
                    setDescriptionInternal(anAttributeToEdit.getDescription());
                anAttributeEditor.setNameInternal(anAttributeToEdit.getName());
            }

        }
        catch(CatalogException ce)
        {
            throw new CatalogBuildFailedException(ce.getMessage());
        }
    }

    protected void buildDetailsInternal(Catalog aCatalog)
        throws CatalogBuildFailedException
    {
        // Will definitely be from that type
        EditableCatalog theCataEditor = (EditableCatalog)aCatalog;
        Catalog theCataToEdit = theCataEditor.getCatalog();
        try
        {
            // Get the details we are going to build editors for
            Iterator theDetailsIterator = theCataToEdit.getDetails();
            while(theDetailsIterator.hasNext())
            {
                CatalogDetail aDetailToEdit =
                    (CatalogDetail)theDetailsIterator.next();

                // The editable detail will pull the internal state from
                // aDetailToEdit
                EditableDetail aEditableDetail =
                    new EditableDetail(theCataEditor,aDetailToEdit);

                //Build the assoziation from parent to child
                theCataEditor.addDetailInternal(aEditableDetail);
            }
        }
        catch(CatalogException ce)
        {
            throw new CatalogBuildFailedException(ce.getMessage());
        }
        return;
    }

    protected void buildDetailsInternal(CatalogAttribute aAttribute)
        throws CatalogBuildFailedException
    {
        // Will be definitely from that type
        EditableAttribute theAttEditor = (EditableAttribute)aAttribute;
        CatalogAttribute theAttToEdit = theAttEditor.getAttribute();
        try
        {
            // Get the details we are going to build editors for
            Iterator theDetailsIterator = theAttToEdit.getDetails();
            while(theDetailsIterator.hasNext())
            {
                CatalogDetail aDetailToEdit =
                    (CatalogDetail)theDetailsIterator.next();

                // The editable detail will pull the internal state from
                // aDetailToEdit
                EditableDetail aEditableDetail =
                    new EditableDetail(theAttEditor,aDetailToEdit);

                // create the assoziation from the parent to the child
                theAttEditor.addDetailInternal(aEditableDetail);
            }
        }
        catch(CatalogException ce)
        {
            throw new CatalogBuildFailedException(ce.getMessage());
        }
    }

    protected void buildDetailsInternal(CatalogCategory aCategory)
        throws CatalogBuildFailedException
    {
        //Will definitely be from that type
        EditableCategory theCateEditor = (EditableCategory)aCategory;
        CatalogCategory theCateToEdit = theCateEditor.getCategory();

        //nothing to do for the builder
        if(theCateToEdit==null)
            return;
        try
        {
            //Get the details we are build editors for
            Iterator theDetailsIterator = theCateToEdit.getDetails();
            while(theDetailsIterator.hasNext())
            {
                CatalogDetail aDetailToEdit =
                    (CatalogDetail)theDetailsIterator.next();

                // The editable detail will pull the internal state from
                // aDetailToEdit
                EditableDetail aEditableDetail =
                    new EditableDetail(theCateEditor,aDetailToEdit);

                // create the assoziation from the parent to the child
                theCateEditor.addDetailInternal(aEditableDetail);
            }
        }
        catch(CatalogException ce)
        {
            throw new CatalogBuildFailedException(ce.getMessage());
        }
    }

    protected void buildQueryInternal(CatalogQuery aQuery)
        throws CatalogBuildFailedException
    {
        /**@todo: implement this com.sap.isa.catalog.impl.CatalogBuilder abstract method*/
    }

    protected void buildItemsInternal(CatalogCategory aCategory)
        throws CatalogBuildFailedException
    {
        //Will definitely be from that type
        EditableCategory theCateEditor = (EditableCategory)aCategory;
        CatalogCategory theCateToEdit = theCateEditor.getCategory();

        //new category nothing to do for us
        if(theCateToEdit==null)
            return;
        try
        {
            //Get the items we want to edit
            Iterator theItemsIter = theCateToEdit.getItems();
            while(theItemsIter.hasNext())
            {
                CatalogItem anItemToEdit =
                    (CatalogItem) theItemsIter.next();

                EditableItem anItemEditor =
                    new EditableItem(theCateEditor,anItemToEdit);

                theCateEditor.addItemInternal(anItemEditor);
                //Now add the editable values to the editable item
                Iterator theValueIter = anItemToEdit.getAttributeValues();
                while(theValueIter.hasNext())
                {
                    CatalogAttributeValue aValue = (CatalogAttributeValue)
                        theValueIter.next();
                    CatalogAttribute theCorrespondingAttrib =
                        (CatalogAttribute) aValue.getAttribute();

                    EditableAttribute anEditableAttribute =
                        (EditableAttribute) theCateEditor.getAttributeInternal(
                        theCorrespondingAttrib.getGuid());

                    if(anEditableAttribute!=null)//Attribute not deleted
                    {
                        EditableAttributeValue aNewValue =
                            new EditableAttributeValue( anItemEditor,
                                                        anEditableAttribute,
                                                        aValue);
                        anItemEditor.addAttributeValueInternal(aNewValue);
                    }
                }
            }
        }
        catch(CatalogException ce)
        {
            throw new CatalogBuildFailedException(ce.getMessage());
        }
    }
}