/*****************************************************************************
  Copyright (c) 2000, SAPMarkets Europe GmbH, Germany, All rights reserved.
  Created:      09 July 2001

  $Id: //sap/ESALES_base/30_SP_COR/src/_pcatAPI/java/com/sap/isa/catalog/editor/CatalogXMLUpdateVisitor.java#2 $
  $Revision: #2 $
  $Change: 127411 $
  $DateTime: 2003/04/30 17:20:32 $
*****************************************************************************/

package com.sap.isa.catalog.editor;

// catalog imports
import java.util.ArrayList;
import java.util.Iterator;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Text;

import com.sap.isa.catalog.ICatalogMessagesKeys;
import com.sap.isa.catalog.boi.CatalogException;
import com.sap.isa.catalog.boi.IComponent;
import com.sap.isa.catalog.boi.IEditableComponent;
import com.sap.isa.catalog.boi.IEditableCompositeComponent;
import com.sap.isa.catalog.boi.IQuantity;
import com.sap.isa.catalog.boi.IUnit;
import com.sap.isa.catalog.catalogvisitor.CatalogVisitor;
import com.sap.isa.catalog.impl.Catalog;
import com.sap.isa.catalog.impl.CatalogAttribute;
import com.sap.isa.catalog.impl.CatalogAttributeValue;
import com.sap.isa.catalog.impl.CatalogCategory;
import com.sap.isa.catalog.impl.CatalogComponent;
import com.sap.isa.catalog.impl.CatalogCompositeComponent;
import com.sap.isa.catalog.impl.CatalogDetail;
import com.sap.isa.catalog.impl.CatalogItem;
import com.sap.isa.catalog.impl.CatalogQuery;
import com.sap.isa.catalog.impl.CatalogQueryItem;
import com.sap.isa.catalog.impl.CatalogQueryStatement;
import com.sap.isa.core.logging.IsaLocation;

/**
 *  This visitor iterates over a given editor catalog tree and converts it
 *  into an XML document that represents the the change status of the
 *  editor tree.
 *
 *  @version     1.0
 *  @since       2.0
 */
public class CatalogXMLUpdateVisitor
    extends CatalogVisitor
{
    // the logging instance
    private static IsaLocation theStaticLocToLog =
        IsaLocation.getInstance(CatalogXMLUpdateVisitor.class.getName());
	private Document theDocument;
	private Element theElement;

    /**
     * Constructs a new visitor that persists an editor tree into an XML
	 * document.
     *
     * @param root the root node of the editor tree persist
     */
    CatalogXMLUpdateVisitor(EditableCatalog root)
    {
        super(root);
        theStaticLocToLog = IsaLocation.getInstance(this.getClass().getName());
    }

    /**
     * Starts the traversal of the tree.
     *
     * @throws CatalogException in case the xml creation fails
     */
    public Document start() throws CatalogException
    {
		try
		{
            DocumentBuilderFactory aXMLFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder aDocBuilder = aXMLFactory.newDocumentBuilder();
            theDocument = aDocBuilder.newDocument();
		}
		catch(ParserConfigurationException pce)
		{
			theStaticLocToLog.error(
				ICatalogMessagesKeys.PCAT_XML_CONFIGURATION_ERR,
				pce);
			throw new CatalogException(pce.getMessage());
		}

        // start traversation
        getRoot().assign(this);

		return theDocument;
    }

    /**
     * Visit the catalog. Called from the start method. Visits the catalog and
     * traverses the tree depth first.
     * Every descendant is visited because it might be the case that e.g.
     * only an attribute value down the tree was changed and so the catalog
     * itself was not changed at all.
     *
     * @param category the editable Catalog to visit.
     */
    public void visitCatalog(Catalog catalog)
    {
        EditableCatalog theEditor = (EditableCatalog)catalog;
        Catalog theCatRef = theEditor.getCatalog();

        if(theStaticLocToLog.isDebugEnabled())
        {
            theStaticLocToLog.debug("Visting Catalog to build update xml: " +
				theEditor.getName());
        }

        try{//for throws from the get** catalog

        //synchronized with catalog/editor down the call stack
        //just to be obvious
        synchronized(theEditor){

		//Start with the <Catalog></Catalog> element
		theElement = theDocument.createElement("Catalog");
		theElement.setAttribute("status","loaded");
		theDocument.appendChild(theElement);

		//For the catlog there is allways a reference
        synchronized(theCatRef){

        //state New not possible
        //state Deleted not possible
        //state changed synch your state to the decorated reference
        if(theEditor.getState()==IComponent.State.CHANGED)
        {
			theElement.setAttribute("status","changed");

            //put your state to the thing to edit
            String aNewName = theEditor.getName();
            String aOldName = theCatRef.getName();
            if(aNewName.intern()!=aOldName.intern())
            {
				Element aNameDetail = detailUpdate("name",
					new String[] {aNewName} );
				theElement.appendChild(aNameDetail);
            }

            String aNewDesc = theEditor.getDescription();
            String aOldDesc = theCatRef.getDescription();
            if(aNewDesc.intern()!=aOldDesc.intern())
            {
				Element aDescriptionDetail = detailUpdate("description",
					new String[] {aNewDesc} );
				theElement.appendChild(aDescriptionDetail);
            }

            String aNewThumbNail = theEditor.getThumbNail();
            String aOldThumbNail = theCatRef.getDescription();
            if(aNewThumbNail.intern()!=aOldThumbNail.intern())
            {
				Element aDescriptionDetail = detailUpdate("thumbNail",
					new String[] {aNewThumbNail} );
				theElement.appendChild(aDescriptionDetail);
            }
        }

        // visit the editable details
		visitDetails(theEditor);

        // visit the edititable attributes
		visitAttributes(theEditor);

        // visit the categories
		visitCategories(theEditor);

        }//end synchronized(theCatRef)
        }//synchronized(catalog)
        }
		catch(CatalogException ce)
        {
            /**@todo add appropriate action*/
            return;
        }
    }

    /**
     * Visit a category. Called from the corresponding parent category.
     * Every descendant, is visited because it might be the case that e.g.
     * only an attribute value down the tree was changed and so the category
     * was not changed at all.
     *
     * @param category the editable category to visit.
     */
    public void visitCategory(CatalogCategory category)
    {
        EditableCategory theCateEditor = (EditableCategory)category;
        EditableCatalog theEditor =
            (EditableCatalog) theCateEditor.getRoot();

        CatalogCategory theCateRef = theCateEditor.getCategory();

        if(theStaticLocToLog.isDebugEnabled())
        {
            theStaticLocToLog.debug("Visting Category to build up xml: "
			+ theCateEditor.getName());
        }

		try{//for throws from the get** category

        //synchronized with category down the call stack
        //just to be obvious
        synchronized(theEditor){

		// add a new element to the xml
		Element theParent = theElement;
		theElement = theDocument.createElement("Category");
		theElement.setAttribute("ID",theCateEditor.getGuid());
		theElement.setAttribute("status","loaded");

        //Update internal state
        if(theCateEditor.isChanged())
        {
			theElement.setAttribute("status","changed");

            //put your state to the thing to edit
            String aNewName = theCateEditor.getName();
            String aOldName = theCateRef.getName();
            if(aNewName.intern()!=aOldName.intern())
            {
				Element aNameDetail = detailUpdate("name",
					new String[]{aNewName});
				theElement.appendChild(aNameDetail);
            }

            String aNewDesc = theEditor.getDescription();
            String aOldDesc = theCateRef.getDescription();
            if(aNewDesc.intern()!=aOldDesc.intern())
            {
				Element aDescriptionDetail = detailUpdate("description",
					new String[] {aNewDesc});
				theElement.appendChild(aDescriptionDetail);
            }

            String aNewThumbNail = theEditor.getThumbNail();
            String aOldThumbNail = theCateRef.getThumbNail();
            if(aNewThumbNail.intern()!=aOldThumbNail.intern())
            {
				Element aThumbDetail = this.detailUpdate("thumbNail",
					new String[] {aNewThumbNail});
				theElement.appendChild(aThumbDetail);
            }
        }
        else if(theCateEditor.isNew())
        {
			theElement.setAttribute("status","added");

            String aNewName = theCateEditor.getName();
			Element aNameDetail = detailAdded("name",
				new String[]{aNewName});

			theElement.appendChild(aNameDetail);

            String aNewDesc = theEditor.getDescription();
			Element aDescriptionDetail = detailAdded("description",
				new String[] {aNewDesc});

			theElement.appendChild(aDescriptionDetail);

            String aNewThumbNail = theEditor.getThumbNail();
			Element aThumbDetail = this.detailAdded("thumbNail",
				new String[] {aNewThumbNail});

			theElement.appendChild(aThumbDetail);
        }
		else
			theElement.setAttribute("status","loaded");

        // visit the editable details
		visitDetails(theCateEditor);

        // visit the edititable attributes
		visitAttributes(theCateEditor);

		// visit the editable categories
		visitCategories(theCateEditor);

        // visit the editable items
        boolean somethingToDo =
            (!theCateEditor.isNew() &&
			 theEditor.getCatalogBuilder().isItemsBuilt(theCateEditor))
            || theCateEditor.getItemsInternal().hasNext();

        boolean askedReferenceForItems = !theCateEditor.isNew() &&
            theEditor.getCatalogBuilder().isItemsBuilt(theCateEditor);

        if(somethingToDo)
        {
            if(askedReferenceForItems)
            {
                //So the cat ref has them also i.e. no call is triggered
                Iterator itemsIter = theCateRef.getItems();

                ArrayList aListOfItems = new ArrayList();
                while(itemsIter.hasNext())
                {
                    aListOfItems.add(itemsIter.next());
                }

                //remove deleted items
                for(int i=0; i < aListOfItems.size(); i++)
                {
                    CatalogItem aItemRef =
                        (CatalogItem) aListOfItems.get(i);

                    synchronized(aItemRef){
                    //deleted?
                    Iterator anIter = theCateEditor.getItems();
                    boolean deleted = true;
                    while(deleted && anIter.hasNext())
                    {
                        EditableItem aItem =
                            (EditableItem) anIter.next();
                        if(aItem.getGuid().intern()==aItemRef.getGuid().intern())
                            deleted = false;
                    }
                    if(deleted)
                    {
						Element anItemElement =
							theDocument.createElement("Item");
						anItemElement.setAttribute("ID",aItemRef.getGuid());
						anItemElement.setAttribute("status","deleted");
						theElement.appendChild(anItemElement);
                    }
                    }//end synchronized(aItemRef)
                }
            }

            // now visit what is left or new
            Iterator itemsIter = theCateEditor.getItemsInternal();

            while(itemsIter.hasNext())
            {
                EditableItem aItem = (EditableItem)itemsIter.next();
                synchronized(aItem){
                    //changed & new are handled by the detail self
                    aItem.assign(this);
                }//end synchronized(aItem){
            }
        }//end visting items

		//return to your parent and add your self,
		//in case any child was added i.e. some descendant was
		//actually changed/adedd/deleted
		if( theElement.hasChildNodes() ||
			!theElement.getAttribute("status").equals("loaded"))
			theParent.appendChild(theElement);

		theElement = theParent;

        }//end synchronized(category)

        }
		catch(CatalogException ce)
        {
            return;
        }
        return;
    }

    /**
     * Visit a item. Called from the corresponding category.
     * Every item is visited because it might be the case that e.g.
     * only an attribute value was changed and so the item was not
     * changed at all.
     *
     * @param item the editable item to visit.
     */
    public void visitItem(CatalogItem item)
    {
        EditableItem theItemEditor = (EditableItem)item;
        CatalogItem theItemRef = theItemEditor.getItem();

        //unused: EditableCatalog theEditor =
        //    (EditableCatalog) theItemEditor.getRoot();

        if(theStaticLocToLog.isDebugEnabled())
        {
            theStaticLocToLog.debug("Visting Item to build XML: " + item.getName());
        }

        try{

        //synchronized with item down the call stack
        //just to be obvious
        synchronized(theItemEditor){

		Element theParent = theElement;
		theElement = theDocument.createElement("Item");
		theElement.setAttribute("ID",theItemEditor.getGuid());
		theElement.setAttribute("orderItem",theItemEditor.getProductGuid());

        if(theItemEditor.isChanged())
        {
            //put your state to the thing to edit
            String aNewName = theItemEditor.getName();
            String aOldName = theItemRef.getName();
            if(aNewName.intern()!=aOldName.intern())
            {
				Element aNameDetail = detailUpdate("name",
					new String[]{aNewName});
				theElement.appendChild(aNameDetail);
            }

			theElement.setAttribute("status","changed");
        }
		else if(theItemEditor.isNew())
		{
            //put your state to the thing to edit
            String aNewName = theItemEditor.getName();
			Element aNameDetail = detailAdded("name",
					new String[]{aNewName});

			theElement.appendChild(aNameDetail);

			theElement.setAttribute("status","added");
		}
		else
			theElement.setAttribute("status","loaded");

        // visit the editable details
		visitDetails(theItemEditor);

        // visit AttributeValues
        // logic different than for the rest because attribute values are always
        // builded for items.
        // i.e. if you got the item (thats the reason you are here)
        // you got the values from the ref and the
        // ref got the values in any case already from the server,
        // so just remove deleted values, add new ones and clean up
        // changed values.
        boolean somethingToDo =
            theItemEditor.getAttributeValues().hasNext() ||
            (theItemRef!=null && theItemRef.getAttributeValues().hasNext());

        if(somethingToDo)
        {
			//check for deleted values
			if(theItemRef!=null)//i.e. not new
			{
                Iterator aValuesIter = theItemRef.getAttributeValues();

                ArrayList aListOfValues = new ArrayList();
                while(aValuesIter.hasNext())
                {
                    aListOfValues.add(aValuesIter.next());
                }

                //remove deleted values
                for(int i=0; i < aListOfValues.size(); i++)
                {
                    CatalogAttributeValue aValueRef =
                        (CatalogAttributeValue) aListOfValues.get(i);

                    synchronized(aValueRef){
                    //deleted
                    if(theItemEditor.getAttributeValue(
                        aValueRef.getAttributeGuid())==null)
                    {
					    Element aDeletedValue =
						    attributeValueDeleted(aValueRef.getAttributeGuid(),
						    aValueRef.getAllAsString());
				        theElement.appendChild(aDeletedValue);
                    }
                    }//end synchronized(aValueRef)
                }
			}

            Iterator aValuesIter = theItemEditor.getAttributeValues();

            while(aValuesIter.hasNext())
            {
                EditableAttributeValue aValue =
                    (EditableAttributeValue)aValuesIter.next();
                synchronized(aValue){
                if(!aValue.isUnchanged())
                {
                    //changed & new are handled by the detail self
                    aValue.assign(this);
                }
                }//end synchronized(aDetail){
            }
        }//end visting attribute values

		//return to your parent and add your self,
		//in case any child was added i.e. some descendant was
		//actually changed/adedd/deleted
		if( theElement.hasChildNodes() ||
			!theElement.getAttribute("status").equals("loaded"))
			theParent.appendChild(theElement);

		theElement = theParent;

        }//synchronized(item)

        }catch(CatalogException ce)
        {
            return;
        }
    }

    public void visitQueryItem(CatalogQueryItem item)
    {
        //nothing to do
    }

    /**
     * Visit a changed or new detail.
	 * Called from the coressponding parent component.
     *
     * @param detail the changed editable detail that needs a clean up.
     */
    public void visitDetail(CatalogDetail detail)
    {
        EditableDetail aEditableDetail = (EditableDetail)detail;
        CatalogDetail aDetailRef =  aEditableDetail.getDetail();

        if(theStaticLocToLog.isDebugEnabled())
        {
            theStaticLocToLog.debug("Visting Detail to build update xml: " +
                aEditableDetail.getName());
        }

        synchronized(aEditableDetail){

        //state changed i.e. aDetailRef != null
        if(aEditableDetail.isChanged())
        {
		    synchronized(aDetailRef){
            String aNewName = aEditableDetail.getName();
            String aOldName = aDetailRef.getName();

			Element theChanged = detailUpdate(  aEditableDetail.getName(),
												aEditableDetail.getAllAsString());

            if(aNewName.intern()!=aOldName.intern())
            {
				Element theOld =
					detailDelete(aDetailRef.getName(),
					aDetailRef.getAllAsString());
				theElement.appendChild(theOld);
				theChanged.setAttribute("status","added");
            }
			theElement.appendChild(theChanged);
	        }//end synchronized(aDetailRef)
        }
		else if(aEditableDetail.isNew())
		{
			Element theChanged = detailAdded( aEditableDetail.getName(),
											  aEditableDetail.getAllAsString());
			theElement.appendChild(theChanged);
		}//we are here onlxy in case changed or new
        }//synchronized(aEditableDetail)
		return;
    }

    /**
     * Visit an Attribute. Called from the corresponding parent composite.
     * Every descendant,  is visited because it might be the case that e.g.
     * only an detail value down the tree was changed and so the attribute
     * was not changed at all.
     *
     * @param category the editable category to visit.
     */
    public void visitAttribute(CatalogAttribute attribute)
    {
        EditableAttribute theAttributeEditor = (EditableAttribute)attribute;
        CatalogAttribute theAttributeRef = theAttributeEditor.getAttribute();

        //unused: EditableCatalog theEditor =
        //    (EditableCatalog)theAttributeEditor.getRoot();

        if(theStaticLocToLog.isDebugEnabled())
        {
            theStaticLocToLog.debug("Visting Attribute: " + theAttributeEditor.getName());
        }

		Element anAttributeElement = theDocument.createElement("Attribute");
		anAttributeElement.setAttribute("ID",attribute.getGuid());
		anAttributeElement.setAttribute("type",attribute.getType());
        IQuantity aQuantity = attribute.getQuantity();

		if(aQuantity!=null)
		{
			anAttributeElement.setAttribute("quantity",aQuantity.getGuid());
		    Iterator theUnits = aQuantity.getUnits();
	    	StringBuffer ids = new StringBuffer();
		    while(theUnits.hasNext())
		    {
			    IUnit aUnit = (IUnit) theUnits.next();
			    ids.append(aUnit.getGuid());
			    if(theUnits.hasNext())
				    ids.append(' ');
		    }
		    if(ids.length()>0)
	            anAttributeElement.setAttribute("units",ids.toString());
		}

		Element theParent = theElement;
		theElement = anAttributeElement;

        try{//for throws from the get** catalog

        //synchronized with attribute/editor down the call stack
        //just to be obvious
        synchronized(theAttributeEditor){

        //state changed synch your state to the decorated reference
        if(theAttributeEditor.isChanged())
        {
	        synchronized(theAttributeRef){

            //compare your state withe the thing to edit
            String aNewName = theAttributeEditor.getName();
            String aOldName = theAttributeRef.getName();

			if(!aNewName.equals(aOldName))
			{
			    Element aNameDetail = detailUpdate("name",
	    		    new String[]{theAttributeEditor.getName()});
		        anAttributeElement.appendChild(aNameDetail);
			}

            String aNewDesc = theAttributeEditor.getDescription();
            String aOldDesc = theAttributeRef.getDescription();
			if(!aNewDesc.equals(aOldDesc))
			{
		        Element aDescriptionDetail = this.detailUpdate("description",
			        new String[]{theAttributeEditor.getDescription()});
	            anAttributeElement.appendChild(aDescriptionDetail);
	        }

			theElement.setAttribute("status","changed");

            /**@todo add the rest of the internal state*/

	        }//end synchronized(theAttributeRef)
        }
		else if (theAttributeEditor.isNew())
		{
			Element aNameDetail = detailAdded("name",
	    		new String[]{theAttributeEditor.getName()});
		    anAttributeElement.appendChild(aNameDetail);

		    Element aDescriptionDetail = this.detailAdded("description",
			    new String[]{theAttributeEditor.getDescription()});
	        anAttributeElement.appendChild(aDescriptionDetail);

            /**@todo add the rest of the internal state*/

			theElement.setAttribute("status","added");
		}
		else
			theElement.setAttribute("status","loaded");

        // visit the editable details
		visitDetails(theAttributeEditor);

		//return to your parent and add your self,
		//in case any child was added i.e. some descendant was
		//actually changed/adedd/deleted
		if( theElement.hasChildNodes() ||
			!theElement.getAttribute("status").equals("loaded"))
			theParent.appendChild(theElement);

		theElement = theParent;

        }//synchronized(attribute)
        }
		catch(CatalogException ce)
        {
            return;
        }
		return;
    }

    /**
     * Visit a changed or new value. Called from the coressponding item parent.
     *
     * @param attrValue the changed or new value that needs a clean up.
     */
    public void visitAttributeValue(CatalogAttributeValue attrValue)
    {
        EditableAttributeValue theAttributeValueEditor =
			(EditableAttributeValue)attrValue;
        CatalogAttributeValue theAttributeValueRef =
			theAttributeValueEditor.getAttributeValue();

        if(theStaticLocToLog.isDebugEnabled())
        {
            theStaticLocToLog.debug("Visting Value for Attribute: to build XML " +
                attrValue.getAttributeName());
            theStaticLocToLog.debug("Updating Value from : " +
                theAttributeValueRef.getAsString() + " to: " +
				theAttributeValueEditor.getAsString());
        }

        //synchronized with attrValue down the call stack
        //just to be obvious
        //only changed values are visited from the corressponding item!
        synchronized(theAttributeValueEditor){

		if(theAttributeValueEditor.isChanged())
		{
			Element anChangedValue = attributeValueUpdated(
				theAttributeValueEditor.getAttributeGuid(),
				theAttributeValueEditor.getAllAsString());
			theElement.appendChild(anChangedValue);
		}
		else
		{
			Element anAddedValue = attributeValueAdded(
				theAttributeValueEditor.getAttributeGuid(),
				theAttributeValueEditor.getAllAsString());
			theElement.appendChild(anAddedValue);
		}

        }//	end synchronized(theAttributeValueEditor)
		return;
    }

    public void visitQueryStatement(CatalogQueryStatement statement)
    {
        // here is nothing to do!!
    }

    public void visitQuery(CatalogQuery query)
    {
        // nothing to do
    }

/* ************************* Private **************************************** */

	/**
	 * Prepare the visit of all details of the current context
	 * <code>theElement</code>.
	 *
	 * @param   aComponent the editable CatalogComponent that represents
	 *          theElement
	 */
	private void visitDetails(CatalogComponent aComponent)
		throws CatalogException
	{
        // visit the editable details
		CatalogComponent aComponentRef =
			(CatalogComponent)
			((IEditableComponent)aComponent).getComponentReference();

        // is actually someting to do?
        boolean somethingToDo =
            (!aComponent.isNew() &&
			 getRoot().getCatalogBuilder().isDetailsBuilt(aComponent))
            || aComponent.getDetailsInternal().hasNext();
        //in case we never touched the rference but simply created new ones

        boolean askedReferenceForDetails = !aComponent.isNew() &&
            getRoot().getCatalogBuilder().isDetailsBuilt(aComponent);

        if(somethingToDo)
        {
            if(askedReferenceForDetails)
            {
                //So the cat ref has them also i.e. no call is triggered
                Iterator detailsIter = aComponentRef.getDetails();

                ArrayList aListOfDetails = new ArrayList();
                while(detailsIter.hasNext())
                {
                    aListOfDetails.add(detailsIter.next());
                }

                //remove deleted details
                for(int i=0; i < aListOfDetails.size(); i++)
                {
                    CatalogDetail aDetailRef =
                        (CatalogDetail) aListOfDetails.get(i);

                    synchronized(aDetailRef){
                    //deleted
                    if(aComponent.getDetail(aDetailRef.getName())==null)
                    {
					    Element aDeletedDetail =
							detailDelete(aDetailRef.getName(),
							aDetailRef.getAllAsString());
						theElement.appendChild(aDeletedDetail);
                    }
                    }//end synchronized(aDetailRef)
                }
            }

            Iterator detailsIter = aComponent.getDetailsInternal();

            while(detailsIter.hasNext())
            {
                EditableDetail aDetail = (EditableDetail)detailsIter.next();
                //unused: CatalogDetail aDetailRef = aDetail.getDetail();
                synchronized(aDetail){
                    //new & changed are handled by the detail self
                    aDetail.assign(this);
                }//end synchronized(aDetail){
            }
        }//end visting details
		return;
	}

	/**
	 * Prepare the visit of all categories of the current context
	 * <code>theElement</code>.
	 *
	 * @param aComposite an editable composite that represents theElement.
	 */
	private void visitCategories(CatalogCompositeComponent aComposite)
		throws CatalogException
	{
		CatalogCompositeComponent aCompositeRef =
			(CatalogCompositeComponent)
			((IEditableCompositeComponent)aComposite).getComponentReference();

        // visit the categories
        boolean somethingToDo = (aCompositeRef!=null &&(
            getRoot().getCatalogBuilder().isCategoriesBuilt(aComposite) ||
            getRoot().getCatalogBuilder().isAllCategoriesBuilt(getRoot()))) ||
            aComposite.getChildCategoriesInternal().hasNext();

        boolean askedReferenceForCategories = aCompositeRef!=null && (
            getRoot().getCatalogBuilder().isCategoriesBuilt(aComposite) ||
            getRoot().getCatalogBuilder().isAllCategoriesBuilt(getRoot()));

        if(somethingToDo)
        {
            if(askedReferenceForCategories)
            {
                //was allready asked no call is triggered
                Iterator categoriesIter =
                    categoriesIter = aCompositeRef.getChildCategories();

                ArrayList aListOfRootCategories = new ArrayList();

                while(categoriesIter.hasNext())
                {
                    aListOfRootCategories.add(categoriesIter.next());
                }

                //remove deleted categories from the ref
                for(int i=0; i < aListOfRootCategories.size(); i++)
                {
                    CatalogCategory aCategoryRef =
                        (CatalogCategory) aListOfRootCategories.get(i);

                    synchronized(aCategoryRef){

                    //deleted
                    Iterator anIter = null;
                    anIter = aComposite.getChildCategories();

                    boolean deleted = true;
                    while(deleted && anIter.hasNext())
                    {
                        EditableCategory aCate =
                            (EditableCategory) anIter.next();
                        if( aCate.getGuid().intern()==
							aCategoryRef.getGuid().intern())
                            deleted = false;
                    }
                    if(deleted)
                    {
						Element aCategoryElement =
							theDocument.createElement("Category");
						aCategoryElement.setAttribute("ID",
							aCategoryRef.getGuid());
						aCategoryElement.setAttribute("status","deleted");
						theElement.appendChild(aCategoryElement);
                    }
                    }//end synchronized(aCategoryRef)
                }
            }

            Iterator categoriesIter = aComposite.getChildCategoriesInternal();

            while(categoriesIter.hasNext())
            {
                EditableCategory aCate = (EditableCategory)categoriesIter.next();
                synchronized(aCate){
                    //changed/new are handled by the category self
                    aCate.assign(this);
                }//end synchronized(aCate){
            }
        }// end visit the categories
		return;
	}

	/**
	 * Prepare to visit and vist the attributes of aComposite.
	 *
	 * @param aComposite the current context
	 */
	private void visitAttributes(CatalogCompositeComponent aComposite)
		throws CatalogException
	{
		CatalogCompositeComponent aCompositeRef =
			(CatalogCompositeComponent)
			((IEditableCompositeComponent)aComposite).getComponentReference();

        // visit the edititable attributes
        boolean somethingToDo = (aCompositeRef!=null &&
            getRoot().getCatalogBuilder().isAttributesBuilt(aComposite)) ||
            aComposite.getAttributesInternal().hasNext();

        boolean askedReferenceForAttributes = aCompositeRef!=null &&
            getRoot().getCatalogBuilder().isAttributesBuilt(aComposite);

        if(somethingToDo)
        {
            if(askedReferenceForAttributes)
            {
                //no call triggered we asked allready
                Iterator aAttributeIterator = aCompositeRef.getAttributes();

                ArrayList aListOfAttributes = new ArrayList();

                while(aAttributeIterator.hasNext())
                {
                    aListOfAttributes.add(aAttributeIterator.next());
                }

                for(int i = 0; i < aListOfAttributes.size(); i++)
                {
                    CatalogAttribute aAttributeRef =
                        (CatalogAttribute) aListOfAttributes.get(i);
                    synchronized(aAttributeRef){
                    //deleted
                    Iterator theAttribIter = aComposite.getAttributes();
                    boolean deleted = true;
                    while(deleted && theAttribIter.hasNext())
                    {
                        EditableAttribute aEditableAttribute =
                            (EditableAttribute) theAttribIter.next();
                        if(aEditableAttribute.getGuid().equals(aAttributeRef.getGuid()))
                            deleted=false;
                    }
                    if(deleted)
                    {
						Element anAttributeElement =
							theDocument.createElement("Attribute");
						anAttributeElement.setAttribute("ID",
							aAttributeRef.getGuid());
						anAttributeElement.setAttribute("status","deleted");
						theElement.appendChild(anAttributeElement);
                    }
                    }//end synchronized(aAttributeRef)
                }
            }

            Iterator aAttributeIterator = aComposite.getAttributesInternal();

            while(aAttributeIterator.hasNext())
            {
                EditableAttribute aEditableAttribute = (EditableAttribute)
                    aAttributeIterator.next();
                synchronized(aEditableAttribute){
	                //changed & new is handled by the attribute itself
                    aEditableAttribute.assign(this);
                }//end synchronized(aEditableAttribute)
            }
        }//end visting attributes
		return;
	}

	private Element attributeValueDeleted(  String attribId,
										    Iterator theValuesOfTheDeleted)
	{
		Element anAttributeValueElement = theDocument.createElement("AttributeValue");
		anAttributeValueElement.setAttribute("attribute",attribId);
		anAttributeValueElement.setAttribute("status","deleted");
		while(theValuesOfTheDeleted.hasNext())
		{
			Element aValue = theDocument.createElement("Value");

		    Text aValueText =
		        theDocument.createTextNode((String)theValuesOfTheDeleted.next());
		    aValue.appendChild(aValueText);
			anAttributeValueElement.appendChild(aValue);
		}
		return anAttributeValueElement;
	}

	private Element attributeValueUpdated(  String attribId,
										    Iterator theValuesOfTheChanged)
	{
		Element anAttributeValueElement = theDocument.createElement("AttributeValue");
		anAttributeValueElement.setAttribute("attribute",attribId);
		anAttributeValueElement.setAttribute("status","changed");
		while(theValuesOfTheChanged.hasNext())
		{
			Element aValue = theDocument.createElement("Value");

		    Text aValueText =
		        theDocument.createTextNode((String)theValuesOfTheChanged.next());
		    aValue.appendChild(aValueText);
			anAttributeValueElement.appendChild(aValue);
		}
		return anAttributeValueElement;
	}

	private Element attributeValueAdded(   String attribId,
										   Iterator theValuesOfTheAdded)
	{
		Element anAttributeValueElement =
			theDocument.createElement("AttributeValue");
		anAttributeValueElement.setAttribute("attribute",attribId);
		anAttributeValueElement.setAttribute("status","added");
		while(theValuesOfTheAdded.hasNext())
		{
			Element aValue = theDocument.createElement("Value");

		    Text aValueText =
		        theDocument.createTextNode((String)theValuesOfTheAdded.next());
		    aValue.appendChild(aValueText);
			anAttributeValueElement.appendChild(aValue);
		}
		return anAttributeValueElement;
	}

	private Element detailUpdate(   String anID,
									String[] theUpdates)
	{
		Element aDetail = theDocument.createElement("Detail");
		aDetail.setAttribute("ID",anID);
		aDetail.setAttribute("status","changed");
		for(int i = 0; i < theUpdates.length; i++)
		{
			Element anUpdateValue = theDocument.createElement("Value");
			Text aUpdateValueText = theDocument.createTextNode(theUpdates[i]);
			aDetail.appendChild(anUpdateValue.appendChild(aUpdateValueText));
		}

		return aDetail;
	}

	private Element detailDelete(String id, String[] theValuesOfTheDeleted)
	{
		Element aDetail = theDocument.createElement("Detail");
		aDetail.setAttribute("ID",id);
		aDetail.setAttribute("status","deleted");
		for(int i = 0; i < theValuesOfTheDeleted.length; i++)
		{
			Element aValue = theDocument.createElement("Value");

		    Text aValueText =
		        theDocument.createTextNode(theValuesOfTheDeleted[i]);
		    aValue.appendChild(aValueText);
			aDetail.appendChild(aValue);
		}
		return aDetail;
	}

	private Element detailAdded(String id, String[] theAddedValues)
	{
		Element aDetailElement = theDocument.createElement("Detail");
		aDetailElement.setAttribute("ID",id);
		aDetailElement.setAttribute("status","added");
		for(int i = 0; i < theAddedValues.length; i++)
		{
		    Element aValue = theDocument.createElement("Value");
		    Text aValueText =
			    theDocument.createTextNode(theAddedValues[i]);
		    aValue.appendChild(aValueText);
		    aDetailElement.appendChild(aValue);
			}
		return aDetailElement;
	}
}
