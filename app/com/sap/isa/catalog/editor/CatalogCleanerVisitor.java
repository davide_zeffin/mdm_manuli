/*****************************************************************************
  Copyright (c) 2000, SAPMarkets Europe GmbH, Germany, All rights reserved.
  Created:      09 July 2001

  $Id: //sap/ESALES_base/30_SP_COR/src/_pcatAPI/java/com/sap/isa/catalog/editor/CatalogCleanerVisitor.java#2 $
  $Revision: #2 $
  $Change: 127411 $
  $DateTime: 2003/04/30 17:20:32 $
*****************************************************************************/

package com.sap.isa.catalog.editor;

// catalog imports
import java.util.ArrayList;
import java.util.Iterator;

import com.sap.isa.catalog.boi.CatalogException;
import com.sap.isa.catalog.boi.IComponent;
import com.sap.isa.catalog.boi.IQuantity;
import com.sap.isa.catalog.catalogvisitor.CatalogVisitor;
import com.sap.isa.catalog.impl.Catalog;
import com.sap.isa.catalog.impl.CatalogAttribute;
import com.sap.isa.catalog.impl.CatalogAttributeValue;
import com.sap.isa.catalog.impl.CatalogCategory;
import com.sap.isa.catalog.impl.CatalogDetail;
import com.sap.isa.catalog.impl.CatalogItem;
import com.sap.isa.catalog.impl.CatalogQuery;
import com.sap.isa.catalog.impl.CatalogQueryItem;
import com.sap.isa.catalog.impl.CatalogQueryStatement;
import com.sap.isa.core.logging.IsaLocation;

/**
 *  This visitor iterates over a given catalog tree and sets
 *  the state of the nodes back to "LOADED". All deleted nodes are removed
 *  from the collection. This visitor should be called after the XML document
 *  was generated and posted to the underlying content engine that describes
 *  the changes that were performed on the catalog.
 *  {@see CatalogXMLUpdateVisitor}
 *
 *  @version     1.0
 *  @since       2.0
 */
public class CatalogCleanerVisitor
    extends CatalogVisitor
{
    // the logging instance
    private static IsaLocation theStaticLocToLog =
        IsaLocation.getInstance(CatalogCleanerVisitor.class.getName());

    /**
     * Constructs a new visitor that cleans the catalog tree ie. the state
     * of all nodes is set back to "LOADED" and the deleted nodes are removed
     * from the memory.
     *
     * @param root the root node of tree
     */
    CatalogCleanerVisitor(EditableCatalog root)
    {
        super(root);
        theStaticLocToLog = IsaLocation.getInstance(this.getClass().getName());
    }

    /**
     * Starts the traversal of the tree.
     *
     * @throws CatalogException in case the commit fails for an
     */
    public void start() throws CatalogException
    {
        /**@todo make sure your user has the locks he needs!*/
        // start traversation
        getRoot().assign(this);
    }

    /**
     * Visit the catalog. Called from the start method. Visits the catalog and
     * traverses the tree depth first.
     * Every descendant is visited because it might be the case that e.g.
     * only an attribute value down the tree was changed and so the catalog
     * itself was not changed at all.
     *
     * @param category the editable Catalog to visit.
     */
    public void visitCatalog(Catalog catalog)
    {
        //synchronized with catalog/editor down the call stack
        //just to be obvious
        synchronized(catalog){
        try{//for throws from the get** catalog

        EditableCatalog theEditor = (EditableCatalog)catalog;
        Catalog theCatRef = theEditor.getCatalog();

        synchronized(theCatRef){

        if(theStaticLocToLog.isDebugEnabled())
        {
            theStaticLocToLog.debug("Visting Catalog: " + theEditor.getName());
        }

        //state New not possible
        //state Deleted not possible
        //state changed synch your state to the decorated reference
        if(theEditor.getState()==IComponent.State.CHANGED)
        {
            //put your state to the thing to edit
            String aNewName = theEditor.getName();
            String aOldName = theCatRef.getName();
            if(aNewName.intern()!=aOldName.intern())
            {
                theCatRef.setNameInternal(aNewName);
            }

            String aNewDesc = theEditor.getDescription();
            String aOldDesc = theCatRef.getDescription();
            if(aNewDesc.intern()!=aOldDesc.intern())
            {
                theCatRef.setDescriptionInternal(aNewDesc);
            }

            String aNewThumbNail = theEditor.getThumbNail();
            String aOldThumbNail = theCatRef.getDescription();
            if(aNewThumbNail.intern()!=aOldThumbNail.intern())
            {
                theCatRef.setThumbNailInternal(aNewThumbNail);
            }
        }

        // visit the editable details
        // is actually someting to do?
        boolean somethingToDo =
            theEditor.getCatalogBuilder().isDetailsBuilt(theEditor)
            || theEditor.getDetailsInternal().hasNext();
        //in case we never touched the rference but simply created new ones

        boolean askedReferenceForDetails =
            theEditor.getCatalogBuilder().isDetailsBuilt(theEditor);
        if(somethingToDo)
        {
            if(askedReferenceForDetails)
            {
                //So the cat ref has them also i.e. no call is triggered
                Iterator detailsIter = detailsIter = theCatRef.getDetails();

                ArrayList aListOfDetails = new ArrayList();
                while(detailsIter.hasNext())
                {
                    aListOfDetails.add(detailsIter.next());
                }

                //remove deleted details
                for(int i=0; i < aListOfDetails.size(); i++)
                {
                    CatalogDetail aDetailRef =
                        (CatalogDetail) aListOfDetails.get(i);

                    synchronized(aDetailRef){
                    //deleted
                    if(theEditor.getDetail(aDetailRef.getName())==null)
                    {
                        //kick off listeners for that component
                        aDetailRef.setStateDeleted();
                        //kickof listener for the catalog
                        theCatRef.deleteDetailInternal((CatalogDetail)aDetailRef);
                    }
                    }//end synchronized(aDetailRef)
                }
            }

            Iterator detailsIter = theEditor.getDetailsInternal();

            while(detailsIter.hasNext())
            {
                EditableDetail aDetail = (EditableDetail)detailsIter.next();
                CatalogDetail aDetailRef = aDetail.getDetail();

                synchronized(aDetail){

                if(aDetail.isNew())
                {
                    CatalogDetail aNewDetail =
                        new CatalogDetail(theCatRef);
                    //Set the state of the new Detail
                    aNewDetail.setNameInternal(aDetail.getName());
                    aNewDetail.setAsStringInternal(aDetail.getAllAsString());

                    theCatRef.addDetailInternal(aNewDetail,true);

                    aDetail.setDetail(aNewDetail);
                    aDetail.setState(IComponent.State.LOADED);
                }
                else if(!aDetail.isUnchanged())
                {
                    synchronized(aDetailRef){
                    //changed are handled by the detail self
                    aDetail.assign(this);
                    }
                }
                }//end synchronized(aDetail){
            }
        }//end visting details

        // visit the categories
        somethingToDo =
            theEditor.getCatalogBuilder().isCategoriesBuilt(theEditor) ||
            theEditor.getCatalogBuilder().isAllCategoriesBuilt(theEditor) ||
            theEditor.getChildCategoriesInternal().hasNext();

        boolean askedReferenceForCategories =
            theEditor.getCatalogBuilder().isCategoriesBuilt(theEditor) ||
            theEditor.getCatalogBuilder().isAllCategoriesBuilt(theEditor);

        if(somethingToDo)
        {
            if(askedReferenceForCategories)
            {
                //was allready asked no call is triggered
                Iterator categoriesIter =
                    categoriesIter = theCatRef.getChildCategories();

                ArrayList aListOfRootCategories = new ArrayList();

                while(categoriesIter.hasNext())
                {
                    aListOfRootCategories.add(categoriesIter.next());
                }

                //remove deleted categories from the ref
                for(int i=0; i < aListOfRootCategories.size(); i++)
                {
                    CatalogCategory aCategoryRef =
                        (CatalogCategory) aListOfRootCategories.get(i);

                    synchronized(aCategoryRef){

                    //deleted
                    Iterator anIter = null;
                    anIter = theEditor.getChildCategories();

                    boolean deleted = true;
                    while(deleted && anIter.hasNext())
                    {
                        EditableCategory aCate =
                            (EditableCategory) anIter.next();
                        if(aCate.getGuid().intern()==aCategoryRef.getGuid().intern())
                            deleted = false;
                    }
                    if(deleted)
                    {
                        //kick off listeners for that component
                        aCategoryRef.setStateDeleted();
                        //kickof listener for the catalog
                        theCatRef.deleteChildCategoryInternal(
                            (CatalogCategory)aCategoryRef);
                        //remove the link in the hashTable
                        theCatRef.removeCategory(aCategoryRef);
                    }
                    }//end synchronized(aCategoryRef)
                }
            }

            Iterator categoriesIter = theEditor.getChildCategoriesInternal();

            while(categoriesIter.hasNext())
            {
                EditableCategory aCate = (EditableCategory)categoriesIter.next();
                CatalogCategory aCateRef = aCate.getCategory();

                synchronized(aCate){

                if(aCate.isNew())
                {
                    CatalogCategory aNewCategory =
                        new CatalogCategory(theCatRef,aCate.getGuid());
                    aNewCategory.setNameInternal(aCate.getName());
                    aNewCategory.setDescriptionInternal(aCate.getDescription());
                    aNewCategory.setThumbNailInternal(aCate.getThumbNail());

                    theCatRef.addChildCategoryInternal(aNewCategory,true);
                    //the hashMap
                    theCatRef.addCategory(aNewCategory);

                    //Set the state of the new Detail
                    aCate.setCategory(aNewCategory);
                    aCate.setState(IComponent.State.LOADED);

                    //take care of the rest
                    //changed are handled by the category self
                    aCate.assign(this);
                }
                else
                {
                    synchronized(aCateRef){
                    //changed are handled by the category self
                    aCate.assign(this);
                    }
                }
                }//end synchronized(aCate){
            }
        }// end visit the categories


        // visit the edititable attributes
        somethingToDo =
            theEditor.getCatalogBuilder().isAttributesBuilt(theEditor) ||
            theEditor.getAttributesInternal().hasNext();

        boolean askedReferenceForAttributes =
            theEditor.getCatalogBuilder().isAttributesBuilt(theEditor);

        if(somethingToDo)
        {
            if(askedReferenceForAttributes)
            {
                //no call triggered we asked allready
                Iterator aAttributeIterator = theCatRef.getAttributes();

                ArrayList aListOfAttributes = new ArrayList();

                while(aAttributeIterator.hasNext())
                {
                    aListOfAttributes.add(aAttributeIterator.next());
                }

                for(int i = 0; i < aListOfAttributes.size(); i++)
                {
                    CatalogAttribute aAttributeRef =
                        (CatalogAttribute) aListOfAttributes.get(i);
                    synchronized(aAttributeRef){
                    //deleted
                    Iterator theAttribIter = theEditor.getAttributes();
                    boolean deleted = true;
                    while(deleted && theAttribIter.hasNext())
                    {
                        EditableAttribute aEditableAttribute =
                            (EditableAttribute) theAttribIter.next();
                        if(aEditableAttribute.getGuid().equals(aAttributeRef.getGuid()))
                            deleted=false;
                    }
                    if(deleted)
                    {
                        aAttributeRef.setStateDeleted();
                        theCatRef.deleteAttributeInternal(aAttributeRef);
                    }
                    }//end synchronized(aAttributeRef)
                }
            }

            Iterator aAttributeIterator = theEditor.getAttributesInternal();

            while(aAttributeIterator.hasNext())
            {
                EditableAttribute aEditableAttribute = (EditableAttribute)
                    aAttributeIterator.next();
                CatalogAttribute aAttributeRef =
                    aEditableAttribute.getAttribute();
                synchronized(aEditableAttribute){
                if(aEditableAttribute.isNew())
                {
                    IQuantity aQuantity = aEditableAttribute.getQuantity();

                    CatalogAttribute aNewAttribute =
                        new CatalogAttribute(   theCatRef,
                                                aEditableAttribute.getGuid(),
                                                aEditableAttribute.getType(),
                                                aQuantity);

                    aNewAttribute.setNameInternal(aEditableAttribute.getName());
                    aNewAttribute.setDescriptionInternal(
                        aEditableAttribute.getDescription());

                    //finally add the parent child relation
                    theCatRef.addAttributeInternal(aNewAttribute,true);

                    aEditableAttribute.setAttribute(aNewAttribute);
                    aEditableAttribute.setState(IComponent.State.LOADED);

                    //take care of the rest i.e. details
                    aEditableAttribute.assign(this);
                }
                else
                {
                    synchronized(aAttributeRef){
                    //changed is handled by the attribute itself
                    aEditableAttribute.assign(this);
                    }
                }//end synchronized(aEditableAttribute)
                }
            }
        }//end visting attributes

        //We cleaned it up
        theEditor.setState(IComponent.State.LOADED);

        }//end synchronized(theCatRef)
        }catch(CatalogException ce)
        {
            return;
        }
        }//synchronized(catalog)
    }

    /**
     * Visit a category. Called from the corresponding parent category.
     * Every descendant,  is visited because it might be the case that e.g.
     * only an attribute value down the tree was changed and so the category
     * was not changed at all.
     *
     * @param category the editable category to visit.
     */
    public void visitCategory(CatalogCategory category)
    {
        //synchronized with category down the call stack
        //just to be obvious
        synchronized(category){
        try{

        if(theStaticLocToLog.isDebugEnabled())
        {
            theStaticLocToLog.debug("Visting Category: " + category.getName());
        }

        //Update internal state
        EditableCategory theCateEditor = (EditableCategory)category;
        EditableCatalog theEditor =
            (EditableCatalog) theCateEditor.getRoot();
        Catalog theCataRef = theEditor.getCatalog();
        CatalogCategory theCateRef = theCateEditor.getCategory();

        if(theCateEditor.isChanged())
        {
            //put your state to the thing to edit
            String aNewName = theCateEditor.getName();
            String aOldName = theCateRef.getName();
            if( (aNewName!=null) &&
                ( aOldName==null || aNewName.intern()!=aOldName.intern()))
            {
                theCateRef.setNameInternal(aNewName);
            }

            String aNewDesc = theCateEditor.getDescription();
            String aOldDesc = theCateRef.getDescription();
            if( (aNewDesc != null) &&
                (aOldDesc == null || aNewDesc.intern()!=aOldDesc.intern()))
            {
                theCateRef.setDescriptionInternal(aNewDesc);
            }

            String aNewThumbNail = theCateEditor.getThumbNail();
            String aOldThumbNail = theCateRef.getDescription();
            if( (aNewThumbNail!=null) &&
                (aOldThumbNail==null || aNewThumbNail.intern()!=aOldThumbNail.intern()))
            {
                theCateRef.setThumbNailInternal(aNewThumbNail);
            }
        }

        // visit the edititable attributes
        boolean somethingToDo =
            theEditor.getCatalogBuilder().isAttributesBuilt(theCateEditor) ||
            theCateEditor.getAttributesInternal().hasNext();

        boolean askedReferenceForAttributes =
            theEditor.getCatalogBuilder().isAttributesBuilt(theCateEditor);

        if(somethingToDo)
        {
            if(askedReferenceForAttributes)
            {
                //no call triggered we asked allready
                Iterator aAttributeIterator = theCateRef.getAttributes();

                ArrayList aListOfAttributes = new ArrayList();

                while(aAttributeIterator.hasNext())
                {
                    aListOfAttributes.add(aAttributeIterator.next());
                }

                for(int i = 0; i < aListOfAttributes.size(); i++)
                {
                    CatalogAttribute aAttributeRef =
                        (CatalogAttribute) aListOfAttributes.get(i);
                    synchronized(aAttributeRef){
                    //deleted
                    Iterator theAttribIter = theCateEditor.getAttributes();
                    boolean deleted = true;
                    while(deleted && theAttribIter.hasNext())
                    {
                        EditableAttribute aEditableAttribute =
                            (EditableAttribute) theAttribIter.next();
                        if(aEditableAttribute.getGuid().equals(aAttributeRef.getGuid()))
                            deleted=false;
                    }
                    if(deleted)
                    {
                        aAttributeRef.setStateDeleted();
                        theCateRef.deleteAttributeInternal(aAttributeRef);
                    }
                    }//end synchronized(aAttributeRef)
                }
            }

            Iterator aAttributeIterator = theCateEditor.getAttributesInternal();

            while(aAttributeIterator.hasNext())
            {
                EditableAttribute aEditableAttribute = (EditableAttribute)
                    aAttributeIterator.next();
                CatalogAttribute aAttributeRef =
                    aEditableAttribute.getAttribute();
                synchronized(aEditableAttribute){
                if(aEditableAttribute.isNew())
                {
                    IQuantity aQuantity = aEditableAttribute.getQuantity();

                    CatalogAttribute aNewAttribute =
                        new CatalogAttribute(   theCateRef,
                                                aEditableAttribute.getGuid(),
                                                aEditableAttribute.getType(),
                                                aQuantity);

                    aNewAttribute.setNameInternal(aEditableAttribute.getName());
                    aNewAttribute.setDescriptionInternal(
                        aEditableAttribute.getDescription());

                    //finally add the parent child relation
                    theCateRef.addAttributeInternal(aNewAttribute,true);

                    aEditableAttribute.setAttribute(aNewAttribute);
                    aEditableAttribute.setState(IComponent.State.LOADED);

                    //take care of the rest i.e. details
                    aEditableAttribute.assign(this);
                }
                else
                {
                    synchronized(aAttributeRef){
                    //changed is handled by the attribute itself
                    aEditableAttribute.assign(this);
                    }
                }//end synchronized(aEditableAttribute)
                }
            }
        }//end visting attributes

        // start vist categories
        somethingToDo =
            theEditor.getCatalogBuilder().isCategoriesBuilt(theCateEditor)
            || theEditor.getCatalogBuilder().isAllCategoriesBuilt(theEditor)
            || theCateEditor.getChildCategoriesInternal().hasNext();

        boolean askedReferenceForChildren =
            theEditor.getCatalogBuilder().isCategoriesBuilt(theCateEditor)
            || theEditor.getCatalogBuilder().isAllCategoriesBuilt(theEditor);

        if(somethingToDo)
        {
            if(askedReferenceForChildren)//then we might have deleted something
            {
                Iterator categoriesIter =
                    theCateRef.getChildCategories();

                ArrayList aListOfChildCategories = new ArrayList();

                while(categoriesIter.hasNext())
                {
                    aListOfChildCategories.add(categoriesIter.next());
                }

                //remove deleted categories from the ref
                for(int i=0; i < aListOfChildCategories.size(); i++)
                {
                    CatalogCategory aCategoryRef =
                        (CatalogCategory) aListOfChildCategories.get(i);

                    synchronized(aCategoryRef){
                    //deleted
                    Iterator anIter = theCateEditor.getChildCategories();
                    boolean deleted = true;
                    while(deleted && anIter.hasNext())
                    {
                        EditableCategory aCate =
                            (EditableCategory) anIter.next();
                        if(aCate.getGuid().intern()==aCategoryRef.getGuid().intern())
                            deleted = false;
                    }
                    if(deleted)
                    {
                        //kick off listeners for that component
                        aCategoryRef.setStateDeleted();
                        //kickof listener for the catalog
                        theCateRef.deleteChildCategoryInternal(
                            (CatalogCategory)aCategoryRef);
                        //remove the link in the hashTable
                        theCataRef.removeCategory(aCategoryRef);
                    }
                    }//end synchronized(aCategoryRef)
                }
            }

            Iterator categoriesIter =
                theCateEditor.getChildCategoriesInternal();

            while(categoriesIter.hasNext())
            {
                EditableCategory aCate =
                    (EditableCategory)categoriesIter.next();
                CatalogCategory aCateRef =
                    aCate.getCategory();

                synchronized(aCate){
                if(aCate.isNew())
                {
                    CatalogCategory aNewCategory =
                        new CatalogCategory(theCateRef,aCate.getGuid());
                    aNewCategory.setNameInternal(aCate.getName());
                    aNewCategory.setDescriptionInternal(aCate.getDescription());
                    aNewCategory.setThumbNailInternal(aCate.getThumbNail());

                    theCateRef.addChildCategoryInternal(aNewCategory,true);
                    //the hashMap
                    theCateRef.getRoot().addCategory(aNewCategory);

                    //Set the state of the new Detail
                    aCate.setCategory(aNewCategory);
                    aCate.setState(IComponent.State.LOADED);

                    //take care of the rest
                    //changed are handled by the category self
                    aCate.assign(this);
                }
                else
                {
                    synchronized(aCateRef){
                    //changed are handled by the category self
                    aCate.assign(this);
                    }
                }

                }//end synchronized(aCate){
            }
        }
        //end vist categories

        // visit the editable details
        somethingToDo =
            theEditor.getCatalogBuilder().isDetailsBuilt(theCateEditor)
            || theCateEditor.getDetailsInternal().hasNext();

        boolean askedReferenceForDetails =
            theEditor.getCatalogBuilder().isDetailsBuilt(theCateEditor);

        if(somethingToDo)
        {
            if(askedReferenceForDetails)//so check if something was deleted
            {
                //So the cat ref has them also i.e. no call is triggered
                Iterator detailsIter = theCateRef.getDetails();

                ArrayList aListOfDetails = new ArrayList();
                while(detailsIter.hasNext())
                {
                    aListOfDetails.add(detailsIter.next());
                }

                //remove deleted details
                for(int i=0; i < aListOfDetails.size(); i++)
                {
                    CatalogDetail aDetailRef =
                        (CatalogDetail) aListOfDetails.get(i);

                    synchronized(aDetailRef){
                    //deleted
                    if(theCateEditor.getDetail(aDetailRef.getName())==null)
                    {
                        //kick off listeners for that component
                        aDetailRef.setStateDeleted();
                        //kickof listener for the catalog
                        theCateRef.deleteDetailInternal(
                            (CatalogDetail)aDetailRef);
                    }
                    }//end synchronized(aDetailRef)
                }
            }

            Iterator detailsIter = theCateEditor.getDetailsInternal();

            while(detailsIter.hasNext())
            {
                EditableDetail aDetail = (EditableDetail)detailsIter.next();
                CatalogDetail aDetailRef = aDetail.getDetail();

                synchronized(aDetail){
                if(aDetail.isNew())
                {
                    CatalogDetail aNewDetail =
                        new CatalogDetail(theCateRef);
                    //Set the state of the new Detail
                    aNewDetail.setNameInternal(aDetail.getName());
                    aNewDetail.setAsStringInternal(aDetail.getAllAsString());

                    theCateRef.addDetailInternal(aNewDetail,true);

                    aDetail.setDetail(aNewDetail);
                    aDetail.setState(IComponent.State.LOADED);
                }
                else if(!aDetail.isUnchanged())
                {
                    synchronized(aDetailRef){
                    //changed are handled by the detail self
                    aDetail.assign(this);
                    }
                }
                }//end synchronized(aDetail)
            }
        }//end visting details


        // visit the editable items
        somethingToDo =
            theEditor.getCatalogBuilder().isItemsBuilt(theCateEditor)
            || theCateEditor.getItemsInternal().hasNext();

        boolean askedReferenceForItems =
            theEditor.getCatalogBuilder().isItemsBuilt(theCateEditor);

        if(somethingToDo)
        {
            if(askedReferenceForItems)
            {
                //So the cat ref has them also i.e. no call is triggered
                Iterator itemsIter = theCateRef.getItems();

                ArrayList aListOfItems = new ArrayList();
                while(itemsIter.hasNext())
                {
                    aListOfItems.add(itemsIter.next());
                }

                //remove deleted items
                for(int i=0; i < aListOfItems.size(); i++)
                {
                    CatalogItem aItemRef =
                        (CatalogItem) aListOfItems.get(i);

                    synchronized(aItemRef){
                    //deleted?
                    Iterator anIter = theCateEditor.getItems();
                    boolean deleted = true;
                    while(deleted && anIter.hasNext())
                    {
                        EditableItem aItem =
                            (EditableItem) anIter.next();
                        if(aItem.getGuid().intern()==aItemRef.getGuid().intern())
                            deleted = false;
                    }
                    if(deleted)
                    {
                        //kick off listeners for that component
                        aItemRef.setStateDeleted();
                        //kickof listener for the category
                        theCateRef.deleteItemInternal((CatalogItem)aItemRef);
                    }
                    }//end synchronized(aItemRef)
                }
            }

            // now visit what is left or new
            Iterator itemsIter = theCateEditor.getItemsInternal();

            while(itemsIter.hasNext())
            {
                EditableItem aItem = (EditableItem)itemsIter.next();
                CatalogItem aItemRef = aItem.getItem();

                synchronized(aItem){
                if(aItem.isNew())
                {
                    CatalogItem aNewItem =
                        new CatalogItem(theCateRef,aItem.getGuid());
                    //Set the state of the new Item
                    aNewItem.setNameInternal(aItem.getName());
                    aNewItem.setProductGuidInternal(aItem.getProductGuid());

                    //Add it to its parent
                    theCateRef.addItemInternal(aNewItem,true);

                    //Add it to its editor
                    aItem.setItem(aNewItem);
                    aItem.setState(IComponent.State.LOADED);
                    //now drill down
                    aItem.assign(this);
                }
                else
                {
                    synchronized(aItemRef){
                    //changed are handled by the detail self
                    aItem.assign(this);
                    }
                }

                }//end synchronized(aItem){
            }

        }//end visting items
        category.setState(IComponent.State.LOADED);
        }catch(CatalogException ce)
        {
            return;
        }
        }//end synchronized(category)

        return;
    }

    /**
     * Visit a item. Called from the corresponding category.
     * Every item is visited because it might be the case that e.g.
     * only an attribute value was changed and so the item was not
     * changed at all.
     *
     * @param item the editable item to visit.
     */
    public void visitItem(CatalogItem item)
    {
        //synchronized with item down the call stack
        //just to be obvious
        synchronized(item){
        try{
        if(theStaticLocToLog.isDebugEnabled())
        {
            theStaticLocToLog.debug("Visting Item: " + item.getName());
        }

        EditableItem theItemEditor = (EditableItem)item;
        EditableCatalog theEditor =
            (EditableCatalog) theItemEditor.getRoot();
        CatalogItem theItemRef = theItemEditor.getItem();

        synchronized(theItemRef){
        if(theItemEditor.isChanged())
        {
            if(theStaticLocToLog.isDebugEnabled())
            {
                theStaticLocToLog.debug("Updating Item: " + item.getName());
            }

            //put your state to the thing to edit
            String aNewName = theItemEditor.getName();
            String aOldName = theItemRef.getName();
            if(aNewName.intern()!=aOldName.intern())
            {
                theItemRef.setNameInternal(aNewName);
            }

            String aNewProductGuid = theItemEditor.getProductGuid();
            String aOldProductGuid = theItemRef.getProductGuid();
            if(aNewProductGuid.intern()!=aOldProductGuid.intern())
            {
                theItemRef.setProductGuidInternal(aNewProductGuid);
            }
        }

        // visit the editable details
        boolean somethingToDo =
            theEditor.getCatalogBuilder().isDetailsBuilt(theItemEditor)
            || theItemEditor.getDetailsInternal().hasNext();

        boolean askedReferenceForDetails =
            theEditor.getCatalogBuilder().isDetailsBuilt(theItemEditor);

        if(somethingToDo)
        {
            if(askedReferenceForDetails)//so check if something was deleted
            {
                //So the cat ref has them also i.e. no call is triggered
                Iterator detailsIter = theItemRef.getDetails();

                ArrayList aListOfDetails = new ArrayList();
                while(detailsIter.hasNext())
                {
                    aListOfDetails.add(detailsIter.next());
                }

                //remove deleted details
                for(int i=0; i < aListOfDetails.size(); i++)
                {
                    CatalogDetail aDetailRef =
                        (CatalogDetail) aListOfDetails.get(i);

                    synchronized(aDetailRef){
                    //deleted
                    if(theItemEditor.getDetail(aDetailRef.getName())==null)
                    {
                        //kick off listeners for that component
                        aDetailRef.setStateDeleted();
                        //kickof listener for the catalog
                        theItemRef.deleteDetailInternal(
                            (CatalogDetail)aDetailRef);
                    }
                    }//end synchronized(aDetailRef)
                }
            }

            Iterator detailsIter = theItemEditor.getDetailsInternal();

            while(detailsIter.hasNext())
            {
                EditableDetail aDetail = (EditableDetail)detailsIter.next();
                CatalogDetail aDetailRef = aDetail.getDetail();

                synchronized(aDetail){
                if(aDetail.isNew())
                {
                    CatalogDetail aNewDetail =
                        new CatalogDetail(theItemRef);

                    //Set the state of the new Detail
                    aNewDetail.setNameInternal(aDetail.getName());
                    aNewDetail.setAsStringInternal(aDetail.getAllAsString());

                    theItemRef.addDetailInternal(aNewDetail,true);

                    aDetail.setDetail(aNewDetail);
                    aDetail.setState(IComponent.State.LOADED);
                }
                else if(!aDetail.isUnchanged())
                {
                    synchronized(aDetailRef){
                    //changed are handled by the detail self
                    aDetail.assign(this);
                    }
                }
                }//end synchronized(aDetail){
            }
        }//end visting details

        // visit AttributeValues
        // logic different than for the rest because attribute values are always
        // builded for items.
        // i.e. if you got the item (thats teh reason you are here)
        // you got the values from the ref and the
        // ref got the values in any case already from the server,
        // so just remove deleted values, add new ones and clean up
        // changed values.
        somethingToDo =
            theItemEditor.getAttributeValues().hasNext() ||
            theItemRef.getAttributeValues().hasNext();
        if(somethingToDo)
        {
            Iterator aValuesIter = theItemRef.getAttributeValues();

            ArrayList aListOfValues = new ArrayList();
            while(aValuesIter.hasNext())
            {
                aListOfValues.add(aValuesIter.next());
            }

            //remove deleted values
            for(int i=0; i < aListOfValues.size(); i++)
            {
                CatalogAttributeValue aValueRef =
                    (CatalogAttributeValue) aListOfValues.get(i);

                synchronized(aValueRef){
                //deleted
                if(theItemEditor.getAttributeValue(
                    aValueRef.getAttributeGuid())==null)
                {
                    //kick off listeners for that component
                    aValueRef.setStateDeleted();
                    //kickof listener for the Item
                    theItemRef.deleteAttributeValueInternal(
                        (CatalogAttributeValue)aValueRef);
                }
                }//end synchronized(aValueRef)
            }

            aValuesIter = theItemEditor.getAttributeValues();

            while(aValuesIter.hasNext())
            {
                EditableAttributeValue aValue =
                    (EditableAttributeValue)aValuesIter.next();
                CatalogAttributeValue aValueRef = aValue.getAttributeValue();

                synchronized(aValue){
                if(aValue.isNew())
                {
                    //The attributes must have been visted!
                    CatalogAttribute theAttributeRef =
                        ((EditableAttribute) aValue.getAttribute()).getAttribute();

                    CatalogAttributeValue aNewValue =
                        new CatalogAttributeValue(theItemRef,theAttributeRef);

                    Iterator anIter = aValue.getAllValues();
                    while(anIter.hasNext())
                    {
                        aNewValue.addAsObjectInternal(anIter.next());
                    }

                    theItemRef.addAttributeValueInternal(aNewValue,true);

                    aValue.setAttributeValue(aNewValue);
                    aValue.setState(IComponent.State.LOADED);
                }
                else if(!aValue.isUnchanged())
                {
                    synchronized(aValueRef){
                    //changed are handled by the detail self
                    aValue.assign(this);
                    }
                }

                }//end synchronized(aDetail){
            }
        }//end visting attribute values

        }//synchronized(theItemRef)
        item.setState(IComponent.State.LOADED);
        }catch(CatalogException ce)
        {
            return;
        }
        }//synchronized(item)
    }

    public void visitQueryItem(CatalogQueryItem item)
    {
        //nothing to do
    }

    /**
     * Visit a changed detail. Called from the coressponding parent component.
     *
     * @param detail the changed detail that needs a clean up.
     */
    public void visitDetail(CatalogDetail detail)
    {
        EditableDetail aEditableDetail = (EditableDetail)detail;
        synchronized(aEditableDetail){
        CatalogDetail aDetailRef =  aEditableDetail.getDetail();
        if(theStaticLocToLog.isDebugEnabled())
        {
            theStaticLocToLog.debug("Visting Detail: " +
                aEditableDetail.getName());
        }
        synchronized(aDetailRef){
        //state changed
        if(aEditableDetail.isChanged())
        {
            String aNewName = aEditableDetail.getName();
            String aOldName = aDetailRef.getName();

            if(aNewName.intern()!=aOldName.intern())
            {
                aDetailRef.setNameInternal(aNewName);
                aDetailRef.setState(IComponent.State.LOADED);
            }

            String[] newValues = aEditableDetail.getAllAsString();
            String[] oldValues = aDetailRef.getAllAsString();
            //values are ordered alphabetically
            //if they ALL the same we got nothing to do
            boolean allTheSame = true;
            if(newValues.length!=oldValues.length)
                allTheSame=false;

            for(int i=0; allTheSame && i < newValues.length; i++)
            {
                String aNewValue = newValues[i];
                String anOldValue = oldValues[i];
                if( aNewValue != null &&
                    (anOldValue==null || aNewValue.intern()!=anOldValue.intern()))
                {
                    allTheSame=false;
                }
            }

            if(!allTheSame)
            {
                aDetailRef.setAsStringInternal(newValues);
                //Inform the listeners
                aDetailRef.setState(IComponent.State.LOADED);
            }

        }
        }//end synchronized(aDetailRef)
        aEditableDetail.setState(IComponent.State.LOADED);
        }//synchronized(aEditableDetail)
    }

    /**
     * Visit an Attribute. Called from the corresponding parent composite.
     * Every descendant,  is visited because it might be the case that e.g.
     * only an detail value down the tree was changed and so the attribute
     * was not changed at all.
     *
     * @param category the editable category to visit.
     */
    public void visitAttribute(CatalogAttribute attribute)
    {
        //synchronized with attribute/editor down the call stack
        //just to be obvious
        synchronized(attribute){
        try{//for throws from the get** catalog

        EditableAttribute theAttributeEditor = (EditableAttribute)attribute;
        CatalogAttribute theAttributeRef = theAttributeEditor.getAttribute();
        EditableCatalog theEditor =
            (EditableCatalog)theAttributeEditor.getRoot();
        synchronized(theAttributeRef){

        if(theStaticLocToLog.isDebugEnabled())
        {
            theStaticLocToLog.debug("Visting Attribute: " + theAttributeEditor.getName());
        }

        //state changed synch your state to the decorated reference
        if(theAttributeEditor.isChanged())
        {
            //put your state to the thing to edit
            String aNewName = theAttributeEditor.getName();
            String aOldName = theAttributeRef.getName();

            if((aOldName==null && aNewName!=null) ||
                ((aOldName!=null && aNewName!=null) &&
                aNewName.intern()!=aOldName.intern()))
            {
                theAttributeRef.setNameInternal(aNewName);
            }

            String aNewDesc = theAttributeEditor.getDescription();
            String aOldDesc = theAttributeRef.getDescription();
            if((aOldDesc==null && aNewDesc!=null) ||
                ((aOldDesc!=null && aNewDesc!=null) &&
                aNewDesc.intern()!=aOldDesc.intern()))

            {
                theAttributeRef.setDescriptionInternal(aNewDesc);
            }
            /**@todo add the rest of the internal state*/
        }

        // visit the editable details
        // is actually someting to do?
        boolean somethingToDo =
            theEditor.getCatalogBuilder().isDetailsBuilt(theAttributeEditor)
            || theAttributeEditor.getDetailsInternal().hasNext();
        //in case we never touched the rference but simply created new ones

        boolean askedReferenceForDetails =
            theEditor.getCatalogBuilder().isDetailsBuilt(theAttributeEditor);
        if(somethingToDo)
        {
            if(askedReferenceForDetails)
            {
                //So the cat ref has them also i.e. no call is triggered
                Iterator detailsIter = theAttributeRef.getDetails();

                ArrayList aListOfDetails = new ArrayList();
                while(detailsIter.hasNext())
                {
                    aListOfDetails.add(detailsIter.next());
                }

                //remove deleted details
                for(int i=0; i < aListOfDetails.size(); i++)
                {
                    CatalogDetail aDetailRef =
                        (CatalogDetail) aListOfDetails.get(i);

                    synchronized(aDetailRef){
                    //deleted
                    if(theAttributeEditor.getDetail(aDetailRef.getName())==null)
                    {
                        //kick off listeners for that component
                        aDetailRef.setStateDeleted();
                        //kickof listener for the catalog
                        theAttributeRef.deleteDetailInternal((CatalogDetail)aDetailRef);
                    }
                    }//end synchronized(aDetailRef)
                }
            }

            Iterator detailsIter = theAttributeEditor.getDetailsInternal();

            while(detailsIter.hasNext())
            {
                EditableDetail aDetail = (EditableDetail)detailsIter.next();
                CatalogDetail aDetailRef = aDetail.getDetail();

                synchronized(aDetail){

                if(aDetail.isNew())
                {
                    CatalogDetail aNewDetail =
                        new CatalogDetail(theAttributeRef);
                    aNewDetail.setNameInternal(aDetail.getName());
                    aNewDetail.setAsStringInternal(aDetail.getAllAsString());

                    theAttributeRef.addDetailInternal(aNewDetail,true);

                    //Set the state of the new Detail
                    aDetail.setDetail(aNewDetail);
                    aDetail.setState(IComponent.State.LOADED);
                }
                else if(!aDetail.isUnchanged())
                {
                    synchronized(aDetailRef){
                    //changed are handled by the detail self
                    aDetail.assign(this);
                    }
                }
                }//end synchronized(aDetail){
            }
        }//end visting details

        //We cleaned it up
        theAttributeEditor.setState(IComponent.State.LOADED);

        }//end synchronized(theAttributeRef)
        }catch(CatalogException ce)
        {
            return;
        }
        }//synchronized(attribute)
    }

    /**
     * Visit a changed value. Called from the coressponding item parent.
     *
     * @param attrValue the changed value that needs a clean up.
     */
    public void visitAttributeValue(CatalogAttributeValue attrValue)
    {
        //synchronized with attrValue down the call stack
        //just to be obvious
        //only changed values are visited from the corressponding item!
        synchronized(attrValue){
        if(theStaticLocToLog.isDebugEnabled())
        {
            theStaticLocToLog.debug("Visting Value for Attribute: " +
                attrValue.getAttributeName());
        }

        EditableAttributeValue aValue = (EditableAttributeValue)attrValue;
        CatalogAttributeValue aValueRef = aValue.getAttributeValue();

        synchronized(aValueRef){
        Iterator anIter = aValue.getAllValues();

        if(theStaticLocToLog.isDebugEnabled())
        {
            theStaticLocToLog.debug("Updating Value from : " +
                aValueRef.getAsString() + " to: " + aValue.getAsString());
        }

        boolean first = true;
        while(anIter.hasNext())
        {
            if(first)
            {
                aValueRef.setAsObjectInternal(anIter.next());
                first = false;
            }
            else
                aValueRef.addAsObjectInternal(anIter.next());
        }
        }//end synchronized(aValueRef)
        attrValue.setState(IComponent.State.LOADED);
        }//end synchronized(attrValue)
    }

    public void visitQueryStatement(CatalogQueryStatement statement)
    {
        // here is nothing to do!!
    }

    public void visitQuery(CatalogQuery query)
    {
        // nothing to do
    }
}
