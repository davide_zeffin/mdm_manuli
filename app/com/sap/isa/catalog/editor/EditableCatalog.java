/*****************************************************************************
  Copyright (c) 2000-2001, SAPMarkets Europe GmbH, Germany, All rights reserved.
  Created:      02 August 2001

  $Id: //sap/ESALES_base/30_SP_COR/src/_pcatAPI/java/com/sap/isa/catalog/editor/EditableCatalog.java#2 $
  $Revision: #2 $
  $Change: 127411 $
  $DateTime: 2003/04/30 17:20:32 $
*****************************************************************************/
package com.sap.isa.catalog.editor;

import java.io.FileWriter;
import java.io.IOException;
import java.io.StringReader;
import java.util.Date;
import java.util.Iterator;

import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;

import org.w3c.dom.Document;

import com.sap.isa.catalog.ICatalogMessagesKeys;
import com.sap.isa.catalog.boi.CatalogException;
import com.sap.isa.catalog.boi.IActor;
import com.sap.isa.catalog.boi.IAttributeCreatedEvent;
import com.sap.isa.catalog.boi.IAttributeDeletedEvent;
import com.sap.isa.catalog.boi.ICatalog;
import com.sap.isa.catalog.boi.IChildCreatedEvent;
import com.sap.isa.catalog.boi.IChildDeletedEvent;
import com.sap.isa.catalog.boi.IComponent;
import com.sap.isa.catalog.boi.IComponentChangedEvent;
import com.sap.isa.catalog.boi.IComponentEventListener;
import com.sap.isa.catalog.boi.ICompositeEventListener;
import com.sap.isa.catalog.boi.IDetailCreatedEvent;
import com.sap.isa.catalog.boi.IDetailDeletedEvent;
import com.sap.isa.catalog.boi.IEditableAttribute;
import com.sap.isa.catalog.boi.IEditableCatalog;
import com.sap.isa.catalog.boi.IEditableCategory;
import com.sap.isa.catalog.boi.IEditableDetail;
import com.sap.isa.catalog.catalogcmd.CatalogCmd;
import com.sap.isa.catalog.catalogcmd.CatalogCmdProcessor;
import com.sap.isa.catalog.impl.Catalog;
import com.sap.isa.catalog.impl.CatalogDetail;
import com.sap.isa.catalog.impl.CatalogLockManager;
import com.sap.isa.core.logging.IsaLocation;

/**
 *  The editor for a catalog. You get an instance of this from a catalog.
 *  It decorates the instance to make it writable. The similar decorator
 *  classes exist for all other components.
 *
 *  @version     1.0
 */
public class EditableCatalog
  extends Catalog
  implements  IEditableCatalog,
              IComponentEventListener,
              ICompositeEventListener
{
  /*
   *  EditableCatalog implements Serializable:
   *  Fields:
   *  theCatToEdit        :   implements Serializable
   *  theUser             :   better should ;-)
   *  theCmdProcessor     :   transient, do undo histories are not persisted
   */
  private Catalog                           theCatToEdit;
  private IActor                            theUser;
  transient private CatalogCmdProcessor   theCmdProcessor;
    // the logging instance
  private static IsaLocation theStaticLocToLog =
    IsaLocation.getInstance(EditableCatalog.class.getName());

  public EditableCatalog(Catalog aCatToEdit, IActor anUser)
  {
      super(aCatToEdit.getServerEngine(),aCatToEdit.getGuid());
    setCatalogBuilder(new EditableCatalogBuilder(this));
    this.theCatToEdit=aCatToEdit;
    this.theMessageResources=aCatToEdit.getMessageResources();

    //listen to your reference
    theCatToEdit.addComponentEventListener(this);
    theCatToEdit.addCompositeEventListener(this);

    this.theUser=anUser;
    setState(IComponent.State.LOADED);

    //pull your state for the thing to edit
    this.setNameInternal(theCatToEdit.getName());
    this.setDescriptionInternal(theCatToEdit.getDescription());
    this.setThumbNailInternal(theCatToEdit.getThumbNail());
  }

  /**
   * Returns this in case of the same user otherwise an editor for the
   * new user.
   *
   * @return  an editor for the user
   */
  public IEditableCatalog getEditor(IActor anUser)
  {
    return theCatToEdit.getEditor(anUser);
  }

  /**
   * The catalog we are going to edit!
   */
  Catalog getCatalog()
  {
    return theCatToEdit;
  }

  /**
   * Factory method for creating a detail instance. Internal method which
   * can't be called via the public interface.
   *
   * @param name the name of the new detail instance
   * @return a instance of type <code>EditableDetail</code>
   */
  synchronized public EditableDetail createEditableDetailInternal(String name)
  {
    EditableDetail aDetail = new EditableDetail(this, name);
    addDetailInternal(aDetail,true);
    return aDetail;
  }

  /**
   * Creates a new attribute. The composite has an association with the instance.
   * Internal method which can't be called via the public interface.
   *
   * @param guid the guid of the attribute
   * @return a instance of type <code>EditableAttribute</code>
   */
  synchronized public EditableAttribute createEditableAttributeInternal(
                                                                        String guid)
  {
    EditableAttribute attribute = new EditableAttribute(this, guid);
    addAttributeInternal(attribute,true);
    return attribute;
  }

  /**
   * Factory method for creating a category instance. Internal method which
   * can't be called via the public interface.
   *
   * @param aCategoryGuid the guid of the category to be created
   * @return a instance of type <code>EditableCategory</code>
   */
  synchronized public EditableCategory createEditableCategoryInternal(
                                                                      String aCategoryGuid)
  {
    EditableCategory child =
      (EditableCategory)getRoot().getCategoryInternal(aCategoryGuid);
    if(child == null)
      {
        child = new EditableCategory(this, aCategoryGuid);
        getRoot().addCategory(child);
      }
    addChildCategoryInternal(child,true);

    return child;
  }

  synchronized CatalogCmdProcessor getCmdProcessor()
  {
    if(theCmdProcessor == null)
      theCmdProcessor = new CatalogCmdProcessor(5);

    return theCmdProcessor;
  }

  /**
   * Write the changes in an xml format for logging and debugging purposes.
   *
   * @return aDocument    a document in an intermediate format (Catalog.dtd)
   *                      that describes the changes
   */
  private Document backUpChangesLocal()
    throws CatalogException
  {
    CatalogXMLUpdateVisitor anXMLUpdatVistor =
      new CatalogXMLUpdateVisitor(this);

    Document theBackUp = anXMLUpdatVistor.start();

    String aPathToTheTempDir = theCatToEdit.getServerEngine().getSite().getEnvironment().getRealPath("/temp");

    //Write the XML
    StringBuffer aBuffer = new StringBuffer(aPathToTheTempDir);
    aBuffer.append("CatalogChanges_");
    aBuffer.append(this.getUser().getName()).append('_');
    aBuffer.append((new Date()).getTime());
    aBuffer.append(".xml");

    try
      {
        FileWriter aFileWriter = new FileWriter(aBuffer.toString());

        TransformerFactory tFactory = TransformerFactory.newInstance();

        //build your simple copyAll xsl
        StringBuffer anotherBuffer = new StringBuffer("<?xml version=\"1.0\"?>");
        anotherBuffer.append("<xsl:stylesheet xmlns:xsl=\"http://www.w3.org/1999/XSL/Transform\" version=\"1.0\">");
        anotherBuffer.append("aBuffer.append(\"<xsl:template match=\"/\">");
        anotherBuffer.append("<xsl:copy-of select=\"*\"/>");
        anotherBuffer.append("</xsl:template>");
        anotherBuffer.append("</xsl:stylesheet>");

        Transformer transformer = tFactory.newTransformer(new StreamSource(new StringReader(anotherBuffer.toString())));

        transformer.transform(new DOMSource(theBackUp), new StreamResult(aFileWriter));

        aFileWriter.close();
      }
    catch(IOException ioe)
      {
        theStaticLocToLog.error(ICatalogMessagesKeys.PCAT_ERR_BACKUP_CHANGES,
            new Object[]{EditableCatalog.class.getName()},ioe);
      }
    catch (TransformerException te) 
      {
        theStaticLocToLog.error(ICatalogMessagesKeys.PCAT_ERR_BACKUP_CHANGES,
            new Object[]{EditableCatalog.class.getName()},te);        
      } // end of catch

    return theBackUp;
  }

  private void commitChangesLocal() throws CatalogException
  {
    CatalogCleanerVisitor anCleanerVistor =
      new CatalogCleanerVisitor(this);
    anCleanerVistor.start();
    return;
  }

  /**
   * Write the changes as specifed in theChanges to the remote catalog engine.
   *
   * @param theChanges    a document in an intermediate format (Catalog.dtd)
   *                      that describes the changes
   */
  public void commitChangesRemote(Document theChanges)
    throws CatalogException
  {
    theCatToEdit.commitChangesRemote(theChanges);
    return;
  }

  /* ****************************** IEditableComponent ************************ */

  public IComponent getComponentReference()
  {
    return this.getCatalogReference();
  }

  public boolean lock()
  {
    //Now get the lock
    CatalogLockManager theLocker =
      CatalogLockManager.getInstanceFor(theCatToEdit);
    return theLocker.getLock(theCatToEdit,false,theUser);
  }

  public void releaseLock()
  {
    CatalogLockManager theLocker =
      CatalogLockManager.getInstanceFor(theCatToEdit);
    theLocker.releaseLock(theCatToEdit,false,theUser);
    return;
  }

  /* ****************** IEditableHierarchicalComponent************************* */

  public boolean lockTree()
  {
    //Now get the lock
    //unused: EditableCatalog theEditor = (EditableCatalog)getRoot();
    CatalogLockManager theLocker =
      CatalogLockManager.getInstanceFor(theCatToEdit);
    return theLocker.getLock(theCatToEdit,
                             true,
                             theUser);
  }

  public void releaseTree()
  {
    CatalogLockManager theLocker =
      CatalogLockManager.getInstanceFor(theCatToEdit);
    theLocker.releaseLock(theCatToEdit,true,theUser);
    return;
  }

  /* ********************* IEditableCompositeComponen ************************* */

  public boolean lockChildren()
  {
    //Now get the lock
    //unused: EditableCatalog theEditor = (EditableCatalog)getRoot();
    CatalogLockManager theLocker =
      CatalogLockManager.getInstanceFor(theCatToEdit);
    return theLocker.getLock(theCatToEdit,
                             true,
                             theUser);
  }

  public void releaseChildren()
  {
    CatalogLockManager theLocker =
      CatalogLockManager.getInstanceFor(theCatToEdit);
    theLocker.releaseLock(theCatToEdit,false,theUser);
    return;
  }

  /* ***************************  IEditableCatalog **************************** */

  public void releaseAll()
  {
    CatalogLockManager theLocker =
      CatalogLockManager.getInstanceFor(theCatToEdit);
    theLocker.releaseLocks(theUser);
    return;
  }

  public IActor getUser()
  {
    return this.theUser;
  }

  public ICatalog getCatalogReference()
  {
    return theCatToEdit;
  }

  public synchronized boolean undoLastChange()
    throws CatalogException
  {
    return getCmdProcessor().undo();
  }

  public synchronized boolean redoLastUndo()
    throws CatalogException
  {
    return getCmdProcessor().redo();
  }

  /**
   * Synchronization: To avoid inconsistancies between threads "of
   * the same session"!
   */
  synchronized public void setName(String aNewName)
    throws CatalogException
  {
    if(isDeleted())
      {
          String theMessage = getMessageResources().getMessage(
            ICatalogMessagesKeys.PCAT_ERR_IS_DELEDED);
        throw new CatalogException(theMessage);
      }

    //Now get the lock, if new you get it any way
    if(!lock())
      {
        String theMessage = getMessageResources().getMessage(
            ICatalogMessagesKeys.PCAT_ERR_LOCK);
        throw new CatalogException(theMessage);
      }

    /********************** without cmd processor**************************
        setNameInternal(aNewName);

        //at the latest now we are changed if not new
        //(might not happen for a catalog only for consistency)
        setStateChanged();
    ***********************************************************************/
    CatalogCmdProcessor processor = getCmdProcessor();
    CatalogCmd command =
      processor.createCmdCatalogEdit( this,
                                      aNewName,
                                      CatalogCmd.PROP_NAME);
    if(!processor.doCmd(command))
      throw new CatalogException("");
    /**@todo real text has to be added **/

    return;
  }

  /**
   * Synchronization: To avoid inconsistancies between threads "of
   * the same session"!
   */
  synchronized public void setDescription(String aNewDescription)
    throws CatalogException
  {
    if(isDeleted())
      {
        String theMessage = getMessageResources().getMessage(
            ICatalogMessagesKeys.PCAT_ERR_IS_DELEDED);
        throw new CatalogException(theMessage);
      }

        //Now get the lock, if new you get it any way
    if(!lock())
    {
        String theMessage = theCatToEdit.getMessageResources().getMessage(
            ICatalogMessagesKeys.PCAT_ERR_LOCK);
        throw new CatalogException(theMessage);
    }

        /********************** without cmd processor**************************
        setDescriptionInternal(aNewDescription);

            //at the latest now we are changed if not new
                //(might not happen for a catalog only for consistency)
        setStateChanged();
        ***********************************************************************/
    CatalogCmdProcessor processor = getCmdProcessor();
    CatalogCmd command =
        processor.createCmdCatalogEdit( this,
                                        aNewDescription,
                                        CatalogCmd.PROP_DESCRIPTION);
    if(!processor.doCmd(command))
        throw new CatalogException("");
        /**@todo real text has to be added **/
  }

        /**
         * Synchronization: To avoid inconsistancies between threads "of
         * the same session"!
         */
    synchronized public IEditableDetail createDetail(String aName)
        throws CatalogException
    {
            //If we are deleted no way!
        if(isDeleted())
        {
            String theMessage = getMessageResources().getMessage(
                ICatalogMessagesKeys.PCAT_ERR_IS_DELEDED);
            throw new CatalogException(theMessage);
        }

            //Now get the lock, if ne w we get it any way
        if(!lock())
        {
            String theMessage = theCatToEdit.getMessageResources().getMessage(
                ICatalogMessagesKeys.PCAT_ERR_LOCK);
            throw new CatalogException(theMessage);
        }

            /********************** without cmd processor**************************
        EditableDetail aNewDetail = new EditableDetail(this,aName);

            //Add something the builder could not build because its new
        this.addDetailInternal(aNewDetail,true);

            //at the latest now we are changed if not new
                //(might not happen for a catalog only for consistency)
        setStateChanged();

        return aNewDetail;
            ***********************************************************************/
        CatalogCmdProcessor processor = getCmdProcessor();
        CatalogCmd command = processor.createCmdDetailCreate(this, aName);
        if(!processor.doCmd(command))
            throw new CatalogException("");
            /**@todo real text has to be added **/

        return (EditableDetail)getDetail(aName); // it's definitly of this type
    }

        /**
         * Synchronization: To avoid inconsistancies between threads "of
         * the same session"!
         */
    synchronized public void deleteDetail(IEditableDetail aThingToDelete)
        throws CatalogException
    {
        EditableDetail theThingToDelete = (EditableDetail) aThingToDelete;

            //If we are deleted there is no chance to delete any thing over us.
        if(isDeleted())
        {
            String theMessage = getMessageResources().getMessage(
                ICatalogMessagesKeys.PCAT_ERR_IS_DELEDED);
            throw new CatalogException(theMessage);
        }

            //If the thing is allready deleted no need to delete it again.
        if(theThingToDelete.isDeleted())
        {
            String theMessage = getMessageResources().getMessage(
                ICatalogMessagesKeys.PCAT_ERR_IS_DELEDED);
            throw new CatalogException(theMessage);
        }

            //If we don't own the thing inform the stubborn user with an exception
        if(!theDetails.contains(theThingToDelete))
        {
            String theMessage = getMessageResources().getMessage(
                ICatalogMessagesKeys.PCAT_ERR_OBJ_NOT_OWNED_THIS);
            throw new CatalogException(theMessage);
        }

            //Now get the lock, if ne w we get it any way
        if(!lock())
        {
            String theMessage = theCatToEdit.getMessageResources().getMessage(
                ICatalogMessagesKeys.PCAT_ERR_LOCK);
            throw new CatalogException(theMessage);
        }
        if(!theThingToDelete.lock())
        {
            String theMessage = theCatToEdit.getMessageResources().getMessage(
                ICatalogMessagesKeys.PCAT_ERR_LOCK);
            throw new CatalogException(theMessage);
        }

            /********************** without cmd processor**************************
                 //we did it
        theThingToDelete.setStateDeleted();

        deleteDetailInternal((CatalogDetail)theThingToDelete);

            //at the latest now we are changed if not new
                //(might not happen for a catalog only for consistency)
        setStateChanged();
            ***********************************************************************/

        CatalogCmdProcessor processor = getCmdProcessor();
        CatalogCmd command =
            processor.createCmdDetailDelete(this, theThingToDelete);
        if(!processor.doCmd(command))
            throw new CatalogException("");
            /**@todo real text has to be added **/

        return;
    }

        /**
         * Synchronization: To avoid inconsistancies between threads "of
         * the same session"!
         */
    synchronized public IEditableCategory createChild(String aGuid)
        throws CatalogException
    {
        if(isDeleted())
        {
            String theMessage = getMessageResources().getMessage(
                ICatalogMessagesKeys.PCAT_ERR_IS_DELEDED);
            throw new CatalogException(theMessage);
        }

            //Now get the lock, if new you get it any way
        if(!lock())
        {
            String theMessage = theCatToEdit.getMessageResources().getMessage(
                ICatalogMessagesKeys.PCAT_ERR_LOCK);
            throw new CatalogException(theMessage);
        }

            /********************** without cmd processor**************************
        EditableCategory aNewCategory = new EditableCategory(this,aGuid);

        addChildCategoryInternal(aNewCategory,true);

            //don't forget the hashMap
        addCategory(aNewCategory);

            //at the latest now we are changed if not new
                //(might not happen for a catalog only for consistency)
        setStateChanged();

        return aNewCategory;
            ***********************************************************************/
        CatalogCmdProcessor processor = getCmdProcessor();
        CatalogCmd command =
            processor.createCmdCategoryCreate(this, aGuid);
        if(!processor.doCmd(command))
            throw new CatalogException("");
            /**@todo real text has to be added **/

        return (EditableCategory)getChildCategory(aGuid);
    }

        /**
         * Synchronization: To avoid inconsistancies between threads "of
         * the same session"!
         */
    synchronized public void deleteChild(IEditableCategory aThingToDelete)
        throws CatalogException
    {
        EditableCategory theThingToDelete = (EditableCategory)aThingToDelete;

        if(isDeleted())
        {
            String theMessage = getMessageResources().getMessage(
                ICatalogMessagesKeys.PCAT_ERR_IS_DELEDED);
            throw new CatalogException(theMessage);
        }

            //If the thing is allready deleted no need to delete it again.
        if(theThingToDelete.isDeleted())
        {
            String theMessage = getMessageResources().getMessage(
                ICatalogMessagesKeys.PCAT_ERR_IS_DELEDED);
            throw new CatalogException(theMessage);
        }

            //If we don't own the thing inform the stubborn user with an exception
        if(!theInnerNodes.contains(theThingToDelete))
        {
            String theMessage = getMessageResources().getMessage(
                ICatalogMessagesKeys.PCAT_ERR_OBJ_NOT_OWNED_THIS);
            throw new CatalogException(theMessage);
        }

            //Now get the lock, if new you get it any way
        if(!lock())
        {
            String theMessage = theCatToEdit.getMessageResources().getMessage(
                ICatalogMessagesKeys.PCAT_ERR_LOCK);
            throw new CatalogException(theMessage);
        }
        if(!theThingToDelete.lockTree())
        {
            String theMessage = theCatToEdit.getMessageResources().getMessage(
                ICatalogMessagesKeys.PCAT_ERR_LOCK);
            throw new CatalogException(theMessage);
        }

            /********************** without cmd processor**************************
        theThingToDelete.setStateDeleted();

        deleteChildCategoryInternal(theThingToDelete);
            //remove it from the internal hasTable
        removeCategory(theThingToDelete);

            //at the latest now we are changed if not new
                //(might not happen for a catalog only for consistency)
        setStateChanged();
            ***********************************************************************/

        CatalogCmdProcessor processor = getCmdProcessor();
        CatalogCmd command =
            processor.createCmdCategoryDelete(this, theThingToDelete);
        if(!processor.doCmd(command))
            throw new CatalogException("");
            /**@todo real text has to be added **/

        return;
    }

        /**
         * Synchronization: To avoid inconsistancies between threads "of
         * the same session"!
         */
    synchronized public IEditableAttribute createAttribute(String aGuid)
        throws CatalogException
    {
            /**@todo we should check that we realy get a new guid*/
        if(isDeleted())
        {
            String theMessage = getMessageResources().getMessage(
                ICatalogMessagesKeys.PCAT_ERR_IS_DELEDED);
            throw new CatalogException(theMessage);
        }

            //Now get the lock, if new you get it any way
        if(!lock())
        {
            String theMessage = theCatToEdit.getMessageResources().getMessage(
                ICatalogMessagesKeys.PCAT_ERR_LOCK);
            throw new CatalogException(theMessage);
        }

            /********************** without cmd processor**************************
        EditableAttribute aNewAttribute = new EditableAttribute(this,aGuid);

        addAttributeInternal(aNewAttribute,true);

            //at the latest now we are changed if not new
                //(might not happen for a catalog only for consistency)
        setStateChanged();

        return aNewAttribute;
            ***********************************************************************/

        CatalogCmdProcessor processor = getCmdProcessor();
        CatalogCmd command = processor.createCmdAttributeCreate(this, aGuid);
        if(!processor.doCmd(command))
            throw new CatalogException(""); /**@todo real text has to be added **/

        return (EditableAttribute)getAttributeInternal(aGuid);
    }

        /**
         * Synchronization: To avoid inconsistancies between threads "of
         * the same session"!
         */
    synchronized public void deleteAttribute(IEditableAttribute aThingToDelete)
        throws CatalogException
    {
        EditableAttribute theThingToDelete = (EditableAttribute) aThingToDelete;
        if(isDeleted())
        {
            String theMessage = getMessageResources().getMessage(
                ICatalogMessagesKeys.PCAT_ERR_IS_DELEDED);
            throw new CatalogException(theMessage);
        }

            //If the thing is allready deleted no need to delete it again.
        if(theThingToDelete.isDeleted())
        {
            String theMessage = getMessageResources().getMessage(
                ICatalogMessagesKeys.PCAT_ERR_IS_DELEDED);
            throw new CatalogException(theMessage);
        }

            //If we don't own the thing inform the stubborn user with an exception
        if(!theAttributes.contains(theThingToDelete))
        {
            String theMessage = getMessageResources().getMessage(
                ICatalogMessagesKeys.PCAT_ERR_OBJ_NOT_OWNED_THIS);
            throw new CatalogException(theMessage);
        }

            //Now get the lock, if new you get it any way
        if(!lockTree())
        {
            String theMessage = theCatToEdit.getMessageResources().getMessage(
                ICatalogMessagesKeys.PCAT_ERR_LOCK);
            throw new CatalogException(theMessage);
        }
        if(!theThingToDelete.lock())
        {
            String theMessage = theCatToEdit.getMessageResources().getMessage(
                ICatalogMessagesKeys.PCAT_ERR_LOCK);
            throw new CatalogException(theMessage);
        }

            /********************** without cmd processor**************************
                 //we did it
        theThingToDelete.setStateDeleted();

        deleteAttributeInternal(theThingToDelete);

            //at the latest now we are changed if not new
                //(might not happen for a catalog only for consistency)
        setStateChanged();
            ***********************************************************************/
        CatalogCmdProcessor processor = getCmdProcessor();
        CatalogCmd command =
            processor.createCmdAttributeDelete(this, theThingToDelete);
        if(!processor.doCmd(command))
            throw new CatalogException("");
            /**@todo real text has to be added **/

        return;
    }


        /**
         * Synchronized to protect from changes with in session
         */
    public synchronized void commit() throws CatalogException
    {
            /**
             * protect theCatalog we are going to edit
             * and continue to grab locks as we travers the catalog.....
             */
        synchronized(this.theCatToEdit)
        {
                //save the changes in an XML format for later use in case somthing
                //goes wrong
                /**@todo Further action item should us enable restart an edit session*/
            Document theChanges = backUpChangesLocal();

            commitChangesRemote(theChanges);

            commitChangesLocal();

                //finally release your locks
            releaseAll();
        }

        return;
    }


        /* ************************* IComponentEventListener ************************ */

        /**
         * Will be called after the reference has been changed by an
         * editing thread from another session.
         */
    synchronized public void componentChanged(IComponentChangedEvent anEvent)
    {
            //changed i.e. the ref has changed
            //update your state from the ref
        if(anEvent.getComponent().getState()!=IComponent.State.DELETED)
        {
            this.setNameInternal(theCatToEdit.getName());
            this.setDescriptionInternal(theCatToEdit.getDescription());
            this.setThumbNailInternal(theCatToEdit.getThumbNail());
        }

            //deleted i.e. your ref has been deleted
            //should not happen
        if(anEvent.getComponent().getState()==IComponent.State.DELETED)
        {
            throw new java.lang.UnsupportedOperationException(
                "Deletion of catalogs are not supported!");
        }
    }

        /**
         * Will be called after a child detail was created for the reference by an
         * editing thread from another session.
         */
    synchronized public void detailCreated(IDetailCreatedEvent aNewDetailEvent)
    {
            //add the new detail ref to your list

            //but do it only if you have allready asked for details
        if(!this.getCatalogBuilder().isDetailsBuilt(this))
            return;

        CatalogDetail aNewDetail = (CatalogDetail) aNewDetailEvent.getDetail();
        EditableDetail aEditableDetail =
            new EditableDetail(this,aNewDetail);
        aEditableDetail.addDetailInternal(aEditableDetail);

        return;
    }

        /**
         * Will be called after a child detail was deleted for the reference by an
         * editing thread from another session.
         */
    synchronized public void detailDeleted(IDetailDeletedEvent anDeleteEvent)
    {
            //remove the deleted details from your list

            //but do it only if you have allready asked for details
        if(!this.getCatalogBuilder().isDetailsBuilt(this))
            return;

            //The editable detail itself was informed before this that it is deleted
        Iterator theEditableDetails = getDetailsInternal();
        while(theEditableDetails.hasNext())
        {
            EditableDetail aDetail = (EditableDetail)theEditableDetails.next();
            if(aDetail.isDeleted())
                theEditableDetails.remove();
        }
    }

        /* ************************** ICompositeEventListener *********************** */

        /**
         * Will be called after a attribute was created for the reference by an
         * editing thread from another session.
         */
    synchronized public void attributeCreated(
        IAttributeCreatedEvent aNewAttributeEvent)
    {
            //add the new attribute ref to your list

            /**@todo implement it*/
        return;
    }

        /**
         * Will be called after a attribute was deleted for the reference by an
         * editing thread from another session.
         */
    synchronized public void attributeDeleted(
        IAttributeDeletedEvent anDeleteEvent)
    {
            //remove the deleted attribute from your list

            /**@todo implement it*/

        return;
    }

        /**
         * Will be called after a child (root category) was created for the
         * reference by an editing thread from another session.
         */
    synchronized public void childCreated(
        IChildCreatedEvent aNewChildEvent)
    {
            //add the new child ref to your list

            /**@todo implement it*/

        return;
    }

        /**
         * Will be called after a child (root chategory) was deleted from the
         * reference by an editing thread from another session.
         *
         */
    synchronized public void childDeleted(
        IChildDeletedEvent anDeleteEvent)
    {
            //remove the deleted child from your list

            /**@todo implement it*/

        return;
    }


}
