/*****************************************************************************
  Copyright (c) 2000-2001, SAPMarkets Europe GmbH, Germany, All rights reserved.
  Created:      02 August 2001

  $Id: //sap/ESALES_base/30_SP_COR/src/_pcatAPI/java/com/sap/isa/catalog/editor/EditableItem.java#2 $
  $Revision: #2 $
  $Change: 129221 $
  $DateTime: 2003/05/15 17:20:58 $
*****************************************************************************/
package com.sap.isa.catalog.editor;

//  catalog interfaces
import com.sap.isa.catalog.ICatalogMessagesKeys;
import com.sap.isa.catalog.boi.CatalogException;
import com.sap.isa.catalog.boi.IAttributeValueCreatedEvent;
import com.sap.isa.catalog.boi.IAttributeValueDeletedEvent;
import com.sap.isa.catalog.boi.IComponent;
import com.sap.isa.catalog.boi.IComponentChangedEvent;
import com.sap.isa.catalog.boi.IComponentEventListener;
import com.sap.isa.catalog.boi.IDetailCreatedEvent;
import com.sap.isa.catalog.boi.IDetailDeletedEvent;
import com.sap.isa.catalog.boi.IEditableAttribute;
import com.sap.isa.catalog.boi.IEditableAttributeValue;
import com.sap.isa.catalog.boi.IEditableDetail;
import com.sap.isa.catalog.boi.IEditableItem;
import com.sap.isa.catalog.boi.IItem;
import com.sap.isa.catalog.boi.IItemEventListener;
import com.sap.isa.catalog.catalogcmd.CatalogCmd;
import com.sap.isa.catalog.catalogcmd.CatalogCmdProcessor;
import com.sap.isa.catalog.impl.Catalog;
import com.sap.isa.catalog.impl.CatalogCompositeComponent;
import com.sap.isa.catalog.impl.CatalogDetail;
import com.sap.isa.catalog.impl.CatalogItem;
import com.sap.isa.catalog.impl.CatalogLockManager;

/**
 *  An editor for an Item.
 *
 *  @version     1.0
 */
public class EditableItem
    extends CatalogItem
    implements  IEditableItem,
                IComponentEventListener,
                IItemEventListener
{
    /*
     *  CatalogItem implements Serializable:
     *  Fields:
     *  theItemToEdit     :   implements Serializable
     */
    private CatalogItem theItemToEdit; // null if new

    /**
     * To create new editors for existing items. The constructor pulls the
     * internal state from the component to edit. The construction process
     * via {@link EditableCatalogBuilder}, {@link CatalogCleanerVisitor},
     * {@link EditableCategory} enforces the preconditions beween the parameters.
     * The state after creation is LOADED.
     *
     * @param   anEditableParentComponent a catalog category that is about to be
     *          edited. i.e. the editable wrapper of the parent category of
     *          anItemToEdit
     * @param   anItemToEdit the component that is edited via this.
     */
    EditableItem(CatalogCompositeComponent anEditableParentComponent,
                 CatalogItem anItemToEdit)
    {
        super(anEditableParentComponent,anItemToEdit.getGuid());
        this.theItemToEdit=anItemToEdit;
        setState(IComponent.State.LOADED);
        //pull your internal state from the existing item
        this.setNameInternal(anItemToEdit.getName());
        this.setProductGuidInternal(anItemToEdit.getProductGuid());

        //listen to your component
        theItemToEdit.addComponentEventListener(this);
        theItemToEdit.addItemEventListener(this);
    }

    /**
     * To create new editors for new items. The construction process
     * via {@link EditableCatalogBuilder}, {@link CatalogCleanerVisitor},
     * {@link EditableCategory} enforces the preconditions beween the parameters.
     * The state after creation is NEW.
     *
     * @param   anEditableParentComponent a catalog category that is about to
     *          get a new item. i.e. the editable wrapper of the category that
     *          will finaly get the new item.
     * @param   aGuid a guid that will be used for this. The user has to
     *          ensure that guids that are used here are consistent with the
     *          guids used by the underlying catalog engine.
     */
    EditableItem(CatalogCompositeComponent anEditableParentComponent,
                 String aGuid)
    {
        super(anEditableParentComponent,aGuid);
        setState(IComponent.State.NEW);
    }

    /**
     * For internal use in the construction process.
     *
     * @return the reference to the item beeing edited.
     */
    CatalogItem getItem()
    {
        return theItemToEdit;
    }

    /**
     * For internal use in the construction process.
     *
     * @return the reference to the item beeing edited.
     */
    void setItem(CatalogItem theRef)
    {
        theItemToEdit= theRef;

        //listen to your component
        theItemToEdit.addComponentEventListener(this);
        theItemToEdit.addItemEventListener(this);

        return;
    }

    /**
     * Creates an associated EditableAttributeValue. Internal method which
     * can't be called via the public interface.
     *
     * @return a new instance of <code>EditableAttributeValue</code>
     */
    synchronized public
        EditableAttributeValue createEditableAttributeValueInternal(String guid)
    {
        EditableAttribute attribute = null;
        EditableAttributeValue value = null;

        try
        {
            attribute = //No doubt about it
                (EditableAttribute)getCategory().getAttributeInternal(guid);
            if(attribute != null)
            {
                value = new EditableAttributeValue(this, attribute);
                addAttributeValueInternal(value,true);
            }
        }
        catch(CatalogException ce)
        {
            value = null;
        }

        return value;
    }

/* ************************** IEditableComponent **************************** */

    public IComponent getComponentReference()
    {
        return this.getItemReference();
    }

    public boolean lock()
    {
        if(theItemToEdit!=null)
        {
            //Now get the lock
            Catalog theCataToEdit = ((EditableCatalog)getRoot()).getCatalog();
            CatalogLockManager theLocker =
                CatalogLockManager.getInstanceFor(theCataToEdit);
            return theLocker.getLock(   theItemToEdit,
                                        false,
                                        ((EditableCatalog)getRoot()).getUser());
        }
        else
            return true;
    }

    public void releaseLock()
    {
        if(theItemToEdit!=null)
        {
            //Now get the lock
            EditableCatalog theEditor = (EditableCatalog)getRoot();
            CatalogLockManager theLocker =
                CatalogLockManager.getInstanceFor(theEditor.getCatalog());
            theLocker.releaseLock(theItemToEdit,
                                  false,
                                  theEditor.getUser());
        }
        return;
    }

/* *********************** IEditableHierachiaclComponent ******************** */

    public boolean lockTree()
    {
        if(theItemToEdit!=null)
        {
            //Now get the lock
            Catalog theCataToEdit = ((EditableCatalog)getRoot()).getCatalog();
            CatalogLockManager theLocker =
                CatalogLockManager.getInstanceFor(theCataToEdit);
            return theLocker.getLock(   theItemToEdit,
                                        true,
                                        ((EditableCatalog)getRoot()).getUser());
        }
        else
            return true;
    }

    public void releaseTree()
    {
        if(theItemToEdit!=null)
        {
            //Now get the lock
            EditableCatalog theEditor = (EditableCatalog)getRoot();
            CatalogLockManager theLocker =
                CatalogLockManager.getInstanceFor(theEditor.getCatalog());
            theLocker.releaseLock(theItemToEdit,
                                  true,
                                  theEditor.getUser());
        }
        return;
    }

/* ********************* IEditableItem ************************************** */

    synchronized public IItem getItemReference()
    {
        return theItemToEdit;
    }

    /**
     * Synchronization: To avoid inconsistancies between threads "of
     * the same session"!
     */
    synchronized public void setName(String aNewName)
        throws CatalogException
    {
        if(isDeleted())
        {
            String theMessage = getRoot().getMessageResources().getMessage(
                ICatalogMessagesKeys.PCAT_ERR_IS_DELEDED);
            throw new CatalogException(theMessage);
        }

        if(!lock())//if new we get it any way
        {
            String theMessage =
                ((EditableCatalog)getRoot()).getCatalog().getMessageResources()
                .getMessage(ICatalogMessagesKeys.PCAT_ERR_LOCK);
            throw new CatalogException(theMessage);
        }

        /******************* without cmd processor *****************************
        setNameInternal(aNewName);

        //if not new we are changed now
        setStateChanged();

        return;
        ***********************************************************************/
        EditableCatalog theEditor = (EditableCatalog)getRoot();
        CatalogCmdProcessor processor = theEditor.getCmdProcessor();
        CatalogCmd command =
            processor.createCmdItemEdit(this, aNewName, CatalogCmd.PROP_NAME);
        if(!processor.doCmd(command))
            throw new CatalogException("");
            /**@todo real text has to be added **/
        return;
    }

    /**
     * Synchronization: To avoid inconsistancies between threads "of
     * the same session"!
     */
    synchronized public void setProductGuid(String aNewGuid)
        throws CatalogException
    {
        if(isDeleted())
        {
            String theMessage = getRoot().getMessageResources().getMessage(
                ICatalogMessagesKeys.PCAT_ERR_IS_DELEDED);
            throw new CatalogException(theMessage);
        }

        if(!lock())//if new we get it any way
        {
            String theMessage =
                ((EditableCatalog)getRoot()).getCatalog().getMessageResources()
                .getMessage(ICatalogMessagesKeys.PCAT_ERR_LOCK);
            throw new CatalogException(theMessage);
        }

        /******************* without cmd processor *****************************
        setProductGuidInternal(aNewGuid);

        //if not new we are changed now
        setStateChanged();

        return;
        ***********************************************************************/
        EditableCatalog theEditor = (EditableCatalog)getRoot();
        CatalogCmdProcessor processor = theEditor.getCmdProcessor();
        CatalogCmd command =
            processor.createCmdItemEdit(this,
                                        aNewGuid,
                                        CatalogCmd.PROP_PRODUCT_GUID);
        if(!processor.doCmd(command))
            throw new CatalogException("");
            /**@todo real text has to be added **/
        return;
    }

    /**
     * Synchronization: To avoid inconsistancies between threads "of
     * the same session"!
     */
    synchronized public IEditableAttributeValue
        createAttributeValue(IEditableAttribute anAttribute)
        throws CatalogException
    {
        //If we are deleted no way!
        if(isDeleted())
        {
            String theMessage = getRoot().getMessageResources().getMessage(
                ICatalogMessagesKeys.PCAT_ERR_IS_DELEDED);
            throw new CatalogException(theMessage);
        }

        //Now get the lock (if this is new you get the lock any way)
        if(!lock())
        {
            String theMessage = getRoot().getMessageResources().getMessage(
                ICatalogMessagesKeys.PCAT_ERR_LOCK);
            throw new CatalogException(theMessage);
        }

        /********************** without cmd processor **************************
        EditableAttribute theAttribute = (EditableAttribute) anAttribute;
        EditableAttributeValue aNewValue =
            new EditableAttributeValue(this,theAttribute);

        this.addAttributeValueInternal(aNewValue,true);

        //at the latest now we are changed if not new
        setStateChanged();

        return aNewValue;
        ***********************************************************************/
        EditableCatalog theEditor = (EditableCatalog)getRoot();
        CatalogCmdProcessor processor = theEditor.getCmdProcessor();
        CatalogCmd command =
            processor.createCmdAttributeValueCreate(this,
                                                    anAttribute.getGuid());
        if(!processor.doCmd(command))
            throw new CatalogException("");
            /**@todo real text has to be added **/

        return (EditableAttributeValue)getAttributeValue(anAttribute.getGuid());

    }

    /**
     * Synchronization: To avoid inconsistancies between threads "of
     * the same session"!
     */
    synchronized public void deleteAttributValue(
        IEditableAttributeValue aThingToDelete)
        throws CatalogException
    {
        EditableAttributeValue theThingToDelete =
            (EditableAttributeValue) aThingToDelete;

        //If we are deleted there is no chance to delete any thing over us.
        if(isDeleted())
        {
            String theMessage = getRoot().getMessageResources().getMessage(
                ICatalogMessagesKeys.PCAT_ERR_IS_DELEDED);
            throw new CatalogException(theMessage);
        }
        //If the thing is allready deleted no need to delete it again.
        if(theThingToDelete.isDeleted())
        {
            String theMessage = getRoot().getMessageResources().getMessage(
                ICatalogMessagesKeys.PCAT_ERR_IS_DELEDED);
            throw new CatalogException(theMessage);
        }

        //If we don't own the thing inform the stubborn user with an exception
        if(!this.theAttributeValues.contains(theThingToDelete))
        {
            String theMessage = getRoot().getMessageResources().getMessage(
                ICatalogMessagesKeys.PCAT_ERR_OBJ_NOT_OWNED_THIS);
            throw new CatalogException(theMessage);
        }

        if(!lock())//if new we get it any way
        {
            String theMessage = getRoot().getMessageResources().getMessage(
                ICatalogMessagesKeys.PCAT_ERR_LOCK);
            throw new CatalogException(theMessage);
        }
        if(!theThingToDelete.lock())//if new we get it any way
        {
            String theMessage = getRoot().getMessageResources().getMessage(
                ICatalogMessagesKeys.PCAT_ERR_LOCK);
            throw new CatalogException(theMessage);
        }

        /********************* without cmd processor ***************************
        //we did it
        theThingToDelete.setStateDeleted();

        deleteAttributeValueInternal((CatalogAttributeValue) theThingToDelete);

        //if not new we are changed now
        setStateChanged();

        return;
        ***********************************************************************/
        EditableCatalog theEditor = (EditableCatalog)getRoot();
        CatalogCmdProcessor processor = theEditor.getCmdProcessor();
        CatalogCmd command =
            processor.createCmdAttributeValueDelete(this, theThingToDelete);
        if(!processor.doCmd(command))
            throw new CatalogException("");
            /**@todo real text has to be added **/
        return;
    }

    /**
     * Synchronization: To avoid inconsistancies between threads "of
     * the same session"!
     */
    synchronized public IEditableDetail createDetail(String aName)
        throws CatalogException
    {
        //If we are deleted no way!
        if(isDeleted())
        {
            String theMessage = getRoot().getMessageResources().getMessage(
                ICatalogMessagesKeys.PCAT_ERR_IS_DELEDED);
            throw new CatalogException(theMessage);
        }

        //Now get the lock, if new we get it any way
        if(!lock())
        {
            String theMessage = getRoot().getMessageResources().getMessage(
                ICatalogMessagesKeys.PCAT_ERR_LOCK);
            throw new CatalogException(theMessage);
        }

        EditableDetail aNewDetail = new EditableDetail(this,aName);

        //Add something the builder could not build because its new
        this.addDetailInternal(aNewDetail,true);

        //at the latest now we are changed if not new
        setStateChanged();

        return aNewDetail;
    }

    /**
     * Synchronization: To avoid inconsistancies between threads "of
     * the same session"!
     */
    synchronized public void deleteDetail(IEditableDetail aThingToDelete)
        throws CatalogException
    {
        EditableDetail theThingToDelete =
            (EditableDetail)aThingToDelete;

        //If we are deleted there is no chance to delete any thing over us.
        if(isDeleted())
        {
            String theMessage = getRoot().getMessageResources().getMessage(
                ICatalogMessagesKeys.PCAT_ERR_IS_DELEDED);
            throw new CatalogException(theMessage);
        }

        //If the thing is allready deleted no need to delete it again.
        if(theThingToDelete.isDeleted())
        {
            String theMessage = getRoot().getMessageResources().getMessage(
                ICatalogMessagesKeys.PCAT_ERR_IS_DELEDED);
            throw new CatalogException(theMessage);
        }

        //If we don't own the thing inform the stubborn user with an exception
        if(!theDetails.contains(theThingToDelete))
        {
            String theMessage = getRoot().getMessageResources().getMessage(
                ICatalogMessagesKeys.PCAT_ERR_OBJ_NOT_OWNED_THIS);
            throw new CatalogException(theMessage);
        }

        if(!lock())//if new we get it any way
        {
            String theMessage = getRoot().getMessageResources().getMessage(
                ICatalogMessagesKeys.PCAT_ERR_LOCK);
            throw new CatalogException(theMessage);
        }
        if(!theThingToDelete.lock())//if new we get it any way
        {
            String theMessage = getRoot().getMessageResources().getMessage(
                ICatalogMessagesKeys.PCAT_ERR_LOCK);
            throw new CatalogException(theMessage);
        }

        //we did it
        theThingToDelete.setStateDeleted();

        this.deleteDetailInternal((CatalogDetail)aThingToDelete);

        //at the latest now we are changed if not new
        setStateChanged();

        return;
    }

/* ************************* IComponentEventListener ************************ */

    synchronized public void componentChanged(IComponentChangedEvent anEvent)
    {
        /**@todo implement listener interface */
    }

    synchronized public void detailCreated(IDetailCreatedEvent p0)
    {
        /**@todo implement listener interface */
    }

    synchronized public void detailDeleted(IDetailDeletedEvent anDeleteEvent)
    {
        /**@todo implement listener interface */
    }

/* ************************* IComponentEventListener ************************ */

    synchronized public void attributeValueCreated(IAttributeValueCreatedEvent p0)
    {
        ;
    }

    synchronized public void attributeValueDeleted(IAttributeValueDeletedEvent p0)
    {
        ;
    }
}
