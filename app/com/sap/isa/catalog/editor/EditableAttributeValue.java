/*****************************************************************************
  Copyright (c) 2000-2001, SAPMarkets Europe GmbH, Germany, All rights reserved.
  Created:      02 August 2001

  $Id: //sap/ESALES_base/30_SP_COR/src/_pcatAPI/java/com/sap/isa/catalog/editor/EditableAttributeValue.java#2 $
  $Revision: #2 $
  $Change: 129221 $
  $DateTime: 2003/05/15 17:20:58 $
*****************************************************************************/
package com.sap.isa.catalog.editor;

//  catalog interfaces
import java.net.URL;
import java.util.Date;
import java.util.Iterator;

import com.sap.isa.catalog.ICatalogMessagesKeys;
import com.sap.isa.catalog.boi.CatalogException;
import com.sap.isa.catalog.boi.IAttribute;
import com.sap.isa.catalog.boi.IAttributeValue;
import com.sap.isa.catalog.boi.IComponent;
import com.sap.isa.catalog.boi.IComponentChangedEvent;
import com.sap.isa.catalog.boi.IComponentEventListener;
import com.sap.isa.catalog.boi.IDetailCreatedEvent;
import com.sap.isa.catalog.boi.IDetailDeletedEvent;
import com.sap.isa.catalog.boi.IEditableAttributeValue;
import com.sap.isa.catalog.boi.IInterval;
import com.sap.isa.catalog.catalogcmd.CatalogCmd;
import com.sap.isa.catalog.catalogcmd.CatalogCmdProcessor;
import com.sap.isa.catalog.impl.Catalog;
import com.sap.isa.catalog.impl.CatalogAttribute;
import com.sap.isa.catalog.impl.CatalogAttributeValue;
import com.sap.isa.catalog.impl.CatalogComponentTypes;
import com.sap.isa.catalog.impl.CatalogItem;
import com.sap.isa.catalog.impl.CatalogLockManager;
import com.sap.isa.core.logging.IsaLocation;

/**
 *  An editor for an attribute value.
 *
 *  @version     1.0
 */
public class EditableAttributeValue
    extends CatalogAttributeValue
    implements  IEditableAttributeValue,
                IComponentEventListener
{
    /*
     *  CatalogAttributeValue implements Serializable:
     *  Fields:
     *  theAttValueToEdit     :   implements Serializable
     */
    private CatalogAttributeValue theAttValueToEdit;

	/** 
	  *  The IsaLocation of the current Action.
	  *  This will be instantiated during perform and is always present.
	  */
	 protected IsaLocation log = IsaLocation.getInstance(EditableAttributeValue.class.getName());

    /**
     * To create new editors for existing values. The constructor pulls the
     * internal state from the component to edit. The construction process
     * via {@link EditableCatalogBuilder}, {@link CatalogCleanerVisitor},
     * {@link EditableItem} enforces the preconditions beween the parameters.
     * The state after creation is LOADED.
     *
     * @param   anEditableParent a catalog item that is about to be edited.
     *          i.e. the editable wrapper of the parent item of anAttValueToEdit
     * @param   anEditableAttribute that is used as the associated attribute
     *          for the new value
     *          i.e. the editable wrapper of the attribute associated with
     *          anAttValueToEdit
     * @param   anAttValueToEdit the component that is edited via this.
     */
    EditableAttributeValue(  CatalogItem anEditableParent,
                             CatalogAttribute anEditableAttribute,
                             CatalogAttributeValue anAttValueToEdit)
    {
        super(anEditableParent,anEditableAttribute);
        this.theAttValueToEdit=anAttValueToEdit;
        setState(IComponent.State.LOADED);
        //pull your state from the existing value
        Iterator theValues = anAttValueToEdit.getAllValues();

        //listen to your reference
        theAttValueToEdit.addComponentEventListener(this);

        while(theValues.hasNext())
        {
            this.setAsObjectInternal(theValues.next());
        }
    }

    /**
     * To create new editors for new values. The construction process
     * via {@link EditableItem}, {@link CatalogCleanerVisitor},
     * {@link EditableItem} enforces the preconditions beween the parameters.
     * The state after creation is NEW.
     *
     * @param   anEditableParent a catalog item that is about to get an new
     *          value. i.e. the editable wrapper of the item that will finaly
     *          get the new value.
     * @param   anEditableAttribute that is used as the associated attribute
     *          for the new value i.e. the editable wrapper of the attribute
     *          that will be associated with this.
     */
    EditableAttributeValue(  CatalogItem anEditableParent,
                             CatalogAttribute anEditableAttribute)
    {
        super(anEditableParent,anEditableAttribute);
        setState(IComponent.State.NEW);
        //pull a default value from the type of the attribute
        String myType=anEditableAttribute.getType().intern();
        if(myType==IAttribute.TYPE_ID_STRING)
            setAsObjectInternal("");
        else if(myType==IAttribute.TYPE_ID_INTEGER.intern())
            setAsObjectInternal(new Integer(0));
        else if(myType==IAttribute.TYPE_ID_DOUBLE.intern())
            setAsObjectInternal(new Double(0));
        else if(myType==IAttribute.TYPE_ID_DATE.intern())
            setAsObjectInternal(new Date());
        else if(myType==IAttribute.TYPE_ID_LONG.intern())
            setAsObjectInternal(new Long(0));
        else if(myType==IAttribute.TYPE_ID_BOOLEAN.intern())
            setAsObjectInternal(new Boolean(false));
        else if(myType==IAttribute.TYPE_ID_OBJECT.intern())
            setAsObjectInternal(new Object());
        else if(myType==IAttribute.TYPE_ID_INTERVAL.intern())
            setAsObjectInternal(null);/**@todo we need at least one impl*/
        else if(myType==IAttribute.TYPE_ID_URL.intern())
        {
            try
            {
                setAsObjectInternal(new URL("http:\\localhost"));
            }
            catch(Throwable t)
            {
            	log.debug(t.getMessage());
            }
        }
    }

    /**
     * For internal use in the construction process.
     *
     * @return the reference to the value beeing edited.
     */
    CatalogAttributeValue getAttributeValue()
    {
        return theAttValueToEdit;
    }

    /**
     * For internal use in the construction process.
     *
     * @param the reference to the value beeing edited.
     */
    void setAttributeValue(CatalogAttributeValue theReference)
    {
        theAttValueToEdit=theReference;

        //listen to your reference
        theAttValueToEdit.addComponentEventListener(this);

        return;
    }

/* ************************** IEditableComponent **************************** */

    public IComponent getComponentReference()
    {
        return this.getAttributeValueReference();
    }

    public boolean lock()
    {
        if(theAttValueToEdit!=null)
        {
            //Now get the lock
            EditableCatalog theEditor = (EditableCatalog)getRoot();
            CatalogLockManager theLocker =
                CatalogLockManager.getInstanceFor(theEditor.getCatalog());
            return theLocker.getLock(   theAttValueToEdit,
                                        false,
                                        theEditor.getUser());
        }
        else
            return true;
    }

    public void releaseLock()
    {
        if(theAttValueToEdit!=null)
        {
            //Now get the lock
            EditableCatalog theEditor = (EditableCatalog)getRoot();
            CatalogLockManager theLocker =
                CatalogLockManager.getInstanceFor(theEditor.getCatalog());
            theLocker.releaseLock(theAttValueToEdit,
                                  false,
                                  theEditor.getUser());
        }
        return;
    }

/* *********************** IEditableAttributeValue ************************** */

    public IAttributeValue getAttributeValueReference()
    {
        return theAttValueToEdit;
    }

    /**
     * Synchronized: To avoid inconsistencies between threads of the
     * same session.
     */
    synchronized public void setAsString(String aStringValue)
        throws CatalogException
    {
        // An insane value will be punished by an exception
        checkPreconditions(aStringValue, IAttribute.TYPE_ID_STRING, false);

        // Type and consitency was checked by method interface an helper method
        // Lets try to set the value
        setValue(aStringValue);
    }

    /**
     * Synchronized: To avoid inconsistencies between threads of the
     * same session.
     */
    synchronized public void setAsInteger(Integer anIntValue)
        throws CatalogException
    {
        //An insane value will be punished by an exception
        checkPreconditions(anIntValue, IAttribute.TYPE_ID_INTEGER, false);

        // Type and consitency was checked by method interface an helper method
        // Lets try to set the value
        setValue(anIntValue);
    }

    /**
     * Synchronized: To avoid inconsistencies between threads of the
     * same session.
     */
    synchronized public void setAsLong(Long aLongValue)
        throws CatalogException
    {
        //An insane value will be punished by an exception
        checkPreconditions(aLongValue, IAttribute.TYPE_ID_LONG, false);

        // Type and consitency was checked by method interface an helper method
        // Lets try to set the value
        setValue(aLongValue);
    }

    /**
     * Synchronized: To avoid inconsistencies between threads of the
     * same session.
     */
    synchronized public void setAsBoolean(Boolean aBooleanValue)
        throws CatalogException
    {
        //An insane value will be punished by an exception
        checkPreconditions(aBooleanValue, IAttribute.TYPE_ID_BOOLEAN, false);

        // Type and consitency was checked by method interface an helper method
        // Lets try to set the value
        setValue(aBooleanValue);
    }

    /**
     * Synchronized: To avoid inconsistencies between threads of the
     * same session.
     */
    synchronized public void setAsObject(Object anObjectValue)
        throws CatalogException
    {
        //An insane value will be punished by an exception
        checkPreconditions(anObjectValue, IAttribute.TYPE_ID_OBJECT, false);

        // Type and consitency was checked by method interface an helper method
        // Lets try to set the value
        setValue(anObjectValue);
    }

    /**
     * Synchronized: To avoid inconsistencies between threads of the
     * same session.
     */
    synchronized public void setAsInterval(IInterval anIntervalValue)
        throws CatalogException
    {
        //An insane value will be punished by an exception
        checkPreconditions(anIntervalValue, IAttribute.TYPE_ID_INTERVAL, false);

        // Type and consitency was checked by method interface an helper method
        // Lets try to set the value
        setValue(anIntervalValue);
    }

    /**
     * Synchronized: To avoid inconsistencies between threads of the
     * same session.
     */
    synchronized public void setAsDate(Date aDateValue)
        throws CatalogException
    {
        //An insane value will be punished by an exception
        checkPreconditions(aDateValue, IAttribute.TYPE_ID_DATE, false);

        // Type and consitency was checked by method interface an helper method
        // Lets try to set the value
        setValue(aDateValue);
    }

    /**
     * Synchronized: To avoid inconsistencies between threads of the
     * same session.
     */
    synchronized public void setAsDouble(Double aDoubleValue)
        throws CatalogException
    {
        //An insane value will be punished by an exception
        checkPreconditions(aDoubleValue, IAttribute.TYPE_ID_DOUBLE, false);

        // Type and consitency was checked by method interface an helper method
        // Lets try to set the value
        setValue(aDoubleValue);
    }

    /**
     * Synchronized: To avoid inconsistencies between threads of the
     * same session.
     */
    synchronized public void setAsURL(URL aURLValue)
        throws CatalogException
    {
        //An insane value will be punished by an exception
        checkPreconditions(aURLValue, IAttribute.TYPE_ID_URL, false);

        // Type and consitency was checked by method interface an helper method
        // Lets try to set the value
        setValue(aURLValue);
    }

    /**
     * Synchronized: To avoid inconsistencies between threads of the
     * same session.
     */
    synchronized public void addAsString(String aStringValue)
        throws CatalogException
    {
        //An insane value will be punished by an exception
        checkPreconditions(aStringValue, IAttribute.TYPE_ID_STRING, true);

        // Type and consitency was checked by method interface an helper method
        // Lets try to set the value
        addValue(aStringValue);
    }

    /**
     * Synchronized: To avoid inconsistencies between threads of the
     * same session.
     */
    synchronized public void addAsInteger(Integer aIntValue)
        throws CatalogException
    {
        //An insane value will be punished by an exception
        checkPreconditions(aIntValue, IAttribute.TYPE_ID_INTEGER, true);

        // Type and consitency was checked by method interface an helper method
        // Lets try to set the value
        addValue(aIntValue);
    }

    /**
     * Synchronized: To avoid inconsistencies between threads of the
     * same session.
     */
    synchronized public void addAsLong(Long aIntValue)
        throws CatalogException
    {
        //An insane value will be punished by an exception
        checkPreconditions(aIntValue, IAttribute.TYPE_ID_LONG, true);

        // Type and consitency was checked by method interface an helper method
        // Lets try to set the value
        addValue(aIntValue);
    }

    /**
     * Synchronized: To avoid inconsistencies between threads of the
     * same session.
     */
    synchronized public void addAsBoolean(Boolean aBooleanValue)
        throws CatalogException
    {
        //An insane value will be punished by an exception
        checkPreconditions(aBooleanValue, IAttribute.TYPE_ID_BOOLEAN, true);

        // Type and consitency was checked by method interface an helper method
        // Lets try to set the value
        addValue(aBooleanValue);
    }

    /**
     * Synchronized: To avoid inconsistencies between threads of the
     * same session.
     */
    synchronized public void addAsObject(Object anObjectValue)
        throws CatalogException
    {
        //An insane value will be punished by an exception
        checkPreconditions(anObjectValue, IAttribute.TYPE_ID_OBJECT, true);

        // Type and consitency was checked by method interface an helper method
        // Lets try to set the value
        addValue(anObjectValue);
    }

    /**
     * Synchronized: To avoid inconsistencies between threads of the
     * same session.
     */
    synchronized public void addAsInterval(IInterval anIntervalValue)
        throws CatalogException
    {
        //An insane value will be punished by an exception
        checkPreconditions(anIntervalValue, IAttribute.TYPE_ID_INTERVAL, true);

        // Type and consitency was checked by method interface an helper method
        // Lets try to set the value
        addValue(anIntervalValue);
    }

    /**
     * Synchronized: To avoid inconsistencies between threads of the
     * same session.
     */
    synchronized public void addAsDate(Date aDateValue)
        throws CatalogException
    {
        //An insane value will be punished by an exception
        checkPreconditions(aDateValue, IAttribute.TYPE_ID_DATE, true);

        // Type and consitency was checked by method interface an helper method
        // Lets try to set the value
        addValue(aDateValue);
    }

    /**
     * Synchronized: To avoid inconsitencies between threads of the
     * same session.
     */
    synchronized public void addAsDouble(Double aDoubleValue)
        throws CatalogException
    {
        //An insane value will be punished by an exception
        checkPreconditions(aDoubleValue, IAttribute.TYPE_ID_DOUBLE, true);

        // Type and consitency was checked by method interface an helper method
        // Lets try to set the value
        addValue(aDoubleValue);
    }

    /**
     * Synchronized: To avoid inconsistencies between threads of the
     * same session.
     */
    synchronized public void addAsURL(URL aURLValue)
        throws CatalogException
    {
        //An insane value will be punished by an exception
        checkPreconditions(aURLValue, IAttribute.TYPE_ID_URL, true);

        // Type and consitency was checked by method interface an helper method
        // Lets try to set the value
        addValue(aURLValue);
    }

    /**
     * Little internal helper to make sure the settings are consistent with the
     * preconditions neccessary
     * A: no null values
     * B: the type must be the type of the attribute
     * C: adding only allowed for multivalued attributes.
     *
     */
    private void checkPreconditions(Object data, String type, boolean add)
        throws CatalogException
    {
        boolean precondition = ((data != null) &&
            (getAttribute().getType().intern() == type.intern()) &&
            (!add || getAttribute().isMultiValued()));
        if(!precondition)
        {
            String theMessage = getRoot()
                .getMessageResources()
                .getMessage(ICatalogMessagesKeys.PCAT_ERR_WRONG_TYPE_NULL);
            throw new CatalogException(theMessage);
        }

        return;
    }

    /**
     *  Handles the logging and State logic
     *  Not synchronized only called via synchronized methods.
     *
     *  @param theNewValue type and consitency checkeing was done before
     */
    private void setValue(Object aNewValue)
        throws CatalogException
    {
        EditableCatalog theEditor = (EditableCatalog)getRoot();
        Catalog theCataToEdit = ((EditableCatalog)getRoot()).getCatalog();

        // We are deleted! Nosense actions are punished by an exception
        if(isDeleted())
        {
            String theMessage = theCataToEdit.getMessageResources().getMessage(
                ICatalogMessagesKeys.PCAT_ERR_IS_DELEDED);
            throw new CatalogException(theMessage);
        }

        // Now get the lock, if new we get it any way
        if(!lock())
        {
            String theMessage =
                theCataToEdit.getMessageResources().getMessage(
                 ICatalogMessagesKeys.PCAT_ERR_LOCK);
            throw new CatalogException(theMessage);
        }

        /************************ without cmd processor ***********************
        setAsObjectInternal(aNewValue);

        //if not new we are changed now
        setStateChanged();

        return;
        ***********************************************************************/
        CatalogCmdProcessor processor = theEditor.getCmdProcessor();
        CatalogCmd command =
            processor.createCmdAttributeValueEdit(this, aNewValue, false);
        if(!processor.doCmd(command))
            throw new CatalogException("");
            /**@todo real text has to be added **/
        return;
    }

    /**
     *  Handles the logging and State logic
     *  Not synchronized only called via synchronized methods.
     *
     *  @param theNewValue type and consistency checkeing was done before
     */
    private void addValue(Object aNewValue)
        throws CatalogException
    {
        EditableCatalog theEditor = (EditableCatalog)getRoot();
        Catalog theCataToEdit = ((EditableCatalog)getRoot()).getCatalog();

        // We are deleted! Nosense actions are punished by an exception
        if(isDeleted())
        {
            String theMessage = theCataToEdit.getMessageResources().getMessage(
                ICatalogMessagesKeys.PCAT_ERR_IS_DELEDED);
            throw new CatalogException(theMessage);
        }

        // Now get the lock, if new we get it any way
        if(!lock())
        {
            String theMessage =
                theCataToEdit.getMessageResources().getMessage(
                 ICatalogMessagesKeys.PCAT_ERR_LOCK);
            throw new CatalogException(theMessage);
        }

        /************************ without cmd processor ***********************
        addAsObjectInternal(aNewValue);

        setStateChanged();

        return;
        ***********************************************************************/

        CatalogCmdProcessor processor = theEditor.getCmdProcessor();
        CatalogCmd command =
            processor.createCmdAttributeValueEdit(this, aNewValue, true);
        if(!processor.doCmd(command))
            throw new CatalogException(""); /**@todo real text has to be added **/
        return;
    }

/* ****************** IComponentEventListener ******************************* */

    synchronized public void componentChanged(IComponentChangedEvent anEvent)
    {
        // do what so ever is necessary for super do it
        IComponent theChangedComponent = anEvent.getComponent();
        if(theChangedComponent.queryInterface(CatalogComponentTypes.I_ATTRIBUTE)!=null)
            super.componentChanged(anEvent);
    }

    synchronized public void detailCreated(IDetailCreatedEvent p0)
    {
        /**@todo implement listener interface */
        ;
    }

    synchronized public void detailDeleted(IDetailDeletedEvent p0)
    {
        /**@todo implement listener interface */
        ;
    }
}
