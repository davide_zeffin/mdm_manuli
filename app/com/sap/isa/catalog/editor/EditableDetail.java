/*****************************************************************************
  Copyright (c) 2000-2001, SAPMarkets Europe GmbH, Germany, All rights reserved.
  Created:      02 August 2001

  $Id: //sap/ESALES_base/30_SP_COR/src/_pcatAPI/java/com/sap/isa/catalog/editor/EditableDetail.java#2 $
  $Revision: #2 $
  $Change: 129221 $
  $DateTime: 2003/05/15 17:20:58 $
*****************************************************************************/
package com.sap.isa.catalog.editor;

import com.sap.isa.catalog.ICatalogMessagesKeys;
import com.sap.isa.catalog.boi.CatalogException;
import com.sap.isa.catalog.boi.IComponent;
import com.sap.isa.catalog.boi.IComponentChangedEvent;
import com.sap.isa.catalog.boi.IComponentEventListener;
import com.sap.isa.catalog.boi.IDetail;
import com.sap.isa.catalog.boi.IDetailCreatedEvent;
import com.sap.isa.catalog.boi.IDetailDeletedEvent;
import com.sap.isa.catalog.boi.IEditableDetail;
import com.sap.isa.catalog.catalogcmd.CatalogCmd;
import com.sap.isa.catalog.catalogcmd.CatalogCmdProcessor;
import com.sap.isa.catalog.impl.Catalog;
import com.sap.isa.catalog.impl.CatalogComponent;
import com.sap.isa.catalog.impl.CatalogDetail;
import com.sap.isa.catalog.impl.CatalogLockManager;

/**
 *  An editor for a detail.
 *
 *  @version     1.0
 */
public class EditableDetail
    extends CatalogDetail
    implements  IEditableDetail,
                IComponentEventListener
{
    /*
     *  EditableDetail implements Serializable:
     *  Fields:
     *  theDetailToEdit     :   implements Serializable
     */
    private CatalogDetail   theDetailToEdit;//null if new

    /**
     * To create new editors for existing details. The constructor pulls the
     * internal state from the component to be edit. The construction process
     * via {@link EditableCatalogBuilder}, {@link CatalogCleanerVisitor},
     * e.g. {@link EditableItem} enforces the preconditions beween the
     * parameters. The state after creation is LOADED.
     *
     * @param   theEditableParent a component that is about to be edit.
     *          i.e. the editable wrapper of the parent component
     *          of aDetailToEdit
     * @param   aDetailToEdit the component that is edited via this.
     */
    EditableDetail( CatalogComponent theEditableParent,
                    CatalogDetail aDetailToEdit)
    {
        super(theEditableParent);

        this.theDetailToEdit = aDetailToEdit;

        //take care we are informed of changes
        theDetailToEdit.addComponentEventListener(this);

        setState(IComponent.State.LOADED);

        //init the editor with the data from the thing to edit
        setNameInternal(aDetailToEdit.getName());
        setAsStringInternal(aDetailToEdit.getAllAsString());
    }

    /**
     * To create new editors for new details. The construction process
     * via {@link EditableCatalogBuilder}, {@link CatalogCleanerVisitor},
     * e.g. {@link EditableCatalog} enforces the preconditions beween the
     * parameters. The state after creation is NEW.
     *
     * @param   theEditableParent a component that is about to get a new detail.
     * @param   aName a name for the new detail should be unique for the
     *          theEditableParent.
     */
    EditableDetail( CatalogComponent theEditableParent,
                    String aName)
    {
        super(theEditableParent);
        setState(IComponent.State.NEW);
        setNameInternal(aName);
    }

    /**
     * The detail we are the editor for!
     */
    CatalogDetail getDetail()
    {
        return theDetailToEdit;
    }

    /**
     * The detail we are the editor for!
     */
    void setDetail(CatalogDetail aDetailRef)
    {
        theDetailToEdit=aDetailRef;

        //take care we are informed of changes
        theDetailToEdit.addComponentEventListener(this);

        return;
    }

/* ************************* IEditableComponent ***************************** */

    public IComponent getComponentReference()
    {
        return this.getDetailReference();
    }

    /**
     * Synchronization: To avoid inconsistancies between threads "of
     * the same session"!
     */
    synchronized public boolean lock()
    {
        //If null there is realy nothing to lock
        if(theDetailToEdit!=null)
        {
            //Now get the lock
            EditableCatalog theEditor = (EditableCatalog)getRoot();
            CatalogLockManager theLocker =
                CatalogLockManager.getInstanceFor(theEditor.getCatalog());
            return theLocker.getLock(   theDetailToEdit,
                                        false,
                                        theEditor.getUser());
        }
        else
            return true;
    }

    /**
     * Synchronization: To avoid inconsistancies between threads "of
     * the same session"!
     */
    synchronized public void releaseLock()
    {
        //If null there is realy nothing to unlock
        if(theDetailToEdit!=null)
        {
            //Now release the lock
            EditableCatalog theEditor = (EditableCatalog)getRoot();
            CatalogLockManager theLocker =
                CatalogLockManager.getInstanceFor(theEditor.getCatalog());
            theLocker.releaseLock(theDetailToEdit,
                                  false,
                                  theEditor.getUser());
        }
        return;
    }

/* ************************* IEditableDetail ******************************** */

    public IDetail getDetailReference()
    {
        return theDetailToEdit;
    }


    /**
     * Synchronization: To avoid inconsistancies between threads "of
     * the same session"!
     */
    synchronized public void setName(String aNewName)
        throws CatalogException
    {
        EditableCatalog theEditor = (EditableCatalog)getRoot();
        Catalog theCataToEdit = ((EditableCatalog)getRoot()).getCatalog();

        //We are deleted! Nosense actions are punished by an exception
        if(isDeleted())
        {
            String theMessage = theCataToEdit.getMessageResources().getMessage(
                ICatalogMessagesKeys.PCAT_ERR_IS_DELEDED);
            throw new CatalogException(theMessage);
        }

        if(!lock())//if new we get it any way
        {
            String theMessage = theCataToEdit.getMessageResources()
                .getMessage(ICatalogMessagesKeys.PCAT_ERR_LOCK);
            throw new CatalogException(theMessage);
        }
        /************** without cmd processor *********************************
        setNameInternal(aNewName);

        //if not new we are changed now
        setStateChanged();

        return;
        ***********************************************************************/
        CatalogCmdProcessor processor = theEditor.getCmdProcessor();
        CatalogCmd command =
            processor.createCmdDetailEdit(this, aNewName, null, false);
        if(!processor.doCmd(command))
            throw new CatalogException("");
            /**@todo real text has to be added **/
        return;
    }

    /**
     * Synchronization: To avoid inconsistancies between threads "of
     * the same session"!
     */
    synchronized public void setAsString(String aValue)
        throws CatalogException
    {
        EditableCatalog theEditor = (EditableCatalog)getRoot();
        Catalog theCataToEdit = ((EditableCatalog)getRoot()).getCatalog();

        // We are deleted! Nosense actions are punished by an exception
        if(isDeleted())
        {
            String theMessage = theCataToEdit.getMessageResources().getMessage(
                ICatalogMessagesKeys.PCAT_ERR_IS_DELEDED);
            throw new CatalogException(theMessage);
        }

        if(!lock())//if new we get it any way
        {
            String theMessage =
                theCataToEdit.getMessageResources().getMessage(
                 ICatalogMessagesKeys.PCAT_ERR_LOCK);
            throw new CatalogException(theMessage);
        }
        /*********** without cmd processor *************************************
        setAsStringInternal(aValue);

        //if not new we are changed now
        setStateChanged();

        return;
        ***********************************************************************/
        CatalogCmdProcessor processor = theEditor.getCmdProcessor();
        CatalogCmd command =
            processor.createCmdDetailEdit(this, null, aValue, false);
        if(!processor.doCmd(command))
            throw new CatalogException("");
            /**@todo real text has to be added **/
        return;
    }

    /**
     * Synchronization: To avoid inconsistancies between threads "of
     * the same session"!
     */
    synchronized public void addAsString(String aValue)
        throws CatalogException
    {
        EditableCatalog theEditor = (EditableCatalog)getRoot();
        Catalog theCataToEdit = ((EditableCatalog)getRoot()).getCatalog();

        //We are deleted! Nosense actions are punished by an exception
        if(isDeleted())
        {
            String theMessage = theCataToEdit.getMessageResources().getMessage(
                ICatalogMessagesKeys.PCAT_ERR_IS_DELEDED);
            throw new CatalogException(theMessage);
        }

        if(!lock())//if new we get it any way
        {
            String theMessage = theCataToEdit.getMessageResources()
                .getMessage(ICatalogMessagesKeys.PCAT_ERR_LOCK);
            throw new CatalogException(theMessage);
        }

        /*********************** without cmd processor *************************
        addAsStringInternal(aValue);

        //if not new we are changed now
        setStateChanged();

        return;
        ***********************************************************************/
        CatalogCmdProcessor processor = theEditor.getCmdProcessor();
        CatalogCmd command =
            processor.createCmdDetailEdit(this, null, aValue, true);
        if(!processor.doCmd(command))
            throw new CatalogException("");
            /**@todo real text has to be added **/
        return;
    }

/* ************************* IComponentEventListener ************************ */

    public void componentChanged(IComponentChangedEvent anEvent)
    {
        //changed i.e. the ref has changed
        //update your state from the ref
        if(anEvent.getComponent().getState()!=IComponent.State.DELETED)
        {
            setNameInternal(this.theDetailToEdit.getName());
            setAsStringInternal(this.theDetailToEdit.getAllAsString());
        }

        //deleted i.e. your ref has been deleted
        //set your self deleted
        if(anEvent.getComponent().getState()==IComponent.State.DELETED)
        {
            //set your state to deleted the "parent" i.e. the catalog or another
            //parent type will remove you from his list
            this.setStateDeleted();
        }
    }

    public void detailCreated(IDetailCreatedEvent p0)
    {
        throw new java.lang.UnsupportedOperationException(
            "Creation of details for details is not supported");
    }

    public void detailDeleted(IDetailDeletedEvent anDeleteEvent)
    {
        throw new java.lang.UnsupportedOperationException(
            "Creation of details for details is not supported");
    }
}
