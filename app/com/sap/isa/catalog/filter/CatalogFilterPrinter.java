/*****************************************************************************
  Copyright (c) 2000, SAPMarkets Europe GmbH, Germany, All rights reserved.
  Created:      03 April 2001

  $Id: //sap/ESALES_base/30_SP_COR/src/_pcatAPI/java/com/sap/isa/catalog/filter/CatalogFilterPrinter.java#2 $
  $Revision: #2 $
  $Change: 129221 $
  $DateTime: 2003/05/15 17:20:58 $
*****************************************************************************/
package com.sap.isa.catalog.filter;

// misc imports


/**
 * This class transforms the filter expression into a string representation.
 * @version     1.0
 */
class CatalogFilterPrinter extends CatalogFilterVisitor
{
    /*
     *  Only transient objects no need to implement Serializable:
     */

    private StringBuffer theOutput = null;

    /**
     * Creates a new instance of the printer of a filter expression tree.
     *
     */
    CatalogFilterPrinter() {
        // no message resources are needed
        super(null);
    }

    /**
     * Transforms the expression tree into a String.
     *
     * @param rootFilter     the root node of the expression tree
     * @return               the string representation of the tree
     */
    synchronized String transform(CatalogFilter rootFilter) {
        String result = "";

        if(rootFilter != null) {
            try {
                theOutput = new StringBuffer();
                // since no attributes and message resource are available the start
                // method is not used!
                theRootFilter = rootFilter;
                theAttributes = null;
                getRootFilter().assign(this);
                theRootFilter = null;
                result = theOutput.toString();
            }
            catch(CatalogFilterInvalidException ex) {
            result = "";
            }
        }

        return result;
    }

    protected void visitAttrContainFilter(CatalogFilterAttrContain filterNode)
            throws CatalogFilterInvalidException {
        // transformation
        theOutput.append(filterNode.getName());
        theOutput.append('=');
        theOutput.append("containing(");
        theOutput.append(filterNode.getPattern());
        theOutput.append(")");
    }

    protected void visitAttrEqualFilter(CatalogFilterAttrEqual filterNode)
            throws CatalogFilterInvalidException {
        // transformation
        theOutput.append(filterNode.getName());
        theOutput.append('=');
        theOutput.append('"');
        theOutput.append(filterNode.getValue());
        theOutput.append('"');
    }

    protected void visitAttrLinguisticFilter(CatalogFilterAttrLinguistic filterNode)
            throws CatalogFilterInvalidException {
        // transformation
        theOutput.append(filterNode.getName());
        theOutput.append('=');
        theOutput.append("linguistic(");
        theOutput.append(filterNode.getPattern());
        theOutput.append(")");
    }

    protected void visitAttrFuzzyFilter(CatalogFilterAttrFuzzy filterNode)
            throws CatalogFilterInvalidException {
        // transformation
        theOutput.append(filterNode.getName());
        theOutput.append('=');
        theOutput.append("fuzzy(");
        theOutput.append(filterNode.getPattern());
        theOutput.append(")");
    }

    protected void visitNotFilter(CatalogFilterNot filterNode)
            throws CatalogFilterInvalidException {
        // transformation
        theOutput.append("NOT ( ");
        CatalogFilter operand = (CatalogFilter) filterNode.getOperands().get(0);
        operand.assign(this);
        theOutput.append(" )");
    }

    protected void visitOrFilter(CatalogFilterOr filterNode)
            throws CatalogFilterInvalidException {
        // transformation
        theOutput.append("( ");
        CatalogFilter lOperand = (CatalogFilter) filterNode.getOperands().get(0);
        lOperand.assign(this);
        theOutput.append(" OR ");
        CatalogFilter rOperand = (CatalogFilter) filterNode.getOperands().get(1);
        rOperand.assign(this);
        theOutput.append(" )");
    }

    protected void visitAndFilter(CatalogFilterAnd filterNode)
            throws CatalogFilterInvalidException {
        // transformation
        theOutput.append("( ");
        CatalogFilter lOperand = (CatalogFilter) filterNode.getOperands().get(0);
        lOperand.assign(this);
        theOutput.append(" AND ");
        CatalogFilter rOperand = (CatalogFilter) filterNode.getOperands().get(1);
        rOperand.assign(this);
        theOutput.append(" )");
    }
    
    protected void visitAttrBetweenFilter(CatalogFilterAttrBetween filterNode)
            throws CatalogFilterInvalidException {
        // transformation
        theOutput.append("( \"");
        theOutput.append(filterNode.getLowValue());
        theOutput.append("\" <= ");
        theOutput.append(filterNode.getName());
        theOutput.append(" <= \"");
        theOutput.append(filterNode.getHighValue());
        theOutput.append("\" )");
    }


}