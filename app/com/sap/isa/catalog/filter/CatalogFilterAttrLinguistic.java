/*****************************************************************************
  Copyright (c) 2000, SAPMarkets Europe GmbH, Germany, All rights reserved.

  $Id: //sap/ESALES_base/30_SP_COR/src/_pcatAPI/java/com/sap/isa/catalog/filter/CatalogFilterAttrLinguistic.java#3 $
  $Revision: #3 $
  $Change: 144303 $
  $DateTime: 2003/08/22 20:18:21 $
*****************************************************************************/
package com.sap.isa.catalog.filter;

import java.util.Properties;

/**
 * This class combines an attribute with a given pattern which the actual value
 * of the attribute has to match. The strategy for determining if the actual value
 * matches the given value is a linguistic search.
 *
 * @version     1.0
 */
public class CatalogFilterAttrLinguistic extends CatalogFilter
{
    /*
     *  CatalogFilter implements Serializable:
     *  Fields:
     *  theName     : no problem
     *  thePatern   : no problem
     */
    private String theName;
    private String thePattern;

    /**
     * Creates a new instance.
     *
     * @param name     name of the attribute
     * @param pattern  pattern which the actual value has to match
     */
    CatalogFilterAttrLinguistic(String name, String pattern) {
        super(null);
        theName = name;
        thePattern = pattern;
    }

    /**
     * Creates a new instance.
     *
     * @param name     name of the attribute
     * @param pattern  pattern which the actual value has to match
     * @param params   a list of properties to configure the linguistic search
     */
    CatalogFilterAttrLinguistic(String name, String pattern, Properties params) {
        super(params);
        theName = name;
        thePattern = pattern;
    }

    /**
     * Gets the name of the attribute.
     *
     * @return  the attribute name
     */
    public String getName() {
        return theName;
    }

    /**
     * Gets the pattern which the value of the attribute should match.
     *
     * @return  the pattern
     */
    public String getPattern() {
        return thePattern;
    }

    public void assign(CatalogFilterVisitor visitor)
            throws CatalogFilterInvalidException {
        visitor.visitAttrLinguisticFilter(this);
    }
}