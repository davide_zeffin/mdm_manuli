/*****************************************************************************
  Copyright (c) 2000, SAPMarkets Europe GmbH, Germany, All rights reserved.
  Created:      03 April 2001

  $Id: //sap/ESALES_base/30_SP_COR/src/_pcatAPI/java/com/sap/isa/catalog/filter/CatalogFilterFactory.java#3 $
  $Revision: #3 $
  $Change: 150872 $
  $DateTime: 2003/09/29 09:18:51 $
*****************************************************************************/

package com.sap.isa.catalog.filter;

import java.io.Serializable;
import java.util.Properties;

import com.sap.isa.catalog.boi.IFilter;

/**
 * This class is the factory instance for all filters which can be associated
 * with a query statement. All filter expressions have to be created via
 * the methods of this class.<BR/><BR/>
 *
 * <B>Example usage:</B> A query for all items which conform to the following
 * condition is performed [DESCRIPTION, TEXT and NO are item attributes]:
 * <P style=margin-left:10px>(DESCRIPTION == "myDescription" OR TEXT == "myText") AND NO == "5"</P>
 *
 * <code><PRE>
 * // create the statement
 * myFilteredStatement = myCatalog.createQueryStatement();
 *
 * // get factory instance
 * CatalogFilterFactory fact = CatalogFilterFactory.getInstance();
 *
 * // create filter expression
 * // 1<SUP>st</SUP>: assign values to the attributes
 * IFilter aDesc = fact.createAttrEqualValue("DESCRIPTION", "myDescription");
 * IFilter aText = fact.createAttrEqualValue("TEXT", "myText");
 * IFilter aNo   = fact.createAttrEqualVAlue("NO", "5");
 *
 * // 2<SUP>nd</SUP>: create boolean expressions - the precedence is represented
 * // through the nesting of the filter expressions
 * IFilter descORtext = fact.createOr(aDesc, aText);     // OR statement
 * IFilter expr       = fact.createAnd(descORtext, aNo); // AND statement
 *
 * // set filter
 * myFilteredStatement.setStatement(expr, null, null);
 *
 * // create query
 * myQuery = myCatalog.createQuery(myFilteredStatement);
 *
 * // submit query
 * myQuery.submit();
 * </code></PRE>
 *
 * @version     1.0
 */
public class CatalogFilterFactory implements Serializable
{
    private static CatalogFilterFactory theInstance = null;

    private CatalogFilterFactory() {
    }

    /**
     * Gets the singleton instance of the filter factory.
     *
     * @return  the singleton instance
     */
    public static CatalogFilterFactory getInstance() {
        if (theInstance == null) {
            theInstance = new CatalogFilterFactory();
        }
        return theInstance;
    }

    /**
     * Creates an AND Filter expression. If one operand is <code>null</code> no
     * filter instance will be created but <code>null</code> will be returned.
     *
     * @param lOperand  the left operand of the AND expression
     * @param rOperand  the right operand of the AND expression
     * @return          the created And filter expression
     */
    public IFilter createAnd(IFilter lOperand, IFilter rOperand) {
        IFilter filter = null;

        if(lOperand != null && rOperand != null) {
            filter =  new CatalogFilterAnd(
                            (CatalogFilter) lOperand,
                            (CatalogFilter) rOperand);
        }

        return filter;
    }

    /**
     * Creates an OR Filter expression. If one operand is <code>null</code> no
     * filter instance will be created but <code>null</code> will be returned.
     *
     * @param lOperand  the left operand of the OR expression
     * @param rOperand  the right operand of the OR expression
     * @return          the created Or filter expression
     */
    public IFilter createOr(IFilter lOperand, IFilter rOperand) {
        IFilter filter = null;

        if(lOperand != null && rOperand != null) {
            filter = new CatalogFilterOr(
                            (CatalogFilter) lOperand,
                            (CatalogFilter) rOperand);
        }

        return filter;
    }

    /**
     * Creates a NOT Filter expression. If the operand is <code>null</code> no
     * filter instance will be created but <code>null</code> will be returned.
     *
     * @param operand   the unary operand of the NOT expression
     * @return          the created Not filter expression
     */
    public IFilter createNot(IFilter operand) {
        IFilter filter = null;

        if(operand != null) {
            filter = new CatalogFilterNot((CatalogFilter)operand);
        }

        return filter;
    }

    /**
     * Creates a Filter expression which assigns a specific value to a given
     * attribute.
     *
     * @param name   the name of the attribute
     * @param value  the value of the attribute
     * @return       the created AttrEqual filter expression
     */
    public IFilter createAttrEqualValue(String name, String value) {
        return new CatalogFilterAttrEqual(name, value);
    }

    /**
     * Creates a Filter expression for an attribute which contain the given
     * value.
     *
     * @param name   the name of the attribute
     * @param value  the value of the attribute
     * @return       the created AttrNotEqual filter expression
     */
    public IFilter createAttrContainValue(String name, String value) {
        return new CatalogFilterAttrContain(name, value);
    }

    /**
     * Creates a Filter expression for an attribute which matches the given
     * value. The strategy to determine if the actual value of the attribute
     * matches the given value is the fuzzy search method.
     *
     * @param name   the name of the attribute
     * @param value  the value of the attribute
     * @return       the created AttrFuzzy filter expression
     */
    public IFilter createAttrFuzzyValue(String name, String value) {
        return new CatalogFilterAttrFuzzy(name, value);
    }

   /**
    * Creates a Filter expression for an attribute which matches the given
    * value. The strategy to determine if the actual value of the attribute
    * matches the given value is the fuzzy search method.
    *
    * @param name   the name of the attribute
    * @param value  the value of the attribute
    * @param params a list of properties to configure the fuzzy search
    * @return       the created AttrFuzzy filter expression
    */
    public IFilter createAttrFuzzyValue(String name, String value, Properties params) {
        return new CatalogFilterAttrFuzzy(name, value, params);
    }

    /**
     * Creates a Filter expression for an attribute which matches the given
     * value. The strategy to determine if the actual value of the attribute
     * matches the given value is the linguistic search method.
     *
     * @param name   the name of the attribute
     * @param value  the value of the attribute
     * @return       the created AttrLinguistic filter expression
     */
    public IFilter createAttrLinguisticValue(String name, String value) {
        return new CatalogFilterAttrLinguistic(name, value);
    }


    /**
     * Creates a Filter expression for an attribute which matches the given
     * value. The strategy to determine if the actual value of the attribute
     * matches the given value is the linguistic search method.
     *
     * @param name   the name of the attribute
     * @param value  the value of the attribute
     * @param params a list of properties to configure the linguistic search
     * @return       the created AttrLinguistic filter expression
     */
    public IFilter createAttrLinguisticValue(String name, String value, Properties params) {
        return new CatalogFilterAttrLinguistic(name, value, params);
    }

    /**
     * Creates a Filter expression which assigns a specific low Value and high value to a given
     * attribute.
     *
     * @param name      the name of the attribute
     * @param lowValue  the low value of the attribute
     * @param highValue the high value of the attribute
     * @return          the created AttrBetween filter expression
     */
    public IFilter createAttrBetweenValue(String name, String lowValue, String highValue) {
        return new CatalogFilterAttrBetween(name, lowValue, highValue);
    }


}