/*****************************************************************************
  Copyright (c) 2000, SAPMarkets Europe GmbH, Germany, All rights reserved.

  $Id: //sap/ESALES_base/30_SP_COR/src/_pcatAPI/java/com/sap/isa/catalog/filter/CatalogFilterAttrContain.java#3 $
  $Revision: #3 $
  $Change: 144303 $
  $DateTime: 2003/08/22 20:18:21 $
*****************************************************************************/
package com.sap.isa.catalog.filter;

/**
 * This class combines an attribute with a given pattern which the actual value
 * of the attribute has to match.
 *
 * @version     1.0
 */
public class CatalogFilterAttrContain extends CatalogFilter
{
    /*
     *  CatalogFilter implements Serializable:
     *  Fields:
     *  theName     : no problem
     *  thePatern   : no problem
     */
    private String theName;
    private String thePattern;

    /**
     * Creates a new instance.
     *
     * @param name     name of the attribute
     * @param pattern  pattern which the actual value has to match
     */
    CatalogFilterAttrContain(String name, String pattern) {
        super(null);
        theName = name;
        thePattern = pattern;
    }

    /**
     * Gets the name of the attribute.
     *
     * @return  the attribute name
     */
    public String getName() {
        return theName;
    }

    /**
     * Gets the pattern which the value of the attribute should match.
     *
     * @return  the pattern
     */
    public String getPattern() {
        return thePattern;
    }

    public void assign(CatalogFilterVisitor visitor)
            throws CatalogFilterInvalidException {
        visitor.visitAttrContainFilter(this);
    }
}