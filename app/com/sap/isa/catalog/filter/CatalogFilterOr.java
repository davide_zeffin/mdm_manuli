/*****************************************************************************
  Copyright (c) 2000, SAPMarkets Europe GmbH, Germany, All rights reserved.
  Created:      03 April 2001

  $Id: //sap/ESALES_base/30_SP_COR/src/_pcatAPI/java/com/sap/isa/catalog/filter/CatalogFilterOr.java#2 $
  $Revision: #2 $
  $Change: 129221 $
  $DateTime: 2003/05/15 17:20:58 $
*****************************************************************************/
package com.sap.isa.catalog.filter;

/**
 * This class combines two given filter expressions with the boolean OR
 * semantics.
 * @version     1.0
 */
public class CatalogFilterOr extends CatalogFilterComposite
{
    /*
     *  CatalogFilter implements Serializable:
     */

    /**
     * Creates a new instance. The instantiation should be performed via the
     * <code>CatalogFilterFactory</code>.
     *
     * @param lOperand  the left operand
     * @param rOperand  the right operand
     */
    CatalogFilterOr(CatalogFilter lOperand, CatalogFilter rOperand) {
        this.setLeftOperand(lOperand); // left operand has to be set first
        this.setRightOperand(rOperand);
    }

    public void assign(CatalogFilterVisitor visitor)
            throws CatalogFilterInvalidException {
        visitor.visitOrFilter(this);
    }
}