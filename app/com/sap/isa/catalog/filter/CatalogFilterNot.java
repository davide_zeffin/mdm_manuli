/*****************************************************************************
  Copyright (c) 2000, SAPMarkets Europe GmbH, Germany, All rights reserved.
  Created:      03 April 2001

  $Id: //sap/ESALES_base/30_SP_COR/src/_pcatAPI/java/com/sap/isa/catalog/filter/CatalogFilterNot.java#2 $
  $Revision: #2 $
  $Change: 129221 $
  $DateTime: 2003/05/15 17:20:58 $
*****************************************************************************/
package com.sap.isa.catalog.filter;

/**
 * This class combines the given filter expression with the boolean NOT
 * semantics.
 * @version     1.0
 */
public class CatalogFilterNot extends CatalogFilterComposite
{
    /*
     *  CatalogFilter implements Serializable:
     */

    /**
     * Creates a new instance. The instantiation should be performed via the
     * <code>CatalogFilterFactory</code>.
     *
     * @param operand  the operand
     */
    CatalogFilterNot(CatalogFilter operand) {
        this.addOperand(operand);
    }

    public void assign(CatalogFilterVisitor visitor)
            throws CatalogFilterInvalidException {
        visitor.visitNotFilter(this);
    }
}