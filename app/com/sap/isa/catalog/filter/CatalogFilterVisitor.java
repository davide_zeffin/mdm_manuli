/*****************************************************************************
  Copyright (c) 2000, SAPMarkets Europe GmbH, Germany, All rights reserved.
  Created:      03 April 2001

  $Id: //sap/ESALES_base/30_SP_COR/src/_pcatAPI/java/com/sap/isa/catalog/filter/CatalogFilterVisitor.java#2 $
  $Revision: #2 $
  $Change: 127411 $
  $DateTime: 2003/04/30 17:20:32 $
*****************************************************************************/
package com.sap.isa.catalog.filter;

// catalog imports
import java.util.List;

import org.apache.struts.util.MessageResources;

import com.sap.isa.catalog.ICatalogMessagesKeys;
import com.sap.isa.core.logging.IsaLocation;

/**
 * This class is the abstract parent class for all visitors of the expression
 * tree which was created from the filters.
 * @version     1.0
 */
public abstract class CatalogFilterVisitor
{
    // the logging instance
    private static IsaLocation theStaticLocToLog =
    IsaLocation.getInstance(CatalogFilterVisitor.class.getName());

    // messages
    private MessageResources theMessageResources;

    // root filter node of the expression
    protected CatalogFilter theRootFilter = null;

    // the attributes which are available in the query context
    protected List theAttributes = null;

    /**
     * Creates a new Visitor.
     *
     * @param messResources  the messages resources for the error messages
     */
    protected CatalogFilterVisitor(MessageResources messResources) {
        theMessageResources = messResources;
        // initialize the logging instance
    }

    /**
     * Returns the root node of the expression tree.
     *
     * @return  the root node
     */
    protected CatalogFilter getRootFilter() {
        return theRootFilter;
    }

    /**
     * Returns the message resources associated with the visitor instance.
     *
     * @return  message resources
     */
    protected MessageResources getMessageResources() {
        return theMessageResources;
    }

    /**
     * Visitor method for an <CODE>And</CODE> node in the filter expression.
     *
     * @param filterNode  the node to be visited
     * @exception CatalogFilterInvalidException error during evaluation of the expression
     */
    protected void visitAndFilter(CatalogFilterAnd filterNode)
            throws CatalogFilterInvalidException {
        if(theStaticLocToLog.isDebugEnabled()) {
            theStaticLocToLog.debug("Visiting And Filter ...");
        }
    }

    /**
     * Visitor method for an <CODE>Or</CODE> node in the filter expression.
      *
      * @param filterNode  the node to be visited
      * @exception CatalogFilterInvalidException error during evaluation of the expression
     */
    protected void visitOrFilter(CatalogFilterOr filterNode)
            throws CatalogFilterInvalidException {
        if(theStaticLocToLog.isDebugEnabled()) {
            theStaticLocToLog.debug("Visiting Or Filter ...");
        }
    }

    /**
     * Visitor method for an <CODE>Not</CODE> node in the filter expression.
     *
     * @param filterNode  the node to be visited
     * @exception CatalogFilterInvalidException error during evaluation of the expression
     */
    protected void visitNotFilter(CatalogFilterNot filterNode)
            throws CatalogFilterInvalidException {
        if(theStaticLocToLog.isDebugEnabled()) {
            theStaticLocToLog.debug("Visiting Not Filter ...");
        }
    }

    /**
     * Visitor method for an <CODE>AttrEqual</CODE> node in the filter expression.
     *
     * @param filterNode  the node to be visited
     * @exception CatalogFilterInvalidException error during evaluation of the expression
     */
    protected void visitAttrEqualFilter(CatalogFilterAttrEqual filterNode)
            throws CatalogFilterInvalidException {
        String name  = filterNode.getName();
        String value = filterNode.getValue();

        if(theStaticLocToLog.isDebugEnabled()) {
            theStaticLocToLog.debug("Visiting AttrEqual Filter (" + name + "=\"" + value + "\") ...");
        }

        if(!theAttributes.contains(name)) {
            String msg = getMessageResources().getMessage(
                   ICatalogMessagesKeys.PCAT_FILTER_INVALID_ATTR, name);
            throw new CatalogFilterInvalidException(msg);
        }
    }

    /**
     * Visitor method for an <CODE>AttrContain</CODE> node in the filter expression.
     *
     * @param filterNode  the node to be visited
     * @exception CatalogFilterInvalidException error during evaluation of the expression
     */
    protected void visitAttrContainFilter(CatalogFilterAttrContain filterNode)
            throws CatalogFilterInvalidException {
        String name    = filterNode.getName();
        String pattern = filterNode.getPattern();

        if(theStaticLocToLog.isDebugEnabled()) {
            theStaticLocToLog.debug("Visiting AttrContain Filter (" + name + "=\"" + pattern + "\") ...");
        }

        if(!theAttributes.contains(name)) {
            String msg = getMessageResources().getMessage(
                   ICatalogMessagesKeys.PCAT_FILTER_INVALID_ATTR, name);
            throw new CatalogFilterInvalidException(msg);
        }
    }

    /**
     * Visitor method for an <CODE>AttrFuzzy</CODE> node in the filter expression.
     *
     * @param filterNode  the node to be visited
     * @exception CatalogFilterInvalidException error during evaluation of the expression
     */
    protected void visitAttrFuzzyFilter(CatalogFilterAttrFuzzy filterNode)
            throws CatalogFilterInvalidException {
        String name    = filterNode.getName();
        String pattern = filterNode.getPattern();

        if(theStaticLocToLog.isDebugEnabled()) {
            theStaticLocToLog.debug("Visiting AttrFuzzy Filter (" + name + "=\"" + pattern + "\") ...");
        }

        if(!theAttributes.contains(name)) {
            String msg = getMessageResources().getMessage(
                   ICatalogMessagesKeys.PCAT_FILTER_INVALID_ATTR, name);
            throw new CatalogFilterInvalidException(msg);
        }
    }

    /**
     * Visitor method for an <CODE>AttrLinguistic</CODE> node in the filter expression.
     *
     * @param filterNode  the node to be visited
     * @exception CatalogFilterInvalidException error during evaluation of the expression
     */
    protected void visitAttrLinguisticFilter(CatalogFilterAttrLinguistic filterNode)
            throws CatalogFilterInvalidException {
        String name    = filterNode.getName();
        String pattern = filterNode.getPattern();

        if(theStaticLocToLog.isDebugEnabled()) {
            theStaticLocToLog.debug("Visiting AttrLinguistic Filter (" + name + "=\"" + pattern + "\") ...");
        }

        if(!theAttributes.contains(name)) {
            String msg = getMessageResources().getMessage(
                   ICatalogMessagesKeys.PCAT_FILTER_INVALID_ATTR, name);
            throw new CatalogFilterInvalidException(msg);
        }
    }

    /**
     * Starts the traversation of the expression tree. The traversation order of the
     * expression tree is determined by the concrete visitor implemenation.
     *
     * @param rootFilter  the root node of the expression tree
     * @param attributes  the attributes which are available in the query context
     * @exception CatalogFilterInvalidException error during evaluation of the expression
     */
    protected void start(CatalogFilter rootFilter, List attributes)
            throws CatalogFilterInvalidException {
        if (rootFilter == null) {
            String msg = getMessageResources().getMessage(
                   ICatalogMessagesKeys.PCAT_FILTER_INVALID_FILTER);
            throw new CatalogFilterInvalidException(msg);
        }

        if (attributes == null) {
            String msg = getMessageResources().getMessage(
                   ICatalogMessagesKeys.PCAT_FILTER_INVALID_ATTRS);
            throw new CatalogFilterInvalidException(msg);
        }

        theRootFilter = rootFilter;
        theAttributes = attributes;
        //Here the visitor itself starts the traversal of the tree!
        getRootFilter().assign(this);
        theRootFilter = null;
        theAttributes = null;
    }
    
    
    /**
     * Visitor method for an <CODE>AttrBetween</CODE> node in the filter expression.
     *
     * @param filterNode  the node to be visited
     * @exception CatalogFilterInvalidException error during evaluation of the expression
     */
    protected void visitAttrBetweenFilter(CatalogFilterAttrBetween filterNode)
            throws CatalogFilterInvalidException {
        String name  = filterNode.getName();
        String lowValue = filterNode.getLowValue();
        String highValue = filterNode.getHighValue();

        if(theStaticLocToLog.isDebugEnabled()) {
            theStaticLocToLog.debug("Visiting AttrBetween Filter ( \"" + lowValue + "\" <= "+ name + " <= \"" + highValue + "\") ...");
        }

        if(!theAttributes.contains(name)) {
            String msg = getMessageResources().getMessage(
                   ICatalogMessagesKeys.PCAT_FILTER_INVALID_ATTR, name);
            throw new CatalogFilterInvalidException(msg);
        }
    }

}
