/*****************************************************************************
  Copyright (c) 2008, SAPMarkets Europe GmbH, Germany, All rights reserved.

*****************************************************************************/
package com.sap.isa.catalog.filter;

/**
 * This class combines an attribute with a given value.
 *
 * @version     1.0
 */
public class CatalogFilterAttrBetween extends CatalogFilter
{
    /*
     *  CatalogFilter implements Serializable:
     *  Fields:
     *  theName     : no problem
     *  thePatern   : no problem
     */
    private String theName;
    private String theLowValue;
    private String theHighValue;

    /**
     * Creates a new instance.
     *
     * @param name     name of the attribute
     * @param pattern  value of the attribute
     */
    CatalogFilterAttrBetween(String name, String lowValue, String highValue) {
        super(null);
        theName = name;
        theLowValue = lowValue;
        theHighValue = highValue;
    }

    /**
     * Gets the name of the attribute.
     *
     * @return  the attribute name
     */
    public String getName() {
        return theName;
    }

    /**
     * Gets the low value which the attribute should contain.
     *
     * @return  the low value
     */
    public String getLowValue() {
        return this.theLowValue;
    }

    /**
     * Gets the high value which the attribute should contain.
     *
     * @return  the high value
     */
    public String getHighValue() {
        return this.theHighValue;
    }

    public void assign(CatalogFilterVisitor visitor)
            throws CatalogFilterInvalidException {
        visitor.visitAttrBetweenFilter(this);
    }
}