/*****************************************************************************
  Copyright (c) 2000, SAPMarkets Europe GmbH, Germany, All rights reserved.

  $Id: //sap/ESALES_base/30_SP_COR/src/_pcatAPI/java/com/sap/isa/catalog/filter/CatalogFilterAnd.java#3 $
  $Revision: #3 $
  $Change: 144303 $
  $DateTime: 2003/08/22 20:18:21 $
*****************************************************************************/
package com.sap.isa.catalog.filter;

/**
 * This class combines two given filter expressions with the boolean AND
 * semantics.
 *
 * @version     1.0
 */
public class CatalogFilterAnd extends CatalogFilterComposite
{
    /*
     *  CatalogFilter  implements Serializable!
     */

    /**
     * Creates a new instance. The instantiation should be performed via the
     * <code>CatalogFilterFactory</code>.
     *
     * @param lOperand  the left operand
     * @param rOperand  the right operand
     */
    CatalogFilterAnd(CatalogFilter lOperand, CatalogFilter rOperand) {
        this.setLeftOperand(lOperand); // left operand has to be set first
        this.setRightOperand(rOperand);
    }

    public void assign(CatalogFilterVisitor visitor)
            throws CatalogFilterInvalidException {
        visitor.visitAndFilter(this);
    }
}