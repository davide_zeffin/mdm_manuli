/*****************************************************************************
  Copyright (c) 2000, SAPMarkets Europe GmbH, Germany, All rights reserved.

  $Id: //sap/ESALES_base/30_SP_COR/src/_pcatAPI/java/com/sap/isa/catalog/filter/CatalogFilterComposite.java#4 $
  $Revision: #4 $
  $Change: 150872 $
  $DateTime: 2003/09/29 09:18:51 $
*****************************************************************************/
package com.sap.isa.catalog.filter;

// misc imports
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Properties;

/**
 * This class is the abstract parent class of all composite filter nodes.
 *
 * @version     1.0
 */
public abstract class CatalogFilterComposite
    extends CatalogFilter
{
    /*
     *  CatalogFilter implements Serializable:
     *  Fields:
     *  theOperands             :   theContainer implements Serializable!
     *                              The parts implement Serializable themself!
     */
    private ArrayList theOperands = new ArrayList();

    CatalogFilterComposite() {
       super(null);
    }

    CatalogFilterComposite(Properties params){
        super(params);
    }

    public boolean isUnary()
    {
        return theOperands.size()==1?true:false;
    }

    public boolean isBinary()
    {
        return theOperands.size()==2?true:false;
    }

    public boolean isLeaf()
    {
        return false;
    }

    /**
     * Adds an operand to the list of operands.
     *
     * @param operand  the operand to be added
     */
    void addOperand(CatalogFilter operand)
    {
        theOperands.add(operand);
    }

    /**
     * Sets the right operand of a binary expression.
     *
     * @param operand  the operand to be added
     */
    void setRightOperand(CatalogFilter operand)
    {
        theOperands.add(operand);
    }

    /**
     * Sets the left operand of a binary expression.
     *
     * @param operand  the operand to be added
     */
    void setLeftOperand(CatalogFilter operand)
    {
        theOperands.add(operand);
    }

    /**
     * Returns an iterator for all operands.
     *
     * @return  iterator instance for all operands
     */
    public Iterator iterator()
    {
        return theOperands.iterator();
    }

    /**
     * Returns a list of all operands.
     *
     * @return  list of all operands
     */
    public List getOperands()
    {
        return Collections.unmodifiableList(theOperands);
    }
}