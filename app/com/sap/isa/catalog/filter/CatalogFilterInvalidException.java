/*****************************************************************************
  Copyright (c) 2000, SAPMarkets Europe GmbH, Germany, All rights reserved.
  Created:      03 April 2001

  $Id: //sap/ESALES_base/30_SP_COR/src/_pcatAPI/java/com/sap/isa/catalog/filter/CatalogFilterInvalidException.java#2 $
  $Revision: #2 $
  $Change: 129221 $
  $DateTime: 2003/05/15 17:20:58 $
*****************************************************************************/
package com.sap.isa.catalog.filter;

/**
 * This exception is thrown if an error occurs during the evaluation of
 * the filter expression.
 *
 * @version     1.0
 */
public class CatalogFilterInvalidException extends Exception
{
    /**
     * Creates a new instance of the exception with the given message text.
     *
     * @param msg  text of the exception
     */
    public CatalogFilterInvalidException(String msg) {
        super(msg);
    }
}