/*****************************************************************************
  Copyright (c) 2000, SAPMarkets Europe GmbH, Germany, All rights reserved.

  $Id: //sap/ESALES_base/30_SP_COR/src/_pcatAPI/java/com/sap/isa/catalog/filter/CatalogFilterAttrEqual.java#3 $
  $Revision: #3 $
  $Change: 144303 $
  $DateTime: 2003/08/22 20:18:21 $
*****************************************************************************/
package com.sap.isa.catalog.filter;

/**
 * This class combines an attribute with a given value.
 *
 * @version     1.0
 */
public class CatalogFilterAttrEqual extends CatalogFilter
{
    /*
     *  CatalogFilter implements Serializable:
     *  Fields:
     *  theName     : no problem
     *  thePatern   : no problem
     */
    private String theName;
    private String theValue;

    /**
     * Creates a new instance.
     *
     * @param name     name of the attribute
     * @param pattern  value of the attribute
     */
    CatalogFilterAttrEqual(String name, String value) {
        super(null);
        theName = name;
        theValue = value;
    }

    /**
     * Gets the name of the attribute.
     *
     * @return  the attribute name
     */
    public String getName() {
        return theName;
    }

    /**
     * Gets the value which the attribute should contain.
     *
     * @return  the value
     */
    public String getValue() {
        return theValue;
    }

    public void assign(CatalogFilterVisitor visitor)
            throws CatalogFilterInvalidException {
        visitor.visitAttrEqualFilter(this);
    }
}