/*****************************************************************************
  Copyright (c) 2000, SAPMarkets Europe GmbH, Germany, All rights reserved.

  $Id: //sap/ESALES_base/30_SP_COR/src/_pcatAPI/java/com/sap/isa/catalog/filter/CatalogFilter.java#4 $
  $Revision: #4 $
  $Change: 150872 $
  $DateTime: 2003/09/29 09:18:51 $
*****************************************************************************/
package com.sap.isa.catalog.filter;

import java.io.Serializable;
import java.util.Properties;

import com.sap.isa.catalog.boi.IFilter;

/**
 * This class is the abstract parent class of all filter nodes.
 *
 * @version     1.0
 */
public abstract class CatalogFilter
    implements  IFilter,
                Serializable
{
    private Properties theParameters;

    CatalogFilter(Properties params) {
        theParameters = params;
    }

    /**
     * Assigns a concrete visitor instance to the node.
     * The visitor can perform the catalog specific evaluation of the node.
     *
     * @param visitor  the visitor instance for the node
     * @exception CatalogFilterInvalidException error during evaluation of the expression
     */
    public abstract void assign(CatalogFilterVisitor visitor)
        throws CatalogFilterInvalidException;

     /**
     * Tests if the node is an unary expression.
     *
     * @return  true if unary expression, otherwise false
     */
    public boolean isUnary() {
        return false;
    }

    /**
     * Tests if the node is a binary expression.
     *
     * @return  true if binary expression, otherwise false
     */
    public boolean isBinary() {
        return false;
    }

    /**
     * Tests if the node is a leaf in the filter expression tree.
     *
     * @return  true if leaf in expression tree, otherwise false
     */
    public boolean isLeaf() {
        return true;
    }

    /**
     * Returns the value of the parameter with the given name. If it
     * was not found <code>null</code> will be returned.
     *
     * @param name the name of the parameter to be read
     * @return the value of the parameter or <code>null</code> if not found
     */
    public String getParameter(String name) {
        return (theParameters != null)?theParameters.getProperty(name):null;
    }

    public String toString() {
        // no message resources are needed!
        CatalogFilterPrinter printer = new CatalogFilterPrinter();
        return printer.transform(this);
    }
}