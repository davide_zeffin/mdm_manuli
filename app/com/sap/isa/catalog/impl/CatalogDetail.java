/*****************************************************************************
  Copyright (c) 2000, SAPMarkets Europe GmbH, Germany, All rights reserved.
  Created:      09 July 2001

  $Id: //sap/ESALES_base/30_SP_COR/src/_pcatAPI/java/com/sap/isa/catalog/impl/CatalogDetail.java#2 $
  $Revision: #2 $
  $Change: 127411 $
  $DateTime: 2003/04/30 17:20:32 $
*****************************************************************************/

package com.sap.isa.catalog.impl;

// catalog imports
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;

import org.apache.struts.util.MessageResources;

import com.sap.isa.catalog.ICatalogMessagesKeys;
import com.sap.isa.catalog.boi.IDetail;
import com.sap.isa.catalog.catalogvisitor.CatalogVisitor;
import com.sap.isa.core.logging.IsaLocation;

/**
 * This class implements the detail functionality for the catalog API.<br/>
 * Only the getter-methods are synchronized. The factory methods for the builder
 * are not synchronized because this is done in the builder instance. Only the
 * builder is allowed to modify the catalog tree. <br/>
 * Users of the catalog should only work with the interface. All other
 * <code>public</code> methods of this class are for internal use only.
 *
 * @version     1.0
 */
public class CatalogDetail extends CatalogComponent implements IDetail, Comparable {
    /*
     *  Implements Serializable:
     *  Fields:
     *  theName             :   no problem!
     *  theParentComponent  :   implemets Serilizable itself!
     *  theValues           :   theContainer implements Serializable!
     *                          The parts will be Strings i.e. no problem
     */

    private String theName;
    private CatalogComponent theParentComponent;
    private ArrayList theValues = new ArrayList(1);

    // the logging instance
    private static IsaLocation theStaticLocToLog = IsaLocation.getInstance(CatalogDetail.class.getName());

    /**
     * Constructs a new catalog detail instance. The parent component has to
     * create the assoziation to this by himself.
     *
     * @param aComponent the component to which this information belongs to
     */
    public CatalogDetail(CatalogComponent aParentComponent) {
        theParentComponent = aParentComponent;
    }

    public Catalog getRoot() {
        return theParentComponent.getRoot();
    }

    public void assign(CatalogVisitor visitor) {
        visitor.visitDetail(this);
    }

    public synchronized String getName() {
        return theName;
    }

    /**
     * Sets the name of the detail information. Internal method which
     * can't be called via the public interface.
     *
     * @param aName the name of the detail info
     */
    public void setNameInternal(String aName) {
        theName = aName;

        //inform your listeners
        //for the builder no problem becuse there are definitly no listeners
        this.dispatchChangedEvent();
    }

    public synchronized String getAsString() {
        getRoot().setTimeStamp();

        Iterator theValueIter = this.theValues.iterator();
        StringBuffer aBuffer = new StringBuffer();
        while (theValueIter.hasNext()) {
            aBuffer.append((String) theValueIter.next());
            if (theValueIter.hasNext())
                aBuffer.append(';');
        }
        return aBuffer.toString();
    }

    public synchronized String[] getAllAsString() {
        
        getRoot().setTimeStamp();

        //Values are oredered alphabetically
        Collections.sort(theValues);

        Iterator theValueIter = this.theValues.iterator();
        String[] result = new String[theValues.size()];
        for (int i = 0; theValueIter.hasNext(); i++) {
            result[i] = (String) theValueIter.next();
        }

        return result;
    }

    public synchronized String[] getAllAsStringUnsorted() {
        
        getRoot().setTimeStamp();

        Iterator theValueIter = this.theValues.iterator();
        String[] result = new String[theValues.size()];
        for (int i = 0; theValueIter.hasNext(); i++) {
            result[i] = (String) theValueIter.next();
        }

        return result;
    }

    /**
     * Sets the value of the instance. Internal method which
     * can't be called via the public interface.
     *
     * @param aValueAsString the string which holds the value
     */
    public void setAsStringInternal(String aValueAsString) {
        this.theValues.clear();
        theValues.add(aValueAsString);
        if (theStaticLocToLog.isDebugEnabled()) {
            theStaticLocToLog.debug("Size of values = " + theValues.size());
        } 
        if (theStaticLocToLog.isDebugEnabled()) {
            MessageResources aResource = getRoot().getMessageResources();
            String aMsg = aResource.getMessage(ICatalogMessagesKeys.PCAT_DETAIL_SET_VALUE, aValueAsString, theName);
            theStaticLocToLog.debug(aMsg);
        }

        //inform your listeners
        //for the builder no problem because there are definitly no listeners
        this.dispatchChangedEvent();
    }

    /**
     * Sets the value of the instance. This method should be used if the
     * attribute can hold multiple values. Internal method which
     * can't be called via the public interface.
     *
     * @param aValueAsStringArray an array of values
     */
    public void setAsStringInternal(String[] aValueAsStringArray) {
        
        this.theValues.clear();
        this.theValues.ensureCapacity(aValueAsStringArray.length);
        
        for (int i = 0; i < aValueAsStringArray.length; i++) {
            this.addAsStringInternal(aValueAsStringArray[i]);
        }
        if (theStaticLocToLog.isDebugEnabled()) {
            theStaticLocToLog.debug("Size of values = " + theValues.size());
        }          

        //inform your listeners
        //for the builder no problem because there are definitly no listeners
        this.dispatchChangedEvent();
    }

    /**
     * Helper method for adding a value.
     *
     * @param aValueAsString the string which holds the value
     */
    public void addAsStringInternal(String aValueAsString) {
        theValues.add(aValueAsString);
        if (theStaticLocToLog.isDebugEnabled()) {
            theStaticLocToLog.debug("Size of values = " + theValues.size());
        } 
        if (theStaticLocToLog.isDebugEnabled()) {
            MessageResources aResource = getRoot().getMessageResources();
            String aMsg = aResource.getMessage(ICatalogMessagesKeys.PCAT_DETAIL_ADDED_VALUE, aValueAsString, theName);
            theStaticLocToLog.debug(aMsg);
        }

        //inform your listeners
        //for the builder no problem because there are definitly no listeners
        this.dispatchChangedEvent();
    }

    public int compareTo(Object op) {
        if (!(op instanceof CatalogDetail))
            throw new ClassCastException();

        return getName().compareTo(((CatalogDetail) op).getName());
    }

    /*************************** private ******************************************/
}
