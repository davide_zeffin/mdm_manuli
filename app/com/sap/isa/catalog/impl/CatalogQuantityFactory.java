/*****************************************************************************
  Copyright (c) 2000, SAPMarkets Europe GmbH, Germany, All rights reserved.
  Created:      02 May 2001

  $Id: //sap/ESALES_base/30_SP_COR/src/_pcatAPI/java/com/sap/isa/catalog/impl/CatalogQuantityFactory.java#3 $
  $Revision: #3 $
  $Change: 150872 $
  $DateTime: 2003/09/29 09:18:51 $
*****************************************************************************/
package com.sap.isa.catalog.impl;

// catalog imports
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;

import com.sap.isa.catalog.boi.IQuantity;
import com.sap.isa.catalog.boi.IUnit;

/**
 * Factory class for quantities in the catalog.
 *
 * @version     1.0
 */
public class CatalogQuantityFactory implements Serializable
{
    //HERE IS PLACE FOR SOME CUSTOMIZING!!!!!!!!!!!!!

    public static final String QUANTITY_ID_UNKNOWN  = "UnknownId";
    public static final String QUANTITY_ID_PRICE    = "PriceId";

    //The default english name in absence of any resources.
    public static final String UNKNOWN              = "Unknown";
    public static final String PRICE                = "Price";

    static private CatalogQuantityFactory theInstance;

    static private HashMap theQuantities = new HashMap();

    private CatalogQuantityFactory()
    {
    }

    public static synchronized CatalogQuantityFactory getInstance()
    {
        if(theInstance == null)
        {
            theInstance = new CatalogQuantityFactory();
        }
        return theInstance;
    }

    public synchronized IQuantity getQuantity(String quantityId)
    {
        IQuantity theQuantity = null;
        theQuantity = (IQuantity) theQuantities.get(quantityId);
        if(theQuantity==null)
        {
            if(quantityId.equals(QUANTITY_ID_PRICE))
            {
                    theQuantity = new Price();
            }
            if(theQuantity!=null)
                theQuantities.put(quantityId,theQuantity);
        }
        return theQuantity;
    }

    public class CatalogQuantity
        implements
            IQuantity,
            Serializable
    {
        protected String theId;
        protected String theName;
        protected String theDefaultUnitId;

        protected ArrayList theUnits  = new ArrayList();

        public String getName()
        {
            return theName;
        }

        public String getGuid()
        {
            return theId;
        }

        public IUnit getDefaultUnit()
        {
            return CatalogUnitFactory.getInstance().getUnit(CatalogUnitFactory.UNIT_ID_UNKNOWN);
        }

        public Iterator getUnits()
        {
            return Collections.unmodifiableList(theUnits).iterator();
        }

        public void addUnit(IUnit aSupportedUnit)
        {
            if(!theUnits.contains(aSupportedUnit))
                theUnits.add(aSupportedUnit);
        }
    }

    public class Price extends CatalogQuantity
    {
        Price()
        {
            theId        = CatalogQuantityFactory.QUANTITY_ID_PRICE;
            theName      = CatalogQuantityFactory.PRICE;
        }

        public IUnit getDefaultUnit()
        {
            return CatalogUnitFactory.getInstance().getUnit(CatalogUnitFactory.DOLLAR);
        }


    }
}