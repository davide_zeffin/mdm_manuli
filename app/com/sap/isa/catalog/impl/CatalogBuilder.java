/*****************************************************************************
  Copyright (c) 2000, SAPMarkets Europe GmbH, Germany, All rights reserved.
  Created:      30 March 2001

  $Id: //sap/ESALES_base/30_SP_COR/src/_pcatAPI/java/com/sap/isa/catalog/impl/CatalogBuilder.java#2 $
  $Revision: #2 $
  $Change: 127411 $
  $DateTime: 2003/04/30 17:20:32 $
*****************************************************************************/
package com.sap.isa.catalog.impl;

// catalog imports
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.WeakHashMap;

import org.apache.struts.util.MessageResources;

import com.sap.isa.catalog.ICatalogMessagesKeys;
import com.sap.isa.core.logging.IsaLocation;

/**
 * Abstract parent class of all CatalogBuilders.<br/>
 * <B>IMPORTANT:</B> In all <code>public</code> builder methods the
 * object for which the property has to be built is synchronized. This
 * sychnronization is absolutly important. In the subclasses only the
 * <code>create-</code>methodsof this object can be called and the catalog
 * builder is the only instance who is allowed to modify an object in the
 * catalog tree. Otherwise a terrible mismatch will occur!
 *
 * @version     1.0
 */
public abstract class CatalogBuilder implements Serializable
{
    /*
     *  Implements Serializable:
     *  Fields:
     *  static final int's              :   no problem static!
     *  theBuiltProperties              :   transient
     *  theBuildPropertiesSerializable  :   theContainer implements
     *                                      Serializable and exists only for
     *                                      serialization.
     *                                      The parts are serializable!
     *  theLocToLog                     :   transient
     */

    protected static final int PROPERTY_ATTRIBUTE = 0;
    protected static final int PROPERTY_CATEGORY  = 1;
    protected static final int PROPERTY_DETAIL    = 2;
    protected static final int PROPERTY_ITEM      = 3;
    protected static final int PROPERTY_QUERY     = 4;
    protected static final int PROPERTY_ALL       = 5;

    protected static final int MAX_PROPERTIES     = 6;

    private transient Map theBuiltProperties = new WeakHashMap();
    private Map theBuiltPropertiesSerializable = new HashMap();

    // the logging instance
    private static IsaLocation theStaticLocToLog =
        IsaLocation.getInstance(CatalogBuilder.class.getName());

   /**
    * Builds the list of all categories of the given catalog instance. The access
    * to the catalog instance is synchronized.
    *
    * @param aCatalog the catalog for which the list of categories have to be built
    * @exception CatalogBuildFailedException error while retrieving the data
    */
    public final void buildAllCategories(Catalog aCatalog)
        throws CatalogBuildFailedException
    {
        synchronized(aCatalog)
        {
            // check if already built - otherwise do nothing
            if(!isAllCategoriesBuilt(aCatalog))
            {
                try
                {
                    // call implementation dependent version
                    this.buildAllCategoriesInternal(aCatalog);
                    // now the propery is built
                    this.setPropertyBuilt(aCatalog, PROPERTY_ALL);
                }
                catch(CatalogBuildFailedException ex)
                {
                    theStaticLocToLog.error(
                        ICatalogMessagesKeys.PCAT_EXCEPTION,
                        new Object[]{ ex.getLocalizedMessage() },
                        ex);
                    throw ex;
                }
            }
        }
    }

   /**
    * Builds all categories of the given composite instance.
    *
    * @param aComposite the composite for which the categories have to be built
    * @exception CatalogBuildFailedException error while retrieving the data
    */
    public final void buildCategories(CatalogCompositeComponent aComposite)
        throws CatalogBuildFailedException
    {
        // test if it is a category instance
        CatalogCategory category =
            (CatalogCategory)aComposite.queryInterface(CatalogComponentTypes.CATALOG_CATEGORY);
        if(category != null)
        {
            buildCategories(category);
            return;
        }

        // test if it is catalog instance
        Catalog catalog =
            (Catalog)aComposite.queryInterface(CatalogComponentTypes.CATALOG);
        if(catalog != null)
        {
            buildCategories(catalog);
            return;
        }

        // can't be built for this component
        MessageResources resource = aComposite.getRoot().getMessageResources();
        String message = resource.getMessage(
            ICatalogMessagesKeys.PCAT_ERR_OPERATION);
        throw new CatalogBuildFailedException(message);
    }

   /**
    * Builds all categories of the given catalog instance. The access
    * to the catalog instance is synchronized.
    *
    * @param aCatalog the catalog for which the root categories have to be built
    * @exception CatalogBuildFailedException error while retrieving the data
    */
    public final void buildCategories(Catalog aCatalog)
        throws CatalogBuildFailedException
    {
        synchronized(aCatalog)
        {
            // check if already built - otherwise do nothing
            if(!isCategoriesBuilt(aCatalog))
            {
                try
                {
                    // call implementation dependent version
                    this.buildCategoriesInternal(aCatalog);
                    // now the propery is built
                    this.setPropertyBuilt(aCatalog, PROPERTY_CATEGORY);
                }
                catch(CatalogBuildFailedException ex)
                {
                    theStaticLocToLog.error(
                        ICatalogMessagesKeys.PCAT_EXCEPTION,
                        new Object[]{ ex.getLocalizedMessage() },
                        ex);
                    throw ex;
                }
            }
        }
    }

   /**
    * Builds all children categories of the given category instance. The access
    * to the category instance is synchronized.
    *
    * @param aCategory the category for which the children have to be built
    * @exception CatalogBuildFailedException error while retrieving the data
    */
    public final void buildCategories(CatalogCategory aCategory)
        throws CatalogBuildFailedException
    {
        synchronized(aCategory)
        {
            // check if already built - otherwise do nothing
            if(!isCategoriesBuilt(aCategory))
            {
                try
                {
                    // call implementation dependent version
                    this.buildCategoriesInternal(aCategory);
                    // now the propery is built
                    this.setPropertyBuilt(aCategory, PROPERTY_CATEGORY);
                }
                catch(CatalogBuildFailedException ex)
                {
                    theStaticLocToLog.error(
                        ICatalogMessagesKeys.PCAT_EXCEPTION,
                        new Object[]{ ex.getLocalizedMessage() },
                        ex);
                    throw ex;
                }
            }
        }
    }

   /**
    * Builds the details of the given component instance.
    *
    * @param aComponent the component for which the details have to be built
    * @exception CatalogBuildFailedException error while retrieving the data
    */
    public final void buildDetails(CatalogComponent aComponent)
        throws CatalogBuildFailedException
    {
        // test if it is a category instance
        CatalogCategory category =
            (CatalogCategory)aComponent.queryInterface(
                CatalogComponentTypes.CATALOG_CATEGORY);
        if(category != null)
        {
            buildDetails(category);
            return;
        }

        // test if it is a catalog instance
        Catalog catalog =
            (Catalog)aComponent.queryInterface(CatalogComponentTypes.CATALOG);
        if(catalog != null)
        {
            buildDetails(catalog);
            return;
        }

        // test if it is an attribute instance
        CatalogAttribute attribute =
            (CatalogAttribute)aComponent.queryInterface(
                CatalogComponentTypes.CATALOG_ATTRIBUTE);
        if(attribute != null)
        {
            buildDetails(attribute);
            return;
        }

        // test if it is an item instance
        CatalogItem item =
            (CatalogItem)aComponent.queryInterface(CatalogComponentTypes.CATALOG_ITEM);
        if(item != null)
        {
            buildDetails(item);
            return;
        }

        // test if it is a detail instance
        CatalogDetail detail =
            (CatalogDetail)aComponent.queryInterface(
                CatalogComponentTypes.CATALOG_DETAIL);
        if(detail != null)
        {
            buildDetails(detail);
            return;
        }

        // test if it is an attribute value instance
        CatalogAttributeValue value =
            (CatalogAttributeValue)aComponent.queryInterface(
                CatalogComponentTypes.CATALOG_ATTRIBUTE_VALUE);
        if(value != null)
        {
            buildDetails(value);
            return;
        }

        // can't be built for this component
        MessageResources resource = aComponent.getRoot().getMessageResources();
        String message = resource.getMessage(
            ICatalogMessagesKeys.PCAT_ERR_OPERATION);
        throw new CatalogBuildFailedException(message);
    }

   /**
    * Builds the details of the given catalog instance. The access
    * to the catalog instance is synchronized.
    *
    * @param aCatalog the catalog for which the details have to be built
    * @exception CatalogBuildFailedException error while retrieving the data
    */
    public final void buildDetails(Catalog aCatalog)
        throws CatalogBuildFailedException
    {
        synchronized(aCatalog)
        {
            // check if already built - otherwise do nothing
            if(!isDetailsBuilt(aCatalog))
            {
                try
                {
                    // call implementation dependent version
                    this.buildDetailsInternal(aCatalog);
                    // now the propery is built
                    this.setPropertyBuilt(aCatalog, PROPERTY_DETAIL);
                }
                catch(CatalogBuildFailedException ex)
                {
                    theStaticLocToLog.error(
                        ICatalogMessagesKeys.PCAT_EXCEPTION,
                        new Object[]{ ex.getLocalizedMessage() },
                        ex);
                    throw ex;
                }
            }
        }
    }

   /**
    * Builds the details of the given category instance. The access
    * to the category instance is synchronized.
    *
    * @param aCategory the category for which the details have to be built
    * @exception CatalogBuildFailedException error while retrieving the data
    */
    public final void buildDetails(CatalogCategory aCategory)
        throws CatalogBuildFailedException
    {
        synchronized(aCategory)
        {
            // check if already built - otherwise do nothing
            if(!isDetailsBuilt(aCategory))
            {
                try
                {
                    // call implementation dependent version
                    this.buildDetailsInternal(aCategory);
                    // now the propery is built
                    this.setPropertyBuilt(aCategory, PROPERTY_DETAIL);
                }
                catch(CatalogBuildFailedException ex)
                {
                    theStaticLocToLog.error(
                        ICatalogMessagesKeys.PCAT_EXCEPTION,
                        new Object[]{ ex.getLocalizedMessage() },
                        ex);
                    throw ex;
                }
            }
        }
    }

   /**
    * Builds the details of the given attribte instance. The access
    * to the attribute instance is synchronized.
    *
    * @param aAttribute the attribute for which the details have to be built
    * @exception CatalogBuildFailedException error while retrieving the data
    */
    public final void buildDetails(CatalogAttribute aAttribute)
        throws CatalogBuildFailedException
    {
        synchronized(aAttribute)
        {
            // check if already built - otherwise do nothing
            if(!isDetailsBuilt(aAttribute))
            {
                try
                {
                    // call implementation dependent version
                    this.buildDetailsInternal(aAttribute);
                    // now the propery is built
                    this.setPropertyBuilt(aAttribute, PROPERTY_DETAIL);
                }
                catch(CatalogBuildFailedException ex)
                {
                    theStaticLocToLog.error(
                        ICatalogMessagesKeys.PCAT_EXCEPTION,
                        new Object[]{ ex.getLocalizedMessage() },
                        ex);
                    throw ex;
                }
            }
        }
    }

   /**
    * Builds the details of the given item instance. The access
    * to the item instance is synchronized.
    *
    * @param aItem the item for which the details have to be built
    * @exception CatalogBuildFailedException error while retrieving the data
    */
    public final void buildDetails(CatalogItem aItem)
        throws CatalogBuildFailedException
    {
        synchronized(aItem)
        {
            // check if already built - otherwise do nothing
            if(!isDetailsBuilt(aItem))
            {
                try
                {
                    // call implementation dependent version
                    this.buildDetailsInternal(aItem);
                    // now the propery is built
                    this.setPropertyBuilt(aItem, PROPERTY_DETAIL);
                }
                catch(CatalogBuildFailedException ex)
                {
                    theStaticLocToLog.error(
                        ICatalogMessagesKeys.PCAT_EXCEPTION,
                        new Object[]{ ex.getLocalizedMessage() },
                        ex);
                    throw ex;
                }
            }
        }
    }

   /**
    * Builds the details of the given detail instance. The access
    * to the detail instance is synchronized.
    *
    * @param aDetail the detail for which the details have to be built
    * @exception CatalogBuildFailedException error while retrieving the data
    */
    public final void buildDetails(CatalogDetail aDetail)
        throws CatalogBuildFailedException
    {
        synchronized(aDetail)
        {
            // check if already built - otherwise do nothing
            if(!isDetailsBuilt(aDetail))
            {
                try
                {
                    // call implementation dependent version
                    this.buildDetailsInternal(aDetail);
                    // now the propery is built
                    this.setPropertyBuilt(aDetail, PROPERTY_DETAIL);
                }
                catch(CatalogBuildFailedException ex)
                {
                    theStaticLocToLog.error(
                        ICatalogMessagesKeys.PCAT_EXCEPTION,
                        new Object[]{ ex.getLocalizedMessage() },
                        ex);
                    throw ex;
                }
            }
        }
    }

   /**
    * Builds the details of the given attribute value instance. The access
    * to the attribute value instance is synchronized.
    *
    * @param aValue the attribute value for which the details have to be built
    * @exception CatalogBuildFailedException error while retrieving the data
    */
    public final void buildDetails(CatalogAttributeValue aValue)
        throws CatalogBuildFailedException
    {
        synchronized(aValue)
        {
            // check if already built - otherwise do nothing
            if(!isDetailsBuilt(aValue))
            {
                try
                {
                    // call implementation dependent version
                    this.buildDetailsInternal(aValue);
                    // now the propery is built
                    this.setPropertyBuilt(aValue, PROPERTY_DETAIL);
                }
                catch(CatalogBuildFailedException ex)
                {
                    theStaticLocToLog.error(
                        ICatalogMessagesKeys.PCAT_EXCEPTION,
                        new Object[]{ ex.getLocalizedMessage() },
                        ex);
                    throw ex;
                }
            }
        }
    }

   /**
    * Builds the attributes of the given composite instance.
    *
    * @param aComposite the composite for which the attributes have to be built
    * @exception CatalogBuildFailedException error while retrieving the data
    */
    public final void buildAttributes(CatalogCompositeComponent aComposite)
        throws CatalogBuildFailedException
    {
        // test if it is a category instance
        CatalogCategory category =
            (CatalogCategory)aComposite.queryInterface(CatalogComponentTypes.CATALOG_CATEGORY);
        if(category != null)
        {
            buildAttributes(category);
            return;
        }

        // test if it is catalog instance
        Catalog catalog =
            (Catalog)aComposite.queryInterface(CatalogComponentTypes.CATALOG);
        if(catalog != null)
        {
            buildAttributes(catalog);
            return;
        }

        // can't be built for this component
        MessageResources resource = aComposite.getRoot().getMessageResources();
        String message = resource.getMessage(
            ICatalogMessagesKeys.PCAT_ERR_OPERATION);
        throw new CatalogBuildFailedException(message);
    }

   /**
    * Builds the attributes of the given catalog instance. The access
    * to the catalog instance is synchronized.
    *
    * @param aCatalog the catalog for which the attributes have to be built
    * @exception CatalogBuildFailedException error while retrieving the data
    */
    public final void buildAttributes(Catalog aCatalog)
        throws CatalogBuildFailedException
    {
        synchronized(aCatalog)
        {
            // check if already built - otherwise do nothing
            if(!isAttributesBuilt(aCatalog))
            {
                try
                {
                    // call implementation dependent version
                    this.buildAttributesInternal(aCatalog);
                    // now the propery is built
                    this.setPropertyBuilt(aCatalog, PROPERTY_ATTRIBUTE);
                }
                catch(CatalogBuildFailedException ex)
                {
                    theStaticLocToLog.error(ICatalogMessagesKeys.PCAT_EXCEPTION,
                             new Object[]{ ex.getLocalizedMessage() }, ex);
                    throw ex;
                }
            }
        }
    }

   /**
    * Builds the attributes of the given category instance. The access
    * to the category instance is synchronized.
    *
    * @param aCategory the category for which the attributes have to be built
    * @exception CatalogBuildFailedException error while retrieving the data
    */
    public final void buildAttributes(CatalogCategory aCategory)
        throws CatalogBuildFailedException
    {
        synchronized(aCategory)
        {
            // check if already built - otherwise do nothing
            if(!isAttributesBuilt(aCategory))
            {
                try
                {
                    // call implementation dependent version
                    this.buildAttributesInternal(aCategory);
                    // now the propery is built
                    this.setPropertyBuilt(aCategory, PROPERTY_ATTRIBUTE);
                }
                catch(CatalogBuildFailedException ex)
                {
                    theStaticLocToLog.error(
                        ICatalogMessagesKeys.PCAT_EXCEPTION,
                        new Object[]{ ex.getLocalizedMessage() },
                        ex);

                    throw ex;
                }
            }
        }
    }

   /**
    * Builds the items of the given composite instance.
    *
    * @param aComposite the composite for which the items have to be built
    * @exception CatalogBuildFailedException error while retrieving the data
    */
    public final void buildItems(CatalogCompositeComponent aComposite)
        throws CatalogBuildFailedException
    {
        // test if it is a category instance
        CatalogCategory category =
            (CatalogCategory)aComposite.queryInterface(CatalogComponentTypes.CATALOG_CATEGORY);
        if(category != null)
        {
            buildItems(category);
            return;
        }

        // can't be built for this component
        MessageResources resource = aComposite.getRoot().getMessageResources();
        String message = resource.getMessage(
            ICatalogMessagesKeys.PCAT_ERR_OPERATION);
        throw new CatalogBuildFailedException(message);
    }

   /**
    * Builds the items of the given category instance. The access
    * to the category instance is synchronized.
    *
    * @param aCategory the category for which the items have to be built
    * @exception CatalogBuildFailedException error while retrieving the data
    */
    public final void buildItems(CatalogCategory aCategory)
        throws CatalogBuildFailedException
    {
        synchronized(aCategory)
        {
            // check if already built - otherwise do nothing
            if(!isItemsBuilt(aCategory))
            {
                try
                {
                    // call implementation dependent version
                    this.buildItemsInternal(aCategory);
                    aCategory.countMainItems();
                    // now the propery is built
                    this.setPropertyBuilt(aCategory, PROPERTY_ITEM);
                }
                catch(CatalogBuildFailedException ex)
                {
                    theStaticLocToLog.error(
                        ICatalogMessagesKeys.PCAT_EXCEPTION,
                        new Object[]{ ex.getLocalizedMessage() },
                        ex);
                    throw ex;
                }
            }
        }
    }

   /**
    * Builds the items of the given query instance. The access
    * to the query instance is synchronized.
    *
    * @param aQuery the query for which the items have to be built
    * @exception CatalogBuildFailedException error while retrieving the data
    */
    public final void buildQuery(CatalogQuery aQuery)
        throws CatalogBuildFailedException
    {
        synchronized(aQuery)
        {
            // check if already built - otherwise do nothing
            if(!isQueryBuilt(aQuery))
            {
                try
                {
                    // call implementation dependent version
                    this.buildQueryInternal(aQuery);
                    // now the propery is built; if we are not in count mode
                    if (!aQuery.inCountMode()) 
                    {
                        this.setPropertyBuilt(aQuery, PROPERTY_QUERY);                        
                    } // end of if ()
                }
                catch(CatalogBuildFailedException ex)
                {
                    theStaticLocToLog.error(
                        ICatalogMessagesKeys.PCAT_EXCEPTION,
                        new Object[]{ ex.getLocalizedMessage() },
                        ex);
                    throw ex;
                }
            }
        }
    }

   /**
    * Tests if list all categories have been built.
    *
    * @param the component for which it has to be tested
    */
    public final boolean isAllCategoriesBuilt(Catalog aComponent)
    {
        return isPropertyBuilt(aComponent, PROPERTY_ALL);
    }

   /**
    * Tests if all categories have been built.
    *
    * @param the component for which it has to be tested
    */
    public final boolean isCategoriesBuilt(CatalogCompositeComponent aComponent)
    {
        return isPropertyBuilt(aComponent, PROPERTY_CATEGORY);
    }

   /**
    * Tests if the details have been built.
    *
    * @param the component for which it has to be tested
    */
    public final boolean isDetailsBuilt(CatalogComponent aComponent)
    {
        return isPropertyBuilt(aComponent, PROPERTY_DETAIL);
    }

   /**
    * Tests if the attributes have been built.
    *
    * @param the component for which it has to be tested
    */
    public final boolean isAttributesBuilt(CatalogCompositeComponent aComponent)
    {
        return isPropertyBuilt(aComponent, PROPERTY_ATTRIBUTE);
    }

   /**
    * Tests if the items have been built.
    *
    * @param the component for which it has to be tested
    */
    public final boolean isItemsBuilt(CatalogCompositeComponent aComponent)
    {
        return isPropertyBuilt(aComponent, PROPERTY_ITEM);
    }

   /**
    * Tests if the query has been built.
    *
    * @param the component for which it has to be tested
    */
    public final boolean isQueryBuilt(CatalogQuery aComponent)
    {
        return isPropertyBuilt(aComponent, PROPERTY_QUERY);
    }

    // helper methods
    protected synchronized boolean isPropertyBuilt(Object aNode, int aProperty)
    {
        boolean result = false;

        boolean props[] = (boolean[])theBuiltProperties.get(aNode);
        if(props != null)
            result = props[aProperty];
        return result;
    }

    protected synchronized void setPropertyBuilt(Object aNode, int aProperty)
    {
        boolean props[];

        if (!isPropertyBuilt(aNode, aProperty))
        {
           props = (boolean[])theBuiltProperties.get(aNode);
           if (props == null)
           {
              props = new boolean[MAX_PROPERTIES];
              Arrays.fill(props, false);
              theBuiltProperties.put(aNode, props);
           }
           props[aProperty] = true;
        }
    }

   /**
    * Internal builder method which has to be redefined by the concrete
    * catalog implementation.
    *
    * @param aCatalog the catalog for which the list of all categories have to be built
    */
    protected abstract void buildAllCategoriesInternal(Catalog aCatalog)
      throws CatalogBuildFailedException;

   /**
    * Internal builder method which has to be redefined by the concrete
    * catalog implementation.
    *
    * @param aCatalog the catalog for which the root categories have to be built
    */
    protected abstract void buildCategoriesInternal(Catalog aCatalog)
      throws CatalogBuildFailedException;

   /**
    * Internal builder method which has to be redefined by the concrete
    * catalog implementation.
    *
    * @param aCategory the category for which the children have to be built
    */
    protected abstract void buildCategoriesInternal(CatalogCategory aCategory)
      throws CatalogBuildFailedException;

   /**
    * Internal builder method which has to be redefined by the concrete
    * catalog implementation.
    *
    * @param aCatalog the catalog for which the details have to be built
    */
    protected abstract void buildDetailsInternal(Catalog aCatalog)
      throws CatalogBuildFailedException;

   /**
    * Internal builder method which has to be redefined by the concrete
    * catalog implementation.
    *
    * @param aCategory the category for which the details have to be built
    */
    protected abstract void buildDetailsInternal(CatalogCategory aCategory)
      throws CatalogBuildFailedException;

   /**
    * Internal builder method which has to be redefined by the concrete
    * catalog implementation.
    *
    * @param aAttribute the attribute for which the details have to be built
    */
    protected abstract void buildDetailsInternal(CatalogAttribute aAttribute)
      throws CatalogBuildFailedException;

   /**
    * Internal builder method which has to be redefined by the concrete
    * catalog implementation.
    *
    * @param aItem the item for which the details have to be built
    */
    protected abstract void buildDetailsInternal(CatalogItem aItem)
      throws CatalogBuildFailedException;

   /**
    * Internal builder method which has to be redefined by the concrete
    * catalog implementation.
    *
    * @param aDetail the detail for which the details have to be built
    */
    protected abstract void buildDetailsInternal(CatalogDetail aDetail)
      throws CatalogBuildFailedException;

   /**
    * Internal builder method which has to be redefined by the concrete
    * catalog implementation.
    *
    * @param aValue the value for which the details have to be built
    */
    protected abstract void buildDetailsInternal(CatalogAttributeValue aValue)
      throws CatalogBuildFailedException;

   /**
    * Internal builder method which has to be redefined by the concrete
    * catalog implementation.
    *
    * @param aCatalog the catalog for which the attributes have to be built
    */
    protected abstract void buildAttributesInternal(Catalog aCatalog)
      throws CatalogBuildFailedException;

   /**
    * Internal builder method which has to be redefined by the concrete
    * catalog implementation.
    *
    * @param aCategory the category for which the attributes have to be built
    */
    protected abstract void buildAttributesInternal(CatalogCategory aCategory)
      throws CatalogBuildFailedException;

   /**
    * Internal builder method which has to be redefined by the concrete
    * catalog implementation.
    *
    * @param aCategory the category for which the items have to be built
    */
    protected abstract void buildItemsInternal(CatalogCategory aCategory)
      throws CatalogBuildFailedException;

   /**
    * Internal builder method which has to be redefined by the concrete
    * catalog implementation.
    *
    * @param aQuery the query for which the items have to be built
    */
    protected abstract void buildQueryInternal(CatalogQuery aQuery)
      throws CatalogBuildFailedException;

    /*************************** private ******************************************/

    /**
     * Store the content of the non serializable WeakHashMap in a
     * serializable HashMap
     */
    private void writeObject(ObjectOutputStream oo) throws IOException
    {
        theBuiltPropertiesSerializable = new HashMap(theBuiltProperties);
        oo.defaultWriteObject();
        //we got it on disc free the space
        theBuiltPropertiesSerializable.clear();
    }

    /**
     *  Store the contents from the serializable HashMap in the non
     *  serializable WeakHashMap
     */
    private void readObject(ObjectInputStream oo)
        throws IOException, ClassNotFoundException
    {
        oo.defaultReadObject();
        theBuiltProperties = new WeakHashMap(theBuiltPropertiesSerializable);
        theBuiltPropertiesSerializable.clear();
    }
}
