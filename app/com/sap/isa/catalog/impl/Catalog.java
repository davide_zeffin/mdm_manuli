/**
 * Copyright (c) 2000, SAPMarkets Europe GmbH, Germany, All rights reserved. Created:      09 July 2001 $Id:
 * //sap/ESALES_base/30_SP_COR/src/_pcatAPI/java/com/sap/isa/catalog/impl/Catalog.java#3 $ $Revision: #3 $ $Change:
 * 129234 $ $DateTime: 2003/05/15 17:40:02 $
 */
package com.sap.isa.catalog.impl;

import java.io.Reader;
import java.io.StringReader;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Locale;
import java.util.Set;

import javax.xml.parsers.ParserConfigurationException;

import org.apache.struts.util.MessageResources;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Text;

import com.sap.isa.catalog.ICatalogMessagesKeys;
import com.sap.isa.catalog.boi.CatalogException;
import com.sap.isa.catalog.boi.IActor;
import com.sap.isa.catalog.boi.ICatalog;
import com.sap.isa.catalog.boi.ICategory;
import com.sap.isa.catalog.boi.IComponent;
import com.sap.isa.catalog.boi.IEditableCatalog;
import com.sap.isa.catalog.boi.IServerEngine;
import com.sap.isa.catalog.cache.CachableItem;
import com.sap.isa.catalog.catalogvisitor.CatalogVisitor;
import com.sap.isa.catalog.catalogvisitor.CatalogXMLVisitor;
import com.sap.isa.catalog.editor.EditableCatalog;
import com.sap.isa.catalog.parsers.xpathinterpreter.ASTXPathExpr;
import com.sap.isa.catalog.parsers.xpathinterpreter.IXPnodeSetValue;
import com.sap.isa.catalog.parsers.xpathinterpreter.IXPvalue;
import com.sap.isa.catalog.parsers.xpathinterpreter.ParseException;
import com.sap.isa.catalog.parsers.xpathinterpreter.XPathContext;
import com.sap.isa.catalog.parsers.xpathinterpreter.XPathDomParserVisitor;
import com.sap.isa.catalog.parsers.xpathinterpreter.XPathParser;
import com.sap.isa.core.TechKey;
import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.core.util.table.ResultData;
import com.sap.isa.core.util.table.Table;
import com.sap.isa.core.util.table.TableRow;

/**
 * Abstract parent class of all catalogs. <br/
 * > Only the getter-methods are synchronized. The factory/setter methods for the builder are not synchronized because
 * this is done in the builder instance. Only the builder is allowed to modify the catalog tree. <br/
 * > Users of the catalog should only work with the interface. All other <code>public</code> methods of this class are
 * for internal use only.
 *
 * @version 1.0
 */
public abstract class Catalog extends CatalogCompositeComponent implements ICatalog, CachableItem {
    /*
     *  CatalogCompositeComponent implements Serializable:
     *  Fields:
     *  API_VERSION             :   no problem
     *  theConcreteBuilder      :   implements Serializable itself!
     *  theBackendContext       :   should implement Serializable ;-)!
     *
     *  theTimeStamp            :   implements Serializable itself!
     *  theName                 :   no problem!
     *  theDescription          :   no problem!
     *  theThumbNail            :   no problem!
     *
     *  theCategories           :   the Container implements Serializable!
     *                              The parts implement Serializable them self!
     *
     *  theMessageResources     :   implements Serializable
     *  theClient               :   implements serializable
     *  theServer               :   implements serializable
     *  isDistributable         :   no problem
     *
     *  theEditorsOfTheInstance :   the Container implements Serializable!
     *                              The parts implement Serializable them self!
     *  theMapOfAllAttributes   :   the Container implements Serializable!
     *                              The parts implement Serializable them self!
     *  theMapOfAttributeUsers  :   the Container implements Serializable!
     *                              The parts implement Serializable them self!
     *  theKeyToAttibuteMap     :   the Container implements Serializable!
     *                              The parts (String) implement Serializable
     *                              them self!
     *  theResKeyToAttibuteTab  :   the Container implements Serializable!
     *                              The parts (String) implement Serializable
     *                              them self!
     *                              Used to assign a resource key to an attribute
     *                              Needed to display a label for an attribute in the advanced search 
     * theDefaultLocale: implements Serializable
     *
     */
    private static final String API_VERSION = "2.1";

    // the logging instance
    private static IsaLocation theStaticLocToLog = IsaLocation.getInstance(Catalog.class.getName());
    private static IsaLocation runtimeLogger = IsaLocation.getInstance("runtime." + Catalog.class.getName());
    private CatalogBuilder theConcreteBuilder;
    private Timestamp theTimeStamp;
    private String theCatalogGuid;
    private String theName;
    private String theDescription;
    private String theThumbNail;
    private HashMap theCategories = new HashMap();
    protected MessageResources theMessageResources;
    protected CatalogClient theClient;
    protected CatalogServerEngine theServerEngine;
    protected boolean isDistributable = false;
    protected HashMap theEditorsOfTheInstance = new HashMap();
    protected HashMap theMapOfAllAttributes = new HashMap();
    protected HashMap theMapOfAttributeUsers = new HashMap();
    protected HashMap theKeyToAttributeMap = new HashMap();
    protected Locale theDefaultLocale = Locale.ENGLISH;
    protected ArrayList theSupportedLocales = new ArrayList();
    protected boolean useRoleForItemTypeDet = true;
    protected String theQuickSearchAttributes[] = null;
    protected TechKey catalogKey = null;
    protected ResultData theResKeyToAttributeTab = null;

    {
        theSupportedLocales.add(theDefaultLocale);
    }

    /**
     * Default constructor. Only the time stamp is set.
     *
     * @param aServerEngine DOCUMENT ME!
     * @param aCatalogGuid DOCUMENT ME!
     */
    protected Catalog(CatalogServerEngine aServerEngine, String aCatalogGuid) {
        this.theServerEngine = aServerEngine;
        this.theCatalogGuid = aCatalogGuid;
        readConfiguration();
        this.setTimeStamp();

        if (IServerEngine.ServerType.CRM.equals(theServerEngine.getType())) {
            useRoleForItemTypeDet = false;
        }
    }

    /**
     * Creates a new Catalog object.
     *
     * @param aServerEngine DOCUMENT ME!
     * @param aCatalogGuid DOCUMENT ME!
     * @param maxNumberOfItemsPerCategory DOCUMENT ME!
     */
    protected Catalog(CatalogServerEngine aServerEngine, String aCatalogGuid, int maxNumberOfItemsPerCategory) {
        this(aServerEngine, aCatalogGuid);
        this.theMaxNumberOfItemLeafs = maxNumberOfItemsPerCategory;
    }

    /**
     * Uses the provided file to read a configuration xml file. Currently only used for<br>
     * (1) mapping keys to attribute guids<br>
     * (2) customizing attribute names for QuickSearch
     */
    protected void readConfiguration() {
        Document aDoc = this.getServerEngine().getConfigDocument();
        String theCatalogGuid = this.theCatalogGuid;

        StringBuffer aXPathStringBuffer = new StringBuffer("/Server/Catalog[@ID='").append(theCatalogGuid).append("']/Map/Key");

        Reader stream = new StringReader(aXPathStringBuffer.toString());
        XPathParser theXpp = new XPathParser(stream);
        theXpp.disable_tracing();

        IXPnodeSetValue theResult = (IXPnodeSetValue) evaluate(theXpp, aXPathStringBuffer.toString(), aDoc);
        ArrayList theResultList = theResult.getValue();

        if (theResultList.size() == 0) //in case we found nothing take the default
         {
            Document aSiteDoc = this.getServerEngine().getSite().getConfigDocument();
            aXPathStringBuffer = new StringBuffer("/Site/Map/Key");
            theResult = (IXPnodeSetValue) evaluate(theXpp, aXPathStringBuffer.toString(), aSiteDoc);
            theResultList = theResult.getValue();
        }

        int size = theResultList.size();

        for (int i = 0; i < size; i++) {
            Element aKeyValueElement = (Element) theResultList.get(i);
            String aKey = aKeyValueElement.getAttribute("ID");
            String aValue = ((Text) aKeyValueElement.getFirstChild()).getData();

            this.theKeyToAttributeMap.put(aKey, aValue);
        }

        // and now the list of attribute names for customizing the QuickSearch
        aXPathStringBuffer = new StringBuffer("/Server/Catalog[@ID='").append(theCatalogGuid).append("']/QuickSearchAttributes/Attribute");
        stream = new StringReader(aXPathStringBuffer.toString());
        theXpp = new XPathParser(stream);
        theXpp.disable_tracing();
        theResult = (IXPnodeSetValue) evaluate(theXpp, aXPathStringBuffer.toString(), aDoc);
        theResultList = theResult.getValue();

        if (theResultList.size() == 0) //in case we found nothing take the default
         {
            Document aSiteDoc = this.getServerEngine().getSite().getConfigDocument();
            aXPathStringBuffer = new StringBuffer("/Site/QuickSearchAttributes/Attribute");
            theResult = (IXPnodeSetValue) evaluate(theXpp, aXPathStringBuffer.toString(), aSiteDoc);
            theResultList = theResult.getValue();
        }

        size = theResultList.size();
        this.theQuickSearchAttributes = new String[size];

        Table attrTab = new Table("attribList");
        attrTab.addColumn(Table.TYPE_STRING, "ID");
        attrTab.addColumn(Table.TYPE_STRING, "resKey");
        
        for (int i = 0; i < size; i++) {
            Element aKeyValueElement = (Element) theResultList.get(i);
            String aKey = aKeyValueElement.getAttribute("ID");
            String aResKey = aKeyValueElement.getAttribute("resKey");
          
            this.theQuickSearchAttributes[i] = aKey;

            TableRow attRow = attrTab.insertRow();
            attRow.getField(1).setValue(aKey);
            attRow.getField(2).setValue(aResKey);
        }
        theResKeyToAttributeTab = new ResultData(attrTab);

    }

    /**
     * DOCUMENT ME!
     *
     * @param visitor DOCUMENT ME!
     */
    public void assign(CatalogVisitor visitor) {
        visitor.visitCatalog(this);
    }

    /**
     * DOCUMENT ME!
     *
     * @param op DOCUMENT ME!
     *
     * @return DOCUMENT ME!
     */
    public int compareTo(Object op) {
        if (!(op instanceof Catalog)) {
            throw new ClassCastException();
        }

        return getGuid().compareTo(((Catalog) op).getGuid());
    }

    /**
     * DOCUMENT ME!
     *
     * @return DOCUMENT ME!
     */
    public boolean isDistributable() {
        return isDistributable;
    }

    /* ****************************** ICatalog ********************************** */
    public synchronized IEditableCatalog getEditor(IActor anUser) {
        IEditableCatalog anEditor = (IEditableCatalog) theEditorsOfTheInstance.get(anUser);

        if (anEditor == null) {
            anEditor = new EditableCatalog(this, anUser);
            theEditorsOfTheInstance.put(anUser, anEditor);
        }

        return anEditor;
    }

    /**
     * DOCUMENT ME!
     *
     * @return DOCUMENT ME!
     */
    public final String getAPIVersion() {
        return API_VERSION;
    }

    /**
     * DOCUMENT ME!
     *
     * @return DOCUMENT ME!
     */
    public String getGuid() {
        return theCatalogGuid;
    }

    /**
     * DOCUMENT ME!
     *
     * @return DOCUMENT ME!
     */
    public String getServerGuid() {
        return theServerEngine.getGuid();
    }

    /**
     * DOCUMENT ME!
     *
     * @return DOCUMENT ME!
     */
    public synchronized String getName() {
        return this.theName;
    }

    /**
     * DOCUMENT ME!
     *
     * @return DOCUMENT ME!
     */
    public synchronized String getDescription() {
        return this.theDescription;
    }

    /**
     * DOCUMENT ME!
     *
     * @return DOCUMENT ME!
     */
    public synchronized String getThumbNail() {
        return this.theThumbNail;
    }

    /**
     * Return the default catalog status: NOT IMPLEMENTED.
     *
     * @return a <code>int</code> value representing the catalog server status
     */
    public int getCatalogStatus() {
        return CATALOG_STATUS_NOT_IMPL;
    }

    /**
     * Returns the list of attribute names used for a QuickSearch
     *
     * @return list of attribute names to search for in a QuickSearch
     */
    public String[] getQuickSearchAttributes() {
        return this.theQuickSearchAttributes;
    }

    /**
     * DOCUMENT ME!
     *
     * @return DOCUMENT ME!
     *
     * @throws CatalogException DOCUMENT ME!
     */
    public synchronized Iterator getCategories()
                                        throws CatalogException {
        if (runtimeLogger.isDebugEnabled()) {
            runtimeLogger.debug("begin getCategories()");
        }

        //Actually right now not possible only to be consistent with the rest
        if (isDeleted()) {
            String theMessage = getMessageResources().getMessage(ICatalogMessagesKeys.PCAT_ERR_IS_DELEDED);
            throw new CatalogException(theMessage);
        }

        this.setTimeStamp();

        try {
            if (!theConcreteBuilder.isAllCategoriesBuilt(this)) {
                theConcreteBuilder.buildAllCategories(this);
            }
        }
        catch (CatalogBuildFailedException ex) {
            throw new CatalogException(ex.getLocalizedMessage());
        }

        ArrayList categories = new ArrayList(theCategories.values());
        Collections.sort(categories);

        // return categories.iterator();
        Iterator result = categories.iterator();

        if (runtimeLogger.isDebugEnabled()) {
            runtimeLogger.debug("end   getCategories");
        }

        return result;
    }

    /**
     * DOCUMENT ME!
     *
     * @param aComparator DOCUMENT ME!
     *
     * @return DOCUMENT ME!
     *
     * @throws CatalogException DOCUMENT ME!
     */
    public synchronized Iterator getCategories(Comparator aComparator)
                                        throws CatalogException {
        if (runtimeLogger.isDebugEnabled()) {
            runtimeLogger.debug("begin getCategories(aComparator)");
        }

        //Actually right now not possible only to be consistent with the rest
        if (isDeleted()) {
            String theMessage = getMessageResources().getMessage(ICatalogMessagesKeys.PCAT_ERR_IS_DELEDED);
            throw new CatalogException(theMessage);
        }

        this.setTimeStamp();

        try {
            if (!theConcreteBuilder.isAllCategoriesBuilt(this)) {
                theConcreteBuilder.buildAllCategories(this);
            }
        }
        catch (CatalogBuildFailedException ex) {
            throw new CatalogException(ex.getLocalizedMessage());
        }

        ArrayList categories = new ArrayList(theCategories.values());
        Collections.sort(categories, aComparator);

        // return categories.iterator();
        Iterator result = categories.iterator();

        if (runtimeLogger.isDebugEnabled()) {
            runtimeLogger.debug("end   getCategories");
        }

        return result;
    }

    /**
     * DOCUMENT ME!
     *
     * @param aCategoryGuid DOCUMENT ME!
     *
     * @return DOCUMENT ME!
     *
     * @throws CatalogException DOCUMENT ME!
     */
    public synchronized ICategory getCategory(String aCategoryGuid)
                                       throws CatalogException {
        if (runtimeLogger.isDebugEnabled()) {
            runtimeLogger.debug("begin getCategory(" + aCategoryGuid + ")");
        }

        //Actually right now not possible only to be consistent with the rest
        if (isDeleted()) {
            String theMessage = getMessageResources().getMessage(ICatalogMessagesKeys.PCAT_ERR_IS_DELEDED);
            throw new CatalogException(theMessage);
        }

        this.setTimeStamp();

        CatalogCategory category = null;

        try {
            if (!theConcreteBuilder.isAllCategoriesBuilt(this)) {
                theConcreteBuilder.buildAllCategories(this);
            }

            category = (CatalogCategory) theCategories.get(aCategoryGuid);
        }
        catch (CatalogBuildFailedException ex) {
            throw new CatalogException(ex.getLocalizedMessage());
        }

        if (runtimeLogger.isDebugEnabled()) {
            runtimeLogger.debug("end   getCategory");
        }

        return category;
    }

    /**
     * DOCUMENT ME!
     *
     * @return DOCUMENT ME!
     */
    public Set getAttributeKeys() {
        return theKeyToAttributeMap.keySet();
    }

    /**
     * DOCUMENT ME!
     *
     * @param aKey DOCUMENT ME!
     *
     * @return DOCUMENT ME!
     */
    public String getAttributeGuid(String aKey) {
        return (String) theKeyToAttributeMap.get(aKey);
    }

    /**
     * Returns a table of attribute descriptions
     *
     * @return table of attribute descriptions
     */
    public ResultData getAttributeResKeys() {
        return this.theResKeyToAttributeTab;
    }

    /* *************************** BackendBusinessObject ************************

    public void setConnectionFactory(ConnectionFactory aConnectionFactory)
    {
        theConnectionFactory = aConnectionFactory;
    }

    public ConnectionFactory getConnectionFactory()
    {
    return theConnectionFactory;
    }

    public void setBackendObjectSupport(BackendObjectSupport aSupporter)
    {
    theBackendSupport=aSupporter;
    return;
    }

    public BackendObjectSupport getBackendObjectSupport()
    {
    return theBackendSupport;
    }

    public void initBackendObject(Properties p0, BackendBusinessObjectParams p1)
    throws BackendException
    {
        // has to be implemented by the subclass
    }

    public void destroyBackendObject()
    {
        // has to be implemented by the subclass
    }

    public void setContext(BackendContext context)
    {
    theBackendContext = context;
    }

    public BackendContext getContext()
    {
    return theBackendContext;
    }

    */
    /* ******************************* CachableItem ****************************** */
    public Timestamp getTimeStamp() {
        return theTimeStamp;
    }

    /**
     * DOCUMENT ME!
     */
    public void setTimeStamp() {
        theTimeStamp = new Timestamp(System.currentTimeMillis());
    }

    /**
     * Fulfills the CacheableItem contract. The semantic is: if the state of the instance is deleted it is not up to
     * date. Overwriting classes should obey the logic that a deleted instance is never up to date.
     *
     * @return a <code>boolean</code> value
     */
    synchronized public boolean isCachedItemUptodate() {
        boolean theResult = true;

        // Should be redefined in the concrete catalog implementation if the
        // underlying catalog engine supports this behavior.
        if (this.getState() == IComponent.State.DELETED) {
            theResult = false;
        }
         // end of if ()

        return theResult;
    }
    
    /**
     * Returns true, if the ROLE Attribute should be used, to determine the IItem
     * Type (Main Item, Accessory, ..). Otherwise it returns false.
     * Fals shoudl only be returned for the CRM catalog which uses different
     * Attributes to specify the type of an IItem.
     *
     * @return a <code>boolean</code> true if ROLE attribute should used tod determine
     *                                the IItem type
     *                                false else
     */
    synchronized public boolean useRoleAttrForItemType() {
        return useRoleForItemTypeDet;
    }

    /* ************************** setInternal ********************************** */

    /**
     * Sets the name of the category. Internal method which can't be called via the public interface.
     *
     * @param aName the name to be set
     */
    public void setNameInternal(String aName) {
        this.theName = aName;

        //inform your listeners
        //for the builder no problem because there are definitely no listeners
        this.dispatchChangedEvent();
    }

    /**
     * Sets the long-text description of the category. Internal method which can't be called via the public interface.
     *
     * @param aDescription the text to be set
     */
    public void setDescriptionInternal(String aDescription) {
        this.theDescription = aDescription;

        //inform your listeners
        //for the builder no problem because there are definitely no listeners
        this.dispatchChangedEvent();
    }

    /**
     * Sets the thumb nail of the category. Internal method which can't be called via the public interface.
     *
     * @param aThumbNail the thumb nail to be set
     */
    public void setThumbNailInternal(String aThumbNail) {
        this.theThumbNail = aThumbNail;

        //inform your listeners
        //for the builder no problem because there are definitely no listeners
        this.dispatchChangedEvent();
    }

    /* ************************************************************************** */

    /**
     * Returns the builder instance.
     *
     * @return the builder of the catalog
     */
    public final CatalogBuilder getCatalogBuilder() {
        return this.theConcreteBuilder;
    }
    
    /**
     * Returns the catalog Staging key.
     *
     * @return the catalog staging key, if present
     */
    public final TechKey getCatalogKey() {
        return catalogKey;
    }

    /**
     * Returns the client of the catalog.
     *
     * @return the client of this catalog
     */
    public final CatalogClient getClient() {
        return theClient;
    }

    /**
     * Returns the server of the catalog.
     *
     * @return the server of this catalog
     */
    public final CatalogServerEngine getServerEngine() {
        return theServerEngine;
    }

    /**
     * Returns the message resources associated with the catalog instance.
     *
     * @return message resources
     */
    public MessageResources getMessageResources() {
        return this.theMessageResources;
    }

    /**
     * Returns the category with the given GUID if already built. If the category with the given GUID was not found
     * <code>null</code> will be returned. If the category was marked as deleted it will be also returned!
     *
     * @param aCategoryGuid unique identifier of the category
     *
     * @return a instance of type <code>CatalogCategory</code>
     */
    public CatalogCategory getCategoryInternal(String aCategoryGuid) {
        return (CatalogCategory) theCategories.get(aCategoryGuid);
    }

    /**
     * Creates a new category for the catalog. The catalog has a association with the instance. Internal method which
     * can't be called via the public interface.
     *
     * @param aCategoryGuid DOCUMENT ME!
     *
     * @return a new instance of <code>CatalogCategory</code>
     */
    public CatalogCategory createCategoryInternal(String aCategoryGuid) {
        // temporarily the catalog is set as the parent of this category
        CatalogCategory aCatalogCategory = new CatalogCategory(this, aCategoryGuid);
        theCategories.put(aCategoryGuid, aCatalogCategory);

        return aCatalogCategory;
    }

    /**
     * Associates the catalog with the category. The {@link CatalogCategory#getGuid()} method must return a String that
     * can be used as a hashKey.
     *
     * @param aCategory the category to be added
     */
    public void addCategory(CatalogCategory aCategory) {
        theCategories.put(aCategory.getGuid(), aCategory);
    }

    /**
     * Remove the association between the catalog and one of its descendant categories.
     *
     * @param aCategory the category to be removed
     */
    public void removeCategory(CatalogCategory aCategory) {
        theCategories.remove(aCategory.getGuid());
    }

    void addAttribute(CatalogAttribute anAttribute, CatalogCompositeComponent anUser) {
        synchronized (theMapOfAllAttributes) {
            synchronized (theMapOfAttributeUsers) {
                theMapOfAllAttributes.put(anAttribute.getGuid(), anAttribute);

                Set theUsers = (Set) theMapOfAttributeUsers.get(anAttribute.getGuid());

                if (theUsers == null) {
                    Set aUserSet = new HashSet();
                    aUserSet.add(anUser);
                    theMapOfAttributeUsers.put(anAttribute.getGuid(), aUserSet);
                }
                else {
                    theUsers.add(anUser);
                }
            }
             //synchronized(theMapOfAttributeUsers)
        }
         //synchronized(theMapOfAllAttributes)
    }

    CatalogAttribute findAttribute(String anGuid) {
        CatalogAttribute anAttribute = null;

        synchronized (theMapOfAllAttributes) {
            anAttribute = (CatalogAttribute) theMapOfAllAttributes.get(anGuid);
        }

        return anAttribute;
    }

    Set findAttributeUsers(String anGuid) {
        Set aSet = null;

        synchronized (theMapOfAttributeUsers) {
            aSet = (Set) theMapOfAttributeUsers.get(anGuid);
        }
         //synchronized(theMapOfAttributeUsers)

        return ((aSet == null) ? new HashSet() : aSet);
    }

    void removeAttribute(CatalogAttribute anAttribute) {
        synchronized (theMapOfAllAttributes) {
            synchronized (theMapOfAttributeUsers) {
                Set theUsers = this.findAttributeUsers(anAttribute.getGuid());
                Iterator theUSersersIter = theUsers.iterator();

                while (theUSersersIter.hasNext()) {
                    CatalogCompositeComponent aUser = (CatalogCompositeComponent) theUSersersIter.next();
                    aUser.deleteAttributeInternal(anAttribute);
                }

                theMapOfAllAttributes.remove(anAttribute.getGuid());
                theMapOfAttributeUsers.remove(anAttribute.getGuid());
            }
             //synchronized(theMapOfAttributeUsers)
        }
         //synchronized(theMapOfAllAttributes)
    }

    /**
     * Starts the recursion to build an XML document from the tree. Every time that function is called the current
     * status of the catalog is frozen and converted to XML.
     *
     * @return the generated XML document
     */
    public Document toXML() {
        Document result = null;

        try {
            result = new CatalogXMLVisitor(this).start();
        }
        catch (ParserConfigurationException pce) {
            theStaticLocToLog.error(ICatalogMessagesKeys.PCAT_XML_ERR, new Object[] { Catalog.class.getName() }, pce);
        }

        return result;
    }

    /**
     * protected
     *
     * @param aBuilder DOCUMENT ME!
     */
    /**
     * Sets the builder instance.
     *
     * @param aBuilder the builder of the catalog
     */
    protected final void setCatalogBuilder(CatalogBuilder aBuilder) {
        this.theConcreteBuilder = aBuilder;
    }

    /**
     * Takes a document that specifies changes to the catalog and sends it to the remote catalog engine. If the commit
     * fails and it can be detected by the catalog client a catalog exception it throw
     *
     * @param theChanges The changes to the current catalog.
     *
     * @exception CatalogException In case the commit to the remote catalog engine fails and is detected by the client
     */
    abstract public void commitChangesRemote(Document theChanges)
                                      throws CatalogException;

    /**
     * private
     *
     * @param theXpp DOCUMENT ME!
     * @param aExpression DOCUMENT ME!
     * @param aDoc DOCUMENT ME!
     *
     * @return DOCUMENT ME!
     */
    /**
     * Small helper to evaluate an xpath expression.
     *
     * @param theXpp a <code>XPathParser</code> value
     * @param aExpression a <code>String</code> value
     * @param aDoc a <code>Document</code> value
     *
     * @return an <code>IXPvalue</code> value
     */
    private IXPvalue evaluate(XPathParser theXpp, String aExpression, Document aDoc) {
        Reader stream = new StringReader(aExpression);

        theXpp.ReInit(stream);

        ASTXPathExpr aXPathExpr = null;

        try {
            aXPathExpr = theXpp.XPathExpr();
        }
        catch (ParseException e) {
            //fail("Parse of correct expression failed");
            return null;
        }

        XPathDomParserVisitor aXPathDomVistor = new XPathDomParserVisitor(aDoc, aXPathExpr);

        ArrayList theContextList = new ArrayList();
        theContextList.add(aDoc);

        XPathContext aContext = new XPathContext(theContextList, 0);

        return aXPathDomVistor.evaluate(aContext);
    }

    /**
     * Describe <code>setDefaultLocale</code> method here.
     *
     * @param aDefaultLocale a <code>Locale</code> value
     *
     * @exception CatalogException if an error occurs
     */
    public synchronized void setDefaultLocale(Locale aDefaultLocale)
                                       throws CatalogException {
        if (!theSupportedLocales.contains(aDefaultLocale)) {
            throw new CatalogException(theMessageResources.getMessage(ICatalogMessagesKeys.PCAT_ERR_LOCALE_NOT_SUPP,
                                                                      aDefaultLocale.toString()));
        }
         // end of if ()

        theDefaultLocale = aDefaultLocale;

        return;
    }

    /**
     * DOCUMENT ME!
     *
     * @return DOCUMENT ME!
     */
    public synchronized Locale getDefaultLocale() {
        return theDefaultLocale;
    }

    /**
     * DOCUMENT ME!
     *
     * @return DOCUMENT ME!
     */
    public synchronized Iterator getSupportedLocales() {
        return theSupportedLocales.iterator();
    }
}