/*****************************************************************************
  Copyright (c) 2000, SAPMarkets Europe GmbH, Germany, All rights reserved.
  Created:      09 July 2001

  $Id: //sap/ESALES_base/30_SP_COR/src/_pcatAPI/java/com/sap/isa/catalog/impl/CatalogAttributeValue.java#3 $
  $Revision: #3 $
  $Change: 129234 $
  $DateTime: 2003/05/15 17:40:02 $
*****************************************************************************/

package com.sap.isa.catalog.impl;

//  catalog imports
import java.net.MalformedURLException;
import java.net.URL;
import java.text.DateFormat;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.Iterator;

import org.apache.struts.util.MessageResources;

import com.sap.isa.catalog.ICatalogMessagesKeys;
import com.sap.isa.catalog.boi.CatalogException;
import com.sap.isa.catalog.boi.IAttribute;
import com.sap.isa.catalog.boi.IAttributeValue;
import com.sap.isa.catalog.boi.IComponent;
import com.sap.isa.catalog.boi.IComponentChangedEvent;
import com.sap.isa.catalog.boi.IComponentEventListener;
import com.sap.isa.catalog.boi.IDetailCreatedEvent;
import com.sap.isa.catalog.boi.IDetailDeletedEvent;
import com.sap.isa.catalog.boi.IInterval;
import com.sap.isa.catalog.boi.IItem;
import com.sap.isa.catalog.boi.IQuantity;
import com.sap.isa.catalog.boi.IUnit;
import com.sap.isa.catalog.catalogvisitor.CatalogVisitor;
import com.sap.isa.core.logging.IsaLocation;

/**
 * This class implements the attribute value functionality for the catalog API.
 * <br/>
 * Only the getter-methods are synchronized. The factory methods for the builder
 * are not synchronized because this is done in the builder instance. Only the
 * builder is allowed to modify the catalog tree. <br/>
 * Users of the catalog should only work with the interface. All other
 * <code>public</code> methods of this class are for internal use only.
 *
 * @version     1.0
 */
public class CatalogAttributeValue
    extends CatalogComponent
    implements  IAttributeValue,
                IComponentEventListener
{
    /*
     *  CatalogComponent implements Serializable:
     *  Fields:
     *  theAttribute                        :   implements Serializable itself!
     *  theItem                             :   implements Serializable itself!
     *  theArributeValues                   :   theContainer implements Serializable!
     *                                          The parts will be Strings i.e. no problem
     *  theUnit                             :   implements Serializable itself!
     *  theArributeValuesToStringVersion    :   transient
     */

    private CatalogAttribute        theAttribute;
    private CatalogItem             theItem;
    private ArrayList               theAttributeValues = new ArrayList(0);
    private IUnit                   theUnit;
    private transient ArrayList     theAttributeValuesToStringVersion = null;
    // the logging instance
    private static IsaLocation theStaticLocToLog =
        IsaLocation.getInstance(CatalogAttributeValue.class.getName());

    /**
     * Constructs a new value instance.
     *
     * @param aItem the item of this value
     */
    public CatalogAttributeValue(CatalogItem aItem)
    {
        this.theItem = aItem;
    }

    /**
     * Constructs a new value instance.
     *
     * @param aItem      the item of this value
     * @param aAttribute the attribute this value belongs to
     */
    public CatalogAttributeValue(CatalogItem aItem, CatalogAttribute aAttribute)
    {
        this.theItem = aItem;
        this.theAttribute = aAttribute;

        //listen to the your attribute
        // null might happen in the requisite case see factory method in item
        //if(theAttribute!=null)
        //    theAttribute.addComponentEventListener(this);
    }

    public void assign(CatalogVisitor visitor)
    {
        visitor.visitAttributeValue(this);
    }

    public Catalog getRoot()
    {
        return theItem.getRoot();
    }

    public synchronized boolean isMultiValue()
    {
        return (theAttributeValues.size()>1)?true:false;
    }

/* ************************* IAttributeValue ******************************** */

    public synchronized IAttribute getAttribute()
    {
        return theAttribute;
    }

    public synchronized String getAttributeName()
    {
        return theAttribute.getName();
    }

    public synchronized IItem getParent()
    {
        return theItem;
    }

    public synchronized String getAttributeDescription()
    {
        return theAttribute.getDescription();
    }

    public synchronized String getAttributeGuid()
    {
        return theAttribute.getGuid();
    }

    public synchronized IUnit getUnit()
    {
        //unused: IUnit unit = null;
        IQuantity quantity = theAttribute.getQuantity();
        // if we got an quantity then we have by contract a unit!
        if(quantity!=null && this.theUnit == null)
            return quantity.getDefaultUnit();
        else
            return this.theUnit;
    }

    public synchronized String getAsString()
    {
        //The conversion must have be done at the monment of creation!
        getRoot().setTimeStamp();

        Iterator theValueIter = theAttributeValues.iterator();
        StringBuffer aBuffer = new StringBuffer();
        while(theValueIter.hasNext())
        {
            //To string what so ever comes out of the pipeline
            Object theValue = theValueIter.next();
            aBuffer.append(theValue.toString());
            if(theValueIter.hasNext())
                aBuffer.append(';');
        }
        return aBuffer.toString();
    }

    public synchronized Iterator getAllAsString()
    {
        //The conversion must have be done at the moment of creation!!!!!
        getRoot().setTimeStamp();

        if(theAttribute.getType() == IAttribute.TYPE_ID_STRING)
            return Collections.unmodifiableList(theAttributeValues).iterator();
        else //Here we got to string the stuff
        {
            if(theAttributeValuesToStringVersion == null) //lazy
            {
                theAttributeValuesToStringVersion = new ArrayList();
                Iterator theValueIter = theAttributeValues.iterator();
                while(theValueIter.hasNext())
                {
                    //To string what so ever comes out of the pipeline
                    Object theValue = theValueIter.next();
                    theAttributeValuesToStringVersion.add(theValue.toString());
                }
            }
            return Collections.unmodifiableList(
                theAttributeValuesToStringVersion).iterator();
        }
    }
    
    /**
     * Returns the number of values in the valuelist
     */
    public int getNoOfValues() {
        return theAttributeValues.size();
    }

    public synchronized Double getAsDouble()
    {
        getRoot().setTimeStamp();

        //The conversion must have be done at the monment of creation!
        Iterator values = getAllValues();
        boolean test = (values != null) &&
            (theAttribute.getType() == IAttribute.TYPE_ID_DOUBLE);
        return (test) ? (Double)values.next() : null;
    }

    public synchronized Date getAsDate()
    {
        getRoot().setTimeStamp();

        //The conversion must have be done at the monment of creation!
        Iterator values = getAllValues();
        boolean test = (values != null) &&
            (theAttribute.getType() == IAttribute.TYPE_ID_DATE);
        return (test)?(Date)values.next():null;
    }

    public synchronized IInterval getAsInterval()
    {
        getRoot().setTimeStamp();

        //The conversion must have be done at the monment of creation!
        Iterator values = getAllValues();
        boolean test = (values != null) &&
            (theAttribute.getType() == IAttribute.TYPE_ID_INTERVAL);
        return (test)?(IInterval)values.next():null;
    }

    public synchronized URL getAsURL()
    {
        getRoot().setTimeStamp();

        //The conversion must have be done at the monment of creation!
        Iterator values = getAllValues();
        //unused: boolean test = (values != null) &&
        //    (theAttribute.getType() == theAttribute.TYPE_ID_URL);
        return (values!=null)?(URL)values.next():null;
    }

    public synchronized Object getAsObject()
    {
        getRoot().setTimeStamp();

        Iterator values = getAllValues();
        return (values!=null)?values.next():null;
    }

    public synchronized Boolean getAsBoolean()
    {
        getRoot().setTimeStamp();

        //The conversion must have be done at the monment of creation!
        Iterator values = getAllValues();
        //unused: boolean test = (values != null) &&
        //    (theAttribute.getType() == theAttribute.TYPE_ID_BOOLEAN);
        return (values!=null)?(Boolean)values.next():null;
    }

    public synchronized Long getAsLong()
    {
        getRoot().setTimeStamp();

        //The conversion must have be done at the monment of creation!
        Iterator values = getAllValues();
        return (values!=null)?(Long)values.next():null;
    }

    public synchronized Integer getAsInteger()
    {
        getRoot().setTimeStamp();

        //The conversion must have be done at the monment of creation!
        Iterator values = getAllValues();
        return (values!=null)?(Integer)values.next():null;
    }

    public synchronized Iterator getAllValues()
    {
        getRoot().setTimeStamp();

        return Collections.unmodifiableList(theAttributeValues).iterator();
    }

/* ****************************** set/add *Internal ************************* */

    /**
     * Internal setter/adder methods
     */
    public void setAsStringInternal(String aValueAsString)
    {
        theAttributeValues.clear();
        theAttributeValues.add(aValueAsString);
        this.dispatchChangedEvent();
        return;
    }

    public void addAsStringInternal(String aValueAsString)
    {
        theAttributeValues.add(aValueAsString);
        if(theStaticLocToLog.isDebugEnabled())
        {
            MessageResources aResource = getRoot().getMessageResources();
            String aMsg = aResource.getMessage(
                ICatalogMessagesKeys.PCAT_ATTR_VALUE_ADDED_VALUE,
                aValueAsString);
            theStaticLocToLog.debug(aMsg);
        }
        this.dispatchChangedEvent();
    }

    public void setAsLongInternal(Long aValueAsLong)
    {
        theAttributeValues.clear();
        theAttributeValues.add(aValueAsLong);
        this.dispatchChangedEvent();
        return;
    }

    public void addAsLongInternal(Long aValueAsLong)
        throws CatalogException
    {
        theAttributeValues.add(aValueAsLong);
        this.dispatchChangedEvent();
        return;
    }

    public void setAsIntervalInternal(IInterval aValueAsInterval)
    {
        theAttributeValues.clear();
        theAttributeValues.add(aValueAsInterval);
        this.dispatchChangedEvent();
        return;
    }

    public void addAsIntervalInternal(IInterval aValueAsInterval)
    {
        theAttributeValues.add(aValueAsInterval);
        this.dispatchChangedEvent();
        return;
    }

    public void setAsIntegerInternal(Integer aValueAsInteger)
    {
        theAttributeValues.clear();
        theAttributeValues.add(aValueAsInteger);
        this.dispatchChangedEvent();
        return;
    }

    public void addAsIntegerInternal(Integer aValueAsInteger)
    {
        theAttributeValues.add(aValueAsInteger);
        this.dispatchChangedEvent();
        return;
    }

    public void setAsDoubleInternal(Double aValueAsDouble)
    {
        theAttributeValues.clear();
        theAttributeValues.add(aValueAsDouble);
        this.dispatchChangedEvent();
        return;
    }

    public void addAsDoubleInternal(Double aValueAsDouble)
    {
        theAttributeValues.add(aValueAsDouble);
        this.dispatchChangedEvent();
        return;
    }

    public void setAsDateInternal(Date aValueAsDate)
    {
        theAttributeValues.clear();
        theAttributeValues.add(aValueAsDate);
        this.dispatchChangedEvent();
        return;
    }

    public void addAsDateInternal(Date aValueAsDate)
    {
        theAttributeValues.add(aValueAsDate);
        this.dispatchChangedEvent();
        return;
    }

    public void setAsBooleanInternal(Boolean aValueAsBoolean)
    {
        theAttributeValues.clear();
        theAttributeValues.add(aValueAsBoolean);
        this.dispatchChangedEvent();
        return;
    }

    public void addAsBooleanInternal(Boolean aValueAsBoolean)
    {
        theAttributeValues.add(aValueAsBoolean);
        this.dispatchChangedEvent();
        return;
    }

    public void setAsObjectInternal(Object aValueAsObject)
    {
        theAttributeValues.clear();
        theAttributeValues.add(aValueAsObject);
        this.dispatchChangedEvent();
    }

    public void addAsObjectInternal(Object aValueAsObject)
    {
        theAttributeValues.add(aValueAsObject);
        this.dispatchChangedEvent();
    }

    /**
     * Sets the value of the instance. This method should be used if the
     * attribute can hold multiple values.
     *
     * @param aValueAsStringArray an array of values
     */
    public void setAsStringInternal(String[] aValueAsStringArray)
    {
        for(int i=0; i < aValueAsStringArray.length; i++)
          this.addAsStringInternal(aValueAsStringArray[i]);
    }

    /**
     * Set the assosciation to the attribute by handing over the guid of the
     * Attribute. The value tries to find its attribute over its parents if
     * successfull it returns a reference to the found attribute
     *
     * @return IAttribute the associated attribute.
     */
    public IAttribute setAttribute(String aAttributeGuid)
    {
        try
        {
            CatalogCategory theParentCat = theItem.getCategory();
            if(theParentCat != null)
              theAttribute = theParentCat.getAttributeInternal(aAttributeGuid);
            else //current hack for query
              theAttribute = getRoot().getAttributeInternal(aAttributeGuid);
        }
        catch(CatalogException ex)
        {
            theAttribute = null;
        }

        if (theStaticLocToLog.isDebugEnabled())
        {
            MessageResources aResource = getRoot().getMessageResources();
            String aMsg = aResource.getMessage(
                ICatalogMessagesKeys.PCAT_ATTR_VALUE_SET_ATTR,
                aAttributeGuid,
                ((theAttribute!=null)?theAttribute.getGuid():"notFound"));
            theStaticLocToLog.debug(aMsg);
        }

        //listen to your attribute
        //theAttribute.addComponentEventListener(this);

        return theAttribute;
    }

/*************************** protected ****************************************/

    protected Object parseValue(String aValueAsString)
        throws CatalogException
    {
        Object aValue = aValueAsString;

        String internType = theAttribute.getType().intern();

        if(internType == IAttribute.TYPE_ID_STRING.intern())
            return aValueAsString;
        else if(internType == IAttribute.TYPE_ID_DOUBLE.intern())
        {
            Double aDouble = new Double(0);
            try
            {
                aDouble = Double.valueOf(aValueAsString);
            }
            catch(NumberFormatException nfe)
            {
                theStaticLocToLog.error(ICatalogMessagesKeys.PCAT_ERR_PARSING_TYPE,
                    new Object[]{CatalogAttributeValue.class.getName()},nfe);

                throw new CatalogException(nfe.toString());
            }
            aValue = aDouble;
        }
        else if(internType == IAttribute.TYPE_ID_INTEGER.intern())
        {
            Integer aInteger = new Integer(0);
            try
            {
                aInteger = Integer.valueOf(aValueAsString);
            }
            catch(NumberFormatException nfe)
            {
                 theStaticLocToLog.error(ICatalogMessagesKeys.PCAT_ERR_PARSING_TYPE,
                    new Object[]{CatalogAttributeValue.class.getName()},nfe);
                    
                throw new CatalogException(nfe.toString());
            }
            aValue = aInteger;
        }
        else if(internType == IAttribute.TYPE_ID_LONG.intern())
        {
            Long aLong = new Long(0L);
            try
            {
                aLong = Long.valueOf(aValueAsString);
            }
            catch(NumberFormatException nfe)
            {
                theStaticLocToLog.error(ICatalogMessagesKeys.PCAT_ERR_PARSING_TYPE,
                    new Object[]{CatalogAttributeValue.class.getName()},nfe);
 
                throw new CatalogException(nfe.toString());
            }
            aValue = aLong;
        }
        else if(internType == IAttribute.TYPE_ID_DATE.intern())
        {
            Date aDate = new Date(); //now
            try
            {
                aDate = DateFormat.getDateInstance().parse(aValueAsString);
            }
            catch(ParseException pe)
            {
                theStaticLocToLog.error(ICatalogMessagesKeys.PCAT_ERR_PARSING_TYPE,
                    new Object[]{CatalogAttributeValue.class.getName()},pe);
 
                throw new CatalogException(pe.toString());
            }
            aValue = aDate;
        }
        else if(internType == IAttribute.TYPE_ID_URL.intern())
        {
            URL aURL = null;
            try
            {
                aURL = new URL(aValueAsString);
            }
            catch(MalformedURLException mue)
            {
                theStaticLocToLog.error(ICatalogMessagesKeys.PCAT_ERR_PARSING_TYPE,
                    new Object[]{CatalogAttributeValue.class.getName()},mue);
 
            }
            aValue = aURL;
         }
        else if(internType == IAttribute.TYPE_ID_BOOLEAN.intern())
        {
            Boolean aBool = Boolean.valueOf(aValueAsString);
            aValue = aBool;
        }
        else if(internType == IAttribute.TYPE_ID_INTERVAL.intern())
        {
            //here we need some creative ideas
            aValue = aValueAsString;
        }
        else if(internType == IAttribute.TYPE_ID_OBJECT.intern())
        {
            //here we need some creative ideas
            aValue = aValueAsString;
        }
        return aValue;
    }

/* ******************* IComponentEventListener ****************************** */

    public void componentChanged(IComponentChangedEvent anEvent)
    {
        //The one and only place where a child administeres the relation to
        //his parent
        IComponent theChangedComponent = anEvent.getComponent();
        if(theChangedComponent.getState()==IComponent.State.DELETED)
        {
            this.setStateDeleted();
            this.theItem.deleteAttributeValueInternal(this);
        }
    }

    public void detailCreated(IDetailCreatedEvent p0)
    {
        //really don't care
        ;
    }

    public void detailDeleted(IDetailDeletedEvent p0)
    {
        //really don't care
        ;
    }
}
