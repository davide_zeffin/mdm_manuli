/**
 * Copyright (c) 2000, SAP AG, Germany, All rights reserved. 
 * Created:      09 July 2001 
 * $Id: //sap/ESALES_base/30_SP_COR/src/_pcatAPI/java/com/sap/isa/catalog/impl/CatalogClient.java#3 $ 
 * $Revision: #3 $
 * $Change: 150872 $ 
 * $DateTime: 2003/09/29 09:18:51 $
 */
package com.sap.isa.catalog.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Locale;

import com.sap.isa.catalog.boi.IActor;
import com.sap.isa.catalog.boi.IClient;
import com.sap.isa.catalog.boi.IView;
import com.sap.isa.core.TechKey;
import com.sap.isa.core.logging.IsaLocation;

/**
 * Simple implementation class of the <code>IClient</code> interface. This class is no component of the catalog tree in
 * the stricter sense.
 *
 * @version 1.0
 */
public class CatalogClient implements IClient, Serializable {
    
    protected static IsaLocation log = IsaLocation.getInstance(CatalogClient.class.getName());
    
    protected Locale theLocale;
    protected String theName;
    protected String thePWD;
    protected ArrayList theViews = new ArrayList();
    protected int catalogStatus = CATALOG_STATE_ACTIVE;
    protected TechKey stagingCatalogKey;

    /**
     * Default constructor
     */
    protected CatalogClient() {
    }

    /**
     * Constructor.
     *
     * @param aClient the source for the new instance
     */
    public CatalogClient(IClient aClient) {
        
        this.theLocale = aClient.getLocale();
        this.theName = aClient.getName();
        this.thePWD = aClient.getPWD();
        this.catalogStatus = aClient.getCatalogStatus();
        this.stagingCatalogKey = aClient.getStagingCatalogKey();

        Iterator toBeClonedIter = aClient.getViews().iterator();

        while (toBeClonedIter.hasNext()) {
            IView aView = (IView) toBeClonedIter.next();
            theViews.add(new CatalogView(aView));
        }
    }

    /**
     * Constructor.
     *
     * @param anActor the source for the new instance
     */
    public CatalogClient(IActor anActor) {
        this.theLocale = Locale.ENGLISH;
        this.theName = anActor.getName();
        this.thePWD = anActor.getPWD();
    }

    /**
     * Constructor.
     *
     * @param aLocale the locale of the user
     * @param aName the user name
     * @param aPWD the password
     * @param views the list of views
     */
    public CatalogClient(Locale aLocale, String aName, String aPWD, String views[]) {
        
        log.entering("CatalogClient(Locale aLocale, String aName, String aPWD, String views[])");
        
        this.theLocale = aLocale;
        this.theName = aName;
        this.thePWD = aPWD;

        for (int i = 0; (views != null) && (i < views.length); i++) {
            if (views[i] != null) {
                if (log.isDebugEnabled()) {
                    log.debug("View \"" + views[i] + "\"");
                }
                theViews.add(new CatalogView(views[i]));
            }
        }
        
        if (views == null || views.length == 0) {
            log.debug("No Views restriction");
        }
        
        log.exiting();
    }

    /**
     * Returns the locale.
     *
     * @return the locale of the client
     */
    public Locale getLocale() {
        if (log.isDebugEnabled()) {
            log.debug("CatalogClient:getLocale()" + theLocale);
        }

        return theLocale;
    }

    /**
     * Returns the name.
     *
     * @return the name of the client
     */
    public String getName() {
        if (log.isDebugEnabled()) {
            log.debug("CatalogClient:getName() " + theName);
        }
        return theName;
    }

    /**
     * Returns the password.
     *
     * @return the password of the client
     */
    public String getPWD() {
        if (log.isDebugEnabled()) {
            log.debug("CatalogClient:getPWD() " + thePWD);
        }
        return thePWD;
    }

    /**
     * Returns the views. The instances are of type <code>IView</code>.
     *
     * @return the list of Views
     */
    public ArrayList getViews() {
        
        log.debug("CatalogClient:getViews()");

        if (log.isDebugEnabled()) {
            if (theViews.size() == 0) {
                log.debug("no views");
            }
            else {
                for (int i = 0; i < theViews.size(); i++) {
                    if (log.isDebugEnabled()) {
                        log.debug("view " + i + ": \"" + ((IView) theViews.get(i)).getGuid() + "\"");
                    }
                }
            }
        }
        
        return theViews;
    }

    /**
     * Returns an empty Itertor.
     *
     * @return an <code>Iterator</code> value
     */
    public Iterator getPermissions() {
        return (new ArrayList()).iterator();
    }

    /**
     * The name is currenly the guid.
     *
     * @return a <code>String</code> value
     */
    public String getGuid() {
        return theName;
    }

    /**
     * Returns the catalog status
     *
     * @return int catalogStatus the status of the catalogue to be read
     */
    public int getCatalogStatus() {
        return catalogStatus;
    }

    /**
     * Returns the techKey of the staging catalogue to read
     *
     * @return String stagingCatalogKey the id of the staging catalogue to read
     */
    public TechKey getStagingCatalogKey() {
        return stagingCatalogKey;
    }

    /**
     * Sets the catalog status
     *
     * @param catalogStatus catalogStatus the status of the catalogue to be read
     */
    public void setCatalogStatus(int catalogStatus) {
        this.catalogStatus = catalogStatus;
    }

    /**
     * Sets the id of the staging catalogue to read
     *
     * @param stagingCatalogKey stagingCatalogKey the TechKey of the staging catalogue to read!
     */
    public void setStagingCatalogKey(TechKey stagingCatalogKey) {
        this.stagingCatalogKey = stagingCatalogKey;
    }

    /**
     * Returns null; Not supported.
     *
     * @return a <code>String</code> value
     */
    public String getCookie() {
        return null;
    }

    /**
     * DOCUMENT ME!
     *
     * @return DOCUMENT ME!
     */
    public String toString() {
        StringBuffer aBuffer = new StringBuffer("");
        aBuffer.append("Name: ");
        aBuffer.append(getName()).append(";");
        aBuffer.append("Guid: ");
        aBuffer.append(getGuid()).append(";");
        aBuffer.append("Cookie: ");
        aBuffer.append(getCookie()).append(";");

        if (theViews.size() > 0) {
            aBuffer.append("Views: ");

            Iterator anIter = theViews.iterator();

            while (anIter.hasNext()) {
                IView aView = (IView) anIter.next();
                aBuffer.append(aView.getGuid());

                if (anIter.hasNext()) {
                    aBuffer.append(",");
                }
                 // end of if ()
            }
        }
         // end of if ()

        return aBuffer.toString();
    }
}