/*****************************************************************************
  Copyright (c) 2000, SAPMarkets Europe GmbH, Germany, All rights reserved.
  Created:      2002

  $Id: //sap/ESALES_base/30_SP_COR/src/_pcatAPI/java/com/sap/isa/catalog/impl/CatalogDeletedEvent.java#3 $
  $Revision: #3 $
  $Change: 150872 $
  $DateTime: 2003/09/29 09:18:51 $
*****************************************************************************/
package com.sap.isa.catalog.impl;

import java.util.EventObject;

import com.sap.isa.catalog.boi.ICatalogDeletedEvent;
import com.sap.isa.catalog.boi.ICatalogInfo;
import com.sap.isa.catalog.boi.IServerEngine;

/**
 * Event that is fired if a catalog is deleted for a server.
 *
 * @version     1.0
 */
public class CatalogDeletedEvent
    extends EventObject
    implements ICatalogDeletedEvent
{
    private CatalogServerEngine.CatalogInfo theDeletedCatalog;

    public CatalogDeletedEvent(CatalogServerEngine aParent,
                                     CatalogServerEngine.CatalogInfo aDoomedCatalog)
    {
        super(aParent);
        this.theDeletedCatalog = aDoomedCatalog;
    }

    public IServerEngine getServer()
    {
        return (IServerEngine) getSource();
    }

    public ICatalogInfo getCatalogInfo()
    {
        return theDeletedCatalog;
    }
}
