/*****************************************************************************
  Copyright (c) 2000, SAPMarkets Europe GmbH, Germany, All rights reserved.
  Created:      09 July 2001

  $Id: //sap/ESALES_base/30_SP_COR/src/_pcatAPI/java/com/sap/isa/catalog/impl/CatalogDetailDeletedEvent.java#3 $
  $Revision: #3 $
  $Change: 150872 $
  $DateTime: 2003/09/29 09:18:51 $
*****************************************************************************/
package com.sap.isa.catalog.impl;

import java.util.EventObject;

import com.sap.isa.catalog.boi.IComponent;
import com.sap.isa.catalog.boi.IDetail;
import com.sap.isa.catalog.boi.IDetailDeletedEvent;

/**
 * Event that is fired if a detail is deleted for a component.
 *
 * @version     1.0
 */
public class CatalogDetailDeletedEvent
    extends EventObject
    implements IDetailDeletedEvent
{
    private CatalogDetail theDeletedDetail;

    public CatalogDetailDeletedEvent(CatalogComponent aParent,
                                     CatalogDetail aNewDetail)
    {
        super(aParent);
        this.theDeletedDetail = aNewDetail;
    }

    public IComponent getComponent()
    {
        return (IComponent) getSource();
    }

    public IDetail getDetail()
    {
        return theDeletedDetail;
    }
}