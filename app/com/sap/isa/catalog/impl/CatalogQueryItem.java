/*****************************************************************************
  Copyright (c) 2000, SAPMarkets Europe GmbH, Germany, All rights reserved.
  Created:      09 July 2001

  $Id: //sap/ESALES_base/30_SP_COR/src/_pcatAPI/java/com/sap/isa/catalog/impl/CatalogQueryItem.java#2 $
  $Revision: #2 $
  $Change: 129221 $
  $DateTime: 2003/05/15 17:20:58 $
*****************************************************************************/
package com.sap.isa.catalog.impl;

//  catalog imports
import com.sap.isa.catalog.boi.IAttribute;
import com.sap.isa.catalog.boi.IAttributeValue;
import com.sap.isa.catalog.boi.IQueryItem;
import com.sap.isa.catalog.catalogvisitor.CatalogVisitor;

/**
 * This class implements the query item functionality for the catalog API. <br/>
 * Only the getter-methods are synchronized. The factory methods for the builder
 * are not synchronized because this is done in the builder instance. Only the
 * builder is allowed to modify the catalog tree. <br/>
 * Users of the catalog should only work with the interface. All other
 * <code>public</code> methods of this class are for internal use only.
 *
 * @version     1.0
 */
public class CatalogQueryItem
    extends CatalogItem
    implements  IQueryItem
{
    /*
     *  CatalogItem implements Serializable:
     *  Fields:
     *  theCategoryGuid     :   no problem!
     */
    private String theCategoryGuid;

    /**
     * Creates a new item of a query.
     *
     * @param aQuery    the query of this item
     * @param aCateGuid the guid of this category
     * @param aItemGuid the guid of the item
     */
    public CatalogQueryItem(CatalogQuery aQuery,
                            String aCateGuid,
                            String aItemId)
    {
        super(aQuery, aItemId);
        theCategoryGuid = aCateGuid;
    }

    public void assign(CatalogVisitor visitor)
    {
        visitor.visitQueryItem(this);
    }

    public synchronized String getCategoryGuid()
    {
        return theCategoryGuid;
    }

    /**
     * Sets the guid of the category to which this items belongs to.
     *
     * @param aGuid the guid of the category
     */
    public synchronized void setCategoryGuid(String aGuid)
    {
        theCategoryGuid = aGuid;
    }

    /**
     * Query items can't be changed. Therefore this method throws the
     * <code>UnsupportedOperationException</code> exception.
     */
    public void setName(String aName)
    {
        throw new UnsupportedOperationException();
    }

    /**
     * Query items can't be changed. Therefore this method throws the
     * <code>UnsupportedOperationException</code> exception.
     */
    public void setProductGuid(String aGuid)
    {
        throw new UnsupportedOperationException();
    }

    /**
     * Query items can't be changed. Therefore this method throws the
     * <code>UnsupportedOperationException</code> exception.
     */
    public IAttributeValue createAttributeValue(IAttribute theAttributeOfTheValue)
    {
        throw new UnsupportedOperationException();
    }

    /**
     * Query items can't be changed. Therefore this method throws the
     * <code>UnsupportedOperationException</code> exception.
     */
    public void deleteAttributeValue(IAttributeValue value)
    {
        throw new UnsupportedOperationException();
    }
}
