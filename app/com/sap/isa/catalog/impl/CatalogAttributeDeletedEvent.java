/*****************************************************************************
  Copyright (c) 2000, SAPMarkets Europe GmbH, Germany, All rights reserved.
  Created:      09 July 2001

  $Id: //sap/ESALES_base/30_SP_COR/src/_pcatAPI/java/com/sap/isa/catalog/impl/CatalogAttributeDeletedEvent.java#3 $
  $Revision: #3 $
  $Change: 150872 $
  $DateTime: 2003/09/29 09:18:51 $
*****************************************************************************/
package com.sap.isa.catalog.impl;

import java.util.EventObject;

import com.sap.isa.catalog.boi.IAttribute;
import com.sap.isa.catalog.boi.IAttributeDeletedEvent;
import com.sap.isa.catalog.boi.IComponent;

/**
 * Event that is fired if an attribute is deleted from a  composite.
 *
 * @version     1.0
 */
public class CatalogAttributeDeletedEvent
    extends EventObject
    implements IAttributeDeletedEvent
{
    private CatalogAttribute theDeletedChild;

    public CatalogAttributeDeletedEvent(CatalogCompositeComponent aParent,
                                        CatalogAttribute aDeletedChild)
    {
        super(aParent);
        this.theDeletedChild = aDeletedChild;
    }

    public IComponent getComponent()
    {
        return (IComponent) getSource();
    }

    public IAttribute getAttribute()
    {
        return theDeletedChild;
    }
}