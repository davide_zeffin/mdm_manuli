/*****************************************************************************
  Copyright (c) 2000, SAPMarkets Europe GmbH, Germany, All rights reserved.
  Created:      09 July 2001

  $Id: //sap/ESALES_base/30_SP_COR/src/_pcatAPI/java/com/sap/isa/catalog/impl/CatalogAttribute.java#2 $
  $Revision: #2 $
  $Change: 129234 $
  $DateTime: 2003/05/15 17:40:02 $
*****************************************************************************/

package com.sap.isa.catalog.impl;

//  catalog specific imports
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;
import java.util.StringTokenizer;

import com.sap.isa.catalog.boi.CatalogException;
import com.sap.isa.catalog.boi.IAttribute;
import com.sap.isa.catalog.boi.IDomain;
import com.sap.isa.catalog.boi.IQuantity;
import com.sap.isa.catalog.boi.IUnit;
import com.sap.isa.catalog.catalogvisitor.CatalogVisitor;

/**
 * This class implements the attribute functionality for the catalog API. <br/>
 * Only the getter-methods are synchronized. The factory methods for the builder
 * are not synchronized because this is done in the builder instance. Only the
 * builder is allowed to modify the catalog tree. <br/>
 * Users of the catalog should only work with the interface. All other
 * <code>public</code> methods of this class are for internal use only.
 *
 * @version     1.0
 */
public class CatalogAttribute
    extends CatalogComponent
    implements IAttribute,
               Comparable
{
    /*
     *  CatalogComponent implements Serializable
     *  Fields:
     *  theComponent            :   implements Serializable itself!
     *  final static stuff      :   no problem!
     *  theAttributeName        :   no problem!
     *  theAttributeGuid        :   no problem!
     *  theAttributeDescription :   no problem!
     *  theFixedValues          :   theContainer implements Serializable!
     *                              The parts implement Serializable themself!
     *  theType                 :   no problem!
     *  isMultiValued           :   no problem!
     *  isLanguageDependent : no problem!
     *  theDomain;              :   implements Serializable itself!
     *  theQuantity;            :   implements Serializable itself!
     */

    private final static String STRINGID  = "String";
    private final static String URLID     = "URL";
    private final static String INTEGERID = "Integer";
    private final static String DOUBLEID  = "Double";
    private final static String BOOLEANID = "Boolean";
    private final static String LONGID    = "Long";
    private final static String DATEID    = "Date";
    private final static String INTEVALID = "Interval";
    private final static String OBJECTID  = "Object";
    
    private static ArrayList emptyFixedValues = new ArrayList(0);

    private     CatalogCompositeComponent theComponent;
    private     String theAttributeName;         // language independent
    private     String theAttributeGuid;
    private     String theAttributeDescription;  // language dependent
    private     ArrayList theFixedValues = null;
    private     String theType        = TYPE_ID_STRING;  // default
    private     boolean isMultiValued  = false;           // default
    private     boolean isLanguageDependent = false; // default
    private     IDomain theDomain;                        // default null
    private     IQuantity theQuantity;                      // default null

    /**
     * Constructs a new attribute.
     *
     * @param aComponent the composite component to which this attribute belongs to
     */
    public CatalogAttribute(CatalogCompositeComponent aComponent)
    {
        this.theComponent = aComponent;
    }

        /**
         * Constructs a new attribute.
         *
         * @param aComponent  the composite component to which this attribute belongs to
         * @param aGuid       the guid of the attribute
         * @param aTypeId     the id of the type of the attribute
         * @param aQuantityId the Id of the quantity of the attribute, optional
         * @param unitIds     a list of unit Ids of the attribute, optional
         * @param isLanguageDependent a <code>boolean</code> value
         */
    public CatalogAttribute(CatalogCompositeComponent aComponent,
                            String aGuid,
                            String aTypeId,
                            String aQuantityId,
                            String unitIds,
                            boolean isLanguageDependent)
    {
        this.theComponent = aComponent;
        this.theAttributeGuid = aGuid;
        this.theType = aTypeId;
        if(aQuantityId != null)
            theQuantity = CatalogQuantityFactory.getInstance().getQuantity(aQuantityId);
        if(theQuantity != null && unitIds != null)
        {
            // we get IDREFS
            StringTokenizer aTokenizer = new StringTokenizer(unitIds," ");
            while(aTokenizer.hasMoreTokens())
            {
                String aUnitId = aTokenizer.nextToken();
                IUnit aUnit = CatalogUnitFactory.getInstance().getUnit(aUnitId);
                ((CatalogQuantityFactory.CatalogQuantity) theQuantity).addUnit(aUnit);
            }
        }
        this.isLanguageDependent=isLanguageDependent;
    }
    

        /**
         * Constructs a new attribute.
         *
         * @param aComponent  the composite component to which this attribute belongs to
         * @param aGuid       the guid of the attribute
         * @param aTypeId     the id of the type of the attribute
         * @param aQuantityId the Id of the quantity of the attribute, optional
         * @param unitIds     a list of unit Ids of the attribute, optional
         */
    public CatalogAttribute(CatalogCompositeComponent aComponent,
                            String aGuid,
                            String aTypeId,
                            String aQuantityId,
                            String unitIds)
    {
        this.theComponent = aComponent;
        this.theAttributeGuid = aGuid;
        this.theType = aTypeId;
        if(aQuantityId != null)
            theQuantity = CatalogQuantityFactory.getInstance().getQuantity(aQuantityId);
        if(theQuantity != null && unitIds != null)
        {
            // we get IDREFS
            StringTokenizer aTokenizer = new StringTokenizer(unitIds," ");
            while(aTokenizer.hasMoreTokens())
            {
                String aUnitId = aTokenizer.nextToken();
                IUnit aUnit = CatalogUnitFactory.getInstance().getUnit(aUnitId);
                ((CatalogQuantityFactory.CatalogQuantity) theQuantity).addUnit(aUnit);
            }
        }
    }

    /**
     * Constructs a new attribute.
     *
     * @param aComponent  the composite component to which this attribute belongs to
     * @param aGuid       the guid of the attribute
     * @param aTypeId     the id of the type of the attribute
     * @param aQuantity   the Quantity of the attribute, optional
     */
    public CatalogAttribute(CatalogCompositeComponent aComponent,
                            String aGuid,
                            String aTypeId,
                            IQuantity aQuantity)
    {
        this.theComponent = aComponent;
        this.theAttributeGuid = aGuid;
        this.theType = aTypeId;
        this.theQuantity = aQuantity;
    }

    public void assign(CatalogVisitor visitor)
    {
        visitor.visitAttribute(this);
    }

    public Catalog getRoot()
    {
        return theComponent.getRoot();
    }

    public boolean isLanguageDependent()
    {
        return isLanguageDependent;
    }
    

/* **************************** IAttribute ********************************** */

    public boolean isMultiValued()
    {
         return isMultiValued;
    }

    public boolean isBase()
    {
        boolean result;
        try
        {
            result =    getRoot().getAttributeInternal(theAttributeGuid)==null?
                        false:true;
        }
        catch(CatalogException ex)
        {
            result = false;
        }
        return result;
    }

    public synchronized String getName()
    {
        return theAttributeName;
    }

    public synchronized String getType()
    {
        return theType;
    }

    public synchronized String getDescription()
    {
        return theAttributeDescription;
    }

    public synchronized IQuantity getQuantity()
    {
        return theQuantity;
    }

    public synchronized IDomain getDomain()
    {
        return theDomain;
    }

    public synchronized Iterator getFixedValues()
    {
        getRoot().setTimeStamp();

        if (theFixedValues != null) {
            return Collections.unmodifiableList(theFixedValues).iterator();
        }
        else {
            return Collections.unmodifiableList(emptyFixedValues).iterator();
        } 
    }
    
    public int getFixedValuesSize() {
        int valSize = 0;

        if (theFixedValues != null) {
            valSize = theFixedValues.size();
        }
        
        return valSize;
    }

    public synchronized Iterator getFixedValues(Comparator aComparator)
    {
        
        ArrayList values = null;
        
        getRoot().setTimeStamp();

        if (theFixedValues != null) {
            values = (ArrayList)theFixedValues.clone();
        }
        else {
            values = (ArrayList)emptyFixedValues.clone();
            
        }

        Collections.sort(values, aComparator);
        return values.iterator();
    }

    public synchronized String getGuid()
    {
        if(theAttributeGuid == null || theAttributeGuid.length() == 0)
            return theAttributeName;

        return theAttributeGuid;
    }

/* **************************** set*Internal ******************************** */

    public void setIsLanguageDependentInternal(boolean isLanguageDependent)
    {
        this.isLanguageDependent=isLanguageDependent;

        //inform your listeners
        //for the builder no problem because there are definitly no listeners
        this.dispatchChangedEvent();        
        return;
    }
    
    /**
     * Sets the name of the attribute. Internal method which
     * can't be called via the public interface.
     *
     * @param aName the name of the attribute to be set
     */
    public void setNameInternal(String aName)
    {
        this.theAttributeName = aName;

        //inform your listeners
        //for the builder no problem because there are definitly no listeners
        this.dispatchChangedEvent();
    }

    /**
     * Sets the type of the attribute. Internal method which
     * can't be called via the public interface.
     *
     * @param aTypeId the name of the attribute to be set
     */
    public void setTypeInternal(String aTypeId)
    {
        if(aTypeId.equals("NUMC")) {
            theType = CatalogAttribute.INTEGERID;
        }
        else {
            theType = CatalogAttribute.STRINGID;
        }

        //inform your listeners
        //for the builder no problem because there are definitly no listeners
        this.dispatchChangedEvent();
    }

    /**
     * Sets the description of the attribute. Internal method which
     * can't be called via the public interface.
     *
     * @param aDescription the description of the attribute to be set
     */
    public void setDescriptionInternal(String aDescription)
    {
        this.theAttributeDescription = aDescription;

        //inform your listeners
        //for the builder no problem because there are definitly no listeners
        this.dispatchChangedEvent();
    }

    /**
     * Sets the fixed value instances. Internal method which
     * can't be called via the public interface.
     *
     * @param aValueList the list of fixed values to be set
     */
    public void setFixedValuesInternal(ArrayList aValueList)
    {
        theFixedValues = aValueList;    

        //inform your listeners
        //for the builder no problem because there are definitly no listeners
        this.dispatchChangedEvent();
    }

    /**
     * Sets the quantity of the attribute. Internal method which
     * can't be called via the public interface.
     *
     * @param aQuantity the quantity of the attribute to be set
     */
    public void setDomainInternal(IDomain aDomain)
    {
        this.theDomain = aDomain;

        //inform your listeners
        //for the builder no problem because there are definitly no listeners
        this.dispatchChangedEvent();
    }

    /**
     * Sets the quantity of the attribute. Internal method which
     * can't be called via the public interface.
     *
     * @param aQuantity the quantity of the attribute to be set
     */
    public void setQuantityInternal(IQuantity aQuantity)
    {
        this.theQuantity = aQuantity;

        //inform your listeners
        //for the builder no problem because there are definitly no listeners
        this.dispatchChangedEvent();
    }

/* ************************************************************************** */

    /**
     * Sets the guid of the attribute. Internal method which
     * can't be called via the public interface.
     *
     * @param aGuid the guid of the attribute to be set
     */
    public void setGuidInternal(String aGuid)
    {
        this.theAttributeGuid = aGuid;
    }

    public int compareTo(Object op)
    {
        if(!(op instanceof CatalogAttribute))
            throw new ClassCastException();

        return getName().compareTo(((CatalogAttribute)op).getName());
    }
}

