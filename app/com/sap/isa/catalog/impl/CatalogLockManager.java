/*****************************************************************************
  Copyright (c) 2000, SAPMarkets Europe GmbH, Germany, All rights reserved.
  Created:      02 May 2001

  $Id: //sap/ESALES_base/30_SP_COR/src/_pcatAPI/java/com/sap/isa/catalog/impl/CatalogLockManager.java#2 $
  $Revision: #2 $
  $Change: 129221 $
  $DateTime: 2003/05/15 17:20:58 $
*****************************************************************************/
package com.sap.isa.catalog.impl;

// catalog imports
import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

import com.sap.isa.catalog.boi.IActor;

/**
 * Manages the locking during write/read access of the catalog.
 *
 * @version     1.0
 */
public class CatalogLockManager implements Serializable
{
    /**
     * Serialization:
     * theCatsToLock        :   container and content implement Serilizable
     * theCatToLock         :   implements Serilizable
     * theLockedComponents  :   container and content implement Serilizable
     * theUsers             :   container and content implement Serilizable
     */
    static private HashMap theCatsToLock = new HashMap();

    private Catalog theCatToLock;

    private HashMap theLockedComponents = new HashMap();

    private HashMap theRecursiveLockedComponents = new HashMap();

    private HashMap theUsers = new HashMap();

    private CatalogLockManager(Catalog aCatToLock)
    {
        this.theCatToLock=aCatToLock;
    }

    /**
     * Get a lock manager for the given instance.
     * There will be one lockmanager per catalog.
     *
     * @param   aCat to get the locker for
     * @return  theLockerForTheCat
     */
    public static synchronized CatalogLockManager getInstanceFor(Catalog aCat)
    {
        CatalogLockManager aManager = (CatalogLockManager) theCatsToLock.get(aCat);

        if(aManager==null)
        {
            aManager = new CatalogLockManager(aCat);
            theCatsToLock.put(aCat,aManager);
        }
        return aManager;
    }

    /**
     * Grab the lock for the component (opt recursive) if the user does not
     * allready has it.
     *
     * @param   theThingToLock the component to lock
     * @param   recursive in case we hav an composite get recursive all childs
     * @param   theOwner i.e. the transaction contexte that is a brace
     *          around a set of changes
     * @return the successe of the action
     */
    public synchronized boolean getLock(CatalogComponent theThingToLock,
                                        boolean recursive,
                                        IActor theOwner)
    {
        //first grab the remote locks
        if(theCatToLock.isDistributable())
            if(!getRemoteLock(theThingToLock,recursive, theOwner))
                return false;

        IActor aLockingUser =
            (IActor) theLockedComponents.get(theThingToLock);

        //Somebody has the lock
        if(aLockingUser!=null)
          if(aLockingUser.equals(theOwner))
                return true;
            else
                return false;

        //check if one of your ancestors has been locked recursive
        if(this.isAncestorRecursiveLocked(theThingToLock,theOwner))
            return false;

        //first try to grab all childern locks and keep track what you locked
        ArrayList theLockedThings = new ArrayList();
        if( recursive &&
            theThingToLock.
            queryInterface(CatalogComponentTypes.COMPOSITE_COMPONENT)!=null)
        {
            CatalogCompositeComponent theThingsToLock =
                (CatalogCompositeComponent) theThingToLock;

            //items,categories
            Iterator theChildIter =
                theThingsToLock.getChildComponents().iterator();

            while(theChildIter.hasNext())
            {
                CatalogComponent aThingToLock =
                    (CatalogComponent) theChildIter.next();

                if(!getLock(aThingToLock, recursive, theOwner))
                {
                    //somebody else grabed allread a child
                    //relase the locks you grabed so far
                    for(int i = 0; i< theLockedThings.size(); i++)
                        releaseLock(    (CatalogComponent)theLockedThings.get(i),
                                        recursive,
                                        theOwner);
                    return false;
                }
                theLockedThings.add(aThingToLock);
            }

            //attributes
            theChildIter =
                theThingsToLock.getAttributesInternal();
            while(theChildIter.hasNext())
            {
                CatalogComponent aThingToLock =
                    (CatalogComponent) theChildIter.next();

                if(!getLock(aThingToLock, recursive, theOwner))
                {
                    //somebody else grabed allread a child
                    //relase the locks you grabed so far
                    for(int i = 0; i< theLockedThings.size(); i++)
                        releaseLock(    (CatalogComponent)theLockedThings.get(i),
                                        recursive,
                                        theOwner);
                    return false;
                }
                theLockedThings.add(aThingToLock);
            }
        }

        //details
        if( recursive )
        {
            //details
            Iterator theChildIter =
                theThingToLock.getDetailsInternal();

            while(theChildIter.hasNext())
            {
                CatalogComponent aThingToLock =
                    (CatalogComponent) theChildIter.next();

                if(!getLock(aThingToLock, recursive, theOwner))
                {
                    //somebody else grabed allread a child
                    //relase the locks you grabed so far
                    for(int i = 0; i< theLockedThings.size(); i++)
                        releaseLock(    (CatalogComponent)theLockedThings.get(i),
                                        recursive,
                                        theOwner);
                    return false;
                }
                theLockedThings.add(aThingToLock);
            }
        }

        if( recursive &&
            theThingToLock.
            queryInterface(CatalogComponentTypes.CATALOG_ITEM)!=null)
        {
            CatalogItem theThingsToLock =
                (CatalogItem) theThingToLock;

            //attribute values
            Iterator theChildIter =
                theThingsToLock.getAttributeValues();

            while(theChildIter.hasNext())
            {
                CatalogComponent aThingToLock =
                    (CatalogComponent) theChildIter.next();

                if(!getLock(aThingToLock, recursive, theOwner))
                {
                    //somebody else grabed allread a child
                    //relase the locks you grabed so far
                    for(int i = 0; i< theLockedThings.size(); i++)
                        releaseLock(    (CatalogComponent)theLockedThings.get(i),
                                        recursive,
                                        theOwner);
                    return false;
                }
                theLockedThings.add(aThingToLock);
            }

            //details values
            theChildIter =
                theThingsToLock.getDetailsInternal();

            while(theChildIter.hasNext())
            {
                CatalogComponent aThingToLock =
                    (CatalogComponent) theChildIter.next();

                if(!getLock(aThingToLock, recursive, theOwner))
                {
                    //somebody else grabed allread a child
                    //relase the locks you grabed so far
                    for(int i = 0; i< theLockedThings.size(); i++)
                        releaseLock(    (CatalogComponent)theLockedThings.get(i),
                                        recursive,
                                        theOwner);
                    return false;
                }
                theLockedThings.add(aThingToLock);
            }
        }

        //finaly lock the thing itself
        put(theThingToLock,theOwner,recursive);
        addLink(theThingToLock,theOwner);
        return true;
    }

    /**
     * In case the catalog is distributed grab the locks from a central
     * locking instance.
     *
     * @param   theThingToLock the component to lock
     * @param   recursive in case we hav an composite get recursive all childs
     * @param   theOwner i.e. the transaction contexte that is a brace
     *          around a set of changes
     * @return  worked
     */
    protected synchronized boolean getRemoteLock(
                                        CatalogComponent theThingToLock,
                                        boolean recursive,
                                        IActor theOwner)
    {
        /**@todo: Implement this method*/
        throw new java.lang.UnsupportedOperationException("Method getRemoteLockLock() not yet implemented.");
    }

    /**
     * Release all locks for the user.
     *
     * @param   theOwner i.e. the transaction contexte that is a brace
     *          around a set of changes
     * @return the successe of the action
     */
    public synchronized boolean releaseLocks(IActor theOwner)
    {
        //release the remote locks first
        if(theCatToLock.isDistributable())
            if(!releaseRemoteLocks(theOwner))
                return false;
            else
                return true;

        //Get the set of locks owned by the user;
        Set theLocksOfTheUser = (Set) theUsers.get(theOwner);
        if(theLocksOfTheUser==null)//something went wrong
            return false;
        else
        {
            theUsers.remove(theOwner);
            Iterator theLocksIter = theLocksOfTheUser.iterator();
            while(theLocksIter.hasNext())//remove all locks
            {
                CatalogComponent theThingToUnLock =
                    (CatalogComponent) theLocksIter.next();
                theLockedComponents.remove(theThingToUnLock);
                theRecursiveLockedComponents.remove(theThingToUnLock);
            }
            return true;
        }
    }

    /**
     * Release all locks for the user from the remote locker instance.
     *
     * @param   theOwner i.e. the transaction contexte that is a brace
     *          around a set of changes
     * @return  the successe of the action
     */
    protected synchronized boolean releaseRemoteLocks(IActor theOwner)
    {
        /**@todo: Implement this method*/
        throw new java.lang.UnsupportedOperationException("Method releaseRemoteLockLock() not yet implemented.");
    }

    /**
     * Release the lock (opt, recursive) for the user.
     *
     * @param   theThingToUnLock the component to lock
     * @param   recursive in case we hav an composite get recursive all childs
     * @param   theOwner i.e. the transaction contexte that is a brace
     *          around a set of changes
     * @return  the successe of the action
     */
    public synchronized boolean releaseLock(CatalogComponent theThingToUnLock,
                                            boolean recursive,
                                            IActor theOwner)
    {
        //release the remote locks first
        if(theCatToLock.isDistributable())
            if(!releaseRemoteLock(theThingToUnLock,recursive, theOwner))
                return false;
            else
                return true;

        // nobody actually holds the lock
        if(theLockedComponents.get(theThingToUnLock)==null)
            return true;

        //first unlock your children!
        if( recursive && theThingToUnLock.
            queryInterface(CatalogComponentTypes.COMPOSITE_COMPONENT)!=null)
        {
            CatalogCompositeComponent theThingsToUnLock =
                (CatalogCompositeComponent) theThingToUnLock;

            //items,categories
            Iterator theChildIter =
                theThingsToUnLock.getChildComponents().iterator();
            while(theChildIter.hasNext())
            {
                CatalogComponent aThingToUnLock =
                    (CatalogComponent) theChildIter.next();
                if(!releaseLock(aThingToUnLock, recursive, theOwner))
                {//something went wrong
                    return false;
                }
            }

            //attributes
            theChildIter =
                theThingsToUnLock.getAttributesInternal();
            while(theChildIter.hasNext())
            {
                CatalogComponent aThingToUnLock =
                    (CatalogComponent) theChildIter.next();
                if(!releaseLock(aThingToUnLock, recursive, theOwner))
                {//something went wrong
                    return false;
                }
            }
        }

        if( recursive )
        {
            //details
            Iterator theChildIter =
                theThingToUnLock.getDetailsInternal();
            while(theChildIter.hasNext())
            {
                CatalogComponent aThingToUnLock =
                    (CatalogComponent) theChildIter.next();
                if(!releaseLock(aThingToUnLock, recursive, theOwner))
                {//something went wrong
                    return false;
                }
            }
        }

        //unlock yourself
        theRecursiveLockedComponents.remove(theThingToUnLock);
        Set theLocksOfTheUser = (Set) theUsers.get(theOwner);


        if(theLocksOfTheUser!=null)
        {
            if( (theLocksOfTheUser.remove(theThingToUnLock))
                && (theLockedComponents.remove(theThingToUnLock)!=null))
            {
                //this was the last lock for the user
                if(theLocksOfTheUser.isEmpty())
                    theUsers.remove(theOwner);
                return true;
            }
            else
                return false;
        }
        else
            return false;
    }

    /**
     * Release the remote lock (recursive) for the user.
     *
     * @param   theThingToUnLock the component to lock
     * @param   recursive in case we hav an composite get recursive all childs
     * @param   theOwner i.e. the transaction contexte that is a brace
     *          around a set of changes
     * @return  the successe of the action
     */
    protected synchronized boolean releaseRemoteLock(
                                            CatalogComponent theThingToUnLock,
                                            boolean recursive,
                                            IActor theOwner)
    {
        /**@todo: Implement this method*/
        throw new java.lang.UnsupportedOperationException("Method releaseRemoteLockLock() not yet implemented.");
    }

    /**
     * Create a new link from the user to the new lock owned by him.
     *
     * @param   theNewThingToLock the new component that was locked
     * @param   theOwner i.e. the transaction contexte that is a brace
     *          around a set of changes
     * @return  the successe of the action
     */
    protected synchronized boolean addLink(CatalogComponent theNewLockedThing,
                                        IActor theOwner)
    {
        Set theOwnersLocks = (Set) theUsers.get(theOwner);
        if(theOwnersLocks!=null)
        {
            if(theOwnersLocks.contains(theNewLockedThing))
                return false;
            else
                theOwnersLocks.add(theNewLockedThing);
                return true;
        }
        else
        {
            Set aNewSet = new HashSet();
            aNewSet.add(theNewLockedThing);
            theUsers.put(theOwner,aNewSet);
            return true;
        }
    }

    /**
     * Create a new lock for the component owned by the user.
     *
     * @param   theNewThingToLock the new component that was locked
     * @param   theOwner i.e. the transaction contexte that is a brace
     *          around a set of changes
     */
    protected void put( CatalogComponent theThingToLock,
                        IActor theOwner,
                        boolean recursive)
    {
        theLockedComponents.put(theThingToLock,theOwner);
        if(recursive)
            theRecursiveLockedComponents.put(theThingToLock,theOwner);
    }

    /**
     * In any lock request a component must first check if any of its
     * ancestors has been locked recursive (before aComponent was instantiated)
     * If thats the case aComponent is acutally locked by that user and a lock
     * request must fail if the user of this lock request and the user of the
     * earlier lock request for the ancestor are different. The call to
     * isAncestorRecursiveLocked updates the lock information in
     * case the aComponent is actually already effectively locked through an
     * earlier lock of an ancestor.
     *
     * @param aComponent A component to lock
     * @param aLocker   An user trying right now to lock the component
     * @return aBoolean If true aComponent has be registered as locked by
     *                  another user in an earlier recursive lock call.
     *                  If false nothing happend.
     */
    protected boolean isAncestorRecursiveLocked(  CatalogComponent aComponent,
                                                  IActor aLocker)
    {
        if(aComponent.queryInterface(CatalogComponentTypes.CATALOG)!=null)
            return false;
        if( aComponent.queryInterface(
            CatalogComponentTypes.CATALOG_ATTRIBUTE)!=null)
        {
            //An Attribute has no parent in the precise meaning but the catalog
            //is effecivley its parent
            CatalogAttribute aAttribute = (CatalogAttribute)aComponent;
            Catalog theCatalog = aAttribute.getRoot();
            IActor anOtherLocker = (IActor)
                theRecursiveLockedComponents.get(theCatalog);
            if(anOtherLocker!=null && aLocker!=anOtherLocker)
            {
                //update the locking information
                this.put(aComponent,anOtherLocker,false);
                this.addLink(aComponent,anOtherLocker);
                return true;
            }
            return false;
        }
        if( aComponent.queryInterface(
            CatalogComponentTypes.CATALOG_ATTRIBUTE_VALUE)!=null)
            return false;
        if( aComponent.queryInterface(
            CatalogComponentTypes.CATALOG_CATEGORY)!=null)
            return false;
        if( aComponent.queryInterface(
            CatalogComponentTypes.CATALOG_DETAIL)!=null)
            return false;
        if( aComponent.queryInterface(
            CatalogComponentTypes.CATALOG_ITEM)!=null)
            return false;
        return false;
        /**@todo implement it*/
    }
}
