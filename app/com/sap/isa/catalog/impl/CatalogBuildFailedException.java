/*****************************************************************************
  Copyright (c) 2000, SAPMarkets Europe GmbH, Germany, All rights reserved.

  $Id: //sap/ESALES_base/30_SP_COR/src/_pcatAPI/java/com/sap/isa/catalog/impl/CatalogBuildFailedException.java#4 $
  $Revision: #4 $
  $Change: 150872 $
  $DateTime: 2003/09/29 09:18:51 $
*****************************************************************************/
package com.sap.isa.catalog.impl;

import com.sap.isa.catalog.boi.CatalogException;

/**
 * This class wrapps all exceptions which occur during the building of the
 * catalogs.
 *
 * @version     1.0
 */
public class CatalogBuildFailedException extends CatalogException
{
        /**
         * Default constructor for the exception.
         *
         * @param  msg  Text to be associated with the exception
         */
    public CatalogBuildFailedException(String msg)
    {
        super(msg);
    }

    public CatalogBuildFailedException(String msg, Throwable aCause)
    {
        super(msg,aCause);
    }

    public CatalogBuildFailedException(Throwable aCause)
    {
        super(aCause);
    }
    
}
