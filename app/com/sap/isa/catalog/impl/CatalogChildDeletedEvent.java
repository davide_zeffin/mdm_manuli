/*****************************************************************************
  Copyright (c) 2000, SAPMarkets Europe GmbH, Germany, All rights reserved.
  Created:      09 July 2001

  $Id: //sap/ESALES_base/30_SP_COR/src/_pcatAPI/java/com/sap/isa/catalog/impl/CatalogChildDeletedEvent.java#3 $
  $Revision: #3 $
  $Change: 150872 $
  $DateTime: 2003/09/29 09:18:51 $
*****************************************************************************/
package com.sap.isa.catalog.impl;

import java.util.EventObject;

import com.sap.isa.catalog.boi.IChildDeletedEvent;
import com.sap.isa.catalog.boi.IComponent;

/**
 * Event that is fired if an child (Item/Category) is deleted from composite.
 *
 * @version     1.0
 */
public class CatalogChildDeletedEvent
    extends EventObject
    implements IChildDeletedEvent
{
    private CatalogComponent theChildCreated;

    public CatalogChildDeletedEvent(CatalogCompositeComponent aParent,
                                    CatalogHierarchicalComponent aNewChild)
    {
        super(aParent);
        this.theChildCreated=aNewChild;
    }

    public IComponent getComponent()
    {
        return (IComponent) getSource();
    }

    public IComponent getChild()
    {
        return theChildCreated;
    }
}