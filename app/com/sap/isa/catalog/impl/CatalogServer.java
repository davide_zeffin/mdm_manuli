/*****************************************************************************
  Copyright (c) 2000, SAPMarkets Europe GmbH, Germany, All rights reserved.
  Created:      09 July 2001

  $Id: //sap/ESALES_base/30_SP_COR/src/_pcatAPI/java/com/sap/isa/catalog/impl/CatalogServer.java#3 $
  $Revision: #3 $
  $Change: 150872 $
  $DateTime: 2003/09/29 09:18:51 $
*****************************************************************************/

package com.sap.isa.catalog.impl;

// catalog imports
import java.io.Serializable;
import java.util.Properties;

import com.sap.isa.catalog.boi.IServer;

/**
 * Simple implementation class of the <code>IServer</code> interface. This class
 * is no component of the catalog tree in the stricter sense.
 *
 * @version     1.0
 */

public class CatalogServer
    implements
        IServer,
        Serializable
{
    private String theGUID;
    private String theURLString;
    private int thePort;
    private String theCatalogGuid;
    private Properties theProperties;

    /**
     * Constructor.
     *
     * @param aServer the source of the instance
     */
    public CatalogServer(IServer aServer)
    {
        this.theGUID = aServer.getGuid();
        this.theCatalogGuid = aServer.getCatalogGuid();
        this.thePort        = aServer.getPort();
        this.theURLString   = aServer.getURLString();
        this.theProperties  = (Properties)aServer.getProperties().clone();
    }

        /**
         * Constructor.
         *
         * @param aGUID        a <code>String</code> value with the guid of the server
         * @param aURL          the URL of the server
         * @param aPort         the port of the server
         * @param aCatalogGuid  the guid of the catalog
         * @param aProps        the properties of the server
         */
    public CatalogServer(
        String aGUID,
        String aURL,
        int aPort,
        String aCatalogGuid,
        Properties aProps)
    {
        this.theGUID=aGUID;
        this.theCatalogGuid = aCatalogGuid;
        this.thePort        = aPort;
        this.theURLString   = aURL;
        this.theProperties  = aProps;
    }

    public String getGuid()
    {
        return theGUID;
    }
    
    /**
     * Returns the URL of the server.
     *
     * @return the URL of the server
     */
    public String getURLString()
    {
        return this.theURLString;
    }

    /**
     * Returns the port of the server.
     *
     * @return the port of the server
     */
    public int getPort()
    {
        return this.thePort;
    }

    /**
     * Returns the catalog guid of the server.
     *
     * @return the catalog guid of the server
     */
    public String getCatalogGuid()
    {
        return this.theCatalogGuid;
    }

    /**
     * Returns the properties of the server.
     *
     * @return the properties of the server
     */
    public Properties getProperties()
    {
        return this.theProperties;
    }
}
