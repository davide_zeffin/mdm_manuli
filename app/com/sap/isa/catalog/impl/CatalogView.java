/*****************************************************************************
  Copyright (c) 2000, SAPMarkets Europe GmbH, Germany, All rights reserved.
  Created:      09 July 2001

  $Id: //sap/ESALES_base/30_SP_COR/src/_pcatAPI/java/com/sap/isa/catalog/impl/CatalogView.java#3 $
  $Revision: #3 $
  $Change: 150872 $
  $DateTime: 2003/09/29 09:18:51 $
*****************************************************************************/
package com.sap.isa.catalog.impl;

// catalog imports
import java.io.Serializable;

import com.sap.isa.catalog.boi.IView;

/**
 * Simple implementation class of the <code>IView</code> interface. This class
 * is no component of the catalog tree in the stricter sense.
 *
 * @version     1.0
 */
public class CatalogView
    implements  IView,
                Serializable
{
    private String theGuid;

    /**
     * Constructor.
     *
     * @param aView the source instance
     */
    public CatalogView(IView aView)
    {
        this.theGuid = aView.getGuid();
    }

    /**
     * Constructor.
     *
     * @param aView the guid of the view
     */
    public CatalogView(String aView)
    {
        this.theGuid = aView;
    }

    /**
     * Returns the unique identifier of the view.
     *
     * @return the quid of the view
     */
    public String getGuid()
    {
        return theGuid;
    }
}