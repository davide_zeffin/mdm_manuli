/*****************************************************************************
  Copyright (c) 2000, SAPMarkets Europe GmbH, Germany, All rights reserved.
  Created:      09 July 2001

  $Id: //sap/ESALES_base/30_SP_COR/src/_pcatAPI/java/com/sap/isa/catalog/impl/CatalogComponent.java#3 $
  $Revision: #3 $
  $Change: 150872 $
  $DateTime: 2003/09/29 09:18:51 $
*****************************************************************************/

package com.sap.isa.catalog.impl;

// catalog imports
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;

import com.sap.isa.catalog.ICatalogMessagesKeys;
import com.sap.isa.catalog.boi.CatalogException;
import com.sap.isa.catalog.boi.ComponentTypes;
import com.sap.isa.catalog.boi.IAttribute;
import com.sap.isa.catalog.boi.IAttributeValue;
import com.sap.isa.catalog.boi.ICatalog;
import com.sap.isa.catalog.boi.ICategory;
import com.sap.isa.catalog.boi.IComponent;
import com.sap.isa.catalog.boi.IComponentEventListener;
import com.sap.isa.catalog.boi.IDetail;
import com.sap.isa.catalog.boi.IEditableAttribute;
import com.sap.isa.catalog.boi.IEditableAttributeValue;
import com.sap.isa.catalog.boi.IEditableCatalog;
import com.sap.isa.catalog.boi.IEditableCategory;
import com.sap.isa.catalog.boi.IEditableDetail;
import com.sap.isa.catalog.boi.IEditableItem;
import com.sap.isa.catalog.boi.IItem;
import com.sap.isa.catalog.boi.IQuery;
import com.sap.isa.catalog.boi.IQueryItem;
import com.sap.isa.catalog.boi.IQueryStatement;
import com.sap.isa.catalog.catalogvisitor.CatalogVisitor;
import com.sap.isa.catalog.editor.EditableAttribute;
import com.sap.isa.catalog.editor.EditableAttributeValue;
import com.sap.isa.catalog.editor.EditableCatalog;
import com.sap.isa.catalog.editor.EditableCategory;
import com.sap.isa.catalog.editor.EditableDetail;
import com.sap.isa.catalog.editor.EditableItem;
import com.sap.isa.core.logging.IsaLocation;

/**
 *  Base class of all components in the catalog tree.
 *
 *  @version     1.0
 *  @since       2.0
 */
abstract public class CatalogComponent
    implements Serializable, IComponent
{
    private static IsaLocation log =
               IsaLocation.getInstance(CatalogCompositeComponent.class.getName());
     
     /*
      *  Implements Serializable:
      *  Fields:
      *  theListeners:     not our problem should bettter be serilizable
      *                    the container is
      *  theDetails  :     are CatalogComponents ;-) so they are serializable
      */

    protected ArrayList theListeners = null;
    protected ArrayList theDetails = new ArrayList(0);

    /**
     * HINT: All methods of this class that access this attribute
     * are <b>not</b> synchronized. Therefore this has to be done by the caller
     * of these methods.
     */
    private IComponent.State theState = IComponent.State.LOADED;

/******************************** IComponent **********************************/

    public final Object queryInterface(ComponentTypes type) {

        Object result;

        if(type == CatalogComponentTypes.CATALOG_ATTRIBUTE)
            result = (this instanceof CatalogAttribute)?this:null;
        else if(type == CatalogComponentTypes.EDITABLE_ATTRIBUTE)
            result = (this instanceof EditableAttribute)?this:null;
        else if(type == CatalogComponentTypes.I_ATTRIBUTE)
            result = (this instanceof IAttribute)?this:null;
        else if(type == CatalogComponentTypes.I_EDITABLE_ATTRIBUTE)
            result = (this instanceof IEditableAttribute)?this:null;
        else if(type == CatalogComponentTypes.CATALOG_ATTRIBUTE_VALUE)
            result = (this instanceof CatalogAttributeValue)?this:null;
        else if(type == CatalogComponentTypes.EDITABLE_ATTRIBUTE_VALUE)
            result = (this instanceof EditableAttributeValue)?this:null;
        else if(type == CatalogComponentTypes.I_ATTRIBUTE_VALUE)
            result = (this instanceof IAttributeValue)?this:null;
        else if(type == CatalogComponentTypes.I_EDITABLE_ATTRIBUTE_VALUE)
            result = (this instanceof IEditableAttributeValue)?this:null;
        else if(type == CatalogComponentTypes.CATALOG)
            result = (this instanceof Catalog)?this:null;
        else if(type == CatalogComponentTypes.EDITABLE_CATALOG)
            result = (this instanceof EditableCatalog)?this:null;
        else if(type == CatalogComponentTypes.I_CATALOG)
            result = (this instanceof ICatalog)?this:null;
        else if(type == CatalogComponentTypes.I_EDITABLE_CATALOG)
            result = (this instanceof IEditableCatalog)?this:null;
        else if(type == CatalogComponentTypes.CATALOG_DETAIL)
            result = (this instanceof CatalogDetail)?this:null;
        else if(type == CatalogComponentTypes.EDITABLE_DETAIL)
            result = (this instanceof EditableDetail)?this:null;
        else if(type == CatalogComponentTypes.I_DETAIL)
            result = (this instanceof IDetail)?this:null;
        else if(type == CatalogComponentTypes.I_EDITABLE_DETAIL)
            result = (this instanceof IEditableDetail)?this:null;
        else if(type == CatalogComponentTypes.CATALOG_CATEGORY)
            result = (this instanceof CatalogCategory)?this:null;
        else if(type == CatalogComponentTypes.EDITABLE_CATEGORY)
            result = (this instanceof EditableCategory)?this:null;
        else if(type == CatalogComponentTypes.I_CATEGORY)
            result = (this instanceof ICategory)?this:null;
        else if(type == CatalogComponentTypes.I_EDITABLE_CATEGORY)
            result = (this instanceof IEditableCategory)?this:null;
        else if(type == CatalogComponentTypes.CATALOG_ITEM)
            result = (this instanceof CatalogItem)?this:null;
        else if(type == CatalogComponentTypes.EDITABLE_ITEM)
            result = (this instanceof EditableItem)?this:null;
        else if(type == CatalogComponentTypes.I_ITEM)
            result = (this instanceof IItem)?this:null;
        else if(type == CatalogComponentTypes.I_EDITABLE_ITEM)
            result = (this instanceof IEditableItem)?this:null;
        else if(type == CatalogComponentTypes.CATALOG_QUERY_ITEM)
            result = (this instanceof CatalogQueryItem)?this:null;
        else if(type == CatalogComponentTypes.I_QUERY_ITEM)
            result = (this instanceof IQueryItem)?this:null;
        else if(type == CatalogComponentTypes.CATALOG_QUERY)
            result = (this instanceof CatalogQuery)?this:null;
        else if(type == CatalogComponentTypes.I_QUERY)
            result = (this instanceof IQuery)?this:null;
        else if(type == CatalogComponentTypes.CATALOG_QUERY_STATEMENT)
            result = (this instanceof CatalogQueryStatement)?this:null;
        else if(type == CatalogComponentTypes.I_QUERY_STATEMENT)
            result = (this instanceof IQueryStatement)?this:null;
        else if(type == CatalogComponentTypes.COMPOSITE_COMPONENT)
            result = (this instanceof CatalogCompositeComponent)?this:null;
        else if(type == CatalogComponentTypes.HIERARCHICAL_COMPONENT)
            result = (this instanceof CatalogHierarchicalComponent)?this:null;
        else
            result = null;

        return result;
    }

    public synchronized Iterator getDetails() throws CatalogException
    {
        if(isDeleted())
        {
            String theMessage = getRoot().getMessageResources().getMessage(
                ICatalogMessagesKeys.PCAT_ERR_IS_DELEDED);
            throw new CatalogException(theMessage);
        }

        Catalog root = getRoot();
        CatalogBuilder builder = root.getCatalogBuilder();
        root.setTimeStamp();
        try
        {
            if(!builder.isDetailsBuilt(this))
               builder.buildDetails(this);
        }
        catch(CatalogBuildFailedException ex)
        {
            throw new CatalogException(ex.getLocalizedMessage(),ex);
        }

        return Collections.unmodifiableList(theDetails).iterator();
    }

    public synchronized Iterator getDetails(Comparator aComparator)
        throws CatalogException
    {
        if(isDeleted())
        {
            String theMessage = getRoot().getMessageResources().getMessage(
                ICatalogMessagesKeys.PCAT_ERR_IS_DELEDED);
            throw new CatalogException(theMessage);
        }

        Catalog root = getRoot();
        CatalogBuilder builder = root.getCatalogBuilder();
        root.setTimeStamp();
        try
        {
            if(!builder.isDetailsBuilt(this))
                builder.buildDetails(this);
        }
        catch(CatalogBuildFailedException ex)
        {
            throw new CatalogException(ex.getLocalizedMessage());
        }

        ArrayList details = (ArrayList)theDetails.clone();
        Collections.sort(details, aComparator);
        return details.iterator();
    }

    public synchronized IDetail getDetail(String aName) throws CatalogException
    {
        if(isDeleted())
        {
            String theMessage = getRoot().getMessageResources().getMessage(
                ICatalogMessagesKeys.PCAT_ERR_IS_DELEDED);
            throw new CatalogException(theMessage);
        }

        IDetail result = null;

        this.getDetails();
        Iterator iter = theDetails.iterator();
        while(iter.hasNext())
        {
            CatalogDetail detail = (CatalogDetail)iter.next();
            if(detail.getName().equals(aName))
            {
                result = detail;
                break;
            }
        }
        return result;
    }
    
    protected ArrayList getTheListeners() {
        if (theListeners == null) {
            theListeners = new ArrayList(5);
        }
        
        return theListeners;
    }

    synchronized public void addComponentEventListener(
        IComponentEventListener aComponentListener)
    {
        if( aComponentListener!= null &&
            (theListeners == null || !theListeners.contains(aComponentListener)))
            getTheListeners().add(aComponentListener);
    }

    synchronized public void removeComponentEventListener(
        IComponentEventListener aComponentListener)
    {
        if(aComponentListener!= null && theListeners != null)
            theListeners.remove(aComponentListener);
    }

    public synchronized IComponent.State getState()
    {
        return theState;
    }


/******************************************************************************/

    /**
     * This method accepts a catalog visitor instance and calls back its
     * specific visitor method.
     *
     * @param visitor the visitor that has to be called back
     */
    public abstract void assign(CatalogVisitor visitor);


    /**
     * Sets the state of the component. The possible states are defined
     * via the State enumuration of the <code>IComponent</code> interface.
     *
     * @param  state the new state to be set
     * @return boolean that indicates if the state was changed
     */
    public boolean setState(IComponent.State state)
    {
        if(state == null)
            return false;

        theState = state;

        return true;
    }

    /**
     * Sets the state of the component to NEW. If this state can't be
     * set <code>false</code> will be returned.
     *
     * @return boolean that indicates if the state was changed
     */
    public boolean setStateNew()
    {
        boolean success = false;

        // if new instance is created the default is LOADED
        if(isNew() || isUnchanged())
        {
            setState(IComponent.State.NEW);
            success = true;
        }

        return success;
    }

    /**
     * Sets the state of the component to CHANGED. If this state can't be
     * set <code>false</code> will be returned. If the state was NEW
     * this state will remain but also <code>true</code> is returned.
     *
     * @return boolean that indicates if the state was changed
     */
    public boolean setStateChanged()
    {
        boolean success = false;

        if(isUnchanged())
        {
            setState(IComponent.State.CHANGED);
            success = true;
        }

        if(isNew())
            success = true;

        return success;
    }

    /**
     * Sets the state of the component to DELETED. If this state can't be
     * set <code>false</code> will be returned.
     *
     * @return boolean that indicates if the state was changed
     */
    public boolean setStateDeleted()
    {
        setState(IComponent.State.DELETED);
        //inform the listening components that their reference is deleted
        this.dispatchChangedEvent();
        return true;
    }

    /**
     * Tests if the component was newly created.
     *
     * @return boolean that indicates if it was newly created
     */
    public boolean isNew()
    {
        return (theState==IComponent.State.NEW)?true:false;
    }

    /**
     * Tests if the component was newly changed.
     *
     * @return boolean that indicates if it was changed
     */
    public boolean isChanged()
    {
        return (theState==IComponent.State.CHANGED)?true:false;
    }

    /**
     * Tests if the component wasn't changed yet.
     *
     * @return boolean that indicates if it was not changed
     */
    public boolean isUnchanged()
    {
        return (theState==IComponent.State.LOADED)?true:false;
    }

    /**
     * Tests if the component was deleted.
     *
     * @return boolean that indicates if it was deleted
     */
    public boolean isDeleted()
    {
        return (theState==IComponent.State.DELETED)?true:false;
    }

    /**
     * Returns the root node of the catalog tree.
     *
     * @return the root of the tree ie. the catalog instance
     */
    public abstract Catalog getRoot();


    /**
     * Adds a Detail instance to the list of details.
     *
     * @param aDetail the detail to be added
     */
    public void addDetailInternal(CatalogDetail aDetail)
    {
        this.addDetailInternal(aDetail,false);
    }

    /**
     * Adds a Detail instance to the list of details.
     *
     * @param aDetail the detail to be added
     * @param dispatchEvent the listeners will be informed
     */
    public void addDetailInternal(CatalogDetail aDetail, boolean dispatchEvent)
    {
        theDetails.add(aDetail);
        //the listeners of this will be informed that a new detail was added.
        if(dispatchEvent)
            dispatchDetailCreatedEvent(aDetail);
    }

    /**
     * Removes a Detail instance from the list of details.
     *
     * @param aDetail the detail to be added
     */
    public void deleteDetailInternal(CatalogDetail aDetail)
    {
        theDetails.remove(aDetail);
        //first the listeners of the DETAIL will be informed that
        //the detail is deleted
        aDetail.setStateDeleted();
        //the listeners of the COMPONENT(this) will be informed that a
        //detail was deleted
        this.dispatchDetailDeletedEvent(aDetail);
    }

    /**
     * Returns the details if already built. If the details weren't built yet
     * an empty list will be returned. The details that were deleted will be
     * also returned!
     *
     * @return a list of details
     */
    public synchronized Iterator getDetailsInternal()
    {
        return theDetails.iterator();
    }

    public synchronized CatalogDetail getDetailInternal(String aName)
    {
        CatalogDetail aDetail = null;
        for(int i = 0; i < theDetails.size(); i++)
        {
            aDetail = (CatalogDetail) theDetails.get(i);
            if(aDetail.getName().intern()==aName.intern())
            {
                break;
            }
        }
        return aDetail;
    }

   /**
    * Sorts the list of details by name.
    */
    public void sortDetails()
    {
         Collections.sort(theDetails);
    }

    /**
     * Factory method for creating a detail instance. Internal method which
     * can't be called via the public interface.
     *
     * @return a instance of type <code>CatalogDetail</code>
     */
    public CatalogDetail createDetailInternal()
    {
        CatalogDetail aDetail = new CatalogDetail(this);
        addDetailInternal(aDetail);
        return aDetail;
    }

    /**
     * Factory method for creating a detail instance. Internal method which
     * can't be called via the public interface.
     *
     * @param name the name of the new detail instance
     * @return a instance of type <code>CatalogDetail</code>
     */
    public CatalogDetail createDetailInternal(String name)
    {
        CatalogDetail aDetail = new CatalogDetail(this);
        aDetail.setNameInternal(name);
        addDetailInternal(aDetail);
        return aDetail;
    }

/******************* event dispatching ****************************************/

    protected void dispatchChangedEvent()
    {
        if(theListeners != null) {
            synchronized(theListeners) {
                CatalogComponentChangedEvent ev =
                    new CatalogComponentChangedEvent(this);
                Iterator iter = theListeners.iterator();
                while(iter.hasNext())
                    ((IComponentEventListener)iter.next()).componentChanged(ev);
            }
        }
    }

    protected void dispatchDetailCreatedEvent(
        CatalogDetail aNewDetail)
    {
        if(theListeners != null) {
            synchronized(theListeners) {
                CatalogDetailCreatedEvent ev =
                    new CatalogDetailCreatedEvent(this,aNewDetail);
                Iterator iter = theListeners.iterator();
                while(iter.hasNext())
                    ((IComponentEventListener)iter.next()).detailCreated(ev);
            }
        }
    }

    protected void dispatchDetailDeletedEvent(
        CatalogDetail aDeletedDetail)
    {
        if(theListeners != null) {
            synchronized(theListeners) {
                CatalogDetailDeletedEvent ev =
                    new CatalogDetailDeletedEvent(this,aDeletedDetail);
                Iterator iter = theListeners.iterator();
                while(iter.hasNext())
                    ((IComponentEventListener)iter.next()).detailDeleted(ev);
            }
        }
    }
    
    protected void finalize() throws Throwable {
        if (log.isDebugEnabled()) {
            log.debug("Finalize Component: " + this);
        }
        theDetails        = null;
        theListeners      = null;
        
        super.finalize();
    }

}
