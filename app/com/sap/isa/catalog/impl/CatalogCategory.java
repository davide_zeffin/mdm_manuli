/**
 * Copyright (c) 2000, SAPMarkets Europe GmbH, Germany, All rights reserved. 
 * 
 * $Id: //sap/ESALES_base/30_SP_COR/src/_pcatAPI/java/com/sap/isa/catalog/impl/CatalogCategory.java#5 $ 
 * $Revision: #5 $
 * $Change: 146280 $ 
 * $DateTime: 2003/09/04 18:13:45 $
 */
package com.sap.isa.catalog.impl;

//  catalog imports
import com.sap.isa.catalog.boi.ICategory;
import com.sap.isa.catalog.boi.IItem;
import com.sap.isa.catalog.catalogvisitor.CatalogVisitor;
import com.sap.isa.core.logging.IsaLocation;

/**
 * This class implements the <code>ICategory</code> interface. <br/> 
 * Only the getter-methods are synchronized. The factory methods for the builder are not synchronized because this is
 * done in the builder instance. Only the builder is allowed to modify the catalog tree. <br/> 
 * Users of the catalog should only work with the interface. All other <code>public</code> methods of this class are
 * for internal use only.
 *
 * @version 1.0
 */
public class CatalogCategory extends CatalogCompositeComponent implements ICategory {
    
    /** Constant, to indicate, that value was not yet determined */
    protected static final int NOT_COUNTED = -1;

    /*
     *  CatalogComponent implements Serializable:
     *  Fields:
     *  theName                     :   no problem!
     *  theDescription              :   no problem!
     *  theGuid                     :   no problem!
     *  theThumbNail                :   no problem!
     */
    protected String theName; // language dependent
    protected String theDescription;
    protected String theGuid; // unique identifier
    protected String theThumbNail;
    protected boolean requestCategorySpecificAttributes;
    protected int theCount = NOT_COUNTED;
    protected int mainItemCount = NOT_COUNTED;
    
	/** 
	  *  The IsaLocation of the current Action.
	  *  This will be instantiated during perform and is always present.
	  */
	 protected IsaLocation log = IsaLocation.getInstance(CatalogCategory.class.getName());
    

    /**
     * Constructs a new category instance.
     *
     * @param aParent the parent component of this category
     * @param aGuid the guid of this category
     */
    public CatalogCategory(CatalogCompositeComponent aParent, String aGuid) {
        setParent(aParent);
        theGuid = aGuid;
        //JP: set name/description to something "obviously" wrong
        theName = "noNameForCategory(guid:" + theGuid + ")";
        theDescription = "noDescriptionForCategory(guid:" + theGuid + ")";
        requestCategorySpecificAttributes = false;
    }

    /**
     * DOCUMENT ME!
     *
     * @param visitor DOCUMENT ME!
     */
    public void assign(CatalogVisitor visitor) {
        visitor.visitCategory(this);
    }

    /**
     * DOCUMENT ME!
     *
     * @return DOCUMENT ME!
     */
    public ICategory getParent() {
        return (ICategory) getParentComponent().queryInterface(CatalogComponentTypes.I_CATEGORY);
    }

    /**
     * DOCUMENT ME!
     *
     * @return DOCUMENT ME!
     */
    public synchronized String getThumbNail() {
        Catalog root = getRoot();
        root.setTimeStamp();

        CatalogBuilder builder = root.getCatalogBuilder();

        try {
            if (!builder.isDetailsBuilt(this)) {
                builder.buildDetails(this);
            }
        }
        catch (CatalogBuildFailedException ex) {
        	log.debug(ex.getMessage());
        }

        return this.theThumbNail;
    }

    /**
     * Sets the thumb nail of the category. Internal method which can't be called via the public interface.
     *
     * @param aThumbNail the thumb nail to be set
     */
    public void setThumbNailInternal(String aThumbNail) {
        this.theThumbNail = aThumbNail;

        //inform your listeners
        //for the builder no problem because there are definitly no listeners
        this.dispatchChangedEvent();
    }

    /**
     * DOCUMENT ME!
     *
     * @return DOCUMENT ME!
     */
    public String getName() {
    // do not synchronize this method: possible deadlock situation
    // Catalog.getCategories() is locking Catalog object
    //      and calls this.getName()
    // CatalogBuilder.buildItems() is locking CatalogCateogry
    //      and calls Catalog.getAttribute() needing lock on Catalog object
        return this.theName;
    }

    /**
     * Sets the name of the category. Internal method which can't be called via the public interface.
     *
     * @param aName the name to be set
     */
    public void setNameInternal(String aName) {
        this.theName = aName;

        //inform your listeners
        //for the builder no problem because there are definitly no listeners
        this.dispatchChangedEvent();
    }
    
    /**
     * Determine the number of all items if the list
     */
    public void countAllItems() {
        // only count if not yet done
        if (theCount == NOT_COUNTED) {
            theCount = theLeafNodes.size();
        }
    }

    /**
     * Determine the number of Main items if not yet done
     */
    public void countMainItems() {
        // only count if not yet done
        if (mainItemCount == NOT_COUNTED) {
            countAllItems();
            //count main items
            mainItemCount = 0;
            for (int i = 0; i < theCount; i++) {
                if (((IItem) theLeafNodes.get(i)).isMainItem()) {
                    mainItemCount++;
                }
            }
        }
    }

    /**
     * Get the number of hits of the query. After submitting a query in count mode this the only information/state that
     * changed.
     *
     * @return theHits the number of items the query has or will retrieve if submitted
     */
    public int getCount() {
        return theCount;
    }
    
    /**
     * Returns the number of main items in the result list
     *
     * @return int the number of main items in the result list
     */
    public synchronized int getMainItemCount() {  
        countMainItems();  
        return mainItemCount;
    }

    /**
     * DOCUMENT ME!
     *
     * @return DOCUMENT ME!
     */
    public synchronized String getDescription() {
        Catalog root = getRoot();
        root.setTimeStamp();

        CatalogBuilder builder = root.getCatalogBuilder();

        try {
            if (!builder.isDetailsBuilt(this)) {
                builder.buildDetails(this);
            }
        }
        catch (CatalogBuildFailedException ex) {
			log.debug(ex.getMessage());
        }

        return theDescription;
    }

    /**
     * Sets the longtext description of the category. Internal method which can't be called via the public interface.
     *
     * @param aDescription the text to be set
     */
    public void setDescriptionInternal(String aDescription) {
        this.theDescription = aDescription;

        //inform your listeners
        //for the builder no problem because there are definitly no listeners
        this.dispatchChangedEvent();
    }

    /**
     * Returns the guid of the category. Synchronization: No need to synchronize. At the moment of creation that
     * variable will be set in the constructor. The created instance will be inserted in a collection that is locked
     * by the creating thread.
     *
     * @return String The technical guid of the catalog
     */
    public String getGuid() {
        return this.theGuid;
    }

    /**
     * Tests if this instance is a root category i.e. if the direct parent is the catalog and not an other category
     * instance.
     *
     * @return boolean that indicates if it is root category
     */
    public boolean isRootCategory() {
        return (getParent() == null) ? true : false;
    }

    /**
     * DOCUMENT ME!
     *
     * @param op DOCUMENT ME!
     *
     * @return DOCUMENT ME!
     */
    public int compareTo(Object op) {
        if (!(op instanceof CatalogCategory)) {
            throw new ClassCastException();
        }

        return getName().compareTo(((CatalogCategory) op).getName());
    }

    /**
     * Sets the requestCategorySpecificAttributes mode. If this is set to <code>true</code> category specific
     * attributes are read.
     *
     * @param mode true, if specific attributes should be read, false otherwise.
     */
    public void setRequestCategorySpecificAttributes(boolean mode) {
        this.requestCategorySpecificAttributes = mode;
    }

    /**
     * Reads the requestCategorySpecificAttributes mode.
     *
     * @return true, if specific attributes should be read, false otherwise.
     */
    public boolean getRequestCategorySpecificAttributes() {
        return requestCategorySpecificAttributes;
    }
}
