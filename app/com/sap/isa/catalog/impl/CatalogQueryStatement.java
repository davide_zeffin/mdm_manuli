/*****************************************************************************
  Copyright (c) 2001, SAPMarkets Europe GmbH, Germany, All rights reserved.
  Created:  27 March 2001

  $Revision: #2 $
  $Date: 2003/04/30 $
*****************************************************************************/
package com.sap.isa.catalog.impl;

// catalog imports
import com.sap.isa.catalog.ICatalogMessagesKeys;
import com.sap.isa.catalog.boi.IFilter;
import com.sap.isa.catalog.boi.IQueryStatement;
import com.sap.isa.catalog.catalogvisitor.CatalogVisitor;
import com.sap.isa.catalog.filter.CatalogFilter;
import com.sap.isa.core.logging.IsaLocation;

/**
 * This class implements a statement of a query.  <BR/>
 * Users of the catalog should only work with the interface. All other
 * <code>public</code> methods of this class are for internal use only.
 *
 * @version     1.0
 */
public class CatalogQueryStatement
    extends     CatalogComponent
    implements  IQueryStatement
{
    /*
     *  Implements Serializable:
     *  Fields:
     *  theQuery            :   implements Serializable itself!
     *  theStatementAsString:   no problem
     *  theFilter           :   implements Serializable itself!
     *  theArributes        :   theContainer implements Serializable!
     *                          The parts will be Strings i.e. no problem
     *  theSortOrder        :   the Container implements Serializable!
     *                          The parts will be Strings i.e. no problem
     *  theRangeFrom        :   no problem
     *  theRangeTo          :   no problem
     *
     */

    private CatalogQuery theQuery;
    private String theStatementAsString;
    private CatalogFilter theFilter;
    private String[] theAttributes;
    private String[] theSortOrder;
    private int theRangeFrom;
    private int theRangeTo;
    private IQueryStatement.Type theSearchType = IQueryStatement.Type.ITEM_SEARCH;

    // the logging instance
    private static IsaLocation theStaticLocToLog =
        IsaLocation.getInstance(CatalogQueryStatement.class.getName());

    /**
     * Creates a new instance of a statement. The query has to be associated to
     * the statement via the <code>setQuery()</code> method.
     *
     */
    CatalogQueryStatement()
    {
    }

    /**
     * Sets the query which belongs to this statement. It is absolutely necessary
     * that this method is called.
     *
     * @param aQuery the query associated with this statement
     */
    void setQuery(CatalogQuery aQuery)
    {
        theQuery = aQuery;
    }

    /**
     * The root is only returned if the statement was previously associated with a
     * query otherwise <code>null</code> will be returned.
     */
    public Catalog getRoot()
    {
        return (theQuery != null)?theQuery.getRoot():null;
    }

    public void assign(CatalogVisitor visitor)
    {
        visitor.visitQueryStatement(this);
    }

        /**
         * The statement is a quick search. Use this method to set the word or phrase
         * to search for in the quick search. The support for regular expressions or
         * "word masking" depends on the underlying search/catalog engine.
         *
         * @param aQuery a <code>String</code> value
         */
    public synchronized void setStatementAsString(String aQuery)
    {
        if (theQuery != null && theQuery.isSubmitted())
        {
            theStaticLocToLog.error(
                ICatalogMessagesKeys.PCAT_ERR_QUERY_SUBMITTED,
                new Object[]{ getName() }, null);
        }
        else
        {
            theStatementAsString = aQuery;
            theFilter = null;
            theAttributes = null;
            theSortOrder = null;
            theRangeFrom = 0;
            theRangeTo = 0;
        }
    }

	public synchronized void setStatementAsString(String aQuery, int from, int to)
	{
		if (theQuery != null && theQuery.isSubmitted())
		{
			theStaticLocToLog.error(
				ICatalogMessagesKeys.PCAT_ERR_QUERY_SUBMITTED,
				new Object[]{ getName() }, null);
		}
		else
		{
			theStatementAsString = aQuery;
			theFilter = null;
			theAttributes = null;
			theSortOrder = null;
			theRangeFrom = from;
			theRangeTo = to;
		}
	}

    public synchronized void setStatement(  IFilter aFilter,
                                            String[] aAttributeList,
                                            String[] aSortOrder)
    {
        if (theQuery != null && theQuery.isSubmitted())
        {
            theStaticLocToLog.error(
                ICatalogMessagesKeys.PCAT_ERR_QUERY_SUBMITTED,
                new Object[]{ getName() }, null);
        }
        else
        {
            theStatementAsString = null;
            theFilter = (CatalogFilter)aFilter;
            theAttributes = aAttributeList;
            theSortOrder  = aSortOrder;
            theRangeFrom = 0;
            theRangeTo = 0;
        }
    }

    public synchronized void setStatement(  IFilter aFilter,
                                            String[] aAttributeList,
                                            String[] aSortOrder,
                                            int from,
                                            int to)
    {
        if (theQuery != null && theQuery.isSubmitted())
        {
            theStaticLocToLog.error(
                ICatalogMessagesKeys.PCAT_ERR_QUERY_SUBMITTED,
                new Object[]{ getName() }, null);
        }
        else
        {
            theStatementAsString = null;
            theFilter = (CatalogFilter)aFilter;
            theAttributes = aAttributeList;
            theSortOrder  = aSortOrder;
            theRangeFrom = from;
            theRangeTo = to;
        }
    }

    public synchronized String getName()
    {
        StringBuffer nameBuffer = new StringBuffer();

        if( theStatementAsString != null )
        {
            // append type if it is not an item search
            if (getSearchType() != IQueryStatement.Type.ITEM_SEARCH &&
                getSearchType() != IQueryStatement.Type.ITEM_ADVSEARCH)
                nameBuffer.append("[").
                    append("{").append(getSearchType().toString()).append("}, ").
                    append("{").append(theStatementAsString).append("}]");
            else
                nameBuffer.append(theStatementAsString);
        }
        else
        {
            nameBuffer = new StringBuffer("[");
            // append type if it is not an item search
            if (getSearchType() != IQueryStatement.Type.ITEM_SEARCH &&
                getSearchType() != IQueryStatement.Type.ITEM_ADVSEARCH)
                nameBuffer.append("{").append(getSearchType().toString()).append("}, ");
            // filter
            nameBuffer.append("{").append(theFilter.toString()).append("}, ");
            // range
            nameBuffer.append("{").append(theRangeFrom);
            nameBuffer.append(", ").append(theRangeTo).append("}, ");
            // attributes
            nameBuffer.append("{");
            for(int i=0; theAttributes != null && i < theAttributes.length; i++)
            {
                if( i > 0 && i < theAttributes.length )
                    nameBuffer.append(", ");
                nameBuffer.append(theAttributes[i]);
            }
            nameBuffer.append("}, ");
            // sort order
            nameBuffer.append("{");
            for(int i=0; theSortOrder != null && i < theSortOrder.length; i++)
            {
                if( i > 0 && i < theSortOrder.length )
                    nameBuffer.append(", ");
                nameBuffer.append(theSortOrder[i]);
            }
            nameBuffer.append("}");
            nameBuffer.append("]");
        }
        return nameBuffer.toString();
    }

    public synchronized String getStatementAsString()
    {
        return theStatementAsString;
    }

    public synchronized void setSearchType(IQueryStatement.Type type)
    {
        theSearchType = type;
    }

    /**
     * Returns the search type that was associated with the statement.
     *
     * @return the search type of this statement
     */
    public synchronized IQueryStatement.Type getSearchType()
    {
        return theSearchType;
    }

    /**
     * Returns the filter expression associated with the statement.
     *
     * @return the filter instance
     */
    public synchronized CatalogFilter getFilter()
    {
        return theFilter;
    }

    /**
     * Returns the list of requested attributes associated with the statement.
     * If all attributes are requested <code>null</code> will be returned.
     *
     * @return the list of requested attributes
     */
    public synchronized String[] getAttributes()
    {
        return theAttributes;
    }

    /**
     * Returns the list of attributes which define the sort order.
     * If no special sort order is requested <code>null</code> will be returned.
     *
     * @return the list of attributes
     */
    public synchronized String[] getSortOrder()
    {
        return theSortOrder;
    }

    /**
     * Returns the start point of the range.
     *
     * @return the start of the search interval
     */
    public synchronized int getRangeFrom()
    {
        return theRangeFrom;
    }

    /**
     * Returns the end point of the range.
     *
     * @return the end of the search interval
     */
    public synchronized int getRangeTo()
    {
        return theRangeTo;
    }

/*************************** private ******************************************/
}
