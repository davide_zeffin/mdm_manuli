/*****************************************************************************
  Copyright (c) 2000, SAPMarkets Europe GmbH, Germany, All rights reserved.
  Created:      09 July 2001

  $Id: //sap/ESALES_base/30_SP_COR/src/_pcatAPI/java/com/sap/isa/catalog/impl/CatalogComponentChangedEvent.java#3 $
  $Revision: #3 $
  $Change: 150872 $
  $DateTime: 2003/09/29 09:18:51 $
*****************************************************************************/
package com.sap.isa.catalog.impl;

import java.util.EventObject;

import com.sap.isa.catalog.boi.IComponent;
import com.sap.isa.catalog.boi.IComponentChangedEvent;

/**
 * Event that is fired if a component is changed.
 *
 * @version     1.0
 */
public class CatalogComponentChangedEvent
    extends EventObject
    implements IComponentChangedEvent
{
    /**
     * Constructor.
     *
     * @param src the CatalogComponent that was changed
     */
    public CatalogComponentChangedEvent(CatalogComponent src)
    {
        super(src);
    }

    public IComponent getComponent()
    {
        return (CatalogComponent) getSource();
    }
}