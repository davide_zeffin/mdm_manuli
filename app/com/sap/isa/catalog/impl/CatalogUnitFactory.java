/*****************************************************************************
  Copyright (c) 2000, SAPMarkets Europe GmbH, Germany, All rights reserved.
  Created:      02 May 2001

  $Id: //sap/ESALES_base/30_SP_COR/src/_pcatAPI/java/com/sap/isa/catalog/impl/CatalogUnitFactory.java#3 $
  $Revision: #3 $
  $Change: 150872 $
  $DateTime: 2003/09/29 09:18:51 $
*****************************************************************************/
package com.sap.isa.catalog.impl;

// catalog imports
import java.io.Serializable;
import java.util.HashMap;

import com.sap.isa.catalog.boi.IUnit;

/**
 * Factory class for units in the catalog.
 *
 * @version     1.0
 */
public class CatalogUnitFactory implements Serializable
{
    public static final String UNIT_ID_UNKNOWN     = "UnkownId";
    public static final String UNIT_ID_DOLLAR      = "DollarId";
    public static final String UNIT_ID_EURO        = "EuroId";
    //The default english in absence of an resources.
    public static final String UNKNOWN      = "Unknown";
    public static final String DOLLAR       = "Dollar";
    public static final String EURO         = "Euro";

    static private CatalogUnitFactory theInstance;

    static private HashMap theUnits = new HashMap();

    private CatalogUnitFactory()
    {
    }

    public static synchronized CatalogUnitFactory getInstance()
    {
        if(theInstance == null)
        {
            theInstance = new CatalogUnitFactory();
        }
        return theInstance;
    }

    public IUnit getUnit(String unitId)
    {
        IUnit theUnit = null;
        theUnit = (IUnit) theUnits.get(unitId);
        if(theUnit==null)
        {
            if(unitId.equals(UNIT_ID_DOLLAR))
                theUnit = new Dollar();
            else if(unitId.equals(UNIT_ID_EURO))
                theUnit = new Euro();
            else if(unitId.equals(UNIT_ID_UNKNOWN))
                theUnit = new Unknown();
        }
        if(theUnit!=null)
            theUnits.put(unitId,theUnit);
        return theUnit;
    }

    private static Double convertEuroToDollar(Double euro)
    {
        return new Double(euro.doubleValue()*0.83);
    }

    private static Double convertDollarToEuro(Double dollar)
    {
        return new Double( dollar.doubleValue()/0.83 );
    }

    public class Unknown implements IUnit, Serializable
    {
        private String theId    =   CatalogUnitFactory.UNIT_ID_UNKNOWN;
        private String theName  =   CatalogUnitFactory.UNKNOWN;

        public String getName()
        {
            return theName;
        }

        public String getGuid()
        {
            return theId;
        }

        public Number convert(Number aNumber, IUnit aCurrency)
        {
            return aNumber;
        }
    }

    public class Euro implements IUnit, Serializable
    {
        private String theId    =   CatalogUnitFactory.UNIT_ID_EURO;
        private String theName  =   CatalogUnitFactory.EURO;

        public String getName()
        {
            return theName;
        }

        public String getGuid()
        {
            return theId;
        }

        public Number convert(Number aNumber, IUnit aCurrency)
        {
            if(aCurrency.getName().equals(CatalogUnitFactory.DOLLAR))
                return convertDollarToEuro((Double) aNumber);
            if(aCurrency.getName().equals(this.theName))
                return aNumber;
            return null;
        }
    }

    public class Dollar implements IUnit, Serializable
    {
        private String theId    = CatalogUnitFactory.UNIT_ID_DOLLAR;
        private String theName  = CatalogUnitFactory.DOLLAR;

        public String getName()
        {
            return theName;
        }

        public String getGuid()
        {
            return theId;
        }

        public Number convert(Number aNumber, IUnit aCurrency)
        {
            if(aCurrency.getName().equals(CatalogUnitFactory.EURO))
                return convertDollarToEuro((Double) aNumber);
            if(aCurrency.getName().equals(this.theName))
                return aNumber;
            return null;
        }
    }
}