/*****************************************************************************
  Copyright (c) 2000, SAPMarkets Europe GmbH, Germany, All rights reserved.
  Created:      09 July 2001

  $Id: //sap/ESALES_base/30_SP_COR/src/_pcatAPI/java/com/sap/isa/catalog/impl/CatalogHierarchicalComponent.java#3 $
  $Revision: #3 $
  $Change: 150872 $
  $DateTime: 2003/09/29 09:18:51 $
*****************************************************************************/

package com.sap.isa.catalog.impl;

import java.util.Iterator;

import com.sap.isa.core.logging.IsaLocation;

/**
 *  Base class of all hierarchical components in the catalog tree.
 *  Hierarchical components are those that are part of the tree in the
 *  stricter sense ie. catalog, categories, items
 *
 *  @version     1.0
 */
abstract public class CatalogHierarchicalComponent
    extends CatalogComponent
    implements Comparable
{
    private static IsaLocation log =
               IsaLocation.getInstance(CatalogCompositeComponent.class.getName());
    
    /*
     *  Implements Serializable:
     *  Fields:
     *  theParentComponent  :   implements Serializable itself!
     */

    CatalogCompositeComponent theParentComponent = null;

    /**
     * Returns the parent node of the actual node. The concrete type of
     * the node can be requested via the <code>queryInterface</code> method.
     *
     * @return parent of the actual node
     */
    public CatalogCompositeComponent getParentComponent() {
        return theParentComponent;
    }

    /**
     * Sets the parent of the component.
     *
     * @param aParent the parent to be set
     */
    public void setParent(CatalogCompositeComponent aParent)
    {
        theParentComponent = aParent;
    }

    public Catalog getRoot() {
        CatalogHierarchicalComponent parent = this;

        // search for the root node
        while(parent.getParentComponent() != null) {
            parent = parent.getParentComponent();
        }

        // root node should always be a catalog node
        return (Catalog)parent;
    }
    
    protected void finalize() throws Throwable {
        if (log.isDebugEnabled()) {
            log.debug("Finalize Hierarchical Component: " + this);
        }
        theParentComponent        = null;
        
        super.finalize();
    }

    public abstract int compareTo(Object op);

    /**
     * Returns an iterator for the hierarchy. <br/>
     * (traversation order: in order)
     *
     * @return an iterator instance
     */
    public abstract Iterator iterator();
}


