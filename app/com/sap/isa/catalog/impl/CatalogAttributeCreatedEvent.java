/*****************************************************************************
  Copyright (c) 2000, SAPMarkets Europe GmbH, Germany, All rights reserved.
  Created:      09 July 2001

  $Id: //sap/ESALES_base/30_SP_COR/src/_pcatAPI/java/com/sap/isa/catalog/impl/CatalogAttributeCreatedEvent.java#3 $
  $Revision: #3 $
  $Change: 150872 $
  $DateTime: 2003/09/29 09:18:51 $
*****************************************************************************/
package com.sap.isa.catalog.impl;

import java.util.EventObject;

import com.sap.isa.catalog.boi.IAttribute;
import com.sap.isa.catalog.boi.IAttributeCreatedEvent;
import com.sap.isa.catalog.boi.IComponent;

/**
 * Event that is fired if an attribute is added to a  composite.
 *
 * @version     1.0
 */
public class CatalogAttributeCreatedEvent
    extends EventObject
    implements IAttributeCreatedEvent
{
    private CatalogAttribute theNewAttribute;

    public CatalogAttributeCreatedEvent(CatalogCompositeComponent aParent,
                                        CatalogAttribute theNewAttribute)
    {
        super(aParent);
        this.theNewAttribute = theNewAttribute;
    }

    public IComponent getComponent()
    {
        return (IComponent) getSource();
    }

    public IAttribute getAttribute()
    {
        return theNewAttribute;
    }
}