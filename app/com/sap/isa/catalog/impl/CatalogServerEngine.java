/**
 * Copyright (c) 2000, SAPMarkets Europe GmbH, Germany, All rights reserved. Created:      13 February 2002 $Id:
 * //sap/ESALES_base/30_SP_COR/src/_pcatAPI/java/com/sap/isa/catalog/impl/CatalogServerEngine.java#9 $ $Revision: #9 $
 * $Change: 150936 $ $DateTime: 2003/09/29 13:00:17 $
 */
package com.sap.isa.catalog.impl;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.OutputStreamWriter;
import java.io.Serializable;
import java.io.StringReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Locale;
import java.util.Properties;
import java.util.Set;
import java.util.Timer;
import java.util.TimerTask;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.Source;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;

import org.apache.struts.util.MessageResources;
import org.w3c.dom.Document;
import org.xml.sax.InputSource;

import com.sap.isa.catalog.ICatalogMessagesKeys;
import com.sap.isa.catalog.boi.CatalogException;
import com.sap.isa.catalog.boi.CatalogPersistenceException;
import com.sap.isa.catalog.boi.IActor;
import com.sap.isa.catalog.boi.ICatalog;
import com.sap.isa.catalog.boi.ICatalogBackUpStrategy;
import com.sap.isa.catalog.boi.ICatalogInfo;
import com.sap.isa.catalog.boi.ICatalogLoadBackUpStrategy;
import com.sap.isa.catalog.boi.ICatalogTask;
import com.sap.isa.catalog.boi.ICatalogUpdateStrategy;
import com.sap.isa.catalog.boi.ICategory;
import com.sap.isa.catalog.boi.IClient;
import com.sap.isa.catalog.boi.IComponent;
import com.sap.isa.catalog.boi.IContext;
import com.sap.isa.catalog.boi.IEnvironment;
import com.sap.isa.catalog.boi.IServer;
import com.sap.isa.catalog.boi.IServerEngine;
import com.sap.isa.catalog.boi.IServerEventlistener;
import com.sap.isa.catalog.boi.ICatalogTask.Action;
import com.sap.isa.catalog.boi.ICatalogTask.Status;
import com.sap.isa.catalog.cache.CachableItem;
import com.sap.isa.catalog.cache.CatalogCache;
import com.sap.isa.catalog.cache.CatalogCacheException;
import com.sap.isa.catalog.trex.TrexCatalogServerEngine;
import com.sap.isa.catalog.trex.TrexManagedCatalog;
import com.sap.isa.core.TechKey;
import com.sap.isa.core.eai.BackendBusinessObjectParams;
import com.sap.isa.core.eai.BackendContext;
import com.sap.isa.core.eai.BackendException;
import com.sap.isa.core.eai.BackendObjectSupport;
import com.sap.isa.core.eai.ConnectionFactory;
import com.sap.isa.core.init.InitializationEnvironment;
import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.core.util.table.ResultData;

/**
 * DOCUMENT ME!
 *
 * @author $author$
 * @version $Revision$
 */
abstract public class CatalogServerEngine implements CachableItem, IServerEngine, Serializable, IContext {
    // the logging instance
    private static IsaLocation theStaticLocToLog = IsaLocation.getInstance(CatalogServerEngine.class.getName());
    public static final String BO_TYPE_CATALOG = "CATALOG";
    private boolean catalogsBuild = false; //serializable OKAY
    protected String stagingSupport = STAGING_NOT_SUPPORTED; // only IMS Catalogs right now support staging
    protected ConnectionFactory theConnectionFactory; //serializable Check
    protected BackendObjectSupport theBackendSupport; //serializable Check
    protected BackendContext theBackendContext; //serializable Check
    protected String theGuid;
    protected CatalogSite theSite;
    protected ArrayList theServerEventListeners = new ArrayList();
    protected ArrayList theCatalogInfos = new ArrayList();
    protected String theDefaultGuid; //serializable OKAY
    protected String theURLString; //serializable OKAY
    protected Properties theNames = new Properties(); //serializable OKAY
    protected Properties theDescriptions = new Properties(); //serializable OKAY
    protected IServerEngine.ServerType theType;
    protected Locale theLocale = Locale.ENGLISH; //serializable OKAY
    protected IActor theActor;
    protected MessageResources theMessageResources;
    protected Timestamp theTimeStamp;
    protected Properties theProperties = new Properties();
    protected Document theConfigDocument;

    /**
     * Creates a new <code>CatalogServerEngine</code> instance. For creation via BEM mechanism.
     */
    public CatalogServerEngine() {
        setTimeStamp();
    }

    /**
     * Creates a new CatalogServerEngine object.
     *
     * @param theGuid DOCUMENT ME!
     * @param theSite DOCUMENT ME!
     * @param aServerConfigFile DOCUMENT ME!
     *
     * @throws BackendException DOCUMENT ME!
     */
    public CatalogServerEngine(String theGuid, CatalogSite theSite, Document aServerConfigFile)
                        throws BackendException {
        this.theGuid = theGuid;
        this.theSite = theSite;
        this.theConfigDocument = aServerConfigFile;
        setTimeStamp();
        init(aServerConfigFile);
    }

    /**
     * BackendBusinessObject
     *
     * @param aConnectionFactory DOCUMENT ME!
     */
    public void setConnectionFactory(ConnectionFactory aConnectionFactory) {
        theConnectionFactory = aConnectionFactory;
    }

    /**
     * DOCUMENT ME!
     *
     * @return DOCUMENT ME!
     */
    public ConnectionFactory getConnectionFactory() {
        return theConnectionFactory;
    }

    /**
     * DOCUMENT ME!
     *
     * @param context DOCUMENT ME!
     */
    public void setContext(BackendContext context) {
        theBackendContext = context;
    }

    /**
     * DOCUMENT ME!
     *
     * @return DOCUMENT ME!
     */
    public BackendContext getContext() {
        return theBackendContext;
    }

    /**
     * DOCUMENT ME!
     *
     * @param aSupporter DOCUMENT ME!
     */
    public void setBackendObjectSupport(BackendObjectSupport aSupporter) {
        theBackendSupport = aSupporter;

        return;
    }

    /**
     * DOCUMENT ME!
     *
     * @return DOCUMENT ME!
     */
    public BackendObjectSupport getBackendObjectSupport() {
        return theBackendSupport;
    }

    /**
     * DOCUMENT ME!
     *
     * @param aPros DOCUMENT ME!
     * @param aParams DOCUMENT ME!
     *
     * @throws BackendException DOCUMENT ME!
     */
    abstract public void initBackendObject(Properties aPros, BackendBusinessObjectParams aParams)
                                    throws BackendException;

    /**
     * DOCUMENT ME!
     *
     * @param aServerConfigFile DOCUMENT ME!
     */
    abstract protected void init(Document aServerConfigFile);

    /* ******************************* CachableItem ****************************** */
    public Timestamp getTimeStamp() {
        return theTimeStamp;
    }

    /**
     * DOCUMENT ME!
     */
    public void setTimeStamp() {
        theTimeStamp = new Timestamp(System.currentTimeMillis());
    }

    /**
     * DOCUMENT ME!
     *
     * @return DOCUMENT ME!
     */
    abstract public boolean isCachedItemUptodate();

    /**
     * IServer
     *
     * @return DOCUMENT ME!
     */
    public String getGuid() {
        return theGuid;
    }

    /**
     * DOCUMENT ME!
     *
     * @return DOCUMENT ME!
     */
    abstract public int getPort();

    /**
     * DOCUMENT ME!
     *
     * @return DOCUMENT ME!
     */
    public Properties getProperties() {
        return theProperties;
    }

    /**
     * DOCUMENT ME!
     *
     * @return DOCUMENT ME!
     */
    abstract public String getURLString();

    /**
     * DOCUMENT ME!
     *
     * @return DOCUMENT ME!
     */
    public synchronized String getCatalogGuid() {
        return theDefaultGuid;
    }

    /******************************************************************************/
    Document getConfigDocument() {
        return theConfigDocument;
    }

    /**
     * DOCUMENT ME!
     *
     * @return DOCUMENT ME!
     */
    public CatalogSite getSite() {
        return theSite;
    }
    
    /**
     * This method always returns null,
     * because staging is only supported by IMS catalogues.
     * 
     * This method might be overwritten by the subclasses to return 
     * a different value.
     *
     * @return null no staging infos available
     */
    public ResultData getInactiveCatalogInfos(String theCatalogGuid, String stagingType, IClient aUser, String country) {
        
        return (ResultData) null;
    }

    /**
     * DOCUMENT ME!
     *
     * @return DOCUMENT ME!
     */
    public IServerEngine.ServerType getType() {
        return theType;
    }

    /**
     * DOCUMENT ME!
     *
     * @return DOCUMENT ME!
     */
    public String getName() {
        return theNames.getProperty(theLocale.toString());
    }

    /**
     * DOCUMENT ME!
     *
     * @return DOCUMENT ME!
     */
    public String getDescription() {
        return theDescriptions.getProperty(theLocale.toString());
    }

    /**
     * DOCUMENT ME!
     *
     * @return DOCUMENT ME!
     */
    public MessageResources getMessageResources() {
        return theMessageResources;
    }

    /**
     * DOCUMENT ME!
     *
     * @return DOCUMENT ME!
     */
    public IActor getActor() {
        return theActor;
    }

    /**
     * DOCUMENT ME!
     *
     * @param theCatalogGuid DOCUMENT ME!
     *
     * @return DOCUMENT ME!
     *
     * @throws CatalogException DOCUMENT ME!
     */
    synchronized public URL backUpCatalog(String theCatalogGuid)
                                   throws CatalogException {
        // figure out if we know that thing.
        CatalogInfo theCatalogInfo = null;
        Iterator anCatInfoIter = theCatalogInfos.iterator();
        boolean found = false;

        while (anCatInfoIter.hasNext()) {
            theCatalogInfo = (CatalogInfo) anCatInfoIter.next();

            if (theCatalogInfo.getGuid().intern() == theCatalogGuid.intern()) {
                found = true;

                break;
            }
        }
         // end of while ()

        if (!found) {
            theStaticLocToLog.error(ICatalogMessagesKeys.PCAT_WARN_CAT_NOT_FOUND);
            throw new CatalogException(theMessageResources.getMessage(ICatalogMessagesKeys.PCAT_WARN_CAT_NOT_FOUND));
        }
         // end of if ()

        this.setCatalogGuid(theCatalogGuid);

        ICatalogBackUpStrategy aStrategy = getBackUpStrategy(theCatalogInfo);

        return aStrategy.backUp();
    }

    /**
     * DOCUMENT ME!
     *
     * @param aFromCatalog DOCUMENT ME!
     * @param aToCatalog DOCUMENT ME!
     *
     * @return DOCUMENT ME!
     *
     * @throws CatalogException DOCUMENT ME!
     */
    protected ICatalogUpdateStrategy getUpdateStrategy(ICatalog aFromCatalog, ICatalog aToCatalog)
                                                throws CatalogException {
        return new UpdateStrategyDeferredDelete(aFromCatalog, aToCatalog);
        //        return new SimpleUpdateStrategy(aFromCatalog,aToCatalog);
    }

    /**
     * DOCUMENT ME!
     *
     * @param aCatalogInfo DOCUMENT ME!
     *
     * @return DOCUMENT ME!
     *
     * @throws CatalogException DOCUMENT ME!
     */
    protected ICatalogBackUpStrategy getBackUpStrategy(CatalogInfo aCatalogInfo)
                                                throws CatalogException {
        return new SimpleBackUpStrategy(aCatalogInfo);
    }

    /**
     * DOCUMENT ME!
     *
     * @param theBackup DOCUMENT ME!
     * @param theFromServerType DOCUMENT ME!
     *
     * @throws CatalogException DOCUMENT ME!
     */
    synchronized public void loadCatalogBackUp(URL theBackup, ServerType theFromServerType)
                                        throws CatalogException {
        ICatalogLoadBackUpStrategy theLoadStrategy = getCatalogLoadBackupStrategy(theBackup, theFromServerType);
        theLoadStrategy.loadBackUp();

        return;
    }

    /**
     * DOCUMENT ME!
     *
     * @param theBackUp DOCUMENT ME!
     * @param theFromServerType DOCUMENT ME!
     *
     * @return DOCUMENT ME!
     *
     * @throws CatalogException DOCUMENT ME!
     */
    protected ICatalogLoadBackUpStrategy getCatalogLoadBackupStrategy(URL theBackUp, ServerType theFromServerType)
        throws CatalogException {
        return new SimpleLoadCatalogBackUpStrategy(theBackUp, theFromServerType);
    }

    /**
     * Get the mappings for the fixed attribute names/guids of this instance. In case a catalog server type uses a
     * subset of fixed attribute names/guids he has to return the mapping from the external attribute names to its own
     * attribute names. From the signature follows that only in case the theFormServerType itself uses fixed attribute
     * names/guids (that cover the subset of fixed attribute values of this) that function must return a mapping.
     *
     * @param theFromServertype a <code>ServerType</code> value
     *
     * @return a <code>Document</code> value
     */
    protected Document getAttributeMapping(ServerType theFromServertype) {
        return null;
    }

    /**
     * Get the mappings for the fixed attribute names/guids of this instance. In case a catalog server type uses a
     * subset of fixed attribute names/guids he has to return the mapping from the external attribute names to its own
     * attribute names. From the signature follows that only in case the subset of fixed attribute names of
     * theFormServerType does not cover the subset of fixed attribute values of this that function must return a
     * mapping.
     *
     * @param theExternalCatalogGuid a <code>String</code> value
     *
     * @return a <code>Document</code> value
     */
    protected Document getAttributeMapping(String theExternalCatalogGuid) {
        return null;
    }

    /**
     * Build a list of references to all catalogs served on this server. The original actor is used as client.
     * Potentially heavy weight call if all or some catalogs are not cached.
     *
     * @return an <code>Iterator</code> value
     *
     * @exception CatalogException if an error occurs
     */
    synchronized public Iterator getCatalogs()
                                      throws CatalogException {
        IClient anActorAsClient = getActorAsClient();

        return getCatalogs(anActorAsClient);
    }

    /**
     * Build a list of references to all catalogs served on this server. Potentially heavy weight call if all or some
     * catalogs are not cached.
     *
     * @param aClient an <code>IClient</code> used to connect to all catalogs
     *
     * @return an <code>Iterator</code> value
     *
     * @exception CatalogException if an error occurs
     */
    synchronized public Iterator getCatalogs(IClient aClient)
                                      throws CatalogException {
        ArrayList theCatalogs = new ArrayList();
        Iterator aCataInfoIter = theCatalogInfos.iterator();

        while (aCataInfoIter.hasNext()) {
            ICatalogInfo anInfo = (ICatalogInfo) aCataInfoIter.next();
            ICatalog anCatalog = getCatalog(anInfo.getGuid(), aClient);
            theCatalogs.add(anCatalog);
        }
         // end of while ()

        synchronized (theCatalogs) {
            return Collections.unmodifiableList(theCatalogs).iterator();
        }
    }

    /**
     * DOCUMENT ME!
     *
     * @return DOCUMENT ME!
     */
    synchronized public Iterator getCatalogInfos() {
        return theCatalogInfos.iterator();
    }

    /**
     * Should return the valid catalog info
     *
     * @param aCatalogGuid DOCUMENT ME!
     *
     * @return DOCUMENT ME!
     */
    synchronized public ICatalogInfo getCatalogInfo(String aCatalogGuid) {
        ICatalogInfo theResult = null;
        Iterator anIter = theCatalogInfos.iterator();

        while (anIter.hasNext()) {
            theResult = (ICatalogInfo) anIter.next();

            if (theResult.getGuid().intern() == aCatalogGuid.intern()) {
                if (theResult instanceof TrexCatalogServerEngine.TrexManagedCatalogInfo) {
                    TrexCatalogServerEngine.TrexManagedCatalogInfo trexCatalogInfo = (TrexCatalogServerEngine.TrexManagedCatalogInfo) theResult;

                    if (trexCatalogInfo.getStatus().intern() == TrexCatalogServerEngine.TrexManagedCatalogInfo.STATUS_VALID.intern()) {
                        break;
                    }
                }
                else {
                    break;
                }
            }
             // end of if ()

            theResult = null;
        }

        return theResult;
    }

    /**
     * DOCUMENT ME!
     *
     * @return DOCUMENT ME!
     *
     * @throws CatalogException DOCUMENT ME!
     */
    public Iterator getTasks()
                      throws CatalogException {
        return CatalogTask.getTasks(this);
    }

    /**
     * DOCUMENT ME!
     *
     * @param aCatalog DOCUMENT ME!
     *
     * @return DOCUMENT ME!
     */
    abstract public boolean isCatalogUpToDate(Catalog aCatalog);
    
    /**
     * This method always STAGING_NOT_SUPPORTED,
     * because staging is only supported by IMS catalogues.
     * 
     * This method might be overwritten by the subclasses to return 
     * a different value.
     *
     * @return String staging is not supported
     */
    public String getStagingSupport() {
        return STAGING_NOT_SUPPORTED;
    }

    /**
     * DOCUMENT ME!
     *
     * @param anOtherGuid DOCUMENT ME!
     *
     * @throws CatalogException DOCUMENT ME!
     */
    synchronized public void setCatalogGuid(String anOtherGuid)
                                     throws CatalogException {
        Iterator anIter = theCatalogInfos.iterator();
        boolean found = false;

        while (anIter.hasNext()) {
            CatalogInfo aCatalogInfo = (CatalogInfo) anIter.next();
            String aGuid = aCatalogInfo.getGuid();

            if (aGuid.equals(anOtherGuid)) {
                theDefaultGuid = anOtherGuid;
                found = true;
            }
             // end of if ()
        }
         // end of while ()

        if (!found) {
            throw new CatalogException("Invalid guid!");
        }
         // end of if ()

        return;
    }

    /**
     * Build a catalog info list based on the given name pattern. If empty string is supplied, all catalogs known to
     * this server will be returned.
     *
     * @param aCatalogNamePattern a <code>String</code> value
     */
    abstract protected void buildCatalogInfosRemote(String aCatalogNamePattern);

    /**
     * event dispatching
     *
     * @param aListener DOCUMENT ME!
     */
    public synchronized void addServerEventListener(IServerEventlistener aListener) {
        if (!theServerEventListeners.contains(aListener)) {
            theServerEventListeners.add(aListener);
        }
         // end of if ()

        return;
    }

    /**
     * DOCUMENT ME!
     *
     * @param aListerner DOCUMENT ME!
     */
    public synchronized void removeServerEventListener(IServerEventlistener aListerner) {
        theServerEventListeners.remove(aListerner);

        return;
    }

    /**
     * DOCUMENT ME!
     *
     * @param aDeletedCatalog DOCUMENT ME!
     */
    synchronized protected void dispatchCatalogDeletedEvent(CatalogInfo aDeletedCatalog) {
        CatalogDeletedEvent aDeletedEvent = new CatalogDeletedEvent(this, aDeletedCatalog);
        Iterator anIter = theServerEventListeners.iterator();

        while (anIter.hasNext()) {
            IServerEventlistener anListener = (IServerEventlistener) anIter.next();
            anListener.catalogDeleted(aDeletedEvent);
        }
         // end of while ()

        return;
    }

    /**
     * DOCUMENT ME!
     *
     * @param aCreatedCatalog DOCUMENT ME!
     */
    synchronized protected void dispatchCatalogCreatedEvent(CatalogInfo aCreatedCatalog) {
        CatalogCreatedEvent aCreatedEvent = new CatalogCreatedEvent(this, aCreatedCatalog);
        Iterator anIter = theServerEventListeners.iterator();

        while (anIter.hasNext()) {
            IServerEventlistener anListener = (IServerEventlistener) anIter.next();
            anListener.catalogCreated(aCreatedEvent);
        }
         // end of while ()

        return;
    }

    /**
     * DOCUMENT ME!
     *
     * @param anUpdatedCatalog DOCUMENT ME!
     */
    synchronized protected void dispatchCatalogUpdatedEvent(CatalogInfo anUpdatedCatalog) {
        CatalogUpdatedEvent anUpdatedEvent = new CatalogUpdatedEvent(this, anUpdatedCatalog);
        Iterator anIter = theServerEventListeners.iterator();

        while (anIter.hasNext()) {
            IServerEventlistener anListener = (IServerEventlistener) anIter.next();
            anListener.catalogUpdated(anUpdatedEvent);
        }
         // end of while ()

        return;
    }

    /**
     * DOCUMENT ME!
     *
     * @throws CatalogException DOCUMENT ME!
     */
    synchronized public void deleteCatalogs()
                                     throws CatalogException {
        //deleteCatalogsRemote();
        Iterator anIter = getCatalogs();

        while (anIter.hasNext()) {
            deleteCatalog((ICatalog) anIter.next());
        }
         // end of while ()
    }

    /**
     * DOCUMENT ME!
     *
     * @param includeAllCacheImages DOCUMENT ME!
     *
     * @throws CatalogException DOCUMENT ME!
     */
    synchronized public void deleteCatalogs(boolean includeAllCacheImages)
                                     throws CatalogException {
        Iterator anIter = getCatalogs();

        while (anIter.hasNext()) {
            deleteCatalog((ICatalog) anIter.next(), includeAllCacheImages);
        }
         // end of while ()

        return;
    }

    /**
     * DOCUMENT ME!
     *
     * @param aCatalog DOCUMENT ME!
     *
     * @throws CatalogException DOCUMENT ME!
     */
    synchronized public void deleteCatalog(ICatalog aCatalog)
                                    throws CatalogException {
        deleteCatalog(aCatalog, true);

        return;
    }

    /**
     * DOCUMENT ME!
     *
     * @param aCatalog DOCUMENT ME!
     * @param includeAllCacheImages DOCUMENT ME!
     *
     * @throws CatalogException DOCUMENT ME!
     */
    synchronized public void deleteCatalog(ICatalog aCatalog, boolean includeAllCacheImages)
                                    throws CatalogException {
        deleteCatalog(aCatalog, includeAllCacheImages, true);
    }

    /**
     * DOCUMENT ME!
     *
     * @param aCatalog DOCUMENT ME!
     * @param includeAllCacheImages DOCUMENT ME!
     * @param removeCatalogRemote DOCUMENT ME!
     *
     * @throws CatalogException DOCUMENT ME!
     */
    synchronized public void deleteCatalog(ICatalog aCatalog, boolean includeAllCacheImages, boolean removeCatalogRemote)
                                    throws CatalogException {
        //make sure the thing is not already deleted
        synchronized (aCatalog) {
            if (theStaticLocToLog.isDebugEnabled()) {
                String theMessage = theMessageResources.getMessage(ICatalogMessagesKeys.PCAT_TRY_DEL_CAT_SITE,
                                                                   this.getGuid(), aCatalog.getGuid());
                theStaticLocToLog.debug(theMessage);
            }
             // end of if ()

            if (aCatalog.getState() == IComponent.State.DELETED) {
                theStaticLocToLog.error(ICatalogMessagesKeys.PCAT_ERR_IS_DELEDED);

                return;
            }
             // end of if ()

            //make sure we own that thing
            if (((Catalog) aCatalog).getServerEngine() != this) {
                String theMessage = theMessageResources.getMessage(ICatalogMessagesKeys.PCAT_ERR_OBJ_NOT_OWNED_THIS);
                theStaticLocToLog.error(ICatalogMessagesKeys.PCAT_ERR_OBJ_NOT_OWNED_THIS);
                throw new CatalogException(theMessage);
            }
             // end of if ()

            //make sure we got that thing
            Iterator anIter = this.theCatalogInfos.iterator();
            boolean found = false;
            ICatalogInfo aCatalogInfo = null;

            while (anIter.hasNext()) {
                aCatalogInfo = (ICatalogInfo) anIter.next();

                if (aCatalogInfo.getGuid().intern() == aCatalog.getGuid().intern()) {
                    if (aCatalogInfo instanceof TrexCatalogServerEngine.TrexManagedCatalogInfo &&
                            aCatalog instanceof TrexManagedCatalog) {
                        TrexCatalogServerEngine.TrexManagedCatalogInfo trexCatalogInfo = (TrexCatalogServerEngine.TrexManagedCatalogInfo) aCatalogInfo;
                        TrexManagedCatalog managedCatalog = (TrexManagedCatalog) aCatalog;

                        if (managedCatalog.getIndexID().equalsIgnoreCase(trexCatalogInfo.getIndexId())) {
                            found = true;

                            break;
                        }

                        // We need to compare the index ids here

                        /*
                        if (trexCatalogInfo.getStatus().intern() == TrexCatalogServerEngine.TrexManagedCatalogInfo.STATUS_VALID.intern())
                        {
                          found = true;
                          break;
                        }
                        */
                    }
                    else {
                        found = true;

                        break;
                    }
                }
                 // end of if ()
            }

            if (!found) {
                theStaticLocToLog.error(ICatalogMessagesKeys.PCAT_WARN_CAT_NOT_FOUND);

                String theMessage = theMessageResources.getMessage(ICatalogMessagesKeys.PCAT_WARN_CAT_NOT_FOUND);
                throw new CatalogException(theMessage);
            }
             // end of if ()

            //make sure no other instance was created on this physical server for another client
            Iterator theCachedInstances = CatalogCache.getInstance().getCachedInstances();
            ArrayList theOtherImages = new ArrayList();

            while (theCachedInstances.hasNext()) {
                CachableItem aItem = (CachableItem) theCachedInstances.next();

                if (!(aItem instanceof Catalog)) {
                    continue;
                }

                Catalog anOtherCat = (Catalog) aItem;

                if (anOtherCat == aCatalog) {
                    continue;
                }

                if (anOtherCat.getServerGuid().intern() != aCatalog.getServerGuid().intern()) {
                    continue;
                }

                if (anOtherCat.getGuid().intern() != aCatalog.getGuid().intern()) {
                    continue;
                }

                if (anOtherCat.getState() == IComponent.State.DELETED) {
                    continue;
                }

                if (includeAllCacheImages) {
                    theOtherImages.add(anOtherCat);
                }
                else {
                    throw new CatalogException(ICatalogMessagesKeys.PCAT_ERR_LOCK);
                }
                 // end of else
            }

            if (removeCatalogRemote) {
                //delete it remote
                this.deleteCatalogRemote((CatalogInfo) aCatalogInfo);
            }

            //removal from the cache (for that instance) takes place automatically
            //remove it from the internal collection
            anIter.remove();

            //delete it
            ((Catalog) aCatalog).setStateDeleted();

            //inform your listeners that you are changed
            this.dispatchCatalogDeletedEvent((CatalogInfo) aCatalogInfo);

            //the other images
            Iterator aCatIter = theOtherImages.iterator();

            while (aCatIter.hasNext()) {
                Catalog aCat = (Catalog) aCatIter.next();
                String aGuid = aCat.getGuid();
                CatalogServerEngine anEngine = aCat.getServerEngine();
                ICatalogInfo anInfo = anEngine.getCatalogInfo(aGuid);
                aCat.setStateDeleted();
                anEngine.removeCatalogInfo(anInfo);
                anEngine.dispatchCatalogDeletedEvent((CatalogInfo) anInfo);
            }
             // end of while ()
        }

        return;
    }

    /**
     * DOCUMENT ME!
     *
     * @param aCataloInfo DOCUMENT ME!
     */
    synchronized protected void removeCatalogInfo(ICatalogInfo aCataloInfo) {
        this.theCatalogInfos.remove(aCataloInfo);

        return;
    }

    /**
     * DOCUMENT ME!
     *
     * @param aDoomedCatalog DOCUMENT ME!
     * @param aSchedule DOCUMENT ME!
     *
     * @throws CatalogException DOCUMENT ME!
     */
    synchronized public void deleteCatalog(ICatalog aDoomedCatalog, ICatalogTask.Schedule aSchedule)
                                    throws CatalogException {
        deleteCatalog(aDoomedCatalog, false, aSchedule);

        return;
    }

    /**
     * DOCUMENT ME!
     *
     * @param aDoomedCatalog DOCUMENT ME!
     * @param includeAllCacheImages DOCUMENT ME!
     * @param aSchedule DOCUMENT ME!
     *
     * @throws CatalogException DOCUMENT ME!
     */
    synchronized public void deleteCatalog(ICatalog aDoomedCatalog, boolean includeAllCacheImages,
                                           ICatalogTask.Schedule aSchedule)
                                    throws CatalogException {
        ICatalogTask aDeleteTask = getDeleteTask(aDoomedCatalog, includeAllCacheImages, aSchedule);
        aDeleteTask.schedule();

        return;
    }

    /**
     * DOCUMENT ME!
     *
     * @param aFromCatalog DOCUMENT ME!
     * @param aToCatalog DOCUMENT ME!
     *
     * @throws CatalogException DOCUMENT ME!
     */
    synchronized public void updateCatalog(ICatalog aFromCatalog, ICatalog aToCatalog)
                                    throws CatalogException {
        //make sure we own the ToCatalog
        synchronized (aToCatalog) {
            Catalog aToCat = (Catalog) aToCatalog;

            if (aToCat.getServerGuid().intern() != this.theGuid.intern()) {
                String theMessage = theMessageResources.getMessage(ICatalogMessagesKeys.PCAT_ERR_OBJ_NOT_OWNED_THIS);
                theStaticLocToLog.error(ICatalogMessagesKeys.PCAT_ERR_OBJ_NOT_OWNED_THIS);
                throw new CatalogException(theMessage);
            }
             // end of if ()

            synchronized (aFromCatalog) {
                //make sure we don't own the FromCatalog,
                Catalog aFromCat = (Catalog) aFromCatalog;

                if (aFromCat.getServerGuid().intern() == this.theGuid.intern()) {
                    String theMessage = theMessageResources.getMessage(ICatalogMessagesKeys.PCAT_ERR_OBJ_ALREADY_OWNED_THIS);
                    theStaticLocToLog.error(ICatalogMessagesKeys.PCAT_ERR_OBJ_ALREADY_OWNED_THIS);
                    throw new CatalogException(theMessage);
                }
                 // end of if ()

                //make sure both are not deleted
                if ((aFromCatalog.getState() == IComponent.State.DELETED) ||
                        (aToCatalog.getState() == IComponent.State.DELETED)) {
                    theStaticLocToLog.error(ICatalogMessagesKeys.PCAT_ERR_IS_DELEDED);

                    String msg = theMessageResources.getMessage(ICatalogMessagesKeys.PCAT_ERR_IS_DELEDED);
                    throw new CatalogException(msg);
                }
                 // end of if ()

                ICatalogUpdateStrategy aStrategy = this.getUpdateStrategy(aFromCatalog, aToCatalog);
                aStrategy.update();
            }
        }

        return;
    }

    /**
     * DOCUMENT ME!
     *
     * @param aFromCatalog DOCUMENT ME!
     * @param aToCatalog DOCUMENT ME!
     * @param aSchedule DOCUMENT ME!
     *
     * @throws CatalogException DOCUMENT ME!
     */
    synchronized public void updateCatalog(ICatalog aFromCatalog, ICatalog aToCatalog, ICatalogTask.Schedule aSchedule)
                                    throws CatalogException {
        ICatalogTask anUpdateTask = getUpdateCatalogTask(aFromCatalog, aToCatalog, aSchedule);
        anUpdateTask.schedule();

        return;
    }

    /**
     * Add a new catalog to this server. Does all necessary internal bookkeeping for this new task (i.e. generates a
     * new task immediately and executes it once) and submits it to the concrete server (in almost any case a remote
     * server). in case an error occurs immediately during submission of the task an exception is throw. Check in any
     * case the status of the generated task.
     *
     * @param aNewCatalog an <code>InputSource</code> that should be valid catalog XML.
     * @param aMappingDocument DOCUMENT ME!
     *
     * @exception CatalogException if an error occurs
     */
    synchronized public void addCatalog(InputSource aNewCatalog, Source aMappingDocument)
                                 throws CatalogException {
        //add to task list
        //internal book keeping will be done by the implementation
        this.addCatalogRemote(aNewCatalog, aMappingDocument);
        this.dispatchCatalogCreatedEvent(null);

        //TODO
        return;
    }

    /**
     * Triggers the same process as the method without the schedule except that the process does not necessarily start
     * immediately.
     *
     * @param aNewCatalog an <code>InputSource</code> value
     * @param aMappingDocument a <code>Source</code> value
     * @param aSchedule an <code>ICatalogTask.Schedule</code> value
     *
     * @exception CatalogException if an error occurs
     */
    synchronized public void addCatalog(InputSource aNewCatalog, Source aMappingDocument,
                                        ICatalogTask.Schedule aSchedule)
                                 throws CatalogException {
        ICatalogTask anAddTask = getAddTask(aNewCatalog, aMappingDocument, aSchedule);
        anAddTask.schedule();

        return;
    }

    /**
     * DOCUMENT ME!
     *
     * @return DOCUMENT ME!
     */
    public InitializationEnvironment getEnvironment() {
        return theSite.getEnvironment();
    }

    File getServerDir(String aPathRelativeToTheServerDir) {
        StringBuffer aPathBuffer = new StringBuffer("Site_");
        aPathBuffer.append(theSite.getGuid());
        aPathBuffer.append(System.getProperty("file.separator"));
        aPathBuffer.append("Server_");
        aPathBuffer.append(getGuid());

        if ((aPathRelativeToTheServerDir != null) && !aPathRelativeToTheServerDir.equals("")) {
            aPathBuffer.append(aPathRelativeToTheServerDir);
        }
         // end of if ()

        String thePathToBackUp = theSite.getEnvironment().getRealPath(aPathBuffer.toString());

        if (theStaticLocToLog.isDebugEnabled()) {
            String theMessage = theMessageResources.getMessage(ICatalogMessagesKeys.PCAT_DIR_USE, thePathToBackUp);
            theStaticLocToLog.debug(theMessage);
        }
         // end of if ()

        File theBackUpDir = new File(thePathToBackUp);

        if (!theBackUpDir.exists()) {
            if (!theBackUpDir.mkdirs()) {
                theBackUpDir = null;
            }
             // end of if ()
        }
         // end of if ()

        return theBackUpDir;
    }

    /**
     * DOCUMENT ME!
     *
     * @param aNewCatalog DOCUMENT ME!
     * @param aMappingDocument DOCUMENT ME!
     * @param aSchedule DOCUMENT ME!
     *
     * @return DOCUMENT ME!
     *
     * @throws CatalogException DOCUMENT ME!
     */
    protected ICatalogTask getAddTask(InputSource aNewCatalog, Source aMappingDocument, ICatalogTask.Schedule aSchedule)
                               throws CatalogException {
        ICatalogTask aNewTask = null;

        //check schedule for reasonable times
        if ((aSchedule.getStart().compareTo(aSchedule.getEnd()) != 0) ||
                aSchedule.isPeriodic() ||
                (aSchedule.getStart().compareTo(new Date()) <= 0)) {
            String msg = theMessageResources.getMessage(ICatalogMessagesKeys.PCAT_ERR_SUPPLY_ADD_SCHEDULE);
            throw new CatalogException(msg);
        }
         // end of if ()

        //store the stuff temporarily
        File theCatalogFileHandle = storeInputSource(aNewCatalog);

        File theMappingFileHandle = storeSource(aMappingDocument);

        //create the task
        aNewTask = new AddCatalogTask(this, theCatalogFileHandle, theMappingFileHandle, aSchedule,
                                      this.getMessageResources());

        //persist the task
        return aNewTask;
    }

    /**
     * DOCUMENT ME!
     *
     * @param aNewCatalog DOCUMENT ME!
     * @param aSchedule DOCUMENT ME!
     *
     * @return DOCUMENT ME!
     *
     * @throws CatalogException DOCUMENT ME!
     */
    protected ICatalogTask getAddCatalogInstanceTask(ICatalog aNewCatalog, ICatalogTask.Schedule aSchedule)
                                              throws CatalogException {
        ICatalogTask aNewTask = null;

        //check schedule for reasonable times
        if ((aSchedule.getStart().compareTo(aSchedule.getEnd()) != 0) ||
                aSchedule.isPeriodic() ||
                (aSchedule.getStart().compareTo(new Date()) <= 0)) {
            String msg = theMessageResources.getMessage(ICatalogMessagesKeys.PCAT_ERR_SUPPLY_ADD_SCHEDULE);
            throw new CatalogException(msg);
        }
         // end of if ()

        //create the task
        aNewTask = new AddCatalogInstanceTask(this, aNewCatalog, aSchedule, getMessageResources());

        return aNewTask;
    }

    /**
     * DOCUMENT ME!
     *
     * @param aFromCatalog DOCUMENT ME!
     * @param aToCatalog DOCUMENT ME!
     * @param aSchedule DOCUMENT ME!
     *
     * @return DOCUMENT ME!
     *
     * @throws CatalogException DOCUMENT ME!
     */
    protected ICatalogTask getUpdateCatalogTask(ICatalog aFromCatalog, ICatalog aToCatalog,
                                                ICatalogTask.Schedule aSchedule)
                                         throws CatalogException {
        ICatalogTask aNewTask = null;

        //check schedule for reasonable times
        if ((aSchedule.getStart().compareTo(aSchedule.getEnd()) >= 0) ||
                (aSchedule.getStart().compareTo(new Date()) <= 0)) {
            String msg = theMessageResources.getMessage(ICatalogMessagesKeys.PCAT_ERR_SUPPLY_UPD_SCHEDULE);
            throw new CatalogException(msg);
        }
         // end of if ()

        ICatalogInfo aToCatInfo = this.getCatalogInfo(aToCatalog.getGuid());

        //create the task
        aNewTask = new UpdateCatalogInstanceTask(this, aFromCatalog, aToCatInfo, aSchedule, getMessageResources());

        return aNewTask;
    }

    /**
     * DOCUMENT ME!
     *
     * @param aDoomedCatalog DOCUMENT ME!
     * @param includeAllCacheImages DOCUMENT ME!
     * @param aSchedule DOCUMENT ME!
     *
     * @return DOCUMENT ME!
     *
     * @throws CatalogException DOCUMENT ME!
     */
    protected ICatalogTask getDeleteTask(ICatalog aDoomedCatalog, boolean includeAllCacheImages,
                                         ICatalogTask.Schedule aSchedule)
                                  throws CatalogException {
        ICatalogTask aNewTask = null;

        //check schedule for reasonable times
        if ((aSchedule.getStart().compareTo(aSchedule.getEnd()) != 0) ||
                aSchedule.isPeriodic() ||
                (aSchedule.getStart().compareTo(new Date()) <= 0)) {
            String msg = theMessageResources.getMessage(ICatalogMessagesKeys.PCAT_ERR_SUPPLY_DEL_SCHEDULE);
            throw new CatalogException(msg);
        }
         // end of if ()

        //create the task
        aNewTask = new DeleteCatalogInstanceTask(this, aDoomedCatalog, includeAllCacheImages, aSchedule,
                                                 getMessageResources());

        return aNewTask;
    }

    /**
     * DOCUMENT ME!
     *
     * @param aSourceCatalog DOCUMENT ME!
     *
     * @throws CatalogException DOCUMENT ME!
     */
    synchronized public void addCatalog(ICatalog aSourceCatalog)
                                 throws CatalogException {
        //make sure the thing is not already deleted
        synchronized (aSourceCatalog) {
            if (aSourceCatalog.getState() == IComponent.State.DELETED) {
                theStaticLocToLog.error(ICatalogMessagesKeys.PCAT_ERR_IS_DELEDED);

                String msg = theMessageResources.getMessage(ICatalogMessagesKeys.PCAT_ERR_IS_DELEDED);
                throw new CatalogException(msg);
            }
             // end of if ()

            //make sure we don't own that thing already
            if (aSourceCatalog.getServerGuid().intern() == this.getGuid().intern()) {
                String theMessage = theMessageResources.getMessage(ICatalogMessagesKeys.PCAT_ERR_OBJ_ALREADY_OWNED_THIS);
                theStaticLocToLog.error(ICatalogMessagesKeys.PCAT_ERR_OBJ_ALREADY_OWNED_THIS);
                throw new CatalogException(theMessage);
            }
             // end of if ()

            //make sure we don't got that thing (paranoid double check)
            Iterator anIter = this.theCatalogInfos.iterator();
            boolean found = false;
            ICatalogInfo aCatalogInfo = null;

            while (anIter.hasNext()) {
                aCatalogInfo = (ICatalogInfo) anIter.next();

                if (aCatalogInfo.getGuid().intern() == aSourceCatalog.getGuid().intern()) {
                    if (aCatalogInfo instanceof TrexCatalogServerEngine.TrexManagedCatalogInfo) {
                        TrexCatalogServerEngine.TrexManagedCatalogInfo trexCatalogInfo = (TrexCatalogServerEngine.TrexManagedCatalogInfo) aCatalogInfo;

                        if (trexCatalogInfo.getStatus().intern() == TrexCatalogServerEngine.TrexManagedCatalogInfo.STATUS_VALID.intern()) {
                            found = true;

                            break;
                        }
                    }
                    else {
                        found = true;

                        break;
                    }
                }
                 // end of if ()
            }

            if (found) {
                theStaticLocToLog.error(ICatalogMessagesKeys.PCAT_ERR_OBJ_ALREADY_OWNED_THIS);

                String theMessage = theMessageResources.getMessage(ICatalogMessagesKeys.PCAT_ERR_OBJ_ALREADY_OWNED_THIS);
                throw new CatalogException(theMessage);
            }
             // end of if ()

            addCatalogDirect((Catalog) aSourceCatalog);

            //preload if necessary
        }

        return;
    }

    private void addCatalogDirect(Catalog aSourceCatalog)
                           throws CatalogException// only called by addCatalog(ICatalog aSourceCatalog)
     {
        IServerEngine theOwnerEngine = aSourceCatalog.getServerEngine();

        // load the catalog on sourceServer
        theStaticLocToLog.info(ICatalogMessagesKeys.PCAT_INFO, new Object[] { "loading catalog() starting" }, null);

        /** Remove the catalog from the cache, do a getCatalog, and this should get the latest version of the catalog. */
        CatalogCache cache = null;

        try {
            cache = CatalogCache.getInstance();
        }
        catch (CatalogCacheException cce) {
            throw new CatalogException(cce.getMessage());
        }
         // end of try-catch

        // try to build a key - if not possible the catalog won't be cached
        CatalogInfo theCatalogInfo = (CatalogInfo) aSourceCatalog.getServerEngine().getCatalogInfo(aSourceCatalog.getGuid());

        if (theCatalogInfo == null) {
            throw new CatalogException("Internal Error, could not get catalog info");
        }

        IClient aUser = new CatalogClient(theActor);
        String key = CatalogCache.generateKey(theCatalogInfo.getImplClass(), aUser, aSourceCatalog.getServerEngine());

        Catalog catalog = null;

        if ((key != null) && (key.length() != 0)) {
            catalog = (Catalog) cache.getCachedInstance(key);
        }

        if ((catalog != null) && (catalog == aSourceCatalog)) {
            cache.removeInstance(key);
            aSourceCatalog = (Catalog) aSourceCatalog.getServerEngine().getCatalog(theCatalogInfo.getGuid());
        }

        aSourceCatalog.getDetails();

        Iterator aCateIter = aSourceCatalog.getCategories();

        while (aCateIter.hasNext()) {
            ICategory aCategory = (ICategory) aCateIter.next();
            aCategory.getDetails();
            aCategory.getItems();
        }

        theStaticLocToLog.info(ICatalogMessagesKeys.PCAT_INFO, new Object[] { "loading catalog() finished" }, null);

        // transfer the sourceCatalog to this ServerEngine
        addCatalogRemote(aSourceCatalog);
    }

    /**
     * DOCUMENT ME!
     *
     * @param aNewCatalog DOCUMENT ME!
     * @param aSchedule DOCUMENT ME!
     *
     * @throws CatalogException DOCUMENT ME!
     */
    synchronized public void addCatalog(ICatalog aNewCatalog, ICatalogTask.Schedule aSchedule)
                                 throws CatalogException {
        ICatalogTask anAddCatalogInstanceTask = getAddCatalogInstanceTask(aNewCatalog, aSchedule);
        anAddCatalogInstanceTask.schedule();

        return;
    }

    /**
     * DOCUMENT ME!
     *
     * @param aGuid DOCUMENT ME!
     *
     * @return DOCUMENT ME!
     *
     * @throws CatalogException DOCUMENT ME!
     */
    public ICatalog getCatalog(String aGuid)
                        throws CatalogException {
        return getCatalog(aGuid, getActorAsClient());
    }

    /**
     * Return a reference to the Catalog defined by the guid. The user of the catalog is defined by the client. If the
     * catalog is not known by the server an exception is thrown.
     *
     * @param theCatalogGuid a <code>String</code> value
     * @param aUser an <code>IClient</code> value
     *
     * @return an <code>ICatalog</code> value
     *
     * @exception CatalogException if an error occurs
     */
    synchronized public ICatalog getCatalog(String theCatalogGuid, IClient aUser)
                                     throws CatalogException {
        //courtesy CRM
        theCatalogGuid = theCatalogGuid.trim();

        // figure out if we know that thing.
        CatalogInfo theCatalogInfo = null;
        Iterator anCatInfoIter = theCatalogInfos.iterator();
        boolean found = false;

        while (anCatInfoIter.hasNext()) {
            theCatalogInfo = (CatalogInfo) anCatInfoIter.next();

            if (theCatalogInfo.getGuid().intern() == theCatalogGuid.intern()) {
                /**
                 * gd -- added status This may be a delete pending catalog, so get the latest... If there is no VALID
                 * catalog, no catalog is available...
                 */
                String status = theCatalogInfo.getProperty("Status");

                if (status != null) {
                    if (status.equalsIgnoreCase("Valid")) {
                        found = true;

                        break;
                    }
                }
                else {
                    found = true;

                    break;
                }
            }
        }
         // end of while ()

        if (!found) {
            /**
             * This surely needs to get modified !
             */
            if (this instanceof TrexCatalogServerEngine) {
                ((TrexCatalogServerEngine) this).rebuildManagedCatalogInfos();
            }

            buildCatalogInfosRemote(theCatalogGuid);
            anCatInfoIter = theCatalogInfos.iterator();

            while (anCatInfoIter.hasNext()) {
                theCatalogInfo = (CatalogInfo) anCatInfoIter.next();

                if (theCatalogInfo.getGuid().intern() == theCatalogGuid.intern()) {
                    /**
                     * gd -- added status This may be a delete pending catalog, so get the valid one... If there is no
                     * VALID catalog, no catalog is available...
                     */
                    String status = theCatalogInfo.getProperty("Status");

                    if (status != null) {
                        if (status.equalsIgnoreCase("Valid")) {
                            found = true;

                            break;
                        }
                    }
                    else {
                        found = true;

                        break;
                    }
                }
            }
             // end of while ()
        }

        /**
         * gd --- Trying a quick fix for the added catalog...Assuming that the guid is name_variant and that the name
         * does not have an "_" in it. JP: fails on catalog names containing underscore -> search underscore in
         * CatalogGuid from left to right (variant name must not contain "_")
         */
        if (!found) {
            try {
                int indexOfUnderScore = theCatalogGuid.lastIndexOf("_");

                if (indexOfUnderScore > 0) {
                    String catName = theCatalogGuid.substring(0, theCatalogGuid.lastIndexOf("_"));
                    buildCatalogInfosRemote(catName);
                    anCatInfoIter = theCatalogInfos.iterator();

                    while (anCatInfoIter.hasNext()) {
                        theCatalogInfo = (CatalogInfo) anCatInfoIter.next();

                        if (theCatalogInfo.getGuid().intern() == theCatalogGuid.intern()) {
                            /**
                             * gd -- added status This may be a delete pending catalog, so get the valid one... If
                             * there is no VALID catalog, no catalog is available...
                             */
                            String status = theCatalogInfo.getProperty("Status");

                            if (status != null) {
                                if (status.equalsIgnoreCase("Valid")) {
                                    found = true;

                                    break;
                                }
                            }
                            else {
                                found = true;

                                break;
                            }
                        }
                    }
                     // end of while ()
                }
            }
            catch (Throwable th) {
				theStaticLocToLog.debug(th.getMessage());
                // Ignore the exception, this is a quick fix.
            }
        }

        if (!found) {
            theStaticLocToLog.warn(ICatalogMessagesKeys.PCAT_WARN_CAT_NOT_FOUND, new Object[] { theCatalogGuid }, null);

            return null;
        }
         // end of if ()

        if (aUser == null) {
            aUser = new CatalogClient(theActor);
        }
         // end of if ()

        this.setCatalogGuid(theCatalogGuid);

        Catalog catalog = null;
        String key = null;
        CatalogCache cache = null;
        
        // don't search for staging catalogues in the Cache
        
        if (aUser.getCatalogStatus() != IClient.CATALOG_STATE_INACTIVE) {
            // first search in the catalog cache
            
            try {
                cache = CatalogCache.getInstance();
            }
            catch (CatalogCacheException cce) {
                throw new CatalogException(cce.getMessage());
            }
             // end of try-catch

            // try to build a key - if not possible the catalog won't be cached
            key = CatalogCache.generateKey(theCatalogInfo.getImplClass(), aUser, this);

            if ((key != null) && (key.length() != 0)) {
                catalog = (Catalog) cache.getCachedInstance(key);
            }

            // gd -- try removal from cache
            if (catalog != null) {
                theStaticLocToLog.debug("Got the catalog from the cache");
            }

            if (catalog != null && !isCatalogUpToDate(catalog)) {
                theStaticLocToLog.debug("Removing from cache");
                cache.removeInstance(key);

                /**
                 * Now to get hold of the new catalog information
                 */

                /*
                System.out.println("Calling build catalog information");
                if (this instanceof TrexCatalogServerEngine)
                {
                  ((TrexCatalogServerEngine)this).rebuildManagedCatalogInfos();
                  System.out.println("Get catalog information");
                  theCatalogInfo = (CatalogServerEngine.CatalogInfo)getCatalogInfo(catalog.getGuid());
                }
                */
                catalog = null;
            }
        }

        // the catalog was not cached yet - create new one and cache it now!
        if (catalog == null) {
            if (this instanceof TrexCatalogServerEngine) {
                theStaticLocToLog.info("Rebuilding catalog information");
                ((TrexCatalogServerEngine) this).rebuildManagedCatalogInfos();
            }

            theCatalogInfo = (CatalogServerEngine.CatalogInfo) getCatalogInfo(theCatalogInfo.getGuid());

            if (theCatalogInfo == null) {
                return null;
            }

            theStaticLocToLog.debug("calling getCatalogRemote");
            catalog = getCatalogRemote(theCatalogInfo, aUser);

            // only cache active catalogues
            if (aUser.getCatalogStatus() == IClient.CATALOG_STATE_ACTIVE) {
                cache.addCachableInstance(key, catalog);
            }
            else {
                theStaticLocToLog.debug("catalog will not be referenced from the catalogue cache");
            }
        }

        return catalog;
    }

    void addProperty(String name, String key, String value) {
        //unused: Properties theResult=null;
        if (name.intern() == "name".intern()) {
            if (theNames == null) {
                theNames = new Properties();
            }
             // end of if ()

            theNames.setProperty(key, value);
        }
         // end of if ()

        if (name.intern() == "description".intern()) {
            if (theDescriptions == null) {
                theDescriptions = new Properties();
            }
             // end of if ()

            theDescriptions.setProperty(key, value);
        }
         // end of if ()

        return;
    }

    private IClient getActorAsClient() {
        if (theActor instanceof IClient) {
            return (IClient) theActor;
        }
         // end of if ()
        else {
            CatalogClient aClient = new CatalogClient(theActor);

            return aClient;
        }
         // end of else
    }

    Source loadSource(File aSourceFile)
               throws CatalogException {
        DOMSource aSource = null;

        try {
            DocumentBuilder aDomBuilder = DocumentBuilderFactory.newInstance().newDocumentBuilder();
            Document aDoc = aDomBuilder.parse(aSourceFile);
            aSource = new DOMSource(aDoc);
        }
        catch (Throwable t) {
            String msg = theMessageResources.getMessage(ICatalogMessagesKeys.PCAT_ERR_LOAD_SOURCE_FILE,
                                                        aSourceFile.getAbsolutePath());
            theStaticLocToLog.error(ICatalogMessagesKeys.PCAT_ERR_LOAD_SOURCE_FILE,
                                    new Object[] { aSourceFile.getAbsolutePath() }, t);
            throw new CatalogException(msg, t);
        }
         // end of try-catch

        return aSource;
    }

    private File storeSource(Source aSource)
                      throws CatalogException {
        File aResultFile = null;

        File thePathToBackUp = getServerDir(System.getProperty("file.separator") + "temp");

        if (thePathToBackUp == null) {
            String theMessage = theMessageResources.getMessage(ICatalogMessagesKeys.PCAT_ERR_FIND_CREATE_DIR, "temp");
            throw new CatalogException(theMessage);
        }
         // end of if ()

        //Make a writer
        StringBuffer aFileNameBuffer = new StringBuffer(thePathToBackUp.getAbsolutePath());
        aFileNameBuffer.append("/").append(this.getClass().getName()).append("BackupSource")
                       .append(System.currentTimeMillis()).append(".xml");
        aResultFile = new File(aFileNameBuffer.toString());

        try {
            FileOutputStream aLocalOutput = new FileOutputStream(aResultFile);
            OutputStreamWriter aWriter = new OutputStreamWriter(aLocalOutput, "UTF-8");
            TransformerFactory tFactory = TransformerFactory.newInstance();

            //build your simple copyAll xsl
            StringBuffer anotherBuffer = new StringBuffer("<?xml version=\"1.0\"?>");
            anotherBuffer.append("<xsl:stylesheet xmlns:xsl=\"http://www.w3.org/1999/XSL/Transform\" version=\"1.0\">");
            anotherBuffer.append("<xsl:template match=\"/\">");
            anotherBuffer.append("<xsl:copy-of select=\"*\"/>");
            anotherBuffer.append("</xsl:template>");
            anotherBuffer.append("</xsl:stylesheet>");

            Transformer transformer = tFactory.newTransformer(new StreamSource(new StringReader(anotherBuffer.toString())));
            transformer.transform(aSource, new StreamResult(aWriter));
            aWriter.close();
        }
        catch (TransformerConfigurationException tce) {
            throw new CatalogException("Error during backup!", tce);
        }
         // end of catch
        catch (TransformerException te) {
            throw new CatalogException("Error during backup!", te);
        }
         // end of catch
        catch (IOException ioe) {
            throw new CatalogException("Error during backup!", ioe);
        }
         // end of try-catch

        return aResultFile;
    }

    InputSource loadInputSource(File aSourceFile)
                         throws CatalogException {
        InputSource aInputSource = null;

        try {
            InputStream aStream = new FileInputStream(aSourceFile);
            aInputSource = new InputSource(aStream);
        }
        catch (Throwable t) {
            String msg = theMessageResources.getMessage(ICatalogMessagesKeys.PCAT_ERR_LOAD_SOURCE_FILE,
                                                        aSourceFile.getAbsolutePath());
            theStaticLocToLog.error(ICatalogMessagesKeys.PCAT_ERR_LOAD_SOURCE_FILE,
                                    new Object[] { aSourceFile.getAbsolutePath() }, t);
            throw new CatalogException(msg, t);
        }
         // end of try-catch

        return aInputSource;
    }

    private File storeInputSource(InputSource aInput)
                           throws CatalogException {
        File aResultFile = null;
        FileOutputStream aLocalOutput = null;

        try {
            InputStream aInputStream = aInput.getByteStream();

            File thePathToBackUp = getServerDir(System.getProperty("file.separator") + "temp");

            //Make a writer
            StringBuffer aFileNameBuffer = new StringBuffer(thePathToBackUp.getAbsolutePath());
            aFileNameBuffer.append("/").append(this.getClass().getName()).append("BackupInputSource")
                           .append(System.currentTimeMillis()).append(".xml");
            aResultFile = new File(aFileNameBuffer.toString());

            aLocalOutput = new FileOutputStream(aResultFile);

            byte array[] = new byte[1024];
            int read = -1;

            while ((read = aInputStream.read(array)) != -1) {
                aLocalOutput.write(array, 0, read);
            }
             // end of while ()

            aLocalOutput.close();
        }
        catch (IOException ioe) {
            String theMessage = theMessageResources.getMessage(ICatalogMessagesKeys.PCAT_ERR_BACKUP_FILE,
                                                               aResultFile.getAbsolutePath());
            throw new CatalogException(theMessage, ioe);
        }
         // end of try-catch

        return aResultFile;
    }

    /**
     * DOCUMENT ME!
     *
     * @return DOCUMENT ME!
     *
     * @throws CatalogException DOCUMENT ME!
     */
    abstract protected Catalog createCatalogRemote()
                                            throws CatalogException;

    /**
     * DOCUMENT ME!
     *
     * @throws CatalogException DOCUMENT ME!
     */
    abstract protected void deleteCatalogsRemote()
                                          throws CatalogException;

    /**
     * DOCUMENT ME!
     *
     * @param aCatalogInfo DOCUMENT ME!
     *
     * @throws CatalogException DOCUMENT ME!
     */
    abstract protected void deleteCatalogRemote(CatalogInfo aCatalogInfo)
                                         throws CatalogException;

    /**
     * DOCUMENT ME!
     *
     * @param aNewCatalog DOCUMENT ME!
     * @param aMapping DOCUMENT ME!
     *
     * @throws CatalogException DOCUMENT ME!
     */
    abstract protected void addCatalogRemote(InputSource aNewCatalog, Source aMapping)
                                      throws CatalogException;

    /**
     * DOCUMENT ME!
     *
     * @param catalog DOCUMENT ME!
     *
     * @throws CatalogException DOCUMENT ME!
     */
    abstract protected void addCatalogRemote(Catalog catalog)
                                      throws CatalogException;

    /**
     * DOCUMENT ME!
     *
     * @param theCatalogInfo DOCUMENT ME!
     * @param aUser DOCUMENT ME!
     *
     * @return DOCUMENT ME!
     *
     * @throws CatalogException DOCUMENT ME!
     */
    abstract protected Catalog getCatalogRemote(CatalogInfo theCatalogInfo, IClient aUser)
                                         throws CatalogException;

    public class CatalogInfo implements ICatalogInfo {
        public static final String PROPERTY_NAME = "name";
        public static final String PROPERTY_DESCRIPTION = "description";
        protected String theCatalogGuid;
        private Properties theProperties = new Properties();
        private Properties theCatalogNames = new Properties();
        private Properties theCatalogDescriptions = new Properties();
        private String theImplClassName;

        public CatalogInfo(String aCatalogGuid, String theImplClassName) {
            theProperties.put(PROPERTY_NAME, theCatalogNames);
            theProperties.put(PROPERTY_DESCRIPTION, theCatalogDescriptions);
            theCatalogGuid = aCatalogGuid;
            this.theImplClassName = theImplClassName;
        }

        public String getImplClass() {
            return theImplClassName;
        }

        public String getName() {
            return theCatalogNames.getProperty(theLocale.toString());
        }

        public String getDescription() {
            return theCatalogDescriptions.getProperty(theLocale.toString());
        }

        public String getGuid() {
            return theCatalogGuid;
        }

        synchronized public Properties getProperties() {
            return theProperties;
        }

        public synchronized Properties getProperties(String name) {
            Properties theResult = new Properties();
            Object aProperty = theProperties.get(name);

            if (aProperty instanceof Properties) {
                theResult = (Properties) aProperty;
            }
             // end of if ()

            return theResult;
        }

        public synchronized String getProperty(String name) {
            String theResult = null;
            Object aProperty = theProperties.get(name);

            if (aProperty instanceof String) {
                theResult = (String) aProperty;
            }
             // end of if ()
            else if (aProperty instanceof Properties) {
                Properties aProp = (Properties) aProperty;
                StringBuffer aBuffer = new StringBuffer();
                Enumeration aEnum = aProp.propertyNames();

                while (aEnum.hasMoreElements()) {
                    aBuffer.append(aProp.getProperty((String) aEnum.nextElement()));

                    if (aEnum.hasMoreElements()) {
                        aBuffer.append(";");
                    }
                     // end of if ()
                }

                theResult = aBuffer.toString();
            }
             // end of if ()

            return theResult;
        }

        synchronized public void setProperty(String name, String key, String value) {
            // name not null and key not null
            //i.e. we set a property on a property
            if ((name != null) && (key != null)) {
                Object aProperty = theProperties.get(name);

                if (aProperty == null) {
                    aProperty = new Properties();
                    theProperties.put(name, aProperty);
                }
                 // end of if ()

                Properties aProp = (Properties) aProperty;
                aProp.setProperty(key, value);
            }

            //name not null and key null
            //i.e. we set a simple property
            else if ((name != null) && (key == null)) {
                theProperties.setProperty(name, value);
            }
             // end of if ()

            return;
        }
    }

    class UpdateStrategyDeferredDelete implements ICatalogUpdateStrategy {
        private Catalog theSourceCatalog;
        private Catalog theTargetCatalog;

        UpdateStrategyDeferredDelete(ICatalog aFromCatalog, ICatalog aToCatalog)
                              throws CatalogException {
            this.theSourceCatalog = (Catalog) aFromCatalog;
            this.theTargetCatalog = (Catalog) aToCatalog;
        }

        /*
         * updates the catalog from sourceCatalog to targetCatalog in the
         * following way
         *  - this  is only feasable with a TRexManagedCatalog as target
         * 1) delete internal structures about this catalog, but do not delete
         * the catalog pysically
         * 2) add the catalog: the created indexes will have "CreatePending"
         * temporary status
         * 3) set the targetCatalog to status "DeletePending"
         */
        public void update()
                    throws CatalogException {
            if (!(theTargetCatalog instanceof TrexManagedCatalog)) {
                throw new CatalogException("update not implemented for " + theTargetCatalog.getClass().getName());
            }

            TrexManagedCatalog targetCatalog = (TrexManagedCatalog) theTargetCatalog;

            // store the indexID of the A-index from the targetCatalog
            // to set the "DeletePending" status for that catalog after adding it
            String theAIndexId = targetCatalog.getIndexID() + "AIndex";

            // delete the targetCatalog internally, but not on the target server
            boolean removeCatalogRemote = false;
            CatalogServerEngine.this.deleteCatalog(theTargetCatalog, true, removeCatalogRemote);

            // create a new target catalog with the contents from source catalog
            CatalogServerEngine.this.addCatalog(theSourceCatalog);

            // set the targetCatalog status to "DeletePending" (on remote server)
            // should be effectively deleted from remote server by some admin thread
            ((TrexCatalogServerEngine) targetCatalog.theServerEngine).storeIndexStatus(theAIndexId, "DeletePending");

            return;
        }
    }

    class SimpleUpdateStrategy implements ICatalogUpdateStrategy {
        private Catalog theFromCatalog;
        private ICatalogInfo theToCatalogInfo;

        SimpleUpdateStrategy(ICatalog aFromCatalog, ICatalog aToCatalog)
                      throws CatalogException {
            this.theFromCatalog = (Catalog) aFromCatalog;
            this.theToCatalogInfo = CatalogServerEngine.this.getCatalogInfo(aToCatalog.getGuid());
        }

        public void update()
                    throws CatalogException {
            //delete the toCatalog
            ICatalog aDoomedCatalog = CatalogServerEngine.this.getCatalog(theToCatalogInfo.getGuid());

            CatalogServerEngine.this.deleteCatalog(aDoomedCatalog, true);

            //add the FromCatalog
            CatalogServerEngine.this.addCatalog(theFromCatalog);

            return;
        }
    }

    class SimpleBackUpStrategy implements ICatalogBackUpStrategy {
        private CatalogInfo theCatalogInfo;
        private Catalog theCatalogToBackUp;

        SimpleBackUpStrategy(CatalogInfo aCatalogInfo)
                      throws CatalogException {
            this.theCatalogInfo = aCatalogInfo;
            theCatalogToBackUp = (Catalog) getCatalogRemote(theCatalogInfo, getActorAsClient());
        }

        public URL backUp()
                   throws CatalogException {
            loadCatalog(theCatalogToBackUp);

            Document theCatalogAsDocument = theCatalogToBackUp.toXML();

            File theServerDir = getServerDir(null);

            if (theServerDir == null) {
                throw new CatalogException("Could create or find backup directory!");
            }
             // end of if ()

            //Make a writer
            StringBuffer aFileNameBuffer = new StringBuffer(theServerDir.getAbsolutePath());
            aFileNameBuffer.append(System.getProperty("file.separator")).append(theCatalogInfo.getGuid())
                           .append("Backup").append(this.getClass().getName()).append(System.currentTimeMillis())
                           .append(".xml");

            File aResultFile = new File(aFileNameBuffer.toString());
            FileOutputStream aFileStream = null;

            try {
                aFileStream = new FileOutputStream(aResultFile);

                OutputStreamWriter aWriter = new OutputStreamWriter(aFileStream, "UTF-8");
                TransformerFactory tFactory = TransformerFactory.newInstance();

                //build your simple copyAll xsl
                StringBuffer anotherBuffer = new StringBuffer("<?xml version=\"1.0\"?>");
                anotherBuffer.append("<xsl:stylesheet xmlns:xsl=\"http://www.w3.org/1999/XSL/Transform\" version=\"1.0\">");
                anotherBuffer.append("<xsl:template match=\"/\">");
                anotherBuffer.append("<xsl:copy-of select=\"*\"/>");
                anotherBuffer.append("</xsl:template>");
                anotherBuffer.append("</xsl:stylesheet>");

                Transformer transformer = tFactory.newTransformer(new StreamSource(new StringReader(anotherBuffer.toString())));
                transformer.transform(new DOMSource(theCatalogAsDocument), new StreamResult(aWriter));
                aWriter.close();
            }
            catch (TransformerConfigurationException tce) {
                throw new CatalogException("Error during backup!", tce);
            }
             // end of catch
            catch (TransformerException te) {
                throw new CatalogException("Error during backup!", te);
            }
             // end of catch
            catch (IOException ioe) {
                throw new CatalogException("Error during backup!", ioe);
            }
             // end of try-catch

            URL theDownLoadURL = null;

            try {
                theDownLoadURL = new URL("file://" + aFileNameBuffer);
            }
            catch (MalformedURLException mue) {
                String theMessage = theMessageResources.getMessage(ICatalogMessagesKeys.PCAT_ERR_BACKUP_CATALOG);

                throw new CatalogException(theMessage, mue);
            }
             // end of try-catch

            return theDownLoadURL;
        }

        private void loadCatalog(Catalog aCatalogToLoad)
                          throws CatalogException {
            aCatalogToLoad.getDetails();

            Iterator aCateIter = aCatalogToLoad.getCategories();

            while (aCateIter.hasNext()) {
                ICategory aCategory = (ICategory) aCateIter.next();
                aCategory.getDetails();
                aCategory.getItems();
            }
             // end of while ()

            return;
        }
    }

    class SimpleLoadCatalogBackUpStrategy implements ICatalogLoadBackUpStrategy {
        URL theBackUp;
        File theLocalFile;
        Document theAttributeMappings;

        SimpleLoadCatalogBackUpStrategy(URL aBackUp, ServerType theFromServerType)
                                 throws CatalogException {
            this.theBackUp = aBackUp;
            theAttributeMappings = getAttributeMapping(theFromServerType);

            if (theAttributeMappings == null) {
                theAttributeMappings = getAttributeMapping("aToBeImportedCatalogGuid");
            }
             // end of if ()
        }

        public void loadBackUp()
                        throws CatalogException {
            File aLocalBackup = transferBackUp(theBackUp);
            InputSource aSource = null;
            Source aMapping = null;

            try {
                aSource = new InputSource(new FileInputStream(aLocalBackup));
                aMapping = ((theAttributeMappings == null) ? null : new DOMSource(theAttributeMappings));
            }
            catch (FileNotFoundException fne) {
                throw new CatalogException("File not found during load of local backup!", fne);
            }
             // end of try-catch

            CatalogServerEngine.this.addCatalogRemote(aSource, aMapping);
            CatalogServerEngine.this.dispatchCatalogCreatedEvent(null);
            //todo get the CatalogInfo of the new catalog
        }

        private File transferBackUp(URL aBackUp)
                             throws CatalogException {
            File aResultFile = null;
            FileOutputStream aLocalOutput = null;

            try {
                InputStream aInputStream = null;

                if (aBackUp.getProtocol().intern() == "file".intern()) {
                    StringBuffer aFileName = new StringBuffer(aBackUp.getAuthority());
                    aFileName.append(aBackUp.getFile());

                    File theBackup = new File(aFileName.toString());
                    aInputStream = new FileInputStream(theBackup);
                }
                else {
                    aInputStream = aBackUp.openStream();
                }
                 // end of else

                File thePathToBackUp = getServerDir(null);

                if (thePathToBackUp == null) {
                    throw new CatalogException("Could not create or find backup directory!");
                }

                // end of if ()
                //Make a writer
                StringBuffer aFileNameBuffer = new StringBuffer(thePathToBackUp.getAbsolutePath());
                aFileNameBuffer.append("/").append(this.getClass().getName()).append("Backup")
                               .append(System.currentTimeMillis()).append(".xml");
                aResultFile = new File(aFileNameBuffer.toString());

                aLocalOutput = new FileOutputStream(aResultFile);

                byte array[] = new byte[1024];
                int read = -1;

                while ((read = aInputStream.read(array)) != -1) {
                    aLocalOutput.write(array, 0, read);
                }
                 // end of while ()

                aLocalOutput.close();
            }
            catch (IOException ioe) {
                throw new CatalogException("Could not write backup file!", ioe);
            }
             // end of try-catch

            return aResultFile;
        }
    }

    class CatInfo implements IClient, IServer, IEnvironment, BackendBusinessObjectParams {
        IServer theServer;
        IClient theClient;
        InitializationEnvironment theInitEnvironment;

        CatInfo(IClient client, IServer server, InitializationEnvironment aEnvironment) {
            theServer = server;
            theClient = client;
            theInitEnvironment = aEnvironment;
        }

        /**
         * IClient Methods
         *
         * @return DOCUMENT ME!
         */
        public Locale getLocale() {
            return theClient.getLocale();
        }

        public String getName() {
            return theClient.getName();
        }

        public String getGuid() {
            return theClient.getName();
        }

        public String getCookie() {
            return null;
        }

        public String getPWD() {
            return theClient.getPWD();
        }

        public ArrayList getViews() {
            return theClient.getViews();
        }

        public Iterator getPermissions() {
            return (new ArrayList()).iterator();
        }
        
        public int getCatalogStatus() { 
            return theClient.getCatalogStatus();
        }
                
        public TechKey getStagingCatalogKey() {
            return theClient.getStagingCatalogKey(); 
        }
        
        public void setCatalogStatus(int catalogState) {
            theClient.setCatalogStatus(catalogState);
        }
        
        public void setStagingCatalogKey(TechKey stagingCatalogKey) {
            theClient.setStagingCatalogKey(stagingCatalogKey);
        }

        /**
         * ISever methods
         *
         * @return DOCUMENT ME!
         */
        public String getURLString() {
            return theServer.getURLString();
        }

        public int getPort() {
            return theServer.getPort();
        }

        public String getCatalogGuid() {
            return theServer.getCatalogGuid();
        }

        public Properties getProperties() {
            return theServer.getProperties();
        }

        /**
         * IEnvironment methods
         *
         * @return DOCUMENT ME!
         */
        public InitializationEnvironment getEnvironment() {
            return theInitEnvironment;
        }
    }
}
 // end of class CatalogServerEngine


/*
 * used for adding a catalog derived from input file
 */
class AddCatalogTask extends CatalogTask {
    // the logging instance
    private static IsaLocation theStaticLocToLog = IsaLocation.getInstance(AddCatalogTask.class.getName());
    private File theCatalogHandle; //Serializable
    private File theMappingHandle; //Serializable

    AddCatalogTask(CatalogServerEngine aTargetEngine, File aCatalogHandle, File aMappingHandle,
                   ICatalogTask.Schedule aSchedule, MessageResources aResource)
            throws CatalogPersistenceException {
        super(aTargetEngine, aSchedule, aResource);
        this.theCatalogHandle = aCatalogHandle;
        this.theMappingHandle = aMappingHandle;
    }

    /**
     * DOCUMENT ME!
     *
     * @return DOCUMENT ME!
     */
    public Action getAction() {
        return Action.ADD;
    }

    /**
     * DOCUMENT ME!
     */
    synchronized public void run() {
        if (theStatus != Status.QUEUED) {
            return;
        }
         // end of if ()

        this.theStatus = ICatalogTask.Status.RUNNING;

        if (theStaticLocToLog.isDebugEnabled()) {
            String msg = theMessageResources.getMessage(ICatalogMessagesKeys.PCAT_START_TASK_ADD_FILE_SRV,
                                                        theCatalogHandle.getAbsolutePath(), theTargetServer.getGuid());
            theStaticLocToLog.debug(msg);
        }
         // end of if ()

        //do your job
        InputSource aInputSource = null;
        Source aSource = null;

        try {
            aInputSource = theTargetServer.loadInputSource(theCatalogHandle);
            aSource = theTargetServer.loadSource(theMappingHandle);
        }
        catch (CatalogException ce) {
            this.theStatus = ICatalogTask.Status.FAILED;
            theStaticLocToLog.error(ICatalogMessagesKeys.PCAT_EXCEPTION, new Object[] { ce.getMessage() }, ce);

            return;
        }
         // end of try-catch

        try {
            theTargetServer.addCatalog(aInputSource, aSource);
        }
        catch (CatalogException ce) {
            this.theStatus = ICatalogTask.Status.FAILED;
            theStaticLocToLog.error(ICatalogMessagesKeys.PCAT_EXCEPTION, new Object[] { ce.getMessage() }, ce);

            return;
        }
         // end of try-catch

        this.theStatus = ICatalogTask.Status.FINISHED;

        if (theStaticLocToLog.isDebugEnabled()) {
            String msg = theMessageResources.getMessage(ICatalogMessagesKeys.PCAT_END_TASK_ADD_INST_SRV,
                                                        theCatalogHandle.getAbsolutePath(), theTargetServer.getGuid());
            theStaticLocToLog.debug(msg);
        }
         // end of if ()

        return;
    }
}


class DeleteCatalogInstanceTask extends CatalogTask {
    // the logging instance
    private static IsaLocation theStaticLocToLog = IsaLocation.getInstance(DeleteCatalogInstanceTask.class.getName());
    transient private ICatalog theTargetCatalogHandle; //Serializable
    private String theTargetServerGuid;
    private String theTargetCatalogGuid;
    private boolean includeAllCachedImages;

    DeleteCatalogInstanceTask(CatalogServerEngine aTargetEngine, ICatalog aTargetCatalog,
                              boolean includeAllCachedImages, ICatalogTask.Schedule aSchedule,
                              MessageResources aResource)
                       throws CatalogPersistenceException {
        super(aTargetEngine, aSchedule, aResource);
        this.theTargetCatalogHandle = aTargetCatalog;
        this.theTargetServerGuid = aTargetCatalog.getServerGuid();
        this.theTargetCatalogGuid = aTargetCatalog.getGuid();
        this.includeAllCachedImages = includeAllCachedImages;
    }

    /**
     * DOCUMENT ME!
     *
     * @return DOCUMENT ME!
     */
    public Action getAction() {
        return Action.DELETE;
    }

    /**
     * DOCUMENT ME!
     *
     * @return DOCUMENT ME!
     */
    public String getTargetCatalogGuid() {
        return theTargetCatalogGuid;
    }

    /**
     * DOCUMENT ME!
     */
    synchronized public void run() {
        //pull the catalog to be deleted out of your bowls
        if (theTargetCatalogHandle == null) {
            try {
                theTargetCatalogHandle = theTargetServer.getCatalog(theTargetCatalogGuid);
            }
            catch (CatalogException ce) {
                this.theStatus = ICatalogTask.Status.FAILED;
                theStaticLocToLog.error(ICatalogMessagesKeys.PCAT_ERR_OBJ_NOT_OWNED_THIS);

                try {
                    serialize();
                }
                catch (CatalogPersistenceException pe) {
                    theStaticLocToLog.error(ICatalogMessagesKeys.PCAT_EXCEPTION, new Object[] { pe.getMessage() }, pe);
                }
                 // end of try-catch

                return;
            }
             // end of try-catch

            if (theTargetCatalogHandle == null) {
                this.theStatus = ICatalogTask.Status.FAILED;
                theStaticLocToLog.error(ICatalogMessagesKeys.PCAT_ERR_OBJ_NOT_OWNED_THIS);

                try {
                    serialize();
                }
                catch (CatalogPersistenceException pe) {
                    theStaticLocToLog.error(ICatalogMessagesKeys.PCAT_EXCEPTION, new Object[] { pe.getMessage() }, pe);
                }
                 // end of try-catch

                return;
            }
             // end of if ()
        }
         // end of if ()

        if (theStatus != Status.QUEUED) {
            return;
        }
         // end of if ()

        this.theStatus = ICatalogTask.Status.RUNNING;

        if (theStaticLocToLog.isDebugEnabled()) {
            String msg = theMessageResources.getMessage(ICatalogMessagesKeys.PCAT_START_TASK_DEL_INST_SRV,
                                                        theTargetCatalogHandle.getGuid(), theTargetServer.getGuid());
            theStaticLocToLog.debug(msg);
        }
         // end of if ()

        //do your job
        try {
            theTargetServer.deleteCatalog(theTargetCatalogHandle, includeAllCachedImages);
        }
        catch (CatalogException ce) {
            this.theStatus = ICatalogTask.Status.FAILED;
            theStaticLocToLog.error(ICatalogMessagesKeys.PCAT_EXCEPTION, new Object[] { ce.getMessage() }, ce);

            try {
                serialize();
            }
            catch (CatalogPersistenceException pe) {
                theStaticLocToLog.error(ICatalogMessagesKeys.PCAT_EXCEPTION, new Object[] { pe.getMessage() }, pe);
            }
             // end of try-catch

            return;
        }
         // end of try-catch

        this.theStatus = ICatalogTask.Status.FINISHED;

        try {
            serialize();
        }
        catch (CatalogPersistenceException pe) {
            theStaticLocToLog.error(ICatalogMessagesKeys.PCAT_EXCEPTION, new Object[] { pe.getMessage() }, pe);
        }
         // end of try-catch

        if (theStaticLocToLog.isDebugEnabled()) {
            String msg = theMessageResources.getMessage(ICatalogMessagesKeys.PCAT_END_TASK_DEL_INST_SRV,
                                                        theTargetCatalogHandle.getGuid(), theTargetServer.getGuid());
            theStaticLocToLog.debug(msg);
        }
         // end of if ()

        return;
    }
}


class AddCatalogInstanceTask extends CatalogTask {
    // the logging instance
    private static IsaLocation theStaticLocToLog = IsaLocation.getInstance(AddCatalogInstanceTask.class.getName());
    transient private ICatalog theSourceCatalogHandle; //Serializable
    private String theSourceServerGuid;
    private String theSourceCatalogGuid;

    AddCatalogInstanceTask(CatalogServerEngine aTargetEngine, ICatalog aSourceCatalog, ICatalogTask.Schedule aSchedule,
                           MessageResources aResource)
                    throws CatalogPersistenceException {
        super(aTargetEngine, aSchedule, aResource);
        this.theSourceCatalogHandle = aSourceCatalog;
        this.theSourceServerGuid = aSourceCatalog.getServerGuid();
        this.theSourceCatalogGuid = aSourceCatalog.getGuid();
    }

    /**
     * DOCUMENT ME!
     *
     * @return DOCUMENT ME!
     */
    public Action getAction() {
        return Action.ADD;
    }

    /**
     * DOCUMENT ME!
     *
     * @return DOCUMENT ME!
     */
    public String getSourceCatalogGuid() {
        return theSourceCatalogGuid;
    }

    /**
     * DOCUMENT ME!
     *
     * @return DOCUMENT ME!
     */
    public String getSourceServerGuid() {
        return theSourceServerGuid;
    }

    /**
     * DOCUMENT ME!
     */
    synchronized public void run() {
        //pull the catalog to be deleted out of your bowls
        if (theSourceCatalogHandle == null) {
            try {
                theSourceCatalogHandle = theTargetServer.getSite().getCatalogServer(theSourceServerGuid).getCatalog(theSourceCatalogGuid);
            }
            catch (CatalogException ce) {
                this.theStatus = ICatalogTask.Status.FAILED;
                theStaticLocToLog.error(ICatalogMessagesKeys.PCAT_ERR_OBJ_NOT_OWNED_THIS);

                try {
                    serialize();
                }
                catch (CatalogPersistenceException pe) {
                    theStaticLocToLog.error(ICatalogMessagesKeys.PCAT_EXCEPTION, new Object[] { pe.getMessage() }, pe);
                }
                 // end of try-catch

                return;
            }
             // end of try-catch

            if (theSourceCatalogHandle == null) {
                this.theStatus = ICatalogTask.Status.FAILED;
                theStaticLocToLog.error(ICatalogMessagesKeys.PCAT_ERR_OBJ_NOT_OWNED_THIS);

                try {
                    serialize();
                }
                catch (CatalogPersistenceException pe) {
                    theStaticLocToLog.error(ICatalogMessagesKeys.PCAT_EXCEPTION, new Object[] { pe.getMessage() }, pe);
                }
                 // end of try-catch

                return;
            }
             // end of if ()
        }
         // end of if ()

        if (theStatus != Status.QUEUED) {
            return;
        }
         // end of if ()

        this.theStatus = ICatalogTask.Status.RUNNING;

        if (theStaticLocToLog.isDebugEnabled()) {
            String msg = theMessageResources.getMessage(ICatalogMessagesKeys.PCAT_START_TASK_ADD_INST_SRV,
                                                        theSourceCatalogHandle.getGuid(), theTargetServer.getGuid());
            theStaticLocToLog.debug(msg);
        }
         // end of if ()

        //do your job
        try {
            theTargetServer.addCatalog(theSourceCatalogHandle);
        }
        catch (CatalogException ce) {
            this.theStatus = ICatalogTask.Status.FAILED;
            theStaticLocToLog.error(ICatalogMessagesKeys.PCAT_EXCEPTION, new Object[] { ce.getMessage() }, ce);

            return;
        }
         // end of try-catch

        this.theStatus = ICatalogTask.Status.FINISHED;

        if (theStaticLocToLog.isDebugEnabled()) {
            String msg = theMessageResources.getMessage(ICatalogMessagesKeys.PCAT_START_TASK_ADD_INST_SRV,
                                                        theSourceCatalogHandle.getGuid(), theTargetServer.getGuid());
            theStaticLocToLog.debug(msg);
        }
         // end of if ()

        return;
    }
}


class UpdateCatalogInstanceTask extends CatalogTask {
    // the logging instance
    private static IsaLocation theStaticLocToLog = IsaLocation.getInstance(UpdateCatalogInstanceTask.class.getName());
    transient private ICatalog theSourceCatalogHandle; //Serializable
    private String theSourceCatalogGuid;
    private String theSourceServerGuid;
    private String theTargetCatalogGuid;

    UpdateCatalogInstanceTask(CatalogServerEngine aTargetEngine, ICatalog aFromCatalog, ICatalogInfo aToCatalogInfo,
                              ICatalogTask.Schedule aSchedule, MessageResources aResource)
                       throws CatalogPersistenceException {
        super(aTargetEngine, aSchedule, aResource);
        this.theSourceCatalogHandle = aFromCatalog;
        this.theSourceCatalogGuid = aFromCatalog.getGuid();
        this.theSourceServerGuid = aFromCatalog.getServerGuid();
        this.theTargetCatalogGuid = aToCatalogInfo.getGuid();
    }

    /**
     * DOCUMENT ME!
     *
     * @return DOCUMENT ME!
     */
    public Action getAction() {
        return Action.UPDATE;
    }

    /**
     * DOCUMENT ME!
     *
     * @return DOCUMENT ME!
     */
    public String getSourceCatalogGuid() {
        return theSourceCatalogGuid;
    }

    /**
     * DOCUMENT ME!
     *
     * @return DOCUMENT ME!
     */
    public String getSourceServerGuid() {
        return theSourceServerGuid;
    }

    /**
     * DOCUMENT ME!
     *
     * @return DOCUMENT ME!
     */
    public String getTargetCatalogGuid() {
        return theTargetCatalogGuid;
    }

    /**
     * DOCUMENT ME!
     */
    synchronized public void run() {
        //pull the catalog to be deleted out of your bowls
        if (theSourceCatalogHandle == null) {
            try {
                theSourceCatalogHandle = theTargetServer.getSite().getCatalogServer(theSourceServerGuid).getCatalog(theSourceCatalogGuid);
            }
            catch (CatalogException ce) {
                this.theStatus = ICatalogTask.Status.FAILED;
                theStaticLocToLog.error(ICatalogMessagesKeys.PCAT_ERR_OBJ_NOT_OWNED_THIS);

                try {
                    serialize();
                }
                catch (CatalogPersistenceException pe) {
                    theStaticLocToLog.error(ICatalogMessagesKeys.PCAT_EXCEPTION, new Object[] { pe.getMessage() }, pe);
                }
                 // end of try-catch

                return;
            }
             // end of try-catch

            if (theSourceCatalogHandle == null) {
                this.theStatus = ICatalogTask.Status.FAILED;
                theStaticLocToLog.error(ICatalogMessagesKeys.PCAT_ERR_OBJ_NOT_OWNED_THIS);

                try {
                    serialize();
                }
                catch (CatalogPersistenceException pe) {
                    theStaticLocToLog.error(ICatalogMessagesKeys.PCAT_EXCEPTION, new Object[] { pe.getMessage() }, pe);
                }
                 // end of try-catch

                return;
            }
             // end of if ()
        }
         // end of if ()

        if ((theStatus != Status.QUEUED) && (theStatus != Status.RUNNING)) {
            return;
        }
         // end of if ()

        this.theStatus = ICatalogTask.Status.RUNNING;

        //check if we are done
        Date now = new Date();

        if (this.theSchedule.getEnd().compareTo(now) < 0) {
            this.theStatus = ICatalogTask.Status.FINISHED;
            theTimer.cancel();

            return;
        }
         // end of if ()

        if (theStaticLocToLog.isDebugEnabled()) {
            String msg = theMessageResources.getMessage(ICatalogMessagesKeys.PCAT_START_TASK_UPD_INST_SRV,
                                                        theSourceCatalogHandle.getGuid(), theTargetCatalogGuid,
                                                        theTargetServer.getGuid());
            theStaticLocToLog.debug(msg);
        }
         // end of if ()

        //do your job
        try {
            ICatalog aCatToUpdate = theTargetServer.getCatalog(theTargetCatalogGuid);

            if (aCatToUpdate == null) {
                this.theStatus = ICatalogTask.Status.FAILED;
                theStaticLocToLog.error(ICatalogMessagesKeys.PCAT_ERR_OBJ_NOT_OWNED_THIS);

                return;
            }
             // end of if ()

            theTargetServer.updateCatalog(theSourceCatalogHandle, aCatToUpdate);
        }
        catch (CatalogException ce) {
            this.theStatus = ICatalogTask.Status.FAILED;
            theStaticLocToLog.error(ICatalogMessagesKeys.PCAT_EXCEPTION, new Object[] { ce.getMessage() }, ce);

            return;
        }
         // end of try-catch

        if (theStaticLocToLog.isDebugEnabled()) {
            String msg = theMessageResources.getMessage(ICatalogMessagesKeys.PCAT_END_TASK_UPD_INST_SRV,
                                                        theSourceCatalogHandle.getGuid(), theTargetCatalogGuid,
                                                        theTargetServer.getGuid());
            theStaticLocToLog.debug(msg);
        }
         // end of if ()

        return;
    }
}


/**
 * Describe class <code>CatalogTask</code> here.
 *
 * @version 1.0
 */
abstract class CatalogTask extends TimerTask implements ICatalogTask {
    // the logging instance
    private static IsaLocation theStaticLocToLog = IsaLocation.getInstance(CatalogTask.class.getName());
    private final static HashMap theHandles = new HashMap();
    transient protected CatalogServerEngine theTargetServer; // mostly myself !!!
    transient protected Timer theTimer;
    protected MessageResources theMessageResources;
    protected File theHandle;
    protected ICatalogTask.Schedule theSchedule; //Serializable
    protected ICatalogTask.Status theStatus; //Serializable

    CatalogTask(CatalogServerEngine aTargetEngine, ICatalogTask.Schedule aSchedule, MessageResources aResource)
         throws CatalogPersistenceException {
        this.theTargetServer = aTargetEngine;
        this.theSchedule = aSchedule;
        this.theStatus = Status.CREATED;
        this.theMessageResources = aResource;
        //
        initFileHandle();
        //burn it on to the disc
        serialize();

        //add it to the handle list
        synchronized (theHandles) {
            theHandles.put(theHandle.getAbsolutePath(), this);
        }
    }

    /**
     * Central factory method to get the tasks running against a server instance.
     *
     * @param theServer a <code>CatalogServerEngine</code> value
     *
     * @return an <code>Iterator</code> value
     *
     * @exception CatalogException if an error occurs
     * @throws CatalogPersistenceException DOCUMENT ME!
     */
    static Iterator getTasks(CatalogServerEngine theServer)
                      throws CatalogException {
        ArrayList theResult = new ArrayList();

        //lockup the tasks deserialized so far for the given server
        synchronized (theHandles) {
            File theServerDir = theServer.getServerDir(System.getProperty("file.separator") + "tasks");
            Set theKeys = theHandles.keySet();
            Iterator anIter = theKeys.iterator();

            while (anIter.hasNext()) {
                String aKey = (String) anIter.next();

                if (aKey.startsWith(theServerDir.getAbsolutePath())) {
                    theResult.add(theHandles.get(aKey));
                }
                 // end of if ()
            }
             // end of while ()

            //nothing initialized so for deserialize the stuff for this server
            if (theResult.size() == 0) {
                File allTasksOntheServer[] = theServerDir.listFiles();
                int count = allTasksOntheServer.length;

                for (int i = 0; i < count; i++) {
                    try {
                        FileInputStream aInputStream = new FileInputStream(allTasksOntheServer[i]);
                        ObjectInputStream theInStream = new ObjectInputStream(aInputStream);
                        Object theResurreced = theInStream.readObject();
                        theInStream.close();

                        CatalogTask theResurrecedTask = (CatalogTask) theResurreced;
                        theResurrecedTask.setParent(theServer);

                        //check if the task became stale
                        if (theResurrecedTask.isStale()) {
                            allTasksOntheServer[i].delete();
                        }
                         // end of if ()
                        else //if not schedule it now
                         {
                            theResult.add(theResurrecedTask);
                            theResurrecedTask.schedule();
                        }
                         // end of else
                    }
                    catch (Exception e) {
                        String msg = theServer.getMessageResources().getMessage(ICatalogMessagesKeys.PCAT_ERR_READING_PERSISTENT_OBJ);
                        throw new CatalogPersistenceException(msg, e);
                    }
                     // end of catch
                }
                 // end of for ()
            }
             // end of if ()
        }
         //synchronized

        return theResult.iterator();
    }

    private final void initFileHandle()
                               throws CatalogPersistenceException {
        File aDirToSaveTheTask = theTargetServer.getServerDir(System.getProperty("file.separator") + "tasks");

        if ((aDirToSaveTheTask == null) || (!aDirToSaveTheTask.exists() && !aDirToSaveTheTask.mkdirs())) {
            String msg = theTargetServer.getEnvironment().getMessageResources().getMessage(ICatalogMessagesKeys.PCAT_ERR_FIND_CREATE_DIR,
                                                                                           "tasks");
            throw new CatalogPersistenceException(msg);
        }
         // end of if ()

        StringBuffer aFileToSaveTheTaskBuffer = new StringBuffer(aDirToSaveTheTask.getAbsolutePath());
        aFileToSaveTheTaskBuffer.append(System.getProperty("file.separator"));
        aFileToSaveTheTaskBuffer.append("Task");
        aFileToSaveTheTaskBuffer.append(System.currentTimeMillis());
        aFileToSaveTheTaskBuffer.append(".ser");
        this.theHandle = new File(aFileToSaveTheTaskBuffer.toString());

        return;
    }

    /**
     * DOCUMENT ME!
     *
     * @return DOCUMENT ME!
     */
    public IActor getOwner() {
        return theTargetServer.getActor();
    }

    /**
     * DOCUMENT ME!
     *
     * @return DOCUMENT ME!
     */
    abstract public Action getAction();

    /**
     * DOCUMENT ME!
     *
     * @return DOCUMENT ME!
     */
    public Schedule getSchedule() {
        return theSchedule;
    }

    /**
     * DOCUMENT ME!
     *
     * @return DOCUMENT ME!
     */
    synchronized public Status getStatus() {
        return theStatus;
    }

    /**
     * DOCUMENT ME!
     *
     * @return DOCUMENT ME!
     */
    public String getSourceServerGuid() {
        return "n/a"; // has to implemented somewhere else
    }

    /**
     * DOCUMENT ME!
     *
     * @return DOCUMENT ME!
     */
    public String getSourceCatalogGuid() {
        return "n/a"; // has to implemented somewhere else
    }

    /**
     * DOCUMENT ME!
     *
     * @return DOCUMENT ME!
     */
    public String getTargetCatalogGuid() {
        return theTargetServer.getCatalogGuid();
    }

    /**
     * DOCUMENT ME!
     *
     * @return DOCUMENT ME!
     */
    final public String getTargetServerGuid() {
        return theTargetServer.getGuid();
    }

    /**
     * DOCUMENT ME!
     *
     * @throws CatalogException DOCUMENT ME!
     */
    synchronized public void schedule()
                               throws CatalogException {
        if (theTimer == null && !isStale()) {
            this.theTimer = new Timer(true);

            if (theSchedule.isPeriodic()) {
                theTimer.schedule(this, theSchedule.getStart(), theSchedule.getPeriod());
            }
             // end of if ()
            else {
                theTimer.schedule(this, theSchedule.getStart());
            }
             // end of else

            this.theStatus = Status.QUEUED;
            //commit the change
            serialize();
        }
         // end of if ()

        return;
    }

    /**
     * DOCUMENT ME!
     *
     * @param theOwner DOCUMENT ME!
     *
     * @throws CatalogException DOCUMENT ME!
     */
    synchronized public void kill(IActor theOwner)
                           throws CatalogException {
        //unused: Date now = new Date();
        if ((this.theStatus == Status.CREATED) || (this.theStatus == Status.QUEUED)) {
            if (theTimer != null) {
                theTimer.cancel();
            }
             // end of if ()

            this.theStatus = Status.KILLED;
            //commit the change
            serialize();
        }

        return;
    }

    boolean isStale() {
        boolean result = false;
        Date now = new Date();

        if ((this.getSchedule().getEnd().compareTo(now) <= 0) ||
                (theStatus == Status.KILLED) ||
                (theStatus == Status.FINISHED) ||
                (theStatus == Status.FAILED)) {
            result = true;
        }
         // end of if ()

        return result;
    }

    /**
     * To be called by the deserializing instance.
     *
     * @param aParent a <code>CatalogServerEngine</code> value
     */
    synchronized void setParent(CatalogServerEngine aParent) {
        if (this.theTargetServer == null) {
            this.theTargetServer = aParent;
        }
         // end of if ()

        return;
    }

    /**
     * DOCUMENT ME!
     *
     * @throws CatalogPersistenceException DOCUMENT ME!
     */
    final protected synchronized void serialize()
                                         throws CatalogPersistenceException {
        if (theHandle != null) {
            try {
                FileOutputStream aOutputStream = new FileOutputStream(theHandle);
                ObjectOutputStream theOutStream = new ObjectOutputStream(aOutputStream);
                theOutStream.writeObject(this);
                theOutStream.close();
            }
            catch (Exception e) {
                String msg = theMessageResources.getMessage(ICatalogMessagesKeys.PCAT_ERR_PERSISTING_OBJ);
                throw new CatalogPersistenceException(msg, e);
            }
             // end of catch
        }
         // end of if ()
    }

    /**
     * DOCUMENT ME!
     */
    abstract public void run();
}