/*****************************************************************************
  Copyright (c) 2000, SAPMarkets Europe GmbH, Germany, All rights reserved.
  Created:      09 July 2001

  $Id: //sap/ESALES_base/30_SP_COR/src/_pcatAPI/java/com/sap/isa/catalog/impl/CatalogCompositeComponent.java#2 $
  $Revision: #2 $
  $Change: 127411 $
  $DateTime: 2003/04/30 17:20:32 $
*****************************************************************************/
package com.sap.isa.catalog.impl;

// misc imports
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.lang.ref.Reference;
import java.lang.ref.ReferenceQueue;
import java.lang.ref.SoftReference;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Stack;

import org.apache.struts.util.MessageResources;

import com.sap.isa.catalog.ICatalogMessagesKeys;
import com.sap.isa.catalog.boi.CatalogException;
import com.sap.isa.catalog.boi.ICompositeEventListener;
import com.sap.isa.catalog.boi.IItem;
import com.sap.isa.catalog.boi.IQuantity;
import com.sap.isa.catalog.boi.IQuery;
import com.sap.isa.catalog.boi.IQueryStatement;
import com.sap.isa.core.logging.IsaLocation;

/**
 *  Base class of all composite components in the catalog tree i.e. catalog and
 *  categories.
 *
 *  @version     1.0
 */
public abstract class CatalogCompositeComponent
    extends CatalogHierarchicalComponent
{
    public static final int MAX_NUMBER_OF_ITEM_LEAFS = 10000;
    
    private static IsaLocation log =
               IsaLocation.getInstance(CatalogCompositeComponent.class.getName());
    
    /*
     *  CatalogHierarchicalComponent implements Serializable:
     *  Fields:
     *  theCompositeEventListener   :   not our problem should better be
                                        serializable, the container is
     *  theInnerNodes               :   the Container implements Serializable!
     *                                  The parts implement Serializable them self!
     *  theLeafNodes                :   the Container implements Serializable!
     *                                  The parts implement Serializable them self!
     *  theAttributes               :   the Container implements Serializable!
     *                                  The parts implement Serializable them self!
     *  theQueries                  :   transient
     *                                  The parts will be stored/resurrected in the
     *                                  writeObject/readObject method!
     *  theSerializableQueries      :   the Container implements Serializable!
     *                                  The parts implement Serializable!
     *  theRefQueue                 :   transient. Will be resurrected in the readObject
     *                                  method!
     */

    protected ArrayList theCompositeEventListener   = null;
    protected ArrayList theInnerNodes               = new ArrayList(0);
    protected ArrayList theLeafNodes                = new ArrayList(0);
    protected ArrayList theAttributes               = new ArrayList(0);

    protected transient Map          theQueries  = new HashMap(0);
    private Map                      theSerializableQueries  = new HashMap(0);
    private transient ReferenceQueue theRefQueue = new ReferenceQueue();

    protected int theMaxNumberOfItemLeafs = MAX_NUMBER_OF_ITEM_LEAFS;
    protected int currentAdministeredProductIndex = 0;
    public static final String PROPERTY_MAX_NUMBER_OF_ITEM_LEAFS = "maxNumCateItems";
    
    // the logging instance
    private static IsaLocation theStaticLocToLog =
        IsaLocation.getInstance(CatalogCompositeComponent.class.getName());
    private String theSerializableConvienceLock =
        new String("theSerializableConvienceLock");

    
    /**
     * Returns the list of children of the actual node.
     *
     * @return list of children
     */
    public List getChildComponents()
    {
        // for performance reasons two separate lists are hold
        List result = new ArrayList(theInnerNodes);
        result.addAll(theLeafNodes);
        return result;
    }

    public boolean addChildComponent(CatalogHierarchicalComponent component)
    {
        boolean theResult = false;
        if(component.queryInterface(CatalogComponentTypes.COMPOSITE_COMPONENT) != null)
        {
            theResult=true;
                //inherite the theMaxNumberOfItems from your parent
            CatalogCompositeComponent aChild = (CatalogCompositeComponent)component;
            aChild.theMaxNumberOfItemLeafs=this.theMaxNumberOfItemLeafs;
            theInnerNodes.ensureCapacity(10);         
            theInnerNodes.add(component);
            if (log.isDebugEnabled()) {
                log.debug("Size of inner node = " + theInnerNodes.size());
            }
        }
        else 
        {
            if (theLeafNodes.size() < theMaxNumberOfItemLeafs ) 
            {   
                theLeafNodes.ensureCapacity(10);
                theResult = theLeafNodes.add(component);  
                if (log.isDebugEnabled()) {
                    log.debug("Size of leaf nodes = " + theLeafNodes.size());
                }                          
            } // end of if ()
        } // end of else
        
        component.setParent(this);

        if (theResult) 
        {
            this.dispatchChildCreatedEvent(component);            
        } // end of if ()
        return theResult;
    }

    public void deleteChildComponent(CatalogHierarchicalComponent component)
    {
        if(component.queryInterface(CatalogComponentTypes.COMPOSITE_COMPONENT) != null)
            theInnerNodes.remove(component);
        else
            theLeafNodes.remove(component);

        component.setParent(null);

        this.dispatchChildDeletedEvent(component);
    }

    public Iterator iterator()
    {
        return new ChildrenIterator(this);
    }

    /**
     * Returns the global unique identifier of the composite component.
     *
     * @return a GUID of the component
     */
    public abstract String getGuid();

    //
    // methods for handling the categories
    //
    public synchronized Iterator getChildCategories()
        throws CatalogException
    {
        if(isDeleted())
        {
            String theMessage = getRoot().getMessageResources().getMessage(
                ICatalogMessagesKeys.PCAT_ERR_IS_DELEDED);
            throw new CatalogException(theMessage);
        }

        Catalog root = getRoot();
        CatalogBuilder builder = root.getCatalogBuilder();
        root.setTimeStamp();
        try
        {
            if(!builder.isCategoriesBuilt(this))
              builder.buildCategories(this);
        }
        catch(CatalogBuildFailedException ex)
        {
            throw new CatalogException(ex.getLocalizedMessage());
        }

        return Collections.unmodifiableList(theInnerNodes).iterator();
    }

   /**
    * Returns the categories if already built. If the categories weren't built yet
    * an empty list will be returned. The Categories that were marked as deleted
    * will be also returned!
    *
    * @return a list of categories
    */
    public synchronized Iterator getChildCategoriesInternal()
    {
        return theInnerNodes.iterator();
    }

    public synchronized Iterator getChildCategories(Comparator aComparator)
        throws CatalogException
    {
        if(isDeleted())
        {
            String theMessage = getRoot().getMessageResources().getMessage(
                ICatalogMessagesKeys.PCAT_ERR_IS_DELEDED);
            throw new CatalogException(theMessage);
        }

        Catalog root = getRoot();
        CatalogBuilder builder = root.getCatalogBuilder();
        root.setTimeStamp();
        try
        {
            if(!builder.isCategoriesBuilt(this))
              builder.buildCategories(this);
        }
        catch(CatalogBuildFailedException ex)
        {
            throw new CatalogException(ex.getLocalizedMessage());
        }

        ArrayList categories = (ArrayList)theInnerNodes.clone();
        Collections.sort(categories, aComparator);
        return categories.iterator();
    }

    /**
     * Sorts the list of categories by name.
     */
     public void sortChildrenCategories()
     {
         Collections.sort(theInnerNodes);
     }

    public CatalogCategory getChildCategory(String categoryGuid)
        throws CatalogException
    {
        if(isDeleted())
        {
            String theMessage = getRoot().getMessageResources().getMessage(
                ICatalogMessagesKeys.PCAT_ERR_IS_DELEDED);
            throw new CatalogException(theMessage);
        }

        getChildCategories();

        CatalogCategory category = null;

        Iterator iter = theInnerNodes.iterator();
        while(iter.hasNext())
        {
            category = (CatalogCategory)iter.next();
            if(category.getGuid().equals(categoryGuid))
                break;
            category = null;
        }

        return category;
    }

    /**
     * Factory method for creating a category instance. Internal method which
     * can't be called via the public interface.
     *
     * @param aCategoryGuid the GUID of the category to be created
     * @return a instance of type <code>CatalogCategory</code>
     */
    public CatalogCategory createChildCategoryInternal(String aCategoryGuid)
    {
        CatalogCategory child = getRoot().getCategoryInternal(aCategoryGuid);
        if(child == null)
        {
            child = new CatalogCategory(this, aCategoryGuid);
            getRoot().addCategory(child);
        }

        addChildCategoryInternal(child);

        return child;
    }

    /**
     * Sets the children categories.
     *
     * @param a list of children categories
     */
    public void setChildCategories(List children)
    {
        // delete the old ones
        theInnerNodes.clear();
        // set the new ones
        Iterator iter = children.iterator();
        while(iter.hasNext())
        {
            addChildCategoryInternal((CatalogCategory)iter.next());
        }
    }

    /**
     * Adds a category to the list of categories.
     *
     * @param a category instance
     */
    public void addChildCategoryInternal(CatalogCategory category)
    {
        this.addChildCategoryInternal(category,false);
    }

    /**
     * Adds a category to the list of categories.
     *
     * @param a category instance
     */
    public void addChildCategoryInternal(CatalogCategory category, boolean dispatch)
    {
            //inherite the theMaxnumberOfItems from your parent
        category.theMaxNumberOfItemLeafs=this.theMaxNumberOfItemLeafs;
        theInnerNodes.ensureCapacity(10);
        theInnerNodes.add(category);
        if (log.isDebugEnabled()) {
            log.debug("Size of inner node = " + theInnerNodes.size());
        }
        if(dispatch)
            this.dispatchChildCreatedEvent(category);
    }

    /**
     * Removes the given category instance from the list of inner nodes.
     * Internal method which can't be called via the public interface.
     *
     * @param category the category to be removed
     */
    public void deleteChildCategoryInternal(CatalogCategory category)
    {
        theInnerNodes.remove(category);
        this.dispatchChildDeletedEvent(category);
    }

    //
    // methods for handling the items
    //
    public synchronized Iterator getItems() throws CatalogException
    {
        if(isDeleted())
        {
            String theMessage = getRoot().getMessageResources().getMessage(
                ICatalogMessagesKeys.PCAT_ERR_IS_DELEDED);
            throw new CatalogException(theMessage);
        }

        Catalog root = getRoot();
        root.setTimeStamp();
        CatalogBuilder builder = root.getCatalogBuilder();
        try
        {
            if(!builder.isItemsBuilt(this))
              builder.buildItems(this);
        }
        catch(CatalogBuildFailedException ex)
        {
            throw new CatalogException(ex.getLocalizedMessage());
        }

        return Collections.unmodifiableList(theLeafNodes).iterator();
    }

   /**
    * Returns the items if already built. If the items weren't built yet
    * an empty list will be returned. The items that were marked as deleted
    * will also be returned.
    *
    * @return a list of items
    */
    public synchronized Iterator getItemsInternal()
    {
        return theLeafNodes.iterator();
    }

    /**
     * Adds an item instance without event dispatching.
     *
     * @param item the item to be added
     * @return the success
     */
    public boolean addItemInternal(CatalogItem item)
    {
        return this.addItemInternal(item,false);
    }

    /**
     * Adds an item instance.
     * If the maximum number of item leafs is exceeded the item is not added.
     * That mechanism will protect you from memory shortage and some extend
     * from performance hits due to unreasonable catalog layouts/search requests.
     * A truly protection from performance hits due to these problems can only be
     * provide by the corresponding catalog engine.
     *
     * @param item the item to be added
     * @param dispatch inform your listeners
     * @return the success 
     */
    public boolean addItemInternal(CatalogItem item, boolean dispatch)
    {
        boolean theResult = false;
        if (theLeafNodes.size() < this.theMaxNumberOfItemLeafs) 
        {   
            theLeafNodes.ensureCapacity(10);
            theResult = theLeafNodes.add(item); 
            if (log.isDebugEnabled()) {
                log.debug("Size of leaf nodes = " + theLeafNodes.size());
            }            
        } // end of if ()
        if(theResult && dispatch)
            this.dispatchChildCreatedEvent(item);
        return theResult;
    }

    /**
     * Removes an item instance.
     *
     * @param item the item to be removed
     */
    public void deleteItemInternal(CatalogItem item)
    {
        theLeafNodes.remove(item);

        this.dispatchChildDeletedEvent(item);
    }


    public void swapItemsInternal(CatalogItem item){
    	int currentIndex = theLeafNodes.size()-1;
    	CatalogItem temp = null;
    	if(currentIndex < this.theMaxNumberOfItemLeafs && currentIndex != 0
    		&& currentIndex != currentAdministeredProductIndex)
    	{
    		temp = (CatalogItem)theLeafNodes.get(currentAdministeredProductIndex);
    		theLeafNodes.set(currentAdministeredProductIndex,item);
    		theLeafNodes.set(currentIndex,temp);
    	}    	
		currentAdministeredProductIndex++;
    }
    
    public synchronized Iterator getItems(Comparator aComparator)
        throws CatalogException
    {
        if(isDeleted())
        {
            String theMessage = getRoot().getMessageResources().getMessage(
                ICatalogMessagesKeys.PCAT_ERR_IS_DELEDED);
            throw new CatalogException(theMessage);
        }

        Catalog root = getRoot();
        CatalogBuilder builder = root.getCatalogBuilder();
        root.setTimeStamp();
        try
        {
            if(!builder.isItemsBuilt(this))
              builder.buildItems(this);
        }
        catch(CatalogBuildFailedException ex)
        {
            throw new CatalogException(ex.getLocalizedMessage());
        }

        ArrayList nodes = (ArrayList)theLeafNodes.clone();
        Collections.sort(nodes, aComparator);
        return nodes.iterator();
    }

   /**
    * Sorts the list of items by name.
    */
    public void sortItems()
    {
         Collections.sort(theLeafNodes);
    }

    public synchronized IItem getItem(String itemGuid) throws CatalogException
    {
        if(isDeleted())
        {
            String theMessage = getRoot().getMessageResources().getMessage(
                ICatalogMessagesKeys.PCAT_ERR_IS_DELEDED);
            throw new CatalogException(theMessage);
        }

        Catalog root = getRoot();
        CatalogBuilder builder = root.getCatalogBuilder();
        root.setTimeStamp();
        try
        {
            if(!builder.isItemsBuilt(this))
              builder.buildItems(this);
        }
        catch(CatalogBuildFailedException ex)
        {
            throw new CatalogException(ex.getLocalizedMessage());
        }

        CatalogItem item = null;

        Iterator iter = theLeafNodes.iterator();
        while(iter.hasNext())
        {
            item = (CatalogItem)iter.next();
            if(item.getGuid().equals(itemGuid))
                break;
            item = null;
        }
        return item;
    }

    /**
     * Factory method for creating an item instance. Internal method which
     * can't be called via the public interface.
     *
     * @see #addItem(CatalogItem)
     * @see #addItem(CatalogItem,boolean) 
     * 
     * @return a instance of type <code>CatalogItem</code>
     */
    public CatalogItem createItemInternal()
    {
        CatalogItem aItem = new CatalogItem(this);
        addItemInternal(aItem);
        if(theStaticLocToLog.isDebugEnabled())
        {
            MessageResources aMr = getRoot().getMessageResources();
            theStaticLocToLog.debug(
                aMr.getMessage(ICatalogMessagesKeys.PCAT_ADDED_ITEM,
                "null",
                this.getGuid()));
        }
        return aItem;
    }

    /**
     * Factory method for creating an item instance. Internal method which
     * can't be called via the public interface.
     *
     * @see #addItem(CatalogItem)
     * @see #addItem(CatalogItem,boolean)
     * 
     * @param aItemGuid the GUID of the item to be created
     * @return a instance of type <code>CatalogItem</code>
     */
    public CatalogItem createItemInternal(String aItemGuid)
    {
        CatalogItem aItem = new CatalogItem(this, aItemGuid);
        addItemInternal(aItem);
        if(theStaticLocToLog.isDebugEnabled())
        {
            MessageResources aMr = getRoot().getMessageResources();
            theStaticLocToLog.debug(
                aMr.getMessage(ICatalogMessagesKeys.PCAT_ADDED_ITEM,
                aItem.getGuid(),
                this.getGuid()));
        }
        return aItem;
    }

    //
    // methods for handling the attributes
    //
    public Iterator getAttributes() throws CatalogException // synchronization ????
    {
        if(isDeleted())
        {
            String theMessage = getRoot().getMessageResources().getMessage(
                ICatalogMessagesKeys.PCAT_ERR_IS_DELEDED);
            throw new CatalogException(theMessage);
        }

        Catalog root = getRoot();
        CatalogBuilder builder = root.getCatalogBuilder();
        root.setTimeStamp();
        try
        {
            if(!builder.isAttributesBuilt(this))
              builder.buildAttributes(this);
        }
        catch(CatalogBuildFailedException ex)
        {
            throw new CatalogException(ex.getLocalizedMessage());
        }
        
        return Collections.unmodifiableList(theAttributes).iterator();  
    }
    
    /**
     * Return the number of attributes
     */
    public int getAttributeSize() {
        int attSize = 0;
        
        if (theAttributes != null) {
            attSize = theAttributes.size();
        }
        
        return attSize;
    }

    /**
     * Returns the attributes if already built. If the attributes weren't built yet
     * an empty list will be returned.
     *
     * @return a list of attributes
     */
    public Iterator getAttributesInternal()
    {
        return theAttributes.iterator();
    }

    public synchronized Iterator getAttributes(Comparator aComparator)
        throws CatalogException
    {
        if(isDeleted())
        {
            String theMessage = getRoot().getMessageResources().getMessage(
                ICatalogMessagesKeys.PCAT_ERR_IS_DELEDED);
            throw new CatalogException(theMessage);
        }

        Catalog root = getRoot();
        CatalogBuilder builder = root.getCatalogBuilder();
        root.setTimeStamp();
        try
        {
            if(!builder.isAttributesBuilt(this))
              builder.buildAttributes(this);
        }
        catch(CatalogBuildFailedException ex)
        {
            throw new CatalogException(ex.getLocalizedMessage());
        }

        ArrayList attributes = (ArrayList)theAttributes.clone();
        Collections.sort(attributes, aComparator);
        return attributes.iterator();
    }

    /**
     * Returns the attribute with the given GUID. If the attribute was
     * deleted in the meantime it is also returned.
     *
     * @return an instance of type <code>CatalogAttribute</code>
     * @exception CatalogException error while retrieving the data from the server
     */
    public CatalogAttribute getAttributeInternal(String aAttributeGuid)
        throws CatalogException
    {
        this.getAttributes();
        
        Iterator iter = theAttributes.iterator();
        while(iter.hasNext()) {
            CatalogAttribute aAttribute = (CatalogAttribute)iter.next();
            if(aAttribute.getGuid().equalsIgnoreCase(aAttributeGuid))
                return aAttribute;
        }

        // propagate search to catalog
        Catalog root = getRoot();
        if(this != root)
            return root.getAttributeInternal(aAttributeGuid);

        return null;
    }

    /**
     * Sorts the list of attributes by name.
     */
    public void sortAttributes()
    {
         Collections.sort(theAttributes);
    }

    /**
     * Creates a new attribute. The composite has an association with the instance.
     * Internal method which can't be called via the public interface.
     *
     * @param aGuid         the GUID of the attribute
     * @param aTypeId       the id of the type of the attribute
     * @param aQuantityId   the Id of the quantity of the attribute, optional
     * @param unitIds       a list of unit Ids of the attribute, optional but a
     *                      quantity comes always with at least one unit
     * @return a instance of type <code>CatalogAttribute</code>
     */
    public CatalogAttribute createAttributeInternal(String aAttGuid,
                                                    String aTypeId,
                                                    String aQuantityId,
                                                    String unitIds)
    {
        CatalogAttribute aCatalogAttribute =
            new CatalogAttribute(this, aAttGuid, aTypeId, aQuantityId, unitIds);
        addAttributeInternal(aCatalogAttribute);
        return aCatalogAttribute;
    }

    /**
     * Creates a new attribute. The composite has an association with the instance.
     * Internal method which can't be called via the public interface.
     *
     * @param aGuid         the GUID of the attribute
     * @param aTypeId       the id of the type of the attribute
     * @param aQuantity     the Quantity of the attribute, optional
     * @return a instance of type <code>CatalogAttribute</code>
     */
    public CatalogAttribute createAttributeInternal(String aAttGuid,
                                                    String aTypeId,
                                                    IQuantity aQuantity)
    {
        CatalogAttribute aCatalogAttribute =
            new CatalogAttribute(this, aAttGuid, aTypeId, aQuantity);
        addAttributeInternal(aCatalogAttribute);
        return aCatalogAttribute;
    }

    /**
     * Add a new attribute to the internal collection without dispatching.
     */
    public void addAttributeInternal(CatalogAttribute aNewAttribute)
    {
        this.addAttributeInternal(aNewAttribute,false);
    }

    /**
     * Add a new attribute.
     */
    public void addAttributeInternal(   CatalogAttribute aNewAttribute,
                                        boolean dispatch)
    {
        synchronized(theAttributes) {
        theAttributes.ensureCapacity(10);  
        theAttributes.add(aNewAttribute);
        if (log.isDebugEnabled()) {
            log.debug("Size of attributes = " + theAttributes.size());
        } 
        getRoot().addAttribute(aNewAttribute,this);
        if(dispatch)
            this.dispatchAttributeCreatedEvent(aNewAttribute);
        }
        return;
    }

    /**
     * Creates a new attribute. The composite has an association with the instance.
     * Internal method which can't be called via the public interface.
     *
     * @return a new instance of <code>CatalogAttribute</code>
     */
    public CatalogAttribute createAttributeInternal()
    {
        CatalogAttribute aCatalogAttribute = new CatalogAttribute(this);
        addAttributeInternal(aCatalogAttribute);
        return aCatalogAttribute;
    }

    /**
     * Removes the given attribute instance from the list of attributes.
     * Internal method which can't be called via the public interface.
     *
     * @param attribute the attribute to be removed
     */
    public void deleteAttributeInternal(CatalogAttribute attribute)
    {
        synchronized(theAttributes) {
            if (!theAttributes.contains(attribute))
                return;
            theAttributes.remove(attribute);
            getRoot().removeAttribute(attribute);
            this.dispatchAttributeDeletedEvent(attribute);
        }
    }
    
    public ArrayList getTheCompositeEventListener() {
        
        if (theCompositeEventListener == null) {
            theCompositeEventListener = new ArrayList(5);
        }
        
        return theCompositeEventListener;
    }

    public void addCompositeEventListener(
        ICompositeEventListener aCompositeEventListener)
    {
        synchronized(theCompositeEventListener)
        {
            if( aCompositeEventListener != null && 
                (theCompositeEventListener == null || !theCompositeEventListener.contains(aCompositeEventListener)))
                getTheCompositeEventListener().add(aCompositeEventListener);
        }
    }

    public void removeCompositeEventListener(
        ICompositeEventListener aCompositeEventListener)
    {
        synchronized(theCompositeEventListener)
        {
            if (theCompositeEventListener!= null)
                theCompositeEventListener.remove(aCompositeEventListener);
        }
    }

    protected void dispatchChildCreatedEvent(
        CatalogHierarchicalComponent aNewChild)
    {
        if (theCompositeEventListener != null) {
            synchronized(theCompositeEventListener)
            {
                CatalogChildCreatedEvent ev =
                    new CatalogChildCreatedEvent(this,aNewChild);
                Iterator iter = theCompositeEventListener.iterator();
                while(iter.hasNext())
                    ((ICompositeEventListener)iter.next()).childCreated(ev);
            }
        }
    }

    protected void dispatchChildDeletedEvent(
        CatalogHierarchicalComponent aDeletedChild)
    {   
        if (theCompositeEventListener != null) {
            synchronized(theCompositeEventListener)
            {
                CatalogChildDeletedEvent ev =
                    new CatalogChildDeletedEvent(this,aDeletedChild);
                Iterator iter = theCompositeEventListener.iterator();
                while(iter.hasNext())
                    ((ICompositeEventListener)iter.next()).childDeleted(ev);
            }
        }
    }

    protected void dispatchAttributeCreatedEvent(
        CatalogAttribute aNewAttribute)
    {
        if (theCompositeEventListener != null) {
            synchronized(theCompositeEventListener)
            {
                CatalogAttributeCreatedEvent ev =
                    new CatalogAttributeCreatedEvent(this,aNewAttribute);
                Iterator iter = theCompositeEventListener.iterator();
                while(iter.hasNext())
                    ((ICompositeEventListener)iter.next()).attributeCreated(ev);
            }
        }
    }

    protected void dispatchAttributeDeletedEvent(
        CatalogAttribute aDeletedAttribute)
    {
        if (theCompositeEventListener != null) {
            synchronized(theCompositeEventListener)
            {
                CatalogAttributeDeletedEvent ev =
                    new CatalogAttributeDeletedEvent(this,aDeletedAttribute);
                Iterator iter = theCompositeEventListener.iterator();
                while(iter.hasNext())
                    ((ICompositeEventListener)iter.next()).attributeDeleted(ev);
            }
        }
    }

    //
    // methods for handling the queries
    //
    public synchronized IQuery getQuery(String aName)
    {
        // check if references were removed through the garbage collector
        this.cleanQueryMap();
        // search for the requested query
        Reference ref = (Reference)theQueries.get(aName);
        return (ref != null)?(IQuery)ref.get():null;
    }

    /**
     * Returns the list queries that were already performed.
     *
     * @return a list of queries
     */
    public synchronized Iterator getQueries()
    {
        // check if references were removed through the garbage collector
        this.cleanQueryMap();

        ArrayList queries = new ArrayList();
        Iterator iter = theQueries.values().iterator();
        while(iter.hasNext())
        {
            Reference ref = (Reference)iter.next();
            CatalogQuery query = (CatalogQuery)ref.get();
            if(query != null)
                queries.add(query);
        }
        return queries.iterator();
    }

    public synchronized IQueryStatement createQueryStatement()
    {

        return new CatalogQueryStatement();
    }

    public synchronized IQuery createQuery(IQueryStatement aStatement)
        throws CatalogException
    {
        return this.createQuery(aStatement, false);
    }

    public synchronized IQuery createQuery(IQueryStatement aStatement, boolean cache)
        throws CatalogException
    {
        // check if references were removed through the garbage collector
        this.cleanQueryMap();
        // search for the requested query
        Reference ref = (Reference)theQueries.get(aStatement.getName());
        CatalogQuery aQuery = (ref != null)?(CatalogQuery)ref.get():null;

        // A new query has to be created. It is stored in the hash map
        if(aQuery == null)
        {
            aQuery = new CatalogQuery(this, (CatalogQueryStatement)aStatement);
                //inherite the max number of leafs from your parent
            aQuery.theMaxNumberOfItemLeafs = this.theMaxNumberOfItemLeafs;
            if(cache)
                theQueries.put(aQuery.getName(), new SoftReference(aQuery, theRefQueue));
            else
                theQueries.put(aQuery.getName(), new WeakReference(aQuery, theRefQueue));
        }

        return aQuery;
    }
    
    protected void finalize() throws Throwable {
        if (log.isDebugEnabled()) {
            log.debug("Finalize Composite Component: " + this);
        }
        theCompositeEventListener   = null;
        theInnerNodes               = null;
        theLeafNodes                = null;
        theAttributes               = null;
        theQueries                  = null;
        theSerializableQueries      = null;
        theRefQueue                 = null;
        
        super.finalize();
    }

/************************** protected *****************************************/

    /**
     * Helper method which cleans the query map. All keys are removed for which
     * the garbage collector has removed the real reference.
     */
    protected void cleanQueryMap()
    {
        Reference ref = null;
        int counter = 0;

        // count the references which were removed through the gc
        do
        {
            ref = (Reference)theRefQueue.poll();
            if(ref != null) counter++;
        }
        while(ref != null);

        // remove all empty reference from the map
        Iterator iter = theQueries.values().iterator();
        while(iter.hasNext() && counter > 0)
        {
            ref = (Reference)iter.next();
            CatalogQuery query = (CatalogQuery)ref.get();
            if(query == null)
            {
                iter.remove();
                counter--;
            }
        }
    }

/************************** protected *****************************************/

    /**
     * Store the serializable object references of non serializable
     * WeakReferences in a separate collection for serialization and clean that
     * black board again after finishing the storage process.
     */
    synchronized private void writeObject(ObjectOutputStream oo) throws IOException
    {
        // store only serializable stuff i.e. no weak references
        Iterator iter = theQueries.keySet().iterator();
        // safe is safe
        theSerializableQueries.clear();
        while(iter.hasNext())
        {
            Object aKey;
            aKey = iter.next();
            Reference aRef = (Reference)theQueries.get(aKey);
            CatalogQuery query = (CatalogQuery)aRef.get();
            // remove all empty reference from the map
            if(query == null)
            {
                theQueries.remove(aKey);
            }
            else
            {
                // and store the serializable CatalogQueries
                theSerializableQueries.put(aKey,query);
            }
        }
        oo.defaultWriteObject();
        // We got the shit on the disc clean the black board
        theSerializableQueries.clear();
    }

    /**
     * Wrap the CatalogQueries in weak references and create a new
     * referenceQueue for the gc to inform about removals of objects TO COME.
     */
    private synchronized void readObject(ObjectInputStream oo)
        throws IOException, ClassNotFoundException
    {
        // get the shit from the disc
        oo.defaultReadObject();

        theQueries = new HashMap();
        // wrap the catalogQuery references as weak references
        this.theRefQueue = new ReferenceQueue();
        Iterator iter = theSerializableQueries.keySet().iterator();

        while(iter.hasNext())
        {
            Object aKey;
            aKey = iter.next();
            CatalogQuery query =
                (CatalogQuery) theSerializableQueries.get(aKey);
            WeakReference aQueryWrapper = new WeakReference(query,theRefQueue);
            // and store the non serializable WeakReferences
            theQueries.put(aKey,aQueryWrapper);
        }
        // clean the black board
        theSerializableQueries.clear();
    }

/************************** private *******************************************/

}

/**
 * Helper class for the traverse of the tree.
 */
class ChildrenIterator
    implements Iterator
{
    private Stack theIterators;
    private CatalogCompositeComponent theSource;

    public ChildrenIterator(CatalogCompositeComponent source)
    {
        theIterators = new Stack();
        theSource = source;
        theIterators.push(theSource.getChildComponents().iterator());
    }

    public boolean hasNext()
    {
        Iterator top = null;

        do
        {
            top = (Iterator)theIterators.peek();
            if(top != null && !top.hasNext())
            {
                theIterators.pop();
                top = null;
            }
        }
        while(!theIterators.empty() && top == null);

        return (theIterators.empty())?false:true;
    }

    public Object next()
    {
        Iterator top = null;
        CatalogHierarchicalComponent element = null;

        do
        {
            top = (Iterator)theIterators.peek();
            if(top.hasNext())
            {
                element = (CatalogHierarchicalComponent)top.next();
                if(!(top instanceof ChildrenIterator))
                    theIterators.push(element.iterator());
            }
            else
                theIterators.pop();
        }
        while(top != null && element == null);

        return element;
    }

    public void remove()
    {
        throw new UnsupportedOperationException();
    }
}
