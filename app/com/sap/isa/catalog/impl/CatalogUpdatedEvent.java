/*****************************************************************************
  Copyright (c) 2000, SAPMarkets Europe GmbH, Germany, All rights reserved.
  Created:      2002

  $Id: //sap/ESALES_base/dev/src/_pcatAPI/java/com/sap/isa/catalog/impl/CatalogDeletedEvent.java#2 $
  $Revision: #2 $
  $Change: 60505 $
  $DateTime: 2002/04/15 09:05:37 $
*****************************************************************************/
package com.sap.isa.catalog.impl;

import java.util.EventObject;

import com.sap.isa.catalog.boi.ICatalogInfo;
import com.sap.isa.catalog.boi.ICatalogUpdatedEvent;
import com.sap.isa.catalog.boi.IServerEngine;

/**
 * Event that is fired if a catalog is updated on a server.
 *
 * @version     1.0
 */
public class CatalogUpdatedEvent
    extends EventObject
    implements ICatalogUpdatedEvent
{
    private CatalogServerEngine.CatalogInfo theUpdatedCatalog;

    public CatalogUpdatedEvent(CatalogServerEngine aParent,
                                     CatalogServerEngine.CatalogInfo anUpdatedCatalog)
    {
        super(aParent);
        this.theUpdatedCatalog = anUpdatedCatalog;
    }

    public IServerEngine getServer()
    {
        return (IServerEngine) getSource();
    }

    public ICatalogInfo getCatalogInfo()
    {
        return theUpdatedCatalog;
    }
}
