/**
 * Copyright (c) 2000, SAPMarkets Europe GmbH, Germany, All rights reserved. 
 * Created:      09 July 2001 
 * $Id: //sap/ESALES_base/30_SP_COR/src/_pcatAPI/java/com/sap/isa/catalog/impl/CatalogQuery.java#2 $ 
 * $Revision: #2 $
 * $Change: 129221 $ 
 * $DateTime: 2003/05/15 17:20:58 $
 */
package com.sap.isa.catalog.impl;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;

import com.sap.isa.catalog.boi.CatalogException;
import com.sap.isa.catalog.boi.IItem;
import com.sap.isa.catalog.boi.IQuery;
import com.sap.isa.catalog.boi.IQueryStatement;
import com.sap.isa.catalog.catalogvisitor.CatalogVisitor;
import com.sap.isa.core.logging.IsaLocation;

/**
 * This class implements the <code>IQuery</code> interface. With this class a query against the catalog can be
 * formulated.  <br/> Only the getter-methods are synchronized. The factory method for the builder are not synchronized because this is
 * done in the builder instance. Only the builder is allowed to modify the catalog tree. <br/> Users of the catalog should only work with the interface. All other <code>public</code> methods of this class are
 * for internal use only.
 *
 * @version 1.0
 */
public class CatalogQuery extends CatalogCategory implements IQuery {
    
    private static IsaLocation log =
            IsaLocation.getInstance(CatalogCompositeComponent.class.getName());
    
    /*
     *  CatalogCategory implements Serializable:
     *  Fields:
     *  theStatement    : implements Serializable
     */
    private CatalogQueryStatement theStatement;
    protected boolean inCountMode = false;
    
    /**
     * Constructs a new instance for a global catalog search or category specific search.
     *
     * @param aComponent the catalog or category of the query
     * @param aStatement the statement of the query
     */
    protected CatalogQuery(CatalogCompositeComponent aComponent, CatalogQueryStatement aStatement) {
        super(aComponent, "CatalogQuery" + System.currentTimeMillis());
        theStatement = aStatement;
        theStatement.setQuery(this);
    }

    /**
     * DOCUMENT ME!
     *
     * @param visitor DOCUMENT ME!
     */
    public void assign(CatalogVisitor visitor) {
        visitor.visitQuery(this);
    }

    /**
     * Returns <code>true</code> if this query is a category specific query.
     *
     * @return <code>true</code> if category specific search
     */
    public boolean isCategorySpecificSearch() {
        return (getParent() == null) ? false : true;
    }

    /**
     * DOCUMENT ME!
     *
     * @return DOCUMENT ME!
     */
    public IQueryStatement getStatement() {
        return this.theStatement;
    }

    /**
     * Returns a unique name of the query.
     *
     * @return the name of the query instance
     */
    public synchronized String getName() {
        return theStatement.getName();
    }

    /**
     * Returns the statement. This method returns a reference to the implementation class. Therefore this method should
     * be used inside the catalog implementation classes.
     *
     * @return the statement instance
     */
    public synchronized CatalogQueryStatement getCatalogQueryStatement() {
        return this.theStatement;
    }

    /**
     * DOCUMENT ME!
     *
     * @return DOCUMENT ME!
     */
    public synchronized String getStatementAsString() {
        return this.theStatement.getStatementAsString();
    }

    /**
     * DOCUMENT ME!
     *
     * @return DOCUMENT ME!
     */
    public Iterator getAttributesInternal() {
        Iterator result = theAttributes.iterator();

        if (isCategorySpecificSearch()) {
            result = ((CatalogCategory) getParent()).getAttributesInternal();
        }

        return result;
    }

    /**
     * DOCUMENT ME!
     *
     * @param aAttributeGuid DOCUMENT ME!
     *
     * @return DOCUMENT ME!
     *
     * @throws CatalogException DOCUMENT ME!
     */
    public CatalogAttribute getAttributeInternal(String aAttributeGuid)
                                          throws CatalogException {
        getRoot().setTimeStamp();

        Iterator iter = this.getAttributes();

        while (iter.hasNext()) {
            CatalogAttribute aAttribute = (CatalogAttribute) iter.next();

            if (aAttribute.getGuid().equalsIgnoreCase(aAttributeGuid)) {
                return aAttribute;
            }
        }

        // propagate search to catalog
        return getRoot().getAttributeInternal(aAttributeGuid);
    }

    /**
     * DOCUMENT ME!
     *
     * @return DOCUMENT ME!
     *
     * @throws CatalogException DOCUMENT ME!
     */
    public Iterator getAttributes()
                           throws CatalogException {
        if (isCategorySpecificSearch()) {
            return getParent().getAttributes();
        }

        return theAttributes.iterator(); // empty iterator instance
    }

    /**
     * DOCUMENT ME!
     *
     * @param aComparator DOCUMENT ME!
     *
     * @return DOCUMENT ME!
     *
     * @throws CatalogException DOCUMENT ME!
     */
    public synchronized Iterator getAttributes(Comparator aComparator)
                                        throws CatalogException {
        if (isCategorySpecificSearch()) {
            return getParent().getAttributes(aComparator);
        }

        return theAttributes.iterator(); // empty iterator instance
    }

    /**
     * DOCUMENT ME!
     *
     * @param itemGuid DOCUMENT ME!
     *
     * @return DOCUMENT ME!
     *
     * @throws CatalogException DOCUMENT ME!
     */
    public synchronized IItem getItem(String itemGuid)
                               throws CatalogException {
        this.submit();

        CatalogItem item = null;

        for (Iterator iter = theLeafNodes.iterator(); iter.hasNext();) {
            item = (CatalogQueryItem) iter.next();

            if (item.getGuid().equals(itemGuid)) {
                break;
            }

            item = null;
        }

        return item;
    }

    /**
     * DOCUMENT ME!
     *
     * @return DOCUMENT ME!
     *
     * @throws CatalogException DOCUMENT ME!
     */
    public synchronized Iterator getItems()
                                   throws CatalogException {
        getRoot().setTimeStamp();
        this.submit();

        return Collections.unmodifiableList(theLeafNodes).iterator();
    }

    /**
     * DOCUMENT ME!
     *
     * @param aComparator DOCUMENT ME!
     *
     * @return DOCUMENT ME!
     *
     * @throws CatalogException DOCUMENT ME!
     */
    public synchronized Iterator getItems(Comparator aComparator)
                                   throws CatalogException {
        getRoot().setTimeStamp();
        this.submit();

        ArrayList items = (ArrayList) theLeafNodes.clone();
        Collections.sort(items, aComparator);

        return items.iterator();
    }

    /**
     * Constructs a new item instance which is associated with this query. Internal method which can't be called via
     * the public interface.
     *
     * @param aItemId an id for the item
     * @param aCategoryId an id for the category
     *
     * @return a new <code>CatalogItem</code> instance
     */
    public CatalogQueryItem createQueryItemInternal(String aItemId, String aCategoryId) {
        CatalogQueryItem aQueryItem = new CatalogQueryItem(this, aCategoryId, aItemId);
        this.addItemInternal(aQueryItem);

        return aQueryItem;
    }
    
    protected void finalize() throws Throwable {
        if (log.isDebugEnabled()) {
            log.debug("Finalize Query: " + this);
        }
        theStatement = null;
        super.finalize();
    }

    /**
     * DOCUMENT ME!
     *
     * @return DOCUMENT ME!
     *
     * @throws CatalogException DOCUMENT ME!
     */
    public synchronized Iterator submit()
                                 throws CatalogException {
        Catalog root = getRoot();

        try {
            if (!root.getCatalogBuilder().isQueryBuilt(this)) {
                root.getCatalogBuilder().buildQuery(this);
            }
        }
        catch (CatalogBuildFailedException ex) {
            throw new CatalogException(ex);
        }

        //if not in count mode synchronize the theCount
        if (!inCountMode) {
            countMainItems();
        }
         // end of if ()

        return Collections.unmodifiableList(this.getChildComponents()).iterator();
    }
    
    /**
     * DOCUMENT ME!
     *
     * @param aCountMode DOCUMENT ME!
     */
    synchronized public void setCountMode(boolean aCountMode) {
        this.inCountMode = aCountMode;

        return;
    }

    /**
     * DOCUMENT ME!
     *
     * @return DOCUMENT ME!
     */
    public boolean inCountMode() {
        return inCountMode;
    }

    /**
     * DOCUMENT ME!
     *
     * @param aCount DOCUMENT ME!
     */
    public void setCountInternal(int aCount) {
        theCount = aCount;

        return;
    }

    /**
     * DOCUMENT ME!
     *
     * @param aCountAsString DOCUMENT ME!
     */
    public void setCountAsStringInternal(String aCountAsString) {
        try {
            theCount = Integer.parseInt(aCountAsString.trim());
        }
        catch (Throwable t) {
            //you got your fair chance to set the count
            theCount = -1;
        }
         // end of try-catch

        return;
    }

    /**
     * package
     *
     * @return DOCUMENT ME!
     */
    /**
     * Returns <code>true</code> if the query was already submitted, otherwise <code>false</code>.
     *
     * @return boolean which indicates if query was already submitted
     */
    boolean isSubmitted() {
        return getRoot().getCatalogBuilder().isQueryBuilt(this);
    }
}
