/*****************************************************************************
  Copyright (c) 2000, SAPMarkets Europe GmbH, Germany, All rights reserved.
  Created:      09 July 2001

  $Id: //sap/ESALES_base/30_SP_COR/src/_pcatAPI/java/com/sap/isa/catalog/impl/CatalogAttributeValueDeletedEvent.java#3 $
  $Revision: #3 $
  $Change: 150872 $
  $DateTime: 2003/09/29 09:18:51 $
*****************************************************************************/
package com.sap.isa.catalog.impl;

import java.util.EventObject;

import com.sap.isa.catalog.boi.IAttributeValue;
import com.sap.isa.catalog.boi.IAttributeValueDeletedEvent;
import com.sap.isa.catalog.boi.IItem;

/**
 * Event that is fired if an attribute value is added to an item.
 *
 * @version     1.0
 */
public class CatalogAttributeValueDeletedEvent
    extends EventObject
    implements IAttributeValueDeletedEvent
{
    private CatalogAttributeValue theDeletedValue;

    public CatalogAttributeValueDeletedEvent(   CatalogItem aParent,
                                                CatalogAttributeValue aDeletedValue)
    {
        super(aParent);
        this.theDeletedValue=aDeletedValue;
    }

    public IItem getItem()
    {
        return (IItem) getSource();
    }

    public IAttributeValue getAttributeValue()
    {
        return theDeletedValue;
    }
}