/*****************************************************************************
  Copyright (c) 2000, SAPMarkets Europe GmbH, Germany, All rights reserved.
  Created:      09 February 2002

  $Id:$
  $Revision:$
  $Change:$
  $DateTime:$
*****************************************************************************/
package com.sap.isa.catalog.impl;

import java.io.Serializable;
import java.security.Permission;
import java.security.Permissions;
import java.util.Enumeration;
import java.util.Iterator;

import com.sap.isa.catalog.boi.IActor;

/**
 * Simple implementation class of the <code>IServer</code> interface. This class
 * is no component of the catalog tree in the stricter sense.
 *
 * @version     1.0
 */
public class CatalogActor
    implements
        IActor,
        Serializable
{
    private String theName;
    private String thePWD;
    private Permissions thePermissions;
    private String theGuid;
    private String theCookie;

    public CatalogActor (String aName,
                         String aPWD,
                         Permissions aPermissions,
                         String aGuid,
                         String aCookie)
    {
        theName=aName;
        thePWD=aPWD;
        thePermissions = new Permissions();
        Enumeration anEnum = aPermissions.elements();
        while (anEnum.hasMoreElements()) 
        {
            Permission aPermission = (Permission) anEnum.nextElement();
            thePermissions.add(aPermission);
        } // end of while ()
        theGuid=aGuid;
        theCookie=aCookie;
    }
    
    public CatalogActor (IActor anActor)
    {
        theName=anActor.getName();
        thePWD=anActor.getPWD();
        thePermissions = new Permissions();
        Iterator anIter = anActor.getPermissions();
        if(anIter != null) while (anIter.hasNext())
        {
            Permission aPermission = (Permission) anIter.next();
            thePermissions.add(aPermission);
        } // end of while ()
        theGuid=anActor.getGuid();
        theCookie=anActor.getCookie();
    }
// implementation of com.sap.isa.catalog.boi.IActor interface

/**
 *
 * @return theName
 */
    public String getName()
    {
        return theName;
    }

/**
 *
 * @return <description>
 */
    public Iterator getPermissions()
    {
        final Enumeration anEnumeration =thePermissions.elements();
        return new Iterator()
            {
                Enumeration theEnumernation;
                {
                    theEnumernation = anEnumeration;
                }
                
                public boolean hasNext()
                {
                    return theEnumernation.hasMoreElements();
                }

                public Object next()
                {
                    return theEnumernation.nextElement();
                }

                public void remove()
                {
                    return;
                }
            };
    }
    

/**
 *
 * @return <description>
 */
    public String getGuid()
    {
        return theGuid;
    }

/**
 *
 * @return <description>
 */
    public String getPWD()
    {
        return thePWD;
    }

/**
 *
 * @return <description>
 */
    public String getCookie()
    {
        return theCookie;
    }
    
}// CatalogActor
