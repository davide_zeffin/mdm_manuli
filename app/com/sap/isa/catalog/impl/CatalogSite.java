/*****************************************************************************
  Copyright (c) 2000, SAPMarkets Europe GmbH, Germany, All rights reserved.

  $Id: //sap/ESALES_base/30_SP_COR/src/_pcatAPI/java/com/sap/isa/catalog/impl/CatalogSite.java#5 $
  $Revision: #5 $
  $Change: 148346 $
  $DateTime: 2003/09/12 15:13:19 $
*****************************************************************************/
package com.sap.isa.catalog.impl;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.Reader;
import java.io.Serializable;
import java.io.StringReader;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Locale;
import java.util.Properties;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.apache.struts.util.MessageResources;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.w3c.dom.Text;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import com.sap.isa.catalog.ICatalogMessagesKeys;
import com.sap.isa.catalog.boi.CatalogException;
import com.sap.isa.catalog.boi.IActor;
import com.sap.isa.catalog.boi.IEnvironment;
import com.sap.isa.catalog.boi.IServer;
import com.sap.isa.catalog.boi.IServerEngine;
import com.sap.isa.catalog.boi.ISite;
import com.sap.isa.catalog.cache.CachableItem;
import com.sap.isa.catalog.cache.CatalogCache;
import com.sap.isa.catalog.cache.CatalogCacheException;
import com.sap.isa.catalog.parsers.xpathinterpreter.ASTXPathExpr;
import com.sap.isa.catalog.parsers.xpathinterpreter.IXPnodeSetValue;
import com.sap.isa.catalog.parsers.xpathinterpreter.IXPvalue;
import com.sap.isa.catalog.parsers.xpathinterpreter.ParseException;
import com.sap.isa.catalog.parsers.xpathinterpreter.XPathContext;
import com.sap.isa.catalog.parsers.xpathinterpreter.XPathDomParserVisitor;
import com.sap.isa.catalog.parsers.xpathinterpreter.XPathParser;
import com.sap.isa.core.eai.BackendBusinessObjectMetaData;
import com.sap.isa.core.eai.BackendBusinessObjectParams;
import com.sap.isa.core.eai.BackendContext;
import com.sap.isa.core.eai.BackendException;
import com.sap.isa.core.eai.BackendObjectSupport;
import com.sap.isa.core.eai.Connection;
import com.sap.isa.core.eai.ConnectionFactory;
import com.sap.isa.core.eai.sp.jco.BackendBusinessObjectSAP;
import com.sap.isa.core.eai.sp.jco.JCoConnection;
import com.sap.isa.core.eai.sp.jco.JCoConnectionEventListener;
import com.sap.isa.core.init.InitializationEnvironment;
import com.sap.isa.core.logging.IsaLocation;

/**
 * CatalogSite.java
 * The top level entry point for clients of a catalog.
 *
 * Created: Thu Jan 24 09:34:58 2002
 *
 * @version 1.0
 */

public class CatalogSite
    implements CachableItem,
        ISite,
        IEnvironment,
        BackendBusinessObjectSAP,
        Serializable
{
    public static final String EAI_PROPERTY_SITE_GUID="siteGuid";
    public static final String EAI_PROPERTY_CATALOG_SITE_CONFIG_FILE="catalogSiteConfigFile";
    public static final String EAI_PROPERTY_IS_CLASS_PATH="isClassPath";
    public static final String EAI_PROPERTY_DISTRIBUTABLE = "distributable";
    
    public static final String CONFIG_FILE="/com/sap/isa/catalog/cfg/siteConfig.xml";
    
    private static final String COMMERCE_ONE_KEY ="cone";
    private static final String COMMERCE_ONE = "com.sap.isa.catalog.commerceone.CommerceOneServerEngine";    
    private static final String REQUISITE_KEY = "requisite";
    private static final String REQUISITE="com.sap.isa.catalog.requisite.RequisiteServerEngine";
    private static final String TREX_KEY ="trex";
    private static final String TREX="com.sap.isa.catalog.trex.TrexCatalogServerEngine";
    private static final String CRM_KEY="crm";
    private static final String CRM="com.sap.isa.catalog.ims.CRMCatalogServerEngine";
    private static final String R3_KEY ="r3";
    private static final String R3="com.sap.isa.catalog.r3.R3ServerEngine";

    private HashMap theTypes;								// serializable: Collection okay, Parts okay
    private ArrayList theCatalogServers = new ArrayList();	// serializable: Collection okay, Parts okay
    private Properties theNames;
    private Properties theDescriptions;
    private CatalogActor theActor;
    private Locale theLocale = Locale.ENGLISH;//default
    private MessageResources theMessageResources;
    private InitializationEnvironment theEnvironment;
    // the logging instance
    private static IsaLocation theStaticLocToLog =
        IsaLocation.getInstance(CatalogSite.class.getName());
    private BackendContext theBackendContext;
    private BackendObjectSupport theBEOS;
    private ConnectionFactory theConnectionFactory;
    private Properties theSiteProperties = new Properties();
    private JCoConnection theSAPConnection;
    private String theGuid;
    private Document theInitFile;
    
    private Timestamp theTimeStamp;

        /**
         * Creates a new <code>CatalogSite</code> instance.
         * Should not be used! Only public to allow creation via BEM
         * mechanism.
         *
         * @exception BackendException if an error occurs
         */
    public CatalogSite ()
        throws BackendException
    {
        theTypes = new HashMap();
        theTypes.put(REQUISITE_KEY, REQUISITE);
        theTypes.put(TREX_KEY, TREX);
        theTypes.put(R3_KEY, R3);
        theTypes.put(CRM_KEY, CRM);
        setTimeStamp();
    }

        /* *************************** BackendBusinessObject ************************ */

    public void setConnectionFactory(ConnectionFactory aConnectionFactory)
    {
        theConnectionFactory = aConnectionFactory;
    }

    public ConnectionFactory getConnectionFactory()
    {
        return theConnectionFactory;
    }

    public void setBackendObjectSupport(BackendObjectSupport aBEOS)
    {
        theBEOS=aBEOS;
        return;
    }

    public BackendObjectSupport getBackendObjectSupport()
    {
        return theBEOS;
    }

        /**
         * Init the object via the BEM mechanism.
         * It is required that the theParams object implements the interfaces
         * IActor and IEnvironment. 
         *
         * @param theEAIProperties a <code>Properties</code> value
         * @param theParams a <code>BackendBusinessObjectParams</code> value
         * @exception BackendException if an error occurs
         */
    public void initBackendObject(
        Properties theEAIProperties,
        BackendBusinessObjectParams theParams)
        throws BackendException
    {
            //add properties supplied via the eai config file
        theSiteProperties.putAll(theEAIProperties);

            //currently we support only one site
        theGuid=theSiteProperties.getProperty("siteGuid");

        IEnvironment theIEnvironment = (IEnvironment)theParams;
        IActor anActor = (IActor)theParams;
        theMessageResources=theIEnvironment.getEnvironment().getMessageResources();
        theEnvironment = theIEnvironment.getEnvironment();
        
        if ( theMessageResources == null || theEnvironment==null) 
        {
            String theMessage = theMessageResources.getMessage(
                ICatalogMessagesKeys.PCAT_ERR_BACKEND_OBJ_INSTANTIATE,
                this.getClass().getName());
            theStaticLocToLog.error(ICatalogMessagesKeys.PCAT_EXCEPTION, new Object[]{ theMessage }, null);            
            throw new BackendException(theMessage);
        } // end of if ()
        
        
        theActor = new CatalogActor(anActor);

            //find the init file
        String theConfigFilePath =
            theSiteProperties.getProperty(EAI_PROPERTY_CATALOG_SITE_CONFIG_FILE);
        boolean isClassPath =
            Boolean.valueOf(theSiteProperties.getProperty(EAI_PROPERTY_IS_CLASS_PATH)).booleanValue();

        theInitFile = loadConfigFile(theConfigFilePath, isClassPath);
        
        init(theInitFile);
        
        if (theStaticLocToLog.isDebugEnabled()) 
        {
            String theMessage = theMessageResources.getMessage(
                ICatalogMessagesKeys.PCAT_OBJ_INSTANTIATED,
                this.getClass().getName());
            theStaticLocToLog.debug(theMessage);
        } // end of if ()
        return;
    }

        /**
         * No resources need to be reclaimed.
         */
    public void destroyBackendObject()
    {
        return;
    }

    public void setContext(BackendContext context)
    {
        theBackendContext = context;
    }

    public BackendContext getContext()
    {
        return theBackendContext;
    }

        /******************************* BackendBusinessObjectSAP **************/

    synchronized public JCoConnection getDefaultJCoConnection()
    {
        if(theSAPConnection == null)
            setDefaultConnection(getConnectionFactory().getDefaultConnection());

        return theSAPConnection;
    }

    public JCoConnection getDefaultJCoConnection(Properties props)
        throws BackendException
    {
        return (JCoConnection) theConnectionFactory.getDefaultConnection(
            getDefaultJCoConnection().getConnectionFactoryName(),
            getDefaultJCoConnection().getConnectionConfigName(),
            props);
    }
        /**
         * This method is used to add JCo-call listener to this backend object.
         * A JCo listener will be notified before a JCo call is being executed
         * and after the JCo call has been exected.
         *
         * @param listener         a listener for JCo calls
         */
    public void addJCoConnectionEventListener(JCoConnectionEventListener listener)
    {
        theConnectionFactory.addConnectionEventListener(listener);
    }

    public JCoConnection getJCoConnection(String conKey)
        throws BackendException
    {
        return (JCoConnection)getConnectionFactory().getConnection(conKey);
    }

    public JCoConnection getModDefaultJCoConnection(Properties conDefProps)
        throws BackendException
    {
        ConnectionFactory factory = getConnectionFactory();
        JCoConnection defConn = (JCoConnection)factory.getDefaultConnection();

        return (JCoConnection)factory.getConnection(
            defConn.getConnectionFactoryName(),
            defConn.getConnectionConfigName(),
            conDefProps);
    }
        /**
         * Sets the default connection to the system where the infos about the
         * catalog are located.
         *
         * @param conn  the default connection
         */
    private void setDefaultConnection(Connection conn)
    {
        theSAPConnection = (JCoConnection)conn;
    }

    /************************ IEnvironment ************************************/

    public InitializationEnvironment getEnvironment()
    {
        return theEnvironment;
    }

        /* ******************************* CachableItem ****************************** */

    public Timestamp getTimeStamp()
    {
        return theTimeStamp;
    }

    public void setTimeStamp()
    {
        theTimeStamp = new Timestamp(System.currentTimeMillis());
    }

    public boolean isCachedItemUptodate()
    {
        return true;
    }

        /**
         * Generates key for catalog site instances.
         * Called by the catalog cache to generate a key to cache a catalogSite instance.
         * Caching is done on basis of the:
         * <ul>
         * <li>The type of the site,</li>
         * <li>The guid of the site,</li>
         * <li>The user of the site.</li>
         * </ul>
         * 
         * @param metaInfo a <code>BackendBusinessObjectMetaData</code> value
         * used to retrieve the "siteGuid" property
         * @param anActor an <code>IActor</code> value
         * @param aServer an <code>IServer</code> value not used for sites
         * @return a <code>String</code> value
         */
    public static String generateCacheKey(
        BackendBusinessObjectMetaData metaInfo,
        IActor anActor,
        IServer aServer)
    {
             
        String theSiteGuid = metaInfo.getBackendConfigKey();
        StringBuffer aBuffer = new StringBuffer(CatalogSite.class.getName());
        aBuffer.append(theSiteGuid);
        aBuffer.append(anActor.getName());
        aBuffer.append(anActor.getPWD());
        return aBuffer.toString();
    }
    
    /*****************************************************************************/

    
    public String getGuid()
    {
        return theGuid;
    }

    public String getName()
    {
        return theNames.getProperty(theLocale.toString());
    }

    public String getDescription()
    {
        return theDescriptions.getProperty(theLocale.toString());
    }

    private void addProperty(String name, String key, String value)
    {
        //unused: Properties theResult=null;
        if (name.intern()=="name".intern())
        {
            if (theNames==null)
            {
                theNames=new Properties();
            } // end of if ()
            theNames.setProperty(key,value);
        } // end of if ()
        if (name.intern()=="description".intern())
        {
            if ( theDescriptions == null)
            {
                theDescriptions = new Properties();
            } // end of if ()
            theDescriptions.setProperty(key, value);
        } // end of if ()
        return;
    }

        /**
         * The current Locale for lang. dep. messages.
         *
         * @return a <code>Locale</code> value
         */
    public Locale getLocale()
    {
        return (Locale)theLocale.clone();
    }

        /**
         * Describe <code>setLocale</code> method here.
         *
         * @param aLocale a <code>Locale</code> value
         * @exception CatalogException if an error occurs
         */
    public void setLocale(Locale aLocale)
        throws CatalogException
    {
        if (!theDescriptions.containsKey(aLocale.toString()))
        {
            throw new CatalogException(
                theMessageResources.getMessage(
                    ICatalogMessagesKeys.PCAT_ERR_LOCALE_NOT_SUPP)) ;
        } // end of if ()
        theLocale = aLocale;
        return ;
    }

        /**
         * Returns the catalog engine with the given guid or null
         * if not found.
         *
         * @param aGuid a <code>String</code> value
         * @return an <code>IServerEngine</code> or null
         */
    synchronized public IServerEngine getCatalogServer(String aGuid)
    {
        Iterator anIter = theCatalogServers.iterator();
        IServerEngine theResult = null;
        while (anIter.hasNext())
        {
            IServerEngine anEngine = (IServerEngine)anIter.next();
            if (anEngine.getGuid().intern()==aGuid.intern())
            {
                theResult=anEngine;
                break;
            } // end of if ()
        } // end of while ()
        return theResult;
    }

        /**
         * Returns the current list of catalog engines known by the site.
         *
         * @return an <code>Iterator</code> over type
         */
    synchronized public Iterator getCatalogServers()
    {
        return Collections.unmodifiableList(theCatalogServers).iterator();
    }

    public MessageResources getMessageResources()
    {
        return theMessageResources;
    }

        /**
         * The actor used to creat this instance.
         *
         * @return an <code>IActor</code> value
         */
    public IActor getActor()
    {
        return theActor;
    }

    public Document getConfigDocument()
    {
        return (Document)theInitFile.cloneNode(true);
    }
    
        /**
         * Initilizes the site accoring to the theInitFile.
         *
         * @param theInitFile a <code>Document</code> value
         * @exception BackendException if an error occurs
         */
    private void init(Document theInitFile)
        throws BackendException
    {
            //set name and description
        StringBuffer aXPathStringBuffer = new StringBuffer("/Site/Detail");
        Reader stream = new StringReader(aXPathStringBuffer.toString());
        XPathParser theXpp  = new XPathParser(stream);
        theXpp.disable_tracing();
        IXPnodeSetValue theResult =  (IXPnodeSetValue)evaluate(theXpp,
                                                               aXPathStringBuffer.toString(),theInitFile,theInitFile);

        ArrayList theResultList = theResult.getValue();
        Iterator anIter = theResultList.iterator();
        while ( anIter.hasNext())
        {
            Element aDetail = (Element) anIter.next();
            String theID = aDetail.getAttribute("ID");
            NodeList theChilds = aDetail.getElementsByTagName("Value");
            int size = theChilds.getLength();
            for (int i = 0; i < size; i++)
            {
                Element  aValue = (Element) theChilds.item(i);
                String theLocale = aValue.getAttribute("xml:lang");
                Text theContent = (Text)aValue.getFirstChild();
                addProperty(theID,theLocale,theContent.getData());
            } // end of for ()
        }

            //create the servers
        aXPathStringBuffer = new StringBuffer("/Site/Server");
        stream = new StringReader(aXPathStringBuffer.toString());
        theXpp.ReInit(stream);
        theResult =  (IXPnodeSetValue)evaluate(theXpp,
                                               aXPathStringBuffer.toString(),theInitFile,theInitFile);

        theResultList = theResult.getValue();
        anIter = theResultList.iterator();
        while (anIter.hasNext())
        {
            Element aServer = (Element)anIter.next();
            //unused: String theType = aServer.getAttribute("type");
            String theServerID = aServer.getAttribute("ID");
            
            Element theConfigFile = aServer;
            StringReader aStringReader =
                new StringReader("<Server></Server>");

            Document theConfigDocument=null;

            try
            {
                DocumentBuilder theDOMBuilder =
                    DocumentBuilderFactory.newInstance().newDocumentBuilder();

                theConfigDocument  = theDOMBuilder.parse(new InputSource(aStringReader));
                Element theConfigElement = theConfigDocument.getDocumentElement();

                NodeList theChilds = theConfigFile.getChildNodes();
                int size = theChilds.getLength();
                for (int i = 0; i < size; i++)
                {
                    Node aChild = (Node) theChilds.item(i);
                    Node aChildClone = aChild.cloneNode(true);
                    Node theImportedChild =
                        theConfigDocument.importNode(aChildClone,true);

                    theConfigElement.appendChild(theImportedChild);

                } // end of for ()
            } catch (ParserConfigurationException pce)
            {
                throw new BackendException( pce.toString());
            } // end of try-catch
            catch ( SAXException se)
            {
                throw new BackendException( se.toString());
            } // end of catch
            catch ( IOException ioe)
            {
                throw new BackendException( ioe.toString());
            } // end of catch

            CatalogServerEngine aNewCatalogServer = null;

                //look first in the cache

            String key = null;
            CatalogCache aCache = null;
            BackendBusinessObjectMetaData metaInfo =
                this.getBackendObjectSupport().getMetaData(theServerID);
            try 
            {
                aCache = CatalogCache.getInstance();
                    // try to build a key - if not possible the catalogserver won't be cached                
                key = CatalogCache.generateKey(metaInfo, theActor, null);                                        
            }
            catch (CatalogCacheException cce) 
            {
                throw new BackendException(cce);
            } // end of try-catch
            if(key != null && key.length() != 0)
                aNewCatalogServer = (CatalogServerEngine) aCache.getCachedInstance(key);

                // the catalog server was not cached yet - create new one and cache it now!            
            if ( aNewCatalogServer == null) 
            {
                    //build ServerEngines via BEM mechanisms
                aNewCatalogServer =
                    (CatalogServerEngine) getBackendObjectSupport().
                    createBackendBusinessObject(theServerID,
                                                new CatalogServerParams(theServerID,
                                                                        this,
                                                                        theConfigDocument));
            
                theCatalogServers.add(aNewCatalogServer);

                aXPathStringBuffer = new StringBuffer("./Detail");
                stream = new StringReader(aXPathStringBuffer.toString());
                theXpp.ReInit(stream);
                theResult =  (IXPnodeSetValue)evaluate(theXpp,
                                                       aXPathStringBuffer.toString(),theInitFile,aServer);
                
                theResultList = theResult.getValue();
                int lenght = theResultList.size();
                for ( int i = 0; i < lenght; i++)
                {
                    Element aDetail = (Element) theResultList.get(i);
                    String theID = aDetail.getAttribute("ID");
                    NodeList theValueChilds = aDetail.getElementsByTagName("Value");
                    int childNum = theValueChilds.getLength();
                    for (int j = 0; j < childNum; j++)
                    {
                        Element  aValue = (Element) theValueChilds.item(j);
                        String theLocale = aValue.getAttribute("xml:lang");
                        if (theLocale.equals("")) 
                            theLocale=null;                        
                        Text theContent = (Text)aValue.getFirstChild();
                        aNewCatalogServer.addProperty(theID,theLocale,theContent.getData());
                    } // end of for ()
                }
                    //finally cache it
                if (key!=null &&  key.length() != 0) 
                {
                    aCache.addCachableInstance(key, aNewCatalogServer);                    
                } // end of if ()
            }
            else 
            {
                theCatalogServers.add(aNewCatalogServer);
            } // end of else
		// end of if ()
        } // end of while ()
        return;
    }

        /**
         * Loads a site configuration file. Loads the file via classpath or
         * via drive path.
         * The configuration file must be a valid document according to the
         * catalog.dtd.
         *
         * @param aPath a <code>String</code> value
         * @param onClassPath a <code>boolean</code> value
         * @return a <code>Document</code> value
         * @exception BackendException if an error occurs
         */
    private Document loadConfigFile(String aPath, boolean onClassPath)
        throws BackendException
    {
        InputStream aInputStream = null;

        if (onClassPath)
        {
            aInputStream = this.getClass().getResourceAsStream(aPath);
            if (aInputStream==null)
            {
                String theMessage = theMessageResources.getMessage(
                    ICatalogMessagesKeys.PCAT_ERR_FILE_NOT_FOUND,
                    aPath);
                throw new BackendException(theMessage);
            } // end of if ()
        }
        else
        {
        	// Check whether the file is xcm managed
        	aInputStream = this.getEnvironment().getResourceAsStream(aPath);
        	if( aInputStream == null)
        	{
	            File aFile = new File(aPath);
	                //figure out what type of path we have here
            
	            if (!aFile.isAbsolute()) 
	            {
	                String theRealPath = this.getEnvironment().getRealPath(aPath);
	                aFile = new File(theRealPath);
	            } // end of if ()
            
	            try
	            {
	                aInputStream = new FileInputStream(aFile);
	            }
	            catch (FileNotFoundException fne)
	            {
	                throw new BackendException(fne.getMessage());
	            } // end of try-catch
        	}
        } // end of else

        Document theResult=null;

        try
        {
            DocumentBuilder theDOMBuilder =
                DocumentBuilderFactory.newInstance().newDocumentBuilder();
            theResult = theDOMBuilder.parse(aInputStream);
        }
        catch (ParserConfigurationException pe)
        {
            throw new BackendException(pe.getMessage());
        } // end of try-catch
        catch (SAXException se)
        {
            throw new BackendException(se.getMessage());
        } // end of catch
        catch ( IOException ioe)
        {
            throw new BackendException(ioe.getMessage());
        } // end of catch
        return theResult;
    }

        /**
         * Small helper to evaluate an xpath expression.
         *
         * @param theXpp a <code>XPathParser</code> value
         * @param aExpression a <code>String</code> value
         * @param aDoc a <code>Document</code> value
         * @return an <code>IXPvalue</code> value
         */
    private IXPvalue evaluate(XPathParser theXpp,
                              String aExpression,
                              Document aDoc ,
                              Node aContextNode)
    {
        Reader stream = new StringReader(aExpression);

        theXpp.ReInit(stream);

        ASTXPathExpr aXPathExpr = null;
        try
        {
            aXPathExpr = theXpp.XPathExpr();
        }
        catch (ParseException e)
        {
                //fail("Parse of correct expression failed");
            return null;
        }

        XPathDomParserVisitor aXPathDomVistor =
            new XPathDomParserVisitor(aDoc,aXPathExpr);

        ArrayList theContextList = new ArrayList();
        theContextList.add(aContextNode);

        XPathContext aContext = new XPathContext(theContextList,0);
        return aXPathDomVistor.evaluate(aContext);
    }

        /**
         * Small helper class for the creation of the server engines via the BEM
         * mechanism.
         */
    public class CatalogServerParams implements BackendBusinessObjectParams
    {
        private String theID;
        private Document theConfigDocument;
        private CatalogSite theSite;
        
        public CatalogServerParams(String theID,
                                   CatalogSite aSite,
                                   Document aConfigDocument )
        {
            this.theID=theID;
            theConfigDocument=aConfigDocument;
            theSite=aSite;
        }

            /**
             * Get the guid of the server to create.
             *
             * @return a <code>String</code> value
             */
        public String getGuid()
        {
            return theID;
        }
        
            /**
             * Get the parent of the server. 
             *
             * @return a <code>CatalogSite</code> value
             */
        public CatalogSite getSite()
        {
            return theSite;
        }
        
            /**
             * Get the config document for the server to create.
             *
             * @return a <code>Document</code> value
             */
        public Document getDocument()
        {
            return theConfigDocument;
        }
    }
}
