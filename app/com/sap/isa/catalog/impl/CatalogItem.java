/*****************************************************************************
  Copyright (c) 2000, SAPMarkets Europe GmbH, Germany, All rights reserved.
  Created:      09 July 2001

  $Id: //sap/ESALES_base/30_SP_COR/src/_pcatAPI/java/com/sap/isa/catalog/impl/CatalogItem.java#4 $
  $Revision: #4 $
  $Change: 144305 $
  $DateTime: 2003/08/22 20:36:34 $
 *****************************************************************************/

package com.sap.isa.catalog.impl;

//  catalog imports
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;

import org.apache.struts.util.MessageResources;

import com.sap.isa.catalog.AttributeKeyConstants;
import com.sap.isa.catalog.ICatalogMessagesKeys;
import com.sap.isa.catalog.boi.CatalogException;
import com.sap.isa.catalog.boi.IAttributeValue;
import com.sap.isa.catalog.boi.ICategory;
import com.sap.isa.catalog.boi.IItem;
import com.sap.isa.catalog.boi.IItemEventListener;
import com.sap.isa.catalog.catalogvisitor.CatalogVisitor;
import com.sap.isa.core.logging.IsaLocation;

/**
 * This class implements the item functionality for the catalog API. <br/>
 * Only the getter-methods are synchronized. The factory methods for the builder
 * are not synchronized because this is done in the builder instance. Only the
 * builder is allowed to modify the catalog tree. <br/>
 * Users of the catalog should only work with the interface. All other
 * <code>public</code> methods of this class are for internal use only.
 *
 * @version     1.0
 */
public class CatalogItem extends CatalogHierarchicalComponent implements IItem {
    /*
     *  Implements Serializable:
     *  Fields:
     *  theName             :   no problem!
     *  theItemEventListener:   not our problem, should better be serilizable,
                                the container is.
     *  theGuid             :   no problem!
     *  theProuctGuid       :   no problem!
     *  theArributeValues   :   theContainer implements Serializable!
     *                          The parts implement Serializable themself!
     */

    private static long guid = 0;
    private ArrayList theItemEventListener = null;
    private String theName = null;
    private String theGuid = null;
    private String theProductGuid = null;
    protected ArrayList theAttributeValues = new ArrayList();

    // the logging instance
    private static IsaLocation theStaticLocToLog =
        IsaLocation.getInstance(CatalogItem.class.getName());

    /**
     * Constructs a new item instance.
     *
     * @param aComponent the component of this item
     */
    public CatalogItem(CatalogCompositeComponent aComponent) {
        //Usually there is no name of an item!
        //It simply exists and has a couple of values.
        theName = String.valueOf(guid++);
        theGuid = theName;
        theProductGuid = theGuid; //must be set later to the correct value
        setParent(aComponent);
    }

    /**
     * Constructs a new item instance.
     *
     * @param aComponent the component of this item
     * @param aGuid      the guid of this item
     */
    public CatalogItem(CatalogCompositeComponent aComponent, String aGuid) {
        //Usually there is no name of an item!
        //It simply exists and has a couple of values.
        theName = aGuid;
        theGuid = aGuid;
        theProductGuid = aGuid; //must be set later to the correct value
        setParent(aComponent);
    }

    public void assign(CatalogVisitor visitor) {
        visitor.visitItem(this);
    }

    public Iterator iterator() {
        // return null iterator since it is a leaf in the tree
        return new Iterator() {
            public boolean hasNext() {
                return false;
            }
            public Object next() {
                return null;
            }
            public void remove() {
                throw new UnsupportedOperationException();
            }
        };
    }

    /**
     * Checks if the item is an accessory.<br> 
     * 
     * This is the case, if the attribute IS_SUBCOMP_OF_ACCESS is set to "X".
     * 
     * @return true, if the item is an accessory.<br>
     *         false, otherwise
     */
    public boolean isAccessory() {

        // Evaluate IS_SUBCOMP_OF_ACCESS attribute in the case of CRM server
        if (!getRoot().useRoleAttrForItemType()) {
            String value = getAttributeByKey(AttributeKeyConstants.IS_SUBCOMP_OF_ACCESS);
            return "X".equalsIgnoreCase(value);
        }

        // Evaluate ROLE attribute in the case of other servers
        if (getAttributeValue("ROLE") == null) {
            return false;
        }

        return getAttributeValue("ROLE") != null
            && getAttributeValue("ROLE").getAsString().equals("B");
    }

    /**
     * Checks, if the item has accessories.
     * 
     * @return  true, if the item has accessories,
     *          false, otherwise.
     */
    public boolean isItemWithAccessories() {

        String value = getAttributeByKey(AttributeKeyConstants.IS_ITEM_WITH_ACC);

        return "X".equalsIgnoreCase(value);
    }

    /**
     * Checks if the item is just a main item.<br> 
     * 
     * Returns true if an item is directly part of an area (e.g would be false if only be 
     * part of an area because of accessory relations)     
     *   
     * @return true, if the item is a main item.<br>
     *         false, otherwise
     */
    public boolean isMainItem() {

        // Evaluate IS_MAIN_ITEM attribute in the case of CRM server
        if (!getRoot().useRoleAttrForItemType()) {
            String value = getAttributeByKey(AttributeKeyConstants.IS_MAIN_ITEM);
            return "X".equalsIgnoreCase(value);
        }

        // Evaluate ROLE attribute in the case of other servers
        if (getAttributeValue("ROLE") == null) {
            return true;
        }

        if (getAttributeValue("ROLE") != null
            && getAttributeValue("ROLE").getAsString().equals("B")) {
            return false;
        }

        return true;
    }
    
    /**
     * Checks, if the item is a Rate Plan (Tariff).
     * 
     * This is the case, if the attribute PRODUCT_ROLE is set to "R".
     * 
     * @return  true, if the item has the role Rate Plan 
     *          false, otherwise.
     */
    public boolean isRatePlan() {

        String value = getAttributeByKey(AttributeKeyConstants.PRODUCT_ROLE);

        return AttributeKeyConstants.IS_PR_RATEPLAN.equalsIgnoreCase(value);
    }

    /**
     * Checks, if the item is a Combined Rate Plan (Tarif).
     * 
     * This is the case, if the attribute PRODUCT_ROLE is set to "C".
     * 
     * @return  true, if the item is a Combined Rate Plan 
     *          false, otherwise.
     */
    public boolean isCombinedRatePlan() {

        String value = getAttributeByKey(AttributeKeyConstants.PRODUCT_ROLE);

        return AttributeKeyConstants.IS_PR_COMBINEDRATEPLAN.equalsIgnoreCase(value);
    }

    /**
     * Checks, if the item is a Sales Package.
     * 
     * This is the case, if the attribute PRODUCT_ROLE is set to "S".
     * 
     * @return  true, if the item is a Sales Package
     *          false, otherwise.
     */
    public boolean isSalesPackage() {

        String value = getAttributeByKey(AttributeKeyConstants.PRODUCT_ROLE);

        return AttributeKeyConstants.IS_PR_SALESPACKAGE.equalsIgnoreCase(value);
    }

    /**
     * Checks if the item is relevant for explosion with the Solution Configurator.<br> 
     * 
     * This is the case, if the attribute IS_SOL_CONF_EXPLO is set to "X".
     * 
     * @return true, if the item is relevant for explosion.<br>
     *         false, otherwise
     */
    public boolean isRelevantForExplosion() {

        String value = getAttributeByKey(AttributeKeyConstants.IS_SOLCONF_EXPLO);

        return "X".equalsIgnoreCase(value);
    }

    /**
     * Checks if the item is a sub component.
     */
    public boolean isSubComponent() {

        return !isMainItem();
    }

    /**
     * Returns the value of a catalog attribute, determined using attribute key mapping.
     *
     * @param attrKey key of the requested catalog attribute, which is mapped to the 
     *        corresponding attribute name, and for that the value is determined using
     *        <code>getAttribute</code>
     *
     * @return value of the requested attribute; empty String (not null!) if not available
     */
    protected String getAttributeByKey(String attrKey) {

        String attrName = getRoot().getAttributeGuid(attrKey);
        String attrVal = "";

        if (attrName == null || attrName.trim().equals("")) {

            theStaticLocToLog.debug(
                "catalog.message.attrMap.notDef; Key is not defined; key= "
                    + attrKey
                    + " ; We return an empty string");
            return "";
        }

        IAttributeValue attr = getAttributeValue(attrName);

        if (attr != null) {
            attrVal = attr.getAsString();
        }

        theStaticLocToLog.debug(
            "getAttributeByKey: key=" + attrKey + " name=" + attrName + " value=" + attrVal);

        return attrVal;
    }

    public synchronized String getName() {
        return theName;
    }

    /**
     * Sets the name of the item. Internal method which
     * can't be called via the public interface.
     *
     * @param aName the name to be set
     */
    public void setNameInternal(String aName) {
        this.theName = aName;
        this.dispatchChangedEvent();
    }

    public synchronized String getGuid() {
        return theGuid;
    }

    /**
     * Sets the guid of the item. Internal method which
     * can't be called via the public interface.
     *
     * @param aGuid the guid to be set
     */
    public void setGuidInternal(String aGuid) {
        this.theGuid = aGuid;
    }

    public synchronized String getProductGuid() {
        return theProductGuid;
    }

    /**
     * Sets the product guid of the item. Internal method which
     * can't be called via the public interface.
     *
     * @param aGuid the guid to be set
     */
    public void setProductGuidInternal(String aGuid) {
        this.theProductGuid = aGuid;
        this.dispatchChangedEvent();
    }

    /**
     * Returns the attribute values if already built. If the attribute values
     * weren't built yet an empty list will be returned. The attribute values
     * that were deleted will be also returned!
     *
     * @return a list of attribute values
     */
    public Iterator getAttributeValuesInternal() {
        return theAttributeValues.iterator();
    }

    public synchronized Iterator getAttributeValues() {
        getRoot().setTimeStamp();
        return Collections.unmodifiableList(theAttributeValues).iterator();
    }

    public synchronized Iterator getAttributeValues(Comparator aComparator) {
        getRoot().setTimeStamp();
        ArrayList values = (ArrayList) theAttributeValues.clone();
        Collections.sort(values, aComparator);
        return values.iterator();
    }

    public synchronized ICategory getParent() {
        return (ICategory) getParentComponent().queryInterface(CatalogComponentTypes.I_CATEGORY);
    }

    public synchronized CatalogCategory getCategory() {
        return (CatalogCategory) getParentComponent().queryInterface(
            CatalogComponentTypes.CATALOG_CATEGORY);
    }

    public synchronized IAttributeValue getAttributeValue(String guidOfaAttribute) {
        getRoot().setTimeStamp();

        Iterator aIterator = theAttributeValues.iterator();
        CatalogAttributeValue aAttributeValue = null;
        while (aIterator.hasNext()) {
            aAttributeValue = (CatalogAttributeValue) aIterator.next();
            if (aAttributeValue.getAttributeGuid().equals(guidOfaAttribute))
                break;
            aAttributeValue = null;
        }
        return aAttributeValue;
    }

    public int compareTo(Object op) {
        if (!(op instanceof CatalogItem))
            throw new ClassCastException();

        return getName().compareTo(((CatalogItem) op).getName());
    }

    /**
     * Associate the item with its value. Without dispaching events.
     *
     * @param attValue a value created with {@link #createAttributeValueInternal()}
     */
    public void addAttributeValueInternal(CatalogAttributeValue aAttValue) {
        this.addAttributeValueInternal(aAttValue, false);
    }

    /**
     * Associate the item with its value.
     *
     * @param attValue a value created with {@link #createAttributeValueInternal()}
     * @param dispatch dispatch event to listeners
     */
    public void addAttributeValueInternal(CatalogAttributeValue aAttValue, boolean dispatch) {
        if (theStaticLocToLog.isDebugEnabled()) {
            MessageResources aMr = getRoot().theMessageResources;
            theStaticLocToLog.debug(
                aMr.getMessage(
                    ICatalogMessagesKeys.PCAT_ADDED_VALUE,
                    (aAttValue != null ? aAttValue.getAttributeName() : "NULL"),
                    (aAttValue != null ? aAttValue.getAsString() : "NULL")));
        }
        theAttributeValues.add(aAttValue);
        if (dispatch)
            dispatchAttributeValueCreatedEvent(aAttValue);
    }

    /**
     * Creates an unassociated CatalogAttributeValue i.e. the item has no
     * association to that value. Call {@link #addAttributeValueInternal} to associate
     * it with the Item. Internal method which can't be called via the public
     * interface.
     *
     * @return a new instance of <code>CatalogAttributeValue</code>
     */
    public CatalogAttributeValue createAttributeValueInternal() {
        CatalogAttributeValue aAtt = new CatalogAttributeValue(this);
        return aAtt;
    }

    /**
     * Creates an associated CatalogAttributeValue. Internal method which
     * can't be called via the public interface.
     *
     * @return a new instance of <code>CatalogAttributeValue</code>
     */
    public CatalogAttributeValue createAttributeValueInternal(String aAttributeGuid) {
        CatalogAttribute aAttribute = null;
        try {
            aAttribute = getCategory().getAttributeInternal(aAttributeGuid);
        }
        catch (CatalogException ce) {
            theStaticLocToLog.error(
                ICatalogMessagesKeys.PCAT_EXCEPTION,
                new Object[] { "CatalogItem" },
                ce);
        }

        CatalogAttributeValue aAtt = null;
        if (aAttribute != null) {
            aAtt = new CatalogAttributeValue(this, aAttribute);
            addAttributeValueInternal(aAtt, false);
        }
        else //compromise for requiste move something on the stack(digester) just to call some methods and than forget it
            aAtt = new CatalogAttributeValue(this, aAttribute);

        return aAtt;
    }

    /**
     * Removes the given attribute value instance from the list of attribute
     * values. Internal method which can't be called via the public interface.
     *
     * @param value the attribue value to be removed
     */
    public void deleteAttributeValueInternal(CatalogAttributeValue value) {
        theAttributeValues.remove(value);
        this.dispatchAttributeValueDeletedEvent(value);
    }

    protected ArrayList getTheItemEventListener() {
        
        if (theItemEventListener == null) {
            theItemEventListener = new ArrayList(5);
        }
        
        return theItemEventListener;
    }
    
    synchronized public void addItemEventListener(IItemEventListener aItemEventListener) {
        if (aItemEventListener != null &&
            (theItemEventListener == null || !theItemEventListener.contains(aItemEventListener)) )
            getTheItemEventListener().add(aItemEventListener);
    }

    synchronized public void removeItemEventListener(IItemEventListener aItemEventListener) {
        if (aItemEventListener != null && theItemEventListener != null)
            theItemEventListener.remove(aItemEventListener);
    }

    synchronized protected void dispatchAttributeValueCreatedEvent(CatalogAttributeValue aNewValue) {
        if (theItemEventListener != null) {
            CatalogAttributeValueCreatedEvent ev =
                new CatalogAttributeValueCreatedEvent(this, aNewValue);
            Iterator iter = theItemEventListener.iterator();
            while (iter.hasNext())
                 ((IItemEventListener) iter.next()).attributeValueCreated(ev);
        }
    }

    synchronized protected void dispatchAttributeValueDeletedEvent(CatalogAttributeValue aDeletedValue) {
        if (theItemEventListener != null) {
            CatalogAttributeValueDeletedEvent ev =
                new CatalogAttributeValueDeletedEvent(this, aDeletedValue);
            Iterator iter = theItemEventListener.iterator();
            while (iter.hasNext())
                 ((IItemEventListener) iter.next()).attributeValueDeleted(ev);
        }
    }

    /*************************** private ******************************************/

}
