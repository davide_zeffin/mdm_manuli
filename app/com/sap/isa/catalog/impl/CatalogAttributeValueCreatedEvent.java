/*****************************************************************************
  Copyright (c) 2000, SAPMarkets Europe GmbH, Germany, All rights reserved.
  Created:      09 July 2001

  $Id: //sap/ESALES_base/30_SP_COR/src/_pcatAPI/java/com/sap/isa/catalog/impl/CatalogAttributeValueCreatedEvent.java#3 $
  $Revision: #3 $
  $Change: 150872 $
  $DateTime: 2003/09/29 09:18:51 $
*****************************************************************************/
package com.sap.isa.catalog.impl;

import java.util.EventObject;

import com.sap.isa.catalog.boi.IAttributeValue;
import com.sap.isa.catalog.boi.IAttributeValueCreatedEvent;
import com.sap.isa.catalog.boi.IItem;

/**
 * Event that is fired if an attribute value is added to an item.
 *
 * @version     1.0
 */
public class CatalogAttributeValueCreatedEvent
    extends EventObject
    implements IAttributeValueCreatedEvent
{
    private CatalogAttributeValue theNewValue;

    public CatalogAttributeValueCreatedEvent(   CatalogItem aParent,
                                                CatalogAttributeValue aNewValue)
    {
        super(aParent);
        this.theNewValue=aNewValue;
    }

    public IItem getItem()
    {
        return (IItem) getSource();
    }

    public IAttributeValue getAttributeValue()
    {
        return theNewValue;
    }
}