/*****************************************************************************
  Copyright (c) 2000, SAPMarkets Europe GmbH, Germany, All rights reserved.
  Created:      09 July 2001

  $Id: //sap/ESALES_base/30_SP_COR/src/_pcatAPI/java/com/sap/isa/catalog/impl/CatalogComponentTypes.java#2 $
  $Revision: #2 $
  $Change: 129221 $
  $DateTime: 2003/05/15 17:20:58 $
*****************************************************************************/

package com.sap.isa.catalog.impl;

import com.sap.isa.catalog.boi.ComponentTypes;

/**
 * This interface defines the constants for the types of the components
 * of the catalog tree. The constants have to be used if a concrete type
 * is requested from a component via <code>queryInterface</code> method
 * (Extension Interface pattern).
 *
 *  @version     1.0
 */
public final class CatalogComponentTypes extends ComponentTypes {

    private CatalogComponentTypes(String name)
    {
        super(name);
    }

    public final static ComponentTypes CATALOG_ATTRIBUTE
        = new CatalogComponentTypes("CatalogAttribute");

    public final static ComponentTypes CATALOG_ATTRIBUTE_VALUE
        = new CatalogComponentTypes("CatalogAttributeValue");

    public final static ComponentTypes CATALOG
        = new CatalogComponentTypes("Catalog");

    public final static ComponentTypes CATALOG_DETAIL
        = new CatalogComponentTypes("CatalogDetail");

    public final static ComponentTypes CATALOG_CATEGORY
        = new CatalogComponentTypes("CatalogCategory");

    public final static ComponentTypes CATALOG_ITEM
        = new CatalogComponentTypes("CatalogItem");

    public final static ComponentTypes CATALOG_QUERY
        = new CatalogComponentTypes("CatalogQuery");

    public final static ComponentTypes COMPOSITE_COMPONENT
        = new CatalogComponentTypes("CompositeComponent");

    public final static ComponentTypes HIERARCHICAL_COMPONENT
        = new CatalogComponentTypes("HierarchicalComponent");

    public final static ComponentTypes CATALOG_QUERY_ITEM
        = new CatalogComponentTypes("CatalogQueryItem");

    public final static ComponentTypes CATALOG_QUERY_STATEMENT
        = new CatalogComponentTypes("CatalogQueryStatement");

    public final static ComponentTypes EDITABLE_ATTRIBUTE
        = new CatalogComponentTypes("EditableAttribute");

    public final static ComponentTypes EDITABLE_ATTRIBUTE_VALUE
        = new CatalogComponentTypes("EditableAttributeValue");

    public final static ComponentTypes EDITABLE_CATALOG
        = new CatalogComponentTypes("EditableCatalog");

    public final static ComponentTypes EDITABLE_CATEGORY
        = new CatalogComponentTypes("EditableCategory");

    public final static ComponentTypes EDITABLE_DETAIL
        = new CatalogComponentTypes("EditableDetail");

    public final static ComponentTypes EDITABLE_ITEM
        = new CatalogComponentTypes("EditableItem");
}