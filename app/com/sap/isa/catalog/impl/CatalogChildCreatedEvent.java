/*****************************************************************************
  Copyright (c) 2000, SAPMarkets Europe GmbH, Germany, All rights reserved.
  Created:      09 July 2001

  $Id: //sap/ESALES_base/30_SP_COR/src/_pcatAPI/java/com/sap/isa/catalog/impl/CatalogChildCreatedEvent.java#3 $
  $Revision: #3 $
  $Change: 150872 $
  $DateTime: 2003/09/29 09:18:51 $
*****************************************************************************/
package com.sap.isa.catalog.impl;

import java.util.EventObject;

import com.sap.isa.catalog.boi.IChildCreatedEvent;
import com.sap.isa.catalog.boi.IComponent;

/**
 * Event that is fired if an child (Item/Category) is added to an composite.
 *
 * @version     1.0
 */
public class CatalogChildCreatedEvent
    extends EventObject
    implements IChildCreatedEvent
{
    private CatalogComponent theChildCreated;

    public CatalogChildCreatedEvent(CatalogCompositeComponent aParent,
                                    CatalogHierarchicalComponent aNewChild)
    {
        super(aParent);
        this.theChildCreated=aNewChild;
    }

    public IComponent getComponent()
    {
        return (IComponent) getSource();
    }

    public IComponent getChild()
    {
        return theChildCreated;
    }
}