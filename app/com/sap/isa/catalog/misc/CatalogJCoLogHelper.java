package com.sap.isa.catalog.misc;

import com.sap.isa.backend.JCoHelper;
import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.core.logging.LogUtil;
import com.sap.mw.jco.IMetaData;
import com.sap.mw.jco.JCO;

/**
 * This class provides convience methods for the catalog, to log JCO calls and exceptions.
 *
 * @author SAP AG
 */
public class CatalogJCoLogHelper {
    
    /**
     * Constants to speficy, that the given Parameters are input or ouput parameters
     */
    protected static int INPUT = 0;
    protected static int OUTPUT = 1;
    protected static int INOUTPUT = 2;
    
    /**
     * Logs a RFC-call into the log file of the application.
     *
     * @param functionName the name of the JCo function that was executed
     * @param input input data for the function module. You may set this parameter to <code>null</code> if you don't
     *        have any data to be logged.
     * @param output output data for the function module. You may set this parameter to <code>null</code> if you don't
     *        have any data to be logged.
     * @param tablelist a list of tables to log
     * @param log the logging context to be used
     */
    public static void logCall(String functionName, JCO.Record input, JCO.Record output, JCO.ParameterList tablelist,
                               IsaLocation log) {
                                   
        boolean recordOK = true;

        if (input instanceof JCO.Table) {
            JCO.Table inputTable = (JCO.Table) input;

            recordOK = inputTable.getNumRows() <= 0;
        }

        if (input != null && recordOK == true) {
            StringBuffer in = new StringBuffer();

            in.append("::").append(functionName).append("::").append(" - IN: ").append(input.getName()).append(" * ");

            JCO.FieldIterator iterator = input.fields();

            while (iterator.hasMoreFields()) {
                JCO.Field field = iterator.nextField();

                in.append(field.getName()).append("='").append(field.getString()).append("' ");
            }

            log.debug(in.toString());
        }

        recordOK = true;

        if (output instanceof JCO.Table) {
            JCO.Table outputTable = (JCO.Table) output;

            recordOK = outputTable.getNumRows() <= 0;
        }

        if (output != null && recordOK == true) {
            StringBuffer out = new StringBuffer();

            out.append("::").append(functionName).append("::").append(" - OUT: ").append(output.getName()).append(" * ");

            JCO.FieldIterator iterator = output.fields();

            while (iterator.hasMoreFields()) {
                JCO.Field field = iterator.nextField();

                out.append(field.getName()).append("='").append(field.getString()).append("' ");
            }

            log.debug(out.toString());
        }

        if (tablelist != null) {
            for (int i = 0; i < tablelist.getCapacity(); i++) {
                if (tablelist.getTable(i) != null) {
                    logCall(functionName, tablelist.getTable(i), log, INOUTPUT);
                }
            }
        }
    }
    
    /**
     * Logs a RFC-call into the log file of the application as Input parameters.
     *
     * @param functionName the name of the JCo function that was executed
     * @param table input data for the function module. You may set this parameter to<code>null</code> if you don't
     *        have any data to be logged.
     * @param log the logging context to be used
     */
    public static void logCallIn(String functionName, JCO.Table table, IsaLocation log) {
        logCall(functionName, table, log, INPUT);
    }
    
    /**
     * Logs a RFC-call into the log file of the application as Input parameters.
     *
     * @param functionName the name of the JCo function that was executed
     * @param struct input data for the function module. You may set this parameter to<code>null</code> if you don't
     *        have any data to be logged.
     * @param log the logging context to be used
     */
    public static void logCallIn(String functionName, JCO.Structure struct, IsaLocation log) {
        logCall(functionName, struct, log, INPUT);
    }
    
    /**
     * Logs a RFC-call into the log file of the application as output parameters.
     *
     * @param functionName the name of the JCo function that was executed
     * @param table output data for the function module. You may set this parameter to<code>null</code> if you don't
     *        have any data to be logged.
     * @param log the logging context to be used
     */
    public static void logCallOut(String functionName, JCO.Table table, IsaLocation log) {
        logCall(functionName, table, log, OUTPUT);
    }
    
    /**
     * Logs a RFC-call into the log file of the application as output parameters.
     *
     * @param functionName the name of the JCo function that was executed
     * @param struct output data for the function module. You may set this parameter to<code>null</code> if you don't
     *        have any data to be logged.
     * @param log the logging context to be used
     */
    public static void logCallOut(String functionName, JCO.Structure struct, IsaLocation log) {
        logCall(functionName, struct, log, OUTPUT);
    }


    /**
     * Logs a RFC-call into the log file of the application.
     *
     * @param functionName the name of the JCo function that was executed
     * @param table input data for the function module. You may set this parameter to<code>null</code> if you don't
     *        have any data to be logged.
     * @param log the logging context to be used
     */
    protected static void logCall(String functionName, JCO.Table table, IsaLocation log, int inOut) {
        
        String praefix = " - IN: ";
        
        if (inOut == OUTPUT) {
            praefix = " - OUT: ";
        }
        else if (inOut == INOUTPUT) {
            praefix = " - IN/OUT: ";
        }
        
        if (table != null) {
            int rows = table.getNumRows();
            int cols = table.getNumColumns();
            String tableName = table.getName();
            
            if (rows < 1)  {
                log.debug( "::" + functionName+ "::"+ praefix + "Table " + tableName + " contains no rows" );
            }

            for (int i = 0; i < rows; i++) {
                StringBuffer in = new StringBuffer();

                in.append("::").append(functionName).append("::").append(praefix).append(tableName).append("[")
                  .append(i).append("] ");

                // add an additional space, just to be fancy
                if (i < 10) {
                    in.append("  ");
                }
                else if (i < 100) {
                    in.append(' ');
                }

                in.append("* ");

                table.setRow(i);

                for (int k = 0; k < cols; k++) {
                    in.append(table.getMetaData().getName(k)).append("='").append(table.getString(k)).append("' ");
                    
                    if (table.getMetaData().getType(k) == IMetaData.TYPE_BYTE) {
                        byte[] byteRep = table.getByteArray(k);
                        String strRep = new String(byteRep);
                        in.append("  byte[] as String ='").append(strRep).append("' ");
                    }
                }

                log.debug(in.toString());
            }

            table.firstRow();
        }
    }
    
    /**
     * Logs a RFC-call into the log file of the application.
     *
     * @param functionName the name of the JCo function that was executed
     * @param struct input data for the function module. You may set this parameter to<code>null</code> if you don't
     *        have any data to be logged.
     * @param log the logging context to be used
     */
    protected static void logCall(String functionName, JCO.Record struct, IsaLocation log, int inOut) {
        
        String praefix = " - IN: ";
        
        if (inOut == OUTPUT) {
            praefix = " - OUT: ";
        }
        else if (inOut == INOUTPUT) {
            praefix = " - IN/OUT: ";
        }
        
        if (struct != null) {
            
            StringBuffer in = new StringBuffer();

            in.append("::").append(functionName).append("::").append(praefix).append(struct.getName()).append(" * ");

            JCO.FieldIterator iterator = struct.fields();

            while (iterator.hasMoreFields()) {
                JCO.Field field = iterator.nextField();

                in.append(field.getName()).append("='").append(field.getString()).append("' ");
            }

            log.debug(in.toString());
        }
    }


    /**
     * Logs the given JCO exception with level ERROR 
     * 
     * @param functionName the name of the JCo function that was executed
     * @param ex The JCO.Exception to log
     * @param log the logging context to be used
     */
    public static void logException(String functionName, JCO.Exception ex, IsaLocation log) {
        String message = functionName + " - EXCEPTION: GROUP='" + getExceptionGroupAsString(ex) + "'" +
                         ", KEY='" + ex.getKey() + "'";

        log.error(LogUtil.APPS_COMMON_INFRASTRUCTURE, "b2b.exception.backend.logentry", new Object[] { message }, ex);

        log.debug(message);
    }
    
    /**
     * Returns the group of the JCOException as a String.
     *
     * @param jcoEx the JCO exception
     * @return string describing the group of the exception
     */
    public static String getExceptionGroupAsString(JCO.Exception jcoEx) {

        String groupString = null;

        switch (jcoEx.getGroup()) {

            case JCO.Exception.JCO_ERROR_COMMUNICATION:
                groupString = "JCO_ERROR_COMMUNICATION";
                break;

            case JCO.Exception.JCO_ERROR_LOGON_FAILURE:
                groupString = "JCO_ERROR_LOGON_FAILURE";
                break;

            case JCO.Exception.JCO_ERROR_SYSTEM_FAILURE:
                groupString = "JCO_ERROR_SYSTEM_FAILURE";
                break;

             case JCO.Exception.JCO_ERROR_SERVER_STARTUP:
                groupString = "JCO_ERROR_SERVER_STARTUP";
                break;

            case JCO.Exception.JCO_ERROR_ABAP_EXCEPTION:
                groupString = "JCO_ERROR_ABAP_EXCEPTION";
                break;

            case JCO.Exception.JCO_ERROR_APPLICATION_EXCEPTION:
                groupString = "JCO_ERROR_APPLICATION_EXCEPTION";
                break;

            case JCO.Exception.JCO_ERROR_CANCELLED:
                groupString = "JCO_ERROR_CANCELLED";
                break;

            case JCO.Exception.JCO_ERROR_ILLEGAL_TID:
                groupString = "JCO_ERROR_ILLEGAL_TID";
                break;

            case JCO.Exception.JCO_ERROR_INTERNAL:
                groupString = "JCO_ERROR_INTERNAL";
                break;

            case JCO.Exception.JCO_ERROR_NOT_SUPPORTED:
                groupString = "JCO_ERROR_NOT_SUPPORTED";
                break;

            case JCO.Exception.JCO_ERROR_PROGRAM:
                groupString = "JCO_ERROR_PROGRAM";
                break;

            case JCO.Exception.JCO_ERROR_PROTOCOL:
                groupString = "JCO_ERROR_PROTOCOL";
                break;

            case JCO.Exception.JCO_ERROR_RESOURCE:
                groupString = "JCO_ERROR_RESOURCE";
                break;

            case JCO.Exception.JCO_ERROR_STATE_BUSY:
                groupString = "JCO_ERROR_STATE_BUSY";
                break;

            case JCO.Exception.JCO_ERROR_CONVERSION:
                groupString = "JCO_ERROR_CONVERSION";
                break;

            case JCO.Exception.JCO_ERROR_FIELD_NOT_FOUND:
                groupString = "JCO_ERROR_FIELD_NOT_FOUND";
                break;

            case JCO.Exception.JCO_ERROR_FUNCTION_NOT_FOUND:
                groupString = "JCO_ERROR_FUNCTION_NOT_FOUND";
                break;

            case JCO.Exception.JCO_ERROR_NULL_HANDLE:
                groupString = "JCO_ERROR_NULL_HANDLE";
                break;

            case JCO.Exception.JCO_ERROR_UNSUPPORTED_CODEPAGE:
                groupString = "JCO_ERROR_UNSUPPORTED_CODEPAGE";
                break;

            case JCO.Exception.JCO_ERROR_XML_PARSER:
                groupString = "JCO_ERROR_XML_PARSER";
                break;

            default:
                groupString = "undefined (" + jcoEx.getGroup() + ")";
                break;
        }

        return groupString;
    }
    
    /**
     * Logs a RFC-call into the log file of the application.
     *
     * @param functionName the name of the JCo function that was executed
     * @param input input data for the function module. You may set this
     *        parameter to <code>null</code> if you don't have any data
     *        to be logged.
     * @param output output data for the function module. You may set this
     *        parameter to <code>null</code> if you don't have any data
     *        to be logged.
     * @param log the logging context to be used
     */
     public static void logCall(
         String functionName,
         JCoHelper.RecordWrapper input,
         JCoHelper.RecordWrapper output,
         IsaLocation log) {
         if ((input != null) && (output != null)) {
             logCall(functionName, input.getRecord(), output.getRecord(), null, log);
         }
         else if (input != null) {
             logCall(functionName, input.getRecord(), log, INPUT);
         }
         else if (output != null) {
             logCall(functionName, output.getRecord(), log, OUTPUT);
         }
     } 

}
