package com.sap.isa.catalog.uiclass;

import java.util.ArrayList;

import javax.servlet.jsp.PageContext;

import com.sap.isa.businessobject.webcatalog.CatalogBusinessObjectManager;
import com.sap.isa.catalog.webcatalog.WebCatArea;
import com.sap.isa.catalog.webcatalog.WebCatInfo;
import com.sap.isa.catalog.webcatalog.WebCatWeightedArea;

import com.sap.ecommerce.businessobject.MarketingBusinessObjectManager;
import com.sap.ecommerce.businessobject.loyalty.LoyaltyMembership;


/**
 * @author V.Manjunath Harish
 *
 * To change the template for this generated type comment go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
public class CategoriesUI extends CatalogBaseUI {

	protected String ExternalCatalogURL;
	protected int maxPos=0;
	protected WebCatInfo theCatalog;
	protected WebCatArea currentArea;
	protected ArrayList categoriesTreeList;
	protected boolean isCurrentArea = false;
    protected boolean showOciLink = false;
	protected String styleClass = "categ";
	protected String styleClassSelected = "categSelected";
	protected boolean flag=false;
	protected int count =0;
	
	
	/**
	 * @param pageContext
	 */
	public CategoriesUI(PageContext pageContext) {
		super(pageContext);
		if (userSessionData != null) {
							CatalogBusinessObjectManager bom =
											  (CatalogBusinessObjectManager)userSessionData.getBOM(CatalogBusinessObjectManager.CATALOG_BOM);
							this.theCatalog = bom.getCatalog();
		}
        
        showOciLink = catalogConfig.isOciAllowed();
	}
	
	public String getExternalCatalogURL() {
		return catalogConfig.getExternalCatalogURL();
	}
	
	public void setMaxPosition() {
		categoriesTreeList = new ArrayList();
		categoriesTreeList = (ArrayList)request.getAttribute("categoriesTree");
		for (int pos=0;pos<categoriesTreeList.size();pos++) {
			WebCatWeightedArea areaTemp = (WebCatWeightedArea)categoriesTreeList.get(pos);
			if(maxPos < areaTemp.getPosition()) {
				maxPos = areaTemp.getPosition();	
			}
		}
		setArrayList(categoriesTreeList);	
	}
	
	public void setArrayList(ArrayList categoriesTreeList) {
		request.setAttribute("categoriesTree",categoriesTreeList);
	}
	
	public int getMaxPosition() {
		return maxPos; 
	}
	
	public void setIsCurrentArea(boolean isCurrentArea) {
		this.isCurrentArea = isCurrentArea;
	}
	
	public boolean getIsCurrentArea() {
		return isCurrentArea;
	}
	
	public String getStyleClass() {
		if (isCurrentArea) {
			setIsCurrentArea(false);
			set_Selectflag(true);
			return styleClassSelected;
		}
		else {
			return styleClass;
		}	
	}
	
	public void set_Selectflag(boolean value) {
		this.flag = value;
	}
	
	public boolean get_selectflag() {
		return this.flag;
	}
	
	public void setCount() {
		count++;
	}
	
	public int getCount() {
		return count;
	}
	
	public String getAreaId() {
		return request.getParameter("select");
	}
	
	public WebCatArea getCurrentArea() {
		this.currentArea = theCatalog.getCurrentArea();
		return this.currentArea;
	}
	
	public boolean IsAdvisorEnabled() {
		return this.currentArea.getCatalog().isAdvisorEnabled();
	}
	
	public WebCatInfo getCatalog() {
		return this.theCatalog;
	}
    
    public boolean showOciLink() {
        log.entering("showOciLink()");
        log.exiting();
        return showOciLink;
    }
    
    public boolean showQuickSearchInCat() {
        log.entering("showQuickSearchInCat()");
        log.exiting();
        return catalogConfig.showCatQuickSearchInCat();
    }
    
	public boolean showLoyaltyLink(){
		log.entering("showLoyaltyLink");
		
		boolean showLoyaltyLink = false;
		
		if (catalogConfig.isEnabledLoyaltyProgram()) {
		  showLoyaltyLink = true;
		  
		  if (userSessionData != null) {
             
            MarketingBusinessObjectManager marketingBom = null;
            try {
                marketingBom = (MarketingBusinessObjectManager) userSessionData.getBOM(MarketingBusinessObjectManager.MARKETING_BOM);
            }
            catch (Exception e) {
                log.error("Exception occurred " +  e.getMessage());
            }
			
            
            if (marketingBom != null) {
                LoyaltyMembership loyaltyMembership = marketingBom.getLoyaltyMembership();
             
                if (loyaltyMembership != null && loyaltyMembership.exists()) {
                  showLoyaltyLink = false; 
                }
            }
            else {
                showLoyaltyLink = false;
            }
		  }	   	
		}
		
		log.exiting();
		return showLoyaltyLink;
	}
	
    public boolean showCamapignFields() {
        log.entering("showCamapignFields()");
        log.exiting();
        return catalogConfig.isManualCampainEntryAllowed() && !catalogConfig.isStandaloneCatalog();
    }
    
    /**
     * Returns the language dependend description of the loyalty program
     * @return loyProgDescr, description of the loyalty program as String
     */
    public String getLoyProgDescr() {
        log.entering("getLoyProgDescr()");
        String loyProgDescr = catalogConfig.getLoyProgDescr();
        
        log.exiting();
        return loyProgDescr;
    }

}