package com.sap.isa.catalog.uiclass;

import java.util.ArrayList;
import java.util.Iterator;

import javax.servlet.jsp.PageContext;

import com.sap.isa.catalog.boi.IAttribute;
import com.sap.isa.catalog.webcatalog.WebCatItem;
import com.sap.isa.catalog.webcatalog.WebCatItemPrice;
import com.sap.isa.core.logging.IsaLocation;

/**
 * To change the template for this generated type comment go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
public class CompareItemsISAUI extends ProductsUI {
	protected PageContext pageContext;
	protected IAttribute name;
    
    // Logging location
    public static IsaLocation log = IsaLocation.getInstance(CompareItemsISAUI.class.getName());

	/**
	 * @param pageContext
	 */
	public CompareItemsISAUI(PageContext pageContext) {
		super(pageContext);
        log.entering("CompareItemsISAUI constructor");
        log.exiting();
		this.pageContext = pageContext;		
	}
	
	public void setIAttribute(IAttribute name) {
        log.entering("setIAttribute:"+name);
        log.exiting();
		this.name = name;
	}
	
	public boolean IsIAttributeGetName() {
        log.entering("IsIAttributeGetName");
        log.exiting();
		return (!name.isBase() && name.getDescription() != null && name.getName().indexOf("_") != 0);
	}
    
    /**
     * Determine if we are in CompareCUA mode
     * 
     * @return true if comaprison is for CUA products
     *         false else
     */
     public boolean isCompareCUA() {
         
        log.entering("isCompareCUA");
        
        boolean response = false;

        if ((request.getParameter("compareCUA") != null && request.getParameter("compareCUA").equals("true")) ||
            (request.getAttribute("compareCUA") != null && request.getAttribute("compareCUA").equals("true"))) {
            response = true; 
        }
        
        if (log.isDebugEnabled()) {
            log.debug("isCompareCUA : value is " + response);
        }
        
        log.exiting();
        return response;
     }
	
	public String getIAttributeDescription() {
        log.entering("getIAttributeDescription");
        log.exiting();
		return this.name.getDescription();
	}
	
	public String getIAttributeName() {
        log.entering("getIAttributeName");
        log.exiting();
		return this.name.getName();
	}
	
	public String getIAttributeGuid() {
        log.entering("getIAttributeGuid");
        log.exiting();
		return this.name.getGuid();
	}
	
    /**
     * function called from compareItemsB2C. 
     * AddToBasket and AddToLeaflet should be displayed for package and non package. 
     * These buttons shouldn't be displayed for package components.
     * 
     * @param webCatItemList
     * @return
     */
     public boolean shouldAddButtonsBeDisplayed() {
        log.entering("shouldAddButtonsBeDisplayed");
        boolean response = true;
        ArrayList webCatItemList = (ArrayList)request.getAttribute("theItems");
        if (webCatItemList.size() > 0) {
           if (((WebCatItem)webCatItemList.get(0)).isSubComponent()) {
              response = false; 
           }
        }
        if (log.isDebugEnabled()) {
            log.debug("shouldAddButtonsBeDisplayed : value is " + response);
        }
        log.exiting();
        return response;
     }
    
    /**
     * returns true if the inline configuration should  be displayed
     */
     public boolean shouldInlineConfigurationBeDisplayed() {
        log.entering("shouldInlineConfigurationBeDisplayed");
        boolean response = false;
        ArrayList webCatItemList = (ArrayList)request.getAttribute("theItems");
        WebCatItem item = null;
        Iterator it = webCatItemList.iterator();
        while (it.hasNext()) {
           item = (WebCatItem)it.next();
           if (item.isConfigurable() == true) {
              response = true;
              break;
           }
        }
        if (log.isDebugEnabled()) {
            log.debug("shouldInlineConfigurationBeDisplayed : value is " + response);
        }
        log.exiting();
        return response;
     }
     
     /**
      * returns true if the contract duration should be displayed
      */
     public boolean shouldContractDurationBeDisplayed() {
         log.entering("shouldContractDurationBeDisplayed");
         boolean response = false;
         
         ArrayList webCatItemList = (ArrayList)request.getAttribute("theItems");
         WebCatItem item = null;
         Iterator it = webCatItemList.iterator();
         while (it.hasNext()) {
            item = (WebCatItem)it.next();
            if (item.getSelectedContractDuration() != null) {
                 response = true;
                 break;
             }
         }
         if (log.isDebugEnabled()) {
             log.debug("shouldContractDurationBeDisplayed : value is " + response);
         }
         log.exiting();
         return response;
     }
     
    /**
     * returns true if there are combined rate plan to compare. in this case we will display the rate plan combinations
     */ 
    public boolean shouldRatePlanCombinationsBeDisplayed() {
        log.entering("shouldRatePlanCombinationsBeCompared");
        boolean response = false;
        
        ArrayList webCatItemList = (ArrayList)request.getAttribute("theItems");
        WebCatItem item = null;
        Iterator it = webCatItemList.iterator();
        while (it.hasNext()) {
           item = (WebCatItem)it.next();
           if (item.isCombinedRatePlan()) {
              response = true;
              break;
           }
        }
        if (log.isDebugEnabled()) {
            log.debug("shouldRatePlanCombinationsBeDisplayed : value is " + response);
        }
        log.exiting();
        return response;
    }
    
    /**
     * returns the list of the rate plan combinations to display
     */
    public Iterator getRatePlanCombinations(WebCatItem item) {
        log.entering("getRatePlanCombinations");
        ArrayList response = new ArrayList();
        
        String parentTechKey = item.getTechKey().getIdAsString();
        if (item.getParentItem()!= null && item.getParentItem().getWebCatSubItemList() != null) {
           Iterator it = item.getParentItem().getWebCatSubItemList().iterator();
           if (it != null){
             while (it.hasNext()) {
               item = (WebCatItem)it.next();
               if (item.getParentTechKey().getIdAsString().equals(parentTechKey)) {
                   if (log.isDebugEnabled()) {
                       log.debug("add item:" + item.getDescription());
                   }
                   response.add(item);
               }
             }
           }
        }
        log.exiting();
        return response.iterator();
    }
     
     /**
      * returns true if one of the item is a package with no parent id or if the item is a rate plan combination
      */
     public int getPriceCategoryToDisplay(WebCatItem item) {
         log.entering("getPriceCategoryToDisplay");      
         int response = WebCatItemPrice.SHOW_NO_SUMS;
         
         if (item.isRelevantForExplosion() && item.getParentItem() == null && item.hasSubItems()) {
             response = WebCatItemPrice.SHOW_SUMS_ONLY;
         }
                  
         if (log.isDebugEnabled()) {
             log.debug("getPriceCategoryToDisplay : value is " + response);
         }        
         log.exiting();
         return response;
     }
     
    public String[] getUnitsOfMeasurement(){
        log.entering("getUnisOfMeasurement");
        log.exiting();
        return getContractItem().getUnitsOfMeasurement();
    }
    
    public String getContractItemUnit() {
        log.entering("getContractItemUnit");
        log.exiting();
        return getContractItem().getUnit().trim();
    }
}