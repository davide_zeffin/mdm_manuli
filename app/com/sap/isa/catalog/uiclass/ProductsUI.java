package com.sap.isa.catalog.uiclass;

import java.util.ArrayList;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.regex.Pattern;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.jsp.PageContext;

import com.sap.isa.backend.boi.isacore.contract.ContractAttributeListData;
import com.sap.isa.backend.boi.webcatalog.CatalogBusinessObjectsAware;
import com.sap.isa.backend.boi.webcatalog.pricing.PriceCalculatorBackend;
import com.sap.isa.backend.boi.webcatalog.pricing.PriceCalculatorInitData;
import com.sap.isa.businessobject.marketing.CUAList;
import com.sap.isa.businessobject.webcatalog.pricing.PriceCalculator;
import com.sap.isa.catalog.actions.ActionConstants;
import com.sap.isa.catalog.boi.IQuery;
import com.sap.isa.catalog.boi.IQueryStatement;
import com.sap.isa.catalog.filter.CatalogFilter;
import com.sap.isa.catalog.impl.CatalogQueryStatement;
import com.sap.isa.catalog.webcatalog.ContractDisplayMode;
import com.sap.isa.catalog.webcatalog.WebCatArea;
import com.sap.isa.catalog.webcatalog.WebCatAreaQuery;
import com.sap.isa.catalog.webcatalog.WebCatItem;
import com.sap.isa.catalog.webcatalog.WebCatItemList;
import com.sap.isa.catalog.webcatalog.WebCatItemPage;
import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.core.util.JspUtil;
import com.sap.isa.core.util.Message;
import com.sap.isa.core.util.MessageList;
import com.sap.isa.core.util.WebUtil;
import com.sap.isa.core.util.table.ResultData;
import com.sap.spc.remote.client.object.IPCItem;
import com.sap.spc.remote.client.object.IPCItemReference;
import com.sap.spc.remote.client.object.imp.rfc.RFCIPCItemReference;

/**
 * DOCUMENT ME!
 *
 * @author V.Manjunath Harish To change the template for this generated type comment go to
 *         Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
public class ProductsUI extends ProductsISAHeaderUI {
    protected int spanSize;
    protected int pageSize;
    protected boolean toPopUp;
    protected boolean notEnoughProducts;
    protected boolean notEnoughProductsForTransfer;
    protected int noOfContractItems;
    protected ArrayList contractRefs;
    protected String isProductList;
    protected String cuaType;
    protected CatalogBusinessObjectsAware catObjBm;
    protected WebCatItem webCatItem;
    protected WebCatItem subItem;
    protected IPCItem ipcItem;
    protected CUAList cuaList;
    protected IPCItemReference ipcItemRef;
    protected ContractDisplayMode contractDisplayMode;
    protected ContractAttributeListData contractAttributeList;
    protected String contrAttrDesc[];
    protected PriceCalculatorInitData priceCalcInitData;
    protected WebCatItemPage itemPage = null;
    protected WebCatItemList searchTermsItemList;
    protected boolean IsContractsAvailable;
    protected boolean showConfLinkCol;
    protected boolean showContracts = false;
    
    // Logging location
    public static IsaLocation log = IsaLocation.getInstance(ProductsUI.class.getName());

    
    /**
     * DOCUMENT ME!
     *
     * @param pageContext
     */
    public ProductsUI(PageContext pageContext) {

        super(pageContext);
        
        log.entering("ProductsUI(PageContext pageContext)");
        contractDisplayMode = theCatalog.getContractDisplayMode();
        // obsolete : contractAttributeList is now read from the webcat item list
        //contractAttributeList = (ContractAttributeListData) request.getAttribute("refNames");
		if (theCatalog.getCurrentItemList() != null) {
			contractAttributeList = theCatalog.getCurrentItemList().getContractAttributeList();
		}
        PriceCalculator priceCalc = theCatalog.getPriceCalculator();
        if(priceCalc != null) {
           priceCalcInitData = theCatalog.getPriceCalculator().getInitData();
        }
        else {
            priceCalcInitData = null;
        }
        isProductList = getValueFromRequest(ActionConstants.RA_IS_PRODUCT_LIST);
        
        // this is a special case. here we have to 
        if(isRecommendations() || isBestSeller()) {
        	
        }
        cuaType = getValueFromRequest("cuaType");
        if(this.cuaType.equals("")) {
        	this.cuaType = theCatalog.getCuaType();
        	if(this.cuaType == null) {
        		this.cuaType = "";
        	}
        }
        cuaList = (CUAList) request.getAttribute("cuaList");
        if(this.cuaList == null) {
        	this.cuaList = (CUAList) theCatalog.getCuaList();
        	if(this.cuaList != null && 
        	   isProductList.equals("") && 
        	   getDisplayScenario().equals(ActionConstants.RA_CURRENT_QUERY) && 
        	   getDetailScenario().equals(ActionConstants.DS_CUA) ) {
        		isProductList = ActionConstants.DS_CUA;
//        		if (detailScenario.equals("")) {
//        			detailScenario = ActionConstants.DS_CUA;
//        		}
        	}
        }
	        
        attributesList = (ArrayList) request.getAttribute("attributesList");
        if(!isCUAList()) {
        	// take the current item list, if the products from area/bestseller/recommendations should be shown
        	searchTermsItemList = theCatalog.getCurrentItemList();
        } else {
        	// take the CUA list if a marketing list is used.
        	searchTermsItemList = theCatalog.getCurrentCUAList();
        }

        if (searchTermsItemList != null) {
            itemPage =  searchTermsItemList.getCurrentItemPage();
            if(itemPage == null) {
            	this.itemPage = (WebCatItemPage) request.getAttribute(ActionConstants.RA_PAGE_ITEMS);
            }
        } else {
        	WebCatItemList tempList = (WebCatItemList) request.getAttribute(ActionConstants.RA_ITEMLIST_EBP);
        	if(tempList != null) {
        		itemPage = tempList.getCurrentItemPage();
        	}
        }
        
        showConfLinkCol = true;
        if (itemPage != null) {
            showConfLinkCol = false;
            for (int i=0; i < itemPage.size() && showConfLinkCol == false; i++) {
                showConfLinkCol = itemPage.getItem(i).isConfigurable();
            }
        }
        
        if (contractAttributeList != null) {
            contrAttrDesc = new String[contractAttributeList.size()];

            int i = 0;

            for (Iterator iter = contractAttributeList.iterator(); iter.hasNext(); i++) {
                contrAttrDesc[i] = ((com.sap.isa.businessobject.contract.ContractAttribute) iter.next()).getDescription();
            }
        }
        
        showContracts = catalogConfig.showCatContracts() && IsContracts();
        
        scalePriceDivId = 0;
        
        log.exiting();
    }

    /**
     * DOCUMENT ME!
     *
     * @return DOCUMENT ME!
     */
    public WebCatItemPage getItemPage() {
        log.entering("getItemPage()");
        log.exiting();
            return itemPage;
    }
    

    
    /**
     * Returns true, if contract data should be displayed
     * 
     * @return true   if contract data should be displayed,
     *         false  otherwise
     */
    public boolean showContracts() {
        log.entering("showContracts");

        if (log.isDebugEnabled()) {
            log.debug("showContracts" + showContracts);
        }
        log.exiting();
        
        return showContracts;
    }
    
   /**
    * Returns true if ther configurable items in the current itemPage
    * or the itemPage is not set
    */
    public boolean showConfLinkCol() {
        log.entering("showConfLinkCol()");
        log.exiting();
        return showConfLinkCol;
    }

    /**
     * DOCUMENT ME!
     *
     * @return DOCUMENT ME!
     */
    public WebCatAreaQuery getAreaQuery() {
        log.entering("getAreaQuery()");
        log.exiting();
        return getItemPage().getItemList().getAreaQuery();
    }

    /**
     * DOCUMENT ME!
     *
     * @param PageSize DOCUMENT ME!
     */
    public void setPageSize(int PageSize) {
        log.entering("setPageSize()");
        this.pageSize = PageSize;
        log.exiting();
    }

    /**
     * Returns true, if the price analysis link for the item should be dsiplayed
     *
     * @param webCatItem the item to check
     *
     * @return true   if the price analysis link should be shown false  otherwise
     */
    public boolean showPriceAnalysisLink(WebCatItem webCatItem) {

        log.entering("showPriceAnalysisLink()");

        boolean showAnalysisLink = catalogConfig.isPricingCondsAvailable() && getIPCItemRef(webCatItem) != null;

        if (log.isDebugEnabled()) {
            log.debug("test if price analysis link should be shown for the webCatItem "  + webCatItem.getProduct()
                      + " : "  + showAnalysisLink);
        }
                
        log.exiting();

        return showAnalysisLink;
    }

    /**
     * Returns true, RCFItemReference for th WebCatItems priceInfo object, if present
     *
     * @param webCatItem the item to determine the IPCItemRefernce
     *
     * @return RFCIPCItemReference of the WebtCatItems priceInfo object, if available null, if not
     */
    public RFCIPCItemReference getIPCItemRef(WebCatItem webCatItem) {
        
        log.entering("getIPCItemRef()");

        RFCIPCItemReference ipcItemRef = null;

        if (log.isDebugEnabled()) {
        log.debug("determine IPCPItemRefernce for webCatItem " + webCatItem.getProduct());
        }

        IPCItem priceRef = null;

        if (webCatItem.getItemPrice().getPriceInfo() != null) {
            priceRef = (IPCItem) webCatItem.getItemPrice().getPriceInfo().getPricingItemReference();
        }

        if (priceRef != null) {
            ipcItemRef = (RFCIPCItemReference) priceRef.getItemReference();
        }

        if (log.isDebugEnabled()) {
            log.debug("determine IPCPItemRefernce for webCatItem " + webCatItem.getProduct() + " : " + ipcItemRef);
        }
        log.exiting();

        return ipcItemRef;
    }

    /**
     * Returns true, if the item is a configured product variant
     *
     * @param webCatItem the item to check
     *
     * @return true   if the item is a configured product variant false  otherwise
     */
    public boolean showProductVariantInfo(WebCatItem webCatItem) {
        log.entering("showProductVariantInfo");
        boolean showVariantinfo = webCatItem.getConfigItemReference() != null && 
                                  !webCatItem.getProductID().equals(webCatItem.getConfigItemReference().getProductGUID());

        if (log.isDebugEnabled()) {
            log.debug("test if the webCatItem is a configured product variant" + webCatItem.getProduct() + " : " + showVariantinfo);
        }
        log.exiting();
        return showVariantinfo;
    }

    /**
     * Returns the title to be displayed for the current document
     *
     * @return String   title to be displayed for the document
     */
    public String getDocumentTitle() {
        
        log.entering("getDocumentTitle()");
        
        String listTitleArg0 = null;

        log.debug("Determing title for document");

        if (isCatalogQuery()) {
            String args[] = { getCurrentQuery() };
            listTitleArg0 = WebUtil.translate(pageContext, "catalog.isa.queryFor", args);

            log.debug("Arguments for title taken from query");
        }
        else if (getCurrentArea() != null && getCurrentArea().getAreaName() != null) {
            String args[] = { JspUtil.encodeHtml(getCurrentArea().getAreaName()) };
            listTitleArg0 = WebUtil.translate(pageContext, "b2c.cat.b2ccatarea", args);

            log.debug("Arguments for title taken from area");
        }

        String listTitleArgs[] = { listTitleArg0 };

        String pageTitle = WebUtil.translate(pageContext, "catalog.isa.productListTitle", listTitleArgs);

        if (log.isDebugEnabled()) {
        log.debug("title for document: " + pageTitle);
        }

        log.exiting();

        return pageTitle;
    }

    /**
     * public int getPageSize()     return getItempage().pageSize();
     *
     * @return DOCUMENT ME!
     */
    public int getPageSize() {
        log.entering("getPageSize()");
        log.exiting();
        return this.pageSize;
    }

    /**
     * DOCUMENT ME!
     *
     * @return DOCUMENT ME!
     */
    public int getPage() {
        log.entering("getPage()");
        log.exiting();
        return itemPage.getPage();
    }

    /**
     * DOCUMENT ME!
     *
     * @return DOCUMENT ME!
     */
    public int getSize() {
        log.entering("getSize()");
        int size = 0;
        if (itemPage != null) {
            size = itemPage.size();
        }

        if (log.isDebugEnabled()) {
            log.debug("size = " + size);
        }
        log.exiting();
        return size;
    }
    
    /**
     * DOCUMENT ME!
     *
     * @return DOCUMENT ME!
     */
    public String getColNo() {
        
        log.entering("getColNo()");
        
        String colno = "4";

        if (IsContracts()) {
            colno = "5";
        }

        if (isStaticDynamicPricing()) {
            colno = "6";
        }

        if (log.isDebugEnabled()) {
            log.debug("getColNo()=" + colno);
        }
        log.exiting();

        return colno;
    }

    /**
     * DOCUMENT ME!
     *
     * @return DOCUMENT ME!
     */
    public String getOverviewColSpan() {
        log.entering("getOverviewColSpan()");
        log.exiting();
        return (catalogConfig.isEAuctionUsed()) ? "4" : "3";
    }

    /**
     * DOCUMENT ME!
     *
     * @return DOCUMENT ME!
     */
    public int getNoOfContractItems() {
        
        log.entering("getNoOfContractItems()");
        
        if (IsContractsAvailable()) {
            noOfContractItems = 1;
        }
        else {
            noOfContractItems = getContractItemsArray().size() + 1;
        }

        if (log.isDebugEnabled()) {
            log.debug("getNoOfContractItems()=" + noOfContractItems);
        }
        log.exiting();

        return noOfContractItems;
    }

    /**
     * DOCUMENT ME!
     *
     * @return DOCUMENT ME!
     */
    public boolean IsContractsAvailable() {
        log.entering("IsContractsAvailable()");

        this.IsContractsAvailable = ((getContractItemsArray() == null) || (getContractItemsArray().size() == 0));

        log.exiting();
        return this.IsContractsAvailable;
    }

    /**
     * Check is the request Attribute RA_PRODUCT_LIST_TITLE_ARG0 is set
     *
     * @return true if the argument is set  false otherwise
     */
    public boolean isTitleArgSet() {
        log.entering("isTitleArgSet()");

        // get argument for the title
        boolean isTitleSet = null != request.getAttribute(ActionConstants.RA_PRODUCT_LIST_TITLE_ARG0);

        if (log.isDebugEnabled()) {
            log.debug("Check if RA_PRODUCT_LIST_TITLE_ARG0 is set : " + isTitleSet);
        }

        log.exiting();
        
        return isTitleSet;
    }

    /**
     * DOCUMENT ME!
     *
     * @return DOCUMENT ME!
     */
    public ArrayList getContractItemsArray() {
        log.entering("getContractItemsArray()");

        this.contractRefs = this.webCatItem.getContractItemsArray();

        log.exiting();
        return contractRefs;
    }
    
	/**
	 * this function returns the amount of associated contract items
	 * 
	 * @return the amount of associated contract items
	 */
	public int getContractItemsAmount() {
		log.entering("getContractItemsAmount()");
		
		int amount = 0; 

		if(this.webCatItem.getContractItemsArray() != null){
			amount = this.webCatItem.getContractItemsArray().size();
		}

		if(log.isDebugEnabled()){
			log.debug("associated contract items amount:" + amount);
		}
		log.exiting();
		return amount;
	}

    /**
     * DOCUMENT ME!
     *
     * @return DOCUMENT ME!
     */
    public Iterator getContrAttribListIterator() {
        log.entering("getContrAttribListIterator()");
        log.exiting();
        return contractAttributeList.iterator();
    }

    /**
     * DOCUMENT ME!
     *
     * @return DOCUMENT ME!
     */
    public String[] getContrAttrDescs() {
        log.entering("getContrAttrDescs()");
        log.exiting();
        return contrAttrDesc;
    }

    /**
     * DOCUMENT ME!
     *
     * @return DOCUMENT ME!
     */
    public int getContrAttrDescsSize() {
        log.entering("getContrAttrDescsSize()");
        log.exiting();
        int size = 0;
        if (contrAttrDesc != null) {
            size = contrAttrDesc.length;
        }
        return size;
    }

    /**
     * DOCUMENT ME!
     *
     * @param i DOCUMENT ME!
     *
     * @return DOCUMENT ME!
     */
    public String getContrAttrDesc(int i) {
        log.entering("getContrAttrDesc(int i)");
        log.exiting();
        return contrAttrDesc[i];
    }

    /**
     * DOCUMENT ME!
     *
     * @return DOCUMENT ME!
     */
    public int getContrColSpanSize() {
        
        log.entering("getContrColSpanSize()");
        
        if (contractDisplayMode.showNoContracts()) {
            spanSize = 0;
        }
        else {
            spanSize = contractAttributeList.size();
        }

        if (log.isDebugEnabled()) {
            log.debug("getContrColSpanSize()=" + spanSize);
        }
        log.exiting();

        return spanSize;
    }

    /**
     * return the maximal number of columns
     *
     * @return maxCols as int
     */
    public int getMaxCols() {
       int maxCols = 8;
       
       // getOverviewColSpan()
       if (catalogConfig.isEAuctionUsed()) {
           maxCols += 1;
       }

       if (IsContracts()) {
           maxCols += getContrColSpanSize() + 2;
       }
       if (showConfLinkCol()) {
           maxCols += 1;
       }
       return maxCols;
    }
    
    /**
     * return the messages for an WebCatItem
     *
     * @return messages as String
     */
    public String getMessage(WebCatItem item) {
       String message = null;
       MessageList messageList = null;
       if( ( messageList = item.getMessageList()) != null) {
           Iterator it = messageList.iterator();
           while(it.hasNext()){
              Message msg = (Message)it.next();
              if(message != null){
                 message = message +", "+msg.getDescription();
              }
              else {
                 message = msg.getDescription();
              }
            }
            item.clearMessages();
        }
        return message;
    }
    
    /**
     * return the key for Bestseller, Recommendations or CUA
     *
     * @return Key for Bestseller, Recommendations or CUA
     */
    public String getProdListKey() {
        
        log.entering("getProdListKey()");
        
        String key = null;

        if (IsProductList() != null) {
            if (isBestSeller()) {
                key = "bestseller.jsp.header";
            }
            else if (isRecommendations()) {
                key = "recommendation.jsp.header";
            }
            else if (isCUAList()) {
                if (isCUAProductId()) {
                    if (isAccessories()) {
                        key = "cua.jsp.accessory";
                    }
                    else if (isRelatedProducts()) {
                        key = "cua.jsp.crossselling";
                    }
                    else if (isAlternatives()) {
                        key = "cua.jsp.upselling";
                    }
                    else {
                        key = "cua.jsp.header";
                    }
                }
            }
        }

        if (log.isDebugEnabled()) {
        log.debug("getProdListKey()=" + key);
        }
        log.exiting();

        return key;
    }

    /**
     * DOCUMENT ME!
     *
     * @param subItem DOCUMENT ME!
     */
    public void setSubItem(WebCatItem subItem) {
        log.entering("setSubItem()");
        this.subItem = subItem;
        log.exiting();
    }

    /**
     * Returns the current subItem.
     *
     * @return subItem that has been set with <code>setSubItem</code>.
     */
    public WebCatItem getSubItem() {
        return this.subItem;
    }
    
    /**
     * DOCUMENT ME!
     *
     * @return DOCUMENT ME!
     */
    public WebCatItem getContractItem() {
        log.entering("getContractItem()");
        log.exiting();
        return (this.subItem);
    }

    /**
     * DOCUMENT ME!
     *
     * @return DOCUMENT ME!
     */
    public String getItemId() {
        log.entering("getItemId()");
        log.exiting();
        return this.webCatItem.getItemID();
    }

    /**
     * DOCUMENT ME!
     *
     * @return DOCUMENT ME!
     */
    public String getContractkey() {
        log.entering("getContractkey()");
        log.exiting();
        return this.getContractItem().getContractRef().getContractKey().toString();
    }

    /**
     * DOCUMENT ME!
     *
     * @return DOCUMENT ME!
     */
    public String getContractItemKey() {
        log.entering("getContractItemKey()");
        log.exiting();
        return this.getContractItem().getContractRef().getItemKey().toString();
    }

    /**
     * DOCUMENT ME!
     *
     * @return DOCUMENT ME!
     */
    public String getQuantity() {
        log.entering("getQuantity()");
        log.exiting();
        return this.getContractItem().getQuantity();
    }

    /**
     * DOCUMENT ME!
     *
     * @return DOCUMENT ME!
     */
    public String getContractId() {
        log.entering("getContractId()");
        log.exiting();
        return this.getContractItem().getContractRef().getContractId();
    }

    /**
     * DOCUMENT ME!
     *
     * @return DOCUMENT ME!
     */
    public String getContractItemId() {
        log.entering("getContractItemId()");
        log.exiting();
        return this.getContractItem().getContractRef().getItemID();
    }

    /**
     * DOCUMENT ME!
     *
     * @param var DOCUMENT ME!
     *
     * @return DOCUMENT ME!
     */
    public String getContractItemValue(int var) {
        log.entering("getContractItemValue()");
        log.exiting();
        return this.getContractItem().getContractRef().getAttributeValues().get(var).getValue();
    }

    /**
     * DOCUMENT ME!
     *
     * @param var DOCUMENT ME!
     *
     * @return DOCUMENT ME!
     */
    public String getContractUnitValue(int var) {
        log.entering("getContractUnitValue()");
        log.exiting();
        return this.getContractItem().getContractRef().getAttributeValues().get(var).getUnitValue();
    }

    /**
     * Checks in the request if the customer choosed to compare items.
     * It is checking the request for the parameter or attribute <b>popUpCompare</b>
     * @return <code>true</code> if a pop up should be displayed or not. In that case the value is <code>false</code>.
     */
    public boolean getPopUpCompare() {
        log.entering("getPopUpCompare()");

        toPopUp = this.getValueFromRequest("popUpCompare").equals("yes");

        if (log.isDebugEnabled()) {
            log.debug("getPopUpCompare()=" + toPopUp);
        }
        log.exiting();
        return toPopUp;
    }

    /**
     * This method is called if the customer did not select enough items to be
     * compared. at least two items are required.
     *
     * @return <code>true</code> if not enough products were selected, otherwise <code>false</code>.
     */
    public boolean getnotEnoughProducts() {
        
        log.entering("getnotEnoughProducts()");
        
        notEnoughProducts = this.getValueFromRequest("notEnoughForCompare").equals("yes");

        if (log.isDebugEnabled()) {
            log.debug("getnotEnoughProducts()=" + notEnoughProducts);
        }
        log.exiting();

        return notEnoughProducts;
    }

    /**
     * This method is called if the customer did not select enough items to be
     * compared. at least two items are required.
     *
     * @return <code>true</code> if not enough products were selected, otherwise <code>false</code>.
     */
    public boolean getnotEnoughProductsForTransfer() {
        
        log.entering("getnotEnoughProductsForTransfer()");
        
        notEnoughProductsForTransfer = this.getValueFromRequest("notEnoughForTransfer").equals("yes");

        if (log.isDebugEnabled()) {
            log.debug("getnotEnoughProductsForTransfer()=" + notEnoughProductsForTransfer);
        }
        log.exiting();

        return notEnoughProductsForTransfer;
    }
    
    /**
     * DOCUMENT ME!
     *
     * @return DOCUMENT ME!
     */
    public boolean isBestSeller() {
        log.entering("isBestSeller()");
        log.exiting();
        return (isProductList != null && isProductList.equals("bestseller"));
    }

    /**
     * DOCUMENT ME!
     *
     * @return DOCUMENT ME!
     */
    public boolean isGoodScenarios() {
        
        log.entering("isGoodScenarios()");
        
        boolean goodScenarios = false;
        String detailScenario = null;

        if ((detailScenario = getDetailScenario()) != null) {
            if (detailScenario.equals(ActionConstants.DS_BESTSELLER)
                || detailScenario.equals(ActionConstants.DS_RECOMMENDATION)
                || detailScenario.equals("cuaTable")) {
                goodScenarios = true;
            }
        }

        if (log.isDebugEnabled()) {
            log.debug("isGoodScenarios()=" + goodScenarios);
        }
        log.exiting();
        
        return goodScenarios;
    }

    /**
     * Returns true, if the webCatItem is old fashion configurable
     *
     * @param webCatItem the item to check
     *
     * @return true   if the item is old fashion configurable false  otherwise
     */
    public boolean isItemOldFashionConfigurable(WebCatItem webCatItem) {
        
        log.entering("isItemOldFashionConfigurable()");
        
        boolean oldFashionConfigurable = false;

        if (log.isDebugEnabled()) {
            log.debug("test webCatItem " + webCatItem.getProduct() + " for oldFashionConfigurable");
        }

        String configUseIPC = webCatItem.getAttribute("CONFIG_USE_IPC");
        String configurableFlag = webCatItem.getAttribute("PRODUCT_CONFIGURABLE_FLAG");

        /*it is an "old fashion" config*/
        if (configUseIPC != null
            && configUseIPC.equals("X")
            && webCatItem.getConfigItemReference() != null
            && configurableFlag != null
            && (configurableFlag.equals("A") || configurableFlag.equals("X") || configurableFlag.equals("C"))) {
            oldFashionConfigurable = true;
        }

        if (log.isDebugEnabled()) {
            log.debug("test webCatItem " + webCatItem.getProduct() + " for oldFashionConfigurable result: " + oldFashionConfigurable);
        }

        log.exiting();
        
        return oldFashionConfigurable;
    }

    /**
     * Returns true, if the webCatItem is a product variant
     *
     * @param webCatItem the item to check
     *
     * @return true   if the item is a product variant false  otherwise
     */
    public boolean isItemProductVariant(WebCatItem webCatItem) {
        
        log.entering("isItemProductVariant()");

        boolean isVariant = false;

        if (log.isDebugEnabled()) {
            log.debug("test if webCatItem " + webCatItem.getProduct() + " is product variant");
        }

        String productVariantFlag = webCatItem.getAttribute("PRODUCT_VARIANT_FLAG");

        /*it is an "old fashion" config*/
        if (productVariantFlag != null && !productVariantFlag.equals("") && webCatItem.getConfigItemReference() != null) {
            isVariant = true;
        }

        if (log.isDebugEnabled()) {
            log.debug("test if webCatItem " + webCatItem.getProduct() + " is product variant: " + isVariant);
        }

        log.exiting();

        return isVariant;
    }

    /**
     * DOCUMENT ME!
     *
     * @return DOCUMENT ME!
     */
    public boolean isRecommendations() {
        log.entering("isRecommendations()");
        log.exiting();
        return (isProductList != null && isProductList.equals("recommendations"));
    }

    /**
     * DOCUMENT ME!
     *
     * @return DOCUMENT ME!
     */
    public boolean isCUAList() {
        log.entering("isCUAList()");
        log.exiting();
        return  ( (isProductList != null && isProductList.equals("cuaList")) || cuaList != null);
    }

    /**
     * DOCUMENT ME!
     *
     * @return DOCUMENT ME!
     */
    public boolean isAccessories() {
        log.entering("isAccessories()");
        log.exiting();
        return (cuaType != null && cuaType.equals("A"));
    }

    /**
     * DOCUMENT ME!
     *
     * @return DOCUMENT ME!
     */
    public boolean isRelatedProducts() {
        log.entering("isRelatedProducts()");
        log.exiting();
        return (cuaType != null && cuaType.equals("C"));
    }

    /**
     * DOCUMENT ME!
     *
     * @return DOCUMENT ME!
     */
    public boolean isAlternatives() {
        log.entering("isAlternatives()");
        log.exiting();
        return (cuaType != null && cuaType.equals("U"));
    }

    /**
     * The method checks if the items to display on the page are Alternatives, 
     * Related Products, Accessories or Recommendations. Also if this is a CUA List. 
     * @return <code>true</code> if items mathes any type above, otherwise <code>false</code> will be returned.
     */
    public boolean isMarketingPage() {
    	return isAlternatives() || isRelatedProducts() || isAccessories() || isCUAList() || isRecommendations() || isBestSeller();
    }
    
    /**
     * DOCUMENT ME!
     *
     * @return DOCUMENT ME!
     */
    public String getCuaProductId() {
        log.entering("getCuaProductId()");
        log.exiting();
        return (cuaType != null) ? cuaList.getProductId(): null;
    }

    /**
     * DOCUMENT ME!
     *
     * @return DOCUMENT ME!
     */
    public boolean IsContracts() {
        log.entering("IsContracts()");
        log.exiting();
        return contractDisplayMode.showContracts();
    }

    /**
     * DOCUMENT ME!
     *
     * @return DOCUMENT ME!
     * @deprecated This method should not be used anymore. Instead use the new flags in the Prices to 
     *             check for special prices etc.
     */
    public boolean isStaticDynamicPricing() {
        log.entering("isStaticDynamicPricing()");
        if(priceCalcInitData != null) {
            log.exiting();
            return (priceCalcInitData.getStrategy() == PriceCalculatorBackend.STATIC_DYNAMIC_PRICING);
        }
        else {
            log.error("priceCalcInitData is null");
            log.exiting();
            return false;
        }
        
    }

    /**
     * DOCUMENT ME!
     *
     * @param webCatItem DOCUMENT ME!
     */
    public void setWebCatItem(WebCatItem webCatItem) {
        log.entering("setWebCatItem()");
        this.webCatItem = webCatItem;
        log.exiting();
    }

    /**
     * DOCUMENT ME!
     *
     * @return DOCUMENT ME!
     */
    public boolean isPriceAnalysisEnabled() {
        log.entering("isPriceAnalysisEnabled()");
        if(priceCalcInitData != null) {
            log.exiting();
            return (priceCalcInitData.isPriceAnalysisEnabled() && (this.ipcItem != null));
        }
        else {
            log.error("priceCalcInitData is null");
            log.exiting();
            return false;
        }
        
    }

    /**
     * DOCUMENT ME!
     */
    public void setPriceReference() {
        
        log.entering("setPriceReference()");
        
        if (webCatItem.getItemPrice().getPriceInfo() == null) {
            ipcItem = null;
        }
        else {
            ipcItem = (IPCItem) webCatItem.getItemPrice().getPriceInfo().getPricingItemReference();
        }

        if (priceCalcInitData != null && priceCalcInitData.isPriceAnalysisEnabled() && (ipcItem != null)) {
            ipcItemRef = ipcItem.getItemReference();
        }
        
        log.exiting();
    }

    /**
     * DOCUMENT ME!
     *
     * @return DOCUMENT ME!
     */
    public IPCItemReference getPriceReference() {
        log.entering("getPriceReference()");
        log.exiting();
        return ipcItemRef;
    }

    /**
     * DOCUMENT ME!
     *
     * @return DOCUMENT ME!
     */
    public String getIsQuery() {
        log.entering("getIsQuery()");
        log.exiting();
        return isQuery;
    }
    
    /**
     * DOCUMENT ME!
     *
     * @return DOCUMENT ME!
     */
    public ResultData getProcessTypes() {
        log.entering("getProcessTypes()");
        log.exiting();
        return catalogConfig.getProcessTypes();
    }

    /**
     * DOCUMENT ME!
     *
     * @return DOCUMENT ME!
     */
    public IPCItem getIPCItem() {
        log.entering("getIPCItem()");
        log.exiting();
        return ipcItem;
    }

    /**
     * DOCUMENT ME!
     *
     * @return DOCUMENT ME!
     */
    public String getPriceAnalysisURL() {
        log.entering("getPriceAnalysisURL()");
        if( priceCalcInitData != null ) {
           log.exiting();
        //TODO What happens if ipcItemRef == null?
        //		return (priceCalcInitData.getPriceAnalysisURL()+"?server=" +((TCPIPCItemReference)this.ipcItemRef).getHost()+"&port="+((TCPIPCItemReference)this.ipcItemRef).getPort()+"&sessionId="+this.ipcItemRef.getSessionId()+"&documentId="+this.ipcItemRef.getDocumentId()+"&itemId="+this.ipcItemRef.getItemId()+"&language=EN&mode=Display&pricingAnalysis=true");
            return (
              priceCalcInitData.getPriceAnalysisURL()
                  + "?documentId="
                  + this.ipcItemRef.getDocumentId()
                  + "&itemId="
                  + this.ipcItemRef.getItemId()
                  + "&language=EN&mode=Display&pricingAnalysis=true");
        }
        else {
            log.error("priceCalcInitData is null");
            log.exiting();
            return null;
        }
    }

    /**
     * DOCUMENT ME!
     *
     * @param itemList DOCUMENT ME!
     */
    public void setSearchTermsItemList(WebCatItemList itemList) {
        log.entering("setSearchTermsItemList()");
        searchTermsItemList = itemList;
        log.exiting();
    }

    /**
     * DOCUMENT ME!
     *
     * @param itempage DOCUMENT ME!
     *
     * @return DOCUMENT ME!
     */
    public Hashtable getSearchTerms(WebCatItemPage itempage) {
        
        log.entering("getSearchTerms(WebCatItemPage itempage)");
        
        if (searchTermsItemList == null) {
            searchTermsItemList = itempage.getItemList();
        }

        log.exiting();

        return getSearchTerms();
    }

    /**
     * DOCUMENT ME!
     *
     * @return DOCUMENT ME!
     */
    public Hashtable getSearchTerms() {
        
        log.entering("getSearchTerms()");
        
        WebCatItemList itemList = searchTermsItemList;
        Hashtable htTerms = new Hashtable();
        IQuery creatingQuery = null;

        if (itemList != null) {
            creatingQuery = (IQuery) itemList.getQuery();
        }

        CatalogQueryStatement queryStatement = null;

        if (creatingQuery != null) {
            queryStatement = (CatalogQueryStatement) creatingQuery.getStatement();
        }

        CatalogFilter filterTerm = null;
        String stmtAsString = null;
        String temp = null;
        String key = null;

        if (queryStatement != null) {
            filterTerm = queryStatement.getFilter();

            if (filterTerm != null) {
                Pattern p = Pattern.compile("AND|OR");
                String arrTerms[] = p.split(filterTerm.toString());
 
                for (int i1 = 0; i1 < arrTerms.length; i1++) {
                    key = null;
                    p = Pattern.compile("=\"|=fuzzy\\(|=containing\\(");

                    String arr1[] = p.split(arrTerms[i1]);

                    for (int j1 = 0; j1 < arr1.length; j1++) {
                        temp = arr1[j1].replace('(', ' ');
                        temp = temp.replace(')', ' ');
                        temp = temp.replace('"', ' ');
                        temp = temp.trim();

                        if ((j1 == 0)
                            && !(temp.equalsIgnoreCase("OBJECT_ID") || temp.equalsIgnoreCase("OBJECT_DESCRIPTION"))) {
                            break;
                        }

                        if (j1 == 0) {
                            key = temp;
                        }
                        else {
                            htTerms.put(key, temp);
                            stmtAsString = temp;
                        }
                    }
                }
                // if advanced search
                IQueryStatement.Type qType = queryStatement.getSearchType();
                if (qType != null &&
                    qType.equals(IQueryStatement.Type.ITEM_ADVSEARCH) &&
                    stmtAsString != null && 
                    !stmtAsString.equalsIgnoreCase("")) {
                    htTerms.put("ADVSEARCH", stmtAsString);
                }
            }
            else {
                stmtAsString = queryStatement.getStatementAsString();

                if (stmtAsString != null && !stmtAsString.equalsIgnoreCase("")) {
                    htTerms.put("QSEARCH", stmtAsString);
                }
            }
        } else if(getDetailScenario().equals(ActionConstants.DS_CATALOG_QUERY) && getDisplayScenario().equals(ActionConstants.RA_CURRENT_QUERY)) {
        	stmtAsString = getTheCatalog().getLastQuickSearchQueryString();
        	// cannot be null!
            if (!stmtAsString.equalsIgnoreCase("")) {
                htTerms.put("QSEARCH", stmtAsString);
            }
        }

        log.exiting();

        return htTerms;
    }

    /**
     * DOCUMENT ME!
     *
     * @return DOCUMENT ME!
     */
    public String IsProductList() {
        log.entering("IsProductList()");
        log.exiting();
        return isProductList;
    }

    /**
     * DOCUMENT ME!
     *
     * @return DOCUMENT ME!
     */
    public boolean isCUAProductId() {
        log.entering("isCUAProductId()");
        log.exiting();
        return (cuaList != null && cuaList.getProductId().length() > 0);
    }

    /**
     * DOCUMENT ME!
     *
     * @return DOCUMENT ME!
     */
    public WebCatArea getCurrentArea() {
        log.entering("getCurrentArea()");
        log.exiting();
        return theCatalog.getCurrentArea();
    }

    /**
     * DOCUMENT ME!
     *
     * @return DOCUMENT ME!
     */
    public PriceCalculatorInitData getPriceCalcInitData() {
        log.entering("getPriceCalcInitData()");
        log.exiting();
        return priceCalcInitData;
    }

    /**
     * DOCUMENT ME!
     *
     * @return DOCUMENT ME!
     */
    public boolean isItMoreThanOnePageToDisplay() {
        log.entering("isItMoreThanOnePageToDisplay()");
        log.exiting();
        return (itemPage.getPage() != 1) && (itemPage.getPage() != 0);
    }

    /**
     * DOCUMENT ME!
     *
     * @return DOCUMENT ME!
     */
    public int getColNumber() {
        log.entering("getColNumber()");
                
        int colno = 5;

        if (isStaticDynamicPricing()) {
            colno = colno + 4;
        }
        else {
            colno = colno + 3;
        }

        log.exiting();
        
        return colno;
    }

    /**
     * DOCUMENT ME!
     *
     * @return DOCUMENT ME!
     */
    public int getRowNumber() {
        log.entering("getRowNumber()");
        log.exiting();
        return itemPage.getItemList().size();
    }

    /**
     * DOCUMENT ME!
     *
     * @return DOCUMENT ME!
     */
    public int getFirstRowNumber() {
        log.entering("getFirstRowNumber()");
        log.exiting();
        return ((itemPage.getPage() - 1) * itemPage.pageSize()) + 1;
    }

    /**
     * DOCUMENT ME!
     *
     * @return DOCUMENT ME!
     */
    public int getLastRowNumber() {
        log.entering("getLastRowNumber()");
        log.exiting();
        return ((itemPage.getPage() - 1) * itemPage.pageSize()) + itemPage.size();
    }

    /**
     * DOCUMENT ME!
     *
     * @param item DOCUMENT ME!
     *
     * @return DOCUMENT ME!
     */
    public boolean isScalePrice(WebCatItem item) {
        log.entering("isScalePrice()");
        log.exiting();
        return item.getItemPrice().isScalePrice();
    }

    /**
     * DOCUMENT ME!
     *
     * @param item DOCUMENT ME!
     *
     * @return DOCUMENT ME!
     */
    public boolean isMultipleAndScalePrice(WebCatItem item) {
        log.entering("isMultipleAndScalePrice()");
        log.exiting();
        return item.getItemPrice().isMultipleAndScalePrice();
    }

    /**
     * DOCUMENT ME!
     *
     * @param item DOCUMENT ME!
     *
     * @return DOCUMENT ME!
     */
    public Iterator getItemPriceList(WebCatItem item) {
        log.entering("getItemPriceList()");
        if (isStaticDynamicPricing()) {
            log.debug("return getListPrices()");
            log.exiting();
            return item.getItemPrice().getListPrices();
        }
        else {
            log.debug("return getPrices()");
            log.exiting();
            return item.getItemPrice().getPrices();
        }
    }

    /**
     * DOCUMENT ME!
     *
     * @param item DOCUMENT ME!
     *
     * @return DOCUMENT ME!
     */
    public String getItemPrice(WebCatItem item) {
        log.entering("getItemPrice()");
        if (isStaticDynamicPricing()) {
            log.debug("return getIPCPrice()");
            log.exiting();
            return item.getItemPrice().getIPCPrice();
        }
        else {
            log.debug("return getPrice()");
            log.exiting();
            return item.getItemPrice().getPrice();
        }
    }

    /**
     * DOCUMENT ME!
     *
     * @return DOCUMENT ME!
     */
    public String getNextAction() {
        log.entering("getNextAction()");
        
        String nextAction = request.getParameter(ActionConstants.RA_NEXT);

        if (null != request.getAttribute(ActionConstants.RA_NEXT)) {
            nextAction = (String) request.getAttribute(ActionConstants.RA_NEXT);
        }

        if (null != nextAction) {
            nextAction = JspUtil.encodeHtml(nextAction);
        }

        if (log.isDebugEnabled()) {
            log.debug("getNextAction()=" + nextAction);
        }
        log.exiting();
        
        return nextAction;
    }
    
    /**
     * Returns the current id, to be used for scalePriceDivs,
     *
     * @return int the current id, to be used for scalePriceDivs
     */
    public int getCurrentScalePriceDivId() {
        log.entering("getCurrentScalePriceDivId");
        log.exiting();
        return scalePriceDivId;
    }

    /**
     * DOCUMENT ME!
     *
     * @return DOCUMENT ME!
     */
    public WebCatItem getCurrentItem() {
        log.entering("getCurrentItem()");
        log.exiting();
        return theCatalog.getCurrentItem();
    }

    /**
     * Returns true, if the subTitel attribute of the item is set to true
     *
     * @param webCatItem the item to check
     *
     * @return true   if the subtitle attribute is set to "true" false  otherwise
     */
    public boolean isSubTitleTrue(WebCatItem webCatItem) {
        if (log.isDebugEnabled()) {
            log.debug( "test if subtitle attribute is set to true for the webCatItem "
                + webCatItem.getProduct()
                + " - value : "
                + webCatItem.getSubtitle());
        }

        return webCatItem.getSubtitle().equalsIgnoreCase("true");
    }
    
    public boolean areaSpecificSearchShouldBeDisplayed() {
        log.entering("areaSpecificSearchShouldBeDisplayed");
        boolean response = getIsQuery() == null || 
                             getIsQuery().equals("") || 
                             getItemPage().getItemList().getAreaQuery() != null;
        if (log.isDebugEnabled()) {
            log.debug("returned value: "+response);
        }
        log.exiting();
    
        return response;
    }
    
    /**
     * Returns the amount of selected items in the itemlist which are not displayed on the current page.
     * The method stops checking the itemlist if atleast 2 items are selected. 
     *
     * @return counter as int, 0 if no item is selected
     *                         1 if one item is selected
     *                         2 if atleast 2 items are selected
     */
    public int getAmountOfSelectedItems() {
        final String METHOD = "getAmountOfSelectedItems()";
        log.entering(METHOD);

        int counter = 0;
        int counter2 = 0;

        // needs to be read again because select flags could be changed by compare jsp 
        searchTermsItemList = theCatalog.getCurrentItemList();

        // check pages before the current page         
        for (int i = 0; i < getFirstRowNumber()-1 && counter < 2; i++) {
            if (searchTermsItemList.getItem(i).isSelected()) {
                counter++;
            }
        }
        if (log.isDebugEnabled()) {
            int j = getFirstRowNumber()-1;
            counter2 = counter;
            log.debug(METHOD + ": check item number 0 to " + j +
                      " / selected items = " + counter);
        }

        // check pages after the current page         
        for (int i = getLastRowNumber()+1; i < searchTermsItemList.size() && counter < 2; i++) {
            if (searchTermsItemList.getItem(i).isSelected()) {
                counter++;
            }
        }
        if (log.isDebugEnabled()) {
            int j1 = getLastRowNumber()+1;
            int j2 = searchTermsItemList.size()-1;
            counter2 -= counter;
            log.debug(METHOD + ": check item number " + j1 +
                      " to " + j2 +
                      " / selected items = " + counter2 +
                      " / total selected items (max 2) = " + counter);
        }

        log.exiting();
        return counter;
    }
    
    /**
     * Checks if the type of the category is a reward catalog area
     * @return flag indication if the category is a reward catalog area 
     */
    public boolean isRewardCategory() {
    	log.entering("isRewardCategory");
        boolean isRewardCategory = false;
        WebCatArea area = this.getCurrentArea();
        if (area != null) {
            isRewardCategory = area.isRewardCategory();
        }
        log.exiting();
        return (isRewardCategory);
    }

    /**
     * Checks if the type of the category is a buy points area
     * @return flag indication if the category is a buy points area 
     */
    public boolean isBuyPointsCategory() {
		log.entering("isBuyPointsCategory");
        boolean isBuyPtsCategory = false;
        WebCatArea area = this.getCurrentArea();
        if (area != null) {
            isBuyPtsCategory = area.isBuyPointsCategory();
        }
        log.exiting();
        return (isBuyPtsCategory);
    }
    
    /**
     * Returns true, if we have a category specific search.
     */
    public boolean isCategorySpecificSearch() {
        
        if (itemPage == null) {
            log.debug("ProductsUI.isCategroySpecificSearch: itemPage == null, cannot proceed");
            return false;
        }
        
        WebCatItemList itemList = itemPage.getItemList();
        if (itemList == null) {
            log.debug("ProductsUI.isCategroySpecificSearch: itemList == null, cannot proceed");
            return false;
        }
    
        return itemList.isCreatedByAreaQuery();
    }
    
    /**
     * checks if the domain relaxation is required
     * @return true, if a domain relaxation is required, false otherwise
     */
	public boolean domainRelaxationRequired(){
		log.entering("domainRelaxationRequired");
		boolean returnValue = catalogConfig.isStandaloneCatalog();
		
		log.exiting();
		return returnValue;
	}

    /**
     * Checks in the request what display and detail scenario should be displayed on the JSP. The
     * first parameter to check is &quot;display_scenario&quot; ({@link ActionConstants#RA_DISPLAY_SCENARIO})
     * and the second parameter is &quot;detailScenario&quot; ({@link ActionConstants#RA_DETAILSCENARIO}).
     * So the result could be &quot;&display_scenario=query&detailScenario=list&quot;. The string 
     * beginns with an & if the boolean parameter isFirstParam is true.
     * <b>!!! Attention !!!</b> The paramters will be encoded to an URL!
     * 
     * @param request The current request from the client.
     * @param isFirstParam A boolean value if the returned string should start with '&' or not. <code>true</code> will start the string with '&'.
     * @return The URL string with the parameters from the request.
     */
    public static String determineScenario(HttpServletRequest request, boolean isFirstParam) {

		//The request parameter detailScenario differentiates between different types of searches/queries
    	StringBuffer params = new StringBuffer(determineDisplayScenarioOnly(request, isFirstParam));
		params.append(determineDetailScenarioOnly(request, (params.length() > 0 || isFirstParam)) );
		
		return params.toString();
    }
    
    /**
     * Checks in the request what display scenario should be displayed on the JSP. The
     * first parameter to check is &quot;display_scenario&quot; ({@link ActionConstants#RA_DISPLAY_SCENARIO}) only.
     * So the result could be &quot;&display_scenario=query&quot;. The string 
     * beginns with an & if the boolean parameter isFirstParam is true.
     * <b>!!! Attention !!!</b> The paramters will be encoded to an URL!
     * 
     * @param request The current request from the client.
     * @param isFirstParam A boolean value if the returned string should start with '&' or not. <code>true</code> will start the string with '&'.
     * @return The URL string with the parameters from the request.
     */
    public static String determineDisplayScenarioOnly(HttpServletRequest request, boolean isFirstParam) {
    	String displayScenario = request.getParameter(ActionConstants.RA_DISPLAY_SCENARIO);
		if (displayScenario == null)
			displayScenario = request.getAttribute(ActionConstants.RA_DISPLAY_SCENARIO) == null ? null : request.getAttribute(ActionConstants.RA_DISPLAY_SCENARIO).toString();
		
		StringBuffer params = new StringBuffer();
		if(displayScenario != null) {
			if(isFirstParam) {
				params.append("&");
			}
			params.append(ActionConstants.RA_DISPLAY_SCENARIO);
			params.append("=");
			params.append(JspUtil.encodeURL(displayScenario));
		}
		return params.toString();
    }
    
    /**
     * Checks in the request what detail scenario should be displayed on the JSP. The
     * first parameter to check is &quot;detailScenario&quot; ({@link ActionConstants#RA_DETAILSCENARIO}) only.
     * So the result could be &quot;&detailScenario=itemDetails&quot;. The string 
     * beginns with an & if the boolean parameter isFirstParam is true.
     * <b>!!! Attention !!!</b> The paramters will be encoded to an URL!
     * 
     * @param request The current request from the client.
     * @param isFirstParam A boolean value if the returned string should start with '&' or not. <code>true</code> will start the string with '&'.
     * @return The URL string with the parameters from the request.
     */
    public static String determineDetailScenarioOnly(HttpServletRequest request, boolean isFirstParam) {
    	String displayScenario = request.getParameter(ActionConstants.RA_DETAILSCENARIO);
		if (displayScenario == null)
			displayScenario = request.getAttribute(ActionConstants.RA_DETAILSCENARIO) == null ? null : request.getAttribute(ActionConstants.RA_DETAILSCENARIO).toString();
		
		StringBuffer params = new StringBuffer();
		if(displayScenario != null) {
			if(isFirstParam) {
				params.append("&");
			}
			params.append(ActionConstants.RA_DETAILSCENARIO);
			params.append("=");
			params.append(JspUtil.encodeURL(displayScenario));
		}
		return params.toString();
    }
}
