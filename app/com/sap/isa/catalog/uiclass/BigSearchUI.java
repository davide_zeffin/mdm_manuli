package com.sap.isa.catalog.uiclass;

import javax.servlet.jsp.PageContext;

import com.sap.isa.catalog.AttributeKeyConstants;
import com.sap.isa.catalog.webcatalog.WebCatAreaAttribute;
import com.sap.isa.core.util.WebUtil;


/**
 * @author V.Manjunath Harish
 *
 * To change the template for this generated type comment go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
public class BigSearchUI extends CategoriesUI {
	protected boolean isCRM = true;
	public PageContext pageContext;
	protected String domainKey = "catalog.isa.bigSearch";
	protected String attritbuteName;
	
	public BigSearchUI(PageContext pageContext){
		super(pageContext);
		this.pageContext = pageContext;
	}
	
	public void setIsCRM(boolean value)
	{
		isCRM = value;
	}
	
	public boolean getIsCRM()
	{
		return isCRM;
	}
	
	public String isRadiobutton()
	{
		return WebCatAreaAttribute.RADIO_BUTTON;
	}
	
	public String isDropDown()
	{
		return WebCatAreaAttribute.DROP_DOWN;
	}
	
	public String isInputField()
	{
		return WebCatAreaAttribute.INPUT_FIELD;
	}
	
	public String parseAttributeName(String key)
	{
		try {
				 attritbuteName = WebUtil.translate(pageContext, domainKey+"."+key, null);
				  if (attritbuteName != null)
					  if (attritbuteName.equals("null") || attritbuteName.indexOf("???") == 0) attritbuteName=null;
				return attritbuteName;
				}
			catch (java.util.MissingResourceException ex) {return null;}
	}
	
	public String getAttributeName()
	{
		return attritbuteName;
	}
	
	public String getProductDescription()
	{
		return (getCatalog().getCatalog().getAttributeGuid((AttributeKeyConstants.PRODUCT_DESCRIPTION)));
	}
	
	public String getProductId()
	{
		return (getCatalog().getCatalog().getAttributeGuid(AttributeKeyConstants.PRODUCT));
	}

}