
package com.sap.isa.catalog.uiclass;

import javax.servlet.jsp.PageContext;

import com.sap.isa.backend.boi.isacore.BaseConfiguration;
import com.sap.isa.catalog.webcatalog.WebCatItem;
import com.sap.isa.isacore.TabStripHelper;
import com.sap.isa.isacore.TabStripHelper.TabButton;

/**
 * @author SAP
 *
 * To change the template for this generated type comment go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
public class CUAHeaderUI extends CatalogBaseUI {
	
	
	private boolean contracts 		= false;
	private boolean accessories    	= false;   
	private boolean crossSelling  	= false;
	private boolean Alternatives    = false;
	private boolean remanufactured	= false;
	private boolean IsCRM			= false;
	private boolean IsAccesories    = false;
	private boolean IsCrossSelling  = false;
	private boolean IsUpSelling		= false;
	private boolean IsRemanufactured = false;
	protected WebCatItem webCatItem; 
	
	
	private TabStripHelper tabStrip;
	private TabStripHelper.TabButton tabButton;
	
	private String activeView = "";
	
	/**
	 * 
	 * @param pageContext
	 * Constructor
	 */ 
	public CUAHeaderUI(PageContext pageContext) {
		super(pageContext);
		if (getCurrentItem() != null) {
			setContracts(!(getCurrentItem().getContractItems() == null || getCurrentItem().getContractItems().size() == 0));
		} else {
			setContracts(false);
		}
		//Initialize boolean variables in UI-Class
		
			  if(IsAccessoriesAvailable())
				  setAccessories(true);
			  if(IsCrossSellingAvailable())
				  setCrossSelling(true);
			  if(IsAlternativeAvailable())
				  setAlternatives(true);
			  if(IsRemanufactureAvailable())
				  setRemanufactured(true); 

		
		tabStrip = new TabStripHelper();
	}
	


	public TabStripHelper getTabStrip(String activeview){
		setActiveView(activeview);
		buildTabStrip();
		
		return tabStrip;
	}

	private void buildTabStrip(){
		
		

		/* All menu entries are defined within this section */

		String appsUrl = null;

		/* first add all buttons, which will be used */
		/* Contracts ******************************************************************* */
		if (contracts) {
				tabButton = tabStrip.createTabButton("contracts",
				"catalog.isa.contractDetails",
				 "javascript:load_Contracts()");
			tabStrip.addTabButton(tabButton);
		}

		/* Accessories ******************************************************************* */
		if (accessories || IsAccesories() || getIsCRM() ) { 
		/* With CRM backend we need the accessory tab in any case, 
		 * because the catalog relies on it in several places.
		 * Should be reworked sometime. */
			tabButton = tabStrip.createTabButton("accessories",
				"catalog.isa.accesories",
				"javascript:load_Accessories()");
			tabStrip.addTabButton(tabButton);
		}
		
		/* CrossSelling  Search ******************************************************************* */
		if (crossSelling || IsCrossSelling()) {
			tabButton = tabStrip.createTabButton("crossSelling",
				"catalog.isa.crossSelling",
				"javascript:load_CrossSelling()");
			tabStrip.addTabButton(tabButton);
		}		

		/* Alternatives Search ************************************************************ */
		if (Alternatives || IsUpSelling()) {
			tabButton = tabStrip.createTabButton("Alternatives",
				"catalog.isa.upDown",
			"javascript:load_Alternatives()");
			tabStrip.addTabButton(tabButton);
		}

		/* Reman/New Search *******************************************************************  */
		if (((this.readExch_BussinessFlag().equals("X"))&&(remanufactured || IsRemanufactured()))) {
				tabButton = tabStrip.createTabButton("remanufactured",
					"catalog.isa.remannewpart",
					"javascript:load_Remanufacture()");
				tabStrip.addTabButton(tabButton);
		}
		
		

	}


	private void setActiveView(String aview){
		this.activeView = aview;
		}
	
	public TabButton getTabButton(){
		return tabButton;
	}
	
	public WebCatItem getCurrentItem()
	{
		this.webCatItem = (WebCatItem) request.getAttribute("currentItem");
		return webCatItem;
	}
	
	public String readExch_BussinessFlag()
	{
		String flag = this.getCurrentItem().readExchBusinessFlag();
		return flag;	
	}
		
	public boolean IsAccesories()
	{
		if(request.getParameter("cuaproducttype")!=null)
		{
			this.IsAccesories = request.getParameter("cuaproducttype").equals("A");
		}
		return IsAccesories;
	}
	
	public boolean IsCrossSelling()
	{
		if(request.getParameter("cuaproducttype")!=null)
		{
			this.IsCrossSelling = request.getParameter("cuaproducttype").equals("C");
		}
		return IsCrossSelling;
	}
	
	public boolean IsUpSelling()
	{
		if(request.getParameter("cuaproducttype")!=null)
		{
			this.IsUpSelling = request.getParameter("cuaproducttype").equals("UD");
		}
		return IsUpSelling;
	}
	
	public boolean IsRemanufactured()
	{
		if(request.getParameter("cuaproducttype")!=null)
		{
			IsRemanufactured = request.getParameter("cuaproducttype").equals("RN");
		}
		return IsRemanufactured;
	}
	
	public boolean IsAccessoriesAvailable()
	{
		return catalogConfig.isAccessoriesAvailable();
	}
	
	public boolean IsCrossSellingAvailable()
	{
		return catalogConfig.isCrossSellingAvailable();
	}
	
	public boolean IsAlternativeAvailable()
	{
		return catalogConfig.isAlternativeAvailable();
	}
	
	public boolean IsRemanufactureAvailable()
	{
		return (catalogConfig.isRemanufactureAvailable()|| catalogConfig.isNewPartAvailable());
	}
	
	public String IsbackendCRM() {
		return BaseConfiguration.BACKEND_CRM;
	}
	
	/* Get and set methods for boolean variables */

	public void setIsCRM(boolean IsCRM)
	{
		this.IsCRM = IsCRM;
	}
	
	public boolean getIsCRM()
	{
		return this.IsCRM;
	}

	public void setContracts(boolean contracts){
		this.contracts = contracts;
	}

	public boolean getContracts(){
		return this.contracts;
	}

	public void setAccessories(boolean accessories){
		this.accessories = accessories;
	}

	public boolean getAccessories(){
		return this.accessories;
	}
	
	public void setCrossSelling(boolean crossSelling){
		this.crossSelling = crossSelling;
	}

	public boolean getCrossSelling(){
		return this.crossSelling;
	}
	
	public void setAlternatives(boolean alternatives){
		this.Alternatives = alternatives;
	}
	
	public boolean getAlternatives(){
		return this.Alternatives;
	}
	
	public void setRemanufactured(boolean reman){
		this.remanufactured = reman;
	}
	
	public boolean getRemanufactured(){
		return this.remanufactured;
	}

}
