package com.sap.isa.catalog.uiclass;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.jsp.PageContext;

import com.sap.isa.businessobject.ProductBaseData;
import com.sap.isa.catalog.AttributeKeyConstants;
import com.sap.isa.catalog.webcatalog.ItemGroup;
import com.sap.isa.catalog.webcatalog.WebCatSubItem;
import com.sap.isa.catalog.webcatalog.WebCatSubItemList;
import com.sap.isa.core.Constants;
import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.core.ui.context.ContextManager;
import com.sap.isa.core.util.Message;
import com.sap.isa.core.util.MessageList;

/**
 * SubItemListUI 
 *
 * @author SAP AG
 * To change the template for this generated type comment go to
 *         Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
public class SubItemListUI extends ProductDetailsUI {

    private static IsaLocation log = IsaLocation.getInstance(SubItemListUI.class.getName());

    static final int listTypeSubItemList = 1;

    protected WebCatSubItemList productList;
    /* collects number of products per group with key = groupKey + parentTechKey */
    protected HashMap groupSizesMap = new HashMap(); 
    /* collects per group if group has selected items */
    protected HashMap groupHasSelectedItemMap = new HashMap();
    /* collects number of optional products per group with key = groupKey + parentTechKey */
    protected HashMap groupNumbOptItemsMap = new HashMap(); 
    /* information per group if mandatory with key = groupKey + parentTechKey */
    protected HashMap groupIsMandatoryMap = new HashMap(); 
    protected int listType;
    protected String groupKey = null;
    protected String parentTechKey = null;
    protected int numArea = 0;
    protected boolean isNewArea = false;
    protected ProductBaseData currentProduct = null;

    /**
     * Constructor for WebCatSubItemList. <br>
     * 
     * @param webCatSubItemList Sub item list
     */
    public SubItemListUI(
        PageContext pageContext,
        WebCatSubItemList subItemList) {

        super(pageContext);

        final String METHOD = "SubItemListUI(PageContext pageContext, WebCatSubItemList subItemList)";
        String firstScCall = "";

        log.entering(METHOD);

        // sort subItemList
        sortSubItemList(subItemList);
        
        this.listType = listTypeSubItemList;

        ContextManager contextManager = ContextManager.getManagerFromRequest((HttpServletRequest) pageContext.getRequest());
        firstScCall = contextManager.getContextValue(Constants.CV_1STSCCALL);
         
        if (firstScCall == null) {
            initialize(true);
        }
        else {
            initialize(firstScCall.equals("Y"));
        }

        if (log.isDebugEnabled()) {
            log.debug(METHOD + ": listType = " + listTypeSubItemList);
        }    
        log.exiting();
    }

    /**
     * Sorts the list of sub items 
     * 
     * @param subItem as WebCatSubItem 
     */
    protected void sortSubItemList2(WebCatSubItemList subItemList) {

        final String METHOD = "sortSubItemList2"; 
        log.entering(METHOD);
        
        // move the dependent products to the end of the list
        WebCatSubItemList sortedList = new WebCatSubItemList(subItemList.getParentCatalog());
        if (subItemList.getItemGroups() != null) {
            sortedList.setItemGroups(subItemList.getItemGroups());
        }
        ArrayList dependentItems = new ArrayList();

        WebCatSubItem subItem = null;
        String currGroupKey = null;
        String groupKey = null;
        boolean isCrp = false;
        boolean currIsCrp = false;
        boolean isNewGroup = false;
        boolean isGroupKeyChanged = false;
        ArrayList depItems = null;
        
        if (log.isDebugEnabled()) {
            log.debug(METHOD + ": Start sorting WebCatSubItemList");
        }    
        // at first copy all non dependent products
        for (int i = 0; i < subItemList.size(); i++) {

            subItem = subItemList.getSubItem(i);
            if (log.isDebugEnabled()) {
                log.debug(METHOD + ": WebCatSubitem " + subItem);
            }    

            isCrp = subItem.isCombinedRatePlan() || subItem.isRatePlanCombination();
            groupKey = subItem.getGroupKey();

            // add all products except of dependent components 
            if (!WebCatSubItem.SC_ITEM_TYPE_DEPENDENT_COMPONENT.equals(subItem.getScItemType())) {
                // check if group key changed or null
                isGroupKeyChanged = ( (groupKey == null && currGroupKey != null) ||
                                      (groupKey != null && (currGroupKey == null || !currGroupKey.equals(groupKey))));    
                
                if (isGroupKeyChanged) {                 
                    isNewGroup = true; 
                } 
                else {
                    // only if no groups are maintained
                    if (groupKey == null) {
                        // Test rate plan combinations and combined rate plan
                        isNewGroup = (currIsCrp != isCrp);
                    }
                    else {
                        isNewGroup = false;
                    }
                }
                
                // add dependent products after the last group entry
                if (isNewGroup && !dependentItems.isEmpty()) {
                    if (log.isDebugEnabled()) {
                        log.debug(METHOD + " :  for group " + currGroupKey + " - " + dependentItems.size() + " dependent products found");
                    }    
                    // finally attached the denepedent products to the end of the list
                    for (int j = 0; j < dependentItems.size(); j++) {
                        sortedList.addItem((WebCatSubItem) dependentItems.get(j));
                    }
                    dependentItems.clear();
                }
                // only for sales components and combined rate plans
                if (!subItem.isRatePlanCombination()) {
                    currGroupKey = groupKey;
                }
                currIsCrp = isCrp;

                if (log.isDebugEnabled()) {
                    log.debug(METHOD + ": " + subItem.getProduct() + " is not a dependent product, keep place in the list");
                }    
                sortedList.addItem(subItem);
            } 
            else {
                // collect dependent components   
                if (log.isDebugEnabled()) {
                    log.debug(METHOD + ": " + subItem.getProduct() + " is a dependent product and  added with group: " + groupKey);
                }    
                dependentItems.add(subItem);
            }

            // also determine group sizes and if items are selected
            setGroupSizeAndSelection(subItem);
        }
        for (int j = 0; j < dependentItems.size(); j++) {
            sortedList.addItem((WebCatSubItem) dependentItems.get(j));
        }

        if (log.isDebugEnabled()) {
            log.debug(METHOD + ": Finished sorting WebCatSubItemList");
        }

        this.productList = sortedList;
        log.exiting();
    }
    protected void sortSubItemList(WebCatSubItemList subItemList) {

        final String METHOD = "sortSubItemList"; 
        log.entering(METHOD);
        
        // move the dependent products to the end of the list
        WebCatSubItemList sortedList = new WebCatSubItemList(subItemList.getParentCatalog());
        if (subItemList.getItemGroups() != null) {
            sortedList.setItemGroups(subItemList.getItemGroups());
        }
        ArrayList dependentItems = new ArrayList();

        WebCatSubItem newSubItem = null;
        WebCatSubItem oldSubItem = null;
        String groupKey = null;
        boolean isNewGroup = false;
        ArrayList depItems = null;
        
        if (log.isDebugEnabled()) {
            log.debug(METHOD + ": Start sorting WebCatSubItemList");
        }    
        // at first copy all non dependent products
        for (int i = 0; i < subItemList.size(); i++) {

            newSubItem = subItemList.getSubItem(i);
            if (log.isDebugEnabled()) {
                log.debug(METHOD + ": WebCatSubitem " + newSubItem);
            }    

            // add all products except of dependent components 
            if (!WebCatSubItem.SC_ITEM_TYPE_DEPENDENT_COMPONENT.equals(newSubItem.getScItemType())) {
                
                if (oldSubItem == null) {
                    isNewGroup = true; 
                }
                else {
                    isNewGroup = isChangeOfArea((ProductBaseData) oldSubItem, (ProductBaseData) newSubItem);
                }
                
                // add dependent products after the last group entry
                if (isNewGroup && !dependentItems.isEmpty()) {
                    if (log.isDebugEnabled()) {
                        String lGroupKey = null;
                        if (newSubItem.isRatePlanCombination()) {
                            lGroupKey = newSubItem.getParent().getGroupKey();
                        }
                        else {
                            lGroupKey = newSubItem.getGroupKey();
                        }
                        log.debug(METHOD + " :  for group " + lGroupKey + " - " + dependentItems.size() + " dependent products found");
                    }    
                    // finally attached the denepedent products to the end of the list
                    for (int j = 0; j < dependentItems.size(); j++) {
                        sortedList.addItem((WebCatSubItem) dependentItems.get(j));
                    }
                    dependentItems.clear();
                }

                if (log.isDebugEnabled()) {
                    log.debug(METHOD + ": " + newSubItem.getProduct() + " is not a dependent product, keep place in the list");
                }    
                sortedList.addItem(newSubItem);
                
                oldSubItem = newSubItem;
            } 
            else {
                // collect dependent components   
                if (log.isDebugEnabled()) {
                    log.debug(METHOD + ": " + newSubItem.getProduct() + " is a dependent product and  added with group: " + groupKey);
                }    
                dependentItems.add(newSubItem);
            }

            // also determine group sizes and if items are selected
            setGroupSizeAndSelection(newSubItem);
        }
        for (int j = 0; j < dependentItems.size(); j++) {
            sortedList.addItem((WebCatSubItem) dependentItems.get(j));
        }

        if (log.isDebugEnabled()) {
            log.debug(METHOD + ": Finished sorting WebCatSubItemList");
        }

        this.productList = sortedList;
        log.exiting();
    }

    /**
     * Determines the size of the group of each sub item and sets selected flags for the groups
     * 
     * @param subItem as WebCatSubItem 
     */
    protected void setGroupSizeAndSelection(WebCatSubItem subItem) {

        String method = "setGroupSizeAndSelection"; 
        log.entering(method);

        Integer groupSize;
        Integer groupNumbOptItems;
        String groupHasSelectedItem;
        String groupIsMandatory;
        ProductBaseData parent = subItem.getParent();
        String lTechKey = parent.getTechKey().getIdAsString();
        String lGroupKey = subItem.getGroupKey() + lTechKey;
        if (log.isDebugEnabled()) {
            log.debug(" group: " + lGroupKey);
        }

        if (lGroupKey != null && lGroupKey.length() > 1) {

            // group size 
            groupSize = (Integer) groupSizesMap.get(lGroupKey);
            if (groupSize == null) {
                groupSize = new Integer(1);
            } else {
                groupSize = new Integer(groupSize.intValue() + 1);
            }
            if (log.isDebugEnabled()) {
                log.debug("Setting group size for group: " + lGroupKey + " to: " + groupSize.toString());
            }    
            groupSizesMap.put(lGroupKey, groupSize);

            // number of optional items per group 
            groupNumbOptItems = (Integer) groupNumbOptItemsMap.get(lGroupKey);
            if (groupNumbOptItems == null) {
                groupNumbOptItems = new Integer(0);
            }
            if (subItem.isOptional()) {
                groupNumbOptItems = new Integer(groupNumbOptItems.intValue() + 1);
            }
            if (log.isDebugEnabled()) {
                log.debug("Setting number of optional items for group: " + lGroupKey + " to: " + groupNumbOptItems.toString());
            }    
            groupNumbOptItemsMap.put(lGroupKey, groupNumbOptItems);

            // group has selected items 
            if (subItem.isScSelected()) {
                // item selection
                groupHasSelectedItem = (String) groupHasSelectedItemMap.get(lGroupKey);

                if (groupHasSelectedItem == null) {
                    if (log.isDebugEnabled()) {
                        log.debug("Setting group selected for group: " + lGroupKey);
                    }    
                    groupHasSelectedItemMap.put(lGroupKey, "X");
                }
            }
            // group is mandatory group 
            if (subItem.isPartOfMandatoryGroup()) {
                groupIsMandatory = (String) groupIsMandatoryMap.get(lGroupKey);
                if (groupIsMandatory == null) {
                    if (log.isDebugEnabled()) {
                        log.debug("Setting group is mandatory: " + lGroupKey);
                    }    
                    groupIsMandatoryMap.put(lGroupKey, "X");
                }
            }
        }

        log.exiting();
    }

    /**
     * Initialization of the productlist
     * The SC selection flags and the collapse flags for the groups are set if the solution configurator 
     * was called the first time.
     *  
     * @param firstScCall as boolean, indicating if the solution configurator was called the first time
     */
    public void initialize(boolean firstScCall) {
        final String METHOD = "initialize(firstScCall)";
        log.entering(METHOD);

        WebCatSubItem subItem;
        String lGroupKey = "";
        String lGroupKeyTmp = "";
        String lTechKey = ""; 
        boolean expanded = false;
        String grpIsMandatory = "";
        Integer groupNumbOptItems;
        
        if (log.isDebugEnabled()) {
            log.debug(METHOD + ": firstScCall = " + firstScCall);
        }
        
        // create Item group if necessary  
        if (productList.getItemGroups() == null) {
            createItemGroups();
        }

        if (firstScCall) {
            for (int i = 0; i < productList.size(); i++) {
                subItem = productList.getSubItem(i);
                expanded = false;
                
                if (!subItem.isPartOfAGroup()) {
                    continue;
                }

                lTechKey = subItem.getParent().getTechKey().getIdAsString();
                lGroupKeyTmp = subItem.getGroupKey() + lTechKey;
                
                // continue if groupKey has changed
                if (lGroupKeyTmp == null || lGroupKeyTmp.equals(lGroupKey)) {
                    continue;
                }
                lGroupKey = lGroupKeyTmp;

                // check if group has items which are selected by default                  
                if (groupHasSelectedItemMap.get(lGroupKey) == null) {
                    // Mandatory groups
                    grpIsMandatory = (String) groupIsMandatoryMap.get(lGroupKey);
                    if (grpIsMandatory != null && grpIsMandatory.equals("X")) {
                        expanded = true;
                    }
                    else {
                        // Groups with only one item
                        groupNumbOptItems = (Integer) groupNumbOptItemsMap.get(lGroupKey);
                        if (groupNumbOptItems != null && groupNumbOptItems.intValue() == 1) {
                            expanded = true;
                        }
                    }
                }
                if (expanded) {
                    ItemGroup itemGroup = productList.getItemGroup(subItem.getGroupKey());
                    if (itemGroup != null) {
                       itemGroup.setDisplayCollapsed(false);
                    }
                }
                if (log.isDebugEnabled()) {
                    log.debug(METHOD + ": show " + lGroupKey + " expanded: " + expanded);
                }    
            }
        }

        log.exiting();
    }

    /**
     * Create the list of groups if empty
     *  
     * @param groupKey as String
     */
    public void createItemGroups() {
        String method = "createItemGroups";
        log.entering(method);

        WebCatSubItem subItem;
        String groupKey;
        ItemGroup itemGroup = null;

        for (int i = 0; i < productList.size(); i++) {
            subItem = productList.getSubItem(i);
            groupKey = subItem.getGroupKey();

            if (groupKey != null && groupKey.length() > 0) {
                if (productList.getItemGroup(groupKey) == null) {
                    itemGroup = new ItemGroup(groupKey, groupKey);
                    productList.setItemGroup(itemGroup);
                    if (log.isDebugEnabled()) {
                        log.debug(method + ": group added with key = " + groupKey);
                    }
                }
            }
        }
        log.exiting();
    }

    /**
     * Returns an item of type ProductBaseData
     * Implements the method of ProductListBaseData
     *  
     * @return item of type ProductBaseData
     */
    public ProductBaseData getProduct(int index) {
        String method = "getProduct(index)";
        log.entering(method);

        ProductBaseData product = null;

        if (log.isDebugEnabled()) {
            log.debug(method + "(" + index + ") for listType: " + listType);
        }
        switch (listType) {
            case 1 :
                product = productList.getProduct(index);
                if (log.isDebugEnabled()) {
                    log.debug(method + "(" + index + ") / listType: " + listType + " / product: " + product.getDescription());
                }
                break;
            default :
                }

        log.exiting();
        return (product);
    }

    /**
     * Returns the group text for the current group
     *
     * @return group text as String
     */
    public String getGroupText() {
        final String METHOD = "getGroupText()";
        log.entering(METHOD);

        ItemGroup itemGroup = null;
        String groupText = "";

        if (groupKey != null && groupKey.length() > 0) {
            if ((productList != null)
                && (productList.getItemGroup(groupKey) != null)) {
                groupText = productList.getItemGroup(groupKey).getGroupText();
            }
            if (groupText.length() == 0) {
                groupText = groupKey;
            }
        }

        if (log.isDebugEnabled()) {
            log.debug(METHOD + ": groupText =" + groupText);
        }
        log.exiting();
        return groupText;
    }

    /**
     * Returns a flag if a group text exists
     *
     * @return flag as boolean, if group text exists
     */
    public boolean isGroupText() {
        final String METHOD = "isGroupText";
        log.entering(METHOD);

        String groupText = getGroupText();
        boolean isGroupText = false;

        if (groupText != null && groupText.length() > 0) {
            isGroupText = true;
        }

        if (log.isDebugEnabled()) {
            log.debug(METHOD + ": group text found =" + isGroupText);
        }
        log.exiting();
        return isGroupText;
    }

    /**
     * Returns the information if a new area (with or without a group) was build
     *
     * @return isNewArea of type boolean
     */
    public boolean getNewArea() {
        final String METHOD = "getNewArea()";
        log.entering(METHOD);
        if (log.isDebugEnabled()) {
            log.debug(METHOD + ": isNewArea = " + isNewArea);
        }
        log.exiting();
        return (this.isNewArea);
    }

    /**
     * Returns the information if an area is the first area
     *
     * @return isFirstArea of type boolean
     */
    public boolean isFirstArea() {
        String method = "isFirstArea()";
        log.entering(method);
        boolean isFirstArea = (numArea == 1) ? true : false;

        if (log.isDebugEnabled()) {
            log.debug(method + ": isFirstArea = " + isFirstArea);
        }

        log.exiting();
        return isFirstArea;
    }

    /**
     * Set the newArea flag and returns the information if a new area was set
     * @param  product which needs to be checked
     * @return isNewArea of type boolean
     */
    public boolean isNewArea(ProductBaseData product) {
        final String METHOD = "isNewArea()";
        log.entering(METHOD);
        if (log.isDebugEnabled()) {
            log.debug(METHOD + ": product = " + product.getDescription());
        }

        if (product == null) {
            if (log.isDebugEnabled()) {
                log.debug(METHOD + ": product = null");
            }
            log.exiting();
            return false;
        }
        
        ProductBaseData parent = product.getParent();
        String lTechKey = parent.getTechKey().getIdAsString();
        String lGroupKey = product.getGroupKey();
        if (lGroupKey == null) {
            lGroupKey = ""; 
        }
        
        this.isNewArea = false;

        // group key changed
        if (this.numArea == 0 ||
            isChangeOfArea(currentProduct, product)) {
            this.isNewArea = true;
            this.numArea += 1;
        }

        // set current product 
        this.currentProduct = product;
        // set new group key 
        if (!product.isRatePlanCombination()) {
            this.groupKey = lGroupKey;
        }
        // set new parent item 
        this.parentTechKey = lTechKey;

        if (log.isDebugEnabled()) {
            log.debug(METHOD + ": groupKey = " + this.groupKey + " / parentTechKey = " + this.parentTechKey);
            log.debug(METHOD + ": numArea = " + numArea + " / isNewArea = " + this.isNewArea);
        }

        log.exiting();
        return (this.isNewArea);
    }

    /**
     * The flag indicates if the sub item is the last item of an area.  
     * In this case the group button needs to be displayed
     * Close group if
     * a) next groupKey is different from current groupKey
     * b) next parent TechKey is different from current parent TechKey
     * @param  index of type int
     * @return isEndOfArea of type boolean
     */
    public boolean isEndOfArea(int index) {
        final String METHOD = "isEndOfArea()";
        log.entering(METHOD);
        if (log.isDebugEnabled()) {
            log.debug(METHOD + ": index = " + index);
        }

        boolean isEndOfArea = false;
        int nextIndex = index + 1;

        if (nextIndex == size()) {
            /* last product, therefore close group */
            isEndOfArea = true;
            if (log.isDebugEnabled()) {
                log.debug(METHOD + ": no further products");
            }
        } else {
            /* is group key of next item identical to current groupKey ? */
            ProductBaseData product = getProduct(nextIndex);
            if (isChangeOfArea(currentProduct, product)) {
                isEndOfArea = true;
            }
            if (log.isDebugEnabled()) {
                log.debug(METHOD + ": product = " + product.getDescription());
            }
        }

        if (log.isDebugEnabled()) {
            log.debug(METHOD + ": isEndOfArea = " + isEndOfArea);
        }
        log.exiting();
        return isEndOfArea;
    }

    /**
     * checks if a change of the area needs to be changed 
     * @param  oldProduct corresponding to current product
     * @param  newProduct which needs to be checked
     * @return isNewArea of type boolean
     */
    public boolean isChangeOfArea(ProductBaseData oldProduct, ProductBaseData newProduct) {
        final String METHOD = "isChangeOfArea()";
        log.entering(METHOD);
        if (log.isDebugEnabled())
            log.debug(METHOD + "new product = " + newProduct.getDescription());

        boolean isChangeOfArea = false;
        
        if (oldProduct == null) {
            isNewArea = true;
            if (log.isDebugEnabled()) {
                log.debug(METHOD + ": old product = null");
            }
        }
        else {    
            String oldTechKey;
            String newTechKey;
            String oldGroupKey;
            String newGroupKey;
            ProductBaseData parent;

            // in case of a rate plan combination take the groupKey and techKey of the parent
            if (oldProduct.isRatePlanCombination() &&
                oldProduct.getHierarchyLevel() > 1 ) {
                    parent = oldProduct.getParent();
                    oldGroupKey = parent.getGroupKey();
                    oldTechKey = parent.getParent().getTechKey().getIdAsString();
            } 
            else {  
                    oldGroupKey = oldProduct.getGroupKey();
                    oldTechKey = oldProduct.getParent().getTechKey().getIdAsString();
            }
            if (newProduct.isRatePlanCombination() &&
                newProduct.getHierarchyLevel() > 1 ) {
                    parent = newProduct.getParent();
                    newGroupKey = parent.getGroupKey();
                    newTechKey = parent.getParent().getTechKey().getIdAsString();
            } 
            else {  
                    newGroupKey = newProduct.getGroupKey();
                    newTechKey = newProduct.getParent().getTechKey().getIdAsString();
            }
            
            if (oldGroupKey == null) {
                oldGroupKey = ""; 
            }
            if (newGroupKey == null) {
                newGroupKey = ""; 
            }
        
            if (!oldGroupKey.equals(newGroupKey)) {
                isChangeOfArea = true;
                if (log.isDebugEnabled())
                    log.debug(METHOD + ": groupKey different: oldGroupKey = " + oldGroupKey + " / newGroupKey = " + newGroupKey);
            }
            // same group but parent item changed 
            else if (!oldTechKey.equals(newTechKey)) {
                isChangeOfArea = true;
                if (log.isDebugEnabled())
                    log.debug(METHOD + ": TechKey different: oldTechKey = " + oldTechKey + " / newTechKey = " + newTechKey);
            }
        }

        if (log.isDebugEnabled()) {
            log.debug(METHOD + ": isChangeOfArea = " + isChangeOfArea);
        }
        
        log.exiting();

        return isChangeOfArea;
    }

    /**
     * The flag indicates if the current group is collapsed.  
     *
     * @return isGroupCollapsed of type boolean
     */
    public boolean isGroupCollapsed() {
        final String METHOD = "isGroupCollapsed()";
        log.entering(METHOD);
        boolean retVal = false;

        if (groupKey != null
            && groupKey.length() > 0
            && productList.getItemGroup(groupKey) != null) {
            retVal = productList.getItemGroup(groupKey).displayCollapsed();
        }

        if (log.isDebugEnabled()) {
            log.debug(METHOD + ": group collapsed = " + retVal);
        }
        log.exiting();
        return retVal;
    }

    /**
     * Set the flag if a group is collapsed  
     *
     * @param groupKey as String
     *        collapsed as boolean, flag indicates if group was set to 
     *        collapsed, if true or
     *        expanded, if false 
     */
    public void setCollapsedFlag(String groupKey, boolean collapsed) {
        final String METHOD = "setCollapsedFlag()";
        log.entering(METHOD);

        if (groupKey != null
            && groupKey.length() > 0
            && productList.getItemGroup(groupKey) != null) {
            productList.getItemGroup(groupKey).setDisplayCollapsed(collapsed);
        }

        if (log.isDebugEnabled()) {
            log.debug(METHOD + ": group = " + groupKey + " / collapse flag = " + collapsed);
        }
        log.exiting();
    }

    /**
     * Returns true, if the group the item belongs to contains 
     * more than one item.
     * 
     * The method returns true, if the items group contains more than 
     * one entry.
     *
     * @param index the index of the item
     */
    public boolean itemGroupHasMoreThanOneEntry(int index) {

        final String METHOD = "itemGroupHasMoreThanOneEntry()";
        log.entering(METHOD);

        boolean retVal = false;

        if (index <= size()) {
            ProductBaseData component = getProduct(index);

            if (component != null) {
                ProductBaseData product = component;
                // check group of parent if product is a rate plan combination
                if (component.isRatePlanCombination()) {
                    product = component.getParent();
                    if (log.isDebugEnabled()) {
                        log.debug("Check parent of rate plan combination");
                    }
                }

                if (product.getGroupKey() != null
                    && product.getGroupKey().length() > 0) {

                    ProductBaseData parent = product.getParent();
                    String lTechKey = parent.getTechKey().getIdAsString();
                    String lGroupKey = product.getGroupKey() + lTechKey;
                        
                    Integer groupSize = (Integer) groupSizesMap.get(lGroupKey);

                    if (groupSize != null) {
                        retVal = groupSize.intValue() > 1;
                    } else {
                        if (log.isDebugEnabled()) {
                            log.debug("Group size not found for GroupKey: " + lGroupKey);
                        }
                    }
                } else {
                    if (log.isDebugEnabled()) {
                        log.debug("Product: " + product.getDescription() + " has no valid group");
                    }
                }
            } else {
                if (log.isDebugEnabled()) {
                    log.debug("Product not found for index: " + index);
                }
            }
        } else {
            if (log.isDebugEnabled()) {
                log.debug("Index " + index + " is greater than size of SubItemlist size: " + size());
            }
        }

        log.exiting();
        return retVal;
    }

    /**
     * Returns the current group key  
     *
     * @return groupKey of type String
     */
    public String getGroupKey() {
        final String METHOD = "getGroupKey()";
        log.entering(METHOD);
        if (log.isDebugEnabled()) {
            log.debug(METHOD + ": groupKey = " + groupKey);
        }
        log.exiting();
        return groupKey;
    }

    /**
     * Get size of ItemList.
     * Implements the method of ProductListBaseData
     * 
     * @return size number of items in this ItemList.
     */
    public int size() {

        final String METHOD = "size";
        log.entering(METHOD);

        int size = 0;

        if (log.isDebugEnabled()) {
            log.debug(METHOD + " - listType: " + listType);
        }
        switch (listType) {
            case 1 :
                size = productList.size();
                break;
            default :
                }

        log.exiting();
        return (size);
    }

    /**
     * Determine if the product is optional, part of a group and the group has more at least 2 optional products
     * - product is optional
     * - product is part of a group with at least 2 optional products
     * 
     * @param  index of the product
     * 
     * @return boolean value
     */
    public boolean isGroupWith2OptProducts(int index) {

        final String METHOD = "isGroupWith2OptProducts(int index)";
        log.entering(METHOD);
        boolean retVal = false;

        ProductBaseData product = getProduct(index);

        if (product != null && product.isOptional() && product.isPartOfAGroup()) {
            ProductBaseData parent = product.getParent();
            String lTechKey = parent.getTechKey().getIdAsString();
            String lGroupKey = product.getGroupKey() + lTechKey;
                        
            Integer groupNumbOptItems = (Integer) groupNumbOptItemsMap.get(lGroupKey);

            if (log.isDebugEnabled()) {
                log.debug(METHOD + ": Product = " + product.getDescription() + " / groupKey = " + lGroupKey + " / number of optional items = " + groupNumbOptItems);
            }

            retVal = groupNumbOptItems.intValue() > 1;
        }
        if (log.isDebugEnabled()) {
            log.debug(METHOD + " - return: " + retVal);
        }
        log.exiting();
        return retVal;
    }

    /**
     * Determine if the selection button is displayed as Radio button
     * - product is optional
     * - product is part of a group with a least 2 optional products
     * 
     * @param  index of the product
     * 
     * @return boolean value
     */
    public boolean isSelectionAsRadioButton(int index) {

        final String METHOD = "isSelectionAsRadioButton";
        log.entering(METHOD);
        boolean retVal = isGroupWith2OptProducts(index);
        log.exiting();
        return retVal;
    }

    /**
     * Return the Solution Configurator selection flag as String
     * 
     * @return selection as string for web
     */
    public String getSCSelectionString(ProductBaseData product) {

        final String METHOD = "getSCSelectionString()";
        log.entering(METHOD);
        String retVal = "";

        if (product.getAuthorFlag()) {
            retVal = "checked";
        }
        if (log.isDebugEnabled()) {
            log.debug(METHOD + ": return: " + retVal);
        }
        log.exiting();
        return retVal;
    }

    /**
     * Determine if the selection button is displayed as Radio button
     * 
     * @return product for which the flag needs to be set
     *         selectFlag as String value
     */
    public void setSCSelectionString(
        ProductBaseData product,
        String selectFlag) {

        final String METHOD = "setSCSelectionString()";
        log.entering(METHOD);
        boolean retVal = false;

        if (selectFlag.equals("checked")) {
            retVal = true;
        }
        product.setAuthorFlag(retVal);
        if (log.isDebugEnabled()) {
            log.debug(METHOD + ": Selection Flag = " + retVal);
        }
        log.exiting();
    }

    /**
     * Return a flag indicating if the compare checkboxes are displayed.
     * Compare fields and buttons are displayed if
     * - the group contains at least two optional fields 
     * 
     * @param  index of the product
     * 
     * @return boolean value, true if compare boxes and buttons are displayed, false else. 
     */
    public boolean isCompCheckboxDisplayed(int index) {

       final String METHOD = "isCompCheckboxDisplayed";
       log.entering(METHOD);
       boolean retVal = isGroupWith2OptProducts(index);
       log.exiting();
       return retVal;
    }

    /**
     * Return a flag indicating if the compare checkboxes are displayed.
     * Compare fields and buttons are displayed if
     * - the area is an old group 
     * - the group is expanded
     * - the group contains at least two optional fields 
     * 
     * @return boolean value, true if compare boxes and buttons are displayed, false else. 
     */
    public boolean isCompCheckboxDisplayed(ProductBaseData product) {

        final String METHOD = "isCompCheckboxDisplayed()";
        log.entering(METHOD);

        boolean retVal = false;

        if (product.isPartOfAGroup()) {
            WebCatSubItem subItem;
            String locGroupKey = null;
            int count = 0;
            if (log.isDebugEnabled()) {
                log.debug(METHOD + ": group key = " + this.groupKey);
            }

            for (int i = 0; i < productList.size(); i++) {
                subItem = productList.getSubItem(i);
                locGroupKey = subItem.getGroupKey();
                if (log.isDebugEnabled()) {
                    log.debug(METHOD + ": i = " + i + " / Product = " + subItem.getDescription() + " / groupKey = " + locGroupKey);
                }
                if (locGroupKey == null
                    || locGroupKey.length() == 0
                    || !locGroupKey.equals(this.groupKey)) {
                    continue;
                }

                count++;
                if (count == 2) {
                    retVal = true;
                    break;
                }
            }

            if (log.isDebugEnabled()) {
                log.debug(METHOD + ": count = " + count);
                log.debug(METHOD + ": display compare fields and buttons = " + retVal);
            }
        }

        if (log.isDebugEnabled()) {
            log.debug(METHOD + ": product = " + product.getDescription() + " / compare check box displayed = " + retVal);
        }
        log.exiting();
        return retVal;
    }

    /**
     * Return a flag indicating if a NoSelection line will be generated in the JSP. 
     * The flag is set if it is a group with group key and one of the following conditions are valid:
     * a) the group does not contain a group element which is default,
     * b) it is a optional group independent if a group element is defined as default value. 
     * c) if group has atleast 2 optional products
     * 
     * @param  index of the product
     * 
     * @return boolean value, true if one of the above conditions are true, 
     *                        false otherwise. 
     */
    public boolean isNoSelectionDisplayed(int index) {

        final String METHOD = "isNoSelectionDisplayed(int index)";
        log.entering(METHOD);

        boolean isNoSelDisp = false;
        boolean isGrp = false;
        boolean isOptionalGrp = false;
        boolean hasDefVal = false;
        boolean has2OptProducts = false;
        WebCatSubItem firstSubItem = productList.getSubItem(index);
        WebCatSubItem subItem;
        ProductBaseData parent = firstSubItem.getParent();
        String lTechKey = parent.getTechKey().getIdAsString();
        String groupKeyExt = this.groupKey + lTechKey;

        if (log.isDebugEnabled()) {
            log.debug(METHOD + ": group key = " + groupKeyExt);
        }

        Integer groupNumbOptItems = (Integer) groupNumbOptItemsMap.get(groupKeyExt);
        
        if (groupNumbOptItems != null
            && groupNumbOptItems.intValue() > 1 
            && this.groupKey != null 
            && this.groupKey.length() > 0) {
            String locGroupKey = null;
            for (int i = 0; i < productList.size(); i++) {
                subItem = productList.getSubItem(i);
                locGroupKey = subItem.getGroupKey();
                if (locGroupKey == null
                    || locGroupKey.length() == 0
                    || !locGroupKey.equals(this.groupKey))
                    continue;
                // check if item is a sales component
                if (!subItem.isSalesComponent() &&
                    !subItem.isDependentComponent()) {
                    continue;
                }
                isGrp = true;
                // check if group is optional or mandatory
                if (!subItem.isPartOfMandatoryGroup()) {
                    isOptionalGrp = true;
                }

                // check if item is selected or
                if (subItem.isScSelected()) {
                    hasDefVal = true;
                }

                // check if item is part of group with at least 2 optional products
                if (isGroupWith2OptProducts(i)) {
                    has2OptProducts = true;
                }
            }
        }

        isNoSelDisp = isGrp && has2OptProducts && (isOptionalGrp || !hasDefVal);
        if (log.isDebugEnabled()) {
            log.debug(METHOD + ": group = " + this.groupKey + " / is a group = " + isGrp + " / has at least 2 opt Products = " + has2OptProducts);
            log.debug(METHOD + ": has default values = " + hasDefVal + " / is optional group = " + isOptionalGrp);
            log.debug(METHOD + ": NoSelection line is displayed = " + isNoSelDisp);
        }

        log.exiting();
        return isNoSelDisp;
    }

    /**
     * Sets a message
     * 
     * @param product 
     *        completeMsg - message which will be displayed for a complete configuration 
     *        incompletMsg - message which will be displayed for an incomplete configuration 
     *        complClass - message class which will be used to show the complete message 
     *        incomplClass - message class which will be used to show the incomplete message 
     * 
     */
    public void setMessageText(String msgText) {

        final String METHOD = "setMessageText()";
        final int ERROR = 2;

        log.entering(METHOD);

        MessageList msgList = new MessageList();
        Message msg = new Message(ERROR, msgText);

        msgList.add(msg);

        pageContext.setAttribute(AttributeKeyConstants.MESSAGE_LIST, msgList);

        if (log.isDebugEnabled()) {
            log.debug(METHOD + ": message = " + msg.getDescription());
        }
        log.exiting();
    }

    /**
     * Checks if the group is a mandatory group without default values
     *
     * @param  index of the product
     * 
     * @return true if group is a mandatory group without defaults, otherwise false  
     */
    public boolean isMandatoryGrpWODefaults(int index) {
        String method = "isMandatoryGrpWODefaults(int index)";
        log.entering(method);

        WebCatSubItem subItem;
        boolean retVal = false;
        
        ProductBaseData product = getProduct(index);
        ProductBaseData parent = product.getParent();
        String lTechKey = parent.getTechKey().getIdAsString();
        String lGroupKey = product.getGroupKey() + lTechKey;

        if (lGroupKey != null && lGroupKey.length() > 0) {
            
            String groupIsMandatory = (String) groupIsMandatoryMap.get(lGroupKey);
            if (groupIsMandatory != null && groupIsMandatory.equals("X")) {
                
                String groupHasSelectedItem = (String) groupHasSelectedItemMap.get(lGroupKey);
                if (groupHasSelectedItem == null ||
                    (groupHasSelectedItem != null && groupHasSelectedItem.equals("0"))) {
                    retVal = true;
                }
            }
        }
        if (log.isDebugEnabled()) {
            log.debug(method + ": group = " + lGroupKey + " / is mandatory group without defaults = " + retVal);
        }
        log.exiting();
        return (retVal);
    }

    /**
     * Checks if the package is consistent
     *
     * @return true if package is correct  
     */
    public boolean isPackageCorrect() {
        final String METHOD = "isPackageCorrect()";
        log.entering(METHOD);
        boolean isCorrect = true;
        WebCatSubItem subItem;
        String lGroupKey = "";
        String lGroupKeyTmp = "";
        boolean isScSelected;
        ProductBaseData parent = null;
        String lTechKey = null;
        
        for (Iterator it = groupIsMandatoryMap.entrySet().iterator(); (it.hasNext() && isCorrect); ) {
            Map.Entry e = (Map.Entry) it.next();
            lGroupKey = (String) e.getKey();
            isScSelected = false;
            if (log.isDebugEnabled()) {
                log.debug(METHOD + ": check mandatory group = " + lGroupKey);
            }
            
            for (int i = 0; i < productList.size() && !isScSelected; i++) {
                subItem = productList.getSubItem(i);
                parent = subItem.getParent();
                lTechKey = parent.getTechKey().getIdAsString();
                
                lGroupKeyTmp = subItem.getGroupKey() + lTechKey;

                if ( (lGroupKeyTmp == null || lGroupKeyTmp.length() == 0) || 
                     !lGroupKey.equals(lGroupKeyTmp)) {
                    continue; 
                }
                isScSelected = subItem.isScSelected();
            }
            
            isCorrect = isScSelected;
            if (log.isDebugEnabled()) {
                log.debug(METHOD + ": is a product selected for this mandatory group = " + isScSelected);
            }
        }
        
        if (log.isDebugEnabled()) {
            log.debug(METHOD + ": package is correct = " + isCorrect);
            log.exiting();
        }    
        return (isCorrect);
    }

}