/**
 * Class:        CatalogBaseUI Copyright (c) 2006, SAP AG, All rights reserved. 
 * Author:       SAP AG
 * Created:      04.04.2006
 * Version:      1.0 
 * $Revision$ 
 * $Date$
 */
package com.sap.isa.catalog.uiclass;

import java.util.Hashtable;
import java.util.Iterator;
import java.util.StringTokenizer;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.servlet.jsp.PageContext;

import com.sap.ecommerce.businessobject.MarketingBusinessObjectManager;
import com.sap.ecommerce.businessobject.campaign.Campaign;
import com.sap.ecommerce.businessobject.campaign.CampaignBusinessObjectManager;
import com.sap.ecommerce.businessobject.loyalty.LoyaltyMembership;
import com.sap.isa.backend.boi.webcatalog.CatalogBusinessObjectsAware;
import com.sap.isa.backend.boi.webcatalog.CatalogConfiguration;
import com.sap.isa.businessobject.ProductBaseData;
import com.sap.isa.businessobject.webcatalog.CatalogBusinessObjectManager;
import com.sap.isa.businesspartner.backend.boi.PartnerFunctionData;
import com.sap.isa.businesspartner.businessobject.BusinessPartner;
import com.sap.isa.businesspartner.businessobject.BusinessPartnerManager;
import com.sap.isa.catalog.AttributeKeyConstants;
import com.sap.isa.catalog.actions.ActionConstants;
import com.sap.isa.catalog.webcatalog.WebCatInfo;
import com.sap.isa.core.businessobject.management.BOMInstantiationException;
import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.core.ui.context.ContextManager;
import com.sap.isa.core.util.JspUtil;
import com.sap.isa.core.util.Message;
import com.sap.isa.core.util.MessageList;
import com.sap.isa.core.util.WebUtil;
import com.sap.isa.isacore.uiclass.PricingBaseUI;

/**
 * This class is the base class for all catalog relevant UI classes
 */
public class CatalogBaseUI extends PricingBaseUI {
    
    static public IsaLocation log = IsaLocation.getInstance(CatalogBaseUI.class.getName());
    protected WebCatInfo theCatalog;
    protected String detailScenario = "";
    protected String displayScenario = "";    
    protected String isQuery;
    protected String domainKey = "catalog.isa.attribute";
    protected CatalogConfiguration catalogConfig = null;
    protected CatalogBusinessObjectsAware catalogObjBom = null;
    protected boolean showAddToLeaflet = false;
    protected boolean showAVWLink = false;
    protected boolean showCatListCUALink = false;
    protected boolean showMultiSelection = false;
    protected boolean showSelectProduct = false;

    /**
     * Create new Instance
     *
     * @param pageContext DOCUMENT ME!
     */
    public CatalogBaseUI(PageContext pageContext) {
        super(pageContext);

        log.entering("CatalogBaseUI()");

        if (userSessionData != null) {
            CatalogBusinessObjectManager bom =
                (CatalogBusinessObjectManager) userSessionData.getBOM(CatalogBusinessObjectManager.CATALOG_BOM);

            theCatalog = bom.getCatalog();

            displayScenario = getValueFromRequest(ActionConstants.RA_DISPLAY_SCENARIO);
            detailScenario = getValueFromRequest(ActionConstants.RA_DETAILSCENARIO);
            
            catalogObjBom = (CatalogBusinessObjectsAware) userSessionData.getMBOM().getBOMByType(CatalogBusinessObjectsAware.class);
        
            // now I can read the shop
            catalogConfig = catalogObjBom.getCatalogConfiguration();
        }

        isQuery = getValueFromRequest(ActionConstants.RA_IS_QUERY);

        if (catalogConfig != null) {
        
            showMultiSelection = catalogConfig.showCatMultiSelect();
        
            showAVWLink = catalogConfig.showCatAVWLink() && catalogConfig.isEAuctionUsed();
        
            showSelectProduct = catalogConfig.isStandaloneCatalog();
            
            showAddToLeaflet = catalogConfig.showCatAddoLeaflet();
            
			showCatListCUALink = catalogConfig.showCatListCUALink();
                }

        log.exiting();
    }

    /**
     * This method checks in the request if it contains an attriubte with the 
     * requested key, if not then it is checking for a parameter. If the parameter
     * also does not exists, then an empty string is returned.
     * 
     * @param requestKey The key to get the value from the request. 
     * @return An empty string of the value of attribute or parameter.
     */
    public String getValueFromRequest(String requestKey) {

    	String toReturn = (String) request.getAttribute(requestKey);

        if (null == toReturn) {
        	toReturn = (String) request.getParameter(requestKey);
        }

        if (null == toReturn) {
        	toReturn = "";
        }
    	return toReturn;
    }
    
    /**
     * Returns the hook url if one has been provided.
     * @return The hook url if one has been provided or an empty string.
     */
    public String getHookUrl() {
    	String hookUrl = "";
    	if (theCatalog != null) {
    		hookUrl = theCatalog.getHookUrl();
    	}
    	return hookUrl;
    }

    /**
     * Get the message list from the campaign BO
     *
     * @return Messagelist the list of messages of the cmapaign BO
     */
    public MessageList getCampaignMessageList() {
        log.entering("getCampaignMessageList()");

        MessageList msgList = null;
        CampaignBusinessObjectManager campaignBom =
            (CampaignBusinessObjectManager) userSessionData.getBOM(CampaignBusinessObjectManager.CAMPAIGN_BOM);

        if (campaignBom != null) {
            Campaign campaign = campaignBom.getCampaign();

            if (campaign != null) {
                if (campaign.getHeaderData() != null) {
                    msgList = campaign.getHeaderData().getMessageList();
                }
            }
        }

        log.exiting();

        return msgList;
    }

    /**
     * Sets the message and and returns the message class depending of the configuration status of a product
     * Is the configuration of a product complete the complete message will be set and the complete message class is returned.  
     * Is the configuration of a product incomplete the incomplete message will be set and the incomplete message class is returned.  
     * @param product 
     *        completeMsg - message which will be displayed for a complete configuration 
     *        incompletMsg - message which will be displayed for an incomplete configuration 
     *        complClass - message class which will be used to show the complete message 
     *        incomplClass - message class which will be used to show the incomplete message 
     * 
     * @return msgClass of type String. 
     */
    public String getMsgConfig(
        ProductBaseData product,
        String completeMsg,
        String incompleteMsg,
        String complClass,
        String incomplClass) {

        final String METHOD = "getMsgConfig()";
        final int INFO = 4;
        final int ERROR = 2;
        log.entering(METHOD);

        MessageList msgList = new MessageList();
        Message msg;
        String msgClass = "";

        if (product.isConfiguredCompletely()) {
            /*          msg = new Message(INFO, WebUtil.translate(pageContext, completeMsg, null)); */
            msg = new Message(INFO, completeMsg);
            msgClass = complClass;
        }
        else {
            msg = new Message(ERROR, incompleteMsg);
            msgClass = incomplClass;
        }
        msgList.add(msg);
        pageContext.setAttribute(AttributeKeyConstants.MESSAGE_LIST, msgList);

        if (log.isDebugEnabled()) {
            log.debug(METHOD + ": product = " + product.getDescription());
            log.debug(METHOD + ": message = " + msg.getDescription());
            log.debug(METHOD + ": message class = " + msgClass);
        }

        log.exiting();
        return msgClass;
    }

    /**
     * DOCUMENT ME!
     *
     * @return DOCUMENT ME!
     */
    public String getCurrentQuery() {

        log.entering("getCurrentQuery()");

        String query = request.getParameter(ActionConstants.RA_CURRENT_QUERY);

        if ((null == query) && (null != request.getAttribute(ActionConstants.RA_CURRENT_QUERY))) {
            query = (String) request.getAttribute(ActionConstants.RA_CURRENT_QUERY);
        }
        
        if(null == query) {
            ContextManager contextManager =  ContextManager.getManagerFromRequest(request);
            query = contextManager.getContextValue(ActionConstants.CV_CURRENT_QUERY);
        }

        if (null == query) {
            query = "";
        }

        if (null != query) {
            query = JspUtil.encodeHtml(query);
        }

        if (log.isDebugEnabled()) {
            log.debug("getCurrentQuery()=" + query);
        }
        log.exiting();

        return query;
    }

    /**
     * get the value of the request attribute or parameter {@link ActionConstants#RA_DETAILSCENARIO}.
     *
     * @return String value of {@link ActionConstants#RA_DETAILSCENARIO}.
     */
    public String getDetailScenario() {
        log.entering("getDetailScenario()");
        if (log.isDebugEnabled()) {
            log.debug("getDetailScenario()=" + detailScenario);
        }
        log.exiting();

        return detailScenario;
    }

    /**
     * get the value of the request attribute or parameter {@link ActionConstants#RA_DISPLAY_SCENARIO}.
     *
     * @return String value of {@link ActionConstants#RA_DISPLAY_SCENARIO}.
     */
    public String getDisplayScenario() {
        log.entering("getDisplayScenario()");
        if (log.isDebugEnabled()) {
            log.debug("getDisplayScenario()=" + displayScenario);
        }
        log.exiting();

        return displayScenario;
    }
    
    /**
     * DOCUMENT ME!
     *
     * @param itemIdHighlight DOCUMENT ME!
     * @param htTerms DOCUMENT ME!
     * @param key DOCUMENT ME!
     *
     * @return DOCUMENT ME!
     */
    public String getHighlightedResults(String itemIdHighlight, Hashtable htTerms, String key, String highlightedTag) {
        log.entering("getHighlightedResults()");

        String token = null;
        char charactersToSkip[] = { '"', '&', '#', '<', '>', ' ', '\n', '\'' };
        //char charactersToEscape[] = { '(', '[', '{', '\\', '^', '$', '|', ')', '?', '*', '+' };

        String highlightedString = JspUtil.encodeHtml(itemIdHighlight, charactersToSkip);
 
        //String highlightedString = JspUtil.encodeHtml(itemIdHighlight);
        if (htTerms != null) {
            String idTerm = (String) htTerms.get("QSEARCH");
            if (idTerm == null) {
                idTerm = (String) htTerms.get("ADVSEARCH");
            }
            StringTokenizer stTerms = null;
            Pattern patTerm = null;

            if (idTerm == null && key != null && key.trim().length() > 0) {
                idTerm = (String) htTerms.get(key);
            }

            if (idTerm != null) {
                idTerm = idTerm.replace('*', ' ');

                // escape reg.exp. characters
                //idTerm = escapeRegularExpressionCharacters(idTerm);
                idTerm = convertMetaCharacters(idTerm);
                 
                stTerms = new StringTokenizer(idTerm);
			
                while (stTerms.hasMoreTokens()) {
                    token = stTerms.nextToken();
                    if(token.equals("<") || token.equals(">") || token.equals("@")) {
                    	continue;
                    }
                    
                    patTerm = Pattern.compile(token, Pattern.CASE_INSENSITIVE);
					
                    Matcher matTerm = patTerm.matcher(highlightedString);

                    if (matTerm.find()) {
						matTerm = patTerm.matcher(highlightedString);
						if(matTerm.find()){
							String match = matTerm.group();
							
                        	if (!(match == null || match.equalsIgnoreCase(""))) {
                        		//we shouldn't replace within the html tag - that's why we first insert <@> or </@>
                            	highlightedString = matTerm.replaceAll("<@>" + match + "<@@>");
                        	}
                        }
                    }
                }
				highlightedString = highlightedString.replaceAll("<@>","<div class=\""+highlightedTag+"\">");
				highlightedString = highlightedString.replaceAll("<@@>","</div>");
            }
        }

        log.exiting();

        return userExitHighlightedResults(highlightedString);
    }

    /**
      * Masks regular expression characters in a <code>String</code>, so that they
      * are not interpreted by Pattern.compile(). 
      *
      * @param input the string to be escaped
      * @return a String without special charcters of regular expressions
      */
    public static String escapeRegularExpressionCharacters(String input) {
        // This method is optimized for speed, so don't whine about the
        // programming style
        final String charsToEscape = "([{\\^$|)?*+";
        
        int length = input.length();
        char[] inputBuff = input.toCharArray();
        char[] outputBuff = new char[length * 2];
        int size = 0;

        for (int i = 0; i < length; i++) {
            
            if (charsToEscape.indexOf(inputBuff[i]) >= 0) {
                outputBuff[size++] = '\\';
                outputBuff[size++] = inputBuff[i];
            }
            else {
                outputBuff[size++] = inputBuff[i];
            }
        }

        return new String(outputBuff, 0, size);
    }

    /**
     * Trys to get the attribute itemAttributesIterator from the request. 
     *
     * @return The attribute iterator is available in the current request. Otherwise null is returned.
     */
    public Iterator getItemAttributesIterator() {
        log.entering("getItemAttributesIterator()");
        log.exiting();
        return (Iterator) request.getAttribute("itemAttributesIterator");
    }

    /**
     * Returns an instance of the current catalog.
     *
     * @return An instance of the current catalog.
     */
    public WebCatInfo getTheCatalog() {
        log.entering("getTheCatalog()");
        log.exiting();

        return theCatalog;
    }

    /**
     * Checks if the variable {@link #isQuery} is set to yes or not.
     *
     * @return <code>true</code> if the variable {@link #isQuery} is set to {@link ActionConstants#RA_IS_QUERY_VALUE_YES} otherwise <code>false</code> is returned.
     */
    public boolean isCatalogQuery() {
        log.entering("isCatalogQuery()");
        log.exiting();
        return (isQuery != null && isQuery.equals(ActionConstants.RA_IS_QUERY_VALUE_YES));
    }

    /**
     * DOCUMENT ME!
     *
     * @return DOCUMENT ME!
     */
    public boolean isShopLeadCreationAllowed() {

        log.entering("isShopLeadCreationAllowed()");

        boolean retVal = false;
        String bp = catalogObjBom.getCatalogUser().getUserId();

        try {
            BusinessPartnerManager buPaMa = catalogObjBom.createBUPAManager();
            BusinessPartner soldTo = buPaMa.getDefaultBusinessPartner(PartnerFunctionData.SOLDTO);

            if (soldTo != null) {
                bp = soldTo.getId();
            }

            retVal = catalogConfig.isLeadCreationAllowed() && bp != null && bp.length() > 0;
        }
        catch (Exception ex) {
            log.debug("Test failed ", ex);
        }

        // if true check additonally the area type 
        if (retVal) { 
            // lead creation is not allowed if it's no query and the
            // area is an buy points area or reward area
            if (this.isBuyPointsArea() || this.isRewardArea()) {
                retVal = false;
            }
        }

        if (log.isDebugEnabled()) {
            log.debug("isShopLeadCreationAllowed()=" + retVal);
        }
        log.exiting();

        return retVal;
    }

    /**
     * Returns true if quantity and unit for the given product can be changed
     * false otherwise. At the moment for all products that can can be exploded 
     * by the Solution Configurator false is returned, for all othjer products true.
     * 
     * @return true if quantity and unit for the given product can be changed
     *         false otherwise
     */
    public boolean isQuantityChangeable(ProductBaseData product) {

        log.entering("isQuantityChangeable()");
        
        boolean isChangeable = !(product.isSalesPackage() || product.isCombinedRatePlan() || product.isRatePlan()) ;

        if (log.isDebugEnabled()) {
            log.debug("isQuantityChangeable()=" + isChangeable);
        }
        log.exiting();

        return isChangeable;
    }

    /**
     * DOCUMENT ME!
     *
     * @param key DOCUMENT ME!
     *
     * @return DOCUMENT ME!
     */
    public String parseAttributeName(String key) {
        log.entering("parseAttributeName(String key)");

        String text = parseAttributeName(domainKey, key);

        log.exiting();

        return text;
    }
    
    /**
     * Returns true, if the link to AVW Auctions should be dsiplayed
     *
     * @return true   if the link to AVW Auctions should be dsiplayed
     */
    public boolean showAVWLink() {

        log.entering("showAVWLink()");

        if (log.isDebugEnabled()) {
            log.debug("showAVWLink: "  + showAVWLink);
        }

        log.exiting();

        return showAVWLink;
    }

	/**
	* Returns true, if the link to AVW Auctions should be dsiplayed
	*
	* @return true   if the link to AVW Auctions should be dsiplayed
	*/
	  public boolean showCatListCUALink() {
	     log.entering("showCatListCUALink()");

	     if (log.isDebugEnabled()) {
		   log.debug("showCatListCUALink: "  + showCatListCUALink);
	     }
                
	     log.exiting();
	     return showCatListCUALink;
      }
    
    /**
     * Returns true, if contract data should be displayed
     * 
     * @return true   if contract data should be displayed,
     *         false  otherwise
     */
    public boolean showSelectedProduct() {
        log.entering("showSelectedProduct");

        if (log.isDebugEnabled()) {
            log.debug("showSelectedProduct" + showSelectProduct);
        }
        log.exiting();
        
        return showSelectProduct;
    }
    
    /**
     * Returns true, if multi selection buttons
     * 
     * @return true   if multi selection buttons,
     *         false  otherwise
     */
    public boolean showMultiSelection() {
        log.entering("showMultiSelection");

        if (log.isDebugEnabled()) {
            log.debug("showMultiSelection" + showMultiSelection);
        }
        log.exiting();
        
        return showMultiSelection;
    }


    /**
     * DOCUMENT ME!
     *
     * @param domainKey DOCUMENT ME!
     * @param key DOCUMENT ME!
     *
     * @return DOCUMENT ME!
     */
    public String parseAttributeName(String domainKey, String key) {
        log.entering("parseAttributeName(String domainKey, String key)");

        String text = WebUtil.translate(pageContext, domainKey + "." + key, null);

        if (text != null && (text.equals("null") || text.indexOf("???") == 0)) {
            text = null;
        }

        log.exiting();

        return text;
    }

    /**
     * DOCUMENT ME!
     *
     * @param itemIdHighlight DOCUMENT ME!
     *
     * @return DOCUMENT ME!
     */
    public String userExitHighlightedResults(String itemIdHighlight) {
        log.entering("userExitHighlightedResults()");
        log.exiting();

        return itemIdHighlight;
    }
    
    /**
     * Flag to indicate, if add to leaflet is allowed
     *
     * @return boolean true if add to leaflet is allowed,
     *                 false else
     */
    public boolean isAddToLeafletAllowed() {
        log.entering("isAddToLeafletAllowed()");
        boolean isAllowed = this.showAddToLeaflet; 
 
        // add to leaflet button is only displayed if it's no query and the
        // area is an buy points area or reward area
        if (!isCatalogQuery() &&
            (this.isBuyPointsArea() || this.isRewardArea())) {
                isAllowed = false;
        }
        
        if (log.isDebugEnabled()) {
            log.debug("display add to leaflet button = " + isAllowed);
        }

        log.exiting();        
        return isAllowed;
    }
     
    /**
     * Returns the points unit for the current price product
     * @return ptsUnit, points unit as String
     */
    public String getPointsUnit() {
        log.entering("getPointsUnit()");
        String ptsUnit = null;

        MarketingBusinessObjectManager mktBom = (MarketingBusinessObjectManager) userSessionData.getBOM(MarketingBusinessObjectManager.MARKETING_BOM);
        if(mktBom != null){
	        LoyaltyMembership loyMembership = mktBom.getLoyaltyMembership();
    	    if (loyMembership != null                   &&
        	    loyMembership.getPointAccount() != null) {  
            	ptsUnit = loyMembership.getPointAccount().getPointCodeId();  
	        }
        }
    	log.exiting();
        return ptsUnit;
    }

    /**
     * Returns the language dependen description of the points unit for the current price product
     * @return ptsUnitDescr, description of points unit as String
     */
    public String getPointsUnitDescr() {
        log.entering("getPointsUnitDescr()");
        String ptsUnitDescr = null;

		MarketingBusinessObjectManager mktBom = null;
		try{
			mktBom = (MarketingBusinessObjectManager) userSessionData.getBOM(MarketingBusinessObjectManager.MARKETING_BOM);
		}
		catch(BOMInstantiationException e){
			log.error("Can not access to MARKETING-BOM");
		}
        
        if(mktBom != null){
	        LoyaltyMembership loyMembership = mktBom.getLoyaltyMembership();
    	    if (loyMembership != null &&
        	    loyMembership.getPointAccount() != null) {  
            	ptsUnitDescr = loyMembership.getPointAccount().getDescr();  
	        }
        }  
    	log.exiting();
        return ptsUnitDescr;
    }

    /**
     * Return if the quantity column is visible in the catalogue . 
     * 
     * @return <code>true</code> if the quantity column should be displayed.
     */
    public boolean isQuantityVisible() {
        log.entering("isQuantityVisible()");
        boolean isVisible = true;
        
        // column will not be displayed if it's no query and the
        // area is an buy points area
        if (!isCatalogQuery() &&
            this.isBuyPointsArea()) {
            isVisible = false;
        }
        
        if (log.isDebugEnabled()) {
            log.debug("is quantity column visible = " + isVisible);
        }

        log.exiting();        
        return isVisible;        
    }

    /**
	 * adds a / to all metacharacters of a string in case those are no real
	 * regex
	 * @param idTerm
	 * @return String with / before meta character
	 */
	private String convertMetaCharacters(String idTerm){		    

		char [] metaCharacters = {'*', '+', '?', '.', '(', ')', '[', ']', '{', '}', '/', '|', '^', '$'};
		char [] charIdTerm = idTerm.toCharArray();
		String convIdTerm = "";
		
		for(int i=0; i<charIdTerm.length; i++){
			for(int j=0; j<metaCharacters.length; j++){
				if(charIdTerm[i] == metaCharacters[j]){
					convIdTerm += "\\";
				}
			}
			convIdTerm += charIdTerm[i];
		}
		
		return convIdTerm;
	}
	
	/**
	 * The method determines the detail scenario specific to the Catalog, Request and
	 * type. 
	 */
	public void setTypeSpecificDetailScenario() {
		WebCatInfo catalog = this.getTheCatalog();
		String type = this.getValueFromRequest(ActionConstants.RA_SCENARIO_TYPE);

		if(this.getDetailScenario().length() == 0 && 
				(type.equals(WebCatInfo.ITEMDETAILS) || type.equals(WebCatInfo.ITEMDETAILSQUERY))
		   ){
        	String lastVisit = catalog.getLastVisited();
        	if(lastVisit.equals(WebCatInfo.ITEMDETAILS)) {
        		// this means the application wants to display item details.
        		// for this we need to get the scenario where the application came from to display the
        		// item details.
        		lastVisit = catalog.getLastUsedDetailScenario();
        		if(  lastVisit != null &&
        			!lastVisit.equals(WebCatInfo.ITEMDETAILS) && 
        			!lastVisit.equals(ActionConstants.DS_PRODUCTS) &&
        			!lastVisit.equals(ActionConstants.RA_ITEMLIST_EBP)) {
        			// also put it into the request in case the value is needed somewhere else as well.
        			request.setAttribute(ActionConstants.RA_DETAILSCENARIO, lastVisit);
        		}
        	}
        } else if(this.getDetailScenario().equals(ActionConstants.DS_CUA) && type.equals(WebCatInfo.ITEMDETAILS) && this.getDisplayScenario().equals("")){
//        	request.setAttribute(ActionConstants.RA_DISPLAY_SCENARIO, ActionConstants.RA_CURRENT_QUERY);
        	this.displayScenario = ActionConstants.RA_CURRENT_QUERY;
        }
	}
}