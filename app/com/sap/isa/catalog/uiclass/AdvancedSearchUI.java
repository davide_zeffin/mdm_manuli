/*****************************************************************************
    Class:        AdvancedSearchUI
    Copyright (c) 2008, SAP AG, Germany, All rights reserved.
    Author:       SAP AG
    Created:      20.03.2008
    Version:      1.0

    $Revision: #1 $
    $Date: 2008/03/20 $ 
*****************************************************************************/

package com.sap.isa.catalog.uiclass;

import java.util.HashMap;

import javax.servlet.jsp.PageContext;

import com.sap.ecommerce.businessobject.MarketingBusinessObjectManager;
import com.sap.ecommerce.businessobject.loyalty.LoyaltyMembership;
import com.sap.isa.backend.boi.isacore.BaseConfiguration;
import com.sap.isa.catalog.webcatalog.WebCatArea;
import com.sap.isa.catalog.webcatalog.WebCatAreaList;
import com.sap.isa.core.ui.layout.UILayoutManager;
import com.sap.isa.core.util.JspUtil;

/**
 * The class AdvancedSearchUI. <br>
 *  
 * @author SAP
 * @version 1.0
 **/
public class AdvancedSearchUI extends CatalogBaseUI {
       
    private final String LAYOUT_ACCOUNT = "accountLayout";

    private boolean isAdvSStdCat = true;
    private boolean isAdvSRewCat = false;
    private boolean isCatSelectDisp = false;
    private String currStdAreaId = null;
    private String currRewAreaId = null;
    private String advSQuery = null;
    public WebCatAreaList stdAreas = null;
    public WebCatAreaList rewAreas = null;
    private HashMap advSSelAtt = null;
    
    /**
     * @param pageContext
     */
    public AdvancedSearchUI(PageContext pageContext) {
        super(pageContext);
        
        boolean backendCRM = true;
         
        WebCatAreaList catalogAreas = (WebCatAreaList) request.getAttribute("stdAreas");

        if (catalogConfig != null && catalogConfig.getBackend() != null) {
             String backend = catalogConfig.getBackend();
             if (backend.equals(BaseConfiguration.BACKEND_CRM)) {
                backendCRM = true;
             }
        }
        
        // set UI settings for CRM shop        
        if (backendCRM) {  
        	if(catalogConfig.isEnabledLoyaltyProgram()){
                // reward catalog settings only if user has Loyalty Membership
                MarketingBusinessObjectManager mktBom = (MarketingBusinessObjectManager) userSessionData.getBOM(MarketingBusinessObjectManager.MARKETING_BOM);
                LoyaltyMembership loyaltyMembership = mktBom.getLoyaltyMembership();
    
                if (loyaltyMembership != null && loyaltyMembership.exists()) {
                    // display catalog selection 
                    this.isCatSelectDisp = true;
                     
                    // initialize catalog selection flags
    //                String searchRewCat = (String) request.getAttribute("searchRewardCatalog");
                     
                    // first call
                    UILayoutManager uiManager = UILayoutManager.getManagerFromRequest(request);
                    if (uiManager.getCurrentUILayout() != null) {
                        String layoutName = uiManager.getCurrentUILayout().getName();
                        if (layoutName != null && layoutName.equals(this.LAYOUT_ACCOUNT)) {
                            this.isAdvSRewCat = true;
                            this.isAdvSStdCat = false;
                        }
                    }
                    // set reward areas
                    request.setAttribute("rewAreas", catalogAreas);
                }
        	}

            // Advanced Search Query String
            String attr = (String) request.getAttribute("advSQuery");
            if (attr != null) {
                advSQuery = attr.toString();
            }
         
            // Standard Catalog search         
            attr = (String) request.getAttribute("advSStdCat");
            if (attr != null) {
                isAdvSStdCat = attr.equals("X") || attr.equals("");
            }
         
            // Reward Catalog search         
            attr = (String) request.getAttribute("advSRewCat");
            if (attr != null) {
                isAdvSRewCat = attr.equals("X");
            }
            
            advSSelAtt = (HashMap) request.getAttribute("advSSelAtt");
            
            // Standard Area
            attr = (String) request.getAttribute("advSStdAreaId");
            if (attr != null) {
                currStdAreaId = attr.toString();
            }
         
            // Reward Area
            attr = (String) request.getAttribute("advSRewAreaId");
            if (attr != null) {
                currRewAreaId = attr.toString();
            }
        }
    }
         
    /**
      * Returns true if the default standard area is selected
      * @return isSelected, true if default area is selected
      */
     public boolean isDefStdAreaSelected() {
         boolean isSelected = false;
         isSelected = (this.currStdAreaId == null || this.currStdAreaId.length() == 0);
         return isSelected;
     }

    /**
      * Returns true if the default reward area is selected
      * @return isSelected, true if default area is selected
      */
     public boolean isDefRewAreaSelected() {
         boolean isSelected = false;
         isSelected = (this.currRewAreaId == null || this.currRewAreaId.length() == 0);
         return isSelected;
     }

    /**
      * Checks if the search should be done primarly in the standard catalog 
      * @return isSearchRewCat as boolean
      */
     public boolean isSearchStdCat() {
         final String METHOD_NAME = "isSearchStdCat()";
         log.entering(METHOD_NAME);

         if (log.isDebugEnabled()) {
             log.debug(METHOD_NAME + ": search in standard catalog =" + this.isAdvSStdCat);
         }

         log.exiting();
         return this.isAdvSStdCat;
     }        

    /**
      * Checks if the search should be done primarly in the reward catalog 
      * @return isSearchRewCat as boolean
      */
     public boolean isSearchRewCat() {
         final String METHOD_NAME = "isSearchRewCat()";
         log.entering(METHOD_NAME);

         if (log.isDebugEnabled()) {
             log.debug(METHOD_NAME + ": search in reward catalog =" + this.isAdvSRewCat);
         }

         log.exiting();
         return this.isAdvSRewCat;
     }        
     
    /**
      * Checks if the catalog selection should be displayed 
      * @return isCatSelectDisp as boolean
      */
     public boolean isCatSelectDisp() {
         final String METHOD_NAME = "isCatSelectDisp()";
         log.entering(METHOD_NAME);

         if (log.isDebugEnabled()) {
             log.debug(METHOD_NAME + ": selection of catalogs is displayed =" + this.isCatSelectDisp);
         }

         log.exiting();
         return this.isCatSelectDisp;
     }

    /**
      * Returns the information if a standard area is selected 
      * @param  areaId, id of the standard area as String
      * @return isSelected as boolean
      */
     public boolean isStdAreaSelected(String areaId) {
         final String METHOD_NAME = "isStdAreaSelected(areaId)";
         log.entering(METHOD_NAME);

         boolean isSelected = false; 
         if (this.currStdAreaId != null && this.currStdAreaId.equals(areaId)) {
             isSelected = true; 
         }

         if (log.isDebugEnabled()) {
             log.debug(METHOD_NAME + ": is area selected = " + isSelected);
         }

         log.exiting();
         return isSelected;
     }
     
    /**
      * Returns the information if a reward area is selected 
      * @param  areaId, id of the reward area as String
      * @return isSelected as boolean
      */
     public boolean isRewAreaSelected(String areaId) {
         final String METHOD_NAME = "isRewAreaSelected(areaId)";
         log.entering(METHOD_NAME);

         boolean isSelected = false; 
         if (this.currRewAreaId != null && this.currRewAreaId.equals(areaId)) {
             isSelected = true; 
         }

         if (log.isDebugEnabled()) {
             log.debug(METHOD_NAME + ": is area selected = " + isSelected);
         }

         log.exiting();
         return isSelected;
     }
     
    /**
      * Returns the information if an area is a standard area  
      * @param  area, area as WebCatArea 
      * @return isStdArea as boolean
      */
     public boolean isStandardArea(WebCatArea area) {
         final String METHOD_NAME = "isStandardArea(area)";
         log.entering(METHOD_NAME);

         boolean isStdArea = false;
         isStdArea = area.isAreaOfType(""); 

         if (log.isDebugEnabled()) {
             log.debug(METHOD_NAME + ": area is standard area = " + isStdArea);
         }

         log.exiting();
         return isStdArea;
     } 

    /**
      * Returns the information if an area is a reward area  
      * @param  area, area as WebCatArea 
      * @return isRewArea as boolean
      */
     public boolean isRewardArea(WebCatArea area) {
         final String METHOD_NAME = "isRewardArea(area)";
         log.entering(METHOD_NAME);

         boolean isRewArea = false;
         isRewArea = area.isRewardCategory(); 

         if (log.isDebugEnabled()) {
             log.debug(METHOD_NAME + ": area is reward area = " + isRewArea);
         }

         log.exiting();
         return isRewArea;
     }
     
    /**
      * Returns the area type of the selected area 
      * @return areaType  as String
      */
     public String getAdvSQuery() {
         final String METHOD_NAME = "getAdvSQuery()";
         log.entering(METHOD_NAME);

         String queryStr = "";
         
         if (this.advSQuery != null) {
             queryStr = JspUtil.encodeHtml(this.advSQuery);
         }

         if (log.isDebugEnabled()) {
             log.debug(METHOD_NAME + ": query string = " + queryStr);
         }

         log.exiting();
         return queryStr;
     }

    /**
      * Returns the information if an attribute is pre-selected
      * @param attrId as String
      * @return flag as boolean, if the attribute is pre-selected
      */
     public boolean isAttSelected(String attrId) {
         final String METHOD_NAME = "isAttSelected()";
         log.entering(METHOD_NAME);

         boolean isSelected = true;
         
         if (this.advSSelAtt != null && 
             this.advSSelAtt.get(attrId) == null) {
             isSelected = false;
         }

         if (log.isDebugEnabled()) {
             log.debug(METHOD_NAME + ": attribute with Id = <" + attrId + "> is selected: " + isSelected);
         }

         log.exiting();
         return isSelected;
     }
 
 
}
