/*****************************************************************************

    Class:        TransferToOrderUI
    Copyright (c) 2008, SAP AG, Germany, All rights reserved.
    Created:      2008

*****************************************************************************/
package com.sap.isa.catalog.uiclass;

import javax.servlet.jsp.PageContext;

import com.sap.isa.businessobject.webcatalog.ItemTransferRequest;
import com.sap.isa.catalog.actions.ActionConstants;
import com.sap.isa.catalog.webcatalog.WebCatItem;

/**
 * Provides the information that is needed to transfer an item to the CRM WebClient UI.
 * 
 * Expects an <code>ItemTransferRequest</code> item as input, as this
 * is provided by the <code>AddToBasketAction</code> of the catalog.
 * 
 */
public class TransferToOrderUI extends CatalogBaseUI {

    private boolean hasTransferOrderItem;

    private String description = null;
    private String productId = null;
    private String productGuid = null;
    private String quantity = null;
    private String unit = null;
    private String configXml = null;
    private String configXmlEncodedB64 = null;
    private String message = null;

    public TransferToOrderUI(PageContext pageContext) {

        super(pageContext);

        WebCatItem webCatItem = (WebCatItem) request.getAttribute(ActionConstants.RA_TRANSFERTOORDERITEM);
        if (null == webCatItem) {
            hasTransferOrderItem = false;
            return;
        }
                
        ItemTransferRequest item = new ItemTransferRequest(webCatItem);
        if (null == item) {
            hasTransferOrderItem = false;
            return;
        }

        hasTransferOrderItem = true;

        // fill the attributes from the transfer item
        setDescription(item.getDescription());
        setProductGuid(item.getProductKey());
        setProductId(item.getProductId());
        setQuantity(item.getQuantity());
        setUnit(item.getUnit());
        setConfigXml(item.getExtConfigXml());
        setConfigXmlEncodedB64(item.getExtConfigXmlEncodedB64());
        setMessage("");
    }

    public String getConfigXml() {
        return configXml;
    }

    public String getConfigXmlEncodedB64() {
        return configXmlEncodedB64;
    }

    /**
     * Returns the country of the catalog configuration as string.
     */
    public String getCountry() {
        return catalogConfig.getCountry();
    }

    /**
     * Returns the language of the catalog configuration as string.
     */
    public String getLanguage() {
        return catalogConfig.getLanguageIso();
    }

    public String getProductGuid() {
        return productGuid;
    }

    public String getQuantity() {
        return quantity;
    }

    /**
     * Returns the id of the catalog configuration.
     */
    public String getCatalogConfigId() {
        return catalogConfig.getId();
    }

    /**
     * Returns true, if there is an item to be transferred.
     */
    public boolean hasTransferOrderItem() {
        return hasTransferOrderItem;
    }

    public void setConfigXml(String string) {
        configXml = string;
    }

    public void setProductGuid(String string) {
        productGuid = string;
    }

    public void setQuantity(String string) {
        quantity = string;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String string) {
        message = string;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String string) {
        description = string;
    }

    public String getUnit() {
        return unit;
    }

    public void setUnit(String string) {
        unit = string;
    }

    public void setConfigXmlEncodedB64(String string) {
        configXmlEncodedB64 = string;
    }

    public String getProductId() {
        return productId;
    }

    public void setProductId(String string) {
        productId = string;
    }

}
