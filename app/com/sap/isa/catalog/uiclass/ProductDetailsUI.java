package com.sap.isa.catalog.uiclass;

import java.util.ArrayList;
import java.util.Hashtable;
import java.util.Iterator;

import javax.servlet.jsp.PageContext;

import com.sap.isa.backend.boi.isacore.BaseConfiguration;
import com.sap.isa.backend.boi.isacore.ProductData;
import com.sap.isa.backend.boi.isacore.marketing.MarketingBusinessObjectsAware;
import com.sap.isa.backend.boi.isacore.marketing.MarketingConfiguration;
import com.sap.isa.backend.boi.webcatalog.atp.IAtpResult;
import com.sap.isa.backend.boi.webcatalog.exchproducts.IExchProductResult;
import com.sap.isa.backend.boi.webcatalog.pricing.PriceCalculatorBackend;
import com.sap.isa.backend.crm.webcatalog.pricing.PriceCalculatorInitDataCRMIPC;
import com.sap.isa.businessobject.webcatalog.pricing.PriceCalculator;
import com.sap.isa.catalog.actions.ActionConstants;
import com.sap.isa.catalog.boi.WebCatItemData;
import com.sap.isa.catalog.webcatalog.WebCatInfo;
import com.sap.isa.catalog.webcatalog.WebCatItem;
import com.sap.isa.core.TechKey;
import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.core.util.MessageList;
import com.sap.isa.isacore.action.EComConstantsBase;

/**
 * DOCUMENT ME!
 *
 * @author $author$
 * @version $Revision$
 */
public class ProductDetailsUI extends CatalogBaseUI {

    // Logging location
    public static IsaLocation log = IsaLocation.getInstance(ProductDetailsUI.class.getName());

    // to be used by the basket to position on a certain item in the details jsp
    public static String BASKET_ANCHOR = "basketAnchor";

    protected ArrayList thePath;
    protected Iterator atpList;
    protected IAtpResult atpResult;
    protected boolean isBasketAnchorCfgLnk = false;
    protected boolean showExchProds = false;
    protected boolean showCUAButtons = false;
    protected boolean showProdCateg = false;
    protected boolean showCompToSim = false;
    protected boolean showATP = false;
    protected String basketAnchorKeyStr;
    protected String[] categoryIds;
    protected String[] categoryDescriptions;
    protected String[] hierarchyIds;
    protected String[] hierarchyDescriptions;
    protected String cuaRelatedProduct = null;
    protected String oldDetailScenario = null;
    protected String cuaType = null;
    private boolean fromCatEntry = false;
    


    /**
     * DOCUMENT ME!
     *
     * @param pageContext
     */
    public ProductDetailsUI(PageContext pageContext) {
        super(pageContext);

        log.entering("ProductDetailsUI()");

        if (userSessionData != null) {
            thePath = (ArrayList) request.getAttribute("thePath");
        }

        basketAnchorKeyStr = (String) request.getAttribute(EComConstantsBase.RC_ANCHOR_KEY);

        if (basketAnchorKeyStr != null && basketAnchorKeyStr.trim().length() > 0) {
            isBasketAnchorCfgLnk = "X".equalsIgnoreCase((String) request.getAttribute(EComConstantsBase.RC_ANCHOR_IS_CFG));
        }
        
		boolean isCRMBackend = catalogConfig.getBackend().equals(BaseConfiguration.BACKEND_CRM);
        
        showExchProds = isCRMBackend && catalogConfig.showCatExchProds() && (isDetailScenarioNotSet() || !IsDetailInWindow());
        showCUAButtons = catalogConfig.showCatCUAButtons() && (isDetailScenarioNotSet() || !IsDetailInWindow());
        showProdCateg = catalogConfig.showCatProdCateg() && isCategoryInfoAvailable();
        showCompToSim = catalogConfig.showCatCompToSim() && getRole() && (isDetailScenarioNotSet() || !IsDetailInWindow());
        showATP = catalogConfig.showCatATP();

        cuaRelatedProduct = (String) request.getAttribute(ActionConstants.RA_CUA_RELATED_PRODUCT);
        oldDetailScenario = (String) request.getAttribute(ActionConstants.RA_OLDDETAILSCENARIO);

        String extendedScenario = theCatalog.getLastUsedDetailScenario();
        if(extendedScenario != null) {
        	fromCatEntry = extendedScenario.equals(ActionConstants.DS_ENTRY);
        }

        //cuaType is need for back-Button
    	this.cuaType = theCatalog.getCuaType();

        if (log.isDebugEnabled()) {
            log.debug("thePath=" + thePath);
            log.debug("detailScenario=" + detailScenario);
            log.debug("basketAnchorKeyStr=" + basketAnchorKeyStr);
            log.debug("isBasketAnchorCfgLnk=" + isBasketAnchorCfgLnk);
            log.debug("showExchProds=" + showExchProds);
            log.debug("showCUAButtons=" + showCUAButtons);
            log.debug("showProdCateg=" + showProdCateg);
            log.debug("showCompToSim=" + showCompToSim);
            log.debug("showCompToSim=" + showATP);
            log.debug("cuaRelatedProduct=" + cuaRelatedProduct);
            log.debug("oldDetailScenario=" + oldDetailScenario);
        }

        log.exiting();
    }

    /**
     * DOCUMENT ME!
     *
     * @return DOCUMENT ME!
     */
    public boolean isAccessories() {
        log.entering("isAccessories()");
        log.exiting();
        return (cuaType != null && cuaType.equals("A"));
    }

    /**
     * DOCUMENT ME!
     *
     * @return DOCUMENT ME!
     */
    public boolean isRelatedProducts() {
        log.entering("isRelatedProducts()");
        log.exiting();
        return (cuaType != null && cuaType.equals("C"));
    }

    /**
     * DOCUMENT ME!
     *
     * @return DOCUMENT ME!
     */
    public boolean isAlternatives() {
        log.entering("isAlternatives()");
        log.exiting();
        return (cuaType != null && cuaType.equals("U"));
    }

    /**
     * DOCUMENT ME!
     *
     * @return DOCUMENT ME!
     */
    public WebCatItem getItem() {
        log.entering("getItem()");

        WebCatItem item = (WebCatItem)request.getAttribute(ActionConstants.RC_WEBCATITEM);
        if(item == null) { 
            item = this.theCatalog.getCurrentItem();
        }
        log.exiting();
        return item;
    }

    /**
     * DOCUMENT ME!
     *
     * @return DOCUMENT ME!
     */
    public String getProductNumber() {
        log.entering("getProductNumber()");
        log.exiting();

        return getItem().getProduct();
    }

    /**
     * Returns true, if the ite is not an accessory
     *
     * @return DOCUMENT ME!
     */
    public boolean getRole() {
        log.entering("getRole()");
        log.exiting();

        return ((getItem().getAttribute("ROLE") == null) || !getItem().getAttribute("ROLE").equals("B"));
    }

    /**
     * DOCUMENT ME!
     *
     * @return DOCUMENT ME!
     */
    public String getItemId() {
        log.entering("getItemId()");
        log.exiting();

        return getItem().getItemID();
    }

    /**
     * DOCUMENT ME!
     *
     * @return DOCUMENT ME!
     */
    public String getItemDescription() {
        log.entering("getItemDescription()");
        log.exiting();

        return getItem().getDescription();
    }

    /**
     * DOCUMENT ME!
     *
     * @return DOCUMENT ME!
     */
    public String getItemlngDesc() {
        log.entering("getItemlngDesc()");
        log.exiting();

        return getItem().getLngDesc();
    }

    /**
     * DOCUMENT ME!
     *
     * @return DOCUMENT ME!
     */
    public boolean IsDetailInWindow() {
        log.entering("IsDetailInWindow()");
        log.exiting();

        return (getDetailScenario().equals(ActionConstants.DS_IN_WINDOW));
    }

    /**
     * Returns true, if the config link of the current item should be set as anchor
     * This might happen, if the catalog was called from the basket to do SC calls
     * for a basket item.
     * 
     * The anchor name is refernced from the basket action 
     *
     * @param itemTechKey the Techkey of the item to check
     * @return true if the config link of the current item should be the anchor for the basket
     *         false else
     */
    public boolean isCfgLnkBasketAnchor(TechKey itemTechKey) {
        log.entering("isCfgLnkBasketAnchor");

        if (log.isDebugEnabled()) {
        log.debug("itemTechKey=" + ((itemTechKey == null) ? "" : itemTechKey.getIdAsString()));
        }

        boolean retVal = false;

        if (itemTechKey != null
            && basketAnchorKeyStr != null
            && isBasketAnchorCfgLnk
            && basketAnchorKeyStr.equals(itemTechKey.getIdAsString())) {
            retVal = true;
        }

        if (log.isDebugEnabled()) {
        log.debug("setAnchorOnConfig=" + retVal);
        }

        log.exiting();

        return retVal;
    }

    /**
     * DOCUMENT ME!
     *
     * @return DOCUMENT ME!
     */
    public boolean IsCatalogEntry() {
        log.entering("IsCatalogEntry()");
        log.exiting();

        return (getDetailScenario().equals(ActionConstants.DS_ENTRY) || fromCatEntry);
    }

    /**
     * DOCUMENT ME!
     *
     * @return DOCUMENT ME!
     */
    public boolean IsCuaList() {
        log.entering("IsCuaList()");
        log.exiting();

        return (getDetailScenario().equals(ActionConstants.DS_CUA));
    }

    /**
     * DOCUMENT ME!
     *
     * @return DOCUMENT ME!
     */
    public boolean IsBestSeller() {
        log.entering("IsBestSeller()");
        log.exiting();

        return (getDetailScenario().equals(ActionConstants.DS_BESTSELLER));
    }

    /**
     * DOCUMENT ME!
     *
     * @return DOCUMENT ME!
     */
    public boolean IsRecommendations() {
        log.entering("IsRecommendations()");
        log.exiting();

        return (getDetailScenario().equals(ActionConstants.DS_RECOMMENDATION));
    }

    /**
     * DOCUMENT ME!
     *
     * @return DOCUMENT ME!
     */
    public boolean IsProducts() {
        log.entering("IsProducts()");
        log.exiting();

        return (request.getParameter("display_scenario") != null && request.getParameter("display_scenario").equals("products"));
    }

    /**
     * Returns a flag if the product was found by a query
     *
     * @return isQueryFlag as boolean
     */
    public boolean IsQuery() {
        log.entering("IsQuery()");
        boolean isQueryFlag = (this.isQuery != null && this.isQuery.equals("yes"))
        						||
        					(getDetailScenario() != null && getDetailScenario().equals(ActionConstants.RA_CURRENT_QUERY));
        log.exiting();
        return isQueryFlag;
//        return (getDetailScenario() != null && getDetailScenario().equals("query"));
    }

    /**
     * Returns a boolean value if the current request is coming from a quick search query.
     * @return A boolean value if the current request is coming from a quick search query.
     */
    public boolean IsQuickSearchQuery() {
        log.entering("IsQuickSearchQuery()");
        log.exiting();

        return this.isCatalogQuery() || this.IsQuery() || (getDetailScenario() != null && getDetailScenario().equals(ActionConstants.DS_CATALOG_QUERY));
    }
    
    /**
     * DOCUMENT ME!
     *
     * @return DOCUMENT ME!
     */
    public boolean IsProductList() {
        log.entering("IsProductList()");
        log.exiting();

        return (
            isDetailScenarioNotSet()
                || IsBestSeller()
                || IsCuaList()
                || IsDetailInWindow()
                || IsRecommendations()
                || getDetailScenario().equals(ActionConstants.DS_ENTRY)
                || getDetailScenario().equals(ActionConstants.DS_IVIEW)
                || IsQuickSearchQuery());
    }

    /**
     * This method returns true, if the current item is coming from
     * the catalog and not from CUA, QuickSearch, etc.. 
     * This menas the detailSenario is not set.
     *
     * @return true if the item is from the catalog
     */
    public boolean isDetailScenarioNotSet() {
        log.entering("isDetailScenarioNotSet()");
        log.exiting();

        return (getDetailScenario() == null || getDetailScenario().trim().length() == 0);
    }

    /**
     * DOCUMENT ME!
     *
     * @return DOCUMENT ME!
     */
    public String getIPCPrice() {
        log.entering("getIPCPrice()");
        log.exiting();

        return getItem().readItemPrice().getIPCPrice();
    }

    /**
     * DOCUMENT ME!
     *
     * @return DOCUMENT ME!
     */
    public boolean IsConfigurable() {
        log.entering("IsConfigurable()");
        log.exiting();

        return (
            getItem().getAttribute("PRODUCT_CONFIGURABLE_FLAG") != null
                && getItem().getAttribute("PRODUCT_CONFIGURABLE_FLAG").equals("A"));
    }

    /**
     * DOCUMENT ME!
     *
     * @return DOCUMENT ME!
     */
    public boolean IsConfigUseIpc() {
        log.entering("IsConfigUseIpc()");
        log.exiting();

        return (
            getItem().getAttribute("CONFIG_USE_IPC") != null
                && getItem().getAttribute("CONFIG_USE_IPC").equals("X")
                && getItem().getConfigItemReference() != null);
    }

    /**
     * DOCUMENT ME!
     *
     * @return DOCUMENT ME!
     */
    public boolean IsConfigUse() {
        log.entering("IsConfigUse()");
        log.exiting();

        return (
            getItem().getAttribute("CONFIG_USE_IPC") == null
                || getItem().getAttribute("CONFIG_USE_IPC").equals("")
                && getItem().getConfigItemReference() != null);
    }

    /**
     * DOCUMENT ME!
     *
     * @return DOCUMENT ME!
     */
    public boolean IsConfigurableFlag() {
        log.entering("IsConfigurableFlag()");
        log.exiting();

        return (
            getItem().getAttribute("PRODUCT_CONFIGURABLE_FLAG") != null
                && getItem().getAttribute("PRODUCT_CONFIGURABLE_FLAG").equals("C"));
    }

    /**
     * DOCUMENT ME!
     *
     * @return DOCUMENT ME!
     */
    public boolean IsProductConfigFlag() {
        log.entering("IsProductConfigFlag()");
        log.exiting();

        return (
            getItem().getAttribute("PRODUCT_CONFIGURABLE_FLAG") != null
                && getItem().getAttribute("PRODUCT_CONFIGURABLE_FLAG").equals("B"));
    }

    /**
     * DOCUMENT ME!
     *
     * @return DOCUMENT ME!
     */
    public boolean IsConfigFlag() {
        log.entering("IsConfigFlag()");
        log.exiting();

        return (getItem().getAttribute("PRODUCT_CONFIGURABLE_FLAG") != null &&
                (getItem().getAttribute("PRODUCT_CONFIGURABLE_FLAG").equals("X") || 
                 getItem().getAttribute("PRODUCT_CONFIGURABLE_FLAG").equals("G")));
    }

    /**
     * DOCUMENT ME!
     *
     * @return DOCUMENT ME!
     */
    public boolean IsProductVariant() {
        log.entering("IsProductVariant()");
        log.exiting();

        return (
            getItem().getAttribute("PRODUCT_VARIANT_FLAG") != null && !getItem().getAttribute("PRODUCT_VARIANT_FLAG").equals(""));
    }

    /**
     * DOCUMENT ME!
     *
     * @return DOCUMENT ME!
     */
    public boolean IsConfigured() {
        log.entering("IsConfigured()");
        log.exiting();

        return getItem().isConfigured();
    }

    /**
     * DOCUMENT ME!
     *
     * @return DOCUMENT ME!
     */
    public String getQuantity() {
        log.entering("getQuantity()");
        log.exiting();

        return getItem().getQuantity();
    }

    /**
     * DOCUMENT ME!
     *
     * @return DOCUMENT ME!
     */
    public String getUnit() {
        log.entering("getUnit()");
        log.exiting();

        return getItem().getUnit();
    }

    /**
     * DOCUMENT ME!
     *
     * @return DOCUMENT ME!
     */
    public Iterator IsATPList() {
        log.entering("IsATPList()");
        log.exiting();

        atpList = (java.util.Iterator) request.getAttribute("atpList");

        return atpList;
    }
    
    /**
     * Returns true if ATP link/infos should be shown
     *
     * @return boolean true, if ATP link/infos should be shown,
     *                 false else
     */
    public boolean showATP() {
        log.entering("showATP()");

        if (log.isDebugEnabled()) {
            log.debug("showATP" + showATP);
        }

        log.exiting();

        return showATP;
    }
    
    /**
     * returns true if the ATP Config link should be shown
     * @return boolean true, if ATP Config link should be shown
     *                 false otherwise
     */
    public boolean showATPConfigLink(){
		log.entering("showATPConfigLink()");
		
		boolean show = isDetailScenarioNotSet() || !IsDetailInWindow();

		if (log.isDebugEnabled()) {
		   log.debug("showATPConfigLink" + show);
		}

	   log.exiting();
	   return show;
    }
    
    /**
     * Returns true if ATP entries are available
     * 
     * @param The current item use to check for an ATP list if nothing is found
     *        in the request.
     * @return boolean true, if ATP entries are available
     *                 false else
     */
    public boolean hasATPEntries(WebCatItem currentItem) {
        log.entering("hasATPEntries()");
        
        IsATPList();
        
        if (atpList == null && currentItem != null) {
            log.debug("No ATP list found in request, try to take it from the item");
            if (currentItem.getAtpResultList() != null) {
                atpList = theCatalog.getCurrentItem().getAtpResultList().iterator();
            }  
            else {
                log.debug("Items TAP list is empty");
            }
        }
        
        log.exiting();

        return atpList != null;
    }

    /**
     * Returns true, if the current item should be set as anchor for the basket
     * This might happen, if the catalog was called from the basket to do SC calls
     * for a basket item.
     * 
     * The anchor name is refernced from the basket action 
     * 
     * @param itemTechKey the Techkey of the item to check
     * @return true if the config link of the current item should be the anchor for the basket
     *         false else
     */
    public boolean isItemBasketAnchor(TechKey itemTechKey) {

        log.entering("isItemBasketAnchor");

        if (log.isDebugEnabled()) {
        log.debug("itemTechKey=" + ((itemTechKey == null) ? "" : itemTechKey.getIdAsString()));
        }

        boolean retVal = false;

        if (itemTechKey != null
            && basketAnchorKeyStr != null
            && !isBasketAnchorCfgLnk
            && basketAnchorKeyStr.equals(itemTechKey.getIdAsString())) {
            retVal = true;
        }

        if (log.isDebugEnabled()) {
        log.debug("isItemBasketAnchor: " + retVal);
        }

        log.exiting();

        return retVal;
    }

    /**
     * same function as isItemBasketAnchor, but for the dummy item, with title 'no selection'.
     * 
     */
    public boolean isNoSelectionItemAnchor(String groupName) {
        log.entering("isNoSelectionItemAnchor");
        
        boolean retVal = false;
        
        if (groupName != null
           && basketAnchorKeyStr != null
           && !isBasketAnchorCfgLnk
           && basketAnchorKeyStr.equals(groupName)) {
              retVal = true;
       }
        
        if (log.isDebugEnabled()) {
        log.debug("isNoSelectionItemAnchor: "+retVal);
        }
        
        log.exiting();
        
        return retVal;
    }

    /**
     * DOCUMENT ME!
     *
     * @return DOCUMENT ME!
     */
    public boolean isItemFromBasket() {
        log.entering("isItemFromBasket()");
        log.exiting();

        return "ItemFromBasket".equals(getDetailScenario());
        // return (request.getAttribute("detailScenario") != null && request.getAttribute("detailScenario").equals("ItemFromBasket"));
    }

    /**
     * DOCUMENT ME!
     *
     * @return DOCUMENT ME!
     */
    public String getATPquantity() {
        log.entering("getATPquantity()");

        atpResult = (IAtpResult) atpList.next();
        String atpQuantity = atpResult.getCommittedQuantity();

        log.exiting();

        return atpQuantity;
    }

    /**
     * DOCUMENT ME!
     *
     * @return DOCUMENT ME!
     */
    public String getCommittedDate() {
        log.entering("getCommittedDate()");
        log.exiting();

        return atpResult.getCommittedDateStr();
    }

    /**
     * DOCUMENT ME!
     *
     * @return DOCUMENT ME!
     */
    public boolean IsATPListHasMoreElements() {
        log.entering("IsATPListHasMoreElements()");
        log.exiting();

        return atpList.hasNext();
    }

    /**
     * DOCUMENT ME!
     *
     * @return DOCUMENT ME!
     */
    public String getCurrentProductVariantId() {
        log.entering("getCurrentProductVariantId()");
        log.exiting();

        return getItem().getConfigItemReference().getProductId();
    }

    /**
     * DOCUMENT ME!
     *
     * @return DOCUMENT ME!
     */
    public String getCurrentProductVariantDescription() {
        log.entering("getCurrentProductVariantDescription()");
        log.exiting();

        return getItem().getConfigItemReference().getProductDescription();
    }

    /**
     * DOCUMENT ME!
     *
     * @return DOCUMENT ME!
     */
    public boolean IsCurrentProductVariant() {
        log.entering("IsCurrentProductVariant()");
        log.exiting();

        return (
            getItem().getConfigItemReference() != null
                && !getItem().getProductID().equals(getItem().getConfigItemReference().getProductGUID()));
    }

    /**
     * DOCUMENT ME!
     *
     * @return DOCUMENT ME!
     */
    public String getBacktoCategoryPath() {
        log.entering("getBacktoCategoryPath()");
        log.exiting();

        return ((com.sap.isa.catalog.webcatalog.WebCatArea) thePath.get(0)).getAreaName();
    }

    /**
     * DOCUMENT ME!
     *
     * @return DOCUMENT ME!
     */
    public boolean IsItemListQuery() {
        log.entering("IsItemListQuery()");
        boolean response = false;
        WebCatItem item = getItem();
        if(item != null) {
            response = item.getItemKey().getParentCatalog().getCurrentItemList() != null
            && item.getItemKey().getParentCatalog().getCurrentItemList().getQuery() != null
            && !IsQuickSearchQuery();
        }
        log.exiting();

        return response;
    }

    /**
     * DOCUMENT ME!
     *
     * @return DOCUMENT ME!
     */
    public boolean IsItemListAreaQuery() {
        log.entering("IsItemListAreaQuery()");
        log.exiting();

        return getItem().getItemKey().getParentCatalog().getCurrentItemList().getAreaQuery() != null;
    }

    /**
     * Checks, if the current item is an item coming from the basket. that should be handeld by the SC.  This is true
     * if the scOrderDocumentGuid is set
     *
     * @return true if the current item is an SC relevant coming from the basket false else
     */
    public boolean isSCBasketItem() {
        log.entering("isCurrentItemFromBasket()");

        boolean retVal = false;

        if (getItem() != null
            && getItem().getSCOrderDocumentGuid() != null
            && getItem().getSCOrderDocumentGuid().getIdAsString().length() > 0) {
            retVal = true;
        }

        if (log.isDebugEnabled()) {
        log.debug("isCurrentItemFromBasket()=" + retVal);
        }

        log.exiting();

        return retVal;
    }

    /**
     * DOCUMENT ME!
     *
     * @return DOCUMENT ME!
     * @deprecated This method should not be used anymore. Instead use the new flags in the Prices to 
     *             check for special prices etc.
     */
    public boolean IsMultiplePrice() {
        log.entering("IsMultiplePrice()");

        boolean isMultiplePrice = false;
        PriceCalculator pricCalc = getItem().getPriceCalculator();
        PriceCalculatorInitDataCRMIPC initData = null;

        if (pricCalc != null) {
            initData = (PriceCalculatorInitDataCRMIPC) pricCalc.getInitData();
        }

        if (initData != null && initData.getStrategy() == PriceCalculatorBackend.STATIC_DYNAMIC_PRICING) {
            isMultiplePrice = true;
        }

        log.exiting();

        return isMultiplePrice;
    }

    /**
     * DOCUMENT ME!
     *
     * @return DOCUMENT ME!
     */
    public Iterator getItemPrices() {
        log.entering("getItemPrices()");
        log.exiting();

        return getItem().readItemPrice().getPrices();
    }

    /**
     * DOCUMENT ME!
     *
     * @return DOCUMENT ME!
     */
    public Iterator getItemListPrices() {
        log.entering("getItemListPrices()");
        log.exiting();

        return getItem().readItemPrice().getListPrices();
    }

    /**
     * Merges the messages from backend and campaign business object manager to one list  which is returned.
     *
     * @param item DOCUMENT ME!
     *
     * @return MessageList
     */
    public MessageList getMessageList(WebCatItem item) {
        log.entering("getMessageList()");

        MessageList msgList = null;
        msgList = getCampaignMessageList();

        if (msgList != null) {
            msgList.add(item.getMessageList());
        }
        else {
            msgList = item.getMessageList();
        }
        log.exiting();

        return msgList;
    }

    public void clearMessageList(WebCatItem item) {
        log.entering("clearMessageList");
        item.clearMessages();
        log.exiting();
    }
        

    /**
     * DOCUMENT ME!
     *
     * @param query DOCUMENT ME!
     *
     * @return DOCUMENT ME!
     */
    public Hashtable getSearchTerms(String query) {
        log.entering("getSearchTerms()");

        Hashtable htTerms = new Hashtable();

        if (query != null && !query.equalsIgnoreCase("")) {
            htTerms.put("QSEARCH", query);
        }

        log.exiting();

        return htTerms;
    }

    /**
     * DOCUMENT ME!
     *
     * @param itemIdHighlight DOCUMENT ME!
     * @param htTerms DOCUMENT ME!
     *
     * @return DOCUMENT ME!
     */
    public String getHighlightedResults(String itemIdHighlight, Hashtable htTerms, String highlightedTag) {
        log.entering("getHighlightedResults()");
        log.exiting();

        return getHighlightedResults(itemIdHighlight, htTerms, null, highlightedTag);
    }

    /**
     * DOCUMENT ME!
     *
     * @return DOCUMENT ME!
     */
    public IExchProductResult getExchProdData() {
        log.entering("getExchProdData()");
        log.exiting();

        return (IExchProductResult) getItem().getExchProdData();
    }

    /**
     * DOCUMENT ME!
     *
     * @return DOCUMENT ME!
     */
    public int getSettlementPeriod() {
        log.entering("getSettlementPeriod()");
        int response = 0;
        IExchProductResult exchProdData = getExchProdData();
        if(exchProdData != null) {
            response = exchProdData.getCorePeriod();
        }
        log.exiting();

        return response;
    }

    /**
     * DOCUMENT ME!
     *
     * @return DOCUMENT ME!
     */
    public String getSettlementPeriodUnit() {
        log.entering("getSettlementPeriodUnit()");
        String response = new String();
        IExchProductResult exchProdData = getExchProdData();
        if(exchProdData != null) {
            response = exchProdData.getCorePeriodUnit();
        }
        log.exiting();

        return response;
    }

    /**
     * DOCUMENT ME!
     *
     * @return DOCUMENT ME!
     */
    public int getValidityPeriod() {
        log.entering("getValidityPeriod()");
        int response = 0;
        IExchProductResult exchProdData = getExchProdData();
        if(exchProdData != null) {
            response = exchProdData.getAttritionPeriod();
        }
        log.exiting();

        return response;
    }

    /**
     * DOCUMENT ME!
     *
     * @return DOCUMENT ME!
     */
    public String getValidityPeriodUnit() {
        log.entering("getValidityPeriodUnit()");
        String response = new String();
        IExchProductResult exchProdData = getExchProdData();
        if(exchProdData != null) {
            response = exchProdData.getAttritionPeriodUnit();
        }
        log.exiting();

        return response;
    }

    /**
     * DOCUMENT ME!
     *
     * @return DOCUMENT ME!
     */
    public String getCurrency() {
        log.entering("getCurrency()");
        log.exiting();

        return getItem().getCurrency();
    }

    /**
     * DOCUMENT ME!
     *
     * @return DOCUMENT ME!
     */
    public Hashtable readExProductItemPrices() {
        log.entering("readExProductItemPrices()");
        log.exiting();

        return getItem().readExProductItemPrices();
    }

    /**
     * DOCUMENT ME!
     *
     * @return DOCUMENT ME!
     */
    public String readExchBusinessFlag() {
        log.entering("readExchBusinessFlag()");
        log.exiting();

        return getItem().readExchBusinessFlag();
    }

    /**
     * DOCUMENT ME!
     *
     * @return DOCUMENT ME!
     */
    public String readRemanufacturableFlag() {
        log.entering("readRemanufacturableFlag()");
        log.exiting();

        return getItem().getRemanufacturableFlag();
    }

    /**
     * DOCUMENT ME!
     *
     * @return DOCUMENT ME!
     */
    public boolean shouldCUAListBeDisplayed() {
        log.entering("shouldCUAListBeDisplayed");

        boolean response = false;

        if ( !getItem().isSubComponent() &&
             !getItem().isPtsItem() &&
             !getItem().isBuyPtsItem() ) {
            if (request.getAttribute("cuaList") != null) {
			//in B2C and Telco case, we only display the list if some products are available
			    response = true;            	
            	if( !( (catalogConfig.getScenario().equals(BaseConfiguration.B2C) ||
				catalogConfig.getScenario().equals(BaseConfiguration.TELCO_CUSOMER_SELF_SERVICE))&&
                theCatalog.getCuaList() != null &&
                theCatalog.getCurrentCUAList().size() > 0)) {
                   response = false;
                }
            }
        }

        if (log.isDebugEnabled()) {
        log.debug("return value : " + response);
        }
        log.exiting();

        return response;
    }

    /**
     * returns true if the subItem has a contract duration to be displayed
     * @param  subItem, which is relevant
     * @return boolean value, true if the subItem has a contract duration
     */
    public boolean shouldContractDurationBeDisplayed(WebCatItem subItem) {
        log.entering("shouldContractDurationBeDisplayed");

        boolean response = subItem.isRatePlan() && subItem.getSelectedContractDuration() != null;

        if (log.isDebugEnabled()) {
        log.debug("return value : " + response);
        }
        log.exiting();

        return response;
    }

    /**
     * returns true if the item is configurable 
     * @param  subItem, which is relevant
     * @return boolean value, true if the subItem is configurable
     */
    public boolean shouldConfigMessageLinkBeDisplayed(WebCatItem subItem) {
        log.entering("shouldConfigMessageLinkBeDisplayed");

        boolean response = subItem.isItemOldFashionConfigurable() || subItem.isProductVariant();

        if (log.isDebugEnabled()) {
        log.debug("return value : " + response);
        }
        log.exiting();

        return response;
    }

    /**
     * Return a flag if a package explosion is performed automatically and the item is relevant for an explosion
     *
     * @param  item, which is checked to be relevant for an explosion
     * @return boolean value, true if a package is exploded automatically, false else.
     */
    public boolean isAutomaticPackageExplosion(WebCatItem webCatItem) {
        final String METHOD = "isAutomaticPackageExplosion()";
        log.entering(METHOD);

        boolean retVal = false;

        if (theCatalog != null) {
            retVal = theCatalog.getAutomaticPackageExplosion() && webCatItem.isRelevantForExplosion();
            if (log.isDebugEnabled()) {
                log.debug(METHOD + ": XCM explosion parameter = " + theCatalog.getAutomaticPackageExplosion());
                log.debug(METHOD + ": isRelevantForExplosion = " + webCatItem.isRelevantForExplosion());
            }
        }

        if (log.isDebugEnabled()) {
            log.debug(METHOD + ": automatic explosion of packages is active = " + retVal);
        }
        log.exiting();

        return retVal;
    }

    /**
     * Returns <code>true</code> if the current total box should be displayed. 
     * 
     * @param  item, which is checked 
     * @return boolean value, <code>true</code> if the current total should be displayed, <code>false</code>
     *                  if it should not be displayed.
     */
    public boolean isCurrentTotalDisplayed(WebCatItem webCatItem) {

        final String METHOD = "isCurrentTotalDisplayed()";
        log.entering(METHOD);

        boolean retVal = false;

        retVal = webCatItem.isRelevantForExplosion() && webCatItem.getParentTechKey() == null && webCatItem.isValid();

        log.debug(METHOD + ": isRelevantForExplosion = " + webCatItem.isRelevantForExplosion());
        log.debug(METHOD + ": getParentTechKey = " + webCatItem.getParentTechKey());
        log.debug(METHOD + ": isValid = " + webCatItem.isValid());

        log.debug(METHOD + ": is current total box displayed = " + retVal);

        log.exiting();

        return retVal;
    }

    /**
     * Return a flag if the update button is displayed
     *
     * @param  item, which is checked to be relevant for an explosion
     * @return boolean value, true if a package is exploded automatically, false else.
     */
    public boolean isUpdateButtonDisplayed(WebCatItem webCatItem) {
        final String METHOD = "isUpdateButtonDisplayed()";
        log.entering(METHOD);

        boolean retVal = false;

        if (theCatalog != null) {
            retVal =
                !theCatalog.getAutomaticPackageExplosion()
                    && webCatItem.isRelevantForExplosion()
                    && webCatItem.getParentTechKey() == null 
                    && BaseConfiguration.TELCO_CUSOMER_SELF_SERVICE.equals(catalogConfig.getScenario())
                    && showScComponents(webCatItem)
                    && showChangeableScComponents(webCatItem);
                    
            if (log.isDebugEnabled()) {
                log.debug(METHOD + ": XCM explosion parameter = " + theCatalog.getAutomaticPackageExplosion());
                log.debug(METHOD + ": isRelevantForExplosion = " + webCatItem.isRelevantForExplosion());
                log.debug(METHOD + ": getParentTechKey = " + webCatItem.getParentTechKey());
                log.debug(METHOD + ": showScComponents = " + showScComponents(webCatItem));
                log.debug(METHOD + ": showChangeableScComponents = " + showChangeableScComponents(webCatItem));
            }
        }

        if (log.isDebugEnabled()) {
            log.debug(METHOD + ": is update button displayed = " + retVal);
        }
        log.exiting();

        return retVal;
    }

    /**
     * Return a flag if the standard buttons (add to Basket, ..) should be displayed
     *
     * @param  item, which is checked to be relevant for an explosion
     * @return boolean  true if the standard buttons should be displayed,
     *                  false else.
     */
    public boolean showStandardButtons(WebCatItem webCatItem) {
        final String METHOD = "showStandardButtons(WebCatItem webCatItem)";
        log.entering(METHOD);

        boolean retVal = false;

        if (theCatalog != null) {
            retVal = !webCatItem.isSubComponent() && !isSCBasketItem() && webCatItem.isValid();
            if (log.isDebugEnabled()) {
                log.debug(METHOD + ": isSubComponent = " + webCatItem.isSubComponent());
                log.debug(METHOD + ": isSCBasketItem = " + isSCBasketItem());
                log.debug(METHOD + ": isValid = " + webCatItem.isValid());
            }
        }

        if (log.isDebugEnabled()) {
            log.debug(METHOD + ": showStandardButtons = " + retVal);
        }
        log.exiting();

        return retVal;
    }
    
    /**
     * Returns true, if exchange Products should be shown
     * 
     * @return true   if exchange Products should be shown,
     *         false  otherwise
     */
    public boolean showExchProds() {
        log.entering("showExchProds");

        if (log.isDebugEnabled()) {
            log.debug("showExchProds" + showExchProds);
        }
        log.exiting();
        
        return showExchProds;
    }
    
    
    /**
	 * Returns true, if contract detail button should be shown
	 * 
	 * @return true   if contract detail button should be shown,
	 *         false  otherwise
	 */
	public boolean showContractDetailsButton() {
		log.entering("showContractDetailsButton");

		boolean show = catalogConfig.showCatContracts() && ( getItem().getContractItems() != null) && (getItem().getContractItems().size() > 0 );
		if (log.isDebugEnabled()) {
			log.debug("showContractDetailsButton" + show);
		}
		log.exiting();
       
		return show;
	} 
	
	/**
	 * Returns true, if contract details should be shown
	 * 
	 * @return true   if contract details should be shown,
	 *         false  otherwise
	 */
	public boolean diplayContractDetails() {
		log.entering("diplayContractDetails");

		boolean show = false;
		if (showContractDetailsButton() &&
		    theCatalog.getCurrentItemList() != null &&
		    theCatalog.getCurrentItemList().getContractAttributeList() != null &&
		    theCatalog.getCurrentItemList().getContractAttributeList().size() >0 ){
			show = true;
		}
		if (log.isDebugEnabled()) {
			log.debug("diplayContractDetails" + show);
		}
		log.exiting();
      
		return show;
	}    
	
    
    /**
     * Returns true, if buttons to determine CUA products should be shown
     * 
     * @return true   if buttons to determine CUA products should be shown,
     *         false  otherwise
     */
    public boolean showCUAButtons() {
        log.entering("showCUAButtons");
        
        boolean show = showCUAButtons;

        if (log.isDebugEnabled()) {
            log.debug("showCUAButtons" + show);
        }
        log.exiting();
        
        return show;
    }
    
	/**
	 * Returns true, if we are in a CRM scenario
	 * 
	 * @return true   if we are in a CRM scenario
	 *         false  otherwise
	 */
	public boolean isCRM() {
		log.entering("isCRM");
        
		boolean isCRM = catalogConfig.BACKEND_CRM.equals(catalogConfig.getBackend());

		if (log.isDebugEnabled()) {
			log.debug("isCRM" + isCRM);
		}
		log.exiting();
        
		return isCRM;
	}
    
	public boolean isCUAAvailable() {
		log.entering("isCUAAvailable");
        
		boolean returnedValue = catalogConfig.isCuaAvailable();
		
		if (log.isDebugEnabled()) {
			log.debug("isCUAAvailable" + returnedValue);
		}
		log.exiting();
       
		return returnedValue;
	}
    
    /**
     * Returns true, if product categories and hierarchies should be shown
     * 
     * @return true   if product categories and hierarchies should be shown,
     *         false  otherwise
     */
    public boolean showProdCateg() {
        log.entering("showProdCateg");

        if (log.isDebugEnabled()) {
            log.debug("showProdCateg" + showProdCateg);
        }
        log.exiting();
        
        return showProdCateg;
    }
    
    /**
     * Returns true, if compare to similar link should be shown
     * 
     * @return true   if compare to similar link should be shown,
     *         false  otherwise
     */
    public boolean showCompToSim() {
        log.entering("showCompToSim");

        if (log.isDebugEnabled()) {
            log.debug("showCompToSim" + showCompToSim);
        }
        log.exiting();
        
        return showCompToSim;
    }
    
    
    /**
     * returns true, if the quantity and units fields should be displayed.
     * @return true  if quantity and units fields should be displayed 
     *         false otherwise
     */
    public boolean showQuantityUnits(){
		log.entering("showQuantityUnits");
		
		boolean show = isDetailScenarioNotSet() || !IsDetailInWindow(); 

		if (log.isDebugEnabled()) {
			log.debug("showQuantityUnits" + show);
		}
		log.exiting();
		return show;
    }
    
	/**
	 * returns true, if the configuration link should be displayed.
	 * @return true  if the configuration link should be displayed 
	 *         false otherwise
	 */
	public boolean showConfigLink(WebCatItem currentItem){
		log.entering("showConfigLink");
	
		boolean show = !currentItem.isSubComponent() && (isDetailScenarioNotSet() || !IsDetailInWindow()) ; 
		if (log.isDebugEnabled()) {
			log.debug("showConfigLink" + show);
		}
		log.exiting();
		return show;
	}
    
    /**
     * returns true, if the configuration link should be displayed.
     * @return true  if the configuration link should be displayed 
     *         false otherwise
     */
    public boolean showInlineConfig(WebCatItem currentItem){
        log.entering("showInlineConfigLink");
    
        boolean show = currentItem.isConfigurable() && (isDetailScenarioNotSet() || !IsDetailInWindow()); 
        if (log.isDebugEnabled()) {
            log.debug("showInlineConfigLink" + show);
        }
        log.exiting();
        return show;
    }
	
    
    /**
     * returns true, if the the AddToBasket button should be displayed.
     * @return true  if AddToBasket button should be displayed
     *         false otherwise
     */
    public boolean showBasketButton(){
    	log.entering("showBasketButton");
    	
    	boolean show = isDetailScenarioNotSet() || !IsDetailInWindow();
    	if (log.isDebugEnabled()) {
    		log.debug("showBasketButton" + show);
    	}
    	log.exiting();
    	return show;
    }
    
	/**
	  * returns true, if the the AddToLeaflet button should be displayed.
	  * @return true  if AddToLeaflet button should be displayed
	  *         false otherwise
	  */
	 public boolean showLeaftletButton(){
		 log.entering("showLeaftletButton");
    	
		 boolean show = isAddToLeafletAllowed() && ( isDetailScenarioNotSet() || !IsDetailInWindow() );
		 if (log.isDebugEnabled()) {
			 log.debug("showLeaftletButton" + show);
		 }
		 log.exiting();
		 return show;
	 }

    
    /**
     * Checks if category/hierarchy specific data is available
     *
     * @return boolean  true if category/hierarchy specific data is available,
     *                  false else.
     */
    public boolean isCategoryInfoAvailable() {
        log.entering("isCategoryInfoAvailable");

        boolean retVal = false;

        if (getItem() != null) {
            // read descriptions if not already done, to determine length 
            if (categoryDescriptions == null) {
                categoryDescriptions = getItem().getCategoryDescriptions();
            }
            if (categoryDescriptions.length > 0) {
                retVal = true;
            }
        }

        log.exiting();

        return retVal;
    }
    
    /**
     * Returns the number of category/hierarchy descriptions that are available
     *
     * @return int the number of category/hierarchy descriptions that are available
     *             0 if none is available.
     */
    public int getNoOfCategoryInfos() {
        log.entering("getNoOfCategoryInfos");

        int retVal = 0;

        if (getItem() != null) { 
            // read descriptions if not already done, to determine length
            if (categoryDescriptions == null) {
                categoryDescriptions = getItem().getCategoryDescriptions();
            }
            retVal = categoryDescriptions.length;
        }

        log.exiting();

        return retVal;
    }
    
    /**
     * Returns the category id for the given index or null if not determinable.
     */
    public String getCategoryId(int idx) {
        log.entering("getCategoryId()");
        String retVal = null;
        
        if (categoryIds == null && getItem() != null) {
            categoryIds = getItem().getCategoryIds();
        }
        
        if (categoryIds != null) {
            retVal = categoryIds[idx];
        }
        else {
            log.debug("Variable categoryIds could be determined from the catalog item");
        }
        
        if (log.isDebugEnabled()) {
            log.debug("categoryIds[" + idx + "] = " + retVal);
        }

        log.exiting();
        return retVal;
    }
    
    /**
     * Returns the category description for the given index or an empty string
     * if the description is unknown in the backend.
     * Null otherwise.
     */
    public String getCategoryDesc(int idx) {
        log.entering("getCategoryDesc()");
        String retVal = null;
        
        if (categoryDescriptions == null && getItem() != null) {
            categoryDescriptions = getItem().getCategoryDescriptions();
        }
        
        if (categoryDescriptions != null) {
            retVal = categoryDescriptions[idx];
            if (WebCatItemData.UNKNOWN_CAT_DESCR.equals(retVal)) {
                retVal = "";
            }
        }
        else {
            log.debug("Variable categoryDescriptions could be determined from the catalog item");
        }
        
        if (log.isDebugEnabled()) {
            log.debug("categoryDescriptions[" + idx + "] = " + retVal);
        }

        log.exiting();
        return retVal;
    }

    /**
     * Returns the hierarchy id for the given index or null if not determinable.
     */
    public String getHierarchyId(int idx) {
        log.entering("getCategoryId()");
        String retVal = null;
        
        if (hierarchyIds == null  && getItem() != null) {
            hierarchyIds = getItem().getHierarchyIds();
        }
        
        if (hierarchyIds != null) {
            retVal = hierarchyIds[idx];
        }
        else {
            log.debug("Variable hierarchyIds could be determined from the catalog item");
        }
        
        if (log.isDebugEnabled()) {
            log.debug("hierarchyIds[" + idx + "] = " + retVal);
        }

        log.exiting();
        return retVal;
    }
    
    /**
     * Returns the hierarchy description for the given index or an empty string
     * if the description is unknown in the backend.
     * Null otherwise.
     */
    public String getHierarchyDesc(int idx) {
        log.entering("getCategoryDesc()");
        String retVal = null;
        
        if (hierarchyDescriptions == null  && getItem() != null) {
            hierarchyDescriptions = getItem().getHierarchyDescriptions();
        }
        
        if (hierarchyDescriptions != null) {
            retVal = hierarchyDescriptions[idx];
            if (WebCatItemData.UNKNOWN_CAT_DESCR.equals(retVal)) {
                retVal = "";
            }
        }
        else {
            log.debug("Variable hierarchyDescriptions could be determined from the catalog item");
        }
        
        if (log.isDebugEnabled()) {
            log.debug("hierarchyDescriptions[" + idx + "] = " + retVal);
        }

        log.exiting();
        return retVal;
    }
    
	/**
	 * Checks if SC components will be displayed 
	 *
	 * @param  item, which is checked to have SC components
	 * @return boolean  true if SC components will be displayed,
	 *                  false else.
	 */
	public boolean showScComponents(WebCatItem webCatItem) {
		final String METHOD = "showScComponents(WebCatItem webCatItem)";
		log.entering(METHOD);

		boolean retVal = false;

		if (theCatalog != null) {
			retVal = webCatItem.getWebCatSubItemList() != null && webCatItem.getWebCatSubItemList().size() > 0;  
			if (log.isDebugEnabled()) {
				if (webCatItem.getWebCatSubItemList() == null) {
					log.debug(METHOD + ": webCatSubItemList is null");
				}
				else {
				   log.debug(METHOD + ": number of items in webCatSubItemList = " + webCatItem.getWebCatSubItemList().size());
				}
				log.debug(METHOD + ": showScComponents = " + retVal);
            }
		}

		log.exiting();

		return retVal;
	}
    
    /**
     * Checks if SC changeable components will be displayed 
     *
     * @param  item, which is checked to have SC components
     * @return boolean  true if changeable SC components will be displayed,
     *                  false else.
     */
    public boolean showChangeableScComponents(WebCatItem webCatItem) {
        final String METHOD = "showChangeableScComponents(WebCatItem webCatItem)";
        log.entering(METHOD);

        boolean retVal = false;

        if (theCatalog != null) {
            if (webCatItem.getWebCatSubItemList() == null  || 
                webCatItem.getWebCatSubItemList().size() == 0) {
                if (log.isDebugEnabled()) {
                    log.debug(METHOD + ": webCatSubItemList is null or empty");
                }
            }
            else {
                for (int i = 0; i < webCatItem.getWebCatSubItemList().size() && retVal == false; i++) {
                    retVal = webCatItem.getWebCatSubItemList().getItem(i).isOptional();
                }
            }
        }
        else {
            log.debug("Catalog is not available");
        }
        
        if (log.isDebugEnabled()) {
            log.debug(METHOD + ": showChangeableScComponents = " + retVal);
        }

        log.exiting();

        return retVal;
    }
    
    /**
     * return the contract attribute list associated to the webcat item list
     * @return Iterator that contrains the contract attributes associated to the webcat item list
     */
	public Iterator getContractAttributeList(){
		log.entering("getContractAttributeList");
		Iterator it = null;
		if (theCatalog.getCurrentItemList()!= null &&
		    theCatalog.getCurrentItemList().getContractAttributeList() != null) {
		    it = theCatalog.getCurrentItemList().getContractAttributeList().iterator();
		}
		if (log.isDebugEnabled()) {
			if(it == null){
				log.debug("getContractAttributeList: iterator is null");
			}
			else  {
				log.debug("getContractAttributeList: iterator is not null");
			}
		}
		log.exiting();
		return it;		
	}
	
	/**
	 * return the contract attribute list Size associated to the webcat item list
	 * @return int that contrains the contract attributes size associated to the webcat item list
	 */
	public int getContractAttributeListSize(){
		log.entering("getContractAttributeListSize");
		int returnValue = 0;
		if (theCatalog.getCurrentItemList()!= null &&
			theCatalog.getCurrentItemList().getContractAttributeList() != null) {
			returnValue = theCatalog.getCurrentItemList().getContractAttributeList().size();
		}
		if (log.isDebugEnabled()) {
			log.debug("getContractAttributeListSize:"+returnValue);
		}
		log.exiting();
		return returnValue;		
	}
	
	
	/**
	 * function that returns if the button (as parameter) is the first to be displayed in the Product Detail Tab.
	 * Following Cells are displayed: Contract, Accessories, Related Products, Alternatives, Exchange Products
	 * This private function is used to determine which stylesheet should be used for the product detail page
	 */
	private boolean isCatTabFirstButton(int buttonType){
		log.entering("isCatTabFirstButton");
		
		boolean returnValue = false;
		if(showContractDetailsButton() && buttonType == WebCatInfo.PROD_DET_LIST_CONTRACTS) {
			returnValue = true;
		}
		else {
			if( !showContractDetailsButton() && showCUAButtons() && buttonType == WebCatInfo.PROD_DET_LIST_ACCESSORIES) {
				returnValue = true;
			}	
			else{
				if(isCRM() && !showContractDetailsButton() && !showCUAButtons() && showContractDetailsButton() && buttonType == WebCatInfo.PROD_DET_LIST_EXCH_PROD ) {
					returnValue = true;
				}
				else {
					return true;
				}
			}		
		}
		log.exiting();
		if (log.isDebugEnabled()) {
			log.debug("isCatTabFirstButton: returnedValue:"+returnValue);
		}
		
	   return returnValue;
	}
	
	
	/**
	 * functions that returns true if the current button is the last button to be displayed, false otherwise
	 */
	private boolean isCatTabLastButton(int buttonType){
		log.entering("isCatTabLastButton");
		
		boolean returnValue = false;
		if(showExchProds() && buttonType == WebCatInfo.PROD_DET_LIST_EXCH_PROD) {
			returnValue = true;
		}
		else {
			if( !showExchProds() && showCUAButtons() && buttonType == WebCatInfo.PROD_DET_LIST_ACCESSORIES ) {
				returnValue = true;
			}
			else {
				if(!showExchProds() && !showCUAButtons() && showExchProds() && buttonType == WebCatInfo.PROD_DET_LIST_CONTRACTS ) {
					returnValue = true;
				}
			}
		}
		log.exiting();
		if (log.isDebugEnabled()) {
			log.debug("isCatTabLastButton: returnedValue:"+returnValue);
		}
		
		return returnValue;
	}

	/**
	 * functions that returns true if the current button is the active button, false otherwise
	 */
	private boolean isCatTabActive(int buttonType){
		log.entering("isCatTabActive");
		int activeTab = 0;
		if(this.getLastActiveTab() != 0){
			activeTab = this.getLastActiveTab();
		}
		else{
			activeTab = theCatalog.getProductDetailListType();
		}

		boolean returnValue = (activeTab == buttonType);
		if(!returnValue && theCatalog.getProductDetailListType() == 0){
			if(buttonType == WebCatInfo.PROD_DET_LIST_ACCESSORIES){
				returnValue = true;
			}
		}

		log.exiting();
		if (log.isDebugEnabled()) {
			log.debug("isCatTabActive: returnedValue:"+returnValue);
		}
		
		return returnValue;
	}
	

	/**
	 * functions that returns true if the current button is placed after the active button, false otherwise
	 */
	private boolean isCatTabAfterActive(int buttonType){
		log.entering("isCatTabAfterActive");
		boolean returnValue = false;

		int activeTab = 0;
		if(this.getLastActiveTab() != 0){
			activeTab = this.getLastActiveTab();
		}
		else{
			activeTab = theCatalog.getProductDetailListType();
		}

		
		if (activeTab == WebCatInfo.PROD_DET_LIST_CONTRACTS && 
		       ( ( showCUAButtons() && buttonType == WebCatInfo.PROD_DET_LIST_ACCESSORIES ) 
		            || (!showCUAButtons() && showExchProds() && buttonType == WebCatInfo.PROD_DET_LIST_EXCH_PROD) )){
		    returnValue = true;
		}
		else{
		   if(activeTab == WebCatInfo.PROD_DET_LIST_ACCESSORIES && buttonType == WebCatInfo.PROD_DET_LIST_REL_PROD 
		      || activeTab == WebCatInfo.PROD_DET_LIST_REL_PROD && buttonType == WebCatInfo.PROD_DET_LIST_ALTERNATIVES ){
			   returnValue = true;
		   }
		   else {
			   if( buttonType == WebCatInfo.PROD_DET_LIST_EXCH_PROD && 
			       ( (showContractDetailsButton() && !showCUAButtons() && activeTab == WebCatInfo.PROD_DET_LIST_CONTRACTS)
			         || ( showCUAButtons() && activeTab == WebCatInfo.PROD_DET_LIST_ALTERNATIVES ))){
			       returnValue = true;
			   }
		   }
	   }
	   
	   log.exiting();
	   if (log.isDebugEnabled()) {
	   	  log.debug("isCatTabAfterActive: returnedValue:"+returnValue);
	   }
	   return returnValue;
	}


	/**
	 * function that returns the stylesheet that should be used to display the product detail list.
	 * @param styleSheetStart, the styleSheetStart, if the stylesheet to be displayed is "cat-tab-afteractive", the styleSheetStart is "cat-tab-"
	 * @param buttonType, the type of the button the stylesheet should be returned
	 * @return the name of the stylesheet. Following name could be returned (if the styleSheetStart is "cat-tab-"):
	 * cat-tab-firstactive
	 * cat-tab-first
	 * cat-tab-active
	 * cat-tab-afteractive
	 * cat-tab-last
	 * cat-tab-lastactive
	 * cat-tab-lastafteractive
	 */
	public String getCatTabStylesheet(String styleSheetStart, int buttonType){
		log.entering("getCatTabStylesheet");
		
		String returnValue = "";
		boolean isFirst = isCatTabFirstButton(buttonType);
		boolean isActive = isCatTabActive(buttonType);
		boolean isLast = isCatTabLastButton(buttonType);
		boolean isAfterActive = isCatTabAfterActive(buttonType);
		
		if (isFirst && isActive) {
			returnValue = styleSheetStart + "firstactive";
		}
		else{
			if( isFirst && !isActive){
				returnValue=styleSheetStart+"first";
			}
			else{
				if(isLast && isActive) {
					returnValue = styleSheetStart + "lastactive";
				}
				else{
					if(isActive){
						returnValue = styleSheetStart + "active";
					}
					else{
						if(isAfterActive && isLast){
							returnValue = styleSheetStart+"lastafteractive";
						}
						else{
							if(isAfterActive && !isLast){
								returnValue = styleSheetStart+"afteractive";
							}
							else {
								if(isLast){
									returnValue = styleSheetStart +"last";
								}
							}
						}
					}
				}
			}
		}
		if (log.isDebugEnabled()) {
			log.debug("getCatTabStylesheet: returnedValue:"+returnValue);
		}
		log.exiting();
		return returnValue;
	}
	
	/**
	 * returns the product detail list type for the current item
	 * @return int, the type of the product detail list that should be displayed.
	 */
	public int getCatalogProductDetailType(){
		log.entering("getCatalogProductDetailType");
		log.exiting();
		return theCatalog.getProductDetailListType();
	}
	
	/**
	 * returns the number of the tab, that was before requested.
	 */
	public int getLastActiveTab(){
		log.entering("getLastActiveTab");
		int returnValue = 0;
		
		if(request.getParameter("displayedTab") != null){
			String tmp = request.getParameter("displayedTab").toString();
			if(!tmp.equals("")){
				returnValue = (new Integer(tmp)).intValue();
			}
		}
		else{
			if(showCUAButtons()){
				returnValue = WebCatInfo.PROD_DET_LIST_ACCESSORIES;
			}
		}
		
		if (log.isDebugEnabled()) {
			log.debug("getLastActiveTab: returnValue:"+returnValue);
		}
		log.exiting();
		return returnValue;
	}
	 
    /**
     * Returns the current area name
     *
     * @return name of the WebCatArea
     */
    public String getCurrAreaName() {
        log.entering("getCurrAreaName()");
        String areaName = null;
        if (this.theCatalog != null &&
            this.theCatalog.getCurrentArea() != null) {
            areaName = this.theCatalog.getCurrentArea().getAreaName();        
        }
        log.exiting();
        return areaName;
    }

    /**
     * Returns the path of the current area 
     *
     * @return current path of the current area as String representation
     */
    public String getCurrPathAsString() {
        log.entering("getCurrPathAsString()");
        String thePath = null;
        if (this.theCatalog != null &&
            this.theCatalog.getCurrentArea() != null) {
            thePath = this.theCatalog.getCurrentArea().getPathAsString();        
        }
        
        log.exiting();
        return thePath;
    }
  
    /**
     * returns true, if the back button should be displayed.
     * @return true  if back button should be displayed
     *         false otherwise
     */
    public boolean showBackButton() {
        log.entering("showBackButton");

        boolean show = catalogConfig.getScenario().equals(BaseConfiguration.B2B)  ||
                       catalogConfig.getScenario().equals(BaseConfiguration.B2BC) ||
                       catalogConfig.getScenario().equals(BaseConfiguration.B2BERPCRM);

        if (log.isDebugEnabled()) {
            log.debug("showBackButton" + show);
        }
        log.exiting();
        return show;
    }
    
    /**
     * returns true, if the buttons should be displayed.
     * @return true  if button should be displayed
     *         false otherwise
     */
    public boolean showButtons(WebCatItem item){
        log.entering("showButtons");

        boolean show = item.isSubComponent() || isUpdateButtonDisplayed(item) || isSCBasketItem() || showBackButton();
        if (log.isDebugEnabled()) {
            log.debug("showButtons =" + show);
        }
        log.exiting();
        return show;
    }

    /**
     * returns the related product of a cua item
     *
     * @return cuaRelatedProduct if the product is a cua item
     */
    public String getCUARelatedProduct() {
        log.entering("getCUARelatedProduct()");
        log.exiting();

        return this.cuaRelatedProduct;
    }

    /**
     * returns the old detail scenario
     *
     * @return oldDetailScenario
     */
    public String getOldDetailScenario() {
        log.entering("getOldDetailScenario()");
        String oldScenario = "";
        if (this.oldDetailScenario != null) {
            oldScenario = this.oldDetailScenario;
        }
        log.exiting();

        return oldScenario;
    }
  
    /**
     * Returns a flag if multiple search results were found by a query
     * @return hasMultiple as boolean, if multiple search results were found
     */
    public boolean hasMultipleSearchResults() {
        log.entering("hasMultipleSearchResults()");
        boolean hasMultiple = false;

        if (theCatalog.getCurrentItemList() != null) {
            hasMultiple = theCatalog.getCurrentItemList().size() > 1;
        }

        log.exiting();
        return hasMultiple;
    }

	/**
	 * return true if the Accessories and Up/Down Selling Link should be displayed in the product details jsp
	 * @return true, if the accessories and up/down selling link should be displayed in the product details jsp
	 *         false, if the accessories and up/down selling link should not be displayed in the product details jsp
	 */
	public boolean showAccUpDownLink(){
		log.entering("showAccUpDownLink");
		boolean show = false;

		MarketingConfiguration mktConfig = null;
		MarketingBusinessObjectsAware mkbom = (MarketingBusinessObjectsAware) userSessionData.getMBOM().getBOMByType(MarketingBusinessObjectsAware.class);
		if (mkbom != null) {
			mktConfig = mkbom.getMarketingConfiguration();
        
			if (mktConfig != null && mktConfig.isAccessoriesAvailable()) {
					show = true;
			}
		}
    	
		if (log.isDebugEnabled()) {
		   log.debug( "showAccUpDownLink: "+show);
		}
		log.exiting();
		return show;
	}	

}
