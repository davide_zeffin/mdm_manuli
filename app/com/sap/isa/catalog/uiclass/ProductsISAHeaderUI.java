package com.sap.isa.catalog.uiclass;

import java.util.ArrayList;

import javax.servlet.jsp.PageContext;

import com.sap.isa.backend.boi.isacore.BaseConfiguration;
import com.sap.isa.catalog.webcatalog.WebCatAreaAttribute;
import com.sap.isa.catalog.webcatalog.WebCatAreaQuery;
import com.sap.isa.catalog.webcatalog.WebCatItemPage;
import com.sap.isa.core.logging.IsaLocation;

/**
 * @author V.Manjunath Harish
 *
 * To change the template for this generated type comment go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
public class ProductsISAHeaderUI extends CatalogBaseUI {

    static public IsaLocation log = IsaLocation.getInstance(ProductsISAHeaderUI.class.getName());
	protected WebCatAreaAttribute attribute;
	protected ArrayList attributesList;
	/**
	 * @param pageContext
	 */
	public ProductsISAHeaderUI(PageContext pageContext) {
		super(pageContext);
		if (userSessionData != null) {
			this.attributesList = (ArrayList)request.getAttribute("attributesList");
		}
	}
	
    /**
     * DOCUMENT ME!
     *
     * @param attribute DOCUMENT ME!
     * @param itemPage DOCUMENT ME!
     *
     * @return DOCUMENT ME!
     */
    public String getSelectedValue(WebCatAreaAttribute attribute, WebCatItemPage itemPage) {
        
        log.entering("getSelectedValue()");
        
        String selectedValue = null;
        WebCatAreaQuery query = itemPage.getItemList().getAreaQuery();

        if (query != null && query.getPrevAttribs() != null && query.getPrevAttribs().size() != 0) {
            for (int i = 0; i < query.getPrevAttribs().size(); i++) {
                String settedGuid = (String) query.getPrevAttribs().get(i);

                if (settedGuid != null && settedGuid.equals(attribute.getGuid())) {
                    selectedValue = (String) query.getPrevValues().get(i);

                    break;
                }
            }
        }
        log.exiting();

        return selectedValue;
    }
    
    /**
     * DOCUMENT ME!
     *
     * @return DOCUMENT ME!
     */
    public String getCategName() {
        log.entering("getCategName()");
        log.exiting();
        return theCatalog.getCurrentArea().getCategory().getName();
    }
    
    /**
     * DOCUMENT ME!
     *
     * @return DOCUMENT ME!
     */
    public String getCategDescription() {
        log.entering("getCategDescription()");
        log.exiting();
        return theCatalog.getCurrentArea().getCategory().getDescription();
    }
	
    /**
     * DOCUMENT ME!
     *
     * @return DOCUMENT ME!
     */
    public String getAreaName() {
        log.entering("getAreaName()");
        log.exiting();
        return theCatalog.getCurrentArea().getAreaName();
    }

    /**
     * DOCUMENT ME!
     *
     * @return DOCUMENT ME!
     */
    public int getAttributeSize() {
        log.entering("getAttributeSize()");
        log.exiting();
        return attributesList.size();
    }
	
	public ArrayList getAttributeList() {
		log.entering("getAttributeList()");
		log.exiting();
		return this.attributesList;
	}
    
    /**
     * is it a shop with CRM backend?
     *
     * @return false if isn't CRM backend
     */
    public boolean isCRM() {
        log.entering("isCRMShop()");
        log.exiting();
        return (catalogConfig != null && catalogConfig.getBackend().equals(BaseConfiguration.BACKEND_CRM));
    }
}
