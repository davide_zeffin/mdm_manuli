/*****************************************************************************
  Copyright (c) 2000, SAPMarkets Europe GmbH, Germany, All rights reserved.
  Created:      09 July 2001

  $Id: //sap/ESALES_base/30_SP_COR/src/_pcatAPI/java/com/sap/isa/catalog/catalogvisitor/CatalogXMLVisitor.java#2 $
  $Revision: #2 $
  $Change: 129221 $
  $DateTime: 2003/05/15 17:20:58 $
*****************************************************************************/

package com.sap.isa.catalog.catalogvisitor;

import java.util.Iterator;
import java.util.Locale;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.w3c.dom.Text;

import com.sap.isa.catalog.impl.Catalog;
import com.sap.isa.catalog.impl.CatalogAttribute;
import com.sap.isa.catalog.impl.CatalogAttributeValue;
import com.sap.isa.catalog.impl.CatalogCategory;
import com.sap.isa.catalog.impl.CatalogDetail;
import com.sap.isa.catalog.impl.CatalogItem;
import com.sap.isa.catalog.impl.CatalogQuery;
import com.sap.isa.catalog.impl.CatalogQueryItem;
import com.sap.isa.catalog.impl.CatalogQueryStatement;

/**
 *  This visitor generates from a given catalog a XML representation.
 *  The current state of the tree is frozen to XML.
 *
 *  @version     1.0
 *  @since       2.0
 */
public class CatalogXMLVisitor
    extends CatalogVisitor
{
    private Document theDocument;
    private Element theCurrentElement;

    /**
     * Constructs a new visitor that generates a XML representation of the tree.
     *
     * @param root the root node of tree
     */
    public CatalogXMLVisitor(Catalog root)
    {
        super(root);
    }

    /**
     * Starts the generation process.
     *
     * @return a XML document that contains the representation of the tree
     * @exception ParserConfigurationException error when creating the parser
     */
    public synchronized Document start() throws ParserConfigurationException
    {
        DocumentBuilderFactory aXMLFactory = DocumentBuilderFactory.newInstance();
        DocumentBuilder aDocBuilder = aXMLFactory.newDocumentBuilder();
        theDocument = aDocBuilder.newDocument();
        theCurrentElement = null;

        // start traversation
        getRoot().assign(this);

        return theDocument;
    }

    /**
     * A catalog is in most cases the root element and must have at least:
     *     	an optional list of Details,
     *     	an optional list of Maps
     *     	an optional list of Attributes, global and local,
     *     	an optional list of Categories,
     *     	an optional list of global Queries excecuted agains that instance,
     *     	an optional list of Products that can be ordered
     *     	an optional list of Quantities known by that catalog,
     *     	an optional list of Units known by that catalog
     *     	to be a meaning full catalog
     *     	<code>
     *     	<!ELEMENT Catalog (Detail*, Map*, Attribute*, Category*, Query*, Product*, Quantity*, Unit*)>
     *     	A catalog has a
     *     	ID unique id
     *     	user a user id
     *     	references to attributes owned by him and used by every category
     *     	an optional status indicator used in the editing process
     *     	<!ATTLIST Catalog
     *     		ID ID #REQUIRED
     *     		user CDATA #REQUIRED
     *     		attributes IDREFS #IMPLIED
     *     		status (loaded | added | deleted | changed) #IMPLIED
     *     	 >
     *     	 </code>
     */
    public void visitCatalog(Catalog catalog)
    {
        Element catalogElement = theDocument.createElement("Catalog");
        theDocument.appendChild(catalogElement);
        String aCatalogGuid = catalog.getGuid();
        aCatalogGuid = generateXMLID(aCatalogGuid);
        catalogElement.setAttribute("ID", aCatalogGuid);
        catalogElement.setAttribute("user",catalog.getClient().getGuid());
        
        // generate details
        Iterator detailsIter = catalog.getDetailsInternal();
        theCurrentElement = catalogElement;        
        while(detailsIter.hasNext())
        {
            CatalogDetail detail = (CatalogDetail)detailsIter.next();
            if(!detail.isDeleted())
            {
                detail.assign(this);
            }
        }
        
        theCurrentElement = null;
        
        // generate attributes
        Iterator attributesIter = catalog.getAttributesInternal();
        StringBuffer theAttribIDRefs = new StringBuffer();
        theCurrentElement = catalogElement;
        while(attributesIter.hasNext())
        {
            CatalogAttribute attr = (CatalogAttribute)attributesIter.next();
            theAttribIDRefs.append(generateXMLID(attr.getGuid()));
            theAttribIDRefs.append(" ");
            if(!attr.isDeleted())
            {
                attr.assign(this);
            }
        }
        if (theAttribIDRefs.toString().length()>0)
        {
            catalogElement.setAttribute("attributes", theAttribIDRefs.toString());
        } // end of if ()
        theCurrentElement = null;
        
        // generate categories
        Iterator categoriesIter = catalog.getChildCategoriesInternal();
        theCurrentElement=catalogElement;        
        while(categoriesIter.hasNext())
        {
            CatalogCategory category = (CatalogCategory)categoriesIter.next();
            if(!category.isDeleted())
            {
                category.assign(this);
            }
        }
        theCurrentElement = null;
        
        // generate queries
        Iterator queriesIter = catalog.getQueries();
        theCurrentElement = catalogElement;        
        if(queriesIter.hasNext())
        {
            while(queriesIter.hasNext())
            {
                CatalogQuery query = (CatalogQuery)queriesIter.next();
                query.assign(this);
            }
        }
        theCurrentElement = null;        
        return;
    }

    /**
     * <code>
     * <!ELEMENT Category (
     *              		Detail*,
     *                          Category*,
     *                           Item*,
     *                           Query*)>
     * <!ATTLIST Category ID ID #REQUIRED
     *                                      attributes IDREFS #IMPLIED
     *                                      status (loaded | added | deleted | changed) #IMPLIED
     * >
     * </code>
     */
    public void visitCategory(CatalogCategory category)
    {
        // store parent element
        Element parentElement = theCurrentElement;

        // create element
        Element categoryElement = theDocument.createElement("Category");
        parentElement.appendChild(categoryElement);

            // set the guid of the element
        categoryElement.setAttribute("ID", generateXMLID(category.getGuid()));

        // generate details
        Iterator detailsIter = category.getDetailsInternal();
        theCurrentElement = categoryElement;                    
        while(detailsIter.hasNext())
        {
            CatalogDetail detail = (CatalogDetail)detailsIter.next();
            if(!detail.isDeleted())
            {
                detail.assign(this);
            }
        }
        theCurrentElement = null;
        
        // generate attributes
        Iterator attributesIter = category.getAttributesInternal();
        StringBuffer theAttribIDRefBuffer =new StringBuffer();
        theCurrentElement = categoryElement;
        while(attributesIter.hasNext())
        {
            CatalogAttribute attr = (CatalogAttribute)attributesIter.next();
            if(!attr.isDeleted())
            {
                theAttribIDRefBuffer.append(generateXMLID(attr.getGuid()));
                theAttribIDRefBuffer.append(" ");
                attr.assign(this);
            }
        }
        if ( theAttribIDRefBuffer.toString().length()>0) 
        {
            categoryElement.setAttribute("attributes", theAttribIDRefBuffer.toString());
        } // end of if ()
        theCurrentElement = null;

        //Categories
        Iterator categoriesIter = category.getChildCategoriesInternal();
        theCurrentElement = categoryElement;                    
        while(categoriesIter.hasNext())
        {
            CatalogCategory child = (CatalogCategory)categoriesIter.next();
            if(!child.isDeleted())
            {
                child.assign(this);
            }
        }
        theCurrentElement = null;

        // generate items
        Iterator itemsIter = category.getItemsInternal();
        theCurrentElement = categoryElement;                    
        while(itemsIter.hasNext())
        {
            CatalogItem item = (CatalogItem)itemsIter.next();
            if(!item.isDeleted())
            {
                item.assign(this);
            }
        }
        theCurrentElement = null;
        
        // generate queries
        Iterator queriesIter = category.getQueries();
        theCurrentElement = categoryElement;        
        if(queriesIter.hasNext())
        {
            while(queriesIter.hasNext())
            {
                CatalogQuery query = (CatalogQuery)queriesIter.next();
                query.assign(this);
            }
        }

        // restore parent element
        theCurrentElement = parentElement;
        return;
    }

    /**
     *An item is something displayable in a category. It is unique inside the
     *	catalog. It might have
     *	an optional Details element
     *	and a list of AttributeValues.
     *	It must have a reference to something orderable and a unique id
     *	<code>
     *	<!ELEMENT Item (Detail*, AttributeValue*)>
     *	<!ATTLIST Item
     *		ID ID #REQUIRED
     *		orderItem IDREF #REQUIRED
     *		status (loaded | added | deleted | changed) #IMPLIED
     *	   >
     *	   </code>
     */
    public void visitItem(CatalogItem item)
    {
        // store parent element
        Element parentElement = theCurrentElement;

        // create element
        Element itemElement = theDocument.createElement("Item");
        parentElement.appendChild(itemElement);

        // set attributes of element
        itemElement.setAttribute("ID", generateXMLID(item.getGuid()));
        itemElement.setAttribute("orderItem",generateXMLID(item.getProductGuid()));

        // generate details
        Iterator detailsIter = item.getDetailsInternal();
        theCurrentElement = itemElement;                    
        while(detailsIter.hasNext())
        {
            CatalogDetail detail = (CatalogDetail)detailsIter.next();
            if(!detail.isDeleted())
            {
                detail.assign(this);
            }
        }
        theCurrentElement = null;
        
        // generate AttributeValues
        Iterator attributeValuesIter = item.getAttributeValues();
        theCurrentElement = itemElement;
        while(attributeValuesIter.hasNext())
        {
            CatalogAttributeValue value =
                (CatalogAttributeValue)attributeValuesIter.next();
            value.assign(this);
        }

        // restore parent element
        theCurrentElement = parentElement;
        return;
    }

    /**
     * <code>
     * <!ELEMENT QueryItem (Detail*, AttributeValue*)>
     * <!ATTLIST QueryItem
     *      item IDREF #REQUIRED
     *      orderItem IDREF #REQUIRED
     *      category IDREF #REQUIRED>
     * </code>
     */
    public void visitQueryItem(CatalogQueryItem item)
    {
        // store parent element
        Element parentElement = theCurrentElement;

        // create element
        Element itemElement = theDocument.createElement("QueryItem");
        parentElement.appendChild(itemElement);

        // set attributes of element
        itemElement.setAttribute("ID", generateXMLID(item.getGuid()));
        itemElement.setAttribute("orderItem", generateXMLID(item.getProductGuid()));        
        itemElement.setAttribute("category", generateXMLID(item.getCategoryGuid()));

        // generate AttributeValues
        Iterator attributeValuesIter = item.getAttributeValues();
        theCurrentElement = itemElement;
        while(attributeValuesIter.hasNext())
        {
            CatalogAttributeValue value =
                (CatalogAttributeValue)attributeValuesIter.next();
            value.assign(this);
        }

        // restore parent element
        theCurrentElement = parentElement;
    }

    /**
     * <code>
     * <!ELEMENT Detail (Value+)>
     * <!ATTLIST Detail
     *      ID CDATA #REQUIRED>
     * </code>
     */
    public void visitDetail(CatalogDetail detail)
    {
        // store parent element
        Element parentElement = theCurrentElement;

        // create detail element
        Element detailElement = theDocument.createElement("Detail");
        parentElement.appendChild(detailElement);

            //determine language dependency
        boolean isLanguageDependent =
            detail.getName().intern()=="name".intern() ||
            detail.getName().intern()=="description".intern();
        Locale theDefaultLocale = detail.getRoot().getDefaultLocale();
        StringBuffer aBuffer = new StringBuffer(theDefaultLocale.getLanguage());
        if (theDefaultLocale.getCountry().intern()!="".intern() ) 
        {
            aBuffer.append("-");
            aBuffer.append(theDefaultLocale.getCountry());            
        } // end of if ()
        
        // set attributes of the element
        detailElement.setAttribute("ID", detail.getName());
        String[] values = detail.getAllAsString();
        for (int i=0; i < values.length; i++) 
        {
            Element aValueElement = theDocument.createElement("Value");
            if (isLanguageDependent) 
            {
                aValueElement.setAttribute("xml:lang", aBuffer.toString());
            } // end of if ()
            Text value = theDocument.createTextNode(values[i]);
            aValueElement.appendChild(value);
            detailElement.appendChild(aValueElement);
        } // end of for ()

        // restore parent element
        theCurrentElement = parentElement;
    }

    /**
     * An Attribute has the attributes
     *   	ID the unique identifier for the attribute.
     *   	isLanguageDependent if the values of the corresponding AttributeValues carry the xml:lang attribute
     *   	owners either the reference to the catalog root or to category references if it is a category specific attribute
     *   	units referencs of units that might be used to measure the values of the attribute
     *   	quantity refernce to the quantity used to measure the attribute
     *   	a status indicator in case editing is relevant
     * <code>
     * <!ELEMENT Attribute (Detail*)>
     *  <!ATTLIST Attribute
     *   		ID ID #REQUIRED
     *   		isLanguageDependent (YES | NO) "NO"
     *   		type (String | URL | Boolean | Integer | Long | Date | Double | Interval | Object) #REQUIRED
     *   		owners IDREFS #IMPLIED
     *   		units IDREFS #IMPLIED
     *   		quantity IDREF #IMPLIED
     *   		status (loaded | added | deleted | changed) #IMPLIED
     *    >
     *   </code>
     */
    public void visitAttribute(CatalogAttribute attribute)
    {
        // store parent element
        Element parentElement = theCurrentElement;

        String theAttribGuid = attribute.getGuid();

        Element attributeElement =  lookUpAttribute(generateXMLID(theAttribGuid));
        if (attributeElement==null) 
        {
                // create attribute element
            attributeElement = theDocument.createElement("Attribute");
            
            NodeList theChilds = theDocument.getDocumentElement().getChildNodes();
            Element aChild=null;
            for ( int i = 0; i < theChilds.getLength(); i++) 
            {
                 aChild=(Element)theChilds.item(i);
                if (aChild.getTagName().equals("Category")) 
                {
                    break;
                } // end of if ()
                aChild = null;
            } // end of for ()
            theDocument.getDocumentElement().insertBefore(attributeElement,aChild);
                        
                // set the id of the element
            attributeElement.setAttribute("ID", generateXMLID(attribute.getGuid()));
            attributeElement.setAttribute("type",attribute.getType());
            if (attribute.isLanguageDependent()) 
            {
                attributeElement.setAttribute("isLanguageDependent","true");                
            } // end of if ()
            
                // generate the details
            Iterator detailsIter = attribute.getDetailsInternal();
            theCurrentElement = attributeElement;
            while(detailsIter.hasNext())
            {
                CatalogDetail detail = (CatalogDetail)detailsIter.next();
                if(!detail.isDeleted())
                {
                    theCurrentElement =  attributeElement;
                    detail.assign(this);
                }
            }
            theCurrentElement = null;
        } // end of if ()
        
        StringBuffer theOwners = new StringBuffer(attributeElement.getAttribute("owners"));

        theOwners.append(generateXMLID(parentElement.getAttribute("ID")));
        theOwners.append(" ");
        
        attributeElement.setAttribute("owners",theOwners.toString());

            // restore parent element
        theCurrentElement = parentElement;
        return;
    }

    private Element lookUpAttribute(String aGuid)
    {
        NodeList theAttributes = theDocument.getElementsByTagName("Attribute");
        Element theAttribute =null;
        int size = theAttributes.getLength();
        for (int i = 0; i < size; i++) 
        {
            Element anAttribute = (Element)theAttributes.item(i);
            if (anAttribute.getAttribute("ID").equals(aGuid)) 
            {
                theAttribute = anAttribute;
                break;
            } // end of if ()
        } // end of for ()
        return theAttribute;
    }
    
    /**
     * <code>
     *  Here are finally the data. It carries a reference to its attribute.
     *  The interpretation of the one or more Values depends on the type
     *  of the Attribute and its unit
     * <!ELEMENT AttributeValue (Value+)>
     * <!ATTLIST AttributeValue
     * 	attribute IDREF #REQUIRED
     * 	unit IDREF #IMPLIED
     * 	status (loaded | added | deleted | changed) #IMPLIED
     * >
     * </code>
     */
    public void visitAttributeValue(CatalogAttributeValue attrValue)
    {
        // store parent element
        Element parentElement = theCurrentElement;

        // create attribute value element
        Element attributeValueElement = theDocument.createElement("AttributeValue");
        parentElement.appendChild(attributeValueElement);

        // set attribute of element
        attributeValueElement.setAttribute("attribute", generateXMLID(attrValue.getAttributeGuid()));

            //determine language dependency
        boolean isLanguageDependent =
            ((CatalogAttribute)attrValue.getAttribute()).isLanguageDependent();
        Locale theDefaultLocale = attrValue.getRoot().getDefaultLocale();
        StringBuffer aBuffer = new StringBuffer(theDefaultLocale.getLanguage());
        if (theDefaultLocale.getCountry().intern()!="".intern() ) 
        {
            aBuffer.append("-");
            aBuffer.append(theDefaultLocale.getCountry());            
        } // end of if ()
        
        Iterator valuesIterator = attrValue.getAllAsString();
        while(valuesIterator.hasNext())
        {
            String value = (String)valuesIterator.next();
            Element valueElement = theDocument.createElement("Value");
            if (isLanguageDependent) 
            {
                valueElement.setAttribute("xml:lang",aBuffer.toString());
            } // end of if ()
            
            Text valueValueElement = theDocument.createTextNode(value);
            valueElement.appendChild(valueValueElement);
            attributeValueElement.appendChild(valueElement);
        }

        // restore parent element
        theCurrentElement = parentElement;
    }

    /**
     * <code>
     * <!ELEMENT QueryStatement (#PCDATA)>
     * </code>
     */
    public void visitQueryStatement(CatalogQueryStatement statement)
    {
        // store parent element
        Element parentElement = theCurrentElement;

        // generate query statement
        Element queryStatementElement = theDocument.createElement("QueryStatement");
        parentElement.appendChild(queryStatementElement);

        String statementString = statement.getStatementAsString();
        if(statement.getFilter() != null)
            statementString = statement.getFilter().toString();
        Text value = theDocument.createTextNode(statementString);
        queryStatementElement.appendChild(value);
    }

    /**
     * <code>
     * <!ELEMENT Query (QueryStatement, Query*, QueryItem*)>
     * <!ATTLIST Query
     *      ID ID #REQUIRED>
     * </code>
     */
    public void visitQuery(CatalogQuery query)
    {
        // store parent element
        Element parentElement = theCurrentElement;

        // create query element
        Element queryElement = theDocument.createElement("Query");
        parentElement.appendChild(queryElement);

        // generate statement
        theCurrentElement = queryElement;
        query.getCatalogQueryStatement().assign(this);
        theCurrentElement = parentElement;

        // generate attributes
        Iterator attributesIter = query.getAttributesInternal();
        Element attributesElement = null;

        while(attributesIter.hasNext())
        {
            CatalogAttribute attr = (CatalogAttribute)attributesIter.next();
            if(!attr.isDeleted())
            {
                if(attributesElement == null)
                {
                    attributesElement = theDocument.createElement("Attributes");
                    parentElement.appendChild(attributesElement);
                    theCurrentElement = attributesElement;
                }
                attr.assign(this);
            }
        }
        theCurrentElement = parentElement;

        // generate items
        Iterator itemsIter = query.getItemsInternal();
        Element itemsElement = null;

        while(itemsIter.hasNext())
        {
            CatalogItem item = (CatalogItem)itemsIter.next();
            if(!item.isDeleted())
            {
                if(itemsElement == null)
                {
                    itemsElement = theDocument.createElement("Items");
                    parentElement.appendChild(itemsElement);
                    theCurrentElement = itemsElement;
                }
                item.assign(this);
            }
        }
        theCurrentElement = parentElement;

        // restore parent element
        theCurrentElement = parentElement;
    }

    private String generateXMLID(String anID)
    {   
        if (!Character.isLetter(anID.charAt(0))) 
        {
            anID ="ID"+anID;
        } // end of if ()
        return anID.replace(' ','_').replace('\\','_').replace('/','_');
    }
    
}
