/*****************************************************************************
  Copyright (c) 2000, SAPMarkets Europe GmbH, Germany, All rights reserved.
  Created:      09 July 2001

  $Id: //sap/ESALES_base/30_SP_COR/src/_pcatAPI/java/com/sap/isa/catalog/catalogvisitor/CatalogVisitor.java#2 $
  $Revision: #2 $
  $Change: 129221 $
  $DateTime: 2003/05/15 17:20:58 $
*****************************************************************************/

package com.sap.isa.catalog.catalogvisitor;

//  logging
import org.apache.struts.util.MessageResources;

import com.sap.isa.catalog.impl.Catalog;
import com.sap.isa.catalog.impl.CatalogAttribute;
import com.sap.isa.catalog.impl.CatalogAttributeValue;
import com.sap.isa.catalog.impl.CatalogCategory;
import com.sap.isa.catalog.impl.CatalogDetail;
import com.sap.isa.catalog.impl.CatalogItem;
import com.sap.isa.catalog.impl.CatalogQuery;
import com.sap.isa.catalog.impl.CatalogQueryItem;
import com.sap.isa.catalog.impl.CatalogQueryStatement;

/**
 *  Abstract base class for all visitors of the catalog tree.
 *
 *  @version     1.0
 *  @since       2.0
 */
abstract public class CatalogVisitor
{
    private Catalog theRoot;

    /**
     * Default constructor for the visitor.
     *
     * @param root the root node of the tree
     */
    protected CatalogVisitor(Catalog root)
    {
        theRoot = root;
    }

    /**
     * Returns the message resource bundle for all messages
     *
     * @return the message resources
     */
    protected MessageResources getMessageResources()
    {
        return theRoot.getMessageResources();
    }

    /**
     * Returns the root node of the catalog tree.
     *
     * @return the root of the tree
     */
    protected Catalog getRoot()
    {
        return theRoot;
    }

    /**
     * Visitor method for the catalog node.
     *
     * @param catalog the node that has to be visited
     */
    public abstract void visitCatalog(Catalog catalog);

    /**
     * Visitor method for the category node.
     *
     * @param category the node that has to be visited
     */
    public abstract void visitCategory(CatalogCategory category);

    /**
     * Visitor method for the item node.
     *
     * @param item the node that has to be visited
     */
    public abstract void visitItem(CatalogItem item);

    /**
     * Visitor method for the query item node.
     *
     * @param query item the node that has to be visited
     */
    public abstract void visitQueryItem(CatalogQueryItem item);

    /**
     * Visitor method for the detail node.
     *
     * @param detail the node that has to be visited
     */
    public abstract void visitDetail(CatalogDetail detail);

    /**
     * Visitor method for the attribute node.
     *
     * @param attribute the node that has to be visited
     */
    public abstract void visitAttribute(CatalogAttribute attribute);

    /**
     * Visitor method for the attribute value node.
     *
     * @param attrValue the node that has to be visited
     */
    public abstract void visitAttributeValue(CatalogAttributeValue attrValue);

    /**
     * Visitor method for the query statement node.
     *
     * @param query the node that has to be visited
     */
    public abstract void visitQueryStatement(CatalogQueryStatement statement);

    /**
     * Visitor method for the query node.
     *
     * @param query the node that has to be visited
     */
    public abstract void visitQuery(CatalogQuery query);
}
