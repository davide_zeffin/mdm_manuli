/* 
 * Created on 31.01.2004
 *
 * To change the template for this generated file go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
package com.sap.isa.catalog;

import java.util.HashMap;
import java.util.StringTokenizer;

import com.sap.isa.catalog.webcatalog.WebCatBusinessObjectBase;
import com.sap.isa.catalog.webcatalog.WebCatItem;
import com.sap.isa.core.logging.IsaLocation;
/**
 * @author D034528
 *
 * To change the template for this generated type comment go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
public class ZWebCatItemAddAttr extends WebCatBusinessObjectBase {

	public final static String SEPARATION_STRING = "/";
	public final static String EXTENSION_VALUE_SEPARATOR = ";";

	public int rowsUOM;
	public int rowsHier;

	public String[][] uomArray;
	public String[][] hierArray;

	private static IsaLocation log =
		IsaLocation.getInstance(ZWebCatItemAddAttr.class.getName());
 
	/**
	 *  
	 */
	public ZWebCatItemAddAttr(WebCatItem item) {
		
		String zUOM = item.getAttribute("ZCFUOM");
		String zHier = item.getAttribute("ZCFHIER");

		if (log.isDebugEnabled()) {
			log.debug("XXXXX: zuom: " + zUOM + "; zhier: " + zHier);
		}
		
		// create Array for UOM
		HashMap rows = new HashMap();

		StringTokenizer tokenizer =
			new StringTokenizer(zUOM, SEPARATION_STRING);
		int t = tokenizer.countTokens();
		String tString = Integer.toString(t, 10);
		setRowsUOM(t);

		uomArray = new String[t][14];

		
		// parse String and create an Array for uom
		if (zUOM != null && zUOM.length() > 0) {
			//if (zUOM.indexOf(SEPARATION_STRING) > 0) {
				//row = zUOM.substring(0, zUOM.indexOf(SEPARATION_STRING));

				int i = 0;

				while (tokenizer.hasMoreTokens()) {

					String value = tokenizer.nextToken();
					String key = String.valueOf(i);

					rows.put(key, value);

					i++;
				}

				for (int k = 0; k < rows.size(); k++) {

					String keyK = String.valueOf(k);
					StringTokenizer tokenizer2 =
						new StringTokenizer(
							rows.get(keyK).toString(),
							EXTENSION_VALUE_SEPARATOR);
					int c = tokenizer.countTokens();

					int j = 0;

					while (tokenizer2.hasMoreTokens()) {

						String value = tokenizer2.nextToken();
						if (value == null)
						{value = " ";
						}
						String key = String.valueOf(j);
						//zUOMr.
						uomArray[k][j] = value;

						j++;
					}
				}
			//	System.out.println(uomArray);
			} else {
				//		shopText = wholeText;
			//}

		}

		// create Array for Hier
		HashMap rowsh = new HashMap();

		StringTokenizer tokenizerh =
			new StringTokenizer(zHier, SEPARATION_STRING);
		int th = tokenizerh.countTokens();
		setRowsHier(th);
		hierArray = new String[th][5];

		// parse String and create an Array for uom
		if (zHier != null && zHier.length() > 0) {
			//if (zHier.indexOf(SEPARATION_STRING) > 0) {
				//row = zUOM.substring(0, zUOM.indexOf(SEPARATION_STRING));

				int i = 0;

				while (tokenizerh.hasMoreTokens()) {

					String value = tokenizerh.nextToken();
					String key = String.valueOf(i);

					rowsh.put(key, value);

					i++;
				}

				for (int k = 0; k < rowsh.size(); k++) {

					String keyK = String.valueOf(k);
					StringTokenizer tokenizer3 =
						new StringTokenizer(
							rowsh.get(keyK).toString(),
							EXTENSION_VALUE_SEPARATOR);
					int c = tokenizer3.countTokens();

					int j = 0;

					while (tokenizer3.hasMoreTokens()) {

						String value = tokenizer3.nextToken();
						String key = String.valueOf(j);
						//zUOMr.
						hierArray[k][j] = value;

						j++;
					}
				}
				//System.out.println(zUOMr);
				if (log.isDebugEnabled()) {
							log.debug("XXXXX: ende");
						}
			} else {
				//		shopText = wholeText;
				
			//}

		}
	}

	/**
	 * @return
	 */
	public int getRowsUOM() {

		return rowsUOM;
	}

	/**
	 * @param string
	 */
	public void setRowsUOM(int integer) {
		rowsUOM = integer;
	}

	/**
		 * @return
		 */
	public String getUni(int row) {
		String value = uomArray[row][0];
		return value;
	}

	/**
	 * @return
	 */
	public String getNumerator(int row) {
		String value = uomArray[row][1];
		return value;
	}

	/**
	 * @return
	 */
	public String getDenominator(int row) {
		String value = uomArray[row][2];
		return value;
	}

	/**
		 * @return
		 */
		public String getExponent(int row) {
			String value = uomArray[row][3];
			return value;
		}
		
	/**
	 * @return
	 */
	public String getGrossWeight(int row) {
		String value = uomArray[row][4];
		return value;
	}

	/**
	 * @return
	 */
	public String getNetWeight(int row) {
		String value = uomArray[row][5];
		return value;
	}

	/**
		 * @return
		 */
		public String getWeightUnit(int row) {
			String value = uomArray[row][6];
			return value;
		}
		
	/**
	 * @return
	 */
	public String getVolume(int row) {
		String value = uomArray[row][7];
		return value;
	}

	/**
	 * @return
	 */
	public String getVolumeUnit(int row) {
		String value = uomArray[row][8];
		return value;
	}

	
	/**
	 * @return
	 */
	public String getLength(int row) {
		String value = uomArray[row][9];
		return value;
	}

	/**
	 * @return
	 */
	public String getWidth(int row) {
		String value = uomArray[row][10];
		return value;
	}

	/**
	 * @return
	 */
	public String getHeight(int row) {
		String value = uomArray[row][11];
		return value;
	}

	/**
	 * @return
	 */
	public String getUnitOfDim(int row) {
		String value = uomArray[row][12];
		return value;
	}

	/**
	 * @return
	 */
	public boolean isBaseUnit(int row) {
		String value = uomArray[row][13];
		if (value.equals("X")) {
			return true;
		} else {
			return false;
		}

	}

	//////////////////////////hierarchien//////////

	/**
		 * @return
		 */
		public int getRowsHier() {

			return rowsHier;
		}

		/**
		 * @param string
		 */
		public void setRowsHier(int integer) {
			rowsHier = integer;
		}
		
	/**
	 * @return
	 */
	public int getHierSize() {

		return rowsHier;
	}

	/**
	 * @return
	 */
	public String getHierarchieID(int row) {
		String value = hierArray[row][0];
		return value;
	}

	/**
	 * @return
	 */
	public String getHierarchieDesc(int row) {
		String value = hierArray[row][1];
		return value;
	}

	/**
	 * @return
	 */
	public String getCategorieID(int row) {
		String value = hierArray[row][2];
		return value;
	}

	/**
	 * @return
	 */
	public String getCategorieDesc(int row) {
		String value = hierArray[row][3];
		return value;

		
	}

}
