/*****************************************************************************
  Copyright (c) 2001, SAPMarkets Europe GmbH, Germany, All rights reserved.
  Created:  27 March 2001

  $Revision: #2 $
  $Date: 2003/05/15 $
*****************************************************************************/

package com.sap.isa.catalog.boi;

import com.sap.isa.catalog.filter.CatalogFilter;

/**
 * This class represents a select statement of a query.
 *
 * @since   1.0
 * @version 2.0
 */
public interface IQueryStatement extends IComponent
{
    /**
     * Sets a query string. A content engine specific default quick search will
     * be performed. If the underlying query was already submitted the statement
     * <B>can't</B> be changed. In this case a new query has to be built.
     *
     * @since   1.0
     * @param   aQuery  value which has to be searched for
     */
    void setStatementAsString(String aQuery);
    
	
	/**
	 * Sets a query string. A content engine specific default quick search will
     * be performed. If the underlying query was already submitted the statement
     * <B>can't</B> be changed. In this case a new query has to be built.
     * 
     * Additionally a search range can be set to select an interval in the
     * result set.
	 * @param aQuery 	value which has to be searched for
	 * @param from 		start of search range
	 * @param to   		end of search range
	 */
	void setStatementAsString(String aQuery, int from, int to);

    /**
     * Sets a filtered statement. The filter has to constructed with
     * the {@link com.sap.isa.catalog.filter.CatalogFilterFactory}.
     * If the underlying query was already submitted the statement
     * <B>can't</B> be changed. In this case a new query has to be built.
     *
     * Only the attributes will be returned which have been specified in the
     * attribute list. If the attribute list is <code>null</code> all
     * attributes are returned.
     *
     * The result set will be sorted ascending according to the order of the
     * attributes in the given list if the underlying content engine supports
     * this behavior. If the list is <code>null</code> no sorting will be
     * performed.
     *
     * @since   1.0
     * @param   aFilter         reference to a filter expression
     * @param   aAttributeList  list of requested attributes; if <code>null</code>
     *                        all attributes will be returned
     * @param   aSortOrder      defines the order of the selected attributes
     */
    void setStatement(  IFilter aFilter,
                        String[] aAttributeList,
                        String[] aSortOrder);

    /**
     * Sets a filtered statement. The filter has to constructed with
     * the {@link com.sap.isa.catalog.filter.CatalogFilterFactory}.
     * If the underlying query was already submitted the statement
     * <B>can't</B> be changed. In this case a new query has to be built.
     *
     * Only the attributes will be returned which have been specified in the
     * attribute list. If the attribute list is <code>null</code> all
     * attributes are returned.
     *
     * The result set will be sorted ascending according to the order of the
     * attributes in the given list if the underlying content engine supports
     * this behavior. If the list is <code>null</code> no sorting will be
     * performed.
     *
     * Additionaly a search range can be set to select an interval in the
     * result set.
     *
     * @since   1.0
     * @param   aFilter         reference to a filter expression
     * @param   aAttributeList  list of requested attributes
     * @param   aSortOrder      defines the order of the selected attributes
     * @param   from            start of search range
     * @param   to              end of search range
     */
    void setStatement(  IFilter aFilter,
                        String[] aAttributeList,
                        String[] aSortOrder,
                        int from,
                        int to);

    /**
     * Returns the name of the statement. Basically all arguments of this
     * statement are concatenated. This method should be called after the
     * statement was set via the setter methods.
     *
     * @since   1.0
     * @return  the name of the statement
     */
    String getName();

	/**
	 * Returns the statement as a string. This method had to be added to provide
	 * the option of getting the statement directly instead of using any impl class. 
	 * @since   04-SEP-2004
	 * @return  returns the statement as a String
	 */    
	String getStatementAsString();

    /**
     * Sets the kind of search that should be performed through this statement.
     * If no value was set the default value (search for items) will be associated
     * with the query statement. For possible values see {@link IQueryStatement.Type}.
     *
     * @since 2.0
     * @param type the search type to be associated with the statement
     */
    void setSearchType(IQueryStatement.Type type);

    /**
     * Returns the kind of search that should be performed through this statement.
     * If no value was set the default value (search for items) will be associated
     * with the query statement. For possible values see {@link IQueryStatement.Type}.
     * @since 7.0
     * @return type the search type to be associated with the statement
     */
    public IQueryStatement.Type getSearchType();

    /**
     * Enumeration of all types that can be associated with the query statement
     * (typesafe enum idiom).
     *
     * @since       2.0
     * @version     1.0
     */
    public static class Type
    {
        private final String theName;

        private Type(String name)
        {
            theName = name;
        }

        public String toString()
        {
            return this.theName;
        }

        /**
         * Enumeration constant for the type CATEGORY_SEARCH i.e. the result set of
         * the query will contain instances of type <code>ICategory</code>.
         */
        public final static Type CATEGORY_SEARCH = new Type("Search for categories");

        /**
         * Enumeration constant for the type ITEM_SEARCH i.e. the result set of the
         * query will contain instances of type <code>IQueryItem</code>.
         */
        public final static Type ITEM_SEARCH = new Type("Search for items");
        
        /**
         * Enumeration constant for the type ITEM_ADVSEARCH i.e. the result set of the
         * query will contain instances of type <code>IQueryItem</code>.
         */
        public final static Type ITEM_ADVSEARCH = new Type("Advanced Search for items");
    }
    
	/**
	 * Returns the filter expression associated with the statement.
	 *
	 * @return the filter instance
	 */
	public CatalogFilter getFilter();
}
