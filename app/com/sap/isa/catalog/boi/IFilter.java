/*****************************************************************************
  Copyright (c) 2001, SAPMarkets Europe GmbH, Germany, All rights reserved.
  Created:  27 March 2001

  $Revision: #2 $
  $Date: 2003/05/15 $
*****************************************************************************/
package com.sap.isa.catalog.boi;

/**
 * This interface is a tagging interface for all filters which can be
 * associated with a {@link IQueryStatement query statement}. The concrete
 * filter instances have to be instantiated via the
 * {@link com.sap.isa.catalog.filter.CatalogFilterFactory
 * <code>CatalogFilterFactory</code>}.
 *
 * @since       1.0
 * @version     2.0
 */
public interface IFilter
{
}