/*****************************************************************************
  Copyright (c) 2000, SAPMarkets Europe GmbH, Germany, All rights reserved.
  Created:      10 July 2001

  $Id: //sap/ESALES_base/30_SP_COR/src/_pcatAPI/java/com/sap/isa/catalog/boi/IInterval.java#3 $
  $Revision: #3 $
  $Change: 150872 $
  $DateTime: 2003/09/29 09:18:51 $
*****************************************************************************/
package com.sap.isa.catalog.boi;


/**
 * In case the Attribute has a Domain that is an interval it might return
 * IIntervals or Doubles.
 *
 * @since       2.0
 * @version     1.0
 */
public interface IInterval
{
    Comparable max = null;
    Comparable min = null;
    boolean includeMin = true;
    boolean includeMax = true;
}
