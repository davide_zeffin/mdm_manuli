/*****************************************************************************
  Copyright (c) 2000, SAPMarkets Europe GmbH, Germany, All rights reserved.
  Created:      29 March 2001

  $Id: //sap/ESALES_base/30_SP_COR/src/_pcatAPI/java/com/sap/isa/catalog/boi/IDetail.java#2 $
  $Revision: #2 $
  $Change: 129221 $
  $DateTime: 2003/05/15 17:20:58 $
*****************************************************************************/

package com.sap.isa.catalog.boi;

/**
 * This interface represents detailed information about a specific component of
 * a catalog.
 * That is typicaly meta information about the specific catalog component that
 * can not be or should not be presented in the API.
 *
 * @since       1.0
 * @version     2.0
 */
public interface IDetail extends IComponent {

    /**
     * Common name of details. <br>
     * Constant that describes the name of the attribute in which the currency
     * of an item is stored.
     */
    public final static String ATTRNAME_CURRENCY = "ATTRNAME_CURRENCY";
    /**
     * Common name of details. <br>
     * Constant that describes the name of the attribute in which the price
     * of an item is stored.
     */
    public final static String ATTRNAME_PRICE = "ATTRNAME_PRICE";

    /**
     * Common name of details. <br>
     * Constant that describes the name of the attribute in which the price type
     * of an item is stored.
     */
    public final static String ATTRIBUTE_PRICETYPE = "ATTRIBUTE_PRICETYPE";

    /**
     * Common name of details. <br>
     * Constant that describes the name of the attribute in which the promotion attribute
     * of an item is stored.
     */
    public final static String ATTRIBUTE_PROMOTION_ATTR = "ATTRIBUTE_PROMOTION_ATTR";

    /**
     * Common name of details. <br>
     * Constant that describes the name of the attribute in which the quantity
     * of an item is stored.
     */
    public final static String ATTRNAME_QUANTITY = "ATTRNAME_QUANTITY";

    /**
     * Common name of details. <br>
     * Constant that describes the name of the attribute in which the scale type
     * of an item is stored.
     */
    public final static String ATTRNAME_SCALETYPE = "ATTRNAME_SCALETYPE";

    /**
     * Common name of details. <br>
     * Constant that describes the name of the attribute in which the UOM
     * of an item is stored.
     */
    public final static String ATTRNAME_UOM = "ATTRNAME_UOM";

    /**
     * Trys to get an array of strings of the value(s) of the detail.
     *
     * @since   1.0
     * @return  an array of strings of the detail, that is sorted by the values
     */
    String[] getAllAsString();

    /**
     * Trys to get an array of strings of the value(s) of the detail.
     *
     * @since   1.0
     * @return  an array of strings of the detail, that is not sorted
     */
    String[] getAllAsStringUnsorted();
    
    /**
     * Trys to get a string representation of the value.
     *
     * @since   1.0
     * @return  the string representation of the detail
     */
    String getAsString();

    /**
    * Returns the name of the detail.
    *
    * @since   1.0
    * @return  the name of the detail
    */
    String getName();
}
