/*****************************************************************************
  Copyright (c) 2000, SAPMarkets Europe GmbH, Germany, All rights reserved.

  $Id: //sap/ESALES_base/30_SP_COR/src/_pcatAPI/java/com/sap/isa/catalog/boi/ISite.java#4 $
  $Revision: #4 $
  $Change: 150872 $
  $DateTime: 2003/09/29 09:18:51 $
*****************************************************************************/
package com.sap.isa.catalog.boi;

import java.util.Iterator;
import java.util.Locale;

import org.w3c.dom.Document;

import com.sap.isa.core.eai.BackendBusinessObject;

/**
 * The highest level abstraction in the catalog API. It might represent an 
 * arbitrary collection of catalog engines and catalogs served by these 
 * engines. If catalog spanning information is needed this is the entry 
 * point to get to that in formation. You might get an instance of of this 
 * by calling the
 * {@link com.sap.isa.catalog.CatalogFactory#getCatalogSite(IActor) CatalogFactory} 
 *
 * @since 2.0
 * @version 2.0
 */

public interface ISite extends
                           BackendBusinessObject
{
	/**
     * Returns a list of {@link IServerEngine} instances collected in this side. 
     *
     * @return an <code>Iterator</code> that returns {@link IServerEngine} instances.
     */
    Iterator getCatalogServers();

    	/**
         * Return a specific {@link IServerEngine} instance with the given GUID.
         * If the given GUID is not know null will be returned.
         *
         * @param aGuid a <code>String</code> aGuid for a server
         * @return an <code>IServerEngine</code> or null.
         */
    IServerEngine getCatalogServer(String aGuid);
    
        /**
         * The GUID of this site.
         *
         * @return a <code>String</code> value of the GUID.
         */
    String getGuid();
    
        /**
         * A short language dependent name of the site.
         *
         * @return a <code>String</code> value
         */
    String getName();

        /**
         * A short language dependent description of the site.
         *
         * @return a <code>String</code> value
         */
    String getDescription();

        /**
         * The configuration document.
         * A catalog site is basically an instantiation of a site configuration.
         * The call returns the original document used to create this instance.
         *
         * @return a <code>Document</code> value
         */
    Document getConfigDocument();
    
        /**
         * The current locale of the site.
         * During construction the servers inherit that locale as default.
         *
         * @return a <code>Locale</code> value
         */
    Locale getLocale();

        /**
         * Set the locale to another locale known by the site.
         *
         * @param aLocale a <code>Locale</code> value
         * @exception CatalogException if the locale is not know by the site
         */
    void setLocale(Locale aLocale)
        throws CatalogException;

        /**
         * @link aggregation
         * @clientCardinality 1
         * @supplierCardinality 0..*
         * @directed
         * @label hasServers 
         */
        /*#IServerEngine lnkIServerEngine;*/  
}
