/*****************************************************************************
  Copyright (c) 2000, SAPMarkets Europe GmbH, Germany, All rights reserved.
  Created:      22 March 2001

  $Id: //sap/ESALES_base/30_SP_COR/src/_pcatAPI/java/com/sap/isa/catalog/boi/IncompleteCommandException.java#2 $
  $Revision: #2 $
  $Change: 129221 $
  $DateTime: 2003/05/15 17:20:58 $
*****************************************************************************/
package com.sap.isa.catalog.boi;

/**
 * Used only internaly by a concrete implementation.
 *
 * Thrown by low level processing of concrete implementations. Finally
 * catched inside the API for logging.
 * <a href="@docRoot/com/sap/isa/catalog/doc-files/ProductCatalogAPIDesign.html">Catalog API</a>
 *
 * @since       1.0
 * @version     2.0
 */
public class IncompleteCommandException extends Exception
{
    public IncompleteCommandException(String msg)
    {
        super(msg);
    }
}