/*****************************************************************************
  Copyright (c) 2000, SAPMarkets Europe GmbH, Germany, All rights reserved.
  Created:      22 March 2001

  $Id: //sap/ESALES_base/30_SP_COR/src/_pcatAPI/java/com/sap/isa/catalog/boi/IHttpCommand.java#3 $
  $Revision: #3 $
  $Change: 150872 $
  $DateTime: 2003/09/29 09:18:51 $
*****************************************************************************/
package com.sap.isa.catalog.boi;

import java.io.IOException;
import java.io.InputStream;
import java.util.Iterator;

import org.xml.sax.ContentHandler;
import org.xml.sax.SAXException;

/**
 * Used only internaly by a concrete implementation.
 *
 * Represents  a specific command (http) a specific catalog
 * engine might support via its (http) interface.
 *
 * @since       1.0
 * @version     2.0
 */
public interface IHttpCommand
{
    /**
     * The keys that have ben set for the command.
     *
     * @return an iterator over the keys of type Sting.
     */
    Iterator getKeys();

    /**
     * To set a parameter before execution.
     */
    void setParameter(String key, Object value);

    /**
     * To set a Post parameter before execution.
     */
    void setPostParameter(String key, Object value);

    /**
     * Add dynamicaly command parameters.
     */
    void addCommandParameter(String key, Object value);

    /**
     * Excecute the http command.
     *
     * @return InputStream the result of the command.
     */
    InputStream excecute()
        throws  IncompleteCommandException,
			    CatalogException;

    /**
     * In case the input stream is of "content type" text/xml execute the
     * command with a specific Contenthandler.
     *
     * @param ContentHandler a SAX content handler for the in comming XML.
     */
    void excecute(ContentHandler aContendHandler)
        throws  SAXException,
			    IOException,
				IncompleteCommandException,
				CatalogException;
}