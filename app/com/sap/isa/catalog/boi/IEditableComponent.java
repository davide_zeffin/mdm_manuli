/*****************************************************************************
  Copyright (c) 2000, SAPMarkets Europe GmbH, Germany, All rights reserved.
  Created:      01 August 2001

  $Id: //sap/ESALES_base/30_SP_COR/src/_pcatAPI/java/com/sap/isa/catalog/boi/IEditableComponent.java#2 $
  $Revision: #2 $
  $Change: 129221 $
  $DateTime: 2003/05/15 17:20:58 $
*****************************************************************************/
package com.sap.isa.catalog.boi;

public interface IEditableComponent extends IComponent
{
    /**
     * Returns the referenc to the thing we are editing via this.
     * Might be null in case this is new.
     *
     * @return aRef to the object we are about to edit.
     */
     IComponent getComponentReference();

    /**
     * Lock the component this is editing.
     *
     * @return ther successe of the locking attempt
     */
    boolean lock();

    /**
     * Release the lock on the component this is editing.
     *
     */
    void releaseLock();
}
