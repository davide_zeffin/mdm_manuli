/*****************************************************************************
  Copyright (c) 2000, SAPMarkets Europe GmbH, Germany, All rights reserved.

  $Id: //sap/ESALES_base/30_SP_COR/src/_pcatAPI/java/com/sap/isa/catalog/boi/IAttributeDeletedEvent.java#3 $
  $Change: 144303 $
  $DateTime: 2003/08/22 20:18:21 $
*******************************************************************************/
package com.sap.isa.catalog.boi;

/**
 * Event that is fired if an attribute is deleted form a composite.
 * {@see ICompositeEventListener}
 *
 */
public interface IAttributeDeletedEvent
{
    /**
     *
     * @return the parent of the created attribute
     */
    IComponent getComponent();

    /**
     *
     * @return the deleted attribute
     */
     IAttribute getAttribute();
}
