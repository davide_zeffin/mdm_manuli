/*****************************************************************************
  Copyright (c) 2000, SAPMarkets Europe GmbH, Germany, All rights reserved.
  Created:      29 March 2001

  $Id: //sap/ESALES_base/30_SP_COR/src/_pcatAPI/java/com/sap/isa/catalog/boi/IAttribute.java#3 $
  $Revision: #3 $
  $Change: 150872 $
  $DateTime: 2003/09/29 09:18:51 $
*****************************************************************************/
package com.sap.isa.catalog.boi;

import java.util.Comparator;
import java.util.Iterator;

/**
 * Represents a property of a product.
 * {@link ICatalog ICatalogs} and {@link ICategory ICategories} have
 * assoziations to their attributes.
 *
 * @since       1.0
 * @version     2.0
 */
public interface IAttribute extends IComponent
{
    /**
     * The corresponding values (AttributeValue) <emph>might</emph>
     * contain more than one value. So the value of the Attribute migth be a set.
     *
     * @since 1.0
     * @return boolean that indicates if it is multi valued attribute
     */
    boolean isMultiValued();

    /**
     * The type of the attribute. The value might be one of the following:
     * {@link IAttribute IAttribute.TYPE_ID_*} final static identifiers.
     *
     * @since 1.0
     * @return the actual Type of the attribute
     */
    String getType();

    /**
     * The quantity of the attribute like: mass, number etc.
     *
     * @since 1.0
     * @return the quantity of the attribute
     */
    IQuantity getQuantity();

    /**
     * Get the domain for the attribute.
     *
     *
     * @return the domain of the attribute
     */
    IDomain getDomain();

    /**
     * Signals if the attribute at hand is a base attribute.
     *
     * @since 1.0
     * @return boolean that indicates if it is a catalog attribute
     */
    boolean isBase();

    /**
     * Returns the language dependent name of the attribute.
     *
     * @since 1.0
     * @return the name of the attribute
     */
    String getName();

    /**
     * Returns the language dependent description of the attribute.
     *
     * @since 1.0
     * @return the description of the attribute
     */
    String getDescription();

    /**
     * Returns an iterator for all fixed values of type {@link IAttributeValue}.
     * The instances have the default sort order.
     *
     * @since 1.0
     * @return the fixed values
     */
    Iterator getFixedValues();
    
    /**
     * Returns the number of fixed values, or 0 if ther are none
     *
     * @return number of fixed values 
     */
    public int getFixedValuesSize();

    /**
     * Returns an iterator for all fixed values of type {@link IAttributeValue}.
     * The instances are sorted according to the
     * given comparator.
     *
     * @since 1.0
     * @param aComparator the comparator for sorting the instances
     * @return the fixed values
     */
    Iterator getFixedValues(Comparator aComparator);

    /**
     * Returns the unique identifier for the attribute.
     *
     * @since 1.0
     * @return a guid for the attribute
     */
    String getGuid();

    public static final String TYPE_ID_BOOLEAN	    = "Boolean";
    public static final String TYPE_ID_INTEGER 	    = "Integer";
    public static final String TYPE_ID_STRING 	    = "String";
    public static final String TYPE_ID_URL     	    = "URL";
    public static final String TYPE_ID_DOUBLE 	    = "Double";
    public static final String TYPE_ID_LONG 	    = "Long";
    public static final String TYPE_ID_DATE 	    = "Date";
    public static final String TYPE_ID_OBJECT 	    = "Object";
    public static final String TYPE_ID_INTERVAL     = "Interval";

}
