/*****************************************************************************
  Copyright (c) 2000, SAPMarkets Europe GmbH, Germany, All rights reserved.
  Created:      01 August 2001

  $Id: //sap/ESALES_base/30_SP_COR/src/_pcatAPI/java/com/sap/isa/catalog/boi/IEditableItem.java#2 $
  $Revision: #2 $
  $Change: 129221 $
  $DateTime: 2003/05/15 17:20:58 $
*****************************************************************************/
package com.sap.isa.catalog.boi;



/**
 * Extends the {@link IItem IItem} interface so that the item can be edited.
 *
 * @since       2.0
 * @version     1.0
 */
public interface IEditableItem extends IItem, IEditableHierarchicalComponent
{
    /**
     * Returns the referenc to the thing we are editing via this.
     * Might be null in case this is new.
     *
     * @return aRef to the object we are about to edit.
     */
     IItem getItemReference();

    /**
     * Sets the language dependent name of the item.
     *
     * @since 2.0
     * @param aName the new name of the item
     * @exception CatalogException error while setting the name
     */
    void setName(String aName) throws CatalogException;

    /**
     * Sets the unique identifier for the product the item represents. The same
     * product might appear in the same catalog in different categories.
     *
     * @since 2.0
     * @param aGuid the new guid of the item
     * @exception CatalogException error while setting the guid
     */
    void setProductGuid(String aGuid) throws CatalogException;

    /**
     * Creates a new value for the given attribute instance.
     *
     * @since 2.0
     * @param aAttribute the attribute of the value
     * @return the newly created value
     * @exception CatalogException error while creating the value
     */
    IEditableAttributeValue createAttributeValue(IEditableAttribute aAttribute)
        throws CatalogException;

    /**
     * Deletes the given attribute value.
     *
     * @since 2.0
     * @param aValue the value to be deleted
     * @exception CatalogException error while deleting the value
     */
    void deleteAttributValue(IEditableAttributeValue aValue)
        throws CatalogException;

    /**
     * Creates a new Detail with the given name.
     *
     * @since 2.0
     * @param aName the name of the new detail
     * @return the new detail instance
     * @exception CatalogException error while creating the detail
     */
    IEditableDetail createDetail(String aName) throws CatalogException;

    /**
     * Deletes a Detail instance.
     *
     * @since 2.0
     * @param aDetail the detail to be deleted
     * @exception CatalogException error while deleting the detail
     */
    void deleteDetail(IEditableDetail aDetail) throws CatalogException;
}
