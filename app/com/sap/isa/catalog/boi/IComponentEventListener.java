/*****************************************************************************
  Copyright (c) 2000, SAPMarkets Europe GmbH, Germany, All rights reserved.
  Created:      09 July 2001

  $Id: //sap/ESALES_base/30_SP_COR/src/_pcatAPI/java/com/sap/isa/catalog/boi/IComponentEventListener.java#2 $
  $Revision: #2 $
  $Change: 129221 $
  $DateTime: 2003/05/15 17:20:58 $
*******************************************************************************/
package com.sap.isa.catalog.boi;

import java.util.EventListener;

/**
 * A listener interface for objects interested in component specific events.
 *
 * @since       2.0
 * @version     1.0
 */
public interface IComponentEventListener extends EventListener
{
    /**
     * A component was changed. The listener is informed about the source
     * component.
     *
     * @param aChangedEvent an event that has the details
     */
    void componentChanged(IComponentChangedEvent aChangedEvent);

    /**
     * A detail was created. The listener is informed about the source
     * component and the new detail.
     *
     * @param aCreatedEvent an event that has the details
     */
    void detailCreated(IDetailCreatedEvent aCreatedEvent);

    /**
     * A detail was deleted. The listener is informed about the source
     * component and the deleted detail.
     *
     * @param aChangedEvent an event that has the details
     */
    void detailDeleted(IDetailDeletedEvent aDeletedEvent);
}
