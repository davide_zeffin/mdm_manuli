/*****************************************************************************
  Copyright (c) 2000, SAPMarkets Europe GmbH, Germany, All rights reserved.
  Created:      29 March 2001

  $Id: //sap/ESALES_base/30_SP_COR/src/_pcatAPI/java/com/sap/isa/catalog/boi/ICatalog.java#3 $
  $Revision: #3 $
  $Change: 150872 $
  $DateTime: 2003/09/29 09:18:51 $
*****************************************************************************/
package com.sap.isa.catalog.boi;

import java.util.Comparator;
import java.util.Iterator;
import java.util.Locale;
import java.util.Set;

import com.sap.isa.core.util.table.ResultData;

/**
 * The central interface of the catalog API.
 * It allows access to all dependent instances of the remaining interfaces.
 * You obtain an instance of ICatalog via the
 * {@link com.sap.isa.catalog.CatalogFactory}.
 *
 * @since       1.0
 * @version     2.0
 */
public interface ICatalog extends IComponent
{
        /**
         * Get the version number of the API
         *
         * @since 1.0
         * @return a String representing the version info of the API
         */
    String getAPIVersion();

        /**
         * Get an editable version of the the catalog.
         * The editable catalog works like an editor for the underlying catalog.
         *
         * @since 2.0
         * @return an editor for this catalog
         */
    IEditableCatalog getEditor(IActor aUser);

        /**
         * The ID
         *
         * @since 1.0
         * @return the unique identifier of the catalog
         */
    String getGuid();

        /**
         * Get the guid of the server engine {@link IServerEngine }
         * that manages this instance.
         * 
         * @ since 2.0 
         * @return a <code>String</code> value
         */
    String getServerGuid();

        /**
         * Gets the language dependent name of the catalog.
         *
         * @since 1.0
         * @return name of the catalog
         */
    String getName();

        /**
         * Gets the language dependent long description of the catalog.
         *
         * @since 1.0
         * @return description of the catalog
         */
    String getDescription();

        /**
         * Returns the thumbnail associated with this instance.
         *
         * @since 1.0
         * @return the thumbnail of the catalog
         */
    String getThumbNail();

        /**
         * Returns an iterator for <em>all</em> categories of the catalog i.e. the
         * flat list of all categories.
         * The returned instances of type {@link ICategory}
         * are sorted by name.
         *
         * @since 1.0
         * @return an iterator for all categories
         * @exception CatalogException error while retrieving the data from the server
         */
    Iterator getCategories() throws CatalogException;

        /**
         * Returns an iterator for <em>all</em> categories of the catalog. The
         * returned instances of type {@link ICategory}
         * are sorted by the given comparator.
         *
         * @since 1.0
         * @param aComparator the comparator for sorting the instances
         * @return iterator for all categories, sorted
         * @exception CatalogException error while retrieving the data from the server
         */
    Iterator getCategories(Comparator aComparator) throws CatalogException;

        /**
         * Returns an iterator for all root categories. The instances of type
         * {@link ICategory} are sorted by the default
         * sort order.
         *
         * @since 1.0
         * @return iterator for all root categories
         * @exception CatalogException error while retrieving the data from the server
         */
    Iterator getChildCategories() throws CatalogException;

        /**
         * Returns an iterator for all root categories. The instances of type
         * {@link ICategory} are sorted by the given
         * comparator.
         *
         * @since 1.0
         * @param aComparator the comparator for sorting the instances
         * @return iterator for all root categories
         * @exception CatalogException error while retrieving the data from the server
         */
    Iterator getChildCategories(Comparator aComparator) throws CatalogException;

        /**
         * Gets a given category defined by the guid. If nothing is found
         * <code>null</code> will be returned.
         *
         * @since 1.0
         * @param CategoryGuid the guid of the requested category
         * @return the specific category
         * @exception CatalogException error while retrieving the data from the server
         */
    ICategory getCategory(String CategoryGuid) throws CatalogException;

        /**
         * Returns an iterator for the global attributes of the catalog.
         * For local attributes of the category see {@link ICategory#getAttributes()}.
         * The instances of type {@link IAttribute}
         * are sorted by the default sort order.
         *
         * @since 1.0
         * @return an iterator for all global attributes
         * @exception CatalogException error while retrieving the data from the server
         */
    Iterator getAttributes() throws CatalogException;

        /**
         * Returns an iterator for the global attributes of the catalog.
         * For local attributes of the category see {@link ICategory#getAttributes()}.
         * The instances of type {@link IAttribute}
         * are sorted by the given comparator.
         *
         * @since 1.0
         * @param aComparator the comparator for sorting the instances
         * @return iterator for all global attributes, sorted
         * @exception CatalogException error while retrieving the data from the server
         */
    Iterator getAttributes(Comparator aComparator) throws CatalogException;

        /**
         * Gets the query with the given name.
         * Provides a history of the already created and submitted queries for
         * the catalog. You should not rely on getting back a reference,
         * i.e. be prepared to reissue a  former statement if you can not find it
         * in the history.
         * The history of the queries will be clean up by an implementation
         * dependent  mechanism.
         *
         * @since 1.0
         * @see ICategory#getQuery(String aString)
         * @param aName the name of the requested query
         * @return an instance of type <code>IQuery</code> if found, otherwise <code>null</code>
         */
    IQuery getQuery(String aName);

        /**
         * Gets the cached queries.
         * Provides a history of the already created and submitted queries for
         * the catalog. You should not rely on getting back a reference, i.e. be prepared to reissue a
         * former statement if you can not find it in the history.
         * The history of the queries will be clean up by an implementation
         * dependent  mechanism.
         *
         * @since 2.1
         * @see ICategory#getQueries()
         * @return an instance of type <code>Iterator</code>
         */
    Iterator getQueries();
    
        /**
         * Gets an instance of an <code>IQueryStatement</code> object to formulate
         * a search statement. This statement has to be passed to the factory
         * method for a query {@link #createQuery(IQueryStatement aStatement)}
         * that can submit that statement to the catalog.
         *
         * @since 1.0
         * @see ICategory#createQueryStatement()
         * @exception CatalogException error while retrieving the data from the server
         * @return a new statement instance
         */
    IQueryStatement createQueryStatement() throws CatalogException;

        /**
         * Gets an instance of an <code>IQuery</code> object to formulate and submit
         * a query against the catalog. The necessary statement has to be created via
         * the {@link #createQueryStatement()} method. If a similar {basesd on the
         * comparison of the statement} query was already created the old instance
         * will be returned.
         *
         * @since 1.0
         * @see ICategory#createQuery(IQueryStatement aStatement)
         * @param  aStatement the statment of the query
         * @exception CatalogException error while retrieving the data from the server
         * @return a new query instance
         */
    IQuery createQuery(IQueryStatement aStatement) throws CatalogException;

        /**
         * Gets an instance of an <code>IQuery</code> object to formulate and submit
         * a query against the catalog. The necessary statement has to be created via
         * the {@link #createQueryStatement()} method. If a similar {basesd on the
         * comparison of the statement} query was already created the old instance
         * will be returned.
         *
         * @see ICategory#createQuery(IQueryStatement aStatement)
         * @param  aStatement the statement of the query
         * @param  cache boolean that indicates if the query should be cached
         * @exception CatalogException error while retrieving the data from the server
         * @return a new query instance
         */
    IQuery createQuery(IQueryStatement aStatement, boolean cache) throws CatalogException;

        /**
         * Adds a listener instance to the list of instances that are interested
         * in composite events.
         *
         * @param listener the listener instance to be added
         */
    void addCompositeEventListener(ICompositeEventListener listener);

        /**
         * Removes a listener instance from the list of instances that are interested
         * in composite events.
         *
         * @param listener the listener instance to be removed
         */
    void removeCompositeEventListener(ICompositeEventListener listener);

        /**
         * Get the set of known keys for attributes that carry specific product
         * information like price, productId, .....
         * Those keys can be used to find the mapped attribute guids via the
         * @link getAttributeGuid
         *
         * @return aSet of keys of type String
         */
    Set getAttributeKeys();

        /**
         * Get the guid for a global attribute of the catalog instance that
         * carries the information of a specific type (Price, ProductId,...) about
         * a product in the catalog.
         * For details how the information must be provided to the catalog API
         * @see <a href="../doc-files/ProductCatalogAPIDesign.html">Catalog API Design</a>
         *
         * @param   aKey    for a specific type of information about a product
         *                  maintained in the attributeMappings.xml file
         * @result  aGuid   for the concrete attribute guid that carries the
         *                  specific information in this catalog instance.
         *                  <code>null</code> if the key is not known.
         */
    String getAttributeGuid(String key);

    Locale getDefaultLocale();

    void setDefaultLocale(Locale aLocale) throws CatalogException ;

    Iterator getSupportedLocales();

    public final int CATALOG_STATUS_NOT_IMPL = 42;
    public final int CATALOG_STATUS_OK       = 43;
    public final int CATALOG_STATUS_ERROR    = 44;
    
        /**
         * Query the status of this catalog server.
         * 
         * @since 2.0
         * @return one of the constants <code>CATALOG_SERVER_STATUS_NOT_IMPL</code>,
         * <code>CATALOG_SERVER_STATUS_OK</code>,
         * or <code>CATALOG_SERVER_STATUS_ERR</code>
         */
    public int getCatalogStatus();


        /**
         * Returns the list of attribute names used for a QuickSearch
         *
         * @return  list of attribute names to search for in a QuickSearch
         */
    public String[] getQuickSearchAttributes();


        /**
         * @supplierCardinality 0..*
         * @label hasDetails
         * @link aggregationByValue
         */
        /*#IDetail lnkIDetail;*/

        /**
         * @link aggregation
         * @supplierCardinality 1..*
         * @label hasSupportedLocales
         * @directed 
         */
        /*#Locale lnkLocale;*/

    /**
     * Returns a ResultData of attribute descriptions
     *
     * @return ResultData of attribute descriptions
     */
    public ResultData getAttributeResKeys();

}
