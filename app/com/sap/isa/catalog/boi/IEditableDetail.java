/*****************************************************************************
  Copyright (c) 2000, SAPMarkets Europe GmbH, Germany, All rights reserved.
  Created:      01 August 2001

  $Id: //sap/ESALES_base/30_SP_COR/src/_pcatAPI/java/com/sap/isa/catalog/boi/IEditableDetail.java#2 $
  $Revision: #2 $
  $Change: 129221 $
  $DateTime: 2003/05/15 17:20:58 $
*****************************************************************************/
package com.sap.isa.catalog.boi;

/**
 * Extends the {@link IDetail IDetail} interface so that the detail can be
 * edited.
 *
 * @since       2.0
 * @version     1.0
 */
public interface IEditableDetail extends IDetail, IEditableComponent
{
    /**
     * Returns the referenc to the thing we are editing via this.
     * Might be null in case this is new.
     *
     * @return aRef to the object we are about to edit.
     */
     IDetail getDetailReference();

    /**
     * Sets the name of the detail.
     *
     * @since 2.0
     * @param aName the new name of the detail
     * @exception CatalogException error while setting the name
     */
    void setName(String aName) throws CatalogException;

    /**
     * Trys to set a string representation of the value.
     *
     * @since 2.0
     * @param aValue the new value of the detail
     * @exception CatalogException error while setting the value
     */
    void setAsString(String aValue) throws CatalogException;

    /**
     * Trys to add a string representation of the value.
     *
     * @since 2.0
     * @param aValue the new value of the detail
     * @exception CatalogException error while setting the value
     */
    void addAsString(String aValue) throws CatalogException;
}
