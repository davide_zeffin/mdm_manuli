/*****************************************************************************
  Copyright (c) 2000, SAPMarkets Europe GmbH, Germany, All rights reserved.

  $Id: //sap/ESALES_base/30_SP_COR/src/_pcatAPI/java/com/sap/isa/catalog/boi/CatalogPersistenceException.java#4 $
  $Revision: #4 $
  $Change: 144243 $
  $DateTime: 2003/08/22 10:13:24 $
*****************************************************************************/
package com.sap.isa.catalog.boi;

/**
 * CatalogPersistenceException.java
 *
 *
 * @version 1.0
 */

public class CatalogPersistenceException extends CatalogException 
{
    public CatalogPersistenceException(String msg)
    {
        super(msg);
    }

    public CatalogPersistenceException(String msg, Throwable aCause)
    {
        super(msg,aCause);
    }    
}
