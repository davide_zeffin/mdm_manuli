/*****************************************************************************
  Copyright (c) 2000, SAPMarkets Europe GmbH, Germany, All rights reserved.
  Created:      01 August 2001

  $Id: //sap/ESALES_base/30_SP_COR/src/_pcatAPI/java/com/sap/isa/catalog/boi/IEditableHierarchicalComponent.java#2 $
  $Revision: #2 $
  $Change: 129221 $
  $DateTime: 2003/05/15 17:20:58 $
*****************************************************************************/
package com.sap.isa.catalog.boi;

public interface IEditableHierarchicalComponent extends IEditableComponent
{
    /**
     * Lock the tree of the component this is editing.
     *
     * @return ther successe of the locking attempt
     */
    boolean lockTree();

    /**
     * Unlock the tree of the component this is editing.
     *
     */
    void releaseTree();
}
