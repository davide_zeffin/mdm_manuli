/*****************************************************************************
  Copyright (c) 2000, SAPMarkets Europe GmbH, Germany, All rights reserved.
  Created:      01 August 2001

  $Id: //sap/ESALES_base/30_SP_COR/src/_pcatAPI/java/com/sap/isa/catalog/boi/IEditableCatalog.java#2 $
  $Revision: #2 $
  $Change: 129221 $
  $DateTime: 2003/05/15 17:20:58 $
*****************************************************************************/
package com.sap.isa.catalog.boi;



/**
 * Extends the {@link ICatalog ICatalog} interface so that the catalog
 * can be edited.
 *
 * @since       2.0
 * @version     1.0
 */
public interface IEditableCatalog
    extends ICatalog,
            IEditableCompositeComponent
{
    /**
     * Release all locks ever grabed over this editor or one of its descendants!
     * Compare to {@link IEditableHierarchicalComponent#releaseTree} that works
     * only in the case you grabed the lock for the corresponding component
     * first.
     */
    void releaseAll();

    /**
     * Returns the user. The user that was used to create this editor!
     *
     * @return aUser The user that was used to create this editor!
     */
    IActor getUser();

    /**
     * Returns the reference to the thing we are editing via this
     *
     * @return aRef to the object we are about to edit.
     */
     ICatalog getCatalogReference();

    /**
     * Trys to undo the last change that was performed
     *
     * @return boolean that indicates success
     * @exception CatalogException error while undoing the last change
     */
    boolean undoLastChange() throws CatalogException;

    /**
     * Trys to redo the last undo that was performed
     *
     * @return boolean that indicates success
     * @exception CatalogException error while redoing the last undo
     */
    boolean redoLastUndo() throws CatalogException;

    /**
     * Sets the language dependent name of the catalog.
     *
     * @since 2.0
     * @param aName the new name of the catalog
     * @exception CatalogException error while setting the name
     */
    void setName(String aName) throws CatalogException;

    /**
     * Sets the language dependent long description of the catalog.
     *
     * @since 2.0
     * @param aDescription the new description of the catalog
     * @exception CatalogException error while setting the description
     */
    void setDescription(String aDescription) throws CatalogException;

    /**
     * Creates a new Detail with the given name.
     *
     * @since 2.0
     * @param aName the name of the new detail
     * @return the new detail instance
     * @exception CatalogException error while creating the detail
     */
    IEditableDetail createDetail(String aName) throws CatalogException;

    /**
     * Creates a new root category with the given guid.
     *
     * @since 2.0
     * @param aGuid the guid of the new root category
     * @return the new category instance
     * @exception CatalogException error while creating the category
     */
    IEditableCategory createChild(String aGuid) throws CatalogException;

    /**
     * Deletes a Detail instance.
     *
     * @since 2.0
     * @param aDetail the detail to be deleted
     * @exception CatalogException error while deleting the detail
     */
    void deleteDetail(IEditableDetail aDetail) throws CatalogException;

    /**
     * Deletes a root category instance.
     *
     * @since 2.0
     * @param aCategory the category to be deleted
     * @exception CatalogException error while deleting the category
     */
    void deleteChild(IEditableCategory aCategory) throws CatalogException;

    /**
     * Creates a new attribute with the given guid.
     *
     * @since 2.0
     * @param aGuid the guid of the new attribute
     * @return the new attribute instance
     * @exception CatalogException error while creating the attribute
     */
    IEditableAttribute createAttribute(String aGuid) throws CatalogException;

    /**
     * Deletes an attribute instance.
     *
     * @since 2.0
     * @param aAttribute the attribute to be deleted
     * @exception CatalogException error while deleting the attribute
     */
    void deleteAttribute(IEditableAttribute aAttribute) throws CatalogException;

    /**
     * Commit the changes made via this editor.
     *
     * @exception CatalogException error while commiting the changes
     */
     void commit() throws CatalogException;
}
