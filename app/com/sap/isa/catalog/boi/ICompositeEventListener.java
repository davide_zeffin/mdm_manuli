/*****************************************************************************
  Copyright (c) 2000, SAPMarkets Europe GmbH, Germany, All rights reserved.
  Created:      09 July 2001

  $Id: //sap/ESALES_base/30_SP_COR/src/_pcatAPI/java/com/sap/isa/catalog/boi/ICompositeEventListener.java#2 $
  $Revision: #2 $
  $Change: 129221 $
  $DateTime: 2003/05/15 17:20:58 $
*******************************************************************************/
package com.sap.isa.catalog.boi;

import java.util.EventListener;

/**
 * A listener interface for objects interested in composite specific events.
 *
 * @since       2.0
 * @version     1.0
 */
public interface ICompositeEventListener extends EventListener
{
    /**
     * A child was created. The listener is informed about the source
     * composite and the new child.
     *
     * @param aCreatedEvent an event that has the details
     */
    void childCreated(IChildCreatedEvent aCreatedEvent);

    /**
     * A child was deleted. The listener is informed about the source
     * composite and the deleted child.
     *
     * @param aCreatedEvent an event that has the details
     */
    void childDeleted(IChildDeletedEvent aDeletedEvent);

    /**
     * An attribute was created. The listener is informed about the source
     * composite and the new attribute.
     *
     * @param aCreatedEvent an event that has the details
     */
    void attributeCreated(IAttributeCreatedEvent aCreatedEvent);

    /**
     * An attribute was created. The listener is informed about the source
     * composite and the deleted attribute.
     *
     * @param aDeletedEvent an event that has the details
     */
    void attributeDeleted(IAttributeDeletedEvent aDeletedEvent);
}
