/*****************************************************************************
  Copyright (c) 2000, SAPMarkets Europe GmbH, Germany, All rights reserved.
  Created:      09 July 2001

  $Id: //sap/ESALES_base/30_SP_COR/src/_pcatAPI/java/com/sap/isa/catalog/boi/IChildDeletedEvent.java#2 $
  $Revision: #2 $
  $Change: 129221 $
  $DateTime: 2003/05/15 17:20:58 $
*******************************************************************************/
package com.sap.isa.catalog.boi;

/**
 * Event that is fired if a child (item/category) is deleted from a component.
 * {@see ICompositeEventListener}
 *
 * @since       2.0
 * @version     1.0
 */
public interface IChildDeletedEvent
{
    /**
     *
     * @return the parent of the deleted child
     */
    IComponent getComponent();

    /**
     *
     * @return the created child
     */
     IComponent getChild();
}