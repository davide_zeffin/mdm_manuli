/*****************************************************************************
  Copyright (c) 2000, SAPMarkets Europe GmbH, Germany, All rights reserved.
  Created:      29 March 2001

  $Id: //sap/ESALES_base/30_SP_COR/src/_pcatAPI/java/com/sap/isa/catalog/boi/IQuantity.java#2 $
  $Revision: #2 $
  $Change: 129221 $
  $DateTime: 2003/05/15 17:20:58 $
*****************************************************************************/
package com.sap.isa.catalog.boi;

import java.util.Iterator;

/**
 * Represents the quantity an {@link IAttribute attribute} might have.
 *
 * Currently for typification.
 *
 * @since       1.0
 * @version     2.0
 */
public interface IQuantity
{
    /**
     * Gets the language dependend name.
     *
     * @since   1.0
     * @return  the name of the quantity
     */
    String getName();

    /**
     * Gets the internal unique identifier for the quantity.
     *
     * @since   1.0
     * @return  the id of the quantity
     */
    String getGuid();

    /**
     * Gets the possible units the Quantity is measured in.
     *
     * @since   2.0
     * @return  the list of IUnit units of the quantity.
     */
    Iterator getUnits();

    /**
     * Gets the default unit the Quantity is measured in.
     *
     * @since   2.0
     * @return  the default unit of the quantity.
     */
    IUnit getDefaultUnit();

    /**
     * @supplierRole UnitsOfQuantity
     * @supplierCardinality 1..*
     * @link aggregation
     */
    /*#IUnit lnkIUnit;*/
}
