/*****************************************************************************
  Copyright (c) 2000, SAPMarkets Europe GmbH, Germany, All rights reserved.
  Created:      09 July 2001

  $Id: //sap/ESALES_base/30_SP_COR/src/_pcatAPI/java/com/sap/isa/catalog/boi/ComponentTypes.java#2 $
  $Revision: #2 $
  $Change: 129221 $
  $DateTime: 2003/05/15 17:20:58 $
*****************************************************************************/

package com.sap.isa.catalog.boi;

/**
 * This class defines the constants for the public types of the components
 * of the catalog tree. The constants have to be used if a concrete type is
 * requested from a component via <code>queryInterface</code> method
 * (Extension Interface pattern). <br>
 * This class obeys the "typesafe enum" idiom. Since also internal types are
 * possible the constructor has <code>protected</code> visibility.
 * Nevertheless never subclass this class outside the CatalogAPI otherwise
 * terrible effects will appear.<br>
 *
 *  @version     1.0
 */
public class ComponentTypes {

    protected final String theName;

    protected ComponentTypes(String name)
    {
        theName = name;
    }

    public String toString()
    {
        return this.theName;
    }

    public final static ComponentTypes I_ATTRIBUTE
        = new ComponentTypes("IAttribute");

    public final static ComponentTypes I_ATTRIBUTE_VALUE
        = new ComponentTypes("IAttributeValue");

    public final static ComponentTypes I_CATALOG
        = new ComponentTypes("ICatalog");

    public final static ComponentTypes I_DETAIL
        = new ComponentTypes("IDetail");

    public final static ComponentTypes I_CATEGORY
        = new ComponentTypes("ICategory");

    public final static ComponentTypes I_ITEM
        = new ComponentTypes("IItem");

    public final static ComponentTypes I_QUERY
        = new ComponentTypes("IQuery");

    public final static ComponentTypes I_QUERY_ITEM
        = new ComponentTypes("IQueryItem");

    public final static ComponentTypes I_QUERY_STATEMENT
        = new ComponentTypes("IQueryStatement");

    public final static ComponentTypes I_EDITABLE_ATTRIBUTE
        = new ComponentTypes("IEditableAttribute");

    public final static ComponentTypes I_EDITABLE_ATTRIBUTE_VALUE
        = new ComponentTypes("IEditableAttributeValue");

    public final static ComponentTypes I_EDITABLE_CATALOG
        = new ComponentTypes("IEditableCatalog");

    public final static ComponentTypes I_EDITABLE_CATEGORY
        = new ComponentTypes("IEditableCategory");

    public final static ComponentTypes I_EDITABLE_DETAIL
        = new ComponentTypes("IEditableDetail");

    public final static ComponentTypes I_EDITABLE_ITEM
        = new ComponentTypes("IEditableItem");

}