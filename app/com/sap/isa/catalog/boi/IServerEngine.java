/*****************************************************************************
  Copyright (c) 2000, SAPMarkets Europe GmbH, Germany, All rights reserved.
  Created:      29 March 2001

  $Id: //sap/ESALES_base/30_SP_COR/src/_pcatAPI/java/com/sap/isa/catalog/boi/IServerEngine.java#2 $
  $Revision: #2 $
  $Change: 129221 $
  $DateTime: 2003/05/15 17:20:58 $
*****************************************************************************/
package com.sap.isa.catalog.boi;

import java.net.URL;
import java.util.Iterator;

import javax.xml.transform.Source;

import org.xml.sax.InputSource;

import com.sap.isa.core.eai.BackendBusinessObject;
import com.sap.isa.core.util.table.ResultData;

/**
 * IServerEngine.java
 *
 * Represents a catalog engine/sever.
 * Instances of that type will be returned by the 
 * {@link ISite#getCatalogServers()} call. It allows to execute 
 * administrative actions and query administrative information about a catalog
 * engine/sever.
 * It extends {@link IServer} and effectivly replaces it.
 *
 * @since 2.0
 * @version 1.0
 */

public interface IServerEngine extends IServer, BackendBusinessObject {
	
	/**
	 * Constants to define staging support
	 */
	public static String STAGING_NOT_SUPPORTED = "A";
	public static String STAGING_SUPPORTED = "B";
	public static String STAGING_DISABLED_DEFERRED_OVERWITE = "C";
	public static String STAGING_DISABLED_DIRECT_OVERWITE = "D";
	
	/**
	 * Constants, to define staging catalogue type
	 */
	public static String STAGING_TYPE_INACTIVE = "A";
	public static String STAGING_TYPE_ACTIVE = "B";
	public static String STAGING_TYPE_OBSOLETE = "C";
	public static String STAGING_TYPE_INDEXING = "D";
    public static String STAGING_TYPE_ERROR = "E";
    
        /**
         * Get the catalogs known on that server. Potentittialy heavy weight call.
         * The list contains only the catalogs accessible to the actor this instance was
         * created with.
         *
         * @return an <code>Iterator</code> over the {@link ICatalog}s known on that 
         * server. 
         */
    Iterator getCatalogs()
        throws CatalogException;

        /**
         * Get the catalog infos known on that server. Light weight call.
         *
         * @return an <code>Iterator</code> over the {@link ICatalogInfo}s known on that 
         * server.
         */
    Iterator getCatalogInfos()
        throws CatalogException;
    
        /**
         * Set the guid of the default catalog.
         *
         * @exception ICatalogException thrown if an invalid guid is used.
         */
    void setCatalogGuid(String aCatalogGuid) throws CatalogException;

        /**
         * Try to backup the catalog defined by the guid. If the catalog is not
         *  known by the server or locally or in the back-end (depending on the
         *  backup strategy) an error occurs a exception of type or subtype
         *  CatalogException is thrown.
         *  Otherwise (again depending on the strategy) it can be assumed that
         *  the action was successful. in case of sucess a url will be returned.
         *  That allows the download of the backup.
         *   
         * @param aCatalogGuid a <code>String</code> value
         * @return an <code>URL</code> value for download.
         * @exception CatalogException if an error occurs
         */
    URL backUpCatalog(String aCatalogGuid)
        throws CatalogException ;

        /**
         * Loads the backup of a catalog to this instance.
         *
         * @param theBackUPURL an <code>URL</code> value
         * @param theFromType a <code>ServerType</code> value
         * @exception CatalogException if an error occurs
         */
    void loadCatalogBackUp(URL theBackUPURL, ServerType theFromType)
        throws CatalogException;
        
        /**
         * Deleted a catalog owned by that catalog sever. If the catalog is not owned 
         * by that server an exception is thrown. If you want to be informed when 
         * the submitted task was completed successfully, register as an  
         * {@link IServerEventlistener} to the catalog. Be aware that the call is 
         * non blocking. So the catalog might be completely sane till the executing 
         * thread gets the log on it and removes it. In case of a problem 
         * an exception will thrown. Be aware that after that aDomedCatalog is a 
         * stale reference.
         *
         * @param aDomedCatalog an <code>ICatalog</code> value
         * @exception CatalogException if an error occurs, e.g the catalog is not 
         *   owned by the server
         */
    void deleteCatalog(ICatalog aDomedCatalog) throws CatalogException;
    
    void deleteCatalog(ICatalog aDomedCatalog, boolean includeAllCacheImages)
        throws CatalogException;

        /**
         * Deleted a catalog owned by that catalog sever. If the catalog is not owned 
         * by that server an exception is thrown. If you want to be informed when 
         * the submitted task was completed successfully, register as an  
         * {@link IServerEventlistener} to the catalog. Be aware that the call is 
         * non blocking. So the catalog might be completely sane till the executing 
         * thread gets the log on it and removes it. In case of a problem 
         * an exception will thrown. Be aware that after that aDomedCatalog is a 
         * stale reference.
         *
         * @param aDomedCatalog an <code>ICatalog</code> value
         * @exception CatalogException if an error occurs, e.g the catalog is not 
         *   owned by the server
         */
    void deleteCatalog(ICatalog aDomedCatalog, ICatalogTask.Schedule aSchedule)
        throws CatalogException;
    
    void deleteCatalog(
        ICatalog aDomedCatalog,
        boolean includeAllCacheImages,
        ICatalogTask.Schedule aSchedule)
        throws CatalogException;
    
        /**
         * Deleted all catalogs owned by that catalog sever. if You want to be informed when 
         * the submitted task was completed successfully, register as an  
         * {@link IServerEventlistener} to the server. Be aware that the call is 
         * non blocking. So the catalogs might be completely sane till the executing 
         * thread gets the log on it and removes them. In case of a problem 
         * an exception will thrown. Be aware that after that all catalogs are stale references.
         *
         * @exception CatalogException if an error occursy
         * */
    void deleteCatalogs() throws CatalogException;

    void deleteCatalogs(boolean includeAllCacheImages) throws CatalogException;

        /**
         * Update a catalog owned by that catalog sever with another catalog. 
         * If the catalog to be updated is not owned  by that server an exception is 
         * thrown. If you want to be informed when  the submitted task was completed 
         * successfully, register as an  {@link IComponentEventListener} to the catalog. 
         * Be aware that the call is  non blocking. So the catalog will be unchanged 
         * till the executing thread gets the log on it and updates it. In case of a 
         * problem an exception will thrown. Be aware that storing references to 
         * <code>toCatalog</code> will post-phone that event to infinity. 
         *(You lock yourself!)
         *
         * Describe <code>updateCatalog</code> method here.
         *
         * @param fromCatalog an <code>ICatalog</code> value
         * @param toCatalog an <code>ICatalog</code> value
         * @exception CatalogException if an error occurs
         */
    void updateCatalog(ICatalog fromCatalog, ICatalog toCatalog) throws CatalogException;

        /**
         * Update a catalog owned by that catalog sever with another catalog. 
         * If the catalog to be updated is not owned  by that server an exception is 
         * thrown. If you want to be informed when  the submitted task was completed 
         * successfully, register as an  {@link IComponentEventListener} to the catalog. 
         * Be aware that the call is  non blocking. So the catalog will be unchanged 
         * till the executing thread gets the log on it and updates it. In case of a 
         * problem an exception will thrown. Be aware that storing references to 
         * <code>toCatalog</code> will post-phone that event to infinity. 
         *(You lock yourself!)
         *
         * Describe <code>updateCatalog</code> method here.
         *
         * @param fromCatalog an <code>ICatalog</code> value
         * @param toCatalog an <code>ICatalog</code> value
         * @exception CatalogException if an error occurs
         */
    void updateCatalog(ICatalog fromCatalog, ICatalog toCatalog, ICatalogTask.Schedule aSchedule) throws CatalogException;

        /**
         * Add a catalog document to this sever. The document should be valid
         * document against catalog.dtd. In case the catalog server has the concept
         * of fixed attribute names you have to provide a mapping document.
         * If you want to be informed when  the submitted task was completed 
         * successfully, register as an  {@link IComponentEventListener} to the
         * server.
         * Be aware that the call is non blocking. So the catalog might not appear on
         * this server till you receive your notification. In case of a problem an
         * exception will thrown.
         *
         * @param anOtherCatalog given as <code>InputSource</code> 
         * @param aMappingDocument an <code>InputSource</code> value
         * @exception CatalogException if an error occurs
         */
    void addCatalog(InputSource anOtherCatalog, Source aMappingDocument)
        throws CatalogException;

        /**
         * Add a catalog document to this sever. The document should be valid
         * document against catalog.dtd. In case the catalog server has the concept
         * of fixed attribute names you have to provide a mapping document.
         * If you want to be informed when  the submitted task was completed 
         * successfully, register as an  {@link IComponentEventListener} to the
         * server.
         * Be aware that the call is non blocking. So the catalog might not appear on
         * this server till you receive your notification. In case of a problem an
         * exception will thrown.
         *
         * @param anOtherCatalog given as <code>InputSource</code>
         * @param aMappingDocument an <code>InputSource</code> value
         * @param aSchedule an <code>ICatalogTask.Schedule</code> value
         * @exception CatalogException if an error occurs
         */
    void addCatalog(
        InputSource anOtherCatalog,
        Source aMappingDocument,
        ICatalogTask.Schedule aSchedule) throws CatalogException;

        /**
         * Add a catalog owned by another catalog server to this sever. 
         * If the catalog to be added is owned by that server an exception is 
         * thrown. Use {@link #addCatalog(InputSource, Source)} with an
         * empty catalog xml file if you want  to create a new catalog on
         * this server.
         * If you want to be informed when the submitted task was completed
         * successfully, register as an  {@link IComponentEventListener} to the
         * server.
         * Be aware that the call is non blocking. So the catalog might not appear
         * on this server till you receive your notification. In case of a problem
         * an exception will thrown.
         *
         * @param anOtherCatalog an <code>ICatalog</code> value
         * @exception CatalogException if an error occurs
         */
    void addCatalog(ICatalog anOtherCatalog) throws CatalogException;

        /**
         * Add a catalog owned by another catalog server to this sever. 
         * If the catalog to be added is owned by that server an exception is 
         * thrown. Use {@link #addCatalog(InputSource, Source)} with an
         * empty catalog xml file if you want  to create a new catalog on
         * this server.
         * If you want to be informed when the submitted task was completed
         * successfully, register as an  {@link IComponentEventListener} to the
         * server.
         * Be aware that the call is non blocking. So the catalog might not appear
         * on this server till you receive your notification. In case of a problem
         * an exception will thrown.
         *
         * @param anOtherCatalog an <code>ICatalog</code> value
         * @exception CatalogException if an error occurs
         */
    public void addCatalog(
        ICatalog anOtherCatalog,
        ICatalogTask.Schedule aSchedule)
        throws CatalogException;
    
    void addServerEventListener(IServerEventlistener aListener);

    void removeServerEventListener(IServerEventlistener aListerner);

    boolean hasServerFeature(ServerFeature aPossibleFeature);

    ServerFeature[] getServerFeatures();

    ICatalog getCatalog(String aGuid, IClient aClient) throws CatalogException ;

    ICatalog getCatalog(String aGuid) throws CatalogException ;

        /**
         * @since 2.0
         * @return anIterator over the ITasks instances found
         * @exception if the supplied user has not the rights to query tasks an exception is thrown 
         */
    Iterator getTasks() throws CatalogException ;
    
    String getName();

    ServerType getType();

    String getDescription();
    
    /**
     * This method will return a list of information
     * for the available staging catalogues
     *
     * @return list of information for the available staging catalogues
     */
    public ResultData getInactiveCatalogInfos(String theCatalogGuid, String stagingType, IClient aUser, String country);
    
    /**
     * This method determines, what kind of catalogue staging is supported
     *
     * @return String kind of staging support
     */
    public String getStagingSupport();

    static class ServerFeature
    {
        private String theName;

        private ServerFeature(String theName)
        {
            this.theName=theName;
        }

        public static ServerFeature ALLOWS_USER_ADMINSITRATION =
        new ServerFeature("allows_user_administration");

		public static ServerFeature ALLOWS_MULTIPLE_LOCALES_PER_CATALOG =
        new ServerFeature("allows_multiple_locales_per_catalog");
    }


        /**
         * @link aggregation
         * @clientCardinality 1
         * @supplierCardinality 0..*
         * @label hasCatalogs
         * @directed 
         */
        /*#ICatalog lnkICatalog;*/

        /**
         * @link aggregation
         * @supplierCardinality 0..*
         * @clientCardinality 1
         * @label maintainsTasks
         * @directed 
         */
        /*#ICatalogTask lnkICatalogTask;*/

        /**
         * @label usesAnActor
         * @supplierCardinality 1
         * @directed 
         */
        /*#IActor lnkIActor;*/

    static class ServerType
    {
        private String theType;

        private ServerType(String aType)
        {
            theType = aType;
        }

        public String getType()
        {
            return theType;
        }

        public String toString()
        {
            return theType;
        }
        
        public static final ServerType TREX 		= new ServerType("trex");

        public static final ServerType REQUISITE 	= new ServerType("requisite");

        public static final ServerType CRM 			= new ServerType("crm");

        public static final ServerType COMMERCEONE 	= new ServerType("commerceone");

        public static final ServerType R3	 		= new ServerType("r3");
    }
}