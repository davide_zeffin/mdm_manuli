/*****************************************************************************
  Copyright (c) 2000, SAPMarkets Europe GmbH, Germany, All rights reserved.
  Created:      29 March 2001

  $Id: //sap/ESALES_base/30_SP_COR/src/_pcatAPI/java/com/sap/isa/catalog/boi/ICatalogInfo.java#2 $
  $Revision: #2 $
  $Change: 129221 $
  $DateTime: 2003/05/15 17:20:58 $
*****************************************************************************/
package com.sap.isa.catalog.boi;

import java.util.Properties;


/**
 * Interface which contains informations about a catalog.
 *
 * @version     1.0
 * @since       1.0
 */
public interface ICatalogInfo
{

        /**
         * Returns the unique identifier of the catalog.
         *
         * @return GUID of the catalog
         */
    String getGuid();
    
        /**
         * Returns the name of the catalog.
         *
         * @return name of the catalog
         */
    String getName();

        /**
         * Get short description of the catalog that is referenced by this.
         *
         * @return a <code>String</code> value
         */
    String getDescription();

        /**
         * Get a list of properties characteristic for that catalog referenced by this.
         * Varies with the implementation.
         *
         * Be aware that these instances might contain values that are them-self
         * are instances of Type Properties. So Properties are only used as convenience
         * Hashtable impl.
         * The warnings in the JDK  apply.
         * 
         * @return a <code>Properties</code> value
         */
    Properties getProperties();

        /**
         * Get a specific property as String. The property is characteristic for that catalog
         * referenced by this. 
         * Varies with the implementation.
         *
         * @return a <code>String</code> value
         */
    String getProperty(String aPropertyName);    
    
}
