/*****************************************************************************
  Copyright (c) 2000, SAPMarkets Europe GmbH, Germany, All rights reserved.
  Created:      29 March 2001

  $Id: //sap/ESALES_base/30_SP_COR/src/_pcatAPI/java/com/sap/isa/catalog/boi/IItem.java#3 $
  $Revision: #3 $
  $Change: 150872 $
  $DateTime: 2003/09/29 09:18:51 $
*****************************************************************************/
package com.sap.isa.catalog.boi;

import java.util.Comparator;
import java.util.Iterator;

/**
 * General Interface of an item in a catalog.
 *
 * An Item is something that can be ordered and that is unique in the catalog.
 * It is a standIn for a specific product/service in the context of a category.
 * If you submit queries to a catalog that contains items in its result set
 * you should not rely on the assumption that you get back pointers to items
 * already existing in a category.
 *
 * @since       1.0
 * @version     2.0
 */
public interface IItem extends IComponent
{
    /**
     * Gets the display (language dependent) name of the item.
     *
     * @since   1.0
     * @return  the name of the item
     */
    String getName();

    /**
     * Gets unique identifier for the item.
     *
     * @since   1.0
     * @return the guid of the item
     */
    String getGuid();

    /**
     * Gets unique identifier for the product the item represents. The same
     * product might appear in the same catalog in different categories.
     *
     * @since   1.0
     * @return  the product guid
     */
    String getProductGuid();

    /**
     * Returns an iterator for all attribute values of the item. The instances
     * will have the type {@link IAttributeValue} and are sorted by the default
     * sort order.
     * The list will be the union of local i.e. category specific
     * and global i.e. catalog specific attributes.
     *
     * @since   1.0
     * @return  iterator for the values of the attributes
     */
    Iterator getAttributeValues();

    /**
     * Returns an iterator for all attributevalues of the item. The instances
     * will have the type {@link IAttributeValue} and are sorted by the given
     * comparator.
     *
     * The list will be the union of local i.e. category specific
     * and global i.e. catalog specific attributes.
     *
     * @since   1.0
     * @param   aComparator the comparator for sorting the instances
     * @return  iterator for the values of the attributes
     */
    Iterator getAttributeValues(Comparator aComparator);

    /**
     * Trys to gets an attributeValue by its unique identifier.
     * If not found <code>null</code> will be returned.
     *
     * @since   1.0
     * @param   aAttributeGuid the name of the requested attribute
     * @return  the value of the attribute
     */
    IAttributeValue getAttributeValue(String aAttributeGuid);

    /**
     * Gets the category that is the parent of this Item.
     *
     * @since   1.0
     * @return the parent of the item typed as <code>ICategory</code>
     */
     ICategory getParent();
     
    /**
     * Checks if the item is an accessory.<br> 
     * 
     * This is the case, if the attribute IS_SUBCOMP_OF_ACCESS is set to "X".
     * 
     * @return true, if the item is an accessory.<br>
     *         false, otherwise
     */
    public boolean isAccessory();
    
    /**
     * Checks, if the item is classified as a Combined Rate Plan.
     * This is the case, if the attribute PRODUCT_ROLE is set to "C".
     * 
     * @return  true, if the item is a Combined Rate Plan 
     *          false, otherwise.
     */
    public boolean isCombinedRatePlan();
    
    /**
     * Checks, if the item is classified as a Rate Plan (Tariff).
     * This is the case, if the attribute PRODUCT_ROLE is set to "R".
     * 
     * @return  true, if the item is a Rate Plan 
     *          false, otherwise.
     */
    public boolean isRatePlan();
    
    /**
     * Checks if the item is relevant for explosion with the Solution Configurator.<br> 
     * 
     * This is the case, if the attribute IS_SOL_CONF_EXPLO is set to "X".
     * 
     * @return true, if the item is relevant for explosion.<br>
     *         false, otherwise
     */
    public boolean isRelevantForExplosion();
    
    /**
     * Checks, if the item is classified as a Sales Package.
     * This is the case, if the attribute PRODUCT_ROLE is set to "S".
     * 
     * @return  true, if the item is a Sales Package
     *          false, otherwise.
     */
    public boolean isSalesPackage();
    
    /**
     * Checks, if the item has accessories.
     * 
     * @return  true, if the item has accessories,
     *          false, otherwise.
     */
    public boolean isItemWithAccessories();
    
    /**
     * Checks if the item is just a main item.<br> 
     * 
     * Returns true if an item is directly part of an area (e.g would be false if only be 
     * part of an area because of accessory relations)     
     *   
     * @return true, if the item is a main item.<br>
     *         false, otherwise
     */
    public boolean isMainItem();
    
    /**
     * Checks if the item is a sub component.
     */
    public boolean isSubComponent();

    /**
     * Adds a listener instance to the list of instances that are interested
     * in item events.
     *
     * @param listener the listener instance to be added
     */
    public void addItemEventListener(IItemEventListener listener);

    /**
     * Removes a listener instance from the list of instances that are interested
     * in item events.
     *
     * @param listener the listener instance to be removed
     */
    public void removeItemEventListener(IItemEventListener listener);

}
