/*****************************************************************************
  Copyright (c) 2000, SAPMarkets Europe GmbH, Germany, All rights reserved.

  $Id: //sap/ESALES_base/30_SP_COR/src/_pcatAPI/java/com/sap/isa/catalog/boi/CatalogAuthorizationException.java#3 $
  $Revision: #3 $
  $Change: 144238 $
  $DateTime: 2003/08/22 09:44:22 $
*****************************************************************************/
package com.sap.isa.catalog.boi;

/**
 * CatalogAuthorizationException.java
 *
 *
 * @version 1.0
 */

public class CatalogAuthorizationException extends CatalogException 
{
    public CatalogAuthorizationException(String msg)
    {
        super(msg);
    }

    public CatalogAuthorizationException(String msg, Throwable aCause)
    {
        super(msg,aCause);
    }    
}// CatalogAuthorizationException
