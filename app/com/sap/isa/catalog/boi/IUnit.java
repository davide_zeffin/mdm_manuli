/*****************************************************************************
  Copyright (c) 2000, SAPMarkets Europe GmbH, Germany, All rights reserved.
  Created:      29 March 2001

  $Id: //sap/ESALES_base/30_SP_COR/src/_pcatAPI/java/com/sap/isa/catalog/boi/IUnit.java#2 $
  $Revision: #2 $
  $Change: 129221 $
  $DateTime: 2003/05/15 17:20:58 $
*****************************************************************************/
package com.sap.isa.catalog.boi;

/**
 * The unit the {@link IQuantity quantity} is measured in like: gramm, dozend
 *
 * @since       1.0
 * @version     2.0
 */
public interface IUnit
{
    /**
     * Converts one commonly used unit in to another. Returns <code>null</code>
     * if the conversion fails.
     *
     * @since   2.0
     * @param   aValue the value to be converted
     * @param   old the old unit
     * @return  the converted value
     */
    Number convert(Number aValue, IUnit convertTo);

    /**
     * Gets the language dependent name.
     *
     * @since   1.0
     * @return  the name of the unit
     */
    String getName();

    /**
     * Gets the unique internla identifier of the unit.
     *
     * @since   1.0
     * @return  the id of the unit
     */
    String getGuid();
}
