/*****************************************************************************

    Class:        WebCatSubItemListData
    Copyright (c) 2006, SAP AG, Germany, All rights reserved.
    Created:      03.02.2006

*****************************************************************************/
package com.sap.isa.catalog.boi;

import com.sap.isa.catalog.webcatalog.ItemGroup;
import com.sap.isa.catalog.webcatalog.WebCatSubItem;
import com.sap.isa.core.TechKey;

/**
 * Provides the methods to access the <code>WebCatSubItemList</code>.<br>
 * 
 * By this interface, backend classes gain access to the <code>WebCatSubItemList</code> object.
 */
public interface WebCatSubItemListData extends WebCatItemListData {

    /**
     * Fills the <code>WebCatSubItemList</code> with new <code>WebCatSubItem</code>s for the 
     * given <code>productID[]</code> in the given order.<br>
     * 
     * If the same product is contained more than once in the input list, it will occur more than 
     * once in the populated <code>WebCatSubItemList</code>. Thus after the method is executed, 
     * the item list of the object holds a <code>WebCatSubItem</code> for every productID in the
     * same sequence as in the <code>productID[]</code> input parameter.<br>
     * 
     * If there are problems with a single productID, the return value is set to false and an
     * empty <code>WebCatSubItem</code> is inserted in the <code>WebCatItemList</code>.
     *  
     * @param productID array with ProductIDs
     * @param areaID    Identifier of the area
     * 
     * @return true  if the population could be completed successfully, this means for every given 
     *               productID, a WebCatSubItem could be created.
     *         false otherwise.
     */
    public boolean populateWithProducts(String[] productID, String areaID);

    /**
     * Returns the <code>WebCatItem</code> entry for a given index in the <code>
     * WebCatItemList</code>.
     *  
     * @return the <code>WebCatItem</code> for the given index.
     */
    public WebCatSubItemData getSubItemData(int index);

    /**
     * Returns the <code>WebCatSubItemData</code> with the given <code>techKey</code>.
     * 
     * @param techKey of the item to be returned.
     * @return WebCatSubItemData object of the item
     */
    public WebCatSubItemData getSubItemData(TechKey techKey);

    /**
     * Returns the number of entries in the <code>WebCatSubItemList</code>.
     * 
     * @return the number of WebCatSubItems
     */
    public int size();

    /**
     * Clears all group infos that are stored in this <code>WebCatSubItemList</code>.
     */
    public void clearItemGroups();
    
    /**
     * Cereates and returns a new itemGroupData object<br>
     * 
     * @param groupId the groupId
     * @param groupText teh group text
     * 
     * @retrun ItemGroupData the ItemGroupData oject
     */
    public ItemGroupData createItemGroupData(String groupId, String groupText);
    
    /**
     * Returns the itemGroupData for a given groupId.
     * 
     * @param groupId 
     * 
     * @return text for the given groupId, null if there is no text.
     */
    public ItemGroupData getItemGroupData(String groupId);

    /**
     * Returns the group text for a given groupId.
     * 
     * @param groupId 
     * 
     * @return text for the given groupId, null if there is no text.
     */
    public String getItemGroupText(String groupId);
    
    /**
     * This method has to be called, to rebuidl the TechKeyMap, in case itesm from the
     * list have been removed via another method than removeSubItem(int) (e.h via an 
     * remove on an iterator) 
     * Than the TechKeyMap is created newly from all entries in the list.
     */
    public void rebuildTechKeyMap();
    
    /**
     * Removes the entry for a given groupId.
     * 
     * @param groupId of the entry to be deleted.
     */
    public void removeItemGroupData(String groupId);
    
    /**
     * Sets the group info for a given itemGroup and groupId.<br>
     * 
     * @param itemGroupData the itemGroupData oject
     */
    public void setItemGroupData(ItemGroupData itemGroup);
    
    /**
     * Changes the <code>TechKey</code> of a <code>WebCatSubItem</code> that is a member of
     * this <code>WebCatSubItemList</code>.
     * 
     * The <code>techKey</code> as well as the <code>itemTeckKeyMap</code> is updated.
     * 
     * @param position of the <code>WebCatSubItem</code> in the <code>items</code> array
     * @param techKey the new <code>TechKey</code> of the <code>WebCatSubItem</code>
     * 
     * @return the <code>WebCatSubItem</code> that has been changed.
     */
    public WebCatSubItem setSubItemTechKey(int position, TechKey techKey);
    
}
