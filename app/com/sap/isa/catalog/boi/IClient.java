/*****************************************************************************
  Copyright (c) 2000, SAPMarkets Europe GmbH, Germany, All rights reserved.
  Created:      29 March 2001

  $Id: //sap/ESALES_base/30_SP_COR/src/_pcatAPI/java/com/sap/isa/catalog/boi/IClient.java#3 $
  $Revision: #3 $
  $Change: 150872 $
  $DateTime: 2003/09/29 09:18:51 $
*****************************************************************************/
package com.sap.isa.catalog.boi;

// misc imports
import java.util.ArrayList;
import java.util.Locale;

import com.sap.isa.core.TechKey;

/**
 * This interface encapsulates all properties a catalog server has to know about
 * a given client (an accual user of the catalog not an administrator) 
 * to facilitate authentication/authorization.
 *
 * @since       1.0
 * @version     2.0
 */
public interface IClient extends IActor {
    
    /**
     * States of the catalog to be read in case catalogue staging is supported by the Backend
     * 
     * Right now this is only true for IMS Catalogues
     */
    public static final int CATALOG_STATE_UNDEFINED = 0;
    public static final int CATALOG_STATE_ACTIVE = 1;
    public static final int CATALOG_STATE_INACTIVE = 2;
    
    /**
     * The locale of the client. The server should return the catalog content
     * in a form that is compatible to that locale. That means that the catalog
     * user might expect language dependent texts in the corresponding
     * language and correct locale specific formating for locale sensitive
     * objects like Date.. etc..
     *
     * @since   1.0
     * @return the locale of the client
     */
    Locale getLocale();

    /**
     * Several catalog engines support the concept of a view to a catalog as
     * subset of a catlog. A given client might only be authorized to see a
     * given set of views in a given catalog. That method should provide that
     * information in case the catalog does not store the relation of the views
     * to the clients. Thats a breatch in a authorization concept but it is
     * enforced by the imperfections of the existing catalog engines.
     *
     * @since   1.0
     * @return the array of {@link IView} instances views that belong to the
     * client
     */
    ArrayList getViews();

    /**
     * Returns the catalog status
     *
     * @return int catalogStatus the status of the catalogue to be read
     */
    public int getCatalogStatus();
    
    /**
     * Returns the TechKey of the staging catalogue to read
     *
     * @return TechKey stagingCatalogKey the TechKey of the staging catalogue to read
     */
    public TechKey getStagingCatalogKey();
    
    /**
     * Sets the catalog status
     *
     * @param catalogStatus catalogStatus the status of the catalogue to be read
     */
    public void setCatalogStatus(int catalogStatus);
    
    /**
     * Sets the TechKey of the staging catalogue to read
     *
     * @param stagingCatalogKey stagingCatalogKey the id of the staging catalogue to read!
     */
    public void setStagingCatalogKey(TechKey stagingCatalogKey);
}