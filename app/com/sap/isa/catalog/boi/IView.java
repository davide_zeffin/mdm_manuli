/*****************************************************************************
  Copyright (c) 2000, SAPMarkets Europe GmbH, Germany, All rights reserved.
  Created:      29 March 2001

  $Id: //sap/ESALES_base/30_SP_COR/src/_pcatAPI/java/com/sap/isa/catalog/boi/IView.java#2 $
  $Revision: #2 $
  $Change: 129221 $
  $DateTime: 2003/05/15 17:20:58 $
*****************************************************************************/
package com.sap.isa.catalog.boi;

/**
 * A view defines a subset of a catalog.
 *
 * Different {@link IClient clients} will typically see different views of a
 * catalog.
 *
 * @since       1.0
 * @version     2.0
 */
public interface IView
{
    /**
     * Gets a unique identifier for a view within a given catalog.
     *
     * @since   1.0
     * @return  the guid of the view
     */
    String getGuid();
}
