/*****************************************************************************

    Class:        WebCatItemListData
    Copyright (c) 2006, SAP AG, Germany, All rights reserved.
    Created:      03.02.2006

*****************************************************************************/
package com.sap.isa.catalog.boi;

import java.util.Iterator;

/**
 * The data interface for <code>WebCatItemList</code>.<br>
 * 
 * By this interface, backend classes gain access to the <code>WebCatItemList</code> object.
 */
public interface WebCatItemListData {

    /**
     * Returns the <code>WebCatItemData</code> entry for a given index in the <code>
     * WebCatItemList</code>.
     *  
     * @return the <code>WebCatItemData</code> for the given index.
     */
    public WebCatItemData getItemData(int index);

    /**
     * Returns the number of entries in the <code>WebCatItemListData</code>.
     * 
     * @return the number of WebCatItems
     */
    public int size();

    /**
     * Get iterator for itemlist.
     * 
     * This method returns an iterator for all items in the item list, no matter
     * whether they have been populated before. Therefore it calls
     * <code>populate(-1)</code> first to ensure that all items of the list are
     * populated. For performance it might be better to call
     * <code>iteratorOnlyPopulated()</code> instead.
     * 
     * @return iterator iterator of item list
     */
    public Iterator iterator();
}
