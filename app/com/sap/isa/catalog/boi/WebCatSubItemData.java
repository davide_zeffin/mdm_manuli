/*****************************************************************************

    Class:        WebCatSubItemData
    Copyright (c) 2006, SAP AG, Germany, All rights reserved.
    Created:      06.02.2006

*****************************************************************************/
package com.sap.isa.catalog.boi;

import com.sap.isa.catalog.webcatalog.WebCatItem;

/**
 *  Data interface for the <code>WebCatSubItem</code>.<br>
 * 
 *  By this interface, backend classes gain access to the WebCatSubItem object.
 */
public interface WebCatSubItemData extends WebCatItemData {

    /**
     * Constant to define that the item is selected for SC explosion
     */
    public final static String SELECTED_FOR_EXP = "S";

    /**
     * Constant to define that the item is deselected for SC explosion,
     * that means, it was explicitly unchoosen by the user
     */
    public final static String DESELECTED_FOR_EXP = "D";
    
    /**
     * Constant to define that the all items of a group have been deselected for 
     * SC explosion,
     */
    public final static String GROUP_DESELECTED_FOR_EXP = "G";

    /**
     * Constant to define that the item is not selected for SC explosion.
     * If it was once selected and than gets deselected its selection status
     * will be DESELECTED_FOR_RES.
     */
    public final static String NOT_SELECTED_FOR_EXP = "";
    
    /**
     * Constant to define that no Soultion Configurator item type is set
     */
    public final static String SC_ITEM_TYPE_UNDEFINED = "";
    
    /**
     * Constant to define that the Soultion Configurator item type is 
     * Sales Component
     */
    public final static String SC_ITEM_TYPE_SALES_COMPONENT = "S";

    /**
     * Constant to define that the Soultion Configurator item type is 
     * Dependent Component, also known as Process Product
     */
    public final static String SC_ITEM_TYPE_DEPENDENT_COMPONENT = "P";

    /**
     * Constant to define that the Soultion Configurator item type is 
     * Combined Rate Plan Component
     */
    public final static String SC_ITEM_TYPE_COMB_RATE_PLAN_COMPONENT = "C";    

    /**
     * Returns always <code>SolutionConfiguratorData.NOT_ELIGIBLE</code>, because
     * the <code>WebCatSubItem</code> will not be exploded.
     * 
     * @return always <code>SolutionConfiguratorData.NOT_ELIGIBLE</code>
     */
    public int explode();

    /**
     * Returns product description.
     *
     * @return description from catalog, as <code>String</code>
     */
    public String getDescription();

    /**
     * Returns the group key.
     * 
     * @return group key as <code>String</code>
     */
    public String getGroupKey();

    /**
     * Returns the parent item of this <code>WebCatSubItem</code>.
     * 
     * @return parent item
     */
    public WebCatItemData getParentItemData();

    /**
     * Returns name of product.
     *
     * @return identifier of the product which is included in this item; usually a speaking 
     *         abbrevation or some product number, in opposite to getProductID
     */
    public String getProduct();
    
    /**
     * Returns the Solution Configurator item type
     * 
     * @return String the Solution configurator item type
     */
    public String getScItemType();

    /**
     * Returns the selection status of this <code>WebCatSubItem</code>.<p>
     * 
     * The selection status controls, which items are transferred to the solution 
     * configurator explosion. The solution configurator needs to know, which items are
     * currently selected, but also which items have been deselected.<br>
     * 
     * The method returns one of the following values: {@link #SELECTED_FOR_RES}, 
     * {@link #DESELECTED_FOR_RES} or {@link #NOT_SELECTED_FOR_RES}.
     * 
     * @return the current selection status for this item as <code>String</code>
     * 
     * @see     #setAuthor(String)
     */
    public String getAuthor();

    /**
     * Returns the top item in the parentItem hierarchy for the current item.
     * This means, if the parentItem is empty, the item will return itself. If not
     * it will call the same method on his parent, thus returning the root item of the
     * hierarchy.
     * 
     * @return top parent item
     */
    public WebCatItemData getTopParentItemData();

    /**
     * Returns the information, if this <code>WebCatSubItem</code> is selected by the 
     * solution configurator.
     * 
     * @return <code>true</code>, if the item is the group default item
     *         <code>false</code>, if not
     * 
     * @see     #setIsScSelected(boolean)
     */
    public boolean isScSelected();

    /**
     * Returns the information, if this <code>WebCatSubItem</code> is optional in the sense, 
     * that it can be selected but must not be selected.
     * 
     * @return <code>true</code>, if the item is optional.
     *         <code>false</code>, if not
     */
    public boolean isOptional();
    
    /**
     * Returns the information, if this <code>WebCatSubItem</code> is a part of a default group. 
     * 
     * @return <code>true</code>, if the item is part of a default group
     *         <code>false</code>, if not
     * 
     * @see     #isPartOfDefaultGroup(boolean)
     */
    public boolean isPartOfDefaultGroup();
    
    /**
     * Returns the information, if this <code>WebCatSubItem</code> is a part of a mandatory group. 
     * 
     * @return <code>true</code>, if the item is part of a mandatory group
     *         <code>false</code>, if not
     * 
     * @see     #setIsPartOfMandatoryGroup(boolean)
     */
    public boolean isPartOfMandatoryGroup();

    /**
     * Sets the identifier of the group the item belongs to, in case it is part of an option
     * group. All items with the same <code>groupKey</code> make up an option group.
     * 
     * @param groupKey a <code>String</code> that is an identifier for the group.
     */
    public void setGroupKey(String groupKey);

    /**
     * Sets the indicator, that a <code>WebCatSubItem</code> is selected by the
     * solution configurator.
     * 
     * @param isScSelected a <code>boolean</code> value that is set to <code>true</code> if
     *                       the <code>WebCatSubItem</code> is selected by the solution configurator.
     * 
     * @see #setGroupKey(String)
     * @see #isScSelected()
     */
    public void setIsScSelected(boolean isScSelected);

    /**
     * Sets the indicator, if this <code>WebCatSubItem</code> is optional in the sense, that it 
     * can be selected but must not be selected.
     * 
     * @param isOptional a <code>boolean</code> value that is set to <code>true</code> if the
     *                   <code>WebCatSubItem</code> is optional.
     */
    public void setIsOptional(boolean isOptional);
    
    /**
     * Sets the indicator, if this <code>WebCatSubItem</code> is part of a default group.
     * 
     * @param isPartOfMandatoryGroup a <code>boolean</code> value that is set to <code>true</code> if the
     *                   <code>WebCatSubItem</code> is part of a default group.
     * 
     * @see     #setIsPartOfDefaultGroup()
     *
     */
    public void setIsPartOfDefaultGroup(boolean isPartOfDefaultGroup);
    
    /**
     * Sets the indicator, if this <code>WebCatSubItem</code> is part of a mandatory group.
     * 
     * @param isPartOfMandatoryGroup a <code>boolean</code> value that is set to <code>true</code> if the
     *                   <code>WebCatSubItem</code> is part of a mandatory group.
     * 
     * @see     #isPartOfMandatoryGroup()
     *
     */
    public void setIsPartOfMandatoryGroup(boolean isPartOfMandatoryGroup);

    /**
     * Sets the parent <code>WebCatItem</code> of this <code>WebCatSubItem</code>.
     * 
     * @param parentItem the parent <code>WebCatItem</code>
     */
    public void setParentItem(WebCatItemData parentItem);
    
    /**
     * Sets the Solution Configurator item type
     * 
     * @param scItemType the Solution configurator item type
     */
    public void setScItemType(String scItemType);

    /**
     * Sets the selection status of this <code>WebCatSubItem</code>.<p>
     * 
     * The selection status is used to control, which items are transferred to the solution 
     * configurator explosion. It is necessary to transfer the information, which items are
     * currently selected, but also which items have been deselected. The allowed values for 
     * <code>author</code> are {@link #SELECTED_FOR_RES}, 
     * {@link #DESELECTED_FOR_RES} and {@link #NOT_SELECTED_FOR_RES}.
     * 
     * @param author selection status as <code>String</code>
     * 
     * @see     #getAuthor()
     */
    public void setAuthor(String selectedForRes);

    /**
     * Sets the sort number of the item for the solution configurator
     * Method implements WebCatSubItemData 
     * 
     * @param sortNr as <code>String</code> that is a sort number of sub item.
     */
    public void setSortNo(String sortNo);

    /**
     * Returns the sort number of the item for the solution configurator
     * Method implements WebCatSubItemData 
     * 
     * @return sortNr as <code>String</code> that is a sort number of sub item.
     */
    public String getSortNo(); 

}
