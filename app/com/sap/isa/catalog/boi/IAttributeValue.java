/*****************************************************************************
  Copyright (c) 2000, SAPMarkets Europe GmbH, Germany, All rights reserved.
  Created:      29 March 2001

  $Id: //sap/ESALES_base/30_SP_COR/src/_pcatAPI/java/com/sap/isa/catalog/boi/IAttributeValue.java#3 $
  $Revision: #3 $
  $Change: 150872 $
  $DateTime: 2003/09/29 09:18:51 $
*****************************************************************************/
package com.sap.isa.catalog.boi;

import java.net.URL;
import java.util.Date;
import java.util.Iterator;

/**
 * A value instance for a given {@link IAttribute} instance.
 * {@link IItem IItems} have assoziations to their IAttributeValues.
 *
 * @since       1.0
 * @version     2.0
 */
public interface IAttributeValue extends IComponent
{
    /**
     * Returns the reference to the attribute.
     *
     * @since 1.0
     * @return the attribute underlying this value
     */
    IAttribute getAttribute();

    /**
     * Returns the language dependent display name of the attribute.
     *
     * @since 1.0
     * @return the name of the associated attribute
     */
    String getAttributeName();

    /**
     * Returns the unique identifier of the attribute.
     *
     * @since 1.0
     * @return the guid of the attribute
     */
    String getAttributeGuid();

    /**
     * Returns the language dependent description of the attribute.
     *
     * @since 1.0
     * @return the description of the associated attribute
     */
    String getAttributeDescription();

    /**
     * Return the unit of the value if available. If the associated
     * attribute does not support units null is returned.
     *
     * @since 1.0
     * @return theUnit of this value
     */
    IUnit getUnit();

    /**
     * Returns the parent item of the value.
     *
     * @since 1.0
     * @return the item to which this value belongs to
     */
    IItem getParent();

    /**
     * Trys to get a String representation for all values.
     *
     * @since 1.0
     * @return an iterator for all string representations of the attribute values
     */
    Iterator getAllAsString();
    
    /**
     * Returns the number of values in the valuelist
     */
    public int getNoOfValues();

    /**
     * Trys to get a list of the type of the
     * {@link IAttribute attribute}.
     *
     * @since 2.0
     * @return a list of IIntervals
     */
    Iterator getAllValues();

    /**
     * Trys to get a String representation of the value.
     *
     * @since 1.0
     * @return a string representation of the attribute
     */
    String getAsString();

    /**
     * Trys to get a String representation of the value.
     *
     * @since 2.0
     * @return a string representation of the attribute
     */
    Integer getAsInteger();

    /**
     * Trys to get a String representation of the value.
     *
     * @since 2.0
     * @return a string representation of the attribute
     */
    Long getAsLong();

    /**
     * Trys to get a String representation of the value.
     *
     * @since 2.0
     * @return a string representation of the attribute
     */
    Boolean getAsBoolean();

    /**
     * Trys to get a String representation of the value.
     *
     * @since 2.0
     * @return a string representation of the attribute
     */
    Object getAsObject();

    /**
     * Trys to get a String representation of the value.
     *
     * @since 2.0
     * @return a string representation of the attribute
     */
    IInterval getAsInterval();

    /**
     * Trys to get a String representation of the value.
     *
     * @since 2.0
     * @return a string representation of the attribute
     */
    Date getAsDate();

    /**
     * Trys to get a String representation of the value.
     *
     * @since 2.0
     * @return a string representation of the attribute
     */
    Double getAsDouble();

    /**
     * Trys to get a URL representation of the value.
     *
     * @since 2.0
     * @return a URL representation of the attribute
     */
    URL getAsURL();

    /**
     * Is this (compare with attribute) multivalued.
     *
     * @since 1.0
     * @return result isMultivalued
     */
	boolean isMultiValue();
}
