/*****************************************************************************
  Copyright (c) 2000, SAPMarkets Europe GmbH, Germany, All rights reserved.
  Created:      29 March 2001

  $Id: //sap/ESALES_base/30_SP_COR/src/_pcatAPI/java/com/sap/isa/catalog/boi/ICategory.java#3 $
  $Revision: #3 $
  $Change: 150872 $
  $DateTime: 2003/09/29 09:18:51 $
*****************************************************************************/
package com.sap.isa.catalog.boi;

import java.util.Comparator;
import java.util.Iterator;

/**
 * The interface to get information about a given category.
 * The categories of a catalog form a hierarchical tree to allow a typographic
 * browsing of the catalog. Any meaning full catalog should have a balanced
 * tree structure i.e. the number of items in a category should be of
 * O(SQRT(#Items in the catalog)). Otherwise you have to pay a performance
 * penalty independent from the implementation. Categories have assoziations
 * to their specific {@link IAttribute attributes}, {@link IDetail details}
 * and of course the {link@ IItem Items} in the category.
 *
 * @since       1.0
 * @version     2.0
 */
public interface ICategory extends IComponent
{
        /**
         * The category specific attributes.
         * Returns an iterator for the category specific attributes sorted by the
         * default sort order.
         * If the category has no attributes an empty iterator will be returned.
         * The global attributes can be queried over the
         * {@link ICatalog#getAttributes()}.
         *
         * @since   1.0
         * @return  an iterator for all {@link IAttribute <code>IAttribute</code>}
         *          instances for the category specific attributes.
         * @see     ICatalog
         * @exception CatalogException error while retrieving the data from the server
         **/
    Iterator getAttributes() throws CatalogException;
    
    /**
     * Return the number of attributes
     * 
     * @return int the number of attributes, or 0 if none
     */
    public int getAttributeSize();

        /**
         * Returns an iterator for the category specific attributes sorted by the
         * given comparator.
         * If the category has no attributes no instances will be returned.
         * The global attributes can be queried over the
         * {@link ICatalog#getCategories()}.
         *
         * @since   1.0
         * @param   aComparator the comparator for sorting the instances
         * @return  an iterator for all {@link IAttribute <code>IAttribute</code>}
         *          instances for category specific attributes, sorted.
         * @see     ICatalog
         * @exception CatalogException error while retrieving the data from the server
         * */
    Iterator getAttributes(Comparator aComparator) throws CatalogException;
    
    /**
    * Get the number of hits of the query.
    * After submitting a query in count mode this the only information/state
    * that changed.
    *
    * @returns theHits the number of items the query has or will retrieve if submitted
    */
    int getCount();   
    
    /**
     * Returns the number of main items in the result list
     *
     * @return int the number of main items in the result list
     */
    public int getMainItemCount(); 

        /**
         * Returns an iterator of {@link ICategory <code>ICategory</code>} instances.
         * They are direct children of this instance. The instances are sorted by
         * the default sort order.
         *
         * @since   1.0
         * @return  iterator of <code>ICategory</code> instances
         * @exception CatalogException error while retrieving the data from the server
         */
    Iterator getChildCategories() throws CatalogException;

        /**
         * Returns an iterator of {@link ICategory <code>ICategory</code>} instances.
         * They are direct children of this instance. The instances are sorted by
         * the given comparator.
         *
         * @since   1.0
         * @param   aComparator the comparator for sorting the instances
         * @return  iterator of <code>ICategory</code> instances
         * @exception CatalogException error while retrieving the data from the server
         */
    Iterator getChildCategories(Comparator aComparator) throws CatalogException;

        /**
         * Returns <code>null</code> if this instance is a root category.
         *
         * @since   1.0
         * @return  ICategory theParent
         */
    ICategory getParent();

        /**
         * Returns an iterator for all items i.e. the products/services. The
         * returned instances will have the type {link@ IItem <code>IItem</code>}.
         * They are sorted by the default sort order.
         *
         * @since   1.0
         * @return  iterator for all items of the category
         * @exception CatalogException error while retrieving the data from the server
         */
    Iterator getItems() throws CatalogException;

        /**
         * Returns an iterator for all items i.e. the products/services. The
         * returned instances will have the type {link@ IItem <code>IItem</code>}.
         * They are sorted by the given comparator.
         *
         * @since   1.0
         * @param   aComparator the comparator for sorting the instances
         * @return  iterator for all items of the category
         * @exception CatalogException error while retrieving the data from the server
         */
    Iterator getItems(Comparator aComparator) throws CatalogException;

        /**
         * Gets a concrete item for the given guid. If the item can't be found
         * <code>null</code> will be returned.
         *
         * @since   1.0
         * @param   itemGuid the guid of the requested item
         * @return  a specific item instance
         * @exception CatalogException error while retrieving the data from the server
         */
    IItem getItem(String itemGuid) throws CatalogException;

        /**
         * Gets the language dependent name of the category.
         *
         * @since   1.0
         * @return  name of the category
         */
    String getName();

        /**
         * Gets the language dependent long description of the category.
         *
         * @since   1.0
         * @return  description of the category
         */
    String getDescription();

        /**
         * Returns the thumbnail associated with this instance.
         *
         * @since   1.0
         * @return  the thumbnail of the category
         */
    String getThumbNail();

        /**
         * Gets the guid of the category. i.e. a key that uniquely
         * identifies the category.
         *
         * @since   1.0
         * @return  the guid of the category
         */
    String getGuid();

        /**
         * Gets the query with the given name.
         * Provides a history of the already created and submitted queries for
         * that category. You should not rely on getting back a reference.
         * The history of the queries will be clean up by an implementation
         * dependent  mechanism.
         *
         * @since   1.0
         * @see     ICatalog#getQuery(String aString)
         * @param   aName the name of the requested query
         * @return  an instance of type <code>IQuery</code> if found,
         *          otherwise <code>null</code>
         */
    IQuery getQuery(String aName);

        /**
         * Gets the cached queries.
         * Provides a history of the already created and submitted queries for
         * the catalog. You should not rely on getting back a reference, i.e. be prepared to reissue a
         * former statement if you can not find it in the history.
         * The history of the queries will be clean up by an implementation
         * dependent  mechanism.
         *
         * @since 2.1
         * @see ICategory#getQueries()
         * @return an instance of type <code>Iterator</code>
         */
    Iterator getQueries();
    
        /**
         * Gets an instance of an <code>IQueryStatement</code> object to formulate
         * a search statement. This statement has to be passed to the factory
         * method for a query {@link #createQuery(IQueryStatement aStatement)}
         * that can submit that statement to the catalog.
         *
         * @since   1.0
         * @see     ICatalog#createQueryStatement()
         * @return  a new statement instance
         */
    IQueryStatement createQueryStatement();

        /**
         * Gets an instance of an <code>IQuery</code> object to formulate and submit
         * a query against the catalog. The necessary statement has to be created via
         * the {@link #createQueryStatement()} method. If a similar {based on the
         * comparison of the statement} query was already created the old instance
         * will be returned.
         *
         * @since   1.0
         * @see     ICatalog#createQuery(IQueryStatement aStatement)
         * @param   aStatement the statment of the query
         * @exception CatalogException error while retrieving the data from the server
         * @return  a new query instance
         */
    IQuery createQuery(IQueryStatement aStatement) throws CatalogException;

        /**
         * Gets an instance of an <code>IQuery</code> object to formulate and submit
         * a query against the catalog. The necessary statement has to be created via
         * the {@link #createQueryStatement()} method. If a similar {based on the
         * comparison of the statement} query was already created the old instance
         * will be returned.
         *
         * @see ICatalog#createQuery(IQueryStatement aStatement)
         * @param  aStatement the statement of the query
         * @param  cache boolean that indicates if the query should be cached
         * @exception CatalogException error while retrieving the data from the server
         * @return a new query instance
         */
    IQuery createQuery(IQueryStatement aStatement, boolean cache) throws CatalogException;

        /**
         * Adds a listener instance to the list of instances that are interested
         * in composite events.
         *
         * @param listener the listener instance to be added
         */
    public void addCompositeEventListener(ICompositeEventListener listener);

        /**
         * Removes a listener instance from the list of instances that are interested
         * in composite events.
         *
         * @param listener the listener instance to be removed
         */
    public void removeCompositeEventListener(ICompositeEventListener listener);

        /**
         * @link aggregationByValue
         * @label hasDetails
         * @supplierCardinality 0..*
         */
        /*#IDetail lnkIDetail;*/
}
