/*****************************************************************************
  Copyright (c) 2000, SAPMarkets Europe GmbH, Germany, All rights reserved.

  $Id: //sap/ESALES_base/30_SP_COR/src/_pcatAPI/java/com/sap/isa/catalog/boi/ICatalogTask.java#3 $
  $Revision: #3 $
  $Change: 144303 $
  $DateTime: 2003/08/22 20:18:21 $
*****************************************************************************/

package com.sap.isa.catalog.boi;

import java.io.Serializable;
import java.util.Date;

/**
 * An instance of ICatalogTask will be created as a consequence of an administrative
 * action taken by an IClient/IActor. The user might query a catalog server to show the
 * tasks running on that server.
 * The task interface shows then more information about the specific task.
 * 
 * @persistent
 * @version 1.0
 * @since 2.0 
 */
public interface ICatalogTask
    extends
        ICatalogIdentity
{
    IActor getOwner();

    Action getAction();

    Schedule getSchedule();

    Status getStatus();

    String getSourceCatalogGuid();
    String getTargetCatalogGuid();

    String getSourceServerGuid();
    String getTargetServerGuid();

    void schedule()
        throws CatalogException;

    void kill(IActor theOwner)
        throws CatalogAuthorizationException, CatalogException;
    
    
        /**
         * @label hasTasks
         * @supplierCardinality 1
         * @clientCardinality 0..* 
         */
        /*#IActor lnkIActor;*/

    static class Action
    {
        private String theName;

        private Action(String theName)
        {
            this.theName=theName;
        }

        static public Action DELETE    =   new Action("delete");
        static public Action ADD       =   new Action("add");
        static public Action UPDATE    =   new Action("update");
    }

    static class Schedule
        implements Serializable
    {
        private Date theStart;
        private Date theEnd;
        private long thePeriod;

        public Schedule(Date aStart, Date anEnd)
        {
            theStart=aStart;
            theEnd=anEnd;
            thePeriod=0L;
        }

        public Schedule(Date aStart, Date anEnd, long aPeriod)
        {
            theStart=aStart;
            theEnd=anEnd;
            thePeriod=aPeriod;
        }

        public Date getStart()
        {
            return theStart;
        }

        public Date getEnd()
        {
            return theEnd;
        }

        public long getPeriod()
        {
            return thePeriod;
        }

        public boolean isPeriodic()
        {
            return (thePeriod==0L?false:true);
        }
    }

    static class Status
        implements Serializable
    {
        private String theName;

        Status(String theName)
        {
            this.theName=theName;
        }

        static public Status FAILED  =   new Status("failed");
        static public Status FINISHED  =   new Status("finished");
        static public Status QUEUED    =   new Status("queued");
        static public Status RUNNING     =   new Status("running");
        static public Status KILLED = new Status("killed");
        static public Status CREATED = new Status("created");        
    }
}
