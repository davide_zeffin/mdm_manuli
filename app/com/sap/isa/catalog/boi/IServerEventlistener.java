/*****************************************************************************
  Copyright (c) 2000, SAPMarkets Europe GmbH, Germany, All rights reserved.

  $Id: //sap/ESALES_base/30_SP_COR/src/_pcatAPI/java/com/sap/isa/catalog/boi/IServerEventlistener.java#3 $
  $Revision: #3 $
  $Change: 144303 $
  $DateTime: 2003/08/22 20:18:21 $
*******************************************************************************/

package com.sap.isa.catalog.boi;

/**
 * @stereotype interface 
 */
public interface IServerEventlistener
{
    void catalogDeleted(ICatalogDeletedEvent aDeletEvent);

    void catalogCreated(ICatalogCreatedEvent aCreatedEvent);

    void catalogUpdated(ICatalogUpdatedEvent anUpdateEvent);
}
