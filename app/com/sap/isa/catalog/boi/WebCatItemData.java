/*****************************************************************************

    Class:        WebCatItemData
    Copyright (c) 2006, SAP AG, Germany, All rights reserved.
    Created:      03.02.2006

*****************************************************************************/
package com.sap.isa.catalog.boi;

import java.util.ArrayList;

import com.sap.isa.backend.boi.isacore.BusinessObjectBaseData;
import com.sap.isa.businessobject.item.ContractDuration;
import com.sap.isa.catalog.webcatalog.WebCatItemKey;
import com.sap.isa.catalog.webcatalog.WebCatItemPrice;
import com.sap.isa.core.TechKey;
import com.sap.isa.core.businessobject.boi.ObjectBaseData;
import com.sap.spc.remote.client.object.IPCItem;
import com.sap.spc.remote.client.util.imp.c_ext_cfg_imp;

/**
 * Data interface for <code>WebCatItem</code>.<br>
 * 
 * Enables the access to the WebCatItem from the backend classes, 
 * e.g. the SolutionConfiguratorBackend.
 */
public interface WebCatItemData extends BusinessObjectBaseData , ObjectBaseData {
    
    /** 
     * Indicator that the product is configurable using IPC 
     */
    public final static String PRODUCT_CONFIGURABLE_IPC = "I";

    /**
     * Indicator that the product is configurable using IPC, but no IPC item was created yet. 
     * This one checks that the product is marked as configurable and as configurable using IPC, 
     * but does not check whether an IPC item was created successfully. This is used in pricing, 
     * as IPC configurable items should be priced through IPC even if the shop settings say list 
     * prices, where no IPCItem exists as it will then be created from pricing.
     */
    public final static String PRODUCT_CONFIGURABLE_IPC_NOITEM = "B";

    /** 
     * Indicator that the product is configurable, but not using IPC 
     */
    public final static String PRODUCT_CONFIGURABLE_OTHER = "O";

    /**
     * Indicator that the product is configurable, but configuration is currently not possible 
     * (for example IPC could not create an item)
     */
    public final static String PRODUCT_CONFIGURABLE_ERROR = "E";

    /** 
     * Indicator that the product actually is a product variant 
     */
    public final static String PRODUCT_VARIANT = "V";

    /**
     * Indicator that the product is a grid product and it that it is configurable within the IPC,
     * but with a different UI. (Grid UI)
     */
    public final static String PRODUCT_CONFIGURABLE_IPC_GRID = "G";

    /** 
     * Indicator that the product is not configurable 
     */
    public final static String PRODUCT_NOT_CONFIGURABLE = " ";
    
    /** 
     * Value returned from the TREX, in case a product category or 
     * product hierarchy description is not known 
     */
    public final static String UNKNOWN_CAT_DESCR = "-";

    /**
     *  Clears the solution configurator relevant IPC attributes HashMap.
     */
    public void clearScRelvIPCAttr();
    
    /**
     * Returns ID of catalog area.
     *
     * @return unique identifier of the catalog area (category) this item belongs to, as String
     */
    public String getAreaID();

    /**
     * Retrieves the value for the requested attribute.
     * 
     * @param attrName the name of the attribute
     * @return the value of the attribute, null if not found
     */
    public String getAttribute(String attrName);

    /**
     * Returns the <code>IItem</code> of this <code>WebCatItemData</code> object.
     * 
     * @return the IItem
     */
    public IItem getCatalogItem();
    
    /**
     * Returns reference to configuration (if available). This does not determine a new reference 
     * if none available! As currently configuration is done through IPC that also does pricing, 
     * call <code>readItemPrice()</code> to determine price and configuration, afterwards this 
     * method will return the reference to configuration.
     *
     * @return reference to configuration
     */
    public IPCItem getConfigItemReference();
    
    /**
     * Determine whether this item's product is configurable. This method checks for the attribute
     * whether product is configurable. In case of IPC usage, it also checks whether an IPC item 
     * is available (otherwise it is configurable, but starting IPC configuration would fail).
     *
     * @return flag indicating whether product is configurable, see constants for allowed values.
     */
    public String getConfigurableFlag();

    /**
     * Returns product description.
     *
     * @return description from catalog, as String
     */
    public String getDescription();
    
    /**
     * Returns the WebCatItemKey of the item.
     *
     * @return WebCatItemKey of this item
     */
    public WebCatItemKey getItemKey();

    /**
     * Returns the <code>TeckKey</code> of the parent of this <code>WebCatItem</code>.
     * 
     * @return parentTechKey
     */
    public TechKey getParentTechKey();
    
    /**
     * Returns name of product.
     *
     * @return identifier of the product which is included in this item; usually a speaking abbrevation 
     *         or some product number, in opposite to <code>getProductID</code>
     */
    public String getProduct();

    /**
     * Returns ID of product.
     *
     * @return unique identifier of the product which is included in this item
     */
    public String getProductID();

    /**
     * Returns quantity. Quantity is not a catalog data, but a property used for the web 
     * visualization to be able to select the quantity before adding products to a shopping basket, 
     * order etc. This quantity is only valid for the lifetime of the itemKey and is not saved
     * anywhere; it should be used when adding this item to some basket etc.
     *
     * @return quantity of this item, as String
     */
    public String getQuantity();
    
    /**
     * Returns the Guid of the baskets SC document, if the item is coming from a basket. This field 
     * is to be set if the details for a basket item are displayed and the SC might be called, so the SC
     * can determine the items SC configuration from the orders SC config document.
     * 
     * @return TechKey scOrderDocumentGuid the belonging SalesDocument SC DocumentGuid, in case the WebCatItem belongs 
     *                                to a Product of a basket item.
     */
    public TechKey getSCOrderDocumentGuid();
    
    /**
     * returns the selected contract duration. Per default, the selectedContractDuration is the defaultContractDuration
     * 
     * @return ContractDuration, the selected contract duration. If the WebCatItem has no contract duration, the function returns null. 
     */
    public ContractDuration getSelectedContractDuration();
    
    /**
     * returns the default contract duration.
     * 
     * @return ContractDuration, the default contract duration. 
     */
    public ContractDuration getDefaultContractDuration();
    
    /**
     * returns the contract duration array.
     * 
     * @return table of ContractDuration, the contract duration array. 
     */
    public ArrayList getContractDurationArray();      

    /**
     * Returns unit of measurement
     *
     * @return unit of measurement, as String
     */
    public String getUnit();
    
    /**
     * Returns the items WebCatInfoData object.
     *
     * @return the WebCatInfoData Object, the item belongs to
     */
    public WebCatInfoData getWebCatInfoData();

    /** 
    * Returns the <code>webCatItemList</code> of this <code>WebCatItemData</code>.
    * 
    * @return the webCatSubItemList object.
    */
    public WebCatSubItemListData getWebCatSubItemListData();

    /**
     * Check whether a configuration item is available (only IPC supported!) and if so, 
     * whether its configuration is not initial.
     *
     * @return boolean indicating whether the item has a non-initial configuration
     */
    public boolean isConfigured();

    /**
     * Adds the given key-value pair to the solution configuratior relevant IPC attributes HashMap.
     * 
     * @param key to be added.
     * @param value to be added.
     */
    public void putScRelvIPCAttr(Object key, Object value);

    /**
     * Sets the item price.
     * 
     * @param price
     */
    public void setItemPrice(WebCatItemPrice price);
    
    /**
     * Sets the external configuration
     * 
     * @param extCfg the external configuration
     */
    public void setExtCfg(c_ext_cfg_imp extCfg);


    /**
     * Sets the parent <code>TechKey</code> of this <code>WebCatItem</code>
     * 
     * @param parentTechKey
     */
    public void setParentTechKey(TechKey parentTechKey);
    
    /**
     * Sets the quantity.
     *
     * @param quantity 
     */
    public void setQuantity(String quantity);
    
    /**
     * Sets the Guid of the baskets SC document, if the item coming from the basket. This field 
     * is to be set if the details for a basket item are displayed and the SC might be called, so the SC
     * can determine the items SC configuration from the orders SC config document.
     * 
     * @param scOrderDocumentGuid the belonging SalesDocument SC DocumentGuid, in case the WebCatItem belongs 
     *                            to a Product of a basket item.
     */
    public void setSCOrderDocumentGuid(TechKey scOrderDocumentGuid);
    
    /**
     * sets the selected contract duration
     * 
     * @param selectedContractDuration : the selectedContractDuration for this WebCatItem
     */
    public void setSelectedContractDuration(ContractDuration selectedContractDuration);
    
    /**
     * sets the default contract duration
     * 
     * @param defaultContractDuration : the defaultContractDuration for this WebCatItem
     */
    public void setDefaultContractDuration(ContractDuration defaultContractDuration);
    
    /**
     * sets the contract duration array
     * 
     * @param contractDurationArray : the contractDurationArray for this WebCatItem
     */
    public void setContractDurationArray(ArrayList contractDurationArray);
    
    
    /**
     * Sets the unit of measurement for this WebCatItem.
     *
     * @param unit
     */
    public void setUnit(String unit);

    /**
     * Sets the <code>webCatSubItemList</code> of this <code>WebCatItemData</code>.
     * 
     * @param webCatSubItemListData
     */
    public void setWebCatSubItemListData(WebCatSubItemListData webCatSubItemListData);
    
    /**
     * Returns all point codes of the product
     * @return point codes, as String[]
     */
    public String[] getLoyPtCodes();

    /**
     * Returns all points of the product
     * @return points, as String[]
     */
    public String[] getLoyPts();
        
}
