/*****************************************************************************
  Copyright (c) 2000, SAPMarkets Europe GmbH, Germany, All rights reserved.

  $Id: //sap/ESALES_base/30_SP_COR/src/_pcatAPI/java/com/sap/isa/catalog/boi/IActor.java#3 $
  $Revision: #3 $
  $Change: 144303 $
  $DateTime: 2003/08/22 20:18:21 $
*****************************************************************************/
package com.sap.isa.catalog.boi;

import java.util.Iterator;

/**
 * IActor.java
 * IActor represents an user how wants to interact in any way with a 
 * catalog side {@link ISite } a catalog server {@link IServerEngine} or a 
 * catalog itself {@link ICatalog}. The Role the user has is sofar und 
 * defined.
 *
 * @since 2.0
 * @version 2.0
 */

public interface IActor 
{
    /**
     * The name of the catalog user/usergroup the client belongs to or
     * represents.
     *
     * @since   1.0
     * @return the name of the client
     */
    String getName();

    /**
     * The corresponding password on the catalog server.
     *
     * @since   1.0
     * @return the password of the client
     */
    String getPWD();

    Iterator getPermissions();

    /**
     * The unique identifier of the user/session/transaction.
     *
     * @since   1.0
     * @return the id
     */
    String getGuid();

    String getCookie();
}// IActor
