/*****************************************************************************
  Copyright (c) 2000, SAPMarkets Europe GmbH, Germany, All rights reserved.
  Created:      29 March 2001

  $Id: //sap/ESALES_base/30_SP_COR/src/_pcatAPI/java/com/sap/isa/catalog/boi/IQuery.java#2 $
  $Revision: #2 $
  $Change: 129221 $
  $DateTime: 2003/05/15 17:20:58 $
*****************************************************************************/
package com.sap.isa.catalog.boi;

import java.util.Iterator;

/**
 * This class represents a query against a catalog or a category.
 * A query is a category. But to get to the items you first have to call
 * <code>{@link #submit()}</code>. In case the query was created by a category
 * it has the same category specific attributes as its parent. The queries of a
 * catalog or category are accessed via a own getter
 * ({@link ICategory#getQuery(String aName)},{@link ICatalog#getQuery(String aName)})
 * and not over the {@link ICategory#getChildCategories()}, {@link ICatalog#getChildCategories()})
 * methods.
 *
 * @since       1.0
 */
public interface IQuery extends ICategory {
    
    /**
     * Returns the statement of the query.
     *
     * @since   1.0
     * @return  a query statement
     */
    IQueryStatement getStatement();

    /**
     * Submit the query to the server.
     *
     * @since   1.0
     * @return  the results of the query as an Iterator. The type of the resulting objects
     * has be requested via the <code>queryInterface()</code> method. It depends on
     * the search type that was set for the underlying query statement.
     * @exception CatalogException error while retrieving the data from the server
     */
    Iterator submit() throws CatalogException;

    /**
    * Toggle the count mode.
    * In the count mode only the catalog engine is queried. No items are actually
    * retrieved/created. You fell no performance/memory  hit on the API side due to
    * creation of objects. All you fell is the performance of the catalog engine.
    * After performing the query in the count mode you might
    * retrieve the number of hits via {@link #getCount()}
    *
    * @param isCount is or is not in count mode
    */
    void setCountMode(boolean isCount);

    /**
    * Query the count mode.
    * 
    * @returnisCount is or is not in count mode
    */    
    boolean inCountMode();
    
    /**
     * Sets the requestCategorySpecificAttributes mode. If this is set to <code>true</code>
     * category specific attributes are read.
     * 
     * @param mode true, if specific attributes should be read, false otherwise.
     */
    void setRequestCategorySpecificAttributes(boolean mode);
    
    /**
     * Reads the requestCategorySpecificAttributes mode.
     * 
     * @return true, if specific attributes should be read, false otherwise.
     */
    boolean getRequestCategorySpecificAttributes();
}
