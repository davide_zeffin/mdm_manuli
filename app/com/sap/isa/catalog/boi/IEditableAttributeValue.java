/*****************************************************************************
  Copyright (c) 2000, SAPMarkets Europe GmbH, Germany, All rights reserved.
  Created:      01 August 2001

  $Id: //sap/ESALES_base/30_SP_COR/src/_pcatAPI/java/com/sap/isa/catalog/boi/IEditableAttributeValue.java#2 $
  $Revision: #2 $
  $Change: 129221 $
  $DateTime: 2003/05/15 17:20:58 $
*****************************************************************************/
package com.sap.isa.catalog.boi;

import java.net.URL;
import java.util.Date;

/**
 * Extends the {@link IAttributeValue IAttributeValue} interface so that
 * the value can be changed.
 *
 * @since       2.0
 * @version     1.0
 */
public interface IEditableAttributeValue extends IAttributeValue, IEditableComponent
{
    /**
     * Returns the referenc to the thing we are editing via this.
     * Might be null in case this is new.
     *
     * @return aRef to the object we are about to edit.
     */
     IAttributeValue getAttributeValueReference();

    /**
     * Trys to set a String representation of the value.
     *
     * @since 2.0
     * @param aValue the value to be set
     * @exception CatalogException error while setting the value
     */
    void setAsString(String aValue) throws CatalogException;

    /**
     * Trys to set a Integer representation of the value.
     *
     * @since 2.0
     * @param aValue the value to be set
     * @exception CatalogException error while setting the value
     */
    void setAsInteger(Integer aValue) throws CatalogException;

    /**
     * Trys to set a Long representation of the value.
     *
     * @since 2.0
     * @param aValue the value to be set
     * @exception CatalogException error while setting the value
     */
    void setAsLong(Long aValue) throws CatalogException;

    /**
     * Trys to get a Boolean representation of the value.
     *
     * @since 2.0
     * @param aValue the value to be set
     * @exception CatalogException error while setting the value
     */
    void setAsBoolean(Boolean aValue) throws CatalogException;

    /**
     * Trys to get a Object representation of the value.
     *
     * @since 2.0
     * @param aValue the value to be set
     * @exception CatalogException error while setting the value
     */
    void setAsObject(Object aValue) throws CatalogException;

    /**
     * Trys to get a IInterval representation of the value.
     *
     * @since 2.0
     * @param aValue the value to be set
     * @exception CatalogException error while setting the value
     */
    void setAsInterval(IInterval aValue) throws CatalogException;

    /**
     * Trys to get a Date representation of the value.
     *
     * @since 2.0
     * @param aValue the value to be set
     * @exception CatalogException error while setting the value
     */
    void setAsDate(Date aValue) throws CatalogException;

    /**
     * Trys to get a Double representation of the value.
     *
     * @since 2.0
     * @param aValue the value to be set
     * @exception CatalogException error while setting the value
     */
    void setAsDouble(Double aValue) throws CatalogException;

    /**
     * Trys to get a URL representation of the value.
     *
     * @since 2.0
     * @param aValue the value to be set
     * @exception CatalogException error while setting the value
     */
    void setAsURL(URL aValue) throws CatalogException;

    /**
     * Trys to add a String representation of the value.
     *
     * @since 2.0
     * @param aValue the value to be set
     * @exception CatalogException error while setting the value
     */
    void addAsString(String aValue) throws CatalogException;

    /**
     * Trys to add a Integer representation of the value.
     *
     * @since 2.0
     * @param aValue the value to be set
     * @exception CatalogException error while setting the value
     */
    void addAsInteger(Integer aValue) throws CatalogException;

    /**
     * Trys to add a Long representation of the value.
     *
     * @since 2.0
     * @param aValue the value to be set
     * @exception CatalogException error while setting the value
     */
    void addAsLong(Long aValue) throws CatalogException;

    /**
     * Trys to add a Boolean representation of the value.
     *
     * @since 2.0
     * @param aValue the value to be set
     * @exception CatalogException error while setting the value
     */
    void addAsBoolean(Boolean aValue) throws CatalogException;

    /**
     * Trys to add a Object representation of the value.
     *
     * @since 2.0
     * @param aValue the value to be set
     * @exception CatalogException error while setting the value
     */
    void addAsObject(Object aValue) throws CatalogException;

    /**
     * Trys to add a IInterval representation of the value.
     *
     * @since 2.0
     * @param aValue the value to be set
     * @exception CatalogException error while setting the value
     */
    void addAsInterval(IInterval aValue) throws CatalogException;

    /**
     * Trys to add a Date representation of the value.
     *
     * @since 2.0
     * @param aValue the value to be set
     * @exception CatalogException error while setting the value
     */
    void addAsDate(Date aValue) throws CatalogException;

    /**
     * Trys to add a Double representation of the value.
     *
     * @since 2.0
     * @param aValue the value to be set
     * @exception CatalogException error while setting the value
     */
    void addAsDouble(Double aValue) throws CatalogException;

    /**
     * Trys to add a URL representation of the value.
     *
     * @since 2.0
     * @param aValue the value to be set
     * @exception CatalogException error while setting the value
     */
    void addAsURL(URL aValue) throws CatalogException;
}
