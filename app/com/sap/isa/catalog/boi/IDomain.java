/*****************************************************************************
  Copyright (c) 2000, SAPMarkets Europe GmbH, Germany, All rights reserved.
  Created:      29 March 2001

  $Id: //sap/ESALES_base/30_SP_COR/src/_pcatAPI/java/com/sap/isa/catalog/boi/IDomain.java#2 $
  $Revision: #2 $
  $Change: 129221 $
  $DateTime: 2003/05/15 17:20:58 $
*****************************************************************************/
package com.sap.isa.catalog.boi;
import java.util.Set;

/**
 * Provides information about the set of possible values an
 * {@link IAttribute attribute} might have.
 *
 * @since       1.0
 * @version     2.0
 */
public interface IDomain
{

    /**
     * Might return true for Domains that belong to attributes of type
     * Number, Object, Interval.
     *
     * @since  2.0
     */
    boolean isDiscrete();

    /**
     * In case the attribute has a discrete subset of possible values.
     * An array of {@link IAttributeValue} instances will be returned
     *
     * @since  2.0
     * @return the discrete subset of the possible values.
     */
    Set getSet();
}
