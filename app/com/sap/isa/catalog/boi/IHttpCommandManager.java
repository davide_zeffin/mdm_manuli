/*****************************************************************************
  Copyright (c) 2000, SAPMarkets Europe GmbH, Germany, All rights reserved.
  Created:      29 March 2001

  $Id: //sap/ESALES_base/30_SP_COR/src/_pcatAPI/java/com/sap/isa/catalog/boi/IHttpCommandManager.java#2 $
  $Revision: #2 $
  $Change: 129221 $
  $DateTime: 2003/05/15 17:20:58 $
*****************************************************************************/
package com.sap.isa.catalog.boi;

import java.util.HashMap;

/**
 * Used only internaly by a concrete implementation.
 *
 * Manages the known http commands (http) a specific catalog
 * engine might support via its (http) interface.
 *
 * @since       1.0
 * @version     2.0
 */
public interface IHttpCommandManager
{
    HashMap getCommands();

    IHttpCommand getCommand(String commandKey);
}