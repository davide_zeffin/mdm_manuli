/*****************************************************************************
  Copyright (c) 2000, SAPMarkets Europe GmbH, Germany, All rights reserved.
  Created:      01 August 2001

  $Id: //sap/ESALES_base/30_SP_COR/src/_pcatAPI/java/com/sap/isa/catalog/boi/IEditableAttribute.java#2 $
  $Revision: #2 $
  $Change: 129221 $
  $DateTime: 2003/05/15 17:20:58 $
*****************************************************************************/
package com.sap.isa.catalog.boi;



/**
 * This interface extends the {@link IAttribute IAttribute} interface so that
 * the properties of the attribute can be edited.
 *
 * @since       2.0
 * @version     1.0
 */
public interface IEditableAttribute extends IAttribute, IEditableComponent
{
    /**
     * Returns the referenc to the thing we are editing via this.
     * Might be null in case this is new.
     *
     * @return aRef to the object we are about to edit.
     */
     IAttribute getAttributeReference();

    /**
     * Sets the type of the attribute. The value might be one of the following:
     * {@link IAttribute IAttribute.TYPE_ID_*} final static identifiers.
     *
     * @since 2.0
     * @param aTypeId the new type of the attribute
     * @exception CatalogException the action could not be performed successfully
     */
    void setType(String aTypeId) throws CatalogException;

    /**
     * Sets the quantity of the attribute like: mass, number etc.
     *
     * @since 2.0
     * @param aQuantityId the new quantity of the attribute
     * @exception CatalogException the action could not be performed successfully
     */
    void setQuantity(String aQuantityId) throws CatalogException;

    /**
     * Sets the language dependent name of the attribute.
     *
     * @since 2.0
     * @param aName the new name of the attribute
     * @exception CatalogException the action could not be performed successfully
     */
    void setName(String aName) throws CatalogException;

    /**
     * Sets the language dependent description of the attribute.
     *
     * @since 2.0
     * @param aDescription the new description of the attribute
     * @exception CatalogException the action could not be performed successfully
     */
    void setDescription(String aDescription) throws CatalogException;

    /**
     * Creates a new Detail with the given name.
     *
     * @since 2.0
     * @param aName the name of the new detail information
     * @return the new detail
     * @exception CatalogException the action could not be performed successfully
     */
    IEditableDetail createDetail(String aName) throws CatalogException;

    /**
     * Deletes a concrete Detail.
     *
     * @since 1.0
     * @param aName the detail instance to be deleted
     * @exception CatalogException the action could not be performed successfully
     */
    void deleteDetail(IEditableDetail aDetail) throws CatalogException;
}
