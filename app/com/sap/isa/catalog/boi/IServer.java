/*****************************************************************************
  Copyright (c) 2000, SAPMarkets Europe GmbH, Germany, All rights reserved.
  Created:      29 March 2001

  $Id: //sap/ESALES_base/30_SP_COR/src/_pcatAPI/java/com/sap/isa/catalog/boi/IServer.java#2 $
  $Revision: #2 $
  $Change: 129221 $
  $DateTime: 2003/05/15 17:20:58 $
*****************************************************************************/
package com.sap.isa.catalog.boi;

import java.util.Properties;

/**
 * Represents information necessary to access a catalog on a given catalog 
 * engine/server except  for  the 
 * authentication/authorization aspect {@link IClient}.
 *
 * {@see IClient}
 *
 * @version     2.0
 * @since       1.0
 */
public interface IServer
{
        /**
         * The unique id of the server.
         *
         * @return a <code>String</code> value
         */
    String getGuid();
    
        
    /**
     * Returns the port of the "serverURL" as String.
     *
     * @since       1.0
     * @returns     the port of the server
     */
    int getPort();

    /**
     * Returns a set of key value pairs the current implementation needs in
     * addition to make a connection to a specific server/catalog.
     *
     * @since       1.0
     * @returns the properties associated with the server
     */
    Properties getProperties();

    /**
     * Returns the server part of the "serverURL" as String.
     *
     * @since       1.0
     * @returns     the url of the server
     */
    String getURLString();

    /**
     * If several catalogs are served on the same catalog server or the catalog
     * server needs in any case an additional unique identifier for a catalog.
     * That method should return that identifier.
     *
     * @since       1.0
     * @returns     the guid of the catalog to be accessed
     */
    String getCatalogGuid();
}
