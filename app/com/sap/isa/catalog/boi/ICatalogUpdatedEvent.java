/******************************************************************************
  Copyright (c) 2000, SAPMarkets Europe GmbH, Germany, All rights reserved.
  Created:      09 July 2001
 
   $Id: //sap/ESALES_base/dev/src/_pcatAPI/java/com/sap/isa/catalog/boi/ICatalogChangedEvent.java#2 $
   $Revision: #2 $
   $Change: 71777 $
   $DateTime: 2002/06/26 15:12:12 $
 ******************************************************************************/
package com.sap.isa.catalog.boi;

/**
 * Event that is fired if a catalog is created from a server.
 * {@see IServerEventListener}
 *
 * @since       2.0
 * @version     1.0
 */
public interface ICatalogUpdatedEvent
{
    /**
     *
     * @return the server of the changed catalog
     */
    IServerEngine getServer();

    /**
     *
     * @return the changeded catalog info
     */
     ICatalogInfo getCatalogInfo();
}
