/*****************************************************************************
  Copyright (c) 2000, SAPMarkets Europe GmbH, Germany, All rights reserved.
  Created:      29 March 2001

  $Id: //sap/ESALES_base/30_SP_COR/src/_pcatAPI/java/com/sap/isa/catalog/boi/IEnvironment.java#2 $
  $Revision: #2 $
  $Change: 129221 $
  $DateTime: 2003/05/15 17:20:58 $
*****************************************************************************/
package com.sap.isa.catalog.boi;

import com.sap.isa.core.init.InitializationEnvironment;

/**
 * This interface contains all the Catalog has to know to about the runtime
 * enviroment.
 *
 * @since       1.0
 * @version     2.0
 */
public interface IEnvironment
{
    /**
     * Returns the InitializationEnvironment  the catalog will use.
     *
     * @since  1.0
     * @return the message resources to be used
     */
    InitializationEnvironment getEnvironment();
}
