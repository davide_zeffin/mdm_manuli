/*****************************************************************************
  Copyright (c) 2000, SAPMarkets Europe GmbH, Germany, All rights reserved.
  Created:      29 March 2001

  $Id: //sap/ESALES_base/30_SP_COR/src/_pcatAPI/java/com/sap/isa/catalog/boi/IQueryItem.java#2 $
  $Revision: #2 $
  $Change: 129221 $
  $DateTime: 2003/05/15 17:20:58 $
*****************************************************************************/
package com.sap.isa.catalog.boi;



/**
 * A result item in a query. For each instance of IQueryItem you have an
 * {@link IItem item} in the catalog. The distinction between a queryItem and
 * an item is that the first might be a partial copy of the last. An item might
 * have category specific attributes the queryItem might have only global
 * attributes.
 * The queryItem is a partial copy of the item if it is part of a global search
 * and the item itself is part of a category that has category specific
 * attributes. The queryItem has a getter for the guid of its "home category".
 * The query item will have the same guid as the item in the catalog.
 *
 * @see     IQuery
 * @see     IQueryStatement
 * @since   1.0
 * @version 2.0
 */
public interface IQueryItem extends IItem
{
     /**
      * Get the guid of the {@link ICategory category} the queryItem belongs to.
      *
      * return the category guid
      */
     String getCategoryGuid();
}
