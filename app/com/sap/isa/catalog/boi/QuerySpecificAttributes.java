package com.sap.isa.catalog.boi;

public interface QuerySpecificAttributes {

     void setRequestCategorySpecificAttributes(boolean mode);
     boolean getRequestCategorySpecificAttributes();

}