/*****************************************************************************
  Copyright (c) 2000, SAPMarkets Europe GmbH, Germany, All rights reserved.
  Created:      09 July 2001

  $Id: //sap/ESALES_base/30_SP_COR/src/_pcatAPI/java/com/sap/isa/catalog/boi/IItemEventListener.java#2 $
  $Revision: #2 $
  $Change: 129221 $
  $DateTime: 2003/05/15 17:20:58 $
*******************************************************************************/
package com.sap.isa.catalog.boi;

import java.util.EventListener;

/**
 * A listener interface for objects interested in item specific events.
 *
 * @since       2.0
 * @version     1.0
 */
public interface IItemEventListener extends EventListener
{
    /**
     * A value was created. The listener is informed about the source
     * item and the new value.
     *
     * @param aCreatedEvent an event that has the details
     */
    void attributeValueCreated(IAttributeValueCreatedEvent aCreatedEvent);

    /**
     * A value was deleted. The listener is informed about the source
     * item and the deleted value.
     *
     * @param aCreatedEvent an event that has the details
     */
    void attributeValueDeleted(IAttributeValueDeletedEvent aDeletedEvent);
}
