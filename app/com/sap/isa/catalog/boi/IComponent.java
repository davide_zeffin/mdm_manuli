/*****************************************************************************
  Copyright (c) 2000, SAPMarkets Europe GmbH, Germany, All rights reserved.
  Created:      09 July 2001

  $Id: //sap/ESALES_base/30_SP_COR/src/_pcatAPI/java/com/sap/isa/catalog/boi/IComponent.java#3 $
  $Revision: #3 $
  $Change: 150872 $
  $DateTime: 2003/09/29 09:18:51 $
*****************************************************************************/

package com.sap.isa.catalog.boi;

import java.util.Comparator;
import java.util.Iterator;

/**
 *  Public Base interface of all components in the catalog.
 *
 *  @since      2.0
 *  @version    1.0
 */
public interface IComponent
{

    /**
     * Tests if the object implements the requested interface and returns
     * a correctly typified reference otherwise <code>null</code> will be
     * returned.
     *
     * @param   type constant for the requested type {@link ComponentTypes}
     */
    public Object queryInterface(ComponentTypes type);

    /**
     * Returns an iterator for the detail information. The returned instances
     * will have the type {@link IDetail <code>IDetail</code>}. They are
     * sorted by the default sort order.
     *
     * @since   1.0
     * @return  an instance of type <code>IDetail</code>
     * @exception CatalogException error while retrieving the data from the server
     */
    Iterator getDetails() throws CatalogException;

    /**
     * Returns an iterator for the detail information. The returned instances
     * will have the type {@link IDetail <code>IDetail</code>}.
     * They are sorted by the given comparator.
     *
     * @since   1.0
     * @param   aComparator the comparator for sorting the instances
     * @return  an instance of type <code>IDetail</code>
     * @exception CatalogException error while retrieving the data from the server
     */
    Iterator getDetails(Comparator aComparator) throws CatalogException;

    /**
     * Gets a concrete Detail for the given name.
     *
     * @since   1.0
     * @param   aName the name of the requested detail information
     * @return  the requested detail
     * @exception CatalogException error while retrieving the data from the server
     */
    IDetail getDetail(String aName) throws CatalogException;

    /**
     * Adds a listener instance to the list of instances that are interested
     * in component events.
     *
     * @param listener the listener instance to be added
     */
    public void addComponentEventListener(IComponentEventListener listener);

    /**
     * Removes a listener instance from the list of instances that are interested
     * in component events.
     *
     * @param listener the listener instance to be removed
     */
    public void removeComponentEventListener(IComponentEventListener listener);

    /**
     * Gets the state of the component. The possible states are defined
     * via the STATE constants of this interface.
     *
     * @return the state of the node
     */
    public IComponent.State getState();

    /**
     * Enumeration of all states that a catalog component can adopt
     * (typesafe enum idiom).
     *
     * @version     1.0
     */
    public static class State
    {
        private final String theName;

        private State(String name)
        {
            theName = name;
        }

        public String toString()
        {
            return this.theName;
        }

        /**
         * Enumeration constant for the state "loaded" i.e. if it
         * was loaded and the component does not differ to that on the
         * content engine.
         */
        public final static State LOADED = new State("loaded");

        /**
         * Enumeration constant for the state "new" i.e. if it
         * was newly created.
         */
        public final static State NEW = new State("new");

        /**
         * Enumeration constant for the state "changed" i.e. if it
         * was changed.
         */
        public final static State CHANGED = new State("changed");

        /**
         * Enumeration constant for the state "deleted" i.e. if it
         * was deleted.
         */
        public final static State DELETED = new State("deleted");
    }
}
