/******************************************************************************
  Copyright (c) 2000, SAPMarkets Europe GmbH, Germany, All rights reserved.
  Created:      09 July 2001
 
   $Id: //sap/ESALES_base/30_SP_COR/src/_pcatAPI/java/com/sap/isa/catalog/boi/ICatalogIdentity.java#2 $
   $Revision: #2 $
   $Change: 129221 $
   $DateTime: 2003/05/15 17:20:58 $
*******************************************************************************/
package com.sap.isa.catalog.boi;

import java.io.Serializable;
public interface ICatalogIdentity extends Serializable {
}
