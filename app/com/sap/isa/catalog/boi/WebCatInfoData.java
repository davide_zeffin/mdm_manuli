/*****************************************************************************

    Class:        WebCatInfoData
    Copyright (c) 2006, SAP AG, Germany, All rights reserved.
    Created:      02.02.2006

*****************************************************************************/
package com.sap.isa.catalog.boi;

/**
 * Enables the access to the WebCatInfo object from the SolutionConfigurator backend classes. 
 * It also will be enhanced, to be able to create SolutionConfigurator business objects. 
 * This means, the WebCatInfo class will act as a factory for these objects. 
 */
public interface WebCatInfoData {

    /**
     * Creates a <code>WebCatSubItemListData</code> object from a backend class. 
     * 
     * @return a new WebCatSubItemListData instance
     */
    public WebCatSubItemListData createWebCatSubItemListData();

    /**
     * Returns the language of the web catalog.
     * 
     * @return language as string.
     */
    public String getLanguage();
    
    /**
     * Returns the iso language of the web catalog.
     * 
     * @return iso language as string.
     */
    public String getLanguageIso();
}
