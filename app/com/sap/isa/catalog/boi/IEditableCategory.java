/*****************************************************************************
  Copyright (c) 2000, SAPMarkets Europe GmbH, Germany, All rights reserved.
  Created:      01 August 2001

  $Id: //sap/ESALES_base/30_SP_COR/src/_pcatAPI/java/com/sap/isa/catalog/boi/IEditableCategory.java#2 $
  $Revision: #2 $
  $Change: 129221 $
  $DateTime: 2003/05/15 17:20:58 $
*****************************************************************************/
package com.sap.isa.catalog.boi;



/**
 * Extends {link@ ICategory ICategory} interface so that the category can
 * be edited.
 *
 * @since       2.0
 * @version     1.0
 */
public interface IEditableCategory extends ICategory, IEditableCompositeComponent
{
    /**
     * Returns the referenc to the thing we are editing via this.
     * Might be null in case this is new.
     *
     * @return aRef to the object we are about to edit.
     */
     ICategory getCategoryReference();

    /**
     * Creates a new attribute with the given guid.
     *
     * @since 2.0
     * @param aGuid the guid of the new attribute
     * @return the new attribute instance
     * @exception CatalogException error while creating the attribute
     */
    IEditableAttribute createAttribute(String aGuid) throws CatalogException;

    /**
     * Deletes an attribute instance.
     *
     * @since 2.0
     * @param aAttribute the attribute to be deleted
     * @exception CatalogException error while deleting the attribute
     */
    void deleteAttribute(IEditableAttribute aAttribute) throws CatalogException;

    /**
     * Creates a new child category with the given guid.
     *
     * @since 2.0
     * @param aGuid the guid of the child category
     * @return the new category instance
     * @exception CatalogException error while creating the category
     */
    IEditableCategory createChild(String aGuid) throws CatalogException;

    /**
     * Deletes a child category instance.
     *
     * @since 2.0
     * @param aCategory the category to be deleted
     * @exception CatalogException error while deleting the category
     */
    void deleteChild(IEditableCategory aCategory) throws CatalogException;

    /**
     * Creates a new item with the given guid.
     *
     * @since 2.0
     * @param aGuid the guid of the new item
     * @return the new item instance
     * @exception CatalogException error while creating the item
     */
    IEditableItem createItem(String itemGuid) throws CatalogException;

    /**
     * Deletes a item instance.
     *
     * @since 2.0
     * @param aItem the item to be deleted
     * @exception CatalogException error while deleting the item
     */
    void deleteItem(IEditableItem aItem) throws CatalogException;

    /**
     * Sets the language dependent name of the category.
     *
     * @since 2.0
     * @param aName the new name of the category
     * @exception CatalogException error while setting the name
     */
    void setName(String aName) throws CatalogException;

    /**
     * Sets the language dependent long description of the category.
     *
     * @since 2.0
     * @param aDescription the new description of the category
     * @exception CatalogException error while setting the description
     */
    void setDescription(String aDescription) throws CatalogException;

    /**
     * Creates a new Detail with the given name.
     *
     * @since 2.0
     * @param aName the name of the new detail
     * @return the new detail instance
     * @exception CatalogException error while creating the detail
     */
    IEditableDetail createDetail(String aName) throws CatalogException;

    /**
     * Deletes a Detail instance.
     *
     * @since 2.0
     * @param aDetail the detail to be deleted
     * @exception CatalogException error while deleting the detail
     */
    void deleteDetail(IEditableDetail aDetail) throws CatalogException;

}
