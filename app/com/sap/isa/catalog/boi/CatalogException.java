/*****************************************************************************
  Copyright (c) 2000, SAPMarkets Europe GmbH, Germany, All rights reserved.

  $Id: //sap/ESALES_base/30_SP_COR/src/_pcatAPI/java/com/sap/isa/catalog/boi/CatalogException.java#2 $
  $Revision: #2 $
  $Change: 126527 $
  $DateTime: 2003/04/28 13:43:12 $
*****************************************************************************/
package com.sap.isa.catalog.boi;

import java.io.PrintStream;
import java.io.PrintWriter;

import com.sap.isa.core.logging.IsaLocation;

/**
 * Error during accessing catalog data via the API.
 *
 * @version     1.0
 * @since       1.0
 */
public class CatalogException extends Exception
{
    private Throwable theCause;
    static IsaLocation theStaticLocToLog = IsaLocation.getInstance(CatalogException.class.getName());
    
    public CatalogException(String msg)
    {
        super(msg);
        if(theStaticLocToLog.isDebugEnabled()) {
            theStaticLocToLog.debug("new CatalogException(" + msg + ")");
        }
    }

    public CatalogException(Throwable cause)
    {
        super();
        theCause=cause;
        if(theStaticLocToLog.isDebugEnabled()) {
            theStaticLocToLog.debug("new CatalogException() because " + cause.getMessage());
        }
    }

    public CatalogException(String msg, Throwable cause)
    {
        super(msg);
        theCause = cause;
        if(theStaticLocToLog.isDebugEnabled()) {
            theStaticLocToLog.debug("new CatalogException(" + msg + ") because " + cause.getMessage());
        }
    }

    public Throwable getCause()
    {
        return theCause;
    }
    
    public String getMessage()
    {
        StringBuffer aBuffer = new StringBuffer();
        if (theCause!=null) 
        {
            aBuffer.append(theCause.getMessage());
            String aSuperMessage = super.getMessage();
            if(aSuperMessage!=null)
            {
            	aBuffer.append("\n ");
            	aBuffer.append(aSuperMessage);            
            }
        } // end of if ()
        else 
        {
            aBuffer.append(super.getMessage());
        } // end of else
        return aBuffer.toString();        
    }
    
        
    public   void printStackTrace()
    {
        if (theCause!=null) 
        {
            theCause.printStackTrace();
        } // end of if ()
    	super.printStackTrace();
        return;        
    }
    
    public  void printStackTrace(PrintStream s)
    {
        if (theCause!=null) 
        {
            theCause.printStackTrace(s);
        } // end of if ()
        super.printStackTrace(s);
        return;         
    }

    public void printStackTrace(PrintWriter s)
    {
        if (theCause!=null) 
        {
            theCause.printStackTrace(s);
        } // end of if ()
        super.printStackTrace(s);
        return;                
    }
}
