/*****************************************************************************
  Copyright (c) 2000, SAPMarkets Europe GmbH, Germany, All rights reserved.

  $Id: //sap/ESALES_base/30_SP_COR/src/_pcatAPI/java/com/sap/isa/catalog/boi/ICatalogBackUpStrategy.java#4 $
  $Revision: #4 $
  $Change: 144303 $
  $DateTime: 2003/08/22 20:18:21 $
*****************************************************************************/
package com.sap.isa.catalog.boi;

import java.net.URL;

/**
 * ICatalogBackUpStrategy.java
 *
 * @version 1.0
 */

public interface ICatalogBackUpStrategy 
{
    URL backUp() throws CatalogException;
}// ICatalogBackUpStrategy
