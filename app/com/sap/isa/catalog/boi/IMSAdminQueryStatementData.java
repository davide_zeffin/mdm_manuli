/*
 * Created on 22.11.2006
 *
 */
package com.sap.isa.catalog.boi;

/**
 * @author d025715
 *
 */
public interface IMSAdminQueryStatementData extends IQueryStatement {

    public String[] getAttributes();
    
    public String[] getSortOrder();
    
    public int getRangeFrom();
    
    public int getRangeTo();
    
}
