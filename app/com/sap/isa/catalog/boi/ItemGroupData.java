/*****************************************************************************

    Class:        ItemGroupData
    Copyright (c) 2006, SAP AG, Germany, All rights reserved.
    Created:      26.04.2006

*****************************************************************************/
package com.sap.isa.catalog.boi;

/**
 * Data interface for <code>ItemFroup</code>.<br>
 * 
 * Enables the access to the ItemGroup from the backend classes, 
 * e.g. the SolutionConfiguratorBackend.
 */
public interface ItemGroupData {
    
    /**
     * Returns true if the group should be displayed in collapsed mode
     * 
     * @return true if the group should be displayed in collapsed mode
     *         false otherwise
     */
    public boolean displayCollapsed();
    
    public void setDisplayCollapsed(boolean displayCollapsed);

    /**
     * Sets the groupId.
     * 
     * @param groupId the groupId
     */
    public void setGroupId(String groupId);

    /**
     * Sets the groupText.
     * 
     * @param String the groupText
     */
    public void setGroupText(String groupText);
}