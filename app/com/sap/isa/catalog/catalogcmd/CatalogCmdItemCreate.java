/*****************************************************************************
  Copyright (c) 2000, SAPMarkets Europe GmbH, Germany, All rights reserved.
  Created:      16 July 2001

  $Id: //java/sapm/esales/dev/src/com/sap/isa/catalog/catalogcmd/CatalogCmdItemCreate.java#5 $
  $Revision: #5 $
  $Change: 26550 $
  $DateTime: 2001/07/31 13:24:42 $
*****************************************************************************/

package com.sap.isa.catalog.catalogcmd;

// catalog imports
import com.sap.isa.catalog.boi.CatalogException;
import com.sap.isa.catalog.boi.IComponent;
import com.sap.isa.catalog.editor.EditableCategory;
import com.sap.isa.catalog.editor.EditableItem;
import com.sap.isa.catalog.impl.CatalogComponentTypes;
import com.sap.isa.catalog.impl.CatalogCompositeComponent;

/**
 *  Command to create an item in the catalog tree.
 *
 *  @version     1.0
 *  @since       2.0
 */
class CatalogCmdItemCreate extends CatalogCmd
{
    private EditableItem theItem;
    private CatalogCompositeComponent theComponent;
    private String theGuid;
    private IComponent.State theOldState;

    /**
     * Constructor.
     *
     * @param parent the component for which an item should be created
     * @param guid   the guid of the new item
     */
    protected CatalogCmdItemCreate( CatalogCompositeComponent parent,
                                    String guid)
    {
        theComponent = parent;
        theGuid = guid;
    }

    protected boolean undoInternal() throws CatalogException
    {
        synchronized(theComponent)
        {
            synchronized(theItem)
            {
                theComponent.deleteItemInternal(theItem);
                theComponent.setState(theOldState);
                theItem.setStateDeleted();
            }
        }
        return true;
    }

    protected boolean executeInternal() throws CatalogException
    {
        boolean success = false;

        synchronized(theComponent)
        {
            theOldState = theComponent.getState();
            EditableCategory category =
                (EditableCategory)theComponent.queryInterface(
                    CatalogComponentTypes.EDITABLE_CATEGORY);
            if(category != null)
            {
                success = theComponent.setStateChanged();
                if(success)
                    theItem = category.createEditableItemInternal(theGuid);
            }
        }
        return success;
    }
}
