/*****************************************************************************
  Copyright (c) 2000, SAPMarkets Europe GmbH, Germany, All rights reserved.
  Created:      16 July 2001

  $Id: //sap/ESALES_base/30_SP_COR/src/_pcatAPI/java/com/sap/isa/catalog/catalogcmd/CatalogCmdComponentMove.java#3 $
  $Revision: #3 $
  $Change: 150872 $
  $DateTime: 2003/09/29 09:18:51 $
*****************************************************************************/

package com.sap.isa.catalog.catalogcmd;

// catalog imports
import com.sap.isa.catalog.boi.CatalogException;
import com.sap.isa.catalog.boi.IComponent;
import com.sap.isa.catalog.impl.CatalogComponentTypes;
import com.sap.isa.catalog.impl.CatalogCompositeComponent;
import com.sap.isa.catalog.impl.CatalogHierarchicalComponent;

/**
 *  Command to move a component of the catalog tree.
 *
 *  @version     1.0
 *  @since       2.0
 */
class CatalogCmdComponentMove extends CatalogCmd
{
    private CatalogHierarchicalComponent theSource;
    private CatalogCompositeComponent theNewParent;
    private CatalogCompositeComponent theOldParent;
    private IComponent.State theOldParentState;
    private IComponent.State theNewParentState;

    /**
     * Constructor.
     *
     * @param src       the component that is to be moved
     * @param newParent the new parent node of this component
     */
    protected CatalogCmdComponentMove(  CatalogHierarchicalComponent src,
                                        CatalogCompositeComponent newParent)
    {
        theSource = src;
        theNewParent = newParent;
    }

    protected boolean undoInternal() throws CatalogException
    {
        // very heavy synchronization - but there is no other way
        synchronized(theOldParent)
        {
            synchronized(theNewParent)
            {
                theOldParent.setState(theOldParentState);
                theNewParent.setState(theNewParentState);
                theNewParent.deleteChildComponent(theSource);
                theOldParent.addChildComponent(theSource);
            }
        }
        return true;
    }

    protected boolean executeInternal() throws CatalogException
    {
        boolean success = false;

        // check if execution is possible
        if(theSource == theNewParent)
            return false;

        if(theSource.queryInterface(CatalogComponentTypes.CATALOG) != null)
            return false;

        // very heavy synchronization - but there is no other way
        theOldParent = theSource.getParentComponent();
        synchronized(theOldParent)
        {
            synchronized(theNewParent)
            {
                theNewParentState = theNewParent.getState();
                theOldParentState = theOldParent.getState();

                success = theNewParent.setStateChanged();
                if(success)
                   success = theOldParent.setStateChanged();

                if(!success)
                    theNewParent.setState(theNewParentState);

                if(success)
                {
                    theOldParent.deleteChildComponent(theSource);
                    theNewParent.addChildComponent(theSource);
                }
            }
        }

        return success;
    }
}
