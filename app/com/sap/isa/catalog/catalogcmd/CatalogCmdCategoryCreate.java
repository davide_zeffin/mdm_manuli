/*****************************************************************************
  Copyright (c) 2000, SAPMarkets Europe GmbH, Germany, All rights reserved.
  Created:      16 July 2001

  $Id: //java/sapm/esales/dev/src/com/sap/isa/catalog/catalogcmd/CatalogCmdCategoryCreate.java#6 $
  $Revision: #6 $
  $Change: 26550 $
  $DateTime: 2001/07/31 13:24:42 $
*****************************************************************************/

package com.sap.isa.catalog.catalogcmd;

// catalog imports
import com.sap.isa.catalog.boi.CatalogException;
import com.sap.isa.catalog.boi.IComponent;
import com.sap.isa.catalog.editor.EditableCatalog;
import com.sap.isa.catalog.editor.EditableCategory;
import com.sap.isa.catalog.impl.CatalogComponentTypes;
import com.sap.isa.catalog.impl.CatalogCompositeComponent;

/**
 *  Command to create a children category in the catalog tree.
 *
 *  @version     1.0
 *  @since       2.0
 */
class CatalogCmdCategoryCreate extends CatalogCmd
{
    private CatalogCompositeComponent theComponent;
    private String theGuid;
    private EditableCategory theCategory;
    private IComponent.State theOldState;

    /**
     * Constructor.
     *
     * @param parent the component for which the category should be created
     * @param guid   the guid of the category that should be created
     */
    protected CatalogCmdCategoryCreate( CatalogCompositeComponent parent,
                                        String guid)
    {
        theComponent = parent;
        theGuid = guid;
    }

    protected boolean undoInternal() throws CatalogException
    {
        synchronized(theComponent)
        {
            synchronized(theCategory)
            {
                theComponent.deleteChildCategoryInternal(theCategory);
                theComponent.setState(theOldState);
                theCategory.setStateDeleted();
            }
        }
        return true;
    }

    protected boolean executeInternal() throws CatalogException
    {
        boolean success = false;

        synchronized(theComponent)
        {
            theOldState = theComponent.getState();
            EditableCatalog catalog =
                (EditableCatalog)theComponent.queryInterface(
                    CatalogComponentTypes.EDITABLE_CATALOG);
            EditableCategory category =
                (EditableCategory)theComponent.queryInterface(
                    CatalogComponentTypes.EDITABLE_CATEGORY);

            if(catalog != null || category != null)
                success = theComponent.setStateChanged();

            if(success)
            {
                if(catalog != null)
                    theCategory = catalog.createEditableCategoryInternal(theGuid);

                if(category != null)
                    theCategory = category.createEditableCategoryInternal(theGuid);
            }
        }
        return success;
    }
}
