/*****************************************************************************
  Copyright (c) 2000, SAPMarkets Europe GmbH, Germany, All rights reserved.
  Created:      16 July 2001

  $Id: //java/sapm/esales/dev/src/com/sap/isa/catalog/catalogcmd/CatalogCmdDetailCreate.java#5 $
  $Revision: #5 $
  $Change: 26550 $
  $DateTime: 2001/07/31 13:24:42 $
*****************************************************************************/

package com.sap.isa.catalog.catalogcmd;

// catalog imports
import com.sap.isa.catalog.boi.CatalogException;
import com.sap.isa.catalog.boi.IComponent;
import com.sap.isa.catalog.editor.EditableAttribute;
import com.sap.isa.catalog.editor.EditableCatalog;
import com.sap.isa.catalog.editor.EditableCategory;
import com.sap.isa.catalog.editor.EditableDetail;
import com.sap.isa.catalog.impl.CatalogComponent;
import com.sap.isa.catalog.impl.CatalogComponentTypes;

/**
 *  Command to create a detail in the catalog tree.
 *
 *  @version     1.0
 *  @since       2.0
 */
class CatalogCmdDetailCreate extends CatalogCmd
{
    private CatalogComponent theComponent;
    private EditableDetail theDetail;
    private String theName;
    private IComponent.State theOldState;

    /**
     * Constructor.
     *
     * @param parent the component for which the detail should be created
     * @param name   the name of the detail
     */
    protected CatalogCmdDetailCreate(CatalogComponent parent, String name)
    {
        theComponent = parent;
        theName = name;
    }

    protected boolean undoInternal() throws CatalogException
    {
        synchronized(theComponent)
        {
            EditableCatalog cat =
                (EditableCatalog)theComponent.queryInterface(
                    CatalogComponentTypes.EDITABLE_CATALOG);
            if(cat != null)
            {
                cat.deleteDetailInternal(theDetail);
                cat.setState(theOldState);
                theDetail.setStateDeleted();
                return true;
            }

            EditableCategory category =
                (EditableCategory)theComponent.queryInterface(
                    CatalogComponentTypes.EDITABLE_CATEGORY);
            if(category != null)
            {
                category.deleteDetailInternal(theDetail);
                category.setState(theOldState);
                theDetail.setStateDeleted();
                return true;
            }

            EditableAttribute attr =
                (EditableAttribute)theComponent.queryInterface(
                    CatalogComponentTypes.EDITABLE_ATTRIBUTE);
            if(attr != null)
            {
                attr.deleteDetailInternal(theDetail);
                attr.setState(theOldState);
                theDetail.setStateDeleted();
                return true;
            }
        }
        return false;
    }

    protected boolean executeInternal() throws CatalogException
    {
        boolean success = false;

        synchronized(theComponent)
        {
            theOldState = theComponent.getState();
            success = theComponent.setStateChanged();
            if(success)
            {
                EditableCatalog cat =
                    (EditableCatalog)theComponent.queryInterface(
                        CatalogComponentTypes.EDITABLE_CATALOG);
                if(cat != null)
                {
                    theDetail = cat.createEditableDetailInternal(theName);
                    return true;
                }

                EditableCategory category =
                    (EditableCategory)theComponent.queryInterface(
                        CatalogComponentTypes.EDITABLE_CATEGORY);
                if(category != null)
                {
                    theDetail = category.createEditableDetailInternal(theName);
                    return true;
                }

                EditableAttribute attr =
                    (EditableAttribute)theComponent.queryInterface(
                        CatalogComponentTypes.EDITABLE_ATTRIBUTE);
                if(attr != null)
                {
                    theDetail = attr.createEditableDetailInternal(theName);
                    return true;
                }
            }
        }
        return success;
    }
}
