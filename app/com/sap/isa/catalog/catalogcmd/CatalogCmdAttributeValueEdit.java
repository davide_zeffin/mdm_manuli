/*****************************************************************************
  Copyright (c) 2000, SAPMarkets Europe GmbH, Germany, All rights reserved.
  Created:      16 July 2001

  $Id: //sap/ESALES_base/30_SP_COR/src/_pcatAPI/java/com/sap/isa/catalog/catalogcmd/CatalogCmdAttributeValueEdit.java#2 $
  $Revision: #2 $
  $Change: 129221 $
  $DateTime: 2003/05/15 17:20:58 $
*****************************************************************************/

package com.sap.isa.catalog.catalogcmd;

// catalog imports
import java.util.Iterator;

import com.sap.isa.catalog.boi.CatalogException;
import com.sap.isa.catalog.boi.IComponent;
import com.sap.isa.catalog.editor.EditableAttributeValue;

/**
 *  Command to edit an attribute value in the catalog tree.
 *
 *  @version     1.0
 *  @since       2.0
 */
class CatalogCmdAttributeValueEdit extends CatalogCmd
{
    private EditableAttributeValue theComponent;
    private Object theNewValue;
    private Object theOldValue;
    private IComponent.State theOldState;
    private Iterator theOldValuesIter;
    private boolean theAddFlag;

    /**
     * Constructor.
     *
     * @param attrValue the component of which the value should be changed
     * @param value     the value to be set
     * @param add       boolean that indicates if the value should be added or set
     */
    protected CatalogCmdAttributeValueEdit(EditableAttributeValue attrValue,
                                           Object value, boolean add)
    {
        theComponent = attrValue;
        theNewValue = value;
        theAddFlag = add;
    }

    protected boolean undoInternal() throws CatalogException
    {
        synchronized(theComponent)
        {
            theComponent.setAsObject(null);
            while(theOldValuesIter.hasNext())
            {
                Object obj = theOldValuesIter.next();
                if(theAddFlag)
                    theComponent.addAsObject(obj);
                else
                    theComponent.setAsObject(obj);
            }
            theComponent.setState(theOldState);
        }
        return true;
    }

    protected boolean executeInternal() throws CatalogException
    {
        boolean success = false;

        synchronized(theComponent)
        {
            theOldState = theComponent.getState();

            if(theAddFlag && !theComponent.isMultiValue())
                success = false;
            else
                success = theComponent.setStateChanged();

            if(success)
            {
                theOldValuesIter = theComponent.getAllValues();
                if(theAddFlag)
                    theComponent.addAsObjectInternal(theNewValue);
                else
                    theComponent.setAsObjectInternal(theNewValue);
            }
        }
        return success;
    }
}
