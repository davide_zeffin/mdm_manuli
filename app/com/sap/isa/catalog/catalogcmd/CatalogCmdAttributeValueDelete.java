/*****************************************************************************
  Copyright (c) 2000, SAPMarkets Europe GmbH, Germany, All rights reserved.
  Created:      16 July 2001

  $Id: //sap/ESALES_base/30_SP_COR/src/_pcatAPI/java/com/sap/isa/catalog/catalogcmd/CatalogCmdAttributeValueDelete.java#2 $
  $Revision: #2 $
  $Change: 129221 $
  $DateTime: 2003/05/15 17:20:58 $
*****************************************************************************/

package com.sap.isa.catalog.catalogcmd;

// catalog imports
import com.sap.isa.catalog.boi.CatalogException;
import com.sap.isa.catalog.boi.IComponent;
import com.sap.isa.catalog.editor.EditableAttributeValue;
import com.sap.isa.catalog.editor.EditableItem;

/**
 *  Command to delete an attribute value in the catalog tree.
 *
 *  @version     1.0
 *  @since       2.0
 */
class CatalogCmdAttributeValueDelete extends CatalogCmd
{
    private EditableItem theComponent;
    private EditableAttributeValue theValue;
    private IComponent.State theOldValueState;
    private IComponent.State theOldCompState;

    /**
     * Constructor.
     *
     * @param parent    the component of which the attribute value should be deleted
     * @param attribute the attribute to be deleted
     */
    protected CatalogCmdAttributeValueDelete(EditableItem parent,
                                             EditableAttributeValue value)
    {
        theComponent = parent;
        theValue     = value;
    }

    protected boolean undoInternal() throws CatalogException
    {
        synchronized(theComponent)
        {
            synchronized(theValue)
            {
                theComponent.addAttributeValueInternal(theValue,true);
                theComponent.setState(theOldCompState);
                theValue.setState(theOldValueState);
            }
        }
        return true;
    }

    protected boolean executeInternal() throws CatalogException
    {
        boolean success = false;

        synchronized(theComponent)
        {
            theOldCompState = theComponent.getState();
            synchronized(theValue)
            {
                theOldValueState = theValue.getState();
                success = theComponent.setStateChanged();
                if(success)
                {
                    theComponent.deleteAttributeValueInternal(theValue);
                    theValue.setStateDeleted();
                }
            }
        }
        return success;
    }
}
