/*****************************************************************************
  Copyright (c) 2000, SAPMarkets Europe GmbH, Germany, All rights reserved.
  Created:      17 July 2001

  $Id: //sap/ESALES_base/30_SP_COR/src/_pcatAPI/java/com/sap/isa/catalog/catalogcmd/CatalogCmdDetailEdit.java#2 $
  $Revision: #2 $
  $Change: 129221 $
  $DateTime: 2003/05/15 17:20:58 $
*****************************************************************************/
package com.sap.isa.catalog.catalogcmd;

import com.sap.isa.catalog.boi.CatalogException;
import com.sap.isa.catalog.boi.IComponent;
import com.sap.isa.catalog.impl.CatalogDetail;

/**
 *  Command to edit a detail in the catalog tree.
 *
 *  @version     1.0
 *  @since       2.0
 */
class CatalogCmdDetailEdit extends CatalogCmd
{
    private CatalogDetail theComponent;
    private String theNewName;
    private String theOldName;
    private String theNewValue;
    private String[] theOldValues;
    private IComponent.State theOldState;
    private boolean theAddFlag;

    /**
     * Constructor.
     *
     * @param  detail the detail to be edited
     * @param  name   the new name of the detail
     * @param  value  the new value of the detail
     * @param  add    boolean that indicates if the value should be added or set
     */
    protected CatalogCmdDetailEdit( CatalogDetail detail,
                                    String name,
                                    String value,
                                    boolean add)
    {
        theComponent = detail;
        theNewName = name;
        theNewValue = value;
        theAddFlag = add;
    }

    protected boolean undoInternal() throws CatalogException
    {
        synchronized(theComponent)
        {
            if(theNewName != null)
                theComponent.setNameInternal(theOldName);
            if(theNewValue != null)
                theComponent.setAsStringInternal(theOldValues);
            theComponent.setState(theOldState);
        }
        return true;
    }

    protected boolean executeInternal() throws CatalogException
    {
        boolean success = false;

        synchronized(theComponent)
        {
            theOldState = theComponent.getState();
            success = theComponent.setStateChanged();
            if(success)
            {
                // store the old settings
                theOldName  = theComponent.getName();
                theOldValues = theComponent.getAllAsString();

                // set the new settings
                if(theNewName != null)
                    theComponent.setNameInternal(theNewName);
                if(theNewValue != null)
                {
                    if(theAddFlag)
                        theComponent.addAsStringInternal(theNewValue);
                    else
                        theComponent.setAsStringInternal(theNewValue);
                }
            }
        }
        return success;
    }
}
