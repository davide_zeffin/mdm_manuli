/*****************************************************************************
  Copyright (c) 2000, SAPMarkets Europe GmbH, Germany, All rights reserved.
  Created:      16 July 2001

  $Id: //sap/ESALES_base/30_SP_COR/src/_pcatAPI/java/com/sap/isa/catalog/catalogcmd/CatalogCmdItemEdit.java#2 $
  $Revision: #2 $
  $Change: 129221 $
  $DateTime: 2003/05/15 17:20:58 $
*****************************************************************************/

package com.sap.isa.catalog.catalogcmd;

// catalog imports
import com.sap.isa.catalog.boi.CatalogException;
import com.sap.isa.catalog.boi.IComponent;
import com.sap.isa.catalog.editor.EditableItem;

/**
 *  Command to edit an item in the catalog tree.
 *
 *  @version     1.0
 *  @since       2.0
 */
class CatalogCmdItemEdit extends CatalogCmd
{
    private EditableItem theComponent;
    private String theNewValue;
    private String theOldValue;
    private int theProperty;
    private IComponent.State theOldState;

    /**
     * Constructor.
     *
     * @param item the item to be edited
     * @param value    the new value of the property to be changed
     * @param property the property to be set (constants see {@link CatalogCmd})
     */
    protected CatalogCmdItemEdit(EditableItem item, String value, int property)
    {
        theComponent = item;
        theNewValue = value;
        theProperty = property;
    }

    protected boolean undoInternal() throws CatalogException
    {
        synchronized(theComponent)
        {
            switch(theProperty)
            {
                case PROP_NAME:
                    theComponent.setName(theOldValue);
                    break;
                case PROP_PRODUCT_GUID:
                    theComponent.setProductGuid(theOldValue);
                    break;
            }
            theComponent.setState(theOldState);
        }
        return true;
    }

    protected boolean executeInternal() throws CatalogException
    {
        boolean success = false;

        synchronized(theComponent)
        {
            theOldState = theComponent.getState();
            success = theComponent.setStateChanged();
            if(success)
            {
                switch(theProperty)
                {
                    case PROP_NAME:
                        theOldValue = theComponent.getName();
                        theComponent.setNameInternal(theNewValue);
                        break;
                    case PROP_PRODUCT_GUID:
                        theOldValue = theComponent.getProductGuid();
                        theComponent.setProductGuidInternal(theNewValue);
                        break;
                    default:
                        theComponent.setState(theOldState);
                        success = false;
                        break;
                }
            }
        }
        return success;
    }
}
