/*****************************************************************************
  Copyright (c) 2000, SAPMarkets Europe GmbH, Germany, All rights reserved.
  Created:      16 July 2001

  $Id: //java/sapm/esales/dev/src/com/sap/isa/catalog/catalogcmd/CatalogCmdAttributeCreate.java#6 $
  $Revision: #6 $
  $Change: 26550 $
  $DateTime: 2001/07/31 13:24:42 $
*****************************************************************************/

package com.sap.isa.catalog.catalogcmd;

// catalog imports
import java.util.Iterator;

import com.sap.isa.catalog.boi.CatalogException;
import com.sap.isa.catalog.boi.IComponent;
import com.sap.isa.catalog.editor.EditableAttribute;
import com.sap.isa.catalog.editor.EditableCatalog;
import com.sap.isa.catalog.editor.EditableCategory;
import com.sap.isa.catalog.impl.Catalog;
import com.sap.isa.catalog.impl.CatalogComponent;
import com.sap.isa.catalog.impl.CatalogComponentTypes;
import com.sap.isa.catalog.impl.CatalogCompositeComponent;

/**
 *  Command to create an attribute in the catalog tree.
 *
 *  @since       2.0
 *  @version     1.0
 */
class CatalogCmdAttributeCreate extends CatalogCmd
{
    private CatalogCompositeComponent theComponent;
    private String theGuid;
    private EditableAttribute theAttribute;
    private IComponent.State theOldState;

    /**
     * Construtor.
     *
     * @param parent     the component for which the attribute should be created
     * @param guid       the guid of the attribute
     */
    protected CatalogCmdAttributeCreate(CatalogCompositeComponent parent,
                                        String aGuid)
    {
        theComponent = parent;
        theGuid      = aGuid;
    }

    protected boolean undoInternal() throws CatalogException
    {
        synchronized(theComponent)
        {
            synchronized(theAttribute)
            {
                theComponent.deleteAttributeInternal(theAttribute);
                theComponent.setState(theOldState);
                theAttribute.setStateDeleted();
            }
        }
        return true;
    }

    protected boolean executeInternal() throws CatalogException
    {
        synchronized(theComponent)
        {
            // check if an attribute with the given guid already exists
            Catalog root = theComponent.getRoot();
            Iterator iter = root.iterator();
            while(iter.hasNext())
            {
                CatalogComponent elem = (CatalogComponent)iter.next();
                CatalogCompositeComponent comp =
                    (CatalogCompositeComponent)elem.queryInterface(CatalogComponentTypes.COMPOSITE_COMPONENT);
                if(comp != null)
                {
                    if(comp.getAttributeInternal(theGuid) != null)
                        return false;
                }
            }

            theOldState = theComponent.getState();
            boolean success = theComponent.setStateChanged();
            if(success)
            {
                EditableCatalog catalog =
                    (EditableCatalog)theComponent.queryInterface(
                        CatalogComponentTypes.EDITABLE_CATALOG);
                if(catalog != null)
                    theAttribute = catalog.createEditableAttributeInternal(theGuid);
                else
                {
                    EditableCategory category =
                        (EditableCategory)theComponent.queryInterface(
                            CatalogComponentTypes.EDITABLE_CATEGORY);
                    if(category != null)
                        theAttribute = category.createEditableAttributeInternal(theGuid);
                }
            }
            return success;
        }
    }
}
