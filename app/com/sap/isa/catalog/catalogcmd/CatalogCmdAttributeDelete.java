/*****************************************************************************
  Copyright (c) 2000, SAPMarkets Europe GmbH, Germany, All rights reserved.
  Created:      09 July 2001

  $Id: //sap/ESALES_base/30_SP_COR/src/_pcatAPI/java/com/sap/isa/catalog/catalogcmd/CatalogCmdAttributeDelete.java#2 $
  $Revision: #2 $
  $Change: 129221 $
  $DateTime: 2003/05/15 17:20:58 $
*****************************************************************************/

package com.sap.isa.catalog.catalogcmd;

// catalog imports
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import com.sap.isa.catalog.boi.CatalogException;
import com.sap.isa.catalog.boi.IComponent;
import com.sap.isa.catalog.editor.EditableAttribute;
import com.sap.isa.catalog.impl.Catalog;
import com.sap.isa.catalog.impl.CatalogAttributeValue;
import com.sap.isa.catalog.impl.CatalogComponent;
import com.sap.isa.catalog.impl.CatalogComponentTypes;
import com.sap.isa.catalog.impl.CatalogCompositeComponent;
import com.sap.isa.catalog.impl.CatalogItem;

/**
 *  Command to delete an attribute in the catalog tree.
 *
 *  @version     1.0
 *  @since       2.0
 */
class CatalogCmdAttributeDelete extends CatalogCmd
{
    private CatalogCompositeComponent theComponent;
    private EditableAttribute theAttribute;
    private List theDeleteCmds = new ArrayList();
    private IComponent.State theOldCompState;
    private IComponent.State theOldAttributeState;

    /**
     * Constructor.
     *
     * @param parent    the component of which the attribute should be deleted
     * @param attribute the attribute to be deleted
     */
    protected CatalogCmdAttributeDelete(CatalogCompositeComponent parent,
                                        EditableAttribute attribute)
    {
        theComponent = parent;
        theAttribute = attribute;
    }

    protected boolean undoInternal() throws CatalogException
    {
        synchronized(theComponent)
        {
            synchronized(theAttribute)
            {
                theComponent.setState(theOldCompState);
                theAttribute.setState(theOldAttributeState);
                theComponent.addAttributeInternal(theAttribute);
            }
        }
        //Iterator iter = theDeleteCmds.iterator();
        //while(iter.hasNext())
        //{
        //    CatalogCmd cmd = (CatalogCmd)iter.next();
        //    cmd.undo();
        //}
        //theDeleteCmds.clear();
        return true;
    }

    protected boolean executeInternal() throws CatalogException
    {
        boolean success = true;

        if(success)
        {
            // now delete the attribute
            theOldAttributeState = theAttribute.getState();
            synchronized(theComponent)
            {
                synchronized(theAttribute)
                {
                    theOldCompState = theComponent.getState();
                    success = theComponent.setStateChanged();
                    if(success)
                    {
                        theAttribute.setStateDeleted();
                        theComponent.deleteAttributeInternal(theAttribute);
                    }
                }
            }
        }

        return success;
    }

    protected boolean doBeforeSave() throws CatalogException
    {
        boolean success = true;
        CatalogAttributeValue value = null;

        // search all attribute values for this attribute and delete them first
        Catalog root = theComponent.getRoot();
        Iterator iter = root.iterator();
        while(iter.hasNext() && success)
        {
            CatalogComponent comp = (CatalogComponent)iter.next();
            CatalogItem item =
                (CatalogItem)comp.queryInterface(CatalogComponentTypes.CATALOG_ITEM);
            if(item == null)
            {
                item =
                    (CatalogItem)comp.queryInterface(CatalogComponentTypes.CATALOG_QUERY_ITEM);
            }
            if(item != null)
            {
                value = (CatalogAttributeValue)item.getAttributeValue(theAttribute.getGuid());
            }
            if(value != null)
            {
//                CatalogCmd cmd =
//                    item.getRoot().getCmdProcessor().createCmdAttributeValueDelete(item, value);
                 // not via the command processor since these instances should
                 // not be done in the history
//                success = cmd.execute();
//                if(success)
//                    theDeleteCmds.add(cmd);
                value = null;
            }
        }

        return success;
    }
}
