/*****************************************************************************
  Copyright (c) 2000, SAPMarkets Europe GmbH, Germany, All rights reserved.
  Created:      16 July 2001

  $Id: //sap/ESALES_base/30_SP_COR/src/_pcatAPI/java/com/sap/isa/catalog/catalogcmd/CatalogCmdDetailDelete.java#2 $
  $Revision: #2 $
  $Change: 129221 $
  $DateTime: 2003/05/15 17:20:58 $
*****************************************************************************/

package com.sap.isa.catalog.catalogcmd;

// catalog imports
import com.sap.isa.catalog.boi.CatalogException;
import com.sap.isa.catalog.boi.IComponent;
import com.sap.isa.catalog.editor.EditableAttribute;
import com.sap.isa.catalog.editor.EditableCatalog;
import com.sap.isa.catalog.editor.EditableCategory;
import com.sap.isa.catalog.editor.EditableDetail;
import com.sap.isa.catalog.impl.CatalogComponent;
import com.sap.isa.catalog.impl.CatalogComponentTypes;

/**
 *  Command to delete a detail in the catalog tree.
 *
 *  @version     1.0
 *  @since       2.0
 */
class CatalogCmdDetailDelete extends CatalogCmd
{
    private CatalogComponent theComponent;
    private EditableDetail theDetail;
    private IComponent.State theOldCompState;
    private IComponent.State theOldDetailState;

    /**
     * Constructor.
     *
     * @param parent the component for which the detail should be deleted
     * @param detail the detail to be deleted
     */
    protected CatalogCmdDetailDelete(CatalogComponent parent, EditableDetail detail)
    {
        theComponent = parent;
        theDetail = detail;
    }

    protected boolean undoInternal() throws CatalogException
    {
        synchronized(theComponent)
        {
            synchronized(theDetail)
            {
                EditableCatalog cat =
                    (EditableCatalog)theComponent.queryInterface(
                        CatalogComponentTypes.EDITABLE_CATALOG);
                if(cat != null)
                {
                    cat.addDetailInternal(theDetail);
                    cat.setState(theOldCompState);
                    theDetail.setState(theOldDetailState);
                    return true;
                }

                EditableCategory category =
                    (EditableCategory)theComponent.queryInterface(
                        CatalogComponentTypes.EDITABLE_CATEGORY);
                if(category != null)
                {
                    category.addDetailInternal(theDetail);
                    category.setState(theOldCompState);
                    theDetail.setState(theOldDetailState);
                    return true;
                }

                EditableAttribute attr =
                    (EditableAttribute)theComponent.queryInterface(
                        CatalogComponentTypes.EDITABLE_ATTRIBUTE);
                if(attr != null)
                {
                    attr.addDetailInternal(theDetail);
                    attr.setState(theOldCompState);
                    theDetail.setState(theOldDetailState);
                    return true;
                }
            }
        }
        return true;
    }

    protected boolean executeInternal() throws CatalogException
    {
        synchronized(theComponent)
        {
            synchronized(theDetail)
            {
                EditableCatalog cat =
                    (EditableCatalog)theComponent.queryInterface(
                        CatalogComponentTypes.EDITABLE_CATALOG);
                if(cat != null)
                {
                    cat.deleteDetailInternal(theDetail);
                    cat.setStateChanged();
                    theDetail.setStateDeleted();
                    return true;
                }

                EditableCategory category =
                    (EditableCategory)theComponent.queryInterface(
                        CatalogComponentTypes.EDITABLE_CATEGORY);
                if(category != null)
                {
                    category.deleteDetailInternal(theDetail);
                    category.setStateChanged();
                    theDetail.setStateDeleted();
                    return true;
                }

                EditableAttribute attr =
                    (EditableAttribute)theComponent.queryInterface(
                        CatalogComponentTypes.EDITABLE_ATTRIBUTE);
                if(attr != null)
                {
                    attr.deleteDetailInternal(theDetail);
                    attr.setStateChanged();
                    theDetail.setStateDeleted();
                    return true;
                }
            }
        }
        return false;
    }
}
