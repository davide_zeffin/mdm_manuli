/*****************************************************************************
  Copyright (c) 2000, SAPMarkets Europe GmbH, Germany, All rights reserved.
  Created:      09 July 2001

  $Id: //sap/ESALES_base/30_SP_COR/src/_pcatAPI/java/com/sap/isa/catalog/catalogcmd/CatalogCmd.java#3 $
  $Revision: #3 $
  $Change: 150872 $
  $DateTime: 2003/09/29 09:18:51 $
*****************************************************************************/
package com.sap.isa.catalog.catalogcmd;

// misc imports
import java.io.Serializable;

import com.sap.isa.catalog.boi.CatalogException;

/**
 *  Abstract base class for all catalog commands. (see GOF command pattern)
 *
 *  @since       2.0
 *  @version     1.0
 */
public abstract class CatalogCmd implements Serializable
{
    boolean isExecuted = false;
    boolean isUndone   = false;

    /**
     * Constant for the edit commands. This constants stand for the NAME
     * of the element that should be edited.
     */
    public final static int PROP_NAME        = 0;

    /**
     * Constant for the edit commands. This constants stand for the DESCRIPTION
     * of the element that should be edited.
     */
    public final static int PROP_DESCRIPTION = 1;

    /**
     * Constant for the edit commands. This constants stand for the TYPE
     * of the element that should be edited.
     */
    public final static int PROP_TYPE        = 2;

    /**
     * Constant for the edit commands. This constants stand for the QUANTITY
     * of the element that should be edited.
     */
    public final static int PROP_QUANTITY    = 3;

    /**
     * Constant for the edit commands. This constants stand for the PRODUCT_GUID
     * of the element that should be edited.
     */
    public final static int PROP_PRODUCT_GUID = 4;

    /**
     * Trys to undo the last command.
     *
     * @return boolean that indicates if command was successfully undone
     * @exception CatalogException error during undoing the command
     */
    protected final boolean undo() throws CatalogException
    {
        if(!isUndone && isExecuted)
        {
            isUndone   = this.undoInternal();
            isExecuted = false;
        }
        return isUndone;
    }

    /**
     * Executes the command.
     *
     * @return boolean that indicates if command was successfully executed
     * @exception CatalogException error during executing the command
     */
    protected final boolean execute() throws CatalogException
    {
        if(!isExecuted)
        {
            isExecuted = this.executeInternal();
            isUndone   = false;
        }
        return isExecuted;
    }

    /**
     * Template method for the undo method. This method has to be
     * redefined by the concrete command class.
     *
     * @return boolean that indicates if command was successfully undone
     * @exception CatalogException error during undoing the command
     */
    protected abstract boolean undoInternal() throws CatalogException;

    /**
     * Template method for the execute method. This method has to be
     * redefined by the concrete command class.
     *
     * @return boolean that indicates if command was successfully executed
     * @exception CatalogException error during executing the command
     */
    protected abstract boolean executeInternal() throws CatalogException;

    /**
     * This method can be redefined by the concrete command class if same
     * actions have to be performed before the changes are submitted to the
     * underlying content engine. The command subclasses which have to be called back
     * have to register themself in their respective factory method of the
     * <code>CatalogCmdProcessor</code>. For performance reasons this
     * (in most case empty) method is not called automatically for all command instances.
     *
     * @return boolean that indicates if the actions could performed successfully
     * @exception CatalogException error during executing the callbacks
     */
    protected boolean doBeforeSave() throws CatalogException
    {
        return true;
    }

    /**
     * This method can be redefined by the concrete command class if same
     * actions have to be performed after the changes were submitted to the
     * underlying content engine. The command subclasses which have to be called back
     * have to register themself in their respective factory method of the
     * <code>CatalogCmdProcessor</code>. For performance reasons this
     * (in most case empty) method is not called automatically for all command instances.
     *
     * @return boolean that indicates if the actions could performed successfully
     * @exception CatalogException error during executing the callbacks
     */
    protected boolean doAfterSave() throws CatalogException
    {
        return true;
    }
}
