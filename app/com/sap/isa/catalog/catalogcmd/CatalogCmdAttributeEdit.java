/*****************************************************************************
  Copyright (c) 2000, SAPMarkets Europe GmbH, Germany, All rights reserved.
  Created:      09 July 2001

  $Id: //sap/ESALES_base/30_SP_COR/src/_pcatAPI/java/com/sap/isa/catalog/catalogcmd/CatalogCmdAttributeEdit.java#2 $
  $Revision: #2 $
  $Change: 129221 $
  $DateTime: 2003/05/15 17:20:58 $
*****************************************************************************/

package com.sap.isa.catalog.catalogcmd;

// catalog imports
import java.util.Iterator;

import com.sap.isa.catalog.boi.CatalogException;
import com.sap.isa.catalog.boi.IComponent;
import com.sap.isa.catalog.editor.EditableAttribute;
import com.sap.isa.catalog.impl.Catalog;
import com.sap.isa.catalog.impl.CatalogComponent;
import com.sap.isa.catalog.impl.CatalogComponentTypes;
import com.sap.isa.catalog.impl.CatalogCompositeComponent;

/**
 *  Command to edit an attribute in the catalog tree.
 *
 *  @version     1.0
 *  @since       2.0
 */
class CatalogCmdAttributeEdit extends CatalogCmd
{
    private EditableAttribute theComponent;
    private Object theNewValue;
    private Object theOldValue;
    private int theProperty;
    private IComponent.State theOldState;

    /**
     * Constructor.
     *
     * @param attribute the attribute that should be edited
     * @param value     the new value of the property
     * @param property  the property to be set (constants see {@link CatalogCmd})
     */
    protected CatalogCmdAttributeEdit(EditableAttribute attribute, Object value, int property)
    {
        theComponent = attribute;
        theNewValue = value;
        theProperty = property;
    }

    protected boolean undoInternal() throws CatalogException
    {
        synchronized(theComponent)
        {
            this.setOldValue();
            theComponent.setState(theOldState);
        }
        return true;
    }

    protected boolean executeInternal() throws CatalogException
    {
        boolean success = false;

        // unfortunately during the whole method call the attribute has to be synchronized!
        synchronized(theComponent)
        {
            // check if an attribute with the given name already exists
            if(theProperty == PROP_NAME)
            {
                String name = (String)theNewValue;
                Catalog root = theComponent.getRoot();
                Iterator iter = root.iterator();
                while(iter.hasNext())
                {
                    CatalogComponent elem = (CatalogComponent)iter.next();
                    CatalogCompositeComponent comp =
                        (CatalogCompositeComponent)elem.queryInterface(CatalogComponentTypes.COMPOSITE_COMPONENT);
                    if(comp != null)
                    {
                        if(comp.getAttributeInternal(name) != null)
                            return false;
                    }
                }
            }

            // change the value
            theOldState = theComponent.getState();
            success = theComponent.setStateChanged();
            if(success)
            {
                success = this.setNewValue();
                if(!success)
                    theComponent.setState(theOldState);
            }
        }

        return success;
    }

    private boolean setNewValue() throws CatalogException
    {
        switch(theProperty)
        {
            case PROP_NAME:
                theOldValue = theComponent.getName();
                theComponent.setNameInternal((String)theNewValue);
                break;
            case PROP_DESCRIPTION:
                theOldValue = theComponent.getDescription();
                theComponent.setDescriptionInternal((String)theNewValue);
                break;
            case PROP_TYPE:
                theOldValue = theComponent.getType();
                theComponent.setTypeInternal((String)theNewValue);
                break;
            case PROP_QUANTITY:
                theOldValue = theComponent.getQuantity();
//                theComponent.setQuantityInternal((String)theNewValue);
                break;
            default:
                return false;
        }

        return true;
    }

    private boolean setOldValue() throws CatalogException
    {
        switch(theProperty)
        {
            case PROP_NAME:
                theComponent.setNameInternal((String)theOldValue);
                break;
            case PROP_DESCRIPTION:
                theComponent.setDescriptionInternal((String)theOldValue);
                break;
            case PROP_TYPE:
                theComponent.setTypeInternal((String)theOldValue);
                break;
            case PROP_QUANTITY:
//                theComponent.setQuantityInternal((String)theOldValue);
                break;
            default:
                return false;
        }

        return true;
    }
}
