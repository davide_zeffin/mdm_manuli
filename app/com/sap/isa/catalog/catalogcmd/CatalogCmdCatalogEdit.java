/*****************************************************************************
  Copyright (c) 2000, SAPMarkets Europe GmbH, Germany, All rights reserved.
  Created:      16 July 2001

  $Id: //sap/ESALES_base/30_SP_COR/src/_pcatAPI/java/com/sap/isa/catalog/catalogcmd/CatalogCmdCatalogEdit.java#2 $
  $Revision: #2 $
  $Change: 129221 $
  $DateTime: 2003/05/15 17:20:58 $
*****************************************************************************/

package com.sap.isa.catalog.catalogcmd;

// catalog imports
import com.sap.isa.catalog.boi.CatalogException;
import com.sap.isa.catalog.boi.IComponent;
import com.sap.isa.catalog.editor.EditableCatalog;

/**
 *  Command to edit the catalog the catalog tree.
 *
 *  @version     1.0
 *  @since       2.0
 */
class CatalogCmdCatalogEdit extends CatalogCmd
{
    private EditableCatalog theComponent;
    private String theNewValue;
    private String theOldValue;
    private int theProperty;
    private IComponent.State theOldState;

    /**
     * Constructor.
     *
     * @param catalog  the catalog that should be edited
     * @param value    the new value of the property to be changed
     * @param property the property to be set (constants see {@link CatalogCmd})
     */
    protected CatalogCmdCatalogEdit(    EditableCatalog catalog,
                                        String value,
                                        int property)
    {
        theComponent = catalog;
        theNewValue = value;
        theProperty = property;
    }

    protected boolean undoInternal() throws CatalogException
    {
        synchronized(theComponent)
        {
            switch(theProperty)
            {
                case PROP_NAME:
                    theComponent.setName(theOldValue);
                    break;
                case PROP_DESCRIPTION:
                    theComponent.setDescription(theOldValue);
                    break;
            }
            theComponent.setState(theOldState);
        }
        return true;
    }

    protected boolean executeInternal() throws CatalogException
    {
        boolean success = false;

        synchronized(theComponent)
        {
            theOldState = theComponent.getState();
            success = theComponent.setStateChanged();
            if(success)
            {
                switch(theProperty)
                {
                    case PROP_NAME:
                        theOldValue = theComponent.getName();
                        theComponent.setName(theNewValue);
                        break;
                    case PROP_DESCRIPTION:
                        theOldValue = theComponent.getDescription();
                        theComponent.setDescription(theNewValue);
                        break;
                    default:
                        theComponent.setState(theOldState);
                        success = false;
                        break;
                }
            }
        }
        return success;
    }
}
