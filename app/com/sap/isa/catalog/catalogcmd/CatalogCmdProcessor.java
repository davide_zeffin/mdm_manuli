/*****************************************************************************
  Copyright (c) 2000, SAPMarkets Europe GmbH, Germany, All rights reserved.
  Created:      09 July 2001

  $Id: //sap/ESALES_base/30_SP_COR/src/_pcatAPI/java/com/sap/isa/catalog/catalogcmd/CatalogCmdProcessor.java#3 $
  $Revision: #3 $
  $Change: 150872 $
  $DateTime: 2003/09/29 09:18:51 $
*****************************************************************************/

package com.sap.isa.catalog.catalogcmd;

// catalog imports
import java.io.Serializable;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

import com.sap.isa.catalog.boi.CatalogException;
import com.sap.isa.catalog.editor.EditableAttribute;
import com.sap.isa.catalog.editor.EditableAttributeValue;
import com.sap.isa.catalog.editor.EditableCatalog;
import com.sap.isa.catalog.editor.EditableCategory;
import com.sap.isa.catalog.editor.EditableDetail;
import com.sap.isa.catalog.editor.EditableItem;
import com.sap.isa.catalog.impl.CatalogComponent;
import com.sap.isa.catalog.impl.CatalogCompositeComponent;
import com.sap.isa.catalog.impl.CatalogHierarchicalComponent;

/**
 *  This calls processes the commands that were created during mainting
 *  a catalog. The command processor handles the history mechanism.
 *  (see command processor pattern)
 *
 *  @version     1.0
 */
public class CatalogCmdProcessor implements Serializable
{
    private LinkedList theUndoHistory = new LinkedList();
    private LinkedList theRedoHistory = new LinkedList();
    private List theBeforeSaveList    = new LinkedList();
    private List theAfterSaveList     = new LinkedList();
    private int theHistorySize;

    /**
     * Constructs a new processor instance.
     *
     * @param historySize the maximal size of the history
     */
    public CatalogCmdProcessor(int historySize)
    {
        theHistorySize = historySize;
    }

    /**
     * Executes the given command and stores it in the history.
     *
     * @param cmd the command to be executed
     * @return boolean that indicates if the command was successfully executed
     * @exception CatalogException error during executing the command
     */
    public boolean doCmd(CatalogCmd cmd) throws CatalogException
    {
        boolean result = cmd.execute();
        if(result)
            addToUndoHistory(cmd);

        return result;
    }

    /**
     * Trys to undo the last command that was executed.
     *
     * @return boolean that indicates if it was successfully
     * @exception CatalogException error during undoing the command
     */
    public boolean undo() throws CatalogException
    {
        boolean result = false;
        CatalogCmd cmd = (CatalogCmd)theUndoHistory.removeFirst();
        if(cmd != null)
        {
            result = cmd.undo();
            if(result)
                addToRedoHistory(cmd);
        }
        return result;
    }

    /**
     * Trys to redo the last command that was undone before.
     *
     * @return boolean that indicates if it was successfully
     * @exception CatalogException error during executing the command
     */
    public boolean redo() throws CatalogException
    {
        boolean result = false;
        CatalogCmd cmd = (CatalogCmd)theRedoHistory.removeFirst();
        if(cmd != null)
            result = doCmd(cmd);
        return result;
    }

    /**
     * Method that should be called before the changes are submitted to
     * the underlying content engine.
     *
     * @return boolean that indicates if the actions could be performed successfully
     * @exception CatalogException error while executing the callback actions
     */
    public boolean doBeforeSave() throws CatalogException
    {
        boolean result = true;

        Iterator iter = theBeforeSaveList.iterator();
        while(iter.hasNext())
        {
            CatalogCmd cmd = (CatalogCmd)iter.next();
            if(!cmd.doBeforeSave())
                result = false;
        }

        return result;
    }

    /**
     * Method that should be called after the changes are submitted to
     * the underlying content engine.
     *
     * @return boolean that indicates if the actions could be performed successfully
     * @exception CatalogException error while executing the callback actions
     */
    public boolean doAfterSave() throws CatalogException
    {
        boolean result = true;

        Iterator iter = theAfterSaveList.iterator();
        while(iter.hasNext())
        {
            CatalogCmd cmd = (CatalogCmd)iter.next();
            if(!cmd.doAfterSave())
                result = false;
        }

        theAfterSaveList.clear();
        theBeforeSaveList.clear();
        this.clearHistory();

        return result;
    }

    /**
     * Clears the whole history.
     */
    private void clearHistory()
    {
        theRedoHistory.clear();
        theUndoHistory.clear();
    }

    /**
     * Adds the given command to the undo history.
     *
     * @param cmd command to be added
     */
    private void addToUndoHistory(CatalogCmd cmd)
    {
        theUndoHistory.addFirst(cmd);
        if(theUndoHistory.size() > theHistorySize)
            theUndoHistory.removeLast();
    }

    /**
     * Adds the given command to the redo history.
     *
     * @param cmd command to be added
     */
    private void addToRedoHistory(CatalogCmd cmd)
    {
        theRedoHistory.addFirst(cmd);
        if(theRedoHistory.size() > theHistorySize)
            theRedoHistory.removeLast();
    }

    /**
     * Adds the given command to the list of commands that are called
     * back before the changes are saved.
     *
     * @param cmd command to be added
     */
    private void addToBeforeSaveList(CatalogCmd cmd)
    {
        theBeforeSaveList.add(cmd);
    }

    /**
     * Adds the given command to the list of commands that are called
     * back after the changes are saved.
     *
     * @param cmd command to be added
     */
    private void addToAfterSaveList(CatalogCmd cmd)
    {
        theAfterSaveList.add(cmd);
    }

    //
    // Factory methods for general commands
    //

    /**
     * Factory method for a concrete command.
     *
     * @param src       the component that is to be copied
     * @param newParent the new parent node of this component
     * @return the created command instance
     */
    public CatalogCmd createCmdComponentCopy(CatalogHierarchicalComponent src,
                                             CatalogHierarchicalComponent newParent)
    {
        return new CatalogCmdComponentCopy(src, newParent);
    }

    /**
     * Factory method for a concrete command.
     *
     * @param src       the component that is to be moved
     * @param newParent the new parent node of this component
     * @return the created command instance
     */
    public CatalogCmd createCmdComponentMove(CatalogHierarchicalComponent src,
                                             CatalogCompositeComponent newParent)
    {
        return new CatalogCmdComponentMove(src, newParent);
    }

    //
    // Factory methods for catalog commands
    //

    /**
     * Factory method for a concrete command.
     *
     * @param catalog the catalog that should be edited
     * @param name the new name of the category
     * @param description the new description of the category
     * @return the created command instance
     */
    public CatalogCmd createCmdCatalogEdit(  EditableCatalog catalog,
                                             String value,
                                             int property)
    {
        return new CatalogCmdCatalogEdit(catalog, value, property);
    }

    //
    // Factory methods for attribute commands
    //

    /**
     * Factory method for a concrete command.
     *
     * @param parent     the component for which the attribute should be created
     * @param guid       the guid of the attribute
     *
     * @return the created command instance
     */
    public CatalogCmd createCmdAttributeCreate(  CatalogCompositeComponent parent,
                                                 String guid)
    {
        return new CatalogCmdAttributeCreate(parent, guid);
    }

    /**
     * Factory method for a concrete command.
     *
     * @param attribute the attribute that should be edited
     * @param value     the new value of the property
     * @param property  the property to be set (constants see {@link CatalogCmd})
     * @return the created command instance
     */
    public CatalogCmd createCmdAttributeEdit(EditableAttribute attribute,
                                             Object value,
                                             int property)
    {
        return new CatalogCmdAttributeEdit(attribute, value, property);
    }

    /**
     * Factory method for a concrete command.
     *
     * @param parent    the component of which the attribute should be deleted
     * @param attribute the attribute to be deleted
     * @return the created command instance
     */
    public CatalogCmd createCmdAttributeDelete(  CatalogCompositeComponent parent,
                                                 EditableAttribute attribute)
    {
        CatalogCmd cmd = new CatalogCmdAttributeDelete(parent, attribute);
        this.addToBeforeSaveList(cmd);
        return cmd;
    }

    //
    //  Factory methods for attribute value commands
    //

    /**
     * Factory method for a concrete command.
     *
     * @param parent    the component of which the attribute value should be created
     * @param guid      the guid of the attribute of the value
     * @return the created command instance
     */
    public CatalogCmd createCmdAttributeValueCreate( EditableItem parent,
                                                     String guid)
    {
        return new CatalogCmdAttributeValueCreate(parent, guid);
    }

    /**
     * Factory method for a concrete command.
     *
     * @param attrValue the component of which the value should be changed
     * @param value     the value to be set
     * @param add       boolean that indicates if the value should be added or set
     * @return the created command instance
     */
    public CatalogCmd createCmdAttributeValueEdit(EditableAttributeValue attrValue,
                                                  Object value, boolean add)
    {
        return new CatalogCmdAttributeValueEdit(attrValue, value, add);
    }

    /**
     * Factory method for a concrete command.
     *
     * @param parent    the component of which the attribute value should be deleted
     * @param value     the value to be deleted
     * @return the created command instance
     */
    public CatalogCmd createCmdAttributeValueDelete( EditableItem parent,
                                                     EditableAttributeValue value)
    {
        return new CatalogCmdAttributeValueDelete(parent, value);
    }

    //
    //  Factory methods for item commands
    //

    /**
     * Factory method for a concrete command.
     *
     * @param parent the component for which an item should be created
     * @param guid   the guid of the new item
     * @return the created command instance
     */
    public CatalogCmd createCmdItemCreate(CatalogCompositeComponent parent,
                                          String guid)
    {
        return new CatalogCmdItemCreate(parent, guid);
    }

    /**
     * Factory method for a concrete command.
     *
     * @param item     the item to be edited
     * @param value    the new value of the property to be changed
     * @param property  the property to be set (constants see {@link CatalogCmd})
     * @return the created command instance
     */
    public CatalogCmd createCmdItemEdit(EditableItem item, String value, int property)
    {
        return new CatalogCmdItemEdit(item, value, property);
    }

    /**
     * Factory method for a concrete command.
     *
     * @param parent the component for which the item should be deleted
     * @param item the item to deleted
     * @return the created command instance
     */
    public CatalogCmd createCmdItemDelete(CatalogCompositeComponent parent,
                                          EditableItem item)
    {
        return new CatalogCmdItemDelete(parent, item);
    }

    //
    //  Factory methods for category commands
    //

    /**
     * Factory method for a concrete command.
     *
     * @param parent the component for which the category should be created
     * @param guid   the guid of the category that should be created
     * @return the created command instance
     */
    public CatalogCmd createCmdCategoryCreate(CatalogCompositeComponent parent,
                                              String guid)
    {
        return new CatalogCmdCategoryCreate(parent, guid);
    }

    /**
     * Factory method for a concrete command.
     *
     * @param category the category that should be edited
     * @param name the new name of the category
     * @param description the new description of the category
     * @return the created command instance
     */
    public CatalogCmd createCmdCategoryEdit( EditableCategory category,
                                             String value,
                                             int property)
    {
        return new CatalogCmdCategoryEdit(category, value, property);
    }

    /**
     * Factory method for a concrete command.
     *
     * @param parent   the component for which the category should be deleted
     * @param category the category that should be deleted
     * @return the created command instance
     */
    public CatalogCmd createCmdCategoryDelete(CatalogCompositeComponent parent,
                                              EditableCategory category)
    {
        return new CatalogCmdCategoryDelete(parent, category);
    }

    //
    //  Factory methods for detail commands
    //

    /**
     * Factory method for a concrete command.
     *
     * @param  parent the component for which the detail should be created
     * @param  name   the name of the detail
     * @return the created command instance
     */
    public CatalogCmd createCmdDetailCreate(CatalogComponent parent, String name)
    {
        return new CatalogCmdDetailCreate(parent, name);
    }

    /**
     * Factory method for a concrete command.
     *
     * @param  parent the component for which the detail should be edited
     * @param  name   the new name of the detail
     * @param  value  the new value of the detail
     * @param  add    boolean that indicates if the value should be added or set
     * @return the created command instance
     */
    public CatalogCmd createCmdDetailEdit(EditableDetail parent,
                                          String name, String value, boolean add)
    {
        return new CatalogCmdDetailEdit(parent, name, value, add);
    }

    /**
     * Factory method for a concrete command.
     *
     * @param parent the component for which the detail should be deleted
     * @param detail the detail to be deleted
     * @return the created command instance
     */
    public CatalogCmd createCmdDetailDelete( CatalogComponent parent,
                                              EditableDetail detail)
    {
        return new CatalogCmdDetailDelete(parent, detail);
    }
}

