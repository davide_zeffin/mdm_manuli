/*****************************************************************************
  Copyright (c) 2000, SAPMarkets Europe GmbH, Germany, All rights reserved.
  Created:      16 July 2001

  $Id: //sap/ESALES_base/30_SP_COR/src/_pcatAPI/java/com/sap/isa/catalog/catalogcmd/CatalogCmdCategoryDelete.java#2 $
  $Revision: #2 $
  $Change: 129221 $
  $DateTime: 2003/05/15 17:20:58 $
*****************************************************************************/

package com.sap.isa.catalog.catalogcmd;

// catalog imports
import com.sap.isa.catalog.boi.CatalogException;
import com.sap.isa.catalog.boi.IComponent;
import com.sap.isa.catalog.editor.EditableCategory;
import com.sap.isa.catalog.impl.CatalogCompositeComponent;

/**
 *  Command to delete a category in the catalog tree.
 *
 *  @version     1.0
 *  @since       2.0
 */
class CatalogCmdCategoryDelete extends CatalogCmd
{
    private CatalogCompositeComponent theComponent;
    private EditableCategory theCategory;
    private IComponent.State theOldCompState;
    private IComponent.State theOldCategoryState;

    /**
     * Constructor.
     *
     * @param parent   the component for which the category should be deleted
     * @param category the category that should be deleted
     */
    protected CatalogCmdCategoryDelete( CatalogCompositeComponent parent,
                                        EditableCategory category)
    {
        theComponent = parent;
        theCategory = category;
    }

    protected boolean undoInternal() throws CatalogException
    {
        synchronized(theComponent)
        {
            synchronized(theCategory)
            {
                theComponent.setState(theOldCompState);
                theCategory.setState(theOldCategoryState);
                theComponent.addChildCategoryInternal(theCategory);
            }
        }
        return true;
    }

    protected boolean executeInternal() throws CatalogException
    {
        boolean success = false;

        synchronized(theComponent)
        {
            synchronized(theCategory)
            {
                theOldCompState = theComponent.getState();
                theOldCategoryState = theCategory.getState();
                success = theComponent.setStateChanged();
                if(success)
                {
                    theComponent.deleteChildCategoryInternal(theCategory);
                    theCategory.setStateDeleted();
                }
            }
        }
        return success;
    }
}
