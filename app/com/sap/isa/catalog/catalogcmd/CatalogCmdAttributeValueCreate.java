/*****************************************************************************
  Copyright (c) 2000, SAPMarkets Europe GmbH, Germany, All rights reserved.
  Created:      16 July 2001

  $Id: //java/sapm/esales/dev/src/com/sap/isa/catalog/catalogcmd/CatalogCmdAttributeValueCreate.java#5 $
  $Revision: #5 $
  $Change: 26550 $
  $DateTime: 2001/07/31 13:24:42 $
*****************************************************************************/

package com.sap.isa.catalog.catalogcmd;

// catalog imports
import com.sap.isa.catalog.boi.CatalogException;
import com.sap.isa.catalog.boi.IComponent;
import com.sap.isa.catalog.editor.EditableAttributeValue;
import com.sap.isa.catalog.editor.EditableItem;

/**
 *  Command to create an attribute value in the catalog tree.
 *
 *  @version     1.0
 *  @since       2.0
 */
class CatalogCmdAttributeValueCreate extends CatalogCmd
{
    private EditableAttributeValue theValue;
    private EditableItem theComponent;
    private String theAttributeGuid;
    private IComponent.State theOldState;

    protected CatalogCmdAttributeValueCreate(EditableItem parent,
                                             String attributeGuid)
    {
        theComponent = parent;
        theAttributeGuid = attributeGuid;
    }

    protected boolean undoInternal() throws CatalogException
    {
        synchronized(theComponent)
        {
            synchronized(theValue)
            {
                theComponent.deleteAttributeValueInternal(theValue);
                theComponent.setState(theOldState);
                theValue.setStateDeleted();
            }
        }
        return true;
    }

    protected boolean executeInternal() throws CatalogException
    {
        boolean success = false;

        synchronized(theComponent)
        {
            theOldState = theComponent.getState();
            success = theComponent.setStateChanged();
            if(success)
                theValue = theComponent.
                    createEditableAttributeValueInternal(theAttributeGuid);
        }
        return success;
    }
}
