/*****************************************************************************
  Copyright (c) 2000, SAPMarkets Europe GmbH, Germany, All rights reserved.
  Created:      16 July 2001

  $Id: //sap/ESALES_base/30_SP_COR/src/_pcatAPI/java/com/sap/isa/catalog/catalogcmd/CatalogCmdComponentCopy.java#2 $
  $Revision: #2 $
  $Change: 129221 $
  $DateTime: 2003/05/15 17:20:58 $
*****************************************************************************/

package com.sap.isa.catalog.catalogcmd;

// catalog imports
import com.sap.isa.catalog.boi.CatalogException;
import com.sap.isa.catalog.impl.CatalogHierarchicalComponent;

/**
 *  Command to copy a component of the catalog tree.
 *
 *  @version     1.0
 *  @since       2.0
 */
class CatalogCmdComponentCopy extends CatalogCmd
{
    private CatalogHierarchicalComponent theSource;
    private CatalogHierarchicalComponent theNewParent;

    /**
     * Constructor.
     *
     * @param src       the component that is to be copied
     * @param newParent the new parent node of this component
     */
    protected CatalogCmdComponentCopy(  CatalogHierarchicalComponent src,
                                        CatalogHierarchicalComponent newParent)
    {
        theSource = src;
        theNewParent = newParent;
    }

    protected boolean undoInternal() throws CatalogException
    {
        return false;
    }

    protected boolean executeInternal() throws CatalogException
    {
        return false;
    }
}
