/*****************************************************************************
  Copyright (c) 2000, SAPMarkets Europe GmbH, Germany, All rights reserved.
  Created:      16 July 2001

  $Id: //sap/ESALES_base/30_SP_COR/src/_pcatAPI/java/com/sap/isa/catalog/catalogcmd/CatalogCmdItemDelete.java#2 $
  $Revision: #2 $
  $Change: 129221 $
  $DateTime: 2003/05/15 17:20:58 $
*****************************************************************************/

package com.sap.isa.catalog.catalogcmd;

// catalog imports
import com.sap.isa.catalog.boi.CatalogException;
import com.sap.isa.catalog.boi.IComponent;
import com.sap.isa.catalog.editor.EditableItem;
import com.sap.isa.catalog.impl.CatalogCompositeComponent;

/**
 *  Command to delete an item in the catalog tree.
 *
 *  @version     1.0
 *  @since       2.0
 */
class CatalogCmdItemDelete extends CatalogCmd
{
    private EditableItem theItem;
    private CatalogCompositeComponent theComponent;
    private IComponent.State theOldCompState;
    private IComponent.State theOldItemState;

    /**
     * Constructor.
     *
     * @param parent the component for which the item should be deleted
     * @param item the item to deleted
     */
    protected CatalogCmdItemDelete(CatalogCompositeComponent parent,
                                   EditableItem item)
    {
        theComponent = parent;
        theItem = item;
    }

    protected boolean undoInternal() throws CatalogException
    {
        synchronized(theComponent)
        {
            synchronized(theItem)
            {
                theComponent.setState(theOldCompState);
                theItem.setState(theOldItemState);
                theComponent.addItemInternal(theItem);
            }
        }
        return true;
    }

    protected boolean executeInternal() throws CatalogException
    {
        boolean success = false;

        synchronized(theComponent)
        {
            synchronized(theItem)
            {
                theOldCompState = theComponent.getState();
                theOldItemState = theItem.getState();
                success = theComponent.setStateChanged();
                if(success)
                {
                    theItem.setStateDeleted();
                    theComponent.deleteItemInternal(theItem);
                }
            }
        }
        return success;
    }
}
