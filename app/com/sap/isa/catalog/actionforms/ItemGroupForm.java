package com.sap.isa.catalog.actionforms;

import org.apache.struts.action.ActionForm;

/**
 * Title:        ItemGroupForm
 * Description:  ActionForm containing input field data of one item to be updated
 *               from HTML form.
 *               For item groups this is enclosed in a WebCatItemPageForm
 * Copyright:    Copyright (c) 2006
 * Company:      SAP AG 
 * @version 1.0
 */

public class ItemGroupForm extends ActionForm {

	/**
	 * ID of item group, used to identify the item group
	 */
    protected String groupId;
    
    /**
     * field determines if the group should be displayed in collapsed or unfold mode.
     */
    protected boolean collapsed = true;

    /**
	 * initialization
	 */

	public ItemGroupForm() {
		this.groupId = "";
		this.collapsed = true;
	}

	public void setGroupId(String newGroupId) {
		this.groupId = newGroupId;
	}

	public String getGroupId() {
		return this.groupId;
	}

	public void setCollapsed(boolean newCollapsed) {
		this.collapsed = newCollapsed;
	}

	public boolean isCollapsed() {
		return this.collapsed;
	}
}