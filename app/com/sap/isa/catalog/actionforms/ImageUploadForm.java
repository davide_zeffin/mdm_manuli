package com.sap.isa.catalog.actionforms;

import org.apache.struts.action.ActionForm;
import org.apache.struts.upload.FormFile;

import com.sap.isa.core.logging.IsaLocation;

/**
 * File: ImageActionForm.java
 * To change this generated comment edit the template variable "typecomment":
 * Window>Preferences>Java>Templates.
 * To enable and disable the creation of type comments go to
 * Window>Preferences>Java>Code Generation.
 */
public class ImageUploadForm extends ActionForm
{
  String areaID = null;
  String itemID = null;
  FormFile imageFile = null;
  FormFile thumbFile = null;
  protected static IsaLocation log = IsaLocation.getInstance(ImageUploadForm.class.getName());
  
  public ImageUploadForm()
  {
    log.debug("ImageUploadForm created.");
  }

  
  /**
   * Returns the imageFile.
   * @return FormFile
   */
  public FormFile getImageFile()
  {
    return imageFile;
  }

  /**
   * Returns the thumbFile.
   * @return FormFile
   */
  public FormFile getThumbFile()
  {
    return thumbFile;
  }

  /**
   * Sets the imageFile.
   * @param imageFile The imageFile to set
   */
  public void setImageFile(FormFile imageFile)
  {
    log.debug("uploading "+imageFile);
    if (imageFile.getFileSize() == 0)
    {
      this.imageFile = null;
      imageFile.destroy();
    }
    else
      this.imageFile = imageFile;
  }

  /**
   * Sets the thumbFile.
   * @param thumbFile The thumbFile to set
   */
  public void setThumbFile(FormFile thumbFile)
  {
    if (thumbFile.getFileSize() == 0)
    {
      this.thumbFile = null;
      thumbFile.destroy();
    }
    else
      this.thumbFile = thumbFile;
  }

  /**
   * Returns the areaId.
   * @return String
   */
  public String getAreaID()
  {
    return areaID;
  }

  /**
   * Returns the itemId.
   * @return String
   */
  public String getItemID()
  {
    return itemID;
  }

  /**
   * Sets the areaId.
   * @param areaId The areaId to set
   */
  public void setAreaID(String areaId)
  {
    this.areaID = areaId;
  }

  /**
   * Sets the itemId.
   * @param itemId The itemId to set
   */
  public void setItemID(String itemId)
  {
    this.itemID = itemId;
  }

}
