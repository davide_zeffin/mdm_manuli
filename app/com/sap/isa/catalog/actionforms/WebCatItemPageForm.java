package com.sap.isa.catalog.actionforms;

import java.util.HashMap;
import java.util.Iterator;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionFormBean;

/**
 * Title:        ProductCatalog UI
 * Description:
 * Copyright:    Copyright (c) 2001
 * Company:      SAPMarkets Europe GmbH
 * @version 1.0
 */

public class WebCatItemPageForm extends ActionForm {

  HashMap items;
  HashMap itemGroups;
  private static Class itemFormClass = null;
  private static Class itemGroupFormClass = null;

  public WebCatItemPageForm() {
    items = new HashMap();
    itemGroups = new HashMap();
  }

  public WebCatItemForm getItem(int index) {
    // Determination of itemFormClass needs to be here, as reference to servlet
    // is not yet set in constructor!
    if (itemFormClass == null)
    {
      itemFormClass = determineItemFormClass();
    }
    String key = String.valueOf(index);
    if (items.containsKey(key))
    {
      return (WebCatItemForm) items.get(key);
    }
    else
    {
      WebCatItemForm itemForm = createWebCatItemForm();
      items.put(key, itemForm);
      return itemForm;
    }
  }

  protected Class determineItemFormClass()
  {
    Class itemFormClass = null;
    ActionFormBean formBean = null;
    if (getServlet() != null)
      formBean = super.getServlet().findFormBean("WebCatItemForm");
    if (formBean != null)
    {
      try
      {
        itemFormClass = Class.forName(formBean.getType());
      }
      catch (ClassNotFoundException e)
      {
        itemFormClass = null;
      }
    }
    if (itemFormClass == null)
    {
      itemFormClass = WebCatItemForm.class;
    }
    return itemFormClass;
  }

  protected WebCatItemForm createWebCatItemForm() {
    WebCatItemForm itemForm = null;
    try
    {
      itemForm = (WebCatItemForm) itemFormClass.newInstance();
    }
    catch (Exception e)
    {
      itemForm = new WebCatItemForm();
    }
    return itemForm;
  }

  public Iterator itemIterator() {
    return items.values().iterator();
  }

  public int size() {
    return items.size();
  }

  public ItemGroupForm getItemGroup(int index) {
    // Determination of itemGroupFormClass needs to be here, as reference to servlet
    // is not yet set in constructor!
    if (itemGroupFormClass == null)
    {
      itemGroupFormClass = determineItemGroupFormClass();
    }
    String key = String.valueOf(index);
    if (itemGroups.containsKey(key))
    {
      return (ItemGroupForm) itemGroups.get(key);
    }
    else
    {
      ItemGroupForm itemGroupForm = createItemGroupForm();
      itemGroups.put(key, itemGroupForm);
      return itemGroupForm;
    }
  }

  protected Class determineItemGroupFormClass()
  {
    Class itemGroupFormClass = null;
    ActionFormBean formBean = null;
    if (getServlet() != null)
      formBean = super.getServlet().findFormBean("ItemGroupForm");
    if (formBean != null)
    {
      try
      {
        itemGroupFormClass = Class.forName(formBean.getType());
      }
      catch (ClassNotFoundException e)
      {
        itemGroupFormClass = null;
      }
    }
    if (itemGroupFormClass == null)
    {
      itemGroupFormClass = ItemGroupForm.class;
    }
    return itemGroupFormClass;
  }

  protected ItemGroupForm createItemGroupForm() {
    ItemGroupForm itemGroupForm = null;
    try
    {
      itemGroupForm = (ItemGroupForm) itemGroupFormClass.newInstance();
    }
    catch (Exception e)
    {
      itemGroupForm = new ItemGroupForm();
    }
    return itemGroupForm;
  }

  public Iterator itemGroupIterator() {
    return itemGroups.values().iterator();
  }

  public int groupSize() {
    return itemGroups.size();
  }

}
