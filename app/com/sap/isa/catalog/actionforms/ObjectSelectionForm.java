package com.sap.isa.catalog.actionforms;

import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;

import com.sap.isa.catalog.webcatalog.ObjectSelection;

/**
 * Title:        ProductCatalog UI
 * Description:  ObjectSelectionForm containing object ids and whether they
 *               are selected or not.
 * Copyright:    Copyright (c) 2002
 * Company:      SAPMarkets Europe GmbH
 * @version 1.0
 */

public abstract class ObjectSelectionForm extends ActionForm {

  protected HashMap selections;
  private int maxnumber = 0;

  public ObjectSelectionForm()
  {
    selections = new HashMap();
  }

  /**
   * Create new selection object to be used. overwrite this method to use
   * other selection objects (which must be subclasses of
   * @see com.sap.isa.catalog.webcatalog.ObjectSelection)
   */
  protected ObjectSelection createObject()
  {
    return new ObjectSelection();
  }

  /**
   * This method does the work of selecting the objects to be used.
   * As you might have several differnt object selections in one page,
   * you need to create a public method whose name does not mix with other
   * selections and which internally calls this method. Example:
   * <code>
   *   public ObjectSelection getContact(int index)
   *   {
   *     return getObject(index);
   *   }
   * </code>
   * So in your page you can set the values using <code>contact[1].id</code>
   * and <code>contact[1].selected</code>.
   */
  protected ObjectSelection getObject(int index)
  {
    String key = String.valueOf(index);
    if (selections.containsKey(key))
    {
      return (ObjectSelection) selections.get(key);
    }
    else
    {
      ObjectSelection selection = createObject();
      selections.put(key, selection);
      if (index > maxnumber)
        maxnumber = index;
      return selection;
    }
  }

  public ObjectSelection[] getAll()
  {
    ObjectSelection[] result = new ObjectSelection[getMaxnumber()+1];
    for (int i=0; i<getMaxnumber()+1; i++)
    {
      result[i] = getObject(i);
    }
    return result;
  }
  
  public int getMaxnumber()
  {
    return maxnumber;
  }
  
  /* (non-Javadoc)
   * @see org.apache.struts.action.ActionForm#reset(org.apache.struts.action.ActionMapping, javax.servlet.http.HttpServletRequest)
   */
  public void reset(ActionMapping mapping, HttpServletRequest request)
  {
    super.reset(mapping, request);
    selections = new HashMap();
    maxnumber = 0;
  }

}