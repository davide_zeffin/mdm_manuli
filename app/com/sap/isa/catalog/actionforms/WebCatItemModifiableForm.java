package com.sap.isa.catalog.actionforms;

/**
 * @author d028031
 *
 * To change this generated comment edit the template variable "typecomment":
 * Window>Preferences>Java>Templates.
 * To enable and disable the creation of type comments go to
 * Window>Preferences>Java>Code Generation.
 */
public class WebCatItemModifiableForm extends WebCatItemForm
{
  
  private String product;
  
  private String productId;
  
  private String productDescription;
  
  private String areaId;
  
  private String itemLongDescription;

  /**
   * Constructor for WebCatItemModifiableForm.
   */
  public WebCatItemModifiableForm()
  {
    super();
  }
  
  
  /**
   * Returns the areaId.
   * @return String
   */
  public String getAreaId()
  {
    return areaId;
  }

  /**
   * Returns the productId.
   * @return String
   */
  public String getProductId()
  {
    return productId;
  }

  /**
   * Sets the areaId.
   * @param areaId The areaId to set
   */
  public void setAreaId(String areaId)
  {
    this.areaId = areaId;
  }

  /**
   * Sets the productId.
   * @param productId The productId to set
   */
  public void setProductId(String productId)
  {
    this.productId = productId;
  }

  /**
   * Returns the productDescription.
   * @return String
   */
  public String getProductDescription()
  {
    return productDescription;
  }

  /**
   * Sets the productDescription.
   * @param productDescription The productDescription to set
   */
  public void setProductDescription(String productDescription)
  {
    this.productDescription = productDescription;
  }

  /**
   * Returns the product.
   * @return String
   */
  public String getProduct()
  {
    return product;
  }

  /**
   * Sets the product.
   * @param product The product to set
   */
  public void setProduct(String product)
  {
    this.product = product;
  }

  /**
   * Returns the areaLongDescription.
   * @return String
   */
  public String getItemLongDescription()
  {
    return itemLongDescription;
  }

  /**
   * Sets the areaLongDescription.
   * @param areaLongDescription The areaLongDescription to set
   */
  public void setItemLongDescription(String itemLongDescription)
  {
    this.itemLongDescription = itemLongDescription;
  }

}
