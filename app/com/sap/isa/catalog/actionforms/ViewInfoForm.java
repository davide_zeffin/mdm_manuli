package com.sap.isa.catalog.actionforms;

import org.apache.struts.action.ActionForm;

/**
 * ActionForm to handle view header data when creating new view or changing data
 * for existing views.
 */

public class ViewInfoForm extends ActionForm {

  private String description;
  private String id;
  private boolean defaultView;
  private boolean automaticallyAddNewItems;

  public ViewInfoForm() {
  }

  public String getDescription() {
    return description;
  }

  public void setDescription(String description) {
    this.description = description;
  }

  public void setId(String id) {
    this.id = id;
  }

  public String getId() {
    return id;
  }

  public void setDefaultView(boolean defaultView) {
    this.defaultView = defaultView;
  }

  public boolean isDefaultView() {
    return defaultView;
  }

  public void setAutomaticallyAddNewItems(boolean automaticallyAddNewItems) {
    this.automaticallyAddNewItems = automaticallyAddNewItems;
  }

  public boolean isAutomaticallyAddNewItems() {
    return automaticallyAddNewItems;
  }

}