package com.sap.isa.catalog.actionforms;

import org.apache.struts.action.ActionForm;

public class InactiveCatalogForm extends ActionForm {

	private String startDate;
    
    public InactiveCatalogForm() {
    }

	public String getStartDate() {
		return startDate;
	}

	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}

}
