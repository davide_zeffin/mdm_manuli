package com.sap.isa.catalog.actionforms;

import org.apache.struts.action.ActionForm;


/**
 * File: WebCatAreaModifiableForm.java
 * To change this generated comment edit the template variable "typecomment":
 * Window>Preferences>Java>Templates.
 * To enable and disable the creation of type comments go to
 * Window>Preferences>Java>Code Generation.
 */
public class WebCatAreaModifiableForm extends ActionForm
{

  private String areaId = null;
  
  private String areaDescription = null;
  
  private String areaLongDescription = null;
  
  /**
   * Constructor for WebCatAreaModifiableForm.
   */
  public WebCatAreaModifiableForm()
  {
    super();
  }


  /**
   * Returns the areaDescription.
   * @return String
   */
  public String getAreaDescription()
  {
    return areaDescription;
  }

  /**
   * Returns the areaId.
   * @return String
   */
  public String getAreaID()
  {
    return areaId;
  }

  /**
   * Sets the areaDescription.
   * @param areaDescription The areaDescription to set
   */
  public void setAreaDescription(String areaDescription)
  {
    this.areaDescription = areaDescription;
  }

  /**
   * Sets the areaId.
   * @param areaId The areaId to set
   */
  public void setAreaID(String areaId)
  {
    this.areaId = areaId;
  }

  /**
   * Returns the areaLongDescription.
   * @return String
   */
  public String getAreaLongDescription()
  {
    return areaLongDescription;
  }

  /**
   * Sets the areaLongDescription.
   * @param areaLongDescription The areaLongDescription to set
   */
  public void setAreaLongDescription(String areaLongDescription)
  {
    this.areaLongDescription = areaLongDescription;
  }

}
