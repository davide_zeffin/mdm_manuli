package com.sap.isa.catalog.actionforms;

import com.sap.isa.catalog.webcatalog.ObjectSelection;

/**
 * Title:        Catalog in ESALES_base dev branch
 * Description:
 * Copyright:    Copyright (c) 2002
 * Company:
 * @author
 * @version 1.0
 */

public class AreaSelectionForm extends ObjectSelectionForm {

  public ObjectSelection getArea(int index)
  {
    return getObject(index);
  }

}