/*****************************************************************************
  Copyright (c) 2000, SAPMarkets Europe GmbH, Germany, All rights reserved.
  Author:       RFU
  Created:      18. Oktober 2001

  $Revision: #1 $
  $Date: 2001/10/18 $
*****************************************************************************/

package com.sap.isa.catalog.actionforms;

import java.beans.IntrospectionException;
import java.beans.PropertyDescriptor;
import java.beans.SimpleBeanInfo;

/**
 * BeanInfo Class for OCIEntryForm. As the parameters provided by EBP are not
 * Java Bean compliant (i.e. are all uppercase, contain underscores etc.) this
 * BeanInfo enables Struts to set java properties from EBP parameters.
 */
public class OCIEntryFormBeanInfo extends SimpleBeanInfo {
  Class beanClass = OCIEntryForm.class;

  public OCIEntryFormBeanInfo() {
  }

  public PropertyDescriptor[] getPropertyDescriptors() {
    try {
      PropertyDescriptor _baskettarget = new PropertyDescriptor("BASKETTARGET", beanClass, "getBaskettarget", "setBaskettarget");
      PropertyDescriptor _catalog = new PropertyDescriptor("CATALOG", beanClass, "getCatalog", "setCatalog");
      PropertyDescriptor _catalogtarget = new PropertyDescriptor("CATALOGTARGET", beanClass, "getCatalogtarget", "setCatalogtarget");
      PropertyDescriptor _catalogVariant = new PropertyDescriptor("CATALOG_VARIANT", beanClass, "getCatalogVariant", "setCatalogVariant");
      PropertyDescriptor _ebpCatId = new PropertyDescriptor("EBP_CAT_ID", beanClass, "getEbpCatId", "setEbpCatId");
      PropertyDescriptor _ebpCatSos = new PropertyDescriptor("EBP_CAT_SOS", beanClass, "getEbpCatSos", "setEbpCatSos");
      PropertyDescriptor _ebpPo = new PropertyDescriptor("EBP_PO", beanClass, "getEbpPo", "setEbpPo");
      PropertyDescriptor _ebpUser = new PropertyDescriptor("EBP_USER", beanClass, "getEbpUser", "setEbpUser");
      PropertyDescriptor _fullscreenUrl = new PropertyDescriptor("FULLSCREEN_URL", beanClass, "getFullscreenUrl", "setFullscreenUrl");
      PropertyDescriptor _hookUrl = new PropertyDescriptor("HOOK_URL", beanClass, "getHookUrl", "setHookUrl");
      PropertyDescriptor _imageServerURL = new PropertyDescriptor("IMAGE_SERVER_URL", beanClass, "getImageServerURL", "setImageServerURL");
      PropertyDescriptor _language = new PropertyDescriptor("LANGUAGE", beanClass, "getLanguage", "setLanguage");
      PropertyDescriptor _contentLanguage = new PropertyDescriptor("CONTENT_LANGUAGE", beanClass, "getContentLanguage", "setContentLanguage");
      PropertyDescriptor _ociVersion = new PropertyDescriptor("OCI_VERSION", beanClass, "getOciVersion", "setOciVersion");
      PropertyDescriptor _password = new PropertyDescriptor("PASSWORD", beanClass, "getPassword", "setPassword");
      PropertyDescriptor _serverURL = new PropertyDescriptor("SERVER_URL", beanClass, "getServerURL", "setServerURL");
      PropertyDescriptor _username = new PropertyDescriptor("USERNAME", beanClass, "getUsername", "setUsername");
      PropertyDescriptor _init = new PropertyDescriptor("init", beanClass, "isInit", "setInit");
      PropertyDescriptor _stylesheet = new PropertyDescriptor("STYLESHEET", beanClass, "getStylesheet", "setStylesheet");
      PropertyDescriptor[] pds = new PropertyDescriptor[] {
          _baskettarget,
          _catalog,
          _catalogtarget,
          _catalogVariant,
          _ebpCatId,
          _ebpCatSos,
          _ebpPo,
          _ebpUser,
          _fullscreenUrl,
          _hookUrl,
          _imageServerURL,
          _language,
          _contentLanguage,
          _ociVersion,
          _password,
          _serverURL,
          _username,
          _init,
          _stylesheet};
      return pds;

    }
    catch(IntrospectionException ex) {
      ex.printStackTrace();
      return null;
    }
  }
}
