package com.sap.isa.catalog.actionforms;

import org.apache.struts.action.ActionForm;

/**
 * Title:        ProductCatalog UI
 * Description:  ActionForm containing input field data of one item to be updated
 *               from HTML form.
 *               For item tables this is enclosed in a WebCatItemPageForm
 * Copyright:    Copyright (c) 2001
 * Company:      SAPMarkets Europe GmbH
 * @version 1.0
 */

public class WebCatItemForm extends ActionForm {

	/**
	 * unique ID of catalog item, used to identify the item
	 */
	private String itemID;

	/**
	 * Unique ID of catalog top item, used to identify the top item of a sub item.
	 * @see com.sap.isa.catalog.webcatalog.WebCatSubItem
	 */
	private String topItemID;

	/**
	 * quantity of item, used when adding item to shopping basket.
	 * item price is NOT evaluated from this quantity!
	 */
	private String quantity;

	/**
	 * Item Unit
	 */
	private String unit;

	/**
	 * flag whether this item was selected for adding to shopping basket.
	 */
	private boolean selected;

	/**
	 * contractID information ... used as a part of a Primary Key, together with contractItemID
	 */
	private String contractKey;

	/**
	 * contractItemID information ... used as a part of a Primary Key, together with contractID
	 */
	private String contractItemKey;

	/**
	 * Status to control, which items are transferred to the solution configurator explosion.
	 */
	private String selForRes;

	/**
	 * Last status for controling, which items are transferred to the solution configurator explosion. 
	 * This value will be needed to make comparison between this value and {@link #author}.
	 */
	private String selForResLast;

    /**
     * Information which items are selected by the solution configurator explosion. 
     * This value is transfered as {@link #isDefault} to the solution configurator.
     */
    private boolean scSelected;

	/**
	 * groupKey -- used for package components -- package component are part of group
	 */
	private String groupKey;

	/**
	 * selected contract duration -- used for rate plan and rate plan combination package
	 */
	private String selectedContractDuration;

    /**
     * itemIndex -- used to identify an item in a product list.
     * In catalogentry an item may be contained in recommendations and special offers. 
     */
    private int itemIndex;

	/**
	 * initialization
	 */

	public WebCatItemForm() {
		this.itemID = "";
		this.quantity = "1";
		this.unit = "";
		this.selected = false;
		this.contractKey = "";
		this.contractItemKey = "";
		this.selForRes = "";
		this.selForResLast = "";
        this.scSelected = false;
		this.topItemID = "";
		this.groupKey = "";
		this.selectedContractDuration = "";
        this.itemIndex = -1;
	}

	public void setContractKey(String contractKey) {
		this.contractKey = contractKey;
	}

	public String getContractKey() {
		return contractKey;
	}

	public void setContractItemKey(String contractItemKey) {
		this.contractItemKey = contractItemKey;
	}

	public String getContractItemKey() {
		return contractItemKey;
	}

	public void setItemID(String newItemID) {
		itemID = newItemID;
	}

	public String getItemID() {
		return itemID;
	}

	public void setQuantity(String newQuantity) {
		quantity = newQuantity;
	}

	public String getQuantity() {
		return quantity;
	}

	public void setSelected(boolean newSelected) {
		selected = newSelected;
	}

	public boolean isSelected() {
		return selected;
	}

	/**
	 * @return
	 */
	public String getUnit() {
		return unit;
	}

	/**
	 * @param string
	 */
	public void setUnit(String newUnit) {
		unit = newUnit;
	}

	/**
	 * @return Returns the selForRes.
	 */
	public String getSelForRes() {
		return selForRes;
	}

	/**
	 * @param selForRes The selForRes to set.
	 */
	public void setSelForRes(String selForRes) {
		this.selForRes = selForRes;
	}

	/**
	 * @return Returns the selForResLast.
	 */
	public String getSelForResLast() {
		return selForResLast;
	}

	/**
	 * @param selForResLast The selForResLast to set.
	 */
	public void setSelForResLast(String selForResLast) {
		this.selForResLast = selForResLast;
	}

    /**
     * @return Returns the scSelected.
     */
    public boolean isScSelected() {
        return scSelected;
    }

    /**
     * @param scSelected the scSelected to set.
     */
    public void setScSelected(boolean scSelected) {
        this.scSelected = scSelected;
    }

	/**
	 * @return Returns the topItemID.
	 */
	public String getTopItemID() {
		return topItemID;
	}
	/**
	 * @param topItemID The topItemID to set.
	 */
	public void setTopItemID(String topItemID) {
		this.topItemID = topItemID;
	}

	/**
	 * @return returns the groupKey
	 */
	public String getGroupKey() {
		return groupKey;
	}

	/**
	 * @param groupKey The groupKey to set.
	 */
	public void setGroupKey(String groupKey) {
		this.groupKey = groupKey;
	}

	/**
	 * returns the selected contract duration
	 * 
	 * @return String, the selected contract duration from this form 
	 */
	public String getSelectedContractDuration() {
		return selectedContractDuration;
	}

	/**
	 * sets the selected contract duration
	 * 
	 * @param selectedContractDuration, the selected contract duration to set
	 */
	public void setSelectedContractDuration(String selectedContractDuration) {
		this.selectedContractDuration = selectedContractDuration;
	}

    /**
     * @return returns the itemIndex
     */
    public int getItemIndex() {
        return this.itemIndex;
    }

    /**
     * set the item index
     * 
     * @param itemIndex, the item index to set.
     */
    public void setItemIndex(int itemIndex) {
        this.itemIndex = itemIndex;
    }

}