package com.sap.isa.catalog.actionforms;

import com.sap.isa.businessobject.webcatalog.views.ContactAssignment;
import com.sap.isa.catalog.webcatalog.ObjectSelection;

/**
 * Title:        ProductCatalog UI
 * Description:  ContactAssignmentForm containing contacts and whether they
 *               should be assigned to a view.
 * Copyright:    Copyright (c) 2001
 * Company:      SAPMarkets Europe GmbH
 * @version 1.0
 */

public class ContactAssignmentForm extends ObjectSelectionForm {

  public ObjectSelection getContact(int index)
  {
    return getObject(index);
  }

  public ContactAssignment[] getAllAssignments()
  {
    int size = getAll().length;
    ContactAssignment[] result = new ContactAssignment[size];
    for (int i=0; i<size; i++)
    {
      ObjectSelection obj = getObject(i);
      result[i] = new ContactAssignment();
      result[i].setContact(obj.getId());
      result[i].setIncluded(obj.isSelected());
    }
    return result;
  }
}