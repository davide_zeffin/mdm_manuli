package com.sap.isa.catalog.actionforms;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionServlet;

import com.sap.isa.catalog.actions.ActionConstants;
import com.sap.isa.core.logging.IsaLocation;

/**
 * This ActionForm collects data provided by EBP when our catalog is called via
 * OCI (Open Catalog Interface).
 * Important: There is an additional <code>OCIEntryFormBeanInfo</code> which handles
 * names of request parameters, as parameters coming from EBP are not Java compliant
 * (i.e. they start with uppercase letters, contain underscores etc.)
 */
public final class OCIEntryForm extends ActionForm
{
    //catalog parameters, pre-set in reset() method
    private String serverURL;
    private String imageServerURL;
    private String username;
    private String password;
    private String language;
    private String contentLanguage;
    private String catalog;
    private String catalogVariant;
    private String stylesheet;

    //OCI parameters
    private String hookUrl;
    private String fullscreenUrl;
    private String baskettarget;
    private String catalogtarget;
    private String ociVersion;
    private String ebpUser;
    private String ebpPo;
    private String ebpCatSos;
    private String ebpCatId;

    //Flag for initialization
    private boolean init;

    private static IsaLocation log =
        IsaLocation.getInstance(OCIEntryForm.class.getName());

    /**
     * Returns the URL used to access the catalog engine
     */
    public String getServerURL()
    {
        return (this.serverURL);
    }
    /**
     * Sets the URL used to access the catalog engine
     */
    public void setServerURL(String serverURL)
    {
        this.serverURL = serverURL;
        log.debug("serverURL set to "+serverURL);
    }
    /**
     * Returns the URL to access catalog images
     */
    public String getImageServerURL()
    {
        return (this.imageServerURL);
    }
    /**
     * Sets the URL to access catalog images
     */
    public void setImageServerURL(String imageServerURL)
    {
        this.imageServerURL = imageServerURL;
        log.debug("imageServerURL set to "+imageServerURL);
    }
    /**
     * Returns the username to be used for logon to catalog engine
     */
    public String getUsername()
    {
        return (this.username);
    }
    /**
     * Sets the username to be used for logon to catalog engine
     */
    public void setUsername(String username)
    {
        this.username = username;
        log.debug("username set to "+username);
    }
    /**
     * Returns the password to be used for logon to catalog engine
     */
    public String getPassword()
    {
        return (this.password);
    }
    /**
     * Sets the password to be used for logon to catalog engine
     */
    public void setPassword(String password)
    {
        this.password = password;
        log.debug("password set to "+password);
    }

    /**
     * Returns the content language
     */
    public String getContentLanguage()
    {
        return (this.contentLanguage);
    }
    /**
     * Sets the content language
     */
    public void setContentLanguage(String contentLanguage)
    {
        this.contentLanguage = contentLanguage;
        log.debug("contentLanguage set to "+contentLanguage);
    }

    /**
     * Returns the language
     */
    public String getLanguage()
    {
        return (this.language);
    }
    /**
     * Sets the language
     */
    public void setLanguage(String language)
    {
        this.language = language;
        log.debug("language set to "+language);
    }

    /**
     * Returns the catalog to be used
     */
    public String getCatalog()
    {
        return this.catalog;
    }
    /**
     * Sets the catalog to be used
     */
    public void setCatalog(String catalog)
    {
        this.catalog = catalog;
        log.debug("catalog set to "+catalog);
    }
    /**
     * Returns the catalog variant to be used
     */
    public String getCatalogVariant()
    {
        return this.catalogVariant;
    }
    /**
     * Sets the catalog variant to be used
     */
    public void setCatalogVariant(String catalogVariant)
    {
        this.catalogVariant = catalogVariant;
        log.debug("catalogVariant set to "+catalogVariant);
    }

    /**
     * Returns the name of the frame where the basket is shown in.
     * This is used to be able to update EBP basket from catalog by posting
     * data via OCI to this frame.
     */
    public String getBaskettarget() {
        return baskettarget;
    }
    /**
     * Sets name of basket frame
     */
    public void setBaskettarget(String baskettarget) {
        this.baskettarget = baskettarget;
        log.debug("baskettarget set to "+baskettarget);
    }
    /**
     * Returns the name of the frame where the catalog is shown in.
     * This is used to be able to update the catalog itself without confusing
     * the frameset.
     */
    public String getCatalogtarget() {
        return catalogtarget;
    }
    /**
     * Sets name of catalog frame
     */
    public void setCatalogtarget(String catalogtarget) {
        this.catalogtarget = catalogtarget;
        log.debug("catalogtarget set to "+catalogtarget);
    }
    /**
     * Returns contract data display mode. This mode determines whether contracts
     * are shown within catalog product list.
     */
    public String getEbpCatSos() {
        return ebpCatSos;
    }
    /**
     * Sets contract display mode.
     */
    public void setEbpCatSos(String ebpCatSos) {
        this.ebpCatSos = ebpCatSos;
        log.debug("ebpCatSos set to "+ebpCatSos);
    }
    /**
     * Returns EBP purchase order number. This number is only stored and routed
     * through to calls to EBP for contract determination.
     */
    public String getEbpPo() {
        return ebpPo;
    }
    /**
     * Sets EBP purchase order number
     */
    public void setEbpPo(String ebpPo) {
        this.ebpPo = ebpPo;
        log.debug("ebpPo set to "+ebpPo);
    }
    /**
     * Returns EBP user. This is stored and routed
     * through to calls to EBP for contract determination.
     */
    public String getEbpUser() {
        return ebpUser;
    }
    /**
     * Sets EBP user
     */
    public void setEbpUser(String ebpUser) {
        this.ebpUser = ebpUser;
        log.debug("ebpUser set to "+ebpUser);
    }
    /**
     * Returns EBP catalog id. This is stored and routed
     * through to calls to EBP for contract determination.
     */
    public String getEbpCatId() {
        return ebpCatId;
    }
    /**
     * Sets EBP catalog id
     */
    public void setEbpCatId(String ebpCatId) {
        this.ebpCatId = ebpCatId;
        log.debug("ebpCatId set to "+ebpCatId);
    }
    /**
     * Returns URL that leads to a fullscreen catalog UI. This is used to be
     * able to switch from our "in frame" UI to a fullscreen catalog UI
     * (usually the regular requisite UI is used for that fullscreen display).
     */
    public String getFullscreenUrl() {
        return fullscreenUrl;
    }
    /**
     * Sets URL that leads to a fullscreen catalog UI
     */
    public void setFullscreenUrl(String fullscreenUrl) {
        this.fullscreenUrl = fullscreenUrl;
        log.debug("fullscreenUrl set to "+fullscreenUrl);
    }
    /**
     * Returns hook URL. This URL is used to post item data to (using OCI).
     */
    public String getHookUrl() {
        return hookUrl;
    }
    /**
     * Sets hook URL
     */
    public void setHookUrl(String hookUrl) {
        this.hookUrl = hookUrl;
        log.debug("hookUrl set to "+hookUrl);
    }
    /**
     * Returns OCI version information
     */
    public String getOciVersion() {
        return ociVersion;
    }
    /**
     * Sets OCI version information
     */
    public void setOciVersion(String ociVersion) {
        this.ociVersion = ociVersion;
        log.debug("ociVersion set to "+ociVersion);
    }
    /**
     * Returns true, if init flag was set, false otherwise
     */
    public boolean isInit() {
        return init;
    }
    /**
     * Sets init flag
     */
    public void setInit(boolean init) {
        this.init = init;
        log.debug("init set to "+init);
    }

    /**
     * Sets the name which will be used as the name of the stylesheet to be used
     * inside the jsp's
     */

     public void setStylesheet(String stylesheet) {
        this.stylesheet = stylesheet;
     }

     public String getStylesheet() {
        return stylesheet;
     }

    /**
     * Resets properties. Some properties are pre-set to values given from
     * web.xml (see there)
     */
    public void reset(ActionMapping mapping, HttpServletRequest request)
    {
        setInit(false);
        //default some properties to values given in web.xml
        ActionServlet theServlet = this.getServlet();
        ServletContext theContext = theServlet.getServletContext();

        setServerURL(theContext.getInitParameter(ActionConstants.IN_SERVER));
        setImageServerURL(theContext.getInitParameter(ActionConstants.INTCAT_IMAGE_SERVER));
        if (getImageServerURL() == null)
        {
          setImageServerURL(getServerURL());
        }
        setUsername(theContext.getInitParameter(ActionConstants.IN_USER));
        setPassword(theContext.getInitParameter(ActionConstants.IN_PASSWORD));
        setLanguage(theContext.getInitParameter(ActionConstants.IN_LANGUAGE));
        setCatalog(theContext.getInitParameter(ActionConstants.IN_CATALOG_ID));
        setCatalogVariant(theContext.getInitParameter(ActionConstants.IN_VARIANT_ID));
    }

}
