/*****************************************************************************
  Copyright (c) 2000, SAPMarkets Europe GmbH, Germany, All rights reserved.

  $Id: //sap/ESALES_base/30_SP_COR/src/_pcatAPI/java/com/sap/isa/catalog/trex/TrexCatalogQueryCompiler.java#5 $
  $Revision: #5 $
  $Change: 168739 $
  $DateTime: 2004/02/04 09:26:00 $
*****************************************************************************/
package com.sap.isa.catalog.trex;

// catalog imports


// misc imports
// trex imports
import java.util.List;

import org.apache.struts.util.MessageResources;

import com.sap.isa.catalog.CatalogParameters;
import com.sap.isa.catalog.boi.IFilter;
import com.sap.isa.catalog.filter.CatalogFilter;
import com.sap.isa.catalog.filter.CatalogFilterFactory;
import com.sap.isa.catalog.filter.CatalogFilterInvalidException;
import com.sap.isa.catalog.impl.Catalog;
import com.sap.isa.catalog.impl.CatalogQuery;
import com.sap.isa.catalog.impl.CatalogQueryStatement;
import com.sapportals.trex.TrexException;
import com.sapportals.trex.core.SearchManager;

/**
 * Helper class for the {@link TrexCatalogBuilder}. Instances of this class
 * are stateless. With the <code>transform...Query</code> methods a given query
 * instance is evaluated and the necessary calls are executed against the passed
 * SearchManager instance. Internally a {@link TrexCatalogFilterVisitor} is used to
 * evaluate the filter expression.
 *
 * @version     2.0
 */
class TrexCatalogQueryCompiler
{
    private TrexCatalogFilterVisitor theFilterVisitor;
    private ITrexQuickSearchStrategy theQuickSearchStreategy;

    private MessageResources theMessageResources;

        /**
         * Creates a new instance of the query compiler. Each instance should only be
         * used of one specific Trex builder instance.
         *
         * @param messResources  the resources for the error messages
         */
    TrexCatalogQueryCompiler(MessageResources messResources, ITrexQuickSearchStrategy aStrategy)
    {
        theMessageResources = messResources;
        theQuickSearchStreategy = aStrategy;
        theFilterVisitor = new TrexCatalogFilterVisitor(messResources);
    }

        /**
         * Transforms the given query for products into the Trex specific represenation.
         *
         * @param query    the query which has to be transformed
         * @param defAttr  default list of attributes
         * @param idxAttr  all attributes of the index
         * @param catalog  the catalog which should be built
         * @param searchManager    the search manager instance for which the query was built
         * @exception CatalogFilterInvalidException error in the filter expression
         * @exception TrexException exception from the search engine
         */
    synchronized void transformItemQuery(CatalogQuery query,
                                         List defAttr,
                                         List idxAttr,
                                         Catalog catalog,
                                         String searchEngine,
                                         SearchManager searchManager)
        throws CatalogFilterInvalidException, TrexException
    {
        CatalogQueryStatement statement = query.getCatalogQueryStatement();
        CatalogFilter filter;
        String[] attributes  = statement.getAttributes();
        String orderBy       = null;
        int rangeFrom        = statement.getRangeFrom();
        int rangeTo          = statement.getRangeTo();

            // a default quick search is requested
        if (statement.getStatementAsString() != null)
        {
            String qsearch = statement.getStatementAsString();
            filter = theQuickSearchStreategy.getQuickSearchFilter(qsearch,catalog);
        }
        else {
            filter = statement.getFilter();
        }

            // set default attribute list if necessary
        if(attributes == null || attributes.length == 0)
            attributes = (String[])defAttr.toArray(new String[0]);

            // set sort order if requested -- Trex supports only one attribute
        String[] sortOrder = statement.getSortOrder();
        if(sortOrder != null && sortOrder.length != 0)
            orderBy = sortOrder[0];

            // set default range if necessary
        if(rangeFrom == 0) rangeFrom = TrexCatalogConstants.INTERVAL_BEGIN;
        if(rangeTo == 0)   rangeTo   = CatalogParameters.trexUpperLimit;
        if(rangeFrom >= rangeTo) {
            rangeFrom = TrexCatalogConstants.INTERVAL_BEGIN;
            rangeTo   = CatalogParameters.trexUpperLimit;
        }

            // set return properties
        searchManager.setResultFromTo(rangeFrom, rangeTo);

            // set return attributes
        for(int i=0; i<attributes.length; i++)
            if (!attributes[i].equalsIgnoreCase(TrexCatalogConstants.ATTR_AREA_GUID) &&
                !attributes[i].equalsIgnoreCase(TrexCatalogConstants.ATTR_OBJECT_GUID) &&
                !attributes[i].equalsIgnoreCase(TrexCatalogConstants.ATTR_OBJECT_DESC) )
                searchManager.addRequestedAttribute(attributes[i]);

            // set sort order
        if(orderBy != null && searchEngine.equals(TrexCatalogConstants.ENGINE_DRFUZZY))
            searchManager.addSortAttribute(orderBy, true);

            // set query condition
        theFilterVisitor.evaluate(filter, idxAttr, searchManager, searchEngine);
        return;
    }

        /**
         * Transforms the given query for categories into the Trex specific represenation.<BR/>
         *
         * @param query    the query which has to be transformed
         * @param defAttr  default list of attributes
         * @param idxAttr  all attributes of the index
         * @param info    the info object about the index
         * @param searchManger the searchManager instance for which the query was built
         * @exception CatalogFilterInvalidException error in filter expression
         * @exception TrexException exception from the search engine
         */
    synchronized void transformCategoryQuery(CatalogQuery query,
                                             List defAttr,
                                             List idxAttr,
                                             String searchEngine,
                                             SearchManager searchManager)
        throws CatalogFilterInvalidException, TrexException
    {
        CatalogQueryStatement statement = query.getCatalogQueryStatement();
        CatalogFilter filter = statement.getFilter();
        String[] attributes  = (String[])defAttr.toArray(new String[0]);
        int rangeFrom        = statement.getRangeFrom();
        int rangeTo          = statement.getRangeTo();

            // a default quick search is requested
        if (statement.getStatementAsString() != null)
        {
            filter = (CatalogFilter)CatalogFilterFactory.getInstance().
                createAttrContainValue(TrexCatalogConstants.ATTR_DESCRIPTION,
                                       statement.getStatementAsString());
        }

            // if category specific only the children categories are searched for
        if (query.isCategorySpecificSearch())
        {
            IFilter cond = CatalogFilterFactory.getInstance().
                createAttrEqualValue( TrexCatalogConstants.ATTR_PARENT_AREA_GUID,
                                      query.getParent().getGuid());

            filter = (CatalogFilter)CatalogFilterFactory.getInstance().
                createAnd(filter, cond);

        }

            // determine the search range
        if(rangeFrom == 0) rangeFrom = TrexCatalogConstants.INTERVAL_BEGIN;
        if(rangeTo == 0)   rangeTo   = CatalogParameters.trexUpperLimit;
        if(rangeFrom >= rangeTo)
        {
            rangeFrom = TrexCatalogConstants.INTERVAL_BEGIN;
            rangeTo   = CatalogParameters.trexUpperLimit;
        }

            // set return properties
        searchManager.setResultFromTo(rangeFrom, rangeTo);

            // set return attributes
        for(int i = 0; i < attributes.length; i++ )
            searchManager.addRequestedAttribute(attributes[i]);

            // set sort order
        String[] sortOrder = statement.getSortOrder();
        if( sortOrder != null &&
            sortOrder.length != 0 &&
            searchEngine.equals(TrexCatalogConstants.ENGINE_DRFUZZY))
            searchManager.addSortAttribute(sortOrder[0], true);

            // set query condition
        theFilterVisitor.evaluate(filter,
                                  idxAttr,
                                  searchManager,
                                  searchEngine);
        return;
    }

        /**
         * Transforms a filtered search statement. This method can be used independent
         * of a catalog instance or the catalog builder. If inconsistent parameters are passed
         * then default values will be taken. Currently this method is used by the
         * Trex adminstration console. <br>
         *
         * @param searchManager the search manager instance for which the query was built
         * @param idxAttr    default list of attributes
         * @param filter     filter of the query
         * @param attributes all attributes of the index
         * @param orderBy    sort order of the query
         * @param rangeFrom  begin of search interval
         * @param rangeTo    end of search interval
         * @param searchEngine name of search engine to be used
         * @exception CatalogFilterInvalidException error in filter expression
         * @exception TrexException exception from the search engine
         */
    synchronized void transformFilterQuery(SearchManager searchManager,
                                           List idxAttr,
                                           CatalogFilter filter,
                                           String[] attributes,
                                           String orderBy,
                                           int rangeFrom,
                                           int rangeTo,
                                           String searchEngine)
        throws CatalogFilterInvalidException, TrexException
    {
            // set return interval
        if(rangeFrom == 0) rangeFrom = TrexCatalogConstants.INTERVAL_BEGIN;
        if(rangeTo == 0)   rangeTo   = CatalogParameters.trexUpperLimit;
        if(rangeFrom >= rangeTo)
        {
            rangeFrom = TrexCatalogConstants.INTERVAL_BEGIN;
            rangeTo   = CatalogParameters.trexUpperLimit;
        }
        searchManager.setResultFromTo(rangeFrom, rangeTo);

            // set return attributes
        if(attributes == null)
            attributes = (String[])idxAttr.toArray(new String[0]);

        for(int i = 0; i < attributes.length; i++ )
            searchManager.addRequestedAttribute(attributes[i]);

            // set sort order
        if(orderBy != null)
            searchManager.addSortAttribute(orderBy, true);

            // set query condition
        theFilterVisitor.evaluate(filter, idxAttr, searchManager, searchEngine);
    }
}
