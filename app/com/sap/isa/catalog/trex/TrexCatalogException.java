/*****************************************************************************
  Copyright (c) 2000, SAPMarkets Europe GmbH, Germany, All rights reserved.

  $Id: //sap/ESALES_base/30_SP_COR/src/_pcatAPI/java/com/sap/isa/catalog/trex/TrexCatalogException.java#3 $
  $Revision: #3 $
  $Change: 144303 $
  $DateTime: 2003/08/22 20:18:21 $
*****************************************************************************/
package com.sap.isa.catalog.trex;

import com.sap.isa.catalog.boi.CatalogException;

/**
 * Exception class for all exceptions which occur during calling or working
 * with the Trex catalog implementation.
 *
 * @version     1.0
 */
class TrexCatalogException extends CatalogException
{
        /**
         * Default constructor for exceptions.
         *
         * @param  msg  text to be associated with the exception
         */
    TrexCatalogException(String msg)
    {
        super(msg);
    }

    TrexCatalogException(String mgs, Throwable aCause)
    {
        super(mgs, aCause);
    }
}
