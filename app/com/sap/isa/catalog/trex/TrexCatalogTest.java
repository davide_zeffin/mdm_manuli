/*****************************************************************************
  Copyright (c) 2000, SAPMarkets Europe GmbH, Germany, All rights reserved.

  $Id: //sap/ESALES_base/30_SP_COR/src/_pcatAPI/java/com/sap/isa/catalog/trex/TrexCatalogTest.java#3 $
  $Revision: #3 $
  $Change: 144303 $
  $DateTime: 2003/08/22 20:18:21 $
*****************************************************************************/
package com.sap.isa.catalog.trex;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.Locale;
import java.util.Properties;

import com.sap.isa.catalog.boi.ICatalog;
import com.sap.isa.catalog.boi.ICategory;
import com.sap.isa.catalog.boi.IClient;
import com.sap.isa.catalog.boi.IFilter;
import com.sap.isa.catalog.boi.IQuery;
import com.sap.isa.catalog.boi.IQueryStatement;
import com.sap.isa.catalog.boi.IServer;
import com.sap.isa.catalog.filter.CatalogFilterFactory;
import com.sap.isa.core.TechKey;

/**
 * This class tests the TrexCatalog implementation. This class is for
 * internal use only!
 *
 * @version     1.0
 */
class TrexCatalogTest implements Runnable
{

    TrexCatalogTest()
    {
    }

    public static void main(String[] a)
    {
        TrexCatalogTest cat1 = new TrexCatalogTest();
        //TrexCatalogTest cat2 = new TrexCatalogTest();
        //TrexCatalogTest cat3 = new TrexCatalogTest();

        new Thread(cat1).start();
            //new Thread(cat2).start();
            //new Thread(cat3).start();
    }

    public void run()
    {
        this.test();
    }

        /**
         * Simple test method.
         */
    void test()
    {
        try 
        {
            String name    = "GD_T01_01                     ";//30 character
            String variant = "GD01                          ";//30

            final String myCatalogGuid = name + variant;
            final ArrayList myCatalogViews = new ArrayList();
                //myCatalogViews.add(new IView() { public String getGuid() { return "VIEW1"; } });
                //myCatalogViews.add(new IView() { public String getGuid() { return "VIEW2"; } });

                // helper class
            class CatInfoTest
                implements IClient, IServer
            {
                protected int catalogState = CATALOG_STATE_ACTIVE;
				protected TechKey stagingCatalogKey;
                
                CatInfoTest() { }

                public Locale getLocale() { return Locale.GERMAN; }

                public String getName() { return "muellerfran"; }

                public String getGuid() { return "muellerfran"; }

                public String getPWD()  { return "zorro"; }

                public ArrayList getViews() { return myCatalogViews; }

                public String getURLString() { return "http:\\P49523"; }

                public int getPort() { return 8353; }
                
                public int getCatalogStatus() { return catalogState; }
                
				public TechKey getStagingCatalogKey() { return stagingCatalogKey; }

                public String getCatalogGuid() { return myCatalogGuid; }

                public String getCookie() { return null; }
                
                /**
                 * Sets the catalog state
                 *
                 * @param catalogStatus catalogStatus the state of the catalogue to be read
                 */
                public void setCatalogStatus(int catalogState) {
                    this.catalogState = catalogState;
                }

				/**
				 * Sets the TechKey of the staging catalogue to read
				 *
				 * @param stagingCatalogId the TechKey of the staging catalogue to read!
				 */
				public void setStagingCatalogKey(TechKey stagingCatalogKey) {
					this.stagingCatalogKey = stagingCatalogKey;
				}
        
                public Properties getProperties()
                {
                    return new Properties();
                }

                public Iterator getPermissions()
                {
                    return null;
                }
            }

            //CatInfoTest catInfo = new CatInfoTest();

            //CatalogFactory fac = CatalogFactory.getInstance();
                // start testing
                //ICatalog catalog = fac.getCatalog((IClient)catInfo, (IServer)catInfo);
                //ICatalog catalog2 = fac.getCatalog((IClient)catInfo, (IServer)catInfo);
                //this.testCatalog(catalog);
                //this.testQueryItem(catalog); // query for items
                //this.testCategoryQueryItem(catalog); // categories spec. query for items
                //this.testQueryCategory(catalog); // query for categories
                //this.testCategoryQueryCategory(catalog); // categories spec. query for categories

        }
        catch (Exception ex)
        {
            System.out.print(ex.getMessage());
        }
    }

    private void testCatalog(ICatalog catalog)
    {
        try 
        {
            Iterator rootIter = catalog.getChildCategories();
            catalog.getCategories();
            rootIter = catalog.getChildCategories();
            catalog.getDetails();
            while(rootIter.hasNext())
            {
                ICategory aCat = (ICategory)rootIter.next();
                System.out.println(aCat.getName());
                aCat.getChildCategories();
                aCat.getItems();
                aCat.getDetails();
            }

            catalog.getChildCategories();
            catalog.getCategories();

                //          ICategory cat2 = catalog.getCategory("E402E757270ED511A6E90800062785ED");
                //          cat2.getParent().getChildren();
                //          cat2.getItems();
            Iterator iter = catalog.getCategories();
            while(iter.hasNext()) {
                ICategory aCat = ((ICategory)iter.next());
                aCat.getChildCategories();
                aCat.getItems();
                aCat.getItems();
                aCat.getDetails();
            }
        }
        catch(Exception ex)
        {
            System.out.println(ex.getMessage());
        }
    }

    private void testQueryItem(ICatalog catalog)
    {
        try 
        {
            IQueryStatement statement  = catalog.createQueryStatement();
                // get filter factory
            CatalogFilterFactory fact = CatalogFilterFactory.getInstance();
                // create filter expression
            IFilter attr1    = fact.createAttrContainValue("OBJECT_DESCRIPTION", "A*");
                //IFilter notAttr1 = fact.createNot(attr1);
            IFilter attr2    = fact.createAttrEqualValue("POS_NR", "00020");
            IFilter cond  = fact.createAnd(attr2, attr1);

                //System.out.print(cond);
                // create filtered statement
            statement.setStatement(cond, null, null);

                // finish and sumit query
            IQuery query = catalog.createQuery(statement);
            query.submit();
                // try to change the statement
            statement.setStatement(attr1, null, null);
        }
        catch(Exception ex)
        {
            System.out.println(ex.getMessage());
        }
    }

    private void testCategoryQueryItem(ICatalog catalog)
    {
        try 
        {
            ICategory myCat = catalog.getCategory("E602E757270ED511A6E90800062785ED");
            IQueryStatement statement = myCat.createQueryStatement();
                // get filter factory
            CatalogFilterFactory fact = CatalogFilterFactory.getInstance();
                // create filter expression
            IFilter attr1 = fact.createAttrContainValue("ZHMFARBE", "blau");
            System.out.print(attr1);
                // create filtered statement
            statement.setStatement(attr1, null, null);
                // finish and sumit query
            IQuery query = myCat.createQuery(statement);
            query.submit();
                // now no longer modifiable
            statement.setStatement(attr1, null, null);
        }
        catch(Exception ex)
        {
            System.out.println(ex.getMessage());
        }
    }

    private void testQueryCategory(ICatalog catalog)
    {
        try 
        {
            IQueryStatement statement  = catalog.createQueryStatement();
            statement.setSearchType(IQueryStatement.Type.CATEGORY_SEARCH);
                // get filter factory
            CatalogFilterFactory fact = CatalogFilterFactory.getInstance();
                // create filter expression
            IFilter attr1    = fact.createAttrContainValue("DESCRIPTION", "Unter*");
            IFilter attr2    = fact.createAttrEqualValue("DESCRIPTION", "Tastaturen");
            IFilter cond  = fact.createOr(attr2, attr1);
                // create filtered statement
            statement.setStatement(cond, null, null);
            System.out.println(statement.getName());
                //statement.setStatement(cond, new String[] {"PON", "OBJECT_DESCRIPTION"} , null);
                // finish and sumit query
            IQuery query = catalog.createQuery(statement);
            query.submit();
        }
        catch(Exception ex)
        {
            System.out.println(ex.getMessage());
        }
    }
}