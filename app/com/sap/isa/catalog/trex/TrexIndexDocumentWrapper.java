/*****************************************************************************
  Copyright (c) 2000, SAPMarkets Europe GmbH, Germany, All rights reserved.
  Created:      20 Feb 2002

  $Id: //sap/ESALES_base/30_SP_COR/src/_pcatAPI/java/com/sap/isa/catalog/trex/TrexIndexDocumentWrapper.java#2 $
  $Revision: #2 $
  $Change: 129221 $
  $DateTime: 2003/05/15 17:20:58 $
*****************************************************************************/
package com.sap.isa.catalog.trex;

import com.sapportals.trex.TrexException;
import com.sapportals.trex.core.DocAttribute;
import com.sapportals.trex.core.IndexDocument;

/**
 * TREXIndexDocumentWrapper.java
 *
 *
 * Created: Fri Feb 08 14:30:21 2002
 *
 * @version 1.0
 */

public class TrexIndexDocumentWrapper extends IndexDocument 
{
    public static final String INDEX = "INDEX";
    public static final String DEINDEX = "DEINDEX";
    
    public TrexIndexDocumentWrapper ()
    {
        super();
    }

    public void setDocumentAction(String anAction)
        throws TrexException
    {
        if (anAction.intern()==INDEX) 
        {
            this.setDocumentAction(IndexDocument.DOCUMENT_ACTION_INDEX);
            
        }
        else if (anAction.intern()==DEINDEX) 
        {
            this.setDocumentAction(IndexDocument.DOCUMENT_ACTION_DEINDEX);
        } // end of if ()
        else 
        {
            throw new TrexException(-1,"Unknow Action!");
        } // end of else
        return;
    }

    public void setDocumentLanguage(String theXMLLanguageTag)
    {
        String theLanguageUnderstoodByTrex =
            TrexCatalogServerEngine.translateXMLLanguageTag(theXMLLanguageTag);
        super.setDocumentLanguage(theLanguageUnderstoodByTrex);
        return;
    }

    public void addDocumentAttribute(String aID, String aValue)
        throws TrexException
    {
        addDocumentAttribute(aID,aValue,"",
                              DocAttribute.ATTRIBUTE_OPERATOR_EQ,
                              DocAttribute.ATTRIBUTE_TYPE_TEXT);
        return;
    }
    
    
}// TREXIndexDocumentWrapper
