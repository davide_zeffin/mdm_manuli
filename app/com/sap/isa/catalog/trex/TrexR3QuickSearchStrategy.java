/*****************************************************************************
  Copyright (c) 2000, SAPMarkets Europe GmbH, Germany, All rights reserved.

  $Id: //sap/ESALES_base/21_SP_COR/src/_pcatAPI/java/com/sap/isa/catalog/trex/TrexR3QuickSearchStrategy.java#5 $
  $Revision: #5 $
  $Change: 123050 $
  $DateTime: 2003/04/08 21:28:58 $
*****************************************************************************/
package com.sap.isa.catalog.trex;

import com.sap.isa.catalog.boi.ICatalog;
import com.sap.isa.catalog.boi.IFilter;
import com.sap.isa.catalog.filter.CatalogFilter;
import com.sap.isa.catalog.filter.CatalogFilterFactory;


/**
 * TrexR3QuickSearchStrategy.java
 *
 *
 * Created: Mon Jul 08 09:23:12 2002
 *
 * @version
 */

public class TrexR3QuickSearchStrategy  implements ITrexQuickSearchStrategy 
{
    public TrexR3QuickSearchStrategy ()
    {
        
    }
    
// implementation of com.sap.isa.catalog.trex.ITrexQuickSearchStrategy interface

        /**
         * Returns a simple quicksearch strategy. The search is done in the attributes,
         * TEXT_0001, OBJECT_DESCRIPTION, and R3_MATNR.
         * 
         * @param aQuickSearchString a <code>String</code> value
         * @return a <code>CatalogFilter</code> value
         */
    public CatalogFilter getQuickSearchFilter(String aQuickSearchString,
                                               ICatalog catalog)
        {
            CatalogFilter filter = null;
            CatalogFilterFactory factory = CatalogFilterFactory.getInstance();

                // set query condition (default are the three selected attributes)
            if(aQuickSearchString.equals(TrexCatalogConstants.VALUE_ALL))
                filter = (CatalogFilter)factory.createAttrContainValue(
                    TrexCatalogConstants.ATTR_TEXT_0001, aQuickSearchString);
            else 
            {
                IFilter text =
                    factory.createAttrContainValue(TrexCatalogConstants.ATTR_TEXT_0001, aQuickSearchString);
                IFilter desc =
                    factory.createAttrContainValue(TrexCatalogConstants.ATTR_OBJECT_DESC, aQuickSearchString);
                IFilter or1 = factory.createOr(text, desc);
                    //Current hack replace by configurable strategy
                IFilter id =
                    factory.createAttrContainValue(TrexCatalogConstants.ATTR_R3_MATNR, aQuickSearchString);
                filter = (CatalogFilter)factory.createOr(or1, id);
            }
            return filter;            
        }
}// TrexR3QuickSearchStrategy
