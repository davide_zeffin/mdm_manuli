/*****************************************************************************
  Copyright (c) 2000, SAPMarkets Europe GmbH, Germany, All rights reserved.

  $Id: //sap/ESALES_base/30_SP_COR/src/_pcatAPI/java/com/sap/isa/catalog/trex/TrexCatalogFilterVisitor.java#5 $
  $Revision: #5 $
  $Change: 150872 $
  $DateTime: 2003/09/29 09:18:51 $
*****************************************************************************/
package com.sap.isa.catalog.trex;

// misc imports
import java.util.List;

import org.apache.struts.util.MessageResources;

import com.sap.isa.catalog.ICatalogMessagesKeys;
import com.sap.isa.catalog.filter.CatalogFilter;
import com.sap.isa.catalog.filter.CatalogFilterAnd;
import com.sap.isa.catalog.filter.CatalogFilterAttrContain;
import com.sap.isa.catalog.filter.CatalogFilterAttrEqual;
import com.sap.isa.catalog.filter.CatalogFilterAttrFuzzy;
import com.sap.isa.catalog.filter.CatalogFilterAttrLinguistic;
import com.sap.isa.catalog.filter.CatalogFilterInvalidException;
import com.sap.isa.catalog.filter.CatalogFilterNot;
import com.sap.isa.catalog.filter.CatalogFilterOr;
import com.sap.isa.catalog.filter.CatalogFilterVisitor;
import com.sapportals.trex.TrexException;
import com.sapportals.trex.core.QueryEntry;
import com.sapportals.trex.core.SearchManager;

/**
 * This class transforms the filter expression tree to the Trex specific
 * representation i.e. the calls against the Trex searchManager are performed.
 *
 * @version     1.0
 */
class TrexCatalogFilterVisitor extends CatalogFilterVisitor
{
    private SearchManager theSearchManager;
    private String theSearchEngine;

        // State info: This attribute holds the reference of the parent node of
        // the actual node. This state information is only valid at the entry of
        // the visitor methods! It can be only used in the leaf of the tree,
        // otherwise a stack would be necessary.
    private CatalogFilter theParentFilterNode = null;

        /**
         * Creates a new instance of the Trex specfic visitor of the filter expression
         * tree.
         *
         * @param messResources  the resources for the error messages
         */
    TrexCatalogFilterVisitor(MessageResources messResources)
    {
        super(messResources);
    }

        /**
         * Starts the transformation of the filter expression tree.
         *
         * @param rootFilter  the root of the expression tree
         * @param attributes  the attributes of the context of the filter expression
         * @param searchManager the searchManager for the Trex server
         * @param searchEngine  name of searchEngine to be used
         * @exception CatalogFilterInvalidException error during evaluation of the expression
         */
    void evaluate(CatalogFilter rootFilter, List attributes, SearchManager searchManager,
                  String searchEngine) throws CatalogFilterInvalidException
    {
            // initialization of state information
        theSearchManager = searchManager;
        theSearchEngine = searchEngine;
        theParentFilterNode = null;
            // start traversation of expression tree
        this.start(rootFilter, attributes);
            // clear state
        theSearchManager = null;
        theSearchEngine = null;
        theParentFilterNode = null;
    }

    protected void visitAttrContainFilter(CatalogFilterAttrContain filterNode)
        throws CatalogFilterInvalidException
    {
            // super call
        super.visitAttrContainFilter(filterNode);
            // transformation
        String name    = filterNode.getName();
        String pattern = filterNode.getPattern();
        byte type = QueryEntry.CONTENT_TYPE_AND;
        if(theSearchEngine.equals(TrexCatalogConstants.ENGINE_VERITY))
            type = QueryEntry.CONTENT_TYPE_STRING;

        try 
        {
            QueryEntry entry = new QueryEntry();
            entry.setRowType(QueryEntry.ROW_TYPE_ATTRIBUTE);
            entry.setLocation(name);
            entry.setContentType(type);
            entry.setTermWeight(10000);
            if(theParentFilterNode instanceof CatalogFilterNot)
                entry.setValue(pattern, "", QueryEntry.ATTRIBUTE_OPERATOR_NE);
            else
                entry.setValue(pattern, "", QueryEntry.ATTRIBUTE_OPERATOR_EQ);
            theSearchManager.addQueryEntry(entry);
        } catch(TrexException ex) {
            throw new CatalogFilterInvalidException(ex.getLocalizedMessage());
        }
    }

    protected void visitAttrEqualFilter(CatalogFilterAttrEqual filterNode)
        throws CatalogFilterInvalidException
    {
            // super call
        super.visitAttrEqualFilter(filterNode);
            // transformation
        String name  = filterNode.getName();
        String value = filterNode.getValue();
        try {
            QueryEntry entry = new QueryEntry();
            entry.setRowType(QueryEntry.ROW_TYPE_ATTRIBUTE);
            entry.setLocation(name);
            entry.setContentType(QueryEntry.CONTENT_TYPE_STRING);
            entry.setTermWeight(10000);
            if(theParentFilterNode instanceof CatalogFilterNot)
                entry.setValue(value, "", QueryEntry.ATTRIBUTE_OPERATOR_NE);
            else
                entry.setValue(value, "", QueryEntry.ATTRIBUTE_OPERATOR_EQ);
            theSearchManager.addQueryEntry(entry);
        }
        catch(TrexException ex)
        {
            throw new CatalogFilterInvalidException(ex.getLocalizedMessage());
        }
    }

    protected void visitAttrLinguisticFilter(CatalogFilterAttrLinguistic filterNode)
        throws CatalogFilterInvalidException
    {
            // super call
        super.visitAttrLinguisticFilter(filterNode);
            // transformation
        String name    = filterNode.getName();
        String pattern = filterNode.getPattern();
        byte type      = QueryEntry.CONTENT_TYPE_AND;

            // unfortunately Verity does not support linguistic search!
        if(theSearchEngine.equals(TrexCatalogConstants.ENGINE_VERITY))
        {
            String msg = getMessageResources().getMessage(
                ICatalogMessagesKeys.PCAT_FILTER_NOT_SUPP_EXPR, filterNode);
            throw new CatalogFilterInvalidException(msg);
        }

        try 
        {
            QueryEntry entry = new QueryEntry();
            entry.setRowType(QueryEntry.ROW_TYPE_ATTRIBUTE);
            entry.setLocation(name);
            entry.setContentType(type);
            entry.setTermWeight(10000);
            entry.setTermAction(QueryEntry.TERM_ACTION_LINGUISTIC);
            if(theParentFilterNode instanceof CatalogFilterNot)
                entry.setValue(pattern, "", QueryEntry.ATTRIBUTE_OPERATOR_NE);
            else
                entry.setValue(pattern, "", QueryEntry.ATTRIBUTE_OPERATOR_EQ);
            theSearchManager.addQueryEntry(entry);
        }
        catch(TrexException ex)
        {
            throw new CatalogFilterInvalidException(ex.getLocalizedMessage());
        }
    }

    protected void visitAttrFuzzyFilter(CatalogFilterAttrFuzzy filterNode)
        throws CatalogFilterInvalidException
    {
            // super call
        super.visitAttrFuzzyFilter(filterNode);
            // transformation
        String name    = filterNode.getName();
        String pattern = filterNode.getPattern();
        int weight     = 3000;
        byte type      = QueryEntry.CONTENT_TYPE_AND;

            // unfortunately Verity does not support fuzzy search!
        if(theSearchEngine.equals(TrexCatalogConstants.ENGINE_VERITY))
        {
            String msg = getMessageResources().getMessage(
                ICatalogMessagesKeys.PCAT_FILTER_NOT_SUPP_EXPR, filterNode);
            throw new CatalogFilterInvalidException(msg);
        }

            // set default weight if necessary
        try 
        {
            if(filterNode.getParameter("TERM_WEIGHT") != null)
                weight = Integer.valueOf(filterNode.getParameter("TERM_WEIGHT")).intValue();
        }
        catch(NumberFormatException numbEx)
        {
            weight = 3000;
        }

        try 
        {
            QueryEntry entry = new QueryEntry();
            entry.setRowType(QueryEntry.ROW_TYPE_ATTRIBUTE);
            entry.setLocation(name);
            entry.setContentType(type);
            entry.setTermWeight(weight);
            entry.setTermAction(QueryEntry.TERM_ACTION_FUZZY);
            if(theParentFilterNode instanceof CatalogFilterNot)
                entry.setValue(pattern, "", QueryEntry.ATTRIBUTE_OPERATOR_NE);
            else
                entry.setValue(pattern, "", QueryEntry.ATTRIBUTE_OPERATOR_EQ);
            theSearchManager.addQueryEntry(entry);
        }
        catch(TrexException ex)
        {
            throw new CatalogFilterInvalidException(ex.getLocalizedMessage());
        }
    }

    protected void visitNotFilter(CatalogFilterNot filterNode)
        throws CatalogFilterInvalidException
    {
            // super call
        super.visitNotFilter(filterNode);
            // transformation
        CatalogFilter operand = (CatalogFilter)filterNode.getOperands().get(0);
        theParentFilterNode = filterNode;
        operand.assign(this);
        try 
        {
            if(!operand.isLeaf())
            {
                    // unfortunately Dr Fuzzy does not support the NOT operator!
                if(theSearchEngine.equals(TrexCatalogConstants.ENGINE_DRFUZZY))
                {
                    String msg = getMessageResources().getMessage(
                        ICatalogMessagesKeys.PCAT_FILTER_NOT_SUPP_EXPR, filterNode);
                    throw new CatalogFilterInvalidException(msg);
                }
                    // in case of Verity it should work
                QueryEntry entry = new QueryEntry();
                entry.setRowType(QueryEntry.ROW_TYPE_OPERATOR);
                entry.setValue(QueryEntry.QUERY_OPERATOR_NOT, "", QueryEntry.ATTRIBUTE_OPERATOR_EQ);
                theSearchManager.addQueryEntry(entry);
            }
        }
        catch(TrexException ex)
        {
            throw new CatalogFilterInvalidException(ex.getLocalizedMessage());
        }
    }

    protected void visitOrFilter(CatalogFilterOr filterNode)
        throws CatalogFilterInvalidException
    {
            // super call
        super.visitOrFilter(filterNode);
            // transformation - inorder
        CatalogFilter lOperand = (CatalogFilter)filterNode.getOperands().get(0);
        theParentFilterNode = filterNode;
        lOperand.assign(this);
        CatalogFilter rOperand = (CatalogFilter)filterNode.getOperands().get(1);
        theParentFilterNode = filterNode;
        rOperand.assign(this);
        try 
        {
            QueryEntry entry = new QueryEntry();
            entry.setRowType(QueryEntry.ROW_TYPE_OPERATOR);
            entry.setValue(QueryEntry.QUERY_OPERATOR_OR, "", QueryEntry.ATTRIBUTE_OPERATOR_EQ);
            theSearchManager.addQueryEntry(entry);
        }
        catch(TrexException ex)
        {
            throw new CatalogFilterInvalidException(ex.getLocalizedMessage());
        }
    }

    protected void visitAndFilter(CatalogFilterAnd filterNode)
        throws CatalogFilterInvalidException
    {
            // super call
        super.visitAndFilter(filterNode);
            // transformation - inorder
        CatalogFilter lOperand = (CatalogFilter)filterNode.getOperands().get(0);
        theParentFilterNode = filterNode;
        lOperand.assign(this);
        CatalogFilter rOperand = (CatalogFilter)filterNode.getOperands().get(1);
        theParentFilterNode = filterNode;
        rOperand.assign(this);
        try 
        {
            QueryEntry entry = new QueryEntry();
            entry.setRowType(QueryEntry.ROW_TYPE_OPERATOR);
            entry.setValue(QueryEntry.QUERY_OPERATOR_AND, "", QueryEntry.ATTRIBUTE_OPERATOR_EQ);
            theSearchManager.addQueryEntry(entry);
        } catch(TrexException ex) {
            throw new CatalogFilterInvalidException(ex.getLocalizedMessage());
        }
    }
}
