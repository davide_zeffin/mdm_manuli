/*****************************************************************************
  Copyright (c) 2000, SAPMarkets Europe GmbH, Germany, All rights reserved.

  $Id: //sap/ESALES_base/21_SP_COR/src/_pcatAPI/java/com/sap/isa/catalog/trex/ITrexQuickSearchStrategy.java#4 $
  $Revision: #4 $
  $Change: 123050 $
  $DateTime: 2003/04/08 21:28:58 $
*****************************************************************************/
package com.sap.isa.catalog.trex;

import com.sap.isa.catalog.boi.ICatalog;
import com.sap.isa.catalog.filter.CatalogFilter;

/**
 * ITrexQuickSearchStrategy.java
 *
 * A simple Interface to provide an arbitray filter
 * {@link com.sap.isa.catalog.filter.CatalogFilter} created from an
 * user input.
 *
 * Created: Mon Jul 08 07:46:55 2002
 *
 * @version 1.0
 */

public interface ITrexQuickSearchStrategy 
{
        /**
         * Provides a filter for a quicksearch. The default strategy will be to
         * search via 'contains' in three attributes named
         * OBJECT_DESCRIPTION, OBJECT_ID, and TEXT_001 (in that order).
         * You can also specify a list of attributes to use within the catalog
         * configuration settings (in the catalog-site-config.xml).
         * If you would like to provide a different Implementation provide a class name in the
         * server config file. The class must implement the Interface and have a default
         * constructor.
         *
         * @param theQuickSearchString a <code>String</code> value from the input
         * @param catalog a <code>TrexCatalog</code> against which to query
         * @return an <code>IFilter</code> value
         */
    CatalogFilter getQuickSearchFilter(String aQuickSearchString,
                                       ICatalog catalog);    

}// ITrexQuickSearchStrategy
