/*****************************************************************************
  Copyright (c) 2000, SAPMarkets Europe GmbH, Germany, All rights reserved.
  Created:      28 Feb 2002

  $Id: //sap/ESALES_base/30_SP_COR/src/_pcatAPI/java/com/sap/isa/catalog/trex/TrexManagedCatalog.java#5 $
  $Revision: #5 $
  $Change: 181616 $
  $DateTime: 2004/04/02 18:22:57 $
*****************************************************************************/
package com.sap.isa.catalog.trex;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;

import org.apache.struts.util.MessageResources;
import org.w3c.dom.Document;

import com.sap.isa.catalog.ICatalogMessagesKeys;
import com.sap.isa.catalog.boi.CatalogException;
import com.sap.isa.catalog.boi.IClient;
import com.sap.isa.catalog.boi.IComponent;
import com.sap.isa.catalog.boi.IServerEngine;
import com.sap.isa.catalog.boi.IView;
import com.sap.isa.catalog.impl.Catalog;
import com.sap.isa.catalog.impl.CatalogClient;
import com.sap.isa.catalog.impl.CatalogView;
import com.sap.isa.core.logging.IsaLocation;

public class TrexManagedCatalog
    extends Catalog
    implements TrexCatalogConstants
{
    // the logging instance
    private static IsaLocation theStaticLocToLog =
        IsaLocation.getInstance(TrexManagedCatalog.class.getName());
        // Catalog infos
    private String theIndexID;
    private String   theVariant;
    private HashMap  theIndexInfo;
    private String   theReplTimeStamp = "";
    
    public TrexManagedCatalog(TrexCatalogServerEngine aTrexServer,
                              IClient aClient,
                              MessageResources aMessageResource,
                              String aCatalogGuid,
                              String aIndexID,
                              String theLanguage)
    {
        super(aTrexServer,aCatalogGuid);
        this.theIndexID = aIndexID;
        theClient = new CatalogClient(aClient);
        this.theDefaultLocale = new Locale(theLanguage.toLowerCase(),"");
        this.theSupportedLocales.add(theDefaultLocale);
        theMessageResources = aMessageResource;
        theServerEngine=aTrexServer;
        setNameInternal(getNameFromGuid(aCatalogGuid));
        setDescriptionInternal("TrexManagedCatalog(guid=" + aCatalogGuid + ")");
        theIndexInfo = new HashMap();
            // create corresponding builder
        setCatalogBuilder(new TrexManagedCatalogBuilder(this));
        try
        {
            //JP: preload the index meta info for Aindex and Pindex
            getIndexInfo(INDEX_LEVEL_A, "");
            getIndexInfo(INDEX_LEVEL_P, "");
        }
        catch(TrexCatalogException ex)
        {
            String msg = "unable to load index for catalogGuid " + aCatalogGuid;
            theStaticLocToLog.error(ICatalogMessagesKeys.PCAT_EXCEPTION,
                                    new Object[] { msg }, ex);
        }
    }

    public String getLanguage()
    {
        return theDefaultLocale.getLanguage();
    }
    
    public String getIndexID()
    {
        return this.theIndexID;
    }

    //JP: has to be re-thought if the format of the CatalogGuid will change!!!
    //JP: fails on catalog names containing underscore -> search underscore
    //    in CatalogGuid from left to right (variant name must not contain "_")
    private static String getNameFromGuid(String aCatalogGuid)
    {
            //divide the catalog name from the variant
        int index = aCatalogGuid.lastIndexOf("_");
        return aCatalogGuid.substring(0, index);
    }

        /**
         * Generates a key for the cache from the passed parameters.<br>
         * This method is called from the {@link com.sap.isa.catalog.cache.CatalogCache}
         * and has to be <code>public static</code>.
         *
         * @param aMetaInfo  meta information from the EAI layer
         * @param aClient    the client spec of the catalog
         * @param aServer    the server spec of the catalog
         */
    public static String generateCacheKey(
        String theClassName,
        IClient aClient,
        IServerEngine aServer)
    {
            // create views id
        StringBuffer views = new StringBuffer();
        List viewList = aClient.getViews();
        Iterator iter = viewList.iterator();
        while(iter.hasNext())
        {
            IView view = (IView)iter.next();
            views.append(view.getGuid());
        }

        String theKey =aServer.getURLString() +
            aServer.getPort() +
            aServer.getCatalogGuid() +
            views.toString();
            // now build and return the key
        return theKey;
    }

    synchronized public boolean isCachedItemUptodate()
    {
        if ( getState()==IComponent.State.DELETED) 
        {
            return false;
        } // end of if ()
        return theServerEngine.isCatalogUpToDate(this);
    }

        /**
         * Adds an information object about an index to the list of index
         * information objects.
         *
         * @param index  information object about an index
         */
    void addIndexInfo(TrexManagedCatalogIndexInfo index)
    {
        synchronized(theIndexInfo)
        {
            theIndexInfo.put(index.generateKey(), index);
        }
    }

        /**
         * Returns an information object about an index.
         *
         * @param indexTypeID 	type of the index (S ,P, A)
         * @param guid      		guid of the catalog (if 'P' index) or of the category  (if 'S' index)
         * @return          			an information object about an index
         * @exception TrexCatalogException connection error to the CRM system
         */
    TrexManagedCatalogIndexInfo getIndexInfo(
        String indexTypeID,
        String categoryID)
        throws TrexCatalogException
    {
        TrexManagedCatalogIndexInfo info = null;
        String key = TrexManagedCatalogIndexInfo.generateKey(indexTypeID, categoryID);

        synchronized(theIndexInfo)
        {
            info = (TrexManagedCatalogIndexInfo)theIndexInfo.get(key);
            if (info == null)
            {
                info = ((TrexCatalogServerEngine)theServerEngine).buildIndexInfo(this,
                                                                       indexTypeID,
                                                                       categoryID);
                addIndexInfo(info);
            }
        }
        return info;
    }

    public void commitChangesRemote(Document theChanges)
        throws CatalogException
    {
        throw new UnsupportedOperationException("No yet implemented!");
    }

        /**
         * Returns the views associated with the catalog.
         *
         * @return  array of views
         */
    String[] getViews()
    {	
        ArrayList views =theClient.getViews();
        
        if (theStaticLocToLog.isDebugEnabled())
	        theStaticLocToLog.debug("getViews(): views size: " + views.size());
        
        String[] result = new String[views.size()];
        
        for (int i = 0;i<views.size();i++){
    	    	
    	    CatalogView view = (CatalogView) views.get(i);
    	    result[i] = view.getGuid();
    	    
		    if (theStaticLocToLog.isDebugEnabled()) 
		   	    theStaticLocToLog.debug("added view with id: "+ view.getGuid());
        }	
        return result;
    }

    String getReplTimeStamp()
    {
        return theReplTimeStamp;
    }
	/**
	  * Returns the status of the (backend) Catalog.
	  *
	  * @return  status of the Catalog (either OK or ERROR)
	  */
	public int getCatalogStatus()
	{
		theStaticLocToLog.debug("getCatalogStatus:" );
		TrexCatalogServerEngine theTrexCatalogServerEngine =
					(TrexCatalogServerEngine)theServerEngine;
					
		String theCatalogGuid = theTrexCatalogServerEngine.getCatalogGuid();
		try {
			theTrexCatalogServerEngine.getCatalog(theCatalogGuid);
			theStaticLocToLog.debug("Catalog is available :" + theCatalogGuid);
		} catch (CatalogException e) {
			theStaticLocToLog.error("getCatalog() failed for Guid " + theCatalogGuid + ": " + e.getLocalizedMessage());
			return CATALOG_STATUS_ERROR;
		}
		return CATALOG_STATUS_OK;
	}

    void setReplTimeStamp(String theNewReplTimeStamp)
    {
        theReplTimeStamp=theNewReplTimeStamp;
    }

    public String[] getListOfIndexGuids()
    {
        synchronized(theIndexInfo)
        {
            int count = theIndexInfo.size();
            String[] result = new String[count];
            Iterator it = theIndexInfo.values().iterator();
            count = 0;
            while(it.hasNext())
            {
                TrexManagedCatalogIndexInfo aTrexIndexInfo = (TrexManagedCatalogIndexInfo)it.next();
                result[count] = aTrexIndexInfo.getStoreId();
                count++;
            }
            return result;
        }
    }
    

 	//Note 1040421
	/**
	 * Returns the list of attribute names used for a QuickSearch
	 *
	 * @return  list of attribute names to search for in a QuickSearch
	 */
  	public String[] getQuickSearchAttributes()
  	{
 		return theQuickSearchAttributes;
  	}

}

