package com.sap.isa.catalog.trex;

import com.sap.isa.catalog.boi.ICatalog;
import com.sap.isa.catalog.boi.IFilter;
import com.sap.isa.catalog.filter.CatalogFilter;
import com.sap.isa.catalog.filter.CatalogFilterFactory;

/**
 *
 */

class TrexDefaultQuickSearchStrategy
    implements
        ITrexQuickSearchStrategy
{
    /**
     * @see com.sap.isa.catalog.trex.ITrexQuickSearchStrategy#getQuickSearchFilter(String, ICatalog)
     */
    public CatalogFilter getQuickSearchFilter(
        String aQuickSearchString,
        ICatalog catalog)
    {
        CatalogFilter filter = null;
        CatalogFilterFactory factory = CatalogFilterFactory.getInstance();

        // get the list of QuickSearch attributes or use the default ones
        String[] quickSearchAttributes = catalog.getQuickSearchAttributes();
        if(quickSearchAttributes == null || quickSearchAttributes.length == 0)
            quickSearchAttributes = new String[] {
                TrexCatalogConstants.ATTR_OBJECT_DESC,
                TrexCatalogConstants.ATTR_OBJECT_ID,
                TrexCatalogConstants.ATTR_TEXT_0001 };

        // set query condition (default are the three selected attributes)
        if(aQuickSearchString.equals(TrexCatalogConstants.VALUE_ALL))
          filter = (CatalogFilter)factory.createAttrContainValue(
            quickSearchAttributes[0], aQuickSearchString);
        else {
            filter = (CatalogFilter)factory.createAttrContainValue(
                quickSearchAttributes[0], aQuickSearchString);
            for(int i = 1; i < quickSearchAttributes.length; i++)
            {
                IFilter a2ndFilter =
                    factory.createAttrContainValue(quickSearchAttributes[i], aQuickSearchString);
                filter = (CatalogFilter)factory.createOr(filter, a2ndFilter);
            }
        }

        return filter;            
    }
}

