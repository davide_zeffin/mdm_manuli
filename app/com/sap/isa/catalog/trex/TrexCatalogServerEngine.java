/*****************************************************************************
  Copyright (c) 2000, SAPMarkets Europe GmbH, Germany, All rights reserved.
  Created:      7 February 2002

  $Id: //sap/ESALES_base/30_SP_COR/src/_pcatAPI/java/com/sap/isa/catalog/trex/TrexCatalogServerEngine.java#14 $
  $Revision: #14 $
  $Change: 168739 $
  $DateTime: 2004/02/04 09:26:00 $
*****************************************************************************/
package com.sap.isa.catalog.trex;

import java.io.IOException;
import java.io.InputStream;
import java.io.StringReader;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.Locale;
import java.util.Properties;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Source;
import javax.xml.transform.Templates;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.URIResolver;
import javax.xml.transform.sax.SAXResult;
import javax.xml.transform.sax.SAXSource;
import javax.xml.transform.sax.SAXTransformerFactory;
import javax.xml.transform.sax.TransformerHandler;
import javax.xml.transform.stream.StreamSource;

import org.apache.struts.util.MessageResources;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.w3c.dom.Text;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.XMLReader;
import org.xml.sax.helpers.XMLReaderFactory;

import com.sap.isa.catalog.CatalogParameters;
import com.sap.isa.catalog.ICatalogMessagesKeys;
import com.sap.isa.catalog.boi.CatalogException;
import com.sap.isa.catalog.boi.IActor;
import com.sap.isa.catalog.boi.IAttribute;
import com.sap.isa.catalog.boi.IAttributeValue;
import com.sap.isa.catalog.boi.ICatalog;
import com.sap.isa.catalog.boi.ICatalogInfo;
import com.sap.isa.catalog.boi.ICatalogTask;
import com.sap.isa.catalog.boi.ICategory;
import com.sap.isa.catalog.boi.IClient;
import com.sap.isa.catalog.boi.IDetail;
import com.sap.isa.catalog.boi.IItem;
import com.sap.isa.catalog.boi.IServer;
import com.sap.isa.catalog.boi.IServerEngine;
import com.sap.isa.catalog.boi.IServerEngine.ServerType;
import com.sap.isa.catalog.digester.CallFactoryMethodRule;
import com.sap.isa.catalog.digester.CallMethodRule;
import com.sap.isa.catalog.digester.CallParamRule;
import com.sap.isa.catalog.digester.Digester;
import com.sap.isa.catalog.digester.ObjectCreateRule;
import com.sap.isa.catalog.digester.SetNextRule;
import com.sap.isa.catalog.digester.SetPropertiesRule;
import com.sap.isa.catalog.impl.Catalog;
import com.sap.isa.catalog.impl.CatalogClient;
import com.sap.isa.catalog.impl.CatalogServerEngine;
import com.sap.isa.catalog.impl.CatalogSite;
import com.sap.isa.core.eai.BackendBusinessObjectMetaData;
import com.sap.isa.core.eai.BackendBusinessObjectParams;
import com.sap.isa.core.eai.BackendException;
import com.sap.isa.core.eai.Connection;
import com.sap.isa.core.eai.ConnectionDefinition;
import com.sap.isa.core.eai.ConnectionFactory;
import com.sap.isa.core.eai.sp.jco.BackendBusinessObjectSAP;
import com.sap.isa.core.eai.sp.jco.JCoConnection;
import com.sap.isa.core.eai.sp.jco.JCoConnectionEventListener;
import com.sap.isa.core.eai.sp.jco.JCoManagedConnectionFactory;
import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.core.logging.LogUtil;
import com.sap.isa.core.util.table.ResultData;
import com.sapportals.trex.TrexConst;
import com.sapportals.trex.TrexException;
import com.sapportals.trex.TrexReturn;
import com.sapportals.trex.core.CreateIndexManager;
import com.sapportals.trex.core.DocAttribute;
import com.sapportals.trex.core.IndexDocument;
import com.sapportals.trex.core.IndexManager;
import com.sapportals.trex.core.QueryEntry;
import com.sapportals.trex.core.ResultDocument;
import com.sapportals.trex.core.SearchManager;
import com.sapportals.trex.core.SearchResult;
import com.sapportals.trex.core.TrexConfigManager;
import com.sapportals.trex.core.TrexFactory;
import com.sapportals.trex.core.admin.AdminIndex;
import com.sapportals.trex.core.admin.AdminManager;
import com.sapportals.trex.core.admin.IndexShortDescription;
import com.sapportals.trex.core.admin.ServerIndexList;

/**
 * TrexCatalogServerEngine.java
 * A class representing an index server.
 *
 * Created: Wed Feb 06 08:00:47 2002
 *
 * @version 1.0
 */
public class TrexCatalogServerEngine 
    extends
        CatalogServerEngine
    implements
        BackendBusinessObjectSAP,
        TrexCatalogConstants
{
    // the logging instance
    private static IsaLocation theStaticLocToLog =
        IsaLocation.getInstance(TrexCatalogServerEngine.class.getName());

    private static final String CATALOG_TO_TREX_TEMPLATE =
    "/com/sap/isa/catalog/trex/catalogToTrex.xsl";

    private static final String ADD_MAPPING_TEMPLATE =
    "/com/sap/isa/catalog/trex/addMappings.xsl";

    private String theProtocol;
    private String theQuickSearchStrategy;
    private transient JCoConnection theSAPConnection;

    // GD - Nameserver
    private boolean masterSlaveReplication = false;
    private int lockTimeout = 5;

    private Templates theCatalogToTrexFilter;
    private Templates theAddMappingFilter;
    private Digester theIndexDigester;
    private boolean useQueueServer = true;
    private boolean displayCRMCatalogs = true;
	private boolean rfcUsedForR3Catalogs = false;

        /**
         * Creates a new <code>TrexCatalogServerEngine</code> instance.
         * Only used when created via BEM mechanism.
         */
    public TrexCatalogServerEngine()
    {
        theType = IServerEngine.ServerType.TREX;
        theCatalogToTrexFilter = loadFilter(CATALOG_TO_TREX_TEMPLATE);
        theAddMappingFilter = loadFilter(ADD_MAPPING_TEMPLATE);
        initDigester();
    }

        /**
         * Creates a new <code>TrexCatalogServerEngine</code> instance.
         *
         * @param theGuid a <code>String</code> value
         * @param theSite a <code>CatalogSite</code> value
         * @param theConfigFile a <code>Document</code> value
         * @param theEAIProperties a <code>Properties</code> value
         */
    public TrexCatalogServerEngine (
        String theGuid,
        CatalogSite theSite,
        Document theConfigFile,
        Properties theEAIProperties)
        throws BackendException
    {
        super(theGuid,theSite,theConfigFile);
        this.theProperties.putAll(theEAIProperties);
        theType = IServerEngine.ServerType.TREX;
        theCatalogToTrexFilter = loadFilter(CATALOG_TO_TREX_TEMPLATE);
        theAddMappingFilter = loadFilter(ADD_MAPPING_TEMPLATE);
        initDigester();
    }

        /**************************************** BackendBusinessObject *********/

    public void initBackendObject(Properties theEAIProperties,
                                  BackendBusinessObjectParams aParams)
        throws BackendException
    {
        theProperties.putAll(theEAIProperties);
        Properties theServerconnectionProperties =
            getBackendObjectSupport().
            getMetaData().
            getDefaultConnectionDefinition().
            getProperties();

            //overlay it with the connection properties from the default connection.
        theProperties.putAll(theServerconnectionProperties);

        CatalogSite.CatalogServerParams theParams =(CatalogSite.CatalogServerParams)aParams;
        theGuid=theParams.getGuid();
        theSite = theParams.getSite();
        theMessageResources = theSite.getMessageResources();
        theActor = theSite.getActor();

            //in case the client is a different than in the default connection create a new connection
        setDefaultConnection(theActor);

        Document aConfigFile = theParams.getDocument();
        this.theConfigDocument=aConfigFile;
        init(aConfigFile);

        if (theStaticLocToLog.isDebugEnabled())
        {
            StringBuffer aBuffer = new StringBuffer();
            Enumeration theKeys = theProperties.keys();
            while(theKeys.hasMoreElements())
            {
                String aKey = (String)theKeys.nextElement();
                aBuffer.append(aKey + ":");
                if(aKey.equals("passwd"))
                    aBuffer.append("?");
                else
                    aBuffer.append(theProperties.getProperty(aKey));
                if (theKeys.hasMoreElements())
                {
                    aBuffer.append("; ");
                } // end of if ()
            }

            String aMessage =
                theMessageResources.getMessage(
                    ICatalogMessagesKeys.PCAT_SRV_PROPS,
                    theGuid,
                    aBuffer.toString());

            theStaticLocToLog.debug(aMessage);
        } // end of if ()
        return;
    }

    public void destroyBackendObject()
    {
        return;
    }

        /*************************** BackendBusinessObjectSAP **********************/

        /**
         * This method is used to add JCo-call listener to this back-end object.
         * A JCo listener will be notified before a JCo call is being executed
         * and after the JCo call has been executed.
         *
         * @param listener         a listener for JCo calls
         */
    public void addJCoConnectionEventListener(JCoConnectionEventListener listener)
    {
        theConnectionFactory.addConnectionEventListener(listener);
    }

    public JCoConnection getDefaultJCoConnection(Properties props)
        throws BackendException
    {
        return (JCoConnection) theConnectionFactory.getDefaultConnection(
            getDefaultJCoConnection().getConnectionFactoryName(),
            getDefaultJCoConnection().getConnectionConfigName(),
            props);
    }

    public JCoConnection getDefaultJCoConnection()
    {
        if(theSAPConnection == null)
            setDefaultConnection(getConnectionFactory().getDefaultConnection());

        return theSAPConnection;
    }

    public JCoConnection getJCoConnection(String conKey)
        throws BackendException
    {
        return (JCoConnection)getConnectionFactory().getConnection(conKey);
    }

    public JCoConnection getModDefaultJCoConnection(Properties conDefProps)
        throws BackendException
    {
        ConnectionFactory factory = getConnectionFactory();
        JCoConnection defConn = (JCoConnection)factory.getDefaultConnection();

        return (JCoConnection)factory.getConnection(
            defConn.getConnectionFactoryName(),
            defConn.getConnectionConfigName(),
            conDefProps);
    }

        /****************************** CachableItem ***************************/

        /**
         * Generates a key for the cache from the passed parameters.<br>
         * This method is called from the {@link com.sap.isa.catalog.cache.CatalogCache}
         * and has to be <code>public static</code>.
         *
         * @param aMetaInfo  meta information for the server from the EAI layer
         * @param aClient    the client spec of the catalog
         * @param aServer    the server spec of the catalog
         */
    public static String generateCacheKey(
        BackendBusinessObjectMetaData aMetaInfo,
        IActor aClient,
        IServer aServer)
    {
        StringBuffer aBuffer = new StringBuffer(TrexCatalogServerEngine.class.getName());

        ConnectionDefinition connDef =
            aMetaInfo.getDefaultConnectionDefinition();

            // create system id
            // special properties for the connection
        String system = null;
        Properties servProps = aServer!=null?aServer.getProperties():null;
        if(servProps != null)
        {
            system = generateKeySAPSystem(
                servProps.getProperty(JCoManagedConnectionFactory.JCO_CLIENT),
                servProps.getProperty(JCoManagedConnectionFactory.JCO_R3NAME),
                servProps.getProperty(JCoManagedConnectionFactory.JCO_ASHOST),
                servProps.getProperty(JCoManagedConnectionFactory.JCO_SYSNR));
        }

            // default properties
        if(system == null)
        {
            Properties defProps = connDef.getProperties();
            system = generateKeySAPSystem(
                defProps.getProperty(JCoManagedConnectionFactory.JCO_CLIENT),
                defProps.getProperty(JCoManagedConnectionFactory.JCO_R3NAME),
                defProps.getProperty(JCoManagedConnectionFactory.JCO_ASHOST),
                defProps.getProperty(JCoManagedConnectionFactory.JCO_SYSNR));
        }

        String theServerGUID =
            (aServer!=null?
             aServer.getGuid():
             aMetaInfo.getBackendBusinessObjectConfig().getType());

        aBuffer.append(theServerGUID);
        aBuffer.append(system);

            // now build and return the key
        return aBuffer.toString();
    }

        /**
         * Generates a key from the passed parameters for a SAP system.
         * If the parameters are incomplete so that no valid key can be
         * built <code>null</code> will be returned.
         *
         * @param client  the client of the r/3 system
         * @param r3name  the name of the r/3 system
         * @param ashost  the application server
         * @param sysnr   the system number
         * @return key for a SAP system
         */
    private static String generateKeySAPSystem(String client, String r3name,
                                               String ashost, String sysnr)
    {
        String key = null;

        if(client != null && client.length() != 0)
        {
            if(r3name != null && r3name.length() != 0)
                key = r3name + client;

            else if(ashost != null && ashost.length() != 0 && sysnr != null && sysnr.length() != 0)
                key = ashost + sysnr + client;
        }
        return key;
    }

    public boolean isCachedItemUptodate()
    {
        return true;
    }

    public ITrexQuickSearchStrategy getQuickSearchStrategy()
    {
        Class aClazz = null;
        ITrexQuickSearchStrategy anOb = new TrexDefaultQuickSearchStrategy();
        if (theQuickSearchStrategy!= null)
        {
            try
            {
                aClazz = Class.forName(theQuickSearchStrategy);
                anOb = (ITrexQuickSearchStrategy) aClazz.newInstance();
            }
            catch (ClassNotFoundException cnfe)
            {
                theStaticLocToLog.error(LogUtil.APPS_COMMON_INFRASTRUCTURE, ICatalogMessagesKeys.PCAT_EXCEPTION,
                                new Object[]{cnfe.getLocalizedMessage()},
                                cnfe);
            } // end of try-catch
            catch (InstantiationException ie )
            {
                theStaticLocToLog.error(LogUtil.APPS_COMMON_INFRASTRUCTURE, ICatalogMessagesKeys.PCAT_EXCEPTION,
                                new Object[]{ie.getLocalizedMessage()},
                                ie);
            } // end of catch
            catch ( IllegalAccessException iae)
            {
                theStaticLocToLog.error(LogUtil.APPS_COMMON_INFRASTRUCTURE, ICatalogMessagesKeys.PCAT_EXCEPTION,
                                new Object[]{iae.getLocalizedMessage()},
                                iae);
            } // end of catch
        }
	// end of if ()
        return anOb;
    }

        /*************************** IServer ***********************************/

    public String getURLString()
    {
		try {
			TrexConfigManager tcm = TrexFactory.getTrexConfigManager();
			return tcm.getMasterNameServer();
		}catch (TrexException ex){
			theStaticLocToLog.error(LogUtil.APPS_COMMON_INFRASTRUCTURE,ICatalogMessagesKeys.PCAT_EXCEPTION,
											new Object[]{ex.getLocalizedMessage()},
											ex);			
		}
        return null; // theProtocol+":\\"+theHttpServerHost + "\\" + theHttpFile;
    }

    /**
     * GD - NameServer
     */
    public boolean isMasterSlaveReplicationEnabled()
    {
      return masterSlaveReplication;
    }

    public int getLockTimeout()
    {
      return lockTimeout;
    }

    public class TrexNameServer
    {
        private String theNameServerURL = null;
        private String theReplicationScriptURL = null;

        public TrexNameServer(String nameServerURL, String replicationScriptURL)
        {
            theNameServerURL = nameServerURL;
            theReplicationScriptURL = replicationScriptURL;
        }

        public String getNameServerURL()
        {
          return theNameServerURL;
        }

        public String getReplicationScriptURL()
        {
          return theReplicationScriptURL;
        }
    }

    private ArrayList theNameServerList = null;

    /**
     * retrieve the list of TrexNameServer configurations found in the
     * configuration file catalog- site- config. xml
     * @return ArrayList of TrexNameServer or null if none is configured
     */
    public ArrayList getNameServerList()
    {
        return theNameServerList;
    }
    private void addNameServer(TrexNameServer nameServer)
    {
        if(theNameServerList == null)
            theNameServerList = new ArrayList();
        theNameServerList.add(nameServer);
    }

        /************************************************************************/


        /**
         * Sets the default connection to the system where the infos about the
         * catalog are located.
         *
         * @param conn  the default connection
         */
    private void setDefaultConnection(Connection conn)
    {
        theSAPConnection = (JCoConnection)conn;
    }

    synchronized public void useQueueServer(boolean useQueueServer)
    {
        this.useQueueServer=useQueueServer;
    }

        /****** implementaions of abstract CatalogServerEngine methods **********/


    protected void init(Document theConfigFile)
    {
        Element theConfigFileElement = (Element)theConfigFile.getElementsByTagName("ServerConfig").item(0);

        Element theQueueServerElement  =(Element)
            theConfigFileElement.getElementsByTagName("trex:queueServer").item(0);
        if ( theQueueServerElement!= null)
        {
            String aBoolean = theQueueServerElement.getAttribute("isUsed");
            this.useQueueServer = Boolean.valueOf(aBoolean).booleanValue();
        } // end of if ()

        Element theCRMCatalogsElement  =(Element)
            theConfigFileElement.getElementsByTagName("trex:CRMCatalogs").item(0);
        if ( theCRMCatalogsElement != null)
        {
            String aBoolean = theCRMCatalogsElement.getAttribute("display");
            this.displayCRMCatalogs = Boolean.valueOf(aBoolean).booleanValue();
        } // end of if ()
		Element theR3CatalogsElement  =(Element)
		theConfigFileElement.getElementsByTagName("trex:R3Catalogs").item(0);
		if ( theR3CatalogsElement != null)
		{
			String aBoolean = theR3CatalogsElement.getAttribute("rfcUsed");
			this.rfcUsedForR3Catalogs = Boolean.valueOf(aBoolean).booleanValue();
		} // end of if ()

        Element theSearchElement  =(Element)
            theConfigFileElement.getElementsByTagName("trex:QuickSearchStrategy").item(0);
        if ( theSearchElement != null)
        {
            String aChild = ((Text)theSearchElement.getFirstChild()).getData();
            aChild.trim();
            this.theQuickSearchStrategy = aChild;
        } // end of if ()

        /**
         * GD - Added for Nameserver
         */
        Element theMasterSlaveEnabledElement = (Element)
            theConfigFileElement.getElementsByTagName("trex:masterSlaveEnabled").item(0);
        if ( theMasterSlaveEnabledElement != null)
        {
            String aBoolean = ((Text)theMasterSlaveEnabledElement.getFirstChild()).getData();
            this.masterSlaveReplication = Boolean.valueOf(aBoolean).booleanValue();
        }
        Element theLockTimeout = (Element)
        theConfigFileElement.getElementsByTagName("trex:lockTimeout").item(0);
        if (theLockTimeout != null)
        {
            this.lockTimeout = Integer.valueOf(
                ((Text)theLockTimeout.getFirstChild()).getData()).intValue();
        }
        // GD - Nameserver end

        MessageResources theMessageResources = theSite.getMessageResources();
        try{
				TrexConfigManager tcm = TrexFactory.getTrexConfigManager();
				String namesServeraddress = tcm.getMasterNameServer();	
				theStaticLocToLog.info(LogUtil.APPS_COMMON_INFRASTRUCTURE,  
					ICatalogMessagesKeys.PCAT_TREX_SRV_INIT_DATA, new Object[] {namesServeraddress});
        }
        catch(TrexException ex)
        {
				theStaticLocToLog.debug("Unable to retrieve Trex nameserver address " , ex);
        }

        if (displayCRMCatalogs)
        {
                //use the supplied catalogs as filter
            NodeList aCatalogList = theConfigFile.getElementsByTagName("Catalog");
            if (aCatalogList!=null && aCatalogList.getLength()>0)
            {
                buildCatalogInfosRemote(aCatalogList);
            }
        } // end of if ()

		buildManagedCatalogInfos();
	
		return;
    }

        /**
         * Describe <code>createCatalog</code> method here.
         *
         * @return an <code>ICatalog</code> value
         * @exception CatalogException if an error occurs
         */
    public ICatalog createCatalog()
        throws CatalogException
    {
        throw new UnsupportedOperationException("Not implemented yet!");
    }

        /**
         * Describe <code>createCatalog</code> method here.
         *
         * @param aSchedule an <code>ICatalogTask.Schedule</code> value
         * @return an <code>ICatalog</code> value
         * @exception CatalogException if an error occurs
         */
    public ICatalog createCatalog(ICatalogTask.Schedule aSchedule)
        throws CatalogException
    {
        throw new UnsupportedOperationException("Not implemented yet!");
    }

        /**
         * Describe <code>hasServerFeature</code> method here.
         *
         * @param aFeature an <code>IServerEngine.ServerFeature</code> value
         * @return a <code>boolean</code> value
         */
    public boolean hasServerFeature(IServerEngine.ServerFeature aFeature)
    {
        throw new UnsupportedOperationException("Not implemented yet!");
    }

        /**
         * Describe <code>getServerFeatures</code> method here.
         *
         * @return an <code>IServerEngine.ServerFeature[]</code> value
         */
    public IServerEngine.ServerFeature[] getServerFeatures()
    {
        throw new UnsupportedOperationException("Not implemented yet!");
    }

    protected Catalog createCatalogRemote()
    {
        return null;
    }

    /**
     * gd -- added status
     * This API is to be used only by the admin UI as of current
     */
    public Catalog getCatalog(ICatalogInfo catalogInfo) throws CatalogException
    {
      if (catalogInfo instanceof CatalogServerEngine.CatalogInfo)
      {
        return getCatalogRemote(((CatalogServerEngine.CatalogInfo)catalogInfo), getActorAsClient());
      }
      return null;
    }

    // JP: added as convenience method for the admin UI
    // with parameter "refresh == true" we first rebuild our internal structures
    synchronized public Iterator getCatalogInfos(boolean refresh)
    {
        if(refresh)
        {
            rebuildManagedCatalogInfos();
            if(displayCRMCatalogs)
                buildCatalogInfosRemote("");
        }
        return theCatalogInfos.iterator();
    }

    private IClient getActorAsClient()
    {
        if (theActor instanceof IClient )
        {
            return (IClient) theActor;
        } // end of if ()
        else
        {
            CatalogClient aClient = new CatalogClient(theActor);
            return aClient;
        } // end of else
    }

        /**
         * Instantiate a new instance of a TrexCatalog.
         * Only called if the catalog was not cached.
         *
         * @param aCatalogInfo a <code>CatalogServerEngine.CatalogInfo</code> value
         * @param aClient an <code>IClient</code> value
         * @return a <code>Catalog</code> value
         * @exception CatalogException if an error occurs
         */
    protected Catalog getCatalogRemote(
        CatalogServerEngine.CatalogInfo aCatalogInfo,
        IClient aClient)
        throws CatalogException
    {
        Catalog aNewCatalog = null;

        TrexManagedCatalogInfo aManagedCatalogInfo =
            (TrexManagedCatalogInfo) aCatalogInfo;
        aNewCatalog =
            new TrexManagedCatalog(
                this,
                aClient,
                this.theMessageResources,
                aManagedCatalogInfo.getGuid(),
                aManagedCatalogInfo.getIndexId(),
                aManagedCatalogInfo.getLanguage());

        return aNewCatalog;
    }

        /**
         * Add an input source (valid catalog XML) as a new catalog to this trex
         * server. Any internal errors will be wrapped and re-thrown as
         * CatalogExceptions. Trex needs a mapping document!
         *
         * @param aSource an <code>InputSource</code>
         * @param aMappingSource a <code>Source</code> value
         * @exception CatalogException if an error occurs
         */
    protected void addCatalogRemote(
        InputSource aSource,
        Source aMappingSource)
        throws CatalogException
    {
            //A:store locally to do the locale preprocessing
            //B:determine available locales
            //C:create respectively update the indices with all available locales
        try
        {
        	TransformerFactory tFactory = TransformerFactory.newInstance();
            if (tFactory.getFeature(SAXSource.FEATURE) && tFactory.getFeature(SAXResult.FEATURE))
            {
                    // Cast the TransformerFactory to SAXTransformerFactory.
                SAXTransformerFactory saxTFactory = ((SAXTransformerFactory) tFactory);

                    // Create a TransformerHandler for each stylesheet.
                TransformerHandler addMappingHandler =
                    saxTFactory.newTransformerHandler(theAddMappingFilter);

                MappingURIResolver aURIResolver = new MappingURIResolver(aMappingSource);

                addMappingHandler.getTransformer().setURIResolver(aURIResolver);

                TransformerHandler toTrexHandler = saxTFactory.newTransformerHandler(theCatalogToTrexFilter);

                String newTrexIndexID = "INDEX_ID" + System.currentTimeMillis();
                toTrexHandler.getTransformer().setParameter("INDEX_ID", newTrexIndexID);

                    // Create an XMLReader.
                XMLReader reader = XMLReaderFactory.createXMLReader();
                reader.setContentHandler(addMappingHandler);

                addMappingHandler.setResult(new SAXResult(toTrexHandler));
                toTrexHandler.setResult(new SAXResult(theIndexDigester));

                    // Parse the XML input document. The input ContentHandler and output ContentHandler
                    // work in separate threads to optimize performance.
                reader.parse(aSource);

                storeIndexStatus(newTrexIndexID + "AIndex", "Valid");   // remapps the "CreatePending" status, which was set by catalogToTrex.xsl
                rebuildManagedCatalogInfos();
            }
        }
        catch(TransformerConfigurationException tce)
        {
			
            theStaticLocToLog.error(ICatalogMessagesKeys.PCAT_EXCEPTION,
                                new Object[]{tce.getLocalizedMessage()},
                                tce);
            throw new CatalogException(tce.getLocalizedMessage(),tce);
        }
        catch (SAXException se)
        {
            theStaticLocToLog.error(ICatalogMessagesKeys.PCAT_EXCEPTION,
                                new Object[]{se.getLocalizedMessage()},
                                se);
            throw new CatalogException(se.getLocalizedMessage(),se);
        }
        catch (IOException ioe)
        {
            theStaticLocToLog.error(ICatalogMessagesKeys.PCAT_EXCEPTION,
                                new Object[]{ioe.getLocalizedMessage()},
                                ioe);
            throw new CatalogException(ioe.getLocalizedMessage(),ioe);
        }
        return;
    }

    // helper method to get a String representation of some time difference
    private static String millis2string(long tdiff)
    {
        int msec = (int)(tdiff % 1000);
        tdiff = (tdiff - msec) / 1000;    // tdiff now is seconds
        int sec = (int)(tdiff % 60);
        tdiff = (tdiff - sec) / 60;       // tdiff now is minutes
        int min = (int)(tdiff % 60);
        tdiff = (tdiff - sec) / 60;       // tdiff now is hours
        StringBuffer sb = new StringBuffer(tdiff + ":");
        if(min < 10)
            sb.append("0");
        sb.append(min + ":");
        if(sec < 10)
            sb.append("0");
        sb.append(sec + ".");
        if(msec < 10)
            sb.append("00");
        else if(msec < 100)
            sb.append("0");
        sb.append(msec);
        return sb.toString();
    }

    protected void addCatalogRemote(Catalog catalog)
        throws CatalogException
    {
        final class DocumentKeyGenerator
        {
            private DecimalFormat guidFormat;
            private long number = 0;
            DocumentKeyGenerator(String prefix)
            {
                guidFormat = new DecimalFormat(prefix + "0000000000");
            }
            String getNewDocumentKey()
            {
                return guidFormat.format(number++).toString();
            }
        }
        final class IndexBuilderLocal
        {
            String newTrexIndexID = "INDEX_ID" + System.currentTimeMillis();

            String theAIndexID = newTrexIndexID + "AIndex";
            String thePIndexID = newTrexIndexID + "PIndex";
            DocumentKeyGenerator theAIndexKeys = new DocumentKeyGenerator("ID_A");
            DocumentKeyGenerator thePIndexKeys = new DocumentKeyGenerator("ID_P");

            String theCatalogName;
            String theCatalogGuid;
            String theCatalogDesc;
            String theCatalogLang;

            IndexManager theAIndexManager;
            IndexManager thePIndexManager;

            int countAIndexDocs = 0;
            int countPIndexDocs = 0;

            IndexBuilderLocal(Catalog catalog)
            {
                theCatalogName = catalog.getName();
                theCatalogGuid = catalog.getGuid();
                theCatalogDesc = catalog.getDescription();
                theCatalogLang = catalog.getDefaultLocale().getLanguage();
            }

            private IndexManager getNewIndexManager(String indexID)
                throws TrexException
            {
                IndexManager indexManager = TrexFactory.getIndexManager();
                indexManager.setIndexId(indexID);
                indexManager.setSearchEngine(TrexConst.SEARCH_ENGINE_DRFUZZY);
                indexManager.useQueueServer(useQueueServer);
                return indexManager;
            }

            void init()
                throws TrexCatalogException
            {
                try
                // create both indexes
                {
                    theAIndexManager = getNewIndexManager(theAIndexID);
                    thePIndexManager = getNewIndexManager(thePIndexID);

                    CreateIndexManager aCreateIndexManager = TrexFactory.getCreateIndexManager();
                    //  the search engine
                    aCreateIndexManager.setSearchEngine(TrexConst.SEARCH_ENGINE_DRFUZZY);
                    aCreateIndexManager.setMiningFlag(true); // must be set if you want to use classification
//JP:deprecated:    aCreateIndexManager.setFilterFlag(true); // use filter software to filter the documents' content (recommended)
                    aCreateIndexManager.useQueueServer(useQueueServer);
                    aCreateIndexManager.setLanguage(theCatalogLang);
                    aCreateIndexManager.setAutoCreateLanguagesFlag(false);

                    //  set the name of the new search index and the access mode
                    aCreateIndexManager.setDescription("AIndex:" + theCatalogGuid);
                    aCreateIndexManager.setIndexId(theAIndexID);
                    aCreateIndexManager.createIndex();

                    aCreateIndexManager.setDescription("PIndex:" + theCatalogGuid);
                    aCreateIndexManager.setIndexId(thePIndexID);
                    aCreateIndexManager.createIndex();

                    // create the root document in AIndex
                    IndexDocument indexDoc = new IndexDocument();
                    indexDoc.setContent(""); // this is the root document in AREA index; there is no valid content
                    // set document key to CATALOG_GUID, as this is the GUID-indicator for the TREX-managed catalog
                    indexDoc.setDocumentKey(theCatalogGuid);
                    indexDoc.setDocumentLanguage(theCatalogLang);
                    indexDoc.setDocumentAction(IndexDocument.DOCUMENT_ACTION_INDEX);

                    indexDoc.addDocumentAttribute(
                        ATTR_AREA, "$ROOT" ,"",
                        DocAttribute.ATTRIBUTE_OPERATOR_EQ,
                        DocAttribute.ATTRIBUTE_TYPE_STRING);
                    indexDoc.addDocumentAttribute(
                        ATTR_AREA_GUID, VALUE_ROOT ,"",
                        DocAttribute.ATTRIBUTE_OPERATOR_EQ,
                        DocAttribute.ATTRIBUTE_TYPE_STRING);
                    indexDoc.addDocumentAttribute(
                        ATTR_CATALOG, theCatalogName ,"",
                        DocAttribute.ATTRIBUTE_OPERATOR_EQ,
                        DocAttribute.ATTRIBUTE_TYPE_STRING);
                    indexDoc.addDocumentAttribute(
                        ATTR_CATALOG_GUID, theCatalogGuid ,"",
                        DocAttribute.ATTRIBUTE_OPERATOR_EQ,
                        DocAttribute.ATTRIBUTE_TYPE_STRING);
                    indexDoc.addDocumentAttribute(
                        ATTR_DESCRIPTION, theCatalogDesc ,"",
                        DocAttribute.ATTRIBUTE_OPERATOR_EQ,
                        DocAttribute.ATTRIBUTE_TYPE_TEXT);  // searchable attribute
                    indexDoc.addDocumentAttribute(
                        "INDEX_STATUS", "CreatePending" ,"",
                        DocAttribute.ATTRIBUTE_OPERATOR_EQ,
                        DocAttribute.ATTRIBUTE_TYPE_STRING);
                    indexDoc.addDocumentAttribute(
                        ATTR_TIMESTAMP, "none" ,"",
                        DocAttribute.ATTRIBUTE_OPERATOR_EQ,
                        DocAttribute.ATTRIBUTE_TYPE_STRING);
                    indexDoc.addDocumentAttribute(
                        ATTR_TEXT_0001, "this is a TrexManaged index for catalog with GUID:" + theCatalogGuid ,"",
                        DocAttribute.ATTRIBUTE_OPERATOR_EQ,
                        DocAttribute.ATTRIBUTE_TYPE_TEXT);  // searchable attribute

                    theAIndexManager.addDocument(indexDoc);
                    theAIndexManager.index();       // burn this onto AIndex
                    theAIndexManager.optimize();    // burn this onto AIndex
                } catch (TrexException te)
                {
                    throw new TrexCatalogException(te.getMessage(),te);
                }
            }// init

            void buildCategoryRec(ICategory category, String parentName, String parentGuid)
                throws CatalogException
            {
                try
                {
                    String areaName = category.getName();
                    String areaGuid = category.getGuid();

                    IndexDocument indexDoc = new IndexDocument();
                    indexDoc.setContent(""); // this is some category document in AREA index; there is no valid content
                    indexDoc.setDocumentKey(theAIndexKeys.getNewDocumentKey());//JP:TODO hier geht auch: "ID" + category.getGuid()
                    indexDoc.setDocumentLanguage(theCatalogLang);
                    indexDoc.setDocumentAction(IndexDocument.DOCUMENT_ACTION_INDEX);

                    Iterator attrIter = category.getAttributes();
                    while(attrIter.hasNext())
                    {
                        // theAttributes of CatalogCategory is "normally" empty
                        // the attributes to be indexed are found in theDetails
                        IAttribute attr = (IAttribute)attrIter.next();
                        String attrName = attr.getName();
                        theStaticLocToLog.warn(ICatalogMessagesKeys.PCAT_WARNING,
                            new Object[]{"unexpected category attribute: " + attrName}, null);
                        //indexDoc.addDocumentAttribute();
                    }
                    StringBuffer aBuffer = null;
                    boolean debug = theStaticLocToLog.isDebugEnabled();
                    if(debug) aBuffer = new StringBuffer("AIndex document");
                    // "special" attributes
                    indexDoc.addDocumentAttribute(
                        ATTR_AREA,
                        areaName, "",
                        DocAttribute.ATTRIBUTE_OPERATOR_EQ,
                        DocAttribute.ATTRIBUTE_TYPE_STRING);
                    if(debug) aBuffer.append(" AREA=" + areaName);
                    indexDoc.addDocumentAttribute(
                        ATTR_AREA_GUID,
                        areaGuid, "",
                        DocAttribute.ATTRIBUTE_OPERATOR_EQ,
                        DocAttribute.ATTRIBUTE_TYPE_STRING);
                    if(debug) aBuffer.append(" AREA_GUID=" + areaGuid);
                    indexDoc.addDocumentAttribute(
                        ATTR_PARENT_AREA,
                        parentName, "",
                        DocAttribute.ATTRIBUTE_OPERATOR_EQ,
                        DocAttribute.ATTRIBUTE_TYPE_STRING);
                    indexDoc.addDocumentAttribute(
                        ATTR_PARENT_AREA_GUID,
                        parentGuid, "",
                        DocAttribute.ATTRIBUTE_OPERATOR_EQ,
                        DocAttribute.ATTRIBUTE_TYPE_STRING);
                    indexDoc.addDocumentAttribute(
                        ATTR_CATALOG,
                        theCatalogName ,"",
                        DocAttribute.ATTRIBUTE_OPERATOR_EQ,
                        DocAttribute.ATTRIBUTE_TYPE_STRING);
                    indexDoc.addDocumentAttribute(
                        ATTR_CATALOG_GUID,
                        theCatalogGuid ,"",
                        DocAttribute.ATTRIBUTE_OPERATOR_EQ,
                        DocAttribute.ATTRIBUTE_TYPE_STRING);
                    indexDoc.addDocumentAttribute(
                        ATTR_DESCRIPTION,
                        category.getDescription() ,"",
                        DocAttribute.ATTRIBUTE_OPERATOR_EQ,
                        DocAttribute.ATTRIBUTE_TYPE_TEXT);  // "DESCRIPTION" is searchable
                    if(debug) aBuffer.append(" DESCRIPTION=" + category.getDescription());

                    Iterator detailIter = category.getDetails();
                    while(detailIter.hasNext())
                    {
                        IDetail detail = (IDetail)detailIter.next();
                        String detailName  = detail.getName();
                        String[] detailValues = detail.getAllAsString();
                        if (theStaticLocToLog.isDebugEnabled() && detailValues.length > 1) {
                        	theStaticLocToLog.debug("Multi-value area attribute " + detailName +
								" found for category " + areaName + ", length: " + detailValues.length);
                        }
                        String attributeType = DocAttribute.ATTRIBUTE_TYPE_STRING;
                        //handle multi-value attributes
                        for (int i = 0; i < detailValues.length; i++) {
                        	if(detailName.equalsIgnoreCase(ATTR_TEXT_0001))
                        	{
                         	   attributeType = DocAttribute.ATTRIBUTE_TYPE_TEXT; // "TEXT_0001" is searchable
                       		}
                        	indexDoc.addDocumentAttribute(
                            	detailName,
                            	detailValues[i],
                            	"",
                            	DocAttribute.ATTRIBUTE_OPERATOR_EQ,
                            	attributeType);
                        	if(debug) aBuffer.append("\ndetail#" + detailName + ":" + detailValues[i]);
                        }	
                    }

                    if(debug) theStaticLocToLog.debug(aBuffer.toString());
                    theAIndexManager.addDocument(indexDoc);
                    // theAIndexManager.index();    <<== should be packaged somehow!!!
                    if(++countAIndexDocs >= 1000)
                    {
                        callIndexer(theAIndexManager, theAIndexID, countAIndexDocs);
                        countAIndexDocs = 0;
                        theAIndexManager = getNewIndexManager(theAIndexID);
                    }
                    // theAIndexManager.optimize(); <<== leave this to the ultimate optimize in fini()

                    // now index all the product items of that category...
                    Iterator itemIter = category.getItems();
                    while(itemIter.hasNext())
                    {
                        indexItem((IItem)itemIter.next(), areaName, areaGuid);
                    }

                    // and now all the subcategories...
                    Iterator cateIter = category.getChildCategories();
                    while(cateIter.hasNext())
                    {
                        buildCategoryRec((ICategory)cateIter.next(),
                            category.getName(),
                            category.getGuid());
                    }
                } catch (TrexException te)
                {
                    throw new TrexCatalogException(te.getMessage(),te);
                }
            }// buildCategoryRec

            private void indexItem(IItem item, String areaName, String areaGuid)
                throws TrexException, CatalogException
            {
                IndexDocument indexDoc = new IndexDocument();
                indexDoc.setContent(""); // this is some product document in PRODUCT index; there is no valid content
                indexDoc.setDocumentKey(thePIndexKeys.getNewDocumentKey());
                indexDoc.setDocumentLanguage(theCatalogLang);
                indexDoc.setDocumentAction(IndexDocument.DOCUMENT_ACTION_INDEX);

                Iterator detailIter = item.getDetails();
                while(detailIter.hasNext())
                {
                    // theDetails of CatalogItem is "normally" empty
                    // the attributes to be indexed are found in theAttributes
                    IDetail detail = (IDetail)detailIter.next();
                    String detailName  = detail.getName();
                    theStaticLocToLog.warn(ICatalogMessagesKeys.PCAT_WARNING,
                        new Object[]{"unexpected product detail: " + detailName}, null);
                    //indexDoc.addDocumentAttribute();
                }

                StringBuffer aBuffer = null;
                boolean debug = theStaticLocToLog.isDebugEnabled();
                if(debug) aBuffer = new StringBuffer("PIndex document");
                // "special" attributes
                indexDoc.addDocumentAttribute(
                    ATTR_AREA,
                    areaName, "",
                    DocAttribute.ATTRIBUTE_OPERATOR_EQ,
                    DocAttribute.ATTRIBUTE_TYPE_STRING);
                if(debug) aBuffer.append(" AREA=" + areaName);
                indexDoc.addDocumentAttribute(
                    ATTR_AREA_GUID,
                    areaGuid, "",
                    DocAttribute.ATTRIBUTE_OPERATOR_EQ,
                    DocAttribute.ATTRIBUTE_TYPE_STRING);
                if(debug) aBuffer.append(" AREA_GUID=" + areaGuid);
                indexDoc.addDocumentAttribute(
                    ATTR_CATALOG,
                    theCatalogName ,"",
                    DocAttribute.ATTRIBUTE_OPERATOR_EQ,
                    DocAttribute.ATTRIBUTE_TYPE_STRING);
                indexDoc.addDocumentAttribute(
                    ATTR_CATALOG_GUID,
                    theCatalogGuid ,"",
                    DocAttribute.ATTRIBUTE_OPERATOR_EQ,
                    DocAttribute.ATTRIBUTE_TYPE_STRING);
                indexDoc.addDocumentAttribute(
                    ATTR_ITEM_GUID,
                    item.getGuid() ,"",
                    DocAttribute.ATTRIBUTE_OPERATOR_EQ,
                    DocAttribute.ATTRIBUTE_TYPE_STRING);
                if(debug) aBuffer.append(" ITEM_GUID=" + item.getGuid());
                indexDoc.addDocumentAttribute(
                    ATTR_OBJECT_GUID,
                    item.getProductGuid() ,"",
                    DocAttribute.ATTRIBUTE_OPERATOR_EQ,
                    DocAttribute.ATTRIBUTE_TYPE_STRING);
                if(debug) aBuffer.append(" OBJECT_GUID=" + item.getProductGuid());

                Iterator attrValIter = item.getAttributeValues();
                while(attrValIter.hasNext())
                {
                    IAttributeValue attrVal = (IAttributeValue)attrValIter.next();
                    IAttribute attr = attrVal.getAttribute();
					String attrName = attr.getName();
                    //get an iterator for all the (multi) values...
                    Iterator theAttributeValuesAsStringIter = attrVal.getAllAsString();
                    //for debugging purposes only
                    int multiValCounter = 0;
                    while (theAttributeValuesAsStringIter.hasNext()) {
						String attrValue = (String) theAttributeValuesAsStringIter.next();
                    
                    	if(attrValue == null)
                    	{
                    	    return;
                    	}
						
						multiValCounter++;
						
                  	  	/*String attributeType = DocAttribute.ATTRIBUTE_TYPE_STRING;
                	  	  if(attrName.equalsIgnoreCase("OBJECT_DESCRIPTION")
                	  	  || attrName.equalsIgnoreCase("OBJECT_ID")
               	     	  || attrName.equalsIgnoreCase("R3_MATNR")
                    	  || attrName.equalsIgnoreCase("TEXT_0001"))
                    	  {
                     	    attributeType = DocAttribute.ATTRIBUTE_TYPE_TEXT;
                    	  }
                   	  	 */
                   		  //CHHI 2003/10/17 copy this to a protected method
                   		  //to let customers extend
                    	String attributeType = getAttributeType(attrName);
                    
                    	indexDoc.addDocumentAttribute(
                        	attrName,
                        	attrValue,
                        	"",
                        	DocAttribute.ATTRIBUTE_OPERATOR_EQ,
                        	attributeType);
                    	if(debug) aBuffer.append("\nattribute#" + attrName + ":" + attrValue);
                    }
                    if (theStaticLocToLog.isDebugEnabled() && multiValCounter > 1) {
                    	theStaticLocToLog.debug("Multi-value item attribute " + attrName +
							" found with " + multiValCounter + " values to item "
							+ item.getName());
                    }
                }

                if(debug) theStaticLocToLog.debug(aBuffer.toString());
                thePIndexManager.addDocument(indexDoc);
                // thePIndexManager.index();    <<== should be packaged somehow!!!
                if(++countPIndexDocs >= 1000)
                {
                    callIndexer(thePIndexManager, thePIndexID, countPIndexDocs);
                    countPIndexDocs = 0;
                    thePIndexManager = getNewIndexManager(thePIndexID);
                }
                // thePIndexManager.optimize(); <<== leave this to the ultimate optimize in fini()
            }

            void fini()
                throws CatalogException
            {
                try {
                    if(countPIndexDocs > 0)
                    {
                        callIndexer(thePIndexManager, "PIndex", countPIndexDocs);
                        countPIndexDocs = 0;
                        // thePIndexManager = getNewIndexManager(thePIndexID);
                    }
                    {
                        callOptimizer(thePIndexManager, "PIndex");
                    }
                    if(countAIndexDocs > 0)
                    {
                        callIndexer(theAIndexManager, "AIndex", countAIndexDocs);
                        countAIndexDocs = 0;
                        // theAIndexManager = getNewIndexManager(theAIndexID);
                    }
                    {
                        callOptimizer(theAIndexManager, "AIndex");
                    }
                } catch (TrexException te)
                {
                    throw new TrexCatalogException(te.getMessage(),te);
                }
                storeIndexStatus(theAIndexID, "Valid");   // remapps the "CreatePending" status, which was set by init()
            }

            private void callIndexer(IndexManager idxMgr, String name, int count)
                throws TrexException
            {
                long startTime = 0;
                if(theStaticLocToLog.isInfoEnabled())
                {
                    startTime = (new Date()).getTime();
                    theStaticLocToLog.info(ICatalogMessagesKeys.PCAT_INFO,
                        new Object[]{"addCatalogRemote() indexing " + name + " with " + count + " documents..."}, null);
                }
                idxMgr.index();
                if(theStaticLocToLog.isInfoEnabled())
                {
                    long endTime = (new Date()).getTime();
                    theStaticLocToLog.info(ICatalogMessagesKeys.PCAT_INFO,
                        new Object[]{"addCatalogRemote() indexing " + name + " done using " + millis2string(endTime-startTime)}, null);
                }
            }

            private void callOptimizer(IndexManager idxMgr, String name)
                throws TrexException
            {
                long startTime = 0;
                if(theStaticLocToLog.isInfoEnabled())
                {
                    startTime = (new Date()).getTime();
                    theStaticLocToLog.info(ICatalogMessagesKeys.PCAT_INFO,
                        new Object[]{"addCatalogRemote() optimizing " + name + "..."}, null);
                }
                idxMgr.optimize();
                if(theStaticLocToLog.isInfoEnabled())
                {
                    long endTime = (new Date()).getTime();
                    theStaticLocToLog.info(ICatalogMessagesKeys.PCAT_INFO,
                        new Object[]{"addCatalogRemote() optimizing " + name + " done using " + millis2string(endTime-startTime)}, null);
                }
            }

        } // IndexBuilderLocal

        // begin of addCatalogRemote(Catalog)
        long startTime = 0;
        if(theStaticLocToLog.isInfoEnabled())
        {
            startTime = (new Date()).getTime();
            theStaticLocToLog.info(ICatalogMessagesKeys.PCAT_INFO,
                new Object[]{"addCatalogRemote() starting"}, null);
        }

        IndexBuilderLocal builder = new IndexBuilderLocal(catalog);
        builder.init();
        Iterator categoriesIter = catalog.getChildCategories();
        while(categoriesIter.hasNext())
        {
            builder.buildCategoryRec((ICategory)categoriesIter.next(),
                "$ROOT", VALUE_ROOT);
        }
        builder.fini();

        rebuildManagedCatalogInfos();
        if(theStaticLocToLog.isInfoEnabled())
        {
            long endTime = (new Date()).getTime();
            theStaticLocToLog.info(ICatalogMessagesKeys.PCAT_INFO,
                new Object[]{"addCatalogRemote() finished using " + millis2string(endTime-startTime)}, null);
        }
    }

    private class MappingURIResolver implements URIResolver
    {
        private Source theSource;

        MappingURIResolver(Source aSource)
        {
            this.theSource = aSource;
        }
        public Source resolve(String aHref, String aBaseRef)
        {
            return theSource;
        }
    };

    protected Document getAttributeMapping(ServerType theFromServertype)
    {
        Document theResult = null;
        if ( theFromServertype == IServerEngine.ServerType.R3)
        {
            StringReader aStringReader =
                new StringReader("<Maps></Maps>");
            try
            {
                DocumentBuilder theDOMBuilder =
                    DocumentBuilderFactory.newInstance().newDocumentBuilder();

                theResult  = theDOMBuilder.parse(new InputSource(aStringReader));
                Element theMapsElement = theResult.getDocumentElement();

                NodeList theMaps = theConfigDocument.getElementsByTagName("Map");
                int size = theMaps.getLength();
                for (int i = 0; i < size; i++)
                {
                    Node aMap = (Node) theMaps.item(i);
                    Node aMapClone = aMap.cloneNode(true);
                    Node theImportedMap =
                        theResult.importNode(aMapClone,true);
                    theMapsElement.appendChild(theImportedMap);
                } // end of for ()
            } catch (ParserConfigurationException pce)
            {
                theStaticLocToLog.error(
                    ICatalogMessagesKeys.PCAT_EXCEPTION,
                    new Object[] {pce.getMessage()},
                    pce);
            } // end of try-catch
            catch ( SAXException se)
            {
                theStaticLocToLog.error(
                    ICatalogMessagesKeys.PCAT_EXCEPTION,
                    new Object[] {se.getMessage()},
                    se);
            } // end of catch
            catch ( IOException ioe)
            {
                theStaticLocToLog.error(
                    ICatalogMessagesKeys.PCAT_EXCEPTION,
                    new Object[] {ioe.getMessage()},
                    ioe);
            } // end of catch
        } // end of if ()
        return theResult;
    }

        /**
         * Delete an index on the server. The index is identified by its index ID.
         *
         * @param anIndexId <code>String</code> identifying the index on the server
         */
    private void deleteIndexRemote(String anIndexId)
        throws CatalogException
    {
        AdminManager theManager = TrexFactory.getAdminManager();
        try
        {
            theManager.setIndexId(anIndexId);
            theManager.deleteIndex();
        }
        catch (TrexException te)
        {
            theStaticLocToLog.error(
                ICatalogMessagesKeys.PCAT_ERR_TREX_INDEX_DESTRUCTION,
                new Object[] {anIndexId},
                te);
            String theMessage =
                theMessageResources.getMessage(
                    ICatalogMessagesKeys.PCAT_ERR_TREX_INDEX_DESTRUCTION,
                    anIndexId);
            throw new CatalogException(theMessage,te);
        } // end of try-catch
    }

        /**
         * Delete a catalog on the server. The catalog is identified by its catalog info.
         *
         * @param aCatalogGuid <code>String</code> value
         */
    protected void deleteCatalogRemote(CatalogServerEngine.CatalogInfo aCatalogInfo)
        throws CatalogException
    {
            //find the index id s belonging to the catalog
            //first catalogs managed by myself
        if (aCatalogInfo instanceof TrexCatalogServerEngine.TrexManagedCatalogInfo)
        {
                //delete the indices
            TrexManagedCatalogInfo aManagedCatalogInfo =
                (TrexManagedCatalogInfo)aCatalogInfo;
            String theAIndexId = aManagedCatalogInfo.getAIndexId();
            String thePIndexId = aManagedCatalogInfo.getPIndexId();
            Iterator anSindexIdIter = aManagedCatalogInfo.getSIndexCategoryIds();
                //delete the AIndex
            deleteIndexRemote(theAIndexId);
                //delete the Pindex
            deleteIndexRemote(thePIndexId);
                //delete the SIndices
            while ( anSindexIdIter.hasNext())
            {
                String anSindexId = (String) anSindexIdIter.next();
                deleteIndexRemote(anSindexId);
            } // end of while ()
        } // end of if ()
        else
        {
//            throw new CatalogException("Destruction of catalogs not managed by this are not supported!");
            // the following properties were set in buildCatalogInfosRemote()
            String theAIndexId = aCatalogInfo.getProperty("AIndexId");
            deleteIndexRemote(theAIndexId);
            String thePIndexId = aCatalogInfo.getProperty("PIndexId");
            deleteIndexRemote(thePIndexId);
            //TODO: and now the S-indexes;
        } // end of else
        return;
    }

        /**
         * Build the list of catalog infos known to that server.
         * The nodelist of <code>Catalog</code> elements is used as a filter.
         * If the node list is empty or null no catalog info elements will be build.
         *
         * @param theCatalogElements a <code>NodeList</code> value
         */
    synchronized protected void buildCatalogInfosRemote(NodeList theCatalogElements)
    {
     
    }

        /**
         * Build the list of catalog infos (from the crm system) known to that server.
         * Use the supplied string as catalog name pattern.
         *
         * @param aCatalogNamePattern a <code>String</code> value
         */
    synchronized protected void buildCatalogInfosRemote(String aCatalogNamePattern)
    {

    }

    private class TRexIndexInvalidException extends TrexCatalogException {
        TRexIndexInvalidException(String msg) {
            super(msg);
        }
    }

	/**
	 * Build the catalog infos for those catalogs on this server that were
	 * replicated using via RFC from R/3(!)
	 *
	 */
	protected void buildManagedCatalogInfos()
	{
		//get the infos about the indices known on the server
		AdminManager theManager = TrexFactory.getAdminManager();
		ServerIndexList theListOfIndices=null;
		try
		{
			theManager.setIndexId("aDummy");
			theListOfIndices =  theManager.getAllIndices();
		}
		catch (TrexException te)
		{
			theStaticLocToLog.error(LogUtil.APPS_COMMON_INFRASTRUCTURE, ICatalogMessagesKeys.PCAT_ERR_INIT_INSTANCE,
							new Object[]{this.getClass().getName(),te.getLocalizedMessage()},
							te);
			return;
		} // end of try-catch

		IndexShortDescription anIndexShortDescription = theListOfIndices.getFirstIndexShortDescription();
		HashMap theBuildCatInfos = new HashMap();
		while(anIndexShortDescription != null) {
			String theIndexID = anIndexShortDescription.getIndexId();
			if (theIndexID.length() != 38) { //it's not a GUID
				//look at the next index
				anIndexShortDescription = theListOfIndices.getNextIndexShortDescription();
				continue;
			}
			TrexManagedCatalogInfo theCatalogInfo =null;
			String theCatalogKey = null;
		
			SearchResult theSearchResult = null;
				
			try {
				SearchManager theSearchManager = TrexFactory.getSearchManager();
				QueryEntry aQueryEntry = new QueryEntry();
							
				aQueryEntry.setValue("$ROOT_ISA_ERP", "", QueryEntry.ATTRIBUTE_OPERATOR_EQ);
				aQueryEntry.setRowType(QueryEntry.ROW_TYPE_ATTRIBUTE);
				aQueryEntry.setLocation("AREA");
				aQueryEntry.setTermAction(QueryEntry.TERM_ACTION_EXACT);
				//TODO: determine language
				theSearchManager.addIndexId(theIndexID,
											"DRFUZZY");
				theSearchManager.setResultFromTo(1, CatalogParameters.trexUpperLimit);
				theSearchManager.addRequestedAttribute("CATALOG_GUID");
				theSearchManager.addRequestedAttribute("P_INDEX_ID");
				theSearchManager.addRequestedAttribute("INDEX_STATUS");
				theSearchManager.addRequestedAttribute("AREA");
				theSearchManager.addQueryEntry(aQueryEntry);
				theSearchResult = theSearchManager.search();
			}
			catch (TrexException ex) {
				theSearchResult = new SearchResult();
				theStaticLocToLog.error(LogUtil.APPS_COMMON_INFRASTRUCTURE,
					ICatalogMessagesKeys.PCAT_ERR_INIT_NO_ERP_ROOT, new Object[] {theIndexID, ex.getLocalizedMessage()},
					ex );
				anIndexShortDescription = theListOfIndices.getNextIndexShortDescription();
				continue;
			}
		
			if (theSearchResult.getNoOfAllHitsInIndex() == 0) {//for some reason getNoOfHits() doesn't work...
				//Index does not contain a $ROOT_ISA_ERP document, hence it's either not an A-Index
				//or not coming from R/3. Maybe add a better check here later.
				anIndexShortDescription = theListOfIndices.getNextIndexShortDescription();
				continue;
			}
			theStaticLocToLog.debug("Building managed catalog info for index id:" + theIndexID);
			ResultDocument aResultDocument = theSearchResult.getFirstResultDocument();
			boolean catalogGuidFound = false;
		
			while (aResultDocument != null && !catalogGuidFound) {
				DocAttribute aDocAttribute = aResultDocument.getFirstDocAttribute();
				//we need a separate loop for the CATALOG_GUID attr to build the catalog info
				while (aDocAttribute != null) {
					if (aDocAttribute.getAttributeName().equalsIgnoreCase("CATALOG_GUID")) {
						theCatalogKey = aDocAttribute.getValue1();
						catalogGuidFound = true;
						if( theStaticLocToLog.isDebugEnabled())
							theStaticLocToLog.debug("Catalog key is:" + theCatalogKey);
						// do we have it already
						if (theBuildCatInfos.containsKey(theIndexID))
						{
						  theCatalogInfo = (TrexManagedCatalogInfo)theBuildCatInfos.get(theIndexID);
						} // end of if ()
						else //then build a new one
						{
						  theCatalogInfo =
							  new TrexManagedCatalogInfo(theCatalogKey, theIndexID);
							  theCatalogInfos.add(theCatalogInfo);
							  theBuildCatInfos.put(theCatalogKey, theCatalogInfo);
						} // end of else
						//set the A-Index
						theCatalogInfo.setAIndexId(theIndexID);
						try
						{
							searchManagedCatalogInfo(theCatalogInfo,theIndexID);
						}
						catch (TrexCatalogException te)
						{
							theCatalogInfos.remove(theCatalogInfo);
							if(te instanceof TRexIndexInvalidException) {
								theStaticLocToLog.warn(
									LogUtil.APPS_COMMON_INFRASTRUCTURE,
									ICatalogMessagesKeys.PCAT_WARNING,
									new Object[]{ "Catalogkey: " + theCatalogKey + ": " + te.getMessage() },
									null);
							} else {
								theStaticLocToLog.error(
									LogUtil.APPS_COMMON_INFRASTRUCTURE,
									ICatalogMessagesKeys.PCAT_ERR_CREATION_CATINFO,
									new Object[] {theCatalogKey, te.getMessage()},
									te);
							}
						} // end of try-catch
					}
					//look at the next attribute of the current result document
					aDocAttribute = aResultDocument.getNextDocAttribute();
				}
				
				aDocAttribute = aResultDocument.getFirstDocAttribute(); //now the remaining attributes
				while (aDocAttribute != null) {
					//set the P-Index
					if (aDocAttribute.getAttributeName().equalsIgnoreCase("P_INDEX_ID")) {
						theCatalogInfo.setPIndexId(aDocAttribute.getValue1().toLowerCase());
					}
					//set the status
					if (aDocAttribute.getAttributeName().equalsIgnoreCase("INDEX_STATUS")) {
						theCatalogInfo.setStatus(aDocAttribute.getValue1());
					}
					//look at the next attribute of the current result document
					aDocAttribute = aResultDocument.getNextDocAttribute();
				}
				//look at the next result document. Of course, there should be only one...
				aResultDocument = theSearchResult.getNextResultDocument();
			}
		
			//look at the next index
			anIndexShortDescription = theListOfIndices.getNextIndexShortDescription();

		}  // end of while ()
		return;
	}

    public void rebuildManagedCatalogInfos()
    {
        Iterator anIter = this.theCatalogInfos.iterator();
        while (anIter.hasNext() )
        {
            CatalogInfo aCatalogInfo = (CatalogInfo)anIter.next();
            if ( aCatalogInfo instanceof TrexManagedCatalogInfo)
            {
                anIter.remove();
            } // end of if ()
        } // end of while ()
        
		buildManagedCatalogInfos();

		return;
    }

        /**
         * Delete all catalogs on the index server.
         *
         */
    protected void deleteCatalogsRemote()
        throws CatalogException
    {
        AdminManager theManager = TrexFactory.getAdminManager();
        try
        {
            theManager.setIndexId("aDummy");
            ServerIndexList theListOfIndices =  theManager.getAllIndices();
            ArrayList theShortDescriptions = theListOfIndices.getIndexShortDescriptionList();
            Iterator anIter = theShortDescriptions.iterator();
            while (anIter.hasNext())
            {
                IndexShortDescription aShortDescription = (IndexShortDescription)anIter.next();
                String aCatalogGuid = aShortDescription.getIndexId();
                AdminManager aManagerForADescrution = TrexFactory.getAdminManager();
                aManagerForADescrution.setIndexId(aCatalogGuid);
                aManagerForADescrution.deleteIndex();
            } // end of while ()
        } catch (TrexException te)
        {
            throw new CatalogException("The destruction of the catalogs failed! Cause: " +
                                       te.getMessage());
        } // end of try-catch
        return;
    }

        /**
         * Delete all indexes on the index server, which have the INDEX_STATUS
         * of "DeletePending"
         *
         */
    public /*protected*/ void deleteManagedIndexesWithDeletePending()
        throws CatalogException
    {
        AdminManager theAdminManager = TrexFactory.getAdminManager();
        try
        {
            theAdminManager.setIndexId("aDummy");
            ServerIndexList theListOfIndices =  theAdminManager.getAllIndices();
            ArrayList theShortDescriptions = theListOfIndices.getIndexShortDescriptionList();
            Iterator anIter = theShortDescriptions.iterator();
            //JP: evtl. kann man in allen Indizes gleichzeitig suchen !?!?
            while (anIter.hasNext())
            {
                IndexShortDescription aShortDescription = (IndexShortDescription)anIter.next();
                String aIndexId = aShortDescription.getIndexId();
                if( ! aIndexId.toUpperCase().startsWith("INDEX_ID") ||
                    ! aIndexId.toUpperCase().endsWith("AINDEX") )
                    continue;
                SearchManager searchManager = TrexFactory.getSearchManager();
                searchManager.addIndexId(
                    aIndexId,
                    TrexConst.SEARCH_ENGINE_DRFUZZY);

                    // set interval
                searchManager.setResultFromTo(INTERVAL_BEGIN, CatalogParameters.trexUpperLimit);
                    // add return attributes
                searchManager.addRequestedAttribute("INDEX_STATUS");               //JP: see catalogToTrex.xsl

                    // set query condition
                QueryEntry entry = new QueryEntry();
                entry.setRowType(QueryEntry.ROW_TYPE_ATTRIBUTE);
                entry.setLocation("INDEX_STATUS");
                entry.setContentType(QueryEntry.CONTENT_TYPE_STRING);
                entry.setValue("DeletePending", "", QueryEntry.ATTRIBUTE_OPERATOR_EQ);
                entry.setTermWeight(10000);
                searchManager.addQueryEntry(entry);
                SearchResult results = searchManager.search();
                if (results.getLastError().getCode()!=TrexReturn.NO_ERROR)
                {
                    throw new TrexCatalogException(results.getLastError().getMsg());
                }

                ResultDocument firstDocument = results.getFirstResultDocument();
                if (firstDocument!=null)
                {
                    // here we found a catalog which is delete pending...
                    theStaticLocToLog.info(ICatalogMessagesKeys.PCAT_INFO, new Object[]{"found index with DeletePending status: " + aIndexId}, null);
                    // delete ALL the indexes in our list which conform to the indexID
                    String indexTime = aIndexId.substring(8, aIndexId.length()-6);
                    theStaticLocToLog.info(ICatalogMessagesKeys.PCAT_INFO, new Object[]{"deleting indexes which contain " + indexTime}, null);
                    for(int i = 0; i < theShortDescriptions.size(); i++)
                    {
                        IndexShortDescription shortDescription = (IndexShortDescription)theShortDescriptions.get(i);
                        String indexName = shortDescription.getIndexId();
                        if( ! indexName.toUpperCase().startsWith("INDEX_ID" + indexTime))
                            continue;
                        theStaticLocToLog.info(ICatalogMessagesKeys.PCAT_INFO, new Object[]{"deleting index " + indexName}, null);
                        AdminManager aManagerForADescrution = TrexFactory.getAdminManager();
                        theAdminManager.setIndexId(indexName);
                        theAdminManager.deleteIndex();
                    }
                }
            } // end of while ()
        } catch (TrexException te)
        {
            throw new CatalogException("The destruction of the remote indexes failed! Cause: " +
                                       te.getMessage(), te);
        }
        return;
    }

        /**
         * Load an xslt filter. Loads the catalogToTrex xlst filter to transform input
         * to the trex specific data format..
         *
         * @param theFilterName a <code>String</code> value
         */
    private Templates loadFilter(String theFilterName)
    {
        Templates theResult = null;
        try
        {
                // Instantiate a TransformerFactory.
            TransformerFactory tFactory = TransformerFactory.newInstance();

                // Create an XML Filter for each style sheet.
            InputStream templateAsStream =
				this.getClass().getResourceAsStream(theFilterName);

            theResult =
				tFactory.newTemplates(new StreamSource(templateAsStream));

        }
        catch(TransformerConfigurationException tce)
        {
            theStaticLocToLog.error(ICatalogMessagesKeys.PCAT_ERR_FILE_NOT_FOUND,
                                new Object[]{theFilterName},
                                tce);
        }
        catch(Throwable t)
        {
            theStaticLocToLog.error(ICatalogMessagesKeys.PCAT_ERR_FILE_NOT_FOUND,
                                new Object[]{theFilterName},
                                t);
        }
        return theResult;
    }

    public class TrexManagedCatalogInfo
        extends CatalogServerEngine.CatalogInfo
    {
        // gd -- added status
        public static final String STATUS_VALID = "Valid";
        public static final String STATUS_CREATEPENDING = "CreatePending";
        public static final String STATUS_DELETEPENDING = "DeletePending";

        public static final String PROPERTY_INDEX_GUID ="IndexGuid";
        public static final String PROPERTY_LANGUAGE ="Langauge";
        public static final String PROPERTY_A_INDEX_ID ="AIndexID";
        public static final String PROPERTY_P_INDEX_ID ="PIndexID";
        public static final String PROPERTY_S_INDEX_IDS = "SIndexIDs";
        // gd -- added status
        public static final String PROPERTY_STATUS = "Status";

        private Hashtable theSIndices;

        public TrexManagedCatalogInfo(String aIndexGuid)
        {
            super(aIndexGuid,TrexManagedCatalog.class.getName());
            theSIndices = new Properties();
            theProperties.put(PROPERTY_S_INDEX_IDS,theSIndices);
            setProperty(PROPERTY_INDEX_GUID,null,aIndexGuid);
        }

        public TrexManagedCatalogInfo(String aCatalogGuid, String aIndexGuid)
        {
            super(aCatalogGuid,TrexManagedCatalog.class.getName());
            theSIndices = new Properties();
            theProperties.put(PROPERTY_S_INDEX_IDS,theSIndices);
            setProperty(PROPERTY_INDEX_GUID,null,aIndexGuid);
        }

        synchronized void setCatalogGuid(String aCatalogGuid)
        {
            this.theCatalogGuid=aCatalogGuid;
        }

        synchronized void setLanguage(String aLanguage)
        {
            this.setProperty(PROPERTY_LANGUAGE,null,aLanguage);
            return;
        }

        // gd -- added status
        synchronized void setStatus(String newVal)
        {
            this.setProperty(PROPERTY_STATUS,null,newVal);
            return;
        }

        public String getStatus()
        {
            return getProperty(PROPERTY_STATUS);
        }

        synchronized String getLanguage()
        {
            return getProperty(PROPERTY_LANGUAGE);
        }

        public String getIndexId()
        {
            return getProperty(PROPERTY_INDEX_GUID);
        }

        public String getAIndexId()
        {
            return getProperty(PROPERTY_A_INDEX_ID);
        }

        public void  setAIndexId(String aAIndexId)
        {
            setProperty(PROPERTY_A_INDEX_ID,null,aAIndexId);
            return;
        }

        public String getPIndexId()
        {
            return getProperty(PROPERTY_P_INDEX_ID);
        }

        public void  setPIndexId(String aPIndexId)
        {
            setProperty(PROPERTY_P_INDEX_ID,null,aPIndexId);
            return;
        }

        public void setSindexId(String aCategoryId, String aSIndexId)
        {
            theSIndices.put(aCategoryId,aSIndexId);
            return;
        }

        public Iterator getSIndexCategoryIds()
        {
            ArrayList theCategoryIndexIds = new ArrayList();
            Iterator theKeys = theSIndices.keySet().iterator();
            while ( theKeys.hasNext())
            {
                String aKey = (String)theKeys.next();
                theCategoryIndexIds.add((String)theSIndices.get(aKey));
            } // end of while ()
            return theCategoryIndexIds.iterator();
        }

        public String getSIndexId(String aCategoryId)
        {
            return (String)theSIndices.get(aCategoryId);
        }

        public IndexBuilder createIndexBuilder(String aId, String aType, String aCategoryId)
            throws TrexException
        {
            if (aType.intern() =="AIndex".intern())
            {
                this.setAIndexId(aId);
            } else if (aType.intern() =="PIndex".intern())
            {
                this.setPIndexId(aId);
            } // end of if ()
            else
            {
                this.setSindexId(aCategoryId,aId);
            } // end of else

            return TrexCatalogServerEngine.this.createIndexBuilder(aId);
        }

        public void addCatalogInfo()
        {
            TrexCatalogServerEngine.this.theCatalogInfos.add(this);
        }
    }

    public TrexManagedCatalogInfo createCatalogInfo(String aCatalogID,String aIndexID)
        throws TrexCatalogException
    {
            //take care that a catalog is added only once
        Iterator anIter = theCatalogInfos.iterator();
        while ( anIter.hasNext())
        {
            ICatalogInfo anInfo = (ICatalogInfo)anIter.next();
            if (anInfo.getGuid().intern()==aCatalogID.intern() )
            {
                String msg =
                    theMessageResources.
                    getMessage(ICatalogMessagesKeys.PCAT_WARN_TRIED_ADD_EXISTING_CAT);
                throw new TrexCatalogException(msg);
            } // end of if ()
        } // end of while ()anIter

        TrexManagedCatalogInfo aCatalogInfo = new TrexManagedCatalogInfo(aCatalogID,aIndexID);
        return aCatalogInfo;
    }

        /**
         * Initialize a digester (ContentHandler). It transforms sax events
         * (trex index xml trex.dtd) into trex api calls.
         */
    private void initDigester()
    {
            //The digester for the index
        theIndexDigester = new Digester();
        theIndexDigester.setValidating(false);
        theIndexDigester.push(this);

            //At the beginning place an IndexBuilder for this index on the stack
        CallFactoryMethodRule theRuleToCreateTheIndex =
            new CallFactoryMethodRule( theIndexDigester,
                                       "createIndexBuilder",
                                       new String[]{"ID"});
        theIndexDigester.addRule("*/Index",
                                 theRuleToCreateTheIndex);

            //rules to create the attributes
        String[] theArgumentTypes = new String[4];
        theArgumentTypes[0]="java.lang.String";
        theArgumentTypes[1]="java.lang.String";
        theArgumentTypes[2]="java.lang.Boolean";
        theArgumentTypes[3]="java.lang.Boolean";

        CallMethodRule theAttributeCreateRule =
            new CallMethodRule(theIndexDigester,
                               "addDocumentAttribute",
                               4,
                               theArgumentTypes);
        theIndexDigester.addRule("*/Attributes/Attribute",
                                 theAttributeCreateRule);
        CallParamRule theAttributeIDValueRule =
            new CallParamRule(theIndexDigester,0,"ID");
        theIndexDigester.addRule("*/Attributes/Attribute",
                                 theAttributeIDValueRule);
        CallParamRule theAttributeTypeValueRule =
            new CallParamRule(theIndexDigester,1,"type");
        theIndexDigester.addRule("*/Attributes/Attribute",
                                 theAttributeTypeValueRule);
        CallParamRule theAttributeMiningValueRule =
            new CallParamRule(theIndexDigester,2,"mining");
        theIndexDigester.addRule("*/Attributes/Attribute",
                                 theAttributeMiningValueRule);
        CallParamRule theAttributeClassificationValueRule =
            new CallParamRule(theIndexDigester,3,"classification");
        theIndexDigester.addRule("*/Attributes/Attribute",
                                 theAttributeClassificationValueRule);

            //execute the createIndex call
        CallMethodRule theIndexCreateRule =
            new CallMethodRule(theIndexDigester,
                               "createIndex",-1);
        theIndexDigester.addRule("*/Attributes",
                                 theIndexCreateRule);

            //push theIndexManager onto the stack
        CallFactoryMethodRule theRuleToPushTheIndexManager =
            new CallFactoryMethodRule( theIndexDigester,
                                       "createIndexManager",
                                       new String[]{});
        theIndexDigester.addRule("*/Index/Documents",
                                 theRuleToPushTheIndexManager);

            //index and optimize the created documents

        CallMethodRule theRuleToOptimize =
            new CallMethodRule( theIndexDigester,
                                "optimize",-1);
        theIndexDigester.addRule("*/Index/Documents",
                                 theRuleToOptimize);

        CallMethodRule theRuleToIndex =
            new CallMethodRule( theIndexDigester,
                                "index",-1);
        theIndexDigester.addRule("*/Index/Documents",
                                 theRuleToIndex);

            //create the documents
        ObjectCreateRule theDocumentCreateRule =
            new ObjectCreateRule(theIndexDigester,
                                 "com.sap.isa.catalog.trex.TrexIndexDocumentWrapper");
        theIndexDigester.addRule("*/Document",
                                 theDocumentCreateRule);

            //set the properties of the documents
        SetPropertiesRule theDocumentSetPropertiesRule =
            new SetPropertiesRule(theIndexDigester);
        theIndexDigester.addRule("*/Document", theDocumentSetPropertiesRule);

            //set the content of the document
        CallMethodRule theDocumentSetContentRule =
            new CallMethodRule(theIndexDigester,"setContent",1);
        theIndexDigester.addRule("*/Document/Content", theDocumentSetContentRule);

        CallParamRule theDocumentSetContentValueRule =
            new CallParamRule(theIndexDigester,0);
        theIndexDigester.addRule("*/Document/Content",
                                 theDocumentSetContentValueRule);

            //set the attributes of the document
        CallMethodRule theDocumentSetAttributesRule =
            new CallMethodRule(theIndexDigester,"addDocumentAttribute",2);
        theIndexDigester.addRule("*/Document/AttributeValue", theDocumentSetAttributesRule);

        CallParamRule theAttributeValueIDValueRule =
            new CallParamRule(theIndexDigester,0,"ID");
        theIndexDigester.addRule("*/Document/AttributeValue",
                                 theAttributeValueIDValueRule);

        CallParamRule theDocumentSetAttributesValueRule =
            new CallParamRule(theIndexDigester,1);
        theIndexDigester.addRule("*/Document/AttributeValue",
                                 theDocumentSetAttributesValueRule);

            //add the document to the index
        SetNextRule theAddDocumentToIndex =
            new SetNextRule(theIndexDigester,"addDocument","com.sapportals.trex.core.IndexDocument");
        theIndexDigester.addRule("*/Document",
                                 theAddDocumentToIndex);
        return;
    }
    
    /**
     * Determines the TREX type of an item attribute. Method is protected since
     * customers could use this method to change the attribute type for new,
     * customer specific attributes.
     * <br>
     * Standard implementation:
     * <br>
     * returns DocAttribute.ATTRIBUTE_TYPE_TEXT for attributes
     * <br> OBJECT_DESCRIPTION,OBJECT_ID,R3_MATNR,TEXT_0001
     * <br> and ATTRIBUTE_TYPE_STRING for any other attribute
     * 
     * @param attrName the attribute id
     * @return the attribute type
     * 
     */
    protected String getAttributeType(String attrName){
    	
        String attributeType;
        
        if(attrName.equalsIgnoreCase("OBJECT_DESCRIPTION")
                || attrName.equalsIgnoreCase("OBJECT_ID")
                || attrName.equalsIgnoreCase("R3_MATNR")
                || attrName.equalsIgnoreCase("TEXT_0001")){
                	
                attributeType = DocAttribute.ATTRIBUTE_TYPE_TEXT;
        }
        else{
	         attributeType = DocAttribute.ATTRIBUTE_TYPE_STRING;
        } 
        
        	
        return attributeType;			
    }

        /**
         * Call back for theIndexDigester. Place an IndexBuilder on the stack
         * to wrap the sick TrexApi conveniently for digesting.
         *
         * @param anIndexGuid a <code>String</code> value with the GUID of the index
         * @return an <code>IndexBuilder</code> to build the index
         * @exception TrexException if an error occurs
         */
    public IndexBuilder createIndexBuilder(String anIndexGuid)
        throws TrexException
    {
        return new IndexBuilder(anIndexGuid);
    }
    
	// Up-port of 4.0 sorting functionality

	/**
	* ************************************************************************************************
	* Please note: This method is only used by TrexManagecCatalogBuilder We need it in a
	* backend object to enable customers to override the methods via modification
	* concept. 
	* Important: Customers have always to override this method to enable the value true if they want to
	* enable product search sort order
	* ************************************************************************************************
	*
	* Returns the boolean value which determines whether product search sort is to be enables or not 
	*
	* @param sortAttribute The attribute which is used to sort the search results
	* @return the boolean value true or false, TRUE = ascending, FALSE = descending
	*/
	public boolean customerExitSearchProductSort(TrexManagedCatalogBuilder trmc)
	{
		//example
		//trmc.sortAttribute = TrexCatalogConstants.ATTR_OBJECT_ID;
		//return TRUE for ASC and FALSE if DESC    
		return false;
	}


    /**
     * A simple internal Wrapper for the sick TrexApi.
     */
    public class IndexBuilder
    {
        private String theIndexGuid;
        private CreateIndexManager theCreateIndexManager;
        private IndexManager theIndexManager;

            /**
             * Creates a new <code>IndexBuilder</code> instance.
             *
             * @param theIndexGuid a <code>String</code> value
             * @exception TrexException if an error occurs
             */
        public IndexBuilder(String theIndexGuid)
            throws TrexException
        {
            this.theIndexGuid=theIndexGuid;
            createCreateIndexManager(theIndexGuid);
        }

            /**
             * Execute the corresponding Trex API calls to init an CreateIndexManager.
             *
             * @param anIndexGuid a <code>String</code> value for the IndexId
             * @exception TrexException if an internal error occurs
             */
        private void createCreateIndexManager(String anIndexGuid)
            throws TrexException
        {
            CreateIndexManager aCreateIndexManager = TrexFactory.getCreateIndexManager();

                //  set the name of the new search index and the access mode
            aCreateIndexManager.setIndexId(anIndexGuid);

                //  the search engine
            aCreateIndexManager.setSearchEngine(TrexConst.SEARCH_ENGINE_DRFUZZY);

            aCreateIndexManager.setMiningFlag(true); // must be set if you want to use classification
//JP: deprecated: aCreateIndexManager.setFilterFlag(true); // use filter software to filter the documents' content (recommended)
            aCreateIndexManager.useQueueServer(useQueueServer);
            this.theCreateIndexManager= aCreateIndexManager;
            return;
        }

            /**
             * Execute corresponding Trex API calls to create an attribute.
             *
             * @param theID a <code>String</code> value
             * @param theType a <code>String</code> value
             * @param isMiningRelavant a <code>boolean</code> value
             * @param isClassificationRelavant a <code>boolean</code> value
             * @exception TrexException if an error occurs
             */
        public void addDocumentAttribute(String theID,
                                         String theType,
                                         Boolean isMiningRelavant,
                                         Boolean isClassificationRelavant)
            throws TrexException
        {
            this.theCreateIndexManager.addDocumentAttribute(theID.toUpperCase(),
                                                            theType,
                                                            isMiningRelavant.booleanValue());
            return;
        }

            /**
             * Create the index an store internal an IndexManager object for further calls.
             *
             * @exception TrexException if an error occurs
             */
        public void createIndex()
            throws TrexException
        {
            if (theIndexManager==null)
            {
                theCreateIndexManager.createIndex();
                theIndexManager = TrexFactory.getIndexManager();

                theIndexManager.setIndexId(theIndexGuid);
                theIndexManager.setSearchEngine(TrexConst.SEARCH_ENGINE_DRFUZZY);
                theIndexManager.useQueueServer(useQueueServer);
            } // end of if ()
            return;
        }

            /**
             * Describe <code>createIndexManager</code> method here.
             *
             * @return an <code>IndexManager</code> value
             */
        public IndexManager createIndexManager()
        {
            return theIndexManager;
        }
    }

    public boolean isCatalogUpToDate(Catalog aCatalog)
    {
      if (aCatalog instanceof TrexManagedCatalog )
        {
			TrexManagedCatalog aTrexCatalog =(TrexManagedCatalog) aCatalog;
			
        	String theIndexGuid = aTrexCatalog.getIndexID();
        	if (!rfcUsedForR3Catalogs) {
        		theIndexGuid = theIndexGuid + "AIndex";
        	}
            boolean result = true;
            /**
             * Get the catalog info that is current. If the catalog was updated,
             * the index is different. If the index is the same, then chances are
             * that the replication was not updated.
             * The question is that we need to rebuild the catalogInfo set.
            rebuildManagedCatalogInfos();
            ICatalogInfo catalogInfo = getCatalogInfo(aCatalog.getGuid());
            if (catalogInfo instanceof TrexManagedCatalogInfo)
            {
              if (!((TrexManagedCatalogInfo)catalogInfo).getIndexId().equalsIgnoreCase(aTrexCatalog.getIndexID()))
              {
                return false;
              }
            }
             */

                // save old time stamp
            String timeStamp = aTrexCatalog.getReplTimeStamp();
            if (timeStamp == null)
              return false;
            theStaticLocToLog.debug("isCatalogUpToDate TRexManagedCatalog(" + theIndexGuid + ") old='" + timeStamp + "'...");
            String theReplTimeStamp ="";
            try
            {
                // get new time stamp
                if (!rfcUsedForR3Catalogs) {
                	theReplTimeStamp
                		= this.getIndexLastReplicationTimestamp(theIndexGuid);
                	if (theReplTimeStamp == null)
                  		theReplTimeStamp = "";
                    // determine result
                	result = theReplTimeStamp.equals(timeStamp);
                }
                else {
                	result = this.isIndexStillValid(theIndexGuid);
                }
            } catch(TrexCatalogException ex) {
                timeStamp = "";
                theReplTimeStamp = "";
                result = false;
            }
            aTrexCatalog.setReplTimeStamp(theReplTimeStamp); 
            /*
            if (! result)
            {
              System.out.println("Rebuilding the Catalog Infos");
              rebuildManagedCatalogInfos();
            }
            */

            return result;
        } else
        {
            return true;
        } // end of else
    }

    static String translateXMLLanguageTag(String theXMLLanguageTag)
    {
        String theISO639_1LanguageCode = theXMLLanguageTag.substring(0,2);
        return theISO639_1LanguageCode;
    }

    TrexManagedCatalogIndexInfo buildIndexInfo(TrexManagedCatalog aCatalog,
                                               String aType,
                                               String aCateId)
        throws TrexCatalogException
    {
		StringBuffer theIndexGUIDBuffer = null;
		if (rfcUsedForR3Catalogs) {
			theIndexGUIDBuffer = new StringBuffer("");
			TrexCatalogServerEngine theServerEngine = (TrexCatalogServerEngine) aCatalog.getServerEngine();
			if (aType.equalsIgnoreCase("A")) {
				theIndexGUIDBuffer.append(theServerEngine.getCatalogInfo(aCatalog.getGuid()).getProperty("AIndexID"));
			}
			else if (aType.equalsIgnoreCase("P")) {
				theIndexGUIDBuffer.append(theServerEngine.getCatalogInfo(aCatalog.getGuid()).getProperty("PIndexID"));
			}
			else {
				//not implemented
				theIndexGUIDBuffer.append(theServerEngine.getCatalogInfo(aCatalog.getGuid()).getProperty("SIndexID"));
			}
		}
		else {
			theIndexGUIDBuffer = new StringBuffer(aCatalog.getIndexID());
			if (aType.equals("A"))
			{
				theIndexGUIDBuffer.append("AIndex");
			} else if (aType.equals("P"))
			{
				theIndexGUIDBuffer.append("PIndex");
			} // end of if ()
			else
			{
				theIndexGUIDBuffer.append("SIndex");
				theIndexGUIDBuffer.append(aCateId);
			} // end of else
		}

        AdminManager aManager = TrexFactory.getAdminManager();
        AdminIndex aAdminIndex =null;
        try
        {
            aManager.setIndexId(theIndexGUIDBuffer.toString());
            aAdminIndex = aManager.showIndex();

        } catch (TrexException te)
        {
            throw new TrexCatalogException("Retrieval of trex index  failed!",te);
        } // end of try-catch
        TrexManagedCatalogIndexInfo aNewIndexInfo=
            new TrexManagedCatalogIndexInfo(theIndexGUIDBuffer.toString(),
                                            aType,
                                            aCateId,
                                            aAdminIndex);

        return aNewIndexInfo;
    }

        /**
         * This method sets a new default connection if the necessary connection
         * settings are passed via the client.
         *
         * @param client  reference to a specific client
         */
    synchronized private void setDefaultConnection(IActor client)
    {
        Properties props = null;

        Properties theDefaultConnectionProperties =
            getBackendObjectSupport().getMetaData().getDefaultConnectionDefinition().getProperties();
        String theClientName = client.getName();
        String theClientPWD = client.getPWD();
        String theDefaultName=
            theDefaultConnectionProperties.getProperty(JCoManagedConnectionFactory.JCO_USER);
        String theDefaultPWD=
            theDefaultConnectionProperties.getProperty(JCoManagedConnectionFactory.JCO_PASSWD);

            // check if special client was specified and if it was different from the default connection
        if( theClientName!= null        &&
            theClientPWD!= null          &&
            theClientName.length() != 0  &&
            theClientPWD.length() != 0 &&
            !theClientName.equals(theDefaultName) &&
            !theClientPWD.equals(theDefaultPWD))
        {
                // define new client settings
            props = new Properties();
            props.setProperty(  JCoManagedConnectionFactory.JCO_USER,
                                theClientName);
            props.setProperty(  JCoManagedConnectionFactory.JCO_PASSWD,
                                theClientPWD);
        }
            // change the default connection
        if(props != null)
        {
            try
            {
                JCoConnection modConn =getModDefaultJCoConnection(props);
                if(modConn.isValid())
                {
                        //update the user specific connection infos
                    this.theProperties.putAll(props);
                    this.setDefaultConnection(modConn);
                }
            }
            catch(Exception ex)
            {
                theStaticLocToLog.warn(ICatalogMessagesKeys.PCAT_EXCEPTION,
                                   new Object[] { ex.getLocalizedMessage() }, null);
            }
        }
        return;
    }

    private void searchManagedCatalogInfo(TrexManagedCatalogInfo aCatalogInfo, String theAIndexId)
        throws TrexCatalogException
    {
        AdminManager aManager = TrexFactory.getAdminManager();
        AdminIndex aAdminIndex =null;
        try
        {
            aManager.setIndexId(theAIndexId);
            aAdminIndex = aManager.showIndex();
        }
        catch (TrexException te)
        {
            throw new TrexCatalogException(te.getMessage(),te);
        } // end of catch

        SearchManager searchManager = TrexFactory.getSearchManager();

        try
        {
            searchManager.addIndexId(theAIndexId.toLowerCase(),
                                     aAdminIndex.getTMEngineName());

                // set interval
            searchManager.setResultFromTo(INTERVAL_BEGIN, CatalogParameters.trexUpperLimit);
                // add return attributes
            searchManager.addRequestedAttribute(ATTR_AREA);
            searchManager.addRequestedAttribute(ATTR_AREA_GUID);
            searchManager.addRequestedAttribute(ATTR_CATALOG);
            searchManager.addRequestedAttribute(ATTR_CATALOG_GUID);
                //searchManager.addRequestedAttribute(ATTR_AREA_SPEZ_EXIST);
            searchManager.addRequestedAttribute(ATTR_DESCRIPTION);
                //searchManager.addRequestedAttribute(ATTR_DOC_PC_CRM_THUMB);
                //searchManager.addRequestedAttribute(ATTR_TEXT_0001);
            searchManager.addRequestedAttribute("INDEX_STATUS"); // see catalogToTrex.xsl

                // set query condition
            QueryEntry entry = new QueryEntry();
            entry.setRowType(QueryEntry.ROW_TYPE_ATTRIBUTE);
            entry.setLocation(ATTR_AREA_GUID);
            entry.setContentType(QueryEntry.CONTENT_TYPE_STRING);
            entry.setValue(VALUE_ROOT, "", QueryEntry.ATTRIBUTE_OPERATOR_EQ);
            entry.setTermWeight(10000);
            searchManager.addQueryEntry(entry);

            SearchResult results = searchManager.search();
            if (results.getLastError().getCode()!=TrexReturn.NO_ERROR)
            {
                throw new TrexCatalogException(results.getLastError().getMsg());
            } // end of if ()

            ResultDocument firstDocument = results.getFirstResultDocument();
            if (firstDocument==null)
            {
                String msg =
                    theMessageResources.
                    getMessage(
                        ICatalogMessagesKeys.PCAT_ERR_TREX_AINDEX_CORRUPTED);
                throw new TrexCatalogException(msg);
            } // end of if ()

			if (!rfcUsedForR3Catalogs) {
				String aCatalogId = firstDocument.getDocumentKey();
                //finally set the correct catalog GUID
            	aCatalogInfo.setCatalogGuid(aCatalogId);
			}

            //figure out the language of the index/catalog
            String aLanguage = firstDocument.getDocumentLanguage();
            aCatalogInfo.setLanguage(aLanguage);

            DocAttribute aDocAttribute = firstDocument.getFirstDocumentAttribute();
            for ( ;aDocAttribute!=null; aDocAttribute = firstDocument.getNextDocumentAttribute())
            {
                String theAttributeName = aDocAttribute.getAttributeName();
                if (theAttributeName.equalsIgnoreCase(ATTR_CATALOG))
                {
                    String aValue = aDocAttribute.getValue1();
                    aCatalogInfo.setProperty("name", "en", aValue); //JP: warum hier "en" ???
                }
                else if (theAttributeName.equalsIgnoreCase(ATTR_DESCRIPTION))
                {
                    String aValue = aDocAttribute.getValue1();
                    aCatalogInfo.setProperty("description", "en", aValue);
                }
                // gd -- added status
                // This should give back all of the existing catalogs
                else if(theAttributeName.equalsIgnoreCase("INDEX_STATUS"))
                {
                    String aValue = aDocAttribute.getValue1();
                    aCatalogInfo.setStatus(aValue);
                    if(aValue.equals("Valid") == false)
                    {	
                    	String cguid="";
						if( aCatalogInfo.getGuid() != null )
							cguid="(CatalogKey: " + aCatalogInfo.getGuid() + ")";
                        String msg = "Invalid status '" + aValue + "' for index " + theAIndexId + cguid;
                        theStaticLocToLog.warn(
										LogUtil.APPS_COMMON_INFRASTRUCTURE,
                        				ICatalogMessagesKeys.PCAT_WARNING,
                                        new Object[] { msg }, null);
//                        throw new TRexIndexInvalidException(msg);
                    }
                } // end of if ()
            } // end of for ()
        } catch (TrexException te)
        {
            throw new TrexCatalogException(te.getMessage(),te);
        } // end of try-catch
    }

    public void storeIndexStatus(
        String theAIndexId,
        String status)
        throws TrexCatalogException
    {
        SearchManager searchManager = TrexFactory.getSearchManager();

        try
        {
            searchManager.addIndexId(
                theAIndexId.toLowerCase(),
                TrexConst.SEARCH_ENGINE_DRFUZZY);

                // set interval
            searchManager.setResultFromTo(INTERVAL_BEGIN, CatalogParameters.trexUpperLimit);
                // add return attributes
            searchManager.addRequestedAttribute(ATTR_AREA);
            searchManager.addRequestedAttribute(ATTR_AREA_GUID);
            searchManager.addRequestedAttribute(ATTR_CATALOG);
            searchManager.addRequestedAttribute(ATTR_CATALOG_GUID);
            searchManager.addRequestedAttribute(ATTR_DESCRIPTION);
            searchManager.addRequestedAttribute("INDEX_STATUS"); //JP: see catalogToTrex.xsl
            searchManager.addRequestedAttribute(ATTR_TEXT_0001);
            searchManager.addRequestedAttribute(ATTR_TIMESTAMP); //JP: see catalogToTrex.xsl

                // set query condition
            QueryEntry entry = new QueryEntry();
            entry.setRowType(QueryEntry.ROW_TYPE_ATTRIBUTE);
            entry.setLocation(ATTR_AREA_GUID);
            entry.setContentType(QueryEntry.CONTENT_TYPE_STRING);
            entry.setValue(VALUE_ROOT, "", QueryEntry.ATTRIBUTE_OPERATOR_EQ);
            entry.setTermWeight(10000);
            searchManager.addQueryEntry(entry);

            SearchResult results = searchManager.search();
            if (results.getLastError().getCode()!=TrexReturn.NO_ERROR)
            {
                throw new TrexCatalogException(results.getLastError().getMsg());
            }

            ResultDocument firstDocument = results.getFirstResultDocument();
            if (firstDocument==null)
            {
                String msg =
                    theMessageResources.
                    getMessage(
                        ICatalogMessagesKeys.PCAT_ERR_TREX_AINDEX_CORRUPTED);
                throw new TrexCatalogException(msg);
            }
            String indexLanguage = firstDocument.getDocumentLanguage();

            // update the RootDocument ...
            IndexDocument indexDoc = new IndexDocument();
            indexDoc.setContent("");
            indexDoc.setDocumentKey(firstDocument.getDocumentKey());
            indexDoc.setDocumentLanguage(indexLanguage);
            indexDoc.setDocumentAction(IndexDocument.DOCUMENT_ACTION_INDEX);
            DocAttribute aDocAttribute;
            for (aDocAttribute = firstDocument.getFirstDocumentAttribute();
                aDocAttribute != null;
                aDocAttribute = firstDocument.getNextDocumentAttribute())
            {
                String attrName     = aDocAttribute.getAttributeName();
                String attrValue1   = aDocAttribute.getValue1();
                String attrValue2   = aDocAttribute.getValue2();
                String attrOperator = DocAttribute.ATTRIBUTE_OPERATOR_EQ; //aDocAttribute.getAttributeOperator(); //JP:TODO: this is null !!!
                String attrType     = DocAttribute.ATTRIBUTE_TYPE_STRING;   //aDocAttribute.getAttributeType(); //JP:TODO: this is null !!!
                if (attrName.equalsIgnoreCase("INDEX_STATUS"))
                {
                    if(status != null) attrValue1 = status;
                }
                else if (attrName.equalsIgnoreCase(ATTR_TIMESTAMP))
                {
                    attrValue1 = "TimeStamp:" + System.currentTimeMillis();
                }
                else if (attrName.equalsIgnoreCase(ATTR_DESCRIPTION))
                {
                    attrType = DocAttribute.ATTRIBUTE_TYPE_TEXT;    // "DESCRIPTION" is searchable
                }
                else if (attrName.equalsIgnoreCase(ATTR_TEXT_0001))
                {
                    attrType = DocAttribute.ATTRIBUTE_TYPE_TEXT;    // "TEXT_0001" is searchable
                }

                indexDoc.addDocumentAttribute(attrName,
                    attrValue1 ,attrValue2, attrOperator, attrType);
            }

            IndexManager theIndexManager = TrexFactory.getIndexManager();
            theIndexManager.setIndexId(theAIndexId);
            theIndexManager.setSearchEngine(TrexConst.SEARCH_ENGINE_DRFUZZY);
            theIndexManager.useQueueServer(useQueueServer);
            theIndexManager.addDocument(indexDoc);
            theIndexManager.index();
            theIndexManager.optimize();

            /**
             * gd -- added status
             */
            Iterator anIter = theCatalogInfos.iterator();
            while ( anIter.hasNext())
            {
                ICatalogInfo anInfo = (ICatalogInfo)anIter.next();
                if ((anInfo.getProperty(TrexManagedCatalogInfo.PROPERTY_A_INDEX_ID)).equalsIgnoreCase(theAIndexId))
                {
                  ((TrexManagedCatalogInfo)anInfo).setStatus(status);
                }
            } // end of while ()anIter

        } catch (TrexException te)
        {
            throw new TrexCatalogException(te.getMessage(),te);
        }
    }

    private String getIndexLastReplicationTimestamp(String theAIndexId)
        throws TrexCatalogException
    {
        SearchManager searchManager = TrexFactory.getSearchManager();

        try
        {
            searchManager.addIndexId( theAIndexId.toLowerCase(),TrexConst.SEARCH_ENGINE_DRFUZZY);

                // set interval
            searchManager.setResultFromTo(INTERVAL_BEGIN, CatalogParameters.trexUpperLimit);
                // add return attributes
            searchManager.addRequestedAttribute(ATTR_TIMESTAMP);

                // set query condition
            QueryEntry entry = new QueryEntry();
            entry.setRowType(QueryEntry.ROW_TYPE_ATTRIBUTE);
            entry.setLocation(ATTR_AREA_GUID);
            entry.setContentType(QueryEntry.CONTENT_TYPE_STRING);
            entry.setValue(VALUE_ROOT, "", QueryEntry.ATTRIBUTE_OPERATOR_EQ);
            entry.setTermWeight(10000);
            searchManager.addQueryEntry(entry);

            SearchResult results = searchManager.search();
            if (results.getLastError().getCode()!=TrexReturn.NO_ERROR)
            {
                throw new TrexCatalogException(results.getLastError().getMsg());
            }
            // there is only one result (hopefully)
            ResultDocument firstDocument = results.getFirstResultDocument();
            if (firstDocument==null)
            {
                String msg =
                    theMessageResources.
                    getMessage(
                        ICatalogMessagesKeys.PCAT_ERR_TREX_AINDEX_CORRUPTED);
                throw new TrexCatalogException(msg);
            }
            // there is only one return attribute (hopefully)
            DocAttribute aDocAttribute = firstDocument.getFirstDocumentAttribute();
            if (aDocAttribute==null ||
                ! aDocAttribute.getAttributeName().equalsIgnoreCase(ATTR_TIMESTAMP))
            {
                String msg =
                    theMessageResources.
                    getMessage(
                        ICatalogMessagesKeys.PCAT_ERR_TREX_AINDEX_CORRUPTED);
                throw new TrexCatalogException(msg);
            }
            return aDocAttribute.getValue1();
        } catch (TrexException te)
        {
            throw new TrexCatalogException(te.getMessage(),te);
        }
    }
    
	private boolean isIndexStillValid(String theIndexGuid)
		throws TrexCatalogException
	{
		//find out if the index is still valid.
		SearchResult theSearchResult = null;
		String theStatus = "";
		
		try {
			SearchManager theSearchManager = TrexFactory.getSearchManager();
				
			QueryEntry aQueryEntry = new QueryEntry();
			aQueryEntry.setValue("$ROOT_ISA_ERP", "", QueryEntry.ATTRIBUTE_OPERATOR_EQ);
			aQueryEntry.setRowType(QueryEntry.ROW_TYPE_ATTRIBUTE);
			aQueryEntry.setLocation("AREA");
			aQueryEntry.setTermAction(QueryEntry.TERM_ACTION_EXACT);
			theSearchManager.addQueryEntry(aQueryEntry);
				
			//TODO: determine language
			theSearchManager.addIndexId(theIndexGuid, "DRFUZZY");
			theSearchManager.setResultFromTo(1, CatalogParameters.trexUpperLimit);
			theSearchManager.addRequestedAttribute("INDEX_STATUS");
			
			//TODO: theSearchManager.setDoNotUseCache(); //not yet available
			theSearchResult = theSearchManager.search();
			}
			catch (TrexException ex) {
				theSearchResult = new SearchResult();
				theStaticLocToLog.error(LogUtil.APPS_COMMON_INFRASTRUCTURE,
					ICatalogMessagesKeys.PCAT_ERR_INIT_NO_ERP_ROOT, new Object[] {theIndexGuid, ex.getLocalizedMessage()},
					ex );
				return false;
			}
		
			if (theSearchResult.getNoOfAllHitsInIndex() == 0) {//for some reason getNoOfHits() doesn't work...
				//Index does not contain a $ROOT_ISA_ERP document, hence something's wrong...
				theStaticLocToLog.error("A-Index " + theIndexGuid + " contains no root document, rebuilding CatalogInfos!");
				return false;
			}

			ResultDocument aResultDocument = theSearchResult.getFirstResultDocument(); //assuming there's only 1
			DocAttribute aDocAttribute = aResultDocument.getFirstDocAttribute();
			while (aDocAttribute != null) {
				if (aDocAttribute.getAttributeName().equalsIgnoreCase("INDEX_STATUS")) {
					theStatus = aDocAttribute.getValue1();
				}
				aDocAttribute = aResultDocument.getNextDocAttribute();
			}
			
			return (theStatus.equalsIgnoreCase("Valid"));
	}

	/* 
	 * Not used with Trex R/3 
	 */
	public int getPort() {
		return 0;
	}

}// TrexCatalogServerEngine
