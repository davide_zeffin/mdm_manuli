/*****************************************************************************
  Copyright (c) 2000, SAPMarkets Europe GmbH, Germany, All rights reserved.
  Created:      06 March 2002

  $Id: //sap/ESALES_base/30_SP_COR/src/_pcatAPI/java/com/sap/isa/catalog/trex/TrexManagedCatalogBuilder.java#10 $
  $Revision: #10 $
  $Change: 168739 $
  $DateTime: 2004/02/04 09:26:00 $
*****************************************************************************/
package com.sap.isa.catalog.trex;

// misc imports
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;

import com.sap.isa.catalog.CatalogParameters;
import com.sap.isa.catalog.ICatalogMessagesKeys;
import com.sap.isa.catalog.boi.CatalogException;
import com.sap.isa.catalog.boi.ICategory;
import com.sap.isa.catalog.boi.IDetail;
import com.sap.isa.catalog.boi.IQueryStatement;
import com.sap.isa.catalog.filter.CatalogFilterInvalidException;
import com.sap.isa.catalog.impl.Catalog;
import com.sap.isa.catalog.impl.CatalogAttribute;
import com.sap.isa.catalog.impl.CatalogAttributeValue;
import com.sap.isa.catalog.impl.CatalogBuildFailedException;
import com.sap.isa.catalog.impl.CatalogBuilder;
import com.sap.isa.catalog.impl.CatalogCategory;
import com.sap.isa.catalog.impl.CatalogCompositeComponent;
import com.sap.isa.catalog.impl.CatalogDetail;
import com.sap.isa.catalog.impl.CatalogItem;
import com.sap.isa.catalog.impl.CatalogQuery;
import com.sap.isa.catalog.impl.CatalogQueryItem;
import com.sap.isa.catalog.impl.CatalogQueryStatement;
import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.core.util.table.ResultData;
import com.sapportals.trex.TrexException;
import com.sapportals.trex.TrexReturn;
import com.sapportals.trex.core.DocAttribute;
import com.sapportals.trex.core.QueryEntry;
import com.sapportals.trex.core.SearchManager;
import com.sapportals.trex.core.SearchResult;
import com.sapportals.trex.core.SearchResultDoc;

/**
 * This class builds the catalog tree for the Trex catalog implementation.
 * @version     1.0
 */
class TrexManagedCatalogBuilder
    extends CatalogBuilder
    implements TrexCatalogConstants
{
    private final static HashSet theExcludedAttributes;

    private TrexManagedCatalog theTrexManagedCatalog;

       // the logging instance
    private static IsaLocation theStaticLocToLog =
        IsaLocation.getInstance(TrexManagedCatalogBuilder.class.getName());
   
	//	D041871 - fix of Note 846055
	public String sortAttribute = null;     

    static
    {
        theExcludedAttributes = new HashSet();
        theExcludedAttributes.add(ATTR_AREA);
        theExcludedAttributes.add(ATTR_AREA_GUID);
        theExcludedAttributes.add(ATTR_AREA_SPEZ_EXIST);
        theExcludedAttributes.add(ATTR_AREA_TYPE);
        theExcludedAttributes.add(ATTR_CATALOG);
        theExcludedAttributes.add(ATTR_CATALOG_GUID);
        theExcludedAttributes.add(ATTR_OBJECT_GUID);
        theExcludedAttributes.add(ATTR_PARENT_AREA);
        theExcludedAttributes.add(ATTR_PARENT_AREA_GUID);
        theExcludedAttributes.add(ATTR_SUBAREA_EXISTS);
        theExcludedAttributes.add(ATTR_VIEWS_GUID);
        theExcludedAttributes.add(ATTR_VIEWS_ID);
    }

        /**
         * Default Constructor for the Trex catalog builder.
         *
         * @param aCatalog  the TrexCatalog which has to be built
         */
    TrexManagedCatalogBuilder(TrexManagedCatalog aManagedCatalog)
    {
            // store catalog
        theTrexManagedCatalog = aManagedCatalog;
    }

    protected void buildCategoriesInternal(Catalog aCatalog)
        throws CatalogBuildFailedException
    {
    	theStaticLocToLog.debug("buildCategoriesInternal(Catalog aCatalog)");
        try
        {
            TrexManagedCatalogIndexInfo index
                = theTrexManagedCatalog.getIndexInfo(INDEX_LEVEL_A, "");
            SearchManager searchManager =
                index.getSearchManager(theTrexManagedCatalog.getLanguage());
                // set interval
            searchManager.setResultFromTo(INTERVAL_BEGIN, CatalogParameters.trexUpperLimit);
                // add return attributes
            searchManager.addRequestedAttribute(ATTR_AREA);
            searchManager.addRequestedAttribute(ATTR_AREA_GUID);
            searchManager.addRequestedAttribute(ATTR_AREA_SPEZ_EXIST);
            searchManager.addRequestedAttribute(ATTR_DESCRIPTION);
            if(index.hasAttribute(ATTR_DOC_PC_CRM_THUMB)) // not always available
                searchManager.addRequestedAttribute(ATTR_DOC_PC_CRM_THUMB);
            if(index.hasAttribute(ATTR_DOC_PC_CRM_IMAGE)) // not always available
                searchManager.addRequestedAttribute(ATTR_DOC_PC_CRM_IMAGE);
            if(index.hasAttribute(ATTR_TEXT_0001)) // not always available
                searchManager.addRequestedAttribute(ATTR_TEXT_0001);
                // set sort order - only possible with DrFuzzy
            if(index.getSearchEngine().equalsIgnoreCase(ENGINE_DRFUZZY))
                searchManager.addSortAttribute(ATTR_POS_NR, true);

                // set query condition
            QueryEntry entry = new QueryEntry();
            entry.setRowType(QueryEntry.ROW_TYPE_ATTRIBUTE);
            entry.setLocation(ATTR_PARENT_AREA_GUID);
            entry.setContentType(QueryEntry.CONTENT_TYPE_STRING);
            entry.setValue(VALUE_ROOT, "", QueryEntry.ATTRIBUTE_OPERATOR_EQ);
            entry.setTermWeight(10000);
            searchManager.addQueryEntry(entry);

                // set condition for the views
            String[] views = theTrexManagedCatalog.getViews();
            if(views != null && views.length > 0)
                addViewsToSearch(views, searchManager);
                // execute query
            SearchResult results = searchManager.search();
            if(results.getLastError().getCode() != TrexReturn.NO_ERROR)
            {
                String msg = results.getLastError().getCode() + ": " +
                    results.getLastError().getMsg();
                throw new CatalogBuildFailedException(msg);
            }
            else if(theStaticLocToLog.isInfoEnabled())
            {
                theStaticLocToLog.info(ICatalogMessagesKeys.PCAT_IMS_RET_HITS,
                                   new Object[] { new Integer(results.size()) }, null);
            }

                // analyse results
            this.addCategories(results, theTrexManagedCatalog);

        }
        catch(TrexException ex)
        {
            throw new CatalogBuildFailedException(ex.getLocalizedMessage());
        }
        catch(TrexCatalogException ex)
        {
            throw new CatalogBuildFailedException(ex.getLocalizedMessage());
        }
    }

    protected void buildAllCategoriesInternal(Catalog aCatalog)
        throws CatalogBuildFailedException
    {
    	
    	theStaticLocToLog.debug("buildAllCategoriesInternal(Catalog aCatalog)");
        try
        {
            String[] views = theTrexManagedCatalog.getViews();
            
            TrexManagedCatalogIndexInfo index =
                theTrexManagedCatalog.getIndexInfo(INDEX_LEVEL_A, "");
            SearchManager searchManager =
                index.getSearchManager(theTrexManagedCatalog.getLanguage());
                // set interval
            searchManager.setResultFromTo(INTERVAL_BEGIN, CatalogParameters.trexUpperLimit);
                // add return attributes
            searchManager.addRequestedAttribute(ATTR_AREA);
            searchManager.addRequestedAttribute(ATTR_AREA_GUID);
            searchManager.addRequestedAttribute(ATTR_AREA_SPEZ_EXIST);
            searchManager.addRequestedAttribute(ATTR_PARENT_AREA_GUID);
            searchManager.addRequestedAttribute(ATTR_DESCRIPTION);
            if (views != null && views.length > 0 )
	            searchManager.addRequestedAttribute(ATTR_VIEWS_ID);
            
            if(index.hasAttribute(ATTR_DOC_PC_CRM_THUMB)) // not always available
                searchManager.addRequestedAttribute(ATTR_DOC_PC_CRM_THUMB);
            if(index.hasAttribute(ATTR_DOC_PC_CRM_IMAGE)) // not always available
                searchManager.addRequestedAttribute(ATTR_DOC_PC_CRM_IMAGE);
            if(index.hasAttribute(ATTR_TEXT_0001)) // not always available
                searchManager.addRequestedAttribute(ATTR_TEXT_0001);
                // set sort order - only possible with DrFuzzy
            if(index.getSearchEngine().equalsIgnoreCase(ENGINE_DRFUZZY))
                searchManager.addSortAttribute(ATTR_POS_NR, true);

            //set query condition
            QueryEntry entry = new QueryEntry();
            entry.setRowType(QueryEntry.ROW_TYPE_ATTRIBUTE);
            entry.setLocation(ATTR_AREA_GUID);
            entry.setContentType(QueryEntry.CONTENT_TYPE_STRING);
            entry.setValue(VALUE_ROOT, "", QueryEntry.ATTRIBUTE_OPERATOR_NE);
            entry.setTermWeight(10000);
            searchManager.addQueryEntry(entry);
                
            if(views != null && views.length > 0)
                addViewsToSearch(views, searchManager);
				            
                // execute query
            SearchResult results = searchManager.search();
            if(results.getLastError().getCode() != TrexReturn.NO_ERROR)
            {
                String msg = results.getLastError().getCode() + ": " +
                    results.getLastError().getMsg();
                throw new CatalogBuildFailedException(msg);
            }
            else if(theStaticLocToLog.isInfoEnabled())
            {
                theStaticLocToLog.info(ICatalogMessagesKeys.PCAT_IMS_RET_HITS,
                                   new Object[] { new Integer(results.size()) }, null);
            }

                // analyse results
            this.addAllCategories(results, theTrexManagedCatalog);

        }
        catch(TrexException ex)
        {
            throw new CatalogBuildFailedException(ex.getLocalizedMessage());
        }
        catch(TrexCatalogException ex)
        {
            throw new CatalogBuildFailedException(ex.getLocalizedMessage());
        }
    }

    protected void buildCategoriesInternal(CatalogCategory category)
        throws CatalogBuildFailedException
    {
    	theStaticLocToLog.debug("buildCategoriesInternal(CatalogCategory category)");
        try
        {
            TrexManagedCatalogIndexInfo index  =
                theTrexManagedCatalog.getIndexInfo(INDEX_LEVEL_A, "");
            SearchManager searchManager =
                index.getSearchManager(theTrexManagedCatalog.getLanguage());
                // set interval
            searchManager.setResultFromTo(INTERVAL_BEGIN, CatalogParameters.trexUpperLimit);
                // add return attributes
            searchManager.addRequestedAttribute(ATTR_AREA);
            searchManager.addRequestedAttribute(ATTR_AREA_GUID);
            searchManager.addRequestedAttribute(ATTR_AREA_SPEZ_EXIST);
            searchManager.addRequestedAttribute(ATTR_DESCRIPTION);
            if(index.hasAttribute(ATTR_DOC_PC_CRM_THUMB)) // not always available
                searchManager.addRequestedAttribute(ATTR_DOC_PC_CRM_THUMB);
            if(index.hasAttribute(ATTR_DOC_PC_CRM_IMAGE)) // not always available
                searchManager.addRequestedAttribute(ATTR_DOC_PC_CRM_IMAGE);
            if(index.hasAttribute(ATTR_TEXT_0001)) // not always available
                searchManager.addRequestedAttribute(ATTR_TEXT_0001);
                // set sort order - only possible with DrFuzzy (performance!)
            if(index.getSearchEngine().equalsIgnoreCase(ENGINE_DRFUZZY))
                searchManager.addSortAttribute(ATTR_POS_NR,true);

                // set query condition
            QueryEntry entry = new QueryEntry();
            entry.setRowType(QueryEntry.ROW_TYPE_ATTRIBUTE);
            entry.setLocation(ATTR_PARENT_AREA_GUID);
            entry.setContentType(QueryEntry.CONTENT_TYPE_STRING);
            entry.setValue(category.getGuid(), "", QueryEntry.ATTRIBUTE_OPERATOR_EQ);
            entry.setTermWeight(10000);
            searchManager.addQueryEntry(entry);

                // set condition for the views
            String[] views = theTrexManagedCatalog.getViews();
            if(views != null && views.length > 0)
                addViewsToSearch(views, searchManager);

                // execute query
            SearchResult results = searchManager.search();
            if(results.getLastError().getCode() != TrexReturn.NO_ERROR)
            {
                String msg = results.getLastError().getCode() + ": " +
                    results.getLastError().getMsg();
                throw new CatalogBuildFailedException(msg);
            }
            else if(theStaticLocToLog.isInfoEnabled())
            {
                theStaticLocToLog.info(ICatalogMessagesKeys.PCAT_IMS_RET_HITS,
                                   new Object[] { new Integer(results.size()) }, null);
            }
                // analyse results
            this.addCategories(results, category);
        }
        catch(TrexException ex)
        {
            throw new CatalogBuildFailedException(ex.getLocalizedMessage());
        }
        catch(TrexCatalogException ex)
        {
            throw new CatalogBuildFailedException(ex.getLocalizedMessage());
        }
    }

    protected void buildItemsInternal(CatalogCategory category)
        throws CatalogBuildFailedException
    {
    	theStaticLocToLog.debug("buildItemsInternal(CatalogCategory category)");
            // first build all attributes if not already done
        if (!isAttributesBuilt(theTrexManagedCatalog))
            this.buildAttributes(theTrexManagedCatalog);

        try
        {
            SearchResult resCategory = null, resProduct = null;

            TrexManagedCatalogIndexInfo index  =
                theTrexManagedCatalog.getIndexInfo(INDEX_LEVEL_P, "");
            SearchManager searchManager =
                index.getSearchManager(theTrexManagedCatalog.getLanguage());
                // set interval
            searchManager.setResultFromTo(INTERVAL_BEGIN, CatalogParameters.trexUpperLimit);
                // add return attributes
            for(Iterator it = index.getAttributes().iterator(); it.hasNext(); )
            {
                String name = (String)it.next();
                if(!theExcludedAttributes.contains(name))
                    searchManager.addRequestedAttribute(name);
            }
            searchManager.addRequestedAttribute(ATTR_AREA);
            searchManager.addRequestedAttribute(ATTR_AREA_GUID);
            searchManager.addRequestedAttribute(ATTR_OBJECT_GUID);
                // set sort order - only possible for DrFuzzy (performance!)
            if(index.getSearchEngine().equalsIgnoreCase(ENGINE_DRFUZZY))
                searchManager.addSortAttribute(ATTR_POS_NR, true);

                // set query condition
            QueryEntry entry = new QueryEntry();
            entry.setRowType(QueryEntry.ROW_TYPE_ATTRIBUTE);
            entry.setLocation(ATTR_AREA_GUID);
            entry.setContentType(QueryEntry.CONTENT_TYPE_STRING);
            entry.setValue(category.getGuid(), "", QueryEntry.ATTRIBUTE_OPERATOR_EQ);
            entry.setTermWeight(10000);
            searchManager.addQueryEntry(entry);

                // set condition for the views
            String[] views = theTrexManagedCatalog.getViews();
            if(views != null && views.length > 0)
                addViewsToSearch(views, searchManager);
                // execute query
            resProduct = searchManager.search();
            if(resProduct.getLastError().getCode() != TrexReturn.NO_ERROR) {
                String msg = resProduct.getLastError().getCode() + ": " +
                    resProduct.getLastError().getMsg();
                throw new CatalogBuildFailedException(msg);
            }
            else if(theStaticLocToLog.isInfoEnabled())
            {
                theStaticLocToLog.info(ICatalogMessagesKeys.PCAT_IMS_RET_HITS,
                                   new Object[] { new Integer(resProduct.size()) }, null);
            }

                //
                // read also the category specific attributes if necessary
                //
            if(category.getAttributes().hasNext() && resProduct.size() > 0)
            {
                    // get name of the area
                SearchResultDoc firstDocument = resProduct.getFirstDocument();
                DocAttribute attribute = firstDocument.getFirstDocumentAttribute();
                while(!attribute.getAttributeName().equalsIgnoreCase(ATTR_AREA))
                    attribute = firstDocument.getNextDocumentAttribute();
                    // get index
                index = theTrexManagedCatalog.getIndexInfo(INDEX_LEVEL_S,category.getGuid());
                searchManager =
                    index.getSearchManager(theTrexManagedCatalog.getLanguage());
                    // set interval
                searchManager.setResultFromTo(INTERVAL_BEGIN, CatalogParameters.trexUpperLimit);
                    // set return properties
                searchManager.addRequestedAttribute(ATTR_OBJECT_GUID);
                    // get all category specific attributes
                for(Iterator it = category.getAttributes(); it.hasNext(); )
                {
                    String name = ((CatalogAttribute)it.next()).getName();
                    searchManager.addRequestedAttribute(name);
                }
                    // set query condition (first get all object guids of products which
                    // were found in the 'P' index)
                for(SearchResultDoc document = resProduct.getFirstDocument();
                    document != null;
                    document = resProduct.getNextDocument())
                {
                    attribute = document.getFirstDocumentAttribute();
                    while(attribute != null)
                    {
                        if(attribute.getAttributeName().equalsIgnoreCase(ATTR_OBJECT_GUID))
                        {
                            entry = new QueryEntry();
                            entry.setRowType(QueryEntry.ROW_TYPE_ATTRIBUTE);
                            entry.setLocation(ATTR_OBJECT_GUID);
                            entry.setContentType(QueryEntry.CONTENT_TYPE_STRING);
                            entry.setValue(attribute.getValue1(), "",
                                           QueryEntry.ATTRIBUTE_OPERATOR_EQ);
                            entry.setTermWeight(10000);
                            searchManager.addQueryEntry(entry);
                            if(firstDocument != document)
                            {
                                    // add the "OR"
                                entry = new QueryEntry();
                                entry.setRowType(QueryEntry.ROW_TYPE_OPERATOR);
                                entry.setValue(QueryEntry.QUERY_OPERATOR_OR, "",
                                               QueryEntry.ATTRIBUTE_OPERATOR_EQ);
                                searchManager.addQueryEntry(entry);
                            }
                                // abort search
                            attribute = null;
                        }
                        else
                        {
                            attribute = document.getNextDocumentAttribute();
                        }
                    }

                }

                    // execute query
                resCategory = searchManager.search();
                if(resCategory.getLastError().getCode() != TrexReturn.NO_ERROR)
                {
                    String msg = resCategory.getLastError().getCode() + ": " +
                        resCategory.getLastError().getMsg();
                    throw new CatalogBuildFailedException(msg);
                }
                else if(theStaticLocToLog.isInfoEnabled())
                {
                    theStaticLocToLog.info(ICatalogMessagesKeys.PCAT_IMS_RET_HITS,
                                       new Object[] { new Integer(resCategory.size()) }, null);
                }

            }

                // analyse results
            this.addItems(resProduct, resCategory, category);
        }
        catch(TrexException ex)
        {
            throw new CatalogBuildFailedException(ex.getLocalizedMessage());
        }
        catch(TrexCatalogException ex)
        {
            throw new CatalogBuildFailedException(ex.getLocalizedMessage());
        }
        catch(CatalogException ex)
        {
            throw new CatalogBuildFailedException(ex.getLocalizedMessage());
        }
    }

    protected void buildDetailsInternal(Catalog catalog)
        throws CatalogBuildFailedException
    {
        try
        {
                // currently this are only the pricing infos
                /*
            ResultData results = TrexCatalogRfcMethods.getPricingMetaInfo(
                theTrexManagedCatalog.getDefaultJCoConnection(),
                theTrexManagedCatalog.getName(),
                theTrexManagedCatalog.getVariant());

                // analyse results
            this.addDetails(results, theTrexManagedCatalog);
                */
        }
        catch(Exception ex)
        {
            throw new CatalogBuildFailedException(ex.getLocalizedMessage());
        }
    }

    protected void buildDetailsInternal(CatalogCategory category)
        throws CatalogBuildFailedException
    {
    	theStaticLocToLog.debug("buildDetailsInternal(CatalogCategory category)");
        try
        {
            TrexManagedCatalogIndexInfo index =
                theTrexManagedCatalog.getIndexInfo(INDEX_LEVEL_A, "");
            SearchManager searchManager =
                index.getSearchManager(theTrexManagedCatalog.getLanguage());
                // set interval
            searchManager.setResultFromTo(INTERVAL_BEGIN, CatalogParameters.trexUpperLimit);
                // add return attributes
            for(Iterator it = index.getAttributes().iterator(); it.hasNext(); )
            {
                String name = (String)it.next();
                if((!theExcludedAttributes.contains(name) &&
                    !name.equalsIgnoreCase(ATTR_TEXT_0001) &&
                    !name.equalsIgnoreCase(ATTR_DESCRIPTION) &&
                    !name.equalsIgnoreCase(ATTR_DOC_PC_CRM_THUMB)) ||
                   (name.equalsIgnoreCase(ATTR_AREA)))
                    searchManager.addRequestedAttribute(name);
            }
                // set sort order - only possible for DrFuzzy
            if(index.getSearchEngine().equalsIgnoreCase(ENGINE_DRFUZZY))
                searchManager.addSortAttribute(ATTR_POS_NR, true);
                // set query condition
            QueryEntry entry = new QueryEntry();
            entry.setRowType(QueryEntry.ROW_TYPE_ATTRIBUTE);
            entry.setLocation(ATTR_AREA_GUID);
            entry.setContentType(QueryEntry.CONTENT_TYPE_STRING);
            entry.setValue(category.getGuid(), "", QueryEntry.ATTRIBUTE_OPERATOR_EQ);
            entry.setTermWeight(10000);
            searchManager.addQueryEntry(entry);

                // set condition for the views
            String[] views = theTrexManagedCatalog.getViews();
            if(views != null && views.length > 0)
                addViewsToSearch(views, searchManager);

                // execute query
            SearchResult results = searchManager.search();
            if(results.getLastError().getCode() != TrexReturn.NO_ERROR)
            {
                String msg = results.getLastError().getCode() + ": " +
                    results.getLastError().getMsg();
                throw new CatalogBuildFailedException(msg);
            }
            else if(theStaticLocToLog.isInfoEnabled())
            {
                theStaticLocToLog.info(ICatalogMessagesKeys.PCAT_IMS_RET_HITS,
                                   new Object[] { new Integer(results.size()) }, null);
            }
                // analyse results
            this.addDetails(results, category);
        }
        catch(TrexException ex)
        {
            throw new CatalogBuildFailedException(ex.getLocalizedMessage());
        }
        catch(TrexCatalogException ex)
        {
            throw new CatalogBuildFailedException(ex.getLocalizedMessage());
        }
    }

    protected void buildDetailsInternal(CatalogAttribute aAttribute)
        throws CatalogBuildFailedException
    {
            // are builded in the same method as the attributes
    }

    protected void buildDetailsInternal(CatalogItem aItem)
        throws CatalogBuildFailedException
    {
            // no details available
    }

    protected void buildDetailsInternal(CatalogDetail aDetail)
        throws CatalogBuildFailedException
    {
            // no details available
    }

    protected void buildDetailsInternal(CatalogAttributeValue aValue)
        throws CatalogBuildFailedException
    {
            // no details available
    }

    protected void buildAttributesInternal(Catalog catalog)
        throws CatalogBuildFailedException
    {
        try
        {
            TrexManagedCatalogIndexInfo index =
                theTrexManagedCatalog.getIndexInfo(INDEX_LEVEL_P, "");
            this.addAttributes(index, theTrexManagedCatalog);
        }
        catch(Exception ex)
        {
            throw new CatalogBuildFailedException(ex.getLocalizedMessage());
        }
    }

    protected void buildAttributesInternal(CatalogCategory category)
        throws CatalogBuildFailedException
    {
    	theStaticLocToLog.debug("buildAttributesInternal(CatalogCategory category)");
    	
        try
        {
            TrexManagedCatalogIndexInfo index =
                theTrexManagedCatalog.getIndexInfo(INDEX_LEVEL_A, "");
            SearchManager searchManager =
                index.getSearchManager(theTrexManagedCatalog.getLanguage());
                // set interval
            searchManager.setResultFromTo(INTERVAL_BEGIN, 2);
                // set return properties
            searchManager.addRequestedAttribute(ATTR_AREA_SPEZ_EXIST);
            searchManager.addRequestedAttribute(ATTR_AREA);
            // gd --- adding LAST
            searchManager.addRequestedAttribute(ATTR_TIMESTAMP);

                // set query condition
            QueryEntry entry = new QueryEntry();
            entry.setRowType(QueryEntry.ROW_TYPE_ATTRIBUTE);
            entry.setLocation(ATTR_AREA_GUID);
            entry.setContentType(QueryEntry.CONTENT_TYPE_STRING);
            entry.setValue(category.getGuid(), "", QueryEntry.ATTRIBUTE_OPERATOR_EQ);
            entry.setTermWeight(10000);
            searchManager.addQueryEntry(entry);

                // set condition for the views
            String[] views = theTrexManagedCatalog.getViews();
            if(views != null && views.length > 0)
                addViewsToSearch(views, searchManager);

                // execute query
            SearchResult results = searchManager.search();
            if(results.getLastError().getCode() != TrexReturn.NO_ERROR)
            {
                String msg = results.getLastError().getCode() + ": " +
                    results.getLastError().getMsg();
                throw new CatalogBuildFailedException(msg);
            } else if(theStaticLocToLog.isInfoEnabled()) {
                theStaticLocToLog.info(ICatalogMessagesKeys.PCAT_IMS_RET_HITS,
                                   new Object[] { new Integer(results.size()) }, null);
            }

                // now get 'S' index
            String areaName = null;
            boolean indexExists = false;

            // gd --- this is an infinite loop
            SearchResultDoc firstDoc = results.getFirstDocument();
            DocAttribute attribute = firstDoc.getFirstDocumentAttribute();
            while(attribute != null)
            {
                if(attribute.getAttributeName().equalsIgnoreCase(ATTR_AREA_SPEZ_EXIST) &&
                   attribute.getValue1().equals(VALUE_X))
                    indexExists = true;

                if(attribute.getAttributeName().equalsIgnoreCase(ATTR_AREA))
                    areaName = attribute.getValue1();

                    // gd ---
                if(attribute.getAttributeName().equalsIgnoreCase(ATTR_TIMESTAMP))
                    theTrexManagedCatalog.setReplTimeStamp(attribute.getValue1());

                attribute = firstDoc.getNextDocumentAttribute();
            }

            if(indexExists)
            {
                index = theTrexManagedCatalog.getIndexInfo(INDEX_LEVEL_S,category.getGuid());
                this.addAttributes(index, category);
            }
        }
        catch(TrexException ex)
        {
            throw new CatalogBuildFailedException(ex.getLocalizedMessage());
        }
        catch(TrexCatalogException ex)
        {
            throw new CatalogBuildFailedException(ex.getLocalizedMessage());
        }
    }

    protected void buildQueryInternal(CatalogQuery query)
        throws CatalogBuildFailedException
    {
        CatalogQueryStatement statement = query.getCatalogQueryStatement();

        if(statement.getSearchType() == IQueryStatement.Type.CATEGORY_SEARCH)
            buildQueryCategoriesInternal(query);
        else
            buildQueryItemsInternal(query);
    }

        /**
         * Internal helper method to build a query for categories.
         */
    protected void buildQueryCategoriesInternal(CatalogQuery query)
        throws CatalogBuildFailedException
    {
    	theStaticLocToLog.debug("buildQueryCategoriesInternal(CatalogQuery query)");
            // first build all categories if not already done
        if(!isAllCategoriesBuilt(theTrexManagedCatalog))
            this.buildAllCategories(theTrexManagedCatalog);

        try
        {
            TrexManagedCatalogIndexInfo index =
                theTrexManagedCatalog.getIndexInfo(INDEX_LEVEL_A, "");
            SearchManager searchManager =
                index.getSearchManager(theTrexManagedCatalog.getLanguage());
                // create new helper instances
            TrexCatalogQueryCompiler compiler
                = new TrexCatalogQueryCompiler(
                    theTrexManagedCatalog.getMessageResources(),
                    ((TrexCatalogServerEngine)theTrexManagedCatalog.getServerEngine()).
                    getQuickSearchStrategy());
                // set return properties
            List defAttr = new ArrayList();
            for(Iterator iter = index.getAttributes().iterator(); iter.hasNext(); )
            {
                String name = (String)iter.next();
                if(!theExcludedAttributes.contains(name) && !name.equalsIgnoreCase(ATTR_TEXT_0001) &&
                   !name.equalsIgnoreCase(ATTR_DESCRIPTION) && !name.equalsIgnoreCase(ATTR_DOC_PC_CRM_THUMB))
                    defAttr.add(name);
            }
                // set default return attributes
            searchManager.addRequestedAttribute(ATTR_AREA_GUID);
                // transform query
            compiler.transformCategoryQuery(query,
                                            defAttr,
                                            index.getAttributes(),
                                            index.getSearchEngine(),
                                            searchManager);
                // add vies to search
            String[] views = theTrexManagedCatalog.getViews();
            if(views != null && views.length > 0)
                addViewsToSearch(views, searchManager);
                // execute query
            SearchResult results = searchManager.search();
            if(results.getLastError().getCode() != TrexReturn.NO_ERROR)
            {
                String msg = results.getLastError().getCode() + ": " +
                    results.getLastError().getMsg();
                throw new CatalogBuildFailedException(msg);
            } else if(theStaticLocToLog.isInfoEnabled())
            {
                theStaticLocToLog.info(ICatalogMessagesKeys.PCAT_IMS_RET_HITS,
                                   new Object[] { new Integer(results.size()) }, null);
            }

                // analyse results
            this.addQuery(results, query);
        }
        catch(TrexException ex)
        {
            throw new CatalogBuildFailedException(ex.getLocalizedMessage());
        }
        catch(TrexCatalogException ex)
        {
            throw new CatalogBuildFailedException(ex.getLocalizedMessage());
        }
        catch(CatalogFilterInvalidException ex)
        {
            throw new CatalogBuildFailedException(ex.getLocalizedMessage());
        }
    }

        /**
         * Internal helper method to build a query for items.
         */
    protected void buildQueryItemsInternal(CatalogQuery query)
        throws CatalogBuildFailedException
    {
    	theStaticLocToLog.debug("buildQueryCategoriesInternal(CatalogQuery query)");
            // first build all global attributes if not already done
        if (!isAttributesBuilt(theTrexManagedCatalog))
            this.buildAttributes(theTrexManagedCatalog);

        try
        {
            TrexManagedCatalogIndexInfo index;
            SearchManager searchManager;
            SearchResult resCategory = null, resProduct = null;

                // create new helper instances
            TrexCatalogQueryCompiler compiler
                = new TrexCatalogQueryCompiler(
                    theTrexManagedCatalog.getMessageResources(),
                    ((TrexCatalogServerEngine)theTrexManagedCatalog.getServerEngine())
                    .getQuickSearchStrategy());

                // category specific search
            if (query.isCategorySpecificSearch())
            {
                    // get category for which query was defined
                ICategory cat = query.getParent();
                    // ask 'P' index for area name
                    // set environment info for 'P' index
                index = theTrexManagedCatalog.getIndexInfo(INDEX_LEVEL_A, "");
                searchManager =
                    index.getSearchManager(theTrexManagedCatalog.getLanguage());
                    // set return properties
                searchManager.setResultFromTo(INTERVAL_BEGIN, CatalogParameters.trexUpperLimit);
                searchManager.addRequestedAttribute(ATTR_AREA);
                    // set query condition
                QueryEntry entry = new QueryEntry();
                entry.setRowType(QueryEntry.ROW_TYPE_ATTRIBUTE);
                entry.setLocation(ATTR_AREA_GUID);
                entry.setContentType(QueryEntry.CONTENT_TYPE_STRING);
                entry.setValue(cat.getGuid(), "", QueryEntry.ATTRIBUTE_OPERATOR_EQ);
                entry.setTermWeight(10000);
                searchManager.addQueryEntry(entry);
                    // execute query
                resProduct = searchManager.search();
                if(resProduct.getLastError().getCode() != TrexReturn.NO_ERROR) {
                    String msg = resProduct.getLastError().getCode() + ": " +
                        resProduct.getLastError().getMsg();
                    throw new CatalogBuildFailedException(msg);
                }
                else if(theStaticLocToLog.isInfoEnabled())
                {
                    theStaticLocToLog.info(ICatalogMessagesKeys.PCAT_IMS_RET_HITS,
                                       new Object[] { new Integer(resProduct.size()) }, null);
                }

                    // now ask 'S' index for all entries which conform to filter expression
                SearchResultDoc firstDocument = resProduct.getFirstDocument();
                DocAttribute attribute = firstDocument.getFirstDocumentAttribute();
                while(!attribute.getAttributeName().equalsIgnoreCase(ATTR_AREA))
                    attribute = firstDocument.getNextDocumentAttribute();

                index = theTrexManagedCatalog.getIndexInfo(INDEX_LEVEL_S,cat.getGuid());
                resProduct = null;
                searchManager =
                    index.getSearchManager(theTrexManagedCatalog.getLanguage());
                    // set default return attributes for 'S' index
                searchManager.addRequestedAttribute(ATTR_OBJECT_GUID);
                    // transform query statement
                ArrayList defAttr = new ArrayList();
                for(Iterator it = index.getAttributes().iterator(); it.hasNext(); )
                {
                    String name = (String)it.next();
                        // add to default attribute list
                    if(!theExcludedAttributes.contains(name))
                        defAttr.add(name);
                }
                compiler.transformItemQuery(query,
                                            defAttr,
                                            index.getAttributes(),
                                            theTrexManagedCatalog,
                                            index.getSearchEngine(),
                                            searchManager);
                    // add vies to search
                String[] views = theTrexManagedCatalog.getViews();
                if(views != null && views.length > 0)
                    addViewsToSearch(views, searchManager);
                    // execute query
                resCategory = searchManager.search();
                if(resCategory.getLastError().getCode() != TrexReturn.NO_ERROR)
                {
                    String msg = resCategory.getLastError().getCode() + ": " +
                        resCategory.getLastError().getMsg();
                    throw new CatalogBuildFailedException(msg);
                }
                else if(theStaticLocToLog.isInfoEnabled())
                {
                    theStaticLocToLog.info(ICatalogMessagesKeys.PCAT_IMS_RET_HITS,
                                       new Object[] { new Integer(resCategory.size()) }, null);
                }
                    // check if something was found - if not there are no results!
                if(resCategory.size() == 0)
                    return;
            }

                // set environment info for 'P' index
            index = theTrexManagedCatalog.
                getIndexInfo(INDEX_LEVEL_P, "");
            searchManager =
                index.getSearchManager(theTrexManagedCatalog.getLanguage());
                // set default return attributes
            searchManager.addRequestedAttribute(ATTR_AREA_GUID);
            searchManager.addRequestedAttribute(ATTR_OBJECT_GUID);
            searchManager.addRequestedAttribute(ATTR_OBJECT_DESC);
                // transform query statement
            ArrayList defAttr = new ArrayList();
            for(Iterator it = index.getAttributes().iterator(); it.hasNext(); )
            {
                String name = (String)it.next();
                    // add to default attribute list
                if(!theExcludedAttributes.contains(name) && !name.equalsIgnoreCase(ATTR_OBJECT_DESC))
                    defAttr.add(name);
            }
                // in case of category spec. search query expression was already transformed
            if(query.isCategorySpecificSearch())
            {
                searchManager.setResultFromTo(INTERVAL_BEGIN, CatalogParameters.trexUpperLimit);
                SearchResultDoc firstDocument = resProduct.getFirstDocument();
                for(SearchResultDoc document = resCategory.getFirstDocument();
                    document != null;
                    document = resCategory.getNextDocument())
                {
                    DocAttribute attribute = document.getFirstDocumentAttribute();
                    while(attribute != null)
                    {
                        if(attribute.getAttributeName().equalsIgnoreCase(ATTR_OBJECT_GUID))
                        {
                            QueryEntry entry = new QueryEntry();
                            entry.setRowType(QueryEntry.ROW_TYPE_ATTRIBUTE);
                            entry.setLocation(ATTR_OBJECT_GUID);
                            entry.setContentType(QueryEntry.CONTENT_TYPE_STRING);
                            entry.setValue(attribute.getValue1(), "",
                                           QueryEntry.ATTRIBUTE_OPERATOR_EQ);
                            entry.setTermWeight(10000);
                            searchManager.addQueryEntry(entry);
                            if(firstDocument != document)
                            {
                                    // add the "OR"
                                entry = new QueryEntry();
                                entry.setRowType(QueryEntry.ROW_TYPE_OPERATOR);
                                entry.setValue(QueryEntry.QUERY_OPERATOR_OR, "",
                                               QueryEntry.ATTRIBUTE_OPERATOR_EQ);
                                searchManager.addQueryEntry(entry);
                            }
                                // abort search
                            attribute = null;
                        }
                        else
                        {
                            attribute = document.getNextDocumentAttribute();
                        }
                    }
                }
                QueryEntry entry = new QueryEntry();
                entry.setRowType(QueryEntry.ROW_TYPE_ATTRIBUTE);
                entry.setLocation(ATTR_AREA_GUID);
                entry.setContentType(QueryEntry.CONTENT_TYPE_STRING);
                entry.setValue(query.getParent().getGuid(), "", QueryEntry.ATTRIBUTE_OPERATOR_EQ);
                entry.setTermWeight(10000);
                searchManager.addQueryEntry(entry);
                    // add the "AND"
                entry = new QueryEntry();
                entry.setRowType(QueryEntry.ROW_TYPE_OPERATOR);
                entry.setValue(QueryEntry.QUERY_OPERATOR_AND, "", QueryEntry.ATTRIBUTE_OPERATOR_EQ);
                searchManager.addQueryEntry(entry);

                for(Iterator it = defAttr.iterator(); it.hasNext(); )
                    searchManager.addRequestedAttribute((String)it.next());
            }
            else
            {
                compiler.transformItemQuery(query,
                                            defAttr,
                                            index.getAttributes(),
                                            theTrexManagedCatalog,
                                            index.getSearchEngine(),
                                            searchManager);
                    // add vies to search
                String[] views = theTrexManagedCatalog.getViews();
                if(views != null && views.length > 0)
                    addViewsToSearch(views, searchManager);
            }
            
			// D041871 - fix of Note 846055
			//	set sort order for product search
			//  String sortAttribute = null;
			boolean searchOrder = false;
			//can trigger no such methos exception if the TrexCatalogServerEngine has an older version
			searchOrder=((TrexCatalogServerEngine)theTrexManagedCatalog.getServerEngine()).customerExitSearchProductSort((TrexManagedCatalogBuilder)(theTrexManagedCatalog.getCatalogBuilder()));
			if(searchOrder){
				theStaticLocToLog.debug("TREX Query will be ordered: ASC"); 
			}else{
				theStaticLocToLog.debug("TREX Query will be ordered: DESC");
			}
			if (sortAttribute != null){
				  theStaticLocToLog.debug("TREX Query will be ordered by: " + sortAttribute);
				  if (index.getSearchEngine().equalsIgnoreCase(ENGINE_DRFUZZY)){
					  searchManager.addSortAttribute(sortAttribute,searchOrder);
				  }				
			} 

            // execute query
            resProduct = searchManager.search();
            if(resProduct.getLastError().getCode() != TrexReturn.NO_ERROR)
            {
                String msg = resProduct.getLastError().getCode() + ": " +
                    resProduct.getLastError().getMsg();
                throw new CatalogBuildFailedException(msg);
            }
            else if(theStaticLocToLog.isInfoEnabled())
            {
                theStaticLocToLog.info(ICatalogMessagesKeys.PCAT_IMS_RET_HITS,
                                   new Object[] { new Integer(resProduct.size()) }, null);
            }

                // analyse results
            this.addQuery(resProduct, resCategory, query);
        }
        catch(TrexException ex)
        {
            throw new CatalogBuildFailedException(ex.getLocalizedMessage());
        }
        catch(TrexCatalogException ex)
        {
            throw new CatalogBuildFailedException(ex.getLocalizedMessage());
        }
        catch(CatalogFilterInvalidException ex)
        {
            throw new CatalogBuildFailedException(ex.getLocalizedMessage());
        }
    }

        /**
         * Adds the content of the result set to the given catalog. Through this
         * method the root categories are added to the catalog.
         *
         * @param results  selected results
         * @param catalog  catalog for which the results were selected
         */
    private void addCategories(SearchResult results, Catalog catalog)
        throws TrexCatalogException
    {
        CatalogCategory category;
        ArrayList rootCategories = new ArrayList();

            // first instantiate all categories
        for(SearchResultDoc document = results.getFirstDocument();
            document != null; document = results.getNextDocument())
        {
            String guid = null;
            String name = null;
            String description = null;
            String text = null;
            String thumb = null;
            String image = null;

            boolean areaSpecExists = false;

            for(DocAttribute attribute = document.getFirstDocumentAttribute();
                attribute != null; attribute = document.getNextDocumentAttribute())
            {
                if(attribute.getAttributeName().equalsIgnoreCase(ATTR_AREA_GUID))
                    guid = attribute.getValue1();
                else if(attribute.getAttributeName().equalsIgnoreCase(ATTR_AREA))
                    name = attribute.getValue1();
                else if(attribute.getAttributeName().equalsIgnoreCase(ATTR_DESCRIPTION))
                    description = attribute.getValue1();
                else if(attribute.getAttributeName().equalsIgnoreCase(ATTR_TEXT_0001))
                    text = attribute.getValue1();
                else if(attribute.getAttributeName().equalsIgnoreCase(ATTR_DOC_PC_CRM_THUMB))
                    thumb = attribute.getValue1();
                else if(attribute.getAttributeName().equalsIgnoreCase(ATTR_DOC_PC_CRM_IMAGE))
                    image = attribute.getValue1();
                else if(attribute.getAttributeName().equalsIgnoreCase(ATTR_AREA_SPEZ_EXIST) &&
                        attribute.getValue1().equals(VALUE_X))
                    areaSpecExists = true;
            }

                // build category
            category = catalog.createCategoryInternal(guid);
			//Note 912768
		   if(description!=null){
			   category.setNameInternal(description);
		   }else{
			   ///set it to empty string as it is by default set to
			   //noNameForCategory(guid:" + theGuid + ")
			   //in the CatalogCategory
			   category.setNameInternal("");
		   }
		   if(text!=null){
			   category.setDescriptionInternal(text);
		   }else{
			   //set it to empty string as it is by default set 
			   //noDescriptionForCategory(guid:" + theGuid + ")
			   //in the CatalogCategory
			   category.setDescriptionInternal("");
		   }
		   if(thumb!=null){
			   category.setThumbNailInternal(thumb);
		   }
            CatalogDetail detail = category.createDetailInternal("DOC_PC_CRM_IMAGE");
            detail.setAsStringInternal(image);
            catalog.addCategory(category);
                // add to root categories
            rootCategories.add(category);

                // add also the area specific attributes (performance!)
            if(areaSpecExists)
            {
                TrexManagedCatalogIndexInfo index =
                    theTrexManagedCatalog.getIndexInfo(INDEX_LEVEL_S,category.getGuid());
                this.addAttributes(index, category);
                    // the property is already built now!
                this.setPropertyBuilt(category, PROPERTY_ATTRIBUTE);
            }
        }

            // add the root categories to the catalog
        catalog.setChildCategories(rootCategories);
    }

        /**
         * Adds the content of the result set to the given catalog. Through
         * method all categories are added to the catalog.
         *
         * @param results  selected results
         * @param catalog  catalog for which the results were selected
         */
    private void addAllCategories(SearchResult results, Catalog catalog)
        throws TrexCatalogException
    {
        CatalogCategory category;
        CatalogCategory parent;
        ArrayList rootCategories = new ArrayList();
//JP        HashMap categoryMap = new HashMap();
        ArrayList categoryArray = new ArrayList();

            // first instantiate all categories
        for(SearchResultDoc document = results.getFirstDocument();
            document != null; document = results.getNextDocument())
        {
            String guid = null, description = null, text = null, thumb = null, image = null;
            String[] values = new String[3];

            for(DocAttribute attribute = document.getFirstDocumentAttribute();
                attribute != null; attribute = document.getNextDocumentAttribute())
            {
                if(attribute.getAttributeName().equalsIgnoreCase(ATTR_AREA_GUID))
                    guid = attribute.getValue1();
                else if(attribute.getAttributeName().equalsIgnoreCase(ATTR_DESCRIPTION))
                    description = attribute.getValue1();
                else if(attribute.getAttributeName().equalsIgnoreCase(ATTR_TEXT_0001))
                    text = attribute.getValue1();
                else if(attribute.getAttributeName().equalsIgnoreCase(ATTR_DOC_PC_CRM_THUMB))
                    thumb = attribute.getValue1();
                else if(attribute.getAttributeName().equalsIgnoreCase(ATTR_DOC_PC_CRM_IMAGE))
                    image = attribute.getValue1();
                else if(attribute.getAttributeName().equalsIgnoreCase(ATTR_PARENT_AREA_GUID))
                    values[0] = attribute.getValue1();
                else if(attribute.getAttributeName().equalsIgnoreCase(ATTR_AREA))
                    values[1] = attribute.getValue1();
                else if(attribute.getAttributeName().equalsIgnoreCase(ATTR_AREA_SPEZ_EXIST))
                    values[2] = attribute.getValue1();
            }

                // build category if not already existing
            category = (CatalogCategory)catalog.getCategoryInternal(guid);
            if(category == null)
            {
                category = catalog.createCategoryInternal(guid);
//				Note 912768
				if(description!=null){
					category.setNameInternal(description);
				}else{
					///set it to empty string as it is by default set to
					//noNameForCategory(guid:" + theGuid + ")
					//in the CatalogCategory
					category.setNameInternal("");
				}
				if(text!=null){
					category.setDescriptionInternal(text);
				}else{
					//set it to empty string as it is by default set 
					//noDescriptionForCategory(guid:" + theGuid + ")
					//in the CatalogCategory
					category.setDescriptionInternal("");
				}
				if(thumb!=null){
					category.setThumbNailInternal(thumb);
				}
                CatalogDetail detail = category.createDetailInternal("DOC_PC_CRM_IMAGE");
                detail.setAsStringInternal(image);
                catalog.addCategory(category);
            }
                // for performance reasons store the values of these attributes in a temp map
//JP            categoryMap.put(category, values);
            categoryArray.add(new Object[]{category, values});
        }

            // set parents for all instantiated categories
//JP        for(Iterator it = categoryMap.keySet().iterator(); it.hasNext(); )
        for(Iterator it = categoryArray.iterator(); it.hasNext(); )
        {
//JP            category = (CatalogCategory)it.next();
//JP            String[] values = (String[])categoryMap.get(category);
            Object[] obj2 = (Object[])it.next();
            category = (CatalogCategory)obj2[0];
            String[] values = (String[])obj2[1];
                // set parent category
            if(!values[0].equals(VALUE_ROOT))
            {
                parent = (CatalogCategory)catalog.getCategoryInternal(values[0]);
                category.setParent(parent);
            }
                // add also the area specific attributes if it is a root category
                // and the root categories weren't built yet (performance!)
            else if (!isCategoriesBuilt(catalog))
            {
                rootCategories.add(category);
                if(values[2] != null && values[2].equals(VALUE_X))
                {
                    TrexManagedCatalogIndexInfo index =
                        theTrexManagedCatalog.getIndexInfo( INDEX_LEVEL_S,category.getGuid());
                    this.addAttributes(index, category);
                }
                    // the property is already built now!
                this.setPropertyBuilt(category, PROPERTY_ATTRIBUTE);
            }
        }

            // the root categories were also built
        if (!isCategoriesBuilt(catalog))
        {
            catalog.setChildCategories(rootCategories);
            this.setPropertyBuilt(catalog, PROPERTY_CATEGORY);
        }
    }

        /**
         * Adds the content of the result set to the given category. Through this
         * mehtod the children of the given category are set.
         *
         * @param results  selected results
         * @param catalog  category for which the children were selected
         */
    private void addCategories(SearchResult results, CatalogCategory category)
        throws TrexCatalogException
    {
        ArrayList children = new ArrayList();

        for(SearchResultDoc document = results.getFirstDocument();
            document != null; document = results.getNextDocument())
        {
            String guid = null, description = null, text = null, thumb = null, name = null, image = null;
            boolean areaSpecExists = false;

            for(DocAttribute attribute = document.getFirstDocumentAttribute();
                attribute != null; attribute = document.getNextDocumentAttribute())
            {
                if(attribute.getAttributeName().equalsIgnoreCase(ATTR_AREA_GUID))
                    guid = attribute.getValue1();
                else if(attribute.getAttributeName().equalsIgnoreCase(ATTR_DESCRIPTION))
                    description = attribute.getValue1();
                else if(attribute.getAttributeName().equalsIgnoreCase(ATTR_TEXT_0001))
                    text = attribute.getValue1();
                else if(attribute.getAttributeName().equalsIgnoreCase(ATTR_DOC_PC_CRM_THUMB))
                    thumb = attribute.getValue1();
                else if(attribute.getAttributeName().equalsIgnoreCase(ATTR_DOC_PC_CRM_IMAGE))
                    image = attribute.getValue1();
                else if(attribute.getAttributeName().equalsIgnoreCase(ATTR_AREA))
                    name = attribute.getValue1();
                else if(attribute.getAttributeName().equalsIgnoreCase(ATTR_AREA_SPEZ_EXIST) &&
                        attribute.getValue1().equals(VALUE_X))
                    areaSpecExists = true;
            }

                // get child
            CatalogCategory child =
                (CatalogCategory)theTrexManagedCatalog.getCategoryInternal(guid);
                // build category if not already done
            if(child == null)
            {
                    // only one thread is allowed to modify the catalog instance
                synchronized(theTrexManagedCatalog)
                {
                        // build category
                    child = theTrexManagedCatalog.createCategoryInternal(guid);
					//Note 912768
					if(description!=null){
						child.setNameInternal(description);
					}else{
						///set it to empty string as it is by default set to
						//noNameForCategory(guid:" + theGuid + ")
						//in the CatalogCategory
						child.setNameInternal("");
					}
					if(text!=null){
						child.setDescriptionInternal(text);
					}else{
						//set it to empty string as it is by default set to
						//noDescriptionForCategory(guid:" + theGuid + ")
						//in the CatalogCategory
						child.setDescriptionInternal("");
					}
					if(thumb!=null){
						child.setThumbNailInternal(thumb);
					}				
                    CatalogDetail detail = category.createDetailInternal("DOC_PC_CRM_IMAGE");
                    detail.setAsStringInternal(image);
                    child.setParent(category);
                    theTrexManagedCatalog.addCategory(child);
                }
            }
                // fill children collection
            children.add(child);

                // add also the area specific attributes (performance!)
            synchronized(child)
            {
                if(areaSpecExists)
                {
                    TrexManagedCatalogIndexInfo index =
                        theTrexManagedCatalog.getIndexInfo( INDEX_LEVEL_S,child.getGuid());
                    this.addAttributes(index, child);
                }
                    // the property is already built now!
                this.setPropertyBuilt(child, PROPERTY_ATTRIBUTE);
            }
        }

            // set children categories
        category.setChildCategories(children);
    }

        /**
         * Adds the content of the result sets to the given category. Through this
         * method the items for the given category are set.
         *
         * @param resProducts  selected results from the 'P' index
         * @param resCategory  selected results from the 'S' index
         * @param category     category for which the results were selected
         */
    private void addItems(SearchResult resProduct, SearchResult resCategory,
                          CatalogCategory category) throws TrexCatalogException
    {

        HashMap builtItems = new HashMap();
        HashMap attrValues = new HashMap();

            // first analyse the results of the P index
        for(SearchResultDoc document = resProduct.getFirstDocument();
            document != null; document = resProduct.getNextDocument())
        {
                // build item
            CatalogItem item = category.createItemInternal();
            String objectGuid = null;
            String areaGuid = null;
            String itemGuid = null;
            attrValues.clear();

            for(DocAttribute attribute = document.getFirstDocumentAttribute();
                attribute != null; attribute = document.getNextDocumentAttribute())
            {
                String attrName = attribute.getAttributeName();
                    // store guid of item
                if(attrName.equalsIgnoreCase(ATTR_OBJECT_GUID))
                    objectGuid = attribute.getValue1();
                    // store area guid of item
                if(attrName.equalsIgnoreCase(ATTR_AREA_GUID))
                    areaGuid = attribute.getValue1();
                    // set name of item
                if(attrName.equalsIgnoreCase(ATTR_OBJECT_DESC))
                    item.setNameInternal(attribute.getValue1());
                    // set all other attributes
                if (!attrName.equalsIgnoreCase(ATTR_AREA_GUID) &&
                    !attrName.equalsIgnoreCase(ATTR_OBJECT_GUID) &&
                    !attrName.equalsIgnoreCase(ATTR_AREA)        )
                { //TODO<<<<<<<<<<<<<<<<<< else if (!colName.equals(ATTR_AREA))        // if multivalued attribute - value was already created
                    CatalogAttributeValue attrValue =
                        (CatalogAttributeValue)attrValues.get(attrName);
                    if(attrValue != null) {
                        attrValue.addAsStringInternal(attribute.getValue1());
                    }
                    else
                    {
                        attrValue = item.createAttributeValueInternal(attrName);
                        attrValue.setAsStringInternal(attribute.getValue1());
                        attrValues.put(attrName, attrValue);
                    }
                }
                if (attribute.getAttributeName().equalsIgnoreCase(ATTR_ITEM_GUID)) {
                   itemGuid = attribute.getValue1();
                }
            }
                 
                // set guid of item
            item.setGuidInternal(itemGuid);
                // set product guid of item
            item.setProductGuidInternal(objectGuid);
                // store item in map
            builtItems.put(objectGuid, new Object[] { item, null });
        }

            // if no area specific attributes were found then exit
        if (resCategory == null)
            return;

            // unfortunately we have first to find the document for each product
        for(SearchResultDoc document = resCategory.getFirstDocument();
            document != null; document = resCategory.getNextDocument())
        {
            DocAttribute attribute = document.getFirstDocumentAttribute();
            while(attribute != null)
            {
                if(attribute.getAttributeName().equalsIgnoreCase(ATTR_OBJECT_GUID)) {
                    Object[] values = (Object[])builtItems.get(attribute.getValue1());
                    values[1] = document;
                    attribute = null;
                }
                else
                    attribute = document.getNextDocumentAttribute();
            }
        }

            // now analyse the results of the S index
        for(Iterator it = builtItems.keySet().iterator(); it.hasNext(); )
        {
            String guid = (String)it.next();
            Object[] values = (Object[])builtItems.get(guid);
            CatalogItem item = (CatalogItem)values[0];
            SearchResultDoc document = (SearchResultDoc)values[1];
            attrValues.clear();

            for(DocAttribute attribute = document.getFirstDocumentAttribute();
                attribute != null; attribute = document.getNextDocumentAttribute())
            {
                    // set all attributes
                if(!attribute.getAttributeName().equalsIgnoreCase(ATTR_OBJECT_GUID))
                {
                        // if multivalued attribute - value was already created
                    CatalogAttributeValue attrValue =
                        (CatalogAttributeValue)attrValues.get(attribute.getAttributeName());
                    if(attrValue != null)
                    {
                        attrValue.addAsStringInternal(attribute.getValue1());
                    }
                    else
                    {
                        attrValue = item.createAttributeValueInternal(attribute.getAttributeName());
                        attrValue.setAsStringInternal(attribute.getValue1());
                        attrValues.put(attribute.getAttributeName(), attrValue);
                    }
                }
            }
        }
    }

        /**
         * Adds the content of the result set to the given catalog. Through this
         * method the detail information of the given catalog are set.
         *
         * @param results  selected results
         * @param catalog  catalog for which the results were selected
         * @exception TrexCatalogException  error from the Trex catalog implementation
         */
    private void addDetails(ResultData results, Catalog catalog)
        throws TrexCatalogException
    {
            // if more than one entries were found - raise error
        if(results.getNumRows() > 1)
        {
            String msg = catalog.getMessageResources().getMessage(
                ICatalogMessagesKeys.PCAT_IMS_RET_2MANYPRICINFOS);
            throw new TrexCatalogException(msg);
        }

            // get the first row if available
        if(!results.first())
            return;

            // create the details
        CatalogDetail detail;
        String value;

        value = results.getString("ATTRNAME_PRICE");
        if(value!= null && value.length() != 0)
        {
            detail = catalog.createDetailInternal();
            detail.setNameInternal(IDetail.ATTRNAME_PRICE); // this is a common attribute
            detail.setAsStringInternal(value);
        }

        value = results.getString("ATTRNAME_QUANTITY");
        if(value!= null && value.length() != 0)
        {
            detail = catalog.createDetailInternal();
            detail.setNameInternal("ATTRNAME_QUANTITY");
            detail.setAsStringInternal(value);
        }

        value = results.getString("ATTRNAME_CURRENCY");
        if(value!= null && value.length() != 0)
        {
            detail = catalog.createDetailInternal();
            detail.setNameInternal(IDetail.ATTRNAME_CURRENCY); // this is a common attribute
            detail.setAsStringInternal(value);
        }

        value = results.getString("ATTRNAME_UOM");
        if(value!= null && value.length() != 0)
        {
            detail = catalog.createDetailInternal();
            detail.setNameInternal("ATTRNAME_UOM");
            detail.setAsStringInternal(value);
        }

        value = results.getString("ATTRNAME_SCALETYPE");
        if(value!= null && value.length() != 0)
        {
            detail = catalog.createDetailInternal();
            detail.setNameInternal("ATTRNAME_SCALETYPE");
            detail.setAsStringInternal(value);
        }
    }

        /**
         * Adds the content of the result set to the given category. Through this
         * method the detail information of the given category are set.
         *
         * @param results   selected results
         * @param category  category for which the results were selected
         */
    private void addDetails(SearchResult results, CatalogCategory category)
        throws TrexCatalogException
    {
        HashMap builtDetails = new  HashMap();

        for(SearchResultDoc document = results.getFirstDocument();
            document != null; document = results.getNextDocument())
        {
            builtDetails.clear();

            for(DocAttribute attribute = document.getFirstDocumentAttribute();
                attribute != null; attribute = document.getNextDocumentAttribute())
            {        // if multivalued attribute - detail was already created
                CatalogDetail detail =
                    (CatalogDetail)builtDetails.get(attribute.getAttributeName());
                if(detail != null)
                {
                    detail.addAsStringInternal(attribute.getValue1());
                }
                else
                {
                    detail = category.createDetailInternal();
                    detail.setNameInternal(attribute.getAttributeName());
                    detail.setAsStringInternal(attribute.getValue1());
                    builtDetails.put(attribute.getAttributeName(), detail);
                }
            }
        }
    }

        /**
         * Adds the attributes names to the given component.
         *
         * @param info     info object about the index
         * @param component  component for which the names were selected
         */
    private void addAttributes(TrexManagedCatalogIndexInfo info,
                               CatalogCompositeComponent component)
        throws TrexCatalogException
    {
            // set name of all attributes
        for(Iterator iter = info.getAttributes().iterator(); iter.hasNext(); )
        {
            String name = (String)iter.next();
            if(theExcludedAttributes.contains(name))
                continue;
                // create new attribute
            CatalogAttribute attribute = component.createAttributeInternal();
                // set name of attribute
            attribute.setNameInternal(name);
                // the property (details of the attribute) is already built now!
            this.setPropertyBuilt(attribute, PROPERTY_DETAIL);
        }
    }

        /**
         * Adds the content of the result sets to the given query.
         *
         * @param resProducts  selected results from the 'P' index
         * @param resCategory  selected results from the 'S' index
         * @param query        query for which the results were selected
         */
    private void addQuery(SearchResult resProduct, SearchResult resCategory,
                          CatalogQuery query) throws TrexCatalogException
    {
        HashMap builtItems = new HashMap();
        HashMap attrValues = new HashMap();

            // first analyse the results of the P index
        for(SearchResultDoc document = resProduct.getFirstDocument();
            document != null; document = resProduct.getNextDocument())
        {
                // first search the guids
            String areaGuid = null;
            String objectGuid= null;
            // changes made to resolve the issues that occur after changelist
            // Note 897176
            String itemGuid = null;
            DocAttribute attribute = document.getFirstDocumentAttribute();
            while(attribute != null)
            {
                if(attribute.getAttributeName().equalsIgnoreCase(ATTR_AREA_GUID))
                    areaGuid = attribute.getValue1();
                if(attribute.getAttributeName().equalsIgnoreCase(ATTR_OBJECT_GUID))
                    objectGuid = attribute.getValue1();
                    // exit loop if both were found
                if(attribute.getAttributeName().equalsIgnoreCase(ATTR_ITEM_GUID))
                    itemGuid = attribute.getValue1();
                if(areaGuid != null && objectGuid != null && itemGuid != null)
                    attribute = null;
                else
                    attribute = document.getNextDocumentAttribute();
            }

                // create the item
            CatalogQueryItem item =
                query.createQueryItemInternal(itemGuid, areaGuid);
            item.setProductGuidInternal(objectGuid);
            builtItems.put(objectGuid, new Object[] { item, null });
            attrValues.clear();

                // now set the attribute values
            for(attribute = document.getFirstDocumentAttribute();
                attribute != null; attribute = document.getNextDocumentAttribute())
            {
                String attrName = attribute.getAttributeName();
                    // set name of item
                if(attrName.equalsIgnoreCase(ATTR_OBJECT_DESC))
                    item.setNameInternal(attribute.getValue1());
                    // set all other attributes
                if( !attrName.equalsIgnoreCase(ATTR_AREA_GUID) && !attrName.equalsIgnoreCase(ATTR_OBJECT_GUID))
                { // TODO: durch else if ersetzen, wegen Beschreibung
                        // if multivalued attribute - value was already created
                    CatalogAttributeValue attrValue =
                        (CatalogAttributeValue)attrValues.get(attrName);
                    if(attrValue != null)
                    {
                        attrValue.addAsStringInternal(attribute.getValue1());
                    }
                    else
                    {
                        attrValue = item.createAttributeValueInternal(attrName);
                        attrValue.setAsStringInternal(attribute.getValue1());
                        attrValues.put(attrName, attrValue);
                    }
                }
            }
        }

            // if no area specific attributes were found then exit
        if (resCategory == null)
            return;

            // unfortunately we have first to find the document for each product
        for(SearchResultDoc document = resCategory.getFirstDocument();
            document != null; document = resCategory.getNextDocument())
        {
            DocAttribute attribute = document.getFirstDocumentAttribute();
            while(attribute != null) {
                if(attribute.getAttributeName().equalsIgnoreCase(ATTR_OBJECT_GUID))
                {
                    Object[] values = (Object[])builtItems.get(attribute.getAttributeName());
                    values[1] = document;
                    attribute = null;
                }
                else
                    attribute = document.getNextDocumentAttribute();
            }
        }

            // now analyse the results of the S index
        for(Iterator it = builtItems.keySet().iterator(); it.hasNext(); )
        {
            String guid = (String)it.next();
            Object[] values = (Object[])builtItems.get(guid);
            CatalogItem item = (CatalogItem)values[0];
            SearchResultDoc document = (SearchResultDoc)values[1];
            attrValues.clear();

            for(DocAttribute attribute = document.getFirstDocumentAttribute();
                attribute != null; attribute = document.getNextDocumentAttribute())
            {        // set all attributes
                if(!attribute.getAttributeName().equalsIgnoreCase(ATTR_OBJECT_GUID))
                {
                        // if multivalued attribute - value was already created
                    CatalogAttributeValue attrValue =
                        (CatalogAttributeValue)attrValues.get(attribute.getAttributeName());
                    if(attrValue != null) {
                        attrValue.addAsStringInternal(attribute.getValue1());
                    }
                    else
                    {
                        attrValue = item.createAttributeValueInternal(attribute.getAttributeName());
                        attrValue.setAsStringInternal(attribute.getValue1());
                        attrValues.put(attribute.getAttributeName(), attrValue);
                    }
                }
            }
        }
    }

        /**
         * Adds the content of the result sets to the given query.
         *
         * @param results      selected results from the 'A' index
         * @param query        query for which the results were selected
         */
    private void addQuery(SearchResult results, CatalogQuery query)
        throws TrexCatalogException
    {
        for(SearchResultDoc document = results.getFirstDocument();
            document != null; document = results.getNextDocument())
        {        // first search the guid
            String areaGuid = null;
            DocAttribute attribute = document.getFirstDocumentAttribute();
            while(attribute != null)
            {
                if(attribute.getAttributeName().equalsIgnoreCase(ATTR_AREA_GUID))
                {
                    areaGuid = attribute.getValue1();
                    attribute = null;
                }
                else
                    attribute = document.getNextDocumentAttribute();
            }

                // add category to query
            CatalogCategory category =
                theTrexManagedCatalog.getCategoryInternal(areaGuid);
            query.addChildCategoryInternal(category);

                // check if details were alread build
            if(this.isDetailsBuilt(category))
                continue;

                // otherwise build the details
            synchronized(category)
            {
                if(!isDetailsBuilt(category))
                {
                    HashMap builtDetails = new HashMap();

                        // iterate over columns
                    for(attribute = document.getFirstDocumentAttribute();
                        attribute != null; attribute = document.getNextDocumentAttribute())
                    {
                        if(!attribute.getAttributeName().equalsIgnoreCase(ATTR_AREA_GUID))
                        {
                                // if multivalued attribute - detail was already created
                            CatalogDetail detail =
                                (CatalogDetail)builtDetails.get(attribute.getAttributeName());
                            if(detail != null) {
                                detail.addAsStringInternal(attribute.getValue1());
                            }
                            else
                            {
                                detail = category.createDetailInternal();
                                detail.setNameInternal(attribute.getAttributeName());
                                detail.setAsStringInternal(attribute.getValue1());
                                builtDetails.put(attribute.getAttributeName(), detail);
                            }
                        }
                    }
                    this.setPropertyBuilt(category, PROPERTY_DETAIL);
                }
            }
        }
    }

        /**
         * Adds the query statement for the views to passed SearchManager.
         *
         * @param views array of views id
         * @param searchManager the searchManager to be used
         * @exception TrexException error from the trex server
         */
    static void addViewsToSearch(String[] views, SearchManager searchManager)
        throws TrexException
    {
        QueryEntry entry;
        
        
        for(int i=0; i < views.length; i++)
        {
            entry = new QueryEntry();
            entry.setRowType(QueryEntry.ROW_TYPE_ATTRIBUTE);
            entry.setLocation(ATTR_VIEWS_ID);
            entry.setContentType(QueryEntry.CONTENT_TYPE_STRING);
            entry.setValue(views[i], "", QueryEntry.ATTRIBUTE_OPERATOR_EQ);
            entry.setTermWeight(10000);
            searchManager.addQueryEntry(entry);
            if(i > 0) {
                // add the "OR"
                entry = new QueryEntry();
                entry.setRowType(QueryEntry.ROW_TYPE_OPERATOR);
                entry.setValue(QueryEntry.QUERY_OPERATOR_OR, "", QueryEntry.ATTRIBUTE_OPERATOR_EQ);
                searchManager.addQueryEntry(entry);
            }
        }
        // add the "AND"
        entry = new QueryEntry();
        entry.setRowType(QueryEntry.ROW_TYPE_OPERATOR);
        entry.setValue(QueryEntry.QUERY_OPERATOR_AND, "", QueryEntry.ATTRIBUTE_OPERATOR_EQ);
        searchManager.addQueryEntry(entry);
        
        
    }
}
