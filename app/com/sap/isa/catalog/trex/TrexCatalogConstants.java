/*****************************************************************************
  Copyright (c) 2000, SAPMarkets Europe GmbH, Germany, All rights reserved.
  Created:      20 Dec 2001

  $Id: //sap/ESALES_base/30_SP_COR/src/_pcatAPI/java/com/sap/isa/catalog/trex/TrexCatalogConstants.java#3 $
  $Revision: #3 $
  $Change: 139408 $
  $DateTime: 2003/07/18 19:32:57 $
*****************************************************************************/
package com.sap.isa.catalog.trex;

/**
 * This interface contains the general constants which are used for the Trex
 * catalog.
 * @version     1.0
 */
interface TrexCatalogConstants 
{
        /**
         * Constant for the 'A' index.
         */
    public final static String INDEX_LEVEL_A = "A";

        /**
         * Constant for the 'P' index.
         */
    public final static String INDEX_LEVEL_P = "P";

        /**
         * Constant for the 'S' index.
         */
    public final static String INDEX_LEVEL_S = "S";

        /**
         * Default value for the beginning of the search interval.
         */
    public final static int INTERVAL_BEGIN = 1;

        /**
         * Default value for the ending of the search interval.
         */
    public final static int INTERVAL_END = 10000;

        /**
         * Constant for the search engine DRFUZZY.
         */
    public final static String ENGINE_DRFUZZY = "DRFUZZY";

        /**
         * Constant for the search engine VERITY.
         */
    public final static String ENGINE_VERITY = "VERITY";

        /**
         * Constant for the AREA attribute.
         */
    public final static String ATTR_AREA = "AREA";

        /**
         * Constant for the AREA_GUID attribute.
         */
    public final static String ATTR_AREA_GUID = "AREA_GUID";

        /**
         * Constant for the AREA_SPECIFIC_ATTR_EXIST attribute.
         */
    public final static String ATTR_AREA_SPEZ_EXIST = "AREA_SPECIFIC_ATTR_EXIST";

        /**
         * Constant for the AREA_TYPE attribute.
         */
    public final static String ATTR_AREA_TYPE = "AREA_TYPE";

        /**
         * Constant for the CATALOG attribute.
         */
    public final static String ATTR_CATALOG = "CATALOG";

        /**
         * Constant for the CATALOG_GUID attribute.
         */
    public final static String ATTR_CATALOG_GUID = "CATALOG_GUID";

        /**
         * Constant for the DESCRIPTION attribute.
         */
    public final static String ATTR_DESCRIPTION = "DESCRIPTION";

        /**
         * Constant for the DOC_PC_CRM_IMAGE attribute.
         */
    public final static String ATTR_DOC_PC_CRM_IMAGE = "DOC_PC_CRM_IMAGE";

        /**
         * Constant for the DOC_PC_CRM_THUMB attribute.
         */
    public final static String ATTR_DOC_PC_CRM_THUMB = "DOC_PC_CRM_THUMB";

        /**
         * Constant for the ITEM_GUID attribute.
         */
    public final static String ATTR_ITEM_GUID = "ITEM_GUID";

        /**
         * Constant for the OBJECT_DESCRIPTION attribute.
         */
    public final static String ATTR_OBJECT_DESC = "OBJECT_DESCRIPTION";

        /**
         * Constant for the OBJECT_GUID attribute.
         */
    public final static String ATTR_OBJECT_GUID = "OBJECT_GUID";

        /**
         * Constant for the OBJECT_ID attribute.
         */
    public final static String ATTR_OBJECT_ID = "OBJECT_ID";

        /**
         * Constant for the PARENT_AREA attribute.
         */
    public final static String ATTR_PARENT_AREA = "PARENT_AREA";

        /**
         * Constant for the PARENT_AREA_GUID attribute.
         */
    public final static String ATTR_PARENT_AREA_GUID = "PARENT_AREA_GUID";

        /**
         * Constant for the POS_NR attribute.
         */
    public final static String ATTR_POS_NR  = "POS_NR";

        /**
         * Constant for the ROLE attribute.
         */
    public final static String ATTR_ROLE = "ROLE";

        /**
         * Constant for the SUBAREAS_EXIST attribute.
         */
    public final static String ATTR_SUBAREA_EXISTS = "SUBAREAS_EXIST";

        /**
         * Constant for the TEXT_0001 attribute.
         */
    public final static String ATTR_TEXT_0001 = "TEXT_0001";

        /**
         * Constant for the VIEWS_GUID attribute.
         */
    public final static String ATTR_VIEWS_GUID = "VIEWS_GUID";

        /**
         * Constant for the VIEW_ID attribute.
         */
    public final static String ATTR_VIEWS_ID = "VIEWS_ID";
    
        /**
         * Constant for the R3_MATNR attribute.
         */
    public final static String ATTR_R3_MATNR = "R3_MATNR";

        /**
         * Constant for the TIMESTAMP attribute.
         */
    public final static String ATTR_TIMESTAMP = "TIMESTAMP";

        /**
         * Constant for selecting all values of an attribute ('*').
         */
    public final static String VALUE_ALL = "*";

        /**
         * Constant for selecting the accessories. This value is stored in the
         * ROLE attribute in the 'P' index if the product is a accessory product.
         */
    public final static String VALUE_B = "B";

        /**
         * Constant for an empty attribute.
         */
    public final static String VALUE_EMPTY = "";

        /**
         * Constant for selecting the root categories. This value have to be used
         * in combination with the PARENT_AREA_GUID attribute. If this attribute
         * contains this constant it's a root category.
         */
    public final static String VALUE_ROOT = "00000000000000000000000000000000";

        /**
         * Constant for the value 'X' in an attribute. This value represents
         * the TRUE value in an attribute of an index.
         */
    public final static String VALUE_X = "X";
}
