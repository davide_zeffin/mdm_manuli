/*****************************************************************************
  Copyright (c) 2000, SAPMarkets Europe GmbH, Germany, All rights reserved.
  Created:      06 March 2002

  $Id:$
  $Revision:$
  $Change: $
  $DateTime:$
*****************************************************************************/
package com.sap.isa.catalog.trex;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import com.sap.isa.catalog.ICatalogMessagesKeys;
import com.sap.isa.core.logging.IsaLocation;
import com.sapportals.trex.TrexConst;
import com.sapportals.trex.TrexException;
import com.sapportals.trex.core.AttributeDefinition;
import com.sapportals.trex.core.SearchManager;
import com.sapportals.trex.core.TrexFactory;
import com.sapportals.trex.core.admin.AdminIndex;

/**
 * This class encapsulate all information about an index of an Trex catalog.
 * @version     1.0
 */
class TrexManagedCatalogIndexInfo implements Serializable
{
        /**
         * The class is serializable. All attributes are serializable with the
         * default implementation.
         */

        /**
         * Constant for the property POS_NR. This property defines the order
         * of the attributes as it was defined in the CRM system.
         */
    static String PROP_POS_NR = "POS_NR";

        /**
         * Constant for the property DESCRIPTION. This property defines the language
         * dependant text of the attribute.
         */
    static String PROP_DESCRIPTION = "DESCRIPTION";

        /**
         * Constant for the property GUID. This property defines the guid of the
         * attribute as it was defined in the CRM system.
         */
    static String PROP_GUID = "GUID";

        /**
         * Constant for the URL for the Trex server
         */
    static String URL = "/TrexHttpServer/TrexHttpServer.dll?";

       // the logging instance
    private static IsaLocation theStaticLocToLog =
        IsaLocation.getInstance(TrexManagedCatalogIndexInfo.class.getName());
    private String theIndexGuid;
    private String theIndexType;
    private String theCategoryID;
    private List theAttributes;
    private String theSearchEngine;

        /**
         * Creates a new instance of a index.
         *
         * @param aindexType	index level (A, P, S)
         * @param aCategoryID       name of area (in case of 'S' index)
         * @param aIndexGuid  	unique identifier of index on Trex
         */
    TrexManagedCatalogIndexInfo(String aIndexGUID,
                                String aIndexType,
                                String aCategoryID,
                                AdminIndex aAdminIndex)
    {
        theIndexGuid = aIndexGUID;
        theIndexType = aIndexType;
        theCategoryID = aCategoryID;
        theAttributes  = new ArrayList();
        theSearchEngine = aAdminIndex.getTMEngineName();
//      ArrayList aListOfAttributes = aAdminIndex.getDocAttrList();            //JP: this is Trex5.0 SP4
//      Iterator anIter = aListOfAttributes.iterator();                        //JP: this is Trex5.0 SP4
//      while ( anIter.hasNext())                                              //JP: this is Trex5.0 SP4 
//      {                                                                      //JP: this is Trex5.0 SP4
//          AdminDocAttribute anDocAttrib = (AdminDocAttribute) anIter.next(); //JP: this is Trex5.0 SP4
//          theAttributes.add(anDocAttrib.getName().toUpperCase());            //JP: this is Trex5.0 SP4
//      } // end of while ()                                                   //JP: this is Trex5.0 SP4

        AttributeDefinition attrDef = aAdminIndex.getFirstDocumentAttribute(); //JP: this is Trex5.0 SP5
        while(attrDef != null) {                                              //JP: this is Trex5.0 SP5
            String attrName = attrDef.getName().toUpperCase();                 //JP: this is Trex5.0 SP5
//          theAttributes.add(attrName, attrDef.getType()); //JP: this is Trex5.0 SP5
            theAttributes.add(attrName);                                       //JP: this is Trex5.0 SP5
            attrDef = aAdminIndex.getNextDocumentAttribute();                  //JP: this is Trex5.0 SP5
        }                                                                      //JP: this is Trex5.0 SP5
            // log the creation
        if(theStaticLocToLog.isInfoEnabled())
        {
            theStaticLocToLog.info( ICatalogMessagesKeys.PCAT_IMS_INFO_INDEX,
                                new Object[] {  theIndexType,
                                                "",
                                                theIndexGuid,
                                                theCategoryID },
                                null);
        }

    }

        /**
         * Get the search manager assoiated with the corresponding Index we are 
         * accessing.
         *
         * @return a <code>SearchManager</code> associated with this index.
         * @exception TrexException if an error occurs
         */
    SearchManager getSearchManager(String language)
        throws TrexException
    {
        SearchManager searchManager =
            TrexFactory.getSearchManager();

        String searchEngine = TrexConst.SEARCH_ENGINE_DRFUZZY;
        if(theSearchEngine.equals(TrexCatalogConstants.ENGINE_VERITY))
            searchEngine = TrexConst.SEARCH_ENGINE_VERITY;
        searchManager.addIndexId(theIndexGuid,language);
        return searchManager;
    }

        /**
         * Generates a unique identifier for the index.
         *
         * @param indexType  name of index level
         * @param categoryID       name of area
         * @return a <code>String</code> value as key for the Index
         */
    static String generateKey(String indexType, String categoryId)
    {
        return indexType + categoryId;
    }

        /**
         * Generates a unique identifier for the index. The key is built
         * through concatenation of attrLevel and area.
         *
         * @return  unique key for index
         */
    String generateKey()
    {
        return generateKey(getIndexType(), getCategoryID());
    }

        /**
         * Adds an attribute to the list of attributes.
         *
         * @param attribute  name of the attribute to be added
         */
    void addAttribute(String attribute)
    {
        theAttributes.add(attribute);
    }

        /**
         * Returns <code>true</code> if the index contains the requested attribute.
         *
         * @param name name of the requested attribute
         * @return boolean that indicates if the index contains the given attribute
         */
    boolean hasAttribute(String name)
    {
        return theAttributes.contains(name);
    }

        /**
         * Returns the index type.
         *
         * @return  name of index level (possible values A, P, S)
         */
    String getIndexType()
    {
        return theIndexType;
    }

        /**
         * Returns the list of attribute names of the index.
         *
         * @return  list of attribute names
         */
    List getAttributes()
    {
        return theAttributes;
    }

        /**
         * Returns the category id.
         *
         * @return  category id
         */
    String getCategoryID()
    {
        return theCategoryID;
    }

        /**
         * Returns the store id.
         *
         * @return  store id
         */
    String getStoreId()
    {
        return theIndexGuid;
    }

        /**
         * Returns the search engine name.
         *
         * @return the name of the search engine
         */
    String getSearchEngine()
    {
        return theSearchEngine;
    }

}
