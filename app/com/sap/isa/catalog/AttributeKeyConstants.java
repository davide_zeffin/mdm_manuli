package com.sap.isa.catalog;

/**
 * Title:        Internet Sales, dev branch
 * Description:
 * Copyright:    Copyright (c) 2001
 * Company:
 * @author
 * @version 1.0
 */

public class AttributeKeyConstants {
    /**
     * Catalog area image (big picture)
     */
    public final static String AREA_IMAGE = "areaImage";
    /**
     * Area long description / long text
     */
    public final static String AREA_LONG_DESCRIPTION = "areaLongDescription";
    /**
     * Catalog area Thumbnail (small picture)
     */
    public final static String AREA_THUMBNAIL = "areaThumbnail";
    
    /**
     * Eye Catcher Text for bundle component.
     */
    public final static String BUNDLE_CPT_EYE_CATCHER = "BUNDLE_CPT_EYE_CATCHER";
    
    /**
     * Price Eye Catcher Text
     */
    public final static String PRICE_EYE_CATCHER = "PRICE_EYE_CATCHER";
    
    /**
     * List of Campaign messages
     */
    public final static String CAMPAIGN_MESSAGE_LIST = "campaignMsgList";
    /**
     * Price currency
     */
    public final static String CURRENCY = "currency";
    
	/**
	 * sales components of a package. Old!!! Can be deleted later
     * @deprecated
	 */
	public final static String DEFAULT_SUBCOMP_FOR_SLSCP = "DEFAULT_SUBCOMP_FOR_SLSCP";
    
    /**
     * sub components of a sales package, combined rate plan, product with dependent products.
     */
    public final static String DEFAULT_SUBCOMP_OF_PCKAGE = "DEFAULT_SUBCOMP_OF_PCKAGE";
    
	/**
	 * hierarchy of sales components of a package.
	 */
	public final static String DFL_SUBCOMP_PARENT_IDX = "DFL_SUBCOMP_PARENT_IDX";
    
	/**
	 * pricing relevant attributes of sales components of a package.
	 */
	public final static String DFL_SUBCOMP_PRC_ATTR_VALU = "DFL_SUBCOMP_PRC_ATTR_VALU";
    /**
     * The product division (might be used for pricing)
     */
    public final static String DIVISION = "division";
    /**
     * Product global Id. Is used for OCI in SRM Integrated Catalog
     */
    public final static String GLOBAL_ID = "GlobalId";

    /**
     * New attribute for additional attribute needed by IPC
     */
    public final static String IPC_PRICE_ATTR = "IPC_PRICING_ATT";
    
    /**
     * Indicates, that this item is classified as a Dependent Components Product.
     * Value: "X" or "".
     */
    public final static String IS_DEPCMP_PROD = "IS_DEPCMP_PROD";
    
    /**
     * Indicates, that this item has accessories.
     * Value: "X" or "".
     */
    public final static String IS_ITEM_WITH_ACC = "IS_ITEM_WITH_ACC";

    /**
     * Indicates, that this item is just a main item, that means item is directly part of
     * an area.
     * Value: "X" or "".
     */
    public final static String IS_MAIN_ITEM = "IS_MAIN_ITEM";

    /** 
     * Constants for attribute PRODUCT_ROLE  
     * Value: "C" = Combined Rate Plan, "R" = Rate Plan, "S" = Sales Package 
     */
    public final static String IS_PR_COMBINEDRATEPLAN = "C";
    public final static String IS_PR_RATEPLAN = "R";
    public final static String IS_PR_SALESPACKAGE = "S";

    /**
     * Indicates, that this item is classified as Rate Plan Combinations Product (Kombi-Tarif).
     * Value: "X" or "".
     */
    public final static String IS_RATEPLAN_PROD = "IS_RATEPLAN_PROD";

    /**
     * Indicates, that the item is classified as Sales Components Product.
     * Value: "X" or "".
     */
    public final static String IS_SLSCMP_PROD = "IS_SLSCMP_PROD";

    /**
     * Indicates, that the item can be exploded against the solution configurator.
     * Value: "X" or "" 
     */
    public final static String IS_SOLCONF_EXPLO = "IS_SOLCONF_EXPLO";
    
    /**
     * Indicates, that this item is an accessory.
     * Value: "X" or "".
     */
    public final static String IS_SUBCOMP_OF_ACCESS = "IS_SUBCOMP_OF_ACCESS";  

    /**
     * Indicates, that this item is a sub component (Package Component) of a 
     * Rate Plan Combinations Product.
     * Value: "X" or "".
     */
    public final static String IS_SUBCOMP_OF_RATEPLAN = "IS_SUBCOMP_OF_RATEPLAN";

    /**
     * Indicates, that this item is a sub component (Package Component) of a
     * Dependent Components Product.
     * Value: "X" or "".
     */
    public final static String IS_SUBCOMP_OF_DEPCMP = "IS_SUBCOMP_OF_DEPCMP";

    /**
     * Indicates, that this item is a sub component (Package Component) of a 
     * Sales Components Product.
     * Value: "X" or "".
     */
    public final static String IS_SUBCOMP_OF_SLSCMP = "IS_SUBCOMP_OF_SLSCMP";
    
	/**
	 * pricing attributes of a sales component of a sales package.
	 */
	public final static String PARENT_PRC_PROD = "PARENT_PRC_PROD";
    
	/**
	 * constant for parent index of a package of sales comonents.
	 */
	public final static String IST_PARENT_IDX = "0";
	public final static String PROV_PRICE_LEVEL = "PROV_PRICE_LEVEL";  // former IST_SCORE
	public final static String PAR_PAR_PRC_PROD = "PAR_PAR_PRC_PROD";  // former IST_SLS_BDL_PROD
    /**
     * Catalog item image (big picture), can be different from product image
     * for some backend systems
     */
    public final static String ITEM_IMAGE = "itemImage";
    /**
     * Catalog item Thumbnail (small picture), can be different from product
     * Thumbnail for some backend systems
     */
    public final static String ITEM_THUMBNAIL = "itemThumbnail";
    /**
     * Lead Time
     */
    public final static String LEAD_TIME = "leadTime";

    /**
     * line number for ordering sequence, if available
     */
    public final static String LINE_NUMBER = "lineNumber";
    
    /**
     * long description text.
     */
    public final static String LNG_DESC = "LNG_DESC";
    
    /**
     * Eye Catcher Text for main product.
     */
    public final static String MAIN_EYE_CATCHER = "MAIN_EYE_CATCHER";

    /**
     * ID of the manufacturer
     */
    public final static String MANUFACT_ID = "manufactId";

    /**
     * material no of the manufacturer
     */
    public final static String MANUFACT_MATERIAL_NO = "manufactMat";
    /**
     * Product material number. Is used for OCI in SRM Integrated Catalog
     */
    public final static String MATERIAL_NO = "MaterialNo";
    /**
     * List of messages
     */
    public final static String MESSAGE_LIST = "MessageList";
    /**
     * Flag which indicates that the object (catalog area or catalog item) is a
     * reseller specific one and thus changeable by the reseller
     */
    public final static String OBJECT_MODIFIABLE_FLAG = "objectModifiableFlag";
    /**
     * Price value; note that this is only used in case of static pricing.
     */
    public final static String PRICE = "price";
    /**
     * Product's price unit
     */
    public final static String PRICE_UNIT = "priceUnit";
    /**
     * Product
     */
    public final static String PRODUCT = "product";
    /**
     * Product category
     */
    public final static String PRODUCT_CATEGORY = "productCategory";
    /**
     * Flag which indicates that the product is configurable
     */
    public final static String PRODUCT_CONFIGURABLE_FLAG = "productConfigurableFlag";
    /**
     * Flag which indicates that the product is configurable using IPC
     */
    public final static String PRODUCT_CONFIGURABLE_IPC_FLAG = "productConfigurableIpcFlag";
    /**
     * Product description / short text
     */
    public final static String PRODUCT_DESCRIPTION = "productDescription";
    /**
     * Product Id (i.e. GUID for CRM)
     */
    public final static String PRODUCT_ID = "productId";
    /**
     * Product image (big picture)
     */
    public final static String PRODUCT_IMAGE = "productImage";
    /**
     * Product long description / long text
     */
    public final static String PRODUCT_LONG_DESCRIPTION = "productLongDescription";
    /**
     * Product Role from product master data
     */
    public final static String PRODUCT_ROLE = "PRODUCT_ROLE";

    /**
     * Product Thumbnail (small picture)
     */
    public final static String PRODUCT_THUMBNAIL = "productThumbnail";
    /**
     * Flag which is set to something different "" if product is a service product
     */
    public final static String PRODUCT_TYPE_IS_SERVICE = "productTypeIsService";
    /**
     * Flag which indicates that the product is a product variant
     */
    public final static String PRODUCT_VARIANT_FLAG = "productVariantFlag";
    public final static String SCHEMA_TYPE = "schemaType";
	
	/**
	 * Constant to define that the item is selected for SC explosion
	 */
	public final static String SELECTED_FOR_EXP = "S";

    /**
     * Subtitle text.
     */
    public final static String SUBTITLE = "SUBTITLE";
    /**
     * Product's unit of measurement
     */
    public final static String UNIT_OF_MEASUREMENT = "unitOfMeasurement";

    /**
     * Unspsc
     */
    public final static String UNSPSC = "unspsc";
    /**
     * ID of the vendor
     */
    public final static String VENDOR_ID = "vendorId";
    /**
     * material no of the vendor
     */
    public final static String VENDOR_MATERIAL_NO = "vendorMat";
    
	/**
	 * Constants for Attributename for Picture
	 *
	 */
	public final static String AN_PICTURE1 = "DOC_PC_CRM_IMAGE";
	public final static String AN_PICTURE2 = "DOC_P_CRM_IMAGE";

	/**
	 * Constants for Attributename for Thumbnail
	 */
	public final static String AN_THUMBNAIL1 = "DOC_PC_CRM_THUMB";
	public final static String AN_THUMBNAIL2 = "DOC_P_CRM_THUMB";
	public final static String AN_THUMBNAIL3 = "DOC_P_BDS_IMAGE";

	/**
	 * Constant for Separator
	 */
	public final static String URLSeparator  = "/";
    
    /**
     * Constants for contract duration
     */
    public final static String CONTRACT_DURATION = "CONTRACT_DURATION";
    public final static String CONTRACT_DURATION_UNIT = "CONTRACT_DURATION_UNIT";
    public final static String CONTRACT_DURATION_DEFAULT = "CONTRACT_DURATION_DEFAULT";
    
    /** 
     * Constants for category and hiearchy related attributes  
     */
    public final static String CATEGORY_ID = "CATEGORY_ID";
    public final static String HIERARCHY_ID = "HIERARCHY_ID";
    public final static String CATEGORY_DESCR = "CATEGORY_DESCR";
    public final static String HIERARCHY_DESCR = "HIERARCHY_DESCR";

    /**
     * Reference to the PriceCalculatorBackend constant STATIC_DYNAMIC_PRICING
     * Added here as it is not desirable to import backend object in the jsp.
     * 
     * the value 2 corresponds to the value PriceCalculatorBackend.STATIC_DYNAMIC_PRICING.
     * 
     */
    public final static int STATIC_DYNAMIC_PRICING = 2;
   
    /** 
     * Area type  
     */
    public final static String AREA_TYPE = "areaType";

    /** 
     * Area Guid  
     */
    public final static String AREA_GUID = "areaGuid";

    /** 
     * Constants for loyalty management  
     */
    public final static String LOY_POINTS = "LOYPTS_";
    public final static String IS_PTS_ITEM = "IS_PTS_ITEM";
    public final static String IS_BUY_PTS_ITEM = "IS_BUY_PTS_ITEM";
 
}
