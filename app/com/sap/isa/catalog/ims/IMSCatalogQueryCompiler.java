/*****************************************************************************
  Copyright (c) 2001, SAPMarkets Europe GmbH, Germany, All rights reserved.

  $Revision: #4 $
  $Date: 2004/02/04 $
*****************************************************************************/

package com.sap.isa.catalog.ims;

// catalog imports
import java.util.List;
import java.util.Properties;

import org.apache.struts.util.MessageResources;

import com.sap.isa.catalog.CatalogParameters;
import com.sap.isa.catalog.boi.IFilter;
import com.sap.isa.catalog.filter.CatalogFilter;
import com.sap.isa.catalog.filter.CatalogFilterFactory;
import com.sap.isa.catalog.filter.CatalogFilterInvalidException;
import com.sap.isa.catalog.impl.CatalogQuery;
import com.sap.isa.catalog.impl.CatalogQueryStatement;
import com.sap.isa.core.logging.IsaLocation;

/**
 * Helper class for the {@link IMSCatalogBuilder}. Instances of this class
 * are stateless. With the <code>transform...Query</code> methods a given query
 * instance is evaluated and the necessary calls are executed against the passed
 * IMS proxy instance. Internally a {@link IMSCatalogFilterVisitor} is used to
 * evaluate the filter expression.
 *
 * @version     2.0
 */
class IMSCatalogQueryCompiler {
    
  private static IsaLocation log = IsaLocation.getInstance(IMSCatalogQueryCompiler.class.getName());

  private IMSCatalogFilterVisitor theFilterVisitor;

  private MessageResources theMessageResources;

  /**
   * Creates a new instance of the query compiler. Each instance should only be
   * used of one specific IMS builder instance.
   *
   * @param messResources  the resources for the error messages
   */
  IMSCatalogQueryCompiler(MessageResources messResources) {
    theMessageResources = messResources;
    theFilterVisitor = new IMSCatalogFilterVisitor(messResources);
  }

  /**
   * Transforms the given query for products into the IMS specific represenation.<BR/>
   * <B>IMPORTANT: </B>The synchronization of the proxy instance has to be done
   * by the caller of this method. For performance reasons the synchronization
   * is omitted here!
   *
   * @param query    the query which has to be transformed
   * @param defAttr  default list of attributes
   * @param idxAttr  all attributes of the index
   * @param catalog  the catalog which should be built
   * @param proxy    the IMS proxy instance for which the query was built
   * @exception CRMException error from the IMS proxy
   */
  synchronized void transformItemQuery(
        CatalogQuery query,
        List defAttr,
        List idxAttr,
        CRMCatalog catalog,
        IMSProxy proxy) throws CRMException {

    // IMPORTANT: The synchronization of the proxy instance has to be done
    //            by the caller of this method. For performance reasons
    //            the synchronization is omitted here!
    try {

      CatalogQueryStatement statement = query.getCatalogQueryStatement();
      CatalogFilter filter;
      String[] attributes  = statement.getAttributes();
      String orderBy       = null;
      int rangeFrom        = statement.getRangeFrom();
      int rangeTo          = statement.getRangeTo();
	  Properties serverEngineProps = new Properties();
	  boolean fuzzySearch = true;
	  serverEngineProps = catalog.getServerEngine().getProperties();
	  String enableFuzzySearch = serverEngineProps.getProperty(IMSConstants.ENABLE_FUZZY_SEARCH);
	  if(enableFuzzySearch!=null && enableFuzzySearch.equalsIgnoreCase("false")){
		fuzzySearch = false;
	  }
	  


      // a default quick search is requested
      if (statement.getStatementAsString() != null) {
        String qsearch = statement.getStatementAsString();
        CatalogFilterFactory factory = CatalogFilterFactory.getInstance();

        // get the list of QuickSearch attributes or use the default ones
        String[] quickSearchAttributes = catalog.getQuickSearchAttributes();
        if(quickSearchAttributes == null || quickSearchAttributes.length == 0)
            quickSearchAttributes = new String[] {
                IMSConstants.ATTR_OBJECT_DESC,
                IMSConstants.ATTR_OBJECT_ID,
                IMSConstants.ATTR_TEXT_0001 };

        // set query condition (default are the three selected attributes)
        if(qsearch.equals(IMSConstants.VALUE_ALL)) {
            //JP: create some dummy condition that fits ALL documents,
            // but don't use "*", because this would break when using VIEW
            filter = (CatalogFilter)factory.createAttrEqualValue(
                IMSConstants.ATTR_CATALOG, catalog.getName().trim());
        }
        else {
            qsearch = qsearch.trim();
//            if (fuzzySearch && qsearch.indexOf(IMSConstants.VALUE_ALL) > -1) {
//                // * in fuzzy searches might lead to unwanted results when directly attached to a string e.g "for*" because than
//                // TREX switches to exact search mode and thus might find nothing or less than with "for *" (because * is normally
//                // ignored by the fuzzy search)
//                log.debug("Before replacing * in search string: " + qsearch);
//                qsearch = qsearch.replaceAll("(^\\**)|(\\**$)", ""); // delete * at the beginning and at the end 
//                qsearch = qsearch.replaceAll("\\*", " "); // replace other * by space
//                log.debug("After replacing leading and trailing * in search string: " + qsearch);
//            }
            
      		if (fuzzySearch) { 
                if (qsearch.indexOf(IMSConstants.VALUE_ALL) > -1) {
                    filter = (CatalogFilter)factory.createAttrContainValue(quickSearchAttributes[0], qsearch);   
                }
                else {
                    filter = (CatalogFilter)factory.createAttrFuzzyValue(quickSearchAttributes[0], qsearch);
                }
      		}
      		else {
				filter = (CatalogFilter)factory.createAttrContainValue(quickSearchAttributes[0], qsearch);      			
      		}
            for(int i = 1; i < quickSearchAttributes.length; i++)
            {
				IFilter a2ndFilter = null;
            	if(fuzzySearch){
					a2ndFilter =
						factory.createAttrFuzzyValue(quickSearchAttributes[i], qsearch);
            	}
            	else{
					a2ndFilter =
						factory.createAttrContainValue(quickSearchAttributes[i], qsearch);

            	}            	
                filter = (CatalogFilter)factory.createOr(filter, a2ndFilter);
            }
        }
      }
      else {
        filter = statement.getFilter();
      }

      // set default attribute list if necessary
      if(attributes == null || attributes.length == 0)
        attributes = (String[])defAttr.toArray(new String[0]);

      // set sort order if requested -- IMS supports only one attribute
      String[] sortOrder = statement.getSortOrder();
      if(sortOrder != null && sortOrder.length != 0)
        orderBy = sortOrder[0];

      // set default range if necessary
      if(rangeFrom == 0) rangeFrom = IMSConstants.INTERVAL_BEGIN;
      if(rangeTo == 0)   rangeTo   = CatalogParameters.imsUpperLimit;
      if(rangeFrom >= rangeTo) {
        rangeFrom = IMSConstants.INTERVAL_BEGIN;
        rangeTo   = CatalogParameters.imsUpperLimit;
      }

      // set return properties
      proxy.setReturnInterval(rangeFrom, rangeTo);

      // set return attributes
      for(int i=0; i<attributes.length; i++)
        if (!attributes[i].equals(IMSConstants.ATTR_AREA_GUID) &&
            !attributes[i].equals(IMSConstants.ATTR_OBJECT_GUID) &&
            !attributes[i].equals(IMSConstants.ATTR_OBJECT_DESC) )
          proxy.addReturnAttribute(attributes[i]);

      // set sort order
      if(orderBy != null && proxy.getSearchEngine().equals(IMSConstants.ENGINE_DRFUZZY))
        proxy.setSortOrderASC(orderBy);

      // set query condition
      theFilterVisitor.evaluate(filter, idxAttr, proxy);
      
      String[] views = catalog.getViews();
      if(!query.isCategorySpecificSearch() &&
         views != null &&
         views.length > 0)
      {
        proxy.addQueryAttributeList(IMSConstants.ATTR_VIEWS_ID, views);
        proxy.addQueryOperator(IMSConstantsQuery.OPERATOR_AND);
      }
      
    } catch(CatalogFilterInvalidException ex) {
      throw new CRMException(ex.getLocalizedMessage());
    }
  }

  /**
   * Transforms the given query for categories into the IMS specific represenation.<BR/>
   * <B>IMPORTANT: </B>The synchronization of the proxy instance has to be done
   * by the caller of this method. For performance reasons the synchronization
   * is omitted here!
   *
   * @param query    the query which has to be transformed
   * @param defAttr  default list of attributes
   * @param idxAttr  all attributes of the index
   * @param catalog  the catalog which should be built
   * @param proxy    the IMS proxy instance for which the query was built
   * @exception CRMException error from the IMS proxy
   */
  synchronized void transformCategoryQuery(
      CatalogQuery query,
      List defAttr,
      List idxAttr,
      CRMCatalog catalog,
      IMSProxy proxy) throws CRMException
    {

    // IMPORTANT: The synchronization of the proxy instance has to be done
    //            by the caller of this method. For performance reasons
    //            the synchronization is omitted here!
    try {

      CatalogQueryStatement statement = query.getCatalogQueryStatement();
      CatalogFilter filter = statement.getFilter();
      String[] attributes  = (String[])defAttr.toArray(new String[0]);
      int rangeFrom        = statement.getRangeFrom();
      int rangeTo          = statement.getRangeTo();

      // a default quick search is requested
      if (statement.getStatementAsString() != null) {
        filter = (CatalogFilter)CatalogFilterFactory.getInstance().
          createAttrContainValue( IMSConstants.ATTR_DESCRIPTION,
                                  statement.getStatementAsString());
      }

      // if category specific only the children categories are searched for
      if (query.isCategorySpecificSearch()) {

        IFilter cond = CatalogFilterFactory.getInstance().
            createAttrEqualValue( IMSConstants.ATTR_PARENT_AREA_GUID,
                                  query.getParent().getGuid());

        filter = (CatalogFilter)CatalogFilterFactory.getInstance().
            createAnd(filter, cond);

      }

      // determine the search range
      if(rangeFrom == 0) rangeFrom = IMSConstants.INTERVAL_BEGIN;
      if(rangeTo == 0)   rangeTo   = CatalogParameters.imsUpperLimit;
      if(rangeFrom >= rangeTo) {
        rangeFrom = IMSConstants.INTERVAL_BEGIN;
        rangeTo   = CatalogParameters.imsUpperLimit;
      }

      // set return properties
      proxy.setReturnInterval(rangeFrom, rangeTo);

      // set return attributes
      proxy.addReturnAttributes(attributes);

      // set sort order
      String[] sortOrder = statement.getSortOrder();
      if( sortOrder != null && sortOrder.length != 0 &&
          proxy.getSearchEngine().equals(IMSConstants.ENGINE_DRFUZZY))
              proxy.setSortOrderASC(sortOrder[0]);

      // set query condition
      theFilterVisitor.evaluate(filter, idxAttr, proxy);

      String[] views = catalog.getViews();
      if(views != null && views.length > 0) {
        proxy.addQueryAttributeList(IMSConstants.ATTR_VIEWS_ID, views);
        proxy.addQueryOperator(IMSConstantsQuery.OPERATOR_AND);
      }

    } catch(CatalogFilterInvalidException ex) {
      throw new CRMException(ex.getLocalizedMessage());
    }
  }

  /**
   * Transforms a filtered search statement. This method can be used independent
   * of a catalog instance or the catalog builder. If inconsistent parameters are passed
   * then default values will be taken. Currently this method is used by the
   * IMS adminstration console. <br>
   * <B>IMPORTANT: </B>The synchronization of the proxy instance has to be done
   * by the caller of this method. For performance reasons the synchronization
   * is omitted here!
   *
   * @param proxy      the IMS proxy instance for which the query was built
   * @param idxAttr    default list of attributes
   * @param filter     filter of the query
   * @param attributes all attributes of the index
   * @param orderBy    sort order of the query
   * @param rangeFrom  begin of search interval
   * @param rangeTo    end of search interval
   * @exception CRMException error from the IMS proxy
   */
  synchronized void transformFilterQuery(IMSProxy proxy, List idxAttr, CatalogFilter filter,
    String[] attributes, String orderBy, int rangeFrom, int rangeTo)
    throws CRMException {

    try {
      // set return interval
      if(rangeFrom == 0) rangeFrom = IMSConstants.INTERVAL_BEGIN;
      if(rangeTo == 0)   rangeTo   = CatalogParameters.imsUpperLimit;
      if(rangeFrom >= rangeTo) {
        rangeFrom = IMSConstants.INTERVAL_BEGIN;
        rangeTo   = CatalogParameters.imsUpperLimit;
      }
      proxy.setReturnInterval(rangeFrom, rangeTo);

      // set return attributes
      if(attributes == null)
        attributes = (String[])idxAttr.toArray(new String[0]);
      proxy.addReturnAttributes(attributes);

      // set sort order
      if(orderBy != null)
        proxy.setSortOrderASC(orderBy);

      // set query condition
      theFilterVisitor.evaluate(filter, idxAttr, proxy);
    } catch(CatalogFilterInvalidException ex) {
      throw new CRMException(ex.getLocalizedMessage());
    }

  }

}
