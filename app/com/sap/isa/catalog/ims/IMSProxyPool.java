/*****************************************************************************
  Copyright (c) 2001, SAPMarkets Europe GmbH, Germany, All rights reserved.
  Created:  12 February 2001

  $Revision: #3 $
  $Date: 2003/09/29 $
*****************************************************************************/
package com.sap.isa.catalog.ims;

// misc imports
import java.util.HashMap;
import java.util.Iterator;

import com.sap.isa.core.logging.IsaLocation;

/**
 * Class for pooling the proxy instances. If a instance was read via the
 * <code>getProxy</code> method the proxy has to be released via the
 * <code>releaseProxy</code> method when it is no longer in usage.
 * @version     1.0
 */
class IMSProxyPool
{
    private CRMCatalog theCRMCatalog;
    private HashMap thePool = new HashMap();
        // the logging instance
    private static IsaLocation theStaticLocToLog =
        IsaLocation.getInstance(IMSProxyPool.class.getName());

        /**
         * Constructs a new pool.
         *
         * @param catalog the catalog of the pool
         */
    IMSProxyPool(CRMCatalog catalog)
    {
        theCRMCatalog = catalog;
    }

        /**
         * Gets a proxy from the pool. If no proxy is available a new one
         * will be instantiated.
         *
         * @return a proxy for the IMS server
         */
    IMSProxy getProxy() throws CRMException
    {
        IMSProxy proxy = null;
        Boolean entry  = null;

        synchronized(thePool)
        {
            Iterator iter = thePool.keySet().iterator();
            while(iter.hasNext())
            {
                    // get next pooled proxy
                proxy = (IMSProxy)iter.next();
                    // check if proxy is free
                entry = (Boolean)thePool.get(proxy);
                if( entry != null && entry.booleanValue() == false )
                    break;
                else
                    proxy = null;
            }

                // no proxy available build new one
            if( proxy == null )
            {
                proxy = IMSProxy.createProxy(
                    theCRMCatalog.getIMSConnectionInfo(),
                    theCRMCatalog.getMessageResources());
                    // set the default settings
                proxy.setQueryEnvInitial(theCRMCatalog.getSearchEngine(),
                                  theCRMCatalog.getCodepage(),
                                  theCRMCatalog.getLanguage(),
                                  IMSConstants.ATTR_CATALOG,       // searchAllDocuments$attrName
                                  theCRMCatalog.getName().trim()); // searchAllDocuments$attrValue
                if(theStaticLocToLog.isDebugEnabled())
                    theStaticLocToLog.debug("IMS proxy was created.");
            }

                // lock the proxy
            thePool.put(proxy, new Boolean(true));
            if(theStaticLocToLog.isDebugEnabled())
                theStaticLocToLog.debug("IMS proxy was locked.");
        }

        return proxy;
    }

        /**
         * Releases a proxy instances in the pool so that it can used for an other
         * request.
         *
         * @param proxy the proxy to be released
         */
    void releaseProxy(IMSProxy proxy)
    {
        synchronized(thePool)
        {
            if(proxy != null)
            {
                    // reset the old settings of the proxy if someone has forgotten it!
                proxy.reset();
                    // now release the proxy
                thePool.put(proxy, new Boolean(false));
                if(theStaticLocToLog.isDebugEnabled())
                    theStaticLocToLog.debug("IMS proxy was unlocked.");
            }
        }
    }

}
