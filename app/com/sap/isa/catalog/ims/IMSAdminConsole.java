/**
 * Copyright (c) 2001, SAPMarkets Europe GmbH, Germany, All rights reserved. $Revision: #3 $ $Date: 2003/08/22 $
 */
package com.sap.isa.catalog.ims;

import com.sap.isa.catalog.boi.IQueryStatement;
import com.sap.isa.core.util.table.ResultData;

// misc imports
import java.util.Map;
import java.util.Properties;


/**
 * This interface defines the functionality of the IMS admin console that is accessible from outside of the IMS
 * package.
 *
 * @version 1.0
 */
public interface IMSAdminConsole {
    /**
     * Returns a map of available systems. The key of the map is the name of the connection. The value of the keys are
     * of type <code>Properties</code>. The properties contain the connection parameters as they were specified in the
     * eai config file.
     *
     * @return a map of systems
     */
    public Map getSystemList()
                      throws CRMException;

    /**
     * Sets the system where the index meta data are stored. All successing calls are against this system.
     */
    public void setSystem(Properties props)
                   throws CRMException;

    /**
     * Returns a list of indices which are available on the selected system.
     *
     * @param catalogPattern the catalog pattern for which the indices should be loaded
     *
     * @return a list of available indices
     *
     * @exception CRMException a communication error occured
     */
    public ResultData getIndexList(String catalogPattern)
                            throws CRMException;

    /**
     * Sets the index against which the queries are to be performed.
     *
     * @param catName name of the catalog
     * @param catVariant name of the variant of the catalog
     * @param attrLevel level of the index
     * @param areaName name of the area in case of 'S' index
     * @param catID index category ID in case of 'S' index derived by LOC
     */
    public void setIndex(String catName,
                         String catVariant,
                         String attrLevel,
                         String areaName,
                         String catId)
                  throws CRMException;

    /**
     * DOCUMENT ME!
     *
     * @param catIdx DOCUMENT ME!
     *
     * @throws CRMException DOCUMENT ME!
     */
    public void setIndex(String catIdx)
                  throws CRMException;

    /**
     * Returns a list of attributes of the currently selected index.
     *
     * @return a list of attribute names (type <code>String</code>)
     *
     * @exception CRMException error since no index was previously selected
     */
    public String[] getIndexAttributeNames()
                                    throws CRMException;

    /**
     * Performs the query specified by the given statement against the selected index.
     *
     * @param statement the query statement which has to be performed
     *
     * @return DOCUMENT ME!
     *
     * @exception error while performing the query
     */
    public IMSResultSet getIndexData(IQueryStatement statement)
                              throws CRMException;

    /**
     * Performs the query specified by the given statement against the selected index.
     *
     * @param statement the query statement which has to be performed
     *
     * @return DOCUMENT ME!
     *
     * @exception error while performing the query
     */
    public ResultData getStagingIndexData(IQueryStatement statement, String[] attributes, String[] sortAttributes)
                                   throws CRMException;

    /**
     * Returns a set of properties which describe the technical paramters of the selected index and the connection to
     * the IMS server.
     *
     * @return a set of technical parameters
     */
    public Properties getDetails();

    /**
     * gets the level of the staging support in the backend
     *
     * @return A, B, C, or D or the constant IServerEngine.STAGING_NOT_SUPPORTED which should be same as A
     */
    public String getStagingSupport();

    /**
     * DOCUMENT ME!
     *
     * @param language The language to read the cataloginfo in
     * @param withNames Read the catalogue names and some other info also if set to true. Currently not supported.
     *
     * @return a table containing the list of catalogues
     *
     * @throws CRMException DOCUMENT ME!
     */
    public ResultData getCatalogueList(String  language,
                                       boolean withNames)
                                throws CRMException;

    /**
     * Returns a list of the versions of via staging functionality created indices
     *
     * @param catalogPattern the catalog pattern for which the indices should be loaded
     *
     * @return a list of available indices
     *
     * @exception CRMException a communication error occured
     */
    public ResultData getVersionList(String catalogPattern)
                              throws CRMException;
}
