/*****************************************************************************
  Copyright (c) 2001, SAPMarkets Europe GmbH, Germany, All rights reserved.
  Created:  12 February 2001

  $Revision: #4 $
  $Date: 2003/09/29 $
*****************************************************************************/
package com.sap.isa.catalog.ims;

import java.io.Serializable;
import java.util.Properties;

import com.sap.isa.catalog.ICatalogMessagesKeys;
import com.sap.isa.core.eai.BackendException;
import com.sap.isa.core.eai.Connection;
import com.sap.isa.core.eai.ConnectionFactory;
import com.sap.isa.core.eai.sp.jco.BackendBusinessObjectSAP;
import com.sap.isa.core.eai.sp.jco.JCoManagedConnectionFactory;
import com.sap.isa.core.logging.IsaLocation;

/**
 * This class encapsulate all information about the IMS connection for
 * a given CRM catalog.
 * @version     1.0
 */
class IMSConnectionInfo implements Serializable
{
/**
 * The class is serializable. All attributes are serializable with the default
 * implementation.
 */

        /**
         * Constant for a connection via RFC to the IMS server.
         */
    final static int CONNECTION_TYPE_RFC = 1;

        /**
         * Constant for a connection via HTTP to the IMS server.
         */
    final static int CONNECTION_TYPE_HTTP = 2;

        /**
         * Constant that describes the IMS connection name which is managed by the
         * EAI framework.
         */
    private final static String PROP_RFC_CONN_NAME = "DynConnName";

        /**
         * Constant that describes the IMS connection type which is managed by the
         * EAI framework.
         */
    private final static String PROP_RFC_CONN_TYPE = "DynConnType";

        /**
         * Constant that describes if the IMS connection parameters which are defined
         * in the CRM system are used or if the static definition of the config
         * file has priority.
         */
    private final static String PROP_RFC_USE_DYN_PARAMS = "useDynConnParams";
        // the logging instance
    private static IsaLocation theStaticLocToLog =
        IsaLocation.getInstance(IMSConnectionInfo.class.getName());
    private BackendBusinessObjectSAP theIMSClient;
    private String theConnName;
    private String theConnType;
    private String theConnUseDyn;
    private Properties theConnectionProps;
    private int theConnectionType; // type of connection
    private transient Connection theIMSConnection = null;

        /**
         * Creates a new instance of type RFC connection.
         *
         * @param imsClient the backend object which mangages the connection to the IMS
         * @param connProps the parameter as they were defined in the eai config file
         * @param gwHost    name of gateway host
         * @param gwServ    name of gateway server
         * @param tpHost    name of terminal host
         * @param tpName    name of terminal progam
         */
    IMSConnectionInfo(
        BackendBusinessObjectSAP imsClient,
        Properties connProps,
        String gwHost,
        String gwServ,
        String tpHost,
        String tpName)
    {
            // initialize the instance
        theIMSClient = imsClient;
            // get the name as they were defined in the config file
        theConnName = connProps.getProperty(PROP_RFC_CONN_NAME);
        theConnType = connProps.getProperty(PROP_RFC_CONN_TYPE);
        theConnUseDyn = connProps.getProperty(PROP_RFC_USE_DYN_PARAMS);
            // it's a RFC connection
        theConnectionType = CONNECTION_TYPE_RFC;
            // set the properties for the connection
        theConnectionProps = new Properties();
        this.addProperty(JCoManagedConnectionFactory.JCO_GWHOST, gwHost);
        this.addProperty(JCoManagedConnectionFactory.JCO_GWSERV, gwServ);
        this.addProperty(JCoManagedConnectionFactory.JCO_TPHOST, tpHost);
        this.addProperty(JCoManagedConnectionFactory.JCO_TPNAME, tpName);
        this.addProperty(JCoManagedConnectionFactory.JCO_TYPE,
                         JCoManagedConnectionFactory.JCO_TYPE_EXTERNAL);
        if(theStaticLocToLog.isInfoEnabled())
        {
            switch(theConnectionType)
            {
                case CONNECTION_TYPE_RFC:
                    gwHost = getProperty(JCoManagedConnectionFactory.JCO_GWHOST);
                    gwServ = getProperty(JCoManagedConnectionFactory.JCO_GWSERV);
                    tpName = getProperty(JCoManagedConnectionFactory.JCO_TPNAME);
                    tpHost = getProperty(JCoManagedConnectionFactory.JCO_TPHOST);
                    theStaticLocToLog.info(ICatalogMessagesKeys.PCAT_IMS_INFO_CONNECTION,
                                       new Object[] { gwServ, gwHost, tpName, tpHost },
                                       null);
                    break;
                default:
                    break;
            }
        }

    }

        /**
         * Adds an entry to the list of properties.
         *
         * @param name   name of the property
         * @param value  value of the property
         */
    private void addProperty(String name, String value)
    {
        theConnectionProps.setProperty(name, value);
    }

        /**
         * Returns the value of the given property. If the property was not
         * found then <code>null</code> will be returned.
         *
         * @param name  name of the requested property
         * @return      value of the property
         */
    String getProperty(String name)
    {
        return theConnectionProps.getProperty(name);
    }

        /**
         * Returns the properties of the info object.
         *
         * @return  connection properties
         */
    Properties getProperties()
    {
        return theConnectionProps;
    }

        /**
         * Returns the type of the connection.
         *
         * @return  type constant
         */
    int getConnectionType()
    {
        return theConnectionType;
    }

        /**
         * Returns the connection for the IMS. The instance has a concrete type
         * corresponding to the IMS connection type.
         *
         * @return  the connection instance for the IMS
         */
    synchronized Connection getConnection() throws CRMException
    {
        if(theIMSConnection == null)
        {
            try
            {
                    // get the connection factory
                ConnectionFactory fact = theIMSClient.getConnectionFactory();
                Object connection = null;

                    // if necessary ignore the connection parameters of the CRM system
                if(theConnUseDyn != null && theConnUseDyn.equalsIgnoreCase("false"))
                    connection = fact.getConnection(theConnType, theConnName);
                else
                    connection = fact.getConnection(theConnType, theConnName, getProperties());

                    // set the connection for the IMS server
                theIMSConnection = (Connection)connection;
            }
            catch(BackendException ex)
            {
                throw new CRMException(ex.getLocalizedMessage());
            }
        }
        return theIMSConnection;
    }

        /**
         * Writes log entry if info logging is enabled.
         */

}
