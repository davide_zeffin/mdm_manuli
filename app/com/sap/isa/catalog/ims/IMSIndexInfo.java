/**
 * Copyright (c) 2001, SAPMarkets Europe GmbH, Germany, All rights reserved. Created:  12 February 2001 $Revision: #3 $
 * $Date: 2003/04/30 $
 */
package com.sap.isa.catalog.ims;

import com.sap.isa.catalog.ICatalogMessagesKeys;
import com.sap.isa.core.logging.IsaLocation;

// misc imports
import java.io.Serializable;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;


/**
 * This class encapsulate all information about an index of an IMS catalog. The meta information of the attributes
 * which are stored in the CRM system can also be requested form this info object.
 *
 * @version 1.0
 */
class IMSIndexInfo implements Serializable {
    /** The class is serializable. All attributes are serializable with the default implementation. */
    /**
     * Constant for the property ENTRYSTYLE. This property defines the kind of input field which has to be used for
     * this attribute as it was defined in the CRM system.
     */
    static String PROP_ENTRYSTYLE = "ENTRYSTYLE";

    /**
     * Constant for the property POS_NR. This property defines the order of the attributes as it was defined in the CRM
     * system.
     */
    static String PROP_POS_NR = "POS_NR";

    /** Constant for the property DESCRIPTION. This property defines the language dependant text of the attribute. */
    static String PROP_DESCRIPTION = "DESCRIPTION";

    /** Constant for the property TYPE. This property defines the type of the attribute. E.g: CHAR, NUMC */
    static String PROP_TYPE = "DATA_TYPE";

    /**
     * Constant for the property GUID. This property defines the guid of the attribute as it was defined in the CRM
     * system.
     */
    static String PROP_GUID = "GUID";

    // the logging instance
    private static IsaLocation theStaticLocToLog = IsaLocation.getInstance(IMSIndexInfo.class.getName());
    private String theAttrLevel; // index
    private List theAttributes; // attribute names
    private Map theFixedValues; // fixed values for the attributes
    private Map theAttributeProps; // properties for the attributes
    private String theStoreId; // unique identifier of index on IMS
    private String theCatId; // unique id of the category
    private String theArea; // area name ('S' index)

    /**
     * Creates a new instance of a index.
     *
     * @param catId unique id of category
     * @param attrLevel index level (A, P, S)
     * @param area name of area (in case of 'S' index)
     * @param idStoreId unique identifier of index on IMS
     */
    IMSIndexInfo(String catId,
                 String attrLevel,
                 String area,
                 String idStoreId) {
        theCatId = catId;
        theAttrLevel = attrLevel;
        theArea = area;
        theStoreId = idStoreId;
        theAttributes = new ArrayList();
        theFixedValues = new HashMap();
        theAttributeProps = new HashMap();

        if (theStaticLocToLog.isInfoEnabled()) {
            theStaticLocToLog.info(ICatalogMessagesKeys.PCAT_IMS_INFO_INDEX,
                                   new Object[] { theAttrLevel, theCatId, theStoreId, theArea }, null);
        }
    }

    /**
     * Generates a unique identifier for the index.
     *
     * @param attrLevel name of index level
     * @param area name of area
     *
     * @return DOCUMENT ME!
     */
    static String generateKey(String attrLevel,
                              String area) {
        return attrLevel + area;
    }

    /**
     * Generates a unique identifier for the index. The key is built through concatenation of attrLevel and area.
     *
     * @return unique key for index
     */
    String generateKey() {
        return generateKey(getAttrLevel(), getArea());
    }

    /**
     * Adds an attribute to the list of attributes.
     *
     * @param attribute name of the attribute to be added
     */
    void addAttribute(String attribute) {
        theAttributes.add(attribute);
    }

    /**
     * Adds a fixed value to the list of fixed values for a given attribute.
     *
     * @param attribute name of the attribute
     * @param valueName the technical value of the fixed value
     * @param valueDesc the language dependent value of the fixed value
     */
    void addFixedValue(String attribute,
                       String valueName,
                       String valueDesc) {
        // get the values for this attribute
        List values = (List) theFixedValues.get(attribute);

        // create new entry for this attribute
        if (values == null) {
            values = new ArrayList();
            theFixedValues.put(attribute, values);
        }

        // construct a small list which hold both values
        // convert the type NUMC, e.g. --> 017 will be 17!. 
        List entryList = new ArrayList();

        if (this.getAttributeProperties(attribute).getProperty(IMSIndexInfo.PROP_TYPE).equals("NUMC")) {
            try {
                valueName = new Integer(valueName).intValue() + "";
            }
            catch (Exception e) {
                theStaticLocToLog.debug("IMSIndexInfo.addFixedValue: " + e.getMessage() + "__value to Convert:" +
                                        valueName);
            }
        }

        entryList.add(valueName);

        if (valueDesc != null) {
            entryList.add(valueDesc);
        }

        // add the value for this attribute
        values.add(Collections.unmodifiableList(entryList));

        if (theStaticLocToLog.isDebugEnabled()) {
            theStaticLocToLog.debug("IndexKey: " + generateKey() + " - Added fixed value '" + valueName + "'='" +
                                    valueDesc + "' for attribute '" + attribute + "'");
        }
    }

    /**
     * Adds a property to the list of properties for a given attribute.
     *
     * @param attribute name of the attribute
     * @param name name of the property
     * @param value value of the property
     */
    void addAttributeProperty(String attribute,
                              String name,
                              String value) {
        // get the properties for this attribute
        Properties props = (Properties) theAttributeProps.get(attribute);

        // create new entry for this attribute
        if (props == null) {
            props = new Properties();
            theAttributeProps.put(attribute, props);
        }

        // add the property entry for this attribute
        props.setProperty(name, value);

        if (theStaticLocToLog.isDebugEnabled()) {
            theStaticLocToLog.debug("IndexKey: " + generateKey() + " - Property \"" + name + "\" for attribute \"" +
                                    attribute + "\" added.");
        }
    }

    /**
     * Returns <code>true</code> if the index contains the requested attribute.
     *
     * @param name name of the requested attribute
     *
     * @return boolean that indicates if the index contains the given attribute
     */
    boolean hasAttribute(String name) {
        return theAttributes.contains(name);
    }

    /**
     * Returns the index level.
     *
     * @return name of index level (possible values A, P, S)
     */
    String getAttrLevel() {
        return theAttrLevel;
    }

    /**
     * Returns the list of attribute names of the index.
     *
     * @return list of attribute names
     */
    List getAttributes() {
        return theAttributes;
    }

    /**
     * Returns the list of fixed values for the given attribute. If no fixed values are found <code>null</code> will be
     * returned.
     *
     * @param attribute name of the attribute
     *
     * @return list of fixed values for the requested attribute
     */
    List getAttributeFixedValues(String attribute) {
        return (List) theFixedValues.get(attribute);
    }

    /**
     * Returns the properties for the given attribute. If no properties are found <code>null</code> will be returned.
     *
     * @param attribute name of the attribute
     *
     * @return properties of the requested attribute
     */
    Properties getAttributeProperties(String attribute) {
        return (Properties) theAttributeProps.get(attribute);
    }

    /**
     * Returns the category id.
     *
     * @return category id
     */
    String getCatId() {
        return theCatId;
    }

    /**
     * Returns the store id.
     *
     * @return store id
     */
    String getStoreId() {
        return theStoreId;
    }

    /**
     * Returns the area name of the index.
     *
     * @return area name in case of 'S' index (otherwise empty)
     */
    String getArea() {
        return theArea;
    }
}
