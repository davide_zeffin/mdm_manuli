/*****************************************************************************
  Copyright (c) 2001, SAPMarkets Europe GmbH, Germany, All rights reserved.

  $Revision: #4 $
  $Date: 2003/09/29 $
*****************************************************************************/

package com.sap.isa.catalog.ims;

// catalog imports
import org.apache.struts.util.MessageResources;

import com.sap.isa.catalog.ICatalogMessagesKeys;

/**
 * Client-side proxy of the IndexManagementServer. This abstract class
 * defines convienient methods for accessing catalog relevant query features of
 * the IMS.<BR/>
 * <B>IMPORTANT: </B>The proxies should always be taken from the pool because
 * of the synchronization!
 *
 * @version     1.0
 */
abstract class IMSProxy implements IMSConstantsQuery
{
        /**
         * Factory method to get a proxy which supports the connection.<BR/>
         * If the proxies are pooled they should be requested over the <code>IMSProxyPool</code>.
         *
         * @param connectionInfo   information about IMS connection
         * @param messageResources resources for the messages which have to be created
         * @return                 proxy instance
         * @exception              CRMException if the connection type isn't supported
         */
    static IMSProxy createProxy(
        IMSConnectionInfo connectionInfo,
        MessageResources messageResources) throws CRMException
    {
        switch(connectionInfo.getConnectionType())
        {
            case IMSConnectionInfo.CONNECTION_TYPE_RFC:
                return new IMSRfcProxy(messageResources);
            default:
                String msg = messageResources.getMessage(
                    ICatalogMessagesKeys.PCAT_IMS_INVALID_CONNECTYPE);
                throw new CRMException(msg);
        }
    }

        // the struts message resources
    private MessageResources theMessageResources;

        /**
         * Constructor
         *
         * @param messageResources  resources for the messages which have to be created
         */
    IMSProxy(MessageResources messageResources)
    {
        theMessageResources = messageResources;
    }

        /**
         * Gets the message resources of the instance.
         *
         * @return message resources
         */
    MessageResources getMessageResources()
    {
        return theMessageResources;
    }

        /**
         * Sets the default environment entries for calling the IMS.
         *
         * @param searchEngine  name of the search engine to be used
         * @param codePage      codepage of the index
         * @param language      language of the index
         * @param searchAllDocumentsAttrName name of the attribute when searching ALL
         * documents
         * @param searchAllDocumentsAttrValue value of the attribute when searching
         * ALL documents
         * @exception           CRMException  error from the IMS proxy
         */
    abstract void setQueryEnvInitial(String searchEngine, String codePage,
                            String language,
                            String searchAllDocumentsAttrName,
                            String searchAllDocumentsAttrValue) throws CRMException;

        /**
         * Sets the environment entries for calling the IMS.
         *
         * @param searchEngine  name of the search engine to be used
         * @param codePage      codepage of the index
         * @param language      language of the index
         * @param index         index to be used for the query
         * @exception           CRMException  error from the IMS proxy
         */
    abstract void setQueryEnv(
        String searchEngine,
        String codePage,
        String language,
        IMSIndexInfo index) throws CRMException;

        /**
         * Sets the index which is to be used for the query. The
         * default searchEngine, codepage and language is used.
         *
         * @param index  index to be used for the query
         * @exception    CRMException  error from the IMS proxy
         */
    abstract void setQueryEnv(IMSIndexInfo index) throws CRMException;

        /**
         * Returns the name of the current search engine.
         *
         * @return  name of the search engine which will be used
         */
    abstract String getSearchEngine();

        /**
         * Add attribute names which are to be returned with the query.
         *
         * @param names  array of attribute names
         * @exception    CRMException  error from the IMS proxy
         */
    abstract void addReturnAttributes(String[] names) throws CRMException;

        /**
         * Add attribute name which is returned with the query.
         *
         * @param name  attribute name
         * @exception   CRMException  error from the IMS proxy
         */
    abstract void addReturnAttribute(String name) throws CRMException;

        /**
         * Sets the range of lines to be selected with the query.
         *
         * @param lower  lower boundery of the intervall
         * @param upper  upper boundery of the intervall
         *  @exception   CRMException  error from the IMS proxy
         */
    abstract void setReturnInterval(int lower, int upper) throws CRMException;

        /**
         * Sets the sort order ascending.
         *
         * @param name  attribute name determining the sort order
         * @exception   CRMException  error from the IMS proxy
         */
    abstract void setSortOrderASC(String name) throws CRMException;

        /**
         * Sets the sort order descending.
         *
         * @param name  attribute name determining the sort order
         * @exception   CRMException  error from the IMS proxy
         */
    abstract void setSortOrderDSC(String name) throws CRMException;

        /**
         * Adds an attribute to the query parameters. The value is linked to the
         * attribute with the given operator.
         *
         * @param name   attribute name
         * @param oper   operator
         * @param value  value of the attribute
         * @exception    CRMException  error from the IMS proxy
         */
    abstract void addQueryAttribute( String name, byte[] oper, String value )
        throws CRMException;

        /**
         * Adds an attribute to the query parameters. The value is linked to the
         * attribute with the given operator.
         *
         * @param name   attribute name
         * @param oper   operator
         * @param value  value of the attribute
         * @param type   content type of the value
         * @exception    CRMException  error from the IMS proxy
         */
    abstract void addQueryAttribute( String name, byte[] oper, String value,
                                     byte[] type) throws CRMException;

        /**
         * Adds an attribute to the query parameters. The value is linked to the
         * attribute with the given operator.
         *
         * @param name   attribute name
         * @param oper   operator
         * @param value  value of the attribute
         * @param type   content type of the value
         * @param action term action associated with the attribute (i.e. fuzzy, linguistic)
         * @param termWeight the term weight for this query
         * @exception    CRMException  error from the IMS proxy
         */
    abstract void addQueryAttribute(String name, byte[] oper, String value,
                                    byte[] type, byte[] action, int termWeight) throws CRMException;

        /**
         * Adds an attribute to the query parameters. The given values are
         * linked to the attribute with the 'EQ' operator.
         *
         * @param name    attribute name
         * @param values  values for the given attribute
         * @exception     CRMException  error from the IMS proxy
         */
    abstract void addQueryAttributeList( String name, String values[] )
        throws CRMException;

        /**
         * Adds an operator to the query parameters. With this operator the
         * last entries are linked (IPN notation).
         *
         * @param oper  operator (IPN notation)
         * @exception   CRMException  error from the IMS proxy
         */
    abstract void addQueryOperator( byte[] Oper ) throws CRMException;

        /**
         * Executes the query against the IMS server.
         *
         * @param connection  the connection info for the IMS server to be used
         * @return            result set which contains the results
         * @exception         CRMException  error from the IMS proxy
         */
    abstract IMSResultSet executeQuery(IMSConnectionInfo connection)
        throws CRMException;

        /**
         * Clears all temporary entries which were defined for the preceding query so
         * that the proxy can be used for the next request.
         */
    abstract void reset();
}
