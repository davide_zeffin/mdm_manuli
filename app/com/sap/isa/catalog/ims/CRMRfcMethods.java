/**
 * Class:        CRMRfcMethods Copyright (c) 2002, SAP AG, Germany, All rights reserved. Created:      04.03.2002
 */
package com.sap.isa.catalog.ims;

import java.util.HashMap;
import java.util.Map;

import org.apache.struts.util.MessageResources;

import com.sap.isa.catalog.ICatalogMessagesKeys;
import com.sap.isa.catalog.misc.CatalogJCoLogHelper;
import com.sap.isa.core.eai.BackendException;
import com.sap.isa.core.eai.sp.jco.JCoConnection;
import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.core.util.conv.GuidConversionUtil;
import com.sap.isa.core.util.table.ResultData;
import com.sap.isa.core.util.table.Table;
import com.sap.isa.core.util.table.TableRow;
import com.sap.mw.jco.IMetaData;
import com.sap.mw.jco.JCO;


/**
 * Encapsulates all functions of the CRM system which are used in the CRM catalog implementation.
 */
class CRMRfcMethods {
    // the logging instance
    private static IsaLocation log = IsaLocation.getInstance(CRMRfcMethods.class.getName());

    /**
     * Gets the list of all indices of a CRM system.
     *
     * @param connection connection to be used for retrieving the data
     * @param catPattern pattern name of the catalog
     *
     * @return the list of indices
     *
     * @exception CRMException connection error occured
     */
    static ResultData getIndexList(JCoConnection connection,
                                   String        catPattern)
                            throws CRMException {
        Table resTab = null;

        try {
            // get jco function
            JCO.Function function = connection.getJCoFunction("CRM_PCAT_IMS_IDXCAT_GETLIST");

            // set exporting parameter (parameter does not exist in all releases)
            JCO.ParameterList impList = function.getImportParameterList();

            if ((impList != null) && impList.isImport("IV_NAME_PATTERN") && (catPattern != null)) {
                impList.setValue(catPattern, "IV_NAME_PATTERN");
            }

            CatalogJCoLogHelper.logCall(function.getName(), function.getImportParameterList(), null,
                                        function.getTableParameterList(), log);

            connection.execute(function);

            CatalogJCoLogHelper.logCall(function.getName(), null, function.getExportParameterList(),
                                        function.getTableParameterList(), log);

            // get table parameters
            JCO.ParameterList tabList = function.getTableParameterList();

            // analyse the fixed values of the attributes
            JCO.Table idxTab = tabList.getTable("IDXATTR");

            // build a generic result table
            resTab = new Table("IDXATTR");
            resTab.addColumn(Table.TYPE_STRING, "PCAT");
            resTab.addColumn(Table.TYPE_STRING, "PCATVARIANT");
            resTab.addColumn(Table.TYPE_STRING, "AREA");
            resTab.addColumn(Table.TYPE_STRING, "ATTRLEVEL");
            resTab.addColumn(Table.TYPE_STRING, "IDXCATEGORY");

            if (idxTab.getNumRows() != 0) {
                idxTab.firstRow();

                do {
                    TableRow resRow = resTab.insertRow();
                    resRow.getField(1).setValue(idxTab.getString("PCAT"));
                    resRow.getField(2).setValue(idxTab.getString("PCATVARIANT"));
                    resRow.getField(3).setValue(idxTab.getString("AREA"));
                    resRow.getField(4).setValue(idxTab.getString("ATTRLEVEL"));
                    resRow.getField(5).setValue(idxTab.getString("IDXCATEGORY"));
                }
                while (idxTab.nextRow());
            }
        }
        catch (BackendException ex) {
            throw new CRMException(ex.getLocalizedMessage(), ex);
        }
        catch (JCO.AbapException ex) {
            throw new CRMException(ex.getLocalizedMessage(), ex);
        }
        finally {
            if (connection != null) {
                connection.close();
            }

            // end of if ()
        }

        return new ResultData(resTab);
    }

    /**
     * Gets the information about the IMS connection for a given catalog instance.
     *
     * @param catalogInfo catalog for which the IMS server should be retrieved
     * @param server DOCUMENT ME!
     *
     * @return an information about the IMS server
     *
     * @exception CRMException connection error occured
     */
    static IMSConnectionInfo getIMSConnectionInfo(CRMCatalogServerEngine.CRMCatalogInfo catalogInfo,
                                                  CRMCatalogServerEngine                server)
                                           throws CRMException {
        IMSConnectionInfo connectionInfo = null;
        JCoConnection connection = server.getDefaultJCoConnection();

        try {
            // get index category ID
            String catId = getCatIdForLevel(connection, catalogInfo.getName(), catalogInfo.getVariant(),
                                            IMSConstants.INDEX_LEVEL_A, "", server.getMessageResources());

            // get jco function
            JCO.Function function = connection.getJCoFunction("SRET_SEARCH_RFC_INFO_GET");

            // set exporting parameters
            JCO.ParameterList impList = function.getImportParameterList();
            impList.setValue(catId, "CATID");

            CatalogJCoLogHelper.logCall(function.getName(), function.getImportParameterList(), null,
                                        function.getTableParameterList(), log);

            connection.execute(function);

            CatalogJCoLogHelper.logCall(function.getName(), null, function.getExportParameterList(),
                                        function.getTableParameterList(), log);

            // get importing parameters
            JCO.ParameterList expList = function.getExportParameterList();

            CatalogJCoLogHelper.logCall("SRET_SEARCH_RFC_INFO_GET", impList, expList, null, log);

            String gwHost = expList.getString("GWHOST");
            String gwServ = expList.getString("GWSERVICE");
            String tpHost = expList.getString("SERVER");
            String tpName = expList.getString("PROGRAM");
            log.debug("getIMSConnectionInfo called SRET_SEARCH_RFC_INFO_GET for index " + catId +
                      " and got following info:" + " gwHost=" + gwHost + " gwServ=" + gwServ + " tpHost=" + tpHost +
                      " tpName=" + tpName);

            // type of these parameters has changed in the meantime, so it has to be
            // checked first
            String codePage = null;
            String language = null;
            String searchEng = null;

            if (expList.getType("CODEPAGE") == IMetaData.TYPE_BYTE) {
                codePage = new String(expList.getByteArray("CODEPAGE")).trim();
            }
            else {
                codePage = expList.getString("CODEPAGE").trim();
            }

            if (expList.getType("XLAISO") == IMetaData.TYPE_BYTE) {
                language = new String(expList.getByteArray("XLAISO")).trim();
            }
            else {
                language = expList.getString("XLAISO");
            }

            if (expList.getType("SEARCHENG") == IMetaData.TYPE_BYTE) {
                searchEng = new String(expList.getByteArray("SEARCHENG")).trim();
            }
            else {
                searchEng = expList.getString("SEARCHENG");
            }

            // set catalogInfo relevant data
            catalogInfo.setCodepage(codePage);
            catalogInfo.setLanguage(language);
            catalogInfo.setSearchEngine(searchEng);

            connectionInfo = new IMSConnectionInfo(server, server.getProperties(), gwHost, gwServ, tpHost, tpName);
        }
        catch (BackendException ex) {
            throw new CRMException(ex.getLocalizedMessage(), ex);
        }
        catch (JCO.AbapException ex) {
            throw new CRMException(ex.getLocalizedMessage(), ex);
        }
        finally {
            connection.close();
        }

        return connectionInfo;
    }

    /**
     * Helper method to get the category id for an index.
     *
     * @param connection connection to be used for retrieving the data
     * @param catName name of the catalog associated with the index
     * @param catVariant variant name of the catalog associated with the index
     * @param attrLevel index level to be read
     * @param area name of area (in case of 'S' index)
     * @param messResources texts to be used for the messages
     *
     * @return DOCUMENT ME!
     *
     * @exception BackendException connection error occured
     * @throws CRMException DOCUMENT ME!
     */
    private static String getCatIdForLevel(JCoConnection    connection,
                                           String           catName,
                                           String           catVariant,
                                           String           attrLevel,
                                           String           area,
                                           MessageResources messResources)
                                    throws BackendException, CRMException {
        log.debug("Get connection info for catalog variant (" + catName + ":" + catVariant + ":" + attrLevel + ":" +
                  area + ")");

        // get function
        final String FALLBACK_KEY = "fallback_to_SRET_IDXCAT_FIND_BY_QUERY";
        JCO.Function function = null;
        Object fallback = connection.getData(FALLBACK_KEY);

        if (fallback == null) {
            if (connection.isJCoFunctionAvailable("COM_PCAT_IDXCAT_FIND_BY_QUERY")) {
                fallback = Boolean.FALSE;
            }
            else {
                fallback = Boolean.TRUE;
            }

            connection.setData(FALLBACK_KEY, fallback);
        }

        if (Boolean.TRUE.equals(fallback)) {
            function = connection.getJCoFunction("SRET_IDXCAT_FIND_BY_QUERY");
        }
        else {
            function = connection.getJCoFunction("COM_PCAT_IDXCAT_FIND_BY_QUERY");
        }

        // set exporting parameters
        JCO.ParameterList impList = function.getImportParameterList();
        impList.setValue("PCAT", "APPLICATIONKEY");

        JCO.Table attrTab = function.getTableParameterList().getTable("CATTATTRIBUTTAB");

        attrTab.appendRow();
        attrTab.setValue("CATALOG", "ATTRNAME");
        attrTab.setValue(catName, "ATTRVAL");
        attrTab.appendRow();
        attrTab.setValue("VARIANT", "ATTRNAME");
        attrTab.setValue(catVariant, "ATTRVAL");
        attrTab.appendRow();
        attrTab.setValue("ATTRIBLEVEL", "ATTRNAME");
        attrTab.setValue(attrLevel, "ATTRVAL");

        if (attrLevel.equals(IMSConstants.INDEX_LEVEL_S)) {
            if (area == null) {
                String msg = messResources.getMessage(ICatalogMessagesKeys.PCAT_IMS_INVALID_LEVELINFO, attrLevel);
                throw new BackendException(msg);
            }

            attrTab.appendRow();
            attrTab.setValue("AREA", "ATTRNAME");
            attrTab.setValue(area, "ATTRVAL");
        }

        try {
            CatalogJCoLogHelper.logCall(function.getName(), function.getImportParameterList(), null,
                                        function.getTableParameterList(), log);

            connection.execute(function);

            CatalogJCoLogHelper.logCall(function.getName(), null, function.getExportParameterList(),
                                        function.getTableParameterList(), log);
        }
        catch (JCO.AbapException ex) {
            String msg = "getCatIdForLevel(catName=" + catName + ",catVariant=" + catVariant + ",attribLevel=" +
                         attrLevel + ",area=" + area + ") failed with JOC.AbapException '" + ex.getLocalizedMessage() +
                         "'";
            log.error(ICatalogMessagesKeys.PCAT_EXCEPTION, new Object[] { msg }, null);
            throw new CRMException(ex.getLocalizedMessage());
        }

        // get importing parameters
        JCO.Table catIdList = function.getTableParameterList().getTable("CATIDLIST");
        int rows = catIdList.getNumRows();

        if (rows > 1) {
            String msg = messResources.getMessage(ICatalogMessagesKeys.PCAT_IMS_RET_2MANYCATS);
            throw new BackendException(msg);
        }
        else if (rows < 1) {
            String msg = messResources.getMessage(ICatalogMessagesKeys.PCAT_IMS_RET_NOCAT);
            throw new BackendException(msg);
        }

        return catIdList.getString("CATID");
    }

    /**
     * Gets the time stamp when the last replication of the given catalog was performed.
     *
     * @param connection connection to be used for retrieving the data
     * @param catName the catalog name
     * @param catVariant the variant name of the catalog
     *
     * @return the time stamp of the last replication
     *
     * @exception CRMException connection error occured
     */
    static String getIMSReplTimeStamp(JCoConnection connection,
                                      String        catName,
                                      String        catVariant)
                               throws CRMException {
        String timeStamp = null;

        try {
            // get jco function
            JCO.Function function = connection.getJCoFunction("COM_PCAT_IMS_GET_LASTREP");

            // set exporting parameters
            JCO.ParameterList impList = function.getImportParameterList();
            impList.setValue(catName, "IV_CATALOGID");
            impList.setValue(catVariant, "IV_VARIANTID");

            CatalogJCoLogHelper.logCall(function.getName(), function.getImportParameterList(), null,
                                        function.getTableParameterList(), log);

            connection.execute(function);

            CatalogJCoLogHelper.logCall(function.getName(), null, function.getExportParameterList(),
                                        function.getTableParameterList(), log);

            // get importing parameters
            JCO.ParameterList expList = function.getExportParameterList();
            timeStamp = expList.getString("EV_LASTREP").trim();
        }
        catch (BackendException ex) {
            throw new CRMException(ex.getLocalizedMessage(), ex);
        }
        catch (JCO.AbapException ex) {
            throw new CRMException(ex.getLocalizedMessage(), ex);
        }
        finally {
            connection.close();
        }

        return timeStamp;
    }

    // copied from IMSRfcMethods.java

    /**
     * Gets the pricing meta information. Unfortunately the meta infos are stored in the CRM system and not on the IMS
     * server.
     *
     * @param connection connection to be used for retrieving the data
     * @param catName the catalog name
     * @param catVariant the variant name of the catalog
     *
     * @return the pricing information from the CRM system
     *
     * @exception CRMException connection error occured
     */
    static ResultData getPricingMetaInfo(JCoConnection connection,
                                         String        catName,
                                         String        catVariant)
                                  throws CRMException {
        Table resTab = null;

        try {
            // get jco function
            JCO.Function function = connection.getJCoFunction("CRM_ISA_PRICESETTINGS_READ");

            // set exporting parameters
            JCO.ParameterList impList = function.getImportParameterList();
            impList.setValue(catName, "CATALOG");
            impList.setValue(catVariant, "VARIANT");
            //impList.setValue("X", "LISTPRICE_ONLY");
            impList.setValue(" ", "LISTPRICE_ONLY");

            CatalogJCoLogHelper.logCall(function.getName(), function.getImportParameterList(), null,
                                        function.getTableParameterList(), log);

            connection.execute(function);

            CatalogJCoLogHelper.logCall(function.getName(), null, function.getExportParameterList(),
                                        function.getTableParameterList(), log);

            // get table parameters
            JCO.ParameterList tabList = function.getTableParameterList();

            // analyse the fixed values of the attributes
            JCO.Table attrTab = tabList.getTable("PRICEATTRNAMES");

            // build a generic result table
            resTab = new Table("PRICEATTRNAMES");
            resTab.addColumn(Table.TYPE_STRING, "ATTRNAME_PRICE");
            resTab.addColumn(Table.TYPE_STRING, "ATTRNAME_QUANTITY");
            resTab.addColumn(Table.TYPE_STRING, "ATTRNAME_CURRENCY");
            resTab.addColumn(Table.TYPE_STRING, "ATTRNAME_UOM");
            resTab.addColumn(Table.TYPE_STRING, "ATTRNAME_SCALETYPE");
            resTab.addColumn(Table.TYPE_STRING, "ATTRIBUTE_PRICETYPE");
            resTab.addColumn(Table.TYPE_STRING, "ATTRIBUTE_PROMOTION_ATTR");

            if (attrTab.getNumRows() != 0) {
                attrTab.firstRow();

                do {
                    TableRow resRow = resTab.insertRow();
                    resRow.getField(1).setValue(attrTab.getString("ATTRNAME_PRICE"));
                    resRow.getField(2).setValue(attrTab.getString("ATTRNAME_QUANTITY"));
                    resRow.getField(3).setValue(attrTab.getString("ATTRNAME_CURRENCY"));
                    resRow.getField(4).setValue(attrTab.getString("ATTRNAME_UOM"));
                    resRow.getField(5).setValue(attrTab.getString("ATTRNAME_SCALETYPE"));
                    resRow.getField(6).setValue(attrTab.getString("PRCTYPE"));
                    resRow.getField(7).setValue(attrTab.getString("PROMOTION_ATTR"));
                }
                while (attrTab.nextRow());
            }
        }
        catch (BackendException ex) {
            throw new CRMException(ex.getLocalizedMessage());
        }
        catch (JCO.AbapException ex) {
            throw new CRMException(ex.getLocalizedMessage());
        }
        finally {
            connection.close();
        }

        return new ResultData(resTab);
    }

    /**
     * Gets the detailed information about an index.
     *
     * @param connection connection to be used for retrieving the data
     * @param catName name of the catalog associated with the index
     * @param catVariant variant name of the catalog associated with the index
     * @param catLanguage language of the catalog associated with the index
     * @param attrLevel index level to be read
     * @param area name of area (in case of 'S' index)
     * @param guid guid of the catalog (if 'P' index) or of the area (if 'S' index)
     * @param messResources texts to be used for the messages
     *
     * @return an information about the index
     *
     * @exception CRMException connection error occured
     *
     * @deprecated please use getIMSIndexInfo(JCoConnection connection, String catName, String catVariant, String
     *             catLanguage, String attrLevel, String area, String guid, MessageResources messResources, Catalog
     *             catalog)
     */
    static IMSIndexInfo getIMSIndexInfo(JCoConnection    connection,
                                        String           catName,
                                        String           catVariant,
                                        String           catLanguage,
                                        String           attrLevel,
                                        String           area,
                                        String           guid,
                                        MessageResources messResources)
                                 throws CRMException {
        IMSIndexInfo indexInfo = null;

        try {
            // get index category ID
            String catId = getCatIdForLevel(connection, catName, catVariant, attrLevel, area, messResources);

            // get jco function
            JCO.Function function = connection.getJCoFunction("SRET_SEARCH_RFC_INFO_GET");

            // set exporting parameters
            JCO.ParameterList impList = function.getImportParameterList();
            impList.setValue(catId, "CATID");

            CatalogJCoLogHelper.logCall(function.getName(), function.getImportParameterList(), null,
                                        function.getTableParameterList(), log);

            connection.execute(function);

            CatalogJCoLogHelper.logCall(function.getName(), null, function.getExportParameterList(),
                                        function.getTableParameterList(), log);

            // get importing parameters
            JCO.ParameterList expList = function.getExportParameterList();
            String idStoreId = null;

            if (expList.getType("IDSTORID") == IMetaData.TYPE_BYTE) {
                idStoreId = new String(expList.getByteArray("IDSTORID")).trim();
            }
            else {
                idStoreId = expList.getString("IDSTORID");
            }

            // create index info
            indexInfo = new IMSIndexInfo(catId, attrLevel, area, idStoreId);

            JCO.Table tab = function.getTableParameterList().getTable("DOCATTRTAB");

            if (tab.getNumRows() != 0) {
                tab.firstRow();

                do {
                    String attrName = null;

                    if (tab.getType("ATTRNAME") == IMetaData.TYPE_BYTE) {
                        attrName = new String(tab.getByteArray("ATTRNAME")).trim();
                    }
                    else {
                        attrName = tab.getString("ATTRNAME");
                    }

                    indexInfo.addAttribute(attrName);
                }
                while (tab.nextRow());
            }

            // create the attribute meta information for the info object
            getAttributeMetaInfo(connection, indexInfo, guid, catLanguage);
        }
        catch (BackendException ex) {
            throw new CRMException(ex.getLocalizedMessage());
        }
        catch (JCO.AbapException ex) {
            throw new CRMException(ex.getLocalizedMessage());
        }
        finally {
            connection.close();
        }

        return indexInfo;
    }

    /**
     * Gets the information about an index for a catalog which was selected in the IMSAdminConsole.
     *
     * @param console console in which the catalog was selected
     * @param catName name of the catalog associated with the index
     * @param catVariant variant name of the catalog associated with the index
     * @param attrLevel index level to be read
     * @param areaName name of area (in case of 'S' index)
     * @param catId DOCUMENT ME!
     *
     * @exception CRMException connection error occured
     */
    static void getIMSInfos(IMSAdminConsoleImpl console,
                            String              catName,
                            String              catVariant,
                            String              attrLevel,
                            String              areaName,
                            String              catId)
                     throws CRMException {
        JCoConnection connection = console.getDefaultJCoConnection();

        try {
            if ((catId == null) || (catId.length() == 0)) {
                // get index category ID
                catId = getCatIdForLevel(connection, catName, catVariant, attrLevel, areaName,
                                         console.getMessageResources());
            }

            // get jco function
            JCO.Function function = connection.getJCoFunction("SRET_SEARCH_RFC_INFO_GET");

            // set exporting parameters
            JCO.ParameterList impList = function.getImportParameterList();
            impList.setValue(catId, "CATID");

            CatalogJCoLogHelper.logCall(function.getName(), function.getImportParameterList(), null,
                                        function.getTableParameterList(), log);

            connection.execute(function);

            CatalogJCoLogHelper.logCall(function.getName(), null, function.getExportParameterList(),
                                        function.getTableParameterList(), log);

            // get importing parameters
            JCO.ParameterList expList = function.getExportParameterList();

            String gwHost = expList.getString("GWHOST");
            String gwServ = expList.getString("GWSERVICE");
            String tpHost = expList.getString("SERVER");
            String tpName = expList.getString("PROGRAM");

            // type of these parameters has changed in the meantime, so it has to be
            // checked first
            String idStoreId = null;
            String codePage = null;
            String language = null;
            String searchEng = null;

            if (expList.getType("IDSTORID") == IMetaData.TYPE_BYTE) {
                idStoreId = new String(expList.getByteArray("IDSTORID")).trim();
            }
            else {
                idStoreId = expList.getString("IDSTORID");
            }

            if (expList.getType("CODEPAGE") == IMetaData.TYPE_BYTE) {
                codePage = new String(expList.getByteArray("CODEPAGE")).trim();
            }
            else {
                codePage = expList.getString("CODEPAGE").trim();
            }

            if (expList.getType("XLAISO") == IMetaData.TYPE_BYTE) {
                language = new String(expList.getByteArray("XLAISO")).trim();
            }
            else {
                language = expList.getString("XLAISO");
            }

            if (expList.getType("SEARCHENG") == IMetaData.TYPE_BYTE) {
                searchEng = new String(expList.getByteArray("SEARCHENG")).trim();
            }
            else {
                searchEng = expList.getString("SEARCHENG");
            }

            console.setCodepage(codePage);
            console.setLanguage(language);
            console.setSearchEngine(searchEng);

            // create Index information
            IMSIndexInfo indexInfo = new IMSIndexInfo(catId, attrLevel, areaName, idStoreId);
            JCO.Table tab = function.getTableParameterList().getTable("DOCATTRTAB");

            if (tab.getNumRows() != 0) {
                tab.firstRow();

                do {
                    String attrName = null;

                    if (tab.getType("ATTRNAME") == IMetaData.TYPE_BYTE) {
                        attrName = new String(tab.getByteArray("ATTRNAME")).trim();
                    }
                    else {
                        attrName = tab.getString("ATTRNAME");
                    }

                    indexInfo.addAttribute(attrName);
                }
                while (tab.nextRow());
            }

            // set the index as the current index in the console
            console.setIndexInfo(indexInfo);

            // set the connection info as the currend IMS connection in the console
            IMSConnectionInfo connectionInfo = new IMSConnectionInfo(console, console.getConnectionProperties(),
                                                                     gwHost, gwServ, tpHost, tpName);
            console.setConnectionInfo(connectionInfo);
        }
        catch (BackendException ex) {
            throw new CRMException(ex.getLocalizedMessage());
        }
        catch (JCO.AbapException ex) {
            throw new CRMException(ex.getLocalizedMessage());
        }
        finally {
            connection.close();
        }
    }

    /**
     * Helper method to get the meta information for the attributes. Unfortunately the meta infos are stored in the CRM
     * system and not on the IMS server.
     *
     * @param connection connection to be used for retrieving the data
     * @param info the index info object of the attributes
     * @param guid guid of the catalog (if 'P' index) or of the area (if 'S' index)
     * @param language the language for the language dependant texts
     *
     * @exception BackendException connection error occured
     */
    private static void getAttributeMetaInfo(JCoConnection connection,
                                             IMSIndexInfo  info,
                                             String        guid,
                                             String        language)
                                      throws BackendException {
        // currently only supported for the 'S' and 'P' index
        if (info.getAttrLevel().equals(IMSConstants.INDEX_LEVEL_A)) {
            return;
        }

        // get jco function
        JCO.Function function = connection.getJCoFunction("COM_PCAT_LOC_GETENTRYMASK_O");

        // set exporting parameters
        JCO.ParameterList impList = function.getImportParameterList();

        if (info.getAttrLevel().equals(IMSConstants.INDEX_LEVEL_P)) {
            // check first if the release support this behavoir
            if (!impList.isImport("IV_CATALOG_ID")) {
                return;
            }

            // then set the value - use id instead of guid
            impList.setValue(guid, "IV_CATALOG_ID");
            impList.setValue(language, "IV_LANGUAGE");
        }
        else if (info.getAttrLevel().equals(IMSConstants.INDEX_LEVEL_S)) {
            impList.setValue(guid, "IV_AREA_GUID");
            impList.setValue(language, "IV_LANGUAGE");
        }

        impList.setValue(" ", "IV_VERSIONTYPE"); // don't bother active/non-active version

        CatalogJCoLogHelper.logCall(function.getName(), function.getImportParameterList(), null,
                                    function.getTableParameterList(), log);

        connection.execute(function);

        CatalogJCoLogHelper.logCall(function.getName(), null, function.getExportParameterList(),
                                    function.getTableParameterList(), log);

        // get table parameters
        JCO.ParameterList tabList = function.getTableParameterList();

        // first check if error occured
        JCO.Table returnInfo = tabList.getTable("ET_RETURN");

        if (returnInfo.getNumRows() != 0) {
            // iterate over the return codes of the function call
            returnInfo.firstRow();

            do {
                String messType = returnInfo.getString("TYPE").trim();

                if (messType.equalsIgnoreCase("E") || messType.equalsIgnoreCase("A")) {
                    log.error(ICatalogMessagesKeys.PCAT_EXCEPTION, new Object[] { returnInfo.getString("MESSAGE") },
                              null);
                    throw new BackendException(returnInfo.getString("MESSAGE").trim());
                }
            }
            while (returnInfo.nextRow());
        }

        // analyse the meta information of the attributes
        Map guidMap = new HashMap();
        JCO.Table metaTab = null;

        if (info.getAttrLevel().equals(IMSConstants.INDEX_LEVEL_P)) {
            metaTab = tabList.getTable("ET_BASE_CIC");
        }
        else {
            metaTab = tabList.getTable("ET_AREA_CIC");
        }

        if (metaTab.getNumRows() != 0) {
            metaTab.firstRow();

            do {
                String attrName = metaTab.getString("CIC_ID").trim();
                String attrGuid = metaTab.getString("CIC_GUID").trim();
                String attrDesc = metaTab.getString("CIC_NAME").trim();
                String attrStyle = metaTab.getString("CIC_ENTRY_STYLE").trim();
                String attrPos = metaTab.getString("CIC_NR").trim();
                String attrType = metaTab.getString("DATA_TYPE").trim();
                info.addAttributeProperty(attrName, IMSIndexInfo.PROP_GUID, attrGuid);
                info.addAttributeProperty(attrName, IMSIndexInfo.PROP_DESCRIPTION, attrDesc);
                info.addAttributeProperty(attrName, IMSIndexInfo.PROP_ENTRYSTYLE, attrStyle);
                info.addAttributeProperty(attrName, IMSIndexInfo.PROP_POS_NR, attrPos);
                info.addAttributeProperty(attrName, IMSIndexInfo.PROP_TYPE, attrType);
                guidMap.put(attrGuid, attrName);
            }
            while (metaTab.nextRow());
        }

        // analyse the fixed values of the attributes
        JCO.Table valueTab = tabList.getTable("ET_CIC_PROP");

        if (valueTab.getNumRows() != 0) {
            valueTab.firstRow();

            do {
                String attrGuid = valueTab.getString("CIC_GUID").trim();
                String attrName = (String) guidMap.get(attrGuid);

                // check if this attribute really exists
                if ((attrName == null) || (attrName.length() == 0)) {
                    continue;
                }

                // language dependent value of the fixed value for this attribute
                String attrValueDesc = valueTab.getString("ATTR_VAL_TEXT").trim();

                // technical value of the fixed value for this attribute
                String attrValueName = valueTab.getString("ATTR_VAL_LOW").trim();
                info.addFixedValue(attrName, attrValueName, attrValueDesc);
            }
            while (valueTab.nextRow());
        }
    }

    /**
     * Gets a list of indices out of CRM for a given catalog instance  Necessary for distributed "index
     * server"-landscape in order to get actual indices after replication to the web.
     *
     * @param connection connection to be used for retrieving the data
     * @param catalog the Catalog Object
     *
     * @return a list of indices (GUID�s) of the actual Catalog Object
     *
     * @exception CRMException connection error occured
     */
    static String[] getListOfIndexGuids(JCoConnection connection,
                                        CRMCatalog    catalog)
                                 throws CRMException {
        String result[] = null;
        String name = catalog.getName();
        String variant = catalog.getVariant();

        log.debug("'getListOfIndexGuids' for catalog_variant (" + name + variant + ")");

        try {
            // get jco function via a JCo client connection
            JCO.Function functionFirst = connection.getJCoFunction("COM_PCAT_IDXCAT_FIND_BY_QUERY");

            //  get field parameter (import for backend)
            JCO.ParameterList impListFirst = functionFirst.getImportParameterList();
            impListFirst.setValue("PCAT", "APPLICATIONKEY");

            //  get table parameter 
            JCO.Table attrTab = functionFirst.getTableParameterList().getTable("CATTATTRIBUTTAB");
            attrTab.appendRow();
            attrTab.setValue("CATALOG", "ATTRNAME");
            attrTab.setValue(name, "ATTRVAL");
            attrTab.appendRow();
            attrTab.setValue("VARIANT", "ATTRNAME");
            attrTab.setValue(variant, "ATTRVAL");

            CatalogJCoLogHelper.logCall(functionFirst.getName(), functionFirst.getImportParameterList(), null,
                                        functionFirst.getTableParameterList(), log);

            connection.execute(functionFirst);

            CatalogJCoLogHelper.logCall(functionFirst.getName(), null, functionFirst.getExportParameterList(),
                                        functionFirst.getTableParameterList(), log);

            // get table parameters (export of backend) in order to accept result
            JCO.ParameterList tabList = functionFirst.getTableParameterList();

            // read specified field from result table into JCO Table
            JCO.Table catIdList = tabList.getTable("CATIDLIST");

            // build a result Array out of JCOTable
            String tempResult[] = new String[catIdList.getNumRows()];

            if (catIdList.getNumRows() != 0) {
                catIdList.firstRow();

                int i = 0;

                do {
                    String attrGuid = catIdList.getString("CATID").trim();
                    tempResult[i] = attrGuid;
                    i++;
                }
                while (catIdList.nextRow());
            }

            //if  
            JCO.Function functionSecond = connection.getJCoFunction("SRET_SEARCH_RFC_INFO_GET");

            // get import and export params 
            JCO.ParameterList impListSecond = functionSecond.getImportParameterList();
            JCO.ParameterList paraList = functionSecond.getExportParameterList();

            result = new String[tempResult.length];

            for (int j = 0; j < tempResult.length; j++) {
                impListSecond.setValue(tempResult[j], "CATID");
                CatalogJCoLogHelper.logCall(functionSecond.getName(), functionSecond.getImportParameterList(), null,
                                            functionSecond.getTableParameterList(), log);

                connection.execute(functionSecond);

                CatalogJCoLogHelper.logCall(functionSecond.getName(), null, functionSecond.getExportParameterList(),
                                            functionSecond.getTableParameterList(), log);

                // accepting result
                String storid = paraList.getString("IDSTORID");
                result[j] = storid;
            }

            if (tempResult.length >= 0) {
                log.debug("'getListOfIndexGuids' returns following " + result.length + " entries: ");

                for (int i = 0; i < result.length; i++) {
                    log.debug(result[i]);
                }
            }
        }

        //try  
        catch (BackendException ex) {
            String msg = "getListOfIndexGuids(catName=" + catalog.getName() + ". catVariant=" + catalog.getVariant() +
                         ") failed with BackendException '" + ex.getLocalizedMessage() + "'";
            log.error(ICatalogMessagesKeys.PCAT_EXCEPTION, new Object[] { ex.getLocalizedMessage() }, null);
            throw new CRMException(ex.getLocalizedMessage(), ex);
        }
        catch (JCO.AbapException ex) {
            throw new CRMException(ex.getLocalizedMessage(), ex);
        }
        finally {
            if (connection != null) {
                connection.close();
            }
        }

        return result;
    }

    /**
     * Gets the list of all indices of a CRM system.
     *
     * @param connection connection to be used for retrieving the data
     *
     * @return the list of indices
     *
     * @exception CRMException connection error occured
     */
    static ResultData getCatalogueList(JCoConnection connection, String language, boolean withNames)
                                throws CRMException {
        Table resTab = null;

        try {
            // get jco function
            JCO.Function function = connection.getJCoFunction("CRM_ISA_PCAT_GETLIST");

            // set exporting parameter (parameter does not exist in all releases)
            JCO.ParameterList impList = function.getImportParameterList();

            if (impList != null) {
                if (language != null) {
                    impList.setValue(language, "IV_LANGUAGE");
                }
                if (withNames) {
                    impList.setValue("X", "IV_WITHNAME");
                }
            }

            CatalogJCoLogHelper.logCall(function.getName(), function.getImportParameterList(), null,
                                        function.getTableParameterList(), log);

            connection.execute(function);

            CatalogJCoLogHelper.logCall(function.getName(), null, function.getExportParameterList(),
                                        function.getTableParameterList(), log);

            // get table parameters
            JCO.ParameterList expList = function.getExportParameterList();

            // analyse the fixed values of the attributes
            JCO.Table catTab = expList.getTable("ET_CATALOGDETAIL");

            // build a generic result table
            resTab = new Table("ET_CATALOGDETAIL");
            
            resTab.addColumn(Table.TYPE_STRING, "GUID");    // COMT_PCAT_GUID  RAW 16
            resTab.addColumn(Table.TYPE_STRING, "ID");  // COMT_PCAT_ID    CHAR    30
            resTab.addColumn(Table.TYPE_STRING, "PCAT_TYPE");   // COMT_PCAT_TYPE  CHAR    4
            resTab.addColumn(Table.TYPE_STRING, "VALID_FROM");  // COMT_PCAT_VALID_FROM    DATS    8
            resTab.addColumn(Table.TYPE_STRING, "VALID_TO");    // COMT_PCAT_VALID_TO  DATS    8
            resTab.addColumn(Table.TYPE_STRING, "BASE_SET");    // COMT_PCAT_BASESETTYPE_GUID  RAW 16
            resTab.addColumn(Table.TYPE_STRING, "BASE_LOC");    // COMT_PCAT_LOC_GUID  RAW 16
            resTab.addColumn(Table.TYPE_STRING, "PCAT_SHORTCUT");   // COMT_PCAT_SC    CHAR    4
            resTab.addColumn(Table.TYPE_STRING, "ALLOW_CVIEWS");   // COMT_PCAT_ALLOW_CVIEWS  CHAR    1
            resTab.addColumn(Table.TYPE_STRING, "ALLOW_SHARED");    // COMT_PCAT_ALLOW_SHARED  CHAR    1
            resTab.addColumn(Table.TYPE_STRING, "PCAT_ITM_ASSIGN"); // COMT_PCAT_ITM_ASSIGN    CHAR    1
            resTab.addColumn(Table.TYPE_STRING, "ITEM_TYPE");   // COMT_PCAT_ITEM_TYPE CHAR    10
            resTab.addColumn(Table.TYPE_STRING, "CTY_PROCEDURE");   // COMT_PCAT_CTY_PROC  CHAR    8
            resTab.addColumn(Table.TYPE_STRING, "ITM_PROCEDURE");   // COMT_PCAT_ITM_PROC  CHAR    8
            resTab.addColumn(Table.TYPE_STRING, "AREA_DOC_TMPL");   // COMT_PCAT_AREA_DOC_TEMPLATE CHAR    64
            resTab.addColumn(Table.TYPE_STRING, "ITEM_DOC_TMPL");   // COMT_PCAT_ITEM_DOC_TEMPLATE CHAR    64
            resTab.addColumn(Table.TYPE_STRING, "PON_USE"); // COMT_PCAT_PON_USE   CHAR    1
            resTab.addColumn(Table.TYPE_STRING, "PON_FORMAT");  // COMT_PCAT_PON_FORMAT    CHAR    2
            resTab.addColumn(Table.TYPE_STRING, "PON_SEPERATOR1");  // COMT_PCAT_PON_SEPERATOR CHAR    1
            resTab.addColumn(Table.TYPE_STRING, "PON_SEPERATOR2");  // COMT_PCAT_PON_SEPERATOR CHAR    1
            resTab.addColumn(Table.TYPE_STRING, "CAMP_NOT_IN_PON"); // COMT_PCAT_CAMPAIGN_NOT_IN_PON   CHAR    1
            resTab.addColumn(Table.TYPE_STRING, "PCAT_USAGE");  // COMT_PCAT_USAGE CHAR    1
            resTab.addColumn(Table.TYPE_STRING, "PRODUCT_TYPES");   // COMT_PCAT_ALLOWED_PROD_TYPES    CHAR    10
            resTab.addColumn(Table.TYPE_STRING, "REPL_CATHIER");    // COMT_PCAT_CATHIER_USAGE CHAR    1
            resTab.addColumn(Table.TYPE_STRING, "ACTIVE");  // COMT_ACTIVE_FLAG    CHAR    1
            resTab.addColumn(Table.TYPE_STRING, "ORIG_SYSTEM"); // COMT_ORIG_SYSTEM    CHAR    10
            resTab.addColumn(Table.TYPE_STRING, "CREATED_BY");  // COMT_CREATOR    CHAR    12
            resTab.addColumn(Table.TYPE_STRING, "CREATED_ON");  // COMT_CREATIONDATE   DATS    8
            resTab.addColumn(Table.TYPE_STRING, "LASTCHG_BY");  // COMT_LASTCHG_NAME   CHAR    12
            resTab.addColumn(Table.TYPE_STRING, "LASTCHG_ON");  // COMT_LASTCHG_DATE   DATS    8
            resTab.addColumn(Table.TYPE_STRING, "LASTCHG_AT");  // COMT_LASTCHG_TIME   TIMS    6
            
            if (catTab.getNumRows() != 0) {
                catTab.firstRow();

                do {
                    TableRow resRow = resTab.insertRow();
                    
                    String guid = GuidConversionUtil.convertToTechKey(catTab.getByteArray("GUID")).getIdAsString();
                    resRow.getField("GUID").setValue(guid);
                    
                    resRow.getField("ID").setValue(catTab.getString("ID"));
                    resRow.getField("PCAT_TYPE").setValue(catTab.getString("PCAT_TYPE"));
                    resRow.getField("VALID_FROM").setValue(catTab.getString("VALID_FROM"));
                    resRow.getField("VALID_TO").setValue(catTab.getString("VALID_TO"));
                    
                    guid = GuidConversionUtil.convertToTechKey(catTab.getByteArray("BASE_SET")).getIdAsString();
                    resRow.getField("BASE_SET").setValue(guid);
                    
                    resRow.getField("BASE_LOC").setValue(catTab.getString("BASE_LOC"));
                    resRow.getField("PCAT_SHORTCUT").setValue(catTab.getString("PCAT_SHORTCUT"));
                    resRow.getField("ALLOW_CVIEWS").setValue(catTab.getString("ALLOW_CVIEWS"));
                    resRow.getField("ALLOW_SHARED").setValue(catTab.getString("ALLOW_SHARED"));
                    resRow.getField("PCAT_ITM_ASSIGN").setValue(catTab.getString("PCAT_ITM_ASSIGN"));
                    resRow.getField("ITEM_TYPE").setValue(catTab.getString("ITEM_TYPE"));
                    resRow.getField("CTY_PROCEDURE").setValue(catTab.getString("CTY_PROCEDURE"));
                    resRow.getField("ITM_PROCEDURE").setValue(catTab.getString("ITM_PROCEDURE"));
                    resRow.getField("AREA_DOC_TMPL").setValue(catTab.getString("AREA_DOC_TMPL"));
                    resRow.getField("ITEM_DOC_TMPL").setValue(catTab.getString("ITEM_DOC_TMPL"));
                    resRow.getField("PON_USE").setValue(catTab.getString("PON_USE"));
                    resRow.getField("PON_FORMAT").setValue(catTab.getString("PON_FORMAT"));
                    resRow.getField("PON_SEPERATOR1").setValue(catTab.getString("PON_SEPERATOR1"));
                    resRow.getField("PON_SEPERATOR2").setValue(catTab.getString("PON_SEPERATOR2"));
                    resRow.getField("CAMP_NOT_IN_PON").setValue(catTab.getString("CAMP_NOT_IN_PON"));
                    resRow.getField("PCAT_USAGE").setValue(catTab.getString("PCAT_USAGE"));
                    resRow.getField("PRODUCT_TYPES").setValue(catTab.getString("PRODUCT_TYPES"));
                    resRow.getField("REPL_CATHIER").setValue(catTab.getString("REPL_CATHIER"));
                    resRow.getField("ACTIVE").setValue(catTab.getString("ACTIVE"));
                    resRow.getField("ORIG_SYSTEM").setValue(catTab.getString("ORIG_SYSTEM"));
                    resRow.getField("CREATED_BY").setValue(catTab.getString("CREATED_BY"));
                    resRow.getField("CREATED_ON").setValue(catTab.getString("CREATED_ON"));
                    resRow.getField("LASTCHG_BY").setValue(catTab.getString("LASTCHG_BY"));
                    resRow.getField("LASTCHG_ON").setValue(catTab.getString("LASTCHG_ON"));
                    resRow.getField("LASTCHG_AT").setValue(catTab.getString("LASTCHG_AT"));
                }
                while (catTab.nextRow());
            }
        }
        catch (BackendException ex) {
            throw new CRMException(ex.getLocalizedMessage(), ex);
        }
        catch (JCO.AbapException ex) {
            throw new CRMException(ex.getLocalizedMessage(), ex);
        }
        finally {
            if (connection != null) {
                connection.close();
            }
        }

        return new ResultData(resTab);
    }
}
