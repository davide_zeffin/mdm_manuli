/*****************************************************************************
  Copyright (c) 2001, SAPMarkets Europe GmbH, Germany, All rights reserved.
  Created:  05 Mar 2002

  $Id: //sap/ESALES_base/30_SP_COR/src/_pcatAPI/java/com/sap/isa/catalog/ims/CRMCatalog.java#4 $
  $Revision: #4 $
  $Change: 144291 $
  $Date: 2003/08/22 $
*****************************************************************************/
package com.sap.isa.catalog.ims;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Properties;

import org.apache.struts.util.MessageResources;
import org.w3c.dom.Document;

import com.sap.isa.catalog.ICatalogMessagesKeys;
import com.sap.isa.catalog.boi.CatalogException;
import com.sap.isa.catalog.boi.IClient;
import com.sap.isa.catalog.boi.IComponent;
import com.sap.isa.catalog.boi.IServer;
import com.sap.isa.catalog.boi.IServerEngine;
import com.sap.isa.catalog.boi.IView;
import com.sap.isa.catalog.impl.Catalog;
import com.sap.isa.catalog.impl.CatalogClient;
import com.sap.isa.catalog.impl.CatalogServerEngine;
import com.sap.isa.core.eai.BackendBusinessObjectMetaData;
import com.sap.isa.core.eai.Connection;
import com.sap.isa.core.eai.ConnectionDefinition;
import com.sap.isa.core.eai.sp.jco.JCoConnection;
import com.sap.isa.core.eai.sp.jco.JCoManagedConnectionFactory;
import com.sap.isa.core.logging.IsaLocation;

public class CRMCatalog
    extends Catalog
{
    // the logging instance
    private static IsaLocation theStaticLocToLog =
    IsaLocation.getInstance(CRMCatalog.class.getName());

        // IMS infos
    private String theLanguage;
    private String theCodepage;
    private String theSearchEngine;

        // Catalog infos
    private String   theVariant;
    private HashMap  theIndexInfo = new HashMap();
    private String   theReplTimeStamp = "";

    private IMSConnectionInfo theIMSConnectionInfo;
    private transient JCoConnection theSAPConnection;

        /**
         * Creates a new IMS catalog instance. The connections are managed by the
         * EAI framework. This constructor is called from outside of the <code>IMS</code>
         * package. Therefore it is defined as <code>public</code>.
         *
         * @exception CRMException  error from the IMS proxy
         * 
         * @deprecated don't use this catalog anymore, instead TrexCRMCatalog should be used. 
         *             This catalogue won't work with the the latest version of the replication.
         */
    protected CRMCatalog(CatalogServerEngine aServerEngine,
                      IMSConnectionInfo theIMSConnectionInfo,
                      IClient aClient,
                      String aCatalogGuid,
                      String aLanguage,
                      String aCodepage,
                      String aSearchEngine,
                      MessageResources aMessageResource)
        throws CRMException
    {
        super(aServerEngine,aCatalogGuid);
        
		theStaticLocToLog.entering("CRMCatalog(aServerEngine, theIMSConnectionInfo, aClient, aCatalogGuid, aLanguage, aCodepage, aSearchEngine, aMessageResource)");
        
        // create corresponding builder
        setCatalogBuilder(new IMSCatalogBuilder(this));
        this.theMessageResources = aMessageResource;
        
        this.theClient = new CatalogClient((IClient)aClient);

        this.setNameInternal(aCatalogGuid.substring(0, 29));
        this.theVariant = aCatalogGuid.substring(30);
        
        // change the default connection if necessary
        this.setDefaultConnection(theClient, theServerEngine);
        
        // initialize the connection info object for the IMS connection
        this.theIMSConnectionInfo = theIMSConnectionInfo;
        
		theLanguage = aLanguage;
		theCodepage = aCodepage;
		theSearchEngine = aSearchEngine;
        
        StringBuffer desc =
            new StringBuffer(getName()).append(" - ").append(getVariant());
        this.setDescriptionInternal(desc.toString());
        
        //set the time stamp
        this. theReplTimeStamp =
            ((CRMCatalogServerEngine)theServerEngine).getCurrentReplTimeStamp(this);
        
        if (theStaticLocToLog.isDebugEnabled()) {
            String aMessage =
                theMessageResources.getMessage(
                    ICatalogMessagesKeys.PCAT_INIT_CATALOG,
                    this.getClass().getName(),
                    this.getGuid(),
                    this.getClient().toString());
            theStaticLocToLog.debug(aMessage);
        } // end of if ()
        
		theStaticLocToLog.exiting();

        return ;
     }

        /**
         * Creates a new IMS catalog instance.
         * Allows the setting of the maximum number of items per category.
         * 
         * @deprecated don't use this catalog anymore, instead TrexCRMCatalog should be used. 
         *             This catalogue won't work with the the latest version of the replication.
         */
    protected CRMCatalog(CatalogServerEngine aServerEngine,
                      IMSConnectionInfo theIMSConnectionInfo,
                      IClient aClient,
                      String aCatalogGuid,
                      String aLanguage,
                      String aCodepage,
                      String aSearchEngine,
                      MessageResources aMessageResource,
                      int aMaxNumberOfItems)
        throws CRMException        
    {
        this(aServerEngine,
             theIMSConnectionInfo,
             aClient,aCatalogGuid,
             aLanguage,
             aCodepage,
             aSearchEngine,
             aMessageResource);
        this.theMaxNumberOfItemLeafs=aMaxNumberOfItems;
    }
    
        /******************************* CachableItem **************************/
    
    
        /**
         * Generates a key for the cache from the passed parameters.<br>
         * This method is called from the {@link com.sap.isa.catalog.cache.CatalogCache}
         * and has to be <code>public static</code>.
         *
         * @param aMetaInfo  meta information from the EAI layer
         * @param aClient    the client spec of the catalog
         * @param aServer    the server spec of the catalog
         */
    //JP: this method was copied from IMSCatalog.java
    public static String generateCacheKey(BackendBusinessObjectMetaData aMetaInfo,
                                          IClient aClient, IServer aServer) {

        ConnectionDefinition connDef = aMetaInfo.getDefaultConnectionDefinition();

            // create system id
            // special properties for the connection
        String system = null;
        Properties servProps = aServer.getProperties();
        if(servProps != null) {
            system = generateKeySAPSystem(
                servProps.getProperty(JCoManagedConnectionFactory.JCO_CLIENT),
                servProps.getProperty(JCoManagedConnectionFactory.JCO_R3NAME),
                servProps.getProperty(JCoManagedConnectionFactory.JCO_ASHOST),
                servProps.getProperty(JCoManagedConnectionFactory.JCO_SYSNR));
        }

            // default properties
        if(system == null) {
            Properties defProps = connDef.getProperties();
            system = generateKeySAPSystem(
                defProps.getProperty(JCoManagedConnectionFactory.JCO_CLIENT),
                defProps.getProperty(JCoManagedConnectionFactory.JCO_R3NAME),
                defProps.getProperty(JCoManagedConnectionFactory.JCO_ASHOST),
                defProps.getProperty(JCoManagedConnectionFactory.JCO_SYSNR));
        }

            // create views id
        StringBuffer views = new StringBuffer();
        List viewList = aClient.getViews();
        Iterator iter = viewList.iterator();
        while(iter.hasNext()) {
            IView view = (IView)iter.next();
            views.append(view.getGuid());
        }

            // now build and return the key
        return aServer.getCatalogGuid() + views.toString() + system;
    }

	/**
         * Generates a key for the cache from the passed parameters.<br>
         * This method is called from the {@link com.sap.isa.catalog.cache.CatalogCache}
         * and has to be <code>public static</code>.
         *
         * @param aMetaInfo  meta information from the EAI layer
         * @param aClient    the client spec of the catalog
         * @param aServer    the server spec of the catalog
         */
    public static String generateCacheKey(
        String className,
        IClient aClient,
        IServerEngine aServer)
    {
            // create system id
            // special properties for the connection
        String system = null;
        Properties servProps = aServer.getProperties();
        system = generateKeySAPSystem(
            servProps.getProperty(JCoManagedConnectionFactory.JCO_CLIENT),
            servProps.getProperty(JCoManagedConnectionFactory.JCO_R3NAME),
            servProps.getProperty(JCoManagedConnectionFactory.JCO_ASHOST),
            servProps.getProperty(JCoManagedConnectionFactory.JCO_SYSNR));

            // create views id
        StringBuffer views = new StringBuffer();
        List viewList = aClient.getViews();
        Iterator iter = viewList.iterator();
        while(iter.hasNext())
        {
            IView view = (IView)iter.next();
            views.append(view.getGuid());
        }

            // now build and return the key
        return aServer.getGuid() + aServer.getCatalogGuid() + views.toString() + system;
    }

        /**
         * Generates a key from the passed parameters for a SAP system. If the parameters
         * are incomplete so that no valid key can be built <code>null</code> will be
         * returned.
         *
         * @param client  the client of the r/3 system
         * @param r3name  the name of the r/3 system
         * @param ashost  the application server
         * @param sysnr   the system number
         * @return key for a SAP system
         */
    private static String generateKeySAPSystem(String client, String r3name,
                                               String ashost, String sysnr)
    {
        String key = null;

        if(client != null && client.length() != 0)
        {
            if(r3name != null && r3name.length() != 0)
                key = r3name + client;
            else if(ashost != null && ashost.length() != 0 && sysnr != null && sysnr.length() != 0)
                key = ashost + sysnr + client;
        }
        return key;
    }

    synchronized public boolean isCachedItemUptodate()
    {
        if (getState()==IComponent.State.DELETED) 
        {
            return false;
        } // end of if ()
        return theServerEngine.isCatalogUpToDate(this);
    }
        /************************************************************************/
    
    /**
         * Returns the variant of the catalog.
         *
         * @return  variant id of the catalog
         */
    String getVariant()
    {
        return theVariant;
    }

    public String[] getViews()
    {
        ArrayList views = this.theClient.getViews();
        ArrayList aList = new ArrayList();
        Iterator anIter = views.iterator();
        while (anIter.hasNext() ) 
        {
            IView aView = (IView)anIter.next();
            aList.add(aView.getGuid());
        } // end of while ()
        return (String[]) aList.toArray( new String[] {});
    }


        /**
         * Returns the status of the (backend) Catalog.
         *
         * @return  status of the Catalog (either OK or ERROR)
         */
    public int getCatalogStatus()
    {
        try {
            IMSProxy proxy = IMSProxy.createProxy(theIMSConnectionInfo, theMessageResources);
            IMSIndexInfo info = getIndexInfo(IMSConstants.INDEX_LEVEL_A,"","");
            List attributeList = info.getAttributes();
            String aAttribute = (String)attributeList.get(0);
            proxy.setQueryEnv(theSearchEngine, theCodepage, theLanguage, info);
            proxy.addQueryAttribute(aAttribute,
                    IMSConstantsQuery.OPERATOR_EQ,"QueryForCatalogStatus");
            proxy.addReturnAttribute(aAttribute);
            proxy.setReturnInterval(1,1);
            proxy.executeQuery(theIMSConnectionInfo);
            proxy = null;
            return CATALOG_STATUS_OK;
        }
        catch(CRMException ex) {
            if(theStaticLocToLog.isDebugEnabled()) {
                theStaticLocToLog.debug("getCatalogStatus failed: " + ex.getLocalizedMessage(), ex);
            }
            return CATALOG_STATUS_ERROR;
        }
    }


        /**
         * Returns the list of attribute names used for a QuickSearch
         *
         * @return  list of attribute names to search for in a QuickSearch
         */
    public String[] getQuickSearchAttributes()
    {
        return theQuickSearchAttributes;
    }


        /**
         * Returns the code page of the Catalog.
         *
         * @return  id of the code page (IMS format)
         */
    String getCodepage() {
        return theCodepage;
    }

        /**
         * Gets the language of the catalog.
         *
         * @return  language  
         */
    String getLanguage() {
        return theLanguage;
    }

        /**
         * Gets the name of the search engine associated with the catalog.
         *
         * @return  name of the search engine
         */
    String getSearchEngine() {
        return theSearchEngine;
    }
    
    /**
     * Sets the code page of the Catalog.
     *
     * @param id of the code page (IMS format)
     */
    void setCodepage(String theCodepage) {
        this.theCodepage = theCodepage;
    }

    /**
     * Gets the language of the catalog.
     *
     * @return  language  
     */
    void setLanguage(String theLanguage) {
        this.theLanguage = theLanguage;
    }

    /**
     * Gets the name of the search engine associated with the catalog.
     *
     * @return  name of the search engine
     */
    void setSearchEngine(String theSearchEngine) {
        this.theSearchEngine = theSearchEngine;
    }    

        /**
         * Adds an information object about an index to the list of index information
         * objects.
         *
         * @param index  information object about an index
         */
    void addIndexInfo(IMSIndexInfo index)
    {
        synchronized(theIndexInfo)
        {
            theIndexInfo.put(index.generateKey(), index);
        }
    }

        /**
         * Returns an information object about an index.
         *
         * @param attrLevel  index level of the index
         * @param area       name of the area (in case of 'S' index; otherwise empty)
         * @param guid       guid of the catalog (if 'P' index) or of the area (if 'S' index)
         * @return           an information object about an index
         * @exception CRMException connection error to the CRM system
         */
    IMSIndexInfo getIndexInfo(String attrLevel, String area, String guid)
        throws CRMException
    {
        IMSIndexInfo info = null;
        String key = IMSIndexInfo.generateKey(attrLevel, area);

		//changed because of possible deadlocks
            info = (IMSIndexInfo)theIndexInfo.get(key);
            if (info == null)
            {
			synchronized(theIndexInfo)
			{
                info = CRMRfcMethods.getIMSIndexInfo(
                    getSAPConnection(),
                    getName(),
                    getVariant(),
                    getLanguage(),
                    attrLevel,
                    area,
                    guid,
                    getMessageResources());
                addIndexInfo(info);
            }
        }
        return info;
    }

    synchronized private JCoConnection getSAPConnection()
    {
        if (theSAPConnection==null) 
        {
            theSAPConnection = ((CRMCatalogServerEngine)theServerEngine).getDefaultJCoConnection();
        } // end of if ()
        return theSAPConnection;
    }
    
    synchronized private void setSAPConnection(Connection conn)
    {
        theSAPConnection = (JCoConnection)conn;
    }

        /**
         * This method sets a new default connection if the necessary connection
         * settings are passed via the client and the server parameter. The
         * settings for the server has to be passed via the properties which
         * are associated with the server instance.
         *
         * @param client  reference to a specific client
         * @param server  reference to a specific server
         */
    private void setDefaultConnection(IClient client, IServer server)
    {
        Properties props = null;

            // check if special client was specified
        if( client.getLocale() != null      &&
            client.getName() != null        &&
            client.getPWD()!= null          &&
            client.getName().length() != 0  &&
            client.getPWD().length() != 0)
        {
                // define new client settings
            props = new Properties();
            props.setProperty(  JCoManagedConnectionFactory.JCO_USER,
                                client.getName());
            props.setProperty(  JCoManagedConnectionFactory.JCO_PASSWD,
                                client.getPWD());
            props.setProperty(JCoManagedConnectionFactory.JCO_LANG,
                              client.getLocale().getLanguage().toUpperCase());
        }

            // check if special a server was specified
        Properties servProps = server.getProperties();
        if (servProps != null &&
            (
                (servProps.containsKey(JCoManagedConnectionFactory.JCO_CLIENT) &&
                 servProps.containsKey(JCoManagedConnectionFactory.JCO_R3NAME) &&
                 servProps.containsKey(JCoManagedConnectionFactory.JCO_MSHOST) &&
                 servProps.containsKey(JCoManagedConnectionFactory.JCO_GROUP) ) ||
                (servProps.containsKey(JCoManagedConnectionFactory.JCO_CLIENT) &&
                 servProps.containsKey(JCoManagedConnectionFactory.JCO_ASHOST) &&
                 servProps.containsKey(JCoManagedConnectionFactory.JCO_SYSNR) )) )
        {
                // define new server settings
            if(props == null)
                props = new Properties();
            props.setProperty(  JCoManagedConnectionFactory.JCO_CLIENT,
                                servProps.getProperty(
                                    JCoManagedConnectionFactory.JCO_CLIENT));
                // with load balancing
            if(servProps.containsKey(JCoManagedConnectionFactory.JCO_R3NAME))
            {
                props.setProperty(  JCoManagedConnectionFactory.JCO_R3NAME,
                                    servProps.getProperty(
                                        JCoManagedConnectionFactory.JCO_R3NAME));
                props.setProperty(  JCoManagedConnectionFactory.JCO_MSHOST,
                                    servProps.getProperty(
                                        JCoManagedConnectionFactory.JCO_MSHOST));
                props.setProperty(  JCoManagedConnectionFactory.JCO_GROUP,
                                    servProps.getProperty(
                                        JCoManagedConnectionFactory.JCO_GROUP));
            }
                // otherwise
            else
            {
                props.setProperty(  JCoManagedConnectionFactory.JCO_ASHOST,
                                    servProps.getProperty(
                                        JCoManagedConnectionFactory.JCO_ASHOST));
                props.setProperty(  JCoManagedConnectionFactory.JCO_SYSNR,
                                    servProps.getProperty(
                                        JCoManagedConnectionFactory.JCO_SYSNR));
            }
        }
            // change the default connection
        if(props != null)
        {
            try
            {
                JCoConnection modConn =
                    ((CRMCatalogServerEngine)theServerEngine).getModDefaultJCoConnection(props);
                if(modConn.isValid())
                {
                    this.setSAPConnection(modConn);
                }
            }
            catch(Exception ex)
            {
                theStaticLocToLog.warn(ICatalogMessagesKeys.PCAT_EXCEPTION,
                                   new Object[] { ex.getLocalizedMessage() }, null);
            }
        }
        return;
    }
    
    public void commitChangesRemote(Document theChanges)
        throws CatalogException
    {
        throw new UnsupportedOperationException("Not yet implemented!");
    }

        /************************** IMS specific methods ***********************/

    IMSConnectionInfo getIMSConnectionInfo()
    {
        return theIMSConnectionInfo;
    }

    String getReplTimeStamp()
    {
        return theReplTimeStamp;
    }
        
    public String[] getListOfIndexGuids()
        throws CRMException
    {
        return CRMRfcMethods.getListOfIndexGuids(getSAPConnection(), this);
    }
           
    

}