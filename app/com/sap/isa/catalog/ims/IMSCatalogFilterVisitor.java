/*****************************************************************************
  Copyright (c) 2001, SAPMarkets Europe GmbH, Germany, All rights reserved.
  Created:  27 March 2001

  $Revision: #4 $
  $Date: 2006/07/17 $
*****************************************************************************/

package com.sap.isa.catalog.ims;

// misc imports
import java.util.List;

import org.apache.struts.util.MessageResources;

import com.sap.isa.catalog.ICatalogMessagesKeys;
import com.sap.isa.catalog.filter.CatalogFilter;
import com.sap.isa.catalog.filter.CatalogFilterAnd;
import com.sap.isa.catalog.filter.CatalogFilterAttrContain;
import com.sap.isa.catalog.filter.CatalogFilterAttrEqual;
import com.sap.isa.catalog.filter.CatalogFilterAttrFuzzy;
import com.sap.isa.catalog.filter.CatalogFilterAttrLinguistic;
import com.sap.isa.catalog.filter.CatalogFilterInvalidException;
import com.sap.isa.catalog.filter.CatalogFilterNot;
import com.sap.isa.catalog.filter.CatalogFilterOr;
import com.sap.isa.catalog.filter.CatalogFilterVisitor;

/**
 * This class transforms the filter expression tree to the IMS specific
 * representation i.e. the calls against the IMS proxy are performed.
 * @version     1.0
 */
class IMSCatalogFilterVisitor extends CatalogFilterVisitor {

  private IMSProxy theIMSProxy;

  // State info: This attribute holds the reference of the parent node of
  // the actual node. This state information is only valid at the entry of
  // the visitor methods! It can be only used in the leaf of the tree,
  // otherwise a stack would be necessary.
  private CatalogFilter theParentFilterNode = null;

  /**
   * Creates a new instance of the IMS specfic visitor of the filter expression
   * tree.
   *
   * @param messResources  the resources for the error messages
   */
  IMSCatalogFilterVisitor(MessageResources messResources) {
    super(messResources);
  }

  /**
   * Starts the transformation of the filter expression tree.<BR/>
   * <B>IMPORTANT: </B>The synchronization of the proxy instance has to be done
   * by the caller of this method. For performance reasons the synchronization
   * is omitted here!
   *
   * @param rootFilter  the root of the expression tree
   * @param attributes  the attributes of the context of the filter expression
   * @param proxy       the proxy for the IMS server
   * @exception CatalogFilterInvalidException error during evaluation of the expression
   */
  void evaluate(CatalogFilter rootFilter, List attributes, IMSProxy proxy)
    throws CatalogFilterInvalidException
  {
    // IMPORTANT: The synchronization of the proxy instance has to be done
    //            by the caller of this method. For performance reasons
    //            the synchronization is omitted here!

    // initialization of state information
    theIMSProxy = proxy;
    theParentFilterNode = null;
    // start traversation of expression tree
    this.start(rootFilter, attributes);
    // clear state
    theIMSProxy = null;
    theParentFilterNode = null;
  }

  protected void visitAttrContainFilter(CatalogFilterAttrContain filterNode)
    throws CatalogFilterInvalidException {
    // super call
    super.visitAttrContainFilter(filterNode);
    // transformation
    String name    = filterNode.getName();
    String pattern = filterNode.getPattern();
    byte[] contentType = IMSConstantsQuery.CONTENT_TYPE_AND; // CONTENT_TYPE_TEXT
    if(theIMSProxy.getSearchEngine().equals(IMSConstants.ENGINE_VERITY))
      contentType = IMSConstantsQuery.TYPE_STRING;

    try {
      if(theParentFilterNode instanceof CatalogFilterNot) {
        theIMSProxy.addQueryAttribute(name,
                                      IMSConstantsQuery.OPERATOR_NE,
                                      pattern,
                                      contentType);
      } else {
        theIMSProxy.addQueryAttribute(name,
                                      IMSConstantsQuery.OPERATOR_EQ,
                                      pattern,
                                      contentType);

      }
    } catch(CRMException imsEx) {
      throw new CatalogFilterInvalidException(imsEx.getLocalizedMessage());
    }
  }

  protected void visitAttrEqualFilter(CatalogFilterAttrEqual filterNode)
    throws CatalogFilterInvalidException {
    // super call
    super.visitAttrEqualFilter(filterNode);
    // transformation
    String name  = filterNode.getName();
    String value = filterNode.getValue();
    try {
      if(theParentFilterNode instanceof CatalogFilterNot) {
        theIMSProxy.addQueryAttribute(name,
                                      IMSConstantsQuery.OPERATOR_NE,
                                      value);
      } else {
        theIMSProxy.addQueryAttribute(name,
                                      IMSConstantsQuery.OPERATOR_EQ,
                                      value);
      }
    } catch(CRMException imsEx) {
      throw new CatalogFilterInvalidException(imsEx.getLocalizedMessage());
    }
  }

  protected void visitAttrLinguisticFilter(CatalogFilterAttrLinguistic filterNode)
    throws CatalogFilterInvalidException {
    // super call
    super.visitAttrLinguisticFilter(filterNode);
    // transformation
    String name    = filterNode.getName();
    String pattern = filterNode.getPattern();
    byte[] contentType = IMSConstantsQuery.CONTENT_TYPE_AND; // CONTENT_TYPE_TEXT

    // unfortunately Verity does not support linguistic search!
    if(theIMSProxy.getSearchEngine().equals(IMSConstants.ENGINE_VERITY)) {
        String msg = getMessageResources().getMessage(
            ICatalogMessagesKeys.PCAT_FILTER_NOT_SUPP_EXPR, filterNode);
        throw new CatalogFilterInvalidException(msg);
    }

    try {
      if(theParentFilterNode instanceof CatalogFilterNot) {
        theIMSProxy.addQueryAttribute(name,
                                      IMSConstantsQuery.OPERATOR_NE,
                                      pattern,
                                      contentType,
                                      IMSConstantsQuery.TERM_ACTION_LINGUISTIC,
                                      10000);
      } else {
        theIMSProxy.addQueryAttribute(name,
                                      IMSConstantsQuery.OPERATOR_EQ,
                                      pattern,
                                      contentType,
                                      IMSConstantsQuery.TERM_ACTION_LINGUISTIC,
                                      10000);
      }
    } catch(CRMException imsEx) {
      throw new CatalogFilterInvalidException(imsEx.getLocalizedMessage());
    }
  }

  protected void visitAttrFuzzyFilter(CatalogFilterAttrFuzzy filterNode)
    throws CatalogFilterInvalidException {
    // super call
    super.visitAttrFuzzyFilter(filterNode);
    // transformation
    String name    = filterNode.getName();
    String pattern = filterNode.getPattern();
    // Changing the fuzzysimilarity (weight) from 3000 to 7000 to make the search
    // more restrictive
    int weight     = 7000;
    byte[] contentType = IMSConstantsQuery.CONTENT_TYPE_AND; // CONTENT_TYPE_TEXT

    // unfortunately Verity does not support fuzzy search!
    if(theIMSProxy.getSearchEngine().equals(IMSConstants.ENGINE_VERITY)) {
        String msg = getMessageResources().getMessage(
            ICatalogMessagesKeys.PCAT_FILTER_NOT_SUPP_EXPR, filterNode);
        throw new CatalogFilterInvalidException(msg);
    }

    // set default weight if necessary
    try {
        if(filterNode.getParameter("TERM_WEIGHT") != null)
            weight = Integer.valueOf(filterNode.getParameter("TERM_WEIGHT")).intValue();
    } catch(NumberFormatException numbEx) {
      weight = 7000;
    }

    try {
      if(theParentFilterNode instanceof CatalogFilterNot) {
        theIMSProxy.addQueryAttribute(name,
                                      IMSConstantsQuery.OPERATOR_NE,
                                      pattern,
                                      contentType,
                                      IMSConstantsQuery.TERM_ACTION_FUZZY,
                                      weight);
      } else {
        theIMSProxy.addQueryAttribute(name,
                                      IMSConstantsQuery.OPERATOR_EQ,
                                      pattern,
                                      contentType,
                                      IMSConstantsQuery.TERM_ACTION_FUZZY,
                                      weight);
      }
    } catch(CRMException imsEx) {
      throw new CatalogFilterInvalidException(imsEx.getLocalizedMessage());
    }
  }

  protected void visitNotFilter(CatalogFilterNot filterNode)
    throws CatalogFilterInvalidException {
    // super call
    super.visitNotFilter(filterNode);
    // transformation
    CatalogFilter operand = (CatalogFilter)filterNode.getOperands().get(0);
    theParentFilterNode = filterNode;
    operand.assign(this);
    try {
      if(!operand.isLeaf()) {
        // unfortunately Dr Fuzzy does not support the NOT operator!
        if(theIMSProxy.getSearchEngine().equals(IMSConstants.ENGINE_DRFUZZY)) {
          String msg = getMessageResources().getMessage(
            ICatalogMessagesKeys.PCAT_FILTER_NOT_SUPP_EXPR, filterNode);
          throw new CatalogFilterInvalidException(msg);
        }
        // in case of Verity it should work
        theIMSProxy.addQueryOperator(IMSConstantsQuery.OPERATOR_NOT);
      }
    } catch(CRMException imsEx) {
      throw new CatalogFilterInvalidException(imsEx.getLocalizedMessage());
    }
  }

  protected void visitOrFilter(CatalogFilterOr filterNode)
    throws CatalogFilterInvalidException {
    // super call
    super.visitOrFilter(filterNode);
    // transformation - inorder
    CatalogFilter lOperand = (CatalogFilter)filterNode.getOperands().get(0);
    theParentFilterNode = filterNode;
    lOperand.assign(this);
    CatalogFilter rOperand = (CatalogFilter)filterNode.getOperands().get(1);
    theParentFilterNode = filterNode;
    rOperand.assign(this);
    try {
      theIMSProxy.addQueryOperator(IMSConstantsQuery.OPERATOR_OR);
    } catch(CRMException imsEx) {
      throw new CatalogFilterInvalidException(imsEx.getLocalizedMessage());
    }
  }

  protected void visitAndFilter(CatalogFilterAnd filterNode)
    throws CatalogFilterInvalidException {
    // super call
    super.visitAndFilter(filterNode);
    // transformation - inorder
    CatalogFilter lOperand = (CatalogFilter)filterNode.getOperands().get(0);
    theParentFilterNode = filterNode;
    lOperand.assign(this);
    CatalogFilter rOperand = (CatalogFilter)filterNode.getOperands().get(1);
    theParentFilterNode = filterNode;
    rOperand.assign(this);
    try {
      theIMSProxy.addQueryOperator(IMSConstantsQuery.OPERATOR_AND);
    } catch(CRMException imsEx) {
      throw new CatalogFilterInvalidException(imsEx.getLocalizedMessage());
    }
  }
}
