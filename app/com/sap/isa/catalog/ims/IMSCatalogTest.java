/*****************************************************************************
  Copyright (c) 2001, SAPMarkets Europe GmbH, Germany, All rights reserved.

  $Revision: #3 $
  $Date: 2003/08/22 $
*****************************************************************************/
package com.sap.isa.catalog.ims;

// core imports
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Locale;
import java.util.Properties;

import com.sap.isa.catalog.boi.ICatalog;
import com.sap.isa.catalog.boi.ICategory;
import com.sap.isa.catalog.boi.IClient;
import com.sap.isa.catalog.boi.IFilter;
import com.sap.isa.catalog.boi.IQuery;
import com.sap.isa.catalog.boi.IQueryStatement;
import com.sap.isa.catalog.boi.IServer;
import com.sap.isa.catalog.filter.CatalogFilterFactory;
import com.sap.isa.core.TechKey;

/**
 * This class tests the IMSCatalog implementation. This class is for
 * internal use only!
 *
 * @version     1.0
 */
class IMSCatalogTest implements Runnable
{

    IMSCatalogTest()
    {
    }

    public static void main(String[] a)
    {
        IMSCatalogTest cat1 = new IMSCatalogTest();
        IMSCatalogTest cat2 = new IMSCatalogTest();
        IMSCatalogTest cat3 = new IMSCatalogTest();

        new Thread(cat1).start();
        if(a.length >= 2)
            new Thread(cat2).start();
        if(a.length >= 3)
            new Thread(cat3).start();
    }

    public void run()
    {
        this.test();
    }

        /**
         * Simple test method.
         */
    void test()
    {
        try 
        {
            String name    = "RH_CATALOG_01                 ";
            String variant = "DEUTSCH                       ";

            final String myCatalogGuid = name + variant;
            final ArrayList myCatalogViews = new ArrayList();
                //myCatalogViews.add(new IView() { public String getGuid() { return "VIEW2"; } });

                // helper class
            class CatInfoTest
                implements IClient, IServer 
            {
                protected int catalogState = CATALOG_STATE_ACTIVE;
                protected TechKey stagingCatalogKey;
                
                CatInfoTest() { }

                public Locale getLocale() { return Locale.GERMAN; }

                public String getName() { return "muellerfran"; }

                public String getGuid() { return "muellerfran"; }

                public String getCookie() { return null; }

                public String getPWD()  { return "zorro"; }

                public ArrayList getViews() { return myCatalogViews; }
    
                public Iterator getPermissions() { return null;}

                public String getURLString() { return ""; }
                
                public int getCatalogStatus() { return catalogState; }
                
                public TechKey getStagingCatalogKey () { return stagingCatalogKey; }

                public int getPort() { return 0; }

                public String getCatalogGuid() { return myCatalogGuid; }
                
                /**
                 * Sets the catalog state
                 *
                 * @param catalogStatus catalogStatus the state of the catalogue to be read
                 */
                public void setCatalogStatus(int catalogState) {
                    this.catalogState = catalogState;
                }

                /**
                 * Sets the TechKey of the staging catalogue to read
                 *
                 * @param stagingCatalogKey  the TechKey of the staging catalogue to read!
                 */
                public void setStagingCatalogKey(TechKey stagingCatalogKey) {
                    this.stagingCatalogKey = stagingCatalogKey;
                }

                public Properties getProperties() 
                {
                    return new Properties();
                }
            }

            CatInfoTest catInfo = new CatInfoTest();

                //CatalogFactory fac = CatalogFactory.getInstance();
                // start testing
                //ICatalog catalog = fac.getCatalog((IClient)catInfo, (IServer)catInfo);
                //ICatalog catalog2 = fac.getCatalog((IClient)catInfo, (IServer)catInfo);
                //this.testCatalog(catalog);
                //this.testQueryItem(catalog); // query for items
                //this.testCategoryQueryItem(catalog); // categories spec. query for items
                //this.testQueryCategory(catalog); // query for categories
                //this.testCategoryQueryCategory(catalog); // categories spec. query for categories
        } catch (Exception ex) {
            System.out.print(ex.getMessage());
        }
    }

    private void testCatalog(ICatalog catalog)
    {
        try 
        {
            Iterator rootIter = catalog.getChildCategories();
            catalog.getCategories();
            rootIter = catalog.getChildCategories();
            while(rootIter.hasNext())
            {
                ICategory aCat = (ICategory)rootIter.next();
                aCat.getChildCategories();
                aCat.getItems();
                aCat.getDetails();
            }

            catalog.getChildCategories();
            catalog.getCategories();

                //      ICategory cat2 = catalog.getCategory("E402E757270ED511A6E90800062785ED");
                //      cat2.getParent().getChildren();
                //      cat2.getItems();
            Iterator iter = catalog.getCategories();
            while(iter.hasNext())
            {
                ICategory aCat = ((ICategory)iter.next());
                aCat.getChildCategories();
                aCat.getItems();
                aCat.getItems();
                aCat.getDetails();
            }
        }
        catch(Exception ex)
        {
            System.out.println(ex.getMessage());
        }
    }

    private void testQueryItem(ICatalog catalog)
    {
        try 
        {
            IQueryStatement statement  = catalog.createQueryStatement();
            IQueryStatement statement2 = catalog.createQueryStatement();
                // get filter factory
            CatalogFilterFactory fact = CatalogFilterFactory.getInstance();
                // create filter expression
            IFilter attr1    = fact.createAttrContainValue("OBJECT_DESCRIPTION", "Ha*");
            IFilter notAttr1 = fact.createNot(attr1);
            IFilter attr2    = fact.createAttrEqualValue("POS_NR", "00020");
            IFilter cond  = fact.createAnd(attr2, notAttr1);

            System.out.print(cond);
                // create filtered statement
            statement.setStatement(cond, null, null);
                //statement2.setStatement(cond, new String[] {"PON", "OBJECT_DESCRIPTION"} , null);
            statement2.setStatementAsString("Halter*");
                // finish and sumit query
            IQuery query = catalog.createQuery(statement);
            query.submit();
            IQuery query2 = catalog.createQuery(statement2);
            query2.submit();
                // try to change the statement
            statement.setStatement(attr2, null, null);
        }
        catch(Exception ex)
        {
            System.out.println(ex.getMessage());
        }
    }

    private void testCategoryQueryItem(ICatalog catalog)
    {
        try 
        {
            ICategory myCat = catalog.getCategory("E602E757270ED511A6E90800062785ED");
            IQueryStatement statement = myCat.createQueryStatement();
                // get filter factory
            CatalogFilterFactory fact = CatalogFilterFactory.getInstance();
                // create filter expression
            IFilter attr1 = fact.createAttrContainValue("ZHMFARBE", "blau");
            System.out.print(attr1);
                // create filtered statement
            statement.setStatement(attr1, null, null);
                // finish and sumit query
            IQuery query = myCat.createQuery(statement);
            query.submit();
                // now no longer modifiable
            statement.setStatement(attr1, null, null);
        } catch(Exception ex) {
            System.out.println(ex.getMessage());
        }
    }

    private void testQueryCategory(ICatalog catalog)
    {
        try 
        {
            IQueryStatement statement  = catalog.createQueryStatement();
            statement.setSearchType(IQueryStatement.Type.CATEGORY_SEARCH);
                // get filter factory
            CatalogFilterFactory fact = CatalogFilterFactory.getInstance();
                // create filter expression
            IFilter attr1    = fact.createAttrContainValue("DESCRIPTION", "Ebene*");
            IFilter attr2    = fact.createAttrEqualValue("DESCRIPTION", "Leergut");
            IFilter cond  = fact.createOr(attr2, attr1);
                // create filtered statement
            statement.setStatement(cond, null, null);
            System.out.println(statement.getName());
                //statement.setStatement(cond, new String[] {"PON", "OBJECT_DESCRIPTION"} , null);
                // finish and sumit query
            IQuery query = catalog.createQuery(statement);
            query.submit();
        } catch(Exception ex) {
            System.out.println(ex.getMessage());
        }
    }
}