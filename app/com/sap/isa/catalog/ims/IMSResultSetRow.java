/*****************************************************************************
  Copyright (c) 2001, SAPMarkets Europe GmbH, Germany, All rights reserved.

  $Revision: #3 $
  $Date: 2003/08/22 $
*****************************************************************************/

package com.sap.isa.catalog.ims;

import java.util.Iterator;

/**
 * This interface represents a row in an IMS result set. Each row consists of
 * several columns.
 *
 * @version     1.0
 */
public interface IMSResultSetRow {

  /**
   * Constant for an empty string array.
   */
  public static final String[] EMPTY_ARRAY = {};

  /**
   * Returns the value of the given column. The multi values are concatenated
   * to a string separated by the value of <code>IMSResultSetItem.SEPARATOR</code>.
   *
   * @param col  index of the column
   * @return     value of the column, empty in case of invalid index
   */
  public String getValue(int col);

  /**
   * Returns the value of the given column. The multi values are concatenated
   * to a string separated by the value of <code>IMSResultSetItem.SEPARATOR</code>.
   *
   * @param colName  name of the column
   * @return         value of the column, empty in case of invalid column name
   */
  public String getValue(String colName);

  /**
   * Returns the values of the given column. This method should be used in
   * case of multi values.
   *
   * @param col  index of the column
   * @return     array of values, empty in case of invalid column name
   */
  public String[] getValues(int col);

  /**
   * Returns the values of the given column. This method should be used in
   * case of multi values.
   *
   * @param colName  name of the column
   * @return         array of values, empty in case of invalid column name
   */
  public String[] getValues(String colName);

  /**
   * Returns true if the given column contains a multi value.
   *
   * @param col  index of the column
   * @return     true if entry is multi value, otherwise false
   */
  public boolean isMultiValue(int col);

  /**
   * Returns true if the given column contains a multi value.
   *
   * @param colName  name of the column
   * @return         true if entry is multi value, otherwise false
   */
  public boolean isMultiValue(String colName);

  /**
   * Returns an iterator for all items in the row.
   *
   * @return  item iterator
   */
  public Iterator iterator();
}

