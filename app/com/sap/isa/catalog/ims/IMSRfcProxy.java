/*****************************************************************************
  Copyright (c) 2001, SAP AG, All rights reserved.
  
  Created:  12 February 2001

  $Revision: #4 $
  $Date: 2004/03/09 $
*****************************************************************************/
package com.sap.isa.catalog.ims;

// misc imports
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.apache.struts.util.MessageResources;

import com.sap.isa.catalog.ICatalogMessagesKeys;
import com.sap.isa.catalog.misc.CatalogJCoLogHelper;
import com.sap.isa.core.eai.BackendException;
import com.sap.isa.core.eai.sp.jco.JCoConnection;
import com.sap.isa.core.logging.IsaLocation;
import com.sap.mw.jco.JCO;

/**
 * Client-side proxy of the IndexManagementServer. With this class
 * the IMS is accessed via RFC.
 * @version     1.0
 */
class IMSRfcProxy extends IMSProxy {
    
    private final static byte[] IMS_IS0885591 = { 0x31, 0, 0 }; // IS0 8859/1
    private final static byte[] IMS_IS0885592 = { 0x32, 0, 0 }; // IS0 8859/2
    private final static byte[] IMS_IS0885593 = { 0x33, 0, 0 }; // IS0 8859/3 (e.g. Turkish)
    private final static byte[] IMS_IS0885595 = { 0x35, 0, 0 }; // IS0 8859/5
    private final static byte[] IMS_IS0885596 = { 0x36, 0, 0 }; // IS0 8859/6
    private final static byte[] IMS_IS0885597 = { 0x37, 0, 0 }; // IS0 8859/7
    private final static byte[] IMS_IS0885598 = { 0x38, 0, 0 }; // IS0 8859/8
    private final static byte[] IMS_IS0885599 = { 0x39, 0, 0 }; // IS0 8859/9
    private final static byte[] IMS_SJIS      = { 0x53, 0, 0 }; // ISO SJIS
    private final static byte[] IMS_UTF8      = { 0x55, 0x54, 0x38 }; // "UT8" UTF-8
    private final static byte[] IMS_UTF16     = { 0x55, 0x54, 0x36 }; // "UT6" UTF-16
    private final static byte[] IMS_CP1252    = { 0x43, 0x50, 0x31 }; // "CP1" Cp1252
    private final static byte[] IMS_GB2312    = { 0x47, 0x42, 0x32 }; // "GB2" GBK
    private final static byte[] IMS_EUCKR     = { 0x45, 0x55, 0x43 }; // "EUC" EUC_KR
    private final static byte[] IMS_EUCJP     = { 0x45, 0x55, 0x4A }; // "EUJ" EUC_JP
    private final static byte[] IMS_BIG5CN    = { 0x42, 0x49, 0x47 }; // "BIG" Big5
    private final static byte[] IMS_TIS620    = { 0x54, 0x36, 0x32 }; // "T62" TIS620 Thai

    private final static String JAVA_IS0885591 = "ISO8859_1"; // IS0 8859/1
    private final static String JAVA_IS0885592 = "ISO8859_2"; // IS0 8859/2
    private final static String JAVA_IS0885593 = "ISO8859_3"; // IS0 8859/3
    private final static String JAVA_IS0885595 = "ISO8859_5"; // IS0 8859/5
    private final static String JAVA_IS0885596 = "ISO8859_6"; // IS0 8859/6
    private final static String JAVA_IS0885597 = "ISO8859_7"; // IS0 8859/7
    private final static String JAVA_IS0885598 = "ISO8859_8"; // IS0 8859/8
    private final static String JAVA_IS0885599 = "ISO8859_9"; // IS0 8859/9
    private final static String JAVA_SJIS      = "SJIS";      // ISO SJIS
    private final static String JAVA_UTF8      = "UTF8";      // UTF-8
    private final static String JAVA_UTF16     = "UTF-16";    // UTF-16
    private final static String JAVA_CP1252    = "Cp1252";    // Cp1252
    private final static String JAVA_GB2312    = "GBK";       // GB2
    private final static String JAVA_EUCKR     = "EUC_KR";    // EUC_KR
    private final static String JAVA_EUCJP     = "EUC_JP";    // EUC_JP
    private final static String JAVA_BIG5CN    = "Big5";      // BIG
    private final static String JAVA_TIS620    = "TIS620";    // TIS620 Thai

    private static JCO.MetaData theLayoutTab1; // Meta data of type "SRETIDSTID"
    private static JCO.MetaData theLayoutTab2; // Meta data of type "SRETGEATTR"
    private static JCO.MetaData theLayoutTab3; // Meta data of type "SRETDOCATR"
    private static JCO.MetaData theLayoutTab4; // Meta data of type "SRETXQYSE"
    private static JCO.MetaData theLayoutTab5; // Meta data of type "SRETXTQATR"
    private static JCO.MetaData theLayoutTab6; // Meta data of type "SRETXRSDOC"
    private static JCO.MetaData theLayoutTab7; // Meta data of type "SRETXDATVL"

    private static HashMap theEncodingMap; // Maps IMS encoding to Java encoding

// the logging instance
    private static IsaLocation log =
        IsaLocation.getInstance(IMSRfcProxy.class.getName());

// Current parameters of the rfc call
    private JCO.ParameterList theImportList = null;
    private JCO.ParameterList theExportList = null;
    private JCO.ParameterList theTableList  = null;

// Default query environment
    private String theDefaultSearchEngine;
    private String theDefaultCodePage;
    private String theDefaultLanguage;

// Current encoding
    private String theEncoding;

// List of attributes of the current index
    private List theIndexAttributes;

// SearchEngine which is currently used
    private String theSearchEngine;

// List of attributes which are requested of the IMS
    private ArrayList theReturnAttributes;

// only for debug purpose
  private String dbgIndexName = null;
  private String dbgWhereClause = "";
  private String dbgFromTo = null;
  private String dbgOrderBy = null;

// attribute name/value to be substituted when searching for ALL documents 
  private String searchAllDocuments$attrName = null;
  private String searchAllDocuments$attrValue = null;

// meta data of rfc call can't be retrieved from the
// RFC Server so they have to be coded here.
    static {
        theLayoutTab1 = new JCO.MetaData("SRETIDSTID", 4);
        theLayoutTab1.addInfo("IDSTORID", JCO.TYPE_BYTE, 40,   0);
        theLayoutTab1.addInfo("LANGU",    JCO.TYPE_BYTE,  2,  40);
        theLayoutTab1.addInfo("STATUSID", JCO.TYPE_NUM,   4,  42);
        theLayoutTab1.addInfo("COUNTER",  JCO.TYPE_NUM,  10,  46);

        theLayoutTab2 = new JCO.MetaData("SRETGEATTR", 4);
        theLayoutTab2.addInfo("NAME",     JCO.TYPE_BYTE, 25,   0);
        theLayoutTab2.addInfo("VALUE1",   JCO.TYPE_BYTE, 25,  25);
        theLayoutTab2.addInfo("VALUE2",   JCO.TYPE_BYTE, 25,  50);
        theLayoutTab2.addInfo("OPERATOR", JCO.TYPE_BYTE,  2,  75);

        theLayoutTab3 = new JCO.MetaData("SRETDOCATR", 2);
        theLayoutTab3.addInfo("ATTRNAME", JCO.TYPE_BYTE, 25,   0);
        theLayoutTab3.addInfo("ATTRTYPE", JCO.TYPE_BYTE,  2,  25);

        theLayoutTab4 = new JCO.MetaData("SRETXQYSE", 6);
        theLayoutTab4.addInfo("SECOUNT",  JCO.TYPE_NUM,    5,   0);
        theLayoutTab4.addInfo("LOCATION", JCO.TYPE_BYTE,  25,   5);
        theLayoutTab4.addInfo("VALUE1",   JCO.TYPE_BYTE, 255,  30);
        theLayoutTab4.addInfo("VALUE2",   JCO.TYPE_BYTE, 255, 285);
        theLayoutTab4.addInfo("OPERATOR", JCO.TYPE_BYTE,   2, 540);
        theLayoutTab4.addInfo("ROWTYPE",  JCO.TYPE_BYTE,   2, 542);

        theLayoutTab5 = new JCO.MetaData("SRETXTQATR", 5);
        theLayoutTab5.addInfo("NAME",     JCO.TYPE_BYTE, 25,   0);
        theLayoutTab5.addInfo("VALUE1",   JCO.TYPE_BYTE, 25,  25);
        theLayoutTab5.addInfo("VALUE2",   JCO.TYPE_BYTE, 25,  50);
        theLayoutTab5.addInfo("OPERATOR", JCO.TYPE_BYTE,  2,  75);
        theLayoutTab5.addInfo("QTREFER",  JCO.TYPE_NUM,   5,  77);

        theLayoutTab6 = new JCO.MetaData("SRETXRSDOC", 5);
        theLayoutTab6.addInfo("DOCID",    JCO.TYPE_BYTE, 64,   0);
        theLayoutTab6.addInfo("RANKV",    JCO.TYPE_NUM,   5,  64);
        theLayoutTab6.addInfo("IDSTORID", JCO.TYPE_BYTE, 40,  69);
        theLayoutTab6.addInfo("LANGU",    JCO.TYPE_BYTE,  2, 109);
        theLayoutTab6.addInfo("COUNTER",  JCO.TYPE_NUM,  10, 111);

        theLayoutTab7 = new JCO.MetaData("SRETXDATVL", 7);
        theLayoutTab7.addInfo("ATTRNAME",   JCO.TYPE_BYTE, 25,   0);
        theLayoutTab7.addInfo("MULTATRCNT", JCO.TYPE_NUM,   5,  25);
        theLayoutTab7.addInfo("ATTRTYPE",   JCO.TYPE_BYTE,  2,  30);
        theLayoutTab7.addInfo("VALUE1",     JCO.TYPE_BYTE, 64,  32);
        theLayoutTab7.addInfo("VALUE2",     JCO.TYPE_BYTE, 64,  96);
        theLayoutTab7.addInfo("OPERATOR",   JCO.TYPE_BYTE,  2, 160);
        theLayoutTab7.addInfo("DOCREFER",   JCO.TYPE_NUM,  10, 162);

        theEncodingMap = new HashMap();
        theEncodingMap.put(new String(IMS_IS0885591).trim(), JAVA_IS0885591);
        theEncodingMap.put(new String(IMS_IS0885592).trim(), JAVA_IS0885592);
        theEncodingMap.put(new String(IMS_IS0885593).trim(), JAVA_IS0885593);
        theEncodingMap.put(new String(IMS_IS0885595).trim(), JAVA_IS0885595);
        theEncodingMap.put(new String(IMS_IS0885596).trim(), JAVA_IS0885596);
        theEncodingMap.put(new String(IMS_IS0885597).trim(), JAVA_IS0885597);
        theEncodingMap.put(new String(IMS_IS0885598).trim(), JAVA_IS0885598);
        theEncodingMap.put(new String(IMS_IS0885599).trim(), JAVA_IS0885599);
        theEncodingMap.put(new String(IMS_SJIS).trim()     , JAVA_SJIS);
        theEncodingMap.put(new String(IMS_UTF8).trim()     , JAVA_UTF8);
        theEncodingMap.put(new String(IMS_UTF16).trim()    , JAVA_UTF16);
        theEncodingMap.put(new String(IMS_CP1252).trim()   , JAVA_CP1252);
        theEncodingMap.put(new String(IMS_GB2312).trim()   , JAVA_GB2312);
        theEncodingMap.put(new String(IMS_EUCKR).trim()    , JAVA_EUCKR);
        theEncodingMap.put(new String(IMS_EUCJP).trim()    , JAVA_EUCJP);
        theEncodingMap.put(new String(IMS_BIG5CN).trim()   , JAVA_BIG5CN);
        theEncodingMap.put(new String(IMS_TIS620).trim()   , JAVA_TIS620);
    }

/**
 * Constructor to initialize the rfc proxy.
 */
    protected IMSRfcProxy(MessageResources messageResources) {
            // initialize parent class
        super(messageResources);
            // prepare parameters for the rfc call
        theImportList = getImportParaList();
        theExportList = getExportParaList();
        theTableList  = getTablesParaList();
            // real encoding is set in method setQueryEnv
        theEncoding = JAVA_IS0885591; // default
            // initialization
        theReturnAttributes = new ArrayList();
    }

    private JCO.ParameterList getImportParaList() {
        JCO.ParameterList inpPara = new JCO.ParameterList();
        inpPara.addInfo("SEARCHENG", JCO.TYPE_BYTE, 10,  0);
        inpPara.addInfo("CODEPAGE",  JCO.TYPE_BYTE,  3, 10);

        return inpPara;
    }

    private JCO.ParameterList getExportParaList() {
        JCO.ParameterList expPara = new JCO.ParameterList();
        expPara.addInfo("NUMOFHITS", JCO.TYPE_NUM, 10,  0);
        expPara.addInfo("RCODE",     JCO.TYPE_CHAR, 4, 10);

        return expPara;
    }

    private JCO.ParameterList getTablesParaList()  {
        JCO.ParameterList tabPara = new JCO.ParameterList();

        tabPara.appendValue("IDXSTORAGETAB", new JCO.Table(theLayoutTab1));
        tabPara.appendValue("QUERYPARAMTAB", new JCO.Table(theLayoutTab2));
        tabPara.appendValue("DOCATTRTAB",    new JCO.Table(theLayoutTab3));
        tabPara.appendValue("QUERYTAB",      new JCO.Table(theLayoutTab4));
        tabPara.appendValue("TERMATTRTAB",   new JCO.Table(theLayoutTab5));
        tabPara.appendValue("RESULTDOCTAB",  new JCO.Table(theLayoutTab6));
        tabPara.appendValue("RESULTATTRTAB", new JCO.Table(theLayoutTab7));

        return tabPara;
    }

    void setQueryEnv(String searchEngine, String codePage,
                     String language, IMSIndexInfo info) throws CRMException {
            // get corresponding java encoding name
        if(theEncodingMap.containsKey(codePage)) {
            theEncoding = (String)theEncodingMap.get(codePage);
        }    
        else {
            // ATTENTION: this is some dirty hack to enable 
            // the staging functionality for IMSAdmin
            theEncoding = (String) theEncodingMap.get(new String(IMS_UTF8).trim());
            
            
/*            String msg = getMessageResources().getMessage(
                ICatalogMessagesKeys.PCAT_IMS_INVALID_CODEPAGE, codePage);
            throw new CRMException(msg);
*/
        }

        try {
            theImportList.setValue(searchEngine.getBytes(theEncoding), "SEARCHENG");
            theImportList.setValue(codePage.getBytes(theEncoding),     "CODEPAGE");

            JCO.Table tab1 = theTableList.getTable("IDXSTORAGETAB");
            tab1.appendRow();
            tab1.setValue(info.getStoreId().getBytes(theEncoding), "IDSTORID");
            tab1.setValue(language.getBytes(theEncoding),  "LANGU");

            addQueryParameter(QUERY_LANGUAGE, language.getBytes(theEncoding), null);

        }
        catch (UnsupportedEncodingException ex) {
            throw new CRMException(ex.getLocalizedMessage());
        }

            // get names of potential return attributes
        theIndexAttributes = info.getAttributes();
            // set name of searchengine
        theSearchEngine = searchEngine;
            // logging
        if(log.isDebugEnabled()) {
            log.debug("Query - Env: Engine: " + searchEngine +
                                ", Codepage: "          + codePage +
                                ", Langu: "             + language +
                                ", Index: "             + info.generateKey() +
                                ".");
        }
        if(/*theStaticLocToLog.isDebugEnabled() || */log.isInfoEnabled()) {
            // as long as isInfoEnabled() is FALSE even if DEBUG is set, this will remain NULL !!!
            dbgIndexName = info.getStoreId();
       }
    }

    void setQueryEnvInitial(String searchEngine, String codePage,
                            String language, String searchAllDocumentsAttrName,
                            String searchAllDocumentsAttrValue) throws CRMException {
                         
        searchAllDocuments$attrName  = searchAllDocumentsAttrName;
        searchAllDocuments$attrValue = searchAllDocumentsAttrValue;
        theDefaultSearchEngine = searchEngine;
        theDefaultCodePage     = codePage;
        theDefaultLanguage     = language;
    }

    void setQueryEnv(IMSIndexInfo info) throws CRMException {
        this.setQueryEnv(theDefaultSearchEngine, theDefaultCodePage, theDefaultLanguage, info);
    }

    String getSearchEngine() {
        return theSearchEngine;
    }

    void addReturnAttribute(String name) throws CRMException {
        if (!theIndexAttributes.contains(name)) {
            String msg = getMessageResources().getMessage(
                ICatalogMessagesKeys.PCAT_IMS_UNKNOWN_ATTR, name);
            throw new CRMException(msg);
        }

        try {
            theReturnAttributes.add(name);
            addDocumentAttribute(name.getBytes(theEncoding));

            log.debug("Query - Return Attribute: " + name + ".");
        }
        catch (UnsupportedEncodingException ex) {
            throw new CRMException(ex.getLocalizedMessage());
        }
    }

    void addReturnAttributes(String[] names) throws CRMException {
        for (int i=0; i < names.length; i++) {
            addReturnAttribute(names[i]);
        }
    }

    void setReturnInterval(int lower, int upper) throws CRMException {
        
        Object[] arguments = { new Integer(lower), new Integer(upper) };
        if (upper < lower) {
            String msg = getMessageResources().getMessage(
                ICatalogMessagesKeys.PCAT_IMS_INVALID_INTERVAL, arguments);
            throw new CRMException(msg);
        }

        byte[] low = (new Integer(lower)).toString().getBytes();
        byte[] up  = (new Integer(upper)).toString().getBytes();
        addQueryParameter(ROW_FROM_TO, low, up);

        log.debug("Query - Return Interval: " + new Integer(lower) + " - " + new Integer(upper) + ".");
        
        if(log.isDebugEnabled() || log.isInfoEnabled()) {
            dbgFromTo = " from row " + lower + " to row " + upper;
        }
    }

    void setSortOrderASC(String name) throws CRMException {
        if (!theIndexAttributes.contains(name))  {
            String msg = getMessageResources().getMessage(
                ICatalogMessagesKeys.PCAT_IMS_UNKNOWN_ATTR, name);
            throw new CRMException(msg);
        }

        try {    
            addQueryParameter(SORT_ORDER, name.getBytes(theEncoding), ORDER_ASC);

            log.debug("Query - Sort Order: " + name + " - " + "ascending.");
        }
        catch (UnsupportedEncodingException ex)  {
            throw new CRMException(ex.getLocalizedMessage());
        }
        
        if(log.isDebugEnabled() || log.isInfoEnabled()) {
            dbgOrderBy = " order by '" + name + "' ascending";
        }
    }

    void setSortOrderDSC(String name) throws CRMException {
        
        if (!theIndexAttributes.contains(name))  {
            String msg = getMessageResources().getMessage(
                ICatalogMessagesKeys.PCAT_IMS_UNKNOWN_ATTR, name);
            throw new CRMException(msg);
        }

        try {
            addQueryParameter(SORT_ORDER, name.getBytes(theEncoding), ORDER_DESC);

            log.debug("Query - Sort Order: " + name + " - " + "descending.");
        }
        catch (UnsupportedEncodingException ex) {
            throw new CRMException(ex.getLocalizedMessage());
        }
        
        if(log.isDebugEnabled() || log.isInfoEnabled()) {
            dbgOrderBy = " order by '" + name + "' descending";
        }
    }

    void addQueryAttribute(String name, byte[] oper, String value)
        throws CRMException {
        addQueryAttribute(name, oper, value, TYPE_STRING, null, 0);
    }

    void addQueryAttribute(String name, byte[] oper, String value, byte[] type)
        throws CRMException {
        addQueryAttribute(name, oper, value, type, null, 0);
    }

    void addQueryAttribute(String name, byte[] oper, String value, byte[] type,
                           byte[] action, int termWeight) throws CRMException  {
                               
        if (!theIndexAttributes.contains(name))  {
            log.error(ICatalogMessagesKeys.PCAT_IMS_UNKNOWN_ATTR, new Object[] { name }, null);
            //JP: WARUM FUNKTIONIERT DIESES getMessageResources()... NICHT ???
            //    da kommt dauernd nur <null> raus !!!
            String msg = getMessageResources().getMessage(ICatalogMessagesKeys.PCAT_IMS_UNKNOWN_ATTR, name);
            throw new CRMException(msg);
        }

        // if value is "*" replace this condition with some "SelectAllDocuments"
        if(value.equals(IMSConstants.VALUE_ALL) &&
            searchAllDocuments$attrName != null) {
            String oldName = name;  // only for debugging purpose
            name = searchAllDocuments$attrName;
            if(searchAllDocuments$attrValue != null)
                value = searchAllDocuments$attrValue;
            if(log.isDebugEnabled() || log.isInfoEnabled()) {
                StringBuffer sb = new StringBuffer("substituting '");
                sb.append(oldName);
                sb.append("'='*' with '");
                sb.append(name);
                sb.append("'='");
                sb.append(value);
                sb.append("'");
                log.info(ICatalogMessagesKeys.PCAT_INFO,
                                new Object[] { sb.toString() }, null);
            }
        }

        try {
            addQueryAttribute(name.getBytes(theEncoding),
                              oper,
                              value.getBytes(theEncoding),
                              type,
                              action,
                              termWeight);
                // logging
            if(log.isDebugEnabled()) {
                log.debug("Query - Attribute: " + name +
                                    ", Operator: "        + new String(oper) +
                                    ", Value: "           + value +
                                    ", Type: "            + new String(type) +
                                    ", Action: "          + ((action==null)?"":new String(action)) +
                                    ", Weight: "          + String.valueOf(termWeight) +
                                    ".");
            }
        }
        catch (UnsupportedEncodingException ex) {
            throw new CRMException(ex.getLocalizedMessage());
        }
        
        if(log.isDebugEnabled() || log.isInfoEnabled()) {
            dbgWhereClause += " (" + name + " " + getQAOperAsString(oper,type) + " " + value + ")";
        }
    }
 
    private String getQAOperAsString(byte[] oper, byte[] type) {
        
        if (oper.equals(OPERATOR_EQ)) {
            if(type.equals(TYPE_STRING)) return "equals";
            else if(type.equals(TYPE_TEXT)) return "contains";
            else if(type.equals(CONTENT_TYPE_AND)) return "contains_AND";
            else return "??opEQ:" + new String(type) + "??";
        }
        
        if (oper.equals(OPERATOR_NE)) {
            if(type.equals(TYPE_STRING)) return "not equals";
            else if(type.equals(TYPE_TEXT)) return "not contains";
            else if(type.equals(CONTENT_TYPE_AND)) return "not contains_AND";
            else return "??opNE:" + new String(type) + "??";
        }
        
        return "??op:" + new String(oper) + "??";
    }

    void addQueryAttributeList(String name, String[] values)
        throws CRMException {
            
        for (int i=0; i < values.length; i++) {
            addQueryAttribute(name, OPERATOR_EQ, values[i], TYPE_STRING);
            if(i > 0) {
                addQueryOperator(OPERATOR_OR);
            }    
        }
    }

    void addQueryOperator(byte[] Oper) throws CRMException {
        // append row to query table (forth table)
        JCO.Table tab4 = theTableList.getTable("QUERYTAB");
        tab4.appendRow();

        tab4.setValue(tab4.getNumRows(), "SECOUNT");
        tab4.setValue(Oper, "VALUE1");
        tab4.setValue(OPERATOR_EQ, "OPERATOR");
        tab4.setValue(ROW_TYPE_OPERATOR, "ROWTYPE");

            // logging
        log.debug("Query - Operator: " + new String(Oper) + ".");
        
        if(log.isDebugEnabled() || log.isInfoEnabled()) {
            dbgWhereClause += " " + getQueryOperatorAsString(Oper);
        }
    }
   
    private String getQueryOperatorAsString( byte[] Oper) {
        if (Oper.equals(OPERATOR_OR)) return "OR";
        else if (Oper.equals(OPERATOR_AND)) return "AND";
        else if (Oper.equals(OPERATOR_NOT)) return "NOT";
        else return "??Op:" + new String(Oper) + "??";
    }

    private String getSqlString() {
        String result = "select " + theReturnAttributes.toString() +
                        " from '" + dbgIndexName + "' where" + dbgWhereClause;
        if(dbgFromTo != null) {
            result += dbgFromTo;
        }
         
        if(dbgOrderBy != null) {
            result += dbgOrderBy;
        } 
        
        return result;
    }

    IMSResultSet executeQuery(IMSConnectionInfo connectionInfo)
        throws CRMException {
            
        log.debug("begin executeQuery(" + getSqlString() + ")");

        IMSResultSet result = null;
        JCoConnection connection = null;

        // output SQL-like statement on INFO level only:
        // but since isDebugEnabled does not(!!!) include isInfoEnabled...
        log.info(ICatalogMessagesKeys.PCAT_IMS_SQL_STATEMENT, new Object[] { getSqlString() }, null);

        try {
            // get the connection - it's definitly a jco connection
            connection = (JCoConnection)connectionInfo.getConnection();
            
            // execute query on server
            JCO.Function function = JCO.createFunction("SRET_INDEX_STORAGE_SEARCHING", theImportList, theExportList, theTableList);
            
            connection.execute(function);
                                                                                 
            CatalogJCoLogHelper.logCall("SRET_INDEX_STORAGE_SEARCHING", theImportList, theExportList, theTableList, log);
            
            // get return code
            int rcode = Integer.parseInt(theExportList.getString("RCODE"));
            
            // analyse return code
            if (rcode != 0) {
                String msg = getMessageResources().getMessage(ICatalogMessagesKeys.PCAT_IMS_RET_CODE,
                                                              theExportList.getString("RCODE").trim());
                throw new CRMException(msg);
            }

            // analyse number of hits
            int hits = Integer.parseInt(theExportList.getString("NUMOFHITS").trim());

            // log number of hits
            log.debug("Number of hts: " + hits);

            if (hits >= 0) {
                result = new IMSResultSetImpl(theReturnAttributes,
                                              theTableList.getTable("RESULTATTRTAB"),
                                              hits, theEncoding); // ??theTableList.getTable("RESULTDOCTAB").getNumRows()
            }
        }
        catch(BackendException ex) {
            throw new CRMException(ex.getLocalizedMessage());
        } 
        
        catch(JCO.AbapException ex) {
            throw new CRMException(ex.getLocalizedMessage());
        }
        
        finally {
            this.reset();
            connection.close();
        }

        log.debug("end   executeQuery()");

        return result;
    }

    void reset() {
        // discard actual parameters of the last rfc call
        theImportList.setValue(
            new byte[] {0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00},
            "SEARCHENG");
            
        theImportList.setValue(new byte[] {0x00,0x00,0x00} ,"CODEPAGE");

        theExportList.setValue(0, "NUMOFHITS");
        theExportList.setValue("", "RCODE");

        for(int i=0; i < theTableList.getFieldCount(); i++) {
            theTableList.getTable(i).deleteAllRows();
        }
            
        theReturnAttributes.clear();
        theSearchEngine = "";

        dbgIndexName = null;
        dbgWhereClause = "";
        dbgFromTo = null;
        dbgOrderBy = null;
    }

    /**
     * Adds an entry to the query parameter table
     */ 
    private void addQueryParameter(byte[] name, byte[] value1, byte[] value2)
        throws CRMException {
        // append row to query parameter tab (second table)
        JCO.Table tab2 = theTableList.getTable("QUERYPARAMTAB");
        tab2.appendRow();

        tab2.setValue(name, "NAME");
        tab2.setValue(value1, "VALUE1");

        if(name == ROW_FROM_TO)  {
            tab2.setValue(value2, "VALUE2");
            tab2.setValue(OPERATOR_BT, "OPERATOR");
        }
        else if(name == SORT_ORDER) {
            tab2.setValue(value2, "VALUE2");
            tab2.setValue(OPERATOR_EQ, "OPERATOR");
        }
        else if(name == QUERY_LANGUAGE) {
            tab2.setValue(OPERATOR_EQ, "OPERATOR");
        }
        else {
            String msg = getMessageResources().getMessage(
                ICatalogMessagesKeys.PCAT_IMS_INVALID_QUERYPARAM);
            throw new CRMException(msg);
        }
    }

    /**
     * Adds entries to the query attribute tables
     */ 
    private void addQueryAttribute(byte[] name, byte[] oper, byte[] value,
                                   byte[] type, byte[] action, int weight) throws CRMException
    {
        try {
            // append row to query table (forth table)
            JCO.Table tab4 = theTableList.getTable("QUERYTAB");
            tab4.appendRow();
            int nrRows = tab4.getNumRows();

            tab4.setValue(nrRows, "SECOUNT");
            tab4.setValue(name, "LOCATION");
            tab4.setValue(value, "VALUE1");
            tab4.setValue(oper, "OPERATOR");
            tab4.setValue(ROW_TYPE_ATTRIBUTE, "ROWTYPE");

            // append rows to term attribute table (fifth table)
            JCO.Table tab5 = theTableList.getTable("TERMATTRTAB");

            tab5.appendRow();
            tab5.setValue(TERM_CONTENTTYPE, "NAME");
            tab5.setValue(type, "VALUE1");
            tab5.setValue(nrRows, "QTREFER");

            if(action != null) {
                tab5.appendRow();
                tab5.setValue(TERM_ACTION, "NAME");
                tab5.setValue(action, "VALUE1");
                tab5.setValue(OPERATOR_EQ, "OPERATOR");
                tab5.setValue(nrRows, "QTREFER");
            }

            // set default term weight
            if(weight == 0) {
                weight = 10000;
            }

                // set term weight
            tab5.appendRow();
            tab5.setValue(TERM_WEIGHT, "NAME");
            tab5.setValue(String.valueOf(weight).getBytes(theEncoding), "VALUE1");
            tab5.setValue(nrRows, "QTREFER");

        }
        catch (UnsupportedEncodingException ex) {
            throw new CRMException(ex.getLocalizedMessage());
        }
    }

    /** 
     * Adds entries to the document attribute table
     */
    private void addDocumentAttribute(byte[] name) throws CRMException {
        JCO.Table tab3 = theTableList.getTable("DOCATTRTAB");
        tab3.appendRow();
        tab3.setValue(name, "ATTRNAME");
        tab3.setValue(DOCATTR_TYPE_STRING, "ATTRTYPE");
    }
}
