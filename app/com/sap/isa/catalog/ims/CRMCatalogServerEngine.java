/*****************************************************************************
  Copyright (c) 2000, SAPMarkets Europe GmbH, Germany, All rights reserved.
  Created:      Feb 19 2002

  $Id: //sap/ESALES_base/30_SP_COR/src/_pcatAPI/java/com/sap/isa/catalog/ims/CRMCatalogServerEngine.java#3 $
  $Revision: #3 $
  $Change: 127412 $
  $DateTime: 2003/04/30 17:25:07 $
*****************************************************************************/
package com.sap.isa.catalog.ims;

import java.util.ArrayList;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Locale;
import java.util.Properties;

import javax.xml.transform.Source;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.w3c.dom.Text;
import org.xml.sax.InputSource;

import com.sap.isa.catalog.ICatalogMessagesKeys;
import com.sap.isa.catalog.boi.CatalogException;
import com.sap.isa.catalog.boi.IActor;
import com.sap.isa.catalog.boi.ICatalogInfo;
import com.sap.isa.catalog.boi.IClient;
import com.sap.isa.catalog.boi.IServer;
import com.sap.isa.catalog.boi.IServerEngine;
import com.sap.isa.catalog.impl.Catalog;
import com.sap.isa.catalog.impl.CatalogCompositeComponent;
import com.sap.isa.catalog.impl.CatalogServerEngine;
import com.sap.isa.catalog.impl.CatalogSite;
import com.sap.isa.core.TechKey;
import com.sap.isa.core.eai.BackendBusinessObjectMetaData;
import com.sap.isa.core.eai.BackendBusinessObjectParams;
import com.sap.isa.core.eai.BackendException;
import com.sap.isa.core.eai.Connection;
import com.sap.isa.core.eai.ConnectionDefinition;
import com.sap.isa.core.eai.ConnectionFactory;
import com.sap.isa.core.eai.sp.jco.BackendBusinessObjectSAP;
import com.sap.isa.core.eai.sp.jco.JCoConnection;
import com.sap.isa.core.eai.sp.jco.JCoConnectionEventListener;
import com.sap.isa.core.eai.sp.jco.JCoManagedConnectionFactory;
import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.core.logging.LogUtil;
import com.sap.isa.core.util.table.ResultData;

/**
 * CRMCatalogServerEngine.java
 *
 *
 * Created: Tue Feb 19 16:48:00 2002
 *
 * @version 1.0
 */

final public class CRMCatalogServerEngine extends CatalogServerEngine implements BackendBusinessObjectSAP {
    
    private JCoConnection theSAPConnection;
    private boolean isStagingSupportDetermined = false;
   
    // the logging instance
    private static IsaLocation log =
        IsaLocation.getInstance(CRMCatalogServerEngine.class.getName());

    /**
     * Creates a new <code>CRMCatalogServerEngine</code> instance.
     * Only used when created via the BEM mechanism.
     * 
     * @deprecated don't use this catalog server engine anymore, instead TrexCRMCatalogServerEngine should 
     *              be used. The underlying catalogue won't work with the the latest version of the replication.
     */
    public CRMCatalogServerEngine() {
        theType = IServerEngine.ServerType.CRM;
    }
    
    /**
     * Creates a new <code>CRMCatalogServerEngine</code> instance.
     *
     * @param aGuid a <code>String</code> value
     * @param aSite a <code>CatalogSite</code> value
     * @param aServerConfigFile a <code>Document</code> value
     * @param theConnectionProperties a <code>Properties</code> value
     * 
     * @deprecated don't use this catalog server engine anymore, instead TrexCRMCatalogServerEngine should 
     *             be used. The underlying catalogue won't work with the the latest version of the replication.
     */
    public CRMCatalogServerEngine (String aGuid,
                                   CatalogSite aSite,
                                   Document aServerConfigFile)
        throws BackendException {
            
        super(aGuid, aSite,aServerConfigFile);
        this.theMessageResources=theSite.getMessageResources();
        theType = IServerEngine.ServerType.CRM;
    }

   /************************ BackendBusinessObject ***********************/
    
    public void initBackendObject(
        Properties theEAIProperties,
        BackendBusinessObjectParams aParams)
        throws BackendException
    {
            //store anything that might come form the eai config file
        theProperties.putAll(theEAIProperties);
        Properties theServerconnectionPropreties =
            getBackendObjectSupport().
            getMetaData().
            getDefaultConnectionDefinition().
            getProperties();

            //overlay it with the default connection properties from the default connection.
        theProperties.putAll(theServerconnectionPropreties);
        
        CatalogSite.CatalogServerParams theParams =
            (CatalogSite.CatalogServerParams)aParams;
        theGuid=theParams.getGuid();
        theSite = theParams.getSite();
        this.theMessageResources = theSite.getMessageResources();
        theActor = theSite.getActor();
        
            //in case the client is a different than in the default connection create a new one
        setDefaultConnection(theActor);

        if (log.isDebugEnabled()) {
            
            StringBuffer aBuffer = new StringBuffer();
            Enumeration theKeys = theProperties.keys();
            while(theKeys.hasMoreElements()) {
                String aKey = (String)theKeys.nextElement();
                aBuffer.append(aKey + ":");
                if (aKey.equals("passwd"))
                    aBuffer.append("?");
                else
                    aBuffer.append(theProperties.getProperty(aKey));
                if (theKeys.hasMoreElements()) {
                    aBuffer.append("; ");
                } // end of if ()
            }

            String aMessage =
                theMessageResources.getMessage(
                    ICatalogMessagesKeys.PCAT_SRV_PROPS,
                    theGuid,
                    aBuffer.toString());
            
            log.debug(aMessage);
        } // end of if ()
        
        Document aConfigFile = theParams.getDocument();
        this.theConfigDocument=aConfigFile;        
        init(aConfigFile);
        return;
    }

    public void destroyBackendObject() {
        return;
    }

    /*************************** BackendBusinessObjectSAP **********************/
        
    /**
     * This method is used to add JCo-call listener to this back-end object.
     * A JCo listener will be notified before a JCo call is being executed
     * and after the JCo call has been executed.
     *
     * @param listener         a listener for JCo calls
     */
    public void addJCoConnectionEventListener(JCoConnectionEventListener listener) {
        theConnectionFactory.addConnectionEventListener(listener);
    }

    public JCoConnection getDefaultJCoConnection(Properties props)
        throws BackendException {
        return (JCoConnection) theConnectionFactory.getDefaultConnection(
            getDefaultJCoConnection().getConnectionFactoryName(),
            getDefaultJCoConnection().getConnectionConfigName(),
            props);
    }

    /*synchronized*/ public JCoConnection getDefaultJCoConnection() {
        if (theSAPConnection == null)
            setDefaultConnection(getConnectionFactory().getDefaultConnection());

        return theSAPConnection;
    }

    public JCoConnection getJCoConnection(String conKey)
        throws BackendException {
        return (JCoConnection)getConnectionFactory().getConnection(conKey);
    }

    public JCoConnection getModDefaultJCoConnection(Properties conDefProps)
        throws BackendException {
        ConnectionFactory factory = getConnectionFactory();
        JCoConnection defConn = (JCoConnection)factory.getDefaultConnection();

        return (JCoConnection)factory.getConnection(
            defConn.getConnectionFactoryName(),
            defConn.getConnectionConfigName(),
            conDefProps);
    }

    /****************************** CachableItem ***************************/
    
    public boolean isCachedItemUptodate() {
        return true;
    }

    /**
     * Generates a key for the cache from the passed parameters.<br>
     * This method is called from the {@link com.sap.isa.catalog.cache.CatalogCache}
     * and has to be <code>public static</code>.
     *
     * @param aMetaInfo  meta information from the EAI layer
     * @param aClient    the client spec of the catalog
     * @param aServer    the server spec of the catalog
     */
    public static String generateCacheKey(
        BackendBusinessObjectMetaData aMetaInfo,
        IActor aClient,
        IServer aServer) {
        StringBuffer aBuffer = new StringBuffer(CRMCatalogServerEngine.class.getName());
        
        ConnectionDefinition connDef =
            aMetaInfo.getDefaultConnectionDefinition();

            // create system id
            // special properties for the connection
        String system = null;
        Properties servProps = aServer!=null?aServer.getProperties():null;
        if (servProps != null) {
            system = generateKeySAPSystem(
                servProps.getProperty(JCoManagedConnectionFactory.JCO_CLIENT),
                servProps.getProperty(JCoManagedConnectionFactory.JCO_R3NAME),
                servProps.getProperty(JCoManagedConnectionFactory.JCO_ASHOST),
                servProps.getProperty(JCoManagedConnectionFactory.JCO_SYSNR));
        }

            // default properties
        if (system == null) {
            Properties defProps = connDef.getProperties();
            system = generateKeySAPSystem(
                defProps.getProperty(JCoManagedConnectionFactory.JCO_CLIENT),
                defProps.getProperty(JCoManagedConnectionFactory.JCO_R3NAME),
                defProps.getProperty(JCoManagedConnectionFactory.JCO_ASHOST),
                defProps.getProperty(JCoManagedConnectionFactory.JCO_SYSNR));
        }

        String theServerGUID =
            (aServer!=null?
             aServer.getGuid():
             aMetaInfo.getBackendBusinessObjectConfig().getType());
        
        aBuffer.append(theServerGUID);
        aBuffer.append(system);
        
            // now build and return the key
        return aBuffer.toString();
    }

    /**
     * Generates a key from the passed parameters for a SAP system.
     * If the parameters are incomplete so that no valid key can be
     * built <code>null</code> will be returned.
     *
     * @param client  the client of the r/3 system
     * @param r3name  the name of the r/3 system
     * @param ashost  the application server
     * @param sysnr   the system number
     * @return key for a SAP system
     */
    private static String generateKeySAPSystem(String client, String r3name,
                                               String ashost, String sysnr) {
        String key = null;

        if (client != null && client.length() != 0) {
            if (r3name != null && r3name.length() != 0)
                key = r3name + client;

            else if (ashost != null && ashost.length() != 0 && sysnr != null && sysnr.length() != 0)
                key = ashost + sysnr + client;
        }
        return key;
    }

    /*******************************************************************************/

    /**
     * Sets the default connection to the system where the infos about the
     * catalog are located.
     *
     * @param conn  the default connection
     */
    private void setDefaultConnection(Connection conn) {
        theSAPConnection = (JCoConnection)conn;
    }

    /***  implementations of abstract methods form CatalogServerEngine *****/

    /********************************* from IServer ****************************/
    
    /**
     * Not appropriate in the CRM case!
     * 
     * @return an <code>int</code> value = -1
     */
    public int getPort() {
        return -1;
    }
    
    /**
     * Not appropriate in the CRM case!
     *
     * @return a <code>String</code> value ="";
     */
    public String getURLString() {
        return "";
    }
    
    /************************* IServerEngine *******************************/
    
    /**
     * Nothing to do in the CRM case.
     * The Catalog infos are fetched from the crm system.
     *
     */
    protected void init(Document theConfigFile) {
            //use the supplied catalogs as filter
        NodeList aCatalogList = theConfigFile.getElementsByTagName("Catalog");
        if (aCatalogList!=null && aCatalogList.getLength()>0) {
            buildCatalogInfosRemote(aCatalogList);
        }
        return;
    }

    public boolean hasServerFeature(IServerEngine.ServerFeature aFeature) {
        return false;
    }

    public IServerEngine.ServerFeature[] getServerFeatures() {
        return new IServerEngine.ServerFeature[]{};
    }
    
    /**
     * Create a new CRMCatalog instance.
     * Only called if the catalog is not cached.
     *
     * @param aGuid a <code>String</code> value
     * @param aClient an <code>IClient</code> value
     * @return a <code>Catalog</code> value
     */
    protected Catalog getCatalogRemote(CatalogInfo aCatalogInfo, IClient aClient)
        throws CatalogException {
        
        CRMCatalogInfo aCRMCatalogInfo=(CRMCatalogInfo) aCatalogInfo;
        // if catalogue staging is used, don't determine the connection info from
        // the catalogInfo, instead the connection info will be determioned in the 
        // catlogues constructor 
        IMSConnectionInfo theIMSConnectionInfo = null;
        
        if (STAGING_NOT_SUPPORTED.equals(getStagingSupport())) {
            theIMSConnectionInfo = aCRMCatalogInfo.getIMSConnectionInfo();
        }
        
        String theGuid = aCRMCatalogInfo.getGuid();
        int size = theGuid.length();
        if (size < 60) {
            StringBuffer aBuffer = new StringBuffer(theGuid);
            do {
                aBuffer.append(' ');
            } 
            while (aBuffer.length() < 60) ;
            theGuid = aBuffer.toString();
        } // end of if ()
        
        CRMCatalog aNewCRMCatalog = null;
        String theMaxNumAsString =
            aCatalogInfo.getProperty(
                CatalogCompositeComponent.PROPERTY_MAX_NUMBER_OF_ITEM_LEAFS);
        int theInt = -1;
        try //just in case the string can't be parsed
        {
            theInt = Integer.parseInt(theMaxNumAsString);
        } catch (Throwable t) {
        	log.debug(t.getMessage());
                //you got your fair chance to set the number
        } // end of try-catch                        int theInt 
        
        if (theInt != -1) {
                new CRMCatalog(
                    this,
                    theIMSConnectionInfo,
                    aClient,                
                    theGuid,
                    aCRMCatalogInfo.getLanguage(),
                    aCRMCatalogInfo.getCodepage(),
                    aCRMCatalogInfo.getSearchEngine(),
                    this.theMessageResources,
                    theInt);            
        }
        else {
            aNewCRMCatalog =
                new CRMCatalog(
                    this,
                    theIMSConnectionInfo,
                    aClient,                
                    theGuid,
                    aCRMCatalogInfo.getLanguage(),
                    aCRMCatalogInfo.getCodepage(),
                    aCRMCatalogInfo.getSearchEngine(),
                    this.theMessageResources);
        } // end of else
        return aNewCRMCatalog;   
    }
    
    protected void addCatalogRemote(
         InputSource aSource,
         Source aMappingDcoument) {
        return;
    }

    protected void addCatalogRemote(Catalog catalog) {
        throw new UnsupportedOperationException("Not implemented yet!");
    }

    protected void deleteCatalogRemote(CatalogServerEngine.CatalogInfo aCatalogInfo) {
        return;
    }

    protected void deleteCatalogsRemote() {
        return;
    }

    protected Catalog createCatalogRemote() {
        return null;
    }

    /**
     * Determines for the given instance the replication timestamp
     * as known in the A-Index of Trex.
     * Fallback: determination of the replication timestamp as known
     * in the CRM.
     *
     * @param aCRMCatalog a <code>CRMCatalog</code> whose timestamp is asked for
     * @return a <code>String</code> the correct time stamp or "" in case of failure.
     */   
    String getCurrentReplTimeStamp(CRMCatalog aCRMCatalog) {
        String theReplTimeStamp ="";
        IMSConnectionInfo theIMSConnectionInfo; 
        final String TIMESTAMP = "TIMESTAMP";
        String myResourceKey = "";
        
        try {
            // get information about A index of CRMcatalog
            IMSIndexInfo info  = aCRMCatalog.getIndexInfo(IMSConstants.INDEX_LEVEL_A,"","");
            //List attributeList = info.getAttributes();
        
          // check if required attribute TIMESTAMP exists on index
            if(info.hasAttribute(TIMESTAMP)) {
                if(log.isInfoEnabled()) 
                    log.info(ICatalogMessagesKeys.PCAT_INFO,
                        new Object[]{"getCurrentReplTimeStamp: retrieving attribute '" + TIMESTAMP + "' from index"}, null);
            
                // read replication timestamp out of A-Index (trex)
                theIMSConnectionInfo = aCRMCatalog.getIMSConnectionInfo();	
                IMSProxy proxy = IMSProxy.createProxy(theIMSConnectionInfo,
                                                theMessageResources);
                // set query environment (use default settings)
                proxy.setQueryEnvInitial(aCRMCatalog.getSearchEngine(),
                                      aCRMCatalog.getCodepage(),
                                      aCRMCatalog.getLanguage(),
                                      IMSConstants.ATTR_CATALOG,     // searchAllDocuments$attrName
                                      aCRMCatalog.getName().trim()); // searchAllDocuments$attrValue
                proxy.setQueryEnv(info);
                
                // fill in proxy (wanna get root category from A index where TIMESTAMP is contained)
                proxy.addQueryAttribute(IMSConstants.ATTR_AREA,
                                    IMSConstantsQuery.OPERATOR_EQ,"$ROOT");
                proxy.addReturnAttribute(TIMESTAMP);
                proxy.setReturnInterval(1,2);
                IMSResultSet results = proxy.executeQuery(theIMSConnectionInfo);
                        
                // receive and check return values
                int count = results.getRowCount();
                if (count < 1) {
                    log.info(ICatalogMessagesKeys.PCAT_INFO,
                         new Object[]{"No '" + TIMESTAMP + "' found on index! - will try to get timestamp from CRM"}, null);
                } 
                else {
                    if (count > 1) {
                        log.warn(ICatalogMessagesKeys.PCAT_WARNING,
                            new Object[]{"More than one ( " + count + ") document on index contains an attribute called 'TIMESTAMP' - first one is used!"}, null);
                    }
                    Iterator iter = results.iterator();
                    while (iter.hasNext()) {
                        IMSResultSetRow row = (IMSResultSetRow)iter.next();
                        theReplTimeStamp = row.getValue(TIMESTAMP);
                        break;
                    }
                    if(log.isInfoEnabled()) {
                        String msg = "getCurrentReplTimeStamp for catalog '"
                                     + aCRMCatalog.getName().trim()
                                     + "' returns following TIMESTAMP from index: "
                                     + theReplTimeStamp
                                     + ".";
                        log.info(ICatalogMessagesKeys.PCAT_INFO,
                            new Object[]{msg}, null);
                    }
                    return theReplTimeStamp;
                }
            }// of if(info.hasAttribute(TIMESTAMP))  
            
        }//try
        catch (CRMException ex) {
            String msg = "getting TIMESTAMP from TREX failed with following exception: "
                         + ex.getLocalizedMessage();
            log.warn(ICatalogMessagesKeys.PCAT_EXCEPTION,
                             new Object[]{msg}, ex);
            theReplTimeStamp ="";
        }// catch

        // in case of failure (reading timestamp out of trex) or
        // attribute TIMESTAMP is not available on index: get it from CRM    	
        try {   
            log.warn(ICatalogMessagesKeys.PCAT_WARNING, new Object[]
                {"No attribute called 'TIMESTAMP' available from index or reading from index failed completely - now trying to get timestamp from CRM."},null);     
         
            theReplTimeStamp =
                CRMRfcMethods.getIMSReplTimeStamp(getDefaultJCoConnection(),
                                                     aCRMCatalog.getName(),
                                                     aCRMCatalog.getVariant());
        }
        catch(CRMException ex) {   
            log.error(
                ICatalogMessagesKeys.PCAT_ERR_DETER_CAT_REPLI_STATUS,
                new Object[] {aCRMCatalog.getGuid()}, ex);            
            theReplTimeStamp = "";
        }
        
        if(log.isInfoEnabled()) {   
            String msg = "'getCurrentReplTimeStamp' for CRMCatalog "
                         + aCRMCatalog.getName()
                         + " returns following TIMESTAMP from CRM (not from index!): "
                         + theReplTimeStamp
                         + ".";
            log.info(ICatalogMessagesKeys.PCAT_INFO, new Object[]{msg}, null);
        }     
                    
        return theReplTimeStamp;       
    }

    // comparison of timestamp of the Catalog Objekt and timestamp of crm 
    public boolean isCatalogUpToDate(Catalog aCatalog)
    {
        CRMCatalog aCRMCatalog = (CRMCatalog)aCatalog;
        
        boolean result = true;
            // save old time stamp
        String timeStamp = aCRMCatalog.getReplTimeStamp(); 
        String theReplTimeStamp = getCurrentReplTimeStamp(aCRMCatalog);

        return theReplTimeStamp.equals(timeStamp);
    }
    
    /**
     * Build the list of catalog infos known to that server.
     * The node-list of Catalog elements is used as a filter.
     * if the node list is empty or null no catalog info elements will be build.
     *
     * @param theCatalogElements a <code>NodeList</code> value
     */
    synchronized protected void buildCatalogInfosRemote(NodeList theCatalogElements) {
        ArrayList theNamePattern = new ArrayList();
        if (theCatalogElements!=null && theCatalogElements.getLength()>0) {
            int size = theCatalogElements.getLength();
            for (int i =0; i < size; i++) {
                Element aCatlogElement = (Element)theCatalogElements.item(i);
                CatalogInfo aCatalogInfo =
                    new CatalogInfo(
                        aCatlogElement.getAttribute("ID"),
                        CRMCatalog.class.getName());
                theNamePattern.add(aCatalogInfo);
                NodeList theDetails = aCatlogElement.getElementsByTagName("Detail");
                int length = theDetails.getLength();
                for (int j = 0; j < length; j++) {
                    Element aDetail = (Element) theDetails.item(j);
                    String theID = aDetail.getAttribute("ID");
                    NodeList theValueChilds = aDetail.getElementsByTagName("Value");
                    int childNum = theValueChilds.getLength();
                    for (int k = 0; k < childNum; k++) {
                        Element  aValue = (Element) theValueChilds.item(k);
                        String theLocale = aValue.getAttribute("xml:lang");
                        Text theContent = (Text)aValue.getFirstChild();
                        aCatalogInfo.setProperty(theID,theLocale,theContent.getData());
                    } // end of for ()
                }
            } // end of for ()
        } // end of if ()
        
        Iterator anIter = theNamePattern.iterator();
        while (anIter.hasNext()) {
            CatalogInfo aCatalogInfo = (CatalogInfo)anIter.next();
	        if (log.isDebugEnabled()) {
                log.debug("load catalog infos for '" + aCatalogInfo.getName() + "'");
            }
            buildCatalogInfosRemote(aCatalogInfo.getName());
        }
        
        return;
    }

    /**
     * Build a catalog info list based on the given name pattern.
     * if null is supplied all catalogs known to this server will be returned.
     *
     * @param aCatalogNamePattern a <code>String</code> value
     */
    synchronized protected void buildCatalogInfosRemote(String aCatalogNamePattern) {
            //get all indices that are known in the back-end
        ResultData theIndices = null;
        try {
            if(aCatalogNamePattern.length() >= 30) {
                aCatalogNamePattern = aCatalogNamePattern.substring(0,29).trim();
            }
            theIndices = CRMRfcMethods.getIndexList(
                getDefaultJCoConnection(),
                aCatalogNamePattern);
        }
        catch (CRMException tce) {
            log.error(LogUtil.APPS_COMMON_INFRASTRUCTURE, ICatalogMessagesKeys.PCAT_ERR_INIT_INSTANCE,
                            new Object[]{this.getClass().getName(), tce.getLocalizedMessage()},
                            tce);
            return;
        } // end of try-catch

        HashMap theBuildCatInfos = new HashMap();
        // initialize the HashMap from the already loaded CatalogInfos
        Iterator aCatalogInfoIter = theCatalogInfos.iterator();
        while (aCatalogInfoIter.hasNext()) {
            ICatalogInfo anInfo = (ICatalogInfo)aCatalogInfoIter.next();
            theBuildCatInfos.put(anInfo.getGuid(), anInfo);
        } // end of while ()

        // get the info of backend system for later use (theDescription)
        String backendInfo = getDefaultJCoConnection().getAttributes().getSystemID()
                     + ":" + getDefaultJCoConnection().getAttributes().getClient();

        theIndices.beforeFirst();
        while (theIndices.next()) {
            String theName = theIndices.getString("PCAT");
            String theVariant  = theIndices.getString("PCATVARIANT");
                
            int sizeName = theName.length();
                
            char[] theNameChars = new char[30];
            for (int i = 0; i < 30; i++) {
                theNameChars[i] = (i<sizeName? theName.charAt(i): ' ');
            } // end of for ()
                
            StringBuffer aBuffer = new StringBuffer();
            aBuffer.append(theNameChars);
            aBuffer.append(theVariant);
                
            String theCatalogGuid = aBuffer.toString();
            if (theBuildCatInfos.containsKey(theCatalogGuid)) {
                continue;
            } // end of if ()
            else {
                String theDescription = theSite.getMessageResources().getMessage(Locale.ENGLISH,
                                                                                 ICatalogMessagesKeys.PCAT_CAT_DOWNLOADED_SYSTEM,
                                                                                 new Object[]{ backendInfo } );
                try {
                    CRMCatalogInfo aCatalogInfo =
                        new CRMCatalogInfo(
                            theCatalogGuid,
                            CRMCatalog.class.getName(),
                            theName,
                            theVariant,
                            theDescription,
                            this);
                    theCatalogInfos.add(aCatalogInfo);
                    theBuildCatInfos.put(theCatalogGuid, aCatalogInfo);                    
                } 
                catch (CatalogException ce) {
                    log.error(
                        ICatalogMessagesKeys.PCAT_OBJ_INSTANTIATED,
                        new Object[]{CRMCatalogInfo.class.getName()},
                        ce);
                } // end of try-catch
            } // end of else
        } // end of while ()
        return;
    }
    
    /************************************************/
    
    public class CRMCatalogInfo extends CatalogServerEngine.CatalogInfo {
        public static final String PROPERTY_VARIANT ="variant";
        public static final String PROPERTY_CODE_PAGE ="codePage";
        public static final String PROPERTY_LANGUAGE = "language";
        public static final String PROPERTY_SEARCH_ENGINE = "searchEngine";
        
        private IMSConnectionInfo theIMSConnectionInfo;
        
        public CRMCatalogInfo(String aCatalogGuid,
                              String theImplClassName,
                              String theName,
                              String theVariant,                              
                              String theDescription,
                              CRMCatalogServerEngine theServerEngine)
            throws CatalogException {
            super(aCatalogGuid,theImplClassName);
            setProperty(PROPERTY_NAME, Locale.ENGLISH.toString(),theName);
            setProperty(PROPERTY_DESCRIPTION, Locale.ENGLISH.toString(),theDescription);
            setProperty(PROPERTY_VARIANT,null,theVariant);
            
            if (STAGING_NOT_SUPPORTED.equals(theServerEngine.getStagingSupport())) {
                theIMSConnectionInfo = CRMRfcMethods.getIMSConnectionInfo(this, theServerEngine);
            } 
        }

        public IMSConnectionInfo getIMSConnectionInfo() {
            return theIMSConnectionInfo;
        }
        
        public String getVariant() {
            return getProperty(PROPERTY_VARIANT);
        }

        public String getSearchEngine() {
            return getProperty(PROPERTY_SEARCH_ENGINE);
        }
        
        public String getLanguage() {
            return getProperty(PROPERTY_LANGUAGE);
        }

        public String getCodepage() {
            return getProperty(PROPERTY_CODE_PAGE);
        }

        public void setSearchEngine(String aSearchEngine) {
            setProperty(PROPERTY_SEARCH_ENGINE,null,aSearchEngine);
        }
        
        public void setLanguage(String aLanguage ) {
            setProperty(PROPERTY_LANGUAGE,null,aLanguage);
        }

        public void setCodepage(String aCodePage) {
            setProperty(PROPERTY_CODE_PAGE,null,aCodePage);
        }
    }
    
    /**
     * This method sets a new default connection if the necessary connection
     * settings are passed via the client. 
     *
     * @param client  reference to a specific client
     */
    synchronized private void setDefaultConnection(IActor client) {
        Properties props = null;

        Properties theDefaultConnectionProperties =
            getBackendObjectSupport().getMetaData().getDefaultConnectionDefinition().getProperties();
        String theClientName = client.getName();
        String theClientPWD = client.getPWD();
        String theDefaultName=
            theDefaultConnectionProperties.getProperty(JCoManagedConnectionFactory.JCO_USER);
        String theDefaultPWD=
            theDefaultConnectionProperties.getProperty(JCoManagedConnectionFactory.JCO_PASSWD);
        
            // check if special client was specified and if it was different from the default connection
        if (theClientName!= null        &&
            theClientPWD!= null         &&
            theClientName.length() != 0 &&
            theClientPWD.length() != 0 &&
            !theClientName.equals(theDefaultName) &&
            !theClientPWD.equals(theDefaultPWD))  {
                // define new client settings
            props = new Properties();
            props.setProperty(  JCoManagedConnectionFactory.JCO_USER,
                                theClientName);
            props.setProperty(  JCoManagedConnectionFactory.JCO_PASSWD,
                                theClientPWD);
        }
            // change the default connection
        if(props != null) {
            try {
                JCoConnection modConn =getModDefaultJCoConnection(props);
                if(modConn.isValid())  {
                    this.theProperties.putAll(props);
                    this.setDefaultConnection(modConn);
                }
                
            }
            catch(Exception ex) {
                log.warn(ICatalogMessagesKeys.PCAT_EXCEPTION,
                                   new Object[] { ex.getLocalizedMessage() }, null);
            }
        }
        return;
    }    
}