/*****************************************************************************
  Copyright (c) 2001, SAPMarkets Europe GmbH, Germany, All rights reserved.

  $Revision: #3 $
  $Date: 2003/08/22 $
*****************************************************************************/

package com.sap.isa.catalog.ims;

/**
 * This interface represents an entry in a column of a row.
 *
 * @version     1.0
 */
public interface IMSResultSetItem {

  /**
   * Constant that defines the character which is used to separate the
   * concatentated multi values.
   */
  public static final char SEPARATOR = ';';

  /**
   * Constant that defines the length of an entry of the returned table of
   * the rfc proxy.
   */
  public static final int LINE_LENGTH = 64;

  /**
   * Returns the value of the item. In case of a multi value the values are
   * concatenated separated by the character which is defined through
   * <code>IMSResultSetItem.SEPARATOR</code>.
   *
   * @return  value of the item.
   */
  public String getValue();

  /**
   * Returns the values of the item. This method should be used in case
   * of multi values.
   *
   * @return  string array of values
   */
  public String[] getValues();

  /**
   * Returns the column name of the item.
   *
   * @return  name of column
   */
  public String getColumnName();

  /**
   * Returns true if the given item contains a multi value.
   *
   * @return  true if multi value, otherwise false
   */
  public boolean isMultiValue();

}