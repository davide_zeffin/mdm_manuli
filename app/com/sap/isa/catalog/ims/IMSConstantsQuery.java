/*****************************************************************************
  Copyright (c) 2001, SAPMarkets Europe GmbH, Germany, All rights reserved.
  Created:  12 February 2001

  $Revision: #2 $
  $Date: 2003/05/15 $
*****************************************************************************/

package com.sap.isa.catalog.ims;

/**
 * This interface contains all constants which are necessary to formulate
 * queries for the {@link IMSRfcProxy}.
 * @version     1.0
 */
interface IMSConstantsQuery {

  /**
   * Constant for the 'query language' entry.
   */
  public final static byte[] QUERY_LANGUAGE = { 0x01 }; // QL:   0x01

  /**
   * Constant for the 'time out' entry.
   */
  public final static byte[] TIME_OUT       = { 0x02 }; // TO:   0x02

  /**
   * Constant for the 'row from to' entry.
   */
  public final static byte[] ROW_FROM_TO    = { 0x03 }; // RFT:  0x03

  /**
   * Constant for the 'max hits' entry.
   */
  public final static byte[] MAX_HITS       = { 0x04 }; // MHT:  0x04

  /**
   * Constant for the 'sort order' entry.
   */
  public final static byte[] SORT_ORDER     = { 0x0C }; // SO:   0x0C


  /**
   * Constant for sort order ascending.
   */
  public final static byte[] ORDER_ASC      = { 0x31 }; // ASC:  '1' = 0x31

  /**
   * Constant for sort order descending.
   */
  public final static byte[] ORDER_DESC     = { 0x32 }; // DESC: '2' = 0x32


  /**
   * Constant for the content type STRING. This constant has to be used in
   * table TERMATTRTAB.
   */
  public final static byte[] TYPE_STRING  = { 0x53 }; // 'S' == 83 == 0x53

  /**
   * Constant for the content type INTEGER. This constant has to be used in
   * table TERMATTRTAB.
   */
  public final static byte[] TYPE_INTEGER = { 0x49 }; // 'I' == 73 == 0x49

  /**
   * Constant for the content type FLOAT. This constant has to be used in
   * table TERMATTRTAB.
   */
  public final static byte[] TYPE_FLOAT   = { 0x46 }; // 'F' == 70 == 0x46

  /**
   * Constant for the content type DATE. This constant has to be used in
   * table TERMATTRTAB.
   */
  public final static byte[] TYPE_DATE    = { 0x44 }; // 'D' == 68 == 0x44

  /**
   * Constant for the content type NATURAL. This constant has to be used in
   * table TERMATTRTAB.
   */
  public final static byte[] TYPE_TEXT = { 0x4E }; // i.e. NATURAL 'N' == 78 == 0x4E

  /**
   * Constant for the content type TIME. This constant has to be used in
   * table TERMATTRTAB.
   */
  public final static byte[] TYPE_TIME    = { 0x54 }; // 'T' == 84 == 0x54

  /**
   * Constant for the content type UTC. This constant has to be used in
   * table TERMATTRTAB.
   */
  public final static byte[] TYPE_UTC     = { 0x55 }; // 'U' == 85 == 0x55


  public final static byte[] CONTENT_TYPE_AND = { 0x41 }; // 'A' == 65 == 0x41

  /**
   * Constant for the attribute type STRING. This constant has to be used in
   * table DOCATTRTAB.
   */
  public final static byte[] DOCATTR_TYPE_STRING  = { 0x38, 0x33 }; // '83'

  /**
   * Constant for the attribute type INTEGER. This constant has to be used in
   * table DOCATTRTAB.
   */
  public final static byte[] DOCATTR_TYPE_INTEGER = { 0x37, 0x33 }; // '73'

  /**
   * Constant for the attribute type FLOAT. This constant has to be used in
   * table DOCATTRTAB.
   */
  public final static byte[] DOCATTR_TYPE_FLOAT   = { 0x37, 0x30 }; // '70'

  /**
   * Constant for the attribute type DATE. This constant has to be used in
   * table DOCATTRTAB.
   */
  public final static byte[] DOCATTR_TYPE_DATE    = { 0x36, 0x38 }; // '68'

  /**
   * Constant for the attribute type NATURAL. This constant has to be used in
   * table DOCATTRTAB.
   */
  public final static byte[] DOCATTR_TYPE_NATURAL = { 0x37, 0x38 }; // '78'

  /**
   * Constant for the attribute type TIME. This constant has to be used in
   * table DOCATTRTAB.
   */
  public final static byte[] DOCATTR_TYPE_TIME    = { 0x38, 0x34 }; // '84'

  /**
   * Constant for the attribute type UTC. This constant has to be used in
   * table DOCATTRTAB.
   */
  public final static byte[] DOCATTR_TYPE_UTC     = { 0x38, 0x35 }; // '85'


  /**
   * Constant for operator 'between'.
   */
  public final static byte[] OPERATOR_BT = { 0x31 }; // BT: '1' == 0x31

  /**
   * Constant for operator 'less than'.
   */
  public final static byte[] OPERATOR_LT = { 0x32 }; // LT: '2' == 0x32

  /**
   * Constant for operator 'equal'.
   */
  public final static byte[] OPERATOR_EQ = { 0x33 }; // EQ: '3' == 0x33

  /**
   * Constant for operator 'greater than'.
   */
  public final static byte[] OPERATOR_GT = { 0x34 }; // GT: '4' == 0x34

  /**
   * Constant for operator 'not equal'.
   */
  public final static byte[] OPERATOR_NE = { 0x35 }; // NE: '5' == 0x35

  /**
   * Constant for operator 'greater equal'.
   */
  public final static byte[] OPERATOR_GE = { 0x36 }; // GE: '6' == 0x36

  /**
   * Constant for operator 'less equal'.
   */
  public final static byte[] OPERATOR_LE = { 0x37 }; // LE: '7' == 0x37


  /**
   * Constant for the boolean operator 'not'.
   */
  public final static byte[] OPERATOR_NOT  = { 0x30 }; // NOT:  '0' == 0x30

  /**
   * Constant for the boolean operator 'and'.
   */
  public final static byte[] OPERATOR_AND  = { 0x31 }; // AND:  '1' == 0x31

  /**
   * Constant for the boolean operator 'or'.
   */
  public final static byte[] OPERATOR_OR   = { 0x32 }; // OR:   '2' == 0x32

  /**
   * Constant for the boolean operator 'nand'.
   */
  public final static byte[] OPERATOR_NAND = { 0x33 }; // NAND: '3' == 0x33

  /**
   * Constant for the boolean operator 'nor'.
   */
  public final static byte[] OPERATOR_NOR  = { 0x34 }; // NOR:  '4' == 0x34


  /**
   * Constant for the row type 'term'.
   */
  public final static byte[] ROW_TYPE_TERM      = { 0x31 }; // '1' == 0x31

  /**
   * Constant for the row type 'attribute'.
   */
  public final static byte[] ROW_TYPE_ATTRIBUTE = { 0x32 }; // '2' == 0x32

  /**
   * Constant for the row type 'operator'.
   */
  public final static byte[] ROW_TYPE_OPERATOR  = { 0x33 }; // '3' == 0x33


  /**
   * Constant for the termaction 'fuzzy'.
   */
  public final static byte[] TERM_ACTION_FUZZY = { 0x46 }; // 'L' == 70 == 0x46

  /**
   * Constant for the termaction 'linguistic'.
   */
  public final static byte[] TERM_ACTION_LINGUISTIC = { 0x4C }; // 'F' == 76 == 0x4C


  /**
   * Constant for the 'contenttype'.
   */
  public final static byte[] TERM_CONTENTTYPE = { 0x31 }; // '1' == 0x31

  /**
   * Constant for the 'translation'.
   */
  public final static byte[] TERM_TRANSLATION = { 0x32 }; // '2' == 0x32

  /**
   * Constant for the 'term action'.
   */
  public final static byte[] TERM_ACTION      = { 0x33 }; // '3' == 0x33

  /**
   * Constant for the 'term weight'.
   */
  public final static byte[] TERM_WEIGHT      = { 0x34 }; // '4' == 0x34
}