/*****************************************************************************

    Class:        IMSCatalogBuilder
    Copyright (c) 2001, SAP AG, Germany, All rights reserved.
    Created:      12.02.2001

*****************************************************************************/

package com.sap.isa.catalog.ims;

// misc imports
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.StringTokenizer;

import com.sap.isa.catalog.CatalogParameters;
import com.sap.isa.catalog.ICatalogMessagesKeys;
import com.sap.isa.catalog.boi.CatalogException;
import com.sap.isa.catalog.boi.IDetail;
import com.sap.isa.catalog.boi.IQueryStatement;
import com.sap.isa.catalog.impl.Catalog;
import com.sap.isa.catalog.impl.CatalogAttribute;
import com.sap.isa.catalog.impl.CatalogAttributeValue;
import com.sap.isa.catalog.impl.CatalogBuildFailedException;
import com.sap.isa.catalog.impl.CatalogBuilder;
import com.sap.isa.catalog.impl.CatalogCategory;
import com.sap.isa.catalog.impl.CatalogCompositeComponent;
import com.sap.isa.catalog.impl.CatalogDetail;
import com.sap.isa.catalog.impl.CatalogItem;
import com.sap.isa.catalog.impl.CatalogQuery;
import com.sap.isa.catalog.impl.CatalogQueryItem;
import com.sap.isa.catalog.impl.CatalogQueryStatement;
import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.core.util.table.ResultData;

/**
 * Builds the catalog tree for the IMS catalog implementation.
 */
class IMSCatalogBuilder extends CatalogBuilder implements IMSConstants, IMSConstantsQuery {

    // the logging instance
    private static IsaLocation log = IsaLocation.getInstance(IMSCatalogBuilder.class.getName());

    private static IsaLocation runtimeLogger = IsaLocation.getInstance("runtime." + IMSCatalogBuilder.class.getName());

    private final static HashSet theExcludedAttributes;
    static {
        theExcludedAttributes = new HashSet();
        theExcludedAttributes.add(ATTR_AREA);
        theExcludedAttributes.add(ATTR_AREA_GUID);
        theExcludedAttributes.add(ATTR_AREA_SPEZ_EXIST);
        theExcludedAttributes.add(ATTR_AREA_TYPE);
        theExcludedAttributes.add(ATTR_CATALOG);
        theExcludedAttributes.add(ATTR_CATALOG_GUID);
        theExcludedAttributes.add(ATTR_OBJECT_GUID);
        theExcludedAttributes.add(ATTR_PARENT_AREA);
        theExcludedAttributes.add(ATTR_PARENT_AREA_GUID);
        theExcludedAttributes.add(ATTR_SUBAREAS_EXIST);
    }

    private CRMCatalog theCRMCatalog;

    // proxy pool for the IMS
    private transient IMSProxyPool theProxyPool = null;

    /**
     * Default Constructor for the IMS catalog builder.
     *
     * @param aCatalog  the IMSCatalog which has to be built
     */
    IMSCatalogBuilder(CRMCatalog aCatalog) throws CRMException {

        // store catalog
        theCRMCatalog = aCatalog;
    }

    /**
     * Adds the content of the result set to the given catalog. Through this
     * method all categories are added to the catalog.
     *
     * @param results  selected results
     * @param catalog  catalog for which the results were selected
     * @exception CRMException  error from the IMS proxy
     */
    private void addAllCategories(IMSResultSet results, Catalog catalog) throws CRMException {

        IMSResultSetRow row;
        CatalogCategory category;
        CatalogCategory parent;
        ArrayList rootCategories = new ArrayList();
        Iterator rowIterator = results.iterator();

        // first instantiate all categories
        while (rowIterator.hasNext()) {

            row = (IMSResultSetRow) rowIterator.next();

            // build category if not already existing
            String guid = row.getValue(ATTR_AREA_GUID);
            category = (CatalogCategory) catalog.getCategoryInternal(guid);
            if (category == null) {
                category = catalog.createCategoryInternal(guid);
                category.setNameInternal(row.getValue(ATTR_DESCRIPTION));
                category.setDescriptionInternal(row.getValue(ATTR_TEXT_0001));
                category.setThumbNailInternal(row.getValue(ATTR_DOC_PC_CRM_THUMB));
                catalog.addCategory(category);
            }
        }

        // set parents for all instantiated categories
        rowIterator = results.iterator();
        while (rowIterator.hasNext()) {

            row = (IMSResultSetRow) rowIterator.next();

            // get guids
            String parentGuid = row.getValue(ATTR_PARENT_AREA_GUID);
            String categoryGuid = row.getValue(ATTR_AREA_GUID);
            category = (CatalogCategory) catalog.getCategoryInternal(categoryGuid);

            //JP: as ALL categories are put under the appropriate parent category/root, the following property is built, too:
            this.setPropertyBuilt(category, PROPERTY_CATEGORY);

            // set parent category
            if (!parentGuid.equals(VALUE_ROOT)) {
                parent = (CatalogCategory) catalog.getCategoryInternal(parentGuid);
                if (parent == null) {
                    //JP: catalog index inconsistency checker: parent area does not exist for this area
                    log.warn(
                        ICatalogMessagesKeys.PCAT_WARNING,
                        new Object[] { "parent area for area '" + category.getName() + "' not found --- ignored" },
                        null);
                    // we ignore this category here, because it is NOT accessible via "normal" browsing activity
                    continue;
                }
                if (runtimeLogger.isDebugEnabled()) {
                    runtimeLogger.debug(
                        "      addCategories("
                            + category.getName()
                            + "): setting PROPERTY_CATEGORY - parent is "
                            + parent.getName());
                }
                category.setParent(parent);
                parent.addChildCategoryInternal(category);
            }
            // add also the area specific attributes if it is a root category
            // and the root categories weren't built yet (performance!)
            else if (!isCategoriesBuilt(catalog)) {
                if (runtimeLogger.isDebugEnabled()) {
                    runtimeLogger.debug(
                        "      addCategories(" + category.getName() + "): setting PROPERTY_CATEGORY - is root category");
                }
                rootCategories.add(category);
                if (row.getValue(ATTR_AREA_SPEZ_EXIST).equals(VALUE_X)) {
                    addCategoryAttributes(category, row.getValue(ATTR_AREA));
                }
            }
        }

        // the root categories were also built
        if (!isCategoriesBuilt(catalog)) {
            catalog.setChildCategories(rootCategories);
            this.setPropertyBuilt(catalog, CatalogBuilder.PROPERTY_CATEGORY);
        }
    }

    /**
     * Adds the attributes names to the given composite componet.
     *
     * @param info     info object about the index
     * @param compoent component for which the names were selected
     */
    private void addAttributes(IMSIndexInfo info, CatalogCompositeComponent component) {

        // set name of all attributes
        Iterator iter = info.getAttributes().iterator();
        while (iter.hasNext()) {

            String name = (String) iter.next();

            if (log.isDebugEnabled()) {
                log.debug("Try to add attribute : " + name);
            }

            if (theExcludedAttributes.contains(name))
                continue;

            // create new attribute
            CatalogAttribute attribute = component.createAttributeInternal();

            // set name of attribute
            attribute.setNameInternal(name);

            // set properties of attribute
            Properties props = info.getAttributeProperties(name);
            if (props != null) {

                // set guid of attribute (can't be used since index does not support it)
                // attribute.setGuid(props.getProperty(IMSIndexInfo.PROP_GUID));
                // set description of attribute
                attribute.setDescriptionInternal(props.getProperty(IMSIndexInfo.PROP_DESCRIPTION));
                attribute.setTypeInternal(props.getProperty(IMSIndexInfo.PROP_TYPE));

                // set detail information for attribute
                CatalogDetail detail = attribute.createDetailInternal();
                detail.setNameInternal(IMSIndexInfo.PROP_ENTRYSTYLE);
                detail.setAsStringInternal(props.getProperty(IMSIndexInfo.PROP_ENTRYSTYLE));
                detail = attribute.createDetailInternal();
                detail.setNameInternal(IMSIndexInfo.PROP_POS_NR);
                detail.setAsStringInternal(props.getProperty(IMSIndexInfo.PROP_POS_NR));
            }
            
            // set fixed values for attribute
            List values = info.getAttributeFixedValues(name);
            if (values != null) {
                attribute.setFixedValuesInternal(new ArrayList(values));
            }

            // the property (details of the attribute) is already built now!
            this.setPropertyBuilt(attribute, CatalogBuilder.PROPERTY_DETAIL);
        }
        return;
    }

    /**
     * Adds the content of the result set to the given catalog. Through this
     * method the root categories are added to the catalog.
     *
     * @param results  selected results
     * @param catalog  catalog for which the results were selected
     * @exception CRMException  error from the IMS proxy
     */
    private void addCategories(IMSResultSet results, Catalog catalog) throws CRMException {

        Iterator rowIterator = results.iterator();
        IMSResultSetRow row;
        CatalogCategory category;
        ArrayList rootCategories = new ArrayList();

        // first instantiate all categories
        while (rowIterator.hasNext()) {
            row = (IMSResultSetRow) rowIterator.next();

            // build category
            category = catalog.createCategoryInternal(row.getValue(ATTR_AREA_GUID));
            category.setNameInternal(row.getValue(ATTR_DESCRIPTION));
            category.setDescriptionInternal(row.getValue(ATTR_TEXT_0001));
            category.setThumbNailInternal(row.getValue(ATTR_DOC_PC_CRM_THUMB));
            catalog.addCategory(category);

            // add to root categories
            rootCategories.add(category);

            // add also the area specific attributes (performance!)
            if (row.getValue(ATTR_AREA_SPEZ_EXIST).equals(VALUE_X)) {
                addCategoryAttributes(category, row.getValue(ATTR_AREA));
            }
        }

        // add the root categories to the catalog
        catalog.setChildCategories(rootCategories);
        return;
    }

    /**
     * Adds the content of the result set to the given category. Through this
     * mehtod the children of the given category are set.
     *
     * @param results  selected results
     * @param catalog  category for which the children were selected
     * @exception CRMException  error from the IMS proxy
     */
    private void addCategories(IMSResultSet results, CatalogCategory category) throws CRMException {

        IMSResultSetRow row;
        CatalogCategory child;
        Iterator rowIterator = results.iterator();
        ArrayList children = new ArrayList();

        while (rowIterator.hasNext()) {

            row = (IMSResultSetRow) rowIterator.next();

            // get child
            String guid = row.getValue(ATTR_AREA_GUID);
            child = (CatalogCategory) theCRMCatalog.getCategoryInternal(guid);

            // build category if not already done
            if (child == null) {

                // only one thread is allowed to modify the catalog instance
                synchronized (theCRMCatalog) {
                    // build category
                    child = theCRMCatalog.createCategoryInternal(guid);
                    child.setNameInternal(row.getValue(ATTR_DESCRIPTION));
                    child.setDescriptionInternal(row.getValue(ATTR_TEXT_0001));
                    child.setThumbNailInternal(row.getValue(ATTR_DOC_PC_CRM_THUMB));
                    child.setParent(category);
                    theCRMCatalog.addCategory(child);
                }
            }

            // fill children collection
            children.add(child);
        }
        
        // set children categories
        category.setChildCategories(children);
    }

    /*
     * Retrieve the attributes of the category which must have attached an area-
     * specific LOC.
     * The caller of this method is already in a synchronized block.
     */
    private void addCategoryAttributes(CatalogCategory category, String areaName) {

        //JP: maybe the area containing the LOC is deactivated/renamed/deleted in CRM since last replication
        try {
            IMSIndexInfo index = theCRMCatalog.getIndexInfo(INDEX_LEVEL_S, areaName, category.getGuid());

            this.addAttributes(index, category);
        }
        catch (CRMException ex) {
            //JP: ignore those LOC attributes
            String msg = "S-index for area '" + areaName + "' not available -- ignored";
            log.warn(ICatalogMessagesKeys.PCAT_EXCEPTION, new Object[] { msg }, null);
        } //JP:end of try

        // the property is built now!
        this.setPropertyBuilt(category, CatalogBuilder.PROPERTY_ATTRIBUTE);
    }

    /**
     * Retrieve the attributes of a category which must have attached an area-
     * specific LOC and assign them to a query.
     * The caller of this method is already in a synchronized block.
     */
    private void addCategoryAttributesToQuery(CatalogQuery query, String areaName, String categoryGuid) {
        
        if (log.isDebugEnabled()) {
            log.debug("Add area specific attributes to query for area: " + areaName);
        }

        try {
            IMSIndexInfo index = theCRMCatalog.getIndexInfo(INDEX_LEVEL_S, areaName, categoryGuid);

            this.addAttributes(index, query);
        }
        catch (CRMException ex) {
            //JP: ignore those LOC attributes
            String msg = "AddCategoryAttributesToQuery S-index for area '" + areaName + "' not available -- ignored";
            log.warn(ICatalogMessagesKeys.PCAT_EXCEPTION, new Object[] { msg }, null);
        } //JP:end of try
    }

    /**
     * Adds the content of the result set to the given category. Through this
     * method the detail information of the given category are set.
     *
     * @param results   selected results
     * @param category  category for which the results were selected
     * @exception CRMException  error from the IMS proxy
     */
    private void addDetails(IMSResultSet results, CatalogCategory category) throws CRMException {

        Iterator colIterator;
        IMSResultSetRow resRow;
        IMSResultSetItem resItem;
        CatalogDetail detail;

        Iterator rowIterator = results.iterator();
        while (rowIterator.hasNext()) {

            resRow = (IMSResultSetRow) rowIterator.next();

            // iterate over columns
            colIterator = resRow.iterator();
            while (colIterator.hasNext()) {
                resItem = (IMSResultSetItem) colIterator.next();
                if (resItem != null) {
                    // build the detail
                    detail = category.createDetailInternal();
                    detail.setNameInternal(resItem.getColumnName());
                    if (resItem.isMultiValue())
                        detail.setAsStringInternal(resItem.getValues());
                    else
                        detail.setAsStringInternal(resItem.getValue());
                }
            }
        }
        return;
    }

    /**
     * Adds the content of the result set to the given catalog. Through this
     * method the detail information of the given catalog are set.
     *
     * @param results  selected results
     * @param catalog  catalog for which the results were selected
     * @exception CRMException  error from the IMS proxy
     */
    private void addDetails(ResultData results, Catalog catalog) throws CRMException {

        // put the LANGUAGE of the catalog (variant) into the details
        CatalogDetail detail;
        detail = catalog.createDetailInternal();
        detail.setNameInternal("LANGUAGE"); // this is a common attribute
        detail.setAsStringInternal(theCRMCatalog.getLanguage());

        results.beforeFirst();
        while (!results.isLast()) {
            results.next();

            copyAttribute(results, catalog, IDetail.ATTRNAME_PRICE);
            copyAttribute(results, catalog, IDetail.ATTRNAME_QUANTITY);
            copyAttribute(results, catalog, IDetail.ATTRNAME_CURRENCY);
            copyAttribute(results, catalog, IDetail.ATTRNAME_UOM);
            copyAttribute(results, catalog, IDetail.ATTRNAME_SCALETYPE);
            copyAttribute(results, catalog, IDetail.ATTRIBUTE_PRICETYPE);
            copyAttribute(results, catalog, IDetail.ATTRIBUTE_PROMOTION_ATTR);
        }

        return;
    }

    /**
     * Copies an attribute from a result row to a catalog detail. If the attribute
     * is already existing, it is appended to the value array of the catalog detail.
     */
    private void copyAttribute(ResultData results, Catalog catalog, String attributeName) {

        // get the value of the attribute
        String value = results.getString(attributeName);
        if (value == null) {
            return;
        }

        // try to get the detail
        CatalogDetail detail = catalog.getDetailInternal(attributeName);

        // getDetailInternal
        if (detail == null || detail.getName().equals(attributeName) == false) {

            // create the detail and set the value
            detail = catalog.createDetailInternal();
            detail.setNameInternal(attributeName);
            detail.setAsStringInternal(value);
        }
        else {
            // add the value to the detail
            detail.addAsStringInternal(value);
        }

    }

    // JP: this is called by buildAllCategoriesInternal(Catalog) to speed up the initialization phase
    private void addDetailsToCategory(IMSResultSetRow row, ArrayList attributeNames, CatalogCategory category)
        throws CRMException {

        // iterate over columns
        Iterator colIterator = row.iterator();
        while (colIterator.hasNext()) {

            IMSResultSetItem resItem = (IMSResultSetItem) colIterator.next();
            if (resItem == null)
                continue;

            String name = resItem.getColumnName();
            if (attributeNames.contains(name)) {

                // build the detail
                CatalogDetail detail = category.createDetailInternal();
                detail.setNameInternal(name);
                if (resItem.isMultiValue())
                    detail.setAsStringInternal(resItem.getValues());
                else
                    detail.setAsStringInternal(resItem.getValue());

                if (runtimeLogger.isDebugEnabled()) {
                    runtimeLogger.debug(
                        "      addDetailsToCategory("
                            + category.getName()
                            + "): "
                            + detail.getName()
                            + "="
                            + detail.getAsString());
                }
            }
        }
        this.setPropertyBuilt(category, CatalogBuilder.PROPERTY_DETAIL);
    }

    /**
     * Adds the content of the result sets to the given category. Through this
     * method the items for the given category are set.
     *
     * @param resProducts  selected results from the 'P' index
     * @param resCategory  selected results from the 'S' index
     * @param category     category for which the results were selected
     * @exception CRMException  error from the IMS proxy
     */
    private void addItems(IMSResultSet resProduct, IMSResultSet resCategory, CatalogCategory category) throws CRMException {

        Iterator colIterator;
        IMSResultSetRow resRow;
        IMSResultSetItem resItem;
        CatalogItem item;
        CatalogAttributeValue attrValue;
        HashMap builtItems = new HashMap();

        // first analyse the results of the P index
        Iterator rowIterator = resProduct.iterator();
        while (rowIterator.hasNext()) {

            String objectGuid = null;
            String areaGuid = null;

            // get next row
            resRow = (IMSResultSetRow) rowIterator.next();

            // build item
            item = category.createItemInternal();

            // iterate over columns
            colIterator = resRow.iterator();
            while (colIterator.hasNext()) {

                resItem = (IMSResultSetItem) colIterator.next();
                if (resItem != null) {

                    String colName = resItem.getColumnName();

                    // store guid of object
                    if (colName.equals(ATTR_OBJECT_GUID)) {
                        objectGuid = resItem.getValue();
                    }

                    // store guid of item
                    if (colName.equals(ATTR_AREA_GUID))
                        areaGuid = resItem.getValue();

                    // set name of item
                    if (colName.equals(ATTR_OBJECT_DESC))
                        item.setNameInternal(resItem.getValue());

                    // set all other attributes
                    if (!colName.equals(ATTR_AREA_GUID)
                        && !colName.equals(ATTR_OBJECT_GUID)
                        && !colName.equals(ATTR_AREA)) {

                        attrValue = item.createAttributeValueInternal(colName);
                        if (resItem.isMultiValue())
                            attrValue.setAsStringInternal(resItem.getValues());
                        else
                            attrValue.setAsStringInternal(resItem.getValue());
                    }
                }
            }

            // set guid of item
            item.setGuidInternal(areaGuid + objectGuid);

            // set product guid of item
            item.setProductGuidInternal(objectGuid);

            // store item in the hash map
            builtItems.put(objectGuid, item);
        }

        processSIndexResults(resCategory, builtItems);

        return;
    }

    /**
     * Adds the content of the result sets to the given query.
     *
     * @param results      selected results from the 'A' index
     * @param query        query for which the results were selected
     * @exception CRMException  error from the IMS proxy
     */
    private void addQuery(IMSResultSet results, CatalogQuery query) throws CRMException {

        for (Iterator rowIterator = results.iterator(); rowIterator.hasNext();) {

            // get next row
            IMSResultSetRow resRow = (IMSResultSetRow) rowIterator.next();

            // add category to query
            String areaGuid = resRow.getValue(ATTR_AREA_GUID);
            CatalogCategory category = theCRMCatalog.getCategoryInternal(areaGuid);
            query.addChildCategoryInternal(category);

            // check if details were alread build
            if (this.isDetailsBuilt(category))
                continue;

            // otherwise build the details
            synchronized (category) {

                if (!isDetailsBuilt(category)) {

                    // iterate over columns
                    for (Iterator colIterator = resRow.iterator(); colIterator.hasNext();) {

                        IMSResultSetItem resItem = (IMSResultSetItem) colIterator.next();
                        if (resItem != null && !resItem.getColumnName().equals(ATTR_AREA_GUID)) {

                            // build the detail
                            CatalogDetail detail = category.createDetailInternal();
                            detail.setNameInternal(resItem.getColumnName());
                            if (resItem.isMultiValue())
                                detail.setAsStringInternal(resItem.getValues());
                            else
                                detail.setAsStringInternal(resItem.getValue());
                        }
                    }
                    this.setPropertyBuilt(category, PROPERTY_DETAIL);
                }
            }
        }
        return;
    }

    /**
     * Adds the content of the result sets to the given query.
     *
     * @param resProducts  selected results from the 'P' index
     * @param resCategory  selected results from the 'S' index
     * @param query        query for which the results were selected
     * @exception CRMException  error from the IMS proxy
     */
    private void addQuery(IMSResultSet resProduct, IMSResultSet resCategory, CatalogQuery query) throws CRMException {

        Iterator colIterator;
        IMSResultSetRow resRow;
        IMSResultSetItem resItem;
        CatalogQueryItem item;
        HashMap builtItems = new HashMap();
        CatalogAttributeValue attrValue;
        String queryAsString;
        boolean isAdministeredItem = false;
        boolean allowAdministeredProd = true;
        String administeredItemText = "";
        int lengthText = 0;
        StringTokenizer st = null;
        String queryToken = "";
        Properties serverEngineProps = new Properties();

        queryAsString = query.getStatementAsString();
        serverEngineProps = theCRMCatalog.getServerEngine().getProperties();

        String allowAdministeredProds = serverEngineProps.getProperty(IMSConstants.ALLOW_ADMINISTERED_PROD);

        if (allowAdministeredProds != null && allowAdministeredProds.equalsIgnoreCase("false")) {
            allowAdministeredProd = false;
        }

        Iterator rowIterator = resProduct.iterator();
        while (rowIterator.hasNext()) {

            if (queryAsString != null && !queryAsString.equalsIgnoreCase("*")) {

                if (queryAsString.indexOf("*") == 0) {
                    queryAsString = queryAsString.substring(1);
                }

                if (!queryAsString.equalsIgnoreCase("*") && queryAsString.indexOf("*") == queryAsString.length() - 1)
                    queryAsString = queryAsString.substring(0, queryAsString.length() - 1);
                st = new StringTokenizer(queryAsString);
            }

            // get next row
            resRow = (IMSResultSetRow) rowIterator.next();
            if (resRow == null) {
                continue;
            }

            // build item
            String areaGuid = resRow.getValue(ATTR_AREA_GUID);
            String objectGuid = resRow.getValue(ATTR_OBJECT_GUID);
            item = query.createQueryItemInternal(areaGuid + objectGuid, areaGuid);

            item.setProductGuidInternal(objectGuid);
            builtItems.put(areaGuid + objectGuid, item);

            // iterate over columns
            colIterator = resRow.iterator();
            isAdministeredItem = false;
            while (colIterator.hasNext()) {

                resItem = (IMSResultSetItem) colIterator.next();
                if (resItem != null) {

                    String colName = resItem.getColumnName();

                    // set name of item
                    if (colName.equals(ATTR_OBJECT_DESC))
                        item.setNameInternal(resItem.getValue());

                    // set all other attributes
                    if (!colName.equals(ATTR_AREA_GUID)
                        && !colName.equals(ATTR_OBJECT_GUID)) { // TODO: durch else if ersetzen, wegen Beschreibung
                        attrValue = item.createAttributeValueInternal();
                        attrValue.setAttribute(colName);
                        if (resItem.isMultiValue())
                            attrValue.setAsStringInternal(resItem.getValues());
                        else
                            attrValue.setAsStringInternal(resItem.getValue());
                        if (colName.equals(ATTR_TEXT_0003)) {
                            if (allowAdministeredProd)
                                attrValue.setAsStringInternal("TRUE");
                        }

                        item.addAttributeValueInternal(attrValue);
                    }

                    if (allowAdministeredProd == true && colName.equals(ATTR_TEXT_0003) && st != null) {
                        administeredItemText = resItem.getValue();
                        if (administeredItemText != null && administeredItemText.length() > 0) {
                            lengthText = administeredItemText.length();
                            while (st.hasMoreTokens()) {
                                queryToken = st.nextToken();
                                for (int i = 0; i < lengthText; i++) {
                                    isAdministeredItem =
                                        queryToken.regionMatches(true, 0, administeredItemText, i, queryToken.length());
                                    if (isAdministeredItem)
                                        break;
                                }
                                if (isAdministeredItem)
                                    break;
                            }
                        }
                    }
                }
            }
            if (isAdministeredItem) {
                query.swapItemsInternal(item);
            }
        }

        processSIndexResults(resCategory, builtItems);

        return;
    }

    /**
     * Updates the S-Index attributes to an existing query.
     *
     * @param resProducts  selected results from the 'P' index
     * @param resCategory  selected results from the 'S' index
     * @param query        query for which the results were selected
     * @exception CRMException  error from the IMS proxy
     */
    private void appendSAttributes(IMSResultSet resProduct, IMSResultSet resCategory, CatalogQuery query)
        throws CRMException {

        // rebuild hash map of available items
        HashMap builtItems = new HashMap();
        for (Iterator it = query.getItemsInternal(); it.hasNext();) {
            CatalogQueryItem item = (CatalogQueryItem) it.next();
            builtItems.put(item.getGuid(), item);
        }

        // append S-Index
        processSIndexResults(resCategory, builtItems);

        return;
    }

    protected void buildAllCategoriesInternal(Catalog aCatalog) throws CatalogBuildFailedException {

        if (runtimeLogger.isDebugEnabled()) {
            runtimeLogger.debug(
                "begin buildAllCategoriesInternal("
                    + aCatalog.getName()
                    + "): get ALL attributes from ALL documents in AIndex");
        }
        IMSProxy proxy = null;

        try {
            IMSResultSet results;
            String searchEngine = theCRMCatalog.getSearchEngine();
            IMSIndexInfo index = theCRMCatalog.getIndexInfo(INDEX_LEVEL_A, "", "");

            // get proxy
            proxy = getProxyPool().getProxy();

            // set query environment (use default settings)
            proxy.setQueryEnv(index);

            // set return properties
            proxy.setReturnInterval(INTERVAL_BEGIN, CatalogParameters.imsUpperLimit);

            // select ALL attributes...
            Iterator iter = index.getAttributes().iterator();
            while (iter.hasNext()) {
                proxy.addReturnAttribute((String) iter.next());
            }

            // set sort order - only possible with DrFuzzy (performance!)
            if (searchEngine.equals(ENGINE_DRFUZZY))
                proxy.setSortOrderASC(ATTR_POS_NR);
            else
                proxy.addReturnAttribute(ATTR_POS_NR);

            // set query condition
            String[] views = theCRMCatalog.getViews();
            if (views != null && views.length > 0)
                proxy.addQueryAttributeList(ATTR_VIEWS_ID, views);
            else
                proxy.addQueryAttribute(ATTR_AREA_GUID, OPERATOR_NE, VALUE_ROOT);

            // execute query
            results = proxy.executeQuery(theCRMCatalog.getIMSConnectionInfo());

            // in case of Verity sort now
            if (searchEngine.equals(ENGINE_VERITY))
                results.sortRows(ATTR_POS_NR);

            // analyse results
            this.addAllCategories(results, theCRMCatalog);

            //JP: determine the names of the attributes used for category's Details
            ArrayList attributeNames = new ArrayList();
            iter = index.getAttributes().iterator();
            while (iter.hasNext()) {
                String name = (String) iter.next();
                if ((!theExcludedAttributes.contains(name)
                    && !name.equals(ATTR_DESCRIPTION)
                    && !name.equals(ATTR_DOC_PC_CRM_THUMB))
                    || name.equals(ATTR_AREA))
                    attributeNames.add(name);
            }
            //JP: insert the details to the categories
            Iterator rowIterator = results.iterator();
            while (rowIterator.hasNext()) {
                IMSResultSetRow row = (IMSResultSetRow) rowIterator.next();
                String guid = row.getValue(ATTR_AREA_GUID);
                CatalogCategory category = (CatalogCategory) aCatalog.getCategoryInternal(guid);
                addDetailsToCategory(row, attributeNames, category);
            }

        }
        catch (CRMException ex) {
            throw new CatalogBuildFailedException(ex.getLocalizedMessage());
        }
        finally {
            getProxyPool().releaseProxy(proxy);
        }

        if (runtimeLogger.isDebugEnabled()) {
            runtimeLogger.debug("end   buildAllCategoriesInternal");
        }
    }

    protected void buildAttributesInternal(Catalog catalog) throws CatalogBuildFailedException {
        try {
            IMSIndexInfo index = theCRMCatalog.getIndexInfo(INDEX_LEVEL_P, "", theCRMCatalog.getName());
            this.addAttributes(index, theCRMCatalog);
        }
        catch (CRMException ex) {
            throw new CatalogBuildFailedException(ex.getLocalizedMessage());
        }
        return;
    }

    protected void buildAttributesInternal(CatalogCategory category) throws CatalogBuildFailedException {

        if (runtimeLogger.isDebugEnabled()) {
            runtimeLogger.debug("begin buildAttributesInternal(" + category.getName() + ")");
        }
        IMSProxy proxy = null;
        try {
            IMSResultSet results;
            IMSIndexInfo index = theCRMCatalog.getIndexInfo(INDEX_LEVEL_A, "", "");

            // first check if category has area specific attributes
            proxy = getProxyPool().getProxy();

            // set query environment (use default settings)
            proxy.setQueryEnv(index);
            proxy.setReturnInterval(INTERVAL_BEGIN, CatalogParameters.imsUpperLimit);

            // set return properties
            proxy.addReturnAttribute(ATTR_AREA_SPEZ_EXIST);
            proxy.addReturnAttribute(ATTR_AREA);

            // set query condition
            proxy.addQueryAttribute(ATTR_AREA_GUID, OPERATOR_EQ, category.getGuid());
            String[] views = theCRMCatalog.getViews();
            if (views != null && views.length > 0) {
                proxy.addQueryAttributeList(ATTR_VIEWS_ID, views);
                proxy.addQueryOperator(OPERATOR_AND);
            }

            // execute query
            results = proxy.executeQuery(theCRMCatalog.getIMSConnectionInfo());

            IMSResultSetRow resRow = results.getRow(0);
            if (resRow != null && resRow.getValue(ATTR_AREA_SPEZ_EXIST).equals(VALUE_X)) {
                addCategoryAttributes(category, resRow.getValue(ATTR_AREA));
            }
        }
        catch (CRMException ex) {
            throw new CatalogBuildFailedException(ex.getLocalizedMessage());
        }
        finally {
            getProxyPool().releaseProxy(proxy);
        }

        if (runtimeLogger.isDebugEnabled()) {
            runtimeLogger.debug("begin buildAttributesInternal()");
        }
    }

    protected void buildCategoriesInternal(Catalog aCatalog) throws CatalogBuildFailedException {

        if (runtimeLogger.isDebugEnabled()) {
            runtimeLogger.debug("begin buildCategoriesInternal(" + aCatalog.getName() + "): get root categories...");
        }
        IMSProxy proxy = null;

        try {
            IMSResultSet results;
            String searchEngine = theCRMCatalog.getSearchEngine();
            IMSIndexInfo index = theCRMCatalog.getIndexInfo(INDEX_LEVEL_A, "", "");

            // get proxy
            proxy = getProxyPool().getProxy();

            // set query environment (use default settings)
            proxy.setQueryEnv(index);

            // set return properties
            proxy.setReturnInterval(INTERVAL_BEGIN, CatalogParameters.imsUpperLimit);
            proxy.addReturnAttribute(ATTR_AREA);
            proxy.addReturnAttribute(ATTR_AREA_GUID);
            proxy.addReturnAttribute(ATTR_AREA_SPEZ_EXIST);
            proxy.addReturnAttribute(ATTR_PARENT_AREA_GUID);
            proxy.addReturnAttribute(ATTR_DESCRIPTION);
            if (index.hasAttribute(ATTR_DOC_PC_CRM_THUMB)) // not always available
                proxy.addReturnAttribute(ATTR_DOC_PC_CRM_THUMB);
            if (index.hasAttribute(ATTR_TEXT_0001)) // not always available
                proxy.addReturnAttribute(ATTR_TEXT_0001);
            if (searchEngine.equals(ENGINE_DRFUZZY))
                proxy.setSortOrderASC(ATTR_POS_NR);
            else
                proxy.addReturnAttribute(ATTR_POS_NR);

            // set query condition
            proxy.addQueryAttribute(ATTR_PARENT_AREA_GUID, OPERATOR_EQ, VALUE_ROOT);

            // restrict to not include the $ROOT area
            proxy.addQueryAttribute(ATTR_AREA_GUID, OPERATOR_NE, VALUE_ROOT);
            proxy.addQueryOperator(OPERATOR_AND);
            String[] views = theCRMCatalog.getViews();
            if (views != null && views.length > 0) {
                proxy.addQueryAttributeList(ATTR_VIEWS_ID, views);
                proxy.addQueryOperator(OPERATOR_AND);
            }

            // execute query
            results = proxy.executeQuery(theCRMCatalog.getIMSConnectionInfo());

            // in case of Verity sort now
            if (searchEngine.equals(ENGINE_VERITY))
                results.sortRows(ATTR_POS_NR);

            // analyse results
            this.addCategories(results, theCRMCatalog);

        }
        catch (CRMException ex) {
            throw new CatalogBuildFailedException(ex.getLocalizedMessage());
        }
        finally {
            getProxyPool().releaseProxy(proxy);
        }

        if (runtimeLogger.isDebugEnabled()) {
            runtimeLogger.debug("end   buildCategoriesInternal");
        }
    }

    protected void buildCategoriesInternal(CatalogCategory category) throws CatalogBuildFailedException {

        if (runtimeLogger.isDebugEnabled()) {
            runtimeLogger.debug("begin buildCategoriesInternal(" + category.getName() + ")");
        }
        IMSProxy proxy = null;

        try {
            IMSResultSet results;
            String searchEngine = theCRMCatalog.getSearchEngine();
            IMSIndexInfo index = theCRMCatalog.getIndexInfo(INDEX_LEVEL_A, "", "");

            // get proxy
            proxy = getProxyPool().getProxy();

            // set query environment (use default settings)
            proxy.setQueryEnv(index);

            // set return properties
            proxy.setReturnInterval(INTERVAL_BEGIN, CatalogParameters.imsUpperLimit);
            proxy.addReturnAttribute(ATTR_AREA);
            proxy.addReturnAttribute(ATTR_AREA_GUID);
            proxy.addReturnAttribute(ATTR_AREA_SPEZ_EXIST);
            proxy.addReturnAttribute(ATTR_DESCRIPTION);
            if (index.hasAttribute(ATTR_DOC_PC_CRM_THUMB)) // not always available
                proxy.addReturnAttribute(ATTR_DOC_PC_CRM_THUMB);
            if (index.hasAttribute(ATTR_TEXT_0001)) // not always available
                proxy.addReturnAttribute(ATTR_TEXT_0001);

            // set sort order - only possible with DrFuzzy (performance!)
            if (searchEngine.equals(ENGINE_DRFUZZY))
                proxy.setSortOrderASC(ATTR_POS_NR);
            else
                proxy.addReturnAttribute(ATTR_POS_NR);

            // set query condition
            proxy.addQueryAttribute(ATTR_PARENT_AREA_GUID, OPERATOR_EQ, category.getGuid());
            String[] views = theCRMCatalog.getViews();
            if (views != null && views.length > 0) {
                proxy.addQueryAttributeList(ATTR_VIEWS_ID, views);
                proxy.addQueryOperator(OPERATOR_AND);
            }

            // execute query
            results = proxy.executeQuery(theCRMCatalog.getIMSConnectionInfo());

            // in case of Verity sort now
            if (searchEngine.equals(ENGINE_VERITY))
                results.sortRows(ATTR_POS_NR);

            // analyse results
            this.addCategories(results, category);
        }
        catch (CRMException ex) {
            throw new CatalogBuildFailedException(ex.getLocalizedMessage());
        }
        finally {
            getProxyPool().releaseProxy(proxy);
        }

        if (runtimeLogger.isDebugEnabled()) {
            runtimeLogger.debug("end   buildCategoriesInternal()");
        }
    }

    protected void buildDetailsInternal(Catalog aCatalog) throws CatalogBuildFailedException {

        if (runtimeLogger.isDebugEnabled()) {
            runtimeLogger.debug("begin buildDetailsInternal(" + aCatalog.getName() + ")");
        }
        try {
            CRMCatalogServerEngine theServerEngine = (CRMCatalogServerEngine) theCRMCatalog.getServerEngine();

            // currently this are only the pricing infos
            ResultData results =
                CRMRfcMethods.getPricingMetaInfo(
                    theServerEngine.getDefaultJCoConnection(),
                    theCRMCatalog.getName(),
                    theCRMCatalog.getVariant());

            // analyse results
            this.addDetails(results, theCRMCatalog);
        }
        catch (CRMException ex) {
            throw new CatalogBuildFailedException(ex.getLocalizedMessage());
        }

        if (runtimeLogger.isDebugEnabled()) {
            runtimeLogger.debug("end   buildDetailsInternal()");
        }
    }

    protected void buildDetailsInternal(CatalogAttribute aAttribute) throws CatalogBuildFailedException {
        // are builded in the same method as the attributes
        return;
    }

    protected void buildDetailsInternal(CatalogAttributeValue aValue) throws CatalogBuildFailedException {
        // no details available
        return;
    }

    protected void buildDetailsInternal(CatalogCategory category) throws CatalogBuildFailedException {

        if (runtimeLogger.isDebugEnabled()) {
            runtimeLogger.debug("begin buildDetailsInternal(" + category.getName() + ")");
        }
        IMSProxy proxy = null;
        try {
            IMSResultSet results;
            IMSIndexInfo index = theCRMCatalog.getIndexInfo(INDEX_LEVEL_A, "", "");
            String searchEngine = theCRMCatalog.getSearchEngine();

            // get proxy
            proxy = getProxyPool().getProxy();

            // set query environment (use default settings)
            proxy.setQueryEnv(index);

            // set return properties
            proxy.setReturnInterval(INTERVAL_BEGIN, CatalogParameters.imsUpperLimit);
            Iterator iter = index.getAttributes().iterator();
            while (iter.hasNext()) {
                String name = (String) iter.next();
                if ((!theExcludedAttributes.contains(name)
                    && !name.equals(ATTR_TEXT_0001)
                    && !name.equals(ATTR_DESCRIPTION)
                    && !name.equals(ATTR_DOC_PC_CRM_THUMB))
                    || name.equals(ATTR_AREA))
                    proxy.addReturnAttribute(name);
            }

            // set sort order - only possible for DrFuzzy (performance!)
            if (searchEngine.equals(ENGINE_DRFUZZY))
                proxy.setSortOrderASC(ATTR_POS_NR);

            // set query condition
            proxy.addQueryAttribute(ATTR_AREA_GUID, OPERATOR_EQ, category.getGuid());
            String[] views = theCRMCatalog.getViews();
            if (views != null && views.length > 0) {
                proxy.addQueryAttributeList(ATTR_VIEWS_ID, views);
                proxy.addQueryOperator(OPERATOR_AND);
            }

            // execute query
            results = proxy.executeQuery(theCRMCatalog.getIMSConnectionInfo());

            // in case of Verity sort now
            if (searchEngine.equals(ENGINE_VERITY))
                results.sortRows(ATTR_POS_NR);

            // analyse results
            this.addDetails(results, category);
        }
        catch (CRMException ex) {
            throw new CatalogBuildFailedException(ex.getLocalizedMessage());
        }
        finally {
            getProxyPool().releaseProxy(proxy);
        }

        if (runtimeLogger.isDebugEnabled()) {
            runtimeLogger.debug("begin buildDetailsInternal()");
        }
    }

    protected void buildDetailsInternal(CatalogDetail aDetail) throws CatalogBuildFailedException {
        // no details available
        return;
    }

    protected void buildDetailsInternal(CatalogItem aItem) throws CatalogBuildFailedException {
        // no details available
        return;
    }

    protected void buildItemsInternal(CatalogCategory category) throws CatalogBuildFailedException {

        if (runtimeLogger.isDebugEnabled()) {
            runtimeLogger.debug("begin buildItemsInternal(" + category.getName() + ")");
        }
        IMSProxy proxy = null;

        // first build all attributes if not already done
        if (!isAttributesBuilt(theCRMCatalog))
            this.buildAttributes(theCRMCatalog);

        try {
            IMSResultSet resProduct = null;
            IMSResultSet resCategory = null;
            Iterator iter;
            IMSIndexInfo index = theCRMCatalog.getIndexInfo(INDEX_LEVEL_P, "", theCRMCatalog.getName());
            String searchEngine = theCRMCatalog.getSearchEngine();

            // get proxy
            proxy = getProxyPool().getProxy();

            // set query environment (use default settings)
            proxy.setQueryEnv(index);

            // set return properties
            proxy.setReturnInterval(INTERVAL_BEGIN, CatalogParameters.imsUpperLimit);
            iter = index.getAttributes().iterator();
            while (iter.hasNext()) {
                String name = (String) iter.next();
                if (!theExcludedAttributes.contains(name))
                    proxy.addReturnAttribute(name);
            }
            proxy.addReturnAttribute(ATTR_AREA);
            proxy.addReturnAttribute(ATTR_AREA_GUID);
            proxy.addReturnAttribute(ATTR_OBJECT_GUID);

            // set sort order - only possible for DrFuzzy (performance!)
            if (searchEngine.equals(ENGINE_DRFUZZY))
                proxy.setSortOrderASC(ATTR_POS_NR);

            // set query condition
            proxy.addQueryAttribute(ATTR_AREA_GUID, OPERATOR_EQ, category.getGuid());
            String[] views = theCRMCatalog.getViews();
            if (views != null && views.length > 0) {
                proxy.addQueryAttributeList(ATTR_VIEWS_ID, views);
                proxy.addQueryOperator(OPERATOR_AND);
            }

            // execute query
            resProduct = proxy.executeQuery(theCRMCatalog.getIMSConnectionInfo());

            // in case of Verity sort now
            if (searchEngine.equals(ENGINE_VERITY))
                resProduct.sortRows(ATTR_POS_NR);

            //
            // read also the category specific attributes if necessary
            //
            iter = category.getAttributes();
            if (iter.hasNext() && resProduct.getRowCount() > 0) {

                String theAreaName = resProduct.getRow(0).getValue(ATTR_AREA);
                // this should not result in NOCATID
                IMSIndexInfo s_index = theCRMCatalog.getIndexInfo(INDEX_LEVEL_S, theAreaName, category.getGuid());

                // set environment info (use default settings)
                proxy.setQueryEnv(s_index);

                // set return properties
                proxy.setReturnInterval(INTERVAL_BEGIN, CatalogParameters.imsUpperLimit);
                proxy.addReturnAttribute(ATTR_OBJECT_GUID);

                // get all category specific attributes
                int i = 0;
                ArrayList names = new ArrayList();
                while (iter.hasNext()) {
                    String name = ((CatalogAttribute) iter.next()).getName();
                    names.add(name);
                }
                proxy.addReturnAttributes((String[]) names.toArray(new String[0]));
                // set query condition:
                //JP: as we want to get the S-attributes of every product in this S-index,
                //    there is no need to explicitly restrict the search to the OBJECT_GUIDs
                //    found in the P-search from above
                // and now restrict the search in LOC-Index to the given area
                proxy.addQueryAttribute(ATTR_AREA_GUID, OPERATOR_EQ, category.getGuid());

                // execute query
                resCategory = proxy.executeQuery(theCRMCatalog.getIMSConnectionInfo());
            }

            // analyse results
            this.addItems(resProduct, resCategory, category);
        }
        catch (CRMException ex) {
            throw new CatalogBuildFailedException(ex.getLocalizedMessage());
        }
        catch (CatalogException ex) {
            throw new CatalogBuildFailedException(ex.getLocalizedMessage());
        }
        finally {
            getProxyPool().releaseProxy(proxy);
        }

        if (runtimeLogger.isDebugEnabled()) {
            runtimeLogger.debug("end   buildItemsInternal()");
        }
    }

    /**
     * Internal helper method to build a query for categories.
     * A-Index is searched.
     */
    protected void buildQueryCategoriesInternal(CatalogQuery query) throws CatalogBuildFailedException {

        IMSProxy proxy = null;

        // first build all categories if not already done
        if (!isAllCategoriesBuilt(theCRMCatalog)) {
            this.buildAllCategories(theCRMCatalog);
        }

        try {
            IMSResultSet results;
            IMSIndexInfo indexA = theCRMCatalog.getIndexInfo(INDEX_LEVEL_A, "", "");

            // create new helper instances
            IMSCatalogQueryCompiler compiler = new IMSCatalogQueryCompiler(theCRMCatalog.getMessageResources());

            // get proxy
            proxy = getProxyPool().getProxy();

            // set query environment (use default settings)
            proxy.setQueryEnv(indexA);

            // set the default list of attributes
            List defAttr = new ArrayList();
            for (Iterator iter = indexA.getAttributes().iterator(); iter.hasNext();) {
                String name = (String) iter.next();
                if (!theExcludedAttributes.contains(name)
                    && !name.equals(ATTR_TEXT_0001)
                    && !name.equals(ATTR_DESCRIPTION)
                    && !name.equals(ATTR_DOC_PC_CRM_THUMB)) {

                    // add the attribute name
                    defAttr.add(name);
                }
            }

            // set default return attributes
            proxy.addReturnAttribute(ATTR_AREA_GUID);

            // transform query
            compiler.transformCategoryQuery(query, defAttr, indexA.getAttributes(), theCRMCatalog, proxy);

            // execute query
            results = proxy.executeQuery(theCRMCatalog.getIMSConnectionInfo());

            // analyse results
            this.addQuery(results, query);
        }
        catch (CRMException ex) {
            throw new CatalogBuildFailedException(ex.getLocalizedMessage());
        }
        finally {
            getProxyPool().releaseProxy(proxy);
        }

        return;
    }

    /**
     * Dispatcher for the different kinds of queries.
     */
    protected void buildQueryInternal(CatalogQuery query) throws CatalogBuildFailedException {
        CatalogQueryStatement statement = query.getCatalogQueryStatement();

        if (statement.getSearchType() == IQueryStatement.Type.CATEGORY_SEARCH)
            buildQueryCategoriesInternal(query);
        else
            buildQueryItemsInternal(query);
        return;
    }

    /**
     * Performs a search on the P-Index for the given query.
     */
    private IMSResultSet buildQueryItemsFromPIndex(CatalogQuery query) throws CatalogBuildFailedException {

        final String METHOD_NAME = "buildQueryItemsFromPIndex(CatalogQuery query)";
        log.entering(METHOD_NAME);
        log.debug("query statement:" + query.getStatementAsString());

        IMSResultSet resultsetP = null;

        // first build all global attributes if not already done
        if (!isAttributesBuilt(theCRMCatalog)) {
            buildAttributes(theCRMCatalog);
        }

        IMSProxy proxy = null;
        try {

            // get proxy - has to be released!
            proxy = getProxyPool().getProxy();

            // set environment info for 'P' index
            IMSIndexInfo indexP = theCRMCatalog.getIndexInfo(INDEX_LEVEL_P, "", theCRMCatalog.getName());
            proxy.setQueryEnv(indexP);

            // set default return attributes
            proxy.addReturnAttribute(ATTR_AREA_GUID);
            proxy.addReturnAttribute(ATTR_OBJECT_GUID);
            proxy.addReturnAttribute(ATTR_OBJECT_DESC);

            // create a list of default return attributes
            ArrayList defaultAttributesP = new ArrayList();
            Iterator it = indexP.getAttributes().iterator();
            while (it.hasNext()) {

                String attributeName = (String) it.next();

                // add to default attribute list
                if (!theExcludedAttributes.contains(attributeName) && !attributeName.equals(ATTR_OBJECT_DESC)) {
                    defaultAttributesP.add(attributeName);
                }
            }

            // compile the query
            IMSCatalogQueryCompiler compiler = new IMSCatalogQueryCompiler(theCRMCatalog.getMessageResources());

            compiler.transformItemQuery(query, defaultAttributesP, indexP.getAttributes(), theCRMCatalog, proxy);

            // execute query
            resultsetP = proxy.executeQuery(theCRMCatalog.getIMSConnectionInfo());

            // analyse results
            if (query.inCountMode()) {

                //the best I can do in the IMS case, I am not going to fidle with the proxy
                query.setCountInternal(resultsetP.getRowCount());
            }

            else {
                addQuery(resultsetP, null, query);
            } // end of else

        }
        catch (CRMException ex) {
            throw new CatalogBuildFailedException(ex.getMessage(), ex);
        }

        finally {
            getProxyPool().releaseProxy(proxy);
        }

        log.exiting();
        return resultsetP;
    }

    /**
     * Performs a search on category specific attributes (S-Index, then P-Index).
     * 
     * If the <code>query</code> is not a category specific search, nothing is done.
     */
    private void buildQueryItemsFromSIndex(CatalogQuery query) throws CatalogBuildFailedException {

        final String METHOD_NAME = "buildQueryItemsFromSIndex(CatalogQuery query)";
        log.entering(METHOD_NAME);
        log.debug("query statement:" + query.getStatementAsString());

        // Check preconditions
        if (!query.isCategorySpecificSearch()) {
            log.debug("buildQueryItemsFromSIndex was called, although no, the query is not category specific.");
            log.exiting();
            return;
        }

        // first build all global attributes if not already done
        if (!isAttributesBuilt(theCRMCatalog)) {
            buildAttributes(theCRMCatalog);
        }

        IMSProxy proxy = null;
        try {

            // get proxy - has to be released!
            proxy = getProxyPool().getProxy();

            // execute the query on the S-Index
            IMSResultSet resultsetS = querySpecificIndex(proxy, query);

            // check if something was found
            if (resultsetS.isEmpty()) {
                getProxyPool().releaseProxy(proxy);
                log.debug("Nothing found in S-Index for " + query.getStatementAsString());
                log.exiting();
                return;
            }

            // create string array with the object guids from the S-Index
            // to ask the P-Index with them.
            String[] objectGuidsS = new String[resultsetS.getRowCount()];
            int i = 0;
            for (Iterator iterS = resultsetS.iterator(); iterS.hasNext(); i++) {
                String objectGuid = ((IMSResultSetRow) iterS.next()).getValue(ATTR_OBJECT_GUID);
                objectGuidsS[i] = objectGuid;
            }

            // set environment info for 'P' index
            IMSIndexInfo indexP = theCRMCatalog.getIndexInfo(INDEX_LEVEL_P, "", theCRMCatalog.getName());
            proxy.setQueryEnv(indexP);

            // set default return attributes
            proxy.addReturnAttribute(ATTR_AREA_GUID);
            proxy.addReturnAttribute(ATTR_OBJECT_GUID);
            proxy.addReturnAttribute(ATTR_OBJECT_DESC);

            // set return properties
            proxy.setReturnInterval(INTERVAL_BEGIN, CatalogParameters.imsUpperLimit);
            proxy.addQueryAttributeList(ATTR_OBJECT_GUID, objectGuidsS);
            proxy.addQueryAttribute(ATTR_AREA_GUID, OPERATOR_EQ, query.getParent().getGuid());
            proxy.addQueryOperator(OPERATOR_AND);

            //don't forget the views
            String[] views = theCRMCatalog.getViews();
            if (views != null && views.length > 0) {
                proxy.addQueryAttributeList(IMSConstants.ATTR_VIEWS_ID, views);
                proxy.addQueryOperator(IMSConstantsQuery.OPERATOR_AND);
            }

            // create a list of default return attributes
            ArrayList defaultAttributesP = new ArrayList();
            Iterator it = indexP.getAttributes().iterator();
            while (it.hasNext()) {

                String attributeName = (String) it.next();

                // add to default attribute list
                if (!theExcludedAttributes.contains(attributeName) && !attributeName.equals(ATTR_OBJECT_DESC)) {
                    defaultAttributesP.add(attributeName);
                }
            }
            proxy.addReturnAttributes((String[]) defaultAttributesP.toArray(new String[0]));

            // execute query
            IMSResultSet resultsetP = proxy.executeQuery(theCRMCatalog.getIMSConnectionInfo());

            // analyse results
            if (query.inCountMode()) {

                //the best I can do in the IMS case, I am not going to fidle with the proxy
                query.setCountInternal(resultsetP.getRowCount());
            }

            else {
                addQuery(resultsetP, resultsetS, query);
            } // end of else

        }
        catch (CRMException ex) {
            throw new CatalogBuildFailedException(ex.getMessage(), ex);
        }

        finally {
            getProxyPool().releaseProxy(proxy);
        }

        log.exiting();
        return;
    }

    /**
     * Dispatches item queries to the appropriate methods.
     * 
     * @param  query to be performed
     * @throws CatalogBuildFailedException
     */
    protected void buildQueryItemsInternal(CatalogQuery query) throws CatalogBuildFailedException {

        // get the query statement
        CatalogQueryStatement statement = query.getCatalogQueryStatement();

        // Do not perform an area search here
        if (statement.getSearchType() == IQueryStatement.Type.CATEGORY_SEARCH) {
            return;
        }

        // search on S-Index
        if (query.isCategorySpecificSearch()) {
            buildQueryItemsFromSIndex(query);
        }
        else { // search on P-Index
            IMSResultSet resultsetP = buildQueryItemsFromPIndex(query);

            // Attributes from S-Index should be read, too
            if (query.getRequestCategorySpecificAttributes()) {
                readSIndexAttributesForPIndexResults(resultsetP, query);
            }
        }

        return;
    }

    /**
     * Returns the proxy pool.
     *
     * @return the proxy pool to be used
     */
    private IMSProxyPool getProxyPool() {

        if (theProxyPool == null)
            theProxyPool = new IMSProxyPool(theCRMCatalog);

        return theProxyPool;
    }

    /**
     * Stores the results of a S-Index search in the <code>builtItems</code>.
     * 
     * @param resCategory
     * @param builtItems
     */
    private void processSIndexResults(IMSResultSet resCategory, HashMap builtItems) {

        Iterator colIterator;
        IMSResultSetRow resRow;
        IMSResultSetItem resItem;
        CatalogItem item;
        CatalogAttributeValue attrValue;
        Iterator rowIterator;

        // if no area specific attributes were found then exit
        if (resCategory == null) {
            return;
        }

        // now analyse the results of the S index
        rowIterator = resCategory.iterator();
        while (rowIterator.hasNext()) {

            resRow = (IMSResultSetRow) rowIterator.next();

            // find item
            String areaGuid = resRow.getValue(ATTR_AREA_GUID);
            String objectGuid = resRow.getValue(ATTR_OBJECT_GUID);
            item = (CatalogItem) builtItems.get(areaGuid + objectGuid);
            //JP: since we search all products from P-index which are contained
            //    in the current area (that may be reduced by VIEW), and search 
            //    all attributes in the S-index (->resCategory) without VIEW,
            //    ==>> the resProduct list may contain fewer results than resCategory 
            if (item == null)
                continue;

            // iterate over columns
            colIterator = resRow.iterator();
            while (colIterator.hasNext()) {

                resItem = (IMSResultSetItem) colIterator.next();
                if (resItem == null) {
                    continue;
                }

                // set all attributes
                if (!resItem.getColumnName().equals(ATTR_OBJECT_GUID)) {
                    attrValue = item.createAttributeValueInternal(resItem.getColumnName());
                    if (resItem.isMultiValue())
                        attrValue.setAsStringInternal(resItem.getValues());
                    else
                        attrValue.setAsStringInternal(resItem.getValue());
                }

            }
        }
    }

    /**
      * Executes a query on the A-Index.
      *  
      * @param proxy            the proxy to be used for the query
      * @param categoryGuid     for which the area name is returned
      * 
      * @return IMSResultSet for the area.
      */
    private IMSResultSet queryAreaIndex(IMSProxy proxy, String categoryGuid) throws CRMException {

        IMSIndexInfo indexA = theCRMCatalog.getIndexInfo(INDEX_LEVEL_A, "", "");
        proxy.setQueryEnv(indexA);

        // set return properties
        proxy.setReturnInterval(INTERVAL_BEGIN, CatalogParameters.imsUpperLimit);
        proxy.addReturnAttribute(ATTR_AREA);
        proxy.addReturnAttribute(ATTR_AREA_SPEZ_EXIST);

        // set query condition
        proxy.addQueryAttribute(ATTR_AREA_GUID, OPERATOR_EQ, categoryGuid);

        // execute query
        IMSResultSet resultSetA = proxy.executeQuery(theCRMCatalog.getIMSConnectionInfo());

        // get the area name
        String hasAreaSpecificAttributes = resultSetA.getRow(0).getValue(ATTR_AREA_SPEZ_EXIST);

        return resultSetA;
    }

    /**
     * Executes a query on the S-Index and returns the object guids (OBJECT_GUID) as well
     * as the default attributes of the S-Index.
     * 
     * @param   proxy
     * @param   query
     * @return  the IMSResultSet with the object guids and the S-Index attributes 
     */
    private IMSResultSet querySpecificIndex(IMSProxy proxy, CatalogQuery query) throws CRMException {

        // Get A-Index attributes
        IMSResultSet resultsetA = queryAreaIndex(proxy, query.getParent().getGuid());
        boolean hasAreaSpecificAttributes = "X".equalsIgnoreCase(resultsetA.getRow(0).getValue(ATTR_AREA_SPEZ_EXIST));
        String areaName = resultsetA.getRow(0).getValue(ATTR_AREA);

        // return, if there are no area specific attributes
        if (!hasAreaSpecificAttributes) {
            return null;
        }

        // set environment info for 'S' index
        IMSIndexInfo indexS = theCRMCatalog.getIndexInfo(INDEX_LEVEL_S, areaName, query.getParent().getGuid());
        proxy.setQueryEnv(indexS);

        // let the S-Index return the object guid
        proxy.addReturnAttribute(ATTR_OBJECT_GUID);

        //let the S-Index return the area guid
		proxy.addReturnAttribute(ATTR_AREA_GUID);

        // create a list with the default attributes
        ArrayList defaultAttributesS = new ArrayList();
        Iterator iter = indexS.getAttributes().iterator();
        while (iter.hasNext()) {

            String attributeName = (String) iter.next();

            // add to default attribute list
            if (!theExcludedAttributes.contains(attributeName)) {
                defaultAttributesS.add(attributeName);
            }
        }

        // create new helper instances
        IMSCatalogQueryCompiler compiler = new IMSCatalogQueryCompiler(theCRMCatalog.getMessageResources());

        // transform query statement for the IMS proxy
        compiler.transformItemQuery(query, defaultAttributesS, indexS.getAttributes(), theCRMCatalog, proxy);

        // and now restrict the search in LOC-Index to the given area
        proxy.addQueryAttribute(ATTR_AREA_GUID, OPERATOR_EQ, query.getParent().getGuid());
        proxy.addQueryOperator(IMSProxy.OPERATOR_AND);

        // execute query
        IMSResultSet resultsetS = proxy.executeQuery(theCRMCatalog.getIMSConnectionInfo());

        return resultsetS;
    }

    /**
     * Executes a query on the S-Index and returns the object guids (OBJECT_GUID) as well
     * as the default attributes of the S-Index.
     * 
     * @param   proxy
     * @param   query
     * @return  the IMSResultSet with the object guids and the S-Index attributes,
     *          or null, if no area specific attributes exist.
     */
    private IMSResultSet querySpecificIndexForObjectGuids(IMSProxy proxy, String areaGuid, String[] objectGuids)
        throws CRMException {

        // Get A-Index attributes
        IMSResultSet resultsetA = queryAreaIndex(proxy, areaGuid);
        boolean hasAreaSpecificAttributes = "X".equalsIgnoreCase(resultsetA.getRow(0).getValue(ATTR_AREA_SPEZ_EXIST));
        String areaName = resultsetA.getRow(0).getValue(ATTR_AREA);

        // return, if there are no area specific attributes
        if (!hasAreaSpecificAttributes) {
            return null;
        }

        // set environment info for 'S' index
        IMSIndexInfo indexS = theCRMCatalog.getIndexInfo(INDEX_LEVEL_S, areaName, areaGuid);
        proxy.setQueryEnv(indexS);

        // let the S-Index return the object guid
        proxy.addReturnAttribute(ATTR_OBJECT_GUID);
        proxy.addReturnAttribute(ATTR_AREA_GUID);

        // create a list with the default attributes
        ArrayList defaultAttributesS = new ArrayList();
        Iterator iter = indexS.getAttributes().iterator();
        while (iter.hasNext()) {

            String attributeName = (String) iter.next();

            // add to default attribute list
            if (!theExcludedAttributes.contains(attributeName)) {
                defaultAttributesS.add(attributeName);
            }
        }
        proxy.addReturnAttributes((String[]) defaultAttributesS.toArray(new String[0]));

        // and now restrict the search in LOC-Index to the given area
        proxy.addQueryAttribute(ATTR_AREA_GUID, OPERATOR_EQ, areaGuid);
        proxy.addQueryAttributeList(ATTR_OBJECT_GUID, objectGuids);
        proxy.addQueryOperator(IMSProxy.OPERATOR_AND);

        // execute query
        IMSResultSet resultsetS = proxy.executeQuery(theCRMCatalog.getIMSConnectionInfo());

        return resultsetS;
    }

    /**
     * Performs a search on the S-Index for the <code>ATTR_OBJECT_GUID</code> objects, that are found
     * inside the <code>resultsetP</code> of a previous P-Index <code>query</code>. The attributes, that
     * are found in the S-Index are appended to the <code>query</code>.
     * 
     * The method can handle, that the objects are from multiple areas. In this case, a search on the
     * S-Index is performed for each area..
     * 
     * If <code>resultsetP</code> contains no entries, nothing is done.
     */
    private void readSIndexAttributesForPIndexResults(IMSResultSet resultsetP, CatalogQuery query)
        throws CatalogBuildFailedException {

        final String METHOD_NAME = "readSIndexAttributesForPIndexResults(IMSResultSet resultsetP, CatalogQuery query)";
        log.entering(METHOD_NAME);
        log.debug("query statement:" + query.getStatementAsString());

        // check if something was provided by the previous P-Index query
        if (resultsetP.isEmpty()) {
            log.debug("ResultSet of P-Index is empty. Returning without doing anything.");
            log.exiting();
            return;
        }

        /*
         * S-Index is area specific. We need the products grouped by area to ask the S-Index.
         * objectsPerArea is a HashMap of the areaGuids with an ArrayList of the products in the 
         * according area.
         */
        HashMap objectsPerArea = new HashMap();
        for (Iterator iterP = resultsetP.iterator(); iterP.hasNext();) {

            IMSResultSetRow row = (IMSResultSetRow) iterP.next();

            String objectGuid = row.getValue(ATTR_OBJECT_GUID);
            String areaGuid = row.getValue(ATTR_AREA_GUID);

            if (objectsPerArea.containsKey(areaGuid)) {
                ArrayList objectsInThisArea = (ArrayList) objectsPerArea.get(areaGuid);
                objectsInThisArea.add(objectGuid);
            }
            else {
                ArrayList objectsInThisArea = new ArrayList();
                objectsInThisArea.add(objectGuid);
                objectsPerArea.put(areaGuid, objectsInThisArea);
            }

        }

        IMSProxy proxy = null;
        try {

            // get proxy - has to be released!
            proxy = getProxyPool().getProxy();

            // Now ask S-Index for each area
            Iterator it = objectsPerArea.entrySet().iterator();
            while (it.hasNext()) {

                Map.Entry me = (Map.Entry) it.next();
                String areaGuid = (String) me.getKey();
                ArrayList objectsInThisArea = (ArrayList) me.getValue();

                String[] objectIDs = (String[]) objectsInThisArea.toArray(new String[0]);

                IMSResultSet resultsetS = querySpecificIndexForObjectGuids(proxy, areaGuid, objectIDs);

                IMSResultSet resultsetA = queryAreaIndex(proxy, areaGuid);
                String areaName = resultsetA.getRow(0).getValue(ATTR_AREA);
                
                addCategoryAttributesToQuery(query, areaName, areaGuid);

                // update results
                appendSAttributes(resultsetP, resultsetS, query);
            }
        }
        catch (CRMException ex) {
            throw new CatalogBuildFailedException(ex.getMessage(), ex);
        }

        finally {
            getProxyPool().releaseProxy(proxy);
        }

        log.exiting();
        return;
    }
}
