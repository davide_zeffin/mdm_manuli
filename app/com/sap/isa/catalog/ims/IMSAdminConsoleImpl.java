/**
 * Copyright (c) 2001, SAPMarkets Europe GmbH, Germany, All rights reserved. Created:  07 May 2001 $Revision: #2 $
 * $Date: 2003/04/30 $
 */
package com.sap.isa.catalog.ims;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.Properties;
import java.util.Set;
import java.util.StringTokenizer;

import org.apache.struts.util.MessageResources;
import org.apache.struts.util.MessageResourcesFactory;

import com.sap.isa.catalog.CatalogFactory;
import com.sap.isa.catalog.ICatalogMessagesKeys;
import com.sap.isa.catalog.boi.IMSAdminQueryStatementData;
import com.sap.isa.catalog.boi.IQueryStatement;
import com.sap.isa.catalog.boi.IServerEngine;
import com.sap.isa.catalog.misc.CatalogJCoLogHelper;
import com.sap.isa.catalog.trexcrm.TrexCRMCatalogException;
import com.sap.isa.catalog.trexcrm.TrexCRMRfcMethods;
import com.sap.isa.core.eai.BackendBusinessObjectParams;
import com.sap.isa.core.eai.BackendContext;
import com.sap.isa.core.eai.BackendException;
import com.sap.isa.core.eai.BackendObjectManagerImpl;
import com.sap.isa.core.eai.BackendObjectSupport;
import com.sap.isa.core.eai.Connection;
import com.sap.isa.core.eai.ConnectionDefinition;
import com.sap.isa.core.eai.ConnectionFactory;
import com.sap.isa.core.eai.ManagedConnectionFactoryConfig;
import com.sap.isa.core.eai.sp.jco.BackendBusinessObjectSAP;
import com.sap.isa.core.eai.sp.jco.JCoConnection;
import com.sap.isa.core.eai.sp.jco.JCoConnectionEventListener;
import com.sap.isa.core.eai.sp.jco.JCoManagedConnectionFactory;
import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.core.logging.LogUtil;
import com.sap.isa.core.util.table.ResultData;
import com.sap.isa.core.util.table.Table;
import com.sap.isa.core.util.table.TableRow;
import com.sap.mw.jco.JCO;
import com.sapportals.trex.TrexException;
import com.sapportals.trex.core.DocAttribute;
import com.sapportals.trex.core.IndexName;
import com.sapportals.trex.core.QueryEntry;
import com.sapportals.trex.core.ResultDocument;
import com.sapportals.trex.core.SearchManager;
import com.sapportals.trex.core.SearchResult;
import com.sapportals.trex.core.TrexFactory;
import com.sapportals.trex.core.admin.AdminIndex;


/**
 * This class implements the functionality required for getting technical information about an IndexMessageServer
 * (IMS). The class has <code>public</code> visibility because it is instantiated through the EAI framework.
 *
 * @version 1.0
 */
public class IMSAdminConsoleImpl implements BackendBusinessObjectSAP, IMSAdminConsole {
    public static final String BO_TYPE_CATALOG = "CATALOG";
    private static String IMS_ADMIN_CONSOLE = "IMSAdminConsole";
    private static BackendObjectManagerImpl theBEM = null;

    // the logging instance
    private static IsaLocation theStaticLocToLog = IsaLocation.getInstance(IMSAdminConsoleImpl.class.getName());
    private MessageResources theMessageResources;
    private IMSProxy theProxy = null;

    // attributes for the currently seclected index
    private IMSConnectionInfo theIMSInfo;
    private IMSIndexInfo theIndexInfo;
    private String theCodepage;
    private String theLanguage;
    private String theSearchEngine;

    // holds the value for the staging support value
    private String stagingSupport = null;

    // Connection infos
    private ConnectionFactory theConnectionFactory;
    private BackendContext theBackendContext;
    private BackendObjectSupport theBEOS;
    private Properties theConnectionProps;
    private JCoConnection theSAPConnection;
    private ResultData versionList;

    /**
     * Default constructor. Never call this constructor directly but over the factory method. The constructor has
     * <code>public</code> visibility because the instances are managed via the EAI layer.
     *
     * @throws CRMException DOCUMENT ME!
     */
    public IMSAdminConsoleImpl()
                        throws CRMException {
    }

    /**
     * Factory method to get an instance of the IMS admin console. <br>
     * <b>IMPORTANT: </b>Always use this factory method otherwise the connection management will not work!
     *
     * @param aBEOS DOCUMENT ME!
     */

    //    synchronized public static IMSAdminConsole createInstance(String confId,
    //                                                              String confData)
    //        throws CRMException {
    //            
    //        try {
    //            if (theBEM == null) {
    //                // create backend object manager
    //                Properties theBEMProps = new Properties();
    //                theBEMProps.setProperty(SessionConst.EAI_CONF_ID, confId);
    //
    //                if ((confData != null) && !confData.equals("")) {
    //                    theBEMProps.setProperty(SessionConst.EAI_CONF_DATA, confData);
    //                }
    //
    //                // end of if ()
    //                theBEM = new BackendObjectManagerImpl(theBEMProps);
    //            }
    //
    //            // end of if ()
    //            return (IMSAdminConsole) theBEM.createBackendBusinessObject(IMS_ADMIN_CONSOLE, null);
    //        }
    //        catch (BackendException ex) {
    //            throw new CRMException(ex.getLocalizedMessage());
    //        }
    //    }

    /**
     * DOCUMENT ME!
     *
     * @param aBEOS DOCUMENT ME!
     */
    public void setBackendObjectSupport(BackendObjectSupport aBEOS) {
        theBEOS = aBEOS;

        return;
    }

    /**
     * DOCUMENT ME!
     *
     * @return DOCUMENT ME!
     */
    public BackendObjectSupport getBackendObjectSupport() {
        return theBEOS;
    }

    /**
     * DOCUMENT ME!
     *
     * @return DOCUMENT ME!
     */
    public JCoConnection getDefaultJCoConnection() {
        if (theSAPConnection == null) {
            setDefaultConnection(getConnectionFactory().getDefaultConnection());
        }

        return theSAPConnection;
    }

    /**
     * DOCUMENT ME!
     *
     * @param props DOCUMENT ME!
     *
     * @return DOCUMENT ME!
     *
     * @throws BackendException DOCUMENT ME!
     */
    public JCoConnection getDefaultJCoConnection(Properties props)
                                          throws BackendException {
        return (JCoConnection) theConnectionFactory.getDefaultConnection(getDefaultJCoConnection()
                                                                             .getConnectionFactoryName(),
                                                                         getDefaultJCoConnection()
                                                                             .getConnectionConfigName(), props);
    }

    /**
     * DOCUMENT ME!
     *
     * @param conDefProps DOCUMENT ME!
     *
     * @return DOCUMENT ME!
     *
     * @throws BackendException DOCUMENT ME!
     */
    public JCoConnection getModDefaultJCoConnection(Properties conDefProps)
                                             throws BackendException {
        ConnectionFactory factory = getConnectionFactory();
        JCoConnection defConn = (JCoConnection) factory.getDefaultConnection();

        return (JCoConnection) factory.getConnection(defConn.getConnectionFactoryName(),
                                                     defConn.getConnectionConfigName(), conDefProps);
    }

    /**
     * DOCUMENT ME!
     *
     * @param conKey DOCUMENT ME!
     *
     * @return DOCUMENT ME!
     *
     * @throws BackendException DOCUMENT ME!
     */
    public JCoConnection getJCoConnection(String conKey)
                                   throws BackendException {
        return (JCoConnection) getConnectionFactory().getConnection(conKey);
    }

    /**
     * This method is used to add JCo-call listener to this backend object. A JCo listener will be notified before a
     * JCo call is being executed and after the JCo call has been exected.
     *
     * @param listener a listener for JCo calls
     */
    public void addJCoConnectionEventListener(JCoConnectionEventListener listener) {
        theConnectionFactory.addConnectionEventListener(listener);
    }

    /**
     * DOCUMENT ME!
     *
     * @param conFactory DOCUMENT ME!
     */
    public void setConnectionFactory(ConnectionFactory conFactory) {
        theConnectionFactory = conFactory;
    }

    /**
     * DOCUMENT ME!
     *
     * @return DOCUMENT ME!
     */
    public ConnectionFactory getConnectionFactory() {
        return theConnectionFactory;
    }

    /**
     * DOCUMENT ME!
     *
     * @param props DOCUMENT ME!
     * @param params DOCUMENT ME!
     *
     * @throws BackendException DOCUMENT ME!
     */
    public void initBackendObject(Properties                  props,
                                  BackendBusinessObjectParams params)
                           throws BackendException {
        theConnectionProps = props;

        // check if also an ims catalog was configured. In this case overwrite
        // the properties of the console with the properties of the catalog.
        //        BackendBusinessObjectMetaData metaInfo = theBEM.getBackendBusinessObjectMetaData(BO_TYPE_CATALOG);
        //
        //        if ((metaInfo != null) &&
        //                metaInfo.getBackendBusinessObjectConfig().getClassName().equals("com.sap.isa.catalog.ims.IMSCatalog")) {
        //            theConnectionProps = metaInfo.getBackendBusinessObjectConfig().getProperties();
        //        }
    }

    /**
     * DOCUMENT ME!
     */
    public void destroyBackendObject() {
        // not necessary yet
    }

    /**
     * DOCUMENT ME!
     *
     * @param context DOCUMENT ME!
     */
    public void setContext(BackendContext context) {
        theBackendContext = context;
    }

    /**
     * DOCUMENT ME!
     *
     * @return DOCUMENT ME!
     */
    public BackendContext getContext() {
        return theBackendContext;
    }

    /**
     * Returns the connection properties.
     *
     * @return value of the requested property
     */
    Properties getConnectionProperties() {
        return theConnectionProps;
    }

    /**
     * Sets the default connection to the system where the infos about the catalogs are located.
     *
     * @param conn the default connection
     */
    private void setDefaultConnection(Connection conn) {
        theSAPConnection = (JCoConnection) conn;
    }

    /**
     * DOCUMENT ME!
     *
     * @return DOCUMENT ME!
     *
     * @throws CRMException DOCUMENT ME!
     */
    public Map getSystemList()
                      throws CRMException {
        Map results = new HashMap();

        try {
            String facName = theBEM.getBackendBusinessObjectMetaData(IMS_ADMIN_CONSOLE).getBackendBusinessObjectConfig()
                                   .getConnectionFactoryName();

            Map configs = getConnectionFactory().getManagedConnectionFactoryConfigs();
            ManagedConnectionFactoryConfig connections = (ManagedConnectionFactoryConfig) configs.get(facName);

            Iterator iter = connections.getConnectionDefinitions().values().iterator();

            while (iter.hasNext()) {
                ConnectionDefinition def = (ConnectionDefinition) iter.next();
                Properties props = def.getProperties();

                if ((props.getProperty(JCoManagedConnectionFactory.JCO_MSHOST) != null) ||
                        (props.getProperty(JCoManagedConnectionFactory.JCO_R3NAME) != null) ||
                        (props.getProperty(JCoManagedConnectionFactory.JCO_ASHOST) != null)) {
                    results.put(def.getName(), props);
                }
            }
        }
        catch (BackendException ex) {
            throw new CRMException(ex.getLocalizedMessage(), ex);
        }

        return results;
    }

    /**
     * DOCUMENT ME!
     *
     * @param props DOCUMENT ME!
     *
     * @throws CRMException DOCUMENT ME!
     */
    public void setSystem(Properties props)
                   throws CRMException {
        JCO.Client client = null;
        JCoConnection modConn = null;

        try {
            modConn = getModDefaultJCoConnection(props);
            // try if connection is valid
            client = modConn.getJCoClient();
            client.connect();
            // no exception appeared
            setDefaultConnection(modConn);
            // delete the current index, connection infos
            setConnectionInfo(null);
            setIndexInfo(null);
            setCodepage("");
            setLanguage("");
            setSearchEngine("");
        }
        catch (BackendException ex) {
            throw new CRMException(ex.getLocalizedMessage());
        }
        finally {
            if (modConn != null) {
                modConn.close();
            }

            // end of if ()
        }
    }

    /**
     * DOCUMENT ME!
     *
     * @param catalogPattern DOCUMENT ME!
     *
     * @return DOCUMENT ME!
     *
     * @throws CRMException DOCUMENT ME!
     */
    public ResultData getIndexList(String catalogPattern)
                            throws CRMException {
        JCoConnection aConnection = getDefaultJCoConnection();

        if (theStaticLocToLog.isDebugEnabled()) {
            String msg = getMessageResources().getMessage(ICatalogMessagesKeys.PCAT_IMS_USING_CONNECTION,
                                                          aConnection.getAttributes().toString());
            theStaticLocToLog.debug(msg);
        }

        // end of if ()
        ResultData indexList = null;

        if (stagingSupport.equals(IServerEngine.STAGING_NOT_SUPPORTED)) {
            indexList = CRMRfcMethods.getIndexList(aConnection, catalogPattern);
        }
        else {
            try {
                indexList = getFullIndexlist(catalogPattern);
            }
            catch (TrexCRMCatalogException e) {
                throw new CRMException("getFullIndex failed", e);
            }
            catch (BackendException e) {
                throw new CRMException("getFullIndex failed", e);
            }
        }

        return indexList;
    }

    /**
     * DOCUMENT ME!
     *
     * @param catName DOCUMENT ME!
     * @param catVariant DOCUMENT ME!
     * @param attrLevel DOCUMENT ME!
     * @param areaName DOCUMENT ME!
     * @param catId DOCUMENT ME!
     *
     * @throws CRMException DOCUMENT ME!
     */
    public void setIndex(String catName,
                         String catVariant,
                         String attrLevel,
                         String areaName,
                         String catId)
                  throws CRMException {
        CRMRfcMethods.getIMSInfos(this, catName, catVariant, attrLevel, areaName, catId);
    }

    /**
     * DOCUMENT ME!
     *
     * @param catIdx DOCUMENT ME!
     *
     * @throws CRMException DOCUMENT ME!
     */
    public void setIndex(String catIdx)
                  throws CRMException {
        AdminIndex adminIndex = TrexCRMRfcMethods.getTrexIndex(catIdx);

        // create Index information
        IMSIndexInfo indexInfo = new IMSIndexInfo(catIdx, "<empty>", "<empty>", "<empty>");

        for (int j = 0; j < adminIndex.countDocumentAttribute(); j++) {
            String attrName;

            if (j == 0) {
                attrName = adminIndex.getFirstDocumentAttribute().getName();
            }
            else {
                attrName = adminIndex.getNextDocumentAttribute().getName();
            }

            indexInfo.addAttribute(attrName);
            theStaticLocToLog.debug("doc name: " + attrName);
        }

        // set the index as the current index in the console
        this.setIndexInfo(indexInfo);

        // set the connection info as the currend IMS connection in the console
        IMSConnectionInfo connectionInfo = new IMSConnectionInfo(this, this.getConnectionProperties(), "", "", "", "");
        this.setConnectionInfo(connectionInfo);
    }

    /**
     * DOCUMENT ME!
     *
     * @return DOCUMENT ME!
     *
     * @throws CRMException DOCUMENT ME!
     */
    public String[] getIndexAttributeNames()
                                    throws CRMException {
        if (theIndexInfo == null) {
            throw new CRMException(getMessageResources().getMessage(ICatalogMessagesKeys.PCAT_ERR_NO_INDEX_SELECTED));
        }

        return (String[]) theIndexInfo.getAttributes().toArray(new String[0]);
    }

    /**
     * DOCUMENT ME!
     *
     * @param query DOCUMENT ME!
     *
     * @return DOCUMENT ME!
     *
     * @throws CRMException DOCUMENT ME!
     */
    public IMSResultSet getIndexData(IQueryStatement query)
                              throws CRMException {
        IMSResultSet result = null;

        // distinguish between staging and non-staging
        if (stagingSupport.equals(IServerEngine.STAGING_NOT_SUPPORTED)) {
            // non-staging
            IMSProxy proxy = getProxy();

            synchronized (proxy) {
                // set query environment
                proxy.setQueryEnv(getSearchEngine(), getCodepage(), getLanguage(), getIndexInfo());

                // set query condition
                IMSCatalogQueryCompiler compiler = new IMSCatalogQueryCompiler(getMessageResources());
                IMSAdminQueryStatementData statement = (IMSAdminQueryStatementData) query;
                compiler.transformFilterQuery(proxy, theIndexInfo.getAttributes(), statement.getFilter(),
                                              statement.getAttributes(),
                                              (statement.getSortOrder() != null) ? statement.getSortOrder()[0] : null,
                                              statement.getRangeFrom(), statement.getRangeTo());

                // execute query
                result = proxy.executeQuery(getConnectionInfo());
            }
        }

        return result;
    }

    /**
     * DOCUMENT ME!
     *
     * @param query DOCUMENT ME!
     *
     * @return DOCUMENT ME!
     *
     * @throws CRMException DOCUMENT ME!
     */
    public ResultData getStagingIndexData(IQueryStatement query, String[] attributes, String[] sortAttributes)
                                   throws CRMException {
        ResultData resTab = null;

        // staging
        // a queryString looks like 
        // ( ( area=containing(areavalue) AND ( area=fuzzy(areavalue) AND area=linguistic(areavalue) ) ) OR NOT ( area_guid="guid" ) )
        String catIdx = getIndexInfo().getCatId();

        String filterString = query.getFilter().toString();
        SearchManager searchManager = TrexFactory.getSearchManager();

        try {
            searchManager.addIndexId(catIdx, getTheLanguage());

            /*  set the range of the result */
            searchManager.setResultFromTo(1, 10000); // default search range :-)
            setRequestedAttributes(searchManager, attributes);
            setSortAttributes(searchManager, sortAttributes);
            createQueryFromString(searchManager, filterString);

            SearchResult trexSearchResult = searchManager.search();

            // only for debugging
            // dumpTREXSearchResult(trexSearchResult);
            // map the results to the IMSResultSet
            resTab = mapTREXResultsToResultData(trexSearchResult);
        }
        catch (TrexException e) {
            throw new CRMException("getIndexData failed. Cause: ", e);
        }

        return resTab;
    }

    private void setSortAttributes(SearchManager searchManager, String[] sortAttributes) throws TrexException {
        if (sortAttributes != null) {
            // use only the first one as TREX can handle only one attribute
            // the IMSAdmin UI implements this selection as a set of radiobuttons anyway
            searchManager.addSortAttribute(sortAttributes[0], true); 
        }
    }

    private void setRequestedAttributes(SearchManager searchManager, String[] attributes) throws TrexException {
        if (attributes != null) {
            for (int i = 0; i < attributes.length; i++) {
                searchManager.addRequestedAttribute(attributes[i]);
            }
        }
        else {
            // get all
            searchManager.addRequestedAttribute("*");
        }
    }

    private Set collectAttrNames(SearchResult trexSearchResult) {
        HashSet nameSet = null;

        if (trexSearchResult.size() > 0) {        
            nameSet = new HashSet();
            ResultDocument doc = trexSearchResult.getFirstResultDocument();
            DocAttribute attr;
            
            for (int i = 0; i < trexSearchResult.size(); i++) {
                attr = doc.getFirstDocumentAttribute();
                while (attr != null) {
                    String attrName = null;
                    IndexName myIndexName = null;
                    attrName = attr.getIndexAndAttr(myIndexName);

                    if (attrName.equalsIgnoreCase("ipc_pricing_att")) {
                        String attrValue = attr.getValue1();
                        
                        int assignmentPos = attrValue.indexOf('=');

                        String attrPostFix = attrValue.substring(0, assignmentPos);
                        String value = attrValue.substring(assignmentPos + 1);
                               
                        nameSet.add(attrName + "::" + attrPostFix); 
                    }
                    else {
                        nameSet.add(attrName);
                    }
                    attr = doc.getNextDocumentAttribute();
                }
                doc = trexSearchResult.getNextResultDocument();
            }
            
        }
        
        return nameSet;
    }

    private ResultData mapTREXResultsToResultData(SearchResult trexSearchResult) throws CRMException {
        Table resTab = null;
        String attrName = null;

        // build a generic result table
        resTab = new Table("IndexList");

        Set nameSet = collectAttrNames(trexSearchResult);

        Iterator iter = nameSet.iterator();
        while (iter.hasNext()) {
            attrName = (String) iter.next();
            resTab.addColumn(Table.TYPE_STRING, attrName);
        }
        
        ResultDocument doc = trexSearchResult.getFirstResultDocument();
        DocAttribute attr = doc.getFirstDocumentAttribute();

        IndexName myIndexName = null;
        TableRow resRow = null;
        try {
            if (trexSearchResult.size() != 0) {
                for (int i = 0; i < trexSearchResult.size(); i++) {
                    attr = doc.getFirstDocumentAttribute();
    
                    resRow = resTab.insertRow();
                    while (attr != null) {
                        attrName = attr.getIndexAndAttr(myIndexName);
                        
                        if (attrName.equalsIgnoreCase("ipc_pricing_att")) {
                            String attrValue = attr.getValue1();
                        
                            int assignmentPos = attrValue.indexOf('=');

                            String attrPostFix = attrValue.substring(0, assignmentPos);
                            String value = attrValue.substring(assignmentPos + 1);
                               
                            nameSet.add(attrName + "::" + attrPostFix); 
                            resRow.getField(attrName + "::" + attrPostFix).setValue(value);
                        }
                        else {
                            resRow.getField(attrName).setValue(attr.getValue1());
                        }
                        attr = doc.getNextDocumentAttribute();
                    }
    
                    doc = trexSearchResult.getNextResultDocument();
                }
            }
        }
        catch (Exception e) {
            theStaticLocToLog.debug(e.getStackTrace());
            throw new CRMException("Error in mapTREXResultsToResultData", e);
        }

        return new ResultData(resTab);
    }

    private void dumpTREXSearchResult(SearchResult trexSearchResult) {
        theStaticLocToLog.debug("Errorcode: " + trexSearchResult.getLastError().getCode() + " " +
                                trexSearchResult.getLastError().getMsg());

        theStaticLocToLog.debug("Number of Hits: " + trexSearchResult.size());
        theStaticLocToLog.debug("Number of all hits in Index: " + trexSearchResult.getNoOfAllHitsInIndex());

        //      wieviel Treffer sind  tatsächlich für die Query  im Index
        theStaticLocToLog.debug("Index size: " + trexSearchResult.getIndexSize()); // wieviele Docs sind im Index

        theStaticLocToLog.debug("Result Documents:");

        ResultDocument doc = trexSearchResult.getFirstResultDocument();

        for (int i = 0; i < trexSearchResult.size(); i++) {
            //    write document key + rank value
            theStaticLocToLog.debug("Document Status: " + doc.getDocumentStatus());
            theStaticLocToLog.debug(" Document Key: " + doc.getDocumentKey());
            theStaticLocToLog.debug(" Rank value: " + doc.getRankValue()); // bei reiner Attr Search stets 0
            theStaticLocToLog.debug(" found in index : " + doc.getIndexId().getIndexName() + " language : " +
                                    doc.getIndexId().getLanguage());

            DocAttribute attr = doc.getFirstDocumentAttribute();

            while (attr != null) {
                String attrName = null;
                IndexName myIndexName = null;
                attrName = attr.getIndexAndAttr(myIndexName);

                StringBuffer buf = new StringBuffer();
                buf.append("    Test attribute " + attrName);
                buf.append("    attribute " + attr.getAttributeName());
                buf.append(" " + attr.getValue1() + " ");
                theStaticLocToLog.debug(buf.toString());
                theStaticLocToLog.debug(attr.getAttributeType());
                attr = doc.getNextDocumentAttribute();
            }

            doc = trexSearchResult.getNextResultDocument();
        }
    }

    /**
     * DOCUMENT ME!
     *
     * @param searchManager DOCUMENT ME!
     * @param queryString DOCUMENT ME!
     *
     * @throws CRMException DOCUMENT ME!
     * @throws TrexException DOCUMENT ME!
     */
    public void createQueryFromString(SearchManager searchManager,
                                      String        queryString)
                               throws CRMException, TrexException {
        StringTokenizer strTok = new StringTokenizer(queryString);

        while (strTok.hasMoreTokens()) {
            String token = strTok.nextToken();
            QueryEntry qentry = getQueryEntryFromToken(token);
            searchManager.addQueryEntry(qentry);
        }
    }

    /**
     * DOCUMENT ME!
     *
     * @param queryToken DOCUMENT ME!
     *
     * @return DOCUMENT ME!
     *
     * @throws TrexException DOCUMENT ME!
     */
    public QueryEntry getQueryEntryFromToken(String queryToken)
                                      throws TrexException {
        QueryEntry qentry = new QueryEntry();

        // query token can be:
        // '(', ')', AND, OR, NOT and a expression
        if (queryToken.equals("(")) {
            qentry.setRowType(QueryEntry.ROW_TYPE_BRACKET_OPEN);
        }
        else if (queryToken.equals(")")) {
            qentry.setRowType(QueryEntry.ROW_TYPE_BRACKET_CLOSE);
        }
        else if (queryToken.equals("AND")) {
            qentry.setValue(QueryEntry.QUERY_OPERATOR_AND, "", QueryEntry.ATTRIBUTE_OPERATOR_EQ);
            qentry.setRowType(QueryEntry.ROW_TYPE_OPERATOR);
        }
        else if (queryToken.equals("OR")) {
            qentry.setValue(QueryEntry.QUERY_OPERATOR_OR, "", QueryEntry.ATTRIBUTE_OPERATOR_EQ);
            qentry.setRowType(QueryEntry.ROW_TYPE_OPERATOR);
        }
        else if (queryToken.equals("NOT")) {
        }
        else {
            // ok, this is an expression
            // an expression can be 
            // attrname=value, attrname=containing(value), attrname=fuzzy(value) or attrname=linguistic(value)
            // all of them can be preceeded by NOT
            qentry = evalExpression(queryToken);
        }

        return qentry;
    }

    /**
     * DOCUMENT ME!
     *
     * @param queryToken DOCUMENT ME!
     *
     * @return DOCUMENT ME!
     *
     * @throws TrexException DOCUMENT ME!
     */
    public QueryEntry evalExpression(String queryToken)
                              throws TrexException {
        QueryEntry qentry = new QueryEntry();
        String attr;
        String value;
        int assignmentPos = queryToken.indexOf('=');

        attr = queryToken.substring(0, assignmentPos);
        value = queryToken.substring(assignmentPos + 1);

        // is value a function? i.e. fuzzy(...) 
        if (value.startsWith("containing")) {
            value = value.substring(value.indexOf('(') + 1, value.length() - 1);

            qentry.setLocation(attr);
            qentry.setValue(value, "", QueryEntry.ATTRIBUTE_OPERATOR_EQ);
            qentry.setRowType(QueryEntry.ROW_TYPE_ATTRIBUTE);
            qentry.setTermAction(QueryEntry.TERM_ACTION_NATURAL_TEXT);
        }
        else if (value.startsWith("fuzzy")) {
            value = value.substring(value.indexOf('(') + 1, value.length() - 1);

            qentry.setLocation(attr);
            qentry.setValue(value, "", QueryEntry.ATTRIBUTE_OPERATOR_EQ);
            qentry.setRowType(QueryEntry.ROW_TYPE_ATTRIBUTE);
            qentry.setTermAction(QueryEntry.TERM_ACTION_FUZZY);
        }
        else if (value.startsWith("linguistic")) {
            value = value.substring(value.indexOf('(') + 1, value.length() - 1);

            qentry.setLocation(attr);
            qentry.setValue(value, "", QueryEntry.ATTRIBUTE_OPERATOR_EQ);
            qentry.setRowType(QueryEntry.ROW_TYPE_ATTRIBUTE);
            qentry.setTermAction(QueryEntry.TERM_ACTION_LINGUISTIC);
        }
        else {
            // simple name-value-assignment
            // delete some special characters from the string
            value = cleanUpValueString(value);

            qentry.setLocation(attr);
            qentry.setValue(value, "", QueryEntry.ATTRIBUTE_OPERATOR_EQ);
            qentry.setRowType(QueryEntry.ROW_TYPE_ATTRIBUTE);
            qentry.setTermAction(QueryEntry.TERM_ACTION_EXACT);
        }

        return qentry;
    }

    private String cleanUpValueString(String value) {
        StringBuffer cleanValue = new StringBuffer();

        for (int i = 0; i < value.length(); i++) {
            char c = value.charAt(i);

            // currently only check for '"' 
            if (c != '"') {
                cleanValue.append(c);
            }
        }

        return cleanValue.toString();
    }

    /**
     * DOCUMENT ME!
     *
     * @return DOCUMENT ME!
     */
    public Properties getDetails() {
        Properties props = new Properties();

        if (getConnectionProperties().getProperty("useDynConnParams").equals("false")) {
            try {
                String facName = getConnectionProperties().getProperty("DynConnType");
                String connName = getConnectionProperties().getProperty("DynConnName");
                Map configs = getConnectionFactory().getManagedConnectionFactoryConfigs();
                ManagedConnectionFactoryConfig connections = (ManagedConnectionFactoryConfig) configs.get(facName);
                String name = connections.getConnectionConfig(connName).getConnectionDefinition();
                props = connections.getConnectionDefinition(name).getProperties();
            }
            catch (BackendException ex) {
				theStaticLocToLog.debug(ex.getMessage());
                // do nothing
            }
        }
        else if (getConnectionInfo() != null) {
            props = (Properties) getConnectionInfo().getProperties().clone();
        }

        if (getIndexInfo() != null) {
            IMSIndexInfo info = getIndexInfo();
            props.setProperty("area", info.getArea());
            props.setProperty("level", info.getAttrLevel());
            props.setProperty("idstoreid", info.getStoreId());
            props.setProperty("catid", info.getCatId());
            props.setProperty("codepage", getCodepage());
            props.setProperty("language", getTheLanguage());
            props.setProperty("engine", getTheSearchEngine());
        }

        return props;
    }

    /**
     * Returns an information object about the current index.
     *
     * @return an information object about an index
     */
    synchronized IMSIndexInfo getIndexInfo() {
        return theIndexInfo;
    }

    /**
     * Sets the current index info object.
     *
     * @param info the info object to be set
     */
    synchronized void setIndexInfo(IMSIndexInfo info) {
        theIndexInfo = info;
    }

    /**
     * Sets the current connection info object.
     *
     * @param info the connection info object
     */
    synchronized void setConnectionInfo(IMSConnectionInfo info) {
        theIMSInfo = info;
    }

    /**
     * Returns the current connection info object.
     *
     * @return connection info object.
     */
    synchronized IMSConnectionInfo getConnectionInfo() {
        return theIMSInfo;
    }

    /**
     * Sets the codepage of the current index.
     *
     * @param codepage the codepage to be set
     */
    void setCodepage(String codepage) {
        theCodepage = codepage;
    }

    /**
     * Returns the codepage.
     *
     * @return the codepage of the catalog
     */
    String getCodepage() {
        return (theCodepage == null) ? "" : theCodepage;
    }

    /**
     * Sets the language of the current index.
     *
     * @param language the language to be set
     */
    void setLanguage(String language) {
        theLanguage = language;
    }

    /**
     * Returns the language.
     *
     * @return the language of the index
     */
    String getLanguage() {
        return theLanguage;
    }

    /**
     * Sets the search engine of the current index.
     *
     * @param engine the engine to be set
     */
    void setSearchEngine(String engine) {
        theSearchEngine = engine;
    }

    /**
     * Returns the search engine.
     *
     * @return the search engine
     */
    String getSearchEngine() {
        return theSearchEngine;
    }

    /**
     * Returns the message resources associated with the admin instance.
     *
     * @return message resources
     */
    MessageResources getMessageResources() {
        if (theMessageResources == null) {
            MessageResourcesFactory factoryObject = MessageResourcesFactory.createFactory();
            theMessageResources = factoryObject.createResources(CatalogFactory.THE_DEFAULT_MESSAGE_RESOURCES_CLASS);
            theMessageResources.setReturnNull(false);
        }

        return theMessageResources;
    }

    /**
     * Returns the IMS proxy to be used for the queries.
     *
     * @return the IMS proxy instance
     *
     * @throws CRMException DOCUMENT ME!
     */
    private IMSProxy getProxy()
                       throws CRMException {
        if (theIMSInfo == null) {
            throw new CRMException(getMessageResources().getMessage(ICatalogMessagesKeys.PCAT_ERR_NO_INDEX_SELECTED));
        }

        if (theProxy == null) {
            theProxy = IMSProxy.createProxy(theIMSInfo, getMessageResources());
        }

        return theProxy;
    }

    /**
     * gets the level of the staging support in the backend
     *
     * @return A, B, C, or D or the constant IServerEngine.STAGING_NOT_SUPPORTED which should be same as A
     */
    public String getStagingSupport() {
        theStaticLocToLog.entering("getStagingSupport()");

        try {
            JCoConnection connection = getDefaultJCoConnection();
            stagingSupport = TrexCRMRfcMethods.getStagingSupport(connection);
        }
        catch (TrexCRMCatalogException tce) {
            theStaticLocToLog.error(LogUtil.APPS_COMMON_INFRASTRUCTURE, ICatalogMessagesKeys.PCAT_ERR_INIT_INSTANCE,
                                    new Object[] { this.getClass().getName(), tce.getLocalizedMessage() }, tce);
        }

        theStaticLocToLog.exiting();

        return stagingSupport;
    }

    /**
     * DOCUMENT ME!
     *
     * @param language The language to read the cataloginfo in
     * @param withNames Read the catalogue names and some other info also if set to true. Currently not supported.
     *
     * @return a table containing the list of catalogues
     *
     * @throws CRMException DOCUMENT ME!
     */
    public ResultData getCatalogueList(String  language,
                                       boolean withNames)
                                throws CRMException {
        JCoConnection aConnection = getDefaultJCoConnection();

        // use this language for all our stuff
        setLanguage(language);

        if (theStaticLocToLog.isDebugEnabled()) {
            String msg = getMessageResources().getMessage(ICatalogMessagesKeys.PCAT_IMS_USING_CONNECTION,
                                                          aConnection.getAttributes().toString());
            theStaticLocToLog.debug(msg);
        }

        // end of if ()
        return CRMRfcMethods.getCatalogueList(getDefaultJCoConnection(), language, withNames);
    }

    /**
     * DOCUMENT ME!
     *
     * @param catalogPattern DOCUMENT ME!
     *
     * @return DOCUMENT ME!
     *
     * @throws CRMException DOCUMENT ME!
     */
    public ResultData getVersionList(String catalogPattern)
                              throws CRMException {
        JCoConnection aConnection = getDefaultJCoConnection();

        if (theStaticLocToLog.isDebugEnabled()) {
            String msg = getMessageResources().getMessage(ICatalogMessagesKeys.PCAT_IMS_USING_CONNECTION,
                                                          aConnection.getAttributes().toString());
            theStaticLocToLog.debug(msg);
        }

        versionList = null;

        try {
            versionList = TrexCRMRfcMethods.getIndexList(aConnection, catalogPattern);
        }
        catch (TrexCRMCatalogException e) {
            if (theStaticLocToLog.isDebugEnabled()) {
                theStaticLocToLog.debug(e.getLocalizedMessage());
            }
        }

        return versionList;
    }

    /**
     * DOCUMENT ME!
     *
     * @param indexGuid DOCUMENT ME!
     *
     * @return DOCUMENT ME!
     *
     * @throws TrexCRMCatalogException DOCUMENT ME!
     * @throws BackendException DOCUMENT ME!
     */
    public ResultData getFullIndexlist(String indexGuid)
                                throws TrexCRMCatalogException, BackendException {
        ResultData indexList = getTRexIds(indexGuid);

        // maybe we need to reformat the indexList a little bit

        /*
        String catalog = (tab.getString("PCAT")       ==null)?"":tab.getString("PCAT");
        String variant = (tab.getString("PCATVARIANT")==null)?"":tab.getString("PCATVARIANT");
        String area =    (tab.getString("AREA")       ==null)?"":tab.getString("AREA");
        String level =   (tab.getString("ATTRLEVEL")  ==null)?"":tab.getString("ATTRLEVEL");
        String catid =   (tab.getString("IDXCATEGORY")==null)?"":tab.getString("IDXCATEGORY");
         */

        // the catalogGuid is the key to the corresponding line in the versionList table         
        return indexList;
    }

    /**
     * reads the index's techkey from the catalogue passed by techkey catalogKey
     *
     * @param catalogKey TechKey of the catalogue
     *
     * @return the techkey of the desired index
     *
     * @throws BackendException DOCUMENT ME!
     * @throws TrexCRMCatalogException DOCUMENT ME!
     */
    public ResultData getTRexIds(String catalogKey)
                          throws BackendException, TrexCRMCatalogException {
        Table resTab = null;
        JCO.Function function = null;
        JCoConnection connection = getDefaultJCoConnection();

        function = connection.getJCoFunction("COM_PCAT_STAGING_IDX_GET");

        // set import parameters
        JCO.ParameterList impList = function.getImportParameterList();
        impList.setValue(catalogKey, "IV_TECHKEY");
        impList.setValue(" ", "IV_ATTR_LEVEL");

        try {
            CatalogJCoLogHelper.logCall(function.getName(), function.getImportParameterList(), null,
                                        function.getTableParameterList(), theStaticLocToLog);

            connection.execute(function);

            CatalogJCoLogHelper.logCall(function.getName(), null, function.getExportParameterList(),
                                        function.getTableParameterList(), theStaticLocToLog);
        }
        catch (JCO.AbapException ex) {
            String msg = "getCatIdForLevel(catalogKey=" + catalogKey + ") failed with JOC.AbapException '" +
                         ex.getLocalizedMessage() + "'";
            theStaticLocToLog.error(ICatalogMessagesKeys.PCAT_EXCEPTION, new Object[] { msg }, null);
            throw new TrexCRMCatalogException(ex.getLocalizedMessage());
        }

        // get export parameters
        JCO.ParameterList expList = function.getExportParameterList();

        // analyse the fixed values of the attributes
        JCO.Table etIdxTab = expList.getTable("ET_RESULTLIST");

        int rows = etIdxTab.getNumRows();

        // build a generic result table
        resTab = new Table("IndexList");
        resTab.addColumn(Table.TYPE_STRING, "CAT_IDX");
        resTab.addColumn(Table.TYPE_STRING, "ATTR_LEVEL");
        resTab.addColumn(Table.TYPE_STRING, "LOC_ID");
        resTab.addColumn(Table.TYPE_STRING, "LOC_GUID");
        resTab.addColumn(Table.TYPE_STRING, "LOC_DESCR");

        if (etIdxTab.getNumRows() != 0) {
            etIdxTab.firstRow();

            do {
                TableRow resRow = resTab.insertRow();
                resRow.getField("CAT_IDX").setValue(etIdxTab.getString("CAT_IDX"));
                resRow.getField("ATTR_LEVEL").setValue(etIdxTab.getString("ATTR_LEVEL"));
                resRow.getField("LOC_ID").setValue(etIdxTab.getString("LOC_ID"));
                resRow.getField("LOC_GUID").setValue(etIdxTab.getString("LOC_GUID"));
                resRow.getField("LOC_DESCR").setValue(etIdxTab.getString("LOC_DESCR"));
            }
            while (etIdxTab.nextRow());
        }

        return new ResultData(resTab);
    }

    /**
     * DOCUMENT ME!
     *
     * @return DOCUMENT ME!
     */
    public String getTheLanguage() {
        return (theLanguage == null) ? "" : theLanguage;
    }

    /**
     * DOCUMENT ME!
     *
     * @return DOCUMENT ME!
     */
    public String getTheSearchEngine() {
        return (theSearchEngine == null) ? "" : theSearchEngine;
    }
}
