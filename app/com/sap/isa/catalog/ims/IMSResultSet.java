/*****************************************************************************
  Copyright (c) 2001, SAPMarkets Europe GmbH, Germany, All rights reserved.

  $Revision: #3 $
  $Date: 2003/08/22 $
*****************************************************************************/

package com.sap.isa.catalog.ims;

import java.util.Iterator;

/**
 * Interface for the results which were returned from a query to the IMS
 * server. The result set represents a table structure with columns and rows.
 *
 * @version     1.0
 */
public interface IMSResultSet {

  /**
   * Returns <code>true</code> if the result set is empty.
   *
   * @return boolean that indicates if the result set is empty
   */
  public boolean isEmpty();

  /**
   * Returns the number of rows.
   *
   * @return  number of rows
   */
  public int getRowCount();

  /**
   * Returns the number of columns.
   *
   * @return  number of columns
   */
  public int getColumnCount();

  /**
   * Sorts the rows ascending according to the values of the given column.
   * If the column name isn't found the sort order remains unchanged.
   *
   * @param colName  column name to be sorted after
   */
  public void sortRows(String colName);

  /**
   * Returns all column names.
   *
   * @return  array of column names
   */
  public String[] getColumnNames();

  /**
   * Returns the row with the given index.
   *
   * @param row  row index
   * @return     row with the given index, null if not found
   */
  public IMSResultSetRow getRow(int row);

  /**
   * Returns an iterator for the rows.
   *
   * @return  row iterator
   */
  public Iterator iterator();

}