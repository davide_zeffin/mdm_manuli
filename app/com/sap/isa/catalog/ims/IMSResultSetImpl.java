/*****************************************************************************
  Copyright (c) 2001, SAPMarkets Europe GmbH, Germany, All rights reserved.
  Created:  12 February 2001

  $Revision: #2 $
  $Date: 2003/04/30 $
*****************************************************************************/

package com.sap.isa.catalog.ims;

// catalog imports
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;

import com.sap.isa.catalog.ICatalogMessagesKeys;
import com.sap.isa.core.logging.IsaLocation;
import com.sap.mw.jco.JCO;

/**
 * This class encapsulate the query results of the IMS calls.
 * @version     1.0
 */
class IMSResultSetImpl implements IMSResultSet {

  private static IsaLocation theStaticLocToLog =
    IsaLocation.getInstance(IMSResultSetImpl.class.getName());

  private ArrayList theColumnNames;
  private ArrayList theRows;
  private String    theEncoding;

  /**
   * Private constructor for creating a new result set.
   *
   * @param headers  name of all columns
   * @param size     number of rows which have to be stored in the result set
   * @param encoding the encoding of the values
   */
  private IMSResultSetImpl(ArrayList headers, int size, String encoding) {
    // set the encoding
    theEncoding = encoding;
    // set header names
    theColumnNames = (ArrayList)headers.clone();
    // initialize the rows
    theRows = new ArrayList();
    for (int i=0; i < size; i++) theRows.add(null);
  }

  /**
   * Constructor for creating a new result set. This constructor is used
   * for results which are returned from IMSRfcProxy via JCO.
   *
   * @param headers        column names
   * @param resAttributes  result table of the JCO call
   * @param lines          number of rows in the result set
   * @param encoding       encoding of the results from the IMS server
   */
  IMSResultSetImpl(ArrayList headers, JCO.Table resAttributes, int lines,
    String encoding) throws CRMException {

    this(headers, lines, encoding);

    if (resAttributes.getNumRows() > 0) {

      resAttributes.firstRow();

      // iterate over the result of function call
      do {
        int docRef = Integer.parseInt(resAttributes.getString("DOCREFER").trim());

        IMSResultSetRowImpl row = (IMSResultSetRowImpl)theRows.get(docRef);
        if (row == null) {
          row = new IMSResultSetRowImpl(this, headers.size());
          theRows.set(docRef, row);
        }

        try {
          // name and value
          String name  = new String(resAttributes.getByteArray("ATTRNAME"), encoding).trim();
          byte[] value = resAttributes.getByteArray("VALUE1");

          if(theStaticLocToLog.isDebugEnabled() && name.equalsIgnoreCase("TEXT_0001")) {

            String dumpValue = new String(value, encoding);
            if(dumpValue.trim().length() > 0) {
              StringBuffer dump = new StringBuffer();
              for(int i=0; i < value.length; i++) {
                Byte b = new Byte(value[i]);
                dump.append( Integer.toHexString(b.intValue()) );
                if(i+1 != value.length) dump.append( "|" );
              }
              // write debug message
              theStaticLocToLog.debug("TEXT_0001(64 bytes): Hex: " +
                                  dump.toString() +
                                  " UniCode: ");
//                                  WebUtil.toUnicodeEscapeString(dumpValue)) ;
            }

          }

          // result is multi-value
          int multi = Integer.parseInt(resAttributes.getString("MULTATRCNT").trim());

          int col = getColumnIndex(name);
          if (col == -1) {
            theStaticLocToLog.error(ICatalogMessagesKeys.PCAT_IMS_UNKNOWN_COLUMN,
                              new Object[] { name }, null);
          } else
            row.addValue(col, value, multi);

        } catch (UnsupportedEncodingException ex) {
            throw new CRMException(ex.getLocalizedMessage());
        }

      } while (resAttributes.nextRow());

      // write logging message
      this.log();
    }
  }

  int getColumnIndex(String name) {
    int result = theColumnNames.indexOf(name);
    if( result == -1 ) {
        // check for lower case attribute name -->> ignore case
        for(int i=0; i<theColumnNames.size(); i++) {
            if( name.equalsIgnoreCase((String)theColumnNames.get(i)) ) {
                result = i;
                if(theStaticLocToLog.isDebugEnabled())
                    theStaticLocToLog.debug("getColumnIndex(" + name + ") case insensitive access -> " + result);
                break;
            }
        }
    }
    return result;
  }

  String getColumnName(int index) {
    try {
      return (String)theColumnNames.get(index);
    } catch(IndexOutOfBoundsException ex) {
		theStaticLocToLog.debug(ex.getMessage());
      return "";
    }
  }

  public boolean isEmpty(){
    return theRows.isEmpty();
  }

  public int getRowCount() {
    return theRows.size();
  }

  public int getColumnCount() {
    return theColumnNames.size();
  }

  public String[] getColumnNames() {
    return (String [])theColumnNames.toArray(new String[0]);
  }

  public IMSResultSetRow getRow(int row) {
    try {
      return (IMSResultSetRow)theRows.get(row);
    } catch(IndexOutOfBoundsException ex) {
		theStaticLocToLog.debug(ex.getMessage());
      return null;
    }
  }

  public Iterator iterator() {
    return theRows.iterator();
  }

  public void sortRows(String colName) {
    final int col = this.getColumnIndex(colName);

    if (col < 0) return; // name not found

    Collections.sort(theRows,
                     new Comparator() {
                        public int compare(Object op1, Object op2) {
                          String rOperand = ((IMSResultSetRowImpl)op1).getValue(col);
                          String lOperand = ((IMSResultSetRowImpl)op2).getValue(col);

                          return rOperand.compareTo(lOperand);
                        }
                     });
  }

  /**
   * Returns the encoding of the values.
   *
   * @return the encodings of the values
   */
  String getEncoding() {
    return theEncoding;
  }

  /**
   * Writes a log entry if info logging is enabled.
   */
  void log() {
      
    if(!theStaticLocToLog.isDebugEnabled()) {
        return;
    }

    theStaticLocToLog.debug("Query results (Rows: " +
                          Integer.toString(getRowCount()) +
                          ", Cols: " +
                          Integer.toString(getColumnCount()) +
                          ").");

    int rowCount = 0;
    Iterator iter = theRows.iterator();
    while(iter.hasNext()) {
      IMSResultSetRowImpl row = (IMSResultSetRowImpl)iter.next();
      theStaticLocToLog.debug("Line: " + Integer.toString(rowCount++) + ": ");
       
      if (row != null) {
          row.log(); 
      }
      else {  
          theStaticLocToLog.debug("row is null");
      }
    } // while
  } // log()
} // class 

/**
 * This class implements the row of a result set.
 */
class IMSResultSetRowImpl implements IMSResultSetRow {

  private static IsaLocation theStaticLocToLog =
    IsaLocation.getInstance(IMSResultSetRowImpl.class.getName());

  private IMSResultSetImpl theSet;
  private ArrayList theItems;

  /**
   * Constructor for creating an new row.
   *
   * @param set  result set which contains the newly created row
   * @param size number of columns in the row
   */
  IMSResultSetRowImpl(IMSResultSetImpl set, int size) {
    theSet = set;
    theItems = new ArrayList(size);
    for (int i=0; i < size; i++) theItems.add(null);
  }

  public boolean isMultiValue(String colName) {
    return isMultiValue(getColumnIndex(colName));
  }

  public boolean isMultiValue(int col) {

    IMSResultSetItemImpl item = null;

    try {
      item = (IMSResultSetItemImpl)theItems.get(col);
    } catch(IndexOutOfBoundsException ex) {
		theStaticLocToLog.debug(ex.getMessage());
      item = null;
    }

    return (item == null)?false:item.isMultiValue();
  }

  public String[] getValues(String colName) {
    return getValues(getColumnIndex(colName));
  }

  public String[] getValues(int col) {

    IMSResultSetItemImpl item = null;

    try {
      item = (IMSResultSetItemImpl)theItems.get(col);
    } catch(IndexOutOfBoundsException ex) {
		theStaticLocToLog.debug(ex.getMessage());
      item = null;
    }

    return (item == null)?IMSResultSetRow.EMPTY_ARRAY:item.getValues();
  }

  public String getValue(String colName) {
    return getValue(getColumnIndex(colName));
  }

  public String getValue(int col) {

    IMSResultSetItemImpl item = null;

    try {
      item = (IMSResultSetItemImpl)theItems.get(col);
    } catch(IndexOutOfBoundsException ex) {
		theStaticLocToLog.debug(ex.getMessage());
      item = null;
    }

    return (item == null)?"":item.getValue();
  }

  public Iterator iterator() {
    return theItems.iterator();
  }

  int getColumnIndex(String name) {
    return theSet.getColumnIndex(name);
  }

  String getColumnName(int index) {
    return theSet.getColumnName(index);
  }

  void addValue(int col, byte[] value, int multiCount) {
    IMSResultSetItemImpl item = (IMSResultSetItemImpl)theItems.get(col);
    if (item == null)
      theItems.set(col, new IMSResultSetItemImpl(this, col, value, multiCount));
    else
      item.addValue(value, multiCount);
  }

  /**
   * Returns the encoding of the values.
   *
   * @return the encodings of the values
   */
  String getEncoding() {
    return theSet.getEncoding();
  }

  /**
   * Writes a log entry if info logging is enabled.
   */
  void log() {
    if(theStaticLocToLog.isDebugEnabled()) {

      Iterator iter = theItems.iterator();
      while(iter.hasNext()) {
        IMSResultSetItemImpl item = (IMSResultSetItemImpl)iter.next();
        if(item != null) {
            theStaticLocToLog.debug("Column: " +
                              item.getColumnName() +
                              ", Value: " +
                              item.getValue() +
                              ".");
        }
      }
    }
  }

}

/**
 * This class implements an item in a result set row.
 */
class IMSResultSetItemImpl implements IMSResultSetItem {

  private static IsaLocation theStaticLocToLog =
    IsaLocation.getInstance(IMSResultSetItemImpl.class.getName());

  private ArrayList           theValues; // filled if multi value
  private byte[]              theValue;  // normal value
  private IMSResultSetRowImpl theRow;
  private int                 theColumnIndex;

  /**
   * Constructor for creating a new item.
   *
   * @param row         row which contains the newly created item
   * @param col         index of the column
   * @param value       value of the item
   * @param multiCount  counter for multi values
   */
  IMSResultSetItemImpl(IMSResultSetRowImpl row, int col,
    byte[] value, int multiCount) {

    theRow = row;
    theColumnIndex = col;
    if (multiCount==0) {
      theValue  = value;
      theValues = null;
      }
    else {
      theValue = null;
      theValues = new ArrayList();
      for(int i=0; i <= multiCount; i++) theValues.add(null);
      theValues.set(multiCount, value);
    }
  }

  public String getColumnName() {
    return theRow.getColumnName(theColumnIndex);
  }

  public boolean isMultiValue() {
    return theValues==null?false:true;
  }

  public String[] getValues() {

    String[] result = new String[0];

    try {
      String encoding = theRow.getEncoding();
      if (isMultiValue()) {
        result = new String[theValues.size()];
        for(int i=0; i < result.length; i++)
          result[i] = new String((byte[])theValues.get(i), encoding).trim();
        }
      else
        result = new String[] { new String(theValue, encoding).trim() };
    } catch (UnsupportedEncodingException ex) {
		theStaticLocToLog.debug(ex.getMessage());
      // can't appear since the encoding is already used in the constructor;
    }

    return result;
  }

  public String getValue() {

    String result = "";

    try {
      String encoding = theRow.getEncoding();
      if (isMultiValue()) {
        StringBuffer sb = new StringBuffer();
        // concatenate the multi values
        for (int i=0; i < theValues.size(); i++) {
          byte[] bytes = (byte[])theValues.get(i);
          if(bytes == null) {
            String msg = "getValue(" + i + "/" + theValues.size()
                        + ") is accessing a null value for col #"
                        + theColumnIndex + ", colName = "
                        + theRow.getColumnName(theColumnIndex);
            theStaticLocToLog.error("pcat.exception", new Object[]{msg}, null);
          }
          String s0 = new String(bytes, encoding).trim();
          if (s0 != null) {
            if (i > 0) sb.append(SEPARATOR);
            sb.append(s0);
          }
        }
      result = sb.toString();
      }
      else
        result = new String(theValue, encoding).trim();
    } catch (UnsupportedEncodingException ex) {
		theStaticLocToLog.debug(ex.getMessage());
      // can't appear since the encoding is already used in the constructor;
    }

    return result;
  }

  void addValue(byte[] value, int multiCount) {
    if (isMultiValue()){
      for(int j=theValues.size(); j <= multiCount; j++) theValues.add(null);
      byte[] values = (byte[])theValues.get(multiCount);
      theValues.set(multiCount, values==null?value:buildValue(values, value));
    }
    else if(multiCount == 0)
      theValue = buildValue(theValue, value);
    else {
      theValues = new ArrayList();
      theValues.add(theValue);
      theValue = null;
      for(int i=1; i <= multiCount; i++) theValues.add(null);
      theValues.set(multiCount, value);
    }
  }

  private byte[] buildValue(byte[] values, byte[] newValue) {

    byte[] result = new byte[values.length + newValue.length];
    System.arraycopy(values,   0, result, 0, values.length);
    System.arraycopy(newValue, 0, result, values.length, newValue.length);

    return result;
  }
}
