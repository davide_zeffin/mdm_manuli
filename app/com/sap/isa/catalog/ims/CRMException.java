/*****************************************************************************
  Copyright (c) 2001, SAPMarkets Europe GmbH, Germany, All rights reserved.

  $Revision: #1 $
  $Date: 2002/02/18 $
*****************************************************************************/
package com.sap.isa.catalog.ims;

import com.sap.isa.catalog.boi.CatalogException;

/**
 * Exception class for all exceptions which occur during calling or working
 * with the CRM catalog implementation.
 *
 * @version     1.0
 */
public class CRMException extends CatalogException
{
/**
   * Default constructor for exceptions.
   *
   * @param  msg  text to be associated with the exception
   */
    public CRMException(String msg)
    {
          super(msg);
      }

        /**
         * Creates a new <code>CRMException</code> instance
         * in case anotherException is the root cause .
         * 
         * @param msg a <code>String</code> value
         * @param aCause a <code>Throwable</code> value
         */
    public CRMException(String msg, Throwable aCause)
    {
        super(msg, aCause);
    }
}
