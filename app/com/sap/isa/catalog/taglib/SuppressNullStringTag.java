package com.sap.isa.catalog.taglib;

import java.io.IOException;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.tagext.TagSupport;

/**
 * File: SuppressNullStringTag.java
 * To change this generated comment edit the template variable "typecomment":
 * Window>Preferences>Java>Templates.
 * To enable and disable the creation of type comments go to
 * Window>Preferences>Java>Code Generation.
 */
public class SuppressNullStringTag extends TagSupport
{

  private String theString = null;
  
  public void setString(String theString)
  {
    this.theString = theString;
  }
  
  public String getString()
  {
    return theString;
  }
  
  /**
   * Constructor for SuppressNullStringTag.
   */
  public SuppressNullStringTag()
  {
    super();
  }

  /**
   * @see javax.servlet.jsp.tagext.Tag#doStartTag()
   */
  public int doStartTag() throws JspException
  {
    try
    {
      if (theString != null)
      {
        pageContext.getOut().print(theString);
      }
    }
    catch (IOException ex)
    {
      throw new JspException(ex.getMessage());
    }
    return SKIP_BODY;
  }

  /**
   * @see javax.servlet.jsp.tagext.Tag#release()
   */
  public void release()
  {
    theString = null;
    super.release();
  }

}
