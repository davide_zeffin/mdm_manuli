/*****************************************************************************
    Class:        IfImageAvailableBaseTag
    Copyright (c) 2006, SAP AG, Germany, All rights reserved.
    Author:       SAP
    Created:      May 2006
    Version:      1.0

    $Revision: #1 $
    $Date: 2006/05/31 $
*****************************************************************************/
package com.sap.isa.catalog.taglib;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.MissingResourceException;
import java.util.StringTokenizer;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.tagext.BodyTagSupport;

import com.sap.isa.backend.boi.isacore.ProductData;
import com.sap.isa.catalog.webcatalog.WebCatArea;
import com.sap.isa.catalog.webcatalog.WebCatItem;
import com.sap.isa.core.UserSessionData;
import com.sap.isa.core.logging.IsaLocation;

/**
 * The implementation checks if an image of a WebCatItem, WebCatArea or Product is available.<br />
 * If yes, the body included in this tag will be displayed.<br /><br />
 * At first you need to provide an object of a WebCatItem, WebCatArea or Product which should
 * be used to check if an image is available. You can store this object in context of:
 * <ul>
 *   <li>PageContext</li>
 *   <li>UserSessionData</li>
 * </ul>
 * Thereafter you need only to provide the name of the attribute of this object.<br />
 * <br />
 * Additionally you can provide a list of Guids which should be used for checking of the image.
 * This attribute is optional and if you don't provide any guids, the following values will be 
 * used as default:
 * <ul>
 *   <li>DOC_PC_CRM_THUMB</li>
 *   <li>DOC_P_CRM_THUMB</li>
 *   <li>DOC_P_BDS_IMAGE</li>
 * </ul>
 * With the optional parameter <code>negation=&quot;yes&quot;</code> you can invert the functionality 
 * of this tag, i.e. you can check if an image is NOT available.<br /><br />
 * 
 * <b>Note:</b><br />
 * Normally, this implementation will not be used directly for a tag definition. It will be used 
 * mainly as basis for classes like ... 
 *
 * @author D038380
 * @version 1.0
 *
 */
public class IfImageAvailableBaseTag extends BodyTagSupport {


    protected static final IsaLocation log =
                 IsaLocation.getInstance(IfImageAvailableBaseTag.class.getName());

    /**
     * Default guids.
     */
    protected static String defaultGuids = "DOC_PC_CRM_THUMB,DOC_P_CRM_THUMB,DOC_P_BDS_IMAGE";
    
	/**
	 * Constant for the DOC_PC_CRM_THUMB attribute.
	 */
	protected final static String ATTR_DOC_PC_CRM_THUMB = "DOC_PC_CRM_THUMB";
    
    /**
     * This list stores the attribute names which are parsed from the input guids
     */
    private ArrayList idList;
    /**
     * String representation of the attribute names.
     */
    private String guids;
    /**
     * The object name to be retrieved.
     */
    private String name = null;
    /**
     * The attribute negation.
     */
    private String negation = null;
    
    /**
     * Create a new instance of the tag handler.
     */
    public IfImageAvailableBaseTag() {
    }

    /**
     * Set the property name
     * @param name
     */
    public void setName(String name) {
      this.name = name;
    }

    /**
     * Set the property guids
     * @param guids
     */
    public void setGuids(String guids) {
        this.guids = guids;
    }
    
    /**
     * Set the property negation.
     */
    public void setNegation(String negation) {
    	this.negation = negation;
    }

    /**
     * Process the start tag.
     *
     * @exception JspException if a JSP exception has occurred
     */
    public int doStartTag() throws JspException {

    	if(guids == null) {
    		guids = defaultGuids;
    	}
    	processGuids(guids);

        try {
            // do the translation
            Object obj = pageContext.findAttribute(name);
            if (obj == null) {
                UserSessionData userSessionData =
                        UserSessionData.getUserSessionData(pageContext.getSession());

                obj = userSessionData.getAttribute(name);
            }

            if (obj == null) {
	            return (SKIP_BODY);
            }
            
            boolean imageFound = false;
            
            if (obj instanceof ProductData) {
                ProductData product = (ProductData) obj;
                WebCatItem item = (WebCatItem) product.getCatalogItemAsObject();
                if(item != null) {
                    Iterator iter = idList.iterator();
                    while (iter.hasNext()) {
                        String st = (String) iter.next();
                        if (item.getAttribute(st) != null && !item.getAttribute(st).equals("")) {
                            
                        	// attribute contains image information
                            imageFound = true;                       	
                        	if(negation == null || !negation.equalsIgnoreCase("YES")) {
                        		return EVAL_BODY_TAG;
                            }
                        }
                    }
                    if(negation != null && negation.equalsIgnoreCase("YES")) {
                		return (imageFound) ? SKIP_BODY : EVAL_BODY_TAG;
                    }
                }
            }        
            else if (obj instanceof WebCatItem) {
                WebCatItem item = (WebCatItem) obj;
                Iterator iter = idList.iterator();
                while (iter.hasNext()) {
                    String st = (String) iter.next();
                    if (item.getAttribute(st) != null && !item.getAttribute(st).equals("")) {
                        
                    	// attribute contains image information
                    	imageFound = true;
                        if(negation == null || !negation.equalsIgnoreCase("YES")) {
                        	return EVAL_BODY_TAG;
                        }
                    }
                }
                if(negation != null && negation.equalsIgnoreCase("YES")) {
            		return (imageFound) ? SKIP_BODY : EVAL_BODY_TAG;
                }
            }
            // check if we are working on an area
            else if (obj instanceof WebCatArea) {
            	
                WebCatArea area = (WebCatArea) obj;
                
                Iterator iter = idList.iterator();
                while (iter.hasNext()) {
                    String st = (String) iter.next();
                    // special treatment for thumbs in area. This attribute is not contained in the attrubute/details list
                    if (ATTR_DOC_PC_CRM_THUMB.equals(st) && area.getCategory() != null && 
                        area.getCategory().getThumbNail() != null && area.getCategory().getThumbNail().length() > 0) {
						imageFound = true;
						if(negation == null || !negation.equalsIgnoreCase("YES")) {
						    return EVAL_BODY_TAG;
						}
                    }
                    else if (area.getDetail(st) != null && !area.getDetail(st).equals("")) {
                        
                    	// attribute contains image information
                    	imageFound = true;
                    	if(negation == null || !negation.equalsIgnoreCase("YES")) {
                    	    return EVAL_BODY_TAG;
                    	}
                    }
                }
                if(negation != null && negation.equalsIgnoreCase("YES")) {
            		return (imageFound) ? SKIP_BODY : EVAL_BODY_TAG;
                }
            }
            
            if(negation != null && negation.equalsIgnoreCase("YES")) {
            	return EVAL_BODY_TAG;
            }
            else {
                return (SKIP_BODY);
            }
        } 
        catch (MissingResourceException ex) {
  	        throw new JspException("system.noResource");
    	}
    	// Continue processing this page
    }

    /**
     *
     * Nothing to do here.
     * @exception JspException if a JSP exception has occurred
     */
    public int doAfterBody() throws JspException {

        return SKIP_BODY;
    }

    /**
     * Clean up after processing this enumeration.
     *
     * @exception JspException if a JSP exception has occurred
     */
    public int doEndTag() throws JspException {

        // Render any previously accumulated body content
        if (bodyContent != null) {
            try {
                JspWriter out = getPreviousOut();
                out.print(bodyContent.getString());
            } catch (IOException e) {
                throw new JspException();
            }
        }
        // Continue processing this page
        return EVAL_PAGE;
    }
   
    /**
     * Drop the state of this tag handler. This method is called by the
     * container
     * if reuse of tag handlers is implemented.
     */
    public void release() {
        super.release();
        idList = null;
        name = null;
        guids = null;
        negation = null;
    }

    /**
     * Maps the given String representation of the guids to an internal ArrayList.  
     * @param guids
     */
    protected void processGuids(String guids) {
        idList = new ArrayList();
        if (guids != null) {
            StringTokenizer str = new StringTokenizer(guids,",",false);
            while (str.hasMoreTokens()) {
                String st = new String(str.nextToken());
                idList.add(st);
            }
        }
    }
}
