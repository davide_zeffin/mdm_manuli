
/*****************************************************************************
  Copyright (c) 2000, SAPMarkets Europe GmbH, Germany, All rights reserved.
  Author:       SAPMarkets
  Created:      07 May 2001

  $Revision: #2 $
  $Date: 2001/07/25 $
*****************************************************************************/

package com.sap.isa.catalog.taglib;


import java.io.IOException;
import java.util.MissingResourceException;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.tagext.TagSupport;

import org.apache.struts.util.MessageResources;

import com.sap.isa.catalog.boi.IAttribute;
import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.core.util.WebUtil;

/**
 * Custom tag that retrieves an internationalized messages string
 * (with optional parametric replacement) from the
 * <code>ActionResources</code> object stored as a context attribute
 * by our associated <code>ActionServlet</code> implementation.
 * If no translation is obtained, it returns the Name of that Attribute.
 */

public class AttributeNameTag extends TagSupport {


    protected static final IsaLocation log =
                 IsaLocation.getInstance(AttributeNameTag.class.getName());


    /**
     * Construct a new instance of this tag.
     */
    public AttributeNameTag() {
	super();
    }


    /**
     * The optional argument.
     */
    private String arg;


    /**
     * The message key of the message to be retrieved.
     */
    private String key = null;




    /**
     * Return the args.
     */
    public String getArg() {
    	  return arg;
    }


    /**
     * Set the first optional argument.
     *
     * @param arg0 The new optional argument
     */
    public void setArg(String arg) {
      this.arg=arg;
    }



    /**
     * Return the message key.
     */
    public String getKey() {

	return key;
    }


    /**
     * Set the message key.
     *
     * @param key The new message key
     */
    public void setKey(String key) {

	this.key = key;
    }


    /**
     * Release any acquired resources.
     */
    public void release() {

	super.release();

	arg = null;
	key = null;
    }




    // ------------------------------------------------------ Public Methods


    /**
     * Process the start tag.
     *
     * @exception JspException if a JSP exception has occurred
     */
    public int doStartTag() throws JspException {

	try {
            // do the translation
        Object obj = pageContext.findAttribute(key);
        if (obj == null) {
	        pageContext.getOut().print("Not foundvhvvhvh");
	        return (SKIP_BODY);
        }
        IAttribute attrib = (IAttribute) obj;
	    String text = WebUtil.translate(pageContext, arg+"."+attrib.getGuid(), null);
        if (text == null || text.equals("null"))
            text=attrib.getName();

	    // Print the retrieved message to our output writer
	    JspWriter writer = pageContext.getOut();
	    writer.print(text);

	} catch (MissingResourceException ex) {

	    throw new JspException("system.noResource");

	} catch (IOException ex) {

	    log.error("system.io", ex);

            MessageResources resources =
                        WebUtil.getResources(pageContext.getServletContext());
	    throw new JspException
		(resources.getMessage("system.io", ex.toString()));
	}

	// Continue processing this page
	return (SKIP_BODY);

    }


}
