/*****************************************************************************
    Class:        IfThumbAvailableTag
    Copyright (c) 2006, SAP AG, Germany, All rights reserved.
    Author:       SAP
    Created:      May 2006
    Version:      1.0

    $Revision: #1 $
    $Date: 2006/05/31 $
*****************************************************************************/
package com.sap.isa.catalog.taglib;


/**
 * This tag checks if a thumb of a WebCatItem, WebCatArea or Product is available.<br />
 * If yes, the body included in this tag will be displayed.<br /><br />
 * At first you need to provide an object of a WebCatItem, WebCatArea or Product which should
 * be used to check if a thumb is available. You can store this object in context of:
 * <ul>
 *   <li>PageContext</li>
 *   <li>UserSessionData</li>
 * </ul>
 * Thereafter you need only to provide the name of the attribute of this object.<br />
 * <br />
 * 
 * As default the implementation works with the following guids:
 * <ul>
 *   <li>DOC_PC_CRM_THUMB</li>
 *   <li>DOC_P_CRM_THUMB</li>
 *   <li>DOC_P_BDS_IMAGE</li>
 * </ul>
 * 
 * If other guids should be used for image determination, this can be provided by the optional 
 * attribute <code>guids</code>.
 * 
 * <h5>Example 1 (usage of default guids)</h5>
 * <pre>
 *   &lt;isa:iterate id=&quot;item&quot; name=&quot;itemPage&quot; type=&quot;com.sap.isa.catalog.webcatalog.WebCatItem&quot;&gt;
 *       &lt;isa:ifThumbAvailable name=&quot;item&quot;&gt;
 *           --- THUMB AVAILABLE ---
 *       &lt;/isa:ifThumbAvailable&gt;
 *   &lt;/isa:iterate&gt;
 * </pre>
 * 
 * <h5>Example 2 (usage of additional custom guids)</h5>
 * <pre>
 *   &lt;% pageContext.setAttribute(&quot;item&quot;, currentItem); %&gt;
 *   &lt;isa:ifThumbAvailable name=&quot;item&quot; guids=&quot;Z_MY_IMAGE_GUID,DOC_PC_CRM_THUMB,DOC_P_CRM_THUMB,DOC_P_BDS_IMAGE&quot;&gt;
 *       --- THUMB AVAILABLE ---
 *   &lt;/isa:ifThumbAvailable&gt;
 * </pre>
 * The implementation base on the functionality of {@link IfImageAvailableBaseTag}.
 * For more details please check the JavaDoc of this class.
 *
 * @author D038380
 * @version 1.0
 *
 */
public class IfThumbAvailableTag extends IfImageAvailableBaseTag {
    
    /**
     * Create a new instance of the tag handler.
     */
    public IfThumbAvailableTag() {
    	super();
    	setGuids("DOC_PC_CRM_THUMB,DOC_P_CRM_THUMB,DOC_P_BDS_IMAGE");
    }
}
