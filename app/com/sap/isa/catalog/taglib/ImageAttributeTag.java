
/*****************************************************************************
  Copyright (c) 2000, SAPMarkets Europe GmbH, Germany, All rights reserved.
  Author:       SAPMarkets
  Created:      14 September 2001

  $Revision: #1 $
  $Date: 2001/09/14 $
*****************************************************************************/

package com.sap.isa.catalog.taglib;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.MissingResourceException;
import java.util.StringTokenizer;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.jsp.JspException;
import javax.servlet.jsp.tagext.TagSupport;

import com.sap.isa.backend.boi.isacore.ProductData;
import com.sap.isa.catalog.webcatalog.WebCatArea;
import com.sap.isa.catalog.webcatalog.WebCatInfo;
import com.sap.isa.catalog.webcatalog.WebCatItem;
import com.sap.isa.core.UserSessionData;
import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.core.util.WebUtil;

/**
 * Custom tag that retrieves an internationalized messages string
 * (with optional parametric replacement) from the
 * <code>ActionResources</code> object stored as a context attribute
 * by our associated <code>ActionServlet</code> implementation.
 * If no translation is obtained, it returns the Name of that Attribute.
 */

public class ImageAttributeTag extends TagSupport {


    protected static final IsaLocation log =
                 IsaLocation.getInstance(ImageAttributeTag.class.getName());

	/**
	 * Constant for the DOC_PC_CRM_THUMB attribute.
	 */
	protected final static String ATTR_DOC_PC_CRM_THUMB = "DOC_PC_CRM_THUMB";

    /**
     * This list stores the attribute names which are parsed from the input guids
     */
    private ArrayList idList;
    /**
     * The URL for the default image
     */
    private String defaultImg;
    /**
     * The object name to be retrieved.
     */
    private String name = null;
    /**
     * The language if necessary
     */
    private String language;

    /**
     * image width if provided
     */
    private String width = null;
    /**
     * image height if provided
     */
    private String height = null;
    
    /**
     * Construct a new instance of this tag.
     */
    public ImageAttributeTag() 
    {
      super();
    }
    
    public void setGuids(String guids) 
    {
      processGuids(guids);
    }

    private void processGuids(String guids) 
    {
      idList = new ArrayList();
      if (guids != null)
      {
        StringTokenizer str = new StringTokenizer(guids,",",false);
        while (str.hasMoreTokens()) 
        {
          String st = new String(str.nextToken());
          idList.add(st);
        }
      }
    }
  
    public void setDefaultImg(String defaultImg) 
    {
      this.defaultImg = defaultImg;
    }

    public void setName(String name) 
    {
      this.name = name;
    }

    public void setLanguage(String language) 
    {
      this.language = language;
    }

    public void setWidth(String width)
    {
      this.width = width;
    }
    
    public void setHeight(String height)
    {
      this.height = height;
    }
    
    /**
     * Release any acquired resources.
     */
    public void release() 
    {
    	super.release();
      idList = null;
    	defaultImg = null;
      name = null;
      width = null;
      height = null;
    }


    /**
     * Process the start tag.
     *
     * @exception JspException if a JSP exception has occurred
     */
    public int doStartTag() throws JspException 
    {
    	try {
        // do the translation
        Object obj = pageContext.findAttribute(name);
        if (obj == null) {
          UserSessionData userSessionData =
                  UserSessionData.getUserSessionData(pageContext.getSession());

          obj = userSessionData.getAttribute(name);
        }

        if (obj == null) {
	        pageContext.getOut().print("Not found");
	        return (SKIP_BODY);
        }
        
        String imageServer = null;
        
        if (obj instanceof ProductData) {
            ProductData product = (ProductData) obj;
            WebCatItem item = (WebCatItem) product.getCatalogItemAsObject();
            if(item != null) {
                imageServer = determineImageServer(null, item);
                Iterator iter = idList.iterator();
                while (iter.hasNext()) {
                    String st = (String) iter.next();
                    if (item.getAttribute(st) != null && !item.getAttribute(st).equals("")) {
                        // attribute contains image information
                        // check for absolute URL and write to output
                        pageContext.getOut().print(
                              getImageOutput(imageServer, item.getAttribute(st)));
                        // we only want to write the first non-empty image attribute, so stop here
                        return (SKIP_BODY);
                    }
                }
            }
        }   
        // check if we are working on an item
        else if (obj instanceof WebCatItem) {
          WebCatItem item = (WebCatItem) obj;
          imageServer = determineImageServer(null, item);
          Iterator iter = idList.iterator();
          while (iter.hasNext()) {
            String st = (String) iter.next();
            if (item.getAttribute(st) != null && !item.getAttribute(st).equals("")) {
              // attribute contains image information
              // check for absolute URL and write to output
              pageContext.getOut().print(
                    getImageOutput(imageServer, item.getAttribute(st)));
              // we only want to write the first non-empty image attribute, so stop here
              return (SKIP_BODY);
            }
          }
        }
        // check if we are working on an area
        else if (obj instanceof WebCatArea) {
          WebCatArea area = null;
          area = (WebCatArea) obj;
          imageServer = determineImageServer(area, null);
          Iterator iter = idList.iterator();
          while (iter.hasNext()) {
            String st = (String) iter.next();
			// special treatment for thumbs in area. This attribute is not contained in the attrubute/details list
			if (ATTR_DOC_PC_CRM_THUMB.equals(st) && area.getCategory() != null && 
			    area.getCategory().getThumbNail() != null && area.getCategory().getThumbNail().length() > 0) {
			    pageContext.getOut().print(
						getImageOutput(imageServer, area.getCategory().getThumbNail()));
				  // we only want to write the first non-empty image attribute, so stop here
				return (SKIP_BODY);
			}
            else if (area.getDetail(st) != null && !area.getDetail(st).equals("") && !area.getDetail(st).equals("null")) {
              // attribute contains image information
              // check for absolute URL and write to output
              pageContext.getOut().print(
                    getImageOutput(imageServer, area.getDetail(st)));
              // we only want to write the first non-empty image attribute, so stop here
              return (SKIP_BODY);
            }
          }
        }
        // if no image provided, check whether the default image is language dependent
        if(defaultImg != null) {
            if (language != null)
              if (language.equalsIgnoreCase("true")) 
                language = UserSessionData.getUserSessionData(pageContext.getSession()).getLocale().getLanguage();
              else 
                language = null;
            // get default image and write
            String s = WebUtil.getMimeURL(pageContext, null, language, defaultImg);
          	pageContext.getOut().print(s);
        }
	    return (SKIP_BODY);
	  } 
      catch (MissingResourceException ex) {
  	    throw new JspException("system.noResource");
      } 
      catch (IOException ex) {
  	    log.error("system.io", ex);
  	    throw new JspException("ImageAttributeTag.IOException");
      }
    	// Continue processing this page
    }

    /**
     * Determine the imageserver depending, depeending on the fact 
     * if https is used as protocol or not.
     */
    private String determineImageServer(WebCatArea area, WebCatItem item) {
        
        String imageServer = null;
        
        boolean isSecure = WebUtil.isSecure((HttpServletRequest) pageContext.getRequest());
        WebCatInfo catalog = null;
        
        if (area != null) {
                catalog = area.getCatalog();
        }
        else if (item != null) {
            catalog = item.getItemKey().getParentCatalog();
        }
        
        if (catalog != null) {
            if (isSecure) {
                imageServer = catalog.getHttpsImageServer();
                
                if (imageServer == null || imageServer.length() == 0) {
                    log.debug("HttpsImageServer is not set, take imageserver instead");
                    imageServer = catalog.getImageServer();
                }
            }
            else {
                imageServer = catalog.getImageServer();
            }
        }
        
        return imageServer;
    }
    

    /**
     * Provides the enriched image data, i.e. adds the image server if the url
     * is not an absolute url, and adds width and height attributes.
     * @param imgServer the image server address to be added to the url
     * @param url the url as it comes from the attribute
     * @return enriched image data
     */
    private String getImageOutput(String imgServer, String url)
    {
      // check whether URL starts with something like "http(s)://"
      if (url.startsWith("http") && url.indexOf("://") != -1 ) 
      {
        // if so, the URL is already absolute, so nothing to do.
      } 
      else 
      {
        // URL is relative, so add the image server parameter to it.
        // For R/3 scenario a file separator is not necessarily part of the url
        if(url.startsWith("/"))
        	url = imgServer + url;
        else
			url = imgServer + "/" + url;
      }
      // check for width and height. note the strange handling of quotes here!
      if (width != null)
        url = url + "\" width=\"" + width;
      if (height != null)
        url = url + "\" height=\"" + height;
      return url;
    }
}
