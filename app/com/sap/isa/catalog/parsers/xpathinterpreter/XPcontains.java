package com.sap.isa.catalog.parsers.xpathinterpreter;

import java.util.ArrayList;

/**
 * Title:        WebXSLT
 * Description:
 * Copyright:    Copyright (c) 2001
 * Company:
 * @version 1.0
 */

public class XPcontains extends XPathFunction
{
	XPstring theStringFunction;
    public XPcontains(XPathContext aContext)
	{
		super(aContext);
		theStringFunction = new XPstring(aContext);
    }

    public IXPvalue call(ArrayList theArguments)
	{
		//runtime check of the arguments
		if(theArguments.size()!=2)
			throw new RuntimeException("Wrong number of arguments for contains");

		IXPvalue value      = (IXPvalue)theArguments.get(0);
		IXPvalue subString  = (IXPvalue)theArguments.get(1);

		//convert to string
		if( !(value instanceof IXPstringValue) )
		{
			ArrayList arguments = new ArrayList();
			arguments.add(value);
			value = theStringFunction.call(arguments);
		}

		//convert to string
		if(	!(subString instanceof IXPstringValue) )
		{
			ArrayList arguments = new ArrayList();
			arguments.add(subString);
			subString = theStringFunction.call(arguments);
		}

		return this.call((IXPstringValue)value, (IXPstringValue)subString);
    }

    public IXPbooleanValue call(IXPstringValue aValue,
								IXPstringValue aSubStringValue)
	{

		String strValue = ((IXPstringValue)aValue).getValue();
		String subStrValue = ((IXPstringValue)aSubStringValue).getValue();

		boolean result = false;
		if(subStrValue.length()==0)
		    return new XPbooleanValue(new Boolean(true));

		if(strValue.length()==0)
			return new XPbooleanValue(new Boolean(false));

		result = strValue.indexOf(subStrValue) != -1;

		return new XPbooleanValue(new Boolean(result));
    }
}
