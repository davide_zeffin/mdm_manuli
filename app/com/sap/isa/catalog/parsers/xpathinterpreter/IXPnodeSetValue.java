package com.sap.isa.catalog.parsers.xpathinterpreter;

/**
 * Title:        WebXSLT
 * Description:
 * Copyright:    Copyright (c) 2001
 * Company:
 * @version 1.0
 */

import java.util.ArrayList;

public interface IXPnodeSetValue extends IXPvalue
{
	final public static int ID = 1;

	ArrayList getValue();

	void union(IXPnodeSetValue aNodeSet);
}
