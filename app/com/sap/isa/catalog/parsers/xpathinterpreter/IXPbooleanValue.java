package com.sap.isa.catalog.parsers.xpathinterpreter;

/**
 * Title:        WebXSLT
 * Description:
 * Copyright:    Copyright (c) 2001
 * Company:
 * @version 1.0
 */

public interface IXPbooleanValue extends IXPvalue
{
	public static final int ID = 0;

	public Boolean getValue();
}
