package com.sap.isa.catalog.parsers.xpathinterpreter;

import java.util.ArrayList;

/**
 * Title:        WebXSLT
 * Description:
 * Copyright:    Copyright (c) 2001
 * Company:
 * @version 1.0
 */

public class XPround extends XPathFunction
{
  XPnumber theNumberFunction;

  public XPround(XPathContext aContext)
  {
    super(aContext);
    theNumberFunction = new XPnumber(aContext);
  }

  public IXPvalue call(ArrayList theArguments)
  {
    if(theArguments.size()!=1)
      throw new RuntimeException("round() called with wrong number of arguments!");
    IXPvalue anArgument = (IXPvalue)theArguments.get(0);
    return call(anArgument);
  }

  public IXPnumberValue call(IXPvalue anArgument)
  {
    IXPnumberValue aNumberValue = null;
    if(anArgument.getTypeId()!=IXPnumberValue.ID)
      aNumberValue = theNumberFunction.call(anArgument);
    else
      aNumberValue = (IXPnumberValue)anArgument;
    return call(aNumberValue);   
  }

  public IXPnumberValue call(IXPnumberValue anNumberArgument)
  {
    if(anNumberArgument.getValue() instanceof Integer)
      return anNumberArgument;
    else
      {
	Double aValue = (Double) anNumberArgument.getValue();
	double aRoundedDouble = 0d;
	if(Double.isNaN(aValue.doubleValue()))
	  aRoundedDouble =aValue.doubleValue();
	else if(aValue.doubleValue()==Double.POSITIVE_INFINITY)
	  aRoundedDouble =aValue.doubleValue();
	else if(aValue.doubleValue()==Double.NEGATIVE_INFINITY )
	  aRoundedDouble =aValue.doubleValue();
	else
	  aRoundedDouble = Math.round(aValue.doubleValue());
	return new XPnumberValue(new Double(aRoundedDouble));
      }
  }
}
