package com.sap.isa.catalog.parsers.xpathinterpreter;

/**
 * Title:        WebXSLT
 * Description:
 * Copyright:    Copyright (c) 2001
 * Company:
 * @version 1.0
 */

import java.util.ArrayList;
import java.util.Iterator;

import org.w3c.dom.Node;

public class XPnodeSetValue implements IXPnodeSetValue
{
	private ArrayList theNodeSet;

	public XPnodeSetValue(ArrayList theNodeSet)
	{
		this.theNodeSet=theNodeSet;
	}

	public int getTypeId()
	{
		return IXPnodeSetValue.ID;
	}

	public ArrayList getValue()
	{
		return theNodeSet;
	}

	public void union(IXPnodeSetValue aNodeSetToUnion)
	{
		ArrayList theNewNodes = aNodeSetToUnion.getValue();
		Iterator theNewNodesIterator = theNewNodes.iterator();
		while(theNewNodesIterator.hasNext())
		{
			Node aNewNode = (Node)theNewNodesIterator.next();
			if(!theNodeSet.contains(aNewNode))
				this.theNodeSet.add(aNewNode);
		}
	}
}

