/*
  Commandline driver for XPath parser.
  Author: Ingo Macherius <macherius@gmd.de>
  (c) 1999 GMD
  No warranties, use at your own risk.
*/
package com.sap.isa.catalog.parsers.xpathinterpreter;
import java.io.IOException;
import java.io.Reader;
import java.io.StringReader;
import java.util.ArrayList;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.xml.sax.SAXException;

public class Driver
{
  final static byte XSLT  = 1;
  final static byte XPATH = 2;

  final static byte USE = XPATH;	// Specify what to test for, XPath or XSLT
  final static boolean DEBUG = false;	// Specify for verbose output

  /**
   * TestCases:
   * 1)   '/' select the root
   * 2)   '//section' select any section elements
   * 3)   '//chapter/section'
   * 4) 	'//chapter/section/@title'
   * 5) 	'//chapter/section/@title/..'
   */

  public static void main( String[] args )
  {
    if (args[0] == null)
      {
	if (USE == XSLT)
	  System.err.println( "Usage: java Driver XSLT_pattern" );
	else
	  System.err.println( "Usage: java Driver XPATH_pattern" );
	return;
      }

    Reader stream = new StringReader(args[0]);

    XPathParser xpp = new XPathParser(stream);

    if (DEBUG)
      xpp.enable_tracing();
    else
      xpp.disable_tracing();

    ASTXPathExpr aXPathExpr = null;
    ASTPattern aPattern = null;
    try
      {
	if (USE == XSLT)
	  aPattern = xpp.Pattern();
	else
	  aXPathExpr = xpp.XPathExpr();
      }
    catch (ParseException e)
      {
	e.printStackTrace();
	System.out.println("Failed parse.");
	return;
      }

    System.out.println( "OK    : " + args[0] );

    if(aXPathExpr!=null)
      {
	Document aDoc = null;
	try
	  {
	    DocumentBuilderFactory aFactory = DocumentBuilderFactory.newInstance();
	    DocumentBuilder aBuilder = aFactory.newDocumentBuilder();
	    aDoc = aBuilder.parse("XPathTest.xml");
	  }
	catch(ParserConfigurationException pce)
	  {
	    System.out.println("Loading or parsing of sample xml doc failed");
	  }
	catch(IOException ioe)
	  {
	    System.out.println("Loading or parsing of sample xml doc failed");
	  }
	catch(SAXException se)
	  {
	    System.out.println("Loading or parsing of sample xml doc failed");
	  }

	//aXPath.dump(" ");
	XPathPrettyPrinterParserVisitor aPrinter = new XPathPrettyPrinterParserVisitor();
	aXPathExpr.jjtAccept(aPrinter," ");

	if(false &&aDoc!=null)
	  {
	    XPathDomParserVisitor aXPathDomVistor = new XPathDomParserVisitor(aDoc,aXPathExpr);

	    ArrayList aResult = aXPathDomVistor.executeQuery();

	    // Set up an identity transformer to use as serializer.
	    try
	      {
		Transformer serializer = TransformerFactory.newInstance().newTransformer();
		serializer.setOutputProperty(OutputKeys.OMIT_XML_DECLARATION, "yes");

		// Serialize the found nodes to System.out.
		System.out.println("<Result size=\"" + aResult.size() + "\" >");
		for(int i = 0; i < aResult.size(); i++)
		  {
		    System.out.println(" "+ i + " /***********************************/");
		    Node aNode = (Node) aResult.get(i);
		    serializer.transform(new DOMSource(aNode), new StreamResult(System.out));
		    System.out.println("/***********************************/");
		  }
		System.out.println("</Result>");
	      }
	    catch(TransformerConfigurationException tce)
	      {
		System.out.println("Displaying of result failed");
	      }
	    catch(TransformerException te)
	      {
		System.out.println("Displaying of result failed");
	      }

	  }
      }

    System.out.println("Valid parse.");
    return;
  }
}
