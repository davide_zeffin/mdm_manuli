package com.sap.isa.catalog.parsers.xpathinterpreter;

/**
 * Title:        WebXSLT
 * Description:
 * Copyright:    Copyright (c) 2001
 * Company:
 * @version 1.0
 */

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;

public class XPFunctionFactory
{
	public static final String CLASS_NAME_UNKNOWN            = "com.sap.isa.catalog.parsers.xpathinterpreter.XPunknown";
	public static final String CLASS_NAME_BOOLEAN            = "com.sap.isa.catalog.parsers.xpathinterpreter.XPboolean";
	public static final String CLASS_NAME_NUMBER             = "com.sap.isa.catalog.parsers.xpathinterpreter.XPnumber";
	public static final String CLASS_NAME_STRING             = "com.sap.isa.catalog.parsers.xpathinterpreter.XPstring";
	public static final String CLASS_NAME_CEILING            = "com.sap.isa.catalog.parsers.xpathinterpreter.XPceiling";
	public static final String CLASS_NAME_FLOOR              = "com.sap.isa.catalog.parsers.xpathinterpreter.XPfloor";
	public static final String CLASS_NAME_ROUND              = "com.sap.isa.catalog.parsers.xpathinterpreter.XPround";
	public static final String CLASS_NAME_CONCAT             = "com.sap.isa.catalog.parsers.xpathinterpreter.XPconcat";
	public static final String CLASS_NAME_CONTAINS           = "com.sap.isa.catalog.parsers.xpathinterpreter.XPcontains";
	public static final String CLASS_NAME_NORMALIZE_SPACE    = "com.sap.isa.catalog.parsers.xpathinterpreter.XPnormalizeSpace";
	public static final String CLASS_NAME_STARTS_WITH        = "com.sap.isa.catalog.parsers.xpathinterpreter.XPstartsWith";
	public static final String CLASS_NAME_STRING_LENGTH      = "com.sap.isa.catalog.parsers.xpathinterpreter.XPstringLength";
	public static final String CLASS_NAME_SUBSTRING          = "com.sap.isa.catalog.parsers.xpathinterpreter.XPsubstring";
	public static final String CLASS_NAME_SUBSTRING_BEFORE   = "com.sap.isa.catalog.parsers.xpathinterpreter.XPsubstringBefore";
	public static final String CLASS_NAME_SUBSTRING_AFTER    = "com.sap.isa.catalog.parsers.xpathinterpreter.XPubstringAfter";
	public static final String CLASS_NAME_TRANSLATE          = "com.sap.isa.catalog.parsers.xpathinterpreter.XPtranslate";
	public static final String CLASS_NAME_COUNT              = "com.sap.isa.catalog.parsers.xpathinterpreter.XPcount";
	public static final String CLASS_NAME_SUM                = "com.sap.isa.catalog.parsers.xpathinterpreter.XPsum";
	public static final String CLASS_NAME_LOCAL_NAME         = "com.sap.isa.catalog.parsers.xpathinterpreter.XPlocalName";
	public static final String CLASS_NAME_NAME               = "com.sap.isa.catalog.parsers.xpathinterpreter.XPname";
	public static final String CLASS_NAME_NAMESPACE_URI      = "com.sap.isa.catalog.parsers.xpathinterpreter.XPnamespaceUri";
	public static final String CLASS_NAME_FALSE              = "com.sap.isa.catalog.parsers.xpathinterpreter.XPfalse";
	public static final String CLASS_NAME_TRUE               = "com.sap.isa.catalog.parsers.xpathinterpreter.XPtrue";
	public static final String CLASS_NAME_NOT                = "com.sap.isa.catalog.parsers.xpathinterpreter.XPnot";
	public static final String CLASS_NAME_LAST               = "com.sap.isa.catalog.parsers.xpathinterpreter.XPlast";
	public static final String CLASS_NAME_POSITION           = "com.sap.isa.catalog.parsers.xpathinterpreter.XPposition";
	public static final String CLASS_NAME_ID                 = "com.sap.isa.catalog.parsers.xpathinterpreter.XPid";
	public static final String CLASS_NAME_LANG               = "com.sap.isa.catalog.parsers.xpathinterpreter.XPlang";

	public static final int FUNC_ID_UNKNOWN            =-1;
	public static final int FUNC_ID_BOOLEAN            = 0;
	public static final int FUNC_ID_NUMBER             = 1;
	public static final int FUNC_ID_STRING             = 2;
	public static final int FUNC_ID_CEILING            = 3;
	public static final int FUNC_ID_FLOOR              = 4;
	public static final int FUNC_ID_ROUND              = 5;
	public static final int FUNC_ID_CONCAT             = 6;
	public static final int FUNC_ID_CONTAINS           = 7;
	public static final int FUNC_ID_NORMALIZE_SPACE    = 8;
	public static final int FUNC_ID_STARTS_WITH        = 9;
	public static final int FUNC_ID_STRING_LENGTH      = 10;
	public static final int FUNC_ID_SUBSTRING          = 11;
	public static final int FUNC_ID_SUBSTRING_BEFORE   = 12;
	public static final int FUNC_ID_SUBSTRING_AFTER    = 13;
	public static final int FUNC_ID_TRANSLATE          = 14;
	public static final int FUNC_ID_COUNT              = 15;
	public static final int FUNC_ID_SUM                = 16;
	public static final int FUNC_ID_LOCAL_NAME         = 17;
	public static final int FUNC_ID_NAME               = 18;
	public static final int FUNC_ID_NAMESPACE_URI      = 19;
	public static final int FUNC_ID_FALSE              = 20;
	public static final int FUNC_ID_TRUE               = 21;
	public static final int FUNC_ID_NOT                = 22;
	public static final int FUNC_ID_LAST               = 23;
	public static final int FUNC_ID_POSITION           = 24;
	public static final int FUNC_ID_ID                 = 25;
	public static final int FUNC_ID_LANG               = 26;

	public static ArrayList CLASSES_AVAILABLE = new ArrayList();

	static
	{
		CLASSES_AVAILABLE.add(FUNC_ID_BOOLEAN,CLASS_NAME_BOOLEAN);
		CLASSES_AVAILABLE.add(FUNC_ID_NUMBER,CLASS_NAME_NUMBER);
		CLASSES_AVAILABLE.add(FUNC_ID_STRING,CLASS_NAME_STRING);
		CLASSES_AVAILABLE.add(FUNC_ID_CEILING,CLASS_NAME_CEILING);
		CLASSES_AVAILABLE.add(FUNC_ID_FLOOR,CLASS_NAME_FLOOR);
		CLASSES_AVAILABLE.add(FUNC_ID_ROUND,CLASS_NAME_ROUND);
		CLASSES_AVAILABLE.add(FUNC_ID_CONCAT,CLASS_NAME_CONCAT);
		CLASSES_AVAILABLE.add(FUNC_ID_CONTAINS,CLASS_NAME_CONTAINS);
		CLASSES_AVAILABLE.add(FUNC_ID_NORMALIZE_SPACE,CLASS_NAME_NORMALIZE_SPACE);
		CLASSES_AVAILABLE.add(FUNC_ID_STARTS_WITH,CLASS_NAME_STARTS_WITH);
		CLASSES_AVAILABLE.add(FUNC_ID_STRING_LENGTH,CLASS_NAME_STRING_LENGTH);
		CLASSES_AVAILABLE.add(FUNC_ID_SUBSTRING,CLASS_NAME_SUBSTRING);
		CLASSES_AVAILABLE.add(FUNC_ID_SUBSTRING_BEFORE,CLASS_NAME_SUBSTRING_BEFORE);
		CLASSES_AVAILABLE.add(FUNC_ID_SUBSTRING_AFTER,CLASS_NAME_SUBSTRING_AFTER);
		CLASSES_AVAILABLE.add(FUNC_ID_TRANSLATE,CLASS_NAME_TRANSLATE);
		CLASSES_AVAILABLE.add(FUNC_ID_COUNT,CLASS_NAME_COUNT);
		CLASSES_AVAILABLE.add(FUNC_ID_SUM,CLASS_NAME_SUM);
		CLASSES_AVAILABLE.add(FUNC_ID_LOCAL_NAME,CLASS_NAME_LOCAL_NAME);
		CLASSES_AVAILABLE.add(FUNC_ID_NAME,CLASS_NAME_NAME);
		CLASSES_AVAILABLE.add(FUNC_ID_NAMESPACE_URI,CLASS_NAME_NAMESPACE_URI);
		CLASSES_AVAILABLE.add(FUNC_ID_FALSE,CLASS_NAME_FALSE);
		CLASSES_AVAILABLE.add(FUNC_ID_TRUE,CLASS_NAME_TRUE);
		CLASSES_AVAILABLE.add(FUNC_ID_NOT,CLASS_NAME_NOT);
		CLASSES_AVAILABLE.add(FUNC_ID_LAST,CLASS_NAME_LAST);
		CLASSES_AVAILABLE.add(FUNC_ID_POSITION,CLASS_NAME_POSITION);
		CLASSES_AVAILABLE.add(FUNC_ID_ID,CLASS_NAME_ID);
		CLASSES_AVAILABLE.add(FUNC_ID_LANG,CLASS_NAME_LANG);
	}

	private static XPFunctionFactory theFuncFactory = null;

    protected XPFunctionFactory()
	{
    }

	synchronized public static XPFunctionFactory getInstance()
	{
		if(theFuncFactory==null)
			theFuncFactory = new XPFunctionFactory();
		return theFuncFactory;
	}

	XPathFunction getFunction(int funcId, XPathContext theContext)
	{
		XPathFunction theFunction = null;
		try
		{
		    Class clazz =
			getClass().forName((String)CLASSES_AVAILABLE.get(funcId));
		    Constructor theConstructor =
				clazz.getConstructor(new Class[]{XPathContext.class});
			theFunction =
				(XPathFunction)
				theConstructor.newInstance(new Object[]{theContext});
		}
		catch(ClassNotFoundException cne)
		{
			System.out.println("error: function  constructor not found");
		}
		catch(NoSuchMethodException nsme)
		{
			System.out.println("error: function  constructor not found");
		}
		catch(InvocationTargetException ite)
		{
			System.out.println("error: function  constructor not found");
		}
		catch(IllegalAccessException iae)
		{
			System.out.println("error: function  constructor not found");
		}
		catch(InstantiationException ie)
		{
			System.out.println("error: function  constructor not found");
		}

		return theFunction;
	}
}
