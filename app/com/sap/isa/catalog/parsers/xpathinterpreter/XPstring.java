package com.sap.isa.catalog.parsers.xpathinterpreter;

import java.io.IOException;
import java.io.StringReader;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.ArrayList;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Attr;
import org.w3c.dom.Comment;
import org.w3c.dom.Document;
import org.w3c.dom.DocumentFragment;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.w3c.dom.ProcessingInstruction;
import org.w3c.dom.Text;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

/**
 * Title:        WebXSLT
 * Description:
 * Copyright:    Copyright (c) 2001
 * Company:
 * @version 1.0
 */

public class XPstring extends XPathFunction
{
  public XPstring(XPathContext aContext)
  {
    super(aContext);
  }

  public IXPvalue call(ArrayList theArguments)
  {
    if(theArguments.size()>1)
      throw new RuntimeException("Wrong number of arguments for string().");

    IXPvalue theResult = null;
    //convert the current node to a string
    if(theArguments.size()==0)
      {
	theResult = call();
      }
    else
      {
	IXPvalue anArgument = (IXPvalue)theArguments.get(0);
	theResult = call(anArgument);
      }
    return theResult;
  }

  public IXPstringValue call()
  {
    Node current = this.theContext.getCurrentNode();
    return new XPstringValue(nodeToString(current));  
  }

  public IXPstringValue call(IXPvalue anArgument)
  {
    String theResult = "";
    int typeId = anArgument.getTypeId();
    switch(typeId){
    case IXPbooleanValue.ID:{
      IXPbooleanValue theValue = (IXPbooleanValue)anArgument;
      Boolean theRealValue = theValue.getValue();   
      theResult = theRealValue.toString();
    }
      break;
    case IXPnodeSetValue.ID:{
      IXPnodeSetValue theValue = (IXPnodeSetValue)anArgument;
      ArrayList theRealValue = theValue.getValue();
      if(theRealValue.size()==0)
	theResult="";
      else
	{
	  Node theFirstNode = (Node) theRealValue.get(0);
	  theResult = nodeToString(theFirstNode);
	}
    }
      break;
    case IXPnumberValue.ID:{
      IXPnumberValue theValue = (IXPnumberValue)anArgument;
      Number theRealValue = theValue.getValue();
      if(theRealValue instanceof Integer)
	theResult = theRealValue.toString();
      else
	{
	  Double theDouble = (Double)theRealValue;
	  DecimalFormatSymbols theSymbols = new DecimalFormatSymbols();
	  theSymbols.setMinusSign('-');
	  theSymbols.setInfinity("Infinity");
	  theSymbols.setNaN("NaN");
	  theSymbols.setDecimalSeparator('.');
	  //adjust the # to what every the system might be able to represent
	  DecimalFormat aFormater = new DecimalFormat( "#################################0.0##############################", theSymbols);
	  theResult = aFormater.format(theDouble.doubleValue());
	}
    }
      break;
    case IXPstringValue.ID:{
      return (IXPstringValue)anArgument;
    }
    case IXPtreeFragmentValue.ID:{
      IXPtreeFragmentValue theValue = (IXPtreeFragmentValue)anArgument;
      DocumentFragment theFrag = theValue.getValue();
      theResult = nodeToString(theFrag);
    }
      break;
    }
    return new XPstringValue(theResult);
  }

  public static String nodeToString(Node theNode)
  {
    String theResult ="";
    //Attributes
    if(theNode.getNodeType() == Node.ATTRIBUTE_NODE)
      theResult = ((Attr) theNode).getValue();
    //Element and Root and DocumentFragement
    else if ( theNode.getNodeType() == Node.ELEMENT_NODE ||
	      theNode.getNodeType() == Node.DOCUMENT_NODE ||
	      theNode.getNodeType() == Node.DOCUMENT_FRAGMENT_NODE)
      theResult = elementOrRootToString(theNode);
    else if (   theNode.getNodeType()==Node.PROCESSING_INSTRUCTION_NODE)
      theResult = ((ProcessingInstruction)theNode).getData();
    else if(theNode.getNodeType()== Node.TEXT_NODE)
      theResult = ((Text)theNode).getData();
    else if(theNode.getNodeType()== Node.COMMENT_NODE)
      theResult = ((Comment)theNode).getData();
    else//works for  namespace ???
      theResult = theNode.toString();
    return theResult;
  }

  public static String expandCharacterReferences(String aStringWithReferences)
  {
    Document  aSimpleDoc = null;
    String data="";
    try
      {
    	DocumentBuilderFactory aFactory = DocumentBuilderFactory.newInstance();
	DocumentBuilder aBuilder = aFactory.newDocumentBuilder();

	StringBuffer aBuffer = new StringBuffer("<doc>");
	aBuffer.append(aStringWithReferences);
	aBuffer.append("</doc>");
	StringReader aReader = new StringReader(aBuffer.toString());
	aSimpleDoc = aBuilder.parse(new InputSource(aReader));
	data = ((Text)aSimpleDoc.getDocumentElement().getFirstChild()).getData();
      }
    catch(ParserConfigurationException pce)
      {
	System.out.println("Loading or parsing of aStringWithReferences as xml  failed");
	data = aStringWithReferences;
      }
    catch(IOException ioe)
      {
	System.out.println("Loading or parsing of aStringWithReferences as xml  failed");
	data = aStringWithReferences;
      }
    catch(SAXException se)
      {
	System.out.println("Loading or parsing of aStringWithReferences as xml  failed");
	data = aStringWithReferences;
      }
    return data;
  }

  private static String elementOrRootToString(Node theNode)
  {
    StringBuffer aBuffer = new StringBuffer();
    buildBuffer(theNode,aBuffer);

    return aBuffer.toString();
  }

  private static void buildBuffer(Node theNode, StringBuffer aBuffer)
  {
    NodeList theChilds = theNode.getChildNodes();
    for(int i = 0; i < theChilds.getLength() ; i++)
      {
	Node aNode = theChilds.item(i);
	//In document order
	if( aNode.getNodeType()==Node.DOCUMENT_NODE ||
	    aNode.getNodeType()==Node.ELEMENT_NODE ||
	    aNode.getNodeType()==Node.DOCUMENT_FRAGMENT_NODE)
	  buildBuffer(aNode,aBuffer);
	if(aNode.getNodeType() == Node.TEXT_NODE)
	  aBuffer.append(((Text)aNode).getData());
      }
  }


}
