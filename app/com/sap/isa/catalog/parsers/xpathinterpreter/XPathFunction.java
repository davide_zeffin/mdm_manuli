package com.sap.isa.catalog.parsers.xpathinterpreter;

import java.util.ArrayList;

/**
 * Title:        WebXSLT
 * Description:
 * Copyright:    Copyright (c) 2001
 * Company:
 * @version 1.0
 */

abstract public class XPathFunction
{
	protected XPathContext theContext;

	public XPathFunction(XPathContext aContext)
	{
		this.theContext=aContext;
	}

	abstract IXPvalue call(ArrayList theArguments);
}
