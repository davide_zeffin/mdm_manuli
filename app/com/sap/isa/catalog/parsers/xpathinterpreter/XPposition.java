package com.sap.isa.catalog.parsers.xpathinterpreter;

import java.util.ArrayList;

/**
 * Title:        WebXSLT
 * Description:
 * Copyright:    Copyright (c) 2001
 * Company:
 * @version 1.0
 */

public class XPposition extends XPathFunction
{
    public XPposition(XPathContext aContext)
	{
		super(aContext);
    }

    public IXPvalue call(ArrayList theArguments)
	{
        if(theArguments.size()!=0)
            throw new RuntimeException(
            "Method position() called with wrong number of arguments!");
        return call();
    }
    
    public IXPnumberValue call()
    {
        return new XPnumberValue(new Integer(theContext.getCurrentNodeIndex()+1));
    }
}
