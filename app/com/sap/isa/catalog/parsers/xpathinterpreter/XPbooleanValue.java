package com.sap.isa.catalog.parsers.xpathinterpreter;

/**
 * Title:        WebXSLT
 * Description:
 * Copyright:    Copyright (c) 2001
 * Company:
 * @version 1.0
 */

public class XPbooleanValue implements IXPbooleanValue
{
	private Boolean theBoolean;

	public XPbooleanValue(Boolean theBoolean)
	{
		this.theBoolean=theBoolean;
	}

	public int getTypeId()
	{
		return IXPbooleanValue.ID;
	}

	public Boolean getValue()
	{
		return theBoolean;
	}

	public String toString()
	{
		return theBoolean.toString();
	}
}

