package com.sap.isa.catalog.parsers.xpathinterpreter;

import java.util.ArrayList;

/**
 * Title:        WebXSLT
 * Description:
 * Copyright:    Copyright (c) 2001
 * Company:
 * @version 1.0
 */

public class XPboolean extends XPathFunction
{
  public XPboolean(XPathContext aContext)
  {
    super(aContext);
  }

  public IXPvalue call(ArrayList theArguments)
  {
    if(theArguments.size()!=1)
      throw new RuntimeException("Wrong number of arguments for boolean().");

    IXPvalue anArgument = (IXPvalue)theArguments.get(0);

    return this.call(anArgument);
  }

  public IXPbooleanValue call(IXPvalue aValueToConvert)
  {
    Boolean theResult = null;

    XPstring theStringFunction = new XPstring(theContext);

    int typeId = aValueToConvert.getTypeId();

    switch(typeId){
    case IXPbooleanValue.ID:{
      IXPbooleanValue theValue = (IXPbooleanValue)aValueToConvert;
      theResult = theValue.getValue();
    }
      break;
    case IXPnodeSetValue.ID:{
      IXPnodeSetValue theValue = (IXPnodeSetValue)aValueToConvert;
      ArrayList theRealValue = theValue.getValue();
      int size = theRealValue.size();
      theResult = (size==0 ? new Boolean(false) : new Boolean(true));
    }
      break;
    case IXPnumberValue.ID:{
      IXPnumberValue theValue = (IXPnumberValue)aValueToConvert;
      Number theRealValue = theValue.getValue();
      double theDValue = theRealValue.doubleValue();
      if (Double.isNaN(theDValue))
      {
	theResult = new Boolean(false);
      } // end of if ()
      else 
       {
	 theResult = (   theDValue==0.0 ?
			  new Boolean(false) :
			  new Boolean(true));
       } // end of else
    }
      break;
    case IXPstringValue.ID:{
      IXPstringValue theValue = (IXPstringValue)aValueToConvert;
      String theStrResult = theValue.getValue();
      theResult = (   theStrResult.length()==0 ?
		      new Boolean(false) :
		      new Boolean(true));
    }
      break;
    case IXPtreeFragmentValue.ID:{
      IXPtreeFragmentValue theValue =
	(IXPtreeFragmentValue)aValueToConvert;
      IXPstringValue theConvertedTree = theStringFunction.call(theValue);
      String theConvertedString = theConvertedTree.getValue();
      theResult = (   theConvertedString.length()==0 ?
		      new Boolean(false) :
		      new Boolean(true));
    }
      break;
    }
    return new XPbooleanValue(theResult);
  }
}
