package com.sap.isa.catalog.parsers.xpathinterpreter;

/**
 * Title:        WebXSLT
 * Description:
 * Copyright:    Copyright (c) 2001
 * Company:
 * @version 1.0
 */

public class XPnumberValue
    implements  IXPnumberValue
{
    private Number theNumber;

    public XPnumberValue(Number aNumber)
    {
        this.theNumber=aNumber;
    }

    public int getTypeId()
    {
        return IXPnumberValue.ID;
    }

    public Number getValue()
    {
        return theNumber;
    }

    public boolean isNaN()
    {
        if ( theNumber instanceof Double)
        {
            Double theDouble = (Double) theNumber;
            return theDouble.isNaN();
        } // end of if ()
        else
            return false;
    }
    
    public void multiplyBy(Number aNumber)
    {
        if (aNumber instanceof Integer) 
        {
            multiplyBy(aNumber.intValue());
        } 
        else 
        {
            multiplyBy(aNumber.doubleValue());
        }
            // end of if ()
        return;
    }

    public void multiplyBy(int aMultiplyer)
    {
        if(theNumber instanceof Integer)
        {
            theNumber=new Integer(theNumber.intValue()*aMultiplyer);
        }
        else
            theNumber=new Double(theNumber.doubleValue()*aMultiplyer);
    }

    public void multiplyBy(double aMultiplyer)
    {
        theNumber=new Double(theNumber.doubleValue()*aMultiplyer);
    }

    public void add(double toBeAdded)
    {
        theNumber = new Double(theNumber.doubleValue()+toBeAdded);
    }

    public void add(int toBeAdded)
    {
        if(theNumber instanceof Integer)
        {
            theNumber=new Integer(theNumber.intValue()+toBeAdded);
        }
        else
            theNumber=new Double(theNumber.doubleValue()+toBeAdded);
    }

    public void add(Number aNumber)
    {
        if (aNumber instanceof Integer) 
        {
            add(aNumber.intValue());
        } 
        else 
        {
            add(aNumber.doubleValue());
        }
            // end of if ()
        return;
    }

    public int compareTo(Object aObject)
        throws ClassCastException
    {
        if(!(aObject instanceof IXPnumberValue) )
            throw new ClassCastException("Not comparable with: " +
                                         aObject.getClass().getName());

        Number theOtherNumber = ((IXPnumberValue)aObject).getValue();
        int theResult = 0;
        if(theOtherNumber instanceof Integer && theNumber instanceof Integer)
            if(theOtherNumber.intValue()==theNumber.intValue())
                theResult = 0;
            else if(theNumber.intValue() < theOtherNumber.intValue())
                theResult = -1;
            else
                theResult = 1;
        else
            if(theOtherNumber.doubleValue()==theNumber.doubleValue())
                theResult = 0;
            else if( theNumber.doubleValue() < theOtherNumber.doubleValue())
                theResult = -1;
            else
                theResult = 1;

        return theResult;
    }

    static public int compareNumbers(Number aNumber, Number anOtherNumber)
    {
        int theResult = 0;
        if(anOtherNumber instanceof Integer && aNumber instanceof Integer)
            if(anOtherNumber.intValue()==aNumber.intValue())
                theResult = 0;
            else if(aNumber.intValue() < anOtherNumber.intValue())
                theResult = -1;
            else
                theResult = 1;
        else
            if(anOtherNumber.doubleValue()==aNumber.doubleValue())
                theResult = 0;
            else if( aNumber.doubleValue() < anOtherNumber.doubleValue())
                theResult = -1;
            else
                theResult = 1;

        return theResult;
    }

    public String toString()
    {
        if(theNumber instanceof Double)
            return ((Double)theNumber).toString();
        else if(theNumber instanceof Integer)
            return ((Integer)theNumber).toString();
        else if(theNumber instanceof Float)
            return ((Float)theNumber).toString();
        else if(theNumber instanceof Long)
            return ((Long)theNumber).toString();
        else if(theNumber instanceof Short)
            return ((Short)theNumber).toString();
        return "";
    }
}

