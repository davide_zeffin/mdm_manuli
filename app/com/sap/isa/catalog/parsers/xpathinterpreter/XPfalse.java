package com.sap.isa.catalog.parsers.xpathinterpreter;

import java.util.ArrayList;

/**
 * Title:        WebXSLT
 * Description:
 * Copyright:    Copyright (c) 2001
 * Company:
 * @version 1.0
 */

public class XPfalse extends XPathFunction
{

    public XPfalse(XPathContext aContext)
	{
		super(aContext);
    }

    public IXPvalue call(ArrayList theArguments)
	{
		return new XPbooleanValue(new Boolean(false));
    }

    public IXPbooleanValue call()
    {
        return new XPbooleanValue(new Boolean(false));
    }
}
