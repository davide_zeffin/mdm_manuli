package com.sap.isa.catalog.parsers.xpathinterpreter;

import java.util.ArrayList;

/**
 * Title:        WebXSLT
 * Description:
 * Copyright:    Copyright (c) 2001
 * Company:
 * @version 1.0
 */

public class XPsubstring extends XPathFunction
{
  XPstring theStringFunction;
  XPnumber theNumberFunction;
  XPround theRoundFunction;

  public XPsubstring(XPathContext aContext)
  {
    super(aContext);
    theStringFunction = new XPstring(aContext);
    theNumberFunction = new XPnumber(aContext);
    theRoundFunction = new XPround(aContext);
  }

  public IXPvalue call(ArrayList theArguments)
  {
    if(theArguments.size()>3 || theArguments.size() < 2)
      throw new RuntimeException("substring() called with wrong number of arguments!");
    if(theArguments.size()==2)
      {
	return call((IXPvalue)theArguments.get(0),(IXPvalue)theArguments.get(1));
       }
    else
	return call((IXPvalue)theArguments.get(0),
		    (IXPvalue)theArguments.get(1),
		    (IXPvalue)theArguments.get(2));
  }

  public IXPstringValue call(IXPvalue aStringValue, IXPvalue aStartingPointValue)
  {
    IXPstringValue aContainingStringValue = theStringFunction.call(aStringValue);
    IXPnumberValue aStartingPointNumber = theNumberFunction.call(aStartingPointValue);
    String aContainingString = theStringFunction.expandCharacterReferences(aContainingStringValue.getValue());

    String subString = aContainingString.substring(aStartingPointNumber.getValue().intValue()-1);
    return new XPstringValue(subString);
  }

  public IXPstringValue call(IXPvalue aStringValue, IXPvalue aStartingPointValue, IXPvalue aLenghtValue)
  {
    IXPstringValue aContainingStringValue = theStringFunction.call(aStringValue);
    IXPnumberValue aStartingPointNumber = theNumberFunction.call(aStartingPointValue);
    IXPnumberValue aLengthNumber = theNumberFunction.call(aLenghtValue);
    String aContainingString = theStringFunction.expandCharacterReferences(aContainingStringValue.getValue());
    int startingPoint = theRoundFunction.call(aStartingPointNumber).getValue().intValue()-1;
    int endPoint = startingPoint + theRoundFunction.call(aLengthNumber).getValue().intValue();
    String subString = aContainingString.substring(startingPoint,endPoint);
    return  new XPstringValue(subString);
    }
}

