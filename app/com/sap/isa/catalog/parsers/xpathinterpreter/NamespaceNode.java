package com.sap.isa.catalog.parsers.xpathinterpreter;

import org.w3c.dom.Attr;
import org.w3c.dom.DOMException;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;


/**
 * NamespaceNode.java
 *
 *
 * Created: Thu Mar 28 21:55:49 2002
 *
 * @version
 */

public class NamespaceNode implements INamespaceNode
{
    private Attr theClonedNamespaceAttribute;
    private Element theParentElement;
    
    public NamespaceNode( Attr aClonedNamespaceAttribute, Element aParentElement)
    {
        this.theClonedNamespaceAttribute=aClonedNamespaceAttribute;
        this.theParentElement=aParentElement;
    }
// implementation of org.w3c.dom.Attr interface

/**
 *
 * @return <description>
 */
    public String getName()
    {
            // TODO: implement this org.w3c.dom.Attr method
        return theClonedNamespaceAttribute.getName();
    }

/**
 *
 * @return <description>
 */
    public String getValue()
    {
            // TODO: implement this org.w3c.dom.Attr method
        return theClonedNamespaceAttribute.getValue();
    }

/**
 *
 * @param param1 <description>
 * @exception org.w3c.dom.DOMException <description>
 */
    public void setValue(String param1) throws DOMException
    {
            // TODO: implement this org.w3c.dom.Attr method
    }

/**
 *
 * @return <description>
 */
    public Element getOwnerElement()
    {
            // TODO: implement this org.w3c.dom.Attr method
        return this.theParentElement;
    }

/**
 *
 * @return <description>
 */
    public boolean getSpecified()
    {
            // TODO: implement this org.w3c.dom.Attr method
        return false;
    }
// implementation of org.w3c.dom.Node interface

/**
 *
 * @return <description>
 */
    public NamedNodeMap getAttributes()
    {
            // TODO: implement this org.w3c.dom.Node method
        return null;
    }

/**
 *
 */
    public void normalize()
    {
            // TODO: implement this org.w3c.dom.Node method
    }

/**
 *
 * @param param1 <description>
 * @return <description>
 * @exception org.w3c.dom.DOMException <description>
 */
    public Node appendChild(Node param1) throws DOMException
    {
            // TODO: implement this org.w3c.dom.Node method
        return null;
    }

/**
 *
 * @param param1 <description>
 * @return <description>
 */
    public Node cloneNode(boolean param1)
    {
            // TODO: implement this org.w3c.dom.Node method
        return null;
    }

/**
 *
 * @return <description>
 */
    public NodeList getChildNodes()
    {
            // TODO: implement this org.w3c.dom.Node method
        return null;
    }

/**
 *
 * @return <description>
 */
    public Node getFirstChild()
    {
            // TODO: implement this org.w3c.dom.Node method
        return null;
    }

/**
 *
 * @return <description>
 */
    public Node getLastChild()
    {
            // TODO: implement this org.w3c.dom.Node method
        return null;
    }

/**
 *
 * @return <description>
 */
    public String getLocalName()
    {
            // TODO: implement this org.w3c.dom.Node method
        return theClonedNamespaceAttribute.getLocalName();
    }

/**
 *
 * @return <description>
 */
    public String getNamespaceURI()
    {
            // TODO: implement this org.w3c.dom.Node method
        return null;
    }

/**
 *
 * @return <description>
 */
    public Node getNextSibling()
    {
            // TODO: implement this org.w3c.dom.Node method
        return null;
    }

/**
 *
 * @return <description>
 */
    public String getNodeName()
    {
        if ( theClonedNamespaceAttribute.getNodeName().indexOf("xmlns:")!=-1)
        {
            return theClonedNamespaceAttribute.getNodeName().substring(6);
        }
        else
        {
            return "";
        } // end of else
    }

/**
 *
 * @return <description>
 */
    public short getNodeType()
    {
            // TODO: implement this org.w3c.dom.Node method
        return theClonedNamespaceAttribute.getNodeType();
    }

/**
 *
 * @return <description>
 * @exception org.w3c.dom.DOMException <description>
 */
    public String getNodeValue() throws DOMException
    {
            // TODO: implement this org.w3c.dom.Node method
        return null;
    }

/**
 *
 * @return <description>
 */
    public Document getOwnerDocument()
    {
            // TODO: implement this org.w3c.dom.Node method
        return theParentElement.getOwnerDocument();
    }

/**
 *
 * @return <description>
 */
    public Node getParentNode()
    {
            // TODO: implement this org.w3c.dom.Node method
        return null;
    }

/**
 *
 * @return <description>
 */
    public String getPrefix()
    {
            // TODO: implement this org.w3c.dom.Node method
        return null;
    }

/**
 *
 * @return <description>
 */
    public Node getPreviousSibling()
    {
            // TODO: implement this org.w3c.dom.Node method
        return null;
    }

/**
 *
 * @return <description>
 */
    public boolean hasAttributes()
    {
            // TODO: implement this org.w3c.dom.Node method
        return false;
    }

/**
 *
 * @return <description>
 */
    public boolean hasChildNodes()
    {
            // TODO: implement this org.w3c.dom.Node method
        return false;
    }

/**
 *
 * @param param1 <description>
 * @param param2 <description>
 * @return <description>
 * @exception org.w3c.dom.DOMException <description>
 */
    public Node insertBefore(Node param1, Node param2) throws DOMException
    {
            // TODO: implement this org.w3c.dom.Node method
        return null;
    }

/**
 *
 * @param param1 <description>
 * @param param2 <description>
 * @return <description>
 */
    public boolean isSupported(String param1, String param2)
    {
            // TODO: implement this org.w3c.dom.Node method
        return false;
    }

/**
 *
 * @param param1 <description>
 * @return <description>
 * @exception org.w3c.dom.DOMException <description>
 */
    public Node removeChild(Node param1) throws DOMException
    {
            // TODO: implement this org.w3c.dom.Node method
        return null;
    }

/**
 *
 * @param param1 <description>
 * @param param2 <description>
 * @return <description>
 * @exception org.w3c.dom.DOMException <description>
 */
    public Node replaceChild(Node param1, Node param2) throws DOMException
    {
            // TODO: implement this org.w3c.dom.Node method
        return null;
    }

/**
 *
 * @param param1 <description>
 * @exception org.w3c.dom.DOMException <description>
 */
    public void setNodeValue(String param1) throws DOMException
    {
            // TODO: implement this org.w3c.dom.Node method
    }

/**
 *
 * @param param1 <description>
 * @exception org.w3c.dom.DOMException <description>
 */
    public void setPrefix(String param1) throws DOMException
    {
            // TODO: implement this org.w3c.dom.Node method
    }

}// NamespaceNode
