package com.sap.isa.catalog.parsers.xpathinterpreter;

/**
 * Title:        WebXSLT
 * Description:
 * Copyright:    Copyright (c) 2001
 * Company:
 * @version 1.0
 */

import java.util.ArrayList;
import java.util.HashMap;

import org.w3c.dom.DocumentFragment;
import org.w3c.dom.Node;

public class XPathContext
{
    public static final int SELF                   = 0;
    public static final int CHILD                  = 1;
    public static final int PARENT                 = 2;
    public static final int ANCESTOR               = 3;
    public static final int ATTRIBUTE              = 4;
    public static final int NAMESPACE              = 5;
    public static final int PRECEDING              = 6;
    public static final int FOLLOWING              = 7;
    public static final int DESCENDANT             = 8;
    public static final int ANCESTOR_OR_SELF       = 9;
    public static final int FOLLOWING_SIBILLING    = 10;
    public static final int PRECEDING_SIBILLING    = 11;
    public static final int DESCENDANT_OR_SELF     = 12;

    private ArrayList theAxisNames = new ArrayList();
    private int theAxisToWalk = CHILD;

    private int theCurrentNode;
    private ArrayList theContext; // a list of nodes

    private int theValueTypeId = IXPbooleanValue.ID; //inital value

    private Boolean             theBooleanValue = new Boolean(false); //init value
    private String              theStringValue;
    private Number              theNumberValue;
    private ArrayList           theNodeSetValue;
    private DocumentFragment    theTreeFragmentValue;

    private boolean isRelativeLocationPath = false;
    private boolean isFirstStepVisited = false;

    private HashMap theVariables = new HashMap();
    
    public XPathContext(ArrayList theContext, int theCurrentNode)
    {
        this.theContext=theContext;
        this.theCurrentNode=theCurrentNode;
        theAxisNames.add("self::");
        theAxisNames.add("child::");
        theAxisNames.add("parent::");
        theAxisNames.add("ancestor::");
        theAxisNames.add("attribute::");
        theAxisNames.add("namespace::");
        theAxisNames.add("preceding::");
        theAxisNames.add("following::");
        theAxisNames.add("descendant::");
        theAxisNames.add("ancestor-or-self::");
        theAxisNames.add("following-sibling::");
        theAxisNames.add("preceding-sibling::");
        theAxisNames.add("descendant-or-self::");
    }

    public IXPvalue getValue()
    {
        IXPvalue theValue = null;
        switch (theValueTypeId) {
            case IXPbooleanValue.ID :
                theValue = new XPbooleanValue(this.theBooleanValue);
                break;
            case IXPnumberValue.ID :
                theValue = new XPnumberValue(this.theNumberValue);
                break;
            case IXPstringValue.ID :
                theValue = new XPstringValue(this.theStringValue);
                break;
            case IXPnodeSetValue.ID :
                theValue = new XPnodeSetValue(this.theNodeSetValue);
                break;
            case IXPtreeFragmentValue.ID :
                theValue = new XPtreeFragmentValue(this.theTreeFragmentValue);
                break;
        }
        return theValue;
    }

    public void setBooleanValue(Boolean aBoolean)
    {
        this.theValueTypeId = IXPbooleanValue.ID;
        this.theBooleanValue=aBoolean;
    }

    public void setNumberValue(Number aNumber)
    {
        this.theValueTypeId = IXPnumberValue.ID;
        this.theNumberValue=aNumber;
    }

    public void setStringValue(String aValue)
    {
        this.theValueTypeId = IXPstringValue.ID;
        this.theStringValue=aValue;
    }

    public void setNodeSetValue(ArrayList aNodeSet)
    {
        this.theValueTypeId = IXPnodeSetValue.ID;
        this.theNodeSetValue=aNodeSet;
    }

    public void setFragmentValue(DocumentFragment aFragment)
    {
        this.theValueTypeId = IXPtreeFragmentValue.ID;
        this.theTreeFragmentValue=aFragment;
    }

    public void setValue(IXPvalue aValue)
    {
        int typeId = aValue.getTypeId();
        this.theValueTypeId = typeId;
        switch (typeId){
            case IXPbooleanValue.ID :
                this.theBooleanValue = ((IXPbooleanValue) aValue).getValue();
                break;
            case IXPnumberValue.ID :
                this.theNumberValue = ((IXPnumberValue) aValue).getValue();
                break;
            case IXPstringValue.ID :
                this.theStringValue = ((IXPstringValue) aValue).getValue();
                break;
            case IXPnodeSetValue.ID :
                this.theNodeSetValue = ((IXPnodeSetValue) aValue).getValue();
                break;
            case IXPtreeFragmentValue.ID :
                this.theTreeFragmentValue = ((IXPtreeFragmentValue) aValue).getValue();
                break;
        }
        return;
    }

    public Node getCurrentNode()
    {
        return (Node) theContext.get(theCurrentNode);
    }

    public int getCurrentNodeIndex()
    {
        return theCurrentNode;
    }

    public void setCurrentNode(int currentNodeIndex)
    {
        this.theCurrentNode = currentNodeIndex;
        return;
    }

    public int getContextSize()
    {
        return theContext.size();
    }
    
    public void setVariables(HashMap aVariablesMap)
    {
        theVariables=aVariablesMap;
    }

    public void setVariable(String aVariablename, IXPvalue aValue)
    {
        theVariables.put(aVariablename,aValue);
    }

    public IXPvalue getVariable(String aVariablename)
    {
        return (IXPvalue) theVariables.get(aVariablename);
    }

    public HashMap getVariables()
    {
        return theVariables;
    }
    
    public ArrayList getContext()
    {
        return this.theContext;
    }

    public void setContext(ArrayList aNewContext)
    {
        if(theContext==theNodeSetValue)
            this.theNodeSetValue=aNewContext;
        this.theContext=aNewContext;
    }

    public int getAxis()
    {
        return this.theAxisToWalk;
    }

    public void setAxis(String aAxisToWalk)
    {
        int aIndex = this.theAxisNames.indexOf(aAxisToWalk);
        if( aIndex>=0 )
            this.theAxisToWalk=aIndex;
    }

    public boolean isRelativeLocationPath()
    {
        return this.isRelativeLocationPath;
    }

    public void setRelativeLocationPath(boolean isRelativeLocationPath)
    {
        this.isRelativeLocationPath = isRelativeLocationPath;
    }

    public boolean isFirstStepVisited()
    {
        return this.isFirstStepVisited;
    }

    public void setFirstStepVisited(boolean isFirstStepVisited)
    {
        this.isFirstStepVisited = isFirstStepVisited;
    }
}
