package com.sap.isa.catalog.parsers.xpathinterpreter;

import java.util.ArrayList;

import org.w3c.dom.Attr;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;

/**
 * Title:        WebXSLT
 * Description:
 * Copyright:    Copyright (c) 2001
 * Company:
 * @version 1.0
 */

public class XPlang extends XPathFunction
{
	private XPstring theStringFunction;

    public XPlang(XPathContext aContext)
	{
		super(aContext);
	    theStringFunction = new XPstring(aContext);
    }

    public IXPvalue call(ArrayList theArguments)
	{
		if(theArguments.size()!=1)
	        throw new RuntimeException("Wrong number of arguments for id()");
		IXPvalue theArgument = (IXPvalue)theArguments.get(0);
		IXPbooleanValue theResult = new XPbooleanValue(new Boolean(false));
		int theTypeId = theArgument.getTypeId();
		switch (theTypeId){
			case IXPnodeSetValue.ID :
			case IXPbooleanValue.ID :
			case IXPstringValue.ID :
			case IXPnumberValue.ID :
			case IXPtreeFragmentValue.ID :
			default:
				IXPstringValue theConvertedValue =
					theStringFunction.call(theArgument);
				theResult = call(theConvertedValue);
			break;
		}
		return theResult;
    }

    public IXPbooleanValue call(IXPstringValue theLang)
	{
		String theLangStr = theLang.getValue();
		Node theCurrent = theContext.getCurrentNode();
		String theXMLLangAttribute = findXMLLang(theCurrent);
		if(theXMLLangAttribute==null)
		    return 	new XPbooleanValue(new Boolean(false));
		if(theXMLLangAttribute.equalsIgnoreCase(theLangStr))
			return new XPbooleanValue(new Boolean(true));
		int theIndex = theXMLLangAttribute.indexOf("-");
		if(theIndex!=-1)
		{
		    String skipSuffixes =
			    theXMLLangAttribute.substring(0,theIndex);
		    if(skipSuffixes.equalsIgnoreCase(theLangStr))
			    return new XPbooleanValue(new Boolean(true));
		}
	    return 	new XPbooleanValue(new Boolean(false));
    }

	private String findXMLLang(Node aNode)
	{
		if(aNode == null)
			return null;
		String aResult = null;
		if(aNode.getNodeType()==Node.ELEMENT_NODE)
		{
			Element aElement = (Element)aNode;
			String aValue = aElement.getAttribute("xml:lang");
			if(aValue==null || aValue.equals(""))
			    aResult = findXMLLang(aElement.getParentNode());
			else
				aResult = aValue;
		}
		if(aNode.getNodeType()==Node.DOCUMENT_NODE)
		{
			Document aDoc =(Document) aNode;
			NamedNodeMap aMap = aDoc.getAttributes();
			if(aMap!=null)
			{
			    Node attribute = aMap.getNamedItem("xml:lang");
			    if(attribute!=null)
			    {
				    Attr anAttr = (Attr)attribute;
				    aResult = anAttr.getValue();
			    }
			}
		}
		return aResult;
	}


}
