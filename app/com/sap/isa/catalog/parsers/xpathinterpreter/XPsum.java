package com.sap.isa.catalog.parsers.xpathinterpreter;

import java.util.ArrayList;
import java.util.Iterator;

import org.w3c.dom.Node;

/**
 * Title:        WebXSLT
 * Description:
 * Copyright:    Copyright (c) 2001
 * Company:
 * @version 1.0
 */

public class XPsum extends XPathFunction
{
  private  XPnumber theNumberFunction;
  private XPstring theStringFunction;

  public XPsum(XPathContext aContext)
  {
    super(aContext);
    theStringFunction = new XPstring(aContext);
    theNumberFunction= new XPnumber(aContext);
  }

  public IXPvalue call(ArrayList theArguments)
  {
    if(theArguments.size()!=1)
      throw new RuntimeException("sum() called with wrong number of arguments!");
    IXPvalue aValue = (IXPvalue)theArguments.get(0);
    if(aValue.getTypeId()!=IXPnodeSetValue.ID)
      throw new RuntimeException("sum() called with wrong type of arguement!");
    IXPnodeSetValue aNodeSetValue =(IXPnodeSetValue)aValue;
    return call(aNodeSetValue);
  }
  
  public IXPnumberValue call(IXPnodeSetValue aValue)
  {
    IXPnumberValue theResult=new XPnumberValue(new Integer(0));
    ArrayList theNodes = aValue.getValue();
    Iterator anIter = theNodes.iterator();
    while (anIter.hasNext()) 
      {
	Node aNode = (Node) anIter.next();
	String aNodeAsString = theStringFunction.nodeToString(aNode);
	Number aNodeAsNumber = theNumberFunction.stringToNumber(aNodeAsString);
	theResult.add(aNodeAsNumber);
      } // end of while ()
    
    return theResult;
  }
}
