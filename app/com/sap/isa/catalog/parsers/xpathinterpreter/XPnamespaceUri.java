package com.sap.isa.catalog.parsers.xpathinterpreter;

import java.util.ArrayList;

import org.w3c.dom.Attr;
import org.w3c.dom.Element;
import org.w3c.dom.Node;

/**
 * Title:        WebXSLT
 * Description:
 * Copyright:    Copyright (c) 2001
 * Company:
 * @version 1.0
 */

public class XPnamespaceUri extends XPathFunction
{
  public XPnamespaceUri(XPathContext aContext)
  {
    super(aContext);
  }

  public IXPvalue call(ArrayList theArguments)
  {
    IXPstringValue theResult = null;
    if(theArguments.size()>1)
      throw new RuntimeException(
				 "Method namespace-uri() was called with the wrong number of arguments!");
    if(theArguments.size()==0)
      {
	theResult = call();
      }
    else
      {
	IXPvalue aValue = (IXPvalue)theArguments.get(0);
	if(aValue.getTypeId()!=IXPnodeSetValue.ID)
	  throw new RuntimeException("Method namespace-uri() was called with the wrong argument type!");
	IXPnodeSetValue aNodeSetValue = (IXPnodeSetValue)aValue;
	theResult = call(aNodeSetValue);
      }
    return theResult;
  }

  public IXPstringValue call()
  {
    Node theCurrentNode = this.theContext.getCurrentNode();
    return call(theCurrentNode);
  }

  public IXPstringValue call(IXPnodeSetValue aNodeSet)
  {
    if(aNodeSet.getValue().size()==0)
      return new XPstringValue("");
    Node aNode = (Node)aNodeSet.getValue().get(0);
    return call(aNode);
  }

  private IXPstringValue call(Node aNode)
  {
    short theTypeId = aNode.getNodeType();
    String aValue = "";

    switch (theTypeId){
    case Node.ATTRIBUTE_NODE :
      Attr anAttr =(Attr)aNode;
      aValue = findNamespaceUri(anAttr);
      break;
    case Node.ELEMENT_NODE :
      Element anElement = (Element)aNode;
      aValue = findNamespaceUri(anElement);
      break;
    case Node.PROCESSING_INSTRUCTION_NODE :
    case Node.TEXT_NODE :
    case Node.COMMENT_NODE :
    case Node.DOCUMENT_NODE :
    default :
    }  
    return new XPstringValue(aValue);
  }

  private String findNamespaceUri(Element anElement)
  {
    String thePrefix=null;
    // hack due to incomplete an inconsisten DOM implementation
    String aFullNodeName = anElement.getNodeName();
    int theIndexOfTheColon = aFullNodeName.indexOf(":");
    if(theIndexOfTheColon!=-1)
      thePrefix = aFullNodeName.substring(0,theIndexOfTheColon);
    else
      thePrefix = "";
    //String thePrefix = anElement.getPrefix();
    return findNamespaceUriForPrefix(anElement,thePrefix);
  }

  private String findNamespaceUri(Attr anAttr)
  {
    String thePrefix=null;
    // hack due to incomplete an inconsisten DOM implementation
    String aFullNodeName = anAttr.getNodeName();
    int theIndexOfTheColon = aFullNodeName.indexOf(":");
    if(theIndexOfTheColon!=-1)
      thePrefix = aFullNodeName.substring(0,theIndexOfTheColon);
    else
      thePrefix = "";
    //String thePrefix = anAttr.getPrefix();
    Element theOwner = anAttr.getOwnerElement();
    return findNamespaceUriForPrefix(theOwner,thePrefix);
  }

  private String findNamespaceUriForPrefix(Element anElement, String thePrefix)
  {
    String theResult ="";
    //check element
    String theNamespaceUri = anElement.getAttribute((thePrefix.equals("")? "xmlns" : "xmlns:"+thePrefix));
    if(theNamespaceUri.intern()!="".intern())
      return theNamespaceUri;
    //ceck parent
    Node aParent = anElement.getParentNode();
    if(aParent!=null)
      {
	if(aParent.getNodeType() == Node.ELEMENT_NODE)
	  return findNamespaceUriForPrefix((Element)aParent,thePrefix);
      }
    return theResult;
  }
}
