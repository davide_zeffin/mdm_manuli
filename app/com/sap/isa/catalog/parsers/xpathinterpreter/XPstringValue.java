package com.sap.isa.catalog.parsers.xpathinterpreter;

/**
 * Title:        WebXSLT
 * Description:
 * Copyright:    Copyright (c) 2001
 * Company:
 * @version 1.0
 */

public class XPstringValue implements IXPstringValue
{
  private String theString;

  public XPstringValue(String theString)
  {
    this.theString=theString;
  }

  public int getTypeId()
  {
    return IXPstringValue.ID;
  }

  public String getValue()
  {
    return theString;
  }

  public String toString()
  {
    return theString;
  }
}

