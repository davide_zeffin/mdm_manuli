package com.sap.isa.catalog.parsers.xpathinterpreter;
/**
 * Title:
 * Description:
 * Copyright:    Copyright (c) 2001
 * Company:
 * @version 1.0
 */

public class XPathPrettyPrinterParserVisitor
implements XPathParserVisitor
{
    public XPathPrettyPrinterParserVisitor()
	{
    }

	public static void main(String[] args)
	{
        XPathPrettyPrinterParserVisitor XPathPrettyPrinterParserVisitor1 =
			new XPathPrettyPrinterParserVisitor();
    }

    public Object visit(SimpleNode node, Object data)
	{
		String prefixes = (String) data;
		System.out.println(node.toString(prefixes+" "));
		node.childrenAccept(this,prefixes+" ");
		return null;
    }

    public Object visit(ASTNCName node, Object data)
	{
		String prefixes = (String) data;
		System.out.println(node.toString(prefixes+" ")+": " + node.getNCName());
		node.childrenAccept(this,prefixes+" ");
		return null;
    }

    public Object visit(ASTNCName_Without_NodeType node, Object data)
	{
		String prefixes = (String) data;
		System.out.println(node.toString(prefixes+" ")+": " + node.getNCFunctionName());
		node.childrenAccept(this,prefixes+" ");
		return null;
    }

    public Object visit(ASTQName node, Object data)
	{
		String prefixes = (String) data;
		System.out.println(node.toString(prefixes+" ")+": " + node.getQName());
		node.childrenAccept(this,prefixes+" ");
		return null;
    }

    public Object visit(ASTQName_Without_NodeType node, Object data)
	{
		String prefixes = (String) data;
		System.out.println(node.toString(prefixes+" ")+": " + node.getQFunctionName());
		node.childrenAccept(this,prefixes+" ");
		return null;
    }

    public Object visit(ASTPattern node, Object data)
	{
		String prefixes = (String) data;
		System.out.println(node.toString(prefixes+" "));
		node.childrenAccept(this,prefixes+" ");
		return null;
    }

    public Object visit(ASTLocationPathPattern node, Object data)
	{
		String prefixes = (String) data;
		System.out.println(node.toString(prefixes+" "));
		node.childrenAccept(this,prefixes+" ");
		return null;
    }

    public Object visit(ASTIdKeyPattern node, Object data)
	{
		String prefixes = (String) data;
		System.out.println(node.toString(prefixes+" "));
		node.childrenAccept(this,prefixes+" ");
		return null;
    }

    public Object visit(ASTRelativePathPattern node, Object data)
	{
		String prefixes = (String) data;
		System.out.println(node.toString(prefixes+" "));
		node.childrenAccept(this,prefixes+" ");
		return null;
    }

    public Object visit(ASTStepPattern node, Object data)
	{
		String prefixes = (String) data;
		System.out.println(node.toString(prefixes+" "));
		node.childrenAccept(this,prefixes+" ");
		return null;
    }

    public Object visit(ASTXPath node, Object data)
	{
		String prefixes = (String) data;
		System.out.println(node.toString(prefixes+" "));
		node.childrenAccept(this,prefixes+" ");
		return null;
    }

    public Object visit(ASTXPathExpr node, Object data)
	{
		String prefixes = (String) data;
		System.out.println(node.toString(prefixes+" "));
		node.childrenAccept(this,prefixes+" ");
		return null;
    }

    public Object visit(ASTLocationPath node, Object data)
	{
		String prefixes = (String) data;
		System.out.println(node.toString(prefixes+" "));
		node.childrenAccept(this,prefixes+" ");
		return null;
    }

    public Object visit(ASTAbsoluteLocationPath node, Object data)
	{
		String prefixes = (String) data;
		System.out.println(node.toString(prefixes+" "));
		node.childrenAccept(this,prefixes+" ");
		return null;
    }

    public Object visit(ASTRelativeLocationPath node, Object data)
	{
		String prefixes = (String) data;
		System.out.println(node.toString(prefixes+" "));
		node.childrenAccept(this,prefixes+" ");
		return null;
    }

    public Object visit(ASTStep node, Object data)
	{
		String prefixes = (String) data;
		System.out.println(node.toString(prefixes+" "));
		node.childrenAccept(this,prefixes+" ");
		return null;
    }

    public Object visit(ASTAxisSpecifier node, Object data)
	{
		String prefixes = (String) data;
		System.out.println(node.toString(prefixes+" "));
		node.childrenAccept(this,prefixes+" ");
		return null;
    }

    public Object visit(ASTAxisName node, Object data)
	{
		String prefixes = (String) data;
		System.out.println(node.toString(prefixes+" ")+": "+node.getName());
		node.childrenAccept(this,prefixes+" ");
		return null;
    }

    public Object visit(ASTNodeTest node, Object data)
	{
		String prefixes = (String) data;
		System.out.println(node.toString(prefixes+" "));
		node.childrenAccept(this,prefixes+" ");
		return null;
    }

    public Object visit(ASTPredicate node, Object data)
	{
		String prefixes = (String) data;
		System.out.println(node.toString(prefixes+" "));
		node.childrenAccept(this,prefixes+" ");
		return null;
    }

    public Object visit(ASTPredicateExpr node, Object data)
	{
		String prefixes = (String) data;
		System.out.println(node.toString(prefixes+" "));
		node.childrenAccept(this,prefixes+" ");
		return null;
    }

    public Object visit(ASTAbbreviatedAbsoluteLocationPath node, Object data)
	{
		String prefixes = (String) data;
		System.out.println(node.toString(prefixes+" "));
		node.childrenAccept(this,prefixes+" ");
		return null;
    }

    public Object visit(ASTAbbreviatedRelativeLocationPath node, Object data)
	{
		String prefixes = (String) data;
		System.out.println(node.toString(prefixes+" "));
		node.childrenAccept(this,prefixes+" ");
		return null;
    }

    public Object visit(ASTAbbreviatedStep node, Object data)
	{
		String prefixes = (String) data;
		System.out.println(node.toString(prefixes+" ")+": "+
			(node.isSelfNode()?"self::node()":"parent::node()"));
		node.childrenAccept(this,prefixes+" ");
		return null;
    }

    public Object visit(ASTAbbreviatedAxisSpecifier node, Object data)
	{
		String prefixes = (String) data;
		System.out.println(node.toString(prefixes+" ")+": '"+node.getToken()+"'");
		node.childrenAccept(this,prefixes+" ");
		return null;
    }

    public Object visit(ASTExpr node, Object data)
	{
		String prefixes = (String) data;
		System.out.println(node.toString(prefixes+" "));
		node.childrenAccept(this,prefixes+" ");
		return null;
    }

    public Object visit(ASTPrimaryExpr aPrimaryExpr, Object data)
	{
		String prefixes = (String) data;
		int theType = aPrimaryExpr.getType();

		switch (theType){
			case ASTPrimaryExpr.NUMBER:
			//We are a number
			System.out.println(aPrimaryExpr.toString(prefixes+" ") + " Number : " + aPrimaryExpr.getToken());
			break;
			case ASTPrimaryExpr.FUNCTION_CALL:
			//let the children do their work
			System.out.println(aPrimaryExpr.toString(prefixes+" "));
	    	aPrimaryExpr.childrenAccept(this,prefixes+" ");
			break;
			case ASTPrimaryExpr.LITERAL:
			//We are a number
			System.out.println(aPrimaryExpr.toString(prefixes+" ") + " Literal : " + aPrimaryExpr.getToken());
			break;
		    case ASTPrimaryExpr.EXPR:
			//let the children do their work
			System.out.println(aPrimaryExpr.toString(prefixes+" "));
	    	aPrimaryExpr.childrenAccept(this,prefixes+" ");
		    break;
		    case ASTPrimaryExpr.VARIABLE_REFERENCE:
			//let the children do their work
			System.out.println(aPrimaryExpr.toString(prefixes+" "));
	    	aPrimaryExpr.childrenAccept(this,prefixes+" ");
		    break;
		}

		return null;
    }

    public Object visit(ASTFunctionCall node, Object data)
	{
		String prefixes = (String) data;
		System.out.println(node.toString(prefixes+" "));
		node.childrenAccept(this,prefixes+" ");
		return null;
    }

    public Object visit(ASTArgument node, Object data)
	{
		String prefixes = (String) data;
		System.out.println(node.toString(prefixes+" "));
		node.childrenAccept(this,prefixes+" ");
		return null;
    }

    public Object visit(ASTUnionExpr node, Object data)
	{
		String prefixes = (String) data;
		System.out.println(node.toString(prefixes+" "));
		node.childrenAccept(this,prefixes+" ");
		return null;
    }

    public Object visit(ASTPathExpr node, Object data)
	{
		String prefixes = (String) data;
		System.out.println(node.toString(prefixes+" "));
		node.childrenAccept(this,prefixes+" ");
		return null;
    }

    public Object visit(ASTFilterExpr node, Object data)
	{
		String prefixes = (String) data;
		System.out.println(node.toString(prefixes+" "));
		node.childrenAccept(this,prefixes+" ");
		return null;
    }

    public Object visit(ASTOrExpr node, Object data)
	{
		String prefixes = (String) data;
		System.out.println(node.toString(prefixes+" "));
		node.childrenAccept(this,prefixes+" ");
		return null;
    }

    public Object visit(ASTAndExpr node, Object data)
	{
		String prefixes = (String) data;
		System.out.println(node.toString(prefixes+" "));
		node.childrenAccept(this,prefixes+" ");
		return null;
    }

    public Object visit(ASTEqualityExpr node, Object data)
	{
		String prefixes = (String) data;
		int theNumberOfComparisons = node.jjtGetNumChildren()-1;
		StringBuffer theComparators = new StringBuffer();
		for(int i = 0; i < theNumberOfComparisons; i++)
			theComparators.append(node.getComparatorToken(i)).append(" ");
		System.out.println(node.toString(prefixes+" ") +
			" Comparators: " + theComparators);
		node.childrenAccept(this,prefixes+" ");
		return null;
    }

    public Object visit(ASTRelationalExpr node, Object data)
	{
		String prefixes = (String) data;
		int theNumberOfComparisons = node.jjtGetNumChildren()-1;
		StringBuffer theComparators = new StringBuffer();
		for(int i = 0; i < theNumberOfComparisons; i++)
			theComparators.append(node.getComparatorToken(i)).append(" ");
		System.out.println(node.toString(prefixes+" ") +
			" Comparators: " + theComparators);
		node.childrenAccept(this,prefixes+" ");
		return null;
    }

    public Object visit(ASTAdditiveExpr node, Object data)
	{
		String prefixes = (String) data;
		int theNumberOfAddends = node.jjtGetNumChildren()-1;
		StringBuffer theOperators = new StringBuffer();
		for(int i = 0; i < theNumberOfAddends; i++)
			theOperators.append(node.getOperatorToken(i)).append(" ");
		System.out.println(node.toString(prefixes+" ") +
			" Operators: " + theOperators);
		node.childrenAccept(this,prefixes+" ");
		return null;
    }

    public Object visit(ASTMultiplicativeExpr node, Object data)
	{
		String prefixes = (String) data;
		int theNumberOfMultipliers = node.jjtGetNumChildren()-1;
		StringBuffer theOperators = new StringBuffer();
		for(int i = 0; i < theNumberOfMultipliers; i++)
			theOperators.append(node.getOperatorToken(i)).append(" ");
		System.out.println(node.toString(prefixes+" ") +
			" Operators: " + theOperators);
		node.childrenAccept(this,prefixes+" ");
		return null;
    }

    public Object visit(ASTUnaryExpr node, Object data)
	{
		String prefixes = (String) data;
		System.out.println(node.toString(prefixes+" ") + (node.isMinus()? " : -" :""));
		node.childrenAccept(this,prefixes+" ");
		return null;
    }

    public Object visit(ASTFunctionName node, Object data)
	{
		String prefixes = (String) data;
		System.out.println(node.toString(prefixes+" ") + ": " + node.getFunctionName());
		node.childrenAccept(this,prefixes+" ");
		return null;
    }

    public Object visit(ASTVariableReference node, Object data)
	{
		String prefixes = (String) data;
		System.out.println(node.toString(prefixes+" ") +": $" + node.getVariableName());
		node.childrenAccept(this,prefixes+" ");
		return null;
    }

    public Object visit(ASTNameTest node, Object data)
	{
		String prefixes = (String) data;
		System.out.println(node.toString(prefixes+" ")+": " + node.getNameTest());
		node.childrenAccept(this,prefixes+" ");
		return null;
    }

    public Object visit(ASTNodeType node, Object data)
	{
		String prefixes = (String) data;
		System.out.println(node.toString(prefixes+" "));
		node.childrenAccept(this,prefixes+" ");
		return null;
    }
}
