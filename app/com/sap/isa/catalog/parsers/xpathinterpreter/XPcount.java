package com.sap.isa.catalog.parsers.xpathinterpreter;

import java.util.ArrayList;

/**
 * Title:        WebXSLT
 * Description:
 * Copyright:    Copyright (c) 2001
 * Company:
 * @version 1.0
 */

public class XPcount extends XPathFunction
{
    public XPcount(XPathContext aContext)
    {
        super(aContext);
    }

    public IXPvalue call(ArrayList theArguments)
    {
        if(theArguments.size()!=1)
            throw new RuntimeException("Wrong number of arguments for count!");
        IXPvalue theValue = (IXPvalue)theArguments.get(0);
        if(theValue.getTypeId()!=IXPnodeSetValue.ID)
            throw new RuntimeException("Wrong type of argument for count!");
        IXPnodeSetValue theNodeSet = (IXPnodeSetValue)theValue;
        return this.call(theNodeSet);
    }

    public IXPnumberValue call(IXPnodeSetValue theNodeSet)
    {
        ArrayList theListOfNodes = theNodeSet.getValue();
        int theSize = theListOfNodes.size();
        return new XPnumberValue(new Integer(theSize));
    }
}
