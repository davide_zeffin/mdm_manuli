package com.sap.isa.catalog.parsers.xpathinterpreter;

/**
 * Title:        WebXSLT
 * Description:
 * Copyright:    Copyright (c) 2001
 * Company:
 * @version 1.0
 */

public interface IXPstringValue extends IXPvalue
{
	final public static int ID = 3;

	String getValue();
}
