package com.sap.isa.catalog.parsers.xpathinterpreter;

import java.util.ArrayList;
import java.util.Iterator;

/**
 * Title:        WebXSLT
 * Description:
 * Copyright:    Copyright (c) 2001
 * Company:
 * @version 1.0
 */

public class XPconcat extends XPathFunction
{
	XPstring theStringFunction;

    public XPconcat(XPathContext aContext)
	{
		super(aContext);
		theStringFunction = new XPstring(aContext);
    }

    public IXPvalue call(ArrayList theArguments)
	{
		IXPstringValue[] theListOfStrings =
			new IXPstringValue[theArguments.size()];
		Iterator stringValueIter = theArguments.iterator();
		int counter=0;
		while(stringValueIter.hasNext())
		{
			IXPvalue aValue = (IXPvalue)stringValueIter.next();
			IXPstringValue aStringValue = null;
		    if(aValue.getTypeId()!=IXPstringValue.ID)
				aStringValue = theStringFunction.call(aValue);
			else
				aStringValue = (IXPstringValue)aValue;
			theListOfStrings[counter++]=aStringValue;
		}
		return call(theListOfStrings);
    }

	public IXPstringValue call(IXPstringValue[] aListOfString)
	{
		StringBuffer aBuffer = new StringBuffer();
		for(int i = 0; i < aListOfString.length; i++)
		{
			aBuffer.append(aListOfString[i].getValue());
		}
		return new XPstringValue(aBuffer.toString());
	}
}
