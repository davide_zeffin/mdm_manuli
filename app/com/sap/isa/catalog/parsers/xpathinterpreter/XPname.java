package com.sap.isa.catalog.parsers.xpathinterpreter;

import java.util.ArrayList;

import org.w3c.dom.Attr;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.ProcessingInstruction;

/**
 * Title:        WebXSLT
 * Description:
 * Copyright:    Copyright (c) 2001
 * Company:
 * @version 1.0
 */

public class XPname extends XPathFunction
{
  public XPname(XPathContext aContext)
  {
    super(aContext);
  }

  public IXPvalue call(ArrayList theArguments)
  {
    IXPstringValue theResult = null;
    if(theArguments.size()>1)
      throw new RuntimeException(
				 "Method name() was called with the wrong number of arguments!");
    if(theArguments.size()==0)
      {
	theResult = call();
      }
    else
      {
	IXPvalue aValue = (IXPvalue)theArguments.get(0);
	if(aValue.getTypeId()!=IXPnodeSetValue.ID)
	  throw new RuntimeException("Method name() was called with the wrong argument type!");
	IXPnodeSetValue aNodeSetValue = (IXPnodeSetValue)aValue;
	theResult = call(aNodeSetValue);
      }
    return theResult;
  }

  public IXPstringValue call()
  {
    Node theCurrentNode = this.theContext.getCurrentNode();
    return call(theCurrentNode);
  }

  public IXPstringValue call(IXPnodeSetValue aNodeSet)
  {
    if(aNodeSet.getValue().size()==0)
      return new XPstringValue("");
    Node aNode = (Node)aNodeSet.getValue().get(0);
    return call(aNode);
  }

  private IXPstringValue call(Node aNode)
  {
    short theTypeId = aNode.getNodeType();
    String aValue = "";

    switch (theTypeId){
    case Node.ATTRIBUTE_NODE :
      Attr anAttr =(Attr)aNode;
      aValue = anAttr.getNodeName();
      break;
    case Node.PROCESSING_INSTRUCTION_NODE :
      ProcessingInstruction aPI = (ProcessingInstruction)aNode;
      aValue = aPI.getTarget();
      break;
    case Node.ELEMENT_NODE :
      Element anElement = (Element)aNode;
      aValue = anElement.getNodeName();
      break;
    case Node.TEXT_NODE :
    case Node.COMMENT_NODE :
    case Node.DOCUMENT_NODE :
    default :
    }  
    return new XPstringValue(aValue);
  }
}
