/* Generated By:JJTree: Do not edit this line. D:\JAVA\jakartaTomcat323\webapps\WebXSLT\WEB-INF\generated\com\sap\isa\catalog\parsers\xpathinterpreter\\XPathParserVisitor.java */

package com.sap.isa.catalog.parsers.xpathinterpreter;

public interface XPathParserVisitor
{
  public Object visit(SimpleNode node, Object data);
  public Object visit(ASTNCName node, Object data);
  public Object visit(ASTNCName_Without_NodeType node, Object data);
  public Object visit(ASTQName node, Object data);
  public Object visit(ASTQName_Without_NodeType node, Object data);
  public Object visit(ASTPattern node, Object data);
  public Object visit(ASTLocationPathPattern node, Object data);
  public Object visit(ASTIdKeyPattern node, Object data);
  public Object visit(ASTRelativePathPattern node, Object data);
  public Object visit(ASTStepPattern node, Object data);
  public Object visit(ASTXPath node, Object data);
  public Object visit(ASTLocationPath node, Object data);
  public Object visit(ASTAbsoluteLocationPath node, Object data);
  public Object visit(ASTRelativeLocationPath node, Object data);
  public Object visit(ASTStep node, Object data);
  public Object visit(ASTAxisSpecifier node, Object data);
  public Object visit(ASTAxisName node, Object data);
  public Object visit(ASTNodeTest node, Object data);
  public Object visit(ASTPredicate node, Object data);
  public Object visit(ASTPredicateExpr node, Object data);
  public Object visit(ASTAbbreviatedAbsoluteLocationPath node, Object data);
  public Object visit(ASTAbbreviatedRelativeLocationPath node, Object data);
  public Object visit(ASTAbbreviatedStep node, Object data);
  public Object visit(ASTAbbreviatedAxisSpecifier node, Object data);
  public Object visit(ASTXPathExpr node, Object data);
  public Object visit(ASTExpr node, Object data);
  public Object visit(ASTPrimaryExpr node, Object data);
  public Object visit(ASTFunctionCall node, Object data);
  public Object visit(ASTArgument node, Object data);
  public Object visit(ASTUnionExpr node, Object data);
  public Object visit(ASTPathExpr node, Object data);
  public Object visit(ASTFilterExpr node, Object data);
  public Object visit(ASTOrExpr node, Object data);
  public Object visit(ASTAndExpr node, Object data);
  public Object visit(ASTEqualityExpr node, Object data);
  public Object visit(ASTRelationalExpr node, Object data);
  public Object visit(ASTAdditiveExpr node, Object data);
  public Object visit(ASTMultiplicativeExpr node, Object data);
  public Object visit(ASTUnaryExpr node, Object data);
  public Object visit(ASTFunctionName node, Object data);
  public Object visit(ASTVariableReference node, Object data);
  public Object visit(ASTNameTest node, Object data);
  public Object visit(ASTNodeType node, Object data);
}
