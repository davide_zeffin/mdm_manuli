package com.sap.isa.catalog.parsers.xpathinterpreter;

/**
 * Title:        WebXSLT
 * Description:
 * Copyright:    Copyright (c) 2001
 * Company:
 * @version 1.0
 */

import java.util.ArrayList;
import java.util.Iterator;

import org.w3c.dom.Node;

public class XPmax extends XPnumber
{
	public XPmax(XPathContext aContext)
	{
		super(aContext);
	}

    public IXPnumberValue call(IXPvalue anArgument)
	{
		XPstring theStringFunction = new XPstring(theContext);

		Number theResult = new Double(0.0);

		IXPnumberValue theResultValue = null;

	    int typeId = anArgument.getTypeId();

		switch(typeId){

		case IXPnodeSetValue.ID:
		ArrayList theNodes = ((IXPnodeSetValue)anArgument).getValue();
		Iterator aNodeIter = theNodes.iterator();
		Number max = null;
		while(aNodeIter.hasNext())
		{
			Node aNode = (Node)aNodeIter.next();
			String aNodeAsString = XPstring.nodeToString(aNode);
			Number aNodeAsNumber = XPnumber.stringToNumber(aNodeAsString);
			if(max==null)
				max = aNodeAsNumber;
			else
			{
				int result = XPnumberValue.compareNumbers(max,aNodeAsNumber);
				if(result>0)
					;
				else
					max = aNodeAsNumber;
			}
		}
		theResultValue = new XPnumberValue(max);
		break;

		case IXPbooleanValue.ID:

		case IXPnumberValue.ID:

		case IXPstringValue.ID:

		case IXPtreeFragmentValue.ID:

		default:
			theResultValue = super.call(anArgument);
		}

		return theResultValue;
    }
}
