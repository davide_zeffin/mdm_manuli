/* Generated By:JJTree: Do not edit this line. ASTAbbreviatedRelativeLocationPath.java */
package com.sap.isa.catalog.parsers.xpathinterpreter;
public class ASTAbbreviatedRelativeLocationPath extends SimpleNode
{
	public ASTAbbreviatedRelativeLocationPath(int id)
	{
        super(id);
    }

    public ASTAbbreviatedRelativeLocationPath(XPathParser p, int id)
	{
        super(p, id);
    }

    /** Accept the visitor. **/
    public Object jjtAccept(XPathParserVisitor visitor, Object data)
	{
        return visitor.visit(this, data);
    }
}
