package com.sap.isa.catalog.parsers.xpathinterpreter;

/**
 * Title:        WebXSLT
 * Description:
 * Copyright:    Copyright (c) 2001
 * Company:
 * @version 1.0
 */


public interface IXPnumberValue
    extends IXPvalue,
Comparable
{
    final public static int ID = 2;

    void add(Number aNumber);

    void add(int amultiplier);

    void add(double amultiplier);

    void multiplyBy(Number aNumber);

    void multiplyBy(int amultiplier);

    void multiplyBy(double amultiplier);
    
    boolean isNaN();
    
    int compareTo(Object anObject) throws ClassCastException;

    Number getValue();
}
