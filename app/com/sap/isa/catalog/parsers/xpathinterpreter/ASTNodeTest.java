/* Generated By:JJTree: Do not edit this line. ASTNodeTest.java */
package com.sap.isa.catalog.parsers.xpathinterpreter;
public class ASTNodeTest extends SimpleNode
{
	private String thePIName = "";

	public ASTNodeTest(int id)
	{
        super(id);
    }

    public ASTNodeTest(XPathParser p, int id)
	{
        super(p, id);
    }

	public void setPIName(String thePIName)
	{
		this.thePIName = thePIName;
	}

	public String getPIName()
	{
		return this.thePIName;
	}

    /** Accept the visitor. **/
    public Object jjtAccept(XPathParserVisitor visitor, Object data)
	{
        return visitor.visit(this, data);
    }
}
