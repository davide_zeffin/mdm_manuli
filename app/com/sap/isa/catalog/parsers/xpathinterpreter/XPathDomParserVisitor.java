package com.sap.isa.catalog.parsers.xpathinterpreter;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Set;

import org.w3c.dom.Attr;
import org.w3c.dom.Document;
import org.w3c.dom.DocumentFragment;
import org.w3c.dom.Element;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

/**
 * Title:
 * Description:
 * Copyright:    Copyright (c) 2001
 * Company:
 * @version 1.0
 */

public class XPathDomParserVisitor implements XPathParserVisitor
{
    private Document theDocument;
    private ASTXPath theXPathQuery;
    private ASTXPathExpr theXPathExpr;
    private ArrayList theResultSet;
    private HashMap theNamespaceNodeAxes = new HashMap();

    public XPathDomParserVisitor(Document theDocument, ASTXPath anXPathQuery)
    {
        this.theDocument = theDocument;
        this.theXPathQuery = anXPathQuery;
    }

    public XPathDomParserVisitor(Document theDocument, ASTXPathExpr anXPathExpr)
    {
        this.theDocument = theDocument;
        this.theXPathExpr = anXPathExpr;
    }

    public ArrayList executeQuery()
    {
        XPathContext aContext = new XPathContext(new ArrayList(), -1);

        theXPathQuery.jjtAccept(this,aContext);

        return aContext.getContext();
    }

        /**
         * Evaluate the XPathExpression
         */
    public IXPvalue evaluate(XPathContext aContext)
    {
        theXPathExpr.jjtAccept(this,aContext);

        return aContext.getValue();
    }

        /**
         * Starting point for Node set queryies:
         */
    public Object visit(ASTXPath node, Object data)
    {
            //Go to the LocationPath
        node.jjtGetChild(0).jjtAccept(this,data);

        return data;
    }

        /**
         * Pseudo production in the jjc-grammar file to have a starting point
         * to evaluate a general XPath expression.
         *
         * [B] XPathExpr     ::= Expr EOF
         *
         * In the jcc-gramar file:
         *
         * ASTXPathExpr XPathExpr() #XPathExpr :
         * {}
         * {
         *      Expr()
         *        <EOF>
         *        { return jjtThis; }
         * }
         *
         * Starting point for a general XPath expression:
         */
    public Object visit(ASTXPathExpr node, Object data)
    {
        XPathContext aContext = (XPathContext)data;

            //Go to the Expr and try to figure what we are
        node.jjtGetChild(0).jjtAccept(this,aContext);

        return aContext;
    }

        /**
         * A Location path might be relative (starts at current node) or absolut
         * (starts at root).
         *
         * XPath production:
         *
         * [1] LocationPath             ::= RelativeLocationPath
         *                                    | AbsoluteLocationPath
         *
         * Used in :
         *
         * [A] XPath (pseudo production)
         *
         * In the jjc-grammar this productin is realized as:
         *
         * void LocationPath() #LocationPath :
         * {}
         * {
         *        RelativeLocationPath()
         *        |   AbsoluteLocationPath()
         * }
         *
         * To figure aout what to do we have to go down the tree.
         *
         * @return theFinal XPathContext that contains the result of the selection
         */
    public Object visit(ASTLocationPath node, Object data)
    {
        XPathContext aContext = (XPathContext)data;

            // all we know is that we (XPathExpression) are of type node set
        if(aContext.getValue().getTypeId() != IXPnodeSetValue.ID)
            aContext.setNodeSetValue(aContext.getContext());

        node.childrenAccept(this,aContext);

        return aContext;
    }

        /**
         * The absolut location path might start(and stop) at the root as context or
         * select all nodes in the document and take them as context.
         * XPath production:
         *
         *    [2] AbsoluteLocationPath     ::= '/' RelativeLocationPath?
         *                                | AbbreviatedAbsoluteLocationPath
         *
         * Used in production:
         *
         *  [1] LocationPath
         *
         * Right now the root (not the document element) is the context.
         *
         *
         * In the jjc-grammar this production is realized as:
         *
         * void AbsoluteLocationPath() #AbsoluteLocationPath :
         * {}
         * {
         *      AbbreviatedAbsoluteLocationPath()
         *      | <SLASH> (   LOOKAHEAD(RelativeLocationPath())
         *                      RelativeLocationPath()
         *                  )?
         * }
         *
         */
    public Object visit(ASTAbsoluteLocationPath node, Object data)
    {
        XPathContext aContext = (XPathContext) data;

        aContext.setFirstStepVisited(true);
        aContext.setRelativeLocationPath(false);

        Node theCurrentNode  = theDocument;
        ArrayList theContextList = new ArrayList();
        theContextList.add(theCurrentNode);

        aContext.setContext(theContextList);
        aContext.setCurrentNode(0);

            // if we got children let them do their work
        node.childrenAccept(this,aContext);

        return aContext;
    }

        /**
         * The relative location path starts at the current context and makes at
         * least one additional (filtering step) step
         *
         * The XPath Production :
         *    [3] RelativeLocationPath     ::= Step
         *                                | RelativeLocationPath '/' Step
         *                                | AbbreviatedRelativeLocationPath
         *
         *  was realized in a jj-grammar file as:
         *
         *  void RelativeLocationPath() #RelativeLocationPath :
         *  {}
         *  {
         *    Step() (
         *        LOOKAHEAD((<SLASH> | <SLASHSLASH> ) RelativeLocationPath())
         *        (<SLASH> | <SLASHSLASH> ) RelativeLocationPath()
         *        )?
         *    }
         *
         **/
    public Object visit(ASTRelativeLocationPath node, Object data)
    {
        XPathContext aContext = (XPathContext) data;

        ASTStep aStep = (ASTStep) node.jjtGetChild(0);

        if(node.jjtGetNumChildren()>1)
                //We are actually of the form Step() '/' | '//' RelativeLocationPath()
        {
                //Let the step do its work
            XPathContext aFilteredContext =
                (XPathContext)aStep.jjtAccept(this,aContext);

            if(node.isAbbreviated())
            {
                ArrayList allNodesDownTheChildAxis = new ArrayList();

                addSelfAndAllChilds(allNodesDownTheChildAxis,
                                    aFilteredContext.getContext());

                aFilteredContext.setContext(allNodesDownTheChildAxis);

                    //Now continue with your path
            }

                //Continue with the rest

            ASTRelativeLocationPath aRelPath =
                (ASTRelativeLocationPath) node.jjtGetChild(1);
            return aRelPath.jjtAccept(this,aFilteredContext);

        }
        else
                // we are actually the last in a sequence of steps
        {
            return aStep.jjtAccept(this,aContext);
        }
    }

        /**
         * Technically this is /descendant-or-self::node()/RelativeLocationPath
         * So the starting point for the child (one relative LocationPath)
         * is the the list of all nodes (text, elements, comments, pi's)
         * in the document in document order.
         *
         * XPath production:
         *
         * [10] AbbreviatedAbsoluteLocationPath ::= '//' RelativeLocationPath
         *
         * Used in production:
         *
         * [2] AbsoluteLocationPath
         *
         * Was realized in the jjc-grammar file as:
         *
         * void AbbreviatedAbsoluteLocationPath() #AbbreviatedAbsoluteLocationPath :
         * {}
         * {
         *      <SLASHSLASH> RelativeLocationPath()
         * }
         *
         *
         * */
    public Object visit(ASTAbbreviatedAbsoluteLocationPath node, Object data)
    {
        XPathContext aContext = (XPathContext) data;

        aContext.setFirstStepVisited(true);
        aContext.setRelativeLocationPath(false);

        ArrayList allNodesInDocumentOrder = getAllNodes();
        aContext.setContext(allNodesInDocumentOrder);
        node.childrenAccept(this,aContext);
        return aContext;
    }

    public Object visit(SimpleNode node, Object data)
    {
            /**@todo: Implement this XPathParserVisitor method*/
        throw new java.lang.UnsupportedOperationException("Method visit() not yet implemented.");
    }

    public Object visit(ASTNCName node, Object data)
    {
            /**@todo: Implement this XPathParserVisitor method*/
        throw new java.lang.UnsupportedOperationException("Method visit() not yet implemented.");
    }

    public Object visit(ASTNCName_Without_NodeType node, Object data)
    {
            /**@todo: Implement this XPathParserVisitor method*/
        throw new java.lang.UnsupportedOperationException("Method visit() not yet implemented.");
    }

    public Object visit(ASTQName node, Object data)
    {
            /**@todo: Implement this XPathParserVisitor method*/
        throw new java.lang.UnsupportedOperationException("Method visit() not yet implemented.");
    }

    public Object visit(ASTQName_Without_NodeType node, Object data)
    {
            /**@todo: Implement this XPathParserVisitor method*/
        throw new java.lang.UnsupportedOperationException("Method visit() not yet implemented.");
    }

    public Object visit(ASTPattern node, Object data)
    {
            /**@todo: Implement this XPathParserVisitor method*/
        throw new java.lang.UnsupportedOperationException("Method visit() not yet implemented.");
    }

    public Object visit(ASTLocationPathPattern node, Object data)
    {
            /**@todo: Implement this XPathParserVisitor method*/
        throw new java.lang.UnsupportedOperationException("Method visit() not yet implemented.");
    }

    public Object visit(ASTIdKeyPattern node, Object data)
    {
            /**@todo: Implement this XPathParserVisitor method*/
        throw new java.lang.UnsupportedOperationException("Method visit() not yet implemented.");
    }

    public Object visit(ASTRelativePathPattern node, Object data)
    {
            /**@todo: Implement this XPathParserVisitor method*/
        throw new java.lang.UnsupportedOperationException("Method visit() not yet implemented.");
    }

    public Object visit(ASTStepPattern node, Object data)
    {
            /**@todo: Implement this XPathParserVisitor method*/
        throw new java.lang.UnsupportedOperationException("Method visit() not yet implemented.");
    }

        /**
         * Key production for selection.
         * By specifing the axis, nodetype, predicates the list of posssible
         * candiates for a selection is modified in each step.
         *
         * XPath production:
         *  [4] Step                     ::= AxisSpecifier NodeTest Predicate*
         *                                | AbbreviatedStep
         *
         * Used in production:
         *
         * [3] RelativeLocationPath
         *
         * In the jjc grammar file this was realized by the production.
         *
         * void Step() #Step :
         * {}
         * {
         *      AxisSpecifier() NodeTest() ( Predicate() )*
         *      | AbbreviatedStep()
         * }
         *
         */
    public Object visit(ASTStep aStep, Object data)
    {
        XPathContext aContext = (XPathContext) data;

            //if you are the first Step and the location path is not absolut
            //take only the context node
        ArrayList theContextList = null;
        if(aContext.isFirstStepVisited())
        {
            theContextList = aContext.getContext();
        }
        else
        {
            aContext.setFirstStepVisited(true);
            theContextList = new ArrayList();
            theContextList.add(aContext.getCurrentNode());
        }        
            //do a step selection process for every node in the context
            //and build the union at the end
        ArrayList theUnion = new ArrayList();
        for(int i = 0; i < theContextList.size(); i++)
        {
            ArrayList theNodeAsContextList = new ArrayList();
            theNodeAsContextList.add(theContextList.get(i));
            XPathContext aContextForANode =
                new XPathContext(theNodeAsContextList,0);
            
                //handover the variables
            aContextForANode.setVariables(aContext.getVariables());
            
                // in any case let the children do their work
            aStep.childrenAccept(this,aContextForANode);

                //build the union of the result sets
            union(theUnion,aContextForANode.getContext());
        }
        aContext.setContext(theUnion);
        return aContext;
    }

    private void union(ArrayList union, ArrayList addToUnion)
    {
        Iterator toBeAdd = addToUnion.iterator();
        while(toBeAdd.hasNext())
        {
            Node aNode = (Node) toBeAdd.next();
            if(!union.contains(aNode))
                union.add(aNode);
        }
    }

        /**
         * Specify the axis in which you got to go starting from your context
         * At return the context will know in which direction to go
         * during the step.
         *
         * XPath production:
         *
         *  [5] AxisSpecifier            ::= AxisName '::'
         *                                | AbbreviatedAxisSpecifier
         *
         * Used in production:
         *
         *  [4] Step
         *
         * In the jjc-grammar the production is realized as:
         *
         * void AxisSpecifier() #AxisSpecifier :
         * {}
         * {
         *        AxisName()
         *        |   AbbreviatedAxisSpecifier()
         *    }
         *
         */
    public Object visit(ASTAxisSpecifier node, Object data)
    {
        XPathContext aContext = (XPathContext) data;

            //let the children define the axis
        node.childrenAccept(this,aContext);

        return aContext;
    }

        /**
         * Define the axis to go during the step.
         *
         * XPath production:
         *
         * [6] AxisName                 ::= 'ancestor'
         *                                | 'ancestor-or-self'
         *                                | 'attribute'
         *                                | 'child'
         *                                | 'descendant'
         *                                | 'descendant-or-self'
         *                                | 'following'
         *                                | 'following-sibling'
         *                                | 'namespace'
         *                                | 'parent'
         *                                | 'preceding'
         *                                | 'preceding-sibling'
         *                                | 'self'
         *
         * In the jjc-grammar file the production is realized in the form
         *
         *    void AxisName() #AxisName :
         *  {
         *        Token axisName;
         *  }
         *  {
         *        axisName=<AxisName>
         *        {
         *            jjtThis.setName(axisName.image);
         *      }
         *  }
         *
         */
    public Object visit(ASTAxisName node, Object data)
    {
        XPathContext aContext = (XPathContext) data;

        String theAxisName = node.getName();

        aContext.setAxis(theAxisName);

        return  aContext;
    }

        /**
         *  Specify the axis to be the child (if empty! i.e defualt ) or attribute
         *  axis for the current step.
         *
         *  XPath production:
         *
         *  [13] AbbreviatedAxisSpecifier::= '@'?
         *
         *  in the jjc-grammar the production is realized as:
         *
         *  void AbbreviatedAxisSpecifier() #AbbreviatedAxisSpecifier :
         *  {
         *        Token theAbbrevAxis;
         *  }
         *  {
         *    ( theAbbrevAxis="@" { jjtThis.setToken(theAbbrevAxis.image); } )?
         *  }
         *
         */
    public Object visit(ASTAbbreviatedAxisSpecifier node, Object data)
    {
        XPathContext aContext = (XPathContext) data;

        String theAxisToken = node.getToken();
        if(theAxisToken.intern()=="@".intern())
            aContext.setAxis("attribute::");
        else
            aContext.setAxis("child::");

        return  aContext;
    }

        /**
         * Select the nodes along the current axis based on either their name
         * or their type for the current context node.
         *
         * XPath production:
         *
         * [7] NodeTest                 ::= NameTest
         *                                | NodeType '(' ')'
         *                                | 'processing-instruction' '(' Literal ')'
         *
         * Used in:
         *
         * [4] Step
         *
         * In the jjc-grammar the production is realized as:
         *
         *    void NodeTest() #NodeTest :
         *    {
         *        Token thePINameToken;
         *    }
         *    {
         *        LOOKAHEAD(NodeType() "(" ")") NodeType() "(" ")"
         *        |   LOOKAHEAD(<PI> "(" <Literal> ")")
         *            <PI> "(" thePINameToken=<Literal> ")"
         *            { jjtThis.setPIName(thePINameToken.image); }
         *        |   NameTest()
         *    }
         *
         **/
    public Object visit(ASTNodeTest aNodeTest, Object data)
    {
        XPathContext aContext = (XPathContext) data;

            //We are looking for processing instructions with a given name
        if(aNodeTest.jjtGetNumChildren()==0)
        {
            Node aNode = aContext.getCurrentNode();

            int theAxisToWalk = aContext.getAxis();

            ArrayList theResultSetForANode = new ArrayList();

            ArrayList theAxis = null;

            switch (theAxisToWalk){

                case XPathContext.ANCESTOR :
                    theAxis = this.getAncestorAxis(aNode);
                    for(int j = 0; j < theAxis.size(); j++)
                    {
                        Node aNodeOnTheAxis  = (Node) theAxis.get(j);
                        if( aNodeOnTheAxis.getNodeType()==
                            Node.PROCESSING_INSTRUCTION_NODE &&
                            aNodeOnTheAxis.getNodeName().intern()==
                            aNodeTest.getPIName().intern())
                            theResultSetForANode.add(aNodeOnTheAxis);
                    }
                        //result set sorted in reverse document order
                    break;

                case XPathContext.ANCESTOR_OR_SELF :
                    theAxis = this.getAncestorOrSelfAxis(aNode);
                    for(int j = 0; j < theAxis.size(); j++)
                    {
                        Node aNodeOnTheAxis  = (Node) theAxis.get(j);
                        if( aNodeOnTheAxis.getNodeType()==
                            Node.PROCESSING_INSTRUCTION_NODE &&
                            aNodeOnTheAxis.getNodeName().intern()==
                            aNodeTest.getPIName().intern())
                            theResultSetForANode.add(aNodeOnTheAxis);
                    }
                        //result set sorted in reverse document order
                    break;

                case XPathContext.ATTRIBUTE:
                        //There are no processing instructions along the attribute
                        //axis. We are done
                    theResultSetForANode.clear();
                    break;

                case XPathContext.CHILD:
                    NodeList theChilds = aNode.getChildNodes();
                    for(int j = 0; j < theChilds.getLength(); j++)
                    {
                        Node aChild  = theChilds.item(j);
                        if(aChild.getNodeType()==Node.PROCESSING_INSTRUCTION_NODE &&
                           aChild.getNodeName().intern()==
                           aNodeTest.getPIName().intern())
                            theResultSetForANode.add(aChild);
                    }
                        //result set sorted in document order
                    break;

                case XPathContext.DESCENDANT:
                    theAxis = this.getDescendantAxis(aNode);
                    for(int j = 0; j < theAxis.size(); j++)
                    {
                        Node aNodeOnTheAxis  = (Node) theAxis.get(j);
                        if( aNodeOnTheAxis.getNodeType()==
                            Node.PROCESSING_INSTRUCTION_NODE &&
                            aNodeOnTheAxis.getNodeName().intern()==
                            aNodeTest.getPIName().intern())
                            theResultSetForANode.add(aNodeOnTheAxis);
                    }
                        //result set sorted in document order
                    break;

                case XPathContext.DESCENDANT_OR_SELF:
                    theAxis = this.getDescendantOrSelf(aNode);
                    for(int j = 0; j < theAxis.size(); j++)
                    {
                        Node aNodeOnTheAxis  = (Node) theAxis.get(j);
                        if( aNodeOnTheAxis.getNodeType()==
                            Node.PROCESSING_INSTRUCTION_NODE &&
                            aNodeOnTheAxis.getNodeName().intern()==
                            aNodeTest.getPIName().intern())
                            theResultSetForANode.add(aNodeOnTheAxis);
                    }
                        //result set sorted in document order
                    break;

                case XPathContext.FOLLOWING:
                    theAxis = this.getFollowingAxis(aNode);
                    for(int j = 0; j < theAxis.size(); j++)
                    {
                        Node aNodeOnTheAxis  = (Node) theAxis.get(j);
                        if( aNodeOnTheAxis.getNodeType()==
                            Node.PROCESSING_INSTRUCTION_NODE &&
                            aNodeOnTheAxis.getNodeName().intern()==
                            aNodeTest.getPIName().intern())
                            theResultSetForANode.add(aNodeOnTheAxis);
                    }
                        //result set sorted in document order
                    break;

                case XPathContext.FOLLOWING_SIBILLING:
                    theAxis = this.getFollowingSibillingAxis(aNode);
                    for(int j = 0; j < theAxis.size(); j++)
                    {
                        Node aNodeOnTheAxis  = (Node) theAxis.get(j);
                        if( aNodeOnTheAxis.getNodeType()==
                            Node.PROCESSING_INSTRUCTION_NODE &&
                            aNodeOnTheAxis.getNodeName().intern()==
                            aNodeTest.getPIName().intern())
                            theResultSetForANode.add(aNodeOnTheAxis);
                    }
                        //result set sorted in document order
                    break;

                case XPathContext.NAMESPACE:
                        //There are no processing instructions along the namespace
                        //axis. We are done
                    theResultSetForANode.clear();
                        //nothing to sort
                    break;

                case XPathContext.PARENT:
                    Node theParent = aNode.getParentNode();
                    if( theParent != null &&
                        theParent.getNodeType()==Node.PROCESSING_INSTRUCTION_NODE &&
                        theParent.getNodeName().intern()==
                        aNodeTest.getPIName().intern())
                        theResultSetForANode.add(theParent);
                        //nothing to sort
                    break;

                case XPathContext.PRECEDING:
                    theAxis = this.getPrecedingAxis(aNode);
                    for(int j = 0; j < theAxis.size(); j++)
                    {
                        Node aNodeOnTheAxis  = (Node) theAxis.get(j);
                        if( aNodeOnTheAxis.getNodeType()==
                            Node.PROCESSING_INSTRUCTION_NODE &&
                            aNodeOnTheAxis.getNodeName().intern()==
                            aNodeTest.getPIName().intern())
                            theResultSetForANode.add(aNodeOnTheAxis);
                    }
                        //result set sorted in reverse document order
                    break;

                case XPathContext.PRECEDING_SIBILLING:
                    theAxis = this.getPrecedingSibillingAxis(aNode);
                    for(int j = 0; j < theAxis.size(); j++)
                    {
                        Node aNodeOnTheAxis  = (Node) theAxis.get(j);
                        if( aNodeOnTheAxis.getNodeType()==
                            Node.PROCESSING_INSTRUCTION_NODE &&
                            aNodeOnTheAxis.getNodeName().intern()==
                            aNodeTest.getPIName().intern())
                            theResultSetForANode.add(aNodeOnTheAxis);
                    }
                        //result set sorted in reverse document order
                    break;

                case XPathContext.SELF:
                    if( aNode != null &&
                        aNode.getNodeType()==
                        Node.PROCESSING_INSTRUCTION_NODE &&
                        aNode.getNodeName().intern()==
                        aNodeTest.getPIName().intern())
                        theResultSetForANode.add(aNode);
                        //nothing to sort
                    break;}//end switch

            aContext.setContext(theResultSetForANode);
        }
            //let the children do the name or type check
        else
        {
            aNodeTest.childrenAccept(this,aContext);
        }
        return aContext;
    }

        /**
         * Test all nodes along a given axis for the current node,
         * if they pass the name test:
         *
         *   [37] NameTest                ::= '*'
         *                                | NCName ':' '*'
         *                                | QName
         *
         * Use in :
         *
         *    [7] NodeTest
         *
         * void NameTest() #NameTest :
         * {
         *      Token isAnyToken;
         *      String theNameTest;
         * }
         * {
         *          isAnyToken="*"
         *              {   jjtThis.setAnyTest(true);
         *                  jjtThis.setNameTest(isAnyToken.image);
         *              }
         *        |   LOOKAHEAD(NCName() ":" "*") theNameTest=NCName() ":" "*"
         *                {
         *               jjtThis.setAnyNameSpaceTest(true);
         *               jjtThis.setNameTest(theNameTest+":*");
         *              }
         *        |   theNameTest=QName()
         *                { jjtThis.setNameTest(theNameTest); }
         *    }
         */
    public Object visit(ASTNameTest aNameTest, Object data)
    {
        XPathContext aContext = (XPathContext) data;

        if(aNameTest.isAnyTest())
        {
                /* select any nodes of the principal type
                 * along the given axis */
            Node aNode = aContext.getCurrentNode();

            int theAxisToWalk = aContext.getAxis();

            ArrayList theResultSetForANode = new ArrayList();

            switch (theAxisToWalk){

                case XPathContext.ANCESTOR :
                        //prinzipal type Element
                    ArrayList theAxis = this.getAncestorAxis(aNode);
                    for(int j = 0; j < theAxis.size(); j++)
                    {
                        Node anNode  = (Node) theAxis.get(j);
                        if(anNode.getNodeType()== Node.ELEMENT_NODE)
                            theResultSetForANode.add(anNode);
                    }
                        //in reverse order
                    break;

                case XPathContext.ANCESTOR_OR_SELF :
                        //prinzipal type Element
                    theAxis = getAncestorOrSelfAxis(aNode);
                    for(int j = 0; j < theAxis.size(); j++)
                    {
                        Node anNode  = (Node) theAxis.get(j);
                        if(anNode.getNodeType()== Node.ELEMENT_NODE)
                            theResultSetForANode.add(anNode);
                    }
                        //in reverse order
                    break;

                case XPathContext.ATTRIBUTE:
                        //prinzipal type Attribute
                    NamedNodeMap theAxisMap = aNode.getAttributes();
                    if(theAxisMap!=null)
                    {
                        for(int j = 0; j < theAxisMap.getLength(); j++)
                        {
                            Node anNode  = (Node) theAxisMap.item(j);
                            if(anNode.getNodeType()== Node.ATTRIBUTE_NODE)
                                theResultSetForANode.add(anNode);
                        }
                    }
                        //in no order
                    break;

                case XPathContext.CHILD:
                        //prinzipal type Element
                    NodeList theAxisNodeList = aNode.getChildNodes();
                    for(int j = 0; j < theAxisNodeList.getLength(); j++)
                    {
                        Node anNode  = (Node) theAxisNodeList.item(j);
                        if(anNode.getNodeType()== Node.ELEMENT_NODE)
                            theResultSetForANode.add(anNode);
                    }
                        //in order
                    break;

                case XPathContext.DESCENDANT:
                        //prinzipal type Element
                    theAxis = getDescendantAxis(aNode);
                    for(int j = 0; j < theAxis.size(); j++)
                    {
                        Node anNode  = (Node) theAxis.get(j);
                        if(anNode.getNodeType()== Node.ELEMENT_NODE)
                            theResultSetForANode.add(anNode);
                    }
                        //in order
                    break;

                case XPathContext.DESCENDANT_OR_SELF:
                        //prinzipal type Element
                    theAxis = getDescendantOrSelf(aNode);
                    for(int j = 0; j < theAxis.size(); j++)
                    {
                        Node anNode  = (Node) theAxis.get(j);
                        if(anNode.getNodeType()== Node.ELEMENT_NODE)
                            theResultSetForANode.add(anNode);
                    }
                        //in reverse order
                    break;

                case XPathContext.FOLLOWING:
                        //prinzipal type Element
                    theAxis = getFollowingAxis(aNode);
                    for(int j = 0; j < theAxis.size(); j++)
                    {
                        Node anNode  = (Node) theAxis.get(j);
                        if(anNode.getNodeType()== Node.ELEMENT_NODE)
                            theResultSetForANode.add(anNode);
                    }
                        //in order
                    break;

                case XPathContext.FOLLOWING_SIBILLING:
                        //prinzipal type Element
                    theAxis = getFollowingSibillingAxis(aNode);
                    for(int j = 0; j < theAxis.size(); j++)
                    {
                        Node anNode  = (Node) theAxis.get(j);
                        if(anNode.getNodeType()== Node.ELEMENT_NODE)
                            theResultSetForANode.add(anNode);
                    }
                        //in order
                    break;

                case XPathContext.NAMESPACE:
                        //select any node of prinzipal type Namespace, on the NS axis 
                        //are only nodes of type NS.
                    HashMap theNSAxis = getNameSpaceAxis(aNode);
                    Set theKeys = theNSAxis.keySet();
                    Iterator anKeyIter =theKeys.iterator();
                    while(anKeyIter.hasNext())
                    {
                        Node anNSNode  = (Node) theNSAxis.get(anKeyIter.next());
                        theResultSetForANode.add(anNSNode);
                    }
                        //in no order
                    break;

                case XPathContext.PARENT:
                        //prinzipal type Element
                    Node theParent=null;
                    if( aNode instanceof Attr)
                    {
                        theParent = ((Attr)aNode).getOwnerElement();
                    }
                    else
                        theParent = aNode.getParentNode();
                    if( theParent != null &&
                        theParent.getNodeType()== Node.ELEMENT_NODE)
                        theResultSetForANode.add(theParent);
                        //in reverse order
                    break;

                case XPathContext.PRECEDING:
                        //prinzipal type Element
                    theAxis = this.getPrecedingAxis(aNode);
                    for(int j = 0; j < theAxis.size(); j++)
                    {
                        Node anNode  = (Node) theAxis.get(j);
                        if(anNode.getNodeType()== Node.ELEMENT_NODE)
                            theResultSetForANode.add(anNode);
                    }
                        //in reverse order
                    break;

                case XPathContext.PRECEDING_SIBILLING:
                        //prinzipal type Element
                    theAxis = this.getPrecedingSibillingAxis(aNode);
                    for(int j = 0; j < theAxis.size(); j++)
                    {
                        Node anNode  = (Node) theAxis.get(j);
                        if(anNode.getNodeType()== Node.ELEMENT_NODE)
                            theResultSetForANode.add(anNode);
                    }
                        //in reverse order
                    break;

                case XPathContext.SELF:
                        //prinzipal type Element
                    if( aNode.getNodeType()== Node.ELEMENT_NODE)
                        theResultSetForANode.add(aNode);
                        //in order
                    break;
            }//end swicth

            aContext.setContext(theResultSetForANode);
        }
        else if(aNameTest.isAnyNameSpaceTest())
        {
                /**@todo: select any nodes along the given axis
                 * (of the principal type of the axis) of the fully qualified name*/
        }
        else
        {
                //do a simple name test along the given axis
            Node aNode = aContext.getCurrentNode();

            int theAxisToWalk = aContext.getAxis();

            ArrayList theResultSetForANode = new ArrayList();

            switch (theAxisToWalk){

                case XPathContext.ANCESTOR :
                    ArrayList theAxis = this.getAncestorAxis(aNode);
                    for(int j = 0; j < theAxis.size(); j++)
                    {
                        Node anNode  = (Node) theAxis.get(j);
                        if( anNode.getNodeName().intern()==
                            aNameTest.getNameTest().intern())
                            theResultSetForANode.add(anNode);
                    }
                        // in reverse order
                    break;

                case XPathContext.ANCESTOR_OR_SELF :
                    theAxis = getAncestorOrSelfAxis(aNode);
                    for(int j = 0; j < theAxis.size(); j++)
                    {
                        Node anNode  = (Node) theAxis.get(j);
                        if(anNode.getNodeName().intern()==
                           aNameTest.getNameTest().intern())
                            theResultSetForANode.add(anNode);
                    }
                        // in reverse order
                    break;

                case XPathContext.ATTRIBUTE:
                    NamedNodeMap theAxisMap = aNode.getAttributes();
                    if(theAxisMap!=null)
                    {
                        for(int j = 0; j < theAxisMap.getLength(); j++)
                        {
                            Node anNode  = (Node) theAxisMap.item(j);
                            if( anNode.getNodeName().intern()==
                                aNameTest.getNameTest().intern())
                                theResultSetForANode.add(anNode);
                        }
                    }
                        //no order
                    break;

                case XPathContext.CHILD:
                    NodeList theAxisLodeList = aNode.getChildNodes();
                    for(int j = 0; j < theAxisLodeList.getLength(); j++)
                    {
                        Node anNode  = (Node) theAxisLodeList.item(j);
                        if(anNode.getNodeName().intern()==
                           aNameTest.getNameTest().intern())
                            theResultSetForANode.add(anNode);
                    }
                        // in order
                    break;

                case XPathContext.DESCENDANT:
                    theAxis = getDescendantAxis(aNode);
                    for(int j = 0; j < theAxis.size(); j++)
                    {
                        Node anNode  = (Node) theAxis.get(j);
                        if(anNode.getNodeName().intern()==
                           aNameTest.getNameTest().intern())
                            theResultSetForANode.add(anNode);
                    }
                        // in order
                    break;

                case XPathContext.DESCENDANT_OR_SELF:
                    theAxis = getDescendantOrSelf(aNode);
                    for(int j = 0; j < theAxis.size(); j++)
                    {
                        Node anNode  = (Node) theAxis.get(j);
                        if( anNode.getNodeName().intern()==
                            aNameTest.getNameTest().intern())
                            theResultSetForANode.add(anNode);
                    }
                        // in order
                    break;

                case XPathContext.FOLLOWING:
                    theAxis = getFollowingAxis(aNode);
                    for(int j = 0; j < theAxis.size(); j++)
                    {
                        Node anNode  = (Node) theAxis.get(j);
                        if(anNode.getNodeName().intern()==
                           aNameTest.getNameTest().intern())
                            theResultSetForANode.add(anNode);
                    }
                        // in order
                    break;

                case XPathContext.FOLLOWING_SIBILLING:
                    theAxis = getFollowingSibillingAxis(aNode);
                    for(int j = 0; j < theAxis.size(); j++)
                    {
                        Node anNode  = (Node) theAxis.get(j);
                        if(anNode.getNodeName().intern()==
                           aNameTest.getNameTest().intern())
                            theResultSetForANode.add(anNode);
                    }
                        // in order
                    break;

                case XPathContext.NAMESPACE:
                        //the name of a NSnode is its prefix
                    HashMap theNSAxis = getNameSpaceAxis(aNode);
                    Set theKeys = theNSAxis.keySet();
                    Iterator anKeyIter =theKeys.iterator();
                    while(anKeyIter.hasNext())
                    {
                        String aPrefix = (String)anKeyIter.next();
                        Node anNSNode  = (Node) theNSAxis.get(aPrefix);
                        if(aPrefix.intern()== aNameTest.getNameTest().intern())
                            theResultSetForANode.add(anNSNode);
                    }
                        //in no order
                    break;

                case XPathContext.PARENT:
                    Node theParent=null;
                    if( aNode instanceof Attr)
                    {
                        theParent = ((Attr)aNode).getOwnerElement();
                    }
                    else
                        theParent = aNode.getParentNode();
                    if( theParent != null &&
                        theParent.getNodeName().intern()==
                        aNameTest.getNameTest().intern())
                        theResultSetForANode.add(theParent);
                        // in reverse order
                    break;

                case XPathContext.PRECEDING:
                    theAxis = this.getPrecedingAxis(aNode);
                    for(int j = 0; j < theAxis.size(); j++)
                    {
                        Node anNode  = (Node) theAxis.get(j);
                        if( anNode.getNodeName().intern()==
                            aNameTest.getNameTest().intern())
                            theResultSetForANode.add(anNode);
                    }
                        // in reverse order
                    break;

                case XPathContext.PRECEDING_SIBILLING:
                    theAxis = this.getPrecedingSibillingAxis(aNode);
                    for(int j = 0; j < theAxis.size(); j++)
                    {
                        Node anNode  = (Node) theAxis.get(j);
                        if(anNode.getNodeName().intern()==
                           aNameTest.getNameTest().intern())
                            theResultSetForANode.add(anNode);
                    }
                        // in reverse order
                    break;

                case XPathContext.SELF:
                    if( aNode.getNodeName().intern()==
                        aNameTest.getNameTest().intern() )
                        theResultSetForANode.add(aNode);
                        //in order
                    break;
            }//end swicth

            aContext.setContext(theResultSetForANode);
        }
        return aContext;
    }

        /**
         * Test a name along an axis and add the passing nodes to the new context.
         */
    private void testNameAlongAxis( ASTNameTest aNameTest,
                                    ArrayList theAxis,
                                    ArrayList theNewContext)
    {
        for(int j = 0; j < theAxis.size(); j++)
        {
            Node anNode  = (Node) theAxis.get(j);
            if(anNode.getNodeName().intern()==
               aNameTest.getNameTest().intern())
                theNewContext.add(anNode);
        }
    }

        /**
         * Select the nodes along the current axis for the current node
         * based on their type.
         *
         * [38] NodeType                ::= 'comment'
         *                                | 'text'
         *                                | 'processing-instruction'
         *                                | 'node'
         *
         * Used in production:
         *
         * [7] NodeTest
         *
         * In the jcc-grammar file the production was relized as:
         *
         * void NodeType() #NodeType :
         * {
         *      Token theTypeToken;
         * }
         * {
         *        theTypeToken=<TEXT>     { jjtThis.setType(theTypeToken.image); }
         *      | theTypeToken=<NODE>     { jjtThis.setType(theTypeToken.image); }
         *      | theTypeToken=<COMMENT>  { jjtThis.setType(theTypeToken.image); }
         *      | theTypeToken=<PI>       { jjtThis.setType(theTypeToken.image); }
         * }
         *
         **/
    public Object visit(ASTNodeType node, Object data)
    {
        XPathContext aContext = (XPathContext) data;

        int xPathNodeTypeToSelect = node.getTypeID();
        short domNodeTypeToSelect = -1;

        switch (xPathNodeTypeToSelect){
            case ASTNodeType.COMMENT :
                domNodeTypeToSelect = Node.COMMENT_NODE;
                break;
            case ASTNodeType.TEXT :
                domNodeTypeToSelect = Node.TEXT_NODE;
                break;
            case ASTNodeType.PI :
                domNodeTypeToSelect = Node.PROCESSING_INSTRUCTION_NODE;
                break;
            case ASTNodeType.NODE :
                    //do nothing, made sure -1 is not use as a Node.TYPE short.
                break;
        }

            /* select any nodes of the type domNodeTypeToSelect
             * along the given axis */

        int theAxisToWalk = aContext.getAxis();

        ArrayList theResultSetForANode = new ArrayList();

        Node aNode = (Node) aContext.getCurrentNode();

        switch (theAxisToWalk){

            case XPathContext.ANCESTOR :{
                ArrayList theAxis = this.getAncestorAxis(aNode);
                for(int j = 0; j < theAxis.size(); j++)
                {
                    Node anNode  = (Node) theAxis.get(j);
                    if( domNodeTypeToSelect==-1 || //select any node
                        anNode.getNodeType()==domNodeTypeToSelect)
                        theResultSetForANode.add(anNode);
                }}
                break;

            case XPathContext.ANCESTOR_OR_SELF :{
                ArrayList theAxis = getAncestorOrSelfAxis(aNode);
                for(int j = 0; j < theAxis.size(); j++)
                {
                    Node anNode  = (Node) theAxis.get(j);
                    if( domNodeTypeToSelect==-1 || //select any node
                        anNode.getNodeType()== domNodeTypeToSelect)
                        theResultSetForANode.add(anNode);
                }}
                break;

            case XPathContext.ATTRIBUTE:{
                NamedNodeMap theAxis = aNode.getAttributes();
                if(theAxis!=null)
                {
                    for(int j = 0; j < theAxis.getLength(); j++)
                    {
                        Node anNode  = (Node) theAxis.item(j);
                        if( domNodeTypeToSelect==-1 || //select any node
                            anNode.getNodeType()==domNodeTypeToSelect)
                            theResultSetForANode.add(anNode);
                    }
                }}
                break;

            case XPathContext.CHILD:{
                NodeList theAxis = aNode.getChildNodes();
                for(int j = 0; j < theAxis.getLength(); j++)
                {
                    Node anNode  = (Node) theAxis.item(j);
                    if( domNodeTypeToSelect==-1 || //select any node
                        anNode.getNodeType()==domNodeTypeToSelect)
                        theResultSetForANode.add(anNode);
                }}
                break;

            case XPathContext.DESCENDANT:{
                ArrayList theAxis = getDescendantAxis(aNode);
                for(int j = 0; j < theAxis.size(); j++)
                {
                    Node anNode  = (Node) theAxis.get(j);
                    if( domNodeTypeToSelect==-1 || //select any node
                        anNode.getNodeType()==domNodeTypeToSelect)
                        theResultSetForANode.add(anNode);
                }}
                break;

            case XPathContext.DESCENDANT_OR_SELF:{
                ArrayList theAxis = getDescendantOrSelf(aNode);
                for(int j = 0; j < theAxis.size(); j++)
                {
                    Node anNode  = (Node) theAxis.get(j);
                    if( domNodeTypeToSelect==-1 || //select any node
                        anNode.getNodeType()==domNodeTypeToSelect)
                        theResultSetForANode.add(anNode);
                }}
                break;

            case XPathContext.FOLLOWING:{
                ArrayList theAxis = getFollowingAxis(aNode);
                for(int j = 0; j < theAxis.size(); j++)
                {
                    Node anNode  = (Node) theAxis.get(j);
                    if( domNodeTypeToSelect==-1 || //select any node
                        anNode.getNodeType()==domNodeTypeToSelect)
                        theResultSetForANode.add(anNode);
                }}
                break;

            case XPathContext.FOLLOWING_SIBILLING:{
                ArrayList theAxis = getFollowingSibillingAxis(aNode);
                for(int j = 0; j < theAxis.size(); j++)
                {
                    Node anNode  = (Node) theAxis.get(j);
                    if( domNodeTypeToSelect==-1 || //select any node
                        anNode.getNodeType()==domNodeTypeToSelect)
                        theResultSetForANode.add(anNode);
                }}
                break;

            case XPathContext.NAMESPACE:{
                    //only in the case of:"select any node" we get anything at all
                if( domNodeTypeToSelect==-1 )  
                {
                    HashMap theNSAxis = getNameSpaceAxis(aNode);
                    Set theKeys = theNSAxis.keySet();
                    Iterator anKeyIter =theKeys.iterator();
                    while(anKeyIter.hasNext())
                    {
                        Node anNSNode  = (Node) theNSAxis.get(anKeyIter.next());
                        theResultSetForANode.add(anNSNode);
                    }
                        //in no order
                }
            }
                break;

            case XPathContext.PARENT:
                Node theParent=null;
                if( aNode instanceof Attr)
                {
                    theParent = ((Attr)aNode).getOwnerElement();
                }
                else
                    theParent = aNode.getParentNode();
                if( theParent != null &&
                    (domNodeTypeToSelect==-1 || //select any node
                     theParent.getNodeType()==domNodeTypeToSelect))
                    theResultSetForANode.add(theParent);
                break;

            case XPathContext.PRECEDING:{
                ArrayList theAxis = this.getPrecedingAxis(aNode);
                for(int j = 0; j < theAxis.size(); j++)
                {
                    Node anNode  = (Node) theAxis.get(j);
                    if( domNodeTypeToSelect==-1 || //select any node
                        anNode.getNodeType()==domNodeTypeToSelect)
                        theResultSetForANode.add(anNode);
                }}
                break;

            case XPathContext.PRECEDING_SIBILLING:{
                ArrayList theAxis = this.getPrecedingSibillingAxis(aNode);
                for(int j = 0; j < theAxis.size(); j++)
                {
                    Node anNode  = (Node) theAxis.get(j);
                    if( domNodeTypeToSelect==-1 || //select any node
                        anNode.getNodeType()==domNodeTypeToSelect)
                        theResultSetForANode.add(anNode);
                }}
                break;

            case XPathContext.SELF:
                if( domNodeTypeToSelect==-1 || //select any node
                    aNode.getNodeType()==domNodeTypeToSelect)
                    theResultSetForANode.add(aNode);
                break;

        }//end swicth

        aContext.setContext(theResultSetForANode);
        return aContext;
    }


        /**
         *  In case of '.' just keep the context. In case of '..'
         *  get all the parents
         *
         *  XPath production:
         *
         *  [12] AbbreviatedStep         ::= '.'
         *                                | '..'
         *
         *  Used in production:
         *
         *  [4] Step
         *
         *  In the jjc-grammar file the production was realized as:
         *
         *    void AbbreviatedStep() #AbbreviatedStep :
         *  {
         *    Token theAbreviatedStep;
         *  }
         *  {
         *      theAbreviatedStep="."   { jjtThis.setIsSelfNode(true); }
         *      |  theAbreviatedStep=".."  { jjtThis.setIsSelfNode(false); }
         *  }
         *
         */
    public Object visit(ASTAbbreviatedStep node, Object data)
    {
        XPathContext aContext = (XPathContext) data;

        if(node.isSelfNode())
        {
                //do nothing
            return aContext;
        }
        else
        {
                //get the parents
            ArrayList theNewContext = new ArrayList();
            ArrayList theContextList = aContext.getContext();
            int size = theContextList.size();
            for(int i = 0; i < size; i++)
            {
                Node aNode = (Node)theContextList.get(i);
                Node theParent=null;
                if( aNode instanceof Attr)
                {
                    theParent = ((Attr)aNode).getOwnerElement();
                }
                else
                    theParent = aNode.getParentNode();
                if(theParent!=null)
                    theNewContext.add(theParent);
            }
            aContext.setContext(theNewContext);
            return aContext;
        }
    }

        /**
         * A condition the nodes in the current context list must satisfy.
         *
         * XPath production:
         *
         *  [8] Predicate                ::= '[' PredicateExpr ']'
         *
         * Used in :
         *
         *  [4] Step
         *
         * Realized in the jjc-gramar file as:
         *
         *  void Predicate() #Predicate :
         *  {}
         *  {
         *        "[" PredicateExpr() "]"
         *  }
         *
         */
    public Object visit(ASTPredicate aPredicate, Object data)
    {
        XPathContext aContext = (XPathContext) data;

            //We are looking for nodes passing the predicate
        if(aPredicate.jjtGetNumChildren()!=0)
        {
            ArrayList theContextList = aContext.getContext();

            ArrayList theNewContext = new ArrayList();
            for(int i = 0; i < theContextList.size(); i++)
            {
                XPathContext anEvaluationContext =
                    new XPathContext(theContextList,i);
                
                    //handover the variables
                anEvaluationContext.setVariables(aContext.getVariables());
                
                Node aNodeInTheContextList = (Node) theContextList.get(i);
                    //evaluate the predicate
                IXPbooleanValue theResult = (IXPbooleanValue)
                    evaluate(   (SimpleNode)aPredicate.jjtGetChild(0),
                                anEvaluationContext);
                if(theResult.getValue().booleanValue())
                    theNewContext.add(aNodeInTheContextList);
            }
                //update theContextList
            aContext.setContext(theNewContext);
        }
        return aContext;
    }

        /**
         * We are on our way down the expression, the XPath context will be used as
         * evaluation stack for the current node to evaluate the expression.
         *
         * If the expression is a number the predicate is true
         * if the current context position is equal to that value.
         * In any other case the expression is coerced to a boolean
         * (equivalent using the boolean() function) and the predicate
         * has the that value.
         *
         * XPath production:
         *
         * [9] PredicateExpr            ::=  Expr
         *
         * used in
         *
         * [8] Predicate
         *
         * void PredicateExpr() #PredicateExpr :
         * {}
         * {
         *      Expr()
         * }
         *
         */
    public Object visit(ASTPredicateExpr aPredicateExpression, Object data)
    {
        XPathContext aContext = (XPathContext) data;

            //evaluate the predicate
        aPredicateExpression.childrenAccept(this,aContext);
        IXPvalue theResult = aContext.getValue();

            //get the boolean Function just in case
        XPFunctionFactory anInstance = XPFunctionFactory.getInstance();
        XPathFunction theBooleanFunction =
            anInstance.getFunction(XPFunctionFactory.FUNC_ID_BOOLEAN,aContext);
        int theTypeId = theResult.getTypeId();
        switch (theTypeId){
            case IXPbooleanValue.ID:
                    //do nothing
                aContext.setValue(theResult);
                break;
            case IXPnumberValue.ID:
                IXPnumberValue theNumber = (IXPnumberValue)theResult;
                Number aNumber = theNumber.getValue();
                if(aNumber instanceof Integer)
                    if(aNumber.intValue()==1+aContext.getCurrentNodeIndex())
                        aContext.setValue(new XPbooleanValue(new Boolean(true)));
                    else
                        aContext.setValue(new XPbooleanValue(new Boolean(false)));
                else
                    if(aNumber.doubleValue()==1.0+aContext.getCurrentNodeIndex())
                        aContext.setValue(new XPbooleanValue(new Boolean(true)));
                    else
                        aContext.setValue(new XPbooleanValue(new Boolean(false)));
                break;
            case IXPstringValue.ID:
            case IXPnodeSetValue.ID:
            case IXPtreeFragmentValue.ID:
            default :
                ArrayList theArguments = new ArrayList();
                theArguments.add(theResult);
                IXPvalue theBooleanResult =
                    theBooleanFunction.call(theArguments);
                aContext.setValue(theBooleanResult);
        }
        return aContext;
    }

        /**
         * This method will not be visited.
         * @see visit(ASTRelativeLocationPath)
         *
         * @param node an <code>ASTAbbreviatedRelativeLocationPath</code> value
         * @param data an <code>Object</code> value
         * @return an <code>Object</code> value
         */
    public Object visit(ASTAbbreviatedRelativeLocationPath node, Object data)
    {
        throw new java.lang.RuntimeException(
            "Visting this Method is an error in the vealuation of the XPath expression!");
    }

        /**
         * We are on our way down the expression, the XPath context will be used as
         * evaluation stack for the current node to evaluate the expression.
         *
         * [14] Expr ::= OrExpr
         *
         * Used in :
         *
         * [9] PredicateExpr
         *
         * In the jj-c gramar file the production was realized as:
         * void Expr() :
         * {}
         * {
         *        (OrExpr())
         *      #Expr(>1)
         * }
         *
         */
    public Object visit(ASTExpr node, Object data)
    {
        XPathContext aContext = (XPathContext) data;

            //let the children do their work
        node.childrenAccept(this,aContext);

        return aContext;
    }

        /**
         * Use the context to evaluate the the primary expression.
         *
         * XPath production:
         *
         * [15] PrimaryExpr             ::= VariableReference
         *                                | '(' Expr ')'
         *                                | Literal
         *                                | Number
         *                                | FunctionCal
         *
         * Used in:
         *
         * [20] FilterExpr
         *
         * In the jcc gramar file realized as:
         *
         * void PrimaryExpr() #PrimaryExpr :
         * {
         *      Token theTerminal;
         * }
         * {
         *    VariableReference()     { jjtThis.setVariableReference(); }
         *      | "(" Expr() ")"          { jjtThis.setExpr(); }
         *      | theTerminal=<Literal>   { jjtThis.setToken(theTerminal.image); jjtThis.setLiteral(); }
         *      | theTerminal=<Number>    { jjtThis.setToken(theTerminal.image); jjtThis.setNumber(); }
         *      | FunctionCall()          { jjtThis.setFunctionCall(); }
         * }
         *
         */
    public Object visit(ASTPrimaryExpr aPrimaryExpr, Object data)
    {
        XPathContext aContext = (XPathContext) data;

        int theType = aPrimaryExpr.getType();

        switch (theType)
        {
            case ASTPrimaryExpr.NUMBER:
                    //We are a number
                int current = aContext.getCurrentNodeIndex();
                String theNumberToken = aPrimaryExpr.getToken();
                Number theValue = null;
                boolean isInt = false;
                if(isInt=(theNumberToken.indexOf(".")==-1))
                    theValue = new Integer(theNumberToken);
                else
                    theValue = new Double(theNumberToken);
                aContext.setNumberValue(theValue);
                break;
            case ASTPrimaryExpr.FUNCTION_CALL:
                    //let the children do their work
                aPrimaryExpr.childrenAccept(this,aContext);
                break;
            case ASTPrimaryExpr.LITERAL:
                    //We are a literal
                    //' or " allways are include in the literal
                String aToken = aPrimaryExpr.getToken();
                String aNackedToken=aToken.substring(1,aToken.length()-1);
                aContext.setStringValue(aNackedToken);
                break;
            case ASTPrimaryExpr.EXPR:
                    //let the children do their work
                aPrimaryExpr.childrenAccept(this,aContext);
                break;
            case ASTPrimaryExpr.VARIABLE_REFERENCE:
                    //let the children do their work
                aPrimaryExpr.childrenAccept(this,aContext);
                break;
        }

        return aContext;
    }

        /**
         * Performing a function call with zero or more arguments and returning
         * the result of the evaluation as one of the five XPath data types.
         * The arguments will be converted to the types required by the concrete
         * function possibly throwing a RuntimeException if a conversion is not
         * meaningfull possible.
         *
         *  XPath                   Java
         *  boolean                 Boolean
         *  number                  Number
         *  string                  String
         *  node-set                ArrayList() of Nodes
         *  result-tree-fragment    org.w3c.dom.DocumentFragment
         *
         *
         * XPath production:
         *
         * [16] FunctionCall  ::= FunctionName '(' ( Argument ( ',' Argument)*)? ')'
         *
         * Used in:
         *
         * [15] PrimaryExpr
         *
         * In the jj-c gramar file the production was realized as:
         *
         * void FunctionCall() #FunctionCall :
         * {}
         * {
         *      FunctionName() "(" ( Argument() ( "," Argument() )* )? ")"
         * }
         */
    public Object visit(ASTFunctionCall aFunctionCall, Object data)
    {
        XPathContext aContext = (XPathContext) data;

            //Do a runtime check of the function name
        String theFunctionName = (String)
            aFunctionCall.jjtGetChild(0).jjtAccept(this,null);

        ArrayList theArguments = new ArrayList();

            //evaluate the expressions supplied as arguments
        for(int i = 1; i < aFunctionCall.jjtGetNumChildren(); i++)
        {
            ASTArgument anArg = (ASTArgument)aFunctionCall.jjtGetChild(i);
            XPathContext anEvalautionContext =
                new XPathContext(aContext.getContext(),
                                 aContext.getCurrentNodeIndex());
            
                //handover the variables
            anEvalautionContext.setVariables(aContext.getVariables());
            
            IXPvalue anArgumentValue = evaluate(anArg.getExpression(),
                                                anEvalautionContext);
            theArguments.add(anArgumentValue);
        }

        int funcId = ASTFunctionName.getFunctionId(theFunctionName);

            //call the function
        IXPvalue result = callFunction(funcId,aContext,theArguments);
        aContext.setValue(result);
        return result;
    }

        /**
         * Evaluate a general XPathExpression
         */
    IXPvalue evaluate(SimpleNode anExpr, XPathContext aContext)
    {
        XPathContext anContext = (XPathContext) anExpr.jjtAccept(this,aContext);
        return (IXPvalue) anContext.getValue();
    }

        /**
         * Call a given XPath function.
         */
    private IXPvalue callFunction(int funcId,
                                  XPathContext aContext,
                                  ArrayList theArguments)
    {
        XPFunctionFactory theFactory = XPFunctionFactory.getInstance();
        XPathFunction theFunction = theFactory.getFunction(funcId,aContext);
        IXPvalue theResult = theFunction.call(theArguments);
        return theResult;
    }

        /**
         * We will restrict our self to the core set of functions in the XPath
         * specification. In case an unkown function is fund a RuntimeException
         * will be thrown.
         *
         * XPath production:
         *
         * [35] FunctionName            ::= QName - NodeType
         *
         * Used in
         *
         * [16] FunctionCall
         *
         * In the java-cc grammar file the production was realized as.
         *
         *
         * void FunctionName() #FunctionName :
         * {
         *      String theFunctionName;
         * }
         * {
         *      theFunctionName=QName_Without_NodeType()
         *      { jjtThis.setFunctionName(theFunctionName); }
         * }
         *
         */
    public Object visit(ASTFunctionName aFuncNameNode, Object data)
    {
            // check the function name at run time
        if(!ASTFunctionName.FUNCTIONS_AVAILABLE
           .contains(aFuncNameNode.getFunctionName()))
            throw new RuntimeException("Unkown function called: "
                                       + aFuncNameNode.getFunctionName());
        return aFuncNameNode.getFunctionName();
    }

        /**
         * Represents an argument supplied for function calls.
         * Its a genaral XPath expression.
         *
         *  XPath production:
         *
         *   [17]    Argument    ::=    Expr
         *
         *  Used in:
         *
         *  [16] FunctionCall
         *
         *  In the jj-c gramar file realized as:
         *
         *   void Argument() #Argument :
         *   {}
         *   {
         *        Expr()
         *   }
         *
         * It will not be vistited.
         */
    public Object visit(ASTArgument node, Object data)
    {
        throw new java.lang.RuntimeException("Method should not be visited.");
    }

        /**
         * If present in the tree (see jcc production) we have a union of two or
         * more path expressions, so we are of the type node set.
         *
         * XPath production:
         *
         * [18] UnionExpr               ::= PathExpr
         *                                | UnionExpr '|' PathExpr
         *
         * Used in:
         *
         * [27] UnaryExpr
         *
         * In the java cc gramar file relized as:
         *
         * void UnionExpr() :
         * {}
         * {
         *      PathExpr() ( <UNION> PathExpr() )*
         *      #UnionExpr(>1)
         * }
         *
         */
    public Object visit(ASTUnionExpr node, Object data)
    {
        XPathContext aContext = (XPathContext)data;

            // all we know is that we (XPathExpression) are of type node set
        if(aContext.getValue().getTypeId() != IXPnodeSetValue.ID)
            aContext.setNodeSetValue(aContext.getContext());

            //visit all your children in turn and add their results
        ArrayList theUnion = new ArrayList();
        int numOfChilds = node.jjtGetNumChildren();
        for(int i = 0; i < numOfChilds; i++)
        {
            ASTPathExpr aPath = (ASTPathExpr) node.jjtGetChild(i);
            ArrayList theNewContext = new ArrayList();
            theNewContext.addAll(aContext.getContext());
            XPathContext aNewContext =
                new XPathContext(   theNewContext,
                                    aContext.getCurrentNodeIndex());
            
                //handover the variables
            aNewContext.setVariables(aContext.getVariables());
            
            aNewContext.setNodeSetValue(aNewContext.getContext());
            aPath.jjtAccept(this,aNewContext);
            union(theUnion,aNewContext.getContext());
        }
        aContext.setContext(theUnion);
        return aContext;
    }

        /**
         *
         * XPath production:
         *
         * [19] PathExpr                ::= LocationPath
         *                                | FilterExpr
         *                                | FilterExpr '/' RelativeLocationPath
         *                                | FilterExpr '//' RelativeLocationPath
         *
         * used in:
         *
         * [18] UnionExpr
         *
         * In the j-cc gramar file realized as:
         *
         *    void PathExpr() #PathExpr :
         *  {}
         *
         *{
         *        LOOKAHEAD(FilterExpr() ( ( <SLASH> | <SLASHSLASH> ) RelativeLocationPath() )?)
         *        FilterExpr() ( ( <SLASH>  { jjtThis.setIsAbbreviated(false); }
         *                        | <SLASHSLASH> { jjtThis.setIsAbbreviated(true); }  ) RelativeLocationPath() )?
         *    |   LocationPath()
         *}
         *
         */
    public Object visit(ASTPathExpr aPathExpr, Object data)
    {
        XPathContext aContext = (XPathContext) data;

            // we still don't know the type because a FilterExpr
            // might be of any type

            //we are in any case of type node set
            //and the first step has been done effectively
            //how to use that type information????

        if(aPathExpr.jjtGetNumChildren()>1)
        {
            aContext.setFirstStepVisited(true);
                //evaluate the filter expr and use it as input for the relative locations step.
            ASTFilterExpr theFilter = (ASTFilterExpr) aPathExpr.jjtGetChild(0);
            IXPvalue theFilterResult = evaluate(theFilter,aContext);
                // if the result is not of type node set
            if (theFilterResult.getTypeId()!=IXPnodeSetValue.ID)
            {
                throw new RuntimeException("Non nodeset followed by location path!");
            } // end of if ()

            IXPnodeSetValue theFilteredNodeSet =(IXPnodeSetValue)theFilterResult;

                //figure out if we are SLASH ort SLASHSLASH
            if (!aPathExpr.isAbbreviated()) //slash
            {
                    //Set the result of the filter expression as the new context for the RelativePath Expression
                aContext.setValue(theFilteredNodeSet);
                aContext.setContext(theFilteredNodeSet.getValue());
                aContext.setCurrentNode(0);
            }
            else // slashSlash
            {
                    // do a descendant::node() step
                ArrayList allNodesDownTheChildAxis = new ArrayList();

                addSelfAndAllChilds(allNodesDownTheChildAxis,
                                    theFilteredNodeSet.getValue());

                    // Set the result as the context for the RelativeLocationPath
                aContext.setContext(allNodesDownTheChildAxis);
                aContext.setCurrentNode(0);

            } // end of else
                // end of if ()
            ASTRelativeLocationPath aPath = (ASTRelativeLocationPath)aPathExpr.jjtGetChild(1);
            IXPvalue theFinalResult = evaluate(aPath,aContext);//theContext will carry the final result.
        }
        else
        {
                //let the child /Location path or single FilterExpression do his work
            aPathExpr.childrenAccept(this,aContext);
        } // end of else

        return aContext;
    }

        /**
         *
         * XPath production:
         *
         * [20] FilterExpr              ::= PrimaryExpr
         *                                | FilterExpr Predicate
         *
         * used in:
         *
         * [19] PathExpr
         *
         * In the j-cc gramar file realized as:
         *
         * void FilterExpr() #FilterExpr :
         * {}
         * {
         *      (LOOKAHEAD(PrimaryExpr()) PrimaryExpr())+ ( Predicate() )?
         * }
         */
    public Object visit(ASTFilterExpr aFilter, Object data)
    {
        XPathContext aContext = (XPathContext) data;

            //let the children do their work
        aFilter.childrenAccept(this,aContext);

        return aContext;
    }

        /**
         * The and expr are evaluated from left to right. If an expression
         * evaluates to (possibly after converting it to a boolean) true we are
         * done in the other case we keep evaluating the and exprssions.
         *
         * XPath production:
         * [21] OrExpr                  ::= AndExpr
         *                                | OrExpr 'or' AndExpr
         * Used in production:
         *
         * [14] Expr
         *
         * In the jcc-gramar file realized as.
         *
         * void OrExpr() :
         * {}
         *
         * {
         *      (AndExpr() ( <OR> AndExpr() )*)
         *      #OrExpr(>1)
         * }
         *
         */
    public Object visit(ASTOrExpr anOrNode, Object data)
    {
        XPathContext aContext = (XPathContext) data;

            /**
             * if we realy exist we must evaluate the two or more children the
             * result must be converted to a
             * boolean starting left to right.
             */
        aContext.setValue(new XPbooleanValue(new Boolean(false)));
        int numOfChildren = anOrNode.jjtGetNumChildren();
        for(int i = 0; i < numOfChildren; i++)
        {
            XPathContext aEvaluationContext =
                new XPathContext(   aContext.getContext(),
                                    aContext.getCurrentNodeIndex());
            
                //handover the variables
            aEvaluationContext.setVariables(aContext.getVariables());
            
            SimpleNode aSimpleNode = (SimpleNode)anOrNode.jjtGetChild(i);
            aSimpleNode.jjtAccept(this,aEvaluationContext);
            if(aEvaluationContext.getValue().getTypeId()!= IXPbooleanValue.ID)
            {
                    //convert it by calling boolean();
                IXPvalue theValue = aEvaluationContext.getValue();
                ArrayList aValueAsList = new ArrayList();
                aValueAsList.add(theValue);
                XPathFunction theBooleanFuction =
                    XPFunctionFactory.getInstance().getFunction(
                        XPFunctionFactory.FUNC_ID_BOOLEAN,
                        aEvaluationContext);
                IXPbooleanValue theConvertedValue =
                    (IXPbooleanValue)theBooleanFuction.call(aValueAsList);
                if(theConvertedValue.getValue().booleanValue())
                {
                        //we are done
                    aContext.setValue(theConvertedValue);
                    break;
                }
            }
            else
            {
                IXPbooleanValue aBool =(IXPbooleanValue) aEvaluationContext.getValue();
                if(aBool.getValue().booleanValue())
                {
                        //we are done
                    aContext.setValue(aBool);
                    break;
                }
            }
        }

        return aContext;
    }

        /**
         * The equality expr are evaluated from left to right. If an expression
         * evaluates to (possibly after converting it to a boolean) false we are
         * done in the other case we keep evaluating the equality expressions.
         *
         * [22] AndExpr                 ::= EqualityExpr
         | AndExpr 'and' EqualityExpr
         *
         * Used in production:
         *
         * [21] OrExpr
         *
         * In the j-cc gramar the prouction was realized as:
         *
         * void AndExpr() :
         * {}
         * {
         *        (EqualityExpr() ( <AND> EqualityExpr() )*)
         *        #AndExpr(>1)
         * }
         *
         */
    public Object visit(ASTAndExpr anAndNode, Object data)
    {
        XPathContext aContext = (XPathContext) data;

            /**
             * if we realy exist we must evaluate the two or more children the
             * result must be converted to a
             * boolean starting left to right.
             */
        aContext.setValue(new XPbooleanValue(new Boolean(true)));
        int numOfChildren = anAndNode.jjtGetNumChildren();
        for(int i = 0; i < numOfChildren; i++)
        {
            XPathContext aEvaluationContext =
                new XPathContext(   aContext.getContext(),
                                    aContext.getCurrentNodeIndex());
            
                //handover the variables
            aEvaluationContext.setVariables(aContext.getVariables());
            
            SimpleNode aSimpleNode = (SimpleNode)anAndNode.jjtGetChild(i);
            aSimpleNode.jjtAccept(this,aEvaluationContext);
            if(aEvaluationContext.getValue().getTypeId()!= IXPbooleanValue.ID)
            {
                    //convert it by calling boolean();
                IXPvalue theValue = aEvaluationContext.getValue();
                    //XPboolean aBooleanFunction = new XPboolean(aEvaluationContext);
                    //aBooleanFunction.
                ArrayList aValueAsList = new ArrayList();
                aValueAsList.add(theValue);
                XPathFunction theBooleanFuction =
                    XPFunctionFactory.getInstance().getFunction(
                        XPFunctionFactory.FUNC_ID_BOOLEAN,
                        aEvaluationContext);
                IXPbooleanValue theConvertedValue =
                    (IXPbooleanValue)theBooleanFuction.call(aValueAsList);
                if(!theConvertedValue.getValue().booleanValue())
                {
                        //we are done
                    aContext.setValue(theConvertedValue);
                    break;
                }
            }
            else
            {
                IXPbooleanValue aBool =
                    (IXPbooleanValue) aEvaluationContext.getValue();
                if(!aBool.getValue().booleanValue())
                {
                        //we are done
                    aContext.setValue(aBool);
                    break;
                }
            }
        }

        return aContext;
    }

        /**
         * If existing always evaluates too a boolean.
         * The evalaution is left recursive and has some realy unexpected semantics:
         *
         * [23] EqualityExpr            ::= RelationalExpr
         *                                | EqualityExpr '=' RelationalExpr
         *                                | EqualityExpr '!=' RelationalExpr
         *
         * Used in production:
         *
         * [22] AndExpr
         *
         * void EqualityExpr() :
         * {}
         * {
         *        (RelationalExpr() ( ( <EQ> | <NEQ> ) RelationalExpr() )*)
         *        #EqualityExpr(>1)
         *  }
         *
         */
    public Object visit(ASTEqualityExpr anEqualityNode, Object data)
    {
        XPathContext aContext = (XPathContext) data;

            /**
             * if we realy exist we must evaluate the two or more children
             * from the left to the right
             * in any case the result is of type boolean
             */
        aContext.setBooleanValue(new Boolean(true));

        int numOfChildren = anEqualityNode.jjtGetNumChildren();

            //the first step
        SimpleNode leftOperant = (SimpleNode)anEqualityNode.jjtGetChild(0);
        SimpleNode rightOperant = (SimpleNode)anEqualityNode.jjtGetChild(1);

        String anComparatorToken = anEqualityNode.getComparatorToken(0);

        Boolean theResult = new Boolean(false);

        XPbooleanValue theLeftResult = new XPbooleanValue(theResult);

        XPathContext aLeftEvaluationContext =
            new XPathContext(   aContext.getContext(),
                                aContext.getCurrentNodeIndex());

            //handover the variables
        aLeftEvaluationContext.setVariables(aContext.getVariables());
        
        XPathContext aRightEvaluationContext =
            new XPathContext(   aContext.getContext(),
                                aContext.getCurrentNodeIndex());
            //handover the variables
        aRightEvaluationContext.setVariables(aContext.getVariables());
        
        leftOperant.jjtAccept(this,aLeftEvaluationContext);
        rightOperant.jjtAccept(this,aRightEvaluationContext);

        IXPvalue leftValue = aLeftEvaluationContext.getValue();
        IXPvalue rightValue = aRightEvaluationContext.getValue();

        theResult = compareValuesForEquality(  leftValue,
                                               rightValue,
                                               anComparatorToken,
                                               aContext);

            //in case we got more to do
        for(int i = 2; i < anEqualityNode.jjtGetNumChildren(); i++)
        {
            leftValue = new XPbooleanValue(theResult);
            aRightEvaluationContext =
                new XPathContext(   aContext.getContext(),
                                    aContext.getCurrentNodeIndex());

                //handover the variables
            aRightEvaluationContext.setVariables(aContext.getVariables());
        
            rightOperant = (SimpleNode)anEqualityNode.jjtGetChild(i);
            anComparatorToken = anEqualityNode.getComparatorToken(i-1);

            rightOperant.jjtAccept(this,aRightEvaluationContext);
            rightValue = aRightEvaluationContext.getValue();
            theResult = compareValuesForEquality(  leftValue,
                                                   rightValue,
                                                   anComparatorToken,
                                                   aContext);
        }

        aContext.setBooleanValue(theResult);

        return aContext;
    }

    private Boolean compareValuesForEquality(  IXPvalue leftValue,
                                               IXPvalue rightValue,
                                               String aComparatorToken,
                                               XPathContext aContext)
    {
        Boolean theResult = new Boolean(false);

        int leftType = leftValue.getTypeId();
        int rightType = rightValue.getTypeId();
        boolean leftIsSimple =
            leftType == IXPbooleanValue.ID ||
            leftType == IXPnumberValue.ID ||
            leftType == IXPstringValue.ID;

        boolean rightIsSimple =
            rightType == IXPbooleanValue.ID ||
            rightType == IXPnumberValue.ID ||
            rightType == IXPstringValue.ID;

        if(leftIsSimple && rightIsSimple)
        {
            if( leftType == IXPbooleanValue.ID ||
                rightType == IXPbooleanValue.ID )
            {
                    //convert if necessary one type to boolean and calculate
                    //theResult
                XPboolean theBooleanFunction = new XPboolean(aContext);
                boolean aBoolean =
                    (leftType == IXPbooleanValue.ID ?
                     ((IXPbooleanValue)leftValue).getValue().booleanValue():
                     ((IXPbooleanValue)rightValue).getValue().booleanValue());

                IXPvalue anOtherValue =
                    (leftType == IXPbooleanValue.ID ? rightValue:leftValue);

                IXPbooleanValue anOtherBooleanValue = null;
                if(anOtherValue.getTypeId()!=IXPbooleanValue.ID)
                    anOtherBooleanValue = theBooleanFunction.call(anOtherValue);
                else
                    anOtherBooleanValue = (IXPbooleanValue)anOtherValue;

                boolean anOtherBoolean =
                    anOtherBooleanValue.getValue().booleanValue();

                if(aComparatorToken.intern()==ASTEqualityExpr.EQ.intern())
                    theResult = new Boolean(aBoolean==anOtherBoolean);
                else
                    theResult = new Boolean(aBoolean!=anOtherBoolean);
            }
            else if(    leftType == IXPnumberValue.ID ||
                        rightType == IXPnumberValue.ID )
            {
                    //convert if necessary one type to number and calculate
                    //theResult
                XPnumber theNumberFunction = new XPnumber(aContext);
                Number aNumber =
                    (leftType == IXPnumberValue.ID ?
                     ((IXPnumberValue)leftValue).getValue():
                     ((IXPnumberValue)rightValue).getValue());

                IXPvalue anOtherValue =
                    (leftType == IXPnumberValue.ID ? rightValue:leftValue);

                IXPnumberValue anOtherNumberValue = null;
                if(anOtherValue.getTypeId()!=IXPnumberValue.ID)
                    anOtherNumberValue = theNumberFunction.call(anOtherValue);
                else
                    anOtherNumberValue = (IXPnumberValue)anOtherValue;


                Number anOtherNumber = anOtherNumberValue.getValue();

                if( anOtherNumber instanceof Integer &&
                    aNumber instanceof Integer)
                {
                    int aInt = aNumber.intValue();
                    int anOtherInt = anOtherNumber.intValue();
                    if(aComparatorToken.intern()==ASTEqualityExpr.EQ.intern())
                        theResult = new Boolean(aInt==anOtherInt);
                    else
                        theResult = new Boolean(aInt!=anOtherInt);
                }
                else
                {
                    double aDouble = aNumber.doubleValue();
                    double anOtherDouble = anOtherNumber.doubleValue();
                    if(aComparatorToken.intern()==ASTEqualityExpr.EQ)
                        theResult = new Boolean(aDouble==anOtherDouble);
                    else
                        theResult = new Boolean(aDouble!=anOtherDouble);
                }
            }
            else
            {
                String aString      = ((IXPstringValue)leftValue).getValue();
                String anOtherString =((IXPstringValue)rightValue).getValue();

                    //compare the strings
                if(aComparatorToken.intern()==ASTEqualityExpr.EQ.intern())
                    theResult =
                        new Boolean(aString.intern()==anOtherString.intern());
                else
                    theResult =
                        new Boolean(aString.intern()!=anOtherString.intern());
            }
        }
        else if ( (leftType == IXPnodeSetValue.ID && rightType != IXPtreeFragmentValue.ID)
                  || (rightType == IXPnodeSetValue.ID && leftType != IXPtreeFragmentValue.ID))
        {   //at least one value is not simple and of type nodeset but none of the
                //values is of type treeFragment
            IXPnodeSetValue theNodeSet =
                ( rightType==IXPnodeSetValue.ID ?
                  (IXPnodeSetValue) rightValue :
                  (IXPnodeSetValue) leftValue
                  );
            IXPvalue theOtherValue =
                ( rightType==IXPnodeSetValue.ID ?
                  leftValue :
                  rightValue
                  );

            if(theOtherValue.getTypeId()==IXPbooleanValue.ID)
            {
                boolean booleanValue =
                    ((IXPbooleanValue)theOtherValue).getValue().booleanValue();
                int sizeOfNodeSet = theNodeSet.getValue().size();

                if(aComparatorToken.intern()==ASTEqualityExpr.EQ.intern())
                {
                    boolean booleanResult = (booleanValue && (sizeOfNodeSet > 0)) ||
                        (!booleanValue && !(sizeOfNodeSet > 0));
                    theResult =
                        new Boolean( booleanResult );
                }
                else
                {
                    boolean booleanResult = !((booleanValue && (sizeOfNodeSet > 0)) ||
                                              (!booleanValue && !(sizeOfNodeSet > 0)));
                    theResult =
                        new Boolean( booleanResult );
                }
            }
            else if(theOtherValue.getTypeId()==IXPnumberValue.ID)
            {
                ArrayList theNodes = theNodeSet.getValue();
                Iterator theNodeIterator = theNodes.iterator();
                theResult = new Boolean(false);
                while(theNodeIterator.hasNext())
                {
                    Node aNode = (Node)theNodeIterator.next();
                    String aNodeAsString = XPstring.nodeToString(aNode);
                    Number aNodeAsNumber =
                        XPnumber.stringToNumber(aNodeAsString);
                    Number theOtherNumber =
                        ((IXPnumberValue)theOtherValue).getValue();
                    if( aNodeAsNumber instanceof Integer &&
                        theOtherNumber instanceof Integer)
                    {
                        if( aComparatorToken.intern()==
                            ASTEqualityExpr.EQ.intern())
                        {
                            boolean booleanResult = aNodeAsNumber.intValue()==
                                theOtherNumber.intValue();
                            if(booleanResult)
                            {
                                theResult = new Boolean(booleanResult);
                                break;
                            }
                        }
                        else
                        {
                            boolean booleanResult = aNodeAsNumber.intValue()!=
                                theOtherNumber.intValue();
                            if(booleanResult)
                            {
                                theResult = new Boolean(booleanResult);
                                break;
                            }
                        }
                    }
                    else
                    {
                        if( aComparatorToken.intern()==
                            ASTEqualityExpr.EQ.intern())
                        {
                            boolean booleanResult =
                                aNodeAsNumber.doubleValue()==
                                theOtherNumber.doubleValue();
                            if(booleanResult)
                            {
                                theResult = new Boolean(booleanResult);
                                break;
                            }
                        }
                        else
                        {
                            boolean booleanResult =
                                aNodeAsNumber.doubleValue()!=
                                theOtherNumber.doubleValue();
                            if(booleanResult)
                            {
                                theResult = new Boolean(booleanResult);
                                break;
                            }
                        }
                    }
                }//end while
            }
            else if(theOtherValue.getTypeId()==IXPstringValue.ID)
            {
                ArrayList theNodes = theNodeSet.getValue();
                Iterator theNodeIterator = theNodes.iterator();
                theResult = new Boolean(false);
                while(theNodeIterator.hasNext())
                {
                    Node aNode = (Node)theNodeIterator.next();
                    String aNodeAsString = XPstring.nodeToString(aNode);
                    String theOtherString =
                        ((IXPstringValue)theOtherValue).getValue();
                    if( aComparatorToken.intern()==
                        ASTEqualityExpr.EQ.intern())
                    {
                        boolean booleanResult =
                            aNodeAsString.equals(theOtherString);
                        if(booleanResult)
                        {
                            theResult = new Boolean(booleanResult);
                            break;
                        }
                    }
                    else
                    {
                        boolean booleanResult =
                            !aNodeAsString.equals(theOtherString);
                        if(booleanResult)
                        {
                            theResult = new Boolean(booleanResult);
                            break;
                        }
                    }
                }//end while
            }
            else//both of type node set
            {
                ArrayList theNodeSetList = theNodeSet.getValue();
                ArrayList theOtherNodeSetList =
                    ((IXPnodeSetValue)theOtherValue).getValue();
                Iterator theNodeSetIterator = theNodeSetList.iterator();
                theResult = new Boolean(false);
                search : while(theNodeSetIterator.hasNext())
                {
                    Node aNode = (Node)theNodeSetIterator.next();
                    Iterator theOtherNodeSetIterator =
                        theOtherNodeSetList.iterator();
                    while(theOtherNodeSetIterator.hasNext())
                    {
                        Node anOtherNode =
                            (Node)theOtherNodeSetIterator.next();
                        String aNodeAsString = XPstring.nodeToString(aNode);
                        String anOtherNodeAsString =
                            XPstring.nodeToString(anOtherNode);
                        if(aComparatorToken.intern()==ASTEqualityExpr.EQ)
                        {
                            boolean booleanResult =
                                aNodeAsString.equals(anOtherNodeAsString);
                            if(booleanResult)
                            {
                                theResult = new Boolean(booleanResult);
                                break  search;//break out of both while's
                            }
                        }
                        else
                        {
                            boolean booleanResult =
                                !aNodeAsString.equals(anOtherNodeAsString);
                            if(booleanResult)
                            {
                                theResult = new Boolean(booleanResult);
                                break  search;//break out of both while's
                            }
                        }
                    }//inner while
                }//outer while
            }
        }
        else
        {
            IXPtreeFragmentValue theTreeFragment =
                ( rightType==IXPtreeFragmentValue.ID ?
                  (IXPtreeFragmentValue) rightValue :
                  (IXPtreeFragmentValue) leftValue
                  );
            IXPvalue theOtherValue =
                ( rightType==IXPnodeSetValue.ID ?
                  leftValue :
                  rightValue
                  );

            if(theOtherValue.getTypeId()==IXPbooleanValue.ID)
            {
                IXPbooleanValue theOtherBooleanValue =
                    (IXPbooleanValue)theOtherValue;
                boolean booleanValue =
                    theOtherBooleanValue.getValue().booleanValue();
                if(aComparatorToken.intern()==ASTEqualityExpr.EQ)
                {
                        //equal if booleanvalue true==1 Node and vice versa
                    theResult = new Boolean(booleanValue);
                }
                else
                {
                        //no equal if booleanvalue false==0 Node and vice versa
                    theResult = new Boolean(!booleanValue);
                }
            }
            else if(theOtherValue.getTypeId()==IXPnumberValue.ID)
            {
                Number theOtherNumberValue =
                    ((IXPnumberValue)theOtherValue).getValue();
                DocumentFragment theTreeFragmentValue =
                    theTreeFragment.getValue();
                String aTreeFragmentAsString =
                    XPstring.nodeToString(theTreeFragmentValue);
                Number aTreeFragmentAsNumber =
                    XPnumber.stringToNumber(aTreeFragmentAsString);

                if( theOtherNumberValue instanceof Integer &&
                    aTreeFragmentAsNumber instanceof Integer)
                {
                    if( aTreeFragmentAsNumber.intValue()==
                        aTreeFragmentAsNumber.intValue())
                    {
                        if(aComparatorToken.intern()==ASTEqualityExpr.EQ)
                            theResult = new Boolean(true);
                        else
                            theResult = new Boolean(false);
                    }
                    else
                    {
                        if(aComparatorToken.intern()==ASTEqualityExpr.EQ)
                            theResult = new Boolean(false);
                        else
                            theResult = new Boolean(true);
                    }
                }
                else
                {
                    if( aTreeFragmentAsNumber.doubleValue()==
                        aTreeFragmentAsNumber.doubleValue())
                    {
                        if(aComparatorToken.intern()==ASTEqualityExpr.EQ)
                            theResult = new Boolean(true);
                        else
                            theResult = new Boolean(false);
                    }
                    else
                    {
                        if(aComparatorToken.intern()==ASTEqualityExpr.EQ)
                            theResult = new Boolean(false);
                        else
                            theResult = new Boolean(true);
                    }
                }
            }
            else if(theOtherValue.getTypeId()==IXPstringValue.ID)
            {
                IXPstringValue theOtherStringValue =
                    (IXPstringValue)theOtherValue;
                DocumentFragment theTreeFragmentValue =
                    theTreeFragment.getValue();
                String aTreeFragmentAsString =
                    XPstring.nodeToString(theTreeFragmentValue);

                if(aComparatorToken.intern()==ASTEqualityExpr.EQ)
                {
                    theResult =
                        new Boolean(theOtherStringValue.equals(aTreeFragmentAsString));
                }
                else
                {
                    theResult =
                        new Boolean(!theOtherStringValue.equals(aTreeFragmentAsString));
                }
            }
            else // node set value
            {
                IXPnodeSetValue theNodeSetValue = (IXPnodeSetValue)theOtherValue;
                ArrayList theNodes = theNodeSetValue.getValue();
                Iterator theNodeIterator = theNodes.iterator();
                DocumentFragment theTreeFragmentValue =
                    theTreeFragment.getValue();
                String aTreeFragmentAsString =
                    XPstring.nodeToString(theTreeFragmentValue);
                theResult = new Boolean(false);
                while(theNodeIterator.hasNext())
                {
                    Node aNode = (Node)theNodeIterator.next();
                    String aNodeAsString = XPstring.nodeToString(aNode);
                    if( aComparatorToken.intern()==
                        ASTEqualityExpr.EQ.intern())
                    {
                        boolean booleanResult =
                            aNodeAsString.equals(aTreeFragmentAsString);
                        if(booleanResult)
                        {
                            theResult = new Boolean(booleanResult);
                            break;
                        }
                    }
                    else
                    {
                        boolean booleanResult =
                            !aNodeAsString.equals(aTreeFragmentAsString);
                        if(booleanResult)
                        {
                            theResult = new Boolean(booleanResult);
                            break;
                        }
                    }
                }//end while
            }
        }
        return theResult;
    }

        /**
         * If existing always evaluates too a boolean.
         * The evalution is left recursive and has some realy unexpected semantics:
         *
         * [24] RelationalExpr          ::= AdditiveExpr
         *                                | RelationalExpr '<' AdditiveExpr
         *                                | RelationalExpr '>' AdditiveExpr
         *                                | RelationalExpr '<=' AdditiveExpr
         *                                | RelationalExpr '>=' AdditiveExpr
         *
         * Used in :
         *
         * [23] EqualityExpr
         *
         * In the jcc gramar file realized as:
         *
         *
         * void RelationalExpr() :
         * {
         *      Token anComparator;
         * }
         * {
         *        (AdditiveExpr()  ( (        anComaprator=<LT>
         *                              |   anComaprator=<GT>
         *                              |   anComaprator=<LTE>
         *                              |   anComaprator=<GTE> )
         *                              { jjtThis.addComparator(anComparator.image); }
         *                              AdditiveExpr() )*)
         *        #RelationalExpr(>1)
         * }
         *
         */
    public Object visit(ASTRelationalExpr aRelationalNode, Object data)
    {
        XPathContext aContext = (XPathContext) data;

            /**
             * if we realy exist we must evaluate the two or more children
             * from the left to the right
             * in any case the result is of type boolean
             */
        aContext.setBooleanValue(new Boolean(true));

        int numOfChildren = aRelationalNode.jjtGetNumChildren();

            //the first step; Type of operants is still undefined
        SimpleNode leftOperant = (SimpleNode)aRelationalNode.jjtGetChild(0);
        SimpleNode rightOperant = (SimpleNode)aRelationalNode.jjtGetChild(1);

        String anComparatorToken = aRelationalNode.getComparatorToken(0);

        Boolean theResult = new Boolean(false);

        XPbooleanValue theLeftResult = new XPbooleanValue(theResult);

        XPathContext aLeftEvaluationContext =
            new XPathContext(aContext.getContext(),
                                aContext.getCurrentNodeIndex());

           //handover the variables
        aLeftEvaluationContext.setVariables(aContext.getVariables());
        
        XPathContext aRightEvaluationContext =
            new XPathContext(aContext.getContext(),
                                aContext.getCurrentNodeIndex());

           //handover the variables
        aRightEvaluationContext.setVariables(aContext.getVariables());
        
        leftOperant.jjtAccept(this,aLeftEvaluationContext);
        rightOperant.jjtAccept(this,aRightEvaluationContext);

        IXPvalue leftValue = aLeftEvaluationContext.getValue();
        IXPvalue rightValue = aRightEvaluationContext.getValue();

        theResult = compareValues(  leftValue,
                                    rightValue,
                                    anComparatorToken,
                                    aContext);

            //in case we got more to do
        for(int i = 2; i < aRelationalNode.jjtGetNumChildren(); i++)
        {
            leftValue = new XPbooleanValue(theResult);
            aRightEvaluationContext =
                new XPathContext(   aContext.getContext(),
                                    aContext.getCurrentNodeIndex());

                //handover the variables
            aRightEvaluationContext.setVariables(aContext.getVariables());
            
            rightOperant = (SimpleNode)aRelationalNode.jjtGetChild(i);
            anComparatorToken = aRelationalNode.getComparatorToken(i-1);

            rightOperant.jjtAccept(this,aRightEvaluationContext);
            rightValue = aRightEvaluationContext.getValue();
            theResult = compareValues(  leftValue,
                                        rightValue,
                                        anComparatorToken,
                                        aContext);
        }

        aContext.setBooleanValue(theResult);

        return aContext;
    }

    private Boolean compareValues(  IXPvalue leftValue,
                                    IXPvalue rightValue,
                                    String anComparatorToken,
                                    XPathContext aContext)
    {
        int leftType = leftValue.getTypeId();
        int rightType = rightValue.getTypeId();

        Boolean theResult = new Boolean(false);

        if(leftType != IXPnodeSetValue.ID && rightType != IXPnodeSetValue.ID)
        {
                //convert them to a number and compare them

            XPnumber theNumberFunction = new XPnumber(aContext);
            IXPnumberValue leftNumberValue = theNumberFunction.call(leftValue);
            IXPnumberValue rightNumberValue = theNumberFunction.call(rightValue);
            if ( leftNumberValue.isNaN() || rightNumberValue.isNaN())
            {
                return new Boolean(false);
            } // end of if ()
            
            int result = leftNumberValue.compareTo(rightNumberValue);

            if(result == 0)//equal
                if(anComparatorToken.intern()==ASTRelationalExpr.GT.intern())
                    theResult = new Boolean(false);
                else if(anComparatorToken.intern()==ASTRelationalExpr.GTE.intern())
                    theResult = new Boolean(true);
                else if(anComparatorToken.intern()==ASTRelationalExpr.LT.intern())
                    theResult = new Boolean(false);
                else //(anComparatorToken.intern()==ASTRelationalExpr.LTE.intern())
                    theResult = new Boolean(true);
            else if(result < 0)//left smaller than right
                if(anComparatorToken.intern()==ASTRelationalExpr.GT.intern())
                    theResult = new Boolean(false);
                else if(anComparatorToken.intern()==ASTRelationalExpr.GTE.intern())
                    theResult = new Boolean(false);
                else if(anComparatorToken.intern()==ASTRelationalExpr.LT.intern())
                    theResult = new Boolean(true);
                else //(anComparatorToken.intern()==ASTRelationalExpr.LTE.intern())
                    theResult = new Boolean(true);
            else//left greater than right
                if(anComparatorToken.intern()==ASTRelationalExpr.GT.intern())
                    theResult = new Boolean(true);
                else if(anComparatorToken.intern()==ASTRelationalExpr.GTE.intern())
                    theResult = new Boolean(true);
                else if(anComparatorToken.intern()==ASTRelationalExpr.LT.intern())
                    theResult = new Boolean(false);
                else //(anComparatorToken.intern()==ASTRelationalExpr.LTE.intern())
                    theResult = new Boolean(false);
        }
        else
        {
            IXPnodeSetValue  theNodeSetValue =
                (leftType==IXPnodeSetValue.ID ?
                 (IXPnodeSetValue) leftValue :
                 (IXPnodeSetValue) rightValue );

            IXPvalue theOtherValue = (leftType==IXPnodeSetValue.ID ?
                                      rightValue : leftValue );

            boolean nodeSetIsLeft = (leftType==IXPnodeSetValue.ID ?
                                     true : false );

            XPnumber theNumberFunction = new XPnumber(aContext);
            XPboolean theBooleanFunction = new XPboolean(aContext);
            if(theOtherValue.getTypeId()==IXPbooleanValue.ID)
            {
                IXPbooleanValue theOtherBooleanValue =
                    (IXPbooleanValue)theOtherValue;
                IXPbooleanValue theNodeSetAsBooleanValue =
                    theBooleanFunction.call(theNodeSetValue);
                boolean theOtherBoolean = theOtherBooleanValue.getValue().booleanValue();
                boolean theNodeSetAsBoolean = theNodeSetAsBooleanValue.getValue().booleanValue();
                int theOtherInt = (theOtherBoolean? 1 : 0);
                int theNodeSetAsInt = (theNodeSetAsBoolean? 1 : 0);
                if (theNodeSetAsInt == theOtherInt )
                    if(anComparatorToken.intern()==ASTRelationalExpr.GT.intern())
                        theResult = new Boolean(false);
                    else if(anComparatorToken.intern()==ASTRelationalExpr.GTE.intern())
                        theResult = new Boolean(true);
                    else if(anComparatorToken.intern()==ASTRelationalExpr.LT.intern())
                        theResult = new Boolean(false);
                    else //(anComparatorToken.intern()==ASTRelationalExpr.LTE.intern())
                        theResult = new Boolean(true);
                else if (theNodeSetAsInt < theOtherInt )
                    if(anComparatorToken.intern()==ASTRelationalExpr.GT.intern())
                        theResult = (nodeSetIsLeft ? new Boolean(false) : new Boolean(true));
                    else if(anComparatorToken.intern()==ASTRelationalExpr.GTE.intern())
                        theResult = (nodeSetIsLeft ? new Boolean(false) : new Boolean(true));
                    else if(anComparatorToken.intern()==ASTRelationalExpr.LT.intern())
                        theResult = (nodeSetIsLeft ? new Boolean(true) : new Boolean(false));
                    else //(anComparatorToken.intern()==ASTRelationalExpr.LTE.intern())
                        theResult = (nodeSetIsLeft ? new Boolean(true) : new Boolean(false));
                else
                    if(anComparatorToken.intern()==ASTRelationalExpr.GT.intern())
                        theResult = (nodeSetIsLeft ? new Boolean(true) : new Boolean(false));
                    else if(anComparatorToken.intern()==ASTRelationalExpr.GTE.intern())
                        theResult = (nodeSetIsLeft ? new Boolean(true) : new Boolean(false));
                    else if(anComparatorToken.intern()==ASTRelationalExpr.LT.intern())
                        theResult = (nodeSetIsLeft ? new Boolean(false) : new Boolean(true));
                    else //(anComparatorToken.intern()==ASTRelationalExpr.LTE.intern())
                        theResult = (nodeSetIsLeft ? new Boolean(false) : new Boolean(true));
            }
            else if(theOtherValue.getTypeId()==IXPnumberValue.ID)
            {
                    //convert one node after the other to string -> to number the
                    //first that compares correctly with the other number makes
                    //the expression true
                IXPnumberValue theOtherNumberValue =
                    (IXPnumberValue)theOtherValue;

                ArrayList theNodesInTheNodeSet = theNodeSetValue.getValue();
                Iterator aNodeListIterator = theNodesInTheNodeSet.iterator();

                while(aNodeListIterator.hasNext())
                {
                    Node aNode = (Node)aNodeListIterator.next();
                    String aNodeAsString = XPstring.nodeToString(aNode);
                    Number aNodeAsNumber = XPnumber.stringToNumber(aNodeAsString);
                    XPnumberValue aNodeaAsNumberValue =
                        new XPnumberValue(aNodeAsNumber);
                    int result = theOtherNumberValue.compareTo(aNodeaAsNumberValue);
                    if (result==0)//both equal
                        if(anComparatorToken.intern()==ASTRelationalExpr.GT.intern())
                            theResult = new Boolean(false);
                        else if(anComparatorToken.intern()==ASTRelationalExpr.GTE.intern())
                        {
                            theResult = new Boolean(true);
                            break;
                        }
                        else if(anComparatorToken.intern()==ASTRelationalExpr.LT.intern())
                            theResult = new Boolean(false);
                        else //(anComparatorToken.intern()==ASTRelationalExpr.LTE.intern())
                        {
                            theResult = new Boolean(true);
                            break;
                        }
                    else if (result < 0)//the other is smaller
                        if(anComparatorToken.intern()==ASTRelationalExpr.GT.intern())
                        {
                            theResult = (nodeSetIsLeft ? new Boolean(true) : new Boolean(false));
                            if(theResult.booleanValue())
                                break;
                        }
                        else if(anComparatorToken.intern()==ASTRelationalExpr.GTE.intern())
                        {
                            theResult = (nodeSetIsLeft ? new Boolean(true) : new Boolean(false));
                            if(theResult.booleanValue())
                                break;
                        }
                        else if(anComparatorToken.intern()==ASTRelationalExpr.LT.intern())
                        {
                            theResult = (nodeSetIsLeft ? new Boolean(false) : new Boolean(true));
                            if(theResult.booleanValue())
                                break;
                        }
                        else //(anComparatorToken.intern()==ASTRelationalExpr.LTE.intern())
                        {
                            theResult = (nodeSetIsLeft ? new Boolean(false) : new Boolean(true));
                            if(theResult.booleanValue())
                                break;
                        }
                    else// the other is bigger
                        if(anComparatorToken.intern()==ASTRelationalExpr.GT.intern())
                        {
                            theResult = (nodeSetIsLeft ? new Boolean(false) : new Boolean(true));
                            if(theResult.booleanValue())
                                break;
                        }
                        else if(anComparatorToken.intern()==ASTRelationalExpr.GTE.intern())
                        {
                            theResult = (nodeSetIsLeft ? new Boolean(false) : new Boolean(true));
                            if(theResult.booleanValue())
                                break;
                        }
                        else if(anComparatorToken.intern()==ASTRelationalExpr.LT.intern())
                        {
                            theResult = (nodeSetIsLeft ? new Boolean(true) : new Boolean(false));
                            if(theResult.booleanValue())
                                break;
                        }
                        else //(anComparatorToken.intern()==ASTRelationalExpr.LTE.intern())
                        {
                            theResult = (nodeSetIsLeft ? new Boolean(true) : new Boolean(false));
                            if(theResult.booleanValue())
                                break;
                        }
                }
            }
            else if(theOtherValue.getTypeId()==IXPstringValue.ID)
            {
                    //convert one node after the other to string -> to number the
                    //first that compares correctly with the other string-> to number
                    //makes the expression true
                IXPstringValue theOtherStringValue =
                    (IXPstringValue)theOtherValue;
                Number theOtherStringAsNumber =
                    XPnumber.stringToNumber(theOtherStringValue.getValue());

                ArrayList theNodesInTheNodeSet = theNodeSetValue.getValue();
                Iterator aNodeListIterator = theNodesInTheNodeSet.iterator();

                while(aNodeListIterator.hasNext())
                {
                    Node aNode = (Node)aNodeListIterator.next();
                    String aNodeAsString = XPstring.nodeToString(aNode);
                    Number aNodeAsNumber = XPnumber.stringToNumber(aNodeAsString);
                    XPnumberValue aNodeaAsNumberValue =
                        new XPnumberValue(aNodeAsNumber);
                    XPnumberValue aStringAsNumberValue  =
                        new XPnumberValue(theOtherStringAsNumber);

                    int result =
                        aStringAsNumberValue.compareTo(aNodeaAsNumberValue);

                    if (result==0)//both equal
                        if(anComparatorToken.intern()==ASTRelationalExpr.GT.intern())
                            theResult = new Boolean(false);
                        else if(anComparatorToken.intern()==ASTRelationalExpr.GTE.intern())
                        {
                            theResult = new Boolean(true);
                            break;
                        }
                        else if(anComparatorToken.intern()==ASTRelationalExpr.LT.intern())
                            theResult = new Boolean(false);
                        else //(anComparatorToken.intern()==ASTRelationalExpr.LTE.intern())
                        {
                            theResult = new Boolean(true);
                            break;
                        }
                    else if (result < 0)//the other is smaller
                        if(anComparatorToken.intern()==ASTRelationalExpr.GT.intern())
                        {
                            theResult = (nodeSetIsLeft ? new Boolean(true) : new Boolean(false));
                            if(theResult.booleanValue())
                                break;
                        }
                        else if(anComparatorToken.intern()==ASTRelationalExpr.GTE.intern())
                        {
                            theResult = (nodeSetIsLeft ? new Boolean(true) : new Boolean(false));
                            if(theResult.booleanValue())
                                break;
                        }
                        else if(anComparatorToken.intern()==ASTRelationalExpr.LT.intern())
                        {
                            theResult = (nodeSetIsLeft ? new Boolean(false) : new Boolean(true));
                            if(theResult.booleanValue())
                                break;
                        }
                        else //(anComparatorToken.intern()==ASTRelationalExpr.LTE.intern())
                        {
                            theResult = (nodeSetIsLeft ? new Boolean(false) : new Boolean(true));
                            if(theResult.booleanValue())
                                break;
                        }
                    else// the other is bigger
                        if(anComparatorToken.intern()==ASTRelationalExpr.GT.intern())
                        {
                            theResult = (nodeSetIsLeft ? new Boolean(false) : new Boolean(true));
                            if(theResult.booleanValue())
                                break;
                        }
                        else if(anComparatorToken.intern()==ASTRelationalExpr.GTE.intern())
                        {
                            theResult = (nodeSetIsLeft ? new Boolean(false) : new Boolean(true));
                            if(theResult.booleanValue())
                                break;
                        }
                        else if(anComparatorToken.intern()==ASTRelationalExpr.LT.intern())
                        {
                            theResult = (nodeSetIsLeft ? new Boolean(true) : new Boolean(false));
                            if(theResult.booleanValue())
                                break;
                        }
                        else //(anComparatorToken.intern()==ASTRelationalExpr.LTE.intern())
                        {
                            theResult = (nodeSetIsLeft ? new Boolean(true) : new Boolean(false));
                            if(theResult.booleanValue())
                                break;
                        }
                }
            }
            else if(theOtherValue.getTypeId()==IXPtreeFragmentValue.ID)
            {
                    //convert one node after the other to string -> to number the
                    //first that compares correctly with the other
                    //treeFragment->to string-> to number makes the expression true
                IXPtreeFragmentValue theOtherTreeFragmentValue =
                    (IXPtreeFragmentValue)theOtherValue;
                XPstring theStringFunction = new XPstring(aContext);
                IXPstringValue theTreeFragmentAsString =
                    theStringFunction.call(theOtherTreeFragmentValue);

                Number theOtherTreeFragmentAsNumber =
                    XPnumber.stringToNumber(theTreeFragmentAsString.getValue());

                ArrayList theNodesInTheNodeSet = theNodeSetValue.getValue();
                Iterator aNodeListIterator = theNodesInTheNodeSet.iterator();

                while(aNodeListIterator.hasNext())
                {
                    Node aNode = (Node)aNodeListIterator.next();
                    String aNodeAsString = XPstring.nodeToString(aNode);
                    Number aNodeAsNumber = XPnumber.stringToNumber(aNodeAsString);
                    XPnumberValue aNodeaAsNumberValue =
                        new XPnumberValue(aNodeAsNumber);
                    XPnumberValue aTreeFragmentAsNumberValue  =
                        new XPnumberValue(theOtherTreeFragmentAsNumber);

                    int result =
                        aTreeFragmentAsNumberValue.compareTo(aNodeaAsNumberValue);

                    if (result==0)//both equal
                        if(anComparatorToken.intern()==ASTRelationalExpr.GT.intern())
                            theResult = new Boolean(false);
                        else if(anComparatorToken.intern()==ASTRelationalExpr.GTE.intern())
                        {
                            theResult = new Boolean(true);
                            break;
                        }
                        else if(anComparatorToken.intern()==ASTRelationalExpr.LT.intern())
                            theResult = new Boolean(false);
                        else //(anComparatorToken.intern()==ASTRelationalExpr.LTE.intern())
                        {
                            theResult = new Boolean(true);
                            break;
                        }
                    else if (result < 0)//the other is smaller
                        if(anComparatorToken.intern()==ASTRelationalExpr.GT.intern())
                        {
                            theResult = (nodeSetIsLeft ? new Boolean(true) : new Boolean(false));
                            if(theResult.booleanValue())
                                break;
                        }
                        else if(anComparatorToken.intern()==ASTRelationalExpr.GTE.intern())
                        {
                            theResult = (nodeSetIsLeft ? new Boolean(true) : new Boolean(false));
                            if(theResult.booleanValue())
                                break;
                        }
                        else if(anComparatorToken.intern()==ASTRelationalExpr.LT.intern())
                        {
                            theResult = (nodeSetIsLeft ? new Boolean(false) : new Boolean(true));
                            if(theResult.booleanValue())
                                break;
                        }
                        else //(anComparatorToken.intern()==ASTRelationalExpr.LTE.intern())
                        {
                            theResult = (nodeSetIsLeft ? new Boolean(false) : new Boolean(true));
                            if(theResult.booleanValue())
                                break;
                        }
                    else// the other is bigger
                        if(anComparatorToken.intern()==ASTRelationalExpr.GT.intern())
                        {
                            theResult = (nodeSetIsLeft ? new Boolean(false) : new Boolean(true));
                            if(theResult.booleanValue())
                                break;
                        }
                        else if(anComparatorToken.intern()==ASTRelationalExpr.GTE.intern())
                        {
                            theResult = (nodeSetIsLeft ? new Boolean(false) : new Boolean(true));
                            if(theResult.booleanValue())
                                break;
                        }
                        else if(anComparatorToken.intern()==ASTRelationalExpr.LT.intern())
                        {
                            theResult = (nodeSetIsLeft ? new Boolean(true) : new Boolean(false));
                            if(theResult.booleanValue())
                                break;
                        }
                        else //(anComparatorToken.intern()==ASTRelationalExpr.LTE.intern())
                        {
                            theResult = (nodeSetIsLeft ? new Boolean(true) : new Boolean(false));
                            if(theResult.booleanValue())
                                break;
                        }
                }
            }
            else //node sets
            {
                IXPnodeSetValue  theLeftNodeSetValue =
                    (IXPnodeSetValue) leftValue;
                IXPnodeSetValue  theRightNodeSetValue =
                    (IXPnodeSetValue) rightValue;
                XPmin theMinFunction = new XPmin(aContext);
                XPmax theMaxFunction = new XPmax(aContext);
                IXPnumberValue leftMin = theMinFunction.call(theLeftNodeSetValue);
                IXPnumberValue leftMax = theMaxFunction.call(theLeftNodeSetValue);
                IXPnumberValue rightMin = theMinFunction.call(theRightNodeSetValue);
                IXPnumberValue rightMax = theMaxFunction.call(theRightNodeSetValue);
                if(anComparatorToken.intern()==ASTRelationalExpr.GT.intern())
                {
                    theResult = new Boolean(leftMax.compareTo(rightMin)>0);
                }
                else if(anComparatorToken.intern()==ASTRelationalExpr.GTE.intern())
                {
                    theResult = new Boolean(leftMax.compareTo(rightMin)>=0);
                }
                else if(anComparatorToken.intern()==ASTRelationalExpr.LT.intern())
                {
                    theResult = new Boolean(leftMin.compareTo(rightMax)<0);
                }
                else //(anComparatorToken.intern()==ASTRelationalExpr.LTE.intern())
                {
                    theResult = new Boolean(leftMin.compareTo(rightMax)<=0);
                }
            }
        }

        return theResult;
    }

        /**
         * If existing (more than one child) evaluates always to the sum of all
         * childs after converting them to a number.
         * XPath production:
         *
         * [25] AdditiveExpr            ::= MultiplicativeExpr
         *                                | AdditiveExpr '+' MultiplicativeExpr
         *                                | AdditiveExpr '-' MultiplicativeExpr
         *
         *
         * Used in:
         *
         * [24] RelationalExpr
         *
         * In the jcc gramar file the production was realized as:
         *
         * void AdditiveExpr() :
         * {
         *      Token anOperator;
         * }
         * {
         *  (MultiplicativeExpr()
         *    (
         *        (
         *            anOperator=<PLUS>
         *              | anOperator=<MINUS>
         *          )
         *          { jjtThis.addOperator(anComparator.image); }
         *          MultiplicativeExpr()
         *      )*
         *   )
         *   #AdditiveExpr(>1)
         * }
         *
         */
    public Object visit(ASTAdditiveExpr anAdditiveNode, Object data)
    {
        XPathContext aContext = (XPathContext)data;

            //What ever we are if we exist/are visitited the
            //result will be a number
        XPnumber theNumberFunction = new XPnumber(aContext);

        Number theResult = null;

        int numOfChildren = anAdditiveNode.jjtGetNumChildren();

            //the first step; Type of operants is still undefined
        SimpleNode leftOperant = (SimpleNode)anAdditiveNode.jjtGetChild(0);
        SimpleNode rightOperant = (SimpleNode)anAdditiveNode.jjtGetChild(1);

        String anOperatorToken = anAdditiveNode.getOperatorToken(0);

        XPnumberValue theLeftResult = new XPnumberValue(theResult);

        XPathContext aLeftEvaluationContext =
            new XPathContext(   aContext.getContext(),
                                aContext.getCurrentNodeIndex());

           //handover the variables
        aLeftEvaluationContext.setVariables(aContext.getVariables());
        
        XPathContext aRightEvaluationContext =
            new XPathContext(   aContext.getContext(),
                                aContext.getCurrentNodeIndex());

           //handover the variables
        aRightEvaluationContext.setVariables(aContext.getVariables());
        
        leftOperant.jjtAccept(this,aLeftEvaluationContext);
        rightOperant.jjtAccept(this,aRightEvaluationContext);

        IXPvalue leftValue = aLeftEvaluationContext.getValue();
        IXPvalue rightValue = aRightEvaluationContext.getValue();

        theResult = calculateValue( leftValue,
                                    rightValue,
                                    anOperatorToken,
                                    aContext);

            //in case we got more to do
        for(int i = 2; i < numOfChildren; i++)
        {
            leftValue = new XPnumberValue(theResult);
            aRightEvaluationContext =
                new XPathContext(   aContext.getContext(),
                                    aContext.getCurrentNodeIndex());
                //handover the variables
            aRightEvaluationContext.setVariables(aContext.getVariables());
            
            rightOperant = (SimpleNode)anAdditiveNode.jjtGetChild(i);
            anOperatorToken = anAdditiveNode.getOperatorToken(i-1);

            rightOperant.jjtAccept(this,aRightEvaluationContext);
            rightValue = aRightEvaluationContext.getValue();
            theResult = calculateValue(leftValue,
                                       rightValue,
                                       anOperatorToken,
                                       aContext);
        }

        aContext.setNumberValue(theResult);

        return aContext;
    }

    private Number calculateValue( IXPvalue leftValue,
                                   IXPvalue rightValue,
                                   String anOperatorToken,
                                   XPathContext aContext)
    {
        int leftType = leftValue.getTypeId();
        int rightType = rightValue.getTypeId();
        if(leftType != IXPnumberValue.ID || rightType != IXPnumberValue.ID)
            throw new RuntimeException(
                "Adding nonnumerical values not allowed!");
        IXPnumberValue leftNumberValue = (IXPnumberValue)leftValue;
        IXPnumberValue rightNumberValue = (IXPnumberValue)rightValue;
        Number leftNumber = leftNumberValue.getValue();
        Number rightNumber = rightNumberValue.getValue();
        Number theResult = null;
        if(leftNumber instanceof Integer && rightNumber instanceof Integer)
        {
            if(anOperatorToken.intern()==ASTAdditiveExpr.PLUS.intern())
                theResult = new Integer(leftNumber.intValue()+
                                        rightNumber.intValue());
            else if(anOperatorToken.intern()==ASTAdditiveExpr.MINUS.intern())
                theResult = new Integer(leftNumber.intValue()-
                                        rightNumber.intValue());
            else if(anOperatorToken.intern()==ASTMultiplicativeExpr.MULT.intern())
                theResult = new Integer(leftNumber.intValue()*
                                        rightNumber.intValue());
            else if(anOperatorToken.intern()==ASTMultiplicativeExpr.DIV.intern())
                theResult = new Integer(leftNumber.intValue()/
                                        rightNumber.intValue());
            else if(anOperatorToken.intern()==ASTMultiplicativeExpr.MOD.intern())
                theResult = new Integer(leftNumber.intValue() %
                                        rightNumber.intValue());
        }
        else
        {
            if(anOperatorToken.intern()==ASTAdditiveExpr.PLUS.intern())
                theResult = new Double(leftNumber.doubleValue()+
                                       rightNumber.doubleValue());
            else if(anOperatorToken.intern()==ASTAdditiveExpr.MINUS.intern())
                theResult =theResult = new Double(leftNumber.doubleValue()-
                                                  rightNumber.doubleValue());
            else if(anOperatorToken.intern()==ASTMultiplicativeExpr.MULT.intern())
                theResult = new Double(leftNumber.doubleValue()*
                                       rightNumber.doubleValue());
            else if(anOperatorToken.intern()==ASTMultiplicativeExpr.DIV.intern())
                theResult = new Double(leftNumber.doubleValue()/
                                       rightNumber.doubleValue());
            else if(anOperatorToken.intern()==ASTMultiplicativeExpr.MOD.intern())
                theResult = new Double(leftNumber.doubleValue() %
                                       rightNumber.doubleValue());
        }
        return theResult;
    }

        /**
         * If existing (more than one child) evaluates always to the "product" of
         * all children after converting them to number.
         * XPath production:
         *
         *  [26] MultiplicativeExpr      ::= UnaryExpr
         *                                | MultiplicativeExpr MultiplyOperator UnaryExpr
         *                                | MultiplicativeExpr 'div' UnaryExpr
         *                                | MultiplicativeExpr 'mod' UnaryExpr
         *
         * Used in :
         *
         * [25] AdditiveExpr
         *
         * In the jcc gramar file the production was realized as:
         *
         *  void MultiplicativeExpr() :
         *  {
         *        Token anOperator;
         *  }
         *  {
         *    (UnaryExpr()
         *        (
         *            (
         *                  anOperator="*"
         *                  | anOperator=<DIV>
         *                  | anOperator=<MOD>
         *              )
         *              { jjtThis.addOperator(anComparator.image); }
         *              UnaryExpr()
         *          )*
         *      )
         *      #MultiplicativeExpr(>1)
         *  }
         */
    public Object visit(ASTMultiplicativeExpr aMultiplikativeNode, Object data)
    {
        XPathContext aContext = (XPathContext)data;

            //What ever we are if we exist/are visitited the
            //result will be a number
        XPnumber theNumberFunction = new XPnumber(aContext);

        Number theResult = null;

        int numOfChildren = aMultiplikativeNode.jjtGetNumChildren();

            //the first step; Type of operants is still undefined
        SimpleNode leftOperant = (SimpleNode)aMultiplikativeNode.jjtGetChild(0);
        SimpleNode rightOperant = (SimpleNode)aMultiplikativeNode.jjtGetChild(1);

        String anOperatorToken = aMultiplikativeNode.getOperatorToken(0);

        XPnumberValue theLeftResult = new XPnumberValue(theResult);

        XPathContext aLeftEvaluationContext =
            new XPathContext(   aContext.getContext(),
                                aContext.getCurrentNodeIndex());

            //handover the variables
        aLeftEvaluationContext.setVariables(aContext.getVariables());
        
        XPathContext aRightEvaluationContext =
            new XPathContext(   aContext.getContext(),
                                aContext.getCurrentNodeIndex());

            //handover the variables
        aRightEvaluationContext.setVariables(aContext.getVariables());

        leftOperant.jjtAccept(this,aLeftEvaluationContext);
        rightOperant.jjtAccept(this,aRightEvaluationContext);

        IXPvalue leftValue = aLeftEvaluationContext.getValue();
        IXPvalue rightValue = aRightEvaluationContext.getValue();

        theResult = calculateValue(leftValue,
                                   rightValue,
                                   anOperatorToken,
                                   aContext);

            //in case we got more to do
        for(int i = 2; i < numOfChildren; i++)
        {
            leftValue = new XPnumberValue(theResult);
            aRightEvaluationContext =
                new XPathContext(   aContext.getContext(),
                                    aContext.getCurrentNodeIndex());
            
                //handover the variables
            aRightEvaluationContext.setVariables(aContext.getVariables());
            
            rightOperant = (SimpleNode)aMultiplikativeNode.jjtGetChild(i);
            anOperatorToken = aMultiplikativeNode.getOperatorToken(i-1);

            rightOperant.jjtAccept(this,aRightEvaluationContext);
            rightValue = aRightEvaluationContext.getValue();
            theResult = calculateValue(leftValue,
                                       rightValue,
                                       anOperatorToken,
                                       aContext);
        }

        aContext.setNumberValue(theResult);

        return aContext;
    }

        /**
         * We are on our way down the expression, the XPath context will be used as
         * evaluation stack with the current node set to evaluate the expression.
         *
         * XPath production:
         *
         * [27] UnaryExpr               ::= UnionExpr
         *                                | '-' UnaryExpr
         *  used in:
         *
         * [26] MultiplicativeExpr
         *
         * In the jj-c gramar file the production was realized as:
         *
         * void UnaryExpr() #UnaryExpr :
         * {}
         * {
         *      UnionExpr()
         *      | <MINUS> UnaryExpr() { jjtThis.setMinus(); }
         * }
         *
         */
    public Object visit(ASTUnaryExpr node, Object data)
    {
        XPathContext aContext = (XPathContext) data;

            //let the chilfden evaluate the expression
        node.childrenAccept(this,aContext);

        if(node.isMinus())//makes only sense for numeric experssions
        {
            IXPnumberValue aNumberValue = null;
            if(aContext.getValue().getTypeId()!=IXPnumberValue.ID)
            {
                XPnumber theNumberFunction = new XPnumber(aContext);
                aNumberValue = theNumberFunction.call();
            }
            else
            {
                aNumberValue
                    = ((IXPnumberValue)aContext.getValue());
            }
            aNumberValue.multiplyBy(-1);
            aContext.setValue(aNumberValue);            
        }
        return aContext;
    }


    public Object visit(ASTVariableReference node, Object data)
    {
        XPathContext aContext = (XPathContext) data;

        String theVariableName = node.getVariableName();
        
        IXPvalue aVariable = aContext.getVariable(theVariableName);
        
        aContext.setValue(aVariable);

        return aContext;
    }

        /**
         * Nodes that are ancestors of the orgin node in the XPath tree model.
         * XPath type of origin node: types of the result set.
         * Root         : Empty.
         * Attribute    : Element ancestors up to the root
         * NameSpace (??TODO)    : Element or attribute parent plus
         *                ancestors up to the root.
         * Element      : The element ancestors up to the root
         * Comment      : The parent (root or element)
         *                plus all the element ancestors up to the root.
         * Text         : The element ancestors up to the root.
         * Processing
         * Instruction  : The parent (element or root) plus all the element
         *                ancestors up to the root.
         */
    ArrayList getAncestorAxis(Node aNode)
    {
        ArrayList theAncestors = new ArrayList();
        short theType = aNode.getNodeType();

        switch (theType) {
            case Node.ATTRIBUTE_NODE :
                Node aChild = aNode;
                Node aParent  = ((Attr)aNode).getOwnerElement();
                if(   aParent != null )
                    theAncestors.add(aParent);
                aChild = aParent;
                aParent = null;
                do{
                    if( aChild != null)
                        aParent = aChild.getParentNode();
                    if( aParent != null )
                        theAncestors.add(aParent);
                    aChild = aParent;
                }
                while(aChild!=null);
                break;
            case Node.COMMENT_NODE :
            case Node.DOCUMENT_NODE :
            case Node.ELEMENT_NODE :
            case Node.PROCESSING_INSTRUCTION_NODE :
            case Node.TEXT_NODE :
            default:
                aChild = aNode;
                aParent = null;
                do{
                    if( aChild != null)
                        aParent = aChild.getParentNode();
                    if( aParent != null )
                        theAncestors.add(aParent);
                    aChild = aParent;
                }
                while(aChild!=null);
        }
        return theAncestors;
    }

        /**
         * Nodes that are ancestors of the orgin node (plus the orignal node)
         * in the XPath tree model.
         * XPath type of origin node: types of the result set.
         * Root         : root.
         * Attribute    : Attribute plus Element ancestors up to the root
         * NameSpace (??TODO) : NameSpace plus element or attribute parent plus
         *                ancestors up to the root.
         * Element      : The element plus all the element ancestors up to the root
         * Comment      : The comment plus his parent (root or element)
         *                plus all the element ancestors up to the root.
         * Text         : The text plus his Element ancestors up to the root.
         * Processing
         * Instruction  : The procerssing instruction plus his parent
         *                (element or root) plus all the element ancestors up to
         *                the root.
         */
    ArrayList getAncestorOrSelfAxis(Node aNode)
    {
        ArrayList theAncestors = this.getAncestorAxis(aNode);
        theAncestors.add(0,aNode);
        return theAncestors;
    }

        /**
         * The attributes of aNode if aNode is an element otherwise empty.
         *
         * @param aNode a <code>Node</code> whose attribute axis is queryied
         * @return an <code>ArrayList</code> containing the attributes
         */
    ArrayList getAttributeAxis(Node aNode)
    {
        ArrayList theResult = new ArrayList();
        if (aNode.getNodeType()==Node.ELEMENT_NODE ) 
        {
            Element anElement = (Element)aNode;
            NamedNodeMap anAttrMap = anElement.getAttributes();
            int size = anAttrMap.getLength();
            for ( int i = 0; i < size; i++) 
            {
                Attr anAttr = (Attr) anAttrMap.item(i);
                theResult.add(anAttr);
            } // end of for ()
        } // end of if ()

            //sort by name for convenience
        Comparator aComparator = new Comparator()
            {
                public boolean equals(Object anAttr)
                {
                    return this.equals(anAttr);
                }
                
                public int hashCode() {
                	return super.hashCode();
                }

                public int compare(Object anAttr, Object anotherAttr)
                {
                    int theResult = 0;
                    Attr anAttribute = (Attr)anAttr;
                    Attr anOtherAttribute =(Attr)anotherAttr;
                    theResult = anAttribute.getNodeName().compareTo(anOtherAttribute.getNodeName());
                    return theResult;
                }
            };
        Collections.sort(theResult, aComparator);
        return theResult;
    }

        /**
         * Nodes that are descendants of the orgin node in the XPath tree model.
         * XPath type of origin node: types of the result set.
         * Root         : All nodes except the root, attributes and name space
         *                nodes in document order.
         * Attribute    : Empty.
         * NameSpace(??)    : Empty.
         * Element      : All nodes except the element, attributes and name space
         *                nodes in document order that are "contained in the
         *                element.
         * Comment      : Empty.
         * Text         : Empty.
         * Processing
         * Instruction  : Empty.
         */
    ArrayList getDescendantAxis(Node aNode)
    {
        ArrayList theChildsArray = new ArrayList();
        ArrayList theDescandants = new ArrayList();
        NodeList theChilds = aNode.getChildNodes();
        int length = theChilds.getLength();
        for(int i = 0; i < length ; i++)
        {
            theChildsArray.add(theChilds.item(i));
        }

        addSelfAndAllChilds(theDescandants,theChildsArray);

        return theDescandants;
    }

        /**
         * Nodes that are descendants of the orgin node in the XPath tree model.
         * plus the orginal node. see getDescendantAxis()
         */
    ArrayList getDescendantOrSelf(Node aNode)
    {
        ArrayList theDescendants = this.getDescendantAxis(aNode);
        theDescendants.add(0,aNode);
        return theDescendants;
    }

        /**
         * Nodes that "start" following (i.e.after the orgin has closed) the
         * origin node.
         * Type of origin node: types of the result set.
         * Root         : Empty
         * Attribute    : ?
         * NameSpace(??): ?
         * Element      : All the elements, text nodes, processing instructions,
         *                comments, in document order that "start" after the current
         *                element has ended ("tag has closed").
         * Comment      : All the elements, text nodes, processing instructions,
         *                comments, in document order that "start" after the current
         *                element has ended ("tag has closed").
         * Processing
         * Instruction  : All the elements, text nodes, processing instructions,
         *                comments, in document order that "start" after the current
         *                element has ended ("tag has closed").
         * Text         : All the elements, text nodes, processing instructions,
         *                comments, in document order that "start" after the current
         *                element has ended ("tag has closed").
         */
    ArrayList getFollowingAxis(Node aNode)
    {
        ArrayList theFollowings = new ArrayList();
        short theType = aNode.getNodeType();
        switch (theType)
        {
            case Node.ATTRIBUTE_NODE :
                    //do nothing so far
                break;
            case Node.DOCUMENT_NODE :
                    //do nothing
                break;
            case Node.COMMENT_NODE :
                    //do default
            case Node.TEXT_NODE :
                    //do default
            case Node.PROCESSING_INSTRUCTION_NODE :
                    //do default
            case Node.ELEMENT_NODE :
                    //do default
            default:
            {
                    // find the starting point
                Node aFollowing = this.getNextFollowing(aNode,false);
                addSelfAndFollowings(theFollowings,aFollowing);
            }
            break;
        }
        return theFollowings;
    }

        /**
         *  Nodes with the same parent following the current node in document order.
         *  Type of origin node: types in the result set.
         *  Root        : Empty
         *  Attribute   : Empty
         *  Namespace(??)   : Empty
         *  Element     : All elements, comments, text nodes, processing
         *                instructions following with the same parent.
         *  Comment     : All elements, comments, text nodes, processing
         *                instructions following with the same parent.
         *  Processing
         *  Instruction : All elements, comments, text nodes, processing
         *                instructions following with the same parent.
         */
    ArrayList getFollowingSibillingAxis(Node aNode)
    {
        ArrayList theFollowingSibillings = new ArrayList();

        short theType = aNode.getNodeType();

        switch (theType)
        {
            case Node.DOCUMENT_NODE :
                    //Do nothing
                break;
            case Node.ATTRIBUTE_NODE :
                    //Do nothing
                break;
            case Node.ELEMENT_NODE :
                    // Do default
            case Node.COMMENT_NODE :
                    // Do default
            case Node.TEXT_NODE :
                    // Do default
            case Node.PROCESSING_INSTRUCTION_NODE :
                    // Do default
            default:
                do
                {
                    Node aSibilling = aNode.getNextSibling();
                    if(aSibilling!=null)
                        theFollowingSibillings.add(aSibilling);
                    aNode = aSibilling;
                }
                while(aNode!=null);
                break;
        }
        return theFollowingSibillings;
    }

        /**
         * if aNode is of type element, returns the namespace nodes 
         *associated with the node otherise an empty list.
         */
    HashMap getNameSpaceAxis(Node aNode)
    {
        HashMap theResult = (HashMap)theNamespaceNodeAxes.get(aNode);
        
        if (theResult!=null)
        {
            return theResult;
        } // end of if ()
        
        theResult = new HashMap();
        if (aNode.getNodeType()==Node.ELEMENT_NODE) 
        {
                //the implicit xml namespace
            Attr theImplicitXMLNamespace = aNode.getOwnerDocument().createAttribute("xmlns:xml");
            theImplicitXMLNamespace.setValue("http://www.w3.org/XML/1998/namespace");
            Element aStartElement = (Element) aNode;            
            NamespaceNode theNSNode = new NamespaceNode(theImplicitXMLNamespace,aStartElement);
            theResult.put("xml",theNSNode);
            ArrayList theAncestors = this.getAncestorOrSelfAxis(aStartElement);
            int size = theAncestors.size();
            for ( int i = size-2; i > -1 ; i--) //exclude the root  
            {
                Element anAncestor = (Element)theAncestors.get(i);
                ArrayList theAttributes = getAttributeAxis(anAncestor);
                Iterator anAttribIter = theAttributes.iterator();
                while ( anAttribIter.hasNext()) 
                {
                    Attr anAttr = (Attr) anAttribIter.next();
                    if (anAttr.getNodeName().equals("xmlns")) //defaultNameSpace
                    {
                        if (i!=0)//not for self
                        {
                            Attr theClone = (Attr)anAttr.cloneNode(false);
                            NamespaceNode aNSnode = new NamespaceNode(theClone,aStartElement);
                            theResult.put("",aNSnode);
                        }
                        else 
                        {
                            NamespaceNode aNSNode = new NamespaceNode(anAttr,aStartElement);
                            theResult.put("", aNSNode);
                        } // end of else
                        if ( anAttr.getValue().intern()=="".intern())//removal of default namespace
                        {
                            theResult.remove("");
                        } // end of if ()
                    } 
                    else if (anAttr.getNodeName().indexOf("xmlns:")!=-1) //nameSpace
                    {
                        String thePrefix = anAttr.getNodeName().substring(6);//xmlns:
                        if (i!=0)//not for self
                        {
                            Attr theClone = (Attr)anAttr.cloneNode(false);
                            NamespaceNode aNSnode = new NamespaceNode(theClone,aStartElement);
                            theResult.put(thePrefix,aNSnode);
                        }
                        else 
                        {
                            NamespaceNode aNSnode = new NamespaceNode(anAttr,aStartElement);                            
                            theResult.put(thePrefix, aNSnode);
                        } // end of else                                                
                    } // end of if ()
                } // end of while ()
            } // end of for ()
        } // otherwise there is nothing
        this.theNamespaceNodeAxes.put(aNode, theResult);
        return theResult;
    }

        /**
         * Nodes that "start" and "close" preceding the origin node. Starting with
         * the element that started last.
         * Type of origin node: types of the result set.
         * Root         : Empty
         * Attribute    : ?
         * NameSpace(??): ?
         * Element      : All the elements, text nodes, processing instructions,
         *                comments, in document order that "start" after the current
         *                element has ended ("tag has closed").
         * Comment      : All the elements, text nodes, processing instructions,
         *                comments, in document order that "start" after the current
         *                element has ended ("tag has closed").
         * Processing
         * Instruction  : All the elements, text nodes, processing instructions,
         *                comments, in document order that "start" after the current
         *                element has ended ("tag has closed").
         * Text         : All the elements, text nodes, processing instructions,
         *                comments, in document order that "start" after the current
         *                element has ended ("tag has closed").
         */

    ArrayList getPrecedingAxis(Node aNode)
    {
        ArrayList thePrecedings = new ArrayList();
        short theType = aNode.getNodeType();
        switch (theType)
        {
            case Node.DOCUMENT_NODE :
                    //Do nothing
                break;
            case Node.ATTRIBUTE_NODE :
                    //Do nothing
                break;
            case Node.ELEMENT_NODE :
                    // Do default
            case Node.COMMENT_NODE :
                    // Do default
            case Node.TEXT_NODE :
                    // Do default
            case Node.PROCESSING_INSTRUCTION_NODE :
                    // Do default
            default:
            {
                    // find the starting point
                ArrayList theAncestors = this.getAncestorAxis(aNode);
                Node aPreceding = this.getNextPreceding(aNode,theAncestors);
                addSelfAndPrecedings(thePrecedings,aPreceding,theAncestors);
            }
            break;
        }
        return thePrecedings;
    }

        /**
         *  Nodes with the same parent preceding the current node in reverse
         *  document order in the XPath tree model.
         *  Type of origin node: types in the result set.
         *  Root        : Empty
         *  Attribute   : Empty
         *  Namespace(??)   : Empty
         *  Element     : All elements, comments, text nodes, processing
         *                instructions preceding with the same parent.
         *  Comment     : All elements, comments, text nodes, processing
         *                instructions preceding with the same parent.
         *  Processing
         *  Instruction : All elements, comments, text nodes, processing
         *                instructions preceding with the same parent.
         */
    ArrayList getPrecedingSibillingAxis(Node aNode)
    {
        ArrayList thePrecedingSibillings = new ArrayList();

        short theType = aNode.getNodeType();

        switch (theType)
        {
            case Node.DOCUMENT_NODE :
                    //Do nothing
                break;
            case Node.ATTRIBUTE_NODE :
                    //Do nothing
                break;
            case Node.ELEMENT_NODE :
                    // Do default
            case Node.COMMENT_NODE :
                    // Do default
            case Node.TEXT_NODE :
                    // Do default
            case Node.PROCESSING_INSTRUCTION_NODE :
                    // Do default
            default:
                do
                {
                    Node aSibilling = aNode.getPreviousSibling();
                    if(aSibilling!=null)
                        thePrecedingSibillings.add(aSibilling);
                    aNode = aSibilling;
                }
                while(aNode!=null);
                break;
        }
        return thePrecedingSibillings;

    }

    ArrayList getAllNodes()
    {
        ArrayList allNodes = new ArrayList();
        ArrayList theDocumentElement = new ArrayList();
        theDocumentElement.add(theDocument);
        addSelfAndAllChilds(allNodes,theDocumentElement);
        return allNodes;
    }

    void addSelfAndAllChilds(ArrayList theNodeListToBuild, ArrayList theParentList)
    {
        for(int k=0; k < theParentList.size(); k++)
        {
            Node aParentNode = (Node) theParentList.get(k);
            theNodeListToBuild.add(aParentNode);
            NodeList theChilds = aParentNode.getChildNodes();
            ArrayList theChildsArray = new ArrayList();
            for(int i = 0; i < theChilds.getLength(); i++ )
            {
                theChildsArray.add(theChilds.item(i));
            }
            addSelfAndAllChilds(theNodeListToBuild,theChildsArray);
        }
        return;
    }

        /**
         * Adds all nodes including the start node and its descendants to the list.
         * So the list is the list of nodes in document order that are the
         * followings of the node <emph>preceding</emph> aStartNode.
         *
         */
    void addSelfAndFollowings(ArrayList theNodeListToBuild, Node aStartNode)
    {
        if(aStartNode == null)
            return;
        do
        {
            int theId = aStartNode.getNodeType();
            boolean isXPathFollowingNode = theId == Node.ELEMENT_NODE ||
                theId ==Node.DOCUMENT_NODE  || 
                theId ==Node.TEXT_NODE ||
                theId ==Node.COMMENT_NODE ||
                theId ==Node.PROCESSING_INSTRUCTION_NODE;
            if (isXPathFollowingNode) 
            {
                theNodeListToBuild.add(aStartNode);   
            } // end of if ()
            aStartNode = this.getNextFollowing(aStartNode,true);
        }
        while(aStartNode!=null);
        return;
    }

        /**
         * Adds all nodes including the start node and its descendants to the list.
         * So the list is the list of nodes in reverse document order that are the
         * precedings of the node <emph>following</emph> aStartNode.
         *
         */
    void addSelfAndPrecedings(ArrayList theNodeListToBuild, Node aStartNode, ArrayList theAncestors)
    {
        if(aStartNode == null)
            return;
        do
        {
            int theId = aStartNode.getNodeType();
            boolean isXPathPrecedingNode = theId == Node.ELEMENT_NODE ||
                theId ==Node.DOCUMENT_NODE  || 
                theId ==Node.TEXT_NODE ||
                theId ==Node.COMMENT_NODE ||
                theId ==Node.PROCESSING_INSTRUCTION_NODE;
            if (isXPathPrecedingNode) 
            {
                theNodeListToBuild.add(aStartNode);   
            } // end of if ()
            aStartNode = this.getNextPreceding(aStartNode,theAncestors);
        }
        while(aStartNode!=null);
        return;
    }

        /**
         * The next node excluding attributes and namespaces following me
         * (aStartNode) in document order.
         */
    Node getNextFollowing(Node aStartNode, boolean recurse)
    {
        if(aStartNode == null)
            return null;

        if(recurse)
        {
            Node aChild = aStartNode.getFirstChild();
            if(aChild != null)
                return aChild;
        }

        Node aSibilling = aStartNode.getNextSibling();

        if(aSibilling != null)
            return aSibilling;
        Node aParent = aStartNode.getParentNode();
        Node theParentsNextSibling = null;
        do
        {
            if(aParent != null && aParent.getNodeType() != Node.DOCUMENT_NODE)
                theParentsNextSibling = aParent.getNextSibling();
            if( theParentsNextSibling==null &&
                (aParent != null && aParent.getNodeType() != Node.DOCUMENT_NODE))
                aParent = aParent.getParentNode();
        }
        while(theParentsNextSibling==null &&
              (aParent!=null && aParent.getNodeType() != Node.DOCUMENT_NODE));

        return theParentsNextSibling;
    }

        /**
         * The previous node excluding attributes and namespaces (and ancestors) before me
         * (aStartNode) in reverse document order.
         */
    Node getNextPreceding(Node aStartNode, ArrayList theAncestors)
    {
            // any thing before me
        Node aSibilling = aStartNode.getPreviousSibling();

            //If we got a previsous sibilling get the last child recursive
        if(aSibilling != null)
        {
            Node theLastChild = getLastChildRecursive(aSibilling);

            return (theLastChild != null ? theLastChild : aSibilling);
        }

            // get only an uncle before our parent
        Node aParent = aStartNode.getParentNode();
        if (!theAncestors.contains(aParent)) 
        {
            return aParent;
        } // end of if ()
    

        if(aParent != null && aParent.getNodeType() != Node.DOCUMENT_NODE)
            return getNextPreceding(aParent, theAncestors);

        return null;
    }

        /**
         * Returns the last child in document order that has aParent as an ancestor.
         * If aParent has no childs it returns aParent.
         */
    Node getLastChildRecursive(Node aParent)
    {
        if(aParent==null)
            return null;
        Node aChild = aParent.getLastChild();
        if(aChild!=null)
        {
            return getLastChildRecursive(aChild);
        }
        return aParent;
    }
}
