/* Generated By:JJTree: Do not edit this line. ASTNameTest.java */
package com.sap.isa.catalog.parsers.xpathinterpreter;
public class ASTNameTest extends SimpleNode
{
	boolean isAnyTest = false;
	boolean isAnyNameSpaceTest = false;
	String theNameTest;

    public ASTNameTest(int id)
    {
        super(id);
    }

    public ASTNameTest(XPathParser p, int id)
	{
        super(p, id);
    }

	public String getNameTest()
	{
		return theNameTest;
	}

	public void setNameTest(String theNameTest)
	{
		this.theNameTest=theNameTest;
	}


	public boolean isAnyTest()
	{
		return isAnyTest;
	}

	public void setAnyTest(boolean aTest)
	{
		this.isAnyTest=aTest;
	}

	public boolean isAnyNameSpaceTest()
	{
		return isAnyTest;
	}

	public void setAnyNameSpaceTest(boolean aTest)
	{
		this.isAnyNameSpaceTest=aTest;
	}

	/** Accept the visitor. **/
    public Object jjtAccept(XPathParserVisitor visitor, Object data)
	{
        return visitor.visit(this, data);
    }
}
