package com.sap.isa.catalog.parsers.xpathinterpreter;

import java.util.ArrayList;

/**
 * Title:        WebXSLT
 * Description:
 * Copyright:    Copyright (c) 2001
 * Company:
 * @version 1.0
 */

public class XPnot extends XPathFunction
{
  XPboolean theBooleanFunction;
  public XPnot(XPathContext aContext)
  {
    super(aContext);
    theBooleanFunction = new XPboolean(aContext);
  }

  public IXPvalue call(ArrayList theArguments)
  {
    IXPbooleanValue theResult = null;

    if(theArguments.size()!=1)
      throw new RuntimeException("not() called with wrong number of arguments!");

    IXPvalue aValue = (IXPvalue) theArguments.get(0);

    if(aValue.getTypeId()!= IXPbooleanValue.ID)
      {
	IXPbooleanValue aBooleanValue = theBooleanFunction.call(aValue);
	theResult = call(aBooleanValue);
      }
    else
      {
	theResult = call((IXPbooleanValue)aValue);
      }
    return theResult;
  }
  
  public IXPbooleanValue call(IXPbooleanValue aValue)
  {
    boolean theBooleanValue = aValue.getValue().booleanValue();
    return new XPbooleanValue( new Boolean(!theBooleanValue));
  }
}
