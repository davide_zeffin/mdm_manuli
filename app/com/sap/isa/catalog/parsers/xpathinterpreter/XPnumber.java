package com.sap.isa.catalog.parsers.xpathinterpreter;

import java.util.ArrayList;

import com.sap.isa.core.logging.IsaLocation;

/**
 * Title:        WebXSLT
 * Description:
 * Copyright:    Copyright (c) 2001
 * Company:
 * @version 1.0
 */

public class XPnumber extends XPathFunction
{
	
	/** 
	  *  The IsaLocation of the current Action.
	  *  This will be instantiated during perform and is always present.
	  */
	 protected static IsaLocation log = IsaLocation.getInstance(XPnumber.class.getName());
	
  XPstring theStringFunction;

  public XPnumber(XPathContext aContext)
  {
    super(aContext);
    theStringFunction = new XPstring(aContext);
  }

  public IXPvalue call(ArrayList theArguments)
  {
    if(theArguments.size()>1)
      throw new RuntimeException("Wrong number of arguments for number().");

    IXPnumberValue theResult = null;

    //convert the current node to a number
    if(theArguments.size()==0)
      {
	theResult = call();
      }
    else
      {
	IXPvalue anArgument = (IXPvalue)theArguments.get(0);
	theResult = call(anArgument);
      }
    return theResult;
  }

  public IXPnumberValue call()
  {
    IXPstringValue theContextNodeAsString = theStringFunction.call();

    Number theStrResult = stringToNumber(theContextNodeAsString.getValue());

    return  new XPnumberValue(theStrResult);
  }

  public IXPnumberValue call(IXPvalue anArgument)
  {
     Number theResult = new Double(0.0);

    int typeId = anArgument.getTypeId();

    switch(typeId){

    case IXPbooleanValue.ID:{
      IXPbooleanValue theValue = (IXPbooleanValue)anArgument;
      Boolean theRealValue = theValue.getValue();
      theResult = (   theRealValue.booleanValue() ?
		      new Double(1.0) :
		      new Double(0.0));}
      break;

    case IXPnodeSetValue.ID:{
      IXPstringValue theNodeSetAsString =
	theStringFunction.call(anArgument);
      theResult = stringToNumber(theNodeSetAsString.getValue());}
      break;

    case IXPnumberValue.ID:{
      return (IXPnumberValue)anArgument;
    }

    case IXPstringValue.ID:{
      IXPstringValue theValue = (IXPstringValue)anArgument;
      theResult = stringToNumber(theValue.getValue());}
      break;

    case IXPtreeFragmentValue.ID:{
      IXPstringValue theTreeFragmentAsString =
	theStringFunction.call(anArgument);
      theResult = stringToNumber(theTreeFragmentAsString.getValue());}
      break;
    }

    return new XPnumberValue(theResult);
  }

  public static Number stringToNumber(String aString)
  {
    try
      {
	int theResult = Integer.parseInt(aString.trim());
	return new Integer(theResult);
      }
    catch(NumberFormatException ne)
      {
      	log.debug(ne.getMessage());
      }
    try
      {
	double theResult = Double.parseDouble(aString);
	return new Double(theResult);
      }
    catch(NumberFormatException ne)
      {
		log.debug(ne.getMessage());
      }

    return new Double(Double.NaN);
  }
}
