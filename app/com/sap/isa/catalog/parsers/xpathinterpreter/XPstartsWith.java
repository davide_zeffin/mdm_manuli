package com.sap.isa.catalog.parsers.xpathinterpreter;

import java.util.ArrayList;

/**
 * Title:        WebXSLT
 * Description:
 * Copyright:    Copyright (c) 2001
 * Company:
 * @version 1.0
 */

public class XPstartsWith extends XPathFunction
{
  public XPstartsWith(XPathContext aContext)
  {
    super(aContext);
  }

  public IXPvalue call(ArrayList theArguments)
  {
    if(theArguments.size()!=2)
      throw new RuntimeException("starts-with called with wrong number of arguments!");
    IXPvalue aValue = (IXPvalue) theArguments.get(0);
    IXPvalue aSubValue = (IXPvalue) theArguments.get(1);
    XPstring aStringFunction = new XPstring(theContext);

    IXPstringValue aStringValue = aStringFunction.call(aValue);
    IXPstringValue aSubString = aStringFunction.call(aSubValue);
    return startsWith(aStringValue, aSubString);
  }

  public IXPbooleanValue startsWith(IXPstringValue aValue, IXPstringValue aSubString)
  {
    String theValue = aValue.getValue();
    String theSubValue = aSubString.getValue();
    if(theSubValue.equals(""))
      return new XPbooleanValue(new Boolean(true));
    if(theValue.equals(""))
      return new XPbooleanValue(new Boolean(false));
    int theIndex = theValue.indexOf(theSubValue);
    if(theIndex==0)
      return new XPbooleanValue(new Boolean(true));
    else
      return new XPbooleanValue(new Boolean(false));      
  }
}
