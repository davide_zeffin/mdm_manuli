package com.sap.isa.catalog.parsers.xpathinterpreter;

import java.util.ArrayList;

/**
 * Title:        WebXSLT
 * Description:
 * Copyright:    Copyright (c) 2001
 * Company:
 * @version 1.0
 */

public class XPtranslate extends XPathFunction
{
  private XPstring theStringFunction;
  public XPtranslate(XPathContext aContext)
  {
    super(aContext);
    theStringFunction = new XPstring(aContext);
  }

  public IXPvalue call(ArrayList theArguments)
  {
    if(theArguments.size()!=3)
      throw new RuntimeException("translate() called with wrong number of arguments!");
    IXPstringValue aValue = theStringFunction.call((IXPvalue)theArguments.get(0));
    IXPstringValue aFromValue = theStringFunction.call((IXPvalue)theArguments.get(1));
    IXPstringValue aToValue = theStringFunction.call((IXPvalue)theArguments.get(2));

    return call(aValue,aFromValue,aToValue);
  }
  
  public IXPstringValue call(IXPstringValue aValue, IXPstringValue aFromValue, IXPstringValue aToValue)
  {
    String aString = theStringFunction.expandCharacterReferences(aValue.getValue());
    String aFromString = theStringFunction.expandCharacterReferences(aFromValue.getValue());
    String aToString = theStringFunction.expandCharacterReferences(aToValue.getValue());
    char[] aArray = aString.toCharArray();
    char[] aFromArray = aFromString.toCharArray();
    char[] aToArray = aToString.toCharArray();
    int size = aArray.length;
    int fromSize = aFromArray.length;
    int toSize = aToArray.length;
    StringBuffer aBuffer = new StringBuffer("");
    for (int i = 0; i < size; i++) 
      {
	char aChar = aArray[i];
	boolean replaced=false;
	for (int j = 0; !replaced && j < fromSize; j++) 
	  {
	    char aFromChar = aFromArray[j];
	    if (aChar==aFromChar) 
	      {
		replaced=true;
		if (j<toSize) 
		  {
		    char aToChar = aToArray[j];
		    aBuffer.append(aToChar);
		  } // end of if ()	    
	      } // end of if ()
	  } // end of for ()
	if (!replaced) 
	  {
	    aBuffer.append(aChar);
	  } // end of if ()
      } // end of for ()	
       return new XPstringValue(aBuffer.toString());
      }
  }
