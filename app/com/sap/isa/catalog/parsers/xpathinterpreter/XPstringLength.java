package com.sap.isa.catalog.parsers.xpathinterpreter;

import java.util.ArrayList;

import org.w3c.dom.Node;

/**
 * Title:        WebXSLT
 * Description:
 * Copyright:    Copyright (c) 2001
 * Company:
 * @version 1.0
 */

public class XPstringLength extends XPathFunction
{
  private XPstring theStringFunction;

  public XPstringLength(XPathContext aContext)
  {
    super(aContext);
    theStringFunction = new XPstring(aContext);
  }

  public IXPvalue call(ArrayList theArguments)
  {
    IXPvalue theResult = null;
    if(theArguments.size()>1)
      throw new RuntimeException("string-lenght() called with wrong number of arguments!");
    if(theArguments.size()==0)
      theResult = call();
    else
      {
	IXPvalue aValue = (IXPvalue)theArguments.get(0);
	theResult =call(aValue);
      }
    return theResult;
  }

  public IXPnumberValue call()
  {
    Node theCurrent = theContext.getCurrentNode();
    String aNodeAsString = theStringFunction.nodeToString(theCurrent);
    String aNodeAsStringExpanded = theStringFunction.expandCharacterReferences(aNodeAsString);
    return new XPnumberValue( new Integer(aNodeAsStringExpanded.length()));
  }

  public IXPnumberValue call(IXPvalue aValue)
  {
    IXPstringValue aStringValue = theStringFunction.call(aValue);
    String aStringExpanded = theStringFunction.expandCharacterReferences(aStringValue.getValue());
    return new XPnumberValue( new Integer(aStringExpanded.length()));
  }
}
