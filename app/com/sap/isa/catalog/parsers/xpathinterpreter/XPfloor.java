package com.sap.isa.catalog.parsers.xpathinterpreter;

import java.util.ArrayList;

/**
 * Title:        WebXSLT
 * Description:
 * Copyright:    Copyright (c) 2001
 * Company:
 * @version 1.0
 */

public class XPfloor extends XPathFunction
{
	private XPnumber theNumberFunction;

    public XPfloor(XPathContext aContext)
	{
		super(aContext);
		theNumberFunction = new XPnumber(aContext);
    }

    public IXPvalue call(ArrayList theArguments)
	{
		if(theArguments.size()!=1)
			throw new RuntimeException("Wrong number of arguments for floor().");

		IXPvalue anArgument = (IXPvalue)theArguments.get(0);
		IXPnumberValue aNumberValue = null;
		if(anArgument.getTypeId()!=IXPnumberValue.ID)
		    aNumberValue = theNumberFunction.call(anArgument);
		else
		    aNumberValue = (IXPnumberValue)anArgument;

		return this.call(aNumberValue);
    }

	public IXPnumberValue call(IXPnumberValue aNumberValue)
	{
		Number aNumber = aNumberValue.getValue();
		if(aNumber instanceof Integer)
			return new XPnumberValue(aNumber);
		else
		    return new XPnumberValue(
				new Integer((int)Math.floor(aNumber.doubleValue())));
	}
}
