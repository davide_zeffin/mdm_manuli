package com.sap.isa.catalog.parsers.xpathinterpreter;

import java.util.ArrayList;

/**
 * Title:        WebXSLT
 * Description:
 * Copyright:    Copyright (c) 2001
 * Company:
 * @version 1.0
 */

public class XPsubstringBefore extends XPathFunction
{
  private XPstring theStringFunction;

  public XPsubstringBefore(XPathContext aContext)
  {
    super(aContext);
    theStringFunction = new XPstring(aContext);
  }

  public IXPvalue call(ArrayList theArguments)
  {
    IXPvalue theResult = null;
    if(theArguments.size()!=2)
      throw new RuntimeException("substring-before was called with the wrong number of arguments");
    IXPvalue aStringValue = (IXPvalue)theArguments.get(0);
    IXPvalue anOtherStringValue = (IXPvalue)theArguments.get(1);
    theResult = call(aStringValue,anOtherStringValue);
    return theResult;
  }
  
  public IXPstringValue call(IXPvalue aStringValue, IXPvalue anOtherValue)
  {
    IXPstringValue theResult =null;
    IXPstringValue theContainingStringValue = theStringFunction.call(aStringValue);
    IXPstringValue theStringValue = theStringFunction.call(anOtherValue);
    String theContaingString = theContainingStringValue.getValue();
    String theString = theStringValue.getValue();
    int theIndex = theContaingString.indexOf(theString);
    if(theIndex==-1)
      theResult = new XPstringValue("");
    else
      theResult = new XPstringValue(theContaingString.substring(0, theIndex));
    return theResult;
  }
}
