package com.sap.isa.catalog.parsers.xpathinterpreter;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.StringTokenizer;

import org.w3c.dom.Document;
import org.w3c.dom.Node;

/**
 * Title:        WebXSLT
 * Description:
 * Copyright:    Copyright (c) 2001
 * Company:
 * @version 1.0
 */

public class XPid extends XPathFunction
{
    private XPstring theStringFunction;

    public XPid(XPathContext aContext)
    {
        super(aContext);
        theStringFunction = new XPstring(aContext);
    }

    public IXPvalue call(ArrayList theArguments)
    {
        if(theArguments.size()!=1)
            throw new RuntimeException("Wrong number of arguments for id()");
        IXPvalue theArgument = (IXPvalue)theArguments.get(0);
        IXPnodeSetValue theResult = new XPnodeSetValue(new ArrayList());
        int theTypeId = theArgument.getTypeId();
        switch (theTypeId)
        {
            case IXPnodeSetValue.ID :
                IXPnodeSetValue theNodeSetValue = (IXPnodeSetValue)theArgument;
                ArrayList theNodes = theNodeSetValue.getValue();
                Iterator theNodesIterator = theNodes.iterator();
                while(theNodesIterator.hasNext())
                {
                    Node aNode = (Node)theNodesIterator.next();
                    String aNodeAsString = XPstring.nodeToString(aNode);
                    IXPnodeSetValue aResult = call(new XPstringValue(aNodeAsString));
                    theResult.union(aResult);
                }
                break;
            case IXPbooleanValue.ID :
            case IXPstringValue.ID :
            case IXPnumberValue.ID :
            case IXPtreeFragmentValue.ID :
            default:
                IXPstringValue theConvertedValue =
                    theStringFunction.call(theArgument);
                theResult = call(theConvertedValue);
                break;
        }
        return theResult;
    }

    public IXPnodeSetValue call(IXPstringValue theIDREFS)
    {
        String theIDREFSStr = theIDREFS.getValue();
        StringTokenizer aStringTokenizer =
            new StringTokenizer(theIDREFSStr," ");
        ArrayList theSelectedNodes = new ArrayList();
        Node theCurrent = theContext.getCurrentNode();
        Document theOwner = null;
        if(theCurrent.getNodeType()==Node.DOCUMENT_NODE)
            theOwner = (Document)theCurrent;
        else
            theOwner = theCurrent.getOwnerDocument();
        while(aStringTokenizer.hasMoreTokens())
        {
            String aId = aStringTokenizer.nextToken();
            Node aNode = theOwner.getElementById(aId);
            if(aNode!=null)
                theSelectedNodes.add(aNode);
        }
        return new XPnodeSetValue(theSelectedNodes);
    }
}
