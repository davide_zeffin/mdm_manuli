package com.sap.isa.catalog.parsers.xpathinterpreter;

/**
 * Title:        WebXSLT
 * Description:
 * Copyright:    Copyright (c) 2001
 * Company:
 * @version 1.0
 */

import org.w3c.dom.DocumentFragment;

public interface IXPtreeFragmentValue extends IXPvalue
{
	final public static int ID = 4;

	DocumentFragment getValue();
}
