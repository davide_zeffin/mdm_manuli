/* Generated By:JJTree: Do not edit this line. ASTStep.java */
package com.sap.isa.catalog.parsers.xpathinterpreter;
public class ASTStep extends SimpleNode
{
	public ASTStep(int id)
	{
        super(id);
    }

    public ASTStep(XPathParser p, int id)
	{
        super(p, id);
    }

    /** Accept the visitor. **/
    public Object jjtAccept(XPathParserVisitor visitor, Object data)
	{
        return visitor.visit(this, data);
    }
}
