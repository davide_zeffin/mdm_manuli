package com.sap.isa.catalog.parsers.xpathinterpreter;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.StringTokenizer;

import org.w3c.dom.Node;

/**
 * Title:        WebXSLT
 * Description:
 * Copyright:    Copyright (c) 2001
 * Company:
 * @version 1.0
 */

public class XPnormalizeSpace extends XPathFunction
{
  public static final String TAB_HEX="&#x9;";
  public static final String TAB_DEC="&#9;";
  public static final String NEW_LINE_HEX="&#xA;";
  public static final String NEW_LINE_DEC="&#10;";
  public static final String CARRIAGE_RETURN_HEX="&#xD;";
  public static final String CARRIAGE_RETURN_DEC="&#13;";
  public static final String SPACE_HEX="&#x20;";
  public static final String SPACE_DEC="&#32;";
  
  public static final String[] CHARACTER_REFERENCES =
  {
    TAB_HEX,
    TAB_DEC,
    NEW_LINE_HEX,
    NEW_LINE_DEC,
    CARRIAGE_RETURN_HEX,
    CARRIAGE_RETURN_DEC,
    SPACE_HEX,
    SPACE_DEC
  };
  
  private XPstring theStringFunction;

  public XPnormalizeSpace(XPathContext aContext)
  {
    super(aContext);
    theStringFunction = new XPstring(aContext);
  }

  public IXPvalue call(ArrayList theArguments)
  {
    IXPvalue theResult=null;
    if(theArguments.size()>1)
      throw new RuntimeException("normalize-space() called with the wrong number of arguments!");
    if(theArguments.size()==0)
      theResult = call();
    else
      {
	IXPvalue theValue = (IXPvalue)theArguments.get(0);
	IXPstringValue theString = theStringFunction.call(theValue);
	theResult = call(theString);
      }
    return theResult;
  }
  
  public IXPstringValue call()
  {
    Node theCurrent = this.theContext.getCurrentNode();
    String theString = this.theStringFunction.nodeToString(theCurrent);
    return new XPstringValue(call(theString));
  }

  public IXPstringValue call(IXPstringValue aStringValue)
  {
    return new XPstringValue(call(aStringValue.getValue()));
  }

  private String call(String theString)
  {
    String theTrimString = theString.trim();
    //eliminate character references &#x20; &#x9; &#xD; &#xA; | &#32; &#9; &#13; &#10;
    String theTrimAndReferencResolved = eliminateReferences(theTrimString, CHARACTER_REFERENCES);
    
    StringTokenizer aTokenizer = new StringTokenizer(theTrimAndReferencResolved," \t\n\r\f");
    StringBuffer aNormalizedString = new StringBuffer();
    while(aTokenizer.hasMoreTokens())
      {
      aNormalizedString.append(aTokenizer.nextToken());
      if(aTokenizer.hasMoreTokens())
	aNormalizedString.append(" ");
      }
    return aNormalizedString.toString();
  }  

  static public String eliminateReferences(String aString, String[] aListOfReferences)
  {
    char[] theStringAsArray = aString.toCharArray();

    for(int j = 0; j < aListOfReferences.length; j++)
      {
	String aReference = aListOfReferences[j];
	//eleminate space references
	ArrayList theIndeces = new ArrayList();
	int currentIndex = -1;
	while((currentIndex=aString.indexOf(aReference,currentIndex))!=-1)
	  {
	    theIndeces.add(new Integer(currentIndex));
	    currentIndex+=aReference.length();
	  }

	Iterator aIter = theIndeces.iterator();
	while(aIter.hasNext())
	  {
	    int aIndex = ((Integer)aIter.next()).intValue();
	    for(int i = 0; i < aReference.length(); i++)
	      theStringAsArray[aIndex+i]=' ';
	  }
      }
    return new String(theStringAsArray);
  }
}
