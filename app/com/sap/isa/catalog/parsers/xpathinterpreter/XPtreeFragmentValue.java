package com.sap.isa.catalog.parsers.xpathinterpreter;

/**
 * Title:        WebXSLT
 * Description:
 * Copyright:    Copyright (c) 2001
 * Company:
 * @version 1.0
 */

import org.w3c.dom.DocumentFragment;

public class XPtreeFragmentValue implements IXPtreeFragmentValue
{
	private DocumentFragment theFragment;

	public XPtreeFragmentValue(DocumentFragment theFragment)
	{
		this.theFragment=theFragment;
	}

	public int getTypeId()
	{
		return IXPtreeFragmentValue.ID;
	}

	public DocumentFragment getValue()
	{
		return theFragment;
	}

	public String toString()
	{
		return theFragment.toString();
	}
}

