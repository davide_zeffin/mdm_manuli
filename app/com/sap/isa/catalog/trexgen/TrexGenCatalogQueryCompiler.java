/*****************************************************************************
    Class:        TrexGenCatalogQueryCompiler
    Copyright (c) 2006, SAP AG, All rights reserved.
    Author:
    Created:      28.08.2006
    Version:      1.0

    $Revision$
    $Date$
*****************************************************************************/
package com.sap.isa.catalog.trexgen;

// catalog imports


// misc imports
// trex imports
import java.util.List;
import java.util.Properties;

import org.apache.struts.util.MessageResources;

import com.sap.isa.catalog.CatalogParameters;
import com.sap.isa.catalog.boi.IFilter;
import com.sap.isa.catalog.filter.CatalogFilter;
import com.sap.isa.catalog.filter.CatalogFilterFactory;
import com.sap.isa.catalog.filter.CatalogFilterInvalidException;
import com.sap.isa.catalog.impl.CatalogQuery;
import com.sap.isa.catalog.impl.CatalogQueryStatement;
import com.sap.isa.core.logging.IsaLocation;
import com.sapportals.trex.TrexException;
import com.sapportals.trex.core.SearchManager;

/**
 * Helper class for the {@link TrexGenCatalogBuilder}. Instances of this class
 * are stateless. With the <code>transform...Query</code> methods a given query
 * instance is evaluated and the necessary calls are executed against the passed
 * SearchManager instance. Internally a {@link TrexGenCatalogFilterVisitor} is used to
 * evaluate the filter expression.
 *
 * @version     1.0
 */
public class TrexGenCatalogQueryCompiler
{
    private static IsaLocation log = IsaLocation.getInstance(TrexGenCatalogQueryCompiler.class.getName());

    private TrexGenCatalogFilterVisitor theFilterVisitor;
//    private ITrexGenQuickSearchStrategy theQuickSearchStreategy;

    private MessageResources theMessageResources;

        /**
         * Creates a new instance of the query compiler. Each instance should only be
         * used of one specific Trex builder instance.
         *
         * @param messResources  the resources for the error messages
         * @param aStrategy      the quick search strategy
         */
//    protected TrexGenCatalogQueryCompiler(MessageResources messResources, ITrexGenQuickSearchStrategy aStrategy)
//    {
//        theMessageResources = messResources;
//        theQuickSearchStreategy = aStrategy;
//        theFilterVisitor = new TrexGenCatalogFilterVisitor(messResources);
//    }

    /**
     * Creates a new instance of the query compiler. Each instance should only be
     * used of one specific Trex builder instance.
     *
     * @param messResources  the resources for the error messages
     */
    protected TrexGenCatalogQueryCompiler(MessageResources messResources) {
        theMessageResources = messResources;
        theFilterVisitor = new TrexGenCatalogFilterVisitor(messResources);
    }

    /**
     * Transforms the given query for products into the Trex specific represenation.
     *
     * @param query    the query which has to be transformed
     * @param defAttr  default list of attributes
     * @param idxAttr  all attributes of the index
     * @param catalog  the catalog which should be built
     * @param searchManager    the search manager instance for which the query was built
     * @exception CatalogFilterInvalidException error in the filter expression
     * @exception TrexException exception from the search engine
     */
     public synchronized void transformItemQuery(CatalogQuery query,
                                         List defAttr,
                                         List idxAttr,
                                         TrexGenCatalog catalog,
                                         String searchEngine,
                                         SearchManager searchManager)
        throws TrexGenCatalogException  {
            
        try {    
            CatalogQueryStatement statement = query.getCatalogQueryStatement();
            CatalogFilter filter = null;
            String[] attributes  = statement.getAttributes();
            String orderBy       = null;
            int rangeFrom        = statement.getRangeFrom();
            int rangeTo          = statement.getRangeTo();
            Properties serverEngineProps = new Properties();
            boolean fuzzySearch = true;
            serverEngineProps = catalog.getServerEngine().getProperties();
            String enableFuzzySearch = serverEngineProps.getProperty(ITrexGenCatalogConstants.ENABLE_FUZZY_SEARCH);
            if(enableFuzzySearch!=null && enableFuzzySearch.equalsIgnoreCase("false")){
                fuzzySearch = false;
            }
    
            // a default quick search is requested
            if (statement.getStatementAsString() != null)
            {
                String qsearch = statement.getStatementAsString();
                CatalogFilterFactory factory = CatalogFilterFactory.getInstance();

                // Search in standard catalog and/or reward catalog
                boolean hasPtsItems = defAttr.contains("IS_PTS_ITEM");
                boolean hasBuyPtsItems = defAttr.contains("IS_BUY_PTS_ITEM");
                CatalogFilter filter2 = null;
                
                // always exclude products contained in loyalty reward area and buy points areas if existing
                if (hasBuyPtsItems) {
                    filter = (CatalogFilter) factory.createAttrEqualValue("IS_BUY_PTS_ITEM", "X");
                    filter = (CatalogFilter) factory.createNot(filter);
                }
                if (hasPtsItems) {
                    filter2 = (CatalogFilter) factory.createAttrEqualValue("IS_PTS_ITEM", "X");
                    filter2 = (CatalogFilter) factory.createNot(filter2);
                    if (filter == null) {
                        filter = filter2;
                    }
                    else {
                        filter = (CatalogFilter) factory.createAnd(filter, filter2);
                    }
                }
                
                // get the list of QuickSearch attributes or use the default ones
                String[] quickSearchAttributes = catalog.getQuickSearchAttributes();
                if(quickSearchAttributes == null || quickSearchAttributes.length == 0)
                    quickSearchAttributes = new String[] {
                        ITrexGenCatalogConstants.ATTR_OBJECT_DESC,
                        ITrexGenCatalogConstants.ATTR_OBJECT_ID,
                        ITrexGenCatalogConstants.ATTR_TEXT_0001 };
        
                // set query condition (default are the three selected attributes)
                if (qsearch.equals(ITrexGenCatalogConstants.VALUE_ALL)) {
                    //JP: create some dummy condition that fits ALL documents,
                    // but don't use "*", because this would break when using VIEW
                    filter2 = (CatalogFilter)factory.createAttrEqualValue(
                        ITrexGenCatalogConstants.ATTR_CATALOG, catalog.getName().trim());
                }
                else {
                    qsearch = qsearch.trim();
//            if (fuzzySearch && qsearch.indexOf(ITrexGenCatalogConstants.VALUE_ALL) > -1) {
//                // * in fuzzy searches might lead to unwanted results when directly attached to a string e.g "for*" because than
//                // TREX switches to exact search mode and thus might find nothing or less than with "for *" (because * is normally
//                // ignored by the fuzzy search)
//                log.debug("Before replacing * in search string: " + qsearch);
//                qsearch = qsearch.replaceAll("(^\\**)|(\\**$)", ""); // delete * at the beginning and at the end 
//                qsearch = qsearch.replaceAll("\\*", " "); // replace other * by space
//                log.debug("After replacing leading and trailing * in search string: " + qsearch);
//            }
                
                    if (fuzzySearch) { 
                        if (qsearch.indexOf(ITrexGenCatalogConstants.VALUE_ALL) > -1) {
                            filter2 = (CatalogFilter)factory.createAttrContainValue(quickSearchAttributes[0], qsearch);   
                        }
                        else {
                            filter2 = (CatalogFilter)factory.createAttrFuzzyValue(quickSearchAttributes[0], qsearch);
                        }
                    }
                    else {
                        filter2 = (CatalogFilter)factory.createAttrContainValue(quickSearchAttributes[0], qsearch);                  
                    }
                    for(int i = 1; i < quickSearchAttributes.length; i++)
                    {
                        IFilter a2ndFilter = null;
                        if(fuzzySearch){
                            a2ndFilter =
                                factory.createAttrFuzzyValue(quickSearchAttributes[i], qsearch);
                        }
                        else{
                            a2ndFilter =
                                factory.createAttrContainValue(quickSearchAttributes[i], qsearch);
        
                        }               
                        filter2 = (CatalogFilter)factory.createOr(filter2, a2ndFilter);
                    }
                }
                
                if (filter == null) {
                    filter = filter2;
                }
                else {
                    filter = (CatalogFilter) factory.createAnd(filter, filter2);
                }
            }
            else {
                filter = statement.getFilter();
            }
    
                // set default attribute list if necessary
            if(attributes == null || attributes.length == 0)
                attributes = (String[])defAttr.toArray(new String[0]);
    
                // set sort order if requested -- Trex supports only one attribute
            String[] sortOrder = statement.getSortOrder();
            if(sortOrder != null && sortOrder.length != 0)
                orderBy = sortOrder[0];
    
                // set default range if necessary
            if(rangeFrom == 0) rangeFrom = ITrexGenCatalogConstants.INTERVAL_BEGIN;
            if(rangeTo == 0)   rangeTo   = CatalogParameters.trexUpperLimit;
            if(rangeFrom >= rangeTo) {
                rangeFrom = ITrexGenCatalogConstants.INTERVAL_BEGIN;
                rangeTo   = CatalogParameters.trexUpperLimit;
            }
    
                // set return properties
            searchManager.setResultFromTo(rangeFrom, rangeTo);
    
                // set return attributes
            for (int i=0; i<attributes.length; i++) {
                if (!attributes[i].equalsIgnoreCase(ITrexGenCatalogConstants.ATTR_AREA_GUID) &&
                    !attributes[i].equalsIgnoreCase(ITrexGenCatalogConstants.ATTR_OBJECT_GUID) &&
                    !attributes[i].equalsIgnoreCase(ITrexGenCatalogConstants.ATTR_OBJECT_DESC) ) {
                    searchManager.addRequestedAttribute(attributes[i]);
                }
            }
    
                // set sort order
            if (orderBy != null) {
                searchManager.addSortAttribute(orderBy, true);
            }
    
                // set query condition
            theFilterVisitor.evaluate(filter, idxAttr, searchManager, searchEngine);
        }
        catch (CatalogFilterInvalidException ex1) {
           throw new TrexGenCatalogException(ex1.getLocalizedMessage());
        }
        catch (TrexException ex2) {
           throw new TrexGenCatalogException(ex2.getLocalizedMessage());
        }
    }

    /**
     * Transforms the given query for categories into the Trex specific represenation.<BR/>
     *
     * @param query    the query which has to be transformed
     * @param defAttr  default list of attributes
     * @param idxAttr  all attributes of the index
     * @param catalog  the catalog which should be built
     * @param info    the info object about the index
     * @param searchManger the searchManager instance for which the query was built
     * @exception CatalogFilterInvalidException error in filter expression
     * @exception TrexException exception from the search engine
     */
    public synchronized void transformCategoryQuery(CatalogQuery query,
                                             List defAttr,
                                             List idxAttr,
                                             TrexGenCatalog catalog,
                                             String searchEngine,
                                             SearchManager searchManager)
        throws TrexGenCatalogException {
        
        try {
            CatalogQueryStatement statement = query.getCatalogQueryStatement();
            CatalogFilter filter = statement.getFilter();
            String[] attributes  = (String[])defAttr.toArray(new String[0]);
            int rangeFrom        = statement.getRangeFrom();
            int rangeTo          = statement.getRangeTo();
    
            // a default quick search is requested
            if (statement.getStatementAsString() != null) {
                filter = (CatalogFilter)CatalogFilterFactory.getInstance().
                    createAttrContainValue(ITrexGenCatalogConstants.ATTR_DESCRIPTION,
                                           statement.getStatementAsString());
            }
    
            // if category specific only the children categories are searched for
            if (query.isCategorySpecificSearch()) {
                IFilter cond = CatalogFilterFactory.getInstance().
                    createAttrEqualValue( ITrexGenCatalogConstants.ATTR_PARENT_AREA_GUID,
                                          query.getParent().getGuid());
    
                filter = (CatalogFilter)CatalogFilterFactory.getInstance().createAnd(filter, cond);
    
            }
    
            // determine the search range
            if(rangeFrom == 0) {
                rangeFrom = ITrexGenCatalogConstants.INTERVAL_BEGIN; 
            }
            if(rangeTo == 0) {
                rangeTo   = CatalogParameters.trexUpperLimit; 
            }
            if(rangeFrom >= rangeTo) {
                rangeFrom = ITrexGenCatalogConstants.INTERVAL_BEGIN;
                rangeTo   = CatalogParameters.trexUpperLimit;
            }
    
            // set return properties
            searchManager.setResultFromTo(rangeFrom, rangeTo);
    
            // set return attributes
            for (int i = 0; i < attributes.length; i++) {
                searchManager.addRequestedAttribute(attributes[i]);
            }
    
            // set sort order
            String[] sortOrder = statement.getSortOrder();
            if (sortOrder != null &&
                sortOrder.length != 0) {
                searchManager.addSortAttribute(sortOrder[0], true);
            }
    
            // set query condition
            theFilterVisitor.evaluate(filter, idxAttr, searchManager, searchEngine);
        } 
        catch(CatalogFilterInvalidException ex1) {
           throw new TrexGenCatalogException(ex1.getLocalizedMessage());
        }
        catch(TrexException ex2) {
           throw new TrexGenCatalogException(ex2.getLocalizedMessage());
        }

    }

    /**
     * Transforms a filtered search statement. This method can be used independent
     * of a catalog instance or the catalog builder. If inconsistent parameters are passed
     * then default values will be taken. Currently this method is used by the
     * Trex adminstration console. <br>
     *
     * @param searchManager the search manager instance for which the query was built
     * @param idxAttr    default list of attributes
     * @param filter     filter of the query
     * @param attributes all attributes of the index
     * @param orderBy    sort order of the query
     * @param rangeFrom  begin of search interval
     * @param rangeTo    end of search interval
     * @param searchEngine name of search engine to be used
     * @exception CatalogFilterInvalidException error in filter expression
     * @exception TrexException exception from the search engine
     */
    public synchronized void transformFilterQuery(SearchManager searchManager,
                                           List idxAttr,
                                           CatalogFilter filter,
                                           String[] attributes,
                                           String orderBy,
                                           int rangeFrom,
                                           int rangeTo,
                                           String searchEngine)
        throws TrexGenCatalogException {
            
        try {
            // set return interval
            if (rangeFrom == 0) {
                rangeFrom = ITrexGenCatalogConstants.INTERVAL_BEGIN;
            }
            if (rangeTo == 0) {
                rangeTo   = CatalogParameters.trexUpperLimit;
            }
            if (rangeFrom >= rangeTo) {
                rangeFrom = ITrexGenCatalogConstants.INTERVAL_BEGIN;
                rangeTo   = CatalogParameters.trexUpperLimit;
            }
            searchManager.setResultFromTo(rangeFrom, rangeTo);
    
            // set return attributes
            if (attributes == null) {
                attributes = (String[])idxAttr.toArray(new String[0]);
            }
    
            for (int i = 0; i < attributes.length; i++ ) {
                searchManager.addRequestedAttribute(attributes[i]);
            }
    
            // set sort order
            if (orderBy != null) {
                searchManager.addSortAttribute(orderBy, true);
            }
    
            // set query condition
            theFilterVisitor.evaluate(filter, idxAttr, searchManager, searchEngine);
            
        } 
        catch(CatalogFilterInvalidException ex1) {
           throw new TrexGenCatalogException(ex1.getLocalizedMessage());
        }
        catch(TrexException ex2) {
           throw new TrexGenCatalogException(ex2.getLocalizedMessage());
        }
    }
    

}
