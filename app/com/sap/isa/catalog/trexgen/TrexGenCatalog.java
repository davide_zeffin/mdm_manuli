/*****************************************************************************
    Class:        TrexGenCatalog
    Copyright (c) 2006, SAP AG, All rights reserved.
    Author:
    Created:      15.09.2006
    Version:      1.0

    $Revision$
    $Date$
*****************************************************************************/
package com.sap.isa.catalog.trexgen;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;

import org.apache.struts.util.MessageResources;
import org.w3c.dom.Document;

import com.sap.isa.catalog.boi.CatalogException;
import com.sap.isa.catalog.boi.IClient;
import com.sap.isa.catalog.boi.IComponent;
import com.sap.isa.catalog.boi.IServerEngine;
import com.sap.isa.catalog.boi.IView;
import com.sap.isa.catalog.impl.Catalog;
import com.sap.isa.catalog.impl.CatalogClient;
import com.sap.isa.catalog.impl.CatalogView;
import com.sap.isa.core.logging.IsaLocation;

public class TrexGenCatalog
    extends Catalog
    implements ITrexGenCatalogConstants
{
    // the logging instance
    private static IsaLocation theStaticLocToLog =
        IsaLocation.getInstance(TrexGenCatalog.class.getName());
        
    // Catalog infos
    private String    theReplTimeStamp = "";
    
    public TrexGenCatalog(TrexGenCatalogServerEngine aTrexServer,
                             IClient aClient,
                             String aCatalogGuid,
                             MessageResources aMessageResource)
    {
        super(aTrexServer,aCatalogGuid);
        theClient = new CatalogClient(aClient);
        theMessageResources = aMessageResource;
        theServerEngine=aTrexServer;
        setDescriptionInternal("TrexGenCatalog(guid=" + aCatalogGuid + ")");
        
    }

    public String getLanguage()
    {
        return theDefaultLocale.getLanguage();
    }
    
    //JP: has to be re-thought if the format of the CatalogGuid will change!!!
    //JP: fails on catalog names containing underscore -> search underscore
    //    in CatalogGuid from left to right (variant name must not contain "_")
    private static String getNameFromGuid(String aCatalogGuid)
    {
            //divide the catalog name from the variant
        int index = aCatalogGuid.lastIndexOf("_");
        return aCatalogGuid.substring(0, index);
    }

    /**
     * Generates a key for the cache from the passed parameters.<br>
     * This method is called from the {@link com.sap.isa.catalog.cache.CatalogCache}
     * and has to be <code>public static</code>.
     *
     * @param aMetaInfo  meta information from the EAI layer
     * @param aClient    the client spec of the catalog
     * @param aServer    the server spec of the catalog
     */
    public static String generateCacheKey(
        String theClassName,
        IClient aClient,
        IServerEngine aServer)
    {
        // create views id
        StringBuffer views = new StringBuffer();
        List viewList = aClient.getViews();
        Iterator iter = viewList.iterator();
        while(iter.hasNext()) {
            IView view = (IView)iter.next();
            views.append(view.getGuid());
        }

        // now build and return the key
        String theKey = aServer.getURLString() +
                        aServer.getPort() +
                        aServer.getCatalogGuid() +
                        views.toString();
        return theKey;
    }

    synchronized public boolean isCachedItemUptodate()
    {
        if ( getState() == IComponent.State.DELETED) 
        {
            return false;
        } // end of if ()
        return theServerEngine.isCatalogUpToDate(this);
    }

    public void commitChangesRemote(Document theChanges)
        throws CatalogException
    {
        throw new UnsupportedOperationException("No yet implemented!");
    }

        /**
         * Returns the views associated with the catalog.
         *
         * @return  array of views
         */
    public String[] getViews()
    {	
        String method = "getViews()";

        ArrayList views = theClient.getViews();
        
        if (theStaticLocToLog.isDebugEnabled())
	        theStaticLocToLog.debug(method + ": views size: " + views.size());
        
        String[] result = new String[views.size()];
        
        for (int i = 0;i<views.size();i++){
    	    	
    	    CatalogView view = (CatalogView) views.get(i);
    	    result[i] = view.getGuid();
    	    
		    if (theStaticLocToLog.isDebugEnabled()) 
		   	    theStaticLocToLog.debug(method + ": added view with id: "+ view.getGuid());
        }	
        return result;
    }

    /**
     * Returns the replication timestamp as String
     * 
     * @return replication timestamp as String
     */
    public String getReplTimeStamp()
    {
        return theReplTimeStamp;
    }
    
	/**
	 * Returns the status of the (backend) Catalog.
	 *
	 * @return  status of the Catalog (either OK or ERROR)
	 */
	public int getCatalogStatus()
	{
        String method = "getCatalogStatus()";
		theStaticLocToLog.entering(method);
        
        int catalogStatus = CATALOG_STATUS_OK;
        
		return catalogStatus;
	}

    /**
     * Sets the replication time stamp
     * 
     * @param theNewReplTimeStamp as String
     */
    public void setReplTimeStamp(String theNewReplTimeStamp)
    {
        theReplTimeStamp=theNewReplTimeStamp;
    }

}

