/*****************************************************************************
    Class:        TrexGenCatalogServerEngine
    Copyright (c) 2006, SAP AG, All rights reserved.
    Author:
    Created:      06.10.2006
    Version:      1.0

    $Revision$
    $Date$
*****************************************************************************/
package com.sap.isa.catalog.trexgen;

import java.util.Enumeration;
import java.util.Properties;

import javax.xml.transform.Source;

import org.w3c.dom.Document;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;

import com.sap.isa.catalog.CatalogParameters;
import com.sap.isa.catalog.ICatalogMessagesKeys;
import com.sap.isa.catalog.boi.CatalogException;
import com.sap.isa.catalog.boi.IActor;
import com.sap.isa.catalog.boi.IClient;
import com.sap.isa.catalog.boi.IServer;
import com.sap.isa.catalog.boi.IServerEngine;
import com.sap.isa.catalog.impl.Catalog;
import com.sap.isa.catalog.impl.CatalogServerEngine;
import com.sap.isa.catalog.impl.CatalogSite;
import com.sap.isa.core.eai.BackendBusinessObjectMetaData;
import com.sap.isa.core.eai.BackendBusinessObjectParams;
import com.sap.isa.core.eai.BackendException;
import com.sap.isa.core.eai.Connection;
import com.sap.isa.core.eai.ConnectionDefinition;
import com.sap.isa.core.eai.ConnectionFactory;
import com.sap.isa.core.eai.sp.jco.BackendBusinessObjectSAP;
import com.sap.isa.core.eai.sp.jco.JCoConnection;
import com.sap.isa.core.eai.sp.jco.JCoConnectionEventListener;
import com.sap.isa.core.eai.sp.jco.JCoManagedConnectionFactory;
import com.sap.isa.core.logging.IsaLocation;
import com.sapportals.trex.TrexConst;
import com.sapportals.trex.TrexException;
import com.sapportals.trex.TrexReturn;
import com.sapportals.trex.core.DocAttribute;
import com.sapportals.trex.core.QueryEntry;
import com.sapportals.trex.core.ResultDocument;
import com.sapportals.trex.core.SearchManager;
import com.sapportals.trex.core.SearchResult;
import com.sapportals.trex.core.TrexFactory;

/**
 * TrexGenCatalogServerEngine.java
 * A class representing an index server.
 *
 * Created: Wed Feb 06 08:00:47 2002
 *
 * @version 1.0
 */
public class TrexGenCatalogServerEngine 
    extends
        CatalogServerEngine
    implements
        BackendBusinessObjectSAP,
        ITrexGenCatalogConstants
{
    // the logging instance
    private static IsaLocation log =
        IsaLocation.getInstance(TrexGenCatalogServerEngine.class.getName());

    private transient JCoConnection theSAPConnection;

        /**
         * Creates a new <code>TrexCatalogServerEngine</code> instance.
         * Only used when created via BEM mechanism.
         */
    public TrexGenCatalogServerEngine() {
        theType = IServerEngine.ServerType.TREX;
    }

        /**
         * Creates a new <code>TrexCatalogServerEngine</code> instance.
         *
         * @param theGuid a <code>String</code> value
         * @param theSite a <code>CatalogSite</code> value
         * @param theConfigFile a <code>Document</code> value
         */
    public TrexGenCatalogServerEngine (
        String theGuid,
        CatalogSite theSite,
        Document theConfigFile)
        throws BackendException {
        super(theGuid,theSite,theConfigFile);
        theType = IServerEngine.ServerType.TREX;
    }

        /**
         * Creates a new <code>TrexCatalogServerEngine</code> instance.
         *
         * @param theGuid a <code>String</code> value
         * @param theSite a <code>CatalogSite</code> value
         * @param theConfigFile a <code>Document</code> value
         * @param theEAIProperties a <code>Properties</code> value
         */
    public TrexGenCatalogServerEngine (
        String theGuid,
        CatalogSite theSite,
        Document theConfigFile,
        Properties theEAIProperties)
        throws BackendException {
        super(theGuid,theSite,theConfigFile);
        this.theProperties.putAll(theEAIProperties);
        theType = IServerEngine.ServerType.TREX;
    }

        /**************************************** BackendBusinessObject *********/
        
    public void destroyBackendObject() {
        return;
    }

    public void initBackendObject(Properties theEAIProperties,
                                  BackendBusinessObjectParams aParams)
        throws BackendException {
        theProperties.putAll(theEAIProperties);
        Properties theServerconnectionProperties =
            getBackendObjectSupport().
            getMetaData().
            getDefaultConnectionDefinition().
            getProperties();

            //overlay it with the connection properties from the default connection.
        theProperties.putAll(theServerconnectionProperties);

        CatalogSite.CatalogServerParams theParams =(CatalogSite.CatalogServerParams)aParams;
        theGuid=theParams.getGuid();
        theSite = theParams.getSite();
        theMessageResources = theSite.getMessageResources();
        theActor = theSite.getActor();

            //in case the client is a different than in the default connection create a new connection
        setDefaultConnection(theActor);

        Document aConfigFile = theParams.getDocument();
        this.theConfigDocument=aConfigFile;
        init(aConfigFile);

        if (log.isDebugEnabled())
        {
            StringBuffer aBuffer = new StringBuffer();
            Enumeration theKeys = theProperties.keys();
            while(theKeys.hasMoreElements()) {
                String aKey = (String)theKeys.nextElement();
                aBuffer.append(aKey + ":");
                if(aKey.equals("passwd"))
                    aBuffer.append("?");
                else
                    aBuffer.append(theProperties.getProperty(aKey));
                if (theKeys.hasMoreElements()) {
                    aBuffer.append("; ");
                } 
            }

            String aMessage =
                theMessageResources.getMessage(
                    ICatalogMessagesKeys.PCAT_SRV_PROPS,
                    theGuid,
                    aBuffer.toString());

            log.debug(aMessage);
        } 
        return;
    }
    
    /*************************** BackendBusinessObjectSAP **********************/

    /**
     * This method is used to add JCo-call listener to this back-end object.
     * A JCo listener will be notified before a JCo call is being executed
     * and after the JCo call has been executed.
     *
     * @param listener         a listener for JCo calls
     */
    public void addJCoConnectionEventListener(JCoConnectionEventListener listener) {
        theConnectionFactory.addConnectionEventListener(listener); 
    }

    public JCoConnection getDefaultJCoConnection(Properties props)
        throws BackendException {
        return (JCoConnection) theConnectionFactory.getDefaultConnection(
            getDefaultJCoConnection().getConnectionFactoryName(),
            getDefaultJCoConnection().getConnectionConfigName(),
            props);
    }

    public JCoConnection getDefaultJCoConnection() {
        if(theSAPConnection == null)
            setDefaultConnection(getConnectionFactory().getDefaultConnection());

        return theSAPConnection;
    }

    public JCoConnection getJCoConnection(String conKey) 
        throws BackendException {
        return (JCoConnection)getConnectionFactory().getConnection(conKey);
    }

    public JCoConnection getModDefaultJCoConnection(Properties conDefProps)
        throws BackendException {
        ConnectionFactory factory = getConnectionFactory();
        JCoConnection defConn = (JCoConnection)factory.getDefaultConnection();

        return (JCoConnection)factory.getConnection(
            defConn.getConnectionFactoryName(),
            defConn.getConnectionConfigName(),
            conDefProps);
    }


    /****************************** CachableItem ***************************/
    
    public boolean isCachedItemUptodate() {
        return true;
    }

    /**
     * Generates a key for the cache from the passed parameters.<br>
     * This method is called from the {@link com.sap.isa.catalog.cache.CatalogCache}
     * and has to be <code>public static</code>.
     *
     * @param aMetaInfo  meta information from the EAI layer
     * @param aClient    the client spec of the catalog
     * @param aServer    the server spec of the catalog
     */
    public static String generateCacheKey(
        BackendBusinessObjectMetaData aMetaInfo,
        IActor aClient,
        IServer aServer) {
        StringBuffer aBuffer = new StringBuffer(TrexGenCatalogServerEngine.class.getName());
        
        ConnectionDefinition connDef =
            aMetaInfo.getDefaultConnectionDefinition();

            // create system id
            // special properties for the connection
        String system = null;
        Properties servProps = aServer!=null?aServer.getProperties():null;
        if (servProps != null) {
            system = generateKeySAPSystem(
                servProps.getProperty(JCoManagedConnectionFactory.JCO_CLIENT),
                servProps.getProperty(JCoManagedConnectionFactory.JCO_R3NAME),
                servProps.getProperty(JCoManagedConnectionFactory.JCO_ASHOST),
                servProps.getProperty(JCoManagedConnectionFactory.JCO_SYSNR));
        }

            // default properties
        if (system == null) {
            Properties defProps = connDef.getProperties();
            system = generateKeySAPSystem(
                defProps.getProperty(JCoManagedConnectionFactory.JCO_CLIENT),
                defProps.getProperty(JCoManagedConnectionFactory.JCO_R3NAME),
                defProps.getProperty(JCoManagedConnectionFactory.JCO_ASHOST),
                defProps.getProperty(JCoManagedConnectionFactory.JCO_SYSNR));
        }

        String theServerGUID =
            (aServer!=null?
             aServer.getGuid():
             aMetaInfo.getBackendBusinessObjectConfig().getType());
        
        aBuffer.append(theServerGUID);
        aBuffer.append(system);
        
            // now build and return the key
        return aBuffer.toString();
    }

    /**
     * Generates a key from the passed parameters for a SAP system.
     * If the parameters are incomplete so that no valid key can be
     * built <code>null</code> will be returned.
     *
     * @param client  the client of the r/3 system
     * @param r3name  the name of the r/3 system
     * @param ashost  the application server
     * @param sysnr   the system number
     * @return key for a SAP system
     */
    protected static String generateKeySAPSystem(String client, String r3name,
                                               String ashost, String sysnr) {
        String key = null;

        if (client != null && client.length() != 0) {
            if (r3name != null && r3name.length() != 0)
                key = r3name + client;

            else if (ashost != null && ashost.length() != 0 && sysnr != null && sysnr.length() != 0)
                key = ashost + sysnr + client;
        }
        return key;
    }


    /*******************************************************************************/

    /**
     * Sets the default connection to the system where the infos about the
     * catalog are located.
     *
     * @param conn  the default connection
     */
     private void setDefaultConnection(Connection conn) {
        theSAPConnection = (JCoConnection)conn;
    }

    /**
     * This method sets a new default connection if the necessary connection
     * settings are passed via the client.
     *
     * @param client  reference to a specific client
     */
    synchronized private void setDefaultConnection(IActor client) {
        Properties props = null;

        Properties theDefaultConnectionProperties =
            getBackendObjectSupport().getMetaData().getDefaultConnectionDefinition().getProperties();
        String theClientName = client.getName();
        String theClientPWD = client.getPWD();
        String theDefaultName=
            theDefaultConnectionProperties.getProperty(JCoManagedConnectionFactory.JCO_USER);
        String theDefaultPWD=
            theDefaultConnectionProperties.getProperty(JCoManagedConnectionFactory.JCO_PASSWD);
        
            // check if special client was specified and if it was different from the default connection
        if (theClientName!= null        &&
            theClientPWD!= null         &&
            theClientName.length() != 0 &&
            theClientPWD.length() != 0 &&
            !theClientName.equals(theDefaultName) &&
            !theClientPWD.equals(theDefaultPWD))  {
                // define new client settings
            props = new Properties();
            props.setProperty(  JCoManagedConnectionFactory.JCO_USER,
                                theClientName);
            props.setProperty(  JCoManagedConnectionFactory.JCO_PASSWD,
                                theClientPWD);
        }
            // change the default connection
        if(props != null) {
            try {
                JCoConnection modConn =getModDefaultJCoConnection(props);
                if(modConn.isValid())  {
                    this.theProperties.putAll(props);
                    this.setDefaultConnection(modConn);
                }
                
            }
            catch(Exception ex) {
                log.warn(ICatalogMessagesKeys.PCAT_EXCEPTION,
                                   new Object[] { ex.getLocalizedMessage() }, null);
            }
        }
        return;
    }

    /***  implementations of abstract methods form CatalogServerEngine *****/
    /********************************* from IServer ****************************/

    public int getPort() {
        return 0;
    }

    public String getURLString() {
        return null; 
    }

    /************************* IServerEngine *******************************/
    
    protected void addCatalogRemote(InputSource aSource, Source aMappingSource) 
        throws TrexGenCatalogException {
        return;
    }

    protected void addCatalogRemote(Catalog catalog)
        throws CatalogException {
    }

    /**
     * Build the list of catalog infos known to that server.
     * The nodelist of <code>Catalog</code> elements is used as a filter.
     * If the node list is empty or null no catalog info elements will be build.
     *
     * @param theCatalogElements a <code>NodeList</code> value
     */
     synchronized protected void buildCatalogInfosRemote(NodeList theCatalogElements) {
     }

    /**
     * Build the list of catalog infos (from the crm system) known to that server.
     * Use the supplied string as catalog name pattern.
     *
     * @param aCatalogNamePattern a <code>String</code> value
     */
     synchronized protected void buildCatalogInfosRemote(String aCatalogNamePattern) {
     }

    protected Catalog createCatalogRemote() {
        return null;
    }

    /**
     * Delete a catalog on the server. The catalog is identified by its catalog info.
     *
     * @param aCatalogGuid <code>String</code> value
     */
     protected void deleteCatalogRemote(CatalogServerEngine.CatalogInfo aCatalogInfo)
        throws CatalogException {
     }
    
    /**
     * Delete all catalogs on the index server.
     */
    protected void deleteCatalogsRemote()
        throws CatalogException {
    }

    /**
     * Instantiate a new instance of a TrexCatalog.
     * Only called if the catalog was not cached.
     *
     * @param aCatalogInfo a <code>CatalogServerEngine.CatalogInfo</code> value
     * @param aClient an <code>IClient</code> value
     * @return a <code>Catalog</code> value
     * @exception CatalogException if an error occurs
     */
    protected Catalog getCatalogRemote(
              CatalogServerEngine.CatalogInfo aCatalogInfo,
              IClient aClient)
        throws CatalogException {
        TrexGenCatalog aNewCatalog = null;

        return aNewCatalog;
    }

    /**
     * Reads last replication timestamp from TREX  
     * 
     * @param theAIndexId
     * @return
     * @throws TrexGenCatalogException
     */
    protected String getIndexLastReplicationTimestamp(String theAIndexId)
        throws TrexGenCatalogException
    {
        SearchManager searchManager = TrexFactory.getSearchManager();

        try
        {
            searchManager.addIndexId( theAIndexId.toLowerCase(),TrexConst.SEARCH_ENGINE_DRFUZZY);

                // set interval
            searchManager.setResultFromTo(INTERVAL_BEGIN, CatalogParameters.trexUpperLimit);
                // add return attributes
            searchManager.addRequestedAttribute(ATTR_TIMESTAMP);

                // set query condition
            QueryEntry entry = new QueryEntry();
            entry.setRowType(QueryEntry.ROW_TYPE_ATTRIBUTE);
            entry.setLocation(ATTR_AREA_GUID);
            entry.setContentType(QueryEntry.CONTENT_TYPE_STRING);
            entry.setValue(VALUE_ROOT, "", QueryEntry.ATTRIBUTE_OPERATOR_EQ);
            entry.setTermWeight(10000);
            searchManager.addQueryEntry(entry);

            SearchResult results = searchManager.search();
            if (results.getLastError().getCode()!=TrexReturn.NO_ERROR) {
                throw new TrexGenCatalogException(results.getLastError().getMsg());
            }
            // there is only one result (hopefully)
            ResultDocument firstDocument = results.getFirstResultDocument();
            if (firstDocument == null) {
                String msg =
                    theMessageResources.
                    getMessage(
                        ICatalogMessagesKeys.PCAT_ERR_TREX_AINDEX_CORRUPTED);
                throw new TrexGenCatalogException(msg);
            }
            // there is only one return attribute (hopefully)
            DocAttribute aDocAttribute = firstDocument.getFirstDocumentAttribute();
            if (aDocAttribute==null ||
                ! aDocAttribute.getAttributeName().equalsIgnoreCase(ATTR_TIMESTAMP)) {
                String msg =
                    theMessageResources.
                    getMessage(
                        ICatalogMessagesKeys.PCAT_ERR_TREX_AINDEX_CORRUPTED);
                throw new TrexGenCatalogException(msg);
            }
            return aDocAttribute.getValue1();
        } 
        catch (TrexException te) {
            throw new TrexGenCatalogException(te.getMessage(),te);
        }
    }
    
    
    /**
     * Describe <code>getServerFeatures</code> method here.
     *
     * @return an <code>IServerEngine.ServerFeature[]</code> value
     */
     public IServerEngine.ServerFeature[] getServerFeatures() {
        return new IServerEngine.ServerFeature[]{};
    }


    /**
     * Describe <code>hasServerFeature</code> method here.
     *
     * @param aFeature an <code>IServerEngine.ServerFeature</code> value
     * @return a <code>boolean</code> value
     */
     public boolean hasServerFeature(IServerEngine.ServerFeature aFeature) {
        return false;
     }

    public void init() throws TrexGenCatalogException {
    }

    protected void init(Document theConfigFile) {
    }

    public boolean isCatalogUpToDate(Catalog aCatalog) {
        return false;
    }



}// TrexGenCatalogServerEngine