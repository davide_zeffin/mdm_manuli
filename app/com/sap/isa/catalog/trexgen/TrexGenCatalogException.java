/*****************************************************************************
    Class:        TrexGenCatalogException
    Copyright (c) 2006, SAP AG, All rights reserved.
    Author:
    Created:      05.09.2006
    Version:      1.0

    $Revision$
    $Date$
*****************************************************************************/
package com.sap.isa.catalog.trexgen;

import com.sap.isa.catalog.boi.CatalogException;

/**
 * Exception class for all exceptions which occur during calling or working
 * with the Trex catalog implementation.
 *
 * @version     1.0
 */
public class TrexGenCatalogException extends CatalogException
{
    /**
     * Default constructor for exceptions.
     *
     * @param  msg  text to be associated with the exception
     */
    protected TrexGenCatalogException(String msg)
    {
        super(msg);
    }

    protected TrexGenCatalogException(String mgs, Throwable aCause)
    {
        super(mgs, aCause);
    }
}
