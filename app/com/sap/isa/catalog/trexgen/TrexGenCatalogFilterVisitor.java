/*****************************************************************************
    Class:        TrexGenCatalogFilterVisitor
    Copyright (c) 2006, SAP AG, All rights reserved.
    Author:
    Created:      05.10.2006
    Version:      1.0

    $Revision$
    $Date$
*****************************************************************************/
package com.sap.isa.catalog.trexgen;

// misc imports
import java.util.List;

import org.apache.struts.util.MessageResources;

import com.sap.isa.catalog.filter.CatalogFilter;
import com.sap.isa.catalog.filter.CatalogFilterAnd;
import com.sap.isa.catalog.filter.CatalogFilterAttrBetween;
import com.sap.isa.catalog.filter.CatalogFilterAttrContain;
import com.sap.isa.catalog.filter.CatalogFilterAttrEqual;
import com.sap.isa.catalog.filter.CatalogFilterAttrFuzzy;
import com.sap.isa.catalog.filter.CatalogFilterAttrLinguistic;
import com.sap.isa.catalog.filter.CatalogFilterInvalidException;
import com.sap.isa.catalog.filter.CatalogFilterNot;
import com.sap.isa.catalog.filter.CatalogFilterOr;
import com.sap.isa.catalog.filter.CatalogFilterVisitor;
import com.sapportals.trex.TrexException;
import com.sapportals.trex.core.QueryEntry;
import com.sapportals.trex.core.SearchManager;

/**
 * This class transforms the filter expression tree to the Trex specific
 * representation i.e. the calls against the Trex searchManager are performed.
 *
 * @version     1.0
 */
public class TrexGenCatalogFilterVisitor extends CatalogFilterVisitor {
    private SearchManager theSearchManager;
    private String theSearchEngine;

        // State info: This attribute holds the reference of the parent node of
        // the actual node. This state information is only valid at the entry of
        // the visitor methods! It can be only used in the leaf of the tree,
        // otherwise a stack would be necessary.
    private CatalogFilter theParentFilterNode = null;

        /**
         * Creates a new instance of the Trex specfic visitor of the filter expression
         * tree.
         *
         * @param messResources  the resources for the error messages
         */
    public TrexGenCatalogFilterVisitor(MessageResources messResources) {
        super(messResources);
    }

        /**
         * Starts the transformation of the filter expression tree.
         *
         * @param rootFilter  the root of the expression tree
         * @param attributes  the attributes of the context of the filter expression
         * @param searchManager the searchManager for the Trex server
         * @param searchEngine  name of searchEngine to be used
         * @exception CatalogFilterInvalidException error during evaluation of the expression
         */
    void evaluate(CatalogFilter rootFilter, List attributes, SearchManager searchManager,
                  String searchEngine) throws CatalogFilterInvalidException {
            // initialization of state information
        theSearchManager = searchManager;
        theSearchEngine = searchEngine;
        theParentFilterNode = null;
            // start traversation of expression tree
        this.start(rootFilter, attributes);
            // clear state
        theSearchManager = null;
        theSearchEngine = null;
        theParentFilterNode = null; 
    }

    protected void visitAttrContainFilter(CatalogFilterAttrContain filterNode)
        throws CatalogFilterInvalidException {
            // super call
        super.visitAttrContainFilter(filterNode);
            // transformation
        String name    = filterNode.getName();
        String pattern = filterNode.getPattern();
        byte type = QueryEntry.CONTENT_TYPE_AND; 

        try {
            QueryEntry entry = new QueryEntry();
            entry.setRowType(QueryEntry.ROW_TYPE_ATTRIBUTE);
            entry.setLocation(name);
            entry.setContentType(type);
            entry.setTermWeight(10000);
            if(theParentFilterNode instanceof CatalogFilterNot) {
                entry.setValue(pattern, "", QueryEntry.ATTRIBUTE_OPERATOR_NE);
            }
            else {
                entry.setValue(pattern, "", QueryEntry.ATTRIBUTE_OPERATOR_EQ);
            }
            theSearchManager.addQueryEntry(entry);
        } 
        catch(TrexException ex) {
            throw new CatalogFilterInvalidException(ex.getLocalizedMessage());
        }
    }

    protected void visitAttrEqualFilter(CatalogFilterAttrEqual filterNode)
        throws CatalogFilterInvalidException {
            // super call
        super.visitAttrEqualFilter(filterNode);
            // transformation
        String name  = filterNode.getName();
        String value = filterNode.getValue();
        try {
            QueryEntry entry = new QueryEntry();
            entry.setRowType(QueryEntry.ROW_TYPE_ATTRIBUTE);
            entry.setLocation(name);
            entry.setContentType(QueryEntry.CONTENT_TYPE_STRING);
            entry.setTermWeight(10000);
            if(theParentFilterNode instanceof CatalogFilterNot) {
                entry.setValue(value, "", QueryEntry.ATTRIBUTE_OPERATOR_NE);
            }
            else {
                entry.setValue(value, "", QueryEntry.ATTRIBUTE_OPERATOR_EQ);
            }
            theSearchManager.addQueryEntry(entry);
        }
        catch(TrexException ex) {
            throw new CatalogFilterInvalidException(ex.getLocalizedMessage());
        }
    }

    public void visitAttrLinguisticFilter(CatalogFilterAttrLinguistic filterNode)
        throws CatalogFilterInvalidException {
            // super call
        super.visitAttrLinguisticFilter(filterNode);
            // transformation
        String name    = filterNode.getName();
        String pattern = filterNode.getPattern();
        byte type      = QueryEntry.CONTENT_TYPE_AND;

        try {
            QueryEntry entry = new QueryEntry();
            entry.setRowType(QueryEntry.ROW_TYPE_ATTRIBUTE);
            entry.setLocation(name);
            entry.setContentType(type);
            entry.setTermWeight(10000);
            entry.setTermAction(QueryEntry.TERM_ACTION_LINGUISTIC);
            if(theParentFilterNode instanceof CatalogFilterNot) {
                entry.setValue(pattern, "", QueryEntry.ATTRIBUTE_OPERATOR_NE);
            }    
            else {
                entry.setValue(pattern, "", QueryEntry.ATTRIBUTE_OPERATOR_EQ);
            }    
            theSearchManager.addQueryEntry(entry);
        }
        catch(TrexException ex) {
            throw new CatalogFilterInvalidException(ex.getLocalizedMessage());
        }
    }

    protected void visitAttrFuzzyFilter(CatalogFilterAttrFuzzy filterNode)
        throws CatalogFilterInvalidException {
        // super call
        super.visitAttrFuzzyFilter(filterNode);
        // transformation
        String name    = filterNode.getName();
        String pattern = filterNode.getPattern();
        int weight     = ITrexGenCatalogConstants.TERM_WEIGHT;
        byte type      = QueryEntry.CONTENT_TYPE_AND;

        // set default weight if necessary
        try {
            if(filterNode.getParameter("TERM_WEIGHT") != null) {
                weight = Integer.valueOf(filterNode.getParameter("TERM_WEIGHT")).intValue();
            }
        }
        catch(NumberFormatException numbEx) {
            weight = ITrexGenCatalogConstants.TERM_WEIGHT;
        }

        try {
            QueryEntry entry = new QueryEntry();
            entry.setRowType(QueryEntry.ROW_TYPE_ATTRIBUTE);
            entry.setLocation(name);
            entry.setContentType(type);
            entry.setTermWeight(weight);
            entry.setTermAction(QueryEntry.TERM_ACTION_FUZZY);
            if(theParentFilterNode instanceof CatalogFilterNot) {
                entry.setValue(pattern, "", QueryEntry.ATTRIBUTE_OPERATOR_NE);
            }
            else {
                entry.setValue(pattern, "", QueryEntry.ATTRIBUTE_OPERATOR_EQ);
            }
            theSearchManager.addQueryEntry(entry);
        }
        catch(TrexException ex) {
            throw new CatalogFilterInvalidException(ex.getLocalizedMessage());
        }
    }

    protected void visitNotFilter(CatalogFilterNot filterNode)
        throws CatalogFilterInvalidException {
        // super call
        super.visitNotFilter(filterNode);
        // transformation
        CatalogFilter operand = (CatalogFilter)filterNode.getOperands().get(0);
        theParentFilterNode = filterNode;
        operand.assign(this);
        
        try {
            if (!operand.isLeaf()) {
                // in case of Verity it should work
                QueryEntry entry = new QueryEntry();
                entry.setRowType(QueryEntry.ROW_TYPE_OPERATOR);
                entry.setValue(QueryEntry.QUERY_OPERATOR_NOT, "", QueryEntry.ATTRIBUTE_OPERATOR_EQ);
                theSearchManager.addQueryEntry(entry);
            }
        }
        catch(TrexException ex) {
            throw new CatalogFilterInvalidException(ex.getLocalizedMessage());
        }
    }

    protected void visitOrFilter(CatalogFilterOr filterNode)
        throws CatalogFilterInvalidException {
            // super call
        super.visitOrFilter(filterNode);
            // transformation - inorder
        CatalogFilter lOperand = (CatalogFilter)filterNode.getOperands().get(0);
        theParentFilterNode = filterNode;
        lOperand.assign(this);
        CatalogFilter rOperand = (CatalogFilter)filterNode.getOperands().get(1);
        theParentFilterNode = filterNode;
        rOperand.assign(this);
        
        try {
            QueryEntry entry = new QueryEntry();
            entry.setRowType(QueryEntry.ROW_TYPE_OPERATOR);
            entry.setValue(QueryEntry.QUERY_OPERATOR_OR, "", QueryEntry.ATTRIBUTE_OPERATOR_EQ);
            theSearchManager.addQueryEntry(entry);
        }
        catch(TrexException ex) {
            throw new CatalogFilterInvalidException(ex.getLocalizedMessage());
        }
    }

    protected void visitAndFilter(CatalogFilterAnd filterNode)
        throws CatalogFilterInvalidException {
            // super call
        super.visitAndFilter(filterNode);
            // transformation - inorder
        CatalogFilter lOperand = (CatalogFilter)filterNode.getOperands().get(0);
        theParentFilterNode = filterNode;
        lOperand.assign(this);
        CatalogFilter rOperand = (CatalogFilter)filterNode.getOperands().get(1);
        theParentFilterNode = filterNode;
        rOperand.assign(this);
        
        try {
            QueryEntry entry = new QueryEntry();
            entry.setRowType(QueryEntry.ROW_TYPE_OPERATOR);
            entry.setValue(QueryEntry.QUERY_OPERATOR_AND, "", QueryEntry.ATTRIBUTE_OPERATOR_EQ);
            theSearchManager.addQueryEntry(entry);
        } 
        catch(TrexException ex) {
            throw new CatalogFilterInvalidException(ex.getLocalizedMessage());
        }
    }
    
    protected void visitAttrBetweenFilter(CatalogFilterAttrBetween filterNode)
        throws CatalogFilterInvalidException {
            // super call
        super.visitAttrBetweenFilter(filterNode);
            // transformation
        String name  = filterNode.getName();
        String lowValue = filterNode.getLowValue();
        String highValue = filterNode.getHighValue();
        try {
            QueryEntry entry = new QueryEntry();
            entry.setRowType(QueryEntry.ROW_TYPE_ATTRIBUTE);
            entry.setLocation(name);
            entry.setContentType(QueryEntry.CONTENT_TYPE_FLOAT);
            entry.setTermAction(QueryEntry.TERM_ACTION_EXACT);
            if(theParentFilterNode instanceof CatalogFilterNot) {
                // not supported currently
                throw new CatalogFilterInvalidException("NOT is not supported for BETWEEN operator");
            }
            else {
                entry.setValue(lowValue, highValue, QueryEntry.ATTRIBUTE_OPERATOR_BT);
            }
            theSearchManager.addQueryEntry(entry);
        }
        catch(TrexException ex) {
            throw new CatalogFilterInvalidException(ex.getLocalizedMessage());
        }
    }

}
