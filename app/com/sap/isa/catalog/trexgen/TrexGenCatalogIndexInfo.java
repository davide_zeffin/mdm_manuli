/*****************************************************************************
    Class:        TrexGenCatalogQueryCompiler
    Copyright (c) 2006, SAP AG, All rights reserved.
    Author:
    Created:      15.09.2006
    Version:      1.0

    $Revision$
    $Date$
*****************************************************************************/
package com.sap.isa.catalog.trexgen;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import com.sap.isa.core.logging.IsaLocation;
import com.sapportals.trex.TrexException;
import com.sapportals.trex.core.AttributeDefinition;
import com.sapportals.trex.core.SearchManager;
import com.sapportals.trex.core.TrexFactory;
import com.sapportals.trex.core.admin.AdminIndex;

/**
 * This class encapsulate all information about an index of an Trex catalog.
 * @version     1.0
 */
public class TrexGenCatalogIndexInfo implements Serializable
{
        /**
         * The class is serializable. All attributes are serializable with the
         * default implementation.
         */

        /**
         * Constant for the property POS_NR. This property defines the order
         * of the attributes as it was defined in the CRM system.
         */
    static String PROP_POS_NR = "POS_NR";

        /**
         * Constant for the property DESCRIPTION. This property defines the language
         * dependant text of the attribute.
         */
    static String PROP_DESCRIPTION = "DESCRIPTION";

        /**
         * Constant for the property GUID. This property defines the guid of the
         * attribute as it was defined in the CRM system.
         */
    static String PROP_GUID = "GUID";

        /**
         * Constant for the URL for the Trex server
         */
    static String URL = "/TrexHttpServer/TrexHttpServer.dll?";

       // the logging instance
    private static IsaLocation log =
        IsaLocation.getInstance(TrexGenCatalogIndexInfo.class.getName());
    protected String theIndexGuid; // StoreId
    protected String theIndexType; // index level
    protected String theCategoryId; // area name
    protected List theAttributes;
    protected String theSearchEngine;
    protected String theCatId; // unique id of the category

    /**
     * Creates a new instance of a index.
     *
     * @param aIndexGuid    unique identifier of index on Trex
     * @param aindexType    index level (A, P, S)
     * @param aCategoryID       name of area (in case of 'S' index)
     * @param aAdminIndex   AdminIndex for the index
     */
    protected TrexGenCatalogIndexInfo(String aIndexGuid,
                                String aIndexType,
                                String aCategoryId,
                                AdminIndex aAdminIndex) {
        final String METHOD = "TrexGenCatalogIndexInfo()";
        log.entering(METHOD);
        theIndexGuid = aIndexGuid;
        theIndexType = aIndexType;
        theCategoryId = aCategoryId;
        theAttributes  = new ArrayList();
        theSearchEngine = aAdminIndex.getTMEngineName();
        
        AttributeDefinition attrDef = aAdminIndex.getFirstDocumentAttribute(); 
        while (attrDef != null) {                                              
            String attrName = attrDef.getName().toUpperCase();                
            theAttributes.add(attrName);                                      
            attrDef = aAdminIndex.getNextDocumentAttribute();                 
            if (log.isDebugEnabled()) {
                log.debug(METHOD + ": attrName = " + attrName);
            }
        }                                                                     
        
        // log the creation
        if (log.isInfoEnabled()) {
            log.info("theIndexType=" + theIndexType +
                     "/ theCatId=" + theCatId +
                     "/ theIndexGuid=" + theIndexGuid +
                     "/ theArea=" + theCategoryId);
        }
        log.exiting();
    }

    /**
     * Get the search manager assoiated with the corresponding Index we are 
     * accessing.
     *
     * @return a <code>SearchManager</code> associated with this index.
     * @exception TrexException if an error occurs
     */
    public SearchManager getSearchManager(String language) 
            throws TrexException {
        SearchManager searchManager =
            TrexFactory.getSearchManager();

        searchManager.addIndexId(theIndexGuid,language);
        return searchManager;
    }

    /**
     * Generates a unique identifier for the index.
     *
     * @param indexType  name of index level
     * @param categoryID       name of area
     * @return a <code>String</code> value as key for the Index
     */
    public static String generateKey(String indexType, String categoryId) {
        return indexType + categoryId;
    }

    /**
     * Generates a unique identifier for the index. The key is built
     * through concatenation of attrLevel and area.
     *
     * @return  unique key for index
     */
    public String generateKey() {
        return generateKey(getIndexType(), getCategoryID());
    }

    /**
     * Adds an attribute to the list of attributes.
     *
     * @param attribute  name of the attribute to be added
     */
    public void addAttribute(String attribute) {
        theAttributes.add(attribute);
    }

    /**
     * Returns <code>true</code> if the index contains the requested attribute.
     *
     * @param name name of the requested attribute
     * @return boolean that indicates if the index contains the given attribute
     */
    public boolean hasAttribute(String name) {
        if (theAttributes != null)
            return theAttributes.contains(name);
        else
            return false;
    }

    /**
     * Returns the index type.
     *
     * @return  name of index level (possible values A, P, S)
     */
    public String getIndexType() {
        return theIndexType;
    }

    /**
     * Returns the list of attribute names of the index.
     *
     * @return  list of attribute names
     */
    public List getAttributes() {
        return theAttributes;
    }

    /**
     * Returns the category id.
     *
     * @return  category id
     */
    public String getCategoryID() {
        return theCategoryId;
    }

    /**
     * Returns the store id.
     *
     * @return  store id
     */
    public String getStoreId() {
        return theIndexGuid;
    }

    /**
     * Returns the index guid
     *
     * @return  index guid
     */
    public String getIndexGuid() {
        return theIndexGuid;
    }

    /**
     * Returns the search engine name.
     *
     * @return the name of the search engine
     */
    public String getSearchEngine() {
        return this.theSearchEngine;
    }

}
