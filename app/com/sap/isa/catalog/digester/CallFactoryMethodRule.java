/*****************************************************************************
  Copyright (c) 2000, SAPMarkets Europe GmbH, Germany, All rights reserved.
  Created:      30 March 2001

  $Id: //sap/ESALES_base/30_SP_COR/src/_pcatAPI/java/com/sap/isa/catalog/digester/CallFactoryMethodRule.java#3 $
  $Revision: #3 $
  $Change: 150872 $
  $DateTime: 2003/09/29 09:18:51 $
*****************************************************************************/
package com.sap.isa.catalog.digester;

//  XML specific imports
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

import org.xml.sax.Attributes;

import com.sap.isa.core.logging.IsaLocation;

/**
 * Rule Implementation that calls a factory method on the top (parent) object
 * and stores the created object onto the object stack. When the element is
 * completed the object will be popped.
 */
public class CallFactoryMethodRule extends Rule
{
    protected String theMethodName;
    protected String[] theAttributeNames;
        private static IsaLocation theStaticLocToLog = 
        IsaLocation.getInstance(CallFactoryMethodRule.class.getName());


    /**
     * Creates a rule that calls a FactoryMethod on the top
     * object that takes no arguments.
     */
    public CallFactoryMethodRule(Digester theDigester, String methodName)
    {
        super(theDigester);
        this.theMethodName = methodName;
    }

    /**
     * Creates a rule that calls a FactoryMethod on the top
     * object that takes a list of attributes as String arguments.
     */
    public CallFactoryMethodRule(Digester theDigester,
                                String methodName,
                                String[] attributeNames)
    {
        super(theDigester);
        this.theMethodName = methodName;
        this.theAttributeNames = attributeNames;
    }

    /**
     * Create an object with the FactoryMethod object
     * when its element is found and place on the stack.
     */
    public void begin(Attributes listOfAtributes)
    {
        Object theTop = digester.peek();
        if(theTop == null)
            return;

        Class theClassOfTop = theTop.getClass();
        try
        {
            Object theProduct = null;
            if(theAttributeNames!=null)
            {
                Class theStringClass = String.class;
                Class[] theStringArguments =
                    new Class[theAttributeNames.length];
                for(int i = 0; i < theStringArguments.length; i++)
                {
                    theStringArguments[i] = theStringClass;
                }

                Method theFactoryMethod =
                    theClassOfTop.getMethod(theMethodName,
                    theStringArguments);

                Object[] theStringArgumentValues =
                    new Object[theAttributeNames.length];
                for(int i = 0; i < theStringArgumentValues.length; i++)
                {
                    theStringArgumentValues[i] =
                        listOfAtributes.getValue(theAttributeNames[i]);
                }

                theProduct =
                    theFactoryMethod.invoke(theTop,
                    theStringArgumentValues);
            }
            else
            {
                Method theFactoryMethod =
                    theClassOfTop.getDeclaredMethod(theMethodName, new Class[]{});
                theProduct =
                    theFactoryMethod.invoke(theTop,new Object[] {});
            }
            digester.push(theProduct);
            if (theStaticLocToLog.isDebugEnabled()) 
            {
                theStaticLocToLog.debug("CallFactoryMethodRule: Pushed instance of:"
                             + theProduct.getClass().getName());
            } // end of if ()
        }
        catch(NoSuchMethodException nsme)
        {
            System.out.println(nsme.getLocalizedMessage());
            nsme.printStackTrace(System.out);
        }
        catch(InvocationTargetException ite)
        {
            System.out.println(ite.getLocalizedMessage());
            ite.printStackTrace(System.out);
        }
        catch(IllegalAccessException iae)
        {
            System.out.println(iae.getLocalizedMessage());
            iae.printStackTrace(System.out);
        }

        return;
    }

    /*
     * Nothing to do
     */
    public void body(String theBody)
    {
        return;
    }

    /*
     * Nothing to do
     */
    public void finish()
    {
        return;
    }

    /*
     * Pop the created objject from the stack
     */
    public void end()
    {
        Object top = digester.pop();
        if (theStaticLocToLog.isDebugEnabled())
            theStaticLocToLog.debug("CallFactoryMethodRule: Pop " + top.getClass().getName());
    }
}
