/*****************************************************************************
  Copyright (c) 2000, SAPMarkets Europe GmbH, Germany, All rights reserved.
  Created:      30 March 2001

  $Id: //sap/ESALES_base/30_SP_COR/src/_pcatAPI/java/com/sap/isa/catalog/digester/CallParamRule.java#2 $
  $Revision: #2 $
  $Change: 127411 $
  $DateTime: 2003/04/30 17:20:32 $
*****************************************************************************/
package com.sap.isa.catalog.digester;

import org.xml.sax.Attributes;

import com.sap.isa.core.logging.IsaLocation;

/**
 * Rule implementation that saves a parameter from either an attribute of
 * this element, or from the element body, to be used in a call generated
 * by a surrounding CallMethodRule rule.
 *
 * @version $Revision: #2 $ $Date: 2003/04/30 $
 */

public final class CallParamRule extends Rule
{
    private static IsaLocation theStaticLocToLog = 
        IsaLocation.getInstance(CallMethodRule.class.getName());

        // ----------------------------------------------------------- Constructors

        /**
         * Construct a "call parameter" rule that will save the body text of this
         * element as the parameter value.
         *
         * @param digester The associated Digester
         * @param paramIndex The zero-relative parameter number
         */
    public CallParamRule(Digester digester, int paramIndex)
    {
        this(digester, paramIndex, null);
    }

        /**
         * Construct a "call parameter" rule that will save the value of the
         * specified attribute as the parameter value.
         *
         * @param digester The associated Digester
         * @param paramIndex The zero-relative parameter number
         * @param attributeName The name of the attribute to save
         */
    public CallParamRule(   Digester digester,
                            int paramIndex,
                            String attributeName)
    {
        super(digester);
        this.paramIndex = paramIndex;
        this.attributeName = attributeName;
    }

        // ----------------------------------------------------- Instance Variables

        /**
         * The attribute from which to save the parameter value
         */
    private String attributeName = null;

        /**
         * The body text collected from this element.
         */
    private String bodyText = null;

        /**
         * The zero-relative index of the parameter we are saving.
         */
    private int paramIndex = 0;

        // --------------------------------------------------------- Public Methods

        /**
         * Process the start of this element.
         *
         * @param attributes The attribute list for this element
         */
    public void begin(Attributes attributes) throws Exception
    {
        if (attributeName != null)
            bodyText = attributes.getValue(attributeName);
    }

        /**
         * Process the body text of this element.
         *
         * @param bodyText The body text of this element
         */
    public void body(String bodyText) throws Exception
    {
        if (attributeName == null)
            this.bodyText = bodyText.trim();

    }

        /**
         * Process the end of this element.
         */
    public void end() throws Exception
    {
        String parameters[] = (String[]) digester.peek();
        parameters[paramIndex] = bodyText;
        if (theStaticLocToLog.isDebugEnabled() ) 
        {
            theStaticLocToLog.debug("CallParamRule: Set paramIndex "
                         + paramIndex
                         + " on parametersList to "
                         + bodyText);
        } // end of if ()
    }

        /**
         * Clean up after parsing is complete.
         */
    public void finish() throws Exception
    {
        bodyText = null;
    }
}
