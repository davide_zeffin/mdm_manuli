/*****************************************************************************
  Copyright (c) 2000, SAPMarkets Europe GmbH, Germany, All rights reserved.
  Created:      30 March 2001

  $Id: //sap/ESALES_base/30_SP_COR/src/_pcatAPI/java/com/sap/isa/catalog/digester/Digester.java#3 $
  $Revision: #3 $
  $Change: 144303 $
  $DateTime: 2003/08/22 20:18:21 $
*****************************************************************************/
package com.sap.isa.catalog.digester;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.ArrayList;
import java.util.EmptyStackException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

import org.apache.commons.collections.ArrayStack;
import org.xml.sax.Attributes;
import org.xml.sax.InputSource;
import org.xml.sax.Locator;
import org.xml.sax.SAXException;
import org.xml.sax.SAXParseException;
import org.xml.sax.helpers.DefaultHandler;

import com.sap.isa.catalog.ICatalogMessagesKeys;
import com.sap.isa.core.logging.IsaLocation;

/**
 * <p>A <strong>Digester</strong> processes an XML input stream by matching a
 * series of element nesting patterns to execute Rules that have been added
 * prior to the start of parsing.  This package was inspired by the
 * <code>XmlMapper</code> class that was part of Tomcat 3.0 and 3.1,
 * but is organized somewhat differently.</p>
 *
 * <p>See the <a href="package-summary.html#package_description">Digester
 * Developer Guide</a> for more information.</p>
 *
 * <p><strong>IMPLEMENTATION NOTE</strong> - A single Digester instance may
 * only be used within the context of a single thread at a time, and a call
 * to <code>parse()</code> must be completed before another can be initiated
 * even from the same thread.</p>
 *
 * @version $Revision: #3 $ $Date: 2003/08/22 $
 */

public final class Digester extends DefaultHandler
{
    private static IsaLocation theStaticLocToLog = 
        IsaLocation.getInstance(Digester.class.getName());
    
        // --------------------------------------------------------- Constructors
        /**
         * Construct a new Digester with default properties.
         */
    public Digester()
    {
        super();
    }

        // --------------------------------------------------- Instance Variables

        /**
         * The body text of the current element.
         */
    private StringBuffer bodyText = new StringBuffer();


        /**
         * The stack of body text string buffers for surrounding elements.
         */
    private ArrayStack bodyTexts = new ArrayStack();

        /**
         * The URLs of DTDs that have been registered, keyed by the public
         * identifier that corresponds.
         */
    private HashMap dtds = new HashMap();

        /**
         * The Locator associated with our parser.
         */
    private Locator locator = null;

        /**
         * The current match pattern for nested element processing.
         */
    private String match = "";

        /**
         * The SAXParser we will use to parse the input stream.
         */
    private SAXParser parser = null;

        /**
         * The "root" element of the stack (in other words, the last object
         * that was popped.
         */
    private Object root = null;

        /**
         * The set of Rules that have been registered with this Digester.  The
         * key is the matching pattern against the current element stack, and
         * the value is a List containing the Rules for that pattern, in the
         * order that they were registered.
         */
    private HashMap rules = new HashMap();

        /**
         * The object stack being constructed.
         */
    private ArrayStack stack = new ArrayStack();

        /**
         * Do we want to use a validating parser?
         */
    private boolean validating = false;

        // ----------------------------------------------------------- Properties

        /**
         * Return the current depth of the element stack.
         */
    public int getCount()
    {
        return (stack.size());
    }

        /**
         * Return the SAXParser we will use to parse the input stream.  If there
         * is a problem creating the parser, return <code>null</code>.
         */
    public SAXParser getParser()
    {
            // Return the parser we already created (if any)
        if (parser != null)
            return (parser);

            // Create and return a new parser
        try
        {
            SAXParserFactory factory = SAXParserFactory.newInstance();
            factory.setNamespaceAware(false);
            factory.setValidating(validating);
            parser = factory.newSAXParser();
            return (parser);
        }
        catch (Exception e)
        {
    //            log("Digester.getParser: ", e);
            theStaticLocToLog.error(
                ICatalogMessagesKeys.PCAT_EXCEPTION, e);            
            return (null);
        }
    }

        /**
         * Return the validating parser flag.
         */
    public boolean getValidating()
    {
        return (this.validating);
    }

        /**
         * Set the validating parser flag.  This must be called before
         * <code>parse()</code> is called the first time.
         *
         * @param validating The new validating parser flag.
         */
    public void setValidating(boolean validating)
    {
        this.validating = validating;
    }

        // ---------------------------------------------- DocumentHandler Methods

        /**
         * Process notification of character data received from the body of
         * an XML element.
         *
         * @param buffer The characters from the XML document
         * @param start Starting offset into the buffer
         * @param length Number of characters from the buffer
         *
         * @exception SAXException if a parsing error is to be reported
         */
    public void characters(char buffer[], int start, int length)
        throws SAXException
    {
            //  if (debug >= 3)
            //      log("characters(" + new String(buffer, start, length) + ")");
        bodyText.append(buffer, start, length);
    }

        /**
         * Process notification of the end of the document being reached.
         *
         * @exception SAXException if a parsing error is to be reported
         */
    public void endDocument() throws SAXException
    {
            //  if (debug >= 3)
            //      log("endDocument()");
        if (getCount() > 1)
            if (theStaticLocToLog.isDebugEnabled())
                theStaticLocToLog.debug("endDocument():  " + getCount() + " elements left");
        while (getCount() > 1)
            pop();

            // Fire "finish" events for all defined rules
        Iterator keys = this.rules.keySet().iterator();
        while (keys.hasNext())
        {
            String key = (String) keys.next();
            List rules = (List) this.rules.get(key);
            for (int i = 0; i < rules.size(); i++)
            {
                try
                {
                    ((Rule) rules.get(i)).finish();
                }
                catch (Exception e)
                {
//                    theStaticLocToLog.error("Finish event threw exception", e);
                    theStaticLocToLog.error(ICatalogMessagesKeys.PCAT_EXCEPTION,
                        new Object[]{Digester.class.getName()},e);
                    throw new SAXException(e);
                }
            }
        }
            // Perform final cleanup
        clear();
        if (theStaticLocToLog.isInfoEnabled())
            theStaticLocToLog.info(ICatalogMessagesKeys.PCAT_INFO,
                new Object[]{"endDocument()"},
                null);
    }

        /**
         * Process notification of the end of an XML element being reached.
         *
         * @param name Name of the element that is ending
         *
         * @exception SAXException if a parsing error is to be reported
         */
    public void endElement( String nameSpaceURI,
                            String localName,
                            String rawName) throws org.xml.sax.SAXException
    {
        if (theStaticLocToLog.isDebugEnabled())
            theStaticLocToLog.debug("endElement(" + match + ")");
        List rules = getRules(match);

            // Fire "body" events for all relevant rules
        if (rules != null)
        {
                //if (debug >= 3)
                //log("  Firing 'body' events for " + rules.size() + " rules");
            String bodyText = this.bodyText.toString();
            for (int i = 0; i < rules.size(); i++)
            {
                try
                {
                    ((Rule) rules.get(i)).body(bodyText);
                }
                catch (Exception e)
                {
                theStaticLocToLog.error(
                        ICatalogMessagesKeys.PCAT_EXCEPTION,
                        new Object[]{Digester.class.getName() + ": Body event threw exception: "},
                        e);

                    throw new SAXException(e);
                }
            }
        }

            // Recover the body text from the surrounding element
        bodyText = (StringBuffer) bodyTexts.pop();

            // Fire "end" events for all relevant rules in reverse order
        if (rules != null)
        {
                // if (debug >= 3)
                //log("  Firing 'end' events for " + rules.size() + " rules");
            for (int i = 0; i < rules.size(); i++)
            {
                int j = (rules.size() - i) - 1;
                try
                {
                    ((Rule) rules.get(j)).end();
                }
                catch (Exception e)
                {
                    theStaticLocToLog.error(
                        ICatalogMessagesKeys.PCAT_EXCEPTION,
                        new Object[]{Digester.class.getName() + ": End event threw exception: "},
                        e);
                    throw new SAXException(e);
                }
            }
        }

            // Recover the previous match expression
        int slash = match.lastIndexOf('/');
        if (slash >= 0)
            match = match.substring(0, slash);
        else
            match = "";
    }

        /**
         * Process notification of ignorable whitespace received from the body of
         * an XML element.
         *
         * @param buffer The characters from the XML document
         * @param start Starting offset into the buffer
         * @param length Number of characters from the buffer
         *
         * @exception SAXException if a parsing error is to be reported
         */
    public void ignorableWhitespace(char buffer[], int start, int len)
        throws SAXException
    {
            //  if (debug >= 3)
            //      log("ignorableWhitespace(" +
            //      new String(buffer, start, len) + ")");

        ;   // No processing required
    }

        /**
         * Process notification of a processing instruction that was encountered.
         *
         * @param target The processing instruction target
         * @param data The processing instruction data (if any)
         *
         * @exception SAXException if a parsing error is to be reported
         */
    public void processingInstruction(String target, String data)
        throws SAXException
    {
            //  if (debug >= 3)
            //      log("processingInstruction('" + target + "', '" + data + "')");
        ;   // No processing is required
    }

        /**
         * Implements ContentHandler method. Uses only rawName.
         * SAX 1.0 is not aware of this method.
         */
    public void skippedEntity(String parm1)
        throws org.xml.sax.SAXException
    {
            //@todo Log the stuff
        return;
    }

        /**
         * Set the document locator associated with our parser.
         *
         * @param locator The new locator
         */
    public void setDocumentLocator(Locator locator)
    {
            //if (debug >= 3)
            //  log("setDocumentLocator()");
        this.locator = locator;
    }

        /**
         * Process notification of the beginning of the document being reached.
         *
         * @exception SAXException if a parsing error is to be reported
         */
    public void startDocument() throws SAXException
    {
        if (theStaticLocToLog.isInfoEnabled())
            theStaticLocToLog.info(ICatalogMessagesKeys.PCAT_INFO,
                new Object[]{"startDocument()"},
                null);
    }

        /**
         * Process notification of the start of an XML element being reached.
         *
         * @param name Name of the element that is starting
         * @param list The attributes associated with this element
         *
         * @exception SAXException if a parsing error is to be reported
         */
    public void startElement(   String nameSpaceURI,
                                String localName,
                                String rawName,
                                Attributes theAtts) throws SAXException
    {
            // Save the body text accumulated for our surrounding element
        bodyTexts.push(bodyText);
        bodyText.setLength(0);

            // Compute the current matching rule
        if (match.length() > 0)
            match += "/" + rawName;
        else
            match = rawName;
        if (theStaticLocToLog.isDebugEnabled())
            theStaticLocToLog.debug("startElement(" + match + ")");

            // Fire "begin" events for all relevant rules
        List rules = getRules(match);
        if (rules != null)
        {
                //      if (debug >= 3)
                //      log("  Firing 'begin' events for " + rules.size() + " rules; bodyText is:" + this.bodyText.toString());
            for (int i = 0; i < rules.size(); i++)
            {
                try
                {
                    ((Rule) rules.get(i)).begin(theAtts);
                }
                catch (Exception e)
                {
                    theStaticLocToLog.error(
                        ICatalogMessagesKeys.PCAT_EXCEPTION,
                        new Object[]{Digester.class.getName() + ": Begin event threw exception: "},
                        e);
                    throw new SAXException(e);
                }
            }
        }
    }

        // --------------------------------------------------- DTDHandler Methods

        /**
         * Receive notification of a notation declaration event.
         *
         * @param name The notation name
         * @param publicId The public identifier (if any)
         * @param systemId The system identifier (if any)
         */
    public void notationDecl(String name, String publicId, String systemId)
    {
        if (theStaticLocToLog.isDebugEnabled())
            theStaticLocToLog.debug("notationDecl('"
                + name
                + "', '"
                + publicId
                + "', '"
                +systemId
                + "')");
    }

        /**
         * Receive notification of an unparsed entity declaration event.
         *
         * @param name The unparsed entity name
         * @param publicId The public identifier (if any)
         * @param systemId The system identifier (if any)
         * @param notation The name of the associated notation
         */
    public void unparsedEntityDecl( String name,
                                    String publicId,
                                    String systemId,
                                    String notation)
    {
        if (theStaticLocToLog.isDebugEnabled())
            theStaticLocToLog.debug("unparsedEntityDecl('"
                + name
                + "', '"
                + publicId
                + "', '"
                + systemId
                + "', '"
                + notation
                + "')");
    }

        // ----------------------------------------------- EntityResolver Methods


        /**
         * Resolve the requested external entity.
         *
         * @param publicId The public identifier of the entity being referenced
         * @param systemId The system identifier of the entity being referenced
         *
         * @exception SAXException if a parsing exception occurs
         */
    public InputSource resolveEntity(String publicId, String systemId)
        throws SAXException
    {
        if (theStaticLocToLog.isDebugEnabled())
            theStaticLocToLog.debug("resolveEntity('" + publicId + "', '" + systemId + "')");

            // Has this system identifier been registered?
        String dtdURL = null;
        if (publicId != null)
            dtdURL = (String) dtds.get(publicId);
        if (dtdURL == null)
        {
            if (theStaticLocToLog.isDebugEnabled())
                theStaticLocToLog.debug(" Not registered, use system identifier");
            return (null);
        }

            // Return an input source to our alternative URL
        if (theStaticLocToLog.isDebugEnabled())
            theStaticLocToLog.debug(" Resolving to alternate DTD '" + dtdURL + "'");

        try
        {
            URL url = new URL(dtdURL);
            InputStream stream = url.openStream();
            return (new InputSource(stream));
        }
        catch (Exception e)
        {
            throw new SAXException(e);
        }
    }

        // ------------------------------------------------- ErrorHandler Methods

        /**
         * Receive notification of a recoverable parsing error.
         *
         * @param exception The warning information
         *
         * @exception SAXException if a parsing exception occurs
         */
    public void error(SAXParseException exception) throws SAXException
    {
          theStaticLocToLog.error(
                        ICatalogMessagesKeys.PCAT_EXCEPTION,
                        new Object[]{Digester.class.getName() + ": parse Error at line "
                            + exception.getLineNumber() 
                            + " column "
                            + exception.getColumnNumber() + ": "
                            + exception.getMessage()},
                            exception);
     }

        /**
         * Receive notification of a fatal parsing error.
         *
         * @param exception The warning information
         *
         * @exception SAXException if a parsing exception occurs
         */
    public void fatalError(SAXParseException exception) throws SAXException
    {
           theStaticLocToLog.error(
                        ICatalogMessagesKeys.PCAT_EXCEPTION,
                        new Object[]{Digester.class.getName() + ": parse Fatal Error at line "
                            + exception.getLineNumber() 
                            + " column "
                            + exception.getColumnNumber() + ": "
                            + exception.getMessage()},
                            exception);
    }

        /**
         * Receive notification of a parsing warning.
         *
         * @param exception The warning information
         *
         * @exception SAXException if a parsing exception occurs
         */
    public void warning(SAXParseException exception) throws SAXException
    {
           
        theStaticLocToLog.warn(ICatalogMessagesKeys.PCAT_EXCEPTION,
                     new Object[]{Digester.class.getName() + ": parse Warning at line "
                     + exception.getLineNumber() 
                     + " column "
                     + exception.getColumnNumber() + ": "
                     + exception.getMessage()},
                     exception);
    }

        // ------------------------------------------------------ Logging Methods

        /**
         * Log a message to the log writer associated with this context.
         *
         * @param message The message to be logged
         */
/*    public void log(String message)
    {
        getLocation().debug(message);
    }
*/
        /**
         * Log a message and associated exception to the log writer
         * associated with this context.
         *
         * @param message The message to be logged
         * @param exception The associated exception to be logged
         */
/*    public void log(String message, Throwable exception)
    {
        getLocation().error(message, exception);
    }

    synchronized IsaLocation getLocation()
    {
        if (theLocToLog==null ) 
        {
            theLocToLog = IsaLocation.getInstance(this.getClass().getName());
        } // end of if ()
        return theLocToLog;
    }
*/    

        // ------------------------------------------------------- Public Methods

        /**
         * Parse the content of the specified file using this Digester.  Returns
         * the root element from the object stack (if any).
         *
         * @param file File containing the XML data to be parsed
         *
         * @exception IOException if an input/output error occurs
         * @exception SAXException if a parsing exception occurs
         */
    public Object parse(File file) throws IOException, SAXException
    {
        getParser().parse(file, this);
        return (root);
    }

        /**
         * Parse the content of the specified input source using this Digester.
         * Returns the root element from the object stack (if any).
         *
         * @param input Input source containing the XML data to be parsed
         *
         * @exception IOException if an input/output error occurs
         * @exception SAXException if a parsing exception occurs
         */
    public Object parse(InputSource input) throws IOException, SAXException
    {
        getParser().parse(input, this);
        return (root);
    }

        /**
         * Parse the content of the specified input stream using this Digester.
         * Returns the root element from the object stack (if any).
         *
         * @param input Input stream containing the XML data to be parsed
         *
         * @exception IOException if an input/output error occurs
         * @exception SAXException if a parsing exception occurs
         */
    public Object parse(InputStream input) throws IOException, SAXException
    {
        getParser().parse(input, this);
        return (root);
    }

        /**
         * Parse the content of the specified URI using this Digester.
         * Returns the root element from the object stack (if any).
         *
         * @param uri URI containing the XML data to be parsed
         *
         * @exception IOException if an input/output error occurs
         * @exception SAXException if a parsing exception occurs
         */
    public Object parse(String uri) throws IOException, SAXException {

        getParser().parse(uri, this);
        return (root);

    }


        /**
         * Register the specified DTD URL for the specified public identifier.
         * This must be called before the first call to <code>parse()</code>.
         *
         * @param publicId Public identifier of the DTD to be resolved
         * @param dtdURL The URL to use for reading this DTD
         */
    public void register(String publicId, String dtdURL)
    {
        if (theStaticLocToLog.isDebugEnabled())
            theStaticLocToLog.debug("register('" + publicId + "', '" + dtdURL + "'");
        dtds.put(publicId, dtdURL);
    }


        // --------------------------------------------------------- Rule Methods

        /**
         * Register a new Rule matching the specified pattern.
         *
         * @param pattern Element matching pattern
         * @param rule Rule to be registered
         */
    public void addRule(String pattern, Rule rule)
    {
        List list = (List) rules.get(pattern);
        if (list == null)
        {
            list = new ArrayList();
            rules.put(pattern, list);
        }
        list.add(rule);
    }

        /**
         * Add an "call method" rule for the specified parameters.
         *
         * @param pattern Element matching pattern
         * @param methodName Method name to be called
         * @param paramCount Number of expected parameters (or zero
         *  for a single parameter from the body of this element)
         */
    public void addCallMethod(String pattern, String methodName,
                              int paramCount)
    {
        addRule(pattern,
                new CallMethodRule(this, methodName, paramCount));
    }

        /**
         * Add an "call method" rule for the specified parameters.
         *
         * @param pattern Element matching pattern
         * @param methodName Method name to be called
         * @param paramCount Number of expected parameters (or zero
         *  for a single parameter from the body of this element)
         * @param paramTypes Set of Java class names for the types
         *  of the expected parameters
         */
    public void addCallMethod(  String pattern,
                                String methodName,
                                int paramCount,
                                String paramTypes[])
    {
        addRule(pattern,
                new CallMethodRule(this, methodName, paramCount, paramTypes));
    }

        /**
         * Add a "call parameter" rule for the specified parameters.
         *
         * @param pattern Element matching pattern
         * @param paramIndex Zero-relative parameter index to set
         *  (from the body of this element)
         */
    public void addCallParam(String pattern, int paramIndex)
    {
        addRule(pattern, new CallParamRule(this, paramIndex));

    }

        /**
         * Add a "call parameter" rule for the specified parameters.
         *
         * @param pattern Element matching pattern
         * @param paramIndex Zero-relative parameter index to set
         *  (from the specified attribute)
         * @param attributeName Attribute whose value is used as the
         *  parameter value
         */
    public void addCallParam(   String pattern,
                                int paramIndex,
                                String attributeName)
    {
        addRule(pattern, new CallParamRule(this, paramIndex, attributeName));

    }

        /**
         * Add an "object create" rule for the specified parameters.
         *
         * @param pattern Element matching pattern
         * @param className Java class name to be created
         */
    public void addObjectCreate(String pattern, String className)
    {
        addRule(pattern, new ObjectCreateRule(this, className));
    }

        /**
         * Add an "object create" rule for the specified parameters.
         *
         * @param pattern Element matching pattern
         * @param className Default Java class name to be created
         * @param attributeName Attribute name that optionally overrides
         *  the default Java class name to be created
         */
    public void addObjectCreate(String pattern,
                                String className,
                                String attributeName)
    {
        addRule(pattern, new ObjectCreateRule(this, className, attributeName));
    }

        /**
         * Add a "set next" rule for the specified parameters.
         *
         * @param pattern Element matching pattern
         * @param methodName Method name to call on the parent element
         */
    public void addSetNext(String pattern, String methodName)
    {
        addRule(pattern, new SetNextRule(this, methodName));
    }

        /**
         * Add a "set next" rule for the specified parameters.
         *
         * @param pattern Element matching pattern
         * @param methodName Method name to call on the parent element
         * @param paramType Java class name of the expected parameter type
         */
    public void addSetNext(String pattern, String methodName,
                           String paramType) {

        addRule(pattern,
                new SetNextRule(this, methodName, paramType));

    }
    
        /**
         * Add a "set properties" rule for the specified parameters.
         *
         * @param pattern Element matching pattern
         */
    public void addSetProperties(String pattern)
    {
        addRule(pattern,
                new SetPropertiesRule(this));
    }

        /**
         * Add a "set property" rule for the specified parameters.
         *
         * @param pattern Element matching pattern
         * @param name Attribute name containing the property name to be set
         * @param value Attribute name containing the property value to set
         */
    public void addSetProperty(String pattern, String name, String value)
    {
        addRule(pattern,
                new SetPropertyRule(this, name, value));
    }
    
        /**
         * Add a "set top" rule for the specified parameters.
         *
         * @param pattern Element matching pattern
         * @param methodName Method name to call on the parent element
         */
    public void addSetTop(String pattern, String methodName)
    {
        addRule(pattern,
                new SetTopRule(this, methodName));
    }

        /**
         * Add a "set top" rule for the specified parameters.
         *
         * @param pattern Element matching pattern
         * @param methodName Method name to call on the parent element
         * @param paramType Java class name of the expected parameter type
         */
    public void addSetTop(String pattern, String methodName,
                          String paramType)
    {
        addRule(pattern,
                new SetTopRule(this, methodName, paramType));
    }

        // -------------------------------------------------------- Stack Methods

        /**
         * Clear the current contents of the object stack.
         */
    public void clear()
    {
        match = "";
        bodyTexts.clear();
        bodyText = new StringBuffer();
    }

        /**
         * Return the top object on the stack without removing it.  If there are
         * no objects on the stack, return <code>null</code>.
         */
    public Object peek()
    {
        try
        {
            return (stack.peek());
        }
        catch (EmptyStackException e)
        {
            return (null);
        }
    }

        /**
         * Return the n'th object down the stack, where 0 is the top element
         * and [getCount()-1] is the bottom element.  If the specified index
         * is out of range, return <code>null</code>.
         *
         * @param n Index of the desired element, where 0 is the top of the stack,
         *  1 is the next element down, and so on.
         */
    public Object peek(int n)
    {
        try
        {
            return (stack.peek(n));
        }
        catch (EmptyStackException e)
        {
            return (null);
        }
    }

        /**
         * Pop the top object off of the stack, and return it.  If there are
         * no objects on the stack, return <code>null</code>.
         */
    public Object pop()
    {
        try
        {
            if (stack.size() == 1)
                root = stack.peek();
            return (stack.pop());
        }
        catch (EmptyStackException e)
        {
            return (null);
        }
    }

        /**
         * Push a new object onto the top of the object stack.
         *
         * @param object The new object
         */
    public void push(Object object)
    {
        stack.push(object);
    }

        // ------------------------------------------------------ Private Methods

        /**
         * Return the set of rules that apply to the specified match position.
         * The selected rules are those that match exactly, or those rules
         * that specify a suffix match and the tail of the rule matches the
         * current match position.  Exact matches have precedence over
         * suffix matches.
         *
         * @param match The current match position
         */
    private List getRules(String match)
    {
        List rulesList = (List) this.rules.get(match);
        if (rulesList == null)
        {
            Iterator keys = this.rules.keySet().iterator();
            while (keys.hasNext())
            {
                String key = (String) keys.next();
                if (key.startsWith("*/"))
                {
                    if (match.endsWith(key.substring(1)))
                    {
                        rulesList = (List) this.rules.get(key);
                        break;
                    }
                }
            }
        }
        return (rulesList);
    }
}
