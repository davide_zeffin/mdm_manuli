/*****************************************************************************
  Copyright (c) 2000, SAPMarkets Europe GmbH, Germany, All rights reserved.
  Created:      30 March 2001

  $Id: //sap/ESALES_base/30_SP_COR/src/_pcatAPI/java/com/sap/isa/catalog/digester/SetTopRule.java#3 $
  $Revision: #3 $
  $Change: 152881 $
  $DateTime: 2003/10/09 09:45:45 $
*****************************************************************************/
package com.sap.isa.catalog.digester;

import java.lang.reflect.Method;

import com.sap.isa.core.logging.IsaLocation;

/**
 * Rule implementation that calls a method on the top (parent)
 * object, passing the (top-1) (child) object as an argument.
 *
 * @version $Revision: #3 $ $Date: 2003/10/09 $
 */
public final class SetTopRule extends Rule
{
    private static IsaLocation theStaticLocToLog = 
        IsaLocation.getInstance(SetTopRule.class.getName());

        // ----------------------------------------------------------- Constructors

        /**
         * Construct a "set parent" rule with the specified method name.  The
         * parent method's argument type is assumed to be the class of the
         * child object.
         *
         * @param digester The associated Digester
         * @param methodName Method name of the parent method to call
         */
    public SetTopRule(Digester digester, String methodName)
    {
        this(digester, methodName, null);
    }

        /**
         * Construct a "set parent" rule with the specified method name.
         *
         * @param digester The associated Digester
         * @param methodName Method name of the parent method to call
         * @param paramType Java class of the parent method's argument
         */
    public SetTopRule(  Digester digester,
                        String methodName,
                        String paramType)
    {
        super(digester);
        this.methodName = methodName;
        this.paramType = paramType;
    }

        // ----------------------------------------------------- Instance Variables

        /**
         * The method name to call on the parent object.
         */
    private String methodName = null;

        /**
         * The Java class name of the parameter type expected by the method.
         */
    private String paramType = null;

        // --------------------------------------------------------- Public Methods

        /**
         * Process the end of this element.
         */
    public void end() throws Exception
    {
            // Identify the objects to be used
        Object child = digester.peek(1);
        Object parent = digester.peek(0);
        if (theStaticLocToLog.isDebugEnabled())
            theStaticLocToLog.debug("SetTopRule: Call "
                         + parent.getClass().getName() + "."
                         + methodName + "(" + child + ")");

            // Call the specified method
        Class paramTypes[] = new Class[1];
        if (paramType != null)
            paramTypes[0] = Class.forName(paramType);
        else
            paramTypes[0] = child.getClass();
        Method method = parent.getClass().getMethod(methodName, paramTypes);
        method.invoke(parent, new Object[] { child });
    }

        /**
         * Clean up after parsing is complete.
         */
    public void finish() throws Exception
    {
    }
}
