/*****************************************************************************
  Copyright (c) 2000, SAPMarkets Europe GmbH, Germany, All rights reserved.
  Created:      30 March 2001

  $Id: //sap/ESALES_base/30_SP_COR/src/_pcatAPI/java/com/sap/isa/catalog/digester/CallMethodRule.java#2 $
  $Revision: #2 $
  $Change: 127411 $
  $DateTime: 2003/04/30 17:20:32 $
*****************************************************************************/
package com.sap.isa.catalog.digester;

import java.lang.reflect.Method;

import org.apache.commons.beanutils.ConvertUtils;
import org.xml.sax.Attributes;

import com.sap.isa.core.logging.IsaLocation;

/**
 * Rule implementation that calls a method on the top (parent)
 * object, passing arguments collected from subsequent
 * <code>CallParamRule</code> rules or from the body of this
 * element.
 *
 * @version $Revision: #2 $ $Date: 2003/04/30 $
 */

public final class CallMethodRule extends Rule
{
        /**
         * Construct a "call method" rule with the specified method name.  The
         * parameter types (if any) default to java.lang.String.
         *
         * @param digester The associated Digester
         * @param methodName Method name of the parent method to call
         * @param paramCount The number of parameters to collect, or
         *  zero for a single argument from the body of this element.
         *  -1 for a function call with zero arguments
         */
    private static IsaLocation theStaticLocToLog = 
        IsaLocation.getInstance(CallMethodRule.class.getName());

    public CallMethodRule(Digester digester, String methodName,
                          int paramCount)
    {
        this(digester, methodName, paramCount, null);
    }

        /**
         * Construct a "call method" rule with the specified method name.
         *
         * @param digester The associated Digester
         * @param methodName Method name of the parent method to call
         * @param paramCount The number of parameters to collect, or
         *  zero for a single argument from the body of ths element,
         *  -1 for a function call with zero arguments.
         * @param paramTypes The Java class names of the arguments
         */
    public CallMethodRule(  Digester digester,
                            String methodName,
                            int paramCount, String paramTypes[])
    {

        super(digester);
        this.methodName = methodName;
        this.paramCount = paramCount;
        if (paramTypes == null)
        {
            if (this.paramCount == -1)
                this.paramTypes = new Class[0];
            else if (this.paramCount == 0)
                this.paramTypes = new Class[1];
            else
                this.paramTypes = new Class[this.paramCount];
            for (int i = 0; i < this.paramTypes.length; i++)
            {
                if (i == 0)
                    this.paramTypes[i] = "abc".getClass();
                else
                    this.paramTypes[i] = this.paramTypes[0];
            }
        }
        else
        {
            this.paramTypes = new Class[paramTypes.length];
            for (int i = 0; i < this.paramTypes.length; i++)
            {
                try
                {
                    this.paramTypes[i] = Class.forName(paramTypes[i]);
                }
                catch (ClassNotFoundException e)
                {
                    this.paramTypes[i] = null;  // Will trigger NPE later
                }
            }
        }
    }

        // ----------------------------------------------------- Instance Variables

        /**
         * The body text collected from this element.
         */
    private String bodyText = null;

        /**
         * The method name to call on the parent object.
         */
    private String methodName = null;

        /**
         * The number of parameters to collect from <code>MethodParam</code> rules.
         * If this value is zero, a single parameter will be collected from the
         * body of this element.
         */
    private int paramCount = 0;

        /**
         * The parameter types of the parameters to be collected.
         */
    private Class paramTypes[] = null;

        // --------------------------------------------------------- Public Methods

        /**
         * Process the start of this element.
         *
         * @param attributes The attribute list for this element
         */
    public void begin(Attributes attributes) throws Exception
    {
            // Push an array to capture the parameter values if necessary
        if (paramCount > 0)
        {
            String parameters[] = new String[paramCount];
            for (int i = 0; i < parameters.length; i++)
                parameters[i] = null;
            digester.push(parameters);
        }
    }

        /**
         * Process the body text of this element.
         *
         * @param bodyText The body text of this element
         */
    public void body(String bodyText) throws Exception
    {
        if (paramCount == 0)
            this.bodyText = bodyText;
    }

        /**
         * Process the end of this element.
         */
    public void end() throws Exception
    {
            // Retrieve or construct the parameter values array
        String parameters[] = null;
        if (paramCount > 0)
            parameters = (String[]) digester.pop();
        else if(paramCount == 0)
        {
            parameters = new String[1];
            parameters[0] = bodyText;
        }
        else
        {
            parameters = new String[0];
        }

            // Construct the parameter values array we will need
        Object paramValues[] = new Object[paramTypes.length];
        for (int i = 0; i < this.paramTypes.length; i++)
            paramValues[i] =
                ConvertUtils.convert(parameters[i], this.paramTypes[i]);

            // Invoke the required method on the top object
        Object top = digester.peek();
        if (theStaticLocToLog.isDebugEnabled())
        {
            StringBuffer sb = new StringBuffer("CallMethodeRule: Call ");
            sb.append(top.getClass().getName());
            sb.append(".");
            sb.append(methodName);
            sb.append("(");
            for (int i = 0; i < paramValues.length; i++)
            {
                if (i > 0)
                    sb.append(",");
                if (paramValues[i] == null)
                    sb.append("null");
                else
                    sb.append(paramValues[i].toString());
            }
            sb.append(")");
            theStaticLocToLog.debug(sb.toString());
        }
        Method method = top.getClass().getMethod(methodName, paramTypes);
        method.invoke(top, paramValues);
    }

        /**
         * Clean up after parsing is complete.
         */
    public void finish() throws Exception
    {
        bodyText = null;
    }
}
