/*****************************************************************************
  Copyright (c) 2000, SAPMarkets Europe GmbH, Germany, All rights reserved.
  Created:      30 March 2001

  $Id: //sap/ESALES_base/30_SP_COR/src/_pcatAPI/java/com/sap/isa/catalog/digester/SetNextRule.java#2 $
  $Revision: #2 $
  $Change: 127411 $
  $DateTime: 2003/04/30 17:20:32 $
*****************************************************************************/
package com.sap.isa.catalog.digester;

import java.lang.reflect.Method;

import com.sap.isa.core.logging.IsaLocation;

/**
 * Rule implementation that calls a method on the (top-1) (parent)
 * object, passing the top object (child) as an argument.  It is
 * commonly used to establish parent-child relationships.
 *
 * @author Craig McClanahan
 * @version $Revision: #2 $ $Date: 2003/04/30 $
 */

public final class SetNextRule extends Rule
{
    private static IsaLocation theStaticLocToLog = 
        IsaLocation.getInstance(SetNextRule.class.getName());

        // ----------------------------------------------------------- Constructors

        /**
         * Construct a "set next" rule with the specified method name.  The
         * method's argument type is assumed to be the class of the
         * child object.
         *
         * @param digester The associated Digester
         * @param methodName Method name of the parent method to call
         */
    public SetNextRule(Digester digester, String methodName)
    {
        this(digester, methodName, null);
    }

        /**
         * Construct a "set next" rule with the specified method name.
         *
         * @param digester The associated Digester
         * @param methodName Method name of the parent method to call
         * @param paramType Java class of the parent method's argument
         */
    public SetNextRule( Digester digester,
                        String methodName,
                        String paramType)
    {
        super(digester);
        this.methodName = methodName;
        this.paramType = paramType;
    }

        // ----------------------------------------------------- Instance Variables

        /**
         * The method name to call on the parent object.
         */
    private String methodName = null;

        /**
         * The Java class name of the parameter type expected by the method.
         */
    private String paramType = null;

        // --------------------------------------------------------- Public Methods

        /**
         * Process the end of this element.
         */
    public void end() throws Exception
    {
            // Identify the objects to be used
        Object child = digester.peek(0);
        Object parent = digester.peek(1);
        if (theStaticLocToLog.isDebugEnabled())
            theStaticLocToLog.debug("SetNextRule: Call "
                         + parent.getClass().getName()
                         + "."
                         + methodName
                         + "(" + child + ")");

            // Call the specified method
        Class paramTypes[] = new Class[1];
        if (paramType != null)
            paramTypes[0] = Class.forName(paramType);
        else
            paramTypes[0] = child.getClass();
        Method method = parent.getClass().getMethod(methodName, paramTypes);
        method.invoke(parent, new Object[] { child });
    }

        /**
         * Clean up after parsing is complete.
         */
    public void finish() throws Exception
    {
    }
}
