/*****************************************************************************
  Copyright (c) 2000, SAPMarkets Europe GmbH, Germany, All rights reserved.
  Created:      30 March 2001

  $Id: //sap/ESALES_base/30_SP_COR/src/_pcatAPI/java/com/sap/isa/catalog/digester/SetPropertyRule.java#2 $
  $Revision: #2 $
  $Change: 127411 $
  $DateTime: 2003/04/30 17:20:32 $
*****************************************************************************/
package com.sap.isa.catalog.digester;

import java.util.HashMap;

import org.apache.commons.beanutils.BeanUtils;
import org.xml.sax.Attributes;

import com.sap.isa.core.logging.IsaLocation;

/**
 * Rule implementation that sets an individual property on the object at the
 * top of the stack, based on attributes with specified names.
 *
 * @author Craig McClanahan
 * @version $Revision: #2 $ $Date: 2003/04/30 $
 */

public final class SetPropertyRule extends Rule
{
    private static IsaLocation theStaticLocToLog = 
        IsaLocation.getInstance(SetPropertyRule.class.getName());

        // ----------------------------------------------------------- Constructors

        /**
         * Construct a "set property" rule with the specified name and value
         * attributes.
         *
         * @param digester The digester with which this rule is associated
         * @param name Name of the attribute that will contain the name of the
         *  property to be set
         * @param value Name of the attribute that will contain the value to which
         *  the property should be set
         */
    public SetPropertyRule(Digester digester, String name, String value)
    {
        super(digester);
        this.name = name;
        this.value = value;
    }

        // ----------------------------------------------------- Instance Variables

        /**
         * The attribute that will contain the property name.
         */
    private String name = null;

        /**
         * The attribute that will contain the property value.
         */
    private String value = null;

        // --------------------------------------------------------- Public Methods

        /**
         * Process the beginning of this element.
         *
         * @param context The associated context
         * @param attributes The attribute list of this element
         */
    public void begin(Attributes attributes) throws Exception
    {
            // Identify the actual property name and value to be used
        String actualName = null;
        String actualValue = null;
        HashMap values = new HashMap();
        for (int i = 0; i < attributes.getLength(); i++)
        {
            String name = attributes.getQName(i);
            String value = attributes.getValue(i);
            if (name.equals(this.name))
                actualName = value;
            else if (name.equals(this.value))
                actualValue = value;
        }
        values.put(actualName, actualValue);

            // Populate the corresponding property of the top object
        Object top = digester.peek();
        if (theStaticLocToLog.isDebugEnabled())
            theStaticLocToLog.debug("SetPropertyRule: Set "
                         + top.getClass().getName()
                         + " property "
                         + actualName
                         + " to "
                         + actualValue);
        BeanUtils.populate(top, values);
    }
}
