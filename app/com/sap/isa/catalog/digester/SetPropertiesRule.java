/*****************************************************************************
  Copyright (c) 2000, SAPMarkets Europe GmbH, Germany, All rights reserved.
  Created:      30 March 2001

  $Id: //sap/ESALES_base/30_SP_COR/src/_pcatAPI/java/com/sap/isa/catalog/digester/SetPropertiesRule.java#2 $
  $Revision: #2 $
  $Change: 127411 $
  $DateTime: 2003/04/30 17:20:32 $
*****************************************************************************/
package com.sap.isa.catalog.digester;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Set;

import org.apache.commons.beanutils.BeanUtils;
import org.xml.sax.Attributes;

import com.sap.isa.core.logging.IsaLocation;

/**
 * Rule implementation that sets properties on the object at the top of the
 * stack, based on attributes with corresponding names.
 *
 * @author Craig McClanahan
 * @version $Revision: #2 $ $Date: 2003/04/30 $
 */

public final class SetPropertiesRule extends Rule
{
    private static IsaLocation theStaticLocToLog = 
        IsaLocation.getInstance(SetPropertiesRule.class.getName());

        // ----------------------------------------------------------- Constructors

        /**
         * Default constructor sets only the the associated Digester.
         *
         * @param digester The digester with which this rule is associated
         */
    public SetPropertiesRule(Digester digester)
    {
        super(digester);
    }

        // --------------------------------------------------------- Public Methods

        /**
         * Process the beginning of this element.
         *
         * @param context The associated context
         * @param attributes The attribute list of this element
         */
    public void begin(Attributes attributes) throws Exception
    {
            // Build a set of attribute names and corresponding values
        HashMap values = new HashMap();
        for (int i = 0; i < attributes.getLength(); i++)
        {
            String name = attributes.getQName(i);
            String value = attributes.getValue(i);
            values.put(name, value);
        }

            // Populate the corresponding properties of the top object
        Object top = digester.peek();
        if (theStaticLocToLog.isDebugEnabled())
        {
            StringBuffer aBuffer =new StringBuffer("properties! ");
            Set theKeys = values.keySet();
            Iterator aSetIter = theKeys.iterator();
            while (aSetIter.hasNext()) 
            {
                String aKey = (String)aSetIter.next();
                aBuffer.append(" Key:"+aKey);
                String aValue = (String)values.get(aKey);
                aBuffer.append(" Value:"+aValue);
            } // end of while ()
            theStaticLocToLog.debug("SetPropertiesRule:  Set " +
                         top.getClass().getName() +
                         aBuffer.toString());

                         
        }
        BeanUtils.populate(top, values);
    }
}
