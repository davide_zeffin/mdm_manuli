/*****************************************************************************
  Copyright (c) 2000, SAPMarkets Europe GmbH, Germany, All rights reserved.
  Created:      30 March 2001

  $Id: //sap/ESALES_base/30_SP_COR/src/_pcatAPI/java/com/sap/isa/catalog/digester/ObjectCreateRule.java#2 $
  $Revision: #2 $
  $Change: 127411 $
  $DateTime: 2003/04/30 17:20:32 $
 *****************************************************************************/
package com.sap.isa.catalog.digester;

import org.xml.sax.Attributes;

import com.sap.isa.core.logging.IsaLocation;

/**
 * Rule implementation that creates a new object and pushes it
 * onto the object stack.  When the element is complete, the
 * object will be popped
 *
 * @author Craig McClanahan
 * @version $Revision: #2 $ $Date: 2003/04/30 $
 */

public final class ObjectCreateRule extends Rule
{
    private static IsaLocation theStaticLocToLog = 
        IsaLocation.getInstance(ObjectCreateRule.class.getName());

        // ----------------------------------------------------------- Constructors

        /**
         * Construct an object create rule with the specified class name.
         *
         * @param digester The associated Digester
         * @param className Java class name of the object to be created
         */
    public ObjectCreateRule(Digester digester, String className)
    {
        this(digester, className, null);
    }

        /**
         * Construct an object create rule with the specified class name and an
         * optional attribute name containing an override.
         *
         * @param digester The associated Digester
         * @param className Java class name of the object to be created
         * @param attributeName Attribute name which, if present, contains an
         *  override of the class name to create
         */
    public ObjectCreateRule(Digester digester,
                            String className,
                            String attributeName)
    {
        super(digester);
        this.className = className;
        this.attributeName = attributeName;
    }

        // ----------------------------------------------------- Instance Variables

        /**
         * The attribute containing an override class name if it is present.
         */
    private String attributeName = null;

        /**
         * The Java class name of the object to be created.
         */
    private String className = null;

        // --------------------------------------------------------- Public Methods

        /**
         * Process the beginning of this element.
         *
         * @param attributes The attribute list of this element
         */
    public void begin(Attributes attributes) throws Exception
    {
            // Identify the name of the class to instantiate
        String realClassName = className;
        if (attributeName != null)
        {
            String value = attributes.getValue(attributeName);
            if (value != null)
                realClassName = value;
        }
	// Instantiate the new object and push it on the context stack
        Class clazz = Class.forName(realClassName);
        Object instance = clazz.newInstance();
        if (theStaticLocToLog.isDebugEnabled())
            theStaticLocToLog.debug("ObjectCreateRule: going to push: New " + realClassName);        
        digester.push(instance);
    }

        /**
         * Process the end of this element.
         */
    public void end() throws Exception
    {
        Object top = digester.pop();
        if (theStaticLocToLog.isDebugEnabled())
            theStaticLocToLog.debug("ObjectCreateRule: Pop " + top.getClass().getName());
    }


        /**
         * Clean up after parsing is complete.
         */
    public void finish() throws Exception
    {
            //attributeName = null;
            //className = null;
    }
}
