/*****************************************************************************
  Copyright (c) 2000, SAPMarkets Europe GmbH, Germany, All rights reserved.
  Created:      30 March 2001

  $Id: //sap/ESALES_base/30_SP_COR/src/_pcatAPI/java/com/sap/isa/catalog/digester/Rule.java#2 $
  $Revision: #2 $
  $Change: 129221 $
  $DateTime: 2003/05/15 17:20:58 $
*****************************************************************************/
package com.sap.isa.catalog.digester;

import org.xml.sax.Attributes;

/**
 * Concrete implementations of this class implement actions to be taken when
 * a corresponding nested pattern of XML elements has been matched.
 *
 * @author Craig McClanahan
 * @version $Revision: #2 $ $Date: 2003/05/15 $
 */

public abstract class Rule
{

        // ----------------------------------------------------------- Constructors

        /**
         * Default constructor sets only the the associated Digester.
         *
         * @param digester The digester with which this rule is associated
         */
    public Rule(Digester digester)
    {
        super();
        this.digester = digester;
    }

        // ----------------------------------------------------- Instance Variables

        /**
         * The Digester with which this Rule is associated.
         */
    protected Digester digester = null;

        // --------------------------------------------------------- Public Methods

        /**
         * This method is called when the beginning of a matching XML element
         * is encountered.
         *
         * @param attributes The attribute list of this element
         */
    public void begin(Attributes attributes) throws Exception
    {
        ;   // The default implementation does nothing
    }

        /**
         * This method is called when the body of a matching XML element
         * is encountered.  If the element has no body, this method is
         * not called at all.
         *
         * @param text The text of the body of this element
         */
    public void body(String text) throws Exception
    {
        ;   // The default implementation does nothing
    }

        /**
         * This method is called when the end of a matching XML element
         * is encountered.
         */
    public void end() throws Exception
    {
        ;   // The default implementation does nothing
    }

        /**
         * This method is called after all parsing methods have been
         * called, to allow Rules to remove temporary data.
         */
    public void finish() throws Exception
    {
        ;   // The default implementation does nothing
    }
}
