package com.sap.isa.catalog.ccms;

import java.util.Locale;

import com.sap.isa.backend.boi.webcatalog.CatalogBusinessObjectsAware;
import com.sap.isa.backend.boi.webcatalog.CatalogConfiguration;
import com.sap.isa.backend.boi.webcatalog.CatalogUser;
import com.sap.isa.businessobject.webcatalog.CatalogBusinessObjectManager;
import com.sap.isa.catalog.boi.ICatalog;
import com.sap.isa.catalog.impl.CatalogClient;
import com.sap.isa.catalog.webcatalog.CatalogServer;
import com.sap.isa.catalog.webcatalog.WebCatInfo;
import com.sap.isa.core.TechKey;
import com.sap.isa.core.init.InitializationEnvironment;
import com.sap.isa.core.test.BaseComponentTest;

/**
 * This class is used to test the availibility of the
 * CRM catalog in CCMS Heartbeat tests.
 */
public class IMSTest extends BaseComponentTest {
	InitializationEnvironment initializationEnvironment=null;
    CatalogBusinessObjectsAware catObjBom;
    CatalogBusinessObjectManager catalogBom;
    Locale locale = null;
    String shopScenario;
	static final public String TEST_NAME="Catalog Engine Test";
	
	private String shopName=null;

	public IMSTest(
	InitializationEnvironment initializationEnvironment,
    CatalogBusinessObjectsAware catObjBom,
    CatalogBusinessObjectManager catalogBom
	)
	{
		this.initializationEnvironment=initializationEnvironment;
		this.catObjBom=catObjBom;
		this.catalogBom=catalogBom;
		BaseComponentTest.TestDescriptor imsPing = new BaseComponentTest.TestDescriptor(TEST_NAME);
		imsPing.setTestDescription("Test connection to the Catalog");
		addTestDescriptor(imsPing);
	}
	
	public void setShopName(String shopName)
	{
		this.shopName = shopName;
	}
	public void setInitializationEnvironment(InitializationEnvironment initializationEnvironment)
	{
		this.initializationEnvironment = initializationEnvironment;
	}

	/**
	 * @see com.sap.isa.core.test.ComponentTest#performTest(String)
	 */
	public boolean performTest(String testName) {
		if( null == initializationEnvironment || 
			null ==catObjBom ||
			null == catalogBom )
		{
			this.setTestResultDescription(TEST_NAME, "The Test was not executed. It's not initialized");
			return false;
		}
		if( locale == null )
		{
			this.setTestResultDescription(TEST_NAME, "Unable to execute IMSTest: language not known" );
			return false;
		}
		if( shopName == null )
		{
			this.setTestResultDescription(TEST_NAME,"No shop defined for test");
	   		return false;
		}
		else
			return testSingleShop( shopName );
	}
	/**
	 * tests the catalog for the given shop
	 */
	public boolean testSingleShop(String shopId) {
		if( shopId == null ) {
			this.setTestResultDescription(TEST_NAME, "Unable to execute IMSTest: shop id not known" );
			return false;
		}
        
	    TechKey shopKey = new TechKey(shopId);
        CatalogConfiguration config;
      // Ask the bom to create a shop with the given key
		try {
			CatalogUser user= catObjBom.createCatalogUser();
			user.setLanguage(locale.getLanguage());
			catObjBom.releaseCatalogConfiguration();
            config= catObjBom.createCatalogConfiguration(shopKey);
		}
		catch ( Exception ex )
		{
			this.setTestResultDescription(TEST_NAME, ex.getMessage());
			log.debug("Test failed ", ex);
			return false;
		}
		String catalog= config.getCatalog();
		if( catalog == null )
		{
			this.setTestResultDescription(TEST_NAME, "Shop " + shopId + " not found" );
			return false;
			
		}		
	    CatalogClient aCatalogClient = new CatalogClient(locale, null, null, config.getViews());
	    CatalogServer aCatalogServer = new CatalogServer(null,catalog);
		WebCatInfo theCatalog = catalogBom.createCatalog(aCatalogClient, aCatalogServer, initializationEnvironment);
		if( theCatalog == null )
		{
			this.setTestResultDescription(TEST_NAME, "Unable to get the Catalog" );
			return false;
		}
		ICatalog cat=theCatalog.getCatalog();
		if( cat == null )
		{
			this.setTestResultDescription(TEST_NAME, "Unable to get the Catalog" );
			return false;
		}
		if( log.isDebugEnabled())
			log.debug("calling getCatalogStatus of " + cat.getClass().getName());
		boolean rc = ICatalog.CATALOG_STATUS_OK == cat.getCatalogStatus();
		if( rc )
			this.setTestResultDescription(TEST_NAME, "The Catalog engine is available" );
		else
			this.setTestResultDescription(TEST_NAME, "The Catalog engine is not available" );
		return rc;
	}

	/**
	 * Sets the catObjBom.
	 * @param catObjBom The catObjBom to set
	 */
	public void setCatObjBom(CatalogBusinessObjectsAware  catObjBom) {
		this.catObjBom = catObjBom;
	}

	/**
	 * Sets the catalogBom.
	 * @param catalogBom The catalogBom to set
	 */
	public void setCatalogBom(CatalogBusinessObjectManager catalogBom) {
		this.catalogBom = catalogBom;
	}

	/**
	 * Sets the locale.
	 * @param locale The locale to set
	 */
	public void setLocale(Locale locale) {
		this.locale = locale;
	}

	/**
	 * Sets the shopScenario.
	 * @param shopScenario The shopScenario to set
	 */
	public void setShopScenario(String shopScenario) {
		this.shopScenario = shopScenario;
	}

}
