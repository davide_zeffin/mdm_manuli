package com.sap.isa.catalog;

import com.sap.isa.core.logging.IsaLocation;

public class CatalogParameters
{
  /**
   * Default value for the upper limit for <b>ims</b> package
   *  (1) items per page
   *  (2) total categories in catalog
   * Default value is 10000.
   * Can be set using System Property ims.upperlimit
   * Example: java -Dims.upperlimit=20000 ...
   */
  public static int imsUpperLimit = 10000;

  /**
   * Default value for the upper limit for <b>trex</b> package
   *  (1) items per page
   *  (2) total categories in catalog
   * Default value is 10000.
   * Can be set using System Property trex.upperlimit
   * Example: java -Dtrex.upperlimit=20000 ...
   */
  public static int trexUpperLimit = 10000; // default

  /**
   * Default value used to restrict the maximum number of query results during a catalog search
   * This is configurable via XCM
   * Default value is 10000
   */
  public static int maxSearchResults = 10000; // default
  
  /** 
	*  The IsaLocation of the current Action.
	*  This will be instantiated during perform and is always present.
	*/
   protected static IsaLocation log = IsaLocation.getInstance(CatalogParameters.class.getName());
  
  
  // try to initialize using system properties
  static
  {
		try 
		{ 
			imsUpperLimit = Integer.parseInt(System.getProperty("ims.upperlimit"));
		}
		catch (Exception e) {
			log.debug(e.getMessage());
		} 
		
		try 
		{ 
			trexUpperLimit = Integer.parseInt(System.getProperty("trex.upperlimit"));
		}
		catch (Exception e) {
			log.debug(e.getMessage());
		}
  }
}