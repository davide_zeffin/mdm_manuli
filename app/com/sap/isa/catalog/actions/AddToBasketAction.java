package com.sap.isa.catalog.actions;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.sap.isa.backend.boi.webcatalog.CatalogConfiguration;
import com.sap.isa.businessobject.CommunicationException;
import com.sap.isa.businessobject.item.BasketTransferItem;
import com.sap.isa.businessobject.marketing.CUAList;
import com.sap.isa.businessobject.webcatalog.ItemTransferRequest;
import com.sap.isa.catalog.webcatalog.OCIInfo;
import com.sap.isa.catalog.webcatalog.WebCatInfo;
import com.sap.isa.catalog.webcatalog.WebCatItem;
import com.sap.isa.catalog.webcatalog.WebCatItemList;
import com.sap.isa.core.UserSessionData;
import com.sap.isa.isacore.action.marketing.ShowCUAAction;
import com.sap.isa.core.util.Message;
import com.sap.isa.core.util.MessageList;

/**
 * Title:        AddToBasketAction
 * Description:  Action to add items to a basket using open catalog interface (OCI)
 *               Needs "cuurentItemList" to be set.
 * Copyright:    Copyright (c) 2001
 * Company:      SAPMarkets Europe GmbH
 * @version 1.0
 */

public class AddToBasketAction extends CatalogBaseAction {

	public BasketTransferItem customerExitCreateBasketTransferItem(WebCatItem item) {
		return new ItemTransferRequest(item);
	}

	public ActionForward doPerform(
		ActionMapping mapping,
		ActionForm form,
		HttpServletRequest request,
		HttpServletResponse response,
		UserSessionData userSessionData,
		WebCatInfo theCatalog)
		throws IOException, ServletException, CommunicationException {

		WebCatItemList items = theCatalog.getCurrentItemList();
		WebCatItemList itemsCUA = theCatalog.getCurrentCUAList();

		// this is for basket transfer in EBP
		WebCatItemList validItems = new WebCatItemList(theCatalog);
		// this is for basket transfer in ISA
		ArrayList basketItems = new ArrayList();

		String itemKey = request.getParameter(ActionConstants.RA_ITEMKEY);
		boolean addOneItem = true;
		WebCatItem item = null;

		if (null == itemKey
			&& theCatalog != null
			&& theCatalog.getCurrentItem() != null)
			itemKey = theCatalog.getCurrentItem().getItemID();

		//int itemKeyInt = -1;
		if (itemKey != null && itemKey.length() != 0) {
			if ( (request.getParameter(ActionConstants.RA_COMPARE_CUA) != null && request.getParameter(ActionConstants.RA_COMPARE_CUA).equals("true"))
					 || (theCatalog.getLastVisited() != null && theCatalog.getLastVisited().equals(ActionConstants.DS_CUA)) ){
				if (itemsCUA != null && itemsCUA.getItem(itemKey) != null)
					item = itemsCUA.getItem(itemKey).getContractItem(
							request.getParameter(ActionConstants.RA_CONTRACTKEY), request.getParameter(
								ActionConstants.RA_CONTRACTITEMKEY));
			}
			if (item == null) {
				if (items == null || items.getItem(itemKey) == null) {
					//if the item is through the current Item List
					if (itemsCUA == null || itemsCUA.getItem(itemKey) == null) {
						// if the item is through the current CUA Item List
						if (request.getParameter(ActionConstants.RA_AREAKEY)== null)
							//if the item cannot be determined using its area
							item = theCatalog.getCurrentItem();
						//try with the current item
						else { 
                            if(theCatalog.getCurrentItem() != null && theCatalog.getCurrentItem().getItemId().equals(itemKey)) { //check if the itemKey of the current item is the same as the itemKey
                                item = theCatalog.getCurrentItem();
                            }
                            else { //the item cand be det. using area in itemID                         
							   item = theCatalog.getItem( (String) request.getParameter(ActionConstants.RA_AREAKEY),
									(String) request.getParameter(ActionConstants.RA_ITEMKEY));
							   item.setQuantity((String) request.getParameter("item[1].quantity"));
                            }
						} //end if 3
					} else { //item is through the CUA list
                            item = itemsCUA.getItem(itemKey).getContractItem(request.getParameter(ActionConstants.RA_CONTRACTKEY),
                                   request.getParameter(ActionConstants.RA_CONTRACTITEMKEY));
					}
				} else { // item is in the current item list
                     item = theCatalog.getCurrentItem();
                     if(item == null || !item.getItemId().equals(itemKey)) {
                         item = items.getItem(itemKey).getContractItem(request.getParameter(ActionConstants.RA_CONTRACTKEY),
							request.getParameter(ActionConstants.RA_CONTRACTITEMKEY));
                     }
				} //end ifs
			}
			if (item != null && item.getSolutionConfigurator() != null) {
                //if this action is used for add To Leaflet then we only add the main product, not the package components
                if (request.getParameter("target") != null && request.getParameter("target").equals("leaflet")) {
                    addOneItem = true;
                }
                else {
                    addOneItem = false;
                }
			}
		} //end transferring only the item having itemKey known
		else { // transfer all selected items
			addOneItem = false;
		}

		if (addOneItem == true) {
			if (item != null) {
					log.debug("Only one item to be added " + item.getDescription());
				validItems.addItem(item);
				basketItems.add(customerExitCreateBasketTransferItem(item));
				item.setSelected(false);
			}
		} else {
            log.debug("add more than one items in the shopping basket");
            WebCatItem itemForBasket = null;
            // item is a package
            if (item != null) {
                itemForBasket = (WebCatItem)item.clone();
                item.changeTechKey(null);
            }
            
			Iterator itemIterator = null;
			if (itemForBasket != null) {
				//all items should be inserted -- the order will checked if the item is selected or not
				if (itemForBasket.getWebCatSubItemList() != null) {
					itemIterator = itemForBasket.getWebCatSubItemList().iterator();
				}
				//add the main product
				validItems.addItem(itemForBasket);
				basketItems.add(customerExitCreateBasketTransferItem(itemForBasket));
			} else {
				if (request.getParameter(ActionConstants.RA_COMPARE_CUA) != null && request.getParameter(ActionConstants.RA_COMPARE_CUA).equals("true")) {
					itemIterator = itemsCUA.iteratorOnlyPopulated();
				} else if( (request.getParameter(ActionConstants.RA_TRANSFER_CUA) != null && request.getParameter(ActionConstants.RA_TRANSFER_CUA).equals("true"))
						|| (theCatalog.getLastVisited() != null && theCatalog.getLastVisited().equals(ActionConstants.DS_CUA))) {
					itemIterator = itemsCUA.iteratorOnlySelected();
				} else {
					itemIterator = items.iteratorOnlySelected();
				}
			}
			if (itemIterator != null) {
				while (itemIterator.hasNext()) {
                    itemForBasket = (WebCatItem) itemIterator.next();
                    //case b2b: transfer selection || case b2c telco -- add only subitem with contract duration
                    if( itemForBasket.isSelected() && (itemForBasket.getSolutionConfigurator()==null) || (itemForBasket.getSelectedContractDuration()!= null && itemForBasket.isScSelected()) ) {
                        validItems.addItem(itemForBasket);
                        basketItems.add(customerExitCreateBasketTransferItem(itemForBasket));
                    }
                    itemForBasket.setSelected(false);
					log.debug("Item is added " + itemForBasket.getDescription());
					Collection subItems = itemForBasket.getContractItems();

					if (subItems != null && subItems.size() != 0) {
						Iterator subItemsIterator = subItems.iterator();
						while (subItemsIterator.hasNext()) {
							WebCatItem subItem = (WebCatItem) subItemsIterator.next();
							if (subItem.isSelected()) {
								validItems.addItem(subItem);
								basketItems.add(customerExitCreateBasketTransferItem(subItem));
								subItem.setSelected(false);
								if (log.isDebugEnabled())
									log.debug("Sub item is added "+ subItem.getDescription()+ " "
											+ subItem.getContractRef().getContractId()
											+ " "+ subItem.getContractRef().getItemID());
							} //end if item is selected
						} //end while loop over sub-items
					} //end if item has sub-items
				} //end while loop over items
			}
		} //end else ... for transfer all selected items

		request.setAttribute(ActionConstants.RA_ITEMLIST_ISA, basketItems);

		ActionForward returnData = null;
        log.debug("There are: " + validItems.size() + " items selected");
		if (validItems.size() < 1) {
			if (request.getParameter("ignoreEmptyListAlert") != null) {
			    returnData = mapping.findForward("empty_list_no_alert");
            }
			else {
				MessageList errorMessages = new MessageList();
				Message mess = new Message(Message.ERROR, "catalog.isa.selectSomethingForTransfer");
				errorMessages.add(mess);
				request.setAttribute(ActionConstants.RC_MESSAGES, errorMessages);
				returnData = mapping.findForward("failure");
            }
		}
		if (returnData != null) {
			return returnData;
        }

		//request.setAttribute(ActionConstants.RA_ITEMLIST_EBP, validItems);

		//    request.setAttribute(ActionConstants.RA_FORWARD, "catalog");
		request.setAttribute(ActionConstants.RA_FORWARD, "backToCatalog");

		if (theCatalog.getLastVisited().equals(WebCatInfo.COMPARE)) {
			log.debug("We add to basket, from some compare screen");
			if (items != null) { //if we have current list of items
				if (items.getArea() != null) {
					//we get the list of items from an area
					theCatalog.setLastVisited(WebCatInfo.ITEMLIST);
                }
				else {//we did not get the items as result of an area 
					if (items.getQuery() != null){
						//we got the list ofitems as result of a query 
						if (items.getQuery().getParent() != null) {
							//we got the list of items from category query 
							theCatalog.setLastVisited(WebCatInfo.CATEGORY_QUERY);
                        }
						else {
							//we got the list of items from catalog query
							theCatalog.setLastVisited(WebCatInfo.CATALOG_QUERY);
                        }
                    } else if(request.getParameter(ActionConstants.RA_DETAILSCENARIO) != null) {
                    	// we are coming from recommendations of bestsellers
                    	theCatalog.setLastVisited(request.getParameter(ActionConstants.RA_DETAILSCENARIO));
                    }
					else {
						//?? not result of area, not result of query. Keep the old functionality
						theCatalog.setLastVisited(WebCatInfo.ITEMLIST);
                    }
                }
            }
			else {
				//we have no current items; keeping the old functionality
				theCatalog.setLastVisited(WebCatInfo.ITEMLIST);
            }
			log.debug("When leaving we have this lastvisited:"+ theCatalog.getLastVisited());
		}

        /* 
         * Handle standalone catalog.
         * If we are in the standalone catalog, the item is stored as special attribute in the
         * request and a special forward is set. The forward normally refreshs the current page.
         * The page is responsible to generate an inline script on the page, that calls the
         * CatalogScripts.transferToOrder() method. Itemlists are not supported for this scenario.
         */
        CatalogConfiguration cc = this.getCatalogConfiguration(userSessionData);
        if (cc.isStandaloneCatalog()) {
            request.setAttribute(ActionConstants.RA_TRANSFERTOORDERITEM, item);
            
            // necessary for select product from the details page, otherwise the bean on 
            // ProductDetails.js.inc.jsp will be null
            if (theCatalog.getCurrentItem() != null) {
                request.setAttribute(ActionConstants.RC_WEBCATITEM, theCatalog.getCurrentItem());
                
                // set CUA list if currentItem is identical to leading product of the cua list
                CUAList cuaList = (CUAList) theCatalog.getCuaList();
                if (theCatalog.getCurrentCUAList() != null &&
                    cuaList != null &&
                    cuaList.getProductId() != null &&
                    cuaList.getProductId().equals(theCatalog.getCurrentItem().getProduct())) {
                    request.setAttribute(ShowCUAAction.RC_CUALIST, cuaList);
                    request.setAttribute(ShowCUAAction.RC_CUATYPE, theCatalog.getCuaType());
                }
            }
            return (mapping.findForward(ActionConstants.FW_TRANSFERTOORDERITEM));
        }
        
		OCIInfo ociInfo = theCatalog.getOciInfo();
		if (ociInfo != null)
			request.setAttribute(ActionConstants.RA_OCIINFO, ociInfo);

		if (request.getParameter("target") != null && request.getParameter("target").equals("leaflet"))
			return (mapping.findForward("leaflet"));
		else
			return (mapping.findForward(ActionConstants.FW_SUCCESS));
	}

}