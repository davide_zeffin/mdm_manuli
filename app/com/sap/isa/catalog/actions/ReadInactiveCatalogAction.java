package com.sap.isa.catalog.actions;

import java.io.IOException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.sap.isa.backend.boi.webcatalog.CatalogBusinessObjectsAware;
import com.sap.isa.backend.boi.webcatalog.CatalogConfiguration;
import com.sap.isa.businessobject.CommunicationException;
import com.sap.isa.businessobject.webcatalog.CatalogBusinessObjectManager;
import com.sap.isa.businesspartner.backend.boi.PartnerFunctionData;
import com.sap.isa.businesspartner.businessobject.BusinessPartner;
import com.sap.isa.businesspartner.businessobject.BusinessPartnerManager;
import com.sap.isa.catalog.trexcrm.TrexCRMCatalog;
import com.sap.isa.catalog.webcatalog.WebCatInfo;
import com.sap.isa.core.TechKey;
import com.sap.isa.core.UserSessionData;
import com.sap.isa.core.eai.BackendException;
import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.core.util.Message;
import com.sap.isa.isacore.MessageDisplayer;

/**
 * This action will read a certain inactive staging catalogue after it 
 * has been selected from the list of available inactive staging catalogues. 
 * To do this the new readStagingCatalog() method of the WebCatInfo
 * object will be called.
 */
public class ReadInactiveCatalogAction extends ISAEntryAction {

	private static IsaLocation log =
		IsaLocation.getInstance(ReadInactiveCatalogAction.class.getName());

	public static final String PARAM_CATALOG = "catalogKey";

	public ActionForward doPerform(
		ActionMapping mapping,
		ActionForm form,
		HttpServletRequest request,
		HttpServletResponse response,
		UserSessionData userData,
		WebCatInfo theCatalog)
		throws IOException, ServletException, CommunicationException {

		log.entering("ReadInactiveCatalogAction_doPerform");

		String forward = "success";
		String catalogGuid = null;
        
        Date theDate = null;
        CatalogBusinessObjectsAware catObjBom = getCatalogObjectAwareBom(userData);
		CatalogConfiguration config = null;
		if (catObjBom != null) {
			config = catObjBom.getCatalogConfiguration();
		}
		
        if (showCatalogueStagingIPCDate(catObjBom)) {
            
			String startDate = request.getParameter("startDate");
            
			if (startDate != null && !startDate.equals("")) {
				// check date for correctness
				String format = config.getDateFormat();
				format = format.replace('Y', 'y');
				format = format.replace('D', 'd');
				DateFormat dateFormat = new SimpleDateFormat(format);
				dateFormat.setLenient(false); // do not accept dates like 34.03.2001

				try {
					theDate = dateFormat.parse(startDate);
            
					if (log.isDebugEnabled()) {
						log.debug("IPC Pricing Date determined as: " + theDate.toString());
					}
					if (config != null) {
						log.debug("Set shop ipc pricing date");
						config.setCatalogStagingIPCDate(dateFormat.format(theDate));
					}
					if (theCatalog.getPriceCalculator() != null) {
						log.debug("Set price calculator ipc pricing date");
						theCatalog.getPriceCalculator().setIpcDate(theDate);
					}
				}
				catch (ParseException e) {
					if (log.isDebugEnabled()) {
						log.debug("Exception when parsing Date " + startDate);
					}
				}
			} 
        }
        
        // Date could not be parsed
        if (theDate == null && showCatalogueStagingIPCDate(catObjBom)) {
            MessageDisplayer messageDisplayer =
            createMessage("stagingCatalog.wrongDate", request);
            forward = "message";
        }
        else {
            String inactiveCatTechKey = request.getParameter("techkey");
            if (inactiveCatTechKey != null && !inactiveCatTechKey.equals("")) {
                try {
                    theCatalog.readInactiveCatalog(new TechKey(inactiveCatTechKey));
                    
					if (!theCatalog.isValid()) {
						log.error("system.initFailed", new String[] { "catalog" }, new Exception("Error creating inactive staging catalog"));

						generateErrorMessage(request);

						return mapping.findForward("message");
					}
					else {
						log.debug("Staging catalog sucessfully read");
					}

					CatalogBusinessObjectManager bom = (CatalogBusinessObjectManager) userData.getBOM(CatalogBusinessObjectManager.CATALOG_BOM);

					BusinessPartnerManager bupaManager = catObjBom.createBUPAManager();
					BusinessPartner soldTo = bupaManager.getDefaultBusinessPartner(PartnerFunctionData.SOLDTO);

					// now take care about the still missing objecst and initialisations
					createPriceCalculator(userData, request, theCatalog, bom, catObjBom, soldTo, theDate);
					
//					// catalog determination and inactive versions? set Attribute 
				    if ( config.isCatalogDeterminationActived() == true ) {
					    if (theCatalog.getCatalog() instanceof TrexCRMCatalog) {
						    catalogGuid = theCatalog.getCatalog().getGuid().trim();
						    request.setAttribute(PARAM_CATALOG, catalogGuid);
					    }
				    }
                } catch (BackendException e) {
                    log.error("BackendException: " + e.getMessage());
					generateErrorMessage(request);
					forward = "message";
                }
            } 
            else {
                MessageDisplayer messageDisplayer =
                createMessage("stagingCatalog.notFound", request);
                forward = "message";
            }
        }

		log.exiting();

		return mapping.findForward(forward);
	}
	
	private MessageDisplayer createMessage(
		String message,
		HttpServletRequest request) {
		log.entering("createStagingMessage");
		MessageDisplayer messageDisplayer = new MessageDisplayer();
        messageDisplayer.setAction("/catalog/readInactiveCatalog.do");
		messageDisplayer.addToRequest(request);
		messageDisplayer.addMessage(
			new Message(Message.ERROR, message, null, null));

		log.debug(message);
		log.exiting();

		return messageDisplayer;
	}
	
	/**
	  * Returns check if IPCStagingDate should be shown
	  * Also see equivalent method in CatalogStageListUI.java
	  * 
	  * @return true if so, false else
	  */
	public boolean  showCatalogueStagingIPCDate(CatalogBusinessObjectsAware catObjBom) {
		boolean retVal = false;
		        
		if (catObjBom != null && catObjBom.getCatalogConfiguration() != null) {
			if (catObjBom.getCatalogConfiguration().isIPCPriceUsed() || !catObjBom.getCatalogConfiguration().isListPriceUsed()) {
				retVal = true;
			}
		}   
        
		return retVal;
	}
    
}
