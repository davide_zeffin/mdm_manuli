/*****************************************************************************
    Class         SetSavedItemListB2BAction
    Copyright (c) 2007, SAP AG, Germany, All rights reserved.
    Description:  Action to set a saved item list as current item list
    Author:       SAP
    Created:      February 2007
    Version:      1.0

    $Revision: #1 $
    $Date: 2007/02/13 $
*****************************************************************************/
package com.sap.isa.catalog.actions;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.sap.isa.businessobject.CommunicationException;
import com.sap.isa.catalog.webcatalog.WebCatInfo;
import com.sap.isa.core.UserSessionData;

/**
 *
 * This action sets a saved item list as current item list after Back navigation
 * from related products page
 *
 * @author SAP
 * @version 1.0
 *
 */
public class SetSavedItemListB2BAction extends CatalogBaseAction {
    
    public ActionForward doPerform(ActionMapping mapping,
            ActionForm form,
            HttpServletRequest request,
            HttpServletResponse response,
            UserSessionData userData,
            WebCatInfo theCatalog)
            throws IOException, ServletException, CommunicationException {

        log.debug("Entered to SetSavedItemListB2BAction");

        //If not do it yourself if you can
        if (theCatalog == null) {
            //WebCatInfo.catalogLog.fatal("No Catalog Information availabale!");
            return (mapping.findForward("error"));
        }
        
        theCatalog.setCurrentItemList(theCatalog.getSavedItemList());
        // clear the saved item list
        theCatalog.setSavedItemList(null);
        theCatalog.resetCUAList();
        
        request.setAttribute(ActionConstants.RA_DISPLAY_SCENARIO, ActionConstants.RA_CURRENT_QUERY);
        request.setAttribute(ActionConstants.RA_IS_QUERY, ActionConstants.RA_IS_QUERY_VALUE_YES);
        request.setAttribute(ActionConstants.RA_DETAILSCENARIO, ActionConstants.DS_CATALOG_QUERY);
        
        return (mapping.findForward(ActionConstants.FW_SUCCESS));
   }    

}
