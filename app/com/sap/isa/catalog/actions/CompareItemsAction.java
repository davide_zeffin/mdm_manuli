package com.sap.isa.catalog.actions;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Comparator;
import java.util.Iterator;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.sap.isa.businessobject.CommunicationException;
import com.sap.isa.catalog.boi.IAttribute;
import com.sap.isa.catalog.boi.IAttributeValue;
import com.sap.isa.catalog.webcatalog.WebCatAttributeList;
import com.sap.isa.catalog.webcatalog.WebCatInfo;
import com.sap.isa.catalog.webcatalog.WebCatItem;
import com.sap.isa.catalog.webcatalog.WebCatItemList;
import com.sap.isa.core.UserSessionData;
import com.sap.isa.core.logging.IsaLocation;

/**
 * Title:        CompareItemsAction
 * Description:  Show to user all the attributes of all the selected items
 *               and make a comparation among them
 * Copyright:    Copyright (c) 2001
 * Company:      SAP AG, Germany
 * @version 1.0
 */

public class CompareItemsAction extends CatalogBaseAction {
    
    private static IsaLocation log = IsaLocation.getInstance(CompareItemsAction.class.getName());

	public class CompareOperation implements Comparator {
		public CompareOperation() {
		}

		public int compare(Object obj1, Object obj2) {
			IAttributeValue atr1 = (IAttributeValue) obj1;
			IAttributeValue atr2 = (IAttributeValue) obj2;
			Integer int1, int2;
			try {
				int1 = new Integer(atr1.getAttribute().getDetail("POS_NR").getAsString());
			} catch (java.lang.Exception e) {
				//        log.info("CompareItemsAction:CompareOperation Exception occured: "+ e.toString());
				int1 = new Integer(-1);
			}

			try {
				int2 = new Integer(	atr2.getAttribute().getDetail("POS_NR").getAsString());
			} catch (java.lang.Exception e) {
				//        log.info("CompareItemsAction:CompareOperation Exception occured: "+ e.toString());
				int2 = new Integer(-1);
			}

			return int1.compareTo(int2);
		}
	}

	/**
	 * Process the specified HTTP request, and create the corresponding HTTP
	 * response (or forward to another web component that will create it).
	 * Return an <code>ActionForward</code> instance describing where and how
	 * control should be forwarded, or <code>null</code> if the response has
	 * already been completed.
	 *
	 * @param mapping The ActionMapping used to select this instance
	 * @param form The optional ActionForm bean for this request (if any)
	 * @param request The HTTP request we are processing
	 * @param response The HTTP response we are creating
	 * @param userData container for session informations
	 * @param theCatalog representation of the catalog
	 *
	 * @exception IOException if an input/output error occurs
	 * @exception ServletException if a servlet exception occurs
	 * 
	 * @return specific action forward corresponding to the struts configuration
	 */
	public ActionForward doPerform(
		ActionMapping mapping,
		ActionForm form,
		HttpServletRequest request,
		HttpServletResponse response,
		UserSessionData userData,
		WebCatInfo theCatalog)
		throws IOException, ServletException, CommunicationException {
            
        log.entering("doPerform");

		String price_parameter =
			servlet.getServletConfig().getInitParameter("prices");
		log.debug("Price configuration is: " + price_parameter);

		if (theCatalog == null) {
			//WebCatInfo.catalogLog.fatal("No Catalog Information availabale!");
			return (mapping.findForward("error"));
		}
		
		ArrayList validItems = new ArrayList(); //the list of selected items
		WebCatItemList currentList;
		
		String groupKey = request.getParameter("groupKey");
		
		if (groupKey == null || "".equals(groupKey)) {
			log.debug("Compare for main items called");
            groupKey = "";
			//finds out which are the current items the user is seeing
    		if (request.getParameter(ActionConstants.RA_COMPARE_CUA) != null
    				&& request.getParameter(ActionConstants.RA_COMPARE_CUA).equals("true")) {
				currentList = theCatalog.getCurrentCUAList();
			} else {
				currentList = this.getScenarioDependentItemList(request, theCatalog);
			}

			Iterator itemIterator = currentList.iteratorOnlyPopulated();
			while (itemIterator.hasNext()) {
				WebCatItem item = (WebCatItem) itemIterator.next();
				if (item.isSelected()) {
					//for items coming from query, get area specific attributes by calling
					//refreshCatalogItem
					if (currentList.getQuery() != null && currentList.getAreaQuery() == null) {
						//it's a catalog query
						item.refreshCatalogItem();
					}
					String quantity = item.getQuantity();
					if (quantity.trim().equals("") || quantity.trim().equals("0")) {
						item.setQuantity("1");
					}
					validItems.add(item);
				}
				Collection subItems = item.getContractItems();
				if (subItems != null && subItems.size() != 0) {
					Iterator subItemsIterator = subItems.iterator();
					while (subItemsIterator.hasNext()) {
						WebCatItem subItem = (WebCatItem) subItemsIterator.next();
						if (subItem.isSelected()) {
							//same groupKey
							if (groupKey.equals("") || subItem.getGroupKey().equals(groupKey)) {
								String quantity = subItem.getQuantity();
								if (quantity.trim().equals("") || quantity.trim().equals("0")) {
									subItem.setQuantity("1");
								}
								validItems.add(subItem);
							} //end if same groupKey
						} //end if item is selected
					} //end while loop over sub-items
				} //end if item has sub-items
			}
		}
		else {
			log.debug("Compare for Group called - uses Subitems");
			//it is probably a package component comparison...
			if (theCatalog.getCurrentItem() != null) {
				currentList = theCatalog.getCurrentItem().getWebCatSubItemList();
				if (currentList != null) {
					Iterator subItemsIterator = currentList.iterator();
					while (subItemsIterator.hasNext()) {
						WebCatItem subItem = (WebCatItem) subItemsIterator.next();
						if (subItem.isSelected()) {
							// same groupKey
							if (subItem.getGroupKey().equals(groupKey)) {
								String quantity = subItem.getQuantity();
								if (quantity.trim().equals("") || quantity.trim().equals("0")) {
									subItem.setQuantity("1");
								}
								validItems.add(subItem);
							}
						}
					}
				}
			}
		}
        
        if (log.isDebugEnabled()) {
			log.debug("No of valid items: "+validItems.size());
        }
        
		WebCatAttributeList atributeNamesMap = new WebCatAttributeList(theCatalog);

		ActionForward returnData = null;

		if (validItems.size() < 2) {
			if (request.getParameter("ignoreEmptyListAlert") != null) {
				returnData = mapping.findForward("empty_list_no_alert");
			} else {
				returnData = mapping.findForward("empty_list");
			}
		}

		if (returnData != null) {
			return returnData;
		}

		Iterator validItemsIterator = validItems.iterator();

		//creates a list of all the names for the attributes
		//this list is the Union of all parameters' names lists for all the selected items

		while (validItemsIterator.hasNext()) {
			WebCatItem theItem = (WebCatItem) validItemsIterator.next();
			//gets the next element in valid items' list
			Iterator theItemAtributes = theItem.getCatalogItemAttributes(new CompareOperation());
			//gets its Attributes' list
			while (theItemAtributes.hasNext()) {
				IAttributeValue atributeValue = (IAttributeValue) theItemAtributes.next();
				IAttribute atributeName = atributeValue.getAttribute();
				//gets Attribute
				if (atributeNamesMap.get(atributeName.getGuid()) == null) {
					//if the name is not already in the list
					atributeNamesMap.put(atributeName.getGuid(), atributeName);
					//adds the new name of atribute
				}
			}
		}

		if (!theCatalog.getLastVisited().equals(ActionConstants.DS_CUA) && (request.getParameter(ActionConstants.RA_COMPARE_CUA) == null
				|| !request.getParameter(ActionConstants.RA_COMPARE_CUA).equals("true"))) {
			theCatalog.setLastVisited(WebCatInfo.COMPARE);
		}

		Iterator atributeNamesIterator = atributeNamesMap.iterator();

		request.setAttribute("theItems", validItems);
		request.setAttribute("theNames", atributeNamesIterator);
		request.setAttribute("theNamesList", atributeNamesMap);

		return (mapping.findForward(ActionConstants.FW_SUCCESS));
	}

}