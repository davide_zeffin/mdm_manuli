package com.sap.isa.catalog.actions;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.sap.isa.businessobject.CommunicationException;
import com.sap.isa.catalog.webcatalog.WebCatInfo;
import com.sap.isa.core.UserSessionData;

/**
 * Title:
 * Description:
 * Copyright:    Copyright (c) 2001
 * Company:
 * @author
 * @version 1.0
 */

public class BackFromCUAAction extends CatalogBaseAction {

    public ActionForward doPerform(
        ActionMapping mapping,
        ActionForm form,
        HttpServletRequest request,
        HttpServletResponse response,
        UserSessionData userData,
        WebCatInfo theCatalog)
        throws IOException, ServletException, CommunicationException {

        String st = "";
        String type = "";

        if (theCatalog.getCurrentItemList() != null && theCatalog.getCurrentItemList().getQuery() != null) {
            if (theCatalog.getCurrentItemList().getAreaQuery() != null) {
                st = "/catalog/getAttributes.do?isQuery=no";
                type = "query";
            }
            else {
                st = "/catalog/itemPage.do?isQuery=yes";
                type = "query";
            }
        }
        else {
            st = "/catalog/products.do";
            if (theCatalog.getCurrentArea() != null)
                type = theCatalog.getCurrentArea().getAreaName();
            else
                type = "products";
        }

        if (!st.equals("")) {
            request.setAttribute("backPath", st);
            request.setAttribute("type", type);
        }
        if (request.getParameter("direct") != null)
            return mapping.findForward("direct");
        else
            return mapping.findForward(ActionConstants.FW_SUCCESS);
    }
}
