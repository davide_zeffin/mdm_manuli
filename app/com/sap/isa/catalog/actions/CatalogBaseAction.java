/*****************************************************************************
 Class:        CatalogBaseAction
 Copyright (c) 2004, SAP AG, Germany, All rights reserved.
 Author:       SAP AG
 Created:      01.12.2004
 Version:      1.0

 $Revision: #13 $
 $Date: 2003/05/06 $ 
 *****************************************************************************/

package com.sap.isa.catalog.actions;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Comparator;
import java.util.Iterator;
import java.util.Properties;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.sap.isa.backend.boi.isacore.contract.ContractView;
import com.sap.isa.backend.boi.isacore.marketing.MarketingConfiguration;
import com.sap.isa.backend.boi.webcatalog.CatalogBusinessObjectsAware;
import com.sap.isa.backend.boi.webcatalog.CatalogConfiguration;
import com.sap.isa.backend.boi.webcatalog.atp.IAtpResult;
import com.sap.isa.backend.boi.webcatalog.pricing.Prices;
import com.sap.isa.backend.crm.webcatalog.CatalogGuid;
import com.sap.isa.businessobject.AppBaseBusinessObjectManager;
import com.sap.isa.businessobject.CommunicationException;
import com.sap.isa.businessobject.IdMapper;
import com.sap.isa.businessobject.contract.ContractAttributeList;
import com.sap.isa.businessobject.contract.ContractReferenceMap;
import com.sap.isa.businessobject.webcatalog.CatalogBusinessObjectManager;
import com.sap.isa.businessobject.webcatalog.atp.Atp;
import com.sap.isa.businessobject.webcatalog.atp.AtpResultList;
import com.sap.isa.businessobject.webcatalog.contract.ContractReferenceRequest;
import com.sap.isa.businessobject.webcatalog.pricing.PriceCalculator;
import com.sap.isa.businesspartner.backend.boi.PartnerFunctionData;
import com.sap.isa.businesspartner.businessobject.BusinessPartner;
import com.sap.isa.businesspartner.businessobject.BusinessPartnerManager;
import com.sap.isa.catalog.AttributeKeyConstants;
import com.sap.isa.catalog.CatalogParameters;
import com.sap.isa.catalog.boi.CatalogException;
import com.sap.isa.catalog.boi.IAttribute;
import com.sap.isa.catalog.boi.IAttributeValue;
import com.sap.isa.catalog.boi.ICatalog;
import com.sap.isa.catalog.boi.ICategory;
import com.sap.isa.catalog.boi.IFilter;
import com.sap.isa.catalog.boi.IQuery;
import com.sap.isa.catalog.boi.IQueryStatement;
import com.sap.isa.catalog.filter.CatalogFilter;
import com.sap.isa.catalog.filter.CatalogFilterFactory;
import com.sap.isa.catalog.webcatalog.WebCatArea;
import com.sap.isa.catalog.webcatalog.WebCatAreaAttributes;
import com.sap.isa.catalog.webcatalog.WebCatAreaQuery;
import com.sap.isa.catalog.webcatalog.WebCatInfo;
import com.sap.isa.catalog.webcatalog.WebCatItem;
import com.sap.isa.catalog.webcatalog.WebCatItemList;
import com.sap.isa.catalog.webcatalog.WebCatItemPrice;
import com.sap.isa.core.TechKey;
import com.sap.isa.core.UserSessionData;
import com.sap.isa.core.businessobject.management.MetaBusinessObjectManager;
import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.core.util.RequestParser;
import com.sap.isa.isacore.action.EComBaseAction;

/**
 * The class CatalogBaseAction is simple base for catalog actions . <br>
 *
 * @author  SAP AG
 * @version 1.0
 */
abstract public class CatalogBaseAction extends EComBaseAction {
    
    // constants for adbaced search populate type 
    protected static final int POPULATE_TYPE_ALL = 0;
    protected static final int POPULATE_TYPE_UP_TO = 1;
    
    private final String PROP_FUZZY_SEARCH = "enableFuzzySearch";
    
    /**
     * ContractView to be used in further processing of the action.
     * Overwrite this in subclass to retrieve contract detail view data in
     * Internet Sales. Defaults to ContractView.PRODUCT_LIST.
     */
    protected ContractView contractView = ContractView.PRODUCT_LIST;
    
    public class CompareOperation implements Comparator {

        public CompareOperation() {
        }

        public int compare(Object obj1, Object obj2) {

            IAttributeValue atr1 = (IAttributeValue) obj1;
            IAttributeValue atr2 = (IAttributeValue) obj2;
            Integer int1, int2;

            try {
                int1 =  new Integer(atr1.getAttribute().getDetail("POS_NR").getAsString());
            } catch (java.lang.Exception e) {
                log.debug("GetProductAction:CompareOperation Exception occured for attribute " + ((atr1 == null)? "null" : atr1.getAttributeName()) + " : " + e.toString());
                int1 = new Integer(-1);
            }

            try {
                int2 = new Integer(atr2.getAttribute().getDetail("POS_NR").getAsString());
            } catch (java.lang.Exception e) {
                log.debug("GetProductAction:CompareOperation Exception occuredfor attribute " + ((atr2 == null)? "null" : atr2.getAttributeName()) + " : " + e.toString());
                int2 = new Integer(-1);
            }

            return int1.compareTo(int2);
        }
    }

    /**
     * gather all items for pricing from the given iterator
     */
    protected ArrayList collectPricingItems(boolean doPricing,  Iterator iterator) {
        
        ArrayList items = new ArrayList();
        
        WebCatItem item;
        
        if (doPricing) {
            
            while (iterator.hasNext()) {
                item = (WebCatItem) iterator.next();
    
                if (item.getItemPrice() == null) {
                    items.add(item);
                }
            
                Collection subItems = item.getContractItems();
            
                if (subItems != null && !subItems.isEmpty()) {
                
                    Iterator subItemIterator = subItems.iterator();
                
                    while (subItemIterator.hasNext()) {
                        WebCatItem subItem = (WebCatItem) subItemIterator.next();
                        if (subItem.getItemPrice() == null) {
                            items.add(subItem);
                        }
                    }
                
                }
            }
        }
    
        return items;
        
    }

    /**
     * Process the specified HTTP request, and create the corresponding HTTP
     * response (or forward to another web component that will create it).
     * Return an <code>ActionForward</code> instance describing where and how
     * control should be forwarded, or <code>null</code> if the response has
     * already been completed.
     * 
     * @param mapping mapping The ActionMapping used to select this instance
     * @param form The <code>FormBean</code> specified in the Struts configuration file for this action
     * @param request The request object
     * @param response The response object
     * @param userData Object wrapping the session
     * @param theCatalog theCatalog Representation of the catalog
     * @return Forward to another action or JSP page
     * @throws IOException
     * @throws ServletException
     * @throws CommunicationException
     */
    public ActionForward doPerform(ActionMapping mapping,
            ActionForm form,
            HttpServletRequest request,
            HttpServletResponse response,
            UserSessionData userData,
            WebCatInfo theCatalog)
            throws IOException, ServletException, CommunicationException {

        return null;
    }

    /**
     * Overwrite this method to add functionality to your action. With this signature you get
     * the access to RequestParser and MetaBusinessObjectManager.
     * 
     * @param mapping mapping The ActionMapping used to select this instance
     * @param form The <code>FormBean</code> specified in the Struts configuration file for this action
     * @param request The request object
     * @param response The response object
     * @param userData Object wrapping the session
     * @param theCatalog theCatalog Representation of the catalog
     * @param requestParser Parser to simple retrieve data from the request
     * @param mbom Meta business object manager
     * 
     * @return Forward to another action or JSP page
     * @throws IOException
     * @throws ServletException
     * @throws CommunicationException
     */
    public ActionForward doPerform(ActionMapping mapping,
            ActionForm form,
            HttpServletRequest request,
            HttpServletResponse response,
            UserSessionData userData,
            WebCatInfo theCatalog,
            RequestParser requestParser,
            MetaBusinessObjectManager mbom)
            throws IOException, ServletException, CommunicationException {

        return doPerform(mapping,
                form,
                request,
                response,
                userData,
                theCatalog);
    }

    /**
     * Overwrites the method . <br>
     * 
     * @param mapping
     * @param form
     * @param request
     * @param response
     * @param userSessionData
     * @param requestParser
     * @param mbom
     * @param multipleInvocation
     * @param browserBack
     * @return
     * @throws IOException
     * @throws ServletException
     * @throws CommunicationException
     * 
     * 
     * @see com.sap.isa.isacore.action.EComBaseAction#ecomPerform(org.apache.struts.action.ActionMapping, org.apache.struts.action.ActionForm, javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse, com.sap.isa.core.UserSessionData, com.sap.isa.core.util.RequestParser, com.sap.isa.core.businessobject.management.MetaBusinessObjectManager, boolean, boolean)
     */
    public ActionForward ecomPerform(ActionMapping mapping,
            ActionForm form,
            HttpServletRequest request,
            HttpServletResponse response,
            UserSessionData userSessionData,
            RequestParser requestParser,
            MetaBusinessObjectManager mbom,
            boolean multipleInvocation,
            boolean browserBack)
            throws IOException, ServletException, CommunicationException {

        CatalogBusinessObjectManager bom = (CatalogBusinessObjectManager) mbom.getBOMbyName(CatalogBusinessObjectManager.CATALOG_BOM);

        WebCatInfo theCatalog = bom.getCatalog();

        return doPerform(mapping,
                form,
                request,
                response,
                userSessionData,
                theCatalog,
                requestParser,
                mbom);
    }

    /**
     * Reprices given ArrayList of WebCatItems.
     * 
     * @param mbom MetaBusinessObjectManager to get access to business objects.
     * @param items ArrayList of WebCatItems
     */
	public static boolean repriceItems(MetaBusinessObjectManager mbom, ArrayList items) {

        CatalogBusinessObjectManager bom = (CatalogBusinessObjectManager) mbom.getBOMbyName(CatalogBusinessObjectManager.CATALOG_BOM);

        PriceCalculator priceCalc = bom.getPriceCalculator();
        if (priceCalc == null) {
            return false;
        }

        WebCatItem[] itemArray = (WebCatItem[]) items.toArray(new WebCatItem[items.size()]);
        
        // set all prices to null to enforce repricing
        for (int i = 0; i < itemArray.length; i++) {
            itemArray[i].setItemPrice(null);
        }

        // start repricing
        Prices[] priceList = priceCalc.getPrices(itemArray);
        if (priceList == null || itemArray.length != priceList.length) {
            for (int i = 0; i < itemArray.length; i++) {
                itemArray[i].setItemPrice(new WebCatItemPrice());
            }
            return false;
        }
        else {
            for (int i = 0; i < itemArray.length; i++) {
                itemArray[i].setItemPrice(new WebCatItemPrice(priceList[i]));
            }
            return true;
        }

    }

    /**
     * Reprices given WebCatItem.
     * 
     * @param mbom MetaBusinessObjectManager to get access to business objects.
     * @param item WebCatItem for repricing
     * 
     * @return true if repricing was successfull
     */
    protected boolean repriceItem(MetaBusinessObjectManager mbom, WebCatItem item) {

        ArrayList items = new ArrayList();
        items.add(item);
        return repriceItems(mbom, items);
    }
    
    /**
     * Returns the catalog configuration aware object used, to get the Catalog configuration
     * 
     * @return the configuration aware object
     */
    public CatalogBusinessObjectsAware getCatalogObjectAwareBom(UserSessionData userSessionData) {
       
        return (CatalogBusinessObjectsAware) userSessionData.getMBOM().getBOMByType(CatalogBusinessObjectsAware.class);
    }
    
    /**
     * Returns the catalog configuration object
     * 
     * @return the catalog configuration object
     */
    public CatalogConfiguration getCatalogConfiguration(UserSessionData userSessionData) {
        
        CatalogConfiguration catalogConfiguration = null;
        
        CatalogBusinessObjectsAware cataConfBom =  getCatalogObjectAwareBom(userSessionData);
        if (cataConfBom != null) {
            catalogConfiguration = cataConfBom.getCatalogConfiguration();
        }
        
        return catalogConfiguration;
    }
    
    /**
     * Returns true if the CUA buttons should be displayed in the application
     * 
     * @param userSessionData Object wrapping the session
     * 
     * @return true if the CUA buttons should be shown, false otherwise
     */
    public boolean showCUAButtons(UserSessionData userSessionData){
    	CatalogConfiguration catalogConfiguration = getCatalogConfiguration(userSessionData);
    	return catalogConfiguration.showCatCUAButtons();
    }

    /**
     * Executes a category specific search for the given filter and area
     * 
     * @param theCatalog theCatalog Representation of the catalog
     * @param filter the query filter to use for query execution
     * @param currentArea the area to execute the search on
     * @param pAttr list of attribute Guids
     * @param pNames list of attribute names
     * @param pValues list of attribute values
     * @param populateAll should the item list be fully populated or or not
     * @param upTo number of items that should be populated, if set 
     *        to 0 the default populate will be executed
     * @param populateType defines if either the populateAll flag should be taken into 
     *        account, or the upTo paramter
     */
    protected WebCatItemList executeCategoryQuery(WebCatInfo theCatalog, IFilter filter, WebCatArea currentArea, 
                                                  ArrayList pAttr, ArrayList pNames, ArrayList pValues, 
                                                  boolean populateAll, int upTo, int populateType) throws CatalogException {
                       
        log.entering("executeCategoryQuery(WebCatInfo theCatalog, IFilter filter, WebCatArea currentArea, pAttr, pNames, pValues, populateAll, upTo, populateType)");
        
        WebCatItemList itemList;
        WebCatAreaQuery areaQuery = new WebCatAreaQuery();
        areaQuery.setPrevAttribs(pAttr);
        areaQuery.setPrevNames(pNames);
        areaQuery.setPrevValues(pValues);
        areaQuery.setPrevFilter(filter);
        
        ICategory category = currentArea.getCategory();
        IQueryStatement queryStmt = category.createQueryStatement();
        queryStmt.setStatement(filter, null, null); // attributes, sort order?
        
        IQuery query = category.createQuery(queryStmt);
        // create WebCatItemList from this query and return
        if (populateType == POPULATE_TYPE_ALL) {
            itemList = new WebCatItemList(theCatalog, query, false, populateAll);
        }
        else {
            itemList = new WebCatItemList(theCatalog, query, upTo);
        }

        itemList.setAreaQuery(areaQuery);
        
        log.exiting();
        
        return itemList;
    }
    
    /**
     * Executes a category specific search for the given filter and area
     * 
     * @param theCatalog theCatalog Representation of the catalog
     * @param filter the query filter to use for query execution
     * @param currentArea the area to execute the search on
     * @param pAttr list of attribute Guids
     * @param pNames list of attribute names
     * @param pValues list of attribute values
     */
    protected WebCatItemList executeCategoryQuery(WebCatInfo theCatalog, IFilter filter, WebCatArea currentArea, 
                                                  ArrayList pAttr, ArrayList pNames, ArrayList pValues) 
                             throws CatalogException {
        log.entering("executeCategoryQuery(WebCatInfo theCatalog, IFilter filter, WebCatArea currentArea, pAttr, pNames, pValues)"); 
        log.exiting();              
        return this.executeCategoryQuery(theCatalog, filter, currentArea, pAttr, pNames, pValues, false, 0, POPULATE_TYPE_ALL);
    }
    
    /**
     * Executes a category specific search for the given filter and area
     * 
     * @param theCatalog theCatalog Representation of the catalog
     * @param filter the query filter to use for query execution
     * @param currentArea the area to execute the search on
     * @param pAttr list of attribute Guids
     * @param pNames list of attribute names
     * @param pValues list of attribute values
     * @param upTo number of items that should be populated, if set 
     *        to 0 the default populate will be executed
     */
    protected WebCatItemList executeCategoryQuery(WebCatInfo theCatalog, IFilter filter, WebCatArea currentArea, 
                                                  ArrayList pAttr, ArrayList pNames, ArrayList pValues, int upTo) 
                             throws CatalogException {
        log.entering("executeCategoryQuery(WebCatInfo theCatalog, IFilter filter, WebCatArea currentArea, pAttr, pNames, pValues, upTo)"); 
        log.exiting();             
        return this.executeCategoryQuery(theCatalog, filter, currentArea, pAttr, pNames, pValues, false, upTo, POPULATE_TYPE_UP_TO);
    }
    
    
    /**
     * Executes a category specific search for the given filter and area
     * 
     * @param theCatalog theCatalog Representation of the catalog
     * @param filter the query filter to use for query execution
     * @param currentArea the area to execute the search on
     * @param pAttr list of attribute Guids
     * @param pNames list of attribute names
     * @param pValues list of attribute values
     * @param populateAll should the item list be fully populated or or not
     */
    protected WebCatItemList executeCategoryQuery(WebCatInfo theCatalog, IFilter filter, WebCatArea currentArea, 
                                                  ArrayList pAttr, ArrayList pNames, ArrayList pValues, 
                                                  boolean populateAll) 
                             throws CatalogException {
        log.entering("executeCategoryQuery(WebCatInfo theCatalog, IFilter filter, WebCatArea currentArea, pAttr, pNames, pValues, populateAll)"); 
        log.exiting();              
        return this.executeCategoryQuery(theCatalog, filter, currentArea, pAttr, pNames, pValues, populateAll, 0, POPULATE_TYPE_ALL);
    }

    /**
     * Add a new attribute for the category specific search is added the given filter
     * via AND condition, if valid
     * 
     * @param filter the query filter to add the value to
     * @param fact the filter factory
     * @param pAttr list of attribute Guids
     * @param pNames list of attribute names
     * @param pValues list of attribute values
     * @param attribute the catalog attribute, to add as new filter value 
     * @param attribValue the value of the attribute
     * 
     * @return the filter, extendend with the new filter value via AND condition 
     */
    protected IFilter addCategoryQueryFilterValue(IFilter filter, CatalogFilterFactory fact, ArrayList pAttr, 
                                                  ArrayList pNames, ArrayList pValues, IAttribute attribute, 
                                                  String attribValue) {
        
        log.entering("addCategoryQueryFilterValue()"); 
                                                      
        if (attribValue != null && !attribValue.equals("")) { //it's ok to include this value to the filters.
            IFilter productFilter;
            productFilter = fact.createAttrContainValue(attribute.getGuid(),
                    attribValue);
            if (filter == null) {
                filter = productFilter;
            }
            else {
                filter = fact.createAnd(productFilter, filter); // AND statement
            }
            pAttr.add(attribute.getGuid());
            pNames.add(attribute.getDescription());
            pValues.add(attribValue);
        }
        
        log.exiting();
        
        return filter;
    }
    
    /**
     * The method performs an advanced search with the given criteria and returns a webCatItemList 
     * 
     * @param mktConfig as MarketingConfiguration 
     * @param theCatalog as WebCatInfo, the catalog 
     * @param advSQuery as String, the query search string
     * @param advSStdCat as boolean, flag indicating if the search should be performed in the standard catalog 
     * @param advSStdArea as String, area key of the standard area 
     * @param advSRewCat as boolean, flag indicating if the search should be performed in the reward catalog 
     * @param advSRewArea as String, area key of the reward area 
     * @param ptsFrom as float, lower boarder of points for search in reward catalog only  
     * @param ptsTo as float, higher boarder of points for search in reward catalog only 
     * @param queryAttr as String[], array of attributes which are compared with the query search string     
     * @param upTo how many items should be populated, if upTo <= 0, just a simple populate() will be done.
     *  
     * @return itemList as WebCatItemList
     */
    protected WebCatItemList advancedSearch(MarketingConfiguration mktConfig, WebCatInfo theCatalog, 
                                            String advSQuery, boolean advSStdCat, String advSStdArea, 
                                            boolean advSRewCat, String advSRewArea, 
                                            float ptsFrom, float ptsTo, String[] queryAttr,
                                            int upTo) { 
                                                
        log.entering("advancedSearch(mktConfig, theCatalog, advSQuery, advSStdCat, advSStdArea, advSRewCat, advSRewArea, ptsFrom, ptsTo, queryAttr, upTo)");
        log.exiting();
        return advancedSearch(mktConfig, theCatalog, advSQuery, advSStdCat, advSStdArea, advSRewCat, 
                       advSRewArea, ptsFrom, ptsTo, queryAttr, false, 0, POPULATE_TYPE_UP_TO);
    }
    
    /**
     * The method performs an advanced search with the given criteria and returns a webCatItemList 
     * 
     * @param mktConfig as MarketingConfiguration 
     * @param theCatalog as WebCatInfo, the catalog 
     * @param advSQuery as String, the query search string
     * @param advSStdCat as boolean, flag indicating if the search should be performed in the standard catalog 
     * @param advSStdArea as String, area key of the standard area 
     * @param advSRewCat as boolean, flag indicating if the search should be performed in the reward catalog 
     * @param advSRewArea as String, area key of the reward area 
     * @param ptsFrom as float, lower boarder of points for search in reward catalog only  
     * @param ptsTo as float, higher boarder of points for search in reward catalog only 
     * @param queryAttr as String[], array of attributes which are compared with the query search string     
     * @param populateAll as boolean, flag for item populate
     *  
     * @return itemList as WebCatItemList
     */
    protected WebCatItemList advancedSearch(MarketingConfiguration mktConfig, WebCatInfo theCatalog, 
                                            String advSQuery, boolean advSStdCat, String advSStdArea, 
                                            boolean advSRewCat, String advSRewArea, 
                                            float ptsFrom, float ptsTo, String[] queryAttr,
                                            boolean populateAll) { 
        
        log.entering("advancedSearch(mktConfig, theCatalog, advSQuery, advSStdCat, advSStdArea, advSRewCat, advSRewArea, ptsFrom, ptsTo, queryAttr, populateAll)");
        
        return advancedSearch(mktConfig, theCatalog, advSQuery, advSStdCat, advSStdArea, advSRewCat, 
                       advSRewArea, ptsFrom, ptsTo, queryAttr, populateAll, 0, POPULATE_TYPE_ALL);
    }
                                                  
    
    /**
     * The method performs an advanced search with the given criteria and returns a webCatItemList 
     * 
     * @param mktConfig as MarketingConfiguration 
     * @param theCatalog as WebCatInfo, the catalog 
     * @param advSQuery as String, the query search string
     * @param advSStdCat as boolean, flag indicating if the search should be performed in the standard catalog 
     * @param advSStdArea as String, area key of the standard area 
     * @param advSRewCat as boolean, flag indicating if the search should be performed in the reward catalog 
     * @param advSRewArea as String, area key of the reward area 
     * @param ptsFrom as float, lower boarder of points for search in reward catalog only  
     * @param ptsTo as float, higher boarder of points for search in reward catalog only 
     * @param queryAttr as String[], array of attributes which are compared with the query search string     
     * @param populateAll as boolean, flag for item populate, if populateType is POPULATE_TYPE_ALL
     * @param upTo how many items should be populated, if populateType is POPULATE_TYPE_UP_TO
     *        if upTo <= 0, just a simple populate() will be done.
     * @param populateType how shoul be populated
     *  
     * @return itemList as WebCatItemList
     */
    protected WebCatItemList advancedSearch ( MarketingConfiguration mktConfig, WebCatInfo theCatalog, 
                                              String advSQuery, boolean advSStdCat, String advSStdArea, boolean advSRewCat, String advSRewArea, 
                                              float ptsFrom, float ptsTo, String[] queryAttr,
                                              boolean populateAll, int upTo, int populateType) {   
        
        final String METHOD_NAME = "advancedSearch(mktConfig, theCatalog, advSQuery, advSStdCat, advSStdArea, advSRewCat, advSRewArea, ptsFrom, ptsTo, queryAttr, populateAll, upTo, populateType)";
        log.entering(METHOD_NAME);
        
        WebCatItemList itemList = null;

        try {
    
            // ---------- Build query on base of the search criteria -----------   
            ICatalog iCatalog = theCatalog.getCatalog();
            IQueryStatement queryStmt = iCatalog.createQueryStatement();
            // set query type = advanced search
            queryStmt.setSearchType(IQueryStatement.Type.ITEM_ADVSEARCH);
    
            CatalogFilterFactory fact = CatalogFilterFactory.getInstance();
    
            Properties serverEngineProps = new Properties();
            boolean fuzzySearch = true;
            serverEngineProps = theCatalog.getCatalogServerEngine().getProperties();
    
            String enableFuzzySearch = serverEngineProps.getProperty(PROP_FUZZY_SEARCH );
            /* no fuzzy search if 
               a) property is not set
               b) property is set to 'false'
               c) query string contains an asterisk
            */
            if (enableFuzzySearch == null ||
                enableFuzzySearch.equalsIgnoreCase("false") ||
                advSQuery.indexOf("*") > -1){
                fuzzySearch = false;
            }
    
            // ---------- Filter creation -----------   
            IFilter filter = null;
            
            // Search in standard catalog and/or reward catalog
            boolean hasPtsItems = false;
            boolean hasBuyPtsItems = false;
            Iterator attribIter = iCatalog.getAttributes();
            
            while ( attribIter.hasNext() && (!hasPtsItems || !hasBuyPtsItems) ) {
                IAttribute attrib = (IAttribute) attribIter.next();
                if ("IS_PTS_ITEM".equals(attrib.getName())) {
                    hasPtsItems = true;
                    continue;
                }
                if ("IS_BUY_PTS_ITEM".equals(attrib.getName())) {
                    hasBuyPtsItems = true;
                }
            }
                
            // Search in standard catalog
            if (advSStdCat) {
                if (hasBuyPtsItems) {
                    filter = (CatalogFilter) fact.createAttrEqualValue("IS_BUY_PTS_ITEM", "X");
                    filter = (CatalogFilter) fact.createNot(filter);
                }
                    
                if (hasPtsItems) {
                    IFilter filter2 = (CatalogFilter) fact.createAttrEqualValue("IS_PTS_ITEM", "X");
                    filter2 = (CatalogFilter) fact.createNot(filter2);
                    if (filter != null) {
                        filter = (CatalogFilter) fact.createAnd(filter, filter2);
                    }
                    else {
                        filter = filter2;
                    }
                }
                
                // and standard area if area was entered as search criterion
                if (advSStdArea != null && advSStdArea.length() > 0) { 
                    String attrName = theCatalog.getCatalog().getAttributeGuid(AttributeKeyConstants.AREA_GUID);
                    if (attrName != null  && attrName.length() > 0) {
                        IFilter filter2 = (CatalogFilter) fact.createAttrEqualValue(attrName, advSStdArea);
                        if (filter != null) {
                            filter = (CatalogFilter) fact.createAnd(filter, filter2);
                        }
                        else {
                            filter = filter2;
                        }
                    }
                }
            }
                        
            // Search in reward catalog only if atleast one area exists with points items 
            if (advSRewCat && hasPtsItems) {
                IFilter filter2 = (CatalogFilter) fact.createAttrEqualValue("IS_PTS_ITEM", "X");
                            
                // and reward area if area was entered as search criterion
                if (advSRewArea != null && advSRewArea.length() > 0) { 
                    String attrName = theCatalog.getCatalog().getAttributeGuid(AttributeKeyConstants.AREA_GUID);
                    if (attrName != null  && attrName.length() > 0) {
                        IFilter filter3 = (CatalogFilter) fact.createAttrEqualValue(attrName, advSRewArea);
                        filter2 = (CatalogFilter) fact.createAnd(filter2, filter3);
                    }
                }
                // add points search
                if (ptsTo > 0 && ptsTo >= ptsFrom) {
                    if (mktConfig != null &&
                        mktConfig.getPointCode() != null) {  
                        String attrName = AttributeKeyConstants.LOY_POINTS + mktConfig.getPointCode();
                        IFilter filter3 = (CatalogFilter) fact.createAttrBetweenValue(attrName, Float.toString(ptsFrom), Float.toString(ptsTo));
                        filter2 = (CatalogFilter) fact.createAnd(filter2, filter3);
                    }  
                }
                
                // append to filter 
                if (filter == null) {
                    filter = filter2;
                }
                else {
                    filter = (CatalogFilter) fact.createOr(filter, filter2);
                }
            }

            // add attributes depending on the search strategy
            // fuzzySeach 
            IFilter filter2 = null;
            if (fuzzySearch) {
                filter2 = (CatalogFilter) fact.createAttrFuzzyValue(queryAttr[0], advSQuery);
    
                for ( int i = 1; i < queryAttr.length; i++ ) {
                    IFilter filter3 = fact.createAttrFuzzyValue(queryAttr[i], advSQuery);
                    filter2 = (CatalogFilter) fact.createOr(filter2, filter3);
                }
            }
            // No fuzzySeach 
            else {
                filter2 = (CatalogFilter)fact.createAttrContainValue(queryAttr[0], advSQuery);   

                for ( int i = 1; i < queryAttr.length; i++ ) {
                    IFilter filter3 = fact.createAttrContainValue(queryAttr[i], advSQuery);
                    filter2 = (CatalogFilter) fact.createOr(filter2, filter3);
                }
            }
            
            // add catalog and catalog area restriction in filter2                         
            if (filter != null) {
                filter = (CatalogFilter) fact.createAnd(filter, filter2);
            }
            else {
                filter = filter2;
            }

            // ---------- Create Query Statement -----------   
            queryStmt.setStatement(filter, null, null, 1, CatalogParameters.maxSearchResults);
        
            IQuery query = theCatalog.getCatalog().createQuery(queryStmt);
            if (log.isDebugEnabled()) {
                log.debug(METHOD_NAME + ": Query Stmt=" + queryStmt.toString());
            }
            
            if (populateType == POPULATE_TYPE_ALL) {
                itemList = new WebCatItemList(theCatalog, query, false, populateAll); 
            }
            else {
                itemList = new WebCatItemList(theCatalog, query, upTo);
            }
        }
        catch (CatalogException e) {
            log.error("system.backend.exception", e);
            itemList = new WebCatItemList(theCatalog);
        }
       
        log.exiting(); 
        
        return (itemList);
    }

    /**
     * The method determines and returns the list of quick search attributes  
     * 
     * @param theCatalog as WebCatInfo, the catalog 
     *  
     * @return String[] the list of queick search attributes
     */
    protected String[] getQuickSearchAttributes(WebCatInfo theCatalog) {
        
        log.entering("getQuickSearchAttributes");
        
        final String METHOD_NAME = "getQuickSearchAttributes(WebCatInfo theCatalog)";
        log.entering(METHOD_NAME);

        String[] attList = null;
        
        ICatalog iCatalog = theCatalog.getCatalog();
        attList = iCatalog.getQuickSearchAttributes();

        if (log.isDebugEnabled()) {
            if (attList == null) {
                log.debug(METHOD_NAME + ": Number of quick search attributes = 0");
            }
            else {
                log.debug(METHOD_NAME + ": Number of quick search attributes =" + attList.length);
            }
        }
        
        log.exiting(); 
        
        return (attList);
    }

    /**
     * Determine the ATP data for the given item
     * 
     * @param userData Object wrapping the session
     * @param bom the catalog business object manager
     * @param theItem the catalog item to determine the ATP data for
     * @param log the location to write messages to
     */
    protected AtpResultList determineATPResult(UserSessionData userSessionData, CatalogBusinessObjectManager bom, 
                                               WebCatItem theItem, IsaLocation log) {
        
        log.entering("determineATPResult");
            
        CatalogConfiguration catConfig = getCatalogConfiguration(userSessionData);
        String catalogGuid = catConfig.getCatalog();
        String cat = CatalogGuid.getCatalog(catalogGuid);
        String variant = CatalogGuid.getVariant(catalogGuid);
           
        AtpResultList atpResultList = null;
         
        if (log.isDebugEnabled()) {
            log.debug("the Guid for the current catalog is: " + catalogGuid);
            log.debug("The catalog is: " + cat + " and the variant: " + variant);
        }
        	
        Atp atp = bom.createAtp();
        
        try {
            if (log.isDebugEnabled()) {
                log.debug("Unit of measurement attribute is: " + theItem.getUnit());
            }
            
            theItem.setAtpResultList(null); 

            AppBaseBusinessObjectManager appBaseBom = (AppBaseBusinessObjectManager) userSessionData.getBOM(AppBaseBusinessObjectManager.APPBASE_BOM);
            IdMapper idMapper = appBaseBom.createIdMapper();

            String matNr = idMapper.mapIdentifier(IdMapper.PRODUCT,                             // object to map id for 
                                                  IdMapper.CATALOG,                              // source  
                                                  IdMapper.BASKET,                                 // target 
                                                  theItem.getProductID());
          	
            atpResultList = atp.readAtpResultList(catConfig.getId(), catConfig.getCountry(),
                                                  theItem,   //the Item itself
                                                  matNr,
                                                  null,          // requested Date
                                                  cat,           //  catalog name
                                                  variant);      //  variant name
        
            if (atpResultList != null) {
                boolean isZero = true;
            
                for (int i=0; i < atpResultList.size(); i++) {
                  IAtpResult atpResult = atpResultList.get(i);
                  if (atpResult.getCommittedQuantity() != null && 
                      atpResult.getCommittedQuantity().charAt(0) != '0') {
                      isZero = false;
                  } 
                  atpResult.setDatesToStr(catConfig.getDateFormat());
                }
                
                if (!isZero){ 
                    theItem.setAtpResultList(atpResultList);
                }
            }
        }
        catch (CommunicationException ex) {
          log.error("Communication exception");
        }
        catch (java.lang.Exception ex) {
          log.error("General Exception Cought");
        }
        
        log.exiting();
        
        return atpResultList;
    }
     
    /**
     * Determine area specific attributes for the item
     * 
     * @param theItem the catalog item to determine the ATP data for
     * @param log the location to write messages to
     * 
     * @return Iterator containing the attributes
     */
    public Iterator determineAreaAttributes(WebCatItem theItem, IsaLocation log) {
        
        log.entering("determineAreaAttributes");
        
        Iterator neededItemAtributes;
        //WebCatAttributes
        if (theItem.getCatalogItem() != null && theItem.getCatalogItem().getParent() != null) {
            log.debug("determine Area Attributes");
            WebCatAreaAttributes areaAttrib = new WebCatAreaAttributes(theItem.getCatalogItem().getParent());
            if (areaAttrib != null) {
                theItem.setAreaAttributes(areaAttrib);
            }
        }
        
        neededItemAtributes = theItem.getCatalogItemAttributes(new CompareOperation());
        
        log.exiting();
        
        return neededItemAtributes;
    }

    /**
       * Fill the request which is used to retrieve contracts with the needed
       * item data. In this implementation, sets product, productGuid, lnaguage
       * and item. Overwrite this method for other data to be used for contract
       * determination. Be sure to call this class' method in sublasses using
       * <code>super.fillContractReferenceRequest()</code> to keep the data of
       * this implementation.
       * For EBP and InternetSales purposes there exist subclasses
       * <code>ISAGetContractInfoAction</code> and <code>EBPGetContractInfoAction</code>.
       * If you want to add data in these scenarios, subclass those actions instead
       * of this action!
       * @param contractRequest the ContractReferenceRequest that is filled
       * @param item the WebCatItem for which contracts are searched. This is used
       *             in most backend specific implementations to determine some
       *             additional data needed for that implementation.
       * @param catalog the catalog we are working with
       * @param userData the user data (which contains current language)
       */
    protected void fillContractReferenceRequest(ContractReferenceRequest contractRequest, WebCatItem item, 
                                                WebCatInfo catalog, UserSessionData userData) {
    	
        log.entering("fillContractReferenceRequest()");
        
        contractRequest.setProduct(item.getProduct());
    	contractRequest.setProductGuid(item.getProductID());
    	contractRequest.setLanguage(userData.getLocale().getLanguage());
    	contractRequest.setItem(item);
        
        log.exiting();
        
      }

    /**
     * Set the contract info at the given items.
     * 
     * @param items the items to set the contract info
     * @param contractRefMap map caonatining the contract references
     * @param contractView the contract view
     * @param log the location to write messages to
     */
    protected void setContractInfo(ArrayList items, ContractReferenceMap contractRefMap,  
                                   ContractView contractView, IsaLocation log) {
        
        log.entering("setContractInfo()");
        
        Iterator iterator;
        WebCatItem item;
        // loop only affected items
        iterator = items.iterator();
        
        // add contract data to WebCatItem
        while (iterator.hasNext()) {
        
            item = (WebCatItem) iterator.next();
        
            if (log.isDebugEnabled())  {
        	    log.debug("set contracts for item "+item.getProduct());
            }
            
            TechKey produktKey = new TechKey(item.getProductID());
            if (contractView == ContractView.PRODUCT_DETAIL) {
                item.setContractDetailedItems(contractRefMap.get(produktKey));
            }
            else {
                item.setContractItems(contractRefMap.get(produktKey));
            }	
        }
        
        log.exiting();
        
    }

    /**
     * Determines contract data for the items given in the iterator
     * 
     * @param userData Object wrapping the session
     * @param theCatalog theCatalog Representation of the catalog
     * @param bom the catalog business object manager
     * @param iterator contains webCatItems to determine contratc data for
     * @param items list of WebCatItems fille from the items in the iterator
     * @param contractView the contract view
     * @param log the location to write messages to
     * 
     * @return ContractReferenceMap the filled contract refrence map
     */
    protected ContractReferenceMap determineContractInfo(UserSessionData userData, WebCatInfo theCatalog, 
                                                         CatalogBusinessObjectManager bom, Iterator iterator, 
                                                         ArrayList items, ContractView contractView, IsaLocation log) {
        
        log.entering("determineContractInfo");
            
        ArrayList contractRequestsList = new ArrayList();
        WebCatItem item;
        
        while (iterator.hasNext()) {
          item = (WebCatItem) iterator.next();
          // if contract info has been evaluated for this item don't do it again.
          // item without contracts has empty contract list, item which has
          // not been evaluated yet has not contracts list at all!
          if ((contractView != ContractView.PRODUCT_DETAIL && item.getContractItems() == null) ||
        	  (contractView == ContractView.PRODUCT_DETAIL && item.getContractDetailedRef() == null))  {
        	ContractReferenceRequest contractRequest = new ContractReferenceRequest();
        	fillContractReferenceRequest(contractRequest, item, theCatalog, userData);
        	contractRequestsList.add(contractRequest);
        	items.add(item);
          }
        }
        
        // call contract reference reader
        ContractReferenceMap contractRefMap = null;
        ContractReferenceRequest[] contractRequests =
        	 (ContractReferenceRequest[]) contractRequestsList.toArray(
        		  new ContractReferenceRequest[contractRequestsList.size()]);
        if (!items.isEmpty()) {// do this only if additional contract info is needed
        
          try {
        	contractRefMap = bom.createContractReferenceReader().
        						 readReferences(contractRequests,
        										contractView);
        	if (log.isDebugEnabled())
        		log.debug("Contracts determined: " + contractRefMap.size());
          }
          catch (Exception e) {
        	// if something happened, continue without contract data...
        	log.error("error in contract data determination",e);
          }
        }
        
        log.exiting();
        
        return contractRefMap;
    }

    /**
     * Determines the contract attribute list
     * 
     * @param userData Object wrapping the session
     * @param bom the catalog business object manager
     * @param contractView the contract view
     * @param log the location to write messages to
     */
    protected ContractAttributeList determineContractAttributeList(UserSessionData userData, CatalogBusinessObjectManager bom, 
                                                                   ContractView contractView, IsaLocation log) {
        
        log.entering("determineContractAttributeList()");
        
        ContractAttributeList attributeList = null;
                                                                       
        try {
          attributeList = bom.createContractReferenceReader().readAttributes(userData.getLocale().getLanguage(),
        									 contractView);
        }
        catch (Exception e) {
          log.debug("Exception ",e);
        }
        if (attributeList == null) {
          attributeList = new ContractAttributeList();
        }
        
        log.exiting();
        
        return attributeList;
    }

    /**
     * Set SoldTo Dat in the contract reference request
     * 
     * @param contractRequest the contract reqquest to set the soldTo data in
     * @param userData Object wrapping the session
     */
    protected void fillContractReferenceRequestForSoldToAndSalesOrg(ContractReferenceRequest contractRequest, UserSessionData userData) {
        
        log.entering("fillContractReferenceRequestForSoldToAndSalesOrg");
        
        CatalogBusinessObjectsAware catObjBom = getCatalogObjectAwareBom(userData);
        
        BusinessPartnerManager buPaMa = catObjBom.createBUPAManager();
        
        BusinessPartner soldTo = buPaMa.getDefaultBusinessPartner(PartnerFunctionData.SOLDTO);
                          
        if (catObjBom != null) {
          if (catObjBom.getCatalogConfiguration() != null) {
            contractRequest.setSalesOrg(catObjBom.getCatalogConfiguration().getSalesOrganisation());
            contractRequest.setDistChannel(catObjBom.getCatalogConfiguration().getDistributionChannel());
          }
          if (soldTo != null) {
            contractRequest.setSoldToId(soldTo.getTechKey().getIdAsString());
          }
        }
        
        log.exiting();
    }


    /**
     * Determines the item list used in the current display scenario. The display scenario 
     * considered here is:<br/>
     * <ul>
     * 	<li>Special Offers/Bestsellers</li>
     * 	<li>Browsing catalog</li>
     * 	<li>Quicksearch</li>
     * 	<li>Cross-/Up-/Downselling Products</li>
     * 	<li>Recommendations</li>
     * </ul>
     * <br/>
     * @param request The current request from the client.
     * @param theCatalog The catalog used in the shop.
     * @return The {@link WebCatItemList} used in the current scenario.
     */
    public WebCatItemList getScenarioDependentItemList(HttpServletRequest request, WebCatInfo theCatalog) {
    	
    	WebCatItemList myItemList = (WebCatItemList) request.getAttribute(ActionConstants.RA_ITEMLIST_EBP);
        

        if (myItemList == null) {
        	// The request parameter display_scenario can have the values "products", for catalog browsing,
        	// and "query", for the result of an explicit or implicit (CUA) search/query
        	String displayScenario = request.getParameter(ActionConstants.RA_DISPLAY_SCENARIO);
        	if (displayScenario == null)
        		displayScenario = request.getAttribute(ActionConstants.RA_DISPLAY_SCENARIO) == null ? null : request.getAttribute(ActionConstants.RA_DISPLAY_SCENARIO).toString();
        	
        	//The request parameter detailScenario differentiates between different types of searches/queries
        	String detailScenario = request.getParameter(ActionConstants.RA_DETAILSCENARIO);
        	if (detailScenario == null)
        		detailScenario = request.getAttribute(ActionConstants.RA_DETAILSCENARIO) == null ? null : request.getAttribute(ActionConstants.RA_DETAILSCENARIO).toString();

        	if(displayScenario == null && detailScenario != null && detailScenario.equals(ActionConstants.DS_CUA)) {
        		request.setAttribute(ActionConstants.RA_IS_PRODUCT_LIST, request.getParameter(ActionConstants.RA_IS_PRODUCT_LIST));
        		myItemList = theCatalog.getCurrentCUAList();
    			if(myItemList != null){
    				myItemList.resetCurrentItemPage();
    			}
        		
        	} else if (displayScenario != null && displayScenario.equals(ActionConstants.DS_PRODUCTS)) {
        		// in case we are coming from a CUA list and display one item, then we need the CUA list and not the current item list.
        		if(detailScenario != null && detailScenario.equals(ActionConstants.DS_CUA)) {
        			myItemList = theCatalog.getCurrentCUAList();
        			if(myItemList != null){
        				myItemList.resetCurrentItemPage();
        			}
        		} else {
        			myItemList = theCatalog.getCurrentItemList();
        		}
            }
            else if (displayScenario != null && displayScenario.equals(ActionConstants.RA_CURRENT_QUERY)) {
            	
            	if(detailScenario == null || detailScenario.equals("") || 
    	        		detailScenario.equals(ActionConstants.DS_BESTSELLER) || 
    	        		detailScenario.equals(ActionConstants.DS_RECOMMENDATION) ||
    	        		detailScenario.equals(ActionConstants.DS_CATALOG_QUERY)) {
    	        	
            		myItemList = theCatalog.getCurrentItemList();
    	        } else {
            		request.setAttribute(ActionConstants.RA_IS_PRODUCT_LIST, request.getParameter(ActionConstants.RA_IS_PRODUCT_LIST));
            		myItemList = theCatalog.getCurrentCUAList();
            		myItemList.resetCurrentItemPage();
    	        }
            } else {
            	myItemList = theCatalog.getCurrentItemList();
            }
        }

        return myItemList;
    }
    
    /**
     * This method checks in the request if it contains an attriubte with the 
     * requested key, if not then it is checking for a parameter. If the parameter
     * also does not exists, then an empty string is returned.
     * 
     * @param requestKey The key to get the value from the request. 
     * @return An empty string of the value of attribute or parameter.
     */
    public String getValueFromRequest(HttpServletRequest request, String requestKey) {

    	String toReturn = (String) request.getAttribute(requestKey);

        if (null == toReturn) {
        	toReturn = (String) request.getParameter(requestKey);
        }

        if (null == toReturn) {
        	toReturn = "";
        }
    	return toReturn;
    }

}