/*****************************************************************************
Class:        CategorieFilterViewAction
Copyright (c) 2006, SAP AG, Germany, All rights reserved.
Author:       d038380
Created:      06.02.2006
Version:      1.0
*****************************************************************************/
package com.sap.isa.catalog.actions;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.sap.isa.businessobject.CommunicationException;
import com.sap.isa.businessobject.webcatalog.CatalogBusinessObjectManager;
import com.sap.isa.catalog.webcatalog.WebCatInfo;
import com.sap.isa.core.BaseAction;
import com.sap.isa.core.UserSessionData;

/**
 * Title:        CategorieFilterViewAction
 * Description:  Shows results for Area Query filtering. 
 *               If no Area Query is available in the current item list of the catalog, the area
 *               without filtering will be shown.
 * Copyright:    Copyright (c) 2006
 * Company:      SAP AG Germany
 * @version 1.0
 * @author d038380
 */
public class CategorieFilterViewAction extends CatalogBaseAction {

    /**
     * Process the specified HTTP request, and create the corresponding HTTP
     * response (or forward to another web component that will create it).
     * Return an <code>ActionForward</code> instance describing where and how
     * control should be forwarded, or <code>null</code> if the response has
     * already been completed.
     *
     * @param mapping The ActionMapping used to select this instance
     * @param form The optional ActionForm bean for this request (if any)
     * @param request The HTTP request we are processing
     * @param response The HTTP response we are creating
     * @param userData container for session informations
     * @param theCatalog representation of the catalog
     *
     * @exception IOException if an input/output error occurs
     * @exception ServletException if a servlet exception occurs
     * 
     * @return specific action forward corresponding to the struts configuration
     */
    public ActionForward doPerform(ActionMapping mapping,
            ActionForm form,
            HttpServletRequest request,
            HttpServletResponse response,
            UserSessionData userData,
            WebCatInfo theCatalog)
            throws IOException, ServletException, CommunicationException {
        
        //If not do it yourself if you can
        if (theCatalog == null) {
            return (mapping.findForward("error"));
        }

        // Area Query is only available, if before this action the action
        // com.sap.isa.catalog.actions.CategorieFilterAction was performed.
        // If the Area Query result page will be bookmarked, this action is the 
        // fist one which will be executed and thus no Area Query is available.
        // Because in this case we don't have any Area Query data, we will show the
        // standard area list w/o Area Query filtering.
        if(theCatalog.getCurrentItemList().getAreaQuery() == null) {
            return (mapping.findForward("noAreaQueryAvailable"));
        }
        
        // we need this attribute because this action is responsible for the view part of the
        // B2C frameless framework. Only if this parameter is set to "no", the ProductsB2C.inc.jsp 
        // works correctly
        request.setAttribute("isQuery", "no");
        return (mapping.findForward(ActionConstants.FW_SUCCESS));
    }

}
