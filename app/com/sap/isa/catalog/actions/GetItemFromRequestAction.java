package com.sap.isa.catalog.actions;

import java.util.Iterator;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.sap.isa.businessobject.webcatalog.ItemDetailRequest;
import com.sap.isa.catalog.webcatalog.WebCatAreaAttributes;
import com.sap.isa.catalog.webcatalog.WebCatInfo;
import com.sap.isa.catalog.webcatalog.WebCatItem;
import com.sap.isa.core.UserSessionData;

/**
 * Title:        GetItemFromRequestAction
 * Description:  Gets on request a ItemDetailRequest Object, reads from it the
 *               areadID, and productID, and with respect of these IDs, it creates
 *               a WebCatItemObject
 * Copyright:    Copyright (c) 2001
 * Company:      SAPMarkets Europe GmbH
 * @version 1.0
 */

public class GetItemFromRequestAction extends CatalogBaseAction
{

    /**
     * Process the specified HTTP request, and create the corresponding HTTP
     * response (or forward to another web component that will create it).
     * Return an <code>ActionForward</code> instance describing where and how
     * control should be forwarded, or <code>null</code> if the response has
     * already been completed.
     *
     * @param mapping The ActionMapping used to select this instance
     * @param actionForm The optional ActionForm bean for this request (if any)
     * @param request The HTTP request we are processing
     * @param response The HTTP response we are creating
     *
     * @exception IOException if an input/output error occurs
     * @exception ServletException if a servlet exception occurs
     */

  public ActionForward doPerform( ActionMapping mapping,
                                  ActionForm form,
                                  HttpServletRequest request,
                                  HttpServletResponse response,
                                  UserSessionData userSessionData,
                                  WebCatInfo theCatalog)
  throws ServletException {
      
    String forward = ActionConstants.FW_SUCCESS;  

    ItemDetailRequest itemFromRequest = (ItemDetailRequest) request.getAttribute(ActionConstants.RA_ITEMREQUEST);
    
    if (itemFromRequest == null) {
      log.error("No Items in the request !");
      return mapping.findForward(ActionConstants.FW_ERROR);
    }
    
    log.debug("itemFromRequest.areaID "+itemFromRequest.getAreaID() + "itemFromRequest.productID "+itemFromRequest.getProductID());
    
    WebCatItem theItem;
    boolean fromRequest = false;
    
    if (request.getAttribute(ActionConstants.RA_SHOW_PROD_DET_ITEM) == null) {
		log.debug("No WebCatItem object in the request. Determined calling the API. Time-consuming behaviour");
		theItem = theCatalog.getItem(itemFromRequest.getAreaID(),  itemFromRequest.getProductID());
	} 
    else {
	    theItem = (WebCatItem) request.getAttribute(ActionConstants.RA_SHOW_PROD_DET_ITEM);
	    fromRequest = true;
	    log.debug("There is WebCatItem object in the request. This is: " + theItem.getProduct());
	}

    if (theItem == null) {
        log.error("The item in the request does not lead to a valid catalog item !");
        return mapping.findForward(ActionConstants.FW_ERROR);
    }
	
    log.debug("itemFromRequest transferred to a WebCatItem:" + theItem.getProduct());
    
    theItem.readItemPrice();

    // Don't set currentItem, if we are called from the QuickSearch, this is true, when scenario is detailInWindow
    if (!ActionConstants.DS_IN_WINDOW.equals(request.getParameter("scenario"))) { 
        theCatalog.setCurrentItem(theItem);
    }
    else {
        forward = "detailInWindow";
    }

    if (theItem.getCatalogItem().getParent() != null && !fromRequest) {
        WebCatAreaAttributes areaAttrib = new WebCatAreaAttributes(theItem.getCatalogItem().getParent());
        if (areaAttrib != null) {
            theItem.setAreaAttributes(areaAttrib);
        }
    }

    Iterator neededItemAtributes = theItem.getCatalogItemAttributes();
    request.setAttribute("currentItem", theItem);
    request.setAttribute("itemAttributesIterator", neededItemAtributes);
    request.setAttribute(ActionConstants.RA_DETAILSCENARIO,itemFromRequest.getDetailScenario());
        
    return mapping.findForward(forward);
  }
}
