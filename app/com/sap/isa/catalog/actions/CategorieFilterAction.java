package com.sap.isa.catalog.actions;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.sap.isa.businessobject.BusinessObjectException;
import com.sap.isa.businessobject.CommunicationException;
import com.sap.isa.businessobject.webcatalog.CatalogBusinessObjectManager;
import com.sap.isa.catalog.boi.CatalogException;
import com.sap.isa.catalog.boi.IAttribute;
import com.sap.isa.catalog.boi.IFilter;
import com.sap.isa.catalog.filter.CatalogFilterFactory;
import com.sap.isa.catalog.webcatalog.WebCatArea;
import com.sap.isa.catalog.webcatalog.WebCatInfo;
import com.sap.isa.catalog.webcatalog.WebCatItemList;
import com.sap.isa.core.UserSessionData;
import com.sap.isa.core.util.Message;
import com.sap.isa.core.util.WebUtil;

/**
 * Title:        GetProductsAction
 * Description:  Creates the query from data obtained from the input query
 *               applies it on a category, and built the resulting WebCatItemList
 *               list of items
 * Copyright:    Copyright (c) 2001
 * Company:      SAPMarkets Europe GmbH
 * @version 1.0
 */

public class CategorieFilterAction extends CatalogBaseAction {

    /**
     * Process the specified HTTP request, and create the corresponding HTTP
     * response (or forward to another web component that will create it).
     * Return an <code>ActionForward</code> instance describing where and how
     * control should be forwarded, or <code>null</code> if the response has
     * already been completed.
     *
     * @param mapping The ActionMapping used to select this instance
     * @param form The optional ActionForm bean for this request (if any)
     * @param request The HTTP request we are processing
     * @param response The HTTP response we are creating
     * @param userData container for session informations
     * @param theCatalog representation of the catalog
     *
     * @exception IOException if an input/output error occurs
     * @exception ServletException if a servlet exception occurs
     * 
     * @return specific action forward corresponding to the struts configuration
     */
    public ActionForward doPerform(ActionMapping mapping,
            ActionForm form,
            HttpServletRequest request,
            HttpServletResponse response,
            UserSessionData userData,
            WebCatInfo theCatalog)
            throws IOException, ServletException, CommunicationException {
        
        CatalogBusinessObjectManager bom = (CatalogBusinessObjectManager) userData.getBOM(CatalogBusinessObjectManager.CATALOG_BOM);

        //If not do it yourself if you can
        if (theCatalog == null) {
            //WebCatInfo.catalogLog.fatal("No Catalog Information availabale!");
            return (mapping.findForward("error"));
        }
        theCatalog.setLastVisited(WebCatInfo.CATEGORY_QUERY);

        // check, if we really should execute the query, or only do some paging, add to basket, ...
        // if the NEXT value is set, this means, the add to basket, compare, ... buttons have been
        // pressed and no new search is necessary.
        String next = request.getParameter(ActionConstants.RA_NEXT);

        boolean noSearch = false;

        if (next != null) {
            noSearch = true;
            // do not rexecute the query.
        }

        WebCatItemList itemList = null;
        IFilter filter = null;

        if (!noSearch) {
            WebCatArea currentArea = bom.getCatalog().getCurrentArea();

            Iterator attributes = null;
            try {
                attributes = currentArea.getCategory().getAttributes();
            }
            catch (CatalogException e) {
                log.error("catalog.exception.backend", e);
                Message msg = new Message(Message.ERROR,
                        "catalog.exception.usermsg");
                BusinessObjectException boe = new BusinessObjectException("Catalog error",
                        msg);
                request.setAttribute("Exception", boe);
                return mapping.findForward(ActionConstants.FW_ERROR);
            }

            CatalogFilterFactory fact = CatalogFilterFactory.getInstance();
            ArrayList pAttr, pNames, pValues;
            pAttr = new ArrayList();
            pNames = new ArrayList();
            pValues = new ArrayList();

            while (attributes.hasNext()) {
                IAttribute attribute = (IAttribute) attributes.next();
                String formValue = request.getParameter(attribute.getGuid());

                filter = addCategoryQueryFilterValue(filter, fact, pAttr, pNames, pValues, attribute, formValue);

                log.debug(attribute.getGuid() + " Read from the request: "  + formValue);
            }

            if (filter != null) {
                
                try {
                    itemList = executeCategoryQuery(theCatalog, filter, currentArea,  pAttr, pNames, pValues);
                }
                catch (CatalogException e) {
                    log.error("system.eai.exception", e);
                    itemList = new WebCatItemList(bom.getCatalog());
                }
                request.setAttribute("isQuery", "no");
            }
            else {
                itemList = theCatalog.getCurrentArea().getItemList();
            }

            bom.getCatalog().setCurrentItemList(itemList);
        }
        else {
            itemList = theCatalog.getCurrentItemList();
            if (itemList != null && itemList.getQuery() != null
                    && itemList.getQuery().getStatement() != null) {
                filter = itemList.getQuery().getStatement().getFilter();
            }
            request.setAttribute("isQuery", "no");
        }

        request.setAttribute(ActionConstants.RA_ITEMLIST_EBP, itemList);
        // argument for the title
        String[] args = { theCatalog.getCurrentArea().getAreaName() };
        String listTitleArg0 = WebUtil.translate(userData.getLocale(),
                "b2c.cat.b2ccatarea",
                args);
        request.setAttribute(ActionConstants.RA_PRODUCT_LIST_TITLE_ARG0,
                listTitleArg0);

        request.setAttribute("toNext", "toItemPage");
        request.setAttribute(ActionConstants.RA_AREA,
                bom.getCatalog().getCurrentArea());
                
        try {
            if (filter == null && mapping.findForward("products") != null) {
                request.setAttribute("theNextPage", "productsDisplay");
                return mapping.findForward("products");
            }
        }
        catch (Exception e) {
            log.debug("Foward products does not exist");
        }

        if (itemList.size() == 1 && mapping.findForward("onlyOne") != null) {
            theCatalog.setCurrentItem(itemList.getItem(0));
            theCatalog.getCurrentItem().readItemPrice();
            return mapping.findForward("onlyOne");
        }
        else {
            return (mapping.findForward(ActionConstants.FW_SUCCESS));
        }
    }
}