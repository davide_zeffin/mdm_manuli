package com.sap.isa.catalog.actions;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.sap.isa.backend.boi.isacore.BaseConfiguration;
import com.sap.isa.backend.boi.isacore.marketing.MarketingBusinessObjectsAware;
import com.sap.isa.backend.boi.isacore.marketing.MarketingConfiguration;
import com.sap.isa.backend.boi.webcatalog.CatalogBusinessObjectsAware;
import com.sap.isa.backend.boi.webcatalog.CatalogConfiguration;
import com.sap.isa.businessobject.CommunicationException;
import com.sap.isa.businessobject.marketing.CUAList;
import com.sap.isa.catalog.webcatalog.WebCatArea;
import com.sap.isa.catalog.webcatalog.WebCatInfo;
import com.sap.isa.catalog.webcatalog.WebCatItem;
import com.sap.isa.core.UserSessionData;
/**
 * Title:        GetPathAction 
 * Description:  Starting from the current Area, this Action moves backward through
 *               Categorie Children - Categorie Parent chain, till the root, and stores 
 *               all the categories from this chain into <code>arrayIDs</code> ArrayList. 
 *               Actually, this Action produces a reversed order list of the path toward 
 *               the current Area. 
 * Copyright:    Copyright (c) 2001 
 * Company:      SAP AG Germany
 *
 * @version 1.0
 */
public class GetPathAction extends CatalogBaseAction {

	/**
	 * Process the specified HTTP request, and create the corresponding HTTP
	 * response (or forward to another web component that will create it).
	 * Return an <code>ActionForward</code> instance describing where and how
	 * control should be forwarded, or <code>null</code> if the response has
	 * already been completed.
	 *
	 * @param mapping The ActionMapping used to select this instance
	 * @param form The optional ActionForm bean for this request (if any)
	 * @param request The HTTP request we are processing
	 * @param response The HTTP response we are creating
	 * @param userData container for session informations
	 * @param theCatalog representation of the catalog
	 *
	 * @exception IOException if an input/output error occurs
	 * @exception ServletException if a servlet exception occurs
	 * 
	 * @return specific action forward corresponding to the struts configuration
	 */
	public ActionForward doPerform(
		ActionMapping mapping,
		ActionForm form,
		HttpServletRequest request,
		HttpServletResponse response,
		UserSessionData userData,
		WebCatInfo theCatalog)
		throws IOException, ServletException, CommunicationException {

		log.entering("Entered to GetPathAction");

		String forward = ActionConstants.FW_SUCCESS;
		ArrayList arrayIDs = new ArrayList();
		WebCatArea theArea = theCatalog.getCurrentArea();

		do {
			arrayIDs.add(theArea);
			theArea = theArea.getParentArea();
		} while (
			(theArea != null)
				&& !theArea.getAreaID().equals(WebCatArea.ROOT_AREA));

		request.setAttribute("thePath", arrayIDs);

		if (request.getParameter("displayedTab") != null 
		    && !((String)request.getParameter("displayedTab")).equals("")
			&& !((String)request.getParameter("displayedTab")).equals("0")) {
			String tabToDisplay = (String)request.getParameter("displayedTab");
			int tab = (new Integer(tabToDisplay)).intValue();
			switch(tab){
				case WebCatInfo.PROD_DET_LIST_ACCESSORIES: 
				   forward = "displayAcc";
				   break;
				case WebCatInfo.PROD_DET_LIST_ALTERNATIVES: 
				   forward = "displayAlt";
				   break;
				case WebCatInfo.PROD_DET_LIST_CONTRACTS: 
				   forward = "displayContracts";
				   break;
				case WebCatInfo.PROD_DET_LIST_EXCH_PROD: 
				   forward = "displayExchProd";
				   break;
				case WebCatInfo.PROD_DET_LIST_REL_PROD: 
				   forward = "displayRelProd";
				   break;
			}
		} else {
			if (request.getAttribute("theNextPage") == null) {
				
				MarketingConfiguration mktConfig = null;
				MarketingBusinessObjectsAware mkbom = (MarketingBusinessObjectsAware) userData.getMBOM().getBOMByType(MarketingBusinessObjectsAware.class);
				if (mkbom != null) {
					mktConfig = mkbom.getMarketingConfiguration();
				}
				
				boolean readAccessory =	false;
				boolean readRelProd = false;
                if (mktConfig != null && showCUAButtons(userData) && theCatalog.getCuaList() == null) {
                	if (mktConfig.isAccessoriesAvailable()) {
						readAccessory = true;	
					}
                	else {
						readRelProd = true;
					}
                }

				if (readAccessory == false
					&& theCatalog.getCuaList() != null
					&& request.getAttribute("lastVisited") != null
					&& (request.getAttribute("lastVisited").equals("itemList"))) {
					WebCatItem webCatItem =	(WebCatItem) request.getAttribute(ActionConstants.RC_WEBCATITEM);
					if (webCatItem != null) {
						String currentProdGuid = webCatItem.getCatalogItem().getAttributeValue("OBJECT_ID").getAsString();
						if (!((CUAList) theCatalog.getCuaList()).getProductId().equals(currentProdGuid)) {
							readAccessory = true;
						}
					}
				}

				//in B2B case, we only display the list of accessories
				if (readAccessory == false) {
					CatalogBusinessObjectsAware catalogObjBom =
						(CatalogBusinessObjectsAware) userData.getMBOM().getBOMByType(CatalogBusinessObjectsAware.class);

					// now I can read the shop
					CatalogConfiguration catalogConfig = catalogObjBom.getCatalogConfiguration();

					if (!(catalogConfig.getScenario().equals(BaseConfiguration.B2C)
						|| catalogConfig.getScenario().equals(BaseConfiguration.TELCO_CUSOMER_SELF_SERVICE))
						&& theCatalog.getCuaList() != null
						&& request.getAttribute("lastVisited") != null
						&& request.getAttribute("lastVisited").equals("itemDetails")
						&& theCatalog.getProductDetailListType() == 0) {
						readAccessory = true;
					}
				}

		        // detail scenario
		        String detailScenario = request.getParameter(ActionConstants.RA_DETAILSCENARIO);
		        if (log.isDebugEnabled()) {
		            log.debug("Request parameter '" + ActionConstants.RA_DETAILSCENARIO + "' = " + detailScenario);
		        }

		        //reset the currentCUAList if it is not the associated to the current webCatItem -> also delete it if it is obsolete
		        //(but only, if detailScenario != "cuaList"
				WebCatItem webCatItem =	(WebCatItem) request.getAttribute(ActionConstants.RC_WEBCATITEM);
				if (webCatItem != null && !(ActionConstants.DS_CUA.equals(detailScenario))) {
					String currentProdGuid = webCatItem.getCatalogItem().getAttributeValue("OBJECT_ID").getAsString();
					if (theCatalog.getCuaList() != null
						&& !((CUAList) theCatalog.getCuaList()).getProductId().equals(currentProdGuid)) {
						theCatalog.setCuaType("");
						theCatalog.setCuaList(null);
					}
				}

				if (readAccessory) {
					forward = "successAcc";
				}
				else if (readRelProd) {
					forward = "displayRelProd";
				}
			} else {
				forward = (String) request.getAttribute("theNextPage");
			}
		}
		return mapping.findForward(forward);
	}
}
