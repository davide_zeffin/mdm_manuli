package com.sap.isa.catalog.actions;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.sap.isa.businessobject.CommunicationException;
import com.sap.isa.catalog.CatalogParameters;
import com.sap.isa.catalog.boi.CatalogException;
import com.sap.isa.catalog.boi.IQuery;
import com.sap.isa.catalog.boi.IQueryStatement;
import com.sap.isa.catalog.webcatalog.WebCatInfo;
import com.sap.isa.catalog.webcatalog.WebCatItem;
import com.sap.isa.catalog.webcatalog.WebCatItemList;
import com.sap.isa.core.UserSessionData;
import com.sap.isa.core.action.ReloadOneTimeAction;
import com.sap.isa.core.util.WebUtil;

/**
 * Title: ProductCatalog UI 
 * Description: 
 * Copyright: Copyright (c) 2001 
 * Company: SAPMarkets Europe GmbH
 * 
 * @version 1.0
 */
public class GetQueryAction extends CatalogBaseAction {
    
    /**
     * Process the specified HTTP request, and create the corresponding HTTP
     * response (or forward to another web component that will create it).
     * Return an <code>ActionForward</code> instance describing where and how
     * control should be forwarded, or <code>null</code> if the response has
     * already been completed.
     *
     * @param mapping The ActionMapping used to select this instance
     * @param form The optional ActionForm bean for this request (if any)
     * @param request The HTTP request we are processing
     * @param response The HTTP response we are creating
     * @param userData container for session informations
     * @param theCatalog representation of the catalog
     *
     * @exception IOException if an input/output error occurs
     * @exception ServletException if a servlet exception occurs
     * 
     * @return specific action forward corresponding to the struts configuration
     */
    public ActionForward doPerform(ActionMapping mapping,
            ActionForm form,
            HttpServletRequest request,
            HttpServletResponse response,
            UserSessionData userData,
            WebCatInfo theCatalog)
            throws IOException, ServletException, CommunicationException {
        
        log.debug("Entered to GetQueryAction");

        String queryStr = request.getParameter(ActionConstants.RA_CURRENT_QUERY);

        if (null == queryStr) {
            queryStr = (String) request.getAttribute(ActionConstants.RA_CURRENT_QUERY);
            if (null == queryStr) {
                queryStr = getContextValue(request,
                        ActionConstants.CV_CURRENT_QUERY);
            }
        }

        String advSQuery = getContextValue(request, ActionConstants.CV_ADV_SEARCH);
        boolean advSearch = ( advSQuery != null && advSQuery.equals("true") );

        //If not do it yourself if you can

        if (theCatalog == null) {
            log.debug("No catalog");
            //WebCatInfo.catalogLog.fatal("No Catalog Information
            // availabale!");
            return (mapping.findForward(ActionConstants.FW_ERROR));
        }

        // get the previous query result
        WebCatItemList itemList = (WebCatItemList) request.getAttribute(ActionConstants.RA_ITEMLIST_EBP);
        // check if there is not a query result
        if (itemList == null
                && queryStr != null
                && !advSearch
                &&
                // don't rexecute the statement if it is already prsent with the
                // same search criteria,
                // because than a new WebCatItemList would be created an
                // selected flags etc. on the existinmg itemlist
                // are lost.
                (theCatalog.getCurrentItemList() == null
                        || theCatalog.getCurrentItemList().getQuery() == null
                        || theCatalog.getCurrentItemList().getQuery().getStatement() == null
                        || theCatalog.getCurrentItemList().getQuery().getStatement().getStatementAsString() == null || !theCatalog.getCurrentItemList().getQuery().getStatement().getStatementAsString().equals(queryStr))) {
            try {
                IQueryStatement queryStmt = theCatalog.getCatalog().createQueryStatement();

                queryStmt.setStatementAsString(queryStr, 1, CatalogParameters.maxSearchResults);
                IQuery query = theCatalog.getCatalog().createQuery(queryStmt);
                log.debug("CatalogQueryAction: Query Stmt=" + queryStmt.toString());
                itemList = new WebCatItemList(theCatalog, query);
            }
            catch (CatalogException e) {
                log.error("system.backend.exception", e);
                itemList = new WebCatItemList(theCatalog);
            }
            theCatalog.setCurrentItemList(itemList);
        }

        if (theCatalog.getCurrentItemList() != null) {
            itemList = theCatalog.getCurrentItemList();
            request.setAttribute(ActionConstants.RA_ITEMLIST_EBP, itemList);
        }

        theCatalog.setLastVisited(WebCatInfo.CATALOG_QUERY);
        request.setAttribute(ActionConstants.RA_NEXT, "toItemPage_catalog");
        request.setAttribute(ActionConstants.RA_IS_QUERY, "yes");

        // set context value
        setContextValue(request, ActionConstants.CV_CURRENT_QUERY, queryStr);

        // argument for the title
        String[] args = { queryStr };
        String listTitleArg0 = WebUtil.translate(userData.getLocale(),
                "catalog.isa.queryFor",
                args);
        request.setAttribute(ActionConstants.RA_PRODUCT_LIST_TITLE_ARG0,
                listTitleArg0);

        if (theCatalog.getCurrentArea() != null) {
            request.setAttribute(ActionConstants.RA_AREA,
                    theCatalog.getCurrentArea());
            request.setAttribute(ActionConstants.RA_AREAKEY,
                    theCatalog.getCurrentArea().getAreaID());
            request.setAttribute(ActionConstants.RA_AREANAME,
                    theCatalog.getCurrentArea().getAreaName());
        }

        if (itemList != null && itemList.size() > 0)
            if (itemList.size() == 1) {
                WebCatItem item = itemList.getItem(0);
                item.refreshCatalogItem();
                theCatalog.setCurrentItem(item);
                theCatalog.getCurrentItem().readItemPrice();
                request.setAttribute(ReloadOneTimeAction.REQ_REDIRECT_FLUSH, "y");
                return mapping.findForward("onlyOne");
            }
            else {
                return mapping.findForward(ActionConstants.FW_SUCCESS);
            }
        else {
            return mapping.findForward(ActionConstants.FW_NO_ITEMS);
        }

    }
}