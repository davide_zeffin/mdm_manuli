package com.sap.isa.catalog.actions;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.sap.isa.businessobject.BusinessObjectException;
import com.sap.isa.businessobject.CommunicationException;
import com.sap.isa.catalog.webcatalog.WebCatArea;
import com.sap.isa.catalog.webcatalog.WebCatInfo;
import com.sap.isa.catalog.webcatalog.WebCatItemList;
import com.sap.isa.catalog.webcatalog.WebCatItemPage;
import com.sap.isa.core.UserSessionData;
import com.sap.isa.core.util.Message;


public class GetItemPageAction extends CatalogBaseAction {

    /**
     * Process the specified HTTP request, and create the corresponding HTTP
     * response (or forward to another web component that will create it).
     * Return an <code>ActionForward</code> instance describing where and how
     * control should be forwarded, or <code>null</code> if the response has
     * already been completed.
     *
     * @param mapping The ActionMapping used to select this instance
     * @param form The optional ActionForm bean for this request (if any)
     * @param request The HTTP request we are processing
     * @param response The HTTP response we are creating
     * @param userData container for session informations
     * @param theCatalog representation of the catalog
     *
     * @exception IOException if an input/output error occurs
     * @exception ServletException if a servlet exception occurs
     * 
     * @return specific action forward corresponding to the struts configuration
     */
    public ActionForward doPerform(ActionMapping mapping,
            ActionForm form,
            HttpServletRequest request,
            HttpServletResponse response,
            UserSessionData userData,
            WebCatInfo theCatalog)
            throws IOException, ServletException, CommunicationException {

        HttpSession theSession = request.getSession();

        log.debug("Entered to GetItemPageAction");
        String forward = null;

        //If not do it yourself if you can
        if (theCatalog == null) {
            //WebCatInfo.catalogLog.fatal("No Catalog Information availabale!");
            return (mapping.findForward(ActionConstants.FW_ERROR));
        }

        WebCatItemList myItemList = this.getScenarioDependentItemList(request, theCatalog);
        
        // no items could be determined
        if(myItemList == null) {
        	forward = ActionConstants.FW_NO_ITEMS;
        }


        //create itempage
        String thePageNumber = request.getParameter(ActionConstants.RP_PAGE_NUMBER);

        if (null == thePageNumber || thePageNumber.equals("")) {
            thePageNumber = getContextValue(request, ActionConstants.CV_PAGE_NUMBER);
        }

        //WebCatInfo.catalogLog.info("Get Page "+thePageNumber);
        if (myItemList != null && myItemList.getArea() != null) {
            theCatalog.setLastVisited(WebCatInfo.ITEMLIST);
        }

        if (myItemList != null && myItemList.getQuery() != null) {
            if (myItemList.getAreaQuery() != null) {
                theCatalog.setLastVisited(WebCatInfo.CATEGORY_QUERY);
            }
            else {
                theCatalog.setLastVisited(WebCatInfo.CATALOG_QUERY);
            }
        }

        String sortBy = (String) theSession.getAttribute(ActionConstants.RA_SORTBY);

        if ((sortBy != null) && !sortBy.equals("")) {
            myItemList.sort((String) sortBy);
        }

        theSession.removeAttribute(ActionConstants.RA_SORTBY);

		int listType = myItemList != null ? myItemList.getListType() : 0;
		
        int pageNumber;

        WebCatItemPage items = null;

        if (thePageNumber != null) {
            try {
                pageNumber = Integer.parseInt(thePageNumber);

                if (pageNumber <= 0) {
                    pageNumber = 1;
                    thePageNumber = "1";
                }

                items = myItemList.getItemPage(pageNumber, listType);
            }
            catch (java.lang.NumberFormatException e) {
                if (thePageNumber.equals(ActionConstants.IN_LAST_PAGELINK)) {
                    // page number 0 means last page when creating
                    items = myItemList.getItemPage(0, listType);
                    pageNumber = items.getPage(); // now it is the real page number!
                }
                else if (thePageNumber.equals(ActionConstants.IN_FIRST_PAGELINK)) {
                    pageNumber = 1;
                    items = myItemList.getItemPage(pageNumber, listType);
                }
            }
        }
        else {				
        	// set page numer to "1" because this should be the default (first) page
        	// (needed as well for bookmarking with context values)
        	thePageNumber = "1";        	
        }

        if (items == null) {
            if (myItemList.getCurrentItemPage() != null) {
                items = myItemList.getCurrentItemPage();
            }
            else {
                items = myItemList.getItemPage(1, listType);
            }
        }

        //ItemList items = new ItemList(theCatalog, theCatalog.getCurrentArea());
        request.setAttribute(ActionConstants.RA_PAGE_ITEMS, items);

        if (request.getParameter(ActionConstants.RA_IS_QUERY) != null) {
            request.setAttribute(ActionConstants.RA_IS_QUERY, request.getParameter(ActionConstants.RA_IS_QUERY));
        }

        if (theCatalog.getCurrentArea() != null) {
            request.setAttribute(ActionConstants.RA_AREA, theCatalog.getCurrentArea());
            request.setAttribute(ActionConstants.RA_AREAKEY, theCatalog.getCurrentArea().getAreaID());
            request.setAttribute(ActionConstants.RA_AREANAME, theCatalog.getCurrentArea().getAreaName());
        }
        else {
            WebCatArea rootArea = null;
            rootArea = theCatalog.getArea(WebCatArea.ROOT_AREA);

            if (rootArea == null) {
                log.error("catalog.area.notFound");

                Message msg = new Message(Message.ERROR, "catalog.exception.usermsg");
                BusinessObjectException boe = new BusinessObjectException("Catalog error", msg);
                request.setAttribute("Exception", boe);

                ArrayList path = new ArrayList();
                path.add("0");
                rootArea.setPath(path);
                request.setAttribute(ActionConstants.RA_AREA, rootArea);

                return mapping.findForward(ActionConstants.FW_ERROR);
            }
        }

        setContextValue(request, ActionConstants.CV_PAGE_NUMBER, thePageNumber);
        
        if (theCatalog.getLastVisited().equals(WebCatInfo.CATALOG_QUERY) &&
            items.size() == 0) {
            forward = ActionConstants.FW_NO_ITEMS;
        }
        else {
            forward = ActionConstants.FW_SUCCESS;
        }
        return (mapping.findForward(forward));
    }
    
}
