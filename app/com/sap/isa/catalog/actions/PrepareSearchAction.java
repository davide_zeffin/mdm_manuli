package com.sap.isa.catalog.actions;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.sap.isa.businessobject.BusinessObjectException;
import com.sap.isa.businessobject.CommunicationException;
import com.sap.isa.catalog.boi.CatalogException;
import com.sap.isa.catalog.boi.IAttribute;
import com.sap.isa.catalog.webcatalog.WebCatAreaAttribute;
import com.sap.isa.catalog.webcatalog.WebCatInfo;
import com.sap.isa.core.UserSessionData;
import com.sap.isa.core.util.Message;

public class PrepareSearchAction extends CatalogBaseAction {

    private class CompareOperation implements Comparator {

        public CompareOperation() {
        }

        public int compare(Object obj1, Object obj2) {

            WebCatAreaAttribute atr1 = (WebCatAreaAttribute) obj1;
            WebCatAreaAttribute atr2 = (WebCatAreaAttribute) obj2;

            Integer int1, int2;
            int1 = new Integer(atr1.getOrder());
            int2 = new Integer(atr2.getOrder());

            return int1.compareTo(int2);
        }
    }

    public ActionForward doPerform(
        ActionMapping mapping,
        ActionForm form,
        HttpServletRequest request,
        HttpServletResponse response,
        UserSessionData userData,
        WebCatInfo theCatalog)
        throws IOException, ServletException, CommunicationException {

        log.debug("Start PrepareSearchAction");

        ArrayList attributesList = new ArrayList();

        Iterator attributes;

        try {
            attributes = theCatalog.getCatalog().getAttributes();
        }
        catch (CatalogException e) {
            log.error("catalog.exception.backend", e);
            Message msg = new Message(Message.ERROR, "catalog.exception.usermsg");
            BusinessObjectException boe = new BusinessObjectException("Catalog error", msg);
            request.setAttribute("Exception", boe);
            return mapping.findForward(ActionConstants.FW_ERROR);
        }

        while (attributes.hasNext()) {
            IAttribute attribute = (IAttribute) attributes.next();
            WebCatAreaAttribute areaAttribute = new WebCatAreaAttribute(attribute);
            attributesList.add(areaAttribute);
        }

        Collections.sort(attributesList, new CompareOperation());

        request.setAttribute("catAttrList", attributesList);
        request.setAttribute("catalog", theCatalog);

        return mapping.findForward(ActionConstants.FW_SUCCESS);
    }
}
