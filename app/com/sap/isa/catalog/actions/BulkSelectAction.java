package com.sap.isa.catalog.actions;

import java.io.IOException;
import java.util.Collection;
import java.util.Iterator;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.sap.isa.businessobject.CommunicationException;
import com.sap.isa.catalog.webcatalog.WebCatInfo;
import com.sap.isa.catalog.webcatalog.WebCatItem;
import com.sap.isa.catalog.webcatalog.WebCatItemList;
import com.sap.isa.core.UserSessionData;

/**
 * Title:        BulkSelectAction
 * Description:  Selects (or un-selects) all the items from the current categorie
 * Copyright:    Copyright (c) 2001
 * Company:      SAPMarkets Europe GmbH
 * @version 1.0
 */

public class BulkSelectAction extends CatalogBaseAction {

    /**
     * Process the specified HTTP request, and create the corresponding HTTP
     * response (or forward to another web component that will create it).
     * Return an <code>ActionForward</code> instance describing where and how
     * control should be forwarded, or <code>null</code> if the response has
     * already been completed.
     *
     * @param mapping The ActionMapping used to select this instance
     * @param actionForm The optional ActionForm bean for this request (if any)
     * @param request The HTTP request we are processing
     * @param response The HTTP response we are creating
     *
     * @exception IOException if an input/output error occurs
     * @exception ServletException if a servlet exception occurs
     */

    public ActionForward doPerform(
        ActionMapping mapping,
        ActionForm form,
        HttpServletRequest request,
        HttpServletResponse response,
        UserSessionData userData,
        WebCatInfo theCatalog)
        throws IOException, ServletException, CommunicationException {

        log.debug("Entered to BulkSelectAction");
        String theAction = request.getParameter("next");

        WebCatItemList items;
        Iterator itemIterator;

        items = this.getScenarioDependentItemList(request, theCatalog);
        String detailScenario = this.getValueFromRequest(request, ActionConstants.RA_DETAILSCENARIO);
        String displayScenario = this.getValueFromRequest(request, ActionConstants.RA_DISPLAY_SCENARIO);
        if (theAction.equals("selectAll")) {
        	if(displayScenario.equals(ActionConstants.RA_CURRENT_QUERY) && detailScenario.equals(ActionConstants.DS_CUA)) {
        		itemIterator=items.iterator();
        	} else {
        		itemIterator=items.getCurrentItemPage().iterator();
        	}
        }
        else {
            itemIterator = items.iteratorOnlyPopulated();
        }

        while (itemIterator.hasNext()) {
            WebCatItem item = (WebCatItem) itemIterator.next();
            Collection subItems = item.getContractItems();
            if (subItems != null && subItems.size() != 0) {
                Iterator subItemsIterator = subItems.iterator();
                while (subItemsIterator.hasNext()) {
                    WebCatItem subItem = (WebCatItem) subItemsIterator.next();
                    if (theAction.equals("selectAll"))
                        subItem.setSelected(true);
                    if (theAction.equals("unselectAll"))
                        subItem.setSelected(false);
                }
            }
            if (theAction.equals("selectAll"))
                item.setSelected(true);
            if (theAction.equals("unselectAll"))
                item.setSelected(false);
        }

        if (request.getParameter("display_scenario").equals("products"))
            return mapping.findForward(ActionConstants.FW_SUCCESS);
        else {
            request.setAttribute("toNext", "toItemPage");
            request.setAttribute("isQuery", "yes");
            return mapping.findForward("query");
        }
    }
}
