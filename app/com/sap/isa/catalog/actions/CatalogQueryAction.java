package com.sap.isa.catalog.actions;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.sap.isa.businessobject.CommunicationException;
import com.sap.isa.catalog.CatalogParameters;
import com.sap.isa.catalog.boi.CatalogException;
import com.sap.isa.catalog.boi.IQuery;
import com.sap.isa.catalog.boi.IQueryStatement;
import com.sap.isa.catalog.webcatalog.WebCatInfo;
import com.sap.isa.catalog.webcatalog.WebCatItem;
import com.sap.isa.catalog.webcatalog.WebCatItemList;
import com.sap.isa.core.UserSessionData;
import com.sap.isa.core.util.WebUtil;
import com.sap.isa.isacore.action.EComBaseAction;

/**
 * Title:        ProductCatalog UI
 * Description:
 * Copyright:    Copyright (c) 2001
 * Company:      SAPMarkets Europe GmbH
 * @version 1.0
 */

public class CatalogQueryAction extends CatalogBaseAction {

    /**
     * Process the specified HTTP request, and create the corresponding HTTP
     * response (or forward to another web component that will create it).
     * Return an <code>ActionForward</code> instance describing where and how
     * control should be forwarded, or <code>null</code> if the response has
     * already been completed.
     *
     * @param mapping The ActionMapping used to select this instance
     * @param form The optional ActionForm bean for this request (if any)
     * @param request The HTTP request we are processing
     * @param response The HTTP response we are creating
     * @param userData container for session informations
     * @param theCatalog representation of the catalog
     *
     * @exception IOException if an input/output error occurs
     * @exception ServletException if a servlet exception occurs
     * 
     * @return specific action forward corresponding to the struts configuration
     */
    public ActionForward doPerform(ActionMapping mapping,
            ActionForm form,
            HttpServletRequest request,
            HttpServletResponse response,
            UserSessionData userData,
            WebCatInfo theCatalog)
            throws IOException, ServletException, CommunicationException {

        log.entering("CatalogQueryAction");

        // Page that should be displayed next.
        String forward = ActionConstants.FW_SUCCESS;
        
        //If not do it yourself if you can
        if (theCatalog != null) {
    
            String queryStr = request.getParameter(ActionConstants.RA_CURRENT_QUERY);
            if (null == queryStr && null != request.getAttribute(ActionConstants.RA_CURRENT_QUERY)) {
                queryStr = request.getAttribute(ActionConstants.RA_CURRENT_QUERY).toString();
            }
    
            // clear the saved item list
            theCatalog.setSavedItemList(null);
            
            WebCatItemList itemList = null;
    
            if (queryStr != null) {
                try {
                    IQueryStatement queryStmt = theCatalog.getCatalog().createQueryStatement();
                    //queryStmt.setStatementAsString(queryStr);
                    queryStmt.setStatementAsString(queryStr, 1, CatalogParameters.maxSearchResults);
    
                    IQuery query = theCatalog.getCatalog().createQuery(queryStmt);
                    log.debug("CatalogQueryAction: Query Stmt=" + queryStmt.toString());
                    // the following populateAll flag is set by the IcssCatalogEntryAction 
                    boolean populateAll = "true".equals(request.getAttribute(ActionConstants.RA_POPULATE_ALL));
                    itemList = new WebCatItemList(theCatalog, query, false, populateAll); 
                    theCatalog.setLastQuickSearchQueryString(queryStr);
                }
                catch (CatalogException e) {
                    log.error("system.backend.exception", e);
                    itemList = new WebCatItemList(theCatalog);
                }
                setContextValues(request, theCatalog, itemList);
                setContextValues(request, theCatalog, itemList);
            }
            forward = setForward(mapping, request, userData, theCatalog, itemList, queryStr);
        }
        else {
            //WebCatInfo.catalogLog.fatal("No Catalog Information availabale!");
            forward = ActionConstants.FW_ERROR;
        }

        log.exiting();
        return mapping.findForward(forward);
    }
    
    /**
     * Set the context attributes 
     * 
     * @param request The HTTP request we are processing
     * @param theCatalog representation of the catalog
     * @param itemlist representation of the WebCatItemList
     */
    protected void setContextValues(HttpServletRequest request,
                                    WebCatInfo theCatalog,
                                    WebCatItemList itemList) {
        log.entering("setContextValues(...)");
                                    
        log.debug("New query - reset page to be displayed to 1");
        if (getContextValue(request, ActionConstants.CV_PAGE_NUMBER) != null && 
            getContextValue(request, ActionConstants.CV_PAGE_NUMBER).length() >0) {
            changeContextValue(request, ActionConstants.CV_PAGE_NUMBER, "1");
        }
        else {
            setContextValue(request, ActionConstants.CV_PAGE_NUMBER, "1");
        }
 
        if (getContextValue(request, ActionConstants.CV_PAGE_SIZE) != null && 
            getContextValue(request, ActionConstants.CV_PAGE_SIZE).length() >0) {
            changeContextValue(request, ActionConstants.CV_PAGE_SIZE, "");
        }

        theCatalog.setCurrentItemList(itemList);
        theCatalog.resetCUAList();
//      theCatalog.setSavedItemList(itemList);
        request.setAttribute(ActionConstants.RA_ITEMLIST_EBP, itemList);
        
        setContextValue(request, ActionConstants.CV_ADV_SEARCH, "false");
        log.exiting();
     }
                                                                        
    /**
     * Set the attributes and forward
     * 
     * @param mapping The ActionMapping used to select this instance
     * @param request The HTTP request we are processing
     * @param userData container for session informations
     * @param theCatalog representation of the catalog
     * @param itemlist representation of the WebCatItemList
     * @param queryStr representation of the query string 
     * 
     * @return specific action forward corresponding to the struts configuration
     */
    protected String setForward(ActionMapping mapping,
                                HttpServletRequest request,
                                UserSessionData userData,
                                WebCatInfo theCatalog,
                                WebCatItemList itemList,
                                String queryStr) {
                                        
        log.entering("setForward(...)");
        String forward = ActionConstants.FW_SUCCESS;
        EComBaseAction.IsaActionMapping isaActionMapping = (EComBaseAction.IsaActionMapping)mapping;

        theCatalog.setLastVisited(WebCatInfo.CATALOG_QUERY);
        theCatalog.setLastUsedDetailScenario(WebCatInfo.CATALOG_QUERY);
        
        request.setAttribute(ActionConstants.RA_TO_NEXT, "toItemPage_catalog");
        request.setAttribute(ActionConstants.RA_DISPLAY_SCENARIO, ActionConstants.RA_CURRENT_QUERY);
        request.setAttribute(ActionConstants.RA_DETAILSCENARIO, ActionConstants.DS_CATALOG_QUERY);
        request.setAttribute(ActionConstants.RA_IS_QUERY, ActionConstants.RA_IS_QUERY_VALUE_YES);
        // argument for the title
        String[] args = { queryStr };
        String listTitleArg0 = WebUtil.translate(userData.getLocale(), "catalog.isa.queryFor", args);
        request.setAttribute(ActionConstants.RA_PRODUCT_LIST_TITLE_ARG0, listTitleArg0);
    
        String redirectToPageAlways = request.getParameter(ActionConstants.RA_QUERY_REDIRECT);
        if (null == redirectToPageAlways && null != request.getAttribute(ActionConstants.RA_QUERY_REDIRECT)) {
            redirectToPageAlways = request.getAttribute(ActionConstants.RA_QUERY_REDIRECT).toString();
        }
    
        if (itemList != null && itemList.size() > 0) {
            if (itemList.size() == 1 && isaActionMapping.isForwardDefined("onlyOne")
                    && redirectToPageAlways == null) {
                 
                WebCatItem item = itemList.getItem(0);
                item.refreshCatalogItem();
                theCatalog.setCurrentItem(item);
                theCatalog.getCurrentItem().readItemPrice();
                if(request.getAttribute(ActionConstants.RA_AREAKEY) != null){
                    request.removeAttribute(ActionConstants.RA_AREAKEY);
                }
                if (getContextValue(request, ActionConstants.CV_CURRENT_ITEM) != null) {
                    changeContextValue(request, ActionConstants.CV_CURRENT_ITEM, null);
                }
                forward = ActionConstants.FW_ONLYONE;
            }
            else {
                forward = ActionConstants.FW_SUCCESS;
            }
        }
        else {
            if (theCatalog.getCurrentArea() != null) {
                request.setAttribute(ActionConstants.RA_AREA, theCatalog.getCurrentArea());
                request.setAttribute(ActionConstants.RA_AREAKEY, theCatalog.getCurrentArea().getAreaID());
                request.setAttribute(ActionConstants.RA_AREANAME, theCatalog.getCurrentArea().getAreaName());
            }
            forward = ActionConstants.FW_NO_ITEMS;
        }
        
        log.exiting();
        return (forward);
    }
}