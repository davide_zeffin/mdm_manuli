package com.sap.isa.catalog.actions;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.sap.isa.businessobject.BusinessObjectException;
import com.sap.isa.businessobject.CommunicationException;
import com.sap.isa.catalog.boi.CatalogException;
import com.sap.isa.catalog.webcatalog.WebCatArea;
import com.sap.isa.catalog.webcatalog.WebCatAreaList;
import com.sap.isa.catalog.webcatalog.WebCatInfo;
import com.sap.isa.catalog.webcatalog.WebCatItem;
import com.sap.isa.catalog.webcatalog.WebCatWeightedArea;
import com.sap.isa.core.UserSessionData;
import com.sap.isa.core.util.Message;

/**
 * Title:        GetCategoriesB2CAction
 * Description:  Query the catalog about the categories who are childrens of the Current Area
 * Copyright:    Copyright (c) 2001
 * Company:      SAP AG, Germany
 * @version 1.0
 */

public class GetCategoriesB2CAction extends CatalogBaseAction {

    /**
     * Process the specified HTTP request, and create the corresponding HTTP
     * response (or forward to another web component that will create it).
     * Return an <code>ActionForward</code> instance describing where and how
     * control should be forwarded, or <code>null</code> if the response has
     * already been completed.
     *
     * @param mapping The ActionMapping used to select this instance
     * @param form The optional ActionForm bean for this request (if any)
     * @param request The HTTP request we are processing
     * @param response The HTTP response we are creating
     * @param userData container for session informations
     * @param theCatalog representation of the catalog
     *
     * @exception IOException if an input/output error occurs
     * @exception ServletException if a servlet exception occurs
     * 
     * @return specific action forward corresponding to the struts configuration
     */
    public ActionForward doPerform(ActionMapping mapping,
            ActionForm form,
            HttpServletRequest request,
            HttpServletResponse response,
            UserSessionData userData,
            WebCatInfo theCatalog)
            throws IOException, ServletException, CommunicationException {

        log.debug("Entered to GetCategoriesB2CAction");
        //If not do it yourself if you can
        if (theCatalog == null) {
            return (mapping.findForward("error"));
        }
        
        String forward = ActionConstants.FW_SUCCESS;

        if (request.getParameter(ActionConstants.RA_FORWARD) != null && request.getParameter(ActionConstants.RA_FORWARD).equals("account")) {
            forward = (String) request.getParameter(ActionConstants.RA_FORWARD);
        }

        String currentAreaId = getContextValue(request, ActionConstants.CV_CURRENT_AREA);
        if (currentAreaId != null) {
            WebCatArea currentArea = theCatalog.getArea(currentAreaId);
            if (currentArea.getAreaName() != null || currentArea.getAreaID().equals(WebCatArea.ROOT_AREA)) {
                if(theCatalog.getCurrentArea() != null && theCatalog.getCurrentArea().getAreaID() != null && ! theCatalog.getCurrentArea().getAreaID().equals(currentArea.getAreaID())) {
                    WebCatItem currentItem = theCatalog.getCurrentItem();
                    theCatalog.setCurrentArea(currentArea);
                    theCatalog.setCurrentItem(currentItem);
                }
            }
        }

        // start action.
        ArrayList pathList;
        ArrayList categoriesList;
        
        // if there is NOT a valid current area (category), use the root
        if (theCatalog.getCurrentArea() == null
            || theCatalog.getCurrentArea().getAreaName() == null
            || theCatalog.getCurrentArea().getAreaID().equals(WebCatArea.ROOT_AREA)) { //if I'm on the Root Category

            theCatalog.setCurrentArea(WebCatArea.ROOT_AREA);
        }
        else {
            /* Here starts the code to handle the go back */
            if (request.getParameter("back") != null && request.getParameter("back").equals("yes")) {
                
                WebCatArea cArea = theCatalog.getCurrentArea();
                if (!cArea.getAreaID().equals(WebCatArea.ROOT_AREA)) {
                    if (cArea.getParentArea() != null) {
                        theCatalog.setCurrentArea(cArea.getParentArea());
                    }    
                    else {
                        theCatalog.setCurrentArea(WebCatArea.ROOT_AREA);
                    }
                }
            }
        }
        
//        if (request.getAttribute("thePath") != null) {
//            pathList = (ArrayList) request.getAttribute("thePath");
//        }
//        else {
            pathList = theCatalog.getCurrentArea().getPath();
            if (pathList == null) {
              pathList = new ArrayList();
              pathList.add("0");
            }
//        }
        categoriesList = new ArrayList();
        
        try {
          makeList(0, categoriesList, theCatalog, pathList, false);
        }
        catch (CatalogException e) {
          log.error("catalog.exception.backend",e);
          Message msg = new Message(Message.ERROR,"catalog.exception.usermsg");
          BusinessObjectException boe = new BusinessObjectException("Catalog error",msg);
          request.setAttribute("Exception",boe);
          return mapping.findForward(ActionConstants.FW_ERROR);
        }

        setContextValue(request, ActionConstants.CV_CURRENT_AREA, theCatalog.getCurrentArea().getAreaID());
        
        if (theCatalog.getLastVisited() != null) {
            request.setAttribute("lastVisited", theCatalog.getLastVisited());
        }
        
        request.setAttribute(ActionConstants.RA_AREA, theCatalog.getCurrentArea());
        
        if (null != theCatalog.getCurrentArea()) {
            
            request.setAttribute(ActionConstants.RA_AREA, theCatalog.getCurrentArea());
            request.setAttribute(ActionConstants.RA_AREAKEY, theCatalog.getCurrentArea().getAreaID());
            
            request.setAttribute(ActionConstants.RA_AREANAME, theCatalog.getCurrentArea().getAreaName());
        }    
        else {
            
            request.setAttribute(ActionConstants.RA_AREAKEY, "");
            
            request.setAttribute(ActionConstants.RA_AREANAME, "");
        }

        request.setAttribute(ActionConstants.RA_AREATREE,categoriesList);

        return (mapping.findForward(forward));
    }
    
    
    private void makeList(int pos, ArrayList categoriesList,
                          WebCatInfo theCatalog, ArrayList pathList,
                          boolean isMinus) throws CatalogException {
                              
      Iterator childrenList;

      if (pos != 0) {
          WebCatArea tempArea = theCatalog.getArea((String)pathList.get(pos));
          if (tempArea.getChildren().size() == 0) return; {
              childrenList = tempArea.getChildren().iterator();
          }
      }
      else {
          childrenList=theCatalog.getRootCategories().iterator();
      }

      WebCatWeightedArea newArea;
      
      while (childrenList.hasNext()) {
        WebCatArea childrenArea = (WebCatArea)childrenList.next();
        if (childrenArea.getAreaID().equals("0")) continue;
        newArea = new WebCatWeightedArea();
        newArea.setArea(childrenArea);
        newArea.setPosition(pos);
        for (int i=0; i<=pos; i++) {
            synchronized (pathList) {
                if (i >= pathList.size()) {
                  log.error("An error has appeared in GetCategoriesTreeAction::makeList(" + pos + ")");
                  log.error("This happens on line: for (int i=0;i<=pos;i++) {");
                  log.error("And the conditions are: ");
                  log.error("i=" + i + " pos=" + pos);
                  log.error("pathList size is: " + pathList.size() + " and elements are:");
                  for(int qq=0; qq<pathList.size(); qq++) {
                      log.error("pathList["+qq+"]= " + (String)pathList.get(qq));
                  } 
                  log.error("current childrenArea is: " + childrenArea.getAreaName());
                } 
                else {
                  newArea.addPath((String)pathList.get(i));
                }
            }
        }
        newArea.addPath(newArea.getArea().getAreaID());
        newArea.setPath();
        if (pos < pathList.size()-1 && newArea.getArea().getAreaID().equals((String) pathList.get(pos+1))) { // we may go further to look for its childrens
            if (hasChildrens(newArea.getArea())) { //it has childrens to be shown
                newArea.setSign(WebCatWeightedArea.MINUS); //and therefore has a minus
            }
            else {
                newArea.setSign(WebCatWeightedArea.NOTHING); //no childrens, so, no sign.
            }    
        }
        else { //we are not going to look for its childrens
            if (hasChildrens(newArea.getArea())) { //it has childrens to be shown
                newArea.setSign(WebCatWeightedArea.PLUS); //but because we're not seeing them now, it has a plus
            }
            else {
                newArea.setSign(WebCatWeightedArea.NOTHING); //no childrens, so, no sign.
            }   
        }
        if (isMinus && pos == pathList.size()-2 && newArea.getSign() == WebCatWeightedArea.MINUS) {
            newArea.setSign(WebCatWeightedArea.PLUS);
        } 
        categoriesList.add(newArea);
        if (!(isMinus && pos == pathList.size()-2)) {
            if (pos < pathList.size()-1 && newArea.getArea().getAreaID().equals((String) pathList.get(pos+1))) {
                makeList(pos+1, categoriesList, theCatalog, pathList, isMinus);
            }
        }        
      } // while
    }

    
    public boolean hasChildrens(WebCatArea area) throws CatalogException {
        if (true)  {
            return hasChildrensRealCall(area);
        }   
        else {
            return hasChildrensAttrib(area);
        } 
    }

    protected boolean hasChildrensAttrib(WebCatArea area){
        return  (area.getDetail("SUBAREAS_EXIST") != null && area.getDetail("SUBAREAS_EXIST").length() != 0);
    }
    
    /**
     * This function does a real call to the Trex  to check if the 
     * required area has childrens in the current View.
     * @param area
     * @return boolean
     */
    protected boolean hasChildrensRealCall(WebCatArea area) throws CatalogException {
        return (area.getChildren().size() != 0 ); //it has childrens to be shown
    }
}