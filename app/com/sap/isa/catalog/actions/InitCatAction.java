package com.sap.isa.catalog.actions;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.sap.isa.backend.boi.webcatalog.CatalogBusinessObjectsAware;
import com.sap.isa.backend.boi.webcatalog.CatalogConfiguration;
import com.sap.isa.backend.boi.webcatalog.CatalogUser;
import com.sap.isa.businessobject.Address;
import com.sap.isa.businessobject.CommunicationException;
import com.sap.isa.businesspartner.backend.boi.PartnerFunctionData;
import com.sap.isa.businesspartner.businessobject.BusinessPartner;
import com.sap.isa.businesspartner.businessobject.BusinessPartnerManager;
import com.sap.isa.catalog.webcatalog.WebCatInfo;
import com.sap.isa.core.Constants;
import com.sap.isa.core.RequestProcessor;
import com.sap.isa.core.UserSessionData;
import com.sap.isa.core.ui.context.GlobalContextManager;
import com.sap.isa.user.util.UserUnSecureConnectionEventHandler;

/*****************************************************************************
	Class         InitCatAction
	Copyright (c) 2000, SAPMarkets Europe GmbH, Germany, All rights reserved.
	Description:  Catalog specific initializations
	Author:
	Created:      23.04.2001
	Version:      1.0

	$Revision: #4 $
	$Date: 2002/03/21 $
*****************************************************************************/

/**
 * Performs all initializations specific to the B2C scenario.
 */
public class InitCatAction extends CatalogBaseAction {

	/**
	 * Initialize Context values. <br>
	 */
	static public void initContextValues() {
		GlobalContextManager.registerValue(
			Constants.CV_DOCUMENT_KEY,
			true,
			false);
		GlobalContextManager.registerValue(
			Constants.CV_DOCUMENT_TYPE,
			true,
			false);
		GlobalContextManager.registerValue(
			Constants.CV_BASKET_ANCHOR_KEY,
			false,
			false);
		GlobalContextManager.registerValue(
			Constants.CV_ANCHOR_IS_CFG,
			false,
			false);
		GlobalContextManager.registerValue(
			Constants.CV_1STSCCALL,
			false,
			false);
	}

	/**
	 * Performs all initializations specific to the B2C scenario.
	 *
	 */
	public ActionForward doPerform(
		ActionMapping mapping,
			ActionForm form,
			HttpServletRequest request,
			HttpServletResponse response,
			UserSessionData userData,
			WebCatInfo theCatalog)
		throws CommunicationException {

		// next page to be displayed
		final String METHOD_NAME = "doPerform()";
		log.entering(METHOD_NAME);
		String forwardTo = null;

        CatalogBusinessObjectsAware cataConfBom =  getCatalogObjectAwareBom(userData);
        
		CatalogUser user = cataConfBom.getCatalogUser();
        CatalogConfiguration catConfig = cataConfBom.getCatalogConfiguration();
        
        catConfig.setStandaloneCatalog(true);
        catConfig.setShowCatATP(true);
        catConfig.setShowCatSpecialOffers(true);
		catConfig.setShowCatQuickSearchInCat(false);
		catConfig.setShowCatQuickSearchInNavigationBar(true);
        
		if (user == null) {
			// create an instance of the user
			user = getCatalogObjectAwareBom(userData).createCatalogUser();
		}

		BusinessPartnerManager buPaMa = getCatalogObjectAwareBom(userData).createBUPAManager();
		BusinessPartner soldTo =
			buPaMa.getDefaultBusinessPartner(PartnerFunctionData.SOLDTO);
		BusinessPartner contact =
			buPaMa.getDefaultBusinessPartner(PartnerFunctionData.CONTACT);

		Address address = null;
		String welcomeText = null;

		// Determine whether the user is a person or represents an organisation.
		// The user represents a person if the lastname of the soldTo is
		// non-empty, otherwise it's an organization
		if (contact != null
			&& contact.getAddress() != null
			&& !contact.getAddress().getName().equals("")) {
			address = contact.getAddress();
		} else if (
			soldTo != null
				&& soldTo.getAddress() != null
				&& !soldTo.getAddress().getName().equals("")) {
			address = soldTo.getAddress();
		}

		if (address != null) {
			String title = address.getTitle();
			String lastName = address.getName();
			welcomeText = title + " " + lastName;
		} else if (
			user.getSalutationText() != null
				&& user.getSalutationText().length() > 0) {
			welcomeText = user.getSalutationText();
		}

		if (welcomeText != null) {
			request.getSession().setAttribute(
			ActionConstants.WELCOME_TEXT,
				welcomeText);
		}

		if (userData.getAttribute(ActionConstants.LOGIN_STATUS) == null) {
			userData.setAttribute(
			ActionConstants.LOGIN_STATUS,
				"BEFORE_LOGIN");
		}

		if (!user.isUserLogged()) {
			// Set the markting user as unknown user  
			user.getMktPartner().setKnown(false);
		}

		// set the frame combination in the mainFS frame set.
		userData.setAttribute(ActionConstants.FRAME_NAME, "INITIAL");

		RequestProcessor.getSecureConnectionChecker().registerEventHandler(
			new UserUnSecureConnectionEventHandler());

		// forward to the navigation page
		forwardTo = "main";
		log.exiting();
		return mapping.findForward(forwardTo);
	}

}
