package com.sap.isa.catalog.actions;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.sap.isa.businessobject.CommunicationException;
import com.sap.isa.catalog.webcatalog.WebCatArea;
import com.sap.isa.catalog.webcatalog.WebCatInfo;
import com.sap.isa.core.UserSessionData;

public class GetCatalogCurrentAreaAction extends CatalogBaseAction {

    public ActionForward doPerform(
        ActionMapping mapping,
        ActionForm form,
        HttpServletRequest request,
        HttpServletResponse response,
        UserSessionData userData,
        WebCatInfo theCatalog)
        throws IOException, ServletException, CommunicationException {

        ActionForward nextPage = null;

        WebCatArea currentArea = theCatalog.getCurrentArea();
        if (currentArea == null) {
            nextPage = mapping.findForward(ActionConstants.FW_ERROR);
        }

        else {
            request.setAttribute(ActionConstants.RA_WEBCATAREA, currentArea);
            nextPage = mapping.findForward(ActionConstants.RA_FORWARD);
            if (nextPage == null) {
                nextPage = mapping.findForward(ActionConstants.FW_SUCCESS);
            }
        }

        log.debug("Forwarding to " + nextPage.getName() + " = " + nextPage.getPath());

        return nextPage;
    }
}