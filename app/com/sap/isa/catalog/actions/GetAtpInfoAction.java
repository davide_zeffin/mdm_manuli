package com.sap.isa.catalog.actions;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.sap.isa.backend.boi.webcatalog.CatalogConfiguration;
import com.sap.isa.businessobject.webcatalog.CatalogBusinessObjectManager;
import com.sap.isa.businessobject.webcatalog.atp.AtpResultList;
import com.sap.isa.catalog.webcatalog.WebCatInfo;
import com.sap.isa.catalog.webcatalog.WebCatItem;
import com.sap.isa.core.UserSessionData;

/**
 * Title:
 * Description:
 * Copyright:    Copyright (c) 2001
 * Company:
 * @author
 * @version 1.0
 */

public class GetAtpInfoAction extends CatalogBaseAction {
    
    public ActionForward doPerform(
        ActionMapping mapping,
        ActionForm form,
        HttpServletRequest request,
        HttpServletResponse response,
        UserSessionData userSessionData,
        WebCatInfo theCatalog) {

        CatalogBusinessObjectManager bom =
                      (CatalogBusinessObjectManager)userSessionData.getBOM(CatalogBusinessObjectManager.CATALOG_BOM);
         
        WebCatItem currentItem;
    
        if (request.getParameter("scenario") != null &&
            request.getParameter("scenario").equals("detailInWindow") &&
            request.getAttribute("currentItem") != null) {
            currentItem = (WebCatItem)request.getAttribute("currentItem");
        } 
        else {
            currentItem = theCatalog.getCurrentItem();
        } 
    
        if (currentItem == null) {
    	  log.debug("No Item selected ! ABORT !");
          return (mapping.findForward(ActionConstants.FW_SUCCESS));
        }
    
        //do not start an ATP check if there is some errors with the current item (e.g. : incorrect quantity)
        if(currentItem.getMessageList() != null && currentItem.getMessageList().size()>0){
			log.debug("Item has some errors ! ABORT !");
			return (mapping.findForward(ActionConstants.FW_SUCCESS));
        }
    
    	if (log.isDebugEnabled()) {
            log.debug("currentItem= " + currentItem.getProduct() + " / " +currentItem.getProductID());
    	}
        
        AtpResultList atpResultList = determineATPResult(userSessionData, bom, currentItem, log);
        
        if (atpResultList == null) {
            log.debug("atpResult is null !! Error ! Aborting !");
            return (mapping.findForward(ActionConstants.FW_SUCCESS)); //problems with ATP
        }
        
        if (currentItem.getAtpResultList() != null) {
            request.setAttribute("atpList", currentItem.getAtpResultList().iterator()); 
        }
        
        if (request.getParameter("scenario") != null && request.getParameter("scenario").equals("detailInWindow")) {
            return mapping.findForward("detailInWindow");
        }
        
		CatalogConfiguration catConf = getCatalogConfiguration(userSessionData);
        
        if (catConf.isSuppCatEventCapturing()) {
	        return (mapping.findForward(ActionConstants.FW_CAPTURE));
        }
            
        return (mapping.findForward(ActionConstants.FW_SUCCESS));
  }
}
