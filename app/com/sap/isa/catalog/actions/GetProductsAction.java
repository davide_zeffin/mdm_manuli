package com.sap.isa.catalog.actions;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.sap.isa.catalog.AttributeKeyConstants;
import com.sap.isa.catalog.webcatalog.WebCatArea;
import com.sap.isa.catalog.webcatalog.WebCatInfo;
import com.sap.isa.catalog.webcatalog.WebCatItemList;
import com.sap.isa.core.UserSessionData;
import com.sap.isa.core.util.WebUtil;

/**
 * Title:        GetProductsAction Description:  Creates a page of products, and stores it inside the request, such
 * that to be able to be displayed by a JSP. The created page is either the first page for a newly selected categorie,
 * or the indicated sub-page, if the user wants to browse through a categorie, whose products lies on several
 * sub-pages. This class's <code>doPerform</code> method is also called when the user selects a new upper limit for
 * the number of items that may be displayed inside a sub-page. Copyright:    Copyright (c) 2001 Company:
 * SAPMarkets Europe GmbH
 *
 * @version 1.0
 */
public class GetProductsAction extends CatalogBaseAction {
    /**
     * Process the specified HTTP request, and create the corresponding HTTP response (or forward to another web
     * component that will create it). Return an <code>ActionForward</code> instance describing where and how control
     * should be forwarded, or <code>null</code> if the response has already been completed.
     *
     * @param mapping The ActionMapping used to select this instance
     * @param form The optional ActionForm bean for this request (if any)
     * @param request The HTTP request we are processing
     * @param response The HTTP response we are creating
     * @param userData DOCUMENT ME!
     * @param theCatalog DOCUMENT ME!
     *
     * @return DOCUMENT ME!
     *
     * @exception IOException if an input/output error occurs
     * @exception ServletException if a servlet exception occurs
     */
    public ActionForward doPerform(ActionMapping mapping, ActionForm form, HttpServletRequest request,
                                   HttpServletResponse response, UserSessionData userData, WebCatInfo theCatalog)
                            throws IOException, ServletException {
        

        if (theCatalog == null) {
            log.error("No Catalog Information availabale!");

            return (mapping.findForward("error"));
        }

        String theCategoryId = request.getParameter(ActionConstants.RA_AREAKEY);

        if (theCategoryId != null) {
            // make this area the current one
            theCatalog.setCurrentArea(theCategoryId);
            // WebCatInfo.catalogLog.info("Get Products of area "+theCategoryId);
        }
        else if (theCatalog.getCurrentArea() == null) {
            theCatalog.setCurrentArea(WebCatArea.ROOT_AREA);
        }     
  
		String rSearch = request.getParameter(ActionConstants.RESET_SEARCH);
		boolean resetSearch = false;
		if (rSearch != null && rSearch.equals("true")) {
			resetSearch = true;
		}
        
		theCatalog.setCurrentItem(null);
		theCatalog.resetCUAList();

		WebCatItemList myItemList = theCatalog.getCurrentItemList();
		if (myItemList == null || myItemList.size() == 0 || resetSearch) {
			myItemList = theCatalog.getCurrentArea().getItemList();
		}
        
		theCatalog.setCurrentItemList(myItemList);
		
		if (myItemList == null) {
            return (mapping.findForward(ActionConstants.FW_NO_ITEMS));
        }

        request.setAttribute(ActionConstants.RA_ITEMLIST_EBP, myItemList);

        // argument for the title
        String args[] = { theCatalog.getCurrentArea().getAreaName() };
        String listTitleArg0 = WebUtil.translate(userData.getLocale(), "b2c.cat.b2ccatarea", args);
        request.setAttribute(ActionConstants.RA_PRODUCT_LIST_TITLE_ARG0, listTitleArg0);

        if (myItemList.getCurrentItemPage() == null && myItemList.size() > 0) {
            // following line changed from first to last item, as the new items which
            // do not have catalog attributes yet are added in the beginning of the
            // list...
            if (myItemList.getItem(myItemList.size() - 1).getAttributeByKey(AttributeKeyConstants.LINE_NUMBER) != null) {
                //myItemList.sort(AttributeKeyConstants.LINE_NUMBER); 
                //This is default for IMS ("POS_NR").
            }
            else if (myItemList.getItem(myItemList.size() - 1).getAttribute("Description") != null) {
                myItemList.sort("Description");
            }
        }

        theCatalog.setLastVisited(WebCatInfo.ITEMLIST);
        theCatalog.setLastUsedDetailScenario(WebCatInfo.ITEMLIST);

        return mapping.findForward(ActionConstants.FW_SUCCESS);
    }
}
