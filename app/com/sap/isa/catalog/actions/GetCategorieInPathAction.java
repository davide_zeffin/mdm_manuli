package com.sap.isa.catalog.actions;

import java.io.IOException;
import java.util.ArrayList;
import java.util.StringTokenizer;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.sap.isa.businessobject.CommunicationException;
import com.sap.isa.catalog.webcatalog.WebCatArea;
import com.sap.isa.catalog.webcatalog.WebCatInfo;
import com.sap.isa.core.UserSessionData;
import com.sap.isa.core.util.WebUtil;

/**
 * Title:        GetCategorieInPathAction
 * Description:  Parses the Input parameter
 *               creates a vector of Strings (ie the categories contained in the path)
 *               set the current Area, as the area indicated as the last categorie from the request
 *               set current Area's path as the previously obtained vector
 *               forward the control to a specified JSP.
 * Copyright:    Copyright (c) 2001
 * Company:      SAPMarkets Europe GmbH
 * @version 1.0
 */

public class GetCategorieInPathAction extends CatalogBaseAction {
    /**
     * Process the specified HTTP request, and create the corresponding HTTP
     * response (or forward to another web component that will create it).
     * Return an <code>ActionForward</code> instance describing where and how
     * control should be forwarded, or <code>null</code> if the response has
     * already been completed.
     *
     * @param mapping The ActionMapping used to select this instance
     * @param form The optional ActionForm bean for this request (if any)
     * @param request The HTTP request we are processing
     * @param response The HTTP response we are creating
     * @param userData container for session informations
     * @param theCatalog representation of the catalog
     *
     * @exception IOException if an input/output error occurs
     * @exception ServletException if a servlet exception occurs
     * 
     * @return specific action forward corresponding to the struts configuration
     */
    public ActionForward doPerform(ActionMapping mapping,
            ActionForm form,
            HttpServletRequest request,
            HttpServletResponse response,
            UserSessionData userData,
            WebCatInfo theCatalog)
            throws IOException, ServletException, CommunicationException {

        log.debug("Entered to GetCategorieInPathAction");

        //If not do it yourself if you can
        if (theCatalog == null) {
            //WebCatInfo.catalogLog.fatal("No Catalog Information availabale!");
            return (mapping.findForward("error"));
        }

        // Reset saved item list form quick search
        theCatalog.setSavedItemList(null);
        
        int listType = 0;
        if (theCatalog.getCurrentItemList() != null) {
            listType = theCatalog.getCurrentItemList().getListType();
        }

        String categoriePath = request.getParameter("key");
        if (categoriePath == null) {
            categoriePath = request.getParameter("keyFullTree");
        }
        ArrayList theCategories = new ArrayList();

        if (categoriePath == null) {
            theCategories.add("0"); //adds the root to the path
        }
        else {
            StringTokenizer strToken = new StringTokenizer(categoriePath, "/");
            while (strToken.hasMoreTokens()) {
                theCategories.add(strToken.nextToken()); //add all the categories from the input string
            }
        }

        if (request.getParameter("type") != null
                && (request.getParameter("type").equals("minus") || request.getParameter("type").equals("plus"))) {
            
            request.setAttribute("thePath", theCategories);
        }
        else if (theCatalog.getCurrentArea() == null
                || (((String) theCategories.get(theCategories.size() - 1)).equals(theCatalog.getCurrentArea().getAreaID())
                        && theCatalog.getLastVisited() != null && (theCatalog.getLastVisited().equals("recommendations") || theCatalog.getLastVisited().equals("bestseller")))
                || !((String) theCategories.get(theCategories.size() - 1)).equals(theCatalog.getCurrentArea().getAreaID())
                || theCatalog.getCurrentItemList() == null
                || theCatalog.getCurrentItemList().getAreaQuery() != null
                || theCatalog.getCurrentItemList().getQuery() != null) {
            
            for (int i = 0; i < theCategories.size(); i++) {
                if (((String) theCategories.get(i)).equals("0")) {
                    theCatalog.setCurrentArea(WebCatArea.ROOT_AREA);
                }
                else {
                    //set current area with respect of the requested path
                    theCatalog.setCurrentArea((String) theCategories.get(i));
                    theCatalog.setLastUsedDetailScenario(WebCatInfo.AREADETAILS);
                }
            }
            
            log.debug("New Area - reset page to be displayed to 1");
            if (getContextValue(request, ActionConstants.CV_PAGE_NUMBER) != null && 
                getContextValue(request, ActionConstants.CV_PAGE_NUMBER).length() >0) {
                changeContextValue(request, ActionConstants.CV_PAGE_NUMBER, "1");
            }
            else {
                setContextValue(request, ActionConstants.CV_PAGE_NUMBER, "1");
            }
            
            log.debug("Reset context value for page size if webCatItemList has different listType");
            if (listType != ActionConstants.CV_LISTTYPE_CATALOG_AREA) {
                if (getContextValue(request, ActionConstants.CV_PAGE_SIZE) != null && 
                    getContextValue(request, ActionConstants.CV_PAGE_SIZE).length() > 0) {
                    changeContextValue(request, ActionConstants.CV_PAGE_SIZE, "");
                }
            }
            
            theCatalog.getCurrentArea().setPath(theCategories);
            // argument for the title
            String[] args = { theCatalog.getCurrentArea().getAreaName() };
            String listTitleArg0 = WebUtil.translate(userData.getLocale(), "b2c.cat.b2ccatarea", args);
            request.setAttribute(ActionConstants.RA_PRODUCT_LIST_TITLE_ARG0, listTitleArg0);
        }

        if (request.getParameter("type") != null) {
            if (request.getParameter("type").equals("Full")) {
                return (mapping.findForward("form_input_frame")); //needed for the javascript version
            }
            if (request.getParameter("type").equals("minus")) {
                return (mapping.findForward("organizer_content_frame"));
            }
            if (request.getParameter("type").equals("plus")) {
                return (mapping.findForward("organizer_content_frame"));
            }
        }

        request.setAttribute("type", "itemList");
        
        theCatalog.setProductDetailListType(0);

        if (theCatalog.getCurrentArea() != null) {
            setContextValue(request, ActionConstants.CV_CURRENT_AREA, theCatalog.getCurrentArea().getAreaID());
        }

        return (mapping.findForward(ActionConstants.FW_SUCCESS));
    }
}