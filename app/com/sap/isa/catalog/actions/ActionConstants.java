/*****************************************************************************
    Class:        ActionConstants
    Copyright (c) 2001, SAPMarkets Europe GmbH, Germany, All rights reserved.
    Created:      May 2001
    Version:      1.0

    $Revision: #4 $
    $Date: 2003/05/27 $
*****************************************************************************/

package com.sap.isa.catalog.actions;


/**
 * this class defines some constants for request attributes (RA) which are used
 * in actions to store and retrieve data from request context,
 * some forward name (FW) which are used for forwarding in actions, and
 * some initial parameters names (IN) which identify parameters used in web.xml.
 * If one constant cannot be found, then check the class {@link com.sap.isa.isacore.action.ActionConstants}
 * in DC crm/isa/isacore.
 */
public class ActionConstants {

  /***************************************************************************
   * Request Parameters
   ***************************************************************************/
  /**
   * page displayed number. The value is <b>page</b>.
   */
  public final static String RP_PAGE_NUMBER          = "page";
  /**
   * page displayed size. The value is <b>itemPageSize</b>.
   */
  public final static String RP_PAGE_SIZE            = "itemPageSize";
  /***************************************************************************
   * Request Attributes
   ***************************************************************************/
  /**
   * key to store area key in request context. The value is <b>currentArea</b>.
   */
  public final static String RA_AREA                 = "currentArea";
  /**
   * key to store area key in request context. The value is <b>areakey</b>.
   */
  public final static String RA_AREAKEY              = "areakey";

  /**
   * key to store area name in request context
   * title parameter for the layout framework
   * resource key b2c.cat.b2ccatarea
   * The value is <b>areaname</b>.
   */
  public final static String RA_AREANAME             = "areaname";
  
  /**
   * key to store area tree in request context
   */
  public final static String RA_AREATREE             = "categoriesTree";
  
  /**
   * key to store forward to call from different Basket actions as follow up.
   * The value is <b>followUpAction</b>.
   */
  public final static String RA_BAKET_FOLLOW_UP_ACTION   = "followUpAction";
  
  /**
   * key to store item key in request context.
   * The value is <b>itemkey</b>.
   */
  public final static String RA_ITEMKEY              = "itemkey";
  
  /**
   * Key to store top item in scenarios where Solution 
   * Configurator will be used. 
   * In this case the constant RA_ITEMKEY will be used for 
   * the representation of the sub-item.  
   * The value is <b>topItemkey</b>.
   */
  public final static String RA_TOP_ITEMKEY       = "topItemkey";

  /**
   * key to store item name in request context
   * title parameter for the layout framework
   * resource key catalog.isa.productTitle
   * The value is <b>itemname</b>.
   */
  public final static String RA_ITEMNAME              = "itemname";

  /**
   * key to store ItemDetailRequest in request context.
   * The value is <b>itemRequest</b>.
   */
  public final static String RA_ITEMREQUEST          = "itemRequest";

  /**
   * key to store WebCatItem in request context.
   * The value is <b>webCatItem</b>.
   */
  public final static String RA_WEBCATITEM           = "webCatItem";
  
  /**
   * The current messages in the request. The value is <b>messages</b>.
   */
  public final static String RC_MESSAGES             = "messages";
  
  /**
   * key to store WebCatItem in request context, coming from the ShowProductDetailAction.
   * The value is <b>currentItem</b>.
   */
  public final static String RA_SHOW_PROD_DET_ITEM     = "currentItem";
  public final static String RC_WEBCATITEM             = "currentItem";

  /**
   * key to store teh forward of the login action to go to, after
   * the catalogue camapign has been checked again.
   * The value is <b>loginCampCheckFwd</b>.
   */
  public final static String RA_LOGIN_CAMPAIGN_CHECK_FWD  = "loginCampCheckFwd";

  /**
   * key to store Campaign in request context.
   * The value is <b>campaign</b>.
   * @deprecated This value is deprecated. Please use {@link ActionConstants#RC_CAMPAIGN}.
   */
  public final static String RA_CAMPAIGN       		  = "campaign";

  /**
   * key to store Campaign in request context. The current value is <b>campaign</b>.
   */
  public final static String RC_CAMPAIGN              = "campaign";
  
  /**
   * key to store Campaign Page in request context. The current value is <b>campaignpage</b>.
   */
  public final static String RC_CAMPAIGN_PAGE        = "campaignpage";
  
  /**
   * key to store Campaign Enroll List Page in request context. The current value is <b>enrollmListPage</b>.
   */
  public final static String RC_CAMPAIGN_ENROLLM     = "enrollmListPage";
  
  /**
   * key to store Campaign ID in request context. The current value is <b>campaignId</b>.
   */
  public final static String RC_CAMPAIGN_ID          = "campaignId";
  
  /**
   * key to store BP search in request context. The current value is <b>searchBpHier</b>.
   */
  public final static String RC_SEARCHBPHIER         = "searchBpHier";
  
  /**
   * key to store Enroller ID in request context. The current value is <b>enrollerId</b>.
   */
  public final static String RC_ENROLLER             = "enrollerId";
  
  /**
   * key to store Enrollee ID in request context. The current value is <b>enrolleeId</b>.
   */
  public final static String RC_ENROLLEE             = "enrolleeId";
  
  /**
   * key to store WebCatItemPage in request context. The current value is <b>webCatItemPage</b>.
   */  
  public final static String RA_WEBCATITEMPAGE       = "webCatItemPage";

  /**
   * key to store WebCatArea in request context. The current value is <b>webCatArea</b>.
   */
  public final static String RA_WEBCATAREA           = "webCatArea";

  /**
   * key to store WebCatItemList in request context. The current value is <b>webCatItemList</b>.
   */
  public final static String RA_WEBCATITEMLIST       = "webCatItemList";

  /**
   * key to store item list for EBP in request context. The current value is <b>itemList</b>.
   */
  public final static String RA_ITEMLIST_EBP         = "itemList";

  /**
   * key to store item list for the page in request context.
   * The value is <b>itemPage</b>.
   */
  public final static String RA_PAGE_ITEMS           = "itemPage";

  /**
   * key to store item list for ISA in request context.
   * The value is <b>baskettransferitemlist</b>.
   */
  public final static String RA_ITEMLIST_ISA         = "baskettransferitemlist";

  /**
   * key to store contract key in request context.
   * The value is <b>contractkey</b>.
   */
  public final static String RA_CONTRACTKEY          = "contractkey";

  /**
   * key to store contract item key in request context.
   * The value is <b>contractitemkey</b>.
   */
  public final static String RA_CONTRACTITEMKEY      = "contractitemkey";

  /**
   * key to store contract item key in request context.
   * The value is <b>contractDisplayMode</b>.
   */
  public final static String RA_CONTRACTDISPLAYMODE  = "contractDisplayMode";

  /**
   * key to store oci information in request context.
   * The value is <b>ociInfo</b>.
   */
  public final static String RA_OCIINFO              = "ociInfo";

  /**
   * key to store an item to be transferred to an external order in the
   * standalone catalog. If the attribute is set, the item shall be
   * transferred to the order.
    * The value is <b>transferToOrderItem</b>.
  */
  public final static String RA_TRANSFERTOORDERITEM  = "transferToOrderItem";
  /**
   * key to store reference to IPC item in request context (for configuration).
   * The value is <b>ipcItemReference</b>.
   */
  public final static String RA_IPC_ITEM_REFERENCE   = "ipcItemReference";

  /**
   * key to tell item list whether price analysis is enabled.
   * The value is <b>priceCalcInitData</b>.
   */
  public final static String RA_PRICECALC_INIT_DATA  = "priceCalcInitData";

  /**
   * key to store list of soldTos in request for soldTo selection.
   * The value is <b>soldToList</b>.
   */
  public final static String RA_SOLDTO_LIST  = "soldToList";

  /**
   * key to store soldTo in request for soldTo selection.
   * The value is <b>soldTo</b>.
   */
  public final static String RA_SOLDTO       = "soldTo";

  /**
   * key to store soldTo guid in request for soldTo selection.
   * The value is <b>soldToKey</b>.
   */
  public final static String RA_SOLDTO_KEY   = "soldToKey";

  /**
   * key to store the index in current item list. The value is <b>PRODUCT_INDEX</b>.
   */
  public final static String RA_PRODUCT_INDEX        = "PRODUCT_INDEX";

  /**
   * key to store a forward name in request context.
   * This is used to route a forward information through UpdateItemsFromRequest
   * action.
    * The value is <b>next</b>.
  */
  public final static String RA_NEXT                 = "next";
  
  /**
   * This is used to route a forward information through catalog navigation.
   * The value is <b>toNext</b>.
   */
  public final static String RA_TO_NEXT                 = "toNext";

  /**
   * key to store the object which has to be moved.
   * This is used in sharedcatalog scenario to move items or areas.
   * The value is <b>objectToMove</b>.
   */
  public final static String RA_OBJECT_TO_MOVE       = "objectToMove";
  
  /**
   * key to store the target area where an area or item will be moved to.
   * This is used in sharedcatalog scenario to move items or areas.
   * The value is <b>moveTargetArea</b>.
   */
  public final static String RA_MOVE_TARGET_AREA     = "moveTargetArea";
  
  /**
   * is a query to display. The value is <b>isQuery</b>.
  */
  public final static String RA_IS_QUERY        = "isQuery";
  
  /**
   * The value if a query has to be displayed. The value is <b>yes</b>.
   */
  public final static String RA_IS_QUERY_VALUE_YES  = "yes";
  
  /**
   * query text to search catalog. The value is <b>query</b>.
   */
  public final static String RA_CURRENT_QUERY        = "query";
  
  /**
   * key to store the campaign code in request context. The value is <b>campaigncode</b>.
   */
  public final static String RA_CAMPAIGN_CODE        = "campaigncode";
  
  /**
   * query redirect to list. The value is <b>redirecttolist</b>.
   */
  public final static String RA_QUERY_REDIRECT       = "redirecttolist";

  /**
   * key to store the title argument for the product list page.
   * title parameter for the layout framework
   * resource key catalog.isa.productListTitle.
   * The value is <b>productListTitleArg0</b>.
  */
  public final static String RA_PRODUCT_LIST_TITLE_ARG0 = "productListTitleArg0";
  
  /**
   * A parameter used for sorting in the catalog. The value is <b>sortBy</b>.
   */
  public final static String RA_SORTBY  	 = "sortBy";
  
  /***************************************************************************
   * Used by ICSS, to indicate, that complete search result list should be populated.
   * The value is <b>populateAll</b>.
   */
  public static final String RA_POPULATE_ALL = "populateAll";
  
  /***************************************************************************
   * Used to transport the scenario used to display items in the catalog.
   * The value is <b>detailScenario</b>.
   * @deprecated This constant must not be used anymore!Please use constant
   * {@link ActionConstants#RA_DETAILSCENARIO} instead.
   */
  public final static String RA_DISPLAYSCENARIO = "detailScenario";

  /**
   * Used to transport the scenario used to display items in detail in the catalog.
   * The value is <b>detailScenario</b>.
   */
  public final static String RA_DETAILSCENARIO  = "detailScenario";

  /**
   * Used to transport the scenario used to display items in the catalog.
   * The value is <b>display_scenario</b>.
   */
  public final static String RA_DISPLAY_SCENARIO= "display_scenario";
  
  /**
   * key to store the attribute list which will be used for the advanced search
   */
  public final static String RA_ATTRIB_LIST     = "attribList";
  
  /**
   * key to store the cua related product
   */
  public final static String RA_CUA_RELATED_PRODUCT  = "cuaRelatedProduct";
  
  /**
   * key to store the old detail scenario
   */
  public final static String RA_OLDDETAILSCENARIO = "oldDetailScenario";


  /* Display Scenario for product catalog */
  /**
    * The key for the request to set the type of the product list. 
    * The value is <b>isProductList</b>.
    */
   public final static String RA_IS_PRODUCT_LIST   		 = "isProductList";
   
   /**
    * The key for the request to set the type of the marketing product list. 
    * The value is <b>type</b>.
    */
   public final static String RA_SCENARIO_TYPE   		 = "type";
   
   /**
    * The key for the request to set the marketing list type of the marketing product list. 
    * The value is <b>cuaType</b>.
    */
   public final static String RA_CUA_TYPE   		 = "cuaType";
   
   /**
    * Detail Scenario for product catalog. In this case simple products are displayed. 
    * The value is <b>products</b>.
    */
   public final static String DS_PRODUCTS      		 = "products";

   /**
    * Detail Scenario to display bestsellers. The value is <b>bestseller</b>.
    */  
  public final static String DS_BESTSELLER       = "bestseller";
  /**
   * Detail Scenario to display a CUA list. This list can be related products or accessories. 
   * The value is <b>cuaList</b>.
   */
  public final static String DS_CUA                  = "cuaList";

  /**
   * Detail Scenario to display the catalog entry. The value is <b>catalogEntry</b>.
   */
  public final static String DS_ENTRY                = "catalogEntry";

  /**
   * Detail Scenario to display product details in a pop up. The value is <b>detailInWindow</b>.
   */
  public final static String DS_IN_WINDOW            = "detailInWindow";

  /**
   * Detail Scenario to display recommendations. The value is <b>recommendations</b>.
   */
  public final static String DS_RECOMMENDATION       = "recommendations";

  /**
   * Detail Scenario to be displayed catalog. In this case coming from a catalog query.
   * The value is <b>iview</b>.
   */
  public final static String DS_IVIEW                 = "iview";
  
  public final static String DS_ACCITEMLIST          = "accItemList";

  public final static String DS_ACCITEMDET           = "accItemDetails";

  /**
   * Detail Scenario to be displayed catalog. In this case coming from a catalog query.
   * The value is <b>catalogQuery</b>.
   */
  public final static String DS_CATALOG_QUERY         = "catalogQuery";
  

  /***************************************************************************
   * Names for Action Forwards
   ***************************************************************************/
  /**
   * Forward name for success, used as default if no differentiation
   * necessary.
   * The value is <b>success</b>.
   */
  public final static String FW_SUCCESS              = "success";
  
  /**
   * Forward name for success, if a Layout need to be setted
    * The value is <b>successLayout</b>.
  */
  public final static String FW_SUCCESS_LAYOUT       = "successLayout";
  
  /**
   * Forward name to transfer an catalog item to an external order.
   * Used in the standalone catalog.
    * The value is <b>transferToOrder</b>.
  */
  public final static String FW_TRANSFERTOORDERITEM  = "transferToOrder";
  
  /**
   * Forward name for success, used as default if no differentiation
   * necessary
   * The value is <b>checkForAuction</b>.
   */
  public final static String FW_CHECK_AVW            = "checkForAuction";

  /**
   * Forward name for failure, used if some essential part of the action
   * failed.
   * The value is <b>error</b>.
   */
  public final static String FW_ERROR                = "error";

  /**
   * Forward name if no items were found (i.e. list/page is empty).
   * The value is <b>noItems</b>.
   */
  public final static String FW_NO_ITEMS             = "noItems";

  /**
   * Forward name if exactly one item was found, used to display product detail
   * immediately in this case instead of product list.
   * The value is <b>oneItem</b>.
   */
  public final static String FW_ONE_ITEM             = "oneItem";

  /**
   * Forward name to cancel an action.
   * The value is <b>cancel</b>.
   */
  public final static String FW_CANCEL               = "cancel";

  /***************************************************************************
   * Forward name for adding a campaign code.
   * The value is <b>addcampcode</b>.
   */
  public final static String FW_ADDCAMP              = "addcampcode";
  
  /**
   * Forward name for event capturing
   * The value is <b>capture</b>.
   */
  public final static String FW_CAPTURE              = "capture";

  /**
   * Forward name for reprice.
   * The value is <b>reprice</b>.
   */
  public final static String FW_REPRICE              = "reprice";

  /**
   * Forward name for onlyOne.
   * The value is <b>onlyOne</b>.
   */
  public final static String FW_ONLYONE              = "onlyOne";

  /**
   * key to store a forward name in request context.
   * This is used to route a forward information through an intermediate
   * action. The value is <b>forward</b>.
   */
  public final static String RA_FORWARD              = "forward";
  
  /***************************************************************************
   * Initial parameters, from web.xml
   ***************************************************************************/

  /**
   * Id of the config which describes the configuration stuff for the webcatalog,
   * including the parameters for image server and item page size below.
   * This is used in the actions to determine the right configuration to use.
   * The value is <b>webcatalog</b>.
   */
  public final static String IN_WEBCATALOG_CONFIG_ID = "webcatalog";
  
  /**
   * Id of the config which describes the configuration stuff for the ui,
   * including the parameters for max items for a list view below.
   * The value is <b>ui</b>.
   */
  public final static String IN_UI_CONFIG_ID = "ui";
  
  /**
   * URL for server that contains images for catalog products / areas
   * The value is <b>imageserver</b>.
   */
  public final static String IN_IMAGE_SERVER = "imageserver";
  
  /**
   * Secure URL (HTTPS) for server that contains images for catalog products / areas.
   * The value is <b>httpsimageserver</b>.
   */
  public final static String IN_HTTPS_IMAGE_SERVER = "httpsimageserver";
  
  /**
   * Internal catalog image sever.
   * The value is <b>imageserver.catalog.isa.sap.com</b>.
   */
  public final static String INTCAT_IMAGE_SERVER = "imageserver.catalog.isa.sap.com";

  /**
   * URL for server that contains images for catalog products / areas.
   * The value is <b>itemPageSize</b>.
   */
  public final static String IN_ITEMPAGE_SIZE = "itemPageSize";
  
  /**
   * The number of items to be displayed in the list view (search results page).
   * The value is <b>Catalog.ListView.MaxItems</b>.
   */
  public final static String IN_LISTVIEW_MAXITEMS = "Catalog.ListView.MaxItems";
  
  /**
    * Flag. to indicate, if for SC relevant items prices should be displayed in the list view.
    * The value is <b>Catalog.ListView.ShowPricesForSCRelItems</b>.
    */
  public final static String IN_LISTVIEW_SHOW_SC_PRICES = "Catalog.ListView.ShowPricesForSCRelItems";
   
  /**
   * The number of page links to be displayed in the search results page.
   * The value is <b>numPageLinks</b>.
   */
  public final static String IN_NUM_PAGELINKS = "numPageLinks";
  
  /**
  * The value for the first page link in the catalog navigation.
  * The value is <b>first</b>.
  */
 public final static String IN_FIRST_PAGELINK = "first";
 
 /**
  * The value for the last page link in the catalog navigation.
  * The value is <b>last</b>.
  */
 public final static String IN_LAST_PAGELINK = "last";
 
 /**
  * The number of page links to be displayed in the page link jsp.
  * The value is <b>Catalog.Page.MaxPageLinks</b>.
  */
  public final static String IN_MAX_PAGE_LINKS = "Catalog.Page.MaxPageLinks";
  
  /**
   * Parameter to control the number of maximum search results to be retrieved.
   * The value is <b>maxSearchHits</b>.
   */
  public final static String IN_MAX_SEARCH_HITS = "maxSearchHits";

  /**
   * Parameter to control the displaying or hiding of decimal places of the WebCat Price.
   * The value is <b>priceDecimalPlace</b>.
   */
  public final static String IN_PRICE_DECIMAL_PLACE = "priceDecimalPlace";
  
  /** Config ID for the ims connection to the search engine.
   *  Used to get the parameter for enabling fuzzy search in extended search.
   * The value is <b>ims</b>.
   */
  public final static String IN_IMS_CONNECTION = "ims";
  
  /**
   * Parameter for fuzzy search enabling in the ims component configuration.
   * The value is <b>enableFuzzySearch</b>.
   */
  public final static String IN_ENABLE_FUZZY = "enableFuzzySearch";
  
  /**
   * Parameter telling whether price analysis should be performed on ipc.
   * The value is <b>enable.priceAnalysis.isa.sap.com</b>.
   */
  public final static String IN_PERFORM_PRICE_ANALYSIS = "enable.priceAnalysis.isa.sap.com";

  /**
   * Parameter telling URL where price analysis can be found.
   * The value is <b>url.priceAnalysis.isa.sap.com</b>.
   */
  public final static String IN_URL_PRICE_ANALYSIS = "url.priceAnalysis.isa.sap.com";

  /**
   * Parameter telling whether config online evaluation should be performed on ipc.
   * The value is <b>configOnlineEvaluate.isa.sap.com</b>.
   */
  public final static String IN_PERFORM_CONFIG_ONLINE_EVALUATION = "configOnlineEvaluate.isa.sap.com";

  /**
   * UI switches to control the block view of the catalog.
   * The value is <b>Catalog.BlockView.MaxColumns</b>. 
   */
  public final static String IN_UI_CAT_MAXCOLS = "Catalog.BlockView.MaxColumns";

  /**
   * UI switches to control the block view of the personal recommendations .
   * The value is <b>PersonalRecommendations.ShowAsBlockView</b>.
   */
  public final static String IN_UI_RECOM_BLOCKVIEW = "PersonalRecommendations.ShowAsBlockView";
  
  /**
   * UI switches to control the max rows in block view of the personal recommendations.
   * The value is <b>PersonalRecommendations.BlockView.MaxRows</b>. 
   */
  public final static String IN_UI_RECOM_MAXROWS = "PersonalRecommendations.BlockView.MaxRows";

  /**
   * UI switches to control the block view of the global recommendations (special offers).
   * The value is <b>GlobalRecommendations.ShowAsBlockView</b>.
   */
  public final static String IN_UI_GLOBAL_BLOCKVIEW = "GlobalRecommendations.ShowAsBlockView";
  
  /**
   * UI switches to control the max rows of block view of the global recommendations (special offers).
   * The value is <b>GlobalRecommendations.BlockView.MaxRows</b>.
   */
  public final static String IN_UI_GLOBAL_MAXROWS = "GlobalRecommendations.BlockView.MaxRows";

  /**
   * UI switches to control the block view of the catalog area.
   * The value is <b>CatalogArea.ShowAsBlockView</b>.
   */
  public final static String IN_UI_CATAREA_BLOCKVIEW = "CatalogArea.ShowAsBlockView";
  
  /**
   * UI switches to control the max rows of block view of the catalog area.
   * The value is <b>CatalogArea.BlockView.MaxRows</b>.
   */
  public final static String IN_UI_CATAREA_MAXROWS = "CatalogArea.BlockView.MaxRows";
  
  /**
   * UI switches to control the block view of the catalog search.
   * The value is <b>CatalogSearch.ShowAsBlockView</b>. 
   */
  public final static String IN_UI_CATSEARCH_BLOCKVIEW = "CatalogSearch.ShowAsBlockView";
  
  /**
   * UI switches to control the max rows of block view of the catalog search.
   * The value is <b>CatalogSearch.BlockView.MaxRows</b>. 
   */
  public final static String IN_UI_CATSEARCH_MAXROWS = "CatalogSearch.BlockView.MaxRows";
  
  /**
   * Catalog switch to control if an explosion of a package is done automatically.
   * The value is <b>Catalog.Package.AutomaticExplosion</b>.  
   */
  public final static String IN_AUTOM_PACK_EXPLOSION = "Catalog.Package.AutomaticExplosion";
  
  /**
   * Catalog switch to set status of catalogue to be read (for catalogue staging). 
   * The value is <b>catalogstatus</b>. 
   */
  public final static String IN_CATSTATUS = "catalogstatus";
  
  /**
   * The following are only used in EBP scenario currently.
   * The variable for the server with value <b>server.catalog.isa.sap.com</b>. 
   */
  public final static String IN_SERVER       = "server.catalog.isa.sap.com";
  
  /**
   * The following are only used in EBP scenario currently.
   * The variable for the server with value <b>user.catalog.isa.sap.com</b>.
   */
  public final static String IN_USER         = "user.catalog.isa.sap.com";
  
  /**
   * The following are only used in EBP scenario currently.
   * The variable for the server with value <b>password.catalog.isa.sap.com</b>.
   */
  public final static String IN_PASSWORD     = "password.catalog.isa.sap.com";
  
  /**
   * The following are only used in EBP scenario currently.
   * The variable for the server with value <b>language.isa.sap.com</b>.
   */
  public final static String IN_LANGUAGE     = "language.isa.sap.com";
  
  /**
   * The following are only used in EBP scenario currently.
   * The variable for the server with value <b>catalogID.catalog.isa.sap.com</b>.
   */
  public final static String IN_CATALOG_ID   = "catalogID.catalog.isa.sap.com";
  
  /**
   * The following are only used in EBP scenario currently.
   * The variable for the server with value <b>variantID.catalog.isa.sap.com</b>.
   */
  public final static String IN_VARIANT_ID   = "variantID.catalog.isa.sap.com";

  /**
   * The following are only used in EBP scenario currently.
   * The variable for the server with value <b>startCategory.catalog.isa.sap.com</b>.
   */
  public final static String IN_START_CHAR   = "startCategory.catalog.isa.sap.com";
  
  /**
   * Constant for context value for last visited shop.
   * The value is <b>cshop</b>.  
   */
  public final static String CV_CURRENT_SHOP = "cshop";  

  /**
   * Constant for context value for last visited catalog area.
   * The value is <b>carea</b>.  
   */
  public final static String CV_CURRENT_AREA = "carea";

  /**
   * Constant for context value for last visited catalog item area.
   * The value is <b>citem</b>.  
   */

  public final static String CV_CURRENT_ITEM = "citem";
  /**
   * Top item (relevant for Solution Configurator products).
   * The value is <b>ctitem</b>. 
   */
  public final static String CV_TOP_ITEM = "ctitem";

  /**
   * key to store a forward name in request context.
   * This is used to route a forward information through UpdateItemsFromRequest
   * action.
   * The value is <b>cquery</b>. 
   */
  public final static String CV_CURRENT_QUERY  = "cquery";
  
  /**
   * This variable is using another variable {@link #RA_QUERY_REDIRECT}.
   * The value is <b>redirecttolist</b>. 
   */
  public final static String CV_QUERY_REDIRECT = RA_QUERY_REDIRECT;

  /**
   * The value is <b>cadvsearch</b>. 
   */
  public final static String CV_ADV_SEARCH  = "cadvsearch";

  /**
   * page displayed number. The value is <b>cpgnum</b>.
   */
  public final static String CV_PAGE_NUMBER          = "cpgnum";
  
  /**
   * page displayed size. The value is <b>cpgsize</b>.
   */
  public final static String CV_PAGE_SIZE            = "cpgsize";
  
  /**
   * Catalog status constants. Variable for the active catalog. 
   * The value is <b>active</b>.
   */
  public final static String CATSTATUS_ACTIVE = "active";
  
  /**
   * Catalog status constants. Variable for the active catalog. 
   * The value is <b>inactive</b>.
   */
  public final static String CATSTATUS_INACTIVE = "inactive";

  /**
   * detail scenario
   * The value is <b>detailScenario</b>.
   */  
  public final static String CV_DETAILSCENARIO = "detailScenario";

  /**
   * Scenario constant for the bestseller scenario. The value is <b>1</b>.
   */
  public final static int CV_SCEN_BESTSELLER      = 1;
  
  /**
   * Scenario constant for the recommendations scenario. The value is <b>2</b>.
   */
  public final static int CV_SCEN_RECOMMENDATIONS = 2;
  
  /**
   * Scenario constant for the catalog area scenario. The value is <b>3</b>.
   */
  public final static int CV_SCEN_CATALOG_AREA    = 3;
  
  /**
   * Scenario constant for the catalog search scenario. The value is <b>4</b>.
   */
  public final static int CV_SCEN_CATALOG_SEARCH  = 4;
  
  /**
   * Constant for the Type of WebCatItemList in the bestseller scenario.
   * The value is <b>1</b>.
   */
  public final static int CV_LISTTYPE_BESTSELLER      = 1;
  
  /**
   * Constant for the Type of WebCatItemList in the recommendations scenario.
   * The value is <b>2</b>.
   */
  public final static int CV_LISTTYPE_RECOMMENDATIONS = 2;
  
  /**
   * Constant for the Type of WebCatItemList in the catalog area scenario.
   * The value is <b>3</b>.
   */
  public final static int CV_LISTTYPE_CATALOG_AREA    = 3;
  
  /**
   * Constant for the Type of WebCatItemList in the catalog search scenario.
   * The value is <b>4</b>.
   */
  public final static int CV_LISTTYPE_CATALOG_SEARCH  = 4;
  
  /**
   * Constant for the Type of WebCatItemList in the CUA scenario.
   * The value is <b>5</b>.
   */
  public final static int CV_LISTTYPE_CUA             = 5;
  
  /**
   * Name of the session attribute to set if an item coming from a basket
   * was exploded.
   * The value is <b>basketDirty</b>.
   */
  public final static String SC_BASKET_DIRTY  = "basketDirty";
  
  /**
   * Last visited area in the catalog. The value is <b>lastVisited</b>.
   */
  public final static String LAST_VISITED = "lastVisited";
  
  /**
   * catalog area search should be resetted?
   * The value is <b>resetSearch</b>.
   */
  public final static String RESET_SEARCH = "resetSearch";
  
  /**
   * This constant is used to store the login status.
   * The value is <b>Login Status</b>.
   */
  public static final String LOGIN_STATUS = "Login Status";
  
  /**
   * Name under which the welcome text is bound in the session context. The
   * welcome text should be displayed after the user has successfully logged in.
   * The value is <b>welcome text</b>.
   */
  public static final String WELCOME_TEXT = "welcome text";
   
  /**
   * This constant is used to store the frame name to be used by the frameset
   * to decide on the combination of frames to be displayed.
   * The value is <b>frameName</b>.
   */
  public static final String FRAME_NAME = "frameName";
     
  
  /**
   * Used when comparing items in the catalog.
   * The value is <b>compareCUA</b>.
   */
  public final static String RA_COMPARE_CUA = "compareCUA";

  /**
   * Used when sending CUA items in the catalog to the basket.
   * The value is <b>transferCUA</b>.
   */
  public final static String RA_TRANSFER_CUA = "transferCUA";
}
