package com.sap.isa.catalog.actions;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.sap.isa.businessobject.CommunicationException;
import com.sap.isa.catalog.webcatalog.WebCatArea;
import com.sap.isa.catalog.webcatalog.WebCatInfo;
import com.sap.isa.catalog.webcatalog.WebCatItemList;
import com.sap.isa.core.UserSessionData;
import com.sap.isa.core.util.WebUtil;

/**
 * Title:        GetProductsB2CAction
 * Description:  Creates a page of products, and stores it inside the request, such
 *               that to be able to be displayed by a JSP.
 *               The created page is either the first page for a newly selected categorie,
 *               or the indicated sub-page, if the user wants to browse through a categorie,
 *               whose products lies on several sub-pages.
 *               This class's <code>doPerform</code> method is also called when
 *               the user selects a new upper limit for the number of items that
 *               may be displayed inside a sub-page.
 * Copyright:    Copyright (c) 2001
 * Company:      SAPMarkets Europe GmbH
 * @version 1.0
 */

public class GetProductsB2CAction extends CatalogBaseAction {

    /**
     * Process the specified HTTP request, and create the corresponding HTTP
     * response (or forward to another web component that will create it).
     * Return an <code>ActionForward</code> instance describing where and how
     * control should be forwarded, or <code>null</code> if the response has
     * already been completed.
     *
     * @param mapping The ActionMapping used to select this instance
     * @param actionForm The optional ActionForm bean for this request (if any)
     * @param request The HTTP request we are processing
     * @param response The HTTP response we are creating
     *
     * @exception IOException if an input/output error occurs
     * @exception ServletException if a servlet exception occurs
     */

    public ActionForward doPerform(
        ActionMapping mapping,
        ActionForm form,
        HttpServletRequest request,
        HttpServletResponse response,
        UserSessionData userData,
        WebCatInfo theCatalog)
        throws IOException, ServletException, CommunicationException {

        log.debug("Entered to GetProductsAction");
        theCatalog.setLastVisited(WebCatInfo.ITEMLIST);

        //If not do it yourself if you can
        if (theCatalog == null) {
            //WebCatInfo.catalogLog.fatal("No Catalog Information availabale!");
            return (mapping.findForward("error"));
        }

        theCatalog.setCurrentItem(null);

        // check if this a query
        String isQuery = request.getParameter(ActionConstants.RA_IS_QUERY);
        if (isQuery == null && request.getAttribute(ActionConstants.RA_IS_QUERY) != null)
            isQuery = request.getAttribute(ActionConstants.RA_IS_QUERY).toString();

        String currentAreaId = null;
        if (currentAreaId == null && request.getAttribute(ActionConstants.RA_AREAKEY) != null)
            currentAreaId = request.getAttribute(ActionConstants.RA_AREAKEY).toString();

        if (currentAreaId != null) {
            WebCatArea currentArea = theCatalog.getArea(currentAreaId);
            if (null != currentArea && null != currentArea.getAreaName())
                theCatalog.setCurrentArea(currentArea);
        }

        //	if I'm on the Root Category
        if (theCatalog.getCurrentArea() == null || theCatalog.getCurrentArea().getAreaID() == WebCatArea.ROOT_AREA) {
            theCatalog.setCurrentArea(WebCatArea.ROOT_AREA);
        }

        // First the current item list is used !!
        WebCatItemList myItemList = theCatalog.getCurrentItemList();

        if (theCatalog.getCurrentArea() != null) {

            if (myItemList == null) {
                myItemList = theCatalog.getCurrentArea().getItemList();
            }

            request.setAttribute(ActionConstants.RA_AREA, theCatalog.getCurrentArea());
            request.setAttribute(ActionConstants.RA_AREAKEY, theCatalog.getCurrentArea().getAreaID());
            request.setAttribute(ActionConstants.RA_AREANAME, theCatalog.getCurrentArea().getAreaName());

            // check if this is a request for a category list
            if (null == isQuery || !isQuery.equalsIgnoreCase("yes")) {
                myItemList = theCatalog.getCurrentArea().getItemList();
                request.setAttribute(ActionConstants.RA_ITEMLIST_EBP, myItemList);
            }

            // argument for the title
            String[] args = { theCatalog.getCurrentArea().getAreaName()};
            String listTitleArg0 = WebUtil.translate(userData.getLocale(), "b2c.cat.b2ccatarea", args);
            request.setAttribute(ActionConstants.RA_PRODUCT_LIST_TITLE_ARG0, listTitleArg0);
        }

        if (myItemList == null || (theCatalog.getCurrentArea().getAreaID().equals("$ROOT") && request.getParameter("order") == null)) {
            return (mapping.findForward("noItems"));
        }

        if (request.getParameter("order") != null && !request.getParameter("order").equals("")) {
            myItemList.sort((String) request.getParameter("order"));
        }

        request.setAttribute("theNextPage", "productsDisplay");

        if (request.getParameter("display_scenario") != null && request.getParameter("display_scenario").equals("query") && request.getParameter("next").equals("order")) {
            request.setAttribute("isQuery", "yes");
            return mapping.findForward("toItemPage");
        }

        return (mapping.findForward(ActionConstants.FW_SUCCESS));

    }

}
