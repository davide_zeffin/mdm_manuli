package com.sap.isa.catalog.actions;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.sap.isa.businessobject.CommunicationException;
import com.sap.isa.catalog.webcatalog.OCIInfo;
import com.sap.isa.catalog.webcatalog.WebCatInfo;
import com.sap.isa.core.UserSessionData;

public class GetOciInfoAction extends CatalogBaseAction {

    public ActionForward doPerform(
        ActionMapping mapping,
        ActionForm form,
        HttpServletRequest request,
        HttpServletResponse response,
        UserSessionData userData,
        WebCatInfo theCatalog)
        throws IOException, ServletException, CommunicationException {

        log.debug("Entered to GetOciInfoAction");

        OCIInfo ociInfo = theCatalog.getOciInfo();

        if (ociInfo != null) {
            request.setAttribute(ActionConstants.RA_OCIINFO, ociInfo);
        }

        String forwardName = request.getParameter(ActionConstants.RA_FORWARD);
        ActionForward forward = null;

        if (forwardName != null) {
            forward = mapping.findForward(forwardName);
        }

        if (forward == null) {
            forward = mapping.findForward(ActionConstants.FW_SUCCESS);
        }

        log.debug("Forwarding to " + forward.getName());
        return forward;
    }
}