package com.sap.isa.catalog.actions;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.sap.isa.catalog.webcatalog.WebCatInfo;
import com.sap.isa.catalog.webcatalog.WebCatItemList;
import com.sap.isa.core.SessionConst;
import com.sap.isa.core.UserSessionData;
import com.sap.isa.core.util.StartupParameter;

/**
 * Title:        ShowLastVisitedAction
 * Description:  With respect of the last part of catalog that was seen by the user,
 *               the class allow forwarding to the needed page, such that the last
 *               view to be re-built on the screen
 * Copyright:    Copyright (c) 2001
 * Company:      SAPMarkets Europe GmbH
 * @version 1.0
 */

public class ShowLastVisitedAction extends CatalogBaseAction {
    
    public String FW_SHOPLIST = "showShopList";

    public ActionForward doPerform(ActionMapping mapping,
            ActionForm form,
            HttpServletRequest request,
            HttpServletResponse response,
            UserSessionData userSessionData,
            WebCatInfo theCatalog) throws ServletException {
                
        // at first check if we already have a catalog, otherwise as application, to show shoplist again
        
        if (theCatalog == null && getCatalogConfiguration(userSessionData) == null) {
            log.debug("Nos catalog available - try to redisplay shoplist");
            return mapping.findForward(FW_SHOPLIST);
        }
        
        WebCatItemList myItemList = theCatalog.getCurrentItemList();
        if (theCatalog.getLastVisited().equals(WebCatInfo.ITEMDETAILS) || theCatalog.getLastVisited().equals("showProduct")) {
                       
            if (myItemList == null || myItemList.getQuery() == null) {
                request.setAttribute(ActionConstants.RA_SCENARIO_TYPE, WebCatInfo.ITEMDETAILS);
            }
            else if (myItemList.size() > 1) {
                request.setAttribute(ActionConstants.RA_SCENARIO_TYPE, WebCatInfo.ITEMLIST);
            }
            else {
                request.setAttribute(ActionConstants.RA_SCENARIO_TYPE, WebCatInfo.ITEMDETAILS + "Query");
            }
        }
        else if (theCatalog.getLastVisited().equals(WebCatInfo.COMPARE)
                && (myItemList) != null) {
            
            if (myItemList.getArea() != null) {
                request.setAttribute(ActionConstants.RA_SCENARIO_TYPE, WebCatInfo.ITEMLIST);
            }
            if (myItemList.getQuery() != null) {
                if (myItemList.getAreaQuery() != null) {
                    request.setAttribute(ActionConstants.RA_SCENARIO_TYPE, WebCatInfo.CATEGORY_QUERY);
                }
                else {
                    request.setAttribute(ActionConstants.RA_SCENARIO_TYPE, WebCatInfo.CATALOG_QUERY);
                }
            }
            // the improbable case that we have for an item list neither Area, nor Query (hopefully, actually an impossible case)
            if (request.getAttribute(ActionConstants.RA_SCENARIO_TYPE) == null) {
                request.setAttribute(ActionConstants.RA_SCENARIO_TYPE, theCatalog.getLastVisited());
            }
        }
        // if NO type request parameter was set 
        else {
            request.setAttribute(ActionConstants.RA_SCENARIO_TYPE, theCatalog.getLastVisited());
        }

        request.setAttribute("extraType",
                new Boolean(theCatalog.isExtendedSearch()));

        ActionForward forward = null;
        try {
            forward = mapping.findForward((String)request.getAttribute(ActionConstants.RA_SCENARIO_TYPE));
        }
        catch (RuntimeException ex) {
            // forward is not known, but that's ok
            log.debug("ShowLastVisitedAction Forward unknown: "
                    + theCatalog.getLastVisited());
        }
        
        //check if we should display the auction page
		StartupParameter startupParameter = (StartupParameter) userSessionData.getAttribute(SessionConst.STARTUP_PARAMETER);
		if(startupParameter != null &&
		   startupParameter.getParameter("_auctionId") != null &&
		   !startupParameter.getParameterValue("_auctionId").equals("") )  {
				forward = mapping.findForward("auction");
		}

        if (forward == null) {
            forward = mapping.findForward(ActionConstants.FW_SUCCESS);
        }

        log.debug("Entered to ShowLastVisitedAction, forward to "
                + forward.getName());

        return forward;
    }
}