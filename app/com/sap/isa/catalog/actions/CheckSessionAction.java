/**
 * Title:        ProductCatalog UI
 * Description:
 * Copyright:    Copyright (c) 2001
 * Company:      SAPMarkets Europe GmbH
 * @version 1.0
 */

package com.sap.isa.catalog.actions;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.sap.isa.core.BaseAction;

/**
 * This action only checks whether a session exists.
 * Forwarding to key "success" if session exists,
 * BaseAction forwards to "session_not_valid" if not.
 */
public class CheckSessionAction extends BaseAction {
  public ActionForward doPerform(  ActionMapping mapping,
                                   ActionForm form,
                                   HttpServletRequest parm3,
                                   HttpServletResponse parm4)
  throws javax.servlet.ServletException, java.io.IOException {

    log.debug("Entered to CheckSessionAction");
    return (mapping.findForward(ActionConstants.FW_SUCCESS));
  }
}
