package com.sap.isa.catalog.actions;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.sap.isa.backend.boi.isacore.contract.ContractView;
import com.sap.isa.businessobject.CommunicationException;
import com.sap.isa.businessobject.webcatalog.CatalogBusinessObjectManager;
import com.sap.isa.catalog.webcatalog.WebCatInfo;
import com.sap.isa.core.UserSessionData;
import com.sap.isa.core.businessobject.management.MetaBusinessObjectManager;
import com.sap.isa.core.util.RequestParser;

/**
 * Title:        Internet Sales, dev branch
 * Description:
 * Copyright:    Copyright (c) 2001
 * Company:
 * @author
 * @version 1.0
 */

public class ISAGetContractDetailAction extends ISAGetContractInfoAction {

  public ActionForward doPerform(ActionMapping mapping,
                           ActionForm form,
                           HttpServletRequest request,
                           HttpServletResponse response,
                           UserSessionData userData,
                           WebCatInfo theCatalog,
                           RequestParser requestParser,
                           MetaBusinessObjectManager mbom)
                           throws IOException, ServletException, CommunicationException {

	CatalogBusinessObjectManager bom =
				  (CatalogBusinessObjectManager)userData.getBOM(CatalogBusinessObjectManager.CATALOG_BOM);
	bom.getCatalog().setProductDetailListType(WebCatInfo.PROD_DET_LIST_CONTRACTS);
	
    contractView = ContractView.PRODUCT_DETAIL;
    return super.doPerform(mapping, form, request, response, userData, theCatalog, requestParser, mbom);
  }

}