package com.sap.isa.catalog.actions;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.sap.isa.businessobject.CommunicationException;
import com.sap.isa.catalog.webcatalog.WebCatInfo;
import com.sap.isa.catalog.webcatalog.WebCatItemList;
import com.sap.isa.core.UserSessionData;

/**
 * Title:        SetItemPageSizeAction
 * Description:  Sets the maximum no. of item to be displayed inside of a page of
 *               products, with respect of the specified parameter.
 * Copyright:    Copyright (c) 2001
 * Company:      SAPMarkets Europe GmbH
 * @version 1.0
 */

public class SetItemPageSizeAction extends CatalogBaseAction {

    /**
     * Process the specified HTTP request, and create the corresponding HTTP
     * response (or forward to another web component that will create it).
     * Return an <code>ActionForward</code> instance describing where and how
     * control should be forwarded, or <code>null</code> if the response has
     * already been completed.
     *
     * @param mapping The ActionMapping used to select this instance
     * @param form The optional ActionForm bean for this request (if any)
     * @param request The HTTP request we are processing
     * @param response The HTTP response we are creating
     * @param userData container for session informations
     * @param theCatalog representation of the catalog
     *
     * @exception IOException if an input/output error occurs
     * @exception ServletException if a servlet exception occurs
     * 
     * @return specific action forward corresponding to the struts configuration
     */
    public ActionForward doPerform(
        ActionMapping mapping,
        ActionForm form,
        HttpServletRequest request,
        HttpServletResponse response,
        UserSessionData userData,
        WebCatInfo theCatalog)
        throws IOException, ServletException, CommunicationException {

        log.debug("Entered SetItemPageSizeAction");
        WebCatItemList myItemList = this.getScenarioDependentItemList(request, theCatalog);
        
        int listType = myItemList.getListType();
//        if (theCatalog.getCurrentItemList() != null) {
//            listType = theCatalog.getCurrentItemList().getListType();
//        }

        String pageSizeStr = request.getParameter(ActionConstants.RP_PAGE_SIZE);
        if (null == pageSizeStr) {
            pageSizeStr = getContextValue(request, ActionConstants.CV_PAGE_SIZE);
        }

        if (pageSizeStr != null && pageSizeStr.length() > 0) {
            try {
                int pageSize = Integer.parseInt(pageSizeStr);
                theCatalog.setPageSize(pageSize, listType);
            }
            catch (Exception e) {
                log.debug(e.getMessage());
                //reset current item
            }
        }

        // set context value for the next request
        setContextValue(request, ActionConstants.CV_PAGE_SIZE, Integer.toString(theCatalog.getPageSize(listType)));

        // determine forward
        if (theCatalog.getCurrentItemList() != null
            && theCatalog.getCurrentItemList().getListType() == ActionConstants.CV_LISTTYPE_CUA
            && theCatalog.getProductDetailListType() != 0) {
            request.setAttribute(ActionConstants.RA_IS_QUERY, "yes");
            return mapping.findForward("cuaList");
        }
        else {
            // if query is set forward to query
            if (request.getAttribute(ActionConstants.RA_IS_QUERY) != null && "yes".equals(request.getAttribute(ActionConstants.RA_IS_QUERY))) {
                request.setAttribute("toNext", "toItemPage");
                return mapping.findForward("query");
            }
        }

        // if query is not set display_scenario might be set (only on the productsB2c.inc.jsp)
        // try this, to determine the forward 
        // parameter might be replaced sometimes by enhancing the isQuery parameter 
        if (request.getParameter("display_scenario") == null || request.getParameter("display_scenario").equals("products")) {
            return mapping.findForward(ActionConstants.FW_SUCCESS);
        }
        else if (request.getParameter("display_scenario").equals("areafilter")) {
            return mapping.findForward("areafilter");
        }
        else {
            request.setAttribute("toNext", "toItemPage");
            request.setAttribute(ActionConstants.RA_IS_QUERY, "yes");
            return mapping.findForward("query");
        }
    }
}