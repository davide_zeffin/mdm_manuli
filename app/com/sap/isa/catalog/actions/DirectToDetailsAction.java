package com.sap.isa.catalog.actions;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.sap.isa.businessobject.CommunicationException;
import com.sap.isa.catalog.webcatalog.WebCatArea;
import com.sap.isa.catalog.webcatalog.WebCatInfo;
import com.sap.isa.catalog.webcatalog.WebCatItemList;
import com.sap.isa.core.UserSessionData;

/**
 * Title:        DirectToDetailsAction
 * Description:  Set the catalog's current Item with respect to the <code>itemkey</code> parameter
 * Copyright:    Copyright (c) 2001
 * Company:      SAP AG, Germany
 * @version 1.0
 */

public class DirectToDetailsAction extends CatalogBaseAction {

    /**
     * Process the specified HTTP request, and create the corresponding HTTP
     * response (or forward to another web component that will create it).
     * Return an <code>ActionForward</code> instance describing where and how
     * control should be forwarded, or <code>null</code> if the response has
     * already been completed.
     *
     * @param mapping The ActionMapping used to select this instance
     * @param form The optional ActionForm bean for this request (if any)
     * @param request The HTTP request we are processing
     * @param response The HTTP response we are creating
     * @param userData container for session informations
     * @param theCatalog representation of the catalog
     *
     * @exception IOException if an input/output error occurs
     * @exception ServletException if a servlet exception occurs
     * 
     * @return specific action forward corresponding to the struts configuration
     */
    public ActionForward doPerform(ActionMapping mapping,
            ActionForm form,
            HttpServletRequest request,
            HttpServletResponse response,
            UserSessionData userData,
            WebCatInfo theCatalog)
            throws IOException, ServletException, CommunicationException {

        log.debug("Entered to DirectToDetailsAction");

        if (theCatalog == null) {
            //WebCatInfo.catalogLog.fatal("No Catalog Information availabale!");
            return (mapping.findForward("error"));
        }

        String areaId = request.getParameter("areaID");
        String itemId = request.getParameter("productID");

        // I set the current area

        WebCatArea currentArea = null;
        currentArea = theCatalog.getArea(areaId);

        theCatalog.setCurrentArea(currentArea);

        ArrayList path = new ArrayList();
        ArrayList rightPath = new ArrayList();
        rightPath.add("0");
        WebCatArea cArea = currentArea;

        while (cArea != null) {
            path.add(cArea);
            cArea = cArea.getParentArea();
        }

        for (int i = path.size() - 1; i >= 0; i--) {
            rightPath.add(((WebCatArea) path.get(i)).getAreaID());
        }

        //I set the current area's path according to its position in category's tree
        currentArea.setPath(rightPath);

        //Set the current Item List, current Item, current Item's price
        WebCatItemList theList = currentArea.getItemList();
        theList.iterator();
        theCatalog.setCurrentItemList(theList);

        //Note 730093
        //    theCatalog.setCurrentItem(theCatalog.getCurrentItemList().getItem(itemId));
        //    theCatalog.getCurrentItem().readItemPrice();

        //Set the last visited
        //    theCatalog.setLastVisited(WebCatInfo.ITEMDETAILS);

        if (itemId != null && itemId != "") {
            if (theCatalog.getCurrentItemList().getItem(itemId) != null) {
                theCatalog.setCurrentItem(theCatalog.getCurrentItemList().getItem(itemId));
                theCatalog.getCurrentItem().readItemPrice();
                //Set the last visited
                theCatalog.setLastVisited(WebCatInfo.ITEMDETAILS);
                // Set context values
                setContextValue(request, ActionConstants.CV_CURRENT_AREA, areaId);
                setContextValue(request, ActionConstants.CV_CURRENT_ITEM, itemId);
            }
            else {
                String msg = "Item with itemId = '" + itemId + "' was not found in current item list of areaId = '" + areaId + "'"; 
                log.error(msg);
                theCatalog.setLastVisited(WebCatInfo.ITEMLIST);
            }
        }
        else {
            theCatalog.setLastVisited(WebCatInfo.ITEMLIST);
        }

        //End Note 730093
        return (mapping.findForward(ActionConstants.FW_SUCCESS));
    }

}