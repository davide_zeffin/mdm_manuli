package com.sap.isa.catalog.actions;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.sap.isa.businessobject.BusinessObjectException;
import com.sap.isa.businessobject.CommunicationException;
import com.sap.isa.businessobject.webcatalog.CatalogBusinessObjectManager;
import com.sap.isa.catalog.boi.CatalogException;
import com.sap.isa.catalog.webcatalog.WebCatArea;
import com.sap.isa.catalog.webcatalog.WebCatInfo;
import com.sap.isa.catalog.webcatalog.WebCatWeightedArea;
import com.sap.isa.core.UserSessionData;
import com.sap.isa.core.util.Message;

/**
 * Title:        GetFullCategoriesTreeAction
 * Description:  Query the catalog about all the categories from the category tree. It creates a list of categories,
 *               in which, each element has 2 fields: The Category itself, and the depth the category is placed in the tree
 *               Actually the created list stores a tree structure.
 */

public class GetFullCategoriesTreeAction extends CatalogBaseAction {
    /**
     * categoriesList is an Array of <code>com.sap.isa.catalog.webcatalog.WebCatWeightedArea</code> elements
     * and contains the list of all the categories from the resulting tree, and their depth
     */
    ArrayList categoriesList;
    /**
     * categoriesListCurrentIndex is an <code>int</code> which stores the last position which was stored in the
     * <code>categoriesList</code> list.
     */
    int categoriesListCurrentIndex;
    WebCatInfo theCatalog;
    /**
     * pathList is an Array of <code>String</code> elements
     * and contains the list of all the categories which make up the current path
     * (its content is always changing during <code>makeList</code> procedure execution
     */
    ArrayList pathList;

    /**
     * This is a recursive procedure which is used to browse the categories tree, starting from the root,
     * and to store in <code>categoriesList</code> Array all the categories from it.
     * @param parentArea The WebCatArea parameter stores informations about the area which is
     *                    direct parent for the current area
     * @param pos The int position the current categorie should be placed on the output tree.
     * @param indexParent The int position for current categorie's parent in <code>categoriesList</code>
     */

    private void makeList(WebCatArea parentArea, int pos, int indexParent) throws CatalogException {

        Iterator childrenList = null;
        if (!parentArea.getAreaID().equals(WebCatArea.ROOT_AREA)) {
            if (parentArea.getChildren().size() == 0)
                return;
            childrenList = parentArea.getChildren().iterator();
        }
        else
            childrenList = theCatalog.getRootCategories().iterator();

        WebCatWeightedArea newArea;
        while (childrenList.hasNext()) {
            WebCatArea childrenArea = (WebCatArea) childrenList.next();
            if (childrenArea.getAreaID().equals("0"))
                continue;
            newArea = new WebCatWeightedArea();
            newArea.setArea(childrenArea);
            newArea.setPosition(pos);
            newArea.setParentId(indexParent);
            pathList.add(childrenArea.getAreaID());
            for (int i = 0; i < pathList.size(); i++) {
                newArea.addPath((String) pathList.get(i));
            }
            newArea.setPath();

            if (newArea.getArea().getChildren().size() != 0)
                newArea.setSign(WebCatWeightedArea.PLUS);
            else
                newArea.setSign(WebCatWeightedArea.NOTHING);

            categoriesList.add(newArea);
            categoriesListCurrentIndex++;
            makeList(childrenArea, pos + 1, categoriesListCurrentIndex);
            pathList.remove(pathList.size() - 1);
        }
    }

    /**
     * Process the specified HTTP request, and create the corresponding HTTP
     * response (or forward to another web component that will create it).
     * Return an <code>ActionForward</code> instance describing where and how
     * control should be forwarded, or <code>null</code> if the response has
     * already been completed.
     *
     * @param mapping The ActionMapping used to select this instance
     * @param actionForm The optional ActionForm bean for this request (if any)
     * @param request The HTTP request we are processing
     * @param response The HTTP response we are creating
     *
     * @exception IOException if an input/output error occurs
     * @exception ServletException if a servlet exception occurs
     */

    public ActionForward doPerform(
        ActionMapping mapping,
        ActionForm form,
        HttpServletRequest request,
        HttpServletResponse response,
        UserSessionData userData,
        WebCatInfo theCatalog)
        throws IOException, ServletException, CommunicationException {

        log.debug("Entered to GetCategoriesISAAction");

        //If not do it yourself if you can
        if (theCatalog == null) {
            return (mapping.findForward("error"));
        }

        categoriesList = new ArrayList();
        categoriesListCurrentIndex = -1;

        if (theCatalog.getCurrentArea().getPath() == null) {
            ArrayList pList = new ArrayList();
            pList.add("0");
            theCatalog.getCurrentArea().setPath(pList);
        }

        WebCatArea tempArea = null;
        tempArea = theCatalog.getArea(WebCatArea.ROOT_AREA);
        if (tempArea == null) {
            log.error("catalog.exception.backend");
            Message msg = new Message(Message.ERROR, "catalog.exception.usermsg");
            BusinessObjectException boe = new BusinessObjectException("Catalog error", msg);
            request.setAttribute("Exception", boe);
            return mapping.findForward(ActionConstants.FW_ERROR);
        }
        
        WebCatWeightedArea newArea = new WebCatWeightedArea();
        newArea.setArea(tempArea);
        newArea.setPosition(0);
        newArea.addPath("0");
        newArea.setPath();

        pathList = new ArrayList();
        pathList.add("0");

        try {
            makeList(tempArea, 1, 0); //parentArea, current position into the tree, parentIndex
        }
        catch (CatalogException e) {
            log.error("catalog.exception.backend", e);
            Message msg = new Message(Message.ERROR, "catalog.exception.usermsg");
            BusinessObjectException boe = new BusinessObjectException("Catalog error", msg);
            request.setAttribute("Exception", boe);
            return mapping.findForward(ActionConstants.FW_ERROR);
        }
        
        request.setAttribute("currentArea", theCatalog.getCurrentArea());
        request.setAttribute("categoriesList", categoriesList);
        
        return (mapping.findForward(ActionConstants.FW_SUCCESS));
    }
}
