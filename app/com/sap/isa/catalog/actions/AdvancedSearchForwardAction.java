package com.sap.isa.catalog.actions;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.sap.isa.businessobject.CommunicationException;
import com.sap.isa.catalog.webcatalog.WebCatInfo;
import com.sap.isa.core.UserSessionData;
import com.sap.isa.core.ui.layout.UILayoutManager;

/**
 * Title:        AdvancedSearchForwardAction
 * Description:
 * Copyright:    Copyright (c) 2008
 * Company:      SAP AG
 * @version 1.0
 */

public class AdvancedSearchForwardAction extends CatalogBaseAction {


    private final String LAYOUT_ACCOUNT = "accountLayout";
    private final String LAYOUT_CATALOG = "catalogLayout";

    /**
     * Process the specified HTTP request, and create the corresponding HTTP
     * response (or forward to another web component that will create it).
     * Return an <code>ActionForward</code> instance describing where and how
     * control should be forwarded, or <code>null</code> if the response has
     * already been completed.
     *
     * @param mapping The ActionMapping used to select this instance
     * @param form The optional ActionForm bean for this request (if any)
     * @param request The HTTP request we are processing
     * @param response The HTTP response we are creating
     * @param userData container for session informations
     * @param theCatalog representation of the catalog
     *
     * @exception IOException if an input/output error occurs
     * @exception ServletException if a servlet exception occurs
     * 
     * @return specific action forward corresponding to the struts configuration
     */
    public ActionForward doPerform(ActionMapping mapping,
            ActionForm form,
            HttpServletRequest request,
            HttpServletResponse response,
            UserSessionData userData,
            WebCatInfo theCatalog)
            throws IOException, ServletException, CommunicationException {

        final String METHOD_NAME = "AdvancedSearchForwardAction";
        log.entering(METHOD_NAME);

        // Page that should be displayed next.
        String forward = ActionConstants.FW_SUCCESS;
        
        UILayoutManager uiManager = UILayoutManager.getManagerFromRequest(request);
        
        if (uiManager.getCurrentUILayout() != null) {
            String layoutName = uiManager.getCurrentUILayout().getName();
            if (layoutName != null &&
                (layoutName.equals(this.LAYOUT_ACCOUNT) || 
                 layoutName.equals(this.LAYOUT_CATALOG))) {
                    forward = layoutName;
            }
        }

        log.exiting();
        return mapping.findForward(forward);
    }
}