package com.sap.isa.catalog.actions;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.sap.isa.businessobject.CommunicationException;
import com.sap.isa.businessobject.contract.ContractAttributeList;
import com.sap.isa.businessobject.contract.ContractReferenceMap;
import com.sap.isa.businessobject.webcatalog.CatalogBusinessObjectManager;
import com.sap.isa.catalog.webcatalog.ContractDisplayMode;
import com.sap.isa.catalog.webcatalog.WebCatInfo;
import com.sap.isa.catalog.webcatalog.WebCatItem;
import com.sap.isa.catalog.webcatalog.WebCatItemList;
import com.sap.isa.catalog.webcatalog.WebCatItemPage;
import com.sap.isa.core.UserSessionData;
import com.sap.isa.core.businessobject.management.MetaBusinessObjectManager;
import com.sap.isa.core.util.RequestParser;


public class GetContractInfoAction extends CatalogBaseAction {

    public ActionForward doPerform(
        ActionMapping mapping,
        ActionForm form,
        HttpServletRequest request,
        HttpServletResponse response,
        UserSessionData userData,
        WebCatInfo theCatalog,
        RequestParser requestParser,
        MetaBusinessObjectManager mbom)
        throws IOException, ServletException, CommunicationException {

	    ActionForward nextPage = mapping.findForward(ActionConstants.FW_SUCCESS);
        
        if (getCatalogConfiguration(userData).showCatContracts()) {
        	
			CatalogBusinessObjectManager bom = (CatalogBusinessObjectManager) mbom.getBOMbyName(CatalogBusinessObjectManager.CATALOG_BOM);

			if (theCatalog.getContractDisplayMode() == null) {
				theCatalog.setContractDisplayMode(new ContractDisplayMode());
			}

			request.setAttribute(ActionConstants.RA_CONTRACTDISPLAYMODE, theCatalog.getContractDisplayMode());

			if (theCatalog.getContractDisplayMode().showNoContracts()) {
				//obsolete : contract attribute list is stored in web cat item list
				//request.setAttribute("refNames", new ContractAttributeList());
				theCatalog.getCurrentItemList().setContractAttributeList(new ContractAttributeList());
				if (log.isDebugEnabled())
					log.debug("Contracts turned off, leaving Action...");
				return nextPage;
			}

			ContractAttributeList attributeList = determineContractAttributeList(userData, bom, contractView, log);

			//obsolete, the contract item list is set on the webcat item list now
			//request.setAttribute("refNames", attributeList);
			theCatalog.getCurrentItemList().setContractAttributeList(attributeList);

			// get iterator of items to be shown on the page, might be itemList or itemPage
			Iterator iterator;
			WebCatItemPage itemPage = null;
			WebCatItemList itemList = null;

			WebCatItem item = (WebCatItem) request.getAttribute(ActionConstants.RC_WEBCATITEM);
			if (item != null) {
				if (log.isDebugEnabled())
					log.debug("running single item");
				ArrayList itList = new ArrayList(1);
				itList.add(item);
				iterator = itList.iterator();
			}
			else {
				itemPage = (WebCatItemPage) request.getAttribute("itemPage");
				if (itemPage != null) {
					if (log.isDebugEnabled())
						log.debug("running item page");
					iterator = itemPage.iterator();
				}
				else {
					itemList = (WebCatItemList) request.getAttribute("itemList");
					if (itemList != null) {
						if (log.isDebugEnabled())
							log.debug("running item list");
						iterator = itemList.iterator();
					}
					else {
						return nextPage;
					}
				}
			}

			// prepare requests data for contract references
			ArrayList items = new ArrayList();

			ContractReferenceMap contractRefMap = determineContractInfo(userData, theCatalog, bom, iterator, items, contractView, log);

			// if no contract info found at all: exit
			if (contractRefMap == null || contractRefMap.size() == 0) {
				return nextPage;
			}

			setContractInfo(items, contractRefMap, contractView, log);
        }

        return nextPage;
    }

}
