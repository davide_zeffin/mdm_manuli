/*****************************************************************************

    Class:        GetPricesAction
    Copyright (c) 2001, SAP AG, Germany, All rights reserved.
    Created:      2001

*****************************************************************************/
package com.sap.isa.catalog.actions;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.sap.isa.businessobject.CommunicationException;
import com.sap.isa.businessobject.webcatalog.CatalogBusinessObjectManager;
import com.sap.isa.businessobject.webcatalog.pricing.PriceCalculator;
import com.sap.isa.catalog.webcatalog.WebCatInfo;
import com.sap.isa.catalog.webcatalog.WebCatItem;
import com.sap.isa.catalog.webcatalog.WebCatItemList;
import com.sap.isa.catalog.webcatalog.WebCatItemPage;
import com.sap.isa.core.UserSessionData;
import com.sap.isa.core.businessobject.management.MetaBusinessObjectManager;
import com.sap.isa.core.util.RequestParser;
import com.sap.isa.isacore.action.EComBaseAction;


public class GetPricesAction extends CatalogBaseAction {

    /**
     * Process the specified HTTP request, and create the corresponding HTTP
     * response (or forward to another web component that will create it).
     * Return an <code>ActionForward</code> instance describing where and how
     * control should be forwarded, or <code>null</code> if the response has
     * already been completed.
     * 
     * @param mapping mapping The ActionMapping used to select this instance
     * @param form The <code>FormBean</code> specified in the Struts configuration file for this action
     * @param request The request object
     * @param response The response object
     * @param userData Object wrapping the session
     * @param theCatalog theCatalog Representation of the catalog
     * @param requestParser Parser to simple retrieve data from the request
     * @param mbom Meta business object manager
     * 
     * @return Forward to another action or JSP page
     * @throws IOException
     * @throws ServletException
     * @throws CommunicationException
     */
    public ActionForward doPerform(ActionMapping mapping,
            ActionForm form,
            HttpServletRequest request,
            HttpServletResponse response,
            UserSessionData userData,
            WebCatInfo theCatalog,
            RequestParser requestParser,
            MetaBusinessObjectManager mbom)
            throws IOException, ServletException, CommunicationException {

        HttpSession theSession = request.getSession();

        CatalogBusinessObjectManager bom = (CatalogBusinessObjectManager) userData.getBOM(CatalogBusinessObjectManager.CATALOG_BOM);

        PriceCalculator priceCalc = bom.getPriceCalculator();
        // provide price analysis setting to page; used for price analysis in b&w only.
        request.setAttribute(ActionConstants.RA_PRICECALC_INIT_DATA, priceCalc.getInitData());

        // get iterator of items to be shown on the page, might be itemList or itemPage
        Iterator iterator;
        WebCatItemPage itemPage = null;
        WebCatItemList itemList = null;

        WebCatItem item = (WebCatItem) request.getAttribute(ActionConstants.RC_WEBCATITEM);
        if (item != null) {
            log.debug("running single item");
            ArrayList itList = new ArrayList(1);
            itList.add(item);
            iterator = itList.iterator();
        }
        else {
            itemPage = (WebCatItemPage) request.getAttribute("itemPage");
            if (itemPage != null) {
                log.debug("running item page");
                iterator = itemPage.iterator();
            }
            else {
                itemList = (WebCatItemList) request.getAttribute("itemList");
                if (itemList != null) {
                    log.debug("running item list");
                    iterator = itemList.iteratorOnlyPopulated();
                }
                else {
                    return mapping.findForward(ActionConstants.FW_ERROR);
                }
            }
        }

        // UoMs
        //String theItemId = (String) theSession.getAttribute("itemId");
        //String selectedIndex = (String) theSession.getAttribute("index");
        //int index = 0;
        // UoMs

        ArrayList items = new ArrayList();
        while (iterator.hasNext()) {
            item = (WebCatItem) iterator.next();
            // if price info has been evaluated for this item don't do it again.
            // item without price has empty price, item which has
            // not been evaluated yet has no prices at all!
            /*
             if (theItemId != null && selectedIndex != null && item.getItemID().equalsIgnoreCase(theItemId))
             {
             index = Integer.parseInt(selectedIndex);
             WebCatItemPrice itemPrice = item.getPrices()[index];
             item.setItemPrice(itemPrice); 	     
             }
             */
            if (item.getItemPrice() == null || bom.getCatalog().isDoPricing()) {
                items.add(item);
            }
            Collection subItems = item.getContractItems();
            if (subItems != null && !subItems.isEmpty()) {
                Iterator subItemIterator = subItems.iterator();
                while (subItemIterator.hasNext()) {
                    WebCatItem subItem = (WebCatItem) subItemIterator.next();
                    if (subItem.getItemPrice() == null
                            || bom.getCatalog().isDoPricing()) {
                        items.add(subItem);
                    }
                }
            }
        }

        if (!items.isEmpty() && priceCalc != null) {

            repriceItems(mbom, items);
        }

        // Remove attributes from the session as they are no longer needed
        theSession.removeAttribute("itemId");
        theSession.removeAttribute("index");

        // Determine forward
        ActionForward nextPage = null;
        String forwardKey = (String) request.getAttribute(ActionConstants.RA_FORWARD);
        if (forwardKey == null) {
            forwardKey = (String) request.getParameter(ActionConstants.RA_FORWARD);
        }
        else {
			request.removeAttribute(ActionConstants.RA_FORWARD);
        }
        if (forwardKey != null) {
            EComBaseAction.IsaActionMapping isaMapping = (EComBaseAction.IsaActionMapping)mapping;
            if (!isaMapping.isForwardDefined(forwardKey)) {
                log.debug("forward not defined for GetPricesAction: "
                        + forwardKey);
            }
            else {
                nextPage = mapping.findForward(forwardKey);                 
            }
        }

        if (nextPage == null) {
            if (getCatalogConfiguration(userData).showCatAVWLink()) {
                log.debug("default forward for GetPricesAction to: "
                        + ActionConstants.FW_CHECK_AVW);
                nextPage = mapping.findForward(ActionConstants.FW_CHECK_AVW);
            }
            else {
                log.debug("default forward for GetPricesAction to: "
                        + ActionConstants.FW_SUCCESS);
                nextPage = mapping.findForward(ActionConstants.FW_SUCCESS);
            }
        }

        return nextPage;
    }
}