package com.sap.isa.catalog.actions;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.sap.isa.businessobject.CommunicationException;
import com.sap.isa.catalog.webcatalog.WebCatInfo;
import com.sap.isa.core.UserSessionData;

/**
 * Title:        ForwardSplitterAction
 * Description:  Action to which allows to choose where to forward, with respect of the
 *               "next" parameter from the request. By default it forwards to success.
 */

public class ForwardSplitterAction extends CatalogBaseAction {

    public ActionForward doPerform(
        ActionMapping mapping,
        ActionForm form,
        HttpServletRequest request,
        HttpServletResponse response,
        UserSessionData userData,
        WebCatInfo theCatalog)
        throws IOException, ServletException, CommunicationException {

        log.debug("Entering ForwardSplitterAction");

        String nextAction = request.getParameter(ActionConstants.RA_NEXT);
        ActionForward forward;

        if (nextAction != null) {
            log.debug(ActionConstants.RA_NEXT + " has value: " + nextAction);
            forward = mapping.findForward(nextAction);
            if (forward != null) {
                return forward;
            }
        }

        forward = mapping.findForward(ActionConstants.FW_SUCCESS);
        if (forward != null) {
            return forward;
        }

        String[] forwards = mapping.findForwards();
        if (forwards.length == 0) {
            log.debug("FATAL !!! No forwards defined !!");
            return null;
        }
        
        log.debug("Forwarding to: " + forwards[0]);
        forward = mapping.findForward(forwards[0]);
        return forward;
    }
}
