package com.sap.isa.catalog.actions;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.sap.isa.backend.boi.webcatalog.CatalogConfiguration;
import com.sap.isa.businessobject.CommunicationException;
import com.sap.isa.catalog.webcatalog.WebCatArea;
import com.sap.isa.catalog.webcatalog.WebCatInfo;
import com.sap.isa.catalog.webcatalog.WebCatItemList;
import com.sap.isa.core.UserSessionData;
import com.sap.isa.core.util.WebUtil;

/**
 * Title:        GetItemsFromCategoryAction 
 * Description:  Creates a page of products for the current catalogue area,
 *               and stores it inside the request, such that to be able to be displayed by a JSP. 
 * Copyright:    Copyright (c) 2001
 * Company:      SAP AG, Germany
 *
 * @version 1.0
 */
public class GetItemsFromCategoryAction extends CatalogBaseAction {

    /**
     * Process the specified HTTP request, and create the corresponding HTTP
     * response (or forward to another web component that will create it).
     * Return an <code>ActionForward</code> instance describing where and how
     * control should be forwarded, or <code>null</code> if the response has
     * already been completed.
     *
     * @param mapping The ActionMapping used to select this instance
     * @param form The optional ActionForm bean for this request (if any)
     * @param request The HTTP request we are processing
     * @param response The HTTP response we are creating
     * @param userData container for session informations
     * @param theCatalog representation of the catalog
     *
     * @exception IOException if an input/output error occurs
     * @exception ServletException if a servlet exception occurs
     * 
     * @return specific action forward corresponding to the struts configuration
     */
    public ActionForward doPerform(ActionMapping mapping,
            ActionForm form,
            HttpServletRequest request,
            HttpServletResponse response,
            UserSessionData userData,
            WebCatInfo theCatalog)
            throws IOException, ServletException, CommunicationException {
        
        log.debug("Entered to GetProductsAction");

        theCatalog.setLastVisited(WebCatInfo.ITEMLIST);

        //If not do it yourself if you can
        if (theCatalog == null) {
            //WebCatInfo.catalogLog.fatal("No Catalog Information availabale!");
            return (mapping.findForward("error"));
        }

        theCatalog.setCurrentItem(null);

        String currentAreaId = null;

        // check if area id is available in request context
        if (request.getAttribute(ActionConstants.RA_AREAKEY) != null) {
            currentAreaId = request.getAttribute(ActionConstants.RA_AREAKEY).toString();
        }

        // use context value, if it is available 
        if (currentAreaId == null) {
            currentAreaId = getContextValue(request, ActionConstants.CV_CURRENT_AREA);
        }

        if (currentAreaId != null) {
            // the context values is also set action only used in B2C. 
            setContextValue(request, ActionConstants.CV_CURRENT_AREA, currentAreaId);

            WebCatArea currentArea = theCatalog.getArea(currentAreaId);

            if ((null != currentArea) && (null != currentArea.getAreaName())) {
                theCatalog.setCurrentArea(currentArea);
            }
        }

        //	if I'm on the Root Category
        if ((theCatalog.getCurrentArea() == null) || (theCatalog.getCurrentArea().getAreaID() == WebCatArea.ROOT_AREA)) {
            theCatalog.setCurrentArea(WebCatArea.ROOT_AREA);
        }

        WebCatItemList myItemList = null;

        if (theCatalog.getCurrentArea() != null) {
            if (myItemList == null) {
                myItemList = theCatalog.getCurrentArea().getItemList();
            }

            request.setAttribute(ActionConstants.RA_AREA, theCatalog.getCurrentArea());
            request.setAttribute(ActionConstants.RA_AREAKEY, theCatalog.getCurrentArea().getAreaID());
            request.setAttribute(ActionConstants.RA_AREANAME, theCatalog.getCurrentArea().getAreaName());

            myItemList = theCatalog.getCurrentArea().getItemList();
            // resolve BB problem
            theCatalog.setCurrentItemList(myItemList);
            request.setAttribute(ActionConstants.RA_ITEMLIST_EBP, myItemList);

            // argument for the title
            String args[] = { theCatalog.getCurrentArea().getAreaName() };
            String listTitleArg0 = WebUtil.translate(userData.getLocale(), "b2c.cat.b2ccatarea", args);
            request.setAttribute(ActionConstants.RA_PRODUCT_LIST_TITLE_ARG0, listTitleArg0);
        }

        if (myItemList == null) {
            return (mapping.findForward("noItems"));
        }

        setContextValue(request, ActionConstants.CV_CURRENT_AREA, theCatalog.getCurrentArea().getAreaID());
        request.setAttribute("theNextPage", "productsDisplay");
        
        CatalogConfiguration catConf = getCatalogConfiguration(userData);
        
        if (catConf.isSuppCatEventCapturing()) {
			return (mapping.findForward(ActionConstants.FW_CAPTURE));
        }

        return (mapping.findForward(ActionConstants.FW_SUCCESS));
    }
}
