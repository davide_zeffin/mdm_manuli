package com.sap.isa.catalog.actions;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.sap.isa.backend.boi.isacore.marketing.MarketingBusinessObjectsAware;
import com.sap.isa.backend.boi.isacore.marketing.MarketingConfiguration;
import com.sap.isa.businessobject.CommunicationException;
import com.sap.isa.catalog.boi.CatalogException;
import com.sap.isa.catalog.boi.ICatalog;
import com.sap.isa.catalog.boi.IQueryStatement;
import com.sap.isa.catalog.webcatalog.WebCatInfo;
import com.sap.isa.catalog.webcatalog.WebCatItemList;
import com.sap.isa.core.UserSessionData;
import com.sap.isa.core.businessobject.management.MetaBusinessObjectManager;
import com.sap.isa.core.util.RequestParser;

/**
 * Title:        AdvancedSearchExecuteAction
 * Description:
 * Copyright:    Copyright (c) 2008
 * Company:      SAP AG
 * @version 1.0
 */

public class AdvancedSearchExecuteAction extends CatalogQueryAction {

    private final String FLD_QUERY_STRING = "advSQuery";
    private final String FLD_ATTR_KEYS = "advSattrKey[]";
    private final String FLD_ATTR_SEL = "advSattrSelected[]";
    private final String FLD_STD_CAT = "advSStdCat";
    private final String FLD_REW_CAT = "advSRewCat";
    private final String FLD_STD_AREA = "advSStdArea";
    private final String FLD_REW_AREA = "advSRewArea";
    private final String FLD_PTS_FROM = "advSPtsFrom";
    private final String FLD_PTS_TO = "advSPtsTo";


    /**
     * Process the specified HTTP request, and create the corresponding HTTP
     * response (or forward to another web component that will create it).
     * Return an <code>ActionForward</code> instance describing where and how
     * control should be forwarded, or <code>null</code> if the response has
     * already been completed.
     * 
     * @param mapping mapping The ActionMapping used to select this instance
     * @param form The <code>FormBean</code> specified in the Struts configuration file for this action
     * @param request The request object
     * @param response The response object
     * @param userData Object wrapping the session
     * @param theCatalog theCatalog Representation of the catalog
     * @param requestParser Parser to simple retrieve data from the request
     * @param mbom Meta business object manager
     * 
     * @return Forward to another action or JSP page
     * @throws IOException
     * @throws ServletException
     * @throws CommunicationException
     */
    public ActionForward doPerform(ActionMapping mapping,
            ActionForm form,
            HttpServletRequest request,
            HttpServletResponse response,
            UserSessionData userData,
            WebCatInfo theCatalog,
            RequestParser requestParser,
            MetaBusinessObjectManager mbom)
            throws IOException, ServletException, CommunicationException {

        final String METHOD_NAME = "AdvancedSearchExecuteAction";
        log.entering(METHOD_NAME);

        HttpSession theSession = request.getSession();

        MarketingBusinessObjectsAware mktAware = (MarketingBusinessObjectsAware) userData.getMBOM().getBOMByType(MarketingBusinessObjectsAware.class);
        MarketingConfiguration mktConfig = null;
        if (mktAware != null) {
            mktConfig = mktAware.getMarketingConfiguration();
        }

        // Page that should be displayed next.
        String forward = ActionConstants.FW_SUCCESS;
        boolean executeSearch = true;
        boolean advSStdCat = true;
        boolean advSRewCat = false;
        String advSStdArea = null;
        String advSRewArea = null;
        float ptsFrom = 0F;
        float ptsTo = 0F;
        String[] queryAttr =  null;
        String advSQuery = null;

        RequestParser.Parameter rp_advSQuery = requestParser.getParameter(FLD_QUERY_STRING);
        
        if (rp_advSQuery == null || !rp_advSQuery.isSet()) {
            // Quick Search
            RequestParser.Parameter rp_queryStr = requestParser.getParameter(ActionConstants.RA_CURRENT_QUERY);
            advSQuery = rp_queryStr.getValue().getString();
            advSStdCat = true;

            try {
                ICatalog iCatalog = theCatalog.getCatalog();
                IQueryStatement queryStmt = iCatalog.createQueryStatement();
                queryAttr = iCatalog.getQuickSearchAttributes();
            }
            catch (CatalogException e) {
                log.error("system.backend.exception", e);
            }
        }
        else {
            // Advanced Search
            RequestParser.Parameter rp_attrKey = requestParser.getParameter(FLD_ATTR_KEYS);
            RequestParser.Parameter rp_attrSelected = requestParser.getParameter(FLD_ATTR_SEL);
            RequestParser.Parameter rp_inStandardCat = requestParser.getParameter(FLD_STD_CAT);
            RequestParser.Parameter rp_inRewardCat = requestParser.getParameter(FLD_REW_CAT);
            RequestParser.Parameter rp_stdArea = requestParser.getParameter(FLD_STD_AREA);
            RequestParser.Parameter rp_rewArea = requestParser.getParameter(FLD_REW_AREA);
            RequestParser.Parameter rp_ptsFrom = requestParser.getParameter(FLD_PTS_FROM);
            RequestParser.Parameter rp_ptsTo = requestParser.getParameter(FLD_PTS_TO);

            advSQuery = rp_advSQuery.getValue().getString();
            advSStdCat = rp_inStandardCat.getValue().getBoolean();
            advSRewCat = rp_inRewardCat.getValue().getBoolean();
            advSStdArea = rp_stdArea.getValue().getString();
            advSRewArea = rp_rewArea.getValue().getString();
            ptsFrom = (float) rp_ptsFrom.getValue().getDouble();
            ptsTo = (float) rp_ptsTo.getValue().getDouble();

            // ---------- Read attributes -----------   
            // count selected attributes
            queryAttr =  new String[rp_attrSelected.getNumValues()];
            
            // take over the attribute flags
            if (rp_attrKey != null            && 
                rp_attrKey.getNumValues() > 0 &&
                rp_attrSelected.getNumValues() > 0) {
                int idx = 0;    
                for (int i = 0; i < rp_attrKey.getNumValues(); i++) {
                    
                    if (rp_attrKey.getValue(i).getString().length() > 0) {
                        String attrKey = rp_attrKey.getValue(i).getString();
                        boolean attrSelected = rp_attrSelected.getValue(i).getBoolean();
            
                        if (attrSelected) {
                            queryAttr[idx] = attrKey;
                            idx++;
                        }
            
                        if (log.isDebugEnabled()) {
                            log.debug(METHOD_NAME + "attribute=" + attrKey + ", isSelected=" + attrSelected);
                        }
                    }
                } // end for
            }
        }
  
        WebCatItemList itemList = null;
        
        if (theCatalog != null &&
            queryAttr.length > 0) {
            // ---------- Read search criteria -----------   
            
            if (ptsTo > 0 && ptsTo >= ptsFrom) {
                request.setAttribute("advSPtsFrom", Float.toString(ptsFrom));
                request.setAttribute("advSPtsTo", Float.toString(ptsTo));
            }
    
            // clear the saved item list
            theCatalog.setSavedItemList(null);
    
            if (advSQuery != null) {
                boolean populateAll = true;
                // the following populateAll flag is set by the IcssCatalogEntryAction 
//                boolean populateAll = "true".equals(request.getAttribute(ActionConstants.RA_POPULATE_ALL));

                itemList = advancedSearch ( mktConfig, theCatalog, advSQuery, advSStdCat, advSStdArea, advSRewCat, advSRewArea, 
                                            ptsFrom, ptsTo, queryAttr, populateAll );
                setContextValues(request, theCatalog, itemList);
                request.setAttribute(ActionConstants.RA_ITEMLIST_EBP, itemList);
            }
            forward = setForward(mapping, request, userData, theCatalog, itemList, advSQuery);

            String advSStdCatStr = (advSStdCat) ? "X" : "";
            request.setAttribute("advSStdCat", advSStdCatStr);
            String advSRewCatStr = (advSRewCat) ? "X" : "";
            request.setAttribute("advSRewCat", advSRewCatStr);
            request.setAttribute("advSStdArea", advSStdArea);
            request.setAttribute("advSRewArea", advSRewArea);
            
            setContextValue(request, ActionConstants.CV_ADV_SEARCH, "true");
        }
        else {
            //WebCatInfo.catalogLog.fatal("No Catalog Information availabale!");
            itemList = new WebCatItemList(theCatalog);
            theCatalog.setCurrentItemList(itemList);
            setContextValues(request, theCatalog, itemList);
            request.setAttribute(ActionConstants.RA_ITEMLIST_EBP, itemList);
            
            forward = ActionConstants.FW_NO_ITEMS;
        }

        log.exiting();
        return mapping.findForward(forward);
    }
    
}