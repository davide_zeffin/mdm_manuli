/*****************************************************************************
  Copyright (c) 2001, SAPMarkets Europe GmbH, Germany, All rights reserved.
  Author:
  Created:  19 September 2001

  $Revision: #1 $
*****************************************************************************/
package com.sap.isa.catalog.actions;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.sap.isa.businessobject.CommunicationException;
import com.sap.isa.catalog.webcatalog.WebCatInfo;
import com.sap.isa.catalog.webcatalog.WebCatItem;
import com.sap.isa.core.UserSessionData;
import com.sap.spc.remote.client.object.IPCException;

public class EndItemConfigurationAction extends CatalogBaseAction {

    public ActionForward doPerform(
        ActionMapping mapping,
        ActionForm form,
        HttpServletRequest request,
        HttpServletResponse response,
        UserSessionData userData,
        WebCatInfo theCatalog)
        throws IOException, ServletException, CommunicationException {

        if (theCatalog.getConfiguredItem() == null)
            return (mapping.findForward(ActionConstants.FW_SUCCESS));

        WebCatItem item = theCatalog.getConfiguredItem();

        try {
            item.getConfigItemReference().pricing();
        }
        catch (IPCException ee) {
            if (log.isDebugEnabled()) {
                log.debug("Error ", ee);
            }
        }

        return (mapping.findForward(ActionConstants.FW_SUCCESS));
    }
}
