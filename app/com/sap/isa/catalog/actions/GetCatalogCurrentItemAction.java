package com.sap.isa.catalog.actions;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.sap.isa.backend.boi.webcatalog.CatalogBusinessObjectsAware;
import com.sap.isa.backend.boi.webcatalog.CatalogConfiguration;
import com.sap.isa.businessobject.CommunicationException;
import com.sap.isa.catalog.webcatalog.WebCatInfo;
import com.sap.isa.catalog.webcatalog.WebCatItem;
import com.sap.isa.core.UserSessionData;



public class GetCatalogCurrentItemAction extends CatalogBaseAction {
    public ActionForward doPerform(
        ActionMapping mapping,
        ActionForm form,
        HttpServletRequest request,
        HttpServletResponse response,
        UserSessionData userData,
        WebCatInfo theCatalog)
        throws IOException, ServletException, CommunicationException {

        ActionForward nextPage = null;

        WebCatItem currentItem = theCatalog.getCurrentItem();
        if (currentItem == null) {
            nextPage = mapping.findForward(ActionConstants.FW_ERROR);
        }

        else {
            request.setAttribute(ActionConstants.RC_WEBCATITEM, currentItem);
            nextPage = mapping.findForward(ActionConstants.RA_FORWARD);
            if (nextPage == null) {
                nextPage = mapping.findForward(ActionConstants.FW_SUCCESS);
                CatalogBusinessObjectsAware cataConfBom = (CatalogBusinessObjectsAware) userData.getMBOM().getBOMByType(CatalogBusinessObjectsAware.class);
                if (cataConfBom != null) {
                    CatalogConfiguration catConfig = cataConfBom.getCatalogConfiguration();
                    if (!catConfig.catUseReloadOneTime()) {
                        nextPage = mapping.findForward(ActionConstants.FW_SUCCESS_LAYOUT);
                    }
                }
            }
        }

        log.debug("Forwarding to " + nextPage.getName() + " = " + nextPage.getPath());

        return nextPage;

    }
}