/*****************************************************************************
 Class:        InvalidateSessionAction
 Copyright (c) 2008, SAP AG, Germany, All rights reserved.
 Author:       SAP AG
 Created:      15.04.2008
 Version:      1.0
 *****************************************************************************/

package com.sap.isa.catalog.actions;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.sap.isa.businessobject.CommunicationException;
import com.sap.isa.core.UserSessionData;
import com.sap.isa.core.businessobject.management.MetaBusinessObjectManager;
import com.sap.isa.core.util.RequestParser;
import com.sap.isa.isacore.action.EComBaseAction;


/**
 * InvalidateSessionAction - Invalidates the current session <br>
 *
 * @author  SAP AG
 * @version 1.0
 */
public class InvalidateSessionAction extends EComBaseAction {

    public ActionForward ecomPerform(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response, UserSessionData userSessionData, RequestParser requestParser, MetaBusinessObjectManager mbom, boolean multipleInvocation, boolean browserBack) throws IOException, ServletException, CommunicationException {
       request.getSession().invalidate();
       
       return mapping.findForward("success"); //new ActionForward("success");
    }

}
