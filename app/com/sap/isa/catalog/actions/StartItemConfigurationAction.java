/*****************************************************************************
 Copyright (c) 2001, SAPMarkets Europe GmbH, Germany, All rights reserved.
 Author:
 Created:  11 May 2001

 $Revision: #5 $
 $Date: 2002/12/09 $
 *****************************************************************************/
package com.sap.isa.catalog.actions;

import java.io.IOException;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.sap.isa.backend.boi.webcatalog.CatalogConfiguration;
import com.sap.isa.businessobject.CommunicationException;
import com.sap.isa.businessobject.contract.ContractReference;
import com.sap.isa.businessobject.ipc.imp.DefaultIPCBOManager;
import com.sap.isa.catalog.webcatalog.WebCatInfo;
import com.sap.isa.catalog.webcatalog.WebCatItem;
import com.sap.isa.catalog.webcatalog.WebCatItemList;
import com.sap.isa.core.FrameworkConfigManager;
import com.sap.isa.core.PanicException;
import com.sap.isa.core.TechKey;
import com.sap.isa.core.UserSessionData;
import com.sap.isa.core.businessobject.management.MetaBusinessObjectManager;
import com.sap.isa.core.interaction.InteractionConfig;
import com.sap.isa.core.interaction.InteractionConfigContainer;
import com.sap.isa.core.util.RequestParser;
import com.sap.isa.core.util.WebUtil;
import com.sap.isa.ipc.ui.jsp.action.RequestParameterConstants;
import com.sap.spc.remote.client.object.IPCClient;
import com.sap.spc.remote.client.object.IPCItem;
import com.sap.spc.remote.client.object.IPCItemReference;
import com.sap.spc.remote.client.object.OriginalRequestParameterConstants;

public class StartItemConfigurationAction extends CatalogBaseAction {

    /**
     * Process the specified HTTP request, and create the corresponding HTTP
     * response (or forward to another web component that will create it).
     * Return an <code>ActionForward</code> instance describing where and how
     * control should be forwarded, or <code>null</code> if the response has
     * already been completed.
     * 
     * @param mapping mapping The ActionMapping used to select this instance
     * @param form The <code>FormBean</code> specified in the Struts configuration file for this action
     * @param request The request object
     * @param response The response object
     * @param userData Object wrapping the session
     * @param theCatalog theCatalog Representation of the catalog
     * @param requestParser Parser to simple retrieve data from the request
     * @param mbom Meta business object manager
     * 
     * @return Forward to another action or JSP page
     * @throws IOException
     * @throws ServletException
     * @throws CommunicationException
     */
    public ActionForward doPerform(ActionMapping mapping,
            ActionForm form,
            HttpServletRequest request,
            HttpServletResponse response,
            UserSessionData userData,
            WebCatInfo theCatalog,
            RequestParser requestParser,
            MetaBusinessObjectManager mbom)
            throws IOException, ServletException, CommunicationException {

        final String METHOD_NAME = "doPerform()";
        log.entering(METHOD_NAME);

        CatalogConfiguration config = getCatalogConfiguration(userData);
        
        DefaultIPCBOManager ipcBom = (DefaultIPCBOManager) userData.getBOM(DefaultIPCBOManager.NAME);

        InteractionConfigContainer interactionConfigContainer = getInteractionConfig(request);
        InteractionConfig interactionConfig = interactionConfigContainer.getConfig("ipc_scenario");

        if (interactionConfig != null) {
            String ipcScenario = interactionConfig.getValue("scenario.cat");

            if (ipcScenario == null) {
                log.debug("XCM Scenario scenario.cat for IPC Product Configuration not found");
            }

            request.setAttribute(com.sap.isa.ipc.ui.jsp.action.InternalRequestParameterConstants.IPC_XCM_SCENARIO, ipcScenario);
            if (log.isDebugEnabled()) {
                log.debug(com.sap.isa.ipc.ui.jsp.action.InternalRequestParameterConstants.IPC_XCM_SCENARIO
                          + " was set to request with value " + ipcScenario);
            } 
        }
        else {
            log.debug("Configuration for ipc_scenario not found in interaction-config.xml");
        }

        WebCatItemList items = theCatalog.getCurrentItemList();
        String itemKey = request.getParameter(ActionConstants.RA_ITEMKEY);
        String topItemKey = request.getParameter(ActionConstants.RA_TOP_ITEMKEY);
        
        if (log.isDebugEnabled()) {
            log.debug("itemKey = " + itemKey);
            log.debug("topItemKey = " + topItemKey);
        }

        // get the item (regarding also valuable contract information)
        WebCatItem item = null;
        
        if(topItemKey != null && !topItemKey.equals("") &&   // we're in Solution Configurator mode 
           itemKey != null && !itemKey.equals("")) {        // and a sub-item was requested
               
            log.debug("Configure sub item");
            
            // check for the catalogs current item at first       
            if (theCatalog.getCurrentItem() != null && theCatalog.getCurrentItem().getItemKey().getItemID().equals(topItemKey)) {
                log.debug("Configure sub item of current catalog item");
                item = theCatalog.getCurrentItem().getWebCatSubItem(new TechKey(itemKey));
            }
            else {
                if(items == null) {
                    // we're not able to read sub-items information!
                    log.exiting();
                    throw new PanicException("determination of sub-items only possible if WebCatItemList is available");
                }
                item = items.getSubItem(topItemKey, new TechKey(itemKey));
            }
        }
        else if (theCatalog.getCurrentItem() != null && theCatalog.getCurrentItem().getItemId().equals(itemKey)) { //check if the itemKey of the current item is the same as the itemKey
            
            log.debug("The current item is the item to configure");
            
            item = theCatalog.getCurrentItem();
        }
        else {
            
            log.debug("Configure normal item");

            if (items == null || items.getItem(itemKey) == null) {
                item = theCatalog.getItem((String) request.getParameter(ActionConstants.RA_AREAKEY),
                        (String) request.getParameter(ActionConstants.RA_ITEMKEY));
            }
            else {
                item = items.getItem(itemKey).getContractItem(request.getParameter(ActionConstants.RA_CONTRACTKEY),
                        request.getParameter(ActionConstants.RA_CONTRACTITEMKEY));
            }
        }  
        
        if(item == null) {
            // we could not determine sub item from the sub item list
            log.debug("Item to be configured could not be determined");
            log.exiting();
            return (mapping.findForward(ActionConstants.FW_ERROR));
        }  

        theCatalog.setConfiguredItem(item);

        //    IPCItemReference ipcItemReference = new IPCItemReference();
        ContractReference contractRef = item.getContractRef();

        if (contractRef == null) {
            contractRef = item.getContractDetailedRef();
        }

        IPCItem ipcItem = (IPCItem) item.getConfigItemReference();

        if (ipcItem == null) {
            // no config information available, do not start config
            log.debug("IPCItem for Item to be configured could not be determined");
            log.exiting();
            return (mapping.findForward(ActionConstants.FW_ERROR));
        }

        //this is to ensure that IPCClient is initialized...
        //      IPCClient ipcClient = ipcBom.createIPCClient();
        IPCClient ipcClient = ipcItem.getDocument().getSession().getIPCClient();

        IPCItemReference ipcItemReference = ipcItem.getItemReference();

        if (log.isDebugEnabled()) {
            //log.debug("IPCItemReference: " + ipcItemReference + " DocumentId=" + ipcItemReference.getDocumentId() + ",ItemId=" + ipcItemReference.getItemId() + ",Host=" + ((TCPIPCItemReference)ipcItemReference).getHost() + ",Port=" + ((TCPIPCItemReference)ipcItemReference).getPort() + ",SessionId=" + ipcItemReference.getSessionId());
            log.debug("IPCItemReference: " + ipcItemReference + " DocumentId="
                    + ipcItemReference.getDocumentId() + ",ItemId="
                    + ipcItemReference.getItemId());
        }

        request.setAttribute(ActionConstants.RA_IPC_ITEM_REFERENCE, ipcItemReference);
        if (config.isStandaloneCatalog()) {
            log.debug("IPC caller is Standalone Catalog");
            
            InteractionConfigContainer interactionConfigData = getInteractionConfig(request);

            String shopScenario = interactionConfigData.getConfig("shop").getValue("shopscenario");
        
            if (log.isDebugEnabled()) {
                log.debug("shopScenario = " + shopScenario);
            }
            
            if (shopScenario == null || shopScenario.toUpperCase().indexOf("B2C") >= 0) {
                log.debug("Shop scenario is null or contains B2C");
                request.setAttribute(RequestParameterConstants.CALLER, RequestParameterConstants.CALLER_B2C_CATALOG);
            }
            else{
                log.debug("Shop scenario does not contain B2C");
                request.setAttribute(RequestParameterConstants.CALLER, RequestParameterConstants.CALLER_BACK_TO_CATALOG);
            }
        }
        else if ("B2C".equalsIgnoreCase(config.getApplication())) {
            log.debug("IPC caller is B2C");
            request.setAttribute(RequestParameterConstants.CALLER, RequestParameterConstants.CALLER_B2C_CATALOG);
        }
        else {
            log.debug("IPC caller is B2B");
            request.setAttribute(RequestParameterConstants.CALLER, RequestParameterConstants.CALLER_BACK_TO_CATALOG);
        }

        // some IPC UI startup parameters...
        if (request.getParameter(ActionConstants.RA_NEXT).equals("config_view")) {
            request.setAttribute(RequestParameterConstants.DISPLAY_MODE, "T");
        }
        else {
            request.setAttribute(RequestParameterConstants.DISPLAY_MODE, "F");
        }

        request.setAttribute(RequestParameterConstants.IPC_SCENARIO, "ISA");

        // Variantensuche erm�glichen
        request.setAttribute("enableVariantSearch", "T");

        ServletContext context = this.getServlet().getServletContext();

        //  change to get parm from XCM, no longer from web.xml
        //    String configOnlineEval = context.getInitParameter(ActionConstants.IN_PERFORM_CONFIG_ONLINE_EVALUATION);
        String configOnlineEval = FrameworkConfigManager.Servlet.getInteractionConfigParameter(request.getSession(),
                "ui",
                "configOnlineEvaluate",
                "configOnlineEvaluate.isa.sap.com");

        log.debug("configOnlineEvaluate parameter obtained");

        if (configOnlineEval != null && configOnlineEval.equalsIgnoreCase("true")) {
            request.setAttribute("online_evaluate", "T");
        }
        else {
            request.setAttribute("online_evaluate", "F");
        }
        
        if (config.isNoPriceUsed()) {
            log.debug("No Prices is used, don't show prices in the config UI also");
            request.setAttribute(OriginalRequestParameterConstants.HIDE_PRICES, "T");
        }

        // set filter to restrict determined product variants to those available
        // in the catalog...
        request.setAttribute(OriginalRequestParameterConstants.IPC_ITEM_FILTER, theCatalog);

        // again some contract specialities ...
        if (contractRef != null && contractRef.getContractKey() != null && 
            !contractRef.getContractKey().isInitial()) {
            // Checking wheather the contractFilter contains only '0' like...
            //  + '0'
            //  + '000000000000000000'
            // ...in this case we will not transfer the contract data to IPC!!!
            
            String configFilter = contractRef.getConfigFilter();
            if (stringIsFilledWith(configFilter,'0') == false) {
                // contract config is always display-only
                request.setAttribute(RequestParameterConstants.DISPLAY_MODE, "T");

                // put caller message into request context (only for non-variants)
                HttpSession session = request.getSession();

                // message "configuration can only be changed in basket"
                String description = WebUtil.translate(context, session, "b2b.contract.message.confDOnly", null);
            
                request.setAttribute("caller_message", description);

                // contract filter
                request.setAttribute("restrictId", contractRef.getConfigFilter());
                request.setAttribute("ownerId", contractRef.getContractKey().getIdAsString());
                request.setAttribute("ownerType", "CONTRACT");
                request.setAttribute("repositoryType", "IBASE");

                if (log.isDebugEnabled()) {
                    log.debug("Config Filter: " + contractRef.getConfigFilter());
                }
            }
            else {
                // we have a contractFilter like '000000000000000000' 
                if (log.isDebugEnabled()) {
                    log.warn("Contract with ContractID "+contractRef.getContractId()+" is not set cause of configFilter '"+contractRef.getConfigFilter()+"'");
                }
            }        

        }
        
        log.exiting();
        return (mapping.findForward(ActionConstants.FW_SUCCESS));
    }
    
    /**
     * Check if String is filled with a character
     * @param string to check
     * @param character searched in string
     * 
     * @return flag as boolean; true if character is contained in string
     */
    protected boolean stringIsFilledWith(String s, char value) {
        boolean retVal = true;
        for (int i = 0; i < s.length() && retVal == true; i++) {
            if (s.charAt(i) != value) {
                retVal = false;
            }
        }
        return retVal;
    }

}