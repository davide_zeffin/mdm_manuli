package com.sap.isa.catalog.actions;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.Iterator;
import java.util.Locale;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.sap.ecommerce.boi.campaign.CampaignHeaderData;
import com.sap.ecommerce.businessobject.campaign.Campaign;
import com.sap.ecommerce.businessobject.campaign.CampaignBusinessObjectManager;
import com.sap.isa.backend.boi.isacore.BaseConfiguration;
import com.sap.isa.backend.boi.webcatalog.CatalogBusinessObjectsAware;
import com.sap.isa.backend.boi.webcatalog.CatalogConfiguration;
import com.sap.isa.backend.boi.webcatalog.CatalogUser;
import com.sap.isa.backend.boi.webcatalog.pricing.PriceCalculatorBackend;
import com.sap.isa.backend.boi.webcatalog.pricing.PriceCalculatorInitData;
import com.sap.isa.businessobject.AppBaseBusinessObjectManager;
import com.sap.isa.businessobject.CommunicationException;
import com.sap.isa.businessobject.IdMapper;
import com.sap.isa.businessobject.webcatalog.CatalogBusinessObjectManager;
import com.sap.isa.businessobject.webcatalog.ManifestReader;
import com.sap.isa.businessobject.webcatalog.pricing.PriceCalculator;
import com.sap.isa.businesspartner.backend.boi.PartnerFunctionData;
import com.sap.isa.businesspartner.businessobject.BusinessPartner;
import com.sap.isa.businesspartner.businessobject.BusinessPartnerManager;
import com.sap.isa.businesspartner.businessobject.SoldTo;
import com.sap.isa.catalog.CatalogParameters;
import com.sap.isa.catalog.boi.IClient;
import com.sap.isa.catalog.impl.CatalogView;
import com.sap.isa.catalog.webcatalog.CatalogClient;
import com.sap.isa.catalog.webcatalog.CatalogServer;
import com.sap.isa.catalog.webcatalog.ContractDisplayMode;
import com.sap.isa.catalog.webcatalog.WebCatInfo;
import com.sap.isa.catalog.webcatalog.WebCatItem;
import com.sap.isa.catalog.webcatalog.WebCatItemList;
import com.sap.isa.core.BaseAction;
import com.sap.isa.core.Constants;
import com.sap.isa.core.TechKey;
import com.sap.isa.core.UserSessionData;
import com.sap.isa.core.init.InitializationEnvironment;
import com.sap.isa.core.interaction.InteractionConfig;
import com.sap.isa.core.interaction.InteractionConfigContainer;
import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.core.ui.context.GlobalContextManager;
import com.sap.isa.core.util.Message;
import com.sap.isa.core.util.MessageList;
import com.sap.isa.core.util.table.ResultData;
import com.sap.isa.isacore.MessageDisplayer;
import com.sap.isa.maintenanceobject.businessobject.UIElement;
import com.sap.isa.ui.uicontrol.UIController;

/**
 * Title: ISAEntryAction Description: This is the starting point for entering in the catalog. Here, the catalog is
 * created (if it's not already created), with respect of parameters taken from the request or from BOM's data.
 * Copyright:    Copyright (c) 2001 Company:
 *
 * @author
 * @version 1.0
 */
public class ISAEntryAction extends CatalogBaseAction {
    /**
     * Initialize Context values. <br>
     */
    static public void initContextValues() {
        GlobalContextManager.registerValue(ActionConstants.CV_CURRENT_AREA, true, false);
        GlobalContextManager.registerValue(ActionConstants.CV_CURRENT_ITEM, true, false);
        GlobalContextManager.registerValue(ActionConstants.CV_CURRENT_QUERY, true, false);
        GlobalContextManager.registerValue(ActionConstants.CV_ADV_SEARCH, true, false);
        // GlobalContextManager.registerValue(ActionConstants.CV_QUERY_REDIRECT, true, false);
        GlobalContextManager.registerValue(ActionConstants.CV_PAGE_NUMBER, true, false);
        GlobalContextManager.registerValue(ActionConstants.CV_PAGE_SIZE, true, false);
    }

    /**
     * Process the specified HTTP request, and create the corresponding HTTP
     * response (or forward to another web component that will create it).
     * Return an <code>ActionForward</code> instance describing where and how
     * control should be forwarded, or <code>null</code> if the response has
     * already been completed.
     *
     * @param mapping The ActionMapping used to select this instance
     * @param form The optional ActionForm bean for this request (if any)
     * @param request The HTTP request we are processing
     * @param response The HTTP response we are creating
     * @param userData container for session informations
     * @param theCatalog representation of the catalog
     *
     * @exception IOException if an input/output error occurs
     * @exception ServletException if a servlet exception occurs
     * 
     * @return specific action forward corresponding to the struts configuration
     */
    public ActionForward doPerform(
        ActionMapping mapping,
        ActionForm form,
        HttpServletRequest request,
        HttpServletResponse response,
        UserSessionData userData,
        WebCatInfo theCatalog)
        throws IOException, ServletException, CommunicationException {

        long start = System.currentTimeMillis();

        final String METHOD_NAME = "ISAEntryAction.doPerform()";
        log.entering(METHOD_NAME);

        int catalogStatus;
        boolean selectInactiveStagingCat = false;

        // check if already logged on
        ManifestReader.logManifest(log, this.getClass()); //for isaCat
        ManifestReader.logManifest(log, ManifestReader.class); //for Base_Catalog
        ManifestReader.logManifest(log, BaseAction.class); //for Base_Core
        ManifestReader.logManifest(log, com.sap.isa.catalog.CatalogFactory.class); //for Base_pcatAPI

        CatalogBusinessObjectManager bom = (CatalogBusinessObjectManager) userData.getBOM(CatalogBusinessObjectManager.CATALOG_BOM);
        CatalogBusinessObjectsAware catObjBom = getCatalogObjectAwareBom(userData);

        BusinessPartnerManager bupaManager = catObjBom.createBUPAManager();
        BusinessPartner soldTo = bupaManager.getDefaultBusinessPartner(PartnerFunctionData.SOLDTO);

        catalogStatus = determineCatalogStatus(request);

        CatalogConfiguration config = getCatalogConfiguration(userData);

        if (config == null || config.getCatalog() == null) {
            log.error("CatConfig not found");
            if (log.isInfoEnabled()) {
                long millis = System.currentTimeMillis() - start;
                log.info("Elapsed time in ms: " + millis);
            }
            log.exiting();
            return mapping.findForward(ActionConstants.FW_ERROR);
        }
        else if (!config.isInternalCatalogAvailable()) {
            log.debug("no catalog");
            if (log.isInfoEnabled()) {
                long millis = System.currentTimeMillis() - start;
                log.info("Elapsed time in ms: " + millis);
            }
            log.exiting();
            // interal catlog isn't used !!
            return (mapping.findForward("nocatalog"));
        }
        // if catalog not initialized, do it now.
        // if catalog has changed (should not happen with the current implementation!)
        // if catalog status has changed (should not happen with the current implementation!)
        // initialize the new catalog.
        else if (
            theCatalog == null
                || !theCatalog.getCatalog().getGuid().trim().equals(config.getCatalog())
                || theCatalog.getCatalogClient().getCatalogStatus() != catalogStatus
                || areViewsDifferent(theCatalog, config) // don't know yet if really needed || catalogStatus == IClient.CATALOG_STATE_STAGING
        ) {
            String catalogGuid = config.getCatalog();

            if (theCatalog == null) {
                log.debug("New Catalog: " + catalogGuid);
            }
            else if (!theCatalog.getCatalog().getGuid().equals(catalogGuid)) {
                log.warn("Catalog changed from {0} to {1}!", new Object[] { theCatalog.getCatalog().getGuid(), catalogGuid }, null);
            }
            else {
                log.warn(
                    "Catalog status changed from {0} to {1}!",
                    new Object[] { String.valueOf(theCatalog.getCatalogClient().getCatalogStatus()), String.valueOf(catalogStatus)},
                    null);
            }

            if (config.getApplication().equals(BaseConfiguration.B2B)) {
                try {
                    CatalogUser user = catObjBom.getCatalogUser();
                    Boolean perm = user.hasPermission("catalog", "Display prices");

                    if (!perm.booleanValue()) {
                        UIController uiController = UIController.getController(userData);
                        UIElement uiElement = uiController.getUIElement("catalog.prices");
                        uiElement.setAllowed(false);
                    }
                }
                catch (CommunicationException e) {
                    log.warn("CommunicationException occured in ISAEntryAction");
                }
            }

            Locale theUserLocale = userData.getLocale();
            log.debug("ISAEntryAction:" + theUserLocale.toString());

            //this.getServlet().log("OCIEntryAction:" + theUserLocale.toString());
            // Create the locale for the User
            CatalogClient aCatalogClient = new CatalogClient(theUserLocale, null, null, config.getViews(), catalogStatus);
            CatalogServer aCatalogServer = new CatalogServer(null, catalogGuid);

            if (catalogStatus == IClient.CATALOG_STATE_INACTIVE) {
                selectInactiveStagingCat = true;
            }

            theCatalog = bom.createCatalog(aCatalogClient, aCatalogServer, (InitializationEnvironment) servlet);

            if (!selectInactiveStagingCat) {
                // if the catalog could not be initialized: Error!
                if (theCatalog == null || !theCatalog.isValid()) {

                    generateErrorMessage(request);
                    log.debug("no valid catalog");
                    if (log.isInfoEnabled()) {
                        long millis = System.currentTimeMillis() - start;
                        log.info("Elapsed time in ms: " + millis);
                    }
                    log.exiting();
                    return mapping.findForward("message");
                }

                if (theCatalog != null && theCatalog.isValid()) {
                    log.debug("ISAEntryAction: Created catalog " + theCatalog);
                }
                else {
                    log.error("system.initFailed", new String[] { "catalog" }, new Exception("Error creating catalog"));

                    generateErrorMessage(request);
                    if (log.isInfoEnabled()) {
                        long millis = System.currentTimeMillis() - start;
                        log.info("Elapsed time in ms: " + millis);
                    }
                    log.exiting();
                    return mapping.findForward("message");
                }
            }
            else {
                log.debug("catalog validtiy not checked, due to open read of inactive staging cat");
            }

            // ensure that pricing calculation is re-initialized
            bom.releasePriceCalculator();
        }

        //  pass the sales org data for getting the exchange product attributes
        /* soldto bug fix for exchange products */
        if (soldTo != null) {
            SoldTo soldToPf = (SoldTo) soldTo.getPartnerFunctionObject(PartnerFunctionData.SOLDTO);
            String debit_credit_strategy = soldToPf.getDepositRefundStrategy(soldTo);
            log.debug("Catalog Debit_Credit_Strategy set to: " + debit_credit_strategy);
            theCatalog.setDebit_Credit_Strategy(debit_credit_strategy);
        }

        setCatalogShopAttributes(theCatalog, catObjBom);

        theCatalog.setHookUrl(request.getParameter("catHookUrl"));

        if (!selectInactiveStagingCat) {
            createPriceCalculator(userData, request, theCatalog, bom, catObjBom, soldTo, null);
        }

        //MIG handling in Catalog
        //If a MIG was provided add the populated campaign object to the catalog
        try {
            addCampaignToCatalogue(theCatalog, userData, catObjBom, log);
        }
        catch (CommunicationException e) {
            log.warn("CommunicationException occured in ISAEntryAction while adding campaign to catalog");
        }

        setCatalogXCMAttributes(request, theCatalog, bom);

        ActionForward forward = null;

        // check if we must read an inactive staging catalogue
        if (selectInactiveStagingCat) {
            forward = mapping.findForward("selectInactiveCat");
            log.debug("Forward to read inactive staging catalog infos");
        }
        else if (request.getParameter("areaID") != null) {
            forward = mapping.findForward("directDisplay");
        }

        // The ws request parameter contains the name of the called web service.
        String webServiceAction = request.getParameter(Constants.PARAM_WEB_SERVICE);
        if (webServiceAction != null && !"".equals(webServiceAction.trim())) {
            forward = mapping.findForward(webServiceAction);
        }

        if (forward == null) {
            forward = mapping.findForward(ActionConstants.FW_SUCCESS);
        }

        if (log.isInfoEnabled()) {
            long millis = System.currentTimeMillis() - start;
            log.info("Elapsed time in ms: " + millis);
        }
        log.exiting();
        return (forward);
    }

    /**
     * Check if Catalog views and shop views differ
     * 
     * @return boolena true if views differ, false else
     */
    protected boolean areViewsDifferent(WebCatInfo theCatalog, CatalogConfiguration config) {

        
        boolean viewsDiffer = true;

        ArrayList shopViews = new ArrayList(0);
        ArrayList catViewList = new ArrayList(0);
		ArrayList catViewListTemp =  new ArrayList(0);

        if (config != null && config.getViews() != null) {
            shopViews = new ArrayList(Arrays.asList(config.getViews()));
        }

		if (theCatalog != null && theCatalog.getCatalogClient() != null && 
			theCatalog.getCatalogClient().getViews() != null) {
			catViewListTemp = theCatalog.getCatalogClient().getViews();
			Iterator viewObjIter = catViewListTemp.iterator();
			while (viewObjIter.hasNext()){
			  CatalogView viewObj = (CatalogView)viewObjIter.next();	
			  String guid = viewObj.getGuid();
			  catViewList.add(guid);
        }
		}

        if (shopViews.size() == catViewList.size() && shopViews.containsAll(catViewList)) {
            viewsDiffer = false;
        }

        if (log.isDebugEnabled()) {
            log.debug("Shop views : " + shopViews.toString());
            log.debug("Catgalog views : " + catViewList.toString());
            log.debug("View comparison returned : " + viewsDiffer);
        }

        return viewsDiffer;
    }

    private int determineCatalogStatus(HttpServletRequest request) {

        int retVal = IClient.CATALOG_STATE_ACTIVE;

        if (getInteractionConfig(request) != null && getInteractionConfig(request).getConfig(ActionConstants.IN_WEBCATALOG_CONFIG_ID) != null) {
            InteractionConfig catalogConf = getInteractionConfig(request).getConfig(ActionConstants.IN_WEBCATALOG_CONFIG_ID);

            String catalogStatusStr = catalogConf.getValue(ActionConstants.IN_CATSTATUS);
            if (catalogStatusStr != null) {
                if (catalogStatusStr.equals(ActionConstants.CATSTATUS_ACTIVE)) {
                    retVal = IClient.CATALOG_STATE_ACTIVE;
                    log.debug("catalog status active");
                }
                else if (catalogStatusStr.equals(ActionConstants.CATSTATUS_INACTIVE)) {
                    retVal = IClient.CATALOG_STATE_INACTIVE;
                    log.debug("catalog status inactive");
                }
                else {
                    if (log.isDebugEnabled()) {
                        log.debug("catalog status " + catalogStatusStr + " not known");
                    }
                }
            }
        }
        return retVal;
    }

    /**
     * creates the PriceCalculator object for the catalog
     */
    protected void createPriceCalculator(
        UserSessionData userSessionData,
        HttpServletRequest request,
        WebCatInfo theCatalog,
        CatalogBusinessObjectManager bom,
        CatalogBusinessObjectsAware catObjBom,
        BusinessPartner soldTo,
        Date ipcStagingDate) {

        // get the price calculator instance
        PriceCalculator priceCalc = bom.getPriceCalculator();
        CatalogConfiguration config = catObjBom.getCatalogConfiguration();

        try {
            if (priceCalc == null) {
                log.debug("Init PriceCalculator");
                priceCalc = bom.createPriceCalculator();

                PriceCalculatorInitData initData = priceCalc.createInitData();

                ServletContext context = this.getServlet().getServletContext();

                initData.setPriceAnalysisEnabled(config.isPricingCondsAvailable() && !config.isNoPriceUsed());
                initData.setShopID(config.getId());

                InteractionConfigContainer icc = com.sap.isa.core.FrameworkConfigManager.Servlet.getInteractionConfig(request.getSession());
                InteractionConfig ic = icc.getConfig("ui");

                if (ic != null) {
                    initData.setPriceAnalysisURL(ic.getValue("url.priceAnalysis"));
                }

                if (config.isNoPriceUsed()) {
                    initData.setStrategy(PriceCalculatorBackend.NO_PRICING);
                }
                else if (config.isListPriceUsed() && !config.isIPCPriceUsed()) {
                    initData.setStrategy(PriceCalculatorBackend.STATIC_PRICING);
                }
                else if (!config.isListPriceUsed() && config.isIPCPriceUsed()) {
                    initData.setStrategy(PriceCalculatorBackend.DYNAMIC_PRICING);
                }
                else {
                    log.debug("Entered to Static and Dynamic Pricing Block");
                    initData.setStrategy(PriceCalculatorBackend.STATIC_DYNAMIC_PRICING);
                }

                log.debug("PriceCaluclator Strategy set to: " + initData.getStrategy());

                if (soldTo != null) {
                    //initData.setSoldToID(soldTo.getId());
                    AppBaseBusinessObjectManager appBaseBom = (AppBaseBusinessObjectManager) userSessionData.getBOM(AppBaseBusinessObjectManager.APPBASE_BOM);
                    IdMapper idMapper = appBaseBom.createIdMapper();
                    String mappedSoldToId = idMapper.mapIdentifier(IdMapper.SOLDTO, IdMapper.USER, IdMapper.CATALOG, soldTo.getId());
                    initData.setSoldToID(mappedSoldToId);
                }

                initData.setCatalog(bom.getCatalog().getCatalog());
                initData.setDecimalSeparator(config.getDecimalSeparator());
                initData.setGroupingSeparator(config.getGroupingSeparator());
                initData.setCondPurpGrp(theCatalog.getCondPurpGroup());
                log.debug(initData); // use the toString() method of initData
                //D040230:060607 - Note 1063581
                //START==>
                readPriceDecimalPlaceSettings(request, initData);
                //<==END                
                priceCalc.setInitData(initData);
                priceCalc.init();

                // an ipc date for catalog staginmg test has been provided
                if (ipcStagingDate != null) {
                    priceCalc.setIpcDate(ipcStagingDate);
                }

                theCatalog.setPriceCalculator(priceCalc);
            }
        }
        catch (Exception ex) {
            log.error("Initialization of priceCalculator failed", ex);
        }
    }

    /**
     * Set the catalog relevant attributes coming from the shop
     */
    protected void setCatalogShopAttributes(WebCatInfo theCatalog, CatalogBusinessObjectsAware catObjBom) {

        CatalogConfiguration config = catObjBom.getCatalogConfiguration();

        if (config.isContractAllowed()) {
            theCatalog.setContractDisplayMode(ContractDisplayMode.ALL_CONTRACTS);
        }
        else {
            theCatalog.setContractDisplayMode(ContractDisplayMode.NO_CONTRACTS);
        }

        String SalesOrganisation = config.getSalesOrganisation();
        theCatalog.setSalesOrganisation(SalesOrganisation);

        String distributionChannel = config.getDistributionChannel();
        theCatalog.setDistributionChannel(distributionChannel);

        String division = config.getDivision();
        theCatalog.setDivision(division);

        String language = config.getLanguage();
        String languageIso = config.getLanguageIso();

        theCatalog.setLanguage(language);
        theCatalog.setLanguageIso(languageIso);

        log.debug("Catalog contract display mode set to: " + theCatalog.getContractDisplayMode());
        log.debug("Catalog sales org set to: " + theCatalog.getSalesOrganisation());
        log.debug("Catalog division set to: " + theCatalog.getDivision());
        log.debug("Catalog language set to: " + theCatalog.getLanguage());
        log.debug("Catalog iso language set to: " + theCatalog.getLanguageIso());

        ResultData condPurpGrp = null;
        try {
            config.readCondPurpsTypes(language);
            condPurpGrp = config.getCondPurpTypes();
            theCatalog.setCondPurpGroup(condPurpGrp);
        }
        catch (Exception ex) {
            log.error("exception is " + ex);
        }
    }

    /**
     * Set the catalog relevant attributes coming from XCM
     */
    protected void setCatalogXCMAttributes(HttpServletRequest request, WebCatInfo theCatalog, CatalogBusinessObjectManager bom) {

        String maxPageLinks;
        String maxItems;

        boolean maxPageLinksfound = false;
        boolean maxItemsfound = false;
        int pageSize = 5; //the default if everything else fails...
        int noPageLinks = 10; //the default if everything else fails...
        int blockViewMaxCols = 2; //the default if everything else fails...
        boolean catAreaShowAsBlockView = false; //the default if everything else fails...
        int catAreaMaxRowsBV = 1; //the default if everything else fails...
        boolean catSearchShowAsBlockView = false; //the default if everything else fails...
        int catSearchMaxRowsBV = 1; //the default if everything else fails...
        boolean bestsellerShowAsBlockView = false; //the default if everything else fails...
        int bestsellerMaxRowsBV = 1; //the default if everything else fails...
        boolean recommendationsShowAsBlockView = false; //the default if everything else fails...
        int recommendationsMaxRowsBV = 1; //the default if everything else fails...
        boolean automaticPackageExplosion = false; //the default if everything else fails...

        if (getInteractionConfig(request) != null && getInteractionConfig(request).getConfig(ActionConstants.IN_UI_CONFIG_ID) != null) {
            InteractionConfig uiConf = getInteractionConfig(request).getConfig(ActionConstants.IN_UI_CONFIG_ID);

            maxPageLinks = uiConf.getValue(ActionConstants.IN_MAX_PAGE_LINKS);
            if (maxPageLinks != null) {
                try {
                    noPageLinks = Integer.parseInt(maxPageLinks);
                    maxPageLinksfound = true;
                }
                catch (NumberFormatException e) {
                    log.warn("invalid Catalog.Page.MaxPageLinks " + maxPageLinks);
                }
            }

            maxItems = uiConf.getValue(ActionConstants.IN_LISTVIEW_MAXITEMS);
            if (maxItems != null) {
                try {
                    pageSize = Integer.parseInt(maxItems);
                    maxItemsfound = true;
                }
                catch (NumberFormatException e) {
                    log.warn("invalid Catalog.ListView.maxItems " + maxItems);
                }
            }

            // Block View - MaxColums 
            String maxCols = uiConf.getValue(ActionConstants.IN_UI_CAT_MAXCOLS);
            if (maxCols != null) {
                try {
                    blockViewMaxCols = Integer.parseInt(maxCols);
                }
                catch (NumberFormatException e) {
                    log.warn("invalid Catalog.BlockView.MaxColumns " + maxCols);
                }
            }

            String blockView;
            String maxRows;
            String pgHeader;

            // Block-View - Catalog Area 
            blockView = uiConf.getValue(ActionConstants.IN_UI_CATAREA_BLOCKVIEW);
            if (blockView != null) {
                catAreaShowAsBlockView = blockView.equals("true");
            }
            maxRows = uiConf.getValue(ActionConstants.IN_UI_CATAREA_MAXROWS);
            if (maxRows != null) {
                try {
                    catAreaMaxRowsBV = Integer.parseInt(maxRows);
                }
                catch (NumberFormatException e) {
                    log.warn("invalid CatalogArea.BlockView.MaxRows " + maxRows);
                }
            }

            // Block-View - Catalog Search 
            blockView = uiConf.getValue(ActionConstants.IN_UI_CATSEARCH_BLOCKVIEW);
            if (blockView != null) {
                catSearchShowAsBlockView = blockView.equals("true");
            }
            maxRows = uiConf.getValue(ActionConstants.IN_UI_CATSEARCH_MAXROWS);
            if (maxRows != null) {
                try {
                    catSearchMaxRowsBV = Integer.parseInt(maxRows);
                }
                catch (NumberFormatException e) {
                    log.warn("invalid CatalogArea.BlockView.MaxRows " + maxRows);
                }
            }

            // Block-View - Bestseller
            blockView = uiConf.getValue(ActionConstants.IN_UI_GLOBAL_BLOCKVIEW);
            if (blockView != null) {
                bestsellerShowAsBlockView = blockView.equals("true");
            }
            maxRows = uiConf.getValue(ActionConstants.IN_UI_GLOBAL_MAXROWS);
            if (maxRows != null) {
                try {
                    bestsellerMaxRowsBV = Integer.parseInt(maxRows);
                }
                catch (NumberFormatException e) {
                    log.warn("invalid CatalogArea.BlockView.MaxRows " + maxRows);
                }
            }

            // Block-View - Recommendations 
            blockView = uiConf.getValue(ActionConstants.IN_UI_RECOM_BLOCKVIEW);
            if (blockView != null) {
                recommendationsShowAsBlockView = blockView.equals("true");
            }
            maxRows = uiConf.getValue(ActionConstants.IN_UI_RECOM_MAXROWS);
            if (maxRows != null) {
                try {
                    recommendationsMaxRowsBV = Integer.parseInt(maxRows);
                }
                catch (NumberFormatException e) {
                    log.warn("invalid CatalogArea.BlockView.MaxRows " + maxRows);
                }
            }
        }

        if (getInteractionConfig(request) != null && getInteractionConfig(request).getConfig(ActionConstants.IN_WEBCATALOG_CONFIG_ID) != null) {
            InteractionConfig catalogConf = getInteractionConfig(request).getConfig(ActionConstants.IN_WEBCATALOG_CONFIG_ID);

            if (bom.getCatalog().getHttpsImageServer() == null) {
                String httpsImageServer = catalogConf.getValue(ActionConstants.IN_HTTPS_IMAGE_SERVER);

                if (httpsImageServer == null) {
                    httpsImageServer = "";
                }

                bom.getCatalog().setHttpsImageServer(httpsImageServer);
                log.debug(ActionConstants.IN_HTTPS_IMAGE_SERVER + " was set to:" + httpsImageServer);
            }
            else {
                log.debug(ActionConstants.IN_HTTPS_IMAGE_SERVER + " is already set to:" + bom.getCatalog().getHttpsImageServer());
            }

            if (bom.getCatalog().getImageServer() == null) {
                String imageServer = catalogConf.getValue(ActionConstants.IN_IMAGE_SERVER);

                if (imageServer == null) {
                    imageServer = "";
                }

                bom.getCatalog().setImageServer(imageServer);
                log.debug(ActionConstants.IN_IMAGE_SERVER + " was set to:" + imageServer);
            }
            else {
                log.debug(ActionConstants.IN_IMAGE_SERVER + " is already set to:" + bom.getCatalog().getImageServer());
            }

            String maxSearchHits = catalogConf.getValue(ActionConstants.IN_MAX_SEARCH_HITS);
            if (maxSearchHits != null) {
                try {
                    CatalogParameters.maxSearchResults = Integer.parseInt(maxSearchHits);
                }
                catch (NumberFormatException e) {
                    log.warn("invalid maxSearchHits " + maxSearchHits);
                }
            }

            if (!maxPageLinksfound) {
                maxPageLinks = catalogConf.getValue(ActionConstants.IN_NUM_PAGELINKS);
                if (maxPageLinks != null) {
                    try {
                        noPageLinks = Integer.parseInt(maxPageLinks);
                    }
                    catch (NumberFormatException e) {
                        log.warn("invalid numPageLinks " + maxPageLinks);
                    }
                }
            }

            if (!maxItemsfound) {
                maxItems = catalogConf.getValue(ActionConstants.IN_ITEMPAGE_SIZE);
                if (maxItems != null) {
                    try {
                        pageSize = Integer.parseInt(maxItems);
                    }
                    catch (NumberFormatException e) {
                        log.warn("invalid itemPageSize " + maxItems);
                    }
                }
            }

            // Automatical package explosion  
            String automExplosion;
            automExplosion = catalogConf.getValue(ActionConstants.IN_AUTOM_PACK_EXPLOSION);
            if (automExplosion != null) {
                automaticPackageExplosion = automExplosion.equals("true");
            }
        }

        theCatalog.setNumPageLinks(noPageLinks);
        log.debug("numPageLinks=" + noPageLinks);
        theCatalog.setItemPageSize(pageSize);
        theCatalog.setItemPageSizeMin(pageSize);
        log.debug("itemPageSize=" + pageSize);

        theCatalog.setBlockViewMaxCols(blockViewMaxCols);
        log.debug("blockViewMaxCols=" + blockViewMaxCols);

        theCatalog.setCatAreaShowAsBlockView(catAreaShowAsBlockView);
        log.debug("catAreaShowAsBlockView=" + catAreaShowAsBlockView);
        theCatalog.setPageSizeCatAreaBV(catAreaMaxRowsBV * blockViewMaxCols);
        theCatalog.setPageSizeCatAreaBVMin(catAreaMaxRowsBV * blockViewMaxCols);
        log.debug("catAreaMaxRowsBV=" + catAreaMaxRowsBV);

        theCatalog.setCatSearchShowAsBlockView(catSearchShowAsBlockView);
        log.debug("catSearchShowAsBlockView=" + catSearchShowAsBlockView);
        theCatalog.setPageSizeCatSearchBV(catSearchMaxRowsBV * blockViewMaxCols);
        theCatalog.setPageSizeCatSearchBVMin(catSearchMaxRowsBV * blockViewMaxCols);
        log.debug("catSearchMaxRowsBV=" + catSearchMaxRowsBV);

        theCatalog.setBestsellerShowAsBlockView(bestsellerShowAsBlockView);
        log.debug("bestsellerShowAsBlockView=" + bestsellerShowAsBlockView);
        theCatalog.setPageSizeBestsellerBV(bestsellerMaxRowsBV * blockViewMaxCols);
        theCatalog.setPageSizeBestsellerBVMin(bestsellerMaxRowsBV * blockViewMaxCols);
        log.debug("bestsellerMaxRowsBV=" + bestsellerMaxRowsBV);

        theCatalog.setRecommShowAsBlockView(recommendationsShowAsBlockView);
        log.debug("recommendationsShowAsBlockView=" + recommendationsShowAsBlockView);
        theCatalog.setPageSizeRecommBV(recommendationsMaxRowsBV * blockViewMaxCols);
        theCatalog.setPageSizeRecommBVMin(recommendationsMaxRowsBV * blockViewMaxCols);
        log.debug("recommendationsMaxRowsBV=" + recommendationsMaxRowsBV);

        theCatalog.setAutomaticPackageExplosion(automaticPackageExplosion);
        log.debug("automaticPackageExplosion=" + automaticPackageExplosion);
    }

    protected void generateErrorMessage(HttpServletRequest request) {
        MessageDisplayer message = new MessageDisplayer();
        message.addMessage(new Message(Message.ERROR, "catalog.exception.usermsg"));
        message.setOnlyLogin();
        message.addToRequest(request);
    }

    /**
     * Reads the Parameter priceDecimalPlace of XCM-Component webcatdata. 
     */
    private void readPriceDecimalPlaceSettings(HttpServletRequest request, PriceCalculatorInitData initData) {
        //D040230:060607 - Note 1063581
        if (getInteractionConfig(request) != null && getInteractionConfig(request).getConfig(ActionConstants.IN_WEBCATALOG_CONFIG_ID) != null) {
            InteractionConfig catalogConf = getInteractionConfig(request).getConfig(ActionConstants.IN_WEBCATALOG_CONFIG_ID);
            String priceDecimalPlace = catalogConf.getValue(ActionConstants.IN_PRICE_DECIMAL_PLACE);
            if (priceDecimalPlace != null) {
                try {
                    if (initData != null) {
                        boolean showDecimalPlaces = Boolean.valueOf(priceDecimalPlace).booleanValue();
                        initData.setStripDecimalPlaces(showDecimalPlaces);
                    }
                }
                catch (Throwable t) {
                    log.debug("can't set " + ActionConstants.IN_PRICE_DECIMAL_PLACE + "cause of Error.", t);
                }
            }
        }
    }

    /**
     * Add the given campaign guid to the catalogue. <br>
     * 
     * @param campaignCode
     * @param theCatalog
     * @param userSessionData
     * @param bom
     * @throws CommunicationException
     */
    static public void addCampaignToCatalogue(WebCatInfo theCatalog, UserSessionData userSessionData, CatalogBusinessObjectsAware catObjBom, IsaLocation log)
        throws CommunicationException {

        CampaignBusinessObjectManager campaignBom = (CampaignBusinessObjectManager) userSessionData.getBOM(CampaignBusinessObjectManager.CAMPAIGN_BOM);
        Campaign campaign = campaignBom.getCampaign();

        if (campaign == null) {
            log.debug("Campaign object null - return");
            return;
        }

        CampaignHeaderData campaignHeader = campaign.getHeaderData();

        if (campaignHeader == null) {
            log.debug("campaignHeader object null - return");
            return;
        }

        String campaignCode = campaignHeader.getCampaignID();

        boolean campainWasValid = theCatalog.getCampaignGuid() != null && theCatalog.isSoldToEligible();

        log.debug("Old Campaign validity :" + campainWasValid + "  Guid != null: " + (theCatalog.getCampaignGuid() != null) + " SoldTo Eligible:" + theCatalog.isSoldToEligible());

        // setting useful defaults
        theCatalog.setCampaignId(campaignCode);
        theCatalog.setSoldToEligible(false);
        theCatalog.setCampaignGuid(null);

        // repricing is true, if the campaign was valid.
        // if the campaign wasn't invalid, it depends if the new camapign is valid
        // this check will done below.      
        boolean repricing = campainWasValid;

        log.debug("CampaignId: " + campaignCode);
        log.debug("campaignHeader.isValid(): " + campaignHeader.isValid());

        if (campaignCode != null && !campaignCode.equals("") && campaignHeader.isValid()) {

            BusinessPartnerManager bupaManager = catObjBom.createBUPAManager();
            BusinessPartner soldTo = bupaManager.getDefaultBusinessPartner(PartnerFunctionData.SOLDTO);

            TechKey soldToGuid = soldTo != null ? soldTo.getTechKey() : null;

            log.debug("campaignHeader.isPublic(): " + campaignHeader.isPublic());

            if (!campaignHeader.isPublic()) {
                log.debug("Private Campaign");
                MessageList messages = campaignHeader.getMessageList();
                messages.remove("catalog.isa.camp.priv.logon");
                if (null != soldToGuid && !soldToGuid.isInitial()) { //Sold-to available
                    CatalogConfiguration catConfig = catObjBom.getCatalogConfiguration();
                    if (soldToGuid != null) {
                        log.debug("Campaign check eligibilty");
                        campaign.checkPrivateCampaignEligibility(
                            soldToGuid,
                            catConfig.getSalesOrganisation(),
                            catConfig.getDistributionChannel(),
                            catConfig.getDivision(),
                            new Date());
                        if (campaignHeader.isValid()) { // Sold-to is eligible
                            log.debug("Campaign is eligible set values");
                            theCatalog.setCampaignGuid(campaignHeader.getTechKey());
                            theCatalog.setSoldToEligible(true);
                        }
                    }
                }
                else {
                    log.debug("Campaign is private but no soldTo available");
                    Message msg = new Message(Message.WARNING, "catalog.isa.camp.priv.logon", new String[] { campaignHeader.getCampaignID().toUpperCase()}, "");
                    messages.add(msg);
                }
            }

            if (campaignHeader.isPublic()) {
                log.debug("Public Campaign set values");
                theCatalog.setCampaignGuid(campaignHeader.getTechKey());
                //Public Campaign, Sold-to not mandatory
                theCatalog.setSoldToEligible(true);
            }
        }

        // repricing is true if the campaign is valid.      
        if (theCatalog.getCampaignGuid() != null && theCatalog.isSoldToEligible()) {
            repricing = true;
            log.debug("Set repricing true");
        }

        if (repricing) {
            WebCatItemList itemList = theCatalog.getCurrentItemList();

            //  Items need to be re-priced 
            if (itemList != null && itemList.getItems() != null) {
                Iterator iter = itemList.getItems().iterator();
                while (iter.hasNext()) {
                    WebCatItem item = (WebCatItem) iter.next();
                    item.setItemPrice(null);
                }
            }
        }
    }

}