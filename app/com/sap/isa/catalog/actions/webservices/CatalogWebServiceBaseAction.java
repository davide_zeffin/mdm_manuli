package com.sap.isa.catalog.actions.webservices;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;

import com.sap.ecommerce.boi.campaign.CampaignHeaderData;
import com.sap.ecommerce.businessobject.campaign.Campaign;
import com.sap.ecommerce.businessobject.campaign.CampaignBusinessObjectManager;
import com.sap.isa.backend.boi.isacore.contract.ContractView;
import com.sap.isa.backend.boi.webcatalog.CatalogConfiguration;
import com.sap.isa.backend.boi.webcatalog.pricing.PriceInfo;
import com.sap.isa.backend.boi.webcatalog.pricing.PriceType;
import com.sap.isa.businessobject.CommunicationException;
import com.sap.isa.businessobject.CurrencyConverter;
import com.sap.isa.businessobject.UOMConverter;
import com.sap.isa.businessobject.contract.ContractReferenceMap;
import com.sap.isa.businessobject.webcatalog.CatalogBusinessObjectManager;
import com.sap.isa.businessobject.webcatalog.contract.ContractReferenceRequest;
import com.sap.isa.businesspartner.backend.boi.PartnerFunctionData;
import com.sap.isa.businesspartner.businessobject.BusinessPartner;
import com.sap.isa.businesspartner.businessobject.BusinessPartnerManager;
import com.sap.isa.catalog.AttributeKeyConstants;
import com.sap.isa.catalog.actions.CatalogBaseAction;
import com.sap.isa.catalog.boi.CatalogException;
import com.sap.isa.catalog.impl.CatalogAttribute;
import com.sap.isa.catalog.webcatalog.WebCatArea;
import com.sap.isa.catalog.webcatalog.WebCatInfo;
import com.sap.isa.catalog.webcatalog.WebCatItem;
import com.sap.isa.catalog.webcatalog.WebCatItemPrice;
import com.sap.isa.core.TechKey;
import com.sap.isa.core.UserSessionData;
import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.core.util.Message;
import com.sap.isa.core.util.MessageList;
import com.sap.isa.core.util.WebUtil;
import com.sap.isa.webservice.catalog.types.CatalogServiceAreaData;
import com.sap.isa.webservice.catalog.types.CatalogServiceAreaLOCDef;
import com.sap.isa.webservice.catalog.types.CatalogServiceContractBasisInfo;
import com.sap.isa.webservice.catalog.types.CatalogServiceItemData;
import com.sap.isa.webservice.catalog.types.CatalogServiceLOCFixedVal;
import com.sap.isa.webservice.catalog.types.CatalogServiceMimesData;
import com.sap.isa.webservice.catalog.types.CatalogServiceRequestBase;
import com.sap.isa.webservice.catalog.types.CatalogServiceResponseBaseData;
import com.sap.isa.webservice.catalog.types.CatalogServiceTextData;
import com.sap.isa.webservice.catalog.types.Log;
import com.sap.isa.webservice.catalog.types.LogItem;


/**
 * Title:        CatalogWebServiceBaseAction
 * Description:  Implements the common functionalitiy or catalog Web Services
 * Copyright:    Copyright (c) 2008
 * Company:      SAP AG, Germany
 * @version 1.0
 */
public class CatalogWebServiceBaseAction extends CatalogBaseAction {
    
    public static final String ATTRIB_TEXT_001 = "TEXT_001";
   
    /**
     * Fill the area texts with TEXT_001 for the response, if present
     * 
     * @param theArea the catalog area to get the text from
     * @param respArea the area response object to add the text data to
     */
    protected void fillAreaTextsResp(WebCatArea theArea, CatalogServiceAreaData respArea) {
        
        log.entering("fillAreaTextsResp()");
            
        if (theArea.getCategory().getDescription() != null) {
            CatalogServiceTextData[] respTexts = new CatalogServiceTextData[1];
            CatalogServiceTextData respText = new CatalogServiceTextData();
            
            respText.attribId = ATTRIB_TEXT_001;
            respText.text = theArea.getCategory().getDescription();
            
            respTexts[0] = respText;
            
            respArea.textData = respTexts;
        }
        
        log.exiting();
    }
    
    /**
     * Fill the area mimes with DOC_PC_CRM_IMAGE for the response, if present
     * 
     * @param theArea the catalog area to get the image from
     * @param respArea the area response object to add the image data to
     */
    protected void fillAreaPictureResp(WebCatArea theArea, CatalogServiceAreaData respArea) {
        
        log.entering("fillAreaPictureResp()");
            
        try {
            if (theArea.getCategory().getDetail(AttributeKeyConstants.AN_PICTURE1) != null) {
                CatalogServiceMimesData[] respMimes = new CatalogServiceMimesData[1];
                CatalogServiceMimesData respMime = new CatalogServiceMimesData();
                
                respMime.attribId = AttributeKeyConstants.AN_PICTURE1;
                String[] theValues = theArea.getCategory().getDetail(AttributeKeyConstants.AN_PICTURE1).getAllAsString();
                if (theValues!= null && theValues.length >0) {
                    respMime.relMimePath = theValues[0];
                }
            
                respMimes[0] = respMime;
                
                respArea.mimeData = respMimes;
            }
        } catch (CatalogException e) {
            log.error("Exception occured : " + e.getMessage());
            log.error(e.getStackTrace().toString());
        }
        
        log.exiting();
    }
      
    /**
     * Fill area specific fxed value LOC data for the response
     * 
     * @param respAttrib the attribute response object to fill
     * @param the catalog atttribute to get the data from
     */
    protected void fillAreaLocFixedValuesResp(CatalogServiceAreaLOCDef respAttrib, CatalogAttribute attrib) {
        
        log.entering("fillAreaLocFixedValuesResp()");
            
        CatalogServiceLOCFixedVal[] fixedVals;
        CatalogServiceLOCFixedVal fixedVal;
        Iterator fixedValIter;
        
        if (attrib.getFixedValuesSize() > 0) {
            
            fixedVals = new CatalogServiceLOCFixedVal[attrib.getFixedValuesSize()];
            
            fixedValIter = attrib.getFixedValues();
            
            int k = 0;
        
            while (fixedValIter.hasNext()) {
                List defVal = (List) fixedValIter.next();
        
                fixedVal = new CatalogServiceLOCFixedVal();
                
                fixedVal.id = (String) defVal.get(0);
                if (defVal.size() > 1) {
                    fixedVal.description = (String) defVal.get(1);
                }
        
                fixedVals[k] = fixedVal;
                
                k++;
            }
            
            respAttrib.fixedValues = fixedVals;
        }
        
        log.exiting();
        
    }
    
    /**
     * Fills area specific LOC data for the response
     * 
     * @param theArea the catalog area to get the loc data from
     * @param respArea the area response object to add the loc data to
     */
    protected void fillAreaLocResp(WebCatArea area, CatalogServiceAreaData respArea) {
        
        log.entering("fillAreaLocResp()");
            
        CatalogServiceAreaLOCDef[] respAttribs;
        CatalogServiceAreaLOCDef respAttrib;
        CatalogAttribute attrib;
        Iterator attribIter = null;
        
        if (area.getCategory().getAttributeSize() > 0) {
            try {
                attribIter = area.getCategory().getAttributes();
            } 
            catch (CatalogException catEx) {
                log.error("Exception occured " + catEx.getMessage());
                log.error(catEx.getStackTrace().toString());
            }
            
            respAttribs = new CatalogServiceAreaLOCDef[area.getCategory().getAttributeSize()];
            
            int j = 0;
            
            while (attribIter.hasNext()) {
                attrib = (CatalogAttribute) attribIter.next();
                
                respAttrib = new CatalogServiceAreaLOCDef();
                
                respAttrib.locInfo.description = attrib.getDescription();
                respAttrib.locInfo.id = attrib.getName();
                
                fillAreaLocFixedValuesResp(respAttrib, attrib);
        
                respAttribs[j] = respAttrib;
                
                j++;
            }
            
            respArea.locData = respAttribs;
        }
        
        log.exiting();
    }
    
    /**
     * Fill area relevant data for the response
     * 
     * @param the catalog area to get the data from
     * @param fillLocData should area specific loc data be filled
     * 
     * @return CatalogServiceAreaData the filled arae response object
     */
    protected CatalogServiceAreaData fillAreaResp(WebCatArea area, boolean fillLocData) {
        
        log.entering("fillAreaResp()");
            
        CatalogServiceAreaData respArea;
        respArea = new CatalogServiceAreaData();
        
        respArea.areaKey = area.getAreaID();
        respArea.description = area.getAreaName();
        if (area.getParentArea() != null) {
            respArea.parentAreaKey = area.getParentArea().getAreaID();
        }
        else {
            respArea.parentAreaKey = "";
        }
        
        if (fillLocData) {
            fillAreaLocResp(area, respArea);
        }
        
        log.exiting();

        return respArea;
    }

    /**
     * Fill item price relevant data for the response
     * 
     * @param returnPrices flag, to indicate if prices should be returned/filled
     * @param respItem the response item object the price data should be filled into
     * @param currConverter the currency converter to get the ISO currency code from
     * @param webCatItemPrice the prices to fill into the response
     */
    protected void fillItemPriceResp(boolean returnPrices, CatalogServiceItemData respItem,
                                     CurrencyConverter currConverter,  WebCatItemPrice webCatItemPrice)
        throws CommunicationException {
            
        log.entering("fillItemPriceResp()");
            
        PriceType[] priceTypeArray;
        int[] showPriceInfoArray;
        PriceType priceType;
        PriceInfo[] priceInfos;
        PriceInfo priceInfo;
        
        if (returnPrices && webCatItemPrice != null && webCatItemPrice.getPriceInfo() != null && 
            webCatItemPrice.getPriceInfo().getPriceType() != null) {
        
            priceType = webCatItemPrice.getPriceInfo().getPriceType();
            
            if (log.isDebugEnabled()) {
                log.debug("PriceType= " + priceType);
            }
    
            if (priceType.equals(PriceType.LIST_VALUE) || priceType.equals(PriceType.SCALE_VALUE)) {
                priceTypeArray = new PriceType[] { PriceType.LIST_VALUE, PriceType.SCALE_VALUE };
                showPriceInfoArray = new int[] { WebCatItemPrice.SHOW_ALL_PRICES, WebCatItemPrice.SHOW_ALL_PRICES };
                log.debug("List and scale");
            }
            else {
                priceTypeArray = new PriceType[] { priceType };
                showPriceInfoArray = new int[] { WebCatItemPrice.SHOW_ALL_PRICES };
            }
    
            priceInfos = webCatItemPrice.getPriceInfosForPriceTypes(priceTypeArray, showPriceInfoArray, true);
            
            if (priceInfos != null) { 
                
                boolean standardPriceFound = false;
                boolean specialPriceFound = false;
                
                if (log.isDebugEnabled() && priceInfos.length > 2) {
                    log.debug("More than two prices returned from pricing");
                }
                
                //assume first none  promotional price is "normal" price
                for (int iPr = 0; iPr < priceInfos.length && (standardPriceFound == false || specialPriceFound); iPr++) {
                    priceInfo = priceInfos[iPr];
                    
                    if (PriceInfo.SPECIAL_PRICE.equals(priceInfo.getPromotionalPriceType())) {
                        specialPriceFound = true;
                        respItem.currencySpecialPrice = priceInfo.getCurrency();
                        respItem.currencySpecialPriceIso = currConverter.convertCurrency(true, respItem.currencySpecialPrice);
                        respItem.specialPrice = priceInfo.getPrice();
                    }
                    else {
                        standardPriceFound = true;
                        respItem.currencyStdPrice = priceInfo.getCurrency();
                        respItem.currencyStdPriceIso = currConverter.convertCurrency(true, respItem.currencyStdPrice);
                        respItem.stdPrice = priceInfo.getPrice();
                    }
                }  
            }
        }
        
        log.exiting();
    }

    /**
     * Fill the basic response data
     * 
     * @param userData Object wrapping the session
     * @param theCatalog theCatalog Representation of the catalog
     * @param responseBaseData basic response data to be filled
     */
    protected void fillResponseBaseData(UserSessionData userData, WebCatInfo theCatalog, CatalogServiceResponseBaseData responseBaseData) {
        
        log.entering("fillResponseBaseData()");
        
        CatalogConfiguration catConfig = getCatalogConfiguration(userData);
        
        responseBaseData.language = catConfig.getLanguage();
        responseBaseData.languageIso = catConfig.getLanguageIso();
        responseBaseData.decimalSeparator = catConfig.getDecimalSeparator();
        responseBaseData.imageServer = theCatalog.getImageServer();
        responseBaseData.httpsImageServer = theCatalog.getHttpsImageServer();
        
        if (responseBaseData.log == null) {
            responseBaseData.log = new Log();
        }
        
        log.exiting();
    }
    
    /**
     * Fill default item specific response data
     * 
     * @param responseBaseData basic response data to be filled
     * @param returnPrices flag, to indicate if prices should be returned/filled
     * @param theItem the catalog item to determine the reponse data from
     * @param UOMConverter the uom converter to get the ISO uom code from
     * @param currConverter the currency converter to get the ISO currency code from 
     * 
     * @return CatalogServiceItemData the filled item response data
     */
    protected CatalogServiceItemData fillItemRespData(CatalogServiceResponseBaseData responseBaseData, boolean returnPrices, WebCatItem theItem, 
                                                      UOMConverter uomConverter, CurrencyConverter currConverter) 
              throws CommunicationException {
        
        log.entering("fillItemRespData()");
            
        CatalogServiceItemData respItem =  new CatalogServiceItemData();
                
        respItem.areaKey = theItem.getAreaID();
        respItem.description = theItem.getArea();
        respItem.eyeCatcherText = theItem.getEyeCatcherText();
        respItem.itemKey = theItem.getItemId();
        respItem.posNr = theItem.getAttributeByKey(AttributeKeyConstants.LINE_NUMBER);
        respItem.priceEyeCatcherText = theItem.getPriceEyeCatcherText();
        respItem.productGuid = theItem.getProductID();
        respItem.productId = theItem.getProduct();
        respItem.productDescription = theItem.getDescription();
        respItem.uom = theItem.getUnit();
        respItem.uomIso = uomConverter.convertUOM(true, respItem.uom, responseBaseData.languageIso);
        respItem.quantity = theItem.getQuantity();
        respItem.thumbNail = theItem.getRelThumbNailPath();
                
        fillItemPriceResp(returnPrices, respItem, currConverter, theItem.getItemPrice());
        
        log.exiting();
        
        return respItem;
    }

    /**
     * Process campaignId if present in input data 
     * 
     * @param userData Object wrapping the session
     * @param theCatalog theCatalog Representation of the catalog
     * @param catConfig the catalog config object, holding configuration data
     * @param log the location to write messages to
     * @param responseBaseData response data to write messages to
     */
    protected void handleCampaignInput(UserSessionData userData, WebCatInfo theCatalog, CatalogConfiguration catConfig, 
                                       String campaignId, IsaLocation log, CatalogServiceResponseBaseData responseBaseData) 
                   throws CommunicationException {
        
        log.entering("handleCampaignInput()");
        
        if (log.isDebugEnabled()) {
            log.debug("Processing Campaign = " + campaignId);
        }
            
        if (campaignId != null && campaignId.trim().length() > 0) {
            
            if (campaignId.equalsIgnoreCase(theCatalog.getCampaignId())) {
                log.debug("Same campaign as in catalog, doing nothing");
                return;
            }
            
            CampaignBusinessObjectManager campaignBom = (CampaignBusinessObjectManager) userData.getBOM(CampaignBusinessObjectManager.CAMPAIGN_BOM);
            Campaign campaign = campaignBom.getCampaign();
            
            CampaignHeaderData campaignHeader = null;
        
            if (campaign == null || campaign.getHeaderData() == null || !campaignId.equalsIgnoreCase(campaignHeader.getCampaignID())) {
                
                if (campaign != null) {
                    campaignBom.releaseCampaign();
                    campaign = null;
                }
                else {
                    log.debug("Campaign object null - create new one");
                }

                campaign = campaignBom.createCampaign();
                campaignHeader = (CampaignHeaderData) campaign.createHeader();

                campaignHeader.setCampaignID(campaignId);
                campaign.setHeader(campaignHeader);

                campaign.readCampaignHeader();
            }
        
            campaignHeader = campaign.getHeaderData();
        
            if (campaignHeader == null) {
                log.debug("campaignHeader object null - return");
                addMessageToResponseLog(responseBaseData, "cat.webservice.noCampaign", LogItem.SEVERITY_ERROR);
                return;
            }
            
            String campaignCode = campaignHeader.getCampaignID(); 

            boolean campainWasValid = theCatalog.getCampaignGuid() != null && theCatalog.isSoldToEligible();
        
            log.debug("Old Campaign validity :" + campainWasValid + "  Guid != null: " + 
                       (theCatalog.getCampaignGuid() != null) + " SoldTo Eligible:" + theCatalog.isSoldToEligible());
        
            // setting useful defaults
            theCatalog.setCampaignId(campaignCode);
            theCatalog.setSoldToEligible(false);
            theCatalog.setCampaignGuid(null);   
        
            log.debug("CampaignId: " + campaignCode);
            log.debug("campaignHeader.isValid(): " + campaignHeader.isValid());
        
            if (campaignCode != null && !campaignCode.equals("") && campaignHeader.isValid()) {
        
                BusinessPartnerManager bupaManager = getCatalogObjectAwareBom(userData).createBUPAManager();
                BusinessPartner soldTo = bupaManager.getDefaultBusinessPartner(PartnerFunctionData.SOLDTO);
        
                TechKey soldToGuid = soldTo!= null ? soldTo.getTechKey() : null;
        
                log.debug("campaignHeader.isPublic(): " + campaignHeader.isPublic());
        
                if (!campaignHeader.isPublic()){
                    log.debug("Private Campaign");
                    if (null != soldToGuid && !soldToGuid.isInitial()) { //Sold-to available
                        log.debug("Campaign check eligibilty");
                        campaign.checkPrivateCampaignEligibility(soldToGuid, catConfig.getSalesOrganisation(), 
                                                                 catConfig.getDistributionChannel(), catConfig.getDivision(), 
                                                                 new Date());
                        if (campaignHeader.isValid()) { // Sold-to is eligible
                            log.debug("Campaign is eligible set values");
                            theCatalog.setCampaignGuid(campaignHeader.getTechKey());
                            theCatalog.setSoldToEligible(true);
                        }
                        else {
                            log.debug("Soldto is not eligible for the campaign.");
                            addMessageToResponseLog(responseBaseData, "cat.webservice.campaignNotEligible", LogItem.SEVERITY_ERROR);
                        }
                    }
                    else {
                        log.debug("Campaign is private but no soldTo available");
                        addMessageToResponseLog(responseBaseData, "cat.webservice.privateCampaign", LogItem.SEVERITY_ERROR);
                    }
                }
                else { 
                    log.debug("Public Campaign set values");
                    theCatalog.setCampaignGuid(campaignHeader.getTechKey());
                    //Public Campaign, Sold-to not mandatory
                    theCatalog.setSoldToEligible(true);
                }
            }
            else {
                log.debug("Camapign not found");
                addMessageToResponseLog(responseBaseData, "cat.webservice.unknownCampaign", LogItem.SEVERITY_ERROR);
            }
        }
        else if (theCatalog.getCampaignId() != null) {
            log.debug("Removing campaign data in catalog");
            theCatalog.setCampaignGuid(null);
            theCatalog.setCampaignId(null);
            theCatalog.setSoldToEligible(false);
        }
        
        log.exiting();
    }

    /**
     * If debug is enabled, determine the start time of the action
     * 
     * @param log the location to write messages to
     */
    protected Calendar determineStartTime(IsaLocation log) {
        
        Calendar startTime = null;
        
        if (log.isInfoEnabled()) {
            startTime = new GregorianCalendar();
        }
        
        return startTime;
    }

    /**
     * If debug is enabled, log the runtime of the catalog related functions of the action
     * 
     * @param startTime the start time to compare the current time to
     * @param log the location to write messages to
     */
    protected void logTimeUsedByCatalog(Calendar startTime, IsaLocation log) {
        
        Calendar midTime;
        
        if (log.isInfoEnabled()) {
            midTime = new GregorianCalendar();
            log.info("Catalog specific Runtime: " + (midTime.getTimeInMillis() - startTime.getTimeInMillis()));
        }
    }
    
    /**
     * If debug is enabled, log the complete runtime of the action
     * 
     * @param startTime the start time to compare the current time to
     * @param log the location to write messages to
     */
    protected void logEndTime(Calendar startTime, IsaLocation log) {
        
        Calendar endTime;
        
        if (log.isInfoEnabled()) {
            endTime = new GregorianCalendar();
            log.info("Complete Service Runtime: " + (endTime.getTimeInMillis() - startTime.getTimeInMillis()));
        }
    }
    
    /**
     * Fill the contract refrence request for web servic items
     * 
     * @param contractRequest the contract reference request to fill
     * @param theItem the catalog item to fill the contract reference request for
     * @param theCatalog theCatalog Representation of the catalog
     * @param userData Object wrapping the session
     */
    protected void fillContractReferenceRequest(ContractReferenceRequest contractRequest,
                                                WebCatItem theItem,
                                                WebCatInfo theCatalog,
                                                UserSessionData userData) {
                                                    
      log.entering("fillContractReferenceRequest()");
                                                                                                  
      super.fillContractReferenceRequest(contractRequest, theItem, theCatalog, userData);
    
      fillContractReferenceRequestForSoldToAndSalesOrg(contractRequest, userData);
      
      log.exiting();
    }
    
    /**
     * Determine the items related contract info in the response, but only if we have a user
     * 
     * @param userData Object wrapping the session
     * @param theCatalog theCatalog Representation of the catalog
     * @param requestBaseData the requestBaseData object
     * @param bom the catalog business object manager
     * @param itemList the items to determine the contract data for
     * @param contractView the contract view
     * @param log the location to write messages to
     */
    protected void determineItemsContractData(UserSessionData userData, WebCatInfo theCatalog,
                                              CatalogServiceRequestBase requestBaseData, CatalogBusinessObjectManager bom,
                                              ArrayList itemList, ContractView contractView, IsaLocation log) {
        
        log.entering("determineItemsContractData()");                                            
        
        if (requestBaseData.user != null && requestBaseData.user.trim().length() > 0) {
            // prepare requests data for contract references
            ArrayList items = new ArrayList();
            
            ContractReferenceMap contractRefMap = determineContractInfo(userData, theCatalog, bom, itemList.iterator(), items, contractView, log);
            
            // if no contract info found at all: exit
            if (contractRefMap != null && contractRefMap.size() > 0) {
                setContractInfo(items, contractRefMap, contractView, log);
                    
                // fill contract infos
            }
            else {
                log.debug("No contract data found");
            }
        }
        else {
            log.debug("user is not set, no contract infos read");
        } 
        
        log.exiting();
    }

    /**
     * Method fill the items contract information into the response object
     * 
     * @param catConfig the catalog config object, holding configuration data
     * @param returnPrices flag, to indicate if prices should be returned/filled
     * @param theItem the catalog item to determine the reponse data from
     * @param respItem the response item object the price data should be filled into
     * @param currConverter the currency converter to get the ISO currency code from
     * @param log the location to write messages to 
     */
    protected void fillItemContractResp(CatalogConfiguration catConfig, boolean returnPrices, 
                                        WebCatItem theItem, CatalogServiceItemData respItem, 
                                        CurrencyConverter currConverter, IsaLocation log) 
                   throws CommunicationException {
                       
        log.entering("fillItemContractResp");
                       
        if (theItem.getContractItems() != null && !theItem.getContractItems().isEmpty()) {
            
            Collection contractItems = theItem.getContractItems();
            CatalogServiceContractBasisInfo[] contractInfos = new CatalogServiceContractBasisInfo[contractItems.size()];
            CatalogServiceContractBasisInfo contractInfo = null;
            
            CatalogServiceItemData respHelpItem = new CatalogServiceItemData();
            
            Iterator contrIter = contractItems.iterator();
            WebCatItem contrSubItem = null;
            int ctrIdx = 0;
            
            while (contrIter.hasNext()) {
                contrSubItem = (WebCatItem) contrIter.next();
                contractInfo = new CatalogServiceContractBasisInfo();
                
                contractInfo.contractItemKey = contrSubItem.getContractRef().getItemKey().getIdAsString();
                contractInfo.contractKey = contrSubItem.getContractRef().getContractKey().getIdAsString();
                contractInfo.contractItemId = contrSubItem.getContractRef().getItemID();
                contractInfo.contractId = contrSubItem.getContractRef().getContractId();
                
                if (returnPrices) {
                    fillItemPriceResp(returnPrices, respHelpItem, currConverter, contrSubItem.getItemPrice());
                    
                    contractInfo.contractCurrency = respHelpItem.currencyStdPrice;
                    contractInfo.contractCurrencyIso = respHelpItem.currencyStdPriceIso;
                    contractInfo.contractPrice = respHelpItem.stdPrice;
                    if (respHelpItem.stdPrice != null) {
                        contractInfo.contractPriceSimpleFormat = respHelpItem.stdPrice.replaceAll(catConfig.getGroupingSeparator(), "");
                    }
                }
        
                contractInfos[ctrIdx] = contractInfo;
                ctrIdx++;
            }
            
            respItem.contracts = contractInfos;
        } 
        
        log.exiting();
    }

    /**
     * Write execptions to the response log
     * 
     * @param responseBaseData the response to add the exception to
     * @param ex the Exception
     * @param log the ISaloCation to write messages to
     */
    protected void logExceptionToResponseLog(CatalogServiceResponseBaseData responseBaseData, Exception ex, IsaLocation log) {
        
        log.error("Exception occured " + ex.getMessage());
        log.error(ex.getStackTrace());
        
        if (responseBaseData.log == null) {
            responseBaseData.log = new Log();
        }
        
        responseBaseData.log.addLogItem(LogItem.SEVERITY_ERROR, "Exception occured " + ex.getMessage());
    }

    /**
     * Log the given message to the response log
     * 
     * @param responseBaseData the response to add the exception to
     * @param msgs the messagelist, to write to the response log
     * @param log the IsaLocation to write messages to
     */
    protected void logMessagesToResponse(CatalogServiceResponseBaseData responseBaseData, MessageList msgs, IsaLocation log) {
        if (msgs != null && msgs.size() > 0) {
        
            Message msg = null;
            String severityCode = null;
            
            if (responseBaseData.log  == null) {
                responseBaseData.log  = new Log();
            }
            
            for (int i = 0; i < msgs.size(); i++) {
                msg = msgs.get(i);
                
                if (msg.isError()) {
                    severityCode = LogItem.SEVERITY_ERROR;
                }
                else if (msg.isWarning()) {
                    severityCode = LogItem.SEVERITY_WARNING;
                }
                else {
                    severityCode = LogItem.SEVERITY_INFO;
                }
                
                responseBaseData.log.addLogItem(severityCode, msg.getDescription());
            }
        }
    }

    /**
     * Translates the given message key with for the language soecified in the responseBaseData object
     * and adds it to the response log with the given severity
     * 
     * @param responseBaseData basic response data write the message to
     * @param msgKey resoource key of the message
     * @param severity message severity
     */
    protected void addMessageToResponseLog(CatalogServiceResponseBaseData responseBaseData, String msgKey, String severity) {
        
        if (responseBaseData.log == null) {
            responseBaseData.log = new Log();
        }
        
        Locale locale = new Locale(responseBaseData.languageIso);
        String messageText = WebUtil.translate(locale, msgKey,  null);
        responseBaseData.log.addLogItem(severity, messageText);
    }
}
