package com.sap.isa.catalog.actions.webservices;

import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.Calendar;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.sap.isa.backend.boi.isacore.contract.ContractView;
import com.sap.isa.backend.boi.webcatalog.CatalogConfiguration;
import com.sap.isa.businessobject.CommunicationException;
import com.sap.isa.businessobject.CurrencyConverter;
import com.sap.isa.businessobject.UOMConverter;
import com.sap.isa.businessobject.webcatalog.CatalogBusinessObjectManager;
import com.sap.isa.catalog.webcatalog.WebCatArea;
import com.sap.isa.catalog.webcatalog.WebCatInfo;
import com.sap.isa.catalog.webcatalog.WebCatItem;
import com.sap.isa.catalog.webcatalog.WebCatItemList;
import com.sap.isa.core.UserSessionData;
import com.sap.isa.core.businessobject.management.MetaBusinessObjectManager;
import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.core.util.RequestParser;
import com.sap.isa.webservice.catalog.types.CatalogServiceAreaData;
import com.sap.isa.webservice.catalog.types.CatalogServiceAreaRequest;
import com.sap.isa.webservice.catalog.types.CatalogServiceAreaResponse;
import com.sap.isa.webservice.catalog.types.CatalogServiceItemData;
import com.sap.isa.webservice.catalog.types.CatalogServiceRequestBase;
import com.sap.isa.webservice.catalog.types.CatalogServiceResponseBaseData;
import com.sap.isa.webservice.catalog.types.Log;
import com.sap.isa.webservice.catalog.types.LogItem;

/**
 * Title:        GetAreaWebServiceAction
 * Description:  Implements the common functionalitiy or catalog Web Services
 * Copyright:    Copyright (c) 2008
 * Company:      SAP AG, Germany
 * @version 1.0
 * 
 * The GetItemDetailsWebService is responsible to execute the service to determine area specific data 
 * in the web application.
 * 
 * This is done in three steps:
 * 
 * 1. deserialize the input parameter, usually one parameter inputMessage
 * 2. call the service, usually the same method with the same parameters as the real web service is called.
 * 3. serialize the output parameter, usually one parameter outputMessage.
 * 
 * Eventually, a fault message is generated for exceptions. Normally, errors are part of the payload of the
 * message.
 */

public class GetAreaWebServiceAction extends CatalogWebServiceBaseAction {
    
    /**
     * Overwrite this method to add functionality to your action. With this signature you get
     * the access to RequestParser and MetaBusinessObjectManager.
     * 
     * @param mapping mapping The ActionMapping used to select this instance
     * @param form The <code>FormBean</code> specified in the Struts configuration file for this action
     * @param request The request object
     * @param response The response object
     * @param userData Object wrapping the session
     * @param theCatalog theCatalog Representation of the catalog
     * @param requestParser Parser to simple retrieve data from the request
     * @param mbom Meta business object manager
     * 
     * @return Forward to another action or JSP page
     * @throws IOException
     * @throws ServletException
     * @throws CommunicationException
     */
    public ActionForward doPerform(ActionMapping mapping,
            ActionForm form,
            HttpServletRequest request,
            HttpServletResponse response,
            UserSessionData userData,
            WebCatInfo theCatalog,
            RequestParser requestParser,
            MetaBusinessObjectManager mbom)
        throws IOException, ServletException, CommunicationException {
            
        Calendar startTime = determineStartTime(log);
        
        // prepare result message
        CatalogServiceAreaResponse result = new CatalogServiceAreaResponse();
        CatalogConfiguration catConfig = getCatalogConfiguration(userData);

        fillResponseBaseData(userData, theCatalog, result.responseBaseData);

        result.responseBaseData.log = new Log();
        
        // get the input message
        CatalogServiceAreaRequest input = getInputMessage(request, result.responseBaseData.log);

        if (input != null && input.areaKey != null) {
            
            WebCatArea theArea = theCatalog.getArea(input.areaKey);
            
            if (theArea != null && theArea.getCategory() != null) {
                
                WebCatItemList theItems = null;
                
                if (log.isDebugEnabled()) {
                    log.debug("startIdx=" + input.startIdx + "  endIdx=" + input.endIdx);
                }
                
                if (input.startIdx < 0) {
                    log.debug("startIdx < 0, setting it to 0");
                    input.startIdx = 0;
                }
                
                if (input.endIdx < 0) {
                    log.debug("endIdx < 0, setting it to 0");
                    input.endIdx = 0;
                }
                
                if (input.startIdx > input.endIdx) {
                    log.debug("startIdx > endIdx, setting startIdx to endIdx");
                    input.startIdx = input.endIdx;
                }
                
                if (input.startIdx == 0 && input.endIdx == 0) {
                    log.debug("Populating all");
                    
                    theItems = theArea.getItemList(true);
                }
                else {
                    log.debug ("populate up to endIdx");
                    theItems = theArea.getItemList(input.endIdx);
                    
                    for (int i = 0; i < input.startIdx - 1 && theItems.size() > 0; i++) {
                        // we always have to remove the first entry, because the indizes are recalculated
                        // after every delete
                        theItems.removeItem(0);
                    }
                }

                if (theItems != null  && theItems.size() > 0) {
                    
                    CatalogBusinessObjectManager bom = (CatalogBusinessObjectManager) userData.getBOM(CatalogBusinessObjectManager.CATALOG_BOM);

                    handleCampaignInput(userData, theCatalog, catConfig, input.campaignId, log, result.responseBaseData);
                    
                    // Do contract items at first, to get their prices also
                    if (input.returnContracts) {
                        determineItemsContractData(userData, theCatalog, input.requestBaseData, bom, 
                                                   theItems.getItemsOnlyPopulated(), contractView, log);
                        
                        userExitDetermineItemsContractDetailData(userData, theCatalog, input, bom, theItems.getItemsOnlyPopulated(), 
                                                                 contractView, log, result.responseBaseData);
                    }

                    if (input.returnPrices) {
                        ArrayList items = collectPricingItems(true, theItems.iteratorOnlyPopulated());
                        if (!items.isEmpty() && bom.getPriceCalculator() != null) {
                            repriceItems(mbom, items);
                        }
                    }
                }
                else  {
                    log.debug("No items found for the area");
                }
                
                logTimeUsedByCatalog(startTime, log);

                CatalogServiceAreaData respArea = fillAreaResp(theArea, input.returnAreaLocs); 
                
                fillAreaTextsResp(theArea, respArea);
                
                userExitFillAreaTextsResp(theArea, respArea, theCatalog, input, log, result.responseBaseData);
                
                fillAreaPictureResp(theArea, respArea);
                
                userExitFillAreaMimesResp(theArea, respArea, theCatalog, input, log, result.responseBaseData);
                
                if (theItems != null) {
                
                    CatalogServiceItemData[] respItems = new CatalogServiceItemData[theItems.size()];   
                    CatalogServiceItemData respItem = null;
                    WebCatItem theItem = null;
                    
                    UOMConverter uomConverter = getCatalogObjectAwareBom(userData).createUOMConverter();
                    CurrencyConverter currConverter = getCatalogObjectAwareBom(userData).createCurrencyConverter();
                    
                    for (int i=0; i < theItems.size(); i++) {
                        
                        theItem = theItems.getItem(i);
                        
                        respItem = fillItemRespData(result.responseBaseData, input.returnPrices, theItem, uomConverter, currConverter);
                        
                        userExitFillItemTextsResp(theItem, respItem, theCatalog, input, log, result.responseBaseData);
                        
                        userExitFillItemMimesResp(theItem, respItem, theCatalog, input, log, result.responseBaseData);
                        
                        if (input.returnContracts) {
                            fillItemContractResp(catConfig, input.returnPrices, theItem, respItem, currConverter, log);
                            
                            userExitFillItemContractDetailResp(catConfig, input, theItem, respItem, currConverter, log, result.responseBaseData);
                        }

                        respItems[i] = respItem;
                        
                    } //items loop
                    
                    result.items = respItems;
                    
                } // items != null
                else {
                    log.debug("No items found for the area");
                }
                
                result.area = respArea;
                 
            } // area != null
            else {
                addMessageToResponseLog(result.responseBaseData, "cat.webservice.areaNotFound", LogItem.SEVERITY_ERROR);
            }
            
            logMessagesToResponse(result.responseBaseData, theCatalog.getMessageList(), log);
        }
        else {
            addMessageToResponseLog(result.responseBaseData, "cat.webservice.areaNotSpecified", LogItem.SEVERITY_ERROR);
        }
        
        // send result message
        sendResultMessage(response, result);
        
        logEndTime(startTime, log);

        return (mapping.findForward("success"));
    }
    
    /**
     * Send the response
     * 
     * @param response the respons eobject to send
     * @param result the object, CatalogServiceAreaResponse to send in the response
     */
    private void sendResultMessage(HttpServletResponse response, CatalogServiceAreaResponse result) throws IOException {
        
        log.entering("sendResultMessage");
        
        response.setContentType("application/x-java-serialized-object");
        ObjectOutputStream objOut = new ObjectOutputStream(response.getOutputStream());
        objOut.writeObject(result.responseBaseData.log);
        objOut.writeObject(result);
        objOut.close();
        
        log.exiting();
    }

    /**
     * Gets the service request data from the HttpServletRequest object
     * 
     * @return CatalogServiceAreaRequest the itemDetails requets object
     */
    private CatalogServiceAreaRequest getInputMessage(HttpServletRequest request, Log servicelog) throws IOException {

        log.entering("getInputMessage");
        
        InputStream is = request.getInputStream();
        ObjectInputStream objIn = new ObjectInputStream(is);
        CatalogServiceAreaRequest input = null;
        
        try {
            CatalogServiceRequestBase requestBaseData = (CatalogServiceRequestBase)objIn.readObject();
            input = (CatalogServiceAreaRequest) objIn.readObject();
        }
        catch (IOException e) {
            servicelog.addLogItem(LogItem.SEVERITY_ERROR, "IOException during deserializing of the input data.");
        }
        catch (ClassNotFoundException e) {
            servicelog.addLogItem(LogItem.SEVERITY_ERROR, "ClassNotFoundException during deserializing of the input data.");
        }
        
        log.exiting();
        
        return input;
    }

    /**
     * User exit to be overwritten, if additional Area texts should
     * be returned in the response
     * 
     * @param theArea the catalog area to determine the data from
     * @param respArea the area response object to add the text data to
     * @param theCatalog theCatalog Representation of the catalog
     * @param input the CatalogServiceAreaRequest input object
     * @param log the location to write messages to
     * @param responseBaseData the response base data object
     */
    public void userExitFillAreaTextsResp(WebCatArea theArea, 
                                          CatalogServiceAreaData respArea, 
                                          WebCatInfo theCatalog, 
                                          CatalogServiceAreaRequest input,
                                          IsaLocation log,
                                          CatalogServiceResponseBaseData responseBaseData) {
        
    }
    
    /**
     * User exit to be overwritten, if additional Area mimes should
     * be returned in the response
     * 
     * @param theArea the catalog area to determine the data from
     * @param respArea the area response object to add the mimes data to
     * @param theCatalog theCatalog Representation of the catalog
     * @param input the CatalogServiceAreaRequest input object
     * @param log the location to write messages to
     * @param responseBaseData the response base data object
     */
    public void userExitFillAreaMimesResp(WebCatArea theArea, 
                                          CatalogServiceAreaData respArea, 
                                          WebCatInfo theCatalog, 
                                          CatalogServiceAreaRequest input,
                                          IsaLocation log,
                                          CatalogServiceResponseBaseData responseBaseData) {
                
    }
    
    /**
     * User exit to be overwritten, if item mimes should
     * be returned in the response
     * 
     * @param theItem the catalog item to determine the reponse data from
     * @param respItem the response item object to add the mimes data to
     * @param theCatalog theCatalog Representation of the catalog
     * @param input the CatalogServiceAreaRequest input object
     * @param log the location to write messages to
     * @param responseBaseData the response base data object
     */
    public void userExitFillItemMimesResp(WebCatItem theItem, 
                                          CatalogServiceItemData respItem, 
                                          WebCatInfo theCatalog,
                                          CatalogServiceAreaRequest input,
                                          IsaLocation log,
                                          CatalogServiceResponseBaseData responseBaseData) {
        
    }
 
    /**
     * User exit to be overwritten, if item texts should
     * be returned in the response
     * 
     * @param theItem the catalog item to determine the reponse data from
     * @param respItem the response item object to add the text data to
     * @param theCatalog theCatalog Representation of the catalog
     * @param input the CatalogServiceAreaRequest input object
     * @param log the location to write messages to
     * @param responseBaseData the response base data object
     */
    public void userExitFillItemTextsResp(WebCatItem theItem, 
                                          CatalogServiceItemData respItem, 
                                          WebCatInfo theCatalog, 
                                          CatalogServiceAreaRequest input,
                                          IsaLocation log,
                                          CatalogServiceResponseBaseData responseBaseData) {  
        
    }
    
    /**
     * User exit to be overwritten, if item attribute data should
     * be returned in the response
     * 
     * @param userData Object wrapping the session
     * @param theCatalog theCatalog Representation of the catalog
     * @param input the CatalogServiceAreaRequest input object
     * @param bom the catalog business object manager
     * @param itemList the items to determine the contract data for
     * @param contractView the contract view
     * @param log the location to write messages to
     * @param responseBaseData the response base data object
     */       
    public void userExitDetermineItemsContractDetailData(UserSessionData userData, 
                                                         WebCatInfo theCatalog, 
                                                         CatalogServiceAreaRequest input, 
                                                         CatalogBusinessObjectManager bom, 
                                                         ArrayList itemList, 
                                                         ContractView contractView, 
                                                         IsaLocation log,
                                                         CatalogServiceResponseBaseData responseBaseData) {            
    }

    /**
     * User exit to be overwritten, if item attribute data should
     * be returned in the response
     * 
     * @param catConfig the catalog config object, holding configuration data
     * @param input the CatalogServiceAreaRequest input object#
     * @param theItem the catalog item to determine the reponse data from
     * @param respItem the response item object to add the loc data to
     * @param currConverter the currency converter to get the ISO currency code from 
     * @param log the location to write messages to
     * @param responseBaseData the response base data object
     */ 
    public void userExitFillItemContractDetailResp(CatalogConfiguration catConfig, 
                                                   CatalogServiceAreaRequest input, 
                                                   WebCatItem theItem, 
                                                   CatalogServiceItemData respItem, 
                                                   CurrencyConverter currConverter,
                                                   IsaLocation log,
                                                   CatalogServiceResponseBaseData responseBaseData) {
            
    }

}
