/*****************************************************************************
Class         UserLoginWebServiceAction
Copyright (c) 2008, SAP AG, All rights reserved.
Created:      19.05.2008
*****************************************************************************/
package com.sap.isa.catalog.actions.webservices;

import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.Locale;

import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.sap.isa.backend.boi.webcatalog.CatalogBusinessObjectsAware;
import com.sap.isa.businessobject.CommunicationException;
import com.sap.isa.businesspartner.businessobject.BusinessPartner;
import com.sap.isa.businesspartner.businessobject.BusinessPartnerManager;
import com.sap.isa.businesspartner.businessobject.Contact;
import com.sap.isa.businesspartner.businessobject.PartnerFunctionBase;
import com.sap.isa.core.TechKey;
import com.sap.isa.core.UserSessionData;
import com.sap.isa.core.businessobject.management.MetaBusinessObjectManager;
import com.sap.isa.core.interaction.InteractionConfigContainer;
import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.core.util.RequestParser;
import com.sap.isa.core.util.WebUtil;
import com.sap.isa.core.util.table.ResultData;
import com.sap.isa.user.action.UserBaseAction;
import com.sap.isa.user.businessobject.IsaUserBase;
import com.sap.isa.user.util.LoginStatus;
import com.sap.isa.webservice.catalog.types.CatalogServiceRequestBase;
import com.sap.isa.webservice.catalog.types.CatalogServiceResponseBaseData;
import com.sap.isa.webservice.catalog.types.Log;
import com.sap.isa.webservice.catalog.types.LogItem;

/**
 * Title:        UserLoginWebServiceAction
 * Description:  Implements the common functionalitiy or catalog Web Services
 * Copyright:    Copyright (c) 2008
 * Company:      SAP AG, Germany
 * @version 1.0
 * 
 * The UserLoginWebServiceAction is responsible to execute a login for a Web service, if user and password are
 * given in the input data. If in addition a SoldTo Id is given, thw validity of tzhe soldTo will be checked
 * and it successfull, it will be set on the bupama.
 * 
 * This is done in three steps:
 * 
 * 1. deserialize the input parameter, usually one parameter inputMessage
 * 2. call the service, usually the same method with the same parameters as the real web service is called.
 * 3. serialize the output parameter, usually one parameter outputMessage.
 * 
 * Eventually, a fault message is generated for exceptions. Normally, errors are part of the payload of the
 * message.
 */

public class UserLoginWebServiceAction extends UserBaseAction {
    
    /** 
     *  The IsaLocation of the current Action.
     *  This will be instantiated during perform and is always present.
     */
    protected IsaLocation log =  IsaLocation.getInstance(UserLoginWebServiceAction.class.getName());

    /**
     * Implement this method to add functionality to your action.
     *
     * @param mapping DOCUMENT ME!
     * @param form The <code>FormBean</code> specified in the config.xml file for this action
     * @param request The request object
     * @param response The response object
     * @param userSessionData Object wrapping the session
     * @param requestParser Parser to simple retrieve data from the request
     * @param bom Reference to the BusinessObjectManager
     * @param log Reference to the IsaLocation, needed for logging
     * @param startupParameter Object containing the startup parameters
     * @param eventHandler Object to capture events with
     *
     * @return Forward to another action or page
     *
     * @throws CommunicationException DOCUMENT ME!
     */
    public ActionForward ecomPerform(
        ActionMapping mapping,
        ActionForm form,
        HttpServletRequest request,
        HttpServletResponse response,
        UserSessionData userSessionData,
        RequestParser requestParser,
        MetaBusinessObjectManager mbom,
        boolean multipleInvocation,
        boolean browserBack)
        throws IOException, ServletException, CommunicationException {
            
        boolean stopProcessing = false;

        Calendar startTime = determineStartTime(log);

        // prepare result message
        CatalogServiceResponseBaseData responseBaseData = new CatalogServiceResponseBaseData();
        responseBaseData.log = new Log();
        
        CatalogBusinessObjectsAware cbomAware = (CatalogBusinessObjectsAware) userSessionData.getMBOM().getBOMByType(CatalogBusinessObjectsAware.class);
            
        responseBaseData.language = cbomAware.getCatalogConfiguration().getLanguage();
        responseBaseData.languageIso = cbomAware.getCatalogConfiguration().getLanguageIso();

        // get the input message
        CatalogServiceRequestBase input = getInputMessage(request, responseBaseData.log);
        
        boolean validUser = input.user != null && input.user.trim().length() > 0;
        boolean validPassword = input.password != null && input.password.trim().length() > 0;
        
        InteractionConfigContainer interactionConfigData = getInteractionConfig(request);

        String shopScenario = interactionConfigData.getConfig("shop").getValue("shopscenario");
        
        if (log.isDebugEnabled()) {
            log.debug("shopScenario = " + shopScenario);
        }
        
        boolean loginRequired = "B2B".equalsIgnoreCase(shopScenario);
        
        if (!validUser && !validPassword) {
            log.debug("Check SSO Cookie");
            Cookie mySAPSSO2Cookie = determineSSOCookie(request);
        
            if (mySAPSSO2Cookie != null) {
                log.debug("SSO Cookie found");
                input.user = SPECIAL_NAME_AS_USERID;
                input.password  = mySAPSSO2Cookie.getValue();
                
                validUser = true;
                validPassword = true;
            }  
        }
        
        if (validUser && validPassword) {
             
            log.debug("Try to login");
            
            IsaUserBase user = (IsaUserBase) getUser(mbom);
            LoginStatus loginStatus = null;
            
            if (input.user.equalsIgnoreCase(user.getUserId()) && user.isUserLogged()) {
                log.debug("user is already logged in");
                loginStatus = LoginStatus.OK;
            }
            else {
                user.setUserId(input.user);
                user.setPassword(input.password);
                user.setLanguage(responseBaseData.languageIso);
        
                // check if language value set in user object
                if(user.getLanguage() == null) {
                    Locale locale = userSessionData.getLocale();
                    if(locale != null) {
                        user.setLanguage(locale.getLanguage());
                    }
                }
            
                log.debug("Login User: " + user.getUserId());
                log.debug("Login Language: " + user.getLanguage());

                // call the login method
                loginStatus = user.login();
            }
        
            log.debug("loginStatus: " + loginStatus);
            
            if (LoginStatus.OK == loginStatus) {
                
                log.debug("login successful");
                                
                BusinessPartnerManager buPaMa = cbomAware.createBUPAManager();
                
                BusinessPartner partner = buPaMa.createBusinessPartner(user.getBusinessPartner(), null, null);

                Contact contact = new Contact();
                partner.addPartnerFunction(contact);
                buPaMa.setDefaultBusinessPartner(partner.getTechKey(),contact.getName());

                ResultData result = buPaMa.getSoldTosBySalesArea(partner, input.shopId);
                
                String soldToId =  null;
                String soldToGuid =  null;
                
                if (input.soldToId != null && input.soldToId.trim().length() > 0) {
                    
                    soldToId = input.soldToId;
                    
                    if (log.isDebugEnabled()) {
                        log.debug("Process SoldTo : " + input.soldToId);
                    }
                    
                    for (int i = 1; i <= result.getNumRows(); i++) {
                        result.absolute(i);
                        if (soldToId.equalsIgnoreCase(result.getString("SOLDTO"))) {
                            soldToGuid = result.getString("SOLDTO_TECHKEY");
                            break;
                        }
                    }
                    
                    if (soldToGuid == null) {
                        addMessageToResponseLog(responseBaseData, "cat.webservice.InvalidSoldTo", LogItem.SEVERITY_ABORTION);
                        stopProcessing = true;
                    } 
                }
                else if (result.getNumRows() == 1) {
                    result.absolute(1);

                    if (log.isDebugEnabled()) {
                        log.debug("found exactly one sold to, using it - SOLDTO: " + result.getString("SOLDTO") +
                                  " - SOLDTO-TECHKEY: " + result.getString("SOLDTO_TECHKEY"));
                    }

                    soldToId =  result.getString("SOLDTO");
                    soldToGuid =   result.getString("SOLDTO_TECHKEY");
                }
                else if (result.getNumRows() > 1)  {
                    addMessageToResponseLog(responseBaseData, "cat.webservice.SeveralSoldTos", LogItem.SEVERITY_ABORTION);
                    stopProcessing = true;
                }
                
                if (!stopProcessing && soldToId != null && soldToGuid != null) {
                    log.debug("setting SoldTo");
                    
                    partner = buPaMa.createBusinessPartner(new TechKey(soldToGuid), soldToId, null);

                    PartnerFunctionBase partnerFunction = new com.sap.isa.businesspartner.businessobject.SoldTo();

                    partner.addPartnerFunction(partnerFunction);
                    buPaMa.setDefaultBusinessPartner(partner.getTechKey(), partnerFunction.getName());
                }
            }
            else {
                log.error("Login failed with status = " + loginStatus);
                addMessageToResponseLog(responseBaseData, "cat.webservice.LoginFailed", LogItem.SEVERITY_ABORTION);
                stopProcessing = true;
            }
        }
        else if ((validUser || validPassword) && !loginRequired) {
            log.error("Invalid User and/or password given, no login required");
            addMessageToResponseLog(responseBaseData, "cat.webservice.inavlidUserPwd", LogItem.SEVERITY_ABORTION);
            stopProcessing = true;
        }
        else if (loginRequired && (!validUser || !validPassword)) {
            log.error("No User and password given, but login required");
            addMessageToResponseLog(responseBaseData, "cat.webservice.LoginRequired", LogItem.SEVERITY_ABORTION);
            stopProcessing = true;
        }
        else if (loginRequired && validUser) {
            log.error("No valid password given, but login required");
            addMessageToResponseLog(responseBaseData, "cat.webservice.invalidPasswd", LogItem.SEVERITY_ABORTION);
            stopProcessing = true;
        }
        else if (loginRequired && validPassword) {
            log.error("No valid user given, but login required");
            addMessageToResponseLog(responseBaseData, "cat.webservice.invalidUser", LogItem.SEVERITY_ABORTION);
            stopProcessing = true;
        }
        else {
            log.debug("No valid user or password, no login");
        }
        
        if (stopProcessing) {
            // send result message
            sendResultMessage(response, responseBaseData);

            logEndTime(startTime, log);

            return (mapping.findForward("failure"));
        }
        else {    
            // this is necessary for the EComCrossEntryAction, to recognize that no login etc. is necessary anymore    
            userSessionData.setInitialized(true); 
            
            return (mapping.findForward("success"));
        }
    }
    
    /**
     * If debug is enabled, determine the start time of the action
     * 
     * @param log the location to write messages to
     */
    protected Calendar determineStartTime(IsaLocation log) {
        
        Calendar startTime = null;
        
        if (log.isInfoEnabled()) {
            startTime = new GregorianCalendar();
        }
        
        return startTime;
    }
    
    /**
     * If debug is enabled, log the complete runtime of the action
     * 
     * @param startTime the start time to compare the current time to
     * @param log the location to write messages to
     */
    protected void logEndTime(Calendar startTime, IsaLocation log) {
        
        Calendar endTime;
        
        if (log.isInfoEnabled()) {
            endTime = new GregorianCalendar();
            log.info("Service Runtimew :" + (endTime.getTimeInMillis() - startTime.getTimeInMillis()));
        }
    }
    
    /**
     * Send the response
     * 
     * @param response the respons eobject to send
     * @param result the object, CatalogServiceResponseBaseData to send in the response
     */
    private void sendResultMessage(HttpServletResponse response, CatalogServiceResponseBaseData responseBaseData) throws IOException {
        
        log.entering("sendResultMessage");
        
        response.setContentType("application/x-java-serialized-object");
        ObjectOutputStream objOut = new ObjectOutputStream(response.getOutputStream());
        objOut.writeObject(responseBaseData.log);
        objOut.close();
        
        log.exiting();
    }

    /**
     * Gets the basicx service request data from the HttpServletRequest object
     * 
     * @return CatalogServiceRequestBase the basic request object
     */
    private CatalogServiceRequestBase getInputMessage(HttpServletRequest request, Log servicelog) throws IOException {

        log.entering("getInputMessage");
        
        InputStream is = request.getInputStream();
        is.mark(4096);
        ObjectInputStream objIn = new ObjectInputStream(is);
        CatalogServiceRequestBase input = null;

        try {
            input = (CatalogServiceRequestBase) objIn.readObject();
        }
        catch (IOException e) {
            servicelog.addLogItem(LogItem.SEVERITY_ERROR, "IOException during deserializing of the input data.");
        }
        catch (ClassNotFoundException e) {
            servicelog.addLogItem(LogItem.SEVERITY_ERROR, "ClassNotFoundException during deserializing of the input data.");
        }
        
        is.reset();
        log.exiting();
        
        return input;
    }
    
    /**
     * Translates the given message key with for the language soecified in the responseBaseData object
     * and adds it to the response log with the given severity
     * 
     * @param responseBaseData basic response data write the message to
     * @param msgKey resoource key of the message
     * @param severity message severity
     */
    protected void addMessageToResponseLog(CatalogServiceResponseBaseData responseBaseData, String msgKey, String severity) {
        
        if (responseBaseData.log == null) {
            responseBaseData.log = new Log();
        }
        
        Locale locale = new Locale(responseBaseData.languageIso);
        String messageText = WebUtil.translate(locale, msgKey,  null);
        responseBaseData.log.addLogItem(severity, messageText);
    }

}
