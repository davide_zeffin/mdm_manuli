/*****************************************************************************
Class         GetAreasWebService
Copyright (c) 2008, SAP AG, All rights reserved.
Created:      28.4.2008
*****************************************************************************/
package com.sap.isa.catalog.actions.webservices;

import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.Calendar;
import java.util.Iterator;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.sap.isa.businessobject.CommunicationException;
import com.sap.isa.catalog.boi.CatalogException;
import com.sap.isa.catalog.boi.IAttributeValue;
import com.sap.isa.catalog.webcatalog.WebCatArea;
import com.sap.isa.catalog.webcatalog.WebCatAreaList;
import com.sap.isa.catalog.webcatalog.WebCatInfo;
import com.sap.isa.core.UserSessionData;
import com.sap.isa.webservice.catalog.types.CatalogServiceAreaData;
import com.sap.isa.webservice.catalog.types.CatalogServiceHierarchyRequest;
import com.sap.isa.webservice.catalog.types.CatalogServiceHierarchyResponse;
import com.sap.isa.webservice.catalog.types.CatalogServiceRequestBase;
import com.sap.isa.webservice.catalog.types.Log;
import com.sap.isa.webservice.catalog.types.LogItem;

/**
 * Title:        GetAreaHierarchyWebServiceAction
 * Description:  Implements the common functionalitiy or catalog Web Services
 * Copyright:    Copyright (c) 2008
 * Company:      SAP AG, Germany
 * @version 1.0
 * 
 * The GetItemDetailsWebService is responsible to execute the service to determine area hierarchy data 
 * in the web application.
 * 
 * This is done in three steps:
 * 
 * 1. deserialize the input parameter, usually one parameter inputMessage
 * 2. call the service, usually the same method with the same parameters as the real web service is called.
 * 3. serialize the output parameter, usually one parameter outputMessage.
 * 
 * Eventually, a fault message is generated for exceptions. Normally, errors are part of the payload of the
 * message.
 */

public class GetAreaHierarchyWebServiceAction extends CatalogWebServiceBaseAction {

    public ActionForward doPerform(
        ActionMapping mapping,
        ActionForm form,
        HttpServletRequest request,
        HttpServletResponse response,
        UserSessionData userSessionData,
        WebCatInfo theCatalog)
        throws IOException, ServletException, CommunicationException {
            
        boolean continueProcessing = true;

        Calendar startTime = determineStartTime(log);

        // prepare result message
        CatalogServiceHierarchyResponse result = new CatalogServiceHierarchyResponse();
        result.responseBaseData.log = new Log();

        // get the input message
        CatalogServiceHierarchyRequest input = getInputMessage(request, result.responseBaseData.log);

        WebCatAreaList areas = null;

        // read the areas
        try {
            areas = theCatalog.getAllCategories();
        }
        catch (CatalogException ex) {
            logExceptionToResponseLog(result.responseBaseData, ex, log);
            continueProcessing = false;
        }

        logTimeUsedByCatalog(startTime, log);
        
        if (continueProcessing) {
            CatalogServiceAreaData[] respAreas = null;
            CatalogServiceAreaData respArea = null;

            if (areas != null) {
                respAreas = new CatalogServiceAreaData[areas.size()];

                Iterator areasIter = areas.iterator();
                WebCatArea area = null;

                Iterator fixedValIter = null;
                IAttributeValue attribVal = null;

                int i = 0;

                while (areasIter.hasNext()) {
                    area = (WebCatArea) areasIter.next();
                    respArea = fillAreaResp(area, input.returnAreaLocs);
                    respAreas[i] = respArea;
                    
                    if (log.isDebugEnabled()) {
                        log.debug("Adding area " + area.getAreaID() + " - " + area.getAreaName());
                    }

                    i++;
                }
            }
            else {
                log.debug("Area list is empty");  
            }

            // prepare result message
            result.areas = respAreas;
            fillResponseBaseData(userSessionData, theCatalog, result.responseBaseData);
        
            logMessagesToResponse(result.responseBaseData, theCatalog.getMessageList(), log);
        }

        // send result message
        sendResultMessage(response, result);

        logEndTime(startTime, log);

        return (mapping.findForward("success"));
    }
    
    /**
     * Send the response
     * 
     * @param response the respons eobject to send
     * @param result the object, CatalogServiceHierarchyResponse to send in the response
     */
    private void sendResultMessage(HttpServletResponse response, CatalogServiceHierarchyResponse result) throws IOException {
        
        log.entering("sendResultMessage");
        
        response.setContentType("application/x-java-serialized-object");
        ObjectOutputStream objOut = new ObjectOutputStream(response.getOutputStream());
        objOut.writeObject(result.responseBaseData.log);
        objOut.writeObject(result);
        objOut.close();
        
        log.exiting();
    }

    /**
     * Gets the service request data from the HttpServletRequest object
     * 
     * @return CatalogServiceHierarchyRequest the itemDetails requets object
     */
    private CatalogServiceHierarchyRequest getInputMessage(HttpServletRequest request, Log servicelog) throws IOException {

        log.entering("getInputMessage");
        
        InputStream is = request.getInputStream();
        ObjectInputStream objIn = new ObjectInputStream(is);
        CatalogServiceHierarchyRequest input = null;

        try {
            CatalogServiceRequestBase requestBaseData = (CatalogServiceRequestBase)objIn.readObject();
            input = (CatalogServiceHierarchyRequest) objIn.readObject();
        }
        catch (IOException e) {
            servicelog.addLogItem(LogItem.SEVERITY_ERROR, "IOException during deserializing of the input data.");
        }
        catch (ClassNotFoundException e) {
            servicelog.addLogItem(LogItem.SEVERITY_ERROR, "ClassNotFoundException during deserializing of the input data.");
        }

        log.exiting();
        
        return input;
    }
}
