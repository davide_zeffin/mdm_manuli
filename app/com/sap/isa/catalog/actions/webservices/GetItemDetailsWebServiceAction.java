package com.sap.isa.catalog.actions.webservices;

import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Iterator;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.sap.isa.backend.boi.isacore.contract.ContractView;
import com.sap.isa.backend.boi.isacore.marketing.CUADisplayTypes;
import com.sap.isa.backend.boi.isacore.marketing.CUAProductData;
import com.sap.isa.backend.boi.isacore.marketing.MarketingBusinessObjectsAware;
import com.sap.isa.backend.boi.isacore.marketing.MarketingConfiguration;
import com.sap.isa.backend.boi.isacore.marketing.MarketingUser;
import com.sap.isa.backend.boi.webcatalog.CatalogConfiguration;
import com.sap.isa.backend.boi.webcatalog.atp.IAtpResult;
import com.sap.isa.businessobject.CommunicationException;
import com.sap.isa.businessobject.CurrencyConverter;
import com.sap.isa.businessobject.UOMConverter;
import com.sap.isa.businessobject.marketing.CUAList;
import com.sap.isa.businessobject.marketing.CUAManager;
import com.sap.isa.businessobject.marketing.CUAProduct;
import com.sap.isa.businessobject.webcatalog.CatalogBusinessObjectManager;
import com.sap.isa.catalog.boi.IAttributeValue;
import com.sap.isa.catalog.webcatalog.WebCatArea;
import com.sap.isa.catalog.webcatalog.WebCatInfo;
import com.sap.isa.catalog.webcatalog.WebCatItem;
import com.sap.isa.catalog.webcatalog.WebCatItemList;
import com.sap.isa.core.UserSessionData;
import com.sap.isa.core.businessobject.management.MetaBusinessObjectManager;
import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.core.util.RequestParser;
import com.sap.isa.webservice.catalog.types.CatalogServiceAreaData;
import com.sap.isa.webservice.catalog.types.CatalogServiceItemAddUOMPrices;
import com.sap.isa.webservice.catalog.types.CatalogServiceItemCUAData;
import com.sap.isa.webservice.catalog.types.CatalogServiceItemData;
import com.sap.isa.webservice.catalog.types.CatalogServiceItemDetailData;
import com.sap.isa.webservice.catalog.types.CatalogServiceItemDetailRequest;
import com.sap.isa.webservice.catalog.types.CatalogServiceItemDetailResponse;
import com.sap.isa.webservice.catalog.types.CatalogServiceLOCData;
import com.sap.isa.webservice.catalog.types.CatalogServiceRequestBase;
import com.sap.isa.webservice.catalog.types.CatalogServiceResponseBaseData;
import com.sap.isa.webservice.catalog.types.LogItem;


/**
 * Title:        GetItemDetailsWebServiceAction
 * Description:  Implements the common functionalitiy or catalog Web Services
 * Copyright:    Copyright (c) 2008
 * Company:      SAP AG, Germany
 * @version 1.0
 * 
 * The GetItemDetailsWebService is responsible to execute the service to determine detail data for an item 
 * in the web application.
 * 
 * This is done in three steps:
 * 
 * 1. deserialize the input parameter, usually one parameter inputMessage
 * 2. call the service, usually the same method with the same parameters as the real web service is called.
 * 3. serialize the output parameter, usually one parameter outputMessage.
 * 
 * Eventually, a fault message is generated for exceptions. Normally, errors are part of the payload of the
 * message.
 */
public class GetItemDetailsWebServiceAction extends CatalogWebServiceBaseAction {
        
        protected static final String ISO_8601_DATE_FORMAT = "yyyy-MM-dd'T'HH:mm:ssZ";
        
        public static final SimpleDateFormat DF_ISO = new SimpleDateFormat(ISO_8601_DATE_FORMAT); 
        
        protected static final String[] CUA_TYPES = new String[] { CUADisplayTypes.CROSSSELLING, 
                                                                   CUADisplayTypes.UPSELLING, 
                                                                   CUADisplayTypes.DOWNSELLING, 
                                                                   CUADisplayTypes.ACCESSORY };

        /**
         * Overwrite this method to add functionality to your action. With this signature you get
         * the access to RequestParser and MetaBusinessObjectManager.
         * 
         * @param mapping mapping The ActionMapping used to select this instance
         * @param form The <code>FormBean</code> specified in the Struts configuration file for this action
         * @param request The request object
         * @param response The response object
         * @param userData Object wrapping the session
         * @param theCatalog theCatalog Representation of the catalog
         * @param requestParser Parser to simple retrieve data from the request
         * @param mbom Meta business object manager
         * 
         * @return Forward to another action or JSP page
         * @throws IOException
         * @throws ServletException
         * @throws CommunicationException
         */
        public ActionForward doPerform(ActionMapping mapping,
                ActionForm form,
                HttpServletRequest request,
                HttpServletResponse response,
                UserSessionData userData,
                WebCatInfo theCatalog,
                RequestParser requestParser,
                MetaBusinessObjectManager mbom)
            throws IOException, ServletException, CommunicationException {
            
            Calendar startTime = determineStartTime(log);

            // prepare result message
            CatalogServiceItemDetailResponse result = new CatalogServiceItemDetailResponse();
        
            CatalogConfiguration catConfig = getCatalogConfiguration(userData);
            fillResponseBaseData(userData, theCatalog, result.responseBaseData);

            // get the input message
            CatalogServiceItemDetailRequest input = getInputMessage(request);
        
            WebCatItem theItem = null;
        
            if (input.areaKey != null && input.itemKey != null) {
                theItem = theCatalog.getItem(input.areaKey, input.itemKey);
            }
            else  {
                log.debug("ItemKey or area key is missing");
                addMessageToResponseLog(result.responseBaseData, "cat.webservice.itemKeysMissing", LogItem.SEVERITY_ERROR);
            }
        
            if (theItem != null && theItem.getItemID() != null) {
                
                int uomPricingIdxStart = 0;
                int uomPricingIdxEnd = 0;
                ArrayList priceItems = null;
                WebCatItem additionalItem = null;
                CatalogBusinessObjectManager bom = (CatalogBusinessObjectManager) userData.getBOM(CatalogBusinessObjectManager.CATALOG_BOM);
                
                handleCampaignInput(userData, theCatalog, catConfig, input.campaignId, log, result.responseBaseData);
           
                // Do contract items at first, to get their prices also
                if (input.returnContracts) {
                    ArrayList itemList = new ArrayList(1);
                    itemList.add(theItem);
                    
                    determineItemsContractData(userData, theCatalog, input.requestBaseData, bom, itemList, contractView, log);
                    
                    userExitDetermineItemsContractDetailData(userData, theCatalog, input, bom, theItem, contractView, log, result.responseBaseData);
                }
                
                if (input.returnCUA) {
                    determineItemCUAData(userData, theCatalog, theItem);
                }

                if (input.returnPrices) {
                    WebCatItemList theItems = new WebCatItemList(theCatalog);
                    theItems.addItem(theItem);
                      
                    priceItems = collectPricingItems(true, theItems.iterator());

                    if (!priceItems.isEmpty() && bom.getPriceCalculator() != null) {
                      
                        if (theItem.getUnitsOfMeasurement() != null && 
                            theItem.getUnitsOfMeasurement().length > 1) {
                            log.debug("Item has more than one UOM. Add additional Items for UOM Pricing");
                          
                            String currUOM = null;
                          
                            uomPricingIdxStart = priceItems.size();
                            uomPricingIdxEnd = priceItems.size() - 1;
                          
                            for (int i = 0; i < theItem.getUnitsOfMeasurement().length; i++) {
                                currUOM = theItem.getUnitsOfMeasurement()[i];
                                if (!theItem.getUnit().equals(currUOM)) {
                                    uomPricingIdxEnd++;
                                    additionalItem = new WebCatItem(theCatalog, theItem.getCatalogItem());
                                    additionalItem.setUnit(currUOM);
            
                                    priceItems.add(additionalItem);
                                }  
                            }
                        }
                        
                        if (input.returnCUA && theCatalog.getCuaList() != null) {
                            
                            Iterator cuaIter = theCatalog.getCuaList().iterator();
                            
                            CUAProduct cuaProd = null;
                            
                            while (cuaIter.hasNext()) {
                                
                                cuaProd = (CUAProduct) cuaIter.next();
                                
                                priceItems.add(cuaProd.getCatalogItem());
                            }
                        }
                      
                        repriceItems(mbom, priceItems);
                    }
                }
                
                if (input.returnATP) {
                    determineItemATPData(userData, mbom, input, theItem);
                }

                logTimeUsedByCatalog(startTime, log);
        
                //process items    
                    
                UOMConverter uomConverter = getCatalogObjectAwareBom(userData).createUOMConverter();
                CurrencyConverter currConverter = getCatalogObjectAwareBom(userData).createCurrencyConverter();
                
                CatalogServiceItemDetailData respItem = new CatalogServiceItemDetailData();

                respItem.item = fillItemRespData(result.responseBaseData, input.returnPrices, theItem, uomConverter, currConverter);
                
                if (input.returnContracts) {
                    fillItemContractResp(catConfig, input.returnPrices, theItem, respItem.item, currConverter, log);
                    
                    userExitFillItemContractDetailResp(catConfig, input, theItem, respItem, currConverter, log, result.responseBaseData);
                }
                
                if (uomPricingIdxStart != 0) {
                    fillItemUOMPriceResp(result, input.returnPrices, uomPricingIdxStart, uomPricingIdxEnd, 
                                         priceItems, uomConverter, currConverter, respItem);
                }
                
                if (input.returnATP && theItem.getAtpResultList() != null && theItem.getAtpResultList().size() > 0) {
                    fillItemATPResp(catConfig, theItem, respItem);
                }
                
                fillItemLocResp(theItem, respItem);
                
                userExitFillItemLocResp(theItem, respItem, theCatalog, input, log, result.responseBaseData);
                
                if (input.returnCUA) {
                    fillItemCUAResp(userData, theCatalog, respItem, input.returnPrices, currConverter, catConfig, log);
                }
                
                userExitFillItemTextsResp(theItem, respItem, theCatalog, input, log, result.responseBaseData);
                        
                userExitFillItemMimesResp(theItem, respItem, theCatalog, input, log, result.responseBaseData);            

                result.item = respItem;
                
                logMessagesToResponse(result.responseBaseData, theCatalog.getMessageList(), log);
                    
            } // theItem != null
            else {
                log.debug("Item not found");
                addMessageToResponseLog(result.responseBaseData, "cat.webservice.itemNotFound", LogItem.SEVERITY_ERROR);
            }
        
            // send result message
            sendResultMessage(response, result);
        
            logEndTime(startTime, log);

            return (mapping.findForward("success"));
        }

        /**
         * Fill item ATP response data 
         * 
         * @param catConfig the catalog config object, holding configuration data
         * @param theItem the catalog item to determine the reponse data from
         * @param respItem the response item object the ATP data should be filled into
         */
        protected void fillItemATPResp(CatalogConfiguration catConfig, WebCatItem theItem, CatalogServiceItemDetailData respItem) {
            
            log.entering("fillItemATPResp");
            
            IAtpResult atpResult = theItem.getAtpResultList().get(0);
                
            respItem.atpAvailDate = atpResult.getCommittedDateStr();
            String dateFormatted = DF_ISO.format(atpResult.getCommittedDate());
            
            respItem.atpAvailDateIso = dateFormatted;
            
            respItem.atpAvailQuant = atpResult.getCommittedQuantity();
            if ( atpResult.getCommittedQuantity() != null) {
                respItem.atpAvailQuantSimpleFormat = atpResult.getCommittedQuantity().replaceAll(catConfig.getGroupingSeparator(), "");
            }
            
            log.exiting();
        }
        
        /**
         * Fill additional item attribute data for response, if available
         * 
         * @param theItem the catalog item to determine the reponse data from
         * @param respItem the response item object the LOC data should be filled into
         */
        protected void fillItemLocResp(WebCatItem theItem, CatalogServiceItemDetailData respItem) {
            
            log.entering("fillItemLocResp");
            
            Iterator neededItemAtributes = determineAreaAttributes(theItem, log);
            
            if (neededItemAtributes != null) {
                
                ArrayList attribs = new ArrayList();
                CatalogServiceLOCData attribData = null;
                IAttributeValue theAttributeVal = null;
                Iterator valIter = null;
                String[] vals = null;
                int valIdx = 0;
                
                while (neededItemAtributes.hasNext()) {
                    
                    theAttributeVal = (IAttributeValue) neededItemAtributes.next();
                    
                    if (!theAttributeVal.getAttribute().isBase() && theAttributeVal.getAttributeDescription() != null && theAttributeVal.getAttributeName().indexOf("_") != 0) {
                        attribData = new CatalogServiceLOCData();
                    
                        attribData.locInfo.id = theAttributeVal.getAttributeGuid();
                        attribData.locInfo.description  = theAttributeVal.getAttributeDescription();
                    
                        valIter = theAttributeVal.getAllAsString();
                    
                        vals = new String[theAttributeVal.getNoOfValues()];
                    
                        valIdx = 0;
                    
                        while (valIter.hasNext()) {
                            vals[valIdx] = (String) valIter.next();
                            valIdx++;
                        }
                    
                        attribData.values = vals;
                    
                        attribs.add(attribData);
                    }
                    else {
                        if (log.isDebugEnabled()) {
                            log.debug("Attribute " + theAttributeVal.getAttributeGuid() + " does not fit requirements");
                        } 
                    }
                }
                
                CatalogServiceLOCData[] attribsData = new CatalogServiceLOCData[attribs.size()];
                
                for (int i = 0; i < attribs.size(); i++) {
                    attribsData[i] = (CatalogServiceLOCData) attribs.get(i);
                }
                
                respItem.item.locData = attribsData;
            }
            
            log.exiting();
            
        }
        
        /**
         * Determines the items CUA data and sets it in the catalogs cuaList attribute
         * 
         * @param userData Object wrapping the session
         * @param theCatalog theCatalog Representation of the catalog
         * @param theItem the catalog item to determine the reponse data from
         */
        protected void determineItemCUAData(UserSessionData userData, WebCatInfo theCatalog, WebCatItem theItem)
                       throws CommunicationException {
                               
            log.entering("determineItemCUAData");
            
            theCatalog.setCuaList(null);
                    
            MarketingBusinessObjectsAware marketBom = (MarketingBusinessObjectsAware) userData.getMBOM().getBOMByType(MarketingBusinessObjectsAware.class);
                
            MarketingUser marketUser = marketBom.getMarketingUser();
            CUAManager cUAManager = marketBom.createCUAManager();
            MarketingConfiguration marketConfig = marketBom.getMarketingConfiguration();
                
            theCatalog.setCuaList(cUAManager.readCUAList(theItem, marketUser, marketConfig, theCatalog, CUA_TYPES));
            
            log.exiting();
        }
        
        /**
         * Fill response data for item CUA products
         * 
         * @param userData Object wrapping the session
         * @param theCatalog theCatalog Representation of the catalog
         * @param respItem the response item object the CUA data should be filled into
         * @param returnPrices flag, to indicate if prices should be returned/filled
         * @param currConverter the currency converter to get the ISO currency code from
         * @param catConfig the catalog config object, holding configuration data
         * @param log the location to write messages to 
         */
        protected void fillItemCUAResp(UserSessionData userData, WebCatInfo theCatalog, CatalogServiceItemDetailData respItem,
                                       boolean returnPrices, CurrencyConverter currConverter, CatalogConfiguration catConfig, 
                                       IsaLocation log)
                       throws CommunicationException {
                           
            log.entering("fillItemCUAResp");
                
            CUAList cUAList = (CUAList) theCatalog.getCuaList();
            
            if (cUAList != null) {
                
                CatalogServiceItemCUAData[] respCuaItems = new CatalogServiceItemCUAData[cUAList.size()];
                CatalogServiceItemCUAData respCuaItem = null;
                
                CatalogServiceItemData respHelpItem = new CatalogServiceItemData();
                
                Iterator cuaIter = cUAList.iterator();
 
                int cuaIdx = 0;
                
                while (cuaIter.hasNext()) {
                    
                    CUAProduct cuaProd = (CUAProduct) cuaIter.next();
                    respCuaItem = new CatalogServiceItemCUAData();
                    
                    respCuaItem.areaKey = cuaProd.getArea();
                    respCuaItem.cuaType = cuaProd.getCuaType();
                    respCuaItem.itemKey = cuaProd.getItemId() ;
                    respCuaItem.productDesc = cuaProd.getDescription() ;
                    respCuaItem.productId = cuaProd.getId() ;
                    respCuaItem.thumbNail = cuaProd.getCatalogItem().getRelThumbNailPath();
                    
                    if (returnPrices) {
                        fillItemPriceResp(returnPrices, respHelpItem, currConverter, cuaProd.getCatalogItem().getItemPrice());
                    
                        respCuaItem.currency = respHelpItem.currencyStdPrice;
                        respCuaItem.currencyIso = respHelpItem.currencyStdPriceIso;
                        respCuaItem.price = respHelpItem.stdPrice;
                        if (respHelpItem.stdPrice != null) {
                            respCuaItem.priceSimpleFormat = respHelpItem.stdPrice.replaceAll(catConfig.getGroupingSeparator(), "");
                        }
                        
                    }
                    
                    respCuaItems[cuaIdx] = respCuaItem;
                    cuaIdx++;
                } 
                
                respItem.cuaData = respCuaItems;
            }
            
            log.exiting();
        }

        /**
         * Fill response data for prices for additional units of measurements
         * 
         * @param result CatalogServiceItemDetailResponse the response object
         * @param returnPrices flag, to indicate if prices should be returned/filled
         * @param uomPricingIdxStart start index of additionl items for UOM prices
         * @param uomPricingIdxEnd end index of additionl items for UOM prices
         * @param priceItems list og priced items
         * @param UOMConverter the uom converter to get the ISO uom code from
         * @param currConverter the currency converter to get the ISO currency code from 
         * @param respItem the response item object the price data should be filled into
         */
        protected void fillItemUOMPriceResp(CatalogServiceItemDetailResponse result, boolean returnPrices,
                                            int uomPricingIdxStart, int uomPricingIdxEnd, ArrayList priceItems,
                                            UOMConverter uomConverter, CurrencyConverter currConverter, 
                                            CatalogServiceItemDetailData respItem)
            throws CommunicationException {
                
            log.entering("fillItemUOMPriceResp");
                
            WebCatItem additionalItem;
            
            int noAddUoms = uomPricingIdxEnd - uomPricingIdxStart + 1;
            
            CatalogServiceItemData respHelpItem = new CatalogServiceItemData();
            
            CatalogServiceItemAddUOMPrices[] addUOMPrices = new CatalogServiceItemAddUOMPrices[noAddUoms];
            
            CatalogServiceItemAddUOMPrices addUOMPrice = null;
            
            for (int i = 0; i < noAddUoms; i++) {
                
                additionalItem = (WebCatItem) priceItems.get(uomPricingIdxStart + i);
                fillItemPriceResp(returnPrices, respHelpItem, currConverter, additionalItem.getItemPrice());
                addUOMPrice = new CatalogServiceItemAddUOMPrices();
                
                addUOMPrice.currencySpecialPrice = respHelpItem.currencySpecialPrice;
                addUOMPrice.currencySpecialPriceIso= respHelpItem.currencySpecialPriceIso;
                addUOMPrice.currencyStdPrice = respHelpItem.currencyStdPrice;
                addUOMPrice.currencyStdPriceIso = respHelpItem.currencyStdPriceIso;
                addUOMPrice.specialPrice = respHelpItem.specialPrice;
                addUOMPrice.stdPrice = respHelpItem.stdPrice;
                addUOMPrice.uom = additionalItem.getUnit();
                addUOMPrice.uomIso = uomConverter.convertUOM(true, addUOMPrice.uom, result.responseBaseData.languageIso);
                
                addUOMPrices[i] = addUOMPrice;
            }
            
            respItem.addUOMPrices = addUOMPrices;
            
            log.exiting();
        }

        /**
         * Determine ATP info for the item and given data from the input (quantityForATPCheck, uomForATPCheck)
         * 
         * @param userData Object wrapping the session
         * @param mbom the MetaBusinessObjectManager
         * @param input the CatalogServiceItemDetailRequest input object
         * @param theItem the catalog item to determine the ATP data for
         */
        protected void determineItemATPData(UserSessionData userData, MetaBusinessObjectManager mbom, 
                                            CatalogServiceItemDetailRequest input, WebCatItem theItem) {
                                                
            log.entering("determineItemATPData");
            
            String origQuant = theItem.getQuantity();
                                           
            if (input.quantityForATPCheck != null && input.quantityForATPCheck.trim().length() > 0) {
                if (log.isDebugEnabled()) {
                    log.debug("Setting quantity for ATP Check to " + input.quantityForATPCheck);
                }
                theItem.setQuantity(input.quantityForATPCheck);
            }
            if (input.uomForATPCheck != null && input.uomForATPCheck.trim().length() > 0) {
                if (log.isDebugEnabled()) {
                    log.debug("Setting Unit for ATP Check to " + input.uomForATPCheck);
                }
                theItem.setUnit(input.uomForATPCheck);
            }
            
            CatalogBusinessObjectManager bom = (CatalogBusinessObjectManager) mbom.getBOMbyName(CatalogBusinessObjectManager.CATALOG_BOM);
            
            determineATPResult(userData, bom, theItem, log);
            
            theItem.setQuantity(origQuant);
            
            log.exiting();
        }
        
        /**
         * Send the response
         * 
         * @param response the respons eobject to send
         * @param result the object, CatalogServiceItemDetailResponse to send in the response
         */
        private void sendResultMessage(HttpServletResponse response, CatalogServiceItemDetailResponse result) throws IOException {
            
            log.entering("sendResultMessage");
            
            response.setContentType("application/x-java-serialized-object");
            ObjectOutputStream objOut = new ObjectOutputStream(response.getOutputStream());
            objOut.writeObject(result.responseBaseData.log);
            objOut.writeObject(result);
            objOut.close();
            
            log.exiting();
        }
        
        /**
         * Gets the service request data from the HttpServletRequest object
         * 
         * @return CatalogServiceItemDetailRequest the itemDetails requets object
         */
        private CatalogServiceItemDetailRequest getInputMessage(HttpServletRequest request) throws IOException {
            
            log.entering("getInputMessage");

            InputStream is = request.getInputStream();
            ObjectInputStream objIn = new ObjectInputStream(is);
            CatalogServiceItemDetailRequest input = null;
        
            try {
                CatalogServiceRequestBase requestBaseData = (CatalogServiceRequestBase)objIn.readObject();
                input = (CatalogServiceItemDetailRequest) objIn.readObject();
            }
            catch (IOException e) {
                e.printStackTrace();
            }
            catch (ClassNotFoundException e) {
                e.printStackTrace();
            }
            
            log.exiting();
        
            return input;
        }
    
        /**
         * User exit to be overwritten, if additional Area texts should
         * be returned in the response
         * 
         * @param theArea the catalog area to determine the data from
         * @param respArea the area response object to add the text data to
         * @param theCatalog theCatalog Representation of the catalog
         * @param input the CatalogServiceItemDetailRequest input object
         * @param log the location to write messages to
         * @param responseBaseData the response base data object@param responseBaseData the response base data object
         */
        public void userExitFillAreaTextsResp(WebCatArea theArea, 
                                              CatalogServiceAreaData respArea, 
                                              WebCatInfo theCatalog, 
                                              CatalogServiceItemDetailRequest input, 
                                              IsaLocation log,
                                              CatalogServiceResponseBaseData responseBaseData) {
        
        }
    
        /**
         * User exit to be overwritten, if additional Area mimes should
         * be returned in the response
         * 
         * @param theArea the catalog area to determine the data from
         * @param respArea the area response object to add the mimes data to
         * @param theCatalog theCatalog Representation of the catalog
         * @param input the CatalogServiceItemDetailRequest input object
         * @param log the location to write messages to
         * @param responseBaseData the response base data object
         */
        public void userExitFillAreaMimesResp(WebCatArea theArea, 
                                              CatalogServiceAreaData respArea, 
                                              WebCatInfo theCatalog, 
                                              CatalogServiceItemDetailRequest input, 
                                              IsaLocation log,
                                              CatalogServiceResponseBaseData responseBaseData) {
        
        }
    
        /**
         * User exit to be overwritten, if item mimes should
         * be returned in the response
         * 
         * @param theItem the catalog item to determine the reponse data from
         * @param respItem the response item object to add the mimes data to
         * @param theCatalog theCatalog Representation of the catalog
         * @param input the CatalogServiceItemDetailRequest input object
         * @param log the location to write messages to
         * @param responseBaseData the response base data object
         */
        public void userExitFillItemMimesResp(WebCatItem theItem, 
                                              CatalogServiceItemDetailData respItem, 
                                              WebCatInfo theCatalog,
                                              CatalogServiceItemDetailRequest input, 
                                              IsaLocation log,
                                              CatalogServiceResponseBaseData responseBaseData) {
        
        }
    
        /**
         * User exit to be overwritten, if item texts should
         * be returned in the response
         * 
         * @param theItem the catalog item to determine the reponse data from
         * @param respItem the response item object to add the text data to
         * @param theCatalog theCatalog Representation of the catalog
         * @param input the CatalogServiceItemDetailRequest input object
         * @param log the location to write messages to
         * @param responseBaseData the response base data object
         */
        public void userExitFillItemTextsResp(WebCatItem theItem, 
                                              CatalogServiceItemDetailData respItem, 
                                              WebCatInfo theCatalog, 
                                              CatalogServiceItemDetailRequest input, 
                                              IsaLocation log,
                                              CatalogServiceResponseBaseData responseBaseData) {
        
        }
        
        /**
         * User exit to be overwritten, if item attribute data should
         * be returned in the response
         * 
         * @param theItem the catalog item to determine the reponse data from
         * @param respItem the response item object to add the loc data to
         * @param theCatalog theCatalog Representation of the catalog
         * @param input the CatalogServiceItemDetailRequest input object
         * @param log the location to write messages to
         * @param responseBaseData the response base data object
         */
        public void userExitFillItemLocResp(WebCatItem theItem, 
                                            CatalogServiceItemDetailData respItem, 
                                            WebCatInfo theCatalog, 
                                            CatalogServiceItemDetailRequest input, 
                                            IsaLocation log,
                                            CatalogServiceResponseBaseData responseBaseData) {
            
        }
 
        /**
         * User exit to be overwritten, if item attribute data should
         * be returned in the response
         * 
         * @param userData Object wrapping the session
         * @param theCatalog theCatalog Representation of the catalog
         * @param input the CatalogServiceItemDetailRequest input object
         * @param bom the catalog business object manager
         * @param theItem the catalog item to determine the reponse data from
         * @param contractView the contract view
         * @param log the location to write messages to
         * @param responseBaseData the response base data object
         */       
        public void userExitDetermineItemsContractDetailData(UserSessionData userData, 
                                                             WebCatInfo theCatalog, 
                                                             CatalogServiceItemDetailRequest input, 
                                                             CatalogBusinessObjectManager bom, 
                                                             WebCatItem theItem, 
                                                             ContractView contractView, 
                                                             IsaLocation log,
                                                             CatalogServiceResponseBaseData responseBaseData) {
            
        }

        /**
         * User exit to be overwritten, if item attribute data should
         * be returned in the response
         * 
         * @param catConfig the catalog config object, holding configuration data
         * @param input the CatalogServiceItemDetailRequest input object#
         * @param theItem the catalog item to determine the reponse data from
         * @param respItem the response item object to add the loc data to
         * @param currConverter the currency converter to get the ISO currency code from 
         * @param log the location to write messages to
         * @param responseBaseData the response base data object
         */ 
        public void userExitFillItemContractDetailResp(CatalogConfiguration catConfig, 
                                                       CatalogServiceItemDetailRequest input, 
                                                       WebCatItem theItem, 
                                                       CatalogServiceItemDetailData respItem, 
                                                       CurrencyConverter currConverter,
                                                       IsaLocation log,
                                                       CatalogServiceResponseBaseData responseBaseData) {
            
        }
        
}
