package com.sap.isa.catalog.actions;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.sap.isa.backend.boi.webcatalog.CatalogConfiguration;
import com.sap.isa.businessobject.CommunicationException;
import com.sap.isa.catalog.boi.IServerEngine;
import com.sap.isa.catalog.webcatalog.WebCatInfo;
import com.sap.isa.core.UserSessionData;
import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.core.util.Message;
import com.sap.isa.core.util.table.ResultData;
import com.sap.isa.isacore.MessageDisplayer;

public class DetermineInactiveCatalogInfoAction extends CatalogBaseAction {

	private static IsaLocation log =
		IsaLocation.getInstance(DetermineInactiveCatalogInfoAction.class.getName());

	/**
	 * Name of the lists of inactive catalog stored in the request context.
	 */
	public static final String RK_INACTIVE_CATALOG_LIST = "inactiveCatalogList";

	/**
	 * This action will determine information on all available inactive staging 
	 * catalogues for the given catalogue, via calling the function 
	 * getInactiveCatalogInfos() method of the WebCatInfoClass.
	 * If there is no inactive staging catalog , ......
	 * If there are inactive staging catalogs, reads the staging catalog and prepares 
	 * the results for being displayed on the new inactiveCatalogList.jsp 
	 *
	 * @param mapping The ActionMapping used to select this instance
	 * @param form The optional ActionForm bean for this request (if any)
	 * @param request The HTTP request we are processing
	 * @param response The HTTP response we are creating
	 * @param userData container for session informations
	 * @param theCatalog representation of the catalog
	 *
	 * @exception IOException if an input/output error occurs
	 * @exception ServletException if a servlet exception occurs
	 * 
	 * @return specific action forward corresponding to the struts configuration
	 */
	public ActionForward doPerform(
		ActionMapping mapping,
		ActionForm form,
		HttpServletRequest request,
		HttpServletResponse response,
		UserSessionData userData,
		WebCatInfo theCatalog)
		throws IOException, ServletException, CommunicationException {

		log.entering("DetermineInactiveCatalogInfoAction_doPerform");
		String forward = "success";

		if (!theCatalog.getCatalogServerEngine().getStagingSupport().equals(IServerEngine.STAGING_NOT_SUPPORTED)) {
				
			int returnedValue = theCatalog.checkServerEngine();

			if (returnedValue != WebCatInfo.STAGING_OK) {
				String msg = new String();
				if (returnedValue == WebCatInfo.STAGING_NO_SERVER) {
					msg = "stagingCatalog.noServer";
				} else { /* retCode == WebCatInfo.STAGING_NOT_SUPPORTED */
					msg = "stagingCatalog.notSupported";
				}
				MessageDisplayer messageDisplayer =
                createMessage(msg, request);
				forward = "message";
			} 
            //display the staging catalog list
            else {
                // display inactive, active, obsolete catalogs
                String stagingType = "";
                //get commom and campaign BOM

                CatalogConfiguration config = getCatalogConfiguration(userData);
                String country = "";
                if (config != null) {
                    country = config.getCountry();
                }
        
                ResultData inactiveCatalogList = theCatalog.getInactiveCatalogInfos(stagingType, country);
				request.setAttribute(RK_INACTIVE_CATALOG_LIST, inactiveCatalogList);
			}
		} else {
			MessageDisplayer messageDisplayer =
            createMessage("stagingCatalog.notSupported", request);
			forward = "message";
		}

		log.exiting();
		return mapping.findForward(forward);
	}

	private MessageDisplayer createMessage(
		String message,
		HttpServletRequest request) {
		log.entering("createMessage");
		MessageDisplayer messageDisplayer = new MessageDisplayer();
		messageDisplayer.addToRequest(request);
		messageDisplayer.addMessage(
			new Message(Message.ERROR, message, null, null));

		log.debug(message);
		log.exiting();

		return messageDisplayer;
	}
}
