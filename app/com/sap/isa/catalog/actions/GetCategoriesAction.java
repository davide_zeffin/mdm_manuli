package com.sap.isa.catalog.actions;

import java.io.IOException;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.sap.isa.businessobject.BusinessObjectException;
import com.sap.isa.businessobject.CommunicationException;
import com.sap.isa.businessobject.webcatalog.CatalogBusinessObjectManager;
import com.sap.isa.catalog.boi.CatalogException;
import com.sap.isa.catalog.webcatalog.WebCatArea;
import com.sap.isa.catalog.webcatalog.WebCatAreaList;
import com.sap.isa.catalog.webcatalog.WebCatAreaPage;
import com.sap.isa.catalog.webcatalog.WebCatInfo;
import com.sap.isa.core.UserSessionData;
import com.sap.isa.core.util.Message;

/**
 * Title:        GetCategoriesAction
 * Description:  Query the catalog about the categories who are childrens of the requested area
 *               (if there is any requested area),
 *               or gets all the categories from the catalog, if no area is provided as request.
 */

public class GetCategoriesAction extends CatalogBaseAction {

    /**
     * Process the specified HTTP request, and create the corresponding HTTP
     * response (or forward to another web component that will create it).
     * Return an <code>ActionForward</code> instance describing where and how
     * control should be forwarded, or <code>null</code> if the response has
     * already been completed.
     *
     * @param mapping The ActionMapping used to select this instance
     * @param actionForm The optional ActionForm bean for this request (if any)
     * @param request The HTTP request we are processing
     * @param response The HTTP response we are creating
     *
     * @exception IOException if an input/output error occurs
     * @exception ServletException if a servlet exception occurs
     */
    public ActionForward doPerform(
        ActionMapping mapping,
        ActionForm form,
        HttpServletRequest request,
        HttpServletResponse response,
        UserSessionData userData,
        WebCatInfo theCatalog)
        throws IOException, ServletException, CommunicationException {

        if (theCatalog == null) {
            return (mapping.findForward("error"));
        }

        String areaID = (String) request.getParameter("key");

        //String first;
        WebCatAreaList theCategories = null;

        if (areaID == null) {

            if (theCatalog.getCurrentAreaList() != null) {
                theCategories = theCatalog.getCurrentAreaList();
            }

            else if (theCatalog.getCurrentArea() != null && theCatalog.getCurrentArea().getAreaID() != WebCatArea.ROOT_AREA) {
                try {
                    theCategories = theCatalog.getCurrentArea().getChildren();
                }
                catch (CatalogException e) {
                    log.error("catalog.exception.backend", e);
                    Message msg = new Message(Message.ERROR, "catalog.exception.usermsg");
                    BusinessObjectException boe = new BusinessObjectException("Catalog error", msg);
                    request.setAttribute("Exception", boe);
                    return mapping.findForward(ActionConstants.FW_ERROR);
                }
            }
            
            else {
                try {
                    theCategories = theCatalog.getAllCategories();
                }
                catch (CatalogException e) {
                    log.error("catalog.exception.backend", e);
                    Message msg = new Message(Message.ERROR, "catalog.exception.usermsg");
                    BusinessObjectException boe = new BusinessObjectException("Catalog error", msg);
                    request.setAttribute("Exception", boe);
                    return mapping.findForward(ActionConstants.FW_ERROR);
                }
                theCategories.sort("");
            }
        }

        else {
            // areaID != null
            theCatalog.setCurrentArea(areaID);
            try {
                theCategories = theCatalog.getCurrentArea().getChildren();
            }
            catch (CatalogException e) {
                log.error("catalog.exception.backend", e);
                Message msg = new Message(Message.ERROR, "catalog.exception.usermsg");
                BusinessObjectException boe = new BusinessObjectException("Catalog error", msg);
                request.setAttribute("Exception", boe);
                return mapping.findForward(ActionConstants.FW_ERROR);
            }
            theCategories.sort("");
        }
        theCatalog.setCurrentAreaList(theCategories);
        theCatalog.setLastVisited(WebCatInfo.AREALIST);

        String page = request.getParameter("first");
        WebCatAreaPage areaPage = null;
        if (page != null) {
            if (page.length() >= 1)
                page = page.substring(0, 1).toUpperCase();
            areaPage = theCategories.getAreaPage(page);
        }
        else if (theCategories.getCurrentAreaPage() != null) {
            areaPage = theCategories.getCurrentAreaPage();
        }
        else {
            ServletContext context = this.getServlet().getServletContext();
            String firstChar = context.getInitParameter(ActionConstants.IN_START_CHAR);
            if (firstChar == null)
                firstChar = "";
            if (firstChar.length() >= 1)
                page = firstChar.substring(0, 1).toUpperCase();
            else
                page = firstChar;
            areaPage = theCategories.getAreaPage(page);
        }
        if (log.isDebugEnabled())
            log.debug("GetCategoriesAction: AreaList=" + theCategories + ", areaPage=" + areaPage);

        request.setAttribute("areaPage", areaPage);

        return (mapping.findForward(ActionConstants.FW_SUCCESS));
    }
}
