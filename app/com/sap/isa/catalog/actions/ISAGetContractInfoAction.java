package com.sap.isa.catalog.actions;

import com.sap.isa.businessobject.webcatalog.contract.ContractReferenceRequest;
import com.sap.isa.catalog.webcatalog.WebCatInfo;
import com.sap.isa.catalog.webcatalog.WebCatItem;
import com.sap.isa.core.UserSessionData;

/**
 * Title:
 * Description:
 * Copyright:    Copyright (c) 2001
 * Company:
 * @author
 * @version 1.0
 */

public class ISAGetContractInfoAction extends GetContractInfoAction {

  protected void fillContractReferenceRequest(
                                  ContractReferenceRequest contractRequest,
                                  WebCatItem item,
                                  WebCatInfo catalog,
                                  UserSessionData userData) {
    super.fillContractReferenceRequest(contractRequest, item, catalog, userData);
    
    fillContractReferenceRequestForSoldToAndSalesOrg(contractRequest, userData);
  }

}
