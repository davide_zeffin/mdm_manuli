package com.sap.isa.catalog.actions;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.sap.isa.businessobject.CommunicationException;
import com.sap.isa.businessobject.webcatalog.CatalogBusinessObjectManager;
import com.sap.isa.catalog.webcatalog.WebCatInfo;
import com.sap.isa.catalog.webcatalog.WebCatItem;
import com.sap.isa.catalog.webcatalog.WebCatItemList;
import com.sap.isa.core.UserSessionData;

/**
 * Title:
 * Description:
 * Copyright:    Copyright (c) 2001
 * Company:
 * @author
 * @version 1.0
 */

public class CrossSellingAction extends CatalogBaseAction {

    /**
     * Process the specified HTTP request, and create the corresponding HTTP
     * response (or forward to another web component that will create it).
     * Return an <code>ActionForward</code> instance describing where and how
     * control should be forwarded, or <code>null</code> if the response has
     * already been completed.
     *
     * @param mapping The ActionMapping used to select this instance
     * @param form The optional ActionForm bean for this request (if any)
     * @param request The HTTP request we are processing
     * @param response The HTTP response we are creating
     * @param userData container for session informations
     * @param theCatalog representation of the catalog
     *
     * @exception IOException if an input/output error occurs
     * @exception ServletException if a servlet exception occurs
     * 
     * @return specific action forward corresponding to the struts configuration
     */
    public ActionForward doPerform(ActionMapping mapping,
            ActionForm form,
            HttpServletRequest request,
            HttpServletResponse response,
            UserSessionData userData,
            WebCatInfo theCatalog)
            throws IOException, ServletException, CommunicationException {
                
        CatalogBusinessObjectManager bom =
            (CatalogBusinessObjectManager)userData.getBOM(CatalogBusinessObjectManager.CATALOG_BOM);
        log.debug("Start CrossSellingAction");

        if (bom.getCatalog().getCurrentItem() != null) {
            request.setAttribute(ActionConstants.RC_WEBCATITEM,
                bom.getCatalog().getCurrentItem());
        }
        else {
            WebCatItemList items = bom.getCatalog().getCurrentItemList();
            WebCatItem item = null;
            String itemKey = request.getParameter(ActionConstants.RA_ITEMKEY);
            
            if (itemKey == null) {
                log.error("catalog.message.noItemProvided", new Exception("catalog.message.noItemProvided"));
            }
            
            if (items == null) {
                log.error(
                    "catalog.message.itemsNotFound",
                    new String[] { "No Current Item List Defined" },
                    new Exception("catalog.message.itemsNotFound"));
            }
            
            if (items != null && itemKey != null) {
                item = items.getItem(itemKey);
            }    
            if (item == null) {
                log.error(
                    "catalog.message.itemNotFound",
                    new String[] { itemKey },
                    new Exception("catalog.message.itemNotFound"));
            }
            else {
                request.setAttribute(ActionConstants.RC_WEBCATITEM, item);
            }
        }
        return mapping.findForward(ActionConstants.FW_SUCCESS);
    }
}
