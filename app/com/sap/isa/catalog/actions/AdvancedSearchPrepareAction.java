package com.sap.isa.catalog.actions;

import java.io.IOException;
import java.util.HashMap;
import java.util.regex.Pattern;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.sap.isa.businessobject.CommunicationException;
import com.sap.isa.catalog.boi.CatalogException;
import com.sap.isa.catalog.boi.ICatalog;
import com.sap.isa.catalog.boi.IQuery;
import com.sap.isa.catalog.boi.IQueryStatement;
import com.sap.isa.catalog.filter.CatalogFilter;
import com.sap.isa.catalog.filter.CatalogFilterFactory;
import com.sap.isa.catalog.impl.CatalogQueryStatement;
import com.sap.isa.catalog.webcatalog.WebCatArea;
import com.sap.isa.catalog.webcatalog.WebCatAreaList;
import com.sap.isa.catalog.webcatalog.WebCatInfo;
import com.sap.isa.catalog.webcatalog.WebCatItemList;
import com.sap.isa.core.UserSessionData;
import com.sap.isa.core.ui.context.ContextManager;
import com.sap.isa.core.ui.context.GlobalContextManager;
import com.sap.isa.core.util.table.ResultData;
import com.sap.isa.core.util.table.Table;
import com.sap.isa.core.util.table.TableRow;
import com.sap.isa.isacore.action.EComBaseAction;

/**
 * Title:        AdvancedSearchPrepareAction
 * Description:
 * Copyright:    Copyright (c) 2008
 * Company:      SAP AG
 * @version 1.0
 */

public class AdvancedSearchPrepareAction extends CatalogBaseAction {

     
    private final String PROP_FUZZY_SEARCH = "enableFuzzySearch";
    
    /**
     * Process the specified HTTP request, and create the corresponding HTTP
     * response (or forward to another web component that will create it).
     * Return an <code>ActionForward</code> instance describing where and how
     * control should be forwarded, or <code>null</code> if the response has
     * already been completed.
     *
     * @param mapping The ActionMapping used to select this instance
     * @param form The optional ActionForm bean for this request (if any)
     * @param request The HTTP request we are processing
     * @param response The HTTP response we are creating
     * @param userData container for session informations
     * @param theCatalog representation of the catalog
     *
     * @exception IOException if an input/output error occurs
     * @exception ServletException if a servlet exception occurs
     * 
     * @return specific action forward corresponding to the struts configuration
     */
    public ActionForward doPerform(ActionMapping mapping,
            ActionForm form,
            HttpServletRequest request,
            HttpServletResponse response,
            UserSessionData userData,
            WebCatInfo theCatalog)
            throws IOException, ServletException, CommunicationException {

        final String METHOD_NAME = "AdvancedSearchPrepareAction";
        log.entering(METHOD_NAME);

        // Page that should be displayed next.
        String forward = ActionConstants.FW_SUCCESS;
        
		EComBaseAction.IsaActionMapping isaActionMapping = (EComBaseAction.IsaActionMapping)mapping;

        //If not do it yourself if you can
        String[] queryAttr = null;
        if (theCatalog != null) {
            String queryStr = request.getParameter(ActionConstants.RA_CURRENT_QUERY);
            if (null == queryStr && null != request.getAttribute(ActionConstants.RA_CURRENT_QUERY)) {
                queryStr = request.getAttribute(ActionConstants.RA_CURRENT_QUERY).toString();
            }
    
            ResultData attrToResKeyTab = null;

            try {
                queryAttr = getQuickSearchAttributes(theCatalog);
                
                ICatalog iCatalog = theCatalog.getCatalog();
                IQueryStatement queryStmt = iCatalog.createQueryStatement();

                attrToResKeyTab = iCatalog.getAttributeResKeys();

                CatalogFilterFactory fact = CatalogFilterFactory.getInstance();

            }
            catch (CatalogException ex) {
                log.error("system.backend.exception", ex);
            }
                
            // set all the required data as request attributes and pass it to the register JSP
            request.setAttribute(ActionConstants.RA_ATTRIB_LIST, attrToResKeyTab);
            
        }
        
        // set catalog areas as request attribute 
        ResultData areaList = null;
        WebCatAreaList webCatAreaList = null;
        try {
            webCatAreaList = theCatalog.getAllCategories();
            
            Table attrTab = new Table("areaList");
            attrTab.addColumn(Table.TYPE_STRING, "areaKey");
            attrTab.addColumn(Table.TYPE_STRING, "areaName");

            for (int i = 0; i < webCatAreaList.size(); i++) {
                WebCatArea area = webCatAreaList.getArea(i);

                TableRow attRow = attrTab.insertRow();

                String aKey = area.getAreaType() + "%" + area.getAreaID();
                attRow.getField(1).setValue(aKey);
                attRow.getField(2).setValue(area.getAreaName());
            }
        
            areaList = new ResultData(attrTab);
        }
        catch (CatalogException ex) {
            log.error("system.backend.exception", ex);
        }
        request.setAttribute("stdAreas", webCatAreaList);

        // query string
        String advSQuery = request.getParameter(ActionConstants.RA_CURRENT_QUERY);
        if (advSQuery == null && request.getAttribute(ActionConstants.RA_CURRENT_QUERY) != null) {
            advSQuery = request.getAttribute(ActionConstants.RA_CURRENT_QUERY).toString();
        }
        if (advSQuery == null && GlobalContextManager.getValue(ActionConstants.CV_CURRENT_QUERY) != null) {
            ContextManager contextManager =  ContextManager.getManagerFromRequest(request); 
            advSQuery = contextManager.getContextValue(ActionConstants.CV_CURRENT_QUERY);
        }       
        if (advSQuery == null) {
            advSQuery = "*";
        }
//        request.setAttribute(ActionConstants.RA_CURRENT_QUERY, advSQuery);

        // analyze query statement and set attributes accordingly
        analyzeQuery(request, theCatalog, queryAttr);

        log.exiting();
        return mapping.findForward(forward);
    }
  
    /**
     * Analyzes the query statement contained in the current item list
     */  
    private void analyzeQuery(HttpServletRequest request, WebCatInfo theCatalog, String[] queryAttr) {
        
        // get the previous query result
        WebCatItemList itemList = (WebCatItemList) request.getAttribute(ActionConstants.RA_ITEMLIST_EBP);
        IQueryStatement.Type qType = null;
        
        if (itemList == null) {
            itemList = theCatalog.getCurrentItemList();
        }
        if (itemList == null ||
            queryAttr == null) {
            return;
        }
        
        IQuery query = itemList.getQuery();
        
        if (query != null &&
            query.getStatement() != null) {
            qType = query.getStatement().getSearchType();    
        }

        // only if advanced search
        if (qType != null && qType.equals(IQueryStatement.Type.ITEM_ADVSEARCH)) {
            
            // Selected attributes 
            String advSQuery = "";
            // Selected attributes 
            HashMap attMap = new HashMap();
            // Selected Catalog types 
            String advSStdCat = "";
            String advSRewCat = "";
            // Selected Catalog Areas 
            String advSStdAreaGuid = null;
            String advSRewAreaGuid = null;
            // Points 
            String advSPtFrom = null;
            String advSPtTo = null;

            CatalogQueryStatement queryStatement = (CatalogQueryStatement) query.getStatement();

            CatalogFilter filterTerm = null;
            String stmtAsString = null;
            String temp = null;
            String key = null;

            if (queryStatement != null) {
                filterTerm = queryStatement.getFilter();

                if (filterTerm != null) {
                    Pattern p = Pattern.compile("AND|OR");
                    String arrTerms[] = p.split(filterTerm.toString());
                    boolean firstAttrib = true;
 
                    for (int i = 0; i < arrTerms.length; i++) {
                        key = null;
                        boolean attrFound = false;
                        p = Pattern.compile("=\"|=fuzzy\\(|=containing\\(");

                        String arr1[] = p.split(arrTerms[i]);

                        for (int j = 0; j < arr1.length; j++) {
                            boolean isNOTfound = false;
                            temp = arr1[j].replace('(', ' ');
                            temp = temp.replace(')', ' ');
                            temp = temp.replace('"', ' ');
                            temp = temp.trim();
                            // check if 'NOT' is contained and delete NOT from string
                            if (temp.length() >= 3) {
                                String prefix = temp.substring(0, 3);
                                if (prefix != null && prefix.equals("NOT")) {
                                    temp = temp.substring(3);
                                    temp = temp.trim();
                                    isNOTfound = true;
                                }
                            }

                            if (j == 0) {
                                key = temp;
                                // check attributes
                                for (int k = 0; !attrFound && k < queryAttr.length; k++) {
                                    if (key.equalsIgnoreCase(queryAttr[k])) {
                                        attrFound = true;
                                    }
                                }
                                if (attrFound) {
                                    attMap.put(key, "X");
                                    if (!firstAttrib) {
                                        break;
                                    }
                                }
                                
                                // check selected catalog type
                                if (key.equalsIgnoreCase("IS_BUY_PTS_ITEM")) {
                                    // set to true if this attribute is contained in the query 
                                    advSStdCat = "X";
                                    break;
                                }
                                if (key.equalsIgnoreCase("IS_PTS_ITEM") && !isNOTfound) {
                                    // set to true if this attribute is contained in the query without NOT
                                    advSRewCat = "X";
                                    break;
                                }
                                // check points
                            }
                            
                            if (j == 1) {
                                // check attributes
                                if (attrFound && firstAttrib) {
                                    // set search string for first attribute
                                    advSQuery = temp;     
                                    firstAttrib = false;
                                    break;
                                }
                                // check selected catalog area
                                if (key != null) {
                                    if (key.equalsIgnoreCase("AREA_GUID")) {
                                        if (advSRewCat.equals("X")) {
                                            // Area Guid corresponds to reward category
                                            advSRewAreaGuid = temp;     
                                        }
                                        else {
                                            // Area Guid corresponds to standard category
                                            advSStdAreaGuid = temp; 
                                        }
                                        break;
                                    }
                                }
                            }
                        }
                    }
                }
                if (advSQuery == null || advSQuery.length() == 0) {
                    advSQuery = "*";
                }
                request.setAttribute("advSQuery", advSQuery);
                request.setAttribute("advSSelAtt", attMap);
                request.setAttribute("advSStdCat", advSStdCat);
                request.setAttribute("advSRewCat", advSRewCat);
                if (advSStdAreaGuid != null) {
                    request.setAttribute("advSStdAreaId", advSStdAreaGuid );
                }
                if (advSRewAreaGuid != null) {
                    request.setAttribute("advSRewAreaId", advSRewAreaGuid);
                }
            }
        }
    }
    
}