package com.sap.isa.catalog.actions.ccms;

import java.io.IOException;
import java.util.Set;
import java.util.TreeSet;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.sap.isa.backend.boi.webcatalog.CatalogBusinessObjectsAware;
import com.sap.isa.businessobject.webcatalog.CatalogBusinessObjectManager;
import com.sap.isa.catalog.ccms.IMSTest;
import com.sap.isa.core.BaseAction;
import com.sap.isa.core.FrameworkConfigManager;
import com.sap.isa.core.UserSessionData;
import com.sap.isa.core.ccms.action.CcmsHeartbeatAction;
import com.sap.isa.core.init.InitializationEnvironment;
import com.sap.isa.core.interaction.InteractionConfig;
import com.sap.isa.core.xcm.XCMTestProperties;

/**
 * 
 * If there is a parameter ShopReadAction.PARAM_SHOPID in the request,
 * AddImsScenarioTestAction adds the session attribute CcmsHeartbeatAction.SCENARIO_TESTS
 * if it doesn't exist. 
 * It puts an initialized IMSTest into this attribute.
 * It reads the ShopReadAction.PARAM_SHOPID from the request and
 * passes it to IMSTest.
 */
public class AddImsScenarioTestAction extends BaseAction {
    
    /**
     * Name of the request parameter to select the shop using its id.
     */
    public static final String PARAM_SHOPID = "shopId";
    
    final static public String IMS_COMPONENTID="IMS";
    
	/**
	 * Constructor for AddImsScenarioTestAction.
	 */
	public AddImsScenarioTestAction() {
		super();
	}

	/**
	 * @see com.sap.isa.core.BaseAction#doPerform(ActionMapping, ActionForm, HttpServletRequest, HttpServletResponse)
	 */
	public ActionForward doPerform(
		ActionMapping mapping,
		ActionForm form,
		HttpServletRequest request,
		HttpServletResponse response)
		throws IOException, ServletException {
		UserSessionData userSessionData = 
	   		UserSessionData.getUserSessionData(request.getSession());
		Set tests=(Set) userSessionData.getAttribute( CcmsHeartbeatAction.SCENARIO_TESTS);
		if( tests == null )
		{
			tests = new TreeSet();
			userSessionData.setAttribute( CcmsHeartbeatAction.SCENARIO_TESTS,tests);
		}
		String shopid = request.getParameter(PARAM_SHOPID);
		if( shopid==null )
		{
			InteractionConfig f = FrameworkConfigManager.Servlet.getInteractionConfig(request).getConfig("shop");
			if( f != null)
			  shopid = f.getValue("defaultShopId");
		}
		// Get the shopId directly from the request, we do not use ReadShopAction here,
		// because ReadShopAction requires a CRM system which is available.
		if( shopid!=null && shopid.length() > 0 ) {
            
            CatalogBusinessObjectsAware catObjBom = (CatalogBusinessObjectsAware) userSessionData.getMBOM().getBOMByType(CatalogBusinessObjectsAware.class);
            
			IMSTest imsTest= new IMSTest((InitializationEnvironment)this.getServlet(), catObjBom,
				(CatalogBusinessObjectManager)userSessionData.getBOM(CatalogBusinessObjectManager.CATALOG_BOM));
			imsTest.setShopName(shopid);
			imsTest.setLocale(userSessionData.getLocale());
			XCMTestProperties imsTestProperties= new XCMTestProperties();
			imsTestProperties.setComponentID(IMS_COMPONENTID);
			imsTestProperties.setConfigID(shopid);
			imsTestProperties.setTestClass(imsTest);
			imsTestProperties.setCcmsName(IMS_COMPONENTID);
			tests.add(imsTestProperties);
		}
		return mapping.findForward("success");
	}

}
