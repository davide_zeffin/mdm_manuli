package com.sap.isa.catalog.actions;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.sap.isa.businessobject.CommunicationException;
import com.sap.isa.catalog.webcatalog.WebCatInfo;
import com.sap.isa.core.UserSessionData;

/**
 * Title:
 * Description:
 * Copyright:    Copyright (c) 2001
 * Company:
 * @author
 * @version 1.0
 */

public class BackToCUAAction extends CatalogBaseAction {

    public ActionForward doPerform(
        ActionMapping mapping,
        ActionForm form,
        HttpServletRequest request,
        HttpServletResponse response,
        UserSessionData userData,
        WebCatInfo theCatalog)
        throws IOException, ServletException, CommunicationException {

        // if shop is not set up for contracts: skip this action
        log.debug("Start BackToCUAAction");

        request.setAttribute(com.sap.isa.isacore.action.marketing.ShowCUAAction.RC_CUALIST, theCatalog.getCuaList());
        request.setAttribute(com.sap.isa.isacore.action.marketing.ShowCUAAction.RC_CUATYPE, theCatalog.getCuaType());

        return mapping.findForward(ActionConstants.FW_SUCCESS);
    }
}
