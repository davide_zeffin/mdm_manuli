package com.sap.isa.catalog.actions;

import java.io.IOException;
import java.util.Iterator;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.sap.isa.businessobject.BusinessObjectException;
import com.sap.isa.catalog.CatalogParameters;
import com.sap.isa.catalog.boi.CatalogException;
import com.sap.isa.catalog.boi.IAttribute;
import com.sap.isa.catalog.boi.IFilter;
import com.sap.isa.catalog.boi.IQuery;
import com.sap.isa.catalog.boi.IQueryStatement;
import com.sap.isa.catalog.filter.CatalogFilterFactory;
import com.sap.isa.catalog.webcatalog.WebCatInfo;
import com.sap.isa.catalog.webcatalog.WebCatItemList;
import com.sap.isa.core.UserSessionData;
import com.sap.isa.core.interaction.InteractionConfig;
import com.sap.isa.core.util.Message;
import com.sap.isa.core.util.WebUtil;

/**
 * Title:        BigSearchAction
 * Description:  Creates the query from data obtained from the input query
 *               applies it to catalog, and build the resulting WebCatItemList
 *               list of items
 * Copyright:    Copyright (c) 2001
 * Company:      SAPMarkets Europe GmbH
 * @version 1.0
 */


public class BigSearchAction extends CatalogBaseAction
{

    /**
     * Process the specified HTTP request, and create the corresponding HTTP
     * response (or forward to another web component that will create it).
     * Return an <code>ActionForward</code> instance describing where and how
     * control should be forwarded, or <code>null</code> if the response has
     * already been completed.
     *
     * @param mapping The ActionMapping used to select this instance
     * @param actionForm The optional ActionForm bean for this request (if any)
     * @param request The HTTP request we are processing
     * @param response The HTTP response we are creating
     *
     * @exception IOException if an input/output error occurs
     * @exception ServletException if a servlet exception occurs
     */

  public ActionForward doPerform( ActionMapping mapping,
                                  ActionForm form,
                                  HttpServletRequest request,
                                  HttpServletResponse response,
                                  UserSessionData userSessionData,
                                  WebCatInfo theCatalog)
  throws IOException, ServletException
  {
    log.debug("Entered to CategorieFilterAction");

    //If not do it yourself if you can
    if(theCatalog == null) {
      //WebCatInfo.catalogLog.fatal("No Catalog Information availabale!");
      return (mapping.findForward("error"));
    }
    theCatalog.setLastVisited(WebCatInfo.CATALOG_QUERY);

    Iterator attributes = null;
    try {
      attributes = theCatalog.getCatalog().getAttributes();
    }
    catch (CatalogException e) {
        log.error("catalog.exception.backend",e);
        Message msg = new Message(Message.ERROR,"catalog.exception.usermsg");
        BusinessObjectException boe = new BusinessObjectException("Catalog error",msg);
        request.setAttribute("Exception",boe);
        return mapping.findForward(ActionConstants.FW_ERROR);
    }

	boolean fuzzySearch = true;
	if (getInteractionConfig(request) != null && 
		getInteractionConfig(request).getConfig(ActionConstants.IN_IMS_CONNECTION) != null) {
        InteractionConfig catalogConf = getInteractionConfig(request).getConfig(ActionConstants.IN_IMS_CONNECTION);
        String enableFuzzySearch = catalogConf.getValue(ActionConstants.IN_ENABLE_FUZZY);
        if(enableFuzzySearch!=null && enableFuzzySearch.equalsIgnoreCase("false")) {
            fuzzySearch = false; 
        }	     	      
	}

    CatalogFilterFactory fact = CatalogFilterFactory.getInstance();
    
    IFilter filter = null;
    IFilter productFilter;
    IAttribute attribute;
    String formValue;

    while (attributes.hasNext()) {
      attribute = (IAttribute) attributes.next();
      formValue = request.getParameter(attribute.getGuid());

      if (formValue != null && !"".equals(formValue)) { //it's ok to include this value to the filters.
        
        if (formValue.indexOf("*") == 0 || formValue.indexOf("*") == formValue.length()-1) {
			productFilter = fact.createAttrContainValue(attribute.getGuid(), formValue);	
        }            
        else{
			if (fuzzySearch) {
                productFilter = fact.createAttrFuzzyValue(attribute.getGuid(), formValue);
			}	
			else {
                productFilter = fact.createAttrEqualValue(attribute.getGuid(), formValue);
			}	
        }

        if (filter == null) {
            filter = productFilter;
        } 
        else {
            if ("and".equals(request.getParameter("algorithm"))) {
                filter = fact.createAnd(productFilter, filter);     // AND statement
            }
            else {
                filter = fact.createOr(productFilter, filter);}     // OR statement
        }   
      }
      log.debug(attribute.getGuid()+" Read from the request: "+formValue);
    }

    WebCatItemList itemList=null;
    if (filter != null) {
        try {
            IQueryStatement queryStmt = theCatalog.getCatalog().createQueryStatement();
            //queryStmt.setStatement(filter, null, null); // attributes, sort order?
		    queryStmt.setStatement(filter, null, null, 1, CatalogParameters.maxSearchResults);
            IQuery query = theCatalog.getCatalog().createQuery(queryStmt);
            // create WebCatItemList from this query and return
            itemList = new WebCatItemList(theCatalog, query);
          }
          catch (CatalogException e)  {
              log.error("system.eai.exception",e);
          }
    }
    else {
        itemList = new WebCatItemList(theCatalog);
    }
    
    theCatalog.setCurrentItemList(itemList);

    theCatalog.setExtendedSearch(true);

    if (itemList != null) {
		request.setAttribute(ActionConstants.RA_ITEMLIST_EBP, itemList);
		// argument for the title
		String [] args = {theCatalog.getCurrentArea().getAreaName()};
		String listTitleArg0 = WebUtil.translate(userSessionData.getLocale(), "b2c.cat.b2ccatarea", args); 
		request.setAttribute(ActionConstants.RA_PRODUCT_LIST_TITLE_ARG0, listTitleArg0);
    }

    // request.setAttribute("toNext","toItemPage");
    request.setAttribute("isQuery","yes");
    if (theCatalog.getCurrentArea() != null) {
        request.setAttribute("currentArea", theCatalog.getCurrentArea());
    } 

    if (itemList != null && itemList.size() > 0) {
        return (mapping.findForward(ActionConstants.FW_SUCCESS));
    }
    else {
        return (mapping.findForward(ActionConstants.FW_NO_ITEMS));
    }
  }
}
