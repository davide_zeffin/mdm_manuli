package com.sap.isa.catalog.actions;

//catalog imports
import com.sap.isa.catalog.boi.CatalogException;
import com.sap.isa.catalog.webcatalog.WebCatArea;

public class GetCategoriesTreeSimpleAction extends GetCategoriesTreeAction {

    public boolean hasChildrens(WebCatArea area) throws CatalogException {
        return hasChildrensAttrib(area);
    }

}
