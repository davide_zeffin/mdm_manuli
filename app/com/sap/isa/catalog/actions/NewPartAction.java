package com.sap.isa.catalog.actions;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.sap.isa.businessobject.CommunicationException;
import com.sap.isa.catalog.webcatalog.WebCatInfo;
import com.sap.isa.catalog.webcatalog.WebCatItem;
import com.sap.isa.catalog.webcatalog.WebCatItemList;
import com.sap.isa.core.UserSessionData;

/**
 * Title:
 * Description:
 * Copyright:    Copyright (c) 2001
 * Company:
 * @author V.Manjunath Harish
 * @version 1.0
 */

public class NewPartAction extends CatalogBaseAction {

    public ActionForward doPerform(
        ActionMapping mapping,
        ActionForm form,
        HttpServletRequest request,
        HttpServletResponse response,
        UserSessionData userData,
        WebCatInfo theCatalog)
        throws IOException, ServletException, CommunicationException {

        log.debug("Start NewPartAction");

        if (theCatalog.getCurrentItem() != null) {
            request.setAttribute(ActionConstants.RC_WEBCATITEM, theCatalog.getCurrentItem());
        }
        else {
            WebCatItemList items = theCatalog.getCurrentItemList();
            WebCatItem item = null;
            String itemKey = request.getParameter(ActionConstants.RA_ITEMKEY);

            if (itemKey == null)
                log.error("catalog.message.noItemProvided", new Exception("catalog.message.noItemProvided"));
            if (items == null)
                log.error("catalog.message.itemsNotFound", new String[] { "No Current Item List Defined" }, new Exception("catalog.message.itemsNotFound"));
            if (items != null && itemKey != null)
                item = items.getItem(itemKey);
            if (item == null)
                log.error("catalog.message.itemNotFound", new String[] { itemKey }, new Exception("catalog.message.itemNotFound"));
            else
                request.setAttribute(ActionConstants.RC_WEBCATITEM, item);
        }

        return mapping.findForward(ActionConstants.FW_SUCCESS);
    }
}
