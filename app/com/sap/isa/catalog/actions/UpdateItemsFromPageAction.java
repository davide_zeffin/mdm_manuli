package com.sap.isa.catalog.actions;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.sap.isa.backend.boi.isacore.BaseConfiguration;
import com.sap.isa.backend.boi.webcatalog.CatalogBusinessObjectsAware;
import com.sap.isa.backend.boi.webcatalog.CatalogConfiguration;
import com.sap.isa.businessobject.CommunicationException;
import com.sap.isa.catalog.actionforms.ItemGroupForm;
import com.sap.isa.catalog.actionforms.WebCatItemForm;
import com.sap.isa.catalog.actionforms.WebCatItemPageForm;
import com.sap.isa.catalog.boi.WebCatSubItemData;
import com.sap.isa.catalog.webcatalog.ItemGroup;
import com.sap.isa.catalog.webcatalog.WebCatArea;
import com.sap.isa.catalog.webcatalog.WebCatInfo;
import com.sap.isa.catalog.webcatalog.WebCatItem;
import com.sap.isa.catalog.webcatalog.WebCatItemKey;
import com.sap.isa.catalog.webcatalog.WebCatItemList;
import com.sap.isa.catalog.webcatalog.WebCatSubItem;
import com.sap.isa.catalog.webcatalog.WebCatSubItemList;
import com.sap.isa.core.Constants;
import com.sap.isa.core.PanicException;
import com.sap.isa.core.TechKey;
import com.sap.isa.core.UserSessionData;
import com.sap.isa.core.businessobject.management.MetaBusinessObjectManager;
import com.sap.isa.core.util.Message;
import com.sap.isa.core.util.RequestParser;
import com.sap.isa.core.util.WebUtil;
import com.sap.isa.isacore.action.EComBaseAction;

/**
 * Title:        UpdateItemsFromPageAction<br /> 
 * Description:  Action to update catalog item data from current itemPage
 *               in current itemList<br />
 * Copyright:    Copyright (c) 2001<br />
 * Company:      SAP AG, Germany<br />
 * 
 * @version 2.0
 */
public class UpdateItemsFromPageAction extends CatalogBaseAction {

    /**
     * Array of all <i>activities</i> which are relevant for Solution Configurator explosion.
     * The <i>activities</i> correspond to the value of the request parameter 
     * {@link ActionConstants.RA_NEXT}. Only if within this parameter a value included in this array will be found, 
     * the SC-relevant execution will be performed.
     * 
     * @see #requestIsRelevantForSc(HttpServletRequest)
     * @see #updateScItemBasedOnForm(HttpServletRequest, UserSessionData, WebCatInfo, WebCatItemForm, WebCatItem)
     * @see #updateScItemBasedOnKeys(HttpServletRequest, UserSessionData, WebCatInfo)
     */
    protected static String[] activitiesRelevantForSC = { "addToBasketDet", "seeItem", "config" };

    /**
     * Process the specified HTTP request, and create the corresponding HTTP
     * response (or forward to another web component that will create it).
     * Return an <code>ActionForward</code> instance describing where and how
     * control should be forwarded, or <code>null</code> if the response has
     * already been completed.
     * 
     * @param mapping mapping The ActionMapping used to select this instance
     * @param form The <code>FormBean</code> specified in the Struts configuration file for this action
     * @param request The request object
     * @param response The response object
     * @param userData Object wrapping the session
     * @param theCatalog Representation of the catalog
     * @param requestParser Parser to simple retrieve data from the request
     * @param mbom Meta business object manager
     * 
     * @return Forward to another action or JSP page
     * @throws IOException
     * @throws ServletException
     * @throws CommunicationException
     */
    public ActionForward doPerform(ActionMapping mapping,
            ActionForm form,
            HttpServletRequest request,
            HttpServletResponse response,
            UserSessionData userData,
            WebCatInfo theCatalog,
            RequestParser requestParser,
            MetaBusinessObjectManager mbom)
            throws IOException, ServletException, CommunicationException {

        /**
         * Map of Solution Configurator relevant top items (WebCatItems). 
         * For these items a new SC-explosion and repricing need to be performed.
         * 
         * @see #updateScItemBasedOnForm(HttpServletRequest, UserSessionData, WebCatInfo, WebCatItemForm, WebCatItem)
         * @see #updateScItemBasedOnKeys(HttpServletRequest, UserSessionData, WebCatInfo)
         */
        Map changedScTopItems = new HashMap();
        
        boolean callSC = false;

        // check isa bom, because the action is also used in the cviews and shared catalogues application
        if (mbom.checkBOMByType(CatalogBusinessObjectsAware.class)) {	

            CatalogConfiguration config = getCatalogConfiguration(userData);
        
	        // only call solution configurator in Telco scenario
	        if (config != null && BaseConfiguration.TELCO_CUSOMER_SELF_SERVICE.equals(config.getScenario())) {
	            callSC = true;
	        }
	        
	        if (log.isDebugEnabled()) {
	            if (config != null) {
	                if (log.isDebugEnabled()) {
	                    log.debug("Configuration scenario = " + config.getScenario());
	                }
	            }
	            else {
	                log.debug("shop is null");
	            }
	            if (log.isDebugEnabled()) {
	                log.debug("callSC = " + callSC);
	            }
	        }
        }    

        // next action        
        String nextAction = request.getParameter(ActionConstants.RA_NEXT);
        if (log.isDebugEnabled()) {
            log.debug("Request parameter '" + ActionConstants.RA_NEXT + "' = " + nextAction);
        }
        if (null == nextAction) {
            nextAction = ActionConstants.FW_SUCCESS;
        }
        
        // detail scenario
        String detailScenario = request.getParameter(ActionConstants.RA_DETAILSCENARIO);
        if (log.isDebugEnabled()) {
            log.debug("Request parameter '" + ActionConstants.RA_DETAILSCENARIO + "' = " + detailScenario);
        }
        if (detailScenario == null || detailScenario.length() == 0) {
            detailScenario = requestParser.getParameter(ActionConstants.RA_DETAILSCENARIO).getValue().getString();  
        }
        if (detailScenario != null) {
            request.setAttribute(ActionConstants.RA_DETAILSCENARIO, detailScenario);
        }

        // detail scenario of parent item or item list
        String oldDetailScenario = request.getParameter(ActionConstants.RA_OLDDETAILSCENARIO);
        if (log.isDebugEnabled()) {
            log.debug("Request parameter '" + ActionConstants.RA_OLDDETAILSCENARIO + "' = " + oldDetailScenario);
        }
        if (oldDetailScenario == null || oldDetailScenario.length() == 0) {
            oldDetailScenario = requestParser.getParameter(ActionConstants.RA_OLDDETAILSCENARIO).getValue().getString();  
        }
        if (oldDetailScenario != null) {
            request.setAttribute(ActionConstants.RA_OLDDETAILSCENARIO, oldDetailScenario);
        }

        // related product of a cua item 
        String cuaRelatedProduct = request.getParameter(ActionConstants.RA_CUA_RELATED_PRODUCT);
        if (log.isDebugEnabled()) {
            log.debug("Request parameter '" + ActionConstants.RA_CUA_RELATED_PRODUCT + "' = " + cuaRelatedProduct);
        }
        if (cuaRelatedProduct == null || cuaRelatedProduct.length() == 0) {
            cuaRelatedProduct = requestParser.getParameter(ActionConstants.RA_CUA_RELATED_PRODUCT).getValue().getString();  
        }
        if (cuaRelatedProduct != null) {
            request.setAttribute(ActionConstants.RA_CUA_RELATED_PRODUCT, cuaRelatedProduct);
        }
        
        // isQuery
        String isQuery = request.getParameter(ActionConstants.RA_IS_QUERY);
        if (log.isDebugEnabled()) {
            log.debug("Request parameter '" + ActionConstants.RA_IS_QUERY + "' = " + isQuery);
        }
        if (isQuery == null || isQuery.length() == 0) {
            isQuery = requestParser.getParameter(ActionConstants.RA_IS_QUERY).getValue().getString();  
        }
        if (isQuery != null) {
            request.setAttribute(ActionConstants.RA_IS_QUERY, isQuery);
        }

        //in case of 'You Can/Must Select More Product Option' -- we should set the catalog.lastVisited value -- error if before compareItems. 
        String lastVisited = request.getParameter(ActionConstants.LAST_VISITED);
        if (lastVisited != null && !lastVisited.equals("")) {
               theCatalog.setLastVisited(lastVisited); 
        }
        
        //reset the product detail list type
        theCatalog.setProductDetailListType(0);
        
        HttpSession theSession = request.getSession();

        WebCatItemList items = theCatalog.getCurrentItemList();
        WebCatItemList cuaItems = theCatalog.getCurrentCUAList();

        WebCatItemPageForm itemPageForm = (WebCatItemPageForm) form;
        Iterator aIter = itemPageForm.itemIterator();
        Iterator bIter = itemPageForm.itemGroupIterator();
        
        boolean setBasketDirty = false;
        boolean isCompareCua = request.getParameter(ActionConstants.RA_COMPARE_CUA) != null && request.getParameter(ActionConstants.RA_COMPARE_CUA).equals("true"); 
        while (aIter.hasNext()) {

            WebCatItemForm itemForm = (WebCatItemForm) aIter.next();
            WebCatItem item = null;

            if ( isCompareCua && cuaItems != null && cuaItems.getItem(itemForm.getItemID()) != null) { // I'm in a refine for compare to similar

                log.debug("UpdateItemsFromPageAction: compareCUA");
                item = cuaItems.getItem(itemForm.getItemID()).getContractItem(itemForm.getContractKey(),
                        itemForm.getContractItemKey());
            }
            else if ( items != null && 
                      items.getListType() == ActionConstants.CV_LISTTYPE_BESTSELLER && 
                      itemForm.getItemIndex() > 0) { // update in catalogentry.inc.jsp
                log.debug("Read in items by itemIndex");
                item = items.getItem(itemForm.getItemIndex());
            }
            else if (itemForm.getItemID() != null && !itemForm.getItemID().equals("") && 
                     itemForm.getTopItemID() != null && !itemForm.getTopItemID().equals("")) { // Solution Configurator-relevant _SUB_ item was requested
                         
                if (theCatalog.getCurrentItem() != null && itemForm.getTopItemID().equals(theCatalog.getCurrentItem().getItemKey().getItemID())) {
                    log.debug("Searched Top item is the current item of the catalog");
                    item = theCatalog.getCurrentItem().getWebCatSubItem(new TechKey(itemForm.getItemID()));
                }
                else {
                    item = items.getSubItem(itemForm.getTopItemID(), new TechKey(itemForm.getItemID()));
                }
            }
            else if (itemForm.getTopItemID() != null && !itemForm.getTopItemID().equals("")) { // Solution Configurator-relevant _MAIN_ item was requested
                 log.debug("Search SC relevant Top Item");
                
                 /* if the item is coming from the basket, it will not be found in the itemlist
                  * or if it is found, it might be the same product, but a different webCatItem 
                  * Thus we check the catalogs currentItem at first
                  */
                  if (theCatalog.getCurrentItem() != null && itemForm.getTopItemID().equals(theCatalog.getCurrentItem().getItemId())) {
                       log.debug("SC relevant item from basket in currentItem found");
                       item = theCatalog.getCurrentItem();
                  }
                  else {
                       log.debug("Current item is not Top Item thus search in list");
                       item = items.getItem(itemForm.getTopItemID());
                  }
            }
            else if (items != null && items.getItem(itemForm.getItemID()) == null) { // it is used to detect whether I'm working with CUA products, or with Items

                if (cuaItems != null && cuaItems.getItem(itemForm.getItemID()) != null) {
                    item = cuaItems.getItem(itemForm.getItemID()).getContractItem(itemForm.getContractKey(),
                            itemForm.getContractItemKey());
                }
                else {
                	//if the item is coming from the CUA list, in items there are only 2 items because we won't to populate entirely.
					if (theCatalog.getCurrentItem() != null && itemForm.getItemID().equals(theCatalog.getCurrentItem().getItemKey().getItemID())) {
						log.debug("Take current item of the catalog");
						item = theCatalog.getCurrentItem();
					}
                }
            }
            else if (items != null) {
                
                // at first check the currentItem. This is necessary, because with ShowProductDetailAction an item from
                // the basket might have been set the current item. Which might have the same itemKey as an item in the
                // ItemList or Area List. But taking one of these tem will be wrong
                if (theCatalog.getCurrentItem() != null && itemForm.getItemID().equals(theCatalog.getCurrentItem().getItemKey().getItemID())) {
                    log.debug("Take current item of the catalog");
                    item = theCatalog.getCurrentItem();
                }
                else {
                    item = items.getItem(itemForm.getItemID());
                    if(!itemForm.getContractKey().equals("") && !itemForm.getContractItemKey().equals("")) {
                        item = item.getContractItem(itemForm.getContractKey(), itemForm.getContractItemKey());
                    }
                }
            }
            else if (null != theCatalog.getCurrentItem()) { // use the current item

                item = theCatalog.getCurrentItem();
            }
            else {
                log.debug("Try to determine item via Search");
                // get itemId 
                String itemId = request.getParameter(ActionConstants.RA_ITEMKEY);

                if (null == itemId && theCatalog != null && theCatalog.getCurrentItem() != null) {
                    itemId = theCatalog.getCurrentItem().getItemID();
                    log.debug("itemId taken from currentItem");
                }

                // get areaId
                String areaId = request.getParameter("productAreaId");
                if (null == areaId) {
                    areaId = request.getParameter(ActionConstants.RA_AREAKEY);
                    log.debug("areaId taken from RA_AREAKEY");
                }
                if (null == areaId && theCatalog != null && theCatalog.getCurrentArea() != null) {
                    areaId = theCatalog.getCurrentArea().getAreaID();
                    log.debug("areaId taken from currentArea");
                }

                // get productId
                String productId = request.getParameter("productId");

                if (log.isDebugEnabled()) {
                    log.debug("Searching with itemId: " + itemId + " areaId: " + areaId);
                }

                if (theCatalog != null && areaId != null && itemId != null) {
                    try {
                        item = theCatalog.getItem(areaId, itemId);
                    }
                    catch (Exception ex) {
						if (log.isDebugEnabled()) {
							log.debug("Error ", ex);
						}
                    }
                }

                if (null == item && theCatalog != null && null != productId && productId.length() > 0) {
                    
                    if (log.isDebugEnabled()) {
                        log.debug("nothing found during search via productId :" + productId);
                    }
                    String productIds[] = { productId };
                    WebCatArea area = theCatalog.getArea(areaId);
                    if (area != null) {
                        String areaType = null;
                        if ( area.isRewardCategory() ) {
                            areaType = WebCatArea.AREATYPE_LOY_REWARD_CATALOG;
                        }
                        else if ( area.isBuyPointsCategory() ) {
                            areaType = WebCatArea.AREATYPE_LOY_BUY_POINTS;
                        }
                        item = theCatalog.getItemList(productIds, areaType).getItem(itemId);
                    }
                    else {
                        item = theCatalog.getItemList(productIds).getItem(itemId);
                    }
                }
            }

            if (item != null) {
                updateSingleItem(request, userData, itemForm, item, theCatalog);
                // check if this item is relevant for Solution Configurator explosion
                updateScItemBasedOnForm(request, userData, theCatalog, itemForm, item, changedScTopItems, nextAction);
            }
            
            log.debug("Finished determine item via Search");
        }

        WebCatItem currItem = theCatalog.getCurrentItem();

        if (currItem != null && currItem.isRelevantForExplosion()) {
            WebCatSubItemList webCatSubItemList = currItem.getWebCatSubItemList();
                
            if (webCatSubItemList != null) {
                while (bIter.hasNext()) {

                    ItemGroupForm itemGroupForm = (ItemGroupForm) bIter.next();
                    String groupId = itemGroupForm.getGroupId();
                    if (log.isDebugEnabled()) {
                        log.debug("GroupId = " + groupId);
                    }

                    if (groupId != null && !groupId.equals("")) {
                        ItemGroup itemGroup = webCatSubItemList.getItemGroup(groupId);
                        if (itemGroup != null) {
                            itemGroup.setDisplayCollapsed(itemGroupForm.isCollapsed());
                            if (log.isDebugEnabled()) {
                                log.debug("Set displayCollapsed to " + itemGroupForm.isCollapsed());
                            }
                        }
                    }
                }
            }
            log.debug("Finished determine group setting");
        }

        // check if a Solution Configurator-relevant item was requested by request 
        // parameters "itemkey" and "topItemkey"
        if( changedScTopItems.size() == 0 ) {
            updateScItemBasedOnKeys(request, userData, theCatalog, changedScTopItems);
        }

        String sortBy = request.getParameter("order");

        if (sortBy != null && !sortBy.equals("")) {
            theSession.setAttribute("sortBy", sortBy);
        }

        // Distinct item selected?
        String itemStr = request.getParameter("item");

        if (itemStr != null) { // item selected? 

            try {
                int selectedLine = Integer.parseInt(itemStr);
                theCatalog.setCurrentItem(items.getItem(selectedLine));
            }
            catch (Exception e) {
                //reset current item
                theCatalog.setCurrentItem(null);
            }
        }

        if (log.isDebugEnabled()) {
            log.debug("Start handling SC relevant items. No of items : " + changedScTopItems.size() + " callsc: " + callSC);
        }

        // check if Solution Configurator-relevant changes were made. 
        // If yes, perform new explosion and reprice the items
        if (changedScTopItems.size() > 0 && callSC) {

            ArrayList itemsForRepricing = new ArrayList();
            Iterator it = changedScTopItems.entrySet().iterator();
            while (it.hasNext()) {

                Map.Entry entry = (Map.Entry) it.next();
                WebCatItem item = (WebCatItem) entry.getValue();

                if (log.isDebugEnabled()) {
                    log.debug("Handle item : " + item.getProduct());
                }

                boolean isExplodedOld = item.isExploded();
                
                //in case of seeSubItemDet; RA_ITEMKEY and RA_TOP_ITEMKEY should be update with the new ItemId and
                //TopItemId generated by the solution configurator.
                
                String productId = null;
                if(nextAction.equals("seeSubItemDet")) {
                    productId = item.getWebCatSubItem(new TechKey(request.getParameter(ActionConstants.RA_ITEMKEY))).getProductID();
                } 
                
                if ((item.isExploded() && nextAction.equals("addToBasket")) 
                    || nextAction.equals("addToBasketDet")) {
                    // if the explosion is called for an add to basket, the sc document holding the
                    // explosion results must be kept, so the explosion tree can be read by the order
                    item.explode(true);
                    log.debug("explode and store");
                }
                else {
                    if (!nextAction.equals("addToBasket")) {
                        log.debug("explode only");
                        item.explode();
                    }
                }

                if(productId != null) { // in this case, we should update the RA_ITEMKEY attribute.
                    WebCatSubItem subItem = item.getWebCatSubItem(productId);
                    if(subItem != null){
                        request.setAttribute(ActionConstants.RA_ITEMKEY, subItem.getTechKey().getIdAsString());
                    }
                }
                if (item.isExploded()) { 
                    if (isExplodedOld) {
                        setContextValue(request, Constants.CV_1STSCCALL, "N");
                    }
                    else {   
                        setContextValue(request, Constants.CV_1STSCCALL, "Y");
                    }
                }

                itemsForRepricing.add(item);
                
                /*
                 * If the exploded item is an item coming from the basket in fact the dirty flag
                 * of the basket should have been set after every explosion, because the SC Wrapper 
                 * also changes the underlying order, but the basket Salesdocument is not 
                 * aware of that and thus would not read the current state from the backend
                 * during the next read. Unfortunately teh basket is not available here, because
                 * the isacore packages are not used DCs of the catalog thus we set a session 
                 * attribute that has to be checked by the basket Actions.
                 */
                if (item.getSCOrderDocumentGuid() != null && item.getSCOrderDocumentGuid().getIdAsString().length() > 0) {
                    log.debug("Item coming from the basket handeld");
                    setBasketDirty = true;   
                }
            }

            repriceItems(mbom, itemsForRepricing);
        }

        log.debug("Finished handling SC relevant items");
        
        String forward = null;

        if (nextAction.equals("compareItems") || 
            nextAction.equals("compareSameWnd") ||
            nextAction.equals("compareItemsWithQuery")) { //we should verify if we have any items to compare

            if (!checkAtLeast2ItemsSelected(request, theCatalog)) {
                request.setAttribute("notEnoughForCompare", "yes");
            }
            else{
            	if ( request.getParameter("groupKey") != null) {
            		request.setAttribute("groupKey", request.getParameter("groupKey") );
				}
            }
        }

        if (request.getParameter(ActionConstants.RA_DISPLAY_SCENARIO) != null && 
            request.getParameter(ActionConstants.RA_DISPLAY_SCENARIO).equals(ActionConstants.RA_CURRENT_QUERY) && 
            nextAction.equals("compareItems")) {

            request.setAttribute(ActionConstants.RA_TO_NEXT, "toItemPage");
            request.setAttribute(ActionConstants.RA_IS_QUERY, ActionConstants.RA_IS_QUERY_VALUE_YES);
            request.setAttribute("popUpCompare", "yes");

            forward = "compareItemsWithQuery";
        }
        else if (nextAction.equals("selectAll") || nextAction.equals("unselectAll")) {
            forward = "bulkSelect";
        }
		else if (nextAction.equals("seeItem")) {
            forward = "seeItem";
			WebCatItem latestWebCatItem = theCatalog.getCurrentItem(); 
			if (latestWebCatItem != null && 
			    !(latestWebCatItem.getSCOrderDocumentGuid() != null && latestWebCatItem.getSCOrderDocumentGuid().getIdAsString().length() > 0 )) {
				forward = "seeItemWithATPInfo";
			}
		}
		else if (nextAction.equals("setPageSize")) {
			   forward = "setPageSize";
			// check isa bom, because the action is also used in the cviews and shared catalogues application
			   if (mbom.checkBOMByType(CatalogBusinessObjectsAware.class)) {
			      CatalogConfiguration config = getCatalogConfiguration(userData);
			      if(config!=null && !config.catUseReloadOneTime() ){
			      	  forward = "setPageSizeLayout";
			      }
			   }
		}      
		else if(request.getParameter(ActionConstants.RA_DISPLAY_SCENARIO) != null && 
				request.getParameter(ActionConstants.RA_DISPLAY_SCENARIO).equals(ActionConstants.RA_CURRENT_QUERY)) {
			request.setAttribute(ActionConstants.RA_TO_NEXT, "toItemPage");
			request.setAttribute(ActionConstants.RA_IS_QUERY, ActionConstants.RA_IS_QUERY_VALUE_YES);
			forward = nextAction;
		}
        else {
            EComBaseAction.IsaActionMapping isaMapping = (EComBaseAction.IsaActionMapping) mapping;
            if (!isaMapping.isForwardDefined(nextAction)) {
                if (log.isDebugEnabled()) {
                    log.debug("no forward defined for 'next' parameter " + nextAction);
                }
                forward = ActionConstants.FW_SUCCESS;
            }
            else {
                forward = nextAction;
            }
        }

        if (setBasketDirty &&
            (!nextAction.equals("addToBasketDet")) ) {
            if (log.isDebugEnabled()) {
                log.debug("Call NotifyBasketB2CExternalChangesAction with " + ActionConstants.RA_BAKET_FOLLOW_UP_ACTION + "= " + forward);
            }
            request.setAttribute(ActionConstants.RA_BAKET_FOLLOW_UP_ACTION, forward);
            forward = "setBasketDirty";
        }
        
        //check if there are error messages for the main item
        //then NO add to Basket action
        if (nextAction.equals("addToBasket") || nextAction.equals("addToBasketDet")) {
        	if (	getNumberOfSelectedItems(request, theCatalog) == 0 && 
        			(request.getParameter(ActionConstants.RA_ITEMKEY)== null || request.getParameter(ActionConstants.RA_ITEMKEY).length()==0)
        		) {
                request.setAttribute("notEnoughForTransfer", "yes");
                nextAction = "notEnoughForTransfer";
            } else {
	            WebCatItem item = theCatalog.getCurrentItem();
	            if(item != null && item.hasMessages()) {
	                forward = "showLastVisited";
	            }
	            else {
	                //find if any item has a message...., in the case of wrong quantity entered in the list view or in the bloc view.
	                Iterator itemsIterator = items.iteratorOnlyPopulated();
	                while(itemsIterator.hasNext() ){
	                    item = (WebCatItem)itemsIterator.next();
	                    if( item != null && item.hasMessages()) {
	                        forward = "showLastVisited";
	                        break;
	                    }
	                }
	            }
            }
        } 

        if (log.isDebugEnabled()) {
            log.debug("Decided to forward to " + forward + "...");
        }
        return mapping.findForward(forward);
    }

    /**
     * Checks, if at least 2 items are selected for comparison.
     * @param request The current request.
     * @param theCatalog The current used catalog.
     * @retrun <code>true</code> if amore than one item is selected
     *        <code>false</code> else.
     */
    private boolean checkAtLeast2ItemsSelected(HttpServletRequest request, WebCatInfo theCatalog) {
        
        log.entering("checkAtLeast2ItemsSelected()");
        
        int noSelected = getNumberOfSelectedItems(request, theCatalog);
        
        log.exiting();
        
        return (noSelected > 1);
    }

    /**
     * Determines how many items are selected in the current displayed list.
     * This also includes the items selected on another page within the paginator.
     * 
     * @param request The current request.
     * @param theCatalog The current used catalog.
     * @return The number of selected items in the current view.
     */
    private int getNumberOfSelectedItems(HttpServletRequest request, WebCatInfo theCatalog) {
    	
    	log.entering("getNumberOfSelectedItems()");
        
        WebCatItemList currentList = this.getScenarioDependentItemList(request, theCatalog);
        int noSelected = 0;
        Iterator itemIterator = currentList.iteratorOnlyPopulated();
        
        while (itemIterator.hasNext()) {
            WebCatItem item = (WebCatItem) itemIterator.next();
        
            if (item.isSelected()) {
                noSelected++;
            }
        
            java.util.Collection subItems = item.getContractItems();
        
            if (subItems != null && subItems.size() != 0) {
                Iterator subItemsIterator = subItems.iterator();
        
                while (subItemsIterator.hasNext()) {
                    WebCatItem subItem = (WebCatItem) subItemsIterator.next();
        
                    if (subItem.isSelected()) {
                        noSelected++;
                    }
                } //end while loop over sub-items
            } //end if item has sub-items
            else { //in case of package component comparison -- some webCatSubItems could be selected 
                if( item.getWebCatSubItemList() != null) {
                    Iterator subItemsIterator = item.getWebCatSubItemList().iterator();
                    while (subItemsIterator.hasNext()) {
                        WebCatItem subItem = (WebCatItem) subItemsIterator.next();
                        if (subItem.isSelected()) {
                            noSelected++;
                        } 
                    }
                }
            }
            
        } //end of while loop over items
        
        if (log.isDebugEnabled()) {
            log.debug(noSelected + " items are selected");
        }
        
        log.exiting();
        
    	return noSelected;
    }

    /**
     * Makes Solution Configurator-relevant updates of a single item through the corresponding itemForm.
     * The form data is already prepared as <code>WebCatItemForm</code> (see there for enhancements).<br /><br />
     * 
     * At first it will be checked, if the current request is relevant for SC (see: {@link #requestIsRelevantForSc(HttpServletRequest)}).<br />
     * Thereafter it will be differentiated between main items and sub items:<br />
     * <ul>
     *   <li>
     *     If the item is main item, this will be added to the
     *     {@link #changedScTopItems} map, if it's relevant for SC explosion.
     *   </li>
     *   <li>
     *     If the item is sub item, at first the top item (main item) will be determined and added to the 
     *     {@link #changedScTopItems} map, if it's relevant for SC explosion. Additionally the 
     *     <i>"Selected for Resolution"</i> flag will be read from the form and it will be compared with 
     *     the last value, which was stored in the form as well. Dependent on it a new value of the
     *     <i>"Selected for Resolution"</i> flag will be calculated.
     *   </li>
     * </ul>
     *  
     * This method is called for every item that is included in the form data.<br />
     * If you want to expand this method, you can use the customer exit method {@link #customerExitUpdateScItemBasedOnForm(HttpServletRequest, UserSessionData, WebCatInfo, WebCatItemForm, WebCatItem)}
     * which will be called at the end in this method.
     * 
     * @param request The request object
     * @param userData Object wrapping the session
     * @param theCatalog Representation of the catalog
     * @param itemForm WebCatItemForm containing the data used to change item
     * @param item WebCatItem which will be changed
     * @param changedScTopItems Map of Solution Configurator relevant top items (WebCatItems)
     */
    protected void updateScItemBasedOnForm(HttpServletRequest request,
            UserSessionData userData,
            WebCatInfo theCatalog,
            WebCatItemForm itemForm,
            WebCatItem item,
            Map changedScTopItems, 
            String nextAction) {

        WebCatItem topItem = null;

        // if this request isn't relevant for SC explostion, don't perform this method
        if (!requestIsRelevantForSc(request) && !nextAction.equals("seeSubItemDet")) {
            return;
        }
        if (item instanceof WebCatSubItem) {
            WebCatSubItem subItem = (WebCatSubItem) item;
            topItem = subItem.getTopParentItem();
            
            boolean isScSelected = itemForm.isScSelected();
            String selForResLast = itemForm.getSelForResLast();
            String selForRes = itemForm.getSelForRes();

            if (selForRes != null && selForRes.equalsIgnoreCase("")) {
                subItem.setAuthor(selForResLast);
            }
            else if (selForRes != null && selForRes.equalsIgnoreCase("true")) {
                if (!isScSelected) {
                    subItem.setAuthor(WebCatSubItemData.SELECTED_FOR_EXP);
                }
                else {
                    subItem.setAuthor(selForResLast);
                }
             }
             else if (selForRes != null && selForRes.equalsIgnoreCase("false")) {
                 if (selForResLast != null && selForResLast.equalsIgnoreCase(WebCatSubItemData.DESELECTED_FOR_EXP)) {
                     subItem.setAuthor(WebCatSubItemData.DESELECTED_FOR_EXP);
                 }
                 else if (selForResLast != null && selForResLast.equalsIgnoreCase(WebCatSubItemData.SELECTED_FOR_EXP)) {
                     subItem.setAuthor(WebCatSubItemData.DESELECTED_FOR_EXP);
                 }
                 else if (selForResLast != null && selForResLast.equalsIgnoreCase(WebCatSubItemData.NOT_SELECTED_FOR_EXP)) {
                     if (isScSelected) {
                         subItem.setAuthor(WebCatSubItemData.DESELECTED_FOR_EXP);
                     }
                     else {
                         subItem.setAuthor(WebCatSubItemData.NOT_SELECTED_FOR_EXP);
                     }
                 }
             }
        }
        else {
            String itemId = request.getParameter(ActionConstants.RA_ITEMKEY);
            // add only the item from request parameter to the changedScTopItems because this is relevant for this action.
            if(itemId != null && itemId.equals(item.getItemId())){ 
               topItem = item;
            }
        }

        if (topItem != null && topItem.isRelevantForExplosion()) {
            changedScTopItems.put(topItem.getItemID(), topItem);
        }

        // call customer exit
        customerExitUpdateScItemBasedOnForm(request, userData, theCatalog, itemForm, item);
    }

    /**
     * Makes Solution Configurator-relevant updates of a single item. This method tries to determine the
     * relevant item on the basis of the request parameters {@link ActionConstants.RA_ITEMKEY} and
     * {@link ActionConstants.RA_TOP_ITEMKEY}.<br />This method checks at first, if the current 
     * request is relevant for SC (see: {@link #requestIsRelevantForSc(HttpServletRequest)}).  
     * Thereafter the needed top item will be read from WebCatItemList and will be added to the
     * {@link #changedScTopItems} map, if the top item is relevant for SC explosion.<br /><br /> 
     * If you want to expand this method, you can use the customer exit method {@link #customerExitUpdateScItemBasedOnForm(HttpServletRequest, UserSessionData, WebCatInfo, WebCatItemForm, WebCatItem)}
     * which will be called at the end in this method.
     * 
     * @param request The request object
     * @param userData Object wrapping the session
     * @param theCatalog Representation of the catalog
     * @param changedScTopItems Map of Solution Configurator relevant top items (WebCatItems)
     */
    protected void updateScItemBasedOnKeys(HttpServletRequest request,
            UserSessionData userData,
            WebCatInfo theCatalog,
            Map changedScTopItems) {

        // if this request isn't relevant for SC explostion, don't perform this method
        if (!requestIsRelevantForSc(request)) {
            return;
        }

        String itemId = request.getParameter(ActionConstants.RA_ITEMKEY);
        String topItemId = (String) request.getAttribute(ActionConstants.RA_TOP_ITEMKEY);
        WebCatItem topItem = null;

        WebCatItemList currentList = theCatalog.getCurrentItemList();
        if (currentList == null) {
            currentList = theCatalog.getCurrentArea().getItemList();
        }
        if (currentList == null) {
            throw new PanicException("determination of top item only possible if WebCatItemList is available");
        }

        if (topItemId != null && !topItemId.equals("")) {
            topItem = currentList.getItem(topItemId);
        }
        // if no value for ActionConstants.RA_TOP_ITEMKEY in request data found,
        // we assume that the _top_ item ID was submitted as ActionConstants.RA_ITEMKEY and should be interpreted in this way
        else if (itemId != null && !itemId.equals("")) {
            topItem = currentList.getItem(itemId);
        }

        if (topItem != null && topItem.isRelevantForExplosion()) {
            changedScTopItems.put(topItem.getItemID(), topItem);
        }
        
        // call customer exit
        customerExitUpdateScItemBasedOnKeys(request, userData, theCatalog);
    }

    /**
     * Update one single item from the data received through the corresponding itemForm. This method is used to update
     * data of a single item with values received from the form that was sent from the browser. The form data is
     * already prepared as <code>WebCatItemForm</code> (see there for enhancements). In default, this sets the item
     * quantity and item selection flag. This method is called for every item that is included in the form data.<br />
     * If you want to expand this method, you can use the customer exit method {@link #customerExitUpdateSingleItem(HttpServletRequest, UserSessionData, WebCatItemForm, WebCatItem)}
     * which will be called at the end in this method.
     *
     * @param request The request object
     * @param userData Object wrapping the session
     * @param itemForm WebCatItemForm containing the data used to change item
     * @param item WebCatItem which will be changed 
     */
    protected void updateSingleItem(HttpServletRequest request,
            UserSessionData userData,
            WebCatItemForm itemForm,
            WebCatItem item,
            WebCatInfo theCatalog) {

        String quan = itemForm.getQuantity();
        item.setQuantity(quan);
        // delete quantity message
        item.clearMessages("b2c.catalog.wrongQuantity");
        if(!WebCatItemKey.checkQuantity(quan, theCatalog)) {
            item.setQuantityWithoutCheck(quan);
            String itemKey = request.getParameter("itemkey"); 
            if( itemKey != null && item.getItemID().equals(itemKey)) {
                //display the quantity message only for the select product, not for all other product which have a wrong quantity.
                Message msg = new Message(Message.WARNING, "b2c.catalog.wrongQuantity", null, "");         
                String messageText = WebUtil.translate(userData.getLocale(), msg.getResourceKey(), null);
                msg.setDescription(messageText);                                                                      
                item.addMessage(msg);
            }
        }
        
        item.setSelected(itemForm.isSelected());
        item.setSelectedContractDuration(item.findContractDuration(itemForm.getSelectedContractDuration()));

        String unit = itemForm.getUnit();

        if (!unit.equalsIgnoreCase(item.getUnit())) {
            if (log.isDebugEnabled()) {
                log.debug("Unit for WebCatItem " + item.getProduct()
                          + " has changed to " + unit + ", recalculate price");
            }
                    
            item.setUnit(unit);
            item.setItemPrice(null);

            // we only want to show prices for Quantity 1
            // so we set the quantity to 1 intermidiately
            String oldQuantity = item.getQuantity();
            item.setQuantity("1");
            item.retrieveItemPrice();
            item.setQuantity(oldQuantity);
        }

        // call customer exit
        customerExitUpdateSingleItem(request, userData, itemForm, item);
    }
    
    /**
     * Reads the request parameter {@link ActionConstants.RA_NEXT} and checks if this value 
     * exists in array {@link #activitiesRelevantForSC}. If yes, this indicates that for this
     * request Solution Configurator-relevant steps should be performed. The mentioned steps
     * will be performed in {@link #updateScItemBasedOnForm(HttpServletRequest, UserSessionData, WebCatInfo, WebCatItemForm, WebCatItem)}
     * and {@link #updateScItemBasedOnKeys(HttpServletRequest, UserSessionData, WebCatInfo)}. 
     *   
     * @param request HttpServletRequest object
     * @return true if the request is relevant for Solution Configurator explosion
     */
    protected boolean requestIsRelevantForSc(HttpServletRequest request) {
        // check if this request is relevant for Solution Configurator explosion
        String nextAction = request.getParameter(ActionConstants.RA_NEXT);
        boolean relevantForSC = false;
        for (int i = 0; i < activitiesRelevantForSC.length; i++) {
            if (activitiesRelevantForSC[i].equalsIgnoreCase(nextAction)) {
                relevantForSC = true;
                break;
            }
        }
        if (log.isDebugEnabled()) {
            log.debug(nextAction + " is relevantForSC = " + relevantForSC);
        }
        return relevantForSC;
    }

    /**
     * Customer exit to update data of one single item. This method will be called at the end of the method
     * {@link #updateSingleItem(HttpServletRequest, UserSessionData, WebCatItemForm, WebCatItem)}.
     *
     * @param request The request object
     * @param userData Object wrapping the session
     * @param itemForm WebCatItemForm containing the data used to change item 
     * @param item WebCatItem which will be changed 
     */
    protected void customerExitUpdateSingleItem(HttpServletRequest request,
            UserSessionData userData,
            WebCatItemForm itemForm,
            WebCatItem item) {

    }

    /**
     * Customer exit to update Solution Configurator-relevant data of one single item. 
     * This method will be called at the end of the method
     * {@link #updateScItemBasedOnForm(HttpServletRequest, UserSessionData, WebCatInfo, WebCatItemForm, WebCatItem)}.
     * 
     * @param request The request object
     * @param userData Object wrapping the session
     * @param theCatalog Representation of the catalog
     * @param itemForm WebCatItemForm containing the data used to change item
     * @param item WebCatItem which will be changed
     */
    protected void customerExitUpdateScItemBasedOnForm(HttpServletRequest request,
            UserSessionData userData,
            WebCatInfo theCatalog,
            WebCatItemForm itemForm,
            WebCatItem item) {

    }

    /**
     * Customer exit to update Solution Configurator-relevant data of one single item. 
     * This method will be called at the end of the method
     * {@link #updateScItemBasedOnKeys(HttpServletRequest, UserSessionData, WebCatInfo)}.
     * 
     * @param request The request object
     * @param userData Object wrapping the session
     * @param theCatalog Representation of the catalog
     */
    protected void customerExitUpdateScItemBasedOnKeys(HttpServletRequest request,
            UserSessionData userData,
            WebCatInfo theCatalog) {

    }

}