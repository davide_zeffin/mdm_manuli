package com.sap.isa.catalog.actions;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.sap.isa.backend.boi.webcatalog.CatalogBusinessObjectsAware;
import com.sap.isa.backend.boi.webcatalog.CatalogConfiguration;
import com.sap.isa.core.UserSessionData;
import com.sap.isa.core.action.ReloadOneTimeAction;

public class CatalogReloadOneTimeAction extends ReloadOneTimeAction {
    
    /**
     * The name of the request parameter which shows if this action was executed already.
     * (rf = Redirect Flush)
     */
    public static final String REQ_REDIRECT_FLUSH = "rf";
    
    /**
     * Returns the catalog configuration aware object used, to get the Catalog configuration
     * 
     * @return the configuration aware object
     */
    public CatalogBusinessObjectsAware getCatalogObjectAwareBom(HttpServletRequest request) {
        
        // get user session data object
        UserSessionData userSessionData =
                UserSessionData.getUserSessionData(request.getSession());
       
        return (CatalogBusinessObjectsAware) userSessionData.getMBOM().getBOMByType(CatalogBusinessObjectsAware.class);
    }
    
    /**
     * Returns the catalog configuration object
     * 
     * @return the catalog configuration object
     */
    public CatalogConfiguration getCatalogConfiguration(HttpServletRequest request) {
        
        CatalogConfiguration catalogConfiguration = null;
        
        CatalogBusinessObjectsAware cataConfBom =  getCatalogObjectAwareBom(request);
        if (cataConfBom != null) {
            catalogConfiguration = cataConfBom.getCatalogConfiguration();
        }
        
        return catalogConfiguration;
    }
    
    /**
     * Implements standard perform method which base on the <code>execute</code> method 
     * of the Struts framework. 
     * 
     * @param mapping
     * @param form
     * @param request
     * @param response
     * @return Action forward.
     * @throws IOException
     * @throws ServletException
     * 
     * @see com.sap.isa.core.BaseAction#doPerform(org.apache.struts.action.ActionMapping, org.apache.struts.action.ActionForm, javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse)
     */
    public ActionForward doPerform(ActionMapping mapping, 
                                   ActionForm form, 
                                   HttpServletRequest request, 
                                   HttpServletResponse response) 
            throws IOException, ServletException {

        final String METHOD_NAME = "doPerform()";
        log.entering(METHOD_NAME);

        String forward = "success";
        
        CatalogConfiguration catConfig = getCatalogConfiguration(request);
        
        String flushAfterRedirect = (String) request.getParameter(REQ_REDIRECT_FLUSH);
        if (flushAfterRedirect == null) {
            flushAfterRedirect = (String) request.getAttribute(REQ_REDIRECT_FLUSH);
        }    
        if ((flushAfterRedirect == null || !flushAfterRedirect.equalsIgnoreCase("y")) && catConfig.catUseReloadOneTime()) {
            forward = "reload";
        }
        
        log.exiting();                          
        return mapping.findForward(forward);
    }

}
