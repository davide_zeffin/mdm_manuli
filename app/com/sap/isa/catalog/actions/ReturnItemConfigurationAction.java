/*****************************************************************************
Copyright (c) 2006, SAP AG, Germany, All rights reserved.
Author: D038380
Created:  02.03.2006

$Revision: #1 $
$Date: 2006/03/02 $
*****************************************************************************/
package com.sap.isa.catalog.actions;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.sap.isa.backend.boi.isacore.BaseConfiguration;
import com.sap.isa.backend.boi.webcatalog.CatalogConfiguration;
import com.sap.isa.businessobject.CommunicationException;
import com.sap.isa.catalog.webcatalog.WebCatInfo;
import com.sap.isa.catalog.webcatalog.WebCatItem;
import com.sap.isa.catalog.webcatalog.WebCatSubItem;
import com.sap.isa.core.UserSessionData;
import com.sap.isa.core.businessobject.management.MetaBusinessObjectManager;
import com.sap.isa.core.util.RequestParser;

/**
 * This Action implements a &quot;return point&quot; for the IPC configuration. 
 * It should be the central return point for the IPC to go back to the catalog
 * area in the ECO B2C application.
 * 
 * @author d038380
 * @version 1.0
 */
public class ReturnItemConfigurationAction extends CatalogBaseAction {

    /**
     * Process the specified HTTP request, and create the corresponding HTTP
     * response (or forward to another web component that will create it).
     * Return an <code>ActionForward</code> instance describing where and how
     * control should be forwarded, or <code>null</code> if the response has
     * already been completed.
     * 
     * @param mapping mapping The ActionMapping used to select this instance
     * @param form The <code>FormBean</code> specified in the Struts configuration file for this action
     * @param request The request object
     * @param response The response object
     * @param userData Object wrapping the session
     * @param theCatalog theCatalog Representation of the catalog
     * @param requestParser Parser to simple retrieve data from the request
     * @param mbom Meta business object manager
     * 
     * @return Forward to another action or JSP page
     * @throws IOException
     * @throws ServletException
     * @throws CommunicationException
     */
    public ActionForward doPerform(ActionMapping mapping,
            ActionForm form,
            HttpServletRequest request,
            HttpServletResponse response,
            UserSessionData userData,
            WebCatInfo theCatalog,
            RequestParser requestParser,
            MetaBusinessObjectManager mbom)
            throws IOException, ServletException, CommunicationException {

        final String METHOD_NAME = "doPerform()";
        log.entering(METHOD_NAME);
        
        String nextForward = (String)request.getParameter(ActionConstants.RA_FORWARD);
        if(nextForward == null) {
            // no "forward" parameter found in request data  
            log.error("Action called without request parameter 'forward'");
            log.exiting();
            return (mapping.findForward(ActionConstants.FW_ERROR));            
        }
        
        if(nextForward.equalsIgnoreCase("exit")) {
            log.exiting();
            return (mapping.findForward("success"));
        }
        else if (nextForward.equalsIgnoreCase("return")) {
            
            CatalogConfiguration config = getCatalogConfiguration(userData);
        
            boolean callSC = false;
        
            // only call solution configurator in Telco scenario
            if (config != null && BaseConfiguration.TELCO_CUSOMER_SELF_SERVICE.equals(config.getScenario())) {
                callSC = true;
            }
        
            if (log.isDebugEnabled()) {
                if (config != null) {
                    log.debug("Config scenario = " + config.getScenario());
                }
                else {
                    log.debug("config is null");
                }
                log.debug("callSC = " + callSC);
            }
            
            if (callSC) {
                WebCatItem item = null;
                WebCatItem configItem = theCatalog.getConfiguredItem();
                if (configItem instanceof WebCatSubItem) {
                    WebCatSubItem subItem = (WebCatSubItem)configItem;
                    item = subItem.getTopParentItem();
                }
                else {
                    item = configItem;  
                }
                if(item.isRelevantForExplosion()) {
                    // start Solution Configurator explosion
                    // (if item isn't relevant for SC explosion, this will be indicated by the explode() method itself)
                    item.explode();
                    repriceItem(mbom, item);
                }
            }
            
            log.exiting();
            return mapping.findForward("success");
        }
        else {
            log.error("Action called with a not allowed request parameter 'forward' - value: " + nextForward);
            log.exiting();
            return (mapping.findForward(ActionConstants.FW_ERROR));
        }
    }
}
