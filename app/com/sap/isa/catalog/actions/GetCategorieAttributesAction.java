package com.sap.isa.catalog.actions;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.sap.isa.businessobject.BusinessObjectException;
import com.sap.isa.businessobject.CommunicationException;
import com.sap.isa.businessobject.webcatalog.CatalogBusinessObjectManager;
import com.sap.isa.catalog.boi.CatalogException;
import com.sap.isa.catalog.boi.IAttribute;
import com.sap.isa.catalog.webcatalog.WebCatArea;
import com.sap.isa.catalog.webcatalog.WebCatAreaAttribute;
import com.sap.isa.catalog.webcatalog.WebCatInfo;
import com.sap.isa.core.UserSessionData;
import com.sap.isa.core.util.Message;

public class GetCategorieAttributesAction extends CatalogBaseAction {

    /**
     * Process the specified HTTP request, and create the corresponding HTTP
     * response (or forward to another web component that will create it).
     * Return an <code>ActionForward</code> instance describing where and how
     * control should be forwarded, or <code>null</code> if the response has
     * already been completed.
     *
     * @param mapping The ActionMapping used to select this instance
     * @param form The optional ActionForm bean for this request (if any)
     * @param request The HTTP request we are processing
     * @param response The HTTP response we are creating
     * @param userData container for session informations
     * @param theCatalog representation of the catalog
     *
     * @exception IOException if an input/output error occurs
     * @exception ServletException if a servlet exception occurs
     * 
     * @return specific action forward corresponding to the struts configuration
     */
    public ActionForward doPerform(ActionMapping mapping,
            ActionForm form,
            HttpServletRequest request,
            HttpServletResponse response,
            UserSessionData userData,
            WebCatInfo theCatalog)
            throws IOException, ServletException, CommunicationException {

        CatalogBusinessObjectManager bom = (CatalogBusinessObjectManager) userData.getBOM(CatalogBusinessObjectManager.CATALOG_BOM);

        if ((request.getAttribute("isQuery") != null)
                && request.getAttribute("isQuery").equals("yes")) {
            return mapping.findForward(ActionConstants.FW_SUCCESS);
        }

        WebCatArea currentArea = bom.getCatalog().getCurrentArea();

        ArrayList attributesList = new ArrayList();

        if ((currentArea != null) && (currentArea.getCategory() != null)) {
            Iterator attributes;

            try {
                attributes = currentArea.getCategory().getAttributes();
            }
            catch (CatalogException e) {
                log.error("catalog.exception.backend", e);

                Message msg = new Message(Message.ERROR,
                        "catalog.exception.usermsg");
                BusinessObjectException boe = new BusinessObjectException("Catalog error",
                        msg);
                request.setAttribute("Exception", boe);

                return mapping.findForward(ActionConstants.FW_ERROR);
            }

            /*      WebCatAreaQuery areaQuery;
             if (bom.getCatalog().getCurrentItemList() != null)
             areaQuery = bom.getCatalog().getCurrentItemList().getAreaQuery() ;
             else areaQuery = null;*/
            while (attributes.hasNext()) {
                IAttribute attribute = (IAttribute) attributes.next();

                /*        if (areaQuery != null) {
                 if (!areaQuery.getPrevAttribs().contains(attribute.getGuid())) {
                 WebCatAreaAttribute areaAttribute = new WebCatAreaAttribute(attribute);
                 attributesList.add(areaAttribute);
                 }
                 } else {*/
                WebCatAreaAttribute areaAttribute = new WebCatAreaAttribute(attribute);
                attributesList.add(areaAttribute);

                /*        } */
            }

            Collections.sort(attributesList, new CompareOperation());
        }

        request.setAttribute("attributesList", attributesList);

        return mapping.findForward(ActionConstants.FW_SUCCESS);
    }

    private class CompareOperation implements Comparator {
        public CompareOperation() {
        }

        public int compare(Object obj1, Object obj2) {
            WebCatAreaAttribute atr1 = (WebCatAreaAttribute) obj1;
            WebCatAreaAttribute atr2 = (WebCatAreaAttribute) obj2;
            Integer int1;
            Integer int2;
            int1 = new Integer(atr1.getOrder());
            int2 = new Integer(atr2.getOrder());

            return int1.compareTo(int2);
        }
    }
}