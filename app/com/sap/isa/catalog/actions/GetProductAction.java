package com.sap.isa.catalog.actions;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.sap.isa.backend.boi.isacore.BaseConfiguration;
import com.sap.isa.backend.boi.webcatalog.CatalogBusinessObjectsAware;
import com.sap.isa.backend.boi.webcatalog.CatalogConfiguration;
import com.sap.isa.businessobject.CommunicationException;
import com.sap.isa.catalog.webcatalog.WebCatInfo;
import com.sap.isa.catalog.webcatalog.WebCatItem;
import com.sap.isa.catalog.webcatalog.WebCatItemList;
import com.sap.isa.catalog.webcatalog.WebCatSubItem;
import com.sap.isa.core.Constants;
import com.sap.isa.core.PanicException;
import com.sap.isa.core.TechKey;
import com.sap.isa.core.UserSessionData;
import com.sap.isa.core.businessobject.management.MetaBusinessObjectManager;
import com.sap.isa.core.util.Message;
import com.sap.isa.core.util.RequestParser;
import com.sap.isa.isacore.action.ActionConstantsBase;
import com.sap.spc.remote.client.object.IPCException;
import com.sap.spc.remote.client.object.IPCItem;
import com.sap.spc.remote.client.util.ext_configuration;
import com.sap.tc.logging.Category;

/**
 * Title:        GetProductAction
 * Description:  Query the catalog about the list of Attributes for the currently
 *               selected item in the catalog. If no item is currently selected,
 *               the current item is the item whose ID is specified in the request
 * Copyright:    Copyright (c) 2001
 * Company:      SAP AG Germany
 * @version 1.0
 */

public class GetProductAction extends CatalogBaseAction {

    /**
     * Process the specified HTTP request, and create the corresponding HTTP
     * response (or forward to another web component that will create it).
     * Return an <code>ActionForward</code> instance describing where and how
     * control should be forwarded, or <code>null</code> if the response has
     * already been completed.
     * 
     * @param mapping mapping The ActionMapping used to select this instance
     * @param form The <code>FormBean</code> specified in the Struts configuration file for this action
     * @param request The request object
     * @param response The response object
     * @param userData Object wrapping the session
     * @param theCatalog theCatalog Representation of the catalog
     * @param requestParser Parser to simple retrieve data from the request
     * @param mbom Meta business object manager
     * 
     * @return Forward to another action or JSP page
     * @throws IOException
     * @throws ServletException
     * @throws CommunicationException
     */
    public ActionForward doPerform(
        ActionMapping mapping,
        ActionForm form,
        HttpServletRequest request,
        HttpServletResponse response,
        UserSessionData userData,
        WebCatInfo theCatalog,
        RequestParser requestParser,
        MetaBusinessObjectManager mbom)
        throws IOException, ServletException, CommunicationException {

        log.entering("doPerform()");

        if (theCatalog == null) {
            log.error("No Catalog Information availabale!");
            return (mapping.findForward(ActionConstants.FW_ERROR));
        }

        WebCatItem neededItem = null;
        boolean isScSubItem = false;
        String basketAnchorKey = null;
        String basketAnchorIsCfg = null;
        boolean callSC = false;
        CatalogConfiguration config = null;
        
		String forward = ActionConstants.FW_SUCCESS;

        // check isa bom, because the action is also used in the cviews and shared catalogues application
        if (mbom.checkBOMByType(CatalogBusinessObjectsAware.class)) {

            config = getCatalogConfiguration(userData);

            // only call solution configurator in Telco scenario
            if (config != null && BaseConfiguration.TELCO_CUSOMER_SELF_SERVICE.equals(config.getScenario())) {
                callSC = true;
            }

            if (log.isDebugEnabled()) {
                if (config != null) {
                    log.debug("Shop scenario = " + config.getScenario());
                }
                else {
                    log.debug("config is null");
                }
                log.debug("callSC = " + callSC);
            }
            
			if (config != null && config.isSuppCatEventCapturing()) {
				forward = ActionConstants.FW_CAPTURE;
            }
        }

        if (config != null && config.showCatATP()) {
            log.debug("showCatATP flag is set to true");
            forward = "successATP";
        }

        //check if item identified in request
        // -----------------------------------------------------
        // get itemId 
        String itemId = null;

        if (request.getAttribute("itemRequest") != null) {
            Object o = request.getAttribute(ActionConstants.RA_SHOW_PROD_DET_ITEM);
            if (o != null) {
                itemId = ((WebCatItem) o).getItemId();
            }
        }
        
        String next = request.getParameter("next");
        
        // if next == addToBasketDet the itemkey could be the key of a CUA Item and this would replace the original Item
        if (null == itemId && !"addToBasketDet".equals(next)) { 
            itemId = request.getParameter(ActionConstants.RA_ITEMKEY);
            
            if (null == itemId) {
                itemId = (String) request.getAttribute(ActionConstants.RA_ITEMKEY);
            } 
        }
        if (null == itemId) {
            itemId = getContextValue(request, ActionConstants.CV_CURRENT_ITEM);
        }
        if (null == itemId) {
            itemId = (String) request.getSession().getAttribute("productId");
        }

        if (log.isDebugEnabled()) {
            log.debug("itemId=" + itemId);
        }

        // get top Item (relevant for Solution Configurator products)
        String topItemId = request.getParameter(ActionConstants.RA_TOP_ITEMKEY);
        if (null == topItemId) {
            topItemId = (String) request.getAttribute(ActionConstants.RA_TOP_ITEMKEY);
        }
        if (null == topItemId) {
            topItemId = getContextValue(request, ActionConstants.CV_TOP_ITEM);
        }

        if (log.isDebugEnabled()) {
            log.debug("topItemId=" + topItemId);
        }

        // get areaId
        String areaId = request.getParameter("productAreaId");
        if (null == areaId) {
            areaId = request.getParameter(ActionConstants.RA_AREAKEY);
        }
        if (null == areaId) {
            areaId = (String) request.getAttribute(ActionConstants.RA_AREAKEY);
        }
        if (null == areaId) {
            areaId = getContextValue(request, ActionConstants.CV_CURRENT_AREA);
        }
        if (null == areaId) {
            areaId = (String) request.getSession().getAttribute("productArea");
        }

        log.debug("areaId=" + areaId);

        if (requestParser.getParameter(ActionConstantsBase.RC_ANCHOR_KEY).isSet()) {
            basketAnchorKey = requestParser.getParameter(ActionConstantsBase.RC_ANCHOR_KEY).getValue().getString();
        }
        else {
            basketAnchorKey = getContextValue(request, Constants.CV_BASKET_ANCHOR_KEY);
        }

        if (log.isDebugEnabled()) {
            log.debug("basketAnchorKey=" + basketAnchorKey);
        }

        // in order to enable to set the anchor on the configuration link
        if (requestParser.getParameter(ActionConstantsBase.RC_ANCHOR_IS_CFG).isSet()) {
            basketAnchorIsCfg = requestParser.getParameter(ActionConstantsBase.RC_ANCHOR_IS_CFG).getValue().getString();
        }
        else {
            basketAnchorIsCfg = getContextValue(request, Constants.CV_ANCHOR_IS_CFG);
        }

        if (log.isDebugEnabled()) {
            log.debug("basketAnchorIsCfg=" + basketAnchorIsCfg);
        }

        // check if a sub-item represented by Solution Configurator was requested
        if (topItemId != null && !topItemId.equals("") && itemId != null && !itemId.equals("")) {

            log.debug("Use topItemId  and itemId to search for the item");

            // if the main item wqs coming from the basket via ShowProductDetailAction it might be,
            // that the sub item can not be found using the standard way, because the main item is not
            // in the item Lists, so we at first check if the curren item conatins the requested sub item
            if (theCatalog.getCurrentItem() != null && topItemId.equals(theCatalog.getCurrentItem().getItemKey().getItemID())) {
                log.debug("Searched Top item is the current item of the catalog");
                neededItem = theCatalog.getCurrentItem().getWebCatSubItem(new TechKey(itemId));
                if (neededItem == null) {
                    itemId = (String) request.getAttribute(ActionConstants.RA_ITEMKEY);
                    neededItem = theCatalog.getCurrentItem().getWebCatSubItem(new TechKey(itemId));
                }
            }
            else {
                WebCatItemList currentList =  this.getScenarioDependentItemList(request, theCatalog); // determineItemList(theCatalog);

                if (currentList == null) {
                    // we're not able to read sub-items information!
                    log.exiting();
                    throw new PanicException("determination of sub-items only possible if WebCatItemList is available");
                }

                // read WebCatSubItem
                neededItem = currentList.getSubItem(topItemId, new TechKey(itemId));
            }

            if (neededItem == null) {
                log.debug("sub item from the sub item list could not be determined");
                log.exiting();
                return (mapping.findForward(ActionConstants.FW_ERROR));
            }

            log.debug("sub-item represented by Solution Configurator was requested");
            isScSubItem = true;
        }
        // else a main item was requested (this could be a main item represented by Solution Configurator as well)
        else if (itemId != null) {

            log.debug("Use itemId to search for the item");

            // at first check the currentItem. This is necessary, because with ShowProductDetailAction an item from
            // the basket might have been set the current item. Which might have the same itemKey as an item in the
            // ItemList or Area List. But taking one of these tem will be wrong
            if (theCatalog.getCurrentItem() != null && itemId.equals(theCatalog.getCurrentItem().getItemKey().getItemID())) {
                log.debug("Take current item of the catalog");
                neededItem = theCatalog.getCurrentItem();
            }
            // maybe we showed the details of a subItem before an there the 'back' button was pressed. Than we must try to 
            // get the main item from the catalogs current item, because otherwise we would not be able to find items again, 
            // coming from the basket (to be changed by the SC), but instead would get a new item with a default explosion
            else if (
                theCatalog.getCurrentItem() != null
                    && theCatalog.getCurrentItem() instanceof WebCatSubItem
                    && itemId.equals(theCatalog.getCurrentItem().getTopParentItem().getItemKey().getItemID())) {
                log.debug("Searched item is top parent item of current catalog item");
                neededItem = theCatalog.getCurrentItem().getTopParentItem();
            }
			// maybe we returned from CUA Ccompare, then take the currrent item
			else if (request.getParameter("compareCUA") != null && 
			         request.getParameter("compareCUA").equals("true") &&
				     theCatalog.getCurrentItem() != null) {
			    log.debug("Returned from compareCUA, take the current catalog item");
			    neededItem = theCatalog.getCurrentItem();
            }
            else {
                log.debug("The searched item must be determined from the item list");

                WebCatItemList currentList =  this.getScenarioDependentItemList(request, theCatalog); // determineItemList(theCatalog);

                if (currentList != null) {
                    log.debug("Search for Catalog Item in itemList");
                    neededItem = currentList.getItem(itemId);
                }

                if (neededItem == null && areaId != null) {
                    log.debug("Retrieve Catalog Item via ItemId and areaId");
                    neededItem = theCatalog.getItem(areaId, itemId);
					ArrayList repriceItems = new ArrayList(1);
					repriceItems.add(neededItem);
					CatalogBaseAction.repriceItems(userData.getMBOM(), repriceItems);
					repriceItems = null;
                }
            }
        }

        if (neededItem == null || neededItem.getCatalogItem() == null) {
            if (theCatalog.getCurrentItem() == null) {
                // no item determinable for detail!
                log.error("Catalog Item not found");
                return (mapping.findForward(ActionConstants.FW_ERROR));
            }
            else {
                log.debug("Use CurentItem as Catalog Item");
                neededItem = theCatalog.getCurrentItem();
            }
        }
        else {
            if (request.getParameter("contractkey") != null
                && request.getParameter("contractitemkey") != null
                && neededItem.getContractItem(request.getParameter("contractkey"), request.getParameter("contractitemkey")) != null) {

                log.debug("Search for Catalog Item in Contrac Items");
                neededItem = neededItem.getContractItem(request.getParameter("contractkey"), request.getParameter("contractitemkey"));
            }
        }

        // check if requested item is Solution Configurator-relevant
        if ((isScSubItem || neededItem.isRelevantForExplosion()) && callSC) {
            log.debug("process SC items");

            WebCatItem scTopItem = neededItem.getTopParentItem();

            if (scTopItem.getSolutionConfigurator() == null) {
                log.debug("explode + reprice Top Item");
                scTopItem.explode();
                repriceItem(mbom, scTopItem);
                setContextValue(request, Constants.CV_1STSCCALL, "Y");
            }
        }

        // change current item
        theCatalog.setCurrentItem(neededItem);
        theCatalog.setLastVisited(WebCatInfo.ITEMDETAILS);
//      String displayScenario = this.getValueFromRequest(request, ActionConstants.RA_DISPLAY_SCENARIO);
//      if(displayScenario != null && !displayScenario.equals("")) {
//      	theCatalog.setLastUsedDetailScenario(displayScenario);
//      }

        Iterator neededItemAtributes = null;
        neededItemAtributes = determineAreaAttributes(neededItem, log);

        request.setAttribute(ActionConstants.RC_WEBCATITEM, neededItem);
        request.setAttribute("itemAttributesIterator", neededItemAtributes);

        // set needed context values
        setContextValue(request, ActionConstants.CV_CURRENT_ITEM, neededItem.getItemID());
        setContextValue(request, ActionConstants.CV_CURRENT_AREA, neededItem.getAreaID());
        setContextValue(request, ActionConstants.CV_TOP_ITEM, neededItem.getTopParentItem().getItemID());

        // anchor to set the focus on the selected component
        if (basketAnchorKey != null) {
            setContextValue(request, Constants.CV_BASKET_ANCHOR_KEY, basketAnchorKey);
            request.setAttribute(ActionConstantsBase.RC_ANCHOR_KEY, basketAnchorKey);
        }

        // in order to enable to set the anchor on the configuration link
        if (basketAnchorIsCfg != null) {
            setContextValue(request, Constants.CV_ANCHOR_IS_CFG, basketAnchorIsCfg);
            request.setAttribute(ActionConstantsBase.RC_ANCHOR_IS_CFG, basketAnchorIsCfg);
        }

        // don't show CUAs if sub-item details should be shown
        if (isScSubItem) {
            forward = "noCua";
        }
        else if ("yes".equals(request.getParameter("isDetailOnly"))) {
            forward = "showDetails";
        }

        // if the item is coming from the basket we must check if it is still valid,
        // because due to using Browser back, the item, or the complete basket might 
        // have gone meanwhile
        if (neededItem.getSCOrderDocumentGuid() != null && neededItem.getSCOrderDocumentGuid().getIdAsString().length() > 0) {
            log.debug("Call CheckB2CCurrentItemValidForBasketAction " + ActionConstants.RA_BAKET_FOLLOW_UP_ACTION + "= " + forward);
            request.setAttribute(ActionConstants.RA_BAKET_FOLLOW_UP_ACTION, forward);
            forward = "checkBasketItem";
        }

        String detailScenario = request.getParameter(ActionConstants.RA_DETAILSCENARIO);
        if (detailScenario == null) {
            detailScenario = getContextValue(request, ActionConstants.RA_DETAILSCENARIO);
        }
        if (detailScenario == null) {
            detailScenario = (String) request.getSession().getAttribute(ActionConstants.CV_DETAILSCENARIO);
            if("ItemFromBasket".equals(detailScenario)){ //make sure we delete only this value from session
            	request.getSession().setAttribute(ActionConstants.CV_DETAILSCENARIO, null); //remove from session
            }
        }
        if (detailScenario != null) {
            request.setAttribute(ActionConstants.CV_DETAILSCENARIO, detailScenario);
        }
        
        log.exiting();
        return mapping.findForward(forward);
    }

    /**
     * Determine the item list
     */
    protected WebCatItemList determineItemList(WebCatInfo theCatalog) {

        log.entering("determineItemList()");

        WebCatItemList currentList = theCatalog.getCurrentItemList();

        if (currentList == null) {
            log.debug("Itemlist from Area");
            currentList = theCatalog.getCurrentArea().getItemList();
        }

        log.exiting();

        return currentList;
    }

    /**
     * Checks configuration of items and assign messages to the item 
     * objects if the configuration isn't complete.
     * 
     * @param itemsForIpcCheck ArrayList of WebCatItems
     */
    protected void checkIpcConfiguration(ArrayList itemsForIpcCheck) {

        for (int i = 0; i < itemsForIpcCheck.size(); i++) {

            WebCatItem item = (WebCatItem) itemsForIpcCheck.get(i);
            // delete old messages related to configuration 
            item.getMessageList().remove(new String[] { "cat.config.missing", "cat.config.incorrect", "cat.config.incomplete", "cat.config.inconsistent" });

            Message msg = null;

            try {
                ext_configuration extConfig = ((IPCItem) item.getConfigItemReference()).getConfig();

                if (extConfig == null) {
                    msg = new Message(Message.ERROR, "cat.config.missing");
                }
                else if (!extConfig.is_complete_p() && !extConfig.is_consistent_p()) {
                    msg = new Message(Message.ERROR, "cat.config.incorrect");
                }
                else if (!extConfig.is_complete_p()) {
                    msg = new Message(Message.ERROR, "cat.config.incomplete");
                }
                else if (!extConfig.is_consistent_p()) {
                    msg = new Message(Message.ERROR, "cat.config.inconsistent");
                }

                if (msg != null) {
                    item.addMessage(msg);
                }
            }
            catch (IPCException e) {
                log.debug("IPCException :" + e);
                log.warn(Category.APPLICATIONS, "isa.log.ipc.ex", e);
            }
        }
    }
}