package com.sap.isa.catalog.actions;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.sap.isa.businessobject.CommunicationException;
import com.sap.isa.catalog.boi.IAttribute;
import com.sap.isa.catalog.boi.IAttributeValue;
import com.sap.isa.catalog.webcatalog.WebCatInfo;
import com.sap.isa.catalog.webcatalog.WebCatItem;
import com.sap.isa.catalog.webcatalog.WebCatItemList;
import com.sap.isa.core.UserSessionData;

/**
 * Title:        CompareItemsAction
 * Description:  Show to user all the attributes of all the selected items
 *               and make a comparation among them
 * Copyright:    Copyright (c) 2001
 * Company:      SAPMarkets Europe GmbH
 * @version 1.0
 */

public class CompareUpSellingItemsAction extends CatalogBaseAction {

    public ActionForward doPerform(
        ActionMapping mapping,
        ActionForm form,
        HttpServletRequest request,
        HttpServletResponse response,
        UserSessionData userData,
        WebCatInfo theCatalog)
        throws IOException, ServletException, CommunicationException {

        log.debug("Entered to CompareItemsAction");

        String price_parameter = servlet.getServletConfig().getInitParameter("prices");
        log.debug("Price configuration is: " + price_parameter);

        if (theCatalog == null) {
            return (mapping.findForward("error"));
        }

        com.sap.isa.businessobject.marketing.CUAList cuaList =
            (com.sap.isa.businessobject.marketing.CUAList) request.getAttribute(com.sap.isa.isacore.action.marketing.ShowCUAAction.RC_CUALIST);

        if (cuaList.size() == 0)
            return (mapping.findForward("noItems"));

        //    WebCatItemList itemList = cuaList.createWebCatItemList(theCatalog,com.sap.isa.businessobject.marketing.CUAProduct.UPSELLING);
        WebCatItemList itemList = theCatalog.getCurrentCUAList();
        for (int l = 0; l < itemList.size(); l++) {
            WebCatItem item = itemList.getItem(l);
            item.refreshCatalogItem();
        }

        itemList.addItem(theCatalog.getCurrentItem(), 0);

        request.setAttribute("itemList", itemList);

        ArrayList validItems = itemList.getItems();

        HashMap atributeNamesList = new HashMap();

        Iterator validItemsIterator = validItems.iterator();

        //creates a list of all the names for the attributes
        //this list is the Union of all parameters' names lists for all the selected items

        while (validItemsIterator.hasNext()) {
            WebCatItem theItem = (WebCatItem) validItemsIterator.next(); //gets the next element in valid items' list
            Iterator theItemAtributes = theItem.getCatalogItemAttributes(); //gets its Attributes' list
            while (theItemAtributes.hasNext()) {
                IAttributeValue atributeValue = (IAttributeValue) theItemAtributes.next();
                IAttribute atributeName = atributeValue.getAttribute(); //gets Attribute
                if (atributeNamesList.get(atributeName.getGuid()) == null) { //if the name is not already in the list
                    atributeNamesList.put(atributeName.getGuid(), atributeName); //adds the new name of atribute
                }
            }
        }

        Iterator atributeNamesIterator = atributeNamesList.values().iterator();

        request.setAttribute("theItems", validItems);
        request.setAttribute("theNames", atributeNamesIterator);

        request.setAttribute(ActionConstants.RA_FORWARD, "CompareUpSelling");

        return (mapping.findForward(ActionConstants.FW_SUCCESS));
    }

}
