package com.sap.isa.catalog.actions;

import java.util.ArrayList;
import java.util.Iterator;

import javax.servlet.http.HttpServletRequest;

import com.sap.isa.catalog.webcatalog.WebCatItem;
import com.sap.isa.catalog.webcatalog.WebCatItemList;
import com.sap.isa.catalog.webcatalog.WebCatItemPage;
import com.sap.isa.core.logging.IsaLocation;

/**
 * This helper class has only one static method that helps actions to determine
 * the list of items to work with.
 */
public abstract class PrepareItemListHelper
{

  /**
   * Determine the list of items to be worked with. This static method is meant
   * to be called from actions to determine the items to be worked with.
   * The method searches first for a single item, then an item page and then
   * an item list in the request. It returns an iterator over the items of the
   * first list found or <code>null</code> if nothing found.
   * This method call requires some action like GetProductsAction or
   * GetItemPageAction to be run before which has put the relevant items
   * in the request. The method can then be called for example within the
   * pricing action to determine the items for which prices should be determined.
   * Note that this action only provides an @see java.util.Iterator over the
   * items, it does not take into account subitems (like contractitems) or check
   * for example for accessories.
   * @param request the http request as used within the action
   * @param log the log where messages should be written to
   * @returns iterator over the items
   */
  public static Iterator getItemListIterator(HttpServletRequest request, IsaLocation log)
  {
    // get iterator of items to be shown on the page, might be itemList or itemPage
    Iterator iterator;
    WebCatItemPage itemPage = null;
    WebCatItemList itemList = null;

    WebCatItem item = (WebCatItem) request.getAttribute(ActionConstants.RA_WEBCATITEM);
    if (item != null)
    {
	  if (log.isDebugEnabled())
      	log.debug("running single item");
      ArrayList itList = new ArrayList(1);
      itList.add(item);
      iterator = itList.iterator();
    }
    else
    {
      itemPage = (WebCatItemPage) request.getAttribute("itemPage");
      if (itemPage != null)
      {
		if (log.isDebugEnabled())
        	log.debug("running item page");
        iterator = itemPage.iterator();
      }
      else
      {
        itemList = (WebCatItemList) request.getAttribute("itemList");
        if (itemList != null)
        {
		  if (log.isDebugEnabled())
          	log.debug("running item list");
          iterator = itemList.iterator();
        }
        else
        {
          return null;
        }
      }
    }
    return iterator;
  }
}
