package com.sap.isa.catalog.actions;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.sap.isa.businessobject.CommunicationException;
import com.sap.isa.catalog.webcatalog.WebCatInfo;
import com.sap.isa.catalog.webcatalog.WebCatItem;
import com.sap.isa.catalog.webcatalog.WebCatItemList;
import com.sap.isa.core.UserSessionData;

public class AccesoriesAction extends CatalogBaseAction {

    public ActionForward doPerform(
        ActionMapping mapping,
        ActionForm form,
        HttpServletRequest request,
        HttpServletResponse response,
        UserSessionData userData,
        WebCatInfo theCatalog)
        throws IOException, ServletException, CommunicationException {

        // if shop is not set up for contracts: skip this action
        log.debug("Start AccesoriesAction");

        if (theCatalog.getCurrentItem() != null) {
            request.setAttribute(ActionConstants.RC_WEBCATITEM, theCatalog.getCurrentItem());
        }
        else {
            String itemKey = request.getParameter(ActionConstants.RA_ITEMKEY);
            WebCatItemList items = theCatalog.getCurrentItemList();
            WebCatItem item = items.getItem(itemKey);
            request.setAttribute(ActionConstants.RC_WEBCATITEM, item);
        }

        theCatalog.setProductDetailListType(WebCatInfo.PROD_DET_LIST_ACCESSORIES);

        return mapping.findForward(ActionConstants.FW_SUCCESS);
    }
}
