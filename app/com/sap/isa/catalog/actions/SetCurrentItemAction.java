package com.sap.isa.catalog.actions;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.sap.isa.businessobject.CommunicationException;
import com.sap.isa.catalog.webcatalog.WebCatInfo;
import com.sap.isa.catalog.webcatalog.WebCatItem;
import com.sap.isa.catalog.webcatalog.WebCatItemList;
import com.sap.isa.core.UserSessionData;

/**
 * Title:        SetCurrentItemAction
 * Description:  Set the catalog's current Item with respect to the <code>itemkey</code> parameter
 * Copyright:    Copyright (c) 2001
 * Company:      SAPMarkets Europe GmbH
 * @version 1.0
 */

public class SetCurrentItemAction extends CatalogBaseAction {

    /**
     * Process the specified HTTP request, and create the corresponding HTTP
     * response (or forward to another web component that will create it).
     * Return an <code>ActionForward</code> instance describing where and how
     * control should be forwarded, or <code>null</code> if the response has
     * already been completed.
     *
     * @param mapping The ActionMapping used to select this instance
     * @param form The optional ActionForm bean for this request (if any)
     * @param request The HTTP request we are processing
     * @param response The HTTP response we are creating
     * @param userData container for session informations
     * @param theCatalog representation of the catalog
     *
     * @exception IOException if an input/output error occurs
     * @exception ServletException if a servlet exception occurs
     * 
     * @return specific action forward corresponding to the struts configuration
     */
    public ActionForward doPerform(ActionMapping mapping,
            ActionForm form,
            HttpServletRequest request,
            HttpServletResponse response,
            UserSessionData userData,
            WebCatInfo theCatalog)
            throws IOException, ServletException, CommunicationException {

        HttpSession theSession = request.getSession();

        log.debug("Entered to SetCurrentItemAction");

        if (theCatalog == null) {
            log.error("No Catalog Information availabale!");
            return (mapping.findForward(ActionConstants.FW_ERROR));
        }

        WebCatItem item = null;
        String theItem = request.getParameter(ActionConstants.RA_ITEMKEY);
        String itemIndex = request.getParameter(ActionConstants.RA_PRODUCT_INDEX);
        int intIndex = -1;
        if (itemIndex != null && itemIndex.length() > 0) {
            intIndex = (new Integer(itemIndex)).intValue();
        }

        WebCatItemList currentList = this.getScenarioDependentItemList(request, theCatalog);
        
        if (theItem != null && !theItem.equals("")) {
            if (currentList == null) {
                currentList = theCatalog.getCurrentArea().getItemList();
            }
            if (currentList != null) {
                item = currentList.getItem(theItem);

                // it's a catalog query
                if (item != null && currentList.getQuery() != null
                        && currentList.getAreaQuery() == null) {
                    item.refreshCatalogItem();
                }
                theCatalog.setCurrentItem(item);
            }
        }
        // read with index because the same item maybe contained in recommendations and special offers
        else if (intIndex >= 0 && currentList != null) {
            item = currentList.getItem(intIndex);
        }
        else {
            item = theCatalog.getCurrentItem();
        }

        String productAreaId = "";
        if (null == item) {
            productAreaId = request.getParameter("productAreaId");
            if (null != productAreaId && productAreaId.length() > 0) {
                item = theCatalog.getItem(productAreaId, theItem);
                theCatalog.setCurrentItem(item);
            }
        }

        String productId = "";
        if (null == item) {
            productId = request.getParameter("productId");
            if (null != productId && productId.length() > 0) {
                String[] productIds = { productId };
                item = theCatalog.getItemList(productIds).getItem(theItem);
                theCatalog.setCurrentItem(item);
            }
        }

        if (item == null) {
            request.setAttribute("productID", productId);
        }
        else {
            request.setAttribute("productID", item.getProductID());
            request.setAttribute(ActionConstants.RC_WEBCATITEM, item);
            request.setAttribute(ActionConstants.RA_WEBCATITEM, item);
            request.setAttribute(ActionConstants.RA_ITEMNAME,
                    item.getDescription());
        }

        // Remove attributes from the session as they are no longer needed
        theSession.removeAttribute("itemId");
        theSession.removeAttribute("index");

        if (item != null) {
            return (mapping.findForward(ActionConstants.FW_SUCCESS));
        }
        else {
            return (mapping.findForward(ActionConstants.FW_ERROR));
        }
    }

}