/*****************************************************************************
    Class:        TrexCRMCatalogException
    Copyright (c) 2006, SAP AG, All rights reserved.
    Author:
    Created:      05.09.2006
    Version:      1.0

    $Revision$
    $Date$
*****************************************************************************/
package com.sap.isa.catalog.trexcrm;

import com.sap.isa.catalog.trexgen.TrexGenCatalogException;

/**
 * Exception class for all exceptions which occur during calling or working
 * with the Trex catalog implementation.
 *
 * @version     1.0
 */
public class TrexCRMCatalogException extends TrexGenCatalogException
{
    /**
     * Default constructor for exceptions.
     *
     * @param  msg  text to be associated with the exception
     */
    public TrexCRMCatalogException(String msg)
    {
        super(msg);
    }

    /**
     * Creates a new exception instance in case anotherException is the root cause .
     * 
     * @param msg a <code>String</code> value
     * @param aCause a <code>Throwable</code> value
     */
    TrexCRMCatalogException(String mgs, Throwable aCause)
    {
        super(mgs, aCause);
    }
}
