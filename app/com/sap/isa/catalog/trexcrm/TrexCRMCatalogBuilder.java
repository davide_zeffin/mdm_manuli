/*****************************************************************************
    Class:        TrexCRMCatalogBuilder
    Copyright (c) 2006, SAP AG, All rights reserved.
    Author:
    Created:      11.10.2006
    Version:      1.0

    $Revision$
    $Date$
*****************************************************************************/
package com.sap.isa.catalog.trexcrm;


// misc imports
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.StringTokenizer;

import com.sap.isa.catalog.CatalogParameters;
import com.sap.isa.catalog.ICatalogMessagesKeys;
import com.sap.isa.catalog.boi.CatalogException;
import com.sap.isa.catalog.boi.IDetail;
import com.sap.isa.catalog.boi.IQueryStatement;
import com.sap.isa.catalog.impl.Catalog;
import com.sap.isa.catalog.impl.CatalogAttribute;
import com.sap.isa.catalog.impl.CatalogAttributeValue;
import com.sap.isa.catalog.impl.CatalogBuildFailedException;
import com.sap.isa.catalog.impl.CatalogBuilder;
import com.sap.isa.catalog.impl.CatalogCategory;
import com.sap.isa.catalog.impl.CatalogCompositeComponent;
import com.sap.isa.catalog.impl.CatalogDetail;
import com.sap.isa.catalog.impl.CatalogItem;
import com.sap.isa.catalog.impl.CatalogQuery;
import com.sap.isa.catalog.impl.CatalogQueryItem;
import com.sap.isa.catalog.impl.CatalogQueryStatement;
import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.core.util.table.ResultData;
import com.sapportals.trex.TrexException;
import com.sapportals.trex.TrexReturn;
import com.sapportals.trex.core.DocAttribute;
import com.sapportals.trex.core.QueryEntry;
import com.sapportals.trex.core.ResultDocument;
import com.sapportals.trex.core.SearchManager;
import com.sapportals.trex.core.SearchResult;

/**
 * This class builds the catalog tree for the Trex catalog implementation.
 * @version     1.0
 */
class TrexCRMCatalogBuilder
    extends CatalogBuilder
    implements ITrexCRMCatalogConstants
{
       // the logging instance
    private static IsaLocation log = IsaLocation.getInstance(TrexCRMCatalogBuilder.class.getName());

    private static IsaLocation runtimeLogger = IsaLocation.getInstance("runtime." + TrexCRMCatalogBuilder.class.getName());

    private final static HashSet theExcludedAttributes;

    private TrexCRMCatalog theTrexCatalog;

    static {
        theExcludedAttributes = new HashSet();
        theExcludedAttributes.add(ATTR_AREA);
        theExcludedAttributes.add(ATTR_AREA_GUID);
        theExcludedAttributes.add(ATTR_AREA_SPEZ_EXIST);
//        theExcludedAttributes.add(ATTR_AREA_TYPE);
        theExcludedAttributes.add(ATTR_CATALOG);
        theExcludedAttributes.add(ATTR_CATALOG_GUID);
        theExcludedAttributes.add(ATTR_OBJECT_GUID);
        theExcludedAttributes.add(ATTR_PARENT_AREA);
        theExcludedAttributes.add(ATTR_PARENT_AREA_GUID);
        theExcludedAttributes.add(ATTR_SUBAREA_EXISTS);
//        theExcludedAttributes.add(ATTR_VIEWS_GUID);
//        theExcludedAttributes.add(ATTR_VIEWS_ID);
    }

    /**
     * Default Constructor for the Trex catalog builder.
     *
     * @param aCatalog  the TrexCatalog which has to be built
     */
    TrexCRMCatalogBuilder(TrexCRMCatalog aCatalog) {
        // store catalog
        theTrexCatalog = aCatalog;
    }

    /**
     * Adds the content of the result set to the given catalog. Through
     * method all categories are added to the catalog.
     *
     * @param results  selected results
     * @param catalog  catalog for which the results were selected
     * @exception TrexCRMCatalogException
     */
    private void addAllCategories(SearchResult results, Catalog catalog) throws TrexCRMCatalogException {
            
        CatalogCategory category;
        CatalogCategory parent;
        ArrayList rootCategories = new ArrayList();
        HashMap categoryMap = new HashMap();
        
        // first instantiate all categories
        for (ResultDocument document = results.getFirstResultDocument();
             document != null; 
             document = results.getNextResultDocument()) {

             // read document attributes
            DocumentAttributes docAttr = new DocumentAttributes(document);     

            // build category if not already existing
            category = (CatalogCategory)catalog.getCategoryInternal(docAttr.getCategoryGuid());
            if (category == null) {
                category = catalog.createCategoryInternal(docAttr.getCategoryGuid());
                category.setNameInternal(docAttr.getNameInternal());
                category.setDescriptionInternal(docAttr.getDescriptionInternal());
                category.setThumbNailInternal(docAttr.getThumbNail());
                catalog.addCategory(category);
            }
            // for performance reasons store the values of these attributes in a temp map
            categoryMap.put(category, docAttr.getAreaValues());
        }
        
        for (ResultDocument document = results.getFirstResultDocument();
             document != null; 
             document = results.getNextResultDocument()) {

             // read document attributes
            DocumentAttributes docAttr = new DocumentAttributes(document);     

            // build category if not already existing
            category = (CatalogCategory)catalog.getCategoryInternal(docAttr.getCategoryGuid());

            String[] values = (String[])categoryMap.get(category);

            // as ALL categories are put under the appropriate parent category/root, the following property is built, too:
            this.setPropertyBuilt(category, CatalogBuilder.PROPERTY_CATEGORY);

            // set parent category (from ATTR_PARENT_AREA_GUID)
            if (!values[0].equals(VALUE_ROOT)) {
                parent = (CatalogCategory)catalog.getCategoryInternal(values[0]);
                if (parent == null) {
                    // catalog index inconsistency checker: parent area does not exist for this area
                    log.warn(
                        ICatalogMessagesKeys.PCAT_WARNING,
                        new Object[] { "parent area for area '" + category.getName() + "' not found --- ignored" },
                        null);
                    // we ignore this category here, because it is NOT accessible via "normal" browsing activity
                    continue;
                }
                if (runtimeLogger.isDebugEnabled()) {
                    runtimeLogger.debug(
                        "      addCategories("
                            + category.getName()
                            + "): setting PROPERTY_CATEGORY - parent is "
                            + parent.getName());
                }
                category.setParent(parent);
                parent.addChildCategoryInternal(category);
            }
            // add also the area specific attributes if it is a root category
            // and the root categories weren't built yet (performance!)
            else if (!isCategoriesBuilt(catalog)) {
                if (runtimeLogger.isDebugEnabled()) {
                    runtimeLogger.debug(
                        "      addCategories(" + category.getName() + "): setting PROPERTY_CATEGORY - is root category");
                }
                rootCategories.add(category);
                // from ATTR_AREA_SPEZ_EXIST
                if (values[2] != null && values[2].equals(VALUE_X)) {
                    addCategoryAttributes(category, values[1]);                                                        
                }
                // the property is already built now!
                this.setPropertyBuilt(category, CatalogBuilder.PROPERTY_ATTRIBUTE);
            }
        }

        // the root categories were also built
        if (!isCategoriesBuilt(catalog)) {
            catalog.setChildCategories(rootCategories);
            this.setPropertyBuilt(catalog, CatalogBuilder.PROPERTY_CATEGORY);
        }
    }
    
    /**  
     * Adds the attributes names to the given component.
     *
     * @param info       info object about the index
     * @param component  component for which the names were selected
     */
    private void addAttributes(TrexCRMCatalogIndexInfo info,
                               CatalogCompositeComponent component)
        throws TrexCRMCatalogException {
        // set name of all attributes
        for (Iterator iter = info.getAttributes().iterator(); iter.hasNext(); )
        {
            String name = (String) iter.next();
            
            if (log.isDebugEnabled()) {
                log.debug("Try to add attribute : " + name);
            }
            
            if (theExcludedAttributes.contains(name))
                continue;
            
            // create new attribute
            CatalogAttribute attribute = component.createAttributeInternal();
                
            // set name of attribute
            attribute.setNameInternal(name);
                
            // set properties of attribute
            Properties props = info.getAttributeProperties(name);
            if (props != null) {
                // set guid of attribute (can't be used since index does not support it)
                // attribute.setGuid(props.getProperty(TrexCRMCatalogIndexInfo.PROP_GUID));
                // set description of attribute
                attribute.setDescriptionInternal(
                    props.getProperty(TrexCRMCatalogIndexInfo.PROP_DESCRIPTION));
                attribute.setTypeInternal(props.getProperty(TrexCRMCatalogIndexInfo.PROP_TYPE));

                // set detail information for attribute
                CatalogDetail detail = attribute.createDetailInternal();
                detail.setNameInternal(TrexCRMCatalogIndexInfo.PROP_ENTRYSTYLE);
                detail.setAsStringInternal(
                    props.getProperty(TrexCRMCatalogIndexInfo.PROP_ENTRYSTYLE));
                detail = attribute.createDetailInternal();
                detail.setNameInternal(TrexCRMCatalogIndexInfo.PROP_POS_NR);
                detail.setAsStringInternal(props.getProperty(
                                               TrexCRMCatalogIndexInfo.PROP_POS_NR));
            }

            // set fixed values for attribute
            List values = info.getAttributeFixedValues(name);
            if (values != null)
                attribute.setFixedValuesInternal(new ArrayList(values));
                
            // the property (details of the attribute) is already built now!
            this.setPropertyBuilt(attribute, CatalogBuilder.PROPERTY_DETAIL);
        }
    }

    /**
     * Adds the content of the result set to the given catalog. Through this
     * method the root categories are added to the catalog.
     *
     * @param results  selected results
     * @param catalog  catalog for which the results were selected
     * @exception TrexCRMCatalogException  
     */
    private void addCategories(SearchResult results, Catalog catalog)
        throws TrexCRMCatalogException {
        CatalogCategory category;
        ArrayList rootCategories = new ArrayList();

        // first instantiate all categories
        for (ResultDocument document = results.getFirstResultDocument();
             document != null; 
             document = results.getNextResultDocument()) {
                 
            // read document attributes
            DocumentAttributes docAttr = new DocumentAttributes(document);     

            // build category
            category = catalog.createCategoryInternal(docAttr.getCategoryGuid());
            category.setNameInternal(docAttr.getNameInternal());
            category.setDescriptionInternal(docAttr.getDescriptionInternal());
            category.setThumbNailInternal(docAttr.getThumbNail());
            catalog.addCategory(category);
            
            // add to root categories
            rootCategories.add(category);

            // add also the area specific attributes (performance!)
            if (docAttr.hasAreaSpecAttr()) {
                addCategoryAttributes(category, docAttr.getAreaName());
            }
        }

        // add the root categories to the catalog
        catalog.setChildCategories(rootCategories);
    }

    /**
     * Adds the content of the result set to the given category. Through this
     * mehtod the children of the given category are set.
     *
     * @param results  selected results
     * @param catalog  category for which the children were selected
     * @exception TreXCRMCatalogException error
     */
    private void addCategories(SearchResult results, CatalogCategory category)
        throws TrexCRMCatalogException
    {
        ArrayList children = new ArrayList();
        CatalogCategory child;

        for (ResultDocument document = results.getFirstResultDocument();
                 document != null; 
                 document = results.getNextResultDocument()) {

            // read document attributes
            DocumentAttributes docAttr = new DocumentAttributes(document);     

            // get child
            child = (CatalogCategory) theTrexCatalog.getCategoryInternal(docAttr.getCategoryGuid());
            
            // build category if not already done
            if (child == null) {
                // only one thread is allowed to modify the catalog instance
                synchronized(theTrexCatalog) {
                    // build category
                    child = theTrexCatalog.createCategoryInternal(docAttr.getCategoryGuid());
                    child.setNameInternal(docAttr.getNameInternal());
                    child.setDescriptionInternal(docAttr.getDescriptionInternal());
                    child.setThumbNailInternal(docAttr.getThumbNail());
                    child.setParent(category);
                    theTrexCatalog.addCategory(child);
                }
            }
            // fill children collection
            children.add(child);

            // add also the area specific attributes (performance!)
            synchronized(child) {
                if (docAttr.hasAreaSpecAttr()) {
                    addCategoryAttributes(child, docAttr.getAreaName());
                }
            }
        }

        // set children categories
        category.setChildCategories(children);
    }

    /**
     * Retrieve the attributes of the category which must have attached an area-
     * specific LOC.
     * The caller of this method is already in a synchronized block.
     */
    private void addCategoryAttributes(CatalogCategory category, String areaName) {

        // maybe the area containing the LOC is deactivated/renamed/deleted in CRM since last replication
        try {
            TrexCRMCatalogIndexInfo index =
                theTrexCatalog.getIndexInfo( INDEX_LEVEL_S,
                                             areaName,
                                             category.getGuid());
            this.addAttributes(index, category);
        }
        catch (TrexCRMCatalogException ex) {
            // ignore those LOC attributes
            String msg = "S-index for area '" + areaName + 
                         "' not available -- ignored";
            log.warn(ICatalogMessagesKeys.PCAT_EXCEPTION, new Object[] { msg }, null);
        } // end of try

        // the property is built now!
        this.setPropertyBuilt(category, CatalogBuilder.PROPERTY_ATTRIBUTE);
    }
    
	/**
	 * Retrieve the attributes of a category which must have attached an area-
	 * specific LOC and assign them to a query.
	 * The caller of this method is already in a synchronized block.
	 */
	private void addCategoryAttributesToQuery(CatalogQuery query, String areaName, String categoryGuid) {
        
		if (log.isDebugEnabled()) {
			log.debug("Add area specific attributes to query for area: " + areaName);
		}

		try {
            // ignore those LOC attributes
			TrexCRMCatalogIndexInfo index =
				theTrexCatalog.getIndexInfo( INDEX_LEVEL_S, areaName, categoryGuid);

			this.addAttributes(index, query);
		}
		catch (TrexCRMCatalogException ex) {
			String msg = "AddCategoryAttributesToQuery S-index for area '" + areaName + "' not available -- ignored";
			log.warn(ICatalogMessagesKeys.PCAT_EXCEPTION, new Object[] { msg }, null);
		} // end of try
	}

    /**
     * Adds the content of the result set to the given category. Through this
     * method the detail information of the given category are set.
     *
     * @param results   selected results
     * @param category  category for which the results were selected
     * @exception TrexCRMCatalogException  error from the Trex catalog implementation
     */
    private void addDetails(SearchResult results, CatalogCategory category)
        throws TrexCRMCatalogException {
            
        HashMap builtDetails = new  HashMap();
        CatalogDetail detail;

        for (ResultDocument document = results.getFirstResultDocument();
                 document != null; 
                 document = results.getNextResultDocument()) {
                     
            builtDetails.clear();

            for (DocAttribute attribute = document.getFirstDocumentAttribute();
                     attribute != null; 
                     attribute = document.getNextDocumentAttribute()) {
                         
                // if multivalued attribute - detail was already created
                detail = (CatalogDetail) builtDetails.get(attribute.getAttributeName());
                if (detail != null) {
                    detail.addAsStringInternal(attribute.getValue1());
                }
                else {
                    detail = category.createDetailInternal();
                    detail.setNameInternal(attribute.getAttributeName());
                    detail.setAsStringInternal(attribute.getValue1());
                    builtDetails.put(attribute.getAttributeName(), detail);
                }
            }
        }
    }

    /**
     * Adds the content of the result set to the given catalog. Through this
     * method the detail information of the given catalog are set.
     *
     * @param results  selected results
     * @param catalog  catalog for which the results were selected
     * @exception TrexCRMCatalogException  error from the Trex catalog implementation
     */
    private void addDetails(ResultData results, Catalog catalog)
        throws TrexCRMCatalogException {
            
        // put the LANGUAGE of the catalog (variant) into the details
        CatalogDetail detail;
        detail = catalog.createDetailInternal();
        detail.setNameInternal("LANGUAGE"); // this is a common attribute
        detail.setAsStringInternal(theTrexCatalog.getLanguage());

        results.beforeFirst();
        while (!results.isLast()) {
            results.next();

            copyAttribute(results, catalog, IDetail.ATTRNAME_PRICE);
            copyAttribute(results, catalog, IDetail.ATTRNAME_QUANTITY);
            copyAttribute(results, catalog, IDetail.ATTRNAME_CURRENCY);
            copyAttribute(results, catalog, IDetail.ATTRNAME_UOM);
            copyAttribute(results, catalog, IDetail.ATTRNAME_SCALETYPE);
            copyAttribute(results, catalog, IDetail.ATTRIBUTE_PRICETYPE);
            copyAttribute(results, catalog, IDetail.ATTRIBUTE_PROMOTION_ATTR);
        }
        return;
    }

    /**
     * Copies an attribute from a result row to a catalog detail. If the attribute
     * is already existing, it is appended to the value array of the catalog detail.
     */
    private void copyAttribute(ResultData results, Catalog catalog, String attributeName) {

        // get the value of the attribute
        String value = results.getString(attributeName);
        if (value == null) {
            return;
        }

        // try to get the detail
        CatalogDetail detail = catalog.getDetailInternal(attributeName);

        // getDetailInternal
        if (detail == null || detail.getName().equals(attributeName) == false) {

            // create the detail and set the value
            detail = catalog.createDetailInternal();
            detail.setNameInternal(attributeName);
            detail.setAsStringInternal(value);
        }
        else {
            // add the value to the detail
            detail.addAsStringInternal(value);
        }

    }
//TODO    
    // This is called by buildAllCategoriesInternal(Catalog) to speed up the initialization phase
    /*
     * @param document contains the ResultDocument
     */
    private void addDetailsToCategory(ResultDocument document, ArrayList attributeNames, CatalogCategory category)
        throws TrexCRMCatalogException {

        HashMap builtDetails = new  HashMap();
        CatalogDetail detail;

        if (document == null) 
            return;
            
        for (DocAttribute attribute = document.getFirstDocumentAttribute();
             attribute != null; 
             attribute = document.getNextDocumentAttribute()) {
        
            String attrName = attribute.getAttributeName().toUpperCase();
            if (attributeNames.contains(attrName)) {

                // if multivalued attribute - detail was already created
                detail = (CatalogDetail) builtDetails.get(attrName);
                if (detail != null) {
                    detail.addAsStringInternal(attribute.getValue1());
                }
                else {
                    detail = category.createDetailInternal();
                    detail.setNameInternal(attrName);
                    detail.setAsStringInternal(attribute.getValue1());
                    builtDetails.put(attrName, detail);
                }

                if (runtimeLogger.isDebugEnabled()) {
                    runtimeLogger.debug(
                        "      addDetailsToCategory("
                            + category.getName()
                            + "): "
                            + detail.getName()
                            + "="
                            + detail.getAsString());
                }
            }
        }
        this.setPropertyBuilt(category, CatalogBuilder.PROPERTY_DETAIL);
    }

    /**
     * Adds the content of the result sets to the given category. Through this
     * method the items for the given category are set.
     *
     * @param resProduct   selected results from the 'P' index
     * @param resCategory  selected results from the 'S' index
     * @param category     category for which the results were selected
     * @exception TrexCRMCatalogException  error from the Trex catalog implementation
     */
    private void addItems(SearchResult resProduct, SearchResult resCategory,
                          CatalogCategory category) 
            throws TrexCRMCatalogException {

        CatalogItem item;
        String objectGuid = null;
        String areaGuid = null;
        HashMap builtItems = new HashMap();
        HashMap attrValues = new HashMap();

        // first analyse the results of the P index
        for (ResultDocument document = resProduct.getFirstResultDocument();
             document != null; 
             document = resProduct.getNextResultDocument()) {
                    
            objectGuid = null;
            areaGuid = null;
            attrValues.clear();
            // build item
            item = category.createItemInternal();

            for (DocAttribute attribute = document.getFirstDocumentAttribute();
                 attribute != null; 
                 attribute = document.getNextDocumentAttribute()) {
                     
                String attrName = attribute.getAttributeName();
                
                // store guid of item
                if (attrName.equalsIgnoreCase(ATTR_OBJECT_GUID))
                    objectGuid = attribute.getValue1();
                
                // store area guid of item
                if (attrName.equalsIgnoreCase(ATTR_AREA_GUID))
                    areaGuid = attribute.getValue1();
                
                // set name of item
                if (attrName.equalsIgnoreCase(ATTR_OBJECT_DESC))
                    item.setNameInternal(attribute.getValue1());
                
                // set all other attributes
                if (!attrName.equalsIgnoreCase(ATTR_AREA_GUID)   &&
                    !attrName.equalsIgnoreCase(ATTR_OBJECT_GUID) &&
                    !attrName.equalsIgnoreCase(ATTR_AREA)        ) { 
                    
                    // convert attribute values of type DOUBLE    
                    String attrType = attribute.getAttributeType();
                    if (attrType != null && attrType.equals("DOUBLE")) {
                        convertAttrDoubleToString(attribute);
                    }
                    
                    // if multivalued attribute - value was already created
                    CatalogAttributeValue attrValue =
                        (CatalogAttributeValue)attrValues.get(attrName);
                    if (attrValue != null) {
                        attrValue.addAsStringInternal(attribute.getValue1());
                    }
                    else {
                        attrValue = item.createAttributeValueInternal(attrName);
                        attrValue.setAsStringInternal(attribute.getValue1());
                        attrValues.put(attrName, attrValue);
                    }
                }
            }
            // set guid of item
            item.setGuidInternal(areaGuid + objectGuid);
            
            // set product guid of item
            item.setProductGuidInternal(objectGuid);
            
            // store item in the hash map
            builtItems.put(objectGuid, new Object[] { item, null });
        }

        processSIndexResults(resCategory, builtItems);
    }

    /**
     * Adds the content of the result sets to the given query.
     *
     * @param results      selected results from the 'A' index
     * @param query        query for which the results were selected
     * @exception TrexCRMCatalogException  error from the Trex catalog implementation
     */
    private void addQuery(SearchResult results, CatalogQuery query)
            throws TrexCRMCatalogException {
                
        for (ResultDocument document = results.getFirstResultDocument();
             document != null; 
             document = results.getNextResultDocument()) {
                
            // first search the guid
            String areaGuid = null;
            boolean found = false;
            
            for (DocAttribute attribute = document.getFirstDocumentAttribute();
                 attribute != null && !found; 
                 attribute = document.getNextDocumentAttribute()) {
                if (attribute.getAttributeName().equalsIgnoreCase(ATTR_AREA_GUID)) {
                    areaGuid = attribute.getValue1();
                    found = true;
                }
            }

            // add category to query
            CatalogCategory category = theTrexCatalog.getCategoryInternal(areaGuid);
            query.addChildCategoryInternal(category);

            // check if details were alread build
            if (this.isDetailsBuilt(category))
                continue;

            // otherwise build the details
            synchronized(category) {
                if (!isDetailsBuilt(category)) {
                    HashMap builtDetails = new HashMap();

                    // iterate over columns
                    for (DocAttribute attribute = document.getFirstDocumentAttribute();
                         attribute != null; 
                         attribute = document.getNextDocumentAttribute()) {
                             
                        if (!attribute.getAttributeName().equalsIgnoreCase(ATTR_AREA_GUID)) {
                            // if multivalued attribute - detail was already created
                            CatalogDetail detail = (CatalogDetail)builtDetails.get(attribute.getAttributeName());
                            if (detail != null) {
                                detail.addAsStringInternal(attribute.getValue1());
                            }
                            else {
                                detail = category.createDetailInternal();
                                detail.setNameInternal(attribute.getAttributeName());
                                detail.setAsStringInternal(attribute.getValue1());
                                builtDetails.put(attribute.getAttributeName(), detail);
                            }
                        }
                    }
                    this.setPropertyBuilt(category, PROPERTY_DETAIL);
                }
            }
        }
    }

    /**
     * Adds the content of the result sets to the given query.
     *
     * @param resProducts  selected results from the 'P' index
     * @param resCategory  selected results from the 'S' index
     * @param query        query for which the results were selected
     * @exception TrexCRMCatalogException  error from the Trex catalog implementation
     */
    private void addQuery(SearchResult resProduct, SearchResult resCategory,
                          CatalogQuery query) 
                 throws TrexCRMCatalogException {
                     
        HashMap builtItems = new HashMap();
        HashMap attrValues = new HashMap();

        CatalogQueryItem item;
        String queryAsString;
        boolean isAdministeredItem = false;
        boolean allowAdministeredProd = true;
        String administeredItemText = "";
        int lengthText = 0;
        StringTokenizer st = null;
        String queryToken = "";
        Properties serverEngineProps = new Properties();

        queryAsString = query.getStatementAsString();
        serverEngineProps = theTrexCatalog.getServerEngine().getProperties();
      
        String allowAdministeredProds = serverEngineProps.getProperty(ITrexCRMCatalogConstants.ALLOW_ADMINISTERED_PROD);
        if (allowAdministeredProds != null && allowAdministeredProds.equalsIgnoreCase("false")) {
            allowAdministeredProd = false;
        }

        // first analyse the results of the P index
        for (ResultDocument document = resProduct.getFirstResultDocument();
             document != null; 
             document = resProduct.getNextResultDocument()) {

            //
            if (queryAsString != null && !queryAsString.equalsIgnoreCase("*")) {
                if (queryAsString.indexOf("*") == 0) {
                    queryAsString = queryAsString.substring(1);
                }
                if (!queryAsString.equalsIgnoreCase("*") && queryAsString.indexOf("*") == queryAsString.length() - 1)
                    queryAsString = queryAsString.substring(0, queryAsString.length() - 1);
                st = new StringTokenizer(queryAsString);
            }

            // first search the guids
            String areaGuid = null;
            String objectGuid= null;
            
            // exit loop if both (areaGuid and objectGuid) were found
            for (DocAttribute attribute = document.getFirstDocumentAttribute();
                 attribute != null && (areaGuid == null || objectGuid == null);
                 attribute = document.getNextDocumentAttribute()) {
                if (attribute.getAttributeName().equalsIgnoreCase(ATTR_AREA_GUID))
                    areaGuid = attribute.getValue1();
                if (attribute.getAttributeName().equalsIgnoreCase(ATTR_OBJECT_GUID))
                    objectGuid = attribute.getValue1();
            }

            // create the item
            item = query.createQueryItemInternal(areaGuid + objectGuid, areaGuid);
            item.setProductGuidInternal(objectGuid);
//          builtItems.put(areaGuid + objectGuid, item);
            builtItems.put(objectGuid, new Object[] { item, null });
            attrValues.clear();

            // now set the attribute values
            for (DocAttribute attribute = document.getFirstDocumentAttribute();
                 attribute != null; 
                 attribute = document.getNextDocumentAttribute()) {
                     
                String attrName = attribute.getAttributeName();
                boolean newAttribute = false;
                    
                // set name of item
                if (attrName.equalsIgnoreCase(ATTR_OBJECT_DESC))
                    item.setNameInternal(attribute.getValue1());
                
                // set all other attributes
                if (!attrName.equalsIgnoreCase(ATTR_AREA_GUID) && 
                    !attrName.equalsIgnoreCase(ATTR_OBJECT_GUID)) { 
                        
                    // convert attribute values of type DOUBLE    
                    String attrType = attribute.getAttributeType();
                    if (attrType != null && attrType.equals("DOUBLE")) {
                        convertAttrDoubleToString(attribute);
                    }

                    // if multivalued attribute - value was already created
                    CatalogAttributeValue attrValue = (CatalogAttributeValue)attrValues.get(attrName);
                    if (attrValue != null) {
                        // add next value to value list of the attribute
                        attrValue.addAsStringInternal(attribute.getValue1());
                    }
                    else {
                        // create new attribute and assign the current value 
                        newAttribute = true;
                        attrValue = item.createAttributeValueInternal();
                        attrValue.setAttribute(attrName);
                        attrValue.setAsStringInternal(attribute.getValue1());
                        attrValues.put(attrName, attrValue);
                        item.addAttributeValueInternal(attrValue);
                    }
                    if (attrName.equalsIgnoreCase(ATTR_TEXT_0003)) {
                        if (allowAdministeredProd)
                            attrValue.setAsStringInternal("TRUE");
                    }
                }
                if (newAttribute == true &&
                    allowAdministeredProd == true && 
                    attrName.equals(ATTR_TEXT_0003) && 
                    st != null) {
                        
                    administeredItemText = attribute.getValue1();
                    if (administeredItemText != null && 
                        administeredItemText.length() > 0) {
                        lengthText = administeredItemText.length();
                        while (st.hasMoreTokens()) {
                            queryToken = st.nextToken();
                            for (int i = 0; i < lengthText; i++) {
                                isAdministeredItem =
                                    queryToken.regionMatches(true, 0, administeredItemText, i, queryToken.length());
                                if (isAdministeredItem)
                                    break;
                            }
                            if (isAdministeredItem)
                                break;
                        } // end of while
                    } // end of if
                } // end of if
            } // end of for
            if (isAdministeredItem) {
                query.swapItemsInternal(item);
            }
        }

        processSIndexResults(resCategory, builtItems);
    }

        /**
         * Adds the query statement for the views to passed SearchManager.
         *
         * @param views array of views id
         * @param searchManager the searchManager to be used
         * @exception TrexCRMCatalogException error from the trex server
         */
    static void xaddViewsToSearch(String[] views, SearchManager searchManager)
        throws TrexCRMCatalogException {
            
        try {
            QueryEntry entry;
            for (int i=0; i < views.length; i++) {
                entry = new QueryEntry();
                entry.setRowType(QueryEntry.ROW_TYPE_ATTRIBUTE);
                entry.setLocation(ATTR_VIEWS_ID);
                entry.setContentType(QueryEntry.CONTENT_TYPE_STRING);
                entry.setValue(views[i], "", QueryEntry.ATTRIBUTE_OPERATOR_EQ);
                entry.setTermWeight(10000);
                searchManager.addQueryEntry(entry);
                if (i > 0) {
                    // add the "OR"
                    entry = new QueryEntry();
                    entry.setRowType(QueryEntry.ROW_TYPE_OPERATOR);
                    entry.setValue(QueryEntry.QUERY_OPERATOR_OR, "", QueryEntry.ATTRIBUTE_OPERATOR_EQ);
                    searchManager.addQueryEntry(entry);
                }
            }
            // add the "AND"
            entry = new QueryEntry();
            entry.setRowType(QueryEntry.ROW_TYPE_OPERATOR);
            entry.setValue(QueryEntry.QUERY_OPERATOR_AND, "", QueryEntry.ATTRIBUTE_OPERATOR_EQ);
            searchManager.addQueryEntry(entry);
        }
        catch (TrexException ex) {
            throw new TrexCRMCatalogException(ex.getLocalizedMessage());
        }
    }
    
    /**
     * Adds the query statement for the views to passed SearchManager.
     *
     * @param views array of views id
     * @param searchManager the searchManager to be used
     * @exception TrexCRMCatalogException error from the trex server
     */
    static void addViewsToSearch(String[] views, SearchManager searchManager, boolean addAndQuery)
    throws TrexCRMCatalogException {
            
    try {
        QueryEntry entry;
        for (int i=0; i < views.length; i++) {
            entry = new QueryEntry();
            entry.setRowType(QueryEntry.ROW_TYPE_ATTRIBUTE);
            entry.setLocation(ATTR_VIEWS_ID);
            entry.setContentType(QueryEntry.CONTENT_TYPE_STRING);
            entry.setValue(views[i], "", QueryEntry.ATTRIBUTE_OPERATOR_EQ);
            entry.setTermWeight(10000);
            searchManager.addQueryEntry(entry);
            if (i > 0) {
                // add the "OR"
                entry = new QueryEntry();
                entry.setRowType(QueryEntry.ROW_TYPE_OPERATOR);
                entry.setValue(QueryEntry.QUERY_OPERATOR_OR, "", QueryEntry.ATTRIBUTE_OPERATOR_EQ);
                searchManager.addQueryEntry(entry);
            }
        }
        
        if (addAndQuery) {
            // add the "AND"
            entry = new QueryEntry();
            entry.setRowType(QueryEntry.ROW_TYPE_OPERATOR);
            entry.setValue(QueryEntry.QUERY_OPERATOR_AND, "", QueryEntry.ATTRIBUTE_OPERATOR_EQ);
            searchManager.addQueryEntry(entry);
        } 

    }
    catch (TrexException ex) {
        throw new TrexCRMCatalogException(ex.getLocalizedMessage());
    }
}
    
    /**
     * Updates the S-Index attributes to an existing query.
     *
     * @param resProducts  selected results from the 'P' index
     * @param resCategory  selected results from the 'S' index
     * @param query        query for which the results were selected
     * @exception TrexCRMCatalogException  error from the Trex implementation
     */
    private void appendSAttributes(SearchResult resProduct, SearchResult resCategory, CatalogQuery query)
        throws TrexCRMCatalogException {

        // rebuild hash map of available items
        HashMap builtItems = new HashMap();
        for (Iterator it = query.getItemsInternal(); it.hasNext();) {
            CatalogQueryItem item = (CatalogQueryItem) it.next();
            builtItems.put(item.getGuid(), item);
        }

        // append S-Index
        processSIndexResults(resCategory, builtItems);

        return;
    }

    /**
     * Builds all attributes from all documents in A index for a catalog
     *
     * @param aCatalog for which all categories are build
     */
    protected void buildAllCategoriesInternal(Catalog aCatalog)
        throws CatalogBuildFailedException {
            
        if (runtimeLogger.isDebugEnabled()) {
            runtimeLogger.debug("begin buildAllCategoriesInternal("
                    + aCatalog.getName()
                    + "): get ALL attributes from ALL documents in AIndex");
        }
        
        try {
            TrexCRMCatalogIndexInfo index =
                theTrexCatalog.getIndexInfo(INDEX_LEVEL_A, "", "");
            SearchManager searchManager = index.getSearchManager(theTrexCatalog.getLanguage());
                
            // set interval
            searchManager.setResultFromTo(INTERVAL_BEGIN, CatalogParameters.trexUpperLimit);
                
            // add return attributes
            Iterator iter = index.getAttributes().iterator();
            while (iter.hasNext()) {
                searchManager.addRequestedAttribute((String) iter.next());
            }
            // searchManager.addRequestedAttribute(ATTR_AREA);
            // searchManager.addRequestedAttribute(ATTR_AREA_GUID);
            // searchManager.addRequestedAttribute(ATTR_AREA_SPEZ_EXIST);
            // searchManager.addRequestedAttribute(ATTR_PARENT_AREA_GUID);
            // searchManager.addRequestedAttribute(ATTR_DESCRIPTION);
            // if(index.hasAttribute(ATTR_DOC_PC_CRM_THUMB)) // not always available
                // searchManager.addRequestedAttribute(ATTR_DOC_PC_CRM_THUMB);
            // if(index.hasAttribute(ATTR_TEXT_0001)) // not always available
                // searchManager.addRequestedAttribute(ATTR_TEXT_0001);
            
            // set sort order 
            searchManager.addSortAttribute(ATTR_POS_NR, true);

            // set query condition
            String[] views = theTrexCatalog.getViews();
            if (views != null && views.length > 0) {
                addViewsToSearch(views, searchManager, false);
            }
            else {
                QueryEntry entry = new QueryEntry();
                entry.setRowType(QueryEntry.ROW_TYPE_ATTRIBUTE);
                entry.setLocation(ATTR_AREA_GUID);
                entry.setContentType(QueryEntry.CONTENT_TYPE_STRING);
                entry.setValue(VALUE_ROOT, "", QueryEntry.ATTRIBUTE_OPERATOR_NE);
                entry.setTermWeight(10000);
                searchManager.addQueryEntry(entry);
            }
            
            // execute query
            SearchResult results = searchManager.search();
            
            if (results.getLastError().getCode() != TrexReturn.NO_ERROR) {
                String msg = results.getLastError().getCode() + ": " +
                    results.getLastError().getMsg();
                throw new CatalogBuildFailedException(msg);
            }
            else if(log.isInfoEnabled()) {
                log.info("Number of Hits: " + results.size());
            }

            // analyse results
            this.addAllCategories(results, theTrexCatalog);

            // determine the names of the attributes used for category's Details
            ArrayList attributeNames = new ArrayList();
            iter = index.getAttributes().iterator();
            while (iter.hasNext()) {
                String name = (String) iter.next();
                if ((!theExcludedAttributes.contains(name)
                    && !name.equals(ATTR_DESCRIPTION)
                    && !name.equals(ATTR_DOC_PC_CRM_THUMB))
                    || name.equals(ATTR_AREA))
                    attributeNames.add(name);
            }
            // insert the details to the categories
            for (ResultDocument document = results.getFirstResultDocument();
                 document != null; 
                 document = results.getNextResultDocument()) {

                // read document attributes
                DocumentAttributes docAttr = new DocumentAttributes(document);     

                // build category if not already existing
                CatalogCategory category = (CatalogCategory) aCatalog.getCategoryInternal(docAttr.getCategoryGuid());
                addDetailsToCategory(document, attributeNames, category);
            }
        }
        catch(TrexException ex) {
            throw new CatalogBuildFailedException(ex.getLocalizedMessage());
        }
        catch(TrexCRMCatalogException ex) {
            throw new CatalogBuildFailedException(ex.getLocalizedMessage());
        }
        if (runtimeLogger.isDebugEnabled()) {
            runtimeLogger.debug("end of buildAllCategoriesInternal");
        }
    }

    /**
     * Build attributes for a catalog
     *
     * @param catalog
     */
    protected void buildAttributesInternal(Catalog catalog)
        throws CatalogBuildFailedException {
        try {
            TrexCRMCatalogIndexInfo index =
                theTrexCatalog.getIndexInfo(INDEX_LEVEL_P, "", theTrexCatalog.getName());
            this.addAttributes(index, theTrexCatalog);
        }
        catch(Exception ex) {
            throw new CatalogBuildFailedException(ex.getLocalizedMessage());
        }
    }

    /**
     * Build attributes for a catagory
     *
     * @param catagory
     */
    protected void buildAttributesInternal(CatalogCategory category)
        throws CatalogBuildFailedException {
        if (runtimeLogger.isDebugEnabled()) {
            runtimeLogger.debug("begin buildAttributesInternal(" + category.getName() + ")");
        }
        try {
            TrexCRMCatalogIndexInfo index =
                theTrexCatalog.getIndexInfo(INDEX_LEVEL_A, "", "");
            SearchManager searchManager = index.getSearchManager(theTrexCatalog.getLanguage());
                
            // set interval
            searchManager.setResultFromTo(INTERVAL_BEGIN, CatalogParameters.trexUpperLimit);
                
            // set return properties
            searchManager.addRequestedAttribute(ATTR_AREA_SPEZ_EXIST);
            searchManager.addRequestedAttribute(ATTR_AREA);
            
            // adding last timestamp 
            searchManager.addRequestedAttribute(ATTR_TIMESTAMP);
                
            // set query condition
            QueryEntry entry = new QueryEntry();
            entry.setRowType(QueryEntry.ROW_TYPE_ATTRIBUTE);
            entry.setLocation(ATTR_AREA_GUID);
            entry.setContentType(QueryEntry.CONTENT_TYPE_STRING);
            entry.setValue(category.getGuid(), "", QueryEntry.ATTRIBUTE_OPERATOR_EQ);
            entry.setTermWeight(10000);
            searchManager.addQueryEntry(entry);

            // set condition for the views
            String[] views = theTrexCatalog.getViews();
            if (views != null && views.length > 0)
                addViewsToSearch(views, searchManager, true);

            // execute query
            SearchResult results = searchManager.search();
            if (results.getLastError().getCode() != TrexReturn.NO_ERROR) {
                String msg = results.getLastError().getCode() + ": " +
                    results.getLastError().getMsg();
                throw new CatalogBuildFailedException(msg);
            } else if(log.isInfoEnabled()) {
                log.info("Number of Hits: " + results.size());
            }

            // now get 'S' index
            String areaName = null;
            boolean indexExists = false;
            
            // take first result document
            ResultDocument firstDocument = results.getFirstResultDocument();
            // read document attributes
            DocumentAttributes docAttr = new DocumentAttributes(firstDocument);     
            if (docAttr.hasTimestamp()) {
                theTrexCatalog.setReplTimeStamp(docAttr.getTimestamp());
            }
            if (docAttr.hasAreaSpecAttr()) {
                addCategoryAttributes(category, docAttr.getAreaName());
            }
        }
        catch(TrexException ex)
        {
            throw new CatalogBuildFailedException(ex.getLocalizedMessage());
        }
        catch(TrexCRMCatalogException ex)
        {
            throw new CatalogBuildFailedException(ex.getLocalizedMessage());
        }
    }

    /**
     * Builds the attributes from the root categories in A index for a catalog
     *
     * @param aCatalog for which all categories are build
     */
    protected void buildCategoriesInternal(Catalog aCatalog)
        throws CatalogBuildFailedException {
            
        if (runtimeLogger.isDebugEnabled()) {
            runtimeLogger.debug("begin buildCategoriesInternal(" 
                                + aCatalog.getName() 
                                + "): get root categories...");
        }
        try {
            TrexCRMCatalogIndexInfo index =
                theTrexCatalog.getIndexInfo(INDEX_LEVEL_A, "", "");
                
            SearchManager searchManager = index.getSearchManager(theTrexCatalog.getLanguage());
                
            // set interval
            searchManager.setResultFromTo(INTERVAL_BEGIN, CatalogParameters.trexUpperLimit);
                
            // add return attributes
            Iterator iter = index.getAttributes().iterator();
            while (iter.hasNext()) {
                searchManager.addRequestedAttribute((String) iter.next());
            }
            // searchManager.addRequestedAttribute(ATTR_AREA);
            // searchManager.addRequestedAttribute(ATTR_AREA_GUID);
            // searchManager.addRequestedAttribute(ATTR_AREA_SPEZ_EXIST);
            // searchManager.addRequestedAttribute(ATTR_DESCRIPTION);
            // if(index.hasAttribute(ATTR_DOC_PC_CRM_THUMB)) // not always available
                // searchManager.addRequestedAttribute(ATTR_DOC_PC_CRM_THUMB);
            // if(index.hasAttribute(ATTR_TEXT_0001)) // not always available
                // searchManager.addRequestedAttribute(ATTR_TEXT_0001);
                
            // set sort order 
            searchManager.addSortAttribute(ATTR_POS_NR, true);
                
            // set query condition
            QueryEntry entry = new QueryEntry();
            entry.setRowType(QueryEntry.ROW_TYPE_ATTRIBUTE);
            entry.setLocation(ATTR_PARENT_AREA_GUID);
            entry.setContentType(QueryEntry.CONTENT_TYPE_STRING);
            entry.setValue(VALUE_ROOT, "", QueryEntry.ATTRIBUTE_OPERATOR_EQ);
            entry.setTermWeight(10000);
            searchManager.addQueryEntry(entry);
            // restrict to not include the $ROOT area
            entry = new QueryEntry();
            entry.setRowType(QueryEntry.ROW_TYPE_ATTRIBUTE);
            entry.setLocation(ATTR_AREA_GUID);
            entry.setContentType(QueryEntry.CONTENT_TYPE_STRING);
            entry.setValue(VALUE_ROOT, "", QueryEntry.ATTRIBUTE_OPERATOR_NE);
            entry.setTermWeight(10000);
            searchManager.addQueryEntry(entry);
            // AND
            entry = new QueryEntry();
            entry.setRowType(QueryEntry.ROW_TYPE_OPERATOR);
            entry.setValue(QueryEntry.QUERY_OPERATOR_AND, "", QueryEntry.ATTRIBUTE_OPERATOR_EQ);
            searchManager.addQueryEntry(entry);
                
            // set condition for the views
            String[] views = theTrexCatalog.getViews();
            if (views != null && views.length > 0) {
                addViewsToSearch(views, searchManager, true);
            }
                
            // execute query
            SearchResult results = searchManager.search();
            if (results.getLastError().getCode() != TrexReturn.NO_ERROR) {
                String msg = results.getLastError().getCode() + ": " +
                    results.getLastError().getMsg();
                throw new CatalogBuildFailedException(msg);
            }
            else if (log.isInfoEnabled()) {
                log.info("Number of Hits: " + results.size());
            }

            // analyse results
            this.addCategories(results, theTrexCatalog);

        }
        catch(TrexException ex) {
            throw new CatalogBuildFailedException(ex.getLocalizedMessage());
        }
        catch(TrexCRMCatalogException ex) {
            throw new CatalogBuildFailedException(ex.getLocalizedMessage());
        }
        
        if (runtimeLogger.isDebugEnabled()) {
            runtimeLogger.debug("end   buildCategoriesInternal");
        }
    }

    /**
     * Builds the attributes for a concrete category in A index
     *
     * @param category for which the attributes are build
     */
    protected void buildCategoriesInternal(CatalogCategory category)
        throws CatalogBuildFailedException {
            
        if (runtimeLogger.isDebugEnabled()) {
            runtimeLogger.debug("begin buildCategoriesInternal(" + category.getName() + ")");
        }
        
        try {
            TrexCRMCatalogIndexInfo index  =
                theTrexCatalog.getIndexInfo(INDEX_LEVEL_A, "", "");
            String language = theTrexCatalog.getLanguage();   
            SearchManager searchManager = index.getSearchManager("DE");
                
            // set interval
            searchManager.setResultFromTo(INTERVAL_BEGIN, CatalogParameters.trexUpperLimit);
                
            // add return attributes
            Iterator iter = index.getAttributes().iterator();
            while (iter.hasNext()) {
                searchManager.addRequestedAttribute((String) iter.next());
            }
            // searchManager.addRequestedAttribute(ATTR_AREA);
            // searchManager.addRequestedAttribute(ATTR_AREA_GUID);
            // searchManager.addRequestedAttribute(ATTR_AREA_SPEZ_EXIST);
            // searchManager.addRequestedAttribute(ATTR_DESCRIPTION);
            // if(index.hasAttribute(ATTR_DOC_PC_CRM_THUMB)) // not always available
                // searchManager.addRequestedAttribute(ATTR_DOC_PC_CRM_THUMB);
            // if(index.hasAttribute(ATTR_TEXT_0001)) // not always available
                // searchManager.addRequestedAttribute(ATTR_TEXT_0001);
                
            // set sort order 
            searchManager.addSortAttribute(ATTR_POS_NR,true);
                
            // set query condition
            QueryEntry entry = new QueryEntry();
            entry.setRowType(QueryEntry.ROW_TYPE_ATTRIBUTE);
            entry.setLocation(ATTR_PARENT_AREA_GUID);
            entry.setContentType(QueryEntry.CONTENT_TYPE_STRING);
            entry.setValue(category.getGuid(), "", QueryEntry.ATTRIBUTE_OPERATOR_EQ);
            entry.setTermWeight(10000);
            searchManager.addQueryEntry(entry);

            // set condition for the views
            String[] views = theTrexCatalog.getViews();
            if (views != null && views.length > 0)
                addViewsToSearch(views, searchManager, true);

            // execute query
            SearchResult results = searchManager.search();
             if (results.getLastError().getCode() != TrexReturn.NO_ERROR) {
                String msg = results.getLastError().getCode() + ": " +
                    results.getLastError().getMsg();
                throw new CatalogBuildFailedException(msg);
            }
            else if (log.isInfoEnabled()) {
                log.info("Number of Hits: " + results.size());
            }
                
            // analyse results
            this.addCategories(results, category);
        }
        catch (TrexException ex) {
            throw new CatalogBuildFailedException(ex.getLocalizedMessage());
        }
        catch (TrexCRMCatalogException ex) {
            throw new CatalogBuildFailedException(ex.getLocalizedMessage());
        }
        
        if (runtimeLogger.isDebugEnabled()) {
            runtimeLogger.debug("end   buildCategoriesInternal()");
        }
    }

    /**
     * Builds the attributes for a concrete category in A index
     *
     * @param category for which the attributes are build
     */
    protected void buildDetailsInternal(Catalog aCatalog)
        throws CatalogBuildFailedException {
            
        if (runtimeLogger.isDebugEnabled()) {
            runtimeLogger.debug("begin buildDetailsInternal(" + aCatalog.getName() + ")");
        }
        
        try {
            TrexCRMCatalogServerEngine catalogServerEngine =
                (TrexCRMCatalogServerEngine)theTrexCatalog.getServerEngine();

            // currently this are only the pricing infos
            ResultData results = TrexCRMRfcMethods.getPricingMetaInfo(
                catalogServerEngine.getDefaultJCoConnection(),
                theTrexCatalog.getName(),
                theTrexCatalog.getVariant());

            // analyse results
            this.addDetails(results, theTrexCatalog);
        }
        catch(Exception ex) {
            throw new CatalogBuildFailedException(ex.getLocalizedMessage());
        }
        
        if (runtimeLogger.isDebugEnabled()) {
            runtimeLogger.debug("end   buildDetailsInternal()");
        }
    }

    protected void buildDetailsInternal(CatalogAttribute aAttribute)
        throws CatalogBuildFailedException {
            // are builded in the same method as the attributes
    }

    protected void buildDetailsInternal(CatalogAttributeValue aValue)
        throws CatalogBuildFailedException {
            // no details available
    }

    protected void buildDetailsInternal(CatalogCategory category)
        throws CatalogBuildFailedException {
            
        if (runtimeLogger.isDebugEnabled()) {
            runtimeLogger.debug("begin buildDetailsInternal(" + category.getName() + ")");
        }
        try {
            TrexCRMCatalogIndexInfo index =
                theTrexCatalog.getIndexInfo(INDEX_LEVEL_A, "", "");
                
            SearchManager searchManager = index.getSearchManager(theTrexCatalog.getLanguage());
                
            // set interval
            searchManager.setResultFromTo(INTERVAL_BEGIN, CatalogParameters.trexUpperLimit);
                
            // add return attributes
            for (Iterator it = index.getAttributes().iterator(); it.hasNext(); ) {
                String name = (String)it.next();
                if ((!theExcludedAttributes.contains(name) &&
                     !name.equalsIgnoreCase(ATTR_TEXT_0001) &&
                     !name.equalsIgnoreCase(ATTR_DESCRIPTION) &&
                     !name.equalsIgnoreCase(ATTR_DOC_PC_CRM_THUMB)) ||
                    (name.equalsIgnoreCase(ATTR_AREA)))
                    searchManager.addRequestedAttribute(name);
            }
                
            // set sort order 
            searchManager.addSortAttribute(ATTR_POS_NR, true);
                
            // set query condition
            QueryEntry entry = new QueryEntry();
            entry.setRowType(QueryEntry.ROW_TYPE_ATTRIBUTE);
            entry.setLocation(ATTR_AREA_GUID);
            entry.setContentType(QueryEntry.CONTENT_TYPE_STRING);
            entry.setValue(category.getGuid(), "", QueryEntry.ATTRIBUTE_OPERATOR_EQ);
            entry.setTermWeight(10000);
            searchManager.addQueryEntry(entry);

            // set condition for the views
            String[] views = theTrexCatalog.getViews();
            if (views != null && views.length > 0) {
                addViewsToSearch(views, searchManager, true);
            }    

            // execute query
            SearchResult results = searchManager.search();
            
            if (results.getLastError().getCode() != TrexReturn.NO_ERROR) {
                String msg = results.getLastError().getCode() + ": " +
                    results.getLastError().getMsg();
                throw new CatalogBuildFailedException(msg);
            }
            else if (log.isInfoEnabled()) {
                log.info("Number of Hits: " + results.size());
            }

            // analyse results
            this.addDetails(results, category);
        }
        catch(TrexException ex) {
            throw new CatalogBuildFailedException(ex.getLocalizedMessage());
        }
        catch(TrexCRMCatalogException ex) {
            throw new CatalogBuildFailedException(ex.getLocalizedMessage());
        }
        
        if (runtimeLogger.isDebugEnabled()) {
            runtimeLogger.debug("begin buildDetailsInternal()");
        }
    }

    protected void buildDetailsInternal(CatalogDetail aDetail)
        throws CatalogBuildFailedException {
        // no details available
    }

    protected void buildDetailsInternal(CatalogItem aItem)
        throws CatalogBuildFailedException {
        // no details available
    }

    protected void buildItemsInternal(CatalogCategory category)
        throws CatalogBuildFailedException
    {
        if (runtimeLogger.isDebugEnabled()) {
            runtimeLogger.debug("begin buildItemsInternal(" + category.getName() + ")");
        }

        // first build all attributes if not already done
        if (!isAttributesBuilt(theTrexCatalog))
            this.buildAttributes(theTrexCatalog);

        try {
            SearchResult resProduct = null;
            SearchResult resCategory = null;

            TrexCRMCatalogIndexInfo index  =
                theTrexCatalog.getIndexInfo(INDEX_LEVEL_P, "", theTrexCatalog.getName());
                
            SearchManager searchManager = index.getSearchManager(theTrexCatalog.getLanguage());
               
            // set interval
            searchManager.setResultFromTo(INTERVAL_BEGIN, CatalogParameters.trexUpperLimit);
                
            // set return attributes
            for (Iterator it = index.getAttributes().iterator(); it.hasNext(); ) {
                String name = (String)it.next();
                if(!theExcludedAttributes.contains(name))
                    searchManager.addRequestedAttribute(name);
            }
            searchManager.addRequestedAttribute(ATTR_AREA);
            searchManager.addRequestedAttribute(ATTR_AREA_GUID);
            searchManager.addRequestedAttribute(ATTR_OBJECT_GUID);
                
            // set sort order 
            searchManager.addSortAttribute(ATTR_POS_NR, true);

            // set query condition
            QueryEntry entry = new QueryEntry();
            entry.setRowType(QueryEntry.ROW_TYPE_ATTRIBUTE);
            entry.setLocation(ATTR_AREA_GUID);
            entry.setContentType(QueryEntry.CONTENT_TYPE_STRING);
            entry.setValue(category.getGuid(), "", QueryEntry.ATTRIBUTE_OPERATOR_EQ);
            entry.setTermWeight(10000);
            searchManager.addQueryEntry(entry);

            // set condition for the views
            String[] views = theTrexCatalog.getViews();
            if (views != null && views.length > 0) 
                addViewsToSearch(views, searchManager, true);
            
            // execute query
            resProduct = searchManager.search();
            
            if (resProduct.getLastError().getCode() != TrexReturn.NO_ERROR) {
                String msg = resProduct.getLastError().getCode() + ": " +
                    resProduct.getLastError().getMsg();
                throw new CatalogBuildFailedException(msg);
            }
            else if(log.isInfoEnabled()) {
                log.info("Number of Hits: " + resProduct.size());
            }

            //
            // read also the category specific attributes if necessary
            //
            if (category.getAttributes().hasNext() && resProduct.size() > 0) {
                    
                // get name of the area
                ResultDocument firstDocument = resProduct.getFirstResultDocument();
                DocAttribute attribute = firstDocument.getFirstDocumentAttribute();
                while (!attribute.getAttributeName().equalsIgnoreCase(ATTR_AREA))
                    attribute = firstDocument.getNextDocumentAttribute();
                String theAreaName = attribute.getValue1();
                    
                // get index
                index = theTrexCatalog.getIndexInfo(INDEX_LEVEL_S,
                                                    theAreaName,
                                                    category.getGuid());
                searchManager = index.getSearchManager(theTrexCatalog.getLanguage());
                    
                // set interval
                searchManager.setResultFromTo(INTERVAL_BEGIN, CatalogParameters.trexUpperLimit);
                    
                // set return properties
                searchManager.addRequestedAttribute(ATTR_OBJECT_GUID);
                    
                // get all category specific attributes
                for (Iterator it = category.getAttributes(); it.hasNext(); ) {
                    String name = ((CatalogAttribute)it.next()).getName();
                    searchManager.addRequestedAttribute(name);
                }
                // set query condition:
                //    as we want to get the S-attributes of every product in this S-index,
                //    there is no need to explicitly restrict the search to the OBJECT_GUIDs
                //    found in the P-search from above
                // and now restrict the search in LOC-Index to the given area
                entry = new QueryEntry();
                entry.setRowType(QueryEntry.ROW_TYPE_ATTRIBUTE);
                entry.setLocation(ATTR_AREA_GUID);
                entry.setContentType(QueryEntry.CONTENT_TYPE_STRING);
                entry.setValue(category.getGuid(), "", QueryEntry.ATTRIBUTE_OPERATOR_EQ);
                entry.setTermWeight(10000);
                searchManager.addQueryEntry(entry);

                // execute query
                resCategory = searchManager.search();
                
                if (resCategory.getLastError().getCode() != TrexReturn.NO_ERROR) {
                    String msg = resCategory.getLastError().getCode() + ": " +
                        resCategory.getLastError().getMsg();
                    throw new CatalogBuildFailedException(msg);
                }
                else if (log.isInfoEnabled()) {
                    log.info("Number of Hits: " + resCategory.size());
                }

            }

            // analyse results
            this.addItems(resProduct, resCategory, category);
        }
        catch (TrexException ex) {
            throw new CatalogBuildFailedException(ex.getLocalizedMessage(), ex);
        }
        catch (TrexCRMCatalogException ex) {
            throw new CatalogBuildFailedException(ex.getLocalizedMessage());
        }
        catch (CatalogException ex) {
            throw new CatalogBuildFailedException(ex.getLocalizedMessage());
        }
        
        if (runtimeLogger.isDebugEnabled()) {
            runtimeLogger.debug("end   buildItemsInternal()");
        }
    }

    /**
     * Internal helper method to build a query for categories.
     * A-Index is searched.
     */
    protected void buildQueryCategoriesInternal(CatalogQuery query)
        throws CatalogBuildFailedException {
            
        // first build all categories if not already done
        if (!isAllCategoriesBuilt(theTrexCatalog))
            this.buildAllCategories(theTrexCatalog);

        try {
            TrexCRMCatalogIndexInfo index = theTrexCatalog.getIndexInfo(INDEX_LEVEL_A, "", "");
                
            SearchManager searchManager = index.getSearchManager(theTrexCatalog.getLanguage());
                
            // create new helper instances
            // TrexCRMCatalogQueryCompiler compiler
                // = new TrexCRMCatalogQueryCompiler(
                    // theTrexCatalog.getMessageResources(),
                    // ((TrexCRMCatalogServerEngine)theTrexCatalog.getServerEngine()).
                    // getQuickSearchStrategy());
            TrexCRMCatalogQueryCompiler compiler = new TrexCRMCatalogQueryCompiler(theTrexCatalog.getMessageResources());
                
            // set return properties
            List defAttr = new ArrayList();
            for (Iterator iter = index.getAttributes().iterator(); iter.hasNext(); ) {
                String name = (String)iter.next();
                if (!theExcludedAttributes.contains(name) 
                    && !name.equalsIgnoreCase(ATTR_TEXT_0001) 
                    && !name.equalsIgnoreCase(ATTR_DESCRIPTION) 
                    && !name.equalsIgnoreCase(ATTR_DOC_PC_CRM_THUMB)) {
                    defAttr.add(name);
                }
            }
                
            // set default return attributes
            searchManager.addRequestedAttribute(ATTR_AREA_GUID);
                
            // transform query
            compiler.transformCategoryQuery(query,
                                            defAttr,
                                            index.getAttributes(),
                                            theTrexCatalog,
                                            index.getSearchEngine(),
                                            searchManager);
            
//TODO check if this is necessary                                            
            // add views to search
            String[] views = theTrexCatalog.getViews();
            if(views != null && views.length > 0)
                addViewsToSearch(views, searchManager, true);
            
            // execute query
            SearchResult results = searchManager.search();
            
            if (results.getLastError().getCode() != TrexReturn.NO_ERROR) {
                String msg = results.getLastError().getCode() + ": " +
                             results.getLastError().getMsg();
                throw new CatalogBuildFailedException(msg);
            } else if (log.isInfoEnabled()) {
                log.info("Number of Hits: " + results.size());
            }

            // analyse results
            this.addQuery(results, query);
        }
        catch (TrexException ex) {
            throw new CatalogBuildFailedException(ex.getLocalizedMessage());
        }
        catch (TrexCRMCatalogException ex) {
            throw new CatalogBuildFailedException(ex.getLocalizedMessage());
        }
    }

    /**
     * Dispatcher for the different kinds of queries.
     */
    protected void buildQueryInternal(CatalogQuery query)
        throws CatalogBuildFailedException {
            
        CatalogQueryStatement statement = query.getCatalogQueryStatement();

        if (statement.getSearchType() == IQueryStatement.Type.CATEGORY_SEARCH)
            buildQueryCategoriesInternal(query);
        else
            buildQueryItemsInternal(query);
    }

    /**
     * Performs a search on the P-Index for the given query.
     */
    private SearchResult buildQueryItemsFromPIndex(CatalogQuery query) throws CatalogBuildFailedException {

        final String METHOD = "buildQueryItemsFromPIndex(CatalogQuery query)";
        if (log.isDebugEnabled()) {
            log.entering(METHOD);
            log.debug("query statement:" + query.getStatementAsString());
        }

        // first build all global attributes if not already done
        if (!isAttributesBuilt(theTrexCatalog)) {
            buildAttributes(theTrexCatalog);
        }

        SearchResult resProduct = null;

        try {

            // set environment info for 'P' index
            TrexCRMCatalogIndexInfo index = theTrexCatalog.
                  getIndexInfo(INDEX_LEVEL_P, "", theTrexCatalog.getName());
                
            SearchManager searchManager = index.getSearchManager(theTrexCatalog.getLanguage());
                
            // set default return attributes
            searchManager.addRequestedAttribute(ATTR_AREA_GUID);
            searchManager.addRequestedAttribute(ATTR_OBJECT_GUID);
            searchManager.addRequestedAttribute(ATTR_OBJECT_DESC);

            // create a list of default return attributes
            List defAttr = new ArrayList();
            for (Iterator it = index.getAttributes().iterator(); it.hasNext(); ) {
                
                String attrName = (String) it.next();
                // add to default attribute list
                if (!theExcludedAttributes.contains(attrName) 
                    && !attrName.equalsIgnoreCase(ATTR_OBJECT_DESC)) {
                    defAttr.add(attrName);
                }
            }

            // compile the query
            TrexCRMCatalogQueryCompiler compiler = new TrexCRMCatalogQueryCompiler(theTrexCatalog.getMessageResources());
            compiler.transformItemQuery(query,
                                        defAttr,
                                        index.getAttributes(),
                                        theTrexCatalog,
                                        index.getSearchEngine(),
                                        searchManager);

            // add views to search
            String[] views = theTrexCatalog.getViews();
            if (views != null && views.length > 0)
                addViewsToSearch(views, searchManager, true);
            
            // execute query
            resProduct = searchManager.search();
            if (resProduct.getLastError().getCode() != TrexReturn.NO_ERROR) {
                String msg = resProduct.getLastError().getCode() + ": " +
                    resProduct.getLastError().getMsg();
                throw new CatalogBuildFailedException(msg);
            }
            else if (log.isInfoEnabled()) {
                log.info("Number of Hits: " + resProduct.size());
            }

            // analyse results
            if (query.inCountMode()) {
                ResultDocument aDoc = resProduct.getFirstResultDocument();
                int count = 0;
                for ( ; aDoc!=null;aDoc=resProduct.getNextResultDocument()) {
                    count++;
                } // end of for ()
                query.setCountInternal(count);
            }
            else {
                this.addQuery(resProduct, null, query);
            } // end of else

        }
        catch (TrexException ex) {
            throw new CatalogBuildFailedException(ex.getLocalizedMessage());
        }
        catch (TrexCRMCatalogException ex) {
            throw new CatalogBuildFailedException(ex.getLocalizedMessage());
        }

        log.exiting();
        return resProduct;
    }

    /**
     * Performs a search on category specific attributes (S-Index, then P-Index).
     * 
     * If the <code>query</code> is not a category specific search, nothing is done.
     */
    private void buildQueryItemsFromSIndex(CatalogQuery query) throws CatalogBuildFailedException {

        final String METHOD = "buildQueryItemsFromSIndex(CatalogQuery query)";
        if (log.isDebugEnabled()) {
            log.entering(METHOD);
            log.debug("query statement:" + query.getStatementAsString());
        }

        // Check preconditions
        if (!query.isCategorySpecificSearch()) {
            if (log.isDebugEnabled()) {
               log.debug(METHOD + " was called, although no, the query is not category specific.");
               log.exiting();
            }   
            return;
        }

        // first build all global attributes if not already done
        if (!isAttributesBuilt(theTrexCatalog)) {
            buildAttributes(theTrexCatalog);
        }

        try {

            // execute the query on the S-Index
            SearchResult resCategory = querySpecificIndex(query);

            // check if something was found
            if (resCategory == null || resCategory.size() == 0) {
                log.debug("Nothing found in S-Index for " + query.getStatementAsString());
                log.exiting();
                return;
            }

            // set environment info for 'P' index
            TrexCRMCatalogIndexInfo index = theTrexCatalog.
                getIndexInfo(INDEX_LEVEL_P, "", theTrexCatalog.getName());
                
            SearchManager searchManager = index.getSearchManager(theTrexCatalog.getLanguage());
                
            // set default return attributes
            searchManager.addRequestedAttribute(ATTR_AREA_GUID);
            searchManager.addRequestedAttribute(ATTR_OBJECT_GUID);
            searchManager.addRequestedAttribute(ATTR_OBJECT_DESC);

            // create a list of default return attributes
            ArrayList defAttr = new ArrayList();
            for (Iterator it = index.getAttributes().iterator(); it.hasNext(); ) {
                String attrName = (String)it.next();
                    // add to default attribute list
                if (!theExcludedAttributes.contains(attrName) 
                    && !attrName.equalsIgnoreCase(ATTR_OBJECT_DESC)) {
                    defAttr.add(attrName);
                }
            }

            // set return properties
            searchManager.setResultFromTo(INTERVAL_BEGIN, CatalogParameters.trexUpperLimit);

            ResultDocument firstDocument = resCategory.getFirstResultDocument();
            for (ResultDocument document = resCategory.getFirstResultDocument();
                document != null;
                document = resCategory.getNextResultDocument()) {

                boolean found = false;
                for (DocAttribute attribute = document.getFirstDocumentAttribute();
                     attribute != null && !found;
                     attribute = document.getNextDocumentAttribute()) {
                    if (attribute.getAttributeName().equalsIgnoreCase(ATTR_OBJECT_GUID)) {
                        QueryEntry entry = new QueryEntry();
                        entry.setRowType(QueryEntry.ROW_TYPE_ATTRIBUTE);
                        entry.setLocation(ATTR_OBJECT_GUID);
                        entry.setContentType(QueryEntry.CONTENT_TYPE_STRING);
                        entry.setValue(attribute.getValue1(), "",
                                       QueryEntry.ATTRIBUTE_OPERATOR_EQ);
                        entry.setTermWeight(10000);
                        searchManager.addQueryEntry(entry);
                            
                        if (firstDocument != document) {
                            // add the "OR"
                            entry = new QueryEntry();
                            entry.setRowType(QueryEntry.ROW_TYPE_OPERATOR);
                            entry.setValue(QueryEntry.QUERY_OPERATOR_OR, "",
                                           QueryEntry.ATTRIBUTE_OPERATOR_EQ);
                            searchManager.addQueryEntry(entry);
                        }
                        // abort search
                        found = true;
                    }
                }
            }
            
            // category guid
            QueryEntry entry = new QueryEntry();
            entry.setRowType(QueryEntry.ROW_TYPE_ATTRIBUTE);
            entry.setLocation(ATTR_AREA_GUID);
            entry.setContentType(QueryEntry.CONTENT_TYPE_STRING);
            entry.setValue(query.getParent().getGuid(), "", QueryEntry.ATTRIBUTE_OPERATOR_EQ);
            entry.setTermWeight(10000);
            searchManager.addQueryEntry(entry);
            
            // add the "AND"
            entry = new QueryEntry();
            entry.setRowType(QueryEntry.ROW_TYPE_OPERATOR);
            entry.setValue(QueryEntry.QUERY_OPERATOR_AND, "", QueryEntry.ATTRIBUTE_OPERATOR_EQ);
            searchManager.addQueryEntry(entry);

            for (Iterator it = defAttr.iterator(); it.hasNext(); ) {
                searchManager.addRequestedAttribute((String)it.next());
            }

            // add views to search
            String[] views = theTrexCatalog.getViews();
            if (views != null && views.length > 0) {
                addViewsToSearch(views, searchManager, true);
            }

            // execute query
            SearchResult resProduct = searchManager.search();
            if (resProduct.getLastError().getCode() != TrexReturn.NO_ERROR) {
                String msg = resProduct.getLastError().getCode() + ": " +
                    resProduct.getLastError().getMsg();
                throw new CatalogBuildFailedException(msg);
            }
            else if (log.isInfoEnabled()) {
                log.info("Number of Hits: " + resProduct.size());
            }

            // analyse results
            if (query.inCountMode()) {
                ResultDocument aDoc = resProduct.getFirstResultDocument();
                int count = 0;
                for ( ; aDoc!=null;aDoc=resProduct.getNextResultDocument()) {
                    count++;
                } // end of for ()
                query.setCountInternal(count);
            }
            else {
                this.addQuery(resProduct, resCategory, query);
            } // end of else

        }
        catch(TrexException ex) {
            throw new CatalogBuildFailedException(ex.getLocalizedMessage());
        }
        catch (TrexCRMCatalogException ex) {
            throw new CatalogBuildFailedException(ex.getLocalizedMessage());
        }

        log.exiting();
        return;
        
    }

    /**
     * Dispatches item queries to the appropriate methods.
     * 
     * @param  query to be performed
     * @throws CatalogBuildFailedException
     */
    protected void buildQueryItemsInternal(CatalogQuery query) throws CatalogBuildFailedException {

        // get the query statement
        CatalogQueryStatement statement = query.getCatalogQueryStatement();

        // Do not perform an area search here
        if (statement.getSearchType() == IQueryStatement.Type.CATEGORY_SEARCH) {
            return;
        }

        // search on S-Index
        if (query.isCategorySpecificSearch()) {
            buildQueryItemsFromSIndex(query);
        }
        else { // search on P-Index
            SearchResult resProduct = buildQueryItemsFromPIndex(query);

            // Attributes from S-Index should be read, too
            if (query.getRequestCategorySpecificAttributes()) {
                readSIndexAttributesForPIndexResults(resProduct, query);
            }
        }
        return;
    }

    /**
     * Stores the results of a S-Index search in the <code>builtItems</code>.
     * 
     * @param resCategory
     * @param builtItems
     */
    private void processSIndexResults(SearchResult resCategory, HashMap builtItems) {

        HashMap attrValues = new HashMap();
        CatalogAttributeValue attrValue;

        // if no area specific attributes were found then exit
        if (resCategory == null) {
            return;
        }

        // unfortunately we have first to find the document for each product
        for (ResultDocument document = resCategory.getFirstResultDocument();
             document != null; 
             document = resCategory.getNextResultDocument()) {
                 
            boolean found = false;
            for (DocAttribute attribute = document.getFirstDocumentAttribute();
                 attribute != null && !found;
                 attribute = document.getNextDocumentAttribute()) {
                if (attribute.getAttributeName().equalsIgnoreCase(ATTR_OBJECT_GUID)) {
                    Object[] values = (Object[])builtItems.get(attribute.getValue1());
					if (values != null) {
                        values[1] = document;
                        found = true;
					}
                }
            }
        }

        // now analyse the results of the S index
        for (Iterator it = builtItems.keySet().iterator(); it.hasNext(); ) {
            
            String guid = (String)it.next();
            Object[] values = (Object[])builtItems.get(guid);
            CatalogItem item = (CatalogItem)values[0];
            ResultDocument document = (ResultDocument)values[1];
            attrValues.clear();

            for (DocAttribute attribute = document.getFirstDocumentAttribute();
                 attribute != null; 
                 attribute = document.getNextDocumentAttribute()) {
                     
                // set all attributes
                if (!attribute.getAttributeName().equalsIgnoreCase(ATTR_OBJECT_GUID)) {
                    // if multivalued attribute - value was already created
                    attrValue = (CatalogAttributeValue)attrValues.get(attribute.getAttributeName());
                    if (attrValue != null) {
                        attrValue.addAsStringInternal(attribute.getValue1());
                    }
                    else {
                        attrValue = item.createAttributeValueInternal(attribute.getAttributeName());
                        attrValue.setAsStringInternal(attribute.getValue1());
                        attrValues.put(attribute.getAttributeName(), attrValue);
                    }
                }
            }
        }
    }

    /**
      * Executes a query on the A-Index.
      *  
      * @param categoryGuid     for which the area name is returned
      * @return SearchResult for the area.
      * @exception CatalogBuildFailedException
      */
    private SearchResult queryAreaIndex(String categoryGuid) 
                 throws CatalogBuildFailedException {

        SearchResult resultSetA = null;
        
        try {
            // ask 'A' index for area name
            // set environment info for 'A' index
            TrexCRMCatalogIndexInfo index = theTrexCatalog.getIndexInfo(INDEX_LEVEL_A, "", "");
            SearchManager searchManager = index.getSearchManager(theTrexCatalog.getLanguage());
            
            // set return properties
            searchManager.setResultFromTo(INTERVAL_BEGIN, CatalogParameters.trexUpperLimit);
            searchManager.addRequestedAttribute(ATTR_AREA);
            searchManager.addRequestedAttribute(ATTR_AREA_SPEZ_EXIST);
            
            // set query condition
            QueryEntry entry = new QueryEntry();
            entry.setRowType(QueryEntry.ROW_TYPE_ATTRIBUTE);
            entry.setLocation(ATTR_AREA_GUID);
            entry.setContentType(QueryEntry.CONTENT_TYPE_STRING);
            entry.setValue(categoryGuid, "", QueryEntry.ATTRIBUTE_OPERATOR_EQ);
            entry.setTermWeight(10000);
            searchManager.addQueryEntry(entry);
            
            // execute query
            resultSetA = searchManager.search();
            if (resultSetA.getLastError().getCode() != TrexReturn.NO_ERROR) {
                String msg = resultSetA.getLastError().getCode() + ": " +
                    resultSetA.getLastError().getMsg();
                throw new CatalogBuildFailedException(msg);
            }
            else if (log.isInfoEnabled()) {
                log.info("Number of Hits: " + resultSetA.size());
            }
        }
        catch (TrexException ex) {
             throw new CatalogBuildFailedException(ex.getLocalizedMessage());
        }
        catch (TrexCRMCatalogException ex) {
             throw new CatalogBuildFailedException(ex.getLocalizedMessage());
        }
 
        return resultSetA; 
    }

    /**
     * Executes a query on the S-Index (category) and returns the object guids (OBJECT_GUID) as well
     * as the default attributes of the S-Index.
     * 
     * @param   query
     * @return  the SearchResult with the object guids and the S-Index attributes 
     * @exception CatalogBuildFailedException
     */
    private SearchResult querySpecificIndex(CatalogQuery query) 
                 throws CatalogBuildFailedException {

        SearchResult resCategory = null;
        
        try {
            // Get A-Index attributes
            SearchResult resultSetA = queryAreaIndex(query.getParent().getGuid());
            ResultDocument document = resultSetA.getFirstResultDocument();
            DocumentAttributes docAttr = new DocumentAttributes(document);
            
            // return, if there are no area specific attributes
            if (!docAttr.hasAreaSpecAttr()) {
                return null;
            }
    
            String theAreaName = docAttr.getAreaName();
    
            // now ask 'S' index for all entries which conform to filter expression
            TrexCRMCatalogIndexInfo index = theTrexCatalog.getIndexInfo(INDEX_LEVEL_S,
                                                                 theAreaName, 
                                                                 query.getParent().getGuid());
            SearchManager searchManager = index.getSearchManager(theTrexCatalog.getLanguage());
            
            // set default return attributes for 'S' index
            searchManager.addRequestedAttribute(ATTR_OBJECT_GUID);
            
            // create a list with the default attributes
            ArrayList defAttr = new ArrayList();
            for (Iterator it = index.getAttributes().iterator(); it.hasNext(); ) {
                String attrName = (String)it.next();
                // add to default attribute list
                if (!theExcludedAttributes.contains(attrName))
                    defAttr.add(attrName);
            }
    
            // create new helper instances
            TrexCRMCatalogQueryCompiler compiler = new TrexCRMCatalogQueryCompiler(theTrexCatalog.getMessageResources());
    
            // transform query statement
            compiler.transformItemQuery(query,
                                        defAttr,
                                        index.getAttributes(),
                                        theTrexCatalog,
                                        index.getSearchEngine(),
                                        searchManager);
            // add vies to search
//            S-Index doesn't know VIEW_ID attribute, so views are unknown
//            String[] views = theTrexCatalog.getViews();
//            if (views != null && views.length > 0)
//                addViewsToSearch(views, searchManager, true);
    
            // and now restrict the search in LOC-Index to the given area
            QueryEntry entry = new QueryEntry();
            entry.setRowType(QueryEntry.ROW_TYPE_ATTRIBUTE);
            entry.setLocation(ATTR_AREA_GUID);
            entry.setContentType(QueryEntry.CONTENT_TYPE_STRING);
            entry.setValue(query.getParent().getGuid(), "",
                           QueryEntry.ATTRIBUTE_OPERATOR_EQ);
            entry.setTermWeight(10000);
            searchManager.addQueryEntry(entry);
            // add the "AND"
            entry = new QueryEntry();
            entry.setRowType(QueryEntry.ROW_TYPE_OPERATOR);
            entry.setValue(QueryEntry.QUERY_OPERATOR_AND, "",
                           QueryEntry.ATTRIBUTE_OPERATOR_EQ);
            searchManager.addQueryEntry(entry);
    
            // execute query
            resCategory = searchManager.search();
            if (resCategory.getLastError().getCode() != TrexReturn.NO_ERROR) {
                String msg = resCategory.getLastError().getCode() + ": " +
                    resCategory.getLastError().getMsg();
                throw new CatalogBuildFailedException(msg);
            }
            else if (log.isInfoEnabled()) {
                log.info("Number of Hits: " + resCategory.size());
            }
        }
        catch (TrexException ex) {
             throw new CatalogBuildFailedException(ex.getLocalizedMessage());
        }
        catch (TrexCRMCatalogException ex) {
             throw new CatalogBuildFailedException(ex.getLocalizedMessage());
        }

        // check if something was found - if not there are no results!
        if (resCategory.size() == 0)
            return null;

        return resCategory;
    }
    
    /**
     * Executes a query on the S-Index and returns the object guids (OBJECT_GUID) as well
     * as the default attributes of the S-Index.
     * 
     * @param   query
     * @return  the SearchResult with the object guids and the S-Index attributes,
     *          or null, if no area specific attributes exist.
     */
    private SearchResult querySpecificIndexForObjectGuids(String areaGuid, String[] objectGuids)
        throws CatalogBuildFailedException {

        SearchResult resCategory = null;
        
        try {
            // Get A-Index attributes
            SearchResult resultSetA = queryAreaIndex(areaGuid);
            ResultDocument document = resultSetA.getFirstResultDocument();
            DocumentAttributes docAttr = new DocumentAttributes(document);
            
            // return, if there are no area specific attributes
            if (!docAttr.hasAreaSpecAttr()) {
                return null;
            }
            
            String theAreaName = docAttr.getAreaName();
    
            // set environment info for 'S' index
            TrexCRMCatalogIndexInfo index = theTrexCatalog.getIndexInfo(INDEX_LEVEL_S,
                                                                 theAreaName, 
                                                                 areaGuid);
            SearchManager searchManager = index.getSearchManager(theTrexCatalog.getLanguage());
    
            // let the S-Index return the object guid
            searchManager.addRequestedAttribute(ATTR_OBJECT_GUID);
            searchManager.addRequestedAttribute(ATTR_AREA_GUID);
    
            // create a list with the default attributes
            ArrayList defAttr = new ArrayList();
            for (Iterator it = index.getAttributes().iterator(); it.hasNext(); ) {
                String attrName = (String)it.next();
                // add to default attribute list
                if (!theExcludedAttributes.contains(attrName))
                    defAttr.add(attrName);
            }
    //        // create new helper instances
    //        TrexCRMCatalogQueryCompiler compiler = new TrexCRMCatalogQueryCompiler(theTrexCatalog.getMessageResources());
    //
    //        // transform query statement
    //        compiler.transformItemQuery(query,
    //                                    defAttr,
    //                                    index.getAttributes(),
    //                                    theTrexCatalog,
    //                                    index.getSearchEngine(),
    //                                    searchManager);
    
            // and now restrict the search in LOC-Index to the given area
            QueryEntry entry = new QueryEntry();
            entry.setRowType(QueryEntry.ROW_TYPE_ATTRIBUTE);
            entry.setLocation(ATTR_AREA_GUID);
            entry.setContentType(QueryEntry.CONTENT_TYPE_STRING);
            entry.setValue(areaGuid, "", QueryEntry.ATTRIBUTE_OPERATOR_EQ);
            entry.setTermWeight(10000);
            searchManager.addQueryEntry(entry);
            // add the "AND"
            entry = new QueryEntry();
            entry.setRowType(QueryEntry.ROW_TYPE_OPERATOR);
            entry.setValue(QueryEntry.QUERY_OPERATOR_AND, "",
                           QueryEntry.ATTRIBUTE_OPERATOR_EQ);
            searchManager.addQueryEntry(entry);
    
            // and now restrict the search in LOC-Index to the given area
            for (int i = 0; i < objectGuids.length; i++) {
                entry = new QueryEntry();
                entry.setRowType(QueryEntry.ROW_TYPE_ATTRIBUTE);
                entry.setLocation(ATTR_OBJECT_GUID);
                entry.setContentType(QueryEntry.CONTENT_TYPE_STRING);
                entry.setValue(objectGuids[i], "", QueryEntry.ATTRIBUTE_OPERATOR_EQ);
                entry.setTermWeight(10000);
                searchManager.addQueryEntry(entry);
                    
                if (objectGuids.length > 1) {
                    // add the "OR"
                    entry = new QueryEntry();
                    entry.setRowType(QueryEntry.ROW_TYPE_OPERATOR);
                    entry.setValue(QueryEntry.QUERY_OPERATOR_OR, "",
                                   QueryEntry.ATTRIBUTE_OPERATOR_EQ);
                    searchManager.addQueryEntry(entry);
                }
            }
            if (objectGuids.length > 0) {
                // add the "AND"
                entry = new QueryEntry();
                entry.setRowType(QueryEntry.ROW_TYPE_OPERATOR);
                entry.setValue(QueryEntry.QUERY_OPERATOR_AND, "",
                               QueryEntry.ATTRIBUTE_OPERATOR_EQ);
                searchManager.addQueryEntry(entry);
            }
            
            // execute query
            resCategory = searchManager.search();
            if (resCategory.getLastError().getCode() != TrexReturn.NO_ERROR) {
                String msg = resCategory.getLastError().getCode() + ": " +
                    resCategory.getLastError().getMsg();
                throw new CatalogBuildFailedException(msg);
            }
            else if (log.isInfoEnabled()) {
                log.info("Number of Hits: " + resCategory.size());
            }
        }
        catch (TrexException ex) {
             throw new CatalogBuildFailedException(ex.getLocalizedMessage());
        }
        catch (TrexCRMCatalogException ex) {
             throw new CatalogBuildFailedException(ex.getLocalizedMessage());
        }
        // check if something was found - if not there are no results!
        if (resCategory.size() == 0)
            return null;

        return resCategory;
    }

    /**
     * Performs a search on the S-Index for the <code>ATTR_OBJECT_GUID</code> objects, that are found
     * inside the <code>resProduct</code> of a previous P-Index <code>query</code>. The attributes, that
     * are found in the S-Index are appended to the <code>query</code>.
     * 
     * The method can handle, that the objects are from multiple areas. In this case, a search on the
     * S-Index is performed for each area..
     * 
     * If <code>resProduct</code> contains no entries, nothing is done.
     * @param resProduct
     * @param query
     * @exception CatalogBuildFailedException
     */
    private void readSIndexAttributesForPIndexResults(SearchResult resProduct, CatalogQuery query)
        throws CatalogBuildFailedException {

        final String METHOD = "readSIndexAttributesForPIndexResults()";
        if (log.isDebugEnabled()) {
            log.entering(METHOD);
            log.debug("query statement:" + query.getStatementAsString());
        }

        // check if something was provided by the previous P-Index query
        if (resProduct.size() == 0) {
            if (log.isDebugEnabled()) {
                log.debug(METHOD + ": ResultSet of P-Index is empty. Returning without doing anything.");
                log.exiting();
            }
            return;
        }

        // S-Index is area specific. We need the products grouped by area to ask the S-Index.
        // objectsPerArea is a HashMap of the areaGuids with an ArrayList of the products in the 
        // according area.
        HashMap objectsPerArea = new HashMap();
        for (ResultDocument document = resProduct.getFirstResultDocument();
            document != null;
            document = resProduct.getNextResultDocument()) {

            String areaGuid = null;
            String objectGuid = null;
            
            // exit loop if both (areaGuid and objectGuid) were found
            for (DocAttribute attribute = document.getFirstDocumentAttribute();
                 attribute != null && (areaGuid == null || objectGuid == null);
                 attribute = document.getNextDocumentAttribute()) {
                if (attribute.getAttributeName().equalsIgnoreCase(ATTR_AREA_GUID))
                    areaGuid = attribute.getValue1();
                if (attribute.getAttributeName().equalsIgnoreCase(ATTR_OBJECT_GUID))
                    objectGuid = attribute.getValue1();
            }

            if (objectsPerArea.containsKey(areaGuid)) {
                ArrayList objectsInThisArea = (ArrayList) objectsPerArea.get(areaGuid);
                objectsInThisArea.add(objectGuid);
            }
            else {
                ArrayList objectsInThisArea = new ArrayList();
                objectsInThisArea.add(objectGuid);
                objectsPerArea.put(areaGuid, objectsInThisArea);
            }
        }

        try {
            // Now ask S-Index for each area
            Iterator it = objectsPerArea.entrySet().iterator();
            while (it.hasNext()) {

                Map.Entry me = (Map.Entry) it.next();
                String areaGuid = (String) me.getKey();
                ArrayList objectsInThisArea = (ArrayList) me.getValue();

                String[] objectIDs = (String[]) objectsInThisArea.toArray(new String[0]);

                SearchResult resCategory = querySpecificIndexForObjectGuids(areaGuid, objectIDs);
                
                // determine area specific attrubte names and attach them to the query object
				SearchResult resultSetA = queryAreaIndex(areaGuid);
			    ResultDocument document = resultSetA.getFirstResultDocument();
			    DocumentAttributes docAttr = new DocumentAttributes(document);
			    String theAreaName = docAttr.getAreaName();
                
				addCategoryAttributesToQuery(query, theAreaName, areaGuid);

                // update results
                appendSAttributes(resProduct, resCategory, query);
            }
        }
        catch (TrexCRMCatalogException ex) {
            throw new CatalogBuildFailedException(ex.getMessage(), ex);
        }

        log.exiting();
        return;
    }

    /**
     * Converts an TREX attribute of type DOUBLE to String
     * 
     * @param attribute
     */
    private void convertAttrDoubleToString(DocAttribute attribute) {

        final String METHOD = "convertAttrDoubleToString()";
        log.entering(METHOD);
        
        String lAttrValue = null;

        if (log.isDebugEnabled()) {
            lAttrValue = new String(attribute.getValue1());
        }

        String attrName = attribute.getAttributeName();
        if (attrName != null && attrName.startsWith(ATTR_LOY_POINTS)) {
            BigDecimal pts = new BigDecimal(attribute.getValue1());
            BigDecimal pts2 = pts.setScale(2, BigDecimal.ROUND_HALF_UP);
            attribute.setValue1(pts2.toString());

            if (log.isDebugEnabled()) {
                log.debug("attrName = " + attribute.getAttributeName() +
                          "/ attrValue1 before conversion = " + lAttrValue +
                          "/ attrValue1 after conversion = " + attribute.getValue1());
            }

        }
                     
        log.exiting();
        return;
    }

    /**
     * Contains the attributes of a result document
     */
    private class DocumentAttributes {
        
        protected String categoryGuid = "";
        protected String nameInternal = "";
        protected String descriptionInternal = "";
        protected String areaName = "";
        protected String thumbNail = "";
        protected String parentAreaGuid = "";
        protected boolean hasAreaSpecAttr = false;
        protected String areaSpecAttr = "";
        protected boolean hasTimestamp = false;
        protected String timestamp = "";
        protected String objectGuid = "";

        /**
         * Constructor
         *
         * @param document, ResultDocument from which the attributes are read
         */
        DocumentAttributes(ResultDocument document) {
            if (document != null) {
                for (DocAttribute attribute = document.getFirstDocumentAttribute();
                         attribute != null; 
                         attribute = document.getNextDocumentAttribute()) {
                    if (attribute.getAttributeName().equalsIgnoreCase(ATTR_AREA_GUID))
                        this.categoryGuid = attribute.getValue1();
                    else if (attribute.getAttributeName().equalsIgnoreCase(ATTR_DESCRIPTION))
                        this.nameInternal = attribute.getValue1();
                    else if (attribute.getAttributeName().equalsIgnoreCase(ATTR_TEXT_0001))
                        this.descriptionInternal = attribute.getValue1();
                    else if (attribute.getAttributeName().equalsIgnoreCase(ATTR_PARENT_AREA_GUID))
                        this.parentAreaGuid = attribute.getValue1();
                    else if (attribute.getAttributeName().equalsIgnoreCase(ATTR_DOC_PC_CRM_THUMB))
                        this.thumbNail = attribute.getValue1();
                    else if (attribute.getAttributeName().equalsIgnoreCase(ATTR_AREA))
                        this.areaName = attribute.getValue1();
                    else if (attribute.getAttributeName().equalsIgnoreCase(ATTR_AREA_SPEZ_EXIST) &&
                             attribute.getValue1().equals(VALUE_X)) {
                        this.hasAreaSpecAttr = true;
                        this.areaSpecAttr = attribute.getValue1();
                    }
                    else if (attribute.getAttributeName().equalsIgnoreCase(ATTR_TIMESTAMP)) {
                        this.hasTimestamp = true;
                        this.timestamp = attribute.getValue1();
                    }
                    else if (attribute.getAttributeName().equalsIgnoreCase(ATTR_OBJECT_GUID))
                        this.objectGuid = attribute.getValue1();
                }
            }
        }

        /**
         * Returns the category guid 
         * @return categroyGuid as String
         */
         String getCategoryGuid() {
             return this.categoryGuid;
         }

         /**
         * Returns the internal name 
         * @return nameInternal as String
         */
         String getNameInternal() {
             return this.nameInternal;
         }

         /**
         * Returns the internal description
         * @return decriptionInternal as String
         */
         String getDescriptionInternal() {
             return this.descriptionInternal;
         }

         /**
         * Returns the area name
         * @return name as String
         */
         String getAreaName() {
             return this.areaName;
         }

         /**
         * Returns the internal thumb nail
         * @return thumbNail as String
         */
         String getThumbNail() {
             return this.thumbNail;
         }

         /**
         * Returns the parent area guid
         * @return parentAreaGuid as String
         */
         String getParentAreaGuid() {
             return this.parentAreaGuid;
         }

         /**
         * Returns a flag if area specific attributes exist
         * @return hasAreaSpecAttr as boolean
         */
         boolean hasAreaSpecAttr() {
             return this.hasAreaSpecAttr;
         }

         /**
         * Returns the parent area guid
         * @return parentAreaGuid as String
         */
         String[] getAreaValues() {
             String[] values = new String[3];
             values[0] = this.parentAreaGuid;
             values[1] = this.areaName;
             values[2] = this.areaSpecAttr;
             return values;
         }

         /**
         * Returns a flag if timestamp attribute exists
         * @return hasTimestamp as boolean
         */
         boolean hasTimestamp() {
             return this.hasTimestamp;
         }

         /**
         * Returns the timestamp ATTR_TIMESTAMP
         * @return timestamp as String
         */
         String getTimestamp() {
             return this.timestamp;
         }

         /**
         * Returns the timestamp ATTR_OBJECT_GUID
         * @return objectGuid as String
         */
         String getObjectGuid() {
             return this.objectGuid;
         }
    }

}