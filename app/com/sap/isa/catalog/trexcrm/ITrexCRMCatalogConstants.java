/*****************************************************************************
    Class:        TrexCRMCatalogConstants
    Copyright (c) 2006, SAP AG, All rights reserved.
    Author:
    Created:      05.09.2006
    Version:      1.0

    $Revision$
    $Date$
*****************************************************************************/
package com.sap.isa.catalog.trexcrm;

import com.sap.isa.catalog.trexgen.*;
/**
 * This interface contains the general constants which are used for the Trex
 * catalog.
 * @version     1.0
 */
public interface ITrexCRMCatalogConstants extends ITrexGenCatalogConstants
{
}
