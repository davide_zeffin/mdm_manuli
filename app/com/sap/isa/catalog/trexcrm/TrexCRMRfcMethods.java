/**
 * Class:        TrexCRMRfcMethods Copyright (c) 2006, SAP AG, Germany, All rights reserved. Created:      04.11.2006
 */
package com.sap.isa.catalog.trexcrm;

import java.util.HashMap;
import java.util.Map;

import org.apache.struts.util.MessageResources;

import com.sap.isa.catalog.ICatalogMessagesKeys;
import com.sap.isa.catalog.boi.IClient;
import com.sap.isa.catalog.boi.IServerEngine;
import com.sap.isa.catalog.impl.Catalog;
import com.sap.isa.catalog.misc.CatalogJCoLogHelper;
import com.sap.isa.core.ExtendedTechKey;
import com.sap.isa.core.TechKey;
import com.sap.isa.core.eai.BackendException;
import com.sap.isa.core.eai.sp.jco.JCoConnection;
import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.core.logging.LogUtil;
import com.sap.isa.core.util.CacheManager;
import com.sap.isa.core.util.table.ResultData;
import com.sap.isa.core.util.table.Table;
import com.sap.isa.core.util.table.TableRow;
import com.sap.mw.jco.JCO;
import com.sapportals.trex.TrexException;
import com.sapportals.trex.core.TrexFactory;
import com.sapportals.trex.core.admin.AdminIndex;
import com.sapportals.trex.core.admin.AdminManager;
import com.sapportals.trex.core.admin.ServerInfo;
import com.sapportals.trex.core.admin.ServerInfoList;


/**
 * Encapsulates all functions of the CRM system which are used in the CRM catalog implementation.
 */
public class TrexCRMRfcMethods {
    // the logging instance
    private static IsaLocation log = IsaLocation.getInstance(TrexCRMRfcMethods.class.getName());

    private static final String MSG_1 = "Catalog not determinable. ";
    private static final String MSG_2 = " catalogs found with state active";
    private static final String MSG_3 = " catalogs found with TechKey : ";
    private static final String MSG_4 = "More than one catalog found for the given Techkey: ";

    private static final String NAMESERVER_CACHE_NAME = "TREXNameServerCache";
    private static final int NAMESERVER_CACHE_SIZE = 1;

    static {
        CacheManager.addCache(NAMESERVER_CACHE_NAME, NAMESERVER_CACHE_SIZE);
    }

    
    /**
     * Gets the list of all indices of a CRM system.
     *
     * @param connection connection to be used for retrieving the data
     * @param catPattern pattern name of the catalog
     *
     * @return the list of indices
     *
     * @exception TrexCRMCatalogException connection error occured
     */
    public static ResultData getIndexList(JCoConnection connection,
                                          String        catPattern)
                                   throws TrexCRMCatalogException {
        Table resTab = null;

        try {
            // get jco function
            JCO.Function function = connection.getJCoFunction("COM_PCAT_STAGINGVERSIONS_GET");

            // set exporting parameter (parameter does not exist in all releases)
            JCO.ParameterList impList = function.getImportParameterList();

            if (impList != null) {
                if (impList.isImport("IV_CATALOG_ID") && (catPattern != null)) {
                    impList.setValue(catPattern, "IV_CATALOG_ID");
                }

                if (impList.isImport("IV_REPL_STATE")) {
                    impList.setValue(IServerEngine.STAGING_TYPE_ACTIVE, "IV_REPL_STATE");
                }
            }

            // set the function input table
            JCO.Table nameServerTab = impList.getTable("IT_NAMESERVER");

            // fill name server table  
            fillTREXNameServerTable(nameServerTab);
            impList.setValue(nameServerTab, "IT_NAMESERVER");

            CatalogJCoLogHelper.logCall(function.getName(), function.getImportParameterList(), null,
                                        function.getTableParameterList(), log);

            connection.execute(function);

            CatalogJCoLogHelper.logCall(function.getName(), null, function.getExportParameterList(),
                                        function.getTableParameterList(), log);

            // get export parameters
            JCO.ParameterList expList = function.getExportParameterList();

            // analyse the fixed values of the attributes
            JCO.Table idxTab = expList.getTable("ET_RESULTLIST");

            // build a generic result table
            resTab = new Table("ET_RESULTLIST");
            resTab.addColumn(Table.TYPE_STRING, "TECHKEY_C");
            resTab.addColumn(Table.TYPE_STRING, "CATALOG_ID");
            resTab.addColumn(Table.TYPE_STRING, "CATALOG_GUID_C");
            resTab.addColumn(Table.TYPE_STRING, "VARIANT_ID");
            resTab.addColumn(Table.TYPE_STRING, "VARIANT_GUID_C");
            resTab.addColumn(Table.TYPE_STRING, "VERSION");
            resTab.addColumn(Table.TYPE_STRING, "REPL_STATE");
            resTab.addColumn(Table.TYPE_STRING, "REPL_TIME");
            resTab.addColumn(Table.TYPE_STRING, "REPL_TIME_D");
            resTab.addColumn(Table.TYPE_STRING, "REPL_TIME_T");
            resTab.addColumn(Table.TYPE_STRING, "STATE_MOD_TIME");
            resTab.addColumn(Table.TYPE_STRING, "STATE_MOD_TIME_D");
            resTab.addColumn(Table.TYPE_STRING, "STATE_MOD_TIME_T");
            resTab.addColumn(Table.TYPE_STRING, "LANGU");

            if (idxTab.getNumRows() != 0) {
                idxTab.firstRow();

                do {
                    TableRow resRow = resTab.insertRow();
                    resRow.getField(1).setValue(idxTab.getString("TECHKEY_C")); // The Guid of all Indizes belonging together
                    resRow.getField(2).setValue(idxTab.getString("CATALOG_ID"));
                    resRow.getField(3).setValue(idxTab.getString("CATALOG_GUID_C"));
                    resRow.getField(4).setValue(idxTab.getString("VARIANT_ID"));
                    resRow.getField(5).setValue(idxTab.getString("VARIANT_GUID_C"));
                    resRow.getField(6).setValue(idxTab.getString("VERSION"));
                    resRow.getField(7).setValue(idxTab.getString("REPL_STATE")); // replication state - productive, obsolete, ...
                    resRow.getField(8).setValue(idxTab.getString("REPL_TIME"));
                    resRow.getField(9).setValue(idxTab.getString("REPL_TIME_D"));
                    resRow.getField(10).setValue(idxTab.getString("REPL_TIME_T"));
                    resRow.getField(11).setValue(idxTab.getString("STATE_MOD_TIME"));
                    resRow.getField(12).setValue(idxTab.getString("STATE_MOD_TIME_D"));
                    resRow.getField(13).setValue(idxTab.getString("STATE_MOD_TIME_T"));
                    resRow.getField(14).setValue(idxTab.getString("LANGU"));
                }
                while (idxTab.nextRow());
            }
        }
        catch (BackendException ex) {
            throw new TrexCRMCatalogException(ex.getLocalizedMessage(), ex);
        }
        catch (JCO.AbapException ex) {
            throw new TrexCRMCatalogException(ex.getLocalizedMessage(), ex);
        }
        finally {
            if (connection != null) {
                connection.close();
            }
             // end of if ()
        }

        return new ResultData(resTab);
    }

    /**
     * Method to determine the staging catalogs replication timestamp
     *
     * @param connection connection to be used for retrieving the data
     * @param catalog the Catalog object, to determine the key for
     *
     * @return replTimeStamp the replication timestamp if determinable, null otherwise
     *
     * @throws TrexCRMCatalogException DOCUMENT ME!
     */
    static String getCatalogStagingReplTimeStamp(JCoConnection connection,
                                                 Catalog       catalog)
                                          throws TrexCRMCatalogException {
        log.entering("getCatalogStagingReplTimeStamp()");

        String replTimeStamp = "";

        if (log.isDebugEnabled()) {
            log.debug("Staging catalogue Replication Timestamp for : " + catalog.getName() + " with Staging Key : " +
                      catalog.getCatalogKey() + "  requested");
        }

        if ((catalog.getCatalogKey() == null) || (catalog.getCatalogKey().getIdAsString().trim().length() == 0)) {
            // catalog staging key is absolutely necessary
            log.error(LogUtil.APPS_BUSINESS_LOGIC, "cat.ims.err.invalidCatKey");
            throw new TrexCRMCatalogException("Catalog key is missing");
        }

        try {
            // get jco function
            JCO.Function function = connection.getJCoFunction("COM_PCAT_STAGINGVERSIONS_GET");

            // set import parameters
            JCO.ParameterList impList = function.getImportParameterList();
            impList.setValue(catalog.getCatalogKey().getIdAsString(), "IV_TECHKEY");

            CatalogJCoLogHelper.logCall(function.getName(), function.getImportParameterList(), null,
                                        function.getTableParameterList(), log);

            connection.execute(function);

            CatalogJCoLogHelper.logCall(function.getName(), null, function.getExportParameterList(),
                                        function.getTableParameterList(), log);

            // get export parameters
            JCO.ParameterList exportParams = function.getExportParameterList();

            JCO.Table stagingInfoJCOTable = exportParams.getTable("ET_RESULTLIST");

            int numEntries = stagingInfoJCOTable.getNumRows();

            String techKeyString;

            if (numEntries == 0) {
                log.error("No catalog found for the given TechKey, can't proceed");
            }
            else if (numEntries > 1) {
                log.error(MSG_4 + numEntries);
                throw new TrexCRMCatalogException(MSG_1 + numEntries + MSG_3 +
                                                  catalog.getCatalogKey().getIdAsString());
            }
            else {
                stagingInfoJCOTable.firstRow();
                replTimeStamp = stagingInfoJCOTable.getString("REPL_TIME");

                if (log.isDebugEnabled()) {
                    log.debug("Determined Replication Timestamp: " + replTimeStamp);
                }
            }
        }
        catch (BackendException ex) {
            log.error(ex.getMessage());
            throw new TrexCRMCatalogException(ex.getLocalizedMessage());
        }
        finally {
            if (connection != null) {
                connection.close();
            }
        }

        log.exiting();

        return replTimeStamp;
    }

    /**
     * Method to determine the catalogs key, created by the catalog staging functionality for active catalogs.
     *
     * @param connection connection to be used for retrieving the data
     * @param catalog the Catalog object, to determine the key for
     *
     * @return techKey the catalog key if determinable, null otherwise
     *
     * @throws TrexCRMCatalogException DOCUMENT ME!
     */
    static TechKey getCatalogStagingKey(JCoConnection connection,
                                        Catalog       catalog)
                                 throws TrexCRMCatalogException {
        log.entering("getCatalogStagingKey()");

        TechKey catalogKey = null;

        if (log.isDebugEnabled()) {
            log.debug("Catalog key for : " + catalog.getName() + ", client-catalog status:" +
                      catalog.getClient().getCatalogStatus() + " requested");
        }

        if (catalog.getClient().getCatalogStatus() == IClient.CATALOG_STATE_UNDEFINED) {
            // only active or inactive allowed
            log.error(LogUtil.APPS_BUSINESS_LOGIC, "cat.ims.err.invalidCatType");
            throw new TrexCRMCatalogException("Undefined catalog state requested");
        }
        else if (catalog.getClient().getCatalogStatus() == IClient.CATALOG_STATE_INACTIVE) {
            // this method is only intended to read the TechKey for active catalogues
            log.error(LogUtil.APPS_BUSINESS_LOGIC, "cat.ims.err.invalidCatStageType");
            throw new TrexCRMCatalogException("Staging catalog key requested for status inactive");
        }

        try {
            // get jco function
            JCO.Function function = connection.getJCoFunction("COM_PCAT_STAGINGVERSIONS_GET");

            // set import parameters
            JCO.ParameterList impList = function.getImportParameterList();
            impList.setValue(((TrexCRMCatalog) catalog).getName(), "IV_CATALOG_ID");
            impList.setValue(((TrexCRMCatalog) catalog).getVariant(), "IV_VARIANT_ID");
            // map external staging types to internal
            impList.setValue(IServerEngine.STAGING_TYPE_ACTIVE, "IV_REPL_STATE");

            // set the function input table
            JCO.Table nameServerTab = impList.getTable("IT_NAMESERVER");

            // fill name server table  
            fillTREXNameServerTable(nameServerTab);
            impList.setValue(nameServerTab, "IT_NAMESERVER");

            CatalogJCoLogHelper.logCall(function.getName(), function.getImportParameterList(), null,
                                        function.getTableParameterList(), log);

            connection.execute(function);

            CatalogJCoLogHelper.logCall(function.getName(), null, function.getExportParameterList(),
                                        function.getTableParameterList(), log);

            // get export parameters
            JCO.ParameterList exportParams = function.getExportParameterList();

            JCO.Table stagingInfoJCOTable = exportParams.getTable("ET_RESULTLIST");

            int numEntries = stagingInfoJCOTable.getNumRows();

            String catalogKeyString;

            if (numEntries == 0) {
                log.error("No catalog TechKeys found for the given criterias, can't proceed");
                throw new TrexCRMCatalogException(MSG_1 + " No catalog found with state active");
            }
            else if (numEntries > 1) {
                log.error(MSG_1 + numEntries + MSG_2);
                throw new TrexCRMCatalogException(MSG_1 + numEntries + MSG_2);
            }
            else {
                stagingInfoJCOTable.firstRow();
                catalogKeyString = stagingInfoJCOTable.getString("TECHKEY_C");

                if (log.isDebugEnabled()) {
                    log.debug("Determined Techkey for active catalog: " + catalogKeyString);
                }

                catalogKey = new TechKey(catalogKeyString);
            }
        }
        catch (BackendException ex) {
            log.error(ex.getMessage());
            throw new TrexCRMCatalogException(ex.getLocalizedMessage());
        }
        finally {
            if (connection != null) {
                connection.close();
            }
        }

        log.exiting();

        return catalogKey;
    }
    
    /**
     * Method to determine the catalogs key, created by the catalog staging functionality for active catalogs.
     *
     * @param connection connection to be used for retrieving the data
     * @param catalog the Catalog object, to determine the key for
     *
     * @return techKey the catalog key if determinable, null otherwise
     *
     * @throws TrexCRMCatalogException DOCUMENT ME!
     */
    static ExtendedTechKey getExtendedCatalogStagingKey(JCoConnection connection,
                                        Catalog       catalog)
                                 throws TrexCRMCatalogException {
        log.entering("getCatalogStagingKey()");

        ExtendedTechKey catalogKey = null;

        if (log.isDebugEnabled()) {
            log.debug("Catalog key for : " + catalog.getName() + ", client-catalog status:" +
                      catalog.getClient().getCatalogStatus() + " requested");
        }

        if (catalog.getClient().getCatalogStatus() == IClient.CATALOG_STATE_UNDEFINED) {
            // only active or inactive allowed
            log.error(LogUtil.APPS_BUSINESS_LOGIC, "cat.ims.err.invalidCatType");
            throw new TrexCRMCatalogException("Undefined catalog state requested");
        }
        else if (catalog.getClient().getCatalogStatus() == IClient.CATALOG_STATE_INACTIVE) {
            // this method is only intended to read the TechKey for active catalogues
            log.error(LogUtil.APPS_BUSINESS_LOGIC, "cat.ims.err.invalidCatStageType");
            throw new TrexCRMCatalogException("Staging catalog key requested for status inactive");
        }

        try {
            // get jco function
            JCO.Function function = connection.getJCoFunction("COM_PCAT_STAGINGVERSIONS_GET");

            // set import parameters
            JCO.ParameterList impList = function.getImportParameterList();
            impList.setValue(((TrexCRMCatalog) catalog).getName(), "IV_CATALOG_ID");
            impList.setValue(((TrexCRMCatalog) catalog).getVariant(), "IV_VARIANT_ID");
            // map external staging types to internal
            impList.setValue(IServerEngine.STAGING_TYPE_ACTIVE, "IV_REPL_STATE");

            // set the function input table
            JCO.Table nameServerTab = impList.getTable("IT_NAMESERVER");

            // fill name server table  
            fillTREXNameServerTable(nameServerTab);
            impList.setValue(nameServerTab, "IT_NAMESERVER");

            CatalogJCoLogHelper.logCall(function.getName(), function.getImportParameterList(), null,
                                        function.getTableParameterList(), log);

            connection.execute(function);

            CatalogJCoLogHelper.logCall(function.getName(), null, function.getExportParameterList(),
                                        function.getTableParameterList(), log);

            // get export parameters
            JCO.ParameterList exportParams = function.getExportParameterList();

            JCO.Table stagingInfoJCOTable = exportParams.getTable("ET_RESULTLIST");

            int numEntries = stagingInfoJCOTable.getNumRows();

            String catalogKeyString;

            if (numEntries == 0) {
                log.error("No catalog TechKeys found for the given criterias, can't proceed");
                throw new TrexCRMCatalogException(MSG_1 + " No catalog found with state active");
            }
            else if (numEntries > 1) {
                log.error(MSG_1 + numEntries + MSG_2);
                throw new TrexCRMCatalogException(MSG_1 + numEntries + MSG_2);
            }
            else {
                stagingInfoJCOTable.firstRow();
                catalogKeyString = stagingInfoJCOTable.getString("TECHKEY_C");
                
                String replTimeStamp = stagingInfoJCOTable.getString("REPL_TIME");
                
                if (log.isDebugEnabled()) {
                    log.debug("Determined Techkey for active catalog: " + catalogKeyString);
                }

                catalogKey = new ExtendedTechKey(new TechKey(catalogKeyString), replTimeStamp);
            }
        }
        catch (BackendException ex) {
            log.error(ex.getMessage());
            throw new TrexCRMCatalogException(ex.getLocalizedMessage());
        }
        finally {
            if (connection != null) {
                connection.close();
            }
        }

        log.exiting();

        return catalogKey;
    }
    /**
     * Helper method to get the category id for an index.
     *
     * @param connection connection to be used for retrieving the data
     * @param catName name of the catalog associated with the index
     * @param catVariant variant name of the catalog associated with the index
     * @param attrLevel index level to be read
     * @param area name of area (in case of 'S' index)
     * @param messResources texts to be used for the messages
     * @param catalog DOCUMENT ME!
     *
     * @return DOCUMENT ME!
     *
     * @exception BackendException connection error occured
     * @throws TrexCRMCatalogException DOCUMENT ME!
     */
    private static String getCatIdForLevel(JCoConnection    connection,
                                           String           catName,
                                           String           catVariant,
                                           String           attrLevel,
                                           String           area,
                                           MessageResources messResources,
                                           Catalog          catalog)
                                    throws BackendException, TrexCRMCatalogException {
        String method = "getCatIdForLevel()";
        String catalogKeyStr = null;

        if (catalog != null) {
            if (catalog.getCatalogKey() != null) {
                catalogKeyStr = catalog.getCatalogKey().getIdAsString();
            }

            if ((catalogKeyStr != null) && (catalogKeyStr.length() > 0)) {
                if (log.isDebugEnabled()) {
                    log.debug("catalogKey (filled from catalog field 'catalogKey'): " + catalogKeyStr);
                }
            }
            else {
                ResultData catalogTab = getStagingCatalogInfos(connection, catName, catVariant,
                                                               IServerEngine.STAGING_TYPE_ACTIVE, "");
                int rows = catalogTab.getNumRows();

                if (rows == 1) {
                    if (catalogTab.next()) {
                        catalogKeyStr = catalogTab.getString("TECHKEY_C").trim();
                    }
                }
                else if (rows > 1) {
                    String msg = messResources.getMessage(ICatalogMessagesKeys.PCAT_IMS_RET_2MANYCATS);
                    throw new BackendException(msg);
                }
                else if (rows < 1) {
                    String msg = messResources.getMessage(ICatalogMessagesKeys.PCAT_IMS_RET_NOCAT);
                    throw new BackendException(msg);
                }

                if (log.isDebugEnabled()) {
                    log.debug("catalogKey filled from CRM catalog: " + catalogKeyStr);
                }
            }
        }
        else {
            log.debug("catalog is null");
        }

        if (log.isDebugEnabled()) {
            log.debug("Get catId info for catalog variant (" + catName + ":" + catVariant + ":" + attrLevel + ":" +
                      area + ":" + catalogKeyStr + ")");
        }

        JCO.Function function = null;
        String catId = null;

        if ((catalogKeyStr != null) && (catalogKeyStr.length() > 0)) {
            if (log.isDebugEnabled()) {
                log.debug("Determine CATID via new STREX interface");
            }

            function = connection.getJCoFunction("COM_PCAT_STAGING_IDX_GET");

            // set import parameters
            JCO.ParameterList impList = function.getImportParameterList();
            impList.setValue(catalogKeyStr, "IV_TECHKEY");
            impList.setValue(attrLevel, "IV_ATTR_LEVEL");

            if (attrLevel.equals(ITrexCRMCatalogConstants.INDEX_LEVEL_S)) {
                if (area == null) {
                    String msg = messResources.getMessage(ICatalogMessagesKeys.PCAT_IMS_INVALID_LEVELINFO, attrLevel);
                    throw new BackendException(msg);
                }

                impList.setValue(catName, "IV_CATALOG_ID");
                impList.setValue(area, "IV_AREA_ID");
            }

            try {
                CatalogJCoLogHelper.logCall(function.getName(), function.getImportParameterList(), null,
                                            function.getTableParameterList(), log);

                connection.execute(function);

                CatalogJCoLogHelper.logCall(function.getName(), null, function.getExportParameterList(),
                                            function.getTableParameterList(), log);
            }
            catch (JCO.AbapException ex) {
                String msg = "getCatIdForLevel(catName=" + catName + ",catVariant=" + catVariant + ",attribLevel=" +
                             attrLevel + ",area=" + area + ") failed with JOC.AbapException '" +
                             ex.getLocalizedMessage() + "'";
                log.error(ICatalogMessagesKeys.PCAT_EXCEPTION, new Object[] { msg }, null);
                throw new TrexCRMCatalogException(ex.getLocalizedMessage());
            }

            // get export parameters
            JCO.ParameterList expList = function.getExportParameterList();

            // analyse the fixed values of the attributes
            JCO.Table etIdxTab = expList.getTable("ET_RESULTLIST");

            int rows = etIdxTab.getNumRows();

            if (rows > 1) {
                String msg = messResources.getMessage(ICatalogMessagesKeys.PCAT_IMS_RET_2MANYCATS);
                throw new BackendException(msg);
            }
            else if (rows < 1) {
                String msg = messResources.getMessage(ICatalogMessagesKeys.PCAT_IMS_RET_NOCAT);
                throw new BackendException(msg);
            }

            // rows == 1
            etIdxTab.firstRow();
            catId = etIdxTab.getString("CAT_IDX");
        }

        if (log.isDebugEnabled()) {
            log.debug("catId=" + catId + " retrieved");
        }

        return catId;
    }

    /**
     * Gets the pricing meta information. Unfortunately the meta infos are stored in the CRM system and not on the IMS
     * server.
     *
     * @param connection connection to be used for retrieving the data
     * @param catName the catalog name
     * @param catVariant the variant name of the catalog
     *
     * @return the pricing information from the CRM system
     *
     * @exception TrexCRMCatalogException connection error occured
     */
    static ResultData getPricingMetaInfo(JCoConnection connection,
                                         String        catName,
                                         String        catVariant)
                                  throws TrexCRMCatalogException {
        Table resTab = null;

        try {
            // get jco function
            JCO.Function function = connection.getJCoFunction("CRM_ISA_PRICESETTINGS_READ");

            // set exporting parameters
            JCO.ParameterList impList = function.getImportParameterList();
            impList.setValue(catName, "CATALOG");
            impList.setValue(catVariant, "VARIANT");
            //impList.setValue("X", "LISTPRICE_ONLY");
            impList.setValue(" ", "LISTPRICE_ONLY");

            CatalogJCoLogHelper.logCall(function.getName(), function.getImportParameterList(), null,
                                        function.getTableParameterList(), log);

            connection.execute(function);

            CatalogJCoLogHelper.logCall(function.getName(), null, function.getExportParameterList(),
                                        function.getTableParameterList(), log);

            // get table parameters
            JCO.ParameterList tabList = function.getTableParameterList();

            // analyse the fixed values of the attributes
            JCO.Table attrTab = tabList.getTable("PRICEATTRNAMES");

            // build a generic result table
            resTab = new Table("PRICEATTRNAMES");
            resTab.addColumn(Table.TYPE_STRING, "ATTRNAME_PRICE");
            resTab.addColumn(Table.TYPE_STRING, "ATTRNAME_QUANTITY");
            resTab.addColumn(Table.TYPE_STRING, "ATTRNAME_CURRENCY");
            resTab.addColumn(Table.TYPE_STRING, "ATTRNAME_UOM");
            resTab.addColumn(Table.TYPE_STRING, "ATTRNAME_SCALETYPE");
            resTab.addColumn(Table.TYPE_STRING, "ATTRIBUTE_PRICETYPE");
            resTab.addColumn(Table.TYPE_STRING, "ATTRIBUTE_PROMOTION_ATTR");

            if (attrTab.getNumRows() != 0) {
                attrTab.firstRow();

                do {
                    TableRow resRow = resTab.insertRow();
                    resRow.getField(1).setValue(attrTab.getString("ATTRNAME_PRICE"));
                    resRow.getField(2).setValue(attrTab.getString("ATTRNAME_QUANTITY"));
                    resRow.getField(3).setValue(attrTab.getString("ATTRNAME_CURRENCY"));
                    resRow.getField(4).setValue(attrTab.getString("ATTRNAME_UOM"));
                    resRow.getField(5).setValue(attrTab.getString("ATTRNAME_SCALETYPE"));
                    resRow.getField(6).setValue(attrTab.getString("PRCTYPE"));
                    resRow.getField(7).setValue(attrTab.getString("PROMOTION_ATTR"));
                }
                while (attrTab.nextRow());
            }
        }
        catch (BackendException ex) {
            throw new TrexCRMCatalogException(ex.getLocalizedMessage());
        }
        catch (JCO.AbapException ex) {
            throw new TrexCRMCatalogException(ex.getLocalizedMessage());
        }
        finally {
            if (connection != null) {
                connection.close();
            }
        }

        return new ResultData(resTab);
    }

    /**
     * Gets a list of StagingCatalogInfo objects, containing informations for all available staging catalogs for the
     * given catalog
     *
     * @param connection connection to be used for retrieving the data
     * @param catName name of the catalog associated with the index
     * @param catVariant variant name of the catalog associated with the index
     * @param stagingType type of the staging (active, inactive)
     * @param country to determine the right date format
     *
     * @return the list of inactive catalog infos
     *
     * @exception TrexCRMCatalogException connection error occured
     */
    static ResultData getStagingCatalogInfos(JCoConnection connection,
                                             String        catName,
                                             String        catVariant,
                                             String        stagingType,
                                             String        country)
                                      throws TrexCRMCatalogException {
        log.entering("getStagingCatalogInfos()");

        ResultData catalogInfos = null;

        if (log.isDebugEnabled()) {
            log.debug("Staging infos for catalog: " + catName + ", variant:" + catVariant + ", stagingType:" +
                      stagingType + ", country:" + country + " requested");
        }

        try {
            // get jco function
            JCO.Function function = connection.getJCoFunction("COM_PCAT_STAGINGVERSIONS_GET");

            // set import parameters
            JCO.ParameterList impList = function.getImportParameterList();
            impList.setValue(catName, "IV_CATALOG_ID");
            impList.setValue(catVariant, "IV_VARIANT_ID");

            if ((stagingType != null) && (stagingType.trim().length() > 0)) {
                impList.setValue(stagingType, "IV_REPL_STATE");
            }

            if ((country != null) && (country.length() > 0)) {
                impList.setValue(country, "IV_COUNTRY");
            }

            // set the function input table
            JCO.Table nameServerTab = impList.getTable("IT_NAMESERVER");

            // fill name server table  
            fillTREXNameServerTable(nameServerTab);
            impList.setValue(nameServerTab, "IT_NAMESERVER");

            CatalogJCoLogHelper.logCall(function.getName(), function.getImportParameterList(), null,
                                        function.getTableParameterList(), log);

            connection.execute(function);

            CatalogJCoLogHelper.logCall(function.getName(), null, function.getExportParameterList(),
                                        function.getTableParameterList(), log);

            // get table parameters
            JCO.ParameterList exportParams = function.getExportParameterList();

            JCO.Table stagingInfoJCOTable = exportParams.getTable("ET_RESULTLIST");

            Table stagingInfTable = new Table("stagingInfos");

            stagingInfTable.addColumn(Table.TYPE_STRING, "TECHKEY_C"); // The Guid of all Indizes belonging together
            stagingInfTable.addColumn(Table.TYPE_STRING, "CATALOG_ID");
            stagingInfTable.addColumn(Table.TYPE_STRING, "VERSION");
            stagingInfTable.addColumn(Table.TYPE_STRING, "REPL_STATE"); // replication state - productive, obsolete, ...
            stagingInfTable.addColumn(Table.TYPE_STRING, "REPL_TIME_IO");
            stagingInfTable.addColumn(Table.TYPE_STRING, "REPL_TIME_T");
            stagingInfTable.addColumn(Table.TYPE_STRING, "REPL_BY");

            int numEntries = stagingInfoJCOTable.getNumRows();
            String replState = "";

            for (int i = 0; i < numEntries; i++) {
                replState = stagingInfoJCOTable.getString("REPL_STATE");

                if ((replState != null) && !replState.equals(IServerEngine.STAGING_TYPE_ERROR)) {
                    TableRow row = stagingInfTable.insertRow();
                    row.setValue("TECHKEY_C", stagingInfoJCOTable.getString("TECHKEY_C"));
                    row.setValue("CATALOG_ID", stagingInfoJCOTable.getString("CATALOG_ID"));
                    row.setValue("VERSION", stagingInfoJCOTable.getString("VERSION"));
                    row.setValue("REPL_STATE", stagingInfoJCOTable.getString("REPL_STATE"));
                    row.setValue("REPL_TIME_IO", stagingInfoJCOTable.getString("REPL_TIME_IO"));
                    row.setValue("REPL_TIME_T", stagingInfoJCOTable.getString("REPL_TIME_T"));
                    row.setValue("REPL_BY", stagingInfoJCOTable.getString("REPL_BY"));
                }

                stagingInfoJCOTable.nextRow();
            }

            catalogInfos = new ResultData(stagingInfTable);
        }
        catch (BackendException ex) {
            throw new TrexCRMCatalogException(ex.getLocalizedMessage());
        }
        finally {
            if (connection != null) {
                connection.close();
            }
        }

        log.exiting();

        return catalogInfos;
    }

    /**
     * Gets the StagingCatalogInfo object for the given catalog
     *
     * @param connection connection to be used for retrieving the data
     * @param ivTechKey TechKey of the catalog
     *
     * @return a catalog info
     *
     * @exception TrexCRMCatalogException connection error occured
     */
    public static ResultData getStagingCatalogInfo(JCoConnection connection,
                                                   String        ivTechKey)
                                            throws TrexCRMCatalogException {
        log.entering("getStagingCatalogInfos()");

        ResultData catalogInfo = null;

        if (log.isDebugEnabled()) {
            log.debug("Catalog - TechKey: " + ivTechKey);
        }

        try {
            // get jco function
            JCO.Function function = connection.getJCoFunction("COM_PCAT_STAGINGVERSIONS_GET");

            // set import parameters
            JCO.ParameterList impList = function.getImportParameterList();
            impList.setValue(ivTechKey, "IV_TECHKEY");

            // set the function input table
            JCO.Table nameServerTab = impList.getTable("IT_NAMESERVER");

            // fill name server table  
            fillTREXNameServerTable(nameServerTab);
            impList.setValue(nameServerTab, "IT_NAMESERVER");

            CatalogJCoLogHelper.logCall(function.getName(), function.getImportParameterList(), null,
                                        function.getTableParameterList(), log);

            connection.execute(function);

            CatalogJCoLogHelper.logCall(function.getName(), null, function.getExportParameterList(),
                                        function.getTableParameterList(), log);

            // get export parameters
            JCO.ParameterList exportParams = function.getExportParameterList();

            JCO.Table stagingInfoJCOTable = exportParams.getTable("ET_RESULTLIST");

            Table stagingInfTable = new Table("stagingInfos");

            stagingInfTable.addColumn(Table.TYPE_STRING, "REPL_TIME");
            stagingInfTable.addColumn(Table.TYPE_STRING, "LANGU");

            int numEntries = stagingInfoJCOTable.getNumRows();

            if (numEntries == 0) {
                log.error("No catalog found for the given TechKey, can't proceed");
            }
            else if (numEntries > 1) {
                log.error(MSG_4 + numEntries);
                throw new TrexCRMCatalogException(MSG_1 + numEntries + MSG_3 +
                                                  ivTechKey);
            }
            else {
                stagingInfoJCOTable.firstRow();

                TableRow row = stagingInfTable.insertRow();
                row.setValue("REPL_TIME", stagingInfoJCOTable.getString("REPL_TIME"));
                row.setValue("LANGU", stagingInfoJCOTable.getString("LANGU"));
            }

            catalogInfo = new ResultData(stagingInfTable);
        }
        catch (BackendException ex) {
            log.error(ex.getMessage());
            throw new TrexCRMCatalogException(ex.getLocalizedMessage());
        }
        finally {
            if (connection != null) {
                connection.close();
            }
        }

        log.exiting();

        return catalogInfo;
    }

    /**
     * Gets the detailed information about an index.
     *
     * @param connection connection to be used for retrieving the data
     * @param catName name of the catalog associated with the index
     * @param catVariant variant name of the catalog associated with the index
     * @param catLanguage language of the catalog associated with the index
     * @param attrLevel index level to be read
     * @param area name of area (in case of 'S' index)
     * @param guid guid of the catalog (if 'P' index) or of the area (if 'S' index)
     * @param messResources texts to be used for the messages
     *
     * @return an information about the index
     *
     * @exception TrexCRMCatalogException connection error occured
     *
     * @deprecated please use getIMSIndexInfo(JCoConnection connection, String catName, String catVariant, String
     *             catLanguage, String attrLevel, String area, String guid, MessageResources messResources, Catalog
     *             catalog)
     */
    static TrexCRMCatalogIndexInfo getIndexInfo(JCoConnection    connection,
                                                String           catName,
                                                String           catVariant,
                                                String           catLanguage,
                                                String           attrLevel,
                                                String           area,
                                                String           guid,
                                                MessageResources messResources)
                                         throws TrexCRMCatalogException {
        return getIndexInfo(connection, catName, catVariant, catLanguage, attrLevel, area, guid, messResources, null);
    }

    /**
     * Gets the detailed information about an index.
     *
     * @param connection connection to be used for retrieving the data
     * @param catName name of the catalog associated with the index
     * @param catVariant variant name of the catalog associated with the index
     * @param catLanguage language of the catalog associated with the index
     * @param attrLevel index level to be read
     * @param area name of area (in case of 'S' index)
     * @param guid guid of the catalog (if 'P' index) or of the area (if 'S' index)
     * @param messResources texts to be used for the messages
     * @param catalog DOCUMENT ME!
     *
     * @return an information about the index
     *
     * @exception TrexCRMCatalogException connection error occured
     */
    static TrexCRMCatalogIndexInfo getIndexInfo(JCoConnection    connection,
                                                String           catName,
                                                String           catVariant,
                                                String           catLanguage,
                                                String           attrLevel,
                                                String           area,
                                                String           guid,
                                                MessageResources messResources,
                                                Catalog          catalog)
                                         throws TrexCRMCatalogException {
        TrexCRMCatalogIndexInfo indexInfo = null;

        try {
            // get index category ID
            String catId = getCatIdForLevel(connection, catName, catVariant, attrLevel, area, messResources, catalog);

            //            String language = catLanguage;
            AdminIndex idxInfo = getTrexIndex(catId);

            //            if (attrLevel != null && attrLevel.equals("A")) {
            //               language = idxInfo.getFirstLanguage();
            //            }
            
            if (idxInfo != null) {
                // create index info
                indexInfo = new TrexCRMCatalogIndexInfo(catId, attrLevel, area, idxInfo);

                // create the attribute meta information for the info object
                //            getAttributeMetaInfo(connection, indexInfo, guid, language);
                getAttributeMetaInfo(connection, indexInfo, guid, catLanguage);
            }
            else {
                String msg = messResources.getMessage(ICatalogMessagesKeys.PCAT_ERR_TREX_INDEX_CORRUPTED, catId);
                log.error(ICatalogMessagesKeys.PCAT_ERR_TREX_INDEX_CORRUPTED, new Object[] { msg }, null);
                throw new TrexCRMCatalogException(msg);

            }
        }
        catch (BackendException ex) {
            throw new TrexCRMCatalogException(ex.getLocalizedMessage());
        }

        return indexInfo;
    }

    /**
     * Gets the detailed information about an index.
     *
     * @param connection connection to be used for retrieving the data
     * @param catName name of the catalog associated with the index
     * @param catVariant variant name of the catalog associated with the index
     * @param catLanguage language of the catalog associated with the index
     * @param attrLevel index level to be read
     * @param area name of area (in case of 'S' index)
     * @param guid guid of the catalog (if 'P' index) or of the area (if 'S' index)
     * @param messResources texts to be used for the messages
     * @param catalog DOCUMENT ME!
     *
     * @return an information about the index
     *
     * @exception TrexCRMCatalogException connection error occured
     */
    static TrexCRMCatalogIndexInfo getIndexInfo(JCoConnection    connection,
                                                String           catId,
                                                MessageResources messResources,
                                                String attrLevel,
                                                String language)
                                         throws TrexCRMCatalogException {
        TrexCRMCatalogIndexInfo indexInfo = null;

        try {
            //            String language = catLanguage;
            AdminIndex idxInfo = getTrexIndex(catId);

            //            if (attrLevel != null && attrLevel.equals("A")) {
            //               language = idxInfo.getFirstLanguage();
            //            }
            // create index info
            indexInfo = new TrexCRMCatalogIndexInfo(catId, attrLevel, "<empty>", idxInfo);

            // create the attribute meta information for the info object
            //            getAttributeMetaInfo(connection, indexInfo, guid, language);
            getAttributeMetaInfo(connection, indexInfo, catId, language);
        }
        catch (BackendException ex) {
            throw new TrexCRMCatalogException(ex.getLocalizedMessage());
        }

        return indexInfo;
    }

    /**
     * Helper method to get the meta information for the attributes. Unfortunately the meta infos are stored in the CRM
     * system and not on the Trex server.
     *
     * @param connection connection to be used for retrieving the data
     * @param info the index info object of the attributes
     * @param guid guid of the catalog (if 'P' index) or of the area (if 'S' index)
     * @param language the language for the language dependant texts
     *
     * @exception BackendException connection error occured
     */
    private static void getAttributeMetaInfo(JCoConnection           connection,
                                             TrexCRMCatalogIndexInfo info,
                                             String                  guid,
                                             String                  language)
                                      throws BackendException {
        // currently only supported for the 'S' and 'P' index
        if (info.getAttrLevel().equals(ITrexCRMCatalogConstants.INDEX_LEVEL_A)) {
            return;
        }

        // get jco function
        JCO.Function function = connection.getJCoFunction("COM_PCAT_LOC_GETENTRYMASK_O");

        // set exporting parameters
        JCO.ParameterList impList = function.getImportParameterList();

        if (info.getAttrLevel().equals(ITrexCRMCatalogConstants.INDEX_LEVEL_P)) {
            // check first if the release support this behavoir
            if (!impList.isImport("IV_CATALOG_ID")) {
                return;
            }

            // then set the value - use id instead of guid
            impList.setValue(guid, "IV_CATALOG_ID");
            impList.setValue(language, "IV_LANGUAGE");
        }
        else if (info.getAttrLevel().equals(ITrexCRMCatalogConstants.INDEX_LEVEL_S)) {
            impList.setValue(guid, "IV_AREA_GUID");
            impList.setValue(language, "IV_LANGUAGE");
        }

        impList.setValue(" ", "IV_VERSIONTYPE"); // don't bother active/non-active version

        CatalogJCoLogHelper.logCall(function.getName(), function.getImportParameterList(), null,
                                    function.getTableParameterList(), log);

        connection.execute(function);

        CatalogJCoLogHelper.logCall(function.getName(), null, function.getExportParameterList(),
                                    function.getTableParameterList(), log);

        // get table parameters
        JCO.ParameterList tabList = function.getTableParameterList();

        // first check if error occured
        JCO.Table returnInfo = tabList.getTable("ET_RETURN");

        if (returnInfo.getNumRows() != 0) {
            // iterate over the return codes of the function call
            returnInfo.firstRow();

            do {
                String messType = returnInfo.getString("TYPE").trim();

                if (messType.equalsIgnoreCase("E") || messType.equalsIgnoreCase("A")) {
                    log.error(ICatalogMessagesKeys.PCAT_EXCEPTION, new Object[] { returnInfo.getString("MESSAGE") },
                              null);
                    throw new BackendException(returnInfo.getString("MESSAGE").trim());
                }
            }
            while (returnInfo.nextRow());
        }

        // analyse the meta information of the attributes
        Map guidMap = new HashMap();
        JCO.Table metaTab = null;

        if (info.getAttrLevel().equals(ITrexCRMCatalogConstants.INDEX_LEVEL_P)) {
            metaTab = tabList.getTable("ET_BASE_CIC");
        }
        else {
            metaTab = tabList.getTable("ET_AREA_CIC");
        }

        if (metaTab.getNumRows() != 0) {
            metaTab.firstRow();

            do {
                String attrName = metaTab.getString("CIC_ID").trim();
                String attrGuid = metaTab.getString("CIC_GUID").trim();
                String attrDesc = metaTab.getString("CIC_NAME").trim();
                String attrStyle = metaTab.getString("CIC_ENTRY_STYLE").trim();
                String attrPos = metaTab.getString("CIC_NR").trim();
                String attrType = metaTab.getString("DATA_TYPE").trim();
                info.addAttributeProperty(attrName, TrexCRMCatalogIndexInfo.PROP_GUID, attrGuid);
                info.addAttributeProperty(attrName, TrexCRMCatalogIndexInfo.PROP_DESCRIPTION, attrDesc);
                info.addAttributeProperty(attrName, TrexCRMCatalogIndexInfo.PROP_ENTRYSTYLE, attrStyle);
                info.addAttributeProperty(attrName, TrexCRMCatalogIndexInfo.PROP_POS_NR, attrPos);
                info.addAttributeProperty(attrName, TrexCRMCatalogIndexInfo.PROP_TYPE, attrType);
                guidMap.put(attrGuid, attrName);
            }
            while (metaTab.nextRow());
        }

        // analyse the fixed values of the attributes
        JCO.Table valueTab = tabList.getTable("ET_CIC_PROP");

        if (valueTab.getNumRows() != 0) {
            valueTab.firstRow();

            do {
                String attrGuid = valueTab.getString("CIC_GUID").trim();
                String attrName = (String) guidMap.get(attrGuid);

                // check if this attribute really exists
                if ((attrName == null) || (attrName.length() == 0)) {
                    continue;
                }

                // language dependent value of the fixed value for this attribute
                String attrValueDesc = valueTab.getString("ATTR_VAL_TEXT").trim();

                // technical value of the fixed value for this attribute
                String attrValueName = valueTab.getString("ATTR_VAL_LOW").trim();
                info.addFixedValue(attrName, attrValueName, attrValueDesc);
            }
            while (valueTab.nextRow());
        }
    }

    /**
     * Gets a list of indices out of CRM for a given catalog instance  Necessary for distributed "index
     * server"-landscape in order to get actual indices after replication to the web.
     *
     * @param connection connection to be used for retrieving the data
     * @param catalog the Catalog Object
     *
     * @return a list of indices (GUIDs) of the actual Catalog Object
     *
     * @exception TrexCRMCatalogException connection error occured
     */
    static String[] getListOfIndexGuids(JCoConnection  connection,
                                        TrexCRMCatalog catalog)
                                 throws TrexCRMCatalogException {
        final String METHOD = "getListOfIndexGuids";
        String result[] = null;
        String catalog_id = catalog.getName();
        String variant_id = catalog.getVariant();

        if (log.isDebugEnabled()) {
            log.entering(METHOD);
            log.debug(METHOD + " for catalog_variant (" + catalog_id + variant_id + ")");
        }

        try {
            ResultData catalogTab = getStagingCatalogInfos(connection, catalog_id, variant_id,
                                                           IServerEngine.STAGING_TYPE_ACTIVE, "");

            if (catalogTab.getNumRows() != 0) {
                int i = 0;

                while (catalogTab.next()) {
                    String attrGuid = catalogTab.getString("TECHKEY_C").trim();
                    result[i] = attrGuid;
                    i++;
                }
            }

            if (log.isDebugEnabled()) {
                log.debug(METHOD + " returns following " + result.length + " entries: ");

                for (int i = 0; i < result.length; i++) {
                    log.debug("result [" + i + "]: " + result[i]);
                }
            }
        }
        catch (JCO.AbapException ex) {
            throw new TrexCRMCatalogException(ex.getLocalizedMessage(), ex);
        }
        finally {
            if (connection != null) {
                connection.close();
            }
        }

        return result;
    }

    /**
     * Method to check if catalog staging is supported by the backend
     *
     * @param connection connection to be used for retrieving the data
     *
     * @return type of staging which is supported (values 'A', 'B', 'C', 'D' supported)
     *
     * @throws TrexCRMCatalogException Exception
     */
    public static String getStagingSupport(JCoConnection connection)
                                    throws TrexCRMCatalogException {
        String funcName = "COM_PCAT_REPLTYPE_GET";

        log.entering("isStagingSupported(JCoConnection connection)");

        String stagingType = IServerEngine.STAGING_NOT_SUPPORTED;

        if (connection.isJCoFunctionAvailable(funcName)) {
            try {
                // get jco function via a JCo client connection
                JCO.Function function = connection.getJCoFunction(funcName);

                CatalogJCoLogHelper.logCall(function.getName(), null, null, null, log);

                connection.execute(function);

                CatalogJCoLogHelper.logCall(function.getName(), null, function.getExportParameterList(), null, log);

                // get table parameters (export of backend) in order to accept result
                JCO.ParameterList exportParams = function.getExportParameterList();

                stagingType = exportParams.getString("EV_REPLTYPE");
            }
            catch (BackendException ex) {
                String msg = "isStagingSupported() failed with BackendException '" + ex.getLocalizedMessage() + "'";
                log.error(ICatalogMessagesKeys.PCAT_EXCEPTION, new Object[] { ex.getLocalizedMessage() }, null);
                throw new TrexCRMCatalogException(ex.getLocalizedMessage(), ex);
            }
            catch (JCO.AbapException ex) {
                throw new TrexCRMCatalogException(ex.getLocalizedMessage(), ex);
            }
            finally {
                if (connection != null) {
                    connection.close();
                }
            }
        }
        else {
            if (log.isDebugEnabled()) {
                log.debug("Function " + funcName + " is not known, so staging is not supported");
            }
        }

        if (log.isDebugEnabled()) {
            log.debug("Staging type = " + stagingType);
        }

        log.exiting();

        return stagingType;
    }

    /**
     * The method returns a list of all TREX name servers.  This result list is analog to the result of the call of the
     * ABAP function module  TREX_EXT_SHOW_SERVERS and can be used for the comparison. All name servers are returned
     * independent if they are active or not.
     *
     * @param nameServerTab of type JCO.Table will be filled with a list of name servers in the    format:
     *        'pwdf2832:30001'
     */
    private static void fillTREXNameServerTable(JCO.Table nameServerTab) {
        final String METHOD = "fillTREXNameServerTable";
        log.entering(METHOD);

        String nameServer = null;
        ServerInfoList siList = null;
        int i = 0;

        // Look for the Nameserver in the cache
        CacheableNameServerData nsCache = (CacheableNameServerData) CacheManager.get(NAMESERVER_CACHE_NAME, "1");

        // If NameServers were not cached read from backend
        if (nsCache == null) {

            AdminManager adm = TrexFactory.getAdminManager();

            try {
                siList = adm.showServers(AdminManager.SERVER_TYPE_NAME_SERVER, false);
    
                // create new container for cached data
                nsCache = new CacheableNameServerData();

                // Store reference to the name server list
                nsCache.nsList = siList;

                // write name server list into cache
                CacheManager.put(NAMESERVER_CACHE_NAME, "1", nsCache);
                CacheManager.setExpirationTimeForCache(NAMESERVER_CACHE_NAME, 1);

            }
            catch (TrexException trEx) {
                log.error("getNameServers() exception caught: " + trEx.toString());
            }
            catch (Exception ex) {
                log.error("getNameServers() exception caught: " + ex.toString());
            }
        }
        else {
            siList = nsCache.nsList;  
        }

        ServerInfo servInfo = siList.getFirstServerInfo();
    
        while (servInfo != null) {
            nameServer = servInfo.getServerLocation().getHost() + ":" + servInfo.getServerLocation().getPort();
            // fill table 
            nameServerTab.appendRow();
            nameServerTab.setValue(nameServer, "NAME");
    
            if (log.isDebugEnabled()) {
                log.debug(METHOD + ": NameServer = " + nameServer);
            }
    
            servInfo = siList.getNextServerInfo();
        }
        
        log.exiting();
    }

    /**
     * Read index info from TREX
     *
     * @param indexName name of the index
     *
     * @return idxInfo of type AdminIndex
     */
    public static AdminIndex getTrexIndex(String indexName) {
        final String METHOD = "getTrexInfo()";
        log.entering(METHOD);

        if (log.isDebugEnabled()) {
            log.debug(METHOD + ": indexName=" + indexName);
        }

        AdminIndex idxInfo = null;

        try {
            AdminManager admin = TrexFactory.getAdminManager();
            admin.setIndexId(indexName);
            idxInfo = admin.showIndex();

            if (log.isDebugEnabled()) {
                log.debug("Index Id: " + idxInfo.getIndexId());

                for (int j = 0; j < idxInfo.countLanguages(); j++) {
                    if (j == 0) {
                        log.debug("Laiso: " + (String) (idxInfo.getFirstLanguage()));
                    }
                    else {
                        log.debug("Laiso: " + (String) (idxInfo.getNextLanguage()));
                    }
                }

                log.debug("Description: " + idxInfo.getDescription());

                for (int j = 0; j < idxInfo.countDocumentAttribute(); j++) {
                    if (j == 0) {
                        log.debug("doc name: " + idxInfo.getFirstDocumentAttribute().getName());
                    }
                    else {
                        log.debug("doc name: " + idxInfo.getNextDocumentAttribute().getName());
                    }
                }
            }
        }
        catch (TrexException trEx) {
            log.error("getTrexIndex() exception caught: " + trEx.toString());
        }

        log.exiting();

        return idxInfo;
    }

    /* Private class to store data for the TREX Name server */
    private static class CacheableNameServerData {
        public ServerInfoList nsList;
    }

}
