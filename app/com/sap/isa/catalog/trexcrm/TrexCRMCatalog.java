/*****************************************************************************
    Class:        TrexCRMCatalog
    Copyright (c) 2006, SAP AG, All rights reserved.
    Author:
    Created:      15.09.2006
    Version:      1.0

    $Revision$
    $Date$
*****************************************************************************/
package com.sap.isa.catalog.trexcrm;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Properties;

import org.apache.struts.util.MessageResources;

import com.sap.isa.catalog.CatalogParameters;
import com.sap.isa.catalog.ICatalogMessagesKeys;
import com.sap.isa.catalog.boi.IClient;
import com.sap.isa.catalog.boi.IComponent;
import com.sap.isa.catalog.boi.IServer;
import com.sap.isa.catalog.boi.IServerEngine;
import com.sap.isa.catalog.boi.IView;
import com.sap.isa.catalog.trexgen.TrexGenCatalog;
import com.sap.isa.core.eai.BackendBusinessObjectMetaData;
import com.sap.isa.core.eai.Connection;
import com.sap.isa.core.eai.ConnectionDefinition;
import com.sap.isa.core.eai.sp.jco.JCoConnection;
import com.sap.isa.core.eai.sp.jco.JCoManagedConnectionFactory;
import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.core.util.table.ResultData;
import com.sapportals.trex.TrexException;
import com.sapportals.trex.TrexReturn;
import com.sapportals.trex.core.QueryEntry;
import com.sapportals.trex.core.SearchManager;
import com.sapportals.trex.core.SearchResult;

/*
 * Catalog uses STREX interface
 *
 */
public class TrexCRMCatalog
    extends TrexGenCatalog
{
    // the logging instance
    private static IsaLocation log =
                   IsaLocation.getInstance(TrexCRMCatalog.class.getName());

    // IMS infos
    private String theLanguage;
//    private String theCodepage;
    private String theSearchEngine;

        // Catalog infos
    private String[] theViews;
    private String   theVariant;
    private HashMap theIndexInfo = new HashMap();

    // Connection infos
//    private TrexCRMConnectionInfo theIMSConnectionInfo;
    private transient JCoConnection theSAPConnection;
    private Properties theConnectionProperties = new Properties();

        /**
         * Creates a new Trex CRM catalog instance. The connections are managed by the
         * EAI framework. This constructor is called from outside of the <code>Trexcrm</code>
         * package. Therefore it is defined as <code>public</code>.
         *
         * @param aServerEngine TrexCRMCatalogServerEngine
         * @param aCatalogGuid, Catalog guid
         * @param aClient, client
         *
         * @exception TrexCRMCatalogException  error from the IMS proxy
         *  
         */
    protected TrexCRMCatalog(TrexCRMCatalogServerEngine aServerEngine,
                      IClient aClient,
                      String aCatalogGuid,
                      String aSearchEngine,
                      MessageResources aMessageResource)
        throws TrexCRMCatalogException
    {
        super(aServerEngine, aClient, aCatalogGuid, aMessageResource);
        log.entering("TrexCRMCatalog(aServerEngine, aClient, aCatalogGuid, aSearchEngine, aIndexId, aMessageResource)");
        
            // create corresponding builder
        setCatalogBuilder(new TrexCRMCatalogBuilder(this));
        this.setNameInternal(aCatalogGuid.substring(0, 29));
        this.theVariant = aCatalogGuid.substring(30);
        this.theSearchEngine = aSearchEngine;
                 
        // change the default connection if necessary
        this.setDefaultConnection(theClient, theServerEngine);
        
        // always STREX and new staging functionality is used 
            
        if (aClient.getStagingCatalogKey() != null && 
            aClient.getStagingCatalogKey().getIdAsString().length() > 0)  {
            // this should be the case, when catalogs of status inactive are requested
            catalogKey = aClient.getStagingCatalogKey();
            if (log.isDebugEnabled()) { 
                log.debug("Catalog key set via the Client to : " + catalogKey.getIdAsString());
            }
        }
        else {
            catalogKey = TrexCRMRfcMethods.getCatalogStagingKey(getSAPConnection(), this);
        }
 
        // determine the language
        ResultData catalogInfo = TrexCRMRfcMethods.getStagingCatalogInfo(getSAPConnection(), catalogKey.getIdAsString());
        
        catalogInfo.first();    
        this.theLanguage = catalogInfo.getString("LANGU");
        String replTimeStamp = catalogInfo.getString("REPL_TIME");
        if (theLanguage != null) {
            this.theDefaultLocale = new Locale(theLanguage.toLowerCase(),"");
            this.theSupportedLocales.add(theDefaultLocale);
        }

//        setReplTimeStamp(TrexCRMRfcMethods.getCatalogStagingReplTimeStamp(getSAPConnection(), this));
        setReplTimeStamp(replTimeStamp);
        
        StringBuffer desc =
            new StringBuffer(getName()).append(" - ").append(getVariant());
        this.setDescriptionInternal(desc.toString());
        
        if (log.isDebugEnabled()) {
            String aMessage =
                theMessageResources.getMessage(
                    ICatalogMessagesKeys.PCAT_INIT_CATALOG,
                    this.getClass().getName(),
                    this.getGuid(),
                    this.getClient().toString());
            log.debug(aMessage);
        } // end of if ()

        log.exiting();

        return ;
     }

        /**
         * Creates a new Trex CRM catalog instance.
         * Allows the setting of the maximum number of items per category.
         */
    protected TrexCRMCatalog(TrexCRMCatalogServerEngine aServerEngine,
                      IClient aClient,
                      String aCatalogGuid,
                      String aSearchEngine,
                      MessageResources aMessageResource,
                      int aMaxNumberOfItems)
        throws TrexCRMCatalogException        
    {
        this(aServerEngine,
             aClient,
             aCatalogGuid,
             aSearchEngine,
             aMessageResource);
        this.theMaxNumberOfItemLeafs=aMaxNumberOfItems;
    }
    
        /******************************* CachableItem **************************/
    
    
        /**
         * Generates a key for the cache from the passed parameters.<br>
         * This method is called from the {@link com.sap.isa.catalog.cache.CatalogCache}
         * and has to be <code>public static</code>.
         *
         * @param aMetaInfo  meta information from the EAI layer
         * @param aClient    the client spec of the catalog
         * @param aServer    the server spec of the catalog
         */
    public static String generateCacheKey(BackendBusinessObjectMetaData aMetaInfo,
                                          IClient aClient, IServer aServer) {

        ConnectionDefinition connDef = aMetaInfo.getDefaultConnectionDefinition();

            // create system id
            // special properties for the connection
        String system = null;
        Properties servProps = aServer.getProperties();
        if(servProps != null) {
            system = generateKeySAPSystem(
                servProps.getProperty(JCoManagedConnectionFactory.JCO_CLIENT),
                servProps.getProperty(JCoManagedConnectionFactory.JCO_R3NAME),
                servProps.getProperty(JCoManagedConnectionFactory.JCO_ASHOST),
                servProps.getProperty(JCoManagedConnectionFactory.JCO_SYSNR));
        }

            // default properties
        if(system == null) {
            Properties defProps = connDef.getProperties();
            system = generateKeySAPSystem(
                defProps.getProperty(JCoManagedConnectionFactory.JCO_CLIENT),
                defProps.getProperty(JCoManagedConnectionFactory.JCO_R3NAME),
                defProps.getProperty(JCoManagedConnectionFactory.JCO_ASHOST),
                defProps.getProperty(JCoManagedConnectionFactory.JCO_SYSNR));
        }

            // create views id
        StringBuffer views = new StringBuffer();
        List viewList = aClient.getViews();
        Iterator iter = viewList.iterator();
        while(iter.hasNext()) {
            IView view = (IView)iter.next();
            views.append(view.getGuid());
        }

            // now build and return the key
        return aServer.getCatalogGuid() + views.toString() + system;
    }

	/**
     * Generates a key for the cache from the passed parameters.<br>
     * This method is called from the {@link com.sap.isa.catalog.cache.CatalogCache}
     * and has to be <code>public static</code>.
     *
     * @param className  className
     * @param aClient    the client spec of the catalog
     * @param aServer    the server spec of the catalog
     */
    public static String generateCacheKey(
        String className,
        IClient aClient,
        IServerEngine aServer)
    {
        StringBuffer aBuffer = new StringBuffer(TrexCRMCatalog.class.getName());
            // create system id
            // special properties for the connection
        String system = null;
        Properties servProps = aServer.getProperties();
        system = generateKeySAPSystem(
            servProps.getProperty(JCoManagedConnectionFactory.JCO_CLIENT),
            servProps.getProperty(JCoManagedConnectionFactory.JCO_R3NAME),
            servProps.getProperty(JCoManagedConnectionFactory.JCO_ASHOST),
            servProps.getProperty(JCoManagedConnectionFactory.JCO_SYSNR));

            // create views id
        StringBuffer views = new StringBuffer();
        List viewList = aClient.getViews();
        Iterator iter = viewList.iterator();
        while(iter.hasNext())
        {
            IView view = (IView)iter.next();
            views.append(view.getGuid());
        }
        // build the key
        aBuffer.append(aServer.getGuid());
        aBuffer.append(aServer.getCatalogGuid());
        aBuffer.append(views.toString());
        aBuffer.append(system);
        
        // return the key
        return aBuffer.toString();
    }

        /**
         * Generates a key from the passed parameters for a SAP system. If the parameters
         * are incomplete so that no valid key can be built <code>null</code> will be
         * returned.
         *
         * @param client  the client of the r/3 system
         * @param r3name  the name of the r/3 system
         * @param ashost  the application server
         * @param sysnr   the system number
         * @return key for a SAP system
         */
    private static String generateKeySAPSystem(String client, String r3name,
                                               String ashost, String sysnr)
    {
        String key = null;

        if(client != null && client.length() != 0)
        {
            if(r3name != null && r3name.length() != 0)
                key = r3name + client;
            else if(ashost != null && ashost.length() != 0 && sysnr != null && sysnr.length() != 0)
                key = ashost + sysnr + client;
        }
        return key;
    }

    synchronized public boolean isCachedItemUptodate()
    {
        if (getState()==IComponent.State.DELETED) 
        {
            return false;
        } // end of if ()
        return theServerEngine.isCatalogUpToDate(this);
    }
        /************************************************************************/

    /**
     * Adds an information object about an index to the list of index
     * information objects.
     *
     * @param index  information object about an index
     */
    protected void addIndexInfo(TrexCRMCatalogIndexInfo index) {
        synchronized(theIndexInfo)
        {
            theIndexInfo.put(index.generateKey(), index);
        }
    }

    /**
     * Returns the status of the (backend) Catalog.
     *
     * @return  status of the Catalog (either OK or ERROR)
     */
    public int getCatalogStatus()
    {
        final String METHOD = "getCatalogStatus()";
        log.entering(METHOD);
        
        int catalogStatus = CATALOG_STATUS_OK;
        
        try {
            TrexCRMCatalogIndexInfo index = getIndexInfo(INDEX_LEVEL_A, "", "");
                    
            SearchManager searchManager = index.getSearchManager(this.getLanguage());
                    
            // set interval
            searchManager.setResultFromTo(INTERVAL_BEGIN, CatalogParameters.trexUpperLimit);
                    
            // add return attributes
            searchManager.addRequestedAttribute(ATTR_AREA);
    
            // restrict to not include the $ROOT area
            QueryEntry entry = new QueryEntry();
            entry.setRowType(QueryEntry.ROW_TYPE_ATTRIBUTE);
            entry.setLocation(ATTR_AREA_GUID);
            entry.setContentType(QueryEntry.CONTENT_TYPE_STRING);
            entry.setValue(VALUE_ROOT, "", QueryEntry.ATTRIBUTE_OPERATOR_NE);
            entry.setTermWeight(10000);
            searchManager.addQueryEntry(entry);
    
            // execute query
            SearchResult results = searchManager.search();
                
            if (results.getLastError().getCode() != TrexReturn.NO_ERROR) {
                catalogStatus = CATALOG_STATUS_ERROR;
            }
            if (log.isDebugEnabled())
                log.debug(METHOD + ": Catalog is available :" + getGuid());
        }
        catch (TrexException ex1) {
            catalogStatus = CATALOG_STATUS_ERROR;
            if( log.isDebugEnabled())
                log.error(METHOD + ": failed for Guid " + getGuid() + ": " + ex1.getLocalizedMessage());
        }
        catch (TrexCRMCatalogException ex2) {
            catalogStatus = CATALOG_STATUS_ERROR;
            if( log.isDebugEnabled())
                log.error(METHOD + ": failed for Guid " + getGuid() + ": " + ex2.getLocalizedMessage());
        }

        return catalogStatus;
    }
    
    /**
     * Returns the connection properties.
     *
     * @return value of the requested property
     */
    Properties getConnectionProperties()
    {
        return theConnectionProperties;
    }

    /**
     * Returns the connection property with the given name.
     *
     * @param name  name of the requested property
     * @return      value of the requested property
     */
    String getConnectionProperty(String name)
    {
        return theConnectionProperties.getProperty(name);
    }

    /**
         * Returns the variant of the catalog.
         *
         * @return  variant id of the catalog
         */
    public String getVariant()
    {
        return theVariant;
    }

    /**
     * Sets the variant of the catalog.
     *
     * @param variant  id to be set
     */
    private void setVariant(String variant)
    {
        theVariant = variant;
    }
    
    /**
     * Returns the views associated with the catalog.
     *
     * @return  array of views
     */
    public String[] getViews()
    {
        ArrayList views = this.theClient.getViews();
        ArrayList aList = new ArrayList();
        Iterator anIter = views.iterator();
        while (anIter.hasNext() ) 
        {
            IView aView = (IView)anIter.next();
            aList.add(aView.getGuid());
        } // end of while ()
        return (String[]) aList.toArray( new String[] {});
    }

    /**
     * Sets the given views for the catalog.
     *
     * @param views  array of views to be set
     */
    private void setViews(String[] views)
    {
        theViews = views;
    }

    /**
     * Gets the language of the catalog.
     *
     * @return  language  
     */
    public String getLanguage() {
        return theLanguage;
    }

        /**
         * Gets the name of the search engine associated with the catalog.
         *
         * @return  name of the search engine
         */
    String getSearchEngine() {
        return theSearchEngine;
    }
    
    /**
     * Gets the language of the catalog.
     *
     * @return  language  
     */
    void setLanguage(String theLanguage) {
        this.theLanguage = theLanguage;
    }

    /**
     * Gets the name of the search engine associated with the catalog.
     *
     * @return  name of the search engine
     */
    void setSearchEngine(String theSearchEngine) {
        this.theSearchEngine = theSearchEngine;
    }    

    /**
     * Returns the list of Java Strings representing the GUIDs for
     * the A-index and the P-index
     *
     * @return  list of Java Strings representing the GUIDs of the attached
     * indexes
     */
    public String[] getListOfIndexGuids()
        throws TrexCRMCatalogException {
        if (theIndexInfo != null) {
            synchronized(theIndexInfo) {
                int count = theIndexInfo.size();
                String[] result = new String[count];
                Iterator it = theIndexInfo.values().iterator();
                count = 0;
                while(it.hasNext())
                {
                    TrexCRMCatalogIndexInfo aTrexIndexInfo = (TrexCRMCatalogIndexInfo)it.next();
                    result[count] = aTrexIndexInfo.getStoreId();
                    count++;
                }
                return result;
            }
        }
        else {
            return TrexCRMRfcMethods.getListOfIndexGuids(theSAPConnection, this);
        }
    }

        /**
         * Returns an information object about an index.
         *
         * @param attrLevel  index level of the index
         * @param area       name of the area (in case of 'S' index; otherwise empty)
         * @param guid       guid of the catalog (if 'P' index) or of the area (if 'S' index)
         * @return           an information object about an index
         * @exception TrexCRMCatalogException connection error to the CRM system
         */
    TrexCRMCatalogIndexInfo getIndexInfo(String attrLevel, String area, String guid)
        throws TrexCRMCatalogException {
        TrexCRMCatalogIndexInfo info = null;
        String key = TrexCRMCatalogIndexInfo.generateKey(attrLevel, area);
        
        //changed because of possible deadlocks
        info = (TrexCRMCatalogIndexInfo)theIndexInfo.get(key);
        if (info == null) {
            synchronized(theIndexInfo) {
                info = TrexCRMRfcMethods.getIndexInfo(
                                getSAPConnection(),
                                getName(),
                                getVariant(),
                                getLanguage(),
                                attrLevel,
                                area,
                                guid,
                                getMessageResources(),
                                this);
                addIndexInfo(info);
            }
        }

        // set the language after A Index
//        if (attrLevel.equals("A")) { 
//            setLanguage(info.getFirstLanguage());
//        }

        return info;
    }

    synchronized private JCoConnection getSAPConnection()
    {
        if (theSAPConnection==null) 
        {
            theSAPConnection = ((TrexCRMCatalogServerEngine)theServerEngine).getDefaultJCoConnection();
        } // end of if ()
        return theSAPConnection;
    }
    
    synchronized private void setSAPConnection(Connection conn)
    {
        theSAPConnection = (JCoConnection)conn;
    }

    /**
     * Sets the default connection to the system where the infos about the
     * catalog are located.
     *
     * @param conn  the default connection
     */
    private void setDefaultConnection(Connection conn)
    {
        theSAPConnection = (JCoConnection)conn;
    }

        /**
         * This method sets a new default connection if the necessary connection
         * settings are passed via the client and the server parameter. The
         * settings for the server has to be passed via the properties which
         * are associated with the server instance.
         *
         * @param client  reference to a specific client
         * @param server  reference to a specific server
         */
    private void setDefaultConnection(IClient client, IServer server)
    {
        Properties props = null;

            // check if special client was specified
        if( client.getLocale() != null      &&
            client.getName() != null        &&
            client.getPWD()!= null          &&
            client.getName().length() != 0  &&
            client.getPWD().length() != 0)
        {
                // define new client settings
            props = new Properties();
            props.setProperty(  JCoManagedConnectionFactory.JCO_USER,
                                client.getName());
            props.setProperty(  JCoManagedConnectionFactory.JCO_PASSWD,
                                client.getPWD());
            props.setProperty(JCoManagedConnectionFactory.JCO_LANG,
                              client.getLocale().getLanguage().toUpperCase());
        }

            // check if special a server was specified
        Properties servProps = server.getProperties();
        if (servProps != null &&
            (
                (servProps.containsKey(JCoManagedConnectionFactory.JCO_CLIENT) &&
                 servProps.containsKey(JCoManagedConnectionFactory.JCO_R3NAME) &&
                 servProps.containsKey(JCoManagedConnectionFactory.JCO_MSHOST) &&
                 servProps.containsKey(JCoManagedConnectionFactory.JCO_GROUP) ) ||
                (servProps.containsKey(JCoManagedConnectionFactory.JCO_CLIENT) &&
                 servProps.containsKey(JCoManagedConnectionFactory.JCO_ASHOST) &&
                 servProps.containsKey(JCoManagedConnectionFactory.JCO_SYSNR) )) )
        {
                // define new server settings
            if(props == null)
                props = new Properties();
            props.setProperty(  JCoManagedConnectionFactory.JCO_CLIENT,
                                servProps.getProperty(
                                    JCoManagedConnectionFactory.JCO_CLIENT));
                // with load balancing
            if(servProps.containsKey(JCoManagedConnectionFactory.JCO_R3NAME))
            {
                props.setProperty(  JCoManagedConnectionFactory.JCO_R3NAME,
                                    servProps.getProperty(
                                        JCoManagedConnectionFactory.JCO_R3NAME));
                props.setProperty(  JCoManagedConnectionFactory.JCO_MSHOST,
                                    servProps.getProperty(
                                        JCoManagedConnectionFactory.JCO_MSHOST));
                props.setProperty(  JCoManagedConnectionFactory.JCO_GROUP,
                                    servProps.getProperty(
                                        JCoManagedConnectionFactory.JCO_GROUP));
            }
                // otherwise
            else
            {
                props.setProperty(  JCoManagedConnectionFactory.JCO_ASHOST,
                                    servProps.getProperty(
                                        JCoManagedConnectionFactory.JCO_ASHOST));
                props.setProperty(  JCoManagedConnectionFactory.JCO_SYSNR,
                                    servProps.getProperty(
                                        JCoManagedConnectionFactory.JCO_SYSNR));
            }
        }
            // change the default connection
        if(props != null)
        {
            try
            {
                JCoConnection modConn =
                    ((TrexCRMCatalogServerEngine)theServerEngine).getModDefaultJCoConnection(props);
                if(modConn.isValid())
                {
                    this.theConnectionProperties.putAll(props);
                    this.setSAPConnection(modConn);
                }
            }
            catch(Exception ex)
            {
                log.warn(ICatalogMessagesKeys.PCAT_EXCEPTION,
                                   new Object[] { ex.getLocalizedMessage() }, null);
            }
        }
        return;
    }
    

}