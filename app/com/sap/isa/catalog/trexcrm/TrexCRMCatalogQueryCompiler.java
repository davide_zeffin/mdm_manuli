/*****************************************************************************
    Class:        TrexCRMCatalogQueryCompiler
    Copyright (c) 2006, SAP AG, All rights reserved.
    Author:
    Created:      28.08.2006
    Version:      1.0

    $Revision$
    $Date$
*****************************************************************************/
package com.sap.isa.catalog.trexcrm;

// catalog imports


// misc imports
// trex imports
import java.util.List;

import org.apache.struts.util.MessageResources;

import com.sap.isa.catalog.impl.CatalogQuery;
import com.sap.isa.catalog.trexgen.TrexGenCatalogException;
import com.sap.isa.catalog.trexgen.TrexGenCatalogQueryCompiler;
import com.sap.isa.core.logging.IsaLocation;
import com.sapportals.trex.TrexException;
import com.sapportals.trex.core.QueryEntry;
import com.sapportals.trex.core.SearchManager;

/**
 * Helper class for the {@link TrexCRMCatalogBuilder}. Instances of this class
 * are stateless. With the <code>transform...Query</code> methods a given query
 * instance is evaluated and the necessary calls are executed against the passed
 * SearchManager instance. Internally a {@link TrexCRMCatalogFilterVisitor} is used to
 * evaluate the filter expression.
 *
 * @version     1.0
 */
public class TrexCRMCatalogQueryCompiler extends TrexGenCatalogQueryCompiler
{
    private static IsaLocation log = IsaLocation.getInstance(TrexCRMCatalogQueryCompiler.class.getName());

        /**
         * Creates a new instance of the query compiler. Each instance should only be
         * used of one specific Trex builder instance.
         *
         * @param messResources  the resources for the error messages
         */
    public TrexCRMCatalogQueryCompiler(MessageResources messResources) {
        super(messResources);
    }

    /**
     * Transforms the given query for products into the Trex specific represenation.
     *
     * @param query    the query which has to be transformed
     * @param defAttr  default list of attributes
     * @param idxAttr  all attributes of the index
     * @param catalog  the catalog which should be built
     * @param searchManager    the search manager instance for which the query was built
     * @exception CatalogFilterInvalidException error in the filter expression
     * @exception TrexException exception from the search engine
     */
    public synchronized void transformItemQuery(CatalogQuery query,
                                         List defAttr,
                                         List idxAttr,
                                         TrexCRMCatalog catalog,
                                         String searchEngine,
                                         SearchManager searchManager)
        throws TrexCRMCatalogException  {
            
        try {    
            super.transformItemQuery(query, defAttr, idxAttr, catalog, searchEngine, searchManager);
        }
        catch (TrexGenCatalogException ex1) {
           throw new TrexCRMCatalogException(ex1.getLocalizedMessage());
        }
    
        // add view     
//        String[] views = catalog.getViews();
//        if (!query.isCategorySpecificSearch() &&
//            views != null &&
//            views.length > 0) {
//            addConstraintViews(searchManager, views);
//        }
    }

    /**
     * Transforms the given query for categories into the Trex specific represenation.<BR/>
     *
     * @param query    the query which has to be transformed
     * @param defAttr  default list of attributes
     * @param idxAttr  all attributes of the index
     * @param catalog  the catalog which should be built
     * @param info    the info object about the index
     * @param searchManger the searchManager instance for which the query was built
     * @exception CatalogFilterInvalidException error in filter expression
     * @exception TrexException exception from the search engine
     */
    public synchronized void transformCategoryQuery(CatalogQuery query,
                                             List defAttr,
                                             List idxAttr,
                                             TrexCRMCatalog catalog,
                                             String searchEngine,
                                             SearchManager searchManager)
        throws TrexCRMCatalogException {
        
        try {
            super.transformCategoryQuery(query, defAttr, idxAttr, catalog, searchEngine, searchManager);
        } 
        catch (TrexGenCatalogException ex1) {
           throw new TrexCRMCatalogException(ex1.getLocalizedMessage());
        }
//        // add views
//        String[] views = catalog.getViews();
//        if (views != null && views.length > 0) {
//            addConstraintViews(searchManager, views);
//        }
            
    }
    
    /**
     * Add view constraint into the Trex specific representation.<BR/>
     *
     * @param catalog  the catalog which should be built
     * @param searchManger the searchManager instance for which the query was built
     */
    protected void addConstraintViews(SearchManager theSearchManager, String[] views) 
              throws TrexCRMCatalogException {
            
        QueryEntry entry = new QueryEntry();

        try 
        {
            // AND operator
            if (views.length > 0) {
                entry.setValue(QueryEntry.QUERY_OPERATOR_AND, "", QueryEntry.ATTRIBUTE_OPERATOR_EQ);
                entry.setRowType(QueryEntry.ROW_TYPE_OPERATOR);
                theSearchManager.addQueryEntry(entry);
            }
            for (int i=0; i < views.length; i++) {
                // view constrant
                entry.setValue(views[i], "", QueryEntry.ATTRIBUTE_OPERATOR_EQ);
                entry.setRowType(QueryEntry.ROW_TYPE_ATTRIBUTE);
                entry.setLocation(ITrexCRMCatalogConstants.ATTR_VIEWS_ID);
                entry.setContentType(QueryEntry.CONTENT_TYPE_STRING);
                entry.setTermAction(QueryEntry.TERM_ACTION_EXACT);
                theSearchManager.addQueryEntry(entry);
                // OR operator
                if(i > 0) {
                    entry.setValue(QueryEntry.QUERY_OPERATOR_OR, "", QueryEntry.ATTRIBUTE_OPERATOR_EQ);
                    entry.setRowType(QueryEntry.ROW_TYPE_OPERATOR);
                    theSearchManager.addQueryEntry(entry);
                }    
            }
        } catch(TrexException ex) {
            throw new TrexCRMCatalogException(ex.getLocalizedMessage());
        }
    }
}
