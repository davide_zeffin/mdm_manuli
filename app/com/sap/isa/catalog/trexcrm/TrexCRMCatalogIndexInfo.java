/*****************************************************************************
    Class:        TrexCRMCatalogIndexInfo
    Copyright (c) 2006, SAP AG, All rights reserved.
    Author:
    Created:      09.10.2006
    Version:      1.0

    $Revision$
    $Date$
*****************************************************************************/

package com.sap.isa.catalog.trexcrm;

// misc imports
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import com.sap.isa.catalog.trexgen.TrexGenCatalogIndexInfo;
import com.sap.isa.core.logging.IsaLocation;
import com.sapportals.trex.core.admin.AdminIndex;

/**
 * This class encapsulate all information about an index of an IMS catalog.
 * The meta information of the attributes which are stored in the CRM system
 * can also be requested form this info object.
 * @version     1.0
 */
public class TrexCRMCatalogIndexInfo
      extends TrexGenCatalogIndexInfo  
      implements Serializable {

  /**
   * The class is serializable. All attributes are serializable with the default
   * implementation.
   */

  /**
   * Constant for the property POS_NR. This property defines the order
   * of the attributes as it was defined in the CRM system.
   */
  static String PROP_POS_NR = "POS_NR";

  /**
   * Constant for the property DESCRIPTION. This property defines the language
   * dependant text of the attribute.
   */
  static String PROP_DESCRIPTION = "DESCRIPTION";
  
  /**
   * Constant for the property GUID. This property defines the guid of the
   * attribute as it was defined in the CRM system.
   */
  static String PROP_GUID = "GUID";


  /**
   * Constant for the property ENTRYSTYLE. This property defines the kind
   * of input field which has to be used for this attribute as it was defined
   * in the CRM system.
   */
  static String PROP_ENTRYSTYLE  = "ENTRYSTYLE";

  /**
   * Constant for the property TYPE. This property defines the type of the
   * attribute. E.g: CHAR, NUMC
   */
  static String PROP_TYPE = "DATA_TYPE";

      // the logging instance
  private static IsaLocation log =
        IsaLocation.getInstance(TrexCRMCatalogIndexInfo.class.getName());
        
  private Map theFixedValues; // fixed values for the attributes
  private Map theAttributeProps; // properties for the attributes

  /**
   * Creates a new instance of a index.
   *
   * @param catId      unique id of catalog
   * @param aIndexType attribute level / index Type ('A', 'P' or 'S')
   * @param area       name of area / category id (in case of 'S' index)
   * @param aAdminIndex Admin index on index
   */
  TrexCRMCatalogIndexInfo(String catId, 
                          String aIndexType, 
                          String area,
                          AdminIndex aAdminIndex) {
      super(catId, aIndexType, area, aAdminIndex);                              

      final String METHOD = "TrexCRMCatalogIndexInfo()";
      log.entering(METHOD);
                              
      theFixedValues = new HashMap();
      theAttributeProps = new HashMap();

      if (log.isInfoEnabled()) {
          log.debug(METHOD + ": theIndexType=" + theIndexType +
                    "/ theCatId=" + theCatId +
                    "/ theIndexGuid=" + theIndexGuid +
                    "/ theArea=" + theCategoryId);
      }
      log.exiting();
  }  

  /**
   * Generates a unique identifier for the index.
   *
   * @param attrLevel  name of index level
   * @param area       name of area
   */
  public static String generateKey(String attrLevel, String area) {
      return attrLevel + area;
  }

  /**
   * Generates a unique identifier for the index. The key is built
   * through concatenation of attrLevel and area.
   *
   * @return  unique key for index
   */
  public String generateKey() {
      return generateKey(getAttrLevel(), getArea());
  }

  /**
   * Adds an attribute to the list of attributes.
   *
   * @param attribute  name of the attribute to be added
   */
  public void addAttribute(String attribute) {
      this.theAttributes.add(attribute);
  }

  /**
   * Adds a fixed value to the list of fixed values for a given attribute.
   *
   * @param attribute name of the attribute
   * @param valueName the technical value of the fixed value
   * @param valueDesc the language dependent value of the fixed value
   */
  void addFixedValue(String attribute, String valueName, String valueDesc) {
      // get the values for this attribute
      List values = (List)theFixedValues.get(attribute);
      // create new entry for this attribute
      if (values == null) {
          values = new ArrayList();
          theFixedValues.put(attribute, values);
      }
      // construct a small list which hold both values
      // convert the type NUMC, e.g. --> 017 will be 17!. 
      List entryList = new ArrayList();
      if (this.getAttributeProperties(attribute).getProperty(TrexCRMCatalogIndexInfo.PROP_TYPE).equals("NUMC")) {
          try {
              valueName = new Integer(valueName).intValue()+"";
          }
          catch (Exception e) {
              log.debug("TrexCRMCatalogIndexInfo.addFixedValue: "+ e.getMessage()+"__value to Convert:"+valueName);
          }
      }

      entryList.add(valueName);
    
      if (valueDesc != null)
          entryList.add(valueDesc);
      // add the value for this attribute
      values.add(Collections.unmodifiableList(entryList));
      if (log.isDebugEnabled()) {
          log.debug("IndexKey: " + generateKey() +
                    " - Added fixed value '" + valueName +
                    "'='" + valueDesc +
                    "' for attribute '" + attribute + "'");
      }
  }

  /**
   * Adds a property to the list of properties for a given attribute.
   *
   * @param attribute  name of the attribute
   * @param name       name of the property
   * @param value      value of the property
   */
  void addAttributeProperty(String attribute, String name, String value) {
      // get the properties for this attribute
      Properties props = (Properties)theAttributeProps.get(attribute);
      // create new entry for this attribute
      if (props == null) {
          props = new Properties();
          theAttributeProps.put(attribute, props);
      }
      // add the property entry for this attribute
      props.setProperty(name, value);
      if (log.isDebugEnabled()) {
          log.debug("IndexKey: " + generateKey() +
                    " - Property \"" + name +
                    "\" for attribute \"" + attribute +
                    "\" added.");
      }
  }

  /**
   * Returns the index level.
   *
   * @return  name of index level (possible values A, P, S)
   */
  String getAttrLevel() {
      return theIndexType;
  }

  /**
   * Returns the list of fixed values for the given attribute. If no fixed
   * values are found <code>null</code> will be returned.
   *
   * @param   attribute name of the attribute
   * @return  list of fixed values for the requested attribute
   */
  List getAttributeFixedValues(String attribute) {
      return (List)theFixedValues.get(attribute);
  }

  /**
   * Returns the properties for the given attribute. If no properties are found
   * <code>null</code> will be returned.
   *
   * @param   attribute name of the attribute
   * @return  properties of the requested attribute
   */
  Properties getAttributeProperties(String attribute) {
      return (Properties)theAttributeProps.get(attribute);
  }

  /**
   * Returns the category id.
   *
   * @return  category id
   */
  String getCatId() {
      return theCatId;
  }

  /**
   * Returns the area name of the index.
   *
   * @return  area name in case of 'S' index (otherwise empty)
   */
  String getArea() {
      return theCategoryId;
  }


}