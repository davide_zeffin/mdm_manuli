/*****************************************************************************
    Class:        TrexCRMCatalogFilterVisitor
    Copyright (c) 2006, SAP AG, All rights reserved.
    Author:
    Created:      05.09.2006
    Version:      1.0

    $Revision$
    $Date$
*****************************************************************************/
package com.sap.isa.catalog.trexcrm;

import org.apache.struts.util.MessageResources;

import com.sap.isa.catalog.trexgen.TrexGenCatalogFilterVisitor;

/**
 * This class transforms the filter expression tree to the Trex specific
 * representation i.e. the calls against the Trex searchManager are performed.
 *
 * @version     1.0
 */
public class TrexCRMCatalogFilterVisitor extends TrexGenCatalogFilterVisitor
{
        /**
         * Creates a new instance of the Trex specific visitor of the filter expression
         * tree.
         *
         * @param messResources  the resources for the error messages
         */
    TrexCRMCatalogFilterVisitor(MessageResources messResources)
    {
        super(messResources);
    }

}