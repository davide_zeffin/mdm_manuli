/*****************************************************************************
    Class:        TrexCRMCatalogServerEngine
    Copyright (c) 2006, SAP AG, All rights reserved.
    Author:
    Created:      15.09.2006
    Version:      1.0

    $Revision$
    $Date$
*****************************************************************************/
package com.sap.isa.catalog.trexcrm;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Locale;
import java.util.Properties;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.w3c.dom.Text;

import com.sap.isa.catalog.ICatalogMessagesKeys;
import com.sap.isa.catalog.boi.CatalogException;
import com.sap.isa.catalog.boi.IActor;
import com.sap.isa.catalog.boi.ICatalogInfo;
import com.sap.isa.catalog.boi.IClient;
import com.sap.isa.catalog.boi.IServer;
import com.sap.isa.catalog.boi.IServerEngine;
import com.sap.isa.catalog.impl.Catalog;
import com.sap.isa.catalog.impl.CatalogCompositeComponent;
import com.sap.isa.catalog.impl.CatalogServerEngine;
import com.sap.isa.catalog.impl.CatalogSite;
import com.sap.isa.catalog.trexgen.TrexGenCatalogException;
import com.sap.isa.catalog.trexgen.TrexGenCatalogServerEngine;
import com.sap.isa.core.TechKey;
import com.sap.isa.core.eai.BackendBusinessObjectMetaData;
import com.sap.isa.core.eai.BackendException;
import com.sap.isa.core.eai.ConnectionDefinition;
import com.sap.isa.core.eai.sp.jco.BackendBusinessObjectSAP;
import com.sap.isa.core.eai.sp.jco.JCoConnection;
import com.sap.isa.core.eai.sp.jco.JCoManagedConnectionFactory;
import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.core.logging.LogUtil;
import com.sap.isa.core.util.table.ResultData;

/**
 * TrexCRMCatalogServerEngine.java
 *
 * @version 1.0
 */

final public class TrexCRMCatalogServerEngine 
      extends TrexGenCatalogServerEngine 
      implements BackendBusinessObjectSAP {
    
    private JCoConnection theSAPConnection;
    private boolean isStagingSupportDetermined = false;
   
    // the logging instance
    private static IsaLocation log =
        IsaLocation.getInstance(TrexCRMCatalogServerEngine.class.getName());

    /**
     * Creates a new <code>TrexCRMCatalogServerEngine</code> instance.
     * Only used when created via the BEM mechanism.
     */
    public TrexCRMCatalogServerEngine() {
        theType = IServerEngine.ServerType.CRM;
    }
    
    /**
     * Creates a new <code>TrexCRMCatalogServerEngine</code> instance.
     *
     * @param aGuid a <code>String</code> value
     * @param aSite a <code>CatalogSite</code> value
     * @param aServerConfigFile a <code>Document</code> value
     */ 
    public TrexCRMCatalogServerEngine (String aGuid,
                                       CatalogSite aSite,
                                       Document aServerConfigFile)
        throws BackendException {
            
        super(aGuid, aSite, aServerConfigFile);
        this.theMessageResources = aSite.getMessageResources();
        theType = IServerEngine.ServerType.CRM;
    }

    /**
     * Creates a new <code>TrexCRMCatalogServerEngine</code> instance.
     *
     * @param aGuid a <code>String</code> value
     * @param aSite a <code>CatalogSite</code> value
     * @param aServerConfigFile a <code>Document</code> value
     * @param theConnectionProperties a <code>Properties</code> value
     */ 
    public TrexCRMCatalogServerEngine (String aGuid,
                                       CatalogSite aSite,
                                       Document aServerConfigFile,
                                       Properties theEAIProperties)
        throws BackendException {
            
        super(aGuid, aSite, aServerConfigFile, theEAIProperties);
        this.theMessageResources = aSite.getMessageResources();
        theType = IServerEngine.ServerType.CRM;
    }

    /************************ BackendBusinessObject ***********************/

    // -> TrexGenCatalogServerEngine:
    // initBackendObject()
    // destroyBackendObject()
        
    /*************************** BackendBusinessObjectSAP **********************/
        
    // -> TrexGenCatalogServerEngine:
    // addJCoConnectionEventListener() 
    // getDefaultJCoConnection(Properties props)
    // getDefaultJCoConnection() 
    // getJCoConnection(String conKey)
    // getModDefaultJCoConnection(Properties conDefProps)

    /****************************** CachableItem ***************************/
    
    // -> TrexGenCatalogServerEngine:
    // isCachedItemUptodate() 
    // generateKeySAPSystem()

    /**
     * Generates a key for the cache from the passed parameters.<br>
     * This method is called from the {@link com.sap.isa.catalog.cache.CatalogCache}
     * and has to be <code>public static</code>.
     *
     * @param aMetaInfo  meta information from the EAI layer
     * @param aClient    the client spec of the catalog
     * @param aServer    the server spec of the catalog
     */
    public static String generateCacheKey(
        BackendBusinessObjectMetaData aMetaInfo,
        IActor aClient,
        IServer aServer) {
        StringBuffer aBuffer = new StringBuffer(TrexCRMCatalogServerEngine.class.getName());
        
        ConnectionDefinition connDef =
            aMetaInfo.getDefaultConnectionDefinition();

            // create system id
            // special properties for the connection
        String system = null;
        Properties servProps = aServer!=null?aServer.getProperties():null;
        if (servProps != null) {
            system = generateKeySAPSystem(
                servProps.getProperty(JCoManagedConnectionFactory.JCO_CLIENT),
                servProps.getProperty(JCoManagedConnectionFactory.JCO_R3NAME),
                servProps.getProperty(JCoManagedConnectionFactory.JCO_ASHOST),
                servProps.getProperty(JCoManagedConnectionFactory.JCO_SYSNR));
        }

            // default properties
        if (system == null) {
            Properties defProps = connDef.getProperties();
            system = generateKeySAPSystem(
                defProps.getProperty(JCoManagedConnectionFactory.JCO_CLIENT),
                defProps.getProperty(JCoManagedConnectionFactory.JCO_R3NAME),
                defProps.getProperty(JCoManagedConnectionFactory.JCO_ASHOST),
                defProps.getProperty(JCoManagedConnectionFactory.JCO_SYSNR));
        }

        String theServerGUID =
            (aServer!=null?
             aServer.getGuid():
             aMetaInfo.getBackendBusinessObjectConfig().getType());
        
        aBuffer.append(theServerGUID);
        aBuffer.append(system);
        
            // now build and return the key
        return aBuffer.toString();
    }

    /*******************************************************************************/

    // -> TrexGenCatalogServerEngine:
    // setDefaultConnection()
    
    /********************************* from IServer ****************************/
    
    /**
     * Not appropriate in the CRM case
     * 
     * @return an <code>int</code> value = -1
     */
    public int getPort() {
        return -1;
    }
    
    /**
     * Not appropriate in the CRM case!
     *
     * @return a <code>String</code> value ="";
     */
    public String getURLString() {
        return "";
    }
    
    // -> TrexGenCatalogServerEngine:
    // ? isMasterSlaveReplicationEnabled()
    // ? getLockTimeOut() 
    // ? getNameServerList()
    // ? addNameServer()
    // ? useQueueServer()
    
    /***  implementations of abstract CatalogServerEngine methods *****/

    /************************* IServerEngine *******************************/
    
    /**
     * Nothing to do in the CRM case.
     * The Catalog infos are fetched from the crm system.
     *
     */
    protected void init(Document theConfigFile) {
            //use the supplied catalogs as filter
        NodeList aCatalogList = theConfigFile.getElementsByTagName("Catalog");
        if (aCatalogList!=null && aCatalogList.getLength()>0) {
            buildCatalogInfosRemote(aCatalogList);
        }
        return;
    }

    // -> TrexGenCatalogServerEngine:
    // hasServerFeature(IServerEngine.ServerFeature aFeature)
    // getServerFeatures()
    // createCatalogRemote()
    // addCatalogRemote(InputSource, Source)

    /**
     * This method will return a list of information for the available staging catalogs
     * @param country as String
     *
     * @return list of information for the available staging catalogs
     */
    public ResultData getInactiveCatalogInfos(String theCatalogGuid, String stagingType, IClient aUser, String country) {
        
        ResultData inactiveCatalogInfos = null;
        
        theCatalogGuid = theCatalogGuid.trim();
        
        try {
            String catName = theCatalogGuid.substring(0, 29);
            String catVariant = theCatalogGuid.substring(30);
            
            inactiveCatalogInfos = TrexCRMRfcMethods.getStagingCatalogInfos(getDefaultJCoConnection(), catName, catVariant, stagingType, country);
        }
        catch (TrexCRMCatalogException tce) {
                    log.error(LogUtil.APPS_COMMON_INFRASTRUCTURE, ICatalogMessagesKeys.PCAT_ERR_INIT_INSTANCE,
                                    new Object[]{this.getClass().getName(), tce.getLocalizedMessage()},
                                    tce);
        }
        
        return inactiveCatalogInfos;
    }

    /**
     * Create a new TrexCRMCatalog instance.
     * Only called if the catalog is not cached.
     *
     * @param aCatalogInfo a <code>this.CatalogInfo</code> value
     * @param aClient an <code>IClient</code> value
     * @return a <code>Catalog</code> value
     * @exception CatalogException if an error occurs
     */
    protected Catalog getCatalogRemote(
                      CatalogInfo aCatalogInfo, 
                      IClient aClient)
               throws CatalogException {
        
        TrexCRMCatalogInfo aTrexCRMCatalogInfo=(TrexCRMCatalogInfo) aCatalogInfo;

        String theGuid = aTrexCRMCatalogInfo.getGuid();
        int size = theGuid.length();
        if (size < 60) {
            StringBuffer aBuffer = new StringBuffer(theGuid);
            do {
                aBuffer.append(' ');
            } 
            while (aBuffer.length() < 60) ;
            theGuid = aBuffer.toString();
        } // end of if ()
        
        Catalog aNewTrexCRMCatalog = null;
        String theMaxNumAsString =
            aCatalogInfo.getProperty(
                CatalogCompositeComponent.PROPERTY_MAX_NUMBER_OF_ITEM_LEAFS);
        int theInt = -1;
        try //just in case the string can't be parsed
        {
            theInt = Integer.parseInt(theMaxNumAsString);
        } catch (Throwable t) {
        	log.debug(t.getMessage());
                //you got your fair chance to set the number
        } // end of try-catch                        int theInt 
        
        if (theInt != -1) {
            aNewTrexCRMCatalog =
                new TrexCRMCatalog(
                    this,
                    aClient,                
                    theGuid,
                    aTrexCRMCatalogInfo.getSearchEngine(),
                    this.theMessageResources,
                    theInt);            
        }
        else {
            aNewTrexCRMCatalog =
                new TrexCRMCatalog(
                    this,
                    aClient,                
                    theGuid,
                    aTrexCRMCatalogInfo.getSearchEngine(),
                    this.theMessageResources);
        } // end of else
        return aNewTrexCRMCatalog;   
    }
    
    protected void addCatalogRemote(Catalog catalog) {
        throw new UnsupportedOperationException("Not implemented yet!");
    }


    /**
     * Determines for the given instance the replication timestamp
     * as known in the A-Index of Trex.
     * Fallback: determination of the replication timestamp as known
     * in the CRM.
     *
     * @param aTrexCRMCatalog a <code>TrexCRMCatalog</code> whose timestamp is asked for
     * @return a <code>String</code> the correct time stamp or "" in case of failure.
     */   
    String getCurrentReplTimeStamp(TrexCRMCatalog aTrexCRMCatalog) {
        String theReplTimeStamp = "";
        final String TIMESTAMP = "TIMESTAMP";
        String myResourceKey = "";
        
        try {
            // get information about A index of TrexCRMcatalog
            TrexCRMCatalogIndexInfo info  = aTrexCRMCatalog.getIndexInfo(ITrexCRMCatalogConstants.INDEX_LEVEL_A,"","");
        
            if (info.hasAttribute(TIMESTAMP)) {
                if (log.isInfoEnabled()) 
                    log.info("getCurrentReplTimeStamp: retrieving attribute '" + TIMESTAMP + "' from index");

                theReplTimeStamp = getIndexLastReplicationTimestamp(info.getIndexGuid());            
                        
                if(log.isInfoEnabled()) {
                        String msg = "getCurrentReplTimeStamp for catalog '"
                                     + aTrexCRMCatalog.getName().trim()
                                     + "' returns following TIMESTAMP from index: "
                                     + theReplTimeStamp
                                     + ".";
                        log.info(msg);
                }
                return theReplTimeStamp;
            }// of if(info.hasAttribute(TIMESTAMP))  
            
        }//try
        catch (TrexGenCatalogException ex) {
            String msg = "getting TIMESTAMP from TREX failed with following exception: "
                         + ex.getLocalizedMessage();
            log.warn(ICatalogMessagesKeys.PCAT_EXCEPTION,
                             new Object[]{msg}, ex);
            theReplTimeStamp ="";
        }// catch

        // in case of failure (reading timestamp out of trex) or
        // attribute TIMESTAMP is not available on index: get it from CRM       
        try {   
            log.warn(ICatalogMessagesKeys.PCAT_WARNING, new Object[]
                {"No attribute called 'TIMESTAMP' available from index or reading from index failed completely - now trying to get timestamp from CRM."},null);     
         
            theReplTimeStamp = TrexCRMRfcMethods.getCatalogStagingReplTimeStamp(getDefaultJCoConnection(), 
                                                                                aTrexCRMCatalog);
        }
        catch(TrexCRMCatalogException ex) {   
            log.error(
                ICatalogMessagesKeys.PCAT_ERR_DETER_CAT_REPLI_STATUS,
                new Object[] {aTrexCRMCatalog.getGuid()}, ex);            
            theReplTimeStamp = "";
        }
        
        if (log.isInfoEnabled()) {   
            String msg = "'getCurrentReplTimeStamp' for TrexCRMCatalog "
                         + aTrexCRMCatalog.getName()
                         + " returns following TIMESTAMP from CRM (not from index!): "
                         + theReplTimeStamp
                         + ".";
            log.info(msg);
        }     
                    
        return theReplTimeStamp;       
    }

    /**
     * comparison of timestamp of the Catalog Objekt and timestamp of crm
     * 
     * @param aCatalog
     */
    public boolean isCatalogUpToDate(Catalog aCatalog) {
        
        log.entering("isCatalogUpToDate(Catalog aCatalog)");
        
        boolean result = true;
        TrexCRMCatalog aTrexCRMCatalog = (TrexCRMCatalog) aCatalog;
        String timeStamp = aTrexCRMCatalog.getReplTimeStamp(); 
        String theReplTimeStamp ="";
        
        // If catalog staging is supported, don't compare timestamps, but the
        // catalog keys
        log.debug("Check upToDate for staging");
            
        result = false;
           
        TechKey oldCatalogKey = aTrexCRMCatalog.getCatalogKey();
        TechKey currCatalogKey = TechKey.EMPTY_KEY;
        try {
            currCatalogKey = TrexCRMRfcMethods.getCatalogStagingKey(getDefaultJCoConnection(), aTrexCRMCatalog);
                    
            if (log.isDebugEnabled()) {
                log.debug("OldCatalogKey = " + oldCatalogKey + " CurrCatalogKey = " + currCatalogKey);
            }
                    
            result = oldCatalogKey.getIdAsString().equals(currCatalogKey.getIdAsString());
        }
        catch (TrexCRMCatalogException ex) {
            log.error("Techkey for staging catalog could not be determined, so we assume it is outdated");
        }
        
        if (log.isDebugEnabled()) {
            log.debug("isCatalogUpToDate=" + result);
        }
        
        log.exiting();
        
        return result; 
    }
    
    
        /**
         * Build the list of catalog infos known to that server.
         * The node-list of Catalog elements is used as a filter.
         * if the node list is empty or null no catalog info elements will be build.
         *
         * @param theCatalogElements a <code>NodeList</code> value
         */
    synchronized protected void buildCatalogInfosRemote(NodeList theCatalogElements) {
        ArrayList theNamePattern = new ArrayList();
        if (theCatalogElements!=null && theCatalogElements.getLength()>0) {
            int size = theCatalogElements.getLength();
            for (int i =0; i < size; i++) {
                Element aCatlogElement = (Element)theCatalogElements.item(i);
                CatalogInfo aCatalogInfo =
                    new CatalogInfo(
                        aCatlogElement.getAttribute("ID"),
                        TrexCRMCatalog.class.getName());
                theNamePattern.add(aCatalogInfo);
                NodeList theDetails = aCatlogElement.getElementsByTagName("Detail");
                int length = theDetails.getLength();
                for (int j = 0; j < length; j++) {
                    Element aDetail = (Element) theDetails.item(j);
                    String theID = aDetail.getAttribute("ID");
                    NodeList theValueChilds = aDetail.getElementsByTagName("Value");
                    int childNum = theValueChilds.getLength();
                    for (int k = 0; k < childNum; k++) {
                        Element  aValue = (Element) theValueChilds.item(k);
                        String theLocale = aValue.getAttribute("xml:lang");
                        Text theContent = (Text)aValue.getFirstChild();
                        aCatalogInfo.setProperty(theID,theLocale,theContent.getData());
                    } // end of for ()
                }
            } // end of for ()
        } // end of if ()
        
        Iterator anIter = theNamePattern.iterator();
        while (anIter.hasNext()) {
            CatalogInfo aCatalogInfo = (CatalogInfo)anIter.next();
            if (log.isDebugEnabled()) {
                log.debug("load catalog infos for '" + aCatalogInfo.getName() + "'");
            }
            buildCatalogInfosRemote(aCatalogInfo.getName());
        }
        
        return;
    }

        /**
         * Build a catalog info list based on the given name pattern.
         * if null is supplied all catalogs known to this server will be returned.
         *
         * @param aCatalogNamePattern a <code>String</code> value
         */
    synchronized protected void buildCatalogInfosRemote(String aCatalogNamePattern) {
        //get all indices that are known in the back-end
        ResultData theIndices = null;
        try {
            if(aCatalogNamePattern.length() >= 30) {
                aCatalogNamePattern = aCatalogNamePattern.substring(0,29).trim();
            }
            theIndices = TrexCRMRfcMethods.getIndexList(
                getDefaultJCoConnection(),
                aCatalogNamePattern);
        }
        catch (TrexCRMCatalogException tce) {
            log.error(LogUtil.APPS_COMMON_INFRASTRUCTURE, ICatalogMessagesKeys.PCAT_ERR_INIT_INSTANCE,
                            new Object[]{this.getClass().getName(), tce.getLocalizedMessage()},
                            tce);
            return;
        } // end of try-catch

        HashMap theBuildCatInfos = new HashMap();
        // initialize the HashMap from the already loaded CatalogInfos
        Iterator aCatalogInfoIter = theCatalogInfos.iterator();
        while (aCatalogInfoIter.hasNext()) {
            ICatalogInfo anInfo = (ICatalogInfo)aCatalogInfoIter.next();
            theBuildCatInfos.put(anInfo.getGuid(), anInfo);
        } // end of while ()

        // get the info of backend system for later use (theDescription)
        String backendInfo = getDefaultJCoConnection().getAttributes().getSystemID()
                     + ":" + getDefaultJCoConnection().getAttributes().getClient();

        theIndices.beforeFirst();
        while (theIndices.next()) {
            String theName = theIndices.getString("CATALOG_ID");
            String theVariant  = theIndices.getString("VARIANT_ID");
                
            int sizeName = theName.length();
                
            char[] theNameChars = new char[30];
            for (int i = 0; i < 30; i++) {
                theNameChars[i] = (i<sizeName? theName.charAt(i): ' ');
            } // end of for ()
                
            StringBuffer aBuffer = new StringBuffer();
            aBuffer.append(theNameChars);
            aBuffer.append(theVariant);
                
            String theCatalogGuid = aBuffer.toString();
            if (theBuildCatInfos.containsKey(theCatalogGuid)) {
                continue;
            } // end of if ()
            else {
                String theDescription = theSite.getMessageResources().getMessage(Locale.ENGLISH,
                                                                                 ICatalogMessagesKeys.PCAT_CAT_DOWNLOADED_SYSTEM,
                                                                                 new Object[]{ backendInfo } );
                try {
                    TrexCRMCatalogInfo aCatalogInfo =
                        new TrexCRMCatalogInfo(
                            theCatalogGuid,
                            TrexCRMCatalog.class.getName(),
                            theName,
                            theVariant,
                            theDescription,
                            this);
                    theCatalogInfos.add(aCatalogInfo);
                    theBuildCatInfos.put(theCatalogGuid, aCatalogInfo);                    
                } 
                catch (CatalogException ce) {
                    log.error(
                         ICatalogMessagesKeys.PCAT_OBJ_INSTANTIATED,
                         new Object[]{TrexCRMCatalogInfo.class.getName()},
                         ce);
                } // end of try-catch
            } // end of else
        } // end of while ()
        return;
    }
    
    /************************************************/
    
    public class TrexCRMCatalogInfo extends CatalogServerEngine.CatalogInfo {
        public static final String PROPERTY_VARIANT ="variant";
        public static final String PROPERTY_SEARCH_ENGINE = "searchEngine";
        
        public TrexCRMCatalogInfo(String aCatalogGuid,
                              String theImplClassName,
                              String theName,
                              String theVariant,                              
                              String theDescription,
                              TrexCRMCatalogServerEngine theServerEngine)
            throws CatalogException {
            super(aCatalogGuid,theImplClassName);
            setProperty(PROPERTY_NAME, Locale.ENGLISH.toString(),theName);
            setProperty(PROPERTY_DESCRIPTION, Locale.ENGLISH.toString(),theDescription);
            setProperty(PROPERTY_VARIANT,null,theVariant);
        }
        
        public String getVariant() {
            return getProperty(PROPERTY_VARIANT);
        }

        public String getSearchEngine() {
            return getProperty(PROPERTY_SEARCH_ENGINE);
        }
        
        public void setSearchEngine(String aSearchEngine) {
            setProperty(PROPERTY_SEARCH_ENGINE,null,aSearchEngine);
        }
    }
    
    // -> TrexGenCatalogServerEngine:
    // class TrexNameServer
    
    //******************************** Staging ****************************
    
    /**
     * This method determines, what kind of catalogue staging is supported
     *
     * @return String kind of staging support
     */
    public synchronized String getStagingSupport() {
        
        log.entering("getStagingSupport()");
        
        if (!isStagingSupportDetermined) {
            try {
                stagingSupport= TrexCRMRfcMethods.getStagingSupport(getDefaultJCoConnection());
            }
            catch (TrexCRMCatalogException tce) {
                log.error(LogUtil.APPS_COMMON_INFRASTRUCTURE, ICatalogMessagesKeys.PCAT_ERR_INIT_INSTANCE,
                                new Object[]{this.getClass().getName(), tce.getLocalizedMessage()},
                                tce);
            } // end of try-catch

            isStagingSupportDetermined = true; 
        }
        
        log.exiting();
        
        return stagingSupport;
    }
    
}