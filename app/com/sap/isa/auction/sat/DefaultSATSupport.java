/*
 * Created on Jan 5, 2005
 */
package com.sap.isa.auction.sat;

import com.sap.isa.core.SharedConst;
import com.sap.isa.core.logging.IsaSession;
import com.sap.util.monitor.jarm.IMonitor;

/**
 * @author I802891
 */
public class DefaultSATSupport implements SATSupport {
	
	private IMonitor mon = null;
	
	public DefaultSATSupport() {
	}
	
	public DefaultSATSupport(IMonitor mon) {
		this.mon = mon;
	}
	
	/* (non-Javadoc)
	 * @see com.sap.isa.auction.sat.SATSupport#getRequestMonitor()
	 */
	public IMonitor getRequestMonitor() {
		if(mon != null) 
			return mon;
		else 
			return (IMonitor)IsaSession.getAttribute(SharedConst.SAT_MONITOR);
	}
}
