/*
 * Created on Dec 31, 2004
 *
 */
package com.sap.isa.auction.sat;

import com.sap.isa.core.util.VersionGet;

/**
 * @author I802891
 *
 */
public abstract class SATConstants {
	private SATConstants() {}
	static {
		if(!VersionGet.isInitialized()) VersionGet.initVersion();
	}
	
	public static final String SAT_REQUEST_PREFIX = VersionGet.getSATRequestPrefix();
	public static final String JDBC_PREFIX = VersionGet.getSATRequestPrefix() + "jdbc:";
	public static final String JDO_PREFIX = VersionGet.getSATRequestPrefix() + "jdo:";
	public static final String JCO_PREFIX = VersionGet.getSATRequestPrefix() + "jco:";
}
