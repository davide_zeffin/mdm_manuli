/*
 * Created on Dec 30, 2004
 */
package com.sap.isa.auction.sat;

import com.sap.util.monitor.jarm.IMonitor;

/**
 * @author I802891
 */
public interface SATSupport {
	IMonitor getRequestMonitor();
}
