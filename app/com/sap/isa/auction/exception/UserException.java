package com.sap.isa.auction.exception;

import com.sap.tc.logging.Category;
import com.sap.tc.logging.Location;
import com.sap.isa.auction.exception.i18n.*;

/**
 * Title:        Internet Sales
 * Description: This exception is thrown for any Invalid Operation triggered within
 *              e-auction by User
 * Copyright:    Copyright (c) 2002
 * Company:      SAPMarkets
 * @author
 * @version 1.0
 */

public class UserException extends AuctionRuntimeExceptionBase {

    private static final String EXC_ID = "UserException";

    public static final String ILLEGAL_OPERATION = EXC_ID + 201;
    public static final String ILLEGAL_ARGUMENT = EXC_ID + 202;


    static {

        ErrorMessageBundle.setDefaultMessage(ILLEGAL_OPERATION,"Operation {0} is not allowed for user {1}");
        ErrorMessageBundle.setDefaultMessage(ILLEGAL_ARGUMENT,"Argument {0} is illegal");
    }

	/**
	 * @param arg0
	 */
	public UserException(Throwable arg0) {
		super(arg0);
	}

	/**
	 * @param arg0
	 */
	public UserException(I18nErrorMessage arg0) {
		super(arg0);
	}

	/**
	 * @param arg0
	 * @param arg1
	 */
	public UserException(I18nErrorMessage arg0, Throwable arg1) {
		super(arg0, arg1);
	}

	/**
	 * @param arg0
	 * @param arg1
	 * @param arg2
	 * @param arg3
	 * @param arg4
	 */
	public UserException(
		Category arg0,
		int arg1,
		Location arg2,
		I18nErrorMessage arg3,
		Throwable arg4) {
		super(arg0, arg1, arg2, arg3, arg4);
	}

}