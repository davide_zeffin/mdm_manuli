/*
 * Created on Sep 9, 2003
 *
 */
package com.sap.isa.auction.exception;

import com.sap.localization.LocalizableText;
import java.util.List;

/**
 * @author I802891
 *
 */
public interface IAuctionExceptionBase {
	/**
	 * Transient property
	 */
	public void setContext(Object ctxt);

	/**
	 * Transient property
	 */
	public Object getContext();
	
	/**
	 * For multiple errors
	 * @param error
	 */
	public void addLocalizableMessage(LocalizableText error);
	
	/**
	 * List of errors in order they are added
	 * @return List unmodifiable
	 */
	public List getLocalizableMessages();
}
