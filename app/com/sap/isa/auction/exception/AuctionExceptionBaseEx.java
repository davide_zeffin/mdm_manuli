/*
 * Created on Sep 9, 2003
 *
 */
package com.sap.isa.auction.exception;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import com.sap.localization.LocalizableText;

/**
 * @author I802891
 *
 */
public class AuctionExceptionBaseEx implements IAuctionExceptionBase, Serializable {

	private transient Object ctxt;
	private ArrayList errorMsgs = new ArrayList();
	
	/* (non-Javadoc)
	 * @see com.sap.isa.auction.exception.IAuctionExceptionBase#addLocalizableMessage(com.sap.localization.LocalizableText)
	 */
	public void addLocalizableMessage(LocalizableText error) {
		errorMsgs.add(error);
	}
	
	/* (non-Javadoc)
	 * @see com.sap.isa.auction.exception.IAuctionExceptionBase#getContext()
	 */
	public Object getContext() {
		return ctxt;
	}

	/* (non-Javadoc)
	 * @see com.sap.isa.auction.exception.IAuctionExceptionBase#getLocalizableMessages()
	 */
	public List getLocalizableMessages() {
		return Collections.unmodifiableList(errorMsgs);
	}

	/* (non-Javadoc)
	 * @see com.sap.isa.auction.exception.IAuctionExceptionBase#setContext(java.lang.Object)
	 */
	public void setContext(Object ctxt) {
		this.ctxt = ctxt;
	}
}
