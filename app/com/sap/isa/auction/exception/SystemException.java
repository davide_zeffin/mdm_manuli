package com.sap.isa.auction.exception;

import com.sap.tc.logging.Category;
import com.sap.tc.logging.Location;
import com.sap.isa.auction.exception.i18n.*;

/**
 * Title:        Internet Sales
 * Description: This exception is thrown if there is an internal error in
 *              e-auction functionality.
 * Copyright:    Copyright (c) 2002
 * Company:      SAPMarkets
 * @author
 * @version 1.0
 */

public class SystemException extends AuctionRuntimeExceptionBase {
    private static final String EXC_ID = "SystemException";

    public static final String INTERNAL_ERROR = EXC_ID + 601;
    public static final String JDO_ERROR = EXC_ID + 602;
	public static final String SCHEDULER_ERROR = EXC_ID + 603;

    static {

		ErrorMessageBundle.setDefaultMessage(INTERNAL_ERROR,"Internal Error, contact System Administrator for help");
		ErrorMessageBundle.setDefaultMessage(JDO_ERROR,"JDO Error, contact System Administrator for help");
		ErrorMessageBundle.setDefaultMessage(SCHEDULER_ERROR,"Scheduler Error, contact System Administrator for help");    
    }

	/**
	 * @param arg0
	 */
	public SystemException(Throwable arg0) {
		super(arg0);
	}

	/**
	 * @param arg0
	 */
	public SystemException(I18nErrorMessage arg0) {
		super(arg0);
	}

	/**
	 * @param arg0
	 * @param arg1
	 */
	public SystemException(I18nErrorMessage arg0, Throwable arg1) {
		super(arg0, arg1);
	}

	/**
	 * @param arg0
	 * @param arg1
	 * @param arg2
	 * @param arg3
	 * @param arg4
	 */
	public SystemException(
		Category arg0,
		int arg1,
		Location arg2,
		I18nErrorMessage arg3,
		Throwable arg4) {
		super(arg0, arg1, arg2, arg3, arg4);
	}

}