/*****************************************************************************
    Copyright (c) 2001, SAPMarkets Palo Alto, USA, All rights reserved.

*****************************************************************************/
package com.sap.isa.auction.exception;

// AuctionExceptionBase.java: Chainable base class for MP exceptions.

import java.util.List;

import com.sap.exception.*;
import com.sap.localization.LocalizableText;
import com.sap.tc.logging.Category;
import com.sap.tc.logging.Location;

import com.sap.isa.auction.exception.i18n.*;

public class AuctionExceptionBase extends BaseException implements IAuctionExceptionBase {

	protected AuctionExceptionBaseEx auctionBase = new AuctionExceptionBaseEx();
	
	/**
	 * @param arg0
	 */
	public AuctionExceptionBase(Throwable arg0) {
		super(arg0);
	}

	/**
	 * @param arg0
	 */
	public AuctionExceptionBase(I18nErrorMessage arg0) {
		super(arg0);
		auctionBase.addLocalizableMessage(arg0);		
	}

	/**
	 * @param arg0
	 * @param arg1
	 */
	public AuctionExceptionBase(I18nErrorMessage arg0, Throwable arg1) {
		super(arg0, arg1);
		auctionBase.addLocalizableMessage(arg0);		
	}

	/**
	 * @param arg0
	 * @param arg1
	 * @param arg2
	 * @param arg3
	 * @param arg4
	 */
	public AuctionExceptionBase(
		Category arg0,
		int arg1,
		Location arg2,
		I18nErrorMessage arg3,
		Throwable arg4) {
		super(arg0, arg1, arg2, arg3, arg4);
		auctionBase.addLocalizableMessage(arg3);
	}
	
	/* (non-Javadoc)
	 * @see com.sap.isa.auction.exception.IAuctionExceptionBase#addLocalizableMessage(com.sap.localization.LocalizableText)
	 */
	public void addLocalizableMessage(LocalizableText error) {
		auctionBase.addLocalizableMessage(error);
	}	
	
	/* (non-Javadoc)
	 * @see com.sap.isa.auction.exception.IAuctionExceptionBase#getContext()
	 */
	public Object getContext() {
		return auctionBase.getContext();
	}

	/* (non-Javadoc)
	 * @see com.sap.isa.auction.exception.IAuctionExceptionBase#getLocalizableMessages()
	 */
	public List getLocalizableMessages() {
		return auctionBase.getLocalizableMessages();
	}

	/**
	 * this is used to bind the context under which the exception was raised
	 * Inherited Exceptions can add meaning ful APIs for this.
	 * For multiple Objects use Array or Collection,
	 * Not available across serializations
	 *
	 * @see com.sap.isa.auction.exception.IAuctionExceptionBase#setContext(java.lang.Object)
	 */
	public void setContext(Object ctxt) {
		auctionBase.setContext(ctxt);
	}	
}
