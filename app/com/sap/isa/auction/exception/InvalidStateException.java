package com.sap.isa.auction.exception;

import com.sap.tc.logging.Category;
import com.sap.tc.logging.Location;
import com.sap.isa.auction.exception.i18n.*;

/**
 * Title:
 * Description:
 * Copyright:    Copyright (c) 2003
 * Company:
 * @author
 * @version 1.0
 */

public class InvalidStateException extends SystemException {

	public static final String EXC_ID = "InvalidStateException";
	public static final String NOT_INITIALIZED = EXC_ID + 1;
	public static final String NOT_AVAILABLE = EXC_ID + 2;
	public static final String CLOSED = EXC_ID + 3;	
	public static final String TEMPORARY_UNAVAILABLE = EXC_ID + 4;	
	public static final String EMAIL_NOT_PUBLISH = EXC_ID + 5;
	public static final String EMAIL_NOT_CLOSE = EXC_ID + 6;
	
	
	static {
		ErrorMessageBundle.setDefaultMessage(NOT_INITIALIZED,"{0} is not initialized");
		ErrorMessageBundle.setDefaultMessage(NOT_AVAILABLE,"{0} is not available");
		ErrorMessageBundle.setDefaultMessage(CLOSED,"{0} is closed");
		ErrorMessageBundle.setDefaultMessage(TEMPORARY_UNAVAILABLE,"{0} is temporary unavailable, retry after some time");
		ErrorMessageBundle.setDefaultMessage(EMAIL_NOT_PUBLISH, "Auction {0} is not published yet. Can not send notification");
		ErrorMessageBundle.setDefaultMessage(EMAIL_NOT_CLOSE, "Auction {0} is not closed yet. Can not send winner notification");
				
	}
	
	/**
	 * @param arg0
	 */
	public InvalidStateException(Throwable arg0) {
		super(arg0);
	}

	/**
	 * @param arg0
	 */
	public InvalidStateException(I18nErrorMessage arg0) {
		super(arg0);
	}

	/**
	 * @param arg0
	 * @param arg1
	 */
	public InvalidStateException(I18nErrorMessage arg0, Throwable arg1) {
		super(arg0, arg1);
	}

	/**
	 * @param arg0
	 * @param arg1
	 * @param arg2
	 * @param arg3
	 * @param arg4
	 */
	public InvalidStateException(
		Category arg0,
		int arg1,
		Location arg2,
		I18nErrorMessage arg3,
		Throwable arg4) {
		super(arg0, arg1, arg2, arg3, arg4);
	}

}
