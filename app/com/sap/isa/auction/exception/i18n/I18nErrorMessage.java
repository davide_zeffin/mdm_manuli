package com.sap.isa.auction.exception.i18n;

import com.sap.localization.LocalizableTextFormatter;

/**
 * Each exception contains a list of errors. An error contains information of
 *  ErrorCode. Used for I18n. Lookup in Resource Bundle is done using ErrorCode
 *  Message (Optional)
 *  Params to constuct the message
 * Description:
 * Copyright:    Copyright (c) 2002
 * Company:      SAPMarkets
 * @author
 * @version 1.0
 */

public class I18nErrorMessage extends LocalizableTextFormatter {

    public static final short ERROR = 0;
    public static final short CRITICAL_ERROR = 1;
	public static final short WARNING = 2;    

    protected short severity = ERROR;

	/**
	 * 
	 */
	public I18nErrorMessage() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @param arg0
	 * @param arg1
	 */
	public I18nErrorMessage(ErrorMessageBundle arg0, String arg1) {
		super(arg0, arg1);
		// TODO Auto-generated constructor stub
	}

	/**
	 * @param arg0
	 * @param arg1
	 * @param arg2
	 */
	public I18nErrorMessage(ErrorMessageBundle arg0, String arg1, Object[] arg2) {
		super(arg0, arg1, arg2);
		// TODO Auto-generated constructor stub
	}

	/**
	 * @param arg0
	 * @param arg1
	 * @param arg2
	 * @param arg3
	 */
	public I18nErrorMessage(
	ErrorMessageBundle arg0,
		String arg1,
		Object[] arg2,
		String arg3) {
		super(arg0, arg1, arg2, arg3);
		// TODO Auto-generated constructor stub
	}

    public short getSeverity() {
        return severity;
    }
    
    public void setSeverity(short arg) {
    	if(!(arg == 0 || arg == 1)) throw new IllegalArgumentException(arg + "is invalid");
    	this.severity = arg;
    }
}