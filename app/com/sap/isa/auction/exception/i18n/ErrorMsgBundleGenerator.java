/*
 * Created on Oct 29, 2003
 *
 */
package com.sap.isa.auction.exception.i18n;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Iterator;

/**
 * @author I802891
 *
 */
public final class ErrorMsgBundleGenerator {
	private PrintWriter logger = null;
	private String baseDir = null;
	private String name = null;
	private int maxlength = 32;
	private List classes = null;
	
	private static final String SOURCE = "ErrorMsgBundleGenerator:";
	private static final String RESOURCE_FILE_EXTN = ".properties";	
	public void setBaseDir(String path) {
		if(path != null && path.length() > 0) {
			baseDir = path;
		}
	}
	
	public void setResourceBundleName(String bundle) {
		if(bundle != null && bundle.length() > 0) {
			int idx = bundle.indexOf(RESOURCE_FILE_EXTN);
			if(idx != -1) name = bundle.substring(0,idx);
			else name = bundle;
		}
	}
	
	private void logMessage(String msg) {
		if(logger != null) {
			logger.println(SOURCE + msg);
			logger.flush();
		}
	}
	
	private void logException(Exception ex) {
		if(logger != null) {
			logger.println(SOURCE + ex.getMessage());
			logger.flush();
		}
	}
	
	public String getResourceBundleName() {
		return name;
	}
	
	public void setExceptionClasses(List list) {
		this.classes = list;
	}

	public List getExceptionClasses() {
		return classes;		
	}
	
	public void setMaxResourceKeyLength(int size) {
		if(size > 0) maxlength = size;
	}
	
	public int getMaxResourceKeyLength() {
		return maxlength;
	}
	
	public PrintWriter getLogWriter() {
		return logger;
	}
	
	public void setLogWriter(PrintWriter pw) {
		logger = pw;
	}
	
	private boolean checkSettings() {
		if(classes == null || baseDir == null || name == null) return false;
		return true;
	}
	
	public void generate() {
		logMessage("Starting error message resource bundle generation, base dir = " + baseDir + ", name = " + name);
		
		if(!checkSettings()) {
			logMessage("Required arg/property settings missing, generation has been terminated");
			return;
		}
						
		Map map = ErrorMessageBundle.getDefaultMessageBundle().getMessages();
		
		Iterator it = map.keySet().iterator();
		
		loadClasses();
		
		try {
			FileWriter writer = openResourceWriter();
					
			while(it.hasNext()) {
				String key = (String)it.next();
				String value = (String)map.get(key);
				
				String entry = getResourceKey(key) + "=" + value + '\n';
				writer.write(entry);
			}
			
			writer.flush();
			writer.close();
		}
		catch(IOException ex) {
			logException(ex);
			logMessage("An error occured, resource bundle generation terminated");
			return;
		}
		
		logMessage("Finished error message resource bundle generation");
	}
	
	private void loadClasses() {
		for(int i = 0; i < classes.size(); i++) {
			String cname = (String)classes.get(i);
			try {
				Class.forName(cname);
			}
			catch(Exception ex) {
				logMessage("Failed to load exception class " + cname);
			}
		}
	}
	
	private FileWriter openResourceWriter() throws IOException {
		String fname = name;
		fname = toCanonicalForm(fname);
		
		File file = new File(baseDir,fname + RESOURCE_FILE_EXTN);
		file.createNewFile();
		FileWriter fw = new FileWriter(file);
		
		return fw;
	}
	
	private String toCanonicalForm(String name) {
		return name.replace('.',File.separatorChar);
	}
	
	private String getResourceKey(String key) {
		if(key.length() > maxlength) {
			String subkey = key.substring(key.lastIndexOf(".") + 1);
			if(subkey.length() > maxlength) {
				subkey = subkey.substring(subkey.length() - maxlength);
			}
			return subkey;
		}
		else {
			return key;
		}
	}
	
	/**
	 * This is a utility that can be run during build time to generate
	 * default EN_US error message bundle
	 * Options:
	 * -d <base dir> (quotes around dir path can be used)
	 * -f <bundle name> (a valid file name, . is treated as file separator)
	 * -mx <max resource key length, default is 32>
	 * -ls <list of exception classes separated by whitespace>
	 * 
	 * @param args
	 */
	public static void main(String[] args) {
		HashMap map = parseCmdLineArgs(args);
		if(map != null) {
			boolean valid = validateArgs(map);
			System.out.println("ErrMsgBudleGenerator: invalid command line args, generation failed");
			System.out.println(usage);			
			if(!valid) return;
		}
		else {
			System.out.println("ErrMsgBudleGenerator: invalid command line args, generation failed");
			System.out.println(usage);			
			return;
		}
		
		ErrorMsgBundleGenerator generator = new ErrorMsgBundleGenerator();
		generator.setBaseDir((String)map.get(OPTION_BASEDIR));
		generator.setResourceBundleName((String)map.get(OPTION_BUNDLENAME));
		generator.setMaxResourceKeyLength(Integer.parseInt((String)map.get(OPTION_MX_RES_KEY_LENGTH)));
		generator.setLogWriter(new PrintWriter(System.out));
		generator.setExceptionClasses((List)map.get(OPTION_EXCEPTION_CLASS_LIST));
		generator.generate();
	}
	
	private static final String OPTION_BASEDIR = "-d";
	private static final String OPTION_BUNDLENAME = "-f";
	private static final String OPTION_MX_RES_KEY_LENGTH = "-mx";
	private static final String OPTION_EXCEPTION_CLASS_LIST = "-ls";	
	
	private static final String usage = 
	"ErrMsgBundleGenerator: ErrorMsgBundleGenerator -d <base dir> -f <bundleName> -mx <max resource key length> -ls <exception class list>"; 
	private static HashMap parseCmdLineArgs(String[] args) {
		if(args.length < 3) {
			System.out.println(usage);
			return null; 
		}
		
		HashMap map = new HashMap();
		ArrayList list = null;
		boolean optionLS = false;
		
		for(int i = 0; i < args.length;) {
			String arg = args[i];
			
			if(arg.startsWith(OPTION_BASEDIR)) {
				optionLS = false;
				String dir = null;
				if(arg.length() > 2) {
					dir = arg.substring(2);
					dir = dir.trim();
				}
				else {
					dir = args[++i];
				}
				map.put(OPTION_BASEDIR,dir);
			}
			
			if(arg.startsWith(OPTION_BUNDLENAME)) {
				optionLS = false;
				String name = null;
				if(arg.length() > 2) {
					name = arg.substring(2);
					name = name.trim();
				}
				else {
					name = args[++i];
				}
				map.put(OPTION_BUNDLENAME,name);
			}
			
			if(arg.startsWith(OPTION_MX_RES_KEY_LENGTH)) {
				optionLS = false;
				String size = null;
				if(arg.length() > 3) {
					size = arg.substring(3);
					size = size.trim();
				}
				else {
					size = args[++i];
				}
				map.put(OPTION_MX_RES_KEY_LENGTH,size);				
			}
			
			if(arg.startsWith(OPTION_MX_RES_KEY_LENGTH)) {
				list = new ArrayList();
				optionLS = true;
			}
			
			if(optionLS) {
				list.add(args[i]);
			}
			
			i++;
		}
		return map;
	}
	
	private static boolean validateArgs(HashMap args) {
		String dir = (String)args.get(OPTION_BASEDIR);
		boolean valid = true;
		if(dir == null || dir.length() == 0) {
			valid = false;
			System.out.println("ErrMsgBundleGenerator:" + OPTION_BASEDIR + " = " + dir + " is invalid");
		}
		java.io.File file = new File(dir);
		if(!file.isDirectory()) {
			valid = false;			
			System.out.println("ErrMsgBundleGenerator:" + OPTION_BASEDIR + " = " + dir + " is invalid");
		}
		
		String name = (String)args.get(OPTION_BUNDLENAME);
		if(name == null || name.length() == 0) {
			valid = false;
			System.out.println("ErrMsgBundleGenerator:" +OPTION_BUNDLENAME + " = " + name + " is invalid");
		}
		else {
			
		}
		
		List list = (List)args.get(OPTION_EXCEPTION_CLASS_LIST);
		if(list.size() == 0) {
			valid = false;
			System.out.println("ErrMsgBundleGenerator:" + OPTION_MX_RES_KEY_LENGTH + " is invalid");				
		}
		
		return valid;
	}
}
