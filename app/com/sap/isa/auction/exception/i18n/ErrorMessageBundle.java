package com.sap.isa.auction.exception.i18n;

import java.util.HashMap;

import com.sap.localization.*;

/**
 * Title:        Internet Sales
 * Description:
 * Copyright:    Copyright (c) 2002
 * Company:      SAP Labs, PA
 * @author: e-auctions
 * @version 0.1
 */
    /****************************************
     *
     * Basic Error Codes are defined here for Errors Raised
     * by Auctions. Each Exceptions can define a set of Error
     * IDs corresponding to diff use cases.
     *
     * Exceptions may choose to define errors here or in individual classes
     * But All exception should reserve a specific range.
     * Also, keep appending to the following table for easier maintenance
     *
     * +----------------------------------------------------------------+
     * |    Exception                       |       Error Range         |
     * +----------------------------------------------------------------+
     * | Generic Error Codes                |   0  -  100 (Reserved)    |
     * | ValidationException                |   101 - 200               |
     * | UserException                      |   201 - 300               |
     * | AuctionEngineException             |   301 - 500               |
     * | InvocationContextException         |   501 - 550               |
     * | SystemException                    |   601 - 700               |
     * | CRMBackendException                |   701 - 800               |
     * | XMLBindingException                |   801 - 850               |
     * | HttpClientException                |   851 - 900 (Reserved to  |
     * |							be used in core pacakge httpclient) |
     * +----------------------------------------------------------------+
     *
     *
     * Error Code should follow following format
     *
     * <ExceptionName>_<ErrorNo> e.g com.sap.isa.auction.businessobject.UserException_101
     *
     ****************************************/
public final class ErrorMessageBundle extends ResourceAccessor {

    public static final String INVALID_NAME = "";
    public static final String UNKNOWN_ERROR = "";
	
	public static final String ERROR_RESOURCE_BUNDLE_NAME = "ErrorResourceBundle";
	
	private static ErrorMessageBundle singleton = null;
	
	private HashMap masterList = new HashMap();
		
    private EN_USMessageBundle msgBundle = new EN_USMessageBundle();
    
    private ErrorMessageBundle(String resourcebundle) {
    	super(resourcebundle);
    	// reserve 
    }

	public static synchronized ErrorMessageBundle getInstance() {
		if(singleton == null) {
			singleton = new ErrorMessageBundle(ERROR_RESOURCE_BUNDLE_NAME);
		}
		return singleton;
	}
	
	static EN_USMessageBundle getDefaultMessageBundle() {
		return getInstance().msgBundle;
	}
	
	public static void setDefaultMessage(String errorCode, String msg) {
		getInstance().msgBundle.setMessage(errorCode,msg);
	}

	public static String getDefaultMessage(String errorCode) {
		return getInstance().msgBundle.getMessage(errorCode);
	}
	
	/**
	 * Factory for creating Internationalized Error Message
	 * @return
	 */
	public I18nErrorMessage newI18nErrorMessage() {
		I18nErrorMessage msg = new I18nErrorMessage();
		msg.setResourceAccessor(this);
		return msg;
	}
	
	/**
	 * Factory for creating Internationalized Error Message
	 * @return
	 */
	public I18nErrorMessage newI18nErrorMessage(String key) {
		return new I18nErrorMessage(this,key);
	}
		
	/**
	 * Factory for creating Internationalized Error Message
	 * @return
	 */
	public I18nErrorMessage newI18nErrorMessage(String key, Object[] args) {
		return new I18nErrorMessage(this,key,args);
	}
	
	public I18nErrorMessage newI18nMessage(String key, Object[] args, String arg) {
		return new I18nErrorMessage(this,key,args,arg);
	}
}