package com.sap.isa.auction.exception.i18n;

import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import com.sap.localization.ResourceAccessor;

/**
 * This class provides the default Message Bundle in case the Internationalization
 * is not used. This bundler provides messages in EN_US locale
 * Copyright:    Copyright (c) 2002
 * Company:      SAPMarkets
 * @author
 * @version 1.0
 */

class EN_USMessageBundle extends ResourceAccessor {

	private HashMap messages = new HashMap();
	EN_USMessageBundle() {
		super("");
	}

	Map getMessages() {
		return messages;
	}
	
	/**
	 *
	 */
	String getMessage(String errorCode) {
		return (String)messages.get(errorCode);
	}

	void setMessage(String errorCode, String message) {
		messages.put(errorCode,message);
	}
    
	/* (non-Javadoc)
	 * @see com.sap.localization.ResourceAccessor#getMessageText(java.util.Locale, java.lang.String)
	 */
	public String getMessageText(Locale arg0, String arg1) {
		return getMessage(arg1);
	}

}
