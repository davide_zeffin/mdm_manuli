/*
 * Created on Apr 20, 2004
 *
 * To change the template for this generated file go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
package com.sap.isa.auction.actionforms;

/**
 * @author I803067
 *
 * To change the template for this generated type comment go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
public interface BaseFormConstants {

	public static final String ERRORSEDIT          = "ErrorsEdit.Auction";
	public static final String ERRORSTEXT          = "ErrorsText.Auction";
	public static final String ERRORSLABEL         = "ErrorsLabel.Auction";
	public static final String LOGCATEGORY		   = "/ISA/AUCTION/SELLER/UI";
	public static final String CREATIONNOTPOSSIBLE_1 = "CreationNotPossible.Error";
	public static final String CREATIONNOTPOSSIBLE_2 = "CreationNotPossible2.Error";
	public static final String APPLICATIONHEADER = "Title.Explorer";
	public static final String SYSTEMERRORLIST = "ErrorList.BaseForm";
	public static final String CRITICALERROR = "CriticalError.BaseForm";

	public static final String HEADERERRORS         = "Header.Error";
	public static final String SHIPINFOERRORS       = "Shipping.Error";
	public static final String PRODUCTSERRORS       = "Products.Error";
	
	public static final String BASEBUNDLE			= "crm~auctionbase~resources";
}
