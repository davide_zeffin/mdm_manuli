/*
 * Created on Apr 26, 2004
 *
 * To change the template for this generated file go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
package com.sap.isa.auction.actionforms;

import com.sap.isa.auction.actions.BaseAction;
import com.sap.isa.auction.businessobject.b2x.AuctionBusinessObjectManager;
import com.sap.isa.auction.businessobject.user.Seller;
import com.sap.isa.auction.helpers.CatalogHelper;
import com.sap.isa.auction.helpers.HelpManager;
import com.sap.isa.auction.helpers.LoginHelper;

import com.sap.isa.core.SessionConst;
import com.sap.isa.core.UserSessionData;
import com.sap.isa.core.init.InitializationEnvironment;
import com.sap.isa.core.util.StartupParameter;
import com.sap.isa.user.util.LoginStatus;
import com.sapportals.htmlb.InputField;

/**
 * @author I803067
 *
 * To change the template for this generated type comment go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
public class LoginForm extends BaseForm implements LoginFormConstants {
	
	private InputField userIdInput;
	private InputField passwdInput;
	
	public void initializeFields()  {
		userIdInput = (InputField)addControl(HTMLB_INPUTFIELD , USERID);
		passwdInput = (InputField)addControl(HTMLB_INPUTFIELD , PASSWD);
	}
	
	/**
	 * @deprecated
	 * Due to UME migration 
	 * pb: 01/18/05 
	 * @return
	 */
	public boolean login()  {
		boolean retval = true;
		
		String userId = getValue(USERID);
		String passwd = getValue(PASSWD);
		if(userId != null)  {
			UserSessionData userSession =
				UserSessionData.getUserSessionData(request.getSession());
			AuctionBusinessObjectManager auctionBOM = (AuctionBusinessObjectManager)
						userSession.getBOM(AuctionBusinessObjectManager.AUCTION_BOM);
			Seller seller = null;
			seller = (Seller)auctionBOM.createUserBase();
			
			seller.setUserId(userId);
			seller.setLanguage(userSession.getLocale().getLanguage());
			LoginStatus status = LoginStatus.NOT_OK;
			try  {
				status = seller.login(userId , passwd);
			}  catch(Exception exc)  {
				logInfo("Unable to login" , exc);
				retval = false;
			}
			if(status.toInteger() == LoginStatus.OK.toInteger())  {
				retval = true;
				LoginHelper loginHelper = (LoginHelper)getHelper(HelpManager.LOGINHELPER); 
				if (isAdminLogin())
					loginHelper.setSellerLogin(false);
				else
					loginHelper.setSellerLogin(true);
				loginHelper.getGreetingUserNameText();
			}  else  {
				retval = false;  
			}
		}
		return retval;
	}

	public boolean loadCatalog() {
		UserSessionData userSession =
			UserSessionData.getUserSessionData(request.getSession());
		StartupParameter startParameter = (StartupParameter)
				userSession.getAttribute(SessionConst.STARTUP_PARAMETER);
		
		/**
		 * Set the seller logon language. Later this will be used for language
		 * for shop
		 */		
		AuctionBusinessObjectManager auctionBOM = (AuctionBusinessObjectManager)
					userSession.getBOM(AuctionBusinessObjectManager.AUCTION_BOM);
		Seller seller = null;
		seller = (Seller)auctionBOM.createUserBase();
						
		if(startParameter.getParameter(LoginForm.SHOPID) != null)  {
			CatalogHelper helper = (CatalogHelper)getHelper(HelpManager.CATALOGHELPER);
			if(helper.loadCatalog(startParameter.getParameterValue(LoginForm.SHOPID) ,
				(BaseAction) action , request , (InitializationEnvironment)servlet))
				return true;
		}
		return false;
	}

	/**
	 * pb 01/18/05: Admin role used to be determined based on url parameter
	 * From 5.0 onwards it will be determined by the user authorizations in the backend. 
	 * @return true if the user is an administrator
	 * false otherwise
	 */
	public boolean isAdminLogin() {

		UserSessionData userSession =
						UserSessionData.getUserSessionData(request.getSession());
		AuctionBusinessObjectManager auctionBOM = (AuctionBusinessObjectManager)
					userSession.getBOM(AuctionBusinessObjectManager.AUCTION_BOM);
		Seller seller = null;
		seller = (Seller)auctionBOM.createUserBase();	
		return seller.isAdministrator();		
	}
}
