/*
 * Created on Apr 29, 2004
 *
 * To change the template for this generated file go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
package com.sap.isa.auction.actionforms;

/**
 * @author I803067
 *
 * To change the template for this generated type comment go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
public interface BaseConstants {
	
	public static final String AUCTIONSELLBUNDLE = "seller";
	public static final String AUCTIONBIDDERBUNDLE = "auctionbidder";
	public static final String AUCTIONADMINBUNDLE = "admin";
	
	public static final String HEADERFRAME = "headerFrame";
	public static final String DESKTOPFRAME = "desktopFrame";
}
