/*
 * Created on May 6, 2004
 *
 * To change the template for this generated file go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
package com.sap.isa.auction.actionforms.buyer;

import java.math.BigDecimal;
import java.util.Collection;

import com.sap.isa.auction.bean.AuctionItem;
import com.sap.isa.auction.bean.AuctionTypeEnum;
import com.sap.isa.auction.bean.Winner;
import com.sap.isa.auction.bean.b2x.PrivateAuction;
import com.sap.isa.auction.bean.b2x.PrivateAuctionWinner;
import com.sap.isa.auction.helpers.CatalogHelper;
import com.sap.isa.auction.helpers.ConversionHelper;
import com.sap.isa.auction.helpers.HelpManager;
import com.sap.isa.auction.helpers.buyer.BidsHelper;
import com.sap.isa.auction.helpers.buyer.CheckoutHelper;
import com.sap.isa.auction.helpers.buyer.SearchAuctionsHelper;
import com.sap.isa.catalog.webcatalog.WebCatItem;
import com.sap.isa.core.TechKey;
import com.sap.isa.core.util.JspUtil;

/**
 * @author I803067
 *
 * To change the template for this generated type comment go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
public class AuctionDetailsForm extends AuctionBaseForm  implements AuctionDetailsConstants  {

	private PrivateAuction auction;
	private boolean invitation = false;
	
	public void setAuction(String index)  {
		SearchAuctionsHelper searchHelper = (SearchAuctionsHelper)
									getHelper(HelpManager.BUYERSEARCHHELPER);
		auction = searchHelper.getAuctionByIndex(index);
	}
	
	public String getPageTitle()  {
		if(productId != null)  {
			WebCatItem webCatItem = getProduct();
			if(webCatItem != null)
				return "Auctions on " + webCatItem.getDescription();
		}
		return "Auctions";
	}
	
	public PrivateAuction getAuction()  {
		return auction;
	}
	
	public void refresh() {
		SearchAuctionsHelper searchHelper = (SearchAuctionsHelper)
											getHelper(HelpManager.BUYERSEARCHHELPER);
		auction = searchHelper.refreshAuction(auction.getAuctionId());
	}
	public int getTotalBids(){
		return this.getBids().size();
	}
	public String getNextMinimumBid(PrivateAuction auction)  {
		BidsHelper bidHelper = (BidsHelper)getHelper(HelpManager.BIDSHELPER);
		BigDecimal bid = bidHelper.getHighestBid(auction);
		
		if(bid != null && bid.doubleValue() > 0.0)  {
			return getDisplayCurrencyValue(bid.add(auction.getBidIncrement())); 
		}  else  {
			return getDisplayCurrencyValue(auction.getStartPrice());
		}
	}
	
	public void bid()  {
		final String methodName = "PlaceBid";
		entering(methodName);
		String amount = JspUtil.encodeHtml(request.getParameter(BIDAMOUNTPARAMETER));
		String quantity = request.getParameter(BIDQUANTITYPARAMETER);
		BigDecimal value = new BigDecimal(0.0);
		BigDecimal qty = new BigDecimal(1.0);
		try  {
			ConversionHelper conversionHelper = (ConversionHelper)getHelper(HelpManager.CONVERSIONHELPER);
			value = conversionHelper.uiCurrencyStringToBigDecimal(amount);
			if(auction.getType() == AuctionTypeEnum.BROKEN_LOT.getValue())
				qty = conversionHelper.uiQuantityStringToBigDecimal(quantity);
			BidsHelper bidHelper = (BidsHelper)
						getHelper(HelpManager.BIDSHELPER);
			if(!bidHelper.placeBid(auction , value , qty.doubleValue()))  {
				setError(bidHelper.getError());
				request.setAttribute(BIDAMOUNTPARAMETER , amount);
				request.setAttribute(BIDQUANTITYPARAMETER , quantity);
			}
		}  catch(Exception exc)  {
			logError("Unable to place a bid " , exc);
			request.setAttribute(BIDAMOUNTPARAMETER , amount);
			request.setAttribute(BIDQUANTITYPARAMETER , quantity);
			addError(translate(INVALIDBID));
		}
		exiting(methodName);
	}
	
	public Collection getBids()  {
		final String methodName = "GetBidHistory";
		entering(methodName);
		BidsHelper bidHelper = (BidsHelper)getHelper(HelpManager.BIDSHELPER);
		Collection bids = bidHelper.getBids(auction);
		exiting(methodName);
		return bids;
	}
	
	public String getTermsAndConditions()  {
		String termsAndConditions = "";
		if ((null != auction) && (null != auction.getAuctionTerms())){			
			if (!auction.getAuctionTerms().equalsIgnoreCase("null")){
				termsAndConditions = auction.getAuctionTerms();
			}
		}
		return termsAndConditions;
	}
	
	public String getBuyNowPrice()  {
		if(auction.getBuyItNowPrice() != null && auction.getBuyItNowPrice().doubleValue() > 0.0)  {
			return getDisplayCurrencyValue(auction.getBuyItNowPrice());
		}  else  {
			return null;
		}
	}
	
	public WebCatItem getProduct(Object item)  {
		CatalogHelper catalogHelper = (CatalogHelper)
								getHelper(HelpManager.CATALOGHELPER);
		return catalogHelper.getWebCatItem(((AuctionItem)item).getProductId());
	}
	
	public boolean isBrokenLot()  {
		if(auction != null && AuctionTypeEnum.BROKEN_LOT.getValue() ==
					auction.getType())  {
			return true;
		}
		return false;
	}
	
	public boolean isCheckedOut(Winner winner)  {
		if(winner != null)  {
			return ((PrivateAuctionWinner)winner).isCheckedOut();
		}
		return false;
	}
	public String buildOrderLink(Winner winner) {
		TechKey tekkey = winner.getOrderGuid();
		String orderId = winner.getOrderId();
		StringBuffer sb = new StringBuffer();
		sb.append("/b2b/documentstatusdetailprepare.do?");
		sb.append("techkey=");
		sb.append(tekkey.getIdAsString());
		sb.append("&object_id=");
		sb.append(orderId);
		sb.append("&objecttype=order");
		return sb.toString();
	}
	public Winner getWinner()  {
		if(auction != null)  {
			try  {
				CheckoutHelper checkoutHelper = (CheckoutHelper)
									getHelper(HelpManager.CHECKOUTHELPER);
				return checkoutHelper.getWinner(auction);
			} catch(Exception exc)  {
				logError("Unable to retrieve winner for auction - " + auction.getAuctionName() , exc);
			}
		}
		return null;
	}
	
	public boolean buyNow()  {
		String quantity = request.getParameter(BIDQUANTITYPARAMETER);
		BigDecimal qty = new BigDecimal(1.0);
		try  {
			BidsHelper helper = (BidsHelper)getHelper(HelpManager.BIDSHELPER);
			ConversionHelper conversionHelper = (ConversionHelper)getHelper(HelpManager.CONVERSIONHELPER);
			if(null != quantity)  {
				qty = conversionHelper.uiQuantityStringToBigDecimal(quantity);
			}
			if(!helper.buyNow(auction , qty))  {
				setError(helper.getError());
			} else  {
				return true;
			}
		}  catch(Exception exc)  {
			logError("Unable to buy now the auction" , exc);
			addError(translate(INVALIDBUYNOW));
		}
		return false;
	}

	/**
	 * @param string
	 * @return
	 */
	public boolean loadAuction(String auctionId) {
		try  {
			
			SearchAuctionsHelper searchHelper = (SearchAuctionsHelper)
										getHelper(HelpManager.BUYERSEARCHHELPER);
			auction = searchHelper.loadAuction(auctionId);
			if(auction != null)
				return true;
  		}  catch(Exception exc)  {
  			logError("Unable to load the auction - " + auctionId , exc);
  		}
		return false;
	}
	
	public String getProductId()  {
		return (productId != null) ? productId : "";
	}
	/**
	 * @return
	 */
	public boolean isInvitation() {
		return invitation;
	}

	/**
	 * @param b
	 */
	public void setInvitation(boolean b) {
		invitation = b;
	}

}
