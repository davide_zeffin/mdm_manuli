/*
 * Created on May 27, 2004
 *
 * To change the template for this generated file go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
package com.sap.isa.auction.actionforms.buyer;

import com.sap.isa.auction.bean.b2x.PrivateAuction;
import com.sap.isa.auction.helpers.HelpManager;
import com.sap.isa.auction.helpers.buyer.CheckoutHelper;
import com.sap.isa.auction.helpers.buyer.SearchAuctionsHelper;
import com.sap.isa.businessobject.BusinessObjectManager;
import com.sap.isa.businessobject.DocumentState;
import com.sap.isa.businessobject.SalesDocument;
import com.sap.isa.businessobject.SalesDocumentStatus;
import com.sap.isa.businessobject.header.HeaderSalesDocument;
import com.sap.isa.businessobject.order.OrderStatus;
import com.sap.isa.core.SessionConst;
import com.sap.isa.core.UserSessionData;
import com.sap.isa.isacore.DocumentHandler;
import com.sap.isa.isacore.ManagedDocument;
import com.sap.isa.isacore.ManagedDocumentLargeDoc;
import com.sap.isa.isacore.action.order.MaintainOrderBaseAction;

/**
 * @author I803067
 *
 * To change the template for this generated type comment go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
public class CheckoutAuctionsForm extends BaseForm {

	public static final String AUCTIONID = "auctionId";
	public static final String CHECKOUT = "checkout";
	public static final String AUCTIONGUID = "auctionGuid";
	public static final String CHECKOUTTEXT = "CAF.Checkout";
	public static final String NOAUCTIONSFORCHTEXT = "CAF.NoAuctions";

	protected String href_action = "b2b/documentstatusdetailprepare.do";

	/** 
	 * forward for ManagedDocument Class.
	 * overwrite with apropriate action
	 */
	protected String forward = "order_status";

	private PrivateAuction auction;
	private com.sap.isa.auction.actionforms.buyer.AuctionsForm form;

	public void setAuctionOrderForCheckout(String auctionId, String checkout) {
		if (null == auctionId)
			return;

		SearchAuctionsHelper searchHelper =
			(SearchAuctionsHelper) getHelper(HelpManager.BUYERSEARCHHELPER);
		auction = searchHelper.getAuctionByIndex(auctionId);
		if (null == auction)
			auction = searchHelper.loadAuction(auctionId);
		if (null == auction)
			return;
		if (checkout != null && checkout.equals(CHECKOUT)) {
			CheckoutHelper checkHelper =
				(CheckoutHelper) getHelper(HelpManager.CHECKOUTHELPER);
			OrderStatus orderStatus =
				checkHelper.initializeAuctionOrder(auction);
			if (null == orderStatus)
				return;
			UserSessionData userSessionData =
				UserSessionData.getUserSessionData(session);
			BusinessObjectManager bom =
				(BusinessObjectManager) userSessionData.getBOM(
					BusinessObjectManager.ISACORE_BOM);
			DocumentHandler documentHandler =
				(DocumentHandler) userSessionData.getAttribute(
					SessionConst.DOCUMENT_HANDLER);
			userSessionData.setAttribute(
				"EAUCTION_PARAMID",
				auction.getAuctionId());					
			try {
				addToDocumentHandler(documentHandler, orderStatus, bom);
			} catch (Exception exc) {
				logError(
					"Unable to set the Document to the Document handler",
					exc);
			}
		}
	}

	protected void addToDocumentHandler(
		DocumentHandler documentHandler,
		OrderStatus orderStatusDetail,
		BusinessObjectManager bom)
		throws Exception {

		HeaderSalesDocument headerSalesDoc;

		headerSalesDoc = orderStatusDetail.getOrderHeader();

		if (documentHandler.getManagedDocumentOnTop()
			instanceof ManagedDocumentLargeDoc
			&& documentHandler.getManagedDocumentOnTop().getDocument()
				instanceof SalesDocumentStatus) {
			String techKeyStr1 = orderStatusDetail.getTechKey().getIdAsString();
			String hrefStr2 = documentHandler.getManagedDocumentOnTop().getHrefParameter();
            
			if (hrefStr2 != null  &&  hrefStr2.indexOf(techKeyStr1) > 0) {
				return;
			}
		}

		String href_parameter =
			(
				"techkey=".concat(
					headerSalesDoc.getTechKey().getIdAsString())).concat(
				"&");
		href_parameter =
			(
				(href_parameter.concat("object_id=")).concat(
					headerSalesDoc.getSalesDocNumber())).concat(
				"&");
		href_parameter =
			(href_parameter.concat("objects_origin=")).concat(
				headerSalesDoc.getSalesDocumentsOrigin());

		// expand HREF by documenttype
		href_parameter =
			(href_parameter.concat("&objecttype=")).concat(
				headerSalesDoc.getDocumentType());

		//orderStatusDetail.setState(DocumentState.VIEW_DOCUMENT);
		DocumentState state = null;

		ManagedDocument mDoc = null;
		//create managed document for large documents, even if it might not be necessary
		mDoc =
			new ManagedDocumentLargeDoc(
				orderStatusDetail,
				headerSalesDoc.getDocumentType(),
				headerSalesDoc.getSalesDocNumber(),
				headerSalesDoc.getPurchaseOrderExt(),
				headerSalesDoc.getDescription(),
				headerSalesDoc.getChangedAt(),
				forward,
				href_action,
				href_parameter);
		mDoc.setState(DocumentState.VIEW_DOCUMENT);
		if (orderStatusDetail
			.isLargeDocument(bom.getShop().getLargeDocNoOfItemsThreshold())) {
			// create managed document for large documents
			((ManagedDocumentLargeDoc) mDoc).setItemSearchStatus(
				ManagedDocumentLargeDoc.ITEM_STATUS_NOT_SPECIFIED);
			((ManagedDocumentLargeDoc) mDoc).setItemSearchProperty(
				ManagedDocumentLargeDoc.ITEM_PROPERTY_NONE);
			((ManagedDocumentLargeDoc) mDoc).setItemSearchPropertyHighValue("");
			((ManagedDocumentLargeDoc) mDoc).setItemSearchPropertyLowValue("");
		}

		documentHandler.add(mDoc);
		documentHandler.setOnTop(orderStatusDetail);
		// needed if the catalog is displayed and a document gets selected from the history
		documentHandler.setCatalogOnTop(false);
	}

	public void checkoutPostProcessing() {
		try {
			CheckoutHelper checkoutHelper =
				(CheckoutHelper) getHelper(HelpManager.CHECKOUTHELPER);
			SalesDocument order = checkoutHelper.postProcessingCheckout();
			if (order != null) {
				request.setAttribute(
					MaintainOrderBaseAction.RC_HEADER,
					order.getHeader());
				request.setAttribute(
					MaintainOrderBaseAction.RC_ITEMS,
					order.getItems());
				request.setAttribute(
					MaintainOrderBaseAction.RC_SHIPCOND,
					order.readShipCond(checkoutHelper.getShop().getLanguage()));
				request.setAttribute(
					MaintainOrderBaseAction.RC_MESSAGES,
					order.getMessageList());
				UserSessionData userSessionData =
					UserSessionData.getUserSessionData(session);
				userSessionData.setAttribute(
					MaintainOrderBaseAction.SC_SHIPTOS,
					order.getShipTos());
			}

		} catch (Exception exc) {
			logError("Unable to finish checkout process", exc);
		}
	}

}
