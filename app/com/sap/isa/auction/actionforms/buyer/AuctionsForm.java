/*
 * Created on May 5, 2004
 *
 * To change the template for this generated file go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
package com.sap.isa.auction.actionforms.buyer;

import java.util.Hashtable;
import java.util.Iterator;

import com.sap.isa.auction.bean.b2x.PrivateAuction;
import com.sap.isa.auction.bean.AuctionStatusEnum;
import com.sap.isa.auction.bean.search.QueryResult;
import com.sap.isa.auction.helpers.HelpManager;
import com.sap.isa.auction.helpers.SearchResult;
import com.sap.isa.auction.helpers.buyer.SearchAuctionsHelper;
import com.sap.isa.catalog.webcatalog.WebCatItem;
import com.sap.isa.catalog.webcatalog.WebCatItemPage;

/**
 * @author I803067
 *
 * To change the template for this generated type comment go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
public class AuctionsForm extends AuctionBaseForm implements AuctionsFormConstants  {

	private String contextInfo;
    private String rcStatus;
	private SearchResult result;
	private int fetchSize = 5;
	private int startIndex = 1;
	private int currentPage = 1;
	private Hashtable pages;
	
	public void setFetchSize(int size) {
		if(size > 0)
			this.fetchSize	= size;
	}
	
	public int getCurrentPage()  {
		return this.currentPage;  
	}
	
	public void setCurrentPage(int pageNumber)  {
		this.currentPage = pageNumber;
	}
	
	public void lookforAuctionsInCurrentCategory()  {
		SearchAuctionsHelper searchHelper = (SearchAuctionsHelper)
								getHelper(HelpManager.BUYERSEARCHHELPER);
		WebCatItemPage itemPage = (WebCatItemPage)request.getAttribute("itemPage");
		if(itemPage != null)  {
			Iterator iterator = itemPage.iterator();
			WebCatItem webCatItem = null;
			String parameter = PRODUCTID + "=";
			QueryResult queryResult = null;
			while(iterator.hasNext())  {
				webCatItem = (WebCatItem)iterator.next();
				if(isAuctioned(webCatItem.getProductID()))  {
					webCatItem.setEAuctionLink(webCatItem.getProductID());
				}  else  {
					webCatItem.setEAuctionLink("false");
				}
			}
		}
	}
	
	public SearchResult searchAuctionsForProduct(String productId)  {
		setProductId(productId);
		setContextInfo(CATALOGCONTEXT);
		SearchAuctionsHelper searchHelper = (SearchAuctionsHelper)
								getHelper(HelpManager.BUYERSEARCHHELPER);
		result = searchHelper.getAuctionsForProductID(productId);
		searchHelper.setCurrentResult(result);
		initPaging();
		setPageContext();
		return result;
	}
	
	public void setPageContext()  {
		if(request.getParameter(STARTINDEX) != null)  {
			int next = getIntValue(request.getParameter(STARTINDEX));
			if(next > 0 && next <= result.getSize())  {
				startIndex = next;
			}
			if(next < 0 || startIndex < 0)  {
				startIndex = 1;
			}
		}
		
		if(request.getParameter(PAGESIZE) != null)  {
			int size = getIntValue(request.getParameter(PAGESIZE));
			if(size > -1 && ((size + 1)*5 != getPageSize()))  {
				setFetchSize((size + 1) * 5);
				initPaging();
			}
		}
		if(request.getParameter(SHOWPAGEPARAMETER) != null)  {
			int showPage = getIntValue(request.getParameter(SHOWPAGEPARAMETER));
			if(showPage > -1)  {
				Paging paging = (Paging)pages.get(Integer.toString(showPage));
				if(paging != null)  {
					setCurrentPage(showPage);
					startIndex = paging.startIndex;
				}
			}
				
			
		}
		try  {
			if(!result.isConnected())
				result.connect();
			result.fetch(startIndex , startIndex + getPageSize());
			result.disconnect();
		}  catch(Exception exc)  {
			logError("Unable to fetch the auctions from result set -> " + startIndex + " - " + startIndex + fetchSize , exc);
		}
	}
	
	private void setContextInfo(String context)  {
		this.contextInfo = context;
	}
	
	public boolean isAuctioned(String productId)  {
		SearchAuctionsHelper searchHelper = (SearchAuctionsHelper)
								getHelper(HelpManager.BUYERSEARCHHELPER);
		return searchHelper.isAuctioned(productId);
	}
	
	public SearchResult getAuctions()  {
		return result;
	}
	
	public PrivateAuction getAuctionByIndex(int index)  {
		return (PrivateAuction)result.fetch(index);
	}

	public int getStartIndex()  {
		return startIndex;
	}
	
	public int getResultSize()  {
		if(result != null)
			return result.getSize();
		else
			return 0;
	}
	
	public void setStartIndex(int index)  {
		this.startIndex = index;
	}
	
	
	public int getPageSize()  {
		return fetchSize;
	}
	
	public int getAuctionsCount()  {
		if(result != null)  {
			return result.getSize();	
		}
		return 0;
	}
	
	public String getPageTitle()  {
		String context = getContextInfo();
		if(context.equals(OPTION_ALL))  {
			return translate(ALLPARTICIPATEDTXT);
		}  else if(context.equals(OPTION_ACTIVE))  {
			return translate(ACTIVEPARTICIPATEDTXT);
		}  else if(context.equals(OPTION_FINALIZED))  {
			return translate(FINALIZEDPARTICIPATEDTXT);
		}  else if(context.equals(OPTION_CLOSED))  {
			return translate(CLOSEDPARTICIPATEDTEXT);
		}  else if(context.equals(OPTION_NOCHECKOUT))  {
			return translate(WONAUCTIONSTXT);
		} else if(context.equals(OPTION_CHECKEDOUT))  {
			return translate(WONAUCTIONSCHECKOUTTEXT);
		}  else if(context.equals(CATALOGCONTEXT))  {
			return translate(AUCTIONSONPRODUCT);
		}
		return "";
	}
	
	public String getContextInfo()  {
		return contextInfo;
	}
	
	public int getEndIndex()  {
		SearchAuctionsHelper searchHelper =  (SearchAuctionsHelper)
			getHelper(HelpManager.BUYERSEARCHHELPER);
		SearchResult auctions = searchHelper.getCurrentResult();
		if(null == auctions)
			return 0;
		return getStartIndex() + getPageSize() > auctions.getSize() 
				? auctions.getSize() : getStartIndex() + getPageSize() -1;
	}
	
	private int getIntValue(String intValue)  {
		int i = -1;
		if (intValue != null && !(intValue.equals("null")) && intValue.length() > 0) {
			try  {
				i = Integer.parseInt(intValue);   
			}  catch(Exception exc)  {
				logInfo("Error in getIntValue ", exc);
			}
		}
		return i;
	}

	/**
	 * @param string
	 */
	public void searchAuctionsParticipated(String context) {
		productId = "";
		setContextInfo(context);
		SearchAuctionsHelper searchHelper =  (SearchAuctionsHelper)
					getHelper(HelpManager.BUYERSEARCHHELPER);
		if(context != null)  {
			SearchResult result = null;
			if(context.equals(OPTION_ALL))  {
				result = searchHelper.getParticipatedAuctions(null);
			}  else if(context.equals(OPTION_ACTIVE))  {
				result = searchHelper.getParticipatedAuctions(AuctionStatusEnum.PUBLISHED);
			}  else if(context.equals(OPTION_FINALIZED))  {
				result = searchHelper.getParticipatedAuctions(AuctionStatusEnum.FINALIZED);
			}  else if(context.equals(OPTION_CLOSED))  {
				result = searchHelper.getParticipatedAuctions(AuctionStatusEnum.CLOSED);
			}  else if(context.equals(OPTION_NOCHECKOUT))  {
				result = searchHelper.getWonAuctions();
			} else if(context.equals(OPTION_CHECKEDOUT))  {
				result = searchHelper.getCheckedout();
			}
			searchHelper.setCurrentResult(result);
			if(result != null)  {
				this.result = result;
				initPaging();
				setPageContext();
			}
			this.result = result;
		}  else {
			
		}
	}
	
	public int getNumberOfPages()  {
		if(pages != null)  {
			return pages.size();
		}  else  {
			return 0;
		}
	}
	
	private void initPaging()  {
		startIndex = 1;
		setCurrentPage(1);
		if(pages != null)  {
			pages.clear();	
		}  else  {
			pages = new Hashtable();
		}
		if(getResultSize() > 0)  {
			int i = 1;
			int start = 1;
			int end = getResultSize() > getPageSize() ? getPageSize() : getResultSize();
			pages.put("1" , new Paging(start , end));
			while(end < getResultSize())  {
				start = start + getPageSize() -1;
				end = (start + getPageSize() - 1) > getResultSize() ? getResultSize() :
												(start + getPageSize() - 1);
				i++;
				pages.put(Integer.toString(i) , new Paging(start , end)); 
			}
		}
	}
	
	class Paging  {
		int startIndex;
		int endIndex;
		
		Paging(int start , int end)  {
			startIndex = start;
			endIndex = end;
		}
	}
    
    /**
     * Return the rcStatus
     */
    public String getRcStatus() {
        return rcStatus;
    }
   
    /**
     * Set the rcStatus
     */
    public void setRcStatus(String rcStatus) {
        this.rcStatus = rcStatus;
    }

}
