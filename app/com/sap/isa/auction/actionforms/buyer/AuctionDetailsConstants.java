/*
 * Created on May 7, 2004
 *
 * To change the template for this generated file go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
package com.sap.isa.auction.actionforms.buyer;

/**
 * @author I803067
 *
 * To change the template for this generated type comment go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
public interface AuctionDetailsConstants {
	
	public static final String BIDTYPE = "_bid";
	public static final String BIDDINGNOW = "biddingnow";
	public static final String BUYINGNOW = "buyingnow";
	public static final String BIDAMOUNTPARAMETER = "_bidAmtParameter";
	public static final String BIDQUANTITYPARAMETER = "_bidQtyParameter";
	
	public static final String BIDTEXT		  = "ADF.Bid";
	public static final String NEXTMINBIDTEXT = "ADF.NextMinBid";
	public static final String MINBIDMESSAGETEXT = "ADF.MinBidMessage";
	public static final String BIDAMOUNTTEXT  = "ADF.BidAmount";
	public static final String BIDQUANTITYTEXT = "ADF.BidQuantity";
	public static final String BIDBUTTONTEXT  = "ADF.BidNow";
	public static final String BUYNOWTEXT	  = "ADF.BuyNow";

	public static final String NOBIDSYETTEXT = "ADF.NoBidsYet"; 
	public static final String BIDDERTEXT 	 = "ADF.Bidder";
	public static final String BIDDERAMOUNTTEXT	= "ADF.BidderAmount";
	public static final String BIDDERQUANTITYTEXT = "ADF.BidderQuantity";
	public static final String BIDONTEXT	= "ADF.BidOn";
	
	public static final String INVALIDBID	= "ADF.InvalidBid";
	public static final String INVALIDBUYNOW= "ADF.InvalidBuyNow";
	
	public static final String PRODUCTSTEXT		= "ADF.Products";
	public static final String TERMSANDCONDTEXT	= "ADF.TermsAndConditions";
	public static final String DOYOUWANTBIDTEXT = "ADF.DoYouBid";
	public static final String DOYOUWANTTOBUYNOWTEXT = "ADF.DoYouBuyNow";
	public static final String UNITTEXT		= "ADF.Units";
	public static final String FORTEXT			= "ADF.For";
	public static final String PERUNITTEXT		= "ADF.PerUnit";
	public static final String CHECKOUTTEXT		= "ADF.Checkout";
	public static final String ORDERTEXT		= "ADF.Order";
	public static final String DETAILSTITLETEXT = "ADF.DetailsTitle";
	public static final String LATERCHECKOUTTEXT = "ADF.LaterCheckoutMsg";
	public static final String BUYNOWAUCTIONCONFTITLE = "ADF.BuyNowConfirmation";
	public static final String BUYNOWTITLE		= "ADF.BuyNowTitle";
	public static final String PLEASESELUNITSTEXT = "ADF.PleaseSelUnits";
	public static final String CANCELTEXT		= "ADF.Cancel";
	public static final String REFRESHBUTTONTEXT = "ADF.Refresh";
	public static final String NUMOFBIDS		= "ADF.NumOfBids";
	public static final String CHECKOUTREMINDER = "ADF.CheckoutReminder";
	public static final String CHECKEDOUTMSG = "ADF.CheckedoutMsg";
	public static final String BIDHISTORY	= "ADF.BidHistory";
		
}
