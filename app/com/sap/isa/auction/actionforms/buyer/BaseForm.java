/*
 * Created on May 5, 2004
 *
 * To change the template for this generated file go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
package com.sap.isa.auction.actionforms.buyer;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.ActionForm;

import com.sap.isa.auction.bean.ValidatingBean;
import com.sap.isa.auction.businessobject.ContextManager;
import com.sap.isa.auction.businessobject.UserRoleTypeEnum;
import com.sap.isa.auction.businessobject.ValidationException;
import com.sap.isa.auction.businessobject.b2x.AuctionBusinessObjectManager;
import com.sap.isa.auction.businessobject.b2x.AuctionBuyerRuntime;
import com.sap.isa.auction.businessobject.b2x.InvocationContext;
import com.sap.isa.auction.exception.SystemException;
import com.sap.isa.auction.exception.i18n.I18nErrorMessage;
import com.sap.isa.auction.helpers.HelpManager;
import com.sap.isa.auction.helpers.HelpManagerBase;
import com.sap.isa.auction.helpers.Helper;
import com.sap.isa.auction.helpers.HelperObjectManager;
import com.sap.isa.auction.helpers.LogSupport;
import com.sap.isa.auction.helpers.buyer.SearchAuctionsHelper;
import com.sap.isa.auction.logging.CategoryProvider;
import com.sap.isa.businessobject.BusinessObjectManager;
import com.sap.isa.core.UserSessionData;
import com.sap.isa.core.util.WebUtil;
import com.sap.tc.logging.Category;
import com.sap.tc.logging.Location;

/**
 * @author I803067
 *
 * To change the template for this generated type comment go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
public class BaseForm extends ActionForm  {
	
	public static final String CRITICALERROR = "BF.CriticalError";
	
	protected HttpSession session;
	protected HttpServletRequest request;
	protected Object error;
	protected Collection errors;
	protected static Location location;
	protected static Category cat = CategoryProvider.getUICategory();
	
	/**
	 * @return
	 */
	public HttpServletRequest getRequest() {
		return request;
	}

	/**
	 * @return
	 */
	public HttpSession getSession() {
		return session;
	}

	/**
	 * @param request
	 */
	public void setRequest(HttpServletRequest request) {
		this.request = request;
	}

	/**
	 * @param session
	 */
	public void setSession(HttpSession session) {
		this.session = session;
		clearErrors();
	}
	
	protected Helper getHelper(String id)  {
		Helper helper = null;
		UserSessionData userSession = UserSessionData.getUserSessionData(session);
		HelperObjectManager helpObjectManager = (HelperObjectManager)userSession.getAttribute(HelpManager.HELPMANAGER);
		if(null == helpObjectManager)  {
			helpObjectManager = new HelperObjectManager(session);
			userSession.setAttribute(HelpManager.HELPMANAGER , helpObjectManager);
		}
		return helpObjectManager.getHelper(id);
	}
	
	public String translate(String textId)  {
		UserSessionData userSessionData = UserSessionData.getUserSessionData(session);
		return WebUtil.translate(userSessionData.getLocale() , textId , null);
	}

	/**
	 * 
	 */
	public void preprocessing() {
		UserSessionData userSessionData = UserSessionData.getUserSessionData(request.getSession());
		setInvocationContext(userSessionData);
	}

	protected void setInvocationContext(UserSessionData userData)  {
		AuctionBusinessObjectManager auctionBOM = (AuctionBusinessObjectManager)
						userData.getBOM(AuctionBusinessObjectManager.AUCTION_BOM);
		InvocationContext context = new InvocationContext(auctionBOM.getProcessBusinessObjectManager());
		
		HelpManagerBase helpManager = (HelpManagerBase)userData.getAttribute(HelpManagerBase.HELPMANAGER);
		BusinessObjectManager bom =
					 (BusinessObjectManager) userData.getBOM(BusinessObjectManager.ISACORE_BOM);			
		context.setUser(bom.getUser());
		context.setShopId(bom.getShop().getId());
        if (bom.getShop().getId() != null && bom.getShop().getId().length() == 0) {
            logInfo("Empty ShopId is returning from ISACORE_BOM ");
        }
		context.setRoleType(UserRoleTypeEnum.BUYER);
		String bpId = ((SearchAuctionsHelper)getHelper(HelpManager.BUYERSEARCHHELPER)).getBusinessPartnerId();
		context.setBpId(bpId);
		ContextManager.bindContext(context);
	}
	
	public void postProcessing()  {
		releaseContext();		
	}

	protected void releaseContext()  {
		ContextManager.unbindContext();
	}

	public boolean isAuctionEnabled() {
		UserSessionData userSession = UserSessionData.getUserSessionData(session);
		boolean flag = false;			
		try  {
			BusinessObjectManager bom = (BusinessObjectManager)
													userSession.getBOM(BusinessObjectManager.ISACORE_BOM);
			flag = bom.getShop().isEAuctionUsed();					
			if (flag) {
				flag = AuctionBuyerRuntime.getRuntime().isAuctionEnabled(userSession.getCurrentConfigContainer().getScenarioName());
				bom.getShop().setEAuctionUsed(flag);
			}
		}  catch(Exception exc)  {
			flag = false;
			logWarning("Unable to determine whether auction is configured" , exc);
		}
		if (!flag)
			userSession.setAttribute("eAuctionUsed" , "false");
		return flag;
	}
	
	protected void setError(Object error) {
		clearErrors();
		if(error != null)  {
			UserSessionData userSessionData = UserSessionData.getUserSessionData(session);
			if(error instanceof SystemException)  {
				SystemException sysExc = (SystemException)error;
				addError(sysExc.getLocalizedMessage(userSessionData.getLocale()));
			}  if(error instanceof ValidationException)  {
				Collection culprits = (Collection)((ValidationException)error).getCulprits();
				if(culprits != null && culprits.size() > 0)  {
					Iterator iterator = culprits.iterator();
					ValidatingBean bean = null;
					while(iterator.hasNext())  {
						bean = (ValidatingBean)iterator.next();
						if(bean.getErrors() != null && bean.getErrors().size() > 0)  {
							Iterator iterator2 = bean.getErrors().iterator();
							Object msg = null;
							while(iterator2.hasNext())  {
								msg = iterator2.next();
								if(msg instanceof I18nErrorMessage)
									try  {
										addError(((I18nErrorMessage)msg).format(userSessionData.getLocale()));
									}  catch(Exception exc)  {
										logError("Unable to translate the msg - " + msg.toString() , exc);
									}
							}
						}
					}
				}
				
			}  else {
				logError("Unknown Error" ,(Exception)error);
				addError(translate(CRITICALERROR));
			}
			error = null;
		}
	}
	
	protected void clearErrors()  {
		this.error = null;
		if(this.errors != null)  {
			errors.clear();
		}
	}
	
	protected void addError(String text)  {
		if(errors == null)  {
			errors = new ArrayList();
		}
		errors.add(text);
	}
	
	public Collection getErrors()  {
		return errors;
	}
	
	protected void entering(String text)  {
		location = Location.getLocation(this.getClass());
		LogSupport.entering(cat , location , text , null);
	}

	protected void exiting(String text)  {
		location = Location.getLocation(this.getClass());
		LogSupport.exiting(cat , location , text , null);
	}

	protected void logInfo(String text)  {
		location = Location.getLocation(this.getClass());
		LogSupport.logInfo(cat , location , text);
	}

	protected void logInfo(String text , Throwable exc)  {
		location = Location.getLocation(this.getClass());
		LogSupport.logError(cat , location , text , exc);
	}

	protected void logError(String text , Throwable exc)  {
		location = Location.getLocation(this.getClass());
		LogSupport.logError(cat , location , text , exc);
	}

	protected void logWarning(String text , Throwable exc)  {
		location = Location.getLocation(this.getClass());
		LogSupport.logWarning(cat , location , text , exc);
	}
	
}

