/*
 * Created on May 26, 2004
 *
 * To change the template for this generated file go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
package com.sap.isa.auction.actionforms.buyer;

/**
 * @author I803067
 *
 * To change the template for this generated type comment go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
public interface AuctionMonitorConstants {
	public static final String SHOW = "AMF.ShowMe";
	public static final String WON = "AMF.Won";
	public static final String PARTICIPATED = "AMF.Participated";

	public static final String PARTICIPATEDFINALIZED = "AMF.PartFinalized";
	public static final String PARTICIPATEDACTIVE = "AMF.PartActive";
	public static final String PARTICIPATIONCLOSE = "AMF.PartClosed";
	public static final String PARTICIPATEDFINALIZEDAUCTIONS = "AMF.PartFinalized";
	public static final String PARTICIPATEDACTIVEAUCTIONS = "AMF.PartActive";
	public static final String PARTICIPATIONCLOSEAUCTIONS = "AMF.PartClosed";
	public static final String PENDINGCHECKOUT = "AMF.ChkPending";
	public static final String CHECKOUT = "AMF.CheckedOut";
	public static final String THATI           = "AMF.ThatI";
	public static final String WHICHARE        = "AMF.WhichAre";
	public static final String UPCOMING         = "AMF.UpComing";
}
