/*
 * Created on May 6, 2004
 *
 * To change the template for this generated file go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
package com.sap.isa.auction.actionforms.buyer;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.Calendar;

import com.sap.isa.auction.bean.AuctionStatusEnum;
import com.sap.isa.auction.bean.b2x.PrivateAuction;
import com.sap.isa.auction.helpers.CatalogHelper;
import com.sap.isa.auction.helpers.ConversionHelper;
import com.sap.isa.auction.helpers.HelpManager;
import com.sap.isa.auction.helpers.buyer.BidsHelper;
import com.sap.isa.auction.helpers.buyer.SearchAuctionsHelper;
import com.sap.isa.catalog.webcatalog.WebCatItem;

/**
 * @author I803067
 *
 * To change the template for this generated type comment go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
public class AuctionBaseForm extends BaseForm implements AuctionBaseConstants  {

	
	protected String productId;
	public String getDisplayCurrencyValue(BigDecimal value)  {
		ConversionHelper conversionHelper = (ConversionHelper)
								getHelper(HelpManager.CONVERSIONHELPER);
		return conversionHelper.bigDecimalToUICurrencyString(value , false);
	}
	
	public String getDisplayQuantityValue(double value)  {
		ConversionHelper conversionHelper = (ConversionHelper)
								getHelper(HelpManager.CONVERSIONHELPER);
		return conversionHelper.bigDecimalToUICurrencyString(value , true);
	}
	
	public String getDisplayQuantityValue(BigDecimal value)  {
		ConversionHelper conversionHelper = (ConversionHelper)
								getHelper(HelpManager.CONVERSIONHELPER);
		return conversionHelper.bigDecimalToUICurrencyString(value , true);
	}

	public String getDisplayDateValue(Timestamp date)  {
		ConversionHelper conversionHelper = (ConversionHelper)
								getHelper(HelpManager.CONVERSIONHELPER);
		return conversionHelper.javaDatetoUILocaleSpecificString(date);
	}
	
	public BigDecimal getHighestBid(PrivateAuction auction)  {
		BidsHelper bidsHelper = (BidsHelper)getHelper(HelpManager.BIDSHELPER);
		return bidsHelper.getHighestBid(auction);
	}
	
	public String getCurrentBid(PrivateAuction auction)  {
		BidsHelper bidsHelper = (BidsHelper)getHelper(HelpManager.BIDSHELPER);
		return getDisplayCurrencyValue(bidsHelper.getHighestBid(auction));
	}
	
	public String getMyLastBid(PrivateAuction auction)  {
		BidsHelper bidsHelper = (BidsHelper)getHelper(HelpManager.BIDSHELPER);
		return getDisplayCurrencyValue(bidsHelper.getMyLastBid(auction));
	}
	
	public String getTimeLeft(PrivateAuction auction)  {
		try  {
			SearchAuctionsHelper searchHelper = (SearchAuctionsHelper)
							getHelper(HelpManager.BUYERSEARCHHELPER);
			Timestamp currentTime = new Timestamp(Calendar.getInstance().getTime().getTime());
			if(!isOpenForBidding(auction))
				return translate(CLOSEDFORBIDDING);
			return searchHelper.getDateDifference(currentTime , auction.getEndDate());
		}  catch(Exception exc) {
			return translate(CLOSEDFORBIDDING);
		}
	}
	
	public String getBidValue(BigDecimal value)  {
		if(value != null && value.doubleValue() > 0.0)  {
			return getDisplayCurrencyValue(value);
		}  else  {
			return translate(AuctionDetailsForm.NOBIDSYETTEXT);
		}
	}
	
	public boolean isOpenForBidding(PrivateAuction auction)  {
		Timestamp currentTime = new Timestamp(Calendar.getInstance().getTime().getTime());
		if(auction.getStatus() != AuctionStatusEnum.PUBLISHED.getValue()
			|| auction.getEndDate().before(currentTime))
			return false;
		return true;		
	}

	public String getReservePriceInfoText(PrivateAuction auction , BigDecimal bidValue)  {
		if(auction.getReservePrice() != null)  {
			if(bidValue != null && bidValue.doubleValue() >= auction.getReservePrice().doubleValue())
				return translate(RESERVEMETTEXT);
			else
				return translate(RESERVENOTMETTEXT);
		}  else  {
			return translate(NORESERVEPRICETEXT);
		}
	}
	
	public WebCatItem getProduct()  {
		WebCatItem item = null;
		CatalogHelper catalogHelper = (CatalogHelper)
								getHelper(HelpManager.CATALOGHELPER);
		return catalogHelper.getWebCatItem(productId);
	}
	
	public void setProductId(String id)  {
		this.productId = id;
	}
	
	public String getProductId()  {
		return productId;
	}
	
	public String getCurrencyText()  {
		SearchAuctionsHelper searchHelper = (SearchAuctionsHelper)
						getHelper(HelpManager.BUYERSEARCHHELPER);
		return searchHelper.getCurrency(); 
	}
	
	public String getBusinessPartnerName(String businessPartnerId)  {
		String name = "";
		BidsHelper helper = (BidsHelper)getHelper(HelpManager.BIDSHELPER);
		name = helper.getBusinessPartnerName(businessPartnerId);
		return name;
	}
}
