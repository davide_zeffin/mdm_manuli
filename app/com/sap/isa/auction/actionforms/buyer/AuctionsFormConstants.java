/*
 * Created on May 6, 2004
 *
 * To change the template for this generated file go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
package com.sap.isa.auction.actionforms.buyer;

/**
 * @author I803067
 *
 * To change the template for this generated type comment go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
public interface AuctionsFormConstants {
	
	public static final String NEXTPAGE 	= "_nextPage";
	public static final String PREVIOUSPAGE = "_PreviousPage";
	public static final String PAGESIZE		= "_pageSize";
	public static final String STARTINDEX	= "_startIndex";
	public static final String NAVIGATION	= "_Navigation";

	public static final String AUCTIONID	= "_auctionId";

	public static final String PRODUCTID = "_productID";
	public static final String CONTEXTID = "_contextId";

	public static final String CATALOGCONTEXT = "_catalogContext";
	public static final String PARTICIPATIONCONTEXT = "participation";
//		public static final String WON					= "_won";
//		public static final String CHECKEDOUT			= "_checkout";
	public static final String STATUSOPTIONTEXT = "rc_status";//dropdown box
	public static final String OPTION_PARTICIPATED = "PARTICIPATED"; //options of the dropdown
	public static final String OPTION_WON = "WON";//options of the dropdown

	public static final String PARTICIPATIONOPTIONTEXT = "rc_participated"; //dropdown box
	public static final String OPTION_ALL = "all"; //options of the dropdown
	public static final String OPTION_ACTIVE = "active"; //options of the dropdown
	public static final String OPTION_CLOSED = "closed";//options of the dropdown
	public static final String OPTION_FINALIZED = "finalized";//options of the dropdown

	public static final String WONOPTIONTEXT = "rc_won"; //dropdown box
	public static final String OPTION_NOCHECKOUT = "notcheckout"; //options of the dropdown
	public static final String OPTION_CHECKEDOUT = "checkedout";//options of the dropdown


	public static final String PARTICIPATIONHISTORYFINALIZED = "_partHistory";
//		public static final String PARTICIPATIONHISTORYCLOSED = "_partClosed";

//		public static final String PARTICIPATIONACTIVE = "_partActive";
	public static final String ALLPARTICIPATEDTXT 		= "AF.ParticipatedAll";
	public static final String ACTIVEPARTICIPATEDTXT 	= "AF.ParticipatedActive";
	public static final String FINALIZEDPARTICIPATEDTXT = "AF.ParticiaptedFinalized";
	public static final String CLOSEDPARTICIPATEDTEXT	= "AF.ParticiaptedClosed";
	public static final String WONAUCTIONSTXT			= "AF.WonAuctions";
	public static final String WONAUCTIONSCHECKOUTTEXT  = "AF.WonCheckoutAuctions";
	public static final String AUCTIONSONPRODUCT		= "AF.AuctionsOn";

	public static final String NOAUCTIONSTEXT			= "AF.NoAuctions";

	public static final String FIVEITEMS			= "5";
	public static final String TENITEMS				= "10";
	public static final String FIFTEENITEMS			= "15";

	public static final String FIVEITEMSTEXT		= "AF.Five";
	public static final String TENITEMSTEXT			= "AF.Ten";
	public static final String FIFTEENITEMSTEXT		= "AF.Fifteen";

	public static final String SHOWPAGEPARAMETER	= "_ShowPage";
	public static final String SHOWPAGETEXT			= "AF.ShowPage";
}

