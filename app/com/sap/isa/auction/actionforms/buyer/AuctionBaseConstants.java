/*
 * Created on Dec 3, 2004
 *
 * To change the template for this generated file go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
package com.sap.isa.auction.actionforms.buyer;

/**
 * @author i802791
 *
 * To change the template for this generated type comment go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
public interface AuctionBaseConstants {
	public static final String RESERVEMETTEXT = "ABF.ReserveMet";
	public static final String RESERVENOTMETTEXT	= "ABF.ReserveNotMet";
	public static final String NORESERVEPRICETEXT	= "ABF.NoReserve";
	public static final String CLOSEDFORBIDDING		= "ABF.ClosedForBidding";

	public static final String STARTPRICETEXT = "ABF.StartPrice";
	public static final String CURRENTBIDTEXT = "ABF.CurrentBid";
	public static final String WINNINGBIDTEXT = "ABF.WinningBid";
	public static final String FINALBIDTEXT   = "ABF.FinalBid";
	public static final String MYLASTBIDTEXT  = "ABF.MyLastBid";
	public static final String BUYNOWPRICETEXT = "ABF.BuyNowPrice";
	public static final String ENDTIMETEXT = "ABF.Endtime";
	public static final String TIMELEFTTEXT    = "ABF.TimeLeft";
	public static final String NEXTTEXT        = "ABF.Next";
	public static final String PREVIOUSTEXT    = "ABF.Previous";
	public static final String UNITSTEXT       = "ABF.Units";
	public static final String NOIMAGETEXT	   = "ABF.NoImage";
	public static final String QUANTITYTEXT	   = "ABF.Quantity";

	public static final String SHOWPAGETEXT	 = "AF.ShowPage";
	public static final String SHOWITEMSTEXT = "AF.ShowItems";
	public static final String SHOWTEXT		 = "AF.Show";
	public static final String FIVEITEMSTEXT = "AF.FiveItems";
	public static final String TENITEMSTEXT	 = "AF.TenItems";
	public static final String FIFTEENITEMSTEXT = "AF.FifteenItems";
}
