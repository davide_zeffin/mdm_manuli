package com.sap.isa.auction.actionforms.seller;

/**
 * Title:			Internet Sales Private Auctions
 * Description:
 * @Copyright: 		Copyright (c) 2004
 * Company:			SAPLabs LLC
 * @author
 * Created on:		Apr 27, 2004
 */
public interface AuctionSearchFormConstants {
	/************************************************
	 *					I13N text					*
	 ************************************************/	
	//Generic search fields
	public static final String SEARCHHEADER     		= "SF.AuctionSearch";
	public static final String LBL_SHOW                 = "SF.Show";
	public static final String LBL_WITH                 = "SF.With";
	public static final String BTN_GET					= "SF.Get";
	public static final String BTN_SHOWADVSEARCH    	= "SF.AdvancedSearch";
	
	//Advanced search fields
	public static final String LBL_NAME              	= "SF.AuctionName";
	public static final String LBL_TITLE              	= "SF.AuctionTitle";
	public static final String LBL_ORDERID				= "SF.OrderId";
	public static final String LBL_FROM              	= "SF.From";
	public static final String LBL_TO                   = "SF.To";
	public static final String LBL_TYPE                 = "SF.Type";
	//public static final String LBL_STATUS               = "SF.Status";
	public static final String LBL_PRODUCTNAME          = "SF.ProductName";
	public static final String BTN_RESET              	= "SF.Reset";
	public static final String BTN_SEARCH             	= "SF.Search";
	public static final String BTN_SHOWLIST            	= "SF.ShowList";
	public static final String RADIOBTNLBL_NORMAL       = "SF.NormalAuction";
	public static final String RADIOBTNLBL_DUTCH        = "SF.DutchAuction";
	
	//Common fields
	public static final String MSG_NORESULTS         	= "SF.xmsg_NoResults";
	public static final String BTN_REFRESH        		= "SF.Refresh";
	
	/************************************************
	 *				htmlb Component Id				*
	 ************************************************/
	//Generic search field Ids
	public static final String BTNID_SHOWADVSEARCH   	= "BTNID_SHOWADVSEARCH.SearchForm";
	public static final String BTNID_GENSEARCH          = "BTNID_GENSEARCH.SearchForm";
	public static final String DROPDOWNID_STATUS        = "DROPDOWNID_STATUS.SearchForm";
	public static final String LSTMODELID_STATUS        = "LSTMODELID_STATUS.SearchForm";
	public static final String DROPDOWNID_SEARCHCRITERIA= "DROPDOWNID_SEARCHCRITERIA.SearchForm";
	public static final String LSTMODELID_SEARCHCRITERIA= "LSTMODELID_SEARCHCRITERIA.SearchForm";
	public static final String INPUTID_SEARCHCRITERIA   = "INPUTID_SEARCHCRITERIA.SearchForm";
	public static final String INPUTID_DATE     		= "INPUTID_DATE.SearchForm";
	
	//Advanced search field Ids
	public static final String DROPDOWNID_AVDSTATUS     = "DROPDOWNID_AVDSTATUS.SearchForm";
	public static final String LSTMODELID_AVDSTATUS     = "LSTMODELID_AVDSTATUS.SearchForm";
	public static final String INPUTID_NAME            	= "INPUTID_NAME.SearchForm";
	public static final String INPUTID_TITLE            = "INPUTID_TITLE.SearchForm";
	public static final String INPUTID_ORDERID          = "INPUTID_ORDERID.SearchForm";
	public static final String INPUTID_PUBLISHFRM       = "INPUTID_PUBLISHFRM.SearchForm";
	public static final String INPUTID_PUBLISHTO        = "INPUTID_PUBLISHTO.SearchForm";
	public static final String INPUTID_CLOSINGFRM       = "INPUTID_CLOSINGFRM.SearchForm";
	public static final String INPUTID_CLOSINGTO        = "INPUTID_CLOSINGTO.SearchForm";
	public static final String INPUTID_PRODUCTNAME      = "INPUTID_PRODUCTNAME.SearchForm";
	public static final String DROPDOWNID_PRIPUB       	= "DROPDOWNID_PRIPUB.SearchForm";
	public static final String DROPDOWNID_FULBROK    	= "DROPDOWNID_FULBROK.SearchForm";
	public static final String BTNID_ADVSEARCH          = "BTNID_ADVSEARCH.SearchForm";
	public static final String BTNID_RESET            	= "BTNID_RESET.SearchForm";
	
	//Common field Ids
	public static final String TBLID_AUCTION         	= "AUCTIONTABLEVIEW.SearchForm";
	public static final String TBLMODELID_AUCTION       = "AUCTIONTABLEMODEL.SearchForm";
	public static final String BTNID_REFRESH          	= "BTNID_REFRESH.SearchForm";
	
	/************************************************
	 *				Other Form Constants			*
	 ************************************************/
	public static final int SRC_MONITORING             	= 1;
	public static final int SRC_PUBLISHQUEUE			= 2;
	public static final int SRC_MANUALWINNERQUEUE		= 3;	
	public static final String FROMSRCPARAM				= "FROMSRCPARAM.SearchForm";
	//date inputfield initial value Constants
	public static final String INITIALDATE 				= "0000-00-00";

}
