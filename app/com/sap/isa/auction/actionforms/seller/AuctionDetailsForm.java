package com.sap.isa.auction.actionforms.seller;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.Collection;
import java.util.Iterator;

import com.sap.isa.auction.actionforms.BaseForm;
import com.sap.isa.auction.bean.AuctionStatusEnum;
import com.sap.isa.auction.bean.AuctionTypeEnum;
import com.sap.isa.auction.bean.Bid;
import com.sap.isa.auction.bean.b2x.AuctionVisibilityEnum;
import com.sap.isa.auction.bean.b2x.BidTypeEnum;
import com.sap.isa.auction.bean.b2x.PrivateAuction;
import com.sap.isa.auction.bean.b2x.WinnerDeterminationTypeEnum;
import com.sap.isa.auction.helpers.AuctionDetailsHelper;
import com.sap.isa.auction.helpers.ConversionHelper;
import com.sap.isa.auction.helpers.HelpManager;
import com.sap.isa.auction.helpers.TargetGroupBPHelper;
import com.sap.isa.auction.models.listmodels.seller.AuctionStatusListConstants;
import com.sap.isa.auction.models.tablemodels.seller.BusinessPartnerTGModel;
import com.sap.isa.auction.models.tablemodels.seller.ProductsTableModel;
import com.sap.isa.core.util.WebUtil;
import com.sapportals.htmlb.Button;
import com.sapportals.htmlb.Component;
import com.sapportals.htmlb.GridLayout;
import com.sapportals.htmlb.GridLayoutCell;
import com.sapportals.htmlb.Image;
import com.sapportals.htmlb.TextView;
import com.sapportals.htmlb.enum.ButtonDesign;
import com.sapportals.htmlb.table.TableView;

/**
 * Title:			Internet Sales Private Auctions
 * Description:
 * @Copyright: 		Copyright (c) 2004
 * Company:			SAPLabs LLC
 * Created on:		May 6, 2004
 */
public class AuctionDetailsForm
	extends BaseForm
	implements AuctionDetailConstants {

	//General Info Tab
	private TextView auctionNameText;
	private TextView titleText;
	private TextView termsText;
	private Image imageURLText;

	private TextView startPriceText;
	private TextView reservePriceText;
	private TextView buynowPriceText;
	private TextView bidIncrText;

	private TextView publishDateLabel;
	private TextView publishDateText;
	private TextView closingDateText;
	private TextView reserveDateText;
	private TextView auctionDurationText;
	private TextView quotationIdText;
	private TextView highestBidText;
	private TextView auctionTypeText;
	private TextView statusText;
	private TextView bidTypeText;
	private TextView fullBrokText;
	private TextView visibilityText;
	private TextView winnerTypeText;
	private TextView quantityText;

	protected GridLayout layout;
	//Product Tab
	private TableView productsTable;
	private ProductsTableModel productsTableModel;

	//TargetGroup Tab
	private TableView targetGroupsTable;
	private BusinessPartnerTGModel tgTableModel;

	PrivateAuction auction;
	AuctionDetailsHelper detailsHelper;
	ConversionHelper conversionHelper;

	private boolean isInitialized;
	private Integer fromSrc = null;

	public void initializeDetails(String auctionId, Integer fromSrc) {
		final String METHOD = "initializeDetails";
		entering(METHOD);
		this.fromSrc = fromSrc;
		auction =
			(PrivateAuction) getDetailsHelper().getAuctionById(
				auctionId,
				fromSrc);
		getDetailsHelper().setAuction(auction);
		exiting(METHOD);

	}
	public void initializeGeneralInfo() {
		final String METHOD = "initializeGeneralInfo";
		entering(METHOD);
		logInfo("Initializing form controls");

		auctionNameText = (TextView) addControl(HTMLB_TEXTVIEW, NAMEVIEW);
		titleText = (TextView) addControl(HTMLB_TEXTVIEW, TITLEVIEW);
		imageURLText = new Image(IMAGEURLVIEW, "");
		this.setAttribute(IMAGEURLVIEW, imageURLText);
		quotationIdText =
			(TextView) addControl(HTMLB_TEXTVIEW, QUOTATIONIDTEXT_ID);
		auctionTypeText =
			(TextView) addControl(HTMLB_TEXTVIEW, AUCTIONTYPETEXT_ID);
		statusText = (TextView) addControl(HTMLB_TEXTVIEW, STATUSTEXT_ID);
		bidTypeText = (TextView) addControl(HTMLB_TEXTVIEW, BIDTYPVIEW);
		fullBrokText = (TextView) addControl(HTMLB_TEXTVIEW, FLBLVIEW);
		visibilityText = (TextView) addControl(HTMLB_TEXTVIEW, PUBPRIVIEW);
		winnerTypeText = (TextView) addControl(HTMLB_TEXTVIEW, WINNERTYPEVIEW);
		//date info
		publishDateLabel =
			(TextView) addControl(HTMLB_TEXTVIEW, PUBLISHDATELABEL_ID);
		publishDateText =
			(TextView) addControl(HTMLB_TEXTVIEW, PUBLISHDATETEXT_ID);
		closingDateText =
			(TextView) addControl(HTMLB_TEXTVIEW, CLOSINGDATETEXT_ID);
		reserveDateText =
			(TextView) addControl(HTMLB_TEXTVIEW, RESERVEDATETEXT_ID);
		auctionDurationText =
			(TextView) addControl(HTMLB_TEXTVIEW, DURATIONVIEW);
		quantityText = (TextView) addControl(HTMLB_TEXTVIEW, QTYVIEW);
		//price info
		startPriceText = (TextView) addControl(HTMLB_TEXTVIEW, STARTPRICEVIEW);
		reservePriceText = (TextView) addControl(HTMLB_TEXTVIEW, RESVPRICEVIEW);
		buynowPriceText = (TextView) addControl(HTMLB_TEXTVIEW, BUYNOWVIEW);
		bidIncrText = (TextView) addControl(HTMLB_TEXTVIEW, BIDINCRVIEW);
		highestBidText =
			(TextView) addControl(HTMLB_TEXTVIEW, HIGHESTBIDTEXT_ID);

		//buttons
		layout = new GridLayout();
		this.setAttribute(ACTIONSAVAILABLE, layout);
		setGeneralInfo();
		setPriceInfo();
		setButtonComponent();

		exiting(METHOD);
	}

	public void initializeTermsConditions() {
		final String METHOD = "initializeProductInfo";
		entering(METHOD);
		auction = (PrivateAuction) getDetailsHelper().getAuction();
		termsText = (TextView) addControl(HTMLB_TEXTVIEW, TERMSVIEW);
		termsText.setText(auction.getAuctionTerms());
		exiting(METHOD);
	}
	public void initializeProductInfo() {
		final String METHOD = "initializeProductInfo";
		entering(METHOD);
		auction = (PrivateAuction) getDetailsHelper().getAuction();
		productsTable =
			(TableView) addControl(HTMLB_TABLEVIEW, PRODUCTSTABLEVIEW);
		productsTableModel =
			new ProductsTableModel(session, request.getContextPath());

		try {
			Collection data = auction.getItems();
			productsTableModel.setData(data);
		} catch (Exception exc) {
			logInfo("Unable to get the products for auction , ", exc);
		}
		this.setAttribute(PRODUCTSTABLEMODEL, productsTableModel);

		exiting(METHOD);
	}

	public void initializeTargetGroupInfo() {
		final String METHOD = "initializeTargetGroupInfo";
		entering(METHOD);
		auction = (PrivateAuction) getDetailsHelper().getAuction();
		targetGroupsTable =
			(TableView) addControl(HTMLB_TABLEVIEW, TARGETSTABLEVIEW);
		tgTableModel = new BusinessPartnerTGModel(servlet, session);
		tgTableModel.setContextPath(request.getContextPath());
		try {
			TargetGroupBPHelper targetHelper =
				(TargetGroupBPHelper) getHelper(HelpManager.TARGETGROUPHELPER);
			Collection items =
				targetHelper.getTargetGroupBPCollection(
					auction.getTargetGroups(),
					auction.getBusinessPartners());
			tgTableModel.setData(items, "", "", false);
		} catch (Exception exc) {
			logInfo(
				"Unable to get the targetgroup busines partners for auction,",
				exc);
		}
		this.setAttribute(TARGETSTABLEMODEL, tgTableModel);
		exiting(METHOD);
	}

	private void setGeneralInfo() {
		auction = (PrivateAuction) getDetailsHelper().getAuction();
		auctionNameText.setText(auction.getAuctionName());
		titleText.setText(auction.getAuctionTitle());
		if (null == auction.getImageURL()
			|| auction.getImageURL().equals("")) {
			imageURLText.setSrc(
				WebUtil.getAppsURL(
					servlet.getServletContext(),
					request,
					response,
					null,
					"/mimes/images/noimage.jpg",
					null,
					null,
					false));
		} else {
            if (auction.getImageURL().startsWith("http") || auction.getImageURL().startsWith("/")) {
                imageURLText.setSrc(auction.getImageURL());
            } else {
                imageURLText.setSrc("../" + auction.getImageURL());   
            }
		}
		imageURLText.setTooltip(auction.getAuctionTitle());

		quotationIdText.setText("");
		reserveDateText.setText("");
		publishDateText.setText("");
		closingDateText.setText("");
		int intStatus = auction.getStatus();
		if (AuctionStatusEnum.SCHEDULED.getValue() == intStatus) {
			publishDateLabel.setText(translate(SCHPUBLISHEDATE_TEXT));
			Timestamp time = new Timestamp(auction.getPublishDate().getTime());
			publishDateText.setText(
				time != null
					? getConversionHelper().javaDatetoUILocaleSpecificString(
						time)
					: "");
			auctionDurationText.setText(
				getConversionHelper().bigDecimalToUIQuantityString(
					new BigDecimal(auction.getAuctionDuration()[0])));
		} else if (
			AuctionStatusEnum.PUBLISHED.getValue() == intStatus
				|| AuctionStatusEnum.FINALIZED.getValue() == intStatus
				|| AuctionStatusEnum.CLOSED.getValue() == intStatus
				|| AuctionStatusEnum.NOT_FINALIZED.getValue() == intStatus
				|| AuctionStatusEnum.CHECKEDOUT.getValue() == intStatus
				|| AuctionStatusEnum.FINISHED.getValue() == intStatus
				|| AuctionStatusEnum.NOT_FINALIZED.getValue() == intStatus) {

			publishDateLabel.setText(translate(PUBLISHDATE_TEXT));
			Timestamp time = auction.getStartDate();
			publishDateText.setText(
				time != null
					? getConversionHelper().javaDatetoUILocaleSpecificString(
						time)
					: "");
			time = auction.getEndDate();
			closingDateText.setText(
				time != null
					? getConversionHelper().javaDatetoUILocaleSpecificString(
						time)
					: "");
			quotationIdText.setText(
				auction.getQuotationId() != null
					? auction.getQuotationId()
					: "");
			Bid bid = getDetailsHelper().getHighestBid(auction);
			if (bid != null) {
				highestBidText.setText(
					getConversionHelper().bigDecimalToUICurrencyString(
						bid.getBidAmount(),
						false));
			} else {
				highestBidText.setText("-");
			}
		} else {
			auctionDurationText.setText(
				getConversionHelper().bigDecimalToUIQuantityString(
					new BigDecimal(auction.getAuctionDuration()[0])));
		}
		int visibilityType = auction.getVisibilityType().getValue();
		if (visibilityType == AuctionVisibilityEnum.INT_PUBLIC) {
			auctionTypeText.setText(translate(PUBPRITEXT1));
		} else if (visibilityType == AuctionVisibilityEnum.INT_PROTECTED) {
			auctionTypeText.setText(translate(PUBPRITEXT2));
		}
		if (intStatus == AuctionStatusEnum.OPEN.getValue()) {
			statusText.setText(
				translate(AuctionStatusListConstants.LSTITM_OPEN));
		} else if (intStatus == AuctionStatusEnum.SCHEDULED.getValue()) {
			statusText.setText(
				translate(AuctionStatusListConstants.LSTITM_SCHEDULED));
		} else if (intStatus == AuctionStatusEnum.PUBLISHED.getValue()) {
			statusText.setText(
				translate(AuctionStatusListConstants.LSTITM_PUBLISHED));
		} else if (intStatus == AuctionStatusEnum.CLOSED.getValue()) {
			statusText.setText(
				translate(AuctionStatusListConstants.LSTITM_CLOSED));
		} else if (intStatus == AuctionStatusEnum.NOT_FINALIZED.getValue()) {
			statusText.setText(
				translate(AuctionStatusListConstants.LSTITM_NOTFINAL));
		} else if (intStatus == AuctionStatusEnum.FINALIZED.getValue()) {
			statusText.setText(
				translate(AuctionStatusListConstants.LSTITM_FINALIZED));
		} else if (intStatus == AuctionStatusEnum.FINISHED.getValue()) {
			statusText.setText(
				translate(AuctionStatusListConstants.LSTITM_FINISHED));
		}
		int intType = auction.getType();
		if (intType == AuctionTypeEnum.NORMAL.getValue())
			fullBrokText.setText(translate(FLBLTEXT1));
		else if (intType == AuctionTypeEnum.BROKEN_LOT.getValue())
			fullBrokText.setText(translate(FLBLTEXT2));
		if (auction.getBidType().getValue() == BidTypeEnum.NORMAL.getValue()) {
			bidTypeText.setText(translate(BIDTYPETEXT1));
		} else {
			bidTypeText.setText(translate(BIDTYPETEXT2));
		}
		if (WinnerDeterminationTypeEnum.INT_AUTOMATIC
			== auction.getWinnerTypeEnum().getValue()) {
			winnerTypeText.setText(translate(AUTOWINNERLABEL));
		} else {
			winnerTypeText.setText(translate(MANUALWINNERLABEL));
		}
		quantityText.setText(
			getConversionHelper().bigDecimalToUIQuantityString(
				new BigDecimal(auction.getQuantity())));
	}

	private void setPriceInfo() {
		startPriceText.setText(
			getConversionHelper().bigDecimalToUICurrencyString(
				auction.getStartPrice(),
				false));
		BigDecimal price = auction.getReservePrice();
		reservePriceText.setText(
			price != null
				? getConversionHelper().bigDecimalToUICurrencyString(
					price,
					false)
				: " - ");
		price = auction.getBuyItNowPrice();
		buynowPriceText.setText(
			price != null
				? getConversionHelper().bigDecimalToUICurrencyString(
					price,
					false)
				: " - ");
		price = auction.getBidIncrement();
		bidIncrText.setText(
			price != null
				? getConversionHelper().bigDecimalToUICurrencyString(
					price,
					false)
				: " - ");
		Bid bid = getDetailsHelper().getHighestBid(auction);
		if (bid != null) {
			highestBidText.setText(
				getConversionHelper().bigDecimalToUICurrencyString(
					bid.getBidAmount(),
					false));
		}

	}

	protected void setButtonComponent() {
		Button button = null;
		String editBtn = translate(EDIT_TEXT);
		String publishBtn = translate(PUBLISH_TEXT);
		String copyBtn = translate(COPY_TEXT);
		String closeBtn = translate(CLOSE_TEXT);
		String refreshTxt = translate(AuctionSearchFormConstants.BTN_REFRESH);

		int statusInt = auction.getStatus();

		Iterator iterate = layout.iterator();
		Component comp = null;
		while (iterate.hasNext()) {
			comp = (Component) iterate.next();
			layout.removeComponent(comp);
		}
		layout.setCellSpacing(4);
		GridLayoutCell cell = null;
		if (AuctionStatusEnum.OPEN.getValue() == statusInt) {
			button = new Button(BTNID_EDIT);
			button.setId(BTNID_EDIT);
			button.setText(editBtn);
			button.setTooltip(editBtn);
			button.setOnClick(BTNID_EDIT);
			button.setOnClientClick("_auction_submitRequest()");
			button.setDesign(ButtonDesign.STANDARD);
			cell = new GridLayoutCell(button);
			layout.addCell(1, 1, cell);
		}
		button = new Button(BTNID_COPY);
		button.setId(BTNID_COPY);
		button.setText(copyBtn);
		button.setTooltip(copyBtn);
		button.setOnClick(BTNID_COPY);
		button.setOnClientClick("inputTheTargetName()");
		button.setDesign(ButtonDesign.STANDARD);
		cell = new GridLayoutCell(button);
		layout.addCell(1, 2, cell);

		if (AuctionStatusEnum.OPEN.getValue() == statusInt) {
			button = new Button(BTNID_PUBLISH);
			button.setId(BTNID_PUBLISH);
			button.setText(publishBtn);
			button.setTooltip(publishBtn);
			button.setOnClick(BTNID_PUBLISH);
			button.setOnClientClick("changeStatus(publish)");
			button.setDesign(ButtonDesign.STANDARD);
			cell = new GridLayoutCell(button);
			layout.addCell(1, 3, cell);
		}

		if (AuctionStatusEnum.PUBLISHED.getValue() == statusInt) {
			button = new Button(BTNID_CLOSE);
			button.setId(BTNID_CLOSE);
			button.setText(closeBtn);
			button.setTooltip(closeBtn);
			button.setOnClick(BTNID_CLOSE);
			button.setOnClientClick("changeStatus(close)");
			button.setDesign(ButtonDesign.STANDARD);
			cell = new GridLayoutCell(button);
			layout.addCell(1, 4, cell);
		}

		button = new Button(BTNID_REFRESH);
		button.setId(BTNID_REFRESH);
		button.setText(refreshTxt);
		button.setTooltip(refreshTxt);
		button.setOnClick(BTNID_REFRESH);
		button.setOnClientClick("_auction_submitRequest()");
		button.setDesign(ButtonDesign.STANDARD);
		cell = new GridLayoutCell(button);
		layout.addCell(1, 5, cell);
	}
	public int getAuctionStatus() {
		return auction.getStatus();
	}
	public int getAuctionType() {
		return auction.getType();
	}
	public int getFromSource() {
		return fromSrc.intValue();
	}
	protected AuctionDetailsHelper getDetailsHelper() {
		if (null == detailsHelper) {
			detailsHelper =
				(AuctionDetailsHelper) getHelper(HelpManager
					.AUCTIONDETAILSHELPER);
		}
		return detailsHelper;
	}

	protected ConversionHelper getConversionHelper() {
		if (conversionHelper == null)
			conversionHelper =
				(ConversionHelper) getHelper(HelpManager.CONVERSIONHELPER);
		return conversionHelper;
	}
	
	/**
	 * Returns Terms and Conditions.
	 * 
	 * @return
	 */
	public String getAuctionTerms(){
		auction = (PrivateAuction) getDetailsHelper().getAuction();
		String termsAndConditions = "";
		if ((null != auction) && (null != auction.getAuctionTerms())){			
			if (!auction.getAuctionTerms().equalsIgnoreCase("null")){
				termsAndConditions = auction.getAuctionTerms();
			}
		}
		return termsAndConditions;
	}
}
