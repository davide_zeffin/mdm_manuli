/*
 * Created on Apr 30, 2004
 *
 * To change the template for this generated file go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
package com.sap.isa.auction.actionforms.seller;

/**
 * @author I803067
 *
 * To change the template for this generated type comment go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
public interface TargetGroupConstants {
	public static final String TARGETGROUPNAME      = "TARGETGROUPNAME.AuctionDetails";
	public static final String TARGETGROUPID        = "TARGETGROUPID.AuctionDetails";
	public static final String FIRSTNAME            = "FIRSTNAME.AuctionDetails";
	public static final String LASTNAME             = "LASTNAME.AuctionDetails";
	public static final String CITY                 = "CITY.AuctionDetails";
	public static final String POCODE               = "POCODE.AuctionDetails";
	public static final String COUNTRY              = "COUNTRY.AuctionDetails";
	public static final String ORGANISATION         = "ORGANISATION.AuctionDetails";
	public static final String BPID                 = "BPID.AuctionDetails";
	public static final String REGION               = "REGION.AuctionDetails";
	public static final String TARGETGROUPDESC      = "TGF.SearchTGID";
	public static final String CASESENSITIVESEARCH  = "TGF.CaseSensitive";
	public static final String EMPTYTEXT			= "TGF.Empty";
	public static final String NOOFITEMSTEXT		= "TGF.NoOfItemsPerPage";
	
	public static final String NOTAPPLICABLETEXT	= "TGF.NotApplicable";
	
	public static final String NOOFITEMS			= "NoOfItems";
	
	
	public static final String SEARCHBUSINESSPARTNERMODEL     = "SearchBusinessPartnerTGModel";
	public static final String SEARCHBUSINESSPARTNERVIEW      = "SearchBusinessPartnerView";

	public static final String SELECTEDBUSINESSPARTNERMODEL     = "SelectedBusinessPartnerTGModel";
	public static final String SELECTEDBUSINESSPARTNERVIEW      = "SelectedBusinessPartnerView";

	public static final String BUSINESSPARTNERTITLE	 = "TGF.BusinessPartner";
	public static final String TARGETGROUPTITLE		 = "TGF.TargetGroup";
	
	public static final String BPIDPARAMETER 					= "bp";
	public static final String TARGETGROUPPARAMETER 			= "tg";
	
	public static final String ITEMSADDED                     = "ItemsAdded.AuctionDetailsEdit";
	public static final String TARGETADDED                    = "TargetAdded.AuctionDetailsForm";

	public static final String BPTGDETAILTARGET					= "bpTarget";
	
	public static final String TARGETGROUPSTABLETITLE 			= "TGF.TargetGroups";
	public static final String REMOVEFROMTARGETBUTTONTEXT 		= "TGF.Remove";
	public static final String ADDTOTARGETBUTTONTEXT			= "TGF.Add";
	public static final String SEARCHBPARTNERSBUTTONTEXT		= "TGF.BPSearch";
	public static final String CLEARBUTTONTEXT					= "TGF.Clear";
	
	public static final String BUSINESSPARTNERSEARCHTEXT		= "TGF.SearchBusinessPartners";
	public static final String TARGETGROUPSEARCHTEXT			= "TGF.SearchTargetGroup";
	public static final String NOTARGETGROUPTEXT				= "TGF.NoTargetGroup";			
	
	public static final String SELECTEDTGBPS					= "TGF.Selected";
	public static final String TARGETLIST						= "TGF.TargetGroupList";
	public static final String SELECTTGBPTEXT					= "TGF.SelectTgBP";
	
	public static final String ADDTOTARGETEVENT					= "AddToTarget";
	public static final String SEARCHTGEVENT					= "SearchTargetGroup";
	public static final String CLEAREVENT						= "Clear";
	public static final String SEARCHBPARTNERSEVENT				= "SearchBusinessPartners";
	public static final String CLEARSELECT       				= "Clear.SelectTgBP";
	public static final String ADDTAGRGETSPARAMETER				= "AddTargetsParameter";
}
