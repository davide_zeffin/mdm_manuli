/*
 * Created on Apr 30, 2004
 *
 * To change the template for this generated file go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
package com.sap.isa.auction.actionforms.seller;

/**
 * @author I803067
 *
 * To change the template for this generated type comment go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
public interface HeaderFormConstants {

	public static final String GREETINGUSER 	= "HF.GreetingUser";
	public static final String LOGOUT			= "HF.Logout";
	public static final String AUCTIONS			= "HF.AuctionManagement";
	public static final String MONITORAUCTION 	= "HF.MonitorAuctions";
	public static final String PRODUCTCATALOG 	= "HF.ProductCatalog";
	public static final String CREATEAUCTION	= "HF.CreateAuction";
	public static final String HOME				= "HF.Home";
	public static final String PUBLISHINGQUEUE	= "HF.PublishingQueue";
	public static final String LOGOFFCONFMSG	= "HF.LogoffMessage";
	public static final String DETERMINEWINNER = "HF.DetermineWinner";
	
}
