/*
 * Created on Apr 30, 2004
 *
 * To change the template for this generated file go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
package com.sap.isa.auction.actionforms.seller;

import com.sap.isa.auction.actionforms.BaseForm;
import com.sap.isa.auction.helpers.HelpManager;
import com.sap.isa.auction.helpers.LoginHelper;


/**
 * @author I803067
 *
 * To change the template for this generated type comment go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
public class HeaderForm extends BaseForm implements HeaderFormConstants  {

	public String getUserName()  {
		LoginHelper loginHelper = (LoginHelper)getHelper(HelpManager.LOGINHELPER);
		String user = loginHelper.getGreetingUserNameText();
		return user;		
	}
}
