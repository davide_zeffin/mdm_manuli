package com.sap.isa.auction.actionforms.seller;

import java.math.BigDecimal;
import java.util.Collection;
import java.util.Enumeration;
import java.util.Iterator;

import com.sap.isa.auction.actionforms.BaseForm;
import com.sap.isa.auction.actions.seller.linkEH.CatalogCategoryEH;
import com.sap.isa.auction.bean.AuctionItem;
import com.sap.isa.auction.helpers.AuctionEditHelper;
import com.sap.isa.auction.helpers.AuctionLineItem;
import com.sap.isa.auction.helpers.CatalogHelper;
import com.sap.isa.auction.helpers.ConversionHelper;
import com.sap.isa.auction.helpers.HelpManager;
import com.sap.isa.auction.helpers.HomeHelper;
import com.sap.isa.auction.helpers.ProductHelper;
import com.sap.isa.auction.helpers.ProductsContainer;
import com.sap.isa.auction.models.tablemodels.seller.ProductsTableModel;
import com.sap.isa.auction.util.Utilidad;
import com.sap.isa.catalog.webcatalog.WebCatArea;
import com.sap.isa.catalog.webcatalog.WebCatAreaList;
import com.sap.isa.catalog.webcatalog.WebCatItem;
import com.sap.isa.core.util.WebUtil;
import com.sap.isa.ui.UIConstants;
import com.sapportals.htmlb.InputField;
import com.sapportals.htmlb.ItemList;
import com.sapportals.htmlb.Link;
import com.sapportals.htmlb.TextView;
import com.sapportals.htmlb.Tree;
import com.sapportals.htmlb.TreeNode;
import com.sapportals.htmlb.enum.TextViewDesign;
import com.sapportals.htmlb.table.TableView;
import com.sapportals.htmlb.type.AbstractDataType;

public class ProductCatalogForm
	extends BaseForm
	implements ProductCatalogConstants {

	private static final String PRODUCLISTURL =
		Utilidad.CONTAINERPATH
			+ "=seller/productlist.do%3F"
			+ CATEGORYID
			+ "%3D";
	protected ProductsTableModel productsTableModel;
	private TableView productTable;

	private Tree categoryTree;
	private Boolean searchTriggered = null;
	private CatalogHelper catalogHelper;

	public ProductCatalogForm() {
	}

	public void initializeCatalog() {
		final String methodName = "Initializing Catalog";
		entering(methodName);
		logInfo("release db connection");
		HomeHelper homeHelper = (HomeHelper) getHelper(HelpManager.HOMEHELPER);
		homeHelper.intermediateCleanup();
		homeHelper.cleanDBConnections();
		productsTableModel =
			new ProductsTableModel(session, request.getContextPath());
		String reference = "";
		/*WebUtil.getAppsURL(servlet.getServletContext(),
		                                               request, response, Boolean.FALSE,
		                                               AuctionDetailsConstants.PRODUCTDETAILSURL+ProductsTableModel.ID,
		                                               null , null , false);*/
		productsTableModel.setReferenceTarget(reference, PRODUCTDETAILSTARGET);
		Collection previousCategoryItems =
			getCatalogHelper().getProductsForCurrentCategory();
		if (previousCategoryItems != null) {
			productsTableModel.setData(previousCategoryItems);
		}
		this.setAttribute(PRODUCTMODEL, productsTableModel);
		productTable = new TableView(PRODUCTTABLE);
		this.setAttribute(PRODUCTTABLE, productTable);

		categoryTree = new Tree(CATALOGTREE);
		this.setAttribute(CATALOGTREE, categoryTree);
		if (request.getParameter(OPENMODE) != null
			&& request.getParameter(OPENMODE).equals(CREATEMODE)) {
			getProductHelper().setMode(ProductHelper.CREATE);
		} else {
			getProductHelper().setMode(ProductHelper.BROWSE);
		}
		if (request.getParameter(CONTEXTPARAMETER) != null) {
			if (request
				.getParameter(CONTEXTPARAMETER)
				.equals(CatalogHelper.CREATIONCONTEXT)) {
				getCatalogHelper().setMode(CatalogHelper.CREATIONCONTEXT);
			} else {
				getCatalogHelper().setMode(CatalogHelper.SELECTIONCONTEXT);
			}
		} else {
			getCatalogHelper().setMode("");
			clearSelection();
		}
		initializeCategories();
		setSelectionList();
		exiting(methodName);
	}

	public void initializeCategories() {
		entering("Initializing Catalog Tree");
		TreeNode root = new TreeNode(CATALOGROOT, "ProductCatalog");
		categoryTree.setRootNode(root);
		categoryTree.setRootNodeIsVisible(false);
		this.setAttribute(CATALOGROOT, root);

		WebCatAreaList list = null;

		list = getCatalogHelper().getRootCategories();

		TreeNode node = null;
		if (null == list) {
			node = new TreeNode("NoData");
			node.setText(translate(NOCATEGORIESTEXT));
			node.setTooltip(node.getText());
			node.setParentNode(root);
			logInfo("No categories found in the Catalog");
			return;
		}

		Iterator iterator = list.iterator();
		WebCatArea area = null;
		root.setOpen(true);
		Link link = null;
		String linkId = null;
		String loadingText = translate(UIConstants.PAGELOADING);
		CatalogCategoryEH linkHandler = new CatalogCategoryEH();
		while (iterator.hasNext()) {
			area = (WebCatArea) iterator.next();
			node = new TreeNode(area.getAreaID(), area.getAreaID());
			node.setText(area.getAreaName());
			//node.setOnNodeClick(area.getAreaID());
			linkId = "L" + area.getAreaID();
			link = new Link(linkId, area.getAreaName());
			link.setOnClientClick("_auction_submitRequest()");
			action.registerEventHandler(link.getId(), linkHandler);
			node.setComponent(link);
			//node.setComponent(new TextView(area.getAreaName()));
			link.setOnClick(area.getAreaID());
			node.setOnNodeClick(area.getAreaID());
			node.setOnNodeExpand(area.getAreaID());

			node.setOpen(false);
			node.setTooltip(area.getAreaName());
			node.addChildNode(
				new TreeNode(area.getAreaID() + "Dummy", loadingText));
			root.addChildNode(node);
		}
		exiting("Initializing Catalog Tree");
	}

	public void expandCategory(String componentId, String id) {

		entering("Expanding the Catalog Tree");
		searchTriggered = new Boolean(false);
		Tree tree = (Tree) this.getComponentForId(CATALOGTREE);
		TreeNode root = (TreeNode) this.getAttribute(CATALOGROOT);
		categoryTree.updateNodeStatus(tree);
		TreeNode subNode = null;

		if (getCatalogHelper().getCurrentAreaId() != null) {
			try {
				TreeNode prevNode =
					root.getNodeByID(getCatalogHelper().getCurrentAreaId());
				if (prevNode.getComponent() != null
					&& prevNode.getComponent() instanceof TextView) {
					((TextView) prevNode.getComponent()).setDesign(
						TextViewDesign.STANDARD);
				}
			} catch (Exception exc) {
				logInfo("Unable to retrieve the Product Description", exc);
			}
		}
		try {
			subNode = root.getNodeByID(id);
		} catch (Exception exc) {
			logInfo(
				"Unable to update the status or get the Node for the event",
				exc);
		}
		if (null == subNode) {
			logInfo("Expand Category Action : Invalid node id - " + id);
			return;
		}
		if (subNode.getComponent() != null
			&& subNode.getComponent() instanceof TextView) {
			((TextView) subNode.getComponent()).setDesign(
				TextViewDesign.EMPHASIZED);
		}

		WebCatArea area = getCatalogHelper().getWebCatArea(id);
		
		if(subNode.getChildNodes() != null)  {
			Enumeration enum = subNode.getChildNodes();
			if(enum != null && enum.hasMoreElements() && enum.nextElement() != null)  {
				if(enum.hasMoreElements())  {
					return;
				}
			}
		}
		if (null == area) {
			logInfo("Expand Category Action : Invalid Category id - " + id);
			return;
		}

		WebCatAreaList list = null;

		try {
			list = area.getChildren();
		} catch (Exception exc) {
			logInfo("Unable load sub categories for category - " + id, exc);
			return;
		}
		WebCatArea subArea = null;

		Iterator iterator = list.iterator();
		subNode.setOpen(true);
		subNode.setOnNodeExpand(null);

		//remove the dummy node
		if (subNode.getChildNodes().hasMoreElements()) {
			subNode.removeChildNode(
				(TreeNode) subNode.getChildNodes().nextElement());
		}

		Link link = null;
		TreeNode node = null;
		String linkId = null;
		String loadingText = translate(UIConstants.PAGELOADING);
		CatalogCategoryEH linkHandler = new CatalogCategoryEH();
		while (iterator.hasNext()) {
			subArea = (WebCatArea) iterator.next();
			node =
				new TreeNode(subArea.getAreaID(), subArea.getAreaID(), subNode);
			node.setText(subArea.getAreaName());
			//node.setOnNodeClick(subArea.getAreaID());
			node.setOnNodeExpand(subArea.getAreaID());
			node.addChildNode(
				new TreeNode("Dummy" + subArea.getAreaID(), loadingText));
			node.setOpen(false);
			linkId = "L" + subArea.getAreaID();
			link = new Link(linkId, subArea.getAreaName());
			link.setReference(
				WebUtil.getAppsURL(
					servlet.getServletContext(),
					request,
					response,
					null,
					PRODUCLISTURL + subArea.getAreaID(),
					null,
					null,
					false));
			link.setTarget(PRODUCTLISTTARGET);
			link.setOnClientClick("_auction_submitRequest()");
			node.setComponent(link);
			action.registerEventHandler(link.getId(), linkHandler);
			link.setOnClick(area.getAreaID());
			node.setOnNodeClick(subArea.getAreaID());
			//node.addChildNode(new TreeNode(area.getAreaID()+"Dummy" , "Loading..."));
			node.setTooltip(subArea.getAreaName());
		}
		exiting("Expanding the Catalog Tree");
		setProductList(id);
	}

	public void setProductList(String id) {
		entering("Getting the Product list for a Category");
		if (null == id) {
			productsTableModel.setData(
				getCatalogHelper().getProductsForCurrentCategory());
			if (0 == productsTableModel.getRowCount()) {
				searchTriggered = null;
			} else {
				searchTriggered = Boolean.FALSE;
			}
		} else {
			searchTriggered = Boolean.FALSE;
			productsTableModel.setData(getCatalogHelper().getProducts(id));
		}
		setSelectionList();
		exiting("Getting the Product list for a Category");
	}

	public String getCategoryPath() {
		return getCatalogHelper().getCategoryPath();
	}

	public void addProducts() {
		final String methodName = "addProducts";
		entering(methodName);
		TableView oldView = (TableView) getComponentForId(PRODUCTTABLE);
		productsTableModel.updateSelection(oldView);
		updateProductSelection();
		productsTableModel.clearSelection();
		exiting(methodName);
	}

	public void removeProducts() {
		final String methodName = "removeProducts";
		entering(methodName);
		TableView oldView = (TableView) getComponentForId(PRODUCTTABLE);
		productsTableModel.updateSelection(oldView);
		removeProductSelection();
		productsTableModel.clearSelection();
		exiting(methodName);

	}

	/**
	 * 
	 */
	private void removeProductSelection() {

	}

	public void clearSelection() {
		//getProductContainerHelper().clearSelection();
		ProductHelper helper = getProductHelper();
		helper.clearSelection();
        if (getProductContainerHelper().getSelectedItems() != null && getProductContainerHelper().getSelectedItems().size() > 0) {
            getProductContainerHelper().clearSelection();
        }
		setSelectionList();
	}

	protected void updateProductSelection() {
		final String methodName = "updateProductSelection";
		entering(methodName);
		clearErrors();
		Enumeration enumerate = productsTableModel.getSelectedRows();
		WebCatItem item = null;
		ConversionHelper helper = getConversionHelper();
		clearSalesAreaError();
		try {

			while (enumerate.hasMoreElements()) {
				int row = Integer.parseInt((String) enumerate.nextElement());
				item = (WebCatItem) productsTableModel.getValueAt(row);
				if (null == item) {
					logInfo(
						"UpdateProductSelection - Selected item is null" + row);
				}

				String qty = "0";
				try {
					AbstractDataType qtyField =
						this.getPageContext().getDataForComponentId(
							PRODUCTTABLE,
							ProductsTableModel.QUANTITY,
							row - productsTableModel.getVisibleFirstRow() + 1);
					qty = qtyField.getValueAsString().trim();
				} catch (Exception exc) {
					logInfo(
						"Unable to retrieve the quantity information for Product"
							+ item.getProduct(),
						exc);
				}

				BigDecimal quantity = new BigDecimal(0.0);
				if (qty != null) {
					try {
						quantity = helper.uiCurrencyStringToBigDecimal(qty);
					} catch (Exception exc) {
						logInfo(
							"Invalid Quantity set for the Item  - "
								+ item.getProduct());
						continue;
					}
				}
				if (!getProductContainerHelper()
					.addItem(item, quantity.doubleValue())) {
					//setSalesAreaError();
				}
			}
			setSelectionList();

		} catch (Exception exc) {
			logInfo("Unable to add items from catalog to auction");
			logInfo("detail message: ", exc);
		}
		exiting(methodName);
	}

	/**
	 * 
	 */
	protected void clearSalesAreaError() {
		TextView view = createTextView("", false, false);
		this.setAttribute(ADDERRORMSG, view);
	}

	protected void setSelectionList() {
		final String methodName = "Setting the selection list";
		entering(methodName);
		ItemList itemList = new ItemList();
		this.setAttribute(SELECTIONLIST, itemList);
		AuctionLineItem product = null;
		ConversionHelper helper = getConversionHelper();
		CatalogHelper catalogHelper = getCatalogHelper();
		Link link = null;
		String reference = "";
		/*WebUtil.getAppsURL(servlet.getServletContext(),
		                                               request, response, Boolean.FALSE,
		                                               AuctionDetailsConstants.PRODUCTDETAILSURL+ProductsTableModel.ID,
		                                               null , null , false);*/
		TextView view = null;
		Collection selection = getProductContainerHelper().getSelectedItems();
		Object item = null;
		AuctionItem auctionItem = null;
		if (selection != null && selection.size() > 0) {
			Iterator iterator = selection.iterator();
			while (iterator.hasNext()) {
				item = iterator.next();
				if (item instanceof AuctionLineItem) {
					product = (AuctionLineItem) item;
					itemList.addComponent(
						createTextView(
							product.getProductId()
								+ "("
								+ helper.bigDecimalToUIQuantityString(
									new BigDecimal(product.getQuantity()))
								+ ")",
							false,
							true));
				} else {
					auctionItem = (AuctionItem) item;
					itemList.addComponent(
						createTextView(
							auctionItem.getProductCode()
								+ "("
								+ helper.bigDecimalToUIQuantityString(
									new BigDecimal(auctionItem.getQuantity()))
								+ ")",
							false,
							true));
				}
			}
		} else {
			view = createTextView(translate(EMPTYTEXT), false, true);
			itemList.addComponent(view);
		}
		exiting(methodName);
	}

	public void searchProducts() {
		final String methodName = "Search for Products";
		entering(methodName);
		searchTriggered = new Boolean(true);

		String productName = null;
		String productDescription = null;

		try {
			productName =
				((InputField) this.getComponentForId(SEARCHBYID))
					.getString()
					.getValue();
		} catch (Exception exc) {
			logInfo("Unable to retrieve the Product ID", exc);
		}

		try {
			productDescription =
				((InputField) this.getComponentForId(SEARCHBYDESC))
					.getString()
					.getValue();
		} catch (Exception exc) {
			logInfo("Unable to retrieve the Product Description", exc);
		}

		productsTableModel.setData(
			getCatalogHelper().searchItems(
				productName,
				productDescription,
				null));

		exiting(methodName);
	}

	public Boolean isSearchTrigger() {
		return searchTriggered;
	}

	public boolean isBrowseCatalog() {
		ProductHelper helper = getProductHelper();
		return (helper.getMode() == ProductHelper.BROWSE);
	}

	public int getSelectionCount() {
		return getProductContainerHelper().getSelectionCount();
	}

	protected ProductHelper getProductHelper() {
		return (ProductHelper) getHelper(HelpManager.PRODUCTHELPER);
	}

	protected ProductsContainer getProductContainerHelper() {
		if (getCatalogHelper()
			.getMode()
			.equals(CatalogHelper.CREATIONCONTEXT)) {
			return (AuctionEditHelper) getHelper(HelpManager.AUCTIONEDITHELPER);
		} else {
			return getProductHelper();
		}
	}

	protected CatalogHelper getCatalogHelper() {
		return (CatalogHelper) getHelper(HelpManager.CATALOGHELPER);
	}

	public int getProductsCount() {
		if (productsTableModel != null) {
			return productsTableModel.getRowCount();
		} else {
			return 0;
		}
	}

	public String getContext() {
		return getCatalogHelper().getMode();
	}
}