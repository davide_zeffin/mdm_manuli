package com.sap.isa.auction.actionforms.seller;

import java.math.BigDecimal;
import java.util.Collection;

import com.sap.isa.auction.backend.boi.order.AuctionOrderData;
import com.sap.isa.auction.bean.AuctionTypeEnum;
import com.sap.isa.auction.bean.Winner;
import com.sap.isa.auction.bean.b2x.PrivateAuction;
import com.sap.isa.auction.businessobject.order.AuctionOrder;
import com.sap.isa.auction.helpers.HelpManager;
import com.sap.isa.auction.helpers.TargetGroupBPHelper;
import com.sap.isa.auction.helpers.OrderHelper;
import com.sap.isa.auction.models.listmodels.seller.OrdersListModel;
import com.sap.isa.backend.boi.isacore.AddressData;
import com.sap.isa.backend.boi.isacore.ShipToData;
import com.sap.isa.backend.boi.isacore.order.HeaderData;
import com.sap.isa.backend.boi.isacore.order.OrderData;
import com.sap.isa.businesspartner.businessobject.BusinessPartner;
import com.sap.isa.core.TechKey;
import com.sapportals.htmlb.DropdownListBox;
import com.sapportals.htmlb.TextView;

/**
 * Title:			Internet Sales Private Auctions
 * Description:
 * @Copyright: 		Copyright (c) 2004
 * Company:			SAPLabs LLC
 * @author 
 * Created on: 		Jun 9, 2004
 */
public class OrdersForm extends AuctionDetailsForm {
	//order tab
	private DropdownListBox orders;
	private OrdersListModel ordersModel;
	
	private TextView winnerName;
	private TextView orderNumber;
	private TextView quantity;
	private TextView deliveryStatus;
	private TextView status;
	private TextView netTotal;
	private TextView grossTotal;
	private TextView tax;
	private TextView freight;
	private TextView insurance;
	private TextView shippingCondition;
	private TextView paymentType;
	private TextView name;
	private TextView city;
	private TextView country;
	private TextView region;

	private String orderId;
	private OrderHelper orderHelper;
	boolean orderExist = false;

	public void initializeOrdersInfo() {
		final String METHOD = "initializeOrdersInfo";
		entering(METHOD);
		initializeOrderDetailsComponents();
		auction = (PrivateAuction) getDetailsHelper().getAuction();
		Collection winners = getDetailsHelper().getWinners(auction);
		if (auction.getType() == AuctionTypeEnum.BROKEN_LOT.getValue()) {
			orders = (DropdownListBox) addControl(HTMLB_DROPDOWN, ORDERSLST_ID);
			ordersModel = new OrdersListModel(session);
			ordersModel.setData(winners);
			this.setAttribute(ORDERSLSTMODEL_ID, ordersModel);
		}
		if (winners != null && !winners.isEmpty()) {
			Winner winner = (Winner) winners.iterator().next();
			setOrderDetails(winner.getOrderGuid());
			BigDecimal qty = (BigDecimal) winner.getQuantity();
			quantity.setText(
				getConversionHelper().bigDecimalToUIQuantityString(qty));
			if (auction.getType() != AuctionTypeEnum.BROKEN_LOT.getValue()) {
				winnerName = (TextView)addControl(HTMLB_TEXTVIEW, WINNER_ID);
				BusinessPartner bp = ((TargetGroupBPHelper)getHelper(HelpManager.TARGETGROUPHELPER))
									.getBusinessPartner(
										winner.getUserId());
				StringBuffer sb = new StringBuffer();
				if (bp.getAddress().getFirstName() != null) {
					sb.append(bp.getAddress().getFirstName());
					sb.append(" ");
				}
				if (bp.getAddress().getLastName() != null) {
					sb.append(bp.getAddress().getLastName());
				}
				winnerName.setText(sb.toString());
			}
		} else
			clearOrderDetails();
		exiting(METHOD);
	}

	private void initializeOrderDetailsComponents() {
		final String METHOD = "initializeOrderDetailsComponents";
		entering(METHOD);
		orderNumber = (TextView) addControl(HTMLB_TEXTVIEW, ORDERNUMBERID);
		quantity = (TextView) addControl(HTMLB_TEXTVIEW, ORDERQTYVIEW);
		deliveryStatus =
			(TextView) addControl(HTMLB_TEXTVIEW, DELIVERYSTATUSID);
		status = (TextView) addControl(HTMLB_TEXTVIEW, ORDERSTATUSID);
		netTotal = (TextView) addControl(HTMLB_TEXTVIEW, TOTALAMOUNTNETID);
		grossTotal = (TextView) addControl(HTMLB_TEXTVIEW, TOTALAMOUNTGROSSID);
		tax = (TextView) addControl(HTMLB_TEXTVIEW, TAXID);
		freight = (TextView) addControl(HTMLB_TEXTVIEW, FREIGHTID);
		shippingCondition =
			(TextView) addControl(HTMLB_TEXTVIEW, SHIPPINGCONDITIONID);
		paymentType = (TextView) addControl(HTMLB_TEXTVIEW, PAYMENTMETHODID);
		insurance = (TextView) addControl(HTMLB_TEXTVIEW, INSURANCEID);
		name = (TextView) addControl(HTMLB_TEXTVIEW, NAMEID);
		city = (TextView) addControl(HTMLB_TEXTVIEW, CITYID);
		country = (TextView) addControl(HTMLB_TEXTVIEW, COUNTRYID);
		region = (TextView) addControl(HTMLB_TEXTVIEW, REGIONID);

		exiting(METHOD);
	}

	private void clearOrderDetails() {
		final String METHOD = "clearOrderDetails";
		entering(METHOD);
		orderNumber.setText("-");
		quantity.setText("-");
		deliveryStatus.setText("-");
		status.setText("-");
		netTotal.setText("-");
		grossTotal.setText("-");
		tax.setText("-");
		freight.setText("-");
		shippingCondition.setText("-");
		paymentType.setText("-");
		insurance.setText("-");
		name.setText("-");
		city.setText("-");
		country.setText("-");
		region.setText("-");

		exiting(METHOD);
	}
	public void setOrderDetails(TechKey orderKey) {
		final String METHOD = "setOrderDetails";
		entering(METHOD);
		clearOrderDetails();
		String id;
		if (orderKey == null) {
			id = getValue(ORDERSLST_ID);
			BigDecimal qty =
				(BigDecimal) ordersModel.getQuantityForOrder(id);
			quantity.setText(
				getConversionHelper().bigDecimalToUIQuantityString(qty));
		}
		else
			id = orderKey.toString();

		orderId = id;
		OrderData order = getOrderHelper().loadOrderById(new TechKey(id));
		if (order != null) {
			orderExist = true;
			if (auction.getType() == AuctionTypeEnum.BROKEN_LOT.getValue()) {
				orders.setSelection(orderId);
				ordersModel.setSelection(orderId);
			}
			HeaderData headerData = order.getHeaderData();
			//orderNumber.setText(order.getHeader().getSalesDocNumber());
			orderId = headerData.getSalesDocNumber();
			orderNumber.setText(orderId);
			String currency = " " + headerData.getCurrency();
			if (order instanceof AuctionOrderData) {
				AuctionOrderData aucOrder = (AuctionOrderData) order;
				deliveryStatus.setText(
					aucOrder.getDeliveryBlockText() != null
						? aucOrder.getDeliveryBlockText()
						: "-");
			}
			String insuranceVale = ((AuctionOrder) order).getInsuranceValue();
			insurance.setText(
				insuranceVale != null ? insuranceVale + currency : "");
			status.setText(
				headerData.getStatus() != null ? headerData.getStatus() : "");

			netTotal.setText(
				headerData.getNetValue() != null
					? headerData.getNetValue() + currency
					: "");

			grossTotal.setText(
				headerData.getGrossValue() != null
					? headerData.getGrossValue() + currency
					: "");

			tax.setText(
				headerData.getTaxValue() != null
					? headerData.getTaxValue() + currency
					: "");

			freight.setText(
				headerData.getFreightValue() != null
					? headerData.getFreightValue() + currency
					: "");

			shippingCondition.setText(
				headerData.getShipCond() != null
					? headerData.getShipCond()
					: "");

			ShipToData shipTo = headerData.getShipToData();
			if (shipTo != null) {
				AddressData addressData = shipTo.getAddressData();
				if (addressData != null) {

					StringBuffer buf = new StringBuffer();
					if (addressData.getLastName() != null
						&& addressData.getLastName().trim().length() > 0) {
						buf.append(addressData.getLastName());

						if (addressData.getFirstName() != null) {
							buf.append(" , ");
							buf.append(addressData.getFirstName());
						}
					}
					else {
						buf.append(addressData.getName1());
					}
					name.setText(buf.toString());
					buf = new StringBuffer();
					if (addressData.getStreet() != null
						&& !addressData.getStreet().trim().equals("")) {
						buf.append(addressData.getStreet());
						if (addressData.getHouseNo() != null
							&& !addressData.getHouseNo().trim().equals("")) {
								buf.append(", ");
								buf.append(addressData.getHouseNo());
						}
						city.setText(buf.toString());
						buf = new StringBuffer();
						if (addressData.getCity() != null
							&& !addressData.getCity().trim().equals("")) {
							buf.append(addressData.getCity());
							buf.append(" , ");
						}
						if (addressData.getCity() != null
							&& !addressData.getCity().trim().equals("")) {
							buf.append(addressData.getPostlCod1());
						}
						region.setText(buf.toString());
						buf = new StringBuffer();
						if (addressData.getRegion() != null
							&& !addressData.getRegion().trim().equals("")) {
							String region = addressData.getRegion();

							if (region != null && !region.equals("")) {
								buf.append(region);
							} else {
								buf.append(
									addressData.getRegion() != null
										? addressData.getRegion()
										: "");
							}
							buf.append(" , ");
						}
						if (addressData.getCountry() != null
							&& !addressData.getCountry().trim().equals("")) {
							String ctry = addressData.getCountry();
							;
							if (ctry != null && !ctry.equals("")) {
								buf.append(ctry);
							} else {
								buf.append(
									addressData.getCountry() != null
										? addressData.getCountry()
										: "");
							}
							country.setText(buf.toString());
						}
					} else {
						city.setText("-");
					}
				}
			}
		} else
			orderExist = false;
		exiting(METHOD);
	}
	public int getAuctionType() {
		return auction.getType();
	}

	public boolean isOrderExist() {
		return orderExist;
	}

	private OrderHelper getOrderHelper() {
		final String METHOD = "getOrderHelper";
		entering(METHOD);
		if (null == orderHelper) {
			orderHelper = (OrderHelper) getHelper(HelpManager.ORDERHELPER);
		}
		exiting(METHOD);
		return orderHelper;
	}

}
