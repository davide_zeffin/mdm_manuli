/*
 * Created on May 1, 2004
 *
 * To change the template for this generated file go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
package com.sap.isa.auction.actionforms.seller;

/**
 * @author I803067
 *
 * To change the template for this generated type comment go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
public interface CreateSelectionConstants {

	public static final String PRODUCTSSELECTIONMODEL = "ProductsModel";
	public static final String PRODUCTSSELECTIONVIEW  = "ProductsView";
	
	public static final String TARGETGROUPSSELECTIONMODEL = "TargetGroupModel";
	public static final String TARGETGROUPSSELECTIONVIEW  = "TargetGroupView";

	public static final String NOPRODUCTSSELECTEDTEXT = "CSF.NoProducts";
	public static final String NOTARGETGROUPSSELECTEDTEXT = "CSF.NoTargets";
		
	public static final String SELECTIONTITLE	 	= "CSF.CreateAuction";
	public static final String CREATEAUCTIONTEXT	= "CSF.Create";
	public static final String CREATEOPTION1TEXT	= "CSF.Single";
	public static final String CREATEOPTION2TEXT	= "CSF.Multiple";
	
	public static final String SELECTEDPRODUCTSTEXT		= "CSF.SelectedProducts";
	public static final String SELECTEDTARGETGROUPSTEXT	= "CSF.SelectedTargets";
	
	public static final String ADDMOREPRODUCTSTEXT		= "CSF.AddProducts";
	public static final String ADDMORETARGETSTEXT		= "CSF.AddTargets";
	public static final String REMOVETEXT				= "CSF.Remove";
	
	public static final String CREATEOPTIONS		= "CreateOptions";
	public static final String CREATEEVENT			= "CreateAuctionEvent";
	public static final String ADDMOREPRODUCTSEVENT	= "AddMoreProd";
	public static final String REMOVEPRODUCTSEVENT	= "RemoveProd";
	public static final String ADDMORETARGETGROUPSEVENT	= "AddMoreTarget";
	public static final String REMOVETARGETEVENT		= "RemoveTarget";
    
    public static final String MULTIPLE                 = "multiple";
	
}
