/*
 * Created on May 1, 2004
 *
 * To change the template for this generated file go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
package com.sap.isa.auction.actionforms.seller;

import java.math.BigDecimal;
import java.util.Enumeration;

import com.sap.isa.auction.actionforms.BaseForm;
import com.sap.isa.auction.helpers.AuctionLineItem;
import com.sap.isa.auction.helpers.ConversionHelper;
import com.sap.isa.auction.helpers.HelpManager;
import com.sap.isa.auction.helpers.ProductHelper;
import com.sap.isa.auction.helpers.TargetGroupBPHelper;
import com.sap.isa.auction.models.tablemodels.seller.BusinessPartnerTGModel;
import com.sap.isa.auction.models.tablemodels.seller.ProductsTableModel;
import com.sapportals.htmlb.DropdownListBox;
import com.sapportals.htmlb.table.TableView;
import com.sapportals.htmlb.type.AbstractDataType;

/**
 * @author I803067
 *
 * To change the template for this generated type comment go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
public class CreateSelectionForm extends BaseForm implements CreateSelectionConstants  {

	private TableView selectedProductsView;
	private ProductsTableModel selectedProductsModel;
	
	private TableView selectedTargetsView;
	private BusinessPartnerTGModel selectedTargetsModel;
	
	private DropdownListBox createOptionsEdit;
	private boolean initialized = false;
	
	private void initializeSelection()  {
		if(initialized)
			return;
		selectedProductsView = (TableView)addControl(HTMLB_TABLEVIEW , PRODUCTSSELECTIONVIEW);
		selectedProductsModel = new ProductsTableModel(session , request.getContextPath());
		this.setAttribute(PRODUCTSSELECTIONMODEL , selectedProductsModel);
		
		selectedTargetsView = (TableView)addControl(HTMLB_TABLEVIEW , TARGETGROUPSSELECTIONVIEW);
		selectedTargetsModel = new BusinessPartnerTGModel(servlet , session);
		selectedTargetsModel.setContextPath(request.getContextPath());
		selectedTargetsModel.setNotApplicableText(translate(TargetGroupsForm.NOTAPPLICABLETEXT));
		this.setAttribute(TARGETGROUPSSELECTIONMODEL , selectedTargetsModel);
		
		createOptionsEdit = (DropdownListBox)addControl(HTMLB_DROPDOWN , CREATEOPTIONS);
		createOptionsEdit.addItem(CREATEOPTION1TEXT , translate(CREATEOPTION1TEXT));
		createOptionsEdit.addItem(CREATEOPTION2TEXT , translate(CREATEOPTION2TEXT));
        if (this.request.getRequestURI().indexOf(MULTIPLE) < 0) {
            createOptionsEdit.setSelection(CREATEOPTION1TEXT);
        }
        else {
            createOptionsEdit.setSelection(CREATEOPTION2TEXT);
        }
		
	}

	public void setSelection()  {
		initializeSelection();
		ProductHelper productHelper = (ProductHelper)getHelper(HelpManager.PRODUCTHELPER);
		TargetGroupBPHelper targetHelper = (TargetGroupBPHelper)getHelper(HelpManager.TARGETGROUPHELPER);
		selectedProductsModel.setData(productHelper.getSelectedItems());
		selectedTargetsModel.setData(targetHelper.getTargetGroupBPSelection() , "" , "" , false);
	}
  	
	public boolean isSingleCreation()  {
		boolean singleCreation = true;
		String selectedItem = getValue(CREATEOPTIONS);
		if(selectedItem.equals(CREATEOPTION1TEXT))  {
			singleCreation = true;
		}  else if(selectedItem.equals(CREATEOPTION2TEXT))  {
			singleCreation = false;
		}
		return singleCreation;
	}
	
	public void updateProducts()  {
		TableView view = (TableView)getComponentForId(PRODUCTSSELECTIONVIEW);
		if(view != null)  {
			selectedProductsModel.updateSelection(view);
		}
		AuctionLineItem item = null;
		ConversionHelper conversionHelper = (ConversionHelper)getHelper(HelpManager.CONVERSIONHELPER);
		for(int i = 0; i < selectedProductsModel.getRowCount(); i++)  {
			String qty = "0";
			item = (AuctionLineItem)selectedProductsModel.getValueAt(i + 1);
			if (!isSingleCreation()) {
				logInfo("retrieve the quantity per auction for product" + item.getDescription());
				try {
					AbstractDataType qtyField = this.getPageContext().getDataForComponentId(PRODUCTSSELECTIONVIEW
												,ProductsTableModel.QUANTPERAUCT
												,i + 1 - selectedProductsModel.getVisibleFirstRow() + 1);
					qty = qtyField.getValueAsString().trim();
				}  catch(Exception exc)  {
					logInfo("Unable to retrieve the quantity per auction for Product" + item.getDescription() , exc);
				}
				BigDecimal quantity = new BigDecimal(0.0);
				if (qty != null) {
					try  {
						quantity = conversionHelper.uiCurrencyStringToBigDecimal(qty);
					}  catch(Exception exc)  {
						logInfo("Invalid Quantity set for the Item  - " + item.getDescription());
						continue;
					}
				}
				item.setQuantityPerAuction(quantity.intValue());
			}
			
			try {
				logInfo("update the quantity information for product" + item.getDescription());
				AbstractDataType qtyField = this.getPageContext().getDataForComponentId(PRODUCTSSELECTIONVIEW
											,ProductsTableModel.QUANTITY
											,i + 1 - selectedProductsModel.getVisibleFirstRow() + 1);
				qty = qtyField.getValueAsString().trim();
			}  catch(Exception exc)  {
				logInfo("Unable to retrieve the quantity information for Product" + item.getDescription() , exc);
			}
			BigDecimal quantity = new BigDecimal(0.0);
			if (qty != null) {
				try  {
					quantity = conversionHelper.uiCurrencyStringToBigDecimal(qty);
				}  catch(Exception exc)  {
					logInfo("Invalid Quantity set for the Item  - " + item.getDescription());
					continue;
				}
			}
			item.setQuantity(quantity.doubleValue());
			ProductHelper productsHelper = (ProductHelper)getHelper(HelpManager.PRODUCTHELPER);
			productsHelper.updateItem(item);
		}
	}
	
	public void removeProducts()  {
		TableView view = (TableView)getComponentForId(PRODUCTSSELECTIONVIEW);
		if(view != null)  {
			selectedProductsModel.updateSelection(view);
		}
		Enumeration selection = selectedProductsModel.getSelectedRows();
		AuctionLineItem item = null;
		ProductHelper productsHelper = (ProductHelper)getHelper(HelpManager.PRODUCTHELPER);
		while(selection.hasMoreElements())  {
			int row = Integer.parseInt((String)selection.nextElement());
			item = (AuctionLineItem)selectedProductsModel.getValueAt(row);
			productsHelper.removeItem(item.getProductId());
		}
		selectedProductsModel.clearSelection();
	}
	
	public void removeTargetGroups()  {
		TableView view = (TableView)getComponentForId(TARGETGROUPSSELECTIONVIEW);
		if(view != null)  {
			selectedTargetsModel.updateSelection(view);
		}
		TargetGroupBPHelper targetGroupHelper = (TargetGroupBPHelper)getHelper(HelpManager.TARGETGROUPHELPER);
		TargetGroupBPHelper.TargetGroupBusinessPartner tgBP = null;
		Enumeration selection = selectedTargetsModel.getSelectedRows();
		while(selection.hasMoreElements())  {
			int row = Integer.parseInt((String)selection.nextElement());
			tgBP = (TargetGroupBPHelper.TargetGroupBusinessPartner)selectedTargetsModel.getValueAt(row);
			targetGroupHelper.removeTargetGroupBusinessPartner(tgBP);
		}
		selectedTargetsModel.clearSelection();		
	}
	
	public int getProductsCount()  {
		ProductHelper helper = (ProductHelper)getHelper(HelpManager.PRODUCTHELPER);
		return helper.getSelectionCount();
	}
	
	public int getTargetsCount()  {
		TargetGroupBPHelper targetGroupHelper = (TargetGroupBPHelper)getHelper(HelpManager.TARGETGROUPHELPER);
		return targetGroupHelper.getTargetGroupBPSelection() != null ?
				targetGroupHelper.getTargetGroupBPSelection().size() : 0;
	}
}
