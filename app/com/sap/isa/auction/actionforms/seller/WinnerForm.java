package com.sap.isa.auction.actionforms.seller;

import java.util.Collection;

import com.sap.isa.auction.actionforms.BaseForm;
import com.sap.isa.auction.bean.Auction;
import com.sap.isa.auction.bean.AuctionTypeEnum;
import com.sap.isa.auction.bean.b2x.PrivateAuction;
import com.sap.isa.auction.helpers.AuctionDetailsHelper;
import com.sap.isa.auction.helpers.HelpManager;
import com.sap.isa.auction.helpers.ManualWinnerHelper;
import com.sap.isa.auction.models.tablemodels.seller.WinnersTableModel;
import com.sapportals.htmlb.table.TableView;

/**
 * Title:			Internet Sales Private Auctions
 * Description:
 * @Copyright: 		Copyright (c) 2004
 * Company:			SAPLabs LLC
 * Created on:		Jun 23, 2004
 */
public class WinnerForm extends BaseForm 
		implements WinnerFormConstants{

	private WinnersTableModel winnersTableModel;
	private TableView winnersTableView;
	private AuctionDetailsHelper detailsHelper;
	private ManualWinnerHelper winnerHelper;
	
	/**
	 * 
	 *
	 */
	public void initializeWinnerInfo(){
		winnersTableModel = new WinnersTableModel(session);
		winnersTableView = (TableView)addControl(HTMLB_TABLEVIEW, 
				WINNERS_TBLVIEW_ID);
		
		Collection winners = getWinnerHelper().getWinners();
		winnersTableModel.setData(winners);
		winnersTableView.setModel(winnersTableModel);
		
		Auction auction = getDetailsHelper().getAuction();
		int auctionType = auction.getType();
		String showQuantity = null;
		if (AuctionTypeEnum.BROKEN_LOT.getValue() == auctionType){
			showQuantity = "true";
		}

		String totalWinners = String.valueOf(winners.size());
		this.setAttribute("totalWinners", totalWinners);		
		this.setAttribute("showQuantity", showQuantity);
		this.setAttribute(WINNERS_TBLMODEL_ID, winnersTableModel);
		this.setAttribute(WINNERS_TBLVIEW_ID, winnersTableView);
		
	}
	
	/**
	 * 
	 * @return
	 */
	protected AuctionDetailsHelper getDetailsHelper(){
		if (null == detailsHelper) {
			detailsHelper =
				(AuctionDetailsHelper) getHelper(HelpManager.
						AUCTIONDETAILSHELPER);
		}
		return detailsHelper;
	}
	
	/**
	 * 
	 * @return
	 */
	protected ManualWinnerHelper getWinnerHelper(){
		if (null == winnerHelper) {
			winnerHelper =
				(ManualWinnerHelper) getHelper(HelpManager.
						MANUALWINNERHELPER);
		}
		return winnerHelper;		
	}
}
