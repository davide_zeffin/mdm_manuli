/*
 * Created on May 10, 2004
 *
 * To change the template for this generated file go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
package com.sap.isa.auction.actionforms.seller;

/**
 * @author I803067
 *
 * To change the template for this generated type comment go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
public interface ConfirmationFormConstants extends AuctionDetailConstants  {
	
	public static final String AUCTIONSTABLEVIEW = "ConfirmedAuctionsTable";
	public static final String AUCTIONSTABLEMODEL = "ConfirmedAuctionsModel";
	
	public static final String CONFIRMATIONAUCTIONTITLE = "CF.SaveConfirmation";
	public static final String CONFIRMATIONDEFAULTTITLE = "CF.Auctions";
	
	public static final String SUBMITEVENT	= "Submit";
	public static final String SUBMITTEXT   = "CF.Submit";
	
	public static final String OPTIONSGROUP = "AfterSaveOptions";
	public static final String OPTIONS1TEXT = "CF.PublishNow";
	public static final String OPTIONS2TEXT = "CF.Schedule";
	
	public static final String OPTIONSTITLE = "CF.OptionsTitle";
}
