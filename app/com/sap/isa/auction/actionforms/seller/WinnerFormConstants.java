package com.sap.isa.auction.actionforms.seller;

/**
 * Title:			Internet Sales Private Auctions
 * Description:
 * @Copyright: 		Copyright (c) 2004
 * Company:			SAPLabs LLC
 * Created on:		Jun 23, 2004
 */
public interface WinnerFormConstants {

	public static final String WINNERS_TBLVIEW_ID = "WINNER_TBLVIEW_ID";
	public static final String WINNERS_TBLMODEL_ID = "WINNER_TBLMODEL_ID";
	
	public static final String QUANTITY = "WF.Quantity";
	public static final String TOTAL_WINNERS = "WF.TotalWinners";
	
	public static final String WINNERS = "WF.Winners";
	public static final String NOWINNERS = "WF.NoWinners";
}
