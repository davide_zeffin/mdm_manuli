/*
 * Created on May 25, 2004
 *
 * To change the template for this generated file go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
package com.sap.isa.auction.actionforms.seller;

import java.sql.ResultSet;
import java.util.Iterator;

import javax.servlet.ServletContext;

import com.sap.isa.auction.actionforms.BaseForm;
import com.sap.isa.auction.actions.BaseAction;
import com.sap.isa.auction.actions.seller.linkEH.ShopReadEH;
import com.sap.isa.auction.businessobject.b2x.AuctionBusinessObjectManager;
import com.sap.isa.auction.helpers.CatalogHelper;
import com.sap.isa.auction.helpers.HelpManager;
import com.sap.isa.auction.models.tablemodels.seller.ShopTableModel;
import com.sap.isa.businessobject.Search;
import com.sap.isa.businessobject.SearchResult;
import com.sap.isa.businessobject.Shop;
import com.sap.isa.businessobject.ShopListSearchCommand;
import com.sap.isa.core.ContextConst;
import com.sap.isa.core.SessionConst;
import com.sap.isa.core.UserSessionData;
import com.sap.isa.core.init.InitializationEnvironment;
import com.sap.isa.core.interaction.InteractionConfig;
import com.sap.isa.core.util.table.ResultData;
import com.sap.isa.user.businessobject.UserBase;
import com.sapportals.htmlb.GridLayout;
import com.sapportals.htmlb.GridLayoutCell;
import com.sapportals.htmlb.HTMLFragment;
import com.sapportals.htmlb.ItemList;
import com.sapportals.htmlb.Link;
import com.sapportals.htmlb.TextView;

/**
 * @author I803067
 *
 * To change the template for this generated type comment go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
public class ShopReadForm extends BaseForm  {

	public static final String SHOPSEDIT = "Shops";
	public static final String CHOOSETITLE = "SR.Title";
	public static final String NOSHOPSTEXT = "SRF.NoShops";
	private ShopTableModel shopsModel;
	
	private ItemList shopsEdit;
	
	
	public void showShops()  {
		
		shopsModel = new ShopTableModel();
		shopsModel.initializeData();
		this.setAttribute(SHOPSEDIT , shopsModel);
		UserSessionData userSession = UserSessionData.getUserSessionData(session);
		AuctionBusinessObjectManager auctionBOM = (AuctionBusinessObjectManager)
												userSession.getBOM(AuctionBusinessObjectManager.AUCTION_BOM);
		/*BusinessObjectManager businessObjectManager = (BusinessObjectManager)
							userSession.getBOM(BusinessObjectManager.ISACORE_BOM);*/
		try  {
			Search search = auctionBOM.createSearch();
			
			UserBase user = auctionBOM.getUserBase(); 
			String application = getInitParameter(ContextConst.IC_SCENARIO);

			// get the scenario form the XCM.
			InteractionConfig interactionConfig = action.getInteractionConfig(request).getConfig("shop");

			String scenario = interactionConfig.getValue("shopscenario");
        
			String configuration = (String)userSession.getAttribute(SessionConst.XCM_CONF_KEY);

			ShopListSearchCommand searchCommand = new ShopListSearchCommand(application,
																			scenario,
																			user.getLanguage(),
																			configuration);                                                                      
			SearchResult result = search.performSearch(searchCommand);
			// read the result set to diplay it on the jsp
			ResultData shopList = new ResultData(result.getTable());
			shopList.beforeFirst();
			Iterator iterator = (Iterator)new ResultSetIterator((ResultSet)shopList);
			GridLayout grid = null;
			GridLayoutCell cell = null;
			Link link = null;
			
			ShopReadEH shopReadEH = new ShopReadEH();
			int i = 1;
			while(iterator.hasNext())  {
				iterator.next();
				if (shopList.getString(Shop.AUCTION_FLAG) != null && shopList.getString(Shop.AUCTION_FLAG).length() > 0) {
					link = new Link("SHOP" + Integer.toString(i));
					link.setId("SHOP" + Integer.toString(i));
					link.setOnClick(shopList.getString(Shop.ID));
					link.setOnClientClick("_auction_submitRequest()");
					action.registerEventHandler(link.getId() , shopReadEH);
					link.addComponent(new TextView(shopList.getString(Shop.ID)));
					shopsModel.addShop(link , new HTMLFragment(shopList.getString(Shop.DESCRIPTION)));
					i++;
				}
			}
		}  catch(Exception exc)  {
			logError("Unable to read the list of Shops " , exc);
		}
		
	}
	
	private class ResultSetIterator implements Iterator {

		private ResultSet rs;

		public ResultSetIterator(ResultSet rs) {
			this.rs = rs;
		}

		public boolean hasNext() {
			try {
				return rs.next();
			}
			catch (Exception e) {
				return false;
			}
		}

		public Object next() {
			return (Object) rs;
		}

		public void remove() {
		}
	}

	
	public String getInitParameter(String parameter) {
		ServletContext ctx = getServlet().getServletConfig().getServletContext();
		return ctx.getInitParameter(parameter);
	}
	
	public boolean loadCatalog(String shopId)  {
		CatalogHelper helper = (CatalogHelper)getHelper(HelpManager.CATALOGHELPER);
		if(helper.loadCatalog(shopId ,
			(BaseAction) action , request , (InitializationEnvironment)servlet))
			return true;
		return false;
	}
	
}
