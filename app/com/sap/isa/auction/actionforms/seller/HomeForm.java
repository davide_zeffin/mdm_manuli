/*
 * Created on May 4, 2004
 *
 * To change the template for this generated file go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
package com.sap.isa.auction.actionforms.seller;

import java.sql.Timestamp;
import java.util.Calendar;
import java.util.Iterator;

import com.sap.isa.auction.actionforms.BaseForm;
import com.sap.isa.auction.actions.seller.linkEH.HomeLinkEH;
import com.sap.isa.auction.helpers.ConversionHelper;
import com.sap.isa.auction.helpers.HelpManager;
import com.sap.isa.auction.helpers.HomeHelper;
import com.sap.isa.auction.helpers.SearchResult;
import com.sapportals.htmlb.ItemList;
import com.sapportals.htmlb.Link;
import com.sapportals.htmlb.TextView;
import com.sapportals.htmlb.enum.TextViewDesign;

/**
 * @author I803067
 *
 * To change the template for this generated type comment go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
public class HomeForm extends BaseForm implements HomeFormConstants {

	private ItemList publishItemList;
	private ItemList alertItemList;
	private ItemList finalizedItemList;
	private TextView dateTxt;
	private boolean initialized = false;
	private static int zero = 0;

	public HomeForm() {
	}

	public void initializedFields() {
		publishItemList =
			(ItemList) addControl(HTMLB_ITEMLIST, CURPUBSTATELIST);
		alertItemList = (ItemList) addControl(HTMLB_ITEMLIST, ALERTLIST);
		dateTxt = (TextView) addControl(HTMLB_TEXTVIEW, TODAYFIELD);
		finalizedItemList =
			(ItemList) addControl(HTMLB_ITEMLIST, FINALIZEDLIST);

		Iterator iterate = publishItemList.iterator();
		while (iterate.hasNext()) {
			iterate.next();
			iterate.remove();
		}

		iterate = alertItemList.iterator();
		while (iterate.hasNext()) {
			iterate.next();
			iterate.remove();
		}

		iterate = finalizedItemList.iterator();
		while (iterate.hasNext()) {
			iterate.next();
			iterate.remove();
		}
	}

	public void setInformation() {
		initializedFields();
		boolean advancedSearch = false;

		HomeHelper homeHelper = (HomeHelper) getHelper(HelpManager.HOMEHELPER);
		homeHelper.intermediateCleanup();
		homeHelper.cleanDBConnections();
		SearchResult result = null;

		Calendar startoftoday = Calendar.getInstance();
		startoftoday.set(Calendar.HOUR_OF_DAY, 0);
		startoftoday.set(Calendar.MINUTE, 0);
		startoftoday.set(Calendar.MILLISECOND, 0);
		Timestamp today = new Timestamp(startoftoday.getTime().getTime());
		setToday(today);
		result = homeHelper.getSearchResult(HomeHelper.PUBLISHEDAUCTIONS);
		StringBuffer textBuffer = new StringBuffer(translate(TOTALACTIVETITLE));
		textBuffer.append(" ");
		int size = zero;
		if (result != null && result.getSize() > 0) {
			size = result.getSize();
			result.close();
		}
		textBuffer.append(size);
		HomeLinkEH eventHandler = new HomeLinkEH();
		Link link = null;
		TextView view = createTextView(textBuffer.toString(), false, false);
		if (size > zero) {
			link = new Link(TOTALACTIVELINK);
			link.setId(TOTALACTIVELINK);
			link.setOnClick(TOTALACTIVELINK);
			link.setOnClientClick("_auction_submitRequest()");
			link.addComponent(view);
			view.setDesign(TextViewDesign.EMPHASIZED);
			publishItemList.addComponent(link);
			action.registerEventHandler(TOTALACTIVELINK, eventHandler);
		} else {
			publishItemList.addComponent(view);
		}

		//Published today
		result = homeHelper.getSearchResult(HomeHelper.TODAYPUBLISHEDATS);
		textBuffer = new StringBuffer(translate(TDYPUBAUCTIONSTITLE));
		textBuffer.append(" ");
		size = zero;
		if (result != null && result.getSize() > 0) {
			size = result.getSize();
			result.close();
		}
		textBuffer.append(size);
		view = createTextView(textBuffer.toString(), false, false);
		if (size > zero) {
			link = new Link(TDYPUBAUCTIONSLINK);
			link.setId(TDYPUBAUCTIONSLINK);
			link.setOnClick(TDYPUBAUCTIONSLINK);
			link.setOnClientClick("_auction_submitRequest()");
			link.addComponent(view);
			view.setDesign(TextViewDesign.EMPHASIZED);
			publishItemList.addComponent(link);
			action.registerEventHandler(TDYPUBAUCTIONSLINK, eventHandler);
		} else {
			publishItemList.addComponent(view);
		}

		//Schedule to publish today
		result = homeHelper.getSearchResult(HomeHelper.SCHTOPUBLISHTODAY);
		textBuffer = new StringBuffer(translate(SCHTOPUBTITLE));
		textBuffer.append(" ");
		size = zero;
		if (result != null && result.getSize() > 0) {
			size = result.getSize();
			result.close();
		}
		textBuffer.append(size);
		view = createTextView(textBuffer.toString(), false, false);
		if (size > zero) {
			link = new Link(SCHTOPUBLINK);
			link.setId(SCHTOPUBLINK);
			link.setOnClick(SCHTOPUBLINK);
			link.setOnClientClick("_auction_submitRequest()");
			view.setDesign(TextViewDesign.EMPHASIZED);
			link.addComponent(view);
			publishItemList.addComponent(link);
			action.registerEventHandler(SCHTOPUBLINK, eventHandler);
		} else {
			publishItemList.addComponent(view);
		}

		//alerts schedule failed
		result = homeHelper.getSearchResult(HomeHelper.SCHEDULEFAILED);
		textBuffer = new StringBuffer(translate(SCHFAILEDTITLE));
		textBuffer.append(" ");
		size = zero;
		if (result != null && result.getSize() > 0) {
			size = result.getSize();
			result.close();
		}
		textBuffer.append(size);
		view = createTextView(textBuffer.toString(), false, false);
		if (size > zero) {
			link = new Link(SCHFAILEDLINK);
			link.setId(SCHFAILEDLINK);
			link.setId(SCHFAILEDLINK);
			link.setOnClick(SCHFAILEDLINK);
			link.setOnClientClick("_auction_submitRequest()");
			view.setDesign(TextViewDesign.EMPHASIZED);
			link.addComponent(view);
			action.registerEventHandler(SCHFAILEDLINK, eventHandler);
			alertItemList.addComponent(link);
		} else {
			alertItemList.addComponent(view);
		}

		//Auction closed without winners
		result = homeHelper.getSearchResult(HomeHelper.CLOSEDNOWINNERS);
		textBuffer = new StringBuffer(translate(CLOSEDNOWINNERSTITLE));
		textBuffer.append(" ");
		size = zero;
		if (result != null && result.getSize() > 0) {
			size = result.getSize();
			result.close();
		}
		textBuffer.append(size);
		view = createTextView(textBuffer.toString(), false, false);
		if (size > zero) {
			link = new Link(CLOSEDNOWINNERSLINK);
			link.setId(CLOSEDNOWINNERSLINK);
			link.setOnClick(CLOSEDNOWINNERSLINK);
			link.setOnClientClick("_auction_submitRequest()");
			link.addComponent(view);
			alertItemList.addComponent(link);
			view.setDesign(TextViewDesign.EMPHASIZED);
			action.registerEventHandler(CLOSEDNOWINNERSLINK, eventHandler);
		} else {
			alertItemList.addComponent(view);
		}

		//Finalized not checkout
		result = homeHelper.getSearchResult(HomeHelper.FINALIZEDNOTCHKED);
		textBuffer = new StringBuffer(translate(FINALEDNOTCHKDTITLE));

		textBuffer.append(" ");
		size = zero;
		if (result != null && result.getSize() > 0) {
			size = result.getSize();
			result.close();
		}
		textBuffer.append(size);
		view = createTextView(textBuffer.toString(), false, false);
		if (size > zero) {
			link = new Link(FINALEDNOTCHKDLINK);
			link.setId(FINALEDNOTCHKDLINK);
			link.setOnClick(FINALEDNOTCHKDLINK);
			link.setOnClientClick("_auction_submitRequest()");
			link.addComponent(view);
			view.setDesign(TextViewDesign.EMPHASIZED);
			finalizedItemList.addComponent(link);
			action.registerEventHandler(FINALEDNOTCHKDLINK, eventHandler);
		} else {
			finalizedItemList.addComponent(view);
		}

		//Finalized auctions with checkout
		result = homeHelper.getSearchResult(HomeHelper.FINALIZEDCHKDOUT);
		textBuffer = new StringBuffer(translate(FINALIZEDCHKDTITLE));

		textBuffer.append(" ");
		size = zero;
		if (result != null && result.getSize() > 0) {
			size = result.getSize();
			result.close();
		}
		textBuffer.append(size);
		view = createTextView(textBuffer.toString(), false, false);
		if (size > zero) {
			link = new Link(FINALIZEDCHKDLINK);
			link.setId(FINALIZEDCHKDLINK);
			link.setOnClick(FINALIZEDCHKDLINK);
			link.setOnClientClick("_auction_submitRequest()");
			link.addComponent(view);
			view.setDesign(TextViewDesign.EMPHASIZED);
			finalizedItemList.addComponent(link);
			action.registerEventHandler(FINALIZEDCHKDLINK, eventHandler);
		} else {
			finalizedItemList.addComponent(view);
		}
	}

	private void setToday(Timestamp date) {
		String text = "";
		dateTxt.setText("");
		try {
			text =
				(
					(ConversionHelper) getHelper(
						HelpManager
							.CONVERSIONHELPER))
							.javaDatetoUILocaleSpecificString(
					date,
					true);
			dateTxt.setText(text);
		} catch (Exception exc) {
			logInfo("Unable to set the Date information for the user ", exc);
		}
	}

	public void setPublishedAuctions() {
		HomeHelper homeHelper = (HomeHelper) getHelper(HelpManager.HOMEHELPER);
		homeHelper.setPublishedAuctions();
	}

	public void setTodayPublishedAuctions() {
		HomeHelper homeHelper = (HomeHelper) getHelper(HelpManager.HOMEHELPER);
		homeHelper.setTodayPublishedAuctions();
	}

	public void setScheduleToPublishToday() {
		HomeHelper homeHelper = (HomeHelper) getHelper(HelpManager.HOMEHELPER);
		homeHelper.setScheduleToPublishToday();
	}

	public void setScheduleFailures() {
		HomeHelper homeHelper = (HomeHelper) getHelper(HelpManager.HOMEHELPER);
		homeHelper.setScheduleFailures();
	}

	public void setClosedWithoutWinners() {
		HomeHelper homeHelper = (HomeHelper) getHelper(HelpManager.HOMEHELPER);
		homeHelper.setClosedWithoutWinners();
	}

	public void setFinalizedWithoutCheckout() {
		HomeHelper homeHelper = (HomeHelper) getHelper(HelpManager.HOMEHELPER);
		homeHelper.setFinalizedWithoutCheckout();
	}

	public void setFinalizedCheckout() {
		HomeHelper homeHelper = (HomeHelper) getHelper(HelpManager.HOMEHELPER);
		homeHelper.setFinalizedCheckout();
	}

	//	public void refresh() {
	//		HomeHelper homeHelper = (HomeHelper)getHelper(HelpManager.HOMEHELPER);
	//		homeHelper.refresh();
	//		setInformation();
	//	}
	//    
}
