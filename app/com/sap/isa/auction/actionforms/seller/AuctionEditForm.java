/*
 * Created on Apr 30, 2004
 *
 * To change the template for this generated file go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
package com.sap.isa.auction.actionforms.seller;

import java.math.BigDecimal;
import java.util.Collection;
import java.util.Enumeration;
import java.util.Iterator;

import com.sap.isa.auction.actionforms.BaseForm;
import com.sap.isa.auction.actions.seller.linkEH.ChangeActiveAuctionLinkEH;
import com.sap.isa.auction.bean.AuctionItem;
import com.sap.isa.auction.bean.AuctionTypeEnum;
import com.sap.isa.auction.bean.b2x.AuctionVisibilityEnum;
import com.sap.isa.auction.bean.b2x.BidTypeEnum;
import com.sap.isa.auction.bean.b2x.PrivateAuction;
import com.sap.isa.auction.bean.b2x.WinnerDeterminationTypeEnum;
import com.sap.isa.auction.helpers.AuctionContainer;
import com.sap.isa.auction.helpers.AuctionEditHelper;
import com.sap.isa.auction.helpers.ConversionHelper;
import com.sap.isa.auction.helpers.HelpManager;
import com.sap.isa.auction.helpers.TargetGroupBPHelper;
import com.sap.isa.auction.models.tablemodels.seller.BusinessPartnerTGModel;
import com.sap.isa.auction.models.tablemodels.seller.ProductsTableModel;
import com.sap.isa.core.util.JspUtil;
import com.sap.isa.core.util.WebUtil;
import com.sapportals.htmlb.Checkbox;
import com.sapportals.htmlb.DropdownListBox;
import com.sapportals.htmlb.GridLayout;
import com.sapportals.htmlb.GridLayoutCell;
import com.sapportals.htmlb.Image;
import com.sapportals.htmlb.InputField;
import com.sapportals.htmlb.ItemList;
import com.sapportals.htmlb.Link;
import com.sapportals.htmlb.TextEdit;
import com.sapportals.htmlb.TextView;
import com.sapportals.htmlb.table.TableView;

/**
 * @author I803067
 *
 * To change the template for this generated type comment go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
public class AuctionEditForm extends BaseForm  implements AuctionDetailConstants  {

	private InputField nameEdit;
	private InputField titleEdit;
	private InputField startPriceEdit;
	private DropdownListBox pripubEdit;
	private DropdownListBox fullBrokEdit;
	private DropdownListBox bidTypeEdit;
	private InputField resvPriceEdit;
	private InputField buyNowEdit;
	private InputField durationEdit;
	private ItemList auctionListEdit;
	private InputField bidIncrEdit;
	private TextEdit termsEdit;
	private InputField imageURLEdit;
	private InputField quantityEdit;
	private Checkbox winnerTypeChkbox;

	private int tabIndex = 3;
	
	private ProductsTableModel productsModel;
	private TableView productsView;
	
	private BusinessPartnerTGModel businessPartnerTGModel;
	private TableView businessPartnerTGView;
	
	private TextView nameTxt;
	private TextView titleTxt;
	private TextView startPriceTxt;
	private TextView pripubTxt;
	private TextView fullBrokTxt;
	private TextView bidTypeTxt;
	private TextView resvPriceTxt;
	private TextView buyNowTxt;
	private TextView durationTxt;
	private TextView bidIncrTxt;
	private Image imageURLTxt;
	private TextView termsTxt;
	private TextView quantityTxt;
	private TextView winnerTypeTxt;

	private ItemList auctionsItems;
	
	private boolean initializedFields = false;
	private boolean initializedProductDetails = false;
	private boolean initializedSummaryDetails = false;
	private boolean initializedTargetsDetails = false; 
	
	private void initializeGeneralDetailsFields()  {
			
		nameEdit = (InputField)addControl(HTMLB_INPUTFIELD , NAMEINPUT);
		titleEdit = (InputField)addControl(HTMLB_INPUTFIELD , TITLEINPUT);
		titleEdit.setMaxlength(50);
		startPriceEdit = (InputField)addControl(HTMLB_INPUTFIELD , STARTPRICEINPUT);
		imageURLEdit = (InputField)addControl(HTMLB_INPUTFIELD , IMAGEURLINPUT);
		pripubEdit = (DropdownListBox)addControl(HTMLB_DROPDOWN , PUBPRIINPUT);
        pripubEdit.addItem(PUBPRITEXT1 , translate(PUBPRITEXT1));
        pripubEdit.addItem(PUBPRITEXT2, translate(PUBPRITEXT2));
		setSelectedAuctionVisibility();                
		bidTypeEdit = (DropdownListBox)addControl(HTMLB_DROPDOWN , BIDTYPINPUT);
        bidTypeEdit.addItem(BIDTYPETEXT1 , translate(BIDTYPETEXT1));
        bidTypeEdit.addItem(BIDTYPETEXT2 , translate(BIDTYPETEXT2));
		fullBrokEdit = (DropdownListBox)addControl(HTMLB_DROPDOWN , FLBLINPUT);
		fullBrokEdit.addItem(FLBLTEXT1 , translate(FLBLTEXT1));
		fullBrokEdit.addItem(FLBLTEXT2 , translate(FLBLTEXT2));
		resvPriceEdit = (InputField)addControl(HTMLB_INPUTFIELD , RESVPRICEINPUT);
		durationEdit = (InputField)addControl(HTMLB_INPUTFIELD , DURATIONINPUT);
		buyNowEdit = (InputField)addControl(HTMLB_INPUTFIELD , BUYNOWINPUT);
		bidIncrEdit = (InputField)addControl(HTMLB_INPUTFIELD , BIDINCRINPUT);
		termsEdit = (TextEdit)addControl(HTMLB_TEXTEDIT , TERMSINPUT);
		auctionsItems = (ItemList)addControl(HTMLB_ITEMLIST , AUCTIONSITEMSLIST);
		quantityEdit = (InputField)addControl(HTMLB_INPUTFIELD , QTYINPUT);
		winnerTypeChkbox = (Checkbox)addControl(HTMLB_CHECKBOX, WINNERTYPECHKBOX);
		initializedFields = true;
	}

	private void setSelectedAuctionVisibility() {
        // In case of BP or TG is assigned the auction should be Private
		AuctionEditHelper editHelper = (AuctionEditHelper)getHelper(HelpManager.AUCTIONEDITHELPER);
		PrivateAuction auction = editHelper.getAuction();
		if ((auction.getBusinessPartners() != null && auction.getBusinessPartners().size() == 0) &&
		     (auction.getTargetGroups()!= null && auction.getTargetGroups().size() == 0)) {
                 pripubEdit.setSelection(PUBPRITEXT1);
                 pripubEdit.setEnabled(true);
                 auction.setVisibilityType(AuctionVisibilityEnum.PUBLIC.getValue());
		}
		else {
		         pripubEdit.setSelection(PUBPRITEXT2);
		         pripubEdit.setEnabled(false);
		}
	}
	
	private void initializeProductDetails()  {
		
		productsModel = new ProductsTableModel(session , request.getContextPath());
		productsModel.setActionServlet(servlet);
		this.setAttribute(PRODUCTSTABLEMODEL , productsModel);
		productsView = (TableView)addControl(HTMLB_TABLEVIEW , PRODUCTSTABLEVIEW);
		initializedProductDetails = true;
	}
	
	private void initializeTargetsDetails()  {
		
		businessPartnerTGModel = new BusinessPartnerTGModel(servlet , session);
		businessPartnerTGModel.setContextPath(request.getContextPath());
		this.setAttribute(TARGETSTABLEMODEL , businessPartnerTGModel);
		businessPartnerTGView = (TableView)addControl(HTMLB_TABLEVIEW , TARGETSTABLEVIEW);
		initializedTargetsDetails = true;
	}
	
	private void initializeSummaryDetails()  {
		initializeProductDetails();
		initializeTargetsDetails();
		nameTxt = (TextView)addControl(HTMLB_TEXTVIEW , NAMEVIEW);
		titleTxt = (TextView)addControl(HTMLB_TEXTVIEW , TITLEVIEW);
		startPriceTxt = (TextView)addControl(HTMLB_TEXTVIEW , STARTPRICEVIEW);
		pripubTxt = (TextView)addControl(HTMLB_TEXTVIEW , PUBPRIVIEW);
		bidTypeTxt = (TextView)addControl(HTMLB_TEXTVIEW , BIDTYPVIEW);
		fullBrokTxt = (TextView)addControl(HTMLB_TEXTVIEW , FLBLVIEW);
		resvPriceTxt = (TextView)addControl(HTMLB_TEXTVIEW , RESVPRICEVIEW);
		durationTxt = (TextView)addControl(HTMLB_TEXTVIEW , DURATIONVIEW);
		buyNowTxt = (TextView)addControl(HTMLB_TEXTVIEW , BUYNOWVIEW);
		bidIncrTxt = (TextView)addControl(HTMLB_TEXTVIEW , BIDINCRVIEW);
		termsTxt = (TextView)addControl(HTMLB_TEXTVIEW , TERMSVIEW);
		imageURLTxt = new Image(IMAGEURLVIEW , "");
		quantityTxt = (TextView)addControl(HTMLB_TEXTVIEW , QTYVIEW);
		winnerTypeTxt = (TextView)addControl(HTMLB_TEXTVIEW , WINNERTYPEVIEW);
		this.setAttribute(IMAGEURLVIEW , imageURLTxt);
		initializedSummaryDetails = true;
	}
	
	
	public void clean()  {
		if(productsModel != null)  {
			productsModel.clean();
		}
		if(businessPartnerTGModel != null)  {
			businessPartnerTGModel.clean();
		}
		productsModel = null;
		businessPartnerTGModel = null;
		removeAttributes();
		nameEdit = null;
		titleEdit = null;
		startPriceEdit = null;
		pripubEdit = null;
		bidTypeEdit = null;
		fullBrokEdit = null;
		resvPriceEdit = null;
		durationEdit = null;
		buyNowEdit = null;
		imageURLEdit = null;
		quantityEdit = null;
		winnerTypeChkbox = null;
	}
	
	
	public void startSingleCreation()  {
		AuctionEditHelper editHelper = (AuctionEditHelper)getHelper(HelpManager.AUCTIONEDITHELPER);
		editHelper.initializeSingleCreation();
		setInformation(GENERALDETAILSTABINDEX);
	}
	
	public void startMultipleCreation()  {
		AuctionEditHelper editHelper = (AuctionEditHelper)getHelper(HelpManager.AUCTIONEDITHELPER);
		editHelper.initializeMultipleCreation();
		setInformation(GENERALDETAILSTABINDEX);
	}
	
	public String getCreateEditTitle()  {
		AuctionEditHelper auctionEditHelper = (AuctionEditHelper)
								getHelper(HelpManager.AUCTIONEDITHELPER);
		if(AuctionEditHelper.CREATE == auctionEditHelper.getCreateMode())  {
			return translate(CREATAUCTIONTEXT);
		}  else  {
			return translate(EDITAUCTIONTEXT);
		}
	}
	
	public void setGeneralDetailsEdit()  {
		initializeGeneralDetailsFields();
		clearGeneralInfoFields();
		AuctionEditHelper editHelper = (AuctionEditHelper)getHelper(HelpManager.AUCTIONEDITHELPER);
		setTabIndex(GENERALDETAILSTABINDEX);
		PrivateAuction auction = editHelper.getAuction();
		nameEdit.setString(auction.getAuctionName());
		titleEdit.setString(auction.getAuctionTitle());
		
        if ((auction.getBusinessPartners() != null && auction.getBusinessPartners().size() == 0) &&
                     (auction.getTargetGroups()!= null && auction.getTargetGroups().size() == 0)) {
    		if(auction.getVisibilityType() != null 
    				&& auction.getVisibilityType().getValue() == AuctionVisibilityEnum.PROTECTED.getValue())  {
    			pripubEdit.setSelection(PUBPRITEXT2);
    		}  else  {
    			pripubEdit.setSelection(PUBPRITEXT1);
    		}
        }
		if(auction.getType() == AuctionTypeEnum.NORMAL.getValue())  {
			fullBrokEdit.setSelection(FLBLTEXT1);	
		}  else  {
			fullBrokEdit.setSelection(FLBLTEXT2);
		}
		fullBrokEdit.setJsObjectNeeded(true);
		if(auction.getBidType() != null)  {
			if(auction.getBidType().getValue() == BidTypeEnum.NORMAL.getValue())  {
				bidTypeEdit.setSelection(BIDTYPETEXT1);
			}  else  {
				bidTypeEdit.setSelection(BIDTYPETEXT2);
			}
		}  else  {
			bidTypeEdit.setSelection(BIDTYPETEXT1);
		}
		if (auction.getWinnerTypeEnum() != null){
			if (auction.getWinnerTypeEnum().getValue() == WinnerDeterminationTypeEnum.INT_AUTOMATIC)
				winnerTypeChkbox.setChecked(false);
			else
				winnerTypeChkbox.setChecked(true);
		} else {
			winnerTypeChkbox.setChecked(false);
		}
		ConversionHelper conversionHelper = (ConversionHelper)getHelper(HelpManager.CONVERSIONHELPER);
		
		startPriceEdit.setString(conversionHelper.bigDecimalToUICurrencyString(auction.getStartPrice() , true));
		resvPriceEdit.setString(conversionHelper.bigDecimalToUICurrencyString(auction.getReservePrice() , true));
		imageURLEdit.setString(auction.getImageURL());
		buyNowEdit.setString(conversionHelper.bigDecimalToUICurrencyString(auction.getBuyItNowPrice() , true));
		bidIncrEdit.setString(conversionHelper.bigDecimalToUICurrencyString(auction.getBidIncrement() , true));
		termsEdit.setText(auction.getAuctionTerms());
		if(auction.getAuctionDuration() != null)
			durationEdit.setString(Integer.toString(auction.getAuctionDuration()[0]));
		if (auction.getQuantity()>0)
			quantityEdit.setString(conversionHelper.bigDecimalToUIQuantityString(new BigDecimal(auction.getQuantity())));
	}
	private void clearGeneralInfoFields(){
		nameEdit.setValue("");
		titleEdit.setValue("");
		startPriceEdit.setValue("");
        setSelectedAuctionVisibility();
		bidTypeEdit.setSelection(BIDTYPETEXT1);
		fullBrokEdit.setSelection(FLBLTEXT1);
		resvPriceEdit.setValue("");
		durationEdit.setValue("");
		buyNowEdit.setValue("");
		imageURLEdit.setValue("");
		quantityEdit.setValue("");
		termsEdit.setText("");
		winnerTypeChkbox.setChecked(false);
	}
	public void updateGeneralDetails()  {
		AuctionEditHelper editHelper = (AuctionEditHelper)getHelper(HelpManager.AUCTIONEDITHELPER);
		PrivateAuction auction = editHelper.getAuction();
		setTabIndex(GENERALDETAILSTABINDEX);
		auction.setAuctionName(getValue(NAMEINPUT));
		String title = getValue(TITLEINPUT).trim();
		if (title.length() > 50)
			title = title.substring(0, 30);
		auction.setAuctionTitle(title);
		try  {
			auction.setAuctionDuration(Integer.parseInt(getValue(DURATIONINPUT)) , 0 , 0);
		}  catch(Exception exc) {
			logWarning("Unable to the set the duration" , exc);
			auction.setAuctionDuration(0 , 0 , 0);
		}
		String terms = JspUtil.encodeHtml(getValue(TERMSINPUT));
		if (terms.length() >100)
			terms = terms.substring(0, 100);
		auction.setAuctionTerms(terms);
		String type = getValue(BIDTYPINPUT);
		if(type != null)  {
			if(type.equals(BIDTYPETEXT1))  {
				auction.setBidType(BidTypeEnum.NORMAL);
			}  else  if(type.equals(BIDTYPETEXT2))  {
				auction.setBidType(BidTypeEnum.SEALED);
			}
		}  else {
			auction.setBidType(BidTypeEnum.NORMAL);
		}
		ConversionHelper conversionHelper = (ConversionHelper)getHelper(HelpManager.CONVERSIONHELPER);
		String reservePrice = getValue(RESVPRICEINPUT);
		if (reservePrice.trim().length()>0)
			auction.setReservePrice(conversionHelper.uiCurrencyStringToBigDecimal(getValue(RESVPRICEINPUT)));
		else
			auction.setReservePrice(null);
		auction.setStartPrice(conversionHelper.uiCurrencyStringToBigDecimal(getValue(STARTPRICEINPUT)));
		auction.setBuyItNowPrice(conversionHelper.uiCurrencyStringToBigDecimal(getValue(BUYNOWINPUT)));
		auction.setBidIncrement(conversionHelper.uiCurrencyStringToBigDecimal(getValue(BIDINCRINPUT)));
			
		type = getValue(FLBLINPUT);
		if(type != null && type.equals(FLBLTEXT2))  {
			auction.setType(AuctionTypeEnum.BROKEN_LOT.getValue());
		}  else  {
			auction.setType(AuctionTypeEnum.NORMAL.getValue());
		}
		String auctionType = getValue(PUBPRIINPUT);
		if(auctionType != null)  {
			if(auctionType.equals(PUBPRITEXT1))  {
				auction.setVisibilityType(AuctionVisibilityEnum.PUBLIC.getValue());
			}  else {
				auction.setVisibilityType(AuctionVisibilityEnum.PROTECTED.getValue());
			}
		}  else  {
            if ((auction.getBusinessPartners() != null && auction.getBusinessPartners().size() == 0) &&
                 (auction.getTargetGroups()!= null && auction.getTargetGroups().size() == 0)) {
			          auction.setVisibilityType(AuctionVisibilityEnum.PUBLIC.getValue());
            }
            else {
                      auction.setVisibilityType(AuctionVisibilityEnum.PROTECTED.getValue());
            }
		}
		
		auction.setImageURL(getValue(IMAGEURLINPUT));
		auction.setQuantity(1);
		try  {
			if(auction.getType() == AuctionTypeEnum.BROKEN_LOT.getValue())
				auction.setQuantity(conversionHelper.uiQuantityStringToBigDecimal(getValue(QTYINPUT)).intValue());
		}  catch(Exception exc)  {
			logWarning("Unable to set the quantity information" , exc);
		}
		if (isSelected(WINNERTYPECHKBOX)) 
			auction.setWinnerTypeEnum(WinnerDeterminationTypeEnum.MANUAL);
		else
			auction.setWinnerTypeEnum(WinnerDeterminationTypeEnum.AUTOMATIC);
	}
	
	protected void setTabIndex(int index)  {
		this.tabIndex = index;
	}
	
	public int getTabIndex()  {
		return tabIndex;
	}
	
	public void updateInformation()  {
		switch(getTabIndex())  {
			case PRODUCTTABINDEX:
				
				break;
			case TARGETGROUPTABINDEX:
				break;
			
			case GENERALDETAILSTABINDEX:
				updateGeneralDetails();
				break;
			case SUMMARYTABINDEX:
				
				break;
		}
	}
	
	public void setInformation(int tabIndex)  {
		setTabIndex(tabIndex);
		
		switch(getTabIndex())  {
			case PRODUCTTABINDEX:
				setAuctionsList(false);
				setProductDetailsEdit();
				break;
			case TARGETGROUPTABINDEX:
				setAuctionsList(false);
				setTargetDetailsEdit();
				break;
			
			case GENERALDETAILSTABINDEX:
				setAuctionsList(false);
				setGeneralDetailsEdit();
				break;
			case SUMMARYTABINDEX:
				setAuctionsList(true);
				setSummary();
				break;
		}
	}
	
	public boolean isBrokenLot()  {
		AuctionEditHelper editHelper = (AuctionEditHelper)getHelper(HelpManager.AUCTIONEDITHELPER);
		PrivateAuction auction = editHelper.getAuction();
		if(auction != null)  {
			if(auction.getType() == AuctionTypeEnum.BROKEN_LOT.getValue())  {
				return true;
			}
		}
		return false;
	}
	
	protected void setAuctionsList(boolean showState)  {
		auctionsItems = (ItemList)addControl(HTMLB_ITEMLIST , AUCTIONSITEMSLIST);
		Iterator iterator = auctionsItems.iterator();
		while(iterator.hasNext())  {
			iterator.next();
			iterator.remove();
		}
		AuctionEditHelper helper = (AuctionEditHelper)getHelper(HelpManager.AUCTIONEDITHELPER);
		Collection items = helper.getAuctions();
		Iterator iterate = items.iterator();
		if(items.size() > 1)  {
			try  {
				TextView view = null;
				Link link = null;
				AuctionContainer auctContainer = null;
				int i = 0;
				String row = null;
				String id = null;
				ChangeActiveAuctionLinkEH eventEH = new ChangeActiveAuctionLinkEH();
				int currentActiveRow = helper.getCurrentPos();
				GridLayout grid = null;
				GridLayoutCell cell = null;
				Checkbox checkBox = null;
				while(iterate.hasNext())  {
					auctContainer = (AuctionContainer)iterate.next();
					row = Integer.toString(i);
					id = auctContainer.getAuction().getAuctionName() + row;
					grid = new GridLayout();
					grid.setCellSpacing(1);
					checkBox = (Checkbox)addControl(HTMLB_CHECKBOX , id + "C");
					checkBox.setId(id + "C");
					cell = new GridLayoutCell(checkBox);
					grid.addCell(1 , 1 , cell);
					if(i == currentActiveRow)  {
						view = createTextView(auctContainer.getAuction().getAuctionName() , false , true);
						i++;
						cell = new GridLayoutCell(view);
						grid.addCell(1 , 2 , cell);
						StringBuffer buffer = new StringBuffer();
						/*buffer.append(FormUtility.getResourceString(FormUtility.AUCTIONSELLBUNDLE , AUCTIONDETAILSTEXT , session));
						buffer.append("  ");
						buffer.append(auctContainer.getAuction().getAuctionName());
						detailsTxt.setText(buffer.toString());*/
						checkBox.setChecked(true);
						/*if(currentItemChecked)
							checkBox.setEnabled(false);
						else
							checkBox.setEnabled(true);*/
						if(auctContainer.getAuction().getUserObject() != null)  {
							StringBuffer stringBuffer = new StringBuffer();
							stringBuffer.append(view.getText());
							stringBuffer.append("(");
							stringBuffer.append(translate(ERRORSTEXT));
							stringBuffer.append(")");
							view.setText(stringBuffer.toString());
						}
					}
					else  {
						view = createTextView(auctContainer.getAuction().getAuctionName() , false , false);
						link = (Link)addControl(HTMLB_LINK , id + "L");
						link.setTooltip(view.getText());
						link.setId(id + "L");
						link.setOnClick(row);
						Iterator iterator2 = link.iterator();
						while(iterator2.hasNext())  {
							iterator2.next();
							iterator2.remove();
						}
						link.addComponent(view);
						link.setOnClientClick("_auction_submitRequest()");
						action.registerEventHandler(link.getId() , eventEH);
						i++;
						cell = new GridLayoutCell(link);
						grid.addCell(1 , 2 , cell);
						if(auctContainer.getAuction().getUserObject() != null)  {
							StringBuffer stringBuffer = new StringBuffer();
							stringBuffer.append(view.getText());
							stringBuffer.append("(");
							stringBuffer.append(translate(ERRORSTEXT));
							stringBuffer.append(")");
							view.setText(stringBuffer.toString());
						}
					}
					auctionsItems.addComponent(grid);
				}
			
			}  catch(Exception exc)  {
				logInfo("Unable to create the auctions list" ,exc);
			}
			 
		}
	}
	
	protected void setProductDetailsEdit()  {
		setTabIndex(PRODUCTTABINDEX);
		initializeProductDetails();
		AuctionEditHelper editHelper = (AuctionEditHelper)getHelper(HelpManager.AUCTIONEDITHELPER);
		productsModel.setData(editHelper.getAuction().getItems());
	}
	
	protected void setTargetDetailsEdit()  {
		setTabIndex(TARGETGROUPTABINDEX);
		initializeTargetsDetails();
		AuctionEditHelper editHelper = (AuctionEditHelper)getHelper(HelpManager.AUCTIONEDITHELPER);
		TargetGroupBPHelper targetHelper = (TargetGroupBPHelper)getHelper(HelpManager.TARGETGROUPHELPER);
		PrivateAuction auction = editHelper.getAuction();
		Collection items = targetHelper.getTargetGroupBPCollection(auction.getTargetGroups() , auction.getBusinessPartners());
		businessPartnerTGModel.setData(items , "" , "" , false);
	}
	
	public String getCurrency() {
		AuctionEditHelper editHelper = (AuctionEditHelper)
								getHelper(HelpManager.AUCTIONEDITHELPER);
		return editHelper.getCurrency();
	}
	
	protected void setSummary()  {
		setTabIndex(SUMMARYTABINDEX);
		initializeSummaryDetails();
		AuctionEditHelper editHelper = (AuctionEditHelper)getHelper(HelpManager.AUCTIONEDITHELPER);
		TargetGroupBPHelper targetHelper = (TargetGroupBPHelper)getHelper(HelpManager.TARGETGROUPHELPER);
		PrivateAuction auction = editHelper.getAuction();
		
		ConversionHelper conversionHelper = (ConversionHelper)getHelper(HelpManager.CONVERSIONHELPER);
		
		nameTxt.setText(auction.getAuctionName());
		titleTxt.setText(auction.getAuctionTitle());
		startPriceTxt.setText(conversionHelper.bigDecimalToUICurrencyString(auction.getStartPrice() , false));
		if(auction.getVisibilityType() != null)  {
			if(auction.getVisibilityType().getValue() == AuctionVisibilityEnum.PUBLIC.getValue())  {
				pripubTxt.setText(translate(PUBPRITEXT1));
			}  else  {
				pripubTxt.setText(translate(PUBPRITEXT2));
			}
		}  else  {
			pripubTxt.setText("");	
		}
		if(auction.getBidType().getValue() == BidTypeEnum.NORMAL.getValue())  {
			bidTypeTxt.setText(translate(BIDTYPETEXT1));
		}  else  {
			bidTypeTxt.setText(translate(BIDTYPETEXT2));
		}
		if(AuctionTypeEnum.NORMAL.getValue() == auction.getType())  {
			fullBrokTxt.setText(translate(FLBLTEXT1));
		}  else  {
			fullBrokTxt.setText(translate(FLBLTEXT2));
		}
		if(WinnerDeterminationTypeEnum.INT_AUTOMATIC == auction.getWinnerTypeEnum().getValue())  {
			winnerTypeTxt.setText(translate(AUTOWINNERLABEL));	
		} else {
			winnerTypeTxt.setText(translate(MANUALWINNERLABEL));
		}
		if(auction.getImageURL() != null && !auction.getImageURL().equals("")) {
		    if (auction.getImageURL().startsWith("http")) {
                imageURLTxt.setSrc(auction.getImageURL());
		    } else {
                imageURLTxt.setSrc("../" + auction.getImageURL());   
		    }		
        } else {
            imageURLTxt.setSrc(WebUtil.getAppsURL(servlet.getServletContext() , request , response , null , "/mimes/images/noimage.jpg" , null , null , false));
        }
			
		resvPriceTxt.setText(conversionHelper.bigDecimalToUICurrencyString(auction.getReservePrice() , false));
		durationTxt.setText(conversionHelper.bigDecimalToUIQuantityString(new BigDecimal(auction.getAuctionDuration()[0])));
		bidIncrTxt.setText(conversionHelper.bigDecimalToUICurrencyString(auction.getBidIncrement() , false));
		buyNowTxt.setText(conversionHelper.bigDecimalToUICurrencyString(auction.getBuyItNowPrice() , false));
		if(auction.getAuctionTerms() != null && !auction.getAuctionTerms().equals(""))
			termsTxt.setText(auction.getAuctionTerms());
		else
			termsTxt.setText(translate(NOTERMSTEXT));
		if(auction.getUserObject() != null)  {
			setErrors(auction.getUserObject() , translate(SAVEERRORSTEXT), AUCTIONSELLBUNDLE);
		}
		quantityTxt.setText(conversionHelper.bigDecimalToUIQuantityString(new BigDecimal(auction.getQuantity())));
		setProductDetailsEdit();
		setTargetDetailsEdit();
		setTabIndex(SUMMARYTABINDEX);
	}
	
	public boolean saveAuctions()  {
		AuctionEditHelper editHelper = (AuctionEditHelper)getHelper(HelpManager.AUCTIONEDITHELPER);
		if(editHelper.saveAuctions())  {
			return true;
		}  else  {
			PrivateAuction auction = editHelper.getAuction();
			if(auction.getUserObject() != null)  {
				setErrors(auction.getUserObject() , translate(SAVEERRORSTEXT), AUCTIONSELLBUNDLE);
			}
			return false;
		}
	}
	
	public int getAuctionsCount()  {
		AuctionEditHelper auctionEditHelper = (AuctionEditHelper)getHelper(HelpManager.AUCTIONEDITHELPER);
		return auctionEditHelper.getAuctions().size();
	}
	
	public int getProductsCount()  {
		AuctionEditHelper helper = (AuctionEditHelper)getHelper(HelpManager.AUCTIONEDITHELPER);
		return helper.getAuction().getItems() != null ? helper.getAuction().getItems().size() : 0;
	}
	
	public int getTargetsCount()  {
		AuctionEditHelper helper = (AuctionEditHelper)getHelper(HelpManager.AUCTIONEDITHELPER);
		int count = 0;
		if(helper.getAuction().getBusinessPartners() != null)  {
			count = count + helper.getAuction().getBusinessPartners().size();
		}
		if(helper.getAuction().getTargetGroups() != null)  {
			count = count + helper.getAuction().getTargetGroups().size(); 
		}
		return count;
	}
	
	public void removeProducts()  {
		TableView view = (TableView)getComponentForId(PRODUCTSTABLEVIEW);
		if(view != null)  {
			productsModel.updateSelection(view);
		}
		Enumeration selection = productsModel.getSelectedRows();
		AuctionItem item = null;
		AuctionEditHelper productsHelper = (AuctionEditHelper)getHelper(HelpManager.AUCTIONEDITHELPER);
		while(selection.hasMoreElements())  {
			int row = Integer.parseInt((String)selection.nextElement());
			item = (AuctionItem)productsModel.getValueAt(row);
			productsHelper.removeItem(item.getProductId());
		}
		productsModel.clearSelection();
		setInformation(PRODUCTTABINDEX);
	}

	public void removeTargetGroups()  {
		TableView view = (TableView)getComponentForId(TARGETSTABLEVIEW);
		if(view != null)  {
			businessPartnerTGModel.updateSelection(view);
		}
		AuctionEditHelper targetGroupHelper = (AuctionEditHelper)getHelper(HelpManager.AUCTIONEDITHELPER);
		TargetGroupBPHelper.TargetGroupBusinessPartner tgBP = null;
		Enumeration selection = businessPartnerTGModel.getSelectedRows();
		while(selection.hasMoreElements())  {
			int row = Integer.parseInt((String)selection.nextElement());
			tgBP = (TargetGroupBPHelper.TargetGroupBusinessPartner)businessPartnerTGModel.getValueAt(row);
			targetGroupHelper.removeTargetGroupBusinessPartner(tgBP);
		}
		businessPartnerTGModel.clearSelection();
		setInformation(TARGETGROUPTABINDEX);
	}

	public void setCurrentAuction(String pos) {
		updateInformation();
		AuctionEditHelper editHelper = (AuctionEditHelper)getHelper(HelpManager.AUCTIONEDITHELPER);
		editHelper.setCurrentPosition(Integer.parseInt(pos));
        PrivateAuction auction = editHelper.getAuction();
        if (auction.getAuctionDuration() == null && auction.getBidType() == null && auction.getStartPrice() == null) {
            // During Multiple Auction creation; if no General data is maintained
            // the Summary tab should not be displayed for this auction
            setTabIndex(GENERALDETAILSTABINDEX);
        }
		setInformation(getTabIndex());
	}

	/**
	 * 
	 */
	public void startEditAuction() {
		AuctionEditHelper auctionEditHelper = (AuctionEditHelper)
							getHelper(HelpManager.AUCTIONEDITHELPER);
		auctionEditHelper.editAuction();
		setInformation(GENERALDETAILSTABINDEX);
	}

	public void cancelAuction() {
		AuctionEditHelper auctionEditHelper = (AuctionEditHelper)
								getHelper(HelpManager.AUCTIONEDITHELPER);
		auctionEditHelper.cancel();
		
	}

	public void copyAuction(String targetName , String titleName) {
		AuctionEditHelper auctionEditHelper = (AuctionEditHelper)
							getHelper(HelpManager.AUCTIONEDITHELPER);
		auctionEditHelper.copyAuction(targetName , titleName);
		setInformation(GENERALDETAILSTABINDEX);
	}
	
	public String getAuctionTerms(){
		AuctionEditHelper editHelper = (AuctionEditHelper)getHelper(HelpManager.AUCTIONEDITHELPER);
		PrivateAuction auction = editHelper.getAuction();
		String termsAndConditions = "";
		if ((null != auction) && (null != auction.getAuctionTerms())){			
			if (!auction.getAuctionTerms().equalsIgnoreCase("null")){
				termsAndConditions = auction.getAuctionTerms();
			}
		}
		return termsAndConditions;
	}
	
}
