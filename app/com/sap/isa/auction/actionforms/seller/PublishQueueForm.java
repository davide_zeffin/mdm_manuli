package com.sap.isa.auction.actionforms.seller;

import java.util.Collection;
import java.util.Date;
import java.util.Calendar;
import java.util.Enumeration;
import java.util.Hashtable;

import com.sap.isa.auction.actions.seller.ActionForwards;
import com.sap.isa.auction.bean.AuctionStatusEnum;
import com.sap.isa.auction.bean.b2x.PrivateAuction;
import com.sap.isa.auction.helpers.HelpManager;
import com.sap.isa.auction.helpers.PublishQueueHelper;
import com.sap.isa.auction.helpers.SearchHelper;
import com.sap.isa.auction.helpers.SearchResult;
import com.sap.isa.auction.models.listmodels.seller.AuctionStatusListModel;
import com.sap.isa.auction.models.tablemodels.seller.AuctionsTableModel;
import com.sap.isa.auction.models.tablemodels.seller.PublishAuctionsTableModel;
import com.sap.isa.ui.components.TitleHeader;
import com.sapportals.htmlb.Button;
import com.sapportals.htmlb.GridLayout;
import com.sapportals.htmlb.GridLayoutCell;
import com.sapportals.htmlb.InputField;
import com.sapportals.htmlb.RadioButtonGroup;
import com.sapportals.htmlb.TextView;
import com.sapportals.htmlb.enum.DataType;
import com.sapportals.htmlb.enum.MessageType;
import com.sapportals.htmlb.table.TableView;
import com.sapportals.htmlb.type.Time;

/**
 * Title:			Internet Sales Private Auctions
 * Description:
 * @Copyright: 		Copyright (c) 2004
 * Company:			SAPLabs LLC
 * @author 
 * Created on: 		May 18, 2004
 */
public class PublishQueueForm
	extends AuctionSearchForm
	implements PublishQueueFormConstants {

	private PublishQueueHelper publishHelper;
	private TitleHeader titleHeader;
	private RadioButtonGroup publishingOptionsEdit;
	private String publishingSelection = SCHEDULENOW;

	private AuctionsTableModel auctionsTableModel;
	private TableView auctionsTable;

	public void initializePublishComponents() {
		initializeSearchComponents();
		
		LstModel_status.usePublishQueueOptions();
		LstBox_status.setModel(LstModel_status);
	
		LstModel_advStatus.usePublishQueueOptions();
		LstBox_advStatus.setModel(LstModel_advStatus);

		setPublishingParameter();
		setGroupHeader();
	}

	public void setGroupHeader() {
		Button button = (Button) addControl(HTMLB_BUTTON, PUBLISHBUTTONID);
		try {
			int context = ((PublishQueueHelper)getSearchHelper()).getContext();
			if (context == PublishQueueHelper.CREATION_CONTEXT) {
				String selection = String.valueOf(AuctionStatusEnum.OPEN.getValue());
				LstBox_status.setSelection(selection);
				LstModel_status.setSelection(selection);
				LstBox_advStatus.setSelection(selection);
				LstModel_advStatus.setSelection(selection);
			}
			else 
				setSearchCriteriaValues();
			String selection = LstBox_status.getSelection();
			if (selection.equals(AuctionStatusListModel.PUBLISHFAILED_ID)
				|| Integer.parseInt(LstBox_status.getSelection())
				== AuctionStatusEnum.OPEN.getValue()
				|| Integer.parseInt(LstBox_status.getSelection())
					== AuctionStatusEnum.NONE.getValue()
				) {
				button.setText(translate(AuctionDetailConstants.PUBLISH_TEXT));
			} else {
				button.setText(translate(CHANGESCHEDULETEXT));
			}
		} catch (Exception exc) {
			logInfo("Unable to set the criteria values");
			button.setText(translate(CHANGESCHEDULETEXT));
		}
		//setResolveErrors();
	}

	public SearchHelper getSearchHelper() {
		if (null == publishHelper) {
			publishHelper =
				(PublishQueueHelper) getHelper(HelpManager.PUBLISHQUEUEHELPER);
		}
		return publishHelper;
	}

	private void setPublishingParameter() {
		try {
			publishingOptionsEdit = new RadioButtonGroup(SCHEDULINGOPTIONS);
			publishingOptionsEdit.setId(SCHEDULINGOPTIONS);
			TextView view = new TextView();
			view.setText(translate(SCHEDULEOPENTXT));
			publishingOptionsEdit.addComponent(view, SCHEDULEOPEN);

			view = new TextView();
			view.setText(translate(SCHEDULENOWTXT));
			publishingOptionsEdit.addComponent(view, SCHEDULENOW);

			GridLayout grid = new GridLayout();

			view = new TextView(translate(SCHEDULEAFTERTXT));
			GridLayoutCell cell = new GridLayoutCell(view);
			grid.addCell(1, 1, cell);

			InputField daysEdit = new InputField();
			daysEdit.setId(SCHEDULELAFTERDATE);
			daysEdit.setSize(7);
			daysEdit.setType(DataType.DATE);
			daysEdit.setShowHelp(true);

			this.setAttribute(SCHEDULELAFTERDATE, daysEdit);
			cell = new GridLayoutCell(daysEdit);
			grid.addCell(1, 2, cell);
			
			InputField timeEdit = new InputField();
			timeEdit.setId(SCHEDULEAFTERTIME);
			timeEdit.setSize(5);
			timeEdit.setType(DataType.TIME);
			
			this.setAttribute(SCHEDULEAFTERTIME, timeEdit);			
			cell = new GridLayoutCell(timeEdit);
			grid.addCell(1, 3, cell);

			publishingOptionsEdit.addComponent(grid, SCHEDULELAFTER);
			publishingOptionsEdit.setSelection(publishingSelection);
			this.setAttribute(SCHEDULINGOPTIONS, publishingOptionsEdit);
		} catch (Exception exc) {
			logInfo("Unable to set the publishing parameters", exc);
		}
	}

	public boolean processPublishing() {
		String selection = SCHEDULEOPEN;
		TableView view = (TableView) this.getComponentForId(PUBLISHAUCTIONVIEW);
		if (view != null) {
			auctionsTableModel.updateSelection(view);
		}

		setGroupHeader();
		try {

			selection =
				((RadioButtonGroup) this.getComponentForId(SCHEDULINGOPTIONS))
					.getSelection();
		} catch (Exception exc) {
			logInfo("Unable to reading the publishing setup options", exc);
		}
		clearErrors();
		if (auctionsTableModel.getSelectedRows() == null
			|| !auctionsTableModel.getSelectedRows().hasMoreElements()) {
			addError(
				createMessageBar(
					translate(PUBLISHINFOTEXT),
					MessageType.ERROR));
			logInfo("The publishing or scheduling selection list was empty");
			return true;
		}
		if (selection.equals(SCHEDULEOPEN)) {
			cancelAuctionsFromPublishing();
		} else if (selection.equals(SCHEDULENOW)) {
			publishAuctions();
		} else if (selection.equals(SCHEDULELAFTER)) {
			scheduleAuctions();
		}
		return true;
	}

	/**
	 * 
	 */
	private void scheduleAuctions() {
		Enumeration selection = auctionsTableModel.getSelectedRows();
		PrivateAuction auction = null;
		Calendar scheduledTime = null;
		try {
			Date date =
				((InputField) this.getComponentForId(SCHEDULELAFTERDATE))
					.getDate()
					.getValue()
					.getUtilDate();
			Time time = ((InputField)this.getComponentForId(SCHEDULEAFTERTIME)).getTime().getValueAsTime();
			scheduledTime = Calendar.getInstance();
			scheduledTime.setTime(date);
			scheduledTime.set(Calendar.HOUR, time.getHour());
			scheduledTime.set(Calendar.MINUTE, time.getMinute());
			scheduledTime.set(Calendar.SECOND, time.getSecond());
		} catch (Exception exc) {
			logInfo("Unable to read the date to schedule", exc);
		}
		try {
			while (selection.hasMoreElements()) {
				int row = Integer.parseInt((String) selection.nextElement());
				auction = (PrivateAuction) auctionsTableModel.getValueAt(row);
				((PublishQueueHelper)getSearchHelper()).schedule(auction, scheduledTime.getTime());
			}
		} catch (Exception exc) {
			logInfo("Unable to publish the selected auctions", exc);
		}
		auctionsTableModel.clearSelection();
	}

	/**
	 * 
	 */
	private void cancelAuctionsFromPublishing() {
		Enumeration selection = auctionsTableModel.getSelectedRows();
		PrivateAuction auction = null;
		try {
			while (selection.hasMoreElements()) {
				int row = Integer.parseInt((String) selection.nextElement());
				auction = (PrivateAuction) auctionsTableModel.getValueAt(row);
				if (auction.getStatus() == AuctionStatusEnum.OPEN.getValue())
					continue;
				((PublishQueueHelper)getSearchHelper()).withDrawFromPublishing(auction);
			}
		} catch (Exception exc) {
			logInfo("Unable to withdraw the selected auctions", exc);
		}
		auctionsTableModel.clearSelection();

	}

	private boolean publishAuctions() {
		boolean flag = true;
		Enumeration selection = auctionsTableModel.getSelectedRows();
		PrivateAuction auction = null;
		try {
			while (selection.hasMoreElements()) {
				int row = Integer.parseInt((String) selection.nextElement());
				auction = (PrivateAuction) auctionsTableModel.getValueAt(row);
				((PublishQueueHelper)getSearchHelper()).publish(auction);
			}
		} catch (Exception exc) {
			logInfo("Unable to publish the selected auctions", exc);
		}
		auctionsTableModel.clearSelection();
		return flag;
	}

	public String showSubArea() {
		String showSection = request.getParameter(SHOWSUBSECTIONPARAMETER);
		String forward = "auctionerror";
		if (showSection != null && showSection.equals(SHOWERRORS)) {

			String id = request.getParameter(INDEXOFPARENTPARAMETER);
			PrivateAuction auction = getSearchHelper().getAuctionById(id);
			if (auction != null) {
				clearErrors();
				String headerMessage = translate(PUBERRORSTEXT);
				if (auction.getUserObject() != null)
					setErrors(auction.getUserObject(), headerMessage, AUCTIONSELLBUNDLE);
				else
					setErrors(auction.getExecutionException(), headerMessage, AUCTIONSELLBUNDLE);
				forward = ActionForwards.SUBERROR;
			}
		}
		return forward;

	}

	protected void setDataToModel(Collection coll) {
		auctionsTableModel.setData(coll);
		setGroupHeader();
	}

	protected void setDataToModel(SearchResult result) {
		auctionsTableModel.setData(result);
		setGroupHeader();
	}
	
	public String getTitle()  {
		PublishQueueHelper publishHelper = (PublishQueueHelper)
							getHelper(HelpManager.PUBLISHQUEUEHELPER);
		if(publishHelper.getContext() == PublishQueueHelper.CREATION_CONTEXT)  {
			publishHelper.setContext(PublishQueueHelper.DEFAULT_CONTEXT);
			setAllItemsSelected();
			return translate(ConfirmationForm.CONFIRMATIONAUCTIONTITLE);
		}  else  {
			return translate(TITLEHEADERTEXT);
		}
	}

	/**
	 * 
	 */
	private void setAllItemsSelected() {
		int count = auctionsTableModel.getRowCount();
		Hashtable selection = new Hashtable();
		for(int i = 0; i < count; i++)  {
			selection.put(Integer.toString(i+1) , Integer.toString(i+1));
		}
		auctionsTableModel.setSelectedRows(selection);
	}

	protected void initializeSearchResultComponents() {
		auctionsTable =
			(TableView) addControl(HTMLB_TABLEVIEW, PUBLISHAUCTIONVIEW);
		if (null == auctionsTableModel) {
			auctionsTableModel =
				new PublishAuctionsTableModel(
					session,
					request.getContextPath());
			auctionsTableModel.setReferenceTarget(
				"/seller/viewAuctionDetails.do",
				"");
		}
		SearchResult data = null;
		int context = ((PublishQueueHelper)getSearchHelper()).getContext();
		if (context != PublishQueueHelper.CREATION_CONTEXT) 
			data = getSearchHelper().refresh();
		else 
			data = ((PublishQueueHelper)getSearchHelper()).getSearchList();
					
		auctionsTableModel.setData(data);
		this.setAttribute(PUBLISHAUCTIONMODEL, auctionsTableModel);
		setSearchCriteriaValues();
	}
	
	
	/* (non-Javadoc)
	 * @see com.sap.isa.auction.actionforms.Form#clean()
	 */
	public void clean() {
		super.clean();
		if(auctionsTableModel != null)  {
			auctionsTableModel.clean();
		}
	}

}
