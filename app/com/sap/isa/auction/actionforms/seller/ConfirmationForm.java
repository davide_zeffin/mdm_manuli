/*
 * Created on May 10, 2004
 *
 * To change the template for this generated file go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
package com.sap.isa.auction.actionforms.seller;

import java.util.Enumeration;
import java.util.Hashtable;

import com.sap.isa.auction.actionforms.BaseForm;
import com.sap.isa.auction.bean.b2x.PrivateAuction;
import com.sap.isa.auction.helpers.AfterSaveHelper;
import com.sap.isa.auction.helpers.HelpManager;
import com.sap.isa.auction.models.tablemodels.seller.AuctionsTableModel;
import com.sapportals.htmlb.RadioButtonGroup;
import com.sapportals.htmlb.table.TableView;

/**
 * @author I803067
 *
 * To change the template for this generated type comment go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
public class ConfirmationForm extends BaseForm  implements ConfirmationFormConstants  {

	private AuctionsTableModel auctionsModel;
	private TableView auctionsTable;
	
	private RadioButtonGroup optionsEdit;
	private boolean initializedField = false;
	private String title;
	
	private void initializeFields()  {
		if(initializedField)
			return;
		auctionsModel = new AuctionsTableModel(session , request.getContextPath());
		this.setAttribute(AUCTIONSTABLEMODEL , auctionsModel);
		auctionsTable = (TableView)addControl(HTMLB_TABLEVIEW , AUCTIONSTABLEVIEW);
		
		optionsEdit = (RadioButtonGroup)addControl(HTMLB_RADIOBUTTONGRP , OPTIONSGROUP);
		optionsEdit.addItem(OPTIONS1TEXT , translate(OPTIONS1TEXT));
		optionsEdit.addItem(OPTIONS2TEXT , translate(OPTIONS2TEXT));
		optionsEdit.setSelection(OPTIONS1TEXT);
		initializedField = true;
	}
	
	public void setConfirmedAuctions()  {
		initializeFields();
		AfterSaveHelper helper = (AfterSaveHelper)getHelper(HelpManager.AFTERSAVEHELPER);
		auctionsModel.setData(helper.getAuctions());
		int count = helper.getAuctions().getSize();
		Hashtable selection = new Hashtable();
		for(int i = 0; i < count; i++)  {
			selection.put(Integer.toString(i+1) , Integer.toString(i+1));
		}
		auctionsModel.setSelectedRows(selection);
		setTitle(translate(CONFIRMATIONAUCTIONTITLE));
	}
	
	private void publish()  {
		setTitle(translate(CONFIRMATIONDEFAULTTITLE));
		AfterSaveHelper helper = (AfterSaveHelper)getHelper(HelpManager.AFTERSAVEHELPER);
		TableView tableView = (TableView)this.getComponentForId(AUCTIONSTABLEVIEW);
		if(tableView != null)  {
			auctionsModel.updateSelection(tableView);
		}
		Enumeration selection = auctionsModel.getSelectedRows();
		PrivateAuction auction = null;
		String rowId = null;
		while(selection.hasMoreElements())  {
			rowId = (String)selection.nextElement();
			auction = (PrivateAuction)auctionsModel.getValueAt(Integer.parseInt(rowId));
			helper.publish(auction);
		}
	}
	
	private void schedule()  {
		
	}
	
	private void setTitle(String txt)  {
		this.title = txt;
	}
	
	public String getTitle()  {
		return title;
	}
	
	public void processPublishing()  {
		String selection = getValue(OPTIONSGROUP);
		if(selection != null)  {
			if(selection.equals(OPTIONS1TEXT))  {
				publish();
			}  else  {
				schedule();
			}
		}
	}
}
