package com.sap.isa.auction.actionforms.seller;

/**
 * Title:			Internet Sales Private Auctions
 * Description:
 * @Copyright: 		Copyright (c) 2004
 * Company:			SAPLabs LLC
 * Created on:		May 27, 2004
 */
public interface ManualWinnerFormConstants {

	public static final String AUCTIONSTABLE_ID = "AUCTIONSTABLE_ID";
	public static final String AUCTIONSTABLEMODEL_ID = "AUCTIONSTABLEMODEL_ID";
	public static final String DETERMINEWINNER_TAB_ID = "DETERMINEWINNER_TAB_ID";
	public static final String SELECTBIDS_BTN_ID = "SELECTBIDS_BTN_ID";
	public static final String SELECTEDBIDSLIST_ID = "SELECTEDBIDSLIST_ID";
	public static final String CLEARBIDS_BTN_ID = "CLEARBIDS_BTN_ID";
	public static final String SUMMARYBIDSTABLE_ID = "SUMMARYBIDSTABLE_ID";	
	public static final String MWBIDSTABLE_ID = "MWBIDSTABLE_ID"; 
	public static final String MWBIDSTABLEMODEL_ID = "MWBIDSTABLEMODEL_ID"; 
	public static final String SUMMARYBIDSTABLEMODEL_ID = "SUMMARYBIDSTABLEMODEL_ID";			
	public static final String NEXT_BTN_ID = "NEXT_BTN_ID";
	public static final String PREV_BTN_ID = "PREV_BTN_ID";
	public static String DETERMINEWINNER_BTN_ID = "DETERMINEWINNER_BTN_ID";
	public static final String CANCEL_BTN_ID = "CANCEL_BTN_ID";
	public static final String START_DETERMINATION_BTN_ID = "START_DETERMINATION_BTN_ID"; 
	public static final String WINNERSTABLE_ID = "WINNERSTABLE_ID"; 
	public static final String WINNERSTABLEMODEL_ID = "WINNERSTABLEMODEL_ID";
	
	public static final String DETERMINEWINNER = "MWF.DetermineWinner";
	public static final String SAVE = "MWF.Save";	
	public static final String SELECTED_BIDS_TXT = "MWF.SelectedBids";
	public static final String CLOSE_DETERMINE_TXT = "MWF.CloseDetermine";
	public static final String NUM_SELECTED_BIDS_TXT = "MWF.NumSelectedBids";
	public static final String AVAILABLE_QUANTITY_TXT = "MWF.AvailableQuantity";
	public static final String SELECTED_QUANTITY_TXT = "MWF.SelectedQuantity";
	public static final String CORRECT_PROBLEMS_TXT = "MWF.CorrectProblems";
	public static final String EMPTY_ADDRESS_TXT = "MWF.EmptyAddress";
	
	public static final String MW_DETAILS = "MWF.MWDetails";
	
	public static final String AUCTION_DETAILS = "MWF.AuctionDetails";
	public static final String AUCTION_BIDS = "MWF.AuctionBids";
	public static final String WINNER_SUMMARY = "MWF.WinnerSummary";
	
	public static final String ADD_BID_SELECTION = "MWF.AddBidSelection";
	public static final String CLEAR = "MWF.Clear";
	public static final String BID_INFORMATION = "MWF.BidInformation";
	
	public static final String QUANTITY_INFO = "MWF.QuantityInfo";
	public static final String DETERMINED_SUCCESSFULLY = "MWF.DeterminedSuccessfully";
	public static final String CLICK_SAVE_TO_CHOOSE = "MWF.ClickSaveToChoose";
	
	public static final String BIDS_HIGHER_THAN_QUANTITY = "MWF.BidsHigherThanQuantity";
	public static final String NO_BIDS_SELECTED = "MWF.NoBidsSelected";
	
	public static final String ERR_AUCTION_HAS_WINNERS = "MWF.AuctionHasWinners";
	public static final String ERR_AUCTION_HAS_NOBIDS = "MWF.AuctionHasNoBids";
	public static final String ERR_UNABLE_CLOSE_AUCTION = "MWF.UnableCloseAuction";
	public static final String ERR_RESERVE_NOT_MET = "MWF.ReserveNotMet";
	
	public static final int MW_AUCTIONDETAILSINDEX = 1;
	public static final int MW_AUCTIONBIDSINDEX = 2;
	public static final int MW_SUMMARYINDEX = 3;
}
