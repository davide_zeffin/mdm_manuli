/*
 * Created on Apr 30, 2004
 *
 * To change the template for this generated file go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
package com.sap.isa.auction.actionforms.seller;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Iterator;

import com.sap.isa.auction.actionforms.BaseForm;
import com.sap.isa.auction.businessobject.b2x.AuctionBusinessObjectManager;
import com.sap.isa.auction.businessobject.businesspartner.BPListSearchData;
import com.sap.isa.auction.businessobject.businesspartner.BusinessPartnerList;
import com.sap.isa.auction.businessobject.targetGroup.TargetGroup;
import com.sap.isa.auction.businessobject.targetGroup.TargetGroupList;
import com.sap.isa.auction.businessobject.targetGroup.TargetGroupSearch;
import com.sap.isa.auction.helpers.AuctionEditHelper;
import com.sap.isa.auction.helpers.CatalogHelper;
import com.sap.isa.auction.helpers.HelpManager;
import com.sap.isa.auction.helpers.TargetGroupBPHelper;
import com.sap.isa.auction.helpers.TargetGroupsContainer;
import com.sap.isa.auction.models.tablemodels.seller.BusinessPartnerTGModel;
import com.sap.isa.businesspartner.businessobject.BusinessPartner;
import com.sap.isa.core.UserSessionData;
import com.sap.isa.core.util.WebUtil;
import com.sapportals.htmlb.GridLayout;
import com.sapportals.htmlb.GridLayoutCell;
import com.sapportals.htmlb.Image;
import com.sapportals.htmlb.InputField;
import com.sapportals.htmlb.ItemList;
import com.sapportals.htmlb.TextView;
import com.sapportals.htmlb.enum.TextViewDesign;
import com.sapportals.htmlb.enum.TextViewLayout;
import com.sapportals.htmlb.table.TableView;

/**
 * @author I803067
 *
 * To change the template for this generated type comment go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
public class TargetGroupsForm extends BaseForm  implements TargetGroupConstants  {

	protected InputField tgName;
	protected InputField firstName;
	protected InputField lastName;
	protected InputField city;
	protected InputField poCode;
	protected InputField country;
	protected InputField organisation;
	protected InputField region;
	protected InputField bpId;
	protected InputField tgDescription;
	protected BusinessPartnerTGModel searchBusinessPartnerTGModel;
	protected BusinessPartnerTGModel selectedBusinessPartnerTGModel;
	protected TableView searchBusinessPartnerView;
	protected TableView selectedBusinessPartnerView;
	private BPListSearchData searchData;
	private TargetGroupSearch tgsearchData = null;
	protected HashMap targetGroupList;
	protected boolean bShowBusinessPartnerList = true;
	protected ItemList targetAdded;
	private InputField noOfitem;
	private int noOfItemsToShow = 10;

	public void initializeTargetGroupElements()  {
		firstName = (InputField)addControl(HTMLB_INPUTFIELD , FIRSTNAME);
		lastName = (InputField)addControl(HTMLB_INPUTFIELD , LASTNAME);
		poCode = (InputField)addControl(HTMLB_INPUTFIELD , POCODE);
		region = (InputField)addControl(HTMLB_INPUTFIELD , REGION);
		country = (InputField)addControl(HTMLB_INPUTFIELD , COUNTRY);
		bpId = (InputField)addControl(HTMLB_INPUTFIELD , BPID);
		organisation = (InputField)addControl(HTMLB_INPUTFIELD , ORGANISATION);
		tgDescription = (InputField)addControl(HTMLB_INPUTFIELD , TARGETGROUPDESC);
		noOfitem = (InputField)addControl(HTMLB_INPUTFIELD , NOOFITEMS);

		if (searchBusinessPartnerView == null)
			searchBusinessPartnerView = new TableView(SEARCHBUSINESSPARTNERVIEW);
		if (searchBusinessPartnerTGModel == null)  {
			searchBusinessPartnerTGModel = new BusinessPartnerTGModel(servlet , session);
			searchBusinessPartnerTGModel.setNotApplicableText(translate(NOTAPPLICABLETEXT));
		}
			
		searchBusinessPartnerTGModel.setContextPath(request.getContextPath());
		if (selectedBusinessPartnerView == null)
			selectedBusinessPartnerView = new TableView(SELECTEDBUSINESSPARTNERVIEW);
		if (selectedBusinessPartnerTGModel == null)  {
			selectedBusinessPartnerTGModel = new BusinessPartnerTGModel(servlet , session);
			selectedBusinessPartnerTGModel.setNotApplicableText(translate(NOTAPPLICABLETEXT));
		}
		
		selectedBusinessPartnerTGModel.setContextPath(request.getContextPath());
		targetAdded = (ItemList)addControl(HTMLB_ITEMLIST , TARGETADDED);
		this.setAttribute(SEARCHBUSINESSPARTNERMODEL , searchBusinessPartnerTGModel);
		this.setAttribute(SEARCHBUSINESSPARTNERVIEW , searchBusinessPartnerView);
		this.setAttribute(SELECTEDBUSINESSPARTNERMODEL , selectedBusinessPartnerTGModel);
		this.setAttribute(SELECTEDBUSINESSPARTNERVIEW , selectedBusinessPartnerView);
		noOfitem.setString(Integer.toString(noOfItemsToShow));
		
		if(request.getParameter(ProductCatalogForm.CONTEXTPARAMETER) != null) {
			if( request.getParameter(ProductCatalogForm.CONTEXTPARAMETER).equals(CatalogHelper.CREATIONCONTEXT))  {
				getTargetGroupBPHelper().setContext(CatalogHelper.CREATIONCONTEXT);
				
			} else {
				getTargetGroupBPHelper().setContext(CatalogHelper.SELECTIONCONTEXT);		
			}
		}  else  {
			clearSearchCriteria();
			clearTGSelection();
			clearSearchResult();		
		}
		setTargetsGroupsSelectionList();
	}

	public void clearSearchResult() {
		searchBusinessPartnerTGModel.setData(new ArrayList() , null , null , true);			
		getTargetGroupBPHelper().setContext(CatalogHelper.SELECTIONCONTEXT);
	}

	public void searchBusinessPartner()  {

		UserSessionData userSession = UserSessionData.getUserSessionData(getSession());
		AuctionBusinessObjectManager auctionBOM =
				(AuctionBusinessObjectManager)userSession.getBOM(AuctionBusinessObjectManager.AUCTION_BOM);
		BusinessPartnerList list = auctionBOM.getBusinessPartnerList();
		if(null == list)  {
			list = auctionBOM.createBusinessPartnerList();
		}
		fixNumberOfItemsToShow();
		BPListSearchData searchData = getSearchData();
		HashMap searchResult = list.searchBusinessPartners(searchData);
		if(searchResult != null)  {
			searchBusinessPartnerTGModel.setData(searchResult.values(),
								  WebUtil.getAppsURL(servlet.getServletContext() , request , response , null ,
								  "/auction/seller/bpTGDetails.do" , null , null , false) ,
								  BPTGDETAILTARGET, true);
		}  else  {
			searchBusinessPartnerTGModel.setData(new ArrayList() , null , null , true);
		}
		bShowBusinessPartnerList = true;
	}
	
	public String getNumberOfItemsToShow()  {
		return Integer.toString(noOfItemsToShow);
	}
	
	public int getSelectionCount()  {
		return getTargetsContainer().getTargetGroupBPSelection() != null ?
				getTargetsContainer().getTargetGroupBPSelection().size() : 0; 
	}

	public void searchTargetGroup()  {

		UserSessionData userSession = UserSessionData.getUserSessionData(getSession());
		AuctionBusinessObjectManager auctionBOM =
				(AuctionBusinessObjectManager)userSession.getBOM(AuctionBusinessObjectManager.AUCTION_BOM);
		TargetGroupList list = auctionBOM.getTargetGroupList();
		if (list == null)
			  list = auctionBOM.createTargetGroupList();
		String desc = "";
		Collection searchResult = null;
		desc = getValue(TARGETGROUPDESC);
	  	tgsearchData = new TargetGroupSearch();
	    tgsearchData.setTgDescription(desc);
	    tgDescription.setString(desc);
		fixNumberOfItemsToShow();
		try {
			  searchResult = list.getSortedTargetGroupObjectList(tgsearchData);
		} catch (Exception exc) {
			  logError("Unable to get search result", exc);
		}

		bShowBusinessPartnerList = false;
		if(searchResult != null)  {
			searchBusinessPartnerTGModel.setData(searchResult,
								  WebUtil.getAppsURL(servlet.getServletContext() , request , response , null ,
								  "/auction/seller/bpTGDetails.do" , null , null , false) ,
								  BPTGDETAILTARGET, true);
		}  else  {
			searchBusinessPartnerTGModel.setData(new ArrayList() , null , null , true);
		}

		bShowBusinessPartnerList = false;
	}
	
	private void fixNumberOfItemsToShow()  {
		String noOfItems = getValue(NOOFITEMS); 
		if(noOfItems != null && !noOfItems.equals(""))  {
			try  {
				noOfItemsToShow = Integer.parseInt(noOfItems);
				noOfitem.setString(noOfItems);
			}  catch(Exception exc)  {
				noOfItemsToShow = 10;
			}
		}
	}

	protected BPListSearchData getSearchData()  {

		if(searchData == null)
			searchData = new BPListSearchData();
		else
			searchData.clear();
		searchData.setLastName(getValue(LASTNAME));
		searchData.setFirstName(getValue(FIRSTNAME));
		searchData.setPostCode(getValue(POCODE));
		searchData.setCountry(getValue(COUNTRY));
		searchData.setPartnerID(getValue(BPID));
		searchData.setRegion(getValue(REGION));
		searchData.setOrganization(getValue(ORGANISATION));

		firstName.setString(searchData.getFirstName());
		lastName.setString(searchData.getLastName());
		poCode.setString(searchData.getPostCode());
		country.setString(searchData.getCountry());
		bpId.setString(searchData.getPartnerID());
		region.setString(searchData.getRegion());
		organisation.setString(searchData.getOrganization());

		return searchData;
	}
	
	public void clean()  {
		if (searchBusinessPartnerTGModel != null)  {
			searchBusinessPartnerTGModel.clean();
		}

		searchBusinessPartnerTGModel.setContextPath(request.getContextPath());
		if (selectedBusinessPartnerView == null)
			selectedBusinessPartnerView = new TableView(SELECTEDBUSINESSPARTNERVIEW);
		if (selectedBusinessPartnerTGModel == null)  {
			selectedBusinessPartnerTGModel = new BusinessPartnerTGModel(servlet , session);
			selectedBusinessPartnerTGModel.setNotApplicableText(translate(NOTAPPLICABLETEXT));
		}
	}
	
	public boolean isBusinessPartnerShown()  {
		return bShowBusinessPartnerList;
	}
	
	public TargetGroupBPHelper getTargetGroupBPHelper()  {
		return (TargetGroupBPHelper)getHelper(HelpManager.TARGETGROUPHELPER);
	}
	
	public TargetGroupsContainer getTargetsContainer()  {
		if(getTargetGroupBPHelper().getContext()!= null &&
			getTargetGroupBPHelper().getContext().equals(CatalogHelper.CREATIONCONTEXT))  {
			return getEditHelper();
		}  else  {
			return getTargetGroupBPHelper();
		}
	}
	
	public AuctionEditHelper getEditHelper()  {
		return (AuctionEditHelper)getHelper(HelpManager.AUCTIONEDITHELPER);
	}

	public void updateTGSelection()  {
		Object item = null;
		TargetGroupsContainer targetGroupBPHelper = getTargetsContainer();
		try  {
			
			TableView view = (TableView)getComponentForId(SEARCHBUSINESSPARTNERVIEW);
			if(view != null)  {
				searchBusinessPartnerTGModel.updateSelection(view);
			}
			Enumeration enumerate = searchBusinessPartnerTGModel.getSelectedRows();
			while(enumerate.hasMoreElements())  {
			   item = getSearchTGModel().getValueAt(Integer.parseInt((String)enumerate.nextElement()));

			   if (item instanceof BusinessPartner) {
				  targetGroupBPHelper.addBusinessPartner(((BusinessPartner)item).getId());
				  //auction.addTargetGroup(BusinessPartnerTGModel.convertBusinessPartnerToTargetGroupBP(bp));
				}
				else if (item instanceof TargetGroup) {
					targetGroupBPHelper.addTargetGroup(((TargetGroup)item).getGuid());
				  //auction.addTargetGroup(BusinessPartnerTGModel.convertTargetGroupToTargetGroupBP(tg));
				}
			}
			setTargetsGroupsSelectionList();
			searchBusinessPartnerTGModel.clearSelection();
		}  catch(Exception exc)  {
			logError("Unable to add tg from searched tg to auction" , exc);
		}
	}
	
	public void removeTGSelection()  {
		
	}
	public void clearSearchCriteria() {
		firstName.setValue("");
		lastName.setValue("");
		poCode.setValue("");
		country.setValue("");
		organisation.setValue("");
		region.setValue("");
		bpId.setValue("");
		tgDescription.setValue("");
	}
	public void clearTGSelection()  {
		TargetGroupsContainer targetGroupContainerHelper = getTargetsContainer();
		targetGroupContainerHelper.clearTgBpSelection();
		setTargetsGroupsSelectionList();
	}

	public void removeSelectedTG()  {
		Enumeration enumerate = selectedBusinessPartnerTGModel.getSelectedRows();
		BusinessPartner itemBP = null;
		TargetGroup itemTG = null;
		Object obj = null;
		//get the auction bean from the session
		//auction = getAuctionBeanFromSession();
		TargetGroupsContainer targetGroupHelper = getTargetsContainer();
		Collection tgBPs = targetGroupHelper.getTargetGroupBPSelection();
		try  {
			while(enumerate.hasMoreElements())  {
				obj = getSelectedTGModel().getValueAt
										  (Integer.parseInt((String)enumerate.nextElement()));
				targetGroupHelper.removeTargetGroupBusinessPartner(itemBP);
			}
			selectedBusinessPartnerTGModel.clearSelection();
			selectedBusinessPartnerTGModel.setData(targetGroupHelper.getTargetGroupBPSelection(), null , null , false);
			//auction.setAcls(acls);
		}  catch(Exception exc)  {
			logError("Unable to remove tg from auction tgs" , exc);
		}
	}

	public BusinessPartnerTGModel getSearchTGModel()  {
		return searchBusinessPartnerTGModel;
	}
	
	public int getSearchResultCount()  {
		if(searchBusinessPartnerTGModel != null)  {
			return searchBusinessPartnerTGModel.getRowCount();
		}  else  {
			return 0;
		}
	}

	public BusinessPartnerTGModel getSelectedTGModel()  {
		return selectedBusinessPartnerTGModel;
	}
	
	public void setTargetsGroupsSelectionList()  {
		int i = 0;

		Object obj = null;
		String name = null;
		targetAdded = new ItemList();
		TextView view = null;
		this.setAttribute(TARGETADDED , targetAdded);
		GridLayout gridContainer = null;
		GridLayoutCell cell = null;
		Image img = null;
		TargetGroupsContainer targetGroupContainerHelper = getTargetsContainer();
		Collection selection = targetGroupContainerHelper.getTargetGroupBPSelection();
		TargetGroupBPHelper targetGroupHelper = getTargetGroupBPHelper();
		if(null == selection || 0 == selection.size())  {
			targetAdded.addComponent(createTextView(translate(EMPTYTEXT)));
			return;
		}
		Iterator iterate = selection.iterator();
		TargetGroupBPHelper.TargetGroupBusinessPartner tgBP = null;
		TargetGroup tg = null;
		BusinessPartner bp = null;
		while(iterate.hasNext())  {
			tgBP = (TargetGroupBPHelper.TargetGroupBusinessPartner)iterate.next();
			if(TargetGroupBPHelper.TargetGroupBusinessPartner.BUSINESSPARTNER == tgBP.getType())  {
				bp = targetGroupHelper.getBusinessPartner(tgBP.getId());
				name = bp.getAddress().getLastName();
				//String mimeURL = WebUtil.getMimeURL(request.getContextPath(), null, null, "/auction/mimes/images/userDL.gif");
				img = new Image(WebUtil.getMimeURL(request.getContextPath() , null , null , "/mimes/images/userDL.gif") ,
					name);
				img.setWidth("16");
				img.setHeight("16");
			}  else {
				tg = targetGroupHelper.getTargetGroup(tgBP.getId());
				name = tg.getDescription();
				//String mimeURL = WebUtil.getMimeURL(servlet.getServletContext(), null, null, "/auction/mimes/images/userDL.gif");
				img = new Image(WebUtil.getMimeURL(request.getContextPath() , null , null , "/mimes/images/companyDL.gif") ,
					name);
				img.setWidth("16");
				img.setHeight("16");
			}
			gridContainer = new GridLayout();
			gridContainer.setCellSpacing(4);
			cell = new GridLayoutCell(img);
			gridContainer.addCell(1, 1, cell);
			view = new TextView();
			cell = new GridLayoutCell(" ");
			gridContainer.addCell(1 , 2 , cell);
			cell = new GridLayoutCell(view);
			gridContainer.addCell(1 , 3 , cell);
			view.setText(name);
			view.setDesign(TextViewDesign.EMPHASIZED);
			view.setLayout(TextViewLayout.BLOCK);
			targetAdded.addComponent(gridContainer);
			i++;
		}
	}

	public String getContext() {
		return getTargetGroupBPHelper().getContext();
	}
}
