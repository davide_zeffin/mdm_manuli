package com.sap.isa.auction.actionforms.seller;

import com.sap.isa.auction.actions.seller.ActionForwards;

/**
 * Title:			Internet Sales Private Auctions
 * Description:
 * @Copyright: 		Copyright (c) 2004
 * Company:			SAPLabs LLC
 * @author 
 * Created on: 		May 18, 2004
 */
public interface PublishQueueFormConstants {
	
	public static final String TITLEHEADERTEXT		= "PQF.Title";
	
	public static final String SCHEDULEDTOPUBLISH	= "PQF.ScheduledToPublish";
	public static final String FAILEDTOPUBLISH		= "PQF.FailedToPublish";

	public static final String WITHDRAWPUBLISHING	= "WithhDrawPublish";
	public static final String WITHFROMPUBLISHTEXT	= "PQF.WithDraw";
	
	public static final String PUBLISHLISTBOX		= "PublishListBox";

	public static final String PUBLISHGROUPTEXT		= "PQF.SelectPublishTitle";

	public static final String PUBLISHBUTTONID		= "PublishButton";
	
	public static final String SCHEDULINGOPTIONS	= "SchedulingOption"; //radiobuttongroup id
	
	public static final String SCHEDULEOPENTXT		= "PQF.Open"; //schedule option1: cancel schedule
	public static final String SCHEDULEOPEN			= "OpenSchedule";
	
	public static final String SCHEDULENOWTXT		= "PQF.Now"; //schedule option2: publish now
	public static final String SCHEDULENOW			= "NowSchedule";

	public static final String SCHEDULEAFTERTXT		= "PQF.After"; //schedule option3: scheudle after
	public static final String SCHEDULELAFTER		= "AfterSchedule";
	public static final String SCHEDULELAFTERDATE	= "AfterScheduleDate";
	public static final String SCHEDULEAFTERTIME	= "AfterScheduleTime";

	public static final String APPLYPUBBUTTONID		= "ApplyPubButton";
	public static final String APPLYPUBTEXT			= "PQF.PublishApply";

	public static final String CHANGESCHEDULETEXT	= "PQF.ChangeSchedule";
	
	public static final String INDEXOFPARENTPARAMETER	= "_id";
	public static final String CHOOSEELEMENTLINK 		= "/seller/showPublishSubArea.do" + ActionForwards.QUESTIONESC + "_show" + ActionForwards.EQUALESC;
	public static final String SHOWERRORS				= "errors";
	public static final String SHOWSUBSECTIONPARAMETER 	= "_show";

	public static final String PUBLPARAMGROUPHEADER		= "PublishingParamHdr";
	public static final String PUBLISHCONTEXTPARAM		= "_publishContext";

	public static final String PUBLISHAUCTIONMODEL		= "PublishModel";
	public static final String PUBLISHAUCTIONVIEW		= "PublishTableView";
	public static final String PUBLISHINFOTEXT			= "PQF.HelpMsg";

	public static final String PUBLISHSCHBUTTONID		= "PublishSchedule";
	public static final String SCHEDULEDATEERRORTEXT	= "PQF.SchDateErr";
	
	public static final String PUBERRORSTEXT			= "PQF.PubWithErrors";
	
	public static final String FROMPUBLISHINGQUEUEPARAM = "FromPublishQueueParam";
}
