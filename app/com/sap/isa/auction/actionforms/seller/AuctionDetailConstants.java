/*
 * Created on Apr 30, 2004
 *
 * To change the template for this generated file go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
package com.sap.isa.auction.actionforms.seller;

/**
 * @author I803067
 *
 * To change the template for this generated type comment go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
public interface AuctionDetailConstants {

	public static final String CREATEAUCTION 	= "CreateAuction";
	public static final String CREATAUCTIONTEXT	= "AF.CreateAuction";
	public static final String EDITAUCTIONTEXT	= "AF.EditAuction";
	public static final String CANCEL		 	= "Cancel";
	public static final String CANCELTEXT		= "AF.Cancel";
	
	public static final String CONTINUETEXT		= "AF.Continue";
	public static final String CONTINUEEH		= "Continue";
	public static final String CONTINUETOTG     = "ContinueToTG";
	public static final String CONTINUETOSEL	= "ContinueToSel";
	
	public static final String PERUNIT			= "AF.PerUnit";
	public static final String SHOWPARAMETER 	= "_show";
	
	public static final String CREATEEDITTABEVENT = "TabEvent";
	
	public static final int GENERALDETAILSTABINDEX = 3;
	public static final int PRODUCTTABINDEX		  = 1;
	public static final int TARGETGROUPTABINDEX	  = 2;
	public static final int SUMMARYTABINDEX		  = 4;
	
	public static final String GENERALDETAILSTABTEXT  = "AF.GeneralDetails";
	public static final String PRODUCTTABTEXT		  = "AF.Products";
	public static final String TARGETTABTEXT		  = "AF.TargetGroups";
	public static final String SUMMARYTABTEXT		  = "AF.Summary";
	public static final String BIDSTABTEXT 			  = "AF.Bids";
	public static final String WINNERSTABTEXT 		  = "AF.Winners";
	public static final String ORDERSTABTEXT 		  = "AF.Orders";
	
	public static final String NEXTTEXT				  = "AF.Next";
	public static final String NEXTEVENT			  = "Next";
	
	public static final String SAVETEXT				  = "AF.Save";
	public static final String SAVEEVENT			  = "Save";

	public static final String PREVIOUSTEXT			  = "AF.Previous";
	public static final String PREVIOUSEVENT		  = "Previous";
	public static final String FINISHTEXT			  = "AF.Finish";
	public static final String FINISHEVENT			  = "Finish";
	
	public static final String NAMELABEL			  = "AF.Name";
	public static final String NAMEINPUT			  = "InputName";
	public static final String NAMEVIEW			  	  = "ViewName";
	
	public static final String TITLELABEL			  = "AF.Title";
	public static final String TITLEINPUT			  = "InputTitle";
	public static final String TITLEVIEW			  = "ViewTitle";
	public static final String TITLEMSG				  = "AF.TitleLength";
	
	public static final String DURATIONLABEL		  = "AF.Duration";
	public static final String DURATIONINPUT		  = "InputDuration";
	public static final String DURATIONVIEW			  = "ViewDuration";
	
	public static final String DAYSTEXT				  = "AF.Days";
	
	public static final String TYPELABEL			  = "AF.Type";
	
	public static final String PUBPRIINPUT			  = "InputPublicPrivate";
	public static final String PUBPRIVIEW			  = "ViewPublicPrivate";
	
	public static final String PUBPRITEXT1			  = "AF.Public";
	public static final String PUBPRITEXT2 			  = "AF.Private";
	
	public static final String FLBLINPUT			  = "InputFLBL";
	public static final String FLBLVIEW			  	  = "ViewFLBL";
	
	public static final String FLBLTEXT1			  = "AF.FullLot";
	public static final String FLBLTEXT2			  = "AF.BrokenLot";
	
	public static final String BIDTYPLABEL		  	  = "AF.BidType";
	public static final String BIDTYPINPUT		  	  = "InputBidType";
	public static final String BIDTYPVIEW			  = "ViewBidType";
	
	public static final String BIDTYPETEXT1			  = "AF.NormalBid";
	public static final String BIDTYPETEXT2			  = "AF.SealedBid";
	
	public static final String STARTPRICELABEL		  = "AF.StartPrice";
	public static final String STARTPRICEINPUT		  = "InputStartPrice";
	public static final String STARTPRICEVIEW		  = "ViewStartPrice";
	
	public static final String RESVPRICELABEL		  = "AF.ResvPrice";
	public static final String RESVPRICEINPUT		  = "InputResvPrice";
	public static final String RESVPRICEVIEW		  = "ViewResvPrice";
	
	public static final String BUYNOWLABEL		  	  = "AF.BuyNowPrice";
	public static final String BUYNOWINPUT			  = "InputBuyNow";
	public static final String BUYNOWVIEW			  = "ViewBuyNow";
	
	public static final String BIDINCRLABEL		  	  = "AF.BidIncr";
	public static final String BIDINCRINPUT			  = "InputBidIncr";
	public static final String BIDINCRVIEW			  = "ViewBidIncr";
	
	public static final String AUTOWINNERLABEL		  = "AF.AutoWinnerDetermin";
	public static final String MANUALWINNERLABEL	  = "AF.ManualWinnerDetermin";
	public static final String WINNERTYPECHKBOX		  = "WinnerTypeCheckboxId";
	public static final String WINNERTYPEVIEW		  = "WinnerDeterminationType";
	
	public static final String QTYLABEL		  	  	= "AF.Qty";
	public static final String QTYINPUT			  	= "InputQty";
	public static final String QTYVIEW			  	= "ViewQty";
	public static final String ORDERQTYVIEW			= "ViewOrderQty";

	public static final String IMAGEURLLABEL	  	  = "AF.ImageURL";
	public static final String IMAGEURLINPUT		  = "InputImageURL";
	public static final String IMAGEURLVIEW			  = "ViewImageURL";

	public static final String TERMSLABEL		  	  = "AF.Terms";
	public static final String TERMSINPUT			  = "InputTerms";
	public static final String TERMSVIEW			  = "ViewTerms";
	public static final String NOTERMSTEXT			  = "AF.NoTerms";
	public static final String TERMSMSG				  = "AF.TermsLength";
	
	public static final String TYPEHEADERTEXT		  = "AF.TypeHeader";
	public static final String PRICEHEADERTEXT		  = "AF.PriceHeader";
	
	public static final String PRODUCTSTABLEVIEW	  = "ProductsView";
	public static final String PRODUCTSTABLEMODEL	  = "ProductsModel";
	public static final String PRODUCTSTABLETITLE	  = "AF.ProductsTable";
	
	public static final String TARGETSTABLEVIEW	  	  = "TargetsView";
	public static final String TARGETSTABLEMODEL	  = "TargetsModel";
	public static final String TARGETSTABLETITLE	  = "AF.TargetsTable";

	public static final String ADDMOREPRODUCTSTEXT	  = "AF.AddProducts";
	public static final String ADDMORETARGETSTEXT	  = "AF.AddTargets";
	public static final String REMOVETEXT			  = "AF.Remove";
	
	public static final String ADDMOREPRODUCTSEVENT	= "AddMoreProd";
	public static final String REMOVEPRODUCTSEVENT	= "RemoveProd";
	public static final String ADDMORETARGETGROUPSEVENT	= "AddMoreTarget";
	public static final String REMOVETARGETEVENT		= "RemoveTarget";
	
	public static final String SAVEERRORSTEXT			= "AF.SaveErrors";
	
	public static final String AUCTIONSITEMSLIST		= "AucitonsEditing";
	public static final String AUCTIONSITEMSLISTTITLE	= "AF.AuctionsEdited";
	
	public static final String AUCTIONSREMOVEEVENT	= "RemoveAuction";
	public static final String AUCTIONSREMOVETEXT	= "AF.RemoveAuction";
	
	/**
	 * AUCTION DETAILS FORM IDs 
	 **/	
	
	public static final String AUCTIONTYPETEXT_ID 	= "AuctionTypeTextId";
	public static final String RESERVEDATETEXT_ID	= "ReserveDateTextId";
	public static final String PUBLISHDATETEXT_ID 	= "PublishDateTextId";
	public static final String CLOSINGDATETEXT_ID 	= "ClosingDateTextId";
	public static final String PUBLISHDATELABEL_ID	= "PublishDateLableId";
	public static final String HIGHESTBIDTEXT_ID 	= "HighestBidTextId";
	public static final String QUOTATIONIDTEXT_ID 	= "QuotationIdTextId";	
	public static final String STATUSTEXT_ID 	  	= "StatusTextId";
	public static final String ACTIONSAVAILABLE   	= "ActionsAvailable"; //buttons
	
	public static final String BIDSTABLE_ID 		= "BidsTableId";
	public static final String BIDSTABLEMODEL_ID	= "BidsTableModelId";
	public static final String BTNID_REFRESHBIDS	= "BTNID_REFRESHBIDS";
	
	public static final String BTNID_EDIT 			= "BTNID_EDIT.AF";
	public static final String BTNID_PUBLISH 		= "BTNID_PUBLISH.AF";
	public static final String BTNID_CLOSE 			= "BTNID_CLOSE.AF";
	public static final String BTNID_COPY 			= "BTNID_COPY.AF";
	public static final String BTNID_REFRESH 		= "BTNID_REFRESH.AF";
	public static final String BTNID_CHOOSEWINNER = "BTNID_CHOOSEWINNER.AF";
	
	public static final String FINALBIDAMOUNTTEXT_ID= "FinalBidAmountTextId"; 

	public static final String WNFIRSTNAMETEXT_ID 	= "WnFirstNameTextId";
	public static final String WNLASTNAMETEXT_ID 	= "WnLastNameTextId";
	public static final String WNORGANIZATIONTEXT_ID= "WnOrganizationTextId";
	public static final String WNCITYTEXT_ID 		= "WnCityTextId";
	public static final String WNCOUNTRYTEXT_ID 	= "WnCountryTextId";
	
	public static final String WINNER_ID			= "WINNER_ID.OrderDetail";
	public static final String ORDERSLST_ID        	= "ORDERSLST_ID.OrderDetail";
	public static final String ORDERSLSTMODEL_ID    = "ORDERSLSTMODEL_ID.OrderDetail";
	public static final String ORDERNUMBERID        = "ORDERNUMBERID.OrderDetail";
	public static final String TOTALAMOUNTNETID     = "TOTALAMOUNTNETID.OrderDetail";
	public static final String TOTALAMOUNTGROSSID	= "TOTALAMOUNTGROSSID.OrderDetail";
	public static final String TAXID				= "TAXID.OrderDetail";
	public static final String FREIGHTID 			= "FREIGHTID.OrderDetail";
	public static final String DELIVERYSTATUSID		= "DELIVERYSTATUSID.OrderDetail";
	public static final String ORDERSTATUSID 		= "ORDERSTATUSID.OrderDetail";
	public static final String SHIPPINGCONDITIONID	= "SHIPPINGCONDITIONID.OrderDetail";
	public static final String PAYMENTMETHODID		= "PAYMENTMETHODID.OrderDetail";
	public static final String INSURANCEID			= "INSURANCEID.OrderDetail";
	public static final String BIDAMMOUNTID 		= "BIDAMMOUNTID.OrderDetail";
	
	public static final String NAMEID				= "LASTNAMEID.OrderDetail";
	public static final String CITYID				= "CITYID.OrderDetail";
	public static final String REGIONID				= "REGIONID.OrderDetail";
	public static final String COUNTRYID			= "COUNTRYID.OrderDetail";
	public static final String BTNID_SELECTORDER	= "BTN_SELECTORDER.OrderDetails";
	
	public static final String LSTITEMID_ALL		= "LSTITEMID_ALL.AF";
	/**
	 * I18N Texts - GIF Auction General Info Form
	 */	
	public static final String HIGHESTBID_TEXT	 	= "GIF.HighestBid";
	public static final String CURRENTSTATUS_TEXT 	= "GIF.CurrentStatus";
	public static final String QUOTATIONID_TEXT 	= "GIF.QuotationId";
	public static final String SCHPUBLISHEDATE_TEXT = "GIF.ScheduledDate";
	public static final String PUBLISHDATE_TEXT 	= "GIF.PublishDate";
	public static final String CLOSEDATE_TEXT 		= "GIF.CloseDate";

	public static final String EDIT_TEXT 			= "GIF.Edit";
	public static final String PUBLISH_TEXT 		= "GIF.Publish";
	public static final String CLOSE_TEXT 			= "GIF.Close";
	public static final String COPY_TEXT 			= "GIF.Copy";
	public static final String DETERMINE_WINNER = "GIF.DetermineWinner";
	
	public static final String PUBERRORS_TEXT		= "AF.PublishErrors";
	public static final String CLOSEERRORS_TEXT		= "AF.CloseErrors";
	
	public static final String COPYAUCTIONAS_TEXT	= "GIF.CopyAs";
	public static final String ENTERNAMEPLEASE_TEXT	= "GIF.CopyNameRequired";
	
	public static final String PUBLISHALERT_TEXT	= "GIF.PublishAlert";
	public static final String CLOSEALERT_TEXT		= "GIF.CloseAlert";
	
	public static final String TOTALBIDS_TEXT		= "BF.TotalBids";
	public static final String REFRESHBIDS_TEXT 	= "BF.RefreshBids";	
	public static final String MSG_NOBIDS         	= "BF.NoBids";
	
	public static final String ORDERNUMBER			= "OF.orderNumber";
	public static final String TOTALAMOUNTNET		= "OF.netTotal";
	public static final String TOTALAMOUNTGROSS		= "OF.grossTotal";
	public static final String TAX					= "OF.tax";
	public static final String FREIGHT				= "OF.freight";
	public static final String DELIVERYSTATUS		= "OF.deliveryStatus";
	public static final String ORDERSTATUS			= "OF.orderStatus";
	public static final String SHIPPINGCONDITION	= "OF.shipCond";
	public static final String SHIPPINGADDRESS		= "OF.shippingAdd";
	public static final String PAYMENTMETHOD		= "OF.paymentMethod";
	public static final String INSURANCELABEL		= "OF.insurance";
	public static final String NOORDERSMESSAGE		= "OF.NoOrder";
	public static final String MSG_SELECTORDER		= "OF.selectOrder";
	
	//target
	public static final String COPYTARGET			= "_copyTarget";
	public static final String COPYTARGETNAME		= "_copyTargetName";
	public static final String COPYTARGETTITLE		= "_copyTargetTitle";
	
	public static final String COPYTITLETEXT		= "CA.Title";
	public static final String COPYNAMETEXT			= "CA.Name";
	public static final String COPYTITLETEXT2		= "CA.Title2";
	public static final String COPYBUTTONTEXT		= "CA.Copy";
	public static final String COPYCANCELTEXT		= "CA.Cancel";
}
