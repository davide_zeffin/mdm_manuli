package com.sap.isa.auction.actionforms.seller;

import java.sql.Timestamp;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.Iterator;

import com.sap.isa.auction.actionforms.BaseForm;
import com.sap.isa.auction.bean.AuctionStatusEnum;
import com.sap.isa.auction.bean.AuctionTypeEnum;
import com.sap.isa.auction.bean.b2x.AuctionVisibilityEnum;
import com.sap.isa.auction.bean.search.Criteria;
import com.sap.isa.auction.bean.search.QueryFilter;
import com.sap.isa.auction.businessobject.b2x.PrivateAuctionQueryFilter;
import com.sap.isa.auction.helpers.AuctionDetailsHelper;
import com.sap.isa.auction.helpers.HelpManager;
import com.sap.isa.auction.helpers.ManualWinnerHelper;
import com.sap.isa.auction.helpers.PublishQueueHelper;
import com.sap.isa.auction.helpers.SearchHelper;
import com.sap.isa.auction.helpers.SearchResult;
import com.sap.isa.auction.models.listmodels.seller.AuctionStatusListConstants;
import com.sap.isa.auction.models.listmodels.seller.AuctionStatusListModel;
import com.sap.isa.auction.models.listmodels.seller.SearchCriteriaListConstants;
import com.sap.isa.auction.models.listmodels.seller.SearchCriteriaListModel;
import com.sap.isa.auction.models.tablemodels.seller.AuctionsTableModel;
import com.sapportals.htmlb.DropdownListBox;
import com.sapportals.htmlb.InputField;
import com.sapportals.htmlb.RadioButtonGroup;
import com.sapportals.htmlb.table.TableView;
import com.sapportals.htmlb.type.DataDate;

/**
 * Title:			Internet Sales Private Auctions
 * Description:
 * @Copyright: 		Copyright (c) 2004
 * Company:			SAPLabs LLC
 * @author
 * Created on:		Apr 27, 2004 
 */
public class AuctionSearchForm
	extends BaseForm
	implements AuctionSearchFormConstants {

	//generic search fields
	protected InputField Input_criteria;
	protected InputField Input_date;
	protected DropdownListBox LstBox_status;
	protected DropdownListBox LstBox_criteria;
	protected AuctionStatusListModel LstModel_status;
	protected SearchCriteriaListModel LstModel_criteria;
	private boolean dateFieldSearch = false;
	private boolean showOrderId = false;

	//advance search fields
	protected InputField Input_name;
	protected InputField Input_title;
	protected InputField Input_orderId;
	protected InputField Input_publishFrmDate;
	protected InputField Input_publishToDate;
	protected InputField Input_closingFrmDate;
	protected InputField Input_closingToDate;
	protected InputField Input_productName;
	protected DropdownListBox LstBox_Pripub;
	protected DropdownListBox LstBox_fullBrok;
	protected RadioButtonGroup RadioBtnGrp_type;
	protected DropdownListBox LstBox_advStatus;
	protected AuctionStatusListModel LstModel_advStatus;

	//search result fields
	private TableView Tbl_auctions;
	private AuctionsTableModel TblModel_auctions;

	protected QueryFilter queryFilter;

	private boolean initializestate = true;
	protected SearchHelper helper;
	protected AuctionDetailsHelper detailsHelper;

	public void initializeSearchComponents() {
		final String METHOD = "initializeGenericSearchComponents";
		entering(METHOD);
		Tbl_auctions = (TableView) addControl(HTMLB_TABLEVIEW, TBLID_AUCTION);
		initializeGenericSearchComponents();
		initializeAdvancedSearchComponents();
		initializeSearchResultComponents();
		exiting(METHOD);
	}

	protected void initializeSearchResultComponents() {
		Tbl_auctions = (TableView) addControl(HTMLB_TABLEVIEW, TBLID_AUCTION);
		TblModel_auctions =
			new AuctionsTableModel(session, request.getContextPath());
		Calendar startoftoday = Calendar.getInstance();
		startoftoday.set(Calendar.HOUR_OF_DAY, 0);
		startoftoday.set(Calendar.MINUTE, 0);
		startoftoday.set(Calendar.MILLISECOND, 0);
		Timestamp today = new Timestamp(startoftoday.getTime().getTime());
		SearchResult data = null;
		
		data = getSearchHelper().refresh();		
		if(null == data)  {
			data = getSearchHelper().search(null , null , null, null, Boolean.TRUE, Boolean.FALSE,
											today , null , null , null ,
											null, false);
		}
		setDataToModel(data);
		TblModel_auctions.setReferenceTarget("/seller/viewAuctionDetails.do" , "");
//		if(data == null || data.getSize() == 0)  {
//			customizedMessage = translate(SPECIALCRITERIAMSG);
//		}
		setSearchCriteriaValues();
		this.setAttribute(TBLMODELID_AUCTION, TblModel_auctions);
	}

	protected void initializeGenericSearchComponents() {

		Input_criteria =
			(InputField) addControl(HTMLB_INPUTFIELD, INPUTID_SEARCHCRITERIA);
		Input_date = (InputField) addControl(HTMLB_INPUTFIELD, INPUTID_DATE);

		LstBox_status =
			(DropdownListBox) addControl(HTMLB_DROPDOWN, DROPDOWNID_STATUS);
		LstModel_status = new AuctionStatusListModel(session);
		this.setAttribute(LSTMODELID_STATUS, LstModel_status);
		LstBox_criteria =
			(DropdownListBox) addControl(HTMLB_DROPDOWN,
				DROPDOWNID_SEARCHCRITERIA);
		LstModel_criteria = new SearchCriteriaListModel(session);
		this.setAttribute(LSTMODELID_SEARCHCRITERIA, LstModel_criteria);

	}
	protected void initializeAdvancedSearchComponents() {

		Input_name = (InputField) addControl(HTMLB_INPUTFIELD, INPUTID_NAME);
		Input_title = (InputField) addControl(HTMLB_INPUTFIELD, INPUTID_TITLE);
		Input_orderId = (InputField) addControl(HTMLB_INPUTFIELD, INPUTID_ORDERID);
		Input_publishFrmDate =
			(InputField) addControl(HTMLB_INPUTFIELD, INPUTID_PUBLISHFRM);
		Input_publishToDate =
			(InputField) addControl(HTMLB_INPUTFIELD, INPUTID_PUBLISHTO);
		Input_closingFrmDate =
			(InputField) addControl(HTMLB_INPUTFIELD, INPUTID_CLOSINGFRM);
		Input_closingToDate =
			(InputField) addControl(HTMLB_INPUTFIELD, INPUTID_CLOSINGTO);
		Input_productName =
			(InputField) addControl(HTMLB_INPUTFIELD, INPUTID_PRODUCTNAME);
		LstBox_Pripub = (DropdownListBox) addControl(HTMLB_DROPDOWN, DROPDOWNID_PRIPUB);
		LstBox_Pripub.addItem(AuctionDetailConstants.LSTITEMID_ALL, translate(AuctionStatusListConstants.LSTITM_ALL));
		LstBox_Pripub.addItem(AuctionDetailConstants.PUBPRITEXT1 , translate(AuctionDetailConstants.PUBPRITEXT1));
		LstBox_Pripub.addItem(AuctionDetailConstants.PUBPRITEXT2, translate(AuctionDetailConstants.PUBPRITEXT2));
		LstBox_Pripub.setSelection(AuctionDetailConstants.LSTITEMID_ALL);
		
		LstBox_fullBrok = (DropdownListBox) addControl(HTMLB_DROPDOWN, DROPDOWNID_FULBROK);
		LstBox_fullBrok.addItem(AuctionDetailConstants.LSTITEMID_ALL, translate(AuctionStatusListConstants.LSTITM_ALL));
		LstBox_fullBrok.addItem(AuctionDetailConstants.FLBLTEXT1 , translate(AuctionDetailConstants.FLBLTEXT1));
		LstBox_fullBrok.addItem(AuctionDetailConstants.FLBLTEXT2 , translate(AuctionDetailConstants.FLBLTEXT2));
		LstBox_fullBrok.setSelection(AuctionDetailConstants.LSTITEMID_ALL);
		
		LstBox_advStatus =
			(DropdownListBox) addControl(HTMLB_DROPDOWN, DROPDOWNID_AVDSTATUS);
		LstModel_advStatus = new AuctionStatusListModel(session);
		LstBox_advStatus.setJsObjectNeeded(true);
		this.setAttribute(LSTMODELID_AVDSTATUS, LstModel_advStatus);
	}
	public boolean showOrderIdInput() {
		return showOrderId;
	}
	public boolean isDateFieldSearch(){
		return dateFieldSearch;
	}
	public void genericSearch() {
		final String METHOD = "genericSearch";
		entering(METHOD);
		Date closingDate = null;
		Date publishDate = null;
		String auctionName = null;
		String prodName = null;
		String auctionTitle = null;
		boolean publishWithErrors = false; 

		String status = getValue(DROPDOWNID_STATUS);

		if (status != null
			&& status.equals(
				Integer.toString(AuctionStatusEnum.NONE.getValue()))) {
			status = null;
		}
		if(status != null && status.equals(AuctionStatusListModel.PUBLISHFAILED_ID))  {
			status = null;
			publishWithErrors = true;
		}
		String searchCriteria = getValue(DROPDOWNID_SEARCHCRITERIA);
		String searchValue = getValue(INPUTID_SEARCHCRITERIA);
		try {
			dateFieldSearch = false;
			if (searchCriteria //closing date
				.equalsIgnoreCase(
					SearchCriteriaListConstants.LSTITM_CLOSINGDATE)) {
				dateFieldSearch = true;
				String closingDateStringVal = getValue(INPUTID_DATE);
				if (closingDateStringVal != null
					&& !closingDateStringVal.trim().equals("")
					&& !closingDateStringVal.equals(INITIALDATE)) {

					closingDate =
						((InputField) this.getComponentForId(INPUTID_DATE))
							.getDate()
							.getValue()
							.getUtilDate();
				}
			} else if (
				//name
			searchCriteria.equalsIgnoreCase(
					SearchCriteriaListConstants.LSTITM_NAME))
				auctionName = searchValue;
			else if ( 
				//title
			searchCriteria.equalsIgnoreCase(
					SearchCriteriaListConstants.LSTITM_TITLE) )
				auctionTitle = searchValue;
            
			else if (
				//product name
			searchCriteria.equals(
					SearchCriteriaListConstants.LSTITM_PRODUCTNAME))
				prodName = searchValue;
			else if (
				//publish date
			searchCriteria.equalsIgnoreCase(
					SearchCriteriaListConstants.LSTITM_PUBLISHDATE)) {
				dateFieldSearch = true;
				String publishDateStringVal = getValue(INPUTID_DATE);
				if (publishDateStringVal != null
					&& !publishDateStringVal.trim().equals("")
					&& !publishDateStringVal.equals(INITIALDATE)) {

					publishDate =
						((InputField) this.getComponentForId(INPUTID_DATE))
							.getDate()
							.getValue()
							.getUtilDate();
				}

			}

		} catch (Exception exc) {
			logInfo("Unable to read the input values for search", exc);
		}

		SearchResult data =
			getSearchHelper().search(
				status,
				closingDate,
				publishDate,
				auctionName,
				prodName,
				auctionTitle,
				publishWithErrors);

		logInfo("set the search result to the auctionsTableModel");

		setDataToModel(data);
		this.setAttribute(TBLMODELID_AUCTION, TblModel_auctions);
		setSearchCriteriaValues(); //set search criteria back
		exiting(METHOD);
	}

	public void advancedSearch() {
		final String METHOD = "advancedSearch";
		entering(METHOD);
		boolean publishWithErrors = false;
		String name = getValue(INPUTID_NAME);
		String prodName = getValue(INPUTID_PRODUCTNAME);
		String title = getValue(INPUTID_TITLE);
		
		String dateStringVal = ((InputField) getComponentForId(INPUTID_PUBLISHFRM)).getDate().getValueAsString();
		//			((DataDate) ((InputField) getComponentForId(PUBLISHFROMID))
		//				.getValue())
		//				.getValueAsString();
		Date publishFromDate = null;
		if (dateStringVal != null
			&& !dateStringVal.trim().equals("")
			&& !dateStringVal.equals(INITIALDATE)) {
			publishFromDate =
				((InputField) getComponentForId(INPUTID_PUBLISHFRM))
					.getDate()
					.getValue()
					.getUtilDate();
		}

		dateStringVal = ((InputField) getComponentForId(INPUTID_PUBLISHTO)).getDate().getValueAsString();
		Date publishTillDate = null;
		if (dateStringVal != null
			&& !dateStringVal.trim().equals("")
			&& !dateStringVal.equals(INITIALDATE)) {
			publishTillDate =
				((InputField) getComponentForId(INPUTID_PUBLISHTO))
					.getDate()
					.getValue()
					.getUtilDate();
		}

		dateStringVal = ((InputField) getComponentForId(INPUTID_CLOSINGFRM)).getDate().getValueAsString();
		Date closingFromDate = null;
		if (dateStringVal != null
			&& !dateStringVal.trim().equals("")
			&& !dateStringVal.equals(INITIALDATE)) {
			closingFromDate =
				((InputField) getComponentForId(INPUTID_CLOSINGFRM))
					.getDate()
					.getValue()
					.getUtilDate();
		}

		dateStringVal = ((InputField) getComponentForId(INPUTID_CLOSINGTO)).getDate().getValueAsString();
		Date closingTillDate = null;
		if (dateStringVal != null
			&& !dateStringVal.trim().equals("")
			&& !dateStringVal.equals(INITIALDATE)) {
			closingTillDate =
				((InputField) getComponentForId(INPUTID_CLOSINGTO))
					.getDate()
					.getValue()
					.getUtilDate();
		}

		String status = getValue(DROPDOWNID_AVDSTATUS);
		String orderId = null;
		if (status != null
			&& status.equals(
				Integer.toString(AuctionStatusEnum.NONE.getValue()))) {
			status = null;
		}
		if(status != null && status.equals(AuctionStatusListModel.PUBLISHFAILED_ID))  {
			status = null;
			publishWithErrors = true;
		}
		
		if(status != null 
			&& (status.equals(Integer.toString(AuctionStatusEnum.CHECKEDOUT.getValue()))
				|| status.equals(Integer.toString(AuctionStatusEnum.FINALIZED.getValue()))
				|| status.equals(Integer.toString(AuctionStatusEnum.FINISHED.getValue()))
			))  {
			orderId = getValue(INPUTID_ORDERID);
		 }

		String type = getValue(DROPDOWNID_FULBROK);
		Boolean normalAuc = null;
		if(type.equals(AuctionDetailConstants.FLBLTEXT1))
			normalAuc = Boolean.TRUE;
		if(type.equals(AuctionDetailConstants.FLBLTEXT2))
			normalAuc = Boolean.FALSE;
		String visibility = getValue(DROPDOWNID_PRIPUB);
		Boolean isPrivate = null;
		if(visibility.equals(AuctionDetailConstants.PUBPRITEXT1))
			isPrivate = Boolean.FALSE;
		if(visibility.equals(AuctionDetailConstants.PUBPRITEXT2))
			isPrivate = Boolean.TRUE;
		SearchResult data =
			getSearchHelper().search(
				name,
				prodName,
				title,
				orderId,
				normalAuc,
				isPrivate,
				publishFromDate,
				publishTillDate,
				closingFromDate,
				closingTillDate,
				status,
				publishWithErrors);
		logInfo("set the search result to the auctionsTableModel");
		setDataToModel(data);
		this.setAttribute(TBLMODELID_AUCTION, TblModel_auctions);
		setSearchCriteriaValues();
		exiting(METHOD);
	}

	public void refresh() {
		final String METHOD = "refresh";
		entering(METHOD);
		SearchResult data = getSearchHelper().refresh();
		logInfo("set the search result to the auctionsTableModel");
		setDataToModel(data);
		exiting(METHOD);
	}
	//clean the advanced search fields
	public void cleanAdvancedSearchFields() {
		final String METHOD = "cleanAdvancedSearchFields";
		entering(METHOD);
		Input_name.setValue("");
		Input_productName.setValue("");
		Input_title.setValue("");
		Input_orderId.setValue("");
		DataDate date = new DataDate();
		Input_publishFrmDate.setDate(date);
		Input_publishToDate.setDate(date);
		Input_closingFrmDate.setDate(date);
		Input_closingToDate.setDate(date);
		LstBox_Pripub.setSelection(AuctionDetailConstants.LSTITEMID_ALL);
		LstBox_fullBrok.setSelection(AuctionDetailConstants.LSTITEMID_ALL);
		LstModel_advStatus.setSelection(
			String.valueOf(AuctionStatusEnum.NONE.getValue()));
		LstBox_advStatus.setSelection(
			String.valueOf(AuctionStatusEnum.NONE.getValue()));
		exiting(METHOD);
	}
	//clean the generic search fields
	public void cleanGenericSearchFields() {
		final String METHOD = "cleanGenericSearchFields";
		entering(METHOD);
		Input_date.setDate(null);
		LstBox_status.setSelection(
			String.valueOf(AuctionStatusEnum.NONE.getValue()));
		LstModel_status.setSelection(
			String.valueOf(AuctionStatusEnum.NONE.getValue()));
		LstBox_criteria.setSelection(SearchCriteriaListConstants.LSTITM_NAME);
		LstModel_criteria.setSelection(SearchCriteriaListConstants.LSTITM_NAME);
		Input_criteria.setString("");
		exiting(METHOD);
	}
	public boolean publishAuction() {
		final String METHOD = "publishAuction";
		entering(METHOD);
		boolean errorOccured = !(getDetailsHelper().publish());
		if(errorOccured)  {
			clearErrors();
			if(getDetailsHelper().getErrors() != null)  {
				errorOccured = true;
				setErrors(getDetailsHelper().getErrors() , translate(AuctionDetailConstants.PUBERRORS_TEXT), AUCTIONSELLBUNDLE);
				getDetailsHelper().clearErrors();
			}
		}
		exiting(METHOD);
		return !errorOccured;
	}
    
	public void resetContext()  {
    	
	}

	public boolean closeAuction() {
		final String METHOD = "closeAuction";
		entering(METHOD);
		boolean errorOccured = !getDetailsHelper().close();
		if(errorOccured)  {
			clearErrors();
			if(getDetailsHelper().getErrors() != null)  {
				errorOccured = true;
				setErrors(getDetailsHelper().getErrors() , translate(AuctionDetailConstants.CLOSEERRORS_TEXT), AUCTIONSELLBUNDLE);
				getDetailsHelper().clearErrors();
			}
		}
		exiting(METHOD);
		return !errorOccured;
	}
	
	public void refreshAuction()  {
		final String METHOD = "refreshAuction";
		entering(METHOD);
		getSearchHelper().refreshAuction();
		exiting(METHOD);
	}
	
	public void copyAuction(String name)  {
		getDetailsHelper().copyAuction(name);
	}
	
	public String getLastAccessedAuctonId(Integer fromSrc){
		final String METHOD = "getLastAccessedAuctonId";
		entering(METHOD);
		String auctionId = null;
		if (SRC_PUBLISHQUEUE == fromSrc.intValue()){
			auctionId = ((PublishQueueHelper)getHelper(
								HelpManager.PUBLISHQUEUEHELPER)).
								getLastAccessedAuctionId();
		} else if (SRC_MONITORING == fromSrc.intValue()){
			auctionId = getSearchHelper().getLastAccessedAuctionId();
		} else if (SRC_MANUALWINNERQUEUE == fromSrc.intValue()){
			auctionId = ((ManualWinnerHelper)getHelper(
								HelpManager.MANUALWINNERHELPER)).
								getLastAccessedAuctionId();
		}
		exiting(METHOD);
		return auctionId;
	}
	protected void setSearchCriteriaValues() {
		final String METHOD = "setSearchCriteriaValues";
		entering(METHOD);
		setGenericSearchCriteria();
		setAdvancedSearchCriteria();
		exiting(METHOD);
	}
	private void setGenericSearchCriteria() {
		final String METHOD = "setGenericSearchCriteria";
		entering(METHOD);
		QueryFilter queryFilter = getSearchHelper().getGenericQueryFilter();
		cleanGenericSearchFields();
        boolean isDateSet = false;
		if (queryFilter != null) {
			Iterator it = queryFilter.getCriterias().iterator();
			while (it.hasNext()) {
				Criteria crit = (Criteria) it.next();
				if (crit.name.equals(PrivateAuctionQueryFilter.STATUS)) {
					//status
					Integer status = (Integer) crit.rhs;
					LstBox_status.setSelection(
						String.valueOf(status.intValue()));
					LstModel_status.setSelection(
						String.valueOf(status.intValue()));
					if(status.intValue() == AuctionStatusEnum.OPEN.getValue())  {
						Iterator iterate = queryFilter.getCriterias().iterator();
						while(iterate.hasNext())  {
							if(((Criteria)iterate.next()).name.equals(PrivateAuctionQueryFilter.PUBLISH_ERROR))  {
								LstModel_status.setSelection(AuctionStatusListModel.PUBLISHFAILED_ID);
								LstBox_status.setSelection(AuctionStatusListModel.PUBLISHFAILED_ID);
								break;
							}
						}
					}
				}
				if (crit.name.equals(PrivateAuctionQueryFilter.NAME)) {
					//auction name
					LstBox_criteria.setSelection(
						SearchCriteriaListConstants.LSTITM_NAME);
					LstModel_criteria.setSelection(
						SearchCriteriaListConstants.LSTITM_NAME);
					Input_criteria.setValue(crit.rhs);
				}
				if (crit.name.equals(PrivateAuctionQueryFilter.TITLE)) {
					//auction title
					LstBox_criteria.setSelection(
						SearchCriteriaListConstants.LSTITM_TITLE);
					LstModel_criteria.setSelection(
						SearchCriteriaListConstants.LSTITM_TITLE);
					Input_criteria.setValue(crit.rhs);
				}
				if (crit.name.equals(PrivateAuctionQueryFilter.PRODUCT_ID)) {
					//product name
					LstBox_criteria.setSelection(
						SearchCriteriaListConstants.LSTITM_PRODUCTNAME);
					LstModel_criteria.setSelection(
						SearchCriteriaListConstants.LSTITM_PRODUCTNAME);
					Input_criteria.setValue(crit.rhs);
				}
				if (crit.name.equals(PrivateAuctionQueryFilter.START_DATE)) {
					//publish date
                    if (isDateSet == false) {
                        isDateSet = true;
                        LstBox_criteria.setSelection(
                            SearchCriteriaListConstants.LSTITM_PUBLISHDATE);
                        LstModel_criteria.setSelection(
                            SearchCriteriaListConstants.LSTITM_PUBLISHDATE);
                        dateFieldSearch = true;
                        Timestamp javaDate = (Timestamp) crit.rhs;
                        DataDate htmlbDate = new DataDate(javaDate);
                        Input_date.setDate(htmlbDate);                        
                    }

				}
				if (crit.name.equals(PrivateAuctionQueryFilter.END_DATE)) {
					//closing date
                    if (isDateSet == false) {
                        isDateSet = true;
                        LstBox_criteria.setSelection(
                            SearchCriteriaListConstants.LSTITM_CLOSINGDATE);
                        LstModel_criteria.setSelection(
                            SearchCriteriaListConstants.LSTITM_CLOSINGDATE);
                        Timestamp javaDate = (Timestamp) crit.rhs;
                        dateFieldSearch = true;
                        DataDate htmlbDate = new DataDate(javaDate);
                        Input_date.setDate(htmlbDate);                        
                    }
				}
			} //end of while
		} //end of outer if
		exiting(METHOD);
	}
	private void setAdvancedSearchCriteria() {
		final String METHOD = "setAdvancedSearchCriteria";
		entering(METHOD);
		QueryFilter queryFilter = getSearchHelper().getAdvancedFilter();
		cleanAdvancedSearchFields();
		if (queryFilter != null) {
			Iterator it = queryFilter.getCriterias().iterator();

			while (it.hasNext()) {
				Criteria crit = (Criteria) it.next();
				if (crit.name.equals(PrivateAuctionQueryFilter.STATUS)) { //status
					Integer status = (Integer) crit.rhs;
					LstBox_advStatus.setSelection(
						String.valueOf(status.intValue()));
					LstModel_advStatus.setSelection(
						String.valueOf(status.intValue()));
					int intStatus = status.intValue();
					if(intStatus == AuctionStatusEnum.OPEN.getValue())  {
						Iterator iterate = queryFilter.getCriterias().iterator();
						while(iterate.hasNext())  {
							if(((Criteria)iterate.next()).name.equals(PrivateAuctionQueryFilter.PUBLISH_ERROR))  {
								LstBox_advStatus.setSelection(AuctionStatusListModel.PUBLISHFAILED_ID);
								LstModel_advStatus.setSelection(AuctionStatusListModel.PUBLISHFAILED_ID);
								break;
							}
						}
					}
					if (intStatus == AuctionStatusEnum.FINALIZED.getValue()
						|| intStatus == AuctionStatusEnum.CHECKEDOUT.getValue()
						|| intStatus == AuctionStatusEnum.FINISHED.getValue())
						showOrderId = true;
					else
						showOrderId = false;
				}
				if (crit.name.equals(PrivateAuctionQueryFilter.TYPE)) { //type
					Integer type = (Integer) crit.rhs;
					if (type.intValue() == AuctionTypeEnum.NORMAL.getValue())						
						LstBox_fullBrok.setSelection(AuctionDetailConstants.FLBLTEXT1);
					else
						LstBox_fullBrok.setSelection(AuctionDetailConstants.FLBLTEXT2);
				}
				if (crit.name.equals(PrivateAuctionQueryFilter.VISIBILITY)) {//visibility
					Integer visibility = (Integer) crit.rhs;
					if (visibility.intValue() == AuctionVisibilityEnum.INT_PUBLIC)
						LstBox_Pripub.setSelection(AuctionDetailConstants.PUBPRITEXT1);
					else
						LstBox_Pripub.setSelection(AuctionDetailConstants.PUBPRITEXT2);
				}				
				if (crit.name.equals(PrivateAuctionQueryFilter.NAME)) { //auction name
					Input_name.setValue(crit.rhs);
				}
				if (crit.name.equals(PrivateAuctionQueryFilter.PRODUCT_ID)) { //product name
					Input_productName.setValue(crit.rhs);
				}
				if (crit.name.equals(PrivateAuctionQueryFilter.TITLE)) {//title
				   	Input_title.setValue(crit.rhs);
				}
				if (crit.name.equals(PrivateAuctionQueryFilter.ORDER_ID)) {//orderId
				   Input_orderId.setValue(crit.rhs);
				}
				Timestamp javaDate;
				DataDate htmlbDate;
				//publish from date
				if (crit.name.equals(PrivateAuctionQueryFilter.START_DATE)
					&& crit instanceof QueryFilter.GreaterThanEqualToCriteria) {
					javaDate = (Timestamp) crit.rhs;
					htmlbDate = new DataDate(javaDate);
					Input_publishFrmDate.setDate(htmlbDate);
				}

				//publish till date
				if (crit.name.equals(PrivateAuctionQueryFilter.START_DATE)
					&& crit instanceof QueryFilter.LessThanEqualToCriteria) {
					javaDate = (Timestamp) crit.rhs;
					htmlbDate = new DataDate(javaDate);
					Input_publishToDate.setDate(htmlbDate);
				}
				//closing from date
				if (crit.name.equals(PrivateAuctionQueryFilter.END_DATE)
					&& crit instanceof QueryFilter.GreaterThanEqualToCriteria) {
					javaDate = (Timestamp) crit.rhs;
					htmlbDate = new DataDate(javaDate);
					Input_closingFrmDate.setDate(htmlbDate);
				}

				//closing till date
				if (crit.name.equals(PrivateAuctionQueryFilter.END_DATE)
					&& crit instanceof QueryFilter.LessThanEqualToCriteria) {
					javaDate = (Timestamp) crit.rhs;
					htmlbDate = new DataDate(javaDate);
					Input_closingToDate.setDate(htmlbDate);
				}
			} //end of while
		} //end of outer if
		exiting(METHOD);
	}
	
	protected void setDataToModel(Collection coll)  {
		TblModel_auctions.setData(coll);
   	}

   	protected void setDataToModel(SearchResult result)  {
		TblModel_auctions.setData(result);
   	}
	protected SearchHelper getSearchHelper() {
		if (null == helper) {
			helper = (SearchHelper) getHelper(HelpManager.SEARCHHELPER);
		}
		return helper;
	}
	protected AuctionDetailsHelper getDetailsHelper() {
		if (null == detailsHelper) {
			detailsHelper =
				(AuctionDetailsHelper) getHelper(HelpManager.AUCTIONDETAILSHELPER);
		}
		return detailsHelper;
	}
	
	
	/* (non-Javadoc)
	 * @see com.sap.isa.auction.actionforms.Form#clean()
	 */
	public void clean() {
		super.clean();
		if(TblModel_auctions != null)  {
			TblModel_auctions.clean();
		}
	}

}
