/*
 * Created on Jun 8, 2004
 *
 * To change the template for this generated file go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
package com.sap.isa.auction.actionforms.seller;

/**
 * @author I803067
 *
 * To change the template for this generated type comment go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
public interface HomeFormConstants {
	public static final String HOMEPAGETITLE		= "HOF.Title";
	public static final String DATEINFOTEXT			= "HOF.Date";
	public static final String CURRENTPUBSTATETITLE = "HOF.CurStatePub";
	public static final String ALERTSTITLE			= "HOF.Alerts";
	public static final String FINALIZEDSTATETITLE	= "HOF.FinalState";
	
	public static final String CURPUBSTATELIST		= "CurPubState";
	public static final String TDYPUBAUCTIONSTITLE	= "HOF.TdyPublish";
	public static final String TDYPUBAUCTIONSLINK	= "TodayPublished";
	public static final String TOTALACTIVETITLE		= "HOF.ActiveOvrAll";
	public static final String TOTALACTIVELINK		= "ActiveOvrAll";
	public static final String SCHTOPUBTITLE		= "HOF.SchPublish";
	public static final String SCHTOPUBLINK			= "SchPublishing";

	
	public static final String ALERTLIST			= "AlertList";
	public static final String SCHFAILEDTITLE		= "HOF.SchFailed";
	public static final String SCHFAILEDLINK		= "ScheduleFailed";
	public static final String CLOSEDNOWINNERSTITLE	= "HOF.ClosedNoWin";
	public static final String CLOSEDNOWINNERSLINK	= "ClosedNoWinner";
	
	public static final String NONE					= "HOF.None";
	public static final String FINALIZEDLIST		= "FinalizedList";
	public static final String FINALEDNOTCHKDTITLE	= "HOF.NotCheckout";
	public static final String FINALEDNOTCHKDLINK	= "NotCheckoutYet";
	public static final String FINALIZEDCHKDTITLE	= "HOF.Checkout";
	public static final String FINALIZEDCHKDLINK	= "CheckedOut";
	public static final String TODAYFIELD			= "TodayField";
	
	public static final String REFRESHID			= "Refresh";
	public static final String REFRESHTEXT			= "HOF.Refresh";

}
