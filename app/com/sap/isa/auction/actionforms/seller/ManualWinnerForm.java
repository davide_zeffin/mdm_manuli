package com.sap.isa.auction.actionforms.seller;

import java.util.Collection;

import com.sap.isa.auction.helpers.HelpManager;
import com.sap.isa.auction.helpers.SearchHelper;
import com.sap.isa.auction.helpers.SearchResult;
import com.sap.isa.auction.models.listmodels.seller.AuctionStatusListModel;
import com.sap.isa.auction.models.tablemodels.seller.ManualWinnerAuctionsTableModel;
import com.sapportals.htmlb.table.TableView;

/**
 * Title:			Internet Sales Private Auctions
 * Description:
 * @Copyright: 		Copyright (c) 2004
 * Company:			SAPLabs LLC
 * Created on:		May 25, 2004
 */
public class ManualWinnerForm extends AuctionSearchForm
		implements ManualWinnerFormConstants {
	
	protected AuctionStatusListModel auctionStatusModel;
	protected AuctionStatusListModel auctionAdvStatusModel;
	protected ManualWinnerAuctionsTableModel auctionsTableModel;
	protected TableView auctionsTable;
	
	public void initializeManualWinnerComponents(){
		final String METHOD = "initialize";
		entering(METHOD);
		
		initializeSearchComponents();

		auctionStatusModel = new AuctionStatusListModel(session);
		// Reset the status lists for use with manual winner search filter
		auctionStatusModel.useManualWinnerOptions();
		LstBox_status.setModel(auctionStatusModel);
		this.setAttribute(LSTMODELID_STATUS, auctionStatusModel);
		
		auctionAdvStatusModel = new AuctionStatusListModel(session);
		auctionAdvStatusModel.useManualWinnerOptions();
		LstBox_advStatus.setModel(auctionAdvStatusModel);
		this.setAttribute(LSTMODELID_AVDSTATUS, auctionAdvStatusModel);
									
		exiting(METHOD);
	}
	
	/* (non-Javadoc)
	 * @see com.sap.isa.auction.actionforms.seller.AuctionSearchForm#getSearchHelper()
	 */
	protected SearchHelper getSearchHelper() {
		final String METHOD = "getSearchHelper";
		entering(METHOD);
		
		if (null == helper) {
			helper = (SearchHelper) getHelper(HelpManager.MANUALWINNERHELPER);
		}		
		exiting(METHOD);
		return helper;
	}

	/* (non-Javadoc)
	 * @see com.sap.isa.auction.actionforms.seller.AuctionSearchForm#setDataToModel(java.util.Collection)
	 */
	protected void setDataToModel(Collection collection) {
		final String METHOD = "setDataToModel";
		entering(METHOD);
		
		auctionsTableModel.setData(collection);
		
		exiting(METHOD);
	}

	/* (non-Javadoc)
	 * @see com.sap.isa.auction.actionforms.seller.AuctionSearchForm#setDataToModel(com.sap.isa.auction.helpers.SearchResult)
	 */
	protected void setDataToModel(SearchResult searchResult) {
		final String METHOD = "setDataToModel";
		entering(METHOD);
		
		auctionsTableModel.setData(searchResult);
		
		exiting(METHOD);
	}
	/* (non-Javadoc)
	 * @see com.sap.isa.auction.actionforms.seller.AuctionSearchForm#initializeSearchResultComponents()
	 */
	protected void initializeSearchResultComponents() {
		final String METHOD = "initializeSearchResultComponents";
		entering(METHOD);
		
		auctionsTable =
			(TableView) addControl(HTMLB_TABLEVIEW, AUCTIONSTABLE_ID);
		if (null == auctionsTableModel) {
			auctionsTableModel =
				new ManualWinnerAuctionsTableModel(
					session,
					request.getContextPath());
			auctionsTableModel.setReferenceTarget(
				"/seller/manualWinnerInfo.do",
				"");
			
		}
		auctionsTableModel.setData(getSearchHelper().refresh());
		this.setAttribute(AUCTIONSTABLEMODEL_ID, auctionsTableModel);
		setSearchCriteriaValues();
		
		exiting(METHOD);
	}
}
