package com.sap.isa.auction.actionforms.seller;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Enumeration;
import java.util.Iterator;

import com.sap.isa.auction.bean.Auction;
import com.sap.isa.auction.bean.AuctionStatusEnum;
import com.sap.isa.auction.bean.Bid;
import com.sap.isa.auction.bean.b2x.PrivateAuction;
import com.sap.isa.auction.bean.b2x.PrivateAuctionBid;
import com.sap.isa.auction.helpers.HelpManager;
import com.sap.isa.auction.helpers.ManualWinnerHelper;
import com.sap.isa.auction.helpers.TargetGroupBPHelper;
import com.sap.isa.auction.models.tablemodels.seller.BidsTableModel;
import com.sap.isa.auction.models.tablemodels.seller.WinnersTableModel;
import com.sap.isa.businessobject.Address;
import com.sap.isa.businesspartner.businessobject.BusinessPartner;
import com.sapportals.htmlb.Button;
import com.sapportals.htmlb.Component;
import com.sapportals.htmlb.GridLayoutCell;
import com.sapportals.htmlb.ItemList;
import com.sapportals.htmlb.MessageBar;
import com.sapportals.htmlb.TextView;
import com.sapportals.htmlb.enum.ButtonDesign;
import com.sapportals.htmlb.enum.MessageType;
import com.sapportals.htmlb.enum.TextViewDesign;
import com.sapportals.htmlb.table.TableView;
import com.sapportals.htmlb.type.AbstractDataType;


/**
 * Title:			Internet Sales Private Auctions
 * Description:
 * @Copyright: 		Copyright (c) 2004
 * Company:			SAPLabs LLC
 * Created on:		Jun 4, 2004
 */
public class ManualWinnerDetailsForm extends AuctionDetailsForm 
		implements ManualWinnerFormConstants {

	private TextView highestBid;
	private TableView bidsTable;
	private TableView summaryBidsTable;
	private TableView winnersTable;
	private BidsTableModel summaryBidsTableModel;
	private BidsTableModel bidsTableModel;
	private WinnersTableModel winnersTableModel;
	private ItemList selectedBidsList;
	private boolean initialized;
//	private boolean determinationComplete;
	private ManualWinnerHelper winnerHelper;
	
	
	/* (non-Javadoc)
	 * @see com.sap.isa.auction.actionforms.seller.AuctionDetailsForm#setButtonComponent()
	 */
	protected void setButtonComponent() {
		final String METHOD = "setButtonComponent";
		entering(METHOD);
		
		Button button = null;
		String chooseWinner = translate(DETERMINE_WINNER);

		Iterator iterate = layout.iterator();
		Component comp = null;
		logInfo("Clearing previous button component");
		while (iterate.hasNext()) {
			comp = (Component) iterate.next();
			layout.removeComponent(comp);
		}
		layout.setCellSpacing(4);
		GridLayoutCell cell = null;


		// Do not show the button if the auction has invalid state;
		if (!getWinnerHelper().isValidDeterminationState()){
			logInfo("Auction has invalid determination state");
			Collection collection = getWinnerHelper().getErrors();
			Iterator iter = collection.iterator();
			while (iter.hasNext()){
				String errorMessage = translate((String)iter.next());
				logInfo(errorMessage);
				TextView errorTextView = new TextView(errorMessage);
				errorTextView.setDesign(TextViewDesign.EMPHASIZED);
				cell = new GridLayoutCell(errorTextView);
				layout.addCell(1, 1, cell);
			}
			exiting(METHOD);
			return;
		}
		
		// Display message instead of button if the reserveprice was not met
		
		if (getDetailsHelper().isAuctionReserveMet(this.auction)){
			logInfo("Displaying winner determination button");
			button = new Button(START_DETERMINATION_BTN_ID);
			button.setId(START_DETERMINATION_BTN_ID);
			button.setText(chooseWinner);
			button.setTooltip(chooseWinner);
			button.setOnClick(BTNID_CLOSE);
			button.setOnClientClick("_auction_submitRequest()");
			button.setDesign(ButtonDesign.STANDARD);
			cell = new GridLayoutCell(button);
			layout.addCell(1, 1, cell);			
		} else {
			logInfo("Displaying reserve not message");
			MessageBar mb = new MessageBar("messageBar");
			mb.setMessage(MessageType.WARNING, 
									translate(ERR_RESERVE_NOT_MET));
			
//			TextView tv = new TextView(ERR_RESERVE_NOT_MET);
//			tv.setDesign(TextViewDesign.EMPHASIZED);
			cell = new GridLayoutCell(mb);
			layout.addCell(1, 1, cell);			
		}

	}

	/* (non-Javadoc)
	 * @see com.sap.isa.auction.actionforms.seller.AuctionDetailsForm#initializeDetails(java.lang.String, java.lang.Integer)
	 */
	public void initializeDetails(String auctionId, Integer fromSrc) {
		final String METHOD = "initializeDetails";
		entering(METHOD);
		
		super.initializeDetails(auctionId, fromSrc);
		initializeGeneralInfo();
				
		Auction auction = getDetailsHelper().getAuction();
		if (null == auction){
			this.setAttribute("determinationComplete", null);			
		} else {
			Collection winners = getDetailsHelper().getWinners(auction);
			if (winners.size() != 0){
				this.setAttribute("determinationComplete", String.valueOf(true));
			} else {
				this.setAttribute("determinationComplete", null);			
			}
		}

		exiting(METHOD);
	}
	
	/**
	 * 
	 */
	public void initializeDetails(){
		initializeGeneralInfo();
//		Auction auction = getDetailsHelper().getAuction();
//		if (null != auction){
//			initializeDetails(auction.getAuctionId(), 
//					new Integer(AuctionSearchFormConstants.SRC_MANUALWINNERQUEUE));
//		}
	}
	
	/**
	 * 
	 *
	 */
	public void initializeBidInfo(){
		final String METHOD = "initializeBidInfo";
		entering(METHOD);

		auction = (PrivateAuction) getDetailsHelper().getAuction();
		Collection winners;
		boolean hasWinners = false;
		if (null != auction){
			winners = getDetailsHelper().getWinners(auction);
			hasWinners = winners.size() > 0;

			if (hasWinners){
				this.setAttribute("determinationComplete", "true");
			} else {
				this.setAttribute("determinationComplete", null);
			}
			
			logInfo("Auction winners: " + hasWinners);
		} else {
			logInfo("Auction is null");
			this.setAttribute("determinationComplete", null);
		}

		highestBid = (TextView) addControl(HTMLB_TEXTVIEW, HIGHESTBIDTEXT_ID);
		bidsTable = (TableView) addControl(HTMLB_TABLEVIEW, MWBIDSTABLE_ID);
		bidsTableModel = new BidsTableModel(session);
		bidsTableModel.setForm(this);

		// Only show bids higher than the reserve price
		Collection highestBids = getWinnerHelper().getHighestBids(auction);
		
		bidsTableModel.setData(highestBids);
		bidsTable.setModel(bidsTableModel);
		this.setAttribute(MWBIDSTABLEMODEL_ID, bidsTableModel);
		this.setAttribute(MWBIDSTABLE_ID, bidsTable);

		if (initialized){
			logInfo("Bid info already initialized");
			return;
		}

		logInfo("Bid info being initialized");
		initializeSelectedBidsList();

							
		if (null == auction){
			logInfo("Auction is null");
			addError("Auction is null");
			return;
		}
		
		// Determine what type of select boxes will be shown in the bids table
		String selectionType = "singleselect";
		if (hasWinners){
			// Show no selection if auction already has winners
			selectionType = "none";
		} else {
			if (getWinnerHelper().isBrokenLot(auction)){
				selectionType = "multiselect";
				logInfo("Auction is broken lot");
			}
		}
		
		this.setAttribute("selectionType", selectionType);
		
		Bid bid = getDetailsHelper().getHighestBid(auction);
		if (bid != null) {
			highestBid.setText(
				getConversionHelper().bigDecimalToUICurrencyString(
					bid.getBidAmount(),
					false));
		} else {
			highestBid.setText("-");
		}
		
//		int availableQuantity = auction.getQuantity();
//		this.setAttribute("availableQuantity", String.valueOf(availableQuantity));
		
		checkQuantity(auction, false);
		initialized = true;
		// reset the auctionlist
		exiting(METHOD);
	}
	
	/**
	 * 
	 * @return
	 */
	public int getTotalBidCount(){
		return bidsTableModel.getRowCount();
	}
	
	/**
	 * 
	 *
	 */	
	public void initializeSummaryInfo(){
		summaryBidsTable = (TableView) addControl(HTMLB_TABLEVIEW, 
				SUMMARYBIDSTABLE_ID);
		summaryBidsTableModel = new BidsTableModel(session);
		summaryBidsTableModel.setData(getWinnerHelper().getSelectedBids());
		summaryBidsTable.setModel(summaryBidsTableModel);

		this.setAttribute(SUMMARYBIDSTABLEMODEL_ID, summaryBidsTableModel);

		// Check if  auction already has winners
		Auction auction = getDetailsHelper().getAuction();
		Collection winners = getDetailsHelper().getWinners(auction);
		winnersTable = new TableView(WINNERSTABLE_ID);
		winnersTableModel = new WinnersTableModel(session);
		winnersTableModel.setData(winners);
		winnersTable.setModel(winnersTableModel);
		this.setAttribute(WINNERSTABLEMODEL_ID, winnersTableModel);
		
		if (!winners.isEmpty()){
			this.setAttribute("determinationComplete", "true");
			return;			
		} else {
			this.setAttribute("determinationComplete", null);
		}
		
		boolean summaryMode = true;
		checkQuantity(auction, summaryMode);
		
	}
	
	/**
	 * 
	 * @param auction
	 */
	private void checkQuantity(Auction auction, boolean summaryMode) {
		// Check quantity
		int availableQuantity = auction.getQuantity();
		int selectedBidSize = getWinnerHelper().getSelectedBids().size();
		
		Collection selectedBids = getWinnerHelper().getSelectedBids();
		Iterator aIter = selectedBids.iterator();
		int selectedQuantity = 0;
		while(aIter.hasNext()){
			Bid bid = (Bid)aIter.next();
			selectedQuantity += bid.getQuantity();
		}
		
		this.setAttribute("availableQuantity", String.valueOf(availableQuantity));
		this.setAttribute("numSelectedBids", String.valueOf(selectedBidSize));
		this.setAttribute("selectedQuantity", String.valueOf(selectedQuantity));
		
		if (selectedQuantity > availableQuantity){
			logInfo("Number of selected bids is higher than available auction quantity");
			addError(translate(BIDS_HIGHER_THAN_QUANTITY));
			this.setAttribute("showSaved", "true");
		} else if (selectedQuantity == 0){
			if (summaryMode){
				logInfo("No Bids were selected");
				addError(translate(NO_BIDS_SELECTED));
				this.setAttribute("showSaved", "true");
			}
		} else {
			this.setAttribute("showSaved", null);
		}
	}

	/**
	 * 
	 */
	public void updateBidSelection() {
		final String METHOD = "updateSelectedBids";
		entering(METHOD);
		
		TableView bidsTableView = (TableView) this.getComponentForId(
																				MWBIDSTABLE_ID);
		bidsTableModel.updateSelection(bidsTableView);																
		
		Enumeration selection = bidsTableModel.getSelectedRows();
		
		// Return if nothing was selected
		if (!selection.hasMoreElements()){
			return;
		}
		// Only allow one selected bid if auction type is anything other than broken lot
		boolean isBrokenLot = getWinnerHelper().isBrokenLot((PrivateAuction)auction); 
		if (!isBrokenLot){
			clearSelection(true);
		}
		while (selection.hasMoreElements()){
			int rowIndex = Integer.parseInt((String)selection.nextElement());			
			PrivateAuctionBid bid = (PrivateAuctionBid)bidsTableModel.
												getValueAt(rowIndex);
			if (isBrokenLot){												
				AbstractDataType adt = this.getPageContext().getDataForComponentId(
						MWBIDSTABLE_ID, "quantity", rowIndex);
				String bidQuantity = adt.getValueAsString();
				bid.setQuantity(Integer.parseInt(bidQuantity));
			}
			getWinnerHelper().addBid(bid);				
		}						
		
		setSelectedBidsList();

		// Return the selection
		Auction auction = getDetailsHelper().getAuction();
		Collection bids = getWinnerHelper().getHighestBids(auction);
		bidsTableModel.setData(null);
		bidsTableModel.setData(bids);
			
		bidsTableView.setModel(bidsTableModel);
		this.setAttribute(BIDSTABLEMODEL_ID, bidsTableModel);

		checkQuantity(auction, false);
		exiting(METHOD);
	}
	
	/**
	 * 
	 *
	 */
	public void setSelectedBidsList(){
		final String METHOD = "setSelectedBidsList";
		entering(METHOD);
		
		clearSelection(false);
		ArrayList selectedBids = (ArrayList)getWinnerHelper().getSelectedBids();
		if (!selectedBids.isEmpty()){
			Iterator iter = selectedBids.iterator();		
			while (iter.hasNext()){
				PrivateAuctionBid bid = (PrivateAuctionBid)iter.next();
				String bidText = getSelectedBidInfo(bid);
				selectedBidsList.addComponent(new TextView(bidText));
			}			
		} else {
			initializeSelectedBidsList(); 
		}		
		bidsTableModel.clearSelection();
		
		exiting(METHOD);	
	}	
	
	/**
	 * 
	 *
	 */
	public void initializeSelectedBidsList(){				
		final String METHOD = "initializeSelectedBidsList";
		entering(METHOD);
		
		selectedBidsList = (ItemList) addControl(HTMLB_ITEMLIST, 
																SELECTEDBIDSLIST_ID);
		
		logInfo("Removing previously selected bids");
		Iterator iter = selectedBidsList.iterator();
		while( iter.hasNext()){
			iter.next();
			iter.remove();
		}

		selectedBidsList.addComponent(new TextView("Empty"));
		this.setAttribute(SELECTEDBIDSLIST_ID, selectedBidsList);
		// Reset the selected quantity to 0
		this.setAttribute("selectedQuantity", "0");		
		
		exiting(METHOD);		
	}
		
	/**
	 * 
	 *
	 */
	public void clearSelection(boolean clearAll){
		final String METHOD = "clearBidsList";
		entering(METHOD);
		
		logInfo("Clearing bids list");
		selectedBidsList = (ItemList)this.getAttribute(SELECTEDBIDSLIST_ID);
		if (selectedBidsList != null) {
			Iterator bidsListIterator = selectedBidsList.iterator();
			while(bidsListIterator.hasNext()){
				bidsListIterator.next();
				bidsListIterator.remove();	
			}		
		}			
		if (clearAll){
			getWinnerHelper().clearSelection();
		}
		
		exiting(METHOD);
	}
	
	/**
	 * 
	 * @param bid
	 * @return
	 */
	protected String getSelectedBidInfo(PrivateAuctionBid bid){
		String bidText = "";
		TargetGroupBPHelper tgHelper = (TargetGroupBPHelper)getHelper(
				HelpManager.TARGETGROUPHELPER);
		BusinessPartner bp = tgHelper.getBusinessPartner(bid.getBuyerId());	
		if (bp != null && null != bp.getAddress()){
			Address address = bp.getAddress();	

			bidText = bid.getQuantity() + "@" +  getConversionHelper().bigDecimalToUICurrencyString(
					bid.getBidAmount(), false) + " " + 
					address.getFirstName().substring(0, 1) + " " + 
					address.getLastName();					
		} else {
			bidText = bid.getBidAmount() + translate(EMPTY_ADDRESS_TXT);
		}
		
		return bidText;
	}
	
	/**
	 * 
	 * @return
	 */
	protected ManualWinnerHelper getWinnerHelper(){
		final String METHOD = "getWinnerHelper";
		entering(METHOD);
		
		if (null == winnerHelper){
			logInfo("Creating winner helper");
			winnerHelper = (ManualWinnerHelper)getHelper(
					HelpManager.MANUALWINNERHELPER);
		}
		logInfo("Returning winner helper");
		exiting(METHOD);
		return winnerHelper;
	}
		
	/**
	 * 
	 *
	 */
	public void updateWinners(){
		final String METHOD = "updateWinners";
		entering(METHOD);
		
		clearErrors();
		
		logInfo("Updating winner list");
		
		// Update the winners first before changing the auction status
		getWinnerHelper().updateWinners(auction);		
		
		if (getWinnerHelper().hasErrors()){
			addErrors(getWinnerHelper().getErrors());
			return;
		}
		
		if (auction.getStatus() != AuctionStatusEnum.CLOSED.toInt()) {
			getWinnerHelper().closeAuction();
			if (getWinnerHelper().hasErrors()){
				addErrors(getWinnerHelper().getErrors());
				return;
			}
			logInfo("Successfully close auction");
		}		

		if (getWinnerHelper().hasErrors()){
			logInfo("Errors occurred during winner creation");
			addErrors(getWinnerHelper().getErrors());
//			return;
		}

		logInfo("Winner creation successful. Clearing state.");
		// Clear state
		//getWinnerHelper().clean();
		clearSelection(true);
		this.setAttribute("savedSuccessfully", "true");
		this.setAttribute("determinationComplete", "true");
		initialized = false;
		
		// Set the winners table
		Auction auction = getDetailsHelper().getAuction();
		Collection winners = getDetailsHelper().getWinners(auction);
		winnersTable = new TableView(WINNERSTABLE_ID);
		winnersTableModel = new WinnersTableModel(session);
		winnersTableModel.setData(winners);
		winnersTable.setModel(winnersTableModel);
		this.setAttribute(WINNERSTABLEMODEL_ID, winnersTableModel);
		
	}

	/**
	 * 
	 *
	 */
	public void cancelDetermination(){
		getWinnerHelper().clean();	
		clearSelection(true);
		initialized = false;
	}
	
	/* (non-Javadoc)
	 * @see com.sap.isa.auction.actionforms.seller.AuctionDetailsForm#initializeGeneralInfo()
	 */
	public void initializeGeneralInfo() {
		final String METHOD = "initializeGeneralInfo";
		entering(METHOD);
				
		if (!getWinnerHelper().isValidDeterminationState()){
			Collection errors = getWinnerHelper().getErrors();
			if (null != errors){
				Iterator iter = errors.iterator();
				while (iter.hasNext()){
					addError(translate((String)iter.next()));
				}
			}			
		}
		
		super.initializeGeneralInfo();
		
		initialized = false;
		exiting(METHOD);
		
	}
	
	/**
	 * 
	 * @param collection
	 */
	public void addErrors(Collection collection){		
		Iterator iter = collection.iterator();
		while(iter.hasNext()){
			addError(translate((String)iter.next()));
		}		
	}
}
