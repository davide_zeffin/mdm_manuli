package com.sap.isa.auction.actionforms.seller;

/**
 * Title:
 * Description:
 * Copyright:    Copyright (c) 2003
 * Company:
 * @author
 * @version 1.0
 */

public interface ProductCatalogConstants {
    public static final String CATEGORYID = "_categoryID";

    public static final String CATALOGTREE = "CatalogTree";
    public static final String CATALOGROOT = "CatalogRoot";
    public static final String PRODUCTTABLE = "ProductTable";
    public static final String PRODUCTMODEL = "ProductModel";

    public static final String PRODUCTTABLETITLE = "PF.ProductsTable";
    public static final String CATALOGTREETITLE  = "PF.CatalogTree";

    public static final String ADDPRODUCT = "SelectProductsTable";
    public static final String ADDPRODUCTTEXT = "PF.Select";
    public static final String PRODUCTID     = "_productID";

    public static final String PRODUCTLISTTARGET = "_productList";
    public static final String PRODUCTDETAILSTARGET = "_productDetails";

    public static final String SEARCHTITLE = "PF.ProductSearch";
    public static final String SEARCHBYID  = "PF.SearchById";
    public static final String SEARCHBYDESC = "PF.SearchByDesc";

    public static final String SERACHCATALOG = "SearchCatalog";
    public static final String SEARCHBUTTON  = "PF.SearchCatalog";

    public static final String SEARCHRESULTTITLE = "PF.SearchResults";
    public static final String SELECTCAT         = "Selected.Category";
    public static final String NOPRODUCT         = "PF.NoProducts";

    public static final String MYSELECTIONTITLE  = "PF.MySelection";
    public static final String SELECTIONLIST     = "ItemList.Selection";

    public static final String REMOVEBUTTON      = "RemoveButton";
    public static final String REMOVEBUTTONTITLE = "PF.Remove";

    public static final String CONTINUEBUTTON    = "ButtonContinue";
    public static final String CONTINUEBUTTONTITLE = "PF.Continue";

	public static final String SELECTPRODUCTSTITLE = "PF.SelectProduct";
    public static final String PRODUCTCATALOGTITLE = "PF.ProductCatalog";
    

    public static final String CREATEAUCTION     = "Create.Auction";
    public static final String CREATEAUCTIONTEXT = "PF.CreateAuction";
    public static final String CLEARSELECT       = "Clear.Auction";
    public static final String CLEARSELECTTEXT   = "PF.ClearSelection";

    public static final String EMPTYTEXT         = "PF.Empty";
    public static final String OPENMODE			 = "_mode";
    public static final String CREATEMODE		 = "CREATE";
    public static final String BROWSEMODE		 = "BROWSE";
    
    public static final String ADDERRORMSG		 = "Add.Error";
    public static final String SALESAREAINCOMP   = "Error.SalesArea";
    public static final String SALESNOSET		 = "Error.NoSalesArea";
    
    public static final String NOCATEGORIESTEXT = "PF.NoCategories";
    
    public static final String CONTEXTPARAMETER = "_context";
}