package com.sap.isa.auction.actionforms.seller;

import com.sap.isa.auction.bean.Bid;
import com.sap.isa.auction.bean.b2x.PrivateAuction;
import com.sap.isa.auction.models.tablemodels.seller.BidsTableModel;
import com.sapportals.htmlb.TextView;
import com.sapportals.htmlb.table.TableView;

/**
 * Title:			Internet Sales Private Auctions
 * Description:
 * @Copyright: 		Copyright (c) 2004
 * Company:			SAPLabs LLC
 * @author 
 * Created on: 		May 13, 2004
 */
public class BidsForm extends AuctionDetailsForm {

	//Bids Tab
	private TextView highestBid;
	private TableView bidsTable;
	private BidsTableModel bidsTableModel;
	
	public void initializeBidsInfo() {
		final String METHOD = "initializeBidsInfo";
		entering(METHOD);
		auction = (PrivateAuction) getDetailsHelper().getAuction();
		highestBid = (TextView) addControl(HTMLB_TEXTVIEW, HIGHESTBIDTEXT_ID);
		bidsTable = (TableView) addControl(HTMLB_TABLEVIEW, BIDSTABLE_ID);
		bidsTableModel = new BidsTableModel(session);
		
		Bid bid = getDetailsHelper().getHighestBid(auction);
		if (bid != null) {
			highestBid.setText(
				getConversionHelper().bigDecimalToUICurrencyString(
					bid.getBidAmount(),
					false));
		} else {
			highestBid.setText("-");
		}
		bidsTableModel.setData(getDetailsHelper().getBidHistory(auction));
		this.setAttribute(BIDSTABLEMODEL_ID, bidsTableModel);
		exiting(METHOD);
	}
	
	public int getTotalBidCount(){
		return bidsTableModel.getRowCount();
	}
	
	public void refreshBidHistory(){
		Bid bid = getDetailsHelper().getHighestBid(auction);
		if (bid != null) {
			highestBid.setText(
				getConversionHelper().bigDecimalToUICurrencyString(
					bid.getBidAmount(),
					false));
		}
		bidsTableModel.setData(getDetailsHelper().getBidHistory(auction));
	}
}
