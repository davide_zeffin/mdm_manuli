/*
 * Created on Apr 26, 2004
 *
 * To change the template for this generated file go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
package com.sap.isa.auction.actionforms;

import java.util.Enumeration;

import javax.servlet.http.HttpServletRequest;

import com.sap.isa.auction.businessobject.b2x.ProcessBusinessObjectManager;
import com.sap.isa.auction.businessobject.b2x.AuctionBusinessObjectManager;
import com.sap.isa.auction.businessobject.management.ProcessMetaBusinessObjectManager;
import com.sap.isa.auction.exception.InvalidStateException;
import com.sap.isa.auction.helpers.HelpManager;
import com.sap.isa.auction.helpers.HelperObjectManager;
import com.sap.isa.auction.helpers.SellerCurrencyHelperImpl;
import com.sap.isa.auction.helpers.StartHelper;
import com.sap.isa.auction.services.b2x.ServiceLocator;
import com.sap.isa.core.Constants;
import com.sap.isa.core.SessionConst;
import com.sap.isa.core.UserSessionData;
import com.sap.isa.core.businessobject.management.BOMInstantiationException;
import com.sap.isa.core.util.StartupParameter;
import com.sapportals.htmlb.rendering.PageContextFactory;

/**
 * @author I803067
 *
 * To change the template for this generated type comment go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
public class StartForm extends BaseForm {

	public static final String INITERROR = "InitializationError";
	
	public boolean setup(
		HttpServletRequest request,
		org.apache.struts.action.ActionServlet servlet) {
		HelperObjectManager helpManager =
			new HelperObjectManager(request.getSession());
		StartHelper helper =
			(StartHelper) helpManager.getHelper(HelpManager.STARTHELPER);
		UserSessionData userSession =
			UserSessionData.getUserSessionData(request.getSession());
		userSession.setAttribute(HelpManager.HELPMANAGER, helpManager);
		HelperObjectManager.unRegisterHelperClass(HelpManager.CURRENCYHELPER);
		HelperObjectManager.registerHelperClass(HelpManager.CURRENCYHELPER , SellerCurrencyHelperImpl.class);
		boolean flag = helper.isApplInitializedProperly();
		this.session = request.getSession();
		this.setPageContext(PageContextFactory.createPageContext(request));
		AuctionBusinessObjectManager aucBOM =
			(AuctionBusinessObjectManager) userSession.getBOM(
				AuctionBusinessObjectManager.AUCTION_BOM);
		StartupParameter startParam =
			(StartupParameter) userSession.getAttribute(
				SessionConst.STARTUP_PARAMETER);
		String scenarioName =
			startParam.getParameterValue(Constants.XCM_SCENARIO_RP);
		try {
			aucBOM.setServiceLocator(ServiceLocator.getBaseInstance(scenarioName));
			aucBOM.setProcessBusinessObjectManager(
				(ProcessBusinessObjectManager) ProcessMetaBusinessObjectManager
					.getInstance(scenarioName)
					.getBOM(ProcessBusinessObjectManager.class));
		} catch (BOMInstantiationException e) {
			// TODO Auto-generated catch block
			flag  = false;
		} catch (InvalidStateException e) {
			// TODO Auto-generated catch block
			flag  = false;
		} catch (Exception e) {
			// TODO Auto-generated catch block
			flag  = false;
		}
		if (!flag) {
			addError(translate(INITERROR));
		}

		StartupParameter startParameter =
			(StartupParameter) userSession.getAttribute(
				SessionConst.STARTUP_PARAMETER);
		if (request.getParameter(LoginForm.USERTYPE) == null) {
			startParameter.addParameter(LoginForm.USERTYPE, "seller", true);
		}  else  {
			StartupParameter.Parameter parameter = new StartupParameter.Parameter(LoginForm.USERTYPE ,
												request.getParameter(LoginForm.USERTYPE) , true); 
			startParameter.addParameter(parameter);
		}
		
		if(request.getParameter(LoginForm.SHOPID) != null)  {
			startParameter.addParameter(LoginForm.SHOPID , 
				request.getParameter(LoginForm.SHOPID) , true);
		}
		return flag;
	}

	public String processAppInfoQueryString(HttpServletRequest request) {
		String queryString = "";
		for (Enumeration paramNames = request.getParameterNames();
			paramNames.hasMoreElements();
			) {
			String paramName = (String) paramNames.nextElement();
			if (paramName.equals("appinfo"))
				continue;
			String paramValue = request.getParameter(paramName);
			queryString = queryString + paramName + "=" + paramValue + "&";
		}
		if (queryString.length() > 0)
			queryString =
				"?" + queryString.substring(0, queryString.length() - 1);
		return queryString;
	}

}

