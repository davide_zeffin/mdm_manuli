/*
 * Created on May 9, 2004
 *
 * To change the template for this generated file go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
package com.sap.isa.auction.actionforms;

import javax.servlet.http.HttpSession;

import com.sap.isa.auction.businessobject.b2x.AuctionBusinessObjectManager;
import com.sap.isa.auction.helpers.HelpManager;
import com.sap.isa.auction.helpers.LoginHelper;
import com.sap.isa.auction.logging.CategoryProvider;
import com.sap.isa.auction.util.FormUtility;
import com.sap.isa.core.SessionConst;
import com.sap.isa.core.UserSessionData;
import com.sap.isa.core.util.StartupParameter;
import com.sap.isa.core.util.WebUtil;
import com.sap.tc.logging.Category;
import com.sap.tc.logging.Location;
import com.sapportals.htmlb.ItemList;
import com.sapportals.htmlb.Link;
import com.sapportals.htmlb.TextView;
import com.sapportals.htmlb.rendering.PageContext;
import com.sapportals.htmlb.rendering.PageContextFactory;

/**
 * @author I803067
 *
 * To change the template for this generated type comment go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
public class LogOffForm extends BaseForm {
	public static final String LOGOFFTITLE = "LOF.LogOffTitle";
	public static final String LOGINAGAIN = "LOF.LoginAgain";
	public static final String MESSAGE = "LOF.LogOffMessage";
	public static final String INVALIDMESSAGE = "LOF.InvalidSession";
	public static final String ERROR = "LOF.GeneralError";
	public static final String USERERLOGINURL = "/init.do";
	
	public static final String LOGINURL = "LoginURLItems";
	protected static Location tracer = Location.getLocation(LogOffForm.class);
	protected static Category logger = CategoryProvider.getUICategory();

	/**
	 * 
	 */
	public void logOff(boolean invalidSession) {
		final String METHOD = "logOff";
		tracer.entering(logger, METHOD);

		TextView view = new TextView();
		TextView titleView = new TextView();
		String appendingURL = "";
		HttpSession session = request.getSession();
		boolean adminLogin = false;

		if (invalidSession) {
			view.setText(translate(AUCTIONSELLBUNDLE, INVALIDMESSAGE));
			titleView.setText(
				FormUtility.getResourceString(
					AUCTIONSELLBUNDLE,
					ERROR,
					session));
		} else {
			view.setText("");
			titleView.setText(translate(AUCTIONSELLBUNDLE, LOGOFFTITLE));
			LoginHelper helper = null;
			if (null == session) {
				return;
			}
			UserSessionData userSessionData =
				UserSessionData.getUserSessionData(session);
			if (null == userSessionData) {
				return;
			}
			AuctionBusinessObjectManager auctionBOM =
				(AuctionBusinessObjectManager) userSessionData.getBOM(
					AuctionBusinessObjectManager.AUCTION_BOM);
			auctionBOM.release();
			HelpManager helperManager =
				(HelpManager) userSessionData.getAttribute(
					HelpManager.HELPMANAGER);
			if (helperManager != null) {
				helper =
					(LoginHelper) helperManager.getHelper(
						HelpManager.LOGINHELPER);
				appendingURL = helper.getLoginURL();
				helperManager.clean();
				userSessionData.removeAttribute(HelpManager.HELPMANAGER);
			}
			StartupParameter parameter =
				(StartupParameter) userSessionData.getAttribute(
					SessionConst.STARTUP_PARAMETER);
			if (parameter != null
				&& parameter.getParameter(LoginForm.USERTYPE) != null
				&& parameter.getParameterValue(LoginForm.USERTYPE).equals(
					"seller")) {
				adminLogin = false;
			} else {
				adminLogin = true;
			}
		}
		PageContext pageContext =
			(PageContext) PageContextFactory.createPageContext(
				request,
				response);
		pageContext.setAttribute(INVALIDMESSAGE, view);
		pageContext.setAttribute(LOGOFFTITLE, titleView);

		String appsUrl;
		appsUrl =
				WebUtil.getAppsURL(
					getServlet().getServletContext(),
					request,
					response,
					null,
					USERERLOGINURL,
					null,
					null,
					false);
		
		StringBuffer buf = new StringBuffer();
		buf.append(appsUrl);
		buf.append(appendingURL);

		Link link = new Link(LOGINURL);
		link.setReference(buf.toString());
		link.setTarget("_top");
		view = new TextView();
		view.setText(translate(AUCTIONSELLBUNDLE, LOGINAGAIN));
		link.addComponent(view);
		ItemList items = new ItemList();
		items.addComponent(link);
		pageContext.setAttribute(LOGINURL, items);
	}

}
