package com.sap.isa.auction.actionforms;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;

import com.sap.isa.auction.backend.exception.BackendUserExceptionBase;
import com.sap.isa.auction.bean.Auction;
import com.sap.isa.auction.bean.AuctionItem;
import com.sap.isa.auction.businessobject.ContextManager;
import com.sap.isa.auction.businessobject.InvocationContext;
import com.sap.isa.auction.businessobject.MissingContextException;
import com.sap.isa.auction.businessobject.ValidationException;
import com.sap.isa.auction.exception.InvalidStateException;
import com.sap.isa.auction.exception.SystemException;
import com.sap.isa.auction.exception.UserException;
import com.sap.isa.auction.exception.i18n.I18nErrorMessage;
import com.sap.isa.auction.helpers.ConversionHelper;
import com.sap.isa.auction.helpers.HelpManagerBase;
import com.sap.isa.auction.helpers.Helper;
import com.sap.isa.auction.helpers.LogSupport;
import com.sap.isa.auction.helpers.LoginHelper;
import com.sap.isa.auction.services.ServiceUserException;
import com.sap.isa.auction.util.FormUtility;
import com.sap.isa.core.SharedConst;
import com.sap.isa.core.UserSessionData;
import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.core.logging.IsaSession;
import com.sap.isa.services.schedulerservice.exceptions.SchedulerException;
import com.sap.isa.ui.controllers.ActionForm;
import com.sap.tc.logging.Location;
import com.sap.util.monitor.jarm.IMonitor;
import com.sapportals.htmlb.Component;
import com.sapportals.htmlb.GridLayout;
import com.sapportals.htmlb.GridLayoutCell;
import com.sapportals.htmlb.ItemList;
import com.sapportals.htmlb.MessageBar;
import com.sapportals.htmlb.TextView;
import com.sapportals.htmlb.enum.MessageType;
import com.sapportals.htmlb.enum.TextViewDesign;
/**
 * Title:
 * Description:
 * Copyright:    Copyright (c) 2002
 * Company:
 * @author
 * @version 1.0
 */

public class Form extends ActionForm implements BaseFormConstants  {

	/** 
	  *  The IsaLocation of the current Action.
	  *  This will be instantiated during perform and is always present.
	  */
	 protected static IsaLocation log = IsaLocation.getInstance(Form.class.getName());

    protected Collection systemErrors = new ArrayList();
    protected ItemList systemErrorItemList;
    protected boolean showErrors = false;
    protected LoginHelper loginHelper;

    private GridLayout errorsList;

    public Form() {
    	super();
    	location = Location.getLocation(this.getClass());
    }

    public void preProcessing()  {
        systemErrorItemList = new ItemList();
		UserSessionData userData = UserSessionData.getUserSessionData(session);
		setInvocationContext(userData);
        this.setAttribute(SYSTEMERRORLIST , systemErrorItemList);
        this.setLocale(userData.getLocale());
		registerForCleanup();
        clearErrors();
    }

    public void postProcessing()  {
        releaseContext();
    }
	protected InvocationContext createInvocationContext(UserSessionData userData) {
    	return new InvocationContext();
	}
    protected void setInvocationContext(UserSessionData userData)  {
		InvocationContext context = createInvocationContext(userData);
		
		HelpManagerBase helpManager = (HelpManagerBase)userData.getAttribute(HelpManagerBase.HELPMANAGER);
		if(helpManager != null)  {
			LoginHelper helper = (LoginHelper)helpManager.getHelper(HelpManagerBase.LOGINHELPER);
			context.setUser(helper.getUser());
			IMonitor monitor = (IMonitor)IsaSession.getAttribute(SharedConst.SAT_MONITOR);
			context.setRequestMonitor(monitor);
			ContextManager.bindContext(context);
		}
    }

    protected void releaseContext()  {
		ContextManager.unbindContext();
    }


    protected TextView createTextView(String msg , boolean required , boolean emphasized)  {
        TextView errorSource = new TextView();
        errorSource.setText(msg);
        if(required)
        	errorSource.setRequired(true);
        if(emphasized)
        	errorSource.setDesign(TextViewDesign.EMPHASIZED);
        errorSource.setWrapping(true);
        return errorSource;
    }
    
    protected MessageBar createMessageBar(String message , MessageType typ)  {
    	MessageBar msgBar = new MessageBar(message);
    	msgBar.setMessage(typ , message);
    	return msgBar;
    }

    protected void setChildErrors(Collection beans)  {
    }

    public void addError(String msgText)  {
        if(null == errorsList)  {
            errorsList = new GridLayout();
        }
		this.setAttribute(ERRORSEDIT , errorsList);
        errorsList.addComponent(errorsList.getRowSize() + 1, 1 , createTextView(msgText , true , false));
    }

	public void addError(Component msgText)  {
		if(null == errorsList)  {
			errorsList = new GridLayout();
		}
		this.setAttribute(ERRORSEDIT , errorsList);
		errorsList.addComponent(errorsList.getRowSize() + 1, 1 , msgText);
	}

    protected void entering(String text)  {
		location = Location.getLocation(this.getClass());
    	LogSupport.entering(cat , location , text , null);
    }

    protected void exiting(String text)  {
		location = Location.getLocation(this.getClass());
    	LogSupport.exiting(cat , location , text , null);
    }

    protected void logInfo(String text)  {
		location = Location.getLocation(this.getClass());
    	LogSupport.logInfo(cat , location , text);
    }

    protected void logInfo(String text , Throwable exc)  {
		location = Location.getLocation(this.getClass());
    	LogSupport.logError(cat , location , text , exc);
    }
	protected void logError(String text , Throwable exc)  {
		location = Location.getLocation(this.getClass());
		LogSupport.logError(cat , location , text , exc);
	}

	protected void logWarning(String text , Throwable exc)  {
		location = Location.getLocation(this.getClass());
		LogSupport.logWarning(cat , location , text , exc);
	}

	public static String exceptionToString(Throwable th)  {
			String stTrace = null;
			try  {
				StringWriter sout = new StringWriter();
				PrintWriter out = new PrintWriter(sout);
				th.printStackTrace(out);
				out.close();
				sout.close();
				stTrace = sout.toString();
				sout = null;
				out = null;
			} catch(Exception _ex)  {
				log.debug("Error  ", _ex);
			}
			return stTrace;
	}

    protected void clearErrors()  {
        errorsList = new GridLayout();
        this.setAttribute(ERRORSEDIT , errorsList);
        showErrors = false;
    }

    protected void setErrors(Object errorObject , String headerMsg, String bundleName)  {
        entering("Errors");
        showErrors = true;
        TextView view = null;
        if(null != errorObject)  {
            if(errorObject instanceof ValidationException)  {
            	setValidationErrorsMessages((ValidationException)errorObject, bundleName);
            }  else if(errorObject instanceof ServiceUserException)  {
				ServiceUserException invoExc = (ServiceUserException)errorObject;
                logInfo("Publish errors are retrieved" , (Throwable)errorObject);
                if(headerMsg != null && !headerMsg.equals(""))
                	addError(createMessageBar(headerMsg , MessageType.ERROR));
                //Todo
                prepareSUEErrorList("" , (ServiceUserException)errorObject);
			}  else if(errorObject instanceof BackendUserExceptionBase)  {
				BackendUserExceptionBase buExc = (BackendUserExceptionBase)errorObject;
				logInfo("Publish errors are retrieved" , (Throwable)errorObject);
				if(headerMsg != null && !headerMsg.equals(""))
					addError(createMessageBar(headerMsg , MessageType.ERROR));
				prepareErrorList("" , new ArrayList());
				setError(buExc);
            }  else if(errorObject instanceof InvalidStateException)  {
                logInfo("InvalidState Exception errors" , (Throwable) errorObject);
                InvalidStateException exc = (InvalidStateException)errorObject;
                //setError(exc.getError());

            }  else if(errorObject instanceof MissingContextException)  {
                logInfo("MissingContext Exception errors" , (Throwable) errorObject);
                MissingContextException exc = (MissingContextException)errorObject;
                setError(exc);

            }  else if(errorObject instanceof UserException)  {
                logInfo("User Exception errors" , (Throwable) errorObject);
                UserException exc = (UserException)errorObject;
                setError(exc);

            }  else if(errorObject instanceof SystemException)  {
                logInfo("System errors" , (Throwable) errorObject);
                SystemException sysExec = (SystemException)errorObject;
                if(headerMsg != null && !headerMsg.equals(""))
                	addError(createMessageBar(headerMsg , MessageType.ERROR));
                setError(sysExec);
            } else if(errorObject instanceof SchedulerException)  {
				logInfo("Scheduler errors" , (Throwable) errorObject);
				SchedulerException schedulerExec = (SchedulerException)errorObject;				
				setError(schedulerExec);
			}   
            else  {
                logInfo("Unknown Error" , (Throwable)errorObject);
                String errorHelp = translate(bundleName ,
                        CRITICALERROR);
				addError(errorHelp);
				if(errorObject instanceof Exception)
					addError(((Exception)errorObject).getMessage());
                
            }
        }
    }
    
    protected void setValidationErrorsMessages(ValidationException exc, String bundleName)  {
		
		if(!(exc.getCulprits() instanceof Collection))  {
			logInfo("The culprits are of unknown type");
			return;
		}
		Iterator iteratorErrors =  ((Collection)exc.getCulprits()).iterator();
		Auction auction = null;
		Object err = null;
		while(iteratorErrors.hasNext())  {
			err = iteratorErrors.next();
			if(err instanceof Auction)  {
				auction = (Auction)err;
				if(null != auction.getErrors() && auction.getErrors().size() > 0)  {
					prepareErrorList(translate(bundleName ,
						HEADERERRORS) , auction.getErrors());
				}
			}  /*Todo else if(err instanceof EBayShippingPaymentInfo)  {
				EBayShippingPaymentInfo shipping = (EBayShippingPaymentInfo)err;
				if(shipping != null)  {
					if(shipping.getErrors() != null &&
						shipping.getErrors().size() > 0)  {
							prepareErrorList(FormUtility.getResourceString(FormUtility.AUCTIONSELLBUNDLE ,
									SHIPINFOERRORS , session) , shipping.getErrors());
					}
				}

			}  */else if(err instanceof AuctionItem)  {
					AuctionItem auctionItem = (AuctionItem)err;
					if(auctionItem.getErrors() != null && auctionItem.getErrors().size() > 0)  {
						prepareErrorList(translate(bundleName ,
								PRODUCTSERRORS) + " - " + auctionItem.getProductCode() , auctionItem.getErrors());
					}
			}

		}
    }

    /**
	 * @param headerMsg
	 * @param list
	 */
	private void prepareSUEErrorList(String headerMsg , ServiceUserException errorObject) {
		GridLayout grid = new GridLayout();
		GridLayoutCell cell = null;

		TextView view = createTextView(headerMsg);
		view.setDesign(TextViewDesign.EMPHASIZED);
		cell = new GridLayoutCell(view);
		ItemList errorItemList = new ItemList();
		addServiceUserExceptionErrors(errorObject , errorItemList);
		grid.addCell(1 , 1 , cell);
		cell = new GridLayoutCell(errorItemList);
		grid.addCell(2 , 1 , cell);
		errorsList.addComponent(errorsList.getRowSize() + 1 , 1 , grid);
	}

	protected void addServiceUserExceptionErrors(ServiceUserException exc , ItemList errorItemList)  {
	}

	protected void addErrors(Collection errors , GridLayout errorItemList)  {
        Iterator iterate = errors.iterator();
        UserSessionData userSessionData = UserSessionData.getUserSessionData(session);
        I18nErrorMessage error = null;
        String msg = null;
        while(iterate.hasNext())  {
            error = (I18nErrorMessage)iterate.next();
            try  {
				msg = error.format(userSessionData.getLocale()).toString();
            }  catch(Exception exc)  {
				log.debug("Error ", exc);
            	msg = error.toString();
            }
            errorItemList.addComponent(errorItemList.getRowSize() + 1 , 
            					1 , createTextView(msg != null ? msg : ""));
        }
    }

    protected void addMessage(String text)  {
        TextView view = createTextView(text);
        view.setDesign(TextViewDesign.EMPHASIZED);
        errorsList.addComponent(errorsList.getRowSize() + 1 , 1  , view);
    }

    protected TextView createTextView(String text)  {
        TextView view = new TextView();
        view.setText(text);
        return view;
    }

    public boolean showErrors()  {
        return showErrors;
    }

    protected void prepareErrorList(String headerText , Collection errors)  {

        GridLayout grid = new GridLayout();
        GridLayoutCell cell = null;

        TextView view = createTextView(headerText);
        view.setDesign(TextViewDesign.EMPHASIZED);
        cell = new GridLayoutCell(view);
        GridLayout errorItemList = new GridLayout();
        addErrors(errors , errorItemList);
        grid.addCell(1 , 1 , cell);
        cell = new GridLayoutCell(errorItemList);
        grid.addCell(2 , 1 , cell);
        errorsList.addComponent(errorsList.getRowSize() + 1, 1  , grid);
    }

    public void setError(I18nErrorMessage error)  {
        Collection errors = new ArrayList();
        errors.add(error);
        addErrors(errors , errorsList);
    }
    
    public void setError(UserException userExc)  {
		UserSessionData userSessionData = UserSessionData.getUserSessionData(session);
    	addError(userExc.getLocalizedMessage(userSessionData.getLocale()));
    }
    
    public void setError(SystemException sysExc)  {
		UserSessionData userSessionData = UserSessionData.getUserSessionData(session);
		addError(sysExc.getLocalizedMessage(userSessionData.getLocale()));
    }
    
	public void setError(BackendUserExceptionBase buExc)  {
		 List errors = buExc.getErrorMessages();
		 for(int i=0; i<errors.size(); i++)
			addError((String)errors.get(i));
		 return;
	}
	
	public void setError(SchedulerException schedulerException)  {
		addError(schedulerException.getMessage());
	}    
	public void setError(MissingContextException missExc)  {
		UserSessionData userSessionData = UserSessionData.getUserSessionData(session);
		addError(missExc.getLocalizedMessage(userSessionData.getLocale()));
	}
	
	protected LoginHelper getLoginHelper()  {
		if(null == loginHelper)  {
			loginHelper = (LoginHelper)getHelper(HelpManagerBase.LOGINHELPER);
		}
		return loginHelper;
	}
	
	protected Helper getHelper(String id)  {
		UserSessionData userSessionData = UserSessionData.getUserSessionData(session);
		HelpManagerBase helpManager = (HelpManagerBase) userSessionData.getAttribute(HelpManagerBase.HELPMANAGER);
		return helpManager.getHelper(id);
	}

	protected ConversionHelper getConversionHelper()  {
		return (ConversionHelper)getHelper(HelpManagerBase.CONVERSIONHELPER);
	}

	protected void registerForCleanup()  {
		UserSessionData userSessionData = UserSessionData.getUserSessionData(session);
		HelpManagerBase helpManager = (HelpManagerBase) userSessionData.getAttribute(HelpManagerBase.HELPMANAGER);
		helpManager.registerForInstantCleanup(this);
	}
	
	public void clean()  {
	}
	
	protected boolean integerEquals(Integer integer1 , Integer integer2)  {
		boolean same = false;
		if(integer1 == null || integer2 == null)
			return false;
		return integer1.equals(integer2);
	}

	/* (non-Javadoc)
	 * @see com.sapmarkets.isa.ui.controllers.ActionForm#translate(java.lang.String, java.lang.String)
	 */
	public String translate(String bundleName, String key) {
		String text = key;
		text = FormUtility.getResourceString(bundleName , key , session);
		return text; 
	}
	/* (non-Javadoc)
	 * @see com.sapmarkets.isa.ui.controllers.ActionForm#translate(java.lang.String)
	 */
	public String translate(String key) {
		return translate(BASEBUNDLE , key);
	}
}
