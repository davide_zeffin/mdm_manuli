/*
 * Created on Jun 1, 2004
 *
 * To change the template for this generated file go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
package com.sap.isa.auction.actionforms.admin;

import com.sap.isa.auction.actionforms.BaseForm;
import com.sap.isa.auction.helpers.HelpManager;
import com.sap.isa.auction.helpers.LoginHelper;

/**
 * @author I803067
 *
 * To change the template for this generated type comment go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
public class HeaderForm extends BaseForm  {
	public static final String ADMINTITLE = "HF.Title";
	public static final String SCHEDULETASKS = "HF.ScheduleTasks";
	
	public static final String LOGOFFCONFMSG	= "HF.LogoffMessage";	
	public static final String GREETINGUSER 	= "HF.GreetingUser";
	public static final String LOGOUT			= "HF.Logout";
	
	public String getUserName()  {
		LoginHelper loginHelper = (LoginHelper)getHelper(HelpManager.LOGINHELPER);
		String user = loginHelper.getGreetingUserNameText();
		return user;		
	}
	
}
