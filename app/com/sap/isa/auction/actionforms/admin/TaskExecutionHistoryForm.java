/*
 * Created on Sep 4, 2003
 *
 * To change the template for this generated file go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
package com.sap.isa.auction.actionforms.admin;

import com.sap.isa.auction.actions.ActionForwards;
import com.sap.isa.auction.models.tablemodels.admin.TaskExecutionHistoryModel;
import com.sap.isa.core.util.WebUtil;
import com.sapportals.htmlb.enum.MessageType;
import com.sapportals.htmlb.table.TableView;

/**
 * Title:
 * Description:
 * @Copyright: 		Copyright (c) 2003
 * Company:
 * @author I802791
 *
 * To change the template for this generated type comment go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
public class TaskExecutionHistoryForm extends TaskDetailsForm {

	// Task execution history tab
	private TaskExecutionHistoryModel tableModel;
	private TableView historyTable;

	private boolean initalizedStage = true;

	public void initializeExecutionHistory() {

		entering("initializeExecutionHistory");
		if (tableModel == null)
			tableModel = new TaskExecutionHistoryModel(session);
		clearErrors();

		tableModel.setData(getTaskHelper().getJobExecutionHistory());
		if (getTaskHelper().getError() != null)
			setErrors(getTaskHelper().getError(), "", AUCTIONADMINBUNDLE);

		tableModel.setReferenceTarget(
			WebUtil.getAppsURL(
				servlet.getServletContext(),
				request,
				response,
				null,
				ActionForwards.SHOWLOADINGURL + "/aucadm/taskhistorylog.do",
				null,
				null,
				false),
			null);
		historyTable =
			(TableView) addControl(HTMLB_TABLEVIEW, HISTORYTABLEVIEW_ID);
		this.setAttribute(HISTORYTABLEMODEL_ID, tableModel);
		initalizedStage = true;
		exiting("initializeExecutionHistory");
	}

	public void clearExecutionHistory() {
		//clearDetailsErrors();
		getTaskHelper().clearJobExecutionHistory();
		if (getTaskHelper().getError() != null)
			//setDetailsError(getTaskHelper().getDetailsError(), "", "");
			setErrors(
				getTaskHelper().getDetailsError(),
				"",
				AUCTIONADMINBUNDLE);
		else {
			String message = translate(AUCTIONADMINBUNDLE, CLEARSUCCESSMSG);
			addError(createMessageBar(message, MessageType.INFO));
			tableModel.setData(null);
		}
	}

	public void refreshExecutionHistory() {
		//clearDetailsErrors();
		tableModel.setData(getTaskHelper().getJobExecutionHistory());
		if (getTaskHelper().getDetailsError() != null)
			//setDetailsError(getTaskHelper().getDetailsError(), "", "");
			setErrors(
				getTaskHelper().getDetailsError(),
				"",
				AUCTIONADMINBUNDLE);
	}
	public boolean showClearJobExecutionHistoryBtn() {
		return tableModel.getRowCount() > 0;
	}
}
