/*
 * Created on Sep 4, 2003
 *
 * To change the template for this generated file go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
package com.sap.isa.auction.actionforms.admin;

/**
 * Title:
 * Description:
 * @Copyright: 		Copyright (c) 2003
 * Company:
 * @author I802791
 *
 * To change the template for this generated type comment go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
public interface TaskDetailsFormConstants extends AdminFormConstants  {
	//I13N text
	public static final String GENERALTAB = "TDF.general";
	public static final String HISTORYTAB = "TDF.history";
	public static final String DETAILSTAB = "TDF.details";
	
	public static final String HISTORYTITLE = "TDF.HistoryTitle";
	public static final String DETAILSTITLE = "TDF.DetailsTitle";
	
	public static final String TIMEZONE = "TDF.Timezone";
	public static final String SCHEDULEDAT = "TDF.ScheduledAt";
	public static final String STARTTIME = "TDF.StartTime";
	public static final String STARTDATE = "TDF.StartDate";
//	public static final String REPEAT = "RepeatAt";
	public static final String STOPATABNORMAL = "TDF.StopAtAbnormal";
	public static final String PAUSE = "TDF.Pause";
	public static final String RESUME = "TDF.Resume";
	public static final String RUNNOW = "TDF.RunNow";
	public static final String SAVESCHEDULE = "TDF.SaveSchedule";
	
	public static final String DAYS = "TDF.Days";
//	public static final String HOURS = "Hours";
//	public static final String MINUTES = "Minutes";
	public static final String ONCEINEVERY = "TDF.OnceInEvery";
	public static final String WEEKS = "TDF.Weeks";
	public static final String ON = "TDF.On";
	public static final String PERIOD = "TDF.Period";
	public static final String ENDTIME = "TDF.Endtime";
	public static final String MINUTES = "TDF.Minutes";
	public static final String NORESULTSMESSAGE	= "TDF.NoRresult";
	
	public static final String STOPSUCCESSMSG = "TDF.stopsuccess";
	public static final String SUCCESSMSG = "TDF.success";
	public static final String CLEARSUCCESSMSG = "TDF.clearsuccess";
	public static final String LOGMESSAGES = "TDF.Log";
	public static final String BACK = "TDF.BackToHistory";
	public static final String CLEARBTN = "TDF.clear";
	public static final String REFRESHBTN = "TDF.refresh";
//	public static final String OFTHEMONTH = "OfTheMonth.TaskDetails";
//	public static final String DAY = "Day.TaskDetails";
//	public static final String THE = "The.TaskDetails";
//	public static final String JANUARY = "January.TaskDetails";
//	public static final String FEBRUARY = "February.TaskDetails";
//	public static final String MARCH = "March.TaskDetails";
//	public static final String APRIL = "April.TaskDetails";
//	public static final String MAY = "May.TaskDetails";
//	public static final String JUNE = "June.TaskDetails";
//	public static final String JULY = "July.TaskDetails";
//	public static final String AUGUST = "August.TaskDetails";
//	public static final String SEPTEMBER = "September.TaskDetails";
//	public static final String OCTOBER = "October.TaskDetails";
//	public static final String NOVEMBER = "Noverber.TaskDetails";
//	public static final String DECEMBER = "December.TaskDetails";
//	public static final String SELECTMONTHBTN = "TDF.selectMonth.TaskDetails";
//	
//	public static final String EVERY = "Every.TaskDetails";	
//	public static final String UNTIL = "Untill.TaskDetails";	
//	public static final String TIME = "Time.TaskDetails";
//	public static final String DURATION = "Duration.TaskDetails";
	
	//Htmlb component ID
	public static final String DESCRIPTION_ID = "DESCRIPTION_ID.TaskDetails";
	public static final String TIMEZONE_ID = "TIMEZONE_ID.TaskDetails";
	public static final String TYPELISTBOX_ID = "TYPELISTBOX_ID.TaskDetails";
	public static final String TYPELISTMODEL_ID = "TYPELISTMODEL_ID.TaskDetails";
	public static final String STARTTIMEINPUT_ID = "STARTTIMEINPUT_ID.TaskDetails";
	public static final String STARTDATEINPUT_ID = "STARTDATEINPUT_ID.TaskDetails";
	public static final String ENDTIMEINPUT_ID = "ENDTIMEINPUT_ID.TaskDetails";
	public static final String PERIODINPUT_ID = "PERIODINPUT_ID.TaskDetails";
	public static final String MAXOCCURANCEINPUT_ID = "MAXOCCURANCEINPUT_ID.TaskDetails";

	public static final String DAYSINPUT_ID = "DAYSINPUT_ID.TaskDetails";
//	public static final String REPEATRBTNGRP_ID = "REPEATRBTNGRP_ID.TaskDetails";
		
	public static final String WEEKSONINPUT_ID = "WEEKSONINPUT_ID.TaskDetails";
	public static final String MONDAY_ID = "MONDAY_ID.TaskDetails";
	public static final String TUESDAY_ID = "TUESDAY_ID.TaskDetails";
	public static final String WEDNESDAY_ID = "WEDNESDAY_ID.TaskDetails";
	public static final String THURSDAY_ID = "THURSDAY_ID.TaskDetails";
	public static final String FRIDAY_ID = "FRIDAY_ID.TaskDetails";
	public static final String SATURDAY_ID = "SATURDAY_ID.TaskDetails";
	public static final String SUNDAY_ID = "SUNDAY_ID.TaskDetails";
	
//	public static final String DAYOFTHEMONTHINPUT_ID = "DAYOFTHEMONTHINPUT_ID.TaskDetails";
//	public static final String WEEKDAYSLISTBOX_ID = "WEEKDAYSLISTBOX_ID.TaskDetails";
//	public static final String WEEKDAYSLISTMODEL_ID = "WEEKDAYSLISTMODEL_ID.TaskDetails";
//	public static final String NUMBERINGLISTBOX_ID = "NUMBERINGLISTBOX_ID.TaskDetails";
//	public static final String NUMBERINGLISTMODEL_ID = "NUMBERINGLISTMODEL_ID.TaskDetails";
//	public static final String SELECTMONTHBTN_ID = "SELECTMONTHBTN_ID.TaskDetails";
//	public static final String JANUARY_ID = "JANUARY_ID.TaskDetails";
//	public static final String FEBRUARY_ID = "FEBRUARY_ID.TaskDetails";
//	public static final String MARCH_ID = "MARCH_ID.TaskDetails";
//	public static final String APRIL_ID = "APRIL_ID.TaskDetails";
//	public static final String MAY_ID = "MAY_ID.TaskDetails";
//	public static final String JUNE_ID = "JUNE_ID.TaskDetails";
//	public static final String JULY_ID = "JULY_ID.TaskDetails";
//	public static final String AUGUST_ID = "AUGUST_ID.TaskDetails";
//	public static final String SEPTEMBER_ID = "SEPTEMBER_ID.TaskDetails";
//	public static final String OCTOBER_ID = "OCTOBER_ID.TaskDetails";
//	public static final String NOVEMBER_ID = "NOVEMBER_ID.TaskDetails";
//	public static final String DECEMBER_ID = "DECEMBER_ID.TaskDetails";
//	public static final String MONTHRBTNGRP_ID = "MONTHRBTNGRP_ID.TaskDetails";
//	public static final String DAYOFTHEMONTHRADIOBTN_ID = "DAYOFTHEMONTHRADIOBTN_ID.TaskDetails";
//	public static final String WEEKDAYSRADIOBTN_ID = "DAYOFTHEMONTHRADIOBTN_ID.TaskDetails";
//	
//	public static final String REPEATFREQ_ID = "REPEATFREQ_ID.TaskDetails";
//	public static final String TIMEMEASURMENTLISTBOX_ID = "TIMELISTBOX_ID.TaskDetails";
//	public static final String TIMEMEASURMENTLISTMODEL_ID = "TIMELISTMODEL_ID.TaskDetails";
//	public static final String TIMERADIOBTN_ID = "TIMERADIOBTN_ID.TaskDetails";
//	public static final String DURATIONRADIOBTN_ID = "DURATIONRADIOBTN_ID";
//	public static final String REPEATENDTIMEINPUT_ID = "REPEATENDTIMEINPUT_ID.TaskDetails";
//	public static final String DURATIONHOURSINPUT_ID = "DURATIONHOURSINPUT_ID.TaskDetails";
//	public static final String DURATIONMINSINPUT_ID = "DURATIONMINSINPUT_ID.TaskDetails";
//	
//	public static final String REPEATCHECKBOX_ID = "REPEATCHECKBOX_ID.TaskDetails";
	
	public static final String STOPATABNORMALCHECKBOX_ID = "STOPATABNORMALCHECKBOX_ID.TaskDetails";
	public static final String PAUSEBTN_ID = "PAUSEBTN_ID.TaskDetails";
	public static final String RESUMEBTN_ID = "RESUMEBTN_ID.TaskDetails";
	public static final String RUNNOWBTN_ID = "RUNNOWBTN_ID.TaskDetails";	
	public static final String SAVEBTN_ID = "SAVEBTN_ID.TaskDetails";
	public static final String CANCELBTN_ID = "CANCELBTN_ID.TaskDetails";
	public static final String EDITBTN_ID = "EDITBTN_ID.TaskDetails";
	public static final String CLEARBTN_ID = "CLEARBTN_ID.TaskDetails";
	public static final String REFRESHBTN_ID = "REFRESHBTN_ID.TaskDetails";
	
	public static final String HISTORYTABLEMODEL_ID = "HISTORYTABLEMODEL_ID.TaskDetails";
	public static final String HISTORYTABLEVIEW_ID = "HISTORYTABLEVIEW_ID.TaskDetails";
	public static final String RECORDSTARTTIME_ID = "RECORDSTARTTIME_ID.TaskDetails";
	public static final String RECORDENDTIME_ID = "RECORDENDTIME_ID.TaskDetails";
	public static final String LOGMESSAGE_ID = "LOGMESSAGE_ID.TaskDetails";
}
