/*
 * Created on Sep 4, 2003
 *
 * To change the template for this generated file go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
package com.sap.isa.auction.actionforms.admin;

import java.text.ParseException;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;

import com.sap.isa.auction.actionforms.BaseForm;
import com.sap.isa.auction.helpers.HelpManager;
import com.sap.isa.auction.helpers.SchedulerTaskHelper;
import com.sap.isa.services.schedulerservice.CronJob;
import com.sap.isa.services.schedulerservice.CronJobHelper;
import com.sap.isa.services.schedulerservice.Job;
import com.sap.isa.services.schedulerservice.ui.model.ScheduledTaskTypeListConstants;
import com.sap.isa.services.schedulerservice.ui.model.ScheduledTaskTypeListModel;
import com.sap.isa.services.schedulerservice.ui.model.TaskTableConstants;
import com.sap.isa.services.schedulerservice.ui.model.WeekdaysListConstants;
import com.sapportals.htmlb.Checkbox;
import com.sapportals.htmlb.DropdownListBox;
import com.sapportals.htmlb.InputField;
import com.sapportals.htmlb.type.DataDate;
import com.sapportals.htmlb.type.DataTime;
import com.sapportals.htmlb.type.Time;

/**
 * Title:
 * Description:
 * @Copyright: 		Copyright (c) 2003
 * Company:
 * @author I802791
 *
 * To change the template for this generated type comment go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
public class TaskDetailsForm
	extends BaseForm
	implements TaskDetailsFormConstants {

	//	Common UI elements
	private InputField taskDesc;
	private InputField startDateInput;
	private InputField startTimeInput;
	private String timezone;

	private Checkbox stopAtAbnormalCheckbox;

	private DropdownListBox taskTypeListBox;
	private ScheduledTaskTypeListModel taskTypeListModel;

	//Regularly Task Specific UI elements
	private InputField periodInput;
	//private InputField endTimeInput;
	//	Daily Task Specific UI elements
	private InputField daysInput;

	//	Weekly Task Sepcific UI elements
	private InputField weeksOnInput;
	private Checkbox mondayCheckbox;
	private Checkbox tuesdayCheckbox;
	private Checkbox wednesdayCheckbox;
	private Checkbox thursdayCheckbox;
	private Checkbox fridayCheckbox;
	private Checkbox saturdayCheckbox;
	private Checkbox sundayCheckbox;

	//	Monthly Task Specific UI elements

	//	Once Task Specific UI elements

	private CronJob cronJob;
	protected Job job;
	private SchedulerTaskHelper helper;
	private CronJobHelper cronHelper;
	private String jobType = ScheduledTaskTypeListConstants.REGULARLY_ID;

	public void initializeByJobId(String jobId) {
		initializeFields();
		clearErrors();
		//clearDetailsErrors();
		job = getTaskHelper().getJobById(jobId);
		if (getTaskHelper().getDetailsError() != null) {
			//setDetailsError(getTaskHelper().getDetailsError(), "", "");
			setErrors(
				getTaskHelper().getDetailsError(),
				"",
				AUCTIONADMINBUNDLE);
			job = getTaskHelper().getErrorJob();
		}
		populateJobDetails();
		if (getTaskHelper().getError() != null)
			setErrors(getTaskHelper().getError(), "", AUCTIONADMINBUNDLE);

		setEditable(isEditMode());
		exiting("TaskDetailsForm initialize()");
	}

	public void initializeByRowId(String rowId) {
		initializeFields();
		//clearDetailsErrors();
		if (getTaskHelper().getDetailsError() != null) {
			//setDetailsError(getTaskHelper().getDetailsError(), "", "");
			setErrors(
				getTaskHelper().getDetailsError(),
				"",
				AUCTIONADMINBUNDLE);
		}
		if (getTaskHelper().getError() != null)
			setErrors(getTaskHelper().getError(), "", AUCTIONADMINBUNDLE);
		job = getTaskHelper().getJobByRowId(rowId);
		populateJobDetails();
		setEditable(isEditMode());
		exiting("TaskDetailsForm initialize()");
	}

	private void initializeFields() {
		entering("TaskDetailsForm initialize()");
		//---- Common UI elements
		taskDesc = new InputField();
		startTimeInput = new InputField();
		startDateInput = new InputField();
		taskTypeListBox = new DropdownListBox(TYPELISTBOX_ID);
		taskTypeListModel = new ScheduledTaskTypeListModel(session);

		this.setAttribute(STARTTIMEINPUT_ID, startTimeInput);
		this.setAttribute(STARTDATEINPUT_ID, startDateInput);
		this.setAttribute(DESCRIPTION_ID, taskDesc);
		this.setAttribute(TYPELISTBOX_ID, taskTypeListBox);
		this.setAttribute(TYPELISTMODEL_ID, taskTypeListModel);
		//----- Regularly Task Specific UI elements
		periodInput = new InputField();
		//endTimeInput = new InputField();

		//this.setAttribute(ENDTIMEINPUT_ID, endTimeInput);
		this.setAttribute(PERIODINPUT_ID, periodInput);
		//----- Daily Task Specific UI elements

		daysInput = new InputField();
		this.setAttribute(DAYSINPUT_ID, daysInput);

		//----- Weekly Task Sepcific UI elements
		weeksOnInput = new InputField();
		mondayCheckbox = new Checkbox(MONDAY_ID);
		tuesdayCheckbox = new Checkbox(TUESDAY_ID);
		wednesdayCheckbox = new Checkbox(WEDNESDAY_ID);
		thursdayCheckbox = new Checkbox(THURSDAY_ID);
		fridayCheckbox = new Checkbox(FRIDAY_ID);
		saturdayCheckbox = new Checkbox(SATURDAY_ID);
		sundayCheckbox = new Checkbox(SUNDAY_ID);

		mondayCheckbox.setText(
			translate(AUCTIONADMINBUNDLE, WeekdaysListConstants.MONDAY));
		tuesdayCheckbox.setText(
			translate(AUCTIONADMINBUNDLE, WeekdaysListConstants.TUESDAY));
		wednesdayCheckbox.setText(
			translate(AUCTIONADMINBUNDLE, WeekdaysListConstants.WEDNESDAY));
		thursdayCheckbox.setText(
			translate(AUCTIONADMINBUNDLE, WeekdaysListConstants.THURSDAY));
		fridayCheckbox.setText(
			translate(AUCTIONADMINBUNDLE, WeekdaysListConstants.FRIDAY));
		saturdayCheckbox.setText(
			translate(AUCTIONADMINBUNDLE, WeekdaysListConstants.SATURDAY));
		sundayCheckbox.setText(
			translate(AUCTIONADMINBUNDLE, WeekdaysListConstants.SUNDAY));

		this.setAttribute(WEEKSONINPUT_ID, weeksOnInput);
		this.setAttribute(MONDAY_ID, mondayCheckbox);
		this.setAttribute(TUESDAY_ID, tuesdayCheckbox);
		this.setAttribute(WEDNESDAY_ID, wednesdayCheckbox);
		this.setAttribute(THURSDAY_ID, thursdayCheckbox);
		this.setAttribute(FRIDAY_ID, fridayCheckbox);
		this.setAttribute(SATURDAY_ID, saturdayCheckbox);
		this.setAttribute(SUNDAY_ID, sundayCheckbox);
		//----- Monthly Task Specific UI elements

		//Once Task Specific UI elements		
	}

	public boolean isEditMode() {

		if (getTaskHelper().getMode() == SchedulerTaskHelper.READONLY_MODE)
			return false;
		else
			return true;
	}

	public boolean isScheduled() {
		boolean scheduled = true;
		if (job != null)
			scheduled = !job.isVirgin() && !job.isStopped();
		else
			scheduled = false;
		return scheduled;
	}

	private void setEditable(boolean editable) {
		taskDesc.setEnabled(editable);
		if (editable && !isScheduled()) {
			startTimeInput.setEnabled(editable);
			startDateInput.setEnabled(editable);
		} else {
			startTimeInput.setEnabled(false);
			startDateInput.setEnabled(false);
		}

		//endTimeInput.setEnabled(editable);
		periodInput.setEnabled(editable);
		taskTypeListBox.setEnabled(editable);
		daysInput.setEnabled(editable);
		weeksOnInput.setEnabled(editable);
		mondayCheckbox.setEnabled(editable);
		tuesdayCheckbox.setEnabled(editable);
		wednesdayCheckbox.setEnabled(editable);
		thursdayCheckbox.setEnabled(editable);
		fridayCheckbox.setEnabled(editable);
		saturdayCheckbox.setEnabled(editable);
		sundayCheckbox.setEnabled(editable);
	}
	/**
	 * 
	 */
	private void populateJobDetails() {
		if (job != null) {
			DataDate htmlbDate = new DataDate(job.getStartDate());
			startDateInput.setDate(htmlbDate);
			DataTime startTime = null;
			if (job instanceof CronJob) {
				cronJob = (CronJob) job;
				logInfo("set Cron String");
				taskDesc.setValue(
					getCronJobHelper(cronJob).getScheduleString());
				timezone =
					cronJob.getTimeZone().getDisplayName(false, TimeZone.SHORT);

				if (cronJob.isDailySchedule()) {
					logInfo("Set Daily Task related fields");
					long days =
						getCronJobHelper(cronJob)
							.getDailySchedule()
							.getRepeatDays();
					int hrs =
						getCronJobHelper(cronJob)
							.getDailySchedule()
							.getStartTimeHr();
					int mins =
						getCronJobHelper(cronJob)
							.getDailySchedule()
							.getStartTimeMin();
					startTime = new DataTime(new Time(hrs, mins, 0));
					taskTypeListBox.setSelection(
						ScheduledTaskTypeListConstants.DAILY_ID);
					taskTypeListModel.setSelection(
						ScheduledTaskTypeListConstants.DAILY_ID);
					jobType = ScheduledTaskTypeListConstants.DAILY_ID;
					daysInput.setValue(String.valueOf(days));

				} else if (cronJob.isWeeklySchedule()) {
					logInfo("Set Weekly Task related fields");
					long weeks =
						getCronJobHelper(cronJob)
							.getWeeklySchedule()
							.getRepeatWeeks();
					int hrs =
						getCronJobHelper(cronJob)
							.getWeeklySchedule()
							.getStartTimeHr();
					int mins =
						getCronJobHelper(cronJob)
							.getWeeklySchedule()
							.getStartTimeMin();
					startTime = new DataTime(new Time(hrs, mins, 0));
					taskTypeListBox.setSelection(
						ScheduledTaskTypeListConstants.WEEKLY_ID);
					taskTypeListModel.setSelection(
						ScheduledTaskTypeListConstants.WEEKLY_ID);
					jobType = ScheduledTaskTypeListConstants.WEEKLY_ID;
					weeksOnInput.setValue(String.valueOf(weeks));
					boolean[] daysOfWeek =
						getCronJobHelper(cronJob)
							.getWeeklySchedule()
							.getEveryDayOfWeek();

					if (daysOfWeek != null && daysOfWeek.length == 7) {
						sundayCheckbox.setChecked(daysOfWeek[0]);
						mondayCheckbox.setChecked(daysOfWeek[1]);
						tuesdayCheckbox.setChecked(daysOfWeek[2]);
						wednesdayCheckbox.setChecked(daysOfWeek[3]);
						thursdayCheckbox.setChecked(daysOfWeek[4]);
						fridayCheckbox.setChecked(daysOfWeek[5]);
						saturdayCheckbox.setChecked(daysOfWeek[6]);
					} //end if (daysOfWeek != null)
				} //end else if (job.isWeeklySchedule())	
			} //end if (job instanceof CronJob)

			else {
				taskTypeListBox.setSelection(
					ScheduledTaskTypeListConstants.REGULARLY_ID);
				taskTypeListModel.setSelection(
					ScheduledTaskTypeListConstants.REGULARLY_ID);
				long period = (long) (job.getPeriod() / (60 * 1000));
				periodInput.setValue(String.valueOf(period));
				timezone = getConversionHelper().getServerTimeZone();
				//				if (job.getEndTime() != -1) {
				//					DataTime endTime = new DataTime(new Time(job.getEndTime()));
				//					endTimeInput.setTime(endTime);
				//				}

				jobType = ScheduledTaskTypeListConstants.REGULARLY_ID;
				startTime = new DataTime(new Time(job.getStartTime()));
			}
			startTimeInput.setTime(startTime);
		}

	}

	public String getServerTimeZone() {
		return timezone;
	}
	public boolean save() {

		entering("TaskDetailsForm save()");

		//get task type related values
		logInfo("Get common task values");
		String desc =
			((InputField) this.getComponentForId(DESCRIPTION_ID))
				.getString()
				.getValueAsString();
		Date startDate =
			((InputField) this.getComponentForId(STARTDATEINPUT_ID))
				.getDate()
				.getValue()
				.getUtilDate();

		Time startTime =
			((InputField) this.getComponentForId(STARTTIMEINPUT_ID))
				.getTime()
				.getValueAsTime();
		String typeId =
			((DropdownListBox) this.getComponentForId(TYPELISTBOX_ID))
				.getSelection();
		//clearDetailsErrors();

		// save daily job
		try {
			if (typeId.equals(ScheduledTaskTypeListConstants.DAILY_ID)) {
				logInfo("Get daily type task related values");
				long repeatDays = 0;
				String numOfDaysStr =
					((InputField) this.getComponentForId(DAYSINPUT_ID))
						.getString()
						.getValueAsString();
				if (numOfDaysStr != null && !numOfDaysStr.equals("")) {
					repeatDays = Long.parseLong(numOfDaysStr);
				}
				CronJobHelper.DailySchedule dailySchedule =
					getCronJobHelper(job).new DailySchedule();
				dailySchedule.setRepeatDays(repeatDays);
				if (!isScheduled()) {
					dailySchedule.setStartTimeHr(startTime.getHour());
					dailySchedule.setStartTimeMin(startTime.getMinute());
				}

				try {
					job = dailySchedule.getCronJob();
				} catch (ParseException ex) {
					logInfo("ParseException occured: " + ex);
					setErrors(ex, "", AUCTIONADMINBUNDLE);
				} catch (Exception ex) {
					logInfo("ParseException occured: " + ex);
					setErrors(ex, "", AUCTIONADMINBUNDLE);
				}
			} else if (
				typeId.equals(ScheduledTaskTypeListConstants.WEEKLY_ID)) {
				logInfo("Get weekly type task related values");
				long repeatWeeks = 0;
				String numOfWeeksStr =
					((InputField) this.getComponentForId(WEEKSONINPUT_ID))
						.getString()
						.getValueAsString();
				if (numOfWeeksStr != null && !numOfWeeksStr.equals("")) {
					repeatWeeks = Long.parseLong(numOfWeeksStr);
				}
				CronJobHelper.WeeklySchedule weeklySchedule =
					getCronJobHelper(job).new WeeklySchedule();
				if (!isScheduled()) {
					weeklySchedule.setStartTimeHr(startTime.getHour());
					weeklySchedule.setStartTimeMin(startTime.getMinute());
				}
				weeklySchedule.setRepeatWeeks(repeatWeeks);
				boolean[] daysOfWeek = weeklySchedule.getEveryDayOfWeek();
				if (((Checkbox) this.getComponentForId(MONDAY_ID)).isChecked())
					daysOfWeek[1] = true;

				if (((Checkbox) this.getComponentForId(TUESDAY_ID))
					.isChecked()) {
					daysOfWeek[2] = true;
				}
				if (((Checkbox) this.getComponentForId(WEDNESDAY_ID))
					.isChecked()) {
					daysOfWeek[3] = true;
				}
				if (((Checkbox) this.getComponentForId(THURSDAY_ID))
					.isChecked()) {
					daysOfWeek[4] = true;
				}
				if (((Checkbox) this.getComponentForId(FRIDAY_ID))
					.isChecked()) {
					daysOfWeek[5] = true;
				}
				if (((Checkbox) this.getComponentForId(SATURDAY_ID))
					.isChecked()) {
					daysOfWeek[6] = true;
				}
				if (!((Checkbox) this.getComponentForId(SUNDAY_ID))
					.isChecked()) {
					daysOfWeek[0] = false;
				}
				weeklySchedule.setEveryDayOfWeek(daysOfWeek);
				try {
					job = weeklySchedule.getCronJob();
				} catch (ParseException ex) {
					logInfo("ParseException occured: " + ex);
					setErrors(ex, "", AUCTIONADMINBUNDLE);
				} catch (Exception ex) {
					logInfo("Exception occured: " + ex);
					setErrors(ex, "", AUCTIONADMINBUNDLE);
				}
			} else if (
				typeId.equals(ScheduledTaskTypeListConstants.REGULARLY_ID)) {
				logInfo("set start time for regular job");
				String period =
					((InputField) this.getComponentForId(PERIODINPUT_ID))
						.getString()
						.getValueAsString();
				//			Time endTime =
				//				((InputField) this.getComponentForId(ENDTIMEINPUT_ID))
				//					.getTime()
				//					.getValueAsTime();
				long periodL = 0;
				if (!period.trim().equals(""))
					periodL = Long.parseLong(period.trim()) * 60 * 1000;
				//period in mini seconds
				if (job instanceof CronJob) {
					cronJob = (CronJob) job;
					job =
						CronJobHelper.convertCronJobToRegularJob(
							cronJob,
							periodL);
				} else
					job.setPeriod(periodL);
				//job.setStartTime(startTime.getUtilDate().getTime());
				//			if (endTime.getUtilDate().after(startTime.getUtilDate())){
				//				job.setEndDate(endTime.getUtilDate());
				//			}
				if (!isScheduled()) {
					Calendar cal = Calendar.getInstance();
					cal.setTime(startDate);
					cal.set(Calendar.HOUR, startTime.getHour());
					cal.set(Calendar.MINUTE, startTime.getMinute());
					cal.set(Calendar.SECOND, startTime.getSecond());
					job.setStartTime(cal.getTime().getTime());
				}

			}

			boolean flag = getTaskHelper().save(job);
			exiting("TaskDetailsForm save()");
			return flag;
		} catch (IllegalArgumentException ex) {
			getTaskHelper().setTaskDetailsError(ex, job);
			return false;
		}

	}
	public void edit() {
		//clearDetailsErrors();
		getTaskHelper().prepareForModification();
	}
	public void cancel() {
		//clearDetailsErrors();
		getTaskHelper().cancel();
	}

	public boolean pause() {
		//clearDetailsErrors();
		boolean flag = getTaskHelper().pause(job);
		return flag;
	}
	public boolean resume() {
		//clearDetailsErrors();
		boolean flag = getTaskHelper().resume(job);
		return flag;
	}
	public boolean runNow() {
		//clearDetailsErrors();
		boolean flag = getTaskHelper().runNow(job);
		return flag;
	}

	public boolean showPauseBtn() {
		boolean show = true;
		if (job != null && (job.isPaused() || job.isVirgin()))
			show = false;
		return show;
	}
	public boolean showResumeBtn() {
		boolean show = false;
		if (job != null && job.isPaused())
			show = true;
		return show;
	}

	public boolean showRunNowBtn() {
		return getTaskHelper().showRunNowBtn(job);
	}
	public String getTaskName() {
//		String taskClassName =
//			job.getName().equals(TaskTableConstants.UPDATEAUCTIONTASKCLASSNAME)
//				? TaskTableConstants.UPDATEAUCTIONWOWINNER
//				: job.getName();
		String taskClassName = job.getName();
		String taskName = translate(AUCTIONADMINBUNDLE, taskClassName);
		return taskName;
	}
	public String getJobType() {
		return jobType;
	}
	public String getJobStatus() {
		String status = "";
		if (job.isVirgin()) {
			status =
				translate(AUCTIONADMINBUNDLE, TaskTableConstants.NOTSCHEDULED);
		}
		if (job.isScheduled()) {
			if (getTaskHelper().isJobQueuedForExecution(job)) {
				status =
					translate(AUCTIONADMINBUNDLE, TaskTableConstants.QUEUED);
			} else {
				status =
					translate(AUCTIONADMINBUNDLE, TaskTableConstants.SCHEDULED);
			}
		} else if (job.isStopped()) {
			status = translate(AUCTIONADMINBUNDLE, TaskTableConstants.STOPPED);
		} else if (job.isRunning()) {
			status = translate(AUCTIONADMINBUNDLE, TaskTableConstants.RUNNING);
		} else if (job.isPaused()) {
			status = translate(AUCTIONADMINBUNDLE, TaskTableConstants.PAUSED);
		}
		return status;
	}
	protected SchedulerTaskHelper getTaskHelper() {
		if (helper == null) {
			logInfo("SchedulerTaskHelper is null, get one");
			helper =
				(SchedulerTaskHelper) getHelper(HelpManager.SCHEDULETASKHELPER);
		}
		return helper;
	}

	private CronJobHelper getCronJobHelper(Job job) {
		CronJobHelper cronHelper = null;
		clearErrors();
		try {
			cronHelper = new CronJobHelper(job);
		} catch (Exception ex) {
			logInfo("Unable to initialize the CronJobHelper: " + ex);
			setErrors(ex, "", AUCTIONADMINBUNDLE);
		}
		return cronHelper;
	}
	private CronJobHelper getCronJobHelper(CronJob job) {
		CronJobHelper cronHelper = null;
		clearErrors();
		try {
			cronHelper = new CronJobHelper(job);
		} catch (Exception ex) {
			logInfo("Unable to initialize the CronJobHelper: " + ex);
			setErrors(ex, "", AUCTIONADMINBUNDLE);
		}
		return cronHelper;
	}
}
