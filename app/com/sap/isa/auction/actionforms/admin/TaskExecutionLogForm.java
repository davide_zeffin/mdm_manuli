/*
 * Created on Sep 4, 2003
 *
 * To change the template for this generated file go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
package com.sap.isa.auction.actionforms.admin;

import java.sql.Timestamp;

import com.sap.isa.auction.actionforms.BaseForm;
import com.sap.isa.auction.helpers.HelpManager;
import com.sap.isa.auction.helpers.SchedulerTaskHelper;
import com.sap.isa.services.schedulerservice.JobExecutionHistoryRecord;
import com.sapportals.htmlb.ItemList;
import com.sapportals.htmlb.TextView;


/**
 * Title:
 * Description:
 * @Copyright: 		Copyright (c) 2003
 * Company:
 * @author I802791
 *
 * To change the template for this generated type comment go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
public class TaskExecutionLogForm
	extends BaseForm
	implements TaskDetailsFormConstants {


	private TextView recordStartTimeTxtView;
	private TextView recordEndTimeTxtView;
	private ItemList recordLog;
	private SchedulerTaskHelper helper;
	
	private JobExecutionHistoryRecord record;
	
	
	
	public void initializeExecutionDetails(int row){
		entering("initializeExecutionDetails");
		
		record = getTaskHelper().getJobExecutionHistoryRecord(row);
		recordStartTimeTxtView = (TextView)addControl(HTMLB_TEXTVIEW, RECORDSTARTTIME_ID);
		recordEndTimeTxtView = (TextView)addControl(HTMLB_TEXTVIEW, RECORDENDTIME_ID);
		recordLog = (ItemList)addControl(HTMLB_ITEMLIST, LOGMESSAGE_ID);		
		recordLog.setOrdered(false);
		if (record != null){
			logInfo("set job execution history details");
			long time = record.getStartTime();
			String timeStr = getConversionHelper().javaDatetoUILocaleSpecificString(new Timestamp(time));
			recordStartTimeTxtView.setText(timeStr);
			time = record.getEndTime();
			if(time > 0)
				timeStr = getConversionHelper().javaDatetoUILocaleSpecificString(new Timestamp(time));
			else 
				timeStr = ""; 
			recordEndTimeTxtView.setText(timeStr);
			String[] messages = record.getLogMessages();
			if (messages != null && messages.length>0){
				logInfo("set the logged messages");
				for (int i=0; i<messages.length; i++){
					TextView message = new TextView();
					message.setText(messages[i]);
					message.setWrapping(true);
					recordLog.addComponent(message);
				}
			}//end if (messages != null && messages.length>0)
		}//end if (record != null)
		exiting("initializeExecutionDetails");
	}
	
	private SchedulerTaskHelper getTaskHelper(){

		if (helper == null){
			logInfo("SchedulerTaskHelper is null, get one");
			helper = (SchedulerTaskHelper)getHelper(HelpManager.SCHEDULETASKHELPER);
		}
		return helper;
	}
}
