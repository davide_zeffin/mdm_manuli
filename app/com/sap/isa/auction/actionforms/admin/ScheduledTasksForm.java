/*
 * Created on Sep 4, 2003
 *
 * To change the template for this generated file go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
package com.sap.isa.auction.actionforms.admin;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Enumeration;

import com.sap.isa.auction.actionforms.BaseForm;
import com.sap.isa.auction.actions.ActionForwards;
import com.sap.isa.auction.helpers.HelpManager;
import com.sap.isa.auction.helpers.SchedulerTaskHelper;
import com.sap.isa.auction.models.tablemodels.admin.TaskTableModel;
import com.sap.isa.core.util.WebUtil;
import com.sap.isa.services.schedulerservice.Job;
import com.sapportals.htmlb.enum.MessageType;
import com.sapportals.htmlb.table.TableView;

/**
 * Title:
 * Description:
 * @Copyright: 		Copyright (c) 2003
 * Company:
 * @author I802791
 *
 * To change the template for this generated type comment go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
public class ScheduledTasksForm
	extends BaseForm
	implements ScheduledTasksFormConstants {

	private TableView tableView;
	private TaskTableModel tableModel;
	private String lassAccessedTaskId = null;
	private SchedulerTaskHelper helper;
	private boolean taskConfigExist = true;

	public void initializeFields() {
		entering("ScheduledTasksForm initialize");
		if (tableModel == null) {
			tableModel = new TaskTableModel(session, request.getContextPath());
			tableModel.setReferenceTarget(
				WebUtil.getAppsURL(
					servlet.getServletContext(),
					request,
					response,
					null,
					ActionForwards.SHOWLOADINGURL + "/aucadm/taskdetails.do",
					null,
					null,
					false),
				DETAILSTARGET);
		}
		clearErrors();
		Collection tasks = getTaskHelper().getScheduledJobs(null);

		//checkTaskConfigExist();
		if (getTaskHelper().getError() != null)
			/*&& getAdminHelper().getTKError() == null)*/
			setErrors(getTaskHelper().getError(), "", AUCTIONADMINBUNDLE);
		else if (getTaskHelper().getError() != null)
			/*&& getAdminHelper().getTKError() != null)*/
			setErrors(getTaskHelper().getError(), "", AUCTIONADMINBUNDLE);
		tableModel.setData(tasks);
		if (tableView == null)
			tableView = new TableView(TASKTABLEVIEW_ID);
		this.setAttribute(TASKTABLEMODEL_ID, tableModel);
		this.setAttribute(TASKTABLEVIEW_ID, tableView);
		exiting("ScheduledTasksForm initialize");
	}

	public String getLastAccessedTaskId() {
		return getTaskHelper().getLastAccessedJobId();
	}
	public String getLastAccessedRowId() {
		return getTaskHelper().getLastAccessedRowId();
	}
	public int getMode() {
		return getTaskHelper().getMode();
	}

	public void scheduleTasks() {
		entering("scheduleTasks");
		TableView oldView = (TableView) getComponentForId(TASKTABLEVIEW_ID);
		if (null != oldView) {
			tableModel.updateSelection(oldView);
		}
		Job job = null;
		Enumeration selection = tableModel.getSelectedRows();
		clearErrors();
		Collection jobs = new ArrayList();
		while (selection.hasMoreElements()) {
			int row = Integer.parseInt((String) selection.nextElement());
			job = (Job) tableModel.getValueAt(row);
			if (null == job) {
				logInfo("Schedule Tasks - Selected job is null" + row);
				continue;
			}
			job =
				job.getID() != null
					? getTaskHelper().getJobById(job.getID(), false)
					: getTaskHelper().getJobByRowId(String.valueOf(row));
			if (job.isVirgin() || job.isStopped())
				jobs.add(job);
		}
		if (!jobs.isEmpty()) {
			logInfo("To be scheduled jobs size >0, schedule");
			boolean flag = getTaskHelper().schedule(jobs);
			if (!flag)
				setErrors(getTaskHelper().getError(), "", AUCTIONADMINBUNDLE);
			Collection tasks = getTaskHelper().getScheduledJobs(null);
			//checkTaskConfigExist();
			tableModel.setData(tasks);
			if (flag && getTaskHelper().getError() != null)
				setErrors(getTaskHelper().getError(), "", AUCTIONADMINBUNDLE);
			else if (!flag && getTaskHelper().getError() != null)
				setErrors(getTaskHelper().getError(), "", AUCTIONADMINBUNDLE);
			else if (flag && getTaskHelper().getError() == null) {
				String message =
					translate(AUCTIONADMINBUNDLE, SCHEDULESUCCESSMSG);
				addError(createMessageBar(message, MessageType.INFO));
			}

		}
		tableModel.clearSelection();
		exiting("scheduleTasks");
	}

	public boolean isTaskConfigurationDone() {
		return taskConfigExist;
	}

	public void refresh() {
		clearErrors();
		tableModel.setData(getTaskHelper().getScheduledJobs(null));
		if (getTaskHelper().getError() != null)
			setErrors(getTaskHelper().getError(), "", AUCTIONADMINBUNDLE);
	}

	public void pauseTasks() {
		entering("pauseTasks");
		TableView oldView = (TableView) getComponentForId(TASKTABLEVIEW_ID);
		if (null != oldView) {
			tableModel.updateSelection(oldView);
		}
		Job job = null;
		Enumeration selection = tableModel.getSelectedRows();
		clearErrors();
		Collection jobs = new ArrayList();
		while (selection.hasMoreElements()) {
			int row = Integer.parseInt((String) selection.nextElement());
			job = (Job) tableModel.getValueAt(row);
			if (null == job) {
				logInfo("Pause Tasks - Selected job is null" + row);
				continue;
			}
			job =
				job.getID() != null
					? getTaskHelper().getJobById(job.getID(), false)
					: getTaskHelper().getJobByRowId(String.valueOf(row));
			if (job.isScheduled())
				jobs.add(job);
		}
		if (!jobs.isEmpty()) {
			logInfo("To be paused jobs size >0, pause jobs");
			boolean flag = getTaskHelper().pause(jobs);
			if (!flag)
				setErrors(getTaskHelper().getError(), "", AUCTIONADMINBUNDLE);
			Collection tasks = getTaskHelper().getScheduledJobs(null);
			//checkTaskConfigExist();
			tableModel.setData(tasks);
			if (flag && getTaskHelper().getError() != null)
				setErrors(getTaskHelper().getError(), "", AUCTIONADMINBUNDLE);
			else if (!flag && getTaskHelper().getError() != null)
				setErrors(getTaskHelper().getError(), "", AUCTIONADMINBUNDLE);
			else if (flag && getTaskHelper().getError() == null) {
				String message = translate(AUCTIONADMINBUNDLE, STOPSUCCESSMSG);
				addError(createMessageBar(message, MessageType.INFO));
			}

		}
		tableModel.clearSelection();
		exiting("pauseTasks");
	}

	public void resumeTasks() {
		entering("resumeTasks");
		TableView oldView = (TableView) getComponentForId(TASKTABLEVIEW_ID);
		if (null != oldView) {
			tableModel.updateSelection(oldView);
		}
		Job job = null;
		Enumeration selection = tableModel.getSelectedRows();
		clearErrors();
		Collection jobs = new ArrayList();
		while (selection.hasMoreElements()) {
			int row = Integer.parseInt((String) selection.nextElement());
			job = (Job) tableModel.getValueAt(row);
			if (null == job) {
				logInfo("Resume Tasks - Selected job is null" + row);
				continue;
			}
			job =
				job.getID() != null
					? getTaskHelper().getJobById(job.getID(), false)
					: getTaskHelper().getJobByRowId(String.valueOf(row));
			if (job.isPaused())
				jobs.add(job);
		}
		if (!jobs.isEmpty()) {
			logInfo("To be resumed jobs size >0, pause jobs");
			boolean flag = getTaskHelper().resume(jobs);
			if (!flag)
				setErrors(getTaskHelper().getError(), "", AUCTIONADMINBUNDLE);
			Collection tasks = getTaskHelper().getScheduledJobs(null);
			//checkTaskConfigExist();
			tableModel.setData(tasks);
			if (flag && getTaskHelper().getError() != null)
				setErrors(getTaskHelper().getError(), "", AUCTIONADMINBUNDLE);
			else if (!flag && getTaskHelper().getError() != null)
				setErrors(getTaskHelper().getError(), "", AUCTIONADMINBUNDLE);
			else if (flag && getTaskHelper().getError() == null) {
				String message =
					translate(AUCTIONADMINBUNDLE, RESUMESUCCESSMSG);
				addError(createMessageBar(message, MessageType.INFO));
			}

		}
		tableModel.clearSelection();
		exiting("resumeTasks");
	}

	public void runTasksNow() {
		entering("runTasksNow");
		TableView oldView = (TableView) getComponentForId(TASKTABLEVIEW_ID);
		if (null != oldView) {
			tableModel.updateSelection(oldView);
		}
		Job job = null;
		Enumeration selection = tableModel.getSelectedRows();
		clearErrors();
		Collection jobs = new ArrayList();
		while (selection.hasMoreElements()) {
			int row = Integer.parseInt((String) selection.nextElement());
			job = (Job) tableModel.getValueAt(row);
			if (null == job) {
				logInfo("Run Tasks now - Selected job is null" + row);
				continue;
			}
			job =
				job.getID() != null
					? getTaskHelper().getJobById(job.getID(), false)
					: getTaskHelper().getJobByRowId(String.valueOf(row));
			if (!job.isRunning())
				jobs.add(job);
		}
		if (!jobs.isEmpty()) {
			logInfo("To be run jobs size >0, run");
			boolean flag = getTaskHelper().runNow(jobs);
			if (!flag)
				setErrors(getTaskHelper().getError(), "", AUCTIONADMINBUNDLE);
			Collection tasks = getTaskHelper().getScheduledJobs(null);
			//checkTaskConfigExist();
			tableModel.setData(tasks);
			if (flag && getTaskHelper().getError() != null)
				setErrors(getTaskHelper().getError(), "", AUCTIONADMINBUNDLE);
			else if (!flag && getTaskHelper().getError() != null)
				setErrors(getTaskHelper().getError(), "", AUCTIONADMINBUNDLE);
			else if (flag && getTaskHelper().getError() == null) {
				String message =
					translate(AUCTIONADMINBUNDLE, RUNNOWSUCCESSMSG);
				addError(createMessageBar(message, MessageType.INFO));
			}

		}
		tableModel.clearSelection();
		exiting("runTasksNow");
	}

	/**
	 * Function used to determine if show the selection checkbox in the 
	 * tableview or not
	 *
	 */

	private SchedulerTaskHelper getTaskHelper() {
		if (helper == null) {
			logInfo("SchedulerTaskHelper is null, get one");
			helper =
				(SchedulerTaskHelper) getHelper(HelpManager.SCHEDULETASKHELPER);
		}
		return helper;
	}

}
