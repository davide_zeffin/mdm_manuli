/*
 * Created on Sep 4, 2003
 *
 * To change the template for this generated file go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
package com.sap.isa.auction.actionforms.admin;


/**
 * Title:
 * Description:
 * @Copyright: 		Copyright (c) 2003
 * Company:
 * @author I802791
 *
 * To change the template for this generated type comment go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
public interface ScheduledTasksFormConstants extends AdminFormConstants  {
//	I13N description
	public static final String TITLE = "STF.Title";
	public static final String NORESULTSMESSAGE	= "STF.Noresult";
	public static final String OVERVIEW = "STF.Overview";
	public static final String WEEKLYVIEW = "STF.Weeklyview";
	public static final String MESSAGE = "STF.MailSent";
	public static final String EMALADDRESSES = "STF.EmailAdds";
	public static final String REMINDER = "STF.Reminder";
	public static final String SCHEDULETASK = "STF.Schedule";
	public static final String STOPSUCCESSMSG = "STF.StopSuccess";
	public static final String RESUMESUCCESSMSG = "STF.ResumeSuccess";
	public static final String RUNNOWSUCCESSMSG = "STF.RunNowSuccess";
	public static final String SCHEDULESUCCESSMSG = "STF.ScheduleSuccess";

	//Htmlb Component ID
	public static final String WEEKLYVIEWBTN_ID =
		"WEEKLYVIEWBTN_ID.ScheduledTasks";
	public static final String EDITBTN_ID =
		"EDITBTN_ID.ScheduledTasks";
	public static final String TASKTABLEVIEW_ID = "TASKTABLEVIEW_ID.ScheduledTasks";
	public static final String TASKTABLEMODEL_ID = "TASKTABLEMODEL_ID.ScheduledTasks";
	public static final String SCHEDULEBTN_ID = "SCHEDULEBTN_ID.ScheduledTasks";
	public static final String PAUSEBTN_ID = "PAUSEBTN_ID.ScheduledTasks";
	public static final String RESUMEBTN_ID = "RESUMEBTN_ID.ScheduledTasks";	
	public static final String RUNNOWBTN_ID = "RUNNOWBTN_ID.ScheduledTasks";
	public static final String REFRESHBTN_ID = "REFRESHBTN_ID.ScheduledTasks";
}
