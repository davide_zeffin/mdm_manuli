/*
 * Created on Aug 25, 2003
 *
 * To change the template for this generated file go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
package com.sap.isa.auction.actionforms.admin;

/**
 * Title:
 * Description:
 * @Copyright: 		Copyright (c) 2003
 * Company:
 * @author I802791
 *
 * To change the template for this generated type comment go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
public interface AdminFormConstants {
	//	I13N description
	public static final String SEARCHBTN = "AF.Search";
	public static final String CREATEBTN = "AF.Create";
	public static final String CANCELBTN = "AF.Cancel";
	public static final String SAVEBTN = "AF.Save";
	public static final String EDITBTN = "AF.Edit";
	public static final String DELETEBTN = "AF.Delete";
	
	public static final String NAMELENGTHMSG = "AF.NameLength";
	public static final String DESCRIITIONLENGTHMSG = "AF.DescLength";
	
	//Details link target frame
	public static final String DETAILSTARGET = "Details";
}
