/*
 * Created on Apr 29, 2004
 *
 * To change the template for this generated file go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
package com.sap.isa.auction.actionforms;

/**
 * @author I803067
 *
 * To change the template for this generated type comment go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
public interface  LoginFormConstants {
	
	public static final String USERID		= "Userid";
	public static final String USERIDLABEL 	= "LF.UserID";
	public static final String PASSWD		= "Passwd";
	public static final String PASSWDLABEL	= "LF.Passwd";
	
	public static final String LOGINTEXT	= "LF.Login";
	public static final String LOGINTITLE	= "LF.LoginTitle";
	
	public static final String WELCOMETITLE1 = "LF.Welcome1";
	public static final String WELCOMETITLE2 = "LF.Welcome2";
	public static final String GREETING		 = "LF.Greeting";
	
	public static final String LOGINEVENT		= "LoginEvent";
	public static final String LOGINBUTTONLABEL	= "LF.LoginBtn";
	
	public static final String USERTYPE	 = "userType";
	public static final String SHOPID	 = "shop";
}
