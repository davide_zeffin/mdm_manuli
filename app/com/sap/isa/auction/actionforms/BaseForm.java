/*
 * Created on Apr 26, 2004
 *
 * To change the template for this generated file go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
package com.sap.isa.auction.actionforms;

import com.sap.isa.auction.businessobject.ContextManager;
import com.sap.isa.auction.businessobject.UserRoleTypeEnum;
import com.sap.isa.auction.businessobject.b2x.InvocationContext;
import com.sap.isa.auction.businessobject.b2x.ProcessBusinessObjectManager;
import com.sap.isa.auction.businessobject.management.ProcessMetaBusinessObjectManager;
import com.sap.isa.auction.helpers.CatalogHelper;
import com.sap.isa.auction.helpers.HelpManager;
import com.sap.isa.auction.helpers.HelpManagerBase;
import com.sap.isa.auction.helpers.LoginHelper;
import com.sap.isa.auction.logging.LogHelper;
import com.sap.isa.core.Constants;
import com.sap.isa.core.SessionConst;
import com.sap.isa.core.UserSessionData;
import com.sap.isa.core.util.StartupParameter;

/**
 * @author I803067
 *
 * To change the template for this generated type comment go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
public class BaseForm extends Form implements BaseConstants  {
		
	public String translate(String key)  {
		String text = key;

		if(this.getClass().getName().indexOf(".seller.") > 0)  {
			return translate(AUCTIONSELLBUNDLE , key);
		}  else if(this.getClass().getName().indexOf(".admin.") > 0)  {
			return translate(AUCTIONADMINBUNDLE , key);
		}  else   {
			return super.translate(key);
		}
	}
	protected com.sap.isa.auction.businessobject.InvocationContext createInvocationContext(UserSessionData userData)  {
		StartupParameter startParam =
			(StartupParameter) userData.getAttribute(
				SessionConst.STARTUP_PARAMETER);
		String scenarioName =
			startParam.getParameterValue(Constants.XCM_SCENARIO_RP);
		ProcessBusinessObjectManager pbom = null;
		try {
			pbom = (ProcessBusinessObjectManager) ProcessMetaBusinessObjectManager
					.getInstance(scenarioName)
						.getBOM(ProcessBusinessObjectManager.class);
		} catch (Exception e) {
			LogHelper.logError(cat , location , 
				"Unable to get ProcessBusinessObjectManager for" + scenarioName , e);
		}
		return new InvocationContext(pbom);
	}
	protected void setInvocationContext(UserSessionData userData)  {
		InvocationContext context = (InvocationContext) createInvocationContext(userData);
		
		HelpManagerBase helpManager = (HelpManagerBase)userData.getAttribute(HelpManagerBase.HELPMANAGER);
		if(helpManager != null)  {
			LoginHelper helper = (LoginHelper)helpManager.getHelper(HelpManagerBase.LOGINHELPER);
			context.setUser(helper.getUser());
			CatalogHelper catalogHelper = (CatalogHelper)helpManager.getHelper(HelpManager.CATALOGHELPER);
			context.setShopId(catalogHelper.getShop());
			context.setRoleType(UserRoleTypeEnum.SELLER);
			ContextManager.bindContext(context);
		}
	}	
}

