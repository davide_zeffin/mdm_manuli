/*
 * Created on Mar 25, 2004
 *
 * To change the template for this generated file go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
package com.sap.isa.auction.backend.exception;

import java.util.*;

/**
 * @author I803746
 *
 * To change the template for this generated type comment go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
public class BackendUserError {
	private List errorMessages = new ArrayList();
	private List warningMessages = new ArrayList();

	
	public void addErrorMessage(String text) {
		errorMessages.add(text);
	}
	
	public void addWarningMessage(String text) {
		warningMessages.add(text);
	}
	
	public List getErrorMessages() {
		return Collections.unmodifiableList(errorMessages);
	}
	
	public List getWarningMessages() {
		return Collections.unmodifiableList(warningMessages);
	}
	
	public List getAllMessages() {
		List dst = new ArrayList();
		Collections.copy(dst, errorMessages);
		dst.addAll(warningMessages);
		return Collections.unmodifiableList(dst);
	}
	
	public boolean isSerious() {
		return (errorMessages.size()>0);
	}

}
