/*
 * Created on Mar 26, 2004
 *
 * To change the template for this generated file go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
package com.sap.isa.auction.backend.exception;

import java.util.List;

import com.sap.isa.businessobject.CommunicationException;

/**
 * @author I803746
 *
 * To change the template for this generated type comment go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
public class CommunicationBackendUserException
	extends CommunicationException
	implements BackendUserExceptionBase {

		BackendUserError error;

		/**
		 * @param arg0
		 */
		public CommunicationBackendUserException(BackendUserExceptionBase arg0) {
			super();
			error = arg0.getBackendUserError();
		}
	
		public void addErrorMessage(String text) {
			error.addErrorMessage(text);
		}
	
		public void addWarningMessage(String text) {
			error.addWarningMessage(text);
		}
	
		public List getErrorMessages() {
			return error.getErrorMessages();
		}
	
		public List getWarningMessages() {
			return error.getWarningMessages();
		}
	
		public List getAllMessages() {
			return error.getAllMessages();
		}
	
		public boolean isSerious() {
			return error.isSerious();
		}

		/* (non-Javadoc)
		 * @see com.sap.isa.auction.backend.exception.BackendUserExceptionBase#getBackendUserError()
		 */
		public BackendUserError getBackendUserError() {
			return error;
		}
		/**
		 * Overwritten the getMessage of the exception so that the
		 * business error messages are written.
		 */
		public String getMessage(){
			String returnStr=super.getMessage();
			if(returnStr==null) returnStr="";
			List errors = getErrorMessages();
			for(int i=0; i<errors.size(); i++){
				returnStr= returnStr+". " + (String)errors.get(i);
			}
			return returnStr;
		}		
}
