/*
 * Created on Mar 31, 2004
 *
 * To change the template for this generated file go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
package com.sap.isa.auction.backend.exception;

import com.sap.isa.auction.exception.SystemException;
import com.sap.isa.auction.exception.i18n.ErrorMessageBundle;
import com.sap.isa.auction.exception.i18n.I18nErrorMessage;

/**
 * @author I803746
 *
 * To change the template for this generated type comment go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
public class InvalidAddressException extends SystemException {
	private static final String EXC_ID = "InvalidAddressException";

	public static final int ADDRESS_NULL_ID = 1;
	public static final String ADDRESS_NULL = EXC_ID + ADDRESS_NULL_ID;
	public static final int NAME_NULL_ID = 2;
	public static final String NAME_NULL = EXC_ID + NAME_NULL_ID;
	public static final int STREET_NULL_ID = 3;
	public static final String STREET_NULL = EXC_ID + STREET_NULL_ID;
	public static final int CITY_NULL_ID = 4;
	public static final String CITY_NULL = EXC_ID + CITY_NULL_ID;
	public static final int JURISDICTION_CODE_INVALID_ID = 5;
	public static final String JURISDICTION_CODE_INVALID = EXC_ID + JURISDICTION_CODE_INVALID_ID;
	public static final int OTHER_ID = 100;
	public static final String OTHER = EXC_ID + OTHER_ID;

	static {
		ErrorMessageBundle.setDefaultMessage(ADDRESS_NULL,
										  "During Reading the shipping address from Order object, Shipping address is null");
		ErrorMessageBundle.setDefaultMessage(NAME_NULL,
										  "During Reading the shipping address from Order object, Shipping names are null");
		ErrorMessageBundle.setDefaultMessage(STREET_NULL,
										  "During Reading the shipping address from Order object, street is null");
		ErrorMessageBundle.setDefaultMessage(CITY_NULL,
										  "During Reading the shipping address from Order object, city is null");
		ErrorMessageBundle.setDefaultMessage(JURISDICTION_CODE_INVALID,
										  "Tax jurisdiction code  could not be determined, no address set");
		ErrorMessageBundle.setDefaultMessage(OTHER,
										  "During Reading the shipping address from Order object, other error occured");
	}

	private int errorId; 
	
	private InvalidAddressException(int errId, I18nErrorMessage arg0) {
		super(arg0);
		errorId = errId;
	}
	
	/**
	 * @param arg0
	 */
	public static InvalidAddressException create(int errorId) {
		String eMsg = EXC_ID + errorId;
		ErrorMessageBundle bundle =	ErrorMessageBundle.getInstance();
		I18nErrorMessage msg = bundle.newI18nErrorMessage(eMsg);
		return new InvalidAddressException(errorId, msg);
	}

	/**
	 * @return
	 */
	public int getErrorId() {
		return errorId;
	}

}
