/*
 * Created on Sep 9, 2003
 *
 */
package com.sap.isa.auction.backend.exception;

import com.sap.isa.auction.exception.SystemException;
import com.sap.isa.auction.exception.i18n.ErrorMessageBundle;
import com.sap.isa.auction.exception.i18n.I18nErrorMessage;
import com.sap.tc.logging.Category;
import com.sap.tc.logging.Location;

/**
 * @author I802891
 *
 */
public class SAPBackendException extends SystemException {
	private static final String EXC_ID = "SAPBackendException";

	public static final String QUOTATION_FAILED = EXC_ID + 1;
	public static final String QUOTATION_UPDATION_FAILED = EXC_ID + 2;
	public static final String AUCTION_BACKEND_SERVICE_LOOKUP_FAILED = EXC_ID + 3;
	public static final String UNKNOWN_BACKEND_ERROR = EXC_ID + 4;
	public static final String EMAIL_FAILED = EXC_ID + 5;
	public static final String QUOTATION_FAILED_AUCTION_STATUS_UNCHANGED = EXC_ID + 6;
	public static final String BACKEND_COMMUNICATION_FAILED = EXC_ID + 7;	
	public static final String ORDER_FAILED = EXC_ID + 8;
	public static final String ORDER_UPDATION_FAILED = EXC_ID + 9;
	static {
		ErrorMessageBundle.setDefaultMessage(QUOTATION_FAILED,
										  "Quotation Creation failed, Please contact SAP CRM administrator.");
		ErrorMessageBundle.setDefaultMessage(QUOTATION_UPDATION_FAILED,
										  "Quotation Updation failed, Please contact SAP CRM administrator.");
		ErrorMessageBundle.setDefaultMessage(AUCTION_BACKEND_SERVICE_LOOKUP_FAILED,
										  "Failed to lookup Auction CRM backend service.");
		ErrorMessageBundle.setDefaultMessage(UNKNOWN_BACKEND_ERROR,
										  "Unknown error occured communicating with Auction CRM backend service");
		ErrorMessageBundle.setDefaultMessage(EMAIL_FAILED,
										  "Email Failed, Please contact SAP CRM administrator.");
		ErrorMessageBundle.setDefaultMessage(QUOTATION_FAILED_AUCTION_STATUS_UNCHANGED,
										  "Quotation failed, Please try again later.");
		ErrorMessageBundle.setDefaultMessage(BACKEND_COMMUNICATION_FAILED,
										  "SAP Backend communication failed, check that the SAP system and network is available.");
		ErrorMessageBundle.setDefaultMessage(ORDER_FAILED,
										  "Sales Order Creation failed, Please contact administrator.");
		ErrorMessageBundle.setDefaultMessage(ORDER_UPDATION_FAILED,
										  "Sales Order Updation failed, Please contact administrator.");
	}
	
	/**
	 * @param arg0
	 */
	public SAPBackendException(Throwable arg0) {
		super(arg0);
	}

	/**
	 * @param arg0
	 */
	public SAPBackendException(I18nErrorMessage arg0) {
		super(arg0);
	}

	/**
	 * @param arg0
	 * @param arg1
	 */
	public SAPBackendException(I18nErrorMessage arg0, Throwable arg1) {
		super(arg0, arg1);
	}

	/**
	 * @param arg0
	 * @param arg1
	 * @param arg2
	 * @param arg3
	 * @param arg4
	 */
	public SAPBackendException(
		Category arg0,
		int arg1,
		Location arg2,
		I18nErrorMessage arg3,
		Throwable arg4) {
		super(arg0, arg1, arg2, arg3, arg4);
	}

}
