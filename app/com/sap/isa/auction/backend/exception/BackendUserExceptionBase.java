/*
 * Created on Mar 25, 2004
 *
 * To change the template for this generated file go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
package com.sap.isa.auction.backend.exception;

import java.util.*;

/**
 * @author I803746
 *
 * To change the template for this generated type comment go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
public interface BackendUserExceptionBase {
	public BackendUserError getBackendUserError();
	
	public void addErrorMessage(String text);
	
	public void addWarningMessage(String text);
	
	public List getErrorMessages();
	
	public List getWarningMessages();
	
	public List getAllMessages();
	
	public boolean isSerious();

}
