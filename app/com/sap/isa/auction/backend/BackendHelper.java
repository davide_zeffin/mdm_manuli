/*
 * Created on Dec 15, 2004
 *
 * To change the template for this generated file go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
package com.sap.isa.auction.backend;

import com.sap.isa.auction.backend.exception.BackendUserException;
import com.sap.isa.auction.logging.CategoryProvider;
import com.sap.isa.backend.r3base.rfc.RFCConstants;
import com.sap.mw.jco.*;
import com.sap.tc.logging.*;
import com.sap.isa.backend.crm.WrapperCrmIsa;
/**
 * @author I803746
 *
 * To change the template for this generated type comment go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
public class BackendHelper {

	private final static Category cat = CategoryProvider.getSystemCategory();
	private final static Location loc =
		Location.getLocation(BackendHelper.class);

	public static boolean processBackendReturn(JCO.Table returnTable) throws BackendUserException{
		return processBackendReturn(returnTable, true);
	}
	
	public  static boolean processBackendReturn(JCO.Table returnTable,
		boolean throwException) throws BackendUserException{

		BackendUserException buExc = new BackendUserException();
		boolean hasError = false;
		String type = null;
		String message = null;

		if(returnTable == null )
			return false;
			 
		if (returnTable.getNumRows() >0) {
			do {
				type = returnTable.getString("TYPE");
				message = returnTable.getString("MESSAGE");
				
				if(type.equals(RFCConstants.BAPI_RETURN_ERROR) ||
				   type.equals(RFCConstants.BAPI_RETURN_ABORT)) 
				{
					hasError = true;
					buExc.addErrorMessage(message);
					cat.logT(Severity.ERROR, loc, "Backend Messages Type-Message " + type + "-"+ message);
				}
				else
				{
					buExc.addWarningMessage(message);
					cat.logT(Severity.DEBUG, loc, "Backend Messages Type-Message " + type + "-"+ message);
				}
			}
			while (returnTable.nextRow());
		}
		if(hasError && throwException)
			throw buExc;
		return hasError;
	}

	public static boolean processBackendReturn(JCO.Structure returnStruct) throws BackendUserException{
		return processBackendReturn(returnStruct, true);
	}
	
	public  static boolean processBackendReturn(JCO.Structure returnStruct,
		boolean throwException) throws BackendUserException{

		BackendUserException buExc = new BackendUserException();
		boolean hasError = false;
		String type = null;
		String message = null;

		if(returnStruct == null )
			return false;
			
		type = returnStruct.getString("TYPE");
		message = returnStruct.getString("MESSAGE");
		
		if(type.equals(RFCConstants.BAPI_RETURN_ERROR) ||
		   type.equals(RFCConstants.BAPI_RETURN_ABORT)) 
		{
			hasError = true;
			buExc.addErrorMessage(message);
			cat.logT(Severity.ERROR, loc, "Backend Messages Type-Message " + type + "-"+ message);
		}
		else
		{
			buExc.addWarningMessage(message);
			cat.logT(Severity.DEBUG, loc, "Backend Messages Type-Message " + type + "-"+ message);
		}
		if(hasError && throwException)
			throw buExc;
		return hasError;
	}
	
	public  static boolean processBackendReturn(WrapperCrmIsa.ReturnValue retVal) throws BackendUserException{
			
		return processBackendReturn(retVal.getMessages());
	}	
	
	public  static boolean processBackendReturn(WrapperCrmIsa.ReturnValue retVal,
		boolean throwException) throws BackendUserException{
			
		return processBackendReturn(retVal.getMessages(), throwException);
	}	

	public static String ExtendString(String id, int destLength)
	{
	  StringBuffer fullID = new StringBuffer();
	  int len = id.length();
	  if(len < destLength) {
		for(int i=0; i<(destLength-len); i++)
		  fullID.append('0');
	  }
	  fullID.append(id);
	  return fullID.toString();
	}

}
