package com.sap.isa.auction.backend.crm.email;


import java.util.ArrayList;
import java.util.Properties;

import com.sap.mw.jco.JCO;
import com.sap.tc.logging.Category;
import com.sap.tc.logging.Location;
import com.sap.tc.logging.Severity;
import com.sap.isa.auction.backend.boi.email.ErrorNotificationBackend;
import com.sap.isa.auction.backend.boi.email.ErrorNotificationData;
import com.sap.isa.auction.logging.CategoryProvider;
import com.sap.isa.backend.JCoHelper;
import com.sap.isa.core.eai.BackendBusinessObjectParams;
import com.sap.isa.core.eai.BackendException;
import com.sap.isa.core.eai.sp.jco.BackendBusinessObjectBaseSAP;
import com.sap.isa.core.eai.sp.jco.JCoConnection;




public class ErrorNotificationCRM extends BackendBusinessObjectBaseSAP implements ErrorNotificationBackend {

	private final static Category cat = CategoryProvider.getSystemCategory();
	private final static Location loc =
		Location.getLocation(ErrorNotificationCRM.class);
	protected Properties props ;
	private final static String ERROR_NOTIFICATION_FORM = "adminNotificationForm";
		
	/* 
	 * Sends email using SAP Connect in SAP system
	 * @see com.sap.isa.auction.backend.boi.email.ErrorNotificationBackend#sendErrorNotification(com.sap.isa.auction.backend.boi.email.ErrorNotificationData)
	 */
	public boolean sendErrorNotification(ErrorNotificationData errorData) throws BackendException {
		JCoConnection connection = null;
		ArrayList to = errorData.getTo();
		ArrayList cc = errorData.getCC();
		String from = errorData.getFrom();
		String errorString = null;
		 
		try {
			if( (to == null) || (from == null) ){
				cat.log(Severity.ERROR, loc, "Error notification aborted due to empty TO(recipient) field");
				return false;
			}
				
			connection = getDefaultJCoConnection();

			JCO.Function emailNotification = connection.getJCoFunction("CRM_ISA_AUC_ADM_EMAIL_NOTIFY");

			
			JCO.ParameterList importParams = emailNotification.getImportParameterList();
			importParams.setValue(props.get(ERROR_NOTIFICATION_FORM), "IT_SMARTFORM");
			importParams.setValue(from, "SEND_PARTNER");
			importParams.setValue(errorData.getSubject(), "MAIL_SUBJECT");
			importParams.setValue(errorData.getError(), "ERROR");
			importParams.setValue(errorData.getCause(), "CAUSE");
			importParams.setValue(errorData.getResolution(), "RESOLUTION");
			
			JCO.ParameterList tblList = emailNotification.getTableParameterList();
			JCO.Table receiverTable = tblList.getTable("RECEIVERS");
			
			for (int i=0; i< to.size(); i++){
				receiverTable.appendRow();
				String receiver = (String) to.get(i);
				if(receiver != null){
					receiverTable.setValue(receiver,"RECEIVER");
					
					receiverTable.setValue("U", "REC_TYPE");
					receiverTable.setValue("INT" , "COM_TYPE");
				}
			}
			if(cc != null){
			
				for (int i=0; i< cc.size(); i++){
					receiverTable.appendRow();
					receiverTable.setValue(cc.get(i),"RECEIVER");
					receiverTable.setValue("U", "REC_TYPE");
					receiverTable.setValue("INT" , "COM_TYPE");
				}
			}				
			
			connection.execute(emailNotification);
			
			JCO.ParameterList exportParams = emailNotification.getExportParameterList();
			JCO.Table statusTable = exportParams.getTable("ET_RETURN");
			for (int i=0 ; i<statusTable.getNumRows() ; i++) {
				statusTable.setRow(i);
				if ("E".equalsIgnoreCase(statusTable.getString("TYPE")) || 
					"A".equalsIgnoreCase(statusTable.getString("TYPE")))
					errorString += statusTable.getString("MESSAGE");
			}
			
			if(errorString != null)
				throw new BackendException("Following Errors occured while sending email " + errorString);
		}
		catch (JCO.Exception ex) {
			cat.log(Severity.DEBUG, loc, "Error in calling CRM function:  ", ex);
			JCoHelper.splitException (ex);
		}
		finally {
			//always close the JCo connection and release the client
			connection.close();
		}
		return true;

	}


	/* (non-Javadoc)
	 * @see com.sapmarkets.isa.core.eai.BackendBusinessObject#initBackendObject(java.util.Properties, com.sapmarkets.isa.core.eai.BackendBusinessObjectParams)
	 */
	public void initBackendObject(
		Properties backendProps,
		BackendBusinessObjectParams baboParams)
		throws BackendException {
		props = backendProps; 
		
		super.initBackendObject(backendProps, baboParams);
	}
	

}
