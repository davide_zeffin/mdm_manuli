package com.sap.isa.auction.backend.crm.email;


import java.util.Iterator;
import java.util.List;
import java.util.Properties;

import com.sap.mw.jco.JCO;
import com.sap.tc.logging.Category;
import com.sap.tc.logging.Location;
import com.sap.tc.logging.Severity;
import com.sap.isa.auction.backend.boi.AuctionData;
import com.sap.isa.auction.backend.boi.WinnerData;
import com.sap.isa.auction.backend.boi.email.EmailNotificationBackend;
import com.sap.isa.auction.backend.boi.email.EmailNotificationData;
import com.sap.isa.auction.backend.boi.email.EmailNotificationType;
import com.sap.isa.auction.bean.AuctionItem;
import com.sap.isa.auction.bean.AuctionTypeEnum;
import com.sap.isa.auction.logging.CategoryProvider;
import com.sap.isa.auction.util.StringUtil;
import com.sap.isa.backend.JCoHelper;
import com.sap.isa.core.eai.BackendBusinessObjectParams;
import com.sap.isa.core.eai.BackendException;
import com.sap.isa.core.eai.sp.jco.BackendBusinessObjectBaseSAP;
import com.sap.isa.core.eai.sp.jco.JCoConnection;




public class EmailNotificationCRM extends BackendBusinessObjectBaseSAP implements EmailNotificationBackend {

	private final static Category cat = CategoryProvider.getSystemCategory();
	private final static Location loc =
		Location.getLocation(EmailNotificationCRM.class);
	protected Properties props;
	
	private final static String BID_INVITATION_FORM = "bidinvitationForm";
	private final static String WINNER_NOTIFICATION_FORM ="winnerNotificationForm";
	
	private final static String VIEWITEM_URL ="viewItemURL";
	
		
	/* Sends email notification using Smart forms
	 * @see com.sap.isa.auction.backend.boi.email.EmailNotificationBackend#sendEmailNotification(com.sap.isa.auction.backend.boi.email.EmailNotificationData, com.sapmarkets.isa.backend.boi.isacore.ShopData)
	 */
	public boolean sendEmailNotification(EmailNotificationData emailData) throws BackendException {
		JCoConnection connection = null;

		String from = emailData.getFrom();
		String emailID = emailData.getTo();
		String resultLog = "";
		boolean isSuccess = true;
		try {
			cat.log(Severity.DEBUG, loc, "Preparing emailNotification" );
			
			connection = getDefaultJCoConnection();

			JCO.Function emailNotification = connection.getJCoFunction("CRM_ISA_AUC_BUYER_EMAIL_NOTIFY");

			
			JCO.ParameterList importParams = emailNotification.getImportParameterList();
			if(emailData.getEmailNotificationType() == EmailNotificationType.BIDDING_INVITATION){
				importParams.setValue(props.get(BID_INVITATION_FORM), "IT_SMARTFORM");
			}else if(emailData.getEmailNotificationType() == EmailNotificationType.AUCTION_WINNING){
				importParams.setValue(props.get(WINNER_NOTIFICATION_FORM), "IT_SMARTFORM");
			}
			
			importParams.setValue(from, "SEND_PARTNER");
			importParams.setValue(emailData.getSubject(), "MAIL_SUBJECT");
			AuctionData auctionData = emailData.getAuctionData();
			
			if(emailData.getEmailNotificationType() == EmailNotificationType.BIDDING_INVITATION){

				//ADD business partner list 
				List bpList = auctionData.getBusinessPartners();
				if(bpList != null){
					Iterator bpIterator = bpList.iterator();
					JCO.Table bpTable = importParams.getTable("IT_BP_ACT");
					while (bpIterator.hasNext()){
						bpTable.appendRow();
						String bpID = (String)bpIterator.next();
						JCO.Field field = bpTable.getField("BP_NUMBER");
						StringUtil.setExtendIdField(field, bpID);
					}
				}
				//Add target group guids
				List targetGroups = auctionData.getTargetGroups();
				if(targetGroups != null){
					Iterator tgIterator = targetGroups.iterator();
					JCO.Table tgTable = importParams.getTable("IT_TARGETGRP");
					while (tgIterator.hasNext()){
						tgTable.appendRow();
						tgTable.setValue(tgIterator.next(), "TARGETGRP_GUID");
					}
				}
			}else if(emailData.getEmailNotificationType() == EmailNotificationType.AUCTION_WINNING){
				JCO.Table bpTable = importParams.getTable("IT_BP_ACT");
				
				String bpID = emailData.getWinnerData().getBuyerId();
				if (bpID != null){
					bpTable.appendRow();
					JCO.Field field = bpTable.getField("BP_NUMBER");
					StringUtil.setExtendIdField(field, bpID);
				}
				
				if( emailID != null){
				   JCO.ParameterList tableParams = emailNotification.getTableParameterList();
				   JCO.Table receivers = tableParams.getTable("IT_RECEIVERS");
				   receivers.appendRow();
				   JCO.Field field = receivers.getField("RECEIVER");
				   field.setValue(emailID);
					
			   }				
			}
			
			//add auction information
			JCO.Structure auctionStruct = importParams.getStructure("IT_AUCTION");
			
			auctionStruct.setValue(auctionData.getAuctionTitle(),"OPP_NAME");
			auctionStruct.setValue(auctionData.getAuctionId(),"OPP_ID");
			auctionStruct.setValue(auctionData.getAuctionTerms(),"TERMS_COND");
			
			auctionStruct.setValue(StringUtil.crmConvertDateToEmailCompatibleString(auctionData.getStartDate()),"OPP_START_DATE");
			auctionStruct.setValue(StringUtil.crmConvertDateToEmailCompatibleString(auctionData.getEndDate()),"OPP_END_DATE");
			auctionStruct.setValue(auctionData.getCurrencyCode(),"CURRENCY");
			auctionStruct.setValue(auctionData.getImageURL(), "IMAGE_URL");
			
			String auctionURL = auctionData.getAuctionURL((String)props.get(VIEWITEM_URL));
			///b2c/init.do?scenario.xcm=CRM_Q5C_705&shop=SB_MIT2&transId=sdfsdfsdfsd&forward=auctionChkout
			if( auctionURL != null){
				
//				if(emailData.getScenario() != null)
//					auctionURL = auctionURL + "&scenario.xcm="+ emailData.getScenario();
				if(emailData.getTxnId() != null)
					auctionURL = auctionURL + "&transId="+ emailData.getTxnId();											  
				auctionStruct.setValue(auctionURL,"OPP_URL");
			}
				
				
			
			if(emailData.getEmailNotificationType() == EmailNotificationType.BIDDING_INVITATION){
				if (auctionData.getStartPrice() != null )
					auctionStruct.setValue(auctionData.getStartPrice().toString(),"OPP_INIT_BID");
				if (auctionData.getReservePrice()!= null)
					auctionStruct.setValue(auctionData.getReservePrice().toString(),"RESERVED_PRICE");
			}
			else if(emailData.getEmailNotificationType() == EmailNotificationType.AUCTION_WINNING){
				WinnerData winnerData = emailData.getWinnerData();
				if (auctionData.getStartPrice() != null )
					auctionStruct.setValue(auctionData.getStartPrice().toString(),"OPP_INIT_BID");
				if(winnerData.getBidAmount() != null)
					auctionStruct.setValue(winnerData.getBidAmount().toString(),"OPP_HIGH_BID"); //checkout URL
			}
			
			int intType = auctionData.getType();
			if (intType == AuctionTypeEnum.BROKEN_LOT.getValue()){
				auctionStruct.setValue(auctionData.getQuantity(),"NUM_UNITS"); //checkout URL
			}
												
			//add items
			List auctionItemList = auctionData.getListings();
			if(auctionItemList != null){
				Iterator auctionIterator = auctionItemList.iterator();
				JCO.Table itemTable = importParams.getTable("IT_ITEMS");
				while (auctionIterator.hasNext()){
					itemTable .appendRow();
					AuctionItem auctionItem = (AuctionItem) auctionIterator.next();
					itemTable.setValue(auctionItem.getDescription() , "PRODUCT");
					itemTable.setValue(auctionItem.getQuantity(), "QUANTITY");
//					itemTable.setValue(auctionItem.get, "CURRENCY");
//					itemTable.setValue(, "COND_TYPE");
//					itemTable.setValue(, "COND_RATE");
					itemTable.setValue(auctionItem.getUOM(), "COND_UNIT");
					itemTable.setValue(auctionItem.getImage(), "IMAGE_URL");
				}
				
			}
			
			connection.execute(emailNotification);

			JCO.ParameterList exportParams = emailNotification.getExportParameterList();
			JCO.Table statusTable = exportParams.getTable("ET_RETURN");
			for (int i=0 ; i<statusTable.getNumRows() ; i++) {
				statusTable.setRow(i);
				resultLog += statusTable.getString("MESSAGE");
				if ("E".equalsIgnoreCase(statusTable.getString("TYPE")) || 
					"A".equalsIgnoreCase(statusTable.getString("TYPE"))){
						isSuccess = false;
					}
			}
			
			if(! isSuccess){
				throw new BackendException("Following Errors occured while sending email " + resultLog);
			}else{
				cat.log(Severity.DEBUG, loc, "Email log: " + resultLog);
			}
			
		}
		catch (JCO.Exception ex) {
			cat.log(Severity.DEBUG, loc, "Error occured while sending email notification:  ", ex);
			JCoHelper.splitException (ex);
		}catch (RuntimeException ex){
			cat.log(Severity.DEBUG, loc, "Error occured while sending email notification:  ", ex);
			throw ex;
		}
		finally {
			//always close the JCo connection and release the client
			connection.close();
		}
		return isSuccess;
	}



	/* (non-Javadoc)
	 * @see com.sapmarkets.isa.core.eai.BackendBusinessObject#initBackendObject(java.util.Properties, com.sapmarkets.isa.core.eai.BackendBusinessObjectParams)
	 */
	public void initBackendObject(
		Properties backendProps,
		BackendBusinessObjectParams baboParams)
		throws BackendException {
		props = backendProps;
		super.initBackendObject(backendProps, baboParams);
	}

}
