/*
 * Created on May 18, 2004
 *
 * To change the template for this generated file go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
package com.sap.isa.auction.backend.crm;

import com.sap.tc.logging.*;
import com.sap.isa.auction.backend.boi.SystemSettingData;
import com.sap.isa.auction.logging.CategoryProvider;
import com.sap.isa.backend.IsaBackendBusinessObjectBaseSAP;
import com.sap.isa.core.eai.BackendException;
import com.sap.isa.core.eai.sp.jco.JCoConnection;

/**
 * @author I803746
 *
 * To change the template for this generated type comment go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
public class SystemSettingCRM
	extends IsaBackendBusinessObjectBaseSAP
	implements SystemSettingData {
		
	private final static Category cat = CategoryProvider.getSystemCategory();
	private final static Location loc =
		Location.getLocation(SystemSettingCRM.class);

	private String client;
	private String systemId;
		
	/**
	 * 
	 */
	public SystemSettingCRM() {
		super();
	}

	/* (non-Javadoc)
	 * @see com.sapmarkets.isa.auction.backend.boi.SystemSettingData#getClient()
	 */
	public String getClient() {
		if(client == null)
			retrieveSystemSetting();
		return client;
	}

	/* (non-Javadoc)
	 * @see com.sapmarkets.isa.auction.backend.boi.SystemSettingData#getSystemId()
	 */
	public String getSystemId() {
		if(systemId == null)
			retrieveSystemSetting();
		return systemId;
	}

	private void retrieveSystemSetting()
	{
		JCoConnection connection;
		try {
			connection = getCompleteConnection();
			systemId = connection.getAttributes().getSystemID();
			client = connection.getAttributes().getClient();
		} catch (BackendException e) {
			cat.log(Severity.ERROR, loc, "Error when retrieve CRM setting: ", e);
		}
	}
	
	protected JCoConnection getCompleteConnection() throws BackendException
	{
		return (JCoConnection)getConnectionFactory().getConnection(com.sap.isa.core.eai.init.InitEaiISA.FACTORY_NAME_JCO,
				com.sap.isa.core.eai.init.InitEaiISA.CON_NAME_ISA_STATELESS);
	}
}
