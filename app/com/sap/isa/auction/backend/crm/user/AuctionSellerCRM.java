/*
 * Title:        eBay Integration Project
 * Description:	 * SAP Labs LLC, the eBay auction project, also is known as Selling via eBay. 
 * Selling via eBay application provides SAP R/3 (and in the future CRM) 
 * customers to auction and sell inventory/products onto an online marketplace 
 * eBay. The application will provide the ability to Create, Schedule, Publish, 
 * Monitor and Close Auction Listings onto eBay and handle all the relevant 
 * Backend processing for Customers and Orders. 

 * Copyright:    Copyright (c) 2003
 * Company:      SAP Labs LLC
 * @author
 * @version 1.0
 */
package com.sap.isa.auction.backend.crm.user;

import java.util.ArrayList;
import java.util.Properties;

import com.sap.isa.auction.backend.boi.businesspartner.ResponsibleSalesEmployeeData;
import com.sap.isa.auction.backend.boi.user.AuctionUserBackend;
import com.sap.isa.auction.backend.boi.user.AuctionUserBaseData;
import com.sap.isa.auction.backend.boi.user.AuctionUserData;
import com.sap.isa.auction.businessobject.businesspartner.ResponsibleEmployee;
import com.sap.isa.auction.logging.CategoryProvider;
import com.sap.isa.backend.JCoHelper;
import com.sap.isa.backend.crm.MessageCRM;
import com.sap.isa.core.TechKey;
import com.sap.isa.core.eai.BackendBusinessObjectParams;
import com.sap.isa.core.eai.BackendException;
import com.sap.isa.core.eai.sp.jco.JCoConnection;
import com.sap.isa.core.util.Message;
import com.sap.isa.user.backend.boi.UserBaseData;
import com.sap.isa.user.backend.crm.UserBaseCRM;
import com.sap.isa.user.util.LoginStatus;
import com.sap.mw.jco.JCO;
import com.sap.tc.logging.Category;
import com.sap.tc.logging.Location;
import com.sap.tc.logging.Severity;
/**
 * Class to control the user login and retrieve the
 * sales employee ID (BP role BUP003), this is a user type SU01
 */
public class AuctionSellerCRM
	extends UserBaseCRM
	implements AuctionUserBackend {
	/**
	 * the sales employee for the auction scenario
	 */
	private String salesEmployee = null;
	private String client;
	private String systemId;

	private final static Category cat = CategoryProvider.getSystemCategory();
	private final static Location loc =
		Location.getLocation(AuctionSellerCRM.class);
	
	private String old_password = null;
	private JCoConnection jcoCon;
	private final static String SPECIAL_NAME_AS_USERID = "$MYSAPSSO2$";

	private static String SVE_SELLER_ROLE = "SS";
	private static String SVE_ADMIN_ROLE = "SA";
	private static String AVW_SELLER_ROLE = "WS";
	private static String AVW_ADMIN_ROLE = "WA";	
	protected Properties props; // backend object properties mentioned in backend-config.xml
	private static final String SCENARIO_PROP_NAME = "scenario"; // for AVW or SVE
	private static final String SVE_SCENARIO = "SVE";
	private static final String AVW_SCENARIO = "AVW";
	
	private String scenario = SVE_SCENARIO	;
		
	protected final static String EMPLOYEE = "Sales Employee";
	/**
	 * Default constructor
	 */
	public AuctionSellerCRM() {
		super();
		cat.logT(Severity.DEBUG, loc, "AuctionSellerCRM constructed");
	}

	/* 
	 * No implemented yet
	 * Get the user defaults like timezone, decimal format and
	 * date format given the crm user id
	 * @param aucUser
	 * @param userid
	 * @throws BackendException
	 */
	public void getUserDefaults(AuctionUserData aucUser, String userid)
		throws BackendException {
		cat.logT(Severity.ERROR, loc, "In AuctionSellerCRM getUserDefaults " +								" not implemented");
		throw new BackendException("Method getUserDefaults not implemented");
	}	

	/**
	 * User login into the CRM system for the auction seller side scenario. 
	 * The login type is SU01 type, and the sales employee us determined from the
	 * user ID. three steps here
	 * check the user name and password
	 * check the profile name
	 * check the BP relationship to get role of employee
	 * 
	 * 
	 * @param password              user password
	 * @param user                  user id
	 * @return indicates if the login is sucessful or not
	 * @exception BackendException  Exception from backend
	 */
	public LoginStatus login(UserBaseData user, String password)
	throws BackendException {

		
		LoginStatus functionReturnValue = LoginStatus.NOT_OK;
		String profileName = null;

		cat.logT(Severity.DEBUG, loc, "In the beginning of the login process");			
		try {
			//check whether there is a try to login via a SSO2-ticket
			if (user.getUserId().equals(SPECIAL_NAME_AS_USERID)) {
			 // note: the password string should in fact contain the ticket
			 	functionReturnValue =  super.loginViaTicket(user, password);
			 	if (functionReturnValue != LoginStatus.OK) {
			 		return functionReturnValue;
			 	}//otherwise logon is successful continue with rest of the validations
				user.setUserId(user.getTechKey().getIdAsString());
				jcoCon = getCompleteConnection();
			} 
			else{
				// check whether the user and password are valid in the backend
				jcoCon = getCompleteConnection();
				// login with SU01 Internet User
				functionReturnValue = normalUserLogin(jcoCon, user, password);				
			}
			
			systemId = jcoCon.getAttributes().getSystemID();
			client = jcoCon.getAttributes().getClient();

			cat.logT(Severity.DEBUG, loc, "After normalUserLogin status is" +
							functionReturnValue);
			if (functionReturnValue == LoginStatus.OK) {
				cat.logT(Severity.DEBUG, loc,
					user + " is validated succesfully for SU01  " + functionReturnValue);
				getScenario();
				functionReturnValue = getCheckEmpolyee(jcoCon, user);
				
			}
			else{
				cat.logT(Severity.INFO, loc,
					user + " is not an SU01  user" );
			}
			jcoCon.close();
		} catch (JCO.Exception ex) {
			cat.logT(Severity.ERROR, loc, "Error calling CRM function:" + ex);
			JCoHelper.splitException(ex);
		} 		
		return functionReturnValue;
				
	}

	/* 
	 * get the sales employee from a SU01 user, in current implementation
	 * we assume that one User gets one sales employee
	 * Her the internet user is type of system user SU01
	 * @param userName
	 * @return
	 * @throws BackendException
	 */
	public ResponsibleSalesEmployeeData getSalesEmployeeFromInternetUser(String userName)
		throws BackendException {
			throw new UnsupportedOperationException();
	}

	/* 
	 * @param profiles
	 * @return
	 * @throws BackendException
	 */
	public ArrayList getAllSellers(ArrayList profiles) throws BackendException {
		throw new UnsupportedOperationException();
	}
	
	/** getter for the sales employee
	 * @return the sales employee
	 */
	public String getSalesEmployee() {
		return salesEmployee;
	}

	/** setter for the sales employee
	 * @param string the sales employee
	 */
	public void setSalesEmployee(String string) {
		salesEmployee = string;
	}


	/* 
	 * Returns the client system against which the current scenario is running
	 * @see com.sap.isa.auction.backend.boi.user.AuctionUserBackend#getClient()
	 */
	public String getClient() {
		if(client == null) {
			JCoConnection connection;
			try {
				connection = getCompleteConnection();
				systemId = connection.getAttributes().getSystemID();
				client = connection.getAttributes().getClient();
			} catch (BackendException e) {
				cat.logT(Severity.ERROR, loc, "error is in getClient " + e.getMessage());
			}
		}
		return client;
	}

	/* Returns the system if of the SAP client against which the current scenario is running
	 * @see com.sap.isa.auction.backend.boi.user.AuctionUserBackend#getSystemId()
	 */
	public String getSystemId() {
		if(systemId == null) {
			JCoConnection connection;
			try {
				connection = getCompleteConnection();
				systemId = connection.getAttributes().getSystemID();
				client = connection.getAttributes().getClient();
			} catch (BackendException e) {
				cat.logT(Severity.ERROR, loc, "error is in getSystemId " + e.getMessage());
			}
		}
		return systemId;
	}
	
	
	/**
	 *  Validates the SU01 userid and password
	 *
	 *@param  jcoCon                Description of Parameter
	 *@param  user                  Description of Parameter
	 *@param  password              Description of Parameter
	 *@return                       Description of the Returned Value
	 *@exception  BackendException  Description of Exception
	 *
	 */
	private LoginStatus normalUserLogin(JCoConnection jcoCon,
										UserBaseData user,
										String password)
	throws BackendException {

		LoginStatus functionReturnValue = LoginStatus.NOT_OK;
		cat.logT(Severity.DEBUG, loc, "Checking of SU01 userid and passwords");
		// execute the login checks
		// therefore we need only a default connection
		try {
			// call the function to login the internet user

			// get the repository infos of the function that we want to call
			JCO.Function function = jcoCon.getJCoFunction("CRM_ISA_LOGIN_R3USER_CHECKS");

			// set the import values
			JCO.ParameterList importParams = function.getImportParameterList();

			// the parameter user identifier specifies the value of the userid attribut
			if ((getUserIdentifier()== null) || (!getUserIdentifier().equals(USERALIAS))) {
				importParams.setValue(user.getUserId().toUpperCase(), "USERNAME");
			}else{
				importParams.setValue(user.getUserId().toUpperCase(), "USERALIAS");
			}
			importParams.setValue(password, "PASSWORD");
			importParams.setValue("X", "WITH_SALUTATION");

			// call the function
			jcoCon.execute(function);

			//get the return values
			JCO.ParameterList exportParams = function.getExportParameterList();

			String returnCode = exportParams.getString("RETURNCODE");

			// get messages
			JCO.Table messages = function.getTableParameterList().getTable("MESSAGES");
			user.clearMessages();

			if (returnCode.length() == 0 ||
				returnCode.equals("0")) {
				MessageCRM.addMessagesToBusinessObject(user, messages);
				user.setTechKey(new TechKey(exportParams.getString("R3_USERNAME")));
				user.setSalutationText(exportParams.getString("SALUTATION"));
				loginSucessful = true;
				functionReturnValue = LoginStatus.OK;
			}
			else {
				loginSucessful = false;
				// bring errors to the error page
				MessageCRM.addMessagesToBusinessObject(user, messages);
				MessageCRM.logMessagesToBusinessObject(user, messages);

				if (exportParams.getString("PASSWORD_EXPIRED").length() > 0) {
					user.setTechKey(new TechKey(exportParams.getString("R3_USERNAME")));
					user.setSalutationText(exportParams.getString("SALUTATION"));
					functionReturnValue = LoginStatus.NOT_OK_PASSWORD_EXPIRED;
				}
				else {
					functionReturnValue = LoginStatus.NOT_OK;
				}
			}
		}
		catch (JCO.Exception ex) {
			cat.logT(Severity.ERROR, loc, "Error calling CRM function module:" + ex.getMessage());
			JCoHelper.splitException(ex);
		}
		cat.logT(Severity.DEBUG, loc, "End of normalUserLogin process");
		return functionReturnValue;
	}



	/**
	 * Verifies whether the user has admin or seller previlages
	 * Also retreives the sales employee from the CRM system corresponding
	 * to the logged on user
	 * @return
	 */
	private LoginStatus getCheckEmpolyee(JCoConnection jcoCon, UserBaseData user) throws BackendException {
		LoginStatus functionReturnValue = LoginStatus.NOT_OK;
		String role;
		if (log.isDebugEnabled()){
			log.debug("Checking the employee for the user ");
		}
		try {
			// call the function to login the internet user

			// get the repository infos of the function that we want to call
			JCO.Function function = jcoCon.getJCoFunction("CRM_ISA_AUC_AUTHORITY_CHECK");
			//set the import parameters for the function call
			JCO.ParameterList importParams =
									function.getImportParameterList();
			importParams.setValue (user.getUserId().toUpperCase(), "AUC_USERNAME");
			if (AVW_SCENARIO.equalsIgnoreCase(scenario)){
				importParams.setValue ('X', "IV_AVW_SCENARIO");
			}
			
			//call the function
			jcoCon.execute(function);
			// check the 
			//get the return values
			JCO.ParameterList exportParams = function.getExportParameterList();
			salesEmployee = exportParams.getString("SALES_EMPLOYEE");
			if(cat.beDebug())
				cat.logT(Severity.DEBUG, loc, "Seller exist for the user "+ user.getUserId() +"is :" + salesEmployee);
			role = exportParams.getString("AUC_USER_TYPE");
			String returnCode = exportParams.getString("RETURNCODE");
			//check if the BP ID is null
			if ((salesEmployee != null) && (salesEmployee.trim().length() != 0)){
				functionReturnValue = LoginStatus.OK;
				
				if (user instanceof AuctionUserBaseData) {
					AuctionUserBaseData aucUser = (AuctionUserBaseData)user;
					TechKey aucUserKey = new TechKey(salesEmployee);
					aucUser.setBusinessPartner(aucUserKey);
					if(aucUser.getSalesEmployee() == null)
						aucUser.setSalesEmployee(new ResponsibleEmployee());
					aucUser.getSalesEmployee().setTechKey(aucUserKey);
					aucUser.getSalesEmployee().setId(salesEmployee);
					if ( ( scenario.equalsIgnoreCase(SVE_SCENARIO) && SVE_ADMIN_ROLE.equals(role) ) ||
						 ( scenario.equalsIgnoreCase(AVW_SCENARIO) && AVW_ADMIN_ROLE.equals(role) ) )
					{
						//enable administrator functionality for the seller
						aucUser.setAdministrator(true);
					}
				}
			}else{
				cat.logT(Severity.INFO, loc, "login.crm.noempl" , user.getUserId());
				JCO.Table messages = function.getTableParameterList().getTable("MESSAGES");
				user.addMessage(
									new Message(
										Message.ERROR,
										"login.crm.noempl",
										new String[]{user.getUserId()},
										""));
				if(messages.getNumRows() != 0){
					if ((returnCode.length() == 0) ||
							returnCode.equals("0")) {
						MessageCRM.logMessagesToBusinessObject(user, messages);		
					}else{
						MessageCRM.addMessagesToBusinessObject(user, messages);
						MessageCRM.logMessagesToBusinessObject(user, messages);
				}
				}
				//add the message to the message row.
				functionReturnValue = LoginStatus.NOT_OK;
			}
			
		}catch (JCO.Exception ex) {
			cat.logT(Severity.ERROR, loc, "Error calling CRM function:" + ex.getMessage());
			JCoHelper.splitException(ex);
		}
		cat.logT(Severity.DEBUG, loc, "At the end of getCheckEmployee process");
		return functionReturnValue;
	}
	
	protected JCoConnection getCompleteConnection() throws BackendException
	{
		return (JCoConnection)getConnectionFactory().getConnection(com.sap.isa.core.eai.init.InitEaiISA.FACTORY_NAME_JCO,
				com.sap.isa.core.eai.init.InitEaiISA.CON_NAME_ISA_STATELESS);
	}

	/**
	 * Initialize the backend properties 
	 * like scenario SVE or AVW
	 */
	public void initBackendObject(
		Properties backendProps,
		BackendBusinessObjectParams baboParams)
		throws BackendException {
		props = backendProps;
		super.initBackendObject(backendProps, baboParams);
	}
	
	private void getScenario(){
		String confScenario = (String) props.get(SCENARIO_PROP_NAME);
		if (confScenario != null)
			scenario = confScenario; //otherwise keep the default scenario sve 
	}


	/*
	 * Changes the password and calls the login again to get appropriate role
	 * @see com.sap.isa.user.backend.boi.UserBaseBackend#changePassword(com.sap.isa.user.backend.boi.UserBaseData, java.lang.String, java.lang.String)
	 */
	public boolean changePassword(UserBaseData user, String pwd, String pwdVerify)
		throws BackendException {
			
		if (loc.beDebug())
			cat.logT(Severity.DEBUG, loc , "Changing password for user " + user.getUserId());
		 
		boolean isSuccessful = super.changePassword(user, pwd, pwdVerify);
		if(isSuccessful){
			LoginStatus status = login(user, pwd);
			
			if(status == LoginStatus.NOT_OK){
				//change password is successful but the user 
				//is not authorized to access SVE UI like employee not assigned
				if (loc.beDebug())
					cat.logT(Severity.DEBUG, loc , "password change for user "+ user.getUserId() + " is successful. But employee role might be missing");
				//stop the user in LoginRoleAction				
			}			
		}
		
		if (loc.beDebug())
			cat.logT(Severity.DEBUG, loc , "Changed the password for user " + user.getUserId() + " Success :" + isSuccessful);
			
		return isSuccessful;
		
	}

	/* 
	 * Changes the expired password and logs-in again to get appropriate role
	 * @see com.sap.isa.user.backend.boi.UserBaseBackend#changeExpiredPassword(com.sap.isa.user.backend.boi.UserBaseData, java.lang.String, java.lang.String)
	 */
	public boolean changeExpiredPassword(
		UserBaseData user,
		String password_old,
		String password_new)
		throws BackendException {
		boolean isSuccessful =  super.changeExpiredPassword(user, password_old, password_new);

		if (loc.beDebug())
			cat.logT(Severity.DEBUG, loc , "Changing password for user " + user.getUserId());
							
		if(isSuccessful){
			LoginStatus status =  login(user, password_new);
			if(status == LoginStatus.NOT_OK){
				//change password is successful but the user 
				//is not authorized to access SVE UI like employee not assigned
				if (loc.beDebug())
					cat.logT(Severity.DEBUG, loc , "password change for user "+ user.getUserId() + " is successful. But employee role might be missing");
				//stop the user in LoginRoleAction
			}			
		}
		
		if (loc.beDebug())
			cat.logT(Severity.DEBUG, loc , "Changed the password for user " + user.getUserId() + " Success :" + isSuccessful);
							
		return isSuccessful;
	}

}
