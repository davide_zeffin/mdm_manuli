/*****************************************************************************
    Copyright (c) 2001, SAPMarkets Palo Alto, USA, All rights reserved.

*****************************************************************************/
package com.sap.isa.auction.backend.crm.user;

import java.security.Permission;
import java.util.ArrayList;

import com.sap.isa.auction.backend.boi.businesspartner.ResponsibleSalesEmployeeData;
import com.sap.isa.auction.backend.boi.user.AuctionUserBackend;
import com.sap.isa.auction.backend.boi.user.AuctionUserData;
import com.sap.isa.auction.logging.CategoryProvider;
import com.sap.isa.backend.JCoHelper;
import com.sap.isa.backend.boi.isacore.AddressData;
import com.sap.isa.core.eai.BackendException;
import com.sap.isa.core.eai.sp.jco.JCoConnection;
import com.sap.isa.core.util.DecimalPointFormat;
import com.sap.isa.user.backend.boi.UserBaseData;
import com.sap.isa.user.backend.crm.UserBaseCRM;
import com.sap.isa.user.util.LoginStatus;
import com.sap.isa.user.util.RegisterStatus;
import com.sap.mw.jco.JCO;
import com.sap.tc.logging.Category;
import com.sap.tc.logging.Location;
import com.sap.tc.logging.Severity;

public class AuctionUserCRM extends UserBaseCRM
implements AuctionUserBackend {

	private final static Category cat = CategoryProvider.getSystemCategory();
	private final static Location loc =
		Location.getLocation(AuctionUserCRM.class);

    /**
     * List of Date Format mappings
     */
    private String date_format_1  = "TT.MM.JJJJ";
    private String date_format_2  = "MM/TT/JJJJ";
    private String date_format_3  = "MM-TT-JJJJ";
    private String date_format_4  = "JJJJ.MM.TT";
    private String date_format_5  = "JJJJ/MM/TT";
    private String date_format_6  = "JJJJ-MM-TT";



    /**
     * List of Decimal Format mappings
     */
    private String decimal_point_format_for_initial = "#.###.###,##";
    private String decimalseparator_for_inital      = ",";
    private String groupingseparator_for_intiial    = ".";

    private String decimal_point_format_for_X       = "#,###,###.##";
    private String decimalseparator_for_X           = ".";
    private String groupingseparator_for_X          = ",";

    private String decimal_point_format_for_Y       = "# ### ###,##";
    private String decimalseparator_for_Y           = ",";
    private String groupingseparator_for_Y          = " ";






    public void getUserDefaults(AuctionUserData aucUser,
                                String userid) throws BackendException {
        JCoConnection connection = null;
        cat.logT(Severity.DEBUG, loc, "Entering AuctionUserCRM - getDefaults(AuctionUserData,userid)");
        try {
            connection = getDefaultJCoConnection();

            // Get repository infos about the function
            JCO.Function userData = connection.getJCoFunction("BAPI_USER_GET_DETAIL");
            JCO.ParameterList importParams = userData.getImportParameterList();
            importParams.setValue (userid, "USERNAME");
            //call the function
            connection.execute(userData);

            //get the output parameter and tables

            //get the output structure
            JCO.ParameterList exportParams = userData.getExportParameterList();
            JCO.Structure logondata = exportParams.getStructure("LOGONDATA");
            JCO.Structure defaultdata = exportParams.getStructure("DEFAULTS");
			JCO.Structure addressData = exportParams.getStructure("ADDRESS");
            //Set the timezone
            aucUser.setTimeZone(logondata.getString("TZONE"));

            //Set date format.
            if(defaultdata.getString("DATFM").equals("1"))
                aucUser.setDateFormat(date_format_1);
            else if(defaultdata.getString("DATFM").equals("2"))
                aucUser.setDateFormat(date_format_2);
            else if(defaultdata.getString("DATFM").equals("3"))
                aucUser.setDateFormat(date_format_3);
            else if(defaultdata.getString("DATFM").equals("4"))
                aucUser.setDateFormat(date_format_4);
            else if(defaultdata.getString("DATFM").equals("5"))
                aucUser.setDateFormat(date_format_5);
            else if(defaultdata.getString("DATFM").equals("6"))
                aucUser.setDateFormat(date_format_6);

            //set decimal formats
            if(defaultdata.getString("DCPFM")==null ||
                    defaultdata.getString("DCPFM").length()==0) {
                aucUser.setDecimalFormat(decimal_point_format_for_initial);
                aucUser.setDecimalSeparator(decimalseparator_for_inital);
                aucUser.setGroupingSeparator(groupingseparator_for_intiial);
            } else if(defaultdata.getString("DCPFM").equals("X")) {
                aucUser.setDecimalFormat(decimal_point_format_for_X);
                aucUser.setDecimalSeparator(decimalseparator_for_X);
                aucUser.setGroupingSeparator(groupingseparator_for_X);
            } else if(defaultdata.getString("DCPFM").equals("Y")) {
                aucUser.setDecimalFormat(decimal_point_format_for_Y);
                aucUser.setDecimalSeparator(decimalseparator_for_Y);
                aucUser.setGroupingSeparator(groupingseparator_for_Y);
            }
            if(addressData != null)  {
            	aucUser.setLastName(addressData.getString("LASTNAME"));
            	aucUser.setFirstName(addressData.getString("FIRSTNAME"));
				aucUser.setSalutationText(addressData.getString("TITLE_P"));
				aucUser.setEMailAddress(addressData.getString("E_MAIL"));
            }
        }
        catch (JCO.Exception ex) {
            cat.log(Severity.DEBUG, loc, "Error in calling CRM function: ", ex);
            JCoHelper.splitException (ex);
        }
        finally {
            //always close the JCo connection and release the client
            connection.close();
            cat.logT(Severity.DEBUG, loc, "Exiting AuctionUserCRM - getDefaults(AuctionUserData,userid)");
        }

    }

	public ResponsibleSalesEmployeeData getSalesEmployeeFromInternetUser(String userName) 
														throws BackendException {
		// TODO Auto-generated method stub
		return null;
	}

	public ArrayList getAllSellers(ArrayList profiles) 
														throws BackendException {
		// TODO Auto-generated method stub
		return null;
	}

	public LoginStatus login(UserBaseData arg0, String arg1) throws BackendException {
		// TODO Auto-generated method stub
		return null;
	}

	public boolean changePassword(UserBaseData arg0, String arg1, String arg2) throws BackendException {
		// TODO Auto-generated method stub
		return false;
	}

	public boolean changeExpiredPassword(UserBaseData arg0, String arg1, String arg2) throws BackendException {
		// TODO Auto-generated method stub
		return false;
	}

	public void setLanguage(String arg0) throws BackendException {
		// TODO Auto-generated method stub
		
	}

	public RegisterStatus changeAddress(UserBaseData arg0, AddressData arg1) throws BackendException {
		// TODO Auto-generated method stub
		return null;
	}

	public void getAddress(UserBaseData arg0, AddressData arg1) throws BackendException {
		// TODO Auto-generated method stub
		
	}

    /* (non-Javadoc)
     * @see com.sap.isa.auction.backend.boi.user.AuctionUserBackend#getClient()
     */
    public String getClient() {
        // TODO Auto-generated method stub
        return null;
    }

    /* (non-Javadoc)
     * @see com.sap.isa.auction.backend.boi.user.AuctionUserBackend#getSystemId()
     */
    public String getSystemId() {
        // TODO Auto-generated method stub
        return null;
    }

	/* (non-Javadoc)
	 * @see com.sap.isa.user.backend.boi.UserBaseBackend#getDecimalPointFormat()
	 */
	public DecimalPointFormat getDecimalPointFormat() throws BackendException {
		// TODO Auto-generated method stub CRM 5.0
		return null;
	}

	/* (non-Javadoc)
	 * @see com.sap.isa.user.backend.boi.UserBaseBackend#getDateFormat(com.sap.isa.user.backend.boi.UserBaseData)
	 */
	public String getDateFormat(UserBaseData arg0) throws BackendException {
		// TODO Auto-generated method stub CRM 5.0
		return null;
	}

	/* (non-Javadoc)
	 * @see com.sap.isa.user.backend.boi.UserBaseBackend#hasPermissions(com.sap.isa.user.backend.boi.UserBaseData, java.security.Permission[])
	 */
	public Boolean[] hasPermissions(UserBaseData arg0, Permission[] arg1) throws BackendException {
		// TODO CRM 5.0 Auto-generated method stub
		return null;
	}

	/* (non-Javadoc)
	 * @see com.sap.isa.user.backend.boi.UserBaseBackend#isUmeLogonEnabled(com.sap.isa.user.backend.boi.UserBaseData)
	 */
	public Boolean isUmeLogonEnabled(UserBaseData arg0) throws BackendException {
		// TODO CRM 5.0 Auto-generated method stub
		return null;
	}
    
}
