/*
 * Title:        eBay Integration Project
 * Description:	 * SAP Labs LLC, the eBay auction project, also is known as Selling via eBay. 
 * Selling via eBay application provides SAP R/3 (and in the future CRM) 
 * customers to auction and sell inventory/products onto an online marketplace 
 * eBay. The application will provide the ability to Create, Schedule, Publish, 
 * Monitor and Close Auction Listings onto eBay and handle all the relevant 
 * Backend processing for Customers and Orders. 

 * Copyright:    Copyright (c) 2003
 * Company:      SAP Labs LLC
 * @author
 * @version 1.0
 */
package com.sap.isa.auction.backend.crm;

/**
 * @author i009657
 *
 * To change the template for this generated type comment go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
public interface AuctionRFCConstantsCRM {
	/**
	 * The property name of the profile name used in user authendication
	 */
	public static final String SELLER_PROFILENAME = "sellerProfile";
	
	/**
	 * The property name of the profile name for the administrator
	 */
	public static final String ADMIN_PROFILENAME = "adminProfile";
	
	/**
	 * The creation mode
	 * 
	 */
	public static final String MODE_CREATE = "A";

}
