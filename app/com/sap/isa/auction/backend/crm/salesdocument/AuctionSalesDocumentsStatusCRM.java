/*
 * Created on Feb 24, 2004
 *
 * To change the template for this generated file go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
package com.sap.isa.auction.backend.crm.salesdocument;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Locale;
import java.util.Properties;

import com.sap.mw.jco.JCO;
import com.sap.mw.jco.JCO.Structure;
import com.sap.tc.logging.*;
import com.sap.isa.auction.backend.boi.order.AuctionOrderStatusBackend;
import com.sap.isa.auction.backend.boi.order.AuctionOrderStatusData;
import com.sap.isa.auction.backend.crm.WrapperCRMAuction;
import com.sap.isa.auction.logging.CategoryProvider;
import com.sap.isa.backend.IsaBackendBusinessObjectBaseSAP;
import com.sap.isa.backend.JCoHelper;
import com.sap.isa.backend.boi.isacore.ShipToData;
import com.sap.isa.backend.boi.isacore.order.HeaderData;
import com.sap.isa.backend.boi.isacore.order.OrderStatusData;
import com.sap.isa.businessobject.Address;
import com.sap.isa.businessobject.ShipTo;
import com.sap.isa.businessobject.header.HeaderSalesDocument;
import com.sap.isa.core.TechKey;
import com.sap.isa.core.eai.BackendBusinessObjectParams;
import com.sap.isa.core.eai.BackendException;
import com.sap.isa.core.eai.sp.jco.JCoConnection;
/**
 * Since the OrderStatusCRM object can not be reused, let us create a
 * new object for our purpose.
 */
public class AuctionSalesDocumentsStatusCRM
	extends IsaBackendBusinessObjectBaseSAP
	implements AuctionOrderStatusBackend {
		
		private static final String ALL = "A";
		private static final String OPEN = "O";
		private static final String COMPLETED = "C";
		private static final String CANCELLED = "C";    //not yet supported, so cancelled are completed too
		private static final String ACCORDING_USER_STATUS = "U";
		private static final String READY_TO_PICKUP = "R";
		private HashMap shipCondMap = new HashMap();
		private final static Category cat = CategoryProvider.getSystemCategory();
		private final static Location loc =
			Location.getLocation(AuctionSalesDocumentsStatusCRM.class);
		private Properties initProperties;
		private BackendBusinessObjectParams initParameters;
		
		public Locale getBackendLocale() {
			return Locale.getDefault();
		}

		/**
		 * Initializes Business Object.
		 *
		 * @param props a set of properties which may be useful to initialize the object
		 * @param params a object which wraps parameters
		 */
		public void initBackendObject (Properties props,
												BackendBusinessObjectParams params)
										throws BackendException {
			initProperties = props;
			initParameters = params;
		}

		/**
		 * Read an order list from the backend. we filter also on the passing order ID
		 * From a list of order list which contains the GUID for a list of orders
		 * @param orderList             order list that will get its data from this method.
		 *        Also holds the search criteria.
		 * @exception BackendException  exception from R/3
		 */
		public void readOrderHeaders(OrderStatusData orderStatus)
			throws BackendException {
			cat.logT(Severity.DEBUG, loc, "readOrderHeaders begin in AuctionOrderStatusR3");
			AuctionOrderStatusData document = (AuctionOrderStatusData) orderStatus;
			JCoConnection connection = getDefaultJCoConnection();
			
			try {
				// get the ordelist
				ArrayList orderIds = document.getFilteredOrderHeaders();
				//while(orderIds.hasNext()) {
				for (int i = 0; i< orderIds.size(); i++){
					String orderGuid = (String) orderIds.get(i);
					callReadOrderHeader(connection, orderGuid, orderStatus);
				}
			} finally {
				//always close the JCo connection and release the client
				if (connection != null)
					connection.close();
			}
			
		}

		/**
		 * Reads order detail information (header and items). Use the detail strategy to get
		 * the sales document information. Sets the status information for order header and
		 * items.  Replaces the shipping condition key with the language dependent long
		 * text.
		 * 
		 * @param orderStatus           order status (holds header and items)
		 * @exception BackendException  exception from R/3
		 */
		public void readOrderStatus(OrderStatusData orderStatus)
			throws BackendException {

				//Step 1: get Shipping Condition based on language-call CRM_ISA_BASEKT_GETSHIPCOND
				cat.logT(Severity.DEBUG, loc, "Step1: get the ShipConditon");
				// get the language from backenenddatasetting
				String language = "EN";
				getShippingCondition(language);

				//Step2: read order header based on order GUID
				cat.logT(Severity.DEBUG, loc, "Step2: read order Header details");
				readOrderHeaderDetails(orderStatus);
		}
		
		
		/**
		 * Step1: Get the order shipping condition based on language and store it in
		 * shipCondMap
		 *
		 * @param lanague get from shop
		 */
		 private void getShippingCondition(String language) throws BackendException{
			JCoConnection connection = null;
			try {
				connection = getDefaultJCoConnection();
				JCO.Function shipCondFM =
							connection.getJCoFunction("CRM_ISA_BASKET_GETSHIPCOND");

				JCO.ParameterList importParams = shipCondFM.getImportParameterList();
				cat.logT(Severity.DEBUG, loc, "Set import parameter LANGUAGE "+language);
				importParams.setValue(language, "LANGUAGE");

				cat.logT(Severity.DEBUG, loc, "Execute FM CRM_ISA_BASKET_GETSHIPCOND");
				connection.execute(shipCondFM);

				JCO.ParameterList exportParams = shipCondFM.getExportParameterList();

				cat.logT(Severity.DEBUG, loc, "Get shipConditon result from the export table");
				JCO.Table shipCondTable = shipCondFM.getTableParameterList().getTable("SHIPPING_COND");
				int numTable = 0;
				while (numTable < shipCondTable.getNumRows()) {
					shipCondMap.put((Object)shipCondTable.getString("SHIP_COND"), (Object)shipCondTable.getString("DESCRIPTION"));
					shipCondTable.nextRow();
					numTable++;
				}
			} catch (JCO.Exception ex) {
				cat.log(Severity.DEBUG, loc, "Error in calling CRM function: ", ex);
				JCoHelper.splitException(ex);
			}
			finally {
				cat.logT(Severity.DEBUG, loc, "Finish Step1 FM call: CRM_ISA_BASKET_GETSHIPCOND");
				if (connection != null)
					connection.close();
			}
		 }

		/**
		 * Step2: read order header details information based on order guid, languae, country
		 *
		 * @param orderHeaders container to store the order header
		 * @param searchData contains order guid, shop info
		 */
		private void readOrderHeaderDetails(OrderStatusData orderStatus )
											throws BackendException{
			 JCoConnection connection = null;
			  //Step2: read order header based on order GUID
			 try{
				connection = getDefaultJCoConnection();
				JCO.Function orderHeaderFM =
							connection.getJCoFunction("CRM_ISA_BASKET_STATUS_ENH");

				cat.logT(Severity.DEBUG, loc, "start setting import parameters");

				JCO.ParameterList importParams = orderHeaderFM.getImportParameterList();
				String orderGuid = orderStatus.getTechKey().getIdAsString();
				if (orderGuid == null){
					cat.logT(Severity.ERROR, loc, "The Order GUID import parameter is null");
					connection.close();
					return;
				}
				if(orderGuid.length()<32)
					orderGuid = WrapperCRMAuction.getGuidFromSalesDocNumber(orderGuid, connection);
					
				cat.logT(Severity.DEBUG, loc, "set import parameter ORDER_GUID: " + orderGuid);
				importParams.setValue(orderGuid, "ORDER_GUID");

				importParams.setValue("EN", "LANGUAGE");

//				importParams.setValue(searchData.getShop().getCountry().toString(), "COUNTRY");

				cat.logT(Severity.DEBUG, loc, "Execute FM CRM_ISA_BASKET_STATUS_ENH");
				connection.execute(orderHeaderFM);
				// get the output tables
				JCO.Table msgTable = orderHeaderFM.getTableParameterList().getTable("MESSAGELINE");
				JCO.Structure oHeaderStruct = orderHeaderFM.getExportParameterList().getStructure("HEAD");
				JCO.Structure oStatusStruct = orderHeaderFM.getExportParameterList().getStructure("HEAD_STATUS");
				JCO.Structure oPartnerShiptoStruct = orderHeaderFM.getExportParameterList().getStructure("HEAD_PARTNER_SHIPTO");

				cat.logT(Severity.DEBUG, loc, "Set order header details");
				setOrderHeaders(oHeaderStruct, oStatusStruct, oPartnerShiptoStruct, orderStatus);
				//process the tables and give the necessary message to
				//user if errors occour
				//boolean hasError = processCRMTables(msgTable);
				//if(!hasError) {

				//}
			} catch (JCO.Exception ex) {
				cat.log(Severity.DEBUG, loc, "Error in calling CRM function: ", ex);
				JCoHelper.splitException(ex);
			}
			finally {
				//always close the JCo connection and release the client
				cat.logT(Severity.DEBUG, loc, "Finish Step2 FM call: CRM_ISA_BASKET_STATUS_ENH");
				if (connection != null)
					connection.close();
			}
		}

		/**
		 * Method to get the order header data in an easy way
		 * @param connection
		 * @param orderGuid
		 * @throws BackendException
		 */
		private void callReadOrderHeader(JCoConnection connection, String orderGuid,
										OrderStatusData orderStatus )
		throws BackendException{
			try{
			   JCO.Function orderHeaderFM =
						   connection.getJCoFunction("CRM_ISA_BASKET_STATUS_ENH");

			   cat.logT(Severity.DEBUG, loc, "start setting import parameters");

			   JCO.ParameterList importParams = orderHeaderFM.getImportParameterList();
			   if (orderGuid == null){
				   cat.logT(Severity.ERROR, loc, "The Order GUID import parameter is null");
				   connection.close();
				   return;
			   }
			   if(orderGuid.length()<32)
				   orderGuid = WrapperCRMAuction.getGuidFromSalesDocNumber(orderGuid, connection);
				   
			   cat.logT(Severity.DEBUG, loc, "set import parameter ORDER_GUID: " + orderGuid);
			   importParams.setValue(orderGuid, "ORDER_GUID");

			   importParams.setValue("EN", "LANGUAGE");

//			   importParams.setValue(searchData.getShop().getCountry().toString(), "COUNTRY");

			   cat.logT(Severity.DEBUG, loc, "Execute FM CRM_ISA_BASKET_STATUS_ENH");
			   connection.execute(orderHeaderFM);
			   // get the output tables
			   JCO.Table msgTable = orderHeaderFM.getTableParameterList().getTable("MESSAGELINE");
			   JCO.Structure oHeaderStruct = orderHeaderFM.getExportParameterList().getStructure("HEAD");
			   JCO.Structure oStatusStruct = orderHeaderFM.getExportParameterList().getStructure("HEAD_STATUS");
			   JCO.Structure oPartnerShiptoStruct = orderHeaderFM.getExportParameterList().getStructure("HEAD_PARTNER_SHIPTO");
			   addOrderHeaders(oHeaderStruct, oStatusStruct, oPartnerShiptoStruct, orderStatus);
			} catch (JCO.Exception ex) {
				cat.log(Severity.DEBUG, loc, "Error in calling CRM function: ", ex);
				JCoHelper.splitException(ex);
			}
		}
		
		 
		/**
		 * @param oHeaderStruct
		 * @param oStatusStruct
		 * @param oPartnerShiptoStruct
		 * @param orderStatus
		 */
		private void addOrderHeaders(Structure oHeaderStruct, 
									Structure oStatusStruct, 
									Structure oPartnerShiptoStruct, 
						OrderStatusData orderStatus) {
			HeaderData orderHeader = fillOrderHeaderData(oHeaderStruct,
							oStatusStruct,
							oPartnerShiptoStruct);			

			orderStatus.addOrderHeader(orderHeader);
			
		}

		/**
		 * set the order ids
		 *
		 * @param table JCO table which contains the guid
		 * @return nothing.
		 */
		private void setOrderHeaders(JCO.Structure oHeaderStruct, JCO.Structure statusStruct,
									 JCO.Structure oPartnerShipToStruct,
									 OrderStatusData readData) {
			HeaderData orderHeader = fillOrderHeaderData(oHeaderStruct,
										statusStruct,
										oPartnerShipToStruct);		
										
			readData.getOrder().setHeader(orderHeader);	
			readData.addOrderHeader(orderHeader);
			readData.setOrderHeader(orderHeader);
			readData.setDocumentExist(true);
		}
	
		/**
		 * 
		 * */
		
		

		/**
		 * @param oHeaderStruct
		 * @param statusStruct
		 * @param oPartnerShipToStruct
		 * @return
		 */
		private HeaderData fillOrderHeaderData(Structure oHeaderStruct, 
												Structure statusStruct, 
												Structure oPartnerShipToStruct) {
			HeaderData orderHeader = new HeaderSalesDocument();
			orderHeader.setTechKey(new TechKey(oHeaderStruct.getString("GUID")));
			orderHeader.setSalesDocNumber(oHeaderStruct.getString("OBJECT_ID"));
			orderHeader.setPurchaseOrderExt(oHeaderStruct.getString("PO_NUMBER_SOLD"));
			orderHeader.setDescription(oHeaderStruct.getString("DESCRIPTION"));
			//String createdAt = formatDateL(oHeaderStruct.getString("CREATED_AT"));
			//String changedAt = formatDateL(oHeaderStruct.getString("CHANGED_AT"));
			orderHeader.setCreatedAt(oHeaderStruct.getString("CREATED_AT"));
			orderHeader.setChangedAt(oHeaderStruct.getString("CHANGED_AT"));
			//if (oHeaderStruct.getString("CHANGED_AT").equals(""))
				// Not yet changed, so last changed date is created at date!!
			  //  orderHeader.setChangedAt(createdAt);
			orderHeader.setCurrency(oHeaderStruct.getString("CURRENCY"));
			orderHeader.setNetValue(oHeaderStruct.getString("NET_VALUE"));
			orderHeader.setTaxValue(oHeaderStruct.getString("TAX_VALUE"));
			orderHeader.setFreightValue(oHeaderStruct.getString("FREIGHT_VALUE"));
			orderHeader.setGrossValue(oHeaderStruct.getString("GROSS_VALUE"));
			orderHeader.setProcessType(oHeaderStruct.getString("PROCESS_TYPE"));
			orderHeader.setShipCond((String)shipCondMap.get((Object)oHeaderStruct.getString("SHIP_COND")));

			// Header Shipto Data
			if (((oPartnerShipToStruct.getField("NAME1") != null) &&
				 (oPartnerShipToStruct.getField("NAME1").getString() != null) &&
				 (oPartnerShipToStruct.getField("NAME1").getString().length() > 0)) ||
				 ((oPartnerShipToStruct.getField("LASTNAME") != null) &&
				 (oPartnerShipToStruct.getField("LASTNAME").getString() != null) &&
				 (oPartnerShipToStruct.getField("LASTNAME").getString().length() > 0)))

			{
				try {
					ShipToData shipTo = new ShipTo();
					shipTo.setShortAddress(oPartnerShipToStruct.getString("ADDRESS_SHORT"));
					shipTo.setTechKey(new TechKey(oPartnerShipToStruct.getString("LINE_KEY")));
					orderHeader.setShipToData(shipTo);
					Address address = new Address();
					address.setName1(oPartnerShipToStruct.getString("NAME1"));
					address.setName2(oPartnerShipToStruct.getString("NAME2"));
					address.setLastName(oPartnerShipToStruct.getString("LASTNAME"));
					address.setFirstName(oPartnerShipToStruct.getString("FIRSTNAME"));
					address.setStreet(oPartnerShipToStruct.getString("STREET"));
					address.setHouseNo(oPartnerShipToStruct.getString("HOUSE_NO"));
					address.setCity(oPartnerShipToStruct.getString("CITY"));
					address.setPostlCod1(oPartnerShipToStruct.getString("POSTL_COD1"));
					address.setCountry(oPartnerShipToStruct.getString("COUNTRY"));
					address.setRegion(oPartnerShipToStruct.getString("REGION"));
					shipTo.setAddress(address);
				} catch (Exception e) {
					cat.log(Severity.DEBUG, loc, "Error when set shipTo address", e);
				}
			}

			// FILL HEADERSTATUS (OVERALL)
			//orderHeader.setValidTo(formatDateL(statusStruct.getString("QUOTEVALIDITY")));
			//orderHeader.setChangeable(statusStruct.getString("CHANGEABLE"));
			String status = statusStruct.getString("COMPLETION");
			if (status.equals("I1005")) {                                    // COMPLETED
			  orderHeader.setStatusCompleted();
			} else if (status.equals("I1032")) {                             // CANCELLED
				orderHeader.setStatusCancelled();
			} else {                                                            // OPEN
				orderHeader.setStatusOpen();
			}
			// ACCEPTED status on header level
			if (statusStruct.getString("QUOTE_ACCEPTED").equals("X")) {        // ACCEPTED
				orderHeader.setStatusAccepted();
			}

			// RELEASED status on header level
			if (statusStruct.getString("QUOTE_RELEASED").equals("X")) {        // RELEASED
				orderHeader.setStatusReleased();
			}

			// DELIVERY STATUS on header level
			if (statusStruct.getString("COMPLETION_DLV").equals("A")) {        // OPEN
				orderHeader.setDeliveryStatusOpen();
			} else {                                                            // COMPLETED
				orderHeader.setDeliveryStatusCompleted();
			}
			return orderHeader;
		}

		/* (non-Javadoc)
		 * @see com.sap.isa.backend.boi.isacore.SalesDocumentStatusBackend#getItemConfigFromBackend(com.sap.isa.backend.boi.isacore.order.OrderStatusData, com.sap.isa.core.TechKey)
		 */
		public void getItemConfigFromBackend(OrderStatusData arg0, TechKey arg1) throws BackendException {
			throw new UnsupportedOperationException();
		}


}
