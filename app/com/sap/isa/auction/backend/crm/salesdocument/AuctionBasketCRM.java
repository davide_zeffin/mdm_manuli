

package com.sap.isa.auction.backend.crm.salesdocument;

import com.sap.isa.auction.backend.crm.WrapperCRMAuction;
import com.sap.isa.backend.JCoHelper;
import com.sap.isa.backend.boi.isacore.ConnectedDocumentData;
import com.sap.isa.backend.boi.isacore.ShopData;
import com.sap.isa.backend.boi.isacore.order.BasketData;
import com.sap.isa.backend.boi.isacore.order.CopyMode;
import com.sap.isa.backend.boi.isacore.order.OrderTemplateData;
import com.sap.isa.backend.boi.isacore.order.PartnerListData;
import com.sap.isa.backend.boi.isacore.order.PartnerListEntryData;
import com.sap.isa.backend.crm.MessageCRM;
import com.sap.isa.backend.crm.ShopCRM;
import com.sap.isa.backend.crm.WrapperCrmIsa;
import com.sap.isa.backend.crm.WrapperCrmIsa.ReturnValue;
import com.sap.isa.backend.crm.order.BasketCRM;
import com.sap.isa.core.TechKey;
import com.sap.isa.core.eai.BackendException;
import com.sap.isa.core.eai.sp.jco.ExtensionSAP;
import com.sap.isa.core.eai.sp.jco.JCoConnection;
import com.sap.isa.core.logging.IsaLocation;
import com.sap.mw.jco.JCO;

/** 
 * The backend implementation for creation of basket
 * in auction scenario
 */
public class AuctionBasketCRM extends BasketCRM {
	static final private IsaLocation log = IsaLocation.getInstance(BasketCRM.class.getName());
	/**
	 * Create a basket in the backend with reference to a predecessor document.
	 *
	 * @param predecessorKey
	 * @param copyMode
	 * @param processType
	 * @param soldToKey
	 * @param shop
	 * @param basket
	 */
	public void createWithReferenceInBackend(
			TechKey predecessorKey,
			CopyMode copyMode,
			String processType,
			TechKey soldToKey,
			ShopData shop,
			BasketData basket
			) throws BackendException {

		final String METHOD_NAME = "createWithReferenceInBackend()";
		log.entering(METHOD_NAME);
		String process = null;
		String systemStatus = null;
		// get JCOConnection
		JCoConnection aJCoCon = getDefaultJCoConnection();
		// actually the predecessorKey is not techkey,
		// here do the conversion
		String docNumber = predecessorKey.getIdAsString();
		TechKey predecessorTechKey = new TechKey(WrapperCRMAuction.getGuidFromSalesDocNumber(docNumber,
													aJCoCon));
		// CRM_ISA_BASKET_CREATE
		WrapperCrmIsa.ReturnValue retVal =
				crmIsaBasketCreateWithRef(
						shop.getTechKey(),
						copyMode,
						ShopCRM.getCatalogId(shop.getCatalog()),
						ShopCRM.getVariantId(shop.getCatalog()),
						processType,
						process,
						systemStatus,
				predecessorTechKey,
						soldToKey,
						basket,
						aJCoCon);

		// add messages to the basket bo
		if (retVal.getReturnCode().length() == 0) {
			MessageCRM.addMessagesToBusinessObject(
				basket,
				retVal.getMessages());
		}
		else {
			MessageCRM.logMessagesToBusinessObject(
				basket,
				retVal.getMessages());
		}
		aJCoCon.close();
		log.exiting();
	}
	/**
	 * Call the backend basket creation functionality
	 * @param predecessorKey
	 * @param copyMode
	 * @param processType
	 * @param soldToKey
	 * @param string
	 * @param string2
	 * @param string3
	 * @param basket
	 * @param aJCoCon
	 * @return
	 */
	private ReturnValue crmIsaBasketCreateWithRef(
						TechKey shopKey,
						CopyMode copyMode,
						String catalogKey,
						String catalogVariant,
						String processType,
						String process,
						String systemStatus,
						TechKey predecessorKey,
						TechKey soldToKey, 
						BasketData posd,
						JCoConnection cn) throws BackendException  {
		// Create basket with API, th ebasket should have the
		// manual price and auction items.
		// CRM_ISA_AUC_BASKET_CREATE_REF
		final String METHOD_NAME = "crmIsaBasketCreateWithRef()";
		log.entering(METHOD_NAME);
		try {
			JCO.Function function =
					cn.getJCoFunction("CRM_ISA_AUC_BASKET_CREATE_REF");

			// getting import parameter
			JCO.ParameterList importParams =
					function.getImportParameterList();

			// getting export parameters
			JCO.ParameterList exportParams =
					function.getExportParameterList();

			// get the import structure
			JCO.Structure basketHeadStructure =
					importParams.getStructure("BASKET_HEAD");

			// setting the id of the shop
			JCoHelper.setValue(basketHeadStructure, shopKey, "SHOP_ID");

			// setting the id of the catalog
			JCoHelper.setValue(importParams, catalogKey, "CATALOG_ID");

			// setting the id of the catalog variant
			JCoHelper.setValue(importParams, catalogVariant, "VARIANT_ID");
            
			// setting the precessor ID
			JCoHelper.setValue(importParams, predecessorKey, "PREDECESSOR_GUID");
			// handle must be set for correct assignment of extensions
			posd.getHeaderData().createUniqueHandle();
            
			if (!posd.getHeaderData().getAssignedCampaignsData().isEmpty() &&
				 posd.getHeaderData().getAssignedCampaignsData().getCampaign(0) != null) {
				// setting the id of the campaign
				String campaignKey = posd.getHeaderData().getAssignedCampaignsData().getCampaign(0).getCampaignGUID().getIdAsString();
				JCoHelper.setValue(importParams, campaignKey, "CAMPAIGN_GUID");
				
				//  setting the object type of the campaign element
				JCoHelper.setValue(importParams, posd.getHeaderData().getAssignedCampaignsData().getCampaign(0).getCampaignTypeId(), "CAMPAIGN_OBJECT_TYPE");
			}
			else {
				// setting the id of the campaign
				String campaignKey = posd.getCampaignKey();
				JCoHelper.setValue(importParams, campaignKey, "CAMPAIGN_GUID");
				
				//  setting the object type of the campaign element
				JCoHelper.setValue(importParams, posd.getCampaignObjectType(), "CAMPAIGN_OBJECT_TYPE");
			}

			// setting the process type
			if (processType != null) {
				basketHeadStructure.setValue(processType, "PROCESS_TYPE");
				JCoHelper.setValue(importParams, process, "PROCESS_TYPE");
			}

			if (process != null) {
				JCoHelper.setValue(importParams, process, "IV_PROCESS");
			}
			else if (posd instanceof OrderTemplateData) {
			// setting of status in backend for ordertemplates
				JCoHelper.setValue(importParams, "TOKO", "IV_PROCESS");
			}

			// set predecessor informations
			JCO.Table docFlowTable = function.getTableParameterList().getTable("IT_DOC_FLOW");
			//ArrayList docFlowList = new ArrayList(posd.getHeaderData().getPredecessorList());
			//Iterator it = docFlowList.iterator();
			//while (it.hasNext()) {
				//ConnectedDocumentData conDoc = (ConnectedDocumentData) it.next();
				docFlowTable.appendRow();
				//JCoHelper.setValue(docFlowTable, conDoc.getTechKey(),               "GUID");
				JCoHelper.setValue(docFlowTable, predecessorKey,                "GUID");
				JCoHelper.setValue(docFlowTable, ConnectedDocumentData.IL_TRANSFER_AND_UPDATE,    "VONA_KIND");
//				if (conDoc.getTransferUpdateType().equals(ConnectedDocumentData.UNDEFINED)) {
//					JCoHelper.setValue(docFlowTable, ConnectedDocumentData.IL_NO_TRANSFER_AND_UPDATE,    "VONA_KIND");
//				}
//				else {
//					JCoHelper.setValue(docFlowTable, conDoc.getTransferUpdateType(),    "VONA_KIND");
//				}
			//}
			
			//for org data determination
			PartnerListData partnerList = posd.getHeaderData().getPartnerListData();
			PartnerListEntryData partner = partnerList.getSoldToData();
			if (soldToKey.isInitial()){ 
				if (partner != null) {
					// setting the id of the soldto
					JCoHelper.setValue(importParams, partner.getPartnerTechKey(), "SOLDTO_GUID");
				}
			}else{
				JCoHelper.setValue(importParams, soldToKey, "SOLDTO_GUID");
			}

			// set extension
			JCO.Table extensionTable = function.getTableParameterList().getTable("EXTENSION_CREATE");

			// add all extension to the given JCo table.
			ExtensionSAP.fillExtensionTable(extensionTable, posd);

			// call the function
			cn.execute(function);

			// get the output parameter
			posd.setTechKey(JCoHelper.getTechKey(exportParams, "BASKET_GUID"));
			posd.getHeaderData().setTechKey(JCoHelper.getTechKey(exportParams, "BASKET_GUID"));

			// delete handle because the guid is already available and the handle should not
			// be used anymore. 
			posd.getHeaderBaseData().setHandle("");

			// write some useful logging information
			if (log.isDebugEnabled()) {
				logCall("CRM_ISA_AUC_BASKET_CREATE_REF", importParams, exportParams);
				logCall("CRM_ISA_AUC_BASKET_CREATE_REF", basketHeadStructure, null);
			}

			// the messages
			// JCO.Table messages;
			JCO.Table messages =
					function.getTableParameterList().getTable("MESSAGELINE");
			return new ReturnValue(messages, "");

		}
		catch (JCO.Exception ex) {
			logException("CRM_ISA_AUC_BASKET_CREATE_REF", ex);
			JCoHelper.splitException(ex);
		} finally {
			log.exiting();
		}
		return null;
	}
	
	/* log an exception with level ERROR */
	private static void logException(String functionName, JCO.Exception ex) {

		String message = functionName
					+ " - EXCEPTION: GROUP='"
					+ JCoHelper.getExceptionGroupAsString(ex) + "'"
					+ ", KEY='"   + ex.getKey() + "'";

		log.error("b2b.exception.backend.logentry", new Object [] { message }, ex);

		if (log.isDebugEnabled()) {
			log.debug(message);
		}
	}
	
	private static void logCall(String functionName,
			JCO.Record input,
			JCO.Record output) {
		WrapperCrmIsa.logCall(functionName, input, output, log);
	}

	private static void logCall(String functionName,
			JCoHelper.RecordWrapper input,
			JCoHelper.RecordWrapper output) {
		WrapperCrmIsa.logCall(functionName, input, output, log);
	}
}
