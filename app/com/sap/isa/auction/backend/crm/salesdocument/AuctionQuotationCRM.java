/*
 * Title:        eBay Integration Project
 * Description:	 * SAP Labs LLC, the eBay auction project, also is known as Selling via eBay. 
 * Selling via eBay application provides SAP R/3 and CRM 
 * customers to auction and sell inventory/products onto an online marketplace 
 * eBay. The application will provide the ability to Create, Schedule, Publish, 
 * Monitor and Close Auction Listings onto eBay and handle all the relevant 
 * Backend processing for Customers and Orders. 

 * Copyright:    Copyright (c) 2004
 * Company:      SAP Labs LLC
 * @author
 * @version 1.0
 */
package com.sap.isa.auction.backend.crm.salesdocument;

import java.util.Iterator;
import java.util.Properties;

import com.sap.isa.auction.backend.BackendHelper;
import com.sap.isa.auction.backend.boi.AuctionData;
import com.sap.isa.auction.backend.boi.order.AuctionQuotationBackend;
import com.sap.isa.auction.backend.boi.order.AuctionQuotationData;
import com.sap.isa.auction.backend.crm.AuctionRFCConstantsCRM;
import com.sap.isa.auction.backend.crm.WrapperCRMAuction;
import com.sap.isa.auction.logging.CategoryProvider;
import com.sap.isa.backend.JCoHelper;
import com.sap.isa.backend.boi.isacore.order.ItemBaseData;
import com.sap.isa.backend.boi.isacore.order.ItemData;
import com.sap.isa.backend.boi.isacore.order.ItemListBaseData;
import com.sap.isa.backend.boi.isacore.order.QuotationData;
import com.sap.isa.backend.crm.MessageCRM;
import com.sap.isa.backend.crm.WrapperCrmIsa;
import com.sap.isa.backend.crm.WrapperCrmIsa.ReturnValue;
import com.sap.isa.backend.crm.order.QuotationCRM;
import com.sap.isa.businessobject.item.ItemList;
import com.sap.isa.businessobject.item.ItemSalesDoc;
import com.sap.isa.core.TechKey;
import com.sap.isa.core.eai.BackendBusinessObjectParams;
import com.sap.isa.core.eai.BackendException;
import com.sap.isa.core.eai.sp.jco.JCoConnection;
import com.sap.mw.jco.JCO;
import com.sap.mw.jco.JCO.ParameterList;
import com.sap.mw.jco.JCO.Table;
import com.sap.tc.logging.Category;
import com.sap.tc.logging.Location;
import com.sap.tc.logging.Severity;
public class AuctionQuotationCRM
	extends QuotationCRM
	implements AuctionQuotationBackend {
	/** The logging instance. */
	private final static Category cat = CategoryProvider.getSystemCategory();
	private final static Location loc =
		Location.getLocation(AuctionQuotationCRM.class);

	/** The backend properties are read within initBackendObject and later on used. */
	protected Properties props;
	
	private static final String TYPE = "type";
	private static final String UNIT = "unit";   // temporary 
	private static final String QUOTATION_VALID_TO = "09.09.9999";
	public void initBackendObject(Properties props,
								  BackendBusinessObjectParams params)
		throws BackendException {
		this.props = props;
	}

	/* 
	 * Create and save the quote to CRM backend
	 * @param AuctionQuotationData that holds all of the quotation 
	 * data use this to save to backend
	 */
	public void createInBackend(AuctionQuotationData quotation)
		throws BackendException {
			cat.logT(Severity.DEBUG, loc, "Start CreateQuotation(AuctionQuotationData)");
			JCoConnection connection = null;
			JCO.Table messages = null;
			String quotationId = null;
			try {
				connection = getDefaultJCoConnection();
				JCO.Function function = connection.getJCoFunction("CRM_ISA_AUC_QUOTE_CREATE_MULT");

				// Get import parameters
				JCO.ParameterList importParams = function.getImportParameterList();

				// Set the import parameter value
				String startDate = null;
				String expDate = QUOTATION_VALID_TO;
				// Read the process name from the eai-config.xml params
				String iv_process = quotation.getHeaderData().getDocumentType();
				String salesOrg = quotation.getHeaderData().getSalesOrg();
				String disChannel = quotation.getHeaderData().getDisChannel();
				String division = quotation.getHeaderData().getDivision();
				String currency = quotation.getHeaderData().getCurrency();
				if ((currency == null) || (currency.trim().length()==0)){
					currency = "USD";
					cat.logT(Severity.WARNING, loc, "The default currency is used" + currency);
				}
				cat.logT(Severity.DEBUG, loc, "startdate: " +startDate);
				cat.logT(Severity.DEBUG, loc, "expdate: " +expDate);
				cat.logT(Severity.DEBUG, loc, "The process is: " +iv_process);
				cat.logT(Severity.DEBUG, loc, "The sales org: " + salesOrg);
				cat.logT(Severity.DEBUG, loc, "The distri channel:" + disChannel);
				cat.logT(Severity.DEBUG, loc, "The currency:" + quotation.getHeaderData().getCurrency());
				cat.logT(Severity.DEBUG, loc, "The Division:" + division);
				cat.logT(Severity.DEBUG, loc, "The Seller:" + quotation.getResponsibleEmployee());

				importParams.setValue(startDate, "IV_VALID_FROM");
				importParams.setValue(expDate, "IV_VALID_TO");
				importParams.setValue(quotation.getResponsibleEmployee().toUpperCase(), //should be BP number
									  "IV_EMPLOYEE_NUM");
				importParams.setValue(iv_process, "IV_PROCESS");
				if (salesOrg != null){
					importParams.setValue(salesOrg,
									  "IV_SALES_ORG");
					importParams.setValue(quotation.getRespSalesOrg(),
										  "IV_SALES_ORG_RESP");
				}
				if (disChannel != null)
					importParams.setValue(disChannel,
									  "IV_DIS_CHANNEL");
				if (division != null)
					importParams.setValue(division,"IV_DIVISION");

				importParams.setValue(AuctionRFCConstantsCRM.MODE_CREATE, "IV_MODE");
				importParams.setValue(currency, "IV_CURRENCY");

				//fill the import table: IT_PRODUCT and  IT_QUANTITY
				setItemInfo(importParams, quotation);

				//execute the quotation
				connection.execute(function);

				// Get the exporting table
				JCO.Table returnTable = function.getExportParameterList().
											getTable("ET_RETURN");
				JCO.Table savedTable = function.getExportParameterList().
											getTable("ET_SAVED_OBJECTS");

				//process the tables and give the necessary message to user if errors occour
				BackendHelper.processBackendReturn(returnTable);
				//MessageCRM.addMessagesToBusinessObject(quotation, returnTable);


				// set the document number and GUID from the saved table
				setGuidFromSavedTable(savedTable, quotation);


			}
			catch (JCO.Exception ex) {
				cat.log(Severity.DEBUG, loc, "Error in calling CRM function: ", ex);
				JCoHelper.splitException (ex);
			}
			finally {
				//always close the JCo connection and release the client
				if (connection != null)
					connection.close();
			}
		}
		
	/**
	 * Set the GUID and the sales document numebr to the quotation object
	 * @param savedTable
	 * @param quotation
	 */
	private void setGuidFromSavedTable(Table savedTable, AuctionQuotationData quotation) 
										throws BackendException {
		String value = null;
		String salesDocNumber = null;
		/// here only one row we are talking about
		if (savedTable.getNumRows() > 0){
			do {
				value = savedTable.getString("GUID");
				salesDocNumber = savedTable.getString("OBJECT_ID");
			}
			while (savedTable.nextRow());
			if (value != null) {
				quotation.getHeaderData().setSalesDocNumber(salesDocNumber);
				quotation.getHeaderData().setTechKey(new TechKey(value));
				cat.logT(Severity.DEBUG, loc, "the quotation GUID Id: " + value);
				cat.logT(Severity.DEBUG, loc, "the sales doc number is : " + salesDocNumber);
				cat.logT(Severity.DEBUG, loc, "The quotation document " + salesDocNumber +
							" is saved successfully");
			}
			else {
				cat.logT(Severity.DEBUG, loc, "Table " + savedTable.getName() + " doesn't contain the GUID!");
				throw new BackendException("Quotation contains error, not SAVED!!!");
			}
		}
		else {
			cat.logT(Severity.DEBUG, loc, "Table " + savedTable.getName() + " is empty");
			cat.logT(Severity.ERROR, loc, "The quotation sales document has not been created");
			throw new BackendException("Quotation contains error, not SAVED!!!");
		}
	}

	/**
	 * Set the product info to the item level, currently we do not 
	 * suppor the broken lot auction.
	 * @param importParams
	 * @param quotation
	 */
	private void setItemInfo(ParameterList importParams, AuctionQuotationData quotation) {
		cat.logT(Severity.DEBUG, loc, "In the setting attributes for the item level");
		String type = quotation.getTotalManualPriceCondition();
		if ((type == null ) || (type.trim().length() <= 1)){
			props.getProperty(TYPE);
		}
		String unit = props.getProperty(UNIT);

		Iterator iter = quotation.getItemListData().iterator();
		JCO.Table itemTbl = importParams.getTable("IT_ITEMS");
		
		// Get the product from the quotation itme level
		while(iter.hasNext()) {
			itemTbl.appendRow();
			ItemSalesDoc item= (ItemSalesDoc)iter.next();
			itemTbl.setValue(item.getProduct(),"PRODUCT");
			itemTbl.setValue(item.getQuantity(), "QUANTITY");
//			itemTbl.setValue(quotation.getHeaderData().getGrossValue(),
//								"COND_RATE");
//			itemTbl.setValue(type, "COND_TYPE");
			try {
				if ((item.getUnit() != null) && (item.getUnit().trim().
														length()>0)){
					itemTbl.setValue(item.getUnit(), "COND_UNIT");
				}else{
					if (unit != null)
						itemTbl.setValue(unit, "COND_UNIT");
				}
			}catch (Exception ex){
				cat.log(Severity.ERROR, loc, "Errors Item Unit is problematic ", ex);
			}
		}

		cat.logT(Severity.DEBUG, loc, "End of the setting auction item data:" + itemTbl);
	}

	/* This API is not supported currently
	 * It will be supported later
	 */
	public void createInBackend(AuctionData auction) throws BackendException {
		throw new UnsupportedOperationException();
	}

	/* This API, change the quotation attributes in the backend
	 * @param AuctionQuotationdata
	 */
	public void modifyInBackend(AuctionQuotationData quotation)
		throws BackendException {
			throw new UnsupportedOperationException();
	}

	/* This API calls to set the field Ext. reference to the eBayListing ID
	 * linking the sales order with eBay listing ID
	 * It calls the RFC change and commit the transaction.
	 * Call the CRM_AUC_QUOTATION_CHANGEHEAD
	 * 
	 * @see com.sap.isa.auction.backend.boi.order.AuctionQuotationBackend#setLisingIdInBackend(com.sap.isa.auction.backend.boi.order.AuctionQuotationData)
	 * @param AuctionQuotationData
	 */
	public void setLisingIdInBackend(AuctionQuotationData quotation)
		throws BackendException {
		if (!(quotation instanceof AuctionQuotationData)){
			throw new BackendException ("Not right type of Backend Object" );
		}
		cat.logT(Severity.DEBUG, loc, "Beginning of setListingIdInBackend!");
		// get
		JCoConnection jcoConnection = getDefaultJCoConnection();
		try {
			WrapperCRMAuction.checkExistenceTechKey(jcoConnection, quotation);
		} catch (Exception ex) {
			//always close the JCo connection and release the client
			if (jcoConnection != null)
				jcoConnection.close();
			throw new BackendException(ex);
		}
		if ((quotation.getTechKey() != null) && (quotation.getTechKey().getIdAsString().length()>1)) {
			try {
				Thread modifyingThread = new ModifyingThread(jcoConnection, 
												quotation.getTechKey().getIdAsString(),
												quotation.getPurchaseNumber());
				modifyingThread.start();
			} catch (Exception ex) {
				cat.log(Severity.ERROR, loc, "In setListingIdInBackend JCO exception", ex);
			} // TRy
		
		} else {
			cat.log(Severity.WARNING, loc, "The TechKey is still null");			
		} 			
		
		cat.logT(Severity.DEBUG, loc, "Ending method setListingIdInBackend!");

	}
	

	/**
	 * Only set the ID to the quotation
	 * @param quotation
	 * @param jcoConnection
	 * @return
	 */
	private ReturnValue quotationSetListingId(String  docGuid, String listingId, 
					JCoConnection cn) throws BackendException {
		cat.logT(Severity.DEBUG, loc, "Beginning method quotationSetListingId!");				
		// first of all set the list
		//String listingId = quotation.getPurchaseNumber();
		ReturnValue retVal = null;
		String retCode = "";
		//
		//String docGuid = quotation.getTechKey().toString();
		cat.logT(Severity.DEBUG, loc, "docGuid = " + docGuid);
			
		//HeaderData headerData = quotation.getHeaderData();
		//cat.logT(Severity.DEBUG, loc, "quotation = " + quotation);
		try {

			JCO.Function function =
					cn.getJCoFunction("CRM_AUC_QUOTE_CHANGEHEAD");

			// getting import parameter
			JCO.ParameterList importParams =
					function.getImportParameterList();

			// setting the id of the sales document
			JCoHelper.setValue(importParams, docGuid, "DOC_GUID");

			// get the import structure
			JCO.Structure changeHeadStructure =
				   importParams.getStructure("DOC_HEAD");
			JCO.Structure changeHeadStructureX =
				   importParams.getStructure("DOC_HEAD_X");
				   
			JCoHelper.setValue(changeHeadStructure, docGuid, "GUID");

			JCoHelper.RecordWrapper importParamStruct =
				new JCoHelper.RecordWrapper(changeHeadStructure,
						changeHeadStructureX);

//			importParamStruct.setValue(headerData.getDescription(),      
//											"DESCRIPTION");
//			importParamStruct.setValue(headerData.getReqDeliveryDate(), 
//											"REQ_DLV_DATE");
			cat.logT(Severity.DEBUG, loc, "The listing Id is " + listingId);
			importParamStruct.setValue(listingId, 
											"PO_NUMBER_SOLD");
			
			// after creation headerData is empty and shipcond from backend has not to be overwritten
//			if ((headerData.getShipCond() != null) && !headerData.getShipCond().equals("")) {
//				importParamStruct.setValue(headerData.getShipCond(),         
//											"SHIP_COND");
//			}
//			changeHeadStructure.setValue(headerData.getHandle(),      
//											"HANDLE");

			cat.logT(Severity.DEBUG, loc, "changeHeadStructure=" + changeHeadStructure);
			cat.logT(Severity.DEBUG, loc, "changeHeadStructureX=" + changeHeadStructureX);
			// call the function
			cn.execute(function);
			// get the output parameter
			JCO.ParameterList exportParams = function.getExportParameterList();

			JCO.Table messages = function.getTableParameterList().getTable("MESSAGELINE");
			
			cat.logT(Severity.DEBUG, loc, "exportParams=" + exportParams);
			cat.logT(Severity.DEBUG, loc, "messages=" + messages);
			
			retVal = new ReturnValue(messages, retCode);
			BackendHelper.processBackendReturn(messages, false);
		}
		catch (JCO.Exception ex) {
			cat.log(Severity.ERROR, loc, "CRM_AUC_QUOTE_CHANGEHEAD", ex);
			JCoHelper.splitException(ex);
		}
		return retVal;
	}


	/**
	 * Cancels quotation in the backend. Not implemented for ISA crm.
	 * 
	 * it will change the status of the quotation to release the
	 * reservation, it uses Rejection reason to change the 
	 * status of a quotation. Currently it will set the rejection reason 
	 * for each of the item, then the complete status of the quotation will be
	 * changed, in CRM this method will complete the quotation, set
	 * the status as complete. 
	 * Call the CRM_AUC_QUOTE_CANCEL, based on GUID and
	 * rejection code to cancel the release the quotation
	 * @param quotation             the auction quotation data
	 * @exception BackendException  Exception from backend
	 */
	public void cancelInBackend(QuotationData quotation)
		throws BackendException {
		JCoConnection jcoConnection = getDefaultJCoConnection();
		if(quotation.getTechKey() == null) {
			cat.logT(Severity.DEBUG, loc, "AuctionQuotationCRM: no TechKey no action!");
			return;
		}
		String quotGuid = quotation.getTechKey().getIdAsString();
		if(quotGuid == null || quotGuid.length()<=0) {
			cat.logT(Severity.DEBUG, loc, "AuctionQuotationCRM: no TechKey no action!");
			return;
		}
		if(quotGuid.length()<32)
			quotGuid = WrapperCRMAuction.getGuidFromSalesDocNumber(quotGuid, jcoConnection);
			
		if (quotGuid != null && quotGuid.length()>0) {
			try {
				WrapperCrmIsa.ReturnValue retVal =
					quotationCancel(
						quotGuid,
						jcoConnection);

				if (retVal.getReturnCode().length() == 0) {
					// possible success message will add to the business object
					MessageCRM.addMessagesToBusinessObject(
						quotation,
						retVal.getMessages());
				} else {
					MessageCRM.logMessagesToBusinessObject(
						quotation,
						retVal.getMessages());
				}

				BackendHelper.processBackendReturn(retVal);		
			} catch (JCO.Exception ex) {
				JCoHelper.splitException(ex);
			} // TRY

		} else {
			cat.logT(Severity.DEBUG, loc, "AuctionQuotationCRM: no TechKey no action!");
		} 	
	}

	/**
	 * to really concel the quotation in the backend
	 * @param key
	 * @param jcoConnection
	 */
	private WrapperCrmIsa.ReturnValue quotationCancel(String guid, JCoConnection cn) throws BackendException {
		cat.logT(Severity.DEBUG, loc, "During the beginning of the quotation cancel method");
		String importMapping[][] = new String[2][2];

		importMapping[0] = new String[] { guid , "DOC_GUID" };

		return crmGenericWrapper("CRM_AUC_QUOTE_CANCEL",
				importMapping, null, cn, "MESSAGELINE");	
	}
	
	/* Generic wrapper for skalar Jco functions */
	private static WrapperCrmIsa.ReturnValue crmGenericWrapper(String functionName,
				String[][] importMapping,
				String[][] exportMapping,
				JCoConnection cn,
				String messageTable) throws BackendException {

		try {

			JCO.Function function =
				cn.getJCoFunction(functionName);

			// getting import parameter
			JCO.ParameterList importParams =
					function.getImportParameterList();

			// getting export parameters
			JCO.ParameterList exportParams =
					function.getExportParameterList();

			// write import params
			if (importMapping != null) {
				for (int i = 0; i < importMapping.length; i++) {
					if (importMapping[i][0] != null) {
						JCoHelper.setValue(importParams,
								importMapping[i][0],
								importMapping[i][1]);
					}
				}
			}

			// call the function
			cn.execute(function);

			// read export params
			if (exportMapping != null) {
				for (int i = 0; i < exportMapping.length; i++) {
					exportMapping[i][0] =
							JCoHelper.getString(exportParams, exportMapping[i][1]);
				}
			}

			String returnCode = "";

			if (exportParams != null) {
				// retrieve return code
				try {
					returnCode  = exportParams.getString("RETURNCODE");
				}
				catch (JCO.Exception ex) {
					returnCode = "";
				}
			}

			// Do some useful logging for the generic wrapper

			// import parameter
			if (importMapping != null) {
				StringBuffer in = new StringBuffer();
				in.append("::").append(functionName).append("::")
				  .append(" - IN: ");

				for (int i = 0; i < importMapping.length; i++) {
					in.append(importMapping[i][1])
					  .append("='")
					  .append(importMapping[i][0])
					  .append("' ");
				}
				cat.logT(Severity.DEBUG, loc, in.toString());
			}

			// export parameter
			if (exportMapping != null) {
				StringBuffer out = new StringBuffer();
				out.append("::").append(functionName).append("::")
				   .append(" - OUT: ");

				for (int i = 0; i < exportMapping.length; i++) {
					out.append(exportMapping[i][1])
					   .append("='")
					   .append(exportMapping[i][0])
					   .append("' ");
				}
				cat.logT(Severity.DEBUG, loc, out.toString());
			}

			// return code
			cat.logT(Severity.DEBUG, loc, functionName + " - ReturnCode: '" + returnCode + "'");

			// retrieve messages and catch exception if
			// table is not present
			JCO.Table messages;

			try {
				messages =
						function.getTableParameterList().getTable(messageTable);
			}
			catch (JCO.Exception e) {
				messages = null;
			}
			catch (NullPointerException e) {
				messages = null;
			}

			return new WrapperCrmIsa.ReturnValue (messages, returnCode);
		}
		catch (JCO.Exception ex) {
			cat.log(Severity.ERROR, loc, functionName, ex);
			JCoHelper.splitException(ex);
			return null;    // never reached
		}
	}
	
	private class ModifyingThread extends Thread {
		JCoConnection jcoConnection;
		String quotationId;
		String listingId;
		
		ModifyingThread(JCoConnection cn, String qtId, String listId)
		{
			jcoConnection = cn;
			quotationId = qtId;
			listingId = listId;
		}
		
		public void run() 
		{
			try {
				synchronized(jcoConnection) {
					boolean success = false;
					int tries=0;
					final int TRY_NUMBER = 10;
					final long INITIAL_SLEEP_TIME = 5000;
					long sleepTime = INITIAL_SLEEP_TIME;
					WrapperCrmIsa.ReturnValue retVal;
					do {
						sleep(sleepTime);
						sleepTime = sleepTime * 2;
						retVal = quotationSetListingId(quotationId, listingId, jcoConnection);
						
						if (retVal.getReturnCode().length() == 0)
							success = true;
						tries++;
					} while (!success && tries<TRY_NUMBER);
				
//				if (success) {
//					// possible success message will add to the business object
//					MessageCRM.addMessagesToBusinessObject(
//						quotation,
//						retVal.getMessages());
//				} else {
//					MessageCRM.logMessagesToBusinessObject(
//						quotation,
//						retVal.getMessages());
//				}
				}
			} catch (Exception ex) {
				cat.log(Severity.ERROR, loc, "In setListingIdInBackend JCO exception", ex);
			} // TRy
			finally {
				if (jcoConnection != null)
					jcoConnection.close();
			}
			
		}
	}
	/* Release the unused quantity in a quotation
	 * - Split the line items if the winner is determined for partial quantity
	 * - Assign rejection code for the line item corresponding to the left over quantity
	 * - Set the sales document header status if needed
	 * @see com.sap.isa.auction.backend.boi.order.AuctionQuotationBackend#releaseUnusedQuantity(com.sap.isa.auction.businessobject.order.AuctionQuotation)
	 */
	public void releaseRemainigQuantityInQuotation(AuctionQuotationData quotation) throws BackendException {
		cat.logT(Severity.DEBUG, loc, "Start releaseRemainigQuantityInQuotation");
		JCoConnection connection = null;
		JCO.Table messages = null;
		String quotationId = null;
		try {
			connection = getDefaultJCoConnection();
			JCO.Function function = connection.getJCoFunction("CRM_AUC_ORDER_MAINTAIN");

			// Get import parameters
			JCO.ParameterList importParams = function.getImportParameterList();
			
			importParams.setValue(quotation.getHeaderData().getTechKey().getIdAsString(), "SALESDOC_GUID"   );
			importParams.setValue("02", "REJECTION_REASON"   ); 
			/**
			 * @TODO - pb read the reject reason from the configuration
			 */
			

			JCO.ParameterList tableParams = function.getTableParameterList();
			//fill the import table: IT_PRODUCT and  IT_QUANTITY
			setReleasedItemInfo(importParams, tableParams, quotation);

			//execute the quotation
			connection.execute(function);

			// Get the exporting table
			JCO.Table returnTable = tableParams.getTable("MESSAGELINE");

			//process the tables and give the necessary message to user if errors occour
			BackendHelper.processBackendReturn(returnTable);
			


			// set the document number and GUID from the saved table
			//setGuidFromSavedTable(savedTable, quotation);


		}
		catch (JCO.Exception ex) {
			cat.log(Severity.DEBUG, loc, "Error in calling CRM function: ", ex);
			JCoHelper.splitException (ex);
		}
		finally {
			//always close the JCo connection and release the client
			connection.close();
		}
		
		
	}

	/**
	 * Set the JCO parameters for the release of unused quantities
	 * @param importParams 
	 * @param tableParams
	 * @param quotation
	 */
	private void setReleasedItemInfo(ParameterList importParams, ParameterList tableParams, AuctionQuotationData quotation) {
		JCO.Structure basketChangeItemStructureX =
				importParams.getStructure("LINE_ITEM_X");
			
		basketChangeItemStructureX.setValue("X", "QUANTITY");
		basketChangeItemStructureX.setValue("X", "PRODUCT");
		basketChangeItemStructureX.setValue("X", "PRODUCT_GUID");
		basketChangeItemStructureX.setValue("X", "CURRENCY");
		JCO.Table basketItem = tableParams.getTable("LINE_ITEMS");
		ItemListBaseData itemList = quotation.getItemListBaseData();
		Iterator iterator = itemList.iterator();
		while (iterator.hasNext()){
			ItemData item = (ItemData) iterator.next();
			basketItem.appendRow();		
			basketItem.setValue( item.getTechKey().getIdAsString() , "GUID");
			basketItem.setValue( item.getHandle()  , "HANDLE");
			basketItem.setValue( item.getParentHandle(), "PARENT_GUID");
			basketItem.setValue( item.getQuantity() , "QUANTITY");
			basketItem.setValue( item.getProduct()  , "PRODUCT");
			basketItem.setValue( item.getProductId().getIdAsString(), "PRODUCT_GUID");
			basketItem.setValue( item.getCurrency() , "CURRENCY");				
		}
		
	}

	/* 
	 * Reads the quotation details from the CRM
	 * @see com.sap.isa.auction.backend.boi.order.AuctionQuotationBackend#readFromBackend(com.sap.isa.auction.backend.boi.order.AuctionQuotationData)
	 */
	public void readFromBackend(AuctionQuotationData quotation) throws BackendException {
		cat.logT(Severity.DEBUG, loc, "Start readFromBackend");
		JCoConnection connection = null;
		JCO.Table messages = null;
		String quotationId = null;
		try {
			connection = getDefaultJCoConnection();
			JCO.Function function = connection.getJCoFunction("CRM_AUC_ORDER_GETITEMS");

			// Get import parameters
			JCO.ParameterList importParams = function.getImportParameterList();
			//set the sales document ID
			importParams.setValue(quotation.getHeaderData().getSalesDocNumber(), "SALESDOC_ID"   );

			//execute the read quotation
			connection.execute(function);

			JCO.ParameterList tableParams = function.getTableParameterList();
			JCO.ParameterList exportParams = function.getExportParameterList();
			quotation.setTechKey(new TechKey(exportParams.getString("SALESDOC_GUID")));
			
			JCO.Table lineItems =  tableParams.getTable("LINE_ITEMS");
			readLineItems (lineItems , quotation);
			JCO.Table returnTable = tableParams.getTable("MESSAGELINE");

			//process the tables and give the necessary message to user if errors occour
			BackendHelper.processBackendReturn(returnTable);
			
		}
		catch (JCO.Exception ex) {
			cat.log(Severity.DEBUG, loc, "Error in calling CRM function: ", ex);
			JCoHelper.splitException (ex);
		}
		finally {
			//always close the JCo connection and release the client
			connection.close();
		}		
		
	}

	/**
	 * REads the line items from JCO table into the quotation BO
	 * @param lineItems Line items
	 * @param quotation quotation data
	 */
	private void readLineItems(Table lineItems, AuctionQuotationData quotation) {
		ItemListBaseData itemList = new ItemList();
		lineItems.firstRow();
		
		do{
			ItemBaseData itemData = new ItemSalesDoc();
			itemData.setTechKey( new TechKey(lineItems.getString("GUID")));
			itemData.setHandle(lineItems.getString("HANDLE"));
			itemData.setParentId( new TechKey(lineItems.getString("PARENT_GUID")));
			itemData.setProduct(lineItems.getString("PRODUCT"));
			itemData.setProductId( new TechKey(lineItems.getString("PRODUCT_GUID")));
			itemData.setUnit(lineItems.getString("UNIT"));
			itemData.setCurrency(lineItems.getString("CURRENCY"));
			//itemData.setQuantity(lineItems.getString("QUANTITY"));
			itemData.setQuantity(lineItems.getString("ORDER_QTY"));
			
			itemList.add(itemData);
			
		}while (lineItems.nextRow());
		quotation.setItemListBaseData(itemList);
	}
	

}
