package com.sap.isa.auction.backend.crm.salesdocument;

import java.util.Date;
import java.util.Iterator;
import java.util.Locale;
import java.util.Properties;

import com.sap.isa.auction.backend.BackendHelper;
import com.sap.isa.auction.backend.boi.order.AuctionOrderBackend;
import com.sap.isa.auction.backend.boi.order.AuctionOrderData;
import com.sap.isa.auction.backend.crm.WrapperCRMAuction;
import com.sap.isa.auction.backend.exception.BackendUserException;
import com.sap.isa.auction.logging.CategoryProvider;
import com.sap.isa.backend.JCoHelper;
import com.sap.isa.backend.boi.isacore.AddressData;
import com.sap.isa.backend.boi.isacore.SalesDocumentData;
import com.sap.isa.backend.boi.isacore.ShipToData;
import com.sap.isa.backend.boi.isacore.ShopData;
import com.sap.isa.backend.boi.isacore.order.HeaderData;
import com.sap.isa.backend.boi.isacore.order.ItemData;
import com.sap.isa.backend.boi.isacore.order.ItemListData;
import com.sap.isa.backend.crm.MessageCRM;
import com.sap.isa.backend.crm.WrapperCrmIsa;
import com.sap.isa.backend.crm.WrapperCrmIsa.ReturnValue;
import com.sap.isa.backend.crm.order.OrderCRM;
import com.sap.isa.backend.crm.order.PartnerFunctionMappingCRM;
import com.sap.isa.backend.r3base.util.Conversion;
import com.sap.isa.businessobject.CommunicationException;
import com.sap.isa.businesspartner.backend.boi.PartnerFunctionData;
import com.sap.isa.core.TechKey;
import com.sap.isa.core.eai.BackendBusinessObjectParams;
import com.sap.isa.core.eai.BackendException;
import com.sap.isa.core.eai.BackendRuntimeException;
import com.sap.isa.core.eai.sp.jco.JCoConnection;
import com.sap.mw.jco.JCO;
import com.sap.tc.logging.Category;
import com.sap.tc.logging.Location;
import com.sap.tc.logging.Severity;
/**
 * Special kind of order for auction purpose
 */
public class AuctionOrderCRM extends OrderCRM implements AuctionOrderBackend {
	private final static Category cat = CategoryProvider.getSystemCategory();
	private final static Location loc =
		Location.getLocation(AuctionOrderCRM.class);

	private Properties initProperties;
	private BackendBusinessObjectParams initParameters;
	private String textId = TEXT_ID;
	/**
	 * Different create modes for a order.
	 */
	private final static String MODE_CREATE = "A";
	private final static String MODE_UPDATE = "B";
	private final static String PROCESS = "process";
	private final static String TYPE = "type";
	private final static String UNIT = "unit";
	private final static String SYSTEM_STATUS = "SystemStatus";
	// the default auction business transaction type
	public static final String TRANSACTION_TYPE = "AUTA";
	/**
	 * Initializes Business Object.
	 *
	 * @param props a set of properties which may be useful to initialize the object
	 * @param params a object which wraps parameters
	 */
	public void initBackendObject(
		Properties props,
		BackendBusinessObjectParams params)
		throws BackendException {
		initProperties = props;
		initParameters = params;
	}

	public void cancelInBackend(SalesDocumentData order)
		throws BackendException {
		if (order.getTechKey() == null) {
			cat.logT(
				Severity.DEBUG,
				loc,
				"AuctionOrderCRM: no TechKey no action!");
			return;
		}
		String orderGuid = order.getTechKey().getIdAsString();
		if (orderGuid == null || orderGuid.length() <= 0) {
			cat.logT(
				Severity.DEBUG,
				loc,
				"AuctionOrderCRM: no TechKey no action!");
			return;
		}
		JCoConnection jcoConnection = getDefaultJCoConnection();
		try {
			if (orderGuid.length() < 32)
				orderGuid =
					WrapperCRMAuction.getGuidFromSalesDocNumber(
						orderGuid,
						jcoConnection);
			
			if (orderGuid != null && orderGuid.length() > 0) {
				try {
			
					JCO.Function function =
						jcoConnection.getJCoFunction("CRM_AUC_QUOTE_CANCEL");
					// getting import parameter
					JCO.ParameterList importParams =
						function.getImportParameterList();
					importParams.setValue(orderGuid, "DOC_GUID");
					// getting export parameters
					JCO.ParameterList exportParams =
						function.getExportParameterList();
					jcoConnection.execute(function);
					String returnCode = "";
			
					if (exportParams != null) {
						// retrieve return code
						try {
							returnCode = exportParams.getString("RETURNCODE");
						} catch (JCO.Exception ex) {
							returnCode = "";
						}
					}
					JCO.Table messages;
			
					try {
						messages =
							function.getTableParameterList().getTable(
								"MESSAGELINE");
					} catch (JCO.Exception e) {
						messages = null;
					} catch (NullPointerException e) {
						messages = null;
					}
					if (returnCode.length() == 0) {
						// possible success message will add to the business object
						MessageCRM.addMessagesToBusinessObject(order, messages);
					} else {
						MessageCRM.logMessagesToBusinessObject(order, messages);
					}
			
					BackendHelper.processBackendReturn(messages);
				} catch (JCO.Exception ex) {
					JCoHelper.splitException(ex);
				} // TRY
			
			} else {
				cat.logT(
					Severity.DEBUG,
					loc,
					"AuctionQuotationCRM: no TechKey no action!");
			}
		}  finally {
			if (jcoConnection != null)
				jcoConnection.close();
		}
	}

	/**
	 * create the auction order(for a full-lot only) and save it into the CRM
	 *
	 * @param order The Order Business Object which contains the order Details
	 * @param webshop contains the sales organization, currency, and distribution
	 * channel, information
	 * one in configutaion file is picked up.
	 */
	public void createAuctionOrder(
		AuctionOrderData order,
		String soldTo,
		ShopData webShop)
		throws BackendException, CommunicationException {
		String processType = TRANSACTION_TYPE;
		if (initProperties != null) {
			processType = initProperties.getProperty(PROCESS);
		}
		createAuctionOrder(order, soldTo, processType, webShop);
	}

	/**
	 * create the auction order(for a full-lot only) and save it into the CRM
	 *
	 * @param order The Order Business Object which contains the order Details
	 * @param webshop contains the sales organization, currency, and distribution
	 * channel, information
	 * @param processType is optional, if the default type is null, then the
	 * one in configutaion file is picked up.
	 */
	public void createAuctionOrder(
		AuctionOrderData order,
		String soldTo,
		String processType,
		ShopData webshop)
		throws BackendException, CommunicationException {
		// It is the real ID not the GUID
		String orderId = null;
		//it is the GUID, the TechKey not the ID
		String orderKey = null;
		JCoConnection connection = null;
		try {
			connection = getDefaultJCoConnection();
			JCO.Function function =
				connection.getJCoFunction("CRM_AUC_ORDER_CREATE_MULT");
			// Get import parameters
			JCO.ParameterList importParams = function.getImportParameterList();
			// Set the import parameter value
			// EAUtilities.getInstance().timestampToCRMTimestamp(
			String startDate = null, expDate = null;
			if (order.getHeaderData() != null) {
				startDate = order.getHeaderData().getPostingDate();
				expDate = order.getHeaderData().getValidTo();
			}
			// Read the process name from the eai-config.xml params
			String iv_process = processType;
			if (startDate != null) {
				importParams.setValue(startDate, "IV_VALID_FROM");
			}
			if (expDate != null) {
				importParams.setValue(expDate, "IV_VALID_TO");
			}
			if (soldTo != null) {
				importParams.setValue(soldTo.toUpperCase(Locale.ENGLISH), "IV_PARTNERID");
			}
			importParams.setValue(iv_process, "IV_PROCESS");
			if (webshop != null) {
				importParams.setValue(
					webshop.getSalesOrganisation(),
					"IV_SALES_ORG");

				String salesUnit = webshop.getResponsibleSalesOrganisation();
				if (salesUnit == null || salesUnit.equals("")) {
					importParams.setValue(
						webshop.getSalesOrganisation(),
						"IV_SALES_ORG_RESP");
				} else {
					importParams.setValue(
						webshop.getResponsibleSalesOrganisation(),
						"IV_SALES_ORG_RESP");
				}
				importParams.setValue(
					webshop.getDistributionChannel(),
					"IV_DIS_CHANNEL");

				if (webshop.getDivision().trim().length() == 0) {
					importParams.setValue("00", "IV_DIVISION");
				} else {
					importParams.setValue(webshop.getDivision(), "IV_DIVISION");
				}
			}
			importParams.setValue(MODE_CREATE, "IV_MODE");

			try {
				importParams.setValue(
					order.getHeaderData().getCurrency(),
					"IV_CURRENCY");
			} catch (Exception ex) {
				cat.log(
					Severity.ERROR,
					loc,
					"Errors in Gettiong currency error",
					ex);
			}
			//fill the import table: IT_PRODUCT and  IT_QUANTITY
			fillImportTables(importParams, order);

			//execute the Auction order
			connection.execute(function);

			// Get the exporting table
			JCO.Table returnTable =
				function.getExportParameterList().getTable("ET_RETURN");
			JCO.Table savedTable =
				function.getExportParameterList().getTable("ET_SAVED_OBJECTS");
			//process the tables and give the necessary message to
			//user if errors occour
			boolean hasError = BackendHelper.processBackendReturn(returnTable);
			//if(!hasError) {
			setOrderIds(savedTable, order);
			//}
		} catch (JCO.Exception ex) {
			cat.log(Severity.DEBUG, loc, "Error in calling CRM function: ", ex);
			JCoHelper.splitException(ex);
		} finally {
			//always close the JCo connection and release the client
			if (connection != null)
				connection.close();
		}
		cat.logT(Severity.DEBUG, loc, "orderId: " + orderId);
	}

	/**
	 * Wrapper for CRM_AUC_ORDER_CHANGE_PRICE, Change the order price
	 * @OrderData, the order data contains order information
	 * @JCoConnection, the JCO connection
	 **/
	public void updateSalesDocumentPrices(AuctionOrderData order)
		throws BackendException {
		boolean hasError = false;
		JCoConnection connection = null;
		try {
			connection = getDefaultJCoConnection();
			JCO.Function function =
				connection.getJCoFunction("CRM_AUC_ORDER_CHANGE_PRICE");
			// Get import parameters
			JCO.ParameterList importParams = function.getImportParameterList();
			String docGuid = null;
			String currency = null;
			try {
				docGuid = order.getHeaderData().getTechKey().getIdAsString();
				currency = order.getHeaderData().getCurrency();
			} catch (Exception ex) {
				cat.logT(
					Severity.ERROR,
					loc,
					"Errors in Gettiong currency error");
			}
			if (docGuid != null) {
				JCoHelper.setValue(importParams, docGuid, "IV_QUOTE_GUID");
			}
			if (currency != null) {
				JCoHelper.setValue(importParams, currency, "IV_CURRENCY");
			}
			//fill the import table: IT_PRODUCT and  IT_QUANTITY
			fillImportTables(importParams, order);

			//execute the Auction order
			connection.execute(function);

			// Get the exporting table
			JCO.Table returnTable =
				function.getExportParameterList().getTable("ET_RETURN");
			//user if errors occour
			hasError = BackendHelper.processBackendReturn(returnTable);

			//}
		} catch (JCO.Exception ex) {
			cat.log(Severity.ERROR, loc, "CRM_AUC_ORDER_CHANGE_PRICE", ex);
			JCoHelper.splitException(ex);
		} finally {
			//always close the JCo connection and release the client
			if (connection != null)
				connection.close();
		}
	}
	/**
	 * Read the ship tos associated with the user into the document.
	 *
	 * @param posd The document to read the data in
	 * 
	 *
	 *
	 */
	public void readShipTosFromBackend(SalesDocumentData posd)
		throws BackendException {
		
		cat.logT(
			Severity.INFO,
			loc,
			"readShipTosFromBackend(SalesDocumentData)");
		if (posd.getHeaderData().getShop() != null) {
			super.readShipTosFromBackend(posd);
			return;
		}

		// read the shiptos but only if the shipto list is empty
		if (posd.getShipTos().length == 0) {
			JCoConnection connection = getDefaultJCoConnection();

			try {
				JCO.Function orderHeaderFM =
					connection.getJCoFunction("CRM_ISA_BASKET_STATUS_ENH");

				cat.logT(
					Severity.DEBUG,
					loc,
					"start setting import parameters");
				String orderGuid = posd.getTechKey().getIdAsString();
				JCO.ParameterList importParams =
					orderHeaderFM.getImportParameterList();
				if (orderGuid == null) {
					cat.logT(
						Severity.ERROR,
						loc,
						"The Order GUID import parameter is null");
					connection.close();
					return;
				}
				cat.logT(
					Severity.DEBUG,
					loc,
					"set import parameter ORDER_GUID: " + orderGuid);
				importParams.setValue(orderGuid, "ORDER_GUID");

				importParams.setValue("EN", "LANGUAGE");

				cat.logT(
					Severity.DEBUG,
					loc,
					"Execute FM CRM_ISA_BASKET_STATUS_ENH");
				connection.execute(orderHeaderFM);
				// get the output tables
				JCO.Table msgTable =
					orderHeaderFM.getTableParameterList().getTable(
						"MESSAGELINE");
				JCO.Structure oPartnerShiptoStruct =
					orderHeaderFM.getExportParameterList().getStructure(
						"HEAD_PARTNER_SHIPTO");
				// use this one for the default shipTo, see below
				// Header Shipto Data
				if (((oPartnerShiptoStruct.getField("NAME1") != null)
					&& (oPartnerShiptoStruct.getField("NAME1").getString()
						!= null)
					&& (oPartnerShiptoStruct
						.getField("NAME1")
						.getString()
						.length()
						> 0))
					|| ((oPartnerShiptoStruct.getField("LASTNAME") != null)
						&& (oPartnerShiptoStruct.getField("LASTNAME").getString()
							!= null)
						&& (oPartnerShiptoStruct
							.getField("LASTNAME")
							.getString()
							.length()
							> 0))) {
					try {
						ShipToData shipTo = posd.createShipTo();
						AddressData adr = shipTo.createAddress();
						shipTo.setAddress(adr);
						shipTo.setShortAddress(
							oPartnerShiptoStruct.getString("ADDRESS_SHORT"));
						shipTo.setTechKey(
							new TechKey(
								oPartnerShiptoStruct.getString("LINE_KEY")));
						AddressData address = shipTo.getAddressData();
						address.setName1(
							oPartnerShiptoStruct.getString("NAME1"));
						address.setName2(
							oPartnerShiptoStruct.getString("NAME2"));
						address.setLastName(
							oPartnerShiptoStruct.getString("LASTNAME"));
						address.setFirstName(
							oPartnerShiptoStruct.getString("FIRSTNAME"));
						address.setStreet(
							oPartnerShiptoStruct.getString("STREET"));
						address.setHouseNo(
							oPartnerShiptoStruct.getString("HOUSE_NO"));
						address.setCity(oPartnerShiptoStruct.getString("CITY"));
						address.setPostlCod1(
							oPartnerShiptoStruct.getString("POSTL_COD1"));
						address.setCountry(
							oPartnerShiptoStruct.getString("COUNTRY"));
						address.setRegion(
							oPartnerShiptoStruct.getString("REGION"));
						posd.addShipTo(shipTo);
					} catch (Exception e) {
						cat.log(
							Severity.ERROR,
							loc,
							"Error when set shipTo address",
							e);
					}
				}

			} catch (JCO.Exception ex) {
				cat.log(
					Severity.DEBUG,
					loc,
					"Error in calling CRM function: ",
					ex);
				JCoHelper.splitException(ex);
			} finally {
				if (connection != null)
					connection.close();
			}
		}
	}
	/**
	 * Fill the import table necessary to the JCO function call when
	 * Creating/updating the sales order
	 * The import table consists of the product details such as product_id
	 * quantity, price.
	 *
	 * @param importParams JCO function call import parameter list
	 * @param order The order data that contains the product details
	 */
	private void fillImportTables(
		JCO.ParameterList importParams,
		AuctionOrderData order) {
		String unit = null;
		String currency = "USD"; // as the default one
		try {
			if (order != null) {
				currency = order.getHeaderData().getCurrency();
			}
			if (currency == null) {
				currency = "USD";
			}
		} catch (Exception ex) {
			currency = "USD";
		}
		if (initProperties != null) {
			unit = initProperties.getProperty(UNIT);
			cat.logT(
				Severity.DEBUG,
				loc,
				"In the AuctionOrderCRM the unit is   "
					+ unit);
		} else {
			cat.logT(Severity.ERROR, loc, "the properties is null");
		}
		ItemListData itemList = order.getItemListData();
		if (itemList != null) {
			cat.logT(
				Severity.DEBUG,
				loc,
				"In AuctionOrderCRM the itemList size is " + itemList.size());
		} else {
			cat.logT(
				Severity.DEBUG,
				loc,
				"In AuctionOrderCRM the itemList size is null");
		}

		JCO.Table itemTbl = importParams.getTable("IT_ITEMS");
		boolean isFirstTime = true;
		String type = null;
		String value = null;
		String itemUnit = null;
		String productId = null;
		String quantity = null; 
		if (itemList != null) {
			Iterator iter = itemList.iterator();
			while (iter.hasNext()) {
				ItemData item = (ItemData) iter.next();
				if ((item.getUnit() != null)
					&& (item.getUnit().trim().length() > 0)) {
						itemUnit = item.getUnit();
				} else
					itemUnit = unit;
				productId = item.getProduct();
				quantity = item.getQuantity();
				if(isFirstTime) {
					value = order.getHeaderData().getNetValue();
					type = order.getTotalManualPriceCondition();
					cat.logT(
						Severity.DEBUG,
						loc,
						"In the AuctionOrderCRM the manual price type from "
							+ "Order Object is "
							+ type);
					if ((type == null) || (type.trim().length() <= 1)) {
						type = initProperties.getProperty(TYPE);
						cat.logT(
							Severity.DEBUG,
							loc,
							"In the AuctionOrderCRM the manual price type from "
								+ "retrieved from property file "
								+ type);
					}
					fillOneItem(itemTbl, productId, quantity, itemUnit,
								value, currency, type, true);
					value = order.getHeaderData().getFreightValue();
					if(value != null && value.length()>0) {
						type = order.getFreightManualPriceCondition();
						if(type != null && type.length()>0)
							fillOneItem(itemTbl, productId, quantity, itemUnit,
										value, currency, type, true);
					}
					value = order.getHeaderData().getTaxValue();
					if(value != null && value.length()>0) {
						type = order.getTaxManualPriceCondition();
						if(type != null && type.length()>0)
							fillOneItem(itemTbl, productId, quantity, itemUnit,
										value, currency, type, true);
					}
					value = order.getInsuranceValue();
					if(value != null && value.length()>0) {
						type = order.getInsurManualPriceCondition();
						if(type != null && type.length()>0)
							fillOneItem(itemTbl, productId, quantity, itemUnit,
										value, currency, type, true);
					}
					value = order.getOtherChargeDiscount();
					if(value != null && value.length()>0) {
						type = order.getOtherPriceCondition();
						if(type != null && type.length()>0)
							fillOneItem(itemTbl, productId, quantity, itemUnit,
										value, currency, type, true);
					}
					isFirstTime = false;
				}
				else {
					fillOneItem(itemTbl, productId, quantity, itemUnit,
								null, currency, type, false
								);
			}
		}
	}
	}

	private void fillOneItem(JCO.Table itemTbl, 
							 String productId, 
							 String quantity,
							 String unit,
							 String value,
							 String currency,
							 String conditionType,
							 boolean isFirstTime){
		itemTbl.appendRow();
		itemTbl.setValue(productId, "PRODUCT");
		//// get the Net value
		cat.logT(
			Severity.DEBUG,
			loc,
			"In AuctionOrderCRM the product Id is " + productId);
		cat.logT(
			Severity.DEBUG,
			loc,
			"In AuctionOrderCRM the product quantity is "
				+ quantity);
		itemTbl.setValue(quantity, "QUANTITY");

		try {
			if (unit != null
				&& unit.trim().length() > 0) {
				itemTbl.setValue(unit, "COND_UNIT");
			}
		} catch (Exception ex) {
			cat.log(
				Severity.ERROR,
				loc,
				"Errors Item Unit is problematic ",
				ex);
		}

		if (isFirstTime) {
			/// set the price to the first Item. 
			try {
				itemTbl.setValue(
					value,
					"COND_RATE");
				cat.logT(
					Severity.INFO,
					loc,
					"The pricing in AuctionOrderCRM is "
						+ value);
			} catch (Exception ex) {
				itemTbl.setValue("0", "COND_RATE");
				cat.log(
					Severity.ERROR,
					loc,
					"Errors Order Header Data Netvalue is problematic "
						+ " and set price value to be zero",
					ex);
			}
			itemTbl.setValue(currency, "CURRENCY");
			itemTbl.setValue(conditionType, "COND_TYPE");
		}
		else {
			/// reset the price to other items-pretty dirty, but the concept and design is .... 
			try {
				itemTbl.setValue(
					"0",
					"COND_RATE");
				cat.logT(
					Severity.INFO,
					loc,
					"The pricing in AuctionOrderCRM is "
						+ value);
			} catch (Exception ex) {
				cat.log(
					Severity.ERROR,
					loc,
					"Errors Order Header Data Netvalue is problematic "
						+ " and set price value to be zero",
					ex);
	}
			itemTbl.setValue(currency, "CURRENCY");
			itemTbl.setValue(conditionType, "COND_TYPE");
	
		}
	}
	
	/**
	 * set the order ids
	 *
	 * @param table JCO table which contains the guid
	 * @return nothing.
	 */
	private void setOrderIds(JCO.Table table, AuctionOrderData order) {
		String value = null;
		String id = null;
		if (table.getNumRows() > 0) {
			do {
				value = table.getString("GUID");
				id = table.getString("OBJECT_ID");
			} while (value.equals(null) && table.nextRow());
			if (id != null) {
				order.getHeaderData().setSalesDocNumber(id);
			}
			if (value != null) {
				order.setTechKey(new TechKey(value));
				if (order.getHeaderData() != null) {
					order.getHeaderData().setSalesDocNumber(id);
				}
				cat.logT(Severity.DEBUG, loc, "the order Id: " + value);
			} else {
				cat.logT(
					Severity.DEBUG,
					loc,
					"Table " + table.getName() + " doesn't contain the GUID!");
			}
		} else {
			cat.logT(
				Severity.DEBUG,
				loc,
				"Table " + table.getName() + " is empty");
		}
	}

	/* This method create a sales order from a quotation
	 * call the RFC CRM_AUC_ORDER_CREATE_WITH_REF
	 * @param quotationId, here the quotationId is the TechKey, not the
	 * 				 document ID
	 * @param order, contains all the information for order
	 * @param newShipToAddress  the new ship to address.
	 * @see com.sap.isa.auction.backend.boi.order.AuctionOrderBackend#createFromQuotation(java.lang.String, com.sap.isa.auction.backend.boi.order.AuctionOrderData, com.sap.isa.backend.boi.isacore.AddressData)
	 */
	public void createFromQuotation(
		String quotationId,
		AuctionOrderData order,
		AddressData newShipToAddress)
		throws BackendException {
		//create a sales order from quotation, also you can add
		// teh address information
		cat.logT(Severity.DEBUG, loc, "createFromQuotation start");
		//get connection to backend
		JCoConnection aJCoCon = getDefaultJCoConnection();
		WrapperCrmIsa.ReturnValue retVal;
		try {
			aJCoCon.reset();
			String soldToId = null;
			String contactId = null;
			try {
				soldToId =
					order
						.getHeaderData()
						.getPartnerListData()
						.getSoldToData()
						.getPartnerId();
                contactId =
                    order
                        .getHeaderData()
                        .getPartnerListData()
                        .getContactData()
                        .getPartnerId();
			
				//soldtoGUID = WrapperCRMAuction.getBPGUID(aJCoCon, soldToId);
			} catch (Exception ex) {
				cat.logT(
					Severity.WARNING,
					loc,
					"The sold To object does not exist in order object");
				BackendUserException backEx = new BackendUserException();
				backEx.addErrorMessage(
					"The sold To object does not exist in order object");
				throw backEx;
			}
			String quotationGuid = null;
			if (quotationId.length() > 10) {
				quotationGuid = quotationId;
			} else
				quotationGuid =
					WrapperCRMAuction.getGuidFromSalesDocNumber(
						quotationId,
						aJCoCon);
			retVal =
//				createOrderFromQuotation(quotationGuid, soldToId, order, aJCoCon);
			    createOrderFromQuotation(quotationGuid, soldToId, contactId, order, aJCoCon);			
			dispatchMessages(order, retVal);
			
		} finally {
			if (aJCoCon != null)
				aJCoCon.close();
		}
		cat.logT(Severity.DEBUG, loc, "createFromQuotation End");
		if (order.getHeaderData() != null
			&& order.getHeaderData().getSalesDocNumber() != null
			&& order.getHeaderData().getSalesDocNumber().trim().length() > 0) {
			cat.logT(Severity.INFO, loc, "order is created ok.");
			BackendHelper.processBackendReturn(retVal, false);
		} else {
			cat.logT(
				Severity.INFO,
				loc,
				"order creation failed. Processing the error message");
			BackendHelper.processBackendReturn(retVal, true);
		}
	}

	/**
	  * Read Data from the backend for cases in which the user is not known now.
	  * This may normally only occur in the B2C scenario where the user can
	  * login after playing around with the basket. Every document consists of
	  * two parts: the header and the items. This method retrieves the
	  * header and the item information.
	  *
	  * @param posd The document to read the data in
	  */
	public void readFromBackend(SalesDocumentData posd)
		throws BackendException {

		//readAllItemsFromBackend(posd, false);
		readHeaderFromBackend(posd);
	}

	/**
	 * Read OrderHeader from the backend.
	 *
	 * @param ordr The order to read the data in
	 * @param change Set this to <code>true</code> if you want to read the
	 *               header data and lock the order to change it. Set it
	 *               to <code>false</code> if you want to read the data
	 *               but do not want a change lock to be set.
	 */
	public void readHeaderFromBackend(SalesDocumentData ordr, boolean change)
		throws BackendException {
		String salesDocNum = ordr.getHeaderData().getSalesDocNumber();
		ordr.clearHeader();
		HeaderData ordrHeader = ordr.getHeaderData();
		
		JCoConnection aJCoCon = getDefaultJCoConnection();
		
		try {
				if ((ordr.getTechKey() == null)
					|| (ordr.getTechKey().getIdAsString().length() < 31)) {
					String techKey =
						WrapperCRMAuction.getGuidFromSalesDocNumber(
							salesDocNum,
							aJCoCon);
					ordr.setTechKey(new TechKey(techKey));
					ordrHeader.setTechKey(new TechKey(techKey));
				}
				PartnerFunctionMappingCRM partnerFunctionMapping =
					new PartnerFunctionMappingCRM();
			
				// set the mappings for the partner function
				partnerFunctionMapping.setPartnerFunctionMapping(
					PartnerFunctionData.SOLDTO,
					PartnerFunctionMappingCRM.SOLDTO);
				partnerFunctionMapping.setPartnerFunctionMapping(
					PartnerFunctionData.SHIPTO,
					PartnerFunctionMappingCRM.SOLDTO);
			
				// CRM_ISA_BASKET_GETHEAD
				WrapperCrmIsa.ReturnValue retVal =
					WrapperCrmIsa.crmIsaBasketGetHead(
						ordrHeader,
						ordr,
						ordr.getTechKey(),
						partnerFunctionMapping,
						change,
                        getContext(),
						aJCoCon);
			
				if (!ordr.getTechKey().isInitial()) {
					ordrHeader.setTechKey(ordr.getTechKey());
					WrapperCrmIsa.crmIsaOrderHeaderStatus(ordrHeader, aJCoCon);
				}
			
				ordr.setHeader(ordrHeader);
			
				if (retVal.getReturnCode().length() == 0) {
					MessageCRM.addMessagesToBusinessObject(ordr, retVal.getMessages());
				} else {
					MessageCRM.addMessagesToBusinessObject(ordr, retVal.getMessages());
			
					if (MessageCRM.hasErrorMessage(retVal.getMessages())) {
						MessageCRM.logMessagesToBusinessObject(
							ordr,
							retVal.getMessages());
						throw (
							new BackendRuntimeException("Error while reading order"));
					}
				}
				// CRM_ISA_BASKET_GETTEXT
					retVal =
						WrapperCrmIsa
							.crmIsaBasketGetText(ordr.getTechKey(), ordr, false,
				// GET_ALL_ITEMS = false
				this.textId, // textid
			aJCoCon);
			
				if (retVal.getReturnCode().length() == 0) {
					MessageCRM.addMessagesToBusinessObject(ordr, retVal.getMessages());
				} else {
					MessageCRM.addMessagesToBusinessObject(ordr, retVal.getMessages());
				}
			
				// CRM_ISA_BASKET_GETTEXT_HIST
					retVal =
						WrapperCrmIsa
							.crmIsaBasketGetTextHist(ordr.getTechKey(), ordr, false,
				// GET_ALL_ITEMS = false
				this.textId, // textid
			aJCoCon);
			
				if (retVal.getReturnCode().length() == 0) {
					MessageCRM.addMessagesToBusinessObject(ordr, retVal.getMessages());
				} else {
					MessageCRM.addMessagesToBusinessObject(ordr, retVal.getMessages());
				}
			
				// CRM_ISA_BASKET_GETPAYMENT
				retVal =
					WrapperCrmIsa.crmIsaBasketGetPayment(
						ordr.getTechKey(),
						ordr.getHeaderData(),
						aJCoCon);
				// Handle the messages
				if (retVal.getReturnCode().length() == 0) {
					MessageCRM.addMessagesToBusinessObject(ordr, retVal.getMessages());
				} else {
					MessageCRM.addMessagesToBusinessObject(ordr, retVal.getMessages());
				}
		}  finally {
			if (aJCoCon != null)
				aJCoCon.close();
		}
		// Since there is no function module to read the payment data, we take
		// it from a member variable.
		//ordrHeader.setPaymentData(payment);
		//payment = ordrHeader.getPaymentData();
	}

	/**
	 * @param order
	 * @param aJCoCon
	 * @return
	 */
	private ReturnValue createOrderFromQuotation(
		String quotationId,
		String soldtoGUID,
		AuctionOrderData order,
		JCoConnection cn)
		throws BackendException {
		cat.logT(
			Severity.DEBUG,
			loc,
			"Beginning method createOrderFromQuotation!");
		// first of all set the list
		ReturnValue retVal = null;
		String docType = order.getHeaderData().getDocumentType();
		if ((quotationId == null) || (quotationId.length() < 1)) {
			throw new BackendException("quotation Id is null");
		}
		try {

			JCO.Function function =
				cn.getJCoFunction("CRM_AUC_ORDER_CREATE_WITH_REF");

			// getting import parameter
			JCO.ParameterList importParams = function.getImportParameterList();

			// setting the id of the predeccesor document ID
			JCoHelper.setValue(importParams, quotationId, "PREDECESSOR_GUID");
			// after creation headerData is empty and shipcond from backend has not to be overwritten
			// call the function
			JCoHelper.setValue(importParams, MODE_CREATE, "IV_VONA_KIND");
			cat.logT(
				Severity.DEBUG,
				loc,
				"The Document process typs is "
					+ order.getHeaderData().getDocumentType());
			if (docType != null)
				JCoHelper.setValue(importParams, docType, "PROCESS_TYPE");
			if (soldtoGUID != null) {
				JCoHelper.setValue(importParams, soldtoGUID, "SOLDTO_ID");
				JCoHelper.setValue(importParams, soldtoGUID, "CONTACT_ID");
			}
// **********************************
			String currency = null;
			try {
				currency = order.getHeaderData().getCurrency();
			} 
			catch (Exception ex) {
				cat.logT( Severity.ERROR, loc, "Errors in Gettiong currency error");
			}
			if (currency != null) {
				JCoHelper.setValue(importParams, currency, "IV_CURRENCY");
			}
// ************************************			
			cat.logT(Severity.DEBUG, loc, "importParams=" + importParams);

			fillImportTables(importParams, order);

			cn.execute(function);
			// get the output parameter
			JCO.Table messages =
				function.getTableParameterList().getTable("MESSAGELINE");

			retVal = new ReturnValue(messages, "");
			JCO.Table savedTable =
				function.getExportParameterList().getTable("ET_SAVED_OBJECTS");

			// set the document number and GUID from the saved table
			setGuidFromSavedTable(savedTable, order, quotationId);

		} catch (JCO.Exception ex) {
			cat.log(Severity.ERROR, loc, "CRM_AUC_ORDER_CREATE_WITH_REF", ex);
			JCoHelper.splitException(ex);
		}
		///after the transport is ready	
		//		try{
		//			WrapperCRMAuction.checkExistenceTechKey(cn, order);
		//			if ((order.getTechKey() == null) || (order.getTechKey().getIdAsString().length()==0)){
		//				cat.logT(Severity.ERROR, loc, "During the releaseDeliveryBlock, the order techkey is null");
		//				throw new BackendException("Order Tech Key is null, can not be released");
		//			}
		//			crmaucChangePrice(order, cn);
		//		}catch (Exception ex){
		//			cat.logT(Severity.ERROR, loc, "the order price is not changed", ex);
		//		}
		return retVal;
	}

    /**
     * @param order
     * @param aJCoCon
     * @return
     */
    private ReturnValue createOrderFromQuotation(
        String quotationId,
        String soldtoGUID,
        String contactID,
        AuctionOrderData order,
        JCoConnection cn)
        throws BackendException {
        cat.logT(
            Severity.DEBUG,
            loc,
            "Beginning method createOrderFromQuotation!");
        // first of all set the list
        ReturnValue retVal = null;
        String docType = order.getHeaderData().getDocumentType();
        if ((quotationId == null) || (quotationId.length() < 1)) {
            throw new BackendException("quotation Id is null");
        }
        try {

            JCO.Function function =
                cn.getJCoFunction("CRM_AUC_ORDER_CREATE_WITH_REF");

            // getting import parameter
            JCO.ParameterList importParams = function.getImportParameterList();

            // setting the id of the predeccesor document ID
            JCoHelper.setValue(importParams, quotationId, "PREDECESSOR_GUID");
            // after creation headerData is empty and shipcond from backend has not to be overwritten
            // call the function
            JCoHelper.setValue(importParams, MODE_CREATE, "IV_VONA_KIND");
            cat.logT(
                Severity.DEBUG,
                loc,
                "The Document process typs is "
                    + order.getHeaderData().getDocumentType());
            if (docType != null)
                JCoHelper.setValue(importParams, docType, "PROCESS_TYPE");
            if (soldtoGUID != null) {
                JCoHelper.setValue(importParams, soldtoGUID, "SOLDTO_ID");
            }    
            if (contactID != null) {            
                JCoHelper.setValue(importParams, contactID, "CONTACT_ID");
            }
// **********************************
            String currency = null;
            try {
                currency = order.getHeaderData().getCurrency();
            } 
            catch (Exception ex) {
                cat.logT( Severity.ERROR, loc, "Errors in Gettiong currency error");
            }
            if (currency != null) {
                JCoHelper.setValue(importParams, currency, "IV_CURRENCY");
            }
// ************************************         
            cat.logT(Severity.DEBUG, loc, "importParams=" + importParams);

            fillImportTables(importParams, order);

            cn.execute(function);
            // get the output parameter
            JCO.Table messages =
                function.getTableParameterList().getTable("MESSAGELINE");

            retVal = new ReturnValue(messages, "");
            JCO.Table savedTable =
                function.getExportParameterList().getTable("ET_SAVED_OBJECTS");

            // set the document number and GUID from the saved table
            setGuidFromSavedTable(savedTable, order, quotationId);

        } catch (JCO.Exception ex) {
            cat.log(Severity.ERROR, loc, "CRM_AUC_ORDER_CREATE_WITH_REF", ex);
            JCoHelper.splitException(ex);
        }
        ///after the transport is ready 
        //      try{
        //          WrapperCRMAuction.checkExistenceTechKey(cn, order);
        //          if ((order.getTechKey() == null) || (order.getTechKey().getIdAsString().length()==0)){
        //              cat.logT(Severity.ERROR, loc, "During the releaseDeliveryBlock, the order techkey is null");
        //              throw new BackendException("Order Tech Key is null, can not be released");
        //          }
        //          crmaucChangePrice(order, cn);
        //      }catch (Exception ex){
        //          cat.logT(Severity.ERROR, loc, "the order price is not changed", ex);
        //      }
        return retVal;
    }
	
	
	/* 
	 * Release the delvery block for a sales order
	 * Call RFC CRM_AUC_ORDER_RELEASE_BLOCK
	 * @param SalesDocumentData, which contain the sales document info
	 */
	public void releaseDeliveryBlock(SalesDocumentData order)
		throws BackendException {
		cat.logT(
			Severity.DEBUG,
			loc,
			"At the beginning of the releaseDeliveryBlock method");
		JCoConnection jcoConnection = getDefaultJCoConnection();
		try {
			WrapperCRMAuction.checkExistenceTechKey(jcoConnection, order);
			if ((order.getTechKey() == null)
				|| (order.getTechKey().getIdAsString().length() == 0)) {
				cat.logT(
					Severity.ERROR,
					loc,
					"During the releaseDeliveryBlock, the order techkey is null");
				throw new BackendException("Order Tech Key is null, can not be released");
			}

			WrapperCrmIsa.ReturnValue retVal =
				releaseDeliveryBlock(order, jcoConnection);
			dispatchMessages(order, retVal);
			BackendHelper.processBackendReturn(retVal);
		} finally {
//			always close the JCo connection and release the client
			if (jcoConnection != null)
				  jcoConnection.close();
		}
	}

	/**
	 * @param order
	 * @param jcoConnection
	 * @return
	 */
	private ReturnValue releaseDeliveryBlock(
		SalesDocumentData order,
		JCoConnection cn)
		throws BackendException {
		cat.logT(Severity.DEBUG, loc, "Beginning method releaseDeliveryBlock!");
		// first of all set the list
		ReturnValue retVal = null;
		//
		String docGuid = order.getTechKey().toString();
		try {

			JCO.Function function =
				cn.getJCoFunction("CRM_AUC_ORDER_RELEASE_BLOCK");

			// getting import parameter
			JCO.ParameterList importParams = function.getImportParameterList();

			// setting the id of the sales document
			JCoHelper.setValue(importParams, docGuid, "DOC_GUID");
			// after creation headerData is empty and shipcond from backend has not to be overwritten
			// call the function
			cn.execute(function);
			// get the output parameter
			JCO.Table messages =
				function.getTableParameterList().getTable("MESSAGELINE");

			retVal = new ReturnValue(messages, "");

		} catch (JCO.Exception ex) {
			cat.log(Severity.ERROR, loc, "CRM_AUC_ORDER_RELEASE_BLOCK", ex);
			JCoHelper.splitException(ex);
		}
		return retVal;

	}

	/* (non-Javadoc)
	 * @see com.sap.isa.auction.backend.boi.order.AuctionOrderBackend#releaseBillingBlock(com.sap.isa.backend.boi.isacore.SalesDocumentData)
	 */
	public void releaseBillingBlock(SalesDocumentData order)
		throws BackendException {
		cat.log(
			Severity.INFO,
			loc,
			"In CRM, releasing billing block operation does nothing as order is not distributed to R3 yet");
		//throw new UnsupportedOperationException();		
	}

	/**
	 * Set the GUID and the sales document numebr to the quotation object
	 * @param savedTable
	 * @param quotation
	 */
	private void setGuidFromSavedTable(
		JCO.Table savedTable,
		AuctionOrderData aOrder,
		String quotationId) {
		String value = null;
		String salesDocNumber = null;
		if (savedTable.getNumRows() > 0) {
			do {
				value = savedTable.getString("GUID");
				salesDocNumber = savedTable.getString("OBJECT_ID");
				if (!quotationId.equals(value))
					break;
			} while (savedTable.nextRow());
			if (value != null) {
				aOrder.getHeaderData().setSalesDocNumber(salesDocNumber);
				aOrder.getHeaderData().setTechKey(new TechKey(value));
				cat.logT(Severity.DEBUG, loc, "the order GUID Id: " + value);
				cat.logT(
					Severity.DEBUG,
					loc,
					"the sales doc number is : " + salesDocNumber);
				Date today = new Date(System.currentTimeMillis());
				aOrder.getHeaderData().setChangedAt(
					Conversion.dateToUIDateString(today, Locale.getDefault()));
				//aOrder.setTechKey(new TechKey(salesDocNumber));
			} else {
				cat.logT(
					Severity.DEBUG,
					loc,
					"Table "
						+ savedTable.getName()
						+ " doesn't contain the GUID!");
			}
		} else {
			cat.logT(
				Severity.DEBUG,
				loc,
				"Table " + savedTable.getName() + " is empty");
			cat.logT(
				Severity.ERROR,
				loc,
				"The quotation sales document has not been created");
		}
	}

	/**
	 * Update the header informatin to the backend
	 */
	public void updateHeaderInBackend(SalesDocumentData posd)
		throws BackendException {

		// new: get JcoConnection
		JCoConnection aJCoCon = getDefaultJCoConnection();

		WrapperCrmIsa.ReturnValue retVal;
		try {
				// CRM_ISA_BASKET_CHANGEHEAD
				retVal =
					WrapperCRMAuction.crmIsaBasketChangeHead(
						posd.getTechKey(),
						posd.getHeaderData(),
						aJCoCon);
			
				// Handle the messages
				dispatchMessages(posd, retVal);
			
				// CRM_ISA_BASKET_ADDTEXT
				// text on header level
					retVal =
						WrapperCrmIsa.crmIsaBasketAddText(posd.getTechKey(), null,
				// indicates text on header level
			this.textId, posd.getHeaderData().getText(), aJCoCon);
			
				// Handle the messages
				dispatchMessages(posd, retVal);
		} finally {
			if (aJCoCon != null)
				aJCoCon.close();
		}

		BackendHelper.processBackendReturn(retVal);
	}

	/**
	 * Adds a manual address to the header of the sales document using the appropriate
	 * backend methods. This method is used to create a new manual address shipto and
	 * add it to the existing shipTo to the sales document.
	 *
	 * @param reference to the actual sales document
	 * @param the technical key of the actually assigned shipto
	 * @param new address to add
	 * @returns integer value
	 *      <code>0</code> if everything is ok
	 *      <code>1</code> if an error occured, display corresponding messages
	 *      <code>2</code> if an error occured, county selection necessary
	 */
	public void updateManualShippingAddress(
		SalesDocumentData salesDoc,
		AddressData newAddress)
		throws BackendException {
		TechKey orderTKey = null;
		orderTKey = salesDoc.getTechKey();
		if (orderTKey == null)
			orderTKey = salesDoc.getHeaderData().getTechKey();
		HeaderData headerData = salesDoc.getHeaderData();
		WrapperCrmIsa.ReturnValue retVal = null;
		JCoConnection aJCoCon = getDefaultJCoConnection();

		try {
			retVal =
				WrapperCRMAuction.shippingAddressHeadUpdate(
					orderTKey,
					newAddress,
					headerData,
					aJCoCon);
			
		} finally {
			if (aJCoCon != null)
				aJCoCon.close();
		}

		BackendHelper.processBackendReturn(retVal);
	}
	/**
	 * Adds a new shipTo to an item of the sales document using the appropriate
	 * backend methods. This method is used to create a new shipto and
	 * add it to the list of available ship tos for the sales document.
	 *
	 * @param reference to the actual sales document
	 * @param itemKey the technical key of the item, the shipto belongs to
	 * @param the technical key of the actually assigned shipto
	 * @param new address to add
	 * @param technical key of the soldto
	 * @param id of the shop
	 *
	 *
	 * @returns integer value
	 *      <code>0</code> if everything is ok
	 *      <code>1</code> if an error occured, display corresponding messages
	 *      <code>2</code> if an error occured, county selection necessary
	 */
	public int addNewShipToInBackend(
		SalesDocumentData salesDoc,
		AddressData newAddress,
		TechKey soldToKey,
		TechKey shopKey,
		TechKey itemKey,
		TechKey oldShipToKey)
		throws BackendException {

		int functionReturnValue;
		ShipToData shipto = null;

		// at first check the new address data
		WrapperCrmIsa.ReturnValue retVal = null;
		//				WrapperCrmIsa.crmIsaShiptoAddressCheck(newAddress,
		//													   shopKey.getIdAsString(),
		//													   aJCoCon);

		//		if (retVal.getReturnCode().length() == 0 ||
		//			retVal.getReturnCode().equals("0")) {
		//			MessageCRM.addMessagesToBusinessObject(newAddress, retVal.getMessages());

		shipto = salesDoc.createShipTo();
		if (itemKey == null) {
			shipto.setId(salesDoc.getHeaderData().getShipToData().getId());
		} else {
			shipto.setId(salesDoc.getItemData(itemKey).getShipToData().getId());
		}

		// if there is no error create a document address object in the backend
		// and get the other values for the new shipto
		JCoConnection aJCoCon = getDefaultJCoConnection();
		try {
			retVal =
				WrapperCrmIsa.crmIsaAddressGenerate(
					salesDoc.getTechKey(),
					salesDoc,
					newAddress,
					shipto,
					aJCoCon);
		} finally {
			if (aJCoCon != null)
				aJCoCon.close();
		}	
		if (retVal.getReturnCode().length() == 0
			|| retVal.getReturnCode().equals("0")) {
			MessageCRM.addMessagesToBusinessObject(
				newAddress,
				retVal.getMessages());
			functionReturnValue = 0; // ok

			if (itemKey != null) {
				salesDoc.getItemData(itemKey).setShipToData(shipto);
			} else {
				salesDoc.getHeaderData().setShipToData(shipto);
			}
			// the data must be updated in the backend
			//updateInBackend(salesDoc);
			updateHeaderInBackend(salesDoc);
		} else {
			MessageCRM.addMessagesToBusinessObject(
				newAddress,
				retVal.getMessages());
			functionReturnValue = 1; // not ok, display messages
		}
		//		}
		//		else {
		//			MessageCRM.addMessagesToBusinessObject(newAddress, retVal.getMessages());
		//
		//			if (retVal.getReturnCode().equals(UserBackend.COUNTY_SELECTION_NEEDED)) {
		//				functionReturnValue = 2;  // not ok, county selection necessary
		//			}
		//			else {
		//			  functionReturnValue = 1;    // not ok, display messages
		//			}
		//		}
		//		functionReturnValue = 0;
		//		updateHeaderInBackend(salesDoc);
		BackendHelper.processBackendReturn(retVal);
		return functionReturnValue;
	}

	/* 
	 * Add a new system status to the sales document
	 * @param the sales document 
	 */
	public void updateOrderSystemStatus(SalesDocumentData order)
		throws BackendException {
		cat.logT(
			Severity.DEBUG,
			loc,
			"At the beginning of the updateOrderSystemStatus method");
		final String functionName = "CRM_AUC_ORDER_RESET_STATUSES";
		JCoConnection jcoConnection = getDefaultJCoConnection();
		WrapperCrmIsa.ReturnValue retVal;
		try {
			WrapperCRMAuction.checkExistenceTechKey(jcoConnection, order);
			if ((order.getTechKey() == null)
				|| (order.getTechKey().getIdAsString().length() == 0)) {
				cat.logT(
					Severity.ERROR,
					loc,
					"During the updateOrderSystemStatus, the order techkey is null");
				throw new BackendException("Order Tech Key is null, can not be updated");
			}
			
			retVal = updateSystemStatus(order, jcoConnection, functionName);
			dispatchMessages(order, retVal);

		} finally {
			if (jcoConnection != null)
				jcoConnection.close();
		}
		cat.logT(
			Severity.DEBUG,
			loc,
			"At the end of the updateOrderSystemStatus method");
		BackendHelper.processBackendReturn(retVal);

	}

	/**
	 * @param order
	 * @param jcoConnection
	 * @return
	 */
	private ReturnValue updateSystemStatus(
		SalesDocumentData order,
		JCoConnection cn,
		String functionName)
		throws BackendException {
		cat.logT(Severity.DEBUG, loc, "Beginning method updateSystemStatus!");
		// first of all set the list
		ReturnValue retVal = null;
		//
		String docGuid = order.getTechKey().toString();
		String systemStatus = initProperties.getProperty(SYSTEM_STATUS);
		try {

			JCO.Function function = cn.getJCoFunction(functionName);
			// getting import parameter
			JCO.ParameterList importParams = function.getImportParameterList();

			// setting the id of the sales document
			JCoHelper.setValue(importParams, docGuid, "DOC_GUID");
			cat.logT(Severity.DEBUG, loc, "System status is" + systemStatus);
			cat.logT(Severity.DEBUG, loc, "doc GUID is" + docGuid);
			if ((systemStatus != null) && (systemStatus.length() > 0)) {
				JCoHelper.setValue(importParams, systemStatus, "SYS_STAUS");
				JCoHelper.setValue(importParams, " ", "SAVE_FLAG");
			} else {
				cat.logT(
					Severity.ERROR,
					loc,
					"During the updateOrderSystemStatus, the system " +					"status uses the default I1825");
			}
			// after creation headerData is empty and shipcond from backend has not to be overwritten
			// call the function
			cn.execute(function);
			// get the output parameter
			JCO.Table messages =
				function.getTableParameterList().getTable("MESSAGELINE");
			retVal = new ReturnValue(messages, "");

		} catch (JCO.Exception ex) {
			cat.log(Severity.ERROR, loc, functionName, ex);
			JCoHelper.splitException(ex);
		}
		return retVal;
	}

	/* Release distribution block and the new order status from 
	 * the sales order
	 * @param the sales document generated from an auction 
	 */
	public void releaseOrderSystemStatus(SalesDocumentData order)
		throws BackendException {
		cat.logT(
			Severity.DEBUG,
			loc,
			"At the beginning of the releaseOrderSystemStatus method");
		JCoConnection jcoConnection = getDefaultJCoConnection();
		WrapperCrmIsa.ReturnValue retVal;
		try {
			WrapperCRMAuction.checkExistenceTechKey(jcoConnection, order);
			final String functionName = "CRM_AUC_ORDER_RESET_STATUSES";
			if ((order.getTechKey() == null)
				|| (order.getTechKey().getIdAsString().length() == 0)) {
				cat.logT(
					Severity.ERROR,
					loc,
					"During the releaseOrderSystemStatus, the order techkey is null");
				throw new BackendException("Order Tech Key is null, can not be updated");
			}
				
			retVal = updateSystemStatus(order, jcoConnection, functionName);
			dispatchMessages(order, retVal);
				
			//always close the JCo connection and release the client
		} finally {
			if (jcoConnection != null)
				jcoConnection.close();
		}
		cat.logT(
			Severity.DEBUG,
			loc,
			"At the end of the updateOrderSystemStatus method");
	
		BackendHelper.processBackendReturn(retVal);
	}

}
