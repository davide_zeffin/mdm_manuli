/*****************************************************************************
    Copyright (c) 2001, SAPMarkets Palo Alto, USA, All rights reserved.

*****************************************************************************/
package com.sap.isa.auction.backend.crm;


import java.util.*;
import com.sap.mw.jco.*;
import com.sap.tc.logging.*;
import com.sap.isa.auction.backend.boi.targetGroup.*;
import com.sap.isa.core.eai.sp.jco.*;
import com.sap.isa.core.*;
import com.sap.isa.core.eai.*;
import com.sap.isa.backend.*; //for JCoHelper

import com.sap.isa.auction.businessobject.targetGroup.TargetGroup;
import com.sap.isa.auction.logging.CategoryProvider;


/**
 * Get target group list from a CRM system
 *
 * @author Yan Cui &date 04/25/2001
 * @version 1.0
 */

public class TargetGroupListCRM extends BackendBusinessObjectBaseSAP
implements TargetGroupListBackend {

    /**
     * Construtor
     */
    public TargetGroupListCRM() {
    }
    //for Logging
	private final static Category cat = CategoryProvider.getSystemCategory();
	private final static Location loc =
		Location.getLocation(TargetGroupListCRM.class);
    //private static IsaLocation log = IsaLocation.getInstance(TargetGroupListCRM.class.getName());

    /**
     * get the properties out of the config file
     * concerning the target group list backend object
     */
    public void initBackendObject(Properties props,
                                  BackendBusinessObjectParams params)
        throws BackendException {

        //this.props = props;

    }
    /**
     * @Deprecated Please use the method with the searchdata set
     * get target group list information from SAP CRM backend system and put it
     * into the target group list business object
     *
     * @param tgLt Frontend business object
     */
    public void getCRMTargetGroupList(TargetGroupListData tgLt)
        throws BackendException {
        HashMap tgLtMap = new HashMap();
        JCoConnection connection = null;
        try {
            connection = getDefaultJCoConnection();

            // Get repository infos about the function
            JCO.Function tgGrouplst = connection.getJCoFunction("CRM_ISA_AUC_TG_GETLIST");

            // This function call doesn't provide any import parameters, so we can
            // Execute the function immediately after get the function
            connection.execute(tgGrouplst);

            // Get the exporting table from export parameter list
            JCO.ParameterList exportParams = tgGrouplst.getExportParameterList();
            JCO.Table tgGrouplstTable = exportParams.getTable("ET_TG");

            //process the table and put it into the TargetGroupList object
            setTargetGroupList(tgGrouplstTable, tgLt);

        }
        catch (JCO.Exception ex) {
            cat.log(Severity.DEBUG, loc, "Error in calling CRM function: ", ex);
            JCoHelper.splitException (ex);
        }
        finally {
            //always close the JCo connection and release the client
            connection.close();
        }
    }
    /**
     * @Deprecated Please use the method with the searchdata set
     * get target group list information from SAP CRM backend system and put it
     * into the target group list business object
     *
     * @param tgLt Frontend business object
     */
    public void getCRMTargetGroupObjectList(TargetGroupListData tgLt)
        throws BackendException {
        HashMap tgLtMap = new HashMap();
        JCoConnection connection = null;
        try {
            connection = getDefaultJCoConnection();

            // Get repository infos about the function
            JCO.Function tgGrouplst = connection.getJCoFunction("CRM_ISA_AUC_TG_GETLIST");

            // This function call doesn't provide any import parameters, so we can
            // Execute the function immediately after get the function
            connection.execute(tgGrouplst);

            // Get the exporting table from export parameter list
            JCO.ParameterList exportParams = tgGrouplst.getExportParameterList();
            JCO.Table tgGrouplstTable = exportParams.getTable("ET_TG");

            //process the table and put it into the TargetGroupList object
            setTargetGroupObjectList(tgGrouplstTable, tgLt);

        }
        catch (JCO.Exception ex) {
            cat.log(Severity.DEBUG, loc, "Error in calling CRM function: ", ex);
            JCoHelper.splitException (ex);
        }
        finally {
            //always close the JCo connection and release the client
            connection.close();
        }
    }

    /**
     * @Deprecated Please use the method with the searchdata set
     * get target group list information from SAP CRM backend system for a specific
     * user and put it into the target group list business object
     *
     * @param tgLt Frontend business object
     * @param bpGuid TeckKey uniquely identify a target group list for the user
     */
    public void getCRMTargetGroupList(TargetGroupListData tgLt, TechKey bpGuid)
        throws BackendException {

        HashMap tgLtMap = new HashMap();
        JCoConnection connection = null;
        try {
            connection = getDefaultJCoConnection();

            // Get repository infos about the function
            JCO.Function tgGrouplst = connection.getJCoFunction("CRM_ISA_AUC_TG_GETLIST");

            // Get the import parameter
            JCO.ParameterList importParams = tgGrouplst.getImportParameterList();

            // Set the import parameter vale
            importParams.setValue(bpGuid.toString(), "IV_PARTNER_GUID");
            cat.logT(Severity.DEBUG, loc, "business partner ID: " + importParams.getValue("IV_PARTNER_GUID").toString());

            // Execute the function
            connection.execute(tgGrouplst);

            // Get the exporting table
            JCO.Table tgGrouplstTable = tgGrouplst.getExportParameterList().getTable("ET_TG");

            //process the table
            setTargetGroupList(tgGrouplstTable, tgLt);


        }
        catch (JCO.Exception ex) {
            cat.log(Severity.DEBUG, loc, "Error in calling CRM function: ", ex);
            JCoHelper.splitException (ex);
        }
        finally {
            //always close the JCo connection
            connection.close();
        }
    }

    /**
     * @Deprecated Please use the method with the searchdata set
     * This method was written for release 3.1 and further.
     * This is used to set the list of Target Groups.
     * if bpGuid is specified then the TG that matches the criteria is returned
     * else the whole list of target groups in crm is returned.
     * The hashMap returns the guid as the Key and the TargetGroup object as the value
     */
    public void getCRMTargetGroupObjectList(TargetGroupListData tgLt,
                                            TechKey bpGuid)
        throws BackendException {
        HashMap tgLtMap = new HashMap();
        JCoConnection connection = null;
        try {
            connection = getDefaultJCoConnection();

            JCO.Function tgGrouplst = connection.getJCoFunction("CRM_ISA_AUC_TG_GETLIST");

            if(bpGuid!=null) {
                JCO.ParameterList importParams = tgGrouplst.getImportParameterList();
                importParams.setValue(bpGuid.toString(), "IV_PARTNER_GUID");
            }

            connection.execute(tgGrouplst);

            JCO.ParameterList exportParams = tgGrouplst.getExportParameterList();
            JCO.Table tgGrouplstTable = exportParams.getTable("ET_TG");

            setTargetGroupObjectList(tgGrouplstTable, tgLt);

        }
        catch (JCO.Exception ex) {
            cat.log(Severity.DEBUG, loc, "Error in calling CRM function: ", ex);
            JCoHelper.splitException (ex);
        }
        finally {
            //always close the JCo connection and release the client
            connection.close();
        }
    }


   /**
     * This is used to get the list of Target Groups based on the search Criteria.
     * Returns all target groups in the crm system if no search criteria is specified.
     *
     * The hashMap returns the guid as the Key and the TargetGroup object as the value
     */
    public void getCRMTargetGroupObjectList(TargetGroupListData tgLt,
                                            TargetGroupSearchData searchData)
        throws BackendException {
        HashMap tgLtMap = new HashMap();
        JCoConnection connection = null;
        try {
            connection = getDefaultJCoConnection();

            JCO.Function tgGrouplst = connection.getJCoFunction("CRM_ISA_AUC_TG_GETLIST");

            if(searchData!=null) {
                JCO.ParameterList importParams = tgGrouplst.getImportParameterList();
                if(searchData.getBpGuid() != null)
                    importParams.setValue(searchData.getBpGuid(), "IV_PARTNER_GUID");
                if(searchData.getTgDescription() != null)
                    importParams.setValue(searchData.getTgDescription(), "IV_TG_DESCR");
            }

            connection.execute(tgGrouplst);

            JCO.ParameterList exportParams = tgGrouplst.getExportParameterList();
            JCO.Table tgGrouplstTable = exportParams.getTable("ET_TG");

            setTargetGroupObjectList(tgGrouplstTable, tgLt);

        }
        catch (JCO.Exception ex) {
            cat.log(Severity.DEBUG, loc, "Error in calling CRM function:  ", ex);
            JCoHelper.splitException (ex);
        }
        finally {
            //always close the JCo connection and release the client
            connection.close();
        }
    }




    /**
     *
     */
    private void setTargetGroupList(JCO.Table tgGrouplstTable,
                                   TargetGroupListData tgLt) {

        HashMap tgLtMap = new HashMap();

		cat.logT(Severity.DEBUG, loc, "TargetGroupListCRM - entering setTargetGroupList(table, TGlistdata)");

        // Loop through the table to get the whole list
        if (tgGrouplstTable.getNumRows() > 0) {
            do {
                String tgGuid = tgGrouplstTable.getString("GUID");
                String tgDesc = tgGrouplstTable.getString("TG_DESCR");

                cat.logT(Severity.DEBUG, loc, "GUID" + tgGuid + " Desc " + tgDesc);
                tgLtMap.put(tgGuid, tgDesc);

            } while (tgGrouplstTable.nextRow());
            tgLt.setTargetGroupList(tgLtMap);
        }
        else {
			cat.logT(Severity.DEBUG, loc, "Table " + tgGrouplstTable.getName() + " is empty");
        }
    }

    /**
     * Sets the HashMap of TagetGroup objects from the CRM list of entries
     */
    private void setTargetGroupObjectList(JCO.Table tgGrouplstTable,
                                         TargetGroupListData tgLt) {

        cat.logT(Severity.DEBUG, loc, "TargetGroupListCRM - entering setTargetGroupObjectList(table, TGlistdata)");

        HashMap tgLtMap = new HashMap();

        if (tgGrouplstTable.getNumRows() > 0) {
            do {
                TargetGroup tgBean = new TargetGroup();
                String tgGuid = tgGrouplstTable.getString("GUID");
                tgBean.setGuid(tgGuid);
                String tgDesc = tgGrouplstTable.getString("TG_DESCR");
                tgBean.setDescription(tgDesc);

                cat.logT(Severity.DEBUG, loc, "GUID" + tgGuid + " Desc " + tgDesc);
                tgLtMap.put(tgGuid, tgBean);
            }
            while (tgGrouplstTable.nextRow());
            tgLt.setTargetGroupList(tgLtMap);
        }
        else {
			cat.logT(Severity.DEBUG, loc, "Table " + tgGrouplstTable.getName() + " is empty");
            tgLt.setTargetGroupList(null);
        }
    }
}
