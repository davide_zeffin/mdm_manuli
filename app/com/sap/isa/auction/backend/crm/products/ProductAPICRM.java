/*
 * Created on Jan 28, 2004
 *
 * To change the template for this generated file go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
package com.sap.isa.auction.backend.crm.products;

import java.util.Iterator;
import java.util.List;

import com.sap.isa.auction.backend.boi.product.ProductsBackend;
import com.sap.isa.backend.IsaBackendBusinessObjectBaseSAP;
import com.sap.isa.core.eai.BackendException;
import com.sap.isa.core.eai.sp.jco.JCoConnection;
import com.sap.isa.auction.backend.crm.products.strategy.*;
import com.sap.isa.auction.businessobject.product.ProductDetail;
import com.sap.isa.auction.logging.CategoryProvider;
import com.sap.isa.auction.util.StringUtil;
import com.sap.mw.jco.JCO;
import com.sap.tc.logging.*;

/**
 * @author I803746
 *
 * To change the template for this generated type comment go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
public class ProductAPICRM
	extends IsaBackendBusinessObjectBaseSAP
	implements ProductsBackend {
		
	private final static Category cat = CategoryProvider.getSystemCategory();
	private final static Location loc =
		Location.getLocation(ProductAPICRM.class);

	private CheckStrategy strategyInstance;

	private CheckStrategy getCheckStrtegy() {
		if (strategyInstance == null) {
			strategyInstance = new CheckStrategyImp();
		}

		return strategyInstance;
	}
		
	/* (non-Javadoc)
	 * @see com.sap.isa.auction.backend.boi.product.ProductsBackend#getProductsDetails(java.util.List)
	 */
	public List getProductsDetails(List products, String lang) throws BackendException {
		JCoConnection jcoCon = this.getDefaultJCoConnection();
		Iterator it = products.iterator();
		while(it.hasNext())
		{
			ProductDetail product = (ProductDetail)it.next();
			getProductDetail(product, lang, jcoCon);
		}
		jcoCon.close();
		return products;
	}

	/* (non-Javadoc)
	 * @see com.sap.isa.auction.backend.boi.product.ProductsBackend#checkExistence(java.util.List)
	 */
	public boolean checkExistence(List products) throws BackendException {
		JCoConnection jcoCon = this.getDefaultJCoConnection();
		getCheckStrtegy().checkMaterialsExist(products, jcoCon);
		jcoCon.close();
		return true;
	}

	/* (non-Javadoc)
	 * @see com.sap.isa.auction.backend.boi.product.ProductsBackend#performATPCheck(java.util.List)
	 */
	public boolean performATPCheck(List products) throws BackendException {
		JCoConnection jcoCon = this.getDefaultJCoConnection();
		getCheckStrtegy().checkMaterialsATP(products, jcoCon);
		jcoCon.close();
		return true;
	}

	/* (non-Javadoc)
	 * @see com.sap.isa.auction.backend.boi.product.ProductsBackend#performCompleteCheck(java.util.List)
	 */
	public boolean performCompleteCheck(List products)
		throws BackendException {
			JCoConnection jcoCon = this.getDefaultJCoConnection();
			getCheckStrtegy().checkMaterialsExist(products, jcoCon);
			getCheckStrtegy().checkMaterialsATP(products, jcoCon);
			//detailedErrorCheck(products, jcoCon);
			jcoCon.close();
			return true;
	}

	private void getProductDetail(ProductDetail product, String lang, JCoConnection jcoCon) throws BackendException
	{
		JCO.Function function = jcoCon.getJCoFunction("CRM_AUC_PROD_GETDETAIL");
		JCO.ParameterList importParam = function.getImportParameterList();
		JCO.Field prodId_field = importParam.getField("IV_PRODUCT_ID");
		StringUtil.setExtendIdField(prodId_field, product.getProductId()); 
		if(lang != null)
			importParam.setValue(lang.toUpperCase(), "IV_LANGU");
		
		jcoCon.execute(function);
		
		JCO.ParameterList exportParam = function.getExportParameterList();
		
		JCO.Table returns = exportParam.getTable("ET_BAPIRETURN");
		if(returns != null && !returns.isEmpty()){
			returns.firstRow();
			boolean error = false;
			do {
				if("E".equals(returns.getString("TYPE"))){
					error = true;
					break;
				}
			}while(returns.nextRow());
			if(error) {
				cat.logT(Severity.DEBUG, loc, returns.toString());
				return;
			} 
		}

		String shortText = exportParam.getString("EV_SHORT_TEXT");
		product.setDescription(shortText);
			
		JCO.Table longTexts = exportParam.getTable("ET_LONG_TEXT");
		if(longTexts != null && longTexts.getNumRows()>0) {
			longTexts.firstRow();
			JCO.Structure data = longTexts.getStructure("DATA");
			JCO.Table lines = data.getTable("LINES");
			if(lines != null && lines.getNumRows()>0) {
				StringBuffer longText = new StringBuffer();
				lines.firstRow();
				do {
					String line = lines.getString("TDLINE");
					longText.append(line);
				}while(lines.nextRow());
				product.setBasicText(longText.toString());
			}
		}
		
		JCO.Table manuInfo = exportParam.getTable("ET_MANUFAC_DATA");
		if(manuInfo != null && manuInfo.getNumRows()>0) {
			manuInfo.firstRow();
			JCO.Structure data = manuInfo.getStructure("DATA");
			String maPart = data.getString("MAPART");
			product.setManufactPartNumber(maPart);
		}
		
		JCO.Table unitInfo = exportParam.getTable("ET_UNIT_DATA");
		if(unitInfo != null && unitInfo.getNumRows()>0) {
			unitInfo.firstRow();
			JCO.Structure data = unitInfo.getStructure("DATA");
			String grossWeight = data.getString("GROSS_WEIGHT");
			product.setGrossWeight(grossWeight);
			String netWeight = data.getString("NET_WEIGHT");
			product.setNetWeight(netWeight);
			String unit = data.getString("UNIT");
			product.setBaseUOM(unit);
			String weightUnit = data.getString("WEIGHT_UNIT");
			product.setWeightUnit(weightUnit);
			String volume = data.getString("VOLUME");
			product.setVolume(volume);
			String volumeUnit = data.getString("VOLUME_UNIT");
			product.setVolumeUnit(volumeUnit);
			StringBuffer dimension = new StringBuffer();
			String length = data.getString("LENGTH");
			dimension.append("length=");
			dimension.append(length);
			String width = data.getString("WIDTH");
			dimension.append("; width=");
			dimension.append(width);
			String height = data.getString("HEIGHT");
			dimension.append("; height=");
			dimension.append(height);
			String dimunit = data.getString("UNIT_OF_DIM");
			dimension.append("; unit=");
			dimension.append(dimunit);
			product.setDimensions(dimension.toString());
		}
	}
}
