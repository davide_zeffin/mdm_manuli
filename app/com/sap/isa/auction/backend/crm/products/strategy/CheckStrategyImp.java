/*
 * Created on Jan 28, 2004
 *
 * To change the template for this generated file go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
package com.sap.isa.auction.backend.crm.products.strategy;

import java.util.*;
import com.sap.mw.jco.JCO;

import com.sap.isa.core.eai.BackendException;
import com.sap.isa.core.eai.sp.jco.JCoConnection;
import com.sap.tc.logging.*;
import com.sap.isa.auction.backend.boi.product.*;
import com.sap.isa.auction.backend.boi.SalesAreaData;
import com.sap.isa.businessobject.BusinessObjectBase;
import com.sap.isa.auction.logging.CategoryProvider;
import com.sap.isa.auction.util.StringUtil;
import com.sap.isa.auction.businessobject.product.*;

/**
 * @author I803746
 *
 * To change the template for this generated type comment go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
public class CheckStrategyImp implements CheckStrategy {
	private final static Category cat = CategoryProvider.getSystemCategory();
	private final static Location loc =
		Location.getLocation(CheckStrategyImp.class);

	private static Map unitConverLangMap = new HashMap();

	/* (non-Javadoc)
	 * @see com.sap.isa.auction.backend.crm.products.strategy.CheckStrategy#checkMaterialsATP(java.util.List, com.sap.isa.core.eai.sp.jco.JCoConnection)
	 */
	public boolean checkMaterialsATP(
		List auctionlisting,
		JCoConnection connection)
		throws BackendException {
			cat.logT(Severity.DEBUG, loc, "products atp check");
		
			Collection subLists = divideBySalesArea(auctionlisting);
			Iterator it = subLists.iterator();
			while(it.hasNext())
			{
				List list = (List)it.next();
				checkSingleSalesAreaATP(list, connection);
			}
		
			return true;
	}

	/* (non-Javadoc)
	 * @see com.sap.isa.auction.backend.crm.products.strategy.CheckStrategy#checkMaterialsExist(java.util.List, com.sap.isa.core.eai.sp.jco.JCoConnection)
	 */
	public boolean checkMaterialsExist(
		List auctionlisting,
		JCoConnection connection)
		throws BackendException {
			cat.logT(Severity.DEBUG, loc, "single sales area product existense check");
			
			if(auctionlisting == null || auctionlisting.size()<=0)
				return false;
			
			JCO.Function getListFunc = connection.getJCoFunction(CRM_AUC_PROD_GETLIST);
			JCO.ParameterList imports = getListFunc.getImportParameterList();
			JCO.ParameterList tables = getListFunc.getTableParameterList();
		
			JCO.Table selection = imports.getTable("IT_PROD_ID_RG");
			selection.clear();
			
			Iterator it = auctionlisting.iterator();
			while(it.hasNext())
			{
				StatusCheckable product = (StatusCheckable)it.next();
		
				selection.appendRow();
				selection.setValue("I", "SIGN");
				selection.setValue("EQ", "OPTION");
				JCO.Field prodId_field = selection.getField("LOW");
				StringUtil.setExtendIdField(prodId_field, product.getProductId().toUpperCase());
				cat.logT(Severity.DEBUG, loc, "The search product ID is " + 
											product.getProductId().toUpperCase());
			}
			
			imports.setValue("X", "IV_REQUEST_DATA");
			if(auctionlisting.size()>100)
				imports.setValue(auctionlisting.size(), "IV_MAX_ROWS");
			
			JCO.Table materialList = tables.getTable("ET_PRODUCT");
			cat.logT(Severity.DEBUG, loc, "Return products are ET_PRODUCT "+ materialList);
			materialList.clear();
		
			connection.execute(getListFunc);
			
			JCO.Table returns = tables.getTable("ET_BAPIRETURN");
			if(returns.getNumRows()>0)
				cat.logT(Severity.DEBUG, loc, "ET_BAPIRETURN=" + returns);
		
			Map products = new HashMap();
			if(materialList.getNumRows()>0)
			{
				materialList.firstRow();
				do{
					String productId = materialList.getString("PRODUCT_ID");
					String productGuid = materialList.getString("PRODUCT_GUID");
					cat.logT(Severity.DEBUG, loc, "Loop in the return result, " +									"the product Id is " + productId);
					products.put(StringUtil.RemoveZero(productId), productGuid);
				}while(materialList.nextRow());
			}
		
			String language = null;
			try {
				connection.getJCoClient();
				language = connection.getAttributes().getLanguage();
			} catch (BackendException e) {
				language = "E";
				cat.log(Severity.DEBUG, loc, "can not get language from JCO connection", e);
			}
			Map unitConversion = (Map)unitConverLangMap.get(language);
			if(unitConversion == null) {
				unitConversion = new HashMap();
				unitConverLangMap.put(language, unitConversion);
			}				
			
			it = auctionlisting.iterator();
			while(it.hasNext())
			{
				Object item = it.next();
				StatusCheckable product = (StatusCheckable)item;
				BusinessObjectBase bo = (BusinessObjectBase)item;
				ProductStatusData  status = product.getStatus();
				if(status == null)
				{
					status = new ProductStatus();
					product.setStatus(status);
				}
			
				String extendId = StringUtil.RemoveZero(product.getProductId().toUpperCase());
				
				cat.logT(Severity.DEBUG, loc, "The checking against product Id is " + extendId);
				
				String productGuid = (String)products.get(extendId);
				String unit = product.getUnit();
				String convertedUnit;
				boolean checkUnit;
				if(productGuid != null)
				{
					status.setExist(true);
					cat.logT(Severity.DEBUG, loc, "After checking the product exists in system");
					if(unit == null || unit.length() <= 0)
						checkUnit = false;
					else {
						unit.toUpperCase();
						convertedUnit = (String)unitConversion.get(unit);
						if(convertedUnit == null || convertedUnit.length() <= 0)
							convertedUnit = convertUnit(connection, unit);
						if (convertedUnit == null || convertedUnit.length() <= 0)
							checkUnit = false;
						else {
							unitConversion.put(unit, convertedUnit);
							checkUnit = checkProductUnit(connection, productGuid, convertedUnit);
						}
					}
					if(!checkUnit) {
						status.setErrorCode(ProductStatusData.ERRCODE_WRONG_UNIT);
						bo.setInvalid();
						cat.logT(Severity.DEBUG, loc, "The unit of measure is wrong!");
					}
				}
				else
				{
					status.setExist(false);
					status.setErrorCode(ProductStatusData.ERRCODE_NONEXIST);
					bo.setInvalid();
					cat.logT(Severity.DEBUG, loc, "After checking the product DOES NOT exist in system");
				}
			}	
			return true;
	}

	private boolean checkProductUnit(JCoConnection connection, String guid, String unit) {
		try {
			JCO.Function func = connection.getJCoFunction(CRM_PRODUCT_UNIT_READ_ALL_RFC);
			JCO.ParameterList imports = func.getImportParameterList();
			imports.setValue(guid, "IV_PRODUCT_GUID");
			connection.execute(func);
			JCO.Table unitTable = func.getTableParameterList().getTable("ET_UNITS");
			
			if(unitTable.getNumRows()>0)
			{
				unitTable.firstRow();
				do{
					String productUnit = unitTable.getString("UNIT");
					cat.logT(Severity.DEBUG, loc, "the productUnit is " + productUnit);
					if(productUnit != null && productUnit.equalsIgnoreCase(unit))
						return true;
				}while(unitTable.nextRow());
			}
		} catch (BackendException e) {
			cat.log(Severity.ERROR, loc, e);
		}
		return false;
	}
	
	private String convertUnit(JCoConnection connection, String unit) {
		try {
			JCO.Function func = connection.getJCoFunction(CRM_CONVERSION_UNIT_INPUT_RFC);
			JCO.ParameterList imports = func.getImportParameterList();
			imports.setValue(unit, "INPUT");
			connection.execute(func);
			
			String result = func.getExportParameterList().getString("OUTPUT");
			if(result != null && result.length()>0)
				return result;
			else
				return null;
		} catch (BackendException e) {
			cat.log(Severity.ERROR, loc, e);
		}
		return null;
	}
	
	private void checkSingleSalesAreaATP(
		List sameSalesAreaList, 
		JCoConnection connection) 
		throws BackendException {
		cat.logT(Severity.DEBUG, loc, "single sales area product atp check");
		
		if(sameSalesAreaList == null || sameSalesAreaList.size()<=0)
			return;
			
		SalesAreaData salesArea = ((StatusCheckable)sameSalesAreaList.get(0)).getSalesArea();

		if(salesArea == null || salesArea.isEmpty())
			return;

		JCO.Function func = connection.getJCoFunction(CRM_AUC_PROD_ATP_CHECK);
		JCO.ParameterList tables = func.getTableParameterList();
		JCO.ParameterList imports = func.getImportParameterList();
		
		JCO.Structure salesAreaParam = imports.getStructure("IV_ISA_PCAT_VARIANT");
		salesAreaParam.clear();
		
        String org = salesArea.getSalesOrganization();
        if(org != null && org.length()>0)
        {
            salesAreaParam.setValue(org, "SALES_ORG");
        }
                
        String respOrg = salesArea.getResponsibleSalesOrganization();
        if(respOrg != null && respOrg.length()>0)
        {
            salesAreaParam.setValue(respOrg, "SALES_ORG_RESP"); 
        }
                
		String channel = salesArea.getDistributionChannel();
		if(channel != null && channel.length()>0)
		{
			salesAreaParam.setValue(channel, "DIST_CHANNEL");
		}
					
		String division = salesArea.getDivision();
		if(division != null && division.length()>0)
		{
			salesAreaParam.setValue(division, "DIVISION");
		}
					
		JCO.Table selection = tables.getTable("IT_PRODUCT");
		selection.clear();
		
		Iterator it = sameSalesAreaList.iterator();
		while(it.hasNext())
		{
			ProductDetail product = (ProductDetail)it.next();
			ProductStatusData  status = product.getStatus();
			if(status == null)
			{
				status = new ProductStatus();
				product.setStatus(status);
			}
						
			selection.appendRow();
			JCO.Field prodId_field = selection.getField("MATNR"); 
			StringUtil.setExtendIdField(prodId_field, product.getProductId());
			selection.setValue(product.getUnit(), "MEINH");
		}
		
		JCO.Table materialList = tables.getTable("ET_RESULT");
		materialList.clear();
		
		connection.execute(func);
		
		JCO.Table returns = tables.getTable("ET_RETURN");
		if(returns.getNumRows()>0)
			cat.logT(Severity.DEBUG, loc, "ET_RETURN=" + returns);
			
		if(materialList.getNumRows()>0)
		{
			materialList.firstRow();
			do{
				String productId = StringUtil.RemoveZero(materialList.getString("MATNR"));
				double qty = materialList.getDouble("WKBST");
				
			
				it = sameSalesAreaList.iterator();
				while(it.hasNext())
				{
					ProductDetail product = (ProductDetail)it.next();
					String extendedId = StringUtil.RemoveZero(product.getProductId());
					if(productId.equals(extendedId)){
						ProductStatusData  status = product.getStatus();
						
						if(qty > 0)
						{
							status.setQuantity(qty);
							double auctionableQuantity = product.getQuantity();
							if(qty >= auctionableQuantity)
								status.setAvailable(true);
							else
							{
								status.setAvailable(false);
								status.setErrorCode(ProductStatusData.ERRCODE_NOT_ENOUGH);
							}
						}
						else
						{
							status.setAvailable(false);
							status.setQuantity(0);
							status.setErrorCode(ProductStatusData.ERRCODE_NOT_AVAILABLE);
						}
						
						break;
					}
				}
			}while(materialList.nextRow());
		}
		
		
		return;
	}

	private Collection divideBySalesArea(List originList)
	{
		Hashtable salesAreaDivision = new Hashtable();
		Iterator it = originList.iterator();
		while(it.hasNext())
		{
			StatusCheckable element = (StatusCheckable)it.next();
//			Object key = new StatusCheckableHashWrapper(element);
//			System.out.println(key);
			Object key = element.getSalesArea();
			if(key == null)
				key = element.getProductId();
			
			List list = (List)salesAreaDivision.get(key);
			if(list == null)
			{
				list = new ArrayList();
				salesAreaDivision.put(key, list);
			}
			list.add(element);
		}
		return salesAreaDivision.values();
	}

}
