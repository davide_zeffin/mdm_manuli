/*
 * Created on Jan 28, 2004
 *
 * To change the template for this generated file go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
package com.sap.isa.auction.backend.crm.products.strategy;

import com.sap.isa.core.eai.BackendException;
import com.sap.isa.core.eai.sp.jco.JCoConnection;
import java.util.List;

/**
 * @author I803746
 *
 * To change the template for this generated type comment go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
public interface CheckStrategy {
	public final String CRM_AUC_PROD_ATP_CHECK = "CRM_AUC_PROD_ATP_CHECK";
	public final String CRM_AUC_PROD_GETLIST = "CRM_AUC_PROD_GETLIST";
	public final String CRM_PRODUCT_UNIT_READ_ALL_RFC = "CRM_PRODUCT_UNIT_READ_ALL_RFC";
	public final String CRM_CONVERSION_UNIT_INPUT_RFC = "CRM_CONVERSION_UNIT_INPUT_RFC";

	public boolean checkMaterialsATP(List auctionlisting,
									 JCoConnection connection)
							  throws BackendException;
							  
	public boolean checkMaterialsExist(List auctionlisting,
									 JCoConnection connection)
							  throws BackendException;
}
