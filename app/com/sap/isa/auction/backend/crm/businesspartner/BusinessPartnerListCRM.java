/*****************************************************************************
    Copyright (c) 2001, SAPMarkets Palo Alto, USA, All rights reserved.

*****************************************************************************/

package com.sap.isa.auction.backend.crm.businesspartner;

import java.util.*;
import com.sap.mw.jco.*;
import com.sap.tc.logging.*;
import com.sap.isa.auction.backend.boi.businesspartner.*;
import com.sap.isa.core.eai.sp.jco.*;
import com.sap.isa.core.*;
import com.sap.isa.core.eai.*;
import com.sap.isa.backend.*; //for JCoHelper
import com.sap.isa.auction.backend.boi.SearchData;
import com.sap.isa.auction.businessobject.businesspartner.BPListSearchData;
import com.sap.isa.auction.businessobject.businesspartner.AuctionBusinessPartner;
import com.sap.isa.businessobject.Address;
import com.sap.isa.auction.logging.CategoryProvider;
import com.sap.isa.auction.util.EAUtilities;


public class BusinessPartnerListCRM extends BackendBusinessObjectBaseSAP
implements BusinessPartnerListBackend {

    /**
     * Construtor
     */
    public BusinessPartnerListCRM() {
    }
    //for Logging
	private final static Category cat = CategoryProvider.getSystemCategory();
	private final static Location loc =
		Location.getLocation(BusinessPartnerListCRM.class);
    //private static IsaLocation log = IsaLocation.getInstance(BusinessPartnerListCRM.class.getName());

    /**
     * get the properties defined in eai-config.xml as params for BusinessPartnerList
     */
    public void initBackendObject(Properties props,
                                  BackendBusinessObjectParams params)
        throws BackendException {

        //this.props = props;

    }

    /**
     * Search busness partners from CRM based on criteria
     */
    public void crmSearchBPList(BusinessPartnerListData bpList,
                                SearchData criteria)
        throws BackendException

    {

        cat.logT(Severity.DEBUG, loc, "BusinessPartnerListCRM: entering crmSearchBPList(bplist,search)");

        JCoConnection connection = null;
        try {
            connection = getDefaultJCoConnection();
            //Get repository infos about this function
            JCO.Function bpSearch = connection.getJCoFunction("CRM_ISA_AUC_BP_SEARCH");
            JCO.ParameterList importParamList = bpSearch.getImportParameterList();
            //assign values to import parameters
            BPListSearchData criteria1 = (BPListSearchData)criteria;
            importParamList.setValue(criteria1.getLastName(),"IV_MC_NAME1");
            importParamList.setValue(criteria1.getFirstName(),"IV_MC_NAME2");
            importParamList.setValue(criteria1.getCity(),"IV_MC_CITY");
            importParamList.setValue(criteria1.getRegion(),"IV_REGION");
            importParamList.setValue(criteria1.getCountry(),"IV_COUNTRY");
            importParamList.setValue(criteria1.getPostCode(),"IV_POST_CODE");
            importParamList.setValue(criteria1.getOrganization(),"IV_NAME_ORG");
            if(criteria1.getB2B())
               importParamList.setValue("X","IV_B2B");
            if(criteria1.getB2C())
               importParamList.setValue("X","IV_B2C");


            // Translate the BP to right format.
            String bPartner = EAUtilities.getInstance()
                               .convertBPtoCRMFormat(criteria1.getPartnerID());

            importParamList.setValue(bPartner,"IV_PARTNER");
            //importParamList.setValue(criteria1.getPartnerID(),"IV_PARTNER");
            importParamList.setValue(criteria1.getTargetGroupGUID(),
                                     "IV_TG_GUID");
            connection.execute(bpSearch);

            // Get the exporting table from export parameter list
            //JCO.ParameterList exportParams = bpSearch.getExportParameterList();
            //JCO.Table bpListTable = exportParams.getTable("ET_BBPV_BUPA_ADDR");
            JCO.Table bpListTable = bpSearch.getTableParameterList().getTable("ET_BBPV_BUPA_ADDR");

            //process the table and put it into the BusinessPartnerList object
            setBPList(bpListTable, bpList);

        }
        catch(JCO.Exception ex) {
            cat.log(Severity.DEBUG, loc, "Error in calling CRM function: CRM_ISA_AUC_BP_SEARCH ", ex);
            JCoHelper.splitException(ex);
        }
        finally {
            //always close the JCo connection and release the client
            connection.close();
        }
    }


    /**
   * Search busness partners from CRM based on criteria
   */
    public void crmSearchBPListByTG(BusinessPartnerListData bpList,
                                    TechKey tgGUID)
        throws BackendException
    {
        cat.logT(Severity.DEBUG, loc, "BusinessPartnerListCRM: entering crmSearchBPListByTG(bplist,tgguid)");

        JCoConnection connection = null;

        try {
            connection = getDefaultJCoConnection();
            //Get repository infos about this function
            JCO.Function bpSearch = connection.getJCoFunction("CRM_ISA_AUC_BP_SEARCH");
            JCO.ParameterList importParamList = bpSearch.getImportParameterList();
            //assign values to import parameters
            importParamList.setValue(tgGUID.getIdAsString(),"IV_TG_GUID");

            connection.execute(bpSearch);

            // Get the exporting table from export parameter list
            //            JCO.ParameterList exportParams = bpSearch.getExportParameterList();
            // JCO.Table bpListTable = exportParams.getTable("ET_BBPV_BUPA_ADDR");

            JCO.Table bpListTable = bpSearch.getTableParameterList().getTable("ET_BBPV_BUPA_ADDR");

            //process the table and put it into the BusinessPartnerList object
            setBPList(bpListTable, bpList);

        }
        catch(JCO.Exception ex) {
            cat.log(Severity.DEBUG, loc, "Error in calling CRM function: CRM_ISA_AUC_BP_SEARCH ", ex);
            JCoHelper.splitException(ex);
        }
        finally {
            //always close the JCo connection and release the client
            connection.close();
        }
    }



    /**
     * put search result into BusinessPartnerList object
     */
    public void setBPList(JCO.Table bpTable,BusinessPartnerListData bpList) {

        HashMap resultList = new  HashMap();
        // Loop through the table to get the whole list
        if (bpTable.getNumRows() > 0) {
            do {
				AuctionBusinessPartner bp = new AuctionBusinessPartner();
                Address addr = new Address();
                // Translate the BP to right format.
                String bpid = EAUtilities.getInstance()
                               .convertBPtoCRMFormat(bpTable.getString("PARTNER"));
                bp.setId(bpid);

				cat.logT(Severity.DEBUG, loc, "Search BP - Id  " + bpTable.getString("PARTNER"));

                bp.setTechKey(new TechKey(bpTable.getString("PARTNER_GUID")));
                addr.setLastName(bpTable.getString("MC_NAME1"));
                addr.setFirstName(bpTable.getString("MC_NAME2"));
                addr.setCity(bpTable.getString("MC_CITY"));
                addr.setRegion(bpTable.getString("REGION"));
                addr.setPostlCod1(bpTable.getString("POST_CODE"));
                addr.setCountry(bpTable.getString("COUNTRY"));
                addr.setCoName(bpTable.getString("NAME_ORG"));
                bp.setAddress(addr);
                resultList.put(bp.getId(),bp);
            }
            while (bpTable.nextRow());
            bpList.setBPSearchResult(resultList);
        }
        else {
            bpList.setBPSearchResult(resultList);
            cat.logT(Severity.DEBUG, loc, "Table " + bpTable.getName() + " is empty");
        }
    }

    /**
 * Get the BP details for the CRM su01 User.
 */
    public void getBPFromUser(BusinessPartnerListData bpartner,
                              String userId) throws BackendException {
		AuctionBusinessPartner bp = null;

        cat.logT(Severity.DEBUG, loc, "getBPFromUser");
        JCoConnection connection = null;
        try {
            connection = getDefaultJCoConnection();
            JCO.Function bpuser = connection.getJCoFunction ("CRM_ICSS_BPARTNER_FROM_USER");

            JCO.ParameterList importParams = bpuser.getImportParameterList();

            importParams.setValue (userId.toUpperCase(), "USERID");
            connection.execute(bpuser);

            JCO.ParameterList exportParams = bpuser.getExportParameterList();
            String bpid = EAUtilities.getInstance()
                 .convertBPtoCRMFormat(exportParams.getString("BPARTNER"));

            cat.logT(Severity.DEBUG, loc, "BPId retrieved " + bpid);
            if(bpid != null) {

                JCO.Function bpdetails = connection.getJCoFunction("CRM_ISA_AUC_BP_SEARCH");

                importParams = bpdetails.getImportParameterList();

                importParams.setValue (bpid, "IV_PARTNER");

                connection.execute(bpdetails);

                JCO.Table bpdata = bpdetails.getTableParameterList().getTable("ET_BBPV_BUPA_ADDR");
                if(bpdata.getNumRows() > 0) {
                    bpdata.setRow(0);

                    bp = new AuctionBusinessPartner();
                    String finalbpid = EAUtilities.getInstance()
                                 .convertBPtoCRMFormat(bpdata.getString("PARTNER"));

                    bp.setId(finalbpid);
                    bp.setTechKey(new TechKey(bpdata.getString("PARTNER_GUID")));

                    Address addr = new Address();
                    addr.setLastName(bpdata.getString("MC_NAME1"));
                    addr.setFirstName(bpdata.getString("MC_NAME2"));
                    addr.setCity(bpdata.getString("MC_CITY"));
                    addr.setRegion(bpdata.getString("REGION"));
                    addr.setPostlCod1(bpdata.getString("POST_CODE"));
                    addr.setCountry(bpdata.getString("COUNTRY"));
                    addr.setCoName(bpdata.getString("NAME_ORG"));
                    bp.setAddress(addr);
                }

            }
            bpartner.setBusinessPartner(bp);

        }
        catch (JCO.Exception ex) {
            JCoHelper.splitException (ex);
        }
        finally {
            connection.close();

        }

    }

}
