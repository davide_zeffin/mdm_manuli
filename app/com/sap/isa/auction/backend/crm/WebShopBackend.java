/*****************************************************************************
    Copyright (c) 2001, SAPMarkets Palo Alto, USA, All rights reserved.

*****************************************************************************/

package com.sap.isa.auction.backend.crm;

import java.util.Properties;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.HashMap;

import com.sap.mw.jco.JCO;
import com.sap.tc.logging.Category;
import com.sap.tc.logging.Location;
import com.sap.tc.logging.Severity;


import com.sap.isa.core.eai.sp.jco.BackendBusinessObjectBaseSAP;
import com.sap.isa.core.eai.sp.jco.JCoConnection;
import com.sap.isa.backend.boi.isacore.ShopData;
import com.sap.isa.backend.boi.isacore.order.OrderData;
import com.sap.isa.backend.JCoHelper;
import com.sap.isa.core.eai.BackendException;
import com.sap.isa.core.TechKey;
import com.sap.isa.core.eai.BackendBusinessObjectParams;
import com.sap.isa.businesspartner.businessobject.BusinessPartner;
import com.sap.isa.businessobject.Shop;
import com.sap.isa.businessobject.Address;


import com.sap.isa.auction.backend.BackendHelper;
import com.sap.isa.auction.backend.boi.AuctionListingData;
import com.sap.isa.auction.backend.boi.AuctionData;
import com.sap.isa.auction.backend.boi.WinnerData;
import com.sap.isa.auction.logging.CategoryProvider;
import com.sap.isa.auction.util.EAUtilities;
import com.sap.isa.backend.crm.ShopCRM;


/**
 * Deals with all the interaction of the Web auction application with the CRM system.
 * Create/update quotation, Getting the BP/TG list info.
 * Send email via the email campaign management in CRM.
 */
public class WebShopBackend extends BackendBusinessObjectBaseSAP {

	private final static Category cat = CategoryProvider.getSystemCategory();
	private final static Location loc =
		Location.getLocation(WebShopBackend.class);
//    private static IsaLocation log = IsaLocation.getInstance(CRMAuctionManager.class.getName());

    // Used to bypass any CRM access during testing, if needed, defaulted to true
    private static boolean TEST_MODE = false;

    private Properties props;

    // For the quotation
    /**
     * Different create modes for a quotation.
     */
    private static final String MODE_CREATE = "A";
    private static final String MODE_UPDATE = "B";
    private static final String PROCESS = "process";
    private static final String TYPE = "type";
    private static final String UNIT = "unit";
    // End



    public WebShopBackend() {
    }

    public void initBackendObject(Properties props,
                                  BackendBusinessObjectParams params)
        throws BackendException {

        this.props = props;

    }

    /**
      * create the quotation(for a full-lot only) and save it into the CRM
      *
      * @param opp The opportunity bean which contains the auction Details
      * @param webshop contains the sales organization, currency, and distribution
      * channel
      */
    public String createQuotation(AuctionData opp, ShopData webshop, WinnerData winner)
        throws BackendException{
        String quotationId = null;
        if(!TEST_MODE) {
            JCoConnection connection = null;
            try {
                cat.logT(Severity.DEBUG, loc, "Start CreateQuotation(Opp, WebShop)");

                connection = getDefaultJCoConnection();
                JCO.Function quotation = connection.getJCoFunction("CRM_ISA_AUC_QUOTE_CREATE_MULT");

                // Get import parameters
                JCO.ParameterList importParams = quotation.getImportParameterList();

                // Set the import parameter value
                String startDate = EAUtilities.getInstance().timestampToCRMTimestamp(opp.getStartDate());
                String expDate = EAUtilities.getInstance().timestampToCRMTimestamp(opp.getQuotationExpirationDate());
                // Read the process name from the eai-config.xml params
                String iv_process = props.getProperty(PROCESS);

                cat.logT(Severity.DEBUG, loc, "startdate: " +startDate);
                cat.logT(Severity.DEBUG, loc, "expdate: " +expDate);
                cat.logT(Severity.DEBUG, loc, "The process is: " +iv_process);
                cat.logT(Severity.DEBUG, loc, "The sales org: " + webshop.getSalesOrganisation());
                cat.logT(Severity.DEBUG, loc, "The sales resp: " + webshop.getResponsibleSalesOrganisation()+"/");
                cat.logT(Severity.DEBUG, loc, "The distri channel:" + webshop.getDistributionChannel());
                cat.logT(Severity.DEBUG, loc, "The currency:" + webshop.getCurrency());
                cat.logT(Severity.DEBUG, loc, "The Division:" + webshop.getDivision());
                cat.logT(Severity.DEBUG, loc, "The Seller:" + opp.getSellerId().toUpperCase());

                importParams.setValue(startDate, "IV_VALID_FROM");
                importParams.setValue(expDate, "IV_VALID_TO");
                importParams.setValue(opp.getSellerId().toUpperCase(),
                                      "IV_USERNAME");
                importParams.setValue(iv_process, "IV_PROCESS");
                importParams.setValue(webshop.getSalesOrganisation(),
                                      "IV_SALES_ORG");

                String salesUnit = webshop.getResponsibleSalesOrganisation();

                if (salesUnit == null || salesUnit.equals("")) {
                    importParams.setValue(webshop.getSalesOrganisation(),
                                          "IV_SALES_ORG_RESP");
                }
                else {
                    importParams.setValue(webshop.getResponsibleSalesOrganisation(),
                                          "IV_SALES_ORG_RESP");
                }
                importParams.setValue(webshop.getDistributionChannel(),
                                      "IV_DIS_CHANNEL");

                if(webshop.getDivision().trim().length() == 0)
                    importParams.setValue("00","IV_DIVISION");
                else
                    importParams.setValue(webshop.getDivision(),"IV_DIVISION");

                importParams.setValue(MODE_CREATE, "IV_MODE");
                importParams.setValue(webshop.getCurrency(), "IV_CURRENCY");

                //fill the import table: IT_PRODUCT and  IT_QUANTITY
                fillImportTables(importParams, opp, winner);

                //execute the quotation
                connection.execute(quotation);

                // Get the exporting table
                JCO.Table returnTable = quotation.getExportParameterList().getTable("ET_RETURN");
                JCO.Table savedTable = quotation.getExportParameterList().getTable("ET_SAVED_OBJECTS");

                //process the tables and give the necessary message to user if errors occour
                boolean hasError = BackendHelper.processBackendReturn(returnTable);
                if(!hasError) {
                    quotationId = getGuidFromTable(savedTable);
                }


            }
            catch (JCO.Exception ex) {
                cat.log(Severity.DEBUG, loc, "Error in calling CRM function: ", ex);
                JCoHelper.splitException (ex);
            }
            finally {
                //always close the JCo connection and release the client
                connection.close();
            }
        }
        cat.logT(Severity.DEBUG, loc, "QuotationId: " + quotationId);
        return quotationId;

    }
    
	public String createQuotation(AuctionData opp, ShopData webshop)
		throws BackendException{
		return createQuotation(opp, webshop, null);
	}
    

    /**
    * create the quotation with the auction data and save it into the CRM
    *
    * @param opp The opportunity bean which contains the auction Details
    */
    public String createQuotation(AuctionData opp)
        throws BackendException {

        String quotationId = null;
        ShopData webshop = null;
        if(!TEST_MODE) {
            cat.logT(Severity.DEBUG, loc, "Start CreateQuotation(Opp)");
            // Get the shop details.
            webshop = this.getShopDetails(opp.getWebShopId());

            // get the quotation
            quotationId = this.createQuotation(opp,webshop);
            cat.logT(Severity.DEBUG, loc, "QuotationId: " + quotationId);

        }
        return quotationId;
    }



    /**
    * create the quotation(for a Broken-lot ) and save it into the CRM
    *
    * @param opp The opportunity bean which contains the auction Details
    * @param winner List of winner for the auction
    * Returns a list of auctionid, quotationid pair
    */
    public HashMap createQuotation(AuctionData opp, ArrayList winners)
        throws BackendException{

        HashMap aucQuote = new HashMap();
        String quotationId = null;
		ShopData webshop = null;

        cat.logT(Severity.DEBUG, loc, "Start CreateQuotation(Opp, winners)");
        // Get the shop details.
        webshop = this.getShopDetails(opp.getWebShopId());
        for(int i=0; i<winners.size(); i++) {
            WinnerData winner = (WinnerData)winners.get(i);
			quotationId = createQuotation(opp, webshop, winner);
            if(quotationId != null) {
                cat.logT(Severity.DEBUG, loc, "QuotationId for Auction:" + quotationId + " " +opp.getAuctionName());
                aucQuote.put(winner.getWinnerId(), quotationId);
            }
        }
        return aucQuote;

    }

    /**
     * Used to get the shop details for the Creation of quotation
     * This include the SalesOrg, SalesOrgResp and Dist. Channel
     */
    public ShopData getShopDetails(String shopId) throws BackendException {
        Shop shop = new Shop();
        JCoConnection connection = null;
        cat.logT(Severity.DEBUG, loc, "Entering AuctionCRMController-getShopDetails()");

        cat.logT(Severity.DEBUG, loc, "ShopID " + shopId.toUpperCase());

        try {
            connection = getDefaultJCoConnection();
            // Get the Catalog / variant
            JCO.Function function = connection.getJCoFunction("CRM_ISA_SHOP_DATA_GET");

            JCO.ParameterList importParams = function.getImportParameterList();

            importParams.setValue(shopId.toUpperCase(), "SHOP");

            connection.execute(function);

            JCO.ParameterList exportParams = function.getExportParameterList();

            JCO.Record shopDetail = function.getExportParameterList().getStructure("SHOP_DATA");

            String catalog = shopDetail.getString("CATALOG");
            String variant = shopDetail.getString("VARIANT");

            // Get the shop data
            JCO.Function variantReadData =
                connection.getJCoFunction("CRM_ISA_PCAT_VARIANT_DATA_GET");
            importParams = variantReadData.getImportParameterList();
            importParams.setValue(catalog,"CATALOG");
            importParams.setValue(variant,"VARIANT");

            connection.execute(variantReadData);
            exportParams = variantReadData.getExportParameterList();
            JCO.Record variantDetail =
                variantReadData.getExportParameterList().getStructure("VARIANT_DATA");
            shop.setSalesOrganisation(variantDetail.getString("SALES_ORG"));
            shop.setResponsibleSalesOrganisation(variantDetail.getString("SALES_ORG_RESP"));
            shop.setDistributionChannel(variantDetail.getString("DIST_CHANNEL"));
            shop.setCurrency(variantDetail.getString("MAIN_CURRENCY"));
            shop.setDivision(variantDetail.getString("DIVISION"));

            String myCatalog = ShopCRM.getCatalogKey(catalog, variant);
            shop.setCatalog(myCatalog);

            cat.logT(Severity.DEBUG, loc, "Sales Org :" + variantDetail.getString("SALES_ORG"));
            cat.logT(Severity.DEBUG, loc, "Res Sales Org :" + variantDetail.getString("SALES_ORG_RESP"));
            cat.logT(Severity.DEBUG, loc, "Dist Channel :" + variantDetail.getString("DIST_CHANNEL"));
            cat.logT(Severity.DEBUG, loc, "Currency :" + variantDetail.getString("MAIN_CURRENCY"));
            cat.logT(Severity.DEBUG, loc, "Division :" + variantDetail.getString("SALES_ORG"));


        } catch (JCO.Exception ex) {
            cat.log(Severity.DEBUG, loc, "Error in calling CRM function: ", ex);
            JCoHelper.splitException (ex);
        }
        finally {
            //always close the JCo connection and release the client
            connection.close();

        }
        return shop;

    }


    /**
     * Used to update the quotation (Called when the auction is modified,
     * Auction attributes can be modified as long as it is in 'OPen' state
     */
    public boolean updateQuotation(AuctionData opp) throws BackendException{
		return updateQuotation(opp, null);
    }

    /**
     * Update a quotation with the winner details.
     * The auction winner and winning amount is updated into the
     * quotation.
     */
    public boolean updateQuotation(AuctionData opp,
                                   WinnerData winner) throws BackendException {
        boolean hasError = false;
        cat.logT(Severity.DEBUG, loc, "Entering UpdateQuotation(Auction,winner) ");
        
		JCO.Table returnTable= null, savedTable = null; 

        if(!TEST_MODE) {
            JCoConnection connection = null;
            try {
                // Read the process name from the eai-config.xml params
                String iv_process = props.getProperty(PROCESS);
                connection = getDefaultJCoConnection();
                JCO.Function quotation = connection.getJCoFunction("CRM_ISA_AUC_QUOTE_CREATE_MULT");

                JCO.ParameterList importParams = quotation.getImportParameterList();

                importParams.setValue(opp.getQuotationId(),
                                      "IV_QUOTE_GUID");
                importParams.setValue(opp.getAuctionId(),
                                      "IV_OPPORTUNITY_ID");
                importParams.setValue(iv_process, "IV_PROCESS");
                importParams.setValue(MODE_UPDATE, "IV_MODE");

				cat.logT(Severity.DEBUG, loc, "Quote Guid : " + opp.getQuotationId());
				cat.logT(Severity.DEBUG, loc, "opp id : " + opp.getAuctionId());

				if(winner != null) {
	                // get the CRMuserID
	                String buyer = this.getUserFromBP(winner.getBuyerId());
	                importParams.setValue(buyer.toUpperCase(),
	                                      "IV_USERNAME"); //user id
					cat.logT(Severity.DEBUG, loc, "Buyer : " + buyer.toUpperCase());
				}

                //fill the import table: with the winner details
                fillImportTables (importParams, opp, winner);
                connection.execute(quotation);

                cat.logT(Severity.DEBUG, loc, "after execute the function");
                // Get the exporting table
                returnTable = quotation.getExportParameterList().getTable("ET_RETURN");
                savedTable = quotation.getExportParameterList().getTable("ET_SAVED_OBJECTS");
            }
            catch (JCO.Exception ex) {
	            cat.log(Severity.DEBUG, loc, "Error in calling CRM function: ", ex);
                JCoHelper.splitException (ex);
            }
            catch(Exception e) {
                cat.log(Severity.DEBUG, loc, "exception happened in quotation crm", e);
            }
            finally {
                //always close the JCo connection and release the client
                connection.close();
            }
        }
		//process the tables and give the necessary message to user if errors occour
		hasError = BackendHelper.processBackendReturn(returnTable);
		
        if(hasError)
            return false;
        else
            return true;

    }

    /**
     * Not implemented, maybe needed for later releases
     */
    public void createOrder(OrderData order) throws BackendException{};

    /**
     * Not implemented, maybe needed for later releases
     */
    public void deleteOrder(OrderData order) throws BackendException{};


    /**
     * Get the BP details for the CRM su01 User.
     */
    public BusinessPartner getBPFromUser(String userId) throws BackendException {
        BusinessPartner bp = null;

        if(!TEST_MODE) {
            cat.logT(Severity.DEBUG, loc, "getBPFromUser");
            JCoConnection connection = null;
            try {
                connection = getDefaultJCoConnection();
                JCO.Function bpuser = connection.getJCoFunction ("CRM_ICSS_BPARTNER_FROM_USER");

                JCO.ParameterList importParams = bpuser.getImportParameterList();

                importParams.setValue (userId.toUpperCase(), "USERID");
                connection.execute(bpuser);

                JCO.ParameterList exportParams = bpuser.getExportParameterList();
                String bpid = EAUtilities.getInstance()
                .convertBPtoCRMFormat(exportParams.getString("BPARTNER"));

				cat.logT(Severity.DEBUG, loc, "BPId retrieved " + bpid);
                if(bpid != null) {

                    JCO.Function bpdetails = connection.getJCoFunction("CRM_ISA_AUC_BP_SEARCH");

                    importParams = bpdetails.getImportParameterList();

                    importParams.setValue (bpid, "IV_PARTNER");

                    connection.execute(bpdetails);

                    JCO.Table bpdata = bpdetails.getTableParameterList().getTable("ET_BBPV_BUPA_ADDR");
                    if(bpdata.getNumRows() > 0) {
                        bpdata.setRow(0);

                        bp = new BusinessPartner();
                        String finalbpid = EAUtilities.getInstance()
                        .convertBPtoCRMFormat(bpdata.getString("PARTNER"));

                        bp.setId(finalbpid);
                        bp.setTechKey(new TechKey(bpdata.getString("PARTNER_GUID")));

                        Address addr = new Address();
                        addr.setLastName(bpdata.getString("MC_NAME1"));
                        addr.setFirstName(bpdata.getString("MC_NAME2"));
                        addr.setCity(bpdata.getString("MC_CITY"));
                        addr.setRegion(bpdata.getString("REGION"));
                        addr.setPostlCod1(bpdata.getString("POST_CODE"));
                        addr.setCountry(bpdata.getString("COUNTRY"));
                        addr.setCoName(bpdata.getString("NAME_ORG"));
                        bp.setAddress(addr);
                    }

                }

            }
            catch (JCO.Exception ex) {
                JCoHelper.splitException (ex);
            }
            finally {
                connection.close();

            }
        }
        return bp;

    }


    /**
     * Get the CRM su01 user from BP.
     */
    public String getUserFromBP(String bpid) throws BackendException {
        String user = null;
        if(!TEST_MODE) {
            cat.logT(Severity.DEBUG, loc, "getUSerFromBP");
            JCoConnection connection = null;
            try {
                connection = getDefaultJCoConnection();
                JCO.Function bpuser = connection.getJCoFunction ("BUPA_NUMBERS_GET");

                JCO.ParameterList importParams = bpuser.getImportParameterList();

                String finalbpid = EAUtilities.getInstance().convertBPtoCRMFormat(bpid);

                cat.logT(Severity.DEBUG, loc, "getUSerFromBP - Padded bpid " + finalbpid );

                importParams.setValue (finalbpid, "IV_PARTNER");
                connection.execute(bpuser);

                JCO.ParameterList exportParams = bpuser.getExportParameterList();
                String bpguid = exportParams.getString("EV_PARTNER_GUID");

                cat.logT(Severity.DEBUG, loc, "getUSerFromBP - bpguid " + bpguid);

                if(bpguid == null) return null;

                JCO.Function userfunc = connection.getJCoFunction("CRM_CENTRALPERSON_GET");

                importParams = userfunc.getImportParameterList();

                importParams.setValue (bpguid, "IV_BU_PARTNER_GUID");

                connection.execute(userfunc);

                user = userfunc.getExportParameterList().getString("EV_USERNAME");

                cat.logT(Severity.DEBUG, loc, "getUSerFromBP - user " + user );


            }
            catch (JCO.Exception ex) {
                JCoHelper.splitException (ex);
            }
            finally {
                connection.close();

            }
        }
        return user.toUpperCase();

    }


    /**
     * Get the InternetAlias from BPID
     */
    public String getInternetUserFromBP(String bpid) throws BackendException {
        String internetUser = null;
        if(!TEST_MODE) {
            cat.logT(Severity.DEBUG, loc, "getInternetUserFromBP");
            JCoConnection connection = null;
            try {
                connection = getDefaultJCoConnection();
                // get the CRM user for the BP
                String user = this.getUserFromBP(bpid);


                JCO.Function func = connection.getJCoFunction ("BAPI_USER_GET_DETAIL");

                JCO.ParameterList importParams = func.getImportParameterList();

                importParams.setValue (user, "USERNAME");
                connection.execute(func);

                JCO.ParameterList exportParams = func.getExportParameterList();
                internetUser = exportParams.getString("ALIAS");
            }
            catch (JCO.Exception ex) {
                JCoHelper.splitException (ex);
            }
            finally {
                connection.close();

            }
        }
        return internetUser;

    }
    /**
     * Fill the import table necessary to the JCO function call when
     * Creating/updating the quotation
     * The import table consists of the product details such as product_id
     * quantity, price.
     *
     * @param importParams JCO function call import parameter list
     * @param opp The auction data that contains the product details
     */
    private void fillImportTables (JCO.ParameterList importParams,
                                   AuctionData opp, WinnerData winner){

        String type = props.getProperty(TYPE);
        String unit = props.getProperty(UNIT);

        Iterator iter = opp.getListings().iterator();

        JCO.Table itemTbl = importParams.getTable("IT_ITEMS");

        // Broken - Lot
        if(opp.isBrokenLot() && winner != null) {
            itemTbl.appendRow();
            // Get the product from the listing that matches
            // the one in the winnerdata
            String listingId = winner.getAuctionListingId();
            while(iter.hasNext()) {
                AuctionListingData item= (AuctionListingData)iter.next();
                if(item.getId().equals(listingId)) {
                    itemTbl.setValue(item.getProductCode(),"PRODUCT");
                    break;
                }
            }
            itemTbl.setValue(winner.getQuantity().toString(), "QUANTITY");
            itemTbl.setValue(winner.getBidAmount().toString(),"COND_RATE");
           cat.logT(Severity.DEBUG, loc, "Auction " + opp.getAuctionName());
           cat.logT(Severity.DEBUG, loc, "Winning Bid " + winner.getBidAmount().toString());
            itemTbl.setValue(type, "COND_TYPE");
            itemTbl.setValue(unit, "COND_UNIT");
        } else {   // Full Lot

            // This parameter has been introduced to support the
            // update of only one product with the price details in case
            // of full-lot multi product item.
            boolean priceUpdated = false;

            while(iter.hasNext()) {
                //put the productId and quantity into the tables
                AuctionListingData product = (AuctionListingData)iter.next();
                itemTbl.appendRow();
                itemTbl.setValue(product.getProductCode(),"PRODUCT");
                itemTbl.setValue(Double.toString(product.getQuantity()),
                                 "QUANTITY");
                if(winner != null) {
                    if(opp.getListings().size() > 1) {
                        if(!priceUpdated) {
                            // do not update if price is already updated.
                            // All these gimmicks are so that the quotation does not add up
                            // the price for each product
                            priceUpdated = true;
                            itemTbl.setValue(winner.getBidAmount().toString(),
                                             "COND_RATE");

                           cat.logT(Severity.DEBUG, loc, "Auction " + opp.getAuctionName());
                           cat.logT(Severity.DEBUG, loc, "Winning Bid " + winner.getBidAmount().toString());
                        } else {
                            itemTbl.setValue("0","COND_RATE");
                        }
                    } else {
                        itemTbl.setValue(winner.getBidAmount().toString(),
                                         "COND_RATE");
                       cat.logT(Severity.DEBUG, loc, "Auction " + opp.getAuctionName());
                       cat.logT(Severity.DEBUG, loc, "Winning Bid " + winner.getBidAmount().toString());

                    }
                    itemTbl.setValue(type, "COND_TYPE");
                    itemTbl.setValue(unit, "COND_UNIT");
                }
            }
        }
    }

    /**
     * Get the quotation guid and save it to the opportunity bean
     *
     * @param table JCO table which contains the guid
     * @return guid of the quotation.
     */
    private String getGuidFromTable (JCO.Table table) {

        String value = null;
        if (table.getNumRows() > 0){
            do {
                value = table.getString("GUID");
            }
            while (value.equals(null) && table.nextRow());
            if (value != null) {
            	cat.logT(Severity.DEBUG, loc, "the quotation Id: " + value);
            }
            else {
				cat.logT(Severity.DEBUG, loc, "Table " + table.getName() + " doesn't contain the GUID!");
            }
        }
        else {
			cat.logT(Severity.DEBUG, loc,"Table " + table.getName() + " is empty");
        }
        return value;
    }


}
