/*****************************************************************************
    Copyright (c) 2001, SAPMarkets Palo Alto, USA, All rights reserved.

*****************************************************************************/
package com.sap.isa.auction.backend.r3.salesdocument.strategy;

import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Properties;

import com.sap.isa.auction.backend.boi.BackendSettingsData;
import com.sap.isa.auction.backend.boi.order.AuctionOrderData;
import com.sap.isa.auction.backend.boi.order.AuctionSalesDocumentData;
import com.sap.isa.auction.backend.exception.BackendUserException;
import com.sap.isa.auction.backend.r3.AuctionRFCConstantsR3;
import com.sap.isa.auction.backend.r3.R3BackendAuctionData;
import com.sap.isa.auction.bean.BackendDefaultSettings;
import com.sap.isa.auction.logging.CategoryProvider;
import com.sap.isa.backend.boi.isacore.AddressData;
import com.sap.isa.backend.boi.isacore.SalesDocumentData;
import com.sap.isa.backend.r3.salesdocument.rfc.CreateStrategyR3;
import com.sap.isa.backend.r3.salesdocument.rfc.ReadStrategy;
import com.sap.isa.backend.r3.salesdocument.rfc.WriteStrategy;
import com.sap.isa.backend.r3base.Extension;
import com.sap.isa.backend.r3base.ExtensionParameters;
import com.sap.isa.backend.r3base.ipc.ServiceR3IPC;
import com.sap.isa.backend.r3base.rfc.RFCConstants;
import com.sap.isa.backend.r3base.rfc.RFCWrapperPreBase;
import com.sap.isa.backend.r3base.rfc.ReturnValue;
import com.sap.isa.backend.r3base.util.Conversion;
import com.sap.isa.backend.r3base.util.R3BackendData;
import com.sap.isa.businessobject.quotation.Quotation;
import com.sap.isa.core.TechKey;
import com.sap.isa.core.eai.BackendException;
import com.sap.isa.core.eai.sp.jco.JCoConnection;
import com.sap.isa.payment.backend.boi.PaymentBaseData;
import com.sap.isa.payment.backend.boi.PaymentMethodData;
import com.sap.isa.payment.businessobject.COD;
import com.sap.mw.jco.JCO;
import com.sap.mw.jco.JCO.Structure;
import com.sap.tc.logging.Category;
import com.sap.tc.logging.Location;
import com.sap.tc.logging.Severity;


/**
 * This class is the extension fo the CreateStrategyR3 class, it only overwrite
 * some method for the auction case, since we do not need IPC service
 * This calss has two several functions, which are different with its parent.
 * 	1. Set the material and plant information as well as storage information.
 *  2. Set the sales employee information
 *  3. Set the price information, which is not from IPC
 *  4. The customer purchase order number, in R3, it is PURCH_NO_C
 **/

public class AuctionCreateStrategyR3 extends CreateStrategyR3 implements AuctionStrategyBase{
	/** The logging instance. */
	private final static Category cat = CategoryProvider.getSystemCategory();
	private final static Location loc =
		Location.getLocation(AuctionCreateStrategyR3.class);
	private Properties props = null;
	private BackendSettingsData backendSettings = null;
	
	/**
	 * Constructor of the class
	 * @param backendData    the R/3 specific backend parameters
	 * @param writeStrategy  the group of algorithms needed for writing to the create,
	 *        change and simulate RFC's
	 * @param readStrategy   for reading sales document info
	 */
	public AuctionCreateStrategyR3(R3BackendData backendData,
							WriteStrategy writeStrategy, 
							ReadStrategy readStrategy) {
		super(backendData, writeStrategy, readStrategy);
		cat.logT(Severity.DEBUG, loc, "In the construction of AuctionCreateStrategyR3");	
	}

	/**
	 * Creating a sales document in R/3. If the RFC call is successfull i.e. there are no
	 * error messages from the backend, a commit will be performed internally. If there
	 * are error messages from R/3, a message will be attached to the ISA document
	 * header.
	 * 
	 * @param posd                  the ISA order
	 * @param ccardObject           the instance that holds the credit card info if this
	 *        is available. Normally of type JCO.Table, but can be changed project
	 *        specific
	 * @param newShipToAddress      a new shipTo address on header level (relevant for
	 *        b2c)
	 * @param documentType          The R/3 type of the document to be created like
	 *        standard order, or standard inquiry. This type is a shop attribute (one
	 *        attribute for orders, one for inquiries)
	 * @param connection            the connection to the backend system
	 * @param serviceR3IPC          the service object that deals with IPC when running
	 *        ISA R/3
	 * @return R/3 messages
	 * @exception BackendException  an exception from R/3
	 */
	public ReturnValue createDocument(SalesDocumentData posd, 
									  Object ccardObject, 
									  AddressData newShipToAddress, 
									  String documentType, 
									  JCoConnection connection, 
									  ServiceR3IPC serviceR3IPC)
							   throws BackendException {
		cat.logT(Severity.DEBUG, loc, "createDocument in AuctionCreateStrategy3 Start");
		Locale locale = backendData.getLocale();
		cat.logT(Severity.INFO, loc, "createDocument in AuctionCreateStrategy3 locale is"
								+ locale.toString() + " " + locale.getLanguage());
		//connection.setAbapDebug(true);

		//If for some reasons this shop setting isn't enabled, call the
		//check again here.
		setBlankProductIds(posd);

		//call RFC
		JCO.Function sdSalesDocCreate = callRfcSd_SalesDocument_Create(
												posd, 
												ccardObject, 
												newShipToAddress, 
												"", 
												documentType, 
												connection, 
												serviceR3IPC);

		//and evaluate the response
		//set sales document number and header information. Retrieve from key table
		String salesDocNumber = "";
		JCO.Table keyTable = sdSalesDocCreate.getTableParameterList().getTable(
									 "SALES_KEYS");
		if (keyTable.getNumRows() > 0) {
			keyTable.firstRow();
			do {
				String refObject = keyTable.getString("REFOBJECT");
				cat.logT(Severity.DEBUG, loc, "Output table the refObject is " + refObject);
				if (refObject.equals("HEADER")) {
					salesDocNumber = keyTable.getString("DOC_NUMBER");

					cat.logT(Severity.DEBUG, loc, "header key found" + salesDocNumber );
				}
			}
			 while (keyTable.nextRow());
		}else{
			cat.logT(Severity.ERROR, loc, "Can not get the document number");
			throw new BackendException("Can not get sales document number");
		}

		//String salesDocNumber = response.getParams().getSalesdocument();
		cat.logT(Severity.DEBUG, loc, "sales document number from R/3: " + salesDocNumber);

		posd.getHeaderData().setSalesDocNumber(Conversion.cutOffZeros(
													   salesDocNumber));
		posd.getHeaderData().setTechKey(new TechKey(salesDocNumber));

		Date today = new Date(System.currentTimeMillis());
		posd.getHeaderData().setChangedAt(Conversion.dateToUIDateString(
												  today, 
												  locale));
		posd.setTechKey(new TechKey(salesDocNumber));

		JCO.Table retTable = sdSalesDocCreate.getTableParameterList().getTable(
		"RETURN");
		
		handleBackendUserException(retTable, connection);
		
		ReturnValue retVal =
			new ReturnValue(
				sdSalesDocCreate.getTableParameterList().getTable("RETURN"),
				"");

//		if (!readStrategy.addSelectedMessagesToBusinessObject(retVal, posd)) {
//			commit(connection, X);
//		} else {
//
//			//Order could not be created - better stop now
//			throw new BackendException("order create not possible");
//		}
		cat.logT(Severity.DEBUG, loc, "Before perform the commmit operation");

		if (retVal != null) {   //!readStrategy.addSelectedMessagesToBusinessObject(retVal, posd)) {
			cat.logT(Severity.DEBUG, loc, "The return value is " + retVal.toString());
			//commit(connection, 
			//	   X);
		}
		 else {

			//Order could not be created - better stop now
			throw new BackendException("order create not possible");
		}

		///For some reasons, the manual price can not be set correctly
		//if(!(posd instanceof QuotationData))
			//setPriceCondition(posd, connection); 
		cat.logT(Severity.DEBUG, loc, "createDocument in AuctionCreateStrategy3 End");
		return retVal;
	}

	/**
	 * Set the winning price to the sales order header level
	 * This method really set the total price including all the tax and 
	 * shipping cost.
	 * 
	 * @param posd				The sales document 
	 * @param connection		JCo connection
	 * @throws BackendException Exception ffrom backend, if anything went wrong
	 */
	private void setPriceCondition(
		SalesDocumentData posd,
		JCO.Function sdSalesDocCreate)
		throws BackendException {
		cat.logT(Severity.DEBUG, loc, "setPriceCondition in AuctionCreateStrategy3 Start");

		if(posd instanceof Quotation) {
			String enabled = props.getProperty("setPriceCondition");
			if(enabled != null && enabled.equalsIgnoreCase("false"))
				return;
		}
		
//		String salesDocNumber = posd.getHeaderData().getSalesDocNumber();
//		if(salesDocNumber == null || salesDocNumber.length()<=0)
//			return;
		
		String totalPriceCondition = null;
//		cat.logT(Severity.DEBUG, loc, "The sales document number is" + salesDocNumber);		
//		JCO.Function sdSalesDocChange =
//			connection.getJCoFunction(
//				RFCConstants.RfcName.SD_SALESDOCUMENT_CHANGE);
//
//		JCO.ParameterList request = sdSalesDocChange.getImportParameterList();
		JCO.ParameterList tableList = sdSalesDocCreate.getTableParameterList();
//
//		request.setValue(
//			StringUtil.ExtendNumber(salesDocNumber, 10),
//			"SALESDOCUMENT");
//
//		JCO.Structure header_inx = request.getStructure("ORDER_HEADER_INX");
//		header_inx.setValue("U", "UPDATEFLAG");

		JCO.Table conditionsIn = tableList.getTable("SALES_CONDITIONS_IN");
		conditionsIn.clear();
		conditionsIn.appendRow();
		if(posd instanceof AuctionSalesDocumentData){
			totalPriceCondition = ((AuctionSalesDocumentData)posd).getTotalManualPriceCondition();
		}
		if(totalPriceCondition == null && backendSettings != null)
			totalPriceCondition = backendSettings.getTotalPriceCondiction();
		if (totalPriceCondition == null)
			totalPriceCondition = 
				props.getProperty(AuctionRFCConstantsR3.TOTAL_PRICE_CONDITION);
		cat.logT(Severity.DEBUG, loc, 
				"The totalPriceCondition is" + totalPriceCondition);
		
		if(totalPriceCondition == null){
			totalPriceCondition = 
					AuctionRFCConstantsR3.DEFAULT_PRICE_CONDITION;
		}
		conditionsIn.setValue("000010", "ITM_NUMBER");   
		conditionsIn.setValue(
				totalPriceCondition,
				"COND_TYPE");
		conditionsIn.setValue(posd.getHeaderData().getCurrency(), "CURRENCY");
		conditionsIn.setValue("1", "COND_P_UNT");

		String price = posd.getHeaderData().getGrossValue();
		try {
			double val = Double.parseDouble(price);
		} catch (Exception ex) {
			cat.logT(Severity.ERROR, loc, "The passing price is not correct " + price + " " +
				" And the price is et to zero");
			price = "0";
		}
		conditionsIn.setValue(price, "COND_VALUE");
		
		cat.logT(Severity.DEBUG, loc, "conditiontable=" + conditionsIn);
//		JCO.Table conditionsInx = tableList.getTable("CONDITIONS_INX");
//		conditionsInx.clear();
//		conditionsInx.appendRow();
//		conditionsInx.setValue("000010", "ITM_NUMBER");   
//		conditionsInx.setValue(
//			totalPriceCondition,
//			"COND_TYPE");
//		conditionsInx.setValue("X", "COND_VALUE");
//		conditionsInx.setValue("X", "UPDATEFLAG");
//		conditionsInx.setValue("X", "CURRENCY");
//		conditionsInx.setValue("X", "COND_P_UNT");
//
//		connection.execute(sdSalesDocChange);
//
//		JCO.Table retTable = tableList.getTable("RETURN");
//		handleBackendUserException(retTable, connection);
//		cat.logT(Severity.DEBUG, loc, "setPriceCondition in AuctionCreateStrategy3 ends");
	}
		

	/**
	 * Copies the document into a new one, uses the R/3 copy mechanism and creates a
	 * document flow. Uses function module SD_SALESDOCUMENT_CREATE to link to the 
	 * quotation document, but it has to be ensured that the sales order data
	 * has been filled correctly. This implementation is very different
	 * with ISA implementation
	 * 
	 * @param posd             ISA source document
	 * @param targetDoc                ISA target document. Will not be filled entirely
	 *        but gets its new techkey
	 * @param connection            JCO connection
	 * @param targetDocumentType        the R/3 document type like standard order. Comes
	 *        from a shop attribute
	 * @return R/3 messages
	 * @exception BackendException exception from backend
	 */
	public ReturnValue copyDocument(String quoteId, 
									SalesDocumentData targetDoc, 
									String targetDocumentType, 
									AddressData newShipToAddress,
									JCoConnection connection)
							 throws BackendException {
		// let us set the  quotation id here
		cat.logT(Severity.DEBUG, loc, "copyDocument in AuctionCreateStrategy3 Start");
		
		if (targetDoc instanceof AuctionOrderData){
			((AuctionOrderData)targetDoc).setPredecessorDocumentId(quoteId);
			
		}
		return createDocument(targetDoc, null, newShipToAddress, targetDocumentType,
												connection, null);
	}

	/**
	 * Calls the RFC SD_SALESDOCUMENT_CREATE with the ISA sales document data. Fires the
	 * RFC and then returns the JCO function to let the caller evaluate the response.
	 * Used for simulating and creating a document.
	 * This method extended from its super class ISA R3 implementation
	 * The purpose is to have the manuall price
	 * 
	 * @param posd                  the ISA sales document
	 * @param ccardObject           holds information regarding credit card. This may
	 *        have been filled earlier through a simulate step
	 * @param newShipToAddress      a new shipTo adress on header level, relevant only
	 *        for B2C
	 * @param simulate              simulate flag ('X' for simulate)
	 * @param documentType          The type of the document to be created like standard
	 *        inquiry or standard order
	 * @param connection            connection to the backend
	 * @param serviceR3IPC          the service object that deals with IPC when running
	 *        ISA R/3
	 * @return the JCO function after the RFC has been fired to R/3
	 * @exception BackendException  exception from R/3
	 */
	protected JCO.Function callRfcSd_SalesDocument_Create(SalesDocumentData posd, 
														  Object ccardObject, 
														  AddressData newShipToAddress, 
														  String simulate, 
														  String documentType, 
														  JCoConnection connection, 
														  ServiceR3IPC serviceR3IPC)
												   throws BackendException {

		cat.logT(Severity.DEBUG, loc, 
				"callRfcSd_SalesDcoument_create in AuctionCreateStrategy3 Start");

		//check on ccardTable
		if ((ccardObject != null) && !(ccardObject instanceof JCO.Table)) {
			throw new BackendException("wrong type of ccard table object");
		}

		cat.logT(Severity.DEBUG, loc, "callRfcSd_SalesDocument_Create for document type: " + documentType);

		JCO.Table ccardTable = (JCO.Table)ccardObject;
		Locale locale = backendData.getLocale();
		String language = locale.getLanguage().toUpperCase();
		cat.logT(Severity.INFO, loc, "callRfcSd_SalesDocument_Create in " +
										"AuctionCreateStrategy3 locale is"
								+ locale.toString() + " " + locale.getLanguage());		
		String configKey = backendData.getConfigKey();
		String orderType = posd.getHeaderData().getDocumentType();
		if (orderType == null) {
			if(posd instanceof Quotation && backendData instanceof R3BackendAuctionData)
				orderType = ((R3BackendAuctionData)backendData).getQuotationType();
			else
				orderType = backendData.getOrderType();
		}
		cat.logT(Severity.DEBUG, loc, "the config key is " + configKey);
		cat.logT(Severity.DEBUG, loc, "The orderType is " + orderType);
		//get input parameters
		JCO.Function sdSalesDocCreate = connection.getJCoFunction(
												RFCConstants.RfcName.SD_SALESDOCUMENT_CREATE);
		JCO.ParameterList request = sdSalesDocCreate.getImportParameterList();

		//set simulation parameter
		request.setValue(simulate, 
						 "TESTRUN");
		cat.logT(Severity.DEBUG, loc, "simulate = " + simulate);
                         
		//set parameter that indicates that R/3 should 
		//assign the item numbers, important for multi level
		//configurables
		request.setValue(X,"INT_NUMBER_ASSIGNMENT");
                         

		//set order specific parameters on header level
		JCO.Structure headerParameters = request.getStructure(
												 "SALES_HEADER_IN");

		cat.logT(Severity.DEBUG, loc, "documentType = " + documentType);
												 
		if (documentType.equals(RFCConstants.TRANSACTION_TYPE_ORDER))
		{
			headerParameters.setValue(orderType, 
									  "DOC_TYPE");
			cat.logT(Severity.DEBUG, loc, "set DOC_TYPE for order :" + orderType);
		}

		if (documentType.equals(RFCConstants.TRANSACTION_TYPE_QUOTATION))
		{
			headerParameters.setValue(orderType, 
									  "DOC_TYPE");
			cat.logT(Severity.DEBUG, loc, "set DOC_TYPE for quotation :" + orderType);
		}
		String uiDateString = posd.getHeaderData().getReqDeliveryDate();

		if (uiDateString != null && (!uiDateString.trim().equals(
											  "")))
			headerParameters.setValue(Conversion.uIDateStringToDate(
											  uiDateString, 
											  locale), 
									  "REQ_DATE_H");
                                      
		headerParameters.setValue(backendData.getPurchOrType(),"PO_METHOD");
		cat.logT(Severity.DEBUG, loc, "backendData.getPurchOrType() = " + backendData.getPurchOrType());	                                      
		
		/// set the blocks

		//String billingBlock = props.getProperty(
		//	AuctionRFCConstantsR3.BILLING_BLOCK);
		if(posd instanceof AuctionOrderData){
			String billingBlock = ((AuctionOrderData)posd).getBillingBlockCode();
			if(billingBlock != null){			
				headerParameters.setValue(billingBlock,"BILL_BLOCK");	 

				cat.logT(Severity.DEBUG, loc, "billingBlock in created order document = " + 
					billingBlock);                                     
			}
		}		

		setDeliveryBlock(posd, headerParameters);	
		// set the reference to to the quotation ID
		//set generic header parameters
		String quoteId = null;
		if(posd instanceof AuctionOrderData){
			quoteId = ((AuctionOrderData)posd).getPredecessorDocumentId();
			setHeaderReference(quoteId,
								headerParameters);
		}
		if (quoteId == null){
			cat.logT(Severity.WARNING, loc, "The quotation Id can not be retrieved");
		}
		writeStrategy.setHeaderParameters(headerParameters, 
										  posd);

		//set item information
		JCO.Table itemTable = sdSalesDocCreate.getTableParameterList().getTable(
									  "SALES_ITEMS_IN");
		itemTable.clear();
		JCO.Table schedlinTable = sdSalesDocCreate.getTableParameterList().getTable(
										  "SALES_SCHEDULES_IN");
		schedlinTable.clear();

		
		writeStrategy.setItemInfo(itemTable, 
								  null, 
								  schedlinTable, 
								  null, 
								  posd, 
								  null, 
								  connection);
		cat.logT(Severity.DEBUG, loc, "The itemTable = " + itemTable);
		cat.logT(Severity.DEBUG, loc, "The schedlinTable = " + schedlinTable);
		PaymentBaseData payment = posd.getHeaderData().getPaymentData();

	
		boolean isPaymentDefined = payment != null && payment.getPaymentMethods() != null && payment.getPaymentMethods().size() > 0 ;

			if (isPaymentDefined)
				cat.logT(Severity.DEBUG, loc, "The defined payment is " + payment);
			else
				cat.logT(Severity.DEBUG, loc, "The payment is not defined");


		//set partner information on header and item level
		//provide cod key only if this has been defined in payment type
		//especially, this only holds for b2c
		JCO.Table partnerTable = sdSalesDocCreate.getTableParameterList().getTable(
										 "SALES_PARTNERS");
		partnerTable.clear();
		
		String codR3Key = null;
		boolean card = false;
		if (isPaymentDefined) {
			List paymentMethods = payment.getPaymentMethods();
			Iterator iter = paymentMethods.iterator();						        				
			boolean leave = false;
			while (leave == false) {
				PaymentMethodData payMethod = (PaymentMethodData) iter.next();
				if (payMethod instanceof COD) {										        	        	
					codR3Key = backendData.getCodCustomer();
					leave = true;				
				}
			}			
				
			while (card == false) {
				PaymentMethodData payMethod = (PaymentMethodData) iter.next();
				if (payMethod instanceof COD) {										        	        	
					codR3Key = backendData.getCodCustomer();
					card = true;
				}
			}
		}
		

		cat.logT(Severity.DEBUG, loc, "The codR3Key is " + codR3Key);

		writeStrategy.setPartnerInfo(partnerTable,
									 sdSalesDocCreate.getTableParameterList().getTable(
										 "PARTNERADDRESSES"),
									 posd, 
									 newShipToAddress, 
									 connection);


		cat.logT(Severity.DEBUG, loc, "partner infos have been set");
		//set payment information
		if (ccardTable != null && isPaymentDefined && card == true) {

			//set ccard information
			JCO.Table order_Ccard = sdSalesDocCreate.getTableParameterList().getTable(
											"SALES_CCARD");
			order_Ccard.clear();

			if (ccardTable.getNumRows() > 0) {
				ccardTable.firstRow();


				cat.logT(Severity.DEBUG, loc, "payment with creditcard " + ccardTable.getString(
																   "CC_TYPE") + " " + ccardTable.getString(
																							  "CC_NUMBER"));


				order_Ccard.copyFrom(ccardTable);
				cat.logT(Severity.DEBUG, loc, "The order_Ccard = " + order_Ccard);
			}
			 else {
				cat.logT(Severity.DEBUG, loc, "ccardTable empty");
			}
		}

		//set config information, it has been disabled for 
		// auction scenario
		//set the condition, aslo can 
		
		/*if (writeStrategy instanceof AuctionWriteStrategy){
			((AuctionWriteStrategy)writeStrategy).setHeaderConditions(
					sdSalesDocCreate.getTableParameterList().getTable(
													"SALES_CONDITIONS_IN"),
					posd
			);			
		}*/
		
		//now set texts into the RFC table
		JCO.Table textTable = sdSalesDocCreate.getTableParameterList().getTable("SALES_TEXT");
		textTable.clear();
		writeStrategy.setTexts(posd, textTable );

		//set extension table and transfer data from sales document
		//to extension table
		JCO.Table extensionTable=sdSalesDocCreate.getTableParameterList().getTable("EXTENSIONIN");
		extensionTable.clear();
		Extension extensionR3 = getExtension(extensionTable, 
											 posd, 
											 backendData, 
											 ExtensionParameters.EXTENSION_DOCUMENT_CREATE);
										 												 
		cat.logT(Severity.DEBUG, loc, "The extensionTable = " + extensionTable);

		setPriceCondition(posd, sdSalesDocCreate);
		//Debugging purpose
		//extensionR3.documentToTable();
//		connection.setAbapDebug(true);
//		java.util.Set set = new java.util.HashSet();
//		set.add("SD_SALESDOCUMENT_CREATE");
//					connection.setAbapDebugFunctionNames(set);

		//call customer exit
		performCustExitBeforeR3Call(posd, 
									sdSalesDocCreate);

		//fire RFC
		connection.execute(sdSalesDocCreate);

		//JCO.Function sdSalesDocChange = connection.getJCoFunction("SD_SALESDOCUMENT_CHANGE");
		
		//JCO.ParameterList changeRequest = sdSalesDocCreate.getImportParameterList();
		//changeRequest.setValue("", "SALESDOCUMENT");


		cat.logT(Severity.DEBUG, loc, 
				"callRfcSd_SalesDcoument_create in AuctionCreateStrategy3 End");

		return sdSalesDocCreate;
	}

	/**
	 * @param posd
	 * @param headerParameters
	 */
	private void setDeliveryBlock(SalesDocumentData posd, Structure headerParameters) {
		if(backendSettings == null || !backendSettings.isDeliveryBlock()) {
			cat.logT(Severity.DEBUG, loc, "No deliveray block!");
			return;
		}
		
		String deliveryBlock = 	null;
		if(posd instanceof AuctionOrderData){
			deliveryBlock = ((AuctionOrderData)posd).getDeliveryBlockCode();
			if (deliveryBlock == null){
				deliveryBlock = props.getProperty(
				AuctionRFCConstantsR3.DELIVERY_BLOCK);
			}
			if(deliveryBlock != null){			
				headerParameters.setValue(deliveryBlock,"DLV_BLOCK");	 

				cat.logT(Severity.DEBUG, loc, "deliveryBlock in created order document = " + 
								deliveryBlock);                                     
			}
			
		}else{
			cat.logT(Severity.WARNING, loc, "In the AuctionCreateStrategyR3 the deliver block is " +
				"not set, because of the document is not order type");	
		}		
	}

	/**
	 * Used to set the header document reference ID
	 * @param quotationId	   The quotation ID is used to link the
	 * 							sales document and the quotation date
	 * @param headerStructure  The RFC header structure that is input parameter for the
	 *        sales order create BAPI or the sales order simulate BAPI. Is of R/3 type
	 *        BAPISDHD1.
	 * @param document         The ISA sales document
	 */
	public void setHeaderReference(String quoteId,
								JCO.Structure headerStructure){
									
		headerStructure.setValue(RFCWrapperPreBase.trimZeros10(quoteId), 
															 "REF_DOC");
		headerStructure.setValue(RFCConstants.TRANSACTION_TYPE_QUOTATION,
								"REFDOC_CAT");								
	}
	/**
	 * To get some attribute from the properties
	 * @return
	 */
	public Properties getProps() {
		return props;
	}

	/**
	 * set the attributes
	 * @param properties
	 */
	public void setProps(Properties properties) {
		props = properties;
	}

	/**
	 * @param settings
	 */
	public void setBackendSettings(BackendDefaultSettings settings) {
		backendSettings = settings;
	}

	private boolean handleBackendUserException(JCO.Table retTable, JCoConnection connection) 
			throws BackendException{
		boolean hasError = false;
		BackendUserException buExc = new BackendUserException();
		if (retTable.getNumRows() > 0) {
			retTable.firstRow();		
			do {
				// if there
				String msg = retTable.getString("MESSAGE");
				if(retTable.getString("TYPE").equalsIgnoreCase("E")){
					hasError = true;
					cat.logT(Severity.ERROR, loc, 
							"error message type: " + retTable.getString("TYPE"));
					cat.logT(Severity.ERROR, loc, 
							"error message number: "
								+ retTable.getString("NUMBER"));
					cat.logT(Severity.ERROR, loc, 
							"error message is " + msg);
					buExc.addErrorMessage(msg);
					
				}else{

					cat.logT(Severity.DEBUG, loc, 
								"error message type: " + retTable.getString("TYPE"));
					cat.logT(Severity.DEBUG, loc, 
								"error message number: "
									+ retTable.getString("NUMBER"));
					cat.logT(Severity.DEBUG, loc, 
								"error message is " + msg);
					buExc.addWarningMessage(msg);
				}
			} while (retTable.nextRow());
			cat.logT(Severity.DEBUG, loc, "recTable = " + retTable);
		}
		if(hasError){
			throw buExc;		
		}
		else {
			commit(connection, X);
		}
		return hasError;
	}

}

