/*
 * Created on Feb 19, 2004
 *
 * To change the template for this generated file go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
package com.sap.isa.auction.backend.r3.salesdocument;

import com.sap.isa.auction.bean.BackendDefaultSettings;

/**
 * @author I803746
 *
 * To change the template for this generated type comment go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
public interface AuctionBackendBase {
	public void setBackendSettings(BackendDefaultSettings settings);
}
