/*
 * Title:        eBay Integration Project
 * Description:	 * SAP Labs LLC, the eBay auction project, also is known as Selling via eBay. 
 * Selling via eBay application provides SAP R/3 (and in the future CRM) 
 * customers to auction and sell inventory/products onto an online marketplace 
 * eBay. The application will provide the ability to Create, Schedule, Publish, 
 * Monitor and Close Auction Listings onto eBay and handle all the relevant 
 * Backend processing for Customers and Orders. 

 * Copyright:    Copyright (c) 2003
 * Company:      SAP Labs LLC
 * @author
 * @version 1.0
 */
package com.sap.isa.auction.backend.r3.salesdocument.strategy;

import com.sap.isa.backend.boi.isacore.SalesDocumentData;
import com.sap.isa.backend.r3.salesdocument.rfc.ChangeStrategy;
import com.sap.isa.backend.r3base.rfc.ReturnValue;
import com.sap.isa.core.eai.BackendException;
import com.sap.isa.core.eai.sp.jco.JCoConnection;

/**
 *
 * Only Interface
 */
public interface AuctionChangeStrategy extends ChangeStrategy {

	/**
	 * Change the status of the auction quotation, then the reservation
	 * or the customer requirements can be released
	 * @param salesDocument         the isa sales document
	 * @param simulate              'X': call the RFC in simulate-mode. When updating the
	 *        document, the function module is simulated; when saving, it is called in
	 *        non-simulate mode and a commit is performed
	 * @param connection            the JCO connection
	 * @return was the change successfull?
	 * @exception BackendException  exception from backend
	 **/
	public ReturnValue changeStatus(
		SalesDocumentData salesDocument,
		String simulate, 
		JCoConnection connection)
							throws BackendException;
	
	/**
	 * set the listing number to the sales document.
	 * This API is really simple, just change the P>O. Purchase number
	 * It calls SD_SALESDOCUMENT_CHANGE
	 * @param salesDocument         the isa sales document
	 * @param simulate              'X': call the RFC in simulate-mode. When updating the
	 *        document, the function module is simulated; when saving, it is called in
	 *        non-simulate mode and a commit is performed
	 * @param connection            the JCO connection
	 * @return was the change successfull?
	 * @exception BackendException  exception from backend
	 */
	public ReturnValue changeListingId(
		SalesDocumentData salesDocument,
		String simulate, 
								   JCoConnection connection)
							throws BackendException;
}
