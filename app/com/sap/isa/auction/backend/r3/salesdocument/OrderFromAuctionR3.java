/*****************************************************************************
	Copyright (c) 2001, SAPMarkets Palo Alto, USA, All rights reserved.

*****************************************************************************/


package com.sap.isa.auction.backend.r3.salesdocument;

import java.util.Properties;

import com.sap.isa.auction.backend.r3.AuctionRFCConstantsR3;
import com.sap.isa.auction.backend.r3.salesdocument.strategy.AuctionCreateStrategyR3;
import com.sap.isa.backend.boi.isacore.ShopData;
import com.sap.isa.backend.boi.isacore.order.CopyMode;
import com.sap.isa.backend.boi.isacore.order.OrderData;
import com.sap.isa.backend.r3.salesdocument.OrderR3;
import com.sap.isa.backend.r3.salesdocument.rfc.CCardStrategyR3;
import com.sap.isa.backend.r3.salesdocument.rfc.CCardStrategyR340b;
import com.sap.isa.backend.r3.salesdocument.rfc.CCardStrategyR3PI;
import com.sap.isa.backend.r3.salesdocument.rfc.ChangeStrategyERP;
import com.sap.isa.backend.r3.salesdocument.rfc.ChangeStrategyR3;
import com.sap.isa.backend.r3.salesdocument.rfc.CreateStrategyR340B;
import com.sap.isa.backend.r3.salesdocument.rfc.CreateStrategyR3PI;
import com.sap.isa.backend.r3.salesdocument.rfc.DetailStrategyR3;
import com.sap.isa.backend.r3.salesdocument.rfc.DetailStrategyR340b;
import com.sap.isa.backend.r3.salesdocument.rfc.DetailStrategyR3PI;
import com.sap.isa.backend.r3.salesdocument.rfc.DetailStrategyR3PI40;
import com.sap.isa.backend.r3.salesdocument.rfc.DetailStrategyR3PIB2CConsumer;
import com.sap.isa.backend.r3.salesdocument.rfc.ReadStrategyR3;
import com.sap.isa.backend.r3.salesdocument.rfc.StatusStrategyR3;
import com.sap.isa.backend.r3.salesdocument.rfc.StatusStrategyR3PI;
import com.sap.isa.backend.r3.salesdocument.rfc.WriteStrategyR3;
import com.sap.isa.backend.r3base.rfc.RFCConstants;
import com.sap.isa.backend.r3base.util.R3BackendData;
import com.sap.isa.core.TechKey;
import com.sap.isa.core.eai.BackendBusinessObjectParams;
import com.sap.isa.core.eai.BackendException;
import com.sap.isa.core.eai.sp.jco.JCoConnection;
import com.sap.isa.core.logging.IsaLocation;

/**
 * It used in B2C checkout scenario for order creation from
 * quotation
 **/
public class OrderFromAuctionR3 extends OrderR3 {
	private static IsaLocation log = IsaLocation.getInstance(
								OrderFromAuctionR3.class.getName());
	/**
	 * Initializes Business Object. This method might be overriden in  customer projects
	 * to register customer specific functionality, it tells the order objects which
	 * algorithms to use. Registers the strategies for reading,  writing, creating and
	 * displaying an order. The specific class of the  strategies depends on the R/3
	 * release and the plug in availability.
	 * 
	 * @param props                 a set of properties which may be useful to initialize
	 *        the object
	 * @param params                an object which wraps parameters
	 * @exception BackendException  exception from backend
	 */
	public void initBackendObject(Properties props, 
								  BackendBusinessObjectParams params)
						   throws BackendException {

		log.entering("init begin orderr3");
		R3BackendData backendData = getOrCreateBackendData();

		//for the read and write strategy, we always use
		//the standard implementation
		readStrategy = new ReadStrategyR3(backendData);
		writeStrategy = new WriteStrategyR3(backendData, 
											readStrategy);

		//now determine the strategies for the different tasks
		//that are required
		if (backendData.getR3Release().equals(RFCConstants.REL40B) && backendData.getBackend()
		   .equals(ShopData.BACKEND_R3_PI)) {
			detailStrategy = new DetailStrategyR3PI40(backendData, 
													  readStrategy);
		}
		 else if (backendData.getBackend().equals(ShopData.BACKEND_R3_PI)) {
			if (!backendData.isConsumerCreationEnabled())
				detailStrategy = new DetailStrategyR3PI(backendData, 
														readStrategy);
			else  detailStrategy = new DetailStrategyR3PIB2CConsumer(backendData, 
														readStrategy);               	                                    
		}
		 else if (backendData.getR3Release().equals(RFCConstants.REL40B)) {
			detailStrategy = new DetailStrategyR340b(backendData, 
													 readStrategy);
		}
		 else {
			detailStrategy = new DetailStrategyR3(backendData, 
												  readStrategy);
		}

		//setting the status strategy
		if (backendData.getBackend().equals(ShopData.BACKEND_R3_PI)) {
			statusStrategy = new StatusStrategyR3PI(backendData , readStrategy);
		}
		 else {
			statusStrategy = new StatusStrategyR3(backendData , readStrategy);
		}


		if (backendData.getBackend().equals(ShopData.BACKEND_R3_PI)) {
			log.debug("Strategy for backend with PI");
			createStrategy = new CreateStrategyR3PI(backendData, 
													writeStrategy, 
													readStrategy);
		}
		 else if (backendData.getR3Release().equals(RFCConstants.REL40B) || backendData.getBackend()
		   .equals(RFCConstants.REL45B)) {
			log.debug("Stragey for 40 und 45 without PI: MVKE_ARRAY_READ");
			createStrategy = new CreateStrategyR340B(backendData, 
													 writeStrategy, 
													 readStrategy);
		}
		 else {
			log.debug("Strategy for R3 >= 4.6 and without PI: BAPI_MATERIAL_GETLIST");
			createStrategy = new AuctionCreateStrategyR3(backendData, 
												  writeStrategy, 
												  readStrategy);
		}

		//changing documents
		if (backendData.getR3Release().equals(RFCConstants.REL700)) {
			changeStrategy = new ChangeStrategyERP(backendData, writeStrategy, readStrategy);
		} else {
			changeStrategy = new ChangeStrategyR3(backendData, writeStrategy, readStrategy);
		}
		

		//performing the credit card check
		if (backendData.getBackend().equals(ShopData.BACKEND_R3_PI)) {
			creditCardStrategy = new CCardStrategyR3PI(backendData, 
													   writeStrategy, 
													   readStrategy);
		}
		 else if (backendData.getR3Release().equals(RFCConstants.REL40B)) {
			creditCardStrategy = new CCardStrategyR340b(backendData, 
														readStrategy);
		}
		 else {
			creditCardStrategy = new CCardStrategyR3(backendData, 
													 readStrategy);
		}
	}
	
	/**
	 * Create an order with reference to a predecessor document.
	 * 
	 * @param predecessorKey        predecessor
	 * @param copyMode              kind of copying
	 * @param processType           process type
	 * @param soldToKey             R/3 key of soldTo
	 * @param shop                  the shop data
	 * @param order                 the order data
	 * @exception BackendException  exception from R/3
	 */
	public void createWithReference(TechKey predecessorKey, 
									CopyMode copyMode, 
									String processType, 
									TechKey soldToKey, 
									ShopData shop, 
									OrderData order)
							 throws BackendException {
		log.debug("createWithReference");
		JCoConnection aJCoCon = null;
		
		try {
			//get connection to backend
			aJCoCon = getDefaultJCoConnection();
			
			//copy the document
			//copy the document
			((AuctionCreateStrategyR3)createStrategy).copyDocument(
										predecessorKey.getIdAsString(), 
										order, 
										AuctionRFCConstantsR3.TRANSACTION_TYPE_ORDER,
										null, 
										aJCoCon);
		} finally {
			if(aJCoCon != null)
				aJCoCon.close();
		}
	}

	
}
