/*****************************************************************************
	Copyright (c) 2001, SAPMarkets Palo Alto, USA, All rights reserved.

*****************************************************************************/


package com.sap.isa.auction.backend.r3.salesdocument;

import java.util.Locale;
import java.util.Properties;

import com.sap.isa.auction.backend.r3.AuctionRFCConstantsR3;
import com.sap.isa.auction.backend.r3.salesdocument.strategy.AuctionChangeStrategyR3;
import com.sap.isa.auction.backend.r3.salesdocument.strategy.AuctionCreateStrategyR3;
import com.sap.isa.auction.backend.r3.salesdocument.strategy.AuctionStatusStrategyR3;
import com.sap.isa.auction.backend.r3.salesdocument.strategy.AuctionStrategyBase;
import com.sap.isa.auction.backend.r3.salesdocument.strategy.AuctionWriteStrategyR3;
import com.sap.isa.auction.bean.BackendDefaultSettings;
import com.sap.isa.auction.logging.CategoryProvider;
import com.sap.isa.backend.IsaBackendBusinessObjectBaseSAP;
import com.sap.isa.backend.boi.isacore.SalesDocumentData;
import com.sap.isa.backend.boi.isacore.ShopData;
import com.sap.isa.backend.boi.isacore.order.BasketBackend;
import com.sap.isa.backend.boi.isacore.order.BasketData;
import com.sap.isa.backend.boi.isacore.order.CopyMode;
import com.sap.isa.backend.boi.isacore.order.ItemData;
import com.sap.isa.backend.r3.salesdocument.SalesDocumentR3;
import com.sap.isa.backend.r3.salesdocument.rfc.DetailStrategyR3;
import com.sap.isa.backend.r3.salesdocument.rfc.ReadStrategyR3;
import com.sap.isa.backend.r3base.util.R3BackendData;
import com.sap.isa.core.TechKey;
import com.sap.isa.core.eai.BackendBusinessObjectParams;
import com.sap.isa.core.eai.BackendException;
import com.sap.isa.core.eai.sp.jco.JCoConnection;
import com.sap.tc.logging.Category;
import com.sap.tc.logging.Location;
import com.sap.tc.logging.Severity;

public class AuctionBasketR3 extends SalesDocumentR3 implements BasketBackend {
 
	private final static Category cat = CategoryProvider.getSystemCategory();
	private final static Location loc =
		Location.getLocation(AuctionBasketR3.class);

	/**
	 * Initializes Business Object.
	 * 
	 * @param props                 a set of properties which may be useful to initialize
	 *        the object
	 * @param params                a object which wraps parameters
	 * @exception BackendException  Description of Exception
	 */
	public void initBackendObject(Properties property, 
								  BackendBusinessObjectParams params)
						   throws BackendException {
		
		cat.logT(Severity.DEBUG, loc, "In the AuctionOrderR3 initlization of BackenObject");
		R3BackendData backendData = getOrCreateBackendData();
		//This block is for the spoof the NullPoint error, in case it happens
		if (backendData.getLocale() == null){
			Locale locale = Locale.US;
			backendData.setLocale(locale);
		}
		// those data should be passed from BackendSettingBase, in case
		// of this object was not initionlized right. Here is the sproof,
		// in order to avoid some exceptions.
		// It should be removed later
		if(backendData.getR3Release() == null)
			backendData.setR3Release("");
		if(backendData.getBackend() == null)
			backendData.setBackend("");
		if(backendData.getSubTotalFreight() == null)	
			backendData.setSubTotalFreight("4");
		if (getContext().getAttribute(
				IsaBackendBusinessObjectBaseSAP.ISA_LANGUAGE) == null){
			getContext().setAttribute(
				IsaBackendBusinessObjectBaseSAP.ISA_LANGUAGE,
				"EN");
		}		
		readStrategy = new ReadStrategyR3(backendData);
		writeStrategy = new AuctionWriteStrategyR3(backendData, readStrategy);
		createStrategy = new AuctionCreateStrategyR3(backendData, 
											  writeStrategy, 
											  readStrategy);
		detailStrategy = new DetailStrategyR3(backendData, 
											readStrategy);
		changeStrategy = new AuctionChangeStrategyR3(backendData,
													writeStrategy,
													readStrategy);
		statusStrategy = new AuctionStatusStrategyR3(backendData , readStrategy);                                            
	}
	public void setBackendSettings(BackendDefaultSettings settings)
	{
		setStrategyBackendSettings(writeStrategy, settings);
		setStrategyBackendSettings(createStrategy, settings);
		setStrategyBackendSettings(changeStrategy, settings);
	}
	
	private void setStrategyBackendSettings(Object strategy, BackendDefaultSettings settings)
	{
		if(strategy != null && strategy instanceof AuctionStrategyBase)
			((AuctionStrategyBase)strategy).setBackendSettings(settings);
	}
	
	/* (non-Javadoc)
	 * @see com.sap.isa.backend.boi.isacore.order.BasketBackend#createWithReferenceInBackend(com.sap.isa.core.TechKey, com.sap.isa.backend.boi.isacore.order.CopyMode, java.lang.String, com.sap.isa.core.TechKey, com.sap.isa.backend.boi.isacore.ShopData, com.sap.isa.backend.boi.isacore.order.BasketData)
	 */
	public void createWithReferenceInBackend(TechKey arg0, 
											CopyMode arg1, 
											String arg2, 
											TechKey arg3, 
											ShopData arg4, 
											BasketData arg5) 
											throws BackendException {
		// Create a sales document in R3 buffer
		// winner data 

		cat.logT(Severity.DEBUG, loc, "createWithReferenceInBackend");
		//get connection to backend
		JCoConnection aJCoCon = getDefaultJCoConnection();

		//copy the document
		String quotationId = arg0.getIdAsString();
		((AuctionCreateStrategyR3)createStrategy).copyDocument(quotationId, 
									arg5, 
									AuctionRFCConstantsR3.TRANSACTION_TYPE_ORDER,
									newShipToAddress, 
									aJCoCon);
		aJCoCon.close();
		cat.logT(Severity.DEBUG, loc, "createWithReferenceInBackend End");
		
		
	}

	/* (non-Javadoc)
	 * @see com.sap.isa.backend.boi.isacore.order.BasketBackend#addConfigItemInBackend(com.sap.isa.backend.boi.isacore.order.BasketData, com.sap.isa.backend.boi.isacore.order.ItemData)
	 */
	public void addConfigItemInBackend(BasketData arg0, ItemData arg1) throws BackendException {
		// Not implemented for R3 scenario
		
	}

	/* (non-Javadoc)
	 * @see com.sap.isa.backend.boi.isacore.SalesDocumentBackend#getGridStockIndicatorFromBackend(com.sap.isa.backend.boi.isacore.SalesDocumentData, com.sap.isa.core.TechKey)
	 */
	public String[][] getGridStockIndicatorFromBackend(SalesDocumentData arg0, TechKey arg1) throws BackendException {
		// Not implemented for AuctionBasket
		return null;
	}
	public void saveInBackend(BasketData arg0, boolean arg1) throws BackendException {
		
	}
	public void readHeaderFromBackend(BasketData arg0, boolean arg1) throws BackendException {
		
	}
	/**
	 * Dequeue document in the backend.
	 *
	 * @param document The document to dequeue
	 */
	public void dequeueInBackend(BasketData basket)
				throws BackendException {
	}
}
