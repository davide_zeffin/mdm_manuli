/*****************************************************************************
    Copyright (c) 2001, SAPMarkets Palo Alto, USA, All rights reserved.

*****************************************************************************/
package com.sap.isa.auction.backend.r3.salesdocument.strategy;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Date;
import java.util.Iterator;
import java.util.Properties;

import com.sap.mw.jco.JCO;
import com.sap.mw.jco.JCO.Table;
import com.sap.tc.logging.*;
import com.sap.isa.auction.backend.boi.order.AuctionItemData;
import com.sap.isa.auction.backend.boi.order.AuctionOrderData;
import com.sap.isa.auction.backend.boi.order.AuctionSalesDocumentData;
import com.sap.isa.auction.backend.r3.AuctionRFCConstantsR3;
import com.sap.isa.auction.bean.BackendDefaultSettings;
import com.sap.isa.auction.businessobject.order.TransactionDataConstants;
import com.sap.isa.auction.logging.CategoryProvider;
import com.sap.isa.backend.boi.isacore.AddressData;
import com.sap.isa.backend.boi.isacore.SalesDocumentData;
import com.sap.isa.backend.boi.isacore.order.ItemData;
import com.sap.isa.backend.boi.isacore.order.ItemListData;
import com.sap.isa.backend.r3.salesdocument.InternalSalesDocument;
import com.sap.isa.backend.r3.salesdocument.SalesDocumentR3;
import com.sap.isa.backend.r3.salesdocument.rfc.ReadStrategy;
import com.sap.isa.backend.r3.salesdocument.rfc.WriteStrategyR3;
import com.sap.isa.backend.r3base.rfc.RFCConstants;
import com.sap.isa.backend.r3base.rfc.RFCWrapperPreBase;
import com.sap.isa.backend.r3base.util.Conversion;
import com.sap.isa.backend.r3base.util.R3BackendData;
import com.sap.isa.businessobject.item.ItemSalesDoc;
import com.sap.isa.core.TechKey;
import com.sap.isa.core.eai.BackendException;
import com.sap.isa.core.eai.sp.jco.JCoConnection;

/**
 * This is the extension class for setting sales responsible employee and
 * shipping address for the order creation and quotation creation process.
 *  1. Set the material and plant information as well as storage information.
 *  2. Set the sales employee information
 *  3. Set the price information, which is not from IPC
 *  4. In the order creation case we will overwrite the shipping address,
 *      especially for one-time customer scenario
 **/

public class AuctionWriteStrategyR3
	extends WriteStrategyR3
	implements AuctionWriteStrategy, AuctionStrategyBase {
		/** The logging instance. */
		private final static Category cat = CategoryProvider.getSystemCategory();
		private final static Location loc =
			Location.getLocation(AuctionStatusStrategyR3.class);		
		private int LENGTH_MAN_SHIPTO = 4;
		private String MANUAL_R3_SHIPTO =
			"theR3KeyOfTheManualShipToWithLeadingZeros";
		private String ID_CAN_BE_REPLACED = "ID_CAN_BE_REPLACED";
		// in order to avoid exception
		private static final String CUSTOMER_PURCHASE_ORDER_NUMBER =
			"ISA AUCTION LIST ID";
		/**
		 * Key for the item extension that holds the previously entered quantity.
		 */
		protected final static String MATNR_ENTERED = "SYSTEM_MATNR_ENTERED";

		/**
		 * for the time being we get all the price condition from the BackendObject-config.xml
		 * file
		 **/
		private Properties props = null;
	
		private BackendDefaultSettings backendSettings = null;
	
		/**
		 * Constructor for the AuctionWriteStrategyR3 object.
		 * 
		 * @param backendData  the bean that holds the R/3 specific customizing
		 */
		public AuctionWriteStrategyR3(
			R3BackendData backendData,
			ReadStrategy readStrat) {
			super(backendData, readStrat);
			if (backendData.getRejectionCode() == null){
				cat.logT(Severity.WARNING, loc, "Can not get Rejection code from backend, using the default" +
					" rejection code");
				backendData.setRejectionCode("W2");
			}
		}

		/**
		 * Fills the RFC partner table with partner and address information before calling
		 * the simulate or create RFC's. Check if the <code> manualShipTo </code> is part of
		 * the  document, if it is the case, all manually entered ship-to's get the key of
		 * the <code> manualShipTo </code>.
		 * 
		 * @param partnerTable          the partner table
		 * @param salesDoc              the isa sales documents
		 * @param newShipToAddress      a new shipTo adress on header level
		 * @param connection            the connection to the backend
		 * @exception BackendException  exception from R/3.
		 */
		public void setPartnerInfo(
			JCO.Table partnerTable,
			JCO.Table addressTable,
			SalesDocumentData salesDoc,
			AddressData newShipToAddress,
			JCoConnection connection)
			throws BackendException {
			// we set the sale employee here
			cat.logT(Severity.DEBUG, loc, "In AuctionWriteStrategyR3 to set the partner info");
			if (salesDoc instanceof AuctionSalesDocumentData) {
				String sellerBP =
					((AuctionSalesDocumentData) salesDoc).getResponsibleEmployee();
//	
				String employeeR3Key = RFCWrapperPreBase.trimZeros10(sellerBP);
				cat.logT(Severity.DEBUG, loc, "The sales employee ID is " + sellerBP);
				partnerTable.appendRow();
				partnerTable.setValue(
					AuctionRFCConstantsR3.ROLE_SALESEMPLOYEE,
					RFCConstants.RfcField.PARTN_ROLE);
				partnerTable.setValue(
					employeeR3Key,
					RFCConstants.RfcField.PARTN_NUMB);
			}
			if (newShipToAddress == null||
					(newShipToAddress.getCity()== null) || 
					(newShipToAddress.getCity().length()==0) ||
					(newShipToAddress.getCountry() == null) ||
					(newShipToAddress.getCountry().length()== 0) ||
					(newShipToAddress.getPostlCod1() == null) ||
					(newShipToAddress.getPostlCod1().length() == 0)){
				super.setPartnerInfo(
					partnerTable,
					addressTable,
					salesDoc,
					newShipToAddress,
					connection);
				setSoldToAddress(
					partnerTable,
					addressTable,
					salesDoc,
					newShipToAddress,
					connection);
				}
			else{
				setPartnerInfoISA(partnerTable,
				addressTable,
				salesDoc,
				newShipToAddress,
				connection);
			}
			cat.logT(Severity.DEBUG, loc, "The partnerTable = " + partnerTable);
			cat.logT(Severity.DEBUG, loc, "The addressTable = " + addressTable);
		}

		private void setSoldToAddress( JCO.Table partnerTable,
					JCO.Table addressTable,
					SalesDocumentData salesDoc,
					AddressData address,
					JCoConnection connection)
		throws BackendException
		{
			if(addressTable!= null && addressTable.getNumRows()>0 )
			{
				boolean isTypeBAPIADDR1 = addressTable.hasField(
				"ADDR_NO");
				if (isTypeBAPIADDR1) {
					partnerTable.firstRow();
					do {
						if(RFCConstants.ROLE_SOLDTO.equals(partnerTable.getValue(
								RFCConstants.RfcField.PARTN_ROLE))){
							String shipToKey = salesDoc.getHeaderData().getShipToData().getTechKey().getIdAsString();
							if(shipToKey == null || shipToKey.length()<=0)
								shipToKey = salesDoc.getHeaderData().getPartnerListData()
												.getSoldToData().getPartnerTechKey().getIdAsString();
							shipToKey = RFCWrapperPreBase.trimZeros10(shipToKey);
							partnerTable.setValue(shipToKey, "ADDR_LINK");
//							addressTable.appendRow();
//							addressTable.setValue(shipToKey, "ADDR_NO");
//							setAddressData(addressTable, address, connection);
							
							break;
						}
					} while(partnerTable.nextRow());
				}
			}
			
		}
		/**
		 * Fill up the price RFC segment with the pricing come from the auction
		 * data, in the auction we have reserved price and starting price and 
		 * winning price, all those price should be set to the 
		 */

		/**
		 * Fills up the partner RFC segment with ISA address data, checks on manually entered
		 * ship-to' and replaces their ID's.
		 * 
		 * @param address                         ISA address data
		 * @param partnerTable                    RFC partner segment
		 * @param shipToKey                       current shipTo
		 * @param isManuallyEnteredAddress        has the adress been entered manually?
		 * @param r3MasterShipToKey               the r3 key of the shipTo
		 * @param isManuallyEnteredHeaderAddress  has the adress been entered manually in b2c
		 *        header?
		 * @param connection                      connection to the backend
		 * @exception BackendException            exception from backend
		 */
		protected void shipToDataToRFCSegment(
			String shipToKey,
			String r3MasterShipToKey,
			boolean isManuallyEnteredAddress,
			boolean isManuallyEnteredHeaderAddress,
			AddressData address,
			JCO.Table partnerTable,
			JCO.Table addressTable,
			JCoConnection connection)
			throws BackendException {

			cat.logT(Severity.DEBUG, loc, 
				"shipToDataToRFCSegment for: "
					+ shipToKey
					+ "/"
					+ r3MasterShipToKey);
			/*
			if ((address.getExtensionData(ID_CAN_BE_REPLACED) != null) &&
						((shipToKey.trim().length() <= LENGTH_MAN_SHIPTO) ||
												isManuallyEnteredHeaderAddress)) {
				r3MasterShipToKey = MANUAL_R3_SHIPTO;
			}*/
			//r3MasterShipToKey = MANUAL_R3_SHIPTO;
			//isManuallyEnteredHeaderAddress = true;
			if(shipToKey == null || shipToKey.length()<=0)
				shipToKey = r3MasterShipToKey;
			///if is for the one Time customer setting
			//// here the shipto is the one time customer
			if (address != null){
			
				boolean isOneTimeCustomer = true;
				if(backendSettings != null)
					isOneTimeCustomer = backendSettings.isOneTimeCustomerScenario();
				if (isOneTimeCustomer) 
				{
					oneTimeCustomershipToDataToRFCSegment(
						shipToKey,
						address,
						partnerTable,
						addressTable,
						connection);
				} 
				else {
					super.shipToDataToRFCSegment(
						shipToKey,
						r3MasterShipToKey,
						isManuallyEnteredAddress,
						isManuallyEnteredHeaderAddress,
						address,
						partnerTable,
						addressTable,
						connection);
				}
			}
			cat.logT(Severity.DEBUG, loc, 
				"shipToDataToRFCSegment End");
		}

		/**
		 * @param shipToKey
		 * @param address
		 * @param partnerTable
		 * @param addressTable
		 * @param connection
		 */
		protected void oneTimeCustomershipToDataToRFCSegment(
			String shipToKey,
			AddressData address,
			Table partnerTable,
			Table addressTable,
			JCoConnection connection)
			throws BackendException {
			cat.logT(Severity.DEBUG, loc, "oneTimeCustomershipToDataToRFCSegment " + shipToKey);

			//BAPIPARNRTable.Fields partnerFields = partnerTable.getFields();
			//append new record to partner table
			partnerTable.appendRow();

			//partnerFields.setPartn_Role(RFCConstants.ROLE_SHIPTO);
			partnerTable.setValue(
				RFCConstants.ROLE_SHIPTO,
				RFCConstants.RfcField.PARTN_ROLE);
			String trimShipToKey = RFCWrapperPreBase.trimZeros10(shipToKey);
			partnerTable.setValue(trimShipToKey, RFCConstants.RfcField.PARTN_NUMB);

			//this is only necessary in cases where the address table
			//is provided, not for credit card checks
			cat.logT(Severity.DEBUG, loc, "The address is " + address);
		
			if (addressTable != null) {
				//set the additional address fields of the
				//partner table
				try{
				if (!address.getCountry().trim().equals(""))   {
					boolean isTypeBAPIADDR1 = addressTable.hasField(
						"ADDR_NO");
					if (isTypeBAPIADDR1) {
						partnerTable.setValue(trimShipToKey,
						 "ADDR_LINK");
						addressTable.appendRow();
						addressTable.setValue(trimShipToKey,
						 "ADDR_NO");
	                                         
						setAddressData(addressTable, address, connection);
					} else
						setAddressToPartnerTable(partnerTable, address);
				}else{
					partnerTable.setValue("", "LANGU_ISO");
				}
				}catch(NullPointerException ex){
					cat.logT(Severity.WARNING, loc, "Shipping Address in RFC segament is not set");
				}
			}
			cat.logT(Severity.DEBUG, loc, "oneTimeCustomershipToDataToRFCSegment End");
		}

		/**
		 * Used for all document types to set parameters on header level before firing the
		 * simulate or create RFC. Can be overwritten by customers to do project specific
		 * extensions.
		 * 
		 * @param headerStructure  The RFC header structure that is input parameter for the
		 *        sales order create BAPI or the sales order simulate BAPI. Is of R/3 type
		 *        BAPISDHD1.
		 * @param document         The ISA sales document
		 */
		public void setHeaderParameters(
			JCO.Structure headerStructure,
			SalesDocumentData document) {
			try {
				cat.logT(Severity.INFO, loc, "Getting sales area information from Sales Document");
				boolean cheakSalesArea = checkSalesArea(document);
				if (cheakSalesArea){
					headerStructure.setValue(
						document.getHeaderData().getSalesOrg(),
						"SALES_ORG");
					headerStructure.setValue(
						document.getHeaderData().getDisChannel(),
						"DISTR_CHAN");
					headerStructure.setValue(
						document.getHeaderData().getDivision(),
						"DIVISION");
					headerStructure.setValue(
						document.getHeaderData().getCurrency(),
						"CURRENCY");
					cat.logT(Severity.DEBUG, loc, "Added by FY: salesArea=" + 
							document.getHeaderData().getSalesOrg());
					cat.logT(Severity.DEBUG, loc, "Added by FY: distrChannel=" + 
							document.getHeaderData().getDisChannel());				
					cat.logT(Severity.DEBUG, loc, "Added by FY: division=" + 
							document.getHeaderData().getDivision());
					cat.logT(Severity.DEBUG, loc, "Added by FY: currency=" + 
							document.getHeaderData().getCurrency());								
				}else{
					cat.logT(Severity.WARNING, loc, "Can not get the sales area from sales document," +
						" Use the sales area in Backend object");
					throw new NullPointerException ("Sales area does not exist" +
						" in sales documentation"); 
				}
			} catch (NullPointerException ex) {
				cat.logT(Severity.INFO, loc, "Getting sales area information from Sales Document");						
				headerStructure.setValue(
					backendData.getSalesArea().getSalesOrg(),
					"SALES_ORG");
				headerStructure.setValue(
					backendData.getSalesArea().getDistrChan(),
					"DISTR_CHAN");
				headerStructure.setValue(
					backendData.getSalesArea().getDivision(),
					"DIVISION");
				headerStructure.setValue(backendData.getCurrency(), "CURRENCY");
				cat.logT(Severity.DEBUG, loc, "Added by FY: salesArea=" + backendData.getSalesArea().getSalesOrg());
				cat.logT(Severity.DEBUG, loc, "Added by FY: distrChannel=" + backendData.getSalesArea().getDistrChan());				
				cat.logT(Severity.DEBUG, loc, "Added by FY: division=" + backendData.getSalesArea().getDivision());
				cat.logT(Severity.DEBUG, loc, "Added by FY: currency=" + backendData.getCurrency());
			}
			//set external purchase number - only dummy in simulation mode
			Date today = new Date(System.currentTimeMillis());
			// get the purchase order 
			if (document.getHeaderData().getPurchaseOrderExt() == null) {
				document.getHeaderData().setPurchaseOrderExt(
					CUSTOMER_PURCHASE_ORDER_NUMBER);
			}
			if (document.getHeaderData().getPurchaseOrderExt().length() > 0) {
				headerStructure.setValue(
					document.getHeaderData().getPurchaseOrderExt(),
					"PURCH_NO_C");
				cat.logT(Severity.DEBUG, loc, "Added by FY: getPurchaseOrderExt=" + document.getHeaderData().getPurchaseOrderExt());
			} else {
				headerStructure.setValue(
					"ISA-AUCTION"
						+ Conversion.dateToUIDateString(today, backendData.getLocale()),
					"PURCH_NO_C");
				
				cat.logT(Severity.INFO, loc, "Tha Auction ID is  set as the default one");
			}
	
			//set purchase date
			headerStructure.setValue(today, "PURCH_DATE");
			cat.logT(Severity.DEBUG, loc, "The purchase date is set" + today);
			String validTo = document.getHeaderData().getValidTo();
			if (validTo == null)
				validTo = TransactionDataConstants.QUOTATION_VALID_TO;
			headerStructure.setValue(
					Conversion.iSADateStringToDate(validTo),
									 "QT_VALID_T");  						 
			cat.logT(Severity.DEBUG, loc, "In the AuctionWriteStrategyR3 " +
				" The Valid To date is " + 
				Conversion.iSADateStringToDate(validTo));
			//set shipping conditions
			headerStructure.setValue(
				document.getHeaderData().getShipCond(),
				"SHIP_COND");
			cat.logT(Severity.DEBUG, loc, "getShipCond=" + document.getHeaderData().getShipCond());
	
			cat.logT(Severity.DEBUG, loc, 
				"shipping conditions in setHeaderParameters: "
					+ document.getHeaderData().getShipCond());
	
			//set the delivery date on header level
			String uiDateString = document.getHeaderData().getReqDeliveryDate();
	
			if (uiDateString != null && (!uiDateString.trim().equals("")))
				headerStructure.setValue(
					Conversion.uIDateStringToDate(
						uiDateString,
						backendData.getLocale()),
					"REQ_DATE_H");
	
			cat.logT(Severity.DEBUG, loc, 
				"delivery date for header: "
					+ document.getHeaderData().getReqDeliveryDate());
		}
	
		/**
		 * @param document
		 * @return		 boolean  if the sales document contains right 
		 * 				sales area
		 */
		private boolean checkSalesArea(SalesDocumentData document) {
			if ((document.getHeaderData().getSalesOrg() == null) ||
				(document.getHeaderData().getSalesOrg().trim().
											equalsIgnoreCase(""))){
					cat.logT(Severity.DEBUG, loc, "In the sales doc, the sales org is null");
					return false;
			}
			if ((document.getHeaderData().getDisChannel() == null) ||
				(document.getHeaderData().getDisChannel().trim().
											equalsIgnoreCase(""))){
					cat.logT(Severity.DEBUG, loc, "In the sales doc, the distribution channel is null");							
					return false;
			}
			if ((document.getHeaderData().getDivision() == null) ||
				(document.getHeaderData().getDivision().trim().
											equalsIgnoreCase(""))){
					cat.logT(Severity.DEBUG, loc, "In the sales doc, the division is null");
					return false;
			}
			return true;
		}

		/**
		 * Set the header conditions, this method uses manual type HM00 to set the
		 * Net value price of a sales order at header level. Basically it set the
		 * reserved price to a quoet document and the winner price to a sales documentation 
		 * @param conditionTable		the JCO table holds the condition type 
		 * 								and condition value
		 * @param document				the sales document that holds all of the 
		 * 								attributes, including the price from ebay
		 *  
		 */
		public void setHeaderConditions(
			JCO.Table conditionTable,
			SalesDocumentData document) {
			conditionTable.appendRow();
			conditionTable.setValue(
				AuctionRFCConstantsR3.DEFAULT_PRICE_CONDITION,
				"COND_TYPE");
			String price = document.getHeaderData().getNetValue();
			try {
				double val = Double.parseDouble(price);
			} catch (Exception ex) {
				cat.logT(Severity.ERROR, loc, "The passing price is not correct " + price);
				price = "0";
			}
			conditionTable.setValue(price, "COND_VALUE");
	
			//conditionTable.setValue(
			/// heer we also can manually input tax information and
			/// Shipping information 
	
		}
	
		/**
		 * Set the tax via manually entry, it is used in eBay based
		 * checkout process, the manual tax profile and the 
		 * shipping fee profiel have to be defined.
		 */
		public void setHeaderTaxConditions(
			JCO.Table conditionTable,
			SalesDocumentData document) {
			conditionTable.appendRow();
			//		conditionTable.setValue(
			//				AuctionRFCConstantsR3.DEFAULT_PRICE_CONDITION, 
			//				"COND_TYPE");
			String price = document.getHeaderData().getTaxValue();
			try {
				double val = Double.parseDouble(price);
			} catch (Exception ex) {
				cat.logT(Severity.ERROR, loc, "The passing price is not correct " + price);
				price = "0";
			}
			conditionTable.setValue(price, "COND_VALUE");
		}
	
		/**
		 * Set the shipping fee, it is used in the eBay integration 
		 * checkout process, the shipping profile has to be configured
		 * first 
		 **/
		public void setHeaderShippingConditions(
			JCO.Table conditionTable,
			SalesDocumentData document) {
			conditionTable.appendRow();
			/*
			conditionTable.setValue(
					AuctionRFCConstantsR3.DEFAULT_PRICE_CONDITION, 
					"COND_TYPE");
			*/
			String price = document.getHeaderData().getFreightValue();
			try {
				double val = Double.parseDouble(price);
			} catch (Exception ex) {
				cat.logT(Severity.ERROR, loc, "The passing price is not correct " + price);
				price = "0";
			}
			conditionTable.setValue(price, "COND_VALUE");
	
		}
	
		/**
		 * Item information for order & quotation. Used for creating, simulation, changing
		 * sales documents. The JCO item and schedlin tables (and for document change the
		 * corresponding X-tables) are filled according to the ISA item data.
		 * this extends from its suoper class
		 * 
		 * @param itemTable             JCO item table
		 * @param schedlinTable         JCO schedlin table
		 * @param salesDocument         the ISA sales document, contains all the sales
		 *        document data
		 * @param itemXTable            JCO.Table that holds the information which item
		 *        fields are to be changed. Might be null e.g. when called in the create
		 *        context
		 * @param schedlinXTable        JCO.Table that holds the information which schedule
		 *        line fields are to be changed. Might be null e.g. when called in the
		 *        create context
		 * @param salesDocumentR3       the ISA R/3 sales document. Contains the status of
		 *        the document in R/3 and the status of the document as it was in the
		 *        previous update step. This parameter might be null if the method is called
		 *        for new documents that do not have a representation in R/3.
		 * @param connection            JCO connection
		 * @exception BackendException  exception from backend
		 */
		public void setItemInfo(
			JCO.Table itemTable,
			JCO.Table itemXTable,
			JCO.Table schedlinTable,
			JCO.Table schedlinXTable,
			SalesDocumentData salesDocument,
			SalesDocumentR3 salesDocumentR3,
			JCoConnection connection)
			throws BackendException {

			cat.logT(Severity.DEBUG, loc, "In the setItemInfo of the AuctionWriteStrategyR3 Start");

			InternalSalesDocument r3Document = null;
			InternalSalesDocument isaR3Document = null;
	
			if (salesDocumentR3 != null) {
				r3Document = salesDocumentR3.getDocumentR3Status();
				isaR3Document = salesDocumentR3.getDocumentFromInternalStorage();
				r3Document.resetItemDirtyMap();
			}
			// loop over item list
			int itemSize = salesDocument.getItemListData().size();
			cat.logT(Severity.DEBUG, loc, "now setting items for bapi, items: " + itemSize);
	
			for (int i = 0; i < salesDocument.getItemListData().size(); i++) {
	
				ItemData posdLine = salesDocument.getItemListData().getItemData(i);
				cat.logT(Severity.DEBUG, loc, "ItemData posdline is " + posdLine);
				ItemData existingItem = null;
	
				if (isaR3Document != null) {
					existingItem =
						isaR3Document.getItem(posdLine.getTechKey().getIdAsString());
				}
	
				ItemData r3Item = null;
	
				if (r3Document != null) {
					r3Document.markItemAsDirty(posdLine.getTechKey());
					r3Item = r3Document.getItem(posdLine.getTechKey().getIdAsString());
	
				}
	
				//check the quantity. For some reasons, the quantity is empty for existing items,
				//so fill it from the internal storage in this case
				if (posdLine.getQuantity().trim().equals("")) {
	
					if (existingItem != null) {
						posdLine.setQuantity(existingItem.getQuantity());
	
						cat.logT(Severity.DEBUG, loc, 
							"reset quantity to: " + existingItem.getQuantity());
					}
				}
	
				//check if item has a parent item. If so, item comes from configuration
				//in this case, the item will not be processed here because R/3 finds the
				//sub items
				TechKey parent = posdLine.getParentId();
	
				if (parent == null || parent.isInitial()) {
	
					cat.logT(Severity.DEBUG, loc, 
						"no parent item for item with techkey: "
							+ posdLine.getTechKey());
	
					//if the delta indicator tables are filled: set the fields
					//that must be changed
					if (itemXTable != null && schedlinXTable != null) {
	
						cat.logT(Severity.DEBUG, loc, "X-tables are provided");
	
						//create new record and set values for item delta table
						itemXTable.appendRow();
						itemXTable.setValue(
							posdLine.getTechKey().getIdAsString(),
							RFCConstants.RfcField.ITM_NUMBER);
	
						if (r3Item != null) {
	
							cat.logT(Severity.DEBUG, loc, "the item exists in R/3, so update the item");
	
							itemXTable.setValue(U, RFCConstants.RfcField.UPDATEFLAG);
						} else {
	
							cat.logT(Severity.DEBUG, loc, 
								"the item does not exist in R/3, so insert the item");
	
							itemXTable.setValue(I, RFCConstants.RfcField.UPDATEFLAG);
						}
	
						itemXTable.setValue(X, RFCConstants.RfcField.PO_ITM_NO);
						itemXTable.setValue(X, RFCConstants.RfcField.MATERIAL);
						itemXTable.setValue(X, RFCConstants.RfcField.HG_LV_ITEM);
						itemXTable.setValue(X, "REASON_REJ");
	
						//create new record and set values for schedule line delta table
						//decide on how the RFC update flag is set, can be 'U' for update
						//or 'I' for insertion
						schedlinXTable.appendRow();
						schedlinXTable.setValue(
							posdLine.getTechKey().getIdAsString(),
							RFCConstants.RfcField.ITM_NUMBER);
						schedlinXTable.setValue(
							RFCConstants.SCHEDULE_INITIAL_KEY,
							RFCConstants.RfcField.SCHED_LINE);
	
						if (r3Item != null) {
	
							cat.logT(Severity.DEBUG, loc, 
								"the item exists in R/3, so update the schedule line");
	
							schedlinXTable.setValue(
								U,
								RFCConstants.RfcField.UPDATEFLAG);
						} else {
	
							cat.logT(Severity.DEBUG, loc, 
								"the item does not exist in R/3, so insert the schedule line");
	
							schedlinXTable.setValue(
								I,
								RFCConstants.RfcField.UPDATEFLAG);
						}
	
						schedlinXTable.setValue(X, RFCConstants.RfcField.REQ_QTY);
						schedlinXTable.setValue(X, RFCConstants.RfcField.REQ_DATE);
					}
	
					cat.logT(Severity.DEBUG, loc, "ItemData posdline TechKey is " + posdLine.getTechKey());
					cat.logT(Severity.DEBUG, loc, "RejectReason is " + backendData.getRejectionCode());
					itemTable.appendRow();
					itemTable.setValue(
						posdLine.getTechKey().getIdAsString(),
						RFCConstants.RfcField.ITM_NUMBER);
					
					itemTable.setValue(
						posdLine.getTechKey().getIdAsString(),
						RFCConstants.RfcField.PO_ITM_NO);
	
					ItemSalesDoc itemSalesDoc = (ItemSalesDoc) posdLine;
	
					if (itemSalesDoc.getStatus() != null
						&& itemSalesDoc.getStatus().equals(
							ItemSalesDoc.DOCUMENT_COMPLETION_STATUS_CANCELLED)) {
	
						cat.logT(Severity.DEBUG, loc, "item has been canceled previously");
	
						itemTable.setValue(
							backendData.getRejectionCode(),
							"REASON_REJ");
					}
	
					//now set the unit information
					setItemUnit(posdLine, itemTable, itemXTable, connection);
	
					// here set the reference document ID to be the one from auction 
					if (salesDocument instanceof AuctionOrderData) {
						// set the plant information
						String plant = null;
						String storage = null;
						try {
							plant = ((AuctionItemData) itemSalesDoc).getPlant();
							storage =
								((AuctionItemData) itemSalesDoc).getStorageLocation();
						} catch (Exception ex) {
							cat.logT(Severity.ERROR, loc, "Can not get Plant location from ");
						
						}
						if (plant != null) {
							itemTable.setValue(plant, "PLANT");
						}
						if (storage != null) {
							itemTable.setValue(storage, "STORE_LOC");
						}
						String docR3 =
							((AuctionOrderData) salesDocument)
								.getPredecessorDocumentId();
						String itemR3 = null;
						itemTable.setValue(
							RFCWrapperPreBase.trimZeros10(docR3),
							"REF_DOC");
						//itemTable.setValue(itemR3, 
						//				   "REF_DOC_IT");
	
						cat.logT(Severity.DEBUG, loc, "contract reference: " + docR3 + "/" + itemR3);
					}
	
					setItemProductInfo(
						existingItem,
						posdLine,
						itemTable,
						salesDocumentR3 , connection);
	
					if ((posdLine.getParentId() != null)
						&& (!posdLine.getParentId().isInitial())) {
						itemTable.setValue(
							posdLine.getParentId().getIdAsString(),
							RFCConstants.RfcField.HG_LV_ITEM);
					}
	
					//if schedule line table is provided: store schedule line data there
					//if not, store it in item table
					if (schedlinTable != null) {
	
						//delete old additional schedule lines
						if (r3Item != null) {
	
							checkAdditionalScheduleLines(
								r3Item,
								schedlinTable,
								schedlinXTable);
						}
	
						schedlinTable.appendRow();
						schedlinTable.setValue(
							posdLine.getTechKey().getIdAsString(),
							RFCConstants.RfcField.ITM_NUMBER);
	
						//field REQ_QTY is of type DEC in R/3, so it must be passed in US locale
						//that's what uIQuantityStringToISAQuantityString delivers
						//11/11/2002: do it the standard way! The field is of type QUANTITY
						schedlinTable.setValue(
							new BigDecimal(
								posdLine.getQuantity()),
							RFCConstants.RfcField.REQ_QTY);
	
						//if the delivery date on item level is not set: take it from header
						if (posdLine.getReqDeliveryDate() != null
							&& (!posdLine.getReqDeliveryDate().trim().equals(""))) {
							schedlinTable.setValue(
								Conversion.uIDateStringToDate(
									posdLine.getReqDeliveryDate(),
									backendData.getLocale()),
								RFCConstants.RfcField.REQ_DATE);
	
							cat.logT(Severity.DEBUG, loc, "delivery date provided on item level");
						} else {
	
							String uiDateString =
								salesDocument.getHeaderData().getReqDeliveryDate();
	
							if (uiDateString != null
								&& (!uiDateString.trim().equals("")))
								schedlinTable.setValue(
									Conversion.uIDateStringToDate(
										uiDateString,
										backendData.getLocale()),
									RFCConstants.RfcField.REQ_DATE);
	
							cat.logT(Severity.DEBUG, loc, "delivery date not provided on item level");
						}
	
						schedlinTable.setValue(
							RFCConstants.SCHEDULE_INITIAL_KEY,
							RFCConstants.RfcField.SCHED_LINE);
					} else {
	
						cat.logT(Severity.DEBUG, loc, 
							"no schedule line table, setting quantity to "
								+ Conversion.uIQuantityStringToBigInteger(
									posdLine.getQuantity(),
									backendData.getLocale()));
	
						//REQ_QTY is of type NUM here
						//this is for BAPI_SALESORDER_SIMULATE, so this code
						//will normally not be entered
						itemTable.setValue(
							new BigInteger(
								posdLine.getQuantity()),
							RFCConstants.RfcField.REQ_QTY);
	
						String uiDateString = posdLine.getReqDeliveryDate();
	
						if (uiDateString != null && (!uiDateString.trim().equals("")))
							itemTable.setValue(
								Conversion.uIDateStringToDate(
									uiDateString,
									backendData.getLocale()),
								RFCConstants.RfcField.REQ_DATE);
					}
	
					cat.logT(Severity.DEBUG, loc, "");
					cat.logT(Severity.DEBUG, loc, "----------------------------------");
					cat.logT(Severity.DEBUG, loc, 
						"item created with number: "
							+ posdLine.getTechKey().getIdAsString());
					cat.logT(Severity.DEBUG, loc, "product " + posdLine.getProduct());
					cat.logT(Severity.DEBUG, loc, "productId " + posdLine.getProductId());
					cat.logT(Severity.DEBUG, loc, "req quantity: " + posdLine.getQuantity());
					cat.logT(Severity.DEBUG, loc, "----------------------------------");
					cat.logT(Severity.DEBUG, loc, "");
				} else {
					cat.logT(Severity.DEBUG, loc, 
						posdLine.getTechKey()
							+ " has parent item, will not be sent to R/3");
				}
			}
	
			//now delete remaining items (means: rejection in R/3)
			if (r3Document != null) {
	
				Iterator remainingItems =
					r3Document.getCleanItems().keySet().iterator();
	
				while (remainingItems.hasNext() && itemXTable != null) {
	
					String itemNo = ((TechKey) remainingItems.next()).getIdAsString();
	
					cat.logT(Severity.DEBUG, loc, "delete item (i.e. set rejection code): " + itemNo);
	
					itemXTable.appendRow();
					itemXTable.setValue(itemNo, RFCConstants.RfcField.ITM_NUMBER);
					itemXTable.setValue(X, RFCConstants.RfcField.PO_ITM_NO);
					itemXTable.setValue(U, RFCConstants.RfcField.UPDATEFLAG);
					itemXTable.setValue(X, "REASON_REJ");
					itemTable.appendRow();
					itemTable.setValue(itemNo, RFCConstants.RfcField.ITM_NUMBER);
					itemTable.setValue(itemNo, RFCConstants.RfcField.PO_ITM_NO);
	
					//remove hardcoding later
					itemTable.setValue(backendData.getRejectionCode(), "REASON_REJ");
				}
			}
			cat.logT(Severity.DEBUG, loc, "setting items finished");
		}
	
		/**
		 * Item information for order & quotation. Used for creating, simulation, changing
		 * sales documents. The JCO item and schedlin tables (and for document change the
		 * corresponding X-tables) are filled according to the ISA item data.
		 * this extends from its suoper class
		 * 
		 * @param itemTable             JCO item table
		 * @param salesDocument         the ISA sales document, contains all the sales
		 *        document data
		 * @param itemXTable            JCO.Table that holds the information which item
		 *        fields are to be changed. Might be null e.g. when called in the create
		 *        context
		 * @param salesDocumentR3       the ISA R/3 sales document. Contains the status of
		 *        the document in R/3 and the status of the document as it was in the
		 *        previous update step. This parameter might be null if the method is called
		 *        for new documents that do not have a representation in R/3.
		 * @param connection            JCO connection
		 * @exception BackendException  exception from backend
		 */
		public void setItemStatus(
			JCO.Table itemTable,
			JCO.Table itemXTable,
			SalesDocumentData salesDocument)
			throws BackendException {
			cat.logT(Severity.DEBUG, loc, "setItemStatus Start");
			ItemListData items = salesDocument.getItemListData();
			if (items != null) {
				cat.logT(Severity.DEBUG, loc, 
					"In the Auction Write strategy the number of Item is"
						+ items.size());
				for (int i = 0; i < items.size(); i++) {
					ItemData item = items.getItemData(i);
					String itemNo = item.getNumberInt();
					if (itemNo != null) {
	
						cat.logT(Severity.DEBUG, loc, 
							"item number(i.e. set rejection code): " + itemNo);
						itemXTable.appendRow();
						itemXTable.setValue(itemNo, RFCConstants.RfcField.ITM_NUMBER);
						itemXTable.setValue(X, RFCConstants.RfcField.PO_ITM_NO);
						itemXTable.setValue(U, RFCConstants.RfcField.UPDATEFLAG);
						itemXTable.setValue(X, "REASON_REJ");
						itemTable.appendRow();
						itemTable.setValue(itemNo, RFCConstants.RfcField.ITM_NUMBER);
						itemTable.setValue(itemNo, RFCConstants.RfcField.PO_ITM_NO);
	
						itemTable.setValue(
							backendData.getRejectionCode(),
							"REASON_REJ");
						cat.logT(Severity.DEBUG, loc, "the ietm number is " + itemNo);
						cat.logT(Severity.DEBUG, loc, "the ietm code is " + backendData.getRejectionCode());
					}
				}
			}
		}
	
		/**
		 *  Used to change the price information in a sales document,
		 * in auction scenario, in most case, most of the price conditions
		 * are set manually
		 * @param conditionsIn	 		JCO condition table
		 * @param conditionsInX			JCO.Table that holds the information which condition
		 *        fields are to be changed. 
		 * @param posd
		 */
		public void setPriceChangeInfo(
			JCO.Table conditionsIn,
			JCO.Table conditionsInx,
			SalesDocumentData posd)
			throws BackendException {

			cat.logT(Severity.DEBUG, loc, "Start update price");
			String totalPriceCondition = null;
			String taxPriceCondition = null;
			String freightCondition = null;
			String insuranceCondition = null;
			String otherCondition = null;
			String taxValue = null;
			String freightValue = null;
			String insuranceValue = null;
			String otherValue = null;
			if (posd instanceof AuctionSalesDocumentData){
				totalPriceCondition = ((AuctionSalesDocumentData)posd).getTotalManualPriceCondition();
			}
			if (totalPriceCondition == null)
				totalPriceCondition = 
					props.getProperty(AuctionRFCConstantsR3.TOTAL_PRICE_CONDITION);
	
			cat.logT(Severity.DEBUG, loc, "The totalPriceCondition is" + totalPriceCondition);
			if (posd instanceof AuctionOrderData){
				taxPriceCondition = ((AuctionOrderData)posd).getTaxManualPriceCondition();
				freightCondition = ((AuctionOrderData)posd).
										getFreightManualPriceCondition();
				insuranceCondition = ((AuctionOrderData)posd).
										getInsurManualPriceCondition();
				otherCondition = ((AuctionOrderData)posd).
										getOtherPriceCondition();
			}
			// for local testing purpose
			if (taxPriceCondition == null)
				taxPriceCondition = props.getProperty(
						AuctionRFCConstantsR3.TOTAL_TAX_PRICE_CONDIFTION);
			if (freightCondition == null)			
				freightCondition = props.getProperty(
						AuctionRFCConstantsR3.TOTAL_SHIPPING_PRICE_CONDITION);
			if (insuranceCondition == null)
				insuranceCondition = props.getProperty(
						AuctionRFCConstantsR3.TOTAL_INSURANCE_PRICE_CONDITION);		
			if (otherCondition == null)
				otherCondition = props.getProperty(
						AuctionRFCConstantsR3.OTHERL_DISCOUNT_CHARGE_PRICE_CONDIFTION);		
			// make sure to have something here
			if(totalPriceCondition == null){
				totalPriceCondition = 
						AuctionRFCConstantsR3.DEFAULT_PRICE_CONDITION;
			}
			cat.logT(Severity.DEBUG, loc, "--------Price condition Part--------------------");
			cat.logT(Severity.DEBUG, loc, "The tax price condition is" + taxPriceCondition);
			cat.logT(Severity.DEBUG, loc, "the freight price condition is " + freightCondition);
			cat.logT(Severity.DEBUG, loc, "The tax price condition is " + insuranceCondition);
			cat.logT(Severity.DEBUG, loc, "--------Price condition Part--------------------");
		
			String price = posd.getHeaderData().getGrossValue();

			String currency = posd.getHeaderData().getCurrency();
			conditionsIn.clear();
			conditionsInx.clear();
//			if (price != null){
//				setPriceTables(
//					conditionsIn,
//					conditionsInx,
//					currency,
//					totalPriceCondition,
//					price);
//			}else{
//				cat.logT(Severity.DEBUG, loc, "The total value is null, not updated");
//			}
			cat.logT(Severity.DEBUG, loc, "The total value updated do not need to happen");
			taxValue = posd.getHeaderData().getTaxValue();
			if ((taxValue != null) && (taxPriceCondition != null)) {
				setPriceTables(
					conditionsIn,
					conditionsInx,
					currency,
					taxPriceCondition,
					taxValue);
			} else {
				cat.logT(Severity.DEBUG, loc, "The tax value is null, not updated");
			}
			freightValue = posd.getHeaderData().getFreightValue();
			if ((freightValue != null)&& (freightCondition != null)) {
				setPriceTables(
					conditionsIn,
					conditionsInx,
					currency,
					freightCondition,
					freightValue);
			} else {
				cat.logT(Severity.DEBUG, loc, "The freight value is null, not updated");
			}
			otherValue = ((AuctionOrderData)posd).getOtherChargeDiscount();
			if ((otherValue != null)&& (otherCondition != null)) {
				setPriceTables(
					conditionsIn,
					conditionsInx,
					currency,
					otherCondition,
					otherValue);
			} else {
				cat.logT(Severity.DEBUG, loc, "The freight value is null, not updated");
			}
			try {
				insuranceValue = ((AuctionOrderData) posd).getInsuranceValue();
				if ((insuranceValue != null) && (insuranceCondition != null)) {
					setPriceTables(
						conditionsIn,
						conditionsInx,
						currency,
						insuranceCondition,
						insuranceValue);
				} else {
					cat.logT(Severity.DEBUG, loc, "The insurance value is null, not updated");
				}
			} catch (Exception ex) {
				cat.log(Severity.WARNING, loc, "Insurance price is not updated due to:", ex);
			}
			cat.logT(Severity.DEBUG, loc, "End update price");
		}
	
		/**
		 * @param conditionsIn
		 * @param conditionsInx
		 * @param posd
		 * @param totalPriceCondition
		 */
		private void setPriceTables(
			Table conditionsIn,
			Table conditionsInx,
			String currency,
			String priceCondition,
			String price)
			throws BackendException {
			try {
				double val = Double.parseDouble(price);
			} catch (Exception ex) {
				cat.logT(Severity.ERROR, loc, 
					"The passing price is not correct "
						+ price
						+ " "
						+ " And the price is et to zero");
				price = "0";
			}
			conditionsIn.appendRow();
			//set to the first item
			conditionsIn.setValue("000010", "ITM_NUMBER");                     
			conditionsIn.setValue(priceCondition, "COND_TYPE");
			conditionsIn.setValue(currency, "CURRENCY");
			conditionsIn.setValue("1", "COND_P_UNT");
			conditionsIn.setValue(price, "COND_VALUE");

			cat.logT(Severity.DEBUG, loc, "conditiontable=" + conditionsIn);

			conditionsInx.appendRow();
			conditionsInx.setValue("000010", "ITM_NUMBER");  
			conditionsInx.setValue(
				priceCondition,
				"COND_TYPE");
			conditionsInx.setValue("X", "COND_VALUE");
			conditionsInx.setValue("X", "UPDATEFLAG");
			conditionsInx.setValue("X", "CURRENCY");
			conditionsInx.setValue("X", "COND_P_UNT");
		}

		/**
		 * To get some attribute from the properties
		 * @return
		 */
		public Properties getProps() {
			return props;
		}

		/**
		 * set the attributes
		 * @param properties
		 */
		public void setProps(Properties properties) {
			props = properties;
		}

		/**
		 * @param settings
		 */
		public void setBackendSettings(BackendDefaultSettings settings) {
			backendSettings = settings;
		}


		/**
		 * Fills the RFC partner table with partner and address information before calling
		 * the simulate or create RFC's. Only used for partner creation, not for partner
		 * change. See <code>ChangeStrategy</code> for changing the sales document partner
		 * information.
		 * <br>
		 * See also <a href="{@docRoot}/isacorer3/salesdocument/shiptohandling.html"> Ship-to handling in ISA R/3 </a>.     
		 * 
		 * @param partnerTable          the partner table
		 * @param addressTable             the table that holds the address data
		 * @param salesDoc              the isa sales documents
		 * @param newShipToAddress      a new shipTo adress on header level
		 * @param connection            the connection to the backend
		 * @exception BackendException  exception from R/3
		 */
		private void setPartnerInfoISA(
			JCO.Table partnerTable,
			JCO.Table addressTable,
			SalesDocumentData salesDoc,
			AddressData newShipToAddress,
			JCoConnection connection)
			throws BackendException {

			cat.logT(
				Severity.DEBUG,
				loc,
				"setPartnerInfo start in setPartnerInfoISA");

			String soldToR3Key =
				RFCWrapperPreBase.trimZeros10(
					salesDoc
						.getHeaderData()
						.getPartnerListData()
						.getSoldToData()
						.getPartnerTechKey()
						.getIdAsString());
			String shipToR3Key =
				RFCWrapperPreBase.trimZeros10(
					salesDoc
						.getHeaderData()
						.getShipToData()
						.getTechKey()
						.getIdAsString());
			String codR3Key = backendData.getCodCustomer();
			if ((shipToR3Key == null) || (shipToR3Key.length() <= 1)) {
				shipToR3Key = soldToR3Key;
			}
			//retrieve the R/3 contact key
			String contactKey = backendData.getContactLoggedIn();

			//retrieve the R/3 reseller key (only if a reseller if available)

			StringBuffer debugOutput = new StringBuffer("\n");
			debugOutput.append("\npartner data to be passed to R/3:");
			debugOutput.append(
				"\nsales document : " + salesDoc.getTechKey().getIdAsString());
			debugOutput.append("\nsoldtokey is : " + soldToR3Key);
			debugOutput.append("\ncontact is : " + contactKey);
			debugOutput.append("\nshiptokey is : " + shipToR3Key);
			debugOutput.append(
				"\nshipto on header changed: "
					+ salesDoc.getHeaderData().getShipToData().hasManualAddress());
			debugOutput.append("\ncod key is : " + codR3Key);
			cat.logT(Severity.DEBUG, loc, debugOutput.toString());

			boolean isTypeBAPIADDR1 = addressTable.hasField(
											  "ADDR_NO");

			//first: set partner soldTo (default) on header level
			partnerTable.appendRow();
			partnerTable.setValue(
				RFCConstants.ROLE_SOLDTO,
				RFCConstants.RfcField.PARTN_ROLE);
			partnerTable.setValue(
				RFCWrapperPreBase.trimZeros10(soldToR3Key),
				RFCConstants.RfcField.PARTN_NUMB);
			if (isTypeBAPIADDR1) {
				partnerTable.setValue(shipToR3Key, 
									  "ADDR_LINK");
				addressTable.appendRow();
				addressTable.setValue(shipToR3Key, 
									  "ADDR_NO");
				                                          
				setAddressData(addressTable, 
								newShipToAddress, 
							   connection);
			}
			else
				setAddressToPartnerTable(partnerTable, newShipToAddress);

			//set contact info on header level
			if (contactKey != null && (!contactKey.equals(""))) {
				partnerTable.appendRow();
				partnerTable.setValue(
					RFCConstants.ROLE_CONTACT,
					RFCConstants.RfcField.PARTN_ROLE);
				partnerTable.setValue(contactKey, RFCConstants.RfcField.PARTN_NUMB);
			}

			//set billto
			partnerTable.appendRow();
			partnerTable.setValue(
				RFCConstants.ROLE_BILLPARTY,
				RFCConstants.RfcField.PARTN_ROLE);
			partnerTable.setValue(
				RFCWrapperPreBase.trimZeros10(soldToR3Key),
				RFCConstants.RfcField.PARTN_NUMB);
			setAddressToPartnerTable(partnerTable, newShipToAddress);

			//set payer
			partnerTable.appendRow();
			partnerTable.setValue(
				RFCConstants.ROLE_PAYER,
				RFCConstants.RfcField.PARTN_ROLE);
			partnerTable.setValue(
				RFCWrapperPreBase.trimZeros10(soldToR3Key),
				RFCConstants.RfcField.PARTN_NUMB);
			setAddressToPartnerTable(partnerTable, newShipToAddress);

			//set shipTo
			partnerTable.appendRow();
			partnerTable.setValue(
				RFCConstants.ROLE_SHIPTO,
				RFCConstants.RfcField.PARTN_ROLE);
			partnerTable.setValue(
				RFCWrapperPreBase.trimZeros10(shipToR3Key),
				RFCConstants.RfcField.PARTN_NUMB);
			if (isTypeBAPIADDR1) {
				partnerTable.setValue(shipToR3Key, 
									  "ADDR_LINK");
				addressTable.appendRow();
				addressTable.setValue(shipToR3Key, 
									  "ADDR_NO");
			                                          
				setAddressData(addressTable, 
								newShipToAddress, 
							   connection);
			}
			else
				setAddressToPartnerTable(partnerTable, newShipToAddress);

			//set ship to partner on item level if this has been provided
			cat.logT(
				Severity.DEBUG,
				loc,
				"Beginning of set partner information at item level: ");
			ItemListData items = salesDoc.getItemListData();
			for (int i = 0; i < items.size(); i++) {

				ItemData item = items.getItemData(i);
				String itemNum = item.getTechKey().getIdAsString();
				cat.logT(Severity.DEBUG, loc, "item number is " + itemNum);
				//check if item is relevant for R/3
				//if (isItemToBeSent(item)){ // we do not need to check for auction scenario

				//only set the shipTo into the order if the item shipTo exists and is
				//not the same as the header shipTo
				if (item.getShipToData() != null
					&& item.getShipToData().getShortAddress() != null
					&& (!item
						.getShipToData()
						.equals(salesDoc.getHeaderData().getShipToData()))) {

					String shipToKey =
						item.getShipToData().getTechKey().getIdAsString();

					cat.logT(
						Severity.DEBUG,
						loc,
						"short adress: " + item.getShipToData().getShortAddress());
					cat.logT(
						Severity.DEBUG,
						loc,
						"setting ship to for item: "
							+ itemNum
							+ " , is: "
							+ shipToKey);

					shipToDataToRFCSegment(
						shipToKey,
						soldToR3Key,
						item.getShipToData().hasManualAddress(),
						false,
						item.getShipToData().getAddressData(),
						partnerTable,
						addressTable,
						connection);
					partnerTable.setValue(
						itemNum,
						RFCConstants.RfcField.ITM_NUMBER);
				}else {
					//set shipTo
					cat.logT(
						Severity.DEBUG,
						loc,
						"Beginning of set shipTo partner at item level");
					partnerTable.appendRow();
					partnerTable.setValue(
						RFCConstants.ROLE_SHIPTO,
						RFCConstants.RfcField.PARTN_ROLE);
					partnerTable.setValue(
						RFCWrapperPreBase.trimZeros10(shipToR3Key),
						RFCConstants.RfcField.PARTN_NUMB);
					partnerTable.setValue(
							itemNum,
							RFCConstants.RfcField.ITM_NUMBER);	
					setAddressToPartnerTable(partnerTable, newShipToAddress);
					cat.logT(
						Severity.DEBUG,
						loc,
						"End of set shipTo partner at item level");
				}
				//first: set partner soldTo (default) on header level
				cat.logT(
					Severity.DEBUG,
					loc,
					"Beginning of set soldTo partner at item level: ");
				partnerTable.appendRow();
				partnerTable.setValue(
					RFCConstants.ROLE_SOLDTO,
					RFCConstants.RfcField.PARTN_ROLE);
				partnerTable.setValue(
					RFCWrapperPreBase.trimZeros10(soldToR3Key),
					RFCConstants.RfcField.PARTN_NUMB);
				partnerTable.setValue(
					itemNum,
					RFCConstants.RfcField.ITM_NUMBER);
				setAddressToPartnerTable(partnerTable, newShipToAddress);
		
				//		//set billto		
				partnerTable.appendRow();
				partnerTable.setValue(
					RFCConstants.ROLE_BILLPARTY,
					RFCConstants.RfcField.PARTN_ROLE);
				partnerTable.setValue(
					RFCWrapperPreBase.trimZeros10(soldToR3Key),
					RFCConstants.RfcField.PARTN_NUMB);
				partnerTable.setValue(
					itemNum,
					RFCConstants.RfcField.ITM_NUMBER);	
				setAddressToPartnerTable(partnerTable, newShipToAddress);

				//set payer
				partnerTable.appendRow();
				partnerTable.setValue(
					RFCConstants.ROLE_PAYER,
					RFCConstants.RfcField.PARTN_ROLE);
				partnerTable.setValue(
					RFCWrapperPreBase.trimZeros10(soldToR3Key),
					RFCConstants.RfcField.PARTN_NUMB);
				partnerTable.setValue(
					itemNum,
					RFCConstants.RfcField.ITM_NUMBER);
				setAddressToPartnerTable(partnerTable, newShipToAddress);	
				cat.logT(
					Severity.DEBUG,
					loc,
					"End of set payer partner at item level: ");		
			}
			cat.logT(
				Severity.DEBUG,
					loc,
					"setPartnerInfo end in setPartnerInfoISA");

		}


		private void setAddressToPartnerTable(JCO.Table partnerTable, 
		   AddressData address){
			//this is only necessary in cases where the address table
			//is provided, not for credit card checks
			cat.logT(Severity.DEBUG, loc, "The address is " + address);
		
			//set the additional address fields of the
				//partner table
				try{
				if (!address.getCountry().trim().equals(""))  { 
					partnerTable.setValue(address.getTitle(), "TITLE");
					partnerTable.setValue(address.getName1(), "NAME");
					partnerTable.setValue(address.getCity(), "CITY");
					partnerTable.setValue(address.getStreet(), "STREET");
					partnerTable.setValue(address.getCountry(), "COUNTRY");
					partnerTable.setValue(address.getPostlCod1(), "POSTL_CODE");
					partnerTable.setValue(address.getRegion(), "REGION");
					partnerTable.setValue(address.getName2(), "NAME_2");
					partnerTable.setValue(address.getName3(), "NAME_3");
					partnerTable.setValue(address.getTel1Numbr(), "TELEPHONE");
					partnerTable.setValue(address.getTel1Ext(), "TELEPHONE2");
					partnerTable.setValue(address.getFaxNumber(), "FAX_NUMBER");
					partnerTable.setValue(address.getTaxJurCode(), "TAXJURCODE");
					partnerTable.setValue(backendData.getLanguage(), "LANGU_ISO");
					//partnerTable.setValue(backendData.getLanguage(), "LANGU");
				}else{
					partnerTable.setValue("", "LANGU_ISO");
				}
				}catch(NullPointerException ex){
					cat.logT(Severity.ERROR, loc, 
						"setAddressToPartnerTable in RFC segament is not set" );
				}catch(Exception ex){
					cat.logT(Severity.ERROR, loc, 
						"setAddressToPartnerTable in RFC segament is not set");
				}		
		}
	
		/**
		 * Transfers the ISA address data into the RFC address table. This method deals with
		 * RFC structures BAPIADDR1 (for creation and change).
		 * 
		 * @param addressTable          the RFC address table
		 * @param address               the ISA address
		 * @param connection            the connection to the backend
		 * @exception BackendException  exception from R/3
		 */
		public void setAddressData(
			JCO.Table addressTable,
			AddressData address,
			JCoConnection connection)
			throws BackendException {
			super.setAddressData(addressTable, address, connection);
			String email = address.getEMail();
			if(email != null)
				addressTable.setValue(email, "E_MAIL");
			String sapLang = null;
			cat.logT(
				Severity.DEBUG,
				loc,
				"setAddressData, language from BackendData is"
					+ backendData.getLanguage());
			if (backendData.getLanguage() != null){
				addressTable.setValue(backendData.getLanguage(), "LANGU_ISO");
				sapLang = Conversion.getR3LanguageCode(
						backendData.getLanguage().toLowerCase());
				cat.logT(
					Severity.DEBUG,
					loc,
					"setAddressData, SAP language is"
						+ sapLang);
				addressTable.setValue(sapLang, "LANGU");
			}
		}
}
