/*
 * Title:        eBay Integration Project
 * Description:	 * SAP Labs LLC, the eBay auction project, also is known as Selling via eBay. 
 * Selling via eBay application provides SAP R/3 (and in the future CRM) 
 * customers to auction and sell inventory/products onto an online marketplace 
 * eBay. The application will provide the ability to Create, Schedule, Publish, 
 * Monitor and Close Auction Listings onto eBay and handle all the relevant 
 * Backend processing for Customers and Orders. 

 * Copyright:    Copyright (c) 2003
 * Company:      SAP Labs LLC
 * @author
 * @version 1.0
 */
package com.sap.isa.auction.backend.r3.salesdocument.strategy;

import com.sap.isa.backend.boi.isacore.AddressData;
import com.sap.isa.backend.boi.isacore.SalesDocumentData;
import com.sap.isa.backend.r3.salesdocument.rfc.CreateStrategy;
import com.sap.isa.backend.r3base.rfc.ReturnValue;
import com.sap.isa.core.eai.BackendException;
import com.sap.isa.core.eai.sp.jco.JCoConnection;

/**
 * @author i009657
 *
 * To change the template for this generated type comment go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
public interface AuctionCreateStrategy extends CreateStrategy {
	/**
	 * Copies the document into a new one, uses the R/3 copy mechanism and creates a
	 * document flow. Uses function module SD_SALESDOCUMENT_CREATE to link to the 
	 * quotation document, but it has to be ensured that the sales order data
	 * has been filled correctly. This implementation is very different
	 * with ISA implementation
	 * 
	 * @param posd             ISA source document
	 * @param targetDoc                ISA target document. Will not be filled entirely
	 *        but gets its new techkey
	 * @param connection            JCO connection
	 * @param targetDocumentType        the R/3 document type like standard order. Comes
	 *        from a shop attribute
	 * @param newShipToAddress		the new ship address passed from eBay
	 * @return R/3 messages
	 * @exception BackendException exception from backend
	 */
	public ReturnValue copyDocument(String quoteId, 
									SalesDocumentData targetDoc, 
									String targetDocumentType, 
									AddressData newShipToAddress,
									JCoConnection connection)
							 throws BackendException;
}
