/*
 * Title:        eBay Integration Project
 * Description:	 * SAP Labs LLC, the eBay auction project, also is known as Selling via eBay. 
 * Selling via eBay application provides SAP R/3 (and in the future CRM) 
 * customers to auction and sell inventory/products onto an online marketplace 
 * eBay. The application will provide the ability to Create, Schedule, Publish, 
 * Monitor and Close Auction Listings onto eBay and handle all the relevant 
 * Backend processing for Customers and Orders. 

 * Copyright:    Copyright (c) 2003
 * Company:      SAP Labs LLC
 * @author
 * @version 1.0
 */
package com.sap.isa.auction.backend.r3.salesdocument.strategy;

import com.sap.mw.jco.JCO;
import com.sap.isa.backend.boi.isacore.SalesDocumentData;
import com.sap.isa.backend.r3.salesdocument.rfc.WriteStrategy;
import com.sap.isa.core.eai.BackendException;
/**
 * Extends form the ISA R3 write stragegy for the condition purpose
 */
public interface AuctionWriteStrategy extends WriteStrategy {
	
	/**
	 * Set the header conditions, this method uses manual type HM00 to set the
	 * Net value price of a sales order at header level. Basically it set the
	 * reserved price to a quoet document and the winner price to a sales documentation 
	 * @param conditionTable		the JCO table holds the condition type 
	 * 								and condition value
	 * @param document				the sales document that holds all of the 
	 * 								attributes, including the price from ebay
	 *  
	 */
	public void setHeaderConditions(JCO.Table conditionTable, 
									SalesDocumentData document);

	/**
	 * Item information for order & quotation. Used for creating, simulation, changing
	 * sales documents. The JCO item and schedlin tables (and for document change the
	 * corresponding X-tables) are filled according to the ISA item data.
	 * this extends from its suoper class
	 * 
	 * @param itemTable             JCO item table
	 * @param salesDocument         the ISA sales document, contains all the sales
	 *        document data
	 * @param itemXTable            JCO.Table that holds the information which item
	 *        fields are to be changed. Might be null e.g. when called in the create
	 *        context
	 * @param salesDocumentR3       the ISA R/3 sales document. Contains the status of
	 *        the document in R/3 and the status of the document as it was in the
	 *        previous update step. This parameter might be null if the method is called
	 *        for new documents that do not have a representation in R/3.
	 * @param connection            JCO connection
	 * @exception BackendException  exception from backend
	 */
	public void setItemStatus(
		JCO.Table itemTable,
		JCO.Table itemXTable,
		SalesDocumentData salesDocument)
		throws BackendException ;

	/**
	 *  Used to change the price information in a sales document,
	 * in auction scenario, in most case, most of the price conditions
	 * are set manually
	 * @param condictionTable	 		JCO condition table
	 * @param conditionXTable			JCO.Table that holds the information which condition
	 *        fields are to be changed. 
	 * @param salesDocument
	 */
	public void setPriceChangeInfo(		
								JCO.Table condictionTable,
								JCO.Table conditionXTable,
								SalesDocumentData salesDocument)
								throws BackendException;
}

