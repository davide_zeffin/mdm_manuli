/*
 * Title:        eBay Integration Project
 * Description:	 * SAP Labs LLC, the eBay auction project, also is known as Selling via eBay. 
 * Selling via eBay application provides SAP R/3 (and in the future CRM) 
 * customers to auction and sell inventory/products onto an online marketplace 
 * eBay. The application will provide the ability to Create, Schedule, Publish, 
 * Monitor and Close Auction Listings onto eBay and handle all the relevant 
 * Backend processing for Customers and Orders. 

 * Copyright:    Copyright (c) 2003
 * Company:      SAP Labs LLC
 * @author
 * @version 1.0
 */
package com.sap.isa.auction.backend.r3.salesdocument.strategy;

import java.util.ArrayList;

import com.sap.mw.jco.JCO;
import com.sap.tc.logging.Category;
import com.sap.tc.logging.Location;
import com.sap.tc.logging.Severity;
import com.sap.isa.auction.backend.boi.order.AuctionOrderStatusData;
import com.sap.isa.auction.bean.BackendDefaultSettings;
import com.sap.isa.auction.backend.boi.BackendSettingsData;
import com.sap.isa.auction.businessobject.order.AuctionOrderStatus;
import com.sap.isa.auction.logging.CategoryProvider;
import com.sap.isa.backend.boi.isacore.order.HeaderData;
import com.sap.isa.backend.boi.isacore.order.OrderStatusData;
import com.sap.isa.backend.r3.salesdocument.rfc.ReadStrategy;
import com.sap.isa.backend.r3.salesdocument.rfc.StatusStrategy;
import com.sap.isa.backend.r3.salesdocument.rfc.StatusStrategyR3;
import com.sap.isa.backend.r3base.rfc.RFCConstants;
import com.sap.isa.backend.r3base.util.Conversion;
import com.sap.isa.backend.r3base.util.R3BackendData;
import com.sap.isa.core.TechKey;
import com.sap.isa.core.eai.BackendException;
import com.sap.isa.core.eai.sp.jco.JCoConnection;
/**
 * @author i009657
 *
 * To change the template for this generated type comment go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
public class AuctionStatusStrategyR3
	extends StatusStrategyR3
	implements StatusStrategy, AuctionStrategyBase {
//		private static IsaLocation log =
//			IsaLocation.getInstance(AuctionStatusStrategyR3.class.getName());
		private final static Category cat = CategoryProvider.getSystemCategory();
		private final static Location loc =
			Location.getLocation(AuctionStatusStrategyR3.class);		
		private BackendSettingsData backendSettings = null;
	
		/**
		 * @param arg0
		 */
		public AuctionStatusStrategyR3(R3BackendData arg0 , ReadStrategy readStrategy) {
			super(arg0 , readStrategy);
			
		}

		/**
		 * Returns the R/3 function module that we use for searching.
		 * 
		 * @param connection connection to R/3
		 * @return the RFC <code> BAPI_SALESORDER_GETLIST </code>
		 * @throws BackendException Exception from backend
		 */
		protected JCO.Function getSearchRFC(JCoConnection connection)
			throws BackendException {

			return connection.getJCoFunction(
				RFCConstants.RfcName.BAPISDORDER_GETDETAILEDLIST);
		}

		/**
		 * Reads sales document header from R/3 when no plugIn 2003.1 is installed
		 * 
		 * @param orderList             the order list to be filled
		 * @param connection            connection to the backend
		 * @exception BackendException  exception from R/3
		 */
		public void getSDHeaders(
			JCoConnection connection,
			OrderStatusData orderList)
			throws BackendException {

			cat.logT(Severity.DEBUG, loc, "getSDHeaders for: " + orderList.getTechKey());

			// let us operate on the order list here

			//fill up the import parameters for getting the document list from R/3
			JCO.Function getList = getSearchRFC(connection);
			JCO.ParameterList importParams = getList.getImportParameterList();
			JCO.ParameterList tableParams = getList.getTableParameterList();
			//clear sort array
			//sortedRows = new ArrayList();
			//no reading from buffer! The document might have been
			//changed in R/3, and we have a stateful connection
			//for order and quotation
			importParams.setValue("A", "I_MEMORY_READ");

			//get the jayco structure for setting the read flags
			JCO.Structure iBapiView = importParams.getStructure("I_BAPI_VIEW");
			iBapiView.setValue(X, "HEADER");

			if (!(orderList instanceof AuctionOrderStatus)) {
				cat.logT(Severity.ERROR, loc, 
					"The passing object is not AuctionOrderStatus, NOT Supported");
				throw new BackendException("The Object is not for auction type, Not supported");
			}
			AuctionOrderStatusData document = (AuctionOrderStatusData) orderList;
			JCO.Table documents = tableParams.getTable("SALES_DOCUMENTS");
			// get the ordelist
			ArrayList orderIds = document.getFilteredOrderHeaders();
			//while(orderIds.hasNext()) {
			for (int i = 0; i< orderIds.size(); i++){
				String orderHeader = (String) orderIds.get(i);
				//SALES_KEYTable documents = importParams.getParams().getSales_Documents();
				documents.appendRow();

				//      documents.getFields().setVbeln(orderHeader.getSalesDocNumber());
				documents.setValue(
					orderHeader,
					"VBELN");

			}

			//fire RFC
			cat.logT(Severity.DEBUG, loc, "fillHeaderFromList, fire RFC for:" + orderIds);
			connection.execute(getList);

			// now fill in the order headers
			JCO.Table headerTable =
				getList.getTableParameterList().getTable("ORDER_HEADERS_OUT");

			setResultsIntoBusinessObject(headerTable, orderList);
			cat.logT(Severity.DEBUG, loc, "getSDHeaders for: " + orderList.getTechKey() + " End");
		}

		/**
		 * Transfers the search results into the order status business object.
		 * 
		 * @param retrievedSalesDocs all sales documents that have been found
		 * @param orderList the business object that gets the search results
		 */
		protected void setResultsIntoBusinessObject(
			JCO.Table retrievedSalesDocs,
			OrderStatusData orderList) {
			cat.logT(Severity.DEBUG, loc, "setResultsIntoBusinessObject Start");
			//the list also contains the items, so append one record to document list
			//if the document number changes in the result table
			//loop over sorted list
			for (int i = 0; i < retrievedSalesDocs.getNumRows(); i++) {

				retrievedSalesDocs.setRow(i);

				HeaderData orderHeader = orderList.createHeader();
				String purchNo = retrievedSalesDocs.getString("PURCH_NO");
				String salesDocNum = retrievedSalesDocs.getString("DOC_NUMBER");
				if (salesDocNum == null
					|| salesDocNum.trim().equals("")) {
					orderHeader.setSalesDocNumber(
						Conversion.cutOffZeros(
							salesDocNum));
				}
	
				String date =
					Conversion.dateToUIDateString(
						retrievedSalesDocs.getDate("DOC_DATE"),
						backendData.getLocale());

				cat.logT(Severity.DEBUG, loc, "document date: " + date);
		
				orderHeader.setDescription("");
				orderHeader.setSalesDocNumber(Conversion.cutOffZeros(salesDocNum));
				orderHeader.setStatusCompleted();

				String transactionGroup = retrievedSalesDocs.getString("TRAN_GROUP");

				if (transactionGroup
					.equals(RFCConstants.TRANSACTION_GROUP_ORDER)) {
					orderHeader.setDocumentTypeOrder();
					cat.logT(Severity.DEBUG, loc, "order found");
				}

				if (transactionGroup
					.equals(RFCConstants.TRANSACTION_GROUP_QUOTATION)) {
					orderHeader.setDocumentTypeQuotation();
					orderHeader.setQuotationExtended();

					//orderHeader.setStatusOpen();
					cat.logT(Severity.DEBUG, loc, "quotation found");
				}

				orderHeader.setPurchaseOrderExt(purchNo);
				orderHeader.setChangedAt(date);
				orderHeader.setValidTo(
					Conversion.dateToUIDateString(
						retrievedSalesDocs.getDate("QT_VALID_T"),
						backendData.getLocale()));
				orderHeader.setSalesDocumentsOrigin("");
				orderHeader.setTechKey(new TechKey(salesDocNum));
				orderHeader.setProcessType("");
				orderList.addOrderHeader(orderHeader);
			}

			orderList.getFilter().setTypeORDER();
			cat.logT(Severity.DEBUG, loc, "setResultsIntoBusinessObject End");
		}

		/**
		 * Return the sort field for the document list.
		 * 
		 * @return the sort field that is <code> SD_DOC </code>
		 */
		protected String getSortField() {
			return "DOC_NUMBER";
		}

		/**
		 * @param settings
		 */
		public void setBackendSettings(BackendDefaultSettings settings) {
			backendSettings = settings;
		}

}
