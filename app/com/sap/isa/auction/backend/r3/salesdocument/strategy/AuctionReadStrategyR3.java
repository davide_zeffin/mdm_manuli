/*
 * Title:        eBay Integration Project
 * Description:	 * SAP Labs LLC, the eBay auction project, also is known as Selling via eBay. 
 * Selling via eBay application provides SAP R/3 (and in the future CRM) 
 * customers to auction and sell inventory/products onto an online marketplace 
 * eBay. The application will provide the ability to Create, Schedule, Publish, 
 * Monitor and Close Auction Listings onto eBay and handle all the relevant 
 * Backend processing for Customers and Orders. 

 * Copyright:    Copyright (c) 2003
 * Company:      SAP Labs LLC
 * @author
 * @version 1.0
 */
package com.sap.isa.auction.backend.r3.salesdocument.strategy;

import java.math.BigDecimal;
import java.util.Locale;

import com.sap.isa.auction.backend.boi.SalesAreaData;
import com.sap.isa.auction.backend.boi.order.AuctionOrderData;
import com.sap.isa.auction.bean.BackendDefaultSettings;
import com.sap.isa.auction.logging.CategoryProvider;
import com.sap.isa.backend.r3.salesdocument.rfc.ReadStrategyR3;
import com.sap.isa.backend.r3base.util.Conversion;
import com.sap.isa.backend.r3base.util.R3BackendData;
import com.sap.isa.core.eai.BackendException;
import com.sap.isa.core.eai.sp.jco.JCoConnection;
import com.sap.isa.user.backend.r3.rfc.RFCWrapperUserBaseR3;
import com.sap.mw.jco.JCO.Table;
import com.sap.tc.logging.Category;
import com.sap.tc.logging.Location;
import com.sap.tc.logging.Severity;


public class AuctionReadStrategyR3
	extends ReadStrategyR3
	implements AuctionReadStrategy, AuctionStrategyBase  {

		private final static Category cat = CategoryProvider.getSystemCategory();
		private BackendDefaultSettings backendSettings = null;
		private final static Location loc =
			Location.getLocation(AuctionReadStrategyR3.class);

			/**
			 * Constructor for the ReadStrategyR3 object.
			 * 
			 * @param backendData  the bean that holds the R/3 specific customizing
			 */
			public AuctionReadStrategyR3(R3BackendData backendData) {
				super(backendData);
			}
			/**
			 * Fills the ISA header prices.
			 * 
			 * @param orderHeader                the ISA sales document header
			 * @param conditionTable                  the RFC table the backend has provided
			 * @param decimalPlaceToShift        how many decimal places do we have to shift for
			 *        the net price and freight? This depends on the R/3 data type
			 * @param connection                 JCO connection
			 * @param determineNetPrViaDivision  Is the net price of the item calculated via
			 *        dividing the net value with the quantity? For small R/3 releases like
			 *        4.0b, it has to be done this way. In the other case, the net price is read
			 *        from the RFC directly and the base unit can differ
			 * @exception BackendException
			 */
		public void getPricesFromHeaderConditions(
			AuctionOrderData order,
			Table conditionTable,
			int decimalPlaceToShift,
			boolean determineNetPrViaDivision,
			JCoConnection connection)
			throws BackendException {

				cat.logT(Severity.DEBUG, loc, "getPricesFromHeaderConditions start");
        
				Locale locale = backendData.getLocale();

				BigDecimal insurance = null;
				BigDecimal freight = null;
				BigDecimal otherChargeDiscount = null;
				boolean isFreightSet = false;
				boolean isInsuranceSet = false;
				boolean isOtherCostSet = false;
				//get insurance price and frieght price
				int i = 0;
				String mfType = order.getFreightManualPriceCondition();
				String miType = order.getInsurManualPriceCondition();
				String otherType = order.getOtherPriceCondition();
				cat.logT(Severity.DEBUG, loc, "The manual condition type frieght is"  + mfType);
				cat.logT(Severity.DEBUG, loc, "The manual condition type insurance is"  + miType);
				if((mfType == null) && (miType == null)){
					cat.logT(Severity.ERROR, loc, "The manual condition types are null, exit from the method");
					mfType = "ZEBF";
					miType = "ZEBI";
					//return;
				}

				if (conditionTable.getNumRows() > 0) {
					conditionTable.firstRow();

					do {
						//taxes
						if (conditionTable.hasField("ITM_NUMBER")) {
							//if (conditionTable.getString("ITM_NUMBER").
							//	equalsIgnoreCase("000000") ){
								String fType = conditionTable.getString("COND_TYPE");
								if(mfType != null){
									if(fType.equalsIgnoreCase(mfType) && (isFreightSet == false)){
										freight = conditionTable.getBigDecimal("COND_VALUE");
										isFreightSet = true;
									}
								}
								if(miType != null){
									if(fType.equalsIgnoreCase(miType)&& (isInsuranceSet == false)){
										insurance = conditionTable.getBigDecimal("COND_VALUE");
										isInsuranceSet = true;
									}
								}
								if(otherType != null){
									if(fType.equalsIgnoreCase(otherType) && (isOtherCostSet == false)){
										otherChargeDiscount = conditionTable.getBigDecimal("COND_VALUE");
										isOtherCostSet = true;
									}
								}
							//}
						}
										}
					 while (conditionTable.nextRow());
				}
				if (freight != null){
					order.getHeaderData().setFreightValue(Conversion.bigDecimalToUICurrencyString(
													freight, 
													locale, 
													order.getHeaderData().getCurrency()));
				}
			
				if(insurance != null){
					order.setInsuranceValue(Conversion.bigDecimalToUICurrencyString(
													insurance, 
													locale, 
													order.getHeaderData().getCurrency()));
				}
			
				if(otherChargeDiscount != null){
					order.setOtherChargeDiscount(Conversion.bigDecimalToUICurrencyString(
													otherChargeDiscount, 
													locale, 
													order.getHeaderData().getCurrency()));
				}
			
				cat.logT(Severity.DEBUG, loc, "getPricesFromHeaderConditions: " + "fright is " + freight + " '" +
					insurance);
		}
		/**
		 * @param settings
		 */
		public void setBackendSettings(BackendDefaultSettings settings) {
			backendSettings = settings;
			RFCWrapperUserBaseR3.SalesAreaData salesArea = new RFCWrapperUserBaseR3.SalesAreaData();
			SalesAreaData salesAreaBkSettings = settings.getSalesArea();
			if(salesAreaBkSettings != null){
				salesArea.setSalesOrg(salesAreaBkSettings.getSalesOrganization());
				salesArea.setDivision(salesAreaBkSettings.getDivision());
				salesArea.setDistrChan(salesAreaBkSettings.getDistributionChannel());
				backendData.setSalesArea(salesArea);
			}else{
				cat.logT(Severity.WARNING, loc, "Backend settings does not contain the sales area information");
			}
			
		}

}
