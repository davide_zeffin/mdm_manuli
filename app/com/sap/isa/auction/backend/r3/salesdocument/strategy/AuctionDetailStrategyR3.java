/*
 * Created on Mar 16, 2004
 *
 * To change the template for this generated file go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
package com.sap.isa.auction.backend.r3.salesdocument.strategy;

import java.util.Locale;

import com.sap.mw.jco.JCO;
import com.sap.spc.remote.client.object.IPCException;
import com.sap.tc.logging.*;
import com.sap.isa.auction.backend.boi.order.AuctionOrderData;
import com.sap.isa.auction.backend.r3.ExtendSalesDocumentHelpValues;
import com.sap.isa.auction.logging.CategoryProvider;
import com.sap.isa.backend.boi.isacore.SalesDocumentData;
import com.sap.isa.backend.boi.isacore.ShipToData;
import com.sap.isa.backend.boi.isacore.order.HeaderData;
import com.sap.isa.backend.r3.salesdocument.rfc.DetailStrategyR3;
import com.sap.isa.backend.r3.salesdocument.rfc.ReadStrategy;
import com.sap.isa.backend.r3base.Extension;
import com.sap.isa.backend.r3base.ExtensionParameters;
import com.sap.isa.backend.r3base.ipc.ServiceR3IPC;
import com.sap.isa.backend.r3base.rfc.RFCConstants;
import com.sap.isa.backend.r3base.rfc.RFCWrapperPreBase;
import com.sap.isa.backend.r3base.util.Conversion;
import com.sap.isa.backend.r3base.util.R3BackendData;
import com.sap.isa.core.TechKey;
import com.sap.isa.core.eai.BackendException;
import com.sap.isa.core.eai.sp.jco.JCoConnection;


public class AuctionDetailStrategyR3 extends DetailStrategyR3 {
	private final static Category cat = CategoryProvider.getSystemCategory();
	private final static Location loc =
		Location.getLocation(AuctionDetailStrategyR3.class);
		
	public AuctionDetailStrategyR3(R3BackendData backendData, 
							ReadStrategy readStrategy) {
		super(backendData, readStrategy);
	}
	
	/**
	 * Fills the whole sales document with items, prices, shipTo's, items, tracking info,
	 * extension data from the backend storage. Reads also document information for
	 * displaying the deliveries attached to sales document items. Uses function module
	 * BAPISDORDER_GETDETAILEDLIST.
	 * 
	 * <p>
	 * Reads the status information for the sales document detail screen. Reads
	 * configuration from the backend storage.
	 * </p>
	 * 
	 * @param document              the ISA document
	 * @param connection            connection to the backend
	 * @param serviceR3IPC          the service object that deals with IPC in when
	 *        running ISA R/3
	 * @exception BackendException  jco exception
	 */
	public void fillDocument(SalesDocumentData document, 
							 JCoConnection connection, 
							 ServiceR3IPC serviceR3IPC)
					  throws BackendException {

		cat.logT(Severity.DEBUG, loc, "fillDocument start");

		JCO.Client theClient = connection.getJCoClient();
		HeaderData orderHeader = document.getHeaderData();

		//check if the document number is filled accordingly
		if (orderHeader.getSalesDocNumber() == null || orderHeader.getSalesDocNumber().trim()
		   .equals("")) {
			orderHeader.setSalesDocNumber(Conversion.cutOffZeros(
												  orderHeader.getTechKey().getIdAsString()));
		}

		//icp expects dates as strings in format yyyyMMdd,
		//that is what Conversion.dateToISADateString delivers
		String ipcDate = null;
		Locale locale = backendData.getLocale();

		//first: fill RFC input parameters
		//get jayco function that refers to the change bapi
		JCO.Function getDetailedList = connection.getJCoFunction(
											   RFCConstants.RfcName.BAPI_ISAORDER_GETDETAILEDLIST);
		JCO.ParameterList importParams = getDetailedList.getImportParameterList();
		JCO.ParameterList tableParams = getDetailedList.getTableParameterList();

		//no reading from buffer! The document might have been
		//changed in R/3, and we have a stateful connection
		//for order and quotation
		importParams.setValue("A", 
							  "I_MEMORY_READ");

		//get the jayco structure for setting the read flags
		JCO.Structure iBapiView = importParams.getStructure(
										  "I_BAPI_VIEW");
		iBapiView.setValue(X, 
						   "HEADER");
		iBapiView.setValue(X, 
						   "PARTNER");

		//read business information. Relevant if some extensions are to be retrieved
		iBapiView.setValue(X, 
						   "BUSINESS");
		iBapiView.setValue(X, 
						   "STATUS_H");
		iBapiView.setValue(X, 
						   "SDCOND");
		iBapiView.setValue(X, 
						   "ITEM");
		iBapiView.setValue(X, 
						   "SDSCHEDULE");
		iBapiView.setValue(X, 
						   "ADDRESS");
		iBapiView.setValue(X, 
						   "FLOW");
		iBapiView.setValue(X, 
						   "STATUS_I");
		iBapiView.setValue(X, 
						   "TEXT");

		if (serviceR3IPC != null && serviceR3IPC.getIPCClient() != null) {
			iBapiView.setValue(X, 
							   "CONFIGURE");
		}

		JCO.Table documents = tableParams.getTable("SALES_DOCUMENTS");

		//SALES_KEYTable documents = importParams.getParams().getSales_Documents();
		documents.appendRow();

		//      documents.getFields().setVbeln(orderHeader.getSalesDocNumber());
		documents.setValue(orderHeader.getTechKey().getIdAsString(), 
						   "VBELN");

		//call customer exit
		performCustExitBeforeR3Call(document, 
									getDetailedList);

		//fire RFC
		cat.logT(Severity.DEBUG, loc, "fillHeaderFromDetailedList, fire RFC for:" + orderHeader.getTechKey().getIdAsString());
		connection.execute(getDetailedList);

		//GetDetailedList.Bapisdorder_Getdetailedlist.Response response = GetDetailedList.bapisdorder_Getdetailedlist(theClient, importParams);
		//check if items have to be constructed as well
		readStrategy.getItemsAndScheduleLines(document, 
											  null, 
											  getDetailedList.getTableParameterList().getTable(
													  "ORDER_ITEMS_OUT"), 
											  getDetailedList.getTableParameterList().getTable(
													  "ORDER_SCHEDULES_OUT"), 
											  getDetailedList.getTableParameterList().getTable(
													  "ORDER_FLOWS_OUT"), 
											  getDetailedList.getTableParameterList().getTable(
													  "ORDER_STATUSITEMS_OUT"), 
											  connection);

		//BAPISDHDTable headerTable = response.getParams().getOrder_Headers_Out();
		JCO.Table headerTable = getDetailedList.getTableParameterList().getTable(
										"ORDER_HEADERS_OUT");

		if (headerTable.getNumRows() > 0) {
			headerTable.firstRow();
			orderHeader.setTechKey(new TechKey(RFCWrapperPreBase.trimZeros10( orderHeader.getSalesDocNumber())));

			//set the order header parameters
			readStrategy.getHeader(orderHeader, 
								   headerTable);
			ipcDate = Conversion.dateToISADateString(headerTable.getDate(
															 "DOC_DATE"));
			orderHeader.setPostingDate(Conversion.dateToUIDateString(headerTable.getDate("DOC_DATE"),backendData.getLocale()));                                                             

			//these values might be changed later on!
			orderHeader.setStatusOpen();
			orderHeader.setDeliveryStatusOpen();
			orderHeader.setSalesDocumentsOrigin("");
			orderHeader.setProcessType("");
			
			//replace shipping condition key
			if(document instanceof AuctionOrderData) {
				AuctionOrderData order = (AuctionOrderData)document;
				order.setDeliveryCode(headerTable.getString("DLV_BLOCK"));				
				ExtendSalesDocumentHelpValues helpValues = (ExtendSalesDocumentHelpValues)
					ExtendSalesDocumentHelpValues.getInstance();
				order.setDeliveryText(helpValues.getHelpValue(ExtendSalesDocumentHelpValues.HELP_TYPE_DLV_BLOCK, 
													   connection, 
													   backendData.getLanguage(), 
													   order.getDeliveryBlockCode(), 
													   backendData.getConfigKey()));
			}
		}

		//fill soldTo and shipTo info
		JCO.Table partnerTable = getDetailedList.getTableParameterList().getTable(
										 "ORDER_PARTNERS_OUT");
		getHeaderPartnerList(document.getHeaderData(),partnerTable);

		//clear all ship-to information
		document.clearShipTos();

		//fill ship-to information on header and item level
		fillShipTo(document, 
				   partnerTable, 
				   getDetailedList.getTableParameterList().getTable(
						   "ORDER_ADDRESS_OUT"), 
				   connection);

		ShipToData shipTo = document.getHeaderData().getShipToData();
		orderHeader.setShipToData(shipTo);

		cat.logT(Severity.DEBUG, loc, "fillShipTo performed");
		cat.logT(Severity.DEBUG, loc, "ship to on order header: " + shipTo);

		if (shipTo != null) {
			cat.logT(Severity.DEBUG, loc, "short adress: " + shipTo.getShortAddress());
		}

		cat.logT(Severity.DEBUG, loc, "size of shipTo-List: " + document.getNumShipTos());

		//fill prices
		readStrategy.getPricesFromDetailedList(orderHeader, 
											   document.getItemListData(), 
											   getDetailedList.getTableParameterList().getTable(
													   "ORDER_ITEMS_OUT"), 
											   0, 
											   false, 
											   connection);
		///If the ReadStragety 
		try{
			if (readStrategy instanceof AuctionReadStrategy){
				((AuctionReadStrategy)readStrategy).getPricesFromHeaderConditions(
						(AuctionOrderData)document,
				getDetailedList.getTableParameterList().getTable(
												   "ORDER_CONDITIONS_OUT"),
													0, 
													false, 								   
												   connection);
			}
		}catch (Exception ex){
			cat.log(Severity.ERROR, loc, "In Detail Strategy exceptions", ex);
		}
		//get document flow
		readStrategy.getDocumentFlow(orderHeader, 
									 getDetailedList.getTableParameterList().getTable(
											 "ORDER_FLOWS_OUT"));

		//set parent id's into items. We don't get this from BAPI_SALESORDER_GETSTATUS,
		//so we have again to loop over items and set parent id's
		//setParentIds(document, response.getParams().getOrder_Items_Out());
		//now set header status information
		JCO.Table statusTable = getDetailedList.getTableParameterList().getTable(
										"ORDER_STATUSHEADERS_OUT");

		//get statuses from R/3
		readStrategy.getHeaderStatuses(orderHeader, 
									   headerTable, 
									   statusTable);

		//get texts from R/3
		readStrategy.getTexts(document, 
							  getDetailedList.getTableParameterList().getTable(
									  "ORDER_TEXTHEADERS_OUT"), 
							  getDetailedList.getTableParameterList().getTable(
									  "ORDER_TEXTLINES_OUT"));

		//fill extensions in header and items
		Extension extensionR3 = getExtension(getDetailedList.getTableParameterList().getTable(
													 getExtensionName()), 
											 document, 
											 backendData, 
											 ExtensionParameters.EXTENSION_DOCUMENT_READ);

		//response.getParams().
		extensionR3.tableToDocument();

		//check extensions on document level
		cat.logT(Severity.DEBUG, loc, "extensions on header level: ");
		logIterator(document.getHeaderData().getExtensionDataValues().iterator());

		for (int i = 0; i < document.getItemListData().size(); i++) {
			cat.logT(Severity.DEBUG, loc, "extensions on item level:");
			logIterator(document.getItemListData().getItemData(
								i).getExtensionDataValues().iterator());
		}

		//check if plugIn is available. Configuration information is only collected
		//if plugIn is available
		if (serviceR3IPC != null && serviceR3IPC.isIPCAvailable()) {

			try {
				fillIPCInfoFromDetailedListRFC(document, 
											   getDetailedList.getTableParameterList().getTable(
													   "ORDER_CFGS_CUCFGS_OUT"), 
											   getDetailedList.getTableParameterList().getTable(
													   "ORDER_CFGS_CUINS_OUT"), 
											   getDetailedList.getTableParameterList().getTable(
													   "ORDER_CFGS_CUPRTS_OUT"), 
											   getDetailedList.getTableParameterList().getTable(
													   "ORDER_CFGS_CUVALS_OUT"), 
											   ipcDate, 
											   serviceR3IPC,
                                               connection);
			}
			 catch (IPCException ex) {
				throw new BackendException(ex.getMessage());
			}
		}

		//call customer exit
		performCustExitAfterR3Call(document, 
								   getDetailedList);
	}
}
