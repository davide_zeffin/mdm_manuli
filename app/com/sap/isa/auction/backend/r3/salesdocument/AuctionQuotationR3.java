/*****************************************************************************
    Copyright (c) 2001, SAPMarkets Palo Alto, USA, All rights reserved.

*****************************************************************************/

package com.sap.isa.auction.backend.r3.salesdocument;

import java.util.HashMap;
import java.util.Locale;
import java.util.Properties;

import com.sap.isa.auction.backend.boi.AuctionData;
import com.sap.isa.auction.backend.boi.order.AuctionQuotationBackend;
import com.sap.isa.auction.backend.boi.order.AuctionQuotationData;
import com.sap.isa.auction.backend.r3.AuctionRFCConstantsR3;
import com.sap.isa.auction.backend.r3.R3BackendAuctionData;
import com.sap.isa.auction.backend.r3.salesdocument.strategy.AuctionChangeStrategy;
import com.sap.isa.auction.backend.r3.salesdocument.strategy.AuctionChangeStrategyR3;
import com.sap.isa.auction.backend.r3.salesdocument.strategy.AuctionCreateStrategyR3;
import com.sap.isa.auction.backend.r3.salesdocument.strategy.AuctionReadStrategyR3;
import com.sap.isa.auction.backend.r3.salesdocument.strategy.AuctionStrategyBase;
import com.sap.isa.auction.backend.r3.salesdocument.strategy.AuctionWriteStrategyR3;
import com.sap.isa.auction.bean.BackendDefaultSettings;
import com.sap.isa.auction.logging.CategoryProvider;
import com.sap.isa.backend.IsaBackendBusinessObjectBaseSAP;
import com.sap.isa.backend.boi.isacore.order.QuotationData;
import com.sap.isa.backend.r3.salesdocument.QuotationR3;
import com.sap.isa.backend.r3.salesdocument.rfc.DetailStrategyR3;
import com.sap.isa.backend.r3.salesdocument.rfc.StatusStrategyR3;
import com.sap.isa.backend.r3base.ExtensionParameters;
import com.sap.isa.backend.r3base.util.R3BackendData;
import com.sap.isa.core.eai.BackendBusinessObjectParams;
import com.sap.isa.core.eai.BackendException;
import com.sap.isa.core.eai.sp.jco.JCoConnection;
import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.core.util.MiscUtil;
import com.sap.tc.logging.Category;
import com.sap.tc.logging.Location;
import com.sap.tc.logging.Severity;
//import com.sap.isa.core.logging.IsaLocation;
/**
 * This class is used to serve following purpose for the auction scenario
 * 1. Set the sale employee
 * 2. Set the price to the reserved price
 * 3. Set the one-time customer
 * 4. Release quotation by change the status
 * 5. Set the purchase order number to the auction listing ID
 * @
 */
public class AuctionQuotationR3
	extends QuotationR3
	implements AuctionQuotationBackend, AuctionBackendBase{
	/** The logging instance. */
	private final static Category cat = CategoryProvider.getSystemCategory();
	private final static Location loc =
		Location.getLocation(AuctionQuotationR3.class);
	/** The backend properties are read within initBackendObject and later on used. */
	protected Properties props;
	
    /**
     *  Sets the bean that holds all the r3 specific information that is not part
     *  of the shop or returns it if this is already present. <p>
     *
     *  If there is no backendData instance in the backend context, a new one is
     *  created and stored there.
     *
     *@return                       the bean that holds the R/3 customizing
     *@exception  BackendException  exception from backend
     */
    protected R3BackendData getOrCreateBackendData() throws BackendException {
        IsaLocation log = IsaLocation.getInstance(
                                                 QuotationR3.class.getName());


        if (backendData == null) {
            if ((log.isDebugEnabled())) {
                log.debug("backend data not set for this business object");
            }
            backendData = (R3BackendAuctionData) context.getAttribute(R3BackendData.R3BACKEND_AUCTION_DATA_KEY);
            if (backendData == null) {
                if ((log.isDebugEnabled())) {
                    log.debug("backend data not set in backend context");
                }
                backendData = new R3BackendAuctionData();

                //set backend config key
                String longBCKey = getBackendObjectSupport().getBackendConfigKey();
                String key = MiscUtil.getDigest(longBCKey);
                
                if (log.isDebugEnabled())
                    log.debug("digest " + key + " from " + longBCKey);
                                    
                if (key == null){ 
                    key = "";
                    log.debug("getBackendObjectSupport().getBackendConfigKey() is null");
                }
                
                backendData.setExtensionData(new HashMap());
                backendData.getExtensionData().put(
                    ExtensionParameters.EXTENSION_DOCUMENT_CHANGE,
                    getContext().getAttribute(
                        ExtensionParameters.EXTENSION_DOCUMENT_CHANGE));
                backendData.getExtensionData().put(
                    ExtensionParameters.EXTENSION_DOCUMENT_CREATE,
                    getContext().getAttribute(
                        ExtensionParameters.EXTENSION_DOCUMENT_CREATE));
                backendData.getExtensionData().put(
                    ExtensionParameters.EXTENSION_DOCUMENT_READ,
                    getContext().getAttribute(
                        ExtensionParameters.EXTENSION_DOCUMENT_READ));
                backendData.getExtensionData().put(
                    ExtensionParameters.EXTENSION_DOCUMENT_SEARCH,
                    getContext().getAttribute(
                        ExtensionParameters.EXTENSION_DOCUMENT_SEARCH));

                backendData.setConfigKey(key);

                context.setAttribute(R3BackendData.R3BACKEND_AUCTION_DATA_KEY, backendData);
            }
            else if ((log.isDebugEnabled())) {
                log.debug("backend data exists in context");
            }
        }
        else if ((log.isDebugEnabled())) {
            log.debug("backend data exists in backend object");
        }
        return backendData;
    }
    
	/**
	 * Initializes Business Object.
	 * 
	 * @param props                 a set of properties which may be useful to
	 * initialize the object
	 * @param params                a object which wraps parameters
	 * @exception BackendException  Description of Exception
	 */
	public void initBackendObject(
		Properties property,
		BackendBusinessObjectParams params)
		throws BackendException {
		//super.initBackendObject(props, 
		//params);

		this.props = property;

        /*
         * here must be called a special version that guarantees that only R3BackendAuctionData are retrieved
         * for this purpose it is ensured that the context can hold separate types of backenddata
         */
		R3BackendData backendData = getOrCreateBackendData();
		if (backendData.getLocale() == null) {
			Locale locale = Locale.US;
			backendData.setLocale(locale);
		}
		if (getContext().getAttribute(
				IsaBackendBusinessObjectBaseSAP.ISA_LANGUAGE) == null){
			getContext().setAttribute(
				IsaBackendBusinessObjectBaseSAP.ISA_LANGUAGE,
				"EN");
		}
		backendData.setLanguage((String)getContext().getAttribute(
							IsaBackendBusinessObjectBaseSAP.ISA_LANGUAGE));
		//Following block to avoid the NullException error
		// normally they should be loaded from BackendSettingBase object
		cat.logT(Severity.DEBUG, loc, "--------------------------------------");
		cat.logT(Severity.DEBUG, loc, "The SubTotalFreight from backenkenddata is" + 
			backendData.getSubTotalFreight());
		cat.logT(Severity.DEBUG, loc, "The R3Release is " + backendData.getR3Release());
		cat.logT(Severity.DEBUG, loc, "The backend is " + backendData.getBackend());
		cat.logT(Severity.DEBUG, loc, "--------------------------------------");

		if (backendData.getSubTotalFreight() == null)
			backendData.setSubTotalFreight("4");
		if (backendData.getR3Release() == null)
			backendData.setR3Release("");
		if (backendData.getBackend() == null)
			backendData.setBackend("");

		readStrategy = new AuctionReadStrategyR3(backendData);
		writeStrategy = new AuctionWriteStrategyR3(backendData, readStrategy);
		createStrategy =
			new AuctionCreateStrategyR3(
				backendData,
				writeStrategy,
				readStrategy);
		// now you can dtermaine the version of implementation to use
		// add later 
		changeStrategy =
			new AuctionChangeStrategyR3(
				backendData,
				writeStrategy,
				readStrategy);
		detailStrategy = new DetailStrategyR3(backendData, 
											readStrategy);
		statusStrategy = new StatusStrategyR3(backendData , readStrategy);
		// we do not need IPC service yet
		this.serviceIPC = null;

		((AuctionCreateStrategyR3)createStrategy).setProps(props);
		cat.logT(Severity.DEBUG, loc, "-------The passing in property is in " +
										"Quotation Creation----- ");
		cat.logT(Severity.DEBUG, loc, "backendData language is " +
							backendData.getLanguage());
		cat.logT(Severity.DEBUG, loc, "-------------------------------------- ");	
	}

	public void setBackendSettings(BackendDefaultSettings settings)
	{
		setStrategyBackendSettings(writeStrategy, settings);
		setStrategyBackendSettings(createStrategy, settings);
		setStrategyBackendSettings(changeStrategy, settings);
		setStrategyBackendSettings(readStrategy, settings);
	}
	
	private void setStrategyBackendSettings(Object strategy, BackendDefaultSettings settings)
	{
		if(strategy != null && strategy instanceof AuctionStrategyBase)
			((AuctionStrategyBase)strategy).setBackendSettings(settings);
	}
	
	/**
	 * Saves both lean or extended quotations in the backend;
	 * needs sales area to decide
	 */
	public void createInBackend(AuctionQuotationData quotation)
		throws BackendException {
		cat.logT(Severity.DEBUG, loc, "saveInBackend(AuctionQuotationData quotation)start");
		JCoConnection aJCoCon = null;
		
		try {
			//check if the internal storage has been set up
			checkInternalStorage(quotation);
			//String language = (String) getContext().getAttribute(IsaBackendBusinessObjectBaseSAP.ISA_LANGUAGE);
			aJCoCon = getDefaultJCoConnection();
			cat.logT(Severity.DEBUG, loc, "saveInBackend: create");
			//Save quotation
			createStrategy.createDocument(
				quotation,
				null,
				newShipToAddress,
				AuctionRFCConstantsR3.TRANSACTION_TYPE_QUOTATION,
				aJCoCon,
				null);
			
			//now set the flag that indicates that the order has to be re-read from
			//backend. Later on we call a bapi that allows to re-read the order
			//directly after creating it
			setDirtyForInternalStorage(true);
		} finally {
			if(aJCoCon != null)
				aJCoCon.close();
		}
		cat.logT(Severity.DEBUG, loc, "Document is created successfully");

	}

	/**
	 * We enabled this method at the business object level. The mapping
	 * is not done here
	 */
	public void createInBackend(AuctionData auction) throws BackendException {
		cat.logT(Severity.DEBUG, loc, "Not supported createInbackend(AuctionData)");
		throw new BackendException();
	}

	/**
	 * Saves the modification both lean or extended quotations in the backend
	 * order type in included in the quotationdata
	 */
	public void modifyInBackend(AuctionQuotationData quotation)
		throws BackendException {
		cat.logT(Severity.DEBUG, loc, "Update InBackend(AuctionQuotationData quotation)start");
		JCoConnection aJCoCon = null;

		try {
			//check if the internal storage has been set up
			checkInternalStorage(quotation);
	
			aJCoCon = getDefaultJCoConnection();
			//now update the internal storage for storing changed shipto's
			//and changing the status of changed ship'tos
	//		adjustInternalStorageBeforeUpdate(
	//			getDocumentFromInternalStorage(),
	//			quotation,
	//			false);
	
			//change RFC
			//now change the document
			changeStrategy.changeDocument(this, quotation, "", aJCoCon, null);
	
			//now set the flag that indicates that the order has to be re-read from
			//backend. Later on we call a bapi that allows to re-read the order
			//directly after creating it
			setDirtyForInternalStorage(true);
		} finally {
			if(aJCoCon != null)
				aJCoCon.close();
		}

		cat.logT(Severity.DEBUG, loc, "update has been performed");
	}

	/**
	 * Cancels quotation in the backend. Not implemented for ISA R/3.
	 * 
	 * it will change the status of the quotation to release the
	 * reservation, it uses Rejection reason to change the 
	 * status of a quotation. Currently it will set the rejection reason 
	 * for each of the item, then the complete status of the quotation will be
	 * changed
	 * @param quotation             the auction quotation data
	 * @exception BackendException  Exception from backend
	 */
	public void cancelInBackend(QuotationData quotation)
		throws BackendException {
		cat.logT(Severity.DEBUG, loc, 
				"In AuctionQuotationR3, "
					+ "		trying to cancel the quotation");
		JCoConnection aJCoCon = null;
			
		try {
			cat.logT(Severity.DEBUG, loc, 
						"In AuctionQuotationR3, "
							+ "		trying to read the quotation from ERP first");
			setDirtyForInternalStorage(true);
			readFromBackend((AuctionQuotationData)quotation);
			
			aJCoCon = getDefaultJCoConnection();
			((AuctionChangeStrategy)changeStrategy).changeStatus(quotation,
												 "", aJCoCon);
			
			//now set the flag that indicates that the order has to be re-read from
			//backend. Later on we call a bapi that allows to re-read the order
			//directly after creating it
			setDirtyForInternalStorage(true);
		} finally {
			if(aJCoCon != null)
				aJCoCon.close();
		}

		cat.logT(Severity.DEBUG, loc, "status change has been performed");
	}
	/**
	 * Saves the modification both lean or extended quotations in the backend
	 * order type in included in the quotationdata
	 */
	public void setLisingIdInBackend(AuctionQuotationData quotation)
		throws BackendException {
			cat.logT(Severity.DEBUG, loc, 
				"In AuctionQuotationR3, "
					+ "		trying to set the listing quotation");
			JCoConnection aJCoCon = null;

			try {
				//check if the internal storage has been set up
				checkInternalStorage(quotation);
	
				aJCoCon = getDefaultJCoConnection();
				//now update the internal storage for storing changed shipto's
				//and changing the status of changed ship'tos
	//			adjustInternalStorageBeforeUpdate(
	//				getDocumentFromInternalStorage(),
	//				quotation,
	//				false);
	
				//change RFC
				//now change the document
				((AuctionChangeStrategy)changeStrategy).changeListingId(quotation,
													 "", aJCoCon);
	
				//now set the flag that indicates that the order has to be re-read from
				//backend. Later on we call a bapi that allows to re-read the order
				//directly after creating it
				setDirtyForInternalStorage(true);
			} finally {
				if(aJCoCon != null)
					aJCoCon.close();
			}

			cat.logT(Severity.DEBUG, loc, 
				"update has been performed");
	}

	/* Release the unused quantity in a quotation
	 * - Split the line items if the winner is determined for partial quantity
	 * - Assign rejection code for the line item corresponding to the left over quantity
	 * - Set the sales document header status if needed
	 * @see com.sap.isa.auction.backend.boi.order.AuctionQuotationBackend#releaseUnusedQuantity(com.sap.isa.auction.businessobject.order.AuctionQuotation)
	 */
	public void releaseRemainigQuantityInQuotation(AuctionQuotationData quotation) throws BackendException {
		cat.logT(Severity.DEBUG, loc, "releaseRemainigQuantityInQuotation(AuctionQuotationData quotation)start");
		JCoConnection aJCoCon = null;

		try {
			//check if the internal storage has been set up
			checkInternalStorage(quotation);
			//String language = (String) getContext().getAttribute(IsaBackendBusinessObjectBaseSAP.ISA_LANGUAGE);
			aJCoCon = getDefaultJCoConnection();
			cat.logT(Severity.DEBUG, loc, "releaseRemainigQuantityInQuotation: modify");
			//Save quotation
			changeStrategy.changeDocument(this,
				quotation,
				"",
				aJCoCon,
				null);
			
			//now set the flag that indicates that the order has to be re-read from
			//backend. Later on we call a bapi that allows to re-read the order
			//directly after modifying it
			setDirtyForInternalStorage(true);
		} finally {
			if(aJCoCon != null)
				aJCoCon.close();
		}

		cat.logT(Severity.DEBUG, loc, "Remaining quantity is released successfully");		
		
	}

	/* 
	 * Reads the sales document from R3/ERP
	 * Header data of the sales document should contain the sales document ID
	 * @see com.sap.isa.auction.backend.boi.order.AuctionQuotationBackend#readFromBackend(com.sap.isa.auction.backend.boi.order.AuctionQuotationData)
	 */
	public void readFromBackend(AuctionQuotationData quotation) throws BackendException {
		super.readFromBackend(quotation);
	}
 
}
