/*
 * Title:        eBay Integration Project
 * Description:	 * SAP Labs LLC, the eBay auction project, also is known as Selling via eBay. 
 * Selling via eBay application provides SAP R/3 (and in the future CRM) 
 * customers to auction and sell inventory/products onto an online marketplace 
 * eBay. The application will provide the ability to Create, Schedule, Publish, 
 * Monitor and Close Auction Listings onto eBay and handle all the relevant 
 * Backend processing for Customers and Orders. 

 * Copyright:    Copyright (c) 2003
 * Company:      SAP Labs LLC
 * @author
 * @version 1.0
 */
package com.sap.isa.auction.backend.r3.salesdocument.strategy;

import java.util.Hashtable;

import com.sap.mw.jco.JCO;
import com.sap.tc.logging.Category;
import com.sap.tc.logging.Location;
import com.sap.tc.logging.Severity;
import com.sap.isa.auction.backend.BackendHelper;
import com.sap.isa.auction.backend.exception.BackendUserException;
import com.sap.isa.auction.bean.BackendDefaultSettings;
import com.sap.isa.auction.backend.boi.BackendSettingsData;
import com.sap.isa.auction.businessobject.order.AuctionOrder;
import com.sap.isa.backend.boi.isacore.SalesDocumentData;
import com.sap.isa.backend.boi.isacore.ShipToData;
import com.sap.isa.backend.boi.isacore.order.ItemData;
import com.sap.isa.backend.boi.isacore.order.ItemListData;
import com.sap.isa.backend.r3.salesdocument.SalesDocumentR3;
import com.sap.isa.backend.r3.salesdocument.rfc.ChangeStrategyR3;
import com.sap.isa.backend.r3.salesdocument.rfc.ReadStrategy;
import com.sap.isa.backend.r3.salesdocument.rfc.WriteStrategy;
import com.sap.isa.backend.r3base.Extension;
import com.sap.isa.backend.r3base.ExtensionParameters;
import com.sap.isa.backend.r3base.ipc.ServiceR3IPC;
import com.sap.isa.backend.r3base.rfc.RFCConstants;
import com.sap.isa.backend.r3base.rfc.RFCWrapperPreBase;
import com.sap.isa.backend.r3base.rfc.ReturnValue;
import com.sap.isa.backend.r3base.util.Conversion;
import com.sap.isa.backend.r3base.util.R3BackendData;
import com.sap.isa.core.TechKey;
import com.sap.isa.core.eai.BackendException;
import com.sap.isa.core.eai.sp.jco.JCoConnection;
import com.sap.isa.auction.logging.CategoryProvider;
import com.sap.isa.auction.util.StringUtil;
//import com.sap.isa.core.logging.IsaLocation;
/**
 * It is used to change the status 
 */
public class AuctionChangeStrategyR3
	extends ChangeStrategyR3
	implements AuctionChangeStrategy, AuctionStrategyBase {
		//	private static IsaLocation log =
		//		IsaLocation.getInstance(AuctionChangeStrategyR3.class.getName());
		private static Hashtable documentKeys = new Hashtable();
		protected BackendSettingsData backendSettings = null;
		private final static Category cat = CategoryProvider.getSystemCategory();
		private final static Location loc =
			Location.getLocation(AuctionChangeStrategyR3.class);
		/**
		 *
		 * @param arg0			R3BackendData, holds all of the backenddata
		 * @param arg1			WriteStrategy
		 * @param arg2			ReadStrategy
		 */
		public AuctionChangeStrategyR3(
			R3BackendData arg0,
			WriteStrategy arg1,
			ReadStrategy arg2) {
			super(arg0, arg1, arg2);

		}

		/**
		 * Change the status of the auction quotation, then the reservation
		 * or the customer requirements can be released
		 * It changes at the Item level, then the header level is
		 * changed automatically
		 * @param salesDocument         the isa sales document
		 * @param simulate              'X': call the RFC in simulate-mode. When updating the
		 *        document, the function module is simulated; when saving, it is called in
		 *        non-simulate mode and a commit is performed
		 * @param connection            the JCO connection
		 * @return was the change successfull?
		 * @exception BackendException  exception from backend
		 **/
		public ReturnValue changeStatus(
			SalesDocumentData salesDocument,
			String simulate,
			JCoConnection connection)
			throws BackendException {

			cat.logT(Severity.DEBUG, loc, "changeDocument Status start");

			//if validation was OK: fill RFC input parameters
			//get jayco function that refers to the change bapi
			JCO.Function sdSalesDocumentChange =
				connection.getJCoFunction(
					RFCConstants.RfcName.SD_SALESDOCUMENT_CHANGE);
			JCO.ParameterList importParams =
				sdSalesDocumentChange.getImportParameterList();
			JCO.ParameterList tableParams =
				sdSalesDocumentChange.getTableParameterList();

			//sales doc number into order header
			salesDocument.getHeaderData().setSalesDocNumber(
				Conversion.cutOffZeros(salesDocument.getTechKey().getIdAsString()));

			//set sales doc number
			importParams.setValue(
				BackendHelper.ExtendString(salesDocument.getTechKey().getIdAsString(), 10),
				"SALESDOCUMENT");
			importParams.setValue(simulate, "SIMULATION");
			importParams.setValue(X, "CALL_FROM_BAPI");
			JCO.Structure headerXData =
				sdSalesDocumentChange.getImportParameterList().getStructure(
					"ORDER_HEADER_INX");

			//now fill those flags
			headerXData.setValue(U, RFCConstants.RfcField.UPDATEFLAG);
			// now we change the the status at Item level
			//retrieve item tables and set delta flags
			JCO.Table itemTable = tableParams.getTable("ITEM_IN");
			JCO.Table itemXTable = tableParams.getTable("ITEM_INX");

			//retrieve schedule tables and set delta flags
			//set items and schedule lines
			((AuctionWriteStrategy) writeStrategy).setItemStatus(
				itemTable,
				itemXTable,
				salesDocument);
			//fire RFC. Synchronization because after excuting the change RFC,
			//the document has to be locked again, the change RFC removes all
			//locks
			String key =
				salesDocument.getTechKey().getIdAsString()
					+ backendData.getConfigKey();
			documentKeys.put(key, X);

			//connection.setAbapDebug(true);
			synchronized (documentKeys.get(key)) {
				connection.execute(sdSalesDocumentChange);

				//directly after executing, get the lock because the sales document change
				//bapi resets all locks
				//enqueueDocument(salesDocument, connection);
			}

			documentKeys.remove(key);
			JCO.Table retTable =
				sdSalesDocumentChange.getTableParameterList().getTable("RETURN");
			
			BackendUserException buExc = new BackendUserException();
			boolean hasError = false;
			if (retTable.getNumRows() > 0) {
				retTable.firstRow();

				do {
					// if there
					String msg = retTable.getString("MESSAGE");
					if(retTable.getString("TYPE").equalsIgnoreCase("E")){
						hasError = true;
						cat.logT(Severity.ERROR, loc, 
								"error message type: " + retTable.getString("TYPE"));
						cat.logT(Severity.ERROR, loc, 
								"error message number: "
									+ retTable.getString("NUMBER"));
						cat.logT(Severity.ERROR, loc, 
								"error message is " + msg);
						buExc.addErrorMessage(msg);
					
					}else{

						cat.logT(Severity.DEBUG, loc, 
									"error message type: " + retTable.getString("TYPE"));
						cat.logT(Severity.DEBUG, loc, 
									"error message number: "
										+ retTable.getString("NUMBER"));
						cat.logT(Severity.DEBUG, loc, 
									"error message is " + msg);
						buExc.addWarningMessage(msg);
					}
				} while (retTable.nextRow());
			}

			if(hasError){
				throw buExc;		
			}
		
			ReturnValue retVal =
				new ReturnValue(
					sdSalesDocumentChange.getTableParameterList().getTable(
						"RETURN"),
					"");
			if (true) {
				//!readStrategy
				//	.addSelectedMessagesToBusinessObject(retVal, salesDocument)) {

				//check the simulate flag. If it was simulation, re-read the data
				//from the RFC, if it was create, a commit has to be called
				if (!simulate.equals(X)) {

					try {
						commit(connection, X);
					} catch (Exception e) {
						cat.logT(
							Severity.ERROR,
							loc,
							"During the commit of the changes");
						throw new BackendException(e.getMessage());
					}
				}
			} else {

				//the message will be displayed on the UI. No exception will be thrown
				retVal =
					new ReturnValue(
						sdSalesDocumentChange.getTableParameterList().getTable(
							"RETURN"),
						RFCConstants.BAPI_RETURN_ERROR);

				cat.logT(
					Severity.DEBUG,
					loc,
					"message list size: " + salesDocument.getMessageList().size());
			}
			cat.logT(Severity.DEBUG, loc, "changeDocument Status End");
			return retVal;
		}

		/**
		 * set the listing number to the sales document.
		 * This API is really simple, just change the P>O. Purchase number
		 * It calls SD_SALESDOCUMENT_CHANGE, here we all want is the document ID
		 * @param salesDocument         the isa sales document
		 * @param simulate              'X': call the RFC in simulate-mode. When updating the
		 *        document, the function module is simulated; when saving, it is called in
		 *        non-simulate mode and a commit is performed
		 * @param connection            the JCO connection
		 * @return was the change successfull?
		 * @exception BackendException  exception from backend
		 */
		public ReturnValue changeListingId(
			SalesDocumentData salesDocument,
			String simulate,
			JCoConnection connection)
			throws BackendException {
			cat.logT(Severity.DEBUG, loc, "changeDocument Listing ID start");

			//first do the validation of dates and quantities
			//if validation was OK: fill RFC input parameters
			//get jayco function that refers to the change bapi
			JCO.Function sdSalesDocumentChange =
				connection.getJCoFunction(
					RFCConstants.RfcName.SD_SALESDOCUMENT_CHANGE);
			JCO.ParameterList importParams =
				sdSalesDocumentChange.getImportParameterList();
			JCO.ParameterList tableParams =
				sdSalesDocumentChange.getTableParameterList();

			//sales doc number into order header
			String docNum = salesDocument.getHeaderData().getSalesDocNumber();
			if (docNum == null)
				docNum = salesDocument.getTechKey().getIdAsString();

			//set sales doc number
			JCO.Field docNumField = importParams.getField("SALESDOCUMENT");
			StringUtil.setExtendIdField(docNumField, docNum);
			cat.logT(
				Severity.DEBUG,
				loc,
				"the document ID for change is " + docNum);
			importParams.setValue(simulate, "SIMULATION");
			importParams.setValue(X, "CALL_FROM_BAPI");

			//get structure that holds the header data
			JCO.Structure headerData =
				sdSalesDocumentChange.getImportParameterList().getStructure(
					"ORDER_HEADER_IN");

			//now fill header parameters
			headerData.setValue(
				salesDocument.getHeaderData().getPurchaseOrderExt(),
				"PURCH_NO_C");

			cat.logT(
				Severity.DEBUG,
				loc,
				"The new listing ID is"
					+ salesDocument.getHeaderData().getPurchaseOrderExt());
			//get structure that holds the header change flags i.e. here we
			//determine which fields have to be changed on header level
			JCO.Structure headerXData =
				sdSalesDocumentChange.getImportParameterList().getStructure(
					"ORDER_HEADER_INX");

			//now fill those flags
			headerXData.setValue(U, RFCConstants.RfcField.UPDATEFLAG);
			headerXData.setValue(X, "PURCH_NO_C");
			//fire RFC. Synchronization because after excuting the change RFC,
			//the document has to be locked again, the change RFC removes all
			//locks
			String key = docNum + backendData.getConfigKey();
			if (key == null) { // try to avoid the exception
				cat.logT(
					Severity.WARNING,
					loc,
					"Something is not right, the config Key is null");
				key = "XXX";
			}
			documentKeys.put(key, X);

			synchronized (documentKeys.get(key)) {
				connection.execute(sdSalesDocumentChange);

				//directly after executing, get the lock because the sales document change
				//bapi resets all locks
				//enqueueDocument(salesDocument, connection);
			}

			documentKeys.remove(key);

			ReturnValue retVal =
				new ReturnValue(
					sdSalesDocumentChange.getTableParameterList().getTable(
						"RETURN"),
					"");
			if (true) {
				//!readStrategy
				//	.addSelectedMessagesToBusinessObject(retVal, salesDocument)) {

				//check the simulate flag. If it was simulation, re-read the data
				//from the RFC, if it was create, a commit has to be called
				if (!simulate.equals(X)) {

					try {
						commit(connection, X);
					} catch (Exception e) {
						cat.logT(
							Severity.ERROR,
							loc,
							"During the commit of tarnsaction");
						throw new BackendException(e.getMessage());
					}
				}
			} else {

				//the message will be displayed on the UI. No exception will be thrown
				retVal =
					new ReturnValue(
						sdSalesDocumentChange.getTableParameterList().getTable(
							"RETURN"),
						RFCConstants.BAPI_RETURN_ERROR);

				cat.logT(
					Severity.DEBUG,
					loc,
					"message list size: " + salesDocument.getMessageList().size());
			}
			cat.logT(Severity.DEBUG, loc, "changeDocument Listing ID End");
			return retVal;
		}

		/**
		 * release the delivery block of the sales document.
		 * This API is really simple, just change the P>O. Purchase number
		 * It calls SD_SALESDOCUMENT_CHANGE, here we all want is the document ID
		 * @param salesDocument         the isa sales document
		 * @param connection            the JCO connection
		 * @return was the change successfull?
		 * @exception BackendException  exception from backend
		 */
		public ReturnValue releaseDeliveryBlock(
			SalesDocumentData salesDocument,
			String simulate,
			JCoConnection connection)
			throws BackendException {
			cat.logT(Severity.DEBUG, loc, "relaese the delivery blick start");
			return releaseXXXBlock(
				salesDocument,
				simulate,
				"DLV_BLOCK",
				connection);
		}

		/**
		 * release the delivery block of the sales document.
		 * This API is really simple, just change the P>O. Purchase number
		 * It calls SD_SALESDOCUMENT_CHANGE, here we all want is the document ID
		 * @param salesDocument         the isa sales document
		 * @param connection            the JCO connection
		 * @return was the change successfull?
		 * @exception BackendException  exception from backend
		 */
		public ReturnValue releaseBillingBlock(
			SalesDocumentData salesDocument,
			String simulate,
			JCoConnection connection)
			throws BackendException {
			cat.logT(Severity.DEBUG, loc, "relaese the delivery blick start");

			return releaseXXXBlock(
				salesDocument,
				simulate,
				"BILL_BLOCK",
				connection);
		}

		/**
		 * release the delivery block of the sales document.
		 * This API is really simple, just change the P>O. Purchase number
		 * It calls SD_SALESDOCUMENT_CHANGE, here we all want is the document ID
		 * @param salesDocument         the isa sales document
		 * @param connection            the JCO connection
		 * @return was the change successfull?
		 * @exception BackendException  exception from backend
		 */
		private ReturnValue releaseXXXBlock(
			SalesDocumentData salesDocument,
			String simulate,
			String fieldName,
			JCoConnection connection)
			throws BackendException {
			//first do the validation of dates and quantities
			//if validation was OK: fill RFC input parameters
			//get jayco function that refers to the change bapi
			JCO.Function sdSalesDocumentChange =
				connection.getJCoFunction(
					RFCConstants.RfcName.SD_SALESDOCUMENT_CHANGE);
			JCO.ParameterList importParams =
				sdSalesDocumentChange.getImportParameterList();
			JCO.ParameterList tableParams =
				sdSalesDocumentChange.getTableParameterList();

			//sales doc number into order header
			salesDocument.getHeaderData().setSalesDocNumber(
				Conversion.cutOffZeros(salesDocument.getTechKey().getIdAsString()));

			//set sales doc number
			importParams.setValue(
				salesDocument.getTechKey().getIdAsString(),
				"SALESDOCUMENT");
			cat.logT(
				Severity.DEBUG,
				loc,
				"the document ID for change is "
					+ salesDocument.getTechKey().getIdAsString());
			importParams.setValue(simulate, "SIMULATION");
			importParams.setValue(X, "CALL_FROM_BAPI");

			cat.logT(
				Severity.DEBUG,
				loc,
				"set sales doc number to : " + salesDocument.getTechKey());

			//get structure that holds the header data
			JCO.Structure headerData =
				sdSalesDocumentChange.getImportParameterList().getStructure(
					"ORDER_HEADER_IN");

			//now fill header parameters
			headerData.setValue("", fieldName);

			//get structure that holds the header change flags i.e. here we
			//determine which fields have to be changed on header level
			JCO.Structure headerXData =
				sdSalesDocumentChange.getImportParameterList().getStructure(
					"ORDER_HEADER_INX");

			//now fill those flags
			headerXData.setValue(U, RFCConstants.RfcField.UPDATEFLAG);
			headerXData.setValue(X, fieldName);
			//fire RFC. Synchronization because after excuting the change RFC,
			//the document has to be locked again, the change RFC removes all
			//locks
			String key =
				salesDocument.getTechKey().getIdAsString()
					+ backendData.getConfigKey();
			if (key == null) { // try to avoid the exception
				cat.logT(
					Severity.WARNING,
					loc,
					"Something is not right, the config Key is null");
				key = "XXX";
			}
			documentKeys.put(key, X);

			synchronized (documentKeys.get(key)) {
				connection.execute(sdSalesDocumentChange);

				//directly after executing, get the lock because the sales document change
				//bapi resets all locks
				//enqueueDocument(salesDocument, connection);
			}

			documentKeys.remove(key);

			JCO.Table retTable = sdSalesDocumentChange.getTableParameterList().getTable("RETURN");
		
			BackendUserException buExc = new BackendUserException();
			boolean hasError = false;
			if (retTable.getNumRows() > 0) {
				retTable.firstRow();

				do {
					// if there
					String msg = retTable.getString("MESSAGE");
					if(retTable.getString("TYPE").equalsIgnoreCase("E")){
						hasError = true;
						cat.logT(Severity.ERROR, loc, 
								"error message type: " + retTable.getString("TYPE"));
						cat.logT(Severity.ERROR, loc, 
								"error message number: "
									+ retTable.getString("NUMBER"));
						cat.logT(Severity.ERROR, loc, 
								"error message is " + msg);
						buExc.addErrorMessage(msg);
					
					}else{

						cat.logT(Severity.DEBUG, loc, 
									"error message type: " + retTable.getString("TYPE"));
						cat.logT(Severity.DEBUG, loc, 
									"error message number: "
										+ retTable.getString("NUMBER"));
						cat.logT(Severity.DEBUG, loc, 
									"error message is " + msg);
						buExc.addWarningMessage(msg);
					}
				} while (retTable.nextRow());
			}

			if(hasError){
				throw buExc;		
			}
		
			ReturnValue retVal = new ReturnValue(retTable, "");
		
			if (!simulate.equals(X)) {

				try {
					commit(connection, X);
				} catch (Exception e) {
					cat.logT(
						Severity.ERROR,
						loc,
						"During the commit of the transaction");
					throw new BackendException(e.getMessage());
				}
			}

			cat.logT(Severity.DEBUG, loc, "End of the releaseXXXStatus");
			return retVal;
		}

		/**
		 * Sets the partner information for changing a sales document. Three JCO tables of
		 * SD_SALESDOCUMENT_CHANGE are involved.
		 * <br>
		 * See also <a href="{@docRoot}/isacorer3/salesdocument/shiptohandling.html"> Ship-to handling in ISA R/3 </a>.     
		 * 
		 * @param partnerTable           holding partner information for newly created
		 *        partners
		 * @param partnerChangesTable    indicates which partners to be changed
		 * @param partnerAddressesTable  the partner addresses
		 * @param salesDocument          the ISA sales document
		 * @param connection             connection to the R/3 backend
		 * @param r3Items                the items that already exist in R/3
		 * @exception BackendException   backend exception
		 */
		protected void setPartnerInfoForChange(
			JCO.Table partnerTable,
			JCO.Table partnerChangesTable,
			JCO.Table partnerAddressesTable,
			SalesDocumentData salesDocument,
			ItemListData r3Items,
			JCoConnection connection)
			throws BackendException {

			cat.logT(Severity.DEBUG, loc, "setPartnerInfoForChange");

			//first: header shipTo
			ShipToData headerShipTo = salesDocument.getHeaderData().getShipToData();
			partnerChangesTable.appendRow();
			partnerChangesTable.setValue(
				salesDocument.getTechKey().getIdAsString(),
				"DOCUMENT");
			partnerChangesTable.setValue(
				RFCConstants.ITEM_NUMBER_EMPTY,
				RFCConstants.RfcField.ITM_NUMBER);
			partnerChangesTable.setValue(U, RFCConstants.RfcField.UPDATEFLAG);
			partnerChangesTable.setValue(
				RFCConstants.ROLE_SHIPTO,
				RFCConstants.RfcField.PARTN_ROLE);
			partnerChangesTable.setValue(headerShipTo.getId(), "P_NUMB_NEW");

			cat.logT(
				Severity.DEBUG,
				loc,
				"header shipTo set with parameters (document/id/key): "
					+ salesDocument.getTechKey().getIdAsString()
					+ ", "
					+ headerShipTo.getId()
					+ ", "
					+ headerShipTo.getTechKey());

			//was the header ship to manually changed
			//or is it an address that has been created manually before??
			//		if (headerShipTo.getTechKey().getIdAsString().length()
			//			<= shipToKeyIsFromManuallyEnteredOne) {

			cat.logT(Severity.DEBUG, loc, "header has manual address");

			partnerChangesTable.setValue(
				headerShipTo.getTechKey().getIdAsString(),
				"ADDR_LINK");
			partnerAddressesTable.appendRow();
			partnerAddressesTable.setValue(
				headerShipTo.getTechKey().getIdAsString(),
				"ADDR_NO");
			if (headerShipTo.getAddressData() != null){	
				writeStrategy.setAddressData(
					partnerAddressesTable,
					headerShipTo.getAddressData(),
					connection);
			}else{
				cat.logT(Severity.WARNING, loc, "ShipTo does not " +					"have valid manual address, it is NULL");
			}
			
			//		} else {

			//			cat.logT(Severity.DEBUG, loc, "header has no manual address");

			//this does not work in R/3, so create manual addresses in this case
			//partnerChangesTable.setValue(getAddressKeyFromShipToKey(headerShipTo.getTechKey().getIdAsString()), "ADDRESS");
			//		}

			//item shiptos

			for (int i = 0; i < salesDocument.getItemListData().size(); i++) {
				cat.logT(Severity.DEBUG, loc, "At sales document item level");
				ItemData item = salesDocument.getItemListData().getItemData(i);
				TechKey parent = item.getParentId();
				String itemNum = item.getTechKey().getIdAsString();
				cat.logT(Severity.DEBUG, loc, "item number is " + itemNum);
				updatePartnerAddress(
					RFCConstants.ROLE_SHIPTO,
					itemNum,
					partnerTable,
					partnerChangesTable,
					partnerAddressesTable,
					salesDocument,
					r3Items,
					connection);
//				if (parent == null || parent.isInitial()) {
//
//					String itemKey = item.getTechKey().getIdAsString();
//					ShipToData shipTo = item.getShipToData();
//
//					//first: if the item exists in R/3, append records to tables partnerChangesTable
//					//and partnerAddressesTable
//					if (r3Items != null) {
//						if (r3Items.getItemData(item.getTechKey()) != null) {
//							cat.logT(Severity.DEBUG, loc, "R/3 shipto info:");
//
//							ItemData oldItem =
//								r3Items.getItemData(item.getTechKey());
//							cat.logT(
//								Severity.DEBUG,
//								loc,
//								"shipTo on R/3 item: " + oldItem.getShipToData());
//
//							if (oldItem.getShipToData() != null)
//								cat.logT(
//									Severity.DEBUG,
//									loc,
//									"id: " + oldItem.getShipToData().getId());
//
//							if (shipTo != null && (!shipTo.equals(headerShipTo))) {
//								cat.logT(
//									Severity.DEBUG,
//									loc,
//									"processing item with shipTo key/ shipTo id: "
//										+ itemKey
//										+ ", "
//										+ shipTo.getTechKey()
//										+ ", "
//										+ shipTo.getId());
//
//								//no need to change anything in R/3 here, because if 
//								//an item ship-to has been changed, it will be marked as
//								//manually changed item
//
//								//was the item ship to manually changed?
//								if (shipTo.getTechKey().getIdAsString().length()
//									<= shipToKeyIsFromManuallyEnteredOne) {
//
//									partnerChangesTable.appendRow();
//									partnerChangesTable.setValue(
//										salesDocument.getTechKey().getIdAsString(),
//										"DOCUMENT");
//									partnerChangesTable.setValue(
//										itemKey,
//										RFCConstants.RfcField.ITM_NUMBER);
//									partnerChangesTable.setValue(
//										U,
//										RFCConstants.RfcField.UPDATEFLAG);
//									partnerChangesTable.setValue(
//										RFCConstants.ROLE_SHIPTO,
//										RFCConstants.RfcField.PARTN_ROLE);
//									partnerChangesTable.setValue(
//										shipTo.getId(),
//										"P_NUMB_NEW");
//
//									cat.logT(
//										Severity.DEBUG,
//										loc,
//										"item has manual address");
//
//									partnerChangesTable.setValue(
//										shipTo.getTechKey().getIdAsString(),
//										"ADDR_LINK");
//									partnerAddressesTable.appendRow();
//									partnerAddressesTable.setValue(
//										shipTo.getTechKey().getIdAsString(),
//										"ADDR_NO");
//									writeStrategy.setAddressData(
//										partnerAddressesTable,
//										shipTo.getAddressData(),
//										connection);
//								} else {
//									cat.logT(
//										Severity.DEBUG,
//										loc,
//										"item has no manual address or item ship to needs not to be updated");
//
//									//not necessary, this does not work in R/3
//									//partnerChangesTable.setValue(getAddressKeyFromShipToKey(shipTo.getTechKey().getIdAsString()), "ADDRESS");
//								}
//							} else
//								cat.logT(
//									Severity.DEBUG,
//									loc,
//									"no shipTo for : " + itemKey);
//						}
//
//						// the item does not exist in R/3, just use partnertable
//						else {
//
//							cat.logT(Severity.DEBUG, loc, "item new in R/3");
//
//							if ((shipTo != null)
//								&& (!shipTo.equals(headerShipTo))) {
//								partnerTable.appendRow();
//								partnerTable.setValue(
//									RFCConstants.ROLE_SHIPTO,
//									RFCConstants.RfcField.PARTN_ROLE);
//								partnerTable.setValue(
//									shipTo.getId(),
//									RFCConstants.RfcField.PARTN_NUMB);
//								partnerTable.setValue(
//									itemKey,
//									RFCConstants.RfcField.ITM_NUMBER);
//
//								if (shipTo.getTechKey().getIdAsString().length()
//									<= shipToKeyIsFromManuallyEnteredOne) {
//
//									cat.logT(
//										Severity.DEBUG,
//										loc,
//										"manually entered address for new item");
//
//									partnerTable.setValue(
//										shipTo.getTechKey().getIdAsString(),
//										"ADDR_LINK");
//									partnerAddressesTable.appendRow();
//									partnerAddressesTable.setValue(
//										shipTo.getTechKey().getIdAsString(),
//										"ADDR_NO");
//									writeStrategy.setAddressData(
//										partnerAddressesTable,
//										shipTo.getAddressData(),
//										connection);
//								}
//							} else {
//
//								cat.logT(
//									Severity.DEBUG,
//									loc,
//									"shipTo hasn't been touched");
//							}
//						}
//					}
//				}else{
//					cat.logT(Severity.DEBUG, loc, "Set to all partners " +
//						"for auction case item level");
//					try {
//						updatePartnerAddress(
//							RFCConstants.ROLE_SHIPTO,
//							itemNum,
//							partnerTable,
//							partnerChangesTable,
//							partnerAddressesTable,
//							salesDocument,
//							r3Items,
//							connection);
//
//					} catch (Exception ex) {
//						cat.logT(
//							Severity.ERROR,
//							loc,
//							"updatePartnerAddress for soldTo"
//								+ "Payer and BillTo failed at item level"
//								+ ex.getMessage());
//					}
//			    		
//				}
				cat.logT(Severity.DEBUG, loc, "partnertable" + partnerTable);
				cat.logT(Severity.DEBUG, loc, "partnerChangetable" + partnerChangesTable);
				cat.logT(Severity.DEBUG, loc, "partnerAddressTable" + partnerAddressesTable);
				cat.logT(Severity.DEBUG, loc, "End at sales document item level");
			}
		}

		/**
		 * Updates a document in the backend storage. If an R/3 error occurs and it is known
		 * (see
		 * <code>com.sapmarkets.isa.backend.r3.salesdocument.rfc.ReadStrategyR3.setUpMessageMap()</code>)
		 * it will be attached to the sales document, otherwise a backend exception will be
		 * raised. The R/3 error and warning messages are returned. Function module
		 * SD_SALESDOCUMENT_CHANGE is used.
		 * 
		 * @param documentR3            the r3 sales document. Used for getting the document
		 *        status before the last update and for getting the R/3 document status. The
		 *        R/3 document status is important because only with it we can tell which
		 *        fields to set into the R/3 RFC change call (SD_SALESDOCUMENT_CHANGE).
		 * @param salesDocument         the ISA sales document
		 * @param simulate              'X': call the RFC in simulate-mode. When updating the
		 *        document, the function module is simulated; when saving, it is called in
		 *        non-simulate mode and a commit is performed
		 * @param connection            JCO connection
		 * @param serviceR3IPC          the service object that deals with IPC in when
		 *        running ISA R/3
		 * @return R/3 messages
		 * @exception BackendException  exception from backend
		 */
		public ReturnValue changeDocument(
			SalesDocumentR3 documentR3,
			SalesDocumentData salesDocument,
			String simulate,
			JCoConnection connection,
			ServiceR3IPC serviceR3IPC)
			throws BackendException {

			cat.logT(Severity.DEBUG, loc, "changeDocument start");

			//first do the validation of dates and quantities
			if (!validateInput(salesDocument, connection))
				return null;

			boolean hasError = false;
			//if validation was OK: fill RFC input parameters
			//get jayco function that refers to the change bapi
			JCO.Function sdSalesDocumentChange =
				connection.getJCoFunction(
					RFCConstants.RfcName.SD_SALESDOCUMENT_CHANGE);
			JCO.ParameterList importParams =
				sdSalesDocumentChange.getImportParameterList();
			JCO.ParameterList tableParams =
				sdSalesDocumentChange.getTableParameterList();

			//sales doc number into order header
			salesDocument.getHeaderData().setSalesDocNumber(
				Conversion.cutOffZeros(salesDocument.getTechKey().getIdAsString()));

			//set sales doc number
			importParams.setValue(
				salesDocument.getTechKey().getIdAsString(),
				"SALESDOCUMENT");
			importParams.setValue(simulate, "SIMULATION");
			importParams.setValue(X, "CALL_FROM_BAPI");

			importParams.setValue(X, "INT_NUMBER_ASSIGNMENT");

			cat.logT(
				Severity.DEBUG,
				loc,
				"set sales doc number to : " + salesDocument.getTechKey());

			//get structure that holds the header data
			JCO.Structure headerData =
				sdSalesDocumentChange.getImportParameterList().getStructure(
					"ORDER_HEADER_IN");

			//now fill header parameters
			//		headerData.setValue(
			//			salesDocument.getHeaderData().getPurchaseOrderExt(),
			//			"PURCH_NO_C");

			//shipping conditions cannot be changed since
			//the schedule lines might be re-determined which might cause
			//problems for quantity update
			//headerData.setValue(salesDocument.getHeaderData().getShipCond(), 
			//                    "SHIP_COND");

			headerData.setValue(
				Conversion.uIDateStringToDate(
					salesDocument.getHeaderData().getReqDeliveryDate(),
					backendData.getLocale()),
				"REQ_DATE_H");

			//get structure that holds the header change flags i.e. here we
			//determine which fields have to be changed on header level
			JCO.Structure headerXData =
				sdSalesDocumentChange.getImportParameterList().getStructure(
					"ORDER_HEADER_INX");

			//now fill those flags
			headerXData.setValue(U, RFCConstants.RfcField.UPDATEFLAG);
			//		headerXData.setValue(X, "PURCH_NO_C");
			//headerXData.setValue(X, 
			//                     "SHIP_COND");

			headerXData.setValue(X, "REQ_DATE_H");

			//retrieve item tables and set delta flags
			JCO.Table itemTable = tableParams.getTable("ITEM_IN");
			JCO.Table itemXTable = tableParams.getTable("ITEM_INX");

			//retrieve schedule tables and set delta flags
			JCO.Table scheduleTable = tableParams.getTable("SCHEDULE_IN");
			JCO.Table scheduleXTable = tableParams.getTable("SCHEDULE_INX");

			//set items and schedule lines
			writeStrategy.setItemInfo(
				itemTable,
				itemXTable,
				scheduleTable,
				scheduleXTable,
				salesDocument,
				documentR3,
				connection);

			//change the order partners
			if (salesDocument instanceof AuctionOrder
				&& ((AuctionOrder) salesDocument).isUpdateAddress())
				setPartnerInfoForChange(
					tableParams.getTable("PARTNERS"),
					tableParams.getTable("PARTNERCHANGES"),
					tableParams.getTable("PARTNERADDRESSES"),
					salesDocument,
					documentR3.getDocumentR3Status().getItemList(),
					connection);

			//set price change information
			if (salesDocument instanceof AuctionOrder) {
				((AuctionWriteStrategy) writeStrategy).setPriceChangeInfo(
					tableParams.getTable("CONDITIONS_IN"),
					tableParams.getTable("CONDITIONS_INX"),
					salesDocument);
			}
			//now set texts into the RFC table
			writeStrategy.setTexts(
				salesDocument,
				tableParams.getTable("SALES_TEXT"));

			//set the extensions
			Extension extension =
				getExtension(
					tableParams.getTable("EXTENSIONIN"),
					salesDocument,
					backendData,
					ExtensionParameters.EXTENSION_DOCUMENT_CHANGE);
			//		extension.documentToTable();

			//call customer extensions
			performCustExitBeforeR3Call(salesDocument, sdSalesDocumentChange);

			//fire RFC. Synchronization because after excuting the change RFC,
			//the document has to be locked again, the change RFC removes all
			//locks
			String key =
				salesDocument.getTechKey().getIdAsString()
					+ backendData.getConfigKey();
			documentKeys.put(key, X);

			synchronized (documentKeys.get(key)) {
				connection.execute(sdSalesDocumentChange);

				//directly after executing, get the lock because the sales document change
				//bapi resets all locks
				//			enqueueDocument(salesDocument, 
				//							connection);
			}

			documentKeys.remove(key);
			JCO.Table retTable =
				sdSalesDocumentChange.getTableParameterList().getTable("RETURN");
			BackendUserException buExc = new BackendUserException();
			if (retTable.getNumRows() > 0) {
				retTable.firstRow();
				do {
					cat.logT(
						Severity.DEBUG,
						loc,
						"error message type: " + retTable.getString("TYPE"));
					cat.logT(
						Severity.DEBUG,
						loc,
						"error message number: " + retTable.getString("NUMBER"));
					String msg = retTable.getString("MESSAGE");
					cat.logT(Severity.DEBUG, loc, "error message is " + msg);
					if (retTable.getString("TYPE").equalsIgnoreCase("E")) {
						hasError = true;
						buExc.addErrorMessage(msg);
					} else
						buExc.addWarningMessage(msg);

				} while (retTable.nextRow());
				cat.logT(
					Severity.DEBUG,
					loc,
					"Added by fy: recTable = " + retTable);
			}
			ReturnValue retVal =
				new ReturnValue(
					sdSalesDocumentChange.getTableParameterList().getTable(
						"RETURN"),
					"");
			cat.logT(Severity.DEBUG, loc, "HERE is the return message" + retVal);
			//check if the RFC has been called successfully
			//		if (!readStrategy.addSelectedMessagesToBusinessObject(
			//					 retVal, 
			//					 salesDocument)) {

			//check the simulate flag. If it was simulation, re-read the data
			//from the RFC, if it was create, a commit has to be called
			if (hasError) {
				throw buExc;
			}
			if (!simulate.equals(X)) {

				try {
					commit(connection, X);
				} catch (Exception e) {
					throw new BackendException(e.getMessage());
				}
			} else {

				//now get the new items into the sales document
				JCO.Table itemExTable =
					sdSalesDocumentChange.getTableParameterList().getTable(
						"ITEMS_EX");
				JCO.Table scheduleExTable =
					sdSalesDocumentChange.getTableParameterList().getTable(
						"SCHEDULE_EX");

				cat.logT(
					Severity.DEBUG,
					loc,
					"returned items: " + itemExTable.getNumRows());
				cat.logT(
					Severity.DEBUG,
					loc,
					"returned schedule lines: " + scheduleExTable.getNumRows());
				try {

					//re-read the items
					readStrategy.getItemsAndScheduleLines(
						salesDocument,
						documentR3,
						itemExTable,
						scheduleExTable,
						null,
						null,
						connection);

					//re-read the prices
					readStrategy.getPricesFromDetailedList(
						salesDocument.getHeaderData(),
						salesDocument.getItemListData(),
						itemExTable,
						0,
						false,
						connection);

					//check if a newly created material is configurable. In this case, create an IPC item
					setConfigAttributes(salesDocument, documentR3, connection);

					//re-read the header
					readStrategy.getHeader(
						salesDocument.getHeaderData(),
						sdSalesDocumentChange
							.getExportParameterList()
							.getStructure(
							"SALES_HEADER_OUT"));

					//re-assign the item keys and numbers, apply
					//the R/3 numbers
					setItemKeysAfterR3Call(salesDocument, itemExTable);

					//read the extensions from R3
					//set the extensions
					extension =
						getExtension(
							tableParams.getTable("EXTENSIONEX"),
							salesDocument,
							backendData,
							ExtensionParameters.EXTENSION_DOCUMENT_CHANGE);
					//extension.tableToDocument();

					//call customer extensions
					performCustExitAfterR3Call(
						salesDocument,
						sdSalesDocumentChange);
				} catch (Exception ex) {

					cat.logT(Severity.DEBUG, loc, "exception catched: " + ex);

					throw new BackendException(ex.getMessage());
				}
			}
			//		}
			//		 else {

			//the message will be displayed on the UI. No exception will be thrown
			retVal =
				new ReturnValue(
					sdSalesDocumentChange.getTableParameterList().getTable(
						"RETURN"),
					RFCConstants.BAPI_RETURN_ERROR);

			cat.logT(
				Severity.DEBUG,
				loc,
				"message list size: " + salesDocument.getMessageList().size());
			//}

			return retVal;
		}

		/**
		 * @param settings
		 */
		public void setBackendSettings(BackendDefaultSettings settings) {
			backendSettings = settings;
		}

		private void updatePartnerAddress(
			String roleName,
			String itemNumber,
			JCO.Table partnerTable,
			JCO.Table partnerChangesTable,
			JCO.Table partnerAddressesTable,
			SalesDocumentData salesDocument,
			ItemListData r3Items,
			JCoConnection connection)
			throws BackendException {
			//first: header shipTo
			ShipToData headerShipTo = salesDocument.getHeaderData().getShipToData();
			String soldToR3Key =
				RFCWrapperPreBase.trimZeros10(
					salesDocument
						.getHeaderData()
						.getPartnerListData()
						.getSoldToData()
						.getPartnerTechKey()
						.getIdAsString());

			String shipToR3Key =
				RFCWrapperPreBase.trimZeros10(headerShipTo.getId());
			if ((soldToR3Key == null) || (soldToR3Key.length() <= 1)) {
				soldToR3Key = shipToR3Key;
			}

			partnerChangesTable.appendRow();
			partnerChangesTable.setValue(
				salesDocument.getTechKey().getIdAsString(),
				"DOCUMENT");
			partnerChangesTable.setValue(
				itemNumber,
				RFCConstants.RfcField.ITM_NUMBER);
			partnerChangesTable.setValue(U, RFCConstants.RfcField.UPDATEFLAG);
			partnerChangesTable.setValue(
				roleName,
				RFCConstants.RfcField.PARTN_ROLE);
			partnerChangesTable.setValue(soldToR3Key, "P_NUMB_NEW");

			cat.logT(
				Severity.DEBUG,
				loc,
				"header soldTo set with parameters (document/id/key): "
					+ salesDocument.getTechKey().getIdAsString()
					+ ", "
					+ soldToR3Key);

			cat.logT(Severity.DEBUG, loc, "header has manual address");

			partnerChangesTable.setValue(soldToR3Key, "ADDR_LINK");

			partnerAddressesTable.appendRow();
			partnerAddressesTable.setValue(soldToR3Key, "ADDR_NO");
			if (headerShipTo.getAddressData() != null){	
				writeStrategy.setAddressData(
					partnerAddressesTable,
					headerShipTo.getAddressData(),
					connection);
			}else{
			cat.logT(Severity.WARNING, loc, "ShipTo does not " +
				"have valid manual address, it is NULL");
			}

		}

}
