/*
 * Title:        eBay Integration Project
 * Description:	 * SAP Labs LLC, the eBay auction project, also is known as Selling via eBay. 
 * Selling via eBay application provides SAP R/3 (and in the future CRM) 
 * customers to auction and sell inventory/products onto an online marketplace 
 * eBay. The application will provide the ability to Create, Schedule, Publish, 
 * Monitor and Close Auction Listings onto eBay and handle all the relevant 
 * Backend processing for Customers and Orders. 

 * Copyright:    Copyright (c) 2003
 * Company:      SAP Labs LLC
 * @author
 * @version 1.0
 */
package com.sap.isa.auction.backend.r3.salesdocument.strategy;

import com.sap.mw.jco.JCO;
import com.sap.isa.auction.backend.boi.order.AuctionOrderData;
import com.sap.isa.backend.r3.salesdocument.rfc.ReadStrategy;
import com.sap.isa.core.eai.BackendException;
import com.sap.isa.core.eai.sp.jco.JCoConnection;
/*
 */
public interface AuctionReadStrategy extends ReadStrategy {

	/**
	 * Fills the ISA header prices.
	 * 
	 * @param orderHeader                the ISA sales document header
	 * @param conditionTable                  the RFC table the backend has provided
	 * @param decimalPlaceToShift        how many decimal places do we have to shift for
	 *        the net price and freight? This depends on the R/3 data type
	 * @param connection                 JCO connection
	 * @param determineNetPrViaDivision  Is the net price of the item calculated via
	 *        dividing the net value with the quantity? For small R/3 releases like
	 *        4.0b, it has to be done this way. In the other case, the net price is read
	 *        from the RFC directly and the base unit can differ
	 * @exception BackendException
	 */
	public void getPricesFromHeaderConditions(AuctionOrderData orderHeader, 
										  JCO.Table conditionTable, 
										  int decimalPlaceToShift, 
										  boolean determineNetPrViaDivision, 
										  JCoConnection connection)
								   throws BackendException;

}
