/*****************************************************************************
    Copyright (c) 2001, SAPMarkets Palo Alto, USA, All rights reserved.

*****************************************************************************/



package com.sap.isa.auction.backend.r3.salesdocument;

import java.util.Locale;
import java.util.Properties;

import com.sap.tc.logging.Category;
import com.sap.tc.logging.Location;
import com.sap.tc.logging.Severity;
import com.sap.isa.auction.backend.boi.order.AuctionOrderBackend;
import com.sap.isa.auction.backend.boi.order.AuctionOrderData;
import com.sap.isa.auction.backend.r3.AuctionRFCConstantsR3;
import com.sap.isa.auction.backend.r3.salesdocument.strategy.AuctionCreateStrategyR3;
import com.sap.isa.backend.IsaBackendBusinessObjectBaseSAP;
import com.sap.isa.backend.boi.isacore.AddressData;
import com.sap.isa.backend.boi.isacore.SalesDocumentData;
import com.sap.isa.backend.boi.isacore.order.ItemData;
import com.sap.isa.backend.r3.salesdocument.BasketServiceR3;
import com.sap.isa.backend.r3.salesdocument.OrderR3;
import com.sap.isa.core.eai.BackendBusinessObjectParams;
import com.sap.isa.core.eai.BackendException;
import com.sap.isa.core.eai.sp.jco.JCoConnection;
//import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.auction.backend.r3.salesdocument.strategy.*;
import com.sap.isa.auction.bean.BackendDefaultSettings;
import com.sap.isa.auction.logging.CategoryProvider;
import com.sap.isa.backend.r3base.ipc.ServiceR3IPC;
import com.sap.isa.backend.r3base.util.R3BackendData;
import com.sap.isa.backend.r3.salesdocument.rfc.DetailStrategyR3;
import com.sap.isa.backend.r3.salesdocument.rfc.ReadStrategyR3;
/**
 * This class is the extension of the ordeR3 class for the auction purpose
 * we do following things, this class is not used in B2C checkout scenario
 * 1. set the sales emplo
 * yee and other partner functions. 
 * 2. Set the price information from eBay
 * 3. Set shipping address, if the one time customer is used
 * 5. 
 * 
 **/
public class AuctionOrderR3 extends OrderR3 implements AuctionOrderBackend, AuctionBackendBase{
    /** The logging instance. */
//	private static IsaLocation log = IsaLocation.getInstance(
//											 AuctionOrderR3.class.getName());

  private final static Category cat = CategoryProvider.getSystemCategory();
  private final static Location loc =
	  Location.getLocation(AuctionOrderR3.class);
		
  /** The backend properties are read within initBackendObject and later on used. */
  protected Properties props;
  /**
   * Initializes Business Object.
   * 
   * @param props                 a set of properties which may be useful to initialize
   *        the object
   * @param params                a object which wraps parameters
   * @exception BackendException  Description of Exception
   */
  public void initBackendObject(Properties property, 
								BackendBusinessObjectParams params)
						 throws BackendException {
		
//	  if(log.isDebugEnabled()){
//		  log.debug("In the AuctionOrderR3 initlization of BackenObject");
//	  }
	  cat.logT(Severity.DEBUG, loc, "In the AuctionOrderR3 initlization of BackenObject");
	  R3BackendData backendData = getOrCreateBackendData();
	  //This block is for the spoof the NullPoint error, in case it happens
	  if (backendData.getLocale() == null){
		  Locale locale = Locale.US;
		  backendData.setLocale(locale);
	  }
	  // those data should be passed from BackendSettingBase, in case
	  // of this object was not initionlized right. Here is the sproof,
	  // in order to avoid some exceptions.
	  // It should be removed later
	  if(backendData.getR3Release() == null)
		  backendData.setR3Release("");
	  if(backendData.getBackend() == null)
		  backendData.setBackend("");
	  if(backendData.getSubTotalFreight() == null)	
		  backendData.setSubTotalFreight("4");
	  if (getContext().getAttribute(
			  IsaBackendBusinessObjectBaseSAP.ISA_LANGUAGE) == null){
		  getContext().setAttribute(
			  IsaBackendBusinessObjectBaseSAP.ISA_LANGUAGE,
			  "EN");
	  }
	  backendData.setLanguage((String)getContext().getAttribute(
					  IsaBackendBusinessObjectBaseSAP.ISA_LANGUAGE));
	  readStrategy = new ReadStrategyR3(backendData);
	  writeStrategy = new AuctionWriteStrategyR3(backendData, readStrategy);
	  createStrategy = new AuctionCreateStrategyR3(backendData, 
											writeStrategy, 
											readStrategy);
	  detailStrategy = new DetailStrategyR3(backendData, 
										  readStrategy);
	  changeStrategy = new AuctionChangeStrategyR3(backendData,
												  writeStrategy,
												  readStrategy);
	  statusStrategy = new AuctionStatusStrategyR3(backendData , readStrategy);
	  this.props = property;
	  ((AuctionCreateStrategyR3)createStrategy).setProps(props);
	  ((AuctionWriteStrategyR3)writeStrategy).setProps(props);
	  cat.logT(Severity.DEBUG, loc, "-------The passing in property is----- ");
	  cat.logT(Severity.DEBUG, loc, "backendData language is " +
									  backendData.getLanguage());
	  cat.logT(Severity.DEBUG, loc, 
							  "-------------------------------------- ");

			                                             
  }

  public void setBackendSettings(BackendDefaultSettings settings)
  {
	  setStrategyBackendSettings(writeStrategy, settings);
	  setStrategyBackendSettings(createStrategy, settings);
	  setStrategyBackendSettings(changeStrategy, settings);
  }
	
  private void setStrategyBackendSettings(Object strategy, BackendDefaultSettings settings)
  {
	  if(strategy != null && strategy instanceof AuctionStrategyBase)
		  ((AuctionStrategyBase)strategy).setBackendSettings(settings);
  }
	
  /**
	* Create an order in the backend from a quotation (incl. save in backend)
	* the responsible employee is included in the quotationdata
	* @param quotationId, the quotation ID not 
	* TechKey, it is used to link the quotation with
	* the sales document which is copied from,here the implementaion
	* is quite different with ISA implementation.
	* @param orderdata, includes the order type, sales area, and the the 
	* winning price
	* @param newShipToAddress		The shipping address passed in
	*/
   public void createFromQuotation(
			  String quotationId,
			  AuctionOrderData order,
			  AddressData newShipToAddress 
		   ) throws BackendException{
	  // winner data 
	  cat.logT(Severity.DEBUG, loc, "createFromQuotation start");
	  JCoConnection aJCoCon = null;
	  
	try {
		  //get connection to backend
		  aJCoCon = getDefaultJCoConnection();
		
		  //copy the document
		  ((AuctionCreateStrategyR3)createStrategy).copyDocument(quotationId, 
									  order, 
									  AuctionRFCConstantsR3.TRANSACTION_TYPE_ORDER,
									  newShipToAddress, 
									  aJCoCon);
	} finally {
		if(aJCoCon != null)
			aJCoCon.close();
	}
	
	  cat.logT(Severity.DEBUG, loc, "createFromQuotation End");
  }
	public void cancelInBackend(SalesDocumentData order)throws BackendException {
		cat.logT(Severity.DEBUG, loc, 
				"In AuctionOrderR3, "
					+ "		trying to cancel the order");
		JCoConnection aJCoCon = null;

		try {
			setDirtyForInternalStorage(true);
			readFromBackend((AuctionOrderData)order);
			
			aJCoCon = getDefaultJCoConnection();
			((AuctionChangeStrategy)changeStrategy).changeStatus(order,
												 "", aJCoCon);
			
			//now set the flag that indicates that the order has to be re-read from
			//backend. Later on we call a bapi that allows to re-read the order
			//directly after creating it
			setDirtyForInternalStorage(true);
		} finally {
			if(aJCoCon != null)
				aJCoCon.close();
		}

		cat.logT(Severity.DEBUG, loc, "status change has been performed");
	}
  /* 
   * release the delivery block from the sales order
   * @param	The sales document number is used
   * @throws BackendException
   */
  public void releaseDeliveryBlock(SalesDocumentData order) throws BackendException {
	  // winner data 
	  cat.logT(Severity.DEBUG, loc, "releaseDeliveryBlock start");
	  JCoConnection aJCoCon = null;

	try {
		  //get connection to backend
		  aJCoCon = getDefaultJCoConnection();
		
		  //copy the document
		  ((AuctionChangeStrategyR3)changeStrategy).releaseDeliveryBlock( 
									  order, 
									  "",
									  aJCoCon);
	} finally {
		if(aJCoCon != null)
			aJCoCon.close();
	}
	
	  cat.logT(Severity.DEBUG, loc, "releaseDeliveryBlock End");
		
  }

  /* 
   * release the billing block from the sales order
   * @param	The sales document number is used
   * @throws BackendException
   */
  public void releaseBillingBlock(SalesDocumentData order) throws BackendException {
	  // winner data 
	  cat.logT(Severity.DEBUG, loc, "releaseBillingBlock start");
	  JCoConnection aJCoCon = null;

	try {
		  //get connection to backend
		  aJCoCon = getDefaultJCoConnection();
		
		  //copy the document
		  ((AuctionChangeStrategyR3)changeStrategy).releaseBillingBlock( 
									  order, 
									  "",
									  aJCoCon);
	} finally {
		if(aJCoCon != null)
			aJCoCon.close();
	}
	  
	  cat.logT(Severity.DEBUG, loc, "releaseBillingBlock End");
		
  }
  /**
	 * Read the ship-tos associated with the user into the document.
	 * 
	 * @param ordr                  The document to read the data in
	 * @param user                  The user to get the shiptos for
	 * @exception BackendException  Exception from backend
	 */
	public void readShipTosFromBackend(SalesDocumentData ordr)
								throws BackendException {
		if (ordr.getHeaderData().getPartnerListData() != null &&
			ordr.getHeaderData().getPartnerListData().getList().size()>0)
			super.readShipTosFromBackend(ordr);
		else
			this.readFromBackend(ordr);
	}

  /**
   * Read the whole document from R/3.
   * 
   * @param posd                  the sales document that gets the data including
   *        header, items, deliveries, schedule lines.
   * @exception BackendException exception from R/3
   */
  protected void getDetailFromBackend(SalesDocumentData posd)
							   throws BackendException {

	  cat.logT(Severity.DEBUG, loc, "getDetailFromBackend(SalesdocumentData posd) with strategy");
	  cat.logT(Severity.DEBUG, loc, "sales document: " + posd.getTechKey());
	  cat.logT(Severity.DEBUG, loc, "sales document header : " + posd.getHeaderData().getTechKey());
	  JCoConnection aJCoCon = null;

	try {
		  aJCoCon = getDefaultJCoConnection();
			    
		  //in 5.0, no need to reset the connection
		  //aJCoCon.reset();
		  //cat.logT(Severity.DEBUG, loc, "reset connection");        
		
		  ServiceR3IPC ipcService = getServiceR3IPC();
			
		
		  detailStrategy.fillDocument(posd, 
									  aJCoCon, 
									  ipcService);
		
		  //now perform the material id
		  //conversion
		  BasketServiceR3 basketService = (BasketServiceR3)getContext().getAttribute(
												  BasketServiceR3.TYPE);
		
		  for (int i = 0; i < posd.getItemListData().size(); i++) {
		
			  ItemData currentItem = posd.getItemListData().getItemData(
											 i);
			  currentItem.setProduct(basketService.convertMaterialNumber(
											 currentItem.getProductId().getIdAsString()));
		  }
		
		  //set the header and item status
		  statusStrategy.setSalesDocumentChangeStatus(posd);
		
		  //update the internal storage and mark the internal storage as clean
		  createSalesDocumentDataInternally(getDocumentFromInternalStorage(), 
											posd);
		  setDirtyForInternalStorage(false);
		
		  //set the R/3 status
		  createSalesDocumentDataInternally(getDocumentR3Status(), 
											posd);
		  setExistsInR3(true);
	} finally {
		if(aJCoCon != null)
			aJCoCon.close();
	}
  }

	/* (non-Javadoc)
	 * @see com.sap.isa.auction.backend.boi.order.AuctionOrderBackend#updateManualShippingAddress(com.sap.isa.backend.boi.isacore.SalesDocumentData, com.sap.isa.backend.boi.isacore.AddressData)
	 */
	public void updateManualShippingAddress(SalesDocumentData order, 
									AddressData address) throws BackendException {
		//throw new UnsupportedOperationException();		
	}

	/* (non-Javadoc)
	 * @see com.sap.isa.auction.backend.boi.order.AuctionOrderBackend#updateSalesDocumentPrices(com.sap.isa.auction.backend.boi.order.AuctionOrderData)
	 */
	public void updateSalesDocumentPrices(AuctionOrderData order) throws BackendException {
		//throw new UnsupportedOperationException();		
	}

	/* (non-Javadoc)
	 * @see com.sap.isa.auction.backend.boi.order.AuctionOrderBackend#updateOrderSystemStatus(com.sap.isa.backend.boi.isacore.SalesDocumentData)
	 */
	public void updateOrderSystemStatus(SalesDocumentData order) throws BackendException {
		throw new UnsupportedOperationException();		
	}

	/* (non-Javadoc)
	 * @see com.sap.isa.auction.backend.boi.order.AuctionOrderBackend#releaseOrderSystemStatus(com.sap.isa.backend.boi.isacore.SalesDocumentData)
	 */
	public void releaseOrderSystemStatus(SalesDocumentData order) throws BackendException {
		cat.log(Severity.INFO, loc, "In R3, releasing distribution block operation does nothing");
		//throw new UnsupportedOperationException();		
	}


}
