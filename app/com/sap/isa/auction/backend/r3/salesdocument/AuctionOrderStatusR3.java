/*
 * Created on Sep 16, 2003
 *
 * SAP Labs LLC, the eBay auction project, also is known as Selling via eBay. 
 * Selling via eBay application provides SAP R/3 (and in the future CRM) 
 * customers to auction and sell inventory/products onto an online marketplace 
 * eBay. The application will provide the ability to Create, Schedule, Publish, 
 * Monitor and Close Auction Listings onto eBay and handle all the relevant 
 * Backend processing for Customers and Orders. 

 */
package com.sap.isa.auction.backend.r3.salesdocument;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.Locale;
import java.util.Properties;

import com.sap.tc.logging.Category;
import com.sap.tc.logging.Location;
import com.sap.tc.logging.Severity;
import com.sap.isa.auction.backend.boi.order.AuctionDocumentListFilterData;
import com.sap.isa.auction.backend.boi.order.AuctionOrderStatusBackend;
import com.sap.isa.auction.backend.boi.order.AuctionOrderStatusData;
import com.sap.isa.auction.backend.r3.salesdocument.strategy.AuctionDetailStrategyR3;
import com.sap.isa.auction.backend.r3.salesdocument.strategy.AuctionReadStrategyR3;
import com.sap.isa.auction.backend.r3.salesdocument.strategy.AuctionStatusStrategyR3;
import com.sap.isa.auction.logging.CategoryProvider;
import com.sap.isa.backend.IsaBackendBusinessObjectBaseSAP;
import com.sap.isa.backend.boi.isacore.ShipToData;
import com.sap.isa.backend.boi.isacore.order.DocumentListFilterData;
import com.sap.isa.backend.boi.isacore.order.HeaderData;
import com.sap.isa.backend.boi.isacore.order.ItemData;
import com.sap.isa.backend.boi.isacore.order.ItemListData;
import com.sap.isa.backend.boi.isacore.order.OrderStatusData;
import com.sap.isa.backend.r3.salesdocument.OrderStatusR3;
import com.sap.isa.backend.r3.salesdocument.rfc.ReadStrategyNonXSITracking;
import com.sap.isa.backend.r3base.rfc.RFCConstants;
import com.sap.isa.backend.r3base.rfc.SalesDocumentHelpValues;
import com.sap.isa.backend.r3base.util.Conversion;
import com.sap.isa.backend.r3base.util.R3BackendData;
import com.sap.isa.core.eai.BackendBusinessObjectParams;
import com.sap.isa.core.eai.BackendException;
import com.sap.isa.core.eai.sp.jco.JCoConnection;

/**
 * @ This class retrieves all the order status for auction scenario
 * it can filtering based on auction type. The purpose for this class is:
 * 1. In auction J2EE persistent, we know all of the order IDS, we can
 *    do the filtering based on this list of order IDs
 * 2. We can search the orders by sales employees
 */
public class AuctionOrderStatusR3
	extends OrderStatusR3
	implements AuctionOrderStatusBackend {

	//		private static IsaLocation log =
	//			IsaLocation.getInstance(AuctionOrderStatusR3.class.getName());
	private final static Category cat = CategoryProvider.getSystemCategory();
	private final static Location loc =
		Location.getLocation(AuctionOrderStatusR3.class);
	
	public Locale getBackendLocale() {
		R3BackendData backendData;
		try {
			backendData = getOrCreateBackendData();
			return backendData.getLocale();
		} catch (BackendException e) {
			return Locale.getDefault();
		}
	}
	/**
	 * Initializes Business Object. Checks on the R/3 release and the plug in
	 * availability and determines the strategy classes that have to be used for
	 * accessing R/3.
	 * 
	 * @param props                 a set of properties which may be useful to initialize
	 *        the object
	 * @param params                a object which wraps parameters
	 * @exception BackendException  exception from R/3
	 */
	public void initBackendObject(
		Properties props,
		BackendBusinessObjectParams params)
		throws BackendException {
		cat.logT(Severity.DEBUG, loc, "init begin order status");
		R3BackendData backendData = getOrCreateBackendData();
		// This block spoof the nullpointer exception in any case		
		if (backendData.getLocale() == null) {
			Locale locale = Locale.US;
			backendData.setLocale(locale);
		}
		if (backendData.getR3Release() == null)
			backendData.setR3Release("");
		if (backendData.getBackend() == null)
			backendData.setBackend("");
		if (backendData.getSubTotalFreight() == null)
			backendData.setSubTotalFreight("4");
		if (getContext()
			.getAttribute(IsaBackendBusinessObjectBaseSAP.ISA_LANGUAGE)
			== null) {
			getContext().setAttribute(
				IsaBackendBusinessObjectBaseSAP.ISA_LANGUAGE,
				"EN");
		}
		//always the standard read strategy except for
		//the non-XSI tracking
		if (backendData.isTrackingOld()) {
			readStrategy = new ReadStrategyNonXSITracking(backendData);
		} else
			readStrategy = new AuctionReadStrategyR3(backendData);

		statusStrategy = new AuctionStatusStrategyR3(backendData , readStrategy);

		detailStrategy = new AuctionDetailStrategyR3(backendData, readStrategy);

	}

	/**
	 * Read an order list from the backend. we filter also on the passing order ID
	 * 
	 * @param orderList             order list that will get its data from this method.
	 *        Also holds the search criteria.
	 * @exception BackendException  exception from R/3
	 */
	public void readOrderHeaders(OrderStatusData orderStatus)
		throws BackendException {
		cat.logT(
			Severity.DEBUG,
			loc,
			"readOrderHeaders begin in AuctionOrderStatusR3");
		JCoConnection aJCoCon = null;
		
		try {
			// get JcoConnection and jayco client
			aJCoCon = getDefaultJCoConnection();
			statusStrategy.getSDHeaders(aJCoCon, orderStatus);
		} catch (BackendException ex) {
			cat.logT(
				Severity.ERROR,
				loc,
				"In Read Order Headers in Auction Order Status");
			throw ex;
		} finally {
			if(aJCoCon != null)
				aJCoCon.close();
		}
		/**** Now we should do further filtering, it should be replaced by 
		 * backend implementation****/
		//			if(log.isDebugEnabled()){
		//				Iterator it = orderStatus.iterator();
		//			}
	}

	/**
	 * Reads order detail information (header and items). Use the detail strategy to get
	 * the sales document information. Sets the status information for order header and
	 * items.  Replaces the shipping condition key with the language dependent long
	 * text.
	 * 
	 * @param orderStatus           order status (holds header and items)
	 * @exception BackendException  exception from R/3
	 */
	public void readOrderStatus(OrderStatusData orderStatus)
		throws BackendException {
		JCoConnection aJCoCon = null;
		
		try {
			String language =
				(String) getContext().getAttribute(
					IsaBackendBusinessObjectBaseSAP.ISA_LANGUAGE);
			aJCoCon = getDefaultJCoConnection();
			if (language == null) {
				language = aJCoCon.getAttributes().getISOLanguage();
				getContext().setAttribute(
					IsaBackendBusinessObjectBaseSAP.ISA_LANGUAGE,
					language);
				cat.logT(
					Severity.DEBUG,
					loc,
					"language is null, then use default value: " + language);
			}
			backendData.setLanguage(language);
			readOrderStatusSimple(orderStatus, aJCoCon);
		} catch (NullPointerException ex) {
			cat.logT(
				Severity.ERROR,
				loc,
				"Null Pointer exception in read " + "Auction Order Status");
		} finally {
			if(aJCoCon != null)
				aJCoCon.close();
		}
	}

	/**
	 * Utility method to iterate through all of the orderIDs from the 
	 * auction to get the header Data
	 * @
	 */
	private void setFilteredHeaderList(
		DocumentListFilterData filter,
		OrderStatusData orderStatus) {
		AuctionDocumentListFilterData aucFilter =
			(AuctionDocumentListFilterData) filter;
		AuctionOrderStatusData status = (AuctionOrderStatusData) orderStatus;
		// we do the filtering on the list of order IDs
		ArrayList orderIds = aucFilter.getOrderIds();
		Iterator iter = orderStatus.iterator();
		if (orderIds != null) {
			// we go through 
			if (iter != null) {
				while (iter.hasNext()) {
					HeaderData header = (HeaderData) iter.next();
					if (!(header == null)) {
						String docId = header.getSalesDocNumber();
						for (int i = 0; i < orderIds.size(); i++) {
							String Id = (String) orderIds.get(i);
							if (docId.equalsIgnoreCase(Id)) {
								status.addOrderHeaderFiltered(docId);
							}
						}
					}
				}

			}
		}
	}
	
	/**
	 * Reads order detail information (header and items). Use the detail strategy to get
	 * the sales document information. Sets the status information for order header and
	 * items.  Replaces the shipping condition key with the language dependent long
	 * text.
	 * 
	 * @param orderStatus           order status (holds header and items)
	 * @exception BackendException  exception from R/3
	 */
	protected void readOrderStatusSimple(OrderStatusData orderStatus,
										JCoConnection aJCoCon)
						 throws BackendException {

		String r3Relase = getOrCreateBackendData().getR3Release();
		boolean tracking = r3Relase.equals(RFCConstants.REL46B) || r3Relase.equals(
																		   RFCConstants.REL46C) || r3Relase.equals(
																										   RFCConstants.REL620);

		cat.logT(
					Severity.DEBUG,
					loc,"id: " + orderStatus.getTechKey());

		//now set the order status techkey into the tech key of the order header
		orderStatus.getOrder().getHeaderData().setTechKey(orderStatus.getTechKey());
		orderStatus.getOrder().getHeaderData().setSalesDocNumber(
				Conversion.cutOffZeros(orderStatus.getTechKey().getIdAsString()));
 
		//read the whole document
		detailStrategy.fillDocument(orderStatus.getOrder(), 
									aJCoCon, 
									getServiceR3IPC());
                                    
		//condense inclusive free goods, for the time being not supported
		//adjustFreeGoods(orderStatus.getOrder());						                                    	


		//transfer the document into the order status object
		orderStatus.addOrderHeader(orderStatus.getOrder().getHeaderData());
		orderStatus.setOrderHeader(orderStatus.getOrder().getHeaderData());
		orderStatus.setDocumentExist(true);

		ItemListData items = orderStatus.getOrder().getItemListData();

		for (int i = 0; i < items.size(); i++) {
			orderStatus.addItem(items.getItemData(i));
		}

		//set the statuses on header and item
		statusStrategy.setSalesDocumentChangeStatus(orderStatus.getOrder());
        
		//set the shipTo's on order status level
		ShipToData[] shipTos = new ShipToData[orderStatus.getOrder().getItemListData().size() + 1];
		shipTos[0] = orderStatus.getOrder().getHeaderData().getShipToData();

		for (int i = 0; i < orderStatus.getOrder().getItemListData().size(); i++) {
			shipTos[i + 1] = orderStatus.getOrder().getItemListData().getItemData(
									 i).getShipToData();
                                     
			//now set the change status for all items
			ItemData item =  orderStatus.getOrder().getItemListData().getItemData(i);
				
			item.setAllValuesChangeable(false, 
												false, 
												false, 
												false, 
												false, 
												false, 
												false, 
												false, 
												false, 
												false, 
												false, 
												false, 
												false, 
												false, 
												false, 
												false, 
												false, 
												false, 
												false, 
												false, 
												false,
												false,
												false,
												false,
												false,
												false, 
												false,
												false);							                                     
		}

		orderStatus.getOrder().clearShipTos();

		for (int i = 0; i < shipTos.length; i++) {
			if (shipTos[i] != null){
			 orderStatus.getOrder().addShipTo(shipTos[i]);
			 orderStatus.addShipTo(shipTos[i]);
			}
		}

		cat.logT(
				Severity.DEBUG,
				loc,
				"reset shiptos done.");

		//replace shipping condition key
		SalesDocumentHelpValues helpValues = SalesDocumentHelpValues.getInstance();
		HeaderData header = orderStatus.getOrder().getHeaderData();
		header.setShipCond(helpValues.getHelpValue(SalesDocumentHelpValues.HELP_TYPE_SHIPPING_COND, 
												   aJCoCon, 
												   backendData.getLanguage(), 
												   header.getShipCond(), 
												   backendData.getConfigKey()));                                         
		aJCoCon.close();
	}

}
