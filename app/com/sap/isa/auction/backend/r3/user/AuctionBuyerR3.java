/**
 * Copyright (c) 2002, SAPLabs, Palo Alto, All rights reserved.
 */
package com.sap.isa.auction.backend.r3.user;

import com.sap.isa.auction.backend.boi.SalesAreaData;
import com.sap.isa.auction.backend.boi.user.AuctionBuyerBackend;
import com.sap.isa.auction.backend.boi.user.BuyerData;
import com.sap.isa.auction.bean.SalesArea;
import com.sap.isa.auction.logging.CategoryProvider;
import com.sap.isa.backend.JCoHelper;
import com.sap.isa.backend.boi.isacore.AddressData;
import com.sap.isa.backend.boi.isacore.BusinessObjectBaseData;
import com.sap.isa.backend.boi.isacore.ShopData;
import com.sap.isa.backend.boi.isacore.UserData;
import com.sap.isa.backend.r3.rfc.RFCWrapperUserR3;
import com.sap.isa.backend.r3.user.UserR3;
import com.sap.isa.backend.r3base.rfc.RFCConstants;
import com.sap.isa.backend.r3base.rfc.ReturnValue;
import com.sap.isa.backend.r3base.util.Conversion;
import com.sap.isa.backend.r3base.util.R3BackendData;
import com.sap.isa.businessobject.Address;
import com.sap.isa.core.TechKey;
import com.sap.isa.core.eai.BackendException;
import com.sap.isa.core.eai.sp.jco.JCoConnection;
import com.sap.isa.core.util.Message;
import com.sap.isa.core.util.table.Table;
import com.sap.isa.user.backend.boi.IsaUserBaseData;
import com.sap.isa.user.backend.boi.UserBaseData;
import com.sap.isa.user.backend.r3.MessageR3;
import com.sap.isa.user.backend.r3.ReleaseInfo;
import com.sap.isa.user.backend.r3.rfc.RFCWrapperUserBaseR3;
import com.sap.isa.user.util.LoginStatus;
import com.sap.isa.user.util.RegisterStatus;
import com.sap.mw.jco.JCO;
import com.sap.tc.logging.Category;
import com.sap.tc.logging.Location;
import com.sap.tc.logging.Severity;

public class AuctionBuyerR3 extends UserR3 implements AuctionBuyerBackend {

	/**
	 * Indicator to show if the login user is one-time customer, if it is onetime
	 * customer, then we skip the login authendication, otherwise we use SU05 and
	 * email address to do the authedication.
	 */
	private boolean isOneTimeCustomer = true;

	/** the onetime customer number **/
	private String oneTimeCustomerNumber = null;

	private final static Category cat = CategoryProvider.getSystemCategory();
	private final static Location loc =
		Location.getLocation(AuctionBuyerR3.class);

	private String old_userid = null;
	private String old_password = null;

	/**
	 * Constructor for the AuctionUser Object.
	 *
	 */
	public AuctionBuyerR3() {
		cat.logT(Severity.DEBUG, loc, "AuctionBuyerR3 constructed");
	}

	/**
	 * @return if the user is on-time customer based
	 */
	public boolean isOneTimeCustomer() {
		return isOneTimeCustomer;
	}

	/**
	 * @param for the boolean if it is one-time customer
	 */
	public void setOneTimeCustomer(boolean b) {
		isOneTimeCustomer = b;
	}

	/**
	 * User login into the r3 system for the auction scenario. 
	 * The login type here is Su05 for B2C scenario or as a one-time customer
	 * 
	 * @param password              user password --- 
	 * here could be null
	 * @param user                  user id -- 
	 * here could be null for the one-time customer
	 * @return indicates if the login is sucessful or not
	 * @exception BackendException  Exception from backend
	 */
	public LoginStatus login(UserBaseData user, String password)
		throws BackendException {
		LoginStatus functionReturnValue = LoginStatus.OK;
		if (isOneTimeCustomer) {
			// we we do not set the soldTo and contcat person for this user
			//soldTo and contact data.
			if (user instanceof IsaUserBaseData) {
				((IsaUserBaseData) user).setBusinessPartner(
					new TechKey(oneTimeCustomerNumber));
			}
			return functionReturnValue;
		}
		// if here we do not have one-time customer, it is same as B2C scenario

		JCoConnection connection = getCompleteConnection();

		r3Release = ReleaseInfo.getR3Release(connection);

		if (r3Release == null) {
			throw new BackendException(" r3 release could not be determined");
		} else {

			cat.logT(Severity.DEBUG, loc, "R3 Release: " + r3Release);
		}

		//get backendData
		backendData = getOrCreateBackendData();

		JCoConnection jcoCon;
		String pi = null;
		functionReturnValue = LoginStatus.NOT_OK;
		String nomessage = null;
		String customer = null;
		String contactPerson = null;
		JCO.Table customerlist = null;

		try {

			JCO.MetaData jcoMetaData = new JCO.MetaData("USER");
			jcoMetaData.addInfo("TYPE", JCO.TYPE_CHAR, 1, 0, 0, 0, null);
			jcoMetaData.addInfo("ID", JCO.TYPE_CHAR, 20, 2, 0, 0, null);
			jcoMetaData.addInfo("MESSAGE", JCO.TYPE_CHAR, 220, 2, 0, 0, null);
			jcoMetaData.addInfo(
				"LOG_MSG_NO",
				JCO.TYPE_CHAR,
				220,
				2,
				0,
				0,
				null);
			jcoMetaData.addInfo("MESSAGE_V1", JCO.TYPE_CHAR, 50, 2, 0, 0, null);
			jcoMetaData.addInfo("MESSAGE_V2", JCO.TYPE_CHAR, 50, 2, 0, 0, null);
			jcoMetaData.addInfo("MESSAGE_V3", JCO.TYPE_CHAR, 50, 2, 0, 0, null);
			jcoMetaData.addInfo("MESSAGE_V4", JCO.TYPE_CHAR, 50, 2, 0, 0, null);

			JCO.Record message = new JCO.Record(jcoMetaData);

			// get a connection for the login checks
			jcoCon = connection;

			// which login type should be used
			String loginType = props.getProperty(LOGINTYPE);
			userConceptToUse = Integer.parseInt(loginType);
			user.clearMessages();

			cat.logT(Severity.DEBUG, loc, "User id is: " + user.getUserId());
			cat.logT(Severity.DEBUG, loc, "login type is: " + userConceptToUse);

			user.setTechKey(new TechKey(user.getUserId()));

			if (user.getUserId().equals("")) {
				user.addMessage(
					new Message(
						Message.ERROR,
						"b2b.login.r3.enteruser",
						null,
						""));
			} else {

				if (password.equals("")) {
					user.addMessage(
						new Message(
							Message.ERROR,
							"user.r3.pwenter",
							null,
							""));
				} else {

					switch (userConceptToUse) {

						case 1 :

							// login with SU05 Internet User from type KNA1, User Id: email adress
							// old_userid = user.getUserId();
							old_userid = user.getUserId();
							pi = backendData.getBackend();

							if (pi.equals(RFCConstants.BACKEND_R3_PI)) {
								customerlist =
									RFCWrapperUserBaseR3
										.getCustomerFromEmailPI(
										jcoCon,
										user,
										nomessage);
							} else {
								customerlist =
									RFCWrapperUserBaseR3.getCustomerFromEmail(
										jcoCon,
										user,
										backendData
										/*nomessage*/);
							}

							if (customerlist == null) {
								cat.logT(Severity.DEBUG, loc, "customer list is null");
								user.addMessage(
									new Message(
										Message.ERROR,
										"b2b.login.r3.problem",
										null,
										""));
							} else {

								//loop through all customers and check the password. We take the first
								//customer that has the correct password
								//(web user has entered e-mail and password)
								//this is scenario dependant
								if (!backendData
									.getR3Release()
									.equals(RFCConstants.REL40B)) {

									cat.logT(Severity.DEBUG, loc, "release is >= 4.5b");

									if (customerlist.getNumRows() > 0)
										customerlist.firstRow();

									for (int i = 0;
										i < customerlist.getNumRows();
										i++) {

										if (customerlist
											.getString("OBJTYPE")
											.equals("KNA1")) {
											customer =
												customerlist.getString(
													"OBJKEY");
											user.setUserId(customer);
										}

										functionReturnValue =
											loginAsCustomerWithOldUserConcept(
												jcoCon,
												user,
												password,
												backendData);

										if (functionReturnValue
											== LoginStatus.NOT_OK) {
											customerlist.nextRow();
										} else {
											setCustomer(customer);

											break;
										}
									}
								} else {

									cat.logT(Severity.DEBUG, loc, "release is = 4.0b");

									if (customerlist.getNumRows() > 0)
										customerlist.firstRow();

									for (int i = 0;
										i < customerlist.getNumRows();
										i++) {
										customer =
											customerlist.getString("CUSTOMER");
										user.setUserId(customer);
										functionReturnValue =
											loginAsCustomerWithOldUserConcept(
												jcoCon,
												user,
												password,
												backendData);

										if (functionReturnValue
											== LoginStatus.NOT_OK) {
											customerlist.nextRow();
										} else {
											setCustomer(customer);

											break;
										}
									}
								}

								// user.setUserId(userid);
								// functionReturnValue = loginAsCustomerWithOldUserConcept(jcoCon, user, password);
							}

							if (functionReturnValue == LoginStatus.OK) {
								switchToPrivateConnection(
									user,
									userConceptToUse);
							}

							user.setUserId(old_userid);

							//soldTo and contact data.
							if (user instanceof IsaUserBaseData) {
								((IsaUserBaseData) user).setBusinessPartner(
									new TechKey(customer));
							}
							jcoCon.close();
							break;
					}
				}
			}

			old_password = password;
		} catch (JCO.Exception ex) {

			cat.log(Severity.DEBUG, loc, "Error calling r3 function:", ex);

			JCoHelper.splitException(ex);
		}

		//CHHI
		cat.logT(Severity.DEBUG, loc, "new user techkey: " + user.getUserId());

		user.setTechKey(new TechKey(user.getUserId()));

		//this is for shop admin (no call of getBusinessPartnerAdress
		user.setSalutationText("");

		return functionReturnValue;
	}

	/**
	 * Register a internet user as a consumer in the internet sales b2c scenario.
	 * 
	 * @param password              password
	 * @param user                  the user
	 * @param shopId                shop id
	 * @param address               adress data from registration
	 * @param password_verify       second password for verifying
	 * @return integer value that indicates if the registration was sucessful '0':
	 *         registriation sucessful '1': registration not sucessful display
	 *         corresponding messages in the jsp '2': registration not sucessful the
	 *         selection of a county is necessary for the taxjurisdiction code
	 *         determination
	 * @exception BackendException  Exception from backend
	 */
	public RegisterStatus register(
		UserData user,
		AddressData address,
		String password,
		String password_verify)
		throws BackendException {
		return register(user, null, address, password, password_verify);
	}

	/**
	 * Changes the data of a customer. Will be called in ISA R/3 B2C only.
	 * 
	 * @param user                  the ISA user
	 * @param address               the users address data
	 * @return integer value that indicates if the changing was sucessful '0': customer
	 *         change was sucessful '1': customer change was not sucessful show
	 *         corresponding messages '2': customer change was not sucessful selection
	 *         of county is necessary
	 * @exception BackendException  exception from R/3
	 * @throws ( BackendException if user is not logged in (not relevant at run-time)
	 */
	public RegisterStatus changeCustomer(UserData user, AddressData address)
		throws BackendException {
		return changeCustomer(user, null, address);
	}
	/**
	 * @return the one-time customer number
	 */
	public String getOneTimeCustomerNumber() {
		return oneTimeCustomerNumber;
	}

	/**
	 * @param string, the onetime customer number, if the length is over ten characters
	 * then we will have troubles, but here we do not catch it
	 */
	public void setOneTimeCustomerNumber(String string) {
		oneTimeCustomerNumber = string;
	}

	/**
	 * That should perform the user switch switch to a shared but privatized connection.
	 * The connection is established with the internet user logon data (user, password,
	 * language); therefore the default connection must be defined by using a incomplete
	 * connection definition, because the connection must be used (shared) as a default
	 * connection by all backend business objects. For login types that do not allow an SU01
	 * connection, the anonymous login parameters will be used.     
	 * 
	 * 
	 * @param user                  the ISA user
	 * @exception BackendException  exception from backend
	 */
	protected void switchToPrivateConnection(UserBaseData user, int loginType)
		throws BackendException {

		try {
			getStatefulConnectionFromAnonymousUser(user);
		} catch (Exception ex) {

			cat.log(Severity.DEBUG, loc, "Error get connection for internet user:", ex);

			throw new BackendException(
				"Error get connection for internet user:" + ex);
		}
	}

	/**
	 * Gets the stateful connection that is build from the imcomplete connection
	 * definition together witch user/password from config files. Since we don't have
	 * SU01 users always, we take the anonymous user here for the named stateful connection.
	 * 
	 * @param user the ISA user
	 * @return the connection
	 * @exception BackendException  the exception from backend
	 */
	protected JCoConnection getStatefulConnectionFromAnonymousUser(UserBaseData user)
		throws BackendException {

		cat.logT(Severity.DEBUG, loc, "getStatefulConnection start");

		try {
			return getCompleteConnection();
		} catch (Exception ex) {
			throw new BackendException("XCM scenario does not exist or is not maintained properly");
		}
	}
	
	public boolean checkAddress(AddressData address) throws BackendException {
		cat.logT(Severity.DEBUG, loc, "checkAddress start");
		
		if(address.getCountry() == null || address.getCountry().length()<=0)
			return false;
		//if(address.getRegion() == null || address.getRegion().length()<=0)
		//	return false;
		if(address.getCity() == null || address.getCity().length()<=0)
			return false;
		if(address.getPostlCod1() == null || address.getPostlCod1().length()<=0)
			return false;
			
		JCoConnection jcoCon = null;
		try {
			jcoCon = getDefaultJCoConnection();
			//getOrCreateBackendData().setPlugInAvailable(true);
			boolean functionReturnValue;

			//First: check postal code
			functionReturnValue = checkPostalCode(address, jcoCon);

			if (functionReturnValue) {
				//Second: check tax jurisdiction code
				functionReturnValue = checkTaxJurCode(address, jcoCon);
			}

			return functionReturnValue;
		} 
		catch(Exception ex) {
			cat.log(Severity.DEBUG, loc, ex);
		}
		finally {
			if(jcoCon != null)
			jcoCon.close();
		}
		return false;
	}

	/**
	 * Checks the postal code of an address by calling function ISA_ADDR_POSTAL_CODE_CHECK.
	 * 
	 * @param addressData  the address to be checked
	 * @param connection   ISA JCO connection
	 * @return success or failure
	 * @exception BackendException exception from R/3
	 */
	protected boolean checkPostalCode(AddressData addressData, 
								  JCoConnection connection)
						   throws BackendException {

		cat.logT(Severity.DEBUG, loc, "checkPostalCode begin");

		boolean functionReturnValue = true;

		//fill request
		JCO.Function function = connection.getJCoFunction(RFCConstants.RfcName.ISA_ADDR_POSTAL_CODE_CHECK);
		JCO.ParameterList request = function.getImportParameterList();
		request.setValue(addressData.getCountry(), 
						 "COUNTRY");
		request.setValue(addressData.getPostlCod1(), 
						 "POSTAL_CODE_CITY");
		request.setValue(addressData.getRegion(), 
						 "REGION");

		//fire RFC
		connection.execute(function);

		//read results
		JCO.Structure message = function.getExportParameterList().getStructure(
										"RETURN");
		ReturnValue returnValue = new ReturnValue(message, 
												  "");

		if (addAllMessagesToBusinessObject(returnValue, addressData)) {
			functionReturnValue = false;
		}

		return functionReturnValue;
	}

	/**
	 * Checks the tax jurisdiction code of an address by calling function
	 * ISA_ADDRESS_TAX_JUR_TABLE. If no tax jurisdiction code can be determined, 
	 * the county has to be re-selected and <code> 2 </code> is returned.
	 * 
	 * @param addressData  the address to be checked
	 * @param connection   ISA JCO connection
	 * @return success or failure
	 * @exception BackendException exception from backend
	 */
	protected boolean checkTaxJurCode(AddressData addressData, 
								  JCoConnection connection)
						   throws BackendException {

		cat.logT(Severity.DEBUG, loc, "checkTaxJurCode begin, district and taxJur code: " + addressData.getDistrict() + "/" + addressData.getTaxJurCode());

		boolean functionReturnValue;
		String taxJurCode = addressData.getTaxJurCode();

		//fill request
		JCO.Function function = connection.getJCoFunction(RFCConstants.RfcName.ISA_ADDRESS_TAX_JUR_TABLE);
		JCO.Structure address_in = function.getImportParameterList().getStructure(
										   "ADDRESS_IN");
		address_in.setValue(addressData.getCountry(), 
							"COUNTRY");
		address_in.setValue(addressData.getRegion(), 
							"STATE");
		address_in.setValue(addressData.getCity(), 
							"CITY");
		address_in.setValue(addressData.getPostlCod1(), 
							"ZIPCODE");

		//fire RFC
		connection.execute(function);

		//read results
		JCO.Structure message = function.getExportParameterList().getStructure(
										"RETURN");
		JCO.Table jurTab = function.getTableParameterList().getTable(
								   "JURTAB");
		ReturnValue returnValue = new ReturnValue(message, 
												  "");
		addressData.clearMessages();

		if (addAllMessagesToBusinessObject(returnValue, addressData)) {
			functionReturnValue = false;
		}
		 else {
			addressData.clearMessages();

			if (jurTab.getNumRows() > 1) {

				//select county only if the existing
				//tax jur code does not match
//				jurTab.firstRow();
//
//				do {
//
//					String currentTaxJurCode = jurTab.getString(
//													   "TXJCD");
//
//					if (taxJurCode.equals(currentTaxJurCode)) {
//						functionReturnValue = true;
//
//						return functionReturnValue;
//					}
//				}
//				 while (jurTab.nextRow());
//
//				functionReturnValue = 2;
				functionReturnValue = true;
				//This is the case there are two counties for one zip code of US,
				//currently we consider this case valid, we may handle it later that 
			}
			 else {

				//ok or no taxjurisdiction code required
				functionReturnValue = true;

				//blank it, not necessary to pass it to R/3
				//tax jur code might have been set previously
				addressData.setTaxJurCode("");
			}
		}

		return functionReturnValue;
	}
	
	/**
	 * Takes all R/3 return messages and attaches them to the business object.
	 * 
	 * @param retVal          the R/3 return messages
	 * @param businessObject  the ISA business object
	 * @return Were there errors?
	 */
	public boolean addAllMessagesToBusinessObject(ReturnValue retVal, 
												  BusinessObjectBaseData businessObject) {

		//is there a structure
		if (retVal.getStructure() != null) {

			String type = retVal.getStructure().getString(
												  "TYPE");
			boolean thisMessageWasError = type.equals(
												  RFCConstants.BAPI_RETURN_ERROR) ||type.equals(
												  RFCConstants.BAPI_RETURN_ABORT);
			if (!type.trim().equals(""))                                                  
				MessageR3.addMessagesToBusinessObject(businessObject, 
													  retVal.getStructure());

			return thisMessageWasError;
		}
		 else {

			boolean error = false;

			//it is a table
			JCO.Table retTable = retVal.getMessages();

			if (retTable.getNumRows() > 0) {
				retTable.firstRow();

				do {
					logMessage(retTable);

					String type = retTable.getString(
												  "TYPE");
					boolean thisMessageWasError = type.equals(
														  RFCConstants.BAPI_RETURN_ERROR)||type.equals(
														  RFCConstants.BAPI_RETURN_ABORT);
					MessageR3.addMessagesToBusinessObject(businessObject, 
														  (JCO.Record)retTable);
					error = error || thisMessageWasError;
				}
				 while (retTable.nextRow());
			}

			return error;
		}
	}

	/**
	 * Log r3 messages.
	 * 
	 * @param message the record containing the message
	 */
	protected void logMessage(JCO.Record message) {

		String messageText = message.getString("MESSAGE");
		String messageType = message.getString("TYPE");
		String messageCode = null;

		if (message.hasField("NUMBER")) {
			messageCode = message.getString("ID") + "/" + message.getString(
																  "NUMBER");
		}
		 else {
			messageCode = message.getString("CODE");
		}

		cat.logT(Severity.DEBUG, loc, "r3 message: " + messageType + "/" + messageCode + "/" + messageText);

		if (messageType.equals(RFCConstants.BAPI_RETURN_ERROR))
			cat.logT(Severity.ERROR, loc, MessageR3.ISAR3MSG_SALESDOC_INFO,new Object[]{ messageType + "/" + messageCode + "/" + messageText});
	}
	
	/** 
	 * It is used to create a customer master record in the backend
	 * It does not create any user in the backend, used in the 
	 * Auction scenario for the scheduler based order creation 
	 */
	public RegisterStatus createCustomerMaster(
		UserData user,
		AddressData address,
		SalesAreaData salesarea)
		throws BackendException {
		boolean ret = false;
		JCoConnection jcoCon = getDefaultJCoConnection();

		r3Release = ReleaseInfo.getR3Release(jcoCon);
		cat.logT(Severity.DEBUG, loc, "The R3 release version is" + r3Release);
		RegisterStatus functionReturnValue = RegisterStatus.NOT_OK;
		LoginStatus returnValue = LoginStatus.NOT_OK;
		//JCoConnection jcoConForProxy = null;
		String returnCode = null;
		String initPassword = null;
		String refcustomer = null;
		String customer = null;
		JCO.Table customerlist = null;

		//check for missing fields
		user.clearMessages();

		//if (!hasMissingFields(address, user, jcoCon)) {

			try {

				if (address.getTaxJurCode() == null
					|| address.getTaxJurCode().trim().equals("")) {

					//first: check counties
					Table counties = checkCounties(user, address, jcoCon);

					if (counties != null && counties.getNumRows() > 1) {

						return RegisterStatus.NOT_OK_ENTER_COUNTY;
					}
				}

				user.setUserId(address.getEMail());

				R3BackendData backendData = getOrCreateBackendData();
				String language = backendData.getLanguage();
				if (language == null) language ="EN";
				
				String currency = null;
				if(user instanceof BuyerData)
					currency = ((BuyerData)user).getCurrency();
				if (currency == null) currency = backendData.getCurrency();
				if (currency == null) currency = "USD";
				
				if (salesarea == null){
					salesarea = new SalesArea();
					salesarea.setSalesOrganization("1000");
					salesarea.setDistributionChannel("10");
					salesarea.setDivision("00");
				}
				refcustomer = backendData.getRefCustomer();
				if (refcustomer == null) refcustomer = "100061";
				cat.logT(Severity.DEBUG, loc, "The refer customer is " + refcustomer);
				String backend = backendData.getBackend();
				cat.logT(Severity.DEBUG, loc, "The backend is " + backend);
				if (backend == null) backend = ShopData.BACKEND_R3_PI;
				String nomessage = "X";
				
				String releaseVersion = getOrCreateBackendData().getR3Release();
				if (releaseVersion == null) releaseVersion = "46C"; 
//				if (backendData.isPlugInAvailable()) {
					customerlist =
						RFCWrapperUserBaseR3.getCustomerFromEmailPI(
							jcoCon,
							user,
							nomessage);
//				} else {
//					customerlist =
//						RFCWrapperUserBaseR3.getCustomerFromEmail(
//							jcoCon,
//							user,
//							backendData
//							/*nomessage*/);
//				}

				// existence check depending on release
				cat.logT(Severity.DEBUG, loc, "starting register, parameters ");
				cat.logT(Severity.DEBUG, loc, "language: " + language);
				cat.logT(Severity.DEBUG, loc, "currency: " + currency);
				cat.logT(Severity.DEBUG, loc, "refcustomer: " + refcustomer);
				cat.logT(Severity.DEBUG, loc, "email: " + address.getEMail());
				cat.logT(Severity.DEBUG, loc, "taxjurcode " + address.getTaxJurCode());
				
				old_userid = address.getEMail();

				if (customerlist != null) {

					cat.logT(Severity.DEBUG, loc, "customerlist = " + customerlist);
					
					//first: 4.0b
					if (releaseVersion
						.equals(REL40B)) {

						if (customerlist.getNumRows() > 0)
							customerlist.firstRow();

						for (int i = 0; i < customerlist.getNumRows(); i++) {
							customer = customerlist.getString("CUSTOMER");
							user.setUserId(customer);
							user.setBusinessPartner(new TechKey(customer));
							cat.logT(Severity.DEBUG, loc, "There is customer master already");
							returnValue = LoginStatus.OK;
							if (returnValue == LoginStatus.NOT_OK) {
								customerlist.nextRow();
							} else {
								setCustomer(customer);

								break;
							}
						}
					} else {

						if (customerlist.getNumRows() > 0)
							customerlist.firstRow();
						cat.logT(Severity.DEBUG, loc, "The number of customer is " + customerlist.getNumRows() );
						for (int i = 0; i < customerlist.getNumRows(); i++) {

							if (customerlist
								.getString("OBJTYPE")
								.equals("KNA1")) {
								customer = customerlist.getString("OBJKEY");
								user.setUserId(customer);
								user.setBusinessPartner(new TechKey(customer));
								cat.logT(Severity.DEBUG, loc, "There is customer master already");
								returnValue = LoginStatus.OK;
								setCustomer(customer);
								break;
							}
							customerlist.nextRow();
						}
					}

					cat.logT(Severity.DEBUG, loc, "Return Value: " + returnValue);

					if (returnValue == LoginStatus.OK) {
						user.clearMessages();

						// bring errors to the error page
						user.addMessage(
							new Message(
								Message.ERROR,
								"b2c.personaldetails.userid.error",
								null,
								""));
						returnCode = "1";
					} else {
						returnCode = "0";
					}
				} else {
					returnCode = "0";
				}

				if (returnCode.equals("0")) {

					// save customer data
//					String title =
//						getTitleDescription(
//							language,
//							jcoCon,
//							address.getTitleKey());
//					String title = getCustomerTitle(
//										refcustomer,
//										salesarea.getSalesOrganization(),
//										salesarea.getDistributionChannel(),
//										salesarea.getDivision(),
//										jcoCon);
//					address.setTitle(title);

					boolean isPerson = true;
//						ShopBase.titleIsForPerson(
//							address.getTitleKey(),
//							getOrCreateBackendData(),
//							jcoCon);
					customer =
						saveCustomerData(
							jcoCon,
							user,
							address,
							salesarea.getSalesOrganization(),
							salesarea.getDistributionChannel(),
							salesarea.getDivision(),
							refcustomer,
							r3Release,
							language,
							currency,
							isPerson,
							true,
							null);

//					cat.logT(Severity.DEBUG, loc, "register: is person: "
//								+ isPerson
//								+ " , title: "
//								+ title);
					cat.logT(Severity.DEBUG, loc, "the new customer: " + customer);

				}

				user.setUserId(customer);
				//user.setUserId(old_userid);
				//CHHI
				user.setTechKey(new TechKey(customer));
				//user.setTechKey(new TechKey(user.getUserId()));
				// let us et the sold to here
				try{
					if(customer != null){
						user.setBusinessPartner(new TechKey(customer));
					}
				}catch(Exception ex){
					cat.logT(Severity.DEBUG, loc, "SoldTo ID can not be set");
				}
				
			} catch (Exception ex) {
				throw new BackendException(ex.getMessage());
			} finally {
				jcoCon.close();
			}
		//}

		return functionReturnValue;
	}
	
	private final static String PI_ADDRESS = "PI_ADDRESS";
	private final static String PI_COMPANYDATA = "PI_COMPANYDATA";
	private final static String PI_PERSONALDATA = "PI_PERSONALDATA";
	private final static String PI_COMPANYDATAX = "PI_COMPANYDATAX";
	private final static String PI_PERSONALDATAX = "PI_PERSONALDATAX";
	private final static String ADDRESS = "ADDRESS";
	private final static String ADDRESSX = "ADDRESSX";
	
	//copyed from RFCWrapperUserR3, only not set PI_CONSUMEREN
	public static String saveCustomerData(JCoConnection jcoCon,
										  UserData user,
										  AddressData address,
										  String salesorg,
										  String distr_chan,
										  String division,
										  String ref_custmr,
										  String r3Release,
										  String language,
										  String currency,
										  boolean isPerson,
										  boolean createConsumer,
										  String title)
								   throws BackendException {

		String objtype = null;
		String userid = null;
		String action = null;
		String initpassword = null;
		JCO.ParameterList importParams;
		JCO.Function function;
		JCO.Structure message = null;
		StringBuffer mestype;
		StringBuffer mesid;
		StringBuffer mesno;
		StringBuffer mes;
		String customerno = null;
		String taxjur = address.getTaxJurCode();

		if (r3Release.equals(REL40B)) {

			// get the repository infos of the function that we want to call
			function = jcoCon.getJCoFunction(RfcName.BAPI_CUSTOMER_CREATEFROMDATA);

			// set the import values
			importParams = function.getImportParameterList();
			importParams.getStructure("PI_COPYREFERENCE").setValue(
					salesorg,
					"SALESORG");
			importParams.getStructure("PI_COPYREFERENCE").setValue(
					distr_chan,
					"DISTR_CHAN");
			importParams.getStructure("PI_COPYREFERENCE").setValue(
					division,
					"DIVISION");
			importParams.getStructure("PI_COPYREFERENCE").setValue(
					RFCWrapperUserR3.trimZeros10(ref_custmr),
					"REF_CUSTMR");
			if(title != null )
				importParams.getStructure(PI_ADDRESS).setValue(title,
														   "FORM_OF_AD");

			if (isPerson) {
				importParams.getStructure(PI_ADDRESS).setValue(
						address.getFirstName(),
						"FIRST_NAME");
				importParams.getStructure(PI_ADDRESS).setValue(
						address.getLastName(),
						"NAME");
			}
			 else {
				importParams.getStructure(PI_ADDRESS).setValue(
						address.getName2(),
						"FIRST_NAME");
				importParams.getStructure(PI_ADDRESS).setValue(
						address.getName1(),
						"NAME");
			}

			importParams.getStructure(PI_ADDRESS).setValue(address.getName3(),
														   "NAME_3");
			importParams.getStructure(PI_ADDRESS).setValue(address.getName4(),
														   "NAME_4");
			importParams.getStructure(PI_ADDRESS).setValue(address.getStreet() + " " + address
				   .getHouseNo(),
														   "STREET");
			importParams.getStructure(PI_ADDRESS).setValue(address.getPostlCod1(),
														   "POSTL_CODE");
			importParams.getStructure(PI_ADDRESS).setValue(address.getCity(),
														   "CITY");
			importParams.getStructure(PI_ADDRESS).setValue(address.getRegion(),
														   "REGION");
			importParams.getStructure(PI_ADDRESS).setValue(address.getCountry(),
														   "COUNTRY");
			importParams.getStructure(PI_ADDRESS).setValue(address.getEMail(),
														   "INTERNET");
			importParams.getStructure(PI_ADDRESS).setValue(address.getFaxNumber(),
														   "FAX_NUMBER");
			importParams.getStructure(PI_ADDRESS).setValue(address.getTel1Numbr(),
														   "TELEPHONE");
			importParams.getStructure(PI_ADDRESS).setValue(address.getCountry(),
														   "COUNTRY");
			importParams.getStructure(PI_ADDRESS).setValue(address.getCountryISO(),
														   "COUNTRAISO");
			importParams.getStructure(PI_ADDRESS).setValue(Conversion.getR3LanguageCode(language.toLowerCase()),
														   "LANGU");
			importParams.getStructure(PI_ADDRESS).setValue(currency,
														   "CURRENCY");

			cat.logT(Severity.DEBUG, loc, "PI_ADDRESS segment: " + importParams.getStructure(
														   PI_ADDRESS));

			// call the function
			jcoCon.execute(function);

			//get the return values
			customerno = function.getExportParameterList().getString(
								 "PE_CUSTOMER");
			message = function.getExportParameterList().getStructure(
							  "RETURN");
			mestype = new StringBuffer("");
			mesid = new StringBuffer("");
			mesno = new StringBuffer("");
			mes = new StringBuffer("");
			getMessageParameters(mestype,
								 mesid,
								 mesno,
								 mes,
								 message);
		}
		 else {

			cat.logT(Severity.DEBUG, loc, "Entering BAPI_CUSTOMER_CREATEFROMDATA1");

			// get the repository infos of the function that we want to call
			function = jcoCon.getJCoFunction(RfcName.BAPI_CUSTOMER_CREATEFROMDATA1);

			// set the import values
			importParams = function.getImportParameterList();
			importParams.getStructure("PI_COPYREFERENCE").setValue(
					salesorg,
					"SALESORG");
			importParams.getStructure("PI_COPYREFERENCE").setValue(
					distr_chan,
					"DISTR_CHAN");
			importParams.getStructure("PI_COPYREFERENCE").setValue(
					division,
					"DIVISION");
			importParams.getStructure("PI_COPYREFERENCE").setValue(
					RFCWrapperUserR3.trimZeros10(ref_custmr),
					"REF_CUSTMR");

			cat.logT(Severity.DEBUG, loc, "reference customer: " + RFCWrapperUserR3.trimZeros10(ref_custmr));
			cat.logT(Severity.DEBUG, loc, "distr. chan.: " + distr_chan);
			cat.logT(Severity.DEBUG, loc, "division: " + division);
			cat.logT(Severity.DEBUG, loc, "language: " + language.toUpperCase());
			cat.logT(Severity.DEBUG, loc, "title: " + address.getTitle());
			cat.logT(Severity.DEBUG, loc, "title_key: " + address.getTitleKey());
			cat.logT(Severity.DEBUG, loc, "name: " + address.getName1());
			cat.logT(Severity.DEBUG, loc, "name2: " + address.getName2());
			cat.logT(Severity.DEBUG, loc, "name3: " + address.getName3());
			cat.logT(Severity.DEBUG, loc, "name4: " + address.getName4());
			cat.logT(Severity.DEBUG, loc, "firstname: " + address.getFirstName());
			cat.logT(Severity.DEBUG, loc, "lastname: " + address.getLastName());
			cat.logT(Severity.DEBUG, loc, "taxjurcode: " + address.getTaxJurCode());
			cat.logT(Severity.DEBUG, loc, "taxjurcode: " + address.getTaxJurCode());
			cat.logT(Severity.DEBUG, loc, "isPerson: " + isPerson);
			cat.logT(Severity.DEBUG, loc, "createConsumer: " + createConsumer);

			String titlekey = RFCWrapperUserR3.trimZeros4(address.getTitleKey());

			boolean createConsumerInR3 = isPerson && createConsumer;
			if (!createConsumerInR3) {

				// importParams.getStructure(PI_COMPANYDATA).setValue(titlekey, "TITLE_KEY");
				importParams.getStructure(PI_COMPANYDATA).setValue(
						title,
						"TITLE_KEY");
				if (!isPerson){
					importParams.getStructure(PI_COMPANYDATA).setValue(address.getName1(), "NAME");
					importParams.getStructure(PI_COMPANYDATA).setValue(address.getName2(), "NAME_2");
				}
				else{
					importParams.getStructure(PI_COMPANYDATA).setValue(address.getLastName(), "NAME");
					importParams.getStructure(PI_COMPANYDATA).setValue(address.getFirstName(), "NAME_2");
				}
				importParams.getStructure(PI_COMPANYDATA).setValue(
						address.getName3(),
						"NAME_3");
				importParams.getStructure(PI_COMPANYDATA).setValue(
						address.getName4(),
						"NAME_4");
				importParams.getStructure(PI_COMPANYDATA).setValue(
						address.getCity(),
						"CITY");
				importParams.getStructure(PI_COMPANYDATA).setValue(
						address.getDistrict(),
						"DISTRICT");
				importParams.getStructure(PI_COMPANYDATA).setValue(
						address.getPostlCod1(),
						"POSTL_COD1");
				importParams.getStructure(PI_COMPANYDATA).setValue(
						address.getPostlCod2(),
						"POSTL_COD2");
				importParams.getStructure(PI_COMPANYDATA).setValue(
						address.getPoBox(),
						"PO_BOX");
				importParams.getStructure(PI_COMPANYDATA).setValue(
						address.getPoBoxCit(),
						"PO_BOX_CIT");
				importParams.getStructure(PI_COMPANYDATA).setValue(
						address.getStreet(),
						"STREET");
				importParams.getStructure(PI_COMPANYDATA).setValue(
						address.getHouseNo(),
						"HOUSE_NO");
				importParams.getStructure(PI_COMPANYDATA).setValue(
						address.getBuilding(),
						"BUILDING");
				importParams.getStructure(PI_COMPANYDATA).setValue(
						address.getFloor(),
						"FLOOR");
				importParams.getStructure(PI_COMPANYDATA).setValue(
						address.getRoomNo(),
						"ROOM_NO");
				importParams.getStructure(PI_COMPANYDATA).setValue(
						address.getCountry(),
						"COUNTRY");
				importParams.getStructure(PI_COMPANYDATA).setValue(
						address.getCountryISO(),
						"COUNTRYISO");
				importParams.getStructure(PI_COMPANYDATA).setValue(
						address.getRegion(),
						"REGION");
				importParams.getStructure(PI_COMPANYDATA).setValue(
						address.getTel1Numbr(),
						"TEL1_NUMBR");
				importParams.getStructure(PI_COMPANYDATA).setValue(
						address.getTel1Ext(),
						"TEL1_EXT");
				importParams.getStructure(PI_COMPANYDATA).setValue(
						address.getFaxNumber(),
						"FAX_NUMBER");
				importParams.getStructure(PI_COMPANYDATA).setValue(
						address.getFaxExtens(),
						"FAX_EXTENS");
				importParams.getStructure(PI_COMPANYDATA).setValue(
						address.getEMail().toUpperCase(),
						"E_MAIL");
				importParams.getStructure(PI_COMPANYDATA).setValue(
						language.toUpperCase(),
						"LANGU_ISO");
				importParams.getStructure(PI_COMPANYDATA).setValue(
						currency,
						"CURRENCY");

				if (taxjur != null && (!taxjur.equals(""))) {
					importParams.getStructure("PI_OPT_COMPANYDATA").setValue(
							taxjur,
							"TAXJURCODE");
				}
			}
			 else {

				//consumer creation
				//importParams.setValue(X,"PI_CONSUMEREN");

				// importParams.getStructure(PI_PERSONALDATA).setValue(titlekey, "TITLE_KEY");
				importParams.getStructure(PI_PERSONALDATA).setValue(
						title,
						"TITLE_KEY");
				importParams.getStructure(PI_PERSONALDATA).setValue(
						address.getFirstName(),
						"FIRSTNAME");
				importParams.getStructure(PI_PERSONALDATA).setValue(
						address.getLastName(),
						"LASTNAME");

				// importParams.getStructure(PI_PERSONALDATA).setValue(address.getSecondName(), "SECONDNAME");
				// importParams.getStructure(PI_PERSONALDATA).setValue(address.getMiddleName(), "MIDDLENAME");
				importParams.getStructure(PI_PERSONALDATA).setValue(
						address.getCity(),
						"CITY");
				importParams.getStructure(PI_PERSONALDATA).setValue(
						address.getDistrict(),
						"DISTRICT");
				importParams.getStructure(PI_PERSONALDATA).setValue(
						address.getPostlCod1(),
						"POSTL_COD1");
				importParams.getStructure(PI_PERSONALDATA).setValue(
						address.getPostlCod2(),
						"POSTL_COD2");
				importParams.getStructure(PI_PERSONALDATA).setValue(
						address.getPoBox(),
						"PO_BOX");
				importParams.getStructure(PI_PERSONALDATA).setValue(
						address.getPoBoxCit(),
						"PO_BOX_CIT");
				importParams.getStructure(PI_PERSONALDATA).setValue(
						address.getStreet(),
						"STREET");
				importParams.getStructure(PI_PERSONALDATA).setValue(
						address.getHouseNo(),
						"HOUSE_NO");
				importParams.getStructure(PI_PERSONALDATA).setValue(
						address.getBuilding(),
						"BUILDING");
				importParams.getStructure(PI_PERSONALDATA).setValue(
						address.getFloor(),
						"FLOOR");
				importParams.getStructure(PI_PERSONALDATA).setValue(
						address.getRoomNo(),
						"ROOM_NO");
				importParams.getStructure(PI_PERSONALDATA).setValue(
						address.getCountry(),
						"COUNTRY");
				importParams.getStructure(PI_PERSONALDATA).setValue(
						address.getCountryISO(),
						"COUNTRYISO");
				importParams.getStructure(PI_PERSONALDATA).setValue(
						address.getRegion(),
						"REGION");
				importParams.getStructure(PI_PERSONALDATA).setValue(
						address.getTel1Numbr(),
						"TEL1_NUMBR");
				importParams.getStructure(PI_PERSONALDATA).setValue(
						address.getTel1Ext(),
						"TEL1_EXT");
				importParams.getStructure(PI_PERSONALDATA).setValue(
						address.getFaxNumber(),
						"FAX_NUMBER");
				importParams.getStructure(PI_PERSONALDATA).setValue(
						address.getFaxExtens(),
						"FAX_EXTENS");
				importParams.getStructure(PI_PERSONALDATA).setValue(
						address.getEMail().toUpperCase(),
						"E_MAIL");
				importParams.getStructure(PI_PERSONALDATA).setValue(
						language.toUpperCase(),
						"LANGU_P");
				importParams.getStructure(PI_PERSONALDATA).setValue(
						currency,
						"CURRENCY");

				if (taxjur != null && (!taxjur.equals(""))) {
					importParams.getStructure("PI_OPT_PERSONALDATA").setValue(
							taxjur,
							"TAXJURCODE");
				}
			}

			// call the function
			jcoCon.execute(function);

			//get the return values
			customerno = function.getExportParameterList().getString(
								 "CUSTOMERNO");
			message = function.getExportParameterList().getStructure(
							  "RETURN");
			mestype = new StringBuffer("");
			mesid = new StringBuffer("");
			mesno = new StringBuffer("");
			mes = new StringBuffer("");
			getMessageParameters(mestype,
								 mesid,
								 mesno,
								 mes,
								 message);

			cat.logT(Severity.DEBUG, loc, "Customer no: " + customerno);
			cat.logT(Severity.DEBUG, loc, "Mes " + mes);
			cat.logT(Severity.DEBUG, loc, "Mesid " + mesid);
		}

		user.clearMessages();

		if (mesid.toString().equals("")) {

			cat.logT(Severity.DEBUG, loc, "Entering Commit work BAPI ");

			JCO.Function func = jcoCon.getJCoFunction(RfcName.BAPI_TRANSACTION_COMMIT);
			JCO.ParameterList impParams = func.getImportParameterList();
			impParams.setValue("X",
							   "WAIT");
			jcoCon.execute(func);

			String mess = function.getExportParameterList().getStructure(
								  "RETURN").getString("MESSAGE");

			cat.logT(Severity.DEBUG, loc, "Commit work Mess: " + mess);

			MessageR3.addMessagesToBusinessObject(user,
												  message);
		}
		 else {

			cat.logT(Severity.DEBUG, loc, "No Commit work.");

			// debug:begin
			// bring errors to the error page
			MessageR3.addMessagesToBusinessObject(user,
												  message,"");

			// debug:end
			MessageR3.logMessagesToBusinessObject(user,
												  message);

			cat.logT(Severity.DEBUG, loc, "Message: " + message.getValue("MESSAGE"));
		}

		return customerno;
	}

	protected static void getMessageParameters(StringBuffer messageType,
											   StringBuffer messageId,
											   StringBuffer messageNumber,
											   StringBuffer messageDescription,
											   JCO.Structure returnStruc) {

		if (returnStruc.hasField("TYPE")) {
			messageType.append(returnStruc.getString("TYPE"));
		}

		if (returnStruc.hasField("CODE")) {

			if (returnStruc.getString("CODE").length() > 1) {
				messageId.append(returnStruc.getString("CODE").substring(
										 0,
										 2));
				messageNumber.append(returnStruc.getString("CODE").substring(
											 2));
			}
		}

		if (returnStruc.hasField("ID")) {
			messageId.append(returnStruc.getString("ID"));
			messageNumber.append(returnStruc.getString("NUMBER"));
		}

		messageDescription.append(returnStruc.getString("MESSAGE"));
		cat.logT(Severity.DEBUG, loc, "getMessageParameters, message: " + messageDescription.toString());
	}

	protected String getCustomerTitle(String customerno,
								  String salesorg,
								  String distch,
								  String division,
								  JCoConnection connection)
						   throws BackendException {

		cat.logT(Severity.DEBUG, loc, "getCustomerTitle start");

		String functionReturnValue = "";

		//fill request
		try {
			JCO.Function function = connection.getJCoFunction(RFCConstants.RfcName.BAPI_CUSTOMER_GETDETAIL1);
			JCO.ParameterList request = function.getImportParameterList();
			request.setValue(RFCWrapperUserR3.trimZeros10(customerno),	"CUSTOMERNO");
			request.setValue(salesorg,	"PI_SALESORG");
			request.setValue(distch,  	"PI_DISTR_CHAN");
			request.setValue(division,  "PI_DIVISION");
			
			//fire RFC
			connection.execute(function);
			
			//read results
			JCO.Structure personalData = function.getExportParameterList().getStructure(
											"PE_PERSONALDATA");
											
			if (personalData != null) {
				functionReturnValue = personalData.getString("TITLE_KEY");
			}
		} catch (BackendException e) {
			cat.log(Severity.DEBUG, loc, "Can not getCustomerTitle", e);
		}

		return functionReturnValue;
	}


	/* (non-Javadoc)
	 * @see com.sap.isa.auction.backend.boi.user.AuctionBuyerBackend#registerUserForExistingBP(java.lang.String, com.sap.isa.businessobject.Address, java.lang.String, java.lang.String, java.lang.String)
	 */
	public RegisterStatus registerUserForExistingBP(String shopId, Address address, String bpId, String password, String password_verify) throws BackendException {
		return null;
	}
}
