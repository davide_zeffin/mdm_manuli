package com.sap.isa.auction.backend.r3.user;

import java.util.ArrayList;
import java.util.Properties;

import com.sap.isa.auction.backend.boi.businesspartner.ResponsibleSalesEmployeeData;
import com.sap.isa.auction.backend.boi.user.AuctionUserBackend;
import com.sap.isa.auction.backend.boi.user.AuctionUserBaseData;
import com.sap.isa.auction.backend.boi.user.AuctionUserData;
import com.sap.isa.auction.backend.r3.user.rfc.RFCSellerUserWrapper;
import com.sap.isa.auction.logging.CategoryProvider;
import com.sap.isa.backend.JCoHelper;
import com.sap.isa.backend.r3.rfc.RFCWrapperUserR3;
import com.sap.isa.backend.r3.user.UserR3;
import com.sap.isa.core.TechKey;
import com.sap.isa.core.eai.BackendException;
import com.sap.isa.core.eai.sp.jco.JCoConnection;
import com.sap.isa.core.eai.sp.jco.JCoManagedConnectionFactory;
import com.sap.isa.core.util.Message;
import com.sap.isa.user.backend.boi.UserBaseData;
import com.sap.isa.user.backend.r3.ReleaseInfo;
import com.sap.isa.user.backend.r3.rfc.RFCWrapperUserBaseR3;
import com.sap.isa.user.util.LoginStatus;
import com.sap.mw.jco.JCO;
import com.sap.tc.logging.Category;
import com.sap.tc.logging.Location;
import com.sap.tc.logging.Severity;

/**
 * This class mainly for the seller side application, since the buyer side
 * is a SU05 user, it is handled by the standard ISA application.
 */
public class AuctionUserR3 extends UserR3 implements AuctionUserBackend {

	/**
	 * the sales employee for the auction scenario
	 */
	private String salesEmployee = null;
	private String client;
	private String systemId;
	private static final String USER_NAME_GET = "USER_NAME_GET";

	private final static Category cat = CategoryProvider.getSystemCategory();
	private final static Location loc =
		Location.getLocation(AuctionUserR3.class);
	
	private final static String SPECIAL_NAME_AS_USERID = "$MYSAPSSO2$";


	/**
	 * Constructor for the AuctionUser Object.
	 *
	 */
	public AuctionUserR3() {
		if (log.isDebugEnabled()) {
			log.debug("AuctionUserR3 constructed");
		}
	}

	/**
	* Get the user defaults like timezone, decimal format and
	* date format given the crm user id
	*/
	public void getUserDefaults(AuctionUserData aucUser, String userid)
		throws BackendException {

	}

	/**
	 * get the sales employee from Internet user, in current implementation
	 * we assume that one User gets one sales employee
	 * Her the internet user is type of system user
	 * @param the system user name
	 * @return the sales employee data
	 */
	public ResponsibleSalesEmployeeData getSalesEmployeeFromInternetUser(String userName)
		throws BackendException {

		return null;
	}

	/**
	 * get all of the users with certain profile, currently this function is implemented in
	 * from our persistent, but in the future
	 * @param profiles, a collection of profiles to retrieve all the users for the seller
	 * @return the collection of users with auction related profile, no matter it is admin
	 * user or normal auction seller user
	 * side application, it is used in admin functionality
	 */

	public ArrayList getAllSellers(ArrayList profiles)
		throws BackendException {
		return null;
	}

	/** getter for the sales employee
	 * @return the sales employee
	 */
	public String getSalesEmployee() {
		return salesEmployee;
	}

	/** setter for the sales employee
	 * @param string the sales employee
	 */
	public void setSalesEmployee(String string) {
		salesEmployee = string;
	}

	/**
	 * User login into the r3 system for the auction seller side scenario. 
	 * The login type is SU01 type, and the sales employee us determined from the
	 * user ID.
	 * This also checks the profile name
	 * 
	 * @param password              user password
	 * @param user                  user id
	 * @return indicates if the login is sucessful or not
	 * @exception BackendException  Exception from backend
	 */
	public LoginStatus login(UserBaseData user, String password)
		throws BackendException {

		JCoConnection connection = getCompleteConnection();
		systemId = connection.getAttributes().getSystemID();
		client = connection.getAttributes().getClient();
		String role;

		r3Release = ReleaseInfo.getR3Release(connection);

		if (r3Release == null) {
			throw new BackendException(" r3 release could not be determined");
		} else {

			if (log.isDebugEnabled()) {
				log.debug("R3 Release: " + r3Release);
			}
		}

		//get backendData
		backendData = getOrCreateBackendData();
		user.clearMessages();
		JCoConnection jcoCon;
		String pi = null;
		LoginStatus functionReturnValue = LoginStatus.NOT_OK;
		String nomessage = null;
		String salesEmp = null;
		
		JCO.Table customerlist = null;

		if (!(user instanceof AuctionUserBaseData)){
			log.error("The wrong user type, should be Auction User Type");
			return functionReturnValue;
		}
		try {

			JCO.MetaData jcoMetaData = new JCO.MetaData("USER");
			jcoMetaData.addInfo("TYPE", JCO.TYPE_CHAR, 1, 0, 0, 0, null);
			jcoMetaData.addInfo("ID", JCO.TYPE_CHAR, 20, 2, 0, 0, null);
			jcoMetaData.addInfo("MESSAGE", JCO.TYPE_CHAR, 220, 2, 0, 0, null);
			jcoMetaData.addInfo(
				"LOG_MSG_NO",
				JCO.TYPE_CHAR,
				220,
				2,
				0,
				0,
				null);
			jcoMetaData.addInfo("MESSAGE_V1", JCO.TYPE_CHAR, 50, 2, 0, 0, null);
			jcoMetaData.addInfo("MESSAGE_V2", JCO.TYPE_CHAR, 50, 2, 0, 0, null);
			jcoMetaData.addInfo("MESSAGE_V3", JCO.TYPE_CHAR, 50, 2, 0, 0, null);
			jcoMetaData.addInfo("MESSAGE_V4", JCO.TYPE_CHAR, 50, 2, 0, 0, null);

			JCO.Record message = new JCO.Record(jcoMetaData);

			// get a connection for the login checks
			jcoCon = connection;

			// which login type should be used
			 String loginType = props.getProperty(LOGINTYPE);
			userConceptToUse = Integer.parseInt(loginType);
			/* Profiles are removed as authorization mechanism. Authorization objects are introduced 

			if (((AuctionUserBaseData)user).isAdministrator()){				
				profileName = props.getProperty(AuctionRFCConstantsR3.ADMIN_PROFILENAME);
			}else{
				profileName = props.getProperty(AuctionRFCConstantsR3.SELLER_PROFILENAME);
			}

			if (log.isDebugEnabled()) {
				log.debug("User id is: " + user.getUserId());
				log.debug("login type is: " + userConceptToUse);
			} */

			user.setTechKey(new TechKey(user.getUserId()));

			if (user.getUserId().equals("")) {
				user.addMessage(
					new Message(
						Message.ERROR,
						"login.r3.enteruser",
						null,
						""));
			    return LoginStatus.NOT_OK;
			} else {

				if (password.equals("")) {
					user.addMessage(
						new Message(
							Message.ERROR,
							"user.r3.pwenter",
							null,
							""));
					return LoginStatus.NOT_OK;
				} else {

					switch (userConceptToUse) {

						case 5 :

							// user switch: login with SU05 Internet User from type KNA1, User Id: Customer no., but migrate to SU01 User
							jcoCon.close();
							throw new BackendException("userConceptToUse/loginType=5 not supported without plug-in, please check configuration");

							//break;
						case 6 :

							// login with SU05 Internet User from type BUS1006001, User Id: Contact person no., but migrate to SU01 User
							jcoCon.close();
							throw new BackendException("userConceptToUse/loginType=6 not supported without plug-in, please check configuration");

							//break;

						case 9 :

							// login with SU01 User as sales employee
							boolean isSSo = false;
							StringBuffer modifiedTicket = new StringBuffer("");

							// login with SU01 User as sales employee
							// check whether there is a try to login via a SSO2-ticket
							if (user
								.getUserId()
								.equals(SPECIAL_NAME_AS_USERID)) {
			
								// note: the password string should in fact contain the ticket
								 
								//UserDataProxy userProxy = null; // new UserDataWrapper(user);
								
								functionReturnValue =
								loginViaTicket(
													user, 
													password,
													modifiedTicket);
													
								isSSo = true;
							} else {

								// login with SU01 Internet User
								if (user.getUserId().length() > 12) {
									log.debug(
										"user id too long (more than 12 chars)");
									user.addMessage(
										new Message(
											Message.ERROR,
											"login.r3.problem",
											null,
											""));
									return LoginStatus.NOT_OK;
								} else {
									/// check the user name and password
									functionReturnValue =
										loginWithNewUserConcept(
											jcoCon,
											user,
											password);

								}
							}

							if (functionReturnValue == LoginStatus.OK) {
								switchToPrivateConnection(
										user,userConceptToUse, user.getUserId());
								loginSucessful = true; //set the login to successful.
														//required for changing the password, if required.
								// Find out the role of the user from the backend.
								//it is based on the autorization profile attached to the user in R/3
								role = RFCSellerUserWrapper.getRole(
													jcoCon,
													user);
																												
							
								
								//first check the profileName is right or not, then get employee.
								salesEmployee =
									RFCSellerUserWrapper
										.getSalesEmployeeFromUser(
										jcoCon,
										user,
										r3Release);
								
								if (salesEmployee == null) {
									log.debug("no Sales Employee for user");
									functionReturnValue = LoginStatus.NOT_OK;

									user.addMessage(
										new Message(
											Message.ERROR,
											"login.r3.noempl",
											null,
											""));
									return functionReturnValue;

								} else {
									if (user instanceof AuctionUserBaseData) {
										(
											(
												AuctionUserBaseData) user)
													.setBusinessPartner(
											new TechKey(salesEmployee));
//												getPartnerKey(
//													salesEmployee,
//													true,
//													false)));
										((AuctionUserBaseData) user)
											.getSalesEmployee()
											.setTechKey(
											new TechKey(salesEmployee));
//												getPartnerKey(
//													salesEmployee,
//													true,
//													false)));
										((AuctionUserBaseData) user)
											.getSalesEmployee()
											.setId(
											salesEmployee);
										backendData.setContactLoggedIn(
											salesEmployee);
										if("AD" .equals(role)){
											//enable administrator functionality for the seller
											((AuctionUserBaseData) user).setAdministrator(true);
										}
									}
								}

							}
							jcoCon.close();

							break;
					}

				}
			}
			old_password = password;
		} catch (JCO.Exception ex) {
			user.addMessage(
				new Message(
					Message.ERROR,
					"login.r3.generror",
					null,
					""));
			log.error("Error calling login r3 function:" + ex);					
			JCoHelper.splitException(ex);
		}catch (Exception ex){
			user.addMessage(
				new Message(
					Message.ERROR,
					"login.r3.generror",
					null,
					""));
			log.error("Error calling r3 function:" + ex);
			throw new BackendException("Other Exceptions in User login");
		}

		if (log.isDebugEnabled()) {
			log.debug("new user techkey: " + user.getUserId());
		}

		user.setTechKey(new TechKey(user.getUserId()));

		//this is for shop admin (no call of getBusinessPartnerAdress
		user.setSalutationText("");
		return functionReturnValue;
	}

    /* (non-Javadoc)
     * @see com.sap.isa.auction.backend.boi.user.AuctionUserBackend#getClient()
     */
    public String getClient() {
        if(client == null) {
			JCoConnection connection;
			try {
				connection = getCompleteConnection();
				systemId = connection.getAttributes().getSystemID();
				client = connection.getAttributes().getClient();
			} catch (BackendException e) {
				log.error(e);
			}
        }
        return client;
    }

    /* (non-Javadoc)
     * @see com.sap.isa.auction.backend.boi.user.AuctionUserBackend#getSystemId()
     */
    public String getSystemId() {
		if(systemId == null) {
			JCoConnection connection;
			try {
				connection = getCompleteConnection();
				systemId = connection.getAttributes().getSystemID();
				client = connection.getAttributes().getClient();
			} catch (BackendException e) {
				log.error(e);
			}
		}
        return systemId;
    }
	
	/*
	 * Override to add explicit login after password change. Login will fetch the roles of the user
	 * 
	 * @see com.sap.isa.user.backend.boi.UserBaseBackend#changePassword(com.sap.isa.user.backend.boi.UserBaseData, java.lang.String, java.lang.String)
	 */
	public boolean changePassword(UserBaseData user, String pwd, String pwdVerify)
		throws BackendException {
		
		if (loc.beDebug())
			cat.logT(Severity.DEBUG, loc , "Changing password for user " + user.getUserId());
									
		boolean isSuccessful = super.changePassword(user, pwd, pwdVerify);
		if(isSuccessful){
			LoginStatus status = login(user, pwd);
			if(status == LoginStatus.NOT_OK){
				//change password is successful but the user doesnot have sales employee assigned
				if (loc.beDebug())
					cat.logT(Severity.DEBUG, loc , "password change for user "+ user.getUserId() + " is successful. But employee role might be missing");
													
				//stop the user in LoginRoleAction 
			}
		}
		
		if (loc.beDebug())
			cat.logT(Severity.DEBUG, loc , "Changed the password for user " + user.getUserId() + " Success :" + isSuccessful);
							
		return isSuccessful;
		
	}

	/* 
	 * Override to add explicit login after password change. Login will fetch the roles of the user
	 * @see com.sap.isa.user.backend.boi.UserBaseBackend#changeExpiredPassword(com.sap.isa.user.backend.boi.UserBaseData, java.lang.String, java.lang.String)
	 */
	public boolean changeExpiredPassword(
		UserBaseData user,
		String password_old,
		String password_new)
		throws BackendException {
		
		if (loc.beDebug())
			cat.logT(Severity.DEBUG, loc , "Changing password for user " + user.getUserId());
									
		boolean isSuccessful =  super.changeExpiredPassword(user, password_old, password_new);
		if(isSuccessful){
			LoginStatus status = login(user, password_new);
			if(status == LoginStatus.NOT_OK){
				//change password is successful but the user doesnot have sales employee assigned
				if (loc.beDebug())
					cat.logT(Severity.DEBUG, loc , "password change for user "+ user.getUserId() + " is successful. But employee role might be missing");				
				//stop the user in LoginRoleAction 
			}			
		}
		
		if (loc.beDebug())
			cat.logT(Severity.DEBUG, loc , "Changed the password for user " + user.getUserId() + " Success :" + isSuccessful);
					
		return isSuccessful;
	}
    /**
     * This method tries to login the user by the given SSO2-Logon ticket. If the login
     * was successful, the user object holds the User ID (SU01) afterwards.
     * 
     * @param user                  The user object
     * @param ticket                The SSO ticket that has been retrieved from the cookie
     * @param modifiedTicket         the modified ticket which gets appended the ticket withouts special characters
     * @return The status of the login attempt. Either LoginStatus.NOT_OK or LoginStatus.OK
     * @exception BackendException  Exception from Backend
     */
    protected LoginStatus loginViaTicket(UserBaseData isaR3user,
                                         String ticket, 
                                         StringBuffer modifiedTicket)
                                  throws BackendException {

        // remove the "$MYSAPSSO2$" string as user id
        isaR3user.setUserId("");

        /*
        * Special characters {+, !, =, /} replacement: Special characters are
        * masked with '%' followed by 2 digits which are the hexadecimal representations
        * of specified characters according to ISO 8859-1. The result of the transformation
        * is a (almost) base64-encoded string. Only almost, because the character '!' is not
        * allowed in pure base64 encoding.
        */
        StringBuffer bufTicket = new StringBuffer(ticket);

        for (int i = 0; i < bufTicket.length(); i++) {

            if (bufTicket.charAt(i) == '%') {

                int c = Integer.parseInt(bufTicket.substring(
                                                 i + 1, 
                                                 i + 3), 
                                         16);
                String s = new String(new byte[] { (byte)c });
                bufTicket.replace(i, 
                                  i + 3, 
                                  s);
            }
        }

        modifiedTicket.append(bufTicket);
        Properties loginProps = new Properties();

        // set the properties' values
        loginProps.setProperty(JCoManagedConnectionFactory.JCO_MYSAPSSO2, 
                               bufTicket.toString());
        loginProps.setProperty(JCoManagedConnectionFactory.JCO_LANG, 
                               isaR3user.getLanguage());
                               
        loginProps.setProperty(JCoManagedConnectionFactory.JCO_USER, 
                                                      "");
        loginProps.setProperty(JCoManagedConnectionFactory.JCO_PASSWD,"");
        JCoConnection jcoCon = null;                                              

        try {

            // get a connection to check whether the SSO2-ticket is valid
            // we only need a stateless connection for this test, but with the right
            // connection properties (see above)
            jcoCon = (JCoConnection)getConnectionFactory().getConnection(
                                           com.sap.isa.core.eai.init.InitEaiISA.FACTORY_NAME_JCO, 
                                           com.sap.isa.core.eai.init.InitEaiISA.CON_NAME_ISA_STATELESS, 
                                           loginProps);

            // test whether the JCO connection is valid
            if (!jcoCon.isValid()) {

                if (log.isDebugEnabled()) {
                    log.debug("The received SSO2-ticket is NOT valid.");
                }


                return LoginStatus.NOT_OK;
            }
             else {

                if (log.isDebugEnabled()) {
                    log.debug("The received SSO2-ticket is valid.");
                }


                //get the user id from the connection
                JCO.Function function = jcoCon.getJCoFunction(USER_NAME_GET);

                // call the function
                jcoCon.execute(function);

                //get the user id from the function module
                JCO.ParameterList exportParams = function.getExportParameterList();
                isaR3user.setUserId(exportParams.getString("USER_NAME").trim());

                if (log.isDebugEnabled()) {
                    log.debug("SSO: corresponding SU01 user name is: " + exportParams.getString(
                                                                                 "USER_NAME")
                                .trim());
                }

                return LoginStatus.OK;
            }
        }
        catch (BackendException backendException) {

            //not expected
            if (log.isDebugEnabled()) {
                log.debug("backendException catched" + backendException);
            }
            throw backendException;
        }
        finally {
            if (jcoCon != null)
                jcoCon.close();
        }
        
    }

}