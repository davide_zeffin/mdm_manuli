package com.sap.isa.auction.backend.r3.user.rfc;

/**
 *
 * SAP Labs LLC, the eBay auction project, also is known as Selling via eBay. 
 * Selling via eBay application provides SAP R/3 (and in the future CRM) 
 * customers to auction and sell inventory/products onto an online marketplace 
 * eBay. The application will provide the ability to Create, Schedule, Publish, 
 * Monitor and Close Auction Listings onto eBay and handle all the relevant 
 * Backend processing for Customers and Orders. 
 */

import com.sap.mw.jco.JCO;
import com.sap.tc.logging.*;
import com.sap.isa.auction.logging.CategoryProvider;
import com.sap.isa.backend.JCoHelper;
import com.sap.isa.backend.r3base.rfc.RFCConstants;
import com.sap.isa.backend.r3base.rfc.RFCWrapperPreBase;
import com.sap.isa.core.eai.BackendException;
import com.sap.isa.core.eai.sp.jco.JCoConnection;
import com.sap.isa.user.backend.boi.UserBaseData;
import com.sap.isa.user.backend.r3.MessageR3;

public class RFCSellerUserWrapper extends RFCWrapperPreBase {
	/**
	 * The R3 ObjectType for the sales employee
	 */
	public static final String SALES_EMPLOYEE_OBJECT_TYPE = "EMPLOYEET";
	private final static Category cat = CategoryProvider.getSystemCategory();
	private final static Location loc =
		Location.getLocation(RFCSellerUserWrapper.class);
	/**
	 *  Reads the R/3 sales employee ID for a SU01 user. We use references to
	 * store the the R/3 sales employee within the user and take
	 * ISA_USER_APPLICATION_OBJ_GET
	 * (PlugIn system) or BAPI_USER_APPLICATION_OBJ_GET (non PlugIn system)
	 *  for reading the reference. This is possible only from 4.6c onwards. <p>
	 *
	 *  For earlier releases, we use BAPI_USER_GET_DETAIL and take the
	 *  sales employee
	 *  number from a parameter 'UID'/'UTY'.
	 *
	 *@param  jcoCon                jayco connection
	 *@param  user                  ISA user
	 *@param  r3Release             the R/3 release
	 *@return                       the R/3 customer number. If no customer is found, 
	 * 								 null is returned. The caller has to take care
	 * 								 on the error handling
	 *@exception  BackendException  backend exception
	 */
	public static String getSalesEmployeeFromUser(
		JCoConnection jcoCon,
		UserBaseData user,
		String r3Release)
		throws BackendException {
		//contact here is the sales employee
		String contact = null;
		String objtype = null;
		JCO.Function function = null;
		JCO.ParameterList importParams = null;
		JCO.Table messages = null;
		JCO.Table contactRef = null;

		cat.logT(Severity.DEBUG, loc, "FM getSlaesEmployeeFromUser Starting");
		if (r3Release.equals(RFCConstants.REL46C)
			|| r3Release.equals(RFCConstants.REL620) 
			|| r3Release.equals(RFCConstants.REL640)
			|| r3Release.substring(0,3).compareTo(RFCConstants.REL700) >= 0 ) {

			boolean doWithPIFunc = true;
			try {
				//Check for PlugIn
				function =
					jcoCon.getJCoFunction(RfcName.ISA_USER_APPLICATION_OBJ_GET);
			} catch (BackendException ex) {
				cat.logT(Severity.ERROR, loc, "Error calling r3 function in PlugIn :" + ex);
				cat.logT(Severity.ERROR, loc, "Do with plugin is false, BAPI is used " +						" to retrieve information");
				doWithPIFunc = false;
			}

			if (doWithPIFunc) {
				function =
					jcoCon.getJCoFunction(RfcName.ISA_USER_APPLICATION_OBJ_GET);
				cat.logT(Severity.DEBUG, loc, "Retrieving sales employee using with the PI Function" +					" with ISA_USER_APPLICATION_OBJ_GET	");
				// set the import values
				importParams = function.getImportParameterList();
				JCO.Structure username = importParams.getStructure("USERNAME");
				username.setValue(user.getUserId().toUpperCase(), "BAPIBNAME");
				importParams.setValue(SALES_EMPLOYEE_OBJECT_TYPE, "OBJTYPE");
				try {
					// call the function
					jcoCon.execute(function);
				} catch (JCO.AbapException ex) {
					cat.logT(Severity.ERROR, loc, "In RFCSellerUserWrapper retrieving the sales employee ID," +
						"	Backend exception" + ex.getMessage());
					throw new BackendException("Sales employee ID can not be retrieved");
					// Exception NO_RELATION_FOR_OBJTYPE
				}

				//get the return values
				messages = function.getTableParameterList().getTable("RETURN");

				contactRef =
					function.getTableParameterList().getTable("OBJKEYS");
				cat.logT(Severity.DEBUG, loc, "Do with PI function, the return message is " + 
								messages);
			} else {
				cat.logT(Severity.DEBUG, loc, "Retrieving sales employee using with the BAPI Function" +
					" with BAPI_USER_APPLICATION_OBJ_GET	");
				function =
					jcoCon.getJCoFunction(
						RfcName.BAPI_USER_APPLICATION_OBJ_GET);

				// set the import values
				importParams = function.getImportParameterList();
				JCO.Structure username = importParams.getStructure("USERNAME");
				username.setValue(user.getUserId().toUpperCase(), "BAPIBNAME");
				importParams.setValue(SALES_EMPLOYEE_OBJECT_TYPE, "OBJTYPE");

				try {
					// call the function
					jcoCon.execute(function);
				} catch (JCO.AbapException ex) {
					cat.logT(Severity.DEBUG, loc, "In RFCSellerUserWrapper retrieving the sales employee ID," +						"	Backend exception" + ex.getMessage());
					throw new BackendException("Sales employee ID can not be retrieved");
					// Exception NO_RELATION_FOR_OBJTYPE
				}
				contactRef =
					function.getTableParameterList().getTable("OBJKEYS");
			}
			if (contactRef.getNumRows() > 0) {
				contactRef.firstRow();
				contact = contactRef.getString("OBJKEY");
			}
			cat.logT(Severity.DEBUG, loc, "The sales employee ID is " 
											+ contact);
		} else {
			// get the repository infos of the function that we want to call
			function = jcoCon.getJCoFunction(RfcName.BAPI_USER_GET_DETAIL);
			cat.logT(Severity.DEBUG, loc, "Retrieving sales employee using with the PI " +					"Function" +
				" with BAPI_USER_GET_DETAIL");
			// set the import values
			importParams = function.getImportParameterList();
			importParams.setValue(user.getUserId().toUpperCase(), "USERNAME");

			// call the function
			jcoCon.execute(function);

			//get the return values
			JCO.ParameterList exportParams = function.getExportParameterList();
			JCO.Table par =
				function.getTableParameterList().getTable("PARAMETER");

			if (par.getNumRows() > 0) {
				par.firstRow();
				for (int i = 0; i < par.getNumRows(); i++) {
					if (par.getString("PARID").equals("UID")) {
						contact = par.getString("PARVA");
					}
					if (par.getString("PARID").equals("UTY")) {
						objtype = par.getString("PARVA");
					}
					par.nextRow();
				}
			}
			if (objtype == null
				|| (!objtype.equals(SALES_EMPLOYEE_OBJECT_TYPE))) {
				cat.logT(Severity.DEBUG, loc, "In RFCSellerUserWrapper the ObjectType error, " +						"the ObjectType is " + objtype);
				contact = null;
			}
			messages = function.getTableParameterList().getTable("RETURN");
		}

		user.clearMessages();

		if (contact != null && (!contact.equals(""))) {
			cat.logT(Severity.DEBUG, loc, "The sales employee found for the user " + 
							user.getUserId() + " is " + contact);
			//MessageR3.addMessagesToBusinessObject(user, messages);
		} else {
			cat.logT(Severity.DEBUG, loc, "no sales employee found for user");
			contact = null;
		}
		cat.logT(Severity.DEBUG, loc, "FM getSlaesEmployeeFrom User Ending");
		return contact;
	}

	/**
	 * Check the user profiel with the passing profile
	 * @param  jcoCon                jayco connection
	 * @param  user                  ISA user
	 * @param  profile               The profile Name needs to be checked
	 * @return if the check is successful
	 * @deprecated
	 */
	public static boolean checkProfileForUser(
		JCoConnection jcoCon,
		UserBaseData user,
		String profile)
		throws BackendException {
		boolean validProfile = false;
		// Here we call
		JCO.Function function = null;
		JCO.ParameterList importParams = null;
		JCO.Table messages = null;
		cat.logT(Severity.DEBUG, loc, "FM checkProfileForUser Starting");
		try{
			function = jcoCon.getJCoFunction(RfcName.BAPI_USER_GET_DETAIL);
	
			// set the import values
			importParams = function.getImportParameterList();
			importParams.setValue(user.getUserId().toUpperCase(), "USERNAME");
	
			// call the function
			jcoCon.execute(function);
	
			//get the return values
			JCO.ParameterList exportParams = function.getExportParameterList();
			JCO.Table par =
				function.getTableParameterList().getTable("PROFILES");
			cat.logT(Severity.DEBUG, loc, "The profile is in RFCSeller " + profile);
			if (par.getNumRows() > 0) {
				par.firstRow();
				for (int i = 0; i < par.getNumRows(); i++) {
					cat.logT(Severity.DEBUG, loc, "The gettiong profile is " + 
										par.getString("BAPIPROF"));
										
					if (par.getString("BAPIPROF").equalsIgnoreCase(profile)) {
						validProfile = true;
						cat.logT(Severity.DEBUG, loc, "The profile " + profile + " is used for " +							"the Seller login");
					}				
					par.nextRow();
				}
			}
			messages = function.getTableParameterList().getTable("RETURN");
		} catch (JCO.Exception ex) {
			cat.logT(Severity.DEBUG, loc, "Error calling r3 function:" + ex);
			JCoHelper.splitException(ex);
		}
		if (messages.getNumRows() > 0){		
			MessageR3.addMessagesToBusinessObject(user, messages);
			cat.logT(Severity.DEBUG, loc, "Backend messages retrieved " +											messages);
		}
		cat.logT(Severity.DEBUG, loc, "The profile checking is true/false" + validProfile);
		cat.logT(Severity.DEBUG, loc, "FM checkProfileForUser Ending");
		return validProfile;
	}

	/**
	 * Retrieves the role of the user from the backend R/3 system
	 * @param jcoCon
	 * @param user - user name
	 * @return Role AD-administrator SE- for seller
	 */
	public static String getRole(JCoConnection jcoCon, UserBaseData user) {
		String role = "";
		
		try {
			JCO.Function function = jcoCon.getJCoFunction("ISA_AUC_AUTHORITY_CHECK");
			
			JCO.ParameterList importParams =
									function.getImportParameterList();
			importParams.setValue (user.getUserId().toUpperCase(), "AUC_USERNAME");
				
			jcoCon.execute(function);
			
			JCO.ParameterList exportParams = function.getExportParameterList();
			role = exportParams.getString("AUC_USER_TYPE");
		} catch (BackendException ex) {
			cat.logT(Severity.DEBUG, loc, "Error calling r3 function: ISA_AUC_AUTHORITY_CHECK" + ex);
		}
		return role;
	}
}