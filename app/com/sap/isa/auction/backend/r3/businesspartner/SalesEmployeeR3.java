/*
 * Created on Sep 15, 2003
 *
 * To change the template for this generated file go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
package com.sap.isa.auction.backend.r3.businesspartner;

import com.sap.tc.logging.*;
import com.sap.isa.auction.backend.boi.businesspartner.ResonsibleEmployeeBackend;
import com.sap.isa.auction.backend.boi.businesspartner.ResponsibleSalesEmployeeData;
import com.sap.isa.auction.logging.CategoryProvider;
import com.sap.isa.backend.r3base.util.R3BackendData;
import com.sap.isa.businesspartner.backend.r3.BusinessPartnerR3;
import com.sap.isa.core.eai.BackendException;
import com.sap.isa.core.eai.sp.jco.JCoConnection;
/**
 * @author i009657
 *
 * To change the template for this generated type comment go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
public class SalesEmployeeR3 extends BusinessPartnerR3 implements ResonsibleEmployeeBackend{

	private final static Category cat = CategoryProvider.getSystemCategory();
	private final static Location loc =
		Location.getLocation(SalesEmployeeR3.class);

	/* (non-Javadoc)
	 * @see com.sap.isa.auction.backend.boi.businesspartner.ResonsibleEmployeeBackend#getEmployeeDetails(java.lang.String)
	 */
	public ResponsibleSalesEmployeeData getEmployeeDetails(String employeeId) throws BackendException {
		if (log.isDebugEnabled()) {
			log.debug("get responsible employee data - begin");
		}

		//From the employee ID to get all of the details
		//..
		try {

			R3BackendData backendData = getOrCreateBackendData();
			JCoConnection jcoCon = (JCoConnection)getConnectionFactory().getConnection(
										   com.sap.isa.core.eai.init.InitEaiISA.FACTORY_NAME_JCO, 
										   com.sap.isa.core.eai.init.InitEaiISA.CON_NAME_ISA_STATELESS);

			/**
				RFCWrapperBusinessPartnerR3.getAddressContact(
						new TechKey(partnerKey.toString()), 
						address, 
						partner, 
						jcoCon, 
						backendData);
			*/
		}
		 catch (Exception ex) {

			if (log.isDebugEnabled()) {
				log.debug("Get employee data  - exception occured");
			}
		}
		 finally {
			getDefaultJCoConnection().close();

			// jcoCon.close();
		}
		return null;
	}

}
