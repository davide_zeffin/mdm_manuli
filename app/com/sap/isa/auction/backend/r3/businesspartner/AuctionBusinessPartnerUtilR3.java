/*
 * Created on Sep 15, 2003
 *
 * To change the template for this generated file go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
package com.sap.isa.auction.backend.r3.businesspartner;

import java.util.ArrayList;

import com.sap.tc.logging.*;
import com.sap.isa.auction.backend.boi.businesspartner.AuctionBusinessPartnerBackend;
import com.sap.isa.auction.backend.r3.businesspartner.rfc.RFCWrapperAuctionBPR3;
import com.sap.isa.auction.logging.CategoryProvider;
import com.sap.isa.backend.r3base.util.R3BackendData;
import com.sap.isa.businesspartner.backend.r3.BupaSearchR3;
import com.sap.isa.core.eai.BackendException;
import com.sap.isa.core.eai.sp.jco.JCoConnection;
/**
 * @author i009657
 *
 * To change the template for this generated type comment go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
public class AuctionBusinessPartnerUtilR3
	extends BupaSearchR3
	implements AuctionBusinessPartnerBackend {


	private final static Category cat = CategoryProvider.getSystemCategory();
	private final static Location loc =
		Location.getLocation(AuctionBusinessPartnerUtilR3.class);

	public static final String ONETIME_CUSTOMER_FLAG = "X";
	
	/* This method is used to search all of the oneTime customer in the
	 * R3 system, the creation of oneTime customer is not yet supported.
	 * It is implementing the BAPI BAPI_CUSTOMER_GETLIST
	 * @return all of the one time customer in the system.
	 * @see com.sap.isa.auction.backend.boi.businesspartner.AuctionBusinessPartnerBackend#searchOneTimeCustoms()
	 */
	public ArrayList searchOneTimeCustoms() throws BackendException {
		if (log.isDebugEnabled()) {
			log.debug("Searching all one time customer: ");
		}
//		use the stateless connection fo reading to prevent 
//		R/3 buffering problems
//		JCoConnection jcoCon = getDefaultJCoConnection();
		//..
		try {
		
			R3BackendData backendData = getOrCreateBackendData();
			JCoConnection jcoCon = (JCoConnection)getConnectionFactory().getConnection(
										   com.sap.isa.core.eai.init.InitEaiISA.FACTORY_NAME_JCO, 
										   com.sap.isa.core.eai.init.InitEaiISA.CON_NAME_ISA_STATELESS);
			
			RFCWrapperAuctionBPR3.getAllOneTimeCustomer( 
											 jcoCon, 
											 backendData);
											
		}
		 catch (Exception ex) {
		
			if (log.isDebugEnabled()) {
				log.debug("search all of one time customer - exception occured");
			}
		}
		 finally {
			getDefaultJCoConnection().close();
		
			// jcoCon.close();
		}
		return null;
	}

}
