/*
 * Created on Sep 15, 2003
 *
 * To change the template for this generated file go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
package com.sap.isa.auction.backend.r3.businesspartner.rfc;

import com.sap.mw.jco.JCO;
import com.sap.tc.logging.*;
import com.sap.isa.auction.backend.boi.businesspartner.ResponsibleSalesEmployeeData;
import com.sap.isa.auction.backend.r3.AuctionRFCConstantsR3;
import com.sap.isa.auction.logging.CategoryProvider;
import com.sap.isa.backend.r3base.rfc.RFCWrapperPreBase;
import com.sap.isa.backend.r3base.rfc.ReturnValue;
import com.sap.isa.backend.r3base.util.R3BackendData;
import com.sap.isa.core.eai.BackendException;
import com.sap.isa.core.eai.sp.jco.JCoConnection;
/**
 * @author i009657
 *
 * To change the template for this generated type comment go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
public class RFCWrapperAuctionBPR3 extends RFCWrapperPreBase {
	private final static Category cat = CategoryProvider.getSystemCategory();
	private final static Location loc =
		Location.getLocation(RFCWrapperAuctionBPR3.class);
	
	/**
	 * Get all of the one time customer. 
	 * BAPI_CUSTOMER_GETLIST is called to read the detailed adress data from
	 * R/3.
	 * 
	 * <p></p>
	 * 
	 * @param backendData           R/3 customizing that is not part of the shop
	 * @param connection            connection to the backend
	 * @return messages from the backend
	 * @exception BackendException  exception from backend
	 */
	public static ReturnValue getAllOneTimeCustomer(
												JCoConnection connection, 
												R3BackendData backendData)
										 throws BackendException {

		JCO.Table customerData = null;

		if (log.isDebugEnabled()) {
			log.debug("Get All of the one time customer: ");
		}

		JCO.Function function = connection.getJCoFunction(
		AuctionRFCConstantsR3.BAPI_CUSTOMER_GETLIST);
		JCO.ParameterList importParams = function.getImportParameterList();

		//fill import parameters
		JCO.Table range = function.getTableParameterList().getTable(
										  "IDRANGE");
		range.appendRow();
		range.setValue("I", 
							   "SIGN");
		range.setValue("GT", 
							   "OPTION");

		//fire RFC
		connection.execute(function);

		ReturnValue retVal = new ReturnValue(function.
				getExportParameterList().getStructure(
													 "ADDRESSDATA"), 
											 "");
		return retVal;
	}
	
	/**
	 * call BAPI to get the details BAPI_EMPLOYEE_GETDATA, for given 
	 * employee ID 
	 * @param employee
	 * @param connection
	 * @param backendData
	 * @throws BackendException
	 */
	public static void getSalesEmployeeDetails(
							ResponsibleSalesEmployeeData employee,
							JCoConnection connection, 
							R3BackendData backendData)	
					 throws BackendException {
		//
		if (log.isDebugEnabled()) {
			log.debug("Get the details for a given employee: ");
		}

		JCO.Function function = connection.getJCoFunction(
		AuctionRFCConstantsR3.BAPI_EMPLOYEE_GETDATA);
		JCO.ParameterList importParams = function.getImportParameterList();

	}
}
