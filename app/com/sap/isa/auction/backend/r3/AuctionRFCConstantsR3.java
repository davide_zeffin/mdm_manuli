/**
 *
 * SAP Labs LLC, the eBay auction project, also is known as Selling via eBay. 
 * Selling via eBay application provides SAP R/3 (and in the future CRM) 
 * customers to auction and sell inventory/products onto an online marketplace 
 * eBay. The application will provide the ability to Create, Schedule, Publish, 
 * Monitor and Close Auction Listings onto eBay and handle all the relevant 
 * Backend processing for Customers and Orders. 
 */
package com.sap.isa.auction.backend.r3;

import com.sap.isa.backend.r3base.rfc.RFCConstants;

public interface AuctionRFCConstantsR3 extends RFCConstants {

	public static final String TOTAL_INSURANCE_PRICE_CONDITION = "insCondition";

	/**
	 * This constant is ised to retrieve all the sales org unit in the R3
	 */
	public static final String ISA_GET_SALESORG_LIST = "ISA_GET_SALESORG_LIST";

	/**
	 * This constant is used to retrieve all the distribution channels in
	 * the R3
	 */
	public static final String ISA_GET_DISTR_CHAN_LIS =
		"ISA_GET_DISTR_CHAN_LIST";

	/**
	 * This constant is used to retrieve all the division in the R3
	 */
	public static final String ISA_GET_DIVISION_LIST = "ISA_GET_DIVISION_LIST";

	/**
	 * Get the plant list based on sales org. unit and division
	 */
	public static final String ISA_GET_PLANTS_LIST = "ISA_GET_PLANTS_LIST";

	/**
	 * according to the search criteria to get a list of business
	 * partner, it used in one-time customer search
	 * @
	 */
	public static final String BAPI_CUSTOMER_GETLIST = "BAPI_CUSTOMER_GETLIST";

	/**
	 * BAPI to retrieve the details for a given employee
	 */
	public static final String BAPI_EMPLOYEE_GETDATA = "BAPI_EMPLOYEE_GETDATA";

	/**
	 * Product get details
	 */
	public static final String BAPI_MATERIAL_GET_DETAIL =
											"BAPI_MATERIAL_GET_DETAIL";

	public static final String ISA_GET_SALES_DOC_TYPE =
											"ISA_GET_SALESDOCTYPES_LIST";

	public static final String ISA_GET_CATALOGLIST = "ISA_GET_CATALOG_LIST";

	public static final String ISA_GET_CATALOG_VARIANT = "ISA_GET_VARIANT_LIST";

	public static final String ISA_GET_SHIPPING_CONDITIONS =
		"ISA_GET_SHIPPING_CONDITIONS";

	/**
	 * The property name of the profile name used in user authendication
	 */
	public static final String SELLER_PROFILENAME = "sellerProfile";

	/**
	 * The property name of the profile name for the administrator
	 */
	public static final String ADMIN_PROFILENAME = "adminProfile";

	/**
	 * The property name of the default manual price condition type
	 * It should be HM00 in general
	 */
	public static final String DEFAULT_PRICE_CONDITION = "HM00"; //"EDI1";

	/**
	 * This attribute holds the Business partner role of 
	 * sales employee
	 */
	public static final String ROLE_SALESEMPLOYEE = "VE";

	public static final String TOTAL_PRICE_CONDITION = "totalPriceCondition";
	public static final String TOTAL_TAX_PRICE_CONDIFTION = "taxPriceCondition";
	public static final String TOTAL_SHIPPING_PRICE_CONDITION =
												"shippingPriceCondition";
	public static final String OTHERL_DISCOUNT_CHARGE_PRICE_CONDIFTION =
												"otherPriceCondition";
	public static final String DELIVERY_BLOCK = "deliveryBlock";
	public static final String BILLING_BLOCK = "billingBlock";
}
