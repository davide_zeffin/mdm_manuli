/**
 *
 * SAP Labs LLC, the eBay auction project, also is known as Selling via eBay. 
 * Selling via eBay application provides SAP R/3 (and in the future CRM) 
 * customers to auction and sell inventory/products onto an online marketplace 
 * eBay. The application will provide the ability to Create, Schedule, Publish, 
 * Monitor and Close Auction Listings onto eBay and handle all the relevant 
 * Backend processing for Customers and Orders. 
 */
package com.sap.isa.auction.backend.r3;

import com.sap.mw.jco.JCO;
import com.sap.tc.logging.*;
import com.sap.isa.auction.backend.boi.OrganizationBackend;
import com.sap.isa.auction.logging.CategoryProvider;
import com.sap.isa.backend.r3base.ISAR3BackendObject;
import com.sap.isa.core.eai.BackendException;
import com.sap.isa.core.util.table.Table;
import com.sap.isa.core.util.table.TableRow;
import com.sap.isa.core.eai.sp.jco.JCoConnection;


public class OrganizationR3
	extends ISAR3BackendObject
	implements OrganizationBackend {
	
	// the log class
	private final static Category cat = CategoryProvider.getSystemCategory();
	private final static Location loc =
		Location.getLocation(OrganizationR3.class);
		
	// build a table	
	public final static String DESCRIPTION = "DESCRIPTION";
	public final static String ID = "ID";
	public final static String LONGDESCRIPTION = "LONGDESCRIPTION";
	/**
	 * Retrieve all the sales organization unit
	 * @param language the SAP langaue ID
	 **/
	public Table getSalesOrganizationList(String language)
		throws BackendException {
		JCoConnection connection = getDefaultJCoConnection();
		Table result = getSalesOrganizationList(language, connection);
		connection.close();
		return result;
	}
	
	public static Table getSalesOrganizationList(String language, 
											JCoConnection connection)
		throws BackendException {

		cat.logT(Severity.DEBUG, loc, "read sales organization unit " +										"FromBackend");
		Table table = getHelpTable();
		
		JCO.Function helpValuesGet = connection.getJCoFunction(
							AuctionRFCConstantsR3.ISA_GET_SALESORG_LIST);
							
		JCO.ParameterList importParams = helpValuesGet.getImportParameterList();

		//		
		importParams.setValue(language, 
							  "LANGU");
		
		// now we get the output
		connection.execute(helpValuesGet);

		//compile r3 attributes
		JCO.Table metaTable = helpValuesGet.getTableParameterList().getTable(
									  "SALESORG_LIST");
		convertToUtilTable(metaTable, table, 
							"VKORG", "VTEXT");
		cat.logT(Severity.DEBUG, loc, "ending of read sales organization unit " +
											"FromBackend");
		return table;
	}

	public Table getDivisionList(String language)
		throws BackendException {
		JCoConnection connection = getDefaultJCoConnection();
		Table result = getDivisionList(language, connection);
		connection.close();
		return result;
	}
		
	public static Table getDivisionList(String language, JCoConnection connection)
		throws BackendException {
		cat.logT(Severity.DEBUG, loc, "read divisions From Backend");
		Table table = getHelpTable();
	
		JCO.Function helpValuesGet = connection.getJCoFunction(
							AuctionRFCConstantsR3.ISA_GET_DIVISION_LIST);
						
		JCO.ParameterList importParams = helpValuesGet.getImportParameterList();

		//		
		importParams.setValue(language, 
							  "LANGU");	
		// now we get the output
		connection.execute(helpValuesGet);

		//compile r3 attributes
		JCO.Table metaTable = helpValuesGet.getTableParameterList().getTable(
									  "DIVISION_LIST");
		convertToUtilTable(metaTable, table, 
							"SPART", "VTEXT");
		cat.logT(Severity.DEBUG, loc, "end of read divisions From Backend");
		return table;
	}

	public Table getDistributionChannelList(String language) throws BackendException {
		JCoConnection connection = getDefaultJCoConnection();
		Table result = getDistributionChannelList(language, connection);
		connection.close();
		return result;
	}
	
	public static Table getDistributionChannelList(String language, JCoConnection connection) 
		throws BackendException {
		cat.logT(Severity.DEBUG, loc, "read distribution channel From Backend");
		Table table = getHelpTable();
	
		JCO.Function helpValuesGet = connection.getJCoFunction(
							AuctionRFCConstantsR3.ISA_GET_DISTR_CHAN_LIS);
						
		JCO.ParameterList importParams = helpValuesGet.getImportParameterList();

		//		
		importParams.setValue(language, 
							  "LANGU");
	
		// now we get the output
		connection.execute(helpValuesGet);

		//compile r3 attributes
		JCO.Table metaTable = helpValuesGet.getTableParameterList().getTable(
									  "DISTR_CHAN_LIST");
		convertToUtilTable(metaTable, table, 
							"VTWEG", "VTEXT");
		cat.logT(Severity.DEBUG, loc, "end of read distribution channel " +										" From Backend");					
		return table;
	}

	public Table getSalesAreaList(String language) throws BackendException {
		// TODO Auto-generated method stub
		return null;
	}

	/**
	 * 
	 */
	public Table getPlantList(
		String language,
		String salesOrg,
		String disChannel)
		throws BackendException {
		JCoConnection connection = getDefaultJCoConnection();
		Table result = getPlantList(language, salesOrg, disChannel, connection);
		connection.close();
		return result;
	}
		
	public static Table getPlantList(
		String language,
		String salesOrg,
		String disChannel,
		JCoConnection connection
		)
		throws BackendException {
		cat.logT(Severity.DEBUG, loc, "read plant info From Backend");
		Table table = new Table("AllPLANT");
		table.addColumn(Table.TYPE_STRING, ID);
		table.addColumn(Table.TYPE_STRING, 
					  DESCRIPTION);
		table.addColumn(Table.TYPE_STRING, 
								  LONGDESCRIPTION);
								  
		JCO.Function helpValuesGet = connection.getJCoFunction(
							AuctionRFCConstantsR3.ISA_GET_PLANTS_LIST);
						
		JCO.ParameterList importParams = helpValuesGet.getImportParameterList();

		//		
		importParams.setValue(salesOrg, 
							  "SALESORG");
		importParams.setValue(disChannel, 
								  "DISTR_CHAN");					  
	
		// now we get the output
		connection.execute(helpValuesGet);

		//compile r3 attributes
		JCO.Table metaTable = helpValuesGet.getTableParameterList().getTable(
									  "PLANT_LIST");
									  
		if (metaTable.getNumRows() > 0) {
			metaTable.firstRow();
			do {
				String name = metaTable.getString("WERKS");
				String desc = metaTable.getString("NAME1");
				String longDesc = metaTable.getString("NAME2");
				TableRow row = table.insertRow();
				row.setValue(ID, name);
				row.setValue(DESCRIPTION,desc);
				row.setValue(LONGDESCRIPTION, longDesc);
			}
			 while (metaTable.nextRow());
		}
		cat.logT(Severity.DEBUG, loc, "end of read plant info From Backend");		
		return table;			  
	}

	/**
	 * utility method to create a table 
	 * @
	 */
	protected static Table getHelpTable(){
		Table table = new Table("HelpTable");
		table.addColumn(Table.TYPE_STRING, ID);
		table.addColumn(Table.TYPE_STRING, 
					  DESCRIPTION);

		return table;
		
	}
	
	/**
	 * covert the JCo.Table to the iSA util Table 
	 * @
	 */
	private static void convertToUtilTable(JCO.Table metaTable, Table table,
									String column1, String column2){
		
		if (metaTable.getNumRows() > 0) {
			metaTable.firstRow();
			do {
				String name = metaTable.getString(column1);
				String desc = metaTable.getString(column2);
				TableRow row = table.insertRow();
				row.setValue(ID, name);
				row.setValue(DESCRIPTION,desc);
			}
			 while (metaTable.nextRow());
		}
	}
}
