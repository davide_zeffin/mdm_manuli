/*****************************************************************************
    Copyright (c) 2001, SAPMarkets Palo Alto, USA, All rights reserved.

*****************************************************************************/

package com.sap.isa.auction.backend.r3.products.strategy;

import java.util.List;

import com.sap.isa.core.eai.BackendException;
import com.sap.isa.core.eai.sp.jco.JCoConnection;
/**
 * This class is used to check the existence and ATP checking materials
 *
 **/
public interface CheckStrategy {
	public final String BAPI_MATERIAL_GETLIST = "BAPI_MATERIAL_GETLIST";
	public final String BAPI_MATERIAL_AVAILABILITY = "BAPI_MATERIAL_AVAILABILITY";
	public final String ISA_AUC_MATERIAL_AVAILABILITY = "ISA_AUC_MATERIAL_AVAILABILITY";
	public final String ISA_AUC_MATERIAL_EXISTENCE = "ISA_AUC_MATERIAL_EXISTENCE";


	/** This API performs ATP checking for a given list of products and their
	 * locations
	 * @param auctionlistings, the place holder for product quantity
	 * @param salesarae, the sales area data 
	 * @param originaldata, where those products come from
	 **/ 
	public boolean checkMaterialsATPInR3(List auctionlisting,
									 JCoConnection connection)
							  throws BackendException;

	
    /**
     * Checks if the materials from flat file upload or
     * manual entry are available in R3. The
     * materials are part of the flat file or entered from user interface
     * It does not cover the ATP ckecking. This API can check all the materials
     * even they reside in different plants and locations.
     * <code> posd </code>.
     * Uses different function modules depending on the R/3 release.
     * 
     * @param products             The productdata for checking purpose, it
     * 								contains the product ID and manufacture
     * 								part number
     * @param salesAreaData        If the products exist in a specific sales
     * 								area
     * @param origindata           where the materials are from, for example
     * 								the plant and storage place
     * @param connection            JCO connection
     * @return false if at least one material is not in R/3
     * @exception BackendException exception from backend
     */
    public boolean checkMaterialsInR3(List products,
                                     JCoConnection connection)
                              throws BackendException;

    
}
