/*
 * Created on Sep 29, 2003
 *
 * To change the template for this generated file go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
package com.sap.isa.auction.backend.r3.products.strategy;

import java.util.*;
import com.sap.mw.jco.JCO;

import com.sap.isa.core.eai.BackendException;
import com.sap.isa.core.eai.sp.jco.JCoConnection;
import com.sap.isa.auction.backend.boi.product.*;
import com.sap.isa.auction.backend.boi.SalesAreaData;
import com.sap.isa.auction.businessobject.product.*;
import com.sap.isa.auction.backend.boi.PlantData;
import com.sap.tc.logging.*;
import com.sap.isa.auction.logging.CategoryProvider;
import com.sap.isa.auction.util.StringUtil;
import com.sap.isa.businessobject.BusinessObjectBase;

/**
 * @author I803746
 *
 * To change the template for this generated type comment go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
public class CheckStrategyNonPIImp implements CheckStrategy {

	private final static Category cat = CategoryProvider.getSystemCategory();
	private final static Location loc =
		Location.getLocation(CheckStrategyNonPIImp.class);

	/* (non-Javadoc)
	 * @see com.sap.isa.auction.backend.r3.products.strategy.CheckStrategy#checkMaterialsATPInR3(java.util.ArrayList, com.sap.isa.core.eai.sp.jco.JCoConnection)
	 */
	public boolean checkMaterialsATPInR3(
		List productlisting,
		JCoConnection connection)
		throws BackendException {
		cat.logT(
			Severity.DEBUG,
			loc,
			"single sales area product existense check");

		if (productlisting == null || productlisting.size() <= 0)
			return false;

		Iterator it = productlisting.iterator();
		while (it.hasNext()) {
			StatusCheckable product = (StatusCheckable) it.next();

			ProductStatusData status = product.getStatus();
			if (status != null
				&& status.getErrorCode() == ProductStatusData.ERRCODE_NONEXIST)
				continue;

			PlantData plant = product.getPlant();
			if (plant == null)
				continue;
			String unit = product.getUnit();

			JCO.Function availCheckFunc =
				connection.getJCoFunction(BAPI_MATERIAL_AVAILABILITY);
			JCO.ParameterList input = availCheckFunc.getImportParameterList();
			input.setValue(plant.getPlantID(), "PLANT");
			JCO.Field prodId_field = input.getField("MATERIAL");
			StringUtil.setExtendIdField(prodId_field, product.getProductId());
			input.setValue(unit, "UNIT");
			connection.execute(availCheckFunc);

			//JCO.ParameterList tables = availCheckFunc.getTableParameterList();
			//JCO.Table wmdvsx = tables.getTable("WMDVSX");
			//JCO.Table wmdvex = tables.getTable("WMDVEX");

			JCO.ParameterList output = availCheckFunc.getExportParameterList();
			//JCO.Structure ret = output.getStructure("RETURN");
			double qty = output.getDouble("AV_QTY_PLT");
			//System.out.println(qty);

			status = product.getStatus();
			if (status == null) {
				status = new ProductStatus();
				product.setStatus(status);
			}

			if (qty > 0) {
				status.setQuantity(qty);
				double auctionableQuantity =
					((ProductDetail) product).getQuantity();
				if (qty >= auctionableQuantity)
					status.setAvailable(true);
				else {
					status.setAvailable(false);
					status.setErrorCode(ProductStatusData.ERRCODE_NOT_ENOUGH);
				}
			} else {
				status.setAvailable(false);
				status.setQuantity(0);
				status.setErrorCode(ProductStatusData.ERRCODE_NOT_AVAILABLE);
			}
		}

		return true;
	}

	/* (non-Javadoc)
	 * @see com.sap.isa.auction.backend.r3.products.strategy.CheckStrategy#checkMaterialsInR3(java.util.ArrayList, com.sap.isa.core.eai.sp.jco.JCoConnection)
	 */
	public boolean checkMaterialsInR3(List products, JCoConnection connection)
		throws BackendException {
		cat.logT(Severity.DEBUG, loc, "products existense check");

		Collection subLists = divideBySalesArea(products);
		Iterator it = subLists.iterator();
		boolean allExists = true;
		while (it.hasNext()) {
			List list = (List) it.next();
			if (!checkSingleMaterialExistence(list, connection)){
				allExists = false;	
			}	
		}
		return allExists;
	}

	private boolean checkSingleMaterialExistence(
										List sameSalesAreaList,
										JCoConnection connection)
										throws BackendException {
		cat.logT(
			Severity.DEBUG,
			loc,
			"single sales area product existense check");
		boolean existsMaterial = false;
		if (sameSalesAreaList == null || sameSalesAreaList.size() <= 0)
			return existsMaterial;

		StatusCheckable product = (StatusCheckable) sameSalesAreaList.get(0);
		SalesAreaData salesArea = product.getSalesArea();
		PlantData plant = product.getPlant();

		JCO.Function getListFunc =
			connection.getJCoFunction(BAPI_MATERIAL_GETLIST);
		JCO.ParameterList tables = getListFunc.getTableParameterList();

		if ((salesArea != null && !salesArea.isEmpty())
			|| (plant != null && !plant.isEmpty())) {
			if (salesArea != null && !salesArea.isEmpty()) {
				JCO.Table salesOrg =
					tables.getTable("SALESORGANISATIONSELECTION");
				salesOrg.clear();
				String org = salesArea.getSalesOrganization();
				if (org != null && org.length() > 0) {
					salesOrg.appendRow();
					salesOrg.setValue("I", "SIGN");
					salesOrg.setValue("EQ", "OPTION");
					salesOrg.setValue(org, "SALESORG_LOW");
				}

				JCO.Table distrChan =
					tables.getTable("DISTRIBUTIONCHANNELSELECTION");
				distrChan.clear();
				String channel = salesArea.getDistributionChannel();
				if (channel != null && channel.length() > 0) {
					distrChan.appendRow();
					distrChan.setValue("I", "SIGN");
					distrChan.setValue("EQ", "OPTION");
					distrChan.setValue(channel, "DISTR_CHAN_LOW");
				}
			}

			if (plant != null && !plant.isEmpty()) {
				JCO.Table plantSel = tables.getTable("PLANTSELECTION");
				plantSel.clear();
				String plantId = plant.getPlantID();
				if (plantId != null && plantId.length() > 0) {
					plantSel.appendRow();
					plantSel.setValue("I", "SIGN");
					plantSel.setValue("EQ", "OPTION");
					plantSel.setValue(plantId, "PLANT_LOW");
				}

				JCO.Table stgeLoc = tables.getTable("STORAGELOCATIONSELECT");
				stgeLoc.clear();
				String storage = plant.getSelectedStorageLocation();
				if (storage != null && storage.length() > 0) {
					stgeLoc.appendRow();
					stgeLoc.setValue("I", "SIGN");
					stgeLoc.setValue("EQ", "OPTION");
					stgeLoc.setValue(storage, "STLOC_LOW");
				}
			}

			JCO.Table selection = tables.getTable("MATNRSELECTION");
			selection.clear();
			selection.appendRow();
			selection.setValue("I", "SIGN");
			selection.setValue("CP", "OPTION");
			selection.setValue("*", "MATNR_LOW");
		} else {
			JCO.Table selection = tables.getTable("MATNRSELECTION");
			selection.clear();
			selection.appendRow();
			selection.setValue("I", "SIGN");
			selection.setValue("EQ", "OPTION");
			JCO.Field prodId_field = selection.getField("MATNR_LOW");
			StringUtil.setExtendIdField(prodId_field, product.getProductId());
		}

		JCO.Table materialList = tables.getTable("MATNRLIST");
		materialList.clear();

		connection.execute(getListFunc);

		Set products = new HashSet();
		if (materialList.getNumRows() > 0) {
			materialList.firstRow();
			do {
				String productId = materialList.getString("MATERIAL");
				products.add(StringUtil.RemoveZero(productId));
			} while (materialList.nextRow());
		}

		Iterator it = sameSalesAreaList.iterator();
		while (it.hasNext()) {
			Object item = it.next();
			StatusCheckable element = (StatusCheckable) item;
			BusinessObjectBase bo = (BusinessObjectBase) item;
			ProductStatusData status = element.getStatus();
			if (status == null) {
				status = new ProductStatus();
				element.setStatus(status);
			}

			String originId = element.getProductId();
			if (products.contains(StringUtil.RemoveZero(originId))) {
				status.setExist(true);
				existsMaterial = true;
				cat.logT(
					Severity.INFO,
					loc,
					"product " + originId + " does exist in R3");
			} else {
				status.setExist(false);
				existsMaterial = false;
				cat.logT(
					Severity.INFO,
					loc,
					"product " + originId + " does NOT exist in R3");
				status.setErrorCode(ProductStatusData.ERRCODE_NONEXIST);
				bo.setInvalid();
			}
		}

		return existsMaterial;
	}

	private Collection divideBySalesArea(List originList) {
		Hashtable salesAreaDivision = new Hashtable();
		Iterator it = originList.iterator();
		while (it.hasNext()) {
			StatusCheckable element = (StatusCheckable) it.next();
			Object key = new StatusCheckableHashWrapper(element);
			//			System.out.println(key);
			//			Object key = element.getSalesArea();
			//			if(key == null)
			//				key = element.getProductId();

			List list = (List) salesAreaDivision.get(key);
			if (list == null) {
				list = new ArrayList();
				salesAreaDivision.put(key, list);
			}
			list.add(element);
		}
		return salesAreaDivision.values();
	}
}
