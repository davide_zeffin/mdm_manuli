/*
 * Created on Sep 15, 2003
 *
 * To change the template for this generated file go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
package com.sap.isa.auction.backend.r3.products;

import java.util.*;

import com.sap.mw.jco.JCO;
import com.sap.isa.auction.backend.boi.product.ProductsBackend;
import com.sap.isa.backend.r3base.ISAR3BackendObject;
import com.sap.isa.core.eai.BackendException;
import com.sap.isa.auction.backend.r3.products.strategy.*;
import com.sap.isa.core.eai.BackendBusinessObjectParams;
import com.sap.isa.core.eai.sp.jco.JCoConnection;
import com.sap.isa.auction.businessobject.product.ProductDetail;
import com.sap.isa.auction.util.StringUtil;
import com.sap.isa.auction.backend.boi.PlantData;
import com.sap.isa.auction.backend.boi.SalesAreaData;
import com.sap.isa.auction.backend.r3.OrganizationR3;
import com.sap.isa.auction.backend.boi.product.*;
import com.sap.isa.core.util.table.*;

/**
 * @author i009657
 *
 * To change the template for this generated type comment go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
public class ProductAPIR3
	extends ISAR3BackendObject
	implements ProductsBackend {

	boolean usePlugIn = true;

	public void initBackendObject(
		Properties props,
		BackendBusinessObjectParams params)
		throws BackendException {

		String strPlugIn = props.getProperty("PlugIn");

		if (strPlugIn != null && strPlugIn.equalsIgnoreCase("false"))
			usePlugIn = false;
		else
			usePlugIn = true;

		super.initBackendObject(props, params);

	}

	private CheckStrategy strategyInstance;

	private CheckStrategy getCheckStrtegy() {
		if (strategyInstance == null) {
			if (usePlugIn)
				strategyInstance = new CheckStrategyPIImp();
			else
				strategyInstance = new CheckStrategyNonPIImp();
		}

		//System.out.println("strategyInstance=" + strategyInstance);
		return strategyInstance;
	}

	/* (non-Javadoc)
	 * @see com.sap.isa.auction.backend.boi.product.ProductsBackend#getProductsDetails(java.util.ArrayList)
	 */
	public List getProductsDetails(List products, String lang)
		throws BackendException {
		JCoConnection jcoCon = this.getDefaultJCoConnection();
		Iterator it = products.iterator();
		while(it.hasNext())
		{
			ProductDetail product = (ProductDetail)it.next();
			getProductDetail(product, jcoCon);
		}
		jcoCon.close();
		return products;
	}

	/* (non-Javadoc)
	 * @see com.sap.isa.auction.backend.boi.product.ProductsBackend#checkExistence(java.util.ArrayList, com.sap.isa.auction.backend.boi.SalesAreaData, java.util.ArrayList)
	 */
	public boolean checkExistence(List products) throws BackendException {
		JCoConnection jcoCon = this.getDefaultJCoConnection();
		boolean isExist = true;
		isExist = getCheckStrtegy().checkMaterialsInR3(products, jcoCon);
		jcoCon.close();
		return isExist;
	}

	/* (non-Javadoc)
	 * @see com.sap.isa.auction.backend.boi.product.ProductsBackend#performATPCheck(java.util.HashMap)
	 */
	public boolean performATPCheck(List products)
		throws BackendException {
		JCoConnection jcoCon = this.getDefaultJCoConnection();
		getCheckStrtegy().checkMaterialsATPInR3(products, jcoCon);
		jcoCon.close();
		return true;
	}

	/* (non-Javadoc)
	 * @see com.sap.isa.auction.backend.boi.product.ProductsBackend#performCompleteCheck(com.sap.isa.auction.backend.boi.SalesAreaData, java.util.HashMap, java.util.HashMap)
	 */
	public boolean performCompleteCheck(List products)
		throws BackendException {
		JCoConnection jcoCon = this.getDefaultJCoConnection();
		boolean checkResult = true;
		checkResult = getCheckStrtegy().checkMaterialsInR3(products, jcoCon);
		getCheckStrtegy().checkMaterialsATPInR3(products, jcoCon);
		detailedErrorCheck(products, jcoCon);
		jcoCon.close();
		return checkResult;
	}

	private void detailedErrorCheck(List products, JCoConnection jcoCon)
		throws BackendException
	{
		String language = jcoCon.getJCoClient().getLanguage();
		if(language == null || language.length()<=0)
		 	language = "EN";

		Table salesOrgList = OrganizationR3.getSalesOrganizationList(language, jcoCon);
		Set salesOrgSet = convertTableToHash(salesOrgList);
		Table distrChanList = OrganizationR3.getDistributionChannelList(language, jcoCon);
		Set distrChanSet = convertTableToHash(distrChanList);
		Table divisionList = OrganizationR3.getDivisionList(language, jcoCon);
		Set divisionSet = convertTableToHash(divisionList);
		
		Iterator it =  products.iterator();
		while(it.hasNext())
		{
			StatusCheckable product = (StatusCheckable)it.next();

			ProductStatusData status = product.getStatus();
			if(status == null || status.getErrorCode() != ProductStatusData.ERRCODE_NONEXIST)
				continue;
				
			if(!checkMaterialInSalesArea(product, false, jcoCon))
			{
				status.setErrorCode(ProductStatusData.ERRCODE_WRONG_PRODUCTID);
				continue;		
			}
			
			SalesAreaData salesArea = product.getSalesArea();
			if(salesArea == null || salesArea.isEmpty())
				continue;
				
            String salesOrg = salesArea.getSalesOrganization();
            if(salesOrg != null && salesOrg.length()>0)
            {
                if(!salesOrgSet.contains(salesOrg))
                {
                    status.setErrorCode(ProductStatusData.ERRCODE_SALESORG_NONEXIST);
                    continue;
                }
            }
                
            String respSalesOrg = salesArea.getResponsibleSalesOrganization();
            if(respSalesOrg != null && respSalesOrg.length()>0)
            {
                if(!salesOrgSet.contains(respSalesOrg))
                {
                    status.setErrorCode(ProductStatusData.ERRCODE_RESPSALESORG_NONEXIST);
                    continue;
                }
            }
                
			String distrChan = salesArea.getDistributionChannel();
			if(distrChan != null && distrChan.length()>0)
			{
				if(!distrChanSet.contains(distrChan))
				{
					status.setErrorCode(ProductStatusData.ERRCODE_DISTRCHAN_NONEXIST);
					continue;
				}
			}
				
			String division = salesArea.getDivision();
			if(division != null && division.length()>0)
			{
				if(!divisionSet.contains(division))
				{
					status.setErrorCode(ProductStatusData.ERRCODE_DIVISION_NONEXIST);
					continue;
				}
			}
			
			PlantData plant = product.getPlant(); 
			if(plant == null || plant.isEmpty())
				continue;
				
			String plantId = plant.getPlantID();
			if(plantId != null && plantId.length()>0)
			{
				Table plantList = OrganizationR3.getPlantList(language, salesOrg, distrChan, jcoCon);
				int rows = plantList.getNumRows();
				boolean exist = false;
				for(int i=1; i<=rows; i++)
				{
					TableRow row = plantList.getRow(i);
					if(row.getField(OrganizationR3.ID).getString().equals(plantId))
						exist = true;
				}
				if(!exist)
				{
					status.setErrorCode(ProductStatusData.ERRCODE_PLANT_NONEXIST);
					continue;	
				}
			}	
			
			if(checkMaterialInSalesArea(product, true, jcoCon))
				status.setErrorCode(ProductStatusData.ERRCODE_WRONG_PLANT);
			else
				status.setErrorCode(ProductStatusData.ERRCODE_WRONG_SALESAREA); 
		}	
	}
	
	private Set convertTableToHash(Table table)
	{
		HashSet result = new HashSet();
		int rows = table.getNumRows();
		for(int i=1; i<=rows; i++)
		{
			TableRow row = table.getRow(i);
			String id = row.getField(OrganizationR3.ID).getString();
			result.add(id);
		}
		return result;
	}

	private boolean checkMaterialInSalesArea(
		StatusCheckable product, 
		boolean checkSalesArea,
		JCoConnection connection) 
		throws BackendException {
		
		JCO.Function getListFunc = connection.getJCoFunction(CheckStrategy.BAPI_MATERIAL_GETLIST);
		JCO.ParameterList tables = getListFunc.getTableParameterList();
		
		if(checkSalesArea)
		{
			SalesAreaData salesArea = product.getSalesArea();
			if(salesArea == null || salesArea.isEmpty())
				return true;
		
			JCO.Table salesOrg = tables.getTable("SALESORGANISATIONSELECTION");
			salesOrg.clear();
			String org = salesArea.getSalesOrganization();
			if(org != null && org.length()>0)
			{
				salesOrg.appendRow();
				salesOrg.setValue("I", "SIGN");
				salesOrg.setValue("EQ", "OPTION");
				salesOrg.setValue(org, "SALESORG_LOW");
			}
					
			JCO.Table distrChan = tables.getTable("DISTRIBUTIONCHANNELSELECTION");
			distrChan.clear();
			String channel = salesArea.getDistributionChannel();
			if(channel != null && channel.length()>0)
			{
				distrChan.appendRow();
				distrChan.setValue("I", "SIGN");
				distrChan.setValue("EQ", "OPTION");
				distrChan.setValue(channel, "DISTR_CHAN_LOW");
			}
		}
				
		JCO.Table selection = tables.getTable("MATNRSELECTION");
		selection.clear();
		selection.appendRow();
		selection.setValue("I", "SIGN");
		selection.setValue("EQ", "OPTION");
		JCO.Field prodId_field = selection.getField("MATNR_LOW");
		StringUtil.setExtendIdField(prodId_field, product.getProductId()); 
		
		JCO.Table materialList = tables.getTable("MATNRLIST");
		materialList.clear();
		
		connection.execute(getListFunc);
		
		Set products = new HashSet();
		if(materialList.getNumRows()>0)
			return true;
		else 
			return false;
	}
	
	protected void getProductDetail(ProductDetail product, JCoConnection jcoCon) throws BackendException
	{
		JCO.Function function = jcoCon.getJCoFunction("BAPI_MATERIAL_GET_DETAIL");
		JCO.ParameterList importParam = function.getImportParameterList();
		JCO.Field prodId_field = importParam.getField("MATERIAL");
		StringUtil.setExtendIdField(prodId_field, product.getProductId()); 
		PlantData plant = product.getPlant();
		if(plant != null)
		{
			String plantId = plant.getPlantID();
			if(plantId != null && plantId.length()>0)
				importParam.setValue(plantId, "PLANT");
		}
		
		jcoCon.execute(function);
		
		JCO.ParameterList exportParam = function.getExportParameterList();
		if(!exportParam.isInitialized("MATERIAL_GENERAL_DATA"))
			return;
		JCO.Structure materialInfo = exportParam.getStructure("MATERIAL_GENERAL_DATA");
		if(materialInfo == null)
			return;
		String description = materialInfo.getString("MATL_DESC");
		if (description == null)
			description = "";
		product.setDescription(description);
		product.addAttribute(ProductDetail.DESCRIPTION, description);
		String oldMtrNum = materialInfo.getString("OLD_MAT_NO");
		if (oldMtrNum == null)
			oldMtrNum = "";
		product.setOldMaterialNumber(oldMtrNum);
		product.addAttribute(ProductDetail.OLDMATNUMBER, oldMtrNum);
		String mtrGroup = materialInfo.getString("MATL_GROUP");
		if (mtrGroup == null)
			mtrGroup = "";		
		product.setMaterialGroup(mtrGroup);
		product.addAttribute(ProductDetail.MATGROUP, mtrGroup);
		String manuPart = materialInfo.getString("MANU_MAT");
		if (manuPart == null)
			manuPart = "";				
		product.setManufactPartNumber(manuPart);
		product.addAttribute(ProductDetail.PARTNUMBER, manuPart);
		String baseUOM = materialInfo.getString("BASE_UOM");
		if (baseUOM == null)
			baseUOM = "";		
		product.setBaseUOM(baseUOM);
		product.addAttribute(ProductDetail.BASEUOM, baseUOM);
		String grossWt = materialInfo.getString("GROSS_WT");
		if (grossWt == null)
			grossWt = "";		
		product.setGrossWeight(grossWt);
		product.addAttribute(ProductDetail.GROSSWEIGHT, grossWt);
		String wtUnit = materialInfo.getString("UNIT_OF_WT");
		if (wtUnit == null)
			wtUnit = "";		
		product.setWeightUnit(wtUnit);
		product.addAttribute(ProductDetail.WEIGHTUNIT, wtUnit);
		String netWt = materialInfo.getString("NET_WEIGHT");
		if (netWt == null)
			netWt = "";		
		product.setNetWeight(netWt);
		product.addAttribute(ProductDetail.NETWEIGHT, netWt);
		String vol = materialInfo.getString("VOLUME");
		if (vol == null)
			vol = "";		
		product.setVolume(vol);
		product.addAttribute(ProductDetail.VOLUME, vol);
		String volUnit = materialInfo.getString("VOLUMEUNIT");
		if (volUnit == null)
			volUnit = "";		
		product.setVolumeUnit(volUnit);
		product.addAttribute(ProductDetail.VOLUMEUNIT, volUnit);
		String dim = materialInfo.getString("SIZE_DIM");
		if (dim == null)
			dim = "";		
		product.setDimensions(dim);
		product.addAttribute(ProductDetail.DIMENSIONS, dim);
		String ean = materialInfo.getString("EAN_UPC");
		if (ean == null)
			ean = "";		
		product.setEAN(ean);
		product.addAttribute(ProductDetail.EAN, ean);
		String eanCat = materialInfo.getString("EAN_CAT");
		if (eanCat == null)
			eanCat = "";		
		product.setEANCategory(eanCat);
		product.addAttribute(ProductDetail.EANCATEGORY, eanCat);
		String baseText = materialInfo.getString("STD_DESCR");
		if (baseText == null)
			baseText = "";		
		product.setBasicText(baseText);
		product.addAttribute(ProductDetail.BASICTEXT, baseText);
		//call customer specific implementation for more product 
		//attributes.
		getCustomerProductExtensionExit(product, jcoCon);
	}

	/**
	 * This is the customer specific implementation method to get 
	 * more material master attributes
	 * @param product
	 * @param jcoCon
	 */
	protected void getCustomerProductExtensionExit(ProductDetail product, 
												JCoConnection jcoCon) {
		//implemented by customer specific implementation
	}
	
}
