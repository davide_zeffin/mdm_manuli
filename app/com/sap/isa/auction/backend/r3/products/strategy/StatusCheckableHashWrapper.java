/*
 * Created on Nov 25, 2003
 *
 * To change the template for this generated file go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
package com.sap.isa.auction.backend.r3.products.strategy;

import com.sap.isa.auction.backend.boi.PlantData;
import com.sap.isa.auction.backend.boi.SalesAreaData;
import com.sap.isa.auction.backend.boi.product.StatusCheckable;
import com.sap.isa.auction.util.StringUtil;

/**
 * @author I803746
 *
 * To change the template for this generated type comment go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
public class StatusCheckableHashWrapper {
	private StatusCheckable data;
	
	public StatusCheckableHashWrapper(StatusCheckable data)
	{
		this.data = data;
	}
	
	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	public boolean equals(Object obj) {
		if( data == null ||
			obj == null || 
		 	!(obj instanceof StatusCheckableHashWrapper) || 
		 	((StatusCheckableHashWrapper)obj).data == null)
			return false;
			
		StatusCheckableHashWrapper other = (StatusCheckableHashWrapper)obj;

		return 
			StringUtil.ObjectCompare(this.data.getSalesArea(), other.data.getSalesArea()) &&
			StringUtil.ObjectCompare(this.data.getPlant(), other.data.getPlant());
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	public int hashCode() {
		int hash = this.toString().hashCode();
		return hash;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	public String toString() {
		if(data == null)
			return null;
			
		SalesAreaData salesArea = data.getSalesArea();
		PlantData plant = data.getPlant();
			
		if((salesArea == null || salesArea.isEmpty()) && 
			(plant == null || plant.isEmpty()))
			return data.getProductId();
			
		StringBuffer buffer = new StringBuffer();
		buffer.append("SalesArea{");
		if(salesArea != null && !salesArea.isEmpty())
			buffer.append(salesArea);
		buffer.append("};Plant{");
		if(plant != null && !plant.isEmpty())
			buffer.append(plant);
		buffer.append("}");
		return buffer.toString();
	}
	
	public boolean isEmpty()
	{
		if(data == null)
			return true;
		return data.getPlant().isEmpty() && data.getSalesArea().isEmpty();
	}

}
