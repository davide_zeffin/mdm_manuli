/*
 * Created on Oct 2, 2003
 *
 * To change the template for this generated file go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
package com.sap.isa.auction.backend.r3.products.strategy;

import java.util.*;
import com.sap.mw.jco.JCO;

import com.sap.isa.core.eai.BackendException;
import com.sap.isa.core.eai.sp.jco.JCoConnection;
import com.sap.isa.auction.backend.boi.product.*;
import com.sap.isa.auction.backend.boi.SalesAreaData;
import com.sap.isa.auction.businessobject.product.*;
import com.sap.tc.logging.*;
import com.sap.isa.auction.logging.CategoryProvider;
import com.sap.isa.auction.util.StringUtil;
import com.sap.isa.businessobject.BusinessObjectBase;

/**
 * @author I803746
 *
 * To change the template for this generated type comment go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
public class CheckStrategyPIImp implements CheckStrategy {

	private final static Category cat = CategoryProvider.getSystemCategory();
	private final static Location loc =
		Location.getLocation(CheckStrategyPIImp.class);

	/* (non-Javadoc)
	 * @see com.sap.isa.auction.backend.r3.products.strategy.CheckStrategy#checkMaterialsATPInR3(java.util.List, com.sap.isa.core.eai.sp.jco.JCoConnection)
	 */
	public boolean checkMaterialsATPInR3(
		List originlisting,
		JCoConnection connection)
		throws BackendException {
			cat.logT(Severity.DEBUG, loc, "check product availability with R/3 plugin");

			if (originlisting == null || originlisting.size() <= 0)
				return false;

			List auctionlisting = new ArrayList();
			Iterator it = originlisting.iterator();
			while(it.hasNext())
			{
				StatusCheckable product = (StatusCheckable)it.next();
				ProductStatusData status = product.getStatus();	
				if(status != null && status.getErrorCode() == ProductStatusData.ERRCODE_NONEXIST)
					continue;
				auctionlisting.add(product);
			}
		
			JCO.Function getListFunc =
				connection.getJCoFunction(ISA_AUC_MATERIAL_AVAILABILITY);
			JCO.ParameterList tables = getListFunc.getTableParameterList();

			JCO.Table prodList = tables.getTable("PRODUCTLIST");
			prodList.clear();
			it = auctionlisting.iterator();
			while(it.hasNext())
			{
				StatusCheckable product = (StatusCheckable)it.next();
				prodList.appendRow();
				JCO.Field prodId_field = prodList.getField("MATERIAL");
				StringUtil.setExtendIdField(prodId_field, product.getProductId()); 
				prodList.setValue(product.getUnit(), "UNIT");
				String plantId;
				if(product.getPlant() == null)
					plantId="null";
				else
					plantId = product.getPlant().getPlantID();
				prodList.setValue(plantId, "PLANT"); 
			}

			
			cat.logT(Severity.DEBUG, loc, "import:" + prodList);
			
			connection.execute(getListFunc);

			cat.logT(Severity.DEBUG, loc, "export:" + prodList);
			
			prodList.firstRow();
			it = auctionlisting.iterator();
			while(it.hasNext())
			{
				StatusCheckable product = (StatusCheckable)it.next();
				ProductStatusData status = product.getStatus();
				if (status == null) {
					status = new ProductStatus();
					product.setStatus(status);
				}
			
				double qty = prodList.getDouble("AV_QTY_PLT");
				if(qty > 0)
				{
					status.setQuantity(qty);
					double auctionableQuantity = ((ProductDetail)product).getQuantity();
					if(qty >= auctionableQuantity)
						status.setAvailable(true);
					else
					{
						status.setAvailable(false);
						status.setErrorCode(ProductStatusData.ERRCODE_NOT_ENOUGH);
					}
				}
				else
				{
					status.setAvailable(false);
					status.setQuantity(0);
					status.setErrorCode(ProductStatusData.ERRCODE_NOT_AVAILABLE);
				}
				prodList.nextRow();
			}

			return true;
	}

	/* (non-Javadoc)
	 * @see com.sap.isa.auction.backend.r3.products.strategy.CheckStrategy#checkMaterialsInR3(java.util.List, com.sap.isa.core.eai.sp.jco.JCoConnection)
	 */
	public boolean checkMaterialsInR3(List products, JCoConnection connection)
		throws BackendException {
		cat.logT(Severity.DEBUG, loc, "check product existense with R/3 plugin");

		if (products == null || products.size() <= 0)
			return false;


		JCO.Function getListFunc =
			connection.getJCoFunction(ISA_AUC_MATERIAL_EXISTENCE);
		JCO.ParameterList tables = getListFunc.getTableParameterList();

		JCO.Table prodList = tables.getTable("PRODUCTLIST");
		prodList.clear();
		Iterator it = products.iterator();
		while(it.hasNext())
		{
			StatusCheckable product = (StatusCheckable)it.next();
			prodList.appendRow();
			JCO.Field prodId_field = prodList.getField("PRODUCTID");
			StringUtil.setExtendIdField(prodId_field, product.getProductId()); 
			SalesAreaData salesArea = product.getSalesArea();
			if(salesArea != null && !salesArea.isEmpty())
			{
				prodList.setValue(salesArea.getSalesOrganization(), "SALESORG");
                prodList.setValue(salesArea.getResponsibleSalesOrganization(), "SALESORGRESP"); // TODO: check parameter's name
				prodList.setValue(salesArea.getDistributionChannel(), "DISTR_CHAN");
				prodList.setValue(salesArea.getDivision(), "DIVISION");
			}
			if(product.getPlant() != null)
			{
				prodList.setValue(product.getPlant().getPlantID(), "PLANT");
				prodList.setValue(product.getPlant().getSelectedStorageLocation(), "STGE_LOC");
			} 
		}

		cat.logT(Severity.DEBUG, loc, "import:" + prodList);
		connection.execute(getListFunc);
		cat.logT(Severity.DEBUG, loc, "export:" + prodList);

		prodList.firstRow();
		it = products.iterator();
		while(it.hasNext())
		{
			Object item = it.next();
			StatusCheckable product = (StatusCheckable)item;
			BusinessObjectBase bo = (BusinessObjectBase)item;
			ProductStatusData status = product.getStatus();
			if (status == null) {
				status = new ProductStatus();
				product.setStatus(status);
			}
			
			String flag = prodList.getString("EXISTFLAG");
			if(flag != null && flag.equals("X"))
				status.setExist(true);
			else
			{
				status.setExist(false);
				status.setErrorCode(ProductStatusData.ERRCODE_NONEXIST);
				bo.setInvalid();
			}
			prodList.nextRow();
		}

		return true;
	}

}
