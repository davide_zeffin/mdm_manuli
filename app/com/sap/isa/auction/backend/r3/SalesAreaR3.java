/**
 *
 * SAP Labs LLC, the eBay auction project, also is known as Selling via eBay. 
 * Selling via eBay application provides SAP R/3 (and in the future CRM) 
 * customers to auction and sell inventory/products onto an online marketplace 
 * eBay. The application will provide the ability to Create, Schedule, Publish, 
 * Monitor and Close Auction Listings onto eBay and handle all the relevant 
 * Backend processing for Customers and Orders. 
 */
package com.sap.isa.auction.backend.r3;
import com.sap.isa.backend.r3base.ISAR3BackendObject;
import com.sap.isa.core.eai.BackendException;
import com.sap.isa.core.util.table.Table;
import com.sap.isa.auction.backend.boi.SalesAreaBackend;

public class SalesAreaR3 extends ISAR3BackendObject implements 
														SalesAreaBackend{

	public Table getDistributionChannels(String language, String salesOrgId) 
								throws BackendException {
		// TODO The backend function module is not ready yet.
		return null;
	}

	public Table getDivisions(String language, String salesOrgId, String disChannel) 
									throws BackendException {
		// TODO The backend function module is not ready yet.
		return null;
	}
	
}
