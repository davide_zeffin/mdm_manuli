/**
 *
 * SAP Labs LLC, the eBay auction project, also is known as Selling via eBay. 
 * Selling via eBay application provides SAP R/3 (and in the future CRM) 
 * customers to auction and sell inventory/products onto an online marketplace 
 * eBay. The application will provide the ability to Create, Schedule, Publish, 
 * Monitor and Close Auction Listings onto eBay and handle all the relevant 
 * Backend processing for Customers and Orders. 
 */
package com.sap.isa.auction.backend.r3;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import com.sap.mw.jco.JCO;
import com.sap.tc.logging.Category;
import com.sap.tc.logging.Location;
import com.sap.tc.logging.Severity;
import com.sap.isa.auction.logging.CategoryProvider;
import com.sap.isa.backend.r3base.rfc.RFCConstants;
import com.sap.isa.backend.r3base.rfc.SalesDocumentHelpValues;
import com.sap.isa.core.TechKey;
import com.sap.isa.core.cache.Cache;
import com.sap.isa.core.eai.BackendException;
import com.sap.isa.core.eai.sp.jco.JCoConnection;
import com.sap.isa.core.util.table.Table;
import com.sap.isa.core.util.table.TableRow;
import com.sap.isa.user.backend.r3.MessageR3;


public class ExtendSalesDocumentHelpValues extends SalesDocumentHelpValues {
	private final static Category cat = CategoryProvider.getSystemCategory();
	private final static Location loc =
		Location.getLocation(ExtendSalesDocumentHelpValues.class);
		
	/** The single instance. */
	protected static ExtendSalesDocumentHelpValues extSalesDocumentHelpValues = null;

	private final static String DESCRIPTION = "DESCRIPTION";
	private final static String ID = "ID";

	public final static int HELP_TYPE_EXTEND_START = 100;
	
	/** Help value for shipping conditions. */
	public final static int HELP_TYPE_DLV_BLOCK = 100;

	/** Array holding R3 fields for each help value type. */
	protected final static String[] EXT_HELP_R3_FIELDS = { ("DLV_BLOCK") };

	/** Array holding R3 business object parameters for each help value type. */
	protected final static String[] EXT_HELP_R3_PARAMETERS = { ("ORDERHEADERIN") };

	/** Array holding R3 business object methods for each help value type. */
	protected final static String[] EXT_HELP_R3_METHODS = { ("CREATEFROMDATA") };

	/** Array holding R3 business object names for each help value type. */
	protected final static String[] EXT_HELP_R3_OBJNAMES = { ("SALESORDER") };

	/** Array holding R3 business object types for each help value type. */
	protected final static String[] EXT_HELP_R3_OBJTYPES = { ("BUS2032") };

	/** For each type, the fields of the resulting ISA table are listed. */
	protected final static String[][] EXT_HELP_TABLE_NAMES = { { (DESCRIPTION) } };

	/** The field types. */
	protected final static int[][] EXT_HELP_TABLE_TYPES = { { (Table.TYPE_STRING) } };
	
	/** For each type, the corresponding R/3 field to HELP_TABLE_NAMES are listed. */
	protected final static String[][] EXT_HELP_TABLE_NAMES_R3 = { { ("VTEXT") } };
	
	/** For each type, the R/3 representation for the row key is given. */
	protected final static String[] EXT_HELP_ROW_KEYS = { ("LIFSP") };
	
	/** What is the key field. */
	protected final static String[][] EXT_HELP_KEY_RELATION = { { (KEY), (DESCRIPTION) } };
	
	/**
	 * Retrieve the single instance of the SalesDocumentHelpValues class.
	 * 
	 * @return The singleton instance
	 */
	public static synchronized SalesDocumentHelpValues getInstance() {

		if (extSalesDocumentHelpValues == null) {
			extSalesDocumentHelpValues = new ExtendSalesDocumentHelpValues();

			try {
				extSalesDocumentHelpValues.access = Cache.getAccess(
														 CACHE_NAME);
			}
			 catch (Cache.Exception e) {
				cat.log(Severity.ERROR, loc, MessageR3.ISAR3MSG_CACHE, e);
			}
		}

		return extSalesDocumentHelpValues;
	}
	
	/**
	 * Create the list of help values. Not synchronized, synchronization is done within
	 * the calling methods.
	 * 
	 * @param type                  the help value type
	 * @param language              current language
	 * @param configKey             configuration key
	 * @param createMap             Shall we as well create a key/value map for the help
	 *        values?
	 * @param connection            connection to the backend
	 * @return an ISA table of help values
	 * @exception BackendException  exception from backend
	 */
	protected Table createHelpValueList(int type, 
										JCoConnection connection, 
										String language, 
										String configKey, 
										boolean createMap)
								 throws BackendException {
		
		if(type < HELP_TYPE_EXTEND_START)
			return super.createHelpValueList(type, 
								connection, 
								language, 
								configKey, 
								createMap);	

		String listKey = getListKeyForCaching(type, 
											  language, 
											  configKey);
		String mapKey = getMapKeyForCaching(type, 
											language, 
											configKey);

		//only return if the map needs not to be created
		//or it is already present
		if (access.get(listKey) != null) {

			if ((!createMap) || (access.get(mapKey) != null))

				return (Table)access.get(listKey);
		}

		cat.logT(Severity.DEBUG, loc, "creation for : " + listKey);

		//fill input for RFC

		/*
		* JCO.Function getDetailedList = connection.getJCoFunction("BAPISDORDER_GETDETAILEDLIST");
		* JCO.ParameterList importParams = getDetailedList.getImportParameterList();
		* JCO.ParameterList tableParams = getDetailedList.getTableParameterList();
		*/
		JCO.Function helpValuesGet = connection.getJCoFunction(
											 "BAPI_HELPVALUES_GET");
		JCO.ParameterList importParams = helpValuesGet.getImportParameterList();

		type = type - HELP_TYPE_EXTEND_START;
		
		//GetHelpValues.Bapi_Helpvalues_Get.Request request = new GetHelpValues.Bapi_Helpvalues_Get.Request();
		importParams.setValue(EXT_HELP_R3_FIELDS[type], 
							  "FIELD");
		importParams.setValue(EXT_HELP_R3_METHODS[type], 
							  "METHOD");
		importParams.setValue(EXT_HELP_R3_OBJNAMES[type], 
							  "OBJNAME");
		importParams.setValue(EXT_HELP_R3_OBJTYPES[type], 
							  "OBJTYPE");
		importParams.setValue(EXT_HELP_R3_PARAMETERS[type], 
							  "PARAMETER");

		//fire RFC
		//GetHelpValues.Bapi_Helpvalues_Get.Response response = GetHelpValues.bapi_Helpvalues_Get(client, request);
		connection.execute(helpValuesGet);

		GenericReturnStructure returnStructure = new GenericReturnStructure(
														 helpValuesGet.getTableParameterList().getTable(
																 "HELPVALUES"));

		//compile r3 attributes
		JCO.Table metaTable = helpValuesGet.getTableParameterList().getTable(
									  "DESCRIPTION_FOR_HELPVALUES");

		//BAPIF4ETable.Fields metaFields = metaTable.getFields();
		if (metaTable.getNumRows() > 0) {
			metaTable.firstRow();

			do {

				//String name = metaFields.getFieldname();
				String name = metaTable.getString("FIELDNAME");
				R3Attribute attribute = new R3Attribute(name, 
														new Integer(
																metaTable.getInt(
																		"OFFSET")).intValue(), 
														new Integer(
																metaTable.getInt(
																		"LENG")).intValue());

				cat.logT(Severity.DEBUG, loc, "new r3 attribute created: " + attribute.toString());

				returnStructure.addAttribute(attribute);

				//is this field key?
				if (name.equals(EXT_HELP_ROW_KEYS[type])) {
					attribute.isKeyField = true;

					cat.logT(Severity.DEBUG, loc, "is key");
				}
			}
			 while (metaTable.nextRow());
		}

		//create the 'X' r3 attribute
		R3Attribute xAttribute = new R3Attribute(RFCConstants.X, 
												 999, 
												 999);
		returnStructure.addAttribute(xAttribute);

		//compile isa attributes
		Table isaTable = new Table("table");

		for (int i = 0; i < EXT_HELP_TABLE_NAMES[type].length; i++) {

			String isaName = EXT_HELP_TABLE_NAMES[type][i];
			IsaAttribute isaAttribute = new IsaAttribute(isaName);

			cat.logT(Severity.DEBUG, loc, "new isa attribute created: " + isaName);

			isaTable.addColumn(EXT_HELP_TABLE_TYPES[type][i], 
							   isaName);

			//link this attribute to the corresponding R/3 one
			String correspondingR3Name = EXT_HELP_TABLE_NAMES_R3[type][i];

			if (!correspondingR3Name.equals("")) {

				R3Attribute r3Attribute = returnStructure.findR3Attribute(
												  correspondingR3Name);

				if (r3Attribute == null) {
					cat.logT(Severity.ERROR, loc, MessageR3.ISAR3MSG_CUST);
				}

				r3Attribute.isaAttributes.add(isaAttribute);

				cat.logT(Severity.DEBUG, loc, "linked to: " + r3Attribute.toString());
			}
			 else {
				xAttribute.isaAttributes.add(isaAttribute);
			}
		}

		//and now compute the return table
		returnStructure.evaluateGenericResponse(isaTable);

		//being at this point means there might be an entry for listKey,
		//but in this case it is out of date, map has to be re-build
		//(synchronization, see begin of this method)
		try {

			if (access.get(listKey) != null)
				access.remove(listKey);

			access.put(listKey, 
					   isaTable);
		}
		 catch (Cache.ObjectExistsException ex) {
			cat.logT(Severity.DEBUG, loc, "list key exists!");
			throw new BackendException("list key exists!");
		}
		 catch (Cache.Exception e) {
			cat.logT(Severity.DEBUG, loc, "cache exception list key");
			throw new BackendException("cache exception");
		}

		//being at this point means there might be an entry for mapKey,
		//but in this case it is out of date, map has to be re-build
		if (createMap) {

			try {

				if (access.get(mapKey) != null)
					access.remove(mapKey);

				access.put(mapKey, 
						   extComputeHashMap(type, 
										  isaTable));
			}
			 catch (Cache.ObjectExistsException ex) {
				cat.logT(Severity.DEBUG, loc, "map key exists!");
				throw new BackendException("map key exists!");
			}
			 catch (Cache.Exception e) {
				cat.logT(Severity.DEBUG, loc, "cache exception mapkey");
				throw new BackendException("cache exception");
			}
		}

		return isaTable;
	}

	/**
	 * Creates a key/value map for the help value table.
	 * 
	 * @param type         help value type
	 * @param returnTable  the help value table
	 * @return the map
	 */
	protected HashMap extComputeHashMap(int type, 
									 Table returnTable) {
		cat.logT(Severity.DEBUG, loc, "compute hash map for type: " + type);

		HashMap map = new HashMap();

		//create a hash map from the Table object. Key is defined
		//in HELP_KEY_RELATION[type][0]. If HELP_KEY_RELATION has
		//3 records, key is the combination of the first two records
		for (int i = 0; i < returnTable.getNumRows(); i++) {

			TableRow row = returnTable.getRow(i + 1);
			String keyColumn = EXT_HELP_KEY_RELATION[type][0];
			String secondKeyColumn = "";

			if (EXT_HELP_KEY_RELATION.length == 3) {
				secondKeyColumn = EXT_HELP_KEY_RELATION[type][1];
			}

			String valueColumn = EXT_HELP_KEY_RELATION[type][EXT_HELP_KEY_RELATION[type].length - 1];
			String key = null;
			String value = row.getField(valueColumn).getString();

			if (keyColumn.equals(KEY)) {
				key = row.getRowKey().toString();
			}
			 else {
				key = row.getField(keyColumn).getString();
			}

			if (!secondKeyColumn.equals("")) {
				key = key + row.getField(secondKeyColumn).getString();
			}

			cat.logT(Severity.DEBUG, loc, "Key/value: " + key + " / " + value);
			map.put(key.toUpperCase().trim(), 
					value);
		}

		return map;
	}
	/**
	 * Interpreting the return table of the sales value get bapi.
	 * 
	 * @author Christoph Hinssen
	 */
	class GenericReturnStructure {

		JCO.Table contentTable = null;
		List attributes = new ArrayList();
		HashMap attributeMap = new HashMap();

		/**
		 * Constructor for the GenericReturnStructure object.
		 * 
		 * @param contentTable  RFC return table
		 */
		GenericReturnStructure(JCO.Table contentTable) {
			this.contentTable = contentTable;
		}

		/**
		 * Adds a feature to the Attribute attribute of the GenericReturnStructure object.
		 * 
		 * @param attribute  The feature to be added to the Attribute attribute
		 */
		void addAttribute(R3Attribute attribute) {
			attributes.add(attribute);
			attributeMap.put(attribute.name, 
							 attribute);
		}

		/**
		 * Find an R/3 attribute in the internal map.
		 * 
		 * @param name  parameter name
		 * @return the attribute
		 */
		R3Attribute findR3Attribute(String name) {

			return (R3Attribute)attributeMap.get(name);
		}

		/**
		 * Parses the BAPI response and creates a table out of it.
		 * 
		 * @param tableToFill  RFC table
		 */
		void evaluateGenericResponse(Table tableToFill) {

			if (contentTable.getNumRows() > 0) {
				contentTable.firstRow();

				do {

					TableRow row = tableToFill.insertRow();

					//String genericContent = contentTable.getFields().getHelpvalues();
					String genericContent = contentTable.getString(
													"HELPVALUES");

					for (int i = 0; i < attributes.size(); i++) {

						R3Attribute attribute = (R3Attribute)attributes.get(
														i);

						if (!attribute.name.equals(RFCConstants.X)) {

							//now get the string content
							int toLength = attribute.length + attribute.offset;

							if (toLength > genericContent.length()) {
								toLength = genericContent.length();
							}

							String attributeContent = null;

							if (toLength < attribute.offset) {
								attributeContent = "";
							}
							 else {
								attributeContent = genericContent.substring(
														   attribute.offset, 
														   toLength);
							}

							// now set value in all corresponding isa attributes
							List isaAttributes = attribute.isaAttributes;

							for (int j = 0; j < isaAttributes.size(); j++) {

								IsaAttribute isaAttribute = (IsaAttribute)isaAttributes.get(
																	j);
								row.setValue(isaAttribute.name, 
											 attributeContent.trim());
							}

							//is this key?
							if (attribute.isKeyField) {
								row.setRowKey(new TechKey(attributeContent.trim()));
							}
						}
						 else {

							//set value constantly to x
							List isaAttributes = attribute.isaAttributes;

							for (int j = 0; j < isaAttributes.size(); j++) {

								IsaAttribute isaAttribute = (IsaAttribute)isaAttributes.get(
																	j);

								try {
									row.setValue(isaAttribute.name, 
												 RFCConstants.X);
								}
								 catch (java.lang.Exception e) {

									//it is boolean
									row.setValue(isaAttribute.name, 
												 true);
								}
							}
						}
					}
				}
				 while (contentTable.nextRow());
			}

			return;
		}
	}

	/**
	 * Generic R/3 attribute, only used within SalesDocumentHelpValue class.
	 * 
	 * @author Christoph Hinssen
	 */
	class R3Attribute {

		String name;
		int offset;
		int length;
		String value;
		List isaAttributes = null;
		boolean isKeyField;

		/**
		 * Constructor for the R3Attribute object.
		 * 
		 * @param name    parameter name
		 * @param offset  offset
		 * @param length  length
		 */
		R3Attribute(String name, 
					int offset, 
					int length) {
			this.name = name;
			this.offset = offset;
			this.length = length;
			this.isKeyField = false;
			this.isaAttributes = new ArrayList();
		}

		/**
		 * For debug purposes.
		 * 
		 * @return the string expression
		 */
		public String toString() {

			return name + "/" + offset + "/" + length;
		}
	}

	/**
	 * The ISA attribute.
	 * 
	 * @author Christoph Hinssen
	 */
	class IsaAttribute {

		String name;

		/**
		 * Constructor for the IsaAttribute object.
		 * 
		 * @param name  the attribute name
		 */
		IsaAttribute(String name) {
			this.name = name;
		}
	}

}
