/*****************************************************************************
    Copyright (c) 2001, SAPMarkets Palo Alto, USA, All rights reserved.

*****************************************************************************/

package com.sap.isa.auction.backend;

/**
 * This interface defines the names that are used in eai-config.xml to set up
 * backend implementations for the business objects.
 * The constants should be used whenever creating a backend business object.
 */

public interface BackendTypeConstants {


  // Eauction
  public static final String BO_TYPE_EMAIL              = "Email";
  public static final String BO_TYPE_AUCTION_USER       = "AuctionUser";
  public static final String BO_TYPE_AUCTION_COMPANY    = "AuctionCompany";
  public static final String BO_TYPE_AUCTION_TG_LIST    = "TgGroupList";
  public static final String BO_TYPE_AUCTION_OPP_QUOTATION = "OppQuotation";
  public static final String BO_TYPE_AUCTION_ORDER      = "AuctionOrder";
  public static final String BO_TYPE_AUCTION_BP_LIST    = "BusinessPartnerList";
  public static final String BO_TYPE_AUCTION_CONTROLLER = "AuctionController";
  public static final String BO_TYPE_ERROR_NOTIFICATION = "ErrorNotification";
  public static final String BO_TYPE_BUYER_NOTIFICATION = "BuyerNotification";
  

}