/*****************************************************************************
    Copyright (c) 2001, SAPMarkets Palo Alto, USA, All rights reserved.

*****************************************************************************/


package com.sap.isa.auction.backend.boi.order;

import com.sap.isa.backend.boi.isacore.SalesDocumentData;
/**
 * This is the extension of the salesdocument for the auction purpose,
 * please note in the auction scenario we do not have the concept of
 * shop basket, we do not have shop, therefore all the data which the
 * Shop hold will go here., the sales arae is included in the head info
 **/

public interface AuctionSalesDocumentData extends SalesDocumentData{
	/**
     * set the responsible employee, often it is the seller ID as a
     * business partner
     * @param string the business partner ID for the responsible employee
     **/
    public void setReponsibleEmployee(String bpId);

    /**
     * get the responsible employee, often it is the seller ID as a
     * business partner
     * @return, the BP ID for the employee who published the auction
     **/
    public String getResponsibleEmployee();

    /**
     * set the responsible sales organisation retrieved in the catalog's 
     * variant that is customized in the web shop settings
     * @param string the responsible sales organisation
     **/
    public void setRespSalesOrg(String respSalesOrg);

    /**
     * get the responsible sales organisation retrieved in the catalog's 
     * variant that is customized in the web shop settings
     * @return, the responsible sales organisation 
     **/
    public String getRespSalesOrg();

    /**
     * set the purchase number id the auction list ID from auction site
     * for exmaple, the eBay site, we can use it cross the reference
     * @param string the auction listing ID from eBay side
     **/
    public void setPurchaseNumber (String purchaseNumber);

    /**
     * get the purchase number id the auction list ID from auction site
     * for exmaple, the eBay site, we can use it cross the reference
     * @return the auction list ID from auction site.
     **/
    public String getPurchaseNumber();
    
    
	/**
	 * Setter for set the Total manula price condition type
	 */
	public String getTotalManualPriceCondition();
	
	/**
	 * getter for set the Total manula price condition type
	 */
	public void setTotalManualPriceCondition(String priceType);
	
}
