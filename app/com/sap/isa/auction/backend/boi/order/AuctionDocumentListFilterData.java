/*****************************************************************************
    Copyright (c) 2001, SAPMarkets Palo Alto, USA, All rights reserved.

*****************************************************************************/

package com.sap.isa.auction.backend.boi.order;

import com.sap.isa.backend.boi.isacore.order.DocumentListFilterData;
import java.util.ArrayList;
/**
 * This class serves the extension of normal documentlistfilterdata interface
 * in order to search by employee responsible
 *
 **/
public interface AuctionDocumentListFilterData extends DocumentListFilterData {
    /**
     * Set the responsible emplyee
     * @param the repsonsible employee
     **/
    public void setResponsibleEmployee(String employee);

    /**
     * get the responsible employee
     * @return the repsonsible meployee
     **/
    public String getResponsibleEmployee();

    /**
     * search criteria for a given list of sales order IDS
     * @param the IDs for a lit of sales order
     **/
     public void setOrderIds (ArrayList orderIds);

     /**
      * get the search criteria for a given list of order
      * @return the Ids for a list of order
      **/
     public ArrayList getOrderIds ();
     
     /**
      * In the detailed search 
      * @
      */
}
