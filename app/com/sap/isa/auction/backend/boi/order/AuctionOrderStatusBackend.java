/*****************************************************************************
    Copyright (c) 2001, SAPMarkets Palo Alto, USA, All rights reserved.

*****************************************************************************/


package com.sap.isa.auction.backend.boi.order;

import java.util.Locale;

import com.sap.isa.backend.boi.isacore.order.OrderStatusBackend;

/**
 * This class serves the purposes of retrieving the order history and order
 * status, for a given orer.
 * From the buyer side we can stick with the ISA concept. but we have to do
 * further filtering, for one-time customer, we filer on the list of orderIDs, 
 * this is ecapusulated within AuctionDocumentListFilterData
 * for that specific eBay user 
 * But from seller side, we want to search by the sales employee ID as well
 * as date information, this also is handled by AuctionDocumentListFilterData	
 * therefore, there are no new interfaces are defined.
 * But in the implementation, it will be different, we will do more filtering
 * based on what passed in.
 **/

public interface AuctionOrderStatusBackend extends OrderStatusBackend{
	public Locale getBackendLocale();
}
