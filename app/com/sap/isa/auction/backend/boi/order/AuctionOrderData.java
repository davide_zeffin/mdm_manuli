/*****************************************************************************
    Copyright (c) 2001, SAPMarkets Palo Alto, USA, All rights reserved.

*****************************************************************************/


package com.sap.isa.auction.backend.boi.order;

public interface AuctionOrderData extends AuctionSalesDocumentData {

	/**
	 * setter for Predecessor document number
	 */
	public void setPredecessorDocumentId(String id);
	
	/**
	 * Getter for the Predecessor document number
	 */
	public String getPredecessorDocumentId();
	
	/**
	 * Setter for set to the insurance fee
	 * @param value
	 */
	public void setInsuranceValue(String value);
	
	/**
	 * Getter for returning the insurance cost
	 * @return
	 */
	public String getInsuranceValue();


	/**
	 * Setter for set to the insurance fee
	 * @param value
	 */
	public void setWinningPrice(String value);
	
	/**
	 * Getter for returning the insurance cost
	 * @return
	 */
	public String getWinningPrice();
	/**
	 * Setter for set the tax manula price condition type
	 */
	public String getTaxManualPriceCondition();
	
	/**
	 * getter for set the Tax manula price condition type
	 */
	public void setTaxManualPriceCondition(String priceType);
	

	/**
	 * Setter for set the insurance manula price condition type
	 */
	public String getInsurManualPriceCondition();
	
	/**
	 * getter for set the insurance manula price condition type
	 */
	public void setInsurManualPriceCondition(String priceType);

	/**
	 * Setter for set the freight manula price condition type
	 */
	public String getFreightManualPriceCondition();
	
	/**
	 * getter for set the Total manula price condition type
	 */
	public void setFreightManualPriceCondition(String priceType);

	
	/**
	 * Setter for set delivery block code type
	 */
	public String getDeliveryBlockCode();
	
	public String getDeliveryBlockText();
	
	/**
	 * getter for set the delivery block code
	 */
	public void setDeliveryCode(String block);
	
	public void setDeliveryText(String text);

	/**
	 * Setter for set billing block code type
	 */
	public String getBillingBlockCode();
	
	/**
	 * getter for set the billing block code
	 */
	public void setBillingBlockCode(String block);
	
	/**
	 * Setter for incoTerm
	 */
	public String getIncoTerm();
	
	/**
	 * getter for set the billing block code
	 */
	public void setIncoterm(String incoTerm);
	
	public String getOtherPriceCondition();
	
	public void setOtherPriceCondition(String string);
	
	public String getOtherChargeDiscount();
	
	public void setOtherChargeDiscount(String string);
	
	/**
	 * Payment Type such as Visa, Discover, AMEX..PayPal
	 * @return
	 */
	public void setPaymentType(String paymentType);
	
	/**
	 * 
	 * @return payment type such as Amex, Discover, PayPal...
	 */
	public String getPaymentType();

	/**
	 * Setter method for set the external user Id to order Data
	 * for example, the eBay user Id to Order Data
	 * @param externalUserId
	 */
	public void setExternalUserId(String externalUserId);
	
	/**
	 * Getter method for get the external user Id
	 * The external user ID, for example, the ebay user Id
	 * @return
	 */
	public String getExternalUserId();
}
