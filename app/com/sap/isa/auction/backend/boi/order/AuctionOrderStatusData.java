/*****************************************************************************
    Copyright (c) 2001, SAPMarkets Palo Alto, USA, All rights reserved.

*****************************************************************************/


package com.sap.isa.auction.backend.boi.order;

import com.sap.isa.backend.boi.isacore.order.OrderStatusData;
import java.util.ArrayList;
/**
 * This class is the extension of orderstatusdata class for the auction purpose
 * for order status and order history tracking
 **/
public interface AuctionOrderStatusData extends OrderStatusData{

    /**
     * is One-time Customer scenario
     **/
    public boolean isOneTimeCustomerScenario();

    /**
     * Get the responsible employee
     *
     **/
    public String getResponsibleEmployee();
    
    /**
     * Get the OrderHeaderData after filtering by the order IDS passed in.
     * @return the OrderHeaderData list after filtering by the 
     * orderIDs from auction.
     */
    public ArrayList getFilteredOrderHeaders ();
    
    /**
     * Add OrderHeaderData to the list of filtered list
     * @OrderHeaderData
     */
	public void addOrderHeaderFiltered(String orderHeader);
}
