
package com.sap.isa.auction.backend.boi.order;

import com.sap.isa.backend.boi.isacore.order.BasketData;


public interface AuctionBasketData extends BasketData {
	/**
	 * set the responsible employee, often it is the seller ID as a
	 * business partner
	 * @param string the business partner ID for the responsible employee
	 **/
	public void setReponsibleEmployee(String bpId);

	/**
	 * get the responsible employee, often it is the seller ID as a
	 * business partner
	 * @return, the BP ID for the employee who published the auction
	 **/
	public String getResponsibleEmployee();

	/**
	 * set the purchase number id the auction list ID from auction site
	 * for exmaple, the eBay site, we can use it cross the reference
	 * @param string the auction listing ID from eBay side
	 **/
	public void setPurchaseNumber (String purchaseNumber);

	/**
	 * get the purchase number id the auction list ID from auction site
	 * for exmaple, the eBay site, we can use it cross the reference
	 * @return the auction list ID from auction site.
	 **/
	public String getPurchaseNumber();
    
    
	/**
	 * Setter for set the Total manula price condition type
	 */
	public String getTotalManualPriceCondition();
	
	/**
	 * getter for set the Total manula price condition type
	 */
	public void setTotalManualPriceCondition(String priceType);
	
	
	/**
	 * Setter for set the Total manula price condition type
	 */
	public String getShippingManualPriceCondition();
	
	/**
	 * getter for set the Total manula price condition type
	 */
	public void setShippingManualPriceCondition(String priceType);

}

