/*****************************************************************************
    Copyright (c) 2001, SAPMarkets Palo Alto, USA, All rights reserved.

*****************************************************************************/



package com.sap.isa.auction.backend.boi.order;

import com.sap.isa.backend.boi.isacore.order.QuotationData;

public interface AuctionQuotationData extends AuctionSalesDocumentData, QuotationData{

}
