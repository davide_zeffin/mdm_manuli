/*****************************************************************************
    Copyright (c) 2001, SAPMarkets Palo Alto, USA, All rights reserved.

*****************************************************************************/


package com.sap.isa.auction.backend.boi.order;

import com.sap.isa.backend.boi.isacore.AddressData;
import com.sap.isa.backend.boi.isacore.SalesDocumentData;
import com.sap.isa.backend.boi.isacore.order.OrderBackend;
import com.sap.isa.core.eai.BackendException;
/**
 * In the auction we eliminated the usage of Web Shop, then we can pass the
 * sales area and order type to create sales order directly.
 * This interface is an extension of the master interface OrderBackend
 *
 **/

public interface AuctionOrderBackend extends OrderBackend{

   /**
     * Create an order in the backend from a quotation (incl. save in backend)
     * the responsible employee is included in the quotationdata, in actual
     * auction scenario, it is not easy to get quotation data, they may be 
     * distached, quotation is created, then after long time order is created
     * @param quotationId, the quotation Id which is copied from, it should be the TechKey
     * @orderdata, includes the order type
     */
    public void createFromQuotation(
            String quotationId,
            AuctionOrderData order,
			AddressData newShipToAddress 
            ) throws BackendException;

	public void cancelInBackend(SalesDocumentData order)throws BackendException;
	/**
	 * Release the delivery block from backend end
	 * @param	The sales document ID for the release
	 * @throws BackendException
	 */
	public void releaseDeliveryBlock(SalesDocumentData order) throws BackendException;

	/**
	 * Release the billing block from backend end
	 * @param	The sales document number for release
	 * @throws BackendException
	 */
	public void releaseBillingBlock(SalesDocumentData order) throws BackendException;
 	
	/**
	 * Update the manual shipping address info	 
	 * */
	public void updateManualShippingAddress(SalesDocumentData order,
											AddressData address) 
											throws BackendException;

	/**
	 * Update the manual pricing info	 
	 * */
	public void updateSalesDocumentPrices(AuctionOrderData order) 
											throws BackendException;		
											
	/**
	 * Add a system status to the sales document
	 * @param	The sales document 
	 * @throws BackendException
	 */
	public void updateOrderSystemStatus(SalesDocumentData order) throws BackendException;
 	
	/**
	 * Release a system status to the sales document
	 * @param	The sales document 
	 * @throws BackendException
	 */
	public void releaseOrderSystemStatus(SalesDocumentData order) throws BackendException;	
																				
}


