/*****************************************************************************
    Copyright (c) 2001, SAPMarkets Palo Alto, USA, All rights reserved.

*****************************************************************************/


package com.sap.isa.auction.backend.boi.order;

import com.sap.isa.auction.backend.boi.AuctionData;
import com.sap.isa.backend.boi.isacore.order.QuotationBackend;
import com.sap.isa.core.eai.BackendException;

/**
 * This interface is an extension of the standard quotationbackend
 * interface for the creation of auction purpose
 **/
public interface AuctionQuotationBackend extends QuotationBackend{
    /**
     * Saves both lean or extended quotations in the backend;
     * needs sales area to decide
     */
    public void createInBackend(AuctionQuotationData quotation)
        										throws BackendException;

 	public void createInBackend(AuctionData auction)
        										throws BackendException;
    /**
     * Saves the modification both lean or extended quotations in the backend
     * order type in included in the quotationdata
     */
    public void modifyInBackend(AuctionQuotationData quotation) 
    								throws BackendException;
    
	/**
	 * Saves the modification both lean or extended quotations in the backend
	 * order type in included in the quotationdata
	 */
	public void setLisingIdInBackend(AuctionQuotationData quotation) 
									throws BackendException;

	/**
	 * Releases unused quantity (quanity for which winner is not determined)
	 * @param quotation
	 */
	public void releaseRemainigQuantityInQuotation(AuctionQuotationData quotation) 
									throws BackendException;

	/**
	 * Reads the line items from the backend.
	 * @throws BackendException
	 */
	public void readFromBackend(AuctionQuotationData quotation) throws BackendException;

}
