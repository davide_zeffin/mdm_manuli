/*
 * Title:        eBay Integration Project
 * Description:	 * SAP Labs LLC, the eBay auction project, also is known as Selling via eBay. 
 * Selling via eBay application provides SAP R/3 (and in the future CRM) 
 * customers to auction and sell inventory/products onto an online marketplace 
 * eBay. The application will provide the ability to Create, Schedule, Publish, 
 * Monitor and Close Auction Listings onto eBay and handle all the relevant 
 * Backend processing for Customers and Orders. 

 * Copyright:    Copyright (c) 2003
 * Company:      SAP Labs LLC
 * @author
 * @version 1.0
 */
package com.sap.isa.auction.backend.boi.order;

import com.sap.isa.backend.boi.isacore.order.ItemData;

/**
 * The extention of ISA ItemData for store the plant information
 * 
 */
public interface AuctionItemData extends ItemData {

	/**
	 * getter for the plant info
	 */
	public String getPlant();
	
	/**
	 * Setter for the plant info
	 */
	public void setPlant(String plant);
	
	/**
	 * getter for the storage location
	 */
	public String getStorageLocation();
	
	/**
	 * Setter for the storage location
	 */
	public void setStorageLocation(String sl);
	
	/**
	 * Set the manual price condition
	 * @param condition
	 */
	public void setManualPriceCondition(String condition);
	
	/**
	 * Set for the auction winning price
	 * @param price
	 */
	public void setManualPrice(String price);
	
	/**
	 * Get the manual price consdition, which will overwrite the
	 * backend setting at the item level 
	 * @return PriceCondition 
	 */
	public String getManualPriceCondition();
	
	/**
	 * Get the manual price for winning auction.
	 * @return: used for auction winning price
	 */
	public String getManualPrice();
}
