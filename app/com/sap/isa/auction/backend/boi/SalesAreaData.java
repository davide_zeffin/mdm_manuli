/*****************************************************************************
    Copyright (c) 2001, SAPMarkets Palo Alto, USA, All rights reserved.

*****************************************************************************/

package com.sap.isa.auction.backend.boi;
/**
 * a sales area is a unique and defined combination of sales organization,
 * distribution channel, and division. Here in Java code we do not want
 * to define the sales area, we only retrieve the sales arae from backend
 * 
 */

public interface SalesAreaData {

    /**
     *  sales organization the org. structure in SD
     */
    public String getSalesOrganization();

    /**
     * set the attribute of sales organization
     */
    public void setSalesOrganization(String salesOrg);

    /**
     *  Responsible sales organization the org. structure in SD
     */
    public String getResponsibleSalesOrganization();

    /**
     * set the attribute of responsible sales organization
     */
    public void setResponsibleSalesOrganization(String salesOrg);

    /**
     * get the attribute of distribution channel
     */
    public String getDistributionChannel ();

    /**
     * Set the attribute of distribution channel
     * it is mandory field of org. unit in SD
     */
    public void setDistributionChannel (String disChannel);

    /**
     * getter method to get the division
     * it is mandatory field
     */
    public String getDivision ();

    /**
     * setter method for division attribute
     * it is mandatory field
     */
    public void setDivision(String division);
    
    public boolean isEmpty();
    
    //must implement these functions, make the class only dependent on its properties
	public boolean equals(Object obj);
	
	public int hashCode();
	
	public String toString();
}




