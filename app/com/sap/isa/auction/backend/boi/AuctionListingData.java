package com.sap.isa.auction.backend.boi;

/**
 * Title:        Internet Sales
 * Description:
 * Copyright:    Copyright (c) 2002
 * Company:      SAPMarkets
 * @author
 * @version 1.0
 */

public interface AuctionListingData {

    public String getId();
    public String getProductId();
    public String getProductCode();
    public double getQuantity();
}