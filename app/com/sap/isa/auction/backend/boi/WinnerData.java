/*****************************************************************************
    Copyright (c) 2001, SAPMarkets Palo Alto, USA, All rights reserved.

*****************************************************************************/

package com.sap.isa.auction.backend.boi;

import java.math.BigDecimal;

import com.sap.isa.core.TechKey;

public interface WinnerData {

    public String getWinnerId();

    public String getAuctionId();

    public String getAuctionListingId();
 
    public String getBidId();

    public String getBidListingId();

    public String getBuyerId();

    public BigDecimal getQuantity();

    public BigDecimal getBidAmount();

    public String getOrderId();
    
	public TechKey getOrderGuid();

}