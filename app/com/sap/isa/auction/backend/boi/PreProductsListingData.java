/*
 * Created on Sep 8, 2003
 *
 * To change the template for this generated file go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
package com.sap.isa.auction.backend.boi;

/**
 * @author i009657
 *
 * This interface extends from AuctionListingData, we want to have 
 * required data for the ATP checking purpose
 */
public interface PreProductsListingData extends AuctionListingData{

	/**
	 * Getter for get the required data for delivery this product
	 * @return the required date, the date here should be in SAP
	 * format
	 */
	public String getRequiredDate();
	
	/**
	 * Setter for the required date, this date is hidden, which is
	 * obtained ffrom the auction closing date plus certain grace period 
	 * for checkout
	 * @param reuiredDate, in the format of SAP date
	 */
	public void setRequiredDate(String requiredDate);
	
}
