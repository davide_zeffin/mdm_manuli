/*
 * Created on May 18, 2004
 *
 * To change the template for this generated file go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
package com.sap.isa.auction.backend.boi;

import com.sap.isa.core.eai.BackendBusinessObject;

/**
 * @author I803746
 *
 * To change the template for this generated type comment go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
public interface SystemSettingData extends BackendBusinessObject {

	public String getClient();
	
	public String getSystemId();

}
