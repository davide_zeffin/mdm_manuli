/*****************************************************************************
    Copyright (c) 2001, SAPMarkets Palo Alto, USA, All rights reserved.

*****************************************************************************/


package com.sap.isa.auction.backend.boi.product;

import java.util.List;
import com.sap.isa.core.eai.BackendException;
import com.sap.isa.core.eai.*;
/**
 * This interface serves the purpose for non-catalog based scenario,
 * basically we can retrieve all the products information, and checks the
 * avalibilty as well as sales organization, and etc
 *
 **/
public interface ProductsBackend extends BackendBusinessObject{
	/**
     * Get the details for a collections of products
     * @param a collection of product Ids
     * @return a collections of product data
     **/
    public List getProductsDetails(List products, String lang)
        						throws BackendException;

	/**
     *	check the existence of the materials
     * @param holds the list of products IDs and the quantity, it is auctionlisting
     * @return if the check failed, then return false
     **/
    public boolean checkExistence (List products)
        								throws BackendException;

    /**
     * perform the ATP check
     * @param holds the list of product IDs with the quantities
     * @return if the check failed, then return false, if all the
     * quantity suffices, then return true
     **/
    public boolean performATPCheck(List products)
        								throws BackendException;



	/**
     * perform the comphensive checking, baically will aggregate all of the
     * four methos above for a complete checking.
     * @param sales arae data, which is used for the auction
     * @param a collection of products  with their locations
     * @param a collection of products with their quantities
     **/
    public boolean performCompleteCheck(List products)
                                        throws BackendException;

}
