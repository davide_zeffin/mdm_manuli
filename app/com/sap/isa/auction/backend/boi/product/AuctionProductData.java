/*****************************************************************************
    Copyright (c) 2001, SAPMarkets Palo Alto, USA, All rights reserved.

*****************************************************************************/

package com.sap.isa.auction.backend.boi.product;

/**
 *Since our material not only comes from product catalog, also it can be
 * retrieve from backend. This interface capture the material master for the
 * product basic attributes
 * We do not provide the setter methods here in general, since all of
 * 
 **/
public interface AuctionProductData {

        /**
         * The product description, this is the long description, perhaps can
         *  be used in the information to publish to eBay
         **/
        public String getDescription();

        /**
         * the manufacturer part number, I am not sure it is same as series
         * number
         **/
        public String getManufactPartNumber();

    	/**
         * The basic of unit of measurement
         * getter method
         **/
		public String getBaseUOM ();

		/**
         * The material group, it may be not applicable to all of the backend
         * getter method
         **/
        public String getMaterialGroup ();

        /**
         * the old material number, it may be used for integratin purpose
         **/
        public String getOldMaterialNumber();

        /**
         * grosss weight
         **/
        public String getGrossWeight();

        /**
         * weight unit
         **/
        public String getWeightUnit();

        /**
         *	net weight
         **/
        public String getNetWeight();

		/**
         * volume, it will be like 0.75 M^3
         **/
        public String getVolume();

        /**
         * volume unit
         **/
        public String getVolumeUnit();

        /**
         * dimension, something like length by width be height
         **/
        public String getDimensions();

        /**
         * EAN
         **/
        public String getEAN();

        /**
         * EAN category
         **/
        public String getEANCategory();

		/**
         * basic text maintained in backend for the material
         * @param the language you want to retrieve the basic data text
         **/
        public String getBasicText(String language);
}



