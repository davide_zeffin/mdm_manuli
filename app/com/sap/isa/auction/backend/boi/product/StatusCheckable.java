/*
 * Created on Sep 29, 2003
 *
 * To change the template for this generated file go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
package com.sap.isa.auction.backend.boi.product;

import com.sap.isa.auction.backend.boi.SalesAreaData;
import com.sap.isa.auction.backend.boi.PlantData;
/**
 * @author I803746
 *
 * To change the template for this generated type comment go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
public interface StatusCheckable{
	public ProductStatusData getStatus();
	public void setStatus(ProductStatusData status);
	
	public String getProductId();
	public void setProductId(String newId);
	
	public SalesAreaData getSalesArea();
	
	public PlantData getPlant();
	
	public String getUnit();
}
