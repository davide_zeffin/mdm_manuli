/*
 * Created on Sep 26, 2003
 *
 * To change the template for this generated file go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 * correspondent to R/3's BAPIWMDVE structure, plus availability field
 */
package com.sap.isa.auction.backend.boi.product;

/**
 * @author I803746
 *
 * To change the template for this generated type comment go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */

public interface ProductStatusData {
	public final int ERRCODE_OK = 0;
	public final int ERRCODE_NONEXIST = 1;
	public final int ERRCODE_NOT_AVAILABLE = 2;
	public final int ERRCODE_NOT_ENOUGH = 3;
	public final int ERRCODE_WRONG_PRODUCTID = 4;
	public final int ERRCODE_SALESORG_NONEXIST = 5;
	public final int ERRCODE_DISTRCHAN_NONEXIST = 6;
	public final int ERRCODE_DIVISION_NONEXIST = 7;
	public final int ERRCODE_PLANT_NONEXIST = 8;
	public final int ERRCODE_WRONG_PLANT = 9; //product exist in sales area but not in specified plant and storage location
	public final int ERRCODE_WRONG_SALESAREA = 10; //product not exist in specified sales area
	public final int ERRCODE_WRONG_UNIT = 11; //unit is wrong for the product
    public final int ERRCODE_RESPSALESORG_NONEXIST = 12;
	
	public boolean isExist();
	public void setExist(boolean ex);
	
	public boolean isAvailable();
	public void setAvailable(boolean availability);
	
	public double getQunatity();
	public void setQuantity(double qty);
	
	public int getErrorCode();
	public void setErrorCode(int code);
}
