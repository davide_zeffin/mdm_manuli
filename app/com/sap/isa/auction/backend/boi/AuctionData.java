/*****************************************************************************
    Copyright (c) 2001, SAPMarkets Palo Alto, USA, All rights reserved.

*****************************************************************************/

package com.sap.isa.auction.backend.boi;

import java.sql.Timestamp;
import java.util.List;
import java.math.BigDecimal;

/**
 * This interface provides only a read only view of data
 */
public interface AuctionData {

    public String getAuctionId();
    public String getAuctionName();
	public String getAuctionTitle();
    public String getAuctionDescription();
    public String getAuctionTerms();
    public Timestamp getStartDate() ;
    public Timestamp getEndDate();
    public String getQuotationId();
    public Timestamp getQuotationExpirationDate();
    public String getSellerId() ;
    public int getStatus() ;
    public int getType();
    public boolean isBrokenLot() ;
    public String getWebShopId() ;
    public BigDecimal getStartPrice();
    public BigDecimal getReservePrice();
    //public BigDecimal getBidIncrement();
    public List getTargetGroups();
    public List getBusinessPartners();
    //public ArrayList getAttachments();
    public List getListings();
	public String getCurrencyCode();
	public String getImageURL();

	// also here add sales area related
	public SalesAreaData getSalesArea();
	public String getAuctionURL(String b2bURL);
	public int getQuantity();
	

} /* end of Interface */
