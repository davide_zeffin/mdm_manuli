/*****************************************************************************
    Copyright (c) 2001, SAPMarkets Palo Alto, USA, All rights reserved.

*****************************************************************************/

package com.sap.isa.auction.backend.boi;
/**
* OranizationUnit represents the legal and organizational structure of your
* company. Oranization structure is linked with those in finicial accounting
* and material management to form a framework for the sales distribution
* processing
* compare with the sales area information, it is more generic, it retrieve
* the list of all the sales org. and a list of all the distribution channel
**/
public interface OrganizationData {

    /**
     *	sales organization the org. structure in SD
     */
    public String getSalesOrganization();

    /**
     * set the attribute of sales organization
     */
    public void setSalesOrganization(String salesOrg);

    /**
     * get the attribute of distribution channel
     */
    public String getDistributionChannel ();

    /**
     * Set the attribute of distribution channel
     * it is mandory field of org. unit in SD
     */
    public void setDistributionChannel (String disChannel);

    /**
     * getter method to get the division
     * it is mandatory field
     */
    public String getDivision ();

    /**
     * setter method for division attribute
     * it is mandatory field
     */
    public void setDivision(String division);


    /**
     * getter the sales area
     */
    public SalesAreaData getSalesArea();

	/**
     * setter for the sales area
     */
    public void setSalesArea(SalesAreaData salesArea);


	/**
     * setter the sales office,
     */
    public void setSalesOffice(String salesOffice);

	/**
     * getter the sales office
     * 
     */
    public String getSalesOffice();

	/**
     * setter the sales group,
     */
    public void setSalesGroup(String salesGroup);

	/**
     * getter the sales group
     * 
     */
    public String getSalesGroup();
}


