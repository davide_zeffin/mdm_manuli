/*****************************************************************************
    Copyright (c) 2001, SAPMarkets Palo Alto, USA, All rights reserved.

*****************************************************************************/

package com.sap.isa.auction.backend.boi;
import com.sap.isa.core.eai.BackendException;
import com.sap.isa.core.util.table.Table;

/** * This class does not intend to provide the creation and manuplication
 * functions to the organization units, it only retrieves all the sales org
 * units, distribution channels, in another words, only retrieving
 * functions, all the customization should be done in the backend. We do not
 * handle the customization issues here
 **/


public interface OrganizationBackend {
	/**
     * Retrieve all the sales organization unit
     * @param language the ISO langaue
     **/
	public Table getSalesOrganizationList(String language)
        				throws BackendException;

    /**
     * Retrieve all the distribution channel
     * @param langauge the ISA langaue code
     **/
    public Table getDistributionChannelList(String language)
        				throws BackendException;

    /**
     * Retrieve all the divisions
     **/
    public Table getDivisionList(String language)
        				throws BackendException;
	/**
     * Get the list of avaliable sales area in the backend
     * @param langaue the language ID in ISO
     * @return The collection of Sales Areas
     **/
    public Table getSalesAreaList(String language)
        				throws BackendException;

    /**
     * Get the plant associate with sales org. unit and distributiion channel
     * @param language the ISO langauge code
     * @param sales organization unit
     * @param distribution channel
     * @return return a list of plant information
     **/
    public Table getPlantList(String language, String salesOrg,
        					  String disChannel) throws BackendException;

    /**
     * All other attributes still in pending, sales office and sales group.
     **/
}




