/*****************************************************************************
    Copyright (c) 2001, SAPMarkets Palo Alto, USA, All rights reserved.

*****************************************************************************/

package com.sap.isa.auction.backend.boi.targetGroup;


/**
 * Interface defined to be useed by any backend object which will get the target
 * group list from backend system like TargetGroupCRM
 */
import com.sap.isa.core.TechKey;
import com.sap.isa.core.eai.*;
import com.sap.isa.auction.backend.boi.targetGroup.TargetGroupSearchData;

public interface TargetGroupListBackend extends BackendBusinessObject {

    public void getCRMTargetGroupList(TargetGroupListData tgLt) throws BackendException;
    public void getCRMTargetGroupList(TargetGroupListData tgLt, TechKey bpGuid)
                throws BackendException;

    public void getCRMTargetGroupObjectList(TargetGroupListData tgLt,TechKey tgGuid)
                throws BackendException;
    public void getCRMTargetGroupObjectList(TargetGroupListData tgLt)
                throws BackendException;

    /** This method should be used for all the
     *  searches of TG's: All the above methods should be phased out.
     */
    public void getCRMTargetGroupObjectList(TargetGroupListData tgLt,
                  TargetGroupSearchData search)  throws BackendException;
}