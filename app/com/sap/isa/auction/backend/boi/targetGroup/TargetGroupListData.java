/*****************************************************************************
    Copyright (c) 2001, SAPMarkets Palo Alto, USA, All rights reserved.

*****************************************************************************/

package com.sap.isa.auction.backend.boi.targetGroup;

import java.util.*;
import com.sap.isa.backend.boi.isacore.*;


/**
 * The class is the wrapper of the TargetGroupList
 */
public interface TargetGroupListData extends BusinessObjectBaseData {
    public void setTargetGroupList(HashMap tglist);

}