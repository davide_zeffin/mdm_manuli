/*****************************************************************************
    Copyright (c) 2001, SAPMarkets Palo Alto, USA, All rights reserved.

*****************************************************************************/


package com.sap.isa.auction.backend.boi.targetGroup;

/**
 * Data object that represents the target group details;
 */
public interface TargetGroupData {

     public String getGuid();

     public String getDescription();

     public void setGuid(String guid);

     public void setDescription(String description);

}