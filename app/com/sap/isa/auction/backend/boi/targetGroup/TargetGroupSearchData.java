/*****************************************************************************
    Copyright (c) 2001, SAPMarkets Palo Alto, USA, All rights reserved.

*****************************************************************************/

package com.sap.isa.auction.backend.boi.targetGroup;

import com.sap.isa.backend.boi.isacore.*;


/**
 * Sets the search conditions for business partners
 */
public interface TargetGroupSearchData extends BusinessObjectBaseData {

    public String getBpGuid();

    public String getTgDescription();

    public void setBpGuid(String bpguid);

    public void setTgType(String tgtype);

    public void setTgDescription(String tgdesc);

    public void setTgCreatedBy(String tgcreatedby);

    public void setTgChangedBy(String tgchangedby);


}