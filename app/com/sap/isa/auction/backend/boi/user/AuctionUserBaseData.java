/*****************************************************************************
    Copyright (c) 2001, SAPMarkets Palo Alto, USA, All rights reserved.

*****************************************************************************/

package com.sap.isa.auction.backend.boi.user;

import com.sap.isa.user.backend.boi.IsaUserBaseData;
import java.util.Enumeration;
import com.sap.isa.auction.backend.boi.businesspartner.ResponsibleSalesEmployeeData;
/**
* This is used for the seller side user management, seller side has two
* roles, administrator and seller
**/
public interface AuctionUserBaseData extends IsaUserBaseData {
	/**
     * is the administrator of the auctioning functionality
     **/
     public boolean isAdministrator ();

    /**
     *  The user profile, role, which is defined in backend
     **/
	public Enumeration getUserRoles ();

	/**
	 * 
	 **/
	public void setSalesEmployee(ResponsibleSalesEmployeeData salesEmployee);


	/**
	 * 
	 * @return sales employee
	 */
	public ResponsibleSalesEmployeeData getSalesEmployee();
	
	/**
	 * Setter for the default of profileNem
	 * @param String the profileName
	 */ 
	public void setCheckingProfileName(String profileName);
	
	/**
	 * Getter for the default profile name
	 * @return the default of profile name
	 */
	public String getCheckingProfileName ();
	
	public String getClient();
	
	public String getSystemId(); 
	
	/**
	 * Sets the user to use adminsitrator functionality
	 * @param isAdmin
	 */
	public void setAdministrator(boolean isAdmin);
}
