/*****************************************************************************
    Copyright (c) 2001, SAPMarkets Palo Alto, USA, All rights reserved.

*****************************************************************************/

package com.sap.isa.auction.backend.boi.user;


import com.sap.isa.core.eai.BackendException;


import com.sap.isa.auction.backend.boi.user.AuctionUserData;
import com.sap.isa.auction.backend.boi.businesspartner.ResponsibleSalesEmployeeData;
import java.util.ArrayList;
import com.sap.isa.user.backend.boi.UserBaseBackend;

public interface AuctionUserBackend extends UserBaseBackend {
    /**
     * Get the user defaults like timezone, decimal format and
     * date format given the crm user id
     */
    public void getUserDefaults(AuctionUserData aucUser,String userid)
                      throws BackendException;

	/**
	 * get the sales employee from Internet user, in current implementation
	 * we assume that one User gets one sales employee
	 * Her the internet user is type of system user
	 * @param the system user name 
	 * @return the sales employee data
	 */
	public ResponsibleSalesEmployeeData getSalesEmployeeFromInternetUser(
						String userName)
						throws BackendException;
						
	/**
	 * get all of the users with certain profile
	 * @param profiles, a collection of profiles to retrieve all the users for the seller
	 * @return the collection of users with auction related profile, no matter it is admin
	 * user or normal auction seller user
	 * side application, it is used in admin functionality
	 */
	
	public ArrayList getAllSellers (ArrayList profiles) throws BackendException;
	
	public String getClient();
	
	public String getSystemId();
	
}