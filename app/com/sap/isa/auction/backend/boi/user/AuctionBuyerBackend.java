/*
 * Created on Sep 15, 2003
 *
 * To change the template for this generated file go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
package com.sap.isa.auction.backend.boi.user;

/**
 * Copyright (c) 2002, SAPLabs, Palo Alto, All rights reserved.
 */
import com.sap.isa.auction.backend.boi.SalesAreaData;
import com.sap.isa.backend.boi.isacore.AddressData;
import com.sap.isa.core.eai.BackendException;
import com.sap.isa.user.backend.boi.UserBaseBackend;
import com.sap.isa.user.util.RegisterStatus;
import com.sap.isa.backend.boi.isacore.UserData;


public interface AuctionBuyerBackend extends UserBaseBackend{

	/**
	 * Register a internet user as a consumer in the internet sales b2c scenario.
	 * 
	 * @param password              password
	 * @param user                  the user
	 * @param shopId                shop id
	 * @param address               adress data from registration
	 * @param password_verify       second password for verifying
	 * @return integer value that indicates if the registration was sucessful '0':
	 *         registriation sucessful '1': registration not sucessful display
	 *         corresponding messages in the jsp '2': registration not sucessful the
	 *         selection of a county is necessary for the taxjurisdiction code
	 *         determination
	 * @exception BackendException  Exception from backend
	 */
	public RegisterStatus register(UserData user,  
								   AddressData address, 
								   String password, 
								   String password_verify)
							throws BackendException; 
	
	/**
	 * Changes the data of a customer. Will be called in ISA R/3 B2C only.
	 * 
	 * @param user                  the ISA user
	 * @param address               the users address data
	 * @return integer value that indicates if the changing was sucessful '0': customer
	 *         change was sucessful '1': customer change was not sucessful show
	 *         corresponding messages '2': customer change was not sucessful selection
	 *         of county is necessary
	 * @exception BackendException  exception from R/3
	 * @throws ( BackendException if user is not logged in (not relevant at run-time)
	 */
	public RegisterStatus changeCustomer(UserData user, 
										 AddressData address)
		
								  throws BackendException; 

	/** 
	 * It is used to create a customer master record in the backend
	 * It does not create any user in the backend, used in the 
	 * Auction scenario for the scheduler based order creation 
	 */
	public RegisterStatus createCustomerMaster(UserData user,  
								   AddressData address, SalesAreaData salesArea)
							throws BackendException;
							
	public boolean checkAddress(AddressData address) throws BackendException;

}
