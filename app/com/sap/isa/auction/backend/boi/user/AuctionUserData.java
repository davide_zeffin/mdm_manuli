/*****************************************************************************
    Copyright (c) 2001, SAPMarkets Palo Alto, USA, All rights reserved.

*****************************************************************************/

package com.sap.isa.auction.backend.boi.user;



/**
 * Used to get the default settings for a CRM user
 * In Web Auctions this would be the seller.
 */
public interface AuctionUserData {
    public String getTimeZone();
    public String getDecimalFormat();
    public String getDateFormat();
    public String getDecimalSeparator();
    public String getGroupingSeparator();

    public void setTimeZone(String tz);
    public void setDecimalFormat(String df);
    public void setDateFormat(String datf);
    public void setDecimalSeparator(String ds);
    public void setGroupingSeparator(String gs);
	
	public void setLastName(String lastName);
	public void setFirstName(String firstName);
	public String getLastName();
	public String getFirstName();
	
	public void setSalutationText(String salutationTxt);
	public String getSalutationTxt();
	
	public void setEMailAddress(String address);
	public String getEMailAddress();
}