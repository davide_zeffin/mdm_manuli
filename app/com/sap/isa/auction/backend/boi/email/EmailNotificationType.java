/*****************************************************************************
	Copyright (c) 2004, SAPLabs Palo Alto, USA, All rights reserved.

*****************************************************************************/

package com.sap.isa.auction.backend.boi.email;

/**
 * This interface describes the default scenarios for email
 * notification, there are three scenarios by default 
 * 	 * 1. Send invitation to a target groupfor bidding invitation
	 * 2. Notify a bider for over bidding
	 * 3. Notify the winner as the highest bidder
 **/

public interface EmailNotificationType {
	/**
	 * Constant to describe the Send invitation to 
	 * a target groupfor bidding invitation
	 */
	public static final byte BIDDING_INVITATION = 0x01;
	
	/**
	 * Constant to describe the scenario
	 * Notify a bider for outbidding
	 */
	public static final byte AUCTION_OUTBIDDING = 0x02;
	

	/**
	 * Constant to describe the scenario
	 * Notify the winner as the highest bidder
	 */
	public static final byte AUCTION_WINNING = 0x03;	
	
}
