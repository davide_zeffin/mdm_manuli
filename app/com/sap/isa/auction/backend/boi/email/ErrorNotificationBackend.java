/*****************************************************************************
	Copyright (c) 2004, SAPLabs Palo Alto, USA, All rights reserved.

*****************************************************************************/

package com.sap.isa.auction.backend.boi.email;

import com.sap.isa.core.eai.BackendBusinessObject;
import com.sap.isa.core.eai.BackendException;


public interface ErrorNotificationBackend extends BackendBusinessObject {
	
	
	/**
	 * Sends Error notification 
	 * Returns true if the error is notified to the recepients 
	 * false otherwise 
	 * @throws BackendException  
	 */
	public boolean sendErrorNotification(ErrorNotificationData errorData) throws BackendException;
}
