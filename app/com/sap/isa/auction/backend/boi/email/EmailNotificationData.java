/*****************************************************************************
	Copyright (c) 2004, SAPLabs Palo Alto, USA, All rights reserved.

*****************************************************************************/

package com.sap.isa.auction.backend.boi.email;


import com.sap.isa.auction.backend.boi.AuctionData;
import com.sap.isa.auction.backend.boi.WinnerData;



public interface EmailNotificationData {

	/**
	 * Following EMail notification types exist: 
	 * 1. Send invitation to a target groupfor bidding invitation
	 * 2. Notify a bidder for over bidding
	 * 3. Notofy the winner as the higest bidder
	 * 
	 * @see EmailNotificationType
	 * @return Byte representation of EmailNotificationType   
	 */
	public byte getEmailNotificationType ();

	/**
	 * Returns AuctionData consists of TargetGroups and auction related 
	 * information 
	 * @return the auction data
	 */
	public AuctionData getAuctionData();
	
	/**
	 * @return
	 */
	public String getFrom();

	/**
	 * @return
	 */
	public String getSubject();
	
	/**
	 * Winner notification data
	 * @return
	 */
	public WinnerData getWinnerData();
	
	/**
	 * @return
	 */
	public String getTo();
	
	/**
	 * XCM scenario for checkout
	 * @param scenario
	 */
	public void setScenarioName(String scenario);
	
	public String getScenario();
	/**
	 * Returns Transaction id for checkout email
	 * @return
	 */
	public String getTxnId();
	
	public void setTxnId(String txnId );
}	
