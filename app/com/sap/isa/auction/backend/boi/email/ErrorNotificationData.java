/*****************************************************************************
	Copyright (c) 2004, SAPLabs Palo Alto, USA, All rights reserved.

*****************************************************************************/

package com.sap.isa.auction.backend.boi.email;

import java.util.ArrayList;


/**
 * Notification to the administrator when exceptions or errors occur in 
 * Auctioning via Webshop runtime
 * @author I802758
 *
 */
public interface ErrorNotificationData {

	
	public String getError();
	
	/**
	 * Get the cause for the error
	 * @return
	 */
	public String getCause();
	
	/**
	 * Get resolution for the error
	 * @return
	 */
	public String getResolution();
	
	/**
	 * Returns subject for notification
	 * @return
	 */
	public String getSubject();
	
	/**
	 * From user ID in the backend. User must be SU01
	 * @return
	 */
	public String getFrom();
	
	/**
	 * To email address list
	 * @return
	 */
	public ArrayList getTo();
	
	/**
	 * returns CC list, if exists null otherwise
	 * @return
	 */
	public ArrayList getCC();
	
	
}	
