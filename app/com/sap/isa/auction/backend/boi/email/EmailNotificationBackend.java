/*****************************************************************************
	Copyright (c) 2004, SAPLabs Palo Alto, USA, All rights reserved.

*****************************************************************************/

package com.sap.isa.auction.backend.boi.email;

import com.sap.isa.core.eai.BackendBusinessObject;
import com.sap.isa.core.eai.BackendException;


public interface EmailNotificationBackend extends BackendBusinessObject {
	
	/**
	 * Send email notification to a target group or to a business partner
	 * @param notificationData
	 * @param shop ShopData used to get all auction related attribues
	 * @return a boolean value to indicate if the email is sent successfully
	 * @throws BackendException
	 */
	public boolean sendEmailNotification(EmailNotificationData notificationData) throws BackendException;

	/**
	 * simulate email notification to a target group or to a business partner
	 * @param notificationData
	 * @param shop ShopData used to get all auction related attribues
	 * @return a boolean value to indicate if the email is sent successfully
	 * @throws BackendException
	 
	 
	public boolean simulatEmailNotification(EmailNotificationData notificationData,
										ShopData shop) throws BackendException;
	*/
	

}
