/*****************************************************************************
    Copyright (c) 2001, SAPMarkets Palo Alto, USA, All rights reserved.

*****************************************************************************/

package com.sap.isa.auction.backend.boi.businesspartner;


/**
 * Interface for backend business objects of business partner list
 *  */
import com.sap.isa.core.TechKey;
import com.sap.isa.core.eai.*;
import com.sap.isa.auction.backend.boi.SearchData;

public interface BusinessPartnerListBackend extends BackendBusinessObject {

    public void crmSearchBPList(BusinessPartnerListData bplist, SearchData searchdata) throws BackendException;
    public void crmSearchBPListByTG(BusinessPartnerListData bplist, TechKey bpGuid)
                throws BackendException;
    public void getBPFromUser(BusinessPartnerListData bplist,String userId)
                throws BackendException;

}