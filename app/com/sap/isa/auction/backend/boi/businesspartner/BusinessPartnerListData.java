/*****************************************************************************
    Copyright (c) 2001, SAPMarkets Palo Alto, USA, All rights reserved.

*****************************************************************************/

package com.sap.isa.auction.backend.boi.businesspartner;

import java.util.HashMap;
import com.sap.isa.core.TechKey;
import com.sap.isa.auction.backend.boi.SearchData;
import com.sap.isa.businesspartner.businessobject.BusinessPartner;

public interface BusinessPartnerListData {

  public HashMap searchBusinessPartners(SearchData searchData);
  public HashMap searchBusinessPartnersByTG(TechKey key);
  public void setBPSearchResult(HashMap list);
  public HashMap getBusinessPartnerList();
  public void setBusinessPartnerList(HashMap list);
  public void addBusinessPartner(BusinessPartner bp);
  public void clear();
  public boolean isEmpty();
  public boolean contains(String id);
  public int size();
  public BusinessPartner getBPFromUser(String userId);
  public void setBusinessPartner(BusinessPartner bPartnerObj);


}