/*
 * Created on Sep 9, 2003
 *
 * SAP Labs LLC, the eBay auction project, also is known as Selling via eBay. 
 * Selling via eBay application provides SAP R/3 (and in the future CRM) 
 * customers to auction and sell inventory/products onto an online marketplace 
 * eBay. The application will provide the ability to Create, Schedule, Publish, 
 * Monitor and Close Auction Listings onto eBay and handle all the relevant 
 * Backend processing for Customers and Orders. 

 */
package com.sap.isa.auction.backend.boi.businesspartner;
import com.sap.isa.core.eai.BackendException;
import com.sap.isa.core.eai.*;
import java.util.ArrayList;
/**
 * @
 */
public interface AuctionBusinessPartnerBackend extends BackendBusinessObject{

	/**
	 * Search all the business partner with certain criteria
	 * @return a collection of AddressData
	 * 
	 */
	public ArrayList searchOneTimeCustoms() throws 
											BackendException;
	

}
