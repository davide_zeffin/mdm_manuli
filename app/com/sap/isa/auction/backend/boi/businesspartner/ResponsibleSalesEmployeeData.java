/*****************************************************************************
    Copyright (c) 2001, SAPMarkets Palo Alto, USA, All rights reserved.

*****************************************************************************/

package com.sap.isa.auction.backend.boi.businesspartner;

import com.sap.isa.businesspartner.backend.boi.BusinessPartnerData;
import java.util.Enumeration;
/**
 * ResponsibleSalesEmployee is an employee at seller who is respionsible for
 * the sales activities, he/she is assigned to certain organization unit.
 * This object will be assigned to the sales order data.
 **/

public interface ResponsibleSalesEmployeeData extends BusinessPartnerData {
	/**
     * setter method for the responsible organization unit number
     */
    public void setResponsibleOrganizations(Enumeration orgs);

    /**
     * getter method for the responsible oranization unit
     */
    public Enumeration getResponsibleOrganizations();

}
