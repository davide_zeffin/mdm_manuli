/*****************************************************************************
	Copyright (c) 2001, SAPMarkets Palo Alto, USA, All rights reserved.

*****************************************************************************/

package com.sap.isa.auction.backend.boi;


public interface BackendSettingsData {
	/**
	 * @return
	 */
	public boolean isAutoReleaseBlock();
	/**
	 * @return
	 */
	public String getBillingBlockCode();
	/**
	 * @return
	 */
	public String getCashOnDeliveryBP();
	/**
	 * @return
	 */
	public String getCatalogDesc();
	/**
	 * @return
	 */
	public String getCatalogId();
	/**
	 * @return
	 */
	public String getCatalogVariant();
	/**
	 * @return
	 */
	public boolean isDeliveryBlock();
	/**
	 * @return
	 */
	public String getDeliveryBlockCode();
	/**
	 * @return
	 */
	public String getId();
	/**
	 * @return
	 */
	public String getIncoterm();
	/**
	 * @return
	 */
	public String getInsuranceCondition();
	/**
	 * @return
	 */
	public boolean isOneTimeCustomerScenario();
	/**
	 * @return
	 */
	public String getOneTimeCustomerId();
	/**
	 * @return
	 */
	public String getOrderType();
	/**
	 * @return
	 */
	public String getPayPalOrderType();
	/**
	 * @return
	 */
	public String getPlant();
	/**
	 * @return
	 */
	public String getQuotationType();
	/**
	 * @return
	 */
	public String getRefBP();
	/**
	 * @return
	 */
	public String getRejectReason();
	/**
	 * @return
	 */
	public SalesAreaData getSalesArea();
	/**
	 * @return
	 */
	public String getShipPriceCondition();
	/**
	 * @return
	 */
	public String getStorage();
	/**
	 * @return
	 */
	public String getTaxPriceCondiction();
	/**
	 * @return
	 */
	public String getTotalPriceCondiction();
	/**
	 * @param b
	 */
	public void setAutoReleaseBlock(boolean b);
	/**
	 * @param string
	 */
	public void setBillingBlockCode(String string);
	/**
	 * @param string
	 */
	public void setCashOnDeliveryBP(String string);
	/**
	 * @param string
	 */
	public void setCatalogDesc(String string);
	/**
	 * @param string
	 */
	public void setCatalogId(String string);
	/**
	 * @param string
	 */
	public void setCatalogVariant(String string);
	/**
	 * @param b
	 */
	public void setDeliveryBlock(boolean b);
	/**
	 * @param string
	 */
	public void setDeliveryBlockCode(String string);
	/**
	 * @param string
	 */
	public void setId(String string);
	/**
	 * @param string
	 */
	public void setIncoterm(String string);
	/**
	 * @param string
	 */
	public void setInsuranceCondition(String string);
	/**
	 * @param b
	 */
	public void setOneTimeCustomerScenario(boolean b);
	/**
	 * @param string
	 */
	public void setOneTimeCustomerId(String string);
	/**
	 * @param string
	 */
	public void setOrderType(String string);
	/**
	 * @param string
	 */
	public void setPayPalOrderType(String string);
	/**
	 * @param string
	 */
	public void setPlant(String string);
	/**
	 * @param string
	 */
	public void setQuotationType(String string);
	/**
	 * @param string
	 */
	public void setRefBP(String string);
	/**
	 * @param string
	 */
	public void setRejectReason(String string);
	/**
	 * @param area
	 */
	public void setSalesArea(SalesAreaData area);
	/**
	 * @param string
	 */
	public void setShipPriceCondition(String string);
	/**
	 * @param string
	 */
	public void setStorage(String string);
	/**
	 * @param string
	 */
	public void setTaxPriceCondiction(String string);
	/**
	 * @param string
	 */
	public void setTotalPriceCondiction(String string);
	
	public String getOtherPriceCondition();

	public void setOtherPriceCondition(String string);
	
	/**
	 * @return
	 */
	public boolean isIndependentCheckout();

	/**
	 * @return
	 */
	public String getWebShopId();

	/**
	 * @param b
	 */
	public void setIndependentCheckout(boolean b);

	/**
	 * @param string
	 */
	public void setWebShopId(String string);
	
	
}