/*****************************************************************************
    Copyright (c) 2001, SAPMarkets Palo Alto, USA, All rights reserved.

*****************************************************************************/

package com.sap.isa.auction.backend.boi;
/**
 * This interface holda more data, it holds the plant information as well
 * as storage location, Number of the storage location at which the material
 * is stored. A plant may contain one or more storage locations. But in our
 * auction scenario, we only use the selected storage location.
 **/

import java.util.ArrayList;
public interface PlantData {
    /**
     * Plant ID, the plant ID in R/3 is a string of length of character 4
     * getter method for plant
     * @return the plant ID
     **/
    public String getPlantID();

    /**
     * setter for plnt ID
     * @param the plant ID
     **/
    public void setPlantID(String plantID);

    /**
     * the name of the plant, the short description of the plant
     * getter method for plant description
     * @return the plant name
     **/
    public String getPlantName();

    /**
     * setter method for the plant name
     *@param the name of the plant
     **/
    public void setPlantName (String plantName);

    /**
     * the name of the plant, the short description of the plant
     * getter method for plant description
     * @return the plant name
     **/
    public String getPlantNameB();

    /**
     * setter method for the plant name
     *@param the name of the plant
     **/
    public void setPlantNameB (String plantName);

    /**
     * the selected storage location. This one is used to check if the
     * product exist at this specific location.
     **/
    public String getSelectedStorageLocation ();

	/**
     * Setter for the selected storage location. This one is used to check if
     * product exist at this specific location.
     **/
    public void setSelectedStorageLocation(String location);

	/**
     * the list of storage locations, one plant can have more than storage
     * locations
     **/
    public ArrayList getStorageLocations();

    /**
     * setter for all of the storage locations for a specific plant
     **/
    public void setStorageLocations(ArrayList locations);
    
	public boolean isEmpty();
    
	//must implement these functions, make the class only dependent on its properties
	public boolean equals(Object obj);
	
	public int hashCode();
	
	public String toString();
}


