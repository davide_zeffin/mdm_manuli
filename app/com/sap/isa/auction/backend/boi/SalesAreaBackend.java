/*****************************************************************************
    Copyright (c) 2001, SAPMarkets Palo Alto, USA, All rights reserved.

*****************************************************************************/

package com.sap.isa.auction.backend.boi;
import com.sap.isa.core.eai.BackendException;
import com.sap.isa.core.util.table.Table;

public interface SalesAreaBackend {
    /**
     * get the distribution channel from a given sales org.
     * @param language the ISO language code
     * @param sales organization ID
     **/
    public Table getDistributionChannels(String language, String salesOrgId)
        	 throws BackendException;

	/**
     * For a given sales organization and distribution channel, we can get
     * the division
     * @param langaue the language IDin ISO format
     * @param sales organization ID
     * @param the ditribution channel ID
     * @return, return the combination of distribution channel and division,
     * even the input parameter ID of distribution chann can be null
     **/
    public Table getDivisions(String language, String salesOrgId,
        								String disChannel)
                                        throws BackendException;

}


