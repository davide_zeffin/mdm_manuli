/*
 * Title:        eBay Integration Project
 * Description:	 * SAP Labs LLC, the eBay auction project, also is known as Selling via eBay. 
 * Selling via eBay application provides SAP R/3 (and in the future CRM) 
 * customers to auction and sell inventory/products onto an online marketplace 
 * eBay. The application will provide the ability to Create, Schedule, Publish, 
 * Monitor and Close Auction Listings onto eBay and handle all the relevant 
 * Backend processing for Customers and Orders. 

 * Copyright:    Copyright (c) 2003
 * Company:      SAP Labs LLC
 * @author
 * @version 1.0
 */
package com.sap.isa.auction.backend.boi;

import java.util.Locale;

import com.sap.isa.core.eai.BackendException;
import com.sap.isa.core.util.table.Table;


public interface SettingHolderBackend {
	/**
	 * Do the initlization for all the backend setting, it consilidate
	 * what we 
	 * This is for non-PI scenario
	 * @param 		BackendDefaultSetting	which comes from the 
	 *										AuctionAdmin
	 * @return      if the initlization successfully	 										
	 * @exception BackendException 	 exception from R3  
	 **/
	public boolean initialize(BackendSettingsData setting)
		throws BackendException;
		
	/**
	 * Retrieve all the sales doc type
	 * @param language the ISO langaue
	 **/
	public Table getSalesDocType(String language)
						throws BackendException;
	
	/**
	 * Retrieve all the catalog ID
	 * @param language the ISO langaue
	 **/
	public Table getCatalogList(String language)
						throws BackendException;
						
	/**
	 * Retrieve all catalog variant associate with a catalog
	 * @param language the ISO langaue
	 **/
	public Table getCatalogVariantList(String language,
										String catalogId)
						throws BackendException;
	/**
	 * Retrieve all the shipping conditions
	 * @param language the ISO langaue
	 **/
	public Table getShippingConditionList(String language)
						throws BackendException;
						
	public Locale getBackendLocale();

}
