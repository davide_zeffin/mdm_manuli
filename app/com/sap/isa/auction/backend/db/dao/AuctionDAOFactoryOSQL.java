
package com.sap.isa.auction.backend.db.dao;

import com.sap.isa.backend.db.dao.DAOFactoryOSQL;
import java.util.Properties;

import com.sap.isa.core.eai.BackendBusinessObject;
import com.sap.isa.core.eai.BackendBusinessObjectBase;
import com.sap.isa.core.eai.BackendBusinessObjectParams;
import com.sap.isa.core.eai.BackendException;
import com.sap.isa.core.logging.IsaLocation;


public class AuctionDAOFactoryOSQL extends DAOFactoryOSQL {

	/**
	 * the constructor 
	 */
	public AuctionDAOFactoryOSQL() {
		super(); 
	}

	/* (non-Javadoc)
	 * @see com.sap.isa.core.eai.BackendBusinessObject#initBackendObject(java.util.Properties, com.sap.isa.core.eai.BackendBusinessObjectParams)
	 */
	public void initBackendObject(
		Properties props,
		BackendBusinessObjectParams params)
		throws BackendException {
            
		// check if we do db synching
		String usedatabaseBasket = props.getProperty("usedatabaseBasket");
		String usedatabaseTemplate = props.getProperty("usedatabaseTemplate");
		if (log.isDebugEnabled()) {
			log.debug("XCM Parameters:");
			log.debug("usedatabaseBasket=" + usedatabaseBasket);
			log.debug("usedatabaseTemplate=" + usedatabaseTemplate);
		}
		
		this.useDatabaseBasket = false;
		this.useDatabaseTemplate = false;
		if (log.isDebugEnabled()) {
			log.debug("usedatabaseBasket has been set to      : " + useDatabaseBasket);
			log.debug("usedatabaseTemplate has been set to    : " + useDatabaseTemplate);
		}
	}


	/**
	 * Returns the flag if we do db synching
	 *
	 * @return useDatabase, true == yes, we do db syncing
	 */
	public boolean isUseDatabase(String docType) {
			return false;
	}

}
