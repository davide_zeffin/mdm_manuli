/*
 * Title:        eBay Integration Project
 * Description:	 * SAP Labs LLC, the eBay auction project, also is known as Selling via eBay. 
 * Selling via eBay application provides SAP R/3 (and in the future CRM) 
 * customers to auction and sell inventory/products onto an online marketplace 
 * eBay. The application will provide the ability to Create, Schedule, Publish, 
 * Monitor and Close Auction Listings onto eBay and handle all the relevant 
 * Backend processing for Customers and Orders. 

 * Copyright:    Copyright (c) 2005
 * Company:      SAP Labs LLC
 * @author
 * @version 1.0
 */
package com.sap.isa.auction.backend.db.salesdocument;

//import java.util.ArrayList;
//import java.util.Iterator;
//import java.util.List;
//import java.util.ListIterator;

//import com.sap.isa.backend.boi.isacore.SalesDocumentData;
import com.sap.isa.backend.boi.isacore.ShopData;
import com.sap.isa.backend.boi.isacore.order.BasketData;
import com.sap.isa.backend.boi.isacore.order.CopyMode;
import com.sap.isa.backend.boi.isacore.order.HeaderData;
//import com.sap.isa.backend.boi.isacore.order.ItemData;
//import com.sap.isa.backend.boi.isacore.order.ItemListData;
import com.sap.isa.backend.db.dao.DAOFactoryBackend;
import com.sap.isa.backend.db.order.BasketDB;
//import com.sap.isa.backend.db.order.ItemDB;
import com.sap.isa.backend.db.order.ObjectIdDB;
//import com.sap.isa.backend.db.order.TextDataDB;
import com.sap.isa.core.TechKey;
import com.sap.isa.core.eai.BackendException;

/**
 * This is an extension of BasketDB object to service disable
 * all price relevent stuff in backend.
 */
public class AuctionBasketDB extends BasketDB {
	
	/**
	  * Creates a new instance.
	  */
	 public AuctionBasketDB() {
	 	super();  
	 }
	 
	/**
	 * Create a basket in the backend with reference to a predecessor document.
	 *
	 * @param predecessorKeys
	 * @param copyMode
	 * @param processType
	 * @param soldToKey
	 * @param shop
	 * @param basket
	 */
	public void createWithReferenceInBackend(TechKey predecessorKey, CopyMode copyMode,
		String processType, TechKey soldToKey, ShopData shop, BasketData salesDocument)
		throws BackendException {
			long startTime  = System.currentTimeMillis();

			// generate a technical key
			TechKey    bGuid  = TechKey.generateKey();
			HeaderData header = salesDocument.getHeaderData();

			// set the technical key for the document
			// set it in the SalesDoc
			salesDocument.setTechKey(bGuid);
			
			if (getInternalDocType().equals(BASKET_TYPE) && !shop.isPartnerBasket()) {
				// for baskets, no salesdoc numbers are necessary as long as we are not in CCH
				header.setSalesDocNumber("");
			} else {
				header.setSalesDocNumber(ObjectIdDB.getNextDocumentId(this).toString());
			} 
			header.setTechKey(bGuid);
			header.setDocumentType(processType);
			header.setProcessType(processType);

			// set the header and the shop
			salesDocumentHeaderDB.setHeaderData(header);
			salesDocumentHeaderDB.setShopData(shop);
			salesDocumentHeaderDB.setSalesDocData(salesDocument);
			
			//set the predecessor techkey
			salesDocumentHeaderDB.setPredecessorDocGuid(predecessorKey);
			salesDocumentHeaderDB.setDocumentType(processType);
			
			// set some flags in the salesdocument
			setAvailableValueFlags(salesDocument, shop);

			salesDocument.setExternalToOrder(true);

			// write the shop into the headerData because it does not already
			// exist there
			header.setShop(shop);

			// manually set the technical key, because field is present
			// on the header and basket level and the one on the basket
			// is the right one.
			salesDocumentHeaderDB.setGuid(bGuid);

			// in the end, write it to our abstraction layer
			salesDocumentHeaderDB.fillFromObject();
        
			salesDocumentHeaderDB.checkHeaderReqDeliveryDate(salesDocument);

			salesDocumentHeaderDB.checkHeaderLatestDlvDate(salesDocument);
		
			// clear the extension data
			salesDocumentHeaderDB.clearExtensionData();
        
			// clear the businesspartnersList
			salesDocumentHeaderDB.clearBusinessPartners();

			// empty all the other stuff
			items.clear();
			itemGuids.clear();
			lateCommit = false;

			// take care of campaigns handeld on via url
			if (salesDocument.getCampaignKey() != null && !"".equals(salesDocument.getCampaignKey())) {
				// determine Id, etc.  via Service Backend Call
				getCampSupport().addCampaignExistenceCheckEntry(new TechKey(salesDocument.getCampaignKey()));
				getCampSupport().addCampaignEligibilityCheckEntry(salesDocumentHeaderDB.getTechKey());
				salesDocumentHeaderDB.setCampaignGuid(new TechKey(salesDocument.getCampaignKey()));
			}

			// anything else ???
			salesDocumentHeaderDB.synchToDB();

			// set this flag after inserting the basket in the db
			salesDocument.setDocumentRecoverable(isUseDatabase(this.getDAODocumentType()));

			long endTime  = System.currentTimeMillis();
			long diffTime = endTime - startTime;

			if (log.isDebugEnabled()) {
				log.debug("Creating a DB basket took " + diffTime + " msecs");
			}
        
				
			setValid(true);
        	try{	
				addBusinessPartnerInBackend(shop, salesDocument);
				copyManualShipTos(shop, salesDocument);
        	}catch (Exception ex){
        		log.error(ex);	
        	}
	} 
	 
//	/**
//	 * @see com.sap.isa.backend.boi.isacore.SalesDocumentBackend#addBusinessPartnerInBackend(com.sap.isa.backend.boi.isacore.ShopData, com.sap.isa.backend.boi.isacore.SalesDocumentData, com.sap.isa.backend.boi.isacore.UserData)
//	 */
//	public void addBusinessPartnerInBackend(
//		ShopData shop,
//		SalesDocumentData salesDocument)
//			throws BackendException {
//
//		// store the userid in the orders table, so it can be used for recovery
//		updateHeaderInBackend(salesDocument, shop);
//        
//		getCampSupport().updateCampaignData(this);
//        
//		//start: cover free goods
//		//do a new free good determination
//		if (log.isDebugEnabled()){
//			log.debug("addBusinessPartnerInBackend, update free goods");
//		}
//	}
	    
//	/**
//	 * Update object in the backend by putting the data into the underlying
//	 * storage.
//	 *
//	 * @param posd the document to update
//	 * @param shop the current shop
//	 */
//	public void updateInBackend(
//		SalesDocumentData posd,
//		ShopData shop)
//		throws BackendException {
//		// check runtime
//		long     startTime = System.currentTimeMillis();
//        
//
//		ShopData tempShop = shop;
//
//		if (null == tempShop) {
//			tempShop = (ShopData)posd.getHeaderData().getShop();
//		}
//
//		if (null == tempShop) {
//			tempShop = salesDocumentHeaderDB.getShopData();
//		}
//
//		if (null == tempShop) {
//			if (log.isDebugEnabled()) {
//				log.debug("No Shop for populateItems available");
//			}
//
//			throw new BackendException("No Shop for populateItems available");
//		}
//        
//		//initialize free good support
//		freeGoodSupport.clearItemList();
//		freeGoodSupport.setFixedCustomizingData(tempShop,getDecimalPointFormat());
//        
//		// get rid of all subitems in the itemlist of the salesdocument
//		//deleteSubItems(posd);
//        
//		// synch the ItemDBs with the itemlist from the salesDoc
//		//cleanUpItemDBs(posd);
//
//		// Messages for DB Items are maintained in internalUpdateItemList
//		// check some properties in the header
//		// 1. reqDeliveryDate
//		// we have to ensure a valid format in the dateFormat
//		setHeaderDateValid(
//			salesDocumentHeaderDB.checkHeaderReqDeliveryDate(posd));
//
//		//check the Header LatestDlvDate (earlier cancelDate)
//		setHeaderLatestDlvDateValid(
//			salesDocumentHeaderDB.checkHeaderLatestDlvDate(posd));
//						
//		// check runtime
//		long startTimePop = System.currentTimeMillis();
//
//		// unfortunately the productIds are not stored on all jsps
//		// but populateItems needs them, to check if a item was found
//		// so we have to set the productids for items that were already
//		// found
//		setProductIdsInSalesDoc(posd);
//        
//		// to reinitiate price calculation in case of change of UOM, we 
//		// change the old Quantity field to 'x' to initiate a new pricing
//		// in the populateItems call
//		// Also remove error messages that might be obsolete
//        
//		ItemListData itemList = posd.getItemListData();
//		ItemData item;
//		ItemDB itemDB;
//        
//		for (int i = 0; i < itemList.size(); i++) {
//			item = itemList.getItemData(i);
//			if (item.getTechKey() != null
//				&& item.getTechKey().getIdAsString().length() > 0) {
//				itemDB = getItemDB(item.getTechKey());
//				if (itemDB != null){
//					itemDB.getMessageList().remove("javabasket.invalidquantity");
//				}
//				if (itemDB != null && !itemDB.getUnit().equals(item.getUnit())) {
//					item.setOldQuantity("x");
//				}
//			}  
//		}
//
//		boolean populated = true; 
//		//posd.populateItemsFromCatalog(tempShop,
////														  forceIPCPricing,
////														  preventIPCPricing);
//
//		long    endTimePop  = System.currentTimeMillis();
//		long    diffTimePop = endTimePop - startTimePop;
//
//		if (log.isDebugEnabled()) {
//			log.debug("Populating the items in a DB basket took " + diffTimePop + " msecs");
//		}
//		try {
//		if (populated) {
//			internalUpdateItemList(posd);
//			// update the header here because all the values are calculated there
//			// and we need to read them from the populate..-method first
//			// and AFTER doing some calculating stuff in internalUpdateItemList
//			updateHeaderInBackend(posd, shop);
//
//			getCampSupport().updateCampaignData(this);
//
//			syncToDb();
//		} else {
//			// populateItems failed, however
//			if (log.isDebugEnabled()) {
//				log.debug("populateItems failed in updateInBackend");
//			}
//
//			throw new BackendException("populateItems failed in updateInBackend");
//		}
//		}catch (Exception ex){
//			log.error(ex);
//		}
//
//		//determine the header attributes for free goods
//		//PricingInfoData pricingInfo = getPricingInfo();
//		//find the free good items	
//		//freeGoodSupport.determineFreeGoods(pricingInfo);
//		
//		long endTime  = System.currentTimeMillis();
//		long diffTime = endTime - startTime;
//
//		if (log.isDebugEnabled()) {
//			log.debug("Updating a DB basket took " + diffTime + " msecs");
//		}
//	}

	/**
	 * recover the items for a salesDocument identified by the basketGuid 
	 * @param salesDoc
	 * @param basketGuid
	 * @return success
	 */
//	protected boolean recoverItemsDB(
//		SalesDocumentData salesDoc,
//		TechKey basketGuid) {
//        
//		boolean    success    = false;
//        
//		if (log.isDebugEnabled()) {
//			log.debug("Start recovering items");
//		}
//
//		// we only need this one for some create...-calls
//		ItemDB itemDB = new ItemDB(basketGuid, this);
//
//		// clear all the items first
//		items.clear();
//		itemGuids.clear();
//
//		// get the number of records for the basketGuid 
//		int    recordCount = itemDB.getRecordCount(basketGuid);
//
//		if (recordCount > 0) {
//            
//			if (log.isDebugEnabled()) {
//				log.debug(recordCount + "  items were found");
//			}
//            
//			// read them from the db
//			// order by number_int ASC
//			List itemDBList =
//				ItemDB.getItemDBsForBasketGuidAscending(this, basketGuid);
//            
//			ListIterator itemDBListIter = itemDBList.listIterator();
//
//			//List to hold grid sub-Items only
//			List gridSubItemDBs = new ArrayList(); 
//			
//			// read the values into the itemDBs
//			while (itemDBListIter.hasNext()) {               
//				itemDB = (ItemDB) itemDBListIter.next();
//				
//				itemDB.fillFromDatabase();
//				calculateItemValues(itemDB);
//                
//				// If the itemDB is grid SubItem, then copy it to create grid IPC sub-items
//				// after the creation of main IPC items
//				if (itemGuids.containsKey(itemDB.getParentId())){
//					gridSubItemDBs.add(itemDB);
//					continue;
//				}
//				
//				items.add(itemDB);
//				// get the textDataDB, if available
//				TextDataDB textDataDB =
//					TextDataDB.recoverTextDataDB(itemDB.getGuid(), this);
//
//				if (textDataDB != null) {
//					itemDB.setTextDataDB(textDataDB);
//				}
//
//				// IPCItem is created below via multi-call
//				itemGuids.put(itemDB.getGuid(), itemDB);
//			}
//
//			// must be called BEFORE recovering the extConfig
////			try {
////				createIPCItems(salesDoc);
////			} catch (BackendException e) {
////				if (debug) {
////					log.debug("recoverItemsDB, createIPCItems failed : " + e);
////				}
////			}
////			//applicable for creating grid Sub-items only
////			if (gridSubItemDBs != null && gridSubItemDBs.size() >0) {
////            
////				try {
////					createGridIPCSubItems(salesDoc, gridSubItemDBs);
////				} catch (BackendException e) {
////					if (debug) {
////						log.debug("recoverItemsDB, createGridIPCSubItems failed : " + e);
////					}
////				}
////			}
//			// loop over itemDB objects just created; if item is configurable, get 
//			// the config from the backend and store it in the ipcItem
//			itemDBListIter = itemDBList.listIterator();
//
//			while (itemDBListIter.hasNext()) {
//                
//				itemDB = (ItemDB) itemDBListIter.next();
//
//				if (log.isDebugEnabled()) {
//					log.debug(itemDB.getProduct());
//				}
//                
//				// find itemData linked to itemDB
////				if (itemDB.getExtConfigDB() != null ){
////					itemDB.getExtConfigDB().recoverFromBackend();
////					itemDB.getPricesFromIpcItem();
////                    
////					if (debug) {
////						try {
////							log.debug(
////								itemDB.getProduct()
////									+ " after recovering the extconfig : "
////									+ ((IPCItem) itemDB.getExternalItem())
////										.getConfig()
////										.toString());
////						} catch (Exception ex) {
////							log.debug(ex);
////						}
////					}  // endif debug
////				}  // endif itemDB....
//			}  // endwhile
//
//			try {
//				// copy the items to the salesdoc
//				// and store the reference to the new created ItemData
//				// in the ItemDB
//				readAllItemsFromBackend(salesDoc, false);
//
//				// check partneravailabilty
//				Iterator itemDBIterator = items.iterator();
//
//				while (itemDBIterator.hasNext()) {
//					((ItemDB) itemDBIterator.next())
//						.checkPartnerAvailabilityInfo();
//				}
//
//				success = true;
//			} catch (BackendException bex) {
//				if (log.isDebugEnabled()) {
//					log.debug(
//						"BackendException in readAllItemsFromBackend called from recoverItemsDB : "
//							+ bex.getMessage());
//				}
//			}
//		}
//		 // endif (recordCount > 0)
//		else {
//			// this one has also succeeded if there are no items found
//			// this basket does simply have no items
//			success = true;
//
//			// clear items in BO
//			salesDoc.clearItems();
//		}
//
//		return success;
//	}


//	public void readAllItemsFromBackend(SalesDocumentData posd, boolean change)
//		throws BackendException {
//		}


	public DAOFactoryBackend getDAOFactory() {
		 if (null == theDAOFactory) {
			 theDAOFactory = (DAOFactoryBackend) getContext().getAttribute("AUCDAOFactoryBackend");
	
			 if (null == theDAOFactory) {
				 if (log.isDebugEnabled()) {
					 log.debug("No DAOFactoryBackend available.");
				 }
			 }
		 }
		
		return theDAOFactory;
	}

	/**
	 * returns the flag if use a database
	 * @return isUseDatabase
	 */
	public boolean isUseDatabase(String daoDcumentType) {
		return false; 
	}

}
