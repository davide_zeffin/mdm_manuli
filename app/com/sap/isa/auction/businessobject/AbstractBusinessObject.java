package com.sap.isa.auction.businessobject;

import com.sap.tc.logging.Category;
import com.sap.tc.logging.Location;
import com.sap.isa.auction.exception.InvalidStateException;
import com.sap.isa.auction.exception.i18n.*;
import com.sap.isa.auction.logging.CategoryProvider;
import com.sap.isa.auction.services.ServiceLocator;

import com.sap.isa.auction.logging.*;	

import  java.util.Stack;
import java.util.EmptyStackException;

/**
 * Title:        Internet Sales
 * Description: Base Class for all BOs in SVE
 * 				BOM
 *              Context
 *              Tracing
 * 				Logging
 * Copyright:    Copyright (c) 2002
 * Company:      SAPMarkets
 * @author		i802891
 * @version 1.0
 */

public abstract class AbstractBusinessObject implements AuctionBusinessObject {
	/** This is the Single BOL Category for SVE Business Objects*/
    protected static final Category logger = CategoryProvider.getBOLCategory();
    
    protected AuctionBusinessObjectManagerBase bom = null;
    protected ServiceLocator locator = null;
    protected boolean initialized = false;

    protected AbstractBusinessObject() {
    }

    /**
     * This must be provided by Individual BOs
     * @return
     */
    protected abstract Location getTracer();

    /**
     * Sets the BOM who created this Manager
     */
    public void setAuctionBOM(AuctionBusinessObjectManagerBase bom) {
        this.bom = bom;
    }

    /**
     * All set calls must have been made before Business Object can trigger
     * its initialization
     */
    public void initialize() {
        initialized = true;
    }

    public void close() {
        this.bom = null;
		this.locator = null;
        initialized = false;
    }

    protected void checkInitialized() {
        if(!initialized) { 
        	ErrorMessageBundle master = ErrorMessageBundle.getInstance();
        	I18nErrorMessage msg = 
        		master.newI18nErrorMessage(InvalidStateException.NOT_INITIALIZED, new Object[]{"Business object"});
        	InvalidStateException ex = new InvalidStateException(msg);
        	throw ex;
        } 
    }

    /**
     * Provides the Calling Context bound with calling Thread
     * @throws MissingContextException
     */
    protected InvocationContext getInvocationContext() {
		final String METHOD_GETINVOCATIONCONTEXT = "getInvocationContext";    	
        InvocationContext invokeCtxt = ContextManager.getContext();
        // Context not bound
        if(invokeCtxt == null) {
			ErrorMessageBundle master = ErrorMessageBundle.getInstance();
			I18nErrorMessage msg = 
				master.newI18nErrorMessage(MissingContextException.CONTEXT_NOT_BOUND);
        	
            MissingContextException ex = new MissingContextException(msg);
            throwing(METHOD_GETINVOCATIONCONTEXT,ex);
            throw ex;
        }
        return invokeCtxt;
    }

    /** Thread specific*/
    private static ThreadLocal calltimeStack = new ThreadLocal();
	
	protected void _exiting(String methodName) {
	   Location tracer = getTracer();
	   if(logger != null && logger.bePath()) {
		   Stack localStack = (Stack)calltimeStack.get();
		   Long begin = null;
		   if(localStack != null) begin = (Long)localStack.pop();
		   long mls = -1; // undetermined
		   if(begin != null) {
			   mls = System.currentTimeMillis() - begin.longValue();
		   }
		   StringBuffer msg = new StringBuffer("exiting ");
		   msg.append(methodName);
		   msg.append(" ");
		   msg.append("[Time=");
		   msg.append(mls);
		   msg.append("mls]");
		   tracer.pathT(msg.toString());
	  	}
   }
      
   /**
     * Every Thread in Businees Object of Auction must call this method
     * upon entering the method.
     * Enter time is stored in Thread local
     * Tracing specific
     */
    private void _entering(String methodName) {
        Location tracer = getTracer();
        if(tracer != null && logger.beDebug()) {
            tracer.pathT("entering " + methodName);
            Stack localStack = (Stack)calltimeStack.get();
            if(localStack == null) {
                localStack = new Stack();
            }
            localStack.push(new Long(System.currentTimeMillis()));
            calltimeStack.set(localStack);
        }
    }
	/**
	 * if an exception is thrown at any level it clears the entire call time stack
	 */
	private void _throwing(String methodName, Throwable th) {
		Location tracer = getTracer();
		if(logger != null && logger.bePath()) {
			Stack localStack = (Stack)calltimeStack.get();
			if(localStack != null)
				try {
					localStack.clear(); // clear the stack of all
				}
				catch(EmptyStackException ignore) {
					catching("_throwing", ignore);
				}
			StringBuffer msg = new StringBuffer("exiting ");
			msg.append(methodName);
			msg.append("[exception raised: ");
			msg.append("type = ");
			msg.append(th.getClass().getName());
			msg.append(", msg = ]");
			tracer.debugT(LogUtil.getMsgPlusStackTraceAsString(th));
		}
	}

	/**
	 * If a number of Exception are raised in the methods. Stack can keep on
	 * growing.
	 * For clean up
	 */
	private static void clearCalltimeStack() {
		Stack localStack = (Stack)calltimeStack.get();
		if(localStack != null) localStack.clear();
	}
	
	protected void entering(String methodName, Object[] args) {
		TraceHelper.entering(getTracer(),methodName, args);
	}
	
	/**
	 * Every method in Business Object of Auction must call this method
	 * upon existing the method. A mismatch in enter and exit indicates the location
	 * leads to erroneous results of Time
	 * Tracing specific
	 */
	protected void exiting(String methodName, Object[] args) {
		TraceHelper.exiting(getTracer(),methodName,args);
	}
	
	protected void throwing(String methodName, Throwable th) {
		TraceHelper.throwing(getTracer(),methodName,th.getMessage(),th);
		LogHelper.logError(logger,getTracer(),th.getMessage(),th);
	}
	
	protected void catching(String methodName, Throwable th) {
		TraceHelper.catching(getTracer(),methodName,th);		
	}
	
	/* (non-Javadoc)
	 * @see com.sap.isa.auction.businessobject.AuctionBusinessObject#setServiceLocator(com.sap.isa.auction.services.ServiceLocator)
	 */
	public void setServiceLocator(ServiceLocator locator) {
		this.locator = locator;
	}
	/* (non-Javadoc)
	 * @see com.sap.isa.auction.businessobject.AuctionBusinessObject#setServiceLocator(com.sap.isa.auction.services.ServiceLocator)
	 */
	public ServiceLocator getServiceLocatorBase() {
		return locator;
	}
}