/*
 * Created on Jun 3, 2004
 *
 * Title:        Internet Sales Private Auctions
 * Description:
 * Copyright:    Copyright (c) 2004
 * Company:      SAP Labs LLC
 * @author
 * @version 1.0
 */
package com.sap.isa.auction.businessobject.b2x;

import java.util.Collection;

import javax.jdo.PersistenceManager;
import javax.jdo.PersistenceManagerFactory;
import javax.jdo.Query;

import com.sap.isa.auction.businessobject.b2x.jdo.PrivateAuctionDAO;
import com.sap.isa.auction.config.SchedulerConfig;
import com.sap.isa.auction.exception.i18n.ErrorMessageBundle;
import com.sap.isa.auction.exception.i18n.I18nErrorMessage;
import com.sap.isa.auction.logging.LogHelper;
import com.sap.isa.auction.services.BackgroundJobManagerBase;
import com.sap.isa.auction.services.InitializeException;
import com.sap.isa.auction.services.ServiceLocator;
import com.sap.isa.auction.services.b2x.BackgroundJobManager;
import com.sap.tc.logging.Location;

/**
 * @author I802850
 *
 * To change the template for this generated type comment go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
public class AVWSellerServiceLocator extends ServiceLocator {
	public static void setRuntimeContext(Object ctxt) {
	}
	private static Location tracer = Location.getLocation(AVWSellerServiceLocator.class.getName());

	public AVWSellerServiceLocator() {
		super();
	}
	
	protected BackgroundJobManagerBase getBkMgr(SchedulerConfig configData,Collection coll){
		return new BackgroundJobManager(configData,coll);
	}
	protected void _initialize() throws InitializeException {
		super._initialize();
		try {
			PersistenceManagerFactory pmf = getPersistenceManagerFactory();
			PersistenceManager pm  =  pmf.getPersistenceManager();
			Query query  = pm.newQuery(PrivateAuctionDAO.class);
			query.compile();
			query.closeAll();
			pm.close();
		}
		catch(Throwable th) {
			initialized = false;
			ErrorMessageBundle bundle = ErrorMessageBundle.getInstance();
			I18nErrorMessage msg = bundle.newI18nErrorMessage(InitializeException.COMPONENT_INIT_FAILED);
			msg.setArguments(new String[]{"ServiceLocator"});
			InitializeException ex = new InitializeException(msg,th);
			LogHelper.logError(logger,tracer,ex.getMessage(),ex);
			throw ex;
		}		
	}
}
