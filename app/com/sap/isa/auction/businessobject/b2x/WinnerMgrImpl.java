package com.sap.isa.auction.businessobject.b2x;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;

import javax.jdo.JDOException;
import javax.jdo.PersistenceManager;
import javax.jdo.PersistenceManagerFactory;
import javax.jdo.Query;

import com.sap.isa.auction.bean.Auction;
import com.sap.isa.auction.bean.AuctionStatusEnum;
import com.sap.isa.auction.bean.AuctionTypeEnum;
import com.sap.isa.auction.bean.Winner;
import com.sap.isa.auction.bean.b2x.PrivateAuction;
import com.sap.isa.auction.bean.b2x.PrivateAuctionWinner;
import com.sap.isa.auction.bean.b2x.WinnerDeterminationTypeEnum;
import com.sap.isa.auction.businessobject.AbstractBusinessObject;
import com.sap.isa.auction.businessobject.AuctionManager;
import com.sap.isa.auction.businessobject.ValidationException;
import com.sap.isa.auction.businessobject.b2x.jdo.AuctionDAO;
import com.sap.isa.auction.businessobject.b2x.jdo.PrivateAuctionBidDAO;
import com.sap.isa.auction.businessobject.b2x.jdo.PrivateAuctionDAO;
import com.sap.isa.auction.businessobject.b2x.jdo.PrivateAuctionWinnerDAO;
import com.sap.isa.auction.businessobject.order.AuctionOrder;
import com.sap.isa.auction.exception.SystemException;
import com.sap.isa.auction.exception.UserException;
import com.sap.isa.auction.exception.i18n.ErrorMessageBundle;
import com.sap.isa.auction.exception.i18n.I18nErrorMessage;
import com.sap.isa.auction.logging.LogHelper;
import com.sap.isa.auction.logging.TraceHelper;
import com.sap.isa.auction.services.ServiceLocator;
import com.sap.isa.businessobject.order.Order;
import com.sap.isa.core.util.OIDGenerator;
import com.sap.tc.logging.Location;

/**
 * Title:
 * Description: Stateless and Pooled Business Object
 * Copyright:    Copyright (c) 2003
 * Company:
 * @author
 * @version 1.0
 */

public class WinnerMgrImpl
	extends AbstractBusinessObject
	implements PrivateAuctionWinnerManager {

	private static Location tracer =
		Location.getLocation(WinnerMgrImpl.class.getName());

	public WinnerMgrImpl() {
	}

	public void initialize() {
		super.initialize();
	}

	public void close() {
		super.close();
	}

	protected Location getTracer() {
		return tracer;
	}

	private void changeAuctionStatus(
		AuctionManager auctionMgr,
		PrivateAuction pauction,
		boolean isWinnerPresent) {
		if (isWinnerPresent) {
			auctionMgr.modifyStatus(pauction, AuctionStatusEnum.FINALIZED);
		} else {
			auctionMgr.modifyStatus(pauction, AuctionStatusEnum.NOT_FINALIZED);
		}
	}

	private void internalModifyAuctionStatusAndQuantity(
		PrivateAuction auction,
		PrivateAuctionWinner winner,
		PersistenceManager pm)
		throws Exception {
		final String METHOD = "internalModifyAuctionStatusAndQuantity";
		int quantity = -1;
		boolean isBrokenLot =
			auction.getType() == AuctionTypeEnum.BROKEN_LOT.getValue();
		if (isBrokenLot
			&& auction.getBuyItNowPrice() != null
			&& auction.getBuyItNowPrice().doubleValue() > 0.0) {
			if (auction.getQuantity() != winner.getQuantity().intValue()) {
				quantity =
					auction.getQuantity() - winner.getQuantity().intValue();
			}
		}
		TraceHelper.entering(tracer, METHOD);
		// Validate the auction status transition
		//        if(newStatus.equals(newStatus.)
		Timestamp modDate = new java.sql.Timestamp(System.currentTimeMillis());
		try {
			// @@temp: begin hack for JDO bug in loading of meta data
			Query query = pm.newQuery(PrivateAuctionDAO.class);
			query.compile();
			query.closeAll();
			// @@temp : end
			PrivateAuctionDAO auctionDAO =
				(PrivateAuctionDAO) pm.getObjectById(
					new AuctionDAO.Id(auction.getAuctionId()),
					false);
			// Log Warning 
			if (auctionDAO == null) {
				logger.errorT(
					tracer,
					"Invalid Auction" + auction.getAuctionId());
				ErrorMessageBundle bundle = ErrorMessageBundle.getInstance();
				I18nErrorMessage msg =
					bundle.newI18nErrorMessage(
						ValidationException.INVALID_AUCTION);
				ValidationException ex = new ValidationException(auction, msg);
				throw ex;
			}
			if (isBrokenLot
				&& auction.getBuyItNowPrice() != null
				&& auction.getBuyItNowPrice().doubleValue() > 0.0) {
				auctionDAO.setQuantity(quantity);
				if (quantity == 0)
					auctionDAO.setStatus(AuctionStatusEnum.FINALIZED.toInt());
			} else
				auctionDAO.setStatus(AuctionStatusEnum.FINALIZED.toInt());
			auctionDAO.setModifiedDate(modDate);
			pm.makePersistent(auctionDAO);
			auction.setLastModifiedDate(modDate);
			auction.setStatus(auctionDAO.getStatus());
			TraceHelper.exiting(tracer, METHOD);
		} catch (Exception ex) {
			throw ex;
		}
	}

	//TODO Check if the winner already exists.Probable use of the crt framework 
	//TODO Followup to the above first question 
	// TODO How to retrieve the record esp in the case of the multiple winners for a single auction
	private void internalCreateWinner(
		PrivateAuction auction,
		PrivateAuctionWinner winner)
		throws Exception {
		PersistenceManager pm = null;
		try {
			PersistenceManagerFactory pmf =
				locator.getPersistenceManagerFactory();
			pm = pmf.getPersistenceManager();
			PrivateAuctionWinnerDAO winnerDAO = new PrivateAuctionWinnerDAO();
			DALBeanSynchronizer.synchWinnerDAOFromBean(winner, winnerDAO);
			winnerDAO.setId(winner.getWinnerId());
			pm.currentTransaction().begin();
			pm.makePersistent(winnerDAO);
			//			pm.currentTransaction().commit();
			orderProcessExecutor(auction, winner);
			//			pm.currentTransaction().begin();
			winnerDAO.setOrderId(winner.getOrderId());
			winnerDAO.setOrderGuid(winner.getOrderGuid().getIdAsString());
			winnerDAO.setBid(
				getBidDAO(winner.getBidId(), pm, auction.getAuctionId()));
			//			winnerDAO.setBid();
			//			pm.makePersistent(winnerDAO);
			internalModifyAuctionStatusAndQuantity(auction, winner, pm);
			pm.currentTransaction().commit();
			pm.close();
			pm = null;
		} catch (Exception e) {
			//TODO if order has been created revert the order in the backend
			if (e instanceof JDOException) {
				if (pm != null && pm.currentTransaction().isActive()) {
					pm.currentTransaction().rollback();
				}
			}
			throw e;
		} finally {
			if (pm != null && !pm.isClosed()) {
				pm.close();
				pm = null;
			}

		}
	}
	private void orderProcessExecutor(
		PrivateAuction auctiondao,
		PrivateAuctionWinner winner) {
		/* ?	Create Order
		?	Update Winner */
		// Create the order
		Order order = createOrder(auctiondao, winner);
	}

	private Order createOrder(
		PrivateAuction auction,
		PrivateAuctionWinner winner) {
		ProcessBusinessObjectManager pbom =
			((AuctionBusinessObjectManager)(super.bom)).getProcessBusinessObjectManager();
		AuctionOrder order =
			pbom.createOrder(auction, winner, winner.getSoldToParty());
		return order;
	}

	/**
	 * Internal method
	 */
	//check whether Auction is needed as a paramater
	// auction object is passed
	public void createWinner(
		PrivateAuctionWinner winner,
		PrivateAuction auction) {
		checkInitialized();
		winner.setWinnerId(OIDGenerator.newOID());
		winner.setClient(auction.getClient());
		winner.setSystemId(auction.getSystemId());
		winner.setWebShopId(auction.getWebShopId());
		winner.setCreatedDate(new Timestamp(System.currentTimeMillis()));
		String METHOD = "createWinner";
		if (winner == null || auction == null) {
			ErrorMessageBundle bundle = ErrorMessageBundle.getInstance();
			I18nErrorMessage msg =
				bundle.newI18nErrorMessage(UserException.ILLEGAL_ARGUMENT);
			msg.setArguments(new String[] { "winner or auction" });
			UserException ex = new UserException(msg);
			super.throwing(METHOD, ex);
			throw ex;
		}

		ServiceLocator serv = getServiceLocatorBase();
		PersistenceManager pm = null;
		try {
			if(auction.getWinnerTypeEnum().getValue() == WinnerDeterminationTypeEnum.INT_MANUAL) {
				Winner[] winners = getWinners(auction);
				int totalQty = 0;
				if (winners != null && winners.length > 0) {
					for (int i = 0; i < winners.length; i++) {
						totalQty += winners[i].getQuantity().intValue();
					}
					if (totalQty + winner.getQuantity().intValue()
						> auction.getQuantity()) {
						// Throw a validation exception
						ErrorMessageBundle bundle =
							ErrorMessageBundle.getInstance();
						I18nErrorMessage msg =
							bundle.newI18nErrorMessage(
								ValidationException.INVALID_QTY_FOR_WINNER);
						winner.addError(msg);
						ArrayList culprits = new ArrayList();
						culprits.add(winner);
						bundle = ErrorMessageBundle.getInstance();
						msg =
							bundle.newI18nErrorMessage(
								SystemException.INTERNAL_ERROR);
						ValidationException ex =
							new ValidationException(culprits, msg);
						super.throwing(METHOD, ex);
						throw ex;
					}
				}
			}
			internalCreateWinner(auction, winner);
		} catch (Exception jdoex) {
			logger.errorT(
				tracer,
				"Failed to create winners in persistence for auction "
					+ auction.getAuctionName());
			try {
				if (pm != null && pm.currentTransaction().isActive()) {
					// rollback the tx
					pm.currentTransaction().rollback();
				}
			} catch (Exception ex) {
				LogHelper.logWarning(
					logger,
					tracer,
					"Rollback of Tx failed in createWinners call",
					ex);
			}
			if (jdoex instanceof UserException) {
				UserException _ex = (UserException) jdoex;
				super.throwing(METHOD, _ex);
				throw _ex;
			} else if (jdoex instanceof SystemException) {
				SystemException _ex = (SystemException) jdoex;
				super.throwing(METHOD, _ex);
				throw _ex;
			} else {
				ErrorMessageBundle bundle = ErrorMessageBundle.getInstance();
				I18nErrorMessage msg =
					bundle.newI18nErrorMessage(SystemException.INTERNAL_ERROR);
				SystemException _ex = new SystemException(msg, jdoex);
				super.throwing(METHOD, _ex);
				throw _ex;
			}
		} finally {
			// close the pm
			if (pm != null)
				pm.close();
		}
	}

	/**
	 * Internal method
	 */
	//check whether Auction is needed as a paramater
	// auction object is passed
	public void updateWinner(
		PrivateAuctionWinner winner,
		PrivateAuction auction) {
		checkInitialized();
		getInvocationContext();
		String METHOD = "updateWinner";
		if (winner == null || auction == null) {
			ErrorMessageBundle bundle = ErrorMessageBundle.getInstance();
			I18nErrorMessage msg =
				bundle.newI18nErrorMessage(UserException.ILLEGAL_ARGUMENT);
			msg.setArguments(new String[] { "winner or auction" });
			UserException ex = new UserException(msg);
			super.throwing(METHOD, ex);
			throw ex;
		}

		ServiceLocator serv = getServiceLocatorBase();
		PersistenceManager pm = null;
		try {
			internalUpdateWinner(auction, winner);
		} catch (Exception jdoex) {
			logger.errorT(
				tracer,
				"Failed to create winners in persistence for auction "
					+ auction.getAuctionName());
			try {
				if (pm != null && pm.currentTransaction().isActive()) {
					// rollback the tx
					pm.currentTransaction().rollback();
				}
			} catch (Exception ex) {
				LogHelper.logWarning(
					logger,
					tracer,
					"Rollback of Tx failed in createWinners call",
					ex);
			}
			if (jdoex instanceof UserException) {
				UserException _ex = (UserException) jdoex;
				super.throwing(METHOD, _ex);
				throw _ex;
			} else if (jdoex instanceof SystemException) {
				SystemException _ex = (SystemException) jdoex;
				super.throwing(METHOD, _ex);
				throw _ex;
			} else {
				ErrorMessageBundle bundle = ErrorMessageBundle.getInstance();
				I18nErrorMessage msg =
					bundle.newI18nErrorMessage(SystemException.INTERNAL_ERROR);
				SystemException _ex = new SystemException(msg, jdoex);
				super.throwing(METHOD, _ex);
				throw _ex;
			}
		} finally {
			// close the pm
			if (pm != null)
				pm.close();
		}
	}

	/**
	 * @param auction
	 * @param winner
	 */
	private void internalUpdateWinner(
		PrivateAuction auction,
		PrivateAuctionWinner winner)
		throws Exception {
		PersistenceManager pm = null;
		try {
			PersistenceManagerFactory pmf =
				locator.getPersistenceManagerFactory();
			pm = pmf.getPersistenceManager();
			Query query = pm.newQuery(PrivateAuctionWinnerDAO.class);
			query.setFilter("id == " + "\"" + winner.getWinnerId() + "\"");
			Collection coll = (Collection) query.execute();
			Iterator iter = coll.iterator();
			PrivateAuctionWinnerDAO winnerDAO = null;
			if (iter.hasNext()) {
				winnerDAO = (PrivateAuctionWinnerDAO) iter.next();
			}
			if (winnerDAO == null)
				throw new Exception("Winner non existent");
			pm.currentTransaction().begin();
			DALBeanSynchronizer.synchWinnerDAOFromBean(winner, winnerDAO);
			pm.makePersistent(winnerDAO);
			pm.currentTransaction().commit();
			pm.close();
			pm = null;
		} catch (Exception e) {
			if (e instanceof JDOException) {
				if (pm != null && !pm.currentTransaction().isActive()) {
					pm.currentTransaction().rollback();
				}
				throw e;
			}
		} finally {
			if (pm != null && !pm.isClosed()) {
				pm.close();
				pm = null;
			}

		}
	}

	/**
	 * List of Winners for a Finalized Auction
	 */
	public Winner[] getWinners(Auction auction) {
		String METHOD = "getWinners";
		checkInitialized();

		PersistenceManager pm = null;
		List winnerList;
		try {
			String auctionId = auction.getAuctionId();
			ServiceLocator locator = getServiceLocatorBase();
			PersistenceManagerFactory pmf =
				locator.getPersistenceManagerFactory();
			pm = pmf.getPersistenceManager();
			// @@temp: begin hack for JDO bug in loading of meta data
			Query query = pm.newQuery(PrivateAuctionWinnerDAO.class);
			StringBuffer filter = new StringBuffer("");
			filter.append("auctionId == ");
			filter.append("\"");
			filter.append(auctionId);
			filter.append("\"");
			query.setFilter(filter.toString());

			if (logger.beDebug()) {
				tracer.debugT(METHOD, "query string: " + query.toString());
			}
			Collection coll = (Collection) query.execute();

			// nps:: JDO has bug for collection sizeLog Warning
			if (coll == null || coll.iterator().hasNext() == false) {
				if (tracer.beDebug()) {
					tracer.debugT(
						METHOD,
						"!!No winner for the auction ID " + auctionId);
				}
				return null;
			}

			Iterator iter = coll.iterator();
			winnerList = new ArrayList();
			while (iter.hasNext()) {
				PrivateAuctionWinnerDAO winnerDAO =
					(PrivateAuctionWinnerDAO) iter.next();
				PrivateAuctionWinner winner = new PrivateAuctionWinner();
				DALBeanSynchronizer.synchWinnerBeanFromDAO(winnerDAO, winner);
				winnerList.add(winner);
			}
			// sync
			query.closeAll();
			Winner[] winners = new Winner[winnerList.size()];
			winnerList.toArray(winners);
			return winners;
		} catch (Exception ex) {
			if (ex instanceof UserException) {
				UserException _ex = (UserException) ex;
				super.throwing(METHOD, _ex);
				throw _ex;
			} else if (ex instanceof SystemException) {
				SystemException _ex = (SystemException) ex;
				super.throwing(METHOD, _ex);
				throw _ex;
			} else {
				ErrorMessageBundle bundle = ErrorMessageBundle.getInstance();
				I18nErrorMessage msg =
					bundle.newI18nErrorMessage(SystemException.INTERNAL_ERROR);
				SystemException _ex = new SystemException(msg);
				super.throwing(METHOD, _ex);
				throw _ex;
			}
		} finally {
			// close the pm
			if (pm != null)
				pm.close();
		}

	}

	/**
	 * Retreival of Winner by Id
	 */
	//cannot be supported as the winner cannot be retreived based on winner Id, but on auction Id
	public Winner getWinner(Auction auction, String winnerId) {
		throw new UnsupportedOperationException("method getWinner is not implemented");
	}

	/**
	 * List of Winners for a Finalized Auction
	 */
	public Winner getWinnerByOrderId(String orderId) {
		String METHOD = "getWinners";
		checkInitialized();

		PersistenceManager pm = null;
		List winnerList;
		try {
			ServiceLocator locator = getServiceLocatorBase();
			PersistenceManagerFactory pmf =
				locator.getPersistenceManagerFactory();
			pm = pmf.getPersistenceManager();
			// @@temp: begin hack for JDO bug in loading of meta data
			Query query = pm.newQuery(PrivateAuctionWinnerDAO.class);
			StringBuffer filter = new StringBuffer("");
			filter.append("orderId == ");
			filter.append("\"");
			filter.append(orderId);
			filter.append("\"");
			query.setFilter(filter.toString());
			if (logger.beDebug()) {
				tracer.debugT(METHOD, "query string: " + query.toString());
			}
			Collection coll = (Collection) query.execute();

			// nps:: JDO has bug for collection sizeLog Warning
			if (coll == null || coll.size() == 0) {
				if (tracer.beDebug()) {
					tracer.debugT(
						METHOD,
						"!!No winner for the auction ID " + orderId);
				}
				return null;
			}

			Iterator iter = coll.iterator();
			PrivateAuctionWinner winner = null;
			if (iter.hasNext()) {
				PrivateAuctionWinnerDAO winnerDAO =
					(PrivateAuctionWinnerDAO) iter.next();
				winner = new PrivateAuctionWinner();
				DALBeanSynchronizer.synchWinnerBeanFromDAO(winnerDAO, winner);
			}
			// sync
			query.closeAll();
			return winner;
		} catch (Exception ex) {
			if (ex instanceof UserException) {
				UserException _ex = (UserException) ex;
				super.throwing(METHOD, _ex);
				throw _ex;
			} else if (ex instanceof SystemException) {
				SystemException _ex = (SystemException) ex;
				super.throwing(METHOD, _ex);
				throw _ex;
			} else {
				ErrorMessageBundle bundle = ErrorMessageBundle.getInstance();
				I18nErrorMessage msg =
					bundle.newI18nErrorMessage(SystemException.INTERNAL_ERROR);
				SystemException _ex = new SystemException(msg);
				super.throwing(METHOD, _ex);
				throw _ex;
			}
		} finally {
			// close the pm
			if (pm != null)
				pm.close();
		}

	}
	/**
	 * List of Winners for a Finalized Auction
	 */
	public PrivateAuctionBidDAO getBidDAO(
		String bidId,
		PersistenceManager pm,
		String auctionId) {
		String METHOD = "getWinners";
		checkInitialized();

		try {
			ServiceLocator locator = getServiceLocatorBase();
			// @@temp: begin hack for JDO bug in loading of meta data
			Query query = pm.newQuery(PrivateAuctionBidDAO.class);
			StringBuffer filter = new StringBuffer("");
			filter.append("( ");
			filter.append("id == ");
			filter.append("\"");
			filter.append(bidId);
			filter.append("\"");
			filter.append(" &&  ");
			filter.append("auctionId == ");
			filter.append("\"");
			filter.append(auctionId);
			filter.append("\"");
			filter.append(")");
			query.setFilter(filter.toString());
			if (logger.beDebug()) {
				tracer.debugT(METHOD, "query string: " + query.toString());
			}
			Collection coll = (Collection) query.execute();

			// nps:: JDO has bug for collection sizeLog Warning
			if (coll == null || coll.iterator().hasNext() == false) {
				if (tracer.beDebug()) {
					tracer.debugT(METHOD, "!!No Bid for the bid ID " + bidId);
				}
				return null;
			}

			Iterator iter = coll.iterator();
			PrivateAuctionBidDAO bidDAO = null;
			if (iter.hasNext()) {
				bidDAO = (PrivateAuctionBidDAO) iter.next();

			}
			// sync
			query.closeAll();
			return bidDAO;
		} catch (Exception ex) {
			if (ex instanceof UserException) {
				UserException _ex = (UserException) ex;
				super.throwing(METHOD, _ex);
				throw _ex;
			} else if (ex instanceof SystemException) {
				SystemException _ex = (SystemException) ex;
				super.throwing(METHOD, _ex);
				throw _ex;
			} else {
				ErrorMessageBundle bundle = ErrorMessageBundle.getInstance();
				I18nErrorMessage msg =
					bundle.newI18nErrorMessage(SystemException.INTERNAL_ERROR);
				SystemException _ex = new SystemException(msg);
				super.throwing(METHOD, _ex);
				throw _ex;
			}
		}

	}
}
