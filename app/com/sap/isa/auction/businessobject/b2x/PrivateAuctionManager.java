package com.sap.isa.auction.businessobject.b2x;

import com.sap.isa.auction.bean.Auction;
import com.sap.isa.auction.bean.AuctionStatusEnum;
import com.sap.isa.auction.bean.b2x.PrivateAuction;
import com.sap.isa.auction.bean.search.*;
import com.sap.isa.auction.businessobject.AuctionManager;

import java.util.Collection;


/**
 * Title:
 * Description:
 * Copyright:    Copyright (c) 2003
 * Company: SAP Labs Palo Alto, LLC
 * @author: narinder.singh@sap.com
 * @version 1.0
 */

public interface PrivateAuctionManager extends AuctionManager {

    /**
     *
     */
    public void createAuction(Auction auction);
    
	/**
	 * @see com.sap.isa.auction.businessobject.AuctionManager#createAuctionByRef(com.sap.isa.auction.bean.Auction, com.sap.isa.auction.bean.Auction)
	 */
	public void createAuctionByRef(Auction auction, Auction refAuction);

	/**
	 * @see com.sap.isa.auction.businessobject.AuctionManager#createAuctions(com.sap.isa.auction.bean.Auction[])
	 */
	public void createAuctions(Auction[] auctions);
	
    /**
     * @param auction NON FINALIZED auction which was relisted
     * @return Collection relistings of <code>auction</code>
     */
    public Collection getRelistings(PrivateAuction auction);

    /**
     * @param auctionId id of the Auction Template
     */
    public Auction getAuctionById(String auctionId);
    
    /**
     * @param templateId id of the Auction Template
     */
    public Auction getAuctionTemplateById(String templateId);

    /**
     *
     */
    public void modifyStatus(Auction auction, AuctionStatusEnum newStatus);

    /**
     * No Status change allowed, here
     */
    public void modifyAuction(Auction auction);

    /**
     * Auction can only be deleted if it is not published to
     * eBay
     */
    public void deleteAuction(Auction auction);

    /**
     * Search specific functions. Only one API
     * Order is important. Based on last synchronization with eBay
     */
    public QueryResult searchAuctions(QueryFilter filer);

    /**
     * User specific scheduling of bkgd synchronization of the Auction
     * specific changes.
     *
     * Only one per Seller allowed
     *
     */
    //public void setSynchronization(Synchronization sync);

    //public Synchronization getSynchronization();

    /**
     * Cancels the existing scheduling of batch processing
     */
    //public void cancelSychronization(Synchronization sync);

    /**
     * List of Auctions to which user made changes but they are not yet synchronized
     * with eBay
     */
    //public Auction[] getPendingSynchronization();

    /**
     * Manual batch processing of Auction Synchronization Task
     * This call will trigger the pending synchronizations now and return immediately
     * @param callback provides the reporting on results of callback. It is made
     * on exactly the same instance as provided in the class
     */
    //public AsyncResult triggerPendingSynchronization(Auction[] auctions, AsyncCallback[] callbacks);

    /**
     * This function returns the auctions scheduled which has schedule date less than the Date
     * @param date Schedule date of the auctions
     * @return collection of auction bean objects
     */
    //public Collection getAuctionsToBePublished(Date date);

}