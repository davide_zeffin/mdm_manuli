/*
 * Created on Apr 23, 2004
 *
 * To change the template for this generated file go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
package com.sap.isa.auction.businessobject.b2x;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import javax.jdo.JDOException;
import javax.jdo.PersistenceManager;
import javax.jdo.PersistenceManagerFactory;
import javax.jdo.Query;

import com.sap.isa.auction.bean.AuctionStatusEnum;
import com.sap.isa.auction.bean.ExecutionStatusEnum;
import com.sap.isa.auction.bean.OrderStatusEnum;
import com.sap.isa.auction.bean.b2x.AuctionVisibilityEnum;
import com.sap.isa.auction.bean.b2x.PrivateAuction;
import com.sap.isa.auction.bean.search.Criteria;
import com.sap.isa.auction.bean.search.QueryFilter;
import com.sap.isa.auction.bean.search.QueryResult;
import com.sap.isa.auction.businessobject.AbstractBusinessObject;
import com.sap.isa.auction.businessobject.AuctionSearchAttributes;
import com.sap.isa.auction.businessobject.SearchManager;
import com.sap.isa.auction.businessobject.b2x.jdo.PrivateAuctionDAO;
import com.sap.isa.auction.businessobject.targetGroup.TargetGroup;
import com.sap.isa.auction.exception.SystemException;
import com.sap.isa.auction.exception.i18n.ErrorMessageBundle;
import com.sap.isa.auction.exception.i18n.I18nErrorMessage;
import com.sap.isa.auction.logging.TraceHelper;
import com.sap.isa.auction.services.ServiceLocator;
import com.sap.isa.user.businessobject.UserBase;
import com.sap.tc.logging.Location;

/**
 * Title:			Internet Sales Private Auctions
 * Description:
 * @Copyright: 		Copyright (c) 2004
 * Company:			SAPLabs LLC
 * @author 
 *
 * To change the template for this generated type comment go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
public class SearchManagerImpl
	extends AbstractBusinessObject
	implements SearchManager {
	
	Location tracer = Location.getLocation(SearchManagerImpl.class);
	private static final String BUYER_ORDERBY = AuctionSearchAttributes.END_DATE;
	/* (non-Javadoc)
	 * @see com.sap.isa.auction.businessobject.SearchManager#searchAuctions(com.sap.isa.auction.bean.search.QueryFilter)
	 */
	public QueryResult searchAuctions(
		QueryFilter filter,
		Collection bps,
		Collection tgs,
		boolean sellerSearch) {
		final String METHOD = "searchAuctions";
		TraceHelper.entering(tracer, METHOD);
		checkInitialized();
		InvocationContext ic = (InvocationContext) getInvocationContext();
		checkPermission(ic.getUser(), METHOD);
		TraceHelper.exiting(tracer, METHOD);
		return internalSearchAuctions(filter, bps, tgs, sellerSearch);
	}

	/* (non-Javadoc)
	 * @see com.sap.isa.auction.businessobject.AbstractBusinessObject#getTracer()
	 */
	protected Location getTracer() {
		return tracer;
	}

	private QueryResult internalSearchAuctions(
		QueryFilter filter,
		Collection bps,
		Collection tgs,
		boolean sellerSearch) {
		final String METHOD = "internalSearchAuctions";
		TraceHelper.entering(tracer, METHOD);

		ServiceLocator provider = (ServiceLocator) getServiceLocatorBase();
		QueryResult result = null;
		PersistenceManager pm = null;
		boolean error = false;
		try {
			PersistenceManagerFactory pmf =
				provider.getPersistenceManagerFactory();
			pm = pmf.getPersistenceManager();
			Query query = pm.newQuery(PrivateAuctionDAO.class);
			StringBuffer queryFilter = new StringBuffer();
			StringBuffer vars = new StringBuffer();
			StringBuffer params = new StringBuffer();
			Map paramValues = new HashMap();

			addClientSysIdAndShopIdFilter(queryFilter);

			if (sellerSearch) {
				queryFilter.append(" && ");
				addSellerSpecificFilterCriteria(queryFilter, bps, tgs, vars);
			} else {
				queryFilter.append(" && ");
				addBuyerSpecificFilterCriteria(queryFilter, bps, tgs, vars);
			}

			Iterator it = filter.getCriterias().iterator();
			String[] criteriaNames = new String[filter.getCriterias().size()];
			int index = 0;
			while (it.hasNext()) {
				//Expecting only criteria as of now				
				Criteria crit = (Criteria) it.next();
				//				if (crit.name.equals(PrivateAuctionQueryFilter.WEBSHOP_ID)) {
				//					// special handling of name if web shop Id is present
				//					if (queryFilter.length() > 0) {
				//						queryFilter.append(" && ");
				//					}
				//					generateQuery(
				//						queryFilter,
				//						vars,
				//						"webShopId",
				//						String.class,
				//						crit,
				//						false,
				//						null,
				//						null);
				//				} else 
				criteriaNames[index++] = crit.name;
				if (crit.name.equals(PrivateAuctionQueryFilter.SALESAREA)) {
					// special handling of name if web shop Id is present
					if (queryFilter.length() > 0) {
						queryFilter.append(" && ");
					}
					generateQuery(
						queryFilter,
						vars,
						params,
						paramValues,
						"salesArea",
						String.class,
						crit,
						false,
						false,
						null,
						null);
				} else if (crit.name.equals(PrivateAuctionQueryFilter.NAME)) {
					// special handling of name if web shop Id is present
					if (queryFilter.length() > 0) {
						queryFilter.append(" && ");
					}
					generateQuery(
						queryFilter,
						vars,
						params,
						paramValues,
						"auctionName",
						String.class,
						crit,
						true,
						false,
						null,
						null);
				} else if (crit.name.equals(PrivateAuctionQueryFilter.TITLE)) {
					if (queryFilter.length() > 0) {
						queryFilter.append(" && ");
					}
					generateQuery(
						queryFilter,
						vars,
						params,
						paramValues,						
						"title",
						String.class,
						crit,
						true,
						false,
						null, null);
			} else if (crit.name.equals(PrivateAuctionQueryFilter.ORDER_ID)) {
				if (queryFilter.length() > 0) {
					queryFilter.append(" && ");
				}
				generateQuery(
					queryFilter,
					vars,
					params,
					paramValues,						
					"winners",
					String.class,
					crit,
					true,
					true,
					"PrivateAuctionWinnerDAO",
					"orderId");	
			} else if (
					crit.name.equals(PrivateAuctionQueryFilter.END_DATE)) {
					// special handling of name if web shop Id is present
					if (queryFilter.length() > 0) {
						queryFilter.append(" && ");
					}
					generateQuery(
						queryFilter,
						vars,
						params,
						paramValues,
						"endDate",
						Date.class,
						crit,
						false,
						false,
						null,
						null);
				} else if (
					crit.name.equals(PrivateAuctionQueryFilter.START_DATE)) {
					// special handling of name if web shop Id is present
					if (queryFilter.length() > 0) {
						queryFilter.append(" && ");
					}
					generateQuery(
						queryFilter,
						vars,
						params,
						paramValues,
						"startDate",
						Date.class,
						crit,
						false,
						false,
						null,
						null);
				} else if (
					crit.name.equals(PrivateAuctionQueryFilter.PUBLISH_DATE)) {
					// special handling of name if web shop Id is present
					if (queryFilter.length() > 0) {
						queryFilter.append(" && ");
					}
					generateQuery(
						queryFilter,
						vars,
						params,
						paramValues,
						"publishDate",
						Date.class,
						crit,
						false,
						false,
						null,
						null);
				} else if (
					crit.name.equals(PrivateAuctionQueryFilter.CREATE_DATE)) {
					// special handling of name if web shop Id is present
					if (queryFilter.length() > 0) {
						queryFilter.append(" && ");
					}
					generateQuery(
						queryFilter,
						vars,
						params,
						paramValues,
						"createdDate",
						Date.class,
						crit,
						false,
						false,
						null,
						null);
				} else if (
					crit.name.equals(PrivateAuctionQueryFilter.PRODUCT_ID)) {
					// special handling of name if web shop Id is present
					if (queryFilter.length() > 0) {
						queryFilter.append(" && ");
					}
					generateQuery(
						queryFilter,
						vars,
						params,
						paramValues,
						"auctionItems",
						String.class,
						crit,
						false,
						true,
						"PrivateAuctionItemDAO",
						"productCode");
				} else if (
					crit.name.equals(PrivateAuctionQueryFilter.PRODUCT_GUID)) {
					// special handling of name if web shop Id is present
					if (queryFilter.length() > 0) {
						queryFilter.append(" && ");
					}
					generateQuery(
						queryFilter,
						vars,
						params,
						paramValues,
						"auctionItems",
						String.class,
						crit,
						false,
						true,
						"PrivateAuctionItemDAO",
						"productId");
				} else if (
					crit.name.equals(PrivateAuctionQueryFilter.STATUS)) {
					// special handling of name if web shop Id is present
					if (queryFilter.length() > 0) {
						queryFilter.append(" && ");
					}
					generateQuery(
						queryFilter,
						vars,
						params,
						paramValues,
						"status",
						Integer.class,
						crit,
						false,
						false,
						null,
						null);
				} else if (
					crit.name.equals(PrivateAuctionQueryFilter.START_PRICE)) {
					if (queryFilter.length() > 0) {
						queryFilter.append(" && ");
					}
					generateQuery(
						queryFilter,
						vars,
						params,
						paramValues,
						"startPrice",
						Double.class,
						crit,
						false,
						false,
						null,
						null);
				} else if (
					crit.name.equals(
						PrivateAuctionQueryFilter.RESERVE_PRICE)) {
					if (queryFilter.length() > 0) {
						queryFilter.append(" && ");
					}
					generateQuery(
						queryFilter,
						vars,
						params,
						paramValues,
						"reservePrice",
						Double.class,
						crit,
						false,
						false,
						null,
						null);
				} else if (
					crit.name.equals(PrivateAuctionQueryFilter.BID_TYPE)) {
					if (queryFilter.length() > 0) {
						queryFilter.append(" && ");
					}
					generateQuery(
						queryFilter,
						vars,
						params,
						paramValues,
						"bidFormat",
						Integer.class,
						crit,
						false,
						false,
						null,
						null);
				} else if (
					crit.name.equals(PrivateAuctionQueryFilter.VISIBILITY)) {
					if (queryFilter.length() > 0) {
						queryFilter.append(" && ");
					}
					generateQuery(
						queryFilter,
						vars,
						params,
						paramValues,
						"visibilityType",
						Integer.class,
						crit,
						false,
						false,
						null,
						null);
				} else if (
					crit.name.equals(PrivateAuctionQueryFilter.WINNER_DETERM_TYPE)) {
					if (queryFilter.length() > 0) {
						queryFilter.append(" && ");
					}
					generateQuery(
						queryFilter,
						vars,
						params,
						paramValues,						
						"winnerDeterminationType",
						Integer.class,
						crit,
						false,
						false,
						null,
						null);
				}
				//				} else if (
				//					crit.name.equals(
				//						PrivateAuctionQueryFilter.CHECKOUT_STATUS)) {
				//					if (queryFilter.length() > 0) {
				//						queryFilter.append(" && ");
				//					}
				//					generateQuery(
				//						queryFilter,
				//						vars,
				//						"transactions",
				//						null,
				//						crit,
				//						true,
				//						"BankTransactionDAO",
				//						"transactionStatus");
				//
				//				} 
				else if (
					crit.name.equals(
						PrivateAuctionQueryFilter.PUBLISH_ERROR)) {
					if (sellerSearch) { //Seller specific search criteria
						if (crit.rhs instanceof Boolean
							&& ((Boolean) crit.rhs).booleanValue()) {
							if (queryFilter.length() > 0) {
								queryFilter.append(" && ");
							}

							final String attrName = "execStatus";
							QueryFilter.EqualToCriteria crit1 =
								new QueryFilter.EqualToCriteria();
							crit1.name = attrName;
							crit1.rhs =
								new Integer(
									ExecutionStatusEnum
										.SYSTEM_ERROR
										.getValue());

							QueryFilter.EqualToCriteria crit2 =
								new QueryFilter.EqualToCriteria();
							crit2.name = attrName;
							crit2.rhs =
								new Integer(
									ExecutionStatusEnum.USER_ERROR.getValue());

							queryFilter.append(" ( ");
							generateQuery(
								queryFilter,
								vars,
								params,
								paramValues,								
								attrName,
								Integer.class,
								crit1,
								false,
								false,
								null,
								null);

							queryFilter.append(" || ");

							generateQuery(
								queryFilter,
								vars,
								params,
								paramValues,								
								attrName,
								Integer.class,
								crit2,
								false,
								false,
								null,
								null);
							queryFilter.append(" ) ");
						}
					}
				} else if (crit.name.equals(PrivateAuctionQueryFilter.TYPE)) {
					// special handling of name if web shop Id is present
					if (queryFilter.length() > 0) {
						queryFilter.append(" && ");
					}
					generateQuery(
						queryFilter,
						vars,
						params,
						paramValues,						
						"auctionType",
						Integer.class,
						crit,
						false,
						false,
						null,
						null);
				}
			}
			StringBuffer orderByStr = new StringBuffer();
			if (criteriaNames != null) {
				for (int i = 0; i < criteriaNames.length; i++) {
					if (PrivateAuctionQueryFilter
						.STATUS
						.equals(criteriaNames[i])) {
						if (orderByStr.toString().indexOf("status") < 0) {
							if (orderByStr.length() > 0)
								orderByStr.append(" , ");
							orderByStr.append(
								"status "
									+ (filter.isAscending()
										? "ascending"
										: "descending"));
						}
							
					} else if (
						PrivateAuctionQueryFilter.NAME.equals(
							criteriaNames[i])) {
						if (orderByStr.toString().indexOf("auctionName") < 0) {
							if (orderByStr.length() > 0)
								orderByStr.append(" , ");
							orderByStr.append(
								"auctionName "
									+ (filter.isAscending()
										? "ascending"
										: "descending"));
						}
					} else if (
						PrivateAuctionQueryFilter.START_DATE.equals(
							criteriaNames[i])) {
						if (orderByStr.toString().indexOf("startDate") < 0) {
							if (orderByStr.length() > 0)
								orderByStr.append(" , ");
							orderByStr.append(
								"startDate "
									+ (filter.isAscending()
										? "ascending"
										: "descending"));
						}
							
					} else if (
						PrivateAuctionQueryFilter.PUBLISH_DATE.equals(
							criteriaNames[i])) {
						if (orderByStr.toString().indexOf("publishDate") < 0)  {
							if (orderByStr.length() > 0)
								orderByStr.append(" , ");
							orderByStr.append(
								"publishDate "
									+ (filter.isAscending()
										? "ascending"
										: "descending"));
						}
					} else if (
						PrivateAuctionQueryFilter.END_DATE.equals(
							criteriaNames[i])) {
						if (orderByStr.toString().indexOf("endDate") < 0)  {
							if (orderByStr.length() > 0)
								orderByStr.append(" , ");
							orderByStr.append(
								"endDate "
									+ (filter.isAscending()
										? "ascending"
										: "descending"));
						}
					} else if (
						PrivateAuctionQueryFilter.CREATE_DATE.equals(
							criteriaNames[i])) {
						if (orderByStr.toString().indexOf("createDate") < 0)  {
							if (orderByStr.length() > 0)
								orderByStr.append(" , ");
							orderByStr.append(
								"createdDate "
									+ (filter.isAscending()
										? "ascending"
										: "descending"));
						}
					}
				}
			}
			if (logger.beInfo()) {
				String msg =
					"In SearchManager SearchQuery -->Variables are "
						+ vars.toString();
				logger.infoT(tracer, msg);
				msg =
					"In SearchManager SearchQuery --> QueryFilter is "
						+ queryFilter.toString();
				logger.infoT(tracer, msg);
				msg = 
					"In SearchManager SearchQuery --> OrderByList is "
						+orderByStr.toString();
				logger.infoT(tracer, msg);
			}
			if (vars.length()>0)
				query.declareVariables(vars.toString());
			if (params.length()>0)
				query.declareParameters(params.toString());
			query.setFilter(queryFilter.toString());
			if (orderByStr.length() > 0)
				query.setOrdering(orderByStr.toString());
			Collection coll = null;
			if (paramValues.size()>0) {
				coll = (Collection) query.executeWithMap(paramValues);
			}
			else 
				coll = (Collection) query.execute();

			// TODO temp instead fire a SQL Query to get the approximate size
			int size = 0;
			Iterator collIt = coll.iterator();
			while (collIt.hasNext()) {
				collIt.next();
				size++;
			}
			result =
				new PrivateAuctionJDOQueryResult(
					pm,
					query,
					coll,
					size,
					provider.getRecordingCallManagerFactory());
			TraceHelper.exiting(tracer, METHOD);
			return result;
		} catch (JDOException jdoex) {
			error = true;
			logger.errorT(tracer, "JDOException in formulating the query");
			ErrorMessageBundle bundle = ErrorMessageBundle.getInstance();
			I18nErrorMessage msg =
				bundle.newI18nErrorMessage(SystemException.JDO_ERROR);
			// This should be fine for now?
			SystemException ex = new SystemException(msg, jdoex);
			super.throwing(METHOD, ex);
			throw ex;
		} catch (Exception ex) {
			error = true;
			ErrorMessageBundle bundle = ErrorMessageBundle.getInstance();
			I18nErrorMessage msg =
				bundle.newI18nErrorMessage(SystemException.INTERNAL_ERROR);
			SystemException sex = new SystemException(msg, ex);
			super.throwing(METHOD, sex);
			throw sex;
		} finally {
			// In case of error PM must be closed else it will lead to Memory leak
			if (error && pm != null && !pm.isClosed())
				pm.close();
		}
	}
	/* (non-Javadoc)
	 * @see com.sap.isa.auction.businessobject.SearchManager#isAuctionExistForProduct(java.lang.String, java.lang.String, java.util.Collection)
	 */
	public boolean isAuctionExistForProduct(
		String bpId,
		String productGuid,
		Collection tgs) {

		final String METHOD = "isAuctionExistForProduct";
		TraceHelper.entering(tracer, METHOD);
		ServiceLocator provider = (ServiceLocator) getServiceLocatorBase();
		PersistenceManager pm = null;
		boolean error = false;
		boolean exist = false;
		try {
			PersistenceManagerFactory pmf =
				provider.getPersistenceManagerFactory();
			pm = pmf.getPersistenceManager();
			Query query = pm.newQuery(PrivateAuctionDAO.class);
			if (logger.beInfo()){
				String msg = "step1: check all public auctions for product->"+productGuid;
				logger.infoT(tracer, msg);
			}
			StringBuffer queryFilter = new StringBuffer();
			StringBuffer vars = new StringBuffer();
			StringBuffer params = new StringBuffer();
			Map paramValues = new HashMap();
			addClientSysIdAndShopIdFilter(queryFilter);
			
			Criteria crit = new QueryFilter.EqualToCriteria();
			crit.name = PrivateAuctionQueryFilter.PRODUCT_GUID;
			crit.rhs = productGuid;
			queryFilter.append(" && ");
			generateQuery(
				queryFilter,
				vars,
				params,
				paramValues,
				"auctionItems",
				String.class,
				crit,
				false,
				true,
				"PrivateAuctionItemDAO",
				"productId");

			queryFilter.append(" && ");
			crit = new QueryFilter.GreaterThanEqualToCriteria(); //add auction not ended
			crit.name = PrivateAuctionQueryFilter.END_DATE;
			crit.rhs = new Date();
			generateQuery(
				queryFilter,
				vars,
				params,
				paramValues,				
				"endDate",
				Date.class,
				crit,
				false,
				false,
				null,
				null);
			
			queryFilter.append(" && ");
			crit = new QueryFilter.EqualToCriteria(); //add status
			crit.name = PrivateAuctionQueryFilter.STATUS;
			crit.rhs = new Integer(AuctionStatusEnum.PUBLISHED.getValue());
			generateQuery(
				queryFilter,
				vars,
				params,
				paramValues,
				"status",
				Integer.class,
				crit,
				false,
				false,
				null,
				null);
				
			StringBuffer queryFilter1 = new StringBuffer(queryFilter.toString());
			StringBuffer vars1 = new StringBuffer(vars.toString());
			StringBuffer params1 = new StringBuffer(params.toString());
			Map paramValues1 = new HashMap(paramValues);
			queryFilter1.append(" && ");
			crit = new QueryFilter.EqualToCriteria();
			crit.name = PrivateAuctionQueryFilter.VISIBILITY;
			crit.rhs = new Integer(AuctionVisibilityEnum.INT_PUBLIC);
			generateQuery(
				queryFilter1,
				vars1,
				params1,
				paramValues1,
				"visibilityType",
				Integer.class,
				crit,
				false,
				false,
				null,
				null);
			if (logger.beInfo()) {
				String msg =
					"In SearchManager SearchQuery --> QueryFilter is "
						+ queryFilter.toString();
				logger.infoT(tracer, msg);
			}
			if (vars1.length() > 0) {
				query.declareVariables(vars1.toString());
			}
			if (params1.length()>0)
			query.declareParameters(params1.toString());
			if (logger.beInfo()) {
				String msg =
					"In SearchManager SearchQuery --> QueryFilter1 is "
						+ queryFilter1.toString();
				logger.infoT(tracer, msg);
			}			
			query.setFilter(queryFilter1.toString());
			query.compile();
			Collection coll = null;
			if (paramValues1.size()>0)
				coll = (Collection) query.executeWithMap(paramValues1);
			else
				coll = (Collection) query.execute();
			Iterator collIt = coll.iterator();
			if (collIt.hasNext()) {
				exist = true;
				if (logger.beInfo()){
					String msg = "Found auctions for the product, exit";
					logger.infoT(tracer, msg);
				}
			}
			query.closeAll();
			if (!exist) {				
				if (logger.beInfo()){
					String msg = "step2: check all procteced auctions for the product visible for the bidder->"+bpId;
					logger.infoT(tracer, msg);
				}
				query = pm.newQuery(PrivateAuctionDAO.class);
				queryFilter.append(" && ");
				crit = new QueryFilter.EqualToCriteria();
				crit.name = PrivateAuctionQueryFilter.VISIBILITY;
				crit.rhs = new Integer(AuctionVisibilityEnum.INT_PROTECTED);
				generateQuery(
					queryFilter,
					vars,
					params,
					paramValues,
					"visibilityType",
					Integer.class,
					crit,
					false,
					false,
					null,
					null);
				ArrayList bps = new ArrayList();
				bps.add(bpId);
				addBuyerSpecificFilterCriteria(queryFilter, bps, tgs, vars);
				query.declareVariables(vars.toString());
				if (params.length()>0)
				query.declareParameters(params.toString());
				query.setFilter(queryFilter.toString());
				query.compile();
				if (logger.beInfo()) {
					String msg =
						"In SearchManager SearchQuery --> QueryFilter is "
							+ queryFilter.toString();
					logger.infoT(tracer, msg);
				}
				if (paramValues.size()>0)
					coll = (Collection) query.executeWithMap(paramValues1);
				else
					coll = (Collection) query.execute();
				collIt = coll.iterator();
				if (collIt.hasNext()) {
					exist = true;
				}
				query.closeAll();
			}
			TraceHelper.exiting(tracer, METHOD);
			return exist;
		} catch (JDOException jdoex) {
			error = true;
			logger.errorT(tracer, "JDOException in formulating the query");
			ErrorMessageBundle bundle = ErrorMessageBundle.getInstance();
			I18nErrorMessage msg =
				bundle.newI18nErrorMessage(SystemException.JDO_ERROR);
			// This should be fine for now?
			SystemException ex = new SystemException(msg, jdoex);
			super.throwing(METHOD, ex);
			throw ex;
		} catch (Exception ex) {
			error = true;
			ErrorMessageBundle bundle = ErrorMessageBundle.getInstance();
			I18nErrorMessage msg =
				bundle.newI18nErrorMessage(SystemException.INTERNAL_ERROR);
			SystemException sex = new SystemException(msg, ex);
			super.throwing(METHOD, sex);
			throw sex;
		} finally {
			// In case of error PM must be closed else it will lead to Memory leak
			if (pm != null && !pm.isClosed())
				pm.close();
		}

	}
	
	/* (non-Javadoc)
	 * @see com.sap.isa.auction.businessobject.SearchManager#searchAuctionsByProduct(java.lang.String, java.lang.String, java.util.Collection)
	 */
	public QueryResult searchAuctionsByProduct(
		String bpId,
		String productGuid,
		Collection tgs) {
			
		final String METHOD = "searchAuctionsByProduct";
		TraceHelper.entering(tracer, METHOD);
		ServiceLocator provider = (ServiceLocator) getServiceLocatorBase();
		PersistenceManager pm = null;
		boolean error = false;
		QueryResult result = null;
		try {
			PersistenceManagerFactory pmf =
				provider.getPersistenceManagerFactory();
			pm = pmf.getPersistenceManager();
			Query query1 = pm.newQuery(PrivateAuctionDAO.class);
			if (logger.beInfo()){
				String msg = "step1: check all public auctions for product->"+productGuid;
				logger.infoT(tracer, msg);
			}
			StringBuffer queryFilter = new StringBuffer();
			StringBuffer vars = new StringBuffer();
			StringBuffer params = new StringBuffer();
			Map paramValues = new HashMap();
			addClientSysIdAndShopIdFilter(queryFilter);
		
			Criteria crit = new QueryFilter.EqualToCriteria();
			crit.name = PrivateAuctionQueryFilter.PRODUCT_GUID;
			crit.rhs = productGuid;
			queryFilter.append(" && ");
			generateQuery(
				queryFilter,
				vars,
				params,
				paramValues,
				"auctionItems",
				String.class,
				crit,
				false,
				true,
				"PrivateAuctionItemDAO",
				"productId");
				
			queryFilter.append(" && ");
			crit = new QueryFilter.GreaterThanEqualToCriteria(); //add auction not ended
			crit.name = PrivateAuctionQueryFilter.END_DATE;
			crit.rhs = new Date();
			generateQuery(
				queryFilter,
				vars,
				params,
				paramValues,
				"endDate",
				Date.class,
				crit,
				false,
				false,
				null,
				null);
				
			queryFilter.append(" && ");
			crit = new QueryFilter.EqualToCriteria(); //add status
			crit.name = PrivateAuctionQueryFilter.STATUS;
			crit.rhs = new Integer(AuctionStatusEnum.PUBLISHED.getValue());
			generateQuery(
				queryFilter,
				vars,
				params,
				paramValues,
				"status",
				Integer.class,
				crit,
				false,
				false,
				null,
				null);
			
			StringBuffer queryFilter1 = new StringBuffer(queryFilter.toString());
			StringBuffer vars1 = new StringBuffer(vars.toString());
			StringBuffer params1 = new StringBuffer(params.toString());
			Map paramValues1 = new HashMap(paramValues);
			queryFilter1.append(" && ");
			crit = new QueryFilter.EqualToCriteria();
			crit.name = PrivateAuctionQueryFilter.VISIBILITY;
			crit.rhs = new Integer(AuctionVisibilityEnum.INT_PUBLIC);
			generateQuery(
				queryFilter1,
				vars1,
				params1,
				paramValues1,
				"visibilityType",
				Integer.class,
				crit,
				false,
				false,
				null,
				null);
			if (logger.beInfo()) {
				String msg =
					"In SearchManager SearchQuery --> QueryFilter is "
						+ queryFilter.toString();
				logger.infoT(tracer, msg);
			}
			query1.declareVariables(vars1.toString());
			if (params1.length()>0)
			query1.declareParameters(params1.toString());
			query1.setFilter(queryFilter1.toString());
			query1.setOrdering(BUYER_ORDERBY+ " descending");
			query1.compile();
			Collection coll = null;
			if (paramValues1.size()>0)
				coll = (Collection) query1.executeWithMap(paramValues1);
			else
				coll = (Collection) query1.execute();
			// TODO temp instead fire a SQL Query to get the approximate size
			Iterator collIt = coll.iterator();
			int size1 = 0;
			while (collIt.hasNext()) {
				collIt.next();
				size1++;
			}
			if (logger.beInfo()){
				String msg = "step2: check all procteced auctions for the product visible for the bidder->"+bpId;
				logger.infoT(tracer, msg);
			}
			Query query2 = pm.newQuery(PrivateAuctionDAO.class);
			queryFilter.append(" && ");
			crit = new QueryFilter.EqualToCriteria();
			crit.name = PrivateAuctionQueryFilter.VISIBILITY;
			crit.rhs = new Integer(AuctionVisibilityEnum.INT_PROTECTED);
			generateQuery(
				queryFilter,
				vars,
				params,
				paramValues,
				"visibilityType",
				Integer.class,
				crit,
				false,
				false,
				null,
				null);
			ArrayList bps = new ArrayList();
			bps.add(bpId);
			addBuyerSpecificFilterCriteria(queryFilter, bps, tgs, vars);
			query2.declareVariables(vars.toString());
			if (params.length()>0)
			query2.declareParameters(params.toString());
			query2.setFilter(queryFilter.toString());
			query2.setOrdering(BUYER_ORDERBY+ " descending");
			query2.compile();
			if (logger.beInfo()) {
				String msg =
					"In SearchManager SearchQuery --> QueryFilter is "
						+ queryFilter.toString();
				logger.infoT(tracer, msg);
			}
			Collection coll2 = null;
			if (paramValues.size()>0)
				coll2 = (Collection) query2.executeWithMap(paramValues);
			else
				coll2 = (Collection) query2.execute();
			
			collIt = coll2.iterator();
			int totalSize = 0;
			while (collIt.hasNext()) {
				collIt.next();
				totalSize++;
			}
			totalSize +=size1;
			result = new PrivateAuctionUnionJDOQueryResult(
									pm,
									query1,
									query2,
									coll,
									coll2,
									totalSize,
									size1,
									provider.getRecordingCallManagerFactory());
			TraceHelper.exiting(tracer, METHOD);
			return result;
		} catch (JDOException jdoex) {
			error = true;
			logger.errorT(tracer, "JDOException in formulating the query");
			ErrorMessageBundle bundle = ErrorMessageBundle.getInstance();
			I18nErrorMessage msg =
				bundle.newI18nErrorMessage(SystemException.JDO_ERROR);
			// This should be fine for now?
			SystemException ex = new SystemException(msg, jdoex);
			super.throwing(METHOD, ex);
			throw ex;
		} catch (Exception ex) {
			error = true;
			ErrorMessageBundle bundle = ErrorMessageBundle.getInstance();
			I18nErrorMessage msg =
				bundle.newI18nErrorMessage(SystemException.INTERNAL_ERROR);
			SystemException sex = new SystemException(msg, ex);
			super.throwing(METHOD, sex);
			throw sex;
		} finally {
			// In case of error PM must be closed else it will lead to Memory leak
			if (error && pm != null && !pm.isClosed())
				pm.close();
		}
	}
	/* (non-Javadoc)
	 * @see com.sap.isa.auction.businessobject.SearchManager#searchAuctionsParticipatedByBidder(java.lang.String, java.sql.Timestamp, com.sap.isa.auction.bean.AuctionStatusEnum)
	 */
	public QueryResult searchAuctionsParticipatedByBidder(
		String bpId,
		String soldTo,
		Timestamp since,
		AuctionStatusEnum status) {

		final String METHOD = "searchAuctionsParticipatedByBidder";
		TraceHelper.entering(tracer, METHOD);
		checkNull(bpId);
		ServiceLocator provider = (ServiceLocator) getServiceLocatorBase();
		QueryResult result = null;
		PersistenceManager pm = null;
		boolean error = false;
		try {
			PersistenceManagerFactory pmf =
				provider.getPersistenceManagerFactory();
			pm = pmf.getPersistenceManager();
			Query query = pm.newQuery(PrivateAuctionDAO.class);
			StringBuffer queryFilter = new StringBuffer();

			addClientSysIdAndShopIdFilter(queryFilter);
			if (status != null){
				queryFilter.append(" && (status == " + status.getValue());
				if (status.getValue() == AuctionStatusEnum.PUBLISHED.getValue()) {
					queryFilter.append(") && (endDate >= ");
					Date today = new Date();
					queryFilter.append(today.getTime() + "L");
				}					
				if (status.getValue() == AuctionStatusEnum.FINALIZED.getValue()) 
					queryFilter.append(" || status =="+AuctionStatusEnum.FINISHED.getValue());
				queryFilter.append(")");		
								
			}
				
			String vars = "PrivateAuctionBidDAO bid;";
			query.declareVariables(vars);
			//queryFilter.append(" && (!bids.isEmpty()");
			//queryFilter.append(" && bids.contains(bid)");
			queryFilter.append(" && bids.contains(bid)");
			queryFilter.append(" && (bid.bpId == \"");
			queryFilter.append(bpId);
			queryFilter.append("\"");
			queryFilter.append(" && bid.bidderSoldToParty== \"");
			queryFilter.append(soldTo);
			queryFilter.append("\"");
			if (since != null)
				queryFilter.append(
					" && bid.bidTime >= " + since.getTime() + "L");
			queryFilter.append(")");
			if (logger.beInfo()) {
				String msg =
					"In SearchManager SearchQuery --> QueryFilter is "
						+ queryFilter.toString();
				logger.infoT(tracer, msg);
			}
			query.setFilter(queryFilter.toString());
			query.setOrdering(BUYER_ORDERBY+ " descending");
			query.compile();
			Collection coll = (Collection) query.execute();
			// TODO temp instead fire a SQL Query to get the approximate size
			int size = 0;
			Iterator collIt = coll.iterator();
			Collection filteredResult = new ArrayList();
			Collection objectIds = new ArrayList();
			while (collIt.hasNext()) {
				PrivateAuctionDAO auctionDAO = (PrivateAuctionDAO)collIt.next();
				String auctionId = auctionDAO.getAuctionId();
				if (objectIds.contains(auctionId))
					continue;
				objectIds.add(auctionId);
				PrivateAuction auction = new PrivateAuction();
				DALBeanSynchronizer.synchAuctionBeanFromDAO(auctionDAO,auction);
				filteredResult.add(auction);
				size++;
			}
			result = new GenericQueryResult(filteredResult, size);
			query.closeAll();
//			result =
//				new PrivateAuctionJDOQueryResult(
//					pm,
//					query,
//					coll,
//					size,
//					provider.getRecordingCallManagerFactory());
			TraceHelper.exiting(tracer, METHOD);
			return result;

		} catch (JDOException jdoex) {
			error = true;
			logger.errorT(tracer, "JDOException in formulating the query");
			ErrorMessageBundle bundle = ErrorMessageBundle.getInstance();
			I18nErrorMessage msg =
				bundle.newI18nErrorMessage(SystemException.JDO_ERROR);
			// This should be fine for now?
			SystemException ex = new SystemException(msg, jdoex);
			super.throwing(METHOD, ex);
			throw ex;
		} catch (Exception ex) {
			error = true;
			ErrorMessageBundle bundle = ErrorMessageBundle.getInstance();
			I18nErrorMessage msg =
				bundle.newI18nErrorMessage(SystemException.INTERNAL_ERROR);
			SystemException sex = new SystemException(msg, ex);
			super.throwing(METHOD, sex);
			throw sex;
		} finally {
			// In case of error PM must be closed else it will lead to Memory leak
			if (pm != null && !pm.isClosed())
				pm.close();
		}
	}

	/* (non-Javadoc)
	 * @see com.sap.isa.auction.businessobject.SearchManager#searchAuctionsWonByBidder(java.lang.String, com.sap.isa.auction.bean.OrderStatusEnum)
	 */
	public QueryResult searchAuctionsWonByBidder(
		String bpId,
		String soldTo,
		Timestamp since,
		OrderStatusEnum status) {
		final String METHOD = "searchAuctionsWonByBidder";
		checkNull(bpId);
		ServiceLocator provider = (ServiceLocator) getServiceLocatorBase();
		QueryResult result = null;
		PersistenceManager pm = null;
		boolean error = false;
		try {
			PersistenceManagerFactory pmf =
				provider.getPersistenceManagerFactory();
			pm = pmf.getPersistenceManager();
			Query query = pm.newQuery(PrivateAuctionDAO.class);
			StringBuffer queryFilter = new StringBuffer();

			addClientSysIdAndShopIdFilter(queryFilter);
//			if (status != null) {
//				queryFilter.append(" && ");
//				if (status == OrderStatusEnum.CHECKED) {
//					queryFilter.append("(status == "+AuctionStatusEnum.toInt(AuctionStatusEnum.FINISHED)+")");
//					// orderId != "" => Checked Auctions
//				}
//
//				if (status == OrderStatusEnum.UNCHECKED) {
//					queryFilter.append("(status == "+AuctionStatusEnum.toInt(AuctionStatusEnum.FINALIZED)+")");
//					// orderId = "" => Unchecked Auctions
//				}
//			}
			String vars = "PrivateAuctionWinnerDAO winner;";
			query.declareVariables(vars);
			//queryFilter.append(" && (!winners.isEmpty()");
			queryFilter.append(" && winners.contains(winner)");
			queryFilter.append(" && (winner.bpId == \"");
			queryFilter.append(bpId);
			queryFilter.append("\"");
			queryFilter.append(" && winner.soldToParty == \"");
			queryFilter.append(soldTo);
			queryFilter.append("\"");
			if (status != null) {
				queryFilter.append(" && ");
				if (status == OrderStatusEnum.CHECKED) {
					queryFilter.append("winner.checkedOut == 1"); //1 means checked out auctions
				}

				if (status == OrderStatusEnum.UNCHECKED) {
					queryFilter.append("winner.checkedOut == -1");//-1 means unchecked out auctions
				}
			}
			//if (since != null)
				// TODO add since filter
				//queryFilter.append(" && winner.");
			queryFilter.append(")");
			
			if (logger.beInfo()) {
				String msg =
					"In SearchManager SearchQuery --> QueryFilter is "
						+ queryFilter.toString();
				logger.infoT(tracer, msg);
			}
			query.setFilter(queryFilter.toString());
			query.setOrdering(BUYER_ORDERBY+ " descending");
			query.compile();
			Collection coll = (Collection) query.execute();
			// TODO temp instead fire a SQL Query to get the approximate size
			int size = 0;
			Iterator collIt = coll.iterator();
			while (collIt.hasNext()) {
				collIt.next();
				size++;
			}
			result =
				new PrivateAuctionJDOQueryResult(
					pm,
					query,
					coll,
					size,
					provider.getRecordingCallManagerFactory());
			TraceHelper.exiting(tracer, METHOD);
			return result;

		} catch (JDOException jdoex) {
			error = true;
			logger.errorT(tracer, "JDOException in formulating the query");
			ErrorMessageBundle bundle = ErrorMessageBundle.getInstance();
			I18nErrorMessage msg =
				bundle.newI18nErrorMessage(SystemException.JDO_ERROR);
			// This should be fine for now?
			SystemException ex = new SystemException(msg, jdoex);
			super.throwing(METHOD, ex);
			throw ex;
		} catch (Exception ex) {
			error = true;
			ErrorMessageBundle bundle = ErrorMessageBundle.getInstance();
			I18nErrorMessage msg =
				bundle.newI18nErrorMessage(SystemException.INTERNAL_ERROR);
			SystemException sex = new SystemException(msg, ex);
			super.throwing(METHOD, sex);
			throw sex;
		} finally {
			// In case of error PM must be closed else it will lead to Memory leak
			if (error && pm != null && !pm.isClosed())
				pm.close();
		}
	}
	
	
	/**
	 * @param queryFilter
	 */
	private void addBuyerSpecificFilterCriteria(
		StringBuffer queryFilter,
		Collection bps,
		Collection tgs,
		StringBuffer vars) {
		checkBidderSearchCriteria(bps);

		if (queryFilter.length() > 0) { 
			queryFilter.append(" && ");
		}
		if (tgs != null && tgs.size() > 0)
			queryFilter.append("(");
		vars.append("TargetGrpDAO bp;");
		queryFilter.append("(targetGroups.contains(bp)");	
		//queryFilter.append("(!targetGroups.isEmpty()");
		//queryFilter.append(" && targetGroups.contains(bp)");
		queryFilter.append(
			" && (bp.targetId ==\"" + (String) bps.iterator().next() + "\"");
		queryFilter.append(" && bp.targetGrp == -1))"); //false is -1 in jdo

		if (tgs != null && tgs.size() > 0) {
			Iterator it = tgs.iterator();
			vars.append("TargetGrpDAO tg;");
			queryFilter.append(" || ");
			//queryFilter.append("(!targetGroups.isEmpty()");
			//queryFilter.append(" && targetGroups.contains(tg)");
			queryFilter.append(" (targetGroups.contains(tg)");
			queryFilter.append(" && ((");
			while (it.hasNext()) {
				queryFilter.append(
					"tg.targetId ==\"" + ((TargetGroup) it.next()).getGuid() + "\"");
				if (it.hasNext())
					queryFilter.append(" || ");
			}
			queryFilter.append(") && tg.targetGrp == 1))"); //true is 1 in jdo
		}
		if (tgs != null && tgs.size() > 0)
		queryFilter.append(")");
	}

	private void addSellerSpecificFilterCriteria(
		StringBuffer queryFilter,
		Collection bps,
		Collection tgs,
		StringBuffer vars) {
		String sellerId = getInvocationContext().getUser().getUserId().toUpperCase();
		queryFilter.append("(createdBy == \"" + sellerId + "\")");
		if (bps != null && bps.size() > 0) {
			Iterator it = bps.iterator();
			int i = 0;
			while (it.hasNext()) {
				String variable = "bp" + i++;
				vars.append("TargetGrpDAO");
				vars.append(variable);
				vars.append(";");
				queryFilter.append(" && (!targetGroups.isEmpty()");
				queryFilter.append(" && targetGroups.contains(");
				queryFilter.append(variable);
				queryFilter.append(")");
				queryFilter.append(" && ");
				queryFilter.append(variable);
				queryFilter.append(
					".targetId ==\"" + (String) it.next() + "\"");
				queryFilter.append(" && bp.targetGrp == -1)");
				//false is -1 in jdo			
			}

		}
		if (tgs != null && tgs.size() > 0) {
			Iterator it = bps.iterator();
			int i = 0;
			while (it.hasNext()) {
				String variable = "tg" + i++;
				vars.append("TargetGrpDAO");
				vars.append(variable);
				vars.append(";");
				queryFilter.append(" && (!targetGroups.isEmpty()");
				queryFilter.append(" && targetGroups.contains(");
				queryFilter.append(variable);
				queryFilter.append(")");
				queryFilter.append(" && ");
				queryFilter.append(variable);
				queryFilter.append(
					".targetId ==\"" + (String) it.next() + "\"");
				queryFilter.append(" && bp.targetGrp == 1)");
				//true is 1 in jdo			
			}
		}
	}

	/**
	 * This method will generate the jdoql string
	 */
	private void generateQuery(
		StringBuffer filter,
		StringBuffer variables,
		StringBuffer params,
		Map paramValues,
		String attributeName,
		Class attributeClassType,
		Criteria criteria,
		boolean unicodeSupport,
		boolean collectionType,
		String collectionTypeName,
		String collectionAttributeName) {

		final String METHOD = "generateQuery";
		TraceHelper.entering(tracer, METHOD);
		String attrValue = criteria.rhs.toString();
		//We are dealing with the dates here.From the database point of view, these are just the
		// long values
		if (attributeClassType
			== Date.class | attributeClassType
			== Timestamp.class)
			if (criteria.rhs instanceof Date) {
				attrValue =
					Long.toString((((Date) criteria.rhs)).getTime()) + "L";
			} else if (criteria.rhs instanceof Long) {
				attrValue = attrValue + "L";
			}

		// handles Byte, Short, Integer, Long, Double, Float data types
		if (Number.class.isAssignableFrom(attributeClassType)) {
			if (criteria.rhs instanceof Long)
				attrValue = ((Number) criteria.rhs).longValue() + "L";
		}
		String paramName = null;
		if (attributeClassType == String.class && unicodeSupport) {
			paramName = "tmp"+attributeName;
			if (params.length()>0)
				params.append(", ");
			params.append("String "+paramName);	
		}
		if (attributeClassType == String.class &&
			!(criteria instanceof QueryFilter.LikeCriteria)) {
			if (unicodeSupport) {
				paramValues.put(paramName,attrValue);
				attrValue = paramName;
			}
			else
				attrValue = "\"" + attrValue + "\"";
		}
			
		/**
		 * Special treatment for variables of type boolean because of the jdo limitation
		 */
		if (attributeClassType == Boolean.class) {
			if (attrValue.equals("true")) {
				attrValue = "1";
			} else if (attrValue.equals("true")) {
				attrValue = "-1";
			}
		}

		if (collectionType) {
			String variableName = "tmp" + collectionTypeName;
			variables.append(collectionTypeName + " " + variableName + " ;");
			//filter.append("(!" + attributeName + ".isEmpty() && ");
			filter.append("( ");
			filter.append(
				attributeName + ".contains(" + variableName + ") && ");
			if (criteria instanceof QueryFilter.EqualToCriteria)
				filter.append(
					variableName
						+ "."
						+ collectionAttributeName
						+ " == "
						+ attrValue
						+ ")");
			else if (criteria instanceof QueryFilter.GreaterThanCriteria)
				filter.append(
					variableName
						+ "."
						+ collectionAttributeName
						+ " > "
						+ attrValue
						+ " )");
			else if (criteria instanceof QueryFilter.LessThanCriteria)
				filter.append(
					variableName
						+ "."
						+ collectionAttributeName
						+ " < "
						+ attrValue
						+ ")");
			else if (
				criteria instanceof QueryFilter.GreaterThanEqualToCriteria)
				filter.append(
					variableName
						+ "."
						+ collectionAttributeName
						+ " >= "
						+ attrValue
						+ " )");
			else if (criteria instanceof QueryFilter.LessThanEqualToCriteria)
				filter.append(
					variableName
						+ "."
						+ collectionAttributeName
						+ " <= "
						+ attrValue
						+ ")");
			else if (criteria instanceof QueryFilter.LikeCriteria) {
				
				if (attrValue.indexOf('*') == 0) {
					attrValue = attrValue.substring(1);
					if (unicodeSupport) {
						paramValues.put(paramName,attrValue);
						filter.append(
							variableName
							+ "."
							+ collectionAttributeName
							+ ".startsWith("
							+paramName
							+ "))"
						);
					}
					else
						filter.append(
							variableName
								+ "."
								+ collectionAttributeName
								+ ".startsWith(\""
								+ attrValue
								+ "\") "
								+ ")");
				}
					
				else if (attrValue.indexOf('*') > 0) {
				
					attrValue = attrValue.substring(0, attrValue.indexOf('*'));
					if (unicodeSupport) {
						paramValues.put(paramName,attrValue);
						filter.append(
							variableName
								+ "."
								+ collectionAttributeName
								+ ".startsWith("
								+ paramName
								+ ")) "
						);
					}
					else 
						filter.append(
							variableName
								+ "."
								+ collectionAttributeName
								+ ".startsWith(\""
								+ attrValue
								+ "\") "
								+ ")");
				}
				else {
					if (unicodeSupport) {
						paramValues.put(paramName,attrValue);
						filter.append(
							variableName
								+ "."
								+ collectionAttributeName
								+ ".startsWith("
								+ paramName
								+ "))"
						);
					}
					else 
						filter.append(
							variableName
								+ "."
								+ collectionAttributeName
								+ ".startsWith(\""
								+ attrValue
								+ "\") "
								+ ")");
				}
					
			}
		} else {
			if (criteria instanceof QueryFilter.EqualToCriteria)
				filter.append("( " + attributeName + " == " + attrValue + ")");
			else if (criteria instanceof QueryFilter.GreaterThanCriteria)
				filter.append("( " + attributeName + " > " + attrValue + ")");
			else if (criteria instanceof QueryFilter.LessThanCriteria)
				filter.append("( " + attributeName + " < " + attrValue + ")");
			else if (
				criteria instanceof QueryFilter.GreaterThanEqualToCriteria)
				filter.append("( " + attributeName + " >= " + attrValue + ")");
			else if (criteria instanceof QueryFilter.LessThanEqualToCriteria)
				filter.append("( " + attributeName + " <= " + attrValue + ")");
			else if (criteria instanceof QueryFilter.LikeCriteria) {
				if (attrValue.indexOf('*') == 0) {
					attrValue = attrValue.substring(1);
					if (unicodeSupport && paramName != null) {
						paramValues.put(paramName,attrValue);
						filter.append(
							"( "
							+ attributeName
							+ ".startsWith("
							+paramName
							+ "))"
						);
					}
					else
						filter.append(
							"( "
							+ attributeName
							+ ".endsWith(\""
							+ attrValue
							+ "\")) "
						);
				}
	
				else if (attrValue.indexOf('*') > 0) {

					attrValue = attrValue.substring(0, attrValue.indexOf('*'));
					if (unicodeSupport && paramName != null) {
						paramValues.put(paramName,attrValue);
						filter.append(
								"( "
								+ attributeName
								+ ".startsWith("
								+ paramName
								+ ")) "
						);
					}
					else 
						filter.append(
							"( "
							+ attributeName
							+ ".startsWith(\""
							+ attrValue
							+ "\"))"
						);
				}
				else {
					if (unicodeSupport && paramName != null) {
						paramValues.put(paramName,attrValue);
						filter.append(
								"( "
								+ attributeName
								+ ".startsWith("
								+ paramName
								+ "))"
						);
					}
					else 
						filter.append(
						"( "
							+ attributeName
							+ ".startsWith(\""
							+ attrValue
							+ "\") "
							+ ")");
				}
			}
		}
		TraceHelper.exiting(tracer, METHOD);
	}

	private void addClientSysIdAndShopIdFilter(StringBuffer queryFilter) {
		InvocationContext invContext = (InvocationContext) getInvocationContext();
		String shopId = invContext.getShopId();
        if (shopId != null && shopId.length() == 0) {            
            String msg = "In addClientsSysIdAndShopIdFilter shopId is empty. ";
                        logger.infoT(tracer, msg);
        }
        String clientId = ((AuctionBusinessObjectManager)bom).createSystemSetting().getClient();
		String systemId = ((AuctionBusinessObjectManager)bom).createSystemSetting().getSystemId();
		queryFilter.append("(client == \"" + clientId + "\")");
		queryFilter.append(" && (systemId == \"" + systemId + "\")");
		queryFilter.append(" && (webShopId == \"" + shopId + "\")");
	}

	/**
	 * @@todo:: check method level perms for user
	 */
	private void checkPermission(UserBase user, final String METHOD) {
		//
	}

	private void checkNull(Object obj) {
		if (obj == null) {
			throw new IllegalArgumentException("arg is null");
		}
	}

	private void checkBidderSearchCriteria(Collection bps) {
		if (bps == null || bps.isEmpty())
			throw new IllegalArgumentException("bidder bp is null");
		else if (bps.size() > 1)
			throw new IllegalArgumentException("contains more than one bp");

	}

}
