package com.sap.isa.auction.businessobject.b2x;

import com.sap.isa.auction.businessobject.*;
import com.sap.isa.persistence.pool.PooledObjectFactory;

/**
 * Title:        Internet Sales
 * Description:
 * Copyright:    Copyright (c) 2002
 * Company:      SAPMarkets
 * @author
 * @version 1.0
 */

public class AuctionManagerFactory extends PooledObjectFactory {

	public AuctionManagerFactory() {
		super(AuctionManager.class);
	}

	public Object create(Object params) {
		return new PrivateAuctionMgrImpl();
	}

	public Object initialize(Object obj) {
		if(obj instanceof PrivateAuctionMgrImpl) {
			((PrivateAuctionMgrImpl)obj).initialize();
		}
		return obj;
	}

	public Object unwrap(Object obj) {
		if(obj instanceof PrivateAuctionMgrImpl) {
			return obj;
		}
		else {
			return null;
		}
	}

	public void close(Object obj) {
		if(obj instanceof PrivateAuctionMgrImpl) {
			((PrivateAuctionMgrImpl)obj).close();
		}
	}

	public void destroy(Object obj) {
		// do nothing
	}
}
