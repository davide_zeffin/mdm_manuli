package com.sap.isa.auction.businessobject.b2x;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;
import java.util.StringTokenizer;

import com.sap.isa.auction.bean.Address;
import com.sap.isa.auction.bean.Auction;
import com.sap.isa.auction.bean.BeanSerializer;
import com.sap.isa.auction.bean.SalesArea;
import com.sap.isa.auction.bean.Winner;
import com.sap.isa.auction.bean.b2x.PrivateAuction;
import com.sap.isa.auction.bean.b2x.PrivateAuctionBid;
import com.sap.isa.auction.bean.b2x.PrivateAuctionItem;
import com.sap.isa.auction.bean.b2x.PrivateAuctionWinner;
import com.sap.isa.auction.bean.b2x.WinnerDeterminationTypeEnum;
import com.sap.isa.auction.businessobject.b2x.jdo.PrivateAuctionBidDAO;
import com.sap.isa.auction.businessobject.b2x.jdo.PrivateAuctionDAO;
import com.sap.isa.auction.businessobject.b2x.jdo.PrivateAuctionItemDAO;
import com.sap.isa.auction.businessobject.b2x.jdo.PrivateAuctionWinnerDAO;
import com.sap.isa.auction.businessobject.b2x.jdo.TargetGrpDAO;
import com.sap.isa.auction.util.DateTimeUtil;
import com.sap.isa.core.TechKey;
import com.sap.isa.persistence.helpers.OIDGenerator;

/**
 * This class will aid in the sychronizing the persistence related data access objects
 * and the UI related bean objects.
 *
 */
public class DALBeanSynchronizer {

	public static void synchAuctionBeanFromDAO(
		PrivateAuctionDAO auctiondao,
		Auction auction) {
		auction.setAuctionDescription(auctiondao.getDescription());
		auction.setAuctionId(auctiondao.getAuctionId());
		auction.setAuctionName(auctiondao.getAuctionName());
		auction.setAuctionTerms(auctiondao.getTerms());
		auction.setQuantity(auctiondao.getQuantity());
		
		/** @todo adjust these data related inconsistencies */
		auction.setCreateDate(auctiondao.getCreatedDate());
		auction.setCreatedBy(auctiondao.getCreatedBy());
		auction.setLastModifiedDate(auctiondao.getModifiedDate());
		auction.setStartDate(auctiondao.getStartDate());
		auction.setPublishDate(auctiondao.getPublishDate());
		/** Duration must be set after start data and before end date else it will clear the end date*/
		int[] duration =
			DateTimeUtil.fromTimeInMillis(auctiondao.getQuotDurationTime());
		auction.setQuotationDuration(duration[0], duration[1], duration[2]);
		duration =
			DateTimeUtil.fromTimeInMillis(auctiondao.getDurationInMillisecs());
		auction.setAuctionDuration(duration[0], duration[1], duration[2]);
		auction.setEndDate(auctiondao.getEndDate());
		auction.setCloseDate(auctiondao.getCloseDate());

		auction.setClient(auctiondao.getClient());
		auction.setSystemId(auctiondao.getSystemId());
		auction.setQuotationId(auctiondao.getQuotationId());
		auction.setReservePrice(auctiondao.getReservePrice());
		auction.setSellerId(auctiondao.getSellerId());
		auction.setStartPrice(auctiondao.getStartPrice());
		auction.setStatus(auctiondao.getStatus());
		auction.setReferencedAuctionId(auctiondao.getRefAucId());
		auction.setType(auctiondao.getAuctionType());
		SalesArea salesArea = new SalesArea();
		codeSalesArea(salesArea, auctiondao.getSalesArea(), false);
		auction.setSalesArea(salesArea);
		auction.setCountryCode(auctiondao.getCountry());
		auction.setCurrencyCode(auctiondao.getCurrencyId());

		if (auction instanceof PrivateAuction) {
			PrivateAuction privateAuction = (PrivateAuction) auction;
			privateAuction.setAuctionTitle(auctiondao.getTitle());
			privateAuction.setWinnerTypeEnum(WinnerDeterminationTypeEnum.toEnum(auctiondao.getWinnerDeterminationType()));
			privateAuction.setBuyItNowPrice(auctiondao.getBuyItNowPrice());
			privateAuction.setBidIncrement(auctiondao.getBidIncrement());
			privateAuction.setVisibilityType(auctiondao.getVisibilityType());
			privateAuction.setBidType(auctiondao.getBidType());
			privateAuction.setWebShopId(auctiondao.getShopId());
			privateAuction.setImageURL(auctiondao.getImageURL());
		}

		if (auctiondao.getAuctionItems() != null) {
			Iterator iter = auctiondao.getAuctionItems().iterator();
			while (iter.hasNext()) {
				PrivateAuctionItemDAO auctionitemdao = (PrivateAuctionItemDAO) iter.next();
				PrivateAuctionItem auctionItem = new PrivateAuctionItem();
				synchAuctionItemBeanFromDAO(auctionitemdao, auctionItem);
				auction.addItem(auctionItem);
			}
		}

		if (auctiondao.getBids() != null
			&& auctiondao.getBids().iterator().hasNext()) {
			Iterator iter = auctiondao.getBids().iterator();
			ArrayList list = new ArrayList();
			while (iter.hasNext()) {
				PrivateAuctionBidDAO biddao = (PrivateAuctionBidDAO) iter.next();
				PrivateAuctionBid bid = new PrivateAuctionBid();
				syncVBeanFromDAO(biddao, bid);
				list.add(bid);
			}
			auction.setBids(list);
		}
		if (auctiondao.getTargetGroups() != null
			&& auctiondao.getTargetGroups().iterator().hasNext()) {
			Iterator iter = auctiondao.getTargetGroups().iterator();
			ArrayList list = new ArrayList();
			while (iter.hasNext()) {
				TargetGrpDAO tgtDAO = (TargetGrpDAO) iter.next();
				if(tgtDAO.isTargetGrp()) {
					auction.addTargetGroup(tgtDAO.getTargetId());
				}
				else {
					auction.addBusinessPartner(tgtDAO.getTargetId());
				}
			}
		}		
	}

	public static String codeSalesArea(
		SalesArea salesArea,
		String salesAreaStr,
		boolean encode) {
		String delim = "$";
		if (encode) {
			salesAreaStr =
				(getValidStringToken(salesArea.getSalesOrganization()))
					+ delim
					+ (getValidStringToken(salesArea.getDistributionChannel()))
					+ delim
					+ (getValidStringToken(salesArea.getDivision()));
			return salesAreaStr;
		} else {
			if (salesAreaStr == null || salesAreaStr.length() == 0)
				return null;
			StringTokenizer strTok = new StringTokenizer(salesAreaStr, delim);
			int i = 0;
			while (strTok.hasMoreTokens()) {
				i++;
				String value = strTok.nextToken();
				if (value != null && "null".equals(value))
					value = null;
				switch (i) {
					case 1 :
						salesArea.setSalesOrganization(value);
						continue;
					case 2 :
						salesArea.setDistributionChannel(value);
						continue;
					case 3 :
						salesArea.setDivision(value);
						continue;
				}
			}
		}
		return null;

	}
	/**
	 * Returns null if the str is blank string
	 * @param str
	 */
	private static String getValidStringToken(String str) {
		if (str == null || str.trim().length() == 0)
			return null;
		return str;
	}
	static private String codeLocation(
		Address address,
		String location,
		boolean encode) {
		String delim = "$";
		if (encode) {
			location =
				(getValidStringToken(address.getStreet1()))
					+ delim
					+ (getValidStringToken(address.getStreet2()))
					+ delim
					+ (getValidStringToken(address.getCity()))
					+ delim
					+ (getValidStringToken(address.getStateOrProvince()))
					+ delim
					+ (getValidStringToken(address.getCountry()));

			return location;
		} else {
			if (location == null || location.length() == 0)
				return null;
			StringTokenizer strTok = new StringTokenizer(location, delim);
			int i = 0;
			while (strTok.hasMoreTokens()) {
				i++;
				String value = strTok.nextToken();
				if (value != null && "null".equals(value))
					value = null;
				switch (i) {
					case 1 :
						address.setStreet1(value);
						continue;
					case 2 :
						address.setStreet2(value);
						continue;
					case 3 :
						address.setCity(value);
						continue;
					case 4 :
						address.setStateOrProvince(value);
						continue;
					case 5 :
						address.setCountry(value);
						continue;

				}
			}
		}
		return null;
	}
	
	public static void synchAuctionItemBeanFromDAO(
		PrivateAuctionItemDAO auctionitemdao,
		PrivateAuctionItem auctionItem) {
		auctionItem.setProductCode(auctionitemdao.getProductCode());
		auctionItem.setUOM(auctionitemdao.getUom());
		auctionItem.setProductId(auctionitemdao.getProductId());
		auctionItem.setQuantity(auctionitemdao.getQuantity());
		auctionItem.setId(auctionitemdao.getId());
		auctionItem.setDescription(auctionitemdao.getDescription());
		auctionItem.setImage(auctionitemdao.getImage());
		
	}

	public static void synchAuctionDAOFromBean(
		Auction auction,
		PrivateAuctionDAO auctiondao,
		boolean itemsSynch) {
		auctiondao.setDescription(auction.getAuctionDescription());
		auctiondao.setQuantity(auction.getQuantity());
		auctiondao.setAuctionName(auction.getAuctionName());
		auctiondao.setTerms(auction.getAuctionTerms());
		auctiondao.setSellerId(auction.getSellerId());
		/** @todo adjust these data related inconsistencies */
		auctiondao.setCreatedDate(auction.getCreateDate());
		auctiondao.setCreatedBy(auction.getCreatedBy());
		auctiondao.setModifiedDate(auction.getLastModifiedDate());
		auctiondao.setStartDate(auction.getStartDate());
		auctiondao.setCloseDate(auction.getCloseDate());
		auctiondao.setRefAucId(auction.getReferencedAuctionId());
		int[] duration = auction.getAuctionDuration();
		auctiondao.setDurationInMillisecs(
			DateTimeUtil.toTimeInMillis(duration[0], duration[1], duration[2]));
		duration = auction.getQuotationDuration();
		auctiondao.setQuotDurationTime(
			DateTimeUtil.toTimeInMillis(duration[0], duration[1], duration[2]));
		auctiondao.setQuotationId(auction.getQuotationId());
		auctiondao.setReservePrice(auction.getReservePrice());
		auctiondao.setEndDate(auction.getEndDate());
		auctiondao.setStartPrice(auction.getStartPrice());
		auctiondao.setStatus(auction.getStatus());
		auctiondao.setAuctionType(auction.getType());
		auctiondao.setSalesArea(
			BeanSerializer.serializeSalesAreaAsString(
				(SalesArea) auction.getSalesArea()));
		auctiondao.setAuctionTemplate(auction.isTemplate());

		if (auction instanceof PrivateAuction) {
			PrivateAuction privateAuction = (PrivateAuction) auction;
			auctiondao.setPublishDate(privateAuction.getPublishDate());

			auctiondao.setExecutionRequestId(
				privateAuction.getExecutionRequestId());
			auctiondao.setExecutionStatus(privateAuction.getExecutionStatus());
			auctiondao.setWinnerDeterminationType(privateAuction.getWinnerTypeEnum().getValue());
			auctiondao.setTitle(privateAuction.getAuctionTitle());
			auctiondao.setCountry(privateAuction.getCountryCode());
			auctiondao.setCurrencyId(privateAuction.getCurrencyCode());
			auctiondao.setCountry(privateAuction.getCountryCode());
			auctiondao.setBidIncrement(privateAuction.getBidIncrement());
			auctiondao.setBuyItNowPrice(privateAuction.getBuyItNowPrice());
			auctiondao.setBidType(privateAuction.getBidType().toInt());
			auctiondao.setVisibilityType(privateAuction.getVisibilityType().toInt());
			auctiondao.setShopId(privateAuction.getWebShopId());
			auctiondao.setImageURL(privateAuction.getImageURL());
			/**
			 * Address and shipping instructions
			 */
			Set set = new HashSet();
			if (itemsSynch && auction.getTargetGroups() != null) {
				Iterator iter = auction.getTargetGroups().iterator();
				while (iter.hasNext()) {
					String tg = (String) iter.next();
					TargetGrpDAO targetGrpDAO =
						new TargetGrpDAO(OIDGenerator.newOID());
					synchTgtDAOFromAuction(targetGrpDAO,tg,privateAuction,true);
					set.add(targetGrpDAO);
				}
			}		
			if (itemsSynch && auction.getBusinessPartners() != null) {
				Iterator iter = auction.getBusinessPartners().iterator();
				while (iter.hasNext()) {
					String tg = (String) iter.next();
					TargetGrpDAO targetGrpDAO =
						new TargetGrpDAO(OIDGenerator.newOID());
					synchTgtDAOFromAuction(targetGrpDAO,tg,privateAuction,false);
					set.add(targetGrpDAO);
				}
			}		
			auctiondao.setTargetGroups(set);
		}
		if (itemsSynch && auction.getItems() != null) {
			Iterator iter = auction.getItems().iterator();
			while (iter.hasNext()) {
				PrivateAuctionItem auctionItem = (PrivateAuctionItem) iter.next();
				PrivateAuctionItemDAO auctionItemdao =
					new PrivateAuctionItemDAO(OIDGenerator.newOID());
				synchAuctionItemDAOFromBean(auctiondao, auctionItem, auctionItemdao,true);
				auctiondao.addAuctionItem(auctionItemdao);
			}
		}

	}

	public static void synchTgtDAOFromAuction(TargetGrpDAO tgtGrpDAO, String id,PrivateAuction auction,boolean tgtGrp){
		tgtGrpDAO.setTargetGrp(tgtGrp);
		tgtGrpDAO.setTargetId(id);
		tgtGrpDAO.setTargetGrp(tgtGrp);
		tgtGrpDAO.setAuctionId(auction.getAuctionId());
		tgtGrpDAO.setClient(auction.getClient());
		tgtGrpDAO.setSystemId(auction.getSystemId());
	}
	/**
	 * AuctionItem ---> AuctionItemDAO
	 */
	public static void synchAuctionItemDAOFromBean(
		PrivateAuctionDAO auctiondao,PrivateAuctionItem auctionItem,
		PrivateAuctionItemDAO auctionItemdao,boolean isNewItem) {
		if(isNewItem){
			auctionItem.setProductCode(
				auctionItem.getProductCode().toUpperCase());
			auctionItemdao.setClient(auctiondao.getClient());
			auctionItemdao.setSystemId(auctiondao.getSystemId());
			auctionItem.setId(auctionItemdao.getId());
		}
		auctionItemdao.setProductCode(auctionItem.getProductCode());
		auctionItemdao.setUom(auctionItem.getUOM());
		auctionItemdao.setProductId(auctionItem.getProductId());
		auctionItemdao.setQuantity(auctionItem.getQuantity());
		auctionItemdao.setDescription(auctionItem.getDescription());
		auctionItemdao.setImage(auctionItem.getImage());
	}

	/**
	 * @param winner
	 * @param winnerDAO
	 * @param b
	 */
	public static void synchWinnerDAOFromBean(
		Winner winner,
		PrivateAuctionWinnerDAO winnerDAO) {
		winnerDAO.setAuctionId(winner.getAuctionId());
		winnerDAO.setBidPrice(winner.getBidAmount());
		winnerDAO.setBpId(winner.getBuyerId());
		if (winner.getOrderGuid() != null)
			winnerDAO.setOrderGuid(winner.getOrderGuid().getIdAsString());
		if (winner.getOrderId() != null)
			winnerDAO.setOrderId(winner.getOrderId());
		winnerDAO.setQuantity(winner.getQuantity().intValue());
		winnerDAO.setUserId(winner.getUserId());
		winnerDAO.setClient(winner.getClient());
		winnerDAO.setSystemId(winner.getSystemId());
		if(winner instanceof PrivateAuctionWinner) {
			winnerDAO.setCheckedOut(((PrivateAuctionWinner)winner).isCheckedOut());	
			winnerDAO.setSoldToParty(((PrivateAuctionWinner)winner).getSoldToParty());
		}
		winnerDAO.setCreateDate(((PrivateAuctionWinner)winner).getCreatedDate());
	}

	/**
	 * @param winnerDAO
	 * @param winner
	 */
	public static void synchWinnerBeanFromDAO(
		PrivateAuctionWinnerDAO winnerDAO,
		PrivateAuctionWinner winner) {
		winner.setAuctionId(winnerDAO.getAuctionId());
		winner.setBidAmount(winnerDAO.getBidPrice());
		winner.setBuyerId(winnerDAO.getBpId());
		winner.setOrderGuid(new TechKey(winnerDAO.getOrderGuid()));
		winner.setOrderId(winnerDAO.getOrderId());
		winner.setQuantity(winnerDAO.getQuantity());
		winner.setUserId(winnerDAO.getUserId());
		winner.setSystemId(winnerDAO.getSystemId());
		winner.setClient(winnerDAO.getClient());
		winner.setWinnerId(winnerDAO.getId());
		winner.setCheckedOut(winnerDAO.getCheckedOut());
		winner.setSoldToParty(winnerDAO.getSoldToParty());
		winner.setCreatedDate(winnerDAO.getCreateDate());
		winner.setBidId(winnerDAO.getBid().getBidderId());


	}

	public static void syncVBeanFromDAO(PrivateAuctionBidDAO src, PrivateAuctionBid tgt) {
		tgt.setAuctionId(src.getAuctionId());
		tgt.setBidAmount(new java.math.BigDecimal(src.getBidPrice()));
		tgt.setQuantity(src.getBidQuantity());
		tgt.setBidDate(new java.sql.Timestamp(src.getBidTime().getTime()));
		tgt.setBidId(src.getId());
		tgt.setBuyerId(src.getBidderId());
		tgt.setCurrency(src.getCurrencyId());
		tgt.setSoldToParty(src.getBidderSoldToParty());
		tgt.setUserId(src.getUserId());
	}

	public static void syncVBeanToDAO(PrivateAuctionBid src, PrivateAuctionBidDAO tgt) {
		tgt.setAuctionId(src.getAuctionId());
		tgt.setBidPrice(src.getBidAmount().doubleValue());
		tgt.setBidQuantity(src.getQuantity());
		tgt.setBidTime(src.getBidDate());
		tgt.setId(src.getBidId());
		tgt.setBidderId(src.getBuyerId());
		tgt.setCurrencyId(src.getCurrency());
		tgt.setClient(src.getClient());
		tgt.setSystemId(src.getSystemId());
		tgt.setBidderSoldToParty(src.getSoldToParty());
	}
}
