package com.sap.isa.auction.businessobject.b2x;

import java.util.Properties;

import com.sap.isa.auction.config.AVWBuyerXCMConfigContainer;
import com.sap.isa.auction.config.XCMConfigContainer;
import com.sap.isa.auction.services.ServiceLocator;
import com.sap.isa.core.init.Initializable;
import com.sap.isa.core.init.InitializationEnvironment;
import com.sap.isa.core.init.InitializeException;
import com.sap.isa.core.logging.IsaLocation;


/**
 * 
 * AuctionRuntime provides a pseudo Runtime for Auction Application. Business Objects
 * can find the INIT status and register for Application life cycle events
 * 
 * AuctionRuntime is valid for the life of Application
 * 
 * Copyright:    Copyright (c) 2003
 * Company:
 * @author
 * @version 1.0
 */

public class AuctionBuyerRuntime implements Initializable {

	private static AuctionBuyerRuntime singleton = null;
	public static final int INIT_OK = 0x1;
	public static final int INIT_FAILED = 0x2;
	public static final int INIT_INPROCESS = 0x3;
	public static final int INIT_NOTDONE = 0x4;
	public static final int DESTROYED = 0x5;

//	private static final Category logger = CategoryProvider.getSystemCategory(); 
//	private static final Location tracer = Location.getLocation(AuctionRuntime.class.getName());
	private static final IsaLocation location = IsaLocation.getInstance(AuctionBuyerRuntime.class.getName());
	private com.sap.isa.core.ActionServlet env;

	private int initStatus = INIT_NOTDONE;
    
	//private HashMap observers = new HashMap();    

	public AuctionBuyerRuntime() {
		singleton = this; // Onyl one instance is created by the Managed env
	}

	public static AuctionBuyerRuntime getRuntime() {
		return singleton;
	}

	public Object getEnvironment() {
		return env;
	}

	public Object getContext() {
		return env.getServletContext();
	}

	/**
	 * Initialize the implementing component.
	 *
	 * Do not throw InitializeException fromt his method event thought it throws
	 * thsi checked exception since this leads to failure in Servlet Loading
	 * @param env the calling environment
	 * @param props   component specific key/value pairs
	 * @exception InitializeException at initialization error
	 */
	public void initialize(InitializationEnvironment env, Properties props)
		throws InitializeException {
		if(initStatus == INIT_OK) return;
		location.info("Start initializing AuctionBuyerRuntime");
		synchronized(this) {
			if(initStatus == INIT_INPROCESS) {
//				logger.warningT(tracer,"Concurrent initialization of Auction Runtime");
				location.warn("Concurrent initialization of Auction Runtime");
				return;
			}
			initStatus = INIT_INPROCESS;			
		}
        
		try {
			AuctionBusinessObjectManager.initialize(this);
		}
		catch(Exception ex) {
			String msg = "Auction Business Object Manager initialization failed";
			location.error(msg,ex);
//			LogHelper.logFatalError(logger,tracer,msg,ex);
			terminate();
			initStatus = INIT_FAILED;
			// don't throw exception
			// throw new InitializeException(msg);
		}

		this.env = (com.sap.isa.core.ActionServlet)env;
		AVWBuyerXCMConfigContainer.XCMConfigContainerClass = AVWBuyerXCMConfigContainer.class;
		ServiceLocator.ServiceLocatorClass = com.sap.isa.auction.services.b2x.ServiceLocator.class;
		
		initStatus = INIT_OK;
		location.info("AuctionBuyerRuntime is initialized successfully.");
	}

	public int getInitStatus() {
		return initStatus;
	}

	/**
	 * Terminate the component.
	 */
	public void terminate() {
		env = null;
		initStatus = DESTROYED;
		try {
			AuctionBusinessObjectManager.destroy();
		}
		catch(Exception ex) {
//			LogHelper.logError(logger,tracer,ex);
			location.error(ex);
		}

		try {
			com.sap.isa.auction.services.b2x.ServiceLocator.close();
		}
		catch(Exception ex) {
			location.error(ex);
//			LogHelper.logError(logger,tracer,ex);
		}
	}
	public boolean isAuctionEnabled(String scenarioName) {
		// Case where init of this runtime is not called
		if(AVWBuyerXCMConfigContainer.XCMConfigContainerClass != AVWBuyerXCMConfigContainer.class) {
			AVWBuyerXCMConfigContainer.XCMConfigContainerClass = AVWBuyerXCMConfigContainer.class;
			ServiceLocator.ServiceLocatorClass = com.sap.isa.auction.services.b2x.ServiceLocator.class;			
		}
		AVWBuyerXCMConfigContainer configContainer = (AVWBuyerXCMConfigContainer)
			XCMConfigContainer.getBaseInstance(scenarioName);
		return configContainer.isAuctionEnabled();
		
	}
}
