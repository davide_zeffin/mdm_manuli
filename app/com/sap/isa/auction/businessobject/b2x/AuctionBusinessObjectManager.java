package com.sap.isa.auction.businessobject.b2x;

/*****************************************************************************
	Copyright (c) 2001, SAPMarkets Palo Alto, USA, All rights reserved.

*****************************************************************************/

import com.sap.isa.auction.businessobject.AuctionBusinessObjectManagerBase;
import com.sap.isa.auction.businessobject.SystemSettingObject;
import com.sap.isa.auction.businessobject.businesspartner.BusinessPartnerList;
import com.sap.isa.auction.businessobject.order.AuctionOrder;
import com.sap.isa.auction.businessobject.targetGroup.TargetGroupList;
import com.sap.isa.auction.logging.LogHelper;
import com.sap.isa.auction.logging.TraceHelper;
import com.sap.isa.businessobject.Search;
import com.sap.isa.businessobject.order.Order;
import com.sap.isa.core.businessobject.event.BusinessObjectCreationEvent;
import com.sap.isa.persistence.pool.ObjectPool;


/**
 * AuctionBusinessObjectManager is the auction specific business object manager.
 * Central point to create and access Objects of the business logic. After
 * a request for a specific object, BusinessObjectManager checks if there is already
 * an object created. If so, a reference to this object is returned, otherwise
 * a new object is created and a reference to the new object is returned.
 * The reason for this class is, that the construction process of the businnes
 * logic objects may be quite complicated (because of some contextual
 * information) and most objects should only exist once per session.
 * To achieve this the <code>BusinessObjectManager</code>
 * is used. If you store a reference to this
 * object (for examle in a session context) you can get access to all other
 * business obejects using it.
 *
 */
public class AuctionBusinessObjectManager
extends AuctionBusinessObjectManagerBase 
 {
	private SystemSettingObject systemSetting = null;
	protected ProcessBusinessObjectManager processBOM;

 	
	// References to the business objects
	//protected Auction auction;
	protected transient BusinessPartnerList bpList;
	protected transient TargetGroupList tgList;
	protected transient Search search;
 	
 	
	/**
	 * Create a new instance of the object.
	 */
	public AuctionBusinessObjectManager() {
		super();
		if(logger.beInfo()) {
			logger.infoT(tracer,"AuctionBusinessObjectManager created");
		}
	}

	/**
	 * Is called from the MBOM whenever the BOM has to be released
	 * (new session or logoff, or timeout)
	 *
	 * @see BOManager com.sap.core.businessobject.management.BOManager
	 */
	public void release() {
		super.release();
		// return the objects to pool
		if(winnerManager != null) { winnerManager.close(); wmPool.returnObject(winnerManager);}
		if(bidManager != null) {bidManager.close(); bmPool.returnObject(bidManager);}
		if(auctionManager != null) {auctionManager.close(); amPool.returnObject(auctionManager);}

		winnerManager = null;
		bidManager = null;
		auctionManager = null;
		systemSetting = null;	
		
		if(logger.beInfo()) {
			logger.infoT(tracer,"AuctionBusinessObjectManager released");
		}		
	}

	public static void  initialize(Object ctxt) throws Exception {
		AuctionBusinessObjectManagerBase.initialize(ctxt);
		/** Statically created Pools => Application ClassLoader level existence*/
		amPool = new ObjectPool("AuctionManager Pool",new com.sap.isa.auction.businessobject.b2x.AuctionManagerFactory());
		wmPool = new ObjectPool("WinnerManager Pool",new com.sap.isa.auction.businessobject.b2x.WinnerManagerFactory());
		bmPool = new ObjectPool("BidManager Pool",new com.sap.isa.auction.businessobject.b2x.BidManagerFactory());
		smPool = new ObjectPool("SearchManager Pool",new com.sap.isa.auction.businessobject.b2x.SearchManagerFactory());
		try {
			amPool.setAllowGC(true);
			amPool.setGCInterval(2*60*60*1000); // 2 hrs
			amPool.setIdleTimeoutInterval(2*60*60*1000); // To aid in shrinking
			amPool.setMaxUsedInterval(1*60*60*1000); // to recover against loitering reference

			wmPool.setAllowGC(true);
			wmPool.setGCInterval(2*60*60*1000); // 2 hrs
			wmPool.setIdleTimeoutInterval(2*60*60*1000); // To aid in shrinking
			wmPool.setMaxUsedInterval(1*60*60*1000); // to recover against loitering reference

			bmPool.setAllowGC(true);
			bmPool.setGCInterval(2*60*60*1000); // 2 hrs
			bmPool.setIdleTimeoutInterval(2*60*60*1000); // To aid in shrinking
			bmPool.setMaxUsedInterval(1*60*60*1000); // to recover against loitering reference

			smPool.setAllowGC(true);
			smPool.setGCInterval(2*60*60*1000); // 2 hrs
			smPool.setIdleTimeoutInterval(2*60*60*1000); // To aid in shrinking
			smPool.setMaxUsedInterval(1*60*60*1000); // to recover against loitering reference

			amPool.initialize();
			wmPool.initialize();
			bmPool.initialize();
			smPool.initialize();
		}
		catch(Exception ex) {
			String msg = "Failed to initialize Object Pools";
			logger.fatalT(tracer,msg);
			LogHelper.logFatalError(logger,tracer,msg,ex);
			destroy();
			throw ex;
		}

		if(logger.beInfo()) {
			logger.infoT(tracer,"Auction BOM initialized");
		}

	}
	
	public synchronized TargetGroupList createTargetGroupList()  {
		if (tgList == null) {
			tgList = new TargetGroupList();
			setBEM(tgList);
			creationNotification(new BusinessObjectCreationEvent(tgList));
		}
		return tgList;
	}

	public TargetGroupList getTargetGroupList() {
		return tgList;
	}

	public synchronized BusinessPartnerList createBusinessPartnerList() {
		if (bpList == null) {
			bpList = new BusinessPartnerList();
			setBEM(bpList);
			creationNotification(new BusinessObjectCreationEvent(bpList));
		}
		return bpList;
	}

	public synchronized AuctionOrder createOrder(Order order) {
		final String METHOD_NAME = "createOrder";
		TraceHelper.entering(tracer, METHOD_NAME); 
		AuctionOrder aucOrder = new AuctionOrder();
		aucOrder.setTechKey(order.getTechKey());
		aucOrder.setHeader(order.getHeader());
		aucOrder.setItems(order.getItems());
		setBEM(aucOrder);
		creationNotification(new BusinessObjectCreationEvent(aucOrder));
		return aucOrder;
	}
	 /**
	 * Getter for the EAuction BusinessPartnerList
	 */
	public BusinessPartnerList getBusinessPartnerList() {
		return bpList;
	}

	public synchronized SystemSettingObject createSystemSetting()  {
		if (systemSetting == null) {
			systemSetting = new SystemSettingObject();
			setBEM(systemSetting);
			creationNotification(new BusinessObjectCreationEvent(systemSetting));
		}
		return systemSetting;
	}

	public SystemSettingObject getSystemSetting() {
		return createSystemSetting();
	}
	public void setProcessBusinessObjectManager(ProcessBusinessObjectManager pbom)
	{
		processBOM = pbom;
	}
	
	public ProcessBusinessObjectManager getProcessBusinessObjectManager()
	{
		return processBOM;
	}

	/**
	 * @return
	 */
	public Search getSearch() {
		return search;
	}
	/**
	 * Creates search business object
	 * @return
	 */
	public synchronized Search createSearch()  {
		if (search == null) {
			search = new Search();
			setBEM(search);
			creationNotification(new BusinessObjectCreationEvent(search));
		}
		return search;
	}

}
