/*
 * Created on Apr 23, 2004
 *
 * To change the template for this generated file go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
package com.sap.isa.auction.businessobject.b2x;

import com.sap.isa.auction.businessobject.AuctionSearchAttributes;

/**
 * Title:			Internet Sales Private Auctions
 * Description:
 * @Copyright: 		Copyright (c) 2004
 * Company:			SAPLabs LLC
 * @author 
 *
 * To change the template for this generated type comment go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
public interface PrivateAuctionSearchAttributes
	extends AuctionSearchAttributes {

	// Value Type java.lang.Integer
	public static final String VISIBILITY = "visibility";

	//Value Type java.lang.Integer
	public static final String BID_TYPE = "bidType";
	
	//Value should be java.lang.String
	public static final String PRODUCT_GUID = "productGuid"; //bidder specific
	
	//Value Type java.lang.Integer
	public static final String WINNER_DETERM_TYPE = "winnerDeterminationType";

//	// Value Type java.lang.String
//	public static final String BP_ID = "businessPartnerId"; //seller specific
//
//	// Value Type java.lang.Integer
//	public static final String TG_ID = "targetGroupId";

	//Value Type java.lang.Double
	public static final String START_PRICE = "startPrice";
	
	//Value Type java.lang.Double
	public static final String RESERVE_PRICE = "reservePrice";
	public static final String[] ATTRIBUTES =
		{
			NAME,
			STATUS,
			CHECKOUT_STATUS,
			TYPE,
			PRODUCT_ID,
			WEBSHOP_ID,
			PUBLISH_DATE,
			SALESAREA,
			PUBLISH_ERROR,
			START_DATE,
			END_DATE,
			CREATE_DATE,
			CREATED_BY,
			START_PRICE,
			RESERVE_PRICE,
			VISIBILITY,
			BID_TYPE,
			PRODUCT_GUID,
			WINNER_DETERM_TYPE,
			ORDER_ID, TITLE};
}
