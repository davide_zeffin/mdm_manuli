/*
 * Created on Sep 30, 2003
 *
 */
package com.sap.isa.auction.businessobject.b2x;

import java.util.Collection;

import javax.jdo.PersistenceManager;
import javax.jdo.Query;

import com.sap.tc.logging.Category;
import com.sap.tc.logging.Location;
import com.sap.isa.auction.bean.ExecutionStatusEnum;
import com.sap.isa.auction.bean.b2x.PrivateAuction;
import com.sap.isa.auction.businessobject.JDOQueryResult;
import com.sap.isa.auction.businessobject.b2x.jdo.PrivateAuctionDAO;
import com.sap.isa.auction.logging.CategoryProvider;
import com.sap.isa.auction.logging.LogHelper;
import com.sap.isa.crt.Call;
import com.sap.isa.crt.RecordingCallManager;
import com.sap.isa.crt.RecordingCallManagerFactory;
import com.sap.isa.crt.Result;

/**
 * @author I802891
 *
 */
public class PrivateAuctionJDOQueryResult extends JDOQueryResult {
	
	/** Static data block*/
	private static Category logger = CategoryProvider.getBOLCategory();	
	private static Location location = Location.getLocation(PrivateAuctionJDOQueryResult.class.getName());

	protected RecordingCallManagerFactory rcmf = null;
	/**
	 * @param pm
	 * @param query
	 * @param coll
	 * @param resultSize
	 */
	public PrivateAuctionJDOQueryResult(
		PersistenceManager pm,
		Query query,
		Collection coll,
		int resultSize,RecordingCallManagerFactory rcmf) {
		super(pm, query, coll, resultSize);
		this.rcmf = rcmf;
	}

	/* (non-Javadoc)
	 * @see com.sap.isa.auction.businessobject.JDOQueryResult#transcribe(java.lang.Object)
	 */
	protected Object transcribe(Object dao) {
		PrivateAuctionDAO auctionDAO = (PrivateAuctionDAO)dao;
		PrivateAuction auction = new PrivateAuction();
		DALBeanSynchronizer.synchAuctionBeanFromDAO(auctionDAO,auction);
		internalSyncExecutionException(auction);
		return auction;
	}
	
	private void internalSyncExecutionException(PrivateAuction auction) {
		final String METHOD = "internalSyncExecutionException";
		
		if(auction.getExecutionRequestId() != null &&
			(auction.getExecutionStatus() == ExecutionStatusEnum.SYSTEM_ERROR ||
			auction.getExecutionStatus() == ExecutionStatusEnum.USER_ERROR)) {
            
			try {	
				RecordingCallManager rcm = (RecordingCallManager)rcmf.getInstance(null);
				String reqId = auction.getExecutionRequestId();
				Call call = rcm.getCallById(reqId);
				Result result = rcm.invocationResult(call); 
				auction.setExecutionException(result.getException());
			}
			catch(Exception ignore) {
				LogHelper.logWarning(logger, location, ignore);
			}
		}
	}
	public void close() {
		if(super.closed) return;
		try {
			super.close();
			rcmf = null;
		}
		catch(Exception e) {
			LogHelper.logWarning(logger,location,e);
		}
		
	}
}