/*
 * Created on Apr 23, 2004
 *
 * To change the template for this generated file go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
package com.sap.isa.auction.businessobject.b2x;

import com.sap.isa.auction.businessobject.AuctionQueryFilter;

/**
 * Title:			Internet Sales Private Auctions
 * Description:
 * @Copyright: 		Copyright (c) 2004
 * Company:			SAPLabs LLC
 * @author 
 *
 * To change the template for this generated type comment go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
public class PrivateAuctionQueryFilter extends AuctionQueryFilter 
				implements PrivateAuctionSearchAttributes{
	/**
	 * Validates against Private Auction search attributes
	 */
	protected void validateSearchAttr(String attrName) {
		String[] names = PrivateAuctionSearchAttributes.ATTRIBUTES;
		boolean valid = false;
		int i = 0;
		while (!valid && i < names.length) {
			if (attrName.equalsIgnoreCase(names[i++])) {
				valid = true;
			}
		}
		if (!valid)
			throw new IllegalArgumentException(
				attrName + " is not a valid search attribute");
	}
}
