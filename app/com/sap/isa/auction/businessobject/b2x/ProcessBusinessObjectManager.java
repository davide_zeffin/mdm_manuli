package com.sap.isa.auction.businessobject.b2x;

/**
 * Title:        eBay Integration Project
 * Description:
 * Copyright:    Copyright (c) 2003
 * Company:      SAP Labs LLC
 * @author
 * @version 1.0
 */

/**
 * This class intends to be used in the application scope
 * mainly for the purpose of order creation and customer master 
 * creation
 * it will be used for the purpose of auction background processing
 */

import java.util.Iterator;
import java.util.Map;
import java.util.Set;

import com.sap.isa.auction.backend.exception.CommunicationBackendUserException;
import com.sap.isa.auction.backend.exception.SAPBackendException;
import com.sap.isa.auction.backend.exception.SAPBackendUserException;
import com.sap.isa.auction.bean.Auction;
import com.sap.isa.auction.bean.Winner;
import com.sap.isa.auction.businessobject.AuctionGenericBusinessObjectManager;
import com.sap.isa.auction.businessobject.SystemSettingObject;
import com.sap.isa.auction.businessobject.email.EmailNotification;
import com.sap.isa.auction.businessobject.email.ErrorNotification;
import com.sap.isa.auction.businessobject.order.AuctionOrder;
import com.sap.isa.auction.businessobject.order.AuctionOrderCreate;
import com.sap.isa.auction.businessobject.order.AuctionQuotation;
import com.sap.isa.auction.businessobject.order.AuctionQuotationChange;
import com.sap.isa.auction.businessobject.order.AuctionQuotationCreate;
import com.sap.isa.auction.exception.InvalidStateException;
import com.sap.isa.auction.exception.i18n.ErrorMessageBundle;
import com.sap.isa.auction.exception.i18n.I18nErrorMessage;
import com.sap.isa.auction.logging.CategoryProvider;
import com.sap.isa.auction.logging.LogHelper;
import com.sap.isa.auction.logging.TraceHelper;
import com.sap.isa.auction.util.StringUtil;
import com.sap.isa.backend.BackendTypeConstants;
import com.sap.isa.backend.boi.isacore.ShopBackend;
import com.sap.isa.businessobject.BusinessObjectManager;
import com.sap.isa.businessobject.CommunicationException;
import com.sap.isa.businessobject.Shop;
import com.sap.isa.core.TechKey;
import com.sap.isa.core.businessobject.BackendAware;
import com.sap.isa.core.eai.BackendException;
import com.sap.isa.core.util.MessageList;
import com.sap.tc.logging.Category;
import com.sap.tc.logging.Location;

/**
 * In this implementation, in the application context we only have one
 * metaBOM, then one processbusinessobjectmanager, but the same pbom can contain
 * one than one order and other business object.
 */
public class ProcessBusinessObjectManager extends AuctionGenericBusinessObjectManager
	implements BackendAware {
	
	private static final Category logger = CategoryProvider.getBOLCategory();
	private static final Location tracer = Location.getLocation(ProcessBusinessObjectManager.class.getName());	

	// Maxmimum Number of intances that can be created of the indexed
	// business objects like order
	private static final int    MAX_INSTANCES          = 2;


	// Number of instances expected for one type. This value is no
	// upper limit and only used to initialize the lists with the
	// right size
	private static final int NUM_INSTANCES = 2;
	/**
	 * Constant containing the name of this BOM
	 */
	public static final String PROCESS_BOM = "PROCESS-BOM";
    	    
  
//	  /// a handy attribute to get the onetime customer ID
//	  private String oneTimeCustomerId;
//	  private BackendDefaultSettings backendSettings;

  public ProcessBusinessObjectManager() {
	///initialize();
  }

  /**
   * Search a  Auction Order object.
   * @param 		order, here should be auctionorder
   * @return winner ID to the found object, or null if not found
   */
  public synchronized String searchOrder(AuctionOrder target) {
	  Map allInstances = (Map) createdObjects.get(AuctionOrder.class);
	  if (allInstances == null) {
		  // No object and no index up to now
		  return null;
	  }
	  // the next avaliable one
	  Set set = allInstances.entrySet();
	  Iterator it = set.iterator();
	  while(it.hasNext()) {
		Map.Entry entry = (Map.Entry)it.next();
		if (entry.getValue() == target) {
		  return (String)entry.getKey();
		}
	  }
	  return null;
  }

	/**
	 * Creates a new order object. If such an object was already created, a
	 * reference to the old object is returned and no new object is created.
	 * it will use the next avaliable index
	 * @return reference to a newly created or already existing order object
	 */
	public synchronized AuctionOrder createOrder() {
		return (AuctionOrder) createBusinessObject(AuctionOrder.class);
	}

	/**
	 * Creates a new order object. If such an object was already created, a
	 * reference to the old object is returned and no new object is created.
	 *
	 * @param id ID of the winner, in orer to 
	 * @return reference to a newly created or already existing order object or
	 *         <code>null</code> if the index is exceeded
	 */
	public synchronized AuctionOrder createOrder(String id) {
		return (AuctionOrder) createBusinessObject(AuctionOrder.class, id);
	}


	/**
	 * Returns a reference to an existing order object.
	 *
	 * @return reference to order object or null if no object is present
	 */
	public AuctionOrder getOrder() {
		return getOrder(DEFAULT_ID);
	}

	/**
	 * Returns a reference to an existing order object.
	 *
	 * @param id ID of the auction ID
	 * @return reference to order object, <code>null</code> if no object is present
	 *         at the given position of if the index is exceeded
	 */
	public AuctionOrder getOrder(String id) {
		AuctionOrder existingOrder =  null;
		// get the order from winner first
		existingOrder = (AuctionOrder) getBusinessObject(AuctionOrder.class, 
															id);
		if(existingOrder != null){
			return existingOrder;
		}
		/// get from the map directly
		Map orderMap = getBusinessObjectsByType(AuctionOrder.class);
		Iterator it = orderMap.values().iterator();	
		while (it.hasNext()){
			AuctionOrder oneOrder = (AuctionOrder)it.next();
			if(oneOrder.getHeader().getSalesDocNumber().
							equalsIgnoreCase(id)){
				existingOrder = oneOrder;
				return oneOrder;					
			}
		}
		/// if the order id is still not found
		/// now new an AuctionOrder Object
		if(existingOrder == null){
			existingOrder = new AuctionOrder();
			String convertId = StringUtil.ExtendNumber(id, 
						StringUtil.TRANSACTION_NUMBER_LENGTH);
			existingOrder.setTechKey(new TechKey(convertId));
			existingOrder.getHeader().setSalesDocNumber(convertId);
			existingOrder.setBackendObjectManager(bem);
		}
		return existingOrder;
	}

	/**
	 * Releases references to the order
	 */
	public synchronized void releaseOrder() {
		releaseOrder(DEFAULT_ID);
	}

	/**
	 * Releases references to the order object specified by the index
	 *
	 * @param id ID of the winner
	 */
	public synchronized void releaseOrder(String id) {
		releaseBusinessObject(AuctionOrder.class, id);
	}

	/**
	 * Releases references to the order
	 */
	public synchronized void releaseOrder(AuctionOrder target) {
		String id = searchOrder(target);
		if(id != null )
			releaseOrder(id);
	}

	/**
	 * Release all the orders, iterate all the index to release all the orders
	 *
	 */
	public synchronized void releaseAllOrder() {
		Map allInstances = (Map) createdObjects.get(AuctionOrder.class);
		if (allInstances != null) {
		  Set keySet = allInstances.keySet();
		  Iterator it = keySet.iterator();
		  while(it.hasNext()) {
			String id = (String)it.next();
			releaseBusinessObject(AuctionOrder.class, id);
		  }
		}
	}
    
	/**
	 * Creates a new order object. If such an object was already created, a
	 * reference to the old object is returned and no new object is created.
	 * it will use the next avaliable index
	 * @return reference to a newly created or already existing order object
	 */
	public synchronized AuctionOrder createOrder(Auction auction, Winner winner) {
		final String METHOD_NAME = "createOrder";
		TraceHelper.entering(tracer, METHOD_NAME); 
		AuctionOrder aucOrder = (AuctionOrder) createBusinessObject(
										AuctionOrder.class,
										winner.getAuctionId());
		AuctionOrderCreate orderCreate = new AuctionOrderCreate(aucOrder);
		// get the shop infor from auction
		Shop shop = auction.getWebShop();
		if(shop == null){
			try{
				shop = getShop(auction.getWebShopId());
				auction.setWebShop(shop);
			}catch(SAPBackendException ex){
				LogHelper.logError(logger,tracer,
					"During the read shop data of createQuotation for "
						+ " in ProcessBusinessObjectManager backend exception",
					ex);	
				throw ex;
			}

		}
		try {
			orderCreate.createAuctionOrder(auction, winner, shop);
		} catch (SAPBackendUserException ex) {
			throw ex;
		} catch(Exception ex){
			logger.errorT(tracer,"During the creation of sales order for in ProcessBusinessObjectManager" +
								 " Communication exception");
			LogHelper.logError(logger,tracer,ex);			
			log.error("During the creation of sales order for " +
						" in ProcessBusinessObjectManager unknown exception",
						ex);	
			ErrorMessageBundle bundle = ErrorMessageBundle.getInstance();
			I18nErrorMessage msg = bundle.newI18nErrorMessage(SAPBackendException.UNKNOWN_BACKEND_ERROR);
			SAPBackendException _ex = new SAPBackendException(msg);
			TraceHelper.throwing(tracer,METHOD_NAME,_ex.getMessage(),_ex);
			throw _ex;
		}	
		TraceHelper.exiting(tracer, METHOD_NAME);
		return aucOrder;
	}
	
	/**
	 * Creates a new order object. If such an object was already created, a
	 * reference to the old object is returned and no new object is created.
	 * it will use the next avaliable index
	 * @return reference to a newly created or already existing order object
	 */
	public synchronized AuctionOrder createOrder(Auction auction, Winner winner,
												 String soldToId) {
		final String METHOD_NAME = "createOrder";
		TraceHelper.entering(tracer, METHOD_NAME); 
		AuctionOrder aucOrder = (AuctionOrder) createBusinessObject(
										AuctionOrder.class,
										winner.getAuctionId());
		AuctionOrderCreate orderCreate = new AuctionOrderCreate(aucOrder);
		// get the shop infor from auction
		Shop shop = auction.getWebShop();
		if(shop == null){
			try{
				shop = getShop(auction.getWebShopId());
				auction.setWebShop(shop);
			}catch(SAPBackendException ex){
				LogHelper.logError(logger,tracer,
					"During the read shop data of createQuotation for "
						+ " in ProcessBusinessObjectManager backend exception",
					ex);	
				throw ex;
			}

		}
		try {
			if (soldToId != null){
				orderCreate.setSoldToId(soldToId);
				LogHelper.logInfo(logger, tracer, "The soldTo Id is in " +
					" createOrder is " + soldToId, null);
			}
			orderCreate.createAuctionOrder(auction, winner, shop);
		} catch (SAPBackendUserException ex) {
			throw ex;
		} catch(Exception ex){
			logger.errorT(tracer,"During the creation of sales order for in ProcessBusinessObjectManager" +
								 " Communication exception");
			LogHelper.logError(logger,tracer,ex);			
			log.error("During the creation of sales order for " +
						" in ProcessBusinessObjectManager unknown exception",
						ex);	
			ErrorMessageBundle bundle = ErrorMessageBundle.getInstance();
			I18nErrorMessage msg = bundle.newI18nErrorMessage(SAPBackendException.UNKNOWN_BACKEND_ERROR);
			SAPBackendException _ex = new SAPBackendException(msg);
			TraceHelper.throwing(tracer,METHOD_NAME,_ex.getMessage(),_ex);
			throw _ex;
		}	
		TraceHelper.exiting(tracer, METHOD_NAME);
		return aucOrder;
	}	
	
	/**
	 * Search a  Auction Quotation object.
	 * @param 		order, here should be auction quotation
	 * @return winner ID to the found object, or null if not found
	 */
	public synchronized String searchQuotation(AuctionQuotation target) {
		Map allInstances = (Map) createdObjects.get(AuctionQuotation.class);
		if (allInstances == null) {
			// No object and no index up to now
			return null;
		}
		// the next avaliable one
		Set set = allInstances.entrySet();
		Iterator it = set.iterator();
		while(it.hasNext()) {
		  Map.Entry entry = (Map.Entry)it.next();
		  if (entry.getValue() == target) {
			return (String)entry.getKey();
		  }
		}
		return null;
	}

	  /**
	   * Creates a new quotation object. If such an object was already created, a
	   * reference to the old object is returned and no new object is created.
	   * it will use the next avaliable index
	   * @return reference to a newly created or already existing order object
	   */
	  public synchronized AuctionQuotation createQuotation() {
		  return (AuctionQuotation) createBusinessObject(AuctionQuotation.class);
	  }
	  
	  /**
	   * Creates a new quotation object. If such an object was already created, a
	   * reference to the old object is returned and no new object is created.
	   * it will use the next avaliable index
	   * @param	 The auction object passed in for the creation of Quotation
	   * @return reference to a newly created or already existing order object
	   */
	  public synchronized AuctionQuotation createQuotation(Auction auction) {
		final String METHOD_NAME = "createQuotation";
		TraceHelper.entering(tracer, METHOD_NAME);		
		AuctionQuotation aucQuote =
			(AuctionQuotation) createBusinessObject(AuctionQuotation.class,
								getAuctionUniqueId(auction));
		AuctionQuotationCreate quoteCreate =
			new AuctionQuotationCreate(aucQuote);
		// get the shop infor from auction
		Shop shop = auction.getWebShop();
		if(shop == null){
			try{
				shop = getShop(auction.getWebShopId());
				auction.setWebShop(shop);
			}catch(SAPBackendException ex){
				LogHelper.logError(logger,tracer,
					"During the read shop data of createQuotation for "
						+ " in ProcessBusinessObjectManager backend exception",
					ex);	
				throw ex;
			}

		}
		try {
			quoteCreate.createFromAuction(auction, shop);
		} catch (CommunicationBackendUserException ex) {
			logger.errorT(tracer,
				"During the creation of Quotation for in ProcessBusinessObjectManager"
					+ " CommunicationBackendUserException");
			LogHelper.logError(logger,tracer,ex);
			SAPBackendUserException _ex = new SAPBackendUserException(ex);
			throw _ex;
		}  catch (CommunicationException ex) {
			logger.errorT(tracer,
				"During the creation of Quotation for in ProcessBusinessObjectManager"
					+ " Communication exception");
			LogHelper.logError(logger,tracer,ex);		
			ErrorMessageBundle bundle = ErrorMessageBundle.getInstance();
			I18nErrorMessage msg =
				bundle.newI18nErrorMessage(
					SAPBackendException.BACKEND_COMMUNICATION_FAILED);
			SAPBackendException _ex = new SAPBackendException(msg);
			throw _ex;

		} catch (SAPBackendUserException ex) {
			logger.errorT(tracer,
				"During the creation of Quotation for in ProcessBusinessObjectManager"
					+ " SAPBackendUserException exception");
			LogHelper.logError(logger,tracer,ex);
			ErrorMessageBundle bundle = ErrorMessageBundle.getInstance();
			I18nErrorMessage msg =
				bundle.newI18nErrorMessage(
					SAPBackendException.BACKEND_COMMUNICATION_FAILED);
			SAPBackendException _ex = new SAPBackendException(msg);
			throw _ex;
		}catch (SAPBackendException ex){
			logger.errorT(tracer,
				"During the creation of Quotation for in ProcessBusinessObjectManager"
					+ " SAPBackendException exception");
			LogHelper.logError(logger, tracer, ex);
			throw ex; 	
		}catch (Exception ex) {
			logger.errorT(tracer,
				"During the creation of Quotation for "
					+ " in ProcessBusinessObjectManager unknown exception" 
						+ ex.getMessage());
			LogHelper.logError(logger,tracer,ex);
			ErrorMessageBundle bundle = ErrorMessageBundle.getInstance();
			I18nErrorMessage msg =
				bundle.newI18nErrorMessage(
					SAPBackendException.UNKNOWN_BACKEND_ERROR);
			SAPBackendException _ex = new SAPBackendException(msg);
			throw _ex;
		}
		TraceHelper.exiting(tracer, METHOD_NAME);
		return aucQuote;
	  }
  
	  /**
	   * It is used to track all of the auction quotation by a 
	   * a unique Id
	   * @param 	auction the auction Object used to generate
	   * @return	The unique Id to track a auction
	   */	
	  private String getAuctionUniqueId(Auction auction){
		String uniqueId = null;
		if(auction == null){
			logger.errorT(tracer, "In the ProcessBusinessObjectManager, the auction object" +
				" is null for getting unique ID");
			ErrorMessageBundle bundle = ErrorMessageBundle.getInstance();
			I18nErrorMessage msg = bundle.newI18nErrorMessage(
						InvalidStateException.NOT_AVAILABLE);
			InvalidStateException _ex = new InvalidStateException(msg);
			throw _ex;				
		}
		uniqueId = auction.getAuctionId(); 
		return uniqueId;	  
	  }
	  /*
	   * modify an existing quotation object. If such an object was already created, a
	   * reference to the old object is returned and no new object is created.
	   * it will use the next avaliable index
	   * @param	 The auction object passed in for the creation of Quotation
	   * @return reference to a newly created or already existing order object
	   */
	  public synchronized AuctionQuotation modifyQuotation(Auction auction) {
		final String METHOD_NAME = "modifyQuotation";
		TraceHelper.entering(tracer, METHOD_NAME);	
		String uniQueId = getAuctionUniqueId(auction);	  	 
		AuctionQuotation aucQuote = null;
		aucQuote = getQuotation(uniQueId);
		if(aucQuote == null){
			log.warn("In ProcessBusinessObjectManager, CAN not get" +
				"Quotation based on ID " + uniQueId);
			aucQuote = (AuctionQuotation) createBusinessObject(AuctionQuotation.class);
			AuctionQuotationCreate quoteCreate =
				new AuctionQuotationCreate(aucQuote);
			Shop shop = new Shop();
			try {
				quoteCreate.createFromAuction(auction, shop);
			} catch (SAPBackendException ex) {
				log.error(
					"During the creation of sales order for in ProcessBusinessObjectManager"
						+ " Communication exception",
					ex);
				throw ex;
			} catch (Exception ex) {
				log.error(
					"During the creation of sales order for "
						+ " in ProcessBusinessObjectManager unknown exception",
					ex);
				ErrorMessageBundle bundle = ErrorMessageBundle.getInstance();
				I18nErrorMessage msg = bundle.newI18nErrorMessage(SAPBackendException.UNKNOWN_BACKEND_ERROR);
				SAPBackendException _ex = new SAPBackendException(msg);
				throw _ex;
			}
			finally {
				releaseQuotation(aucQuote);
			}
		}else{
			AuctionQuotationChange quoteChange =
				new AuctionQuotationChange(aucQuote);
			try {
				quoteChange.modifyQuotation(auction);
			} catch (CommunicationException ex) {
				log.error(
					"During the modification of of Auction Quotation " +
						"for in ProcessBusinessObjectManager"
						+ " Communication exception",
					ex);
				ErrorMessageBundle bundle = ErrorMessageBundle.getInstance();
				I18nErrorMessage msg = bundle.newI18nErrorMessage(SAPBackendException.BACKEND_COMMUNICATION_FAILED);
				SAPBackendException _ex = new SAPBackendException(msg);
				throw _ex;
			} catch (Exception ex) {
				log.error(
					"During the creation of AuctionQuotation for "
						+ " in ProcessBusinessObjectManager unknown exception",
					ex);
				ErrorMessageBundle bundle = ErrorMessageBundle.getInstance();
				I18nErrorMessage msg = bundle.newI18nErrorMessage(SAPBackendException.UNKNOWN_BACKEND_ERROR);
				SAPBackendException _ex = new SAPBackendException(msg);
				throw _ex;
			}
			finally {
				releaseQuotation(aucQuote);
			}
		}
		TraceHelper.exiting(tracer, METHOD_NAME);
		return aucQuote;
	  }
	  

	  /**
	   * Creates a new quotation object. If such an object was already created, a
	   * reference to the old object is returned and no new object is created.
	   *
	   * @param id ID of the winner, in orer to 
	   * @return reference to a newly created or already existing order object or
	   *         <code>null</code> if the index is exceeded
	   */
	  public synchronized AuctionQuotation createQuotation(String id) {
		  return (AuctionQuotation) createBusinessObject(AuctionQuotation.class, id);
	  }


	  /**
	   * Returns a reference to an existing order object.
	   *
	   * @return reference to order object or null if no object is present
	   */
	  public AuctionQuotation getQuotation() {
		  return getQuotation(DEFAULT_ID);
	  }

	  /**
	   * Returns a reference to an existing order object.
	   *
	   * @param id ID of the winner
	   * @return reference to order object, <code>null</code> if no object is present
	   *         at the given position of if the index is exceeded
	   */
	  public AuctionQuotation getQuotation(String id) {
		  return (AuctionQuotation) getBusinessObject(AuctionQuotation.class, id);
	  }

	  /**
	   * Releases references to the order
	   */
	  public synchronized void releaseQuotation() {
		  releaseQuotation(DEFAULT_ID);
	  }

	  /**
	   * Releases references to the order object specified by the index
	   *
	   * @param id ID of the winner
	   */
	  public synchronized void releaseQuotation(String id) {
		  releaseBusinessObject(AuctionQuotation.class, id);
	  }

	  /**
	   * Releases references to the order
	   */
	  public synchronized void releaseQuotation(AuctionQuotation target) {
		  String id = searchQuotation(target);
		  if(id != null )
			  releaseQuotation(id);
	  }

	  /**
	   * Release all the orders, iterate all the index to release all the orders
	   *
	   */
	  public synchronized void releaseAllQuotation() {
		  Map allInstances = (Map) createdObjects.get(AuctionQuotation.class);
		  if (allInstances != null) {
			Set keySet = allInstances.keySet();
			Iterator it = keySet.iterator();
			while(it.hasNext()) {
			  String id = (String)it.next();
			  releaseBusinessObject(AuctionQuotation.class, id);
			}
		  }
	  }
    
	/**
	 * This method simulates the backend quotation and
	 * order creation process, all of the information
	 * comes from auction object, here only the onetime customer
	 * is used. 
	 *
	 */
	public MessageList simulate(Auction auction){
		MessageList messageList = new MessageList();
		// simulate the auctioin process
		
		
		return messageList;
	}
	
	/**
	 * This metod read a shop data from a given shop ID, the shop
	 * object is linked to a auction object, and is not cached in 
	 * the memory
	 * @param shopId
	 * @return
	 */
	public Shop getShop(String shopIdValue){
		final String METHOD_NAME = "getShop";
		TraceHelper.entering(tracer, METHOD_NAME);	
		Shop existingShop = null;
		existingShop = (Shop) getBusinessObject(Shop.class, shopIdValue);
		if(existingShop != null){
			return existingShop;
		}
		/// get from the map directly
		Map shopMap = getBusinessObjectsByType(Shop.class);
		TechKey shopKey = new TechKey(shopIdValue);
		try {
			ShopBackend shopBE =
				(ShopBackend) bem.createBackendBusinessObject(BackendTypeConstants.BO_TYPE_SHOP,
				 BusinessObjectManager.OBJECT_FACTORY);

			Shop shop = (Shop) shopBE.read(shopKey);
			shopMap.put(shopIdValue, shop);
			existingShop = shop;
		}
		catch (BackendException ex) {
			LogHelper.logError(logger,tracer,
				"During the read shop data for "
					+ " in ProcessBusinessObjectManager backend exception",
				ex);
			ErrorMessageBundle bundle = ErrorMessageBundle.getInstance();
			I18nErrorMessage msg = bundle.newI18nErrorMessage(SAPBackendException.UNKNOWN_BACKEND_ERROR);
			SAPBackendException _ex = new SAPBackendException(msg);
			throw _ex;
		}
		TraceHelper.exiting(tracer, METHOD_NAME);
		return existingShop;
	}

	/**
	 * Creates a new EmailNotification object. If such an object was already created, a
	 * reference to the old object is returned and no new object is created.
	 * it will use the next avaliable index
	 * @return reference to a newly created or already existing EmailNotification object
	 */
	public synchronized EmailNotification createEmailNotification() {
		return (EmailNotification) createBusinessObject(EmailNotification.class);
	}

	/**
	 * Returns a reference to an existing EmailNotification object.
	 *
	 * @return reference to EmailNotification object or null if no object is present
	 */
	public synchronized EmailNotification getEmailNotification() {
		return (EmailNotification) getBusinessObject(EmailNotification.class);
	}

	/**
	 * Releases references to the EmailNotification
	 */
	public synchronized void releaseEmailNotification() {
		releaseBusinessObject(EmailNotification.class);
	}
	
	/**
	 * Creates a new ErrorNotification object. If such an object was already created, a
	 * reference to the old object is returned and no new object is created.
	 * it will use the next avaliable index
	 * @return reference to a newly created or already existing ErrorNotification object
	 */
	public synchronized ErrorNotification createErrorNotification() {
		return (ErrorNotification) createBusinessObject(ErrorNotification.class);
	}

	/**
	 * Returns a reference to an existing ErrorNotification object.
	 *
	 * @return reference to ErrorNotification object or null if no object is present
	 */
	public synchronized ErrorNotification getErrorNotification() {
		return (ErrorNotification) getBusinessObject(ErrorNotification.class);
	}

	/**
	 * Releases references to the ErrorNotification
	 */
	public synchronized void releaseErrorNotification() {
		releaseBusinessObject(EmailNotification.class);
	}	

	public synchronized SystemSettingObject createSystemSetting()  {
		return (SystemSettingObject) createBusinessObject(SystemSettingObject.class);
	}

	public synchronized SystemSettingObject getSystemSetting() {
		return (SystemSettingObject) getBusinessObject(SystemSettingObject.class);
	}

	public synchronized void releaseSystemSetting() {
		releaseBusinessObject(SystemSettingObject.class);
	}
}
