package com.sap.isa.auction.businessobject.b2x;

import java.sql.Timestamp;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.ResourceBundle;

import javax.jdo.JDOException;
import javax.jdo.PersistenceManager;
import javax.jdo.PersistenceManagerFactory;
import javax.jdo.Query;

import com.sap.localization.LocalizationException;
import com.sap.tc.logging.Location;
import com.sap.isa.auction.backend.boi.WinnerData;
import com.sap.isa.auction.backend.boi.email.EmailNotificationType;
import com.sap.isa.auction.backend.boi.user.AuctionUserBaseData;
import com.sap.isa.auction.backend.exception.SAPBackendException;
import com.sap.isa.auction.bean.Auction;
import com.sap.isa.auction.bean.AuctionStatusEnum;
import com.sap.isa.auction.bean.AuctionTypeEnum;
import com.sap.isa.auction.bean.ExecutionStatusEnum;
import com.sap.isa.auction.bean.ValidatingBean;
import com.sap.isa.auction.bean.Winner;
import com.sap.isa.auction.bean.b2x.PrivateAuction;
import com.sap.isa.auction.bean.b2x.PrivateAuctionItem;
import com.sap.isa.auction.bean.b2x.PrivateAuctionWinner;
import com.sap.isa.auction.bean.search.Criteria;
import com.sap.isa.auction.bean.search.QueryFilter;
import com.sap.isa.auction.bean.search.QueryResult;
import com.sap.isa.auction.businessobject.AbstractBusinessObject;
import com.sap.isa.auction.businessobject.AuctionQueryFilter;
import com.sap.isa.auction.businessobject.BOUserException;
import com.sap.isa.auction.businessobject.ContextManager;
import com.sap.isa.auction.businessobject.ValidationException;
import com.sap.isa.auction.businessobject.b2x.jdo.PrivateAuctionDAO;
import com.sap.isa.auction.businessobject.b2x.jdo.PrivateAuctionItemDAO;
import com.sap.isa.auction.businessobject.b2x.jdo.TargetGrpDAO;
import com.sap.isa.auction.businessobject.b2x.status.AuctionStatus;
import com.sap.isa.auction.businessobject.email.EmailNotification;
import com.sap.isa.auction.businessobject.email.ErrorNotification;
import com.sap.isa.auction.businessobject.b2x.jdo.AuctionDAO;
import com.sap.isa.auction.businessobject.order.AuctionQuotation;
import com.sap.isa.auction.businessobject.order.AuctionQuotationChange;
import com.sap.isa.auction.exception.AuctionRuntimeExceptionBase;
import com.sap.isa.auction.exception.InvalidStateException;
import com.sap.isa.auction.exception.SystemException;
import com.sap.isa.auction.exception.i18n.ErrorMessageBundle;
import com.sap.isa.auction.exception.i18n.I18nErrorMessage;
import com.sap.isa.auction.logging.LogHelper;
import com.sap.isa.auction.logging.TraceHelper;
import com.sap.isa.auction.services.ServiceLocator;
import com.sap.isa.auction.util.TrackedArrayList;
import com.sap.isa.core.TechKey;
import com.sap.isa.crt.AsyncCallback;
import com.sap.isa.crt.AsyncResult;
import com.sap.isa.crt.Result;
import com.sap.isa.persistence.helpers.OIDGenerator;
import com.sap.isa.thread.GenericThreadPool;
import com.sap.isa.user.businessobject.UserBase;

/**
 * Title:
 * Description: Stateless and Pooled Business Object
 * Copyright:    Copyright (c) 2003
 * Company: SAP Lans LLC, Palo Alto - all rights reserved
 * @author
 * @version 0.1
 */

public class PrivateAuctionMgrImpl
	extends AbstractBusinessObject
	implements PrivateAuctionManager {

	/** Static data block*/
	private static final Location tracer =
		Location.getLocation(PrivateAuctionManager.class.getName());
		
	/**
	 * Friends of this class
	 * Only this class knows who are its friends
	 * Each friend class is given a Hash which serves as a key to retreive
	 * the Friend interface from this Manager
	 */
	private static HashMap friends = new HashMap();
	static {
		friends.put(
			"com.sap.isa.auction.businessobject.order.AuctionOrderCreate",
			"com.sap.isa.auction.businessobject.order.AuctionOrderCreate");
			
		friends.put(
			"com.sap.isa.auction.services.b2x.scheduler.PublishAuctionTask",
			"com.sap.isa.auction.services.b2x.scheduler.PublishAuctionTask");
			
		friends.put(
			"com.sap.isa.auction.services.b2x.scheduler.CloseAuctionTask",
			"com.sap.isa.auction.services.b2x.scheduler.CloseAuctionTask");

		friends.put(
			"com.sap.isa.auction.services.b2x.scheduler.WinnerPollTask",
			"com.sap.isa.auction.services.b2x.scheduler.WinnerPollTask");
			
		friends.put(
			"com.sap.isa.auction.helpers.ManualWinnerHelper",
			"com.sap.isa.auction.helpers.ManualWinnerHelper");
	};

	private LinkedList backendTaskList = new LinkedList();
	private Thread backendTaskThread = null;
	
	public PrivateAuctionMgrImpl() {
	}

	/**
	 * All set calls are made before initialize is invoked
	 */
	public void initialize() {
		final String METHOD = "initialize";
		TraceHelper.entering(tracer, METHOD);
		super.initialize();
		TraceHelper.exiting(tracer, METHOD);
	}

	public void close() {
		final String METHOD = "close";
		TraceHelper.entering(tracer, METHOD);
		super.close();
		TraceHelper.exiting(tracer, METHOD);
	}

	protected Location getTracer() {
		return tracer;
	}

	/**
	 * @@todo:: check method level perms for user
	 */
	private void checkPermission(UserBase user, final String METHOD) {
		final String LOCAL_METHOD = "checkPermission" + METHOD;
		TraceHelper.entering(tracer, LOCAL_METHOD);
		
		TraceHelper.exiting(tracer, LOCAL_METHOD);
	}

	private void checkForModify(Auction obj) {
		final String METHOD = "checkForModify";
		TraceHelper.entering(tracer, METHOD);
		if (!obj.isRecording()) {
			ErrorMessageBundle bundle = ErrorMessageBundle.getInstance();
			I18nErrorMessage msg =
				bundle.newI18nErrorMessage(BOUserException.ILLEGAL_OPERATION);
			BOUserException ex = new BOUserException(msg);
			throwing(METHOD, ex);
			throw ex;
		}

		if (!(obj.getStatus() == AuctionStatusEnum.OPEN.getValue()
			|| obj.getStatus() == AuctionStatusEnum.DRAFT.getValue())) {
			ErrorMessageBundle bundle = ErrorMessageBundle.getInstance();
			I18nErrorMessage msg =
				bundle.newI18nErrorMessage(
					BOUserException.AUCTION_NOT_MODIFIABLE);
			BOUserException ex = new BOUserException(msg);
			throwing(METHOD, ex);
			throw ex;
		}
		TraceHelper.exiting(tracer, METHOD);
	}
	
	private void handleAndThrowException(Auction auction, final String METHOD, PersistenceManager pm, Exception ex) 
		throws RuntimeException {
		// roll back the necessary stuff
		//E.g. quoation creation needs to be rollback
		//Set the quotation exp to the current date
		
//		ByteArrayOutputStream output = new ByteArrayOutputStream();
//		PrintWriter writer = new PrintWriter(output);
//		ex.printStackTrace(writer);
//		writer.flush();
//		internalSendErrorNotification(auction, output.toString());
		
		try {
			if (pm != null && pm.currentTransaction().isActive()) {
				// rollback the tx
				pm.currentTransaction().rollback();
			}
		} catch (Exception jdoex) {
			LogHelper.logWarning(logger,tracer,"Rollback of Tx failed", jdoex);
		}
			
		// bubble
		if(ex instanceof AuctionRuntimeExceptionBase) {
			RuntimeException _ex = (AuctionRuntimeExceptionBase)ex;
			super.throwing(METHOD,_ex);
			throw _ex;
		}
			
		SystemException _ex = null;
		ErrorMessageBundle bundle = ErrorMessageBundle.getInstance();
		I18nErrorMessage msg = bundle.newI18nErrorMessage();

		if (ex instanceof JDOException) {
			msg.setPatternKey(SystemException.JDO_ERROR);
		} 
		else {
			msg.setPatternKey(SystemException.INTERNAL_ERROR);
		}
		_ex = new SystemException(msg, ex);
		super.throwing(METHOD, _ex);
		throw _ex;		
	}
	
	/**
	 * This method only create Auction in the standalone persistent datastore
	 * Auctions can only be created in DRAFT or OPEN Status
	 * If the Auction is a Template status is internally set to NONE
	 * and no validations are done
	 * No validations are done for DRAFT status
	 * LOGIC:: validate bean level attributes
	 *
	 * @throws  MissingContextExcetion,
	 *          ValidationException,
	 *          BOUserException,
	 *          InvalidStateException,
	 *          SystemException
	 */
	public void createAuction(Auction auction) {
		final String METHOD = "createAuction";
		TraceHelper.entering(tracer, METHOD);

		checkInitialized();

		InvocationContext ic = (InvocationContext)getInvocationContext();

		checkPermission(ic.getUser(), METHOD);

		if (auction.isTemplate()) {
			// set type and status to NONE for Template
			auction.setStatus(AuctionStatusEnum.NONE.getValue());
			auction.setType(AuctionTypeEnum.NONE.getValue());
		} else {
			// check that the status is either DRAFT or OPEn
			// override status to OPEN
			AuctionStatusEnum status =
				AuctionStatusEnum.toEnum(auction.getStatus());

			if (!(status == AuctionStatusEnum.DRAFT
				|| status == AuctionStatusEnum.OPEN)) {
				ErrorMessageBundle bundle = ErrorMessageBundle.getInstance();
				I18nErrorMessage msg =
					bundle.newI18nErrorMessage(
						BOUserException.INVALID_AUCTION_STATUS);
				msg.setArguments(
					new Object[] {
						auction.getAuctionName(),
						status.toString()});
				BOUserException ex = new BOUserException(msg);
				handleAndThrowException(auction, METHOD, null, ex);
			}
		}

//		ErrorMessageBundle bundle = ErrorMessageBundle.getInstance();
//		if (auction.getReferencedAuctionId() != null) {
//			I18nErrorMessage msg =
//				bundle.newI18nErrorMessage(ValidationException.INVALID_AUCTION);
//			BOUserException ex = new BOUserException(msg);
//			super.throwing(METHOD, ex);
//			throw ex;
//		}

		// @@todo:: Seller Id is also storing CRM user ID for quotation
		// creation to work. But this is redundant
		AuctionUserBaseData user = (AuctionUserBaseData)ic.getUser();
		if(	user.getSalesEmployee()!= null && 
			user.getSalesEmployee().getId() != null && 
			user.getSalesEmployee().getId().length() > 0 )
			auction.setSellerId(user.getSalesEmployee().getId());
		else //CRM case
			auction.setSellerId(user.getUserId());
		auction.setCreateDate(
			new java.sql.Timestamp(System.currentTimeMillis()));
		auction.setCreatedBy(user.getUserId().toUpperCase());
		auction.setQuotationDuration(0, 0 , 0);

		PrivateAuction privateAuction = (PrivateAuction) auction;
		privateAuction.setWebShopId(ic.getShopId());
		privateAuction.setCurrencyCode(ic.getShop().getCurrency());
		// remove hard coding of the country code to fetched
		privateAuction.setCountryCode("US");
		internalValidateAuction(privateAuction);

		internalValidateDynamicAttribute(privateAuction);
		
		internalValidateBusinessRule(privateAuction);
		
		
		if (logger.beInfo()) {
			String msg = "About to create the aucion with name " + auction.getAuctionName();
			logger.infoT(tracer, msg);
		}
		internalCreateAuction(privateAuction);
		TraceHelper.exiting(tracer, METHOD);
	}

	/* (non-Javadoc)
	 * @see com.sap.isa.auction.businessobject.AuctionManager#createAuctions(com.sap.isa.auction.bean.Auction[])
	 */
	public void createAuctions(Auction[] auctions) {
		throw new UnsupportedOperationException("Batch creation is not supported");
	}

	/**
		 * First one will be client
		 * Second one will System Id
		 * @return
		 */
	private String[] getClientAndSysId() {
		try {
			/**
			 * This is a valid assumption that there will be always the invocation context
			 * through which the auction is created
			 */
			InvocationContext invContext = (InvocationContext)ContextManager.getContext();
			AuctionUserBaseData user =
				(AuctionUserBaseData) invContext.getUser();
			String[] str = new String[2];
			str[0] = user.getClient();
			str[1] = user.getSystemId();
			return str;
		} catch (Exception e) {
			LogHelper.logError(logger,tracer,e);
			return null;
		}
	}

	/**
	  * Persistent store specific only
	  * @throws SystemException if a JDO Error is encountered
	  */
	private void internalCreateAuction(PrivateAuction auction) {
		final String METHOD = "internalCreateAuction";
		TraceHelper.entering(tracer, METHOD);
		PrivateAuctionDAO auctiondao = new PrivateAuctionDAO();
		auction.setAuctionId(OIDGenerator.newOID().toString());
		auctiondao.setAuctionId(auction.getAuctionId());
		/**
		 * This is a valid assumption that there will be always the invocation context
		 * through which the auction is created
		 */
		String[] str = getClientAndSysId();
		if (str != null) {
			auctiondao.setClient(str[0]);
			auctiondao.setSystemId(str[1]);
			auction.setClient(str[0]);
			auction.setSystemId(str[1]);
		}
		PersistenceManager pm = null;
		try {
			auction.setAuctionId(OIDGenerator.newOID().toString());
			DALBeanSynchronizer.synchAuctionDAOFromBean(
				auction,
				auctiondao,
				true);
			ServiceLocator serv = getServiceLocatorBase();
			PersistenceManagerFactory pmf = serv.getPersistenceManagerFactory();
			pm = pmf.getPersistenceManager();
			pm.currentTransaction().begin();
			auctiondao.setAuctionId(auction.getAuctionId());
			pm.makePersistent(auctiondao);
			pm.currentTransaction().commit();
			TraceHelper.exiting(tracer, METHOD);
		} 
		catch (Exception ex) {
			handleAndThrowException(auction, METHOD,pm,ex);
		} 
		catch (Throwable th) {
			th.printStackTrace();
		} 

		finally {
			// close the pm
			// Close the transaction
			if(pm!=null && pm.currentTransaction().isActive()) {
				pm.currentTransaction().rollback();
			}
			if (pm != null)
				pm.close();
		}
	}

	public Auction getAuctionById(String auctionId) {
		final String METHOD = "getAuctionById";
		TraceHelper.entering(tracer, METHOD);
		checkInitialized();

		InvocationContext ic = (InvocationContext)getInvocationContext();

		checkPermission(ic.getUser(), METHOD);
		TraceHelper.exiting(tracer, METHOD);
		return internalGetAuction(auctionId, false);
	}

	public Auction getAuctionTemplateById(String templateId) {
		final String METHOD = "getAuctionTemplateById";
		TraceHelper.entering(tracer, METHOD);

		checkInitialized();

		InvocationContext ic = (InvocationContext)getInvocationContext();

		checkPermission(ic.getUser(), METHOD);
		TraceHelper.exiting(tracer, METHOD);
		return internalGetAuction(templateId, true);
	}
	
	private PrivateAuctionDAO _getAuctionDAO(PersistenceManager pm,Query query,String auctionId,boolean auctionTemplate){
		final String METHOD = "_getAuctionDAO";
		StringBuffer filter = new StringBuffer("");
		filter.append("auctionId == ");
		filter.append("\"");
		filter.append(auctionId);
		filter.append("\"");
		filter.append(" && ");
		filter.append("isAuctionTemplate == ");
		/** @todo
		 *  Remove the hard coding */
		if (auctionTemplate)
			filter.append("1");
		else
			filter.append("-1");
		query.setFilter(filter.toString());
		Collection coll = (Collection) query.execute();
		// @@temp : end
		if (!coll.iterator().hasNext()) {
			logger.warningT(tracer,"Invalid Auction" + auctionId);
			TraceHelper.exiting(tracer, METHOD);
			return null;
		}
		return  (PrivateAuctionDAO) coll.iterator().next();
	}
	/**
	 * Persistent store specific only
	 * @throws SystemException if a JDO Error is encountered
	 */
	private PrivateAuction internalGetAuction(
		String auctionId,
		boolean auctionTemplate) {
		final String METHOD = "internalGetAuction";
		TraceHelper.entering(tracer, METHOD);
		PersistenceManager pm = null;
		PrivateAuction auction = null;
		try {
			ServiceLocator locator = getServiceLocatorBase();
			PersistenceManagerFactory pmf =
				locator.getPersistenceManagerFactory();
			pm = pmf.getPersistenceManager();
			auction = new PrivateAuction();
			Query query = pm.newQuery(PrivateAuctionDAO.class);
//			StringBuffer filter = new StringBuffer("");
//			filter.append("auctionId == ");
//			filter.append("\"");
//			filter.append(auctionId);
//			filter.append("\"");
//			filter.append(" && ");
//			filter.append("isAuctionTemplate == ");
//			/** @todo
//			 *  Remove the hard coding */
//			if (auctionTemplate)
//				filter.append("1");
//			else
//				filter.append("-1");
//			query.setFilter(filter.toString());
//			Collection coll = (Collection) query.execute();
//			// @@temp : end
//			if (!coll.iterator().hasNext()) {
//				logger.warningT(tracer,"Invalid Auction" + auctionId);
//				TraceHelper.exiting(tracer, METHOD);
//				return null;
//			}
//			PrivateAuctionDAO auctionDAO = (PrivateAuctionDAO) coll.iterator().next();
			
			PrivateAuctionDAO auctionDAO = _getAuctionDAO(pm,query,auctionId,auctionTemplate);
			if(auctionDAO == null) return null;
			// sync
			DALBeanSynchronizer.synchAuctionBeanFromDAO(auctionDAO, auction);
			query.closeAll();
			TraceHelper.exiting(tracer, METHOD);
			return auction;
		} catch (Exception ex) {
			handleAndThrowException(auction, METHOD, pm, ex);
		} finally {
			// close the pm
			if (pm != null)
				pm.close();
		}
		return null;
	}


	/**
	 * ACTIVE state is not supported by EBayAuctionManager
	 * Only Status Change is allowed in this method
	 * No Status change is allowed for Templates
	 * Explict check is done for the State Transition
	 * Possible State transitions:
	 * OPEN -> PUBLISHED
	 * OPEN -> CLOSED
	 * OPEN -> DELETED (Physical delete)
	 * OPEN -> WITHDRAWN -- not supported currently
	 *
	 * PUBLISHED -> CLOSED
	 * PUBLISHED -> FINALIZED -- internal
	 *
	 */
	public void modifyStatus(Auction auction, AuctionStatusEnum newStatus) {
		final String METHOD = "modifyStatus";
		TraceHelper.entering(tracer, METHOD);
		checkInitialized();

		InvocationContext ic = (InvocationContext)getInvocationContext();

		checkPermission(ic.getUser(), METHOD);

		AuctionStatusEnum oldStatus =
			AuctionStatusEnum.toEnum(auction.getStatus());

		// No Status change is allowed for Templates
		if (auction.isTemplate()) {
			ErrorMessageBundle bundle = ErrorMessageBundle.getInstance();
			I18nErrorMessage msg =
				bundle.newI18nErrorMessage(
					BOUserException.AUCTION_STATUS_CHANGE_NOT_ALLOWED);
			BOUserException ex = new BOUserException(msg);
			handleAndThrowException(auction, METHOD, null, ex);
		}

		AuctionStatus statusTransition =
			AuctionStatus.getAuctionStatus(oldStatus);
		PrivateAuction privateAuction = (PrivateAuction) auction;

		try {

			if (newStatus == AuctionStatusEnum.FINISHED) {
				AuctionStatusEnum aucStatusEnum = AuctionStatusEnum.toEnum(auction.getStatus());
				if(aucStatusEnum == AuctionStatusEnum.NOT_FINALIZED) return;
				// Get all the winners and check their quantities are the same as the ones
				// in the auction
				Winner[] winners  = bom.getWinnerManager().getWinners(auction);
				if(winners == null || winners.length == 0 ) return;
//				int totalQty = 0;
				for(int i = 0; i< winners.length;i++) {
					PrivateAuctionWinner winner = (PrivateAuctionWinner)winners[i];
					if(!winner.isCheckedOut()) return;
//					totalQty +=winner.getQuantity().intValue();
				}
				// Commenting out the quantity check for the transition to the finished status
//				if(totalQty< auction.getQuantity())  {
//					return;				
//				}
			}
			
			if (newStatus == AuctionStatusEnum.PUBLISHED) {
				privateAuction.setExecutionStatus(ExecutionStatusEnum.INPROCESS);
				internalPersistExecution(privateAuction);
				Result result =
					statusTransition.transitiontoFS(
						new FriendInterface(),
						privateAuction,
						newStatus);
				// persist the Execution request parameters
				internalPersistExecution(privateAuction);
				// bubble the exception
				if (privateAuction.getExecutionException() != null)
					throw privateAuction.getExecutionException();
			} else {
				statusTransition.transitionTo(
					new FriendInterface(),
					(PrivateAuction) auction,
					newStatus);
			}
			TraceHelper.exiting(tracer, METHOD);			
		} catch (Exception ex) {
			handleAndThrowException(auction, METHOD, null, ex);
		}
	}

	private void internalPersistExecution(PrivateAuction auction) {
		final String METHOD = "internalPersistExecution";
		TraceHelper.entering(tracer, METHOD);

		// Validate the auction status transition
		//        if(newStatus.equals(newStatus.)
		PersistenceManager pm = null;
		try {
			ServiceLocator locator = getServiceLocatorBase();
			PersistenceManagerFactory pmf =
				locator.getPersistenceManagerFactory();
			pm = pmf.getPersistenceManager();
			// @@temp: begin hack for JDO bug in loading of meta data
			Query query = pm.newQuery(PrivateAuctionDAO.class);
			query.compile();
			query.closeAll();
			// @@temp : end
			PrivateAuctionDAO auctionDAO =
				(PrivateAuctionDAO) pm.getObjectById(
			new AuctionDAO.Id(auction.getAuctionId()),
					false);
			// Log Warning
			if (auctionDAO == null) {
				ErrorMessageBundle bundle = ErrorMessageBundle.getInstance();
				I18nErrorMessage msg =
					bundle.newI18nErrorMessage(
						ValidationException.INVALID_AUCTION);
				ValidationException ex = new ValidationException(auction, msg);
				handleAndThrowException(auction, METHOD, pm, ex);
			}
			pm.currentTransaction().begin();
			// sync
			auctionDAO.setExecutionRequestId(auction.getExecutionRequestId());
			auctionDAO.setExecutionStatus(auction.getExecutionStatus());
			pm.makePersistent(auctionDAO);
			pm.currentTransaction().commit();
			TraceHelper.exiting(tracer, METHOD);
		} catch (Exception ex) {
			handleAndThrowException(auction, METHOD, pm, ex);
		} finally {
			// close the pm
			if (pm != null)
				pm.close();
		}
	}

	private void internalModifyStatus(
		PrivateAuction auction,
		AuctionStatusEnum newStatus) {
		final String METHOD = "internalModifyStatus";
		TraceHelper.entering(tracer, METHOD);
		// Validate the auction status transition
		//        if(newStatus.equals(newStatus.)
		PersistenceManager pm = null;
		Timestamp modDate = new java.sql.Timestamp(System.currentTimeMillis());
		try {
			ServiceLocator locator = getServiceLocatorBase();
			PersistenceManagerFactory pmf =
				locator.getPersistenceManagerFactory();
			pm = pmf.getPersistenceManager();
			// @@temp: begin hack for JDO bug in loading of meta data
			Query query = pm.newQuery(PrivateAuctionDAO.class);
			query.compile();
			query.closeAll();
			// @@temp : end
			PrivateAuctionDAO auctionDAO =
				(PrivateAuctionDAO) pm.getObjectById(
			new AuctionDAO.Id(auction.getAuctionId()),
					false);
			// Log Warning
			if (auctionDAO == null) {
				logger.errorT(tracer,"Invalid Auction" + auction.getAuctionId());
				ErrorMessageBundle bundle = ErrorMessageBundle.getInstance();
				I18nErrorMessage msg =
					bundle.newI18nErrorMessage(
						ValidationException.INVALID_AUCTION);
				ValidationException ex = new ValidationException(auction, msg);
				handleAndThrowException(auction, METHOD, pm, ex);
			}
			pm.currentTransaction().begin();
			// sync
			//auctionDAO.setScheduleDate(auction.getStartDate());
			if(newStatus == AuctionStatusEnum.PUBLISHED) {
				Timestamp startDate = auction.getStartDate();
				Timestamp now = new Timestamp(System.currentTimeMillis());
				if(startDate == null || startDate.compareTo(now) < 0) {
					auction.setStartDate(now);
					//auction.setPublishDate(now);
				}
			}else if(newStatus == AuctionStatusEnum.CLOSED) {
				Timestamp now = new Timestamp(System.currentTimeMillis());
				auction.setEndDate(now);
			}

			auctionDAO.setPublishDate(auction.getPublishDate());
			//if(auction.getStartDate()!=null)
			auctionDAO.setStartDate(auction.getStartDate());
			//else if(auction.getPublishDate()!=null){ 
			//}
			//auctionDAO.setDuration(auction.getDuration());
			auctionDAO.setEndDate(auction.getEndDate());

			auctionDAO.setStatus(newStatus.getValue());
			//auctionDAO.setEbayAuctionId(auction.getItemId());
			auctionDAO.setQuotationId(auction.getQuotationId());
			auctionDAO.setModifiedDate(modDate);

			pm.makePersistent(auctionDAO);
			pm.currentTransaction().commit();
			auction.setStatus(newStatus.getValue()); // set to new status
			auction.setLastModifiedDate(modDate);
			TraceHelper.exiting(tracer, METHOD);
		} catch (Exception ex) {
			handleAndThrowException(auction, METHOD,pm,ex);
		} finally {
			// close the pm
			if (pm != null)
				pm.close();
		}
	}

	/**
	 * Auction Status must be DRAFT or OPEN for modify to happen
	 * No Status change allowed, here, explicit check is done against old status
	 *
	 * @throws MissingContextException
	 *          BOUserException
	 *          ValidationException
	 *          SystemException
	 */
	public void modifyAuction(Auction auction) {
		final String METHOD = "modifyAuction";
		TraceHelper.entering(tracer, METHOD);
		checkInitialized();

		InvocationContext ic = (InvocationContext)getInvocationContext();

		checkPermission(ic.getUser(), METHOD);

		checkForModify(auction);

		// check that the status is not modified
		Auction savedAuction =
			internalGetAuction(auction.getAuctionId(), auction.isTemplate());
		if (savedAuction.getStatus() != auction.getStatus()) {
			ErrorMessageBundle bundle = ErrorMessageBundle.getInstance();
			I18nErrorMessage msg =
				bundle.newI18nErrorMessage(
					BOUserException.AUCTION_STATUS_CHANGE_NOT_ALLOWED);
			BOUserException ex = new BOUserException(msg);
			handleAndThrowException(auction, METHOD, null, ex);
		}

		AuctionStatusEnum status =
			AuctionStatusEnum.toEnum(auction.getStatus());
		// Modification are allowed only for NONE or DRAFT or OPEN status
		if (!(status == AuctionStatusEnum.NONE
			|| status == AuctionStatusEnum.OPEN
			|| status == AuctionStatusEnum.DRAFT)) {
			ErrorMessageBundle bundle = ErrorMessageBundle.getInstance();
			I18nErrorMessage msg =
				bundle.newI18nErrorMessage(
					BOUserException.AUCTION_NOT_MODIFIABLE);
			BOUserException ex = new BOUserException(msg);
			handleAndThrowException(auction, METHOD, null, ex);
		}

		PrivateAuction privateAuction = (PrivateAuction) auction;

		internalValidateAuction(privateAuction);

		internalValidateDynamicAttribute(privateAuction);
		
		internalValidateBusinessRule(privateAuction);
		
		if (logger.beInfo()) {
			String msg = "About to modify the auction with name " + auction.getAuctionName();
			logger.infoT(tracer, msg);
		}
		internalModifyAuction(privateAuction);

		// end the recording
		auction.endRecording();
		TraceHelper.exiting(tracer, METHOD);
	}

	/**
	 * Persistent store specific only
	 * @throws SystemException if a JDO Error is encountered
	 */
	private void internalModifyAuction(PrivateAuction auction) {
		final String METHOD = "internalModifyAuction";
		TraceHelper.entering(tracer, METHOD);
		PersistenceManager pm = null;

		Timestamp modDate = new java.sql.Timestamp(System.currentTimeMillis());
		try {
			ServiceLocator locator = getServiceLocatorBase();
			PersistenceManagerFactory pmf =
				locator.getPersistenceManagerFactory();
			pm = pmf.getPersistenceManager();
			// @@temp: begin hack for JDO bug in loading of meta data
			Query query = pm.newQuery(PrivateAuctionDAO.class);
			// @@temp : end
			PrivateAuctionDAO auctionDAO = _getAuctionDAO(pm,query,auction.getAuctionId(),false);
			// Log Warning
			if (auctionDAO == null) {
				logger.errorT(tracer,"Invalid Auction " + auction.getAuctionId());
				ErrorMessageBundle bundle = ErrorMessageBundle.getInstance();
				I18nErrorMessage msg =
					bundle.newI18nErrorMessage(
						ValidationException.INVALID_AUCTION);
				ValidationException ex = new ValidationException(auction, msg);
				handleAndThrowException(auction, METHOD, pm, ex);
			}
			// sync
			pm.currentTransaction().begin();

			// Setting the currency code

			DALBeanSynchronizer.synchAuctionDAOFromBean(
				auction,
				auctionDAO,
				false);
			auctionDAO.setModifiedDate(modDate); // set the Modification date

			internalSynchAuctionItems(auction, pm, auctionDAO);
			internalSynchAuctionBPs(auction,pm,auctionDAO);
			internalSynchAuctionTargetGroups(auction,pm,auctionDAO);

			// add to Tx
			pm.makePersistent(auctionDAO);
			pm.currentTransaction().commit();
			query.closeAll();
			auction.setLastModifiedDate(modDate);
			TraceHelper.exiting(tracer, METHOD);
		} catch (Exception ex) {
			handleAndThrowException(auction, METHOD,pm,ex);
		} finally {
			// close the pm
			if (pm != null) {
				if(pm.currentTransaction().isActive()){
					pm.currentTransaction().rollback();
				}
				pm.close();
			}
		}
	}
	


	private void internalSynchAuctionItems(
		PrivateAuction auction,
		PersistenceManager pm,
		PrivateAuctionDAO auctionDAO) {
		final String METHOD = "internalSynchAuctionItems";
		TraceHelper.entering(tracer, METHOD);
		Collection addedItems = null;
		Collection removedItems = null;
		// check first that the items attribute have been modified i.e added or removed
		if (auction.isModified(Auction.ATTR_ITEMS)) {
			// handle Auction Items
			TrackedArrayList trackedItems =
				(TrackedArrayList) auction.getNewAttributeValue(
					Auction.ATTR_ITEMS);
			addedItems = trackedItems.getAddedItems();
			if (addedItems != null) {
				Iterator it = addedItems.iterator();
				while (it.hasNext()) {
					PrivateAuctionItem item = (PrivateAuctionItem) it.next();
					PrivateAuctionItemDAO itemDAO =
						new PrivateAuctionItemDAO(OIDGenerator.newOID());
					DALBeanSynchronizer.synchAuctionItemDAOFromBean(
						auctionDAO,item,
						itemDAO,true);
					pm.makePersistent(itemDAO);
					auctionDAO.addAuctionItem(itemDAO);
				}
			}
			removedItems = trackedItems.getRemovedItems();
			if (removedItems != null) {
				Iterator it = removedItems.iterator();
				while (it.hasNext()) {
					PrivateAuctionItem item = (PrivateAuctionItem) it.next();
					PrivateAuctionItemDAO itemDAO =
						auctionDAO.getAuctionItem(item.getId());
					if (itemDAO != null) {
						pm.deletePersistent(itemDAO);
						auctionDAO.removeAuctionItem(itemDAO);
					}
				}
			}
		}

		List items = auction.getItems();
		/**
		 * Existing items may be modified, exclude added items
		 */
		for (int i = 0; i < items.size(); i++) {
			Object o = items.get(i);
			if ((addedItems == null)
				|| (addedItems != null && !addedItems.contains(o))) {
				PrivateAuctionItem item = (PrivateAuctionItem) o;
				// if the item is modified
				if (item.isRecording()) {
					PrivateAuctionItemDAO itemDAO =
						auctionDAO.getAuctionItem(item.getId());
					DALBeanSynchronizer.synchAuctionItemDAOFromBean(
						auctionDAO,item,
						itemDAO,false);
					pm.makePersistent(itemDAO);
				}
			}
		}
		TraceHelper.exiting(tracer, METHOD);
	}
	

	private void internalSynchAuctionTargetGroups(
		PrivateAuction auction,
		PersistenceManager pm,
		PrivateAuctionDAO auctionDAO) {
		final String METHOD = "internalSynchAuctionTargetGroups";
		TraceHelper.entering(tracer, METHOD);
		Collection addedItems = null;
		Collection removedItems = null;
		// check first that the items attribute have been modified i.e added or removed
		if (auction.isModified(Auction.ATTR_TGT_GRPS)) {
			// handle Auction Items
			TrackedArrayList trackedItems =
				(TrackedArrayList) auction.getNewAttributeValue(
					Auction.ATTR_TGT_GRPS);
			addedItems = trackedItems.getAddedItems();
			if (addedItems != null) {
				Iterator it = addedItems.iterator();
				while (it.hasNext()) {
					String tg = (String) it.next();
					TargetGrpDAO itemDAO =
						new TargetGrpDAO(OIDGenerator.newOID());
					DALBeanSynchronizer.synchTgtDAOFromAuction(itemDAO,
						tg,(PrivateAuction)auction,
						true);
					pm.makePersistent(itemDAO);
					auctionDAO.addTargetGrp(itemDAO);
				}
			}
			removedItems = trackedItems.getRemovedItems();
			if (removedItems != null) {
				Iterator it = removedItems.iterator();
				while (it.hasNext()) {
					String tg = (String) it.next();
					TargetGrpDAO itemDAO =
						auctionDAO.getTargetGrp(tg,true);
					if (itemDAO != null) {
						pm.deletePersistent(itemDAO);
						auctionDAO.removeTargetGrp(itemDAO);
					}
				}
			}
		}

//		List items = auction.getTargetGroups();
//		/**
//		 * Existing items may be modified, exclude added items
//		 */
//		for (int i = 0; i < items.size(); i++) {
//			Object o = items.get(i);
//			if ((addedItems == null)
//				|| (addedItems != null && !addedItems.contains(o))) {
//				String item = (String) o;
//				TargetGrpDAO itemDAO =
//					auctionDAO.getTargetGrp(item,true);
//				DALBeanSynchronizer.synchTgtDAOFromAuction(itemDAO,item,auction,true);
//				pm.makePersistent(itemDAO);
//			}
//		}
		TraceHelper.exiting(tracer, METHOD);
	}
	
	private void internalSynchAuctionBPs(
		PrivateAuction auction,
		PersistenceManager pm,
		PrivateAuctionDAO auctionDAO) {
		final String METHOD = "internalSynchAuctionBPs";
		TraceHelper.entering(tracer, METHOD);
		Collection addedItems = null;
		Collection removedItems = null;
		// check first that the items attribute have been modified i.e added or removed
		if (auction.isModified(Auction.ATTR_BPS)) {
			// handle Auction Items
			TrackedArrayList trackedItems =
				(TrackedArrayList) auction.getNewAttributeValue(
					Auction.ATTR_BPS);
			addedItems = trackedItems.getAddedItems();
			if (addedItems != null) {
				Iterator it = addedItems.iterator();
				while (it.hasNext()) {
					String tg = (String) it.next();
					TargetGrpDAO itemDAO =
						new TargetGrpDAO(OIDGenerator.newOID());
					DALBeanSynchronizer.synchTgtDAOFromAuction(itemDAO,
						tg,(PrivateAuction)auction,
						true);
					pm.makePersistent(itemDAO);
					auctionDAO.addTargetGrp(itemDAO);
				}
			}
			removedItems = trackedItems.getRemovedItems();
			if (removedItems != null) {
				Iterator it = removedItems.iterator();
				while (it.hasNext()) {
					String tg = (String) it.next();
					TargetGrpDAO itemDAO =
						auctionDAO.getTargetGrp(tg,false);
					if (itemDAO != null) {
						pm.deletePersistent(itemDAO);
						auctionDAO.removeTargetGrp(itemDAO);
					}
				}
			}
		}

//		List items = auction.getBusinessPartners();
//		/**
//		 * Existing items may be modified, exclude added items
//		 */
//		for (int i = 0; i < items.size(); i++) {
//			Object o = items.get(i);
//			if ((addedItems == null)
//				|| (addedItems != null && !addedItems.contains(o))) {
//				String item = (String) o;
//				TargetGrpDAO itemDAO =
//					auctionDAO.getTargetGrp(item,false);
//				DALBeanSynchronizer.synchTgtDAOFromAuction(itemDAO,item,auction,false);
//				pm.makePersistent(itemDAO);
//			}
//		}
		TraceHelper.exiting(tracer, METHOD);
	}	
	
		
	/**
	 * This operation allows to Permanently delete Draft or Open Auctions
	 * from Persistent Store
	 * Published Auction cannot be deleted. They can only be WITHDRAWN
	 * Auction can only be deleted if it's Status is DRAFT or OPEN
	 *
	 * @throws BOUserException
	 *         MissingContextException
	 *         SystemException
	 */
	public void deleteAuction(Auction auction) {
		final String METHOD = "deleteAuction";
		TraceHelper.entering(tracer, METHOD);

		checkInitialized();

		InvocationContext ic = (InvocationContext)getInvocationContext();

		checkPermission(ic.getUser(), METHOD);

		if (!(auction.getStatus() == AuctionStatusEnum.OPEN.getValue())) {
			ErrorMessageBundle bundle = ErrorMessageBundle.getInstance();
			I18nErrorMessage msg =
				bundle.newI18nErrorMessage(BOUserException.ILLEGAL_OPERATION);
			BOUserException ex = new BOUserException(msg);
			handleAndThrowException(auction, METHOD,null,ex);
		}

		internalDeleteAuction((PrivateAuction) auction);
		TraceHelper.exiting(tracer, METHOD);
	}

	/**
	 * Persistent store specific only
	 * @throws SystemException if a JDO Error is encountered
	 */
	private void internalDeleteAuction(PrivateAuction auction) {
		final String METHOD = "internalDeleteAuction";
		TraceHelper.entering(tracer, METHOD);

		PrivateAuctionDAO auctiondao = new PrivateAuctionDAO();
		auction.setAuctionId(OIDGenerator.newOID().toString());
		auctiondao.setAuctionId(auction.getAuctionId());
		DALBeanSynchronizer.synchAuctionDAOFromBean(auction, auctiondao, true);
		ServiceLocator serv = getServiceLocatorBase();
		PersistenceManager pm = null;
		try {
			PersistenceManagerFactory pmf = serv.getPersistenceManagerFactory();
			pm = pmf.getPersistenceManager();
			pm.currentTransaction().begin();
			pm.deletePersistent(auctiondao);
			pm.currentTransaction().commit();
		} catch (Exception ex) {
			handleAndThrowException(auction, METHOD,pm,ex);
		} finally {
			// close the pm
			if (pm != null)
				pm.close();
		}
		TraceHelper.exiting(tracer, METHOD);
	}

	/**
	 * Template is set to true
	 * Status is set to DRAFT
	 */
	public void createAuctionTemplate(Auction auction) {
		final String METHOD = "createAuctionTemplate";
		TraceHelper.entering(tracer, METHOD);
		InvocationContext ic = (InvocationContext)getInvocationContext();

		checkPermission(ic.getUser(), METHOD);

		// override status to DRAFT
		auction.setStatus(AuctionStatusEnum.DRAFT.getValue());
		auction.setTemplate(true);
		AuctionUserBaseData user = (AuctionUserBaseData)ic.getUser();
		if(	user.getSalesEmployee()!= null && 
			user.getSalesEmployee().getId() != null && 
			user.getSalesEmployee().getId().length() > 0 )
			auction.setSellerId(user.getSalesEmployee().getId());
		else //CRM case
			auction.setSellerId(user.getUserId());
		auction.setCreateDate(
			new java.sql.Timestamp(System.currentTimeMillis()));
		auction.setCreatedBy(user.getUserId().toUpperCase());

		PrivateAuction privateAuction = (PrivateAuction)auction;
		privateAuction.setWebShopId(ic.getShopId());
		
		internalValidateAuction(privateAuction);

		internalCreateAuction(privateAuction);
		TraceHelper.exiting(tracer, METHOD);
	}

	/**
	 * Status should remain DRAFT and should not be modified
	 */
	public void modifyAuctionTemplate(Auction auction) {
		final String METHOD = "modifyAuctionTemplate";
		TraceHelper.entering(tracer, METHOD);
		if (!(auction.getStatus() == AuctionStatusEnum.DRAFT.getValue())) {
			ErrorMessageBundle bundle = ErrorMessageBundle.getInstance();
			I18nErrorMessage msg =
				bundle.newI18nErrorMessage(
					BOUserException.AUCTION_NOT_MODIFIABLE);
			BOUserException ex = new BOUserException(msg);
			handleAndThrowException(auction, METHOD, null, ex);
		}

		PrivateAuction privateAuction = (PrivateAuction)auction;
		
		internalValidateAuction(privateAuction);

		internalModifyAuction(privateAuction);
		TraceHelper.exiting(tracer, METHOD);
	}

	void deleteAuctionTemplate(Auction auction) {
		final String METHOD = "deleteAuctionTemplate";
		TraceHelper.entering(tracer, METHOD);
		checkInitialized();

		InvocationContext ic = (InvocationContext)getInvocationContext();

		checkPermission(ic.getUser(), METHOD);

		if (!(auction.isTemplate()
			&& auction.getStatus() == AuctionStatusEnum.DRAFT.getValue())) {
			ErrorMessageBundle bundle = ErrorMessageBundle.getInstance();
			I18nErrorMessage msg =
				bundle.newI18nErrorMessage(BOUserException.ILLEGAL_OPERATION);
			BOUserException ex = new BOUserException(msg);
			handleAndThrowException(auction, METHOD, null, ex);
		}

		internalDeleteAuction((PrivateAuction) auction);
		TraceHelper.exiting(tracer, METHOD);
	}

	/**
	 * Search specific functions. Only one API
	 * Order is important. Based on last synchronization with eBay
	 */

	public QueryResult searchAuctions(QueryFilter filter) {
		final String METHOD = "searchAuctions";
		TraceHelper.entering(tracer, METHOD);
		checkInitialized();
		InvocationContext ic = (InvocationContext)getInvocationContext();

		checkPermission(ic.getUser(), METHOD);
		ServiceLocator locator = getServiceLocatorBase();
		QueryResult result = null;
		PersistenceManager pm = null;
		boolean error = false;
		try {
			PersistenceManagerFactory pmf =
				locator.getPersistenceManagerFactory();
			pm = pmf.getPersistenceManager();
			Query query = pm.newQuery(PrivateAuctionDAO.class);
			StringBuffer queryFilter = new StringBuffer();
			StringBuffer vars = new StringBuffer();
			Iterator it = filter.getCriterias().iterator();
			InvocationContext context = (InvocationContext)ContextManager.getContext();
			String[] str = getClientAndSysId();
			queryFilter.append("(client == \"" + str[0] + "\")");
			queryFilter.append(" && (systemId == \"" + str[1] + "\")");
			while (it.hasNext()) {
				//Expecting only criteria as of now
				com.sap.isa.auction.bean.search.Criteria crit =
					(com.sap.isa.auction.bean.search.Criteria) it.next();
				if (crit.name.equals(AuctionQueryFilter.WEBSHOP_ID)) {
					// special handling of name if web shop Id is present
					if (queryFilter.length() > 0) {
						queryFilter.append(" && ");
					}
					generateQuery(
						queryFilter,
						vars,
						"webShopId",
						String.class,
						crit,
						false,
						null,
						null);
				} else if (crit.name.equals(AuctionQueryFilter.SALESAREA)) {
					// special handling of name if web shop Id is present
					if (queryFilter.length() > 0) {
						queryFilter.append(" && ");
					}
					generateQuery(
						queryFilter,
						vars,
						"salesArea",
						String.class,
						crit,
						false,
						null,
						null);
				} else if (crit.name.equals(AuctionQueryFilter.CREATED_BY)) {
					// special handling of name if web shop Id is present
					if (queryFilter.length() > 0) {
						queryFilter.append(" && ");
					}
					generateQuery(
						queryFilter,
						vars,
						"createdBy",
						String.class,
						crit,
						false,
						null,
						null);
				} else if (crit.name.equals(AuctionQueryFilter.NAME)) {
					// special handling of name if web shop Id is present
					if (queryFilter.length() > 0) {
						queryFilter.append(" && ");
					}
					generateQuery(
						queryFilter,
						vars,
						"auctionName",
						String.class,
						crit,
						false,
						null,
						null);
				} else if (crit.name.equals(AuctionQueryFilter.END_DATE)) {
					// special handling of name if web shop Id is present
					if (queryFilter.length() > 0) {
						queryFilter.append(" && ");
					}
					generateQuery(
						queryFilter,
						vars,
						"endDate",
						Date.class,
						crit,
						false,
						null,
						null);
				} else if (crit.name.equals(AuctionQueryFilter.START_DATE)) {
					// special handling of name if web shop Id is present
					if (queryFilter.length() > 0) {
						queryFilter.append(" && ");
					}
					generateQuery(
						queryFilter,
						vars,
						"startDate",
						Date.class,
						crit,
						false,
						null,
						null);
				} else if (crit.name.equals(AuctionQueryFilter.PUBLISH_DATE)) {
					// special handling of name if web shop Id is present
					if (queryFilter.length() > 0) {
						queryFilter.append(" && ");
					}
					generateQuery(
						queryFilter,
						vars,
						"publishDate",
						Date.class,
						crit,
						false,
						null,
						null);
				} else if (crit.name.equals(AuctionQueryFilter.CREATE_DATE)) {
					// special handling of name if web shop Id is present
					if (queryFilter.length() > 0) {
						queryFilter.append(" && ");
					}
					generateQuery(
						queryFilter,
						vars,
						"createdDate",
						Date.class,
						crit,
						false,
						null,
						null);
				} else if (crit.name.equals(AuctionQueryFilter.PRODUCT_ID)) {
					// special handling of name if web shop Id is present
					if (queryFilter.length() > 0) {
						queryFilter.append(" && ");
					}
					generateQuery(
						queryFilter,
						vars,
						"auctionItems",
						String.class,
						crit,
						true,
						"AuctionItemDAO",
						"productCode");
				} else if (crit.name.equals(AuctionQueryFilter.STATUS)) {
					// special handling of name if web shop Id is present
					if (queryFilter.length() > 0) {
						queryFilter.append(" && ");
					}
					generateQuery(
						queryFilter,
						vars,
						"status",
						Integer.class,
						crit,
						false,
						null,
						null);
				} else if (crit.name.equals(AuctionQueryFilter.CHECKOUT_STATUS)) {
					if (queryFilter.length() > 0) {
						queryFilter.append(" && ");
					}
					generateQuery(
						queryFilter,
						vars,
						"transactions",
						Integer.class,
						crit,
						true,
						"BankTransactionDAO",
						"transactionStatus");

				} else if (crit.name.equals(AuctionQueryFilter.PUBLISH_ERROR)) {
					if(crit.rhs instanceof Boolean && ((Boolean)crit.rhs).booleanValue()) {
						if (queryFilter.length() > 0) {
							queryFilter.append(" && ");
						}
					
						final String attrName = "execStatus";
						QueryFilter.EqualToCriteria crit1 = new QueryFilter.EqualToCriteria();
						crit1.name = attrName;
						crit1.rhs = new Integer(ExecutionStatusEnum.SYSTEM_ERROR.getValue());

						QueryFilter.EqualToCriteria crit2 = new QueryFilter.EqualToCriteria();
						crit2.name = attrName;
						crit2.rhs = new Integer(ExecutionStatusEnum.USER_ERROR.getValue());
						
						queryFilter.append(" ( ");
						generateQuery(
							queryFilter,
							vars,
							attrName,
							Integer.class,
							crit1,
							false,
							null,
							null);
						
						queryFilter.append(" || ");
					
						generateQuery(
							queryFilter,
							vars,
							attrName,
							Integer.class,
							crit2,
							false,
							null,
							null);
						queryFilter.append(" ) ");
					}
				}
				else if (crit.name.equals(AuctionQueryFilter.TYPE)) {
					// special handling of name if web shop Id is present
					if (queryFilter.length() > 0) {
						queryFilter.append(" && ");
					}
					generateQuery(
						queryFilter,
						vars,
						"auctionType",
						Integer.class,
						crit,
						false,
						null,
						null);
				}
			}
			StringBuffer orderByStr = new StringBuffer();
			if (str != null) {
				for (int i = 0; i < str.length; i++) {
					if (orderByStr.length() > 0)
						orderByStr.append(" , ");
					if (AuctionQueryFilter.STATUS.equals(str[i])) {
						orderByStr.append(
							"status "
								+ (filter.isAscending()
									? "ascending"
									: "descending"));
					} else if (AuctionQueryFilter.NAME.equals(str[i])) {
						orderByStr.append(
							"auctionName "
								+ (filter.isAscending()
									? "ascending"
									: "descending"));
					} else if (AuctionQueryFilter.START_DATE.equals(str[i])) {
						orderByStr.append(
							"startDate "
								+ (filter.isAscending()
									? "ascending"
									: "descending"));
					} else if (
						AuctionQueryFilter.PUBLISH_DATE.equals(str[i])) {
						orderByStr.append(
							"publishDate "
								+ (filter.isAscending()
									? "ascending"
									: "descending"));
					} else if (AuctionQueryFilter.END_DATE.equals(str[i])) {
						orderByStr.append(
							"endDate "
								+ (filter.isAscending()
									? "ascending"
									: "descending"));
					} else if (AuctionQueryFilter.CREATE_DATE.equals(str[i])) {
						orderByStr.append(
							"createdDate "
								+ (filter.isAscending()
									? "ascending"
									: "descending"));
					}
				}
			}
			if (logger.beInfo()) {
				String msg = "In EbayAucMgr SearchQuery -->Variables are "
					+ vars.toString();
				logger.infoT(tracer, msg);
				msg = "In EbayAucMgr SearchQuery --> QueryFilter is "
					+ queryFilter.toString();
				logger.infoT(tracer, msg);
			}

			query.declareVariables(vars.toString());
			query.setFilter(queryFilter.toString());
			if (orderByStr.length() > 1)
				query.setOrdering(orderByStr.toString());
			Collection coll = (Collection) query.execute();

			int size = 0;
			Iterator collIt = coll.iterator();
			while (collIt.hasNext()) {
				collIt.next();
				size++;
			}
			result =
				new PrivateAuctionJDOQueryResult(
					pm,
					query,
					coll,
					size,
					getServiceLocatorBase().getRecordingCallManagerFactory());
			TraceHelper.exiting(tracer, METHOD);
			return result;
		} catch (Exception ex) {
			handleAndThrowException(null, METHOD, pm, ex);
		}
		finally {
			// In case of error PM must be closed else it will lead to Memory leak
			if(error && pm != null && !pm.isClosed()) pm.close();
		}
		return null;
	}

	/**
	 * @@todo:: to be implemented, defaulted to modifyAuction
	 * No Synchronization is done is the Auction is still in Open State
	 */
	public void modifyAuctionWOSynchronization(Auction auction) {
		final String METHOD = "modifyAuctionWOSynchronization";
		TraceHelper.entering(tracer, METHOD);
		modifyAuction(auction);
		TraceHelper.exiting(tracer, METHOD);
	}

	/**
	 * @@todo:: to be implemented, defaulted to modifyAuction
	 */
	public void modifyStatusWOSynchronization(
		Auction auction,
		AuctionStatusEnum status) {
		final String METHOD = "modifyStatusWOSynchronization";
		TraceHelper.entering(tracer, METHOD);
		modifyStatus(auction, status);
		TraceHelper.exiting(tracer, METHOD);
	}

	/**
	 * User specific scheduling of bkgd synchronization of the Auction
	 * specific changes.
	 *
	 * Only one per Seller allowed
	 *
	 */
//	public void setSynchronization(Synchronization sync) {
//		final String METHOD = "setSynchronization";
//		TraceHelper.entering(tracer, METHOD);
//		checkInitialized();
//
//		InvocationContext ic = getInvocationContext();
//
//		checkPermission(ic.getUser(), METHOD);
//
//		ServiceLocator serv = getServiceLocatorBase();
//		try {
//			BackgroundJobManager jobMgr = serv.getBackgroundJobManager();
//			jobMgr.addSynchronization(sync, ic.getUser().getUserId());
//			TraceHelper.exiting(tracer, METHOD);
//		} catch (Exception jdoex) {
//			ErrorMessageBundle bundle = ErrorMessageBundle.getInstance();
//			I18nErrorMessage msg = bundle.newI18nErrorMessage();
//			SystemException ex = new SystemException(msg, jdoex);
//			super.throwing(METHOD, jdoex);
//			throw ex;
//		}
//	}

//	public Synchronization getSynchronization() {
//		final String METHOD = "getSynchronization";
//		TraceHelper.entering(tracer, METHOD);
//		checkInitialized();
//
//		InvocationContext ic = getInvocationContext();
//
//		checkPermission(ic.getUser(), METHOD);
//
//		ServiceLocator serv = getServiceLocatorBase();
//		try {
//			BackgroundJobManager jobMgr = serv.getBackgroundJobManager();
//			TraceHelper.exiting(tracer, METHOD);
//			return jobMgr.getSynchronization(ic.getUser().getUserId());
//		} catch (Exception jdoex) {
//			ErrorMessageBundle bundle = ErrorMessageBundle.getInstance();
//			I18nErrorMessage msg = bundle.newI18nErrorMessage();
//			SystemException ex = new SystemException(msg, jdoex);
//			super.throwing(METHOD, jdoex);
//			throw ex;
//		}
//	}

	/**
	 * Cancels the existing scheduling of batch processing
	 */
//	public void cancelSychronization(Synchronization sync) {
//		final String METHOD = "cancelSychronization";
//		TraceHelper.entering(tracer, METHOD);
//		checkInitialized();
//
//		InvocationContext ic = getInvocationContext();
//
//		checkPermission(ic.getUser(), METHOD);
//
//		ServiceLocator serv = getServiceLocatorBase();
//		try {
//			BackgroundJobManager jobMgr = serv.getBackgroundJobManager();
//			jobMgr.cancelSynchronization(ic.getUser().getUserId());
//			TraceHelper.exiting(tracer, METHOD);
//		} catch (Exception jdoex) {
//			ErrorMessageBundle bundle = ErrorMessageBundle.getInstance();
//			I18nErrorMessage msg =
//				bundle.newI18nErrorMessage(SystemException.INTERNAL_ERROR);
//			SystemException ex = new SystemException(msg, jdoex);
//			super.throwing(METHOD, jdoex);
//			throw ex;
//		}
//	}

	/**
	 * List of Auctions to which user made changes but they are not yet synchronized
	 * with eBay
	 */
	public Auction[] getPendingSynchronization() {
		final String METHOD = "getPendingSynchronization";
		TraceHelper.entering(tracer, METHOD);
		checkInitialized();

		InvocationContext ic = (InvocationContext)getInvocationContext();

		checkPermission(ic.getUser(), METHOD);
		TraceHelper.exiting(tracer, METHOD);
		/** @todo The algorithm can be as simple as the looking up the local database for
		 * the auctions*/
		return null;
	}

	/**
	 * Manual batch processing of Auction Synchronization Task
	 * Logic:
	 * If Synchronization Task is set,
	 *      If Task is executing throw Concurrent Exception
	 *      else Pause the Synchronization Task
	 * Trigger the Pending Synchronizations in async mode
	 * Notify callbacks
	 * Resume the Synchronization Task is paused earlier
	 *
	 * This call will trigger the pending synchronizations now and return immediately
	 * @param callback provides the reporting on results of callback. It is made
	 * on exactly the same instance as provided in the class
	 */
	public AsyncResult triggerPendingSynchronization(
		Auction[] auctions,
		AsyncCallback[] callbacks) {
		final String METHOD = "triggerPendingSynchronization";
		TraceHelper.entering(tracer, METHOD);
		checkInitialized();

		InvocationContext ic = (InvocationContext)getInvocationContext();

		checkPermission(ic.getUser(), METHOD);
		TraceHelper.exiting(tracer, METHOD);
		return null;
	}

	/**
	 * This method will generate the jdoql string
	 */
	private void generateQuery(
		StringBuffer filter,
		StringBuffer variables,
		String attributeName,
		Class attributeClassType,
		Criteria criteria,
		boolean collectionType,
		String collectionTypeName,
		String collectionAttributeName) {
		final String METHOD = "generateQuery";
		TraceHelper.entering(tracer, METHOD);
		String attrValue = criteria.rhs.toString();
		//We are dealing with the dates here.From the database point of view, these are just the
		// long values
		if (attributeClassType == Date.class | attributeClassType == Timestamp.class)
			if(criteria.rhs instanceof Date) {
				attrValue = Long.toString((((Date) criteria.rhs)).getTime()) + "L";
			} else if (criteria.rhs instanceof Long) {
				attrValue = attrValue + "L";
		}
		
		// handles Byte, Short, Integer, Long, Double, Float data types
		if(Number.class.isAssignableFrom(attributeClassType)) {
			if(criteria.rhs instanceof Long)
				attrValue = ((Number)criteria.rhs).longValue() + "L";
		}
		
		if (attributeClassType == String.class
			&& !(criteria instanceof QueryFilter.LikeCriteria))
			attrValue = "\"" + attrValue + "\"";
		/**
		 * Special treatment for variables of type boolean because of the jdo limitation
		 */
		if (attributeClassType == Boolean.class) {
			if (attrValue.equals("true")) {
				attrValue = "1";
			} else if (attrValue.equals("true")) {
				attrValue = "-1";
			}
		}

		if (collectionType) {
			String variableName = "tmp" + collectionTypeName;
			variables.append(collectionTypeName + " " + variableName + " ;");
			filter.append("(!" + attributeName + ".isEmpty() && ");
			filter.append(
				attributeName + ".contains(" + variableName + ") && ");
			if (criteria instanceof QueryFilter.EqualToCriteria)
				filter.append(
					variableName
						+ "."
						+ collectionAttributeName
						+ " == "
						+ attrValue
						+ ")");
			else if (criteria instanceof QueryFilter.GreaterThanCriteria)
				filter.append(
					variableName
						+ "."
						+ collectionAttributeName
						+ " > "
						+ attrValue
						+ " )");
			else if (criteria instanceof QueryFilter.LessThanCriteria)
				filter.append(
					variableName
						+ "."
						+ collectionAttributeName
						+ " < "
						+ attrValue
						+ ")");
			else if (
				criteria instanceof QueryFilter.GreaterThanEqualToCriteria)
				filter.append(
					variableName
						+ "."
						+ collectionAttributeName
						+ " >= "
						+ attrValue
						+ " )");
			else if (criteria instanceof QueryFilter.LessThanEqualToCriteria)
				filter.append(
					variableName
						+ "."
						+ collectionAttributeName
						+ " <= "
						+ attrValue
						+ ")");
			else if (criteria instanceof QueryFilter.LikeCriteria) {
				StringBuffer strBuf = new StringBuffer(attrValue);
				if (attrValue.indexOf('*') == 0)
					filter.append(
						variableName
							+ "."
							+ collectionAttributeName
							+ ".startsWith(\""
							+ attrValue.substring(1)
							+ "\") "
							+ ")");
				else if (attrValue.indexOf('*') > 0)
					filter.append(
						variableName
							+ "."
							+ collectionAttributeName
							+ ".startsWith(\""
							+ attrValue.substring(0, attrValue.indexOf('*'))
							+ "\") "
							+ ")");
				else
					filter.append(
						variableName
							+ "."
							+ collectionAttributeName
							+ ".startsWith(\""
							+ attrValue
							+ "\") "
							+ ")");
			}
		} 
		else {
			if (criteria instanceof QueryFilter.EqualToCriteria)
				filter.append("( " + attributeName + " == " + attrValue + ")");
			else if (criteria instanceof QueryFilter.GreaterThanCriteria)
				filter.append("( " + attributeName + " > " + attrValue + ")");
			else if (criteria instanceof QueryFilter.LessThanCriteria)
				filter.append("( " + attributeName + " < " + attrValue + ")");
			else if (
				criteria instanceof QueryFilter.GreaterThanEqualToCriteria)
				filter.append("( " + attributeName + " >= " + attrValue + ")");
			else if (criteria instanceof QueryFilter.LessThanEqualToCriteria)
				filter.append("( " + attributeName + " <= " + attrValue + ")");
			else if (criteria instanceof QueryFilter.LikeCriteria) {
				StringBuffer strBuf = new StringBuffer(attrValue);
				if (attrValue.indexOf('*') == 0)
					filter.append(
						"( "
							+ attributeName
							+ ".endsWith(\""
							+ attrValue.substring(1)
							+ "\") "
							+ ")");
				else if (attrValue.indexOf('*') > 0)
					filter.append(
						"( "
							+ attributeName
							+ ".startsWith(\""
							+ attrValue.substring(0, attrValue.indexOf('*'))
							+ "\") "
							+ ")");
				else
					filter.append(
						"( "
							+ attributeName
							+ ".startsWith(\""
							+ attrValue
							+ "\") "
							+ ")");
			}
		}
		TraceHelper.exiting(tracer, METHOD);
	}

	/////////////////////////////////////////////////
	// Internal methods

	/**
	 * Validates auction attribute values 
	 */
	private void internalValidateAuction(PrivateAuction auction) {
		final String METHOD = "internalValidateAuction";
		TraceHelper.entering(tracer, METHOD);
		if (AuctionStatusEnum.toEnum(auction.getStatus())
			!= AuctionStatusEnum.DRAFT) {
			if (logger.beInfo()) {
				String msg = "Validating the auction with name " + auction.getAuctionName();
				logger.infoT(tracer, msg);
			}
			auction.validate(true);
		}
		boolean validAuction = true;
		ArrayList culprits = new ArrayList();
		if (auction.getErrors().size() > 0) {
			culprits.add(auction);
			validAuction = false;
			logErrors(auction);
		}

		if (auction.getItems() != null) {
			Iterator it = auction.getItems().iterator();

			while (it.hasNext()) {
				PrivateAuctionItem item = (PrivateAuctionItem) it.next();
				if (item.getErrors().size() > 0) {
					culprits.add(item);
					validAuction = false;
					logErrors(item);
				}
			}
		}

		if(!validAuction) {
			ErrorMessageBundle bundle = ErrorMessageBundle.getInstance();
			I18nErrorMessage msg =
				bundle.newI18nErrorMessage(SystemException.INTERNAL_ERROR);
			ValidationException ex = new ValidationException(culprits, msg);
			super.throwing(METHOD, ex);
			throw ex;
		}

		TraceHelper.exiting(tracer, METHOD);
	}
	
	/**
	 * Validates auction dynamic values
	 */
	private void internalValidateDynamicAttribute(PrivateAuction auction) {
		final String METHOD = "internalValidateDynamicAttribute";
		TraceHelper.entering(tracer, METHOD);

		TraceHelper.exiting(tracer, METHOD);
	}

	/**
	 * Validates auction business rules
	 */
	private void internalValidateBusinessRule(PrivateAuction auction) {
		final String METHOD = "internalValidateBusinessRule";
		TraceHelper.entering(tracer, METHOD);

		TraceHelper.exiting(tracer, METHOD);
	}

	private void logErrors(ValidatingBean bean) {
		// log errors
		Iterator it = bean.getErrors().iterator();
		while (it.hasNext()) {
			I18nErrorMessage error = (I18nErrorMessage) it.next();
			try {
				logger.errorT(tracer,error.format());
			} catch (LocalizationException ignore) {
				LogHelper.logError(logger,tracer,ignore);
			}
		}
	}

	private void internalCreateQuotation(PrivateAuction auction) {
		final String METHOD = "internalCreateQuotation";
		TraceHelper.entering(tracer, METHOD);

		try {
			if (logger.beInfo()) {
				logger.infoT(tracer,
					"creating Quotation for Auction "
						+ auction.getAuctionName());
			}

			/*
			if(auction.getWebShopId() == null) {
				logger.debug("Error : Auction cannot be created without the shop data for " +
							   auction.getAuctionName());
			}*/

			ProcessBusinessObjectManager pbom =
				((AuctionBusinessObjectManager)(super.bom)).getProcessBusinessObjectManager();
//			BackendDefaultSettings settings =
//				XCMConfigContainer
//					.getInstance(getServiceLocatorBase().getScenarioName())
//					.getBackendConfig();
			AuctionQuotation quotation =
				pbom.createQuotation(auction);
			auction.setQuotationId(quotation.getHeader().getSalesDocNumber());
		} catch (Exception e) {
			ErrorMessageBundle bundle = ErrorMessageBundle.getInstance();
			SAPBackendException ex = null;
			if (e instanceof SAPBackendException) {
				ex = (SAPBackendException) e;
			} else {
				I18nErrorMessage msg =
					bundle.newI18nErrorMessage(
						SAPBackendException.UNKNOWN_BACKEND_ERROR);
				ex = new SAPBackendException(msg, e);
			}
			handleAndThrowException(auction, METHOD, null, ex);
		}

		if (logger.beInfo()) {
			logger.infoT(tracer,
				"created Quotation for Auction " + auction.getAuctionName());
		}
		TraceHelper.exiting(tracer, METHOD);
	}

	private void internalModifyQuotation(PrivateAuction auction) {
		final String METHOD = "internalModifyQuotation";
		TraceHelper.entering(tracer, METHOD);
		// modify the quotation
		try {
			ProcessBusinessObjectManager pbom =
				((AuctionBusinessObjectManager)(super.bom)).getProcessBusinessObjectManager();
			pbom.modifyQuotation(auction);
		} catch (Exception e) {
			ErrorMessageBundle bundle = ErrorMessageBundle.getInstance();
			SAPBackendException ex = null;
			if (e instanceof SAPBackendException) {
				ex = (SAPBackendException) e;
			} else {
				I18nErrorMessage msg =
					bundle.newI18nErrorMessage(
						SAPBackendException.UNKNOWN_BACKEND_ERROR);
				ex = new SAPBackendException(msg, e);
			}
			handleAndThrowException(auction, METHOD, null, ex);
		}
		TraceHelper.exiting(tracer, METHOD);
	}

	/**
	 * Close the Quotation By setting its Exp Date to now
	 */
	private void internalReleaseQuotation(PrivateAuction auctionBean) {
		final String METHOD = "internalReleaseQuotation";
		TraceHelper.entering(tracer, METHOD);

		// set the Quotation Date to Current
		if (auctionBean.getQuotationId() != null) {
			ProcessBusinessObjectManager pbom =
			((AuctionBusinessObjectManager)(super.bom)).getProcessBusinessObjectManager();
			AuctionQuotation quotation = pbom.createQuotation();
			AuctionQuotationChange quotChange =
				new AuctionQuotationChange(quotation);
			try {
				quotChange.cancelQuotationInBackend(
					new TechKey(auctionBean.getQuotationId()));
			} catch (Exception e) {
				ErrorMessageBundle bundle = ErrorMessageBundle.getInstance();
				SAPBackendException ex = null;
				if (e instanceof SAPBackendException) {
					ex = (SAPBackendException) e;
				} else {
					I18nErrorMessage msg =
						bundle.newI18nErrorMessage(
							SAPBackendException.UNKNOWN_BACKEND_ERROR);
					ex = new SAPBackendException(msg, e);
				}
				handleAndThrowException(auctionBean, METHOD, null, ex);
			}
			Timestamp now = new Timestamp(System.currentTimeMillis());
			auctionBean.setQuotationExpirationDate(now);
		}
		TraceHelper.exiting(tracer, METHOD);
	}

	/**
	 * Friend methods are exposed from this class
	 */
	public class FriendInterface {
		/**
		 * private ctor
		 */
		private FriendInterface() {
		}

		/**
		 * Exposes the public interface of EBayAuctionManager
		 */
		public PrivateAuctionMgrImpl getPrivateAuctionManager() {
			return PrivateAuctionMgrImpl.this;
		}

		public void validateAuction(PrivateAuction auction) {
			PrivateAuctionMgrImpl.this.internalValidateAuction(auction);
		}

		public void createAuction(PrivateAuction auction) {
			PrivateAuctionMgrImpl.this.internalCreateAuction(auction);
		}

		public void createQuotation(PrivateAuction auction) {
			PrivateAuctionMgrImpl.this.internalCreateQuotation(auction);
		}

		public void modifyQuotation(PrivateAuction auction) {
			PrivateAuctionMgrImpl.this.internalModifyQuotation(auction);
		}

		public void releaseQuotation(PrivateAuction auction) {
			PrivateAuctionMgrImpl.this.internalReleaseQuotation(auction);
		}

		public void modifyAuction(PrivateAuction auction) {
			PrivateAuctionMgrImpl.this.internalModifyAuction(auction);
		}

		public void modifyAuctionStatus(
			PrivateAuction auction,
			AuctionStatusEnum status) {
			PrivateAuctionMgrImpl.this.internalModifyStatus(auction, status);
		}

		public void deleteAuction(PrivateAuction auction) {
			PrivateAuctionMgrImpl.this.internalDeleteAuction(auction);
		}

		public PrivateAuction getAuctionById(String auctionId, boolean template) {
			return PrivateAuctionMgrImpl.this.internalGetAuction(
				auctionId,
				template);
		}

		public Collection getAuctionsToBePublished(Date date) {
			return PrivateAuctionMgrImpl.this.internalGetAuctionsToBePublished(
				date);
		}
		
		public Collection getAuctionsToBeClosed(Date date) {
			return PrivateAuctionMgrImpl.this.internalGetAuctionsToBeClosed(
				date);
		}
		
		public ServiceLocator getServiceLocator() {
			return PrivateAuctionMgrImpl.this.getServiceLocatorBase();
		}
		
		public void sendErrorNotification(Auction auction, String errorMsg) {
			PrivateAuctionMgrImpl.this.internalSendErrorNotification(auction, errorMsg);
		}
		
		public void sendBidderInvitation(Auction auction) {
			PrivateAuctionMgrImpl.this.internalSendBidderInvitation(auction);
		}
		
		public void sendOutbiddingNotification(Auction auction) {
			PrivateAuctionMgrImpl.this.internalSendOutbiddingNotification(auction);
		}
		
		public void sendWinnerNotification(Auction auction, WinnerData winner) {
			PrivateAuctionMgrImpl.this.internalSendWinnerNotification(auction, winner);
		}
	}

	/**
	 *
	 */
	public FriendInterface getFriendInterface(Object hash, Class friendClass)
		throws IllegalAccessException {
		if (hash == null || friendClass == null) {
			throw new IllegalAccessException("Invalid hash or friendClass");
		}

		String fqn = (String) friends.get(hash);

		if (fqn == null) {
			throw new IllegalAccessException("hash is invalid");
		}

		if (friendClass.getName().equals(fqn)) {
			return new FriendInterface();
		} else {
			throw new IllegalAccessException(
				friendClass.getName() + " is not my friend");
		}
	}

	private Collection internalGetAuctionsToBePublished(Date date) {
		final String METHOD = "internalGetAuctionsToBePublished";
		TraceHelper.entering(tracer, METHOD);
		PersistenceManager pm = null;
		checkInitialized();
		InvocationContext ic = (InvocationContext)getInvocationContext();
		checkPermission(ic.getUser(), METHOD);
		String client = bom.createSeller().getClient();
		String systemId = bom.createSeller().getSystemId();
		if (client == null || systemId == null) {
			// This should be fine for now?
			logger.errorT(tracer,
				"client or system id is null for the scenario "
					+ getServiceLocatorBase().getScenarioName());
			ErrorMessageBundle bundle = ErrorMessageBundle.getInstance();
			I18nErrorMessage msg =
				bundle.newI18nErrorMessage(
					ValidationException.INVALID_CLIENT_SYSTEM_ID);
			ArrayList culprits = new ArrayList();
			ValidationException vex = new ValidationException(culprits, msg);
			handleAndThrowException(null, METHOD, pm, vex);
		}
		Auction auction = null;
		try {
			ServiceLocator locator = getServiceLocatorBase();
			PersistenceManagerFactory pmf =
				locator.getPersistenceManagerFactory();
			pm = pmf.getPersistenceManager();
			// @@temp: begin hack for JDO bug in loading of meta data
			Query query = pm.newQuery(PrivateAuctionDAO.class);
			StringBuffer filter = new StringBuffer("");
			filter.append("publishDate < ");
			filter.append(date.getTime() + "L");
			filter.append(" &&  status");
			filter.append(" == ");
			filter.append(AuctionStatusEnum.toInt(AuctionStatusEnum.SCHEDULED));
			filter.append("&&  client");
			filter.append(" == ");
			filter.append("\"" + client + "\"");
			filter.append("&&  systemId");
			filter.append(" == ");
			filter.append("\"" + systemId + "\"");
			query.setFilter(filter.toString());
			Collection coll = (Collection) query.execute();
			if (!coll.iterator().hasNext()) {
				LogHelper.logInfo(logger,tracer,"No Auctions found in SCHEDULED status", null);
			}
			Iterator iter = coll.iterator();
			Collection returnCollection = new ArrayList();
			while (iter.hasNext()) {
				PrivateAuctionDAO auctionDAO = (PrivateAuctionDAO) iter.next();
				auction = new PrivateAuction();
				// sync
				DALBeanSynchronizer.synchAuctionBeanFromDAO(
					auctionDAO,
					auction);
				returnCollection.add(auction);
			}
			query.closeAll();
			TraceHelper.exiting(tracer, METHOD);
			return returnCollection;
		} catch (Exception ex) {
			handleAndThrowException(auction, METHOD,pm,ex);
			return null; // DUMMY return, above method Always throws exception
		} finally {
			// close the pm
			if (pm != null)
				pm.close();
		}
	}

	private Collection internalGetAuctionsToBeClosed(Date date) {
		final String METHOD = "internalGetAuctionsToBePublished";
		TraceHelper.entering(tracer, METHOD);
		PersistenceManager pm = null;
		checkInitialized();
		InvocationContext ic = (InvocationContext)getInvocationContext();
		checkPermission(ic.getUser(), METHOD);
		String client = bom.createSeller().getClient();
		String systemId = bom.createSeller().getSystemId();
		if (client == null || systemId == null) {
			// This should be fine for now?
			logger.errorT(tracer,
				"client or system id is null for the scenario "
					+ getServiceLocatorBase().getScenarioName());
			ErrorMessageBundle bundle = ErrorMessageBundle.getInstance();
			I18nErrorMessage msg =
				bundle.newI18nErrorMessage(
					ValidationException.INVALID_CLIENT_SYSTEM_ID);
			ArrayList culprits = new ArrayList();
			ValidationException vex = new ValidationException(culprits, msg);
			handleAndThrowException(null, METHOD, pm, vex);
		}
		Auction auction = null;
		try {
			ServiceLocator locator = getServiceLocatorBase();
			PersistenceManagerFactory pmf =
				locator.getPersistenceManagerFactory();
			pm = pmf.getPersistenceManager();
			// @@temp: begin hack for JDO bug in loading of meta data
			Query query = pm.newQuery(PrivateAuctionDAO.class);
			StringBuffer filter = new StringBuffer("");
			filter.append("endDate <= ");
			filter.append(date.getTime() + "L");
			filter.append(" &&  status");
			filter.append(" == ");
			filter.append(AuctionStatusEnum.toInt(AuctionStatusEnum.PUBLISHED));
			filter.append("&&  client");
			filter.append(" == ");
			filter.append("\"" + client + "\"");
			filter.append("&&  systemId");
			filter.append(" == ");
			filter.append("\"" + systemId + "\"");
			query.setFilter(filter.toString());
			Collection coll = (Collection) query.execute();
			if (!coll.iterator().hasNext()) {
				LogHelper.logInfo(logger,tracer,"No Auctions found in SCHEDULED status", null);
			}
			Iterator iter = coll.iterator();
			Collection returnCollection = new ArrayList();
			while (iter.hasNext()) {
				PrivateAuctionDAO auctionDAO = (PrivateAuctionDAO) iter.next();
				auction = new PrivateAuction();
				// sync
				DALBeanSynchronizer.synchAuctionBeanFromDAO(
					auctionDAO,
					auction);
				returnCollection.add(auction);
			}
			query.closeAll();
			TraceHelper.exiting(tracer, METHOD);
			return returnCollection;
		} catch (Exception ex) {
			handleAndThrowException(auction, METHOD,pm,ex);
			return null; // DUMMY return, above method Always throws exception
		} finally {
			// close the pm
			if (pm != null)
				pm.close();
		}
	}

	/**
	 * <p>Data is copied from ref Auctions</p>
	 * Only one level of references are allowed
	 * @see com.sap.isa.auction.businessobject.ebay.EBayAuctionManager#createAuctionByRef(com.sap.isa.auction.bean.Auction)
	 */
	public void createAuctionByRef(Auction newAuction, Auction ref) {
		final String METHOD = "createAuctionByRef";
		TraceHelper.entering(tracer, METHOD);
		checkInitialized();
		InvocationContext ic = (InvocationContext)getInvocationContext();
		checkPermission(ic.getUser(), METHOD);

		// only Auction refering to NON finalized auctions can be stored as of now
		AuctionStatusEnum status = AuctionStatusEnum.toEnum(ref.getStatus());
		ErrorMessageBundle bundle = ErrorMessageBundle.getInstance();
		if (status != AuctionStatusEnum.NOT_FINALIZED) {
			I18nErrorMessage msg =
				bundle.newI18nErrorMessage(
					BOUserException
						.ONLY_NON_FINALIZED_AUCTION_RELISTING_ALLOWED_VIOLATION);
			BOUserException ex = new BOUserException(msg);
			handleAndThrowException(ref, METHOD, null, ex);
		}

		// only one level of references allowed
		if (ref.getReferencedAuctionId() != null) {
			I18nErrorMessage msg =
				bundle.newI18nErrorMessage(
					BOUserException.ONLY_ONE_RELISTING_ALLOWED_VIOLATION);
			BOUserException ex = new BOUserException(msg);
			handleAndThrowException(ref, METHOD, null, ex);
		}

		// check that the auction exists and does not have references to it
		Collection refAuctions = internalGetRefAuctions((PrivateAuction) ref);

		// Only one Auction can be referenced by Another Auction
		if (refAuctions != null && refAuctions.size() > 1) {
			I18nErrorMessage msg =
				bundle.newI18nErrorMessage(
					BOUserException.ONLY_ONE_RELISTING_ALLOWED_VIOLATION);
			BOUserException ex = new BOUserException(msg);
			handleAndThrowException(ref, METHOD, null, ex);
		}

		if (!newAuction.validate()) {
			I18nErrorMessage msg =
				bundle.newI18nErrorMessage(ValidationException.INVALID_AUCTION);
			ValidationException ex = new ValidationException(newAuction, msg);
			handleAndThrowException(ref, METHOD, null, ex);
		}

		// Relisting must be done in 30 days, can be done only once etc.
		// validate the new Auction. Only certain attributes can be modified and new
		// values depened on refereced Auction atttribute values e.g Reserve price must be
		// lower
		PrivateAuction newPrivateAuction = (PrivateAuction) newAuction;
		// setting of state vriables
		newPrivateAuction.setStartDate(null);
		newPrivateAuction.setEndDate(null);
		newPrivateAuction.setLastModifiedDate(null);
		newPrivateAuction.setPublishDate(null);
		newPrivateAuction.setQuotationExpirationDate(null);
		newPrivateAuction.setQuotationId(null);
		newAuction.setStatus(AuctionStatusEnum.OPEN.getValue());
		newAuction.setReferencedAuctionId(ref.getAuctionId());
		AuctionUserBaseData user = (AuctionUserBaseData)ic.getUser();
		if(	user.getSalesEmployee()!= null && 
			user.getSalesEmployee().getId() != null && 
			user.getSalesEmployee().getId().length() > 0 )
			newAuction.setSellerId(user.getSalesEmployee().getId());
		else //CRM case
			newAuction.setSellerId(user.getUserId());
		newPrivateAuction.setCreateDate(
			new java.sql.Timestamp(System.currentTimeMillis()));
		newPrivateAuction.setCreatedBy(user.getUserId().toUpperCase());
		newPrivateAuction.setWebShopId(ic.getShopId());
		if (logger.beInfo()) {
			String msg = "Creating the auction from reference " + ref.getAuctionName();
			logger.infoT(tracer, msg);
		}
		internalCreateAuction(newPrivateAuction);
		TraceHelper.exiting(tracer, METHOD);
	}

	/* (non-Javadoc)
	 * @see com.sap.isa.auction.businessobject.ebay.EBayAuctionManager#getRelistings(com.sap.isa.auction.bean.Auction)
	 */
	public Collection getRelistings(PrivateAuction auction) {
		final String METHOD = "getRelistings";
		TraceHelper.entering(tracer, METHOD);
		checkInitialized();
		InvocationContext ic = (InvocationContext)getInvocationContext();
		checkPermission(ic.getUser(), METHOD);

		// only Auction refering to NON finalized auctions can be stored as of now
		AuctionStatusEnum status =
			AuctionStatusEnum.toEnum(auction.getStatus());
		if (status != AuctionStatusEnum.NOT_FINALIZED) {
			ErrorMessageBundle bundle = ErrorMessageBundle.getInstance();
			I18nErrorMessage msg = bundle.newI18nErrorMessage();
			BOUserException ex = new BOUserException(msg);
			handleAndThrowException(auction, METHOD, null, ex);
		}
		TraceHelper.exiting(tracer, METHOD);
		return internalGetRefAuctions(auction);
	}

	private Collection internalGetRefAuctions(PrivateAuction auction) {
		final String METHOD = "internalGetRefAuctions";
		TraceHelper.entering(tracer, METHOD);

		TraceHelper.exiting(tracer, METHOD);
		return null;
	}

	/* (non-Javadoc)
	 * @see com.sap.isa.auction.businessobject.b2x.PrivateAuctionManager#sendBidderInvitation(com.sap.isa.auction.bean.Auction)
	 */
	private void internalSendBidderInvitation(Auction auction) {
		final String METHOD = "sendBidderInvitation";
		TraceHelper.entering(tracer, METHOD);
		
		if (AuctionStatusEnum.toEnum(auction.getStatus())
			!= AuctionStatusEnum.PUBLISHED) {
			ErrorMessageBundle bundle = ErrorMessageBundle.getInstance();
			I18nErrorMessage msg =
				bundle.newI18nErrorMessage(InvalidStateException.EMAIL_NOT_PUBLISH, 
					new Object[]{auction.getAuctionId()});
			InvalidStateException ex = new InvalidStateException(msg);
			handleAndThrowException(auction, METHOD, null, ex);
		}
		
		if((auction.getTargetGroups() == null || auction.getTargetGroups().size()<=0) && 
			(auction.getBusinessPartners() == null || auction.getBusinessPartners().size()<=0))
			return;    //no need to send notification, not error
					
		try {
			ProcessBusinessObjectManager pbom =
			((AuctionBusinessObjectManager)(super.bom)).getProcessBusinessObjectManager();
			
			EmailNotification emailNotification = pbom.createEmailNotification();
			synchronized(emailNotification) {
				String from = getEmailSender(auction);
				String subject = getEmailSubject("email.bidinvite.subject", auction);

				emailNotification.setData(EmailNotificationType.BIDDING_INVITATION,
					from, null, subject , auction); //This is default Email subject
					// If the subject is provided in XCM, it will overwrite this value 
				executeBackendTask(emailNotification);
				//emailNotification.sendEmail();
			}
			pbom.releaseEmailNotification();
		} catch (Exception ex) {
			handleAndThrowException(auction, METHOD, null, ex);
		}
		
		TraceHelper.exiting(tracer, METHOD);
	}

	/**
	 * Returns the computer subject
	 * @param string
	 * @return
	 */
	private String getEmailSubject(String string, Auction auction) {
		String subject ;
		try {
			subject = ResourceBundle.getBundle("admin").getString(string);
			if(subject == null)
				subject = "";
			else{
				subject = MessageFormat.format(subject, new Object[]{ auction.getAuctionName()} );	
			}
		} catch (Throwable t) {
			subject = "";
		}
		return subject;
	}

	/* (non-Javadoc)
	 * @see com.sap.isa.auction.businessobject.b2x.PrivateAuctionManager#sendOutbiddingNotification(com.sap.isa.auction.bean.Auction)
	 */
	private void internalSendOutbiddingNotification(Auction auction) {
		final String METHOD = "sendOutbiddingNotification";
		TraceHelper.entering(tracer, METHOD);
		
		if (AuctionStatusEnum.toEnum(auction.getStatus())
			!= AuctionStatusEnum.PUBLISHED) {
			ErrorMessageBundle bundle = ErrorMessageBundle.getInstance();
			I18nErrorMessage msg =
				bundle.newI18nErrorMessage(InvalidStateException.EMAIL_NOT_PUBLISH, 
					new Object[]{auction.getAuctionId()});
			InvalidStateException ex = new InvalidStateException(msg);
			handleAndThrowException(auction, METHOD, null, ex);
		}
		
		try {
			ProcessBusinessObjectManager pbom =
			((AuctionBusinessObjectManager)(super.bom)).getProcessBusinessObjectManager();
			
			EmailNotification emailNotification = pbom.createEmailNotification();
			synchronized(emailNotification) {
				String from = getEmailSender(auction);
				emailNotification.setData(EmailNotificationType.AUCTION_OUTBIDDING,
					from, null, "OUT BIDDING NOTIFICATION", auction);
				executeBackendTask(emailNotification);
				//emailNotification.sendEmail();
			}
			pbom.releaseEmailNotification();
		} catch (Exception ex) {
			handleAndThrowException(auction, METHOD, null, ex);
		}
		
		TraceHelper.exiting(tracer, METHOD);
	}

	/* (non-Javadoc)
	 * @see com.sap.isa.auction.businessobject.b2x.PrivateAuctionManager#sendWinnerNotification(com.sap.isa.auction.bean.Auction)
	 */
	private void internalSendWinnerNotification(Auction auction, WinnerData winner) {
		final String METHOD = "sendWinnerNotification";
		TraceHelper.entering(tracer, METHOD);

		if ( (AuctionStatusEnum.toEnum(auction.getStatus())!= AuctionStatusEnum.CLOSED) && 
			(AuctionStatusEnum.toEnum(auction.getStatus()) != AuctionStatusEnum.FINALIZED) ) {
			ErrorMessageBundle bundle = ErrorMessageBundle.getInstance();
			I18nErrorMessage msg =
				bundle.newI18nErrorMessage(InvalidStateException.EMAIL_NOT_CLOSE, 
					new Object[]{auction.getAuctionId()});
			InvalidStateException ex = new InvalidStateException(msg);
			handleAndThrowException(auction, METHOD, null, ex);
		}
		
		try {
			ProcessBusinessObjectManager pbom =
			((AuctionBusinessObjectManager)(super.bom)).getProcessBusinessObjectManager();
			
			EmailNotification emailNotification = pbom.createEmailNotification();
			synchronized(emailNotification) {
				String from = getEmailSender(auction);
				String subject = getEmailSubject("email.winnernotify.subject", auction);
				emailNotification.setData(EmailNotificationType.AUCTION_WINNING,
					from, null, subject, auction, winner);//This is default Email subject
				// If the subject is provided in XCM, it will overwrite this value
				executeBackendTask(emailNotification);
				//emailNotification.sendEmail();
			}
			pbom.releaseEmailNotification();
		} catch (Exception ex) {
			handleAndThrowException(auction, METHOD, null, ex);
		}
		
		TraceHelper.exiting(tracer, METHOD);
	}

	/* (non-Javadoc)
	 * @see com.sap.isa.auction.businessobject.b2x.PrivateAuctionManager#sendWinnerNotification(com.sap.isa.auction.bean.Auction)
	 */
	private void internalSendErrorNotification(Auction auction, String errorMsg) {
		final String METHOD = "internalSendErrorNotification";
		TraceHelper.entering(tracer, METHOD);

		try {
			ProcessBusinessObjectManager pbom =
				((AuctionBusinessObjectManager)(super.bom)).getProcessBusinessObjectManager();
			
			ErrorNotification errorNotification = pbom.createErrorNotification();
			synchronized(errorNotification) {
				//errorNotification.setFrom("AUCADMIN");
				String from = getEmailSender(auction);
				errorNotification.setFrom(from);
				ArrayList to = new ArrayList();
				to.add(from);
				errorNotification.setTo(to);
				String subject = getEmailSubject("email.errornotify.subject", auction);
				errorNotification.setSubject(subject);//This is default Email subject
				// If the subject is provided in XCM, it will overwrite this value
				errorNotification.setError(errorMsg);
				executeBackendTask(errorNotification);
				//errorNotification.sendErrorNotification();
			}
			pbom.releaseErrorNotification();
		} catch (Exception ex) {
			handleAndThrowException(auction, METHOD, null, ex);
		}
		
		TraceHelper.exiting(tracer, METHOD);
	}
	
	private String getEmailSender(Auction auction) {
		String from;
		if(auction == null || 
		  auction.getCreatedBy() == null || 
		  auction.getCreatedBy().length()<=0) {
			InvocationContext ic = (InvocationContext)getInvocationContext();
			from = ic.getUser().getUserId();
		} 
		else
			from = auction.getCreatedBy();
		
		return from;
	}
	
	private static GenericThreadPool tpool = null;
	
	private void executeBackendTask(Runnable task)
	{
		if(tpool == null) {
			tpool = new GenericThreadPool("email pool",null);
			tpool.start();
		}
		
		tpool.execute(task);
	}
	
}
