/*
 * Created on Apr 23, 2004
 *
 * To change the template for this generated file go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
package com.sap.isa.auction.businessobject.b2x;

import com.sap.isa.auction.businessobject.AuctionBusinessObject;
import com.sap.isa.auction.businessobject.SearchManager;
import com.sap.isa.persistence.pool.PooledObjectFactory;

/**
 * Title:			Internet Sales Private Auctions
 * Description:
 * @Copyright: 		Copyright (c) 2004
 * Company:			SAPLabs LLC
 * @author 
 *
 * To change the template for this generated type comment go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
public class SearchManagerFactory extends PooledObjectFactory {


	public SearchManagerFactory() {
		super(SearchManager.class);
	}

	/* (non-Javadoc)
	 * @see com.sap.isa.persistence.pool.PooledObjectFactory#create(java.lang.Object)
	 */
	public Object create(Object arg0) {
		return new SearchManagerImpl();
	}

	public Object initialize(Object obj) {
		if(obj instanceof SearchManagerImpl) {
			((AuctionBusinessObject)obj).initialize();
		}
		return obj;
	}

	public Object unwrap(Object obj) {
		if(obj instanceof SearchManagerImpl) {
			return obj;
		}
		else {
			return null;
		}
	}

	public void close(Object obj) {
		if(obj instanceof SearchManagerImpl) {
			((SearchManagerImpl)obj).close();
		}
	}

	public void destroy(Object obj) {
		// do nothing
	}

}
