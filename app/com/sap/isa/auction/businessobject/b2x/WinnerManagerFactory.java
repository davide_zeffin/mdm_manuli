/*
 * Created on Apr 23, 2004
 *
 * Title:        Internet Sales Private Auctions
 * Description:
 * Copyright:    Copyright (c) 2004
 * Company:      SAP Labs LLC
 * @author
 * @version 1.0
 */
package com.sap.isa.auction.businessobject.b2x;

import com.sap.isa.auction.businessobject.*;
import com.sap.isa.persistence.pool.PooledObjectFactory;

/**
 * Title:        Internet Sales
 * Description:
 * Copyright:    Copyright (c) 2002
 * Company:      SAPMarkets
 * @author
 * @version 1.0
 */

public class WinnerManagerFactory extends PooledObjectFactory {

	public WinnerManagerFactory() {
		super(null);
		//super(WinnerMgrImpl.class);
	}

	public Object create(Object params) {
		return new WinnerMgrImpl();
	}

	public Object initialize(Object obj) {
		if(obj instanceof WinnerMgrImpl) {
			((AuctionBusinessObject)obj).initialize();
		}
		return obj;
	}

	public Object unwrap(Object obj) {
		if(obj instanceof WinnerMgrImpl) {
			return obj;
		}
		else {
			return null;
		}
	}

	public void close(Object obj) {
		if(obj instanceof WinnerMgrImpl) {
			((WinnerMgrImpl)obj).close();
		}
	}

	public void destroy(Object obj) {
		// do nothing
	}
}