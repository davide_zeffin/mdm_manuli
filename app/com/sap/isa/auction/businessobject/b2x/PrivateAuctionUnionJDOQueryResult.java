package com.sap.isa.auction.businessobject.b2x;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;

import javax.jdo.PersistenceManager;
import javax.jdo.Query;
import javax.jdo.spi.PersistenceCapable;

import com.sap.tc.logging.Category;
import com.sap.tc.logging.Location;
import com.sap.isa.auction.exception.InvalidStateException;
import com.sap.isa.auction.exception.i18n.ErrorMessageBundle;
import com.sap.isa.auction.exception.i18n.I18nErrorMessage;
import com.sap.isa.auction.logging.CategoryProvider;
import com.sap.isa.auction.logging.LogHelper;
import com.sap.isa.crt.RecordingCallManagerFactory;

/**
 * Title:			Internet Sales Private Auctions
 * Description:
 * @Copyright: 		Copyright (c) 2004
 * Company:			SAPLabs LLC
 * @author 
 * Created on: 		May 25, 2004
 */
public class PrivateAuctionUnionJDOQueryResult extends PrivateAuctionJDOQueryResult {
	/** Static data block*/
	private static Category logger = CategoryProvider.getBOLCategory();
	private static Location tracer = Location.getLocation(PrivateAuctionUnionJDOQueryResult.class.getName());
	
	private Query query2 = null;
	private Collection coll2 = null;
	private int resultSize1;
	/**
	 * @param pm
	 * @param query
	 * @param coll
	 * @param resultSize
	 */
	public PrivateAuctionUnionJDOQueryResult(
		PersistenceManager pm,
		Query query1,
		Query query2,
		Collection coll,
		Collection coll2,
		int resultSize,
		int resultSize1, RecordingCallManagerFactory rcmf) {
		super(pm, query1, coll, resultSize, rcmf);
		this.query2 = query2;
		this.coll2 = coll2;
		this.resultSize1 = resultSize1;
	}
	public Object[] fetch() {
		usageCheck();
		Object[] ret = new Object[size];
		int startIndex = pos;
		int endIndex = pos+size;
		if(!available()) return ret;
		boolean startSearchColl2 = pos>resultSize1;
		
		// implicityl connected mode

		if ( connected && coll != null && !startSearchColl2) { //if position falls in the first collection
				Iterator it = coll.iterator();
				int index = 1;
				// move to current pos. Thats the only way:(
				while(available() && index < pos){
				   index++;
				   it.next(); // index to pos
				}
				while(available() && !startSearchColl2 && pos < endIndex){
					fetchObject(it, ret, startIndex);
					startSearchColl2 = pos>resultSize1;
				}
				while(it.hasNext()) {
					it.next();
				}
		} 
		else if (connected && coll2 != null && startSearchColl2){	//if position falls in the second collection
			Iterator it = coll2.iterator();
			int newIndex = 1;
			while(available() && newIndex < (pos-resultSize1)){
				newIndex++;
				it.next();
			}
			while(available() && pos < endIndex) {
				fetchObject(it, ret, startIndex);
			}
			while(it.hasNext()) {
				it.next();
			}
			
		}
	
		// reconnected usage mode
		else if (coll == null || coll2 == null){
			// retreive the Object ids for the list of objects to connect
			for(int i = 0; i < size && available(); i++) {
				Object id = objectIds.get(pos++ - 1);
				Object valueBean = cache.get(id.toString());
				if(valueBean == null) {
					// TODO:: If possible use batch query for current fetch than firing individual queries
					// disconnected mode, raise exception
					if(!connected) {
						ErrorMessageBundle bundle = ErrorMessageBundle.getInstance();
						I18nErrorMessage msg = bundle.newI18nErrorMessage(InvalidStateException.NOT_AVAILABLE);
						msg.setArguments(new String[]{"JDOQueryResult disconnected, OID = " + id.toString()});
						InvalidStateException ex = new InvalidStateException(msg);
						LogHelper.logError(logger,tracer,ex.getMessage(),ex);
						throw ex;
					}
					PersistenceCapable dao = (PersistenceCapable)pm.getObjectById(id,false);
					valueBean = transcribe(dao);
					cache.put(id.toString(),valueBean);
				}
				ret[i] = valueBean;
			}
		}
	
		return ret;
	}

	private void fetchObject(Iterator it, Object[] ret, int index){
		if(available() && pos < index + size) {
			PersistenceCapable dao = (PersistenceCapable)it.next();
			// Look up in temp mem sensitive cache to avoid transcribing
			Object valueBean = cache.get(dao.jdoGetObjectId().toString());
			if(valueBean == null) {
				valueBean = transcribe(dao);
				cache.put(dao.jdoGetObjectId().toString(),valueBean);
			}
			ret[pos - index] = valueBean;
			pos++;
		}
	}
	public void close() {
		if(super.closed) return;
		try {
			if(query != null) query.closeAll();
			if(query2 != null) query2.closeAll();
			coll = null;
			coll2 = null;
			query = null;
			query2 = null;
			rcmf = null;
		}
		catch(Exception ex) {
			RuntimeException rtex = handleException(ex);
			LogHelper.logWarning(logger,tracer,"Error during close of JDOQueryResult",ex);
		}
		catch(Throwable th){
			LogHelper.logWarning(logger,tracer,"Error during close of JDOQueryResult", th);
		}
		finally {
			if(pm != null) pm.close();
			pm = null;
		}
		closed = true;
	}
	
	public void disconnect() {
		usageCheck();
		if(!connected) return;
		connected = false;
		super.disconnect();
		
		try {
			// If object Ids are once filled, there is no need to refill
			if(objectIds == null) {
				objectIds = new ArrayList(resultSize); 
				// cache the Object Ids as Hard References;
				Iterator it = coll.iterator();
				while(it.hasNext()) {
					Object id = ((PersistenceCapable)it.next()).jdoGetObjectId();
					objectIds.add(id); // in order of search result
				}
				if (coll2 != null) {
					Iterator it2 = coll2.iterator();
					while(it2.hasNext()){
						Object id = ((PersistenceCapable)it.next()).jdoGetObjectId();
						objectIds.add(id); // in order of search result
					}
				}
			}
			if(query != null) { query.closeAll(); query = null; }
			if(query2 != null) { query2.closeAll(); query2 = null; }
			coll = null;
			coll2 = null;
		}
		catch(Exception ex) {
			RuntimeException rtex = handleException(ex);
			LogHelper.logWarning(logger,tracer,"Error during disconnect of JDOQueryResult",rtex);
		}
		finally {
			// close the pm
			if(pm != null) {
				pm.close();
				pm = null;
			}

		}
	}
}
