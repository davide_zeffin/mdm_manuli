package com.sap.isa.auction.businessobject.b2x;

import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

import javax.jdo.JDOException;
import javax.jdo.PersistenceManager;
import javax.jdo.PersistenceManagerFactory;
import javax.jdo.Query;

import com.sap.localization.LocalizationException;
import com.sap.tc.logging.Location;
import com.sap.isa.auction.bean.Auction;
import com.sap.isa.auction.bean.AuctionStatusEnum;
import com.sap.isa.auction.bean.Bid;
import com.sap.isa.auction.bean.ValidatingBean;
import com.sap.isa.auction.bean.b2x.BidTypeEnum;
import com.sap.isa.auction.bean.b2x.PrivateAuction;
import com.sap.isa.auction.bean.b2x.PrivateAuctionBid;
import com.sap.isa.auction.businessobject.AbstractBusinessObject;
import com.sap.isa.auction.businessobject.b2x.InvocationContext;
import com.sap.isa.auction.businessobject.UserRoleTypeEnum;
import com.sap.isa.auction.businessobject.ValidationException;
import com.sap.isa.auction.businessobject.b2x.jdo.PrivateAuctionBidDAO;
import com.sap.isa.auction.exception.SystemException;
import com.sap.isa.auction.exception.i18n.ErrorMessageBundle;
import com.sap.isa.auction.exception.i18n.I18nErrorMessage;
import com.sap.isa.auction.logging.LogHelper;
import com.sap.isa.auction.services.ServiceLocator;
import com.sap.isa.auction.services.LockingManager;
import com.sap.isa.services.schedulerservice.core.helpers.GuidGenerator;

/**
 * Title:
 * Description: Stateless and Pooled Business Object
 * Copyright:    Copyright (c) 2003
 * Company:
 * @author
 * @version 1.0
 */

public class BidMgrImpl
	extends AbstractBusinessObject
	implements PrivateAuctionBidMgr {
	boolean testMode = false;
	private static Location tracer =
		Location.getLocation(BidMgrImpl.class.getName());
	private final static String AUC_TABLE_NAME="CRM_ISA_AVW_AUC";
	private final static String INSERT_BIDSQL_STMT =
		"INSERT INTO CRM_ISA_AVW_BID (ID, CLIENT,SYSTEMID,AUCTIONID,"
			+ "BIDPRICE,CURRENCYID,BIDQUANTITY,BIDTIME,BPID,SOLDTOPARTY,USERID)VALUES(?,?,?,?,?,?,?,?,?,?,?)";

	public BidMgrImpl() {
	}

	public void initialize() {
		super.initialize();
	}

	public void close() {
		super.close();
	}

	protected Location getTracer() {
		return tracer;
	}

	/**
	 * 
	 * If an auction is Active, bids are placed. Bids are placed concurrently by different bidders but bidding 
	 * needs to be handled in a synchronous manner inside the engine. DB level 
	 * locking is used to avoid lock synchronization in case of clustered deployment of auction engine. 
	 * Bid crea-tion has to be handled using direct SQL statements for efficiency and db level locking:
	
	 */

	/**
	 * Internal method
	 * Creates a new Bid
	 * This method is triggered by the Scheduler based on event notifications from
	 * eBay
	 *
	 * Note:: No validation checks are porformed on the state of Auction
	 */
	public void createBid(PrivateAuction auction, PrivateAuctionBid bid) {
		final String METHOD = "createBid";

		checkInitialized();
		InvocationContext ic =(InvocationContext) getInvocationContext();
		checkNull(bid);
		/**
		 * Check the type of the bid
		 */
		// For normal bid lock the DB
		long currentTime = System.currentTimeMillis();
		bid.setBidDate(new Timestamp(currentTime));
		bid.setWebShopId(ic.getShopId());
		//Client and SystemId setting
		String client =
			((AuctionBusinessObjectManager) bom)
				.createSystemSetting()
				.getClient();
		String systemId =
			((AuctionBusinessObjectManager) bom)
				.createSystemSetting()
				.getSystemId();
		bid.setClient(client);
		bid.setSystemId(systemId);
		bid.setCurrency(auction.getCurrencyCode());
		bid.setAuctionId(auction.getAuctionId());

		//		Attach the current timestamp to bid when it arrives on server
		//		If Bid timestamp > Auction End Time
		//		Error ?Auction has already ended? (Note: Auction may still be in Active state because Scheduler task 
		//		has not moved it to close state yet)
		bid.validate(true);
		if (auction.getSellerId().equals(bid.getBuyerId())) {
			ErrorMessageBundle bundle = ErrorMessageBundle.getInstance();
			I18nErrorMessage msg =
				bundle.newI18nErrorMessage(
					ValidationException.BIDDER_IS_SELLER);
			bid.addError(msg);
		}
		if (auction.getQuantity() < bid.getQuantity()) {
			ErrorMessageBundle bundle = ErrorMessageBundle.getInstance();
			I18nErrorMessage msg =
				bundle.newI18nErrorMessage(ValidationException.INVALID_BID_QTY);
			bid.addError(msg);
		}
		//whether the bid has surpassed the start price

		if (auction.getStartPrice().compareTo(bid.getBidAmount()) > 0) {
			ErrorMessageBundle bundle = ErrorMessageBundle.getInstance();
			I18nErrorMessage msg =
				bundle.newI18nErrorMessage(
					ValidationException.INVALID_BIDAMOUNT);
			bid.addError(msg);
		}

		checkErrors(bid);
		Connection conn = null;
		boolean invalidBid = false;
		//		Start a DB transaction in Serialized mode for BID table
		try {
			conn = getServiceLocatorBase().getDataSource().getConnection();
			conn.setAutoCommit(false);
			invalidBid =
				checkAuctionStatusAndEndDate(
					auction,
					bid,
					currentTime,
					conn,
					invalidBid);
			if (invalidBid) {
				checkErrors(bid);
			}
			Statement stmt = conn.createStatement();
			BigDecimal highestBid = null;
			String bidderId = null;
			String sqlStmt = null;
			if (auction.getBidType() != BidTypeEnum.SEALED) {
				// Select Highest Bid record for auction only if it is not sealed bid type
				sqlStmt =
					"SELECT BIDPRICE,BPID FROM CRM_ISA_AVW_BID WHERE AUCTIONID=\'"
						+ auction.getAuctionId()
						+ "'"
						+ " ORDER BY BIDPRICE DESC";
				ResultSet rs = stmt.executeQuery(sqlStmt);
				if (rs.next()) {
					highestBid = rs.getBigDecimal(1);
					bidderId = rs.getString(2);
				}
				rs.close();
				stmt.close();
			}
			BigDecimal tmpBid = null;
			boolean sameBidderError = false;
			if (highestBid != null) {
				tmpBid = highestBid;
				if (auction.getBidIncrement() != null) {
					tmpBid = tmpBid.add(auction.getBidIncrement());
					if (tmpBid.compareTo(bid.getBidAmount()) > 0) {
						invalidBid = true;
					}

				} else if (tmpBid.compareTo(bid.getBidAmount()) >= 0) {
					invalidBid = true;
				}
				if (bidderId.equals(bid.getBuyerId())) {
					if (auction.getReservePrice() != null) {
						if (highestBid.compareTo(auction.getReservePrice())
							>= 0) {
							invalidBid = true;
							sameBidderError = true;
						}
					} else {
						invalidBid = true;
						sameBidderError = true;
					}
				}
			}
			//			If the Highest bid + Bid Increment > = Placed bid 
			//				Rollback tx
			//				Return Error ?Bid not accepted. It must exceed current Highest Bid + Bid Increment?
			if (invalidBid) {
				conn.rollback();
				ErrorMessageBundle bundle = ErrorMessageBundle.getInstance();
				I18nErrorMessage msg = null;
				if (bidderId.equals(bid.getBuyerId()) && sameBidderError) {
					msg =
						bundle.newI18nErrorMessage(
							ValidationException.ALREADY_HIGHEST_BIDDER);
				} else
					msg =
						bundle.newI18nErrorMessage(
							ValidationException.INVALID_BIDAMOUNT);
				bid.addError(msg);
				checkErrors(bid);
			}
			if (!testMode) {
				logger.infoT(
					tracer,
					"About to lock the auction table for the auction "
						+ auction.getAuctionName());
				LockingManager.getLockingManager(AUC_TABLE_NAME).lockAuctionRecord(
					locator.getTableLocking(),
					conn,
					auction);
			}

			//			Else 
			//				Insert new Bid record
//			sqlStmt =
//				"INSERT INTO CRM_ISA_AVW_BID (ID, CLIENT,SYSTEMID,AUCTIONID,"
//					+ "BIDPRICE,CURRENCYID,BIDQUANTITY,BIDTIME,BPID,SOLDTOPARTY,USERID)VALUES(?,?,?,?,?,?,?,?,?,?,?)";
			PreparedStatement pstmt = conn.prepareStatement(INSERT_BIDSQL_STMT);
			pstmt.setString(1, GuidGenerator.newGuid().toString());
			pstmt.setString(2, bid.getClient());
			pstmt.setString(3, bid.getSystemId());
			//			pstmt.setString(4, bid.getWebShopId());
			pstmt.setString(4, auction.getAuctionId());
			pstmt.setDouble(5, bid.getBidAmount().doubleValue());
			pstmt.setString(6, bid.getCurrency());
			pstmt.setInt(7, bid.getQuantity());
			pstmt.setLong(8, bid.getBidDate().getTime());
			pstmt.setString(9, bid.getBuyerId());
			pstmt.setString(10, bid.getSoldToParty());
			pstmt.setString(
				11,
				bid.getUserId() != null ? bid.getUserId() : bid.getBuyerId());
			pstmt.execute();
			conn.commit();
			//				Commit tx
			//				Return Success ?Bid accepted?

		} catch (Exception e) {
			//Don't handle validation exceptions
			if (e instanceof ValidationException) {
				throw (ValidationException) e;
			}
			try {
				if (conn != null && !conn.isClosed())
					conn.rollback();
			} catch (SQLException e1) {
				LogHelper.logError(logger, tracer, e1);
			}
			ErrorMessageBundle bundle = ErrorMessageBundle.getInstance();
			I18nErrorMessage msg =
				bundle.newI18nErrorMessage(SystemException.INTERNAL_ERROR);
			SystemException _ex = new SystemException(msg, e);
			super.throwing(METHOD, _ex);
			throw _ex;
		} finally {
			try {
				if (!testMode) {
					logger.infoT(
						tracer,
						"About to unlock the auction table for the auction "
							+ auction.getAuctionName());
					LockingManager.getLockingManager(AUC_TABLE_NAME).unLockAuctionRecord(
							locator.getTableLocking(),
						conn,
						auction);
				}

			} catch (Exception neglect) {
				LogHelper.logError(logger, tracer, neglect);
			}

			try {
				if (conn != null && !conn.isClosed())
					conn.close();
					conn = null;
			} catch (SQLException e1) {
				// neglect
				LogHelper.logError(logger, tracer, e1);
			}
		}

	}

	private boolean checkAuctionStatusAndEndDate(
		PrivateAuction auction,
		PrivateAuctionBid bid,
		long currentTime,
		Connection conn,
		boolean invalidBid)
		throws SQLException {

		Statement stmt = conn.createStatement();
		// Select Highest Bid record for auction only if it is not sealed bid type
		String sqlStmt =
			"SELECT STATUS,ENDDATE FROM CRM_ISA_AVW_AUC WHERE AUCTIONID=\'"
				+ auction.getAuctionId()
				+ "'";
		ResultSet rs = stmt.executeQuery(sqlStmt);
		int status = 0;
		long endDateinMilliSecs = -1;
		if (rs.next()) {
			status = rs.getInt(1);
			endDateinMilliSecs = rs.getLong(2);
			if (rs.wasNull()) {
				endDateinMilliSecs = -1;
			}
		} else {
			invalidBid = true;
		}
		if (!invalidBid) {
			AuctionStatusEnum statusEnum = AuctionStatusEnum.toEnum(status);
			if (statusEnum != AuctionStatusEnum.PUBLISHED) {
				invalidBid = true;
				ErrorMessageBundle bundle = ErrorMessageBundle.getInstance();
				I18nErrorMessage msg =
					bundle.newI18nErrorMessage(
						ValidationException.BID_FOR_CLOSED_AUCTION);
				bid.addError(msg);
			} else {
				if (endDateinMilliSecs > 0
					&& endDateinMilliSecs < currentTime) {
					invalidBid = true;
					ErrorMessageBundle bundle =
						ErrorMessageBundle.getInstance();
					I18nErrorMessage msg =
						bundle.newI18nErrorMessage(
							ValidationException.BID_FOR_CLOSED_AUCTION);
					bid.addError(msg);
				}
			}
		}

		return invalidBid;
	}

	private void checkErrors(PrivateAuctionBid bid) {
		final String METHOD = "checkErrors";
		boolean validBid = true;
		if (bid.getErrors().size() > 0) {
			ArrayList culprits = new ArrayList();
			culprits.add(bid);
			logErrors(bid);
			ErrorMessageBundle bundle = ErrorMessageBundle.getInstance();
			I18nErrorMessage msg =
				bundle.newI18nErrorMessage(SystemException.INTERNAL_ERROR);
			ValidationException ex = new ValidationException(culprits, msg);
			super.throwing(METHOD, ex);
			throw ex;
		}
	}

	/**
	 * Returns query string based on auction id, system id, client id and site id
	 * under which the auction is published on ebay
	 * @param auction
	 * @return
	 */
	private String getBidHistoryQueryString(PrivateAuction auction) {
		StringBuffer filter = new StringBuffer("(");
		filter.append("auctionId == \"");
		filter.append(auction.getAuctionId());
		filter.append("\" && systemId == \"");
		filter.append(auction.getSystemId());
		filter.append("\" && client == \"");
		filter.append(auction.getClient());
		filter.append("\")");
		// && webShopId == \"" + auction.getWebShopId() + "\")");
		return filter.toString();
	}

	private void logErrors(ValidatingBean bean) {
		// log errors
		Iterator it = bean.getErrors().iterator();
		while (it.hasNext()) {
			I18nErrorMessage msg = (I18nErrorMessage) it.next();
			try {
				logger.errorT(tracer, msg.format());
			} catch (LocalizationException e) {
				LogHelper.logError(logger, tracer, e);
			}
		}
	}

	/**
	 * Retreives Bid History
	 * Order is important
	 */
	public List getBidHistory(Auction auction) {
		checkInitialized();
		checkNull(auction);
		InvocationContext ic = (InvocationContext)getInvocationContext();
		UserRoleTypeEnum enum = ic.getRoleType();
		boolean isSeller = false;
		boolean isBuyer = false;
		if (enum == UserRoleTypeEnum.SELLER) {
			isSeller = true;
		} else if (enum == UserRoleTypeEnum.BUYER) {
			isBuyer = true;
		}
		final String METHOD = "getBidHistory";

		// validate that all required attributes are filled and valid
		ServiceLocator provider = getServiceLocatorBase();
		PersistenceManagerFactory pmf = null;
		PersistenceManager pm = null;
		ArrayList result = null;
		try {
			pmf = provider.getPersistenceManagerFactory();
			pm = pmf.getPersistenceManager();
			// load the Auction DAO and navaigate to bids
			Query query = pm.newQuery(PrivateAuctionBidDAO.class);
			// @@todo:: should be an object level association filter
			String filter =
				(getBidHistoryQueryString((PrivateAuction) auction));
			if (auction instanceof PrivateAuction) {
				PrivateAuction pauction = (PrivateAuction) auction;
				if (pauction.getBidType() == BidTypeEnum.SEALED) {
					if (isBuyer) {
						//TODO Filter needs to be set on the BP id
						filter =
							filter
								+ "&& ( userId == \""
								+ ic.getBpId()
								+ "\")";
					}
				}
			}
			logger.infoT(
				tracer,
				"Query Filter in the bid manager is " + filter);
			query.setFilter(filter);
			query.setOrdering("bidTime descending");
			Collection queryResult = (Collection) query.execute();
			result = new ArrayList();
			Iterator it = queryResult.iterator();
			while (it.hasNext()) {
				PrivateAuctionBidDAO bidDAO = (PrivateAuctionBidDAO) it.next();
				PrivateAuctionBid bid = new PrivateAuctionBid();
				DALBeanSynchronizer.syncVBeanFromDAO(bidDAO, bid);
				result.add(bid);
			}
		} catch (JDOException ex) {
			ErrorMessageBundle bundle = ErrorMessageBundle.getInstance();
			I18nErrorMessage msg =
				bundle.newI18nErrorMessage(SystemException.JDO_ERROR);
			SystemException _ex = new SystemException(msg, ex);
			super.throwing(METHOD, _ex);
			throw _ex;
		} catch (Exception ex) {
			ErrorMessageBundle bundle = ErrorMessageBundle.getInstance();
			I18nErrorMessage msg =
				bundle.newI18nErrorMessage(SystemException.INTERNAL_ERROR);
			SystemException _ex = new SystemException(msg, ex);
			super.throwing(METHOD, _ex);
			throw _ex;
		} finally {
			if (pm != null) {
				pm.close();
			}
		}
		return Collections.unmodifiableList(result);
	}

	public Bid[] getHighestBids(Auction auction) {
		checkNull(auction);

		final String METHOD = "getHighestBids";
		// validate that all required attributes are filled and valid

		InvocationContext ic = (InvocationContext)getInvocationContext();
		UserRoleTypeEnum enum = ic.getRoleType();
		boolean isSeller = false;
		boolean isBuyer = false;
		if (enum == UserRoleTypeEnum.SELLER) {
			isSeller = true;
		} else if (enum == UserRoleTypeEnum.BUYER) {
			isBuyer = true;
		}
		ServiceLocator provider = getServiceLocatorBase();
		PersistenceManagerFactory pmf = null;
		PersistenceManager pm = null;
		ArrayList result = null;
		try {
			pmf = provider.getPersistenceManagerFactory();
			pm = pmf.getPersistenceManager();

			// load the Auction DAO and navaigate to bids
			Query query = pm.newQuery(PrivateAuctionBidDAO.class);
			// @@todo:: should be an object level association filter
			String filter = getBidHistoryQueryString((PrivateAuction) auction);
			if (auction instanceof PrivateAuction) {
				PrivateAuction pauction = (PrivateAuction) auction;
				if (pauction.getBidType() == BidTypeEnum.SEALED) {
					if (isBuyer) {
						filter =
							filter
								+ "&& ( userId == \""
								+ ic.getUser().getUserId()
								+ "\")";
					}
				}
			}
			query.setFilter(filter);
			// TODO not necessarily the bidPrice Descending. There might be auctions
			// for which bid price is not the valid cirtierio
			query.setOrdering("bidPrice descending");
			if (tracer.beDebug())
				tracer.debugT("filter string: " + filter.toString());

			Collection queryResult = (Collection) query.execute();
			result = new ArrayList();
			Iterator it = queryResult.iterator();
			while (it.hasNext()) {
				PrivateAuctionBidDAO bidDAO = (PrivateAuctionBidDAO) it.next();
				PrivateAuctionBid bid = new PrivateAuctionBid();
				DALBeanSynchronizer.syncVBeanFromDAO(bidDAO, bid);
				result.add(bid);
			}
		} catch (JDOException ex) {
			ErrorMessageBundle bundle = ErrorMessageBundle.getInstance();
			I18nErrorMessage msg =
				bundle.newI18nErrorMessage(SystemException.JDO_ERROR);
			SystemException _ex = new SystemException(msg, ex);
			super.throwing(METHOD, _ex);
			throw _ex;
		} catch (Exception ex) {
			ErrorMessageBundle bundle = ErrorMessageBundle.getInstance();
			I18nErrorMessage msg =
				bundle.newI18nErrorMessage(SystemException.INTERNAL_ERROR);
			SystemException _ex = new SystemException(msg, ex);

			super.throwing(METHOD, _ex);
			throw _ex;
		} finally {
			if (pm != null) {
				pm.close();
			}
		}
		Bid[] arrResult = new Bid[result.size()];
		result.toArray(arrResult);
		return arrResult;
	}

	private void checkNull(Object obj) {
		if (obj == null) {
			throw new IllegalArgumentException("arg is null");
		}
	}

	/* (non-Javadoc)
	 * @see com.sap.isa.auction.businessobject.BidManager#getBidCount(com.sap.isa.auction.bean.Auction)
	 */
	public long getBidCount(Auction arg0) {
		return 0;
	}

}