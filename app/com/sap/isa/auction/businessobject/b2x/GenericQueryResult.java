package com.sap.isa.auction.businessobject.b2x;

import java.util.Collection;
import java.util.Iterator;

import com.sap.tc.logging.Category;
import com.sap.tc.logging.Location;
import com.sap.isa.auction.bean.search.QueryResult;
import com.sap.isa.auction.logging.CategoryProvider;
import com.sap.isa.core.util.SoftHashMap;

/**
 * Title:			Internet Sales Private Auctions
 * Description:
 * @Copyright: 		Copyright (c) 2004
 * Company:			SAPLabs LLC
 * @author 
 * Created on: 		Jun 1, 2004
 */
public class GenericQueryResult extends QueryResult {
	private static Category logger = CategoryProvider.getBOLCategory();
	private static Location tracer = Location.getLocation(GenericQueryResult.class.getName());
	
	private SoftHashMap cache = new SoftHashMap();
	private int pos = 1;
	private int resultSize;
	private Collection coll;
	
	public GenericQueryResult(Collection coll, int resultSize){
		this.coll = coll;
		this.resultSize = resultSize;
	}
	public int resultSize() {
		usageCheck();
		return resultSize;
	}

	public boolean available() {
		usageCheck();
		return pos <=resultSize();
	}

	public void position(int pos) {
		usageCheck();
		if (pos < 1 || pos > resultSize()) {
			throw new IllegalArgumentException("Position is invalid");
		}
		this.pos = pos;
	}

	public int getPosition() {
		usageCheck();
		return pos;
	}

	public Object[] fetch() {
		usageCheck();
		Object[] ret = new Object[size];
		int index = 1;
		
		if(!available()) return ret;
		if(coll != null) {
			Iterator it = coll.iterator();
			
			// move to current pos. Thats the only way:(
			while(available() && index < pos){
			   index++;
			   it.next(); // index to pos
			}			
			// move pos
			while(available() && pos < index + size) {
				ret[pos - index] = it.next();
				pos++;
			}
		}
		return ret;
	}
}
