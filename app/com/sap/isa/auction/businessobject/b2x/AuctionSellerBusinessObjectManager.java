/*
 * 
 */
package com.sap.isa.auction.businessobject.b2x;

import com.sap.isa.user.businessobject.UserBaseAware;

/**
 * Auction BOM for AVW seller
 * This BOM is UserAware. Implemenation is provided in 
 * <code> AuctionBusinessObjectManagerBase</code>
 * UserAware is moved to SellerBom to prevent clash with ISACORE BOM
 * 
 */
public class AuctionSellerBusinessObjectManager
	extends AuctionBusinessObjectManager implements UserBaseAware{

}
