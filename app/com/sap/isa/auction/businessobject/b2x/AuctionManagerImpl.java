/*
 * Created on Apr 23, 2004
 *
 * Title:        Internet Sales Private Auctions
 * Description:
 * Copyright:    Copyright (c) 2004
 * Company:      SAP Labs LLC
 * @author
 * @version 1.0
 */
package com.sap.isa.auction.businessobject.b2x;

import com.sap.isa.auction.bean.Auction;
import com.sap.isa.auction.bean.AuctionStatusEnum;
import com.sap.isa.auction.bean.search.QueryFilter;
import com.sap.isa.auction.bean.search.QueryResult;
import com.sap.isa.auction.businessobject.AuctionBusinessObjectManagerBase;
import com.sap.isa.auction.businessobject.AuctionManager;
import com.sap.isa.auction.services.ServiceLocator;

/**
 * @author I802850
 *
 * To change the template for this generated type comment go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
public class AuctionManagerImpl implements AuctionManager {

	/* (non-Javadoc)
	 * @see com.sap.isa.auction.businessobject.AuctionManager#createAuction(com.sap.isa.auction.bean.Auction)
	 */
	public void createAuction(Auction auction) {
		// TODO Auto-generated method stub
		
	}

	/* (non-Javadoc)
	 * @see com.sap.isa.auction.businessobject.AuctionManager#createAuctions(com.sap.isa.auction.bean.Auction[])
	 */
	public void createAuctions(Auction[] auctions) {
		// TODO Auto-generated method stub
		
	}

	/* (non-Javadoc)
	 * @see com.sap.isa.auction.businessobject.AuctionManager#createAuctionByRef(com.sap.isa.auction.bean.Auction, com.sap.isa.auction.bean.Auction)
	 */
	public void createAuctionByRef(Auction auction, Auction refAuction) {
		// TODO Auto-generated method stub
		
	}

	/* (non-Javadoc)
	 * @see com.sap.isa.auction.businessobject.AuctionManager#getAuctionById(java.lang.String)
	 */
	public Auction getAuctionById(String auctionId) {
		// TODO Auto-generated method stub
		return null;
	}

	/* (non-Javadoc)
	 * @see com.sap.isa.auction.businessobject.AuctionManager#getAuctionTemplateById(java.lang.String)
	 */
	public Auction getAuctionTemplateById(String templateId) {
		// TODO Auto-generated method stub
		return null;
	}

	/* (non-Javadoc)
	 * @see com.sap.isa.auction.businessobject.AuctionManager#modifyStatus(com.sap.isa.auction.bean.Auction, com.sap.isa.auction.bean.AuctionStatusEnum)
	 */
	public void modifyStatus(Auction auction, AuctionStatusEnum newStatus) {
		// TODO Auto-generated method stub
		
	}

	/* (non-Javadoc)
	 * @see com.sap.isa.auction.businessobject.AuctionManager#modifyAuction(com.sap.isa.auction.bean.Auction)
	 */
	public void modifyAuction(Auction auction) {
		// TODO Auto-generated method stub
		
	}

	/* (non-Javadoc)
	 * @see com.sap.isa.auction.businessobject.AuctionManager#deleteAuction(com.sap.isa.auction.bean.Auction)
	 */
	public void deleteAuction(Auction auction) {
		// TODO Auto-generated method stub
		
	}

	/* (non-Javadoc)
	 * @see com.sap.isa.auction.businessobject.AuctionManager#searchAuctions(com.sap.isa.auction.bean.search.QueryFilter)
	 */
	public QueryResult searchAuctions(QueryFilter filer) {
		// TODO Auto-generated method stub
		return null;
	}

	/* (non-Javadoc)
	 * @see com.sap.isa.auction.businessobject.AuctionBusinessObject#initialize()
	 */
	public void initialize() {
		// TODO Auto-generated method stub
		
	}

	/* (non-Javadoc)
	 * @see com.sap.isa.auction.businessobject.AuctionBusinessObject#setAuctionBOM(com.sap.isa.auction.businessobject.AuctionBusinessObjectManagerBase)
	 */
	public void setAuctionBOM(AuctionBusinessObjectManagerBase bom) {
		// TODO Auto-generated method stub
		
	}

	/* (non-Javadoc)
	 * @see com.sap.isa.auction.businessobject.AuctionBusinessObject#close()
	 */
	public void close() {
		// TODO Auto-generated method stub
		
	}

	/* (non-Javadoc)
	 * @see com.sap.isa.auction.businessobject.ServiceAware#setServiceLocator(com.sap.isa.auction.services.ServiceLocator)
	 */
	public void setServiceLocator(ServiceLocator locator) {
		// TODO Auto-generated method stub
		
	}

	/* (non-Javadoc)
	 * @see com.sap.isa.auction.businessobject.ServiceAware#getServiceLocatorBase()
	 */
	public ServiceLocator getServiceLocatorBase() {
		// TODO Auto-generated method stub
		return null;
	}


}
