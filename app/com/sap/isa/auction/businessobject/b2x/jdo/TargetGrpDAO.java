/*
 * Created on Apr 21, 2004
 *
 * Title:        Internet Sales Asset Remarketing
 * Description:
 * Copyright:    Copyright (c) 2004
 * Company:      SAP Labs LLC
 * @author
 * @version 1.0
 */
package com.sap.isa.auction.businessobject.b2x.jdo;

import java.io.Serializable;

/**
 * @author I802850
 *
 * To change the template for this generated type comment go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
public class TargetGrpDAO {
	private String id;
	private String targetId;
	private String auctionId;
	// defaulted to CRM Target Group, Target Group == false => CRM BusinessPartner
	private short targetGrp = 1;
	private String client;
	private String systemId;
	
	public TargetGrpDAO(String id){
		this.id = id;
	}
	public TargetGrpDAO(){
	}
	
	/**
	 * JDO Application Identity Class
	 */
	public static class Id implements Serializable {

		public String id;

		public Id() {
		}

		public Id(String id) {
			this.id = id;
		}

		/**
		 * @return int hashcode
		 */
		public int hashCode() {
			int result = 17;
			result =
				result * 37 + (id == null ? 0 : id.hashCode());
			return result;
		}

		/**
		 *
		 */
		public boolean equals(Object obj) {
			boolean ret = false;
			if (obj == this)
				return true;
			if (obj instanceof Id) {
				if (this.id.equals(((Id)obj).id))
					ret = true;
			}

			return ret;
		}

		public String toString() {
			return id;
		}
	}
	
	/**
	 * @return
	 */
	public String getAuctionId() {
		return auctionId;
	}

	/**
	 * @return
	 */
	public boolean isTargetGrp() {
		return targetGrp == 1;
	}

	/**
	 * @return
	 */
	public String getTargetId() {
		return targetId;
	}

	/**
	 * @param l
	 */
	public void setAuctionId(String s) {
		auctionId = s;
	}

	/**
	 * @param b
	 */
	public void setTargetGrp(boolean b) {
		if(b)
		targetGrp = 1;
		else targetGrp = -1;
	}

	/**
	 * @param string
	 */
	public void setTargetId(String string) {
		targetId = string;
	}

	/**
	 * @return
	 */
	public String getId() {
		return id;
	}

	/**
	 * @param string
	 */
	public void setId(String string) {
		id = string;
	}

	/**
	 * @return
	 */
	public String getClient() {
		return client;
	}

	/**
	 * @return
	 */
	public String getSystemId() {
		return systemId;
	}

	/**
	 * @param string
	 */
	public void setClient(String string) {
		client = string;
	}

	/**
	 * @param string
	 */
	public void setSystemId(String string) {
		systemId = string;
	}
}
