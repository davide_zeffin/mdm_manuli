package com.sap.isa.auction.businessobject.b2x.jdo;

import java.io.Serializable;

/**
 * Title:        Internet Sales
 * Description:  
 * Copyright:    Copyright (c) 2002
 * Company:      SAPMarkets
 * @author 
 * @version 1.0
 */
public class PrivateAuctionItemDAO {

	private String client;
	private String systemId;
	private String productId;
	private String productCode;
	private double quantity;
	private String auctionId;
	private String id;
	private String description;
	private String uom;
	private String image;

	public PrivateAuctionItemDAO() {
	}
	public PrivateAuctionItemDAO(String id) {
		this.id = id;
	}


	/**
	 * JDO Application Identity Class
	 */
	public static class Id implements Serializable {

		public String id;
		public Id() {
		}
		public Id(String id) {
			this.id = id;
		}

		/**
		 * @return int hashcode
		 */
		public int hashCode() {
			int result = 17;
			result = result * 37 + (id == null ? 0 : id.hashCode());
			return result;
		}

		/**
		 *
		 */
		public boolean equals(Object obj) {
			boolean ret = false;
			if (obj instanceof Id) {
				if (this.id.equals(((Id)obj).id))
					ret = true;
			}
			return ret;
		}
	}

	public void setProductId(String productId) {
		this.productId = getOpenSqlCompatibleString(productId);
	}
	public String getProductId() {
		return this.productId;
	}
	public void setProductCode(String productCode) {
		this.productCode = getOpenSqlCompatibleString(productCode);
	}
	public String getProductCode() {
		return this.productCode;
	}
	public void setQuantity(double quantity) {
		this.quantity = quantity;
	}
	public double getQuantity() {
		return this.quantity;
	}
	public void setAuctionId(String auctionId) {
		this.auctionId = getOpenSqlCompatibleString(auctionId);
	}
	public String getAuctionId() {
		return this.auctionId;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getId() {
		return this.id;
	}

	/**
	 * @return
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * @param string
	 */
	public void setDescription(String string) {
		description = getOpenSqlCompatibleString(string);
	}

	/**
	 * @return
	 */
	public String getUom() {
		return uom;
	}

	/**
	 * @param string
	 */
	public void setUom(String string) {
		uom = string;
	}

	/**
	 * @return
	 */
	public String getImage() {
		return image;
	}



	/**
	 * @param string
	 */
	public void setImage(String string) {
		image = getOpenSqlCompatibleString(string);
	}




	/**
	 * Important utility function since open sql does not allow
	 * blank strings to be persisted nor the blank strings
	 * we will try to store null
	 * @param str
	 * @return
	 */
	private String getOpenSqlCompatibleString(String str){
		if(str==null ) return null;
		if(str.trim().length() == 0) return null;
		return str.trim();
	}
	/**
	 * @return
	 */
	public String getClient() {
		return client;
	}

	/**
	 * @return
	 */
	public String getSystemId() {
		return systemId;
	}

	/**
	 * @param string
	 */
	public void setClient(String string) {
		client = string;
	}



	/**
	 * @param string
	 */
	public void setSystemId(String string) {
		systemId = string;
	}
}
