/*
 * Created on Oct 18, 2003
 *
 */
package com.sap.isa.auction.businessobject.b2x.jdo;

import com.sap.exception.standard.*;
import com.sap.isa.auction.util.OpenSQLUtil;

/**
 * @author I802891
 *
 */
public final class KeyValueDAO {

	private String id;
	private String name;
	private String value;
	private String dataDiscriminator;
	private String groupName;
	private String userId;	
	private String webShopId;
	private String systemId;
	private String client;

	public static class Id {
		public String id;

		public Id() {
		}

		public Id(String id) {
			this.id = id;
		}
		
		/* (non-Javadoc)
		 * @see java.lang.Object#equals(java.lang.Object)
		 */
		public boolean equals(Object obj) {
			if(!(obj instanceof Id)) {
				return false;
			}
			
			Id other = (Id)obj;
			
			// quick check
			if(id == other.id) {
				return true;
			}
			 
			if(id != null && id.equals(other.id)) {
				return true;
			}
			else {
				return false;
			}
		}

		/* (non-Javadoc)
		 * @see java.lang.Object#hashCode()
		 */
		public int hashCode() {
			int hash = 0;
			
			if(id != null) {
				hash = id.hashCode();
			}
			
			if(hash == 0) hash = super.hashCode();
			
			return hash;
		}
		
		public String toString() {
			return id;
		}
	}
	
	/**
	 * @return
	 */
	public String getGroupName() {
		return groupName;
	}

	/**
	 * @return
	 */
	public String getName() {
		return name;
	}

	/**
	 * @return
	 */
	public String getValue() {
		return value;
	}

	/**
	 * MAX of 80 Chars
	 * @param string
	 */
	public void setGroupName(String string) {
		StandardResourceAccessor bundle = StandardResourceAccessor.getInstance();		
		if(string == null) {
			throw new SAPIllegalArgumentException(SAPIllegalArgumentException.PARAMETER_NULL, new Object[]{"groupName"});
		}
		
		if(string.length() > 50) {
			throw new SAPIllegalArgumentException(SAPIllegalArgumentException.PARAMETER_LENGTH_TOO_HIGH, new Object[]{"groupName","50"});
		}

		groupName = string;
	}

	/**
	 * MAX of 80 chars
	 * @param string
	 */
	public void setName(String string) {
		if(string == null) {
			throw new SAPIllegalArgumentException(SAPIllegalArgumentException.PARAMETER_NULL, new Object[]{"name"});
		}
		
		if(string.length() > 80) {
			throw new SAPIllegalArgumentException(SAPIllegalArgumentException.PARAMETER_LENGTH_TOO_HIGH, new Object[]{"name","80"});
		}
		name = string;
	}

	/**
	 * MAX of 120 chars
	 * @param string
	 */
	public void setValue(String string) {
		if(string != null && string.length() > 1024) {
			throw new SAPIllegalArgumentException(SAPIllegalArgumentException.PARAMETER_LENGTH_TOO_HIGH, new Object[]{"value","1024"});
		}
		
		value = OpenSQLUtil.toOpenSQLCompliantString(string);
	}
	
	/**
	 * @return
	 */
	public String getId() {
		return id;
	}

	/**
	 * @param string
	 */
	public void setId(String string) {
		id = string;
	}
	
	/**
	 * @return
	 */
	public String getClient() {
		return client;
	}

	/**
	 * @return
	 */
	public int getWebShopId() {
		if(webShopId != null) {
			return Integer.parseInt(webShopId);
		}
		else {
			return -1;
		}
	}


	/**
	 * @param string
	 */
	public void setClient(String string) {
		client = string;
	}

	/**
	 * @param string
	 */
	public void setWebShopId(int val) {
		webShopId = Integer.toString(val);
	}

	/**
	 * @return
	 */
	public String getSystemId() {
		return systemId;
	}

	/**
	 * @param string
	 */
	public void setSystemId(String string) {
		systemId = string;
	}	
	/**
	 * @return
	 */
	public String getDataDiscriminator() {
		return dataDiscriminator;
	}

	/**
	 * @param string
	 */
	public void setDataDiscriminator(String string) {
		StandardResourceAccessor bundle = StandardResourceAccessor.getInstance();		
		if(string == null) {
			throw new SAPIllegalArgumentException(SAPIllegalArgumentException.PARAMETER_NULL, new Object[]{"dataDiscriminator"});
		}
		
		if(string.length() > 50) {
			throw new SAPIllegalArgumentException(SAPIllegalArgumentException.PARAMETER_LENGTH_TOO_HIGH, new Object[]{"dataDiscriminator","50"});
		}
		
		dataDiscriminator = string;
	}

	/**
	 * @return
	 */
	public String getUserId() {
		return userId;
	}

	/**
	 * @param string
	 */
	public void setUserId(String string) {
		userId = string;
	}
}
