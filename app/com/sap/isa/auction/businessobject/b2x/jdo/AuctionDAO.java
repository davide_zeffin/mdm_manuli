package com.sap.isa.auction.businessobject.b2x.jdo;

import java.io.Serializable;
import java.math.BigDecimal;
import java.sql.Timestamp;

import com.sap.isa.auction.bean.ExecutionStatusEnum;

public class AuctionDAO {
	private String client;
	private String systemId;
	private String auctionId; // GUID
	private short isAuctionTemplate = -1;
	private String auctionName;
	private int auctionType;
	private String country;
	private String currencyId;
	private String description;
	private long endDate = -1;
	private long closeDate = -1;
	private double startPrice = -1;
	private int quantity;
	private long quotDurationTime = -1;
	// in millisecs relative to the start date or enddate
	private String quotationId;
	private double reservePrice = -1;
	private long publishDate = -1;
	private int status;
	private String salesArea;
	private String title;
	private String terms;
	private String createdBy;
	private long startDate = -1;
	private long createdDate = -1;
	private long modifiedDate = -1;
	private short isPrivate = -1;
	private String imageURL;

	/**
	 * New and missing variables from the bean
	 */
	private long durationInMillSecs;
	private String bpId;
	private String refAucId;

	private short execStatus = -1;
	private String execRequestId;

	
	public AuctionDAO() {
	}

	/** 
	 * JDO Application Identity Class
	 */
	public static class Id implements Serializable {
		public String auctionId;

		public Id() {
		}

		public Id(String auctionId) {
			this.auctionId = auctionId;
		}

		/**
		 * @return int hashcode
		 */
		public int hashCode() {
			int result = 17;
			result =
				result * 37 + (auctionId == null ? 0 : auctionId.hashCode());
			return result;
		}

		/**
		 *
		 */
		public boolean equals(Object obj) {
			boolean ret = false;
			if (obj == this)
				return true;
			if (obj instanceof Id) {
				if (this.auctionId.equals(((Id)obj).auctionId))				
					ret = true;
			}

			return ret;
		}

		public String toString() {
			return auctionId;
		}
	}

	public AuctionDAO(String auctionId) {
		this.auctionId = auctionId;
	}

	public long getDurationInMillisecs() {
		return durationInMillSecs;
	}

	/**
	 * This function takes the auction duration in millisecs
	 */
	public void setDurationInMillisecs(long duration) {
		this.durationInMillSecs = duration;
	}

	public void setAuctionName(String auctionName) {
		this.auctionName = getOpenSqlCompatibleString(auctionName);
	}
	public String getAuctionName() {
		return this.auctionName;
	}
	public void setAuctionType(int auctionType) {
		this.auctionType = auctionType;
	}
	public int getAuctionType() {
		return this.auctionType;
	}
	public void setAuctionId(String auctionId) {
		this.auctionId = getOpenSqlCompatibleString(auctionId);
	}
	public String getAuctionId() {
		return this.auctionId;
	}
	public void setCountry(String country) {
		this.country = getOpenSqlCompatibleString(country);;
	}
	public String getCountry() {
		return this.country;
	}
	public void setCurrencyId(String currencyId) {
		this.currencyId = getOpenSqlCompatibleString(currencyId);
	}
	public String getCurrencyId() {
		return this.currencyId;
	}
	public void setDescription(String description) {
		this.description = getOpenSqlCompatibleString(description);
	}
	public String getDescription() {
		return this.description;
	}
	public void setEndDate(Timestamp endDate) {
		if (endDate != null)
			this.endDate = endDate.getTime();
	}
	public Timestamp getEndDate() {
		if (endDate < 0)
			return null;
		return new Timestamp(this.endDate);
	}

	public void setCloseDate(Timestamp date) {
		if (date != null)
			this.closeDate = date.getTime();
	}
	public Timestamp getCloseDate() {
		if (closeDate < 0)
			return null;
		return new Timestamp(this.closeDate);
	}

	public void setAuctionTemplate(boolean isAucTemplate) {
		if (isAucTemplate)
			this.isAuctionTemplate = 1;
		else
			this.isAuctionTemplate = -1;

	}
	public boolean isAuctionTemplate() {
		return this.isAuctionTemplate > 0;
	}

	public void setStartPrice(BigDecimal startPrice) {
		if (startPrice != null)
			this.startPrice = startPrice.doubleValue();
	}
	public BigDecimal getStartPrice() {
		if (startPrice < 0)
			return null;
		return new BigDecimal(this.startPrice);
	}
	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}
	public int getQuantity() {
		return this.quantity;
	}
	public void setQuotDurationTime(long quotDurationTime) {
		this.quotDurationTime = quotDurationTime;
	}
	public long getQuotDurationTime() {
		return this.quotDurationTime;
	}
	public void setQuotationId(String quotationId) {
		this.quotationId = getOpenSqlCompatibleString(quotationId);
	}
	public String getQuotationId() {
		return this.quotationId;
	}
	public void setReservePrice(BigDecimal reservePrice) {
		if (reservePrice != null)
			this.reservePrice = reservePrice.doubleValue();
	}
	public BigDecimal getReservePrice() {
		if(reservePrice< 0)
		return null;
		return new BigDecimal(this.reservePrice);
	}
	public void setPublishDate(Timestamp publishDate) {
		if (publishDate != null)
			this.publishDate = publishDate.getTime();
	}

	public Timestamp getPublishDate() {
		if (publishDate < 0)
			return null;
		return new Timestamp(this.publishDate);
	}

	public void setTitle(String title) {
		this.title = getOpenSqlCompatibleString(title);
	}
	public String getTitle() {
		return this.title;
	}
	public void setTerms(String terms) {
		this.terms = getOpenSqlCompatibleString(terms);
	}
	public String getTerms() {
		return this.terms;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = getOpenSqlCompatibleString(createdBy);;
	}
	public String getCreatedBy() {
		return this.createdBy;
	}

	public Timestamp getCreatedDate() {
		if (createdDate != -1)
			return new Timestamp(createdDate);
		return null;
	}

	public void setCreatedDate(Timestamp createdDate) {
		if (createdDate != null)
			this.createdDate = createdDate.getTime();
	}

	public Timestamp getModifiedDate() {
		if (modifiedDate != -1)
			return new Timestamp(modifiedDate);
		return null;
	}

	public void setModifiedDate(Timestamp modifiedDate) {
		if (modifiedDate != null)
			this.modifiedDate = modifiedDate.getTime();
	}

	public Timestamp getStartDate() {
		if (startDate != -1)
			return new Timestamp(startDate);
		return null;
	}

	public void setStartDate(Timestamp startDate) {
		if (startDate != null)
			this.startDate = startDate.getTime();
	}

	public boolean isPrivate() {
		return isPrivate > 0;
	}

	public void setPrivate(boolean isPrivate) {
		if (isPrivate)
			this.isPrivate = 1;
		else
			this.isPrivate = -1;
	}

	public String getImageURL() {
		return imageURL;
	}

	public void setImageURL(String imageURL) {
		this.imageURL = getOpenSqlCompatibleString(imageURL);
	}

	public void setStatus(int status) {
		this.status = status;
	}
	public int getStatus() {
		return this.status;
	}

	public void setSellerId(String sellerId) {
		this.bpId = getOpenSqlCompatibleString(sellerId);
	}
	public String getSellerId() {
		return this.bpId;
	}

	/**
	 * @return the sales Area
	 */
	public String getSalesArea() {
		return salesArea;
	}

	/**
	 * @param string sales Area
	 */
	public void setSalesArea(String string) {
		salesArea = getOpenSqlCompatibleString(string);
	}


	/**
	 * @return
	 */
	public String getRefAucId() {
		return refAucId;
	}

	/**
	 * @param string
	 */
	public void setRefAucId(String string) {
		refAucId = getOpenSqlCompatibleString(string);
	}

	/**
	 * Important utility function since open sql does not allow
	 * blank strings to be persisted nor the blank strings
	 * we will try to store null
	 * @param str
	 * @return
	 */
	private String getOpenSqlCompatibleString(String str){
		if(str==null ) return null;
		if(str.trim().length() == 0) return null;
		return str.trim();
	}

	/**
	 * @return
	 */
	public String getClient() {
		return client;
	}


	/**
	 * @return
	 */
	public String getSystemId() {
		return systemId;
	}

	/**
	 * @param string
	 */
	public void setClient(String string) {
		client = string;
	}

	/**
	 * @param string
	 */
	public void setSystemId(String string) {
		systemId = string;
	}

	/**
	 * @return
	 */
	public String getExecutionRequestId() {
		return execRequestId;
	}

	/**
	 * @return
	 */
	public ExecutionStatusEnum getExecutionStatus() {
		if(execStatus == -1) return null;
		return ExecutionStatusEnum.toEnum(execStatus);
	}

	/**
	 * @param string
	 */
	public void setExecutionRequestId(String string) {
		execRequestId = string;
	}

	/**
	 * @param s
	 */
	public void setExecutionStatus(ExecutionStatusEnum s) {
		if(s != null)
			execStatus = (short)s.getValue();
		else
			execStatus = -1;
	}


}
