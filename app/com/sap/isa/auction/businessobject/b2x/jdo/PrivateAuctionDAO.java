package com.sap.isa.auction.businessobject.b2x.jdo;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.lang.reflect.Field;
import java.math.BigDecimal;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

import com.sap.isa.auction.bean.b2x.PrivateAuction;

public class PrivateAuctionDAO extends AuctionDAO{
	private double bidIncrement = -1;
	private double buyItNowPrice = -1;
	private int visibilityType;
	private int bidType;
	private int winnerDeterminationType;
	private String webShopId;
	private String client;
	private String systemId;
	private Set auctionItems; // 1 to N Association
	private Set winners; // 1 to N Association
	private Set bids;
	private Set targetGroups;

	public PrivateAuctionDAO() {
		super();
	}

	public void setBidIncrement(BigDecimal bidIncrement) {
		if (bidIncrement != null)
			this.bidIncrement = bidIncrement.doubleValue();
	}
	public BigDecimal getBidIncrement() {
		if (bidIncrement < 0)
			return null;
		return new BigDecimal(this.bidIncrement);
	}

	public void addAuctionItem(PrivateAuctionItemDAO auctionItem) {
		if (auctionItems == null)
			auctionItems = new HashSet();
		if (!auctionItems.contains(auctionItem)) {
			auctionItems.add(auctionItem);
			auctionItem.setAuctionId(getAuctionId());
		}
	}

	public void addAuctionItems(PrivateAuctionItemDAO[] _auctionItems) {
		if (auctionItems == null)
			auctionItems = new HashSet();
		for (int i = 0; i < _auctionItems.length; i++) {
			auctionItems.add(_auctionItems[i]);
		}
	}

	public void removeAuctionItem(PrivateAuctionItemDAO item) {
		if (auctionItems != null)
			auctionItems.remove(item);
	}

	public Set getAuctionItems() {
		return auctionItems;
	}


	public void addTargetGrp(TargetGrpDAO tgtGrp) {
		if (targetGroups == null)
			targetGroups = new HashSet();
		if (!targetGroups.contains(tgtGrp)) {
			targetGroups.add(tgtGrp);
			tgtGrp.setAuctionId(getAuctionId());
		}
	}

	public void addTargetGrps(TargetGrpDAO[] tgtGrps) {
		if (targetGroups == null)
		targetGroups = new HashSet();
		for (int i = 0; i < tgtGrps.length; i++) {
			targetGroups.add(tgtGrps[i]);
		}
	}

	public void removeTargetGrp(TargetGrpDAO tgtGrp) {
		if (auctionItems != null)
			targetGroups.remove(tgtGrp);
	}


	public void addWinner(PrivateAuctionWinnerDAO winner) {
		if (winners == null)
			winners = new HashSet();
		if (!winners.contains(winner)) {
			winners.add(winner);
		}
	}

	public void setWinners(PrivateAuctionWinnerDAO[] winners) {
		for (int i = 0; i < winners.length; i++) {
			addWinner(winners[i]);
		}
	}

	public void removeWinner(PrivateAuctionWinnerDAO winner) {
		if (winners != null)
			winners.remove(winner);
	}

	public Set getWinners() {
		return winners;
	}

	public void addBid(PrivateAuctionBidDAO bid) {
		if (bids == null)
			bids = new HashSet();
		if (!bids.contains(bid)) {
			bids.add(bid);
		}
	}

	public void setBids(PrivateAuctionBidDAO[] bids) {
		for (int i = 0; i < bids.length; i++) {
			addBid(bids[i]);
		}
	}

	public void removeBid(PrivateAuctionBidDAO bid) {
		if (bids != null)
			bids.remove(bid);
	}

	public Set getBids() {
		return bids;
	}

	/**
	 * Fetches the auctionitemdao within the current collection with the specified id
	 */
	public PrivateAuctionItemDAO getAuctionItem(String id) {
		if (auctionItems == null)
			return null;
		Iterator it = auctionItems.iterator();
		while (it.hasNext()) {
			PrivateAuctionItemDAO item = (PrivateAuctionItemDAO)it.next();
			if (item.getId().equals(id))
				return item;
		}
		return null;
	}

	/**
	 * Fetches the auctionitemdao within the current collection with the specified id
	 */
	public TargetGrpDAO getTargetGrp(String id, boolean tgtGrp) {
		if (targetGroups == null)
			return null;
		Iterator it = targetGroups.iterator();
		while (it.hasNext()) {
			TargetGrpDAO tgtDAO = (TargetGrpDAO)it.next();
			if (tgtDAO.isTargetGrp() == tgtGrp && tgtDAO.getTargetId().equals(id))
				return tgtDAO;
		}
		return null;
	}

	public static void main(String[] args) {
		try {

			BufferedWriter fw3 =
				new BufferedWriter(
					new FileWriter("c:\\jdo.map"));
			BufferedWriter fw4 =
				new BufferedWriter(
					new FileWriter("c:\\jdo.jdo"));

			Class [] classes = new Class[] {KeyValueDAO.class,PrivateAuction.class,
											PrivateAuctionBidDAO.class,PrivateAuctionItemDAO.class,
											PrivateAuctionWinnerDAO.class,TargetGrpDAO.class};
            
			String [] tables = new String[] {"CRM_ISA_AVW_KEYVAL","CRM_ISA_AVW_B2XAUC",
											"CRM_ISA_AVW_BID","CRM_ISA_AVW_AUCITM",
											"CRM_ISA_AVW_WIN","CRM_ISA_AVW_TGT"};

			for(int classCount = 0 ; classCount < classes.length; classCount++) {
				Class clazz = classes[classCount];
				Field[] fields = clazz.getDeclaredFields();
				fw4.newLine();
				fw4.write("<class name=\""+ clazz.getName()+"\" identity-type=\"application\" objectid-class = \"" +clazz.getName() +"$Id\">");
				fw3.newLine();
				fw3.write("<class name=\""+ clazz.getName()+"\">");
				for(int i = 0 ; i < fields.length;i++) {
					String attr = fields[i].getName();
					attr = attr.trim();
					fw3.write("\t<field name=\"" + attr + "\">");
					fw3.newLine();
					fw3.write(
						"\t\t<column name=\""
							+ attr.toUpperCase()
							+ "\""
							+ " table=\""+ tables[classCount]+"\"/>");
					fw3.newLine();
					fw3.write("\t</field>");
					fw3.newLine();

					/**
					 * .jdo related changes
					 */
					//                  System.out.println("\t<field name=\"" + attr+"\"" + " persistence-modifier=\"persistent\"/>");
					fw4.write(
						"\t<field name=\""
							+ attr
							+ "\""
							+ " persistence-modifier=\"persistent\"/>");
					fw4.newLine();
					//                  <field name="webshopId" persistence-modifier="persistent"/>
				}
				fw3.write("</class>");
				fw4.write("</class>");

			}
			fw3.close();
			fw4.close();            
		} catch (Exception ex) {
			ex.printStackTrace();
		}

	}
	/**
	 * @return
	 */
	public Set getTargetGroups() {
		return targetGroups;
	}

	/**
	 * @param set
	 */
	public void setTargetGroups(Set set) {
		targetGroups = set;
	}

	/**
	 * @return
	 */
	public BigDecimal getBuyItNowPrice() {
		return new BigDecimal(buyItNowPrice);
	}

	/**
	 * @return
	 */
	public int getVisibilityType() {
		return visibilityType;
	}

	/**
	 * @param d
	 */
	public void setBuyItNowPrice(BigDecimal d) {
		if(d!=null)
			buyItNowPrice = d.doubleValue();
	}

	/**
	 * @param i
	 */
	public void setVisibilityType(int i) {
		visibilityType = i;
	}

	/**
	 * @return
	 */
	public int getBidType() {
		return bidType;
	}

	/**
	 * @param i
	 */
	public void setBidType(int i) {
		bidType = i;
	}

	/**
	 * @return
	 */
	public String getShopId() {
		return webShopId;
	}

	/**
	 * @param string
	 */
	public void setShopId(String string) {
		webShopId = string;
	}
	/**
	 * @return
	 */
	public String getClient() {
		return client;
	}

	/**
	 * @return
	 */
	public String getSystemId() {
		return systemId;
	}

	/**
	 * @return
	 */
	public String getWebShopId() {
		return webShopId;
	}

	/**
	 * @param string
	 */
	public void setClient(String string) {
		client = string;
		super.setClient(string);
	}

	/**
	 * @param string
	 */
	public void setSystemId(String string) {
		systemId = string;
		super.setSystemId(string);
	}

	/**
	 * @param string
	 */
	public void setWebShopId(String string) {
		webShopId = string;
	}


	/**
	 * @return
	 */
	public int getWinnerDeterminationType() {
		return winnerDeterminationType;
	}

	/**
	 * @param i
	 */
	public void setWinnerDeterminationType(int i) {
		winnerDeterminationType = i;
	}

}
