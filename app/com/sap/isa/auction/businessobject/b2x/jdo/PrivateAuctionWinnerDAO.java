package com.sap.isa.auction.businessobject.b2x.jdo;

/**
 * Title:        Internet Sales
 * Description:  
 * Copyright:    Copyright (c) 2002
 * Company:      SAPMarkets
 * @author 
 * @version 1.0
 */
import java.io.Serializable;
import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.Date;
public class PrivateAuctionWinnerDAO {

	private String client;
	private String systemId;
	
	private String auctionId;
	private double bidPrice = -1;
	private String userId;
	private String bpId;
	private String orderGuid;	
	private String orderId;
	private int quantity;
	private String id;
	private short checkedOut = -1;
	private String soldToParty;
	
	private long createDate = -1;
	
	private PrivateAuctionBidDAO bid;
	
	/**
	 * JDO Application Identity Class
	 */
	public static class Id implements Serializable{

		public String id;

		public Id() {
		}

		public Id(String id) {
			this.id = id;
		}

		/**
		 * @return int hashcode
		 */
		public int hashCode() {
		int result = 17;
		result = result *37 + (id == null?0:id.hashCode());
		return result;
		}

		/**
		 *
		 */
		public boolean equals(Object obj) {
			boolean ret = false;
		if(obj == this)    return true;
			if(obj instanceof Id) {
				if(this.id.equals( ((Id)obj).id)) ret = true;
			}

			return ret;
		}

		public String toString() {
			return id;
		}
	}

	public void setAuctionId(String auctionId){ 
		this.auctionId= getOpenSqlCompatibleString(auctionId);
	}
	public String getAuctionId(){
		return this.auctionId;
	}
	public void setBidPrice(BigDecimal bidPrice){ 
		if(bidPrice!=null)
			this.bidPrice= bidPrice.doubleValue();
	}
	public BigDecimal getBidPrice(){
		if(bidPrice < 0 ) return null;
		return new BigDecimal(this.bidPrice);
	}
	public void setOrderGuid(String orderGuid){
		this.orderGuid = getOpenSqlCompatibleString(orderGuid);
	}
	public String getOrderGuid(){
		return this.orderGuid;
	}
	public void setOrderId(String orderId){ 
		this.orderId= getOpenSqlCompatibleString(orderId);
	}
	public String getOrderId(){
		return this.orderId;
	}
	public void setQuantity(int quantity){ 
		this.quantity= quantity;
	}
	public int getQuantity(){
		return this.quantity;
	}
	public void setId(String id){ 
		this.id = id;

	}
	public String getId(){
		return this.id;
	}	

	public void setUserId(String id){ 
		this.userId = getOpenSqlCompatibleString(id);

	}
	public String getUserId(){
		return this.userId;
	}	

	public void setBpId(String id){ 
		this.bpId = getOpenSqlCompatibleString(id);

	}
	public String getBpId(){
		return this.bpId;
	}	

	
	public PrivateAuctionWinnerDAO() {
	}

	/**
	 * Important utility function since open sql does not allow
	 * blank strings to be persisted nor the blank strings
	 * we will try to store null
	 * @param str
	 * @return
	 */
	private String getOpenSqlCompatibleString(String str){
		if(str==null ) return null;
		if(str.trim().length() == 0) return null;
		return str.trim();
	}
	/**
	 * @return
	 */
	public String getClient() {
		return client;
	}

	/**
	 * @return
	 */
	public String getSystemId() {
		return systemId;
	}

	/**
	 * @param string
	 */
	public void setClient(String string) {
		client = string;
	}
	/**
	 * @param string
	 */
	public void setSystemId(String string) {
		systemId = string;
	}
	/**
	 * @param d
	 */
	public void setBidPrice(double d) {
		bidPrice = d;
	}

	/**
	 * @return
	 */
	public boolean getCheckedOut() {
		return checkedOut > 0;
	}

	/**
	 * @param s
	 */
	public void setCheckedOut(boolean b) {
		if(b)
			checkedOut = 1;
		else checkedOut = -1;
	}

	/**
	 * @return
	 */
	public String getSoldToParty() {
		return soldToParty;
	}

	/**
	 * @param string
	 */
	public void setSoldToParty(String string) {
		soldToParty = string;
	}

	/**
	 * @return
	 */
	public PrivateAuctionBidDAO getBid() {
		return bid;
	}

	/**
	 * @param bidDAO
	 */
	public void setBid(PrivateAuctionBidDAO bidDAO) {
		bid = bidDAO;
	}

	/**
	 * @return
	 */
	public Timestamp getCreateDate() {
		if(createDate == -1)
			return null;
		else 
			return new Timestamp(createDate);
	}

	/**
	 * @param l
	 */
	public void setCreateDate(Date dt) {
		if(dt != null) {
			createDate = dt.getTime();
		}
		else {
			createDate = -1;
		}
	}
}
