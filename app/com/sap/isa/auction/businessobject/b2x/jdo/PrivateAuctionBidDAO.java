package com.sap.isa.auction.businessobject.b2x.jdo;

import java.io.Serializable;
import java.util.Date;

public class PrivateAuctionBidDAO {

	private String client;
	private String systemId;
	private String auctionId;
	private double bidPrice;
	private String currencyId;
	private int bidQuantity;
	private long bidTime;
	private String id;
	private String userId;
	// Bidder Id
	private String bpId; // stores BPID of Bidder
	private String bidderSoldToParty;

	/**
	 * JDO Application Identity Class
	 */
	public static class Id implements Serializable{
	public String id;
	public Id() {
	}
	public Id(String id) {
		this.id = id;
	}

		/**
		 * @return int hashcode
		 */
		public int hashCode() {
		int result = 17;
		result = result *37 + (id == null?0:id.hashCode());
		return result;
		}

		/**
		 *
		 */
		public boolean equals(Object obj) {
			boolean ret = false;
			if(obj instanceof Id) {
				if(this.id.equals(((Id)obj).id)) 
			ret = true;
			}
			return ret;
		}

		public String toString() {
			return id;//;auctionId + DELIM + bidPrice + DELIM + bidQuantity;
		}
	}    
	public PrivateAuctionBidDAO(String id) {
		this.id = id;
	}
    
	public void setAuctionId(String auctionId){ 
		this.auctionId=  getOpenSqlCompatibleString(auctionId);
	}
    
	public String getAuctionId(){
		return this.auctionId;
	}
    
	public void setBidPrice(double bidPrice){ 
		this.bidPrice= bidPrice;
	}
    
	public double getBidPrice(){
		return this.bidPrice;
	}
    
	public void setCurrencyId(String currencyId){ 
		this.currencyId= currencyId;
	}
    
	public String getCurrencyId(){
		return this.currencyId;
	}
    
	public void setBidQuantity(int bidQuantity){ 
		this.bidQuantity= bidQuantity;
	}
	public int getBidQuantity(){
		return this.bidQuantity;
	}
	public void setBidTime(Date bidTime){ 
		this.bidTime= bidTime.getTime();
	}
	public Date getBidTime(){
		return new Date(this.bidTime);
	}

	public void setId(String id) {
		this.id = id;
	}
	public String getId() {
		return this.id;
	}

	
	public PrivateAuctionBidDAO() {
	}

	/**
	 * Important utility function since open sql does not allow
	 * blank strings to be persisted nor the blank strings
	 * we will try to store null
	 * @param str
	 * @return
	 */
	private String getOpenSqlCompatibleString(String str){
		if(str==null ) return null;
		if(str.trim().length() == 0) return null;
		return str.trim();
	}

	/**
	 * @return
	 */
	public String getClient() {
		return client;
	}

	/**
	 * @return
	 */
	public String getSystemId() {
		return systemId;
	}

	/**
	 * @param string
	 */
	public void setClient(String string) {
		client = string;
	}

	/**
	 * @param string
	 */
	public void setSystemId(String string) {
		systemId = string;
	}

	/**
	 * @return
	 */
	public String getBidderId() {
		return bpId;
	}

	/**
	 * @param string
	 */
	public void setBidderId(String string) {
		bpId = string;
	}

	/**
	 * @return
	 */
	public String getBidderSoldToParty() {
		return bidderSoldToParty;
	}

	/**
	 * @param string
	 */
	public void setBidderSoldToParty(String string) {
		bidderSoldToParty = string;
	}

	/**
	 * @return
	 */
	public String getUserId() {
		return userId;
	}

	/**
	 * @param string
	 */
	public void setUserId(String string) {
		userId = string;
	}
}
