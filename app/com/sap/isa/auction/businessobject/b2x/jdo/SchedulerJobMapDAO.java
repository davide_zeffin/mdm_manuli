/*
 * Created on Sep 2, 2003
 *
 * To change the template for this generated file go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
package com.sap.isa.auction.businessobject.b2x.jdo;

import java.io.Serializable;

/**
 * 
 *
 */
public class SchedulerJobMapDAO {
	
	private String jobId;

	private static final int INVALID_JOB = -1;	

	//Set to invalid job
	private int jobType = INVALID_JOB;
	
	private String scenarioName ;


    /**
     * @return
     */
    public String getJobId() {
        return jobId;
    }

    /**
     * @return
     */
    public int getJobType() {
        return jobType;
    }


    /**
     * @param string
     */
    public void setJobId(String string) {
        jobId = string;
    }

    /**
     * @param i
     */
    public void setJobType(int i) {
        jobType = i;
    }

	/**
	 * JDO Application Identity Class
	 */
	public static class Id implements Serializable{

		public String jobId;

		public Id() {
		}

		public Id(String jobId) {
			this.jobId = jobId;
		}

		/**
		 * @return int hashcode
		 */
		public int hashCode() {
		int result = 17;
		result = result *37 + (jobId == null?0:jobId.hashCode());
		return result;
		}

		/**
		 *
		 */
		public boolean equals(Object obj) {
			boolean ret = false;
		if(obj == this)    return true;
			if(obj instanceof Id) {
				if(this.jobId.equals( ((Id)obj).jobId)) ret = true;
			}

			return ret;
		}

		public String toString() {
			return jobId;
		}
	}
	/**
	 * Important utility function since open sql does not allow
	 * blank strings to be persisted nor the blank strings
	 * we will try to store null
	 * @param str
	 * @return
	 */
	private String getOpenSqlCompatibleString(String str){
		if(str==null ) return null;
		if(str.trim().length() == 0) return null;
		return str.trim();
	}
    
	/**
	 * @return
	 */
	public String getScenarioName() {
		return scenarioName;
	}

	/**
	 * @param string
	 */
	public void setScenarioName(String string) {
		scenarioName = string;
	}

}
