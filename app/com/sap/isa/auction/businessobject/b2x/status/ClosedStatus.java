package com.sap.isa.auction.businessobject.b2x.status;

import com.sap.isa.auction.bean.AuctionStatusEnum;
import com.sap.isa.auction.bean.b2x.PrivateAuction;
import com.sap.isa.auction.businessobject.b2x.PrivateAuctionMgrImpl.FriendInterface;


/**
 * Title:
 * Description:
 * Copyright:    Copyright (c) 2003
 * Company:
 * @author
 * @version 1.0
 */

public class ClosedStatus extends AuctionStatus {

    public ClosedStatus() {
        super(AuctionStatusEnum.CLOSED);
    }

	/* (non-Javadoc)
	 * @see com.sap.isa.auction.businessobject.ebay.status.AuctionStatus#transitionToFinalized(com.sap.isa.auction.businessobject.ebay.EBayAuctionMgrImpl.FriendInterface, com.sap.isa.auction.bean.ebay.EBayAuction)
	 */
	public void transitionToFinalized(
		FriendInterface mgr,
		PrivateAuction auction) {
		mgr.modifyAuctionStatus(auction, AuctionStatusEnum.FINALIZED);
	}

	/* (non-Javadoc)
	 * @see com.sap.isa.auction.businessobject.ebay.status.AuctionStatus#transitionToNotFinalized(com.sap.isa.auction.businessobject.ebay.EBayAuctionMgrImpl.FriendInterface, com.sap.isa.auction.bean.ebay.EBayAuction)
	 */
	public void transitionToNotFinalized(
		FriendInterface mgr,
		PrivateAuction auction) {
		mgr.releaseQuotation(auction);
		mgr.modifyAuctionStatus(auction, AuctionStatusEnum.NOT_FINALIZED);
	}

}