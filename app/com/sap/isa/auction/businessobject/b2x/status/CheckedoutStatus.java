/*
 * Created on Oct 1, 2003
 *
 */
package com.sap.isa.auction.businessobject.b2x.status;

import com.sap.isa.auction.bean.AuctionStatusEnum;
import com.sap.isa.auction.bean.b2x.PrivateAuction;
import com.sap.isa.auction.businessobject.b2x.PrivateAuctionMgrImpl.FriendInterface;

/**
 * @author I802891
 *
 */
public class CheckedoutStatus extends AuctionStatus {

	/**
	 * @param status
	 */
	public CheckedoutStatus() {
		super(AuctionStatusEnum.CHECKEDOUT);
	}

	/* 
	 * Requires Checkout again 
	 * @see com.sap.isa.auction.businessobject.Private.status.AuctionStatus#transitionToFinalized(com.sap.isa.auction.businessobject.Private.PrivateAuctionMgrImpl.FriendInterface, com.sap.isa.auction.bean.Private.PrivateAuction)
	 */
	public void transitionToFinalized(
		FriendInterface mgr,
		PrivateAuction auction) {
			
		mgr.modifyAuctionStatus(auction, AuctionStatusEnum.FINALIZED);
	}

	/*
	 * Auction has finished
	 * @see com.sap.isa.auction.businessobject.Private.status.AuctionStatus#transitionToFinished(com.sap.isa.auction.businessobject.Private.PrivateAuctionMgrImpl.FriendInterface, com.sap.isa.auction.bean.Private.PrivateAuction)
	 */
	public void transitionToFinished(
		FriendInterface mgr,
		PrivateAuction auction) {

		mgr.modifyAuctionStatus(auction, AuctionStatusEnum.FINISHED);
	}
}
