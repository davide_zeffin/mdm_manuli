package com.sap.isa.auction.businessobject.b2x.status;

import com.sap.isa.auction.bean.AuctionStatusEnum;
import com.sap.isa.auction.bean.b2x.PrivateAuction;
import com.sap.isa.auction.businessobject.b2x.PrivateAuctionMgrImpl.FriendInterface;

/**
 * Title:
 * Description:
 * Copyright:    Copyright (c) 2003
 * Company:
 * @author
 * @version 1.0
 */

public class WithdrawnStatus extends AuctionStatus {

    public WithdrawnStatus() {
        super(AuctionStatusEnum.WITHDRAWN);
    }
    
	public void transitionToArchivable(
		FriendInterface mgr,
		PrivateAuction auction) {
		mgr.modifyAuctionStatus(auction, AuctionStatusEnum.ARCHIVABLE);
	}
}