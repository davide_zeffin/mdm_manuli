package com.sap.isa.auction.businessobject.b2x.status;

import com.sap.isa.auction.bean.AuctionStatusEnum;
import com.sap.isa.auction.bean.b2x.PrivateAuction;
import com.sap.isa.auction.businessobject.b2x.PrivateAuctionMgrImpl;
import com.sap.isa.crt.Result;

/**
 * Title:
 * Description:
 * Copyright:    Copyright (c) 2003
 * Company:
 * @author
 * @version 1.0
 */

public class ScheduledStatus extends OpenStatus {

	public ScheduledStatus() {
		super();
		this.status = AuctionStatusEnum.SCHEDULED;
	}

	/* (non-Javadoc)
	 * @see com.sap.isa.auction.businessobject.ebay.status.AuctionStatus#transitionToOpen(com.sap.isa.auction.businessobject.ebay.EBayAuctionMgrImpl.FriendInterface, com.sap.isa.auction.bean.ebay.EBayAuction)
	 */
	public void transitionToOpen(PrivateAuctionMgrImpl.FriendInterface mgr, PrivateAuction auction) {
		mgr.modifyAuctionStatus(auction,AuctionStatusEnum.OPEN);
	}
	public Result transitionToOpenFS(PrivateAuctionMgrImpl.FriendInterface mgr, PrivateAuction auction) {
		transitionToOpen(mgr,auction);
		return null;
	}
}