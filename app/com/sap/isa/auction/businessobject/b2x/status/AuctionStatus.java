package com.sap.isa.auction.businessobject.b2x.status;

import com.sap.isa.auction.bean.b2x.PrivateAuction;
import com.sap.isa.auction.bean.AuctionStatusEnum;
import com.sap.isa.auction.businessobject.b2x.PrivateAuctionMgrImpl;

import com.sap.isa.auction.businessobject.BOUserException;
import com.sap.isa.auction.exception.i18n.*;

import com.sap.isa.crt.Result;

/**
 * Title: Internal Class
 * Description: Status for an Auction. Also provides the allowed transitional
 * status changes
 * Copyright:    Copyright (c) 2003
 * Company:
 * @author
 * @version 1.0
 */

public abstract class AuctionStatus {

    private static AuctionStatus DRAFT =        new DraftStatus();
    private static AuctionStatus OPEN =         new OpenStatus();
    private static AuctionStatus PUBLISHED =    new PublishedStatus();
    private static AuctionStatus CHECKEDOUT=    new CheckedoutStatus();
    private static AuctionStatus CLOSED =       new ClosedStatus();
    private static AuctionStatus FINALIZED =    new FinalizedStatus();
    private static AuctionStatus WITHDRAWN =    new WithdrawnStatus();
	private static AuctionStatus SCHEDULED =    new ScheduledStatus();
	private static AuctionStatus FINISHED =     new FinishedStatus();	
	private static AuctionStatus NOT_FINALIZED = new NotFinalizedStatus();	
	private static AuctionStatus ARCHIVABLE = 	new ArchivableStatus();

    protected AuctionStatusEnum status;

    public AuctionStatus(AuctionStatusEnum status) {
        this.status = status;
    }

    public AuctionStatusEnum getStatus() {
        return status;
    }

    public final void transitionTo(PrivateAuctionMgrImpl.FriendInterface mgr, PrivateAuction auction, AuctionStatusEnum targetStatus) {
        if(targetStatus.getValue() == AuctionStatusEnum.DRAFT.getValue()) transitionToDraft(mgr,auction) ;
        else if(targetStatus.getValue() == AuctionStatusEnum.OPEN.getValue()) transitionToOpen(mgr,auction) ;
		else if(targetStatus.getValue() == AuctionStatusEnum.SCHEDULED.getValue()) transitionToScheduled(mgr,auction);
        else if(targetStatus.getValue() == AuctionStatusEnum.PUBLISHED.getValue()) transitionToPublished(mgr,auction)  ;
        else if(targetStatus.getValue() == AuctionStatusEnum.CHECKEDOUT.getValue())  transitionToCheckedout(mgr,auction)  ;
        else if(targetStatus.getValue() == AuctionStatusEnum.CLOSED.getValue()) transitionToClosed(mgr,auction) ;
        else if(targetStatus.getValue() == AuctionStatusEnum.FINALIZED.getValue()) transitionToFinalized(mgr,auction) ;
        else if(targetStatus.getValue() == AuctionStatusEnum.WITHDRAWN.getValue()) transitionToWithdrawn(mgr,auction) ;
		else if(targetStatus.getValue() == AuctionStatusEnum.FINISHED.getValue()) transitionToFinished(mgr,auction) ;        
		else if(targetStatus.getValue() == AuctionStatusEnum.NOT_FINALIZED.getValue()) transitionToNotFinalized(mgr,auction) ;		
		else if(targetStatus.getValue() == AuctionStatusEnum.ARCHIVABLE.getValue()) transitionToArchivable(mgr,auction) ;
    }

	public final Result transitiontoFS(PrivateAuctionMgrImpl.FriendInterface mgr, PrivateAuction auction, AuctionStatusEnum targetStatus) {
		if(targetStatus.getValue() == AuctionStatusEnum.DRAFT.getValue()) return transitionToDraftFS(mgr,auction) ;
		else if(targetStatus.getValue() == AuctionStatusEnum.OPEN.getValue()) return transitionToOpenFS(mgr,auction) ;
		else if(targetStatus.getValue() == AuctionStatusEnum.SCHEDULED.getValue()) return transitionToScheduledFS(mgr,auction);
		else if(targetStatus.getValue() == AuctionStatusEnum.PUBLISHED.getValue()) return transitionToPublishedFS(mgr,auction)  ;
		else if(targetStatus.getValue() == AuctionStatusEnum.CHECKEDOUT.getValue())  return transitionToCheckedoutFS(mgr,auction)  ;
		else if(targetStatus.getValue() == AuctionStatusEnum.CLOSED.getValue()) return transitionToClosedFS(mgr,auction) ;
		else if(targetStatus.getValue() == AuctionStatusEnum.FINALIZED.getValue()) return transitionToFinalizedFS(mgr,auction) ;
		else if(targetStatus.getValue() == AuctionStatusEnum.WITHDRAWN.getValue()) return transitionToWithdrawnFS(mgr,auction) ;
		else if(targetStatus.getValue() == AuctionStatusEnum.FINISHED.getValue()) return transitionToFinishedFS(mgr,auction) ;        
		else if(targetStatus.getValue() == AuctionStatusEnum.NOT_FINALIZED.getValue()) return transitionToNotFinalizedFS(mgr,auction) ;
		else if(targetStatus.getValue() == AuctionStatusEnum.ARCHIVABLE.getValue()) return transitionToArchivableFS(mgr,auction) ;
		transitionNotAllowed(targetStatus);
		return null;		
	}
	
    public Result transitionToDraftFS(PrivateAuctionMgrImpl.FriendInterface mgr, PrivateAuction auction) {
    	transitionNotAllowed(AuctionStatusEnum.DRAFT);
    	return null;
    }

    public Result transitionToOpenFS(PrivateAuctionMgrImpl.FriendInterface mgr, PrivateAuction auction) {
		transitionNotAllowed(AuctionStatusEnum.OPEN);
		return null;
    }

    public Result transitionToPublishedFS(PrivateAuctionMgrImpl.FriendInterface mgr, PrivateAuction auction) {
		transitionNotAllowed(AuctionStatusEnum.PUBLISHED);
		return null;
    }

    public Result transitionToClosedFS(PrivateAuctionMgrImpl.FriendInterface mgr, PrivateAuction auction) {
		transitionNotAllowed(AuctionStatusEnum.CLOSED);
		return null;
    }

    public Result transitionToFinalizedFS(PrivateAuctionMgrImpl.FriendInterface mgr, PrivateAuction auction) {
		transitionNotAllowed(AuctionStatusEnum.FINALIZED);
		return null;    	
    }

	public Result transitionToNotFinalizedFS(PrivateAuctionMgrImpl.FriendInterface mgr, PrivateAuction auction) {
		transitionNotAllowed(AuctionStatusEnum.NOT_FINALIZED);
		return null;    	
  	}
  	
    public Result transitionToWithdrawnFS(PrivateAuctionMgrImpl.FriendInterface mgr, PrivateAuction auction) {
		transitionNotAllowed(AuctionStatusEnum.WITHDRAWN);
		return null;    	
    }
    
	public Result transitionToScheduledFS(PrivateAuctionMgrImpl.FriendInterface mgr, PrivateAuction auction) {
		transitionNotAllowed(AuctionStatusEnum.SCHEDULED);
		return null;		
	}
	
	public Result transitionToCheckedoutFS(PrivateAuctionMgrImpl.FriendInterface mgr, PrivateAuction auction) {
		transitionNotAllowed(AuctionStatusEnum.CHECKEDOUT);
		return null;		
	}
	
	public Result transitionToFinishedFS(PrivateAuctionMgrImpl.FriendInterface mgr, PrivateAuction auction) {
		transitionNotAllowed(AuctionStatusEnum.FINISHED);
		return null;
	}

	public Result transitionToArchivableFS(PrivateAuctionMgrImpl.FriendInterface mgr, PrivateAuction auction) {
		transitionNotAllowed(AuctionStatusEnum.ARCHIVABLE);
		return null;
	}
	
	public void transitionToDraft(PrivateAuctionMgrImpl.FriendInterface mgr, PrivateAuction auction) {
		transitionNotAllowed(AuctionStatusEnum.DRAFT);
	}

	public void transitionToOpen(PrivateAuctionMgrImpl.FriendInterface mgr, PrivateAuction auction) {
		transitionNotAllowed(AuctionStatusEnum.OPEN);
	}

	public void transitionToPublished(PrivateAuctionMgrImpl.FriendInterface mgr, PrivateAuction auction) {
		transitionNotAllowed(AuctionStatusEnum.PUBLISHED);
	}

	public void transitionToClosed(PrivateAuctionMgrImpl.FriendInterface mgr, PrivateAuction auction) {
		transitionNotAllowed(AuctionStatusEnum.CLOSED);
	}

	public void transitionToFinalized(PrivateAuctionMgrImpl.FriendInterface mgr, PrivateAuction auction) {
		transitionNotAllowed(AuctionStatusEnum.FINALIZED);
	}

	public void transitionToNotFinalized(PrivateAuctionMgrImpl.FriendInterface mgr, PrivateAuction auction) {
	  transitionNotAllowed(AuctionStatusEnum.NOT_FINALIZED);
	}
  	
	public void transitionToWithdrawn(PrivateAuctionMgrImpl.FriendInterface mgr, PrivateAuction auction) {
		transitionNotAllowed(AuctionStatusEnum.WITHDRAWN);
	}
    
	public void transitionToScheduled(PrivateAuctionMgrImpl.FriendInterface mgr, PrivateAuction auction) {
		transitionNotAllowed(AuctionStatusEnum.SCHEDULED);
	}
	
	public void transitionToCheckedout(PrivateAuctionMgrImpl.FriendInterface mgr, PrivateAuction auction) {
		transitionNotAllowed(AuctionStatusEnum.CHECKEDOUT);
	}
	
	public void transitionToFinished(PrivateAuctionMgrImpl.FriendInterface mgr, PrivateAuction auction) {
		transitionNotAllowed(AuctionStatusEnum.FINISHED);
	}	
	
	public void transitionToArchivable(PrivateAuctionMgrImpl.FriendInterface mgr, PrivateAuction auction) {
		transitionNotAllowed(AuctionStatusEnum.ARCHIVABLE);
	}
	
	protected void transitionNotAllowed(AuctionStatusEnum to) {
		ErrorMessageBundle bundle = ErrorMessageBundle.getInstance();
		I18nErrorMessage msg = bundle.newI18nErrorMessage(BOUserException.AUCTION_STATUS_CHANGE_NOT_ALLOWED);
		msg.setArguments(new Object[]{status.toString(),to.toString()});
		throw new BOUserException(msg);
	}
	
    /**
     *
     */
    public static AuctionStatus getAuctionStatus(AuctionStatusEnum status) {
        if(status.getValue() == AuctionStatusEnum.DRAFT.getValue()) return DRAFT ;
        else if(status.getValue() == AuctionStatusEnum.OPEN.getValue()) return OPEN ;
        else if(status.getValue() == AuctionStatusEnum.PUBLISHED.getValue()) return PUBLISHED ;
        else if(status.getValue() == AuctionStatusEnum.CHECKEDOUT.getValue()) return CHECKEDOUT ;
        else if(status.getValue() == AuctionStatusEnum.CLOSED.getValue()) return CLOSED ;
        else if(status.getValue() == AuctionStatusEnum.FINALIZED.getValue()) return FINALIZED ;
		else if(status.getValue() == AuctionStatusEnum.NOT_FINALIZED.getValue()) return NOT_FINALIZED ;        
        else if(status.getValue() == AuctionStatusEnum.WITHDRAWN.getValue()) return WITHDRAWN ;
		else if(status.getValue() == AuctionStatusEnum.SCHEDULED.getValue()) return SCHEDULED ;
		else if(status.getValue() == AuctionStatusEnum.FINISHED.getValue()) return FINISHED ;
		else if(status.getValue() == AuctionStatusEnum.ARCHIVABLE.getValue()) return ARCHIVABLE ;		
        else return null;
    }
}