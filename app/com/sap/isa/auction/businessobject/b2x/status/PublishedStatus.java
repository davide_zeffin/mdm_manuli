package com.sap.isa.auction.businessobject.b2x.status;

import com.sap.isa.auction.bean.b2x.PrivateAuction;
import com.sap.isa.auction.bean.AuctionStatusEnum;
import com.sap.isa.auction.businessobject.b2x.PrivateAuctionMgrImpl;
import com.sap.isa.auction.businessobject.b2x.PrivateAuctionMgrImpl.FriendInterface;
/**
 * Title:
 * Description:
 * Copyright:    Copyright (c) 2003
 * Company:
 * @author
 * @version 1.0
 */

public class PublishedStatus extends AuctionStatus {

	public PublishedStatus() {
		super(AuctionStatusEnum.PUBLISHED);
	}

	/**
	 * End the Listing on Ebay
	 * Update End time
	 * Set to new Status
	 * Persist the state in persistent store
	 */
	public void transitionToClosed(PrivateAuctionMgrImpl.FriendInterface mgr, PrivateAuction auction) {
		mgr.modifyAuctionStatus(auction,AuctionStatusEnum.CLOSED);
	}

	/* 
	 * Release the Quotation
	 * Set status to NOT_FINALIZED
	 * @see com.sap.isa.auction.businessobject.ebay.status.AuctionStatus#transitionToNotFinalized(com.sap.isa.auction.businessobject.ebay.EBayAuctionMgrImpl.FriendInterface, com.sap.isa.auction.bean.ebay.EBayAuction)
	 */
	public void transitionToNotFinalized(
		FriendInterface mgr,
		PrivateAuction auction) {
		mgr.releaseQuotation(auction);
		mgr.modifyAuctionStatus(auction,AuctionStatusEnum.NOT_FINALIZED);
	}

	/* 
	 * Set the status to FINALIZED
	 * @see com.sap.isa.auction.businessobject.ebay.status.AuctionStatus#transitionToFinalized(com.sap.isa.auction.businessobject.ebay.EBayAuctionMgrImpl.FriendInterface, com.sap.isa.auction.bean.ebay.EBayAuction)
	 */
	public void transitionToFinalized(
		FriendInterface mgr,
		PrivateAuction auction) {
		mgr.modifyAuctionStatus(auction, AuctionStatusEnum.FINALIZED);
	}
	/**
	 * There has to be check that the task is not running at the time when the
	 * the auction is about to be changed through this function
	 */
//	public void transitionToOpen(EBayAuctionMgrImpl.FriendInterface mgr, EBayAuction auction) {
//		mgr.modifyAuctionStatus(auction, AuctionStatusEnum.OPEN);
//	}
}
