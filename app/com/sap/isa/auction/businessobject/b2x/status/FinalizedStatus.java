package com.sap.isa.auction.businessobject.b2x.status;

import com.sap.isa.auction.bean.AuctionStatusEnum;
import com.sap.isa.auction.bean.b2x.PrivateAuction;
import com.sap.isa.auction.businessobject.b2x.PrivateAuctionMgrImpl.FriendInterface;


/**
 * Title:
 * Description:
 * Copyright:    Copyright (c) 2003
 * Company:
 * @author
 * @version 1.0
 */

public class FinalizedStatus extends AuctionStatus {

    public FinalizedStatus() {
        super(AuctionStatusEnum.FINALIZED);
    }
	/* (non-Javadoc)
	 * @see com.sap.isa.auction.businessobject.ebay.status.AuctionStatus#transitionToCheckedout(com.sap.isa.auction.businessobject.ebay.EBayAuctionMgrImpl.FriendInterface, com.sap.isa.auction.bean.ebay.EBayAuction)
	 */
	public void transitionToCheckedout(
		FriendInterface mgr,
		PrivateAuction auction) {
		
			mgr.modifyAuctionStatus(auction, AuctionStatusEnum.CHECKEDOUT);
	}

	/* (non-Javadoc)
	 * @see com.sap.isa.auction.businessobject.ebay.status.AuctionStatus#transitionToFinished(com.sap.isa.auction.businessobject.ebay.EBayAuctionMgrImpl.FriendInterface, com.sap.isa.auction.bean.ebay.EBayAuction)
	 */
	public void transitionToFinished(
		FriendInterface mgr,
		PrivateAuction auction) {
			
		mgr.modifyAuctionStatus(auction, AuctionStatusEnum.FINISHED);
	}

}