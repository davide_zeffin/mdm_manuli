package com.sap.isa.auction.businessobject.b2x.status;

import com.sap.isa.auction.bean.b2x.PrivateAuction;
import com.sap.isa.auction.bean.AuctionStatusEnum;
import com.sap.isa.auction.businessobject.b2x.PrivateAuctionMgrImpl;
import com.sap.isa.auction.exception.SystemException;
import com.sap.isa.auction.exception.i18n.*;

/**
 * Title:
 * Description:
 * Copyright:    Copyright (c) 2003
 * Company:
 * @author
 * @version 1.0
 */

public class DraftStatus extends AuctionStatus {

	public DraftStatus() {
		super(AuctionStatusEnum.DRAFT);
	}

	/**
	 * Validate Auction
	 * Modify Status
	 * Save to Persistent Store
	 */
	public void transitionToOpen(PrivateAuctionMgrImpl.FriendInterface mgr, PrivateAuction auction) {
		mgr.validateAuction(auction);
		mgr.modifyAuctionStatus(auction,AuctionStatusEnum.OPEN);
	}

	/**
	 * Validate Auction
	 * Create Quotation
	 * Publish Auction
	 * Modify Status, Start Date, End Date etc.
	 * Save to Persistent Store
	 */
	public void transitionToPublished(PrivateAuctionMgrImpl.FriendInterface mgr, PrivateAuction auction) {
		mgr.validateAuction(auction);
		mgr.createQuotation(auction);
		try {
			mgr.modifyAuctionStatus(auction,AuctionStatusEnum.PUBLISHED);
		}
		catch(Throwable th) {
			if(auction.getQuotationId() != null && auction.getQuotationId().length() > 0)
				mgr.releaseQuotation(auction);
                
			if(th instanceof SystemException) {
				throw (SystemException)th;
			}
			else {
				ErrorMessageBundle bundle = ErrorMessageBundle.getInstance();
				I18nErrorMessage msg = bundle.newI18nErrorMessage(SystemException.INTERNAL_ERROR);
				// bubble as System Exception
				SystemException ex = new SystemException(msg,th);
				throw ex;
			}
		}
	}
}
