/*
 * Created on Oct 7, 2003
 *
 */
package com.sap.isa.auction.businessobject.b2x.status;

import com.sap.isa.auction.bean.AuctionStatusEnum;


/**
 * @author I802891
 *
 */
public class NotFinalizedStatus extends AuctionStatus {

	/**
	 * @param status
	 */
	public NotFinalizedStatus() {
		super(AuctionStatusEnum.NOT_FINALIZED);
	}
}
