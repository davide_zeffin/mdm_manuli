/*
 * Created on Oct 1, 2003
 *
 */
package com.sap.isa.auction.businessobject.b2x.status;

import com.sap.isa.auction.bean.AuctionStatusEnum;
import com.sap.isa.auction.bean.b2x.PrivateAuction;
import com.sap.isa.auction.businessobject.b2x.PrivateAuctionMgrImpl.FriendInterface;

/**
 * @author I802891
 *
 */
public class FinishedStatus extends AuctionStatus {

	/**
	 * @param status
	 */
	public FinishedStatus() {
		super(AuctionStatusEnum.FINISHED);
	}
	
	public void transitionToArchivable(
		FriendInterface mgr,
		PrivateAuction auction) {
		mgr.modifyAuctionStatus(auction, AuctionStatusEnum.ARCHIVABLE);
	}
}
