
package com.sap.isa.auction.businessobject.b2x;

import com.sap.isa.auction.businessobject.order.AuctionBasket;
import com.sap.isa.auction.businessobject.order.AuctionOrder;
import com.sap.isa.businessobject.GenericBusinessObjectManager;
import com.sap.isa.core.businessobject.BackendAware;

/**
 * Used in session scope for the B2C checkout process
 */
public class CheckoutBusinessObjectManager
	extends GenericBusinessObjectManager
	implements BackendAware{
		
		/**
		 * Creates a new auction order object. If such an object was already created, a
		 * reference to the old object is returned and no new object is created.
		 *
		 * @return reference to a newly created or already existing order object
		 */
		public synchronized AuctionOrder createAuctionOrder() {
			return createAuctionOrder(0);
		}

	   /**
		 * Creates a new order object. If such an object was already created, a
		 * reference to the old object is returned and no new object is created.
		 *
		 * @param index Index of the order
		 * @return reference to a newly created or already existing order object or
		 *         <code>null</code> if the index is exceeded
		 */
		public synchronized AuctionOrder createAuctionOrder(int index) {
			return (AuctionOrder) createBusinessObject(AuctionOrder.class, index);
		}


		/**
		 * Returns a reference to an existing order object.
		 *
		 * @return reference to basket object or null if no object is present
		 */
		public AuctionOrder getAuctionOrder() {
			return getAuctionOrder(0);
		}

		/**
		 * Returns a reference to an existing order object.
		 *
		 * @param index Index of the order
		 * @return reference to order object, <code>null</code> if no object is present
		 *         at the given position of if the index is exceeded
		 */
		public AuctionOrder getAuctionOrder(int index) {
			return (AuctionOrder) getBusinessObject(AuctionOrder.class, index);
		}

		/**
		 * Releases references to the order
		 */
		public synchronized void releaseAuctionOrder() {
			releaseAuctionOrder(0);
		}

		/**
		 * Releases references to the order object specified by the index
		 *
		 * @param index Index of the order to be released
		 */
		public synchronized void releaseAuctionOrder(int index) {
			releaseBusinessObject(AuctionOrder.class, index);
		}

		/**
		 * Creates a new basket object. If such an object was already created, a
		 * reference to the old object is returned and no new object is created.
		 *
		 * @return reference to a newly created or already existing basket object
		 */
		public synchronized AuctionBasket createAuctionBasket() {
			return (AuctionBasket) createBusinessObject(AuctionBasket.class);
		}

		/**
		 * Returns a reference to an existing basket object.
		 *
		 * @return reference to basket object or null if no object is present
		 */
		public AuctionBasket getAuctionBasket() {
			return (AuctionBasket) getBusinessObject(AuctionBasket.class);
		}

		/**
		 * Releases references to the basket
		 */
		public synchronized void releaseAuctionBasket() {
			releaseBusinessObject(AuctionBasket.class);
		}



}
