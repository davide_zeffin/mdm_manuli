/*
 * Created on Dec 2, 2004
 *
 * To change the template for this generated file go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
package com.sap.isa.auction.businessobject.b2x;

import com.sap.isa.auction.bean.SalesArea;
import com.sap.isa.auction.businessobject.b2x.ProcessBusinessObjectManager;
import com.sap.isa.auction.businessobject.UserRoleTypeEnum;
import com.sap.isa.auction.util.PropertyContainer;
import com.sap.isa.businessobject.Shop;
import com.sap.tc.logging.Severity;

/**
 * @author I802850
 *
 * To change the template for this generated type comment go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
public class InvocationContext extends com.sap.isa.auction.businessobject.InvocationContext{
	
	private String shopId;
	private Shop sellerShop = null;   
	private SalesArea salesArea = null;
	private UserRoleTypeEnum roleType = UserRoleTypeEnum.NONE;
	private String bpId;
	private ProcessBusinessObjectManager pbom = null;
	// global context data is kept as static data and set during initialization
	private static PropertyContainer globalData = new PropertyContainer();

	public InvocationContext(ProcessBusinessObjectManager bom) {
		super();
		pbom = bom;
	}

	/**
	 * @return
	 */
	public String getShopId() {
        if (shopId != null && shopId.length() == 0) {
            logger.logT(Severity.DEBUG, tracer, "No ShopId is given");   
        }
		return shopId;
	}

	/**
	 * @param string
	 */
	public void setShopId(String shop) {
		shopId = shop;
	}

	/**
	 * @return
	 */
	public Shop getShop() {
		if(sellerShop == null) {
			if(pbom == null) {
				logger.logT(Severity.ERROR, tracer, "pbom is null");
				return null;
			}
			sellerShop = pbom.getShop(shopId);
		}
		return sellerShop;
	}

	public String getCurrency() {
		Shop shop = getShop();
		if(shop == null) {
			logger.logT(Severity.ERROR, tracer, "can not read shop");
			return null;
		}
		return shop.getCurrency();
	}

	public String getCountry() {
		Shop shop = getShop();
		if(shop == null) {
			logger.logT(Severity.ERROR, tracer, "can not get shop");
			return null;
		}
		return shop.getCountry();
	}

	public SalesArea getSalesArea() {
		if(salesArea == null) {
			Shop shop = getShop();
			if(shop == null) {
				logger.logT(Severity.ERROR, tracer, "can not get shop");
				return null;
			}
			
			salesArea = new SalesArea();
			salesArea.setSalesOrganization(shop.getSalesOrganisation());
			salesArea.setDistributionChannel(shop.getDistributionChannel());
			salesArea.setDivision(shop.getDivision());
		}
		return salesArea;
	}

	/**
	 * @param enum
	 */
	public void setRoleType(UserRoleTypeEnum enum) {
		roleType = enum;
	}
	
	/**
	 * @return
	 */
	public UserRoleTypeEnum getRoleType() {
		return roleType;
	}

	/**
	 * @return
	 */
	public String getBpId() {
		return bpId;
	}

	/**
	 * @param string
	 */
	public void setBpId(String string) {
		bpId = string;
	}


}
