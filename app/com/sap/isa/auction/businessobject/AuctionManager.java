package com.sap.isa.auction.businessobject;

import com.sap.isa.auction.bean.Auction;
import com.sap.isa.auction.bean.AuctionStatusEnum;

import com.sap.isa.auction.bean.search.QueryFilter;
import com.sap.isa.auction.bean.search.QueryResult;

/**
 * Title:
 * Description:
 * Copyright:    Copyright (c) 2003
 * Company:
 * @author
 * @version 1.0
 */

public interface AuctionManager extends AuctionBusinessObject {
    /**
	 * @param auction
	 */
    public void createAuction(Auction auction);
    
    /**
     * Batch creation 
     * @param auctions
     */
    public void createAuctions(Auction[] auctions);

	/**
	* @param ref referenced Auction
	* @return Auction the newly created Auction in OPEN STATE
	*/
	public void createAuctionByRef(Auction auction, Auction refAuction);
	
	/**
	 * @param auctionId
	 * @return
	 */
    public Auction getAuctionById(String auctionId);
    
	/**
	 * @param templateId id of the Auction Template
	 */
	public Auction getAuctionTemplateById(String templateId);

	/**
	 * @param auction
	 * @param newStatus
	 */
    public void modifyStatus(Auction auction, AuctionStatusEnum newStatus);

    /**
     * No Status change allowed, here
     */
    public void modifyAuction(Auction auction);

    /**
     * Auction can only be deleted if it is not published to
     * eBay
     */
    public void deleteAuction(Auction auction);

    /**
     * Search specific functions. Only one API
     * Order is important. Based on last synchronization with eBay
     */
    public QueryResult searchAuctions(QueryFilter filer);
}