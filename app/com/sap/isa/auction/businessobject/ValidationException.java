package com.sap.isa.auction.businessobject;

import com.sap.tc.logging.Category;
import com.sap.tc.logging.Location;
import com.sap.isa.auction.exception.i18n.ErrorMessageBundle;
import com.sap.isa.auction.exception.i18n.I18nErrorMessage;

/**
 * Title:        Internet Sales
 * Description: This exception is thrown when the the Beans passed to
 *              Logic layer are not in valid state. This exception provides
 *              the list of culprit object on whom the validation failed
 *
 *              Validation Exception provides a list of Errors based on
 *              attributes on which the Validation Failed
 * Copyright:    Copyright (c) 2002
 * Company:      SAP Labs, PA
 * @author: e-auctions
 * @version 0.1
 */

public class ValidationException
	extends com.sap.isa.auction.exception.UserException {

	private final static String EXC_ID = "ValidationException";

	public static final String INVALID_AUCTION = EXC_ID + 101;
	public static final String INVALID_AUCTIONLISTING = EXC_ID + 102;
	public static final String INVALID_RESPONSE = EXC_ID + 103;
	public static final String INVALID_RESPONSELISTING = EXC_ID + 104;
	public static final String INVALID_WINNER = EXC_ID + 105;
	public static final String INVALID_AUTOBID = EXC_ID + 106;

	// finer error IDs are used by Validating Beans
	public static final String INVALID_START_DATE = EXC_ID + 107;
	public static final String INVALID_END_DATE = EXC_ID + 108;
	public static final String INVALID_QUOT_DURATION = EXC_ID + 109;
	public static final String INVALID_RESERVE_PRICE = EXC_ID + 110;
	public static final String INVALID_START_PRICE = EXC_ID + 111;
	public static final String INVALID_BIDINCREMENT = EXC_ID + 112;
	public static final String INVALID_WEBSHOPID = EXC_ID + 113;
	public static final String INVALID_STATUS_CHANGE = EXC_ID + 114;
	public static final String END_DATE_BEFORE_START_DATE = EXC_ID + 115;
	public static final String QUOT_EXP_DATE_BEFORE_END_DATE = EXC_ID + 116;
	public static final String INVALID_AUCTION_RULE = EXC_ID + 117;
	public static final String END_DATE_BEFORE_NOW = EXC_ID + 118;

	public static final String INVALID_QUANTITY = EXC_ID + 119;
	public static final String INVALID_BIDAMOUNT = EXC_ID + 120;
	public static final String INVALID_CEILINGAMOUNT = EXC_ID + 121;

	public static final String INVALID_DURATION = EXC_ID + 122;
	public static final String INVALID_NAME = EXC_ID + 123;

	public static final String INVALID_PRODUCTID = EXC_ID + 124;
	public static final String INVALID_PRODUCTAREAID = EXC_ID + 125;

	public static final String INVALID_AUTOBID_INCREMENT = EXC_ID + 126;

	public static final String INVALID_NAME_LENGTH = EXC_ID + 127;
	public static final String INVALID_PRODUCTID_LENGTH = EXC_ID + 128;
	public static final String INVALID_PRODUCTAREAID_LENGTH = EXC_ID + 129;

	public static final String INVALID_WINNER_BUYERID = EXC_ID + 130;
	public static final String INVALID_BROKEN_LOT_WINNER = EXC_ID + 131;

	public static final String INVALID_BUSINESS_PARTNERID = EXC_ID + 132;

	public static final String INVALID_WINNER_USERID = EXC_ID + 133;

	public static final String INVALID_ORDERID = EXC_ID + 134;

	public static final String INVALID_DESCRIPTION_LENGTH = EXC_ID + 135;

	public static final String NO_AUCTION_ITEMS_ERROR = EXC_ID + 136;
	public static final String INVALID_SALES_UNITS = EXC_ID + 137;
	public static final String INVALID_TITLE = EXC_ID + 138;
	public static final String MISSING_CATEGORY = EXC_ID + 139;
	public static final String INVALID_AUCTION_DURATION = EXC_ID + 140;
	public static final String MISSING_CKOUT_INST = EXC_ID + 141;
	public static final String MISSING_SHIPPING_PYMT_INFO = EXC_ID + 142;
	public static final String MISSING_SELLER_ADDRESS = EXC_ID + 143;
	public static final String MISSING_BUYER_REGION = EXC_ID + 144;
	public static final String MISSING_CURRENCY = EXC_ID + 145;
	public static final String MISSING_SHIPPING_COST = EXC_ID + 146;
	public static final String INVALID_SALES_TAX = EXC_ID + 147;
	public static final String NO_CC_ALLOWED_ERROR = EXC_ID + 148;
	public static final String INVALID_BUYNOW_PRICE = EXC_ID + 149;
	public static final String INVALID_PRODUCT_CODE = EXC_ID + 150;
	public static final String INVALID_IMAGE_URL = EXC_ID + 151;

	public static final String INVALID_DESCRIPTION = EXC_ID + 152;

	public static final String INVALID_CKOUT_INSTR = EXC_ID + 153;

	public static final String INVALID_SHIP_PROFILE = EXC_ID + 154;
	public static final String INVALID_MKT_PROFILE = EXC_ID + 155;
	public static final String INVALID_BILL_PROOFILE = EXC_ID + 156;
	public static final String INVALID_CATEGORY_ID = EXC_ID + 157;
	public static final String INVALID_SELLER_ID = EXC_ID + 158;
	public static final String INVALID_BKEND_MAP = EXC_ID + 159;
	public static final String NO_LISTING_DEFAULT_SETTING_ERROR = EXC_ID + 160;
	public static final String NO_BACKEND_DEFAULT_SETTING_ERROR = EXC_ID + 161;
	public static final String INVALID_RAW_THEME = EXC_ID + 162;
	public static final String INVALID_THEME_PROPERTY = EXC_ID + 163;
	public static final String INVALID_THEME_TEMPLATE = EXC_ID + 164;
	public static final String INVALID_SCHEDULE_DATE = EXC_ID + 165;

	public static final String INVALID_FEEDBACK = EXC_ID + 166;

	public static final String INVALID_SCHED_JOB_TYPE = EXC_ID + 167;

	public static final String INVALID_FEEDBACK_TYPE = EXC_ID + 168;
	public static final String INVALID_FEEDBACK_COMMENT = EXC_ID + 169;
	public static final String BKMAP_ALREADY_EXISTS = EXC_ID + 170;

	public static final String SELLER_PAYS_WITH_CKOUT_INSTRUCTIONS_ERROR =
		EXC_ID + 171;

	//newly added valication exception type
	public static final String INVALID_ORDER_TYPE = EXC_ID + 172;
	public static final String INVALID_QUOTATION_TYPE = EXC_ID + 173;
	public static final String INVALID_REJECTION_REASON = EXC_ID + 174;
	public static final String INVALID_REFERENCE_BP = EXC_ID + 175;
	public static final String INVALID_COD_BP = EXC_ID + 176;

	public static final String INVALID_PAYMENT_TYPE = EXC_ID + 177;
	public static final String INVALID_SHIPPING_REGION = EXC_ID + 178;
	public static final String INVALID_PAYPAL_ACCOUNT = EXC_ID + 179;
	public static final String INVALID_INSURANCE_COST = EXC_ID + 180;

	public static final String INVALID_EBAY_USERID = EXC_ID + 181;
	public static final String INVALID_EBAY_PWD = EXC_ID + 182;
	public static final String INVALID_BACKEND_USERID = EXC_ID + 183;
	public static final String INVALID_VALIDFROM_DATE = EXC_ID + 184;
	public static final String INVALID_VALIDUNTIL_DATE = EXC_ID + 185;
	public static final String UNTIL_DATE_BEFORE_FROM_DATE = EXC_ID + 186;
	public static final String UNTIL_DATE_BEFORE_NOW = EXC_ID + 187;
	
	public static final String INVALID_TOTAL_PRICE_CONDITION = EXC_ID + 188;
	public static final String INVALID_DELIVERY_BLOCK = EXC_ID + 189;
	public static final String INVALID_CLIENT_SYSTEM_ID = EXC_ID + 190;
	public static final String IDENTICAL_CATEGORIES_ERROR = EXC_ID + 191;
	public static final String INVALID_VAT_PERCENT = EXC_ID + 192;
	public static final String SHIPPING_REGIONS_NOT_SPEC_ERROR = EXC_ID + 193;
	public static final String SHIPPING_REGIONS_SPEC_ERROR = EXC_ID + 194;
	public static final String SHIPPING_CALC_ATTRS_SPEC_ERROR = EXC_ID + 195;
	public static final String SHIPPING_CALC_ATTRS_NOT_SPEC_ERROR = EXC_ID + 196;
	public static final String SHIPPING_FLAT_ATTRS_SPEC_ERROR = EXC_ID + 197;
	public static final String NON_B2B_VAT_CATEGORY_ERROR = EXC_ID + 198;
	public static final String SHIPPING_PAYMENT_INFO_DETAILS_SPEC_FOR_NON_CKOUTDETAILS_ERROR = EXC_ID + 199;
	public static final String INVALID_BANKTRANSACTION_ID = EXC_ID + 200;
	public static final String INVALID_DURATION_FOR_FEEDBACK_RATING = EXC_ID + 201;
	public static final String INSUFFICIENT_FEEDBACK_RATING_FOR_BIN_AUCTIONS = EXC_ID + 202;
	public static final String REQUIRED_CATEGORY_ATTRIBUTE_MISSING = EXC_ID + 203;
	public static final String INVALID_CATEGORY_ATTRIBUTE_VALUE_LENGTH = EXC_ID + 204;
	public static final String INVALID_MAX_CATEGORY_ATTRIBUTE_VALUES_SEL = EXC_ID + 205;
	public static final String INVALID_MIN_CATEGORY_ATTRIBUTE_VALUES_SEL = EXC_ID + 206;
	public static final String INVALID_MAX_MIN_CATEGORY_ATTRIBUTE_VALUES_SEL = EXC_ID + 207;
	public static final String INVALID_CATEGORY_ATTR_VAL_URL = EXC_ID + 208;
	public static final String INVALID_CATEGORY_ATTR_VAL_DATE_FORMAT = EXC_ID + 209;
	public static final String INVALID_CATEGORY_ATTR_VAL_NUM_RANGE = EXC_ID + 210;
	public static final String INVALID_CATEGORY_ATTR_VAL_MAX_NUM = EXC_ID + 211;
	public static final String INVALID_CATEGORY_ATTR_VAL_MIN_NUM = EXC_ID + 212;
	public static final String INVALID_CATEGORY_ATTR_VAL_PRECISION = EXC_ID + 213;
	public static final String INVALID_CATEGORY_ATTR_VALUE_DATE = EXC_ID + 214;
	public static final String INVALID_CATEGORY_ATTR_VALUE_DATE_RANGE = EXC_ID + 215;
	public static final String INSUFFICIENT_FEEDBACK_RATING_FOR_FEATURED_AUCTIONS = EXC_ID + 216;
	public static final String CKOUT_INSTR_MISSING = EXC_ID + 217;
	public static final String MALFORMED_TAG = EXC_ID + 218;
	public static final String REQUIRED_ORDERTYPE_MISSING = EXC_ID + 219;
	public static final String INVALID_INSURANCE_FEE_SPEC = EXC_ID + 220;
	public static final String MALFORMED_TAG_NOCOMMAS = EXC_ID + 221;
	public static final String MALFORMED_EMPTY_PROPERTY = EXC_ID + 222;
	public static final String INVALID_EMAIL_ADDRESS = EXC_ID + 223;
	public static final String SELLER_PAYS_WITHOUT_CKOUT_INSTRUCTIONS_ERROR = EXC_ID + 224;		
	public static final String ATLEAST_ONE_PAYMENT_OPTION_WITH_CKOUTDETAILS_ERROR = EXC_ID + 225;		
	public static final String SELLER_PAYS_SHIPPING_WITH_SHIPPING_IN_TAX_ERROR = EXC_ID + 226;
	public static final String UNRECOGNIZED_THEME_PROPERTY_VALUE = EXC_ID + 227;
	public static final String JAVASCRIPT_METHOD_NOT_ALLOWED = EXC_ID + 228;
	public static final String HTML_ELEMENT_NOT_ALLOWED = EXC_ID + 229;
	public static final String JAVASCRIPT_REMOTE_NOT_ALLOWED = EXC_ID + 230;
	public static final String RELIST_MIN_BID_ERROR = EXC_ID + 231;
	public static final String RELIST_RESERVEPRICE_ERROR = EXC_ID + 232;
    public static final String MAX_FEEDBACK_LENGTH_EXCEEDED = EXC_ID + 233;
    public static final String FEEDBACK_CANNOT_BE_EMPTY = EXC_ID + 234;
	public static String RESERVEPRICE_NOT_SUPPORTED = EXC_ID + 235;

	public static final String ALREADY_HIGHEST_BIDDER = EXC_ID + 236;
	public static final String BIDDER_IS_SELLER = EXC_ID + 237;
	public static final String BID_FOR_CLOSED_AUCTION = EXC_ID + 238;
	public static final String INVALID_BID_QTY = EXC_ID + 239;	
	public static final String NO_TARGETGRP_OR_BP = EXC_ID + 240;
	public static final String INVALID_AUCTION_QUANTITY = EXC_ID + 241;
	public static final String INVALID_QTY_FOR_WINNER = EXC_ID + 242;		
	public static final String INVALID_UOM = EXC_ID + 243;		
    public static final String MAX_THEMELENGTH_EXCEEDED = EXC_ID + 244;
    public static final String MISSING_SHOP_PROPERTY = EXC_ID + 245;
    public static final String SEARCH_LENGTH_EXCEEDED = EXC_ID + 246;
    
	public static final String INVALID_LOCALE = EXC_ID + 247;
	public static final String INVALID_TRANSACTION_AMOUNT = EXC_ID + 248;
	public static final String EMPTY_COLUMN_CONTENT = EXC_ID + 249;
	public static final String INVALID_CSV_COLUMN_COUNT = EXC_ID + 250;
    
	
	static {
		
		ErrorMessageBundle.setDefaultMessage(
			INVALID_AUCTION,
			"Auction bean is invalid");
		ErrorMessageBundle.setDefaultMessage(
			INVALID_AUCTIONLISTING,
			"AuctionListing bean is invalid");
		ErrorMessageBundle.setDefaultMessage(
			INVALID_RESPONSE,
			"Response bean is invalid");
		ErrorMessageBundle.setDefaultMessage(
			INVALID_RESPONSELISTING,
			"ResponseListing bean is invalid");
		ErrorMessageBundle.setDefaultMessage(
			INVALID_AUTOBID,
			"Autobid bean is invalid");
		ErrorMessageBundle.setDefaultMessage(
			INVALID_START_DATE,
			"Auction start date is invalid");
		ErrorMessageBundle.setDefaultMessage(
			INVALID_END_DATE,
			"Auction end date is invalid");
		ErrorMessageBundle.setDefaultMessage(
			INVALID_QUOT_DURATION,
			"Auction Quotation expiration duration is invalid");
		ErrorMessageBundle.setDefaultMessage(
			INVALID_RESERVE_PRICE,
			"Auction reserve price is invalid");
		ErrorMessageBundle.setDefaultMessage(
			INVALID_START_PRICE,
			"Auction start price is invalid");
		ErrorMessageBundle.setDefaultMessage(
			INVALID_WEBSHOPID,
			"Auction web shop id is invalid");
		ErrorMessageBundle.setDefaultMessage(
			INVALID_STATUS_CHANGE,
			"Auction status change is invalid");
		ErrorMessageBundle.setDefaultMessage(
			INVALID_QUANTITY,
			"Bid quantity is invalid");
		ErrorMessageBundle.setDefaultMessage(
			INVALID_BIDAMOUNT,
			"Bid amount is invalid");
		ErrorMessageBundle.setDefaultMessage(
			INVALID_CEILINGAMOUNT,
			"Auto bid ceiling amount is invalid");
		ErrorMessageBundle.setDefaultMessage(
			INVALID_STATUS_CHANGE,
			"Status change is not allowed");
		ErrorMessageBundle.setDefaultMessage(
			QUOT_EXP_DATE_BEFORE_END_DATE,
			"Quotation expiration date is before end date");
		ErrorMessageBundle.setDefaultMessage(
			INVALID_STATUS_CHANGE,
			"Invalid Status change");
		ErrorMessageBundle.setDefaultMessage(
			END_DATE_BEFORE_START_DATE,
			"Start date is before end date");
		ErrorMessageBundle.setDefaultMessage(
			INVALID_AUCTION_RULE,
			"Auction winner determination rule is invalid");
		ErrorMessageBundle.setDefaultMessage(
			INVALID_NAME,
			"Name {0} is invalid");
		ErrorMessageBundle.setDefaultMessage(
			END_DATE_BEFORE_NOW,
			"End Date is before current date");

		ErrorMessageBundle.setDefaultMessage(
			INVALID_PRODUCTAREAID,
			"Invalid product area id");
		ErrorMessageBundle.setDefaultMessage(
			INVALID_PRODUCTID,
			"Invalid product id");
		ErrorMessageBundle.setDefaultMessage(
			INVALID_AUTOBID_INCREMENT,
			"Auto bid increment is invalid");
		ErrorMessageBundle.setDefaultMessage(
			INVALID_NAME_LENGTH,
			"Auction name longer than {0} characters not allowed");
		ErrorMessageBundle.setDefaultMessage(
			INVALID_PRODUCTID_LENGTH,
			"Product Id longer than {0} characters not allowed");
		ErrorMessageBundle.setDefaultMessage(
			INVALID_PRODUCTAREAID_LENGTH,
			"Product Area Id longer than {0} characters not allowed");
		ErrorMessageBundle.setDefaultMessage(
			INVALID_WINNER_BUYERID,
			"Buyer id is invalid for winner bean");
		ErrorMessageBundle.setDefaultMessage(
			INVALID_BROKEN_LOT_WINNER,
			"Broken lot winner bean is invalid, both auctionlisting and responselisting must be provided");
		ErrorMessageBundle.setDefaultMessage(
			INVALID_BUSINESS_PARTNERID,
			"Business Partner Id is invalid, a valid value must be provided");
		ErrorMessageBundle.setDefaultMessage(
			INVALID_WINNER_USERID,
			"User Id is invalid, a valid value must be provided");
		ErrorMessageBundle.setDefaultMessage(
			INVALID_ORDERID,
			"Order Id is invalid, a valid value must be provided");
		ErrorMessageBundle.setDefaultMessage(
			INVALID_DESCRIPTION_LENGTH,
			"Description longer than {0} characters is not allowed");

		ErrorMessageBundle.setDefaultMessage(
			NO_AUCTION_ITEMS_ERROR,
			"Auction must have atleast 1 item");

		ErrorMessageBundle.setDefaultMessage(
			INVALID_SALES_UNITS,
			"Sales unit for product {0} is invalid");

		ErrorMessageBundle.setDefaultMessage(
			INVALID_TITLE,
			"Auction title is either empty or exceeded the maximum length of {0} chars");

		ErrorMessageBundle.setDefaultMessage(
			MISSING_CATEGORY,
			"Auction must have atleast 1 ebay category");

		ErrorMessageBundle.setDefaultMessage(
			INVALID_AUCTION_DURATION,
			"Auction duration is invalid.Can only be {0} days for {1} auctions");
		ErrorMessageBundle.setDefaultMessage(
			MISSING_CKOUT_INST,
			"For checkout at ebay, checkout instructions must be provided");
		ErrorMessageBundle.setDefaultMessage(
			MISSING_SHIPPING_PYMT_INFO,
			"For checkout at ebay shipping and payment info must be provided");
		ErrorMessageBundle.setDefaultMessage(
			MISSING_SELLER_ADDRESS,
			"Seller address is missing");
		ErrorMessageBundle.setDefaultMessage(
			MISSING_BUYER_REGION,
			"Buyer region is missing");
		ErrorMessageBundle.setDefaultMessage(
			MISSING_CURRENCY,
			"Currency code is missing");
		ErrorMessageBundle.setDefaultMessage(
			MISSING_SHIPPING_COST,
			"Shipping cost is not provided");
		ErrorMessageBundle.setDefaultMessage(
			INVALID_SALES_TAX,
			"Sales tax and sales tax state both must be provided and 0 <= SalesTaxPercentage <= 100");
		ErrorMessageBundle.setDefaultMessage(
			NO_CC_ALLOWED_ERROR,
			"No credit cards is allowed as payment option");
		ErrorMessageBundle.setDefaultMessage(
			INVALID_BUYNOW_PRICE,
			"Buy now price must be greater than start price and reserve price(if present)");
		ErrorMessageBundle.setDefaultMessage(
			INVALID_PRODUCT_CODE,
			"Product code is invalid");
		ErrorMessageBundle.setDefaultMessage(
			INVALID_IMAGE_URL,
			"Image url {0} is either invalid or exceeds {1} chars");

		ErrorMessageBundle.setDefaultMessage(
			INVALID_DESCRIPTION,
			"Description must be provided, upto {0} chars of data can be provided");

		ErrorMessageBundle.setDefaultMessage(
			INVALID_CKOUT_INSTR,
			"Checkout instructions is either empty or exceeded the maximum length of {0} chars");

		ErrorMessageBundle.setDefaultMessage(
			INVALID_SELLER_ID,
			"Seller Id is invalid");

		ErrorMessageBundle.setDefaultMessage(
			INVALID_CATEGORY_ID,
			"Category Id is invalid");

		ErrorMessageBundle.setDefaultMessage(
			INVALID_SHIP_PROFILE,
			"Shipping Profile is invalid");

		ErrorMessageBundle.setDefaultMessage(
			INVALID_MKT_PROFILE,
			"Marketing Profile is invalid");

		ErrorMessageBundle.setDefaultMessage(
			INVALID_BILL_PROOFILE,
			"Billing Profile is invalid");

		ErrorMessageBundle.setDefaultMessage(
			INVALID_BKEND_MAP,
			"Backend Map is invalid");
		ErrorMessageBundle.setDefaultMessage(
			NO_LISTING_DEFAULT_SETTING_ERROR,
			"No Listing Default Setting Present");
		ErrorMessageBundle.setDefaultMessage(
			NO_BACKEND_DEFAULT_SETTING_ERROR,
			"No Backend Default Settings Present");
		ErrorMessageBundle.setDefaultMessage(
			INVALID_RAW_THEME,
			"Raw Theme is invalid");
		ErrorMessageBundle.setDefaultMessage(
			INVALID_THEME_PROPERTY,
			"Theme Property is invalid");
		ErrorMessageBundle.setDefaultMessage(
			INVALID_THEME_TEMPLATE,
			"Theme Template is invalid");
		ErrorMessageBundle.setDefaultMessage(INVALID_UOM, "UOM is invalid");
		ErrorMessageBundle.setDefaultMessage(
			INVALID_SCHEDULE_DATE,
			"Schedule date is invalid");
		ErrorMessageBundle.setDefaultMessage(
			INVALID_FEEDBACK,
			"Feedback is invalid");
		ErrorMessageBundle.setDefaultMessage(
			INVALID_SCHED_JOB_TYPE,
			"Invalid Scheduler Job Type");

		ErrorMessageBundle.setDefaultMessage(
			INVALID_FEEDBACK_TYPE,
			"Feedback Type is missing or invalid");

		ErrorMessageBundle.setDefaultMessage(
			INVALID_FEEDBACK_COMMENT,
			"Feedback comment is missing or invalid");

		ErrorMessageBundle.setDefaultMessage(
			BKMAP_ALREADY_EXISTS,
			"eBay Backend Map Already Exists for the user");

		ErrorMessageBundle.setDefaultMessage(
			SELLER_PAYS_WITH_CKOUT_INSTRUCTIONS_ERROR,
			"Seller pays shipping is invalid if checkout instructions (shipping, insurance, handling etc) are provided");
			
		//NEW EXCEPTION TEXT
		ErrorMessageBundle.setDefaultMessage(
			INVALID_ORDER_TYPE,
			"Must provide one order type: either PayPal order type or regular order type");
		
		ErrorMessageBundle.setDefaultMessage(
			INVALID_QUOTATION_TYPE,
			"Quotation Type is missing or invalid");
		ErrorMessageBundle.setDefaultMessage(
			INVALID_REJECTION_REASON,
			"Rejection Reason is missing or invalid");
		ErrorMessageBundle.setDefaultMessage(
			INVALID_REFERENCE_BP,
			"Must provide the reference business partner number in non one time customer scenario");
		ErrorMessageBundle.setDefaultMessage(
			INVALID_COD_BP,
			"Must provide the reference one time customer in one time customer scenario");
		ErrorMessageBundle.setDefaultMessage(
			INVALID_PAYMENT_TYPE,
			"Must select at least one payment type");
		ErrorMessageBundle.setDefaultMessage(
			INVALID_SHIPPING_REGION,
			"Must select at least one shipping region if allowing shipping to other regions besides site");
		ErrorMessageBundle.setDefaultMessage(
			INVALID_PAYPAL_ACCOUNT,
			"Must provide Paypal account if allowing Paypal payment type");
		ErrorMessageBundle.setDefaultMessage(
			INVALID_INSURANCE_COST,
			"Must provide insurance fee if insurance is required");

		ErrorMessageBundle.setDefaultMessage(
			INVALID_EBAY_USERID,
			"eBay user id is missing or invalid");
		ErrorMessageBundle.setDefaultMessage(
			INVALID_EBAY_PWD,
			"eBay password is missing or invalid");
		ErrorMessageBundle.setDefaultMessage(
			INVALID_BACKEND_USERID,
			"SAP system user id is missing or invalid");
		ErrorMessageBundle.setDefaultMessage(
			INVALID_VALIDFROM_DATE,
			"Valid from date is missing or invalid");
		ErrorMessageBundle.setDefaultMessage(
			INVALID_VALIDUNTIL_DATE,
			"Valid until date is missing or invalid");
		ErrorMessageBundle.setDefaultMessage(
			UNTIL_DATE_BEFORE_FROM_DATE,
			"Valid until date is before valid from date");
		ErrorMessageBundle.setDefaultMessage(
			UNTIL_DATE_BEFORE_NOW,
			"Valid until date is is before current date");
		ErrorMessageBundle.setDefaultMessage(
			INVALID_TOTAL_PRICE_CONDITION,
			"Total price condition is missing or invalid");
		ErrorMessageBundle.setDefaultMessage(
			INVALID_DELIVERY_BLOCK,
			"Delivery block is missing or invalid");
		ErrorMessageBundle.setDefaultMessage(
			INVALID_CLIENT_SYSTEM_ID,
			"Client or System id is missing");
		ErrorMessageBundle.setDefaultMessage(
			IDENTICAL_CATEGORIES_ERROR,
			"Primary and Secondary categories are identical");	

		ErrorMessageBundle.setDefaultMessage(
			INVALID_VAT_PERCENT,
			"VAT Percent is invalid");	

		ErrorMessageBundle.setDefaultMessage(
			SHIPPING_REGIONS_NOT_SPEC_ERROR,
			"One of the shipping regions has to be specified");	

		ErrorMessageBundle.setDefaultMessage(
			SHIPPING_REGIONS_SPEC_ERROR,
			"Shipping regions cannot be specified");
		
		ErrorMessageBundle.setDefaultMessage(
			SHIPPING_CALC_ATTRS_SPEC_ERROR,
			"Shipping calculator attributes must not be specified for Flat Shipping");
		ErrorMessageBundle.setDefaultMessage(
			SHIPPING_CALC_ATTRS_NOT_SPEC_ERROR,
			"Shipping calculator attributes must be specified for Calculated Shipping");
		ErrorMessageBundle.setDefaultMessage(
			SHIPPING_FLAT_ATTRS_SPEC_ERROR,
			"Flat shipping attributes must not be specified for Calculated Shipping");			
		ErrorMessageBundle.setDefaultMessage(
			NON_B2B_VAT_CATEGORY_ERROR,
			"Category chosen is not B2B VAT enabled category");
		ErrorMessageBundle.setDefaultMessage(
			SHIPPING_PAYMENT_INFO_DETAILS_SPEC_FOR_NON_CKOUTDETAILS_ERROR,
			"Shipping and Payment details specified while checkoutdetails is not set");		
		ErrorMessageBundle.setDefaultMessage(
			INVALID_DURATION_FOR_FEEDBACK_RATING,
			"Positive feedback rating of {0} is insufficient for posting auction with duration of {1} day(s).Has to be alteast {2} or the Seller needs to be ID verified");		
		ErrorMessageBundle.setDefaultMessage(
			INVALID_BANKTRANSACTION_ID,
			"Invalid bank transaction internal id");	
		ErrorMessageBundle.setDefaultMessage(
			INSUFFICIENT_FEEDBACK_RATING_FOR_BIN_AUCTIONS,
			"Positive feedback rating of {0} insufficient for posting Buy It Now auctions.Has to be alteast {1} or the Seller needs to be ID verified");	
		ErrorMessageBundle.setDefaultMessage(
			REQUIRED_CATEGORY_ATTRIBUTE_MISSING,
			"Required Category Attribute {0} is missing");
		ErrorMessageBundle.setDefaultMessage(
			INVALID_CATEGORY_ATTRIBUTE_VALUE_LENGTH,
			"Category Attribute {0} can have maximum length of {1}");
		ErrorMessageBundle.setDefaultMessage(
			INVALID_MAX_CATEGORY_ATTRIBUTE_VALUES_SEL,
			"Category Attribute {0} can have maximum {1} of multi selected  values");	
		ErrorMessageBundle.setDefaultMessage(
			INVALID_MIN_CATEGORY_ATTRIBUTE_VALUES_SEL,
			"Category Attribute {0} can have minimum {1} of multi selected values");	
		ErrorMessageBundle.setDefaultMessage(
			INVALID_MAX_MIN_CATEGORY_ATTRIBUTE_VALUES_SEL,
			"Category Attribute {0} can have minimum {1} or maximum {2} of multi selected values");	
		ErrorMessageBundle.setDefaultMessage(
			INVALID_CATEGORY_ATTR_VAL_URL,
			"Category Attribute {0} has invalid URL");	
		ErrorMessageBundle.setDefaultMessage(
		INVALID_CATEGORY_ATTR_VAL_DATE_FORMAT,
			"Category Attribute {0} has invalid date format");	
		ErrorMessageBundle.setDefaultMessage(
			INVALID_CATEGORY_ATTR_VAL_NUM_RANGE,
			"Category Attribute {0} can have only values between {1} and {2}");
		ErrorMessageBundle.setDefaultMessage(
			INVALID_CATEGORY_ATTR_VAL_MAX_NUM,
			"Category Attribute {0} can have only maximum value of {1}");	
		ErrorMessageBundle.setDefaultMessage(
			INVALID_CATEGORY_ATTR_VAL_MIN_NUM,
			"Category Attribute {0} can have only minimum value of {1}");	
		ErrorMessageBundle.setDefaultMessage(
			INVALID_CATEGORY_ATTR_VAL_PRECISION,
			"Category Attribute {0} can have only precision of {1}");
		ErrorMessageBundle.setDefaultMessage(
			INVALID_CATEGORY_ATTR_VALUE_DATE,
			"Category Attributes {0} and {1} should have a value");
		ErrorMessageBundle.setDefaultMessage(
			INVALID_CATEGORY_ATTR_VALUE_DATE_RANGE,
			"Category Attribute {0} should have earlier date than the Category Attribute {1}");	
		ErrorMessageBundle.setDefaultMessage(
			INSUFFICIENT_FEEDBACK_RATING_FOR_FEATURED_AUCTIONS,
			"Positive feedback rating of {0} insufficient for posting featured/super featured auctions.Has to be alteast {1}");	
		ErrorMessageBundle.setDefaultMessage(
			CKOUT_INSTR_MISSING,
			"Checkout instruction is missing");	
		ErrorMessageBundle.setDefaultMessage(MALFORMED_TAG, 
			"A custom tag was not well-formed. No valid ending tag was found. " +
			"Please recheck the syntax of your tags in the HTML.");
		ErrorMessageBundle.setDefaultMessage(REQUIRED_ORDERTYPE_MISSING,
			"Order type is missing");
		ErrorMessageBundle.setDefaultMessage(INVALID_INSURANCE_FEE_SPEC,
			"Insurance Fee must be greater than 0.0");
		ErrorMessageBundle.setDefaultMessage(MALFORMED_TAG_NOCOMMAS,
		"A custom tag was not well-formed. Tag properties are missing commas. " +
		"Please recheck the syntax of your tags in the HTML.");
		ErrorMessageBundle.setDefaultMessage(MALFORMED_EMPTY_PROPERTY,
		"A custom tag was not well-formed. A tag property contains an empty value. " +
		"Please recheck the syntax of your tags in the HTML.");
		ErrorMessageBundle.setDefaultMessage(INVALID_EMAIL_ADDRESS,
		"Invalid email address.");
		
		ErrorMessageBundle.setDefaultMessage(
			SELLER_PAYS_WITHOUT_CKOUT_INSTRUCTIONS_ERROR,
			"If checkout instructions (shipping, insurance, handling etc) are not provided, Seller must pay shipping");
			
		ErrorMessageBundle.setDefaultMessage(
			SELLER_PAYS_SHIPPING_WITH_SHIPPING_IN_TAX_ERROR,
			"Shipping in tax is invalid option when Seller pays shipping");
			
		ErrorMessageBundle.setDefaultMessage(
			ATLEAST_ONE_PAYMENT_OPTION_WITH_CKOUTDETAILS_ERROR,
			"At least one payment option must be provided with checkout details");

		ErrorMessageBundle.setDefaultMessage(
			UNRECOGNIZED_THEME_PROPERTY_VALUE,
			"{0} is not a valid theme tag property");

		ErrorMessageBundle.setDefaultMessage(
			JAVASCRIPT_METHOD_NOT_ALLOWED,
			"The javascript method {0} is not allowed");
		
		ErrorMessageBundle.setDefaultMessage(
			HTML_ELEMENT_NOT_ALLOWED,
			"The HTML element {0} is not allowed");
			
		ErrorMessageBundle.setDefaultMessage(
			JAVASCRIPT_REMOTE_NOT_ALLOWED,
			"Script tag containing a src attribute is not allowed");
		ErrorMessageBundle.setDefaultMessage(
			RELIST_MIN_BID_ERROR,
			"Minimum bid for Relist auction cannot be greater than that of original auction");
		ErrorMessageBundle.setDefaultMessage(
			RELIST_RESERVEPRICE_ERROR,
			"Reserve Price for Relist auction cannot be greater than that of original auction");

        ErrorMessageBundle.setDefaultMessage(
            MAX_FEEDBACK_LENGTH_EXCEEDED,
            "The maximum length for feedback is 80 characters");
			
        ErrorMessageBundle.setDefaultMessage(
            FEEDBACK_CANNOT_BE_EMPTY,
            "Feedback text cannot be empty");
            
		ErrorMessageBundle.setDefaultMessage(
			RESERVEPRICE_NOT_SUPPORTED,
			"This eBay site does not support reserve price");
		ErrorMessageBundle.setDefaultMessage(
			INVALID_AUCTION_QUANTITY,
			"Quantity specified for auction {0} is invalid");			

		ErrorMessageBundle.setDefaultMessage(
			INVALID_DURATION,
			"Invalid Auction duration");		
		ErrorMessageBundle.setDefaultMessage(ALREADY_HIGHEST_BIDDER,
			"Cannot bid since already a Highest Bidder");
		ErrorMessageBundle.setDefaultMessage(BIDDER_IS_SELLER,
			"Seller cannot bid on the Auction");
		ErrorMessageBundle.setDefaultMessage(BID_FOR_CLOSED_AUCTION,
			"Bidding for Closed Auction");
		ErrorMessageBundle.setDefaultMessage(INVALID_BID_QTY,
			"Bid Quantity is greater than the Auction Quantity");
		ErrorMessageBundle.setDefaultMessage(NO_TARGETGRP_OR_BP,
			"No targetgroup or businesspartber choosed");
		ErrorMessageBundle.setDefaultMessage(INVALID_QTY_FOR_WINNER,
			"Winner Quantity allocation exceeds the available auction quantity");
			
        ErrorMessageBundle.setDefaultMessage(
            MAX_THEMELENGTH_EXCEEDED,
            "The maximum length for a theme is {0} characters");            
		ErrorMessageBundle.setDefaultMessage(
			MISSING_SHOP_PROPERTY,
			"Shop needs to be set for B2C checkout");            
		ErrorMessageBundle.setDefaultMessage(
			SEARCH_LENGTH_EXCEEDED,
			"Search key length can only be {0} chars.Hence exceeded.");            

		ErrorMessageBundle.setDefaultMessage(
			INVALID_LOCALE,
			"The locale code '{0}' for row {1}, column {2} is invalid.");            

		ErrorMessageBundle.setDefaultMessage(
			INVALID_TRANSACTION_AMOUNT,
			"The transaction amount '{0}' for row {1}, column {2} is invalid.");            
            
		ErrorMessageBundle.setDefaultMessage(
			EMPTY_COLUMN_CONTENT,
			"Content for row {0}, column {1} is empty.");            
        
		ErrorMessageBundle.setDefaultMessage(
			INVALID_CSV_COLUMN_COUNT,
			"Row {0} in CSV file has invalid number of columns. {1} columns " +
			"required.");   
	}

	/**
	 * @param arg0
	 */
	public ValidationException(Object culprits, Throwable arg0) {
		super(arg0);
		super.setContext(culprits);
	}

	/**
	 * @param arg0
	 */
	public ValidationException(Object culprits, I18nErrorMessage arg0) {
		super(arg0);
		super.setContext(culprits);
	}

	/**
	 * @param arg0
	 * @param arg1
	 */
	public ValidationException(
		Object culprits,
		I18nErrorMessage arg0,
		Throwable arg1) {
		super(arg0, arg1);
		super.setContext(culprits);
	}

	/**
	 * @param arg0
	 * @param arg1
	 * @param arg2
	 * @param arg3
	 * @param arg4
	 */
	public ValidationException(
		Object culprits,
		Category arg0,
		int arg1,
		Location arg2,
		I18nErrorMessage arg3,
		Throwable arg4) {
		super(arg0, arg1, arg2, arg3, arg4);
		super.setContext(culprits);
	}

	public Object getCulprits() {
		return super.getContext();
	}
}
