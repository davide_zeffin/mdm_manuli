package com.sap.isa.auction.businessobject.targetGroup;


import java.util.Comparator;

/**
 * Title:        Auction
 * Description:  Comparator to sort TargetGroups by description
 * Copyright:    Copyright (c) 2003
 * Company:
 * @author
 * @version 1.0
 */

public class TargetGroupComparator implements Comparator{

    public TargetGroupComparator() {
    }

    /**
     * compare two TargetGroups regarding description
     * @param object1 first object to compare, needs to be of class TargetGroup
     * @param object2 second object to compare, needs to be of class TargetGroup
     * @return result int which is < 0 if object1 < object2, = 0 if they are equal
     *         and >0 if object1 > object2
     */
    public int compare(Object o1, Object o2) {
        TargetGroup tg1 = (TargetGroup)o1;
        TargetGroup tg2 = (TargetGroup)o2;
        return tg1.getDescription().compareToIgnoreCase(tg2.getDescription());
    }
}