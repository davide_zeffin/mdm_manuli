/*****************************************************************************
    Copyright (c) 2001, SAPMarkets Palo Alto, USA, All rights reserved.

*****************************************************************************/

package com.sap.isa.auction.businessobject.targetGroup;

import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.businessobject.BusinessObjectBase;

import com.sap.isa.auction.backend.boi.targetGroup.TargetGroupData;
/**
 * business object that represents the Target Group details retrieved from the CRM
 */

public class TargetGroup extends BusinessObjectBase implements  TargetGroupData {

    private static IsaLocation log = IsaLocation.getInstance(TargetGroup.class.getName());

    private String guid = null;
    private String description = null;

    public TargetGroup() {
    }

    public String getGuid() {
        return guid;
    }

    public String getDescription() {
        return description;
    }

    public void setGuid(String guid) {
        this.guid = guid;
    }

    public void setDescription(String description){
        this.description = description;
    }


}
