/*****************************************************************************
    Copyright (c) 2001, SAPMarkets Palo Alto, USA, All rights reserved.

*****************************************************************************/


package com.sap.isa.auction.businessobject.targetGroup;

import com.sap.isa.auction.backend.boi.targetGroup.TargetGroupSearchData;
import com.sap.isa.businessobject.BusinessObjectBase;





/**
 * Holds the search data for searching for Target Groups.
 * Currently it supports the search by Bps and Tg descriptions
 */
public class TargetGroupSearch extends BusinessObjectBase
              implements TargetGroupSearchData {

    private String bpGuid;
    private String tgDescription;

    public TargetGroupSearch() {
    }

    public String getBpGuid(){
        return bpGuid;
    }

    public String getTgDescription() {
        return tgDescription;
    }

    public void setBpGuid(String bpguid) {
        bpGuid = bpguid;
    }

    public void setTgDescription(String tgdesc) {
        tgDescription = tgdesc;
    }

    public void setTgType(String tgtypd) {};

    public void setTgCreatedBy(String creatby){};

    public void setTgChangedBy(String changeby){};


}
