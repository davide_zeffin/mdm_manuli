/*****************************************************************************
    Copyright (c) 2001, SAPMarkets Palo Alto, USA, All rights reserved.

*****************************************************************************/


package com.sap.isa.auction.businessobject.targetGroup;

import java.util.*;

import com.sap.isa.core.businessobject.BackendAware;
import com.sap.isa.core.eai.*; //BackendobjectManager
import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.core.TechKey;
import com.sap.isa.backend.BackendTypeConstants;

import com.sap.isa.auction.backend.boi.targetGroup.TargetGroupListBackend;
import com.sap.isa.auction.backend.boi.targetGroup.TargetGroupListData;
import com.sap.isa.businessobject.*;





/**
 * The class is the modified version of the TargetGroupListBean in order to match
 * the EAI framework so TargetGroupList  becomes BackendAware
 */
public class TargetGroupList extends BusinessObjectBase implements
    TargetGroupListData, BackendAware {

    private HashMap targetGroupList = null;
    private BackendObjectManager bem;
    private TargetGroupListBackend backendService;
    //for Logging
    private static IsaLocation log = IsaLocation.getInstance(TargetGroupList.class.getName());

    public TargetGroupList() {
    }

    /**
     * @deprecated Please use the getTargetGroupObjectList(TargetGroupSearchData searchdata)
     */
    public HashMap getTargetGroupList() throws CommunicationException {
        try {
            getBackendService().getCRMTargetGroupList(this);

        }
        catch(BackendException ex) {
            if(log.isDebugEnabled())
                log.debug(ex);
        }
        return targetGroupList;
    }


   /**
    * @deprecated : Please use the getTargetGroupObjectList(TargetGroupSearchData searchdata)
    */
    public HashMap getTargetGroupObjectList() throws CommunicationException {
        try {
            getBackendService().getCRMTargetGroupObjectList(this);

        }
        catch(BackendException ex) {
            if(log.isDebugEnabled())
                log.debug(ex);
        }
        return targetGroupList;
    }

    /**
     * @deprecated : Please use the getTargetGroupObjectList(TargetGroupSearchData searchdata)
     * Get the targetGroup list for a specific user
     *
     * @param bpGuid GUID uniquely identifies the targetgroup list for the user
     */
    public HashMap getTargetGroupList(TechKey bpGuid) throws CommunicationException {
        try {
            getBackendService().getCRMTargetGroupList(this, bpGuid);

        }
        catch (BackendException ex) {
            if(log.isDebugEnabled())
                log.debug(ex);

        }
        return targetGroupList;
    }



    /**
     *  Used to retrieve the HashMap as a list of
     *  Key.           Value
     *  --------------------
     *  Guid,          TargetGroupObject
     *  Search could be null, in which case the complete list of Target Groups
     *  is retrieved from the CRM.
     */
    public HashMap getTargetGroupObjectList(TargetGroupSearch search) throws CommunicationException {
    	if ((targetGroupList == null) || 
    		(search.getTgDescription() != null)) {    	
        	try {
            	getBackendService().getCRMTargetGroupObjectList(this, search);
        	}
        	catch (BackendException ex) {
            	if(log.isDebugEnabled())
                	log.debug(ex);
        	}
    	}
        return targetGroupList;
    }

    /**
     * get sorted TargetGroupObjectList, which contains a collection of targetgroup beans
     * sorted by target group description
     */
    public Collection getSortedTargetGroupObjectList(TargetGroupSearch search) throws CommunicationException {
        getTargetGroupObjectList(search);
        ArrayList tgbeanlist = new ArrayList(targetGroupList.values());
        Collections.sort(tgbeanlist, new TargetGroupComparator());
        return tgbeanlist;
    }


    /**
     * Set the haspmap of TargetGroup List
     */
    public void setTargetGroupList(HashMap tglist) {
        targetGroupList = tglist;
    }

    /**
     * Returns a collection of TG descriptions.
     */
    public Collection getTargetGroupDescrOnly() {
        return targetGroupList.values();

    }

    /**
     * Returns a set of Target Group Ids
     */
    public Set getTargetGroupIDsOnly() {
        return targetGroupList.keySet();
    }

    /**
     * Method the BOM calls after a new object is created
     *
     * @param bem reference to the BackendObjectManager object
     */

    public void setBackendObjectManager (BackendObjectManager bem){
        this.bem = bem;
    }

    /**
     * Get the BackendService, if necessary
     */
    private TargetGroupListBackend getBackendService() throws BackendException {

        if (backendService == null) {
            // get the Backend from the Backend Object Manager
            backendService = (TargetGroupListBackend)bem.createBackendBusinessObject(BackendTypeConstants.BO_TYPE_AUCTION_TG_LIST,null);
        }
        return backendService;
    }



}
