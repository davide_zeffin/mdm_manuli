/*
 */
package com.sap.isa.auction.businessobject.email;

import java.util.ArrayList;

import com.sap.isa.auction.backend.BackendTypeConstants;
import com.sap.isa.auction.backend.boi.email.ErrorNotificationBackend;
import com.sap.isa.auction.backend.boi.email.ErrorNotificationData;
import com.sap.isa.auction.logging.CategoryProvider;
import com.sap.isa.businessobject.BusinessObjectBase;
import com.sap.isa.businessobject.BusinessObjectHelper;
import com.sap.isa.businessobject.CommunicationException;
import com.sap.isa.core.businessobject.BackendAware;
import com.sap.isa.core.eai.BackendException;
import com.sap.isa.core.eai.BackendObjectManager;
import com.sap.tc.logging.Category;
import com.sap.tc.logging.Location;

/**
 * @author I802758
 *
 * 
 */
public class ErrorNotification
	extends BusinessObjectBase
	implements ErrorNotificationData, Runnable, BackendAware {

	private static final Category logger = CategoryProvider.getBOLCategory();
	private static final Location tracer = Location.getLocation(ErrorNotification.class.getName());
			
	private BackendObjectManager bem;
	private ErrorNotificationBackend backendService;
	
	public String subject;
	
	public String from;
	
	public ArrayList to;
	
	public ArrayList cc;
	
	public String error;
	
	public String cause;
	
	public String resolution;
	
	/**
	 * 
	 * @param from from USER ID in backend 
	 * @param to To Arraylist containing the email addresses
	 * @param subject subject of the email
	 * @param error error to included in the email
	 * @param cause cause for the error
	 * @param resolution resoultion to the error
	 */
	public void setData( String from, ArrayList to, String subject, String error, String cause, String resolution){
		
		this.from = from;
		this.to = to;
		this.subject = subject;
		this.error = error;
		this.cause  = cause;
		this.resolution = resolution ;
	}
	
	/**
	 * 
	 * @param from from USER ID in backend 
	 * @param toaddress To email address
	 * @param subject subject of the email
	 * @param error error to included in the email
	 * @param cause cause for the error
	 * @param resolution resoultion to the error
	 */
	public void setData( String from, String toaddress, String subject, String error, String cause, String resolution){
		
		this.from = from;
		
		this.to = new ArrayList();
		to.add(toaddress);
		
		this.subject = subject;
		this.error = error;
		this.cause  = cause;
		this.resolution = resolution ;
	}
	
	public ErrorNotification(){
		super();
	}
	

	/* (non-Javadoc)
	 * @see com.sapmarkets.isa.auction.backend.boi.email.ErrorNotificationData#getError()
	 */
	public String getError() {
		return error;
	}

	/* (non-Javadoc)
	 * @see com.sapmarkets.isa.auction.backend.boi.email.ErrorNotificationData#getCuase()
	 */
	public String getCause() {
		return cause;
	}

	/* (non-Javadoc)
	 * @see com.sapmarkets.isa.auction.backend.boi.email.ErrorNotificationData#getResolution()
	 */
	public String getResolution() {
		return resolution;
	}

	/* (non-Javadoc)
	 * @see com.sapmarkets.isa.auction.backend.boi.email.ErrorNotificationData#getSubject()
	 */
	public String getSubject() {
		return subject;
	}

	/* (non-Javadoc)
	 * @see com.sapmarkets.isa.auction.backend.boi.email.ErrorNotificationData#getFrom()
	 */
	public String getFrom() {
		return from;
	}

	/* (non-Javadoc)
	 * @see com.sapmarkets.isa.auction.backend.boi.email.ErrorNotificationData#getTo()
	 */
	public ArrayList getTo() {
		return to;
	}

	/* (non-Javadoc)
	 * @see com.sapmarkets.isa.auction.backend.boi.email.ErrorNotificationData#getCC()
	 */
	public ArrayList getCC() {
		return cc;
	}

	/* (non-Javadoc)
	 * @see com.sapmarkets.isa.core.businessobject.BackendAware#setBackendObjectManager(com.sapmarkets.isa.core.eai.BackendObjectManager)
	 */
	public void setBackendObjectManager(BackendObjectManager bem) {
		this.bem = bem;
	}

	protected ErrorNotificationBackend getBackendService()
	throws BackendException {

		synchronized (this) {
			if (backendService == null) {
				backendService = (ErrorNotificationBackend) bem.createBackendBusinessObject(
					BackendTypeConstants.BO_TYPE_ERROR_NOTIFICATION, null);
			}
	
		}
		return backendService;
	}
	
	/**
	 * Sends error notification. Fill the necessary information before calling this method.
	 * @return true if mail is sent successfully
	 * false otherwise
	 * @throws CommunicationException
	 */
	public boolean sendErrorNotification() throws CommunicationException{
		try {
			ErrorNotificationBackend backend = getBackendService();
			return backend.sendErrorNotification(this);
		} catch (BackendException ex) {
			logger.errorT(tracer, "Error occured while sending error notification ");
			BusinessObjectHelper.splitException(ex);
		}				
		return false;
	}
	/**
	 * @param string
	 */
	public void setCause(String cause) {
		this.cause = cause;
	}

	/**
	 * @param list
	 */
	public void setCC(ArrayList list) {
		cc = list;
	}

	/**
	 * @param string
	 */
	public void setFrom(String from) {
		this.from = from;
	}

	/**
	 * @param string
	 */
	public void setResolution(String resolution) {
		this.resolution = resolution;
	}

	/**
	 * @param string
	 */
	public void setSubject(String subject ) {
		this.subject = subject ;
	}

	/**
	 * @param list
	 */
	public void setTo(ArrayList to) {
		this.to = to;
	}

	
	/**
	 * @param string
	 */
	public void setError(String error) {
		this.error = error;
	}

	/* (non-Javadoc)
	 * @see com.sapmarkets.isa.auction.backend.boi.BackendTask#run()
	 */
	public void run() {
		try {
			sendErrorNotification();
		} catch (CommunicationException e) {
			logger.errorT(tracer, "Error occured while sending error notification ");
		}
	}

}
