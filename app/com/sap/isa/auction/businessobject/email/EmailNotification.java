/*****************************************************************************
	Copyright (c) 2004, SAPLabs Palo Alto, USA, All rights reserved.

*****************************************************************************/

package com.sap.isa.auction.businessobject.email;


import com.sap.isa.auction.backend.BackendTypeConstants;
import com.sap.isa.auction.backend.boi.AuctionData;
import com.sap.isa.auction.backend.boi.WinnerData;
import com.sap.isa.auction.backend.boi.email.EmailNotificationBackend;
import com.sap.isa.auction.backend.boi.email.EmailNotificationData;
import com.sap.isa.auction.bean.Auction;
import com.sap.isa.auction.logging.CategoryProvider;
import com.sap.isa.businessobject.BusinessObjectBase;
import com.sap.isa.businessobject.BusinessObjectHelper;
import com.sap.isa.businessobject.CommunicationException;
import com.sap.isa.core.businessobject.BackendAware;
import com.sap.isa.core.eai.BackendException;
import com.sap.isa.core.eai.BackendObjectManager;
import com.sap.tc.logging.Category;
import com.sap.tc.logging.Location;


public class EmailNotification
	extends BusinessObjectBase
	implements EmailNotificationData , Runnable, BackendAware {

	private WinnerData winnerData;
	private BackendObjectManager bem;
	private EmailNotificationBackend backendService;
	private byte emailNotificationType;
	private Auction auction;
	
	private static final Category logger = CategoryProvider.getBOLCategory();
	private static final Location tracer = Location.getLocation(EmailNotification.class.getName());
	
	private String from ; //from Backend ID
	private String subject ; //subject for the email
	private String to ; //to address for the email
	private String scenario; // for checkout URL in email
	private String txnId; // for checkout URL in email
	
	/**
	 * 
	 */
	public EmailNotification() {
		super();
	}

	public void setData(byte emailNotificationType, String from, String to, String subject, Auction auction){
		this.emailNotificationType = emailNotificationType;
		this.from = from;
		this.subject = subject;
		this.auction = auction;
		this.to = to;
	}
	
	public void setData(byte emailNotificationType, String from, String to, String subject, Auction auction, WinnerData winner){
		setData(emailNotificationType, from, to, subject, auction);
		winnerData = winner;
	}
	/**
	 *	Returns Email notification type
	 * @return byte corresponding to the email notification type
	 */
	public byte getEmailNotificationType() {
		return emailNotificationType;
	}

	/**
	 * 
	 * @return the auction data, where we can get to know all of the
	 * attributes about auction
	 */
	public AuctionData getAuctionData() {
		return auction;
	}


	public void setBackendObjectManager(BackendObjectManager bom) {
		bem = bom;
		
	}

	protected EmailNotificationBackend getBackendService()
	throws BackendException {

		synchronized (this) {
			if (backendService == null) {
				backendService = (EmailNotificationBackend) bem.createBackendBusinessObject(
					BackendTypeConstants.BO_TYPE_BUYER_NOTIFICATION, null);
			}
	
		}
		return backendService;
	}

	/**
	 * @param auction
	 */
	public void setAuction(Auction auction) {
		this.auction = auction;
	}

	/* (non-Javadoc)
	 * @see com.sapmarkets.isa.auction.backend.boi.email.EmailNotificationData#sendEmail()
	 */
	public boolean sendEmail() throws CommunicationException{
		try {
			EmailNotificationBackend backend = getBackendService();
			return backend.sendEmailNotification(this);
		} catch (BackendException ex) {
			logger.error(tracer, "Errors occured while sending email notification to the buyers" , ex);
			BusinessObjectHelper.splitException(ex);
		}				
		return false;
	}


	/**
	 * @param b
	 */
	public void setEmailNotificationType(byte b) {
		emailNotificationType = b;
		
	}


	/* 
	 * Returns from backend user ID 
	 * @see com.sapmarkets.isa.auction.backend.boi.email.EmailNotificationData#getFrom()
	 */
	public String getFrom() {
		return from;
	}

	/* 
	 * Returns the TO email address, if exists
	 * @see com.sapmarkets.isa.auction.backend.boi.email.EmailNotificationData#getFrom()
	 */
	public String getTo() {
		return to;
	}
	
	/* (non-Javadoc)
	 * @see com.sapmarkets.isa.auction.backend.boi.email.EmailNotificationData#getSubject()
	 */
	public String getSubject() {
		return subject;
	}

	/* (non-Javadoc)
	 * @see com.sapmarkets.isa.auction.backend.boi.email.EmailNotificationData#getWinnerData()
	 */
	public WinnerData getWinnerData() {
		return winnerData;
	}
	
	public void setWinnerData(WinnerData winnerData){
		this.winnerData =winnerData;
	}

	/* (non-Javadoc)
	 * @see com.sapmarkets.isa.auction.backend.boi.BackendTask#run()
	 */
	public void run() {
		try {
			sendEmail();
		} catch (CommunicationException e) {
			logger.errorT(tracer, "Errors occured while sending email notification to the buyers");
		}
	}
	
	public void setScenarioName(String scenario){
		this.scenario = scenario;
	}
	
	public String getScenario(){
		return scenario;
	}
	/**
	 * Returns Transaction id
	 * @return
	 */
	public String getTxnId(){
		return txnId;
	}
	
	public void setTxnId(String txnId ){
		this.txnId = txnId;
	}
}
