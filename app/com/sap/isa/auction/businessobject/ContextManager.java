package com.sap.isa.auction.businessobject;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import com.sap.tc.logging.Category;
import com.sap.tc.logging.Location;
import com.sap.isa.auction.logging.CategoryProvider;
import com.sap.isa.auction.logging.LogHelper;

/**
 * Title:        Internet Sales
 * Description: Context Manager maintains the binding of Context with Calling thread
 *              for Business Objects of Auction. It provides a rudimentary service only.
 *              Each business method on the Business Object picks up the
 *              Context associated with the Calling thread from Context Manager.
 * Copyright:    Copyright (c) 2002
 * Company:      SAPMarkets
 * @author
 * @version 1.0
 */

public class ContextManager {

	private static Category logger = CategoryProvider.getBOLCategory();
    private static Location tracer = Location.getLocation(ContextManager.class.getName());

    private static Map ctxtMap = Collections.synchronizedMap(new HashMap());

    private ContextManager() {
    }

    /**
     * Associates the supplied context with the Thread.
     * Nested contexts are not maintained by Context Manager.
     * User must preserve the old Context and restore it after thread execution is
     * finished with new context
     *
     * @param reqCtxt the externally bound context
     * @return InvocationContext previous associated context with thread
     * @throws IllegalArgumentException if reqCtxt is null,
     *      to disassociate context from thread invoke unbind
     */
    public static InvocationContext bindContext(InvocationContext reqCtxt) {
    	final String METHOD = "bndContext";
        if(reqCtxt == null) {
            IllegalArgumentException ex = new IllegalArgumentException("Request Context is null");
            LogHelper.logError(logger,tracer,ex);
            throw ex;
        }

        Thread current = Thread.currentThread();
        InvocationContext oldCtxt = unbindContext();
        ctxtMap.put(current,reqCtxt);

		if(logger.beInfo())
        	logger.infoT(tracer,METHOD,"Bound Context with Thread " + current.toString());

        return oldCtxt;
    }

    /**
     * @return RequestContext reqCtxt bound with the Calling Thread
     *                      null, if there is no context associated
     */
    public static InvocationContext getContext() {
        Thread current = Thread.currentThread();
        return (InvocationContext)ctxtMap.get(current);
    }

    /**
     * Unbinds the Context with the Current Thread
     * @return RequestContext the previous bound context
     */
    public static InvocationContext unbindContext() {
    	final String METHOD = "unbindContext";
    	
        Thread current = Thread.currentThread();
        InvocationContext oldCtxt = null;
        if(ctxtMap.containsKey(current)) {
            if(logger.beInfo()) {
                logger.infoT(tracer,METHOD,"Unbound Context with Thread " + current.toString());
            }
            oldCtxt = (InvocationContext)ctxtMap.remove(current);
        }
        return oldCtxt;
    }
}