/*
 * Title:        eBay Integration Project
 * Description:	 * SAP Labs LLC, the eBay auction project, also is known as Selling via eBay. 
 * Selling via eBay application provides SAP R/3 (and in the future CRM) 
 * customers to auction and sell inventory/products onto an online marketplace 
 * eBay. The application will provide the ability to Create, Schedule, Publish, 
 * Monitor and Close Auction Listings onto eBay and handle all the relevant 
 * Backend processing for Customers and Orders. 

 * Copyright:    Copyright (c) 2003
 * Company:      SAP Labs LLC
 * @author
 * @version 1.0
 */
package com.sap.isa.auction.businessobject.order;

import java.util.List;

import com.sap.isa.auction.backend.exception.CommunicationBackendUserException;
import com.sap.isa.auction.bean.Address;
import com.sap.isa.auction.bean.Auction;
import com.sap.isa.auction.bean.Winner;
import com.sap.isa.auction.logging.CategoryProvider;
import com.sap.isa.auction.util.StringUtil;
import com.sap.isa.backend.boi.isacore.ShopData;
import com.sap.isa.backend.boi.isacore.UserData;
import com.sap.isa.businessobject.CommunicationException;
import com.sap.isa.businessobject.ShipTo;
import com.sap.isa.businessobject.header.HeaderSalesDocument;
import com.sap.isa.businessobject.item.ItemSalesDoc;
import com.sap.isa.businessobject.order.Order;
import com.sap.isa.businessobject.order.OrderChange;
import com.sap.isa.core.TechKey;
import com.sap.tc.logging.Category;
import com.sap.tc.logging.Location;


public class AuctionOrderChange extends OrderChange {

	private static final Category logger = CategoryProvider.getBOLCategory();
	private static final Location tracer = Location.getLocation(AuctionOrderChange.class.getName());

	/**
	 * @param arg0
	 */
	public AuctionOrderChange(AuctionOrder arg0) {
		super(arg0);
	}

	/**
	 * Retrieves the complete order information for update.
	 * This method is still using the ISA logging, since it will be called
	 * from the ISA b2b context
	 * @param winner
	 * @throws CommunicationException
	 */
	public void readForUpdate(Winner winner,
								UserData usr, 
								ShopData shop)
		   throws CommunicationException {
		 //set the TechKey to the order object
		TechKey orderTechKey = new TechKey(winner.getOrderId());
		order.setTechKey(orderTechKey);
		order.getHeaderData().setTechKey(orderTechKey);
		order.getHeaderData().setShop(shop);
		order.readForUpdate(shop);
		//Set all of the items for winning auction
		//in the order not changable.
		setNoUpdateToItems(order);
	}
	

	/**
	 *  
	 * @param changeInfo		The information needes to be updated.
	 * @throws CommunicationException
	 */
	public void updateOrder(AuctionOrderChangeInfo changeInfo, boolean validAddress) 
										throws CommunicationException {
		if (order == null){
			logger.errorT(tracer, "In the AuctionOrderChange, the order object is null");
			throw new CommunicationException(" No Order Object exists for" +
				"updates");
		}
		ShipTo shipTo = null;
		logger.infoT(tracer, "Beginning of the updateOrder(changeInfo)");
		try{
			// here we update all the order information
			String salesDocNum = StringUtil.padLeadingZerosToSalesDocNumber(
						order.getHeader().getSalesDocNumber());
			String changInfoOrderId = StringUtil.padLeadingZerosToSalesDocNumber(
						changeInfo.getOrderId());
			if (!salesDocNum.equalsIgnoreCase(
								changInfoOrderId)){
				throw new CommunicationException ("Exception order ID is not" +
					" consistent!");				
			}
			// update the order information
			HeaderSalesDocument header = order.getHeader();
			if (changeInfo.getFreightValue() != null){
				header.setFreightValue(changeInfo.getFreightValue());
				logger.infoT(tracer, "setFrieght value" + changeInfo.getFreightValue());			
			}
			if(changeInfo.getInsuranceValue() != null){
				((AuctionOrder)order).setInsuranceValue(
				changeInfo.getInsuranceValue());
			}
			if(changeInfo.getTaxValue() != null){
				header.setTaxValue(changeInfo.getTaxValue());
			}
			// this is the total value, including the freight and tax
			if(changeInfo.getTotalValue() != null){
				header.setNetValue(changeInfo.getTotalValue());
				header.setGrossValue(changeInfo.getTotalValue());
				logger.infoT(tracer, "setTotal value" + changeInfo.getTotalValue());
			}
			// this will set the shipping address to shipto
			if (changeInfo.getShippingAddress() != null){
				shipTo = updateAddress(changeInfo.getShippingAddress());
			}else{
				logger.warningT(tracer, "updateAddress is not called, " +
					"address is " + changeInfo.getShippingAddress());
			}
			logger.infoT(tracer, "After update Shipping address shipTo is" + shipTo);
			order.getHeader().setShipTo(shipTo);
			// save the changes
			com.sap.isa.businessobject.Address isaAddress;
			if(validAddress) {
				isaAddress = mapAuctionAddresstoIsaAddress(changeInfo.getShippingAddress());
				try{
					logger.infoT(tracer, "Before update shipping info address is " + isaAddress);
					((AuctionOrder)order).updateShippingInfo(isaAddress);
					((AuctionOrder)order).setUpdateAddress(true);
					logger.infoT(tracer, "After update shipping info ");
				}catch(Exception ex){
					logger.errorT(tracer, "The shipping infomation updating failed");
				}
			} else {
				//isaAddress = new com.sapmarkets.isa.businessobject.Address();
				//isaAddress.setCountry(changeInfo.getShippingAddress().getCountry());
				((AuctionOrder)order).setUpdateAddress(false);
				shipTo.setAddress(null);
			}
			try{
				logger.infoT(tracer, "Before update price info ");
				((AuctionOrder)order).updatePriceInfo();
				logger.infoT(tracer, "After update price info ");
			}catch (Exception ex){
				logger.errorT(tracer, "The pricing updating failed");
			}
			order.saveAndCommit();
		}catch(CommunicationBackendUserException ex){
			logger.errorT(tracer, "During the update sales order exception throws" + ex.getMessage());
			throw ex;		
		}catch(Exception ex){
			logger.errorT(tracer, "During the update sales order exception throws" + ex.getMessage());
			throw new CommunicationException("Other exceptions in AuctionOrderChange");		
		}
	}

	
	/**
	 * Here we have to convert the addresss from auction to the 
	 * address to ISA
	 * @param address				The auction address
	 */
	private ShipTo updateAddress(Address address) {
		if(logger.beInfo()){
			logger.infoT(tracer, "---The shipping address from auction---");
			logger.infoT(tracer, address.toString());
		}
		
		ShipTo shipTo = null;
		try{
			shipTo = order.getHeader().getShipTo();
		if (shipTo == null){
			if(logger.beInfo()){
				logger.infoT(tracer, "Create a new Ship To for shipping adress update");
			}
			shipTo = new ShipTo();
			shipTo.setId(order.getHeader().getPartnerListData().getSoldToData().getPartnerId());
			shipTo.setManualAddress();
			shipTo.setTechKey(order.getHeader().getPartnerListData().getSoldToData().getPartnerTechKey());		
		}
		com.sap.isa.businessobject.Address isaAddress = 
				mapAuctionAddresstoIsaAddress(address);
		// used the ship in the order information
		isaAddress.setAddressPartner(order.getHeader().getShipTo().getId());
		//shipTo.setId(order.getHeader().getShipTo().getId());
		shipTo.setAddress(isaAddress);
		if ((shipTo.getTechKey() == null))
			shipTo.setTechKey(new TechKey("tt"));
		}catch(Exception ex){
			logger.errorT(tracer, "Error in getting ShipTo" + ex.getMessage());
		}
		return shipTo;
	}
	
	private com.sap.isa.businessobject.Address mapAuctionAddresstoIsaAddress(Address address){
		com.sap.isa.businessobject.Address isaAddress = 
						new com.sap.isa.businessobject.Address();
		if (address.getCity()!= null){
			isaAddress.setCity(address.getCity());
			logger.infoT(tracer, "address city is" + address.getCity());
		}else{
			logger.warning(tracer, "The shipping address city is null");
		}
		if(address.getCountry() != null){
			isaAddress.setCountry(address.getCountry());						
			logger.infoT(tracer, "country is" + address.getCountry());
		}else{
			logger.warning(tracer, "The shippiong address country is null");
		}
		if(address.getName() != null){
			isaAddress.setLastName(address.getName());
			isaAddress.setName1(address.getName());	
			logger.infoT(tracer, "name is" + address.getName());		
		}else{
			logger.warning(tracer, "The name in shipping address is null");
		}
		if(address.getStateOrProvince() != null){
			isaAddress.setRegion(address.getStateOrProvince());			
			logger.infoT(tracer, "state is" + address.getStateOrProvince());
		}
		if (address.getStreet1() != null){
			StringBuffer street = new StringBuffer();
			street.append(address.getStreet1());
			if (address.getStreet2() != null){
				street.append(' ');
				street.append(address.getStreet2());				
			}
			isaAddress.setStreet(street.toString());
			logger.infoT(tracer, "street is" + street);			
		}
		if (address.getZip() != null){
			isaAddress.setPostlCod1(address.getZip());
			logger.infoT(tracer, "zipcode is" + address.getZip());
		}
		logger.infoT(tracer, "ISA Address is" + isaAddress);
		return isaAddress;
	}
	
	/**
	 * from the auction we can get the all of the products related to this auction
	 */
	private List getProductList(Auction auction) {
		//
		List items = null;
		if (auction != null) {
			items = auction.getItems();
		} else {
			logger.infoT(tracer, "In AuctionOrder Creation the auction is null");
		}
		if (items != null) {
			logger.infoT(tracer, 
				"In AuctionOrder Creation the items size is " + items.size());
		}
		return items;
	}

	/**
	 * Set No Update, No Delete to the winning auction items,
	 * Those items can not be modified.
	 * @param order
	 */
	private void setNoUpdateToItems(Order posd) {
		int size = posd.getItems().size();

		// set the flags
		for (int i = 0; i < size; i++) {
			ItemSalesDoc item = posd.getItems().get(i);
			item.setChangeable(false);
			item.setCancelable(false);
			item.setDeletable(false);
		}		
	}

	/** 
	 * This class is the bridge class between a the transaction info
	 * from Auction side and the update information inside of sales order
	 *  Total Amount
	 *	Shipping Address
	 *	Tax
	 *	Shipping Handling
	 *	Insurance
	 * 
	 */
	private class AuctionOrderChangeInfo {
		// the unique auction ID defined at our end
		private String auctionId = null;
		// the order ID generated in backend 
		private String orderId = null;
		// the total amount of value
		private String totalValue = null;
		// the shipping address
		private Address shippingAddress = null;
		// tax value
		private String taxValue = null;
		// freight fee
		private String freightValue = null;
		// insurance value
		private String insuranceValue = null;
		
		public AuctionOrderChangeInfo (String aucId){
			auctionId = aucId;
		}
		
		public AuctionOrderChangeInfo (String aucId,
									   String orderUniqueId){
			auctionId = aucId;
			this.orderId = orderUniqueId;
		}
		
		
		
		/**
		 * @return
		 */
		public String getAuctionId() {
			return auctionId;
		}

		/**
		 * @return
		 */
		public String getFreightValue() {
			return freightValue;
		}

		/**
		 * @return
		 */
		public String getInsuranceValue() {
			return insuranceValue;
		}

		/**
		 * @return
		 */
		public String getOrderId() {
			return orderId;
		}

		/**
		 * @return
		 */
		public Address getShippingAddress() {
			return shippingAddress;
		}

		/**
		 * @return
		 */
		public String getTaxValue() {
			return taxValue;
		}

		/**
		 * @return
		 */
		public String getTotalValue() {
			return totalValue;
		}

		/**
		 * @param string
		 */
		public void setAuctionId(String string) {
			auctionId = string;
		}

		/**
		 * @param string
		 */
		public void setFreightValue(String string) {
			freightValue = string;
		}

		/**
		 * @param string
		 */
		public void setInsuranceValue(String string) {
			insuranceValue = string;
		}

		/**
		 * @param string
		 */
		public void setOrderId(String string) {
			orderId = string;
		}

		/**
		 * @param address
		 */
		public void setShippingAddress(Address address) {
			shippingAddress = address;
			logger.infoT(tracer, "The Address in setShippingAddress is " + shippingAddress);
		}

		/**
		 * @param string
		 */
		public void setTaxValue(String string) {
			taxValue = string;
		}

		/**
		 * @param string
		 */
		public void setTotalValue(String string) {
			totalValue = string;
		}
		
		public String toString(){
			StringBuffer buffer = new StringBuffer();
			buffer.append("Change Order ID :");
			buffer.append((orderId== null)?"":orderId);
			buffer.append("The Total value :");
			buffer.append((totalValue == null)?"":totalValue);
			buffer.append("The fright :");
			buffer.append((freightValue == null)?"":freightValue);
			buffer.append("The tax :");
			buffer.append((taxValue == null)?"":taxValue);
			buffer.append("The Insurance :");
			buffer.append((insuranceValue == null)?"":insuranceValue);
			buffer.append("The shipping adress :");
			buffer.append((shippingAddress == null)?"":shippingAddress.toString());			
			return buffer.toString();		
		}

	}

}
