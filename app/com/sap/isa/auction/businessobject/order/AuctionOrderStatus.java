/*
 * Created on Sep 18, 2003
 *
 * SAP Labs LLC, the eBay auction project, also is known as Selling via eBay. 
 * Selling via eBay application provides SAP R/3 (and in the future CRM) 
 * customers to auction and sell inventory/products onto an online marketplace 
 * eBay. The application will provide the ability to Create, Schedule, Publish, 
 * Monitor and Close Auction Listings onto eBay and handle all the relevant 
 * Backend processing for Customers and Orders. 

 */
package com.sap.isa.auction.businessobject.order;

import java.util.ArrayList;
import java.util.Locale;

import com.sap.tc.logging.Category;
import com.sap.tc.logging.Location;
import com.sap.isa.auction.backend.boi.order.AuctionOrderStatusBackend;
import com.sap.isa.auction.backend.boi.order.AuctionOrderStatusData;
import com.sap.isa.auction.backend.crm.salesdocument.AuctionSalesDocumentsStatusCRM;
import com.sap.isa.auction.logging.CategoryProvider;
import com.sap.isa.auction.util.StringUtil;
import com.sap.isa.backend.boi.isacore.order.OrderStatusBackend;
import com.sap.isa.businessobject.BusinessObjectHelper;
import com.sap.isa.businessobject.CommunicationException;
import com.sap.isa.businessobject.order.OrderStatus;
import com.sap.isa.core.TechKey;
import com.sap.isa.core.eai.BackendException;

/**
 * @ extended from ISA OrderStatus, which contains a list of orders and a 
 * default order for the display purpose
 */
public class AuctionOrderStatus
	extends OrderStatus
	implements AuctionOrderStatusData {

	
	/**
	 * all of the order header that is filtered
	 */
	private ArrayList filteredOrderHeaders = null;
	
	/**
	 * the responsibel employee ID
	 */
	private String employeeId = null;

	private static final Category logger = CategoryProvider.getBOLCategory();
	private static final Location tracer = Location.getLocation(AuctionOrderStatus.class.getName());

	/**
	 * 
	 */
	private AuctionOrderStatusBackend backendService = null;

	/*
	 * Default constructor 	 
	 **/
	public AuctionOrderStatus(){
		order = new AuctionOrder();
		filteredOrderHeaders = new ArrayList(5);
	}
	
	/*
	 * Default constructor 	 
	 **/
	public AuctionOrderStatus(AuctionOrder order){
		this.order = order;
		filteredOrderHeaders = new ArrayList(5);
		logger.infoT(tracer, "Done Initlizing AuctionOrderStatus");
	}
	
	public boolean isOneTimeCustomerScenario() {
		return false;
	}

	/**
	 * @return the sales employee ID, it can be get from the
	 * DocumentFilter
	 */
	public String getResponsibleEmployee() {
		return employeeId;
	}

	/**
	 * @param the sales employee ID, is used for the filtering purpose
	 */
	public void setResponsibleEmployee(String saleEmployeeID){
		employeeId = saleEmployeeID;
	}
	
	/**
	 * @return the collection of order headers after filering
	 * by the criteria of auction related
	 */
	public ArrayList getFilteredOrderHeaders() {
		return filteredOrderHeaders;
	}

	/**
	 * @param the order header to be added to the filtering list
	 */
	public void addOrderHeaderFiltered(String orderHeader) {
		filteredOrderHeaders.add(orderHeader);

	}

	/**
	 * Read all of the OrderHeaders
	 * @param 		a list of order Ids(string) holds the OrderID
	 */
	public void readOrderHeaders(ArrayList orderIds)throws CommunicationException {
		for (int i= 0; i< orderIds.size(); i++){
			String id = (String)orderIds.get(i);
			filteredOrderHeaders.add(id);				
		}
		
		try {
			// read AttributeSet from Backend
			getBackendService().readOrderHeaders(this);
		}
		catch (BackendException ex) {
			BusinessObjectHelper.splitException(ex);
		}
	}


	public boolean isBasedOnCRM() {
		try {
			if(getBackendService() instanceof AuctionSalesDocumentsStatusCRM)
				return true;
			else
				return false;	
		} catch (BackendException e) {
			return false;
		}
	}

	/**
	  * get the suitable BackendService for the given sales document type
	  *
	  * @param type sales document type (see <code>DocumentListFilterData</code>
	  *             for allowed values
	  *
	  * @see com.sap.isa.backend.boi.isacore.order.DocumentListFilterData
	  */
	protected OrderStatusBackend getBackendService()
				throws BackendException {

//		if (!type.equals(DocumentListFilter.SALESDOCUMENT_TYPE_ORDER)) {
//			log.warn("A wrong type is passed, in auction we " +
//				"only handle type order");
//		}
		
		logger.infoT(tracer, "GetBackendService");
		if (backendService == null) {
			try {
					// get the Backend from the Backend Manager
					backendService = (AuctionOrderStatusBackend) bem
						.createBackendBusinessObject("AuctionOrderStatus", null);

				}
				catch (Exception ex) {
					if(logger.beInfo()) {
						logger.infoT(tracer, 
								"Exception occured in getBackendService():"
								);
					}
					throw(new BackendException(ex));
				}
		}
		logger.infoT(tracer, "end GetBackendService");
		return backendService;
	} // OrderListBackend

	/**
	 * Read the object for the given techKey
	 *
	 */
	public void readOrderStatus(TechKey techKey,
								String orderNumber)
			throws CommunicationException {
		logger.infoT(tracer, "Beginning readOrderStatus");
		if ((techKey == null) && (orderNumber == null)){
			throw new CommunicationException ("passing orer Id " +									"and tech ID are null");
		}
		String ppId = null;
		if (techKey == null) {
			// re-read requested if neccessary
			// we use the sales
			logger.infoT(tracer, "techKey is null in readOrderStatus");
			if (orderNumber.length() == StringUtil.TRANSACTION_NUMBER_LENGTH){
				ppId = orderNumber; 
			}else{
				ppId = StringUtil.ExtendNumber(orderNumber,
							StringUtil.TRANSACTION_NUMBER_LENGTH);
			}
			this.setTechKey(new TechKey(ppId));		
			order.setTechKey(new TechKey(ppId));
			this.setSalesDocumentNumber(ppId);
			order.getHeader().setSalesDocNumber(ppId);
			order.getHeader().setTechKey(new TechKey(ppId));
		} else {
			// Normal entry, read details
			logger.infoT(tracer, "Using normal sales order number in readOrderStatus");
			ppId = techKey.getIdAsString();
			if (!(ppId.length() == StringUtil.TRANSACTION_NUMBER_LENGTH)){
				ppId = StringUtil.ExtendNumber(techKey.getIdAsString(),
							StringUtil.TRANSACTION_NUMBER_LENGTH);
			}
			this.setTechKey(new TechKey(ppId));		
			order.setTechKey(new TechKey(ppId));
			this.setSalesDocumentNumber(ppId);
			order.getHeader().setSalesDocNumber(ppId);
			order.getHeader().setTechKey(new TechKey(ppId));
			//this.setOrderHeader(null);
			//this.getItemList().clear();
		}
		logger.infoT(tracer, "before calling backend service in readOrderStatus");
		try {
			// read AttributeSet from Backend
			getBackendService().readOrderStatus(this);
		}
		catch (BackendException ex) {
			BusinessObjectHelper.splitException(ex);
		}
		logger.infoT(tracer, "end calling backend service in readOrderStatus");
	}
	/* (non-Javadoc)
	 * @see com.sap.isa.auction.backend.boi.order.AuctionOrderData#setInsurManualPriceCondition(java.lang.String)
	 */
	public void setInsurManualPriceCondition(String priceType) {
		((AuctionOrder)order).setInsurManualPriceCondition(priceType);
		logger.infoT(tracer, "The insurance condition type is" + priceType);
		
	}
	
	/* (non-Javadoc)
	 * @see com.sap.isa.auction.backend.boi.order.AuctionOrderData#setFreightManualPriceCondition(java.lang.String)
	 */
	public void setFreightManualPriceCondition(String priceType) {
		((AuctionOrder)order).setFreightManualPriceCondition(priceType);
		logger.infoT(tracer, "The freight condition type is" + priceType);
	}
	public Locale getLocale()  {
		return backendService.getBackendLocale() != null ? backendService.getBackendLocale() : Locale.getDefault();
	}
}
