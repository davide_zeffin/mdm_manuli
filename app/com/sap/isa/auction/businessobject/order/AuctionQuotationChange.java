/*
 * Created on Sep 22, 2003
 *
 * SAP Labs LLC, the eBay auction project, also is known as Selling via eBay. 
 * Selling via eBay application provides SAP R/3 (and in the future CRM) 
 * customers to auction and sell inventory/products onto an online marketplace 
 * eBay. The application will provide the ability to Create, Schedule, Publish, 
 * Monitor and Close Auction Listings onto eBay and handle all the relevant 
 * Backend processing for Customers and Orders. 

 */
package com.sap.isa.auction.businessobject.order;

import java.util.Iterator;
import java.util.List;

import com.sap.isa.auction.bean.Auction;
import com.sap.isa.auction.bean.AuctionItem;
import com.sap.isa.auction.bean.SalesArea;
import com.sap.isa.auction.logging.CategoryProvider;
import com.sap.isa.auction.util.StringUtil;
import com.sap.isa.businessobject.BORuntimeException;
import com.sap.isa.businessobject.BusinessObjectException;
import com.sap.isa.businessobject.CommunicationException;
import com.sap.isa.businessobject.header.HeaderSalesDocument;
import com.sap.isa.businessobject.item.ItemList;
import com.sap.isa.businessobject.item.ItemSalesDoc;
import com.sap.isa.core.TechKey;
import com.sap.tc.logging.Category;
import com.sap.tc.logging.Location;

/**
 * This class is used to change the status of the quotation and also 
 * set the listing ID as the backend purchase order number.
 */
public class AuctionQuotationChange extends AuctionQuotationDecoratorBase {

	private static final Category logger = CategoryProvider.getBOLCategory();
	private static final Location tracer = Location.getLocation(AuctionQuotationChange.class.getName());

	/**
	 * Creates a new instance without parameters
	 *
	 */
	public AuctionQuotationChange() {
	}

	/**
	 * Creates a new instance for the given <code>Quotation</code> object.
	 *
	 * @param order Quotation to decorate
	 */
	public AuctionQuotationChange(AuctionQuotation quotation) {
		super(quotation);
	}
	
	/**
	 * Update the quotation status, filling with different status 
	 * @
	 */
	public void updateQuotationStatus() throws CommunicationException{
		try {
			quotation.modifyQuotation();
		}catch (Exception ex){
			logger.errorT(tracer, 
						"Backend exception in AuctionQuotation Change");		
		}
	}
	
	/**
	 * Update the purchase order number, filling with auction listing ID 
	 * @
	 */
	public void updateListingId (String listingId) throws 
											CommunicationException{
		quotation.setPurchaseNumber(listingId);
		///Now call the backend to do modification
		quotation.setListingId(listingId);													
	}

	/**
	 * Cancel a Quotation, first we have to read the quotation, then
	 * set the status to the cancel status.
	 * @param TechKey, the unique identifier of a quotation ID
	 */
	public void cancelQuotationInBackend(TechKey techKey) throws 
												 BusinessObjectException,
												BORuntimeException {
		quotation.setTechKey(techKey);

		quotation.cancel(techKey);
	}
	
	/**
	 * 
	 * @param auction				Auction Object to modify a quotation
	 * 								it syncs the auction obejvet with the
	 * 								quotation object
	 * @throws CommunicationException
	 */
	public void modifyQuotation(Auction auction) throws 
												CommunicationException{
		// sync the quotation and the auction data.
		try {
			upDateHeader(auction);
			updateItems(auction);
			quotation.modifyQuotation();
		}catch(CommunicationException ex){
			logger.errorT(tracer, "In updating Quotation ");
			throw ex;
		}catch(Exception ex){
			logger.errorT(tracer, "In updating Quotation Other exception ");
			throw new CommunicationException("In AuctionQuotationChange, " +					"updating broken");
		}
												
	}

	/**
	 * Set the header information for the quotation from an auction
	 * @param the auction, which is in the context
	 */
	private void upDateHeader(Auction auction){
		SalesArea salesArea = (SalesArea)auction.getSalesArea();
		HeaderSalesDocument header = quotation.getHeader();
		
		if (salesArea != null) {
			updateHeaderSalesArea(header, salesArea);
		}else{
			logger.warningT(tracer, 
					"Sales Area is not defined in Auction");
		}
		//TO DO: header.setCurrency();
		header.setDescription("Quotation for" + auction.getAuctionDescription());
		
		//To DO: Get the qoutation document type header.setDocumentType(auction.g)
		header.setStatusOpen();
		///KIND of silly, we set the reserved price to the, in order to show the
		/// it in the overview screen of sales order
		try{
			String reservedPrice = header.getNetValue();
			if (reservedPrice == null){
				header.setNetValue(auction.getReservePrice().toString());
			}else if(!reservedPrice.equalsIgnoreCase(
					auction.getReservePrice().toString())){
				header.setNetValue(auction.getReservePrice().toString());			
			}else{
				if(logger.beInfo()){
					logger.infoT(tracer, "In AuctionQuotationChange the reserved" +						" price has not been changed");				
				}
			}

		}catch(NullPointerException ex){
			logger.errorT(tracer, "Some attributes in Order have been not set");
		}
	}
	
	
	/**
	 * The the sales area to the header level.
	 * @param the sales area from auction definition
	 * @
	 */
	private void updateHeaderSalesArea(HeaderSalesDocument header, 
								SalesArea salesArea){		
		String salesOrg = header.getSalesOrg();
		String disChannel = header.getDisChannel();
		String division = header.getDivision();
		if (salesOrg == null){
			header.setSalesOrg(salesArea.getSalesOrganization());
		}else if(!salesOrg.equalsIgnoreCase(salesArea.getSalesOrganization())){
			header.setSalesOrg(salesArea.getSalesOrganization());			
		}else{
			if (logger.beInfo()){
				logger.infoT(tracer, "The sales organization is NOT" +					"	changed in AuctionQuotationChange");
			}
		}
		if (disChannel == null){
			header.setDisChannel(salesArea.getDistributionChannel());
		}else if(!disChannel.equalsIgnoreCase(
				salesArea.getDistributionChannel())){
			header.setDisChannel(salesArea.getDistributionChannel());			
		}else{
			if (logger.beInfo()){
				logger.infoT(tracer, "The Distribution Channel is NOT" +
					"	changed in AuctionQuotationChange");
			}
		}
		if (division == null){
			header.setDivision(salesArea.getDivision());
		}else if(!division.equalsIgnoreCase(salesArea.getDivision())){
			header.setDivision(salesArea.getDivision());			
		}else{
			if (logger.beInfo()){
				logger.infoT(tracer, "The division is NOT " +
					"	changed in AuctionQuotationChange");
			}
		}
		
	}
	
	/**
	 * Set the item level data
	 * @param the auctio, used to create a quotation
	 */	
	private void updateItems(Auction auction) throws CommunicationException{
		//Get the items from the quotation
		ItemList items = quotation.getItems();
		List list = getProductList(auction);
		//now build up the itemlist
		// build up the item list
		updateItemList(items, list);		
	}
	
	/**
	 * Set the items for a sales order, all the product information
	 * @param: list the ItemList for a sales order
	 * @param: all the products retrieve from an auction
	 */
	private void updateItemList(ItemList list, List allProducts) 
								throws CommunicationException {
		if (allProducts != null) {
			try {
				for (int i = 0; i < allProducts.size(); i++) {
					// here in item product Id the GUId, the code is the real product
					AuctionItem it = (AuctionItem)allProducts.get(i);
					String ppId = StringUtil.ExtendNumber(it.getProductCode(),
												StringUtil.BAPI_MATERIAL_LENGTH);
					TechKey techKey = new TechKey(ppId);
															
					ItemSalesDoc item = (ItemSalesDoc)list.getItemData(techKey); 
					if(item == null) { // a new added item
						item = new ItemSalesDoc();					
						// here in item product Id the GUId, the code is the real product
	
						item.setProduct(ppId);
						item.setProductId(new TechKey(ppId));
						item.setQuantity(new Double(it.getQuantity()).toString());
						//set the data					
						list.add(item);
					}else{
						String quantity = item.getQuantity();
						item.setQuantity(new Double(
									it.getQuantity()).toString());
						if (logger.beInfo()){
							logger.infoT(tracer, "In the AuctionQuotationChange " +
										" the new quantity is " + it.getQuantity());
						}
					}
				}
			} catch (Exception ex) {    // TO DO Exception
				logger.errorT(tracer, "Error Product list error");
				throw new CommunicationException("In setting Item exception");
			}

		} else {
			logger.warning(tracer, "No ptoducts are avaliable for this auction ");
		}
		
		removeUpdatedItem(list, allProducts);
	}


	/**
	 * Used to remove the item in the Item but not in the allProducts
	 * @param 		 ItemList
	 * @param        List allProducts; get from the AuctionObject
	 */
	private void removeUpdatedItem(ItemList list, List allProducts) 
									throws CommunicationException{
		if (list != null) {
			try {
				Iterator iter = list.iterator();
				while (iter.hasNext()) {
					// here in item product Id the GUId, the code is the real product
					ItemSalesDoc item = (ItemSalesDoc)iter.next();
					String productId = item.getProduct();
					boolean containsItem = false;
					if((allProducts != null) && (productId !=null)){
						for (int j = 0; j <allProducts.size(); j++){
							AuctionItem it = (AuctionItem)allProducts.get(j);							
							String ppId = StringUtil.ExtendNumber(it.getProductCode(),
														StringUtil.BAPI_MATERIAL_LENGTH);
							if(productId.equalsIgnoreCase(ppId)){
								containsItem = true;
								break;
							}
						}
						if (!containsItem){
							list.remove(new TechKey(productId));						
						}
					}
				}
			} catch (Exception ex) {    // TO DO Exception
				logger.errorT(tracer, "Error Product list error");
				throw new CommunicationException("In updating Item exception");
			}

		} else {
			logger.warning(tracer, "No products are avaliable for updating ");
		}
		
	}

	/**
	 * from the auction we can get the all of the products related to 
	 * this auction
	 * @param, the auction contains all the products for auction
	 * @return a a collection of AuctionItems
	 */
	private List getProductList(Auction auction) {
		//
		List items = null;
		if (auction != null) {
			items = auction.getItems();
		} else {
			logger.warning(tracer, "In AuctionQuotationCreate the auction is null");
		}
		if(logger.beInfo()){
			if (items != null) {
				logger.infoT(tracer, 
					"In OrderEventListenerIMpl the items size is " + items.size());
			} else {
				logger.infoT(tracer, "In OrderEventListenerIMpl the items size is null");
			}
		}
		return items;
	}
	
}
