/*
 * Created on Sep 18, 2003
 *
 * SAP Labs LLC, the eBay auction project, also is known as Selling via eBay. 
 * Selling via eBay application provides SAP R/3 (and in the future CRM) 
 * customers to auction and sell inventory/products onto an online marketplace 
 * eBay. The application will provide the ability to Create, Schedule, Publish, 
 * Monitor and Close Auction Listings onto eBay and handle all the relevant 
 * Backend processing for Customers and Orders. 

 */
package com.sap.isa.auction.businessobject.order;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.List;

import com.sap.isa.auction.backend.exception.BackendUserException;
import com.sap.isa.auction.backend.exception.CommunicationBackendUserException;
import com.sap.isa.auction.backend.exception.SAPBackendException;
import com.sap.isa.auction.bean.Auction;
import com.sap.isa.auction.bean.AuctionItem;
import com.sap.isa.auction.bean.SalesArea;
import com.sap.isa.auction.bean.b2x.PrivateAuction;
import com.sap.isa.auction.businessobject.InvalidAttributeUserException;
import com.sap.isa.auction.businessobject.businesspartner.AuctionPartnerList;
import com.sap.isa.auction.exception.UserException;
import com.sap.isa.auction.exception.i18n.ErrorMessageBundle;
import com.sap.isa.auction.exception.i18n.I18nErrorMessage;
import com.sap.isa.auction.logging.CategoryProvider;
import com.sap.isa.auction.logging.LogHelper;
import com.sap.isa.auction.logging.TraceHelper;
import com.sap.isa.auction.util.StringUtil;
import com.sap.isa.businessobject.CommunicationException;
import com.sap.isa.businessobject.ShipTo;
import com.sap.isa.businessobject.Shop;
import com.sap.isa.businessobject.header.HeaderSalesDocument;
import com.sap.isa.businessobject.item.ItemList;
import com.sap.isa.businessobject.item.ItemSalesDoc;
import com.sap.isa.businessobject.order.PartnerList;
import com.sap.isa.businessobject.order.PartnerListEntry;
import com.sap.isa.core.TechKey;
import com.sap.tc.logging.Category;
import com.sap.tc.logging.Location;

/**
 * This class is used to create a quotation from an auction 
 */
public class AuctionQuotationCreate extends AuctionQuotationDecoratorBase {

	private static final Category logger = CategoryProvider.getBOLCategory();
	private static final Location tracer = Location.getLocation(AuctionQuotationCreate.class.getName());

	//// Here should be the dummy customer ID or the 
	//// One Time customer ID
	private String soldToId;
	private static final ErrorMessageBundle bundle = ErrorMessageBundle.getInstance();
	/**
	 * Creates a new instance for the given <code>Quotation</code> object.
	 *
	 * @param order Quotation to decorate
	 */
	public AuctionQuotationCreate(AuctionQuotation quotation) {
		super(quotation);
	}

	/**
	 * Copies the Auction's properties to the quotation object.
	 *
	 * @param baskt Basket which is used as source
	 */
	public void createFromAuction(Auction auction, Shop shop)
		throws CommunicationException {
		final String METHOD_NAME = "createFromAuction";
		TraceHelper.entering(tracer, METHOD_NAME); 
		// copy of auction properties, let us get the header first, the sales
		// document the header and items have been instantiated
		if(quotation != null)
			quotation.clearData();
		checkWebShop(shop, METHOD_NAME);
		setHeader(auction, shop);
		// set the responsible employee
		quotation.setReponsibleEmployee(auction.getSellerId());
		// set the responsible sales organisation
		quotation.setRespSalesOrg(shop.getResponsibleSalesOrganisation());
		// set the possible manual price condition for the starting price
		String conditionType = shop.getAuctionPriceType();
		if ((conditionType == null) || 
				(conditionType.trim().length()<=1)){
			I18nErrorMessage msg =
				bundle.newI18nErrorMessage(
				InvalidAttributeUserException.PRICE_CONDITION_TYPE_IS_NULL);
			LogHelper.logWarning(logger, tracer, msg.toString(), null);					
		}else{
			quotation.setTotalManualPriceCondition(conditionType);
		}
		// set theitem level
		setItems(auction);
		try {
			checkSalesTransactionFields();
			quotation.save();
		}catch (CommunicationBackendUserException ex) {
			LogHelper.logError(logger, tracer, 
			"In AuctionQuotationCreation "
				+ "CommunicationBackendUserException during the creation of "
				+ "the quotation", ex);
			throw ex;
		}catch (CommunicationException ex) {
			LogHelper.logError(logger, tracer, 
			"In AuctionQuotationCreation "
				+ "CommunicationException during the creation of "
				+ "the quotation", ex);
			TraceHelper.throwing(tracer,METHOD_NAME,ex.getMessage(), ex);		
			throw ex;
		} catch (Exception ex) {		
			ErrorMessageBundle bundle = ErrorMessageBundle.getInstance();
			I18nErrorMessage msg =
				bundle.newI18nErrorMessage(SAPBackendException.QUOTATION_FAILED);
			SAPBackendException exe = new SAPBackendException(msg);			
			LogHelper.logError(logger,tracer, ex.getMessage(), ex);
			TraceHelper.throwing(tracer,METHOD_NAME,ex.getMessage(),ex);		
			throw exe;
		}
		TraceHelper.exiting(tracer, METHOD_NAME); 

	}

	/**
	 * In this method we have two mode, simulation mode or real mode, if it
	 * is simulation mode, then the quotation will not be created in the
	 * backend system, only return some simulation messages
	 * @param auction
	 * @param simulation
	 * @throws CommunicationException
	 */
	public void createFromAuction(
		Auction auction,
		Shop shop,
		boolean simulation)
		throws SAPBackendException {
		final String METHOD_NAME = "createFromAuction";
		TraceHelper.entering(tracer, METHOD_NAME); 
		if(quotation != null)
			quotation.clearData();
		// copy of auction properties, let us get the header first, the sales
		// document the header and items have been instantiated
		checkWebShop(shop, METHOD_NAME);
		setHeader(auction, shop);
		// set the responsible employee
		quotation.setReponsibleEmployee(auction.getSellerId());
		String conditionType = shop.getAuctionPriceType();
		if ((conditionType == null) || 
				(conditionType.trim().length()<=1)){
			I18nErrorMessage msg =
				bundle.newI18nErrorMessage(
					InvalidAttributeUserException.PRICE_CONDITION_TYPE_IS_NULL);	
			LogHelper.logWarning(logger,tracer, msg.toString(), null);					
		}else{
			quotation.setTotalManualPriceCondition(conditionType);
		}
		// set theitem level
		setItems(auction);

		try {
			checkSalesTransactionFields();
			quotation.simulate();
		} catch (Exception ex) {		
			ErrorMessageBundle bundle = ErrorMessageBundle.getInstance();
			I18nErrorMessage msg =
				bundle.newI18nErrorMessage(SAPBackendException.QUOTATION_FAILED);
			SAPBackendException exe = new SAPBackendException(msg);			
			LogHelper.logError(logger,tracer, ex.getMessage(), ex);
			TraceHelper.throwing(tracer,METHOD_NAME,ex.getMessage(),ex);		
			throw exe;
		}
		TraceHelper.exiting(tracer, METHOD_NAME); 
	}


	/**
	 * Set the header information for the quotation from an auction
	 * @param the auction, which is in the context
	 */
	private void setHeader(Auction auction, Shop shop) {
		final String METHOD_NAME = "setHeader";
		TraceHelper.entering(tracer, METHOD_NAME); 	
		SalesArea salesArea = (SalesArea) auction.getSalesArea();
		HeaderSalesDocument header = quotation.getHeader();
		setPartnerList(header);

		if (salesArea != null && salesArea.getSalesOrganization() != null) {
			setHeaderSalesArea(header, salesArea);
		} else {
			salesArea = new SalesArea();
			salesArea.setSalesOrganization(shop.getSalesOrganisation());
			salesArea.setDistributionChannel(shop.getDistributionChannel());
			salesArea.setDivision(shop.getDivision());
			auction.setSalesArea(salesArea);
			setHeaderSalesArea(header, salesArea);
			logger.warning(tracer, 
				"Sales Area is not defined in Auction");
		}
		//Here Should use the title or name
		header.setDescription(
			"Quotation:" + auction.getAuctionTitle());
		header.setDocumentTypeQuotation();
		
		quotation.setPurchaseNumber(auction.getAuctionId());
		header.setPurchaseOrderExt(auction.getAuctionId());
		LogHelper.logInfo(logger, tracer, "Auction ID is " + 
							auction.getAuctionId(), null);
		String quoteType = shop.getAuctionQuoteType();
		if ((quoteType == null) ||
			(quoteType.trim().length()<=1)) {
			I18nErrorMessage msg =
					bundle.newI18nErrorMessage(
						InvalidAttributeUserException.QUOTE_PROCESS_TYPE_IS_NULL);
			InvalidAttributeUserException ex = new 
										InvalidAttributeUserException(msg);	
			logger.errorT(tracer,
				"The Quotation Document type is Null");
			tracer.throwing(logger, METHOD_NAME, ex);
			throw ex;	
		} else {	
			header.setDocumentType(quoteType);
		}	
		header.setCurrency(shop.getCurrency());
		header.setStatusOpen();		
		try {
			Timestamp date = auction.getLastModifiedDate();
			if (date == null)
				date = auction.getCreateDate();
			if (date != null) {
				header.setCreatedAt(date.toString());
			} else {
				LogHelper.logInfo(logger,tracer,
					"In AuctionQuotationCreate the date is null", null);
			}
			header.setValidTo(TransactionDataConstants.QUOTATION_VALID_TO);
			BigDecimal price = auction.getReservePrice();
			if (price == null) {
				price = auction.getStartPrice();
			}
			if (price != null) {
				header.setGrossValue(price.toString());
			} else {
				LogHelper.logInfo(logger,tracer,
                    "In AuctionQuotationCreate the price is null", null);
			}

		} catch (NullPointerException ex) {
			// do not endanger the process flow.
			LogHelper.logWarning(logger,tracer,"Some attributes in " +
				"Order Header have been not set, nullpointer error", ex);			
		} catch (Exception ex) {
			LogHelper.logWarning(logger,tracer,"Some attributes in " +
				"Order Header have been not set", ex);
		}
		TraceHelper.exiting(tracer, METHOD_NAME); 	
	}

	/**
	 * Set the partner list data
	 * @param header
	 * 
	 */
	private void setPartnerList(HeaderSalesDocument header) {
		final String METHOD_NAME = "setPartnerList";
		TraceHelper.entering(tracer, METHOD_NAME);	
		AuctionPartnerList partnerList = new AuctionPartnerList();
		// set the sales employee and
		// no matter of what copy over
		if (header.getPartnerList() != null) {
			PartnerList oldPartnerList = header.getPartnerList();
			if (oldPartnerList.getSoldTo() != null) {
				partnerList.setSoldTo(oldPartnerList.getSoldTo());
			} else {
				PartnerListEntry entrySoldTo = new PartnerListEntry();
				if (soldToId != null) {
					entrySoldTo.setPartnerId(soldToId);
					entrySoldTo.setPartnerTechKey(new TechKey(soldToId));
				}
				partnerList.setSoldTo(entrySoldTo);
			}
		}
		header.setPartnerList(partnerList);
		// by default set the shipTo as the SoldTo, but you can overwrite
		if (header.getShipTo() == null) {
			ShipTo shipTo = new ShipTo();
			shipTo.setId(soldToId);
			shipTo.setTechKey(new TechKey(soldToId));
			header.setShipTo(shipTo);
		}
		TraceHelper.exiting(tracer, METHOD_NAME);			
	}

	/**
	 * The the sales area to the header level.
	 * @param the sales area from auction definition
	 * @
	 */
	private void setHeaderSalesArea(
		HeaderSalesDocument header,
		SalesArea salesArea) {
		header.setSalesOrg(salesArea.getSalesOrganization());
		header.setDisChannel(salesArea.getDistributionChannel());
		header.setDivision(salesArea.getDivision());
	}

	/**
	 * Set the item level data
	 * @param the auctio, used to create a quotation
	 */
	private void setItems(Auction auction){
		//Get the items from the quotation
		ItemList items = quotation.getItems();
		// get the items based on ebayItem id
		List list = getProductList(auction);
		//now build up the itemlist
		// build up the item list
		setItemList(items, list, auction);

	}

	/**
	 * Set the items for a sales order, all the product information
	 * @param: list the ItemList for a sales order
	 * @param: all the products retrieve from an auction
	 */
	private void setItemList(ItemList list, List allProducts,
							 Auction auction)
		throws UserException {
		final String METHOD_NAME = "setItemList";
		TraceHelper.entering(tracer, METHOD_NAME);		
		if (allProducts != null) {
			try {
				for (int i = 0; i < allProducts.size(); i++) {
					ItemSalesDoc item = new ItemSalesDoc();
					AuctionItem it = (AuctionItem) allProducts.get(i);
					// here in item product Id the GUId, the code is the real product
					String ppId =
						StringUtil.ExtendNumber(
							it.getProductCode(),
							StringUtil.BAPI_MATERIAL_LENGTH);
					item.setProduct(ppId);
					item.setProductId(new TechKey(ppId));
					//no matter it is broken lot or whole lot
					// always do thye multiplication
					LogHelper.logInfo(logger, tracer, "In setItemList the " +
						" item Quantity is " + it.getQuantity(), null);
					double totalNumber = it.getQuantity()*((PrivateAuction)auction).getQuantity();
					LogHelper.logInfo(logger, tracer, "In setItemList the " +						" total Quantity is " + totalNumber, null);
					item.setQuantity(new Double(totalNumber).toString());
					try{
						item.setUnit(it.getUOM());
					}catch(Exception ex){
						LogHelper.logError(logger,tracer, ex.getMessage(), ex);
					}
					//item.setTechKey(new TechKey(ppId));
					//set the data
					list.add(item);
				}
			} catch (Exception ex) { 
				ErrorMessageBundle bundle = ErrorMessageBundle.getInstance();
				I18nErrorMessage msg =
					bundle.newI18nErrorMessage(UserException.ILLEGAL_ARGUMENT);
				UserException exe = new UserException(msg);			
				LogHelper.logError(logger,tracer, ex.getMessage(), ex);
				TraceHelper.throwing(tracer,METHOD_NAME,ex.getMessage(),ex);
				throw exe;
			}

		} else {
			logger.warning(tracer, "No ptoducts are avaliable for this auction ");
		}
		TraceHelper.exiting(tracer, METHOD_NAME);	
	}

	/**
	 * from the auction we can get the all of the products related to 
	 * this auction
	 * @param, the auction contains all the products for auction
	 * @return a a collection of AuctionItems
	 */
	private List getProductList(Auction auction) {
		List items = null;
		if (auction != null) {
			items = auction.getItems();
		} else {
			logger.infoT(tracer, "In AuctionQuotationCreate the auction is null");
		}
		if (logger.beInfo()) {
			if (items != null) {
				logger.infoT(tracer, 
					"In OrderEventListenerIMpl the items size is "
						+ items.size());
			} else {
				logger.infoT(tracer, "In OrderEventListenerIMpl the items size is null");
			}
		}
		return items;
	}


	/**
	 * Convience method for setting the one time customer 
	 * @return
	 */
	public String getSoldToId() {
		return soldToId;
	}

	/**
	 * @param string
	 */
	public void setSoldToId(String string) {
		soldToId = string;
	}

	/**
	 * This method checks the mandatory fields in the quotation object
	 * basically the SOLDTo, the sales area and business transaction type,
	 * if any of them has not been defined, then throw communication exception
	 * @param quotation
	 */
	private void checkSalesTransactionFields() throws InvalidAttributeUserException {
		ErrorMessageBundle bundle = ErrorMessageBundle.getInstance();
		final String METHOD_NAME = "checkSalesTransactionFields";
		TraceHelper.entering(tracer, METHOD_NAME);		
		if (quotation == null) {
			I18nErrorMessage msg =
				bundle.newI18nErrorMessage(
					InvalidAttributeUserException.QUOTATION_IS_NULL);
			InvalidAttributeUserException ex = new 
									InvalidAttributeUserException(msg);			
			logger.errorT(tracer,
				"During the creation of quotation from Auction, the quotation is NULL");
			tracer.throwing(logger, METHOD_NAME, ex);
			throw ex;			
		} else {
			HeaderSalesDocument header = quotation.getHeader();
			if (header.getDocumentType() == null) {
				I18nErrorMessage msg =
						bundle.newI18nErrorMessage(
							InvalidAttributeUserException.SALESDOC_TYPE_IS_NULL);
				InvalidAttributeUserException ex = new 
											InvalidAttributeUserException(msg);	
				
				logger.errorT(tracer,
					"During the creation of quotation, the transaction"
						+ " type is not defined");
				tracer.throwing(logger, METHOD_NAME, ex);
				throw ex;	
			}
			if (header.getSalesOrg() == null) {
				I18nErrorMessage msg =
						bundle.newI18nErrorMessage(
							InvalidAttributeUserException.SALESORG_IS_NULL);
				InvalidAttributeUserException ex = new 
											InvalidAttributeUserException(msg);	
				logger.errorT(tracer,
					"During the creation of quotation, sales org is "
						+ " not defined.");
				tracer.throwing(logger, METHOD_NAME, ex);
				throw ex;	
			}
			if (header.getDisChannel() == null) {
				I18nErrorMessage msg =
						bundle.newI18nErrorMessage(
							InvalidAttributeUserException.DISCHANNEL_IS_NULL);
				InvalidAttributeUserException ex = new 
											InvalidAttributeUserException(msg);	
				logger.errorT(tracer,
					"During the creation of quotation, distribution "
						+ "chanel is not defined");
				tracer.throwing(logger, METHOD_NAME, ex);
				throw ex;	
			}

		}
		TraceHelper.exiting(tracer, METHOD_NAME);	
	}
	
	private void checkWebShop(Shop shop, String methodName){
		ErrorMessageBundle bundle = ErrorMessageBundle.getInstance();
		if (shop == null){
			I18nErrorMessage msg =
				bundle.newI18nErrorMessage(
					InvalidAttributeUserException.WEBSHOP_IS_NULL);
			InvalidAttributeUserException ex = new 
									InvalidAttributeUserException(msg);			
			logger.errorT(tracer,
				"During the creation of quotation from Auction, the passing WebShop is NULL");
			tracer.throwing(logger, methodName, ex);
			throw ex;			
//		}else if (!(shop.isEAuctionUsed())){
//			I18nErrorMessage msg =
//				bundle.newI18nErrorMessage(
//					InvalidAttributeUserException.WEBSHOP_INVALID);
//			InvalidAttributeUserException ex = new 
//									InvalidAttributeUserException(msg);			
//			logger.errorT(tracer,
//				"During the creation of quotation from Auction, the passing WebShop is not valid");
//			tracer.throwing(logger, methodName, ex);
//			throw ex;
			
		}
		
		
	}
}
