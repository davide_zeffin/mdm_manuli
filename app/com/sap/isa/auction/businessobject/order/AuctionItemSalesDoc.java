/*
 * Created on Nov 24, 2003
 *
 * To change the template for this generated file go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
package com.sap.isa.auction.businessobject.order;

import com.sap.isa.auction.backend.boi.order.AuctionItemData;
import com.sap.isa.businessobject.item.ItemSalesDoc;
import com.sap.isa.core.TechKey;


public class AuctionItemSalesDoc
	extends ItemSalesDoc
	implements AuctionItemData {
		

	private String plant = null;
	private String storage = null;
	private String winningPrice = null;
	/** Price Condition Used to set manual price at the itme level**/
	private String priceCondition = null;
	
	/**
	 * Default constructor for the Item
	 */
	public AuctionItemSalesDoc() {
		super();
	}


	/**
	 * Constructor for the object directly taking the technical
	 * key for the object
	 * if the technical key is empty an unique handle will be generate
	 * in the super class
	 * @param techKey Technical key for the object
	 */
	public AuctionItemSalesDoc(TechKey techKey) {
		super(techKey);
	}


	/* (non-Javadoc)
	 * @see com.sap.isa.auction.backend.boi.order.AuctionItemData#getPlant()
	 */
	public String getPlant() {
		return plant;
	}

	/* (non-Javadoc)
	 * @see com.sap.isa.auction.backend.boi.order.AuctionItemData#setPlant(java.lang.String)
	 */
	public void setPlant(String plant) {
		this.plant = plant;
	}
	/* (non-Javadoc)
	 * @see com.sapmarkets.isa.auction.backend.boi.order.AuctionItemData#getStorageLocation()
	 */
	public String getStorageLocation() {
		return storage;
	}

	/* (non-Javadoc)
	 * @see com.sapmarkets.isa.auction.backend.boi.order.AuctionItemData#setStorageLocation(java.lang.String)
	 */
	public void setStorageLocation(String sl) {
		this.storage = sl;
	}

	/**
	 * Set the manual price condition
	 * @param condition
	 */
	public void setManualPriceCondition(String condition){
		priceCondition = condition;
	}
	
	/**
	 * Set for the auction winning price
	 * @param price
	 */
	public void setManualPrice(String price){
		winningPrice = price;
	}
	
	/**
	 * Get the manual price consdition, which will overwrite the
	 * backend setting at the item level 
	 * @return PriceCondition 
	 */
	public String getManualPriceCondition(){
		return priceCondition;
	}
	/**
	 * Get the manual price for winning auction.
	 * @return: used for auction winning price
	 */
	public String getManualPrice(){
		return winningPrice;
	}

}
