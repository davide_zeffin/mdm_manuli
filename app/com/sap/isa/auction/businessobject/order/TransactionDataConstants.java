/*
 * Created on Sep 22, 2003
 *
 * SAP Labs LLC, the eBay auction project, also is known as Selling via eBay. 
 * Selling via eBay application provides SAP R/3 (and in the future CRM) 
 * customers to auction and sell inventory/products onto an online marketplace 
 * eBay. The application will provide the ability to Create, Schedule, Publish, 
 * Monitor and Close Auction Listings onto eBay and handle all the relevant 
 * Backend processing for Customers and Orders. 

 */
package com.sap.isa.auction.businessobject.order;

/**
 * This interface keeps all the constants in the transactional data
 */
public interface TransactionDataConstants {
	/**
	 * Used for the valid quotation valid to date
	 */
	public final static String QUOTATION_VALID_TO = "99990909";
	/**
	 * Used for the default currency
	 */ 
	public static final String DEFAULT_CURRENCY = "USD";
	
	/**
	 * Used in the backend connection for the Auction Quotation Object
	 */
	public static final String OBJECT_AUCTION_QUOTATION = "AuctionQuotation";
	
	/**
	 * 
	 * Used in the backend connection for the Auction Order Object
	 */
	public static final String OBJECT_AUCTION_ORDER = "AuctionOrder";	
	
}
