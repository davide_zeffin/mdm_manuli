/*
 * Title:        eBay Integration Project
 * Description:	 * SAP Labs LLC, the eBay auction project, also is known as Selling via eBay. 
 * Selling via eBay application provides SAP R/3 (and in the future CRM) 
 * customers to auction and sell inventory/products onto an online marketplace 
 * eBay. The application will provide the ability to Create, Schedule, Publish, 
 * Monitor and Close Auction Listings onto eBay and handle all the relevant 
 * Backend processing for Customers and Orders. 

 * Copyright:    Copyright (c) 2003
 * Company:      SAP Labs LLC
 * @author
 * @version 1.0
 */
package com.sap.isa.auction.businessobject.order;

import java.util.ArrayList;

import com.sap.isa.auction.backend.boi.order.AuctionDocumentListFilterData;
import com.sap.isa.businessobject.order.DocumentListFilter;

/**
 * @author i009657
 *
 * For auction order status purpose, this is used in auctionstatus 
 * search
 */
public class SellerDocumentListFilter
	extends DocumentListFilter
	implements AuctionDocumentListFilterData {

	private ArrayList orderIds = new ArrayList(5);
	private String salesEmployee;
	/**
	 * Constructor
	 */
	public SellerDocumentListFilter() {
		super();
	}

	/* 
	 * @param employee
	 */
	public void setResponsibleEmployee(String employee) {
		salesEmployee = employee;
	}

	/* 
	 * @return
	 */
	public String getResponsibleEmployee() {
		return salesEmployee;
	}

	/* 
	 * Setter for all the order IDs form our persistence
	 * @param orderIds
	 */
	public void setOrderIds(ArrayList orderIds) {
		this.orderIds = orderIds;
	}

	/* 
	 * @return all of the Order IDs
	 */
	public ArrayList getOrderIds() {
		return orderIds;
	}

}
