package com.sap.isa.auction.businessobject.order;

import com.sap.isa.auction.backend.boi.order.AuctionBasketData;
import com.sap.isa.auction.logging.CategoryProvider;
import com.sap.isa.backend.boi.isacore.ShopData;
import com.sap.isa.backend.boi.isacore.order.BasketBackend;
import com.sap.isa.backend.boi.isacore.order.BasketData;
import com.sap.isa.backend.boi.isacore.order.CopyMode;
import com.sap.isa.businessobject.BusinessObjectHelper;
import com.sap.isa.businessobject.CommunicationException;
import com.sap.isa.businessobject.Shop;
import com.sap.isa.businessobject.order.Basket;
import com.sap.isa.core.TechKey;
import com.sap.isa.core.eai.BackendException;
import com.sap.tc.logging.Category;
import com.sap.tc.logging.Location;

/**
 * The basket object is populated from auction related
 * information
 */
public class AuctionBasket extends Basket implements AuctionBasketData {

	/**
	 * The sales employee ID
	 */
	private String employee = null;

	private String totalPriceType = null;
	
	private String shippingPriceType = null;
	/**
	 * This attribute actually holds the ID from auction side for the published 
	 * list
	 */
	private String listingId = null;

	private static final Category logger = CategoryProvider.getBOLCategory();
	private static final Location tracer =
		Location.getLocation(AuctionQuotation.class.getName());

	/**
	 * @param, the sales employee ID, as in the partners section
	 */
	public void setReponsibleEmployee(String bpId) {
		employee = bpId;
	}

	/**
	 * @return the responsible employee copied from auction
	 */
	public String getResponsibleEmployee() {
		return employee;
	}

	/**
	 * This is the listing numebr from the auction side, the ID
	 * is retrieved usually from the auction site, it is used to
	 * retrieve all the corelated quotation and orders.
	 */
	public void setPurchaseNumber(String purchaseNumber) {
		listingId = purchaseNumber;
		getHeader().setPurchaseOrderExt(purchaseNumber);
	}

	/**
	 * Return the auction listing ID
	 * @return the auctiion listing ID, used for tracking order history
	 */
	public String getPurchaseNumber() {
		if (listingId == null) {
			listingId = this.getHeader().getPurchaseOrderExt();
		}
		return listingId;
	}
	
	/* (non-Javadoc)
	 * @see com.sap.isa.auction.backend.boi.order.AuctionSalesDocumentData#getTotalManualPriceCondition()
	 */
	public String getTotalManualPriceCondition() {
		return this.totalPriceType;
	}

	/* (non-Javadoc)
	 * @see com.sap.isa.auction.backend.boi.order.AuctionSalesDocumentData#setTotalManualPriceCondition(java.lang.String)
	 */
	public void setTotalManualPriceCondition(String priceType) {
		this.totalPriceType = priceType;		
	}
	
	/**
	 * Initializes a basket from an auction predecessor document.
	 *
	 * @param predecessorKey <code>TechKey</code> of a predecessor document
	 * @param processType process type that shall be used for the resulting
	 * order. If initial (""), the process type specified in the shop will be
	 * used.
	 * @param soltoKey <code>TechKey</code> of the sold to for whom the order
	 * shall be created
	 * @param shop the shop the action takes place in
	 */
	public void initWithAuctionRef(
			TechKey predecessorKey,
			String processType,
			TechKey solToKey,
			Shop shop
			) throws CommunicationException {
		clearData();

		// write some debugging info
		if (log.isDebugEnabled()) {
			log.debug("initWithAuctionRef(): predecessorKey = " + predecessorKey
					+ ", processType = " + processType
					+ ", solToKey = " + solToKey
					+ ", shop = " + shop);
		}


		if (!alreadyInitialized) {
			try {
				// create Basket in Backend
				((BasketBackend)getBackendService()).createWithReferenceInBackend(
						predecessorKey,
						CopyMode.TRANSFER_AND_UPDATE,
						processType,
						solToKey,
						(ShopData)shop,
						(BasketData)this
						);
				alreadyInitialized = true;
				isDirty = true;
				header.setDirty(true);
			}
			catch (BackendException ex) {
				log.error("Error occures in populate auction basket");
				BusinessObjectHelper.splitException(ex);
			}
		}
	}

	/* (non-Javadoc)
	 * @see com.sap.isa.auction.backend.boi.order.AuctionBasketData#getShippingManualPriceCondition()
	 */
	public String getShippingManualPriceCondition() {
		return shippingPriceType;
	}

	/* (non-Javadoc)
	 * @see com.sap.isa.auction.backend.boi.order.AuctionBasketData#setShippingManualPriceCondition(java.lang.String)
	 */
	public void setShippingManualPriceCondition(String priceType) {
		shippingPriceType = priceType;
		
	}


}
