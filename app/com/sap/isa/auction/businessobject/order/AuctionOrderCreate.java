/*
 * Created on Sep 18, 2003
 *

 */
package com.sap.isa.auction.businessobject.order;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.List;

import com.sap.isa.auction.backend.exception.CommunicationBackendUserException;
import com.sap.isa.auction.backend.exception.SAPBackendException;
import com.sap.isa.auction.backend.exception.SAPBackendUserException;
import com.sap.isa.auction.bean.Auction;
import com.sap.isa.auction.bean.AuctionItem;
import com.sap.isa.auction.bean.SalesArea;
import com.sap.isa.auction.bean.Winner;
import com.sap.isa.auction.bean.b2x.PrivateAuction;
import com.sap.isa.auction.bean.b2x.PrivateAuctionWinner;
import com.sap.isa.auction.businessobject.InvalidAttributeUserException;
import com.sap.isa.auction.businessobject.businesspartner.AuctionPartnerList;
import com.sap.isa.auction.exception.i18n.ErrorMessageBundle;
import com.sap.isa.auction.exception.i18n.I18nErrorMessage;
import com.sap.isa.auction.logging.CategoryProvider;
import com.sap.isa.auction.logging.LogHelper;
import com.sap.isa.auction.logging.TraceHelper;
import com.sap.isa.auction.util.StringUtil;
import com.sap.isa.businessobject.Address;
import com.sap.isa.businessobject.ShipTo;
import com.sap.isa.businessobject.Shop;
import com.sap.isa.businessobject.header.HeaderSalesDocument;
import com.sap.isa.businessobject.item.ItemList;
import com.sap.isa.businessobject.item.ItemSalesDoc;
import com.sap.isa.businessobject.order.OrderCreate;
import com.sap.isa.businessobject.order.PartnerListEntry;
import com.sap.isa.core.TechKey;
import com.sap.tc.logging.Category;
import com.sap.tc.logging.Location;

/**
 * @
 */
public class AuctionOrderCreate extends OrderCreate {
	private static final Category logger = CategoryProvider.getBOLCategory();
	private static final Location tracer = Location.getLocation(AuctionOrderCreate.class.getName());

	public static final String DEFAULT_CURRENCY = "USD";
	private String soldToId;
	private String documentType;
	private static final ErrorMessageBundle bundle = ErrorMessageBundle.getInstance();
	public AuctionOrderCreate(AuctionOrder order) {
		super(order);
	}
	
	/**
	 * 
	 * @return the order object
	 */
	public AuctionOrder getOrder() {
		return (AuctionOrder) order;
	}

	/**
	 * Create the order from auction and winner information, basically copy all
	 * the quotation information, then using the winner to set the price information
	 * @param auction, the auction information, from where we have the quoet ID
	 * @param winner, from where we have the winning price
	 */
	public void createAuctionOrder(
		Auction auction,
		Winner winner,
		Shop shop)
		throws SAPBackendException {
		final String METHOD_NAME = "createAuctionOrder";
		TraceHelper.entering(tracer, METHOD_NAME); 
		/// first upload the the quotation ID from auction
		String quoteId = auction.getQuotationId();
		String priceType = shop.getAuctionPriceType();
		if (quoteId != null) {
			((AuctionOrder) order).setPredecessorDocumentId(quoteId);
		} else {
			I18nErrorMessage msg =
					bundle.newI18nErrorMessage(
						InvalidAttributeUserException.QUOTATION_ID_IS_NULL);
			InvalidAttributeUserException ex = new 
										InvalidAttributeUserException(msg);	
			logger.errorT(tracer,
				"In AuctionOrderCreate Object thequotation Id is not passed");
			tracer.throwing(logger, METHOD_NAME, ex);
			throw ex;	
		}
		/// The soldTo party is used at both header and item level
		String soldTo = null;
		Address address = null;
		if (getSoldToId() != null){
			soldTo = getSoldToId();
		}else {
			soldTo = getSoldTo(winner);
		}
		//		getBackendSetting();
		if (soldTo == null) {
			I18nErrorMessage msg =
					bundle.newI18nErrorMessage(
						InvalidAttributeUserException.SOLDTO_PARTY_IS_NULL);
			InvalidAttributeUserException ex = new 
										InvalidAttributeUserException(msg);	
			logger.errorT(tracer,
				"The sold to partner is Null");
			tracer.throwing(logger, METHOD_NAME, ex);
			throw ex;	
		} else {
			setSoldToId(soldTo);
		}
		((AuctionOrder) order).setReponsibleEmployee(auction.getSellerId());
		((AuctionOrder) order).setTotalManualPriceCondition(priceType);
		LogHelper.logInfo(logger,tracer, "prioceType is " + priceType, null);

		setHeader(auction, winner, shop);
		List items = getProductList(auction);
		//now build up the itemlist
		// build up the item list
		ItemList list = new ItemList();
		setItemList(list, items, auction.getCurrencyCode(), winner, 
					auction);
		if (address == null)
			address = new Address();
		/// for testing remove late 
		try {
			order.setItems(list);
		} catch (Exception e) {
			logger.errorT(tracer, "In Auction Order Creation ");
			// this exception is not bubbled up, since we create the
			// order form the quotation
		}
		// 
		try {
			getOrder().createFromQuotation(
				quoteId,
				(AuctionOrder) order,
				address);
		} catch (CommunicationBackendUserException ex) {
			I18nErrorMessage msg =
				bundle.newI18nErrorMessage(SAPBackendException.ORDER_FAILED);
			SAPBackendUserException exe = new SAPBackendUserException(ex);			
			LogHelper.logError(logger,tracer, ex.getMessage(), ex);
			TraceHelper.throwing(tracer,METHOD_NAME,ex.getMessage(),ex);		
			throw exe;
		} catch (Exception ex) {
			I18nErrorMessage msg =
				bundle.newI18nErrorMessage(SAPBackendException.ORDER_FAILED);
			SAPBackendException exe = new SAPBackendException(msg);			
			LogHelper.logError(logger,tracer, ex.getMessage(), ex);
			TraceHelper.throwing(tracer,METHOD_NAME,ex.getMessage(),ex);		
			throw exe;			
		}

		//here let us set the the order id back to winner
		if (order.getHeader() != null) {
			// here we set the order id as the Guid not the sales docuemnt id
			//winner.setOrderId(order.getHeaderData().getSalesDocNumber());
			//winner.setOrderId(getOrder().getTechKey().getIdAsString());
			String orderTechId = 
					order.getHeaderData().getTechKey().getIdAsString();
			String orderId = order.getHeaderData().getSalesDocNumber();
			if(winner instanceof PrivateAuctionWinner)
			((PrivateAuctionWinner)winner).setOrderGuid(
				new TechKey(orderTechId));
			winner.setOrderId(orderId);
			LogHelper.logInfo(logger, tracer, "The order GUID after creation Is " + 
						orderTechId, null);	
			LogHelper.logInfo(logger, tracer, "The order ID after creation Is " + 
						orderId, null);		
		} else {
			logger.warning(tracer,
				"In the winner the order Id is not set, order header is null");
		}
		TraceHelper.exiting(tracer, METHOD_NAME); 

	}


	/**
	 * Set the header information about a sales document
	 */
	private void setHeader(Auction auction, 
							Winner winner,
							Shop shop) {
		final String METHOD_NAME = "setHeader";
		TraceHelper.entering(tracer, METHOD_NAME); 								
		HeaderSalesDocument header = order.getHeader();
		BigDecimal nPrice = winner.getBidAmount();
		if(logger.beInfo())
			logger.infoT(tracer, "setHeader: price = " + nPrice);
		//BigDecimal price = (nPrice == null ? "" : nPrice.toString());
		int quantity = -1;
		if(winner instanceof PrivateAuctionWinner)
		quantity = ((PrivateAuctionWinner)winner).getQuantity().intValue();
		BigDecimal price = nPrice.multiply(new BigDecimal(quantity));
		header.setGrossValue(price.toString());
		header.setNetValue(price.toString());
		header.setCurrency(auction.getCurrencyCode());	
		SalesArea salesArea = (SalesArea) auction.getSalesArea();

		setPartnerList(header, winner);

		if (salesArea != null) {
			setHeaderSalesArea(header, salesArea);
		} else {
			logger.warning(tracer,
				"Sales Area is not defined in Auction",
				new Exception("empty sales area data"));
		}
		//TO DO: header.setCurrency();
		header.setDescription(
			"Sales Order for: " + auction.getAuctionName());
		header.setDocumentTypeOrder();
		String orderType = shop.getAuctionOrderType();
		if ((orderType == null) ||
			(orderType.trim().length()<=1)) {
			I18nErrorMessage msg =
					bundle.newI18nErrorMessage(
						InvalidAttributeUserException.ORDER_PROCESS_TYPE_IS_NULL);
			InvalidAttributeUserException ex = new 
										InvalidAttributeUserException(msg);	
			logger.errorT(tracer,
				"The sales Order Document type is Null");
			tracer.throwing(logger, METHOD_NAME, ex);
			throw ex;	
		} else {		
			header.setDocumentType(shop.getAuctionOrderType());
		}
		header.setStatusOpen();
		///KIND of silly, we set the reserved price to the, in order to show the
		/// it in the overview screen of sales order
		try {
			Timestamp date = auction.getLastModifiedDate();
			if (date == null)
				date = auction.getCreateDate();
			if (date != null) {
				header.setCreatedAt(date.toString());
			} else {
				logger.warningT(tracer, "In AuctionOrderCreate the date is null");
			}
			header.setValidTo(TransactionDataConstants.QUOTATION_VALID_TO);
		} catch (NullPointerException ex) {
			logger.errorT(tracer, "Some attributes in Order have been not set");
		} catch (Exception ex){
			logger.errorT(tracer, "Unexpected exception will be ignored, " +
				 " exception is :" + ex.getMessage());
		}
	}

	/**
	 * Calculate the identifier for an order, it is based on auction id combined
	 * with buyer ID
	 * @param winner: The winner object
	 * @return: String which is unique that represents order
	 */
	private String getIdentifier(Winner winner) {
		String unique = null;
		if (winner != null) {
			unique = winner.getAuctionId() + winner.getBuyerId();
		}
		return unique;
	}

	/**
	 * from the auction we can get the all of the products related to this auction
	 */
	private List getProductList(Auction auction) {
		//
		List items = null;
		if (auction != null) {
			// here always get one part from the bundle, the total
			// quantity should be the auction item's quantity
			// mutiple the auction.quantity()
			items = auction.getItems();
		} else {
			logger.infoT(tracer, "In AuctionOrder Creation the auction is null");
		}
		if (items != null) {
			logger.infoT(tracer, 
				"In AuctionOrder Creation the items size is " + items.size());
		}
		return items;
	}

	/**
	 * Set the items for a sales order, all the product information
	 * @param: list the ItemList for a sales order
	 * @param: all the products retrieve from an auction
	 * @param: currency: the currency used in auction
	 */
	private void setItemList(ItemList list, List allProducts, String currency,
								Winner winner, Auction auction) {
		final String METHOD_NAME = "setItemList";
		TraceHelper.entering(tracer, METHOD_NAME); 	
		BigDecimal averagePricePerUnit = winner.getBidAmount();
		BigDecimal winnerNumberOfBundle = null;
		BigDecimal winnerTotalQuantity = null;
		if(winner instanceof PrivateAuctionWinner)
		winnerNumberOfBundle = ((PrivateAuctionWinner)winner).getQuantity();
		if(auction instanceof PrivateAuction)
		winnerTotalQuantity = winnerNumberOfBundle.
								multiply(new BigDecimal(
								((PrivateAuction)auction).getTotalQuantityOfOneBundle()));
		try{
			averagePricePerUnit = winner.getBidAmount().
				divide(winnerTotalQuantity, 
						10, BigDecimal.ROUND_HALF_UP);					
		}catch (Exception ex){
			logger.infoT(tracer, "in setItemList the calculation item " +
							"price failed with exception " + ex.getMessage());								
			LogHelper.logError(logger,tracer, ex.getMessage(), ex);
		}
		if (allProducts != null) {
			try {
				for (int i = 0; i < allProducts.size(); i++) {
					AuctionItem it = (AuctionItem) allProducts.get(i);
					ItemSalesDoc item = new ItemSalesDoc();
					// here in item product Id the GUId, the code is the real product
					String ppId =
						StringUtil.ExtendNumber(
							it.getProductCode(),
							StringUtil.BAPI_MATERIAL_LENGTH);
					item.setProduct(ppId);
				
					if (logger.beInfo()) {
						logger.infoT(tracer, 
							"In AuctionOrder Creation product is " + ppId);
					}
					item.setProductId(new TechKey(ppId));
					double eachItemQuantity = winnerNumberOfBundle.doubleValue()*it.getQuantity();
					item.setQuantity(new Double(eachItemQuantity).toString());
					item.setCurrency(currency);
					item.setNetValue(averagePricePerUnit.toString());
					logger.infoT(tracer, "In setItemList the price is " + 
											averagePricePerUnit.toString());		
					try{
						item.setUnit(it.getUOM());
					}catch(Exception ex){
						LogHelper.logError(logger,tracer, ex.getMessage(), ex);
					}
					//set the data
					list.add(item);
				}
			} catch (Exception ex) {
				//ErrorMessageBundle bundle = ErrorMessageBundle.getInstance();
				I18nErrorMessage msg =
					bundle.newI18nErrorMessage(SAPBackendException.ORDER_FAILED);
				SAPBackendException exe = new SAPBackendException(ex);			
				LogHelper.logError(logger,tracer, ex.getMessage(), ex);
				TraceHelper.throwing(tracer,METHOD_NAME,ex.getMessage(),ex);		
				throw exe;
			}
		} else {
			logger.warning(tracer, 
							"No products are avaliable for this auction ");
		}
		TraceHelper.exiting(tracer, METHOD_NAME); 	
	}



	/**
	 * get the CRM business Partner Id from winner object
	 */
	private String getSoldTo(Winner winner) {
		String soldTo = null;
		if (winner != null) {
			soldTo = winner.getBuyerId();
		}
		return soldTo;
	}

	/**
	 * set the currency to the header
	 */
	private void setHeaderCurrency(String currency) {
		try {
			if (currency != null) {
				order.getHeader().setCurrency(currency);
			}
		} catch (Exception ex) {
			logger.errorT(tracer, 
					"Errors In Auction Order Creation setting currency ");
		}
	}

	/**
	 * Set the partner list data
	 * @param header
	 * 
	 */
	private void setPartnerList(HeaderSalesDocument header, Winner winner) {
		AuctionPartnerList partnerList = new AuctionPartnerList();
		// set the soldTo
		if (soldToId != null) {
			PartnerListEntry entrySoldTo = new PartnerListEntry();
			LogHelper.logInfo(logger, tracer, "The SoldToId in the " +
												"AuctionOrderCreate is " + 
										soldToId, null);
			entrySoldTo.setPartnerId(soldToId);
			entrySoldTo.setPartnerTechKey(new TechKey(soldToId));
			partnerList.setSoldTo(entrySoldTo);
		} 
		// the contact person from the winner object
		if((winner.getBuyerId() != null) && 
							(winner.getBuyerId().trim().length()>0)){
			PartnerListEntry entryContactP = new PartnerListEntry();
			LogHelper.logInfo(logger, tracer, "The contact person Id is " + 
							winner.getBuyerId(), null);				
			entryContactP.setPartnerId(winner.getBuyerId());
			entryContactP.setPartnerTechKey(new TechKey(winner.getBuyerId()));
			partnerList.setContact(entryContactP);
		}
		header.setPartnerList(partnerList);
		// by default set the shipTo as the SoldTo, but you can overwrite
		if (header.getShipTo() == null
			|| header.getShipTo().getId() == null
			|| header.getShipTo().getId().length() <= 0) {
			ShipTo shipTo = new ShipTo();
			shipTo.setId(soldToId);
			shipTo.setTechKey(new TechKey(soldToId));
			header.setShipTo(shipTo);
		}
	}

	/**
	 * The the sales area to the header level.
	 * @param the sales area from auction definition
	 * @
	 */
	private void setHeaderSalesArea(
		HeaderSalesDocument header,
		SalesArea salesArea) {
		header.setSalesOrg(salesArea.getSalesOrganization());
		header.setDisChannel(salesArea.getDistributionChannel());
		header.setDivision(salesArea.getDivision());

	}
	/**
	 * @return
	 */
	public String getSoldToId() {
		return soldToId;
	}

	/**
	 * @param string
	 */
	public void setSoldToId(String string) {
		soldToId = string;
	}

}
