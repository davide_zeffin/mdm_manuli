/*
 * Created on Sep 18, 2003
 *
 * SAP Labs LLC, the eBay auction project, also is known as Selling via eBay. 
 * Selling via eBay application provides SAP R/3 (and in the future CRM) 
 * customers to auction and sell inventory/products onto an online marketplace 
 * eBay. The application will provide the ability to Create, Schedule, Publish, 
 * Monitor and Close Auction Listings onto eBay and handle all the relevant 
 * Backend processing for Customers and Orders. 

 */
package com.sap.isa.auction.businessobject.order;

import com.sap.isa.businessobject.DecoratorBase;

/**
 * @
 */
public class AuctionQuotationDecoratorBase extends DecoratorBase {
	protected AuctionQuotation quotation;

   /**
	* Creates a new decorator wrapped around an <code>Quotation</code>
	* object.
	*/
   public AuctionQuotationDecoratorBase(AuctionQuotation quotation) {
	   this.quotation = quotation;
   }

   // DEBUG START
   public AuctionQuotationDecoratorBase() {
   }
}
