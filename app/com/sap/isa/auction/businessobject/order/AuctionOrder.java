/*
 * Created on Sep 18, 2003
 *
 * SAP Labs LLC, the eBay auction project, also is known as Selling via eBay. 
 * Selling via eBay application provides SAP R/3 (and in the future CRM) 
 * customers to auction and sell inventory/products onto an online marketplace 
 * eBay. The application will provide the ability to Create, Schedule, Publish, 
 * Monitor and Close Auction Listings onto eBay and handle all the relevant 
 * Backend processing for Customers and Orders. 

 */
package com.sap.isa.auction.businessobject.order;

import com.sap.tc.logging.Category;
import com.sap.tc.logging.Location;
import com.sap.isa.auction.backend.exception.BackendUserException;
import com.sap.isa.auction.backend.exception.CommunicationBackendUserException;
import com.sap.isa.auction.backend.exception.InvalidAddressException;
import com.sap.isa.auction.backend.boi.order.AuctionOrderBackend;
import com.sap.isa.auction.backend.boi.order.AuctionOrderData;
import com.sap.isa.auction.logging.CategoryProvider;
import com.sap.isa.backend.boi.isacore.AddressData;
import com.sap.isa.backend.boi.isacore.SalesDocumentBackend;
import com.sap.isa.backend.boi.isacore.order.OrderBackend;
import com.sap.isa.backend.crm.order.OrderCRM;
import com.sap.isa.businessobject.Address;
import com.sap.isa.businessobject.BusinessObjectHelper;
import com.sap.isa.businessobject.CommunicationException;
import com.sap.isa.businessobject.ShipTo;
import com.sap.isa.businessobject.order.Order;
import com.sap.isa.core.TechKey;
import com.sap.isa.core.eai.BackendException;
/**
/**
 * The extended order class for the auction scenario
 */
public class AuctionOrder extends Order implements AuctionOrderData {
	
	/**
	 * The sales employee ID
	 */
	private String employee= null;

	/**
	 * The resp sales organisation
	 */
	private String respSalesOrg = null;
	
	/**
	 * This attribute actually holds the ID from auction side for the published 
	 * list
	 */
	private String listingId = null;
	
	private String insuranceValue = null;
	
	private String winningPrice = null;
	
	private String otherChargeDiscount = null;
	/**
	 * Holds the Predecessor document ID, which is the
	 * quotation ID
	 */
	private String predecessorDocumentId = null;
	
	// following attributes from BackendSetting
	private String tManualPriceCondition;
	private String taxManualPriceCondition;
	private String iManualPriceCondition;
	private String fManualPriceCondition;
	private String oManualPriceCondition;
	private String deliveryBlock;
	private String deliveryBlockText;
	private String billingBlock;
	private String incoTerm;
	private boolean updateAddress = true;
	
	private String paymentType;
	
	/**
	 * The user Id to track the external eBay user.
	 */
	private String externalUserId;

	private static final Category logger = CategoryProvider.getBOLCategory();
	private static final Location tracer = Location.getLocation(AuctionOrder.class.getName());

	/**
	 * @param, the sales employee ID, as in the partners section
	 */
	public void setReponsibleEmployee(String bpId) {
		employee = bpId;
	}

	/**
	 * @return the responsible employee copied from auction
	 */
	public String getResponsibleEmployee() {
		return employee;
	}

	/**
	 * @param, the responsible sales organisation
	 */
	public void setRespSalesOrg(String respSalesOrg) {
		this.respSalesOrg = respSalesOrg;
	}

	/**
	 * @return the responsible sales organisation
	 */
	public String getRespSalesOrg() {
		return this.respSalesOrg;
	}

	/**
	 * Setter for set to the insurance fee
	 * @param value
	 */
	public void setInsuranceValue(String value){
		insuranceValue = value;
	}
	
	/**
	 * Getter for returning the insurance cost
	 * @return
	 */
	public String getInsuranceValue(){
		return insuranceValue;
	}

	/**
	 * This is the listing numebr from the auction side, the ID
	 * is retrieved usually from the auction site, it is used to
	 * retrieve all the corelated quotation and orders.
	 */
	public void setPurchaseNumber(String purchaseNumber) {
		listingId = purchaseNumber;
	}

	public String getPurchaseNumber() {
		return listingId;
	}

	
	/**
	 * @return
	 */
	public String getPredecessorDocumentId() {
		return predecessorDocumentId;
	}

	/**
	 * @param string
	 */
	public void setPredecessorDocumentId(String string) {
		predecessorDocumentId = string;
	}
	/**
	 * Cancel an order
	 */
	 public void cancel(TechKey techKey) throws CommunicationException {
		 this.techKey = techKey;
			
		 final String METHOD = "cancel()";
		 log.entering(METHOD);
		 // write some debugging info
		 if (log.isDebugEnabled()) {
			 log.debug("cancel(): techKey = " + techKey);
		 }
	
		 try {
			 // cancel order in Backend
			 isDirty = true;
			 header.setDirty(true);
			 ((AuctionOrderBackend) getBackendService()).cancelInBackend(this);
		 }
		 catch (BackendException ex) {
			 BusinessObjectHelper.splitException(ex);
		 }
		 finally {
			 log.exiting();
		 }
	 }
	public void releaseDistributionBlock() throws CommunicationException {
		getHeader().setDirty(true); //enforce always read from backend
		String orderID = getHeader().getSalesDocNumber();
		if (orderID == null) {
			logger.errorT(
				tracer,
				"During the release distribution block the orderID is null");
			throw new CommunicationException("During the release distribution block the orderID is null");
		}
		try {
			((AuctionOrderBackend) getBackendService()).releaseOrderSystemStatus(this);
		//			isDirty = true;
		//			header.setDirty(true);
		} catch (BackendUserException ex) {
			logger.errorT(
				tracer,
				"User Exception in AuctionOrder release distribution block" + ex.getMessage());
			throw new CommunicationBackendUserException(ex);
		} catch (BackendException ex) {
			logger.errorT(
				tracer,
				"Errors in AuctionOrder delivery block" + ex.getMessage());
			BusinessObjectHelper.splitException(ex);
		}
	}
	/**
	 * Release the delivery block after the Auction order has been created
	 * In the second step, check the shipping address is null.
	 * If the shipping address is null, then do not release delivery block.
	 * Otherwise, it is OK to release delivery block.
	 * @throws CommunicationException
	 */
	public void releaseDeliveryBlock() throws CommunicationException {
		getHeader().setDirty(true); //enforce always read from backend
		String orderID = getHeader().getSalesDocNumber();
		if (orderID == null) {
			logger.errorT(
				tracer,
				"During the setting delivery block the orderID is null");
			throw new CommunicationException("During the setting delivery block the orderID is null");
		}
		if (checkShippingAddress()) {
			try {
				((AuctionOrderBackend) getBackendService()).releaseDeliveryBlock(this);
			//			isDirty = true;
			//			header.setDirty(true);
			} catch (BackendUserException ex) {
				logger.errorT(
					tracer,
					"User Exception in AuctionOrder delivery block" + ex.getMessage());
				throw new CommunicationBackendUserException(ex);
			} catch (BackendException ex) {
				logger.errorT(
					tracer,
					"Errors in AuctionOrder delivery block" + ex.getMessage());
				BusinessObjectHelper.splitException(ex);
			}
		}
		else
			throw InvalidAddressException.create(InvalidAddressException.OTHER_ID);
	}
	
	/**
	 * Release the billing block after the 
	 * @throws CommunicationException
	 */
	public void releaseBillingBlock() throws CommunicationException{
		getHeader().setDirty(true); //enforce always read from backend
		String orderID = getHeader().getSalesDocNumber();
		if(orderID == null){
			logger.errorT(tracer, "During the setting billing block the orderID is null");
			throw new CommunicationException("During the setting billing block the orderID is null");
		}
		try {
			((AuctionOrderBackend) getBackendService()).releaseBillingBlock(this);
//			isDirty = true;
//			header.setDirty(true);
		} catch (BackendUserException ex) {
			logger.errorT(
				tracer,
				"User Exception in AuctionOrder delivery block" + ex.getMessage());
			throw new CommunicationBackendUserException(ex);
		} catch (BackendException ex) {
			logger.errorT(tracer, "Errors in AuctionOrder release billing block" + ex.getMessage());
			BusinessObjectHelper.splitException(ex);
		}				
		
	}	
	
	
	/**
	 * Update the shipping address 
	 * @throws CommunicationException
	 */
	public void updateShippingInfo(AddressData address) throws CommunicationException{
		String orderID = getHeader().getSalesDocNumber();
		String orderTKey = null;
		try{
			orderTKey = getTechKey().getIdAsString().trim();
		}catch(NullPointerException ex){
			logger.warning(tracer, "In the update of shipping address, the order" +
				" TechKey was not set");
		}
		if((orderID == null) && ((orderTKey == null) || (orderTKey.length()==0))){
			logger.errorT(tracer, "During the update shipping info the order ID is null");
			throw new CommunicationException("During the update shipping info the orderID is null");
		}
		try {
			((AuctionOrderBackend) getBackendService()).
							updateManualShippingAddress(this, address);
//			isDirty = true;
//			header.setDirty(true);
		} catch (BackendUserException ex) {
			logger.errorT(tracer, "Errors in AuctionOrder Update");
			CommunicationBackendUserException _ex= new CommunicationBackendUserException(ex);
			throw _ex;
		} catch (BackendException ex) {
			logger.errorT(tracer, "Errors in AuctionOrder in updating shipping address");
			BusinessObjectHelper.splitException(ex);
		}				
		
	}	
	
	/**
	 * Update the price info 
	 * @throws CommunicationException
	 */
	public void updatePriceInfo() throws CommunicationException{
		String orderID = getHeader().getSalesDocNumber();
		String orderTKey = null;
		try{
			orderTKey = getTechKey().getIdAsString().trim();
		}catch(NullPointerException ex){
			logger.warning(tracer, "In the update of pricing, the order" +
				" TechKey was not set");
		}
		if((orderID == null) && ((orderTKey == null) || (orderTKey.length()==0))){
			logger.errorT(tracer, "During the update pricing info the order ID is null");
			throw new CommunicationException("During the update pricing info the orderID is null");
		}
		try {
			((AuctionOrderBackend) getBackendService()).
							updateSalesDocumentPrices(this);
//			isDirty = true;
//			header.setDirty(true);
		} catch (BackendUserException ex) {
			logger.errorT(tracer, "Errors in AuctionOrder update");
			CommunicationBackendUserException _ex= new CommunicationBackendUserException(ex);
			throw _ex;
		} catch (BackendException ex) {
			logger.errorT(tracer, "Errors in AuctionOrder update pricing" + ex.getMessage());
			BusinessObjectHelper.splitException(ex);
		}				
		
	}	
	/**
	 * Create The sales order from a quotation
	 * @param quotationId			The quotation ID from the auction
	 * @param order					The orer data has been populated
	 * @param newShipToAddress		maually set the shipping address
	 * @throws CommunicationException
	 */
	public void createFromQuotation(
				String quotationId,
				AuctionOrderData order,
			   Address newShipToAddress 
				) throws CommunicationException {
	
		try {
			((AuctionOrderBackend) getBackendService()).createFromQuotation(quotationId,
														this,
														newShipToAddress);
//			isDirty = true;
//			header.setDirty(true);
		} catch (BackendUserException ex) {
			logger.errorT(tracer, "Errors in AuctionOrder Creation");
			CommunicationBackendUserException _ex= new CommunicationBackendUserException(ex);
			throw _ex;
		} catch (BackendException ex) {
			logger.errorT(tracer, "Errors in AuctionOrder Creation");
			BusinessObjectHelper.splitException(ex);
		}				
	}
	
	public boolean isBasedOnCRM() {
		try {
			if(getBackendService() instanceof OrderCRM)
				return true;
			else
				return false;	
		} catch (BackendException e) {
			return false;
		}
	}
				
	protected SalesDocumentBackend getBackendService()
	throws BackendException {

		synchronized (this) {
			if (backendService == null) {
				backendService = (AuctionOrderBackend) bem.createBackendBusinessObject(
								TransactionDataConstants.OBJECT_AUCTION_ORDER, null);
			}
	
		}
		return backendService;
	}

	/* (non-Javadoc)
	 * @see com.sapmarkets.isa.auction.backend.boi.order.AuctionOrderData#getTotalManualPriceCondition()
	 */
	public String getTotalManualPriceCondition() {
		return this.tManualPriceCondition;
	}

	/* (non-Javadoc)
	 * @see com.sapmarkets.isa.auction.backend.boi.order.AuctionOrderData#setTotalManualPriceCondition(java.lang.String)
	 */
	public void setTotalManualPriceCondition(String priceType) {
		this.tManualPriceCondition = priceType;
		
	}

	/* (non-Javadoc)
	 * @see com.sapmarkets.isa.auction.backend.boi.order.AuctionOrderData#getTaxManualPriceCondition()
	 */
	public String getTaxManualPriceCondition() {
		return this.taxManualPriceCondition;
	}

	/* (non-Javadoc)
	 * @see com.sapmarkets.isa.auction.backend.boi.order.AuctionOrderData#setTaxManualPriceCondition(java.lang.String)
	 */
	public void setTaxManualPriceCondition(String priceType) {
		this.taxManualPriceCondition = priceType;
		
	}

	/* (non-Javadoc)
	 * @see com.sapmarkets.isa.auction.backend.boi.order.AuctionOrderData#getInsurManualPriceCondition()
	 */
	public String getInsurManualPriceCondition() {
		return this.iManualPriceCondition;
	}

	/* (non-Javadoc)
	 * @see com.sapmarkets.isa.auction.backend.boi.order.AuctionOrderData#setInsurManualPriceCondition(java.lang.String)
	 */
	public void setInsurManualPriceCondition(String priceType) {
		this.iManualPriceCondition = priceType;
		
	}

	/* (non-Javadoc)
	 * @see com.sapmarkets.isa.auction.backend.boi.order.AuctionOrderData#getFreightManualPriceCondition()
	 */
	public String getFreightManualPriceCondition() {
		return this.fManualPriceCondition;
	}

	/* (non-Javadoc)
	 * @see com.sapmarkets.isa.auction.backend.boi.order.AuctionOrderData#setFreightManualPriceCondition(java.lang.String)
	 */
	public void setFreightManualPriceCondition(String priceType) {
		this.fManualPriceCondition = priceType;
		
	}

	/* (non-Javadoc)
	 * @see com.sapmarkets.isa.auction.backend.boi.order.AuctionOrderData#getDeliveryBlockCode()
	 */
	public String getDeliveryBlockCode() {
		return this.deliveryBlock;
	}

	/* (non-Javadoc)
	 * @see com.sapmarkets.isa.auction.backend.boi.order.AuctionOrderData#setDeliveryCode(java.lang.String)
	 */
	public void setDeliveryCode(String block) {
		this.deliveryBlock = block;
		
	}

	/* (non-Javadoc)
	 * @see com.sapmarkets.isa.auction.backend.boi.order.AuctionOrderData#getBillingBlockCode()
	 */
	public String getBillingBlockCode() {
		return this.billingBlock;
	}

	/* (non-Javadoc)
	 * @see com.sapmarkets.isa.auction.backend.boi.order.AuctionOrderData#setBillingBlockCode(java.lang.String)
	 */
	public void setBillingBlockCode(String block) {
		this.billingBlock = block;		
	}

	/* (non-Javadoc)
	 * @see com.sapmarkets.isa.auction.backend.boi.order.AuctionOrderData#getIncoTerm()
	 */
	public String getIncoTerm() {
		return this.incoTerm;
	}

	/* (non-Javadoc)
	 * @see com.sapmarkets.isa.auction.backend.boi.order.AuctionOrderData#setIncoterm(java.lang.String)
	 */
	public void setIncoterm(String incoTerm) {
		this.incoTerm = incoTerm;
		
	}

	/* (non-Javadoc)
	 * @see com.sapmarkets.isa.auction.backend.boi.order.AuctionOrderData#getDeliveryBlockText()
	 */
	public String getDeliveryBlockText() {
		return deliveryBlockText;
	}

	/* (non-Javadoc)
	 * @see com.sapmarkets.isa.auction.backend.boi.order.AuctionOrderData#setDeliveryText(java.lang.String)
	 */
	public void setDeliveryText(String text) {
		deliveryBlockText = text;
	}
	/**
	 * @return
	 */
	public String getWinningPrice() {
		return winningPrice;
	}

	/**
	 * @param string
	 */
	public void setWinningPrice(String string) {

		winningPrice = string;
	}
	
	/**
	 * Saves the order in the backend.
	 */
	public void saveAndCommit() throws CommunicationException {

		// write some debugging info
		logger.errorT(tracer, "save()");

		try {
			((OrderBackend) getBackendService()).saveInBackend(this, true);
			isDirty = true;
			header.setDirty(true);
		}
		catch (BackendUserException ex) {
			CommunicationBackendUserException _ex = new CommunicationBackendUserException(ex);
			throw _ex;
		}
		catch (BackendException ex) {
			BusinessObjectHelper.splitException(ex);
		}
	}
	/**
	 * @return
	 */
	public boolean isUpdateAddress() {
		return updateAddress;
	}

	/**
	 * @param b
	 */
	public void setUpdateAddress(boolean b) {
		updateAddress = b;
	}

	/**
	 * @return
	 */
	public String getOtherPriceCondition() {
		return oManualPriceCondition;
	}

	/**
	 * @return
	 */
	public String getOtherChargeDiscount() {
		return otherChargeDiscount;
	}

	/**
	 * @param string
	 */
	public void setOtherPriceCondition(String string) {
		oManualPriceCondition = string;
	}

	/**
	 * @param string
	 */
	public void setOtherChargeDiscount(String string) {
		otherChargeDiscount = string;
	}
	
	public boolean checkShippingAddress(){
		String orderID = getHeader().getSalesDocNumber();
		boolean validAddress = false;
		InvalidAddressException throwOutException = null;
		try {
			logger.infoT(tracer, "before the read Order Object");
			if((getTechKey() == null) || (getTechKey().getIdAsString().trim().length() == 0)){
				setTechKey(new TechKey(orderID));
			}
			ShipTo[] shipTos = getShipTos();
			if (shipTos == null || shipTos.length<=0) {
				readShipTos(); //read shipto only if necessary
				shipTos = getShipTos();
			}			
			logger.infoT(tracer, "Order's shipTo is read successfully");
			
			ShipTo shipTo = null;
			for(int i = 0; i< shipTos.length; i++){
				shipTo = shipTos[i];
				logger.infoT(tracer, "The shipTo is " + shipTo);
				Address address = null;
				if(shipTo != null)
					address = shipTo.getAddress();
				try {
					if (address == null) {
						logger.errorT(
							tracer,
							"During Reading the shipping address from " +
									"Order object address is null");
						InvalidAddressException _ex = InvalidAddressException.create(
								InvalidAddressException.ADDRESS_NULL_ID);
						throw _ex;
					} else {
						if ((address.getLastName() == null)
							&& (address.getBirthName() == null)
							&& (address.getFirstName() == null)
							&& (address.getName() == null)) {
							logger.errorT(
								tracer,
								"During Reading the shipping address from "
									+ "Order object Names are null");
							InvalidAddressException _ex = InvalidAddressException.create(
									InvalidAddressException.NAME_NULL_ID);
							throw _ex;
						}
						if ((address.getStreet() == null)
							|| (address.getStreet().trim().length() == 0)) {
								logger.errorT(
									tracer,
									"During Reading the shipping address from " +
											"Order object street is null");
								InvalidAddressException _ex = InvalidAddressException.create(
										InvalidAddressException.STREET_NULL_ID);
								throw _ex;					}
						if((address.getCity() == null) || 
						(address.getCity().trim().length() == 0)){
							logger.errorT(
								tracer,
								"During Reading the shipping address from " +
										"Order object city is null");
							InvalidAddressException _ex = InvalidAddressException.create(
									InvalidAddressException.CITY_NULL_ID);
							throw _ex;
						}
						validAddress = true;
						logger.infoT(tracer, "The shipping address is " + validAddress);
						break;
					}
				} catch (InvalidAddressException _ex) {
					throwOutException = _ex;
				}
			}
		}catch (Exception ex){
			logger.errorT(tracer,
				"During Reading the shipping address from Order object "
					+ ex.getMessage());
			InvalidAddressException _ex = InvalidAddressException.create(
					InvalidAddressException.OTHER_ID);
			throw _ex;
		}
		
		if(throwOutException != null)
			throw throwOutException;
			
		return validAddress;
	}

	/* (non-Javadoc)
	 * @see com.sapmarkets.isa.auction.backend.boi.order.AuctionOrderData#setPaymentType(java.lang.String)
	 */
	public void setPaymentType(String payType) {
		this.paymentType = payType;
		
	}

	/* (non-Javadoc)
	 * @see com.sapmarkets.isa.auction.backend.boi.order.AuctionOrderData#getPaymentType()
	 */
	public String getPaymentType() {
		return paymentType;
	}

	/**
	 * @return
	 */
	public String getExternalUserId() {
		return externalUserId;
	}

	/**
	 * @param string
	 */
	public void setExternalUserId(String string) {
		externalUserId = string;
	}


}
