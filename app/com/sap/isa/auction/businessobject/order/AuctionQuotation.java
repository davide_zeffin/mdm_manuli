/*
 * Created on Sep 18, 2003
 *
 * SAP Labs LLC, the eBay auction project, also is known as Selling via eBay. 
 * Selling via eBay application provides SAP R/3 (and in the future CRM) 
 * customers to auction and sell inventory/products onto an online marketplace 
 * eBay. The application will provide the ability to Create, Schedule, Publish, 
 * Monitor and Close Auction Listings onto eBay and handle all the relevant 
 * Backend processing for Customers and Orders. 

 ****** the handle for B2C checkout, without ask for the 
 ****** onetime customer
 */
package com.sap.isa.auction.businessobject.order;

import com.sap.tc.logging.Category;
import com.sap.tc.logging.Location;
import com.sap.isa.auction.backend.boi.order.AuctionQuotationBackend;
import com.sap.isa.auction.backend.boi.order.AuctionQuotationData;
import com.sap.isa.auction.backend.exception.BackendUserException;
import com.sap.isa.auction.backend.exception.CommunicationBackendUserException;
import com.sap.isa.auction.backend.r3.salesdocument.AuctionBackendBase;
import com.sap.isa.auction.bean.Auction;
import com.sap.isa.auction.bean.BackendDefaultSettings;
import com.sap.isa.auction.logging.CategoryProvider;
import com.sap.isa.auction.logging.TraceHelper;
import com.sap.isa.backend.boi.isacore.SalesDocumentBackend;
import com.sap.isa.backend.r3.salesdocument.SalesDocumentR3;
import com.sap.isa.businessobject.BusinessObjectHelper;
import com.sap.isa.businessobject.CommunicationException;
import com.sap.isa.businessobject.Shop;
import com.sap.isa.businessobject.header.HeaderSalesDocument;
import com.sap.isa.businessobject.order.PartnerList;
import com.sap.isa.businessobject.order.PartnerListEntry;
import com.sap.isa.businessobject.quotation.Quotation;
import com.sap.isa.core.TechKey;
import com.sap.isa.core.eai.BackendException;

public class AuctionQuotation
	extends Quotation
	implements AuctionQuotationData {
	/**
	 * The sales employee ID
	 */
	private String employee = null;

	private String totalPriceType = null;
	/**
	 * The responsible sales organisation of the catalog variant used in the shop
	 */	
	private String respSalesOrg = null;
	
	/**
	 * This attribute actually holds the ID from auction side for the published 
	 * list
	 */
	private String listingId = null;
	
	private static final Category logger = CategoryProvider.getBOLCategory();
	private static final Location tracer = Location.getLocation(AuctionQuotation.class.getName());

	/**
	 * @param, the sales employee ID, as in the partners section
	 */
	public void setReponsibleEmployee(String bpId) {
		employee = bpId;
	}

	/**
	 * @return the responsible employee copied from auction
	 */
	public String getResponsibleEmployee() {
		return employee;
	}

	/**
	 * @param, set the responsible sales organisation
	 */
	public void setRespSalesOrg(String respSalesOrg) {
		this.respSalesOrg = respSalesOrg;
	}

	/**
	 * @return set the responsible sales organisation
	 */
	public String getRespSalesOrg() {
		return this.respSalesOrg;
	}

	/**
	 * This is the listing numebr from the auction side, the ID
	 * is retrieved usually from the auction site, it is used to
	 * retrieve all the corelated quotation and orders.
	 */
	public void setPurchaseNumber(String purchaseNumber) {
		listingId = purchaseNumber;
		getHeader().setPurchaseOrderExt(purchaseNumber);
	}

	/**
	 * Return the auction listing ID
	 * @return the auctiion listing ID, used for tracking order history
	 */
	public String getPurchaseNumber() {
		if (listingId == null) {
			listingId = this.getHeader().getPurchaseOrderExt();
		}
		return listingId;
	}

	/**
	 * Saves both lean or extended quotations in the backend;
	 * needs shop to decide, this is not implemented in auction scenario
	 */
	public void save(Shop shop) throws CommunicationException {

		// write some debugging info
		if (logger.beDebug()) {
			logger.infoT(tracer, "save(Shop) is impossible in auction scenario");
		}
		throw new CommunicationException("Not supportted API in auction");
	}

	/**
	 * Saves only lean quotations in the backend; here for compatibility
	 */
	public void save() throws CommunicationException {	
		final String METHOD_NAME = "save()";	
		TraceHelper.entering(tracer, METHOD_NAME);		
		// write some debugging info
		if(logger.beInfo()) {
			logger.infoT(tracer,"save method in AuctionQuotation is called");
		}
		try {
			//setBackendSetting();
			// Remove quotation status in Backend
			((AuctionQuotationBackend) getBackendService()).createInBackend(
				this);
			isDirty = true;
			header.setDirty(true);
		} catch (BackendUserException ex) {
			logger.errorT(tracer, "In save() method caught exception " + ex.getMessage());
			CommunicationBackendUserException _ex = new CommunicationBackendUserException(ex);
			throw _ex;
		} catch (BackendException ex) {
			logger.errorT(tracer, "caught backendexception in save() method " 
										+ ex.getMessage());
			BusinessObjectHelper.splitException(ex);
		}
		if(logger.beInfo()) {
			logger.infoT(tracer,"save method in AuctionQuotation is finished");
		}
		TraceHelper.exiting(tracer, METHOD_NAME);	
	}

	/**
	 * Saves only lean quotations in the backend; here for compatibility
	 * @param one auction that is used to create a quotation
	 */
	public void save(Auction auction) throws CommunicationException {

		final String METHOD_NAME = "save(Auction auction)";	
		TraceHelper.entering(tracer, METHOD_NAME);	

		try {
			// Remove quotation status in Backend
			((AuctionQuotationBackend) getBackendService()).createInBackend(
				auction);
			isDirty = true;
			header.setDirty(true);
		} catch (BackendUserException ex) {
			log.error(ex);
			CommunicationBackendUserException _ex = new CommunicationBackendUserException(ex);
			throw _ex;
		} catch (BackendException ex) {
			BusinessObjectHelper.splitException(ex);
		}
		if(logger.beInfo()) {
			logger.infoT(tracer,"save method in AuctionQuotation is finished");
		}
		TraceHelper.exiting(tracer, METHOD_NAME);	
	}

	/**
	 * Set the eBay listing ID to a known auction
	 * Her only the eBay list ID and the auction ID is
	 * useful
	 * @param  String 		eBay listing IT
	 * @throws CommunicationException
	 */
	public void setListingId(String id) throws CommunicationException {
		// write some debugging info
		final String METHOD_NAME = "setListingId";			
		TraceHelper.entering(tracer, METHOD_NAME);	
		setPurchaseNumber(id);
		try {
			// Remove quotation status in Backend
			((AuctionQuotationBackend) getBackendService()).setLisingIdInBackend(
				this);
			isDirty = true;
			header.setDirty(true);
		} catch (BackendException ex) {
			BusinessObjectHelper.splitException(ex);
		}
		if(logger.beInfo()) {
			logger.infoT(tracer,"setListingId method in AuctionQuotation is finished");
		}
		TraceHelper.exiting(tracer, METHOD_NAME);	
	}
	

	/**
	 * modify the quotation in the backend
	 * @param AuctionQuotationData
	 * @
	 */
	public void modifyQuotation() throws CommunicationException {
		// write some debugging info
		if(logger.beInfo()) {
			logger.infoT(tracer,"modifyQuotation method in AuctionQuotation is called");
		}		
		try {
			// Remove quotation status in Backend
			((AuctionQuotationBackend) getBackendService()).modifyInBackend(
				this);
			isDirty = true;
			header.setDirty(true);
		} catch (BackendUserException ex) {
			log.error(ex);
			CommunicationBackendUserException _ex = new CommunicationBackendUserException(ex);
			throw _ex;
		} catch (BackendException ex) {
			BusinessObjectHelper.splitException(ex);
		}
		if(logger.beInfo()) {
			logger.infoT(tracer,"modifyQuotation method in AuctionQuotation is finished");
		}		
	}


	/*
	 * Releases the left over(unsold) quantity in a quotation
	 * 
	 */
	public void releaseRemainigQuantityInQuotation() throws CommunicationException {
		if(logger.beInfo()) {
			logger.infoT(tracer,"releaseUnusedQuantity method in AuctionQuotation is called");
		}		
		try {
			// Release the remaining items from the backend
			((AuctionQuotationBackend) getBackendService()).releaseRemainigQuantityInQuotation(
				this);
			isDirty = true;
			header.setDirty(true);
		} catch (BackendUserException ex) {
			log.error(ex);
			CommunicationBackendUserException _ex = new CommunicationBackendUserException(ex);
			throw _ex;
		} catch (BackendException ex) {
			BusinessObjectHelper.splitException(ex);
		}
		if(logger.beInfo()) {
			logger.infoT(tracer,"releaseUnusedQuantity method in AuctionQuotation is finished");
		}		
	}
	
	/*
	 * Reads the quotation information from backend
	 * Reason for not using ISA supported method:
	 * Reading quotaiton data is bound tightly to basket and shop .
	 * SVE does not have shop in eBay checkout scenario.
	 */
	public void readFromBackend() throws CommunicationException {
		if(logger.beInfo()) {
			logger.infoT(tracer,"readFromBackend method in AuctionQuotation is called");
		}		
		try {
			// Read the quotation from theBackend
			((AuctionQuotationBackend) getBackendService()).readFromBackend(
				this);
			
		} catch (BackendUserException ex) {
			log.error(ex);
			CommunicationBackendUserException _ex = new CommunicationBackendUserException(ex);
			throw _ex;
		} catch (BackendException ex) {
			BusinessObjectHelper.splitException(ex);
		}
		if(logger.beInfo()) {
			logger.infoT(tracer,"readFromBackend method in AuctionQuotation is finished");
		}		
	}	
	/**
	 * Get the reference of the backend service and creates it when necessary
	 * this protected method can be used by inherited classes to get
	 * the backend service
	 *
	 * @return the "typeless" backend business object
	 *
	 */
	protected SalesDocumentBackend getBackendService()
		throws BackendException {

		synchronized (this) {
			if (backendService == null) {
				backendService =
					(AuctionQuotationBackend) bem.createBackendBusinessObject(
						TransactionDataConstants.OBJECT_AUCTION_QUOTATION,
						null);
			}
		}
		return backendService;
	}

	/**
	 * Set the sales area and one time customer information etc
	 * As the first step of the setting
	 * @param mgr		EBayAuctionProfilesMgr, holds all the backend data
	 */
	public void setBackendSetting(BackendDefaultSettings setting) throws CommunicationException, BackendException {

		if(logger.beInfo()) {
			logger.infoT(tracer,"setBackendSetting method in AuctionQuotation is called");
		}				
		SalesDocumentBackend backend = getBackendService();
		if(backend != null && backend instanceof AuctionBackendBase) {
			((AuctionBackendBase)backend).setBackendSettings(setting);
			if (setting.getSalesArea() != null) {
				setRespSalesOrg(setting.getSalesArea().getResponsibleSalesOrganization());  // TODO: check if needed at this place
			}
		}
		
		//change history, here the soldTo party is not a must
		// even the SOLDTO party is not needed, then we have to
		// pass the ID
		
		String oneTimeId = null;
		if(backend instanceof SalesDocumentR3) {
			if (setting.isIndependentCheckout()){
				oneTimeId = setting.getCashOnDeliveryBP();
				if ((oneTimeId == null) || (oneTimeId.length() <1)){
					oneTimeId = setting.getRefBP();
				}
				// we should throw exception here
				if ((oneTimeId == null) || (oneTimeId.length() <1)){
					
				}	
			}else{
				boolean isOneTimeCustomer = setting.isOneTimeCustomerScenario();
				// ID actually used to retrieve the ID
				if (isOneTimeCustomer){
					oneTimeId = setting.getCashOnDeliveryBP();
				}else{
					oneTimeId = setting.getRefBP();
				}
			}
		}
		else
			oneTimeId = "0000000000"; //for CRM, no use of ref BPId
			
		String quoteType = setting.getQuotationType();
		HeaderSalesDocument header = getHeader();

		if (quoteType != null) {
			header.setDocumentType(quoteType);
		}
		if (oneTimeId != null) {
			PartnerList partnerList = header.getPartnerList();
			if (partnerList == null){
				if(logger.beInfo()) {
					logger.infoT(tracer,
							"The PartnerList is null, create a new one");
				}				
				partnerList = new PartnerList();
				header.setPartnerList(partnerList);
			}
			
			//PartnerListEntry entry = partnerList.getSoldTo();
			//if (partnerList.getSoldTo() == null || ) {
				PartnerListEntry entrySoldTo = new PartnerListEntry();
				entrySoldTo.setPartnerId(oneTimeId);
				entrySoldTo.setPartnerTechKey(new TechKey(oneTimeId));
				partnerList.setSoldTo(entrySoldTo);
			//}
		} else {
			logger.errorT(tracer, "Can not get SoldTo from OneTime Customer or Ref BP");
			throw new CommunicationException(
				"Can not get SoldTo from OneTimeCustomer or" + " RefBP");
		}
		if(logger.beInfo()) {
			logger.infoT(tracer,"setBackendSetting method in AuctionQuotation is finished");
		}				
	}

	/* (non-Javadoc)
	 * @see com.sap.isa.auction.backend.boi.order.AuctionSalesDocumentData#getTotalManualPriceCondition()
	 */
	public String getTotalManualPriceCondition() {
		return this.totalPriceType;
	}

	/* (non-Javadoc)
	 * @see com.sap.isa.auction.backend.boi.order.AuctionSalesDocumentData#setTotalManualPriceCondition(java.lang.String)
	 */
	public void setTotalManualPriceCondition(String priceType) {
		this.totalPriceType = priceType;		
	}


}
