/*
 * Created on May 18, 2004
 *
 * To change the template for this generated file go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
package com.sap.isa.auction.businessobject;

import com.sap.isa.auction.backend.boi.SystemSettingData;
import com.sap.isa.businessobject.BusinessObjectBase;
import com.sap.isa.core.TechKey;
import com.sap.isa.core.businessobject.BackendAware;
import com.sap.isa.core.eai.BackendBusinessObject;
import com.sap.isa.core.eai.BackendException;
import com.sap.isa.core.eai.BackendObjectManager;

/**
 * @author I803746
 *
 * To change the template for this generated type comment go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
public class SystemSettingObject
	extends BusinessObjectBase
	implements BackendAware {

	private String client = null;
	private String systemId = null;

	protected BackendObjectManager bem;
	protected BackendBusinessObject backendService;

	/**
	 * 
	 */
	public SystemSettingObject() {
		super();
	}

	/**
	 * @param arg0
	 */
	public SystemSettingObject(TechKey arg0) {
		super(arg0);
	}

	/* (non-Javadoc)
	 * @see com.sapmarkets.isa.core.businessobject.BackendAware#setBackendObjectManager(com.sapmarkets.isa.core.eai.BackendObjectManager)
	 */
	public void setBackendObjectManager(BackendObjectManager bom) {
		bem = bom;
	}

	/**
	 * Get the reference of the backend service and creates it when necessary
	 * this protected method can be used by inherited classes to get
	 * the backend service
	 *
	 * @return the "typeless" backend business object
	 *
	 */
	protected BackendBusinessObject createBackendService()
		throws BackendException {

		if (backendService == null) {
			backendService = bem.createBackendBusinessObject("SystemSetting", null);
		}
		return backendService;
	}

	/* (non-Javadoc)
	 * 
	 */
	public String getClient() {
		if(client!=null) return client;
		try {
			return ((SystemSettingData)createBackendService()).getClient();
		} catch (BackendException e) {
			return null;
		}
	}

	/* (non-Javadoc)
	 * 
	 */
	public String getSystemId() {
		if(systemId!=null) return systemId;
		try {
			return ((SystemSettingData)createBackendService()).getSystemId();
		} catch (BackendException e) {
			return null;
		}
	}

}
