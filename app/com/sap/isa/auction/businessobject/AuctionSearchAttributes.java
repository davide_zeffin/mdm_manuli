package com.sap.isa.auction.businessobject;

/**
 * Attributes based on which Auctions can be searched. Used for Advanced search
 * Description:
 * Copyright:    Copyright (c) 2002
 * Company:      SAPMarkets
 * @author
 * @version 1.0
 */

public interface AuctionSearchAttributes {

    // Value Type java.lang.String
    public static final String NAME = "name";   // wild card allowed
    // Value Type java.lang.Integer
    public static final String STATUS = "status";
    // Value Type java.lang.Integer
    public static final String TYPE = "type";

    // Value Type java.lang.String
    public static final String PRODUCT_ID = "productId"; // wild card allowed

    // Value Type java.lang.String
    public static final String WEBSHOP_ID = "webshopId";

    // Value Type java.sql.Timestamp
    public static final String START_DATE = "startDate";
    public static final String END_DATE = "endDate";
	public static final String PUBLISH_DATE = "publishDate";
    public static final String CREATE_DATE = "createDate";

    // Value Type java.lang.String
    public static final String CREATED_BY = "createdBy"; // if present indicates seller specific search

	// Value Type java.lang.String
	public static final String SALESAREA = "salesArea";
	
	//Value Type boolean
	public static final String PUBLISH_ERROR = "publishFailure";
	
	//Value Type java.lang.Integer
	public static final String CHECKOUT_STATUS = "checkoutStatus";
	
	//Value Type java.lang.Integer
	public static final String WINNER_STATUS = "winnerStatus";
	
	
	//Value Type java.lang.String
	public static final String ORDER_ID = "orderId";
	
	//Value Type java.lang.String
	public static final String TITLE = "title";
    
    public static final String[] ATTRIBUTES = {NAME, STATUS, 
    		CHECKOUT_STATUS,TYPE, PRODUCT_ID, WEBSHOP_ID,
    		PUBLISH_DATE,SALESAREA,PUBLISH_ERROR,
    		START_DATE, END_DATE, CREATE_DATE, CREATED_BY,
			ORDER_ID, TITLE,WINNER_STATUS};
}