/*
 * Created on Sep 24, 2003
 *
 */
package com.sap.isa.auction.businessobject;

import com.sap.tc.logging.Category;
import com.sap.tc.logging.Location;
import com.sap.isa.auction.exception.UserException;
import com.sap.isa.auction.exception.i18n.ErrorMessageBundle;
import com.sap.isa.auction.exception.i18n.I18nErrorMessage;

/**
 * @author I802891
 *
 */
public class BOUserException extends UserException {
	
	private static final String EXC_ID = "BOUserException";
	
	public static final String AUCTION_NOT_FOUND = EXC_ID + 203;
	public static final String AUCTIONLISTING_NOT_FOUND = EXC_ID + 204;
	public static final String AUCTION_NOT_FINALIZED = EXC_ID + 205;
	public static final String AUCTION_NOT_CLOSED = EXC_ID + 206;
	public static final String AUCTION_NOT_PUBLISHED = EXC_ID + 207;
	public static final String INVALID_AUCTION_STATUS = EXC_ID + 208;
	public static final String AUCTION_NOT_ACTIVE = EXC_ID + 209;

	public static final String AUTOBID_BROKEN_LOT_ERROR = EXC_ID + 210;
	public static final String AUTOBID_ONE_PER_USER_PER_AUCTION_ERROR = EXC_ID + 211;

	public static final String MANUAL_WINNER_NOT_ALLOWED = EXC_ID + 212;

	public static final String NO_PERMISSION_ERROR = EXC_ID + 213;
	public static final String INVALID_USER = EXC_ID + 214;

	public static final String AUCTION_NOT_MODIFIABLE = EXC_ID + 215;
	public static final String AUCTION_STATUS_CHANGE_NOT_ALLOWED = EXC_ID + 216;

	public static final String NO_AUCTION_LISTING_ERROR = EXC_ID + 217;

	public static final String AUTOBID_END_DATE_AFTER_AUCTION_END_DATE = EXC_ID + 218;
	public static final String AUTOBID_BID_INCR_LT_AUCTION_BID_INCR = EXC_ID + 219;

	public static final String INVALID_USER_FOR_SETTING_MANUAL_WINNERS = EXC_ID + 220;
	public static final String INVALID_AUCTION_WINNER = EXC_ID + 221;
	public static final String AUCTION_WINNER_COUNT_MISMATCH = EXC_ID + 222;

	public static final String INVALID_SEARCH = EXC_ID + 223;

	public static final String QUOTATION_EXPIRATION_ERROR = EXC_ID + 224;

	public static final String MULTIPLE_WINNER_PER_LISTING_ERROR = EXC_ID + 225;
	public static final String WINNER_ALLOCATED_QUANTITY_MISMATCH_ERROR = EXC_ID + 226;

	public static final String ONLY_ONE_RELISTING_ALLOWED_VIOLATION = EXC_ID + 227;
	public static final String ALREADY_RELISTED_ERROR = EXC_ID + 228;	
	public static final String RELISTING_TIME_EXPIRATION_ERROR = EXC_ID + 229;	
	public static final String ONLY_NON_FINALIZED_AUCTION_RELISTING_ALLOWED_VIOLATION = EXC_ID + 230;
	
	public static final String NO_POSITIVE_FEEDBACK_MAINTAINED = EXC_ID + 231;
	
	public static final String NO_AUCTION_WINNER_ERROR = EXC_ID + 232;
	
	static {
		ErrorMessageBundle.setDefaultMessage(AUCTION_NOT_FOUND,"Auction with id {0} not found");
		ErrorMessageBundle.setDefaultMessage(AUCTIONLISTING_NOT_FOUND,"Auction listing with id {0} not found");
		ErrorMessageBundle.setDefaultMessage(AUCTION_NOT_FINALIZED,"Auction {0} is not in finalized state");
		ErrorMessageBundle.setDefaultMessage(AUCTION_NOT_CLOSED,"Auction {0} is not in closed state");
		ErrorMessageBundle.setDefaultMessage(AUCTION_NOT_PUBLISHED,"Auction {0} is not in published state");
		ErrorMessageBundle.setDefaultMessage(AUCTION_NOT_ACTIVE,"Auction {0} is not in active state");
		ErrorMessageBundle.setDefaultMessage(INVALID_AUCTION_STATUS,"Auction {0} status {1} is invalid");
		ErrorMessageBundle.setDefaultMessage(AUTOBID_BROKEN_LOT_ERROR,"Autobidding is only allowed for Full-lot auctions");
		ErrorMessageBundle.setDefaultMessage(AUTOBID_ONE_PER_USER_PER_AUCTION_ERROR,"Only one active Autobid is allowed per auction per user");
		ErrorMessageBundle.setDefaultMessage(MANUAL_WINNER_NOT_ALLOWED,"Auction {0} finalization rule does not allow Manual Winner determination");
		ErrorMessageBundle.setDefaultMessage(NO_PERMISSION_ERROR,"User {0} does not have permission to perform the requested operation");
		ErrorMessageBundle.setDefaultMessage(INVALID_USER,"User {0} is invalid");
		ErrorMessageBundle.setDefaultMessage(AUCTION_NOT_MODIFIABLE,"Auction {0} is not modifiable");
		ErrorMessageBundle.setDefaultMessage(AUCTION_STATUS_CHANGE_NOT_ALLOWED,"Status change from {0} to {1} is not allowed");
		ErrorMessageBundle.setDefaultMessage(NO_AUCTION_LISTING_ERROR,"No listing is provided for Auction");
		ErrorMessageBundle.setDefaultMessage(AUTOBID_END_DATE_AFTER_AUCTION_END_DATE,"Autobid end date is before Auction end date");
		ErrorMessageBundle.setDefaultMessage(AUTOBID_BID_INCR_LT_AUCTION_BID_INCR,"Autobid bid increment is less than Auction bid increment");
		ErrorMessageBundle.setDefaultMessage(INVALID_USER_FOR_SETTING_MANUAL_WINNERS,"Illegal operation, only seller who created auction can set manual winners");
		ErrorMessageBundle.setDefaultMessage(INVALID_AUCTION_WINNER,"Winner is invalid for auction {0}");
		ErrorMessageBundle.setDefaultMessage(AUCTION_WINNER_COUNT_MISMATCH,"Winner count does not match with required auction winners count");
		ErrorMessageBundle.setDefaultMessage(INVALID_SEARCH,"Search filter {0} is invalid or not allowed");
		ErrorMessageBundle.setDefaultMessage(QUOTATION_EXPIRATION_ERROR,"Quotation has expired");
		ErrorMessageBundle.setDefaultMessage(MULTIPLE_WINNER_PER_LISTING_ERROR,"Only one winner per listing is allowed for fixed Quantity Auctions");
		ErrorMessageBundle.setDefaultMessage(WINNER_ALLOCATED_QUANTITY_MISMATCH_ERROR,"Winners allocated quantity {0} exceeds the listing quantity {1}");

		ErrorMessageBundle.setDefaultMessage(ONLY_ONE_RELISTING_ALLOWED_VIOLATION,"Only one relisting is allowed for a Non Finalized auction");
		ErrorMessageBundle.setDefaultMessage(ALREADY_RELISTED_ERROR,"Referenced Non Finalized auction has already been relisted. Relisting can be done at most once");
		ErrorMessageBundle.setDefaultMessage(RELISTING_TIME_EXPIRATION_ERROR,"Relisting time has expired. Relisting must be done in {0} days, ");
		ErrorMessageBundle.setDefaultMessage(ONLY_NON_FINALIZED_AUCTION_RELISTING_ALLOWED_VIOLATION,"Only Non finalized auction can be relisted");
		ErrorMessageBundle.setDefaultMessage(NO_POSITIVE_FEEDBACK_MAINTAINED,"No postive feedback message maintained by Seller {0}");
		ErrorMessageBundle.setDefaultMessage(NO_AUCTION_WINNER_ERROR,"No Winner exists for Auction");		
	}
	/**
	 * @param arg0
	 */
	public BOUserException(Throwable arg0) {
		super(arg0);
		// TODO Auto-generated constructor stub
	}

	/**
	 * @param arg0
	 */
	public BOUserException(I18nErrorMessage arg0) {
		super(arg0);
		// TODO Auto-generated constructor stub
	}

	/**
	 * @param arg0
	 * @param arg1
	 */
	public BOUserException(I18nErrorMessage arg0, Throwable arg1) {
		super(arg0, arg1);
		// TODO Auto-generated constructor stub
	}

	/**
	 * @param arg0
	 * @param arg1
	 * @param arg2
	 * @param arg3
	 * @param arg4
	 */
	public BOUserException(
		Category arg0,
		int arg1,
		Location arg2,
		I18nErrorMessage arg3,
		Throwable arg4) {
		super(arg0, arg1, arg2, arg3, arg4);
		// TODO Auto-generated constructor stub
	}

}
