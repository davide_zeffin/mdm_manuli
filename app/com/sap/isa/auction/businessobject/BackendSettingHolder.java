/*
 * Title:        eBay Integration Project
 * Description:	 * SAP Labs LLC, the eBay auction project, also is known as Selling via eBay. 
 * Selling via eBay application provides SAP R/3 (and in the future CRM) 
 * customers to auction and sell inventory/products onto an online marketplace 
 * eBay. The application will provide the ability to Create, Schedule, Publish, 
 * Monitor and Close Auction Listings onto eBay and handle all the relevant 
 * Backend processing for Customers and Orders. 

 * Copyright:    Copyright (c) 2003
 * Company:      SAP Labs LLC
 * @author
 * @version 1.0
 */
package com.sap.isa.auction.businessobject;

import java.util.Locale;

import com.sap.isa.auction.backend.boi.SettingHolderBackend;
import com.sap.isa.auction.bean.BackendDefaultSettings;
import com.sap.isa.businessobject.BusinessObjectBase;
import com.sap.isa.businessobject.BusinessObjectHelper;
import com.sap.isa.businessobject.CommunicationException;
import com.sap.isa.core.businessobject.BackendAware;
import com.sap.isa.core.eai.BackendException;
import com.sap.isa.core.eai.BackendObjectManager;
import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.core.util.table.Table;

/**
 * @author i009657
 *
 * To change the template for this generated type comment go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
public class BackendSettingHolder
	extends BusinessObjectBase
	implements BackendAware {
	protected SettingHolderBackend backendService;

	private BackendObjectManager bem;

	private static IsaLocation log =
		IsaLocation.getInstance(BackendSettingHolder.class.getName());

	/**
	 * 
	 */
	public BackendSettingHolder() {
		super();
	}

	/**
	 * Do the initialization, retrieve all the shop information
	 * all the product catgory
	 * sales organization stuff is bundled, the shopdata is a lean version.
	 *
	 */
	public boolean initialize(BackendDefaultSettings setting)
		throws CommunicationException {
		boolean ret = false;
		try {
			// read and lock from Backend
			ret =
				((SettingHolderBackend) getBackendService()).initialize(
					setting);
		} catch (BackendException ex) {
			log.error("During the initlization errors occur ", ex);
			BusinessObjectHelper.splitException(ex);
		}
		return ret;
	}

	/**
	 * Sets the BackendObjectManager for the shop. This method is used
	 * by the object manager to allow the user object interaction with
	 * the backend logic. This method is normally not called
	 * by classes other than BusinessObjectManager.
	 *
	 * @param bem BackendObjectManager to be used
	 */
	public void setBackendObjectManager(BackendObjectManager bem) {
		this.bem = bem;
	}

	/**
	 * @param	language
	 * @return	Table contains all the help values for sales doc type
	 * 
	 */
	public Table getSalesDocTypes(String language)
		throws CommunicationException {
		Table allVal = null;
		try{
			allVal = ((SettingHolderBackend) getBackendService()).getSalesDocType(language);
		}catch(BackendException ex){
			log.error("Error in retrieving the sales doc type", ex);
			BusinessObjectHelper.splitException(ex);	
		}
		return allVal;			
	}	
	
	/**
	 * @param	language
	 * @return	Table contains all the help values for catalog ID
	 * 
	 */
	public Table getCatalogIdList(String language)
		throws CommunicationException {
		Table allVal = null;
		try{
			allVal = ((SettingHolderBackend) getBackendService()).getCatalogList(language);
		}catch(BackendException ex){
			log.error("Error in retrieving the catalog Id list", ex);
			BusinessObjectHelper.splitException(ex);	
		}
		return allVal;			
	}

	/**
	 * @param	language
	 * @return	Table contains all the help values for shipping conditions
	 * 
	 */
	public Table getShippingConditionList(String language)
		throws CommunicationException {
		Table allVal = null;
		try{
			allVal = ((SettingHolderBackend) getBackendService()).getShippingConditionList(language);
		}catch(BackendException ex){
			log.error("Error in retrieving the catalog Id list", ex);
			BusinessObjectHelper.splitException(ex);	
		}
		return allVal;			
	}	
	
	/**
	 * @param	language
	 * @param 	The catalog ID
	 * @return	Table contains all the help values for shipping conditions
	 * 
	 */
	public Table getCatalogVariant(String language,
									String catalogId)
		throws CommunicationException {
		Table allVal = null;
		try{
			allVal = ((SettingHolderBackend) getBackendService()).
						getCatalogVariantList(language, catalogId);
		}catch(BackendException ex){
			log.error("Error in retrieving the catalog variant list", ex);
			BusinessObjectHelper.splitException(ex);	
		}
		return allVal;			
	}	
	
	/**
	 *
	 */
	private SettingHolderBackend getBackendService() throws BackendException {
		synchronized (this) {
			if (backendService == null) {
				try {
					// get the Backend from the Backend Manager
					backendService =
						(SettingHolderBackend) bem.createBackendBusinessObject(
							"BackendSetting",
							null);
				} catch (Exception ex) {
					log.debug("Exception occured in getBackendService():", ex);
					throw (new BackendException(ex));
				}
			}
		}
		return backendService;
	}
	
	public Locale getBackendLocale() {
		try {
			return 	getBackendService().getBackendLocale();
		} catch (BackendException e) {
			return Locale.US;
		}
	}

}
