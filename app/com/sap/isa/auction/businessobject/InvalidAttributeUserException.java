
package com.sap.isa.auction.businessobject;

import com.sap.isa.auction.exception.i18n.ErrorMessageBundle;
import com.sap.isa.auction.exception.i18n.I18nErrorMessage;
import com.sap.tc.logging.Category;
import com.sap.tc.logging.Location;


public class InvalidAttributeUserException extends BOUserException {
	private static final String EXC_ID = "InvalidAttributeUserException";
	
	public static final String SALESORG_IS_NULL = EXC_ID + 301;
	public static final String DIVISION_IS_NULL = EXC_ID + 302;
	public static final String DISCHANNEL_IS_NULL = EXC_ID + 303;
	public static final String SALESDOC_TYPE_IS_NULL = EXC_ID + 304;
	public static final String ONE_TIME_CUSTOMER_IS_NULL = EXC_ID + 305;
	public static final String QUOTATION_IS_NULL = EXC_ID + 306;
	public static final String WEBSHOP_IS_NULL = EXC_ID + 307;
	public static final String WEBSHOP_INVALID = EXC_ID + 308;
	public static final String QUOTATION_ID_IS_NULL = EXC_ID + 309;
	public static final String SOLDTO_PARTY_IS_NULL = EXC_ID + 310;
	public static final String ORDER_PROCESS_TYPE_IS_NULL = EXC_ID + 311;
	public static final String QUOTE_PROCESS_TYPE_IS_NULL = EXC_ID + 311;
	public static final String PRICE_CONDITION_TYPE_IS_NULL = EXC_ID + 312;
	static {
		ErrorMessageBundle.setDefaultMessage(SALESORG_IS_NULL,
											"Sales Organization field not found");			
		ErrorMessageBundle.setDefaultMessage(DIVISION_IS_NULL,
											"Division not found");
		ErrorMessageBundle.setDefaultMessage(DISCHANNEL_IS_NULL,	
											"Sales channel not found");
		ErrorMessageBundle.setDefaultMessage(SALESDOC_TYPE_IS_NULL,
											"Sales Document type not found");
		ErrorMessageBundle.setDefaultMessage(ONE_TIME_CUSTOMER_IS_NULL,
											"One customer not found");
		ErrorMessageBundle.setDefaultMessage(QUOTATION_IS_NULL,
											"Quotation Object not initilized");
		ErrorMessageBundle.setDefaultMessage(WEBSHOP_IS_NULL,
											"Web Shop is null");
		ErrorMessageBundle.setDefaultMessage(WEBSHOP_INVALID,
											"Web Shop is invalid");																						
		ErrorMessageBundle.setDefaultMessage(QUOTATION_ID_IS_NULL,
											"Quotation ID is Null");
		ErrorMessageBundle.setDefaultMessage(SOLDTO_PARTY_IS_NULL,
											"SoldTo Party is Null");
		ErrorMessageBundle.setDefaultMessage(ORDER_PROCESS_TYPE_IS_NULL,
											"Order Process Type is Null");
		ErrorMessageBundle.setDefaultMessage(QUOTE_PROCESS_TYPE_IS_NULL,
											"Quotation Process Type is Null");																							
		ErrorMessageBundle.setDefaultMessage(PRICE_CONDITION_TYPE_IS_NULL,
											"Price Condition Type is not defined");											
	}
	/**
	 * @param arg0
	 */
	public InvalidAttributeUserException(Throwable arg0) {
		super(arg0);
		// TODO Auto-generated constructor stub
	}

	/**
	 * @param arg0
	 */
	public InvalidAttributeUserException(I18nErrorMessage arg0) {
		super(arg0);
		// TODO Auto-generated constructor stub
	}

	/**
	 * @param arg0
	 * @param arg1
	 */
	public InvalidAttributeUserException(
		I18nErrorMessage arg0,
		Throwable arg1) {
		super(arg0, arg1);
		// TODO Auto-generated constructor stub
	}

	/**
	 * @param arg0
	 * @param arg1
	 * @param arg2
	 * @param arg3
	 * @param arg4
	 */
	public InvalidAttributeUserException(
		Category arg0,
		int arg1,
		Location arg2,
		I18nErrorMessage arg3,
		Throwable arg4) {
		super(arg0, arg1, arg2, arg3, arg4);
		// TODO Auto-generated constructor stub
	}

}
