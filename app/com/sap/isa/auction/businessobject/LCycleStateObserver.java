package com.sap.isa.auction.businessobject;

/**
 * Title:        Auction
 * Description: Any instance interested in listening to Application Life Cycle states
 * should implement this interface and register centrally with AuctionRuntime
 *
 * Copyright:    Copyright (c) 2003
 * Company:
 * @author e-auctions 1.0
 * @version 1.0
 */

public interface LCycleStateObserver {

    void initialize() throws Exception;
    void destroy();
}