package com.sap.isa.auction.businessobject;

import com.sap.isa.auction.logging.CategoryProvider;
import com.sap.isa.auction.sat.SATSupport;
import com.sap.isa.auction.util.PropertyContainer;
import com.sap.isa.core.ContextConst;
import com.sap.isa.user.businessobject.UserBase;
import com.sap.tc.logging.Category;
import com.sap.tc.logging.Location;
import com.sap.util.monitor.jarm.IMonitor;

/**
 * Title:        Internet Sales
 * Description: Invocation Context propagates the calling context from UI layer to
 *              business logic layer. It is the responsibility of the UI Layer to
 *              bind the Invocation Context with executing thread before invoking
 *              any logic layer functionality
 *              It stores attributes for
 *              User
 *              Security
 *              Dynamic Properties
 * Copyright:    Copyright (c) 2002
 * Company:      SAPMarkets
 * @author
 * @version 1.0
 */

public class InvocationContext extends PropertyContainer
                            implements java.io.Serializable, 
                            			ContextConst,
                            			SATSupport {

	protected static final Category logger = CategoryProvider.getBOLCategory();
	protected Location tracer = Location.getLocation(InvocationContext.class.getName());
    
	protected UserBase user;
	
	private boolean sapBkendFilter = true;
	private boolean ebaySiteFilter = true;
	private String shopId;
	private IMonitor satMonitor = null;
	
    // global context data is kept as static data and set during initialization
    private static PropertyContainer globalData = new PropertyContainer();

    public InvocationContext() {
    }

    public static void setGlobalProperty(String name, Object value) {
        globalData.addProperty(name,value);
    }

    public static Object getGlobalProperty(String name) {
        return globalData.getProperty(name);
    }

    /**
     * User
     */
    public UserBase getUser() {
        return user;
    }

    /**
     * User who's action triggered the Request
     */
    public void setUser(UserBase user) {
        this.user = user;
    }
	
	/*
	 * Following methods indicates the nature of the Operation performed
	 * through the invocation context. BOL internally filter the business
	 * data based on this. Most of API calls know the nature of operation
	 * for some calls, it may not be possible to distinguish unless explicitly
	 * specified by the UI layer
	 */
	public void setSAPSystemSpecific(boolean val) {
		this.sapBkendFilter = val;
	}

	public boolean isSAPSystemSpecific() {
		return sapBkendFilter;	
	}
	
	public void setEBaySystemSpecific(boolean val) {
		this.ebaySiteFilter = val;
	}
	
	public boolean isEBaySystemSpecific() {
		return this.ebaySiteFilter;
	}
	/**
	 * @return
	 */
	public String getShopId() {
		return shopId;
	}
	/**
	 * @param string
	 */
	public void setShopId(String string) {
		shopId = string;
	}
	
	public boolean isSATOn() {
		return this.satMonitor != null && satMonitor.getTraceSwitch();
	}
	
	public void setRequestMonitor(IMonitor monitor) {
		this.satMonitor = monitor;
	}
	
	public IMonitor getRequestMonitor() {
		return satMonitor;
	}
	
}