package com.sap.isa.auction.businessobject;

import java.util.Map;

/**
 * <p>
 * Preferences are stored as simple name value pairs. To provide extensibility,
 * Interpretation of these preferences is kept upto the Layer storing these preferences
 * for the User. Keys are checked against duplicacy for a Particular user and group.
 * </p>
 * <p>
 * Preference Manager is not expected to store any security sensitive
 * information about User.
 * </p>
 *
 * Copyright SAP Labs Palo Alto, LLC, All rights reserved
 * @version 1.0
 */

public interface UserPreferenceManager extends AuctionBusinessObject {
	/**
	 * This is the key name for the default group. Do not modify it else old data will be lost
	 */
	public static final String DEFAULT_GROUP = "defaultGroup";
	
    /**
     * @return java.util.Map map of named preferences to values
     */
    public Map getMyPreferences();

    /**
     * requires admin access privilege
     */
    public Map getPreferences(String userId);

    /**
     * @return Object preference value
     */
    public Object getMyPreference(String groupName, String name);
    
    /**
     * @param groupName if null, default group is returned 
     * @author I802891
     */
    public Map getMyGroupPreferences(String groupName);

    /**
     * <p>
     * User info is retreived from the Inovacation Context
     * Preference Names are unique for a given user
     * </p>
     * @param groupName name of the group (optional)
     * @param name name of the Preference e.g. DefaultShippingAddress, PostLoginScreen
     * @param value value of the preference. Strings and java native type wrappers are handled specially.
     *                                       All other values must be Serializable
     */
    public void addMyPreference(String groupName, String name, Object value);

	/**
	 * <p>
	 * User info is retreived from the Inovacation Context
	 * Preference Names are unique for a given user
	 * </p>
	 * @param name name of the Preference e.g. DefaultShippingAddress, PostLoginScreen
	 * @param value value of the preference. Strings and java native type wrappers are handled specially.
	 *                                       All other values must be Serializable
	 */
	public void addMyPreferences(String groupName, Map prefs);

	/**
	 * Removes an existing preference
	 * @return Object value associated with the preference
	 */
	public void removeMyGroupPreferences(String[] groupNames);
	
	/**
	 * Removes an existing preference
	 * @return Object value associated with the preference
	 */
	public void removeMyPreferences(String groupName, String[] names);
}