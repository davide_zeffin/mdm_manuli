/*****************************************************************************
    Class:        GenericBusinessObjectManager
    Copyright (c) 2001, SAPMarkets Europe GmbH, Germany, All rights reserved.
    Author:       SAP AG
    Created:      23.7.2001
    Version:      1.0

    $Revision: #5 $
    $Date: 2001/07/26 $
*****************************************************************************/

package com.sap.isa.auction.businessobject;

import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import com.sap.tc.logging.Category;
import com.sap.tc.logging.Location;
import com.sap.isa.auction.logging.CategoryProvider;
import com.sap.isa.businessobject.Search;
import com.sap.isa.businessobject.SearchCommand;
import com.sap.isa.core.businessobject.BackendAware;
import com.sap.isa.core.businessobject.ObjectBase;
import com.sap.isa.core.businessobject.management.BOManager;
import com.sap.isa.core.businessobject.management.DefaultBusinessObjectManager;
import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.user.businessobject.UserBase;

/**
 * Superclass for the easy implementation of business object managers. You
 * have to create a subclass of this class and implement type safe creation
 * methods for the business objects.
 *
 * @author SAP AG
 * @version 1.0
 */
public class AuctionGenericBusinessObjectManager extends DefaultBusinessObjectManager
         implements BOManager, BackendAware {

	private static final Category logger = CategoryProvider.getBOLCategory();
	private static final Location tracer = Location.getLocation(AuctionGenericBusinessObjectManager.class.getName());

    protected Map createdObjects;               // Map to store all created objects
    
	protected static final String DEFAULT_ID = "default"; 
	protected SearchCommand searchCommand;

    /**
     * Constructs a new instance of the manager providing a reference
     * to the backend object manager that is used.
     *
     * @param bem reference to the backend object manager
     */
    protected AuctionGenericBusinessObjectManager() {
        createdObjects = new HashMap();
        log = IsaLocation.getInstance(this.getClass().getName());
    }


    /**
     * Creates a business object of the given type.
     * If such an object was already created, a reference to the old object is
     * returned and no new object is created. This features gurantees that
     * only on instance per given type is available in the application.
     * If this is not what you want, because you need a limited number of
     * instances, use the indexed creation methods.<br>
     *
     * @param type type of the business object that is requested
     * @return reference to a newly created or already existing
     *         business object
     */
    protected synchronized Object createBusinessObject(Class type) {
        return createBusinessObject(type, DEFAULT_ID);
    }

	public synchronized Search createSearch()  {
		return (Search)createBusinessObject(Search.class);
	}

    /**
     * Prepares the internal map of created objects for adding an new
     * business object of the given type at the given position and
     * returns a reference to the map of objects maintained for the given
     * type.
     *
     * @param type type of the business object that is requested
     * @param index the index of the object that is requested
     */
    protected Map prepareMap(Class type) {

        Map allInstances = (Map) createdObjects.get(type);

        if (allInstances == null) {
            // No object and no index up to now
            allInstances = new HashMap();
            createdObjects.put(type, allInstances);
        }
        
        return allInstances;
    }


    /**
     * Creates a business object of the given type but allows to create more
     * than one object per given type. This is necessary to create for example
     * two orders.<br>
     * If such an object for the given index was already created, a reference
     * to the old object is returned and no new object is created.<br>
     *
     * @param type type of the business object that is requested
     * @param index the index of the object that is requested
     * @return reference to a newly created or already existing
     *         business object
     */
    protected synchronized Object createBusinessObject(Class type, String id) {
        if (log.isDebugEnabled()) {
            log.debug("createBusinessObject('"
                    + type.getName() + "'," + id + ")");
        }

        Map allInstances = prepareMap(type);

        Object instance = allInstances.get(id);

        if (instance == null) {
			instance = createInstance(type);
			allInstances.put(id, instance);
        }

        return instance;

    }

	protected synchronized Object createInstance(Class type)
	{
		Object instance;
		try {
			instance = type.newInstance();
		}
		catch (InstantiationException ex1) {
			instance = null;

			if (log.isDebugEnabled()) {
				log.debug("exception occured during creation "
						+ ex1.toString());
			}
		}
		catch (IllegalAccessException ex2) {
			instance = null;

			if (log.isDebugEnabled()) {
				log.debug("exception occured during creation "
						+ ex2.toString());
			}

		}

		assignBackendObjectManager(instance);
		creationNotification(instance);
		
		return instance;
	}
	
	
    /**
     * Returns a reference to an existing business object.
     *
     * @param type type of the business object to be returned
     * @return reference to the object or null if no such object is present
     */
    protected synchronized Object getBusinessObject(Class type) {
        return getBusinessObject(type, DEFAULT_ID);
    }

	/**
	 * 
	 * @param type
	 * @param id
	 * @return
	 */
	protected synchronized Map getBusinessObjectsByType(Class type) {
		return prepareMap(type);
	}
    /**
     * Returns a reference to an existing business object referenced by the
     * given index.
     *
     * @param type type of the business object to be returned
     * @param index the index of the object to be returned
     * @return reference to basket object or null if no such object is present
     */
    protected synchronized Object getBusinessObject(Class type, String id) {
        Object instance = null;
        Map allInstances = (Map) createdObjects.get(type);

        if (log.isDebugEnabled()) {
            log.debug("getBusinessObject('"
                    + type.getName() + "', " + id + ")");
        }

        if (allInstances != null) {
             instance = allInstances.get(id);
        }

//--        if ((paranoid) && (instance == null)) {
//--            throw new BusinessObjectNotAvailableException(
//--                "Object of type '" + type.getName()+ "' not available.");
//--        }

        return instance;
    }

	/**
	 * Creates a UserBase type user object
	 * @return reference to the UserBase businessobject
	 */
	
	public synchronized UserBase createUser()  {
		return (UserBase) createBusinessObject(UserBase.class);
	}
	
	/**
	 * Release the references to created user object.
	 */
	public synchronized void releaseUser() {
		releaseBusinessObject(UserBase.class);
	}
    /**
     * Releases the reference the bom has to an object. Please note, that
     * calling this method for a given business object means that the
     * object is not used anywhere any longer. The <code>destroy</code> method of
     * the object is called. After this the object must be considered to
     * be in an illegal state. Do not use a reference to a business object
     * after calling this method.
     *
     * @param type the type of object to be released
     * @param index the index of the object
     */
    protected synchronized void releaseBusinessObject(Class type, String id) {
        Map allInstances = (Map) createdObjects.get(type);

        if (log.isDebugEnabled()) {
            log.debug("releaseBusinessObject('"
                    + type.getName() + "', " + id + ")");
        }

        if (allInstances == null) {
            return;
        }

        Object instance = allInstances.get(id);

        if (instance != null) {
            removalNotification(instance);
            callbackDestroy(instance);
            
            allInstances.remove(id);
        }
    }

    /**
     * Releases the reference the bom has to an object. Please note, that
     * calling this method for a given business object means that the
     * object is not used anywhere any longer. The <code>destroy</code> method of
     * the object is called. After this the object must be considered to
     * be in an illegal state. Do not use a reference to a business object
     * after calling this method.
     *
     * @param type the type of object to be released
     */
    protected synchronized void releaseBusinessObject(Class type) {
        releaseBusinessObject(type, DEFAULT_ID);
    }

    /**
     * Makes all business objects invalid by calling their destroy method and
     * calls the destroy() callback on the backend object manager.
     * After calling this method you should not use any business objects or the
     * <code>BackendObjectManager</code> at all. The only thing you are allowed
     * to do after a call to this method is to throw away your reference to
     * this object.<br><br>
     * <b>
     * Normally <code>destroy()</code> is only called internally when the session
     * times out.
     * </b>
     */
    public void release() {
        Collection objectsList = createdObjects.values();
        Iterator it = objectsList.iterator();

        if (log.isDebugEnabled()) {
            log.debug("release was called for all objects");
        }

        while (it.hasNext()) {
          HashMap subList = (HashMap)it.next();
		  Collection subObjects = createdObjects.values();
		  Iterator subIt = subObjects.iterator();
		  while(subIt.hasNext()) {	
		  	Object element = subIt.next();
            // set references to the bem to null
            if (element instanceof BackendAware) {
                ((BackendAware) element).setBackendObjectManager(null);
            }

            // tell business objects to drop ressources
            if (element instanceof ObjectBase) {
                ((ObjectBase) element).destroy();
            }
          }
        }
    }


    protected void callbackDestroy(Object o) {
        if (o instanceof ObjectBase) {
            ((ObjectBase)o).destroy();
        }
    }

}