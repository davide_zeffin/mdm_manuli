/*
 * Title:        eBay Integration Project
 * Description:	 * SAP Labs LLC, the eBay auction project, also is known as Selling via eBay. 
 * Selling via eBay application provides SAP R/3 (and in the future CRM) 
 * customers to auction and sell inventory/products onto an online marketplace 
 * eBay. The application will provide the ability to Create, Schedule, Publish, 
 * Monitor and Close Auction Listings onto eBay and handle all the relevant 
 * Backend processing for Customers and Orders. 

 * Copyright:    Copyright (c) 2003
 * Company:      SAP Labs LLC
 * @author
 * @version 1.0
 */
package com.sap.isa.auction.businessobject;

import com.sap.isa.auction.backend.boi.OrganizationBackend;
import com.sap.isa.businessobject.BusinessObjectBase;
import com.sap.isa.businessobject.BusinessObjectHelper;
import com.sap.isa.businessobject.CommunicationException;
import com.sap.isa.core.TechKey;
import com.sap.isa.core.businessobject.BackendAware;
import com.sap.isa.core.eai.BackendException;
import com.sap.isa.core.eai.BackendObjectManager;
import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.core.util.table.Table;

/**
 * @author i009657
 *
 * To change the template for this generated type comment go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
public class SalesOrganization
	extends BusinessObjectBase
	implements BackendAware {

		private BackendObjectManager bem;

		private static IsaLocation log =
							IsaLocation.getInstance(SalesOrganization.class.getName());
		private OrganizationBackend backendService;
	
	/**
	 * 
	 */
	public SalesOrganization() {
		super();
	}

	/**
	 * @param arg0
	 */
	public SalesOrganization(TechKey arg0) {
		super(arg0);
	}

	/**
	 * 
	 * @param language
	 * @return		Table contains all the help values
	 * @throws CommunicationException
	 */
	public Table getSalesOrganizationList(String language)
				throws CommunicationException{
		Table allVal = null;
		try{
			allVal = ((OrganizationBackend) getBackendService()).getSalesOrganizationList(language);
		}catch(BackendException ex){
			log.error("Error in retrieving the sales org data", ex);
			BusinessObjectHelper.splitException(ex);	
		}
		return allVal;			
	}
	
	/**
	 * @param	language
	 * @return	Table contains all the help values for distribution channel
	 * 
	 */
	public Table getDistributionChannelList(String language)
		throws CommunicationException {
		Table allVal = null;
		try{
			allVal = ((OrganizationBackend) getBackendService()).getDistributionChannelList(language);
		}catch(BackendException ex){
			log.error("Error in retrieving the distribution data", ex);
			BusinessObjectHelper.splitException(ex);	
		}
		return allVal;			
	}
	
	/**
	 * @param	language
	 * @return	Table contains all the help values for distribution channel
	 * 
	 */
	public Table getDivisionList(String language)
		throws CommunicationException {
		Table allVal = null;
		try{
			allVal = ((OrganizationBackend) getBackendService()).getDivisionList(language);
		}catch(BackendException ex){
			log.error("Error in retrieving the division data", ex);
			BusinessObjectHelper.splitException(ex);	
		}
		return allVal;			
	}	

	/**
	 * Based on sales area to get the list of plant
	 * @param language
	 * @param salesOrg
	 * @param disChannel
	 * @return
	 * @throws BackendException
	 */	
	public Table getPlantList(
			String language,
			String salesOrg,
			String disChannel)
			throws CommunicationException {
		Table allVal = null;
		try{
			allVal = ((OrganizationBackend) getBackendService()).getPlantList(language,
											salesOrg, disChannel);
		}catch(BackendException ex){
			log.error("Error in retrieving the plant list data", ex);
			BusinessObjectHelper.splitException(ex);	
		}
		return allVal;					
	}
	/**
	 * Sets the BackendObjectManager for the shop. This method is used
	 * by the object manager to allow the user object interaction with
	 * the backend logic. This method is normally not called
	 * by classes other than BusinessObjectManager.
	 *
	 * @param bem BackendObjectManager to be used
	 */
	public void setBackendObjectManager (BackendObjectManager bem) {
		this.bem = bem;
	}

	/**
	 *
	 */
	private OrganizationBackend getBackendService ()
		throws BackendException {
		if (backendService == null) {
			try {
				// get the Backend from the Backend Manager
				backendService =
					(OrganizationBackend)
						bem.createBackendBusinessObject("SalesOrg",
																	null);
			} catch (Exception ex) {
				log.debug("Exception occured in getBackendService():", ex);
				throw (new BackendException(ex));
			}
		}
		return backendService;
	}

}
