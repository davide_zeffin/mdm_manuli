package com.sap.isa.auction.businessobject;

import com.sap.isa.auction.bean.Auction;
import com.sap.isa.auction.bean.Winner;

/**
 * Title:
 * Description:
 * Copyright:    Copyright (c) 2003
 * Company:
 * @author
 * @version 1.0
 */

public interface WinnerManager extends AuctionBusinessObject {
    /**
     * List of Winners for a Finalized Auction
     */
    Winner[] getWinners(Auction auction);

    /**
     * Retrieval of Winner by Id
     */
    Winner getWinner(Auction auction, String userId);
}