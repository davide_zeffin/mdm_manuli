package com.sap.isa.auction.businessobject;

import com.sap.isa.auction.bean.search.QueryFilter;

/**
 * Title:        Internet Sales
 * Description:  Web Auctions
 * Copyright:    Copyright (c) 2002, All rights reserved
 * Company:      SAP Labs, PA
 * @author e-auctions
 * @version 0.1
 */

public class AuctionQueryFilter extends QueryFilter implements AuctionSearchAttributes {

    public AuctionQueryFilter() {
    }

    public AuctionQueryFilter(String name) {
        super(name);
    }

    //////////////////////////////////////////////

    /**
     * Validates against Auction search attributes
     */
    protected void validateSearchAttr(String attrName) {
        String[] names = ATTRIBUTES;
        boolean valid = false;
        int i = 0;
        while(!valid && i < names.length) {
            if(attrName.equalsIgnoreCase(names[i++])) {
                valid = true;
            }
        }
        if(!valid) throw new IllegalArgumentException(attrName + " is not a valid search attribute");
    }
	/* (non-Javadoc)
	 * @see com.sap.isa.auction.bean.search.QueryFilter#checkSupported(java.lang.String, short)
	 */
	protected boolean checkSupported(String attrName, short op) {
		switch(op) {
			case LikeCriteria.OP :
				if(attrName.equals(NAME) || attrName.equals(PRODUCT_ID)
					|| attrName.equals(TITLE) || attrName.equals(ORDER_ID)) return true;
				else return false;
			default: return super.checkSupported(attrName,op);
		}
	}
	//////////////////////////////////////////////
	// Only very limited criterias are supported
	/**
	 * Supported only for Name and Product ID
	 * Like Criteria Name like 'SAP*', Wild Card Search
	 */
	/*public void addLikeCriteria(String name, Object value) {
		if(!(name.equals(NAME) || name.equals(PRODUCT_ID))) {
			I18nErrorMessage msg = ErrorMessageBundle.getInstance().newI18nErrorMessage(BOUserException.INVALID_SEARCH);
			BOUserException ex = new BOUserException(msg);
			throw ex;
		}
		super.addLikeCriteria(name,value);
	}*/

}