package com.sap.isa.auction.businessobject;


/**
 * AuctionBusinessObject 
 */

public interface AuctionBusinessObject extends ServiceAware {
    void initialize();
    void setAuctionBOM(AuctionBusinessObjectManagerBase bom);
    void close();
}