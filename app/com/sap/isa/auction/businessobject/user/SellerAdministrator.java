/*
 * Created on Sep 17, 2003
 *
 * SAP Labs LLC, the eBay auction project, also is known as Selling via eBay. 
 * Selling via eBay application provides SAP R/3 (and in the future CRM) 
 * customers to auction and sell inventory/products onto an online marketplace 
 * eBay. The application will provide the ability to Create, Schedule, Publish, 
 * Monitor and Close Auction Listings onto eBay and handle all the relevant 
 * Backend processing for Customers and Orders. 

 */
package com.sap.isa.auction.businessobject.user;

/**
 * @
 */
public class SellerAdministrator extends Seller {
	/**
	 *  just a simple constructor
	 */
	public SellerAdministrator() {
		super();
	}


	/**
	 * with this constructor the userId and
	 * the password are set in the user object
	 *
	 * @param userid of the internet user-- su01 user
	 * @param password of the internet user
	 *
	 */
	public SellerAdministrator(String userId, String password) {
		super(userId, password);
	}

	/**
	 * with this constructor the userId,
	 * the password and the language
	 * are set in the user object
	 *
	 * @param userid of the internet user
	 * @param password of the internet user
	 * @param language of the internet user
	 *
	 */
	public SellerAdministrator(String userId, String password, String language) {
		super(userId, password, language);
	}

	/**
	 * @return the boolean to determine if is a Administrator
	 */
	public boolean isAdministrator() {
		return true;
	}

	/**
	 * Get the reference of the backend service and creates it when necessary
	 * this protected method can be used by inherited classes to get
	 * the backend service
	 *
	 * @return the "typeless" backend business object
	 *
	 */
//	protected BackendBusinessObject createBackendService()
//		throws BackendException {
//
//		if (backendService == null) {
//			backendService = (UserBaseBackend) bem.createBackendBusinessObject("Administrator", null);
//		}
//		return backendService;
//	}
}
