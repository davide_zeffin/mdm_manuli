/*****************************************************************************
    Copyright (c) 2001, SAPMarkets Palo Alto, USA, All rights reserved.

*****************************************************************************/

package com.sap.isa.auction.businessobject.user;

import com.sap.isa.auction.backend.BackendTypeConstants;
import com.sap.isa.auction.backend.boi.user.AuctionUserBackend;
import com.sap.isa.auction.backend.boi.user.AuctionUserData;
import com.sap.isa.businessobject.BusinessObjectBase;
import com.sap.isa.core.businessobject.BackendAware;
import com.sap.isa.core.eai.BackendException;
import com.sap.isa.core.eai.BackendObjectManager;
import com.sap.isa.core.logging.IsaLocation;

public class AuctionUser extends BusinessObjectBase
implements AuctionUserData,BackendAware{

    private BackendObjectManager bem;
    private AuctionUserBackend backendService;

    private String timeZone;
    private String dateFormat;
    private String decimalFormat;
    private String decimalSeparator;
    private String groupingSeparator;
    private String lastName;
    private String firstName;
    private String salutationTxt;
    private String emailAddress;

    private static IsaLocation log = IsaLocation.getInstance(AuctionUser.class.getName());

    public AuctionUser() {
    }


    public String getTimeZone() {
        return timeZone;
    }

    public String getDateFormat() {
        return dateFormat;
    }

    public String getDecimalFormat() {
        return decimalFormat;
    }

    public String getDecimalSeparator() {
        return decimalSeparator;
    }

    public String getGroupingSeparator() {
        return groupingSeparator;
    }

    public void setTimeZone(String tz) {
        timeZone = tz;
    }

    public void setDateFormat(String df) {
        dateFormat = df;
    }

    public void setDecimalFormat(String decf) {
        decimalFormat = decf;
    }

    public void setDecimalSeparator(String ds) {
        decimalSeparator = ds;
    }

    public void setGroupingSeparator(String gs) {
        groupingSeparator = gs;
    }

    /**
     * Set the user defaults such as timezone, decimal format,
     * date format based on crm User id
     */
    public void setUserDefaults(String userid) {
        try {
            getBackendService().getUserDefaults(this,userid);

        }
        catch(BackendException ex) {
            if(log.isDebugEnabled())
                log.debug(ex);
        }

    }

    /**
     * Method the BOM calls after a new object is created
     *
     * @param bem reference to the BackendObjectManager object
     */

    public void setBackendObjectManager (BackendObjectManager bem){
        this.bem = bem;
    }

    /**
     * Get the BackendService, if necessary
     */
    private AuctionUserBackend getBackendService() throws BackendException {

        if (backendService == null) {
            // get the Backend from the Backend Object Manager
            backendService = (AuctionUserBackend)bem.createBackendBusinessObject(BackendTypeConstants.BO_TYPE_AUCTION_USER,null);
        }
        return backendService;
    }


	/* (non-Javadoc)
	 * @see com.sap.isa.auction.backend.boi.user.AuctionUserData#getLastName()
	 */
	public String getLastName() {
		return lastName;
	}


	/* (non-Javadoc)
	 * @see com.sap.isa.auction.backend.boi.user.AuctionUserData#getFirstName()
	 */
	public String getFirstName() {
		return firstName;
	}


	/* (non-Javadoc)
	 * @see com.sap.isa.auction.backend.boi.user.AuctionUserData#setLastName(java.lang.String)
	 */
	public void setLastName(String lastName) {
		this.lastName = lastName;
		
	}


	/* (non-Javadoc)
	 * @see com.sap.isa.auction.backend.boi.user.AuctionUserData#setFirstName(java.lang.String)
	 */
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}


	/* (non-Javadoc)
	 * @see com.sap.isa.auction.backend.boi.user.AuctionUserData#setSalutationText(java.lang.String)
	 */
	public void setSalutationText(String salutationTxt) {
		this.salutationTxt = salutationTxt;
	}


	/* (non-Javadoc)
	 * @see com.sap.isa.auction.backend.boi.user.AuctionUserData#getSalutationTxt()
	 */
	public String getSalutationTxt() {
		return salutationTxt;
	}
	
	public void setEMailAddress(String address)  {
		this.emailAddress = address;
	}


	/* (non-Javadoc)
	 * @see com.sap.isa.auction.backend.boi.user.AuctionUserData#getEMailAddress()
	 */
	public String getEMailAddress() {
		return emailAddress;
	}
}

