/*
 * Created on Sep 17, 2003
 *
 * SAP Labs LLC, the eBay auction project, also is known as Selling via eBay. 
 * Selling via eBay application provides SAP R/3 (and in the future CRM) 
 * customers to auction and sell inventory/products onto an online marketplace 
 * eBay. The application will provide the ability to Create, Schedule, Publish, 
 * Monitor and Close Auction Listings onto eBay and handle all the relevant 
 * Backend processing for Customers and Orders. 

 */
package com.sap.isa.auction.businessobject.user;

import java.util.Enumeration;

import com.sap.isa.auction.backend.boi.businesspartner.ResponsibleSalesEmployeeData;
import com.sap.isa.auction.backend.boi.user.AuctionUserBackend;
import com.sap.isa.auction.backend.boi.user.AuctionUserBaseData;
import com.sap.isa.businessobject.User;
import com.sap.isa.auction.bean.SAPBackendSystemEnum;
import com.sap.isa.auction.businessobject.businesspartner.ResponsibleEmployee;
import com.sap.isa.core.eai.BackendBusinessObject;
import com.sap.isa.core.eai.BackendException;
import com.sap.isa.user.backend.boi.UserBaseBackend;
/**
 * @This class is used for the seller side administration
 */
public class Seller extends User implements AuctionUserBaseData {

	/**
	 * is administrator role
	 */
	private boolean isAdmin = false;
	private Enumeration allProfiles = null;
	private ResponsibleEmployee employee = null;
	private String checkingProfileName = null;
	private String client = null;
	private String systemId = null;
	/**
	 *  just a simple constructor
	 */
	public Seller() {
		super();
		employee = new ResponsibleEmployee();
	}


	/**
	 * with this constructor the userId and
	 * the password are set in the user object
	 *
	 * @param userid of the internet user-- su01 user
	 * @param password of the internet user
	 *
	 */
	public Seller(String userId, String password) {
		super(userId, password);
		if (employee == null){
			employee = new ResponsibleEmployee();			
		}
	}

	/**
	 * with this constructor the userId,
	 * the password and the language
	 * are set in the user object
	 *
	 * @param userid of the internet user
	 * @param password of the internet user
	 * @param language of the internet user
	 *
	 */
	public Seller(String userId, String password, String language) {
		super(userId, password, language);
		if (employee == null){
			employee = new ResponsibleEmployee();			
		}		
	}

	/**
	 * @return the boolean to determine if is a Administrator
	 */
	public boolean isAdministrator() {
		return isAdmin;
	}
	/*
	 * @retrun the SAP backend system type, like: CRM or R3
	 * 	
	 */
	public SAPBackendSystemEnum getSAPBackendSystemType() throws BackendException{
		boolean isR3 = true;
		BackendBusinessObject backendService;
		backendService = createBackendService();
		SAPBackendSystemEnum type = null;
		if (backendService instanceof com.sap.isa.auction.backend.r3.user.AuctionUserR3)
			type = SAPBackendSystemEnum.R3;
		else if (backendService instanceof com.sap.isa.auction.backend.crm.user.AuctionSellerCRM)
			type = SAPBackendSystemEnum.CRM;
		return type;
	}

	/**
	 * @return a collection of all the User Profiles
	 */
	public Enumeration getUserRoles() {
		return allProfiles;
	}

	/**
	 * @param the sales employee data
	 */
	public void setSalesEmployee(ResponsibleSalesEmployeeData salesEmployee) {
		employee = (ResponsibleEmployee)salesEmployee;
	}

	/**
	 * @return the sales employee data
	 */
	public ResponsibleSalesEmployeeData getSalesEmployee() {
		return employee;
	}
	
	/**
	 * Get the reference of the backend service and creates it when necessary
	 * this protected method can be used by inherited classes to get
	 * the backend service
	 *
	 * @return the "typeless" backend business object
	 *
	 */
	protected BackendBusinessObject createBackendService()
		throws BackendException {

		if (backendService == null) {
			backendService = (UserBaseBackend) bem.createBackendBusinessObject("Seller", null);
		}
		return backendService;
	}

	/**
	 * this is read from the backed configuration file
	 * @return
	 */
	public String getCheckingProfileName() {
		return checkingProfileName;
	}

	/**
	 * set from the backedn configuration file
	 * @param string
	 */
	public void setCheckingProfileName(String string) {
		checkingProfileName = string;
	}


    /* (non-Javadoc)
     * @see com.sap.isa.auction.backend.boi.user.AuctionUserBaseData#getClient()
     */
    public String getClient() {
		if(client!=null) return client;
        try {
			return ((AuctionUserBackend)createBackendService()).getClient();
		} catch (BackendException e) {
			return null;
		}
    }

    /* (non-Javadoc)
     * @see com.sap.isa.auction.backend.boi.user.AuctionUserBaseData#getSystemId()
     */
    public String getSystemId() {
        if(systemId!=null) return systemId;
		try {
			return ((AuctionUserBackend)createBackendService()).getSystemId();
		} catch (BackendException e) {
			return null;
		}
    }
	public void setSystemId(String systemId){
		this.systemId=  systemId;
	}
	public void setClient(String client){
		this.client=  client;
	}


	/* 
	 * Sets the administrator functionality
	 * @see com.sap.isa.auction.backend.boi.user.AuctionUserBaseData#setAdministrator(boolean)
	 */
	public void setAdministrator(boolean admin) {
		isAdmin = admin;
	}

}
