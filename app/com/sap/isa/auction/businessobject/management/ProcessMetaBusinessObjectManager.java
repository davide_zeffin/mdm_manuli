package com.sap.isa.auction.businessobject.management;

/**
 * Title:        eBay Integration Project
 * Description:
 * Copyright:    Copyright (c) 2003
 * Company:      SAP Labs LLC
 * @author
 * @version 1.0
 */

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import com.sap.isa.auction.businessobject.ServiceAware;
import com.sap.isa.auction.config.XCMConfigContainer;
import com.sap.isa.auction.exception.InvalidStateException;
import com.sap.isa.auction.logging.CategoryProvider;
import com.sap.isa.auction.services.ServiceLocator;
import com.sap.isa.backend.IsaBackendBusinessObjectBaseSAP;
import com.sap.isa.core.SharedConst;
import com.sap.isa.core.businessobject.BOLifetimeObserver;
import com.sap.isa.core.businessobject.BackendAware;
import com.sap.isa.core.businessobject.event.BusinessObjectCreationEvent;
import com.sap.isa.core.businessobject.event.BusinessObjectCreationListener;
import com.sap.isa.core.businessobject.event.BusinessObjectRemovalEvent;
import com.sap.isa.core.businessobject.event.BusinessObjectRemovalListener;
import com.sap.isa.core.businessobject.management.BOLifetimeAware;
import com.sap.isa.core.businessobject.management.BOMInstantiationException;
import com.sap.isa.core.businessobject.management.BOManager;
import com.sap.isa.core.businessobject.management.MetaBOMConfigReader;
import com.sap.isa.core.eai.BackendObjectManagerImpl;
import com.sap.isa.core.init.InitializeException;
import com.sap.isa.core.xcm.ConfigContainer;
import com.sap.isa.core.xcm.scenario.ScenarioManager;
import com.sap.tc.logging.Category;
import com.sap.tc.logging.Location;
import com.sap.tc.logging.Severity;

 /**
  * This is a singlton class which manages all of the instance of business
  * object managers, it is not sesion dependent.
  */
public class ProcessMetaBusinessObjectManager {

	private static final Category logger = CategoryProvider.getBOLCategory();
	private static final Location tracer = Location.getLocation(ProcessMetaBusinessObjectManager.class.getName());

  /**
   * This HashMap keeps the index with a processbusinessobjectmanager
   */
  
  private static HashMap indexMapper;
//	private static ProcessMetaBusinessObjectManager manager;
  public static final int INDEX_MAPPER_SIZE = 5;

  private static final String INSTANTIATION_ERROR_KEY =
											   "system.bom.instantiation.error";
  private String scenarioName;
  private static HashMap scenarios2pboms = new HashMap();
  private BackendObjectManagerImpl bem;

  private HashMap knownBOMs;

  private List creationListener;
  private List removalListener;

  private Properties props;

  private BOMsEventHandling eventHandling;

  private MetaBOMConfigReader reader;
	/**
	 * In the application context
	 */
	private ProcessMetaBusinessObjectManager(Properties props) {
		indexMapper = new HashMap(INDEX_MAPPER_SIZE);
		knownBOMs = new HashMap();
		this.props     = props;
		creationListener = new ArrayList();
		removalListener  = new ArrayList();
		eventHandling = new BOMsEventHandling();
	}


	public static ProcessMetaBusinessObjectManager getInstance(String scenarioName) throws InitializeException {
		synchronized(ProcessMetaBusinessObjectManager.class) {
			ProcessMetaBusinessObjectManager pbom = (ProcessMetaBusinessObjectManager)scenarios2pboms.get(scenarioName);
			if(pbom==null) {
				pbom = createPBOM(scenarioName);
				scenarios2pboms.put(scenarioName,pbom);
			}
			return pbom;
		}
	}

	private static ProcessMetaBusinessObjectManager createPBOM(String scenarioName) {
		Properties mbomProps = new Properties();
		XCMConfigContainer xcmContainer = XCMConfigContainer.getBaseInstance(scenarioName);
		ConfigContainer config = xcmContainer.getConfigContainer(); 
		mbomProps.setProperty(SharedConst.XCM_CONF_KEY, config.getConfigKey());
		ProcessMetaBusinessObjectManager metaBOM =
			new ProcessMetaBusinessObjectManager(mbomProps);
			
		BackendObjectManagerImpl bm = new BackendObjectManagerImpl(mbomProps);
		String key =config.getConfigParams().getParamValue("jcodata");
		Map params = xcmContainer.getConfigParamsFromNode("jco", key, null, false);
		String lang = (String)params.get("lang");
		if(lang == null || lang.length() < 0)
			lang = "en";
		bm.setAttribute(IsaBackendBusinessObjectBaseSAP.ISA_LANGUAGE, lang);
		metaBOM.setBackEndBusinessObjectManager(bm);
		metaBOM.scenarioName = scenarioName;
		return metaBOM;		
		
	}

	/**
	 * a convinent way to get the MetaBOM, the inilization has been done in
	 * InitializationConfigHandler...
	 */
	public static ProcessMetaBusinessObjectManager getInstance()
													throws InitializeException{
		return getInstance(ScenarioManager.getScenarioManager().getDefaultScenario());
//		if (manager == null) {
//		  throw new InitializeException("The Instance does not exist");
//		}
//		return manager;
	}

//	  public static ProcessMetaBusinessObjectManager getInstance(StandaloneHandler env, Properties props) {
//		if (manager == null) {
//		  manager = new ProcessMetaBusinessObjectManager(env, props);
//		}
//		return manager;
//	  }

	private void checkNull(Object obj) throws BOMInstantiationException {

	   if (obj == null) {
		   Object[] args = new Object[1]; args[0] = "null";
		   logger.errorT(tracer,INSTANTIATION_ERROR_KEY, args);
		   throw new BOMInstantiationException(INSTANTIATION_ERROR_KEY);
	   }
	}


	/*
	 * creates and manages an instance of the BackendObjectManager
	*/
	protected BackendObjectManagerImpl getBackendObjectManager() {
	   if (bem == null)
		  bem = new BackendObjectManagerImpl(props);
	   return bem;
	}

	/**
	 * set the backend business object manager
	 */
	public void setBackEndBusinessObjectManager(BackendObjectManagerImpl bm){
	  bem = bm;
	}

	/*
	 * This method is called whenever an new BOM-instance was created
	*/
	private void newBOMIntroduction(BOManager bom) {
	   if (bom instanceof BackendAware)
		 ((BackendAware)bom).setBackendObjectManager(getBackendObjectManager());

	   if (bom instanceof BOLifetimeAware)
		 ((BOLifetimeAware)bom).setBOLifetimeObserver(eventHandling);
	}


	/*
	 *  Factory for the BOM
	 *
	 * @type listener The listener to be removed
	*/
	public synchronized BOManager getBOM(Class type)
									  throws BOMInstantiationException {
	   checkNull(type);
	   BOManager bom = (BOManager)knownBOMs.get(type);
	   if (bom == null) {
		  try {
			 bom = (BOManager)type.newInstance();
			 newBOMIntroduction(bom);
			 knownBOMs.put(type, bom);
			 logger.infoT(tracer, "system.bom.created",
							new Object[] {type.getName()});
		  } catch (Throwable ex ) {
			  logger.error(tracer, INSTANTIATION_ERROR_KEY, ex, new Object[] {type.getName()});
			  throw new BOMInstantiationException(ex.getMessage());
		  }
	   }
	   if(bom instanceof ServiceAware) {
			try {
				((ServiceAware)bom).setServiceLocator(ServiceLocator.getBaseInstance(scenarioName));
			} catch (InvalidStateException e) {
				logger.error(tracer, e);
			} catch (com.sap.isa.auction.services.InitializeException e) {
				logger.error(tracer, e);
			} 
	   }
	   return bom;
	}

	public BOManager getBOM(String bomClassName) throws BOMInstantiationException {

	   checkNull(bomClassName);

	   try {
		logger.logT(Severity.DEBUG, tracer, "DebugInfo: "+bomClassName);
		  Class type = Class.forName(bomClassName);
		  return getBOM(type);
	   } catch (ClassNotFoundException ex) {
		  logger.error(tracer, INSTANTIATION_ERROR_KEY, ex, new Object[] {bomClassName});
		  throw new BOMInstantiationException(ex.getMessage());
	   }
	}

	public BOManager getBOMbyName(String bomName) throws BOMInstantiationException {

	   checkNull(bomName);

	   String className = null;
	   try {
		  className = lookupName(bomName);
	   } catch (Throwable ex) {
		  logger.error(tracer, INSTANTIATION_ERROR_KEY, ex, new Object[] {bomName});
		  throw new BOMInstantiationException(ex.getMessage());
	   }

	   return getBOM(className);
	}

	private String lookupName(String name) {
	   return MetaBOMConfigReader.getClassName(name);
	}


	/**
	 * Callback to notify this object that
	 * it is going to be unbound completely from application context
	 *
	 */
	public void valueUnbound() {

		Object[] args = new Object[1];
		Iterator iterator = knownBOMs.keySet().iterator();

		while (iterator.hasNext()) {
			Class type = (Class)iterator.next();
			BOManager bom = (BOManager)knownBOMs.get(type);
			bom.release();

			args[0] = type.getName();
			logger.infoT(tracer, "system.bom.released", args);
		}

		if (bem != null) {
			bem.destroy();
			logger.infoT(tracer, "system.bom.destroy.bem");

			bem = null;
		}

		knownBOMs.clear();

		removalListener.clear();
		creationListener.clear();
	}



	/**
	 * Adds a new listener for the events fired by the BOM after the creation
	 * of new business objects.
	 *
	 * @param listener The listener to be registered
	 */
	public synchronized void addBusinessObjectCreationListener(BusinessObjectCreationListener listener) {
		creationListener.add(listener);
	}

	/**
	 * Adds a new listener for the events fired by the BOM after the removal
	 * of new business objects.
	 *
	 * @param listener The listener to be registered
	 */
	public synchronized void addBusinessObjectRemovalListener(BusinessObjectRemovalListener listener) {
		removalListener.add(listener);
	}

	/**
	 * Removes a listener for the events fired by the BOM after the creation
	 * of new business objects.
	 *
	 * @param listener The listener to be removed
	 */
	public synchronized void removeBusinessObjectCreationListener(BusinessObjectCreationListener listener) {
		creationListener.remove(listener);
	}

	/**
	 * Removes a listener for the events fired by the BOM after the removal
	 * of new business objects.
	 *
	 * @param listener The listener to be removed
	 */
	public synchronized void removeBusinessObjectRemovalListener(BusinessObjectRemovalListener listener) {
		removalListener.remove(listener);
	}



	private class BOMsEventHandling implements BOLifetimeObserver {


	   public synchronized void removalNotification(Object obj) {

		  BusinessObjectRemovalEvent e = new BusinessObjectRemovalEvent(obj);

		  int size = removalListener.size();

		  for (int i = size-1; i >= 0; i--) {
			((BusinessObjectRemovalListener) removalListener.get(i)).objectRemoved(e);
		  }
	   }


	   public synchronized void creationNotification(Object obj) {
		 BusinessObjectCreationEvent e = new BusinessObjectCreationEvent(obj);
		 int size = creationListener.size();

		 for (int i = size-1; i >= 0; i--) {
			((BusinessObjectCreationListener) creationListener.get(i)).objectCreated(e);
		 }
	   }

	}  // class BOMsEventHandling
	public synchronized void _close(){
		synchronized(this) {
			knownBOMs.clear();
			knownBOMs = null;
			scenarioName = null;
		}
	}

	/**
	 * Only to be used when shutting down the application
	 *
	 */
	public synchronized static void close() {
		Iterator iter = scenarios2pboms.keySet().iterator();
		while(iter.hasNext()) {
			String key = (String)iter.next();
			ProcessMetaBusinessObjectManager pbom = (ProcessMetaBusinessObjectManager) scenarios2pboms.get(key);
			pbom._close();
		}
		scenarios2pboms.clear();
		scenarios2pboms = null;
	}
}
