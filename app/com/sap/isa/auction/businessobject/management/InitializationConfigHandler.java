package com.sap.isa.auction.businessobject.management;

/**
 * Title:        eBay Integration Project
 * Description:
 * Copyright:    Copyright (c) 2003
 * Company:      SAP Labs LLC
 * @author
 * @version 1.0
 */
import java.util.Properties;

import com.sap.isa.core.SharedConst;
import com.sap.isa.core.init.InitializeException;
import com.sap.isa.core.util.StartupParameter;
import com.sap.isa.core.xcm.ConfigContainer;
import com.sap.isa.core.xcm.ExtendedConfigManager;
 /**
  * Do the initlization, construct all the configuration, after this class
  * you should have the access to backend, this class can be initilized via
  * standalone mode or via the servlet context
  * In normal case, the properties has been passed from Web initlization context
  * her we just use it to create MetaBOM

  */
public class InitializationConfigHandler {
    
    public static final String DEFAULT_BOOT_STRAP_INIT =
        "/WEB-INF/xcm/sap/system/bootstrap-config.xml";
    public static final String DEFAULT_INIT_CONFIG_PATH =
        "/WEB-INF/xcm/sap/system/init-config.xml";
    public static final String DEFAULT_MESSAGE_RESOURCE =
        "com.sap.isa.ISAResources";
    private String scenarioName;
    
    /**
     * The context is the context information which is retrieved from Web
     * application context, it can be obtained via application context
     *
     */
    private static String context;
    
    /**
     * The instance of InitilizationConfigHandler class
     */
    private static InitializationConfigHandler handler;
    
    
    /**
     * It save all the initlization enviroment parameters
     */
    private Properties mbomProps;
    
    /**
     * for initilization parameters
     */
    private StartupParameter startupParameter;
    
    
    /**
     * private consructor, this is the standalone version of initlization
     */
    private InitializationConfigHandler(String scenario) {
        mbomProps = new Properties();
        startupParameter = new StartupParameter();
        this.scenarioName = scenario;
//        // i am doing the standalone version here
//        StandaloneHandler standaloneHandler = new StandaloneHandler();
//        
//        standaloneHandler.setBootStrapInitConfigPath(DEFAULT_BOOT_STRAP_INIT);
//        standaloneHandler.setInitConfigPath(DEFAULT_INIT_CONFIG_PATH);
//        standaloneHandler.setClassNameMessageResources(DEFAULT_MESSAGE_RESOURCE);
//        
//        // get the root dir
//        AuctionRuntime runtime = AuctionRuntime.getRuntime();
//        ServletContext  ctxt = (ServletContext )runtime.getContext();
//        String rootPath = ctxt.getRealPath("\\");
//        
//        standaloneHandler.setBaseDir(rootPath);
//        Hashtable parameters = new Hashtable();
//        standaloneHandler.setParameter(parameters);
//        parameters.put(
//                       "customer.config.path.xcm.config.isa.sap.com",
//                       "/WEB-INF/xcm/customer/configuration");
//        parameters.put("scenario.xcm.config.isa.sap.com",
//                       scenarioName);
//        try {
//            standaloneHandler.initialize();
//        } catch (Exception ex) {
//            ex.printStackTrace();
//        }
        
        ExtendedConfigManager mgr = ExtendedConfigManager.getExtConfigManager();
        ConfigContainer config = mgr.getConfig(scenarioName);
        mbomProps.setProperty(SharedConst.XCM_CONF_KEY, config.getConfigKey());
    }
    
    /**
     * constructor
     */
    private InitializationConfigHandler(Properties prop) {
        mbomProps = prop;
    }
    
    
    /**
     * constructor
     */
    private InitializationConfigHandler(StartupParameter parameter,
                                        Properties prop) {
        mbomProps = prop;
        startupParameter = parameter;
    }
    
    
    public static InitializationConfigHandler getInstance(Properties prop)
        throws InitializeException
    {
        if (handler== null) {
            handler = new InitializationConfigHandler(prop);
        }
        return handler;
    }
    
    /**
     * The standalone version of initlization
     */
    public static InitializationConfigHandler getInstance(String scenario)
        throws InitializeException
    {
        if (handler== null) {
            handler = new InitializationConfigHandler(scenario);
        }
        return handler;
    }
    
    
    public static InitializationConfigHandler getInstance(StartupParameter parameter,
                                                          Properties prop)
        throws InitializeException
    {
        if (handler== null) {
            handler = new InitializationConfigHandler(parameter, prop);
        }
        return handler;
    }
    
    /**
     * Getter methods for properties
     */
    public Properties getProperties () {
        return mbomProps;
    }
    
    
    public StartupParameter getStartParameter() {
        return startupParameter;
    }
    
    
}
