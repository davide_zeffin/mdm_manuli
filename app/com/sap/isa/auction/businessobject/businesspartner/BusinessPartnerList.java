/*****************************************************************************
    Copyright (c) 2001, SAPMarkets Palo Alto, USA, All rights reserved.

*****************************************************************************/

package com.sap.isa.auction.businessobject.businesspartner;

import java.util.HashMap;
import java.util.Iterator;

import com.sap.isa.auction.backend.BackendTypeConstants;
import com.sap.isa.auction.backend.boi.SearchData;
import com.sap.isa.auction.backend.boi.businesspartner.BusinessPartnerListBackend;
import com.sap.isa.auction.backend.boi.businesspartner.BusinessPartnerListData;
import com.sap.isa.businessobject.BusinessObjectBase;
import com.sap.isa.businesspartner.businessobject.BusinessPartner;
import com.sap.isa.core.TechKey;
import com.sap.isa.core.businessobject.BackendAware;
import com.sap.isa.core.eai.BackendException;
import com.sap.isa.core.eai.BackendObjectManager;
import com.sap.isa.core.eai.BackendObjectManagerImpl;
import com.sap.isa.core.init.StandaloneHandler;
//import com.sap.isa.auction.businessobject.BPListSearchData
public class BusinessPartnerList extends BusinessObjectBase
implements BusinessPartnerListData, BackendAware{

    private HashMap bpSearchResult = null;
    private HashMap bpList = null;
    private BusinessPartnerListBackend backendService;
    private BackendObjectManager bem = null;
    private BusinessPartner bPartner = null;

    public BusinessPartnerList() {
        HashMap bpList = new HashMap();
    }


    /**
     * Get list of business partners
     */
    public HashMap getBusinessPartnerList() {
        return bpList;
    }

    /**
     * Set list of buisness partner
     */
    public void setBusinessPartnerList(HashMap bpList) {
        this.bpList = bpList;
    }

    /**
     * Add business partner to the list
     */
    public void addBusinessPartner(BusinessPartner bp) {
        bpList.put(bp.getId(),bp);
    }

    /**
     * Assign the BP
     */
    public void setBusinessPartner(BusinessPartner bp) {
        this.bPartner = bp;
    }

    /**
    * Removes all entries.
    */
    public void clear() {
        bpList.clear();
    }

    /**
     * Returns the number of elements in this list.
     *
     * @return the number of elements in this list.
     */
    public int size() {
        return bpList.size();
    }

    /**
     * Returns true if this list contains no data.
     *
     * @return <code>true</code> if list contains no data.
     */
    public boolean isEmpty() {
        return bpList.isEmpty();
    }

    /**
     * Returns true if this list contains the specified element.
     *
     * @param value value whose presence in this list is to be tested.
     * @return <code>true</code> if this list contains the specified value.
     */
    public boolean contains(String id) {
        return bpList.containsValue(id);
    }




    /**
     * Search business partners based on search criteria
     * @param searchData specify search criteria
     * @return business partner list as search result
     */
    public HashMap searchBusinessPartners(SearchData searchData) {

        try {
            getBackendService().crmSearchBPList(this,searchData);

        }
        catch(BackendException ex) {
            if(log.isDebugEnabled())
                log.debug(ex);
        }
        return bpSearchResult;
    }

    /**
     * Search busienss partners based on given target group
     * @param tgGUID  specify target group GUID
     * @return business partner list as search result
     */
    public HashMap searchBusinessPartnersByTG(TechKey tgGUID) {
        try {
            getBackendService().crmSearchBPListByTG(this,tgGUID);
        }
        catch(BackendException ex) {
            if(log.isDebugEnabled())
                log.debug(ex);
        }
        return bpSearchResult;
    }

    public void setBPSearchResult(HashMap list) {
        bpSearchResult = list;
    }

     /**
      * Get the business Partner details from the SU01 user ID
      */
     public BusinessPartner getBPFromUser(String userId)  {
        try {
            getBackendService().getBPFromUser(this,userId);
        }
        catch(BackendException ex) {
            if(log.isDebugEnabled())
                log.debug(ex);
        }
        return bPartner;
     }

    /**
     * Method the BOM calls after a new object is created
     *
     * @param bem reference to the BackendObjectManager object
     */

    public void setBackendObjectManager (BackendObjectManager bem){
        this.bem = bem;
    }

    /**
     * Get the BackendService
     */
    private BusinessPartnerListBackend getBackendService() throws BackendException {

        if (backendService == null) {
            // get the Backend Object from the Backend Object Manager
            backendService = (BusinessPartnerListBackend)bem.createBackendBusinessObject(BackendTypeConstants.BO_TYPE_AUCTION_BP_LIST,null);
        }
        return backendService;
    }


    /**
     * Test data
     */

    public static void main(String[] args) throws Exception{

        StandaloneHandler standaloneHandler = new StandaloneHandler();
        standaloneHandler.setClassNameMessageResources("com.sap.isa.ISAResources");
        standaloneHandler.setBaseDir("C:/tomcat/jakarta-tomcat-3.2.3/webapps/b2b");
        standaloneHandler.setInitConfigPath("/WEB-INF/cfg/init-config.xml");
        standaloneHandler.initialize();

        BusinessPartnerList list = new BusinessPartnerList();
        BackendObjectManager bm = new BackendObjectManagerImpl();
        list.setBackendObjectManager(bm);
        BPListSearchData searchData = new BPListSearchData();
        searchData.setFirstName("cla*");
        HashMap searchResult = list.searchBusinessPartners(searchData);
        if(searchResult != null) {
            Iterator i = searchResult.keySet().iterator();
            while(i.hasNext()) {
                BusinessPartner bp = (BusinessPartner)searchResult.get((String)i.next());
                System.out.println("partner id:"+ bp.getId());
                System.out.println("GUID:" + bp.getTechKey().getIdAsString());
                System.out.println("LastName:"+ bp.getAddress().getLastName());
                System.out.println("FirstName:"+ bp.getAddress().getFirstName());
            }
        }
    }
}
