/*
 * Title:        eBay Integration Project
 * Description:	 * SAP Labs LLC, the eBay auction project, also is known as Selling via eBay. 
 * Selling via eBay application provides SAP R/3 (and in the future CRM) 
 * customers to auction and sell inventory/products onto an online marketplace 
 * eBay. The application will provide the ability to Create, Schedule, Publish, 
 * Monitor and Close Auction Listings onto eBay and handle all the relevant 
 * Backend processing for Customers and Orders. 

 * Copyright:    Copyright (c) 2003
 * Company:      SAP Labs LLC
 * @author
 * @version 1.0
 */
package com.sap.isa.auction.businessobject.businesspartner;

import com.sap.isa.backend.boi.isacore.order.PartnerListEntryData;
import com.sap.isa.businessobject.order.PartnerList;
import com.sap.isa.businessobject.order.PartnerListEntry;
import java.util.HashMap;
/**
 * @author i009657
 *
 */
public class AuctionPartnerList extends PartnerList 
		implements Cloneable{

	/**
	 * Default Constructor
	 */
	public AuctionPartnerList() {
		super();
	}
	
	/**
	 * Performs a deep copy of this object.
	 * Because of the fact that
	 * all fields of this object consist of immutable objects like
	 * <code>String</code> and <code>TechKey</code> or primitive types
	 * the shallow copy is identical with a deep copy. For the
	 * <code>payment</code> property an explicit copy operation is performed
	 * because this object is the only mutable one.
	 *
	 * @return shallow (deep-like) copy of this object
	 */
	public Object clone() {
		AuctionPartnerList myClone = new AuctionPartnerList();
		myClone.partnerList = (HashMap) partnerList.clone();
		return myClone;

	}
	/**
	 * Convenience method to Get the sales employee business partner, 
	 * or null, if not available
	 *
	 * @return PartnerListEntry of the sales employee business partner, 
	 * or null if not found
	 */
	public PartnerListEntry getSalesEmployee() {
		return getPartner(ResponsibleEmployeePF.SALES_EMPLOYEE);
	}

	/**
	 * Convenience method to Get the sales employee business partner, 
	 * or null, if not available
	 *
	 * @return PartnerListEntry of the sales employee business partner, 
	 * or null if not found	 */
	public PartnerListEntryData getSalesEmployeeData() {
		return (PartnerListEntryData) getSalesEmployee();
	}
	
	/**
	 * Convenience method to set the sales employee business partner
	 *
	 * @param PartnerListEntry of the sales employee business partner
	 */
	public void setSalesEmployee(PartnerListEntry entry) {
		setPartner(ResponsibleEmployeePF.SALES_EMPLOYEE, entry);
	}

		/**
	 * Convenience method to set the sales employee business partner
	 *
	 * @param PartnerListEntryData of the sales employee 
	 * business partner
	 */
	public void setSalesEmployeeData(PartnerListEntryData entry) {
		setSalesEmployee((PartnerListEntry) entry);
	}

}
