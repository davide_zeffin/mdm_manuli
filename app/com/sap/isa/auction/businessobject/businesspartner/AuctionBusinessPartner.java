/*
 * Created on May 20, 2005
 *
 * To change the template for this generated file go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
package com.sap.isa.auction.businessobject.businesspartner;

import com.sap.isa.businessobject.Address;
import com.sap.isa.businesspartner.businessobject.BusinessPartner;

/**
 * @author i802791
 *
 * If address exits for the bp, don't read it again. Only valid when get the
 * bp using Auction BP search.
 */
public class AuctionBusinessPartner extends BusinessPartner {
	/**
	  * returns the address data of a business partner, if address is null,
	  * the date is read from the backend system
	  *
	  * @return address of a business partner
	  */
	 public Address getAddress() {
         if (address != null) {
         	return address;
         } 
         else
         	return super.getAddress();

	 }

}
