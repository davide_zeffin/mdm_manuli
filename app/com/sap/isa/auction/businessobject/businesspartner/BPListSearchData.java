/*****************************************************************************
    Copyright (c) 2001, SAPMarkets Palo Alto, USA, All rights reserved.

*****************************************************************************/
package com.sap.isa.auction.businessobject.businesspartner;

import com.sap.isa.auction.backend.boi.SearchData;
import com.sap.isa.core.TechKey;

public class BPListSearchData implements SearchData{

  private String firstname;
  private String lastname;
  private String postcode;
  private String city;
  private String country;
  private String organization;
  private String region;
  private String partnerID;
  private TechKey tgGUID;
  private boolean b2b;
  private boolean b2c;

  public BPListSearchData() {
  }

  public String getFirstName() {
     return firstname;
  }

  public String getLastName() {
     return lastname;
  }

  public String getPostCode() {
     return postcode;
  }

  public String getCity() {
     return city;
  }

  public String getCountry() {
     return country;
  }

  public String getOrganization() {
     return organization;
  }

  public String getRegion() {
     return region;
  }

  public String getPartnerID() {
     return partnerID;
  }

  public TechKey getTargetGroupGUID() {
     return tgGUID;
  }

  public boolean getB2B() {
     return b2b;
  }

  public boolean getB2C() {
     return b2c;
  }

  /**
   * Setters
   */
  public void setFirstName(String firstname) {
     this.firstname = firstname;
  }

  public void setLastName(String lastname) {
     this.lastname = lastname;
  }

  public void setPostCode(String postcode) {
     this.postcode = postcode;
  }

  public void setCity(String city) {
     this.city = city;
  }

  public void setCountry(String country) {
     this.country = country;
  }

  public void setOrganization(String org) {
     this.organization = org;
  }

  public void setRegion(String region) {
     this.region = region;
  }

  public void setPartnerID(String partnerID) {
     this.partnerID = partnerID;
  }

  public void setTargetGroupGUID(TechKey tgGUID) {
     this.tgGUID = tgGUID;
  }

  public void setB2B(boolean bb) {
     b2b = bb;
  }

  public void setB2C(boolean bc) {
     b2c = bc;
  }

  public void clear()  {
      firstname = "";
      lastname = "";
      postcode = "";
      city = "";
      country = "";
      organization = "";
      region = "";
      partnerID = "";
      tgGUID = null;
      b2b=false;
      b2c=false;
  }
}