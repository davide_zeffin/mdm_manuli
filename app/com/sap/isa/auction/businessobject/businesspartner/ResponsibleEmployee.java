/*
 * Created on Sep 12, 2003
 *
 * SAP Labs LLC, the eBay auction project, also is known as Selling via eBay. 
 * Selling via eBay application provides SAP R/3 (and in the future CRM) 
 * customers to auction and sell inventory/products onto an online marketplace 
 * eBay. The application will provide the ability to Create, Schedule, Publish, 
 * Monitor and Close Auction Listings onto eBay and handle all the relevant 
 * Backend processing for Customers and Orders. 

 */
package com.sap.isa.auction.businessobject.businesspartner;

import java.util.Enumeration;

import com.sap.isa.auction.backend.boi.businesspartner.ResponsibleSalesEmployeeData;
import com.sap.isa.businesspartner.businessobject.BusinessPartner;
import com.sap.isa.core.TechKey;
/**
 * This is extension of businesspartner data, then we can get the
 * organization data from business partner
 * The busines spartner ID is the employee ID
 */
public class ResponsibleEmployee
	extends BusinessPartner
	implements ResponsibleSalesEmployeeData {
	
	// all of the organization the responsible employee is responsible for
	private Enumeration respOrgs = null;
	 
	 
	public void setResponsibleOrganizations(Enumeration orgs) {
		respOrgs = orgs;
	}

	public Enumeration getResponsibleOrganizations() {
		return respOrgs;
	}
	/**
	 * Simple constructor of class ResponsibleEmployee.
	 *
	 */
	 public ResponsibleEmployee() {
		  super();
	 }


	/**
	 * Constructor of class ResponsibleEmployee
	 * that sets the id and the technical key
	 * of a soldto.
	 *
	 * @param id of a soldto
	 * @param technical key of a ResponsibleEmployee
	 */
	 public ResponsibleEmployee(String id, TechKey techKey) {
		 super(id, techKey);
	 }


	/**
	 * Constructor of class ResponsibleEmployee
	 * that sets the technical key of
	 * a ResponsibleEmployee.
	 *
	 * @param technical key of a ResponsibleEmployee
	 */
	 public ResponsibleEmployee(TechKey techKey) {
		 super(techKey);
	 }
}
