/*
 * Created on Sep 12, 2003
 *
 * SAP Labs LLC, the eBay auction project, also is known as Selling via eBay. 
 * Selling via eBay application provides SAP R/3 (and in the future CRM) 
 * customers to auction and sell inventory/products onto an online marketplace 
 * eBay. The application will provide the ability to Create, Schedule, Publish, 
 * Monitor and Close Auction Listings onto eBay and handle all the relevant 
 * Backend processing for Customers and Orders. 

 */
package com.sap.isa.auction.businessobject.businesspartner;

import com.sap.isa.businesspartner.businessobject.PartnerFunctionBase;

/**
 * @
 */
public class ResponsibleEmployeePF extends PartnerFunctionBase {
	/**
	 * constant for the sales employee parttner function
	 */
	public static final String SALES_EMPLOYEE = "SALES_EMPLOYEE";
	
	public String getName() {
		return ResponsibleEmployeePF.SALES_EMPLOYEE;
	}

}
