/*
 * Created on Sep 30, 2003
 *
 */
package com.sap.isa.auction.businessobject;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;

import javax.jdo.JDOException;
import javax.jdo.PersistenceManager;
import javax.jdo.PersistenceManagerFactory;
import javax.jdo.Query;
import javax.jdo.spi.PersistenceCapable;

import com.sap.tc.logging.Category;
import com.sap.tc.logging.Location;
import com.sap.isa.auction.bean.search.QueryResult;
import com.sap.isa.auction.exception.AuctionRuntimeExceptionBase;
import com.sap.isa.auction.exception.InvalidStateException;
import com.sap.isa.auction.exception.SystemException;
import com.sap.isa.auction.exception.UserException;
import com.sap.isa.auction.exception.i18n.ErrorMessageBundle;
import com.sap.isa.auction.exception.i18n.I18nErrorMessage;
import com.sap.isa.auction.logging.CategoryProvider;
import com.sap.isa.auction.logging.LogHelper;
import com.sap.isa.core.util.SoftHashMap;

/**
 * Query Result is not Thread safe
 * @author I802891
 *
 */
public abstract class JDOQueryResult extends QueryResult {
	/** Static data block*/
	private static Category logger = CategoryProvider.getBOLCategory();
	private static Location tracer = Location.getLocation(JDOQueryResult.class.getName());

	protected PersistenceManagerFactory pmf = null;
	protected PersistenceManager pm = null;
	protected Query query = null;
	protected Collection coll = null;
	protected int pos = 1;
	protected int resultSize;
	
	protected SoftHashMap cache = new SoftHashMap(); 	// SOFT references of Cached transcribed object
	protected List objectIds; 		// HARD references to ObjectIds for Disconnected mode
	
	protected boolean connected = true;
	
	/**
	 * 
	 * @param pm Persistance manager involved in the creation of the query
	 * @param query JDO query object involved 
	 * @param coll the result set which contains the objects
	 * @param resultSize size of the collection as the jdo collection 
	 * 			does not support, one has to explicitly provide the size as well 
	 */
	protected JDOQueryResult(
		PersistenceManager pm, 
		Query query,
		Collection coll,
		int resultSize) {
			
		this.query = query;
		this.pm = pm;
		this.pmf = pm.getPersistenceManagerFactory(); // used to connect again
		this.coll = coll;
		this.resultSize = resultSize;
	}
    

	/**
	 * @return the Total no of Result Records obtained by Query execution
	 */
	public int resultSize() {
		usageCheck();
		return resultSize;
	}

	/**
	 * If more matching Objects are available
	 */
	public boolean available() {
		usageCheck();
		return pos <= resultSize();
	}

	/**
	 * Positions by absolute pos
	 */
	public void position(int pos) {
		usageCheck();
		if(pos < 1 || pos > resultSize()) {
			throw new IllegalArgumentException("Position is invalid");
		}
		this.pos = pos;
	}

	/**
	 * @return int the Current Absolute Pos
	 */
	public int getPosition() {
		usageCheck();
		return pos;
	}


	/* (non-Javadoc)
	 * @see com.sap.isa.auction.bean.search.QueryResult#fetch()
	 */
	public Object[] fetch() {
		usageCheck();
		Object[] ret = new Object[size];
		int index = 1;
		
		if(!available()) return ret;
		
		// implicityl connected mode
		if(connected && coll != null) {
			Iterator it = coll.iterator();
			
			// move to current pos. Thats the only way:(
			while(available() && index < pos){
			   index++;
			   it.next(); // index to pos
			}
			
			// move pos
			while(available() && pos < index + size) {
				PersistenceCapable dao = (PersistenceCapable)it.next();
				// Look up in temp mem sensitive cache to avoid transcribing
				Object valueBean = cache.get(dao.jdoGetObjectId().toString());
				if(valueBean == null) {
					valueBean = transcribe(dao);
					cache.put(dao.jdoGetObjectId().toString(),valueBean);
				}
				ret[pos - index] = valueBean;
				pos++;
			}
			
			// TODO:: temp for JDO bug, iterate till the end
			while(it.hasNext()) {
				it.next();
			}
		}
		// reconnected usage mode
		else {
			// retreive the Object ids for the list of objects to connect
			for(int i = 0; i < size && available(); i++) {
				Object id = objectIds.get(pos++ - 1);
				Object valueBean = cache.get(id.toString());
				if(valueBean == null) {
					// TODO:: If possible use batch query for current fetch than firing individual queries
					// disconnected mode, raise exception
					if(!connected) {
						ErrorMessageBundle bundle = ErrorMessageBundle.getInstance();
						I18nErrorMessage msg = bundle.newI18nErrorMessage(InvalidStateException.NOT_AVAILABLE);
						msg.setArguments(new String[]{"JDOQueryResult disconnected, OID = " + id.toString()});
						InvalidStateException ex = new InvalidStateException(msg);
						LogHelper.logError(logger,tracer,ex.getMessage(),ex);
						throw ex;
					}
					PersistenceCapable dao = (PersistenceCapable)pm.getObjectById(id,false);
					valueBean = transcribe(dao);
					cache.put(id.toString(),valueBean);
				}
				ret[i] = valueBean;
			}
		}
		
		return ret;
	}
	
	/**
	 * Derived classes provide concrete implementations of this method, this is
	 * used to transform DAOs to Bean representation
	 * @author I802891
	 *
	 */
	protected abstract Object transcribe(Object dao);

	public void close() {
		if(super.closed) return;
		try {
			if(query != null) query.closeAll();
			coll = null;
			query = null;
		}
		catch(Exception e) {
			LogHelper.logWarning(logger,tracer,"Error during close of JDOQueryResult",e);
		}
		catch(Throwable th){
			LogHelper.logWarning(logger,tracer,"Error during close of JDOQueryResult", th);
		}
		finally {
			if(pm != null) pm.close();
			pm = null;
		}
		super.close();
	}
	
	//////////////////////////////////////////////
	// Dynamic connection mgmt specific methods	
	/* (non-Javadoc)
	 * @see com.sap.isa.auction.bean.search.QueryResult#connect()
	 */
	public void connect() {
		usageCheck();
		if(connected) return;
		
		try {
			PersistenceManager pm = pmf.getPersistenceManager();
			// coll remains null indicating explicitly connected again
			this.pm = pm;
		}
		catch(Exception ex) {
			RuntimeException rtex = handleException(ex);
			LogHelper.logError(logger,tracer,"Error during connect of JDOQueryResult",rtex);
			throw rtex;
		}
		connected = true;
	}
	
	protected AuctionRuntimeExceptionBase handleException(Exception ex) {
		ErrorMessageBundle bundle = ErrorMessageBundle.getInstance();
		I18nErrorMessage msg = bundle.newI18nErrorMessage();
		if(ex instanceof JDOException) {
			msg.setPatternKey(SystemException.JDO_ERROR);
		}
		else {
			msg.setPatternKey(SystemException.INTERNAL_ERROR);
		}
		return new SystemException(msg,ex);
	}

	/* (non-Javadoc)
	 * @see com.sap.isa.auction.bean.search.QueryResult#disconnect()
	 */
	public void disconnect() {
		usageCheck();
		if(!connected) return;
		connected = false;
		super.disconnect();
		
		try {
			// If object Ids are once filled, there is no need to refill
			if(objectIds == null) {
				objectIds = new ArrayList(resultSize); 
				// cache the Object Ids as Hard References;
				Iterator it = coll.iterator();
				while(it.hasNext()) {
					Object id = ((PersistenceCapable)it.next()).jdoGetObjectId();
					objectIds.add(id); // in order of search result
				}
			}
			if(query != null) { query.closeAll(); query = null; }
			coll = null;
		}
		catch(Exception ex) {
			RuntimeException rtex = handleException(ex);
			LogHelper.logWarning(logger,tracer,"Error during disconnect of JDOQueryResult",rtex);
		}
		finally {
			// close the pm
			if(pm != null) {
				pm.close();
				pm = null;
			}

		}
	}

	/* (non-Javadoc)
	 * @see com.sap.isa.auction.bean.search.QueryResult#isConnected()
	 */
	public boolean isConnected() {
		usageCheck();
		return connected;
	}

	/* (non-Javadoc)
	 * @see com.sap.isa.auction.bean.search.QueryResult#refresh()
	 */
	public void refresh() {
		usageCheck();
		if(!connected) {
			ErrorMessageBundle bundle = ErrorMessageBundle.getInstance();
			I18nErrorMessage msg = bundle.newI18nErrorMessage(UserException.ILLEGAL_OPERATION);
			msg.setArguments(new String[]{"refresh","JDOQueryResult"});
			UserException ex = new UserException(msg);
			LogHelper.logError(logger,tracer,ex.getMessage(),ex);
			throw ex;
		}
		// do nothing, later
		/*
		if(objectIds != null) objectIds.clear();
		objectIds = null;
		// create a fresh query
		try {
			Query freshQuery = pm.newQuery(query.getCandidateClass());
			freshQuery.setFilter(query.getFilter());
			query.closeAll();
			coll = (Collection)freshQuery.execute();
		}
		catch(JDOException ex) {
		}
		catch(Exception ex) {
		}*/
	}
}
