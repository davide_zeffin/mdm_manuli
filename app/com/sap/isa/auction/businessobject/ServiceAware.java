/*
 * Created on Feb 13, 2004
 *
 * Title:        Internet Sales Asset Remarketing
 * Description:
 * Copyright:    Copyright (c) 2004
 * Company:      SAP Labs LLC
 * @author
 * @version 1.0
 */
package com.sap.isa.auction.businessobject;

import com.sap.isa.auction.services.ServiceLocator;

/**
 * @author I802850
 *
 * To change the template for this generated type comment go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
public interface ServiceAware {

	public void setServiceLocator(ServiceLocator locator);
	public ServiceLocator getServiceLocatorBase();

}
