/*
 * Created on Apr 22, 2004
 *
 * To change the template for this generated file go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
package com.sap.isa.auction.businessobject;

import java.sql.Timestamp;
import java.util.Collection;

import com.sap.isa.auction.bean.AuctionStatusEnum;
import com.sap.isa.auction.bean.OrderStatusEnum;
import com.sap.isa.auction.bean.search.QueryFilter;
import com.sap.isa.auction.bean.search.QueryResult;

/**
 * Title:			Asset Remarketing
 * Description:
 * @Copyright: 		Copyright (c) 2004
 * Company:			SAPLabs LLC
 * @author 
 *
 * To change the template for this generated type comment go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
public interface SearchManager extends AuctionBusinessObject {

	/**
	 * Seller Use case:
	 * Bidder Use case: 
	 * 
	 * Search for Auctions matching the filter criteria. 
	 * Only one API for advanced search on both seller and
	 * bidder use cases.
	 *  
	 * In case of a seller use case: 
	 * 1) A createdBy will be added to the query
	 * 2) Auctions will be filtered out by the bps and tgs 
	 *    if they are not null
	 * 
	 * In case of a bidder use case:
	 * 1) ignore some seller specific search filter like: 
	 *    createdBy, transaction Error
	 * 2) bps contains only the bidder bpId
	 * 3) tgs contains only the tgs bidder belongs to
	 * 
	 * Order is important.
	 * 
	 * @param filter PrivateAuctionQueryFilter
	 * @param bps Collection contains business partner ids. 
	 * 		  Different meaning in seller and bidder search cases
	 * @param tgs Collection contains target group guids. 
	 * 		  Different meaning in seller and bidder search cases
	 * @param sellerSearch boolean indicate if this is a seller
	 * 		  search case or bidder search case
	 */
	public QueryResult searchAuctions(
		QueryFilter filter,
		Collection bps,
		Collection tgs,
		boolean sellerSearch);

	/**
	 * Bidder Use case:
	 *
	 * Check if any active auction that is visible to the bidder 
	 * exists for a particular product. Bidder is a Business Partner
	 * in CRM
	 *
	 * @param bpId String Bidder's CRM business partner Id
	 * @param productGuid String Product guid 
	 * @param tgs Collection contains target group guids bidder
	 * 		  belongs to
	 */
	public boolean isAuctionExistForProduct(
		String bpId,
		String productGuid,
		Collection tgs);
	/**
	 * Bidder Use case:
	 *
	 * Search active auctions for a product that is visible to 
	 * the bidder. Bidder is a Business Partner in CRM
	 *
	 * @param bpId String Bidder's CRM business partner Id
	 * @param productGuid String Product guid 
	 * @param tgs Collection contains target group guids bidder
	 * 		  belongs to
	 */
	public QueryResult searchAuctionsByProduct(
		String bpId,
		String productGuid,
		Collection tgs);
	/**
	 * Bidder Use case:
	 *
	 * Search Auctions in which the Bidder has participated
	 * Bidder is a Business Partner in CRM
	 * Doesn't excluding the TargetGroups filter since Target groups 
	 * may change over time
	 *
	 * @param bpId String Bidder's CRM business partner Id
	 * @param soldTo String Bidder's sold to party Id
	 * @param since Timestamp 
	 * @param status AuctionStatusEnum
	 */
	public QueryResult searchAuctionsParticipatedByBidder(
		String bpId,
		String soldTo,
		Timestamp since,
		AuctionStatusEnum status);
	/**
	 * Bidder Use case:
	 *
	 * Search Auctions won by a particular Bidder
	 * Bidder is a Business Partner in CRM
	 * 
	 * @param bpId String Bidder's CRM business partner Id
	 * @param soldTo String Bidder's sold to party Id
	 * @param since Timestamp
	 * @param status OrderStatusEnum
	 * 1 => Checked <--> OrderStatusEnum.CHECKED
	 * 2 => Unchecked <--> OrderStatusEnum.UNCHECKED
	 * null => ignore i.e Both Checked and Unchecked
	 * 
	 */
	public QueryResult searchAuctionsWonByBidder(
		String bpId,
		String soldTo,
		Timestamp since,
		OrderStatusEnum status);
	//	/**
	//	* Bidder Use case:
	//	*
	//	* Searches Auctions which the Bidder tracked
	//	*
	//	*/
	//	public QueryResult searchAuctionsTrackedByBidder(
	//		String bpId,
	//		Timestamp since,
	//		AuctionStatusEnum status);
}
