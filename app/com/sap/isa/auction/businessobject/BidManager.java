package com.sap.isa.auction.businessobject;

import com.sap.isa.auction.bean.Auction;
import com.sap.isa.auction.bean.Bid;

import java.util.List;

/**
 * Title:
 * Description:
 * Copyright:    Copyright (c) 2003
 * Company:
 * @author
 * @version 1.0
 */

public interface BidManager extends AuctionBusinessObject {
    /**
     * Retreives Bid History
     * Order is important
     */
    List getBidHistory(Auction auction);

    /**
     *
     */
    Bid[] getHighestBids(Auction auction);
    
    long getBidCount(Auction auction);
}