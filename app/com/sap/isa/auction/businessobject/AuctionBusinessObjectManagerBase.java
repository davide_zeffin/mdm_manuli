package com.sap.isa.auction.businessobject;

/*****************************************************************************
    Copyright (c) 2001, SAPMarkets Palo Alto, USA, All rights reserved.

*****************************************************************************/


import com.sap.isa.auction.businessobject.order.AuctionOrderStatus;
import com.sap.isa.auction.businessobject.user.AuctionUser;
import com.sap.isa.auction.businessobject.user.Seller;
import com.sap.isa.auction.businessobject.user.SellerAdministrator;
import com.sap.isa.auction.logging.CategoryProvider;
import com.sap.isa.auction.logging.TraceHelper;
import com.sap.isa.auction.services.ServiceLocator;
import com.sap.isa.backend.BackendTypeConstants;
import com.sap.isa.backend.boi.isacore.ShopBackend;
import com.sap.isa.businessobject.BusinessObjectHelper;
import com.sap.isa.businessobject.BusinessObjectManager;
import com.sap.isa.businessobject.CommunicationException;
import com.sap.isa.businessobject.Shop;
import com.sap.isa.businesspartner.businessobject.BusinessPartnerManager;
import com.sap.isa.core.TechKey;
import com.sap.isa.core.businessobject.BackendAware;
import com.sap.isa.core.businessobject.event.BusinessObjectCreationEvent;
import com.sap.isa.core.businessobject.management.BusinessObjectManagerBase;
import com.sap.isa.core.eai.BackendException;
import com.sap.isa.core.eai.BackendObjectManager;
import com.sap.isa.persistence.pool.ObjectPool;
import com.sap.isa.user.businessobject.UserBase;
import com.sap.tc.logging.Category;
import com.sap.tc.logging.Location;


/**
 * AuctionBusinessObjectManager is the auction specific business object manager.
 * Central point to create and access Objects of the business logic. After
 * a request for a specific object, BusinessObjectManager checks if there is already
 * an object created. If so, a reference to this object is returned, otherwise
 * a new object is created and a reference to the new object is returned.
 * The reason for this class is, that the construction process of the businnes
 * logic objects may be quite complicated (because of some contextual
 * information) and most objects should only exist once per session.
 * To achieve this the <code>BusinessObjectManager</code>
 * is used. If you store a reference to this
 * object (for examle in a session context) you can get access to all other
 * business obejects using it.
 *
 */
public abstract class AuctionBusinessObjectManagerBase
extends BusinessObjectManagerBase
implements BackendAware,ServiceAware {


    protected static Location tracer =
        Location.getLocation(AuctionBusinessObjectManagerBase.class.getName());
        
    protected static final Category logger = CategoryProvider.getBOLCategory();

    /*
     * Name of this BOM
    */
    public static final String AUCTION_BOM = "AUCTION-BOM";

    /** Statically created Pools => Application ClassLoader level existence*/
    protected static ObjectPool amPool = null;
    protected static ObjectPool wmPool = null;
    protected static ObjectPool bmPool = null;
	protected static ObjectPool smPool = null;

    protected BackendObjectManager bem;
	protected ServiceLocator serviceLocator;

    // References to the business objects
    //protected Auction auction;
    protected transient UserBase user;
    protected transient AuctionUser auctionUser;
	protected transient SalesOrganization salesOrg;

    protected transient WinnerManager winnerManager;
    protected transient AuctionManager auctionManager;
    protected transient BidManager bidManager;
	protected transient UserPreferenceManager userPrefMgr;
	protected transient SearchManager searchManager;

	/// In this BOM we assume it binds to a http session, thus
	/// It hands the seller object, product list, order status
	protected transient	Seller seller;
	protected transient	SellerAdministrator sellerAdmin;	
	protected transient AuctionOrderStatus aucOrderStatus;
	protected transient Shop shop;
	
	protected transient BusinessPartnerManager bupaManager;
    /**
     * Create a new instance of the object.
     */
    public AuctionBusinessObjectManagerBase() {
        super();
        if(logger.beInfo()) {
        	logger.infoT(tracer,"AuctionBusinessObjectManager created");
        }
    }

    /**
     * Static initialization of BOM is triggered thru the init of Action Servlet
     */
	/**
	 * Static initialization of BOM is triggered thru the init of Action Servlet
	 */
	public static void initialize(Object ctxt) throws Exception {
		
	}

    /**
     * Static close of BOM is triggered thru the destroy of Action Servlet
     */
    public static void destroy() {
    }

	/**
	 * Each BOM who requires the BackendObjectManager will be informed
	 * from the MBOM about the correct one.
	 *
	 * @see BackendAware com.sap.core.businessobject.BackendAware
	 */
	public void setBackendObjectManager(BackendObjectManager bem) {
		this.bem = bem;
		if(tracer.beDebug()) {
			TraceHelper.logDebug(tracer,"BackendObjectManager set", null);
		}
	}
	
	/**
	 * This method initializes the Business Objects of this BOM
	 * @param obj
	 */
	protected void initializeBO(AuctionBusinessObject obj) {
		setBEM(obj);
		setServiceLocator(obj);
		obj.setAuctionBOM(this);
		obj.initialize();
	}
	
	/**
	 * Utility method internally used to set the BackendObjectManager
	 * only for objects that implement BackendAware.
	 *
	 * @see BackendAware BackendAware
	 */
	protected void setBEM(Object obj) {
		if ((obj instanceof BackendAware) && (obj != null)) {
			((BackendAware) obj).setBackendObjectManager(bem);
		}
	}
	
	/**
	 * Utility method internally used to set the BackendObjectManager
	 * only for objects that implement ServiceAware.
	 *
	 * @see ServiceAware ServiceAware
	 */
	protected void setServiceLocator(Object obj) {
		if ((obj instanceof ServiceAware) && (obj != null)) {
			((ServiceAware) obj).setServiceLocator(serviceLocator);
		}
	}

	protected void callbackDestroy(Object o) {
	}
	
	/**
	 * Is called from the MBOM whenever the BOM has to be released
	 * (new session or logoff, or timeout)
	 *
	 * @see BOManager com.sap.core.businessobject.management.BOManager
	 */
	public void release() {
		if(searchManager != null) { searchManager.close(); smPool.returnObject(searchManager);}
		searchManager = null;
		if(userPrefMgr != null) userPrefMgr.close();
		userPrefMgr = null;

		user = null;
		auctionUser = null;
		seller = null;
		aucOrderStatus = null;
		sellerAdmin = null;
		salesOrg = null;
	}

	/**
	 * Returns the auctionManager object
	 */
   /**
     * Returns the auctionManager object
     */
    public synchronized WinnerManager createWinnerManager() {
        WinnerManager winnerManager  = (WinnerManager)wmPool.getObjectNoWait();
        if(tracer.beDebug()) {
            tracer.debugT("Winner Manager pool size:" + wmPool.getSize());
        }
		initializeBO(winnerManager);
        creationNotification(new BusinessObjectCreationEvent(winnerManager));
        return this.winnerManager = winnerManager;
    }

    public synchronized WinnerManager getWinnerManager() {
        // renew lease
        if(winnerManager != null) {
            wmPool.updateUsedTime(winnerManager);
        }
        return winnerManager;
    }

    public synchronized void releaseWinnerManager(WinnerManager wm) {
        if(wm != null && wm == winnerManager) {
            wmPool.returnObject(winnerManager);
            winnerManager.close();
            winnerManager = null;
        }
    }

    /**
     * Returns the auctionManager object
     */
    public synchronized AuctionManager createAuctionManager() {
        AuctionManager auctionManager = (AuctionManager)amPool.getObjectNoWait();
        if(tracer.beDebug()) {
            tracer.debugT("Auction Manager pool size:" + amPool.getSize());
        }
        
		// @@todo:: initialization may throw exception, release the object
		initializeBO(auctionManager);
        creationNotification(new BusinessObjectCreationEvent(auctionManager));
        return this.auctionManager = auctionManager; // properly initialized, now cache the reference
    }

    /**
     * returns the auctionManager
     */
    public synchronized AuctionManager getAuctionManager() {
        // renew lease with Pool
        if(auctionManager != null) {
            amPool.updateUsedTime(auctionManager);
        }
		else {
			createAuctionManager();
		}
        return auctionManager;
    }

    public synchronized void releaseAuctionManager(AuctionManager am) {
        if(am != null && auctionManager == am) {
            amPool.returnObject(auctionManager);
            auctionManager.close();
            auctionManager = null;
        }
    }

    /**
     * Returns the auctionManager object
     */
    public synchronized BidManager createBidManager() {
        BidManager bidManager = (BidManager)bmPool.getObjectNoWait();
        if(tracer.beDebug()) {
            tracer.debugT("Bid Manager pool size:" + bmPool.getSize());
        }
		initializeBO(bidManager);
        creationNotification(new BusinessObjectCreationEvent(bidManager));
        return this.bidManager = bidManager;
    }

    /**
     * returns the auctionManager
     */
    public synchronized BidManager getBidManager() {
        // renew lease
        if(bidManager != null) {
            bmPool.updateUsedTime(bidManager);
        }
        return bidManager;
    }
	

    /**
     * Release the Business Object to the underlying object pool
     */
    public synchronized void releaseBidManager(BidManager bm) {
        if(bm != null && bidManager == bm) {
            bmPool.returnObject(bidManager);
            bidManager.close();
            bidManager = null;
        }
    }

	/**
	 * Returns the SearchManager object
	 */
	public synchronized SearchManager createSearchManager() {
		SearchManager searchManager = (SearchManager)smPool.getObjectNoWait();
		if(tracer.beDebug()) {
			tracer.debugT("Search Manager pool size:" + smPool.getSize());
		}
		initializeBO(searchManager);
		creationNotification(new BusinessObjectCreationEvent(searchManager));
		return this.searchManager = searchManager;
	}

	/**
	 * returns the SearchManager
	 */
	public synchronized SearchManager getSearchManager() {
		// renew lease
		if(searchManager != null) {
			smPool.updateUsedTime(searchManager);
		}
		return searchManager;
	}
	

	/**
	 * Release the Business Object to the underlying object pool
	 */
	public synchronized void releaseSearchManager(SearchManager sm) {
		if(sm != null && searchManager == sm) {
			smPool.returnObject(searchManager);
			searchManager.close();
			searchManager = null;
		}
	}

	/**
	 * Creates a UserBase type user object
	 * Will be called ISA-UME framework 
	 * @return reference to the UserBase businessobject
	 */
	
	public synchronized UserBase createUserBase()  {
		if(null == user)  {
			user = new Seller();
			user.setBackendObjectManager(bem);
			creationNotification(new BusinessObjectCreationEvent(user));
		}
		return user;
	}
	
	/**
	 * Release the references to created user object.
	 */
	public synchronized void releaseUser() {
		user = null;
	}
	
	public synchronized UserBase getUserBase()  {
		if(null == user)  {
			user = createUserBase();
		}
		return user;  
	}


    /**
     * Creates a new user object. If such an object was already created, a
     * reference to the old object is returned and no new object is created.
     *
     * @return referenc to a newly created or already existend user object
     */
    public synchronized AuctionUser createAuctionUser() {
        if(null == auctionUser)  {
            auctionUser = new AuctionUser();
            auctionUser.setBackendObjectManager(bem);
            creationNotification(new BusinessObjectCreationEvent(auctionUser));
        }
        return auctionUser;
    }

    /**
     * Returns a reference to an existing user object.
     *
     * @return reference to user object or null if no object is present
     */
    public AuctionUser getAuctionUser() {
      return auctionUser;
    }

    
	/**
	 * Returns a reference to an existing auction order status
	 * object.
	 *
	 * @return reference to user object or null if no object is 
	 * present
	 */
	public AuctionOrderStatus getAuctionOrderStatus() {
	  logger.infoT(tracer, "Enter the getAuctionOrderStatus");
	  return aucOrderStatus;
	}
	
	/**
	 * Creates a new sales org object. 
	 * If such an object was already created, a
	 * reference to the old object is returned 
	 * and no new object is created. 
	 *
	 * @return referenc to a newly created or already existend user object
	 */
	public synchronized SalesOrganization createSalesOrganization() {
		if(null == salesOrg)  {
			salesOrg = new SalesOrganization();
			salesOrg.setBackendObjectManager(bem);
			creationNotification(new BusinessObjectCreationEvent(salesOrg));
		}
		return salesOrg;
	}

	/**
	 * Returns a reference to an existing sales org
	 * object.
	 *
	 * @return reference to user object or null if no object is 
	 * present
	 */
	public SalesOrganization getSalesOrganization() {
	  return salesOrg;
	}
	
	/**
	 * Creates a new auction Seller object. 
	 * If such an object was already created, a
	 * reference to the old object is returned 
	 * and no new object is created. In order can be used you have to
	 * set the documentsearchfilter
	 *
	 * @return referenc to a newly created or already existend user object
	 * @deprecated
	 */
	public synchronized Seller createSeller() {
		if(null == seller)  {
			seller = new Seller();
			seller.setBackendObjectManager(bem);
			creationNotification(new BusinessObjectCreationEvent(seller));
		}
		return seller;
	}

	/**
	 * Returns a reference to an existing auction seller
	 * object.
	 *
	 * @return reference to user object or null if no object is 
	 * @deprecated use getUserBase
	 * present
	 */
	public Seller getSeller() {
	  return seller;
	}
	
	
	/**
	 * Creates a new auction Seller object. 
	 * If such an object was already created, a
	 * reference to the old object is returned 
	 * and no new object is created. In order can be used you have to
	 * set the documentsearchfilter
	 *
	 * @return referenc to a newly created or already existend user object
	 * @deprecated
	 */
	public synchronized SellerAdministrator createSellerAdministrator() {
		if(null == sellerAdmin)  {
			sellerAdmin = new SellerAdministrator();
			sellerAdmin.setBackendObjectManager(bem);
			creationNotification(new BusinessObjectCreationEvent(sellerAdmin));
		}
		return sellerAdmin;
	}

	/**
	 * Returns a reference to an existing auction seller
	 * object.
	 *
	 * @return reference to user object or null if no object is 
	 * present
	 */
	public SellerAdministrator getSellerAdministrator() {
	  return sellerAdmin;
	}


	/* (non-Javadoc)
	 * @see com.sap.isa.auction.businessobject.ServiceAware#setServiceLocator(com.sap.isa.auction.services.ServiceLocator)
	 */
	public void setServiceLocator(ServiceLocator locator) {
		this.serviceLocator = locator;
		
	}
	
	/* (non-Javadoc)
	 * @see com.sap.isa.auction.businessobject.ServiceAware#getServiceLocator()
	 */
	public ServiceLocator getServiceLocatorBase() {
		return serviceLocator;
	}
	/**
	 * Creates a new auction order status object. 
	 * If such an object was already created, a
	 * reference to the old object is returned 
	 * and no new object is created. In order can be used you have to
	 * set the documentsearchfilter
	 *
	 * @return referenc to a newly created or already existend user object
	 */
	public synchronized AuctionOrderStatus createAuctionOrderStatus() {
		logger.infoT(tracer, "Enter the createAuctionOrderStatus");
		if(null == aucOrderStatus)  {
			aucOrderStatus = new AuctionOrderStatus();
			aucOrderStatus.setBackendObjectManager(bem);
			creationNotification(new BusinessObjectCreationEvent(aucOrderStatus));
		}
		logger.infoT(tracer, "Exit the createAuctionOrderStatus");
		return aucOrderStatus;
		
	}
	

	/**
	 * Creates a shop for the given technical key and stores a reference
	 * to this shop in the bom. This reference can be retrieved with
	 * the <code>getShop</code> method. The shops are held
	 * in a cache, so that only the first call really reads the
	 * shop data from the underlying storage. All later calls for the
	 * same key will be answered directly by the data stored in the
	 * cache.
	 *
	 * @param key Technical key of the desired shop
	 * @return The shop read
	*/
	public synchronized Shop createShop(TechKey techKey) throws CommunicationException {

		final String METHOD = "createShop()";
		tracer.entering(METHOD);
		if (logger.beDebug()) {
			logger.infoT(tracer, "createShop(" + techKey + ")");
		}
		
			if (shop == null) {
				try {
					ShopBackend shopBE =
						(ShopBackend) bem.createBackendBusinessObject(BackendTypeConstants.BO_TYPE_SHOP, BusinessObjectManager.OBJECT_FACTORY);
	
					shop = (Shop) shopBE.read(techKey);
				}
				catch (BackendException e) {
					BusinessObjectHelper.splitException(e);
					return null; // never reached, just to satisfy the compiler
				}
				
				if (shop != null) {
					shop.setBackendObjectManager(bem);
				}
				creationNotification(shop);
			}
		
		return shop;
	}

	
	/**
	 * Returns a reference to an existing shop object.
	 *
	 * @return reference to shop object or null if no object is present
	 */
	public Shop getShop() {
		return shop;
	}

	/**
	 * Releases reference to the shop object.
	 */
	public synchronized void releaseShop() {
		if (shop != null) {
			removalNotification(shop);
			callbackDestroy(shop);
			shop = null;
		}
	}

	/**
	 * Creates a new BusinessPartnerManager object. If such an object was already created, a
	 * reference to the old object is returned and no new object is created.
	 *
	 * @return reference to a newly created or already existing object
	 */
	public synchronized BusinessPartnerManager createBUPAManager() {

		if(null == bupaManager)  {
			bupaManager = new BusinessPartnerManager();
			bupaManager.setBackendObjectManager(bem);
			creationNotification(new BusinessObjectCreationEvent(bupaManager));
		}
		return bupaManager;
	}
	

}
