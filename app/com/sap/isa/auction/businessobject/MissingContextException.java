package com.sap.isa.auction.businessobject;

import com.sap.tc.logging.Category;
import com.sap.tc.logging.Location;
import com.sap.isa.auction.exception.UserException;
import com.sap.isa.auction.exception.i18n.*;

/**
 * Title:        Internet Sales
 * Description:  This exception is thrown when the calling thread does not have
 *              a valid context attached with it.
 * Copyright:    Copyright (c) 2002
 * Company:      SAPMarkets
 * @author : e-auction
 * @version 1.0
 */

public class MissingContextException extends UserException {
    /** Resource bundle Key*/
    private static final String EXC_ID = "MissingContextException";

    public static final String CONTEXT_NOT_BOUND = EXC_ID + 501;
    public static final String INVALID_CONTEXT = EXC_ID + 502;
    public static final String ISAUSER_NOT_BOUND = EXC_ID + 503;

    static {

		ErrorMessageBundle.setDefaultMessage(CONTEXT_NOT_BOUND,
            "Invocation Context not attached with calling thread [{0}]");
		ErrorMessageBundle.setDefaultMessage(INVALID_CONTEXT,
            "Invocation Context is invalid");
		ErrorMessageBundle.setDefaultMessage(ISAUSER_NOT_BOUND,
            "ISA User not bound with Invocation Context");
    }

    private transient Thread thread;


	/**
	 * @param arg0
	 */
	public MissingContextException(Throwable arg0) {
		super(arg0);
		thread = Thread.currentThread();
	}

	/**
	 * @param arg0
	 */
	public MissingContextException(I18nErrorMessage arg0) {
		super(arg0);
		thread = Thread.currentThread();		
	}

	/**
	 * @param arg0
	 * @param arg1
	 */
	public MissingContextException(I18nErrorMessage arg0, Throwable arg1) {
		super(arg0, arg1);
		thread = Thread.currentThread();		
	}

	/**
	 * @param arg0
	 * @param arg1
	 * @param arg2
	 * @param arg3
	 * @param arg4
	 */
	public MissingContextException(
		Category arg0,
		int arg1,
		Location arg2,
		I18nErrorMessage arg3,
		Throwable arg4) {
		super(arg0, arg1, arg2, arg3, arg4);
		thread = Thread.currentThread();		
	}


    /**
     * @return the culprit thread
     */
    public Thread getThread() {
        return thread;
    }
}