/*
 * Title:        eBay Integration Project
 * Description:	 * SAP Labs LLC, the eBay auction project, also is known as Selling via eBay. 
 * Selling via eBay application provides SAP R/3 (and in the future CRM) 
 * customers to auction and sell inventory/products onto an online marketplace 
 * eBay. The application will provide the ability to Create, Schedule, Publish, 
 * Monitor and Close Auction Listings onto eBay and handle all the relevant 
 * Backend processing for Customers and Orders. 

 * Copyright:    Copyright (c) 2003
 * Company:      SAP Labs LLC
 * @author
 * @version 1.0
 */
package com.sap.isa.auction.businessobject.product;

import java.util.Enumeration;
import java.util.Hashtable;

import com.sap.isa.auction.backend.boi.PlantData;
import com.sap.isa.auction.backend.boi.SalesAreaData;
import com.sap.isa.auction.backend.boi.product.ProductStatusData;
import com.sap.isa.businessobject.Product;
import com.sap.isa.core.TechKey;
/**
 *
 * Extend the ISA product information for retrieving the product extension
 * which does not exist in the normal ISA product
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
public class CatalogProductExtension extends Product implements ProductDetail{

	// the plant data
	private PlantData plant = null;	
	// the sales area the plant associate to, this is indirect
	private SalesAreaData salesArea = null;
	private double quantity = 0.0;
	private String productId;
	
	private ProductStatusData status;
	
	private String manufactPartNumber = null;
	private String baseUOM = null;
	private String materialGroup = null;
	private String oldMaterialNumber = null;
	private String grossWeight = null;
	private String weightUnit = null;
	private String netWeight = null;
	private String volume = null;
	private String volumeUnit = null;
	private String dimensions = null;
	private String EAN = null;
	private String EANCategory = null;
	private String basicText = null;
	
	protected Hashtable attPairs  = null;
	/*
	 * Constructor
	 *
	 * @param techKey  techKey of the product
	 *
	 */
	public CatalogProductExtension(TechKey techKey)
	{
	  super(techKey);
	  attPairs = new Hashtable();
	}

	/*
	 * Constructor
	 *
	 * @param techKey  techKey of the product
	 * @param id product id
	 *
	 */
	public CatalogProductExtension(TechKey techKey, String id) {
		super(techKey, id);
		attPairs = new Hashtable();
	}
	

	/**
	 * @return th eplant data associate with the product
	 */
	public PlantData getPlant() {
		return plant;
	}

	/**
	 * @param string,  set the plant data associate with the product
	 */
	public void setPlant(PlantData plantData) {
		plant = plantData;
	}

	/**
	 * @return the quantity for the checking purpose
	 */
	public double getQuantity() {
		return quantity;
	}

	/**
	 * @return the sales area belongs to this
	 */
	public SalesAreaData getSalesArea() {
		return salesArea;
	}

	/**
	 * setter for the product quantity
	 * @param decimal
	 */
	public void setQuantity(double decimal) {
		quantity = decimal;
	}

	/**
	 * setter for the sales area
	 * @param data
	 */
	public void setSalesArea(SalesAreaData data) {
		salesArea = data;
	}

	/**
	 * @return
	 */
	public String getProductId() {
		return productId;
	}

	/**
	 * @param string
	 */
	public void setProductId(String string) {
		productId = string;
	}

	/**
	 * @return
	 */
	public ProductStatusData getStatus() {
		return status;
	}

	/**
	 * @param data
	 */
	public void setStatus(ProductStatusData data) {
		status = data;
	}

	/* (non-Javadoc)
	 * @see com.sap.isa.auction.businessobject.product.ProductDetail#fromCatalog()
	 */
	public boolean fromCatalog() {
		return true;
	}

	/* (non-Javadoc)
	 * @see com.sap.isa.auction.businessobject.product.ProductDetail#getBaseUOM()
	 */
	public String getBaseUOM() {
		return baseUOM;
	}

	/* (non-Javadoc)
	 * @see com.sap.isa.auction.businessobject.product.ProductDetail#getBasicText()
	 */
	public String getBasicText() {
		return basicText;
	}

	/* (non-Javadoc)
	 * @see com.sap.isa.auction.businessobject.product.ProductDetail#getDimensions()
	 */
	public String getDimensions() {
		return dimensions;
	}

	/* (non-Javadoc)
	 * @see com.sap.isa.auction.businessobject.product.ProductDetail#getEAN()
	 */
	public String getEAN() {
		return EAN;
	}

	/* (non-Javadoc)
	 * @see com.sap.isa.auction.businessobject.product.ProductDetail#getEANCategory()
	 */
	public String getEANCategory() {
		return EANCategory;
	}

	/* (non-Javadoc)
	 * @see com.sap.isa.auction.businessobject.product.ProductDetail#getGrossWeight()
	 */
	public String getGrossWeight() {
		return grossWeight;
	}

	/* (non-Javadoc)
	 * @see com.sap.isa.auction.businessobject.product.ProductDetail#getManufactPartNumber()
	 */
	public String getManufactPartNumber() {
		return manufactPartNumber;
	}

	/* (non-Javadoc)
	 * @see com.sap.isa.auction.businessobject.product.ProductDetail#getMaterialGroup()
	 */
	public String getMaterialGroup() {
		return materialGroup;
	}

	/* (non-Javadoc)
	 * @see com.sap.isa.auction.businessobject.product.ProductDetail#getNetWeight()
	 */
	public String getNetWeight() {
		return netWeight;
	}

	/* (non-Javadoc)
	 * @see com.sap.isa.auction.businessobject.product.ProductDetail#getOldMaterialNumber()
	 */
	public String getOldMaterialNumber() {
		return oldMaterialNumber;
	}

	/* (non-Javadoc)
	 * @see com.sap.isa.auction.businessobject.product.ProductDetail#getVolume()
	 */
	public String getVolume() {
		return volume;
	}

	/* (non-Javadoc)
	 * @see com.sap.isa.auction.businessobject.product.ProductDetail#getVolumeUnit()
	 */
	public String getVolumeUnit() {
		return volumeUnit;
	}

	/* (non-Javadoc)
	 * @see com.sap.isa.auction.businessobject.product.ProductDetail#getWeightUnit()
	 */
	public String getWeightUnit() {
		return weightUnit;
	}

	/* (non-Javadoc)
	 * @see com.sap.isa.auction.businessobject.product.ProductDetail#setBaseUOM(java.lang.String)
	 */
	public void setBaseUOM(String baseUOM) {
		this.baseUOM = baseUOM;
	}

	/* (non-Javadoc)
	 * @see com.sap.isa.auction.businessobject.product.ProductDetail#setBasicText(java.lang.String)
	 */
	public void setBasicText(String text) {
		basicText = text;
	}

	/* (non-Javadoc)
	 * @see com.sap.isa.auction.businessobject.product.ProductDetail#setDimensions(java.lang.String)
	 */
	public void setDimensions(String dimension) {
		this.dimensions = dimension;
	}

	/* (non-Javadoc)
	 * @see com.sap.isa.auction.businessobject.product.ProductDetail#setEAN(java.lang.String)
	 */
	public void setEAN(String ean) {
		EAN = ean;
	}

	/* (non-Javadoc)
	 * @see com.sap.isa.auction.businessobject.product.ProductDetail#setEANCategory(java.lang.String)
	 */
	public void setEANCategory(String eanCategory) {
		EANCategory = eanCategory;
	}

	/* (non-Javadoc)
	 * @see com.sap.isa.auction.businessobject.product.ProductDetail#setGrossWeight(java.lang.String)
	 */
	public void setGrossWeight(String grossWeight) {
		this.grossWeight = grossWeight;
	}

	/* (non-Javadoc)
	 * @see com.sap.isa.auction.businessobject.product.ProductDetail#setManufactPartNumber(java.lang.String)
	 */
	public void setManufactPartNumber(String mpNum) {
		this.manufactPartNumber = mpNum;
	}

	/* (non-Javadoc)
	 * @see com.sap.isa.auction.businessobject.product.ProductDetail#setMaterialGroup(java.lang.String)
	 */
	public void setMaterialGroup(String mtrGroup) {
		materialGroup = mtrGroup;
	}

	/* (non-Javadoc)
	 * @see com.sap.isa.auction.businessobject.product.ProductDetail#setNetWeight(java.lang.String)
	 */
	public void setNetWeight(String netWeight) {
		this.netWeight = netWeight;
	}

	/* (non-Javadoc)
	 * @see com.sap.isa.auction.businessobject.product.ProductDetail#setOldMaterialNumber(java.lang.String)
	 */
	public void setOldMaterialNumber(String oldMtrNum) {
		oldMaterialNumber = oldMtrNum;
	}

	/* (non-Javadoc)
	 * @see com.sap.isa.auction.businessobject.product.ProductDetail#setVolume(java.lang.String)
	 */
	public void setVolume(String vol) {
		this.volume = vol;
	}

	/* (non-Javadoc)
	 * @see com.sap.isa.auction.businessobject.product.ProductDetail#setVolumeUnit(java.lang.String)
	 */
	public void setVolumeUnit(String volUnit) {
		volumeUnit = volUnit;
	}

	/* (non-Javadoc)
	 * @see com.sap.isa.auction.businessobject.product.ProductDetail#setWeightUnit(java.lang.String)
	 */
	public void setWeightUnit(String weightUnit) {
		this.weightUnit = weightUnit;
	}


	/* (non-Javadoc)
	 * @see com.sap.isa.auction.backend.boi.product.AuctionProductData#getBasicText(java.lang.String)
	 */
	public String getBasicText(String language) {
		return basicText;
	}

	/* (non-Javadoc)
	 * @see com.sap.isa.auction.businessobject.product.ProductDetail#getAttributeValByName(java.lang.String)
	 */
	public String getAttributeValByName(String attName) {
		return getValByIgnoreKeyNameCase(attName);
	}


	/* (non-Javadoc)
	 * @see com.sap.isa.auction.businessobject.product.ProductDetail#addAttribute(java.lang.String, java.lang.String)
	 */
	public void addAttribute(String attName, String attValue) {
		attPairs.put(attName, attValue);
		
	}


	/* (non-Javadoc)
	 * @see com.sap.isa.auction.businessobject.product.ProductDetail#getAllAttributeNames()
	 */
	public Enumeration getAllAttributeNames() {
		return attPairs.keys();
	}


	/* (non-Javadoc)
	 * @see com.sap.isa.auction.businessobject.product.ProductDetail#getAllAttributes()
	 */
	public Hashtable getAllAttributes() {
		return attPairs;
	}

	/**
	 * Make it case insensitive to ge the value from a key.
	 * @param attName
	 * @return the value of corresponding key.
	 */
	private String getValByIgnoreKeyNameCase (String attName){
		String val = null;
		String realName = null;
		Enumeration enu = getAllAttributeNames();
		while (enu.hasMoreElements()){
			String attributeName = (String)enu.nextElement();
			if (attributeName.equalsIgnoreCase(attName)){
				realName = attributeName;
				break;			
			}
		}
		if(realName !=  null){
			val = (String)attPairs.get(realName);
		}
		return val;
	}
}
