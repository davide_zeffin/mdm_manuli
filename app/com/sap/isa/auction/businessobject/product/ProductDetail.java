/*
 * Created on Oct 14, 2003
 *
 * To change the template for this generated file go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
package com.sap.isa.auction.businessobject.product;

import java.util.Enumeration;
import java.util.Hashtable;

import com.sap.isa.auction.backend.boi.product.*;
import com.sap.isa.auction.bean.ProductAttributeEnum;

/**
 * @author I803746
 *
 * To change the template for this generated type comment go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
public interface ProductDetail extends StatusCheckable, 
								AuctionProductData, ProductAttributeEnum{
	
	public boolean fromCatalog();
	
	public String getDescription();
	public void setDescription(String desc);
	
	public String getManufactPartNumber();
	public void setManufactPartNumber(String mpNum);
	
	public String getBaseUOM();
	public void setBaseUOM(String baseUOM);
	
	public String getMaterialGroup();
	public void setMaterialGroup(String mtrGroup);
	
	public String getOldMaterialNumber();
	public void setOldMaterialNumber(String oldMtrNum);
	
	public String getGrossWeight();
	public void setGrossWeight(String grossWeight);
	
	public String getWeightUnit();
	public void setWeightUnit(String weightUnit);
	
	public String getNetWeight();
	public void setNetWeight(String netWeight); 
	
	public String getVolume();
	public void setVolume(String vol);
	
	public String getVolumeUnit();
	public void setVolumeUnit(String volUnit);
	
	public String getDimensions();
	public void setDimensions(String dimension);
	
	public String getEAN();
	public void setEAN(String ean);
	
	public String getEANCategory();
	public void setEANCategory(String eanCategory);
	
	public String getBasicText();
	public void setBasicText(String text);
	
	public double getQuantity();
	public void setQuantity(double qunatity);
	
	/**
	 * Get the Attribute Value from Attribute Name
	 * @param attName
	 */
	public void addAttribute(String attName, String attValue);
	
	/**
	 * Get the attribute value for a given attribute name,
	 * if the no corresponding key exists, then return null
	 * instead of exception.
	 * @param The name of the attribute
	 * @return The value of the product attribute
	 */
	public String getAttributeValByName (String attName);
	
	/**
	 * 
	 * @return All the attribute names
	 */
	public Enumeration getAllAttributeNames ();
	
	/**
	 * 
	 * @return All the attribute key and value pairs
	 */
	public Hashtable getAllAttributes ();
}
