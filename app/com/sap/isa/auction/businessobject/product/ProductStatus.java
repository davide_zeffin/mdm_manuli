/*
 * Created on Sep 26, 2003
 *
 * To change the template for this generated file go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
package com.sap.isa.auction.businessobject.product;

import com.sap.isa.auction.backend.boi.product.ProductStatusData;

/**
 * @author I803746
 *
 * To change the template for this generated type comment go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
public class ProductStatus implements ProductStatusData {
	private int errorcode = ERRCODE_OK;
	//0: OK
	//1: Non-exist
	//2: Not available
	
	public int getErrorCode()
	{
		return errorcode;
	}

	public void setErrorCode(int code)
	{
		errorcode = code;
	}
	
	private boolean availability = false;
	/* (non-Javadoc)
	 * @see com.sap.isa.auction.backend.boi.product.ProductStatusData#isAvailable()
	 */
	public boolean isAvailable() {
		return availability;
	}

	/* (non-Javadoc)
	 * @see com.sap.isa.auction.backend.boi.product.ProductStatusData#setAvailable(boolean)
	 */
	public void setAvailable(boolean availability) {
		this.availability = availability;
	}


	private double quantity = 0; 
	/* (non-Javadoc)
	 * @see com.sap.isa.auction.backend.boi.product.ProductStatusData#getQunatity()
	 */
	public double getQunatity() {
		return quantity;
	}

	/* (non-Javadoc)
	 * @see com.sap.isa.auction.backend.boi.product.ProductStatusData#setQuantity(double)
	 */
	public void setQuantity(double qty) {
		quantity = qty;
	}

	private boolean exist = false;
	/* (non-Javadoc)
	 * @see com.sap.isa.auction.backend.boi.product.ProductStatusData#isExist()
	 */
	public boolean isExist() {
		return exist;
	}

	/* (non-Javadoc)
	 * @see com.sap.isa.auction.backend.boi.product.ProductStatusData#setExist()
	 */
	public void setExist(boolean ex) {
		exist = ex;
	}

	public String toString() {
		StringBuffer buffer = new StringBuffer();
		buffer.append("Exist=");
		buffer.append(this.exist);
		buffer.append(";Availability=");
		buffer.append(this.availability);
		buffer.append(";Quantity=");
		buffer.append(this.quantity);
		buffer.append(";Errorcode=");
		buffer.append(this.errorcode);
		return buffer.toString();
	}
}
