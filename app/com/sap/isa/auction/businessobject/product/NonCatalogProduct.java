/*
 * Created on Sep 18, 2003
 *
 * SAP Labs LLC, the eBay auction project, also is known as Selling via eBay. 
 * Selling via eBay application provides SAP R/3 (and in the future CRM) 
 * customers to auction and sell inventory/products onto an online marketplace 
 * eBay. The application will provide the ability to Create, Schedule, Publish, 
 * Monitor and Close Auction Listings onto eBay and handle all the relevant 
 * Backend processing for Customers and Orders. 

 */
package com.sap.isa.auction.businessobject.product;

import java.util.Enumeration;
import java.util.Hashtable;

import com.sap.isa.auction.backend.boi.PlantData;
import com.sap.isa.auction.backend.boi.SalesAreaData;
import com.sap.isa.auction.backend.boi.product.AuctionProductData;
import com.sap.isa.auction.backend.boi.product.ProductStatusData;
import com.sap.isa.backend.boi.isacore.ProductData;
import com.sap.isa.businessobject.BusinessObjectBase;
import com.sap.isa.core.TechKey;
import com.sap.isa.core.businessobject.BackendAware;
import com.sap.isa.core.eai.BackendObjectManager;
/**
 * @author i009657
 *
 * The noncatalog product is the reflection of the material master in
 * the backend, there is no catalog itme associated with this product
 */
public class NonCatalogProduct
	extends BusinessObjectBase
	implements BackendAware, ProductData, AuctionProductData, ProductDetail {
	
	// the product ID
	private String productId;
	private String description;

	private String manufactPartNumber = null;
	private String baseUOM = null;
	private String materialGroup = null;
	private String oldMaterialNumber = null;
	private String grossWeight = null;
	private String weightUnit = null;
	private String netWeight = null;
	private String volume = null;
	private String volumeUnit = null;
	private String dimensions = null;
	private String EAN = null;
	private String EANCategory = null;
	private String basicText = null;
	private PlantData plant = null;
	
	private SalesAreaData salesArea = null;
	private double quantity = 0.0;	
	
	private ProductStatusData status;
	protected Hashtable attPairs  = null;
	/*
	 * Constructor
	 *
	 * @param techKey  techKey of the product
	 *
	 */
	public NonCatalogProduct(TechKey techKey)
	{
	  setTechKey(techKey);
	  attPairs = new Hashtable();
	}


	/*
	 * Constructor
	 *
	 * @param techKey  techKey of the product
	 *
	 */
	public NonCatalogProduct()
	{
		attPairs = new Hashtable();
	}
	/*
	 * Constructor
	 *
	 * @param techKey  techKey of the product
	 * @param id product id
	 *
	 */
	public NonCatalogProduct(TechKey techKey, String id) {
	  setTechKey(techKey);
	  productId = id;
	  attPairs = new Hashtable();
	}


	/* (non-Javadoc)
	 * @see com.sap.isa.core.businessobject.BackendAware#setBackendObjectManager(com.sap.isa.core.eai.BackendObjectManager)
	 */
	public void setBackendObjectManager(BackendObjectManager arg0) {
		// TODO Auto-generated method stub

	}

	/* (non-Javadoc)
	 * @see com.sap.isa.backend.boi.isacore.ProductData#getCatalogItemAsObject()
	 */
	public Object getCatalogItemAsObject() {
		// TODO throws some exception here
		return null;
	}

	/* Return the description form the backend
	 * @return the description from backend
	 */
	public String getDescription() {
		// 
		return description;
	}

	/* The manufacture number
	 * @see com.sap.isa.auction.backend.boi.product.AuctionProductData#getManufactPartNumber()
	 */
	public String getManufactPartNumber() {
		return this.manufactPartNumber;
	}

	/* get the base UOM
	 * @see com.sap.isa.auction.backend.boi.product.AuctionProductData#getBaseUOM()
	 */
	public String getBaseUOM() {
		return this.baseUOM;
	}

	/* get the material group
	 * @return The material group
	 */
	public String getMaterialGroup() {
		return this.materialGroup;
	}

	/* get the old material number
	 * @return String the old material number
	 */
	public String getOldMaterialNumber() {
		return this.oldMaterialNumber;
	}

	/* Getter for the gross weight
	 * @return, the string for a gross weight
	 */
	public String getGrossWeight() {
		return this.grossWeight;
	}

	/* Getter for the weight unit
	 * @return the weight unit, it may be useful to publish 
	 * the weight unit for the product
	 */
	public String getWeightUnit() {
		return weightUnit;
	}

	/* Getter for the net weight
	 * @return string the net weight of a product
	 */
	public String getNetWeight() {
		return netWeight;
	}

	/* Getter for the volume
	 * @return, the volume of the auction product
	 */
	public String getVolume() {
		return volume;
	}

	/* Getter for for the volume unit 
	 * @return String the volume unit
	 */
	public String getVolumeUnit() {
		return volumeUnit;
	}

	/* Getter for the dimensions
	 * @return String the dimensions
	 */
	public String getDimensions() {
		return dimensions;
	}

	/* Getter for EAN data
	 * @return the EAN data as string
	 */
	public String getEAN() {
		return EAN;
	}

	/* Return the EAN category
	 * @return the EAN category
	 */
	public String getEANCategory() {
		return EANCategory;
	}

	/* Getter the basic text
	 * @return the basic text
	 */
	public String getBasicText(String language) {
		return basicText;
	}

	/**
	 * Returns the property available
	 *
	 * @return available
	 *
	 */
	public boolean isAvailable() {
		if(status != null)
	   		return status.isAvailable();
	   	else
	   		return false;
	}

	/**
	 * @param string
	 */
	public void setBaseUOM(String string) {
		baseUOM = string;
	}

	/**
	 * @param string
	 */
	public void setBasicText(String string) {
		basicText = string;
	}

	/**
	 * @param string
	 */
	public void setDescription(String string) {
		description = string;
	}

	/**
	 * @return the plant data
	 */
	public PlantData getPlant() {
		return plant;
	}

	/**
	 * Setter the plant data
	 * @param the plant data
	 */
	public void setPlant(PlantData data) {
		plant = data;
	}


	/**
	 * @return the quantity for the checking purpose
	 */
	public double getQuantity() {
		return quantity;
	}

	/**
	 * @return the sales area belongs to this
	 */
	public SalesAreaData getSalesArea() {
		return salesArea;
	}

	/**
	 * setter for the product quantity
	 * @param decimal
	 */
	public void setQuantity(double decimal) {
		quantity = decimal;
	}

	/**
	 * setter for the sales area
	 * @param data
	 */
	public void setSalesArea(SalesAreaData data) {
		salesArea = data;
	}

	/**
	 * @return
	 */
	public String getProductId() {
		return productId;
	}

	/**
	 * @param string
	 */
	public void setProductId(String string) {
		productId = string;
	}

	/**
	 * @return
	 */
	public ProductStatusData getStatus() {
		return status;
	}

	/**
	 * @param data
	 */
	public void setStatus(ProductStatusData data) {
		status = data;
	}
	
	public String getUnit()
	{
		return this.getVolumeUnit();
	}

	/**
	 * @param string
	 */
	public void setVolumeUnit(String string) {
		volumeUnit = string;
	}
	
	public boolean fromCatalog()
	{
		return false;
	}
	
	/* (non-Javadoc)
	 * @see com.sap.isa.auction.businessobject.product.ProductDetail#getBasicText()
	 */
	public String getBasicText() {
		return basicText;
	}

	/* (non-Javadoc)
	 * @see com.sap.isa.auction.businessobject.product.ProductDetail#setDimensions(java.lang.String)
	 */
	public void setDimensions(String dimension) {
		this.dimensions = dimension;
	}

	/* (non-Javadoc)
	 * @see com.sap.isa.auction.businessobject.product.ProductDetail#setEAN(java.lang.String)
	 */
	public void setEAN(String ean) {
		this.EAN = ean;
	}

	/* (non-Javadoc)
	 * @see com.sap.isa.auction.businessobject.product.ProductDetail#setEANCategory(java.lang.String)
	 */
	public void setEANCategory(String eanCategory) {
		this.EANCategory = eanCategory;
	}

	/* (non-Javadoc)
	 * @see com.sap.isa.auction.businessobject.product.ProductDetail#setGrossWeight(java.lang.String)
	 */
	public void setGrossWeight(String grossWeight) {
		this.grossWeight = grossWeight;
	}

	/* (non-Javadoc)
	 * @see com.sap.isa.auction.businessobject.product.ProductDetail#setManufactPartNumber(java.lang.String)
	 */
	public void setManufactPartNumber(String mpNum) {
		manufactPartNumber = mpNum;
	}

	/* (non-Javadoc)
	 * @see com.sap.isa.auction.businessobject.product.ProductDetail#setMaterialGroup(java.lang.String)
	 */
	public void setMaterialGroup(String mtrGroup) {
		this.materialGroup = mtrGroup;
	}

	/* (non-Javadoc)
	 * @see com.sap.isa.auction.businessobject.product.ProductDetail#setNetWeight(java.lang.String)
	 */
	public void setNetWeight(String netWeight) {
		this.netWeight = netWeight;
	}

	/* (non-Javadoc)
	 * @see com.sap.isa.auction.businessobject.product.ProductDetail#setOldMaterialNumber(java.lang.String)
	 */
	public void setOldMaterialNumber(String oldMtrNum) {
		this.oldMaterialNumber = oldMtrNum;
	}

	/* (non-Javadoc)
	 * @see com.sap.isa.auction.businessobject.product.ProductDetail#setVolume(java.lang.String)
	 */
	public void setVolume(String vol) {
		this.volume = vol;
	}

	/* (non-Javadoc)
	 * @see com.sap.isa.auction.businessobject.product.ProductDetail#setWeightUnit(java.lang.String)
	 */
	public void setWeightUnit(String weightUnit) {
		this.weightUnit = weightUnit;
	}


	/* (non-Javadoc)
	 * @see com.sap.isa.backend.boi.isacore.ProductData#setId(java.lang.String)
	 */
	public void setId(String arg0) {
		productId = arg0;
	}


	/* (non-Javadoc)
	 * @see com.sap.isa.backend.boi.isacore.ProductData#getId()
	 */
	public String getId() {
		return productId;
	}


	/* (non-Javadoc)
	 * @see com.sap.isa.backend.boi.isacore.ProductData#setUnit(java.lang.String)
	 */
	public void setUnit(String arg0) {
		volumeUnit = arg0;
	}

	/* (non-Javadoc)
	 * @see com.sap.isa.auction.businessobject.product.ProductDetail#getAttributeValByName(java.lang.String)
	 */
	public String getAttributeValByName(String attName) {
		return getValByIgnoreKeyNameCase(attName);
	}

	/* (non-Javadoc)
	 * @see com.sap.isa.auction.businessobject.product.ProductDetail#addAttribute(java.lang.String, java.lang.String)
	 */
	public void addAttribute(String attName, String attValue) {
		attPairs.put(attName, attValue);
		
	}


	/* (non-Javadoc)
	 * @see com.sap.isa.auction.businessobject.product.ProductDetail#getAllAttributeNames()
	 */
	public Enumeration getAllAttributeNames() {
		return attPairs.keys();
	}


	/* (non-Javadoc)
	 * @see com.sap.isa.auction.businessobject.product.ProductDetail#getAllAttributes()
	 */
	public Hashtable getAllAttributes() {
		return attPairs;
	}
	
	/**
	 * Make it case insensitive to ge the value from a key.
	 * @param attName
	 * @return the value of corresponding key.
	 */
	private String getValByIgnoreKeyNameCase (String attName){
		String val = null;
		String realName = null;
		Enumeration enu = getAllAttributeNames();
		while (enu.hasMoreElements()){
			String attributeName = (String)enu.nextElement();
			if (attributeName.equalsIgnoreCase(attName)){
				realName = attributeName;
				break;			
			}
		}
		if(realName !=  null){
			val = (String)attPairs.get(realName);
		}
		return val;
	}

}
