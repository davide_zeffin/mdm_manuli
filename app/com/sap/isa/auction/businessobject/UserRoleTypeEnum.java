/*
 * Created on Nov 30, 2004
 *
 * To change the template for this generated file go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
package com.sap.isa.auction.businessobject;

import java.io.Serializable;

/**
 * Title:        Internet Sales
 * Description:
 * Copyright:    Copyright (c) 2002
 * Company:      SAPMarkets
 * @author
 * @version 1.0
 */

public class UserRoleTypeEnum implements Serializable {
	
	private static final String STR_SELLER = "SELLER";
	private static final String STR_ADMIN = "ADMIN";
	private static final String STR_BUYER = "BUYER";
	private static final String STR_NONE = "NONE";

	private static final int INT_NONE = 0;	
	private static final int INT_ADMIN = 1;
	private static final int INT_SELLER = 2;
	private static final int INT_BUYER = 9;

	public static final UserRoleTypeEnum NONE = new UserRoleTypeEnum(STR_NONE,INT_NONE);
	public static final UserRoleTypeEnum SELLER = new UserRoleTypeEnum(STR_SELLER,INT_SELLER);
	public static final UserRoleTypeEnum ADMIN = new UserRoleTypeEnum(STR_ADMIN,INT_ADMIN);
	public static final UserRoleTypeEnum BUYER = new UserRoleTypeEnum(STR_BUYER,INT_BUYER);

	private String name;
	private int value;

	private UserRoleTypeEnum(String name, int value) {
		this.name = name;
		this.value = value;
	}

	public int getEBayValue() {
		return getValue();
	}
	
	public int getValue() {
		return value;
	}

	public String toString() {
		return "[" + name + " = " + value + "]";
	}

	public static final int toInt(UserRoleTypeEnum enum) {
		return enum.getValue();
	}

	public static final UserRoleTypeEnum toEnum(int value) {
		switch(value) {
			case INT_ADMIN: return ADMIN;
			case INT_BUYER: return BUYER;
			case INT_SELLER: return SELLER;
			case INT_NONE: return NONE;
			default: throw new IllegalArgumentException("Enum = " + value + " is invalid");
		}
	}
}
