/*
 * Auctioning Via Webshop XCM config container
 */
package com.sap.isa.auction.config;

import java.util.ArrayList;
import java.util.Collection;

import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import com.sap.tc.logging.Category;
import com.sap.tc.logging.Location;
import com.sap.isa.auction.logging.CategoryProvider;

/**
 * 
 */
public class AVWSellerXCMConfigContainer extends XCMConfigContainer {

	private static Category logger =
		CategoryProvider.getSystemCategory();
		
	private static Location tracer =
		Location.getLocation(AVWSellerXCMConfigContainer.class.getName());
				
	/* 
	 * AVW specific Scheduler configutation 
	 * @see com.sap.isa.auction.config.XCMConfigContainer#loadSchedulerTasksConfig(org.w3c.dom.Node)
	 */
	void loadSchedulerTasksConfig(Node node) {
		if (node == null) {
			logger.errorT(tracer,"Failed to load Scheduler task Config, node is null");
			return;
		}

		NamedNodeMap attrMap = node.getAttributes();
		Collection taskScheduleColl = new ArrayList();
		NodeList children = node.getChildNodes();
		int size = children.getLength();
		for (int i = 0; i < size; i++) {
			Node child = children.item(i);
			if (child.getNodeType() == Node.ELEMENT_NODE) {
				String name = child.getNodeName();
				SchedulerConfig.TaskSchedule taskHolder = null;
				if (name.equals("params")) {
					NamedNodeMap childAttrMap = child.getAttributes();
					String id = childAttrMap.getNamedItem("id").getNodeValue();
					taskHolder = new SchedulerConfig.TaskSchedule();
					loadSchedulerTaskData(child, taskHolder);
					if (taskHolder != null)
						taskScheduleColl.add(taskHolder);
				}
			}
		}
		schedulerTasksConfig = taskScheduleColl;


	}

}
