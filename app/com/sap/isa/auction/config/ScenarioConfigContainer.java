/*****************************************************************************
  Copyright (c) 2003, SAP Labs, All rights reserved.
  Author:       Pavan Bayyapu
  Created:      

  Revision: #
  Date: Oct 6, 2003
*****************************************************************************/
package com.sap.isa.auction.config;

import java.util.Map;

import com.sap.tc.logging.Location;
import com.sap.isa.core.interaction.InteractionConfig;
import com.sap.isa.core.interaction.InteractionConfigContainer;


/**
 * Container to hold the configurable properties
 */

public class ScenarioConfigContainer {

	public static final String AUCTION_SCENARIO_CONFIG = ScenarioConfigContainer.class.getName();
	
	private final static Location loc =
		Location.getLocation(ScenarioConfigContainer.class);

	/**
	 * JCO parameters of the current scenario. Needed to maintain MANDANT in standalone persistance
	 */
	private Map jcoParams = null;
		

	/**
	 * Current xcm scenario
	 */
	private String xcmScenario = null;

	/**
	 * Default constructor
	 */
	public ScenarioConfigContainer() {
	}
	
	/**
	 * Construct with scenario
	 * @param scenario
	 */
	public ScenarioConfigContainer (String scenario){
		this();
		xcmScenario = scenario;		
	}

	/**
	 * loads configuration from <code> InteractionConfigContainer </code>
	 * @param config InteractionConfigContainer containing xcm configuration for current scenario
	 * @return
	 */
	public boolean loadConfiguration(InteractionConfigContainer config){
		InteractionConfig jcoconfig = config.getConfig("jco");
	
		if(jcoconfig != null)
			setJcoParams(jcoconfig.getParams());
		else
			loc.debugT("JCO params is not found for the scenario");
			
			
		return true; 
		
	}
	
	/**
	 * @return
	 */
	public String getXcmScenario() {
		return xcmScenario;
	}

	/**
	 * @param string
	 */
	public void setXcmScenario(String scenario) {
		xcmScenario = scenario;
	}

	/**
	 * @return
	 */
	public Map getJcoParams() {
		return jcoParams;
	}

	/**
	 * @param map
	 */
	public void setJcoParams(Map map) {
		jcoParams = map;
	}

}
