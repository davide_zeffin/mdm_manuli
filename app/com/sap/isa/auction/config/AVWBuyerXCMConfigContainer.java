
package com.sap.isa.auction.config;

import org.w3c.dom.Node;

import com.sap.tc.logging.Category;
import com.sap.tc.logging.Location;
import com.sap.isa.auction.logging.CategoryProvider;

/*
 * Created on May 10, 04
 *
 */


/**
 * 
 * This class holds the XCM configuration data for Auctioning via webshop
 * buyer side of the application
 */

public class AVWBuyerXCMConfigContainer extends XCMConfigContainer{

	private static Category logger =
		CategoryProvider.getSystemCategory();
	public String auctionConfig = null;
	
	private static Location tracer =
		Location.getLocation(AVWBuyerXCMConfigContainer.class.getName());

	public AVWBuyerXCMConfigContainer(){
		
	}
	
	protected void initConfigContainer(String scenarioName)  {
		super.initConfigContainer(scenarioName);		
		auctionConfig = configContainer.getConfigParams().getParamValue("auctiondata");
	}
	public boolean isAuctionEnabled(){
		return auctionConfig != null ; 
	}
	
	protected Node getJDONode() {
		Node node = getConfigParamsNode("auction", auctionConfig, null, false);
		return node;
	}


	/* (non-Javadoc)
	 * @see com.sap.isa.auction.config.XCMConfigContainer#loadSchedulerTasksConfig(org.w3c.dom.Node)
	 */
	void loadSchedulerTasksConfig(Node node) {
		// TODO Auto-generated method stub
	}
	
}

