/*
 * Created on Oct 17, 2003
 *
 */
package com.sap.isa.auction.config;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;

import org.w3c.dom.Document;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import com.sap.engine.lib.xml.dom.xpath.XPathEvaluatorImpl;
import com.sap.engine.lib.xml.dom.xpath.XPathExpressionImpl;
import com.sap.engine.lib.xml.dom.xpath.XPathResultImpl;
import com.sap.tc.logging.Category;
import com.sap.tc.logging.Location;
import com.sap.isa.auction.backend.boi.user.AuctionUserBaseData;
import com.sap.isa.auction.businessobject.InvocationContext;
import com.sap.isa.auction.logging.CategoryProvider;
import com.sap.isa.auction.logging.LogHelper;
import com.sap.isa.core.FrameworkConfigManager;
import com.sap.isa.core.xcm.ConfigContainer;

/**
 * 
 * This class holds the XCM configuration data common to all Auction scenarios
 * including SellingViaeBay and auctioningViawebshop
 * It provides a facility to register Configuration data changes
 * listeners as well
 * 
 * Common configurations include JDO and scheduler
 */

public abstract class XCMConfigContainer extends ApplicationConfigContainer {

	private static Category logger =
		CategoryProvider.getSystemCategory();

	public static Class XCMConfigContainerClass = XCMConfigContainer.class;
	
	private static Location tracer =
		Location.getLocation(XCMConfigContainer.class.getName());
		

	private static final String JDOCOMPONENT = "jdo";
	private static final String SCHEDULERTASKCONFIGURATORCOMPONENT =
		"schedulerTasksConfigurator";
	//	private static XCMConfigContainer singleton;

	//	/**
	//	 * XCM Default Scenario Config for Application Scope Data
	//	 */
	protected ConfigContainer configContainer;
	protected String scenarioName;

	protected static HashMap scenarios2CC = new HashMap();

	/*
	 * NOTE: These configs are cached, so these may get stale in case Application is deployed
	 * across clustered node.
	 */


	/**
	 * JDO configuration
	 */
	protected JDOConfig jdoConfig = null;
	/**
	 * eBay and HttpClient properties
	 */
	protected Collection schedulerTasksConfig = null;



	protected XCMConfigContainer(String scenarioName) {
	}
	
	protected void initConfigContainer(String scenarioName)  {
		configContainer =
			FrameworkConfigManager.XCM.getXCMScenarioConfig(scenarioName);
		this.scenarioName = scenarioName;
	}
	
	public XCMConfigContainer()  {
		
	}

	public static synchronized XCMConfigContainer getDefaultScenarioBaseInstance() {
		return getBaseInstance(
			(FrameworkConfigManager.XCM.getDefaultXCMScenarioName()));
	}

	public static synchronized XCMConfigContainer getBaseInstance(String scenarioName) {
		XCMConfigContainer container =
			(XCMConfigContainer) scenarios2CC.get(scenarioName);
		if (container == null) {
			try  {
				container = (XCMConfigContainer)XCMConfigContainerClass.newInstance();
			}  catch(Exception exc)  {
				LogHelper.logError(logger , tracer , "Unable to Instantiate XCMConfigContainer" , exc);
			}
			container.initConfigContainer(scenarioName);
			scenarios2CC.put(scenarioName, container);
		}
		return container;
	}
	
	private String[] getClientAndSystemId(InvocationContext ic) {
		AuctionUserBaseData auctionUser = (AuctionUserBaseData) ic.getUser();
		return new String[] {
			auctionUser.getClient(),
			auctionUser.getSystemId()};
	}

	private void appendSAPSystemFilter(
		StringBuffer filter,
		InvocationContext ic) {
		AuctionUserBaseData user = (AuctionUserBaseData) ic.getUser();
		if (user == null)
			return;
		filter.append(" && ");
		filter.append("client");
		filter.append(" == ");
		filter.append("\"");
		filter.append(user.getClient());
		filter.append("\"");
		filter.append(" && ");
		filter.append("systemId");
		filter.append(" == ");
		filter.append("\"");
		filter.append(user.getSystemId());
		filter.append("\"");

	}


	public JDOConfig getJDOConfig() {
		JDOConfig jdoConfig = null;
		if (jdoConfig == null) {
			JDOConfig config = new JDOConfig();
			Node node = getJDONode();
			loadJDOData(node, config);
			jdoConfig = config;
		}
		return jdoConfig;
	}

	protected Node getJDONode() {
		String key =
			configContainer.getConfigParams().getParamValue("jdodata");
		Node node = getConfigParamsNode(JDOCOMPONENT, key, null, false);
		return node;
	}
	
	public Collection getSchedulerTaskConfig() {
		if (schedulerTasksConfig == null) {
			Collection coll = new ArrayList();
			String key =
				configContainer.getConfigParams().getParamValue(
					"schedulerTasksConfigData");
			Document doc =
				configContainer.getConfigUsingAliasAsDocument("config-data");
			XPathEvaluatorImpl eval = new XPathEvaluatorImpl();
			// XPATH = //configs/config[@id = \"scheduler\"]/*
			String path = "//configs/config[@id = \"" + key + "\"]";
			// extract all param nodes
			// Add the scheduler component name
			path =
				"//component[@id = \""
					+ SCHEDULERTASKCONFIGURATORCOMPONENT
					+ "\"]"
					+ path;
			// extract all param nodes
			XPathExpressionImpl expr = eval.createExpression(path, null);
			XPathResultImpl res = new XPathResultImpl();
			res = expr.evaluate(doc, (short) 0, res);
			Node node = res.getSingleNodeValue();
			loadSchedulerTasksConfig(node);
		}
		return schedulerTasksConfig;
	}

	/**
	 * @param node Config node for Scheduler
	 */
	abstract void loadSchedulerTasksConfig(Node node) ;
	
	protected void loadSchedulerTaskData(
		Node node,
		SchedulerConfig.TaskSchedule holder) {
		try {
			NodeList children = node.getChildNodes();
			int size = children.getLength();
			for (int i = 0; i < size; i++) {
				Node child = children.item(i);
				if (child.getNodeType() == Node.ELEMENT_NODE) {
					String nodeName = child.getNodeName();
					if (nodeName.equals("param")) {
						NamedNodeMap attrMap = child.getAttributes();
						String name =
							attrMap.getNamedItem("name").getNodeValue();
						String value =
							attrMap.getNamedItem("value").getNodeValue();
						if (name.equals("name")) {
							holder.setTaskName(value);
						} else if (
							name.equals("enabled")
								&& (!(value == null || value.length() == 0))) {
							holder.setEnabled(
								Boolean.valueOf(value).booleanValue());
						} else if (name.equals("cronExpr")) {
							holder.setCronExp(value);
						} else if (
							name.equals("period")
								&& (!(value == null || value.length() == 0))) {
							holder.setPeriod(Long.parseLong(value));
						} else if (
							name.equals("endDate")
								&& (!(value == null || value.length() == 0))) {
							holder.setEndTime(Long.parseLong(value));
						}
					}
				}
			}
		} catch (Exception e) {
			LogHelper.logError(logger,tracer,"Failed to Scheduler Config task data", e);
		}
	}
	
	/**
	 * Gets the Dom node corresponding to the 
	 * @param configName Name of the configuration for which the child elements need to be obtained
	 * @return DOM node corresponding to the confignode
	 */
	protected synchronized Node getConfigParamsNode(
		String componentName,
		String configName,
		String paramsId,
		boolean appScope) {
		Node node = null;
		Document doc = null;
		if (appScope) {
			doc =
				configContainer.getConfigUsingAliasAsDocument("auction-config");
		} else {
			doc = configContainer.getConfigUsingAliasAsDocument("config-data");
		}

		XPathEvaluatorImpl eval = new XPathEvaluatorImpl();
		String path = "//configs/config[@id = \"" + configName + "\"]/params";
		// extract all param nodes
		if (componentName != null) {
			path = "//component[@id = \"" + componentName + "\"]" + path;
		}
		if (paramsId != null)
			path += "[@id = \"" + paramsId + "\"]";
		XPathExpressionImpl expr = eval.createExpression(path, null);
		XPathResultImpl res = new XPathResultImpl();
		res = expr.evaluate(doc, (short) 0, res);
		node = res.getSingleNodeValue();
		return node;
	}

	/**
	 * @return
	 */
	public ConfigContainer getConfigContainer() {
		return configContainer;
	}

}
