package com.sap.isa.auction.config;

/**
 * Title:
 * Description:
 * Copyright:    Copyright (c) 2003
 * Company:
 * @author
 * @version 1.0
 */

public class JDBCConfig implements Cloneable {

    private String driver;
    private String user;
    private String pwd;
    private String url;
    private String jndiName;

    JDBCConfig() {
    }

    public String getDriverClassName() {
        return this.driver;
    }

    void setDriverClassName(String val) {
        this.driver = val;
    }

    public String getUser() {
        return this.user;
    }

    public void setUser(String val) {
        this.user = val;
    }

    public String getPassword() {
        return this.pwd;
    }

    public void setPassword(String val) {
        this.pwd = val;
    }

    public String getURL() {
        return this.url;
    }

    public void setURL(String val) {
        this.url = val;
    }

    public String getDataSourceJNDIName() {
        return this.jndiName;

    }

    public void setDataSourceJNDIName(String val) {
        this.jndiName = val;
    }
    
    public Object clone() {
    	try {
			return super.clone();
		} catch (CloneNotSupportedException e) {
			return null;
		}
    }
}