/*
 * Created on Oct 17, 2003
 *
 */
package com.sap.isa.auction.config;

import java.util.HashMap;
import java.util.Map;

import javax.jdo.JDOException;

import org.w3c.dom.Document;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import com.sap.engine.lib.xml.dom.xpath.XPathEvaluatorImpl;
import com.sap.engine.lib.xml.dom.xpath.XPathExpressionImpl;
import com.sap.engine.lib.xml.dom.xpath.XPathResultImpl;
import com.sap.tc.logging.Category;
import com.sap.tc.logging.Location;
import com.sap.isa.auction.exception.SystemException;
import com.sap.isa.auction.exception.i18n.ErrorMessageBundle;
import com.sap.isa.auction.exception.i18n.I18nErrorMessage;
import com.sap.isa.auction.logging.CategoryProvider;
import com.sap.isa.auction.logging.LogHelper;
import com.sap.isa.core.FrameworkConfigManager;
import com.sap.isa.core.xcm.ConfigContainer;

/**
 * 
 * This class holds the Application scope configuration data
 * It provides a facility to register Configuration data changes
 * listeners as well
 * 
 * This container provides the Configs for Global data that needs to be loaded for Application Components
 * like Scheduler and JDO
 */

public class ApplicationConfigContainer {

	private static Category logger = CategoryProvider.getSystemCategory();

	private static Location tracer =
		Location.getLocation(ApplicationConfigContainer.class.getName());

	private static final String SCHEDULERCOMPONENT = "scheduler";

	private static ApplicationConfigContainer singleton;

	/**
	 * XCM Default Scenario Config for Application Scope Data
	 */
	protected ConfigContainer configContainer;

	/*
	 * NOTE: These configs are cached, so these may get stale in case Application is deployed
	 * across clustered node.
	 */

	/**
	 * Scheduler Config
	 */
	private SchedulerConfig sc;

	protected ApplicationConfigContainer() {
		String defaultScenario =
			FrameworkConfigManager.XCM.getDefaultXCMScenarioName();
		configContainer =
			FrameworkConfigManager.XCM.getXCMScenarioConfig(defaultScenario);
	}

	public static synchronized ApplicationConfigContainer getApplicationConfigContainer() {
		if (singleton == null) {
			singleton = new ApplicationConfigContainer();
		}
		return singleton;
	}

	protected void handleException(Exception ex) {
		ErrorMessageBundle bundle = ErrorMessageBundle.getInstance();
		I18nErrorMessage msg = bundle.newI18nErrorMessage();
		if (ex instanceof JDOException) {
			msg.setPatternKey(SystemException.JDO_ERROR);
		} else {
			msg.setPatternKey(SystemException.INTERNAL_ERROR);
		}
		SystemException sysex = new SystemException(msg, ex);
		LogHelper.logError(logger, tracer, sysex.getMessage(), ex);
		throw sysex;
	}

	public Map getConfigParamsFromNode(
		String componentName,
		String configName,
		String paramsNodeName,
		boolean appScope) {
		Map map = new HashMap();
		Node node =
			getConfigParamsNode(
				componentName,
				configName,
				paramsNodeName,
				appScope);
		try {
			NodeList children = node.getChildNodes();
			int size = children.getLength();
			for (int i = 0; i < size; i++) {
				Node child = children.item(i);
				if (child.getNodeType() == Node.ELEMENT_NODE) {
					String nodeName = child.getNodeName();
					if (nodeName.equals("param")) {
						NamedNodeMap attrMap = child.getAttributes();
						String name =
							attrMap.getNamedItem("name").getNodeValue();
						String value =
							attrMap.getNamedItem("value").getNodeValue();
						map.put(name, value);
					}
				}
			}
		} catch (Exception e) {
			LogHelper.logError(
				logger,
				tracer,
				"Failed to initialize email properties",
				e);
		}
		return map;
	}

	/**
	 * Gets the Dom node corresponding to the 
	 * @param configName Name of the configuration for which the child elements need to be obtained
	 * @return DOM node corresponding to the confignode
	 */
	protected synchronized Node getConfigParamsNode(
		String componentName,
		String configName,
		String paramsId,
		boolean appScope) {
		Node node = null;
		Document doc = null;
		if (appScope) {
			doc =
				configContainer.getConfigUsingAliasAsDocument("auction-config");
		} else {
			doc = configContainer.getConfigUsingAliasAsDocument("config-data");
		}

		XPathEvaluatorImpl eval = new XPathEvaluatorImpl();
		String path = "//configs/config[@id = \"" + configName + "\"]/params";
		// extract all param nodes
		if (componentName != null) {
			path = "//component[@id = \"" + componentName + "\"]" + path;
		}
		if (paramsId != null)
			path += "[@id = \"" + paramsId + "\"]";
		XPathExpressionImpl expr = eval.createExpression(path, null);
		XPathResultImpl res = new XPathResultImpl();
		res = expr.evaluate(doc, (short) 0, res);
		node = res.getSingleNodeValue();
		return node;
	}

	public synchronized SchedulerConfig getSchedulerConfig() {
		synchronized (XCMConfigContainer.class) {
			if (sc == null) {
				Document doc =
					configContainer.getConfigUsingAliasAsDocument(
						"auction-config");
				XPathEvaluatorImpl eval = new XPathEvaluatorImpl();
				// XPATH = //configs/config[@id = \"scheduler\"]/*
				String path = "//configs/config[@id = \"scheduler\"]";
				// extract all param nodes
				XPathExpressionImpl expr = eval.createExpression(path, null);
				XPathResultImpl res = new XPathResultImpl();
				res = expr.evaluate(doc, (short) 0, res);
				Node node = res.getSingleNodeValue();
				loadSchedulerConfig(node);
			}
		}
		return sc;
	}

	/**
	 * Loads JDO configuration from dom node into configuration object
	 * @param node
	 * @param holder
	 */
	protected void loadJDOData(Node node, JDOConfig holder) {
		try {
			NodeList children = node.getChildNodes();
			int size = children.getLength();
			for (int i = 0; i < size; i++) {
				Node child = children.item(i);
				if (child.getNodeType() == Node.ELEMENT_NODE) {
					String nodeName = child.getNodeName();
					if (nodeName.equals("param")) {
						NamedNodeMap attrMap = child.getAttributes();
						String name =
							attrMap.getNamedItem("name").getNodeValue();
						String value =
							attrMap.getNamedItem("value").getNodeValue();
						if (name.equals("driver")) {
							holder.setDriverClassName(value);
						} else if (name.equals("url")) {
							holder.setURL(value);
						} else if (name.equals("user")) {
							holder.setUser(value);
						} else if (name.equals("password")) {
							holder.setPassword(value);
						} else if (name.equals("jndiDataSource")) {
							holder.setDataSourceJNDIName(value);
						} else if (name.equals("jndijdo")) {
							holder.setJdoJNDIName(value);
						} else if (name.equals("pmFactory")) {
							holder.setPMFClassName(value);
						} else if (name.equals("connectionFactory")) {
							holder.setConnectionFactoryName(value);
						}
					}
				}
			}
		} catch (Exception e) {
			LogHelper.logError(
				logger,
				tracer,
				"Failed to Scheduler Config JDO data",
				e);
		}
	}

	/**
	 * @param node Config node for Scheduler
	 */
	private void loadSchedulerConfig(Node node) {
		final String METHOD = "loadSchedulerConfig";
		if (node == null) {
			logger.errorT(
				tracer,
				"Failed to load Scheduler Config, node is null");
			return;
		}

		SchedulerConfig holder = new SchedulerConfig();

		NamedNodeMap attrMap = node.getAttributes();
		if (!attrMap
			.getNamedItem("id")
			.getNodeValue()
			.equalsIgnoreCase("scheduler")) {
			logger.errorT(tracer, METHOD, "Failed to load Scheduler Config");
			return;
		}

		NodeList children = node.getChildNodes();
		int size = children.getLength();
		for (int i = 0; i < size; i++) {
			Node child = children.item(i);
			if (child.getNodeType() == Node.ELEMENT_NODE) {
				String name = child.getNodeName();

				if (name.equals("params")) {
					NamedNodeMap childAttrMap = child.getAttributes();
					String id = childAttrMap.getNamedItem("id").getNodeValue();
					if (id.equals("schedulerData")) {
						loadSchedulerData(child, holder);
					} else if (id.equals("jdoData")) {
						JDOConfig jdoConfig = new JDOConfig();
						loadJDOData(child, jdoConfig);
						holder.setJDOConfig(jdoConfig);
					}
					//					else if(id.equals("categoriesSynchTaskData")) {
					//						SchedulerConfig.TaskSchedule taskHolder = new SchedulerConfig.TaskSchedule();
					//						loadSchedulerTaskData(child,taskHolder);
					////						holder.addTaskSchedule(taskHolder);
					//					}
					//					else if(id.equals("winnerPollTaskData")) {
					//						SchedulerConfig.TaskSchedule taskHolder = new SchedulerConfig.TaskSchedule();
					//						loadSchedulerTaskData(child,taskHolder);
					////						holder.addTaskSchedule(taskHolder);
					//					}
					//					else if(id.equals("updateAuctionStatusWithoutWinnerTaskData")) {
					//						SchedulerConfig.TaskSchedule taskHolder = new SchedulerConfig.TaskSchedule();
					//						loadSchedulerTaskData(child,taskHolder);
					////						holder.addTaskSchedule(taskHolder);
					//					}					
					//					else if(id.equals("bidPollTaskData")) {
					//						SchedulerConfig.TaskSchedule taskHolder = new SchedulerConfig.TaskSchedule();
					//						loadSchedulerTaskData(child,taskHolder);
					////						holder.addTaskSchedule(taskHolder);
					//					}
					//					else if(id.equals("publishAuctionTaskData")) {
					//						SchedulerConfig.TaskSchedule taskHolder = new SchedulerConfig.TaskSchedule();
					//						loadSchedulerTaskData(child,taskHolder);
					////						holder.addTaskSchedule(taskHolder);
					//					}
					//					else if(id.equals("feedbackSynchTaskData")) {
					//						SchedulerConfig.TaskSchedule taskHolder = new SchedulerConfig.TaskSchedule();
					//						loadSchedulerTaskData(child,taskHolder);
					////						holder.addTaskSchedule(taskHolder);
					//					}
					//					else if(id.equals("bankTxProcessTaskData")) {
					//						SchedulerConfig.TaskSchedule taskHolder = new SchedulerConfig.TaskSchedule();
					//						loadSchedulerTaskData(child,taskHolder);
					////						holder.addTaskSchedule(taskHolder);
					//					}
				}
			}
		}
		sc = holder;
	}
	private void loadSchedulerData(Node node, SchedulerConfig holder) {
		try {
			NodeList children = node.getChildNodes();
			int size = children.getLength();
			for (int i = 0; i < size; i++) {
				Node child = children.item(i);
				if (child.getNodeType() == Node.ELEMENT_NODE) {
					String nodeName = child.getNodeName();
					if (nodeName.equals("param")) {
						NamedNodeMap attrMap = child.getAttributes();
						String name =
							attrMap.getNamedItem("name").getNodeValue();
						String value =
							attrMap.getNamedItem("value").getNodeValue();
						if (name.equals("priority")) {
						} else if (name.equals("maxThreads")) {
							holder.setMaxThreads(Integer.parseInt(value));
						} else if (
							name.equals("enabled")
								&& (!(value == null || value.length() == 0))) {
							holder.setRunScheduler(
								Boolean.valueOf(value).booleanValue());
						} else if (
							name.equals("minThreads")
								&& (!(value == null || value.length() == 0))) {
							holder.setMinThreads(Integer.parseInt(value));
						} else if (
							name.equals("idleTime")
								&& (!(value == null || value.length() == 0))) {
							holder.setIdleTime(Integer.parseInt(value));
						} else if (name.equals("schedulerClass")) {
							holder.setSchedulerClass(value);
						} else if (name.equals("useOnlyOpenSQLConn")) {
							if (value != null && value.trim().length() != 0) {
								if (value.equalsIgnoreCase("true"))
									holder.setUseOpenSQL(true);
								else
									holder.setUseOpenSQL(false);
							}
						} else if (
							name.equals("jobthreadpool.maxusedinterval")) {
							if (value != null && value.trim().length() != 0) {
								holder.setJobThreadPoolMaxusedInt(
									Long.parseLong(value));
							}
						} else if (name.equals("jobthreadpool.gcinterval")) {
							if (value != null && value.length() != 0) {
								holder.setJobThreadPoolGCInt(
									Long.parseLong(value));
							}

						} else if (
							name.equals("jobthreadpool.idletimeoutinterval")) {
							if (value != null && value.trim().length() != 0) {
								holder.setJobThreadPoolIdleTimeoutInt(
									Long.parseLong(value));
							}
						} else if (name.equals("maxLogRecords")) {
							if (value != null && value.trim().length() != 0) {
								holder.setMaxLogRecords(Long.parseLong(value));
							}
						}
					}
				}
			}
		} catch (Exception e) {
			LogHelper.logError(
				logger,
				tracer,
				"Failed to Scheduler Config scheduler data",
				e);
		}
	}

}
