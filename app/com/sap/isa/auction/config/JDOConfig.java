package com.sap.isa.auction.config;

import com.sap.tc.logging.Location;

/**
 * Title:
 * Description:
 * Copyright:    Copyright (c) 2003
 * Company:
 * @author
 * @version 1.0
 */

public class JDOConfig extends JDBCConfig {

/**
			<params>
				<param 	name="driver" 	value="com.sap.sql.jdbc.common.CommonDriver" />
				<param 	name="url" 	value="" />
				<param 	name="user" 	value="" />
				<param 	name="password" 	value="" />
				<param 	name="jndiDataSource" 	value="ISA/WA_EBAY_DB" />
				
				<param 	name="pmFactory" 	value="com.sap.jdo.sql.SQLPMF" />
				<param 	name="connectionFactory" 	value="" />
				<param 	name="jndijdo" 	value="jdo/defaultPMF" />
			</params>
 */

    private String pmfName;
    private String cfName;
	private String jndijdo;
	
	private Location loc = Location.getLocation(JDOConfig.class.getName());
	
	private static final String DRIVER="driver";
	private static final String URL="url";
	private static final String USER="user";
	private static final String PASSWORD="password";
	private static final String JNDIDATASOURCE="jndiDataSource";
	private static final String PMFACTORY="pmFactory";
	private static final String CONNECTIONFACTORY="connectionFactory";
	private static final String JNDIJDO="jndijdo";

	
    JDOConfig() {
    }

    public String getPMFClassName() {
        return pmfName;
    }

    void setPMFClassName(String pmf) {
        this.pmfName = pmf;
    }

    public String getConnectionFactoryName() {
        return this.cfName;
    }

    public void setConnectionFactoryName(String val) {
        this.cfName = val;
    }
    /**
     * @return
     */
    public String getJdoJNDIName() {
        return jndijdo;
    }

    /**
     * @param string
     */
    public void setJdoJNDIName(String string) {
        jndijdo = string;
    }

}