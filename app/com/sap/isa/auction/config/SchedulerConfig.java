package com.sap.isa.auction.config;

import com.sap.isa.core.logging.IsaLocation;

/**
 * Title:
 * Description:
 * Copyright:    Copyright (c) 2003
 * Company:
 * @author
 * @version 1.0
 */
public class SchedulerConfig implements Cloneable {
    // set suitable defaults
    private int maxThreads;
    private int minThreads;
    private int idleTime;
    private String schedulerClass;
    //private String schema;
    private boolean runScheduler = true;
    private String schedulerId;
//    private Collection schedColl = new ArrayList();
	private JDOConfig jdoConfig;
	private boolean useOpenSQL = false;
	private long jobThreadPoolMaxusedInt = -1;
	private long jobThreadPoolGCInt = -1;
	private long jobThreadPoolIdleTimeoutInt = -1;
	private long maxLogRecords;

	/** 
	  *  The IsaLocation of the current Action.
	  *  This will be instantiated during perform and is always present.
	  */
	 protected IsaLocation log = IsaLocation.getInstance(SchedulerConfig.class.getName());

	    
    public static class TaskSchedule {
        private String taskName;
        private String taskClass;
        private long period = -1;
        private long endTime = -1;
        private String cronExp = null;
        private boolean enable = false;
        
		TaskSchedule() {
		}
		public boolean getEnabled() {
			return enable;
		}
		void setEnabled(boolean val) {
			this.enable = val;
		}
        public String getTaskName(){
            return taskName;
        }
        public void setTaskName(String taskName) {
            this.taskName = taskName;
        }
        public String getTaskClass(){
            return taskClass;
        }
        public void setTaskClass(String taskClass) {
            this.taskClass = taskClass;
        }

        public long getPeriod(){
            return period;
        }
        public void setPeriod(long period) {
            this.period = period;
        }

        public long getEndTime(){
            return endTime;
        }
        public void setEndTime(long endTime) {
            this.endTime = endTime;
        }
        /**
         * @return
         */
        public String getCronExp() {
            return cronExp;
        }

        /**
         * @param string
         */
        public void setCronExp(String string) {
            cronExp = string;
        }

    }

    SchedulerConfig() {
    }

    public void setRunScheduler(boolean bool) {
        this.runScheduler = bool;
    }
    public boolean isRunScheduler() {
        return this.runScheduler;
    }
    /** Thread Pool specific*/
    public int getMaxThreads() {
        return maxThreads;
    }

    void setMaxThreads(int size) {
        this.maxThreads = size;
    }

    public int getMinThreads() {
        return minThreads;
    }

    void setMinThreads(int size) {
        this.minThreads = size;
    }

    public int getIdleTime() {
        return idleTime;
    }

    void setIdleTime(int peroid) {
        this.idleTime = peroid;
    }

    /** Scheduler Factory specific*/
    /**
     * FQN of the scheduler implementation
     */
    public String getSchedulerClass() {
        return schedulerClass;
    }

    void setSchedulerClass(String val) {
        schedulerClass = val;
    }
    
    public String getSchema() {
        return "";
    }

    void setSchema(String val) {
        //schema = val;
    }
    
//    void addTaskSchedule(TaskSchedule schedule) {
//        schedColl.add(schedule);
//    }
//    
//    public Collection getTaskSchedules() {
//        return schedColl;
//    }
    /** Clustering specific*/

    /** Failover specific */
	/**
	 * @return
	 */
	public JDOConfig getJDOConfig() {
		return jdoConfig;
	}

	/**
	 * @param config
	 */
	public void setJDOConfig(JDOConfig config) {
		jdoConfig = config;
	}
	/* (non-Javadoc)
	 * @see java.lang.Object#clone()
	 */
	public Object clone() {
		SchedulerConfig clone = null;
		try {
			clone = (SchedulerConfig)super.clone();
			if(jdoConfig != null)
				clone.jdoConfig = (JDOConfig)jdoConfig.clone();
		}
		catch(CloneNotSupportedException ex) {
			log.debug("Error in clone() ", ex);
		}
		return clone;
	}

	/**
	 * @return
	 */
	public String getSchedulerId() {
		return schedulerId;
	}

	/**
	 * @param string
	 */
	public void setSchedulerId(String string) {
		schedulerId = string;
	}

	/**
	 * @return
	 */
	public long getJobThreadPoolGCInt() {
		return jobThreadPoolGCInt;
	}

	/**
	 * @return
	 */
	public long getJobThreadPoolIdleTimeoutInt() {
		return jobThreadPoolIdleTimeoutInt;
	}

	/**
	 * @return
	 */
	public long getJobThreadPoolMaxusedInt() {
		return jobThreadPoolMaxusedInt;
	}

	/**
	 * @return
	 */
	public boolean isUseOpenSQL() {
		return useOpenSQL;
	}

	/**
	 * @param l
	 */
	public void setJobThreadPoolGCInt(long l) {
		jobThreadPoolGCInt = l;
	}

	/**
	 * @param l
	 */
	public void setJobThreadPoolIdleTimeoutInt(long l) {
		jobThreadPoolIdleTimeoutInt = l;
	}

	/**
	 * @param l
	 */
	public void setJobThreadPoolMaxusedInt(long l) {
		jobThreadPoolMaxusedInt = l;
	}

	/**
	 * @param b
	 */
	public void setUseOpenSQL(boolean b) {
		useOpenSQL = b;
	}

	/**
	 * @return
	 */
	public long getMaxLogRecords() {
		return maxLogRecords;
	}

	/**
	 * @param l
	 */
	public void setMaxLogRecords(long l) {
		maxLogRecords = l;
	}

}
