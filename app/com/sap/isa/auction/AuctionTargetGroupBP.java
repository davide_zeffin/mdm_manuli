/*****************************************************************************
    Copyright (c) 2001, SAPMarkets Palo Alto, USA, All rights reserved.

*****************************************************************************/

package com.sap.isa.auction;


/**
 * Interface for the TGBP value bean for auctions
 */
public interface AuctionTargetGroupBP {

    public static String TARGET_GROUP="TG";
    public static String BUSINESS_PARTNER="BP";


    /**
     * The GUID from the CRM system for the BP/TG
     */
    public String getGuid();

    /**
     * The type of the object guid, BP or TG
     */
    public String getType();

    public void setGuid(String guid);

    public void setType(String Type);
}