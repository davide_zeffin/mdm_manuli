/*
 * Created on Apr 19, 2004
 *
 * Title:        Internet Sales Asset Remarketing
 * Description:
 * Copyright:    Copyright (c) 2004
 * Company:      SAP Labs LLC
 * @author
 * @version 1.0
 */
package com.sap.isa.auction.services;


import java.text.ParseException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Properties;

import javax.jdo.JDOException;
import javax.jdo.PersistenceManager;
import javax.jdo.PersistenceManagerFactory;
import javax.jdo.Query;

import com.sap.tc.logging.Category;
import com.sap.tc.logging.Location;
import com.sap.isa.auction.businessobject.ValidationException;
import com.sap.isa.auction.config.JDOConfig;
import com.sap.isa.auction.config.SchedulerConfig;
import com.sap.isa.auction.exception.SystemException;
import com.sap.isa.auction.exception.i18n.ErrorMessageBundle;
import com.sap.isa.auction.exception.i18n.I18nErrorMessage;
import com.sap.isa.auction.logging.CategoryProvider;
import com.sap.isa.auction.logging.LocationProvider;
import com.sap.isa.auction.logging.LogHelper;
import com.sap.isa.auction.logging.TraceHelper;
import com.sap.isa.services.schedulerservice.CronJob;
import com.sap.isa.services.schedulerservice.Job;
import com.sap.isa.services.schedulerservice.Scheduler;
import com.sap.isa.services.schedulerservice.SchedulerFactory;
import com.sap.isa.services.schedulerservice.exceptions.SchedulerException;
import com.sap.isa.services.schedulerservice.properties.PropertyConstants;

/**
 * Title:
 * Description: Maintains Auction specific background job definitions
 *              Maintains mappings between seller and jobs
 *              Performs initialization of Scheduler
 *
 * Copyright:    Copyright (c) 2003
 * Company:
 * @author
 * @version 1.0
 */

public abstract class BackgroundJobManagerBase implements Service {

	protected final static Category logger = CategoryProvider.getSystemCategory();
	protected static Scheduler sch = null;
	protected final static Location tracer =
		LocationProvider.getLocation(BackgroundJobManagerBase.class);

	protected ServiceLocator locator = null;
	protected SchedulerConfig config = null;
	protected Collection  taskConfigsDataColl = null;

	/**
	 * Initializes the underlying the scheduler with the
	 * @param SchedulerConfig data containing the scheduler configuration data
	 */
	public BackgroundJobManagerBase(SchedulerConfig configData,Collection taskConfigsData) {
		config = (SchedulerConfig) configData.clone();
		taskConfigsDataColl  = taskConfigsData; 
	}
	protected   Class getTaskClass(String taskName){
		return null;
	}
	protected Job createJob(SchedulerConfig.TaskSchedule taskSchedule)
		throws ClassNotFoundException, ParseException {
		final String METHOD = "createJob";
		TraceHelper.entering(tracer,METHOD,null);
		Job job = null;
		Class taskClass = getTaskClass(taskSchedule.getTaskName());
		if (taskSchedule.getCronExp() != null
			&& taskSchedule.getCronExp().length() > 0) {
			job = new CronJob(taskSchedule.getCronExp());
			job.setTaskClass(taskClass);
			job.setStartDate(new Date());
			try {
				/**
				 * Only for certain cron jobs period can be set
				 */
				if (taskSchedule.getPeriod() != -1)
					job.setPeriod(taskSchedule.getPeriod());
			} catch (Exception e) {
				LogHelper.logWarning(logger, tracer, e);
			}

		} else {
			job = new Job(taskClass, new Date());
			if (taskSchedule.getPeriod() != -1)
				job.setPeriod(taskSchedule.getPeriod());
		}
		job.setName(taskSchedule.getTaskName());
		job.setExtRefID(taskSchedule.getTaskName());
		// All jobs are stateful jobs
		job.setStateful(true);
		// Logging enabled for all the jobs??
		job.setLoggingEnabled(true);
		if (taskSchedule.getEndTime() != -1)
			job.setEndDate(new Date(taskSchedule.getEndTime()));


		TraceHelper.exiting(tracer,METHOD,null);
		return job;
	}


	public void initialize() throws Exception {
		final String METHOD = "initialize";
		TraceHelper.entering(tracer,METHOD,null);
		
		synchronized (BackgroundJobManagerBase.class) {
			if (sch == null) {
				Properties props = new Properties();
				props.setProperty(
					PropertyConstants.SCHEDULERIMPLCLASS,
					config.getSchedulerClass());
				JDOConfig jdoConfig = config.getJDOConfig();
				if (jdoConfig.getJdoJNDIName() != null
					&& jdoConfig.getJdoJNDIName().length() > 0
					&& jdoConfig.getDataSourceJNDIName() != null
					&& jdoConfig.getDataSourceJNDIName().length() > 0) {
					props.setProperty(
						PropertyConstants.JDOJNDI,
						jdoConfig.getJdoJNDIName());
					props.setProperty(
						PropertyConstants.DATASOURCEJNDI,
						jdoConfig.getDataSourceJNDIName());
				} else {
					props.setProperty(
						PropertyConstants.DBSTOREDRIVER,
						jdoConfig.getDriverClassName());
					props.setProperty(
						PropertyConstants.DBSTOREPASSWD,
						jdoConfig.getPassword());
					props.setProperty(
						PropertyConstants.DBSTORESCHEMA,
						jdoConfig.getUser());
					props.setProperty(
						PropertyConstants.DBSTOREURL,
						jdoConfig.getURL());
					props.setProperty(
						PropertyConstants.DBSTOREUSER,
						jdoConfig.getUser());
					props.setProperty(
						PropertyConstants.JDOFACTORY,
						jdoConfig.getPMFClassName());
				}
				props.setProperty(
					PropertyConstants.THREADPOOLMAXTHREADS,
					"" + config.getMaxThreads());
				props.setProperty(
					PropertyConstants.THREADPOOLMINTHREADS,
					"" + config.getMinThreads());
				props.setProperty(
					PropertyConstants.THREADPOOLTHREADIDLETIME,
					"" + config.getIdleTime());

				if(config.isUseOpenSQL()){
					props.setProperty(
						PropertyConstants.USEOPENSQLIMPLJOBSTORE,
						"true");
					props.setProperty(
						PropertyConstants.USEJDOIMPLJOBSTORE,
						"false");
				}
				if(config.getJobThreadPoolGCInt()>0){
					props.setProperty(
						PropertyConstants.JOBTHREADPOOLGCINTERVAL,
						""+config.getJobThreadPoolGCInt());
				}
				if(config.getJobThreadPoolIdleTimeoutInt()>0){
					props.setProperty(
						PropertyConstants.JOBTHREADPOOLIDLETIMEOUTINTERVAL,
						""+config.getJobThreadPoolIdleTimeoutInt());
				}
				if(config.getJobThreadPoolMaxusedInt()>0){
					props.setProperty(
						PropertyConstants.JOBTHREADPOOLMAXUSEDTIME,
						""+config.getJobThreadPoolMaxusedInt());
				}
				if(config.getMaxLogRecords()>0){
					props.setProperty(
						PropertyConstants.SCHEDULERMAXLOGEXECRECORDS,
						""+config.getMaxLogRecords());
				}

				if(config.getSchedulerId()!=null && config.getSchedulerId().length() > 0 )
					props.setProperty(
						PropertyConstants.SCHEDULERID,
						"" + config.getSchedulerId());				
				logger.infoT(tracer,"About to start the scheduler");
				SchedulerFactory.init(props);
				sch = SchedulerFactory.getScheduler();
				sch.start();
				logger.infoT(tracer,"Scheduler started successfully");				
			}
		}
		TraceHelper.exiting(tracer,METHOD,null);
		
	}

	public  void close() {

	}
	public  static void shutDown() {
		final String METHOD = "shutDown";
		TraceHelper.entering(tracer,METHOD,null);
		logger.infoT(tracer,"In the shutdown of the background job manager");
		try {
			if(sch != null) sch.shutDown();
		} catch (SchedulerException sce) {
			logger.errorT(tracer,"Exception occurred in the shutdown of the background job manager");
			LogHelper.logError(logger, tracer, sce);
		}
		TraceHelper.exiting(tracer,METHOD,null);
		
	}



	protected abstract Collection getJobIds(
		Integer jobType,
		String sellerId,
		String jobId); 
//		{
//		final String METHOD = "getJobIds";
//		TraceHelper.entering(tracer,METHOD,null);
//		PersistenceManager pm = null;
//		try {
//			PersistenceManagerFactory pmf =
//				locator.getPersistenceManagerFactory();
//			pm = pmf.getPersistenceManager();
//			// @@temp: begin hack for JDO bug in loading of meta data
//			Query query = pm.newQuery(SchedulerJobMapDAO.class);
//			StringBuffer filter = new StringBuffer("");
//			if (jobType != null) {
//				filter.append("jobType == ");
//				filter.append(jobType);
//			}
//			if (sellerId != null) {
//				if (filter.length() > 0)
//					filter.append(" && ");
//				filter.append("sellerId == ");
//				filter.append("\"");
//				filter.append(sellerId);
//				filter.append("\"");
//			}
//			if (jobId != null) {
//				if (filter.length() > 0)
//					filter.append(" && ");
//				filter.append("jobId == ");
//				filter.append("\"");
//				filter.append(jobId);
//				filter.append("\"");
//			}
//			if (filter.length() > 0)
//				filter.append(" && ");
//			filter.append("scenarioName == ");
//			filter.append("\"");
//			filter.append(locator.getScenarioName());
//			filter.append("\"");
//			query.setFilter(filter.toString());
//			Collection coll = (Collection) query.execute();
//			// @@temp : end
//			// Log Warning
//			if (coll == null && coll.size() < 1) {
//				return null;
//			}
//			if (!coll.iterator().hasNext())
//				return null;
//			Iterator iter = coll.iterator();
//			Collection returnColl = new HashSet();
//			while (iter.hasNext()) {
//				SchedulerJobMapDAO schedulerJobMapDAO =
//					(SchedulerJobMapDAO) iter.next();
//				returnColl.add(schedulerJobMapDAO.getJobId());
//			}
//			query.closeAll();
//			TraceHelper.exiting(tracer,METHOD,null);
//			return returnColl;
//		} catch (Exception jdoex) {
//			SystemException ex = null;
//			if (jdoex instanceof JDOException) {
//				// This should be fine for now?
//				ErrorMessageBundle bundle = ErrorMessageBundle.getInstance();
//				I18nErrorMessage msg =
//					bundle.newI18nErrorMessage(SystemException.JDO_ERROR);
//				// This should be fine for now?
//				ex = new SystemException(msg, jdoex);
//			} else {
//				ErrorMessageBundle bundle = ErrorMessageBundle.getInstance();
//				I18nErrorMessage msg =
//					bundle.newI18nErrorMessage(SystemException.INTERNAL_ERROR);
//				// This should be fine for now?
//				jdoex.printStackTrace();
//				ex = new SystemException(msg, jdoex);
//			}
//			LogHelper.logError(logger, tracer, ex);
//			throw ex;
//		} finally {
//			// close the pm
//			pm.close();
//		}
//		
//	}



	protected abstract void createJobIdMap(int jobType, String sellerId, String jobId); 
//	{
//		final String METHOD = "createJobIdMap";
//		TraceHelper.entering(tracer,METHOD,null);
//		PersistenceManager pm = null;
//		try {
//			PersistenceManagerFactory pmf =
//				locator.getPersistenceManagerFactory();
//			pm = pmf.getPersistenceManager();
//			SchedulerJobMapDAO schedulerJobMapDAO = new SchedulerJobMapDAO();
//			schedulerJobMapDAO.setJobId(jobId);
//			schedulerJobMapDAO.setJobType(jobType);
//			schedulerJobMapDAO.setScenarioName(locator.getScenarioName());
//			pm.currentTransaction().begin();
//			pm.makePersistent(schedulerJobMapDAO);
//			pm.currentTransaction().commit();
//		} catch (Exception jdoex) {
//			// roll back the necessary stuff
//			//E.g. quoation creation needs to be rollback
//			//Set the quotation exp to the current date
//			try {
//				if (pm != null && pm.currentTransaction().isActive()) {
//					// rollback the tx
//					pm.currentTransaction().rollback();
//				}
//			} catch (Exception ex) {
//				LogHelper.logWarning(logger, tracer, ex);
//			}
//			SystemException ex = null;
//			if (jdoex instanceof JDOException) {
//				// This should be fine for now?
//				ErrorMessageBundle bundle = ErrorMessageBundle.getInstance();
//				I18nErrorMessage msg =
//					bundle.newI18nErrorMessage(SystemException.JDO_ERROR);
//				// This should be fine for now?
//				ex = new SystemException(msg, jdoex);
//			} else {
//				ErrorMessageBundle bundle = ErrorMessageBundle.getInstance();
//				I18nErrorMessage msg =
//					bundle.newI18nErrorMessage(SystemException.INTERNAL_ERROR);
//				// This should be fine for now?
//				ex = new SystemException(msg, jdoex);
//			}
//			LogHelper.logError(logger, tracer, ex);
//			throw ex;
//		} finally {
//			// close the pm
//			pm.close();
//		}
//		TraceHelper.exiting(tracer,METHOD,null);
//	}

	protected abstract void removeJobIdMap(String jobId); 
//	{
//		final String METHOD = "removeJobIdMap";
//		TraceHelper.entering(tracer,METHOD,null);
//		PersistenceManager pm = null;
//		try {
//			PersistenceManagerFactory pmf =
//				locator.getPersistenceManagerFactory();
//			pm = pmf.getPersistenceManager();
//			// @@temp: begin hack for JDO bug in loading of meta data
//			Query query = pm.newQuery(SchedulerJobMapDAO.class);
//			query.closeAll();
//			SchedulerJobMapDAO mapdao =
//				(SchedulerJobMapDAO) pm.getObjectById(
//					new SchedulerJobMapDAO.Id(jobId),
//					false);
//			pm.currentTransaction().begin();
//			pm.deletePersistent(mapdao);
//			pm.currentTransaction().commit();
//		} catch (Exception jdoex) {
//			SystemException ex = null;
//			try {
//				if (pm != null && pm.currentTransaction().isActive()) {
//					pm.currentTransaction().rollback();
//				}
//			} catch (Exception e) {
//				LogHelper.logWarning(logger, tracer, e);
//			}
//			if (jdoex instanceof JDOException) {
//				// This should be fine for now?
//				ErrorMessageBundle bundle = ErrorMessageBundle.getInstance();
//				I18nErrorMessage msg =
//					bundle.newI18nErrorMessage(SystemException.JDO_ERROR);
//				// This should be fine for now?
//				ex = new SystemException(msg, jdoex);
//			} else {
//				ErrorMessageBundle bundle = ErrorMessageBundle.getInstance();
//				I18nErrorMessage msg =
//					bundle.newI18nErrorMessage(SystemException.INTERNAL_ERROR);
//				// This should be fine for now?
//				ex = new SystemException(msg, jdoex);
//			}
//			LogHelper.logError(logger, tracer, ex);
//			throw ex;
//		} finally {
//			// close the pm
//			if(pm!=null)
//				pm.close();
//		}
//		TraceHelper.exiting(tracer,METHOD,null);
//		
//	}
	/**
	 * All possible jobs both from the boot strap file and as well as the job id map table
	 * @return Collection of type Job
	 */
	public abstract Collection getAllJobs(); 
//	{
//		final String METHOD = "getAllJobs";
//		TraceHelper.entering(tracer,METHOD,null);
//		PersistenceManager pm = null;
//		try {
//			PersistenceManagerFactory pmf =
//				locator.getPersistenceManagerFactory();
//			pm = pmf.getPersistenceManager();
//			// @@temp: begin hack for JDO bug in loading of meta data
//			Query query = pm.newQuery(SchedulerJobMapDAO.class);
//			StringBuffer queryFilter = new StringBuffer();
//			queryFilter.append("scenarioName");
//			queryFilter.append("==");
//			queryFilter.append("\"");
//			queryFilter.append(locator.getScenarioName());
//			queryFilter.append("\"");
//			query.setFilter(queryFilter.toString());
//			Collection coll = (Collection) query.execute();
//			Iterator iter = coll.iterator();
//			ArrayList jobIdList = new ArrayList();
//			while (iter.hasNext()) {
//				SchedulerJobMapDAO mapdao = (SchedulerJobMapDAO) iter.next();
//				jobIdList.add(mapdao.getJobId());
//			}
//			query.closeAll();
//			Scheduler sch = SchedulerFactory.getScheduler();
//			Collection anotherColl = null;
//			if (jobIdList != null && jobIdList.size() > 0)
//				anotherColl = sch.getJobs(jobIdList);
//			Collection newColl = getDefaultJobsLimitedByColl(anotherColl);
//			TraceHelper.exiting(tracer,METHOD,null);
//			if (newColl != null && anotherColl != null) {
//				anotherColl.addAll(newColl);
//				return anotherColl;
//			}
//			if (newColl != null && anotherColl == null) {
//				return newColl;
//			}
//			return anotherColl;
//		} catch (Exception jdoex) {
//			SystemException ex = null;
//			if (jdoex instanceof JDOException) {
//				// This should be fine for now?
//				ErrorMessageBundle bundle = ErrorMessageBundle.getInstance();
//				I18nErrorMessage msg =
//					bundle.newI18nErrorMessage(SystemException.JDO_ERROR);
//				// This should be fine for now?
//				ex = new SystemException(msg, jdoex);
//			} else {
//				ErrorMessageBundle bundle = ErrorMessageBundle.getInstance();
//				I18nErrorMessage msg =
//					bundle.newI18nErrorMessage(SystemException.INTERNAL_ERROR);
//				// This should be fine for now?
//				ex = new SystemException(msg, jdoex);
//			}
//			LogHelper.logError(logger, tracer, ex);
//			throw ex;
//		} finally {
//			// close the pm
//			if(pm!=null)
//			pm.close();
//		}
//	}
//
	public boolean cancelJob(String jobId) {
		final String METHOD = "cancelJob";
		TraceHelper.entering(tracer,METHOD,null);
		try {
			Scheduler sch = SchedulerFactory.getScheduler();
			TraceHelper.exiting(tracer,METHOD,null);
			return sch.cancelJob(jobId);
		} catch (SchedulerException schEx) {
			SystemException ex = null;
			ErrorMessageBundle bundle = ErrorMessageBundle.getInstance();
			I18nErrorMessage msg =
				bundle.newI18nErrorMessage(SystemException.SCHEDULER_ERROR);
			// This should be fine for now?
			ex = new SystemException(msg, schEx);
			LogHelper.logError(logger, tracer, ex);
			throw ex;
		}

	}

	public boolean pauseJob(String jobId) throws SchedulerException {
		final String METHOD = "pauseJob";
		TraceHelper.entering(tracer,METHOD,null);
		
		try {
			Scheduler sch = SchedulerFactory.getScheduler();
			TraceHelper.exiting(tracer,METHOD,null);
			return sch.pauseJob(jobId);
		} catch (SchedulerException schEx) {
			SystemException ex = null;
			ErrorMessageBundle bundle = ErrorMessageBundle.getInstance();
			I18nErrorMessage msg =
				bundle.newI18nErrorMessage(SystemException.SCHEDULER_ERROR);
			// This should be fine for now?
			ex = new SystemException(msg, schEx);
			LogHelper.logError(logger, tracer, ex);
			throw ex;
		}

	}

	public boolean removeJob(Job job) throws SchedulerException {
		final String METHOD = "removeJob";
		TraceHelper.entering(tracer,METHOD,null);
		
		Integer  taskType = getJobType(job);
		if (taskType == null) {
			ErrorMessageBundle bundle = ErrorMessageBundle.getInstance();
			I18nErrorMessage msg =
				bundle.newI18nErrorMessage(
					ValidationException.INVALID_SCHED_JOB_TYPE);
			ValidationException ex = new ValidationException(job, msg);
			throw ex;
		}
		try {
			Scheduler sch = SchedulerFactory.getScheduler();
			removeJobIdMap(job.getID());
			boolean bool = sch.removeJob(job.getID());
			TraceHelper.exiting(tracer,METHOD,null);
			return bool;

		} catch (SchedulerException schEx) {
			SystemException ex = null;
			ErrorMessageBundle bundle = ErrorMessageBundle.getInstance();
			I18nErrorMessage msg =
				bundle.newI18nErrorMessage(SystemException.SCHEDULER_ERROR);
			// This should be fine for now?
			ex = new SystemException(msg, schEx);
			LogHelper.logError(logger, tracer, ex);
			throw ex;
		}
	}

	public boolean resumeJob(String jobId) {
		final String METHOD = "resumeJob";
		TraceHelper.entering(tracer,METHOD,null);
		try {
			Scheduler sch = SchedulerFactory.getScheduler();
			TraceHelper.exiting(tracer,METHOD,null);
			return sch.resumeJob(jobId);
		} catch (SchedulerException schEx) {
			SystemException ex = null;
			ErrorMessageBundle bundle = ErrorMessageBundle.getInstance();
			I18nErrorMessage msg =
				bundle.newI18nErrorMessage(SystemException.SCHEDULER_ERROR);
			// This should be fine for now?
			ex = new SystemException(msg, schEx);
			LogHelper.logError(logger, tracer, ex);
			throw ex;
		}

	}

	public void runNow(Job job) throws SchedulerException {
		final String METHOD = "runNow";
		TraceHelper.entering(tracer,METHOD,null);
		Scheduler sch = SchedulerFactory.getScheduler();
		sch.runNow(job);
		TraceHelper.exiting(tracer,METHOD,null);
		
	}
	protected Integer getJobType(Job job) {
		return null;
	}
	public synchronized String schedule(Job job) {
		//Currently naming binding used for finding out about the job
		// This is a new job so that our scheduler job id map should be mapped
		// as well in our databases as such.
		final String METHOD = "schedule";
		TraceHelper.entering(tracer,METHOD,null);
		Integer taskType = getJobType(job);
		if (taskType == null) {
			ErrorMessageBundle bundle = ErrorMessageBundle.getInstance();
			I18nErrorMessage msg =
				bundle.newI18nErrorMessage(
					ValidationException.INVALID_SCHED_JOB_TYPE);
			ValidationException ex = new ValidationException(job, msg);
			throw ex;
		}
		try {
			Scheduler sch = SchedulerFactory.getScheduler();
			if (job.isVirgin()) {
				// Check whether this kind of already exists already scheduled
				//Through some other session
				Collection jobs =
					getJobIds(
						taskType,
						null,
						null);
				if (jobs != null &&  jobs.iterator().hasNext()) {
					return (String)jobs.iterator().next();
				}
				String jobId = sch.schedule(job);
				createJobIdMap(
					taskType.intValue(),
					null,
					job.getID());
				TraceHelper.exiting(tracer,METHOD,null);
				return jobId;
			}
			TraceHelper.exiting(tracer,METHOD,null);			
			return sch.schedule(job);
		} catch (SchedulerException schEx) {
			SystemException ex = null;
			ErrorMessageBundle bundle = ErrorMessageBundle.getInstance();
			I18nErrorMessage msg =
				bundle.newI18nErrorMessage(SystemException.SCHEDULER_ERROR);
			// This should be fine for now?
			ex = new SystemException(msg, schEx);
			LogHelper.logError(logger, tracer, ex);
			throw ex;
		}
	}
	public Job getJob(String jobId) {
		final String METHOD = "getJob";
		TraceHelper.entering(tracer,METHOD,null);
		try {
			Scheduler sch = SchedulerFactory.getScheduler();
			TraceHelper.exiting(tracer,METHOD,null);
			return sch.getJob(jobId);
		} catch (SchedulerException schEx) {
			SystemException ex = null;
			ErrorMessageBundle bundle = ErrorMessageBundle.getInstance();
			I18nErrorMessage msg =
				bundle.newI18nErrorMessage(SystemException.SCHEDULER_ERROR);
			// This should be fine for now?
			ex = new SystemException(msg, schEx);
			LogHelper.logError(logger, tracer, ex);
			throw ex;
		}
	}
	protected Collection getDefaultJobsLimitedByColl(Collection coll)
		throws Exception {
		if (config == null)
			return null;
		Iterator configIter = taskConfigsDataColl.iterator();
		Collection returnColl = new ArrayList();
		while (configIter.hasNext()) {
			SchedulerConfig.TaskSchedule taskSchedule =
				(SchedulerConfig.TaskSchedule) configIter.next();
			/**
			 * Compare the job names for comparision purposes
			 */
			boolean foundOne = false;
			if (coll != null) {
				Iterator iter = coll.iterator();
				while (iter.hasNext()) {
					Job job2 = (Job) iter.next();
					if (job2.getName().equals(taskSchedule.getTaskName())) {
						foundOne = true;
					}
				}
			}
			if (!foundOne) {
				Job job = createJob(taskSchedule);
				returnColl.add(job);
			}
		}
		return returnColl;
	}
	/* (non-Javadoc)
	 * @see com.sap.isa.auction.services.Service#setServiceLocator(com.sap.isa.auction.services.EBayServiceLocator)
	 */
	public void setServiceLocator(ServiceLocator locator) {
		this.locator = locator;
	}

	public boolean isJobQueuedForExecution(Job j){
		final String METHOD = "isJobQueuedForExecution";
		TraceHelper.entering(tracer,METHOD,null);
		try {
			Scheduler sch = SchedulerFactory.getScheduler();
			TraceHelper.exiting(tracer,METHOD,null);
			return sch.isJobQueuedForExecution(j);
		} catch (SchedulerException schEx) {
			SystemException ex = null;
			ErrorMessageBundle bundle = ErrorMessageBundle.getInstance();
			I18nErrorMessage msg =
				bundle.newI18nErrorMessage(SystemException.SCHEDULER_ERROR);
			// This should be fine for now?
			ex = new SystemException(msg, schEx);
			LogHelper.logError(logger, tracer, ex);
			throw ex;
		}	
	} 
}

