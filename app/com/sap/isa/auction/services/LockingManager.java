/*
 * Created on Aug 25, 2004
 *
 * To change the template for this generated file go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
package com.sap.isa.auction.services;

import java.sql.Connection;
import java.util.HashMap;
import java.util.Map;
import java.util.WeakHashMap;

import com.sap.engine.frame.core.locking.LockException;
import com.sap.engine.frame.core.locking.TechnicalLockException;
import com.sap.engine.services.applocking.TableLocking;
import com.sap.isa.auction.bean.Auction;

/**
 * @author I802850
 *
 * To change the template for this generated type comment go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
public class LockingManager {
	private LockingManager(String auctionTableName){
		this.aucTableName = auctionTableName;
	}
	private static WeakHashMap tableLockingsMap = new WeakHashMap();
	public static LockingManager getLockingManager(String auctionTableName){
		LockingManager lockManager = null;
		synchronized(LockingManager.class){
				lockManager = (LockingManager)tableLockingsMap.get(auctionTableName);
				if(lockManager!=null) return lockManager;
					lockManager = new LockingManager(auctionTableName);
					tableLockingsMap.put(auctionTableName,lockManager);
					return lockManager;
		}
	}
	private final String aucTableName;
	public  void lockAuctionRecord(
		TableLocking tableLocking,
		Connection connection,
		Auction auction)
		throws LockException, TechnicalLockException, IllegalArgumentException {
		Map pkMap = new HashMap();
		pkMap.put("AUCTIONID", auction.getAuctionId());
		pkMap.put("CLIENT", auction.getClient());
		pkMap.put("SYSTEMID", auction.getSystemId());
		tableLocking.lock(
			TableLocking.LIFETIME_TRANSACTION,
			connection,
			aucTableName,
			pkMap,
			TableLocking.MODE_EXCLUSIVE_CUMULATIVE);

	}
	public void unLockAuctionRecord(
		TableLocking tableLocking,
		Connection connection,
		Auction auction)
		throws TechnicalLockException, IllegalArgumentException {
		Map pkMap = new HashMap();
		pkMap.put("AUCTIONID", auction.getAuctionId());
		pkMap.put("CLIENT", auction.getClient());
		pkMap.put("SYSTEMID", auction.getSystemId());
		tableLocking.unlock(
			TableLocking.LIFETIME_TRANSACTION,
			connection,
			aucTableName,
			pkMap,
			TableLocking.MODE_EXCLUSIVE_CUMULATIVE);

	}
}