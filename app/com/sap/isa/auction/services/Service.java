/*
 * Created on Feb 20, 2004
 *
 */
package com.sap.isa.auction.services;

/**
 * @author I802891
 *
 */
public interface Service {
	public void setServiceLocator(ServiceLocator locator);
	public void initialize() throws Exception;
	public void close();
}
