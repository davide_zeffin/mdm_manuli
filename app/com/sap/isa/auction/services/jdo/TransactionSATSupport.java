/*
 * Created on Dec 30, 2004
 *
 */
package com.sap.isa.auction.services.jdo;

import javax.jdo.PersistenceManager;
import javax.jdo.Transaction;

import com.sap.isa.auction.sat.SATConstants;
import com.sap.isa.auction.sat.SATSupport;
import com.sap.util.monitor.jarm.IMonitor;

import javax.transaction.Synchronization;

/**
 * @author I802891
 */
public class TransactionSATSupport implements Transaction {
	private Transaction delegate = null;
	private SATSupport sat = null;
	
	public TransactionSATSupport(Transaction actual, SATSupport sat) {
		this.delegate = actual;
		this.sat = sat;
	}
	
	private IMonitor getRequestMonitor() {
		if(sat != null) return sat.getRequestMonitor();
		return null;
	}
		
	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	public int hashCode() {
		return delegate.hashCode();
	}

	/**
	 * @return
	 */
	public boolean getNontransactionalWrite() {
		return delegate.getNontransactionalWrite();
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	public String toString() {
		return delegate.toString();
	}

	/**
	 * @return
	 */
	public boolean getRestoreValues() {
		return delegate.getRestoreValues();
	}

	/**
	 * @param arg0
	 */
	public void setRestoreValues(boolean arg0) {
		delegate.setRestoreValues(arg0);
	}

	/**
	 * @return
	 */
	public boolean isActive() {
		return delegate.isActive();
	}

	/**
	 * @return
	 */
	public boolean getRetainValues() {
		return delegate.getRetainValues();
	}

	/**
	 * @return
	 */
	public boolean getOptimistic() {
		return delegate.getOptimistic();
	}

	/**
	 * 
	 */
	public void rollback() {
		final String METHOD = "rollback";
		final String ACTIVITY = SATConstants.JDO_PREFIX + METHOD;
		IMonitor monitor = getRequestMonitor();
		if(monitor != null) monitor.startComponent(ACTIVITY);
		delegate.rollback();
		if(monitor != null) monitor.endComponent(ACTIVITY);
	}

	/**
	 * @return
	 */
	public boolean getNontransactionalRead() {
		return delegate.getNontransactionalRead();
	}

	/**
	 * @param arg0
	 */
	public void setSynchronization(Synchronization arg0) {
		delegate.setSynchronization(arg0);
	}

	/**
	 * @param arg0
	 */
	public void setOptimistic(boolean arg0) {
		delegate.setOptimistic(arg0);
	}

	/**
	 * @param arg0
	 */
	public void setNontransactionalRead(boolean arg0) {
		delegate.setNontransactionalRead(arg0);
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	public boolean equals(Object obj) {
		return delegate.equals(obj);
	}

	/**
	 * @return
	 */
	public Synchronization getSynchronization() {
		return delegate.getSynchronization();
	}

	/**
	 * @return
	 */
	public PersistenceManager getPersistenceManager() {
		return delegate.getPersistenceManager();
	}

	/**
	 * @param arg0
	 */
	public void setNontransactionalWrite(boolean arg0) {
		delegate.setNontransactionalWrite(arg0);
	}

	/**
	 * 
	 */
	public void commit() {
		final String METHOD = "committ";
		final String ACTIVITY = SATConstants.JDO_PREFIX + METHOD;
		IMonitor monitor = getRequestMonitor();
		if(monitor != null) monitor.startComponent(ACTIVITY);
		delegate.commit();
		if(monitor != null) monitor.endComponent(ACTIVITY);		
	}

	/**
	 * @param arg0
	 */
	public void setRetainValues(boolean arg0) {
		delegate.setRetainValues(arg0);
	}

	/**
	 * 
	 */
	public void begin() {
		delegate.begin();
	}

}
