/*
 * Created on Dec 30, 2004
 */
package com.sap.isa.auction.services.jdo;

import java.util.Collection;
import java.util.Map;

import javax.jdo.Extent;
import javax.jdo.PersistenceManager;
import javax.jdo.Query;

import com.sap.isa.auction.sat.SATConstants;
import com.sap.isa.auction.sat.SATSupport;
import com.sap.util.monitor.jarm.IMonitor;

/**
 * @author I802891
 *
 * To change the template for this generated type comment go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
public class QuerySATSupport implements Query {

	private Query delegate = null;
	private SATSupport sat = null;
	
	private static final String SAT_PREFIX = SATConstants.JDO_PREFIX + ":Query:";
		
	public QuerySATSupport(Query actual, SATSupport sat) {
		this.delegate = actual;
		this.sat = sat;
	}
	
	private IMonitor getRequestMonitor() {
		if(sat != null) return sat.getRequestMonitor();
		return null;
	}

	/**
	 * @param arg0
	 */
	public void close(Object arg0) {
		delegate.close(arg0);
	}

	/**
	 * 
	 */
	public void closeAll() {
		delegate.closeAll();
	}

	/**
	 * 
	 */
	public void compile() {
		delegate.compile();
	}

	/**
	 * @param arg0
	 */
	public void declareImports(String arg0) {
		delegate.declareImports(arg0);
	}

	/**
	 * @param arg0
	 */
	public void declareParameters(String arg0) {
		delegate.declareParameters(arg0);
	}

	/**
	 * @param arg0
	 */
	public void declareVariables(String arg0) {
		delegate.declareVariables(arg0);
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	public boolean equals(Object obj) {
		return delegate.equals(obj);
	}

	/**
	 * @return
	 */
	public Object execute() {
		final String METHOD = "execute";
		final String ACTIVITY = SAT_PREFIX + METHOD;
		IMonitor monitor = getRequestMonitor();
		if(monitor != null) monitor.startComponent(ACTIVITY);
		Object result = delegate.execute();
		if(monitor != null) monitor.endComponent(ACTIVITY);
		return result;
	}

	/**
	 * @param arg0
	 * @return
	 */
	public Object execute(Object arg0) {
		final String METHOD = "execute";
		final String ACTIVITY = SAT_PREFIX + METHOD;
		IMonitor monitor = getRequestMonitor();
		if(monitor != null) monitor.startComponent(ACTIVITY);
		Object result = delegate.execute(arg0);
		if(monitor != null) monitor.endComponent(ACTIVITY);
		return result;
	}

	/**
	 * @param arg0
	 * @param arg1
	 * @return
	 */
	public Object execute(Object arg0, Object arg1) {
		final String METHOD = "execute";
		final String ACTIVITY = SAT_PREFIX + METHOD;
		IMonitor monitor = getRequestMonitor();
		if(monitor != null) monitor.startComponent(ACTIVITY);
		Object result = delegate.execute(arg0,arg1);
		if(monitor != null) monitor.endComponent(ACTIVITY);
		return result;
	}

	/**
	 * @param arg0
	 * @param arg1
	 * @param arg2
	 * @return
	 */
	public Object execute(Object arg0, Object arg1, Object arg2) {
		final String METHOD = "execute";
		final String ACTIVITY = SAT_PREFIX + METHOD;
		IMonitor monitor = getRequestMonitor();
		if(monitor != null) monitor.startComponent(ACTIVITY);
		Object result = delegate.execute(arg0,arg1,arg2);
		if(monitor != null) monitor.endComponent(ACTIVITY);
		return result;
	}

	/**
	 * @param arg0
	 * @return
	 */
	public Object executeWithArray(Object[] arg0) {
		final String METHOD = "execute";
		final String ACTIVITY = SAT_PREFIX + METHOD;
		IMonitor monitor = getRequestMonitor();
		if(monitor != null) monitor.startComponent(ACTIVITY);
		Object result = delegate.executeWithArray(arg0);
		if(monitor != null) monitor.endComponent(ACTIVITY);
		return result;		
	}

	/**
	 * @param arg0
	 * @return
	 */
	public Object executeWithMap(Map arg0) {
		final String METHOD = "execute";
		final String ACTIVITY = SAT_PREFIX + METHOD;
		IMonitor monitor = getRequestMonitor();
		if(monitor != null) monitor.startComponent(ACTIVITY);
		Object result = delegate.executeWithMap(arg0);
		if(monitor != null) monitor.endComponent(ACTIVITY);
		return result;			
	}

	/**
	 * @return
	 */
	public boolean getIgnoreCache() {
		return delegate.getIgnoreCache();
	}

	/**
	 * @return
	 */
	public PersistenceManager getPersistenceManager() {
		return delegate.getPersistenceManager();
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	public int hashCode() {
		return delegate.hashCode();
	}

	/**
	 * @param arg0
	 */
	public void setCandidates(Collection arg0) {
		delegate.setCandidates(arg0);
	}

	/**
	 * @param arg0
	 */
	public void setCandidates(Extent arg0) {
		delegate.setCandidates(arg0);
	}

	/**
	 * @param arg0
	 */
	public void setClass(Class arg0) {
		delegate.setClass(arg0);
	}

	/**
	 * @param arg0
	 */
	public void setFilter(String arg0) {
		delegate.setFilter(arg0);
	}

	/**
	 * @param arg0
	 */
	public void setIgnoreCache(boolean arg0) {
		delegate.setIgnoreCache(arg0);
	}

	/**
	 * @param arg0
	 */
	public void setOrdering(String arg0) {
		delegate.setOrdering(arg0);
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	public String toString() {
		return delegate.toString();
	}

}
