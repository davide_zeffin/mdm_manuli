/*
 * Created on Jan 5, 2005
 *
 * To change the template for this generated file go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
package com.sap.isa.auction.services.jdo;

import java.util.Collection;
import java.util.Properties;

import javax.jdo.PersistenceManager;
import javax.jdo.PersistenceManagerFactory;

import com.sap.isa.auction.sat.DefaultSATSupport;
import com.sap.isa.auction.sat.SATSupport;
import com.sap.util.monitor.jarm.IMonitor;

/**
 * @author I802891
 *
 */
public final class PMFSATSupport implements PersistenceManagerFactory {
	private PersistenceManagerFactory delegate = null;
	private SATSupport sat = null;
	
	public PMFSATSupport(PersistenceManagerFactory actual, SATSupport sat) {
		delegate = actual;
		this.sat = sat;
	}
	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	public boolean equals(Object obj) {
		return delegate.equals(obj);
	}

	/**
	 * @return
	 */
	public String getConnectionDriverName() {
		return delegate.getConnectionDriverName();
	}

	/**
	 * @return
	 */
	public Object getConnectionFactory() {
		return delegate.getConnectionFactory();
	}

	/**
	 * @return
	 */
	public Object getConnectionFactory2() {
		return delegate.getConnectionFactory2();
	}

	/**
	 * @return
	 */
	public String getConnectionFactory2Name() {
		return delegate.getConnectionFactory2Name();
	}

	/**
	 * @return
	 */
	public String getConnectionFactoryName() {
		return delegate.getConnectionFactoryName();
	}

	/**
	 * @return
	 */
	public String getConnectionURL() {
		return delegate.getConnectionURL();
	}

	/**
	 * @return
	 */
	public String getConnectionUserName() {
		return delegate.getConnectionUserName();
	}

	/**
	 * @return
	 */
	public boolean getIgnoreCache() {
		return delegate.getIgnoreCache();
	}

	/**
	 * @return
	 */
	public int getMaxPool() {
		return delegate.getMaxPool();
	}

	/**
	 * @return
	 */
	public int getMinPool() {
		return delegate.getMinPool();
	}

	/**
	 * @return
	 */
	public int getMsWait() {
		return delegate.getMsWait();
	}

	/**
	 * @return
	 */
	public boolean getMultithreaded() {
		return delegate.getMultithreaded();
	}

	/**
	 * @return
	 */
	public boolean getNontransactionalRead() {
		return delegate.getNontransactionalRead();
	}

	/**
	 * @return
	 */
	public boolean getNontransactionalWrite() {
		return delegate.getNontransactionalWrite();
	}

	/**
	 * @return
	 */
	public boolean getOptimistic() {
		return delegate.getOptimistic();
	}

	/**
	 * @return
	 */
	public PersistenceManager getPersistenceManager() {
		PersistenceManager actual = delegate.getPersistenceManager();
		return new PersistenceManagerSATSupport(actual, new DefaultSATSupport());
	}

	/**
	 * @param arg0
	 * @param arg1
	 * @return
	 */
	public PersistenceManager getPersistenceManager(String arg0, String arg1) {
		PersistenceManager actual = delegate.getPersistenceManager(arg0,arg1);
		return new PersistenceManagerSATSupport(actual, sat);
	}

	/**
	 * @return
	 */
	public Properties getProperties() {
		return delegate.getProperties();
	}

	/**
	 * @return
	 */
	public boolean getRestoreValues() {
		return delegate.getRestoreValues();
	}

	/**
	 * @return
	 */
	public boolean getRetainValues() {
		return delegate.getRetainValues();
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	public int hashCode() {
		return delegate.hashCode();
	}

	/**
	 * @param arg0
	 */
	public void setConnectionDriverName(String arg0) {
		delegate.setConnectionDriverName(arg0);
	}

	/**
	 * @param arg0
	 */
	public void setConnectionFactory(Object arg0) {
		delegate.setConnectionFactory(arg0);
	}

	/**
	 * @param arg0
	 */
	public void setConnectionFactory2(Object arg0) {
		delegate.setConnectionFactory2(arg0);
	}

	/**
	 * @param arg0
	 */
	public void setConnectionFactory2Name(String arg0) {
		delegate.setConnectionFactory2Name(arg0);
	}

	/**
	 * @param arg0
	 */
	public void setConnectionFactoryName(String arg0) {
		delegate.setConnectionFactoryName(arg0);
	}

	/**
	 * @param arg0
	 */
	public void setConnectionPassword(String arg0) {
		delegate.setConnectionPassword(arg0);
	}

	/**
	 * @param arg0
	 */
	public void setConnectionURL(String arg0) {
		delegate.setConnectionURL(arg0);
	}

	/**
	 * @param arg0
	 */
	public void setConnectionUserName(String arg0) {
		delegate.setConnectionUserName(arg0);
	}

	/**
	 * @param arg0
	 */
	public void setIgnoreCache(boolean arg0) {
		delegate.setIgnoreCache(arg0);
	}

	/**
	 * @param arg0
	 */
	public void setMaxPool(int arg0) {
		delegate.setMaxPool(arg0);
	}

	/**
	 * @param arg0
	 */
	public void setMinPool(int arg0) {
		delegate.setMinPool(arg0);
	}

	/**
	 * @param arg0
	 */
	public void setMsWait(int arg0) {
		delegate.setMsWait(arg0);
	}

	/**
	 * @param arg0
	 */
	public void setMultithreaded(boolean arg0) {
		delegate.setMultithreaded(arg0);
	}

	/**
	 * @param arg0
	 */
	public void setNontransactionalRead(boolean arg0) {
		delegate.setNontransactionalRead(arg0);
	}

	/**
	 * @param arg0
	 */
	public void setNontransactionalWrite(boolean arg0) {
		delegate.setNontransactionalWrite(arg0);
	}

	/**
	 * @param arg0
	 */
	public void setOptimistic(boolean arg0) {
		delegate.setOptimistic(arg0);
	}

	/**
	 * @param arg0
	 */
	public void setRestoreValues(boolean arg0) {
		delegate.setRestoreValues(arg0);
	}

	/**
	 * @param arg0
	 */
	public void setRetainValues(boolean arg0) {
		delegate.setRetainValues(arg0);
	}

	/**
	 * @return
	 */
	public Collection supportedOptions() {
		return delegate.supportedOptions();
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	public String toString() {
		return delegate.toString();
	}
}
