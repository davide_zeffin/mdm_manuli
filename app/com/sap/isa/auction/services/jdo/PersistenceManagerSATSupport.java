/*
 * Created on Dec 30, 2004
 */
package com.sap.isa.auction.services.jdo;

import java.util.Collection;

import javax.jdo.Extent;
import javax.jdo.PersistenceManager;
import javax.jdo.PersistenceManagerFactory;
import javax.jdo.Query;
import javax.jdo.Transaction;

import com.sap.isa.auction.sat.SATConstants;
import com.sap.isa.auction.sat.SATSupport;
import com.sap.util.monitor.jarm.IMonitor;

/**
 * @author I802891
 *
 */
public final class PersistenceManagerSATSupport implements PersistenceManager {
	private PersistenceManager delegate = null;
	private SATSupport sat = null;
	private TransactionSATSupport tx = null;
	
	private static final String SAT_PREFIX = SATConstants.JDO_PREFIX + ":PM:";
	
	public PersistenceManagerSATSupport(PersistenceManager actual, SATSupport sat) {
		this.delegate = actual;
		this.sat = sat;
		tx = new TransactionSATSupport(delegate.currentTransaction(),sat);
	}
	
	private IMonitor getRequestMonitor() {
		if(sat != null) return sat.getRequestMonitor();
		return null;
	}
	
	
	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	public int hashCode() {
		return delegate.hashCode();
	}

	/**
	 * @param arg0
	 * @param arg1
	 * @param arg2
	 * @return
	 */
	public Query newQuery(Class arg0, Collection arg1, String arg2) {
		Query actual = delegate.newQuery(arg0,arg1,arg2);
		QuerySATSupport querySAT = new QuerySATSupport(actual,sat);
		return querySAT;
	}

	/**
	 * @param arg0
	 */
	public void deletePersistent(Object arg0) {
		delegate.deletePersistent(arg0);
	}

	/**
	 * @param arg0
	 */
	public void refreshAll(Collection arg0) {
		final String METHOD = "refreshAll";
		final String ACTIVITY = SAT_PREFIX + METHOD;
		IMonitor monitor = getRequestMonitor();
		if(monitor != null) monitor.startComponent(ACTIVITY);
		delegate.refreshAll(arg0);
		if(monitor != null) monitor.endComponent(ACTIVITY);
	}

	/**
	 * @param arg0
	 */
	public void makePersistent(Object arg0) {
		final String METHOD = "makePersistent";
		final String ACTIVITY = SAT_PREFIX + METHOD;
		IMonitor monitor = getRequestMonitor();
		if(monitor != null) monitor.startComponent(ACTIVITY);
		delegate.makePersistent(arg0);
		if(monitor != null) monitor.endComponent(ACTIVITY);
	}

	/**
	 * @param arg0
	 * @return
	 */
	public Query newQuery(Extent arg0) {
		Query actual = delegate.newQuery(arg0);
		QuerySATSupport querySAT = new QuerySATSupport(actual,sat);
		return querySAT;
	}

	/**
	 * @param arg0
	 */
	public void setMultithreaded(boolean arg0) {
		delegate.setMultithreaded(arg0);
	}

	/**
	 * @param arg0
	 */
	public void makeNontransactionalAll(Collection arg0) {
		delegate.makeNontransactionalAll(arg0);
	}

	/**
	 * @param arg0
	 * @return
	 */
	public Object getTransactionalObjectId(Object arg0) {
		return delegate.getTransactionalObjectId(arg0);
	}

	/**
	 * @param arg0
	 * @return
	 */
	public Class getObjectIdClass(Class arg0) {
		return delegate.getObjectIdClass(arg0);
	}

	/**
	 * @param arg0
	 * @param arg1
	 * @return
	 */
	public Extent getExtent(Class arg0, boolean arg1) {
		return delegate.getExtent(arg0, arg1);
	}

	/**
	 * @param arg0
	 */
	public void makeNontransactional(Object arg0) {
		delegate.makeNontransactional(arg0);
	}

	/**
	 * @return
	 */
	public Transaction currentTransaction() {
		return tx;
	}

	/**
	 * @param arg0
	 */
	public void makeTransientAll(Collection arg0) {
		final String METHOD = "makeTransientAll";
		final String ACTIVITY = SAT_PREFIX + METHOD;
		IMonitor monitor = getRequestMonitor();
		if(monitor != null) monitor.startComponent(ACTIVITY);
		delegate.makeTransientAll(arg0);
		if(monitor != null) monitor.endComponent(ACTIVITY);
	}

	/**
	 * @param arg0
	 * @param arg1
	 * @return
	 */
	public Query newQuery(Class arg0, Collection arg1) {
		Query actual = delegate.newQuery(arg0,arg1);
		QuerySATSupport querySAT = new QuerySATSupport(actual,sat);
		return querySAT;
	}

	/**
	 * @param arg0
	 */
	public void evict(Object arg0) {
		delegate.evict(arg0);
	}

	/**
	 * @param arg0
	 */
	public void deletePersistentAll(Collection arg0) {
		final String METHOD = "deletePersistentAll";
		final String ACTIVITY = SAT_PREFIX + METHOD;
		IMonitor monitor = getRequestMonitor();
		if(monitor != null) monitor.startComponent(ACTIVITY);
		delegate.deletePersistentAll(arg0);
		if(monitor != null) monitor.endComponent(ACTIVITY);
	}

	/**
	 * @param arg0
	 * @return
	 */
	public Query newQuery(Class arg0) {
		Query actual = delegate.newQuery(arg0);
		QuerySATSupport querySAT = new QuerySATSupport(actual,sat);
		return querySAT;
	}

	/**
	 * @param arg0
	 */
	public void makeTransactional(Object arg0) {
		delegate.makeTransactional(arg0);
	}

	/**
	 * @param arg0
	 * @return
	 */
	public Query newQuery(Object arg0) {
		Query actual = delegate.newQuery(arg0);
		QuerySATSupport querySAT = new QuerySATSupport(actual,sat);
		return querySAT;
	}

	/**
	 * @return
	 */
	public boolean getMultithreaded() {
		return delegate.getMultithreaded();
	}

	/**
	 * 
	 */
	public void evictAll() {
		delegate.evictAll();
	}

	/**
	 * @param arg0
	 */
	public void setIgnoreCache(boolean arg0) {
		delegate.setIgnoreCache(arg0);
	}

	/**
	 * 
	 */
	public void refreshAll() {
		final String METHOD = "refreshAll";
		final String ACTIVITY = SAT_PREFIX + METHOD;
		IMonitor monitor = getRequestMonitor();
		if(monitor != null) monitor.startComponent(ACTIVITY);
		delegate.refreshAll();
		if(monitor != null) monitor.endComponent(ACTIVITY);
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	public boolean equals(Object obj) {
		return delegate.equals(obj);
	}

	/**
	 * @param arg0
	 */
	public void retrieveAll(Object[] arg0) {
		delegate.retrieveAll(arg0);
	}

	/**
	 * @param arg0
	 * @param arg1
	 * @return
	 */
	public Query newQuery(String arg0, Object arg1) {
		Query actual = delegate.newQuery(arg0,arg1);
		QuerySATSupport querySAT = new QuerySATSupport(actual,sat);
		return querySAT;
	}

	/**
	 * @return
	 */
	public boolean isClosed() {
		return delegate.isClosed();
	}

	/**
	 * @param arg0
	 */
	public void refreshAll(Object[] arg0) {
		delegate.refreshAll(arg0);
	}

	/**
	 * @param arg0
	 */
	public void refresh(Object arg0) {
		delegate.refresh(arg0);
	}

	/**
	 * @param arg0
	 */
	public void deletePersistentAll(Object[] arg0) {
		final String METHOD = "deletePersistentAll";
		final String ACTIVITY = SAT_PREFIX + METHOD;
		IMonitor monitor = getRequestMonitor();
		if(monitor != null) monitor.startComponent(ACTIVITY);
		delegate.deletePersistentAll(arg0);
		if(monitor != null) monitor.endComponent(ACTIVITY);
	}

	/**
	 * 
	 */
	public void close() {
		delegate.close();
	}

	/**
	 * @param arg0
	 */
	public void setUserObject(Object arg0) {
		delegate.setUserObject(arg0);
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	public String toString() {
		return delegate.toString();
	}

	/**
	 * @param arg0
	 */
	public void retrieveAll(Collection arg0) {
		delegate.retrieveAll(arg0);
	}

	/**
	 * @param arg0
	 */
	public void retrieve(Object arg0) {
		delegate.retrieve(arg0);
	}

	/**
	 * @param arg0
	 * @param arg1
	 * @return
	 */
	public Object newObjectIdInstance(Class arg0, String arg1) {
		return delegate.newObjectIdInstance(arg0, arg1);
	}

	/**
	 * @param arg0
	 */
	public void makePersistentAll(Object[] arg0) {
		delegate.makePersistentAll(arg0);
	}

	/**
	 * @param arg0
	 */
	public void makeTransactionalAll(Collection arg0) {
		delegate.makeTransactionalAll(arg0);
	}

	/**
	 * @param arg0
	 */
	public void makePersistentAll(Collection arg0) {
		final String METHOD = "makePersistentAll";
		final String ACTIVITY = SAT_PREFIX + METHOD;
		IMonitor monitor = getRequestMonitor();
		if(monitor != null) monitor.startComponent(ACTIVITY);
		delegate.makePersistentAll(arg0);
		if(monitor != null) monitor.endComponent(ACTIVITY);
	}

	/**
	 * @return
	 */
	public Object getUserObject() {
		return delegate.getUserObject();
	}

	/**
	 * @param arg0
	 */
	public void evictAll(Collection arg0) {
		delegate.evictAll(arg0);
	}

	/**
	 * @param arg0
	 */
	public void makeTransient(Object arg0) {
		delegate.makeTransient(arg0);
	}

	/**
	 * @param arg0
	 */
	public void makeTransactionalAll(Object[] arg0) {
		delegate.makeTransactionalAll(arg0);
	}

	/**
	 * @param arg0
	 * @param arg1
	 * @return
	 */
	public Query newQuery(Class arg0, String arg1) {
		Query actual = delegate.newQuery(arg0,arg1);
		QuerySATSupport querySAT = new QuerySATSupport(actual,sat);
		return querySAT;
	}

	/**
	 * @return
	 */
	public PersistenceManagerFactory getPersistenceManagerFactory() {
		return delegate.getPersistenceManagerFactory();
	}

	/**
	 * @param arg0
	 * @param arg1
	 * @return
	 */
	public Object getObjectById(Object arg0, boolean arg1) {
		return delegate.getObjectById(arg0, arg1);
	}

	/**
	 * @param arg0
	 */
	public void makeNontransactionalAll(Object[] arg0) {
		delegate.makeNontransactionalAll(arg0);
	}

	/**
	 * @param arg0
	 */
	public void makeTransientAll(Object[] arg0) {
		final String METHOD = "makeTransientAll";
		final String ACTIVITY = SAT_PREFIX + METHOD;
		IMonitor monitor = getRequestMonitor();
		if(monitor != null) monitor.startComponent(ACTIVITY);
		delegate.makeTransientAll(arg0);
		if(monitor != null) monitor.endComponent(ACTIVITY);
	}

	/**
	 * @return
	 */
	public Query newQuery() {
		Query actual = delegate.newQuery();
		QuerySATSupport querySAT = new QuerySATSupport(actual,sat);
		return querySAT;
	}

	/**
	 * @param arg0
	 */
	public void evictAll(Object[] arg0) {
		delegate.evictAll(arg0);
	}

	/**
	 * @param arg0
	 * @param arg1
	 * @return
	 */
	public Query newQuery(Extent arg0, String arg1) {
		Query actual = delegate.newQuery(arg0,arg1);
		QuerySATSupport querySAT = new QuerySATSupport(actual,sat);
		return querySAT;
	}

	/**
	 * @return
	 */
	public boolean getIgnoreCache() {
		return delegate.getIgnoreCache();
	}

	/**
	 * @param arg0
	 * @return
	 */
	public Object getObjectId(Object arg0) {
		return delegate.getObjectId(arg0);
	}

}
