/*
 * Created on Sep 19, 2003
 *
 */
package com.sap.isa.auction.services;

import com.sap.tc.logging.Category;
import com.sap.tc.logging.Location;
import com.sap.isa.auction.exception.i18n.ErrorMessageBundle;
import com.sap.isa.auction.exception.i18n.I18nErrorMessage;

/**
 * @author I802891
 *
 */
public class ServiceUserException extends com.sap.isa.auction.exception.UserException {

	private static final String EXC_ID = "ServiceUserException";
	private static final char SEP = '_';
	
	public static final String MISSING_USER_CONTEXT = EXC_ID + SEP + 1;
	public static final String MISSING_USER_EBAY_MAPPING = EXC_ID + SEP + 2;
	public static final String INVALID_TOKEN = EXC_ID + SEP + 3;
	
	static {
		ErrorMessageBundle.setDefaultMessage(MISSING_USER_CONTEXT,"User context is missing to perform the operation");
		ErrorMessageBundle.setDefaultMessage(MISSING_USER_EBAY_MAPPING,"User {0} mapping to ebay user is missing");
		ErrorMessageBundle.setDefaultMessage(INVALID_TOKEN,"User {0} has invalid EBay Token. Pls Contact System Administrator");			
	}
	/**
	 * @param arg0
	 */
	public ServiceUserException(Throwable arg0) {
		super(arg0);
		// TODO Auto-generated constructor stub
	}

	/**
	 * @param arg0
	 */
	public ServiceUserException(I18nErrorMessage arg0) {
		super(arg0);
		// TODO Auto-generated constructor stub
	}

	/**
	 * @param arg0
	 * @param arg1
	 */
	public ServiceUserException(I18nErrorMessage arg0, Throwable arg1) {
		super(arg0, arg1);
		// TODO Auto-generated constructor stub
	}

	/**
	 * @param arg0
	 * @param arg1
	 * @param arg2
	 * @param arg3
	 * @param arg4
	 */
	public ServiceUserException(
		Category arg0,
		int arg1,
		Location arg2,
		I18nErrorMessage arg3,
		Throwable arg4) {
		super(arg0, arg1, arg2, arg3, arg4);
		// TODO Auto-generated constructor stub
	}
}
