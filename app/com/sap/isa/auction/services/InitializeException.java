/*
 * Created on Sep 17, 2003
 *
 */
package com.sap.isa.auction.services;

import com.sap.tc.logging.Category;
import com.sap.tc.logging.Location;
import com.sap.isa.auction.exception.AuctionExceptionBase;
import com.sap.isa.auction.exception.i18n.I18nErrorMessage;
import com.sap.isa.auction.exception.i18n.ErrorMessageBundle;

/**
 * @author I802891
 *
 */
public class InitializeException extends AuctionExceptionBase {

	private static final String EXC_ID = "InitializeException";
	public static final String INVALID_LICENSE_KEY = EXC_ID + 1;	
	public static final String INVALID_SITEID = EXC_ID + 2;
	public static final String API_VERSION_NOT_SUPPORTED = EXC_ID + 3;
	public static final String URL_IS_INVALID = EXC_ID + 4;	
	public static final String JNDI_LOOKUP_FAILED = EXC_ID + 5;	
    public static final String COMPONENT_INIT_FAILED = EXC_ID + 6;  
    public static final String COMMUNICATOR_INIT_FAILED = EXC_ID + 7;  
	
	static {
		ErrorMessageBundle.setDefaultMessage(INVALID_LICENSE_KEY,"License is invalid");
		ErrorMessageBundle.setDefaultMessage(INVALID_SITEID,"Site ID {0} is invalid");
		ErrorMessageBundle.setDefaultMessage(API_VERSION_NOT_SUPPORTED,"API compatibility level {0} is not supported");
		ErrorMessageBundle.setDefaultMessage(URL_IS_INVALID,"url {0} is not well formed");		
		ErrorMessageBundle.setDefaultMessage(JNDI_LOOKUP_FAILED,"JNDI lookup for name {0} failed");		
		ErrorMessageBundle.setDefaultMessage(COMPONENT_INIT_FAILED,"{0} initialization failed");		
	}
	
	/**
	 * @param arg0
	 */
	public InitializeException(Throwable arg0) {
		super(arg0);
		// TODO Auto-generated constructor stub
	}

	/**
	 * @param arg0
	 */
	public InitializeException(I18nErrorMessage arg0) {
		super(arg0);
		// TODO Auto-generated constructor stub
	}

	/**
	 * @param arg0
	 * @param arg1
	 */
	public InitializeException(I18nErrorMessage arg0, Throwable arg1) {
		super(arg0, arg1);
		// TODO Auto-generated constructor stub
	}

	/**
	 * @param arg0
	 * @param arg1
	 * @param arg2
	 * @param arg3
	 * @param arg4
	 */
	public InitializeException(
		Category arg0,
		int arg1,
		Location arg2,
		I18nErrorMessage arg3,
		Throwable arg4) {
		super(arg0, arg1, arg2, arg3, arg4);
		// TODO Auto-generated constructor stub
	}

}
