package com.sap.isa.auction.services;

import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Properties;

import javax.jdo.PersistenceManagerFactory;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;

import com.sap.engine.services.applocking.TableLocking;
import com.sap.isa.auction.config.JDOConfig;
import com.sap.isa.auction.config.SchedulerConfig;
import com.sap.isa.auction.config.XCMConfigContainer;
import com.sap.isa.auction.exception.InvalidStateException;
import com.sap.isa.auction.exception.i18n.ErrorMessageBundle;
import com.sap.isa.auction.exception.i18n.I18nErrorMessage;
import com.sap.isa.auction.logging.CategoryProvider;
import com.sap.isa.auction.logging.LogHelper;
import com.sap.isa.auction.logging.TraceHelper;
import com.sap.isa.auction.sat.DefaultSATSupport;
import com.sap.isa.auction.services.jdo.PMFSATSupport;
import com.sap.isa.core.db.DBHelper;
import com.sap.isa.core.db.DBPersistenceManagerFactory;
import com.sap.isa.core.db.jdo.JDOPersistenceManagerFactory;
import com.sap.isa.core.xcm.init.ExtendedConfigInitHandler;
import com.sap.isa.core.xcm.init.ParamsConfig;
import com.sap.isa.core.xcm.scenario.ScenarioManager;
import com.sap.isa.core.xcm.xml.ParamsStrategy;
import com.sap.isa.crt.RecordingCallManagerFactory;
import com.sap.isa.crt.SimpleCallManagerFactory;
import com.sap.tc.logging.Category;
import com.sap.tc.logging.Location;

/**
 * Title:
 * Description: Internal helper class
 * Copyright:    Copyright (c) 2003
 * Company:
 * @author
 * @version 1.0
 */

/**
 * Title:
 * Description: Internal helper class
 * Copyright:    Copyright (c) 2003
 * Company:
 * @author
 * @version 1.0
 */

public class ServiceLocator {
	
	private static TableLocking tableLocking = null;
	protected static Category logger = CategoryProvider.getSystemCategory();	
	private static Location tracer = Location.getLocation(ServiceLocator.class.getName());
	protected static String JNDI_PREFIX = "java:comp/env/";
	
	/** Background Job Manager instance */
	protected BackgroundJobManagerBase jobMgr = null;
	public static Class ServiceLocatorClass = ServiceLocator.class;

	protected static Map scenario2ServiceLocators = new HashMap();

	protected String scenarioName;

	protected boolean initialized = false;
    
	/** Persistence Manager Factory instance */
	protected PersistenceManagerFactory pmFactory = null;
    
	/** Data souce */
	protected DataSource ds = null;
    
	protected Object lockObject = new Object();

	/** Recording Call Manager factory */
	protected RecordingCallManagerFactory rcmf = null;
	
	/** Simple Call Manager Factory */
	protected SimpleCallManagerFactory scmf = null;

	protected ServiceLocator(String scenarioName) {
		this.scenarioName = scenarioName;
	}

    
	public ServiceLocator()  {
	}
    
	protected void setInitialized(boolean bool){
		initialized = bool;
	}
    
	public static ServiceLocator getDefaultScenarioBaseInstance() throws InvalidStateException, InitializeException {
		final String METHOD = "getDefaultScenarioInstance";
		TraceHelper.entering(tracer, METHOD);
		TraceHelper.exiting(tracer, METHOD);
		return getBaseInstance(ScenarioManager.getScenarioManager().getDefaultScenario());
	}

	public synchronized static ServiceLocator getBaseInstance(String scenarioName) throws InvalidStateException, InitializeException {
		final String METHOD = "getInstance";
		TraceHelper.entering(tracer, METHOD);
		
		ServiceLocator locator = (ServiceLocator)scenario2ServiceLocators.get(scenarioName);
		if(locator == null) {
			try  {
				locator = (ServiceLocator)ServiceLocatorClass.newInstance();
			}  catch(Exception exc)  {
				LogHelper.logError(logger , tracer , "Unable to Instantiate Service Locator" , exc);
			}
			locator.scenarioName = scenarioName;
			scenario2ServiceLocators.put(scenarioName,locator);
			locator._initialize();
		}
		TraceHelper.exiting(tracer, METHOD);
		return locator;
		
	}

	protected void initTableLocking() throws InitializeException {
		try {
			Context initialContext = new InitialContext();
			tableLocking = (TableLocking) initialContext.lookup(TableLocking.JNDI_NAME);
		}
		catch(NamingException ne ) {
			LogHelper.logError(logger,tracer,"Error in initializing Datasource", ne);
			ErrorMessageBundle bundle = ErrorMessageBundle.getInstance();
			I18nErrorMessage msg = bundle.newI18nErrorMessage(InitializeException.JNDI_LOOKUP_FAILED);
			msg.setArguments(new Object[]{TableLocking.JNDI_NAME});
			if( DBHelper.getAlternateJDOProperties() != null )
			{
				//Unit Tests have no table locking
				if (logger.beInfo()) {
					String msgTrace = "Disable Table Locking for Unit Tests";
					logger.infoT(tracer, msgTrace);
				}
				return;
			}
			throw new InitializeException(msg,ne);					
		}		
	}

	private void checkInitializedBase() throws InvalidStateException {
		if(!initialized) {
			ErrorMessageBundle bundle = ErrorMessageBundle.getInstance();
			I18nErrorMessage msg = bundle.newI18nErrorMessage(InvalidStateException.NOT_INITIALIZED);
			InvalidStateException ex = new InvalidStateException(msg);
			throw ex;
		} 
	}

	/**
	 * Initialize all services for use by Logic layer
	 */
	protected void _initialize() throws InitializeException {
		if(initialized) return;
		final String METHOD = "_initialize";
		TraceHelper.entering(tracer, METHOD);
		initialized = true;
		try {
			// Order of initialization is important
			initDataSource(); 		
			if (logger.beInfo()) {
				String msg = "Initialzed the Datasource for scenario " + scenarioName;
				logger.infoT(tracer, msg);
			}
			initPersistenceManagerFactory();
			if (logger.beInfo()) {
				String msg = "Initialzed the PMF for scenario " + scenarioName;
				logger.infoT(tracer, msg);
			}
			// @@todo temp hack for JDo meta data loading bug
			// Order of initialization is important
			PersistenceManagerFactory pmf = getPersistenceManagerFactory();
			initSimpleCallManagerFactory();
			if (logger.beInfo()) {
				String msg = "Initialzed the simple call manager factory for scenario " + scenarioName;
				logger.infoT(tracer, msg);
			}
			
			initRecordingCallManagerFactory();
			if (logger.beInfo()) {
				String msg = "Initialzed the recording call manager factory for scenario " + scenarioName;
				logger.infoT(tracer, msg);
			}
			
			initBackgroundJobManager();
			if (logger.beInfo()) {
				String msg = "Initialzed the background job manager factory for scenario " + scenarioName;
				logger.infoT(tracer, msg);
			}
			
			// Order of initialization is important
			initTableLocking();
			// done in the last
			TraceHelper.exiting(tracer, METHOD);	
		}
		catch(InitializeException ex) {
			initialized = false;
			ErrorMessageBundle bundle = ErrorMessageBundle.getInstance();
			I18nErrorMessage msg = bundle.newI18nErrorMessage(InitializeException.COMPONENT_INIT_FAILED);
			msg.setArguments(new String[]{"ServiceLocator"});
			LogHelper.logError(logger,tracer,ex.getMessage(),ex);
			throw ex;
		}
		catch(Throwable th) {
			initialized = false;
			ErrorMessageBundle bundle = ErrorMessageBundle.getInstance();
			I18nErrorMessage msg = bundle.newI18nErrorMessage(InitializeException.COMPONENT_INIT_FAILED);
			msg.setArguments(new String[]{"ServiceLocator"});
			InitializeException ex = new InitializeException(msg,th);
			LogHelper.logError(logger,tracer,ex.getMessage(),ex);
			throw ex;
		}
	}

	/**
	 * Closes all the ServiceLocator
	 *
	 */
	public static synchronized void close() {
		final String METHOD = "close";
		TraceHelper.entering(tracer, METHOD);
		Iterator iter = scenario2ServiceLocators.keySet().iterator(); 
		while(iter.hasNext()) {
			String scenarioName = (String)iter.next();
			ServiceLocator servLocator = (ServiceLocator) scenario2ServiceLocators.get(scenarioName);
			servLocator._close();
		}
		if (logger.beInfo()) {
			String msg = "About to call the static call of shutDown of the Background Job Manager";
			logger.infoT(tracer, msg);
		}
		scenario2ServiceLocators.clear();
		TraceHelper.exiting(tracer, METHOD);
	}
    
	/**
	 * Stop all services for use by Logic layer
	 */
	protected synchronized void _close() {
		final String METHOD = "_close";
		TraceHelper.entering(tracer, METHOD);
		if(!initialized) return;
		clearCache();
		initialized = false;
		TraceHelper.exiting(tracer, METHOD);
	}

	/**
	 *
	 */
	public synchronized void clearCache() {
		final String METHOD = "clearCache";
		TraceHelper.entering(tracer, METHOD);
		try {
			if(jobMgr != null) {
				if (logger.beInfo()) {
					String msg = "About to close the Background job manager " + scenarioName;
					logger.infoT(tracer, msg);
				}
				jobMgr.close();
			} 
		}
		catch(Exception ex) {
			LogHelper.logError(logger,tracer,"Failed to stop Scheduler",ex);
		}
		try {
			if (logger.beInfo()) {
				String msg = "About to close Recording Call Manager factory " + scenarioName;
				logger.infoT(tracer, msg);
			}
			rcmf.close();
		}
		catch(Exception ex) {
			LogHelper.logError(logger,tracer,"Failed to close Recording Call Manager Factory",ex);	    	
		}
		try {
			if (logger.beInfo()) {
				String msg = "About to close the simple call manager factory " + scenarioName;
				logger.infoT(tracer, msg);
			}
			scmf.close();
		}
		catch(Exception ex) {
			LogHelper.logError(logger,tracer,"Failed to close Simple Call Manager factory",ex);	    	
		}
		
		pmFactory = null;
		ds = null;
		jobMgr = null;
		TraceHelper.exiting(tracer, METHOD);
	}

	/**
	 *
	 */

	
	/**
	 * If the scheduler is not run then the null is thrown
	 */
	public synchronized PersistenceManagerFactory getPersistenceManagerFactory() {
		checkInitializedBase();
		ClassLoader cl = Thread.currentThread().getContextClassLoader();
		tracer.debug(logger,"Persistence Manager Factory retreived: Classloader instance : " + cl);
		return pmFactory;
	}
	
	/**
	 *
	 */
	 protected void initPersistenceManagerFactory() throws InitializeException {
		final String METHOD = "initPersistenceManagerFactory";
		TraceHelper.entering(tracer, METHOD);
		ClassLoader cl = Thread.currentThread().getContextClassLoader();
		if(cl == null) cl = this.getClass().getClassLoader();
		XCMConfigContainer loader = XCMConfigContainer.getBaseInstance(scenarioName);
		JDOConfig configData = loader.getJDOConfig();
		logger.infoT(tracer,"Persistence Manager Factory initialized: Classloader instance : " + cl);
		if(configData.getJdoJNDIName()!=null && configData.getJdoJNDIName().length() > 0 ) {
			try {
				if (logger.beInfo()) {
					String msg = "About to initialize the pmf through the JNDI names for scenario " + scenarioName;
					logger.infoT(tracer, msg);
				}
				Context ctx = new InitialContext(null);
				PersistenceManagerFactory actual = (javax.jdo.PersistenceManagerFactory) ctx.lookup(JNDI_PREFIX +loader.getJDOConfig().getJdoJNDIName());
				pmFactory = new PMFSATSupport(actual,new DefaultSATSupport());
			}
			catch(NamingException ne ) {
				Properties props = DBHelper.getAlternateJDOProperties();
				if( props != null)
				{
					// Set pmFactory for automated tests
					props.setProperty("javax.jdo.PersistenceManagerFactoryClass","com.sap.jdo.sql.SQLPMF");
					pmFactory = javax.jdo.JDOHelper.getPersistenceManagerFactory(props,cl);
				}
				else
				{
					LogHelper.logError(logger,tracer,"Error in initializing Datasource", ne);
					ErrorMessageBundle bundle = ErrorMessageBundle.getInstance();
					I18nErrorMessage msg = bundle.newI18nErrorMessage(InitializeException.JNDI_LOOKUP_FAILED);
					msg.setArguments(new Object[]{loader.getJDOConfig().getJdoJNDIName()});
					throw new InitializeException(msg,ne);					
				}
			}
		}
		else {
			if (logger.beInfo()) {
				String msg = "About to initialize the pmf through the username/password/URL.. for scenario " + scenarioName;
				logger.infoT(tracer, msg);
			}
			Properties props = new Properties();
			props.setProperty("javax.jdo.PersistenceManagerFactoryClass",configData.getPMFClassName());
			props.setProperty("javax.jdo.option.ConnectionDriverName",configData.getDriverClassName());
			props.setProperty("javax.jdo.option.ConnectionURL",configData.getURL());
			props.setProperty("javax.jdo.option.ConnectionUserName",configData.getUser());
			props.setProperty("javax.jdo.option.ConnectionPassword",configData.getPassword());
			pmFactory = javax.jdo.JDOHelper.getPersistenceManagerFactory(props,cl);
		}
		TraceHelper.exiting(tracer, METHOD);
	}

	public synchronized DataSource getDataSource() {
		checkInitializedBase();
		return ds;
	}
	
	/**
	 * 
	 * @return null if the JDO jndi name is not specified for testing purposes only 
	 * 				within the Tomcat env 
	 * @throws InitializeException
	 */
	 protected void initDataSource() throws InitializeException {
		final String METHOD = "initDataSource";
		TraceHelper.entering(tracer, METHOD);
		checkInitializedBase();		
		XCMConfigContainer cc = XCMConfigContainer.getBaseInstance(scenarioName);
		JDOConfig jdoConfig = cc.getJDOConfig();
		try {
			if(ds == null ) {
				if (logger.beInfo()) {
					String msg = "About to initialize the datasource using the userName/password/URL for scenario " + scenarioName;
					logger.infoT(tracer, msg);
				}
				if(jdoConfig.getDataSourceJNDIName()==null || 
					jdoConfig.getDataSourceJNDIName().length() == 0) {
					// For testing purposes only
					ds =  new DataSourceImpl(jdoConfig.getURL(),
								jdoConfig.getUser(),jdoConfig.getPassword(),
								jdoConfig.getDriverClassName());
				}
				else {
					if (logger.beInfo()) {
						String msg = "About to initialize the datasource using the jndi datasource for scenario " + scenarioName;
						logger.infoT(tracer, msg);
					}
					Context ctx = new InitialContext(null);
					ds = DBHelper.getApplicationSpecificDataSource(jdoConfig.getDataSourceJNDIName());					
				}
			}
		}
		catch (NamingException ne) {
			LogHelper.logError(logger,tracer,"Error in initializing Datasource", ne);
			ErrorMessageBundle bundle = ErrorMessageBundle.getInstance();
			I18nErrorMessage msg = bundle.newI18nErrorMessage(InitializeException.JNDI_LOOKUP_FAILED);
			msg.setArguments(new Object[]{jdoConfig.getJdoJNDIName()});
			throw new InitializeException(msg,ne);		
		}
		TraceHelper.exiting(tracer, METHOD);
	}

	/**
	 *
	 */
	public synchronized SimpleCallManagerFactory getSimpleCallManagerFactory() {
		checkInitializedBase();
		return scmf;
	}
   	
	private void initSimpleCallManagerFactory() {
		final String METHOD = "initSimpleCallManagerFactory";
		TraceHelper.entering(tracer, METHOD);
		if (logger.beInfo()) {
			String msg = "About to initialize the simple call manager factory for scenario " + scenarioName;
			logger.infoT(tracer, msg);
		}
		scmf = new SimpleCallManagerFactory();
		scmf.initialize(null);
		TraceHelper.exiting(tracer, METHOD);
	}
   	
   

	public synchronized RecordingCallManagerFactory getRecordingCallManagerFactory() {
		checkInitializedBase();
		return rcmf;
	}
   	
	public void initRecordingCallManagerFactory() throws InitializeException {
		final String METHOD = "initRecordingCallManagerFactory";
		TraceHelper.entering(tracer, METHOD);
		if (logger.beInfo()) {
			String msg = "About to initialize the recording call factory for scenario " + scenarioName;
			logger.infoT(tracer, msg);
		}
		rcmf = new RecordingCallManagerFactory();
		rcmf.setLogger(logger);
		rcmf.setJDOPMF(getPersistenceManagerFactory());
		Properties props = new Properties();
		props.setProperty(RecordingCallManagerFactory.PROP_RECORD_CALL_SUCCESS,"false");
		rcmf.initialize(props);
		TraceHelper.exiting(tracer, METHOD);
	}
    

	public synchronized BackgroundJobManagerBase getBackgroundJobManager() {
		checkInitializedBase();
		return jobMgr;
	}
	
	/**
	 * If the scheduler is not run then the null is thrown
	 */
	private void initBackgroundJobManager() throws InitializeException {
		final String METHOD = "initBackgroundJobManager";
		TraceHelper.entering(tracer, METHOD);
		if(jobMgr == null) {
			if (logger.beInfo()) {
				String msg = "Background job manager is null.So,Reading config data for initialization for scenario " + scenarioName;
				logger.infoT(tracer, msg);
			}
			ClassLoader cl = Thread.currentThread().getContextClassLoader();
			if(cl == null) cl = this.getClass().getClassLoader();
			XCMConfigContainer loader = XCMConfigContainer.getBaseInstance(scenarioName);
			SchedulerConfig configData = loader.getSchedulerConfig();
			Collection coll = loader.getSchedulerTaskConfig();
			//configData = ApplicationConfigContainer.getInstance().getSchedulerConfig();
			if(configData == null || !configData.isRunScheduler()) return;
			ParamsConfig params=ExtendedConfigInitHandler.getParamsConfig();
			String applicationName="";
			if( params != null)
			{
				 ParamsStrategy.Params p= params.getParamsContainer();
				 applicationName=p.getParamValue("application.name");
			}            
			if(applicationName!=null && applicationName.length() > 0 ){
				configData.setSchedulerId(applicationName);
			}
			if (logger.beInfo()) {
				String msg = "About to initialize the Background job manager";
				logger.infoT(tracer, msg);
			}
			jobMgr = getBkMgr(configData,coll);
			jobMgr.setServiceLocator(this);
			try {
				if (logger.beInfo()) {
					String msg = "About to initialize the background job manager";
					logger.infoT(tracer, msg);
				}
				jobMgr.initialize();
			}
			catch(Exception ex) {
				logger.errorT(tracer,"Error while initializing the Background job manager");
				LogHelper.logError(logger,tracer,ex.getMessage(),ex);
				ErrorMessageBundle bundle = ErrorMessageBundle.getInstance();
				I18nErrorMessage msg = bundle.newI18nErrorMessage(InitializeException.COMPONENT_INIT_FAILED);
				msg.setArguments(new String[]{"BackgroungJobManager"});
				throw new InitializeException(msg,ex);
			}
		}
		TraceHelper.exiting(tracer, METHOD);
	}
	/**
	 * Base class should instantiate this 
	 * @param configData
	 * @param coll
	 * @return
	 */
	protected BackgroundJobManagerBase getBkMgr(SchedulerConfig configData,Collection coll){
		return null;
	}

	// Inner Class for testing purposes only within the standalone environment
	private static class DataSourceImpl implements DataSource {
		private String userName;
		private String password;
		private String url;
		private String driverName;

		public DataSourceImpl(String url,String userName,String password,String driverName) {
			this.url = url;
			this.userName = userName;
			this.password = password;
			this.driverName = driverName;
		}
		/* (non-Javadoc)
		 * @see javax.sql.DataSource#getConnection()
		 */
		public Connection getConnection() throws SQLException {
			try {
				Class.forName(driverName);
			} catch (ClassNotFoundException e) {
				throw new SQLException("Class not found " + e.getMessage());
			}
			Connection conn = DriverManager.getConnection(url,userName,
			password);
			return conn;
		}

		/* (non-Javadoc)
		 * @see javax.sql.DataSource#getConnection(java.lang.String, java.lang.String)
		 */
		public Connection getConnection(String arg0, String arg1) throws SQLException {
			return getConnection();
		}

		/* (non-Javadoc)
		 * @see javax.sql.DataSource#getLogWriter()
		 */
		public PrintWriter getLogWriter() throws SQLException {
			return null;
		}

		/* (non-Javadoc)
		 * @see javax.sql.DataSource#getLoginTimeout()
		 */
		public int getLoginTimeout() throws SQLException {
			return 0;
		}

		/* (non-Javadoc)
		 * @see javax.sql.DataSource#setLogWriter(java.io.PrintWriter)
		 */
		public void setLogWriter(PrintWriter arg0) throws SQLException {
		}

		/* (non-Javadoc)
		 * @see javax.sql.DataSource#setLoginTimeout(int)
		 */
		public void setLoginTimeout(int arg0) throws SQLException {
		}
	}
	/**
	 * @return
	 */
	public String getScenarioName() {
		return scenarioName;
	}
	/**
	 * @return
	 */
	public TableLocking getTableLocking() {
		return tableLocking;
	}

}