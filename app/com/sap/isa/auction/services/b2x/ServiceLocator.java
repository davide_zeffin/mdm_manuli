package com.sap.isa.auction.services.b2x;

import java.util.Iterator;

import javax.jdo.PersistenceManager;
import javax.jdo.PersistenceManagerFactory;
import javax.jdo.Query;

import com.sap.isa.auction.businessobject.b2x.jdo.AuctionDAO;
import com.sap.isa.auction.businessobject.b2x.jdo.PrivateAuctionDAO;
import com.sap.isa.auction.exception.InvalidStateException;
import com.sap.isa.auction.exception.i18n.ErrorMessageBundle;
import com.sap.isa.auction.exception.i18n.I18nErrorMessage;
import com.sap.isa.auction.logging.LogHelper;
import com.sap.isa.auction.logging.TraceHelper;
import com.sap.isa.auction.services.InitializeException;
import com.sap.isa.core.xcm.scenario.ScenarioManager;
import com.sap.tc.logging.Location;

/**
 * Title:
 * Description: Internal helper class
 * Copyright:    Copyright (c) 2003
 * Company:
 * @author
 * @version 1.0
 */

public class ServiceLocator
	extends com.sap.isa.auction.services.ServiceLocator {

	private static Location tracer =
		Location.getLocation(ServiceLocator.class.getName());

	private boolean initialized = false;



	protected ServiceLocator(String scenarioName) {
		super(scenarioName);
	}

	public ServiceLocator() {
		super();
	}

	protected void checkInitialized() throws InvalidStateException {
		if (!initialized) {
			ErrorMessageBundle bundle = ErrorMessageBundle.getInstance();
			I18nErrorMessage msg =
				bundle.newI18nErrorMessage(
					InvalidStateException.NOT_INITIALIZED);
			InvalidStateException ex = new InvalidStateException(msg);
			throw ex;
		}
	}

	public static ServiceLocator getDefaultScenarioInstance()
		throws InvalidStateException, InitializeException {
		final String METHOD = "getDefaultScenarioInstance";
		TraceHelper.entering(tracer, METHOD);
		TraceHelper.exiting(tracer, METHOD);
		return getInstance(
			ScenarioManager.getScenarioManager().getDefaultScenario());
	}

	public synchronized static ServiceLocator getInstance(String scenarioName)
		throws InvalidStateException, InitializeException {
		final String METHOD = "getInstance";
		TraceHelper.entering(tracer, METHOD);
		ServiceLocator locator = (ServiceLocator) getBaseInstance(scenarioName);
		TraceHelper.exiting(tracer, METHOD);
		return locator;
	}

	/**
	 * Initialize all services for use by Logic layer
	 */
	protected void _initialize()
		throws com.sap.isa.auction.services.InitializeException {
		if (initialized)
			return;
		final String METHOD = "_initialize";
		TraceHelper.entering(tracer, METHOD);
		super.setInitialized(true);
		initialized = true;
		try {
			
			// Order of initialization is important
			initDataSource(); 		
			if (logger.beInfo()) {
				String msg = "Initialzed the Datasource for scenario " + scenarioName;
				logger.infoT(tracer, msg);
			}
			initPersistenceManagerFactory();
			if (logger.beInfo()) {
				String msg = "Initialzed the PMF for scenario " + scenarioName;
				logger.infoT(tracer, msg);
			}
			// @@todo temp hack for JDo meta data loading bug
			// Order of initialization is important
			PersistenceManagerFactory pmf = getPersistenceManagerFactory();
			PersistenceManager pm  =  pmf.getPersistenceManager();
			Query query  = pm.newQuery(AuctionDAO.class);
			query.compile();
			query.closeAll();
			
			query  = pm.newQuery(PrivateAuctionDAO.class);
			query.compile();
			query.closeAll();
			pm.close();
			initTableLocking();
			TraceHelper.exiting(tracer, METHOD);
		} catch (Throwable th) {
			initialized = false;
			ErrorMessageBundle bundle = ErrorMessageBundle.getInstance();
			I18nErrorMessage msg =
				bundle.newI18nErrorMessage(
					com
						.sap
						.isa
						.auction
						.services
						.InitializeException
						.COMPONENT_INIT_FAILED);
			msg.setArguments(new String[] { "ServiceLocator" });
			com.sap.isa.auction.services.InitializeException ex =
				new com.sap.isa.auction.services.InitializeException(
					msg,
					th);
			LogHelper.logError(logger, tracer, ex.getMessage(), ex);
			throw ex;
		}
	}
	
	/**
	 * Closes all the ServiceLocator
	 *
	 */
	public static synchronized void close() {
		final String METHOD = "close";
		com.sap.isa.auction.services.ServiceLocator.close();
		TraceHelper.entering(tracer, METHOD);
		Iterator iter = scenario2ServiceLocators.keySet().iterator();
		while (iter.hasNext()) {
			String scenarioName = (String) iter.next();
			ServiceLocator servLocator =
				(ServiceLocator) scenario2ServiceLocators.get(scenarioName);
			servLocator._close();
		}
		if (logger.beInfo()) {
			String msg =
				"About to call the static call of shutDown of the Background Job Manager";
			logger.infoT(tracer, msg);
		}
		scenario2ServiceLocators.clear();
		TraceHelper.exiting(tracer, METHOD);
	}

	/**
	 * Stop all services for use by Logic layer
	 */
	protected synchronized void _close() {
		final String METHOD = "_close";
		TraceHelper.entering(tracer, METHOD);
		if (!initialized)
			return;
		clearCache();
		initialized = false;
		TraceHelper.exiting(tracer, METHOD);
	}

}