/*
 * Created on Apr 19, 2004
 *
 * Title:        Internet Sales Asset Remarketing
 * Description:
 * Copyright:    Copyright (c) 2004
 * Company:      SAP Labs LLC
 * @author
 * @version 1.0
 */
package com.sap.isa.auction.services.b2x;


import java.text.ParseException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;

import javax.jdo.JDOException;
import javax.jdo.PersistenceManager;
import javax.jdo.PersistenceManagerFactory;
import javax.jdo.Query;

import com.sap.isa.auction.businessobject.b2x.jdo.SchedulerJobMapDAO;
import com.sap.isa.auction.config.SchedulerConfig;
import com.sap.isa.auction.exception.SystemException;
import com.sap.isa.auction.exception.i18n.ErrorMessageBundle;
import com.sap.isa.auction.exception.i18n.I18nErrorMessage;
import com.sap.isa.auction.logging.LogHelper;
import com.sap.isa.auction.logging.TraceHelper;
import com.sap.isa.auction.services.BackgroundJobManagerBase;
import com.sap.isa.auction.services.SchedulerTaskTypeBaseEnum;
import com.sap.isa.auction.services.b2x.scheduler.CloseAuctionTask;
import com.sap.isa.auction.services.b2x.scheduler.PublishAuctionTask;
import com.sap.isa.auction.services.b2x.scheduler.ScheduledTask;
import com.sap.isa.auction.services.b2x.scheduler.WinnerPollTask;
import com.sap.isa.services.schedulerservice.Job;
import com.sap.isa.services.schedulerservice.Scheduler;
import com.sap.isa.services.schedulerservice.SchedulerFactory;

/**
 * Title:
 * Description: Maintains Auction specific background job definitions
 *              Maintains mappings between seller and jobs
 *              Performs initialization of Scheduler
 *
 * Copyright:    Copyright (c) 2003
 * Company:
 * @author
 * @version 1.0
 */

public  class BackgroundJobManager extends BackgroundJobManagerBase {

	/**
	 * @param configData
	 * @param taskConfigsData
	 */
	public BackgroundJobManager(SchedulerConfig configData, Collection taskConfigsData) {
		super(configData, taskConfigsData);
	}

	protected  Class getTaskClass(String taskName){
		Class taskClass = null;
		// Initialize the job state variables
		if (SchedulerTaskTypeBaseEnum.STR_WINNER_TASK.equals(taskName)) {
			taskClass = WinnerPollTask.class;
		}
		else if (SchedulerTaskTypeBaseEnum.STR_PUBLISH_AUCTION_TASK.equals(taskName)) {
			taskClass = PublishAuctionTask.class;
		} 
		else if (SchedulerTaskTypeBaseEnum.STR_CLOSE_AUCTION_TASK.equals(taskName)) {
			taskClass = CloseAuctionTask.class;
		}
		return taskClass;		
	}
	protected Job createJob(SchedulerConfig.TaskSchedule taskSchedule)
		throws ClassNotFoundException, ParseException {
		final String METHOD = "createJob";
		Job job = super.createJob(taskSchedule);
		// Common property for all the jobs
		job.getJobPropertiesMap().put(
			ScheduledTask.SCENARIOPARAM,
			locator.getScenarioName());
		TraceHelper.exiting(tracer,METHOD,null);
		return job;
	}	
	protected Integer getJobType(Job job) {
		SchedulerTaskTypeBaseEnum type = SchedulerTaskTypeBaseEnum.toEnum(job.getName());
		if(type != null)
			return new Integer(type.getValue());
		else
			return null; 
	}
	protected  Collection getJobIds(
		Integer jobType,
		String sellerId,
		String jobId) 
		{
		final String METHOD = "getJobIds";
		TraceHelper.entering(tracer,METHOD,null);
		PersistenceManager pm = null;
		try {
			PersistenceManagerFactory pmf =
				locator.getPersistenceManagerFactory();
			pm = pmf.getPersistenceManager();
			// @@temp: begin hack for JDO bug in loading of meta data
			Query query = pm.newQuery(SchedulerJobMapDAO.class);
			StringBuffer filter = new StringBuffer("");
			if (jobType != null) {
				filter.append("jobType == ");
				filter.append(jobType);
			}
			if (sellerId != null) {
				if (filter.length() > 0)
					filter.append(" && ");
				filter.append("sellerId == ");
				filter.append("\"");
				filter.append(sellerId);
				filter.append("\"");
			}
			if (jobId != null) {
				if (filter.length() > 0)
					filter.append(" && ");
				filter.append("jobId == ");
				filter.append("\"");
				filter.append(jobId);
				filter.append("\"");
			}
			if (filter.length() > 0)
				filter.append(" && ");
			filter.append("scenarioName == ");
			filter.append("\"");
			filter.append(locator.getScenarioName());
			filter.append("\"");
			query.setFilter(filter.toString());
			Collection coll = (Collection) query.execute();
			// @@temp : end
			// Log Warning
			if (coll == null && coll.size() < 1) {
				return null;
			}
			if (!coll.iterator().hasNext())
				return null;
			Iterator iter = coll.iterator();
			Collection returnColl = new HashSet();
			while (iter.hasNext()) {
				SchedulerJobMapDAO schedulerJobMapDAO =
					(SchedulerJobMapDAO) iter.next();
				returnColl.add(schedulerJobMapDAO.getJobId());
			}
			query.closeAll();
			TraceHelper.exiting(tracer,METHOD,null);
			return returnColl;
		} catch (Exception jdoex) {
			SystemException ex = null;
			if (jdoex instanceof JDOException) {
				// This should be fine for now?
				ErrorMessageBundle bundle = ErrorMessageBundle.getInstance();
				I18nErrorMessage msg =
					bundle.newI18nErrorMessage(SystemException.JDO_ERROR);
				// This should be fine for now?
				ex = new SystemException(msg, jdoex);
			} else {
				ErrorMessageBundle bundle = ErrorMessageBundle.getInstance();
				I18nErrorMessage msg =
					bundle.newI18nErrorMessage(SystemException.INTERNAL_ERROR);
				// This should be fine for now?
				jdoex.printStackTrace();
				ex = new SystemException(msg, jdoex);
			}
			LogHelper.logError(logger, tracer, ex);
			throw ex;
		} finally {
			// close the pm
			pm.close();
		}
		
	}



	protected  void createJobIdMap(int jobType, String sellerId, String jobId)
	{
		final String METHOD = "createJobIdMap";
		TraceHelper.entering(tracer,METHOD,null);
		PersistenceManager pm = null;
		try {
			PersistenceManagerFactory pmf =
				locator.getPersistenceManagerFactory();
			pm = pmf.getPersistenceManager();
			SchedulerJobMapDAO schedulerJobMapDAO = new SchedulerJobMapDAO();
			schedulerJobMapDAO.setJobId(jobId);
			schedulerJobMapDAO.setJobType(jobType);
			schedulerJobMapDAO.setScenarioName(locator.getScenarioName());
			pm.currentTransaction().begin();
			pm.makePersistent(schedulerJobMapDAO);
			pm.currentTransaction().commit();
		} catch (Exception jdoex) {
			// roll back the necessary stuff
			//E.g. quoation creation needs to be rollback
			//Set the quotation exp to the current date
			try {
				if (pm != null && pm.currentTransaction().isActive()) {
					// rollback the tx
					pm.currentTransaction().rollback();
				}
			} catch (Exception ex) {
				LogHelper.logWarning(logger, tracer, ex);
			}
			SystemException ex = null;
			if (jdoex instanceof JDOException) {
				// This should be fine for now?
				ErrorMessageBundle bundle = ErrorMessageBundle.getInstance();
				I18nErrorMessage msg =
					bundle.newI18nErrorMessage(SystemException.JDO_ERROR);
				// This should be fine for now?
				ex = new SystemException(msg, jdoex);
			} else {
				ErrorMessageBundle bundle = ErrorMessageBundle.getInstance();
				I18nErrorMessage msg =
					bundle.newI18nErrorMessage(SystemException.INTERNAL_ERROR);
				// This should be fine for now?
				ex = new SystemException(msg, jdoex);
			}
			LogHelper.logError(logger, tracer, ex);
			throw ex;
		} finally {
			// close the pm
			pm.close();
		}
		TraceHelper.exiting(tracer,METHOD,null);
	}

	protected  void removeJobIdMap(String jobId)
	{
		final String METHOD = "removeJobIdMap";
		TraceHelper.entering(tracer,METHOD,null);
		PersistenceManager pm = null;
		try {
			PersistenceManagerFactory pmf =
				locator.getPersistenceManagerFactory();
			pm = pmf.getPersistenceManager();
			// @@temp: begin hack for JDO bug in loading of meta data
			Query query = pm.newQuery(SchedulerJobMapDAO.class);
			query.closeAll();
			SchedulerJobMapDAO mapdao =
				(SchedulerJobMapDAO) pm.getObjectById(
					new SchedulerJobMapDAO.Id(jobId),
					false);
			pm.currentTransaction().begin();
			pm.deletePersistent(mapdao);
			pm.currentTransaction().commit();
		} catch (Exception jdoex) {
			SystemException ex = null;
			try {
				if (pm != null && pm.currentTransaction().isActive()) {
					pm.currentTransaction().rollback();
				}
			} catch (Exception e) {
				LogHelper.logWarning(logger, tracer, e);
			}
			if (jdoex instanceof JDOException) {
				// This should be fine for now?
				ErrorMessageBundle bundle = ErrorMessageBundle.getInstance();
				I18nErrorMessage msg =
					bundle.newI18nErrorMessage(SystemException.JDO_ERROR);
				// This should be fine for now?
				ex = new SystemException(msg, jdoex);
			} else {
				ErrorMessageBundle bundle = ErrorMessageBundle.getInstance();
				I18nErrorMessage msg =
					bundle.newI18nErrorMessage(SystemException.INTERNAL_ERROR);
				// This should be fine for now?
				ex = new SystemException(msg, jdoex);
			}
			LogHelper.logError(logger, tracer, ex);
			throw ex;
		} finally {
			// close the pm
			if(pm!=null)
				pm.close();
		}
		TraceHelper.exiting(tracer,METHOD,null);
		
	}
	/**
	 * All possible jobs both from the boot strap file and as well as the job id map table
	 * @return Collection of type Job
	 */
	public Collection getAllJobs() 
	{
		final String METHOD = "getAllJobs";
		TraceHelper.entering(tracer,METHOD,null);
		PersistenceManager pm = null;
		try {
			PersistenceManagerFactory pmf =
				locator.getPersistenceManagerFactory();
			pm = pmf.getPersistenceManager();
			// @@temp: begin hack for JDO bug in loading of meta data
			Query query = pm.newQuery(SchedulerJobMapDAO.class);
			StringBuffer queryFilter = new StringBuffer();
			queryFilter.append("scenarioName");
			queryFilter.append("==");
			queryFilter.append("\"");
			queryFilter.append(locator.getScenarioName());
			queryFilter.append("\"");
			query.setFilter(queryFilter.toString());
			Collection coll = (Collection) query.execute();
			Iterator iter = coll.iterator();
			ArrayList jobIdList = new ArrayList();
			while (iter.hasNext()) {
				SchedulerJobMapDAO mapdao = (SchedulerJobMapDAO) iter.next();
				jobIdList.add(mapdao.getJobId());
			}
			query.closeAll();
			Scheduler sch = SchedulerFactory.getScheduler();
			Collection anotherColl = null;
			if (jobIdList != null && jobIdList.size() > 0)
				anotherColl = sch.getJobs(jobIdList);
			Collection newColl = getDefaultJobsLimitedByColl(anotherColl);
			TraceHelper.exiting(tracer,METHOD,null);
			if (newColl != null && anotherColl != null) {
				anotherColl.addAll(newColl);
				return anotherColl;
			}
			if (newColl != null && anotherColl == null) {
				return newColl;
			}
			return anotherColl;
		} catch (Exception jdoex) {
			SystemException ex = null;
			if (jdoex instanceof JDOException) {
				// This should be fine for now?
				ErrorMessageBundle bundle = ErrorMessageBundle.getInstance();
				I18nErrorMessage msg =
					bundle.newI18nErrorMessage(SystemException.JDO_ERROR);
				// This should be fine for now?
				ex = new SystemException(msg, jdoex);
			} else {
				ErrorMessageBundle bundle = ErrorMessageBundle.getInstance();
				I18nErrorMessage msg =
					bundle.newI18nErrorMessage(SystemException.INTERNAL_ERROR);
				// This should be fine for now?
				ex = new SystemException(msg, jdoex);
			}
			LogHelper.logError(logger, tracer, ex);
			throw ex;
		} finally {
			// close the pm
			if(pm!=null)
			pm.close();
		}
	}	
}

