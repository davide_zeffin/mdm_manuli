package com.sap.isa.auction.services.b2x.scheduler;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Comparator;
import java.util.Iterator;
import java.util.TreeSet;

import javax.jdo.PersistenceManager;
import javax.jdo.PersistenceManagerFactory;
import javax.jdo.Query;

import com.sap.tc.logging.Location;
import com.sap.isa.auction.bean.AuctionStatusEnum;
import com.sap.isa.auction.bean.AuctionTypeEnum;
import com.sap.isa.auction.bean.Bid;
import com.sap.isa.auction.bean.b2x.PrivateAuction;
import com.sap.isa.auction.bean.b2x.PrivateAuctionBid;
import com.sap.isa.auction.bean.b2x.PrivateAuctionWinner;
import com.sap.isa.auction.bean.b2x.WinnerDeterminationTypeEnum;
import com.sap.isa.auction.businessobject.AuctionManager;
import com.sap.isa.auction.businessobject.BidManager;
import com.sap.isa.auction.businessobject.ContextManager;
import com.sap.isa.auction.businessobject.b2x.InvocationContext;
import com.sap.isa.auction.businessobject.b2x.ProcessBusinessObjectManager;
import com.sap.isa.auction.businessobject.UserRoleTypeEnum;
import com.sap.isa.auction.businessobject.WinnerManager;
import com.sap.isa.auction.businessobject.b2x.AuctionBusinessObjectManager;
import com.sap.isa.auction.businessobject.b2x.DALBeanSynchronizer;
import com.sap.isa.auction.businessobject.b2x.PrivateAuctionMgrImpl;
import com.sap.isa.auction.businessobject.b2x.PrivateAuctionWinnerManager;
import com.sap.isa.auction.businessobject.b2x.jdo.PrivateAuctionDAO;
import com.sap.isa.auction.businessobject.b2x.jdo.PrivateAuctionItemDAO;
import com.sap.isa.auction.businessobject.b2x.jdo.TargetGrpDAO;
import com.sap.isa.auction.businessobject.management.ProcessMetaBusinessObjectManager;
import com.sap.isa.auction.businessobject.user.Seller;
import com.sap.isa.auction.logging.LogHelper;
import com.sap.isa.auction.logging.LogUtil;
import com.sap.isa.auction.services.ServiceLocator;
import com.sap.isa.services.schedulerservice.context.JobContext;
import com.sap.isa.services.schedulerservice.exceptions.JobExecutionException;

/**
 *
Title:        Asset Remarketing
 * Description:
 * Copyright:    Copyright (c) 2002
 * Company:      SAPLabs LLC
 * @author
 * @version 1.0
 */

/**
 * Algorithm goes likes this 

 */
public class WinnerPollTask extends ScheduledTask {

	private static Location tracer = Location.getLocation(WinnerPollTask.class);
	private BidManager bidMgr = null;
	private WinnerManager winnerMgr = null;
	private AuctionManager auctionMgr = null;
	private ServiceLocator locator  = null;
	protected Location getLocation() {
		return tracer;
	}

	private Collection getAuctionsClosed(ServiceLocator locator, String systemId,String client){
		PersistenceManagerFactory pmf =
			locator.getPersistenceManagerFactory();
		PersistenceManager pm = pmf.getPersistenceManager();
		Query query = pm.newQuery(PrivateAuctionDAO.class);
		// @@todo:: should be an object level association filter
		StringBuffer filter = new StringBuffer("");
		filter.append(
			getQueryString(systemId, client));
		filter.append(" && ((");
		filter.append(
			" status == "
				+ AuctionStatusEnum.toInt(AuctionStatusEnum.CLOSED)
				+ " ) && (winnerDeterminationType == " + WinnerDeterminationTypeEnum.INT_AUTOMATIC + "))");
		logger.infoT(
			tracer,
			"Query filter for the auctions is " + filter.toString());
		query.setFilter(filter.toString());
		Collection queryResult = (Collection) query.execute();
		Iterator resultIterator = queryResult.iterator();
		Collection arraylist = new ArrayList();
		while (resultIterator.hasNext()) {
			PrivateAuctionDAO auction = (PrivateAuctionDAO) resultIterator.next();
			Collection coll = auction.getTargetGroups();
			if(coll!=null) {
				Iterator iter = coll.iterator();
				while(iter.hasNext()) {
					TargetGrpDAO tgt = (TargetGrpDAO) iter.next();
					tgt.getAuctionId();
				}
			}
			coll = auction.getAuctionItems();
			if(coll!=null) {
				Iterator iter = coll.iterator();
				while(iter.hasNext()) {
					PrivateAuctionItemDAO item = (PrivateAuctionItemDAO) iter.next();
					item.getAuctionId();
				}
			}
			
			pm.makeTransientAll(auction.getTargetGroups());
			pm.makeTransientAll(auction.getAuctionItems());
			pm.makeTransient(auction);
			arraylist.add(auction);
		}
		// Close the query and connection
		query.closeAll();
		pm.close();
		pm = null;
		return arraylist;
	}
	/**
	 * Returns the sorted history of the bids for the auction from the bid manager
	 * Sorted by the price and by the quantity
	 * @param auctiondao
	 * @return bid history collection
	 */
	private Collection getBidHistory(PrivateAuctionDAO auctiondao){
		PrivateAuction pauction = new PrivateAuction();
		DALBeanSynchronizer.synchAuctionBeanFromDAO(auctiondao,pauction);
		Collection bidHistory = bidMgr.getBidHistory(pauction);
		// Sort the list according to the price and qty and then the time
		TreeSet set  = new TreeSet(new BidComparator());
		set.addAll(bidHistory);	
		return set;
	}
	private class BidComparator implements Comparator {

		/* (non-Javadoc)
		 * @see java.util.Comparator#compare(java.lang.Object, java.lang.Object)
		 */
		public int compare(Object o1, Object o2) {
			Bid bid1 = (Bid)o1;
			Bid bid2 = (Bid)o2;
			if(bid1.getBidAmount().compareTo(bid2.getBidAmount())!=0){
				return -bid1.getBidAmount().compareTo(bid2.getBidAmount());
			}
			if(bid1.getQuantity()!=bid2.getQuantity()){
				return -(bid1.getQuantity() - bid2.getQuantity());
			}
			if(bid1.getBidDate().compareTo(bid2.getBidDate())!=0){
				return -bid1.getBidDate().compareTo(bid2.getBidDate());
			}
			return 0;
		}
	}
	private Collection  determineWinner(PrivateAuctionDAO auction){
		Collection bidHistory = getBidHistory(auction);
		if(bidHistory!=null && !bidHistory.iterator().hasNext())
			return null;
		if(auction.getAuctionType() == AuctionTypeEnum.toInt(AuctionTypeEnum.NORMAL)) {
			Iterator iter = bidHistory.iterator();
			if(iter.hasNext()){
				Bid bid = (Bid)iter.next();
				if(auction.getReservePrice()!=null){
					if(auction.getReservePrice().compareTo(bid.getBidAmount()) > 0 ) {
						//No winner
						return null;
					}
				}
				PrivateAuctionWinner winner = createWinnerFromBid(auction, bid,bid.getQuantity());
				Collection coll = new ArrayList();
				coll.add(winner);
				return coll;
			}
			return null;
		}
		Collection alreadyWinnersList = new ArrayList();
		if(auction.getAuctionType() == AuctionTypeEnum.toInt(AuctionTypeEnum.DUTCH)) {
			BigDecimal winningBid = findLowestBid(bidHistory);
			if(auction.getReservePrice()!=null)
				if(auction.getReservePrice().compareTo(winningBid) > 0 ) {
					//No winner
					return null;
				}
			Iterator iter = bidHistory.iterator();
			int availableQty = auction.getQuantity();
			Collection coll = new ArrayList();
			while(iter.hasNext()){
				if(availableQty <= 0 ) break; 
				int quantity = availableQty;
				Bid bid = (Bid) iter.next();
				if(alreadyWinnersList.contains(bid.getBuyerId())) continue;
				if(bid.getQuantity() < availableQty ){
					quantity = bid.getQuantity();
					availableQty = availableQty - quantity;
				}
				else {
					availableQty = 0;
				}
				PrivateAuctionWinner winner = createWinnerFromBid(auction, bid,quantity);
				coll.add(winner);
				alreadyWinnersList.add(winner.getBuyerId());
			}
			return coll;
		}
		if(auction.getAuctionType() == AuctionTypeEnum.toInt(AuctionTypeEnum.BROKEN_LOT)) {
			Iterator iter = bidHistory.iterator();
			int availableQty = auction.getQuantity();
			Collection coll = new ArrayList();
			while(iter.hasNext()){
				if(availableQty <= 0 ) break; 
				int quantity = availableQty;
				Bid bid = (Bid) iter.next();
				if(alreadyWinnersList.contains(bid.getBuyerId())) continue;
				if(auction.getReservePrice()!=null &&  bid.getBidAmount().compareTo(auction.getReservePrice())< 0) continue;
				if(bid.getQuantity() < availableQty ){
					quantity = bid.getQuantity();
					availableQty = availableQty - quantity;
				}
				else {
					availableQty = 0;
				}
				PrivateAuctionWinner winner = createWinnerFromBid(auction, bid,quantity);
				coll.add(winner);
				alreadyWinnersList.add(winner.getBuyerId());
			}
			alreadyWinnersList.clear();
			alreadyWinnersList = null;
			return coll;
		}
		return null;
	}
	private PrivateAuctionWinner createWinnerFromBid(
		PrivateAuctionDAO auction,
		Bid bid,int availableQty) {
		// This is the winning bid
		PrivateAuctionWinner winner = new PrivateAuctionWinner();
		winner.setAuctionId(auction.getAuctionId());
		winner.setBidAmount(bid.getBidAmount());
		winner.setBidId(bid.getBidId());
		winner.setBuyerId(bid.getBuyerId());
		// TODO setting the userid same as the buyer id
		winner.setUserId(bid.getBuyerId());
		winner.setClient(bid.getClient());
		winner.setSystemId(bid.getSystemId());
		winner.setQuantity(availableQty);
		winner.setCheckedOut(false);
		if(bid instanceof PrivateAuctionBid) {
			winner.setWebShopId(((PrivateAuctionBid)bid).getWebShopId());
			winner.setSoldToParty(((PrivateAuctionBid)bid).getSoldToParty());
		}

		return winner;
	}
	
	private BigDecimal findLowestBid(Collection bidHistory) {
		Iterator iter = bidHistory.iterator();
		BigDecimal bidAmt =  null;
		while(iter.hasNext()) {
			Bid bid = (Bid)iter.next();
			if(bidAmt == null ) {
				bidAmt = bid.getBidAmount();
				continue;
			}
			else if(bid.getBidAmount().compareTo(bidAmt) <0 ){
				bidAmt = bid.getBidAmount();		
			}
		}
		return bidAmt;
	}
	


	private void changeAuctionStatus(PrivateAuctionDAO auction,boolean isWinnerPresent){
		PrivateAuction pauction = new PrivateAuction();
		DALBeanSynchronizer.synchAuctionBeanFromDAO(auction,pauction);
		if(isWinnerPresent){
				auctionMgr.modifyStatus(pauction,AuctionStatusEnum.FINALIZED);
		}
		else {
			auctionMgr.modifyStatus(pauction,AuctionStatusEnum.NOT_FINALIZED);
		}
	}
	
/*
 *  (non-Javadoc)
 * @see com.sap.isa.auction.services.b2x.scheduler.ScheduledTask#runJob(com.sap.isa.services.schedulerservice.context.JobContext, java.lang.String)
 */
	public void runJob(JobContext jc, String scenarioName)
		throws JobExecutionException {
		ProcessMetaBusinessObjectManager metaBom = null;
		AuctionBusinessObjectManager auctionBOM = null;
		PersistenceManager pm = null;
		ProcessBusinessObjectManager pbom = null;
		try {
			logMessage("About to run the winner poll task for the scenario " + scenarioName,jc,INFO);
			// Get the Service Locator based on the scenario Name
			locator = ServiceLocator.getBaseInstance(scenarioName);
			metaBom =
				ProcessMetaBusinessObjectManager.getInstance(scenarioName);
			pbom = (ProcessBusinessObjectManager)metaBom.getBOMbyName(ProcessBusinessObjectManager.PROCESS_BOM);			
			auctionBOM =
				(AuctionBusinessObjectManager) metaBom.getBOM(
					AuctionBusinessObjectManager.class);
			winnerMgr =
				 auctionBOM.createWinnerManager();
			auctionMgr = auctionBOM.createAuctionManager();
			bidMgr = auctionBOM.createBidManager();
			String client = auctionBOM.createSeller().getClient();
			String systemId = auctionBOM.createSeller().getSystemId();
			if (client == null || systemId == null) {
				//Log the error and exit
				logMessage("Client or SystemId is null for the scenario " + scenarioName,jc,INFO);
				return;
			}
			logMessage("About to query for qualifying auctions for which winners needs to be determined",jc,INFO);			
			Collection coll = getAuctionsClosed(locator,systemId,client);
			Iterator iter = coll.iterator();
			if(!iter.hasNext()) {
				logMessage("No qualifying auctions present for which winners needs to be determined",jc,INFO);
			}
			while(iter.hasNext()){
				PrivateAuctionDAO auctionDAO = (PrivateAuctionDAO)iter.next();
				PrivateAuction auction  = new PrivateAuction();
				DALBeanSynchronizer.synchAuctionBeanFromDAO(auctionDAO,auction);
				bindIC(pbom, auctionDAO);
				logMessage("Determining the winners for auction " + auction.getAuctionName(),jc,INFO);
				Collection winners = determineWinner(auctionDAO);
				if(winners == null || !winners.iterator().hasNext()) {
					logMessage("No Winners and hence changing the status of auction " + auction.getAuctionName() + " to NOT Finalized",jc,INFO);					
					changeAuctionStatus(auctionDAO,false);
					ContextManager.unbindContext();					
					continue;
				}
				Iterator winnersIterator = winners.iterator();
//				int totalWonQty = 0;
				while(winnersIterator.hasNext()){
					PrivateAuctionWinner winner = (PrivateAuctionWinner)winnersIterator.next();
					((PrivateAuctionWinnerManager)winnerMgr).createWinner(winner,auction);
					logMessage("Creating the winner "+ winner.getBuyerId() + " for  auction " + auction.getAuctionName() + " with Order " + winner.getOrderId(),jc,INFO);
					logMessage("Triggered email notification to the winner " + winner.getBuyerId(),jc,INFO);
					((PrivateAuctionMgrImpl) auctionMgr)
										.getFriendInterface(
											this.getClass().getName(),
											this.getClass())
										.sendWinnerNotification(auction, winner);					
					logMessage("Finished email notification trigger for the winner " + winner.getBuyerId(),jc,INFO);										
					
					//Converting quantities to int values
//					totalWonQty+=winner.getQuantity().intValue();

				}
//				if(auction.getType() == AuctionTypeEnum.toInt(AuctionTypeEnum.BROKEN_LOT) ||
//					auction.getType() == AuctionTypeEnum.toInt(AuctionTypeEnum.DUTCH)) {
//						if(totalWonQty!=auction.getQuantity()) {
//							// Move the auction to not finalized state
//							changeAuctionStatus(auctionDAO,false);
//						}
//					}
				ContextManager.unbindContext();
			}
			logMessage("About to exit the winner poll task for the scenario " + scenarioName,jc,INFO);		
		}
		catch (Exception ex) {
			jc.getLogRecorder().addMessage(
				"Exception Occurred: "
					+ LogUtil.getMsgPlusStackTraceAsString(ex),
				true);
			LogHelper.logError(logger, tracer, ex);
			throw new JobExecutionException(ex);
		}
		//Do some clean up operations for the boms
		finally {
			locator = null;
			try {
				if (auctionBOM != null) {
					if (auctionMgr != null)
						auctionBOM.releaseAuctionManager(auctionMgr);
					if (bidMgr != null)
						auctionBOM.releaseBidManager(bidMgr);
					if (winnerMgr != null)
						auctionBOM.releaseWinnerManager(winnerMgr);

				}
				if (pm != null) {
					pm.close();
					pm = null;
				}
			} catch (Exception neglect) {
				LogHelper.logWarning(logger, tracer, neglect);
			}
		}
		logMessage("Exiting the winner poll task " + scenarioName,jc,INFO);
	}

	private void bindIC(
		ProcessBusinessObjectManager pbom,
		PrivateAuctionDAO auctionDAO) {
		InvocationContext ic = new InvocationContext(pbom);
		ic.setShopId(auctionDAO.getShopId());
		Seller seller = new Seller(auctionDAO.getSellerId(),null);
		ic.setUser(seller);
		ic.setRoleType(UserRoleTypeEnum.SELLER);
		ContextManager.bindContext(ic);
	}

	/* (non-Javadoc)
	 * @see com.sap.isa.auction.services.b2x.scheduler.ScheduledTask#getPropertyNames()
	 */
	public Collection getPropertyNames() {
		// TODO Auto-generated method stub
		return null;
	}
	public boolean isLockRequiredForClientSystem(){
		return true;
	}

}
