package com.sap.isa.auction.services.b2x.scheduler;

import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;

import java.util.Set;

import com.sap.tc.logging.Category;
import com.sap.tc.logging.Location;
import com.sap.isa.auction.backend.exception.BackendUserExceptionBase;
import com.sap.isa.auction.backend.exception.SAPBackendUserException;
import com.sap.isa.auction.businessobject.b2x.AuctionBusinessObjectManager;
import com.sap.isa.auction.businessobject.b2x.ProcessBusinessObjectManager;
import com.sap.isa.auction.businessobject.management.ProcessMetaBusinessObjectManager;
//import com.sap.isa.auction.config.EmailConfig;
//import com.sap.isa.auction.config.XCMConfigContainer;
import com.sap.isa.auction.exception.InvalidStateException;
import com.sap.isa.auction.logging.CategoryProvider;
import com.sap.isa.auction.logging.LogHelper;
import com.sap.isa.auction.logging.LogUtil;
import com.sap.isa.auction.services.InitializeException;
import com.sap.isa.auction.services.ServiceLocator;
import com.sap.isa.core.xcm.scenario.ScenarioManager;
import com.sap.isa.services.schedulerservice.Task;
import com.sap.isa.services.schedulerservice.context.JobContext;
import com.sap.isa.services.schedulerservice.exceptions.JobExecutionException;

/**
 * All tasks in SellingViaeBay should extend from this task.
 * Intention of this abstract task implementation is to provide the ability to  
 * maintain activities common to all other tasks in the application
 *
 */
public abstract class ScheduledTask implements Task {

	protected static final Category logger =
		CategoryProvider.getSystemCategory();
	protected static Location tracer =
		Location.getLocation(ScheduledTask.class);

	// Mandatory parameter required for the running the scheduler tasks
	// right now
	public static String SCENARIOPARAM = "SCENARIOPARAM";

	protected final short FATAL = 1;
	protected final short INFO = 2;
	protected final short WARNING = 3;
	protected final short ERROR = 4;

	// Lock variables which ensure the locks between the threads
	// having the same metadata
	//wait time currently put at 20 secs
	private static final long WAIT_TIME = 20L * 1000L;

	private static Object lockObject = new Object();

	protected boolean isLockRequiredForWebShop() {
		return false;
	}

	protected boolean isLockRequiredForClientSystem() {
		return false;
	}
	
	public String[] getClientSystemId(String scenarioName)
		throws
			InvalidStateException,
			InitializeException,
			com.sap.isa.core.init.InitializeException {
		ProcessMetaBusinessObjectManager metaBom = null;
		AuctionBusinessObjectManager auctionBOM = null;
		ProcessBusinessObjectManager pbom = null;
		ServiceLocator locator = ServiceLocator.getBaseInstance(scenarioName);
		metaBom = ProcessMetaBusinessObjectManager.getInstance(scenarioName);
		auctionBOM =
			(AuctionBusinessObjectManager) metaBom.getBOM(
				AuctionBusinessObjectManager.class);
		pbom =
			(ProcessBusinessObjectManager) metaBom.getBOM(
				ProcessBusinessObjectManager.class);

		auctionBOM.setProcessBusinessObjectManager(pbom);
		//Fetch the client and system id for the scenario
		String client = auctionBOM.createSeller().getClient();
		String systemId = auctionBOM.createSeller().getSystemId();
		String[] str = new String[2];
		str[0] = client;
		str[1] = systemId;
		return str;
	}

	public String getDBId(String scenarioName)
		throws
			InvalidStateException,
			InitializeException,
			com.sap.isa.core.init.InitializeException {
		ProcessMetaBusinessObjectManager metaBom = null;
		AuctionBusinessObjectManager auctionBOM = null;
		ProcessBusinessObjectManager pbom = null;
		ServiceLocator locator = ServiceLocator.getBaseInstance(scenarioName);
		metaBom = ProcessMetaBusinessObjectManager.getInstance(scenarioName);
		auctionBOM =
			(AuctionBusinessObjectManager) metaBom.getBOM(
				AuctionBusinessObjectManager.class);
		pbom =
			(ProcessBusinessObjectManager) metaBom.getBOM(
				ProcessBusinessObjectManager.class);

		auctionBOM.setProcessBusinessObjectManager(pbom);
		//Fetch the client and system id for the scenario
		String dbId =  "DBID";
//			auctionBOM
//				.getEBayMetaDataManager()
//				.getDatabaseVersionInfo()
//				.getDBIdentifier();
		return dbId;
	}

	/* 
	 * @see com.sap.isa.services.schedulerservice.Task#runJob(com.sap.isa.services.schedulerservice.context.JobContext)
	 */
	public abstract void runJob(JobContext jc, String scenarioName)
		throws JobExecutionException;

	/* 
	 * @see com.sap.isa.services.schedulerservice.Task#run(com.sap.isa.services.schedulerservice.context.JobContext)
	 */
	public void run(JobContext jc) throws JobExecutionException {
		JobExecutionException jobEx = null;
		// No need to do the processing for sap scenarios
		String scenarioName = null;
		String client = null;
		String systemId = null;
		String dbId = null;
		String webShopId = null;
		try {
			scenarioName =
				jc.getJob().getJobPropertiesMap().getString(SCENARIOPARAM);
			//scenarioName = "CRM_XBF_200";				
			if (!ScenarioManager
				.getScenarioManager()
				.isScenario(scenarioName)) {
				throw new Exception(
					"Scenario " + scenarioName + " does not exist");
			}

			/**
			 * Fetching the locks for the required attributes
			 */
			if (isLockRequiredForClientSystem()
				|| isLockRequiredForWebShop()) {
				if (isLockRequiredForClientSystem()) {
					String[] str = getClientSystemId(scenarioName);
					client = str[0];
					systemId = str[1];
					if (client == null || systemId == null)
						throw new Exception(
							"Client/SystemId for "
								+ scenarioName
								+ " cannot be null");

				} 
			}
			dbId = getDBId(scenarioName);
			if (dbId == null)
				throw new Exception(
					"DB Id for " + scenarioName + " cannot be null");

			boolean b = false;
			while (!b
				&& (isLockRequiredForWebShop()
					|| isLockRequiredForClientSystem())) {
				tracer.debugT(
					"Fetching the lock for "
						+ scenarioName
						+ " for "
						+ this.getClass().getName());
				b =
					CommonDataLockFactory.getInstance(
						this.getClass().getName()).getLock(
						client,
						systemId,
						webShopId,
						dbId);
				if (!b) {
					tracer.debugT(
						"Waiting for the lock "
							+ scenarioName
							+ " for "
							+ this.getClass().getName());
					synchronized (lockObject) {
						lockObject.wait(WAIT_TIME);
					}
				}
			}

			logMessage("Running the task for " + scenarioName, jc, INFO);
			runJob(jc, scenarioName);
			// execute runJob method implemented by the extended class
			logMessage("Finished  the task for " + scenarioName, jc, INFO);
		} catch (Exception jEx) {
			logMessage(
				"Error while running the task for " + scenarioName,
				jc,
				ERROR);
			if (!(jEx instanceof JobExecutionException)) {
				jEx = new JobExecutionException(jEx);
				LogHelper.logError(logger, tracer, jEx);
				jc.getLogRecorder().addMessage(
					"Exception Occurred: "
						+ LogUtil.getMsgPlusStackTraceAsString(jEx),
					true);
			}
			notifyViaEmail((JobExecutionException) jEx, scenarioName);
			throw jobEx;
		} finally {
			if (isLockRequiredForWebShop() || isLockRequiredForClientSystem())
				CommonDataLockFactory.getInstance(
					this.getClass().getName()).releaseLock(
					client,
					systemId,
					webShopId,
					dbId);
			synchronized (lockObject) {
				lockObject.notifyAll();
			}
		}
	}

	/* 
	 * @see com.sap.isa.services.schedulerservice.Task#getPropertyNames()
	 */
	public abstract Collection getPropertyNames();

	public boolean notifyViaEmail(
		JobExecutionException jEx,
		String scenarioName) {
			/**
			 * @TODO PB-implement new notification mechanism
			 */
		/*
		try {
			ServiceLocator locator = ServiceLocator.getInstance(scenarioName);
			String emailAddress;
				emailAddress = null;
			if (emailAddress != null) {
				EmailConfig emailConfig =
					XCMConfigContainer
						.getBaseInstance(scenarioName)
						.getEmailConfig();
				Properties smtpProps = emailConfig.getEmailProps();
				String subject =
					"ERROR occured in sellingViaeBay notification ";
				return false;
			}

		} catch (InvalidStateException ex) {
			tracer.debugT(
				logger,
				"Failed to send notification as the admin map not found for running the task");
		} catch (InitializeException ex) {
			tracer.debugT(
				logger,
				"Failed to send notification as the admin map not found for running the task");
		} 
		catch (Exception ex) {
			tracer.debugT(
				logger,
				"Failed to send notification ",
				ex.toString());
		}*/
		return false;
	}

	protected Location getLocation() {
		return tracer;
	}

	protected void logMessage(String message, JobContext jc, short level) {
		jc.getLogRecorder().addMessage(message, true);
		Location tracer = getLocation();
		switch (level) {
			case FATAL :
				logger.fatalT(tracer, message);
				break;
			case INFO :
				logger.infoT(tracer, message);
				break;
			case WARNING :
				logger.warningT(tracer, message);
				break;
			case ERROR :
				logger.errorT(tracer, message);
				break;
		}
	}

	/**
	 * Returns query string to retrieve all auctions under the current scenario
	 * @param systemId CRM/R3 system id
	 * @param clienId  CRM/R3 client id
	 * @param siteId ebay site id
	 * @return
	 */
	protected String getQueryString(
		String systemId,
		String clienId) {
		// @@todo:: should be an object level association filter
		StringBuffer filter = new StringBuffer("");
		filter.append("systemId == \"");
		filter.append(systemId);
		filter.append("\" && client == \"");
		filter.append(clienId);
		filter.append("\"");
		return filter.toString();
	}
	/**
	 * This class is used to obtain the locks on the row corresponding 
	 * to the clientId, systemId and siteId which is the common data across 
	 * the tasks. 
	 *
	 */
	protected static class CommonDataLockFactory {
		private HashMap webShopIdLockMap = new HashMap();
		private HashMap clientSystemIdsLockMap = new HashMap();
		private static HashMap scenarioNames2DLFs = new HashMap();
		String className = null;
		public static synchronized CommonDataLockFactory getInstance(String className) {
			CommonDataLockFactory cdf =
				(CommonDataLockFactory) scenarioNames2DLFs.get(className);
			if (cdf == null) {
				cdf = new CommonDataLockFactory();
				cdf.className = className;
				scenarioNames2DLFs.put(className, cdf);
			}
			return cdf;
		}

		public synchronized boolean getLock(
			String clientId,
			String systemId,
			String webShopId,
			String dbId) {
			Set webShopIdSet = (Set) webShopIdLockMap.get(dbId);
			if (webShopIdSet == null) {
				webShopIdSet = new HashSet();
				webShopIdLockMap.put(dbId, webShopIdSet);
			}
			Set clientSystemIdsLockSet = (Set) clientSystemIdsLockMap.get(dbId);
			if (clientSystemIdsLockSet == null) {
				clientSystemIdsLockSet = new HashSet();
				clientSystemIdsLockMap.put(dbId, webShopIdSet);
			}

			if (clientId != null && systemId != null && webShopId != null) {
				// check whethere is any lock exists
				if (clientSystemIdsLockSet.contains(systemId + clientId)
					|| webShopIdSet.contains(webShopId)) {
					// There is a lock. Need to wait
					return false;
				} else {
					clientSystemIdsLockSet.add(systemId + clientId);
					webShopIdSet.add("" + webShopId);
					return true;
				}
			}

			if (clientId != null && systemId != null) {
				// check whethere is any lock exists
				if (clientSystemIdsLockSet.contains(systemId + clientId)) {
					// There is a lock. Need to wait
					return false;
				} else {
					clientSystemIdsLockSet.add(systemId + clientId);
					return true;
				}
			}

			if (webShopId != null) {
				// check whethere is any lock exists
				if (webShopIdSet.contains(webShopId)) {
					// There is a lock. Need to wait
					return false;
				} else {
					webShopIdSet.add(webShopId);
					return true;
				}
			}
			return false;
		}
		/**
		 * Releasing the lock for the common Data
		 * @param clientId
		 * @param systemId
		 * @param siteId
		 * @param dbId
		 */
		public synchronized void releaseLock(
			String clientId,
			String systemId,
			String  webShopId,
			String dbId) {
			Set webShopIdSet = (Set) webShopIdLockMap.get(dbId);
			Set clientSystemIdsLockSet = (Set) clientSystemIdsLockMap.get(dbId);
			if (webShopIdSet != null && webShopId != null) {
				webShopIdSet.remove(webShopId);
			}
			if (clientSystemIdsLockSet != null
				&& clientId != null
				&& systemId != null) {
				clientSystemIdsLockSet.remove(systemId + clientId);
			}
		}
	}



	
	protected void logBackendErrors(SAPBackendUserException ex,JobContext jc){
		logMessage("Warning messages from backend",jc,ERROR);
		List list = ((BackendUserExceptionBase)ex).getWarningMessages();
		for(int i = 0 ; list!=null && i < list.size();i++){
			logMessage((String)list.get(i),jc,ERROR);				
		}
		logMessage("Error messages from backend",jc,ERROR);
		list = ((BackendUserExceptionBase)ex).getErrorMessages();
		for(int i = 0 ; list!=null && i < list.size();i++){
			logMessage((String)list.get(i),jc,ERROR);				
		}
	}
}
