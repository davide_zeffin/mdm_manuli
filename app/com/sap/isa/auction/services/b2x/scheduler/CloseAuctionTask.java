package com.sap.isa.auction.services.b2x.scheduler;

import java.util.Collection;
import java.util.Date;
import java.util.Iterator;

import com.sap.tc.logging.Location;
import com.sap.isa.auction.bean.Auction;
import com.sap.isa.auction.bean.AuctionStatusEnum;
import com.sap.isa.auction.businessobject.AuctionManager;
import com.sap.isa.auction.businessobject.ContextManager;
import com.sap.isa.auction.businessobject.b2x.InvocationContext;
import com.sap.isa.auction.businessobject.b2x.ProcessBusinessObjectManager;
import com.sap.isa.auction.businessobject.b2x.AuctionBusinessObjectManager;
import com.sap.isa.auction.businessobject.b2x.PrivateAuctionMgrImpl;
import com.sap.isa.auction.businessobject.management.ProcessMetaBusinessObjectManager;
import com.sap.isa.auction.businessobject.user.Seller;
import com.sap.isa.auction.logging.LogHelper;
import com.sap.isa.auction.logging.LogUtil;
import com.sap.isa.auction.services.InitializeException;
import com.sap.isa.services.schedulerservice.context.JobContext;
import com.sap.isa.services.schedulerservice.exceptions.JobExecutionException;

/**
 * Title:			Internet Sales Private Auctions
 * Description:
 * @Copyright: 		Copyright (c) 2004
 * Company:			SAPLabs LLC
 * @author 
 * Created on: 		May 3, 2004
 */
public class CloseAuctionTask extends ScheduledTask {
	private static final Location tracer =
		Location.getLocation(CloseAuctionTask.class);

	protected Location getLocation() {
		return tracer;
	}
	/* (non-Javadoc)
	 * @see com.sap.isa.auction.services.b2x.scheduler.ScheduledTask#runJob(com.sap.isa.services.schedulerservice.context.JobContext, java.lang.String)
	 */
	public void runJob(JobContext jc, String scenarioName)
		throws JobExecutionException {
		AuctionBusinessObjectManager auctionBOM = null;
		ProcessMetaBusinessObjectManager metaBom = null;
		AuctionManager auctionMgr = null;
		Exception closeAucEx = null;
		try {
			String msg =
				"Entering the CloseAuctionTask for scenario " + scenarioName;
			logMessage(msg, jc, INFO);
			metaBom =
				ProcessMetaBusinessObjectManager.getInstance(scenarioName);
			ProcessBusinessObjectManager pbom =
				(ProcessBusinessObjectManager) metaBom.getBOM(
					ProcessBusinessObjectManager.class);
			auctionBOM =
				(AuctionBusinessObjectManager) metaBom.getBOM(
					AuctionBusinessObjectManager.class);
			auctionBOM.setProcessBusinessObjectManager(pbom);
			auctionMgr = (AuctionManager) auctionBOM.createAuctionManager();
			initSetting(auctionBOM, pbom);
			Date now = new Date();
			logMessage(
				"Querying for auctions whose endDate >= " + now,
				jc,
				INFO);
			Collection coll =
				((PrivateAuctionMgrImpl) auctionMgr)
					.getFriendInterface(
						this.getClass().getName(),
						this.getClass())
					.getAuctionsToBeClosed(now);
			if (coll == null || coll.size() < 1) {
				msg =
					"No auctions found to be closed. Hence exiting from the publish auction task";
				logMessage(msg, jc, INFO);
				return;
			}
			Iterator iter = coll.iterator();
			while (iter.hasNext()) {
				Auction auction = (Auction) iter.next();

				msg =
					"About to close the auction for "
						+ auction.getAuctionName();
				logMessage(msg, jc, INFO);

				try {
					auctionMgr.modifyStatus(auction, AuctionStatusEnum.CLOSED);

				} catch (Exception apiEx) {
					LogHelper.logError(
						logger,
						tracer,
						"Close of auction "
							+ auction.getAuctionName()
							+ " failed",
						apiEx);
					msg =
						"Error messages from auction engine while closing the auction "
							+ auction.getAuctionName();
					logMessage(msg, jc, ERROR);
					LogHelper.logError(
						logger,
						tracer,
						"Auction engine exception while closing the auction "
							+ auction.getAuctionName(),
						apiEx);
					closeAucEx = apiEx;
					continue;
				}
				msg = "Closed the auction " + auction.getAuctionName();
				logMessage(msg, jc, INFO);
			}
			if (closeAucEx != null) {
				throw closeAucEx;
			}
			msg =
				"Exiting the CloseAuctionTask for the scenario " + scenarioName;
			logMessage(msg, jc, INFO);
		} catch (InitializeException ie) {
			logger.errorT(
				tracer,
				"An initialize exception has occurred in the publish auction task");
			LogHelper.logError(logger, tracer, ie);
			jc.getLogRecorder().addMessage(
				LogUtil.getMsgPlusStackTraceAsString(ie),
				true);
			throw new JobExecutionException(ie);
		} catch (Exception ex) {
			logger.errorT(
				tracer,
				"An exception has occurred in the publish auction task");
			jc.getLogRecorder().addMessage(
				LogUtil.getMsgPlusStackTraceAsString(ex),
				true);
			LogHelper.logError(logger, tracer, ex);
			throw new JobExecutionException(ex);
		} finally {
			ContextManager.unbindContext();
			try {
				if (auctionBOM != null) {
					if (auctionMgr != null)
						auctionBOM.releaseAuctionManager(auctionMgr);
				}
			} catch (Exception neglect) {
				LogHelper.logError(logger, tracer, neglect);
			}
		}
	}

	/* (non-Javadoc)
	 * @see com.sap.isa.services.schedulerservice.Task#getPropertyNames()
	 */
	public Collection getPropertyNames() {
		return null;
	}

	protected boolean isLockRequiredForClientSystem() {
		return true;
	}
	private void initSetting(AuctionBusinessObjectManager auctionBOM, ProcessBusinessObjectManager pbom) {
		Seller seller = auctionBOM.createSeller();
		InvocationContext invContext = (InvocationContext)ContextManager.getContext();
		if (invContext == null) {
			invContext = new InvocationContext(pbom);
			ContextManager.bindContext(invContext);
			invContext.setUser(seller);
		}
		ContextManager.bindContext(invContext);
	}
}
