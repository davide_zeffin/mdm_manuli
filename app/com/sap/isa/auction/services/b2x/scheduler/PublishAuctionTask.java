package com.sap.isa.auction.services.b2x.scheduler;

import java.util.Collection;
import java.util.Date;
import java.util.Iterator;

import com.sap.tc.logging.Location;
import com.sap.isa.auction.bean.Auction;
import com.sap.isa.auction.bean.AuctionStatusEnum;
import com.sap.isa.auction.businessobject.AuctionManager;
import com.sap.isa.auction.businessobject.ContextManager;
import com.sap.isa.auction.businessobject.b2x.InvocationContext;
import com.sap.isa.auction.businessobject.b2x.ProcessBusinessObjectManager;
import com.sap.isa.auction.businessobject.b2x.AuctionBusinessObjectManager;
import com.sap.isa.auction.businessobject.b2x.PrivateAuctionMgrImpl;
import com.sap.isa.auction.businessobject.management.ProcessMetaBusinessObjectManager;
import com.sap.isa.auction.businessobject.user.Seller;
import com.sap.isa.auction.logging.LogHelper;
import com.sap.isa.auction.logging.LogUtil;
import com.sap.isa.core.init.InitializeException;
import com.sap.isa.services.schedulerservice.context.JobContext;
import com.sap.isa.services.schedulerservice.exceptions.JobExecutionException;
/**
 * Title:			Internet Sales Private Auctions
 * Description:
 * @Copyright: 		Copyright (c) 2004
 * Company:			SAPLabs LLC
 * @author 
 * Created on: 		Apr 30, 2004
 */

/**
 * This class will change the status of all the saved auctions in
 * the auction engine to be active
 * The pseudo code goes like this
 * 1. Check against the database what are scheduled to be actice
 * 2. Get all those auctions and try to activate them with required preprocessing like quotation creation
 * 3. Log all errors to the auction table so that the admin or other seller can view the same
 * 4. Decision points are whether to retry the posting process again or just log the error
 * to the admin so that the admin himself can take care of it.  
 */
public class PublishAuctionTask extends ScheduledTask {

	private static final Location tracer =
		Location.getLocation(PublishAuctionTask.class);

	protected Location getLocation() {
		return tracer;
	}
	/**
	 * Essentially the user codes all his required scheduled activity in this method
	 */
	public void runJob(JobContext jc, String scenarioName)
		throws JobExecutionException {
		AuctionBusinessObjectManager auctionBOM = null;
		AuctionManager auctionMgr = null;
		ProcessMetaBusinessObjectManager metaBom = null;
		/**
		 * @todo to initialize this
		 */
		Exception publishAucEx = null;
		try {
			String msg =
				"Entering the publish auction task for the scenario "
					+ scenarioName;
			logMessage(msg, jc, INFO);
			metaBom =
				ProcessMetaBusinessObjectManager.getInstance(scenarioName);
			auctionBOM =
				(AuctionBusinessObjectManager) metaBom.getBOM(
					AuctionBusinessObjectManager.class);
			ProcessBusinessObjectManager pbom =
				(ProcessBusinessObjectManager) metaBom.getBOM(
					ProcessBusinessObjectManager.class);
			auctionBOM.setProcessBusinessObjectManager(pbom);
			auctionMgr = (AuctionManager) auctionBOM.createAuctionManager();
			initSetting(auctionBOM, pbom);
			Date now = new Date();
			logMessage(
				"Querying for auctions whose scheduleDate < " + now,
				jc,
				INFO);
			Collection coll =
				((PrivateAuctionMgrImpl) auctionMgr)
					.getFriendInterface(
						this.getClass().getName(),
						this.getClass())
					.getAuctionsToBePublished(now);
			if (coll == null || coll.size() < 1) {
				msg =
					"No auctions found to be published. Hence exiting from the publish auction task";
				logMessage(msg, jc, INFO);
				return;
			}
			Iterator iter = coll.iterator();
			while (iter.hasNext()) {
				Auction auction = (Auction) iter.next();
				
				msg =
					"About to publish the auction for "
						+ auction.getAuctionName();
				logMessage(msg, jc, INFO);

				try {
					auctionMgr.modifyStatus(
						auction,
						AuctionStatusEnum.PUBLISHED);
				
					} catch (Exception apiEx) {
						LogHelper.logError(
							logger,
							tracer,
							"Publish of auction "
								+ auction.getAuctionName()
								+ " failed",
							apiEx);
						msg =
							"Error messages from auction engine while publishing the auction "
								+ auction.getAuctionName();
						logMessage(msg, jc, ERROR);
						LogHelper.logError(
							logger,
							tracer,
							"Auction engine exception while publishing the auction "
								+ auction.getAuctionName(),
							apiEx);
						publishAucEx = apiEx;
						continue;
					}
					msg = "Published the auction " + auction.getAuctionName();
					logMessage(msg, jc, INFO);
			}
			if (publishAucEx != null) {
				throw publishAucEx;
			}
			msg =
				"Exiting the publish auction task for the scenario "
					+ scenarioName;
			logMessage(msg, jc, INFO);
		} catch (InitializeException ie) {
			logger.errorT(
				tracer,
				"An initialize exception has occurred in the publish auction task");
			LogHelper.logError(logger, tracer, ie);
			jc.getLogRecorder().addMessage(
				LogUtil.getMsgPlusStackTraceAsString(ie),
				true);
			throw new JobExecutionException(ie);
		} catch (Exception ex) {
			logger.errorT(
				tracer,
				"An exception has occurred in the publish auction task");
			jc.getLogRecorder().addMessage(
				LogUtil.getMsgPlusStackTraceAsString(ex),
				true);
			LogHelper.logError(logger, tracer, ex);
			throw new JobExecutionException(ex);
		}
		//Do some clean up operations for the boms
		finally {
			ContextManager.unbindContext();
			try {
				if (auctionBOM != null) {
					if (auctionMgr != null)
						auctionBOM.releaseAuctionManager(auctionMgr);
				}
			} catch (Exception neglect) {
				LogHelper.logError(logger, tracer, neglect);
			}
		}
	}
	/**
	 * This method will give the property names for the particular job used at the execution
	 * of the job.
	 */
	public Collection getPropertyNames() {
		return null;
	}

	protected boolean isLockRequiredForClientSystem() {
		return true;
	}

	private void initSetting(AuctionBusinessObjectManager auctionBOM, ProcessBusinessObjectManager pbom) {
		Seller seller = auctionBOM.createSeller();
		InvocationContext invContext =(InvocationContext) ContextManager.getContext();
		if (invContext == null) {
			invContext = new InvocationContext(pbom);
			ContextManager.bindContext(invContext);
			invContext.setUser(seller);
		}
		ContextManager.bindContext(invContext);
	}
}
