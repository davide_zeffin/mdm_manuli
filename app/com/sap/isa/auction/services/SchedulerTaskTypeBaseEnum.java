/*
 * Created on Apr 19, 2004
 *
 * Title:        Internet Sales Asset Remarketing
 * Description:
 * Copyright:    Copyright (c) 2004
 * Company:      SAP Labs LLC
 * @author
 * @version 1.0
 */
package com.sap.isa.auction.services;

import com.sap.isa.services.schedulerservice.Job;

/**
 * @author I802850
 *
 * To change the template for this generated type comment go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
public class SchedulerTaskTypeBaseEnum {

	public static final String STR_WINNER_TASK = "WinnerPollTask";
	public static final String STR_BID_TASK = "BidPollTask";
	public static final String STR_PUBLISH_AUCTION_TASK = "PublishAuctionTask";
	public static final String STR_AUC_STATUS_UPD_WO_WINNER_TASK = "UpdateAuctionStatusWithoutWinnerTask";
	public static final String STR_CLOSE_AUCTION_TASK = "CloseAuctionTask";
	
	
	// Job Type can be one of the following constants
	private static final int INT_WINNER_TASK = 1;
	private static final int INT_BID_TASK = 2;
	private static final int INT_PUBLISH_AUCTION_TASK = 6;
	private static final int INT_CLOSE_AUCTION_TASK = 7;
	private static final int INT_AUC_STATUS_UPD_WO_WINNER_TASK = 11;
	
	
	public static final SchedulerTaskTypeBaseEnum WINNER_TASK = new SchedulerTaskTypeBaseEnum(STR_WINNER_TASK,INT_WINNER_TASK);
	public static final SchedulerTaskTypeBaseEnum BID_TASK = new SchedulerTaskTypeBaseEnum(STR_BID_TASK,INT_BID_TASK);
	public static final SchedulerTaskTypeBaseEnum PUBLISH_AUCTION_TASK = new SchedulerTaskTypeBaseEnum(STR_PUBLISH_AUCTION_TASK,INT_PUBLISH_AUCTION_TASK);
	public static final SchedulerTaskTypeBaseEnum CLOSE_AUCTION_TASK = new SchedulerTaskTypeBaseEnum(STR_CLOSE_AUCTION_TASK,INT_CLOSE_AUCTION_TASK);
	public static final SchedulerTaskTypeBaseEnum AUC_STATUS_UPD_WO_WINNER_TASK = new SchedulerTaskTypeBaseEnum(STR_AUC_STATUS_UPD_WO_WINNER_TASK,INT_AUC_STATUS_UPD_WO_WINNER_TASK);
	private String name;
	private int value;
	
	protected SchedulerTaskTypeBaseEnum(String name, int value) {
		this.name = name;
		this.value = value;
	}
	
	public int getValue() {
		return value;
	}
	
	public String toString() {
		return "[" + name + " = " + value + "]";
	}
	
	public static final int toInt(SchedulerTaskTypeBaseEnum enum) {
		return enum.getValue();
	}
	
	public static SchedulerTaskTypeBaseEnum toEnum(int value) {
		switch(value) {
			case INT_BID_TASK : return BID_TASK;
			case INT_PUBLISH_AUCTION_TASK: return PUBLISH_AUCTION_TASK;
			case INT_CLOSE_AUCTION_TASK: return CLOSE_AUCTION_TASK;
			case INT_WINNER_TASK: return WINNER_TASK;
			case INT_AUC_STATUS_UPD_WO_WINNER_TASK: return AUC_STATUS_UPD_WO_WINNER_TASK;
			default: throw new IllegalArgumentException("Enum = " + value + " is invalid");
		}
	}
	public static SchedulerTaskTypeBaseEnum toEnum(String value) {
	
			if(STR_BID_TASK.equals(value))
				return BID_TASK;
		
			if(STR_PUBLISH_AUCTION_TASK.equals(value))
				return PUBLISH_AUCTION_TASK;
				
			if(STR_CLOSE_AUCTION_TASK.equals(value))
				return CLOSE_AUCTION_TASK;
	
			if(STR_WINNER_TASK.equals(value))
				return WINNER_TASK;
	
			if(STR_AUC_STATUS_UPD_WO_WINNER_TASK.equals(value))
				return AUC_STATUS_UPD_WO_WINNER_TASK;
	
			else throw new IllegalArgumentException("Enum = " + value + " is invalid");
		}

	protected Integer getJobType(Job job) {
		SchedulerTaskTypeBaseEnum type = SchedulerTaskTypeBaseEnum.toEnum(job.getName());
		if(type != null)
			return new Integer(type.getValue());
		else
			return null; 
	}	
   
}
