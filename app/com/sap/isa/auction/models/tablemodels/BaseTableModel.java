package com.sap.isa.auction.models.tablemodels;

import java.util.Collection;

import com.sap.tc.logging.Category;
import com.sap.tc.logging.Location;
import com.sap.isa.auction.helpers.LogSupport;
import com.sap.isa.auction.logging.CategoryProvider;
import com.sap.isa.ui.components.table.TableModel;

/**
 * Title:
 * Description:
 * Copyright:    Copyright (c) 2003
 * Company:
 * @author
 * @version 1.0
 */

public class BaseTableModel extends TableModel  {

	protected static Category cat = CategoryProvider.getUICategory();
	protected static Location location;
	public static final String SELECTION = "SELECTION";
	
	protected Collection dataColl;
	protected Object[] dataArray;

	public BaseTableModel() {
		location = Location.getLocation(this.getClass());
	}

	protected void entering(String text)  {
		location = Location.getLocation(this.getClass());
		LogSupport.entering(cat , location , text , null);
	}

	protected void exiting(String text)  {
		location = Location.getLocation(this.getClass());
		LogSupport.exiting(cat , location , text , null);
	}

	protected void logInfo(String text)  {
		location = Location.getLocation(this.getClass());
		LogSupport.logInfo(cat , location , text);
	}

	protected void logInfo(String text , Throwable exc)  {
		location = Location.getLocation(this.getClass());
		LogSupport.logError(cat , location , text , exc);
	}

    public int getRowCount()  {
    	int count = 0;
    	if(null != dataColl)  {
    		count = dataColl.size();
    	}
    	return count;
    }
    
    public void clean()  {
    	if(dataColl != null)  {
			dataColl.clear();
			dataColl = null;
    	}
    	if(dataArray != null)  {
			dataArray = null;
    	}
    }

}