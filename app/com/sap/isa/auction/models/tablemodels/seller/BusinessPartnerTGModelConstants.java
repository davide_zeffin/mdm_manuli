/*
 * Created on Apr 30, 2004
 *
 * To change the template for this generated file go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
package com.sap.isa.auction.models.tablemodels.seller;

/**
 * @author I803067
 *
 * To change the template for this generated type comment go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
public interface BusinessPartnerTGModelConstants {
	public static final String FIRSTNAMETITLE = "BP.FirstName";
	public static final String LASTNAMETITLE = "BP.LastName";
	public static final String PARTNERIDTITLE = "BP.PartnerID";
	public static final String REGIONTITLE = "BP.Region";
	public static final String COUNTRYTITLE = "BP.Country";
	public static final String ORGANISATIONTITLE = "BP.Organisation";
	public static final String POCODETITLE = "BP.PoCode";
	public static final String CITYTITLE = "BP.City";

	public static final String FIRSTNAME = "FIRSTNAME.BusinessPartner";
	public static final String LASTNAME = "LASTNAME.BusinessPartner";
	public static final String PARTNERID = "PARTNERID.BusinessPartner";
	public static final String REGION = "REGION.BusinessPartner";
	public static final String COUNTRY = "COUNTRY.BusinessPartner";
	public static final String ORGANISATION = "ORGANISATION.BusinessPartner";
	public static final String POCODE = "POCODE.BusinessPartner";
	public static final String IMAGE = "IMAGE.BusinessPartner";
	public static final String CITY = "CITY.BusinessPartner";

	public static final String INVALIDBP = "InvalidBP.BusinessPartner";
	public static final String INVALIDTG = "InvalidTG.TargetGroup";
}
