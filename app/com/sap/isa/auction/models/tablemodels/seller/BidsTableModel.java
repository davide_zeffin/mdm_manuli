package com.sap.isa.auction.models.tablemodels.seller;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;

import javax.servlet.http.HttpSession;

import com.sap.isa.auction.bean.b2x.PrivateAuctionBid;
import com.sap.isa.auction.helpers.ConversionHelper;
import com.sap.isa.auction.helpers.HelpManager;
import com.sap.isa.auction.helpers.TargetGroupBPHelper;
import com.sap.isa.auction.models.tablemodels.BaseTableModel;
import com.sap.isa.businesspartner.businessobject.BusinessPartner;
import com.sap.isa.core.UserSessionData;
import com.sap.isa.ui.components.DataComponent;
import com.sap.isa.ui.controllers.ActionForm;
import com.sapportals.htmlb.DropdownListBox;
import com.sapportals.htmlb.enum.TableColumnType;
import com.sapportals.htmlb.table.TableColumn;
import com.sapportals.htmlb.type.AbstractDataType;
import com.sapportals.htmlb.type.DataString;

/**
 * Title:			Internet Sales Private Auctions
 * Description:
 * @Copyright: 		Copyright (c) 2004
 * Company:			SAPLabs LLC
 * @author 
 * Created on: 		May 5, 2004
 */
public class BidsTableModel
	extends BaseTableModel
	implements BidsTableConstants {

	private String reference;
	private String target;
	private HttpSession session;
	private ConversionHelper conversionHelper;
	private TargetGroupBPHelper bpHelper;
	private ActionForm actionForm;

	public BidsTableModel(HttpSession session) {
		this.session = session;
	}

	public void initColumns() {

		super.initColumns();

		TableColumn column =
			new TableColumn(
				this,
				TBLCOLID_LSTNAME,
				BusinessPartnerTGModelConstants.LASTNAMETITLE);
		addColumn(column);

		column =
			new TableColumn(
				this,
				TBLCOLID_FSTNAME,
				BusinessPartnerTGModelConstants.FIRSTNAMETITLE);
		addColumn(column);

		column =
			new TableColumn(
				this,
				TBLCOLID_BIDAMT,
				WinnersTableConstants.TBLCOL_BIDAMT);
		addColumn(column);

		column = new TableColumn(this, TBLCOLID_BIDTIME, TBLCOL_BIDTIME);
		addColumn(column);

		column =
			new TableColumn(this, TBLCOLID_BIDQUANTITY, TBLCOL_BIDQUANTITY);
		addColumn(column);
	}

	public void setReferenceTarget(String referencePath, String target) {
		this.reference = referencePath;
		this.target = target;
	}

	public void setData(Collection data) {
		final String METHOD = "setData";
		entering(METHOD);
		if (null == data) {
			dataColl = new ArrayList();
		} else {
			dataColl = data;
		}
		dataArray = dataColl.toArray();
		setVisibleFirstRow(1);
		exiting(METHOD);
	}

	public Object getValueAt(int row) {
		final String METHOD = "getValueAt";
		entering(METHOD);
		logInfo("selected rowindex:" + Integer.toString(row));
		
		Object object = null;
		if (dataArray != null) {
			object = dataArray[row - 1];
		}
		
		exiting(METHOD);
		return object;
	}

	public AbstractDataType getValueAt(int row, String columnName) {
		final String METHOD = "getValueAt";
		entering(METHOD);
		PrivateAuctionBid bid = null;
		logInfo(
			"get object at the row: " + row + " , column name: " + columnName);
		if (null == dataArray) {
			logInfo("data collection is null, return");
			return new DataString("");
		}
		try {
			bid = (PrivateAuctionBid) dataArray[row - 1];

		} catch (Exception exc) {
			//return new DataString("");
			logInfo("Error in getValueAt ", exc);
		}
		if (columnName.equals(TBLCOLID_FSTNAME)) {
			BusinessPartner bp = getBusinessPartner(bid.getBuyerId());
			String fstName = "-";
			if (bp != null && bp.getAddress() != null)
				fstName =
					bp.getAddress().getFirstName() != null
						? bp.getAddress().getFirstName()
						: "-";
			exiting(METHOD);
			return new DataString(fstName);
		} else if (columnName.equals(TBLCOLID_LSTNAME)) {
			String bpId = bid.getBuyerId();
			BusinessPartner bp = getBusinessPartner(bpId);
			String lstName = "-";
			if (bp != null && bp.getAddress() != null)
				lstName =
					bp.getAddress().getLastName() != null
						? bp.getAddress().getLastName()
						: "-";
			//			Link link = new Link(Integer.toString(row), lstName);
			//			link.setReference(
			//				reference + "?" + PARAM_BIDDERID + "=" + bpId);
			//			link.setTarget(target);
			//			DataComponent dataCompont = new DataComponent(link);
			exiting(METHOD);
			return new DataString(lstName);
		} else if (columnName.equals(TBLCOLID_BIDAMT)) {
			String bidAmount =
				getConversionHelper().bigDecimalToUICurrencyString(
					bid.getBidAmount(),
					false);
			exiting(METHOD);
			return new DataString(bidAmount);
		} else if (columnName.equals(TBLCOLID_BIDTIME)) {
			String bidTime =
				getConversionHelper().javaDatetoUILocaleSpecificString(
					bid.getBidDate());
			exiting(METHOD);
			return new DataString(bidTime);
		} else if (columnName.equals(TBLCOLID_BIDQUANTITY)) {
			TableColumnType columnType = getColumn(TBLCOLID_BIDQUANTITY).
					getType();
			if ("TEXT".equals(columnType.getStringValue())){ 
				BigDecimal qty = new BigDecimal((double) bid.getQuantity());
				String qtyStr =
					getConversionHelper().bigDecimalToUIQuantityString(qty);
				exiting(METHOD);
				return new DataString(qtyStr);			
			} else if ("USER".equals(columnType.getStringValue())){
				String listBoxId = "quantity";
				DropdownListBox listBox = createQuantityListBox(listBoxId, 1, 
						bid.getQuantity());	
				exiting(METHOD);
				return new DataComponent(listBox);			
			}
		}
		exiting(METHOD);
		return new DataString("");
	}

	protected BusinessPartner getBusinessPartner(String id) {
		BusinessPartner bp = getTargetGroupBPHelper().getBusinessPartner(id);
		return bp;
	}

	protected ConversionHelper getConversionHelper() {
		if (null == conversionHelper) {
			UserSessionData userSession =
				UserSessionData.getUserSessionData(session);
			HelpManager manager =
				(HelpManager) userSession.getAttribute(HelpManager.HELPMANAGER);
			conversionHelper =
				(ConversionHelper) manager.getHelper(
					HelpManager.CONVERSIONHELPER);
		}
		return conversionHelper;
	}

	private TargetGroupBPHelper getTargetGroupBPHelper() {
		if (null == bpHelper) {
			UserSessionData userSession =
				UserSessionData.getUserSessionData(session);
			HelpManager helpMang =
				(HelpManager) userSession.getAttribute(HelpManager.HELPMANAGER);
			bpHelper =
				(TargetGroupBPHelper) helpMang.getHelper(
					HelpManager.TARGETGROUPHELPER);
		}
		return bpHelper;
	}
	
	/**
	 * 
	 * @param lowerBound
	 * @param upperBound
	 * @return
	 */
	private DropdownListBox createQuantityListBox(String listBoxId, int lowerBound, 
			int upperBound){
		DropdownListBox listBox = new DropdownListBox(listBoxId);
		for (int x = lowerBound; x <= upperBound; x++){
			listBox.addItem(String.valueOf(x), String.valueOf(x));			
		}
		listBox.setSelection(String.valueOf(upperBound));
		return listBox;
	}

	public void setForm(ActionForm actionForm){
		this.actionForm = actionForm;
	}
}
