package com.sap.isa.auction.models.tablemodels.seller;

import java.util.ArrayList;
import java.util.Collection;

import javax.servlet.http.HttpSession;

import com.sap.isa.auction.actionforms.seller.WinnerFormConstants;
import com.sap.isa.auction.bean.Winner;
import com.sap.isa.auction.helpers.ConversionHelper;
import com.sap.isa.auction.helpers.HelpManager;
import com.sap.isa.auction.helpers.TargetGroupBPHelper;
import com.sap.isa.auction.models.tablemodels.BaseTableModel;
import com.sap.isa.businesspartner.businessobject.BusinessPartner;
import com.sap.isa.core.UserSessionData;
import com.sap.isa.ui.components.DataComponent;
import com.sapportals.htmlb.Link;
import com.sapportals.htmlb.table.TableColumn;
import com.sapportals.htmlb.type.AbstractDataType;
import com.sapportals.htmlb.type.DataString;

/**
 * Title:			Internet Sales Private Auctions
 * Description:
 * @Copyright: 		Copyright (c) 2004
 * Company:			SAPLabs LLC
 * @author 
 * Created on: 		May 5, 2004
 */
public class WinnersTableModel
	extends BaseTableModel
	implements WinnersTableConstants {

	protected String reference;
	protected String target;
	protected HttpSession session;
	private ConversionHelper conversionHelper;
	private TargetGroupBPHelper bpHelper;

	public WinnersTableModel(HttpSession session) {
		this.session = session;
	}

	public void initColumns() {

		super.initColumns();

		TableColumn column = new TableColumn(this, BusinessPartnerTGModelConstants.LASTNAMETITLE, TBLCOLID_LSTNAME);
		addColumn(column);

		column = new TableColumn(this, BusinessPartnerTGModelConstants.FIRSTNAMETITLE, TBLCOLID_FSTNAME);
		addColumn(column);

		column = new TableColumn(this, TBLCOL_BIDAMT, TBLCOLID_BIDAMT);
		addColumn(column);

		column = new TableColumn(this, WinnerFormConstants.QUANTITY,TBLCOLID_QUANTITY);
		addColumn(column);
	}

	public void setReferenceTarget(String referencePath, String target) {
		this.reference = referencePath;
		this.target = target;
	}

	public void setData(Collection data) {
		final String METHOD = "setData";
		entering(METHOD);
		if (null == data) {
			dataColl = new ArrayList();
		} else {
			dataColl = data;
		}
		dataArray = dataColl.toArray();
		setVisibleFirstRow(1);
		exiting(METHOD);
	}

	public AbstractDataType getValueAt(int row, String columnName) {
		final String METHOD = "getValueAt";
		entering(METHOD);
		Winner winner = null;
		logInfo(
			"get object at the row: " + row + " , column name: " + columnName);
		if (null == dataArray) {
			logInfo("data collection is null, return");
			return new DataString("");
		}
		try {
			winner = (Winner) dataArray[row - 1];

		} catch (Exception exc) {
			//return new DataString("");
			logInfo("Error occurred: ", exc);
		}
		if (columnName.equals(TBLCOLID_FSTNAME)) {
			BusinessPartner bp = getBusinessPartner(winner.getUserId());
			String firstName = bp.getAddress().getFirstName();
			exiting(METHOD);
			return new DataString(firstName);
		}

		if (columnName.equals(TBLCOLID_LSTNAME)) {
			BusinessPartner bp = getBusinessPartner(winner.getUserId());
			String lastName = bp.getAddress().getLastName();
//			Link link = new Link(Integer.toString(row), lstName);
//			link.setReference(
//				reference + "?" + PARAM_WINNERID + "=" + winner.getWinnerId());
//			link.setTarget(target);
//			DataComponent dataCompont = new DataComponent(link);
			exiting(METHOD);
			return new DataString(lastName);
		} else if (columnName.equals(TBLCOLID_BIDAMT)) {
			String bidAmount =
				getConversionHelper().bigDecimalToUICurrencyString(
					winner.getBidAmount(),
					false);
			exiting(METHOD);
			return new DataString(bidAmount);
		} else if (columnName.equals(TBLCOLID_QUANTITY)){
			String quantity = winner.getQuantity().toString();
			
			exiting(METHOD);
			return new DataString(quantity);
		}
		exiting(METHOD);
		return new DataString("");
	}

	protected BusinessPartner getBusinessPartner(String id) {
		BusinessPartner bp = getTargetGroupBPHelper().getBusinessPartner(id);
		return bp;
	}

	protected ConversionHelper getConversionHelper() {
		if (null == conversionHelper) {
			UserSessionData userSession =
				UserSessionData.getUserSessionData(session);
			HelpManager manager =
				(HelpManager) userSession.getAttribute(HelpManager.HELPMANAGER);
			conversionHelper =
				(ConversionHelper) manager.getHelper(
					HelpManager.CONVERSIONHELPER);
		}
		return conversionHelper;
	}

	private TargetGroupBPHelper getTargetGroupBPHelper()  {
		if(null == bpHelper)  {
			UserSessionData userSession = UserSessionData.getUserSessionData(session);
			HelpManager helpMang = (HelpManager)userSession.getAttribute(HelpManager.HELPMANAGER);
			bpHelper = (TargetGroupBPHelper)helpMang.getHelper(HelpManager.TARGETGROUPHELPER);
		}
		return bpHelper;
	}
}
