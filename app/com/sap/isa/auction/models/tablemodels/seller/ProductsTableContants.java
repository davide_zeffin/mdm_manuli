package com.sap.isa.auction.models.tablemodels.seller;

/**
 * Title:
 * Description:
 * Copyright:    Copyright (c) 2003
 * Company:
 * @author
 * @version 1.0
 */

public interface ProductsTableContants {

    //htmlb component ID
    public static final String ID            = "productId";
    public static final String DESCRIPTION   = "productDescription";
    public static final String NORMALDESCRIPTION = "productDescriptionNormal";
    public static final String QUANTITY      = "quantity";
    public static final String QUANTITYTEXT  = "quantitytext";
    public static final String IMAGE         = "image";
    public static final String QUANTPERAUCT  = "quantPerAuct";
    public static final String UNIT          = "unit";

	public static final String SALESAREA 	= "SalesArea";
	public static final String SALESAREATEXT 	= "PT.SalesArea";

    //I18N text
    public static final String IDTITLE                = "PT.ProductId";
    public static final String DESCRIPTIONTITLE       = "PT.Description";
    public static final String QUANTITYTITLE          = "PT.Quantity";
    public static final String IMAGETITLE             = "PT.ProductImage";
    public static final String UNITTITLE              = "PT.ProductUnit";
	public static final String QUANTITYPERAUCTTITLE	  = "PT.QuantPerAuct";
    public static final String DETAILSTARGET          = "DETAILSTARGET.ProductsTable";
}