package com.sap.isa.auction.models.tablemodels.seller;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Collection;

import javax.servlet.http.HttpSession;

import com.sap.isa.auction.actionforms.BaseForm;
import com.sap.isa.auction.actionforms.seller.PublishQueueForm;
import com.sap.isa.auction.actions.seller.ActionForwards;
import com.sap.isa.auction.bean.AuctionStatusEnum;
import com.sap.isa.auction.bean.b2x.PrivateAuction;
import com.sap.isa.auction.helpers.ConversionHelper;
import com.sap.isa.auction.helpers.HelpManager;
import com.sap.isa.auction.helpers.SearchResult;
import com.sap.isa.auction.models.listmodels.seller.AuctionStatusListConstants;
import com.sap.isa.auction.models.tablemodels.BaseTableModel;
import com.sap.isa.auction.util.FormUtility;
import com.sap.isa.core.UserSessionData;
import com.sap.isa.core.util.WebUtil;
import com.sap.isa.ui.components.DataComponent;
import com.sapportals.htmlb.GridLayout;
import com.sapportals.htmlb.GridLayoutCell;
import com.sapportals.htmlb.Image;
import com.sapportals.htmlb.Link;
import com.sapportals.htmlb.TextView;
import com.sapportals.htmlb.enum.TableColumnType;
import com.sapportals.htmlb.table.TableColumn;
import com.sapportals.htmlb.type.AbstractDataType;
import com.sapportals.htmlb.type.DataString;

/**
 * Title:			Internet Sales Private Auctions
 * Description:
 * @Copyright: 		Copyright (c) 2004
 * Company:			SAPLabs LLC
 * @author
 * Created on:		Apr 27, 2004 
 */

public class AuctionsTableModel
	extends BaseTableModel
	implements AuctionsTableConstants {

	protected HttpSession session;
	private String reference;
	private String target;
	private String contextPath;
	private ConversionHelper helper;
	protected SearchResult result;

	public AuctionsTableModel(HttpSession session, String contextPath) {
		this.session = session;
		this.contextPath = contextPath;
	}

	public void setReferenceTarget(String referencePath, String target) {
		this.reference = referencePath;
		this.target = target;
		logInfo("Asign reference: " + reference + " , target: " + target);
	}

	public void initColumns() {
		TableColumn column =
			new TableColumn(
				this,
				TBLCOLID_NAME,
				TBLCOL_NAME,
				TableColumnType.LINK);
		addActualColumn(column);

		column =
			new TableColumn(
				this,
				TBLCOLID_DESCRIPTION,
				TBLCOL_DESCRIPTION,
				TableColumnType.TEXT);
		addActualColumn(column);

		column =
			new TableColumn(
				this,
				TBLCOLID_PUBLISHDATE,
				TBLCOL_PUBLISHDATE,
				TableColumnType.TEXT);
		addActualColumn(column);

		column =
			new TableColumn(
				this,
				TBLCOLID_CLOSINGDATE,
				TBLCOL_CLOSINGDATE,
				TableColumnType.TEXT);
		addActualColumn(column);

		column =
			new TableColumn(
				this,
				TBLCOLID_STATUS,
				TBLCOL_STATUS,
				TableColumnType.IMAGE);
		addActualColumn(column);

		column =
			new TableColumn(
				this,
				TBLCOLID_ERRORS,
				TBLCOL_ERRORS,
				TableColumnType.LINK);
		addActualColumn(column);

	}

	public void setData(Collection data) {
		final String METHOD = "setData";
		entering(METHOD);
		if (null == data) {
			logInfo("Data collection is null, create a new collection");
			data = new ArrayList();
		}
		dataColl = data;
		dataArray = dataColl.toArray();
		setVisibleFirstRow(1);
		result = null;
		exiting(METHOD);
	}

	public void setData(SearchResult data) {
		final String METHOD = "setData to table model";
		entering(METHOD);
		if (null == data) {
			logInfo("Data collection is null, create a new collection");
			dataArray = null;
			result = null;
			return;
		}
		dataColl = null;
		result = data;
		setVisibleFirstRow(1);
		exiting(METHOD);
	}

	public AbstractDataType getValueAt(int row, String columnName) {
		final String METHOD = "getValueAt";
		entering(METHOD);
		PrivateAuction auction = null;
		logInfo(
			"get object at the row: " + row + " , column name: " + columnName);
		try {
			auction = (PrivateAuction) result.fetch(row);

		} catch (Exception exc) {
			//return new DataString("");
			logInfo("Error occurred ", exc);
		}

		if (columnName.equals(TBLCOLID_NAME)) {
			Link link =
				new Link(Integer.toString(row), auction.getAuctionName());
			link.setOnClientClick(
				getSubareaLink(
					reference
						+ ActionForwards.QUESTIONESC
						+ PARAM_AUCTIONID
						+ ActionForwards.EQUALESC
						+ (row)));
			DataComponent dataCompont = new DataComponent(link);
			exiting(METHOD);
			return dataCompont;
		} else if (columnName.equals(TBLCOLID_DESCRIPTION)) {
			String desc =
				auction.getAuctionTitle() != null
					? auction.getAuctionTitle()
					: "";
			if (desc.length() > 60) {
				desc = desc.substring(0, 60) + "....";
			}

			TextView view = new TextView();
			view.setText(desc);
			DataComponent dataCompont = new DataComponent(view);
			exiting(METHOD);
			return dataCompont;
		} else if (columnName.equals(TBLCOLID_PUBLISHDATE)) {
			String value =
				getConversionHelper().javaDatetoUILocaleSpecificString(
					auction.getStartDate());
			exiting(METHOD);
			return new DataString(value);

		} else if (columnName.equals(TBLCOLID_CLOSINGDATE)) {
			Timestamp endDate = auction.getEndDate();
			String value =
				getConversionHelper().javaDatetoUILocaleSpecificString(endDate);
			exiting(METHOD);
			return new DataString(value);

		} else if (columnName.equals(TBLCOLID_STATUS)) {
			DataComponent component = null;
			String tooltip = "";
			Image img = null;
			int status = auction.getStatus();
			if (status == AuctionStatusEnum.OPEN.getValue()) {
				tooltip =
					FormUtility.getResourceString(
						BaseForm.AUCTIONSELLBUNDLE,
						AuctionStatusListConstants.LSTITM_OPEN,
						session);
				img =
					new Image(
						WebUtil.getMimeURL(
							contextPath,
							null,
							null,
							"/mimes/images/open.gif"),
						tooltip);
				img.setWidth("16");
				img.setHeight("16");
				component = new DataComponent(img);
			} 
			else if (status == AuctionStatusEnum.SCHEDULED.getValue()) {
				tooltip =
					FormUtility.getResourceString(
						BaseForm.AUCTIONSELLBUNDLE,
						AuctionStatusListConstants.LSTITM_SCHEDULED,
						session);
				img =
					new Image(
						WebUtil.getMimeURL(
							contextPath,
							null,
							null,
							"/mimes/images/icon_calendar.gif"),
						tooltip);
				img.setWidth("16");
				img.setHeight("16");
				component = new DataComponent(img);
			} else if (status == AuctionStatusEnum.PUBLISHED.getValue()) {
				tooltip =
					FormUtility.getResourceString(
						BaseForm.AUCTIONSELLBUNDLE,
						AuctionStatusListConstants.LSTITM_PUBLISHED,
						session);
				img =
					new Image(
						WebUtil.getMimeURL(
							contextPath,
							null,
							null,
							"/mimes/images/publish.gif"),
						tooltip);
				img.setWidth("16");
				img.setHeight("16");
				component = new DataComponent(img);
			} else if (status == AuctionStatusEnum.WITHDRAWN.getValue()) {
				tooltip =
					FormUtility.getResourceString(
						BaseForm.AUCTIONSELLBUNDLE,
						AuctionStatusListConstants.LSTITM_WITHDRAWN,
						session);
				img =
					new Image(
						WebUtil.getMimeURL(
							contextPath,
							null,
							null,
							"/mimes/images/withdrawn.gif"),
						tooltip);
				img.setWidth("16");
				img.setHeight("16");
				component = new DataComponent(img);
			} else if (status == AuctionStatusEnum.CLOSED.getValue()) {
				tooltip =
					FormUtility.getResourceString(
						BaseForm.AUCTIONSELLBUNDLE,
						AuctionStatusListConstants.LSTITM_CLOSED,
						session);
				img =
					new Image(
						WebUtil.getMimeURL(
							contextPath,
							null,
							null,
							"/mimes/images/closed1.gif"),
						tooltip);
				img.setWidth("16");
				img.setHeight("16");
				component = new DataComponent(img);
			} else if (status == AuctionStatusEnum.FINALIZED.getValue()) {
				tooltip =
					FormUtility.getResourceString(
						BaseForm.AUCTIONSELLBUNDLE,
						AuctionStatusListConstants.LSTITM_FINALIZED,
						session);
				img =
					new Image(
						WebUtil.getMimeURL(
							contextPath,
							null,
							null,
							"/mimes/images/finalized1.gif"),
						tooltip);
				img.setWidth("16");
				img.setHeight("16");
				GridLayout grid = new GridLayout();
				grid.setCellSpacing(4);
				GridLayoutCell cell = new GridLayoutCell(img);
				grid.addCell(1, 1, cell);
				TextView view = new TextView();
				view.setText(tooltip);
				cell = new GridLayoutCell(view);
				grid.addCell(1, 2, cell);
				component = new DataComponent(grid);
			} else if (status == AuctionStatusEnum.NOT_FINALIZED.getValue()) {
				tooltip =
					FormUtility.getResourceString(
						BaseForm.AUCTIONSELLBUNDLE,
						AuctionStatusListConstants.LSTITM_NOTFINAL,
						session);
				img =
					new Image(
						WebUtil.getMimeURL(
							contextPath,
							null,
							null,
							"/mimes/images/closed1.gif"),
						tooltip);
				img.setWidth("16");
				img.setHeight("16");
				GridLayout grid = new GridLayout();
				grid.setCellSpacing(4);
				GridLayoutCell cell = new GridLayoutCell(img);
				grid.addCell(1, 1, cell);
				TextView view = new TextView();
				view.setText(tooltip);
				cell = new GridLayoutCell(view);
				grid.addCell(1, 2, cell);
				if (isReferencedAuction(auction)) {

				}
				component = new DataComponent(grid);
			} else if (status == AuctionStatusEnum.FINISHED.getValue()) {
				tooltip =
					FormUtility.getResourceString(
						BaseForm.AUCTIONSELLBUNDLE,
						AuctionStatusListConstants.LSTITM_FINISHED,
						session);
				img =
					new Image(
						WebUtil.getMimeURL(
							contextPath,
							null,
							null,
							"/mimes/images/finalized1.gif"),
						tooltip);
				img.setWidth("16");
				img.setHeight("16");
				GridLayout grid = new GridLayout();
				grid.setCellSpacing(4);
				GridLayoutCell cell = new GridLayoutCell(img);
				grid.addCell(1, 1, cell);
				TextView view = new TextView();
				view.setText(tooltip);
				cell = new GridLayoutCell(view);
				grid.addCell(1, 2, cell);
				component = new DataComponent(grid);

			} else if (status == AuctionStatusEnum.CHECKEDOUT.getValue()) {
				tooltip =
					FormUtility.getResourceString(
						BaseForm.AUCTIONSELLBUNDLE,
						AuctionStatusListConstants.LSTITM_INCOMPLETE,
						session);
				img =
					new Image(
						WebUtil.getMimeURL(
							contextPath,
							null,
							null,
							"/mimes/images/finalized1.gif"),
						tooltip);
				img.setWidth("16");
				img.setHeight("16");
				GridLayout grid = new GridLayout();
				grid.setCellSpacing(4);
				GridLayoutCell cell = new GridLayoutCell(img);
				grid.addCell(1, 1, cell);
				TextView view = new TextView();
				view.setText(tooltip);
				cell = new GridLayoutCell(view);
				grid.addCell(1, 2, cell);
				component = new DataComponent(grid);
			} else {
				TextView view = new TextView("Unknown");
				view.setText("Unknown");
				component = new DataComponent(view);
			}
			exiting(METHOD);
			return component;
		} else if (columnName.equals(TBLCOLID_ERRORS)) {
			DataComponent component = null;
			if (auction.getUserObject() != null
				|| (auction.getExecutionException() != null
					&& auction.getStatus()
						== AuctionStatusEnum.SCHEDULED.getValue())) {
				Link link = new Link(TBLCOL_ERRORS);
				link.addText(
					FormUtility.getResourceString(
						BaseForm.AUCTIONSELLBUNDLE,
						TBLCOL_ERRORS,
						session));
				link.setOnClientClick(
					getSubareaLink(
						FormUtility.getResourceString(
							BaseForm.AUCTIONSELLBUNDLE,
							TBLCOL_ERRORS,
							session),
						PublishQueueForm.SHOWERRORS,
						row));
				component = new DataComponent(link);
			} else {
				TextView view = new TextView("Result");
				view.setText("");
				component = new DataComponent(view);
			}
			exiting(METHOD);
			return component;
		}

		return new DataString("");
	}

	private boolean isReferencedAuction(PrivateAuction auction) {
		return (auction.getReferencedAuctionId() != null);
	}

	public int getRowCount() {
		if (result != null) {
			try {
				int size = result.getSize();
				return size;
			} catch (Exception exc) {
				logInfo("Unable to retrieve the size of the result", exc);
			}
		}
		return 0;
	}

	protected ConversionHelper getConversionHelper() {
		if (null == helper) {
			UserSessionData userSession =
				UserSessionData.getUserSessionData(session);
			HelpManager manager =
				(HelpManager) userSession.getAttribute(HelpManager.HELPMANAGER);
			helper =
				(ConversionHelper) manager.getHelper(
					HelpManager.CONVERSIONHELPER);
		}
		return helper;
	}

	public Object getValueAt(int row) {
		try {
			return result.fetch(row);
		} catch (Exception exc) {
			logInfo(
				"Unable to retrieve the auction from index - " + (row - 1),
				exc);
		}
		return null;
	}

	/* (non-Javadoc)
	 * @see com.sap.isa.ui.components.table.ExtTableViewModel#setVisibleRowCount(int)
	 */
	public void setVisibleRowCount(int newVisibleRowCount) {
		super.setVisibleRowCount(newVisibleRowCount);
		if (result != null) {
			if (newVisibleRowCount > 0
				&& newVisibleRowCount <= result.getSize()) {
				result.setFetchSize(newVisibleRowCount);
			}
		}
	}

	/* (non-Javadoc)
	 * @see com.sap.isa.ui.components.table.TableModel#setVisibleFirstRow(int)
	 */
	public void setVisibleFirstRow(int newFirstVisibleRow) {
		super.setVisibleFirstRow(newFirstVisibleRow);
		if (result != null && result.getSize() > 0) {
			if (firstVisibleRow < 1) {
				firstVisibleRow = 1;
			}
			result.fetch(
				newFirstVisibleRow,
				newFirstVisibleRow + getVisibleRowCount() - 1);
			result.setFirstVisibleRow(newFirstVisibleRow);
		}
	}
	private String getSubareaLink(String title, String subarea, int row) {
		StringBuffer clientClickBuffer = new StringBuffer();
		clientClickBuffer.append("_auction_showLink('");
		clientClickBuffer.append(title);
		clientClickBuffer.append("','");
		clientClickBuffer.append(PublishQueueForm.CHOOSEELEMENTLINK);
		clientClickBuffer.append(subarea);
		clientClickBuffer.append(ActionForwards.AMBERSANDESC);
		clientClickBuffer.append(PublishQueueForm.INDEXOFPARENTPARAMETER);
		clientClickBuffer.append(ActionForwards.EQUALESC);
		clientClickBuffer.append(row);
		clientClickBuffer.append("')");
		return clientClickBuffer.toString();
	}
	
	protected String getSubareaLink(String referencePath) {
		StringBuffer clientClickBuffer = new StringBuffer();
		clientClickBuffer.append("_auction_showLink('");
		clientClickBuffer.append("','");
		clientClickBuffer.append(referencePath);
		clientClickBuffer.append("')");
		return clientClickBuffer.toString();
	}
	
	public void clean()  {
		if(result != null)  {
			result.disconnect();
			result = null;
		}
	}
}
