package com.sap.isa.auction.models.tablemodels.seller;

import javax.servlet.http.HttpSession;

import com.sap.isa.auction.actionforms.seller.AuctionSearchFormConstants;
import com.sap.isa.auction.actions.ActionForwards;

/**
 * Title:			Internet Sales Private Auctions
 * Description:
 * @Copyright: 		Copyright (c) 2004
 * Company:			SAPLabs LLC
 * Created on:		Jun 3, 2004
 */
public class ManualWinnerAuctionsTableModel extends AuctionsTableModel {

	public ManualWinnerAuctionsTableModel(HttpSession session, 
			String contextPath){
		super(session, contextPath);
	}
	/* (non-Javadoc)
	 * @see com.sap.isa.auction.models.tablemodels.seller.AuctionsTableModel#getSubareaLink(java.lang.String)
	 */
	protected String getSubareaLink(String referencePath) {
		StringBuffer clientClickBuffer = new StringBuffer();
		clientClickBuffer.append("_auction_showLink('");
		clientClickBuffer.append("','");
		clientClickBuffer.append(referencePath);
		clientClickBuffer.append(ActionForwards.AMBERSANDESC);
		clientClickBuffer.append(AuctionSearchFormConstants.FROMSRCPARAM);
		clientClickBuffer.append(ActionForwards.EQUALESC);
		clientClickBuffer.append(
				AuctionSearchFormConstants.SRC_MANUALWINNERQUEUE);
		clientClickBuffer.append("')");
		return clientClickBuffer.toString();
	}

}
