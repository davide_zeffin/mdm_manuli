package com.sap.isa.auction.models.tablemodels.seller;

import javax.servlet.http.HttpSession;

import com.sap.isa.auction.actionforms.seller.AuctionSearchFormConstants;
import com.sap.isa.auction.actions.seller.ActionForwards;
import com.sap.isa.auction.bean.Auction;
import com.sap.isa.auction.bean.AuctionStatusEnum;
import com.sap.isa.auction.bean.b2x.PrivateAuction;
import com.sapportals.htmlb.type.AbstractDataType;
import com.sapportals.htmlb.type.DataString;

/**
 * Title:			Internet Sales Private Auctions
 * Description:
 * @Copyright: 		Copyright (c) 2004
 * Company:			SAPLabs LLC
 * @author 
 * Created on: 		May 19, 2004
 */
public class PublishAuctionsTableModel extends AuctionsTableModel {

	public PublishAuctionsTableModel(HttpSession session, String contextPath) {
		super(session, contextPath);
	}

	/* (non-Javadoc)
	 * @see com.sap.isa.ui.components.table.TableModel#isRowSelectable(int)
	 */
	public boolean isRowSelectable(int row) {
		try {
			Auction auction = (Auction) result.fetch(row);
			if (auction.getStatus()
				== AuctionStatusEnum.PUBLISHED.getValue()) {
				return false;
			}
		} catch (Exception exc) {
			logInfo("Error occurred ", exc);
		}
		return super.isRowSelectable(row);
	}
	
	public AbstractDataType getValueAt(int row, String columnName) {
		if (!columnName.equals(TBLCOLID_PUBLISHDATE)) {
			return super.getValueAt(row, columnName);
		}
		else {
			PrivateAuction auction = null;
			logInfo(
				"get object at the row: " + row + " , column name: " + columnName);
			try {
				auction = (PrivateAuction) result.fetch(row);

			} catch (Exception exc) {
				//return new DataString("");
				logInfo("Error occurred ", exc);
			}
			String	value =
				getConversionHelper().javaDatetoUILocaleSpecificString(
					auction.getPublishDate());
			return new DataString(value);
		}
	}
	protected String getSubareaLink(String referencePath) {
		StringBuffer clientClickBuffer = new StringBuffer();
		clientClickBuffer.append("_auction_showLink('");
		clientClickBuffer.append("','");
		clientClickBuffer.append(referencePath);
		clientClickBuffer.append(ActionForwards.AMBERSANDESC);
		clientClickBuffer.append(AuctionSearchFormConstants.FROMSRCPARAM);
		clientClickBuffer.append(ActionForwards.EQUALESC);
		clientClickBuffer.append(AuctionSearchFormConstants.SRC_PUBLISHQUEUE);
		clientClickBuffer.append("')");
		return clientClickBuffer.toString();
	}
}
