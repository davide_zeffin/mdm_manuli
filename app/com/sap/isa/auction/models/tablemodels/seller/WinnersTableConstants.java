package com.sap.isa.auction.models.tablemodels.seller;

/**
 * Title:			Internet Sales Private Auctions
 * Description:
 * @Copyright: 		Copyright (c) 2004
 * Company:			SAPLabs LLC
 * @author 
 * Created on: 		May 5, 2004
 */
public interface WinnersTableConstants {
	//I13N Text
	public static final String TBLCOL_BIDAMT		= "WF.BidAmount";
	//htmlb componet ID
	public static final String TBLCOLID_LSTNAME 	= "TBLCOLID_LSTNAME.WinnerTable";
	public static final String TBLCOLID_FSTNAME 	= "TBLCOLID_FSTNAME.WinnerTable";
	public static final String TBLCOLID_BIDAMT 		= "TBLCOLID_BIDAMT.WinnerTable";
	public static final String TBLCOLID_QUANTITY = "TBLCOLID_QUANTITY.WinnerTable";

	public static final String PARAM_WINNERID 		= "WINNERID.WinnerTable";
	public static final String PARAM_DETAILSTARGET 	= "DETAILSTARGET.WinnerTable";
}
