package com.sap.isa.auction.models.tablemodels.seller;

/**
 * Title:			Internet Sales Private Auctions
 * Description:
 * @Copyright: 		Copyright (c) 2004
 * Company:			SAPLabs LLC
 * @author 
 * Created on: 		May 5, 2004
 */
public interface BidsTableConstants {
	//I13N text
	public static final String TBLCOL_BIDTIME 		= "BT.BidTime";
	public static final String TBLCOL_BIDQUANTITY	= "BT.BidQuantity";
	//htmlb componet ID
	public static final String TBLCOLID_LSTNAME		= "TBLCOLID_LSTNAME.BidTable";
	public static final String TBLCOLID_FSTNAME 	= "TBLCOLID_FSTNAME.BidTable";
	public static final String TBLCOLID_BIDAMT  	= "TBLCOLID_BIDAMT.BidTable";
	public static final String TBLCOLID_BIDTIME 	= "TBLCOLID_BIDTIME.BidTable";
	public static final String TBLCOLID_BIDQUANTITY = "TBLCOLID_BIDQUANTITY.BidTable";
	
	public static final String PARAM_BIDDERID 		= "BIDERID.BidTable";
	public static final String PARAM_DETAILSTARGET 	= "DETAILSTARGET.BidTable";
}
