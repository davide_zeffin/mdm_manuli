package com.sap.isa.auction.models.tablemodels.seller;


import java.util.ArrayList;
import java.util.Collection;

import javax.servlet.http.HttpSession;

import org.apache.struts.action.ActionServlet;

import com.sap.isa.auction.AuctionTargetGroupBP;
import com.sap.isa.auction.actionforms.seller.TargetGroupsForm;
import com.sap.isa.auction.bean.TargetGroupBP;
import com.sap.isa.auction.businessobject.targetGroup.TargetGroup;
import com.sap.isa.auction.helpers.HelpManager;
import com.sap.isa.auction.helpers.TargetGroupBPHelper;
import com.sap.isa.auction.models.tablemodels.BaseTableModel;
import com.sap.isa.businesspartner.businessobject.BusinessPartner;
import com.sap.isa.core.UserSessionData;
import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.core.util.WebUtil;
import com.sap.isa.ui.components.DataComponent;
import com.sapportals.htmlb.Image;
import com.sapportals.htmlb.Link;
import com.sapportals.htmlb.TextView;
import com.sapportals.htmlb.table.TableColumn;
import com.sapportals.htmlb.type.AbstractDataType;
import com.sapportals.htmlb.type.DataString;

/**
 * Title:
 * Description:
 * Copyright:    Copyright (c) 2002
 * Company:
 * @author
 * @version 1.0
 */

public class BusinessPartnerTGModel extends BaseTableModel
									implements BusinessPartnerTGModelConstants  {

  public static final String NA = "NotApplicable.TargetGroup";

  private Collection dataColl;
  private Object[] dataArray;
  private IsaLocation log = IsaLocation.getInstance("BusinessPartnerTGModel");
  private boolean showBPTGDetailsLink = false;
  private String reference;
  private String target;
  private String contextPath;
  private TargetGroupBPHelper tgBPHelper;
  private String businessPartnerTxt;
  private String targetGroupTxt;
  private String notApplicableTxt;
  private ActionServlet servlet;
  private HttpSession session;

  public BusinessPartnerTGModel() {
	  super();
  }

  public void setContextPath(String contextPath)  {
	  this.contextPath = contextPath;
  }

  public BusinessPartnerTGModel(ActionServlet servlet , HttpSession session)  {
  		super();
	  	this.servlet = servlet;
	  	this.session = session;
  }
  
  private TargetGroupBPHelper getHelper()  {
  	if(tgBPHelper == null)  {
  		UserSessionData userSession = UserSessionData.getUserSessionData(session);
  		HelpManager helpManager = (HelpManager)userSession.getAttribute(HelpManager.HELPMANAGER);
  		tgBPHelper = (TargetGroupBPHelper)helpManager.getHelper(HelpManager.TARGETGROUPHELPER);
  	}
  	return tgBPHelper;
  }

  public void initColumns() {
	  TableColumn column = new TableColumn(this , IMAGE , IMAGE);
	  addColumn(column);

	  column = new TableColumn(this , PARTNERIDTITLE , PARTNERID);
	  addColumn(column);

	  column = new TableColumn(this , LASTNAMETITLE , LASTNAME);
	  addColumn(column);

	  column = new TableColumn(this , FIRSTNAMETITLE , FIRSTNAME);
	  addColumn(column);

	  //column = new TableColumn(this , ORGANISATIONTITLE , ORGANISATION);
	  //addColumn(column);

	  column = new TableColumn(this , CITYTITLE , CITY);
	  addColumn(column);

	  //column = new TableColumn(this , POCODETITLE , POCODE);
	  //addColumn(column);

	  //column = new TableColumn(this , REGIONTITLE , REGION);
	  //addColumn(column);

	  column = new TableColumn(this , COUNTRYTITLE , COUNTRY);
	  addColumn(column);
  }

  
  private AbstractDataType getValueAt(BusinessPartner businessPartner,
													  String column)  {

		if(column.equals(IMAGE))  {
			
			  Image img = new Image(WebUtil.getMimeURL(contextPath , null , null , "/mimes/images/userDL.gif") ,
				  businessPartner.getAddress().getLastName());
			  img.setWidth("16");
			  img.setHeight("16");
			  DataComponent component = new DataComponent(img);
			  return component;
		}  else if(column.equals(PARTNERID))  {

			  if (/*showBPTGDetailsLink*/false)  {
				  Link link = new Link(businessPartner.getId(), businessPartner.getId());
				  link.setReference(reference+"?"+TargetGroupsForm.BPIDPARAMETER+
									  "="+businessPartner.getId());
				  link.setTarget(target);
				  DataComponent dataCompont = new DataComponent(link);
				  return dataCompont;
			  }  else {
				  TextView view = new TextView();
				  view.setText(businessPartner.getId());
				  return new DataComponent(view);
			  }
		} else if(column.equals(LASTNAME))  {
			  return new DataString(businessPartner.getAddress().getLastName());
		} else if(column.equals(FIRSTNAME))  {
			  return new DataString(businessPartner.getAddress().getFirstName());
		} else if(column.equals(CITY))  {
			  return new DataString(businessPartner.getAddress().getCity());
		} else if(column.equals(COUNTRY))  {
			  return new DataString(businessPartner.getAddress().getCountry());
	  	}
	    return new DataString("-");
  }

  private AbstractDataType getValueAt(TargetGroup tg , 
  											String col)  {

		if(col.equals(IMAGE))  {
			  Image img = new Image(WebUtil.getMimeURL(contextPath , null , null , "/mimes/images/companyDL.gif") ,
				  tg.getDescription());
			  img.setWidth("16");
			  img.setHeight("16");
			  DataComponent component = new DataComponent(img);
			  return component;
		}  else if(col.equals(PARTNERID))  {
			  if (false/*showBPTGDetailsLink*/) {
				  Link link = new Link(tg.getGuid(), tg.getDescription());
				  link.setReference(reference+"?"+TargetGroupsForm.TARGETGROUPPARAMETER+
									  "="+tg.getGuid());
				  link.setTarget(target);
				  DataComponent dataCompont = new DataComponent(link);
				  return dataCompont;
				}  else  {
					TextView view = new TextView();
					view.setText(tg.getDescription());
					DataComponent dataCompont = new DataComponent(view);
					return dataCompont;
				}
		}  else if(col.equals(LASTNAME))  {
				if(showBPTGDetailsLink)  {
					TextView view= new TextView();
					view.setText(tg.getDescription());
					return new DataComponent(view);
				}
		}
		return new DataString(notApplicableTxt);
  }


  public void setData(Collection data, String reference, String target,
					  boolean showDetailsLink)  {
	  if(null == data)  {
		  dataColl = new ArrayList();
	  }  else  {
		  dataColl = data;
	  }
	  dataArray = dataColl.toArray();
	  this.reference = reference;
	  this.target = target;
	  showBPTGDetailsLink = showDetailsLink;
	  setVisibleFirstRow(1);
  }

  public int getRowCount()  {
	  if(null == dataColl)  {
		  return 0;
	  }
	  return dataColl.size();
  }

  public Object getValueAt(int row)  {
	  if(null == dataColl)  {
		  return null;
	  }

	  Object item = null;
	  try  {
		  item = dataArray[row-1];
		  if(item instanceof TargetGroupBP)  {
			  TargetGroupBP targetGroupOrBP = (TargetGroupBP)item;
			  String guid = targetGroupOrBP.getGuid();
			  if(targetGroupOrBP.getType().equals(AuctionTargetGroupBP.TARGET_GROUP))  {

				  item = getHelper().getTargetGroup(targetGroupOrBP.getGuid());
				  if(null == item)  {
					  log.debug("Unknown TargetGroup Id : " + targetGroupOrBP.getGuid());
					  return targetGroupOrBP;//new DataString("Unknown Target Group id" + targetGroupOrBP.getGuid());
				  }

			  } else  {
				  item = getHelper().getBusinessPartner(targetGroupOrBP.getGuid());
				  if(null == item)  {
					  log.debug("Unknown BusinessPartner Id : " + targetGroupOrBP.getGuid());
					  return targetGroupOrBP;//new DataString("Unknown BP id" + targetGroupOrBP.getGuid());
				  }

			  }
		  }
	  }  catch(Exception exc)  {
		  return null;
	  }
	  return item;
  }


	public void setNotApplicableText(String notApplicable) {
		this.notApplicableTxt = notApplicable;
	}
	/* (non-Javadoc)
	 * @see com.sapportals.htmlb.table.TableViewModel#getValueAt(int, java.lang.String)
	 */
	public AbstractDataType getValueAt(int row, String column) {
		
		try  {
			Object obj = dataArray[row - 1];
			if(obj instanceof TargetGroupBPHelper.TargetGroupBusinessPartner)  {
				TargetGroupBPHelper.TargetGroupBusinessPartner tgBP = (TargetGroupBPHelper.TargetGroupBusinessPartner)obj;
				if(TargetGroupBPHelper.TargetGroupBusinessPartner.BUSINESSPARTNER == tgBP.getType())  {
					BusinessPartner bp = getTargetGroupBPHelper().getBusinessPartner(tgBP.getId());
					return getValueAt(bp , column);
				}  else {
					TargetGroup tg = getTargetGroupBPHelper().getTargetGroup(tgBP.getId());
					return getValueAt(tg , column);
				}
			}  else if(obj instanceof BusinessPartner)  {
				return getValueAt((BusinessPartner)obj , column);
			}  else if(obj instanceof TargetGroup)  {
				return getValueAt((TargetGroup)obj , column);
			}
		}  catch(Exception exc)  {
			logInfo("Unable retrieve value for the current row - " + row + " column - " + column);
		}
		return new DataString("-");
	}
	
	private TargetGroupBPHelper targetGroupHelper;
	
	private TargetGroupBPHelper getTargetGroupBPHelper()  {
		if(null == targetGroupHelper)  {
			UserSessionData userSession = UserSessionData.getUserSessionData(session);
			HelpManager helpMang = (HelpManager)userSession.getAttribute(HelpManager.HELPMANAGER);
			targetGroupHelper = (TargetGroupBPHelper)helpMang.getHelper(HelpManager.TARGETGROUPHELPER);
		}
		return targetGroupHelper;
	}

}
