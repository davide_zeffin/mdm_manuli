/*
 * Created on Jun 11, 2004
 *
 * To change the template for this generated file go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
package com.sap.isa.auction.models.tablemodels.seller;

import java.util.ArrayList;
import java.util.Collection;

import com.sap.isa.auction.models.tablemodels.BaseTableModel;
import com.sap.isa.ui.components.DataComponent;
import com.sapportals.htmlb.Component;
import com.sapportals.htmlb.HTMLFragment;
import com.sapportals.htmlb.Link;
import com.sapportals.htmlb.TextView;
import com.sapportals.htmlb.type.AbstractDataType;

/**
 * @author I803067
 *
 * To change the template for this generated type comment go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
public class ShopTableModel extends BaseTableModel  {

	public static final String SHOPNAME = "ShopName";
	public static final String SHOPDESC = "ShopDescription";
	private Collection dataColl;
	
	public void initializeData()  {
		this.dataColl = new ArrayList();
	}
	
	public AbstractDataType getValueAt(int row , String columnName) {
		ShopData shopData = null;
		try  {
//			row - 1 => htmlb library always starts with line one. 
//			upport note 1167939				
			shopData = (ShopTableModel.ShopData)((ArrayList)dataColl).get(row-1);
		}  catch(Exception exc)  {
			return new DataComponent(new TextView(""));
		}
		
		if(columnName.equals(SHOPNAME))  {
			return new DataComponent(shopData.getShopId());
		}  else if(columnName.equals(SHOPDESC))  {
			return new DataComponent(shopData.getShopDescription());
		}
		return new DataComponent(new TextView(""));
	}
	
	public class ShopData  {
		private Component shopId;
		private Component shopDescription;
		
		public ShopData(Component id , Component desc)  {
			this.shopId = id;
			this.shopDescription = desc;
		}
		
		/**
		 * @return
		 */
		public Component getShopDescription() {
			return shopDescription;
		}

		/**
		 * @return
		 */
		public Component getShopId() {
			return shopId;
		}

		/**
		 * @param component
		 */
		public void setShopDescription(Component component) {
			shopDescription = component;
		}

		/**
		 * @param component
		 */
		public void setShopId(Component component) {
			shopId = component;
		}

	}
	public void addShop(Link link, HTMLFragment fragment) {
		dataColl.add(new ShopData(link , fragment));
	}
	
	public int getRowCount()  {
		return dataColl != null ? dataColl.size() : 0;
	}
}
