package com.sap.isa.auction.models.tablemodels.seller;


import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;

import javax.servlet.http.HttpSession;

import com.sap.isa.auction.bean.AuctionItem;
import com.sap.isa.auction.bean.SalesArea;
import com.sap.isa.auction.helpers.AuctionLineItem;
import com.sap.isa.auction.helpers.ConversionHelper;
import com.sap.isa.auction.helpers.ExtArrayList;
import com.sap.isa.auction.helpers.HelpManager;
import com.sap.isa.auction.models.tablemodels.BaseTableModel;
import com.sap.isa.auction.util.Utilidad;
import com.sap.isa.catalog.webcatalog.WebCatItem;
import com.sap.isa.core.UserSessionData;
import com.sap.isa.core.util.WebUtil;
import com.sap.isa.ui.components.DataComponent;
import com.sapportals.htmlb.Image;
import com.sapportals.htmlb.InputField;
import com.sapportals.htmlb.Link;
import com.sapportals.htmlb.TextView;
import com.sapportals.htmlb.enum.TableColumnType;
import com.sapportals.htmlb.table.TableColumn;
import com.sapportals.htmlb.type.AbstractDataType;
import com.sapportals.htmlb.type.DataString;

/**
 * Title:
 * Description:
 * Copyright:    Copyright (c) 2003
 * Company:
 * @author
 * @version 1.0
 */

public class ProductsTableModel extends BaseTableModel implements ProductsTableContants  {

  protected String referencePath;
  protected String contextPath;
  protected boolean salesAreaEditable = true;
  protected SalesArea salesArea;
  protected String plant;
  protected String storage;

  protected String target;
  protected ConversionHelper conversionHelper;

  public ProductsTableModel(HttpSession session , String contextPath) {
      this.session = session;
      this.contextPath = contextPath;
  }

    public void setReferenceTarget(String referencePath , String target)  {
        this.referencePath = referencePath;
        this.target = target;
    }

    public void init1Columns() {
        TableColumn column = new TableColumn(this , ID , IDTITLE , TableColumnType.LINK);
        addActualColumn(column);

        column = new TableColumn(this , DESCRIPTION , DESCRIPTIONTITLE , TableColumnType.TEXT);
        addActualColumn(column);

		column = new TableColumn(this , NORMALDESCRIPTION , DESCRIPTIONTITLE , TableColumnType.TEXT);
		addActualColumn(column);

        column = new TableColumn(this , QUANTITY , QUANTITYTITLE , TableColumnType.TEXT);
        addActualColumn(column);

        column = new TableColumn(this , IMAGE , IMAGETITLE , TableColumnType.USER);
        addActualColumn(column);
    }

    public void setData(Collection data)  {
        if(null == data)  {
            data = new ArrayList();
        }
        dataColl = data;
        dataArray = dataColl.toArray();
        setVisibleFirstRow(1);
        salesAreaEditable = true;
        salesArea = null;
        plant = null;
    }

	public void setData(Collection data , boolean salesAreaEditable)  {
		setData(data);
		this.salesAreaEditable = salesAreaEditable;
	}
	
    public AbstractDataType getValueAt(int row, String columnName) {

        Object item = null;
        try  {
            item = dataArray[row - 1];
        }  catch(Exception exc)  {
            return getDummyData(columnName);
        }
        if(item instanceof WebCatItem)  {
			return getValueAt((WebCatItem)item , row , columnName);	
        }  else if(item instanceof AuctionLineItem)  {
			return getValueAt((AuctionLineItem)item , row , columnName);
        }  else if(item instanceof AuctionItem)  {
			return getValueAt((AuctionItem)item , row , columnName);
        }
        return getDummyData(columnName);
    }
    
    public AbstractDataType getValueAt(WebCatItem item , int row , String columnName)  {
        if(columnName.equals(ID))  {
            return new DataString(item.getProduct());
        }  else if(columnName.equals(DESCRIPTION)) {
            Link link = new Link(Integer.toString(row));
            TextView view = new TextView();
            view.setText(item.getDescription());
            view.setWrapping(true);
            link.addComponent(view);
            link.setReference(referencePath+"="+item.getProductID());
            if(target != null)  {
                link.setTarget(target);
            }
            DataComponent dataCompont = new DataComponent(link);
            return dataCompont;
		}  else if(columnName.equals(NORMALDESCRIPTION)) {
			TextView view = new TextView();
			view.setText(item.getDescription());
			view.setWrapping(true);
			DataComponent dataCompont = new DataComponent(view);
			return dataCompont;
        }  else if(columnName.equals(QUANTITY))  {
            return getQuantity(QUANTITY , row);
        }  else if(columnName.equals(UNIT))  {
            return new DataString(item.getUnit());
        }  else if(columnName.equals(IMAGE))  {
            String imgURL = Utilidad.getProductImageURL(item);
            Image img;
            if(!imgURL.startsWith("http"))  {
                imgURL = WebUtil.getMimeURL(contextPath , null , null , imgURL);
            }
            img = new Image(imgURL , item.getDescription());
            DataComponent componentImg = new DataComponent(img);
            img.setWidth("60");
            img.setHeight("60");
            return componentImg;
        }
        return getDummyData(columnName);
    }

	public AbstractDataType getValueAt(AuctionLineItem item , int row , String columnName)  {
		if(columnName.equals(ID))  {
			return new DataString(item.getProductId());
		}  else if(columnName.equals(NORMALDESCRIPTION)) {
			TextView view = new TextView();
			view.setText(item.getDescription());
			view.setWrapping(true);
			DataComponent dataCompont = new DataComponent(view);
			return dataCompont;
		}  else if(columnName.equals(QUANTITY))  {
			return getQuantity(item.getQuantity() , row);
		}  else if(columnName.equals(UNIT))  {
			return new DataString(item.getUnit());
		}  else if(columnName.equals(SALESAREA))  {
			StringBuffer strbuffer = new StringBuffer();
			return new DataString(strbuffer.toString());
		}  else if(columnName.equals(QUANTITYTEXT))  {
			return new DataString(getDisplayString(item.getQuantity()));
		}  else if(columnName.equals(QUANTPERAUCT))  {
			return getQuantity( QUANTPERAUCT , row);
		}
		return getDummyData(columnName);
	}

	public AbstractDataType getValueAt(AuctionItem item , int row , String columnName)  {
		if(columnName.equals(ID))  {
			return new DataString(item.getProductCode());
		}  else if(columnName.equals(NORMALDESCRIPTION)) {
			TextView view = new TextView();
			view.setText(item.getDescription());
			view.setWrapping(true);
			DataComponent dataCompont = new DataComponent(view);
			return dataCompont;
		}  else if(columnName.equals(QUANTITY))  {
			return getQuantity(item.getQuantity() , row);
		}  else if(columnName.equals(UNIT))  {
			return new DataString(item.getUOM());
		}  else if(columnName.equals(SALESAREA))  {
			StringBuffer strbuffer = new StringBuffer();
			return new DataString(strbuffer.toString());
		}  else if(columnName.equals(QUANTITYTEXT))  {
			return new DataString(getDisplayString(item.getQuantity()));
		}  else if(columnName.equals(QUANTPERAUCT))  {
			return getQuantity( QUANTPERAUCT , row);
		}
		return getDummyData(columnName);
	}

    public Object getValueAt(int row)  {
        if(dataArray != null && dataArray.length >= row)  {
            return dataArray[row - 1];
        }  else  {
            return null;
        }
    }

    public WebCatItem getWebCatItem(int index)  {
        return (WebCatItem)dataArray[index-1];
    }

    public DataComponent getQuantity(String id , int row)  {
        InputField input = new InputField(id);
        input.setId(id);
        input.setValue("1");
        input.setSize(8);
        DataComponent componentInput = new DataComponent(input);
        return componentInput;
    }

	public DataComponent getQuantity(double quantity , int row)  {
		InputField input = new InputField(QUANTITY);
		input.setId(QUANTITY);
		if(quantity > 0.0)
			input.setValue(getDisplayString(quantity));
		else 
			input.setValue("");
		input.setSize(8);
		DataComponent componentInput = new DataComponent(input);
		return componentInput;
	}

	protected String getDisplayString(double value) {
		return getConversionHelper().bigDecimalToUIQuantityString(new BigDecimal(value));
	}

	protected String getDisplayString(BigDecimal value) {
		return getConversionHelper().bigDecimalToUIQuantityString(value);
	}
	
	private ConversionHelper getConversionHelper()  {
		if(null == conversionHelper)  {
			UserSessionData sessionData =
				UserSessionData.getUserSessionData(session);
			HelpManager helpManager =
				(HelpManager) sessionData.getAttribute(HelpManager.HELPMANAGER);
			conversionHelper =
				(ConversionHelper) helpManager.getHelper(
					HelpManager.CONVERSIONHELPER);
		}
		return conversionHelper;
	}
	
	public int getRowCount()  {
		if(dataColl != null && dataColl instanceof ExtArrayList)  {
			return ((ExtArrayList)dataColl).getCapacity();
		}
		return super.getRowCount();
	}
}
