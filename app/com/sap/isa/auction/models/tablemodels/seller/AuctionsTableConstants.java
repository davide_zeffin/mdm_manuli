package com.sap.isa.auction.models.tablemodels.seller;

/**
 * Title:			Internet Sales Private Auctions
 * Description:
 * @Copyright: 		Copyright (c) 2004
 * Company:			SAPLabs LLC
 * @author
 * Created on:		Apr 27, 2004 
 */

public interface AuctionsTableConstants {

    //I13N text
    public static final String TBLCOL_NAME			= "AT.name";
    public static final String TBLCOL_DESCRIPTION	= "AT.description";
    public static final String TBLCOL_PUBLISHDATE	= "AT.publishDate";
    public static final String TBLCOL_SCHEDLUEDDATE = "AT.scheduledDate";
    public static final String TBLCOL_CLOSINGDATE	= "AT.closingDate";
    public static final String TBLCOL_STATUS		= "AT.status";
	public static final String TBLCOL_ERRORS		= "AT.Error";
	  
    //Htmlb Component Id (Table column)
    public static final String TBLCOLID_NAME		= "TBLCOLID_NAME";
    public static final String TBLCOLID_DESCRIPTION	= "TBLCOLID_DESCRIPTION";
    public static final String TBLCOLID_PUBLISHDATE	= "TBLCOLID_PUBLISHDATE";
    public static final String TBLCOLID_CLOSINGDATE	= "TBLCOLID_CLOSINGDATE";
	public static final String TBLCOLID_ERRORS		= "TBLCOLID_ERRORS";
    public static final String TBLCOLID_STATUS		= "TBLCOLID_STATUS";
	
    //AuctionID parameter
    public static final String PARAM_AUCTIONID		= "AUCTIONID";
    
}