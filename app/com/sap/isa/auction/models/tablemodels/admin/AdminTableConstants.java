/*
 * Created on Aug 25, 2003
 *
 * To change the template for this generated file go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
package com.sap.isa.auction.models.tablemodels.admin;

/**
 * Title:
 * Description:
 * @Copyright: 		Copyright (c) 2003
 * Company:
 * @author I802791
 *
 * To change the template for this generated type comment go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
public interface AdminTableConstants {
	//	tabel columne title: I13N text
	  public static final String NAME = "AT.Name";
	  public static final String DESCRIPTION = "AT.Description";
	  public static final String STATUS = "AT.Status";
	  public static final String ACTIVE = "AT.Active";
	  public static final String INACTIVE = "AT.Inactive";
}
