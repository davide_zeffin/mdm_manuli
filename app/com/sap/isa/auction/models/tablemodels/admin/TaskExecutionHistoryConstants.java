package com.sap.isa.auction.models.tablemodels.admin;


public interface TaskExecutionHistoryConstants extends TaskTableConstants  {
	
	//Table Column ID
	public static final String STARTTIMEID 	= "STARTTIMEID.Task";	
	public static final String ENDTIMEID 	= "ENDTIMEID.Task";	
	public static final String STATUSID	 	= "STATUSID.Task";
	public static final String VIEWLOGID	= "VIEWLOGID.Task";
	
	//I13N Table Column Text
	public static final String VIEWLOGTEXT	= "TEH.Task";
}
