/*
 * Created on Sep 3, 2003
 *
 * To change the template for this generated file go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
package com.sap.isa.auction.models.tablemodels.admin;

/**
 * Title:
 * Description:
 * @Copyright: 		Copyright (c) 2003
 * Company:
 * @author I802791
 *
 * To change the template for this generated type comment go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
public interface TaskTableConstants extends AdminTableConstants  {
	//	TaskTableModel Table Column Name: I13N name 
	public static final String TYPE = "TT.Type";
	public static final String NEXTRUNTIME = "TT.NextRunTime";
	public static final String LASTRUNTIME = "TT.TastRunTime";
	public static final String LASTRUNRESULT = "TT.LastRunResult";
	public static final String COUNT = "TT.Count";
	public static final String UPDATEAUCTIONWOWINNER = "UpdateAuctionWOWinnerTask";
	public static final String UPDATEAUCTIONTASKCLASSNAME = "UpdateAuctionStatusWithoutWinnerTask";
	
	
	//Task run result and status
	public static final String SCHEDULED = "Scheduled.TaskTable";
	public static final String QUEUED = "Queued.TaskTable";
	public static final String STOPPED = "Stopped.TaskTable";
	public static final String ERROR = "Error.TaskTable";
	public static final String EXECUTED = "Success.TaskTable";
	public static final String PAUSED = "Paused.TaskTable";
	public static final String RUNNING = "Running.TaskTable";
	public static final String NOTSCHEDULED = "NotScheduled.TaskTable";
	
	
		//TaskTableModel Table Column ID
	public static final String NAME_ID = "NAME_ID.TaskTable";
	public static final String TYPE_ID = "TYPE_ID.TaskTable";
	public static final String DESCRIPTION_ID = "DESCRIPTION_ID.TaskTable";
	public static final String NEXTRUNTIME_ID = "NEXTRUNTIME_ID.TaskTable";
	public static final String LASTRUNTIME_ID = "LASTRUNTIME_ID.TaskTable";
	public static final String STATUS_ID = "STATUS_ID.TaskTable";
	public static final String LASTRUNRESULT_ID = "LASTRUNRESULT_ID.TaskTable";
	public static final String COUNT_ID = "COUNT_ID.TaskTable";
	
	//request parameter Id
	public static final String TASK_ID = "TASK_ID.TaskTable";
	public static final String ROW_ID = "ROW_ID.TaskTable";
	
}
