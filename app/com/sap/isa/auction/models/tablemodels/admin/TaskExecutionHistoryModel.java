package com.sap.isa.auction.models.tablemodels.admin;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Collection;

import javax.servlet.http.HttpSession;

import com.sap.isa.auction.actionforms.BaseForm;
import com.sap.isa.auction.actionforms.admin.TaskDetailsFormConstants;
import com.sap.isa.auction.actions.ActionForwards;
import com.sap.isa.auction.helpers.ConversionHelper;
import com.sap.isa.auction.helpers.HelpManager;
import com.sap.isa.auction.models.tablemodels.BaseTableModel;
import com.sap.isa.auction.util.FormUtility;
import com.sap.isa.core.UserSessionData;
import com.sap.isa.services.schedulerservice.JobCycleStateManager;
import com.sap.isa.services.schedulerservice.JobExecutionHistoryRecord;
import com.sap.isa.ui.components.DataComponent;
import com.sapportals.htmlb.Link;
import com.sapportals.htmlb.enum.TableColumnType;
import com.sapportals.htmlb.table.TableColumn;
import com.sapportals.htmlb.type.AbstractDataType;
import com.sapportals.htmlb.type.DataString;

public class TaskExecutionHistoryModel extends BaseTableModel implements TaskExecutionHistoryConstants  {

	private HttpSession session;
	private ConversionHelper helper;
	private String reference;
	private String target;
	
	public TaskExecutionHistoryModel(HttpSession session)  {
		this.session = session;		
	}
	
	public void setReferenceTarget(String referencePath, String target) {
		this.reference = referencePath;
		this.target = target;
		logInfo("Asign reference: " + reference + " , target: " + target);
	}
	
	public void initColumns() {
		TableColumn column =
			new TableColumn(this, STARTTIMEID, TaskDetailsFormConstants.STARTTIME, TableColumnType.TEXT);
		addColumn(column);
		
		column = new TableColumn(this , ENDTIMEID , TaskDetailsFormConstants.ENDTIME, TableColumnType.TEXT);
		addColumn(column);
		
		column = new TableColumn(this , STATUSID , STATUS, TableColumnType.TEXT);
		addColumn(column);
		
		column = new TableColumn(this , VIEWLOGID , VIEWLOGTEXT, TableColumnType.LINK);
		addColumn(column);
	}
	
	public void setData(Collection data)  {
		if(null == data)  {
			dataColl = new ArrayList();
		}  else {
			dataColl = data;
		}
		dataArray = dataColl.toArray();
	}
	
	
	public AbstractDataType getValueAt(int row, String columnName) {
		
		JobExecutionHistoryRecord history = null;
		try  {
			history = (JobExecutionHistoryRecord)dataArray[row - 1];
		}  catch(Exception exc)  {
			logInfo("Error occurred: ", exc);
		}
		
		if(columnName.equals(STARTTIMEID))  {
			return new DataString(getDisplayString(new Timestamp(history.getStartTime())));
			//TextView view = new TextView(getDisplayString(new Timestamp(history.getStartTime())));
			//view.setText(getDisplayString(new Timestamp(history.getStartTime())));
			//link.addComponent(view);			
			//link.setOnClientClick("_showHistoryLog(" + row + ")");
			//return new DataComponent(link);
		}  else if(columnName.equals(ENDTIMEID))  {
			String displayString = "";
			if(history.getEndTime() > 0){
				displayString  = getDisplayString(new Timestamp(history.getEndTime()));	
			}
			return new DataString(displayString);
		}	else if (columnName.equals(VIEWLOGID)) {
			 Link link = new Link(VIEWLOGID);
			 link.addText(
					FormUtility.getResourceString(
						BaseForm.AUCTIONADMINBUNDLE,
						VIEWLOGTEXT,
						session)
			 );
			link.setReference(reference + ActionForwards.QUESTIONESC + ROW_ID + ActionForwards.EQUALESC + row);
			return new DataComponent(link);
		}else if(columnName.equals(STATUSID))  {

			String strResult = "";
			switch (history.getStatus()) {
				case JobCycleStateManager.SCHEDULED :
					{
						strResult =
							FormUtility.getResourceString(
								BaseForm.AUCTIONADMINBUNDLE,
								SCHEDULED,
								session);
						break;
					}
				case JobCycleStateManager.STOPPED :
					{
						strResult =
							FormUtility.getResourceString(
								BaseForm.AUCTIONADMINBUNDLE,
								STOPPED,
								session);
						break;
					}
				case JobCycleStateManager.ERROR :
					{
						strResult =
							FormUtility.getResourceString(
								BaseForm.AUCTIONADMINBUNDLE,
								ERROR,
								session);
						break;
					}
				case JobCycleStateManager.EXECUTED :
					{
						strResult =
							FormUtility.getResourceString(
								BaseForm.AUCTIONADMINBUNDLE,
								EXECUTED,
								session);
						break;
					}
				case JobCycleStateManager.PAUSED :
					{
						strResult =
							FormUtility.getResourceString(
								BaseForm.AUCTIONADMINBUNDLE,
								PAUSED,
								session);
						break;
					}
			}
			return new DataString(strResult);
		}
		return super.getDummyData(columnName);
	}
	
	private String getDisplayString(Timestamp date)  {
		return getConversionHelper().javaDatetoUILocaleSpecificString(date);
	}

	private ConversionHelper getConversionHelper() {
		if (null == helper) {
			UserSessionData userSession =
				UserSessionData.getUserSessionData(session);
			HelpManager manager =
				(HelpManager) userSession.getAttribute(HelpManager.HELPMANAGER);
			helper =
				(ConversionHelper) manager.getHelper(
					HelpManager.CONVERSIONHELPER);
		}
		return helper;
	}

}
