/*
 * Created on Sep 3, 2003
 *
 * To change the template for this generated file go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
package com.sap.isa.auction.models.tablemodels.admin;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Collection;

import javax.servlet.http.HttpSession;

import com.sap.isa.auction.actionforms.BaseForm;
import com.sap.isa.auction.actions.ActionForwards;
import com.sap.isa.auction.models.listmodels.admin.ScheduledTaskTypeListConstants;
import com.sap.isa.auction.helpers.ConversionHelper;
import com.sap.isa.auction.helpers.HelpManager;
import com.sap.isa.auction.helpers.SchedulerTaskHelper;
import com.sap.isa.auction.models.tablemodels.BaseTableModel;
import com.sap.isa.auction.util.FormUtility;
import com.sap.isa.core.UserSessionData;
import com.sap.isa.services.schedulerservice.CronJob;
import com.sap.isa.services.schedulerservice.Job;
import com.sap.isa.services.schedulerservice.JobCycleStateManager;
import com.sap.isa.ui.components.DataComponent;
import com.sapportals.htmlb.Link;
import com.sapportals.htmlb.enum.TableColumnType;
import com.sapportals.htmlb.table.TableColumn;
import com.sapportals.htmlb.type.AbstractDataType;
import com.sapportals.htmlb.type.DataString;

/**
 * Title:
 * Description:
 * @Copyright: 		Copyright (c) 2003
 * Company:
 * @author I802791
 *
 * To change the template for this generated type comment go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
public class TaskTableModel
	extends BaseTableModel
	implements TaskTableConstants {
	private HttpSession session;
	private Collection dataColl;
	private Object[] dataArray;
	private String reference;
	private String target;
	private String contextPath;
	private ConversionHelper helper;
	private SchedulerTaskHelper taskHelper;

	public TaskTableModel(HttpSession session, String contextPath) {
		this.session = session;
		this.contextPath = contextPath;
	}

	public void setReferenceTarget(String referencePath, String target) {
		this.reference = referencePath;
		this.target = target;
		logInfo("Asign reference: " + reference + " , target: " + target);
	}

	public void initColumns() {
		TableColumn column =
			new TableColumn(this, NAME_ID, NAME, TableColumnType.LINK);
		addColumn(column);

		column = new TableColumn(this, TYPE_ID, TYPE, TableColumnType.TEXT);
		addColumn(column);

		column =
			new TableColumn(
				this,
				DESCRIPTION_ID,
				DESCRIPTION,
				TableColumnType.TEXT);
		addColumn(column);

		column =
			new TableColumn(
				this,
				NEXTRUNTIME_ID,
				NEXTRUNTIME,
				TableColumnType.TEXT);
		addColumn(column);

		column =
			new TableColumn(
				this,
				LASTRUNTIME_ID,
				LASTRUNTIME,
				TableColumnType.TEXT);
		addColumn(column);

		column =
			new TableColumn(
				this,
				LASTRUNRESULT_ID,
				LASTRUNRESULT,
				TableColumnType.TEXT);
		addColumn(column);

		column = new TableColumn(this, STATUS_ID, STATUS, TableColumnType.TEXT);
		addColumn(column);

		column = new TableColumn(this, COUNT_ID, COUNT, TableColumnType.TEXT);
		addColumn(column);
	}

	public void setData(Collection data) {
		entering("setData to TaskTableModel");
		if (null == data) {
			logInfo("Data collection is null, create a new collection");
			data = new ArrayList();
		}
		dataColl = data;
		dataArray = dataColl.toArray();
		setVisibleFirstRow(1);
		exiting("setData to TaskTableModel");
	}
	
	public Object getValueAt(int row) {
		entering("getValueAt");
		logInfo("The row is:"+row);
		Object obj = null;
		if (dataArray != null) {
			obj = dataArray[row - 1];
		}
		exiting("getValueAt");
		return obj;
	}
	public AbstractDataType getValueAt(int row, String columnName) {

		entering("getValueAt");

		logInfo(
			"get object at the row: " + row + " , column name: " + columnName);
		Job job = null;
		if (dataColl != null
			&& (null == dataArray || dataArray.length != dataColl.size())) {
			dataArray = dataColl.toArray();
		}
		if (null == dataArray) {
			logInfo("data collection is null, return");
			return new DataString("");
		}
		try {
			job = (Job) dataArray[row - 1];

		} catch (Exception exc) {
			return new DataString("");
		}

		if (columnName.equals(NAME_ID)) {
			String taskClassName = job.getName().equals(UPDATEAUCTIONTASKCLASSNAME)?
					UPDATEAUCTIONWOWINNER:job.getName();
			String taskName = FormUtility.getResourceString(
								BaseForm.AUCTIONADMINBUNDLE,
								taskClassName,
								session);
			Link link = new Link(Integer.toString(row), taskName);
			if (job.getID()!=null)
				link.setReference(reference + ActionForwards.QUESTIONESC + TASK_ID + ActionForwards.EQUALESC + job.getID());
			else
				link.setReference(reference + ActionForwards.QUESTIONESC + ROW_ID + ActionForwards.EQUALESC + row);
			logInfo(link.getReference());
			link.setTarget(target);
			DataComponent dataCompont = new DataComponent(link);
			exiting("getValueAt");
			return dataCompont;
		} else if (columnName.equals(TYPE_ID)) {
			String type = "";
			if (job instanceof CronJob) {
				CronJob cronJob = (CronJob) job;
				if (cronJob.isDailySchedule())
					type =
						FormUtility.getResourceString(
							BaseForm.AUCTIONADMINBUNDLE,
							ScheduledTaskTypeListConstants.DAILY,
							session);
				else if (cronJob.isWeeklySchedule())
					type =
						FormUtility.getResourceString(
							BaseForm.AUCTIONADMINBUNDLE,
							ScheduledTaskTypeListConstants.WEEKLY,
							session);
			} else {
				type =
					FormUtility.getResourceString(
						BaseForm.AUCTIONADMINBUNDLE,
						ScheduledTaskTypeListConstants.REGULARLY,
						session);
			}

			exiting("getValueAt");
			return new DataString(type);
		} else if (columnName.equals(DESCRIPTION_ID)) {
			String value = job.getTaskName();
			exiting("getValueAt");
			return new DataString(value);

		} else if (columnName.equals(NEXTRUNTIME_ID)) {
			long time = job.getNextExecutionTime();
			String value = "";
			if (time != -1 && showNextRunTime(job))
				value =
					getConversionHelper().javaDatetoUILocaleSpecificString(
						new Timestamp(time));
			else
				value = "--";
			exiting("getValueAt");
			return new DataString(value);

		} else if (columnName.equals(LASTRUNTIME_ID)) {
			long time = job.getNextExecutionTime();
			String value = "";
			if (time != -1)
				value =
					getConversionHelper().javaDatetoUILocaleSpecificString(
						new Timestamp(time));
			exiting("getValueAt");
			return new DataString(value);

		} else if (columnName.equals(STATUS_ID)) {
			String status = "";
			if (job.isVirgin()) {
				status =
					FormUtility.getResourceString(
						BaseForm.AUCTIONADMINBUNDLE,
						NOTSCHEDULED,
						session);
			}
			if (job.isScheduled()) {
				if (getTaskHelper().isJobQueuedForExecution(job)){
					status =
						FormUtility.getResourceString(
							BaseForm.AUCTIONADMINBUNDLE,
							QUEUED,
							session);
				}
				else {
					status =
						FormUtility.getResourceString(
							BaseForm.AUCTIONADMINBUNDLE,
							SCHEDULED,
							session);
				}
				 
			} else if (job.isRunning()) {
				status =
					FormUtility.getResourceString(
						BaseForm.AUCTIONADMINBUNDLE,
						RUNNING,
						session);
			} else if (job.isPaused()) {
				status =
					FormUtility.getResourceString(
						BaseForm.AUCTIONADMINBUNDLE,
						PAUSED,
						session);
			}
			return new DataString(status);
		} else if (columnName.equals(LASTRUNRESULT_ID)) {
			int result = job.getLastCycleStatus();
			String strResult = "";
			switch (result) {
				case JobCycleStateManager.SCHEDULED :
					{
						strResult =
							FormUtility.getResourceString(
								BaseForm.AUCTIONADMINBUNDLE,
								SCHEDULED,
								session);
						break;
					}
				case JobCycleStateManager.STOPPED :
					{
						strResult =
							FormUtility.getResourceString(
								BaseForm.AUCTIONADMINBUNDLE,
								STOPPED,
								session);
						break;
					}
				case JobCycleStateManager.ERROR :
					{
						strResult =
							FormUtility.getResourceString(
								BaseForm.AUCTIONADMINBUNDLE,
								ERROR,
								session);
						break;
					}
				case JobCycleStateManager.EXECUTED :
					{
						strResult =
							FormUtility.getResourceString(
								BaseForm.AUCTIONADMINBUNDLE,
								EXECUTED,
								session);
						break;
					}
				case JobCycleStateManager.PAUSED :
					{
						strResult =
							FormUtility.getResourceString(
								BaseForm.AUCTIONADMINBUNDLE,
								PAUSED,
								session);
						break;
					}

			}
			exiting("getValueAt");
			return new DataString(strResult);

		} else if (columnName.equals(COUNT_ID)) {
			String count = String.valueOf(job.getNoOfTimesRun());
			exiting("getValueAt");
			return new DataString(count);
		}
		return new DataString("");
	}

	public int getRowCount() {
		if (dataColl == null)
			return 0;
		else {
			return dataColl.size();
		}
	}

	private ConversionHelper getConversionHelper() {
		if (null == helper) {
			UserSessionData userSession =
				UserSessionData.getUserSessionData(session);
			HelpManager manager =
				(HelpManager) userSession.getAttribute(HelpManager.HELPMANAGER);
			helper =
				(ConversionHelper) manager.getHelper(
					HelpManager.CONVERSIONHELPER);
		}
		return helper;
	}
	
	private SchedulerTaskHelper getTaskHelper() {
			if (null == taskHelper) {
				UserSessionData userSession =
					UserSessionData.getUserSessionData(session);
				HelpManager manager =
					(HelpManager) userSession.getAttribute(HelpManager.HELPMANAGER);
				taskHelper =
					(SchedulerTaskHelper) manager.getHelper(
						HelpManager.SCHEDULETASKHELPER);
			}
			return taskHelper;
		}
	
	private boolean showNextRunTime(Job job){
		boolean show = true;
		if (job.isVirgin() || job.isPaused())
			show = false;
		return show;
	}

}
