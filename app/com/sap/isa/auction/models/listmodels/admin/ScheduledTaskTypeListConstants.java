/*
 * Created on Sep 3, 2003
 *
 * To change the template for this generated file go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
package com.sap.isa.auction.models.listmodels.admin;

/**
 * Title:
 * Description:
 * @Copyright: 		Copyright (c) 2003
 * Company:
 * @author I802791
 *
 * To change the template for this generated type comment go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
public interface ScheduledTaskTypeListConstants {
	//	List box item, I13N text
	public static final String DAILY = "STTL.daily";
	public static final String WEEKLY = "STTL.weekly";
	public static final String REGULARLY = "STTL.regular";
//	public static final String MONTHLY = "xlst_monthly.ScheduledTaskType";
//	public static final String ONCE = "xlst_once.ScheduledTaskType";

	//List box itemId
	public static final String DAILY_ID = "DAILY_ID.ScheduledTaskType";
	public static final String WEEKLY_ID = "WEEKLY_ID.ScheduledTaskType";
	public static final String REGULARLY_ID = "REGULARLY_ID.ScheduledTaskType";
//	public static final String MONTHLY_ID = "MONTHLY_ID.ScheduledTaskType";
//	public static final String ONCE_ID = "ONCE_ID.ScheduledTaskType";
}
