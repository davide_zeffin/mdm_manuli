/*
 * Created on Sep 4, 2003
 *
 * To change the template for this generated file go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
package com.sap.isa.auction.models.listmodels.admin;

import javax.servlet.http.HttpSession;

import com.sap.isa.auction.actionforms.BaseForm;
import com.sap.isa.auction.models.listmodels.BaseListModel;
import com.sap.isa.auction.util.FormUtility;

/**
 * Title:
 * Description:
 * @Copyright: 		Copyright (c) 2003
 * Company:
 * @author I802791
 *
 * To change the template for this generated type comment go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
public class WeekdaysListModel
	extends BaseListModel
	implements WeekdaysListConstants {

	/**
	 * 
	 */
	public WeekdaysListModel(HttpSession session) {
		setData();
		this.setSelection(MONDAY_ID);
	}

	/**
	 * Set up the preset of listbox elements
	 */
	public void setData() {
		clearData();

		this.addItem(
			MONDAY_ID,
			FormUtility.getResourceString(
				BaseForm.AUCTIONADMINBUNDLE,
				MONDAY,
				session));
		this.addItem(
			TUESDAY_ID,
			FormUtility.getResourceString(
				BaseForm.AUCTIONADMINBUNDLE,
				TUESDAY,
				session));
		this.addItem(
			WEDNESDAY_ID,
			FormUtility.getResourceString(
				BaseForm.AUCTIONADMINBUNDLE,
				WEDNESDAY,
				session));
		this.addItem(
			THURSDAY_ID,
			FormUtility.getResourceString(
				BaseForm.AUCTIONADMINBUNDLE,
				THURSDAY,
				session));
		this.addItem(
			FRIDAY_ID,
			FormUtility.getResourceString(
				BaseForm.AUCTIONADMINBUNDLE,
				FRIDAY,
				session));
		this.addItem(
			SATURDAY_ID,
			FormUtility.getResourceString(
				BaseForm.AUCTIONADMINBUNDLE,
				SATURDAY,
				session));
		this.addItem(
			SUNDAY_ID,
			FormUtility.getResourceString(
				BaseForm.AUCTIONADMINBUNDLE,
				SUNDAY,
				session));
	}

	public String getSelected() {
		return getSelected();
	}

	public boolean isSingleSelection() {
		return true;
	}
}
