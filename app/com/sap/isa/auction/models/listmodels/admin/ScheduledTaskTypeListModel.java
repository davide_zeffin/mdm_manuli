/*
 * Created on Sep 3, 2003
 *
 * To change the template for this generated file go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
package com.sap.isa.auction.models.listmodels.admin;

import javax.servlet.http.HttpSession;

import com.sap.isa.auction.actionforms.BaseForm;
import com.sap.isa.auction.models.listmodels.BaseListModel;
import com.sap.isa.auction.util.FormUtility;

/**
 * Title:
 * Description:
 * @Copyright: 		Copyright (c) 2003
 * Company:
 * @author I802791
 *
 * To change the template for this generated type comment go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
public class ScheduledTaskTypeListModel
	extends BaseListModel
	implements ScheduledTaskTypeListConstants {

	/**
	 * 
	 */
	public ScheduledTaskTypeListModel(HttpSession session) {
		this.session = session;
		setData();
		this.setSelection(REGULARLY_ID);
	}
	/**
		 * Set up the preset of listbox elements
		 */
	public void setData() {
		clearData();
		this.addItem(
			REGULARLY_ID,
			FormUtility.getResourceString(
				BaseForm.AUCTIONADMINBUNDLE,
				REGULARLY,
				session));
		this.addItem(
			DAILY_ID,
			FormUtility.getResourceString(
				BaseForm.AUCTIONADMINBUNDLE,
				DAILY,
				session));
		this.addItem(
			WEEKLY_ID,
			FormUtility.getResourceString(
				BaseForm.AUCTIONADMINBUNDLE,
				WEEKLY,
				session));
//		this.addItem(
//			MONTHLY_ID,
//			FormUtility.getResourceString(
//				FormUtility.AUCTIONADMINBUNDLE,
//				MONTHLY,
//				session));
//		this.addItem(
//			ONCE_ID,
//			FormUtility.getResourceString(
//				FormUtility.AUCTIONADMINBUNDLE,
//				ONCE,
//				session));
	}

	public String getSelected() {
		return getSelected();
	}

	public boolean isSingleSelection() {
		return true;
	}
}
