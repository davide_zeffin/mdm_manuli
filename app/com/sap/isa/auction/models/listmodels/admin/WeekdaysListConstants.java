/*
 * Created on Sep 4, 2003
 *
 * To change the template for this generated file go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
package com.sap.isa.auction.models.listmodels.admin;

/**
 * Title:
 * Description:
 * @Copyright: 		Copyright (c) 2003
 * Company:
 * @author I802791
 *
 * To change the template for this generated type comment go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
public interface WeekdaysListConstants {
	//	I13N text, list box Item values
	public static final String MONDAY = "WDL.Monday";
	public static final String TUESDAY = "WDL.Tuesday";
	public static final String WEDNESDAY = "WDL.Wednesday";
	public static final String THURSDAY = "WDL.Thursday";
	public static final String FRIDAY = "WDL.Friday";
	public static final String SATURDAY = "WDL.Saturday";
	public static final String SUNDAY = "WDL.Sunday";

	//list box Item Id
	public static final String MONDAY_ID = "monday";
	public static final String TUESDAY_ID = "tuesday";
	public static final String WEDNESDAY_ID = "wednesday";
	public static final String THURSDAY_ID = "thursday";
	public static final String FRIDAY_ID = "friday";
	public static final String SATURDAY_ID = "saturday";
	public static final String SUNDAY_ID = "sunday";
}
