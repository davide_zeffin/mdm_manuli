package com.sap.isa.auction.models.listmodels.seller;

import javax.servlet.http.HttpSession;

import com.sap.isa.auction.actionforms.BaseForm;
import com.sap.isa.auction.bean.AuctionStatusEnum;
import com.sap.isa.auction.models.listmodels.BaseListModel;
import com.sap.isa.auction.util.FormUtility;

/**
 * Title:			Internet Sales Private Auctions
 * Description:
 * @Copyright: 		Copyright (c) 2004
 * Company:			SAPLabs LLC
 * @author
 * Created on:		Apr 27, 2004 
 */

public class AuctionStatusListModel
	extends BaseListModel
	implements AuctionStatusListConstants {

	public AuctionStatusListModel(HttpSession session) {
		this.session = session;
		setData();
		this.setSelection(LSTITM_ALL);
	}
	
	public void usePublishQueueOptions(){
		final String METHOD = "userPublishQueueOptions";
		entering(METHOD);		
		clearData();
		this.addItem(
			String.valueOf(AuctionStatusEnum.SCHEDULED.getValue()),
			FormUtility.getResourceString(
				BaseForm.AUCTIONSELLBUNDLE,
				LSTITM_SCHEDULED,
				session));
		this.addItem(
			String.valueOf(AuctionStatusEnum.OPEN.getValue()),
			FormUtility.getResourceString(
				BaseForm.AUCTIONSELLBUNDLE,
				LSTITM_OPEN,
				session));
		this.addItem(
			PUBLISHFAILED_ID,
			FormUtility.getResourceString(
				BaseForm.AUCTIONSELLBUNDLE,
						LSTITM_PUBFAILED,
						session));
		exiting(METHOD);
	}
	public void useManualWinnerOptions(){
		final String METHOD = "setData";
		entering(METHOD);		
		clearData();
		
		this.addItem(
			String.valueOf(AuctionStatusEnum.CLOSED.getValue()),
			FormUtility.getResourceString(
				BaseForm.AUCTIONSELLBUNDLE,
				LSTITM_CLOSED,
				session));
		this.addItem(
			String.valueOf(AuctionStatusEnum.PUBLISHED.getValue()),
			FormUtility.getResourceString(
				BaseForm.AUCTIONSELLBUNDLE,
				LSTITM_PUBLISHED,
				session));
		exiting(METHOD);		
	}
	
	/**
	 * Set up the preset of listbox elements
	 */
	public void setData() {
		clearData();

		this.addItem(
			String.valueOf(AuctionStatusEnum.NONE.getValue()),
			FormUtility.getResourceString(
				BaseForm.AUCTIONSELLBUNDLE,
				LSTITM_ALL,
				session));
		this.addItem(
			String.valueOf(AuctionStatusEnum.OPEN.getValue()),
			FormUtility.getResourceString(
				BaseForm.AUCTIONSELLBUNDLE,
				LSTITM_OPEN,
				session));
		this.addItem(
			String.valueOf(AuctionStatusEnum.SCHEDULED.getValue()),
			FormUtility.getResourceString(
				BaseForm.AUCTIONSELLBUNDLE,
				LSTITM_SCHEDULED,
				session));
		this.addItem(
			String.valueOf(AuctionStatusEnum.PUBLISHED.getValue()),
			FormUtility.getResourceString(
				BaseForm.AUCTIONSELLBUNDLE,
				LSTITM_PUBLISHED,
				session));
//		this.addItem(
//			String.valueOf(AuctionStatusEnum.WITHDRAWN.getValue()),
//			FormUtility.getResourceString(
//				BaseForm.AUCTIONSELLBUNDLE,
//				LSTITM_WITHDRAWN,
//				session));
		this.addItem(
			String.valueOf(AuctionStatusEnum.CLOSED.getValue()),
			FormUtility.getResourceString(
				BaseForm.AUCTIONSELLBUNDLE,
				LSTITM_CLOSED,
				session));
		this.addItem(
			String.valueOf(AuctionStatusEnum.NOT_FINALIZED.getValue()),
			FormUtility.getResourceString(
				BaseForm.AUCTIONSELLBUNDLE,
				LSTITM_NOTFINAL,
				session));
		this.addItem(
			String.valueOf(AuctionStatusEnum.FINALIZED.getValue()),
			FormUtility.getResourceString(
				BaseForm.AUCTIONSELLBUNDLE,
				LSTITM_FINALIZED,
				session));
		this.addItem(
			String.valueOf(AuctionStatusEnum.CHECKEDOUT.getValue()),
			FormUtility.getResourceString(
				BaseForm.AUCTIONSELLBUNDLE,
				LSTITM_INCOMPLETE,
				session));
		this.addItem(
			String.valueOf(AuctionStatusEnum.FINISHED.getValue()),
			FormUtility.getResourceString(
				BaseForm.AUCTIONSELLBUNDLE,
				LSTITM_FINISHED,
				session));
	}

	public String getSelected() {
		return getSelected();
	}

	public boolean isSingleSelection() {
		return true;
	}
}