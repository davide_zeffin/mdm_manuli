package com.sap.isa.auction.models.listmodels.seller;

/**
 * Title:			Internet Sales Private Auctions
 * Description:
 * @Copyright: 		Copyright (c) 2004
 * Company:			SAPLabs LLC
 * @author
 * Created on:		Apr 27, 2004 
 */

public interface AuctionStatusListConstants {

    //I13N text
	public static final String LSTITM_ALL          	= "AS.All";
    public static final String LSTITM_OPEN    		= "AS.Open";
	public static final String LSTITM_SCHEDULED		= "AS.Scheduled";
	public static final String LSTITM_PUBLISHED     = "AS.Published";
    public static final String LSTITM_CLOSED       	= "AS.Closed";
    public static final String LSTITM_FINALIZED    	= "AS.Finalized";
    public static final String LSTITM_WITHDRAWN		= "AS.Withdrawn";
    public static final String LSTITM_NOTFINAL		= "AS.NotFinalized";
	public static final String LSTITM_INCOMPLETE	= "AS.InCmCheckout";
	public static final String LSTITM_FINISHED		= "AS.Finished";
	
	public static final String LSTITM_PUBFAILED		= "AS.PublishFailed";
	public static final String PUBLISHFAILED_ID		= "PUBLISHFAILED_ID";
	
}