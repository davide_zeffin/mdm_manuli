package com.sap.isa.auction.models.listmodels.seller;

/**
 * Title:			Internet Sales Private Auctions
 * Description:
 * @Copyright: 		Copyright (c) 2004
 * Company:			SAPLabs LLC
 * @author
 * Created on:		Apr 27, 2004 
 */

public interface SearchCriteriaListConstants {

    //I13N text
    public static final String LSTITM_NAME         = "SC.Name";
    public static final String LSTITM_PRODUCTNAME  = "SC.ProductName";
    public static final String LSTITM_PUBLISHDATE  = "SC.PublishDate";
    public static final String LSTITM_CLOSINGDATE  = "SC.ClosingDate";
	public static final String LSTITM_TITLE 	   = "SC.Title";

}