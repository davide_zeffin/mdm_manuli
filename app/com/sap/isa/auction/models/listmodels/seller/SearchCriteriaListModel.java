package com.sap.isa.auction.models.listmodels.seller;

import javax.servlet.http.HttpSession;

import com.sap.isa.auction.actionforms.BaseForm;
import com.sap.isa.auction.actionforms.seller.AuctionDetailConstants;
import com.sap.isa.auction.models.listmodels.BaseListModel;
import com.sap.isa.auction.util.FormUtility;

/**
 * Title:			Internet Sales Private Auctions
 * Description:
 * @Copyright: 		Copyright (c) 2004
 * Company:			SAPLabs LLC
 * @author
 * Created on:		Apr 27, 2004 
 */

public class SearchCriteriaListModel
	extends BaseListModel
	implements SearchCriteriaListConstants {

	public SearchCriteriaListModel(HttpSession session) {
		this.session = session;
		setData();
		this.setSelection(LSTITM_NAME);
	}

	/**
	 * Set up the preset of listbox elements
	 */
	public void setData() {
		clearData();

		this.addItem(
			LSTITM_NAME,
			FormUtility.getResourceString(
				BaseForm.AUCTIONSELLBUNDLE,
				LSTITM_NAME,
				session));
		this.addItem(
			LSTITM_TITLE,
			FormUtility.getResourceString(
				BaseForm.AUCTIONSELLBUNDLE, 
				AuctionDetailConstants.TITLELABEL, session));		
		this.addItem(
			LSTITM_PRODUCTNAME,
			FormUtility.getResourceString(
				BaseForm.AUCTIONSELLBUNDLE,
				LSTITM_PRODUCTNAME,
				session));
		this.addItem(
			LSTITM_PUBLISHDATE,
			FormUtility.getResourceString(
				BaseForm.AUCTIONSELLBUNDLE,
				LSTITM_PUBLISHDATE,
				session));
		this.addItem(
			LSTITM_CLOSINGDATE,
			FormUtility.getResourceString(
				BaseForm.AUCTIONSELLBUNDLE,
				LSTITM_CLOSINGDATE,
				session));
	}

	public String getSelected() {
		return getSelected();
	}

	public boolean isSingleSelection() {
		return true;
	}
}