package com.sap.isa.auction.models.listmodels.seller;

import java.math.BigDecimal;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import javax.servlet.http.HttpSession;

import com.sap.isa.auction.actionforms.BaseForm;
import com.sap.isa.auction.actionforms.seller.AuctionDetailConstants;
import com.sap.isa.auction.bean.Winner;
import com.sap.isa.auction.helpers.HelpManager;
import com.sap.isa.auction.helpers.TargetGroupBPHelper;
import com.sap.isa.auction.models.listmodels.BaseListModel;
import com.sap.isa.auction.util.FormUtility;
import com.sap.isa.businesspartner.businessobject.BusinessPartner;
import com.sap.isa.core.UserSessionData;

/**
 * Title:			Internet Sales Private Auctions
 * Description:
 * @Copyright: 		Copyright (c) 2004
 * Company:			SAPLabs LLC
 * @author 
 * Created on: 		Jun 9, 2004
 */
public class OrdersListModel extends BaseListModel {
	private Map winnerMap;
	private TargetGroupBPHelper targetGroupHelper;
	
	public OrdersListModel(HttpSession session){
		this.session = session;
	}

	public void setData(Collection winners) {
		clearData();
		if (winners != null && !winners.isEmpty()) {
			winnerMap = new HashMap();
			Iterator it = winners.iterator();
			Winner winner = null;
			while (it.hasNext()) {
				winner = (Winner) it.next();
				StringBuffer sb = new StringBuffer();
				sb.append(winner.getOrderId());
				sb.append(" (");
				sb.append(
					FormUtility.getResourceString(
						BaseForm.AUCTIONSELLBUNDLE,
						AuctionDetailConstants.WINNERSTABTEXT,
						session));
				sb.append(" - ");
				BusinessPartner bp =
					getTargetGroupBPHelper().getBusinessPartner(
						winner.getUserId());
				if (bp != null){
					if (bp.getAddress().getFirstName() != null) {
						sb.append(bp.getAddress().getFirstName());
						sb.append(" ");
					}
					if (bp.getAddress().getLastName() != null) {
						sb.append(bp.getAddress().getLastName());
						sb.append(" ");
					}
				}
				sb.append(")");
				this.addItem(winner.getOrderGuid().getIdAsString(), sb.toString());
				winnerMap.put(winner.getOrderGuid().getIdAsString(), winner.getQuantity());
			}
		}
	}

	public boolean isSingleSelection() {
		return true;
	}

	public BigDecimal getQuantityForOrder(String orderGuid) {
		return (BigDecimal) winnerMap.get(orderGuid);
	}
	private TargetGroupBPHelper getTargetGroupBPHelper() {
		if (null == targetGroupHelper) {
			UserSessionData userSession =
				UserSessionData.getUserSessionData(session);
			HelpManager helpMang =
				(HelpManager) userSession.getAttribute(HelpManager.HELPMANAGER);
			targetGroupHelper =
				(TargetGroupBPHelper) helpMang.getHelper(
					HelpManager.TARGETGROUPHELPER);
		}
		return targetGroupHelper;
	}

}
