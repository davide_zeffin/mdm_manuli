package com.sap.isa.auction.models.listmodels;

import javax.servlet.http.HttpSession;

import com.sap.tc.logging.Category;
import com.sap.tc.logging.Location;
import com.sap.isa.auction.helpers.LogSupport;
import com.sap.isa.auction.logging.CategoryProvider;
import com.sap.isa.ui.components.listbox.ListboxModel;

/**
 * Title:
 * Description:
 * Copyright:    Copyright (c) 2003
 * Company:
 * @author
 * @version 1.0
 */

public class BaseListModel extends ListboxModel {

    protected static Location location;
    protected static Category category = CategoryProvider.getUICategory();

    public BaseListModel() {
    	super();
    	location = Location.getLocation(this.getClass());
    }

    protected void entering(String text)  {
		location = Location.getLocation(this.getClass());
    	LogSupport.entering(category , location , text , null);
    }

    protected void exiting(String text)  {
		location = Location.getLocation(this.getClass());
    	LogSupport.exiting(category , location , text , null);
    }

    protected void logInfo(String text)  {
		location = Location.getLocation(this.getClass());
    	LogSupport.logInfo(category , location , text);
    }

    protected void logInfo(String text , Throwable exc)  {
		location = Location.getLocation(this.getClass());
    	LogSupport.logError(category , location , text , exc);
    }
    
    public void setHttpSession(HttpSession session)  {
    	this.session = session;
    }
    
    public void clean()  {
    }
}