/*
 * Created on Apr 26, 2004
 *
 * To change the template for this generated file go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
package com.sap.isa.auction.actions;

import java.io.IOException;
import java.util.Locale;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.sap.isa.auction.actionforms.StartForm;
import com.sap.isa.auction.businessobject.b2x.AuctionBusinessObjectManager;
import com.sap.isa.auction.config.ScenarioConfigContainer;
import com.sap.isa.core.ActionServlet;
import com.sap.isa.core.BaseAction;
import com.sap.isa.core.Constants;
import com.sap.isa.core.UserSessionData;
import com.sap.isa.core.interaction.InteractionConfigContainer;
import com.sap.isa.core.logging.LogUtil;
import com.sap.isa.user.businessobject.UserBase;

/**
 * @author I803067
 *
 * To change the template for this generated type comment go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
// 03-30-05 Support for locale added for user to prevent exception during logon

public class StartAction extends BaseAction {

	/* (non-Javadoc)
	 * @see com.sap.isa.core.BaseAction#doPerform(org.apache.struts.action.ActionMapping, org.apache.struts.action.ActionForm, javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse)
	 */
	public ActionForward doPerform(
		ActionMapping mapping,
		ActionForm actionForm,
		HttpServletRequest request,
		HttpServletResponse response)
		throws IOException, ServletException {
			StartForm startForm = (StartForm) actionForm;
			String reqParam = request.getParameter(Constants.APP_INFO);

			if (!startForm.setup(request, servlet)) {
				return mapping.findForward(ActionForwards.ERROR);
			}

			//handle xcm scenario related initializations
			InteractionConfigContainer config = getInteractionConfig(request);
			UserSessionData userData =
				UserSessionData.getUserSessionData(request.getSession());
			ScenarioConfigContainer scenarioConfig =
				(ScenarioConfigContainer) userData.getAttribute(
					ScenarioConfigContainer.AUCTION_SCENARIO_CONFIG);
			if (scenarioConfig == null) {
				//create new scenarioconfig container and set in usersessiondata
				String scenario =
					getXCMConfigContainer(request).getScenarioName();
				scenarioConfig = new ScenarioConfigContainer(scenario);
				scenarioConfig.loadConfiguration(config);
				userData.setAttribute(
					ScenarioConfigContainer.AUCTION_SCENARIO_CONFIG,
					scenarioConfig);
			}
			request.getSession().setAttribute(ActionServlet.ENCODING_SKIP, "true");
			// get BOM
			AuctionBusinessObjectManager bom =
				(AuctionBusinessObjectManager)userData.getBOM(AuctionBusinessObjectManager.AUCTION_BOM);

			UserBase user = bom.createUserBase();
			// Locale was set in core.InitAction
			Locale loc=userData.getLocale();
			String language=null;
			if( loc != null)
				language = loc.getLanguage();
			if( language == null )
				log.error(LogUtil.APPS_USER_INTERFACE, "core.language.not.found");
			else
				user.setLanguage(language);

			if ((language != null) && (language.trim().length() > 0 )) {
			 // already set in InitAction	
			 // userSessionData.setLocale(new Locale(language,""));
				user.setLanguage(language);
			}
			if (reqParam != null) {
				String appUrl =
					"http://"
						+ request.getServerName()
						+ ":"
						+ request.getServerPort()
						+ request.getRequestURI()
						+ startForm.processAppInfoQueryString(request);
			}
			return mapping.findForward(ActionForwards.SUCCESS);	
	}

}
