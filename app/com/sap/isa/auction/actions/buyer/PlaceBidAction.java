/*
 * Created on May 17, 2004
 *
 * To change the template for this generated file go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
package com.sap.isa.auction.actions.buyer;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.sap.isa.auction.actionforms.buyer.AuctionDetailsForm;
import com.sap.isa.auction.actionforms.buyer.BaseForm;

/**
 * @author I803067
 *
 * To change the template for this generated type comment go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
public class PlaceBidAction extends BaseAction {

	/* (non-Javadoc)
	 * @see com.sap.isa.auction.actions.buyer.BaseAction#doService(org.apache.struts.action.ActionMapping, com.sap.isa.auction.actionforms.buyer.BaseForm, javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse)
	 */
	public ActionForward doService(
		ActionMapping mapping,
		BaseForm form,
		HttpServletRequest request,
		HttpServletResponse response)
		throws IOException, ServletException {
		AuctionDetailsForm auctionForm = (AuctionDetailsForm)form;
		
		String bidTyp = request.getParameter(AuctionDetailsForm.BIDTYPE);
		if(bidTyp != null)  {
			if(bidTyp.equals(AuctionDetailsForm.BIDDINGNOW))
				auctionForm.bid();
			else if(bidTyp.equals(AuctionDetailsForm.BUYINGNOW))
				if(auctionForm.buyNow())
					return mapping.findForward("buynowconf");
		}
		
		return mapping.findForward(ActionForwards.SUCCESS);
	}

}
