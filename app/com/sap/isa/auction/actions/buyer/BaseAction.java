/*
 * Created on May 4, 2004
 *
 * To change the template for this generated file go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
package com.sap.isa.auction.actions.buyer;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.sap.isa.auction.actionforms.buyer.BaseForm;

/**
 * @author I803067
 *
 * To change the template for this generated type comment go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
public class BaseAction extends com.sap.isa.core.BaseAction  {

	protected void preProcessing(BaseForm form)  {
		form.preprocessing();	
	}

	public final ActionForward doPerform(
		ActionMapping mapping,
		ActionForm form,
		HttpServletRequest request,
		HttpServletResponse response)
		throws IOException, ServletException {
		
		BaseForm baseForm = (BaseForm)form;
		baseForm.setSession(request.getSession());
		baseForm.setRequest(request);
		if(!((BaseForm)form).isAuctionEnabled())  {
			return mapping.findForward(ActionForwards.SUCCESS);
		}
		preProcessing(baseForm);
		ActionForward forward = doService(mapping , (BaseForm)form , request , response);
		postProcessing(baseForm); 
		return forward;
	}
	
	protected void postProcessing(BaseForm form)  {
		form.postProcessing();
	}
	
	public ActionForward doService(
		ActionMapping mapping,
		BaseForm form,
		HttpServletRequest request,
		HttpServletResponse response)
		throws IOException, ServletException {
		return mapping.findForward(mapping.getInput());
	}
	

}
