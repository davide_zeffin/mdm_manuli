/*
 * Created on Jun 11, 2004
 *
 * To change the template for this generated file go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
package com.sap.isa.auction.actions.buyer;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.sap.isa.auction.actionforms.buyer.BaseForm;
import com.sap.isa.auction.actionforms.buyer.CheckoutAuctionsForm;

/**
 * @author I803067
 *
 * To change the template for this generated type comment go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
public class AfterOrderCheckoutAction extends BaseAction  {

	/* (non-Javadoc)
	 * @see com.sap.isa.auction.actions.buyer.BaseAction#doService(org.apache.struts.action.ActionMapping, com.sap.isa.auction.actionforms.buyer.BaseForm, javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse)
	 */
	public ActionForward doService(
		ActionMapping mapping,
		BaseForm form,
		HttpServletRequest request,
		HttpServletResponse response)
		throws IOException, ServletException {
		
		CheckoutAuctionsForm checkoutAuctionForm = (CheckoutAuctionsForm)form;
		checkoutAuctionForm.checkoutPostProcessing();
		return mapping.findForward(ActionForwards.SUCCESS);
	}

}
