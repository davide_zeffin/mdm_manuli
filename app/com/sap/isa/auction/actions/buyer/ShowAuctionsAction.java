/*
 * Created on May 5, 2004
 *
 * To change the template for this generated file go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
package com.sap.isa.auction.actions.buyer;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.sap.isa.auction.actionforms.buyer.AuctionsFormConstants;
import com.sap.isa.auction.actionforms.buyer.BaseForm;
import com.sap.isa.auction.actionforms.buyer.AuctionsForm;

/**
 * @author I803067
 *
 * To change the template for this generated type comment go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
public class ShowAuctionsAction extends BaseAction  {


	/* (non-Javadoc)
	 * @see com.sap.isa.auction.actions.buyer.BaseAction#doService(org.apache.struts.action.ActionMapping, com.sap.isa.auction.actionforms.buyer.BaseForm, javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse)
	 */
	public ActionForward doService(
		ActionMapping mapping,
		BaseForm form,
		HttpServletRequest request,
		HttpServletResponse response)
		throws IOException, ServletException {
		AuctionsForm auctionsForm = (AuctionsForm)form;
		String auctionId = request.getParameter(AuctionsForm.AUCTIONID);
		if (auctionId != null && !auctionId.equals("")) {
			request.setAttribute(AuctionsForm.AUCTIONID , request.getParameter(AuctionsForm.AUCTIONID));
			if (request.getParameter(AuctionsForm.PRODUCTID) != null) {
				request.setAttribute(AuctionsForm.PRODUCTID , request.getParameter(AuctionsForm.PRODUCTID));
			}
			return mapping.findForward(ActionForwards.AUCTIONDETAIL);
		}
        
		String navigation = request.getParameter(AuctionsForm.NAVIGATION);
        
		if (navigation != null && navigation.equals(AuctionsForm.NAVIGATION)) {
            // We have to read always the complete auctions otherwise during paging sometimes an
            // "javax.jdo.JDOFatalUserException: PersistenceManager was closed" was raised
            if (request.getParameter(AuctionsForm.PRODUCTID) != null && 
                request.getParameter(AuctionsForm.PRODUCTID).length() > 0) {
                // We have to read always the complete auctions otherwise during paging sometimes an
                // "javax.jdo.JDOFatalUserException: PersistenceManager was closed" was raised
                auctionsForm.searchAuctionsForProduct(request.getParameter(AuctionsForm.PRODUCTID));
            }
            //auctionsForm.setPageContext();   -- old might throw javax.jdo.JDOFatalUserException
		}  
        else  {
			String productId = request.getParameter(AuctionsForm.PRODUCTID);
			if (productId != null && !productId.equals("")) {
                auctionsForm.searchAuctionsForProduct(request.getParameter(AuctionsForm.PRODUCTID));
			}
			else {
                auctionsForm.setProductId("");
			}	
		}
        
		if (auctionsForm.getProductId() != null && !auctionsForm.getProductId().equals("")) {
			return mapping.findForward(ActionForwards.PRODUCTAUCTIONS);
		}
        
		String context = request.getParameter(AuctionsForm.STATUSOPTIONTEXT);
        
		if (null == context) {
			context = auctionsForm.getContextInfo(); 
		}  
        else  {
            // this is needed to know where we are coming from during paging, so save it in the auctionsForm
            // than it is set as hidden field in the auctionlist.jsp, womlist.jsp and thus available if paging is done
            auctionsForm.setRcStatus(context);
            // Every time paging is triggered, we have to re-execute the query again, because otherwise 
            // javax.jdo.JDOFatalUserException might occur
            
            if (navigation != null && navigation.equals(AuctionsForm.NAVIGATION)) {
                // are we doing paging? Than we have to determine the state from the auctionForms
                context = auctionsForm.getContextInfo(); 
            }
			if (context.equals(AuctionsForm.OPTION_PARTICIPATED)) {
                context = request.getParameter(AuctionsForm.PARTICIPATIONOPTIONTEXT);
			}
			else if (context.equals(AuctionsFormConstants.OPTION_WON)) { 
                context = request.getParameter(AuctionsForm.WONOPTIONTEXT);
			}
				
			auctionsForm.searchAuctionsParticipated(context);
		}
        
		if (context != null)  {
			if (context.equals(AuctionsForm.OPTION_NOCHECKOUT)) {
				return mapping.findForward(ActionForwards.CHECKOUT);
			}  
            else if (context.equals(AuctionsForm.OPTION_CHECKEDOUT)) {
				return mapping.findForward(ActionForwards.SUCCESS);
			}
		}
		
		return mapping.findForward(ActionForwards.SUCCESS);
	}

}
