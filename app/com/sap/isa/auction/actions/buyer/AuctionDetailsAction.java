/*
 * Created on May 6, 2004
 *
 * To change the template for this generated file go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
package com.sap.isa.auction.actions.buyer;

import java.io.IOException;
import java.util.Iterator;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.sap.isa.auction.actionforms.buyer.AuctionDetailsForm;
import com.sap.isa.auction.actionforms.buyer.AuctionsForm;
import com.sap.isa.auction.actionforms.buyer.BaseForm;
import com.sap.isa.core.SessionConst;
import com.sap.isa.core.UserSessionData;
import com.sap.isa.core.util.StartupParameter;
import com.sap.isa.isacore.DocumentHandler;

/**
 * @author I803067
 *
 * To change the template for this generated type comment go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
public class AuctionDetailsAction extends BaseAction {

	/* (non-Javadoc)
	 * @see com.sap.isa.auction.actions.buyer.BaseAction#doService(org.apache.struts.action.ActionMapping, com.sap.isa.auction.actionforms.buyer.BaseForm, javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse)
	 */
	public ActionForward doService(
		ActionMapping mapping,
		BaseForm form,
		HttpServletRequest request,
		HttpServletResponse response)
		throws IOException, ServletException {

		AuctionDetailsForm detailsForm = (AuctionDetailsForm) form;
		UserSessionData userSessionData =
			UserSessionData.getUserSessionData(request.getSession());
		StartupParameter startupParameter =
			(StartupParameter) userSessionData.getAttribute(
				SessionConst.STARTUP_PARAMETER);
		String auctionId = null;
		String fwd = ActionForwards.SUCCESS;
		
		if (startupParameter != null
			&& (auctionId =
				startupParameter.getParameterValue(AuctionsForm.AUCTIONID))
				!= null
			&& auctionId.trim().length() > 0) {
			Iterator iterate = startupParameter.iterator();
			while (iterate.hasNext()) {
				if (((StartupParameter.Parameter) iterate.next())
					.getName()
					.equals(AuctionsForm.AUCTIONID)) {
					iterate.remove();
				}
			}
			if (auctionId != null && !auctionId.equals("")) {
				detailsForm.setInvitation(true);
				if (detailsForm.loadAuction(auctionId))
					return mapping.findForward(ActionForwards.SUCCESS_CAT);
			}
			return mapping.findForward(ActionForwards.UPDATEDOCUMENT);
		}
		auctionId = (String) request.getAttribute(AuctionsForm.AUCTIONID);
		detailsForm.setInvitation(false);
		if (auctionId != null) {
			detailsForm.setAuction(auctionId);
			detailsForm.setProductId(
				request.getParameter(AuctionsForm.PRODUCTID));
		} else {
			String bidTyp = request.getParameter(AuctionDetailsForm.BIDTEXT);
			if (bidTyp != null) {
				if (bidTyp.equals(AuctionDetailsForm.BIDDINGNOW))
					detailsForm.bid();
				else if (bidTyp.equals(AuctionDetailsForm.BUYINGNOW))
					detailsForm.buyNow();
			}
		}
		
		DocumentHandler documentHandler = (DocumentHandler)
						userSessionData.getAttribute(SessionConst.DOCUMENT_HANDLER);

		if (documentHandler.isCatalogOnTop()) {
			fwd = ActionForwards.SUCCESS_CAT;
		}
		
		return mapping.findForward(fwd);

	}

}
