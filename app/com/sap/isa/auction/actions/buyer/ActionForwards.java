/*
 * Created on May 5, 2004
 *
 * To change the template for this generated file go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
package com.sap.isa.auction.actions.buyer;

/**
 * @author I803067
 *
 * To change the template for this generated type comment go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
public interface ActionForwards {
	
	public static final String SUCCESS				  = "success";
	public static final String SUCCESS_CAT			  = "successCat";
	public static final String NOAUCTIONS			  = "noauctions";
	public static final String SHOWAUCTIONSFORPRODUCT = "auctions";
	public static final String PRODUCTAUCTIONS		  = "product";
	public static final String AUCTIONDETAIL		  = "details";
	public static final String CHECKOUT				  = "checkout";
	public static final String CHECKEDOUT			  = "checkedout";
	public static final String UPDATEDOCUMENT		  = "updateDocument";
}
