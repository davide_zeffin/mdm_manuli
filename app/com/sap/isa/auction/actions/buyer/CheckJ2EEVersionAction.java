package com.sap.isa.auction.actions.buyer;

/**
 * Title:			Internet Sales Private Auctions
 * Description:
 * @Copyright: 		Copyright (c) 2004
 * Company:			SAPLabs LLC
 * @author 
 * Created on: 		Jun 17, 2004
 */

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.sap.isa.core.EnvironmentManager;

public class CheckJ2EEVersionAction extends com.sap.isa.core.BaseAction { 
	private static final String CHECKFORAUCTION = "checkForAuction";
	public final ActionForward doPerform(
		ActionMapping mapping,
		ActionForm form,
		HttpServletRequest request,
		HttpServletResponse response)
		throws IOException, ServletException {
		
		String forward = CHECKFORAUCTION;
		if (EnvironmentManager.isSAPJ2EE620())
			forward = ActionForwards.SUCCESS;
		return mapping.findForward(forward);
	}
}
