/*
 * Created on Aug 31, 2004
 *
 * To change the template for this generated file go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
package com.sap.isa.auction.actions.buyer;

/**
 * @author I802791
 *
 * To change the template for this generated type comment go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.sap.isa.auction.actionforms.buyer.AuctionDetailsForm;
import com.sap.isa.auction.actionforms.buyer.BaseForm;
import com.sap.isa.core.SessionConst;
import com.sap.isa.core.UserSessionData;
import com.sap.isa.isacore.DocumentHandler;

public class RefreshAuctionAction extends BaseAction {

	public ActionForward doService(
		ActionMapping mapping,
		BaseForm form,
		HttpServletRequest request,
		HttpServletResponse response)
		throws IOException, ServletException {
			
		String fwd = ActionForwards.SUCCESS;
		UserSessionData userSessionData =
			UserSessionData.getUserSessionData(request.getSession());

		AuctionDetailsForm detailsForm = (AuctionDetailsForm) form;
		detailsForm.refresh();
		
		DocumentHandler documentHandler = (DocumentHandler)
						userSessionData.getAttribute(SessionConst.DOCUMENT_HANDLER);

		if (documentHandler.isCatalogOnTop()) {
			fwd = ActionForwards.SUCCESS_CAT;
		}
		
		return mapping.findForward(fwd);

	}

}
