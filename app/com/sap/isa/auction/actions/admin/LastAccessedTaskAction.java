/*
 * Created on Sep 8, 2003
 *
 * To change the template for this generated file go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
package com.sap.isa.auction.actions.admin;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.sap.isa.auction.actionforms.BaseForm;
import com.sap.isa.auction.actionforms.admin.ScheduledTasksForm;
import com.sap.isa.auction.actions.BaseAction;
import com.sap.isa.auction.helpers.SchedulerTaskHelper;
import com.sap.isa.auction.models.tablemodels.admin.TaskTableConstants;

/**
 * Title:
 * Description:
 * @Copyright: 		Copyright (c) 2003
 * Company:
 * @author I802791
 *
 * To change the template for this generated type comment go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
public class LastAccessedTaskAction extends BaseAction {

	/**
		 * 
		 */
	public LastAccessedTaskAction() {
		super();
	}

	protected ActionForward doService(
		ActionMapping mapping,
		BaseForm actionForm,
		HttpServletRequest request,
		HttpServletResponse response)
		throws java.io.IOException, javax.servlet.ServletException {
		
		String forward = ActionForwards.EMPTY;
		ScheduledTasksForm tasksForm = (ScheduledTasksForm) actionForm;
		String id = tasksForm.getLastAccessedTaskId();
		String rowId = tasksForm.getLastAccessedRowId();
		if (id != null) {
			request.setAttribute(TaskTableConstants.TASK_ID, id);
			forward = ActionForwards.SUCCESS;
		}
		else if (rowId != null ) {
			request.setAttribute(TaskTableConstants.ROW_ID, rowId);
			forward = ActionForwards.SUCCESS;
		}
		
		return mapping.findForward(forward);
	}
}
