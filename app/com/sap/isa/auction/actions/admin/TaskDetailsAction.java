/*
 * Created on Sep 8, 2003
 *
 * To change the template for this generated file go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
package com.sap.isa.auction.actions.admin;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.sap.isa.auction.actionforms.BaseForm;
import com.sap.isa.auction.actionforms.admin.TaskDetailsForm;
import com.sap.isa.auction.actionforms.admin.TaskDetailsFormConstants;
import com.sap.isa.auction.actions.BaseAction;
import com.sap.isa.auction.actions.admin.buttonEH.EditScheduledTaskEH;
import com.sap.isa.auction.actions.admin.buttonEH.PauseTaskEH;
import com.sap.isa.auction.actions.admin.buttonEH.ResumeTaskEH;
import com.sap.isa.auction.actions.admin.buttonEH.RunTaskNowEH;
import com.sap.isa.auction.actions.admin.buttonEH.SaveCancelTaskModificationEH;
import com.sap.isa.auction.models.tablemodels.admin.TaskTableConstants;


/**
 * Title:
 * Description:
 * @Copyright: 		Copyright (c) 2003
 * Company:
 * @author I802791
 *
 * To change the template for this generated type comment go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
public class TaskDetailsAction extends BaseAction {

	/**
	 * 
	 */
	public TaskDetailsAction() {
		super();
	}
	protected ActionForward doService(
		ActionMapping mapping,
		BaseForm actionForm,
		HttpServletRequest request,
		HttpServletResponse response)
		throws javax.servlet.ServletException, java.io.IOException {

		TaskDetailsForm taskForm = (TaskDetailsForm) actionForm;
		Object rowId = request.getParameter(TaskTableConstants.ROW_ID);
		if (rowId == null)
			rowId = request.getAttribute(TaskTableConstants.ROW_ID);
		if (rowId != null) {
			taskForm.initializeByRowId((String) rowId);
		} else {
			Object taskId = request.getParameter(TaskTableConstants.TASK_ID);
			if (taskId == null)
				taskId = request.getAttribute(TaskTableConstants.TASK_ID);
			String taskIdStr = taskId != null ? (String) taskId : null;
			taskForm.initializeByJobId(taskIdStr);
		}

		return mapping.findForward(ActionForwards.SUCCESS);
	}

	protected void registerEventHandlers() {
		super.registerEventHandlers();
		registerEventHandler(
			TaskDetailsFormConstants.PAUSEBTN_ID,
			new PauseTaskEH());
		registerEventHandler(
			TaskDetailsFormConstants.RESUMEBTN_ID,
			new ResumeTaskEH());
		registerEventHandler(
			TaskDetailsFormConstants.RUNNOWBTN_ID,
			new RunTaskNowEH());
		registerEventHandler(
			TaskDetailsFormConstants.SAVEBTN_ID,
			new SaveCancelTaskModificationEH());
		registerEventHandler(
			TaskDetailsFormConstants.CANCELBTN_ID,
			new SaveCancelTaskModificationEH());
		registerEventHandler(
			TaskDetailsFormConstants.EDITBTN_ID,
			new EditScheduledTaskEH());
	}
}
