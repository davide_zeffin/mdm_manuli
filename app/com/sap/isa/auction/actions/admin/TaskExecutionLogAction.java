/*
 * Created on Oct 22, 2003
 *
 * To change the template for this generated file go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
package com.sap.isa.auction.actions.admin;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.sap.isa.auction.actionforms.BaseForm;
import com.sap.isa.auction.actionforms.admin.TaskExecutionLogForm;
import com.sap.isa.auction.actions.BaseAction;
import com.sap.isa.auction.models.tablemodels.admin.TaskTableConstants;

/**
 * Title:
 * Description:
 * @Copyright: 		Copyright (c) 2003
 * Company:
 * @author I802791
 *
 * To change the template for this generated type comment go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
public class TaskExecutionLogAction extends BaseAction {

	/**
	 * 
	 */
	protected ActionForward doService(
		ActionMapping mapping,
		BaseForm actionForm,
		HttpServletRequest request,
		HttpServletResponse response)
		throws javax.servlet.ServletException, java.io.IOException {

		TaskExecutionLogForm logForm =
			(TaskExecutionLogForm) actionForm;
		String row = (String)request.getParameter(TaskTableConstants.ROW_ID);
		logForm.initializeExecutionDetails(Integer.parseInt(row.trim()));
		return mapping.findForward(ActionForwards.SUCCESS);
	}
}
