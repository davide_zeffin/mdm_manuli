/*
 * Created on Sep 8, 2003
 *
 * To change the template for this generated file go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
package com.sap.isa.auction.actions.admin;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

//struts imports
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.sap.isa.auction.actionforms.BaseForm;
import com.sap.isa.auction.actionforms.admin.ScheduledTasksForm;
import com.sap.isa.auction.actionforms.admin.ScheduledTasksFormConstants;
import com.sap.isa.auction.actions.ActionForwards;
import com.sap.isa.auction.actions.BaseAction;
import com.sap.isa.auction.actions.admin.buttonEH.RefreshScheduledTasksEH;
import com.sap.isa.auction.actions.admin.buttonEH.RunScheduledTasksNowEH;
import com.sap.isa.auction.actions.admin.buttonEH.ScheduleTaskEH;
import com.sap.isa.auction.actions.admin.buttonEH.PauseScheduledTasksEH;
import com.sap.isa.auction.actions.admin.buttonEH.ResumeScheduledTasksEH;
import com.sap.isa.auction.models.tablemodels.admin.TaskTableConstants;
import com.sap.isa.ui.eventhandlers.TableEventHandler;

/**
 * Title:
 * Description:
 * @Copyright: 		Copyright (c) 2003
 * Company:
 * @author I802791
 *
 * To change the template for this generated type comment go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
public class ScheduledTasksAction extends BaseAction {

	/**
	 * 
	 */
	protected ActionForward doService(ActionMapping mapping,
									BaseForm actionForm,
									HttpServletRequest request,
									HttpServletResponse response)
									throws javax.servlet.ServletException, java.io.IOException {

		ScheduledTasksForm tasksForm = (ScheduledTasksForm)actionForm;
		Object obj = request.getAttribute(TaskTableConstants.TASK_ID);
		tasksForm.initializeFields();
		return mapping.findForward(ActionForwards.SUCCESS);
	}

	protected void registerEventHandlers() {
		super.registerEventHandlers();
		registerEventHandler(ScheduledTasksFormConstants.TASKTABLEVIEW_ID , new TableEventHandler());        
		registerEventHandler(ScheduledTasksFormConstants.PAUSEBTN_ID, new PauseScheduledTasksEH());
		registerEventHandler(ScheduledTasksFormConstants.RESUMEBTN_ID, new ResumeScheduledTasksEH());
		registerEventHandler(ScheduledTasksFormConstants.RUNNOWBTN_ID, new RunScheduledTasksNowEH());
		registerEventHandler(ScheduledTasksFormConstants.REFRESHBTN_ID, new RefreshScheduledTasksEH());
		registerEventHandler(ScheduledTasksFormConstants.SCHEDULEBTN_ID, new ScheduleTaskEH());	
	}

}
