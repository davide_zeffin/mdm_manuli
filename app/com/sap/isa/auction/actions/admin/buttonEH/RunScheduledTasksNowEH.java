/*
 * Created on Sep 4, 2003
 *
 * To change the template for this generated file go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
package com.sap.isa.auction.actions.admin.buttonEH;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.sap.isa.auction.actionforms.admin.ScheduledTasksForm;
import com.sap.isa.auction.actions.admin.ActionForwards;
import com.sap.isa.ui.controllers.ActionForm;
import com.sap.isa.ui.eventhandlers.ButtonClickedEventHandler;
import com.sapportals.htmlb.event.ButtonClickEvent;

/**
 * Title:
 * Description:
 * @Copyright: 		Copyright (c) 2003
 * Company:
 * @author I802791
 *
 * To change the template for this generated type comment go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
public class RunScheduledTasksNowEH extends ButtonClickedEventHandler {
	public ActionForward buttonClickedEvent(
			ActionMapping mapping,
			ActionForm form,
			ButtonClickEvent event,
			HttpServletRequest request,
			HttpServletResponse response)
			throws java.io.IOException, javax.servlet.ServletException {

		ScheduledTasksForm tasksForm = (ScheduledTasksForm)form;
		tasksForm.runTasksNow();		
		return mapping.findForward(ActionForwards.SUCCESS);		
	}
}
