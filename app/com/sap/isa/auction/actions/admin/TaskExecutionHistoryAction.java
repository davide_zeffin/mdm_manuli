/*
 * Created on Oct 22, 2003
 *
 * To change the template for this generated file go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
package com.sap.isa.auction.actions.admin;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.sap.isa.auction.actionforms.BaseForm;
import com.sap.isa.auction.actionforms.admin.TaskDetailsFormConstants;
import com.sap.isa.auction.actionforms.admin.TaskExecutionHistoryForm;
import com.sap.isa.auction.actions.BaseAction;
import com.sap.isa.auction.actions.admin.buttonEH.ClearExecutionHistoryEH;
import com.sap.isa.auction.actions.admin.buttonEH.RefreshExecutionHistoryEH;
import com.sap.isa.ui.eventhandlers.TableEventHandler;

/**
 * Title:
 * Description:
 * @Copyright: 		Copyright (c) 2003
 * Company:
 * @author I802791
 *
 * To change the template for this generated type comment go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
public class TaskExecutionHistoryAction extends BaseAction {

	/**
	 * 
	 */
	protected ActionForward doService(
		ActionMapping mapping,
		BaseForm actionForm,
		HttpServletRequest request,
		HttpServletResponse response)
		throws javax.servlet.ServletException, java.io.IOException {

		TaskExecutionHistoryForm historyForm =
			(TaskExecutionHistoryForm) actionForm;
		historyForm.initializeExecutionHistory();
		return mapping.findForward(ActionForwards.SUCCESS);
	}
	
	protected void registerEventHandlers() {
		super.registerEventHandlers();
		registerEventHandler(TaskDetailsFormConstants.HISTORYTABLEVIEW_ID , new TableEventHandler()); 
		registerEventHandler(TaskDetailsFormConstants.CLEARBTN_ID , new ClearExecutionHistoryEH()); 
		registerEventHandler(TaskDetailsFormConstants.REFRESHBTN_ID , new RefreshExecutionHistoryEH());                             
	}
}
