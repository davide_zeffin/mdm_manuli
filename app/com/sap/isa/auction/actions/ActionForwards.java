/*
 * Created on Apr 26, 2004
 *
 * To change the template for this generated file go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
package com.sap.isa.auction.actions;

/**
 * @author I803067
 *
 * To change the template for this generated type comment go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
public interface ActionForwards {
	public static final String SUCCESS 	= "success";
	public static final String ERROR	= "auctionerror";
	
	public static final String MAIN 	= "main";
	public static final String SHOWLOADINGURL = "/auction/container.jsp?_load_Page=";
	
	public static final String AMBERSANDESC = "%26";
	public static final String QUESTIONESC	= "%3F";
	public static final String EQUALESC = "%3D";
	
	public static final String FORWARD = "_forward";
	
	public static final String SHOPS   = "shops";
	public static final String ADMIN   = "admin";
	public static final String EMPTY   = "empty";
}
