/*
 * Created on May 4, 2004
 *
 * To change the template for this generated file go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
package com.sap.isa.auction.actions;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.sap.isa.auction.actionforms.BaseForm;
import com.sap.isa.auction.actionforms.Form;

/**
 * @author I803067
 *
 * To change the template for this generated type comment go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
public class BaseAction extends Action  {


	/* (non-Javadoc)
	 * @see com.sap.isa.auction.actions._BaseAction#doService(org.apache.struts.action.ActionMapping, com.sap.isa.auction.actionforms._BaseForm, javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse)
	 */
	protected final ActionForward doService(
		ActionMapping mapping,
		Form actionForm,
		HttpServletRequest request,
		HttpServletResponse response)
		throws ServletException, IOException {
		// TODO Auto-generated method stub
		return doService(mapping, (BaseForm)actionForm, request, response);
	}

	protected ActionForward doService(
		ActionMapping mapping,
		BaseForm actionForm,
		HttpServletRequest request,
		HttpServletResponse response)
		throws ServletException, IOException {
		// TODO Auto-generated method stub
		return mapping.findForward(mapping.getInput());
	}
}
