/*
 * Created on Jun 11, 2004
 *
 * To change the template for this generated file go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
package com.sap.isa.auction.actions.seller.linkEH;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.sap.isa.auction.actionforms.seller.ProductCatalogForm;
import com.sap.isa.auction.actions.seller.ActionForwards;
import com.sap.isa.ui.controllers.ActionForm;
import com.sap.isa.ui.eventhandlers.LinkClickedEventHandler;
import com.sapportals.htmlb.event.LinkClickEvent;

/**
 * @author I803067
 *
 * To change the template for this generated type comment go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
public class CatalogCategoryEH extends LinkClickedEventHandler  {

	/* (non-Javadoc)
	 * @see com.sap.isa.ui.eventhandlers.LinkClickedEventHandler#linkClickedEvent(org.apache.struts.action.ActionMapping, com.sap.isa.ui.controllers.ActionForm, com.sapportals.htmlb.event.LinkClickEvent, javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse)
	 */
	public ActionForward linkClickedEvent(
		ActionMapping mapping,
		ActionForm form,
		LinkClickEvent event,
		HttpServletRequest request,
		HttpServletResponse response)
		throws IOException, ServletException {
		ProductCatalogForm catalogForm = (ProductCatalogForm)form;
		catalogForm.expandCategory(event.getComponentName() , event.getAction());
		return mapping.findForward(ActionForwards.SUCCESS);
	}

}
