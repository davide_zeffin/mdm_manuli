package com.sap.isa.auction.actions.seller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.sap.isa.auction.actionforms.BaseForm;
import com.sap.isa.auction.actionforms.seller.AuctionSearchForm;
import com.sap.isa.auction.actionforms.seller.AuctionSearchFormConstants;
import com.sap.isa.auction.actions.BaseAction;
import com.sap.isa.auction.models.tablemodels.seller.AuctionsTableModel;

/**
 * Title:
 * Description:
 * Copyright:    Copyright (c) 2003
 * Company:
 * @author
 * @version 1.0
 */

public class LastAuctionFetchedAction extends BaseAction {

	public LastAuctionFetchedAction() {
	}

	protected ActionForward doService(
		ActionMapping mapping,
		BaseForm actionForm,
		HttpServletRequest request,
		HttpServletResponse response)
		throws java.io.IOException, javax.servlet.ServletException {
			
		AuctionSearchForm searchForm = (AuctionSearchForm) actionForm;
		Integer fromSrc = new Integer(AuctionSearchFormConstants.SRC_MONITORING);
		String param = request.getParameter(AuctionSearchFormConstants.FROMSRCPARAM);
		if (param!= null)
			fromSrc = Integer.valueOf(param);

		String id = searchForm.getLastAccessedAuctonId(fromSrc);
		if (id != null && !id.equals("")) {
			request.setAttribute(AuctionsTableModel.PARAM_AUCTIONID, id);
			request.setAttribute(AuctionSearchFormConstants.FROMSRCPARAM, fromSrc);
			
			// Choose the correct detail
			if (AuctionSearchFormConstants.SRC_MANUALWINNERQUEUE == 
					fromSrc.intValue()){
				return mapping.findForward(ActionForwards.MWINFO);				
			} else {
				return mapping.findForward(ActionForwards.DETAILS);
			}			
		}
		return mapping.findForward(ActionForwards.EMPTY);
	}
}
