package com.sap.isa.auction.actions.seller;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.sap.isa.auction.actionforms.BaseForm;
import com.sap.isa.auction.actionforms.seller.AuctionDetailsForm;
import com.sap.isa.auction.actionforms.seller.AuctionSearchFormConstants;
import com.sap.isa.auction.actions.BaseAction;

import com.sap.isa.auction.models.tablemodels.seller.AuctionsTableConstants;

/**
 * Title:			Internet Sales Private Auctions
 * Description:
 * @Copyright: 		Copyright (c) 2004
 * Company:			SAPLabs LLC
 * Created on:		May 6, 2004
 */
public class AuctionDetailsBaseAction extends BaseAction {

	
	/* (non-Javadoc)
	 * @see com.sap.isa.auction.actions.BaseAction#doService(org.apache.struts.action.ActionMapping, com.sap.isa.auction.actionforms.BaseForm, javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse)
	 */
	protected ActionForward doService(
		ActionMapping mapping,
		BaseForm actionForm,
		HttpServletRequest request,
		HttpServletResponse response)
		throws ServletException, IOException {
		
		AuctionDetailsForm detailsForm = (AuctionDetailsForm) actionForm;
		String auctionId = "";
		Integer fromSrc = new Integer(AuctionSearchFormConstants.SRC_MONITORING);
//		if (actionForm instanceof AuctionDetailsForm){
//			AuctionDetailsForm detailsForm = (AuctionDetailsForm) actionForm;
//			String auctionId = "";
//		if(request.getParameter(AuctionDetailsForm.SHOWSAVEDAUCTION) != null)  {
//			detailsForm.showSavedAuction();
//		} else {
		Object obj = null;
		if((obj = request.getParameter(AuctionsTableConstants.PARAM_AUCTIONID)) != null)  {
			auctionId = (String)obj;
		}  else if((obj = request.getAttribute(AuctionsTableConstants.PARAM_AUCTIONID)) != null) {
			auctionId = (String)obj;
		}
		if ((obj = request.getParameter(AuctionSearchFormConstants.FROMSRCPARAM)) != null) {
			fromSrc = Integer.valueOf((String)obj);
		} else if ((obj = request.getAttribute(AuctionSearchFormConstants.FROMSRCPARAM)) != null){
			fromSrc = Integer.valueOf((String)obj);
		}
		detailsForm.initializeDetails(auctionId, fromSrc);

		return mapping.findForward(ActionForwards.SUCCESS);		
	}
}

