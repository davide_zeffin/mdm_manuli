package com.sap.isa.auction.actions.seller;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.sap.isa.auction.actionforms.BaseForm;
import com.sap.isa.auction.actionforms.seller.AuctionEditForm;
import com.sap.isa.auction.actionforms.seller.ProductCatalogForm;
import com.sap.isa.auction.actions.ActionForwards;
import com.sap.isa.auction.actions.BaseAction;
import com.sap.isa.auction.actions.seller.buttonEH.AddRemoveProductEH;
import com.sap.isa.auction.actions.seller.buttonEH.CancelCreateEditEH;
import com.sap.isa.auction.actions.seller.buttonEH.CatalogSearchEH;
import com.sap.isa.auction.actions.seller.buttonEH.ClearProductSelectionEH;
import com.sap.isa.auction.actions.seller.buttonEH.ContinueCreateEditEH;
import com.sap.isa.auction.actions.seller.treeEH.CategoryPCEH;
import com.sap.isa.ui.eventhandlers.TableEventHandler;

/**
 * Title:
 * Description:
 * Copyright:    Copyright (c) 2003
 * Company:
 * @author
 * @version 1.0
 */

public class ProductCatalogAction extends BaseAction {

  public ProductCatalogAction() {
  }

	protected void registerEventHandlers() {
		super.registerEventHandlers();
		registerEventHandler(ProductCatalogForm.CATALOGTREE , new CategoryPCEH());
		registerEventHandler(ProductCatalogForm.SERACHCATALOG , new CatalogSearchEH());
		
		registerEventHandler(ProductCatalogForm.PRODUCTTABLE , new TableEventHandler());
		registerEventHandler(ProductCatalogForm.ADDPRODUCT , new AddRemoveProductEH());
		registerEventHandler(AuctionEditForm.CONTINUEEH , new ContinueCreateEditEH());
		//registerEventHandler(ProductCatalogForm.CREATEAUCTION , new SelectedProductsEH());
		registerEventHandler(ProductCatalogForm.CLEARSELECT , new ClearProductSelectionEH());
		registerEventHandler(AuctionEditForm.CANCEL , new CancelCreateEditEH());
	}

	/* (non-Javadoc)
	 * @see com.sap.isa.auction.actions.BaseAction#doService(org.apache.struts.action.ActionMapping, com.sap.isa.auction.actionforms._BaseForm, javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse)
	 */
	protected ActionForward doService(
		ActionMapping mapping,
		BaseForm actionForm,
		HttpServletRequest request,
		HttpServletResponse response)
		throws ServletException, IOException {
			ProductCatalogForm productCatalogForm = (ProductCatalogForm)actionForm;
			productCatalogForm.initializeCatalog();
			return mapping.findForward(ActionForwards.SUCCESS);
	}

}