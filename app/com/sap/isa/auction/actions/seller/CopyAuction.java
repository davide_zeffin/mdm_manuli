/*
 * Created on May 25, 2004
 *
 * To change the template for this generated file go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
package com.sap.isa.auction.actions.seller;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.sap.isa.auction.actionforms.BaseForm;
import com.sap.isa.auction.actionforms.seller.AuctionEditForm;
import com.sap.isa.auction.actions.BaseAction;

/**
 * @author I803067
 *
 * To change the template for this generated type comment go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
public class CopyAuction extends BaseAction  {

	/* (non-Javadoc)
	 * @see com.sap.isa.auction.actions.BaseAction#doService(org.apache.struts.action.ActionMapping, com.sap.isa.auction.actionforms.BaseForm, javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse)
	 */
	protected ActionForward doService(
		ActionMapping mapping,
		BaseForm actionForm,
		HttpServletRequest request,
		HttpServletResponse response)
		throws ServletException, IOException {
		AuctionEditForm auctionEditForm = (AuctionEditForm)actionForm;
		String nameTitle = request.getParameter(AuctionEditForm.COPYTARGET);
		String name = "";
		String title = "";
		if(nameTitle != null)  {
			name = nameTitle.substring(0 , nameTitle.indexOf("$$$$"));
			title = nameTitle.substring(nameTitle.indexOf("$$$$") + 4);
		}
		auctionEditForm.copyAuction(name , title);
		return mapping.findForward(ActionForwards.SUCCESS);
	}

}
