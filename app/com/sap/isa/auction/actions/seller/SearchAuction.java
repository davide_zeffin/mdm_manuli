package com.sap.isa.auction.actions.seller;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.sap.isa.auction.actionforms.BaseForm;
import com.sap.isa.auction.actionforms.seller.AuctionDetailConstants;
import com.sap.isa.auction.actionforms.seller.AuctionSearchForm;
import com.sap.isa.auction.actions.BaseAction;
import com.sap.isa.auction.actions.seller.buttonEH.CopyAuctionEH;
import com.sap.isa.auction.actions.seller.buttonEH.EditAuctionEH;
import com.sap.isa.auction.actions.seller.buttonEH.PublishCloseAuctionEH;
import com.sap.isa.auction.actions.seller.buttonEH.RefreshAuctionEH;
import com.sap.isa.auction.actions.seller.buttonEH.SearchEH;
import com.sap.isa.auction.actions.seller.buttonEH.SearchResetEH;
import com.sap.isa.ui.eventhandlers.TableEventHandler;

/**
 * Title:			Internet Sales Private Auctions
 * Description:
 * @Copyright: 		Copyright (c) 2004
 * Company:			SAPLabs LLC
 * @author 
 * Created on: 		May 4, 2004
 */
public class SearchAuction extends BaseAction {
	/* (non-Javadoc)
	 * @see com.sap.isa.auction.actions.BaseAction#doService(org.apache.struts.action.ActionMapping, com.sap.isa.auction.actionforms.BaseForm, javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse)
	 */
	protected ActionForward doService(
		ActionMapping mapping,
		BaseForm actionForm,
		HttpServletRequest request,
		HttpServletResponse response)
		throws ServletException, IOException {
		
		AuctionSearchForm searchForm = (AuctionSearchForm)actionForm;
		searchForm.initializeSearchComponents();
		return mapping.findForward(ActionForwards.SUCCESS);

	}
	
	/* (non-Javadoc)
	 * @see com.sap.isa.ui.controllers.Action#registerEventHandlers()
	 */
	protected void registerEventHandlers() {
		super.registerEventHandlers();
		SearchEH searchEH = new SearchEH();
		RefreshAuctionEH refreshEH = new RefreshAuctionEH();
		registerEventHandler(AuctionSearchForm.BTNID_ADVSEARCH, searchEH);
	   	registerEventHandler(AuctionSearchForm.BTNID_GENSEARCH, searchEH);
	   	registerEventHandler(AuctionSearchForm.BTN_REFRESH, refreshEH);
	   	registerEventHandler(AuctionSearchForm.BTNID_RESET, new SearchResetEH());
		registerEventHandler(AuctionSearchForm.TBLID_AUCTION , new TableEventHandler());
		
		PublishCloseAuctionEH publishCloseEH = new PublishCloseAuctionEH();
		registerEventHandler(AuctionDetailConstants.BTNID_EDIT, new EditAuctionEH());
		registerEventHandler(AuctionDetailConstants.BTNID_PUBLISH, publishCloseEH);
		registerEventHandler(AuctionDetailConstants.BTNID_REFRESH , refreshEH);
		registerEventHandler(AuctionDetailConstants.BTNID_CLOSE, publishCloseEH);
		registerEventHandler(AuctionDetailConstants.BTNID_COPY , new CopyAuctionEH());
	}
}
