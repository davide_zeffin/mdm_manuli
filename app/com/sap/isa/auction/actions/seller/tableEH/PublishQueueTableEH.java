package com.sap.isa.auction.actions.seller.tableEH;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.sap.isa.auction.actionforms.seller.PublishQueueForm;
import com.sap.isa.ui.eventhandlers.TableEventHandler;
import com.sap.isa.ui.controllers.ActionForm;
import com.sapportals.htmlb.event.TableNavigationEvent;

/**
 * Title:			Internet Sales Private Auctions
 * Description:
 * @Copyright: 		Copyright (c) 2004
 * Company:			SAPLabs LLC
 * @author 
 * Created on: 		May 19, 2004
 */
public class PublishQueueTableEH extends TableEventHandler {

	/* (non-Javadoc)
	 * @see com.sap.isa.ui.eventhandlers.TableEventHandler#tableNavigationEvent(org.apache.struts.action.ActionMapping, com.sap.isa.ui.controllers.ActionForm, com.sapportals.htmlb.event.TableNavigationEvent, javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse)
	 */
	public ActionForward tableNavigationEvent(
		ActionMapping mapping,
		ActionForm form,
		TableNavigationEvent event,
		HttpServletRequest request,
		HttpServletResponse response)
		throws ServletException, IOException {
		// TODO Auto-generated method stub
		ActionForward forwarding = super.tableNavigationEvent(
									mapping,
									form,
									event,
									request,
									response);
		PublishQueueForm publishingForm = (PublishQueueForm)form;
		publishingForm.setGroupHeader();									
		return forwarding;
	}

}
