/*
 * Created on May 1, 2004
 *
 * To change the template for this generated file go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
package com.sap.isa.auction.actions.seller;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.sap.isa.auction.actionforms.BaseForm;
import com.sap.isa.auction.actionforms.seller.AuctionEditForm;
import com.sap.isa.auction.actionforms.seller.CreateSelectionForm;
import com.sap.isa.auction.actions.BaseAction;
import com.sap.isa.auction.actions.seller.buttonEH.AddRemoveProductEH;
import com.sap.isa.auction.actions.seller.buttonEH.AddRemoveTGBPEH;
import com.sap.isa.auction.actions.seller.buttonEH.CancelCreateEditEH;
import com.sap.isa.auction.actions.seller.buttonEH.CreateAuctionEH;

/**
 * @author I803067
 *
 * To change the template for this generated type comment go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
public class CreateSelectionAction extends BaseAction  {


	/* (non-Javadoc)
	 * @see com.sap.isa.auction.actions.BaseAction#doService(org.apache.struts.action.ActionMapping, com.sap.isa.auction.actionforms._BaseForm, javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse)
	 */
	protected ActionForward doService(
		ActionMapping mapping,
		BaseForm actionForm,
		HttpServletRequest request,
		HttpServletResponse response)
		throws ServletException, IOException {
		
		CreateSelectionForm selectionForm = (CreateSelectionForm)actionForm;
		selectionForm.setSelection();
		return mapping.findForward(ActionForwards.SUCCESS);
	}

	/* (non-Javadoc)
	 * @see com.sap.isa.ui.controllers.Action#registerEventHandlers()
	 */
	protected void registerEventHandlers() {
		super.registerEventHandlers();
		registerEventHandler(CreateSelectionForm.CREATEEVENT , new CreateAuctionEH());
		AddRemoveProductEH productEH = new AddRemoveProductEH();
		registerEventHandler(CreateSelectionForm.REMOVEPRODUCTSEVENT , productEH);
		registerEventHandler(CreateSelectionForm.ADDMOREPRODUCTSEVENT , productEH);
		AddRemoveTGBPEH targetsEH = new AddRemoveTGBPEH();
		registerEventHandler(CreateSelectionForm.REMOVETARGETEVENT , targetsEH);
		registerEventHandler(CreateSelectionForm.ADDMORETARGETGROUPSEVENT , targetsEH);
		registerEventHandler(AuctionEditForm.CANCEL , new CancelCreateEditEH());
	}

}
