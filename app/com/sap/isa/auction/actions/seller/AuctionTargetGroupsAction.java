package com.sap.isa.auction.actions.seller;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.sap.isa.auction.actionforms.BaseForm;
import com.sap.isa.auction.actionforms.seller.AuctionDetailConstants;
import com.sap.isa.auction.actionforms.seller.AuctionDetailsForm;
import com.sap.isa.auction.actions.BaseAction;
import com.sap.isa.ui.eventhandlers.TableEventHandler;


/**
 * Title:			Internet Sales Private Auctions
 * Description:
 * @Copyright: 		Copyright (c) 2004
 * Company:			SAPLabs LLC
 * @author 
 * Created on: 		May 14, 2004
 */
public class AuctionTargetGroupsAction extends BaseAction {
	
		protected ActionForward doService(
			ActionMapping mapping,
			BaseForm actionForm,
			HttpServletRequest request,
			HttpServletResponse response)
			throws ServletException, IOException {
		
		AuctionDetailsForm detailsForm = (AuctionDetailsForm) actionForm;	
		detailsForm.initializeTargetGroupInfo();
		return mapping.findForward(ActionForwards.SUCCESS);
	}
	protected void registerEventHandlers() {
		super.registerEventHandlers();
		registerEventHandler(AuctionDetailConstants.TARGETSTABLEVIEW , new TableEventHandler());
	}
}