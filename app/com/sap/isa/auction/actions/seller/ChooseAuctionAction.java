package com.sap.isa.auction.actions.seller;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.sap.isa.auction.actionforms.BaseForm;
import com.sap.isa.auction.actionforms.seller.AuctionSearchForm;
import com.sap.isa.auction.actionforms.seller.ManualWinnerForm;
import com.sap.isa.auction.actions.seller.buttonEH.RefreshAuctionEH;
import com.sap.isa.auction.actions.seller.buttonEH.SearchEH;
import com.sap.isa.auction.actions.seller.buttonEH.SearchResetEH;
import com.sap.isa.ui.eventhandlers.TableEventHandler;

/**
 * Title:			Internet Sales Private Auctions
 * Description:
 * @Copyright: 		Copyright (c) 2004
 * Company:			SAPLabs LLC
 * Created on:		May 25, 2004
 */
public class ChooseAuctionAction extends SearchAuction {

	/* (non-Javadoc)
	 * @see com.sap.isa.auction.actions.BaseAction#doService(org.apache.struts.action.ActionMapping, com.sap.isa.auction.actionforms.BaseForm, javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse)
	 */
	protected ActionForward doService(
		ActionMapping mapping,
		BaseForm actionForm,
		HttpServletRequest request,
		HttpServletResponse response)
		throws ServletException, IOException {

		String forwards = null;
//		if (actionForm instanceof ManualWinnerForm){
			ManualWinnerForm mwForm = (ManualWinnerForm) actionForm;
			mwForm.initializeManualWinnerComponents();
			forwards = ActionForwards.SUCCESS;
//		} else if (actionForm instanceof ManualWinnerDetailsForm){
//			ManualWinnerDetailsForm mwDetailsForm = (ManualWinnerDetailsForm) 
//					actionForm;
//			// Is this request from the manual winner auction list page or the
//			// manual winner determination page
//			String page = request.getParameter("forward");
//			if (null == page){ 
//				mwDetailsForm.initializeGeneralInfo();
//				forwards = ActionForwards.SUCCESS;
//			} else {				
//				if ("details".equals(page)){
//					mwDetailsForm.initializeDetails();
//					forwards = ActionForwards.MWDETAILS;
//				} else if ("bids".equals(page)){
//					mwDetailsForm.initializeBidInfo();
//					forwards = ActionForwards.MWBIDS;
//				} else if ("summary".equals(page)){
//					mwDetailsForm.initializeSummaryInfo();
//					forwards = ActionForwards.MWSUMMARY;
//				}
//			}
//		}
		return mapping.findForward(forwards);
	}

	/* (non-Javadoc)
	 * @see com.sap.isa.ui.controllers.Action#registerEventHandlers()
	 */
	protected void registerEventHandlers() {
		SearchEH searchEH = new SearchEH();
		RefreshAuctionEH refreshEH = new RefreshAuctionEH();
		registerEventHandler(AuctionSearchForm.BTNID_ADVSEARCH, searchEH);
		registerEventHandler(AuctionSearchForm.BTNID_GENSEARCH, searchEH);
		registerEventHandler(AuctionSearchForm.BTN_REFRESH, refreshEH);
		registerEventHandler(AuctionSearchForm.BTNID_RESET, 
										new SearchResetEH());
		registerEventHandler(AuctionSearchForm.TBLID_AUCTION , 
										new TableEventHandler());	
	}

}
