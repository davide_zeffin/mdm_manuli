package com.sap.isa.auction.actions.seller;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.sap.isa.auction.actionforms.BaseForm;
import com.sap.isa.auction.actionforms.seller.OrdersForm;
import com.sap.isa.auction.actions.BaseAction;
import com.sap.isa.auction.actions.seller.buttonEH.ShowOrderDetailsEH;
/**
 * Title:			Internet Sales Private Auctions
 * Description:
 * @Copyright: 		Copyright (c) 2004
 * Company:			SAPLabs LLC
 * @author 
 * Created on: 		Jun 9, 2004
 */
public class OrdersAction extends BaseAction {
	protected ActionForward doService(
		ActionMapping mapping,
		BaseForm actionForm,
		HttpServletRequest request,
		HttpServletResponse response)
		throws ServletException, IOException {

		OrdersForm ordersForm = (OrdersForm) actionForm;
		ordersForm.initializeOrdersInfo();
		return mapping.findForward(ActionForwards.SUCCESS);
	}
	
	protected void registerEventHandlers() {
		super.registerEventHandlers();
		registerEventHandler(OrdersForm.BTNID_SELECTORDER, new ShowOrderDetailsEH());
	}
}
