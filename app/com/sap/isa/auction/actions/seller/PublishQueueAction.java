package com.sap.isa.auction.actions.seller;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.sap.isa.auction.actionforms.BaseForm;
import com.sap.isa.auction.actionforms.seller.PublishQueueForm;
import com.sap.isa.auction.actions.seller.buttonEH.PublishCloseAuctionEH;
import com.sap.isa.auction.actions.seller.tableEH.PublishQueueTableEH;


/**
 * Title:			Internet Sales Private Auctions
 * Description:
 * @Copyright: 		Copyright (c) 2004
 * Company:			SAPLabs LLC
 * @author 
 * Created on: 		May 19, 2004
 */
public class PublishQueueAction extends SearchAuction {
	protected ActionForward doService(
		ActionMapping mapping,
		BaseForm actionForm,
		HttpServletRequest request,
		HttpServletResponse response)
		throws ServletException, IOException {

		PublishQueueForm publishForm = (PublishQueueForm) actionForm;
		publishForm.initializePublishComponents();
		return mapping.findForward(ActionForwards.SUCCESS);
	}

	protected void registerEventHandlers() {
		super.registerEventHandlers();
		this.registerEventHandler(
			PublishQueueForm.PUBLISHAUCTIONVIEW,
			new PublishQueueTableEH());
		this.registerEventHandler(
			PublishQueueForm.PUBLISHSCHBUTTONID,
			new PublishCloseAuctionEH());
	}
}
