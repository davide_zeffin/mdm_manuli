package com.sap.isa.auction.actions.seller.tabEH;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.sap.isa.auction.actionforms.seller.ManualWinnerDetailsForm;
import com.sap.isa.auction.actions.seller.ActionForwards;
import com.sap.isa.ui.controllers.ActionForm;
import com.sap.isa.ui.eventhandlers.TabSelectedEventHandler;
import com.sapportals.htmlb.event.TabSelectEvent;

/**
 * Title:			Internet Sales Private Auctions
 * Description:
 * @Copyright: 		Copyright (c) 2004
 * Company:			SAPLabs LLC
 * Created on:		Jun 14, 2004
 */
public class DetermineWinnerTabEH extends TabSelectedEventHandler {

	/* (non-Javadoc)
	 * @see com.sap.isa.ui.eventhandlers.TabSelectedEventHandler#tabSelectedEvent(org.apache.struts.action.ActionMapping, com.sap.isa.ui.controllers.ActionForm, com.sapportals.htmlb.event.TabSelectEvent, javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse)
	 */
	public ActionForward tabSelectedEvent(
		ActionMapping mapping,
		ActionForm form,
		TabSelectEvent event,
		HttpServletRequest request,
		HttpServletResponse response)
		throws IOException, ServletException {

		ManualWinnerDetailsForm aForm = (ManualWinnerDetailsForm)form;

		String forward = ActionForwards.SUCCESS;
		switch (event.getSelectedIndex()){
			case ManualWinnerDetailsForm.MW_AUCTIONDETAILSINDEX:
//				aForm.initializeDetails();
				forward = ActionForwards.MWDETAILS;
				break;
			case ManualWinnerDetailsForm.MW_AUCTIONBIDSINDEX:
				aForm.initializeBidInfo();
				forward = ActionForwards.MWBIDS;
				break;
			case ManualWinnerDetailsForm.MW_SUMMARYINDEX:
				aForm.initializeSummaryInfo();
				forward = ActionForwards.MWSUMMARY;
				break;
		}
		return mapping.findForward(forward);
	}

}
