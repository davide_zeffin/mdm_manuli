/*
 * Created on Apr 30, 2004
 *
 * To change the template for this generated file go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
package com.sap.isa.auction.actions.seller;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.sap.isa.auction.actionforms.BaseForm;
import com.sap.isa.auction.actionforms.seller.AuctionEditForm;
import com.sap.isa.auction.actionforms.seller.TargetGroupConstants;
import com.sap.isa.auction.actionforms.seller.TargetGroupsForm;
import com.sap.isa.auction.actions.BaseAction;
import com.sap.isa.auction.actions.seller.buttonEH.AddRemoveTGBPEH;
import com.sap.isa.auction.actions.seller.buttonEH.CancelCreateEditEH;
import com.sap.isa.auction.actions.seller.buttonEH.ClearTGBPSelectionEH;
import com.sap.isa.auction.actions.seller.buttonEH.ContinueCreateEditEH;
import com.sap.isa.auction.actions.seller.buttonEH.TargetGroupBusinessPartnerSearchEH;
import com.sap.isa.ui.eventhandlers.TableEventHandler;

/**
 * @author I803067
 *
 * To change the template for this generated type comment go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
public class TargetGroupsAction extends BaseAction  {

	/* (non-Javadoc)
	 * @see com.sap.isa.auction.actions.BaseAction#doService(org.apache.struts.action.ActionMapping, com.sap.isa.auction.actionforms._BaseForm, javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse)
	 */
	protected ActionForward doService(
		ActionMapping mapping,
		BaseForm actionForm,
		HttpServletRequest request,
		HttpServletResponse response)
		throws ServletException, IOException {
		TargetGroupsForm targetForm = (TargetGroupsForm)actionForm;
		if (targetForm.getCurrentEvent().getAction() != null && targetForm.getCurrentEvent().getAction().equals(TargetGroupConstants.CLEAREVENT)) {
			targetForm.clearSearchCriteria();
			targetForm.clearSearchResult();			
		} 
		else {
			targetForm.initializeTargetGroupElements();
		}
		return mapping.findForward(ActionForwards.SUCCESS);
	}
	/* (non-Javadoc)
	 * @see com.sap.isa.ui.controllers.Action#registerEventHandlers()
	 */
	protected void registerEventHandlers() {
		super.registerEventHandlers();
		TargetGroupBusinessPartnerSearchEH search = new TargetGroupBusinessPartnerSearchEH();
		registerEventHandler(TargetGroupsForm.SEARCHTGEVENT , search);
		registerEventHandler(TargetGroupsForm.SEARCHBPARTNERSEVENT , search);
		registerEventHandler(TargetGroupsForm.SEARCHBUSINESSPARTNERVIEW , new TableEventHandler());
		registerEventHandler(AuctionEditForm.CANCEL , new CancelCreateEditEH());
		registerEventHandler(TargetGroupsForm.ADDTOTARGETEVENT , new AddRemoveTGBPEH());
		registerEventHandler(AuctionEditForm.CONTINUETOSEL , new ContinueCreateEditEH());
		registerEventHandler(TargetGroupsForm.CLEARSELECT, new ClearTGBPSelectionEH());
	}

}
