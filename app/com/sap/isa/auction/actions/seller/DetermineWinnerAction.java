package com.sap.isa.auction.actions.seller;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.sap.isa.auction.actionforms.BaseForm;
import com.sap.isa.auction.actionforms.seller.AuctionDetailConstants;
import com.sap.isa.auction.actionforms.seller.ManualWinnerDetailsForm;
import com.sap.isa.auction.actions.BaseAction;
import com.sap.isa.auction.actions.seller.buttonEH.BeginWinnerDeterminationEH;
import com.sap.isa.auction.actions.seller.buttonEH.CancelDetermineWinnerEH;
import com.sap.isa.auction.actions.seller.buttonEH.ClearBidSelectionEH;
import com.sap.isa.auction.actions.seller.buttonEH.DetermineWinnerEH;
import com.sap.isa.auction.actions.seller.buttonEH.ManualWinnerNextPreviousEH;
import com.sap.isa.auction.actions.seller.buttonEH.SelectBidsEH;
import com.sap.isa.auction.actions.seller.tabEH.DetermineWinnerTabEH;
import com.sap.isa.ui.eventhandlers.TableEventHandler;

/**
 * Title:			Internet Sales Private Auctions
 * Description:
 * @Copyright: 		Copyright (c) 2004
 * Company:			SAPLabs LLC
 * Created on:		Jun 9, 2004
 */
public class DetermineWinnerAction extends BaseAction {

	/* (non-Javadoc)
	 * @see com.sap.isa.auction.actions.BaseAction#doService(org.apache.struts.action.ActionMapping, com.sap.isa.auction.actionforms.BaseForm, javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse)
	 */
	protected ActionForward doService(
		ActionMapping mapping,
		BaseForm actionForm,
		HttpServletRequest request,
		HttpServletResponse response)
		throws ServletException, IOException {

		ManualWinnerDetailsForm form = (ManualWinnerDetailsForm)actionForm;
		String page = request.getParameter("page");
		String forward = ActionForwards.SUCCESS;
		if (null != page){ 
			if ("details".equals(page)){
//				form.initializeDetails();
				forward = ActionForwards.MWDETAILS;
			} else if ("bids".equals(page)){
				form.initializeBidInfo();
				forward = ActionForwards.MWBIDS;
			} else if ("summary".equals(page)){
				form.initializeSummaryInfo();
				forward = ActionForwards.MWSUMMARY;
			}
		}

		return mapping.findForward(forward);
	}

	/* (non-Javadoc)
	 * @see com.sap.isa.ui.controllers.Action#registerEventHandlers()
	 */
	protected void registerEventHandlers() {
		registerEventHandler(ManualWinnerDetailsForm.DETERMINEWINNER_TAB_ID,
				new DetermineWinnerTabEH());
		registerEventHandler(ManualWinnerDetailsForm.SELECTBIDS_BTN_ID,
				new SelectBidsEH());
		registerEventHandler(AuctionDetailConstants.BIDSTABLE_ID, 
				new TableEventHandler());
		registerEventHandler(ManualWinnerDetailsForm.CLEARBIDS_BTN_ID,
				new ClearBidSelectionEH());
		registerEventHandler(ManualWinnerDetailsForm.DETERMINEWINNER_BTN_ID,
				new DetermineWinnerEH());
		registerEventHandler(ManualWinnerDetailsForm.START_DETERMINATION_BTN_ID,
				new BeginWinnerDeterminationEH());
		registerEventHandler(ManualWinnerDetailsForm.CANCEL_BTN_ID,
				new CancelDetermineWinnerEH());
		
		ManualWinnerNextPreviousEH nextPrevEH = 
				new ManualWinnerNextPreviousEH();
		
		registerEventHandler(ManualWinnerDetailsForm.PREV_BTN_ID, nextPrevEH);		
		registerEventHandler(ManualWinnerDetailsForm.NEXT_BTN_ID, nextPrevEH);		
	}

}
