package com.sap.isa.auction.actions.seller;

/**
 * Title:			Internet Sales Private Auctions
 * Description:
 * @Copyright: 		Copyright (c) 2004
 * Company:			SAPLabs LLC
 * @author
 * Created on:		Apr 27, 2004 
 */

public interface ActionForwards extends com.sap.isa.auction.actions.ActionForwards{
    
 	//Monitor, Search related forward constants
    public static final String EDIT   		= "edit";
	public static final String COPY   		= "copy";

    //frame name ->targets
    public static final String HEADER  = "Header";
    public static final String DESKTOP = "Desktop";
    public static final String DETAILS = "details";
    public static final String EMPTY   = "empty";
    
    public static final String TARGETGROUPS = "targetgroups";
    public static final String HOME			= "home";
    
    public static final String SELECTION	= "selection";
    public static final String SINGLECREATE = "singlecreate";
    public static final String MULTICREATE	= "multicreate";
    
    public static final String EDITGENERAL 	= "general";
    public static final String EDITPRODUCTS = "products";
    public static final String EDITTARGETS	= "targets";
    public static final String SUMMARY		= "summary";
    public static final String ADDPRODUCTS  = "addproducts";
    public static final String ADDTARGETS	= "addtargets";
    public static final String PUBLISH		= "publish";
	public static final String SUBERROR		= "suberror";
	
	public static final String MONITOR		= "monitor";
	public static final String PUBLISHING	= "publishing";
	public static final String MWAUCTIONS	= "mwauctions";

	public static final String MWINFO = "mwinfo";
	public static final String MWDETAILS = "mwdetails";
	public static final String MWBIDS = "mwbids";
	public static final String MWSUMMARY = "mwsummary";
	public static final String CANCEL = "cancel";
	
}
