/*
 * Created on Apr 30, 2004
 *
 * To change the template for this generated file go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
package com.sap.isa.auction.actions.seller.treeEH;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.sap.isa.auction.actionforms.seller.ProductCatalogForm;
import com.sap.isa.auction.actions.seller.ActionForwards;
import com.sap.isa.ui.controllers.ActionForm;
import com.sap.isa.ui.eventhandlers.TreeEventHandler;
import com.sapportals.htmlb.event.TreeNodeExpandEvent;
import com.sapportals.htmlb.event.TreeNodeSelectEvent;

/**
 * @author I803067
 *
 * To change the template for this generated type comment go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
public class CategoryPCEH extends TreeEventHandler  {

	public ActionForward treeNodeExpandHandler(ActionMapping mapping,
												ActionForm form,
												TreeNodeExpandEvent event,
												HttpServletRequest request,
												HttpServletResponse response)
												throws java.io.IOException, javax.servlet.ServletException {

		ProductCatalogForm categoriesForm = (ProductCatalogForm)form;
		categoriesForm.expandCategory(event.getComponentName() , event.getNodeKey());
		return mapping.findForward(ActionForwards.SUCCESS);
	}

	public ActionForward treeNodeSelectHandler(ActionMapping mapping,
												  ActionForm form,
												  TreeNodeSelectEvent event,
												  HttpServletRequest request,
												  HttpServletResponse response)
												  throws java.io.IOException, javax.servlet.ServletException {
		ProductCatalogForm categoriesForm = (ProductCatalogForm)form;
		categoriesForm.expandCategory(event.getComponentName() , event.getNodeKey());
		return mapping.findForward(ActionForwards.SUCCESS);
	}

}
