/*
 * Created on Apr 30, 2004
 *
 * To change the template for this generated file go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
package com.sap.isa.auction.actions.seller;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.sap.isa.auction.actionforms.BaseForm;
import com.sap.isa.auction.actionforms.seller.AuctionEditForm;
import com.sap.isa.auction.actions.BaseAction;
import com.sap.isa.auction.actions.seller.buttonEH.AddRemoveProductEH;
import com.sap.isa.auction.actions.seller.buttonEH.AddRemoveTGBPEH;
import com.sap.isa.auction.actions.seller.buttonEH.AuctionEditNextPreviousEH;
import com.sap.isa.auction.actions.seller.buttonEH.SaveCancelEH;
import com.sap.isa.auction.actions.seller.tabEH.AuctionEditTabEH;

/**
 * @author I803067
 *
 * To change the template for this generated type comment go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
public class CreateAuction extends BaseAction  {

	public static final String PARAMETER = "_create";
	public static final String SINGLE	 = "single";
	public static final String MULTIPLE	 = "multiple";
	
	/* (non-Javadoc)
	 * @see com.sap.isa.auction.actions.BaseAction#doService(org.apache.struts.action.ActionMapping, com.sap.isa.auction.actionforms._BaseForm, javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse)
	 */
	protected ActionForward doService(
		ActionMapping mapping,
		BaseForm actionForm,
		HttpServletRequest request,
		HttpServletResponse response)
		throws ServletException, IOException {
		
		AuctionEditForm auctionForm = (AuctionEditForm)actionForm;
		if(request.getParameter(PARAMETER) != null)  {
			if(request.getParameter(PARAMETER).equals(SINGLE))  {
				auctionForm.startSingleCreation();	
			}  else  {
				auctionForm.startMultipleCreation();
			}
			return mapping.findForward(ActionForwards.EDITGENERAL);
		}  else  if(request.getParameter(AuctionEditForm.SHOWPARAMETER) != null
				&& request.getParameter(AuctionEditForm.SHOWPARAMETER).equals(ActionForwards.EDITPRODUCTS)){
			auctionForm.setInformation(AuctionEditForm.PRODUCTTABINDEX);
			return mapping.findForward(ActionForwards.EDITPRODUCTS);
		}  else if(request.getParameter(AuctionEditForm.SHOWPARAMETER) != null
			&& request.getParameter(AuctionEditForm.SHOWPARAMETER).equals(ActionForwards.EDITTARGETS))  {
			auctionForm.setInformation(AuctionEditForm.TARGETGROUPTABINDEX);
			return mapping.findForward(ActionForwards.EDITTARGETS);
		}  else {
			return mapping.findForward(ActionForwards.SUCCESS);
		}
	}

	/* (non-Javadoc)
	 * @see com.sap.isa.ui.controllers.Action#registerEventHandlers()
	 */
	protected void registerEventHandlers() {
		super.registerEventHandlers();
		registerEventHandler(AuctionEditForm.CREATEEDITTABEVENT , new AuctionEditTabEH());
		AddRemoveProductEH productEH = new AddRemoveProductEH();
		registerEventHandler(AuctionEditForm.ADDMOREPRODUCTSEVENT , productEH);
		registerEventHandler(AuctionEditForm.REMOVEPRODUCTSEVENT , productEH);
		AddRemoveTGBPEH targetEH = new AddRemoveTGBPEH();
		registerEventHandler(AuctionEditForm.ADDMORETARGETGROUPSEVENT , targetEH);
		registerEventHandler(AuctionEditForm.REMOVETARGETEVENT , targetEH);
		AuctionEditNextPreviousEH nextPreviousEH = new AuctionEditNextPreviousEH();
		registerEventHandler(AuctionEditForm.NEXTEVENT , nextPreviousEH);
		registerEventHandler(AuctionEditForm.PREVIOUSEVENT , nextPreviousEH);
		registerEventHandler(AuctionEditForm.FINISHEVENT , nextPreviousEH);
		SaveCancelEH saveEH = new SaveCancelEH();
		registerEventHandler(AuctionEditForm.SAVEEVENT , saveEH);
		registerEventHandler(AuctionEditForm.CANCEL , saveEH);
	}
}
