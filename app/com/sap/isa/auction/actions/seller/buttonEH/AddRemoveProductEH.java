/*
 * Created on Apr 30, 2004
 *
 * To change the template for this generated file go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
package com.sap.isa.auction.actions.seller.buttonEH;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.sap.isa.auction.actionforms.seller.AuctionEditForm;
import com.sap.isa.auction.actionforms.seller.CreateSelectionForm;
import com.sap.isa.auction.actionforms.seller.ProductCatalogForm;
import com.sap.isa.auction.actionforms.seller.TargetGroupsForm;
import com.sap.isa.auction.actions.seller.ActionForwards;
import com.sap.isa.ui.controllers.ActionForm;
import com.sapportals.htmlb.event.ButtonClickEvent;

/**
 * @author I803067
 *
 * To change the template for this generated type comment go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
public class AddRemoveProductEH extends com.sap.isa.ui.eventhandlers.ButtonClickedEventHandler {

	/* (non-Javadoc)
	 * @see com.sap.isa.ui.eventhandlers.ButtonClickedEventHandler#buttonClickedEvent(org.apache.struts.action.ActionMapping, com.sap.isa.ui.controllers.ActionForm, com.sapportals.htmlb.event.ButtonClickEvent, javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse)
	 */
	public ActionForward buttonClickedEvent(
		ActionMapping mapping,
		ActionForm form,
		ButtonClickEvent event,
		HttpServletRequest request,
		HttpServletResponse response)
		throws ServletException, IOException {
		if(form instanceof ProductCatalogForm)  {
			ProductCatalogForm productForm = (ProductCatalogForm)form;
			if(event.getAction().equals(ProductCatalogForm.ADDPRODUCT))
				productForm.addProducts();
			else  if(event.getAction().equals(ProductCatalogForm.REMOVEBUTTON))  {
				productForm.removeProducts();
			}
			return mapping.findForward(com.sap.isa.auction.actions.seller.ActionForwards.SUCCESS);
		}  else if(form instanceof AuctionEditForm)  {
			if(event.getAction().equals(AuctionEditForm.ADDMOREPRODUCTSEVENT))  {
				return mapping.findForward(ActionForwards.ADDPRODUCTS);
			}  else  {
				((AuctionEditForm)form).removeProducts();
			}
			return mapping.findForward(ActionForwards.EDITPRODUCTS);
		}  else if(form instanceof CreateSelectionForm)  {
			if(event.getAction().equals(CreateSelectionForm.ADDMOREPRODUCTSEVENT))  {
				return mapping.findForward(ActionForwards.ADDPRODUCTS);
			}  else if(event.getAction().equals(CreateSelectionForm.REMOVEPRODUCTSEVENT))  {
				((CreateSelectionForm)form).removeProducts();
				return mapping.findForward(ActionForwards.SUCCESS);
			}
		}
		return mapping.findForward(ActionForwards.SUCCESS);
	}

}
