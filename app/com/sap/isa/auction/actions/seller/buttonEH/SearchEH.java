package com.sap.isa.auction.actions.seller.buttonEH;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.sap.isa.auction.actionforms.seller.AuctionSearchForm;
import com.sap.isa.auction.actionforms.seller.ManualWinnerForm;
import com.sap.isa.auction.actions.seller.ActionForwards;
import com.sap.isa.ui.controllers.ActionForm;
import com.sap.isa.ui.eventhandlers.ButtonClickedEventHandler;
import com.sapportals.htmlb.event.ButtonClickEvent;

/**
 * Title:			Internet Sales Private Auctions
 * Description:
 * @Copyright: 		Copyright (c) 2004
 * Company:			SAPLabs LLC
 * @author 
 * Created on: 		May 4, 2004
 */
public class SearchEH extends ButtonClickedEventHandler {
	
	public ActionForward buttonClickedEvent(
		ActionMapping mapping,
		ActionForm form,
		ButtonClickEvent event,
		HttpServletRequest request,
		HttpServletResponse response)
		throws ServletException, IOException {

		AuctionSearchForm searchForm = null;
		if (form instanceof ManualWinnerForm){
			searchForm = (ManualWinnerForm) form;
		} else {
			searchForm = (AuctionSearchForm) form;			
		}
		
		if(event.getAction().equals(AuctionSearchForm.BTNID_GENSEARCH))  {
			searchForm.genericSearch();
		}  else if(event.getAction().equals(AuctionSearchForm.BTNID_ADVSEARCH))   {
			searchForm.advancedSearch();
		}  else if(event.getAction().equals(AuctionSearchForm.BTNID_REFRESH))  {
			searchForm.refresh();
		}
		return mapping.findForward(ActionForwards.SUCCESS);
	}
}