/*
 * Created on May 3, 2004
 *
 * To change the template for this generated file go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
package com.sap.isa.auction.actions.seller.buttonEH;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.sap.isa.auction.actionforms.seller.AuctionEditForm;
import com.sap.isa.auction.actions.seller.ActionForwards;
import com.sap.isa.ui.controllers.ActionForm;
import com.sap.isa.ui.eventhandlers.ButtonClickedEventHandler;
import com.sapportals.htmlb.event.ButtonClickEvent;

/**
 * @author I803067
 *
 * To change the template for this generated type comment go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
public class AuctionEditNextPreviousEH extends ButtonClickedEventHandler  {


	/* (non-Javadoc)
	 * @see com.sap.isa.ui.eventhandlers.ButtonClickedEventHandler#buttonClickedEvent(org.apache.struts.action.ActionMapping, com.sap.isa.ui.controllers.ActionForm, com.sapportals.htmlb.event.ButtonClickEvent, javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse)
	 */
	public ActionForward buttonClickedEvent(
		ActionMapping mapping,
		ActionForm form,
		ButtonClickEvent event,
		HttpServletRequest request,
		HttpServletResponse response)
		throws ServletException, IOException {
		
		AuctionEditForm auctionForm = (AuctionEditForm)form;
		auctionForm.updateInformation();
		int target = Integer.parseInt(event.getAction());
		auctionForm.setInformation(target);
		switch(target)  {
			case AuctionEditForm.GENERALDETAILSTABINDEX :
				return mapping.findForward(ActionForwards.EDITGENERAL);
			
			case AuctionEditForm.PRODUCTTABINDEX :
				return mapping.findForward(ActionForwards.EDITPRODUCTS);
			
			case AuctionEditForm.TARGETGROUPTABINDEX :
				return mapping.findForward(ActionForwards.EDITTARGETS);
			
			case AuctionEditForm.SUMMARYTABINDEX :
				return mapping.findForward(ActionForwards.SUMMARY);
		}
		return mapping.findForward(ActionForwards.SUCCESS);
		
	}

}
