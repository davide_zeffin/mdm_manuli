package com.sap.isa.auction.actions.seller.buttonEH;

//Struts imports
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.sap.isa.auction.actions.seller.ActionForwards;
import com.sap.isa.ui.controllers.ActionForm;
import com.sap.isa.ui.eventhandlers.ButtonClickedEventHandler;
import com.sapportals.htmlb.event.ButtonClickEvent;


/**
 * Title:			Internet Sales Private Auctions
 * Description:
 * @Copyright: 		Copyright (c) 2004
 * Company:			SAPLabs LLC
 * @author 
 * Created on: 		May 14, 2004
 */

public class EditAuctionEH extends ButtonClickedEventHandler {

  public EditAuctionEH() {
  }

    public ActionForward buttonClickedEvent(ActionMapping mapping,
                                          ActionForm form,
                                          ButtonClickEvent event,
                                          HttpServletRequest request,
                                          HttpServletResponse response)
                                          throws java.io.IOException, javax.servlet.ServletException {
        return mapping.findForward(ActionForwards.EDIT);
    }
}
