/*
 * Created on Apr 30, 2004
 *
 * To change the template for this generated file go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
package com.sap.isa.auction.actions.seller.buttonEH;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.sap.isa.auction.actionforms.seller.AuctionEditForm;
import com.sap.isa.auction.actionforms.seller.ProductCatalogForm;
import com.sap.isa.auction.actionforms.seller.TargetGroupsForm;
import com.sap.isa.auction.actions.seller.ActionForwards;
import com.sap.isa.auction.helpers.CatalogHelper;
import com.sap.isa.ui.controllers.ActionForm;
import com.sap.isa.ui.eventhandlers.ButtonClickedEventHandler;
import com.sapportals.htmlb.event.ButtonClickEvent;

/**
 * @author I803067
 *
 * To change the template for this generated type comment go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
public class ContinueCreateEditEH extends ButtonClickedEventHandler  {


	/* (non-Javadoc)
	 * @see com.sap.isa.ui.eventhandlers.ButtonClickedEventHandler#buttonClickedEvent(org.apache.struts.action.ActionMapping, com.sap.isa.ui.controllers.ActionForm, com.sapportals.htmlb.event.ButtonClickEvent, javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse)
	 */
	public ActionForward buttonClickedEvent(
		ActionMapping mapping,
		ActionForm form,
		ButtonClickEvent event,
		HttpServletRequest request,
		HttpServletResponse response)
		throws ServletException, IOException {
		
		if(form instanceof ProductCatalogForm)  {
			if(((ProductCatalogForm)form).getContext().equals(CatalogHelper.SELECTIONCONTEXT))  {
				return mapping.findForward(ActionForwards.SELECTION);
			}  else  if(((ProductCatalogForm)form).getContext().equals(CatalogHelper.CREATIONCONTEXT)){
				return mapping.findForward(ActionForwards.EDITPRODUCTS);
			}
		}  else if(form instanceof TargetGroupsForm)  {
			if(((TargetGroupsForm)form).getContext().equals(CatalogHelper.CREATIONCONTEXT))  {
				return mapping.findForward(ActionForwards.EDITTARGETS);
			}  else  {
				return mapping.findForward(ActionForwards.SELECTION);	
			}
		}
		if(event.getAction().equals(AuctionEditForm.CONTINUETOTG))  {
			return mapping.findForward(ActionForwards.TARGETGROUPS);
		}  else if(event.getAction().equals(AuctionEditForm.CONTINUETOSEL))  {
			return mapping.findForward(ActionForwards.SELECTION);
		}
		return mapping.findForward(ActionForwards.SUCCESS);
	}

}
