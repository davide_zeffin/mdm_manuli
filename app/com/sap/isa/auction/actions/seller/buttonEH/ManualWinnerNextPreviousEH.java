package com.sap.isa.auction.actions.seller.buttonEH;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.sap.isa.auction.actionforms.seller.ManualWinnerDetailsForm;
import com.sap.isa.auction.actions.seller.ActionForwards;
import com.sap.isa.ui.controllers.ActionForm;
import com.sap.isa.ui.eventhandlers.ButtonClickedEventHandler;
import com.sapportals.htmlb.event.ButtonClickEvent;

/**
 * Title:			Internet Sales Private Auctions
 * Description:
 * @Copyright: 		Copyright (c) 2004
 * Company:			SAPLabs LLC
 * Created on:		Jun 16, 2004
 */
public class ManualWinnerNextPreviousEH extends ButtonClickedEventHandler {

	/* (non-Javadoc)
	 * @see com.sap.isa.ui.eventhandlers.ButtonClickedEventHandler#buttonClickedEvent(org.apache.struts.action.ActionMapping, com.sap.isa.ui.controllers.ActionForm, com.sapportals.htmlb.event.ButtonClickEvent, javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse)
	 */
	public ActionForward buttonClickedEvent(
		ActionMapping mapping,
		ActionForm form,
		ButtonClickEvent event,
		HttpServletRequest request,
		HttpServletResponse response)
		throws ServletException, IOException {

		ManualWinnerDetailsForm aForm = (ManualWinnerDetailsForm) form;
		
		String forwards = ActionForwards.SUCCESS;
		int index = Integer.parseInt(event.getAction());		
		switch(index){
			case ManualWinnerDetailsForm.MW_AUCTIONDETAILSINDEX:
				aForm.initializeDetails();
				forwards = ActionForwards.MWDETAILS;
				break;
			case ManualWinnerDetailsForm.MW_AUCTIONBIDSINDEX:
				aForm.initializeBidInfo();
				forwards = ActionForwards.MWBIDS;
				break;
			case ManualWinnerDetailsForm.MW_SUMMARYINDEX:
				aForm.initializeSummaryInfo();
				forwards = ActionForwards.MWSUMMARY;
			
		}
		return mapping.findForward(forwards);
	}

}
