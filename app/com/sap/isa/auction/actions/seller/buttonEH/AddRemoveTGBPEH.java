/*
 * Created on Apr 30, 2004
 *
 * To change the template for this generated file go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
package com.sap.isa.auction.actions.seller.buttonEH;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.sap.isa.auction.actionforms.seller.AuctionEditForm;
import com.sap.isa.auction.actionforms.seller.CreateSelectionForm;
import com.sap.isa.auction.actionforms.seller.TargetGroupsForm;
import com.sap.isa.auction.actions.seller.ActionForwards;
import com.sap.isa.ui.controllers.ActionForm;
import com.sap.isa.ui.eventhandlers.ButtonClickedEventHandler;
import com.sapportals.htmlb.event.ButtonClickEvent;

/**
 * @author I803067
 *
 * To change the template for this generated type comment go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
public class AddRemoveTGBPEH extends ButtonClickedEventHandler  {

	/* (non-Javadoc)
	 * @see com.sap.isa.ui.eventhandlers.ButtonClickedEventHandler#buttonClickedEvent(org.apache.struts.action.ActionMapping, com.sap.isa.ui.controllers.ActionForm, com.sapportals.htmlb.event.ButtonClickEvent, javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse)
	 */
	public ActionForward buttonClickedEvent(
		ActionMapping mapping,
		ActionForm actionForm,
		ButtonClickEvent event,
		HttpServletRequest request,
		HttpServletResponse response)
		throws ServletException, IOException {
		if(actionForm instanceof TargetGroupsForm)  {
			TargetGroupsForm targetForm = (TargetGroupsForm)actionForm;
			if(event.getAction().equals(TargetGroupsForm.ADDTOTARGETEVENT))  {
				targetForm.updateTGSelection();
			}  else  {
				targetForm.removeSelectedTG();
			}
			return mapping.findForward(ActionForwards.SUCCESS);
		}  else if(actionForm instanceof CreateSelectionForm)  {
			
			if(event.getAction().equals(CreateSelectionForm.ADDMORETARGETGROUPSEVENT))  {
				return mapping.findForward(ActionForwards.ADDTARGETS);
			}  else if(event.getAction().equals(CreateSelectionForm.REMOVETARGETEVENT))  {
				((CreateSelectionForm)actionForm).removeTargetGroups();
				return mapping.findForward(ActionForwards.SUCCESS);
			}
		}  else if(actionForm instanceof AuctionEditForm)  {
			if(event.getAction().equals(AuctionEditForm.ADDMORETARGETGROUPSEVENT))  {
				return mapping.findForward(ActionForwards.ADDTARGETS);
			}  else  {
				((AuctionEditForm)actionForm).removeTargetGroups();
				return mapping.findForward(ActionForwards.EDITTARGETS);
			}
		}
		return mapping.findForward(ActionForwards.SUCCESS);
	}

}
