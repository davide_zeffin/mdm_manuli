package com.sap.isa.auction.actions.seller.buttonEH;

//Struts imports
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.sap.isa.auction.actionforms.seller.AuctionEditForm;
import com.sap.isa.auction.actionforms.seller.AuctionSearchForm;
import com.sap.isa.auction.actions.seller.ActionForwards;
import com.sap.isa.ui.controllers.ActionForm;
import com.sap.isa.ui.eventhandlers.ButtonClickedEventHandler;
import com.sapportals.htmlb.event.ButtonClickEvent;

/**
 * Title:			Internet Sales Private Auctions
 * Description:
 * @Copyright: 		Copyright (c) 2004
 * Company:			SAPLabs LLC
 * @author 
 * Created on: 		May 14, 2004
 */
/**
 * Title:
 * Description:
 * Copyright:    Copyright (c) 2003
 * Company:
 * @author
 * @version 1.0
 */

public class CopyAuctionEH extends ButtonClickedEventHandler {

    public CopyAuctionEH() {
    }

    public ActionForward buttonClickedEvent(ActionMapping mapping,
                                          ActionForm form,
                                          ButtonClickEvent event,
                                          HttpServletRequest request,
                                          HttpServletResponse response)
                                          throws java.io.IOException, javax.servlet.ServletException {

		AuctionSearchForm searchForm = (AuctionSearchForm)form;
        searchForm.copyAuction(request.getParameter(AuctionEditForm.COPYTARGET));
        return mapping.findForward(ActionForwards.COPY);
    }
}
