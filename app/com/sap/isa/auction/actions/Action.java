package com.sap.isa.auction.actions;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.sap.isa.auction.actionforms.Form;
import com.sap.isa.ui.controllers.ActionForm;

/**
 * Title:
 * Description:
 * Copyright:    Copyright (c) 2002
 * Company:
 * @author
 * @version 1.0
 */

public class Action extends com.sap.isa.ui.controllers.Action {

  public static final String ERRORPAGE = "auctionerror";

  public Action() {
  }

  protected void preProcessing(ActionForm form)  {
      ((Form)form).preProcessing();
  }

  protected final ActionForward doServiceRequest(ActionMapping mapping, ActionForm actionForm, HttpServletRequest request, HttpServletResponse response) throws javax.servlet.ServletException, java.io.IOException {
      Form processForm = ((Form)actionForm);
      try  {

          return doService(mapping, (Form)actionForm, request, response);

        }  catch(Exception exc)  {
            log.error("Unknown Exception " , exc);
            processForm.addError(exc.getMessage());
        } catch (Throwable t){//catch runtime exceptions
			log.error("Unknown Exception " , t);
			processForm.addError(t.getMessage());
        }
        return mapping.findForward(ERRORPAGE);
  }

  protected ActionForward doService(ActionMapping mapping, Form actionForm, HttpServletRequest request, HttpServletResponse response) throws javax.servlet.ServletException, java.io.IOException {
      return new ActionForward(mapping.getInput());
  }

  protected void postProcessing(ActionForm form)  {
      ((Form)form).postProcessing();
  }

  protected final ActionForward dispatchEventHandler(ActionMapping mapping, ActionForm actionForm, HttpServletRequest request, HttpServletResponse response) throws javax.servlet.ServletException, java.io.IOException {
      Form processForm = ((Form)actionForm);
      try  {

          return super.dispatchEventHandler( mapping,  actionForm,  request,  response);

        }  catch(Exception exc)  {
            log.error("Unknown Exception " , exc);
        } catch (Throwable t){ //catch runtime exceptions
			log.error("Unknown Exception " , t);
		}
        return mapping.findForward(ERRORPAGE);
  }
}
