package com.sap.isa.auction.actions;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.sap.isa.auction.actionforms.BaseForm;
import com.sap.isa.auction.actionforms.LoginForm;

/**
 * Routes the user request to UI corresponding to the role
 * If the user is a seller, forward to Seller UI
 * If the user is admin forward to SVE administrator UI
 * Make the user logon again, if the user is neither an admin nor a seller
 * 			<forward name="admin" path="/aucadm/main.do"/>
			<forward name="shops" path="/seller/shops.do"/>
			<forward name="main" path="/seller/main.do"/>
 */

public class LoginRoleAction extends  BaseAction{

  public LoginRoleAction() {
  }

   
  protected ActionForward doService(ActionMapping mapping,
									BaseForm actionForm,
									HttpServletRequest request,
									HttpServletResponse response)
									throws java.io.IOException, javax.servlet.ServletException {

	  LoginForm loginForm = (LoginForm)actionForm;


		if(loginForm.isAdminLogin())  {
			return mapping.findForward(ActionForwards.ADMIN);
		}
		if(loginForm.loadCatalog())  {
			return mapping.findForward(ActionForwards.MAIN);
		}  else  {
			return mapping.findForward(ActionForwards.SHOPS);
		}

  }




}
