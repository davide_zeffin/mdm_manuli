/*
 * Created on May 9, 2004
 *
 * To change the template for this generated file go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
package com.sap.isa.auction.actions;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.Action;

import com.sap.tc.logging.Category;
import com.sap.tc.logging.Location;
import com.sap.isa.auction.actionforms.LogOffForm;
import com.sap.isa.auction.actions.seller.ActionForwards;
import com.sap.isa.auction.logging.CategoryProvider;
import com.sapportals.htmlb.rendering.PageContext;
import com.sapportals.htmlb.rendering.PageContextFactory;

/**
 * @author I803067
 *
 * To change the template for this generated type comment go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
public class LogOffAction extends Action  {

	public static final String INVALIDSESSIONPARAMETER = "invalidsession";

	protected static Location tracer = Location.getLocation(LogOffAction.class);
	protected static Category logger = CategoryProvider.getUICategory();

	/* (non-Javadoc)
	 * @see org.apache.struts.action.Action#perform(org.apache.struts.action.ActionMapping, org.apache.struts.action.ActionForm, javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse)
	 */
	public ActionForward perform(
		ActionMapping mapping,
		ActionForm form,
		HttpServletRequest request,
		HttpServletResponse response)
		throws IOException, ServletException {
			final String METHOD = "perform";
			tracer.entering(logger, METHOD);

			LogOffForm aForm = (LogOffForm)form;
			PageContext pageContext = 
					(PageContext) PageContextFactory.createPageContext(
							request , response);
								
			aForm.setRequest(request);
			aForm.setResponse(response);
			aForm.setServlet(servlet);
			aForm.setSession(request.getSession());
			aForm.setPageContext(pageContext);
			request.setAttribute("actionForm" , aForm);			
			if(request.getParameter(INVALIDSESSIONPARAMETER) != null){
				logger.infoT(tracer, "Logging off with invalid session");
				aForm.logOff(true);
			} else {
				logger.infoT(tracer, "Logging off manually");
				aForm.logOff(false);
			}
			
			tracer.exiting(METHOD, logger);
			return mapping.findForward(ActionForwards.SUCCESS);
	}

}
