/*
 * Created on May 21, 2004
 *
 * To change the template for this generated file go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
package com.sap.isa.auction.helpers;

import com.sap.isa.auction.businessobject.AuctionBusinessObjectManagerBase;
import com.sap.isa.auction.businessobject.b2x.AuctionBusinessObjectManager;
import com.sap.isa.auction.businessobject.user.AuctionUser;
import com.sap.isa.core.UserSessionData;
import com.sap.isa.user.businessobject.UserBase;

/**
 * @author I803067
 *
 * To change the template for this generated type comment go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
public class SellerCurrencyHelperImpl extends GenericHelper implements CurrencyHelper  {

	protected AuctionUser user;
	/* (non-Javadoc)
	 * @see com.sap.isa.auction.helpers.CurrencyHelper#getAuctionUser()
	 */
	public AuctionUser getAuctionUser() {
		final String methodName = "getAuctionUser";
		entering(methodName);
		
		AuctionBusinessObjectManagerBase auctionBOM = getAuctionBOM();
		if(null == user)  {
			user = auctionBOM.createAuctionUser();
			user.setUserDefaults(getUser().getUserId());
		}
		exiting(methodName);
		return user;
	}
	
	protected UserBase getUser()  {
		AuctionBusinessObjectManager bom = (AuctionBusinessObjectManager)getAuctionBOM();
		return bom.getUserBase();
	}

	/* (non-Javadoc)
	 * @see com.sap.isa.auction.helpers.GenericHelperBase#getCurrency()
	 */
	public String getCurrency() {
		String currency = "";
		UserSessionData userSession = UserSessionData.getUserSessionData(helpManager.getSession());
		AuctionBusinessObjectManager bom = (AuctionBusinessObjectManager)
				userSession.getBOM(AuctionBusinessObjectManager.AUCTION_BOM);
		try  {
			return bom.getShop().getCurrency();
		}  catch(Exception exc)  {
			logError("Unable to retrieve the Currency from the Shop" , exc);
		}
		return  "";
	}

}
