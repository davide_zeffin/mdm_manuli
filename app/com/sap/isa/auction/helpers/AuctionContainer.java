/*
 * Created on Nov 30, 2004
 *
 * To change the template for this generated file go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
package com.sap.isa.auction.helpers;

import java.util.Date;

import com.sap.isa.auction.bean.Auction;

/**
 * @author I803067
 *
 * To change the template for this generated type comment go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
public class AuctionContainer {
	
	public static final int NEW 		= 1;
	public static final int MODIFIED 	= 5;
	public static final int SAVED 		= 2;
	public static final int SCHEDULED 	= 3;
	public static final int PUBLISH 	= 4;
	
	public static final int OPENSCH		 = 1;
	public static final int PUBLISHNOW	 = 2;
	public static final int SCHEDULEDIT  = 3;
	
	private Auction auction;
	private int status = NEW;
	private int publishStatus = OPENSCH;
	private Object errorObject;
	private Date scheduleDate;
	 
	
	public AuctionContainer(Auction auction)  {
		this.auction = auction;
	}
	
	public void setAuction(Auction auction)  {
		this.auction = auction;
	}
	
	public Auction getAuction()  {
		return auction;
	}
	
	public int getStatus()  {
		return status;
	}
	
	public void setStatus(int status)  {
		this.status = status;
	}
	
	public void setError(Object error)  {
		this.errorObject = error;
	}
	
	public Object getError()  {
		return this.errorObject;
	}
	
	public void setPublishStatus(int status)  {
		this.publishStatus = status;
	}
	
	public int getPublishStatus()  {
		return publishStatus;
	}
	
	public void setScheduleDate(Date date)  {
		this.scheduleDate = date;
	}
	
	public Date getScheduleDate()  {
		return scheduleDate;
	}

	public void clean() {
		auction = null;		
		errorObject = null;
		scheduleDate = null;
	}
	
}
