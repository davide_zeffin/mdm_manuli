/*
 * Created on May 8, 2004
 *
 * To change the template for this generated file go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
package com.sap.isa.auction.helpers;

import java.util.Collection;

import com.sap.isa.catalog.webcatalog.WebCatItem;

/**
 * @author I803067
 *
 * To change the template for this generated type comment go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
public interface ProductsContainer {

	public Collection getSelectedItems() ;
	public int getSelectionCount();
	public boolean addItem(WebCatItem item , double quantity);
	public void removeItem(String id);
	public void clearSelection();
}
