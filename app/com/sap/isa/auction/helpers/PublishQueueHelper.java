package com.sap.isa.auction.helpers;

import java.sql.Timestamp;
import java.util.Date;

import com.sap.isa.auction.bean.AuctionStatusEnum;
import com.sap.isa.auction.bean.b2x.PrivateAuction;
import com.sap.isa.auction.businessobject.b2x.PrivateAuctionManager;

/**
 * Title:			Internet Sales Private Auctions
 * Description:
 * @Copyright: 		Copyright (c) 2004
 * Company:			SAPLabs LLC
 * @author 
 * Created on: 		May 21, 2004
 */
public class PublishQueueHelper extends SearchHelper {

	public static final int CREATION_CONTEXT = 1;
	public static final int DEFAULT_CONTEXT = 2;

	private int context = DEFAULT_CONTEXT;

	public boolean publish(PrivateAuction auction) {
		boolean flag = false;
		PrivateAuctionManager auctionMngr =
			(PrivateAuctionManager) getAuctionBOM().getAuctionManager();
		if (null == auctionMngr) {
			auctionMngr =
				(PrivateAuctionManager) getAuctionBOM().createAuctionManager();
		}
		auction.setUserObject(null);

		try {
			if (AuctionStatusEnum.OPEN.getValue() == auction.getStatus()
				|| AuctionStatusEnum.SCHEDULED.getValue() == auction.getStatus())
				auctionMngr.modifyStatus(auction, AuctionStatusEnum.PUBLISHED);
		} catch (Exception exc) {
			auction.setUserObject(exc);
			logError(
				"Unable to publish the auction - " + auction.getAuctionName(),
				exc);
		}
		return flag;
	}

	public void schedule(PrivateAuction auction, Date date) {
		if (auction != null) {
			auction.setUserObject(null);
		}
		Timestamp prevTimestamp = auction.getPublishDate();
		try {
			auction.setPublishDate(new java.sql.Timestamp(date.getTime()));
			getAuctionManager().modifyStatus(
				auction,
				AuctionStatusEnum.SCHEDULED);
		} catch (Exception exc) {
			logInfo("Unable to schedule the auction", exc);
			auction.setUserObject(exc);
			auction.setPublishDate(prevTimestamp);
		}
	}

	public void withDrawFromPublishing(PrivateAuction auction) {
		if (auction != null) {
			auction.setUserObject(null);
		}
		try {
			auction.setPublishDate(null);
			getAuctionManager().modifyStatus(auction, AuctionStatusEnum.OPEN);
		} catch (Exception exc) {
			logInfo(
				"Unable to remove auction from publishing schedule list",
				exc);
			auction.setUserObject(exc);
		}
	}
	
	public SearchResult getSearchList() {
		if (result != null) {
			cleanDBConnections();
			result.connect();
		}
		return result;
	}
	
	public void setSearchList(SearchResult result) {
		this.result = result;
		clearLastAccess();
	}
	protected void cleanDBConnections() {
		((HomeHelper) helpManager.getHelper(HelpManager.HOMEHELPER))
			.intermediateCleanup();
		((SearchHelper) helpManager.getHelper(HelpManager.SEARCHHELPER))
			.intermediateCleanup();
		((ManualWinnerHelper) helpManager
			.getHelper(HelpManager.MANUALWINNERHELPER))
			.intermediateCleanup();
	}

	/**
	 * @return
	 */
	public int getContext() {
		return context;
	}

	/**
	 * @param i
	 */
	public void setContext(int i) {
		context = i;
	}

}
