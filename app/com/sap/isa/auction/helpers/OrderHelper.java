package com.sap.isa.auction.helpers;

import com.sap.isa.auction.businessobject.AuctionBusinessObjectManagerBase;
import com.sap.isa.auction.businessobject.order.AuctionOrderStatus;
import com.sap.isa.backend.boi.isacore.order.OrderData;
import com.sap.isa.businessobject.CommunicationException;
import com.sap.isa.core.TechKey;
import com.sap.isa.core.UserSessionData;
/**
 * Title:
 * Description:
 * Copyright:    Copyright (c) 2003
 * Company:
 * @author
 * @version 1.0
 */

public class OrderHelper extends GenericHelper {

	public OrderHelper() {
	}
	/**
	 * Load the Order object based on order GUID
	 *
	 * @param orderGUID Order GUID
	 * @return HeaderData object contains all the orderHeader details
	 */
	public OrderData loadOrderById(TechKey orderGUID) {
		final String METHOD = "loadOrderById";
		entering(METHOD);
		if (orderGUID == null)
			return null;
		logInfo("Get the bom");

		AuctionBusinessObjectManagerBase bom = getBOM();
		AuctionOrderStatus orderRead = bom.getAuctionOrderStatus();
		if (orderRead == null) {
			orderRead = bom.createAuctionOrderStatus();
		}

		try {
			orderRead.readOrderStatus(orderGUID, null);
			return orderRead.getOrder();
		} catch (CommunicationException ex) {
			String errMsg =
				"Error reading order header info from the backend"
					+ ex.getMessage();
			logInfo(errMsg, ex);
		}
		exiting(METHOD);
		return null;
	}

	private AuctionBusinessObjectManagerBase getBOM() {
		final String METHOD = "getBOM";
		entering(METHOD);
		UserSessionData userSession =
			UserSessionData.getUserSessionData(helpManager.getSession());
		AuctionBusinessObjectManagerBase bom =
			(AuctionBusinessObjectManagerBase) userSession.getBOM(
				AuctionBusinessObjectManagerBase.AUCTION_BOM);
		exiting(METHOD);
		return bom;

	}

}
