package com.sap.isa.auction.helpers;

/**
 * Title:
 * Description:
 * Copyright:    Copyright (c) 2003
 * Company:
 * @author
 * @version 1.0
 */

public interface Helper {
    public void initialize(HelpManagerBase manager);
    public void clean();
    public void intermediateCleanup();
}