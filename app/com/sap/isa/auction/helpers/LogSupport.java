package com.sap.isa.auction.helpers;

import com.sap.tc.logging.Category;
import com.sap.tc.logging.Location;
import com.sap.isa.auction.logging.CategoryProvider;
import com.sap.isa.auction.logging.LogHelper;
import com.sap.isa.auction.logging.TraceHelper;

public class LogSupport {

	protected static Category cat = CategoryProvider.getUICategory();
	protected static Location location;
	public static final String ENTERING = "Entering - ";
	public static final String EXIT = "Exiting - ";

	public LogSupport() {
		location = Location.getLocation(this.getClass());
	}

	protected void entering(String text) {
		location = Location.getLocation(this.getClass());
		entering(cat, location, text, null);
	}

	protected void logInfo(String text) {
		location = Location.getLocation(this.getClass());
		logInfo(cat, location, text);
	}

	protected void logInfo(String text, Throwable exc) {
		location = Location.getLocation(this.getClass());
		logError(cat, location, text, exc);
	}
	protected void logError(String text, Throwable exc) {
		location = Location.getLocation(this.getClass());
		logError(cat, location, text, exc);
	}

	protected void logWarning(String text) {
		location = Location.getLocation(this.getClass());
		logWarning(cat, location, text);
	}

	protected void logWarning(String text, Throwable exc) {
		location = Location.getLocation(this.getClass());
		logWarning(cat, location, text, exc);
	}

	protected void logDebug(String text) {
		location = Location.getLocation(this.getClass());
		logDebug(cat, location, text, null);
	}
	protected void exiting(String text) {
		location = Location.getLocation(this.getClass());
		exiting(cat, location, EXIT + text, null);
	}

	public static void entering(
		Category cat,
		Location location,
		String text,
		Object args[]) {
		TraceHelper.entering(location, text, args);
	}

	public static void exiting(
		Category cat,
		Location location,
		String text,
		Object args[]) {
		TraceHelper.exiting(location, text, args);
	}

	public static void logInfo(Category cat, Location location, String text) {
		LogHelper.logInfo(cat, location, text, null);
	}

	public static void logError(
		Category cat,
		Location location,
		String text,
		Throwable throwable) {
		LogHelper.logError(cat, location, text, throwable);
	}

	public static void logDebug(
		Category cat,
		Location location,
		String text,
		Object args[]) {
		TraceHelper.logDebug(location, cat, text, args);
	}

	public static void logWarning(
		Category cat,
		Location location,
		String text,
		Throwable throwable) {
		LogHelper.logWarning(cat, location, text, throwable);
	}

	public static void logWarning(
		Category cat,
		Location location,
		String text) {
		LogHelper.logError(cat, location, text, null);
	}

}