/*
 * Created on May 21, 2004
 *
 * To change the template for this generated file go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
package com.sap.isa.auction.helpers.buyer;

import com.sap.isa.auction.helpers.SellerCurrencyHelperImpl;
import com.sap.isa.businessobject.BusinessObjectManager;
import com.sap.isa.core.UserSessionData;
import com.sap.isa.user.businessobject.UserBase;

/**
 * @author I803067
 *
 * To change the template for this generated type comment go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
public class BuyerCurrencyHelper extends SellerCurrencyHelperImpl {

	protected UserBase getUser() {
		UserSessionData userSession = UserSessionData.getUserSessionData(helpManager.getSession());
		BusinessObjectManager bom = (BusinessObjectManager)
				userSession.getBOM(BusinessObjectManager.ISACORE_BOM);
		return bom.getUser();
	}
	
	/* (non-Javadoc)
	 * @see com.sap.isa.auction.helpers.GenericHelperBase#getCurrency()
	 */
	public String getCurrency() {
		String currency = "";
		UserSessionData userSession = UserSessionData.getUserSessionData(helpManager.getSession());
		BusinessObjectManager bom = (BusinessObjectManager)
				userSession.getBOM(BusinessObjectManager.ISACORE_BOM);
		try  {
			return bom.getShop().getCurrency();
		}  catch(Exception exc)  {
			logError("Unable to retrieve the Currency from the Shop" , exc);
		}
		return  "";
	}
	
}
