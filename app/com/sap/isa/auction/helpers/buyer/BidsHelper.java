/*
 * Created on May 18, 2004
 *
 * To change the template for this generated file go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
package com.sap.isa.auction.helpers.buyer;

import java.math.BigDecimal;
import java.util.Collection;

import com.sap.isa.auction.bean.Bid;
import com.sap.isa.auction.bean.b2x.PrivateAuction;
import com.sap.isa.auction.bean.b2x.PrivateAuctionBid;
import com.sap.isa.auction.bean.b2x.PrivateAuctionWinner;
import com.sap.isa.auction.businessobject.BidManager;
import com.sap.isa.auction.businessobject.WinnerManager;
import com.sap.isa.auction.businessobject.b2x.BidMgrImpl;
import com.sap.isa.auction.businessobject.b2x.WinnerMgrImpl;
import com.sap.isa.businessobject.BusinessObjectManager;
import com.sap.isa.businesspartner.businessobject.BusinessPartner;
import com.sap.isa.businesspartner.businessobject.BusinessPartnerManager;
import com.sap.isa.core.UserSessionData;

/**
 * @author I803067
 *
 * To change the template for this generated type comment go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
public class BidsHelper extends BuyerHelperBase {

	public BigDecimal getHighestBid(PrivateAuction auction)  {
		
		BigDecimal value = new BigDecimal(0.0);
		try  {
			Bid[] bids = getBidMngr().getHighestBids(auction);
			if(bids != null && bids.length > 0)  {
				return bids[0].getBidAmount();
			}
		}  catch(Exception exc)  {
			logError("Unable to retrieve the bids for auction - " + auction.getAuctionName() , exc);		
		}
		return value;
	}
	
	public BigDecimal getMyLastBid(PrivateAuction auction)  {
		
		BigDecimal value = new BigDecimal(0.0);
		try  {
			Bid[] bids = getBidMngr().getHighestBids(auction);
			if(bids != null && bids.length > 0)  {
				return bids[0].getBidAmount();
			}
		}  catch(Exception exc)  {
			logError("Unable to retrieve the bids for auction - " + auction.getAuctionName() , exc);		
		}
		return value;
	}

	public boolean placeBid(PrivateAuction auction, BigDecimal value , double quantity) {
		setError(null);
		try  {
			PrivateAuctionBid bid = new PrivateAuctionBid();
			bid.setBuyerId(getBusinessPartnerId());
			bid.setUserId(getLoggedInUserId());
			bid.setSoldToParty(getSoldTo());
			bid.setBidAmount(value);
			bid.setQuantity((int)quantity);
			((BidMgrImpl)getBidMngr()).createBid(auction , bid);
		}  catch(Exception exc)  {
			logError("Unable to place a bid on auction - " + auction.getAuctionName() , exc);
			setError(exc);
			return false;
		}
		return true;
	}
	
	public Collection getBids(PrivateAuction auction)  {
		try  {
			
			return ((BidMgrImpl)getBidMngr()).getBidHistory(auction);
		}  catch(Exception exc)  {
			logError("Unable to retrieve bids for the auction - " + auction.getAuctionName() , exc);
			return null;
		}
	}

	private BidManager getBidMngr()  {
		BidManager bidManager = (BidMgrImpl)getAuctionBOM().getBidManager();
		if(null == bidManager)  {
			bidManager = (BidManager)getAuctionBOM().createBidManager();
		}
		return bidManager;
	}

	/**
	 * @param businessPartnerId
	 * @return
	 */
	public String getBusinessPartnerName(String businessPartnerId) {
		UserSessionData userSession = UserSessionData.getUserSessionData(helpManager.getSession());
		BusinessObjectManager isaBOM = (BusinessObjectManager)
					userSession.getBOM(BusinessObjectManager.ISACORE_BOM);
		try  {
			BusinessPartnerManager buPaMa = isaBOM.getBUPAManager();
			BusinessPartner partner = buPaMa.getBusinessPartner(businessPartnerId);
			return partner.getAddress().getLastName() + " " + partner.getAddress().getFirstName(); 
		}  catch(Exception exc)  {
			logError("Unable to fetch the Business partner - " + businessPartnerId , exc);
		}
		return null;
	}
	
	public String getLoggedInUserId() {
		String userId = "";
		UserSessionData userSession = UserSessionData.getUserSessionData(helpManager.getSession());
		BusinessObjectManager isaBOM = (BusinessObjectManager)
					userSession.getBOM(BusinessObjectManager.ISACORE_BOM);
		userId = isaBOM.getUser().getUserId();
		return userId;
	}
	public boolean buyNow(PrivateAuction auction , BigDecimal quantity)  {
		boolean flag = false;
		setError(null);
		try  {
			
			WinnerManager winnerManager = getAuctionBOM().getWinnerManager();
			if(null == winnerManager)  {
				winnerManager = getAuctionBOM().createWinnerManager();
			}
			PrivateAuctionWinner winner = new PrivateAuctionWinner();
			winner.setBidId("01");
			winner.setUserId(getLoggedInUserId());
			winner.setBidAmount(auction.getBuyItNowPrice());
			winner.setQuantity(quantity);
			winner.setBuyerId(getBusinessPartnerId());
			((WinnerMgrImpl)winnerManager).createWinner(winner , auction);
			return true;
		}  catch(Exception exc)  {
			logError("Unable to create winner with for the buy now price for auction - " + (auction != null ? auction.getAuctionName() : "-NULL") , exc);
			setError(exc);
		}
		return flag; 
	}
}
