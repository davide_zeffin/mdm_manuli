/*
 * Created on May 27, 2004
 *
 * To change the template for this generated file go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
package com.sap.isa.auction.helpers.buyer;

import com.sap.isa.auction.bean.AuctionStatusEnum;
import com.sap.isa.auction.bean.Winner;
import com.sap.isa.auction.bean.b2x.PrivateAuction;
import com.sap.isa.auction.bean.b2x.PrivateAuctionWinner;
import com.sap.isa.auction.businessobject.WinnerManager;
import com.sap.isa.auction.businessobject.b2x.AuctionBusinessObjectManager;
import com.sap.isa.auction.businessobject.b2x.PrivateAuctionManager;
import com.sap.isa.auction.businessobject.b2x.WinnerMgrImpl;
import com.sap.isa.auction.businessobject.order.AuctionOrder;
import com.sap.isa.auction.helpers.HelpManager;
import com.sap.isa.backend.boi.isacore.order.DocumentListFilterData;
import com.sap.isa.businessobject.BusinessObjectManager;
import com.sap.isa.businessobject.CommunicationException;
import com.sap.isa.businessobject.DocumentState;
import com.sap.isa.businessobject.SalesDocument;
import com.sap.isa.businessobject.Shop;
import com.sap.isa.businessobject.header.HeaderSalesDocument;
import com.sap.isa.businessobject.item.ItemList;
import com.sap.isa.businessobject.item.ItemSalesDoc;
import com.sap.isa.businessobject.order.Order;
import com.sap.isa.businessobject.order.OrderDataContainer;
import com.sap.isa.businessobject.order.OrderStatus;
import com.sap.isa.core.SessionConst;
import com.sap.isa.core.TechKey;
import com.sap.isa.core.UserSessionData;
import com.sap.isa.isacore.DocumentHandler;
import com.sap.isa.isacore.ManagedDocument;

/**
 * @author I803067
 *
 * To change the template for this generated type comment go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
public class CheckoutHelper extends BuyerHelperBase {

	private PrivateAuction auction;

	public OrderStatus initializeAuctionOrder(PrivateAuction auction) {
		OrderStatus orderStatusDetail = null;
		try {

			Winner[] winners = getWinnerManager().getWinners(auction);
			String businessPartnerId = getBusinessPartnerId();
			Winner winner = null;
			if (winners != null && winners.length > 0) {
				for (int index = 0; index < winners.length; index++) {
					if (winners[index]
						.getBuyerId()
						.equals(businessPartnerId)) {
						winner = winners[index];
						break;
					}
				}
			}
			if (winner != null) {
				UserSessionData userSessionData =
					UserSessionData.getUserSessionData(
						helpManager.getSession());
				BusinessObjectManager bom =
					(BusinessObjectManager) userSessionData.getBOM(
						BusinessObjectManager.ISACORE_BOM);

				orderStatusDetail = getSalesDocumentStatus(bom);
				TechKey documentKey = winner.getOrderGuid();
				String documentId = winner.getOrderId();
				String documentsOrigin = "";
				String documentType =
					DocumentListFilterData.SALESDOCUMENT_TYPE_ORDER;
				Shop shop = bom.getShop();
				// Is Shop still available
				if (orderStatusDetail.getShop() == null) {
					orderStatusDetail.setShop(shop);
				}
				// We use OrderStatus and DocumentListFilter as data container for
				// transport purposes between muliple Actions

				// Set attributes given by request
				orderStatusDetail.setTechKey(documentKey);
				orderStatusDetail.setSalesDocumentNumber(documentId);
				orderStatusDetail.setSalesDocumentsOrigin("");
				try {
					orderStatusDetail.readOrderStatus(
						documentKey,
						documentId,
						documentsOrigin,
						documentType);
				} catch (Exception exc) {
					logError(
						"Unable to fetch the order for the auction won - "
							+ (auction != null ? auction.getAuctionName() : ""),
						exc);
					return null;
				}

				HeaderSalesDocument headerSalesDoc =
					orderStatusDetail.getOrderHeader();
				// eAuction integration: Get auction process type

				// eAuction integration: Do not allow documents created via eAuctions to be changed!
				if (documentType != null
					&& documentType.equalsIgnoreCase(
						headerSalesDoc.getProcessType())) {
					headerSalesDoc.setChangeable("");
				}
				enhanceOrderItems(orderStatusDetail);

			}
		} catch (Exception exc) {
			logError(
				"Unable load the order for the checkout - aucitonid - "
					+ auction
					!= null
					? auction.getAuctionId()
					: "null",
				exc);
		}
		this.auction = auction;
		return orderStatusDetail;
	}

	/**
	 * Enhance the Order Items not changeable
	 * @param orderStatusDetail
	 */
	private void enhanceOrderItems(OrderStatus orderStatusDetail) {
		ItemList itemList = orderStatusDetail.getItemList();
		int size = itemList.size();
		// set the flags
		for (int i = 0; i < size; i++) {
			ItemSalesDoc item = itemList.get(i);
			item.setChangeable(false);
			item.setCancelable(false);
			item.setDeletable(false);
			item.setFromAuction(true);
		}
	}

	/**
	 * Returns the order status object.
	 * Detailed OrderStatus contains all of the details of Order Items
	 * 
	 */
	protected OrderStatus getSalesDocumentStatus(BusinessObjectManager bom) {
		OrderStatus orderStatusDetail = bom.getOrderStatus();
		if (orderStatusDetail == null) {
			//			  Order order = new Order();
			OrderDataContainer order = new OrderDataContainer();
			orderStatusDetail = bom.createOrderStatus(order);
		}
		return orderStatusDetail;
	}

	private WinnerMgrImpl getWinnerManager() {
		WinnerMgrImpl winnerMngr =
			(WinnerMgrImpl) getAuctionBOM().getWinnerManager();
		if (null == winnerMngr) {
			winnerMngr = (WinnerMgrImpl) getAuctionBOM().createWinnerManager();
		}
		return winnerMngr;
	}

	public Winner getWinner(PrivateAuction auction) {
		boolean flag = false;
		try {
			WinnerManager winnerMngr = getWinnerManager();
			Winner[] winners = winnerMngr.getWinners(auction);
			Winner winner = null;
			String businessPartnerId = getBusinessPartnerId();
			if (winners != null && winners.length > 0) {
				for (int index = 0; index < winners.length; index++) {
					if (winners[index]
						.getBuyerId()
						.equals(businessPartnerId)) {
						winner = winners[index];
						break;
					}
				}
			}

			return winner;
		} catch (Exception exc) {
			logError(
				"Unable to retrieve winners for auction - "
					+ auction.getAuctionName(),
				exc);
		}
		return null;
	}

	public SalesDocument postProcessingCheckout() {
		//step 1: save order
		UserSessionData userSessionData =
			UserSessionData.getUserSessionData(helpManager.getSession());
		DocumentHandler documentHandler =
			(DocumentHandler) userSessionData.getAttribute(
				SessionConst.DOCUMENT_HANDLER);
		DocumentState targetDocument =
			documentHandler.getDocument(ManagedDocument.TARGET_DOCUMENT);
		SalesDocument doc = (SalesDocument) targetDocument;
		if (targetDocument instanceof Order) {
			Order isaOrder = (Order) targetDocument;
			AuctionOrder aucOrder = ((AuctionBusinessObjectManager)getAuctionBOM()).createOrder(isaOrder);
			doc = aucOrder;
			String orderId = aucOrder.getHeader().getSalesDocNumber();
			PrivateAuctionWinner winner =
				(PrivateAuctionWinner) getWinnerManager().getWinnerByOrderId(
					paddingZerosForBackendId(orderId));
			if (!winner.isCheckedOut()) {
				try {
					logInfo("Start checkout process: Step1: Save order and release delivery block");
					try {
						logInfo("The order ID is" + orderId);
						logInfo(
							"The GUID ID is"
								+ aucOrder.getTechKey().getIdAsString());
						aucOrder.releaseDistributionBlock();
						aucOrder.releaseDeliveryBlock();		
					} catch (Exception ex) {
						logInfo("An error to release system status" + ex.getMessage());
					}					
					aucOrder.saveAndCommit();
					logInfo("Step2: modify winner and auction status");
					winner.setCheckedOut(true);
					if (auction == null)
						auction =
							(
								(SearchAuctionsHelper) helpManager.getHelper(
									HelpManager
										.BUYERSEARCHHELPER))
										.loadAuction(
								winner.getAuctionId());
					if (auction != null) {
						getWinnerManager().updateWinner(winner, auction);
						PrivateAuctionManager auctionManager =
							(PrivateAuctionManager) getAuctionBOM()
								.getAuctionManager();
						if (null == auctionManager) {
							auctionManager =
								(PrivateAuctionManager) getAuctionBOM()
									.createAuctionManager();
						}
						auctionManager.modifyStatus(
							auction,
							AuctionStatusEnum.FINISHED);
						logInfo("End of the checkout process");
					}

				} catch (CommunicationException e) {
					logError("Save order failed. Return", e);
				} catch (Exception ex) {
					logError("Exception happens", ex);
				}
			} else {
				logInfo("auction winner checked out already, just save the order");
				try {
					aucOrder.saveAndCommit();
				} catch (CommunicationException e) {
					logError("Save order failed. Return", e);
				}
			}

		} else {
			logInfo("Document is not the correct type: AuctionOrder type.");
		}
		return doc;
	}

}
