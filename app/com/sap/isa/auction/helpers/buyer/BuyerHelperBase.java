/*
 * Created on May 14, 2004
 *
 * To change the template for this generated file go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
package com.sap.isa.auction.helpers.buyer;

import java.util.Collection;
import java.util.HashMap;

import com.sap.isa.auction.businessobject.AuctionBusinessObjectManagerBase;
import com.sap.isa.auction.businessobject.b2x.AuctionBusinessObjectManager;
import com.sap.isa.auction.businessobject.b2x.ProcessBusinessObjectManager;
import com.sap.isa.auction.businessobject.management.ProcessMetaBusinessObjectManager;
import com.sap.isa.auction.businessobject.targetGroup.TargetGroupList;
import com.sap.isa.auction.businessobject.targetGroup.TargetGroupSearch;
import com.sap.isa.auction.services.ServiceLocator;
import com.sap.isa.businessobject.BusinessObjectManager;
import com.sap.isa.businessobject.CommunicationException;
import com.sap.isa.businessobject.Shop;
import com.sap.isa.businesspartner.businessobject.BusinessPartner;
import com.sap.isa.businesspartner.businessobject.BusinessPartnerManager;
import com.sap.isa.businesspartner.businessobject.PartnerFunctionBase;
import com.sap.isa.core.UserSessionData;

/**
 * @author I803067
 *
 * To change the template for this generated type comment go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
public class BuyerHelperBase extends com.sap.isa.auction.helpers.GenericHelper {
	
	protected Object error;

	/* (non-Javadoc)
	 * @see com.sap.isa.auction.helpers.GenericHelper#getAuctionBOM()
	 */
	public AuctionBusinessObjectManagerBase getAuctionBOM() {
		UserSessionData userSessionData = UserSessionData.getUserSessionData(helpManager.getSession());
		AuctionBusinessObjectManager auctionBOM = (AuctionBusinessObjectManager)
					userSessionData.getBOM(AuctionBusinessObjectManager.AUCTION_BOM);
		if(null == auctionBOM.getServiceLocatorBase())  {
			
			String scenarioName = userSessionData.getCurrentConfigContainer().getScenarioName();
			try  {
				auctionBOM.setServiceLocator(ServiceLocator.getBaseInstance(scenarioName));
				auctionBOM.setProcessBusinessObjectManager(
					(ProcessBusinessObjectManager) ProcessMetaBusinessObjectManager
						.getInstance(scenarioName)
						.getBOM(ProcessBusinessObjectManager.class));
			}  catch(Exception exc)  {
				logError("Unable to set the service location information to the Auctionbom" , exc);
			}
		}
		return auctionBOM;
	}
	
	public String getBusinessPartnerId()  {
		UserSessionData userSession = UserSessionData.getUserSessionData(helpManager.getSession());
		BusinessObjectManager isaBOM = (BusinessObjectManager)
					userSession.getBOM(BusinessObjectManager.ISACORE_BOM);
		BusinessPartnerManager buPaMa = isaBOM.getBUPAManager();

		BusinessPartner partner = buPaMa.getDefaultBusinessPartner(PartnerFunctionBase.CONTACT);
		if(partner != null)  {
			StringBuffer buf = new StringBuffer(partner.getId());
			return paddingZerosForBackendId(partner.getId());		
		}
		return "";
	}
	protected String getSoldTo()  {
		UserSessionData userSession = UserSessionData.getUserSessionData(helpManager.getSession());
		BusinessObjectManager isaBOM = (BusinessObjectManager)
					userSession.getBOM(BusinessObjectManager.ISACORE_BOM);
		BusinessPartnerManager buPaMa = isaBOM.getBUPAManager();

		BusinessPartner partner = null;
		try  {
			partner = buPaMa.getDefaultBusinessPartner(PartnerFunctionBase.SOLDTO);
			if(partner != null)  {
				return paddingZerosForBackendId(partner.getId());
			}
		}  catch(Exception exc)  {
			logError("Unable to retrieve the Sold To party " , exc);
		}
		return "";
	}
	public Shop getShop() {
		UserSessionData userSession = UserSessionData.getUserSessionData(helpManager.getSession());
		BusinessObjectManager isaBOM = (BusinessObjectManager)
					userSession.getBOM(BusinessObjectManager.ISACORE_BOM);
		return isaBOM.getShop();
		
	}
	public void setError(Object error) {
		this.error = error;
	}
	
	public Object getError() {
		return error;
	}
	
	public String paddingZerosForBackendId(String id)  {
		try {
			Integer.parseInt(id);
		} catch (NumberFormatException e) {
			return id;
		}
		StringBuffer buf = new StringBuffer();
		if(id.length() < 10)  {
			for(int i = id.length(); i < 10; i++)
				buf.append("0");
		}
		buf.append(id);
		return buf.toString();
	}
	
	
	public Collection getTargetGroups()  {
		UserSessionData userSession = UserSessionData.getUserSessionData(helpManager.getSession());
		BusinessObjectManager isaBOM = (BusinessObjectManager)
					userSession.getBOM(BusinessObjectManager.ISACORE_BOM);
		AuctionBusinessObjectManager auctionBOM = (AuctionBusinessObjectManager)userSession.getBOM(AuctionBusinessObjectManager.AUCTION_BOM);
		BusinessPartnerManager buPaMa = isaBOM.getBUPAManager();

		BusinessPartner partner = buPaMa.getDefaultBusinessPartner(PartnerFunctionBase.CONTACT);
		if(partner != null)  {

			TargetGroupSearch targetGroupSearch = new TargetGroupSearch();
			targetGroupSearch.setBpGuid(partner.getTechKey().getIdAsString());
		
			TargetGroupList targetGroupList = auctionBOM.createTargetGroupList();
			
			try {
				HashMap groupMap = targetGroupList.getTargetGroupObjectList(targetGroupSearch);
				if (groupMap!=null) {
					return groupMap.values();
				}
			} catch (CommunicationException exception) {
				logError("Unable to retrieve the targetgroups " , exception);
			}
		}	

		return null;
	}
		

}
