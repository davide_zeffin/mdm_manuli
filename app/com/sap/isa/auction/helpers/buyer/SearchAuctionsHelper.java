/*
 * Created on May 5, 2004
 *
 * To change the template for this generated file go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
package com.sap.isa.auction.helpers.buyer;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Hashtable;
import java.util.Iterator;

import com.sap.isa.auction.bean.AuctionStatusEnum;
import com.sap.isa.auction.bean.OrderStatusEnum;
import com.sap.isa.auction.bean.b2x.PrivateAuction;
import com.sap.isa.auction.bean.search.QueryFilter;
import com.sap.isa.auction.bean.search.QueryResult;
import com.sap.isa.auction.businessobject.AuctionManager;
import com.sap.isa.auction.businessobject.SearchManager;
import com.sap.isa.auction.businessobject.b2x.PrivateAuctionQueryFilter;
import com.sap.isa.auction.helpers.HelpManager;
import com.sap.isa.auction.helpers.HelperObjectManager;
import com.sap.isa.auction.helpers.SearchResult;
import com.sap.isa.businessobject.webcatalog.CatalogBusinessObjectManager;
import com.sap.isa.catalog.webcatalog.WebCatItem;
import com.sap.isa.catalog.webcatalog.WebCatItemList;
import com.sap.isa.core.UserSessionData;
import com.sap.isa.core.util.WebUtil;

/**
 * @author I803067
 *
 * To change the template for this generated type comment go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
public class SearchAuctionsHelper extends BuyerHelperBase  {
	
	static  {
		HelperObjectManager.unRegisterHelperClass(HelpManager.CURRENCYHELPER);
		HelperObjectManager.registerHelperClass(HelpManager.CURRENCYHELPER , BuyerCurrencyHelper.class);	
	}
	
	public static final String DAYS = "AF.Days";
	public static final String HOURS = "AF.Hours";
	public static final String MINUTES = "AF.Minutes";
	
	private Hashtable auctionsByProductID;
	private QueryFilter filter;
	private SearchResult currentResult;
		
	private SearchResult searchAuctions(String productID)  {
		
		QueryResult auctions = null;
		if(auctions != null)  {
			auctions.close();
		}
		SearchManager searchMngr = getSearchManager();
		if(null == filter)
			filter = new PrivateAuctionQueryFilter();
		filter.clear();
		filter.addEqualToCriteria(PrivateAuctionQueryFilter.PRODUCT_GUID , productID);
		auctions = null;
		try  {
			ArrayList bps = new ArrayList();
			bps.add(getBusinessPartnerId());
			
			auctions = searchMngr.searchAuctionsByProduct(getBusinessPartnerId() , productID , getTargetGroups());
		}  catch(Exception exc)  {
			logError("Unable to search for auctions for product " + productID , exc);
		}
		if(auctions != null)
			return new SearchResult(auctions);
		return null;
	}
	
	private SearchManager getSearchManager()  {
		SearchManager searchMng = null;
		searchMng = getAuctionBOM().getSearchManager();
		if(null == searchMng)
			searchMng = getAuctionBOM().createSearchManager();
		return searchMng;
	}
	
	public SearchResult getAuctionsForProductID(String id)  {
		final String METHODNAME = "getAuctionsForProductID";
		entering(METHODNAME);
		SearchResult result = searchAuctions(id);
		exiting(METHODNAME);
		return result;
	}
	
	public boolean isAuctioned(String productId)  {
		try  {
			return getSearchManager().isAuctionExistForProduct(getBusinessPartnerId() , productId , getTargetGroups());
		}  catch(Exception exc)  {
			logError("Unable to look for auctions for a product - " + productId , exc);
			
		}
		return false;
	}
	
	public void lookForAuctionsInCurrentCategory()  {
		final String methodName = "lookForAuctionsInCurrentCategory";
		entering(methodName);
		UserSessionData userSession = UserSessionData.getUserSessionData(helpManager.getSession());
		CatalogBusinessObjectManager catBOM = (CatalogBusinessObjectManager)
					userSession.getBOM(CatalogBusinessObjectManager.CATALOG_BOM);
		try  {
			if(auctionsByProductID != null)  {
				clean();
			}  else  {
				auctionsByProductID = new Hashtable();
			}
			WebCatItemList list = catBOM.getCatalog().getCurrentArea().getItemList();
			if(list != null)  {
				Iterator iterate = list.iterator();
				WebCatItem webCatItem = null;
				SearchResult queryResult = null;
				while(iterate.hasNext())  {
					webCatItem = (WebCatItem)iterate.next();
					queryResult = searchAuctions(webCatItem.getItemID());
					if(queryResult != null)  {
						auctionsByProductID.put(webCatItem.getItemID() , queryResult);
					}
				}
			}
		} catch(Exception exc)  {
			logError("Unable to check for items in the Category " , exc);
		}
		exiting(methodName);
	}
	
	public void clean()  {
		final String methodName = "clean";
		entering(methodName);
		if(auctionsByProductID != null)  {
			Iterator enumerate = auctionsByProductID.values().iterator();
			QueryResult result = null;
			try  {
				while(enumerate.hasNext())  {
					((QueryResult)enumerate.next()).close();
				}
			}  catch(Exception exc)  {
				logInfo("Error in clean() ", exc);
			}
		}
		if (currentResult != null)
			currentResult.close();
		exiting(methodName);
	}
	
	public void setCurrentResult(SearchResult result)  {
		if (currentResult != null)
			currentResult.close();
		currentResult = result;
	}
	
	public SearchResult getCurrentResult()  {
		return currentResult;
	}
	
	public PrivateAuction getAuctionByIndex(String index)  {
		final String methodName = "getAuctionById"; 
		entering(methodName);

		PrivateAuction auction = null;
		int intIndex = 0;
		if(currentResult != null)
			try  {
				intIndex = Integer.parseInt(index);
				auction = null;
				auction = (PrivateAuction)currentResult.fetch(intIndex);
			}  catch(Exception exc)  {
				logError("Unable to get the auction in index - " + index , exc);
			}
		exiting(methodName);
		return auction;
	}
	
	public PrivateAuction refreshAuction(String auctionId) {
		return (PrivateAuction)getAuctionManager().getAuctionById(auctionId);
	}
	public static int calcDateDifference(float calendarItem, Timestamp d1, Timestamp d2) {
		int x = 1000;
		if (calendarItem == Calendar.DAY_OF_MONTH) {
			x = x * 60 * 60 * 24;
		} else if (calendarItem == Calendar.HOUR_OF_DAY) {
			x = x * 60 * 60;
		} else if (calendarItem == Calendar.MINUTE) {
			x = x * 60;
		} else
			return 1;

		return (int) ((d2.getTime() - d1.getTime()) / (x));
	   // seconds in 1 day
	}

	public String getDateDifference(Timestamp from , Timestamp to)  {
		int days = 0;
		int hours = 0;
		int minutes = 0;
		boolean dayInfo = false;
		StringBuffer buff = new StringBuffer();
		UserSessionData userSesn = UserSessionData.getUserSessionData(helpManager.getSession());

		days = calcDateDifference(Calendar.DAY_OF_MONTH , from , to);
		if(days > 1)  {
			buff.append(days);
			buff.append(" ");
			//buff.append(FormUtility.getResourceString(SessionConstants.RESOURCE , DAYS , session));
			buff.append(WebUtil.translate(userSesn.getLocale() , DAYS , null));
			buff.append("+");
			return buff.toString();
		}
		hours = calcDateDifference(Calendar.HOUR_OF_DAY , from , to);
		if(days > 0)  {
			buff.append(days);
			buff.append(" ");
			//buff.append(FormUtility.getResourceString(SessionConstants.RESOURCE , DAYS , session));
			buff.append(WebUtil.translate(userSesn.getLocale() , DAYS , null));
			hours -= 24;
			dayInfo = true;
		}
		if(hours > 1)  {
			buff.append(hours);
			buff.append(" ");
			//buff.append(FormUtility.getResourceString(SessionConstants.RESOURCE , HOURS , session));
			buff.append(WebUtil.translate(userSesn.getLocale() , HOURS , null));
			return buff.toString();
		}
		if(dayInfo)
			return buff.toString();
		minutes = calcDateDifference(Calendar.MINUTE , from , to);
		if(hours > 0)  {
			buff.append(hours);
			buff.append(" ");
			//buff.append(FormUtility.getResourceString(SessionConstants.RESOURCE , HOURS , session));
			buff.append(WebUtil.translate(userSesn.getLocale() , HOURS , null));
			minutes -= 60;
		}
		if(minutes > 0)  {
			buff.append(minutes);
			buff.append(" ");
			//buff.append(FormUtility.getResourceString(SessionConstants.RESOURCE , MINUTES , session));
			buff.append(WebUtil.translate(userSesn.getLocale() , MINUTES , null));
		}
		return buff.toString();
	}
	
	public SearchResult getParticipatedAuctions(AuctionStatusEnum status) {
		try  {
			return new SearchResult(getSearchManager().searchAuctionsParticipatedByBidder(getBusinessPartnerId()
						, getSoldTo(), null , status));
		}  catch(Exception exc)  {
			logError("Unable to get the Active participated auctions" ,exc);
		}
		return null;
	}

//	/**
//	 * 
//	 */
//	public SearchResult getActiveAuctions() {
//		
//		try  {
//			return new SearchResult(getSearchManager().searchAuctionsParticipatedByBidder(getBusinessPartnerId()
//						, getSoldTo(), null , AuctionStatusEnum.PUBLISHED));
//		}  catch(Exception exc)  {
//			logError("Unable to get the Active participated auctions" ,exc);
//		}
//		return null;
//	}
//
//	/**
//	 * 
//	 */
//	public SearchResult getFinalizedAuctions() {
//		try  {
//			return new SearchResult(getSearchManager().searchAuctionsParticipatedByBidder(getBusinessPartnerId()
//						, getSoldTo(), null , AuctionStatusEnum.FINALIZED));
//		}  catch(Exception exc)  {
//			logError("Unable to get the Active Finalized auctions" ,exc);
//		}
//		return null;
//	}
//
//	/**
//	 * 
//	 */
//	public SearchResult getClosedAuctions() {
//		try  {
//			return new SearchResult(getSearchManager().searchAuctionsParticipatedByBidder(getBusinessPartnerId()
//						, getSoldTo(), null , AuctionStatusEnum.NOT_FINALIZED));
//		}  catch(Exception exc)  {
//			logError("Unable to get the closed w/o winners auctions" ,exc);
//		}
//		return null;
//		
//	}

	/**
	 * 
	 */
	public SearchResult getWonAuctions() {
		try  {
			return new SearchResult(getSearchManager().searchAuctionsWonByBidder(getBusinessPartnerId()
						, getSoldTo(), null , OrderStatusEnum.UNCHECKED));
		}  catch(Exception exc)  {
			logError("Unable to get the won but not checked out auctions" ,exc);
		}
		return null;
	}

	/**
	 * 
	 */
	public SearchResult getCheckedout() {
		try  {
			return new SearchResult(getSearchManager().searchAuctionsWonByBidder(getBusinessPartnerId()
						, getSoldTo(), null , OrderStatusEnum.CHECKED));
		}  catch(Exception exc)  {
			logError("Unable to get the won checked out auctions" ,exc);
		}
		return null;
		
	}

	public PrivateAuction loadAuction(String auctionId)  {
		PrivateAuction auction = null;
		AuctionManager auctionMngr = getAuctionBOM().getAuctionManager();
		if(null == auctionMngr)  {
			auctionMngr = getAuctionBOM().createAuctionManager();
		}
		auction = (PrivateAuction) auctionMngr.getAuctionById(auctionId);
		return auction;
	}
}
