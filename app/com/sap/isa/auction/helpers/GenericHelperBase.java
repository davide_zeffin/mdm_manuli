/*
 * Created on Apr 20, 2004
 *
 * To change the template for this generated file go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
package com.sap.isa.auction.helpers;

import com.sap.isa.auction.bean.SAPBackendSystemEnum;
import com.sap.isa.auction.businessobject.AuctionBusinessObjectManagerBase;
import com.sap.isa.auction.businessobject.AuctionManager;
import com.sap.isa.auction.businessobject.user.Seller;
import com.sap.isa.auction.businessobject.user.SellerAdministrator;
import com.sap.isa.auction.services.ServiceLocator;
import com.sap.isa.core.UserSessionData;

/**
 * @author I803067
 *
 * To change the template for this generated type comment go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
public abstract class GenericHelperBase extends LogSupport implements Helper  {

	public static final int INPROCESSMODE = 1;
	public static final int PROCESSCOMPLETEMODE = 2;

	protected HelpManagerBase helpManager;
	protected AuctionManager auctionManager;
	protected ServiceLocator serviceLocator;
	protected int statusOfProcessMode = PROCESSCOMPLETEMODE;
	protected SAPBackendSystemEnum bkSysType;
	protected String currency;
	
	public GenericHelperBase() {
	}

	public void initialize(HelpManagerBase manager) {
		this.helpManager = manager;
	}

	public AuctionManager getAuctionManager() {
		entering("getAuctionManager");
		if (auctionManager == null) {
			auctionManager = getAuctionBOM().createAuctionManager();
		}
		exiting("getAuctionManager");
		return auctionManager;
	}

	protected ServiceLocator getServiceLocatorBase() {
		if (serviceLocator == null) {
			serviceLocator = getAuctionBOM().getServiceLocatorBase();
		}
		return serviceLocator;
	}

	public AuctionBusinessObjectManagerBase getAuctionBOM()  {
		UserSessionData userSession =
			UserSessionData.getUserSessionData(helpManager.getSession());
		AuctionBusinessObjectManagerBase bom =
			(AuctionBusinessObjectManagerBase) userSession.getBOM(
				AuctionBusinessObjectManagerBase.AUCTION_BOM);
		return bom;
	}
	
	public int getProcessStatusMode() {
		return statusOfProcessMode;
	}

	public void setProcessStatusMode(int mode) {
		this.statusOfProcessMode = mode;
	}

	public SAPBackendSystemEnum getSapBackendSystemType(){
		if (bkSysType == null){
			AuctionBusinessObjectManagerBase bom = getAuctionBOM();
			try {
				if (bom.getUserBase() instanceof Seller)
					bkSysType=((Seller)bom.getUserBase()).getSAPBackendSystemType();
				else
					bkSysType=((SellerAdministrator)bom.getUserBase()).getSAPBackendSystemType();
			} catch (Exception e) {
				logError("Unable to get Backend System type" , e);
				bkSysType = null;
			}
		}
		return bkSysType;
	}
	public void clean() {
		serviceLocator = null;
		auctionManager = null;
	}

	public void intermediateCleanup()  {
	
	}

	public String getCurrency()  {
		final String methodName = "getCurrency";
		entering(methodName);
		try {
			if(null == currency)  {
				CurrencyHelper helper = (CurrencyHelper) helpManager.getHelper(HelpManagerBase.CURRENCYHELPER);
				currency = helper.getCurrency();
			}
		}  catch(Exception exc)  {
			logInfo("Unable to get the Currency Information" , exc);
		}
		exiting(methodName);
		return currency;
	}
	
	public void setCurrency(String cur)  {
		this.currency = cur;
	}

}
