package com.sap.isa.auction.helpers;

import com.sap.isa.auction.businessobject.b2x.AuctionRuntime;

/**
 * Title:			Internet Sales Private Auctions
 * Description:
 * @Copyright: 		Copyright (c) 2004
 * Company:			SAPLabs LLC
 * @author 
 * Created on: 		Apr 27, 2004
 */

public class GenericHelper extends GenericHelperBase {
	
	public boolean isApplInitializedProperly() {
		try {
			if(AuctionRuntime.getRuntime().getInitStatus() == AuctionRuntime.INIT_OK)  {
				return true;
			}
		} catch (Exception exc) {
			logInfo("Unable to get the initialized status" , exc);
		}
		return false;
	}
}
