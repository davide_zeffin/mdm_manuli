package com.sap.isa.auction.helpers;

import java.sql.Timestamp;
import java.util.Date;

import com.sap.isa.auction.bean.Auction;
import com.sap.isa.auction.bean.AuctionStatusEnum;
import com.sap.isa.auction.bean.AuctionTypeEnum;
import com.sap.isa.auction.bean.b2x.AuctionVisibilityEnum;
import com.sap.isa.auction.bean.b2x.PrivateAuction;
import com.sap.isa.auction.bean.search.QueryFilter;
import com.sap.isa.auction.bean.search.QueryResult;
import com.sap.isa.auction.businessobject.AuctionBusinessObjectManagerBase;
import com.sap.isa.auction.businessobject.SearchManager;
import com.sap.isa.auction.businessobject.b2x.PrivateAuctionQueryFilter;
import com.sap.isa.core.UserSessionData;

/**
 * Title:			Internet Sales Private Auctions
 * Description:
 * @Copyright: 		Copyright (c) 2004
 * Company:			SAPLabs LLC
 * @author 
 * Created on: 		Apr 27, 2004
 */
public class SearchHelper extends GenericHelper {
	protected QueryFilter genFilter;
	protected QueryFilter advancedFilter;
	protected SearchResult result;
	protected boolean isGenericSearchLastAction = true;
	protected int auctionIdLastAccessed = -1;
	private SearchManager searchMgr = null;

	public static final long LONGVALUEFORDAY = (24L * 60L * 60L * 1000L);

	public SearchHelper() {
	}
	//Generic search
	public SearchResult search(
		String status,
		Date closingDateStringVal,
		Date publishDateStringVal,
		String auctionName,
		String prodName,
		String auctionTitle,
		boolean publishWithErrors) {

		final String METHOD = "search";
		entering(METHOD);
		isGenericSearchLastAction = true;
		intermediateCleanup();
		cleanDBConnections();
		try {
			advancedFilter = null;
			genFilter =
				createGenericQueryFilter(
					status,
					closingDateStringVal,
					publishDateStringVal,
					auctionName,
					prodName,
					auctionTitle,
					publishWithErrors);
			searchAuctionEngine(genFilter);
		} catch (Exception exc) {
			logInfo("Unable to perform generic search", exc);
		}
		exiting(METHOD);
		return result;

	}

	//advancedSearch
	public SearchResult search(
		String auctionName,
		String prodName,
		String auctionTitle,
		String orderId,
		Boolean isNormalAuction,
		Boolean isPrivateAuction,
		Date publishFromDate,
		Date publishTillDate,
		Date closingFromDate,
		Date closingTillDate,
		String status,
		boolean publishWithErrors) {

		final String METHOD = "search";
		entering(METHOD);
		isGenericSearchLastAction = false;
		intermediateCleanup();
		cleanDBConnections();
		try {
			genFilter = null;
			advancedFilter =
				createAdvancedQueryFilter(
					auctionName,
					prodName,
					auctionTitle,
					orderId,
					isNormalAuction,
					isPrivateAuction,
					publishFromDate,
					publishTillDate,
					closingFromDate,
					closingTillDate,
					status,
					publishWithErrors);
			searchAuctionEngine(advancedFilter);
		} catch (Exception exc) {
			logInfo("Unable to perform advanced search function", exc);
		}
		exiting(METHOD);
		return result;
	}

	public QueryFilter getGenericQueryFilter() {
		return genFilter;
	}
	public void setGenericQueryFilter(QueryFilter genericQry) {
		if (genericQry != null)
			isGenericSearchLastAction = true;
		this.genFilter = genericQry;
	}
	public void setAdvancedQueryFilter(QueryFilter advancedQry) {
		if (advancedQry != null)
			isGenericSearchLastAction = false;
		this.advancedFilter = advancedQry;
	}
	public QueryFilter getAdvancedFilter() {
		return advancedFilter;
	}

	public PrivateAuction getAuctionById(String auctionPos) {
		final String METHOD = "getAuctionById";
		entering(METHOD);
		PrivateAuction auction = null;
		auctionIdLastAccessed = -1;
		try {
			auction =
				(PrivateAuction) result.fetch(Integer.parseInt(auctionPos));
			auctionIdLastAccessed = Integer.parseInt(auctionPos);
		} catch (Exception exc) {
			logInfo(
				"Unable to retrieve the auction from query result in position "
					+ auctionPos,
				exc);
		}
		exiting(METHOD);
		return auction;
	}

	public SearchResult refresh() {
		final String METHOD = "refresh";
		entering(METHOD);
		intermediateCleanup();
		cleanDBConnections();
		if (isGenericSearchLastAction) {
			result = searchAuctionEngine(genFilter);
		} else {
			result = searchAuctionEngine(advancedFilter);
		}
		exiting(METHOD);
		return result;
	}

	public void refreshAuction() {
		final String METHOD = "refreshAuction";
		entering(METHOD);
		if (auctionIdLastAccessed != -1) {
			PrivateAuction auction1 =
				(PrivateAuction) result.fetch(auctionIdLastAccessed);
			PrivateAuction auction2 =
				(PrivateAuction) getAuctionManager().getAuctionById(
					auction1.getAuctionId());
			replaceOldAuctionWithRefreshed(auction2, auctionIdLastAccessed);
		}
		exiting(METHOD);
	}

	public String getLastAccessedAuctionId() {
		if (auctionIdLastAccessed > -1)
			return Integer.toString(auctionIdLastAccessed);
		return "";
	}

	public void replaceOldAuctionWithRefreshed(Auction auction, int position) {
		entering("Replacing the old Auction with refreshed");
		result.replaceObjectInPosition(auction, position);
		exiting("Replacing the old Auction with refreshed");
	}

	public SearchResult SearchResultWithConnect() {
		return result;
	}
	private void setResultCollection(QueryResult coll) {
		if (result != null)
			result.close();
		result = null;
		if (coll != null) {
			this.result = new SearchResult(coll);
			if (coll.resultSize() == 0) {
				coll.close();
			}
		}
		auctionIdLastAccessed = -1;
	}

	public void clean() {
		intermediateCleanup();
		if (genFilter != null)
			genFilter = null;
		if (advancedFilter != null)
			advancedFilter = null;
	}

	public void intermediateCleanup() {
		setResultCollection(null);
		if (result != null)
			result.close();
		result = null;
	}
	protected void cleanDBConnections() {
		if (result == null) //in case display result from home page links
			((HomeHelper) helpManager.getHelper(HelpManager.HOMEHELPER))
				.intermediateCleanup();
		((PublishQueueHelper) helpManager
			.getHelper(HelpManager.PUBLISHQUEUEHELPER))
			.intermediateCleanup();
		((ManualWinnerHelper) helpManager
			.getHelper(HelpManager.MANUALWINNERHELPER))
			.intermediateCleanup();
	}
	protected SearchResult searchAuctionEngine(QueryFilter filter) {
		final String METHOD = "searchAuctionEngine";
		entering(METHOD);
		if (filter != null)
			setResultCollection(
				getSearchManager().searchAuctions(filter, null, null, true));

		exiting(METHOD);
		return result;
	}

	QueryFilter createGenericQueryFilter(
		String status,
		Date closingDate,
		Date publishDate,
		String auctionName,
		String prodName,
		String auctionTitle,
		boolean publishWithErrors) {

		final String METHOD = "createGenericQueryFilter";
		entering(METHOD);

		QueryFilter genFilter = new PrivateAuctionQueryFilter();
		auctionIdLastAccessed = -1;
		//clean up the filter to allow a clean search

		if (auctionName != null && !auctionName.equals("")) {
			logInfo("set auction name to the query filter");
			genFilter.addLikeCriteria(
				PrivateAuctionQueryFilter.NAME,
				auctionName.trim());
		}
		if (auctionTitle != null && !auctionTitle.equals("")) {
			logInfo("set auction title to the query filter");
			genFilter.addLikeCriteria(
				PrivateAuctionQueryFilter.TITLE,
				auctionTitle.trim());
		}
		if (status != null) {
			int statusInt = Integer.parseInt(status);
			if (statusInt > 0) {
				logInfo("set auction status to the query filter");
				genFilter.addEqualToCriteria(
					PrivateAuctionQueryFilter.STATUS,
					new Integer(statusInt));
			}
		}
		if (prodName != null) {
			logInfo("set product name to the query filter");
			genFilter.addLikeCriteria(
				PrivateAuctionQueryFilter.PRODUCT_ID,
				prodName);
		}

		if (publishDate != null) {
			logInfo("parse and set publish date to the query filter");
			if (status != null
				&& Integer.parseInt(status)
					== AuctionStatusEnum.SCHEDULED.getValue()) {
				genFilter.addGTEqualToCriteria(
					PrivateAuctionQueryFilter.PUBLISH_DATE,
					new Timestamp(publishDate.getTime()));
				genFilter.addLessThanCriteria(
					PrivateAuctionQueryFilter.PUBLISH_DATE,
					new Timestamp(publishDate.getTime() + LONGVALUEFORDAY));
			} else {
				genFilter.addGTEqualToCriteria(
					PrivateAuctionQueryFilter.START_DATE,
					new Timestamp(publishDate.getTime()));
				genFilter.addLessThanCriteria(
					PrivateAuctionQueryFilter.START_DATE,
					new Timestamp(publishDate.getTime() + LONGVALUEFORDAY));
			}

		}

		if (closingDate != null) {
			logInfo("parse and set closing date to the query filter");

			genFilter.addGTEqualToCriteria(
				PrivateAuctionQueryFilter.END_DATE,
				new Timestamp(closingDate.getTime()));
			genFilter.addLessThanCriteria(
				PrivateAuctionQueryFilter.END_DATE,
				new Timestamp(closingDate.getTime() + LONGVALUEFORDAY));

		}
		if (publishWithErrors) {
			genFilter.addEqualToCriteria(
				PrivateAuctionQueryFilter.PUBLISH_ERROR,
				Boolean.TRUE);
			genFilter.addEqualToCriteria(
				PrivateAuctionQueryFilter.STATUS,
				new Integer(AuctionStatusEnum.OPEN.getValue()));
		}
		exiting(METHOD);
		return genFilter;
	}

	QueryFilter createAdvancedQueryFilter(
		String auctionName,
		String prodName,
		String auctionTitle,
		String orderId,
		Boolean isNormalAuction,
		Boolean isPrivateAuction,
		Date publishFromDate,
		Date publishTillDate,
		Date closingFromDate,
		Date closingTillDate,
		String status,
		boolean publishWithErrors) {

		final String METHOD = "createAdvancedQueryFilter";
		entering(METHOD);
		QueryFilter advancedFilter = new PrivateAuctionQueryFilter();
		auctionIdLastAccessed = -1;
		if (auctionName != null && !auctionName.equals("")) {
			logInfo("set auction name to the query filter");
			advancedFilter.addLikeCriteria(
				PrivateAuctionQueryFilter.NAME,
				auctionName.trim());
		}

		if (prodName != null && !prodName.equals("")) {
			logInfo("set product name to the query filter");
			advancedFilter.addLikeCriteria(
				PrivateAuctionQueryFilter.PRODUCT_ID,
				prodName);
		}
		
		if (auctionTitle != null && !auctionTitle.equals("")) {
			logInfo("set auctionTitle to the query filter");
			advancedFilter.addLikeCriteria(
				PrivateAuctionQueryFilter.TITLE,
				auctionTitle);
		}
		
		if (orderId != null && !orderId.equals("")) {
			logInfo("set orderId to the query filter");
			advancedFilter.addLikeCriteria(
				PrivateAuctionQueryFilter.ORDER_ID,
				orderId);
		}
		if (status != null) {
			int statusInt = Integer.parseInt(status);
			if (statusInt > 0 && statusInt < 11) {
				logInfo("set auction status to the query filter");
				advancedFilter.addEqualToCriteria(
					PrivateAuctionQueryFilter.STATUS,
					new Integer(statusInt));
			}
		}
		logInfo("set aution type to the query fileter");
		if (isNormalAuction != null) {
			if (isNormalAuction.booleanValue())
				advancedFilter.addEqualToCriteria(
					PrivateAuctionQueryFilter.TYPE,
					new Integer(AuctionTypeEnum.NORMAL.getValue()));
			else
				advancedFilter.addEqualToCriteria(
					PrivateAuctionQueryFilter.TYPE,
					new Integer(AuctionTypeEnum.BROKEN_LOT.getValue()));
		}

		if (isPrivateAuction != null) {
			if (isPrivateAuction.booleanValue())
				advancedFilter.addEqualToCriteria(
					PrivateAuctionQueryFilter.VISIBILITY,
					new Integer(AuctionVisibilityEnum.INT_PROTECTED));
			else
				advancedFilter.addEqualToCriteria(
					PrivateAuctionQueryFilter.VISIBILITY,
					new Integer(AuctionVisibilityEnum.INT_PUBLIC));
		}

		if (publishFromDate != null) {
			logInfo("set publish from date to the query filter");
			advancedFilter.addGTEqualToCriteria(
				PrivateAuctionQueryFilter.START_DATE,
				new Timestamp(publishFromDate.getTime()));
		}

		if (publishTillDate != null) {
			logInfo("set publish till date to the query filter");
			advancedFilter.addLTEqualToCriteria(
				PrivateAuctionQueryFilter.START_DATE,
				new Timestamp(publishTillDate.getTime()));
		}

		if (closingFromDate != null) {
			logInfo("set closing from date to the query filter");

			advancedFilter.addGTEqualToCriteria(
				PrivateAuctionQueryFilter.END_DATE,
				new Timestamp(closingFromDate.getTime()));
		}

		if (closingTillDate != null) {
			logInfo("set closing till date to the query filter");
			advancedFilter.addLTEqualToCriteria(
				PrivateAuctionQueryFilter.END_DATE,
				new Timestamp(closingTillDate.getTime()));
		}
		if (publishWithErrors) {
			advancedFilter.addEqualToCriteria(
				PrivateAuctionQueryFilter.PUBLISH_ERROR,
				Boolean.TRUE);
			advancedFilter.addEqualToCriteria(
				PrivateAuctionQueryFilter.STATUS,
				new Integer(AuctionStatusEnum.OPEN.getValue()));
		}
		exiting(METHOD);
		return advancedFilter;
	}

	public SearchManager getSearchManager() {
		final String METHOD = "getSearchManager";
		entering(METHOD);
		if (searchMgr == null) {
			logInfo("SearchManager is null, create a new one");
			UserSessionData userSession =
				UserSessionData.getUserSessionData(helpManager.getSession());
			AuctionBusinessObjectManagerBase bom =
				(AuctionBusinessObjectManagerBase) userSession.getBOM(
					AuctionBusinessObjectManagerBase.AUCTION_BOM);
			logInfo("create the AuctionManager from AuctionBusinessObjectManager");
			searchMgr = bom.createSearchManager();
		}
		exiting(METHOD);
		return searchMgr;
	}

	protected void clearLastAccess() {
		auctionIdLastAccessed = -1;
	}
}
