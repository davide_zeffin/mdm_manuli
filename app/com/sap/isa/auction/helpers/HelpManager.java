package com.sap.isa.auction.helpers;

/**
 * Title:			Internet Sales Private Auctions
 * Description:
 * @Copyright: 		Copyright (c) 2004
 * Company:			SAPLabs LLC
 * @author
 * Created on:		Apr 27, 2004 
 */

public interface HelpManager extends HelpManagerBase {

    public static final String HELPMANAGER = "HelperManager.auction";
    public static final String CATALOGHELPER = "CatalogHelper.auction.isa.sap.com";
    public static final String BUSINESSPARTNERHELPER = "BusinessPartnerHelper.auction.isa.sap.com";
    public static final String AUCTIONEDITHELPER = "AuctionEditHelper.auction.isa.sap.com";
    public static final String SEARCHHELPER = "SearchHelper.auction.isa.sap.com";
	public static final String PUBLISHQUEUEHELPER = "PublishQueueHelper.auction.isa.sap.com";
    
    public static final String AUCTIONDETAILSHELPER = "AuctionDetailsHelper.auction.isa.sap.com";
    public static final String ORDERHELPER = "OrderHelper.auction.isa.sap.com";
    public static final String HOMEHELPER     = "HomeHelper.auction.isa.sap.com";
	
	public static final String STARTHELPER	 = "StartHelper.auction.isa.sap.com";
	public static final String PRODUCTHELPER = "ProductHelper.auction.isa.sap.com";
	public static final String TARGETGROUPHELPER = "TargetGroupBP.auction.isa.sap.com";
	
	public static final String BUYERSEARCHHELPER = "SearchHelper.buyer.auctions.isa.sap.com";
	public static final String AFTERSAVEHELPER   = "AfterSaveHelper.buyer.auctions.isa.sap.com";
	public static final String BIDSHELPER		 = "BidHelper.buyer.auctions.isa.sap.com";
	public static final String CHECKOUTHELPER	 = "CheckoutHelper.buyer.auctions.isa.sap.com"; 
	
	public static final String MANUALWINNERHELPER = "ManualWinnerHelper.seller.auctions.isa.sap.com";
	public static final String SCHEDULETASKHELPER = "ScheduleTaskHelper.seller.auctions.isa.sap.com";
}
