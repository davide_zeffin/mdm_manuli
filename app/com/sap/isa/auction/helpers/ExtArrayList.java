package com.sap.isa.auction.helpers;

import java.util.ArrayList;

public class ExtArrayList extends ArrayList {
	
	private int capacity = 1;
	public ExtArrayList(int capacity)  {
		super(capacity);
		this.capacity = capacity;
	}
	
	public int getCapacity()  {
		return capacity;
	}
}
