package com.sap.isa.auction.helpers;

import java.util.Enumeration;
import java.util.Hashtable;

import javax.servlet.http.HttpSession;

import com.sap.isa.auction.actionforms.Form;
import com.sap.isa.auction.businessobject.AuctionBusinessObjectManagerBase;
import com.sap.isa.core.UserSessionData;

/**
 * Title:
 * Description:
 * Copyright:    Copyright (c) 2003
 * Company:
 * @author
 * @version 1.0
 */

public class HelperObjectManagerBase extends LogSupport implements HelpManagerBase {

	protected static Hashtable helperClassMapping;

	protected Hashtable helpers = new Hashtable();
	protected HttpSession httpSession;
	protected Hashtable instantCleanupHelpers = new Hashtable();
	protected Hashtable intermediateCleanups = new Hashtable();

	static {
		helperClassMapping = new Hashtable();
		helperClassMapping.put(HelpManagerBase.LOGINHELPER, LoginHelper.class);
		helperClassMapping.put(HelpManagerBase.CONVERSIONHELPER, ConversionHelper.class);
	}

	public HelperObjectManagerBase() {
	}

	public HelperObjectManagerBase(HttpSession sesn) {
		this();
		this.httpSession = sesn;
	}
	public static void registerHelperClass(String id , Class className)  {
		helperClassMapping.put(id , className);
	}
	
	public static void unRegisterHelperClass(String id)  {
		helperClassMapping.remove(id);
	}

	public void registerForInstantCleanup(String helperId) {
		registerForInstantCleanup(getHelper(helperId));
	}
	
	public void registerForInstantCleanup(Object obj)  {
		instantCleanupHelpers.put(obj , obj);
	}

	public Helper getHelper(String helperId) {
		Helper helper = (Helper) helpers.get(helperId);
		if (null == helper) {
			try {
				Class classObj = (Class) helperClassMapping.get(helperId);
				helper = (Helper) classObj.newInstance();
				helpers.put(helperId, helper);
				helper.initialize((HelpManagerBase) this);
			} catch (Exception exc) {
				logError(cat , location , exc.getMessage() , exc);
			}
		}
		return helper;
	}

	public HttpSession getSession() {
		return httpSession;
	}

	public void clean() {
		doInstantCleanup();
		doIntermediateCleanup();
		Enumeration enum = helpers.elements();
		Helper helper = null;
		while (enum.hasMoreElements()) {
			helper = (Helper) enum.nextElement();
			helper.clean();
		}
		UserSessionData userSession =
			UserSessionData.getUserSessionData(httpSession);
		AuctionBusinessObjectManagerBase auctionBOM =
			(AuctionBusinessObjectManagerBase) userSession.getBOM(
				AuctionBusinessObjectManagerBase.AUCTION_BOM);

		auctionBOM.release();
		helpers.clear();
		instantCleanupHelpers.clear();
	}

	public void doInstantCleanup() {
		if(null == instantCleanupHelpers)  {
			return;
		}
		Enumeration enum = instantCleanupHelpers.elements();
		Helper helper = null;
		Object dirtyObject = null;
		while (enum.hasMoreElements()) {
			dirtyObject = enum.nextElement();
			try  {
				if(dirtyObject instanceof Helper)  {
					helper = (Helper) dirtyObject;
						helper.clean();
				}  else if(dirtyObject instanceof Form)  {
						((Form)dirtyObject).clean();
				}
			}  catch(Exception exc)  {
				if(dirtyObject != null)
					logInfo("Unable do instant cleanup" + dirtyObject.toString());
			}
		}
	}
	
	public void doIntermediateCleanup() {
		if(null == instantCleanupHelpers)  {
			return;
		}
		Enumeration enum = intermediateCleanups.elements();
		Helper helper = null;
		Object dirtyObject = null;
		while (enum.hasMoreElements()) {
			dirtyObject = enum.nextElement();
			try  {
				if(dirtyObject instanceof Helper)  {
					helper = (Helper) dirtyObject;
						helper.intermediateCleanup();
				}  else if(dirtyObject instanceof Form)  {
						((Form)dirtyObject).clean();
				}
			}  catch(Exception exc)  {
				if(dirtyObject != null)
					logInfo("Unable do intermediate cleanup" + dirtyObject.toString());
			}
		}
	}

}
