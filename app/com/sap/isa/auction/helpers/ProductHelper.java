/*
 * Created on Apr 30, 2004
 *
 * To change the template for this generated file go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
package com.sap.isa.auction.helpers;

import java.util.Collection;
import java.util.Hashtable;
import java.util.Iterator;

import com.sap.isa.auction.bean.AuctionItem;
import com.sap.isa.auction.bean.b2x.PrivateAuctionItem;
import com.sap.isa.auction.util.Utilidad;
import com.sap.isa.catalog.webcatalog.WebCatItem;

/**
 * @author I803067
 *
 * To change the template for this generated type comment go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
public class ProductHelper extends GenericHelper implements ProductsContainer  {

	public static final int BROWSE = 1;
	public static final int CREATE = 2;
	
	private int mode = BROWSE;
	private Hashtable selectedItems;
	private Collection beforeConsumingCollection;
	
	public boolean addItem(AuctionLineItem item , double quantity)  {
		entering("addItem(item,quantity)");
		if(null == selectedItems)  {
			selectedItems = new Hashtable();
		}
		if (selectedItems.get(item.getProductId()) != null) {
			item = (AuctionLineItem)selectedItems.get(item.getProductId());
			item.setQuantity(quantity+item.getQuantity());
		} else {
			item.setQuantity(quantity);
			selectedItems.put(item.getProductId() , item);
		}
		exiting("addItem(item,quantity)");
		return true;
	}
	
	public boolean addItem(WebCatItem item , double quantity)  {
		return addItem(createAuctionLineItem(item) , quantity);
	}
	
	public boolean updateItem(AuctionLineItem item) {
		if (isAddedBefore(item.getProductId())) {
			selectedItems.put(item.getProductId() , item);
			return true;
		}
		return false;
	}
	
	public boolean isAddedBefore(String id)  {
		entering("isAddedBefore");
		boolean flag = false;
		if(null != selectedItems)  {
			Object itemExisting = selectedItems.get(id);
			if(null == itemExisting)  {
				flag = false;
			}  else {
				flag = true;
			}
		}
		exiting("isAddedBefore");
		return flag;
	}

	public void removeItem(String id)  {
		entering("removeItem");
		if(null != selectedItems)  {
			Object itemExisting = null;
			String code = null;
			Iterator iterateContainer = selectedItems.values().iterator();
			while(iterateContainer.hasNext())  {
				itemExisting = iterateContainer.next();
				if(itemExisting instanceof AuctionLineItem)  {
					code = ((AuctionLineItem)itemExisting).getProductId();
				}
				if(code != null && code.equals(id))  {
					iterateContainer.remove();
					break;
				}
			}

		}
		exiting("removeItem");
	}
	
	public void setMode(int mode)  {
		this.mode = mode;
	}
	
	public int getMode()  {
		return mode;
	}
	
	public Collection getSelectedItems()  {
		return (selectedItems != null ? selectedItems.values() : null);
	}
	
	public int getSelectionCount()  {
		return (selectedItems != null ? selectedItems.size() : 0);
	}
	
	public void clearSelection()  {
		if(selectedItems != null)  {
			selectedItems.clear();
		}
	}
	
	public void setSelection(Collection items)  {
		final String methodName = "setSelection";
		entering(methodName);
		clearSelection();
		if(items != null)  {
			Iterator iterateItems = items.iterator();
			Object obj;
			while(iterateItems.hasNext())  {
				obj = iterateItems.next();
				if(obj instanceof WebCatItem)  {
					addItem(createAuctionLineItem((WebCatItem)obj) , 1);
				}  else if(obj instanceof AuctionItem)  {
					addItem(createAuctionLineItem((AuctionItem)obj) , ((AuctionItem)obj).getQuantity());
				}
			}
		}
		exiting(methodName);
	}
	
	protected AuctionLineItem createAuctionLineItem(WebCatItem item)  {
		AuctionLineItem product = new AuctionLineItem();
		product.setProductId(item.getProduct());
		product.setProductGUID(item.getProductID());
		product.setDescription(item.getDescription());
		product.setUnit(item.getUnit());
		product.setImageURL(Utilidad.getProductImageURL(item));
		return product;    	
	}
    
	protected AuctionLineItem createAuctionLineItem(AuctionItem item)  {
		AuctionLineItem product = new AuctionLineItem();
		product.setProductId(item.getProductCode());
		product.setProductGUID(item.getProductId());
		product.setDescription(item.getDescription());
		product.setUnit(item.getUOM());
		product.setQuantity(item.getQuantity());
		product.setImageURL(item.getImage());
		return product;    	
	}

	protected AuctionItem createAuctionItem(AuctionLineItem item)  {
		AuctionItem auctionItem = new PrivateAuctionItem();
		auctionItem.setDescription(item.getDescription());
		auctionItem.setProductCode(item.getProductId());
		auctionItem.setProductId(item.getProductGUID());
		auctionItem.setUOM(item.getUnit());
		auctionItem.setQuantity(item.getQuantity());
		auctionItem.setImage(item.getImageURL());
		return auctionItem;
	}

	protected AuctionItem createAuctionItem(WebCatItem item)  {
		AuctionItem auctionItem = new PrivateAuctionItem();
		auctionItem.setDescription(item.getDescription());
		auctionItem.setProductCode(item.getProduct());
		auctionItem.setProductId(item.getProductID());
		auctionItem.setUOM(item.getUnit());
		auctionItem.setImage(Utilidad.getProductImageURL(item));
		return auctionItem;
	}
}
