/*
 * Created on May 10, 2004
 *
 * To change the template for this generated file go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
package com.sap.isa.auction.helpers;

import java.util.ArrayList;
import java.util.Collection;

import com.sap.isa.auction.bean.Auction;
import com.sap.isa.auction.bean.AuctionStatusEnum;
import com.sap.isa.auction.bean.b2x.PrivateAuction;
import com.sap.isa.auction.businessobject.b2x.PrivateAuctionManager;

/**
 * @author I803067
 *
 * To change the template for this generated type comment go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
public class AfterSaveHelper extends GenericHelper  {

	private SearchResult result;
	
	public void setSavedAuctions(Collection auctions)  {
		if(result != null)  {
			result.close();
		}
		result = new SearchResultCollection(auctions);
	}
	
	public SearchResult getAuctions()  {
		return result;
	}
	
	public void setSavedAuctions(Auction auction)  {
		if(result != null)  {
			result.close();
		}
		ArrayList list = new ArrayList();
		list.add(auction);
		result = new SearchResultCollection(list);
	}
	
	public boolean publish(PrivateAuction auction)  {
		boolean flag = false;
		PrivateAuctionManager auctionMngr = (PrivateAuctionManager)getAuctionBOM().getAuctionManager();
		if(null == auctionMngr)  {
			auctionMngr = (PrivateAuctionManager)getAuctionBOM().createAuctionManager();
		}
		auction.setUserObject(null);
		if(AuctionStatusEnum.OPEN.getValue() != auction.getStatus())
			return true;
		try  {
			auctionMngr.modifyStatus(auction , AuctionStatusEnum.PUBLISHED);
		} catch(Exception exc)  {
			auction.setUserObject(exc);
			logError("Unable to publish the auction - " + auction.getAuctionName() , exc);
		}
		return flag;
	}
}
