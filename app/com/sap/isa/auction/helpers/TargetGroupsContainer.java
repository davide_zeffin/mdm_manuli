/*
 * Created on May 8, 2004
 *
 * To change the template for this generated file go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
package com.sap.isa.auction.helpers;

import java.util.Collection;

/**
 * @author I803067
 *
 * To change the template for this generated type comment go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
public interface TargetGroupsContainer {
	public void addBusinessPartner(String businessPartnerID);
	public void addTargetGroup(String targetGroupID);
	public void clearTgBpSelection();
	public Collection getTargetGroupBPSelection();
	public void removeTargetGroupBusinessPartner(Object tgBP);
}
