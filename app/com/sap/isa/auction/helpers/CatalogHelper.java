/*
 * Created on Apr 29, 2004
 *
 * To change the template for this generated file go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
package com.sap.isa.auction.helpers;

import java.util.Collection;
import java.util.Iterator;
import java.util.Locale;

import javax.servlet.http.HttpServletRequest;

import com.sap.isa.auction.actions.BaseAction;
import com.sap.isa.auction.bean.SAPBackendSystemEnum;
import com.sap.isa.auction.businessobject.b2x.AuctionBusinessObjectManager;
import com.sap.isa.businessobject.Shop;
import com.sap.isa.businessobject.webcatalog.CatalogBusinessObjectManager;
import com.sap.isa.catalog.AttributeKeyConstants;
import com.sap.isa.catalog.actions.ActionConstants;
import com.sap.isa.catalog.boi.IFilter;
import com.sap.isa.catalog.boi.IQueryStatement;
import com.sap.isa.catalog.filter.CatalogFilterFactory;
import com.sap.isa.catalog.webcatalog.CatalogClient;
import com.sap.isa.catalog.webcatalog.CatalogServer;
import com.sap.isa.catalog.webcatalog.WebCatArea;
import com.sap.isa.catalog.webcatalog.WebCatAreaList;
import com.sap.isa.catalog.webcatalog.WebCatInfo;
import com.sap.isa.catalog.webcatalog.WebCatItem;
import com.sap.isa.catalog.webcatalog.WebCatItemList;
import com.sap.isa.core.TechKey;
import com.sap.isa.core.UserSessionData;
import com.sap.isa.core.init.InitializationEnvironment;
import com.sap.isa.core.interaction.InteractionConfig;

/**
 * @author I803067
 *
 * To change the template for this generated type comment go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
public class CatalogHelper extends GenericHelper {
	
	static {
		WebCatItemList.populateAll = true;
	}
	public static final String SELECTIONCONTEXT = "selectionContext";
	public static final String CREATIONCONTEXT  = "createContext";
	
	private String categoryPath = "";
	private String mode = "";
	
	private String getCatalog(String catalogId , String variant)  {
		if(getSapBackendSystemType() == SAPBackendSystemEnum.CRM)  {
			StringBuffer catalogKey = new StringBuffer();
			catalogKey.append(catalogId);
			for(int i = catalogId.length(); i < 30; i++)  {
				catalogKey.append(' ');
			}
			catalogKey.append(variant);
			return catalogKey.toString();
		}
		  else  {
			return catalogId + "_" + variant;
		}
	}
	
	public String getShop()  {
        final String methodName = "ShopId getShop"; 
        entering(methodName);
		UserSessionData userData = UserSessionData.getUserSessionData(helpManager.getSession());
		
		AuctionBusinessObjectManager bom = (AuctionBusinessObjectManager)
				userData.getBOM(AuctionBusinessObjectManager.AUCTION_BOM);
			//(BusinessObjectManager)userData.getBOM(BusinessObjectManager.ISACORE_BOM);

		Shop shop = bom.getShop();
		if(shop != null)  {
			return shop.getId();
		}  else  {
            logDebug("No Shop available BOM returns : " + bom.getShop());
            exiting(methodName);
			return "";
		}
	}

	public Collection searchItems(String productId , String productDescription , String productGUID)  {
		IFilter filter = null;
		try  {
			CatalogFilterFactory filterFactory = CatalogFilterFactory.getInstance();
			UserSessionData userSession = UserSessionData.getUserSessionData(helpManager.getSession());
			CatalogBusinessObjectManager catalogBom =
									(CatalogBusinessObjectManager)userSession.getBOM(CatalogBusinessObjectManager.CATALOG_BOM);

			WebCatInfo catalog = catalogBom.getCatalog();

			IFilter filterId = null;
			if(productId != null && !productId.equals(""))  {
				if(productId.startsWith("*") || productId.endsWith("*"))  {
					filterId = filterFactory.createAttrContainValue(catalog.getCatalog().getAttributeGuid(AttributeKeyConstants.PRODUCT) , productId);
				}  else  {
					filterId = filterFactory.createAttrEqualValue(catalog.getCatalog().getAttributeGuid(AttributeKeyConstants.PRODUCT) , productId);
				}
			}
			IFilter filterDesc = null;
			if(productDescription != null && !productDescription.equals(""))  {
				if(productDescription.startsWith("*") || productDescription.endsWith("*"))  {
					filterDesc = filterFactory.createAttrContainValue(catalog.getCatalog().getAttributeGuid(AttributeKeyConstants.PRODUCT_DESCRIPTION) , productDescription);
				}  else  {
					filterDesc = filterFactory.createAttrEqualValue(catalog.getCatalog().getAttributeGuid(AttributeKeyConstants.PRODUCT_DESCRIPTION) , productDescription);
				}
			}
			IFilter filterGUID = null;
			if(productGUID != null && !productGUID.equals(""))  {
				filterGUID = filterFactory.createAttrEqualValue(catalog.getCatalog().getAttributeGuid(AttributeKeyConstants.PRODUCT_ID) , productGUID);
			}
			if(filterId != null)  {
				filter = filterId;
			}
			if(filterDesc != null)  {
				if(filter != null)  {
					filter = filterFactory.createAnd(filter , filterDesc);
				}  else  {
					filter = filterDesc;
				}
			}
			if(filterGUID != null)  {
				if(filter != null)  {
					filter = filterFactory.createAnd(filter , filterGUID);
				}  else  {
					filter = filterGUID;
				}
			}

			IQueryStatement statement = catalog.getCatalog().createQueryStatement();
			statement.setStatement(filter , null , null);
			Collection nonConfigItems = null;
			WebCatItemList itemList = new WebCatItemList(catalog , catalog.getCatalog().createQuery(statement));
			if(null != itemList)  {
				nonConfigItems = removeConfigurableProducts(itemList.getItems());
				if(nonConfigItems.size() > 0) {
//					itemList.sort(catalog.getCatalog().getAttributeGuid(AttributeKeyConstants.PRODUCT));
//				return itemList.getItems();
					return nonConfigItems;
				}
			}
			return null;
		}  catch(Exception exc)  {
			logInfo("CatalogSearch failed" , exc);
			return null;
		}
	}

	public boolean loadCatalog(String shopId , 
							BaseAction action , 
							HttpServletRequest request , 
							InitializationEnvironment env) {
		UserSessionData userData = UserSessionData.getUserSessionData(helpManager.getSession());
		AuctionBusinessObjectManager bom = (AuctionBusinessObjectManager)
				userData.getBOM(AuctionBusinessObjectManager.AUCTION_BOM);
		/*BusinessObjectManager bom =
			(BusinessObjectManager)userData.getBOM(BusinessObjectManager.ISACORE_BOM);*/

		Shop shop = bom.getShop();
		if(shop != null)  {
			bom.releaseShop();
		}
		try  {
			TechKey key = new TechKey(shopId);
			shop = bom.createShop(key);
			if(shop == null)  {
				return false;
			}
		}  catch(Exception exc)  {
			logError(cat , location , "Unable to create the Shop" , exc);
			return false;
		}
		
		CatalogBusinessObjectManager catalogbom =
					  (CatalogBusinessObjectManager)userData.getBOM(CatalogBusinessObjectManager.CATALOG_BOM);


		WebCatInfo theCatalog = catalogbom.getCatalog();

		if (theCatalog != null)  {
			catalogbom.releaseCatalog();
			logInfo("Release the previous catalog " + theCatalog.getCatalog().getName());
		}

		Locale theUserLocale = userData.getLocale();

		try  {
			CatalogClient aCatalogClient = new CatalogClient(theUserLocale, null, null, shop.getViews());
			CatalogServer aCatalogServer = new CatalogServer(null,shop.getCatalog());

			theCatalog = catalogbom.createCatalog(aCatalogClient, aCatalogServer, env);

			int pageSize = 5; //the default if everything else fails...
			if (action.getInteractionConfig(request) != null &&
				action.getInteractionConfig(request).getConfig(ActionConstants.IN_WEBCATALOG_CONFIG_ID) != null)  {
				InteractionConfig catalogConf = action.getInteractionConfig(request).getConfig(ActionConstants.IN_WEBCATALOG_CONFIG_ID);

			if (catalogbom.getCatalog().getImageServer() == null)  {
				String imageServer = catalogConf.getValue(ActionConstants.IN_IMAGE_SERVER);
				if (imageServer == null)
					imageServer = "";
				catalogbom.getCatalog().setImageServer(imageServer);
				logInfo(ActionConstants.IN_IMAGE_SERVER+" was set to:"+imageServer);
			}  else
				logInfo(ActionConstants.IN_IMAGE_SERVER+" is already set to:"+catalogbom.getCatalog().getImageServer());

			String itemPageSize = catalogConf.getValue(ActionConstants.IN_ITEMPAGE_SIZE);
			if (itemPageSize != null)  {
				try  {
				  pageSize = Integer.parseInt(itemPageSize);
				}  catch (NumberFormatException e)  {
				  logInfo("invalid itemPageSize "+itemPageSize);
				}
			}
		  }
			theCatalog.setItemPageSize(pageSize);
			logInfo("itemPageSize="+pageSize);
			//PriceCalculator priceCalc = initializePriceCalculator(bom, catalogbom , env);
				  // set the price calculator for the catalog
			//theCatalog.setPriceCalculator(priceCalc);
		}  catch(Exception exc)  {
			logInfo("Unable to load the Catalog" , exc);
			return false;
		}
		return true;
	}

	public WebCatAreaList getRootCategories()  {
		final String methodName = "Catalog getRootCategories"; 
		entering(methodName);
		WebCatAreaList list = null;

		try  {
			list = getCatalog().getRootCategories();
		}  catch(Exception exc)  {
			logInfo("Catalog not Initialized properly" , exc);
		}
		exiting(methodName);
		return list;
	}

	public WebCatArea getWebCatArea(String id)  {
		final String methodName = "Retrieving categories for id";
		entering(methodName);
		WebCatArea area = null;

		try  {
			area = getCatalog().getArea(id);
		}  catch(Exception exc)  {
			logInfo("Unable to retrieve categories for id - " + id , exc);
		}
		exiting(methodName);
		return area;
	}

	public Collection getProductsForCurrentCategory() {
		final String methodName = "getProductForTheCurrentCategory";
		entering(methodName);
		Collection items = null;
		Collection nonConfigItems = null;
		try  {
			items = getCatalog().getCurrentArea().getItemList().getItems();
			nonConfigItems = removeConfigurableProducts(items);
			categoryPath = getCatalog().getCurrentArea().getAreaID();
		} catch(Exception exc) {
			logInfo("There is no items for the current area browsed by the user");
		}
		exiting(methodName);
		return nonConfigItems;
	}

	public Collection getProducts(String category)  {
		final String methodName = "Retrieving Items for category"; 
		entering(methodName);

		Collection items = null;
		Collection nonConfigItems = null;
		try  {
			WebCatArea area = getCatalog().getArea(category);
			if(null == area)  {
				logInfo("Expand Category Action : Invalid Category id - " + category);
				return null;
			}
			categoryPath = area.getAreaID();
			items = area.getItemList(true).getItems();
			nonConfigItems = removeConfigurableProducts(items);
			getCatalog().setCurrentArea(area);
		}  catch(Exception exc)  {
			logInfo("Unable to retrieve products for Category - " + category , exc);
		}
		exiting(methodName);
		return nonConfigItems;
	}

	public String getCategoryPath()  {
		if(categoryPath != null)  {
			WebCatArea area = getWebCatArea(categoryPath);
			if(area != null)  {
				return area.getAreaName();	
			}
		}
		return "";
	}
	
	public String getCurrentAreaId()  {
		return categoryPath;
	}
	
	private WebCatInfo getCatalog()  {
		UserSessionData userSession = UserSessionData.getUserSessionData(helpManager.getSession());
		CatalogBusinessObjectManager catalogBom =
								(CatalogBusinessObjectManager)userSession.getBOM(CatalogBusinessObjectManager.CATALOG_BOM);

		WebCatInfo catalog = catalogBom.getCatalog();

		if(null == catalogBom.getCatalog())  {
			logInfo("The catalog is null and not created - so the categories tree cannot be displayed");
			return null;
		}
		return catalog;
	}

	public WebCatItem getWebCatItem(String productGUID)  {
		final String methodName = "getWebCatItem By ProductGUID";
		entering(methodName);
		WebCatItem item = null;
		Collection items = searchItems(null , null , productGUID);
		if(items != null && items.size() > 0)  {
			item = (WebCatItem)items.iterator().next();
		}  else  {
			logInfo("No products found for - " + productGUID);
		}
		exiting(methodName);
		return item;
	}
	/**
	 * @return
	 */
	public String getMode() {
		return mode;
	}

	/**
	 * @param string
	 */
	public void setMode(String context) {
		mode = context;
	}
	
	/**
	 * Removes all configurable products from Item list
	 * @param items
	 * @return Collection nonConfigItems
	 */
	private Collection removeConfigurableProducts(Collection items) {
		
//		ArrayList nonConfigItems = null;
		WebCatItem item = null;

		if (items != null && items.size() > 0) {
			Iterator iterator = items.iterator();
			while (iterator.hasNext()) {
				item = (WebCatItem)iterator.next();
				if (item.isConfigurable()) {
					items.remove(item);
				}
			}
			if (items.size() == 1) {
				Iterator iteratorLast = items.iterator();
				while (iteratorLast.hasNext()) {
					item = (WebCatItem)iteratorLast.next();
					if (item.isConfigurable()) {
						items.remove(item);
					}
				}				
			}
		}

		
		return items;
	}

}


