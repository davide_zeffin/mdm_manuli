/*
 * Created on Apr 29, 2004
 *
 * To change the template for this generated file go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
package com.sap.isa.auction.helpers;

import javax.servlet.http.HttpSession;

import com.sap.isa.auction.helpers.buyer.BidsHelper;
import com.sap.isa.auction.helpers.buyer.CheckoutHelper;
import com.sap.isa.auction.helpers.buyer.SearchAuctionsHelper;

/**
 * @author I803067
 *
 * To change the template for this generated type comment go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
public class HelperObjectManager extends HelperObjectManagerBase implements HelpManager {

	static  {
		helperClassMapping.put(HelpManager.CURRENCYHELPER , SellerCurrencyHelperImpl.class);
		helperClassMapping.put(HelpManager.STARTHELPER , StartHelper.class);
		helperClassMapping.put(HelpManager.CATALOGHELPER , CatalogHelper.class);
		helperClassMapping.put(HelpManager.PRODUCTHELPER , ProductHelper.class);
		helperClassMapping.put(HelpManager.TARGETGROUPHELPER , TargetGroupBPHelper.class);
		helperClassMapping.put(HelpManager.AUCTIONEDITHELPER , AuctionEditHelper.class);
		helperClassMapping.put(HelpManager.SEARCHHELPER, SearchHelper.class);
		helperClassMapping.put(HelpManager.BUYERSEARCHHELPER , SearchAuctionsHelper.class);
		helperClassMapping.put(HelpManager.AFTERSAVEHELPER , AfterSaveHelper.class);
		helperClassMapping.put(HelpManager.AUCTIONDETAILSHELPER, AuctionDetailsHelper.class);
		helperClassMapping.put(HelpManager.BIDSHELPER , BidsHelper.class);
		helperClassMapping.put(HelpManager.ORDERHELPER , OrderHelper.class);
		helperClassMapping.put(HelpManager.PUBLISHQUEUEHELPER , PublishQueueHelper.class);
		helperClassMapping.put(HelpManager.MANUALWINNERHELPER , ManualWinnerHelper.class);
		helperClassMapping.put(HelpManager.CHECKOUTHELPER , CheckoutHelper.class);
		helperClassMapping.put(HelpManager.SCHEDULETASKHELPER, SchedulerTaskHelper.class);
		helperClassMapping.put(HelpManager.HOMEHELPER, HomeHelper.class);
	}
	public HelperObjectManager(HttpSession session)  {
		super(session);
	}
}
