package com.sap.isa.auction.helpers;

import javax.servlet.http.HttpSession;

/**
 * Title:
 * Description:
 * Copyright:    Copyright (c) 2003
 * Company:
 * @author
 * @version 1.0
 */

public interface HelpManagerBase {

    public static final String HELPMANAGER = "HelperManager.auction";
    public static final String LOGINHELPER = "LoginHelper.auction.isa.sap.com";
	public static final String CONVERSIONHELPER = "ConversionHelper.auction.isa.sap.com";
	public static final String CURRENCYHELPER = "CurrencyHelper.auction.isa.sap.com";
	
    public Helper getHelper(String helper);

    public HttpSession getSession();

    public void clean();
    
    public void doInstantCleanup();
    
    public void registerForInstantCleanup(Object obj);

	public void doIntermediateCleanup();
}
