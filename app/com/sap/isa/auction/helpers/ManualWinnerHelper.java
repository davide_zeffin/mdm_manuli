package com.sap.isa.auction.helpers;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.Iterator;

import com.sap.isa.auction.actionforms.seller.ManualWinnerDetailsForm;
import com.sap.isa.auction.bean.Auction;
import com.sap.isa.auction.bean.AuctionStatusEnum;
import com.sap.isa.auction.bean.AuctionTypeEnum;
import com.sap.isa.auction.bean.Bid;
import com.sap.isa.auction.bean.b2x.PrivateAuction;
import com.sap.isa.auction.bean.b2x.PrivateAuctionBid;
import com.sap.isa.auction.bean.b2x.PrivateAuctionWinner;
import com.sap.isa.auction.bean.b2x.WinnerDeterminationTypeEnum;
import com.sap.isa.auction.bean.search.QueryFilter;
import com.sap.isa.auction.businessobject.WinnerManager;
import com.sap.isa.auction.businessobject.b2x.AuctionBusinessObjectManager;
import com.sap.isa.auction.businessobject.b2x.PrivateAuctionManager;
import com.sap.isa.auction.businessobject.b2x.PrivateAuctionMgrImpl;
import com.sap.isa.auction.businessobject.b2x.PrivateAuctionQueryFilter;
import com.sap.isa.auction.businessobject.b2x.PrivateAuctionWinnerManager;
import com.sap.isa.core.UserSessionData;

/**
 * Title:			Internet Sales Private Auctions
 * Description:
 * @Copyright: 		Copyright (c) 2004
 * Company:			SAPLabs LLC
 * Created on:		May 25, 2004
 */
public class ManualWinnerHelper extends SearchHelper {

	private Collection selectedBids = new ArrayList();
	private Collection winners = new ArrayList();
	private Collection errorList = new ArrayList();
	private AuctionDetailsHelper detailsHelper;
	private WinnerManager winnerManager;

	/**
	 * 
	 * @param bid
	 */
	public void addBid(PrivateAuctionBid bid) {
		final String METHOD = "addBid";
		entering(METHOD);

		if (!containsBid(bid)) {
			logInfo("Adding bid to selected bids");
			selectedBids.add(bid);
		}
		exiting(METHOD);
	}

	private boolean containsBid(PrivateAuctionBid bid) {
		final String METHOD = "containsBid";
		entering(METHOD);
		Iterator iter = selectedBids.iterator();
		while (iter.hasNext()) {
			PrivateAuctionBid currentBid = (PrivateAuctionBid) iter.next();
			if (bid.getBidId().equals(currentBid.getBidId())) {
				logInfo("Bid already selected");
				// If bid is same, but quantity differs, remove previous bid and readd
				// with new quantity
				if (bid.getQuantity() != currentBid.getQuantity()) {
					iter.remove();
					return false;
				}

				exiting(METHOD);
				return true;
			}
		}
		exiting(METHOD);
		return false;
	}

	/**
	 * 
	 * @return
	 */
	public Collection getSelectedBids() {
		return selectedBids;
	}

	/**
	 * 
	 * @param selection
	 */
	public void setSelection(Collection selection) {
		selectedBids = selection;
	}

	/**
	 * 
	 *
	 */
	public void clearSelection() {
		selectedBids.clear();
	}

	/**
	 * 
	 * @param auction
	 * @return
	 */
	public boolean isBrokenLot(PrivateAuction auction) {
		return AuctionTypeEnum.BROKEN_LOT.getValue() == auction.getType();
	}

	/**
	 * 
	 * @param auction
	 * @param bid
	 * @param availableQty
	 * @return
	 */
	public PrivateAuctionWinner createWinnerFromBid(
		PrivateAuction auction,
		Bid bid,
		int availableQty) {
		final String METHOD = "createWinnerFromBid";
		entering(METHOD);

		logInfo("Creating winner from bid");
		// This is the winning bid
		PrivateAuctionWinner winner = new PrivateAuctionWinner();
		winner.setAuctionId(auction.getAuctionId());
		winner.setBidAmount(bid.getBidAmount());
		winner.setBidId(bid.getBidId());
		winner.setBuyerId(bid.getBuyerId());
		// TODO setting the userid same as the buyer id
		winner.setUserId(bid.getBuyerId());
		winner.setClient(bid.getClient());
		winner.setSystemId(bid.getSystemId());
		winner.setQuantity(availableQty);
		winner.setCheckedOut(false);
		if (bid instanceof PrivateAuctionBid) {
			winner.setWebShopId(((PrivateAuctionBid) bid).getWebShopId());
			winner.setSoldToParty(((PrivateAuctionBid) bid).getSoldToParty());
		}

		exiting(METHOD);
		return winner;
	}

	/**
	 * 
	 * @param auction
	 * @return
	 */
	public void updateWinners(PrivateAuction auction) {
		final String METHOD = "updateWinners";
		entering(METHOD);

		clearErrors();

		logInfo("Creating winners from auction");
		int availableQuantity = auction.getQuantity();

		logDebug("Available quantity: " + String.valueOf(availableQuantity));

		ArrayList selectedBids = (ArrayList) getSelectedBids();
		if (null == selectedBids || selectedBids.isEmpty()) {
			logInfo("No bids were selected");
			addError(ManualWinnerDetailsForm.NO_BIDS_SELECTED);
		}

		// Create the winners
		Iterator iter = selectedBids.iterator();
		while (iter.hasNext()) {
			PrivateAuctionWinner winner = null;
			PrivateAuctionBid bid = (PrivateAuctionBid) iter.next();
			int bidQuantity = bid.getQuantity();
			availableQuantity -= bidQuantity;
			if (availableQuantity >= 0) {
				winner = createWinnerFromBid(auction, bid, bid.getQuantity());
				winners.add(winner);
				// Create the winner
				(
					(PrivateAuctionWinnerManager) getWinnerManager())
						.createWinner(
					(PrivateAuctionWinner) winner,
					auction);

				// Send the notification to winners
				try {
					((PrivateAuctionMgrImpl) getAuctionManager())
						.getFriendInterface(
							this.getClass().getName(),
							this.getClass())
						.sendWinnerNotification(auction, winner);
				} catch (IllegalAccessException e) {
					logError("Unable to send winner notification", e);
					addError(e.getMessage());
				}
			} else {
				break;
			}
		}

		exiting(METHOD);
	}

	/**
	 * 
	 * @param auction
	 */
	protected void updateAuctionStatus(
		Auction auction,
		AuctionStatusEnum status) {
		getPrivateAuctionMgr().modifyStatus(auction, status);
	}

	/**
	 * 
	 * @param auction
	 * @return
	 */
	public Collection getWinners() {
		final String METHOD = "getWinners";
		entering(METHOD);

		Auction auction = getDetailsHelper().getAuction();
		winners = getDetailsHelper().getWinners(auction);

		//		Winner[] auctionWinners = getWinnerMgr().getWinners((Auction)auction);
		//		this.winners.clear();
		//		if (null != auctionWinners){
		//			for (int x = auctionWinners.length; x < auctionWinners.length; x++){
		//				winners.add(auctionWinners[x]);
		//			}
		//		}
		exiting(METHOD);
		return winners;
	}

	/**
	 * 
	 * @return
	 */
	protected PrivateAuctionWinnerManager getWinnerMgr() {
		return (PrivateAuctionWinnerManager) getAuctionBOM().getWinnerManager();
	}

	/**
	 * 
	 * @return
	 */
	protected PrivateAuctionManager getPrivateAuctionMgr() {
		return (PrivateAuctionManager) getAuctionBOM().getAuctionManager();
	}

	/**
	 * 
	 *
	 */
	private void clearErrors() {
		errorList.clear();
	}

	/**
	 * 
	 * @param object
	 */
	private void addError(Object object) {
		errorList.add(object);
	}

	/**
	 * 
	 * @return
	 */
	public Collection getErrors() {
		return errorList;
	}
	/* (non-Javadoc)
	 * @see com.sap.isa.auction.helpers.SearchHelper#createGenericQueryFilter(java.lang.String, java.util.Date, java.util.Date, java.lang.String, java.lang.String, boolean)
	 */
	QueryFilter createGenericQueryFilter(
		String status,
		Date closingDate,
		Date publishDate,
		String auctionName,
		String prodName,
		boolean publishWithErrors) {

		final String METHOD = "createGenericQueryFilter";
		entering(METHOD);

		QueryFilter genFilter = new PrivateAuctionQueryFilter();
		auctionIdLastAccessed = -1;
		//clean up the filter to allow a clean search

		// Manual winner determination type
		genFilter.addEqualToCriteria(
			PrivateAuctionQueryFilter.WINNER_DETERM_TYPE,
			new Integer(WinnerDeterminationTypeEnum.MANUAL.getValue()));

		if (auctionName != null && !auctionName.equals("")) {
			logInfo("set auction name to the query filter");
			genFilter.addLikeCriteria(
				PrivateAuctionQueryFilter.NAME,
				auctionName.trim());
		}
		// Status selected
		if (status != null) {
			int statusInt = Integer.parseInt(status);
			if (statusInt > 0) {
				logInfo("set auction status to the query filter");
				genFilter.addEqualToCriteria(
					PrivateAuctionQueryFilter.STATUS,
					new Integer(statusInt));
			}
		}

		if (prodName != null) {
			logInfo("set product name to the query filter");
			genFilter.addLikeCriteria(
				PrivateAuctionQueryFilter.PRODUCT_ID,
				prodName);
		}

		if (publishDate != null) {
			logInfo("parse and set publish date to the query filter");
			genFilter.addGTEqualToCriteria(
				PrivateAuctionQueryFilter.START_DATE,
				new Timestamp(publishDate.getTime()));
			genFilter.addLessThanCriteria(
				PrivateAuctionQueryFilter.START_DATE,
				new Timestamp(publishDate.getTime() + LONGVALUEFORDAY));
		}

		if (closingDate != null) {
			logInfo("parse and set closing date to the query filter");
			genFilter.addGTEqualToCriteria(
				PrivateAuctionQueryFilter.END_DATE,
				new Timestamp(closingDate.getTime()));
			genFilter.addLessThanCriteria(
				PrivateAuctionQueryFilter.END_DATE,
				new Timestamp(closingDate.getTime() + LONGVALUEFORDAY));
		}
		if (publishWithErrors) {
			genFilter.addEqualToCriteria(
				PrivateAuctionQueryFilter.PUBLISH_ERROR,
				Boolean.TRUE);
			genFilter.addEqualToCriteria(
				PrivateAuctionQueryFilter.STATUS,
				new Integer(AuctionStatusEnum.OPEN.getValue()));
		}
		exiting(METHOD);
		return genFilter;
	}

	/**
	 * 
	 * @return
	 */
	protected AuctionDetailsHelper getDetailsHelper() {
		if (null == detailsHelper) {
			detailsHelper =
				(AuctionDetailsHelper) helpManager.getHelper(
					HelpManager.AUCTIONDETAILSHELPER);
		}
		return detailsHelper;
	}

	/* (non-Javadoc)
	 * @see com.sap.isa.auction.helpers.Helper#clean()
	 */
	public void clean() {
		super.clean();
		clearErrors();
	}

	/**
	 * Checks that auction has bids and does not already have winners 
	 * @return
	 */
	public boolean isValidDeterminationState() {
		final String METHOD = "isValidDeterminationState";
		entering(METHOD);
		clearErrors();

		boolean clean = true;

		Auction auction = getDetailsHelper().getAuction();

		if (null == auction) {
			logWarning("Auction is null");
			return false;
		}
		// Check auction has bids
		Collection bids = getDetailsHelper().getBidHistory(auction);
		if (null == bids || 0 == bids.size()) {
			logWarning("Auction has no bids");
			clean = false;
			errorList.add(ManualWinnerDetailsForm.ERR_AUCTION_HAS_NOBIDS);
		}

		// Check auction has no winners
		Collection winners = getDetailsHelper().getWinners(auction);
		if (null != winners && 0 != winners.size()) {
			logWarning("Auction already has winners");
			clean = false;
			errorList.add(ManualWinnerDetailsForm.ERR_AUCTION_HAS_WINNERS);
		}

		exiting(METHOD);
		return clean;
	}

	/**
	 * 
	 *
	 */
	public void closeAuction() {
		final String METHOD = "closeAuction";
		entering(METHOD);

		clearErrors();
		logInfo("Attempting to close the auction");

		if (!getDetailsHelper().close()) {
			addError(ManualWinnerDetailsForm.ERR_UNABLE_CLOSE_AUCTION);
		}

		exiting(METHOD);
	}

	/**
	 * 
	 * @return
	 */
	private WinnerManager getWinnerManager() {
		final String METHOD = "getWinnerManager";
		entering(METHOD);
		if (winnerManager == null) {
			UserSessionData userSession =
				UserSessionData.getUserSessionData(helpManager.getSession());
			AuctionBusinessObjectManager bom =
				(AuctionBusinessObjectManager) userSession.getBOM(
					AuctionBusinessObjectManager.AUCTION_BOM);
			logInfo("Creating new winner manager");
			if (bom.getWinnerManager() != null)
				winnerManager = bom.getWinnerManager();
			else
				winnerManager = bom.createWinnerManager();
		}
		logInfo("Return winner manager");
		exiting(METHOD);
		return winnerManager;
	}

	/**
	 * 
	 * @return
	 */
	public boolean hasErrors() {
		return errorList.size() > 0;
	}

	protected void cleanDBConnections() {
		((HomeHelper) helpManager.getHelper(HelpManager.HOMEHELPER))
			.intermediateCleanup();
		((SearchHelper) helpManager.getHelper(HelpManager.SEARCHHELPER))
			.intermediateCleanup();
		((PublishQueueHelper) helpManager
			.getHelper(HelpManager.PUBLISHQUEUEHELPER))
			.intermediateCleanup();
	}

	/**
	 * Returns collection of highest bids from each bidder of an auction.
	 * 
	 * @param auction
	 * @return
	 */
	public Collection getHighestBids(Auction auction) {
		final String METHOD = "getHighestBids";
		entering(METHOD);

		Collection allBids = getDetailsHelper().getBidHistory(auction);
		Collection filteredBids = new ArrayList();
		int reservePrice =
			auction.getReservePrice() != null
				? auction.getReservePrice().intValue()
				: 0;

		logInfo("filtering bids that are higher than auction reserve price");

		for (Iterator outerIter = allBids.iterator(); outerIter.hasNext();) {
			PrivateAuctionBid pbid = (PrivateAuctionBid) outerIter.next();
			if (reservePrice <= pbid.getBidAmount().intValue()) {
				if (filteredBids.size() == 0) {
					filteredBids.add(pbid);
					continue;
				}
				boolean bidderExistsInCollection = false;
				boolean lowerBidInCollection = false;
				for (Iterator innerIter = filteredBids.iterator();
					innerIter.hasNext();
					) {
					PrivateAuctionBid innerPBid =
						(PrivateAuctionBid) innerIter.next();
					if (pbid.getBuyerId().equals(innerPBid.getBuyerId())) {
						bidderExistsInCollection = true;
						if ((pbid.getBidAmount().intValue()
							> innerPBid.getBidAmount().intValue())) {
							bidderExistsInCollection = true;
							lowerBidInCollection = true;
							break;
						}
					}
				}
				if (!bidderExistsInCollection) {
					filteredBids.add(pbid);
				} else if (bidderExistsInCollection && lowerBidInCollection) {
					filteredBids.add(pbid);
				}
			}
		}

		exiting(METHOD);
		return filteredBids;
	}
}
