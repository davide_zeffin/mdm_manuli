/*
 * Created on Nov 30, 2004
 *
 * To change the template for this generated file go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
package com.sap.isa.auction.helpers;

/**
 * @author I803067
 *
 * To change the template for this generated type comment go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
public class AuctionLineItem  {
	private int quanPerAuction;
	private String productGUID;
	private String productId;
	private String imageURL;
	private String unit;
	private String description;
	private double quantity;
	
	

	public String getProductGUID() {
		return productGUID;
	}

	/**
	 * @param string
	 */
	public void setProductGUID(String guid) {
		productGUID = guid;
	}
	
	public void setQuantityPerAuction(int qty)  {
		this.quanPerAuction = qty;
	}
	
	public int getQuantityPerAuction()  {
		return quanPerAuction;
	}
	/**
	 * @return
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * @return
	 */
	public String getImageURL() {
		return imageURL;
	}

	/**
	 * @return
	 */
	public String getProductId() {
		return productId;
	}

	/**
	 * @return
	 */
	public double getQuantity() {
		return quantity;
	}

	/**
	 * @return
	 */
	public String getUnit() {
		return unit;
	}

	/**
	 * @param string
	 */
	public void setDescription(String string) {
		description = string;
	}

	/**
	 * @param string
	 */
	public void setImageURL(String string) {
		imageURL = string;
	}

	/**
	 * @param string
	 */
	public void setProductId(String string) {
		productId = string;
	}

	/**
	 * @param i
	 */
	public void setQuantity(double i) {
		quantity = i;
	}

	/**
	 * @param string
	 */
	public void setUnit(String string) {
		unit = string;
	}

}
