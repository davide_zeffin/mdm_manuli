/*
 * Created on Nov 30, 2004
 *
 * To change the template for this generated file go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
package com.sap.isa.auction.helpers;

import java.util.Collection;


public class SearchResultCollection extends SearchResult  {

	public SearchResultCollection(Collection coll)  {
		super();
		if(coll != null)  {
			dataArray = coll.toArray();
			size = dataArray.length;	
		}
	}

	public Object[] fetch(int startIndex, int endIndex) {
		return null;
	}

	public Object fetch(int pos) {
		return dataArray[pos - 1];
	}

}
