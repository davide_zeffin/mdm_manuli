package com.sap.isa.auction.helpers;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.NumberFormat;
import java.util.Locale;

import com.sap.isa.auction.businessobject.AuctionBusinessObjectManagerBase;
import com.sap.isa.auction.businessobject.user.AuctionUser;
import com.sap.isa.auction.businessobject.user.Seller;
import com.sap.isa.auction.businessobject.user.SellerAdministrator;
import com.sap.isa.businesspartner.businessobject.BusinessPartner;
import com.sap.isa.businesspartner.businessobject.BusinessPartnerManager;
import com.sap.isa.businesspartner.businessobject.PartnerFunctionBase;
import com.sap.isa.core.SessionConst;
import com.sap.isa.core.UserSessionData;
import com.sap.isa.core.util.StartupParameter;
import com.sap.isa.user.businessobject.UserBase;

/**
 * Title:
 * Description:
 * Copyright:    Copyright (c) 2003
 * Company:
 * @author
 * @version 1.0
 */

public class LoginHelper extends GenericHelperBase {

  public static final String SHOP = "shop";

  private BusinessPartner businessPartner;
  private StartupParameter startupParameter;
  private AuctionUser auctionUser;
  private String logoURL;
  private boolean sellerLogin = true;

  public LoginHelper() {
  }

    public void setSellerLogin(boolean sellerLogin){
    	this.sellerLogin = sellerLogin;
    }

    public BusinessPartner getBusinessPartner()  {
        if(null == businessPartner)  {
            UserSessionData userSessionData = UserSessionData.getUserSessionData(helpManager.getSession());
            AuctionBusinessObjectManagerBase bom = (AuctionBusinessObjectManagerBase )userSessionData.getBOM(AuctionBusinessObjectManagerBase.AUCTION_BOM);
            BusinessPartnerManager businessPartnerManager = bom.createBUPAManager();
            businessPartner = businessPartnerManager.getDefaultBusinessPartner(PartnerFunctionBase.CONTACT);
        }
        return businessPartner;
    }

    public String getLastName()  {
        String name = "";
        try  {
            name = getAuctionUser().getLastName();
        }  catch(Exception exc)  {
            logInfo("Unable to get the name of the Business partner" , exc);
        }
        return name;
    }

    public String getGreetingUserNameText()  {
		StringBuffer greetingText = new StringBuffer();
        try  {
            greetingText.append(getAuctionUser().getSalutationTxt());
            greetingText.append(" ");
            greetingText.append(getAuctionUser().getLastName());
        }  catch(Exception exc)  {
            logInfo("Unable to get the Salutation name for the Business partner" , exc);
        }
        return greetingText.toString();
    }

    public DecimalFormat getUserNumberFormatter() {
        AuctionUser user = getAuctionUser();
        Locale loc = getUserLocale();
        DecimalFormat format = (DecimalFormat)NumberFormat.getInstance(loc);
        DecimalFormatSymbols symbol = new DecimalFormatSymbols(loc);
        if(user != null)  {
			symbol.setDecimalSeparator(user.getDecimalSeparator().charAt(0));
			symbol.setGroupingSeparator(user.getGroupingSeparator().charAt(0));
			format.applyLocalizedPattern(user.getDecimalFormat());
        }
        format.setDecimalFormatSymbols(symbol);
        return format;
    }

    public DecimalFormat getUserCurrencyFormatter() {
        DecimalFormat formatter = getUserNumberFormatter();
        formatter.setMaximumFractionDigits(2);
        formatter.setMinimumFractionDigits(2);
        return formatter;
    }

    public AuctionUser getAuctionUser() {
        UserSessionData userData = UserSessionData.getUserSessionData(helpManager.getSession());
        AuctionBusinessObjectManagerBase auctionBOM = (AuctionBusinessObjectManagerBase)userData.getBOM(AuctionBusinessObjectManagerBase.AUCTION_BOM);
        if(null == auctionUser)  {
        	auctionUser = auctionBOM.createAuctionUser();
			auctionUser.setUserDefaults(getUser().getUserId());
        }
        return auctionUser;
    }

    public UserBase getUser()  {
        UserSessionData userSession = UserSessionData.getUserSessionData(helpManager.getSession());
        AuctionBusinessObjectManagerBase bom = (AuctionBusinessObjectManagerBase)userSession.getBOM(AuctionBusinessObjectManagerBase.AUCTION_BOM);
        return (UserBase)bom.getUserBase();
        
    }
    
	public Seller getAdministrator()  {
		UserSessionData userSession = UserSessionData.getUserSessionData(helpManager.getSession());
		AuctionBusinessObjectManagerBase auctionBOM = (AuctionBusinessObjectManagerBase)userSession.getBOM(AuctionBusinessObjectManagerBase.AUCTION_BOM);
		return (Seller) auctionBOM.getUserBase();
	}

    public boolean isAdministrator()  {
		try  {
			Seller user = getAdministrator();
			return user.isAdministrator();
		}  catch(Exception exc)  {
			logError("Unable to determine the user type" , exc);
		}
		return false;
    }

    public Locale getUserLocale()  {
        UserSessionData userSession = UserSessionData.getUserSessionData(helpManager.getSession());
        return userSession.getLocale();
    }

    public StartupParameter getStartupParameter()  {
		try  {
			UserSessionData sessionData = UserSessionData.getUserSessionData(helpManager.getSession());
			startupParameter = (StartupParameter)sessionData.getAttribute(SessionConst.STARTUP_PARAMETER);
		}  catch(Exception exc)  {
			logInfo("Unable to get the Startup parameter");
		}
        return startupParameter;
    }

    public String getLoginURL()  {

        if(null == getStartupParameter())
            return "";
        StringBuffer buf = new StringBuffer();
        String rebuildParameters = getStartupParameter().rebuildParameter();
        if(rebuildParameters.length()>0) {
            buf.append(rebuildParameters);
        }
        return buf.toString();
    }

    public void clean() {
        try  {
            super.clean();
            UserSessionData sessionData = UserSessionData.getUserSessionData(helpManager.getSession());
            AuctionBusinessObjectManagerBase bom = (AuctionBusinessObjectManagerBase)sessionData.getBOM(AuctionBusinessObjectManagerBase.AUCTION_BOM);
            bom.release();
            auctionUser = null;
        }  catch(Exception exc)  {
            logInfo("Unable to clean the session" , exc);
        }
    }

}
