/*
 * Created on Jun 8, 2004
 *
 * To change the template for this generated file go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
package com.sap.isa.auction.helpers;

import java.sql.Timestamp;
import java.util.Calendar;
import java.util.Hashtable;

import com.sap.isa.auction.bean.AuctionStatusEnum;
import com.sap.isa.auction.bean.search.QueryFilter;

/**
 * @author I803067
 *
 * To change the template for this generated type comment go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
public class HomeHelper extends GenericHelper {
	public static final String PUBLISHEDAUCTIONS = "PublishedAuctions";
	public static final String TODAYPUBLISHEDATS = "TodayPublishedAuctions";
	public static final String SCHTOPUBLISHTODAY = "ScheduledToPublish";
	public static final String SCHEDULEFAILED = "ScheduleFailed";
	public static final String CLOSEDNOWINNERS = "ClosedNoWinners";
	public static final String FINALIZEDCHKDOUT = "FinalizedCheckout";
	public static final String FINALIZEDNOTCHKED = "FinalizedNotChecked";

	private Hashtable queryCollections = new Hashtable();

	public void clean() {
		intermediateCleanup();
		queryCollections = null;
	}

	public void intermediateCleanup() {
		if (queryCollections != null)
			queryCollections.clear();
	}

	public void cleanDBConnections() {
		((SearchHelper) helpManager.getHelper(HelpManager.SEARCHHELPER))
			.intermediateCleanup();
		((PublishQueueHelper) helpManager
			.getHelper(HelpManager.PUBLISHQUEUEHELPER))
			.intermediateCleanup();
		((ManualWinnerHelper) helpManager
			.getHelper(HelpManager.MANUALWINNERHELPER))
			.intermediateCleanup();
	}
	private void initCollections() {
		if (null == queryCollections)
			queryCollections = new Hashtable();
	}

	public SearchResult getSearchResult(String key) {
		initCollections();
		SearchResult result = null;
		QueryFilter savedFilter = null;
		boolean advancedSearch = false;
		SearchHelper helper =
			(SearchHelper) helpManager.getHelper(HelpManager.SEARCHHELPER);

		Calendar startoftoday = Calendar.getInstance();
		startoftoday.set(Calendar.HOUR_OF_DAY, 0);
		startoftoday.set(Calendar.MINUTE, 0);
		startoftoday.set(Calendar.SECOND, 0);
		startoftoday.set(Calendar.MILLISECOND, 0);
		Timestamp todaystart = new Timestamp(startoftoday.getTime().getTime());
		startoftoday.set(Calendar.HOUR_OF_DAY, 23);
		startoftoday.set(Calendar.MINUTE, 59);
		startoftoday.set(Calendar.SECOND, 59);
		startoftoday.set(Calendar.MILLISECOND, 0);
		Timestamp todayend = new Timestamp(startoftoday.getTime().getTime());

		Calendar cal = Calendar.getInstance();
		cal.add(Calendar.DATE, -7);
		cal.set(Calendar.HOUR_OF_DAY, 0);
		cal.set(Calendar.MINUTE, 0);
		cal.set(Calendar.MILLISECOND, 0);
		Timestamp sevenDaysBefore = new Timestamp(cal.getTime().getTime());

		QueryFilter filter = null;
		if (key.equals(PUBLISHEDAUCTIONS)) {
			//			Search for all published auctions	
			filter =
				helper.createGenericQueryFilter(
					Integer.toString(AuctionStatusEnum.PUBLISHED.getValue()),
					null,
					null,
					null,
					null,
					null,
					false);
			result = helper.searchAuctionEngine(filter);
			if (result != null) {
				queryCollections.put(PUBLISHEDAUCTIONS, filter);
			}

		} else if (key.equals(TODAYPUBLISHEDATS)) {
			filter =
				helper.createGenericQueryFilter(
					Integer.toString(AuctionStatusEnum.PUBLISHED.getValue()),
					null,
					todaystart,
					null,
					null,
					null,
					false);
			result = helper.searchAuctionEngine(filter);
			if (result != null) {
				queryCollections.put(TODAYPUBLISHEDATS, filter);
			}

		} else if (key.equals(SCHTOPUBLISHTODAY)) {
			filter =
				helper.createGenericQueryFilter(
					Integer.toString(AuctionStatusEnum.SCHEDULED.getValue()),
					null,
					todaystart,
					null,
					null,
					null,
					false);
			result = helper.searchAuctionEngine(filter);
			if (result != null) {
				queryCollections.put(SCHTOPUBLISHTODAY, filter);
			}

		} else if (key.equals(CLOSEDNOWINNERS)) {
			filter =
				helper.createAdvancedQueryFilter(
					null,
					null,
					null,
					null,
					null,
					null,
					null,
					null,
					sevenDaysBefore,
					todayend,
					Integer.toString(
						AuctionStatusEnum.NOT_FINALIZED.getValue()),
					false);
			result = helper.searchAuctionEngine(filter);
			if (result != null) {
				queryCollections.put(CLOSEDNOWINNERS, filter);
			}

		} else if (key.equals(FINALIZEDCHKDOUT)) {
			filter =
				helper.createAdvancedQueryFilter(
					null,
					null,
					null,
					null,
					null,
					null,
					null,
					null,
					sevenDaysBefore,
					todayend,
					Integer.toString(AuctionStatusEnum.FINISHED.getValue()),
					false);
			result = helper.searchAuctionEngine(filter);
			if (result != null) {
				queryCollections.put(FINALIZEDCHKDOUT, filter);
			}

		} else if (key.equals(FINALIZEDNOTCHKED)) {
			filter =
				helper.createGenericQueryFilter(
					Integer.toString(AuctionStatusEnum.FINALIZED.getValue()),
					null,
					null,
					null,
					null,
					null,
					false);
			result = helper.searchAuctionEngine(filter);
			if (result != null) {
				queryCollections.put(FINALIZEDNOTCHKED, filter);
			}

		} else if (key.equals(SCHEDULEFAILED)) {
			filter =
				helper.createGenericQueryFilter(
					null,
					null,
					null,
					null,
					null,
					null,
					true);
			result = helper.searchAuctionEngine(filter);
			if (result != null) {
				queryCollections.put(SCHEDULEFAILED, filter);
			}
		}
		if (result != null) {
			result.disconnect();
		}
		return result;
	}

	public void refresh() {
		intermediateCleanup();
	}

	public void setPublishedAuctions() {
		QueryFilter filter =
			(QueryFilter) queryCollections.get(PUBLISHEDAUCTIONS);
		SearchHelper searchHelper =
			(SearchHelper) helpManager.getHelper(HelpManager.SEARCHHELPER);
		searchHelper.setGenericQueryFilter(filter);
		searchHelper.setAdvancedQueryFilter(null);
	}

	public void setTodayPublishedAuctions() {
		QueryFilter filter =
			(QueryFilter) queryCollections.get(TODAYPUBLISHEDATS);
		SearchHelper searchHelper =
			(SearchHelper) helpManager.getHelper(HelpManager.SEARCHHELPER);
		searchHelper.setGenericQueryFilter(filter);
		searchHelper.setAdvancedQueryFilter(null);
	}

	public void setScheduleToPublishToday() {
		QueryFilter filter =
			(QueryFilter) queryCollections.get(SCHTOPUBLISHTODAY);
		PublishQueueHelper searchHelper =
			(PublishQueueHelper) helpManager.getHelper(
				HelpManager.PUBLISHQUEUEHELPER);
		searchHelper.setGenericQueryFilter(filter);
		searchHelper.setAdvancedQueryFilter(null);
	}

	public void setScheduleFailures() {
		QueryFilter filter = (QueryFilter) queryCollections.get(SCHEDULEFAILED);
		PublishQueueHelper searchHelper =
			(PublishQueueHelper) helpManager.getHelper(
				HelpManager.PUBLISHQUEUEHELPER);
		searchHelper.setGenericQueryFilter(filter);
		searchHelper.setAdvancedQueryFilter(null);
	}

	public void setClosedWithoutWinners() {
		QueryFilter filter =
			(QueryFilter) queryCollections.get(CLOSEDNOWINNERS);
		SearchHelper searchHelper =
			(SearchHelper) helpManager.getHelper(HelpManager.SEARCHHELPER);
		searchHelper.setAdvancedQueryFilter(filter);
		searchHelper.setGenericQueryFilter(null);
	}

	public void setFinalizedWithoutCheckout() {
		QueryFilter filter =
			(QueryFilter) queryCollections.get(FINALIZEDNOTCHKED);
		SearchHelper searchHelper =
			(SearchHelper) helpManager.getHelper(HelpManager.SEARCHHELPER);
		searchHelper.setGenericQueryFilter(filter);
		searchHelper.setAdvancedQueryFilter(null);
	}

	public void setFinalizedCheckout() {
		QueryFilter filter =
			(QueryFilter) queryCollections.get(FINALIZEDCHKDOUT);
		SearchHelper searchHelper =
			(SearchHelper) helpManager.getHelper(HelpManager.SEARCHHELPER);
		searchHelper.setAdvancedQueryFilter(filter);
		searchHelper.setGenericQueryFilter(null);
	}

}
