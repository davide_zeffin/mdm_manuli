/*
 * Created on Apr 30, 2004
 *
 * To change the template for this generated file go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
package com.sap.isa.auction.helpers;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Iterator;

import com.sap.isa.auction.AuctionTargetGroupBP;
import com.sap.isa.auction.businessobject.b2x.AuctionBusinessObjectManager;
import com.sap.isa.auction.businessobject.businesspartner.BPListSearchData;
import com.sap.isa.auction.businessobject.businesspartner.BusinessPartnerList;
import com.sap.isa.auction.businessobject.targetGroup.TargetGroup;
import com.sap.isa.auction.businessobject.targetGroup.TargetGroupList;
import com.sap.isa.auction.businessobject.targetGroup.TargetGroupSearch;
import com.sap.isa.businesspartner.businessobject.BusinessPartner;
import com.sap.isa.core.TechKey;
import com.sap.isa.core.UserSessionData;

/**
 * @author I803067
 *
 * To change the template for this generated type comment go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
public class TargetGroupBPHelper extends GenericHelper implements TargetGroupsContainer  {

	private Hashtable bpTableForTargetGroup;
	private Hashtable businessPartnerlist;
	private Hashtable targetGroupList;
	private Hashtable selectionList;
	
	private String context;
	
	public TargetGroup getTargetGroup(String guid)  {
		TargetGroup retval = null;
		synchronized(this)  {
			getTargetGroupLists(null , guid);
			retval = (TargetGroup)targetGroupList.get(guid);
		}

		if(retval != null)  {
		  return retval;
		}
		else {
			logInfo("Unknown Target Group id = " + guid);
		}
		return null;
	}

	public void getTargetGroupLists(String keyword , String tgGUID) {
		//get the target group list and cache it
		if (targetGroupList == null) {
			targetGroupList = new Hashtable();
		}
		UserSessionData userData = UserSessionData.getUserSessionData(helpManager.getSession());
		AuctionBusinessObjectManager auctionBOM = (AuctionBusinessObjectManager)userData.getBOM(AuctionBusinessObjectManager.AUCTION_BOM);
		TargetGroupList tgList = auctionBOM.createTargetGroupList();

		HashMap map = null;
		try  {
			TargetGroupSearch search = new TargetGroupSearch();
			if(keyword != null)
				search.setTgDescription(keyword);
			if(tgGUID != null)
				search.setTechKey(new TechKey(tgGUID));	
			map = tgList.getTargetGroupObjectList(search);
			targetGroupList.putAll(map);
			
		}  catch(Exception exc)  {
			logError("Unable to retrieve Target groups " , exc);
		}
	}

	public BusinessPartner getBusinessPartner(String bpId)  {
		BusinessPartner retval = null;
		synchronized(this)  {
			if(null == businessPartnerlist)  {
				businessPartnerlist = new Hashtable();
			}
		    retval = (BusinessPartner)businessPartnerlist.get(bpId);
		}
		if(retval != null)  {
		  return retval;
		}

		UserSessionData userData = UserSessionData.getUserSessionData(helpManager.getSession());
		AuctionBusinessObjectManager auctionBOM = (AuctionBusinessObjectManager)userData.getBOM(AuctionBusinessObjectManager.AUCTION_BOM);
		BusinessPartnerList bpList = auctionBOM.createBusinessPartnerList();

		HashMap map = bpList.searchBusinessPartners(getSearchData(bpId));
		if(null == map || map.size() == 0)  {
		  //throw new RuntimeException("Unknown Business partner Id " + guid);
		  return null;
		}
		try  {
		  retval = (BusinessPartner)map.values().toArray()[0];
		  synchronized(this)  {
			  businessPartnerlist.put(bpId , retval);
		  }
		  return retval;
		}  catch(Exception exc) {
		  //throw new RuntimeException("Unable to retrieve Business Partner information for id " + guid);
		  return null;
		}
	}

	private BPListSearchData getSearchData(String guid)  {
		BPListSearchData searchData = new BPListSearchData();
		searchData.setPartnerID(guid);
		return searchData;
	}

	public HashMap getBusinessPartnerListForTargetGroup(String guid) {
		HashMap bpMap = null;
		synchronized(this)  {
			bpMap = (HashMap)bpTableForTargetGroup.get(guid);
		}

		if(bpMap != null)  {
			return bpMap;
		}
		else {
			logInfo("load business partner list for Target Group id = " + guid);
			UserSessionData userData = UserSessionData.getUserSessionData(helpManager.getSession());
			AuctionBusinessObjectManager auctionBOM = (AuctionBusinessObjectManager)userData.getBOM(AuctionBusinessObjectManager.AUCTION_BOM);
			BusinessPartnerList bpList = auctionBOM.createBusinessPartnerList();
			TechKey key = new TechKey(guid);
			bpMap = bpList.searchBusinessPartnersByTG(key);
			if (bpMap != null) {
				synchronized(this) {
					bpTableForTargetGroup.put(guid, bpMap);
				}
			}
			return bpMap;
		}
	}
	
	public void clearTgBpSelection()  {
		if(selectionList != null)  {
			selectionList.clear();
		}
	}
	
	public void addBusinessPartner(String businessPartnerID)  {
		if(selectionList == null)  {
			selectionList = new Hashtable();
		}
		if(selectionList.get(AuctionTargetGroupBP.BUSINESS_PARTNER + businessPartnerID) == null)  {
			TargetGroupBusinessPartner tgBP = new TargetGroupBusinessPartner(businessPartnerID , TargetGroupBusinessPartner.BUSINESSPARTNER);
			selectionList.put(AuctionTargetGroupBP.BUSINESS_PARTNER + businessPartnerID , tgBP);	
		}
	}
	
	public void addTargetGroup(String targetGroupID)  {
		if(null == selectionList)  {
			selectionList = new Hashtable();
		}
		if(null == selectionList.get(AuctionTargetGroupBP.TARGET_GROUP + targetGroupID))  {
			TargetGroupBusinessPartner tgBP = new TargetGroupBusinessPartner(targetGroupID , TargetGroupBusinessPartner.TARGETGROUP);
			selectionList.put(AuctionTargetGroupBP.TARGET_GROUP + targetGroupID , tgBP);
		}
	}
	
	public Collection getTargetGroupBPSelection()  {
		if(selectionList != null)  {
			return selectionList.values();
		}  else  {
			return null;
		}
	}
	
	public Collection getTargetGroupBPCollection(Collection tgs , Collection bps)  {
		Collection items = new ArrayList();
		TargetGroupBusinessPartner tgBP = null;
		if(tgs != null)  {
			Iterator iterate = tgs.iterator();
			
			while(iterate.hasNext())  {
				tgBP = new TargetGroupBusinessPartner((String)iterate.next() , TargetGroupBusinessPartner.TARGETGROUP);
				items.add(tgBP);
			}
		}
		if(bps != null)  {
			Iterator iterator = bps.iterator();
			while(iterator.hasNext())  {
				tgBP = new TargetGroupBusinessPartner((String)iterator.next() , TargetGroupBusinessPartner.BUSINESSPARTNER);
				items.add(tgBP);
				
			}
		}
		return items;
	}
	
	public class TargetGroupBusinessPartner  {
		
		public static final int TARGETGROUP = 1;
		public static final int BUSINESSPARTNER = 2;
		private String id;
		private int type;
		
		public TargetGroupBusinessPartner(String id , int typ)  {
			this.id = id;
			this.type = typ;
		}
		/**
		 * @return
		 */
		public String getId() {
			return id;
		}

		/**
		 * @return
		 */
		public int getType() {
			return type;
		}

		/**
		 * @param string
		 */
		public void setId(String string) {
			id = string;
		}

		/**
		 * @param i
		 */
		public void setType(int i) {
			type = i;
		}

	}

	public void removeTargetGroupBusinessPartner(Object itemTGBP) {
		try  {
			if(itemTGBP instanceof TargetGroup)
				selectionList.remove(
					AuctionTargetGroupBP.TARGET_GROUP + ((TargetGroup)itemTGBP).getGuid());
			else if(itemTGBP instanceof BusinessPartner)  {
				selectionList.remove(
					AuctionTargetGroupBP.BUSINESS_PARTNER + ((TargetGroup)itemTGBP).getGuid());
			}  else if(itemTGBP instanceof TargetGroupBusinessPartner)  {
				TargetGroupBusinessPartner tgBP = (TargetGroupBusinessPartner)itemTGBP;
				if(tgBP.getType() == TargetGroupBusinessPartner.TARGETGROUP)  {
					selectionList.remove(
						AuctionTargetGroupBP.TARGET_GROUP + tgBP.getId());
				}  else  {
					selectionList.remove(
						AuctionTargetGroupBP.BUSINESS_PARTNER + tgBP.getId());
				}
			}
		}  catch(Exception exc)  {
			logError("Unable to remove the specific item from the selected target groups" , exc);
		}
	}
	/**
	 * @return
	 */
	public String getContext() {
		return context;
	}

	/**
	 * @param string
	 */
	public void setContext(String context) {
		this.context = context;
	}

}
