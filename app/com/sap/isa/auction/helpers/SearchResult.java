package com.sap.isa.auction.helpers;

import com.sap.isa.auction.bean.search.QueryResult;
import com.sap.isa.core.logging.IsaLocation;

public class SearchResult {

	private QueryResult queryResult;
	protected Object[] dataArray;
	protected int size;
	protected int fetchSize;
	protected int firstVisibleRow;
	
	/** 
	  *  The IsaLocation of the current Action.
	  *  This will be instantiated during perform and is always present.
	  */
	 protected IsaLocation log = IsaLocation.getInstance(SearchResult.class.getName());
	
	
	public SearchResult()  {
		dataArray = null;
		size = 0;
		fetchSize = 0;
		firstVisibleRow = 1;
		queryResult = null;	
	}
	
	public SearchResult(QueryResult result)  {
		queryResult = result;
		size = result.resultSize();
		firstVisibleRow = 1;
		dataArray = new Object[size];
	}
	
	public void setFetchSize(int fetchSize)  {
		this.fetchSize = fetchSize;
		//queryResult.setFetchSize(fetchSize);
	}
	
	public Object fetch(int pos)  {
		Object obj = dataArray[pos - 1]; 
		if(null == obj)  {
			queryResult.position(pos);
			obj = queryResult.fetchNext();
			dataArray[pos - 1] = obj;
		}
		return obj;
	}
	
	public Object[] fetch(int startIndex , int endIndex)  {
		 Object subsetArray[] = null;
		 if(endIndex > size)  {
		 	subsetArray = new Object[size - startIndex + 1];
		 	endIndex = size;
		 }  else {
			subsetArray = new Object[endIndex - startIndex + 1];
		 }
		 Object object = null;
		 connect();
		 for(int i = startIndex - 1, j = 0; i < endIndex; i++ , j++)  {
		 	object = dataArray[i];
		 	if(null == object)  {
		 		queryResult.position(i + 1);
		 		if(queryResult.available())
		 			object = queryResult.fetchNext();
		 		dataArray[i] = object;
		 	}
		 	subsetArray[j] = object;
		 }
		 return subsetArray; 
		
	}
	
	public void close()  {
		if(queryResult != null)  {
			if(!queryResult.isClosed())
				queryResult.close();
		}
		if(dataArray != null)  {
			for(int i = 0 ;i < dataArray.length; i++)  {
				dataArray[i] = null;
			}
		}
		size = 0;
		fetchSize = 0;
	}
	
	public int getSize()  {
		return size;
	}
	
	public void setFirstVisibleRow(int visibleRow)  {
		this.firstVisibleRow = visibleRow;
	}
	
	public void replaceObjectInPosition(Object object , int position)  {
		if(position > 0 && position <= dataArray.length)  {
			dataArray[position - 1] = object;
		}
	}
	
	public Object[] getDataArray()  {
		return dataArray;
	}

	/**
	 * 
	 */
	public void disconnect() {
		if(queryResult != null)  {
			if(!queryResult.isClosed() && queryResult.resultSize() > 0 )
				try  {
					queryResult.disconnect();
				}  catch(Exception exc)  {
					log.debug("Disconnect of QueryResult failed" ,exc);
				}
			else
				queryResult.close();
		}
	}
	
	public void connect()  {
		if(queryResult != null && !queryResult.isClosed()
			&& queryResult.resultSize() > 0 
			&& !isConnected())  {
			queryResult.connect();
		}
	}

	public boolean isConnected() {
		if(queryResult != null)  {
			return queryResult.isConnected();
		}
		return true;
	}
}
