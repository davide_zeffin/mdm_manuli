package com.sap.isa.auction.helpers;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Iterator;

import com.sap.isa.auction.actionforms.seller.AuctionSearchFormConstants;
import com.sap.isa.auction.bean.Auction;
import com.sap.isa.auction.bean.AuctionItem;
import com.sap.isa.auction.bean.AuctionStatusEnum;
import com.sap.isa.auction.bean.Bid;
import com.sap.isa.auction.bean.Winner;
import com.sap.isa.auction.businessobject.BidManager;
import com.sap.isa.auction.businessobject.WinnerManager;
import com.sap.isa.auction.businessobject.b2x.AuctionBusinessObjectManager;
import com.sap.isa.auction.exception.AuctionRuntimeExceptionBase;
import com.sap.isa.core.UserSessionData;


/**
 * Title:			Internet Sales Private Auctions
 * Description:
 * @Copyright: 		Copyright (c) 2004
 * Company:			SAPLabs LLC
 * @author 
 * Created on: 		May 10, 2004
 */

public class AuctionDetailsHelper extends GenericHelper  {

    private BidManager bidManager;
    private WinnerManager winnerManager;
    private Auction auction;
    private Auction destination;
    private Object errorObject;

    public AuctionDetailsHelper() {
    }

    public Auction getAuctionById(String auctionId, Integer fromSrc) {
		final String METHOD = "getAuctionById";
        entering(METHOD);
        destination = null;
        int comefrom = fromSrc!=null? fromSrc.intValue():AuctionSearchFormConstants.SRC_MONITORING;
        if (comefrom == AuctionSearchFormConstants.SRC_MONITORING)
        	auction = (Auction) ((SearchHelper)helpManager.getHelper(HelpManager.SEARCHHELPER)).getAuctionById(auctionId);
        else if (comefrom == AuctionSearchFormConstants.SRC_PUBLISHQUEUE)
			auction = (Auction) ((SearchHelper)helpManager.getHelper(HelpManager.PUBLISHQUEUEHELPER)).getAuctionById(auctionId);
        else if (comefrom == AuctionSearchFormConstants.SRC_MANUALWINNERQUEUE)
			auction = (Auction) ((SearchHelper)helpManager.getHelper(HelpManager.MANUALWINNERHELPER)).getAuctionById(auctionId);
        exiting(METHOD);
        return auction;
    }
    
    public void setAuction(Auction auction)  {
    	this.auction = auction;
    }

    public Auction getSavedAuction()  {
        SearchHelper searchHelper = (SearchHelper)helpManager.getHelper(HelpManager.SEARCHHELPER);
        return searchHelper.getAuctionById(searchHelper.getLastAccessedAuctionId());
    }

    public Collection getProducts(Auction auction) {
		final String METHOD = "getProducts";
		entering(METHOD);
    	Collection products = new ArrayList();
    	Collection aucItems = auction.getItems();
    	if (aucItems != null && aucItems.size()>0){
			ProductHelper productHelper = (ProductHelper)helpManager.getHelper(HelpManager.PRODUCTHELPER);
			Iterator it = aucItems.iterator();
			AuctionLineItem auctionLineItem = null;
			while (it.hasNext()){
        		Object obj = it.next();
				if(obj instanceof AuctionItem)  {				
					auctionLineItem = productHelper.createAuctionLineItem((AuctionItem)obj);
					products.add(auctionLineItem);
					
				}
	   		}//end while
    	}
		exiting(METHOD);
        return products;
    }
	
	public Collection getTargetGroupBPs(Auction auction){
		final String METHOD = "getTargetGroupBPs";
		entering(METHOD);
		Collection tgBps = null;
		TargetGroupBPHelper targetsHelper = (TargetGroupBPHelper)helpManager.getHelper(HelpManager.TARGETGROUPHELPER);
		if(auction != null)  {
			tgBps = targetsHelper.getTargetGroupBPCollection(auction.getTargetGroups() , auction.getBusinessPartners());
		}		
		exiting(METHOD);
		return tgBps;
	}
    public Bid getHighestBid(Auction auction) {
		final String METHOD = "getHighestBid";
		entering(METHOD);
        Bid bid = null;
        int statusInt = auction.getStatus();
        if (statusInt == AuctionStatusEnum.PUBLISHED.getValue() ||
            statusInt == AuctionStatusEnum.CLOSED.getValue() ||
            statusInt == AuctionStatusEnum.FINALIZED.getValue()
			|| statusInt == AuctionStatusEnum.NOT_FINALIZED.getValue()
			|| statusInt == AuctionStatusEnum.FINISHED.getValue()
			|| statusInt == AuctionStatusEnum.CHECKEDOUT.getValue()
        )
        {
            logInfo("auction is published, get the bid");
            Bid[] bids = getBidManager().getHighestBids(auction);
             if (bids != null && bids.length>0) {
                for (int i=0; i<bids.length; i++) {
                	 bid = bids[0];
                	 break;
              	}
            }
        }
        exiting(METHOD);
        return bid;
    }
    public Collection getBidHistory(Auction auction) {
		final String METHOD = "getBidHistory";
		entering(METHOD);
        Collection bidHistory = getBidManager().getBidHistory(auction);
        exiting(METHOD);
		return bidHistory;
    }

    public Collection getWinners(Auction auction) {
        final String METHOD = "getWinners";
		entering(METHOD);
        Collection winners = new ArrayList();
        Object[] data = getWinnerManager().getWinners(auction);
        if (data != null) {
            for (int i= 0; i<data.length; i++) {
                winners.add(data[i]);
            }
        }
        //winners = LoadAuction.getInstance().loadWinners(auction);
		exiting(METHOD);
        return winners;
    }

    public boolean close()  {
		final String METHOD = "close";
		entering(METHOD);
        clearErrors();
        try  {
            getAuctionManager().modifyStatus(auction, AuctionStatusEnum.CLOSED);
        }  catch(AuctionRuntimeExceptionBase aExc)  {
            logInfo("Unable to perform close operation" , aExc);
            errorObject = aExc;
            return false;
        }  catch(Exception exc)  {
            errorObject = exc;
            logInfo("Unable to publish the auction" , exc);
            return false;
        }
		exiting(METHOD);
        return true;
    }

     public boolean publish()  {
		final String METHOD = "publish";
		entering(METHOD);
        clearErrors();
        try  {
			getAuctionManager().modifyStatus(auction, AuctionStatusEnum.PUBLISHED);
        }  catch(AuctionRuntimeExceptionBase aExc)  {
            logInfo("Unable to perform publish operation" , aExc);
            errorObject = aExc;
            auction.setUserObject(aExc);
            return false;
        }  catch(Exception exc)  {
            logInfo("Unable to publish the auction" , exc);
            errorObject = exc;
            auction.setUserObject(exc);
            return false;
        }
		exiting(METHOD);
        return true;
    }

    public void copyAuction(String name)  {
		final String METHOD = "copyAuction";
		entering(METHOD);
        destination = (Auction)auction.clone();
        //@todo
        destination.setAuctionName(name != null ? name : "");
        Calendar cal = Calendar.getInstance();
        cal.add(java.util.Calendar.DATE, 15);
        destination.setQuotationExpirationDate(new Timestamp(cal.getTime().getTime()));
        destination.setAuctionId("");
        destination.setStatus(AuctionStatusEnum.OPEN.getValue());
		exiting(METHOD);
    }

    public void clean()  {
		final String METHOD = "getAuctionById";
				entering(METHOD);
        super.clean();
		bidManager = null;
		winnerManager = null;
		auction = null;
		destination = null;
		errorObject = null;
		exiting(METHOD);
    }

    private BidManager getBidManager() {
		final String METHOD = "getBidManager";
		entering(METHOD);
        if (bidManager == null) {
            logInfo("bidManager is null, create a new one");
            UserSessionData userSession = UserSessionData.getUserSessionData(helpManager.getSession());
            AuctionBusinessObjectManager bom = (AuctionBusinessObjectManager)userSession.getBOM(AuctionBusinessObjectManager.AUCTION_BOM);
            logInfo("create the BidManager from AuctionBusinessObjectManager");
            if (bom.getBidManager() != null)
                bidManager = bom.getBidManager();
            else
                bidManager = bom.createBidManager();
        }
		exiting(METHOD);
        return bidManager;
    }

	private WinnerManager getWinnerManager() {
		final String METHOD = "getWinnerManager";
		entering(METHOD);
        if (winnerManager == null) {
            logInfo("WinnerManager is null, create a new one");
            UserSessionData userSession = UserSessionData.getUserSessionData(helpManager.getSession());
            AuctionBusinessObjectManager bom = (AuctionBusinessObjectManager)userSession.getBOM(AuctionBusinessObjectManager.AUCTION_BOM);
            logInfo("create the WinnerManager from AuctionBusinessObjectManager");
            if (bom.getWinnerManager() != null)
                winnerManager = bom.getWinnerManager();
            else
                winnerManager = bom.createWinnerManager();
        }
		exiting(METHOD);
        return winnerManager;
    }

    public Auction getAuction()  {
        return auction;
    }

    public Auction getCopiedAuction()  {
        return destination;
    }

    public void clearErrors()  {
        errorObject = null;
        if(auction != null)  {
        	auction.setUserObject(null);
        }
    }

    public Object getErrors()  {
        return errorObject;
    }
    
    public Auction getAuctionByGUID(String auctionGUID) {
		final String METHOD = "getAuctionByGUID";
		entering(METHOD);
    	Auction auction = null;
		try {
			auction = getAuctionManager().getAuctionById(auctionGUID); 
		} catch (Exception exc) {
			logInfo("Unable to retrieve the auction from query result in position " + auctionGUID , exc);
		}
		exiting(METHOD);
		return auction;
	}
	
	/**
	 * Returns whether an auction reserve has been met by a winning bid.
	 * 
	 * @param auction Auction whose reserve is being checked.
	 * @return Boolean whether auction reserve has been met.
	 */
	public boolean isAuctionReserveMet(Auction auction){
		final String METHOD = "isAuctionReserveMet";
		entering(METHOD);
		logInfo("Checking where auction reserve was met");
		boolean reserveMet = false;
		if (null != auction){
			Bid bid = getHighestBid(auction);
			if (null != bid){
				int bidAmount = bid.getBidAmount().intValue();
				if(auction.getReservePrice()!=null) {
					int reservePrice = auction.getReservePrice().intValue();
					logInfo("highest bid amount: " + bidAmount);
					logInfo("auction reserve price : " + reservePrice);
					if (bidAmount >= reservePrice){
						logInfo("reserve was met");
						reserveMet = true;
					} else {
						reserveMet = false;
						logInfo("reserve was not met");
					}
				}else
					reserveMet = true;
				
			}
		} else {
			logInfo("Auction was null");
		}
		
		exiting(METHOD);
		return reserveMet;
	}
}