/*
 * Created on May 3, 2004
 *
 * To change the template for this generated file go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
package com.sap.isa.auction.helpers;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;

import com.sap.isa.auction.bean.AuctionItem;
import com.sap.isa.auction.bean.SalesArea;
import com.sap.isa.auction.bean.b2x.PrivateAuction;
import com.sap.isa.auction.bean.b2x.PrivateAuctionItem;
import com.sap.isa.auction.businessobject.b2x.PrivateAuctionManager;
import com.sap.isa.auction.businessobject.targetGroup.TargetGroup;
import com.sap.isa.businesspartner.businessobject.BusinessPartner;
import com.sap.isa.catalog.webcatalog.WebCatItem;

/**
 * @author I803067
 *
 * To change the template for this generated type comment go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
public class AuctionEditHelper extends GenericHelper implements ProductsContainer , TargetGroupsContainer  {

	public static int CREATE = 1;
	public static int EDIT	 = 2;
	
	private PrivateAuction auction;
	private int createMode = CREATE;

	private boolean singleCreation = true;
	private Collection auctions;
	private int currentPos = 0;
	 
	/**
	 * @return
	 */
	public int getCreateMode() {
		return createMode;
	}

	/**
	 * @return
	 */
	public boolean isSingleCreation() {
		return singleCreation;
	}

	/**
	 * @param i
	 */
	public void setCreateMode(int i) {
		createMode = i;
	}

	/**
	 * @param b
	 */
	public void setSingleCreation(boolean b) {
		singleCreation = b;
	}

	private void setAuction(PrivateAuction auction)  {
		this.auction = auction;
	}
	/**
	 * @return
	 */
	public PrivateAuction getAuction() {
		return auction;
	}
	
	public Collection getAuctions()  {
		return auctions;
	}
	
	public void clean()  {
		if(auctions != null)  {
			Iterator iterator = auctions.iterator();
			try  {
				AuctionContainer auctionContainer = null;
				while(iterator.hasNext())  {
					auctionContainer = (AuctionContainer)iterator.next();
					if(auctionContainer.getStatus() == AuctionContainer.SAVED)
						auctionContainer.getAuction().revertRecording();
					auctionContainer.clean();
				}
				auctions.clear();
			}  catch(Exception exc)  {
				logError("Unable to clean the Edit Helper" ,exc);
			}
		}
		auction = null;
		setCreateMode(-1);
	}
	
	public void initializeSingleCreation()  {
		clean();
		//auction = createAuction();
		setCreateMode(CREATE);
		if(auctions != null)  {
			auctions.clear();
		}  else {
			auctions = new ArrayList();	
		}
		ProductHelper productHelper = (ProductHelper)helpManager.getHelper(HelpManager.PRODUCTHELPER);
		Collection items = productHelper.getSelectedItems();
		Iterator iterate = items.iterator();
		AuctionItem auctionItem = null;
		AuctionLineItem lineItem = null;
		while(iterate.hasNext())  {
			lineItem = (AuctionLineItem)iterate.next();
			if(null == auction)  {
				auction = addAuction(productHelper , lineItem , 1);
			}  else  {
				auctionItem = productHelper.createAuctionItem(lineItem);
				auction.addItem(auctionItem);
			}
		}
        addTargetBPToAuction(auction);
	}
	
	public void initializeMultipleCreation()  {
		clean();
		int pos = 1;
		auctions = new ArrayList();
		setCreateMode(CREATE);
		ProductHelper productHelper = (ProductHelper)helpManager.getHelper(HelpManager.PRODUCTHELPER);
		Collection containerItems = productHelper.getSelectedItems();
		Iterator iterator = containerItems.iterator();
		PrivateAuction auction = null;
		AuctionContainer auctionContainer = null;
		AuctionItem item = null;
		AuctionLineItem lineItem = null;
		while(iterator.hasNext())  {
			lineItem = (AuctionLineItem)iterator.next();
			if(lineItem.getQuantity() > lineItem.getQuantityPerAuction() && 
												lineItem.getQuantityPerAuction() > 0)  {
				double saveReq , availableQty;
				saveReq = availableQty = lineItem.getQuantity();
				lineItem.setQuantity(lineItem.getQuantityPerAuction());
				int i = 1;
				while(availableQty > lineItem.getQuantityPerAuction())  {
					auction = addAuction(productHelper , lineItem, pos++);
					availableQty = availableQty - lineItem.getQuantityPerAuction();
					auction.setAuctionName(auction.getAuctionName() + "-" + i++);
				}
				lineItem.setQuantity(availableQty);
				addAuction(productHelper , lineItem, pos++);
				lineItem.setQuantity(saveReq);
			}  else  {
				addAuction(productHelper , lineItem, pos++);
			}
		}
        
		setAuction((PrivateAuction)((AuctionContainer)((ArrayList)auctions).get(0)).getAuction());
                
	}

	private void addTargetBPToAuction(PrivateAuction auction) {
		TargetGroupBPHelper targetsHelper = (TargetGroupBPHelper)helpManager.getHelper(HelpManager.TARGETGROUPHELPER);
		if(targetsHelper.getTargetGroupBPSelection() != null)  {
		    Iterator iterat = targetsHelper.getTargetGroupBPSelection().iterator();
		    TargetGroupBPHelper.TargetGroupBusinessPartner targetBP = null;
		    while(iterat.hasNext())  {
		        targetBP = (TargetGroupBPHelper.TargetGroupBusinessPartner)iterat.next();
		        if(TargetGroupBPHelper.TargetGroupBusinessPartner.BUSINESSPARTNER == targetBP.getType())  {
		            auction.addBusinessPartner(targetBP.getId());
		        }  else  {
		            auction.addTargetGroup(targetBP.getId());
		        }
		    }
		}
	}
	
	private PrivateAuction addAuction(ProductHelper productHelper , AuctionLineItem lineItem, int pos)  {
		PrivateAuction auction = createAuction();
		AuctionContainer auctionContainer = new AuctionContainer(auction);
		AuctionItem item = productHelper.createAuctionItem(lineItem);
		auction.addItem(item);
		auctions.add(auctionContainer);
		auction.setAuctionName(item.getProductCode());
		auction.setAuctionTitle(item.getDescription());
		auction.setImageURL(item.getImage());
		auction.setAuctionId(Integer.toString(pos));
//      Add BP or TG to the auction
        addTargetBPToAuction(auction);
		return auction;
	}
	
	private PrivateAuction createAuction()  {
		PrivateAuction auction = new PrivateAuction();
		return auction;
	}
	
	public void editAuction()  {
		clean();
		AuctionDetailsHelper detailsHelper = (AuctionDetailsHelper)
						helpManager.getHelper(HelpManager.AUCTIONDETAILSHELPER);
		if(detailsHelper.getAuction() != null)  {
			auction = (PrivateAuction)getAuctionManager().getAuctionById(detailsHelper.getAuction().getAuctionId());
			setCreateMode(EDIT);
		}  else  {
			initializeSingleCreation();
		}
		if(auctions != null)  {
			auctions.clear();
		}  else {
			auctions = new ArrayList();	
		}
		AuctionContainer container = new AuctionContainer(auction);
		auctions.add(container);
		auction.startRecording();
		container.setStatus(AuctionContainer.SAVED);
	}
	
	public boolean saveAuctions()  {
		boolean flag = true;
		
		if(getCreateMode() == CREATE || getCreateMode() == EDIT)  {
			if(auctions == null || auctions.size() < 1)
				return true;
			Iterator iterator = auctions.iterator();
			AuctionContainer auctionContainer = null;
			PrivateAuction auction = null;
			while(iterator.hasNext())  {
				auctionContainer = (AuctionContainer)iterator.next();
				try  {
					auction = (PrivateAuction)auctionContainer.getAuction();
					auction.setUserObject(null);
					auction.setQuotationDuration(0,0,0);
					auction.setSalesArea(new SalesArea());
					if(auctionContainer.getStatus() == AuctionContainer.SAVED
						|| AuctionContainer.MODIFIED == auctionContainer.getStatus())  {
						auctionContainer.setStatus(AuctionContainer.MODIFIED);
						getPrivateAuctionMgr().modifyAuction(auction);
					} else  {
						auctionContainer.setStatus(AuctionContainer.NEW);
						getPrivateAuctionMgr().createAuction(auction);
					}
					auction.startRecording();
					auctionContainer.setStatus(AuctionContainer.SAVED);
				}  catch(Exception exc)  {
					logError("Unable to create Auction - " + auction.getAuctionName() , exc);
					auction.setUserObject(exc);
					flag = false;
				}
			}
		}
		if(flag)  {
			PublishQueueHelper afterSave = (PublishQueueHelper)
							helpManager.getHelper(HelpManager.PUBLISHQUEUEHELPER);
			Collection savedAuctions = new ArrayList();
			Iterator iterator = auctions.iterator();
			AuctionContainer container = null;
			while(iterator.hasNext())  {
				container = (AuctionContainer)iterator.next();
				savedAuctions.add(container.getAuction());
				container.getAuction().revertRecording();
			}
			afterSave.setContext(PublishQueueHelper.CREATION_CONTEXT);
			afterSave.setSearchList(new SearchResultCollection(savedAuctions));
			setCreateMode(-1);
			clean();
		}
		return flag;
	}
	
	protected PrivateAuctionManager getPrivateAuctionMgr()  {
		return (PrivateAuctionManager)getAuctionBOM().getAuctionManager();
	}

	/* (non-Javadoc)
	 * @see com.sap.isa.auction.helpers.ProductsContainer#getSelectedItems()
	 */
	public Collection getSelectedItems() {
		if(auction != null)  {
			return auction.getItems();
		}
		return null;
	}

	/* (non-Javadoc)
	 * @see com.sap.isa.auction.helpers.ProductsContainer#getSelectionCount()
	 */
	public int getSelectionCount() {
		if(auction != null && auction.getItems() != null)  {
			return auction.getItems().size();
		}
		return 0;
	}

	/* (non-Javadoc)
	 * @see com.sap.isa.auction.helpers.ProductsContainer#addItem(com.sap.isa.catalog.webcatalog.WebCatItem, double)
	 */
	public boolean addItem(WebCatItem item, double quantity) {
		ProductHelper productHelper = (ProductHelper)helpManager.getHelper(HelpManager.PRODUCTHELPER);
		if(auction != null)  {
			PrivateAuctionItem auctItem = null;
			if ( (auctItem = getItemExist(item.getProductID())) != null) {//exist
                auction.removeItem(auctItem);
				auctItem.setQuantity(auctItem.getQuantity()+quantity);
			} else {
				auctItem = (PrivateAuctionItem)productHelper.createAuctionItem(item);
				auctItem.setQuantity(quantity);
			}		
			auction.addItem(auctItem);
			return true;
		}
		return false;
	}

	private PrivateAuctionItem getItemExist(String id)  {
		PrivateAuctionItem item = null;
		if(auction != null  && auction.getItems() != null)  {
			Iterator iterate = auction.getItems().iterator();
			while(iterate.hasNext())  {
				item = (PrivateAuctionItem)iterate.next();
				if(item.getProductId().equals(id))  {
//                    auction.getItems().remove(item);                 
					return item;
				}
			}
		}
		return null;
	}
	
	/* (non-Javadoc)
	 * @see com.sap.isa.auction.helpers.ProductsContainer#removeItem(java.lang.String)
	 */
	public void removeItem(String id) {
		if(auction != null && auction.getItems() != null)  {
			Iterator iterate = auction.getItems().iterator();
			AuctionItem item = null;
			while(iterate.hasNext())  {
				item =(AuctionItem)iterate.next(); 
				if(item.getProductId().equals(id))  {
					auction.removeItem(item);
					break;
				}
			}
		}
	}
	
	public void clearSelection()  {
		if(auction != null && auction.getItems() != null)  {
			auction.setItems(new ArrayList());
//            Iterator iterate = auction.getItems().iterator();
//			while(iterate.hasNext())  {
//                auction.removeItem((AuctionItem)iterate.next());
//			}
		}
	}

	/* (non-Javadoc)
	 * @see com.sap.isa.auction.helpers.TargetGroupsContainer#addBusinessPartner(java.lang.String)
	 */
	public void addBusinessPartner(String businessPartnerID) {
		if(auction != null)  {
			auction.addBusinessPartner(businessPartnerID);
		}
	}

	/* (non-Javadoc)
	 * @see com.sap.isa.auction.helpers.TargetGroupsContainer#addTargetGroup(java.lang.String)
	 */
	public void addTargetGroup(String targetGroupID) {
		if(auction != null)  {
			auction.addTargetGroup(targetGroupID);
		}
	}
	/* (non-Javadoc)
	 * @see com.sap.isa.auction.helpers.TargetGroupsContainer#clearTgBpSelection()
	 */
	public void clearTgBpSelection()  {
		if(auction != null)  {
			auction.setBusinessPartners(new ArrayList());
			auction.setTargetGroups(new ArrayList());
		}
	}
	/* (non-Javadoc)
	 * @see com.sap.isa.auction.helpers.TargetGroupsContainer#getTargetGroupBPSelection()
	 */
	public Collection getTargetGroupBPSelection() {
		TargetGroupBPHelper targetsHelper = (TargetGroupBPHelper)helpManager.getHelper(HelpManager.TARGETGROUPHELPER);
		if(auction != null)  {
			return targetsHelper.getTargetGroupBPCollection(auction.getTargetGroups() , auction.getBusinessPartners());
		}
		return null;
	}

	/* (non-Javadoc)
	 * @see com.sap.isa.auction.helpers.TargetGroupsContainer#removeTargetGroupBusinessPartner(java.lang.Object)
	 */
	public void removeTargetGroupBusinessPartner(Object tgBP) {
		if(auction != null)  {
			if(tgBP instanceof TargetGroup && auction.getTargetGroups() != null)  {
				TargetGroup tg = (TargetGroup)tgBP;
				Iterator iterate = auction.getTargetGroups().iterator();
				while(iterate.hasNext())  {
					if(tg.getGuid().equals((String)iterate.next()))  {
						iterate.remove();
						break;
					}
				}
			}  else if(tgBP instanceof BusinessPartner && auction.getBusinessPartners() != null)  {
				BusinessPartner bp = (BusinessPartner)tgBP;
				Iterator iterate = auction.getBusinessPartners().iterator();
				while(iterate.hasNext())  {
					if(bp.getId().equals((String)iterate.next()))  {
						iterate.remove();
						break;
					}
				}
			}  else if(tgBP instanceof TargetGroupBPHelper.TargetGroupBusinessPartner)  {
				TargetGroupBPHelper.TargetGroupBusinessPartner tg =
						(TargetGroupBPHelper.TargetGroupBusinessPartner)tgBP;
				if(tg.getType() == TargetGroupBPHelper.TargetGroupBusinessPartner.BUSINESSPARTNER)  {
					Iterator iterate = auction.getBusinessPartners().iterator();
					while(iterate.hasNext())  {
						if(tg.getId().equals((String)iterate.next()))  {
							iterate.remove();
							break;
						}
					}
				}  else  {
					Iterator iterate = auction.getTargetGroups().iterator();
					while(iterate.hasNext())  {
						if(tg.getId().equals((String)iterate.next()))  {
							iterate.remove();
							break;
						}
					}
				}
			}
		}
	}

	/**
	 * @param i
	 */
	public void setCurrentPosition(int i) {
		if(auctions != null)  {
			try  {
				auction = (PrivateAuction)((AuctionContainer)((ArrayList)auctions).get(i)).getAuction();
				currentPos = i;
			}  catch(Exception exc)  {
				logError("Unable to change the auction position" , exc);
			}
		}
	}

	/**
	 * @return
	 */
	public int getCurrentPos() {
		return currentPos;
	}

	/**
	 * 
	 */
	public void cancel() {
		clean();
	}

	public void copyAuction(String auctionName , String title) {
		clean();
		AuctionDetailsHelper detailsHelper = (AuctionDetailsHelper)
						helpManager.getHelper(HelpManager.AUCTIONDETAILSHELPER);
		auction = (PrivateAuction)detailsHelper.getAuction().clone();
		if(null == auctions)  {
			auctions = new ArrayList();
		}  else  {
			auctions.clear();
		}
		auctions.add(new AuctionContainer(auction));
		auction.setAuctionName(auctionName);
		auction.setAuctionTitle(title);
		setCreateMode(CREATE);
	}

}
