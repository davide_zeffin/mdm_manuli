package com.sap.isa.auction.helpers;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.NumberFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;

import com.sap.isa.auction.businessobject.user.AuctionUser;
import com.sap.isa.core.UserSessionData;


/**
 * Title:
 * Description:
 * Copyright:    Copyright (c) 2003
 * Company:
 * @author
 * @version 1.0
 */

public class ConversionHelper extends GenericHelperBase {

    private DecimalFormat numberFormatter;
    private DecimalFormat currencyFormatter;
    private AuctionUser auctionUser;
    private String dateFormatPattern;

    public ConversionHelper() {
    }

    public BigDecimal uiCurrencyStringToBigDecimal(String amount) {
    	double doubleValue = 0.0; 
        try {
            entering("uiCurrencyStringToBigDecimal");
			if(amount != null) amount = amount.trim();
            logInfo("Before parseing, the currency value is: " + amount);
            Number value = null;
            value = getUserCurrencyFormatter().parse(amount);
            logInfo("After parseing, the BigDecimal currency value is: " + value.doubleValue());
            BigDecimal retAmount = new BigDecimal(value.doubleValue());
            exiting("uiCurrencyStringToBigDecimal");
            return retAmount;
        } catch(Exception ex) {
            logError("Abnormal exit of uiCurrencyStringToBigDecimal, not valid: " + amount , ex);
            try  {
            	doubleValue = Double.parseDouble(amount);
            }  catch(Exception exc1)  {
				doubleValue = 0.0;
            }
            return new BigDecimal(doubleValue);
        }
    }

    public BigDecimal uiQuantityStringToBigDecimal(String qty) {
    	double doubleValue = 0.0;
        try {
            entering("uiQuantityStringToBigDecimal");
			if(qty != null) qty = qty.trim();
            logInfo("Before parseing, the quantity value is: " + qty);
            Number value = null;
            value = getUserNumberFormatter().parse(qty);
            logInfo("After parseing, the BigDecimal quantity value is: " + value.doubleValue());
            BigDecimal retQty = new BigDecimal(value.doubleValue());
            exiting("uiQuantityStringToBigDecimal: " + qty);
            return retQty;

        } catch(Exception ex) {
            logError("Abnormal exit of uiQuantityStringToBigDecimal, not valid: " + qty , ex);
			try  {
				doubleValue = Double.parseDouble(qty);
			}  catch(Exception exc1)  {
				doubleValue = 0.0;
			}
            return new BigDecimal(doubleValue);
        }
    }

    public String bigDecimalToUICurrencyString(BigDecimal value, boolean noCurrency)  {
        StringBuffer retval = new StringBuffer();
        entering("bigDecimalToUICurrencyString");
        try  {
        	if (value != null) {
				return bigDecimalToUICurrencyString(value.doubleValue() , noCurrency);
	        }  else  {
	            logInfo("The amount is null");
	        }
        }  catch(Exception exc)  {
			logError("Unable to format the value - " + value , exc);
        	retval.append(value.doubleValue());
        }
        exiting("bigDecimalToUICurrencyString");
        return retval.toString();
    }

	public String bigDecimalToUICurrencyString(double value, boolean noCurrency)  {
		StringBuffer retval = new StringBuffer();
		entering("bigDecimalToUICurrencyString(double)");
		logInfo("Before formating, the BigDecimal currency value is: " + value);
		try  {
			retval.append(getUserCurrencyFormatter().format(value));
			logInfo("After formating, the currency value is: "+retval);

			if(!noCurrency)  {
				String currency = getCurrency();
				retval.append(" ");
				retval.append(currency);
			}
		}  catch(Exception exc)  {
			logError("Unable to format the value - " + value , exc);
			return Double.toString(value);
		}
		exiting("bigDecimalToUICurrencyString(double)");
		return retval.toString();
	}

    public String bigDecimalToUIQuantityString(BigDecimal qty)  {

        entering("bigDecimalToUIQuantityString");
		String retQty = "";
        try  {
			if (qty != null) {
				logInfo("Before formating, the BigDecimal quantity value is: " + qty.doubleValue());
				retQty = getUserNumberFormatter().format(qty.doubleValue());
				logInfo("After formating, the quantity value is: " + retQty);
			}
        	
        }  catch(Exception exc)  {
			logError("Unable to format the value - " + qty , exc);
			retQty = qty.toString();			
        }
        exiting("bigDecimalToUIQuantityString");
        return retQty;
    }

    public Date uiStringToJavaDate(String dateString) {
        entering("uiStringToJavaDate-parse the ui string to a java date oject");
        Date date = null;
        try {
            if (dateString != null) {
				dateString = dateString.trim();
                logInfo("to be parsed DateString: " + dateString);
                date = getDateFormatter(false).parse(dateString);
                logInfo("Parsed date: " + date.toString());
            }
        } catch(ParseException ex) {
            logInfo("error parsing the string " + dateString + ex);
            return null;
        }
        exiting("uiStringToJavaDate-parse the ui string to a java date oject");
        return date;
    }

    public String javaDatetoUILocaleSpecificString(Timestamp date)  {
        entering("JavaDateToUILocaleSpecificString-convert the Java date object to a String");
        String dateString = "";
        if (date != null) {
            logInfo("to be formatted date: " +date.toString());
            try{
                dateString = getDateFormatter(false).format(new Date(date.getTime()));
            } catch(Exception ex) {
                logError("Exception happened during formatting: " , ex);
            }
            logInfo("Formatted date string: " + dateString);
        }
        exiting("JavaDateToUIString-convert the Java date object to a String");
        return dateString;
    }

	public String javaDatetoUILocaleSpecificString(Timestamp date , boolean noTimeInfo)  {
		entering("JavaDateToUILocaleSpecificString-convert the Java date object to a String");
		String dateString = "";
		if (date != null) {
			logInfo("to be formatted date: " +date.toString());
			try{
				dateString = getDateFormatter(noTimeInfo).format(new Date(date.getTime()));
			} catch(Exception ex) {
				logError("Exception happened during formatting: " , ex);
			}
			logInfo("Formatted date string: " + dateString);
		}
		exiting("JavaDateToUIString-convert the Java date object to a String");
		return dateString;
	}
	
    /**
     * Returns locale pattern for current user date format. E.g. dd.MM.yy
     * @param noTimeInfo
     * @return
     */
    public String getLocaleDateFormatPattern(boolean noTimeInfo){
        final String METHOD = "enclosing_method";        
        entering(METHOD);
        String localePattern = "";        
        try{
            localePattern = getDateFormatter(noTimeInfo).toLocalizedPattern();
        } catch(Exception ex) {
            logError("Exception happened during formatting: " , ex);
        }
        exiting(METHOD);
        return localePattern;
    }
    
	public String getServerTimeZone(){
		return getDateFormatter(false).getTimeZone().getDisplayName(false, TimeZone.SHORT);
	}

    //DateFormatter, CurrencyFormatter, NumberFormatter to be used for conversion
    private SimpleDateFormat getDateFormatter(boolean noTimeInfo) {
        entering("getDateFormatter");		
       	SimpleDateFormat dateFormatter = new SimpleDateFormat(getUserDateFormatPattern(noTimeInfo), getUserLocale());
		dateFormatter.setTimeZone(TimeZone.getDefault());
		dateFormatter.getCalendar().setLenient(false);
		logInfo("created a new SimpleDateFormat instance");
        exiting("getDateFormatter");
        return dateFormatter;
    }

    private DecimalFormat getUserNumberFormatter() {
        entering("getUserNumberFormatter() -- get user specific number formatter");
        if (numberFormatter == null) {
            logInfo("the user specific numberFormatter is null, create a new one");
            AuctionUser user = getAuctionUser();
            Locale loc = getUserLocale();

            logInfo("create a new instance of NumberFormat for locale: " + loc);
            numberFormatter = (DecimalFormat)NumberFormat.getInstance(loc);
            logInfo("create a new instance of DecimalFormatSymbols for the locale: " +loc);
            DecimalFormatSymbols symbol = new DecimalFormatSymbols(loc);
            symbol.setDecimalSeparator(user.getDecimalSeparator().charAt(0));
            symbol.setGroupingSeparator(user.getGroupingSeparator().charAt(0));
            logInfo("apply the new instance of DecimalFormatSymbols to the formatter");
            numberFormatter.setDecimalFormatSymbols(symbol);
            logInfo("apply the user specific DecimalFormat to the formatter");
            numberFormatter.applyLocalizedPattern(user.getDecimalFormat());
        }
        exiting("getUserNumberFormatter() -- get user specific number formatter");
        return numberFormatter;
    }

    private DecimalFormat getUserCurrencyFormatter() {
        entering("getUserCurrencyFormatter -- get user specific currency formatter");
        if (currencyFormatter == null) {
            currencyFormatter = (DecimalFormat)getUserNumberFormatter().clone();
            currencyFormatter.setMaximumFractionDigits(2);
            currencyFormatter.setMinimumFractionDigits(2);
        }
        exiting("getUserCurrencyFormatter -- get user specific currency formatter");
        return currencyFormatter;
    }

	private String getUserDateFormatPattern(boolean noTimeInfo) {
		entering("getUserDateFormatPattern");
		String userDateFormatPattern = null;
		if (dateFormatPattern == null) {
			logInfo("the dateFormatPattern is null, get it from the AuctionUser");
			dateFormatPattern = getAuctionUser().getDateFormat();
			logInfo("the dateFormat pattern: " +dateFormatPattern);
		} 
		dateFormatPattern = dateFormatPattern.replace('J', 'y');
		dateFormatPattern = dateFormatPattern.replace('T', 'd');
		dateFormatPattern = dateFormatPattern.replace('m', 'M');	

		if(!noTimeInfo)
			userDateFormatPattern = dateFormatPattern+ " HH:mm:ss z";
		else
			userDateFormatPattern = dateFormatPattern;
		logInfo("the user dateFormat pattern: " +userDateFormatPattern);
		exiting("getUserDateFormatPattern");
		return userDateFormatPattern;
	}

    private AuctionUser getAuctionUser() {

        entering("getAuctionUser");
        if(null == auctionUser)  {
        	CurrencyHelper helper = (CurrencyHelper) helpManager.getHelper(HelpManagerBase.CURRENCYHELPER);
            auctionUser = helper.getAuctionUser();
        }
        exiting("getAuctionUser");
        return auctionUser;

    }

    private Locale getUserLocale()  {
        entering("getUserLocale");
        UserSessionData userSession = UserSessionData.getUserSessionData(helpManager.getSession());
        Locale loc = userSession.getLocale();
        logInfo("the locale is : "+loc);
        exiting("getUserLocale");
        return userSession.getLocale();
    }

	/**
	 * @param format
	 */
	public void setCurrencyFormatter(DecimalFormat format) {
		currencyFormatter = format;
	}

	/**
	 * @param string
	 */
	public void setDateFormatPattern(String format) {
		dateFormatPattern = format;
	}

	/**
	 * @param format
	 */
	public void setNumberFormatter(DecimalFormat format) {
		numberFormatter = format;
	}

}
