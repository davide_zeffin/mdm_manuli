/*
 * Created on Mar 10, 2004
 *
 */
package com.sap.isa.auction.logging;

import com.sap.tc.logging.Category;
import com.sap.tc.logging.Location;

/**
 * @author I802891
 *
 */
public class TraceHelper {
	static final String ENTER = "Entering";
	static final String EXIT = "Exiting";
	static final String EMPTY = "";
	private static final String ARG1 = "Args:{arg1 = {0}}";
	private static final String ARG2 = "Args:{arg1 = {0},arg2 = {1}}";
	private static final String ARG3 = "Args:{arg1 = {0},arg2 = {1},arg3 ={2}}";
	private static final String ARG4 = "Args:{arg1 = {0},arg2 = {1},arg3 = {2},arg4 = {3}}";
	private static final String ARG5 = "Args:{arg1 = {0},arg2 = {1},arg3 = {2},arg4 = {3},arg5={4}}";		
	private static final String ARG6 = "Args:{arg1 = {0},arg2 = {1},arg3 = {2},arg4 = {3},arg5={4},arg6={5}}";	
	
	private TraceHelper() {
	}

	/**
	 * @param args
	 * @return
	 */
	private static String getTraceArgsMsg(Object[] args) {
		if(args == null) return EMPTY;
		int i = args.length;
		
		switch(i) {
			case 1:return ARG1;
			case 2:return ARG2;
			case 3:return ARG3;
			case 4:return ARG4;
			case 5:return ARG5;
			case 6:
			default: return ARG6;
		}
	}
	
	public static void entering(Location loc, String methodName) {
		
		if(loc.bePath()) {
			loc.pathT(methodName,ENTER);			
		}
	}
			
	public static void entering(Location loc, String methodName, Object[] args) {

		if(loc.bePath()) {
			loc.pathT(methodName,ENTER + getTraceArgsMsg(args),args);			
		}
	}
	
	public static void exiting(Location loc, String methodName) {
		
		if(loc.bePath()) {
			loc.pathT(methodName,EXIT);			
		}
	}

	public static void exiting(Location loc, String methodName, Object[] args) {

		if(loc.bePath()) {
			loc.pathT(methodName,EXIT + getTraceArgsMsg(args),args);			
		}
	}
	
	public static void throwing(Location loc, String methodName, String msg, Throwable th) {
		if(loc.bePath()) {
			loc.throwing(methodName,th);			
		}
	}

	public static void catching(Location loc, String methodName, Throwable th) {
		if(loc.bePath()) {
			loc.catching(methodName,th);			
		}
	}

	/**
	 * 
	 * @param loc
	 * @param cat
	 * @param msg
	 * @param args
	 */
	public static void logDebug(Location loc, Category cat, String msg, Object[] args) {
		if(loc.beDebug()) {
			loc.debugT(cat,msg,args);
		}
	}
	
	public static void logDebug(Location loc, String msg, Object[] args) {
		if(loc.beDebug()) {
			loc.debugT(msg,args);
		}
	}
}
