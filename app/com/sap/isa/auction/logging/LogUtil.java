/*
 * Created on Mar 10, 2004
 *
 */
package com.sap.isa.auction.logging;

import java.io.PrintWriter;
import java.io.StringWriter;

/**
 * @author I802891
 *
 */
public class LogUtil {
	public static String getStackTraceAsString(Throwable th) {
		if(th == null) return null;
		StringWriter strW = new StringWriter(0);
		PrintWriter prW = new PrintWriter(strW);
		th.printStackTrace(prW);
		return strW.toString();
	}
	
	public static String getMsgPlusStackTraceAsString(Throwable ex) {
		if(ex == null) return null;		
		String msg = ex.getMessage();
		String st = getStackTraceAsString(ex);
		if(msg != null) return msg + '\n' + st;
		else return st;
	}	
}
