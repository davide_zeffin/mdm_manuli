/*
 * Created on Mar 23, 2004
 *
 */
package com.sap.isa.auction.logging;

import com.sap.tc.logging.Location;

/**
 * @author I802891
 *
 */
public class LocationProvider {
	public static final Location getLocation(Class clazz) {
		return Location.getLocation(clazz.getName());  
	}
	
	public static final Location getLocation(String name) {
		return Location.getLocation(name);
	}
	
}
