/*
 * Created on Mar 10, 2004
 *
 */
package com.sap.isa.auction.logging;

import com.sap.tc.logging.Category;
import com.sap.tc.logging.Location;

/**
 * @author I802891
 *
 */
public class LogHelper {
	private LogHelper() {
	}
	public static void logInfo(Category cat, Location loc, String msg, Object[] args) {
		if(cat.beInfo()) {
			if(args != null) {
				cat.infoT(loc,msg,args);
			}
			else {
				cat.infoT(loc,msg);
			}
		}
	}
	
	/**
	 * This method will put the stacktrace using the cat.warningT() call
	 */
	public static void logWarning(Category cat,Location loc,Throwable th) {
		if(cat.beWarning())
			cat.warningT(loc,LogUtil.getStackTraceAsString(th));
	}

	public static void logWarning(Category cat,Location loc,String msg, Throwable th) {
		if(cat.beWarning())
			cat.warningT(loc,LogUtil.getStackTraceAsString(th));
	}
	
	/**
	 * This method will put the stacktrace using the cat.errorT() call
	 */
	public static void logError(Category cat,Location loc,Throwable th) {
		cat.errorT(loc,LogUtil.getStackTraceAsString(th));	    
	}

	public static void logError(Category cat,Location loc, String msg, Throwable th) {
		cat.errorT(loc,LogUtil.getStackTraceAsString(th));	    
	}
	
	public static void logFatalError(Category cat, Location loc, String msg, Throwable th) {
		cat.fatalT(loc,LogUtil.getStackTraceAsString(th));
	}
}
