/*
 * Created on Mar 10, 2004
 *
 */
package com.sap.isa.auction.logging;

import com.sap.isa.core.logging.LogUtil;
import com.sap.tc.logging.Category;

/**
 * <p>
 * This is the Provider class for Lo gging categories. All Application components must
 * log only with these categories. Basic Categories are defined here.<br/> 
 * There are maintained transparently for the entire Application and are centrally configured here
 * </p> 
 * @author I802891
 */
public final class CategoryProvider {
	private CategoryProvider() {
	}
	
	private static Category rootCategory = Category.APPLICATIONS;
	private static Category uiCategory = LogUtil.APPS_USER_INTERFACE;
	private static Category bolCategory = LogUtil.APPS_BUSINESS_LOGIC;
	private static Category sysCategory = Category.SYSTEM;
	private static Category utilCategory = bolCategory;	
	
	public static Category getRootCategory() {
		return rootCategory;	
	}

	public static Category getUICategory() {
		return uiCategory;			
	}
	
	public static Category getBOLCategory() {
		return bolCategory;			
	}
	
	public static Category getSystemCategory() {
		return sysCategory;			
	}
	
	public static Category getUtilCategory() {
		return utilCategory;
	}
}
