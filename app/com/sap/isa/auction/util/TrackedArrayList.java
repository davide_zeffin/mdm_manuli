package com.sap.isa.auction.util;

import java.util.Collection;
import java.util.ArrayList;
import java.util.Iterator;

/**
 * Description:  Tracks the changes made to Array List in terms of additions and
 *              removals. Indexes of stored elements are not tracked
 * Copyright:    Copyright (c) 2002
 * Company:      SAP Lans, Inc
 * @author e-auctions
 * @version 0.1
 */

public class TrackedArrayList extends ArrayList {

    private transient ArrayList addedItems;
    private transient ArrayList removedItems;

    /**
     * Constructs an empty list with the specified initial capacity.
     *
     * @param   initialCapacity   the initial capacity of the list.
     * @exception IllegalArgumentException if the specified initial capacity
     *            is negative
     */
    public TrackedArrayList(int initialCapacity) {
        super(initialCapacity);
    }

    /**
     * Constructs an empty list.
     */
    public TrackedArrayList() {
        super();
    }

    /**
     * Constructs a list containing the elements of the specified
     * collection, in the order they are returned by the collection's
     * iterator.  The <tt>ArrayList</tt> instance has an initial capacity of
     * 110% the size of the specified collection.
     *
     * @param c the collection whose elements are to be placed into this list.
     */
    public TrackedArrayList(Collection c) {
        super(c);
    }


    /**
     * @return null if no item added
     */
    public Collection getAddedItems() {
        if(addedItems != null) {
            return (Collection)addedItems.clone();
        }
        return null;
    }

    /**
     * @return null if no item removed
     */
    public Collection getRemovedItems() {
        if(removedItems != null) {
            return (Collection)removedItems.clone();
        }
        return null;
    }

    /**
     * Flushes the internal list of added and removed items for fresh tracking
     */
    public void commit() {
        if(addedItems != null) addedItems.clear();
        if(removedItems != null) removedItems.clear();
    }

    /**
     * Resets the state to begining after last flush
     */
    public void revert() {
        if(addedItems != null) {
            Iterator it = addedItems.iterator();
            while(it.hasNext()) {
            	removeInternal(it.next());
            }
            addedItems.clear();
        }
        
        if(removedItems != null) {
            Iterator it = removedItems.iterator();
            while(it.hasNext()) {
                addInternal(it.next());
            }
			removedItems.clear();            
        }

    }
    
    /**
     * Avoids the tracking mechanism
     * @param o
     */
    public void addInternal(Object o) {
    	super.add(o);
    }
    
    /**
     * Avoids the tracking mechanism
     * @param o
     */
    public void removeInternal(Object o) {
    	int index = super.indexOf(o);
    	super.remove(index);
    }

    /**
     * Replaces the element at the specified position in this list with
     * the specified element.
     *
     * @param index index of element to replace.
     * @param element element to be stored at the specified position.
     * @return the element previously at the specified position.
     * @throws    IndexOutOfBoundsException if index out of range
     *		  <tt>(index &lt; 0 || index &gt;= size())</tt>.
     */
    public Object set(int index, Object element) {
        Object removed = super.set(index,element);
        if(removed != null) {
            if(removedItems == null) removedItems = new ArrayList();
            removedItems.add(removed);
        }
        if(addedItems == null) addedItems = new ArrayList();
        addedItems.add(element);

        return removed;
    }

    /**
     * Appends the specified element to the end of this list.
     *
     * @param o element to be appended to this list.
     * @return <tt>true</tt> (as per the general contract of Collection.add).
     */
    public boolean add(Object o) {
        boolean ret = super.add(o);
        // removed earlier and readded
        if(removedItems != null && removedItems.contains(o)) { 
        	removedItems.remove(o);
        }
        else {
			if(addedItems == null) addedItems = new ArrayList();
			addedItems.add(o);
        }
        return ret;
    }

    /**
     * Inserts the specified element at the specified position in this
     * list. Shifts the element currently at that position (if any) and
     * any subsequent elements to the right (adds one to their indices).
     *
     * @param index index at which the specified element is to be inserted.
     * @param element element to be inserted.
     * @throws    IndexOutOfBoundsException if index is out of range
     *		  <tt>(index &lt; 0 || index &gt; size())</tt>.
     */
    public void add(int index, Object element) {
        super.add(index,element);
        if(addedItems == null) addedItems = new ArrayList();
        addedItems.add(element);
    }

    /**
     * Removes the element at the specified position in this list.
     * Shifts any subsequent elements to the left (subtracts one from their
     * indices).
     *
     * @param index the index of the element to removed.
     * @return the element that was removed from the list.
     * @throws    IndexOutOfBoundsException if index out of range <tt>(index
     * 		  &lt; 0 || index &gt;= size())</tt>.
     */
    public Object remove(int index) {
        Object removed = super.remove(index);
        if(removedItems == null) removedItems = new ArrayList();
        if(removed != null) removedItems.add(removed);
    	return removed;
    }

    /**
     * Removes all of the elements from this list.  The list will
     * be empty after this call returns.
     */
    public void clear() {
        ArrayList toBeRemoved = new ArrayList();
        Iterator it = super.iterator();
        while(it.hasNext()) {
            toBeRemoved.add(it.next());
        }
        super.clear();
        if(removedItems == null)
            removedItems = toBeRemoved;
        else
            removedItems.addAll(toBeRemoved);
    }

    /**
     * Appends all of the elements in the specified Collection to the end of
     * this list, in the order that they are returned by the
     * specified Collection's Iterator.  The behavior of this operation is
     * undefined if the specified Collection is modified while the operation
     * is in progress.  (This implies that the behavior of this call is
     * undefined if the specified Collection is this list, and this
     * list is nonempty.)
     *
     * @param c the elements to be inserted into this list.
     * @throws    IndexOutOfBoundsException if index out of range <tt>(index
     *		  &lt; 0 || index &gt; size())</tt>.
     */
    public boolean addAll(Collection c) {
        boolean ret = super.addAll(c);
        Iterator it = c.iterator();
        if(addedItems == null) addedItems = new ArrayList();
        while(it.hasNext()) {
            addedItems.add(it.next());
        }
        return ret;
    }

    /**
     * Inserts all of the elements in the specified Collection into this
     * list, starting at the specified position.  Shifts the element
     * currently at that position (if any) and any subsequent elements to
     * the right (increases their indices).  The new elements will appear
     * in the list in the order that they are returned by the
     * specified Collection's iterator.
     *
     * @param index index at which to insert first element
     *		    from the specified collection.
     * @param c elements to be inserted into this list.
     * @throws    IndexOutOfBoundsException if index out of range <tt>(index
     *		  &lt; 0 || index &gt; size())</tt>.
     */
    public boolean addAll(int index, Collection c) {
        boolean ret = super.addAll(index,c);
        Iterator it = c.iterator();
        if(addedItems == null) addedItems = new ArrayList();
        while(it.hasNext()) {
            addedItems.add(it.next());
        }
        return ret;
    }

    /**
     * Removes from this List all of the elements whose index is between
     * fromIndex, inclusive and toIndex, exclusive.  Shifts any succeeding
     * elements to the left (reduces their index).
     * This call shortens the list by <tt>(toIndex - fromIndex)</tt> elements.
     * (If <tt>toIndex==fromIndex</tt>, this operation has no effect.)
     *
     * @param fromIndex index of first element to be removed.
     * @param toIndex index after last element to be removed.
     */
    protected void removeRange(int fromIndex, int toIndex) {
        ArrayList toBeRemoved = new ArrayList();
        while(fromIndex < toIndex) {
            Object removed = super.get(fromIndex);
            toBeRemoved.add(removed);
            fromIndex++;
        }
        super.removeRange(fromIndex,toIndex);
        if(removedItems == null)
            removedItems = toBeRemoved;
        else
            removedItems.addAll(toBeRemoved);
    }
}