/*
 * Created on Nov 3, 2003
 *
 */
package com.sap.isa.auction.util;

/**
 * @author I802891
 *
 */
public class OpenSQLUtil {

	public static String toOpenSQLCompliantString(String val) {
		if(val != null) val = val.trim();
		if(val != null && val.length() == 0) {
			return null;
		}
		else {
			return val;
		}
	}
}
