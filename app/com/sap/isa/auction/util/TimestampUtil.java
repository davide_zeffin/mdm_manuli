package com.sap.isa.auction.util;

import java.sql.Timestamp;

import com.sap.tc.logging.Location;
import com.sapportals.htmlb.type.DataDate;
import com.sapportals.htmlb.type.DataTime;
import com.sapportals.htmlb.type.Date;
import com.sapportals.htmlb.type.Time;

/**
 * Title:        Internet Sales
 * Description:
 * Copyright:    Copyright (c) 2002
 * Company:      SAPMarkets
 * @author
 * @version 1.0
 */

public class TimestampUtil {

    private static TimestampUtil instance;
    //for Logging
    private static Location log = Location.getLocation(TimestampUtil.class.getName());

    private TimestampUtil() {
    }

    public static TimestampUtil getInstance() {
        if (instance == null)
            instance = new TimestampUtil();
        return instance;
    }

    public Timestamp convertDateTimeToTimestamp(Date date, Time time) {
        Timestamp retval = null;
        if (date != null) {
            StringBuffer timestampstring = new StringBuffer(date.toString());
            timestampstring.append(" ");
            timestampstring.append(time.toString());
            timestampstring.append(".000000000");
            retval = Timestamp.valueOf(timestampstring.toString());
        }
        return retval;
    }

    public DataDate getDataDateFromTimestamp(Timestamp timestamp) {
        DataDate retval = null;
        if (timestamp != null) {
            retval = new DataDate(timestamp);
        }
        return retval;
    }

    public DataTime getDataTimeFromTimestamp(Timestamp timestamp) {
        DataTime retval = null;
        if (timestamp != null) {
            retval = new DataTime(timestamp);
        }
        return retval;
    }
}