package com.sap.isa.auction.util;

import java.sql.Timestamp;
import java.util.Calendar;
import java.util.Date;

import com.sap.isa.core.logging.IsaLocation;

/**
 * Title:
 * Description:
 * Copyright:    Copyright (c) 2003
 * Company:
 * @author
 * @version 1.0
 */

public class DateTimeUtil {

    private static SimpleDateFormatThreadLocal threadlocal = new SimpleDateFormatThreadLocal();
    static java.util.TimeZone GMT = java.util.TimeZone.getTimeZone("GMT");

	/** 
	  *  The IsaLocation of the current Action.
	  *  This will be instantiated during perform and is always present.
	  */
	 protected static IsaLocation log = IsaLocation.getInstance(DateTimeUtil.class.getName());

    private DateTimeUtil() {
    }

    /**
     * Date time utility method @@todo:: move to utility class
     */
    public static java.util.Date convertGMTStringToDate(String gmtTime) {
        if(gmtTime == null) return null;
        try {
            java.text.DateFormat parser = (java.text.DateFormat)threadlocal.get();
            return parser.parse(gmtTime);
        }
        catch(java.text.ParseException pe) {
        	log.debug("Error ", pe);
        }
        return null;
    }

    public static java.lang.String convertDateToGMTString(java.util.Date time) {
        if(time == null) return null;
        java.text.DateFormat parser = (java.text.DateFormat)threadlocal.get();
        return parser.format(time);
    }

    /**
     * Params are assumed to be positive and within valid range
     * Converts the days, hrs and mins duration into their equivalent
     * millis
     */
    public static long toTimeInMillis(int days, int hrs, int mins) {
        Calendar cal = Calendar.getInstance(GMT); // GMT Time zone
        long ref = 0L;
        cal.setTime(new java.util.Date(ref));

        cal.add(Calendar.DAY_OF_YEAR,days);
        cal.add(Calendar.HOUR_OF_DAY,hrs);
        cal.add(Calendar.MINUTE,mins);

        long total = cal.getTime().getTime();

        return total - ref;
    }

    /**
     *
     * To do this conversion a Reference Time of UTC 0 is taken
     * Converts the Millis into their equivalent duration for days, hrs and mins
     *
     * @return int[]    [0] days
     *                  [1] hrs
     *                  [2] mins
     */
    public static int[] fromTimeInMillis(long millis) {
        long now = System.currentTimeMillis();
        Calendar reference = Calendar.getInstance(GMT); // GMT Time zone
        reference.setTime(new Date(millis));

        int days = reference.get(Calendar.DAY_OF_YEAR) - 1; // 0 is treated as 1 day
        int hrs = reference.get(Calendar.HOUR_OF_DAY);
        int mins = reference.get(Calendar.MINUTE);

        //if(hrs > 0 || mins > 0) days = days - 1; // since this adds the next day

        return new int[] {days,hrs,mins};
    }

    /**
     * Returns the diff in 2 dates as millis
     * @param begin start Date
     * @param end end Date
     * @return long millis between end and begin
     */
    public static long diffMillis(Timestamp begin, Timestamp end) {
        return end.getTime() - begin.getTime();
    }

    /**
     * Adds or subtract specified Offset Millis to the Reference Date
     */
    public static Timestamp adjustDate(Timestamp ref, long offsetMillis) {
        long refMillis = ref.getTime();
        return new java.sql.Timestamp(refMillis + offsetMillis);
    }


    public static void main(String[] args) {
        Date date = convertGMTStringToDate("2003-06-17 25:59:37");
        String strDate = convertDateToGMTString(date);

        DateTimeUtil test= new DateTimeUtil();
        long days = 90 * 24 * 60 * 60 *1000L;
        long hrs = 21 * 60 * 60 * 1000;
        long mins = 59*60*1000;
        long total = days + hrs + mins;
        long val = toTimeInMillis(3,23,59);
        int[] vals = fromTimeInMillis(val);
        com.sap.isa.auction.util.Debug.print(vals[0] + vals[1] + vals[2]);
    }

    /**
     * SimpleDateFormat class is not thread safe. Thread Local is used to provide
     * thread safety
     */
    private static class SimpleDateFormatThreadLocal extends ThreadLocal {
        protected Object initialValue() {
            java.text.SimpleDateFormat parser = new java.text.SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            parser.setTimeZone(GMT);
            parser.setLenient(false);
            return parser;
        }
    }
}