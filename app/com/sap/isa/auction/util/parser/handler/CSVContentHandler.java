package com.sap.isa.auction.util.parser.handler;

import java.util.Collection;

/**
 * Title:
 * Description:
 * Copyright:    Copyright (c) 2004
 * Company:
 * @author
 * @version 1.0
 */
public interface CSVContentHandler extends ParserContentHandler {

	public void onStringToken(String content, int column);
	public void onDelimiterToken(String delimiterToken);
	public void onLineStart(String content, int lineNumber);
	public void onLineEnd(int lineNumber);
	public Collection getCollection();
}