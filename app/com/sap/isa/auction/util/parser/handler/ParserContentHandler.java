package com.sap.isa.auction.util.parser.handler;

/**
 * Title:
 * Description:
 * Copyright:    Copyright (c) 2004
 * Company:
 * @author
 * @version 1.0
 */
public interface ParserContentHandler {

	public void onStartDocument();
	public void onEndDocument();
}
