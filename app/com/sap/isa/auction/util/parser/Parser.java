package com.sap.isa.auction.util.parser;

/**
 * Title:
 * Description: Generic Parser Interface
 * Copyright:    Copyright (c) 2003
 * Company:
 * @author
 * @version 1.0
 */

public interface Parser {

	public void parse();
//	public void setInput(InputStream inputStream);
//    public InputStream getInput();
//    public void setExpression(String regEx);
//    public String getExpression();

}
