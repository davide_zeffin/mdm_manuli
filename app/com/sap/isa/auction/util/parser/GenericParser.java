package com.sap.isa.auction.util.parser;

import java.io.InputStream;

import com.sap.tc.logging.Category;
import com.sap.tc.logging.Location;
import com.sap.isa.auction.logging.CategoryProvider;
import com.sap.isa.auction.logging.LogHelper;
import com.sap.isa.auction.logging.TraceHelper;

/**
 * Title:
 * Description:
 * Copyright:    Copyright (c) 2003
 * Company:
 * @author
 * @version 1.0
 */

public abstract class GenericParser implements Parser
 {
	private InputStream inputStream;
//	  private String expression;
	private String sourceURL;
    
	protected static Category logger = CategoryProvider.getUtilCategory();
	protected static Location tracer;


	public static final String COMMA = ",";
	public static final String DBLQUOTE = "\"";
	public static final String SNGQUOTE = "'";
    	
	public GenericParser(){
		tracer =  Location.getLocation(this.getClass());
	}
	/**
	 * 
	 */
	public abstract void parse();

	/**
	 * 
	 */
	public void setInput(InputStream inputStream) {
		this.inputStream = inputStream;
	}

	/**
	 * 
	 */
	public InputStream getInput(){
		return this.inputStream;
	}

	/**
	 * 
	 * @return
	 */
	public String getSourceURL(){
		return this.sourceURL;
	}
	
	/**
	 * 
	 * @param filePath
	 */
	public void setSourceURL(String sourceURL){
		this.sourceURL = sourceURL;
	}
	
	/**
	 * Logs message at the info logging level
	 * @param message String to log
	 */
	protected void logInfo(String message){
		logInfo(message, null);
	}
	
	/**
	 * Logs message and context at the info logging level
	 * @param message String to log
	 * @param object Context to log. Usually an exception parameter is passed
	 */
	protected void logInfo(String message, Object object){
		tracer = Location.getLocation(this.getClass());
		LogHelper.logInfo(logger, tracer, message, new Object[]{object});		
	}
    
	/**
	 * 
	 * @param text
	 */
	protected void entering(String text)  {
		tracer = Location.getLocation(this.getClass());
		TraceHelper.entering(tracer, text , null);
	}

	/**
	 * 
	 * @param text
	 */
	protected void exiting(String text)  {
		tracer = Location.getLocation(this.getClass());
		TraceHelper.exiting(tracer, text , null);
	}
    
	protected void debug(String text){
		debug(text, null);
	}
    
	protected void debug(String text, Object object){
		tracer = Location.getLocation(this.getClass());
		TraceHelper.logDebug(tracer, text, new Object[]{object});
	}
}