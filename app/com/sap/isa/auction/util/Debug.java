/*****************************************************************************
    Copyright (c) 2001, SAPMarkets Palo Alto, USA, All rights reserved.

*****************************************************************************/
package com.sap.isa.auction.util;


 //The value of DEBUG should be FALSE if debugging messages are not to be printed

 public class Debug {
	private final static boolean debug = true; /*the value of this field should be false
	                                           if debugging messages are not be printed*/
/**
 * Insert the method's description here.
 * Creation date: (8/29/00 9:35:07 PM)
 * @param _num int
 */
public static void print(int _num) {
	try {
		if (debug) {

			DebugLog debuglog = new DebugLog();
			debuglog.print(Integer.toString(_num));
		}
	}
	catch(Exception _e){
		DebugLog debuglog = new DebugLog();
		debuglog.print(_e);
	}
	}
/**
 * Insert the method's description here.
 * Creation date: (8/15/00 2:35:59 PM)
 */
public static void print(Exception _ex) {

	try {
		if (debug) {

			DebugLog debuglog = new DebugLog();
            java.io.StringWriter sw = new java.io.StringWriter();
            java.io.PrintWriter pw = new java.io.PrintWriter(sw,true);
            _ex.printStackTrace(pw);
			debuglog.print(sw.toString());
		}
	}
	catch(Exception _e){
		DebugLog debuglog = new DebugLog();
		debuglog.print(_e);
	}
	}
/**
 * If the value of debug is TRUE, print the message .
 * Creation date: (8/4/00 4:50:08 PM)
 * @param _message java.lang.String
 */
public static void print(String _message) {

	try {
		if (debug) {

			DebugLog debuglog = new DebugLog();
			debuglog.print(_message);
		}
	}
	catch(Exception _e){
		DebugLog debuglog = new DebugLog();
		debuglog.print(_e);
	}

	}
/**
 * Insert the method's description here.
 * Creation date: (8/29/00 3:42:37 PM)
 * @param _status boolean
 */
public static void print(boolean _status) {

   try {
		if (debug) {

			DebugLog debuglog = new DebugLog();
			if (_status)
				debuglog.print("true");
			else
				debuglog.print("false");

		}
	}
	catch(Exception _e){
		DebugLog debuglog = new DebugLog();
		debuglog.print(_e);
	}
	}
}
