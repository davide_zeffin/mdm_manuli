package com.sap.isa.auction.util;

import javax.servlet.http.HttpSession;
import javax.servlet.jsp.PageContext;

import com.sap.isa.auction.logging.CategoryProvider;
import com.sap.isa.auction.logging.LogHelper;
import com.sap.isa.core.UserSessionData;
import com.sap.isa.core.action.SwitchAccessibilityAction;
import com.sap.isa.ui.taglib.PageLoadingTag;
import com.sap.isa.ui.uiclass.BaseUI;
import com.sap.tc.logging.Category;
import com.sap.tc.logging.Location;

/**
 * Title:			Internet Sales Selling Via eBay
 * Description:
 * @Copyright: 		Copyright (c) 2004
 * Company:			SAPLabs LLC
 * Created on:		Dec 1, 2004
 */
public class AccessibilityUtil {

    protected static Category logger = CategoryProvider.getUICategory();
    protected static Location tracer = Location.getLocation(AccessibilityUtil.class);
    /**
     * 
     */
    public AccessibilityUtil() {
        super();
    }

    /**
     * Returns whether the accessibility switch is turned on for the application
     * @param pageContext
     * @return boolean Whether the acc switch is on
     */
    public synchronized static boolean isAccessibleContext(PageContext pageContext){
        BaseUI baseUI = getBaseUI(pageContext);
        boolean accessible = false;
        if (null == baseUI){
            LogHelper.logInfo(logger, tracer, "BaseUI was null. Accessibility " +
                    "functionality is unavailable", null);
        } else {
            accessible = baseUI.isAccessible;
        }
        return accessible;        
    }
    
//    /**
//     * Returns whether the accessibility switch is turned on. Alternate method
//     * using a core class, this method is typically used in non-jsp situations.
//     * 
//     * @param session
//     * @return boolean Whether the acc switch is on
//     */
//    public synchronized static boolean isAccessibleMode(HttpSession session){
//        if (null == session){
//            throw new IllegalArgumentException("Session parameter was null");
//        }        
//        UserSessionData userSessionData = UserSessionData.getUserSessionData(session);
//        if (null == userSessionData){
//            userSessionData = UserSessionData.createUserSessionData(session);
//        }
//        return SwitchAccessibilityAction.getAccessibilitySwitch(userSessionData);
//    }

    /**
     * Returns either a newly created BaseUI object or an existing one from the 
     * pagecontext 
     * @return BaseUI object
     */
    public static BaseUI getBaseUI(PageContext pageContext){
        BaseUI baseUI = (BaseUI) pageContext.getAttribute(PageLoadingTag.BASEUI);
        if (null == baseUI){
            baseUI = new BaseUI(pageContext);
        }
        return baseUI;
    }        
}
