/**
 *  (C) Copyright 2000 by SAPMarkets, Inc.,
 *  3475 Deer Creek Road, Palo Alto, CA, 94304, U.S.A.
 *  All rights reserved.
 *  =============================================================================
 *  Description :Utility  class for the auction module
 */
package com.sap.isa.auction.util;
import java.math.BigDecimal;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.NoSuchElementException;
import java.util.StringTokenizer;
import java.util.TimeZone;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.w3c.dom.Document;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import com.sap.isa.auction.businessobject.AuctionBusinessObjectManagerBase;
import com.sap.isa.backend.r3base.util.Conversion;
import com.sap.isa.businessobject.BusinessObjectManager;
import com.sap.isa.core.UserSessionData;
import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.core.util.CodePageUtils;

public class EAUtilities {
    private static EAUtilities _instance;
    //for Logging
    private static IsaLocation log = IsaLocation.getInstance(EAUtilities.class.getName());
    public static final String XMLHEADER = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>";
    private EAUtilities() {
    }

    public static EAUtilities getInstance() {
        if(_instance == null) {
            _instance = new EAUtilities();
        }
        return _instance;
    }

    /**
     * Used to display a date in a string format ex:01/02/2001
     */
    public String dateToString(java.util.Date date) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        int day = cal.get(Calendar.DAY_OF_MONTH);
        int month = cal.get(Calendar.MONTH);
        int year = cal.get(Calendar.YEAR);

        String m=null;
        String d=null;

        if(month<=9)
            m = "0" + month;
        else
            m = String.valueOf(month);

        if(day<=9)
            d = "0" + day;
        else
            d = String.valueOf(day);
        String strDate = m + "/" + d + "/" + String.valueOf(year);
        //System.out.println("The string date is" +  strDate);
        return strDate;
    }


    /**
     *  Timestamp to string from 12-31-2001T15:67:30.0 to 12/31/2001 03:67 PM    *
     */
    public String timestampToString(Timestamp ts) {
        //System.out.println(" The time stamp is " + ts);
        String temp = ts.toString();
        if(ts==null) {
            return null;
        }
        int hour = Integer.parseInt(temp.substring(11,13));
        String ampm =  hour>12 ?  (hour-12) + ":" + temp.substring(14,
                                                                   16) + " PM" : (hour) + ":" + temp.substring(14,16) + " AM";

        return temp.substring(5,
                              7) + "/" + temp.substring(8,
                                                        10) + "/" + temp.substring(0,4) + " " + ampm;
    }

    /**
     * Convert Timestamp to String format in GMT timeZone
     */
    public String timestampToGMTString(Timestamp ts) {
        TimeZone gmt = TimeZone.getTimeZone ("GMT" );
        GregorianCalendar cal = new GregorianCalendar(gmt);

        // set the timestamp to the calendar
        Date dat = new Date();
        dat.setTime(ts.getTime());
        cal.setTime(dat);

        DateFormat df = DateFormat.getDateTimeInstance();
        df.setCalendar(cal);

        String dateString = df.format(cal.getTime()) + " GMT";
        return dateString;

    }



    /**
     * To convert a java timestamp to the format CRM required timestamp:" yyyymmddhhmmss"
     *
     * @param time Java Timestamp object
     * @return Qualified CRM format of the importing Java Timestamp
     */
    public String timestampToCRMTimestamp (Timestamp time) {

        StringTokenizer timeT = new StringTokenizer(time.toString(), "- :.");
        StringBuffer CRMTime = new StringBuffer(" ");
        while (CRMTime.length()<15 && timeT.hasMoreTokens()) {
            CRMTime.append(timeT.nextToken());
        }
        return CRMTime.toString();
    }
    /**
     * Strips the string formatted date(01/01/2001 or 10/10/2001 2:14 AM) and returns a timestamp equivalent for it
     * Also yyyMMddTHH:mm:ss to a timestamp object
     */
    public Timestamp stringToTimestamp(String strDate) {

        try {
            if(strDate==null) return null;
            SimpleDateFormat _dateFormat = new SimpleDateFormat("yyyyMMdd'T'HH:mm:ss");
            java.util.Date _stDate = _dateFormat.parse(strDate);
            if(_stDate==null) return null;
            return new Timestamp(_stDate.getTime());
        }
        catch(ParseException pe) {
            //System.out.println("The strdate in eautilities is " + strDate);
            Timestamp ts = null;
            if(strDate == null)
                return null;
            try {
                java.util.Date dat = new java.util.Date();
                Calendar cal = Calendar.getInstance();
                cal.setTime((Date)dat);
                String hour = String.valueOf(cal.get(Calendar.HOUR_OF_DAY));
                String minute = String.valueOf(cal.get(Calendar.MINUTE));

                StringTokenizer st = new StringTokenizer(strDate);

                String date = st.nextToken(" ");

                // Get the date part
                StringTokenizer stdate = new StringTokenizer(date);
                String month = stdate.nextToken("/");
                String day = stdate.nextToken("/");
                String year = stdate.nextToken("/");

                // System.out.println(" The date is " + month + day + year);

                try {
                    // process the time part
                    String time = st.nextToken(" ");
                    String ampm = st.nextToken();
                    st = new StringTokenizer(time);
                    String h = st.nextToken(":");
                    String m = st.nextToken(":");

                    // If PM add 12
                    if(ampm!=null &&  ampm.equals("PM")) {
                        int temp = Integer.parseInt(h)+12;
                        h = String.valueOf(temp);
                    }
                    ts = Timestamp.valueOf(year + "-" + month + "-" +day + " " + h + ":" + m + ":" + "00.000000000");

                } catch(NoSuchElementException ne) {
                    // if not time has been specified return date with current time
                    ts = Timestamp.valueOf(year + "-" + month + "-" +day + " " + hour + ":" + minute + ":" + "00.000000000");; // no time specified
                    return ts;
                }
            } catch(Exception e) {
                // if date/time has not been set then send the current date and time.
                // should never happen if the UI takes care of the this exception
                ts = new Timestamp(new java.util.Date().getTime());
            }
            return ts;
        }
    }

    /**
     *  Returns a delimited string of target groups
     *  @param tgs  array contain the ids of target groups selected
     *  @param delim  delimited for the String .
     */
    public String getTargetGroupDelimited(String[] tgs, String delim) {
        String targetGroup = "";
        if(tgs == null)
            return null;
        if(delim == null)
            return null;
        if(tgs.length == 1)
            return tgs[0];
        for(int i=0; i<tgs.length;i++) {
            if(i!=0 &&  i != tgs.length)
                targetGroup = targetGroup + delim;
            targetGroup = targetGroup + tgs[i];
        }
        return targetGroup;

    }

    /**
    *  Returns a delimited string of target groups
    *  @param tgs String contain the ids of target groups delimited
    *  @param delim  delimiter for the String .
    */
    public Object[] getTargetGroups(String tgs, String delim) {
        if(tgs == null)
            return null;
        if(delim == null)
            return null;
        ArrayList targetGroup = new ArrayList();
        StringTokenizer st = new StringTokenizer(tgs,delim);
        while(st.hasMoreTokens()) {
            targetGroup.add(st.nextToken(delim));
        }
        return targetGroup.toArray();

    }

    /**
     *  Returns a delimited string of category paths
     *  @param  cp array contain the category paths selected
     *  @param delim  delimited for the String .
     */
    public String getCategoryPathDelimited(String[] cp, String delim) {
        String categoryPath = "";
        if(cp == null)
            return null;
        if(delim == null)
            return null;
        if(cp.length == 1)
            return cp[0];
        for(int i=0; i<cp.length;i++) {
            if(i!=0 && i != cp.length)
                categoryPath = categoryPath + delim;
            categoryPath = categoryPath + cp[i];
        }
        return categoryPath;

    }

    /**
    *  Returns a delimited string of category paths
    *  @param cps String contain the ids of category paths delimited
    *  @param delim  delimiter for the String .
    */
    public Object[] getCategoryPaths(String cps, String delim) {
        if(cps == null)
            return null;
        if(delim == null)
            return null;
        ArrayList categoryPath = new ArrayList();
        StringTokenizer st = new StringTokenizer(cps,delim);
        while(st.hasMoreTokens()) {
            categoryPath.add(st.nextToken());
        }
        return categoryPath.toArray();

    }



    public String diffBetweenDates(Date d1, Date d2) {
        String result = null;
        // if(d2.before(d1))
        //    return result;
        long temp = d2.getTime() - d1.getTime();
        double min = temp/(1000*60);
        double hr = min/60;

        //System.out.println(" hrs " + hr + " minu" + min);
        if(min >= 60) {
            if(hr >= 24) {
                result =  (int) hr/24 + "+ day(s) left";
                return result;
            } else {
                result = (int) hr + "+ hrs left";
                return result;
            }
        } else {
            result =  (int) min + "+ minutes left";
            return result;
        }

    }

    public static String xmlDocumentToString(Document doc)
    {
        StringBuffer sb = new StringBuffer();
        convertDOMTreeToString(doc,sb);
        return sb.toString();
    }

    /** Walks through the dom tree and converts into it the string sb
     *  Recursive call if there are any children
     * */


    private static void  convertDOMTreeToString(Node node,StringBuffer sb)
    {

        int type = node.getNodeType();
        switch (type)
        {
            // print the document element
        case Node.DOCUMENT_NODE:
            {
                sb.append(XMLHEADER);
                convertDOMTreeToString(((Document)node).getDocumentElement(),
                                       sb);
                break;
            }
            // print element with attributes
        case Node.ELEMENT_NODE:
            {
                sb.append("<");
                sb.append(node.getNodeName());
                NamedNodeMap attrs = node.getAttributes();
                for (int i = 0; i < attrs.getLength(); i++)
                {
                    Node attr = attrs.item(i);
                    sb.append(" "+ attr.getNodeName() +
                              "=\"" + attr.getNodeValue() +
                              "\"");
                }
                sb.append(">");
                NodeList children = node.getChildNodes();
                if (children != null)
                {
                    int len = children.getLength();
                    for (int i = 0; i < len; i++)
                        convertDOMTreeToString(children.item(i),sb);
                }
                break;
            }
            // handle entity reference nodes
        case Node.ENTITY_REFERENCE_NODE:
            {
                sb.append("&");
                sb.append(node.getNodeName());
                sb.append(";");
                break;
            }
            // print cdata sections
        case Node.CDATA_SECTION_NODE:
            {
                sb.append("<![CDATA[");
                sb.append(node.getNodeValue());
                sb.append("]]>");
                break;
            }
        case Node.TEXT_NODE:
            {
                sb.append(node.getNodeValue());
                break;
            }
            // print processing instruction
        case Node.PROCESSING_INSTRUCTION_NODE:
            {
                sb.append("<?");
                sb.append(node.getNodeName());
                String data = node.getNodeValue();
                {
                    sb.append("");
                    sb.append(data);
                }
                sb.append("?>");
                break;
            }
        }
        if (type == Node.ELEMENT_NODE)
        {
            sb.append(" ");
            sb.append("</");
            sb.append(node.getNodeName());
            sb.append('>');
        }
    }
    /**
     * returns the shop id from the category path
     * Category path is of the format 1SHOP/Second Son/SVF02
     */
    public String getShopFromCategoryPath(String catPath) {
        if(catPath == null)
            return null;
        StringTokenizer st = new StringTokenizer(catPath, "/");
        String shop = null;
        if(st.hasMoreTokens())
            shop = st.nextToken();

        return shop;
    }

    /** Converts the timestamp from the GMT timezone to the
     *  Timezone that the server is running
     */
    public final Timestamp covertTimeFromGmtToServerTz(Timestamp ts) {
        try {

            TimeZone _deftz = TimeZone.getDefault ();
            long _defOffset = _deftz.getRawOffset();
            long _time = ts.getTime();
            long _lDate = _time + _defOffset;
            java.sql.Timestamp _ts = new java.sql.Timestamp(_lDate);
            return _ts;

        }catch (Exception _ex){
            return null;
        }

    }

    /**
     * This gives to the current encoding format String given a HttpServletRequest object
     */
    public static String getEncoding(HttpServletRequest request)  {
        HttpSession session = request.getSession();
        if (session == null)
            return null;

        UserSessionData userData = UserSessionData.getUserSessionData(session);
        if (userData == null)
            return null;
        return getEncoding(userData.getSAPLanguage());
    }

    public static String getEncoding(String language)  {
        String sapCp = CodePageUtils.getSapCpForSapLanguage(language.toUpperCase());
        if (sapCp == null)
            return null;

        return CodePageUtils.getHtmlCharsetForSapCp(sapCp);

    }

    public String bigDecimalToUICurrencyString(HttpSession session ,
                                               BigDecimal amount)  {
        UserSessionData userSession = UserSessionData.getUserSessionData(session);

        AuctionBusinessObjectManagerBase bom = (AuctionBusinessObjectManagerBase)userSession.getBOM(AuctionBusinessObjectManagerBase.AUCTION_BOM);
        String retAmount = Conversion.bigDecimalToUICurrencyString(amount ,
                                                                   userSession.getLocale() , bom.getShop().getCurrency());

        return retAmount;

    }

    public BigDecimal stringToBigDecimal(HttpSession session , String number)  {
        UserSessionData userSession = UserSessionData.getUserSessionData(session);

		AuctionBusinessObjectManagerBase bom = (AuctionBusinessObjectManagerBase)userSession.getBOM(AuctionBusinessObjectManagerBase.AUCTION_BOM);
        return Conversion.uIQuantityStringToBigDecimal(number ,
                                                       userSession.getLocale());
    }

    /**
     * Used for padding of BP Ids before passing it to CRM
     * If it is a clear string then it converts the value to uppercase
     */
    public String convertBPtoCRMFormat(String bpid) {
        if(bpid == null)
            return null;
        else {
            try {
                int n = Integer.parseInt(bpid);
                // bpid is an integer, so pad the data
                int length = bpid.length();
                StringBuffer pid = new StringBuffer();
                for(int i=0; i<10-length; i++) {
                    pid.append("0");
                }
                pid.append(bpid);
                return pid.toString();
            }
            catch(NumberFormatException nfe)  {
                // bpid is not an integer
                return bpid.trim().toUpperCase();
            }
        }
    }


    /**
     * Cleans the BPID that comes from the CRM
     * for a better display format
     */
    public String convertBPCRMFormatToUIFormat(String bpid) {
        if(bpid == null)
            return null;
        else {
            try {
                int n = Integer.parseInt(bpid);
                // bpid is an integer, so trim the data
                return String.valueOf(n);
            }
            catch(NumberFormatException nfe)  {
                // bpid is not an integer
                return bpid.trim().toUpperCase();
            }
        }
    }


    public static void main(String args[]) {
        EAUtilities ea = EAUtilities.getInstance();
        String bp = ea.convertBPCRMFormatToUIFormat("  jack   ");
        String bp1 = ea.convertBPCRMFormatToUIFormat("0000006360");
        System.out.println("Bp " + bp + " " + bp1);
    }

}
