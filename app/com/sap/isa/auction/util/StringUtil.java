/*
 * Created on Oct 1, 2003
 *
 * To change the template for this generated file go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
package com.sap.isa.auction.util;

import java.text.SimpleDateFormat;
import java.util.Date;

import com.sap.mw.jco.JCO;

/**
 * @author I803746
 *
 * To change the template for this generated type comment go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
public class StringUtil {
	public static final int BAPI_MATERIAL_LENGTH = 18;
	public static final int TRANSACTION_NUMBER_LENGTH = 10;
	
	private static SimpleDateFormat crmDateFormatter = new SimpleDateFormat("dd.MM.yyyy HH:mm:ss");
	private static SimpleDateFormat emailDateFormatter = new SimpleDateFormat("yyyyMMdd");
	
	public static String ExtendString(String id, int destLength)
	{
		return ExtendString(id, destLength, '0');
	}
	
	public static String padLeadingZerosToSalesDocNumber(String id){
		if (id != null){
			return StringUtil.ExtendString(id, TRANSACTION_NUMBER_LENGTH); 
		}else
			return id;
		
	}
	public static String ExtendString(String id, int destLength, char filler)
	{
	  StringBuffer fullID = new StringBuffer();
	  int len = id.length();
	  if(len < destLength) {
		for(int i=0; i<(destLength-len); i++)
		  fullID.append(filler);
	  }
	  fullID.append(id);
	  return fullID.toString();
	}
	
	public static String ExtendNumber(String num, int desLength)
	{
		try
		{
			Integer.parseInt(num);
			return ExtendString(num, desLength);
		}
		catch(Exception ex)
		{
			return num;
		}
	}
	
	public static boolean ObjectCompare(Object obj1, Object obj2)
	{
		if(obj1 == null && obj2 == null)
			return true; 
		
		if(obj1 != null && obj2 != null)
			return obj1.equals(obj2); 
			
		return false;
	}
	
	public static void setExtendIdField(JCO.Field field, String id)
	{
		int fieldLength = field.getLength();
		String extendId = ExtendNumber(id, fieldLength); 
		field.setValue(extendId);
	}
	
	public static String RemoveZero(String id)
	{
		try
		{
			int num = Integer.parseInt(id);
			StringBuffer result = new StringBuffer();
			result.append(num); 
			return result.toString();
		}
		catch(Exception ex)
		{
			return id;
		}
	}
	public static String crmConvertDateToString(Date date) 
	{
		if(date == null)
			return null;
			
		try {
			return crmDateFormatter.format(date);
		}
		catch (Exception e) {
			return null;
		}
	}
	
	public static String crmConvertDateToEmailCompatibleString(Date date) 
	{
		if(date == null)
			return null;
		
		try {
			return emailDateFormatter.format(date);
		}
		catch (Exception e) {
			return null;
		}
	}
}
