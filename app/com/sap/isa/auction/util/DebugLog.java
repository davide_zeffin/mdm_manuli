/*****************************************************************************
    Copyright (c) 2001, SAPMarkets Palo Alto, USA, All rights reserved.

*****************************************************************************/
package com.sap.isa.auction.util;


public class DebugLog {
/**
 * DebugLog constructor comment.
 */
public DebugLog() {

}
/**
 * Insert the method's description here.
 * Creation date: (8/15/00 2:37:48 PM)
 * @param param java.lang.Exception
 */
public void print(Exception _excp) {
	try {
		System.out.println(_excp);
	}
	catch(Exception _e){
		DebugLog debuglog = new DebugLog();
		debuglog.print(_e);	
	}
	}
/**
 * Print the debug message
 * Creation date: (8/4/00 5:16:55 PM)
 * @param _message java.lang.String
 */
public void print(String _message) {
	try {
		System.out.println(_message);
	}
	catch(Exception _e){
		DebugLog debuglog = new DebugLog();
		debuglog.print(_e);

	}
	}
}
