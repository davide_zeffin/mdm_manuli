/*
 * Created on May 26, 2004
 *
 * Title:        Internet Sales Private Auctions
 * Description:
 * Copyright:    Copyright (c) 2004
 * Company:      SAP Labs LLC
 * @author
 * @version 1.0
 */
package com.sap.isa.auction.util;

import javax.jdo.PersistenceManager;
import javax.jdo.Transaction;

/**
 * @author I802850
 *
 * To change the template for this generated type comment go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
public class JDOUtil {
	
	public static void startTransaction(PersistenceManager pm){
		Transaction tx = pm.currentTransaction();
		if(tx.getOptimistic()){
			tx.setOptimistic(false);
		}
		tx.begin();
	}
}
