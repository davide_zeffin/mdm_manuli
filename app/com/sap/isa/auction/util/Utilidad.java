package com.sap.isa.auction.util;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.StringTokenizer;

import com.sap.isa.auction.helpers.LogSupport;
import com.sap.isa.catalog.webcatalog.WebCatItem;
import com.sap.isa.ui.controllers.ActionForward;


public class Utilidad extends LogSupport  {

    public static final String CONTAINERPATH = "/seller/container.jsp?"+ActionForward.LOADPAGE;
    public static final String DUMMYIMAGE    = "mimes/images/no_thumb.gif";

    public synchronized static String getProductImageURL(WebCatItem item)  {
        String imageURL = ""; // "mimes/images/no_thumb.gif";
        StringTokenizer tokens = new StringTokenizer("DOC_PC_CRM_THUMB,DOC_P_CRM_THUMB,DOC_P_BDS_IMAGE" , "," , false);
        Collection idList = new ArrayList();
        String url = "";
        while (tokens.hasMoreTokens()) {
            idList.add(new String(tokens.nextToken()));
        }
        Iterator iter = idList.iterator();
        while (iter.hasNext()){
            String st = (String) iter.next();
            String attr = item.getAttribute(st); 
            if (attr != null && !attr.equals("")) {
                if (attr.toLowerCase().startsWith("http") && attr.indexOf("://") != -1 ) {
                    imageURL = attr;
                } else {
                    String imgServer = item.getItemKey().getParentCatalog().getImageServer();
                    if(imgServer == null)  {
                        imgServer = "";
                    }
                    if(!(imgServer.endsWith("/")) && !attr.startsWith("/"))  {
                        imgServer = imgServer + "/";
                    }
                    imageURL = imgServer + attr;
                }
            }
        }

        return imageURL;
    }
}
