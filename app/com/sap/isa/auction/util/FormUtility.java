package com.sap.isa.auction.util;

import java.util.Locale;
import java.util.ResourceBundle;

import javax.servlet.http.HttpSession;

import org.apache.struts.action.Action;

import com.sap.isa.core.UserSessionData;
import com.sap.isa.core.logging.IsaLocation;

/**
 * Title:
 * Description:
 * Copyright:    Copyright (c) 2002
 * Company:
 * @author
 * @version 1.0
 */

public class FormUtility {
	
	public static final String AUCTIONBIDDERBUNDLE = "auctionbidder";
	
	/** 
	  *  The IsaLocation of the current Action.
	  *  This will be instantiated during perform and is always present.
	  */
	 protected static IsaLocation log = IsaLocation.getInstance(FormUtility.class.getName());
	
	public static String getResourceString( String bundleName,
                                          String key,
                                          HttpSession session)  {
        if (key == null)
          return null;

        String retval = null;
        ResourceBundle bundle = null;

        Locale lang = null;
        try  {
            if (session != null)  {
                try  {
                    UserSessionData userSession = UserSessionData.getUserSessionData(session);
                    if(null == userSession)  {
						if(session.getAttribute(Action.LOCALE_KEY) != null)  {
							lang = ((Locale)session.getAttribute(Action.LOCALE_KEY));
						}
                    }  else  {
						lang = userSession.getLocale();	
                    }
                }  catch(Exception exc)  {
					lang = Locale.getDefault();	
                }
            }
            if (lang == null)  {
                bundle = ResourceBundle.getBundle(bundleName);
            }  else  {
                bundle = ResourceBundle.getBundle(bundleName, lang);
            }
        }  catch(Exception ex)  {
			log.debug("Error ", ex);
        }

        retval = key;
        try  {
            retval = (bundle == null) ? key : bundle.getString(key);
        }
        catch(Exception ex)  {
			log.debug("Error ", ex);
        }
        return retval;
    }
}