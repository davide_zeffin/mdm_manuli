package com.sap.isa.auction.util;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Properties;

/**
 * Title:      PropertyContainer
 * Description: Serializable, Any object element added must be serializable as well
 *              Not thread safe
 *              Null values are not supported
 * Copyright:  Copyright (c) 2002
 * Company:    SAP
 * @author: e-auctions
 * @version 0.1
 */

public class PropertyContainer implements java.io.Serializable {

    private HashMap properties = new HashMap();
    private PropertyContainer parent = null;

    public PropertyContainer() {
    }

    /**
     * For heirarchichaly organizing properties
     * Properties are recursively checked in parent PropertyContainer if not foudn locally
     */
    public PropertyContainer(PropertyContainer parent) {
        this.parent = parent;
    }

    /**
     * @return Object property value,
     *         For primitive types their Object wrapper is returned
     */
    public Object getProperty(String name) {
        Object value = properties.get(name);
        if(value == null && parent != null) {
            value = parent.getProperty(name);
        }
        return value;
    }

    public String getPropertyAsString(String name) {
        String value = (String)properties.get(name);
        if(value == null && parent != null) {
            value = parent.getPropertyAsString(name);
        }
        return value;
    }

    public boolean getPropertyAsBoolean(String name) {
        Object value = properties.get(name);
        if(value == null && parent != null) {
            value = parent.getProperty(name);
        }
        return ((Boolean)value).booleanValue();
    }

    public int getPropertyAsInt(String name) {
        Object value = properties.get(name);
        if(value == null && parent != null) {
            value = parent.getProperty(name);
        }
        return ((Integer)value).intValue();
    }

    public long getPropertyAsLong(String name) {
        Object value = properties.get(name);
        if(value == null && parent != null) {
            value = parent.getProperty(name);
        }
        return ((Long)value).longValue();
    }

    public double getPropertyAsDouble(String name) {
        Object value = properties.get(name);
        if(value == null && parent != null) {
            value = parent.getProperty(name);
        }
        return ((Double)value).doubleValue();
    }

    /**
     * @return already associated property with specified name
     *          null, if there is no already associated property
     */
    public Object addProperty(String name, Object value) {
        Object oldValue = null;
        if(properties.containsKey(name)) {
            oldValue = properties.get(name);
        }
        properties.put(name,value);
        return oldValue;
    }

    public Object setPropertyAsString(String name, String value) {
        return addProperty(name,value);
    }

    public Object setPropertyAsBoolean(String name, boolean value) {
        return addProperty(name,new Boolean(value));
    }

    public Object setPropertyAsInt(String name, int value) {
        return addProperty(name,new Integer(value));
    }

    public Object setPropertyAsLong(String name, long value) {
        return addProperty(name,new Long(value));
    }

    public Object setPropertyAsDouble(String name, double value) {
        return addProperty(name,new Double(value));
    }

    public HashMap getProperties() {
        return properties;
    }

    /**
     * Converts to Properties representation for using features of
     * Properties. toString() is invoked on object's
     */
    public Properties toProperties() {
        Properties _props = new Properties();
        Iterator it = this.properties.keySet().iterator();
        while(it.hasNext()) {
            String name = (String)it.next();
            _props.setProperty(name,properties.get(name).toString());
        }
        return _props;
    }
}