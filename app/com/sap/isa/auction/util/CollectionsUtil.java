/*
 * Created on Sep 12, 2003
 *
 */
package com.sap.isa.auction.util;

import java.util.ArrayList;
import java.util.List;
import java.util.Collections;

/**
 * @author I802891
 *
 */
public class CollectionsUtil {

	public static List toList(Object[] arr) {
		if(arr == null) throw new NullPointerException("array is null");
		ArrayList list = new ArrayList();
		for(int i = 0; i < arr.length; i++) {
			list.add(arr[i]);
		}
		return list;
	}
	
	public static List toUnmodifiableList(Object[] arr) {
		return Collections.unmodifiableList(toList(arr));
	}
}
