package com.sap.isa.auction.util;

/*****************************************************************************
  Copyright (c) 2003, SAP Labs, All rights reserved.
  Author:       Pavan Bayyapu
  Created:      May 7, 2003

  Revision: #
  Date: 
*****************************************************************************/
public class DataTypeConverter {
	public static java.lang.String printBoolean(Boolean val) {
		return val.booleanValue() ? "1" : "0";
	}

	public static Boolean parseBoolean(java.lang.String val) {
		return new Boolean("1".equals(val));
	}

	public static synchronized int convertToInt(Integer integer){
		return ( (integer == null) ? 0 : integer.intValue() );
	}

	public static synchronized Integer convertToInteger(int intVal){
		return new Integer(intVal);
	}
	
	/**
	 * @param bool
	 * @return
	 */
	public static synchronized boolean convertToboolean(Boolean bool) {
		return ( (bool == null) ? false : bool.booleanValue() );
	}

	/**
	 * @param longVal
	 * @return
	 */
	public static long convertTolong(Long longVal) {
		return ( (longVal == null) ? 0 : longVal.longValue() );
	}
	
	public static short convertToShort(Integer intVal){
		return ( (intVal == null) ? 0 : intVal.shortValue() );
	}

	
}