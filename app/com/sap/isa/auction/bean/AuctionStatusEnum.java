package com.sap.isa.auction.bean;

import java.util.*;;

/**
 * Title:        Internet Sales
 * Description:
 * Copyright:    Copyright (c) 2002
 * Company:      SAPMarkets
 * @author
 * @version 1.0
 */

public class AuctionStatusEnum extends Enumeration {
	private static List AuctionStatusEnums = new ArrayList();

	/**
	 * These int values should not be modified as they are mapped to database
	 */
    private static final int INT_NONE        = 0;
    private static final int INT_DRAFT       = 1;
    private static final int INT_OPEN        = 2;
    private static final int INT_PUBLISHED   = 3;
    private static final int INT_CHECKEDOUT  = 4;
    private static final int INT_CLOSED      = 5;
    private static final int INT_FINALIZED   = 6;
    private static final int INT_WITHDRAWN   = 7;
	private static final int INT_SCHEDULED   = 8;    
	private static final int INT_FINISHED    = 9;
	private static final int INT_NOT_FINALIZED = 10;
	private static final int INT_ARCHIVABLE = 11;
	private static final int INT_PARTIAL_FINALIZED = 12;

    private static final String STR_NONE         = "NONE";
    private static final String STR_DRAFT        = "DRAFT";
    private static final String STR_OPEN         = "OPEN";
    private static final String STR_PUBLISHED    = "PUBLISHED";
    private static final String STR_CHECKEDOUT   = "CHECKEDOUT";
    private static final String STR_CLOSED       = "CLOSED";
    private static final String STR_FINALIZED    = "FINALIZED";
    private static final String STR_WITHDRAWN    = "WITHDRAWN";
	private static final String STR_SCHEDULED    = "SCHEDULED";
	private static final String STR_FINISHED     = "FINISHED";	
	private static final String STR_NOT_FINALIZED = "NOT_FINALIZED";
	private static final String STR_ARCHIVABLE	 = "ARCHIVABLE";
	private static final String STR_PARTIAL_FINALIZED = "PARTIAL_FINALIZED";		

    public static final AuctionStatusEnum NONE =
        new AuctionStatusEnum(INT_NONE,STR_NONE);

    public static final AuctionStatusEnum DRAFT =
        new AuctionStatusEnum(INT_DRAFT,STR_DRAFT);
    public static final AuctionStatusEnum OPEN =
        new AuctionStatusEnum(INT_OPEN,STR_OPEN);
    public static final AuctionStatusEnum PUBLISHED =
        new AuctionStatusEnum(INT_PUBLISHED,STR_PUBLISHED);
    public static final AuctionStatusEnum CHECKEDOUT =
        new AuctionStatusEnum(INT_CHECKEDOUT,STR_CHECKEDOUT);
    public static final AuctionStatusEnum CLOSED =
        new AuctionStatusEnum(INT_CLOSED,STR_CLOSED);
    public static final AuctionStatusEnum FINALIZED =
        new AuctionStatusEnum(INT_FINALIZED,STR_FINALIZED);
    public static final AuctionStatusEnum WITHDRAWN =
        new AuctionStatusEnum(INT_WITHDRAWN,STR_WITHDRAWN);
	public static final AuctionStatusEnum SCHEDULED =
		new AuctionStatusEnum(INT_SCHEDULED,STR_SCHEDULED);
	/**
	 * @deprecated no more finished status
	 * @see AuctionStatusEnum.PARTIAL_FINALIZED
	 * @see AuctionStatusEnum.FINALIZED
	 */
	public static final AuctionStatusEnum FINISHED =
		new AuctionStatusEnum(INT_FINISHED,STR_FINISHED);
	public static final AuctionStatusEnum NOT_FINALIZED =
		new AuctionStatusEnum(INT_NOT_FINALIZED,STR_NOT_FINALIZED);
	public static final AuctionStatusEnum ARCHIVABLE =
		new AuctionStatusEnum(INT_ARCHIVABLE,STR_ARCHIVABLE);

	public static final AuctionStatusEnum PARTIAL_FINALIZED =
		new AuctionStatusEnum(INT_PARTIAL_FINALIZED,STR_PARTIAL_FINALIZED);

	static {
		addEnum(AuctionStatusEnums, NONE);
		addEnum(AuctionStatusEnums, DRAFT);
		addEnum(AuctionStatusEnums, OPEN);
		addEnum(AuctionStatusEnums, PUBLISHED);
		addEnum(AuctionStatusEnums, CHECKEDOUT);
		addEnum(AuctionStatusEnums, CLOSED);
		addEnum(AuctionStatusEnums, FINALIZED);
		addEnum(AuctionStatusEnums, WITHDRAWN);
		addEnum(AuctionStatusEnums, SCHEDULED);
		addEnum(AuctionStatusEnums, FINISHED);
		addEnum(AuctionStatusEnums, NOT_FINALIZED);
		addEnum(AuctionStatusEnums, ARCHIVABLE);
		addEnum(AuctionStatusEnums, PARTIAL_FINALIZED);
	}
	
    private AuctionStatusEnum(int value, String name) {
        super(name, value);
    }

	public static AuctionStatusEnum[] enumerate() {
		return (AuctionStatusEnum[])enumerate(AuctionStatusEnums);
	}

	public static final AuctionStatusEnum toEnum(int value) {
		return (AuctionStatusEnum)toEnum(AuctionStatusEnums, value);
	}
	
	public static final int toInt(AuctionStatusEnum enum) {
		return enum.getValue();
	}
}