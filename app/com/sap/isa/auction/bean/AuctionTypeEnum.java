package com.sap.isa.auction.bean;

import java.io.Serializable;

/**
 * Title:        Internet Sales
 * Description:
 * Copyright:    Copyright (c) 2002
 * Company:      SAPMarkets
 * @author
 * @version 1.0
 */

public class AuctionTypeEnum implements Serializable {
    private static final String STR_NONE = "NONE";
    private static final String STR_NORMAL = "NORMAL";
    private static final String STR_DUTCH = "DUTCH";
	private static final String STR_FIXED_PRICE = "FixedPriceItem";
	private static final String STR_BROKEN_LOT = "BROKENLOT";


	/** DONT CHANGE THESE VALUES AS THEY HAVE EBAY SEMANTICS*/
    private static final int INT_NONE = 0;
    private static final int INT_NORMAL = 1;
    private static final int INT_DUTCH = 2;
	private static final int INT_FIXED_PRICE = 9;    
	private static final int INT_BROKEN_LOT = 11;

	public static final AuctionTypeEnum NONE = new AuctionTypeEnum(STR_NONE,INT_NONE);
    public static final AuctionTypeEnum NORMAL = new AuctionTypeEnum(STR_NORMAL,INT_NORMAL);
    public static final AuctionTypeEnum DUTCH = new AuctionTypeEnum(STR_DUTCH,INT_DUTCH);
	public static final AuctionTypeEnum FIXED_PRICE = new AuctionTypeEnum(STR_FIXED_PRICE,INT_FIXED_PRICE);    
	public static final AuctionTypeEnum BROKEN_LOT = new AuctionTypeEnum(STR_BROKEN_LOT,INT_BROKEN_LOT);

    private String name;
    private int value;

    private AuctionTypeEnum(String name, int value) {
        this.name = name;
        this.value = value;
    }

	public int getEBayValue() {
		return getValue();
	}
	
    public int getValue() {
        return value;
    }

	public String getName()
	{
		return name;
	}
    public String toString() {
        return "[" + name + " = " + value + "]";
    }

    public static final int toInt(AuctionTypeEnum enum) {
        return enum.getValue();
    }

    public static final AuctionTypeEnum toEnum(int value) {
        switch(value) {
            case INT_NORMAL: return NORMAL;
            case INT_DUTCH: return DUTCH;
            case INT_NONE: return NONE;
            case INT_FIXED_PRICE: return FIXED_PRICE;
			case INT_BROKEN_LOT: return BROKEN_LOT;
            default: throw new IllegalArgumentException("Enum = " + value + " is invalid");
        }
    }

}