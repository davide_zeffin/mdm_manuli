/*
 * Created on Sep 10, 2003
 *
 */
package com.sap.isa.auction.bean;

/**
 * @author I802891
 *
 */
public interface ChangeRecorderBean {

	boolean isModified(String attributeName);	
	String[] getModifiedAttributes();
	
	Object getNewAttributeValue(String attributeName);
	Object getOldAttributeValue(String attributeName);

	boolean isRecording();
	void startRecording();
	String[] endRecording();
	void revertRecording();
}
