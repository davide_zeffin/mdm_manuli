/*
 * Created on Jan 12, 2004
 *
 */
package com.sap.isa.auction.bean;

/**
 * @author I802891
 *
 */
public class ExecutionStatusEnum implements java.io.Serializable {
	private static final String STR_PENDING = "PENDING";
	private static final String STR_INPROCESS = "IN_PROCESS";
	private static final String STR_SUCCESS = "SUCCESS";
	private static final String STR_USER_ERROR = "USER_ERROR";
	private static final String STR_SYSTEM_ERROR = "SYSTEM_ERROR";
	private static final String STR_TIMEOUT = "TIMEOUT";

	private static final int INT_PENDING = 0;
	private static final int INT_INPROCESS = 1;
	private static final int INT_SUCCESS = 2;
	private static final int INT_USER_ERROR = 3;
	private static final int INT_SYSTEM_ERROR = 4;
	private static final int INT_TIMEOUT = 5;
	
	public static final ExecutionStatusEnum PENDING = new ExecutionStatusEnum(STR_PENDING,INT_PENDING);
	public static final ExecutionStatusEnum INPROCESS = new ExecutionStatusEnum(STR_INPROCESS,INT_INPROCESS);
	public static final ExecutionStatusEnum SUCCESS = new ExecutionStatusEnum(STR_SUCCESS,INT_SUCCESS);
	public static final ExecutionStatusEnum USER_ERROR = new ExecutionStatusEnum(STR_USER_ERROR,INT_USER_ERROR);
	public static final ExecutionStatusEnum SYSTEM_ERROR = new ExecutionStatusEnum(STR_SYSTEM_ERROR,INT_SYSTEM_ERROR);
	public static final ExecutionStatusEnum TIMEOUT = new ExecutionStatusEnum(STR_TIMEOUT,INT_TIMEOUT);	

	private String name;
	private int value;

	private ExecutionStatusEnum(String name, int value) {
		this.name = name;
		this.value = value;
	}

	public int getValue() {
		return value;
	}

	public String toString() {
		return "[" + name + " = " + value + "]";
	}

	public static final int toInt(OrderStatusEnum enum) {
		return enum.getValue();
	}

	public static final ExecutionStatusEnum toEnum(int value) {
		switch(value) {
			case INT_PENDING: return PENDING;
			case INT_INPROCESS: return INPROCESS;
			case INT_SUCCESS: return SUCCESS;
			case INT_USER_ERROR: return USER_ERROR;
			case INT_SYSTEM_ERROR: return SYSTEM_ERROR;									
			case INT_TIMEOUT: return TIMEOUT;			
			default: throw new IllegalArgumentException("Enum = " + value + " is invalid");
		}
	}
}
