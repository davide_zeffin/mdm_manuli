package com.sap.isa.auction.bean;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;

/**
 * Title:        Internet Sales
 * Description:
 * Copyright:    Copyright (c) 2002
 * Company:      SAPMarkets
 * @author
 * @version 1.0
 */

public abstract class ValidatingBean {

    private ArrayList errors = null;
    private ArrayList warnings = null;
    private ArrayList invalidChildren = null;

    protected ValidatingBean() {
    }

    /**
     * Each validating bean implements this method with its custom validation
     * logic.
     *
     * @return boolean true , if validation succeeds,
     *                  false, it validation fails, Error list is populated with
     *                  set of errors
     */
    public boolean validate() {
    	return validate(false);
    }
    
    /**
     * <p>
     * if a bean has some child beans, then those can be validated in one call
     * using deep as true
     * </p>
     * @param deep
     * @return
     */
	public abstract boolean validate(boolean deep);
	
	public ValidatingBean[] getInvalidChildren() {
		if(invalidChildren == null) invalidChildren = new ArrayList();
		ValidatingBean[] result = new ValidatingBean[invalidChildren.size()];
		invalidChildren.toArray(result);
		return result;    
	}

    /**
     * @return ArrayList list of errors resulting from last invocation of validate
     */
    public Collection getErrors() {
        if(errors == null) errors = new ArrayList();
        return Collections.unmodifiableCollection(errors);
    }

    public Collection getWarnings() {
        if(warnings == null) warnings = new ArrayList();
        return Collections.unmodifiableCollection(warnings);
    }

    /**
     * clears the list of errors, if any
     */
    public void clearErrors() {
        if(errors != null) errors.clear();
    }

	public void clearWarnings() {
		if(warnings != null) warnings.clear();
	}
	
	public void clearInvalidChildren() {
		invalidChildren = null;
	}
	
    /**
     * Adds an error to the existing list of errors
     * In very simple form it can be a string message
     */
    public void addError(Object error) {
        if(errors == null) errors = new ArrayList();
        errors.add(error);
    }

    public void addWarning(Object error) {
        if(errors == null) errors = new ArrayList();
        errors.add(error);
    }
    
    public void addInvalidChild(ValidatingBean invalid) {
    	if(invalidChildren == null) invalidChildren = new ArrayList();
    	invalidChildren.add(invalid);
    }
}