package com.sap.isa.auction.bean;

import java.io.Serializable;

/**
 * Title:        Internet Sales
 * Description:  Web Auctions
 * Copyright:    Copyright (c) 2002, All rights reserved
 * Company:      SAP Labs, PA
 * @author e-auctions
 * @version 0.1
 */

public class OrderStatusEnum implements Serializable {

    private static final String STR_CHECKED = "CHECKED";
    private static final String STR_UNCHECKED = "UNCHECKED";

    private static final int INT_CHECKED = 0x1;
    private static final int INT_UNCHECKED = 0x2;

    public static final OrderStatusEnum CHECKED = new OrderStatusEnum(STR_CHECKED,INT_CHECKED);
    public static final OrderStatusEnum UNCHECKED = new OrderStatusEnum(STR_UNCHECKED,INT_UNCHECKED);

    private String name;
    private int value;

    private OrderStatusEnum(String name, int value) {
        this.name = name;
        this.value = value;
    }

    public int getValue() {
        return value;
    }

    public String toString() {
        return "[" + name + " = " + value + "]";
    }

    public static final int toInt(OrderStatusEnum enum) {
        return enum.getValue();
    }

    public static final OrderStatusEnum toEnum(int value) {
        switch(value) {
            case INT_CHECKED: return CHECKED;
            case INT_UNCHECKED: return UNCHECKED;
            default: throw new IllegalArgumentException("Enum = " + value + " is invalid");
        }
    }
}