package com.sap.isa.auction.bean;

import java.io.ByteArrayInputStream;
import java.io.Serializable;

import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import com.sap.isa.core.logging.IsaLocation;

/**
 * Title:
 * Description:
 * Copyright:    Copyright (c) 2003
 * Company:
 * @author
 * @version 1.0
 */

public class Address implements Cloneable, Serializable {

    private String name;
    private String street1;
    private String street2;
    private String city;
    private String phone;

    private String countryCode;
    private String stateorProvinceCode;
    private String zipCode;
	private String email;

	/** 
	  *  The IsaLocation of the current Action.
	  *  This will be instantiated during perform and is always present.
	  */
	 protected static IsaLocation log = IsaLocation.getInstance(Address.class.getName());

    public Address() {
    }

    public java.lang.String getZip() {
        return this.zipCode;
    }

    public void setZip(java.lang.String value) {
        this.zipCode = value;
    }

    public java.lang.String getStateOrProvince() {
        return this.stateorProvinceCode;
    }

    public void setStateOrProvince(java.lang.String value) {
        this.stateorProvinceCode = value;
    }

    public java.lang.String getPhone() {
        return this.phone;
    }

    public void setPhone(java.lang.String value) {
        this.phone = value;
    }

    public java.lang.String getName() {
        return this.name;
    }

    public void setName(java.lang.String value) {
        this.name = value;
    }

    public java.lang.String getStreet2() {
        return this.street2;
    }

    public void setStreet2(java.lang.String value) {
        this.street2 = value;
    }

    public java.lang.String getStreet1() {
        return this.street1;
    }

    public void setStreet1(java.lang.String value) {
        this.street1 = value;
    }

    public java.lang.String getCity() {
        return this.city;
    }

    public void setCity(java.lang.String value) {
        this.city = value;
    }

    public java.lang.String getCountry() {
        return this.countryCode;
    }

    public void setCountry(java.lang.String value) {
        this.countryCode = value;
    }

	public String toString() {
		StringBuffer buffer = new StringBuffer();
		if(street1 != null) buffer.append(street1);
		if(buffer.length() != 0) buffer.append(' ');
		if(street2 != null) buffer.append(street2);
		if(buffer.length() != 0) buffer.append(',');
		if(city != null) {
			buffer.append(city);
			buffer.append(',');
		}
		if(stateorProvinceCode != null) {
			buffer.append(stateorProvinceCode);
			buffer.append(' ');
		}
		if(zipCode != null) {
			buffer.append(zipCode);
			buffer.append(',');
		}

		if(countryCode != null) buffer.append(countryCode);

		return buffer.toString();
	}
	
	public String printAddress() {
		StringBuffer buffer = new StringBuffer();
		buffer.append("Name: ");
		buffer.append((name==null)?"":name);
		buffer.append("\n"); 
		buffer.append("Street: ");       
		if(street1 != null) 
			buffer.append(street1);
		else
			buffer.append("");       
		if(buffer.length() != 0) buffer.append(' ');
		if(street2 != null) buffer.append(street2);
		if(buffer.length() != 0) buffer.append('.');
		buffer.append("\n"); 
		buffer.append("City: ");   
		if(city != null) {
			buffer.append(city);
			buffer.append(',');
		}
		if(stateorProvinceCode != null) {
			buffer.append(stateorProvinceCode);
			buffer.append(' ');
		}
		if(zipCode != null) {
			buffer.append(zipCode);
			buffer.append(',');
		}

		if(countryCode != null) buffer.append(countryCode);

		return buffer.toString();
	}

    public Object clone() {
        try {
            return super.clone();
        }
        catch(CloneNotSupportedException ex) {
			log.debug("Error ", ex);
            return null;
        }
    }
/*
	<RegistrationAddress>
	  <City><![CDATA[ value ]]></City>
	  <Country><![CDATA[ value ]]></Country>
	  <Name><![CDATA[ value ]]></Name>
	  <Phone><![CDATA[ value ]]></Phone>
	  <StateorProvince><![CDATA[ value ]]></StateorProvince>
	  <Street><![CDATA[ value ]]></Street>
	  <Zip><![CDATA[ value ]]></Zip>
	</RegistrationAddress>
*/    
   public String toXMLString(){
   		StringBuffer returnStr = new StringBuffer("<Address>");
   		if(getCity()!=null)
			returnStr.append("<City Value=\"").append(getCity()).append("\"/>");
		if(getCountry()!=null)
			returnStr.append("<Country Value=\"").append(getCountry()).append("\"/>");
		if(getName()!=null)
			returnStr.append("<Name Value=\"").append( getName()).append( "\"/>");
		if(getStateOrProvince()!=null)
			returnStr.append("<StateorProvince Value=\"").append(getStateOrProvince()).append("\"/>");
		if(getStreet1()!=null)
			returnStr.append("<Street Value=\"").append( getStreet1() ).append( "\"/>");
		if(getZip()!=null)
			returnStr.append("<Zip Value=\"" ).append( getZip() ).append( "\"/>");
   		returnStr.append("</Address>");
		return returnStr.toString();
   }
   
   /**
		* This method is for internal purposes only
		* this method initializes the datamap from the database
		*/
   public void setXML(String xmlStr) {
	   if (xmlStr == null)
		   return;
	   SAXParserFactory factory = SAXParserFactory.newInstance();
	   try {
		   // Parse the input
		   SAXParser saxParser = factory.newSAXParser();
		   ByteArrayInputStream byteStream =
			   new ByteArrayInputStream(xmlStr.getBytes());
		   saxParser.parse(byteStream, new AddressXMLParser());
	   } catch (Exception neglect) {
		log.debug("Error ", neglect);
	   }
   }
   class AddressXMLParser extends  DefaultHandler{
		  	public void startElement(
			   String namespaceURI,
			   String sName,
			   // simple name (localName)
			   String qName, // qualified name
				Attributes attrs) throws SAXException {

		   String eName = sName; // element name
		   if ("".equals(eName))
			   eName = qName; // namespaceAware = false            
		   if (!eName.equals("City") && 
				!eName.equals("Country") &&
				!eName.equals("Name") &&
				!eName.equals("StateorProvince") &&
				!eName.equals("Street") &&
				!eName.equals("Zip")
		   )
			return;
		   String value = null;
		   if (attrs != null) {
			   for (int i = 0; i < attrs.getLength(); i++) {
				   String aName = attrs.getLocalName(i); // Attr name
					if (aName.equals("Value")) {
					   value = attrs.getValue(i);
				   } 

			   }
		   }
		   
		   if (eName != null && value != null) {
				if(eName.equals("City"))
					setCity(value);		   	
			else if(eName.equals("Country"))
				setCountry(value);		   	
			else if(eName.equals("Name"))
				setName(value);		   	
			else if(eName.equals("StateorProvince"))
				setStateOrProvince(value);		   	
			else if(eName.equals("Street"))
				setStreet1(value);		   	
			else if(eName.equals("Zip"))
				setZip(value);		   	
		   }

	   }

   }
   /**
	* @return
	*/
   public String getEmail() {
	   return email;
   }
   
   /**
	* @param string
	*/
   public void setEmail(String string) {
	   email = string;
}
   
}