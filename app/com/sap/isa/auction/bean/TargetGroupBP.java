/*****************************************************************************
    Copyright (c) 2001, SAPMarkets Palo Alto, USA, All rights reserved.

*****************************************************************************/

package com.sap.isa.auction.bean;

import com.sap.isa.auction.AuctionTargetGroupBP;
import com.sap.isa.core.logging.IsaLocation;

/**
 * Data bean for Target Group or business Partners in an auction
 */


public class TargetGroupBP implements AuctionTargetGroupBP {

    private static IsaLocation log = IsaLocation.getInstance(TargetGroupBP.class.getName());

    public String guid;
    public String Type; // BP or TG

    public TargetGroupBP() {
    }
    public String getGuid() {
        return guid;
    }
    public String getType() {
        return Type;
    }
    public void setGuid(String guid) {
        this.guid = guid;
    }
    public void setType(String Type) {
        this.Type = Type;
    }

} // End of Class
