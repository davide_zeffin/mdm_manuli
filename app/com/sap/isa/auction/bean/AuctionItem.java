package com.sap.isa.auction.bean;

import com.sap.isa.auction.backend.boi.AuctionListingData;
import com.sap.isa.auction.exception.i18n.*;
import com.sap.isa.auction.businessobject.ValidationException;

import java.io.Serializable;

/**
 * Title:
 * Description:
 * Copyright:    Copyright (c) 2003
 * Company:
 * @author
 * @version 1.0
 */

public class AuctionItem extends ValidatingBean implements ChangeRecorderBean, AuctionListingData, Cloneable, Serializable {

	public static final String ATTR_PRODUCTID = "PRODUCTID";
	public static final String ATTR_PRODUCTCODE = "PRODUCTCODE";
	public static final String ATTR_QUANTITY = "QUANTITY";
	public static final String ATTR_UOM = "UOM";	
	public static final String ATTR_FROMCATALOG = "FROM_CATALOG";	
	public static final String ATTR_DESCRIPTION = "DESCRIPTION";	
	public static final String ATTR_PLANT = "PLANT";
	public static final String ATTR_STORAGE = "STORAGE";
	public static final String ATTR_IMAGE = "IMAGE";

    private Auction auction;
    private String productId;
    private String productCode;
    private double quantity = 0.0;
    private String description;
    private String uom;
	
	private String image;
	private String plant;
	private String storage;
    //Only for uniquenss in the database
    private String id;
    private double price;

	private boolean isFromCatalog;
	
    public AuctionItem() {
    }

    public Auction getAuction() {
        return auction;
    }

	/**
	 * Non modifiable property
	 * @param auction
	 */
    void setAuction(Auction auction) {
        this.auction = auction;
    }

    public String getProductId() {
        return this.productId;
    }

	/**
	 * 
	 * @param id
	 */
    public void setProductId(String id) {
    	if(isRecording()) {
    		setAttributeValue(ATTR_PRODUCTID,productId,id);
    	}
        this.productId = id;
    }

    public String getProductCode() {
        return this.productCode;
    }

    public void setProductCode(String code) {
    	if(isRecording()) {
			setAttributeValue(ATTR_PRODUCTCODE,productCode==null?null:productCode.toUpperCase(),code);
    	}
        this.productCode = code;
    }

    public double getQuantity() {
        return quantity;
    }

    public void setQuantity(double quantity) {
		if(isRecording()) {
			setAttributeValue(ATTR_QUANTITY,new Double(this.quantity),new Double(quantity));    		
		}
        this.quantity = quantity;
    }
    /**
     * Only for the db uniqueness.No use for the UI purposes
     */
    public String getId() {
        return this.id;
    }

	/**
	 * Privileged operation
	 * @param id
	 */
    public void setId(String id) {
        this.id = id;
    }

    public Object clone() {
        try {
            return super.clone();
        }
        catch(CloneNotSupportedException ex) {
            // should not occur
            return null;
        }
    }

    public boolean validate(boolean deep) {
        super.clearErrors();

		ErrorMessageBundle bundle = ErrorMessageBundle.getInstance();
		
        if(productCode == null) {
            I18nErrorMessage error = bundle.newI18nErrorMessage(ValidationException.INVALID_PRODUCT_CODE);
            super.addError(error);
        }

//        if(productId == null) {
//            I18nErrorMessage error = bundle.newI18nErrorMessage(ValidationException.INVALID_PRODUCTID);
//            super.addError(error);
//        }

        if(quantity <= 0.0) {
            I18nErrorMessage error = bundle.newI18nErrorMessage(ValidationException.INVALID_QUANTITY);
            super.addError(error);
        }
		if(uom == null) {
			I18nErrorMessage error = bundle.newI18nErrorMessage(ValidationException.INVALID_UOM);
			super.addError(error);
		}        

        if(super.getErrors().size() > 0) return false;
        else return true;
    }
    /**
     * @return
     */
    public String getDescription() {
        return description;
    }

    /**
     * @param string
     */
    public void setDescription(String string) {
		if(isRecording()) {
			setAttributeValue(ATTR_DESCRIPTION,description,string);    		
		}        	
        description = string;
    }

    /**
     * @return
     */
    public String getUOM() {
        return uom;
    }

    /**
     * @param string
     */
    public void setUOM(String string) {
		if(isRecording()) {
			setAttributeValue(ATTR_UOM,uom,string);    		
		}    	
        uom = string;
    }

	/**
	 * If not from catalog, then from manually entry
	 * @return boolean indicates this from product catalog
	 */
	public boolean isFromCatalog() {
		return isFromCatalog;
	}

	/**
	 * @param boolean, indicate if the item from product catalog
	 */
	public void setFromCatalog(boolean b) {
		if(isRecording()) {
			setAttributeValue(ATTR_FROMCATALOG,new Boolean(isFromCatalog),new Boolean(b));    		
		}    		
		isFromCatalog = b;
	}
	
    //////////////////////////
    // change recording specific
    
    ChangeRecorder recorder = new ChangeRecorder();
    
	/* (non-Javadoc)
	 * @see com.sap.isa.auction.bean.ChangeRecorderBean#endRecording()
	 */
	public String[] endRecording() {
		return recorder.endRecording();
	}

	/* (non-Javadoc)
	 * @see com.sap.isa.auction.bean.ChangeRecorderBean#getModifiedAttributes()
	 */
	public String[] getModifiedAttributes() {
		return recorder.getModifiedAttributes();
	}

	/* (non-Javadoc)
	 * @see com.sap.isa.auction.bean.ChangeRecorderBean#getNewAttributeValue(java.lang.String)
	 */
	public Object getNewAttributeValue(String attributeName) {
		return getNewAttributeValue(attributeName);
	}

	/* (non-Javadoc)
	 * @see com.sap.isa.auction.bean.ChangeRecorderBean#getOldAttributeValue(java.lang.String)
	 */
	public Object getOldAttributeValue(String attributeName) {
		return recorder.getOldAttributeValue(attributeName);
	}

	/* (non-Javadoc)
	 * @see com.sap.isa.auction.bean.ChangeRecorderBean#isModified(java.lang.String)
	 */
	public boolean isModified(String attributeName) {
		return recorder.isModified(attributeName);
	}

	/* (non-Javadoc)
	 * @see com.sap.isa.auction.bean.ChangeRecorderBean#isRecording()
	 */
	public boolean isRecording() {
		return recorder.isRecording();
	}

	/* (non-Javadoc)
	 * @see com.sap.isa.auction.bean.ChangeRecorderBean#setAttributeValue(java.lang.String, java.lang.Object, java.lang.Object)
	 */
	public void setAttributeValue(
		String attributeName,
		Object oldValue,
		Object newValue) {
		recorder.setAttributeValue(attributeName,oldValue,newValue);
	}

	/* (non-Javadoc)
	 * @see com.sap.isa.auction.bean.ChangeRecorderBean#startRecording()
	 */
	public void startRecording() {
		recorder.startRecording();
	}

	/* (non-Javadoc)
	 * @see com.sap.isa.auction.bean.ChangeRecorderBean#revertRecording()
	 */
	public void revertRecording() {
		if(recorder.isRecording()) {
			String[] attrs = recorder.getModifiedAttributes();
			for(int i = 0; i < attrs.length; i++) {
				String attr = attrs[i];
				setAttribute(attr,recorder.getOldAttributeValue(attr));
			}		
		}
	}
	
	private void setAttribute(String attr, Object value) {
		if(attr.equals(ATTR_DESCRIPTION)) {
			setDescription((String)value);
		}
		else if(attr.equals(ATTR_FROMCATALOG)) {
			setFromCatalog(((Boolean)value).booleanValue());
		}
		else if(attr.equals(ATTR_PRODUCTCODE)) {
			setProductCode((String)value);
		}
		else if(attr.equals(ATTR_PRODUCTID)) {
			setProductId((String)value);
		}
		else if(attr.equals(ATTR_QUANTITY)) {
			setQuantity(((Double)value).doubleValue());
		}
		else if(attr.equals(ATTR_UOM)) {
			setUOM((String)value);
		}
		else if(attr.equals(ATTR_PLANT)) {
			setPlant((String)value);
		}
		else if(attr.equals(ATTR_IMAGE)) {
			setImage((String)value);
		}
		else if(attr.equals(ATTR_STORAGE)) {
			setStorage((String)value);
		}
	}
    /**
     * @return
     */
    public String getImage() {
        return image;
    }

    /**
     * @return
     */
    public String getPlant() {
        return plant;
    }

    /**
     * @return
     */
    public String getStorage() {
        return storage;
    }

    /**
     * @param string
     */
    public void setImage(String string) {
		if(isRecording()) {
			setAttributeValue(ATTR_IMAGE,storage,string);    		
		}
        image = string;
    }

    /**
     * @param string
     */
    public void setPlant(String string) {
		if(isRecording()) {
			setAttributeValue(ATTR_PLANT,plant,string);    		
		}
        plant = string;
    }

    /**
     * @param string
     */
    public void setStorage(String string) {
		if(isRecording()) {
			setAttributeValue(ATTR_STORAGE,storage,string);    		
		}
        storage = string;
    }

	/**
	 * @return
	 */
	public double getPrice() {
		return price;
	}

	/**
	 * @param d
	 */
	public void setPrice(double d) {
		price = d;
	}

}