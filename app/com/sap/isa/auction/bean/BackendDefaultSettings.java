/**
 * Title:        Asset Remarketing
 * description:  
 * Copyright:    Copyright (c) 2003
 * Company:      SAPLabs LLC
 * @author 
 * @version 1.0
 */
package com.sap.isa.auction.bean;

import com.sap.isa.auction.backend.boi.BackendSettingsData;
import com.sap.isa.auction.backend.boi.SalesAreaData;
import com.sap.isa.auction.businessobject.ValidationException;
import com.sap.isa.auction.exception.i18n.ErrorMessageBundle;

public class BackendDefaultSettings
	extends ValidatingBean
	implements Cloneable, BackendSettingsData {
	private String id;
	private String catalogId;
	private String catalogVariant;
	private String catalogDesc;
	private String plant;
	private String storage;
	private String orderType;
	private SalesAreaData salesArea;
	private SalesAreaData refBPSalesArea;
	private String rejectReason;
	private boolean deliveryBlock = true; //default has delivery block
	private boolean autoReleaseBlock = true;
	private String refBP;
	private String payPalOrderType;
	private String quotationType;
	private String cashOnDeliveryBP;
	
	private String deliveryBlockCode;
	private String incoterm;
	private String insuranceCondition;	//applicable if buyer pays shipping
	private String shipPriceCondition; //applicable if buyer pays shipping
    private String rebatePriceCondition; //applicable if buyer pays shipping
	private String taxPriceCondiction; //applicable if buyer pays shipping
	private String otherPriceCondition;
	private String totalPriceCondition;
	private String billingBlockCode;
	
	//field for validation purpose. No need to save into db
	private int sapSysType = SAPBackendSystemEnum.R3.getValue(); //default R3 backend

	/**
	 * On time customer ID, used across through the 
	 * auction process, used as SoldTo in the order process
	 */
	private String oneTimeCustomerId;

	/**
	 * Indicate is the one time customer scenario
	 *
	 */
	private boolean isOneTimeCustomerScenario;

	/**
	 * Used in B2C checkout scenario, all of the backend
	 * attributes are from WebShop info, only a few can be defined 
	 * here
	 */
	private String webShopId;
	
	/**
	 * Used in B2C checkout scenario, if it is true, all of the 
	 * information comes from WebShop, only followinhg attributes
	 * are from Admin side
	 *  Auction Quotation Type
	 *	Auction Order Type
	 *	Manual price procedure for winning price
	 *	Manual price procedure for shipping price 
	 */
	private boolean isIndependentCheckout = false;
	/**
	 * 
	 */
	public BackendDefaultSettings() {
	}
	
	/* (non-Javadoc)
	 * @see com.sap.isa.auction.bean.ValidatingBean#validate()
	 */
	public boolean validate(int sapSysType) {
		this.sapSysType = sapSysType;
		return validate(true);
	}
	/* (non-Javadoc)
	 * @see com.sap.isa.auction.bean.ValidatingBean#validate()
	 */
	public boolean validate(boolean deep) {
		super.clearErrors();

		ErrorMessageBundle bundle = ErrorMessageBundle.getInstance();
		
		if (!isIndependentCheckout() && sapSysType == SAPBackendSystemEnum.R3.getValue()){
			//check R3 Specific manadatory fields 
			
			//Either reference BP or COD BP has to have value
			  if (isOneTimeCustomerScenario) {
				  if (cashOnDeliveryBP == null || cashOnDeliveryBP.length() == 0) {
					  super.addError(
						  bundle.newI18nErrorMessage(ValidationException.INVALID_COD_BP));
				  }
			  } else {
				  if (refBP == null || refBP.length() == 0){
					  super.addError(
						  bundle.newI18nErrorMessage(ValidationException.INVALID_REFERENCE_BP));
				  }
			  }
		}
		if (isIndependentCheckout() && sapSysType == SAPBackendSystemEnum.R3.getValue()) {
			if (cashOnDeliveryBP == null || cashOnDeliveryBP.length() == 0) {
			  super.addError(
				  bundle.newI18nErrorMessage(ValidationException.INVALID_COD_BP));
		  	}
		}
		if(isIndependentCheckout() && (webShopId == null || webShopId.length() == 0 || webShopId.equals("NONE")))  {
			super.addError(
				bundle.newI18nErrorMessage(ValidationException.MISSING_SHOP_PROPERTY));
		}
		//Order Type can't be null or empty
		if (orderType == null || orderType.length() == 0) 
		{
			super.addError(
				bundle.newI18nErrorMessage(ValidationException.REQUIRED_ORDERTYPE_MISSING));
		}
		//Quotation Type can't be null or empty
		if (quotationType == null || quotationType.length() == 0) {
			super.addError(
				bundle.newI18nErrorMessage(ValidationException.INVALID_QUOTATION_TYPE));
		}

		//total price condiction must be provided
		if (totalPriceCondition == null || totalPriceCondition.length() == 0) {
			super.addError(
				bundle.newI18nErrorMessage(ValidationException.INVALID_TOTAL_PRICE_CONDITION));			
		}
		//Reason for cancallation must be provided
		 if (rejectReason == null || rejectReason.length() == 0) {
			 super.addError(
				 bundle.newI18nErrorMessage(ValidationException.INVALID_REJECTION_REASON));
		 }
		if(!isIndependentCheckout())  {
			
			//delivery block code must be provided if has delivery block
			if (isDeliveryBlock() && (deliveryBlockCode == null || deliveryBlockCode.length()== 0)){
				super.addError(
					bundle.newI18nErrorMessage(ValidationException.INVALID_DELIVERY_BLOCK));			
			}
		}

		if (super.getErrors().size() > 0) {
			return false;
		} else {
			return true;
		}
	}



	/**
	 * A method for easy printing
	 */
	public String toString() {
		StringBuffer ret = new StringBuffer("The backend setting is");
		ret.append("------------------------\n");
		ret.append("One Time Customer is ");
		ret.append(getOneTimeCustomerId());
		ret.append("\n");
		ret.append("Order Type is ");
		ret.append(getOrderType());
		ret.append("\n");
		ret.append("payPal Order Type is ");
		ret.append(getPayPalOrderType());
		ret.append("\n");
		ret.append("Quotation Type is ");
		ret.append(getQuotationType());
		ret.append("\n");
		ret.append("The refBP is ");
		ret.append(getRefBP());
		ret.append("\n");
		ret.append("The Rejection Reason is ");
		ret.append(getRejectReason());
		ret.append("\n");
		ret.append("The sales is ");
		ret.append(getSalesArea());
		ret.append("\n");
		ret.append("The cash on delivery BP is ");
		ret.append(getCashOnDeliveryBP());
		ret.append("\n");
		ret.append("------------------------\n");
		return ret.toString();

	}

	/**
	 * @return
	 */
	public boolean isAutoReleaseBlock() {
		return autoReleaseBlock;
	}

	/**
	 * @return
	 */
	public String getBillingBlockCode() {
		return billingBlockCode;
	}

	/**
	 * @return
	 */
	public String getCashOnDeliveryBP() {
		return cashOnDeliveryBP;
	}

	/**
	 * @return
	 */
	public String getCatalogDesc() {
		return catalogDesc;
	}

	/**
	 * @return
	 */
	public String getCatalogId() {
		return catalogId;
	}

	/**
	 * @return
	 */
	public String getCatalogVariant() {
		return catalogVariant;
	}

	/**
	 * @return
	 */
	public boolean isDeliveryBlock() {
		return deliveryBlock;
	}

	/**
	 * @return
	 */
	public String getDeliveryBlockCode() {
		return deliveryBlockCode;
	}

	/**
	 * @return
	 */
	public String getId() {
		return id;
	}

	/**
	 * @return
	 */
	public String getIncoterm() {
		return incoterm;
	}

	/**
	 * @return
	 */
	public String getInsuranceCondition() {
		return insuranceCondition;
	}

	/**
	 * @return
	 */
	public boolean isOneTimeCustomerScenario() {
		return isOneTimeCustomerScenario;
	}

	/**
	 * @return
	 */
	public String getOneTimeCustomerId() {
		return oneTimeCustomerId;
	}

	/**
	 * @return
	 */
	public String getOrderType() {
		return orderType;
	}

	/**
	 * @return
	 */
	public String getPayPalOrderType() {
		return payPalOrderType;
	}

	/**
	 * @return
	 */
	public String getPlant() {
		return plant;
	}

	/**
	 * @return
	 */
	public String getQuotationType() {
		return quotationType;
	}

	/**
	 * @return
	 */
	public String getRefBP() {
		return refBP;
	}

	/**
	 * @return
	 */
	public String getRejectReason() {
		return rejectReason;
	}

	/**
	 * @return
	 */
	public SalesAreaData getSalesArea() {
		return salesArea;
	}
	/**
	 * @return
	 */
	public SalesAreaData getRefBPSalesArea() {
		return refBPSalesArea;
	}
	/**
	 * @return
	 */
	public String getShipPriceCondition() {
		return shipPriceCondition;
	}
    
    /**
     * @return
     */
    public String getRebatePriceCondition() {
        return rebatePriceCondition;
    }

	/**
	 * @return
	 */
	public String getStorage() {
		return storage;
	}

	/**
	 * @return
	 */
	public String getTaxPriceCondiction() {
		return taxPriceCondiction;
	}

	/**
	 * @return
	 */
	public String getTotalPriceCondiction() {
		return totalPriceCondition;
	}

	/**
	 * @param b
	 */
	public void setAutoReleaseBlock(boolean b) {
		autoReleaseBlock = b;
	}

	/**
	 * @param string
	 */
	public void setBillingBlockCode(String string) {
		billingBlockCode = string;
	}

	/**
	 * @param string
	 */
	public void setCashOnDeliveryBP(String string) {
		cashOnDeliveryBP = string;
	}

	/**
	 * @param string
	 */
	public void setCatalogDesc(String string) {
		catalogDesc = string;
	}

	/**
	 * @param string
	 */
	public void setCatalogId(String string) {
		catalogId = string;
	}

	/**
	 * @param string
	 */
	public void setCatalogVariant(String string) {
		catalogVariant = string;
	}

	/**
	 * @param b
	 */
	public void setDeliveryBlock(boolean b) {
		deliveryBlock = b;
	}

	/**
	 * @param string
	 */
	public void setDeliveryBlockCode(String string) {
		deliveryBlockCode = string;
	}

	/**
	 * @param string
	 */
	public void setId(String string) {
		id = string;
	}

	/**
	 * @param string
	 */
	public void setIncoterm(String string) {
		incoterm = string;
	}

	/**
	 * @param string
	 */
	public void setInsuranceCondition(String string) {
		insuranceCondition = string;
	}

	/**
	 * @param b
	 */
	public void setOneTimeCustomerScenario(boolean b) {
		isOneTimeCustomerScenario = b;
	}

	/**
	 * @param string
	 */
	public void setOneTimeCustomerId(String string) {
		oneTimeCustomerId = string;
	}

	/**
	 * @param string
	 */
	public void setOrderType(String string) {
		orderType = string;
	}

	/**
	 * @param string
	 */
	public void setPayPalOrderType(String string) {
		payPalOrderType = string;
	}

	/**
	 * @param string
	 */
	public void setPlant(String string) {
		plant = string;
	}

	/**
	 * @param string
	 */
	public void setQuotationType(String string) {
		quotationType = string;
	}

	/**
	 * @param string
	 */
	public void setRefBP(String string) {
		refBP = string;
	}

	/**
	 * @param string
	 */
	public void setRejectReason(String string) {
		rejectReason = string;
	}

	/**
	 * @param area
	 */
	public void setSalesArea(SalesAreaData area) {
		salesArea = area;
	}
	/**
	 * @param area
	 */
	public void setRefBPSalesArea(SalesAreaData area) {
		refBPSalesArea = area;
	}
	/**
	 * @param string
	 */
	public void setShipPriceCondition(String string) {
		shipPriceCondition = string;
	}
    
    /**
     * @param string
     */
    public void setRebatePriceCondition(String string) {
        rebatePriceCondition = string;
    }

	/**
	 * @param string
	 */
	public void setStorage(String string) {
		storage = string;
	}

	/**
	 * @param string
	 */
	public void setTaxPriceCondiction(String string) {
		taxPriceCondiction = string;
	}

	/**
	 * @param string
	 */
	public void setTotalPriceCondiction(String string) {
		totalPriceCondition = string;
	}

	/**
	 * @return
	 */
	public String getOtherPriceCondition() {
		return otherPriceCondition;
	}

	/**
	 * @param string
	 */
	public void setOtherPriceCondition(String string) {
		otherPriceCondition = string;
	}

	/**
	 * @return
	 */
	public boolean isIndependentCheckout() {
		return isIndependentCheckout;
	}

	/**
	 * @return
	 */
	public String getWebShopId() {
		return webShopId;
	}

	/**
	 * @param b
	 */
	public void setIndependentCheckout(boolean b) {
		isIndependentCheckout = b;
	}

	/**
	 * @param string
	 */
	public void setWebShopId(String string) {
		webShopId = string;
	}

}
