/*
 * Created on Feb 19, 2004
 *
 */
package com.sap.isa.auction.bean;

import java.io.Serializable;
import java.util.List;

/**
 * @author I802891
 *
 */
public abstract class Enumeration implements Serializable {
	private String name;
	private int value;
	
	//private static ArrayList enums = new ArrayList();

	protected Enumeration(String name, int value) {
		this.name = name;
		this.value = value;
	}

	public int getValue() {
		return value;
	}
	
	public String getName() {
		return name;
	}
	
	public String toString() {
		return "[" + name + " = " + value + "]";
	}
	
	public int toInt() {
		return value;
	}

	/**
	 * This should be called statically for each enum defined by derived class
	 * @param enum
	 */
	protected static void addEnum(List enums, Enumeration enum) {
		if(!enums.contains(enum)) {
			enums.add(enum);
		}
	}
	
	protected static Enumeration[] enumerate(List enums) {
		final Enumeration[] arr = new Enumeration[enums.size()];
		enums.toArray(arr);
		return arr;
	}
	
	protected static final Enumeration toEnum(List enums, int value) {
		for(int i=0; i<enums.size(); i++) {
			Enumeration enum = (Enumeration)enums.get(i); 
			if(value == enum.getValue())
				return enum;
		}
		throw new IllegalArgumentException("Enum = " + value + " is invalid");
	}
}
