/*
 * Created on Sep 30, 2003
 *
 * To change the template for this generated file go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
package com.sap.isa.auction.bean;

import java.util.ArrayList;

import com.sap.isa.auction.backend.boi.PlantData;
import com.sap.isa.auction.util.StringUtil;

/**
 * @author I803746
 *
 * To change the template for this generated type comment go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
public class Plant implements PlantData {

	private String plantID;
	/* (non-Javadoc)
	 * @see com.sap.isa.auction.backend.boi.PlantData#getPlantID()
	 */
	public String getPlantID() {
		return plantID;
	}

	/* (non-Javadoc)
	 * @see com.sap.isa.auction.backend.boi.PlantData#setPlantID(java.lang.String)
	 */
	public void setPlantID(String plantID) {
		this.plantID = plantID;
	}

	private String plantName;
	/* (non-Javadoc)
	 * @see com.sap.isa.auction.backend.boi.PlantData#getPlantName()
	 */
	public String getPlantName() {
		return plantName;
	}

	/* (non-Javadoc)
	 * @see com.sap.isa.auction.backend.boi.PlantData#setPlantName(java.lang.String)
	 */
	public void setPlantName(String plantName) {
		this.plantName = plantName;
	}

	private String plantNameB;
	/* (non-Javadoc)
	 * @see com.sap.isa.auction.backend.boi.PlantData#getPlantNameB()
	 */
	public String getPlantNameB() {
		return plantNameB;
	}

	/* (non-Javadoc)
	 * @see com.sap.isa.auction.backend.boi.PlantData#setPlantNameB(java.lang.String)
	 */
	public void setPlantNameB(String plantName) {
		this.plantNameB = plantName;
	}

	private String location;
	/* (non-Javadoc)
	 * @see com.sap.isa.auction.backend.boi.PlantData#getSelectedStorageLocation()
	 */
	public String getSelectedStorageLocation() {
		return location;
	}

	/* (non-Javadoc)
	 * @see com.sap.isa.auction.backend.boi.PlantData#setSelectedStorageLocation(java.lang.String)
	 */
	public void setSelectedStorageLocation(String location) {
		this.location = location;
	}

	ArrayList locations;
	/* (non-Javadoc)
	 * @see com.sap.isa.auction.backend.boi.PlantData#getStorageLocations()
	 */
	public ArrayList getStorageLocations() {
		if(locations == null)
			locations = new ArrayList();
		return locations;
	}

	/* (non-Javadoc)
	 * @see com.sap.isa.auction.backend.boi.PlantData#setStorageLocations(java.util.ArrayList)
	 */
	public void setStorageLocations(ArrayList locations) {
		this.locations = locations;
	}

///////////////////////////////////////////////////////////////////////////	
	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	public boolean equals(Object obj) {
		if(obj == null || !(obj instanceof PlantData))
			return false;
		PlantData other = (PlantData)obj;
		return 
			StringUtil.ObjectCompare(this.getPlantID(), other.getPlantID()) &&
			StringUtil.ObjectCompare(this.getSelectedStorageLocation(), other.getSelectedStorageLocation());
	}
	
	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	public int hashCode() {
		int hash = this.toString().hashCode();
		return hash;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	public String toString() {
		StringBuffer buffer = new StringBuffer();
		buffer.append("PI=");
		buffer.append(this.getPlantID());
		buffer.append(";SO=");
		buffer.append(this.getSelectedStorageLocation());
		return buffer.toString();
	}
	
	public boolean isEmpty()
	{
		if(plantID != null && plantID.length()>0)
			return false;
			
		if(location != null && location.length()>0)
			return false;

		return true;
	}
}
