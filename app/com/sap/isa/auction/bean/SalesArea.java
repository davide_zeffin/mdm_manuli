/*****************************************************************************
    Copyright (c) 2001, SAPMarkets Palo Alto, USA, All rights reserved.

*****************************************************************************/

package com.sap.isa.auction.bean;

import com.sap.isa.auction.backend.boi.SalesAreaData;
import com.sap.isa.auction.util.StringUtil;

import java.io.Serializable;

public class SalesArea implements SalesAreaData, Serializable {
    /** responsible sales organization **/
    private String respSalesOrganization;

    /** sales organization **/
    private String salesOrganization;

    /**division**/
    private String division;

    /**distribution channel */
    private String distributionChannel;

    //constructor
    public SalesArea() {

    }
	
    public SalesArea (String org, String respOrg, String disChannel, String div){
		salesOrganization = org;
        respSalesOrganization = respOrg;
        distributionChannel = disChannel;
        division = div;
    }
    
    public String getSalesOrganization() {
        return salesOrganization;
    }

    public String getResponsibleSalesOrganization() {
        return respSalesOrganization;
    }

    public void setSalesOrganization(String salesOrg) {
        salesOrganization = salesOrg;
    }

    public void setResponsibleSalesOrganization(String respSalesOrg) {
        respSalesOrganization = respSalesOrg;
    }

    public String getDistributionChannel() {
        return distributionChannel;
    }

    public void setDistributionChannel(String disChannel) {
        distributionChannel = disChannel;
    }

    public String getDivision() {
        return division;
    }

    public void setDivision(String division) {
        this.division = division;
    }
    
    
	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	public boolean equals(Object obj) {
		if(obj == null || !(obj instanceof SalesAreaData))
			return false;
		SalesAreaData other = (SalesAreaData)obj;
		return 
            StringUtil.ObjectCompare(this.getSalesOrganization(), other.getSalesOrganization()) &&
            StringUtil.ObjectCompare(this.getResponsibleSalesOrganization(), other.getResponsibleSalesOrganization()) &&
			StringUtil.ObjectCompare(this.getDistributionChannel(), other.getDistributionChannel()) &&
			StringUtil.ObjectCompare(this.getDivision(), other.getDivision());
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	public int hashCode() {
		int hash = this.toString().hashCode();
		return hash;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	public String toString() {
		StringBuffer buffer = new StringBuffer();
        buffer.append("SO=");
        buffer.append(this.getSalesOrganization());
        buffer.append(";RS=");
        buffer.append(this.getResponsibleSalesOrganization());
		buffer.append(";DC=");
		buffer.append(this.getDistributionChannel());
		buffer.append(";DV=");
		buffer.append(this.getDivision());
		return buffer.toString();
	}
	
	public boolean isEmpty()
	{
        if(salesOrganization != null && salesOrganization.length()>0)
            return false;
            
        if(respSalesOrganization != null && respSalesOrganization.length()>0)
            return false;
            
		if(distributionChannel != null && distributionChannel.length()>0)
			return false;
			
		if(division != null && division.length()>0)
			return false;
			
		return true;
	}
}
