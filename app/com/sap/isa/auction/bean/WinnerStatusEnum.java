package com.sap.isa.auction.bean;

import java.util.*;;

/**
 * Title:        Internet Sales
 * Description:
 * Copyright:    Copyright (c) 2002
 * Company:      SAPMarkets
 * @author
 * @version 1.0
 */

public class WinnerStatusEnum extends Enumeration {
	private static List winnerStatusEnum = new ArrayList();

	/**
	 * These int values should not be modified as they are mapped to database
	 */
	private static final int INT_NONE = 0;
	private static final int INT_CHECKOUTINCOMPLETE  = 4;
	private static final int INT_CHECKOUTPENDING   = 6;
	private static final int INT_CHECKOUTCOMPLETE    = 9;
	private static final int INT_RETRACTED    = 10;


	private static final String STR_NONE = "NONE";
	private static final String STR_CHECKOUTINCOMPLETE   = "CHECKOUTINCOMPLETE";
	private static final String STR_CHECKOUTPENDING    = "CHECKOUTPENDING";
	private static final String STR_CHECKOUTCOMPLETE     = "CHECKOUTCOMPLETE";	
	private static final String STR_RETRACTED     = "RETRACTED";
	public static final WinnerStatusEnum NONE =
		new WinnerStatusEnum(INT_NONE,STR_NONE);

	public static final WinnerStatusEnum CHECKOUTINCOMPLETE =
		new WinnerStatusEnum(INT_CHECKOUTINCOMPLETE,STR_CHECKOUTINCOMPLETE);
	public static final WinnerStatusEnum CHECKOUTPENDING =
		new WinnerStatusEnum(INT_CHECKOUTPENDING,STR_CHECKOUTPENDING);
	public static final WinnerStatusEnum CHECKOUTCOMPLETE =
		new WinnerStatusEnum(INT_CHECKOUTCOMPLETE,STR_CHECKOUTCOMPLETE);
	public static final WinnerStatusEnum RETRACTED =
		new WinnerStatusEnum(INT_RETRACTED,STR_RETRACTED);


	static {
		addEnum(winnerStatusEnum, NONE);
		addEnum(winnerStatusEnum, CHECKOUTINCOMPLETE);
		addEnum(winnerStatusEnum, CHECKOUTPENDING);
		addEnum(winnerStatusEnum, CHECKOUTCOMPLETE);
		addEnum(winnerStatusEnum, RETRACTED);
	}
	
	private WinnerStatusEnum(int value, String name) {
		super(name, value);
	}

	public static WinnerStatusEnum[] enumerate() {
		return (WinnerStatusEnum[])enumerate(winnerStatusEnum);
	}

	public static final WinnerStatusEnum toEnum(int value) {
		return (WinnerStatusEnum)toEnum(winnerStatusEnum, value);
	}
	
	public static final int toInt(WinnerStatusEnum enum) {
		return enum.getValue();
	}
}