/*
 * Created on Oct 27, 2003
 *
 */
package com.sap.isa.auction.bean;

import java.util.ArrayList;
import java.util.StringTokenizer;

/**
 * @author I802891
 * 
 * TP DO to move the String specific code to a String Utility class
 *
 */
public class BeanSerializer {
	private BeanSerializer() {
	}

	private static final char PROP_DELIM = '$';
	private static final String NULL = "null"; // used to distinguish null values
		
	public static String serializeSalesAreaAsString(SalesArea sa) {
		if(sa == null) return null;
		/* 
		 * Order of property value is important, 
		 * Do not changes as it will break deserialization
		 */
		String escValue = getValidStringToken(escapeDelimiter(sa.getSalesOrganization(),PROP_DELIM));
		StringBuffer buffer = new StringBuffer();
		buffer.append(escValue);
		buffer.append(PROP_DELIM);
		escValue = getValidStringToken(escapeDelimiter(sa.getDistributionChannel(),PROP_DELIM));
		buffer.append(escValue);
		buffer.append(PROP_DELIM);
        escValue = getValidStringToken(escapeDelimiter(sa.getDivision(),PROP_DELIM));
        buffer.append(escValue);
        buffer.append(PROP_DELIM);
        escValue = getValidStringToken(escapeDelimiter(sa.getResponsibleSalesOrganization(),PROP_DELIM));
        buffer.append(escValue);
        buffer.append(PROP_DELIM);
		
		return buffer.toString();
	}
	
	public static SalesArea deserializeSalesArea(String salesArea) {
		if(salesArea == null) return null;
		
		// tokenize on unescaped DELIMs		
		String[] tokens = tokenize(salesArea,PROP_DELIM,ESCAPE);

		SalesArea sa = new SalesArea();
		sa.setSalesOrganization(unescapeDelimiter(getValidString(tokens[0]),PROP_DELIM));
		sa.setDistributionChannel(unescapeDelimiter(getValidString(tokens[1]),PROP_DELIM));
		sa.setDivision(unescapeDelimiter(getValidString(tokens[2]),PROP_DELIM));
        if (tokens.length > 3)
            sa.setResponsibleSalesOrganization(unescapeDelimiter(getValidString(tokens[3]),PROP_DELIM));
        else
            sa.setResponsibleSalesOrganization(sa.getSalesOrganization()); // set default to salesorg
		return sa;
	}
	
	public static String serializeAddressAsString(Address address) {
		if(address == null) return null;
		StringBuffer strBuffer = new StringBuffer();
		/* 
		 * Order of property value is important, 
		 * Do not changes as it will break deserialization
		 */		
		strBuffer.append(getValidStringToken(escapeDelimiter(address.getStreet1(),PROP_DELIM)));
		strBuffer.append(PROP_DELIM);
		strBuffer.append(getValidStringToken(escapeDelimiter(address.getStreet2(),PROP_DELIM)));
		strBuffer.append(PROP_DELIM);
		strBuffer.append(getValidStringToken(escapeDelimiter(address.getCity(),PROP_DELIM)));
		strBuffer.append(PROP_DELIM);
		strBuffer.append(getValidStringToken(escapeDelimiter(address.getStateOrProvince(),PROP_DELIM)));
		strBuffer.append(PROP_DELIM);	
		strBuffer.append(getValidStringToken(escapeDelimiter(address.getCountry(),PROP_DELIM)));
		strBuffer.append(PROP_DELIM);
		strBuffer.append(getValidStringToken(escapeDelimiter(address.getZip(),PROP_DELIM)));
		strBuffer.append(PROP_DELIM);		
		
		return strBuffer.toString();		
	}
	
	public static Address deserializeAddress(String address) {
		if(address == null) return null;
		
		String[] tokens = tokenize(address,PROP_DELIM,ESCAPE);
		Address add = new Address();
		for(int i = 0; i < tokens.length; i++) {
			switch(i) {
				case 0: add.setStreet1(unescapeDelimiter(getValidString(tokens[0]),PROP_DELIM)); break;
				case 1: add.setStreet2(unescapeDelimiter(getValidString(tokens[1]),PROP_DELIM)); break;
				case 2: add.setCity(unescapeDelimiter(getValidString(tokens[2]),PROP_DELIM)); break;
				case 3: add.setStateOrProvince(unescapeDelimiter(getValidString(tokens[3]),PROP_DELIM)); break;
				case 4: add.setCountry(unescapeDelimiter(getValidString(tokens[4]),PROP_DELIM)); break;
				case 5: add.setZip(unescapeDelimiter(getValidString(tokens[4]),PROP_DELIM)); break;				
			}
		}
		return add;
	}
	
	/*
	 * String utility functions
	 */
	/**
	 * tokenize on unescaped delims
	 * @param input
	 * @param delim
	 * @param escape
	 * @return
	 */
	
	private static String getValidStringToken(String str) {
		if(str != null) {
			return str;
		}
		else {
			return NULL;
		}
	}
	
	private static String getValidString(String str) {
		if(str.equals(NULL)) {
			return null;
		}
		else {
			return str;
		}
	}
	
	/**
	 * Empty String tokens between Delims are not returned they are skipped
	 * @param input
	 * @param delim
	 * @param escape
	 * @return
	 */
	private static String[] tokenize(String input, char delim, char escape) {
	
		ArrayList tokens = new ArrayList();
		int lastIdx = 0; // at start
		char lookback = 0;
		for(int i = 0 ; i < input.length();i++) {
			char c = input.charAt(i);
			// we have found the split
			if(c == PROP_DELIM && lookback != escape) {
				String token = input.substring(lastIdx,i);
				// add only non empty tokens
				/*if(token.length() > 0)*/ tokens.add(token);
				lastIdx = i + 1;
			}
			lookback = c;
		}
		// Last token
		if(lastIdx < input.length()) {
			String token = input.substring(lastIdx);
			tokens.add(token);
		}
		
		String[] strTokens = new String[tokens.size()];
		tokens.toArray(strTokens);
		return strTokens;
	}
	
	private static char ESCAPE = '\\';
	private static String escapeDelimiter(String input, char delim) {
		String escInput = input;
		if(escInput != null && escInput.length() > 0) {
			StringBuffer escInputBuf = new StringBuffer(escInput);
			String strDelim = new String(new char[]{delim});
			int current = 0;
			int escaped = 0;
			while((current = escInput.indexOf(strDelim,current)) != -1) {
				escInputBuf.insert(current + escaped++,ESCAPE);
				current = current + strDelim.length();
			}
			escInput = escInputBuf.toString();
		}
		return escInput;
	}
	
	private static String unescapeDelimiter(String input, char delim) {
		String unescInput = input;
		if(unescInput != null && unescInput.length() > 0) {
			String strDelim = new String(new char[]{ESCAPE,delim});
			StringBuffer unescBuffer = new StringBuffer(unescInput);
			int current = 0;
			int escaped = 0;
			while((current = unescInput.indexOf(strDelim,current)) != -1) {
				unescBuffer.deleteCharAt(current - escaped++);
				current = current + strDelim.length();
			}
			unescInput = unescBuffer.toString();
		}
		return unescInput;
	}
	
	public static void main(String[] args) {
		String str = "$abc$$$def";
		String val = escapeDelimiter(str,'$');
		val = unescapeDelimiter(val,'$');
		String[] tokens = tokenize(str,'$','\\');
		
		StringTokenizer tokenizer = new StringTokenizer(str,"$");
		int count = tokenizer.countTokens();
		while(tokenizer.hasMoreTokens()) {
			String token = tokenizer.nextToken();
			com.sap.isa.auction.util.Debug.print(token);
		}

		
		SalesArea sa = new SalesArea();
		sa.setDistributionChannel("abc\\$");
		sa.setDivision("$def$");
        sa.setSalesOrganization("$sa$sa$");
        sa.setResponsibleSalesOrganization("$rs$rs$");
		String ser = serializeSalesAreaAsString(sa);
		sa = deserializeSalesArea(ser);
	}
}
