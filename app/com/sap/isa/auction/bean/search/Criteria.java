package com.sap.isa.auction.bean.search;

/**
 * Title:        Internet Sales
 * Description:  Web Auctions
 * Copyright:    Copyright (c) 2002, All rights reserved
 * Company:      SAP Labs, PA
 * @author e-auctions
 * @version 0.1
 */

public abstract class Criteria {
    public String name; // attribute/Column Name
    public Object rhs;  // value

    public abstract boolean evaluate(Object lhs);
}