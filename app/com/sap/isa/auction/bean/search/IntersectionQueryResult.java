package com.sap.isa.auction.bean.search;

import java.util.Iterator;
import java.util.Set;
import java.util.HashSet;
import java.util.Collection;

/**
 * Description:  This query result returns only those elements which are present
 *              in both the search results.
 * Copyright:    Copyright (c) 2002, All rights reserved
 * Company:      SAP Labs, PA
 * @author e-auctions
 * @version 0.1
 */

public class IntersectionQueryResult extends QueryResult {

    private static final Collection EMPTY_COLL = new HashSet(0);

    protected transient Collection result = null;
    protected transient QueryResult result1 = null;
    protected transient QueryResult result2 = null;
    protected int pos = 1; // starting pos

    public IntersectionQueryResult(QueryResult result1, QueryResult result2) {
        this.result1 = result1;
        this.result2 = result2;
    }

    /**
     * Diff policy can be overridden for custom behavior
     */

    protected Collection diff() {
        // if either result is empty, return EMPTY_COLL
        if(result1.resultSize() == 0 || result2.resultSize() == 0)
            return EMPTY_COLL;

        Set set1 = toSet(result1);
        Set set2 = toSet(result2);
        Set intersection = intersection(set1,set2);
        return intersection;
    }

    protected static Set toSet (QueryResult result) {
        result.position(1);

        HashSet set = new HashSet(result.resultSize());
        while(result.available()) {
            Object[] objs = result.fetch();
            // loop overhead can be avoided if fetch returns a collection
            for(int i = 0; i < objs.length; i++) {
                set.add(objs[i]);
            }
        }
        return set;
    }

    /**
     * Allow overidding for custom intersection logic
     */
    protected Set intersection(Set set1, Set set2) {
        HashSet intersection = new HashSet();

        Set smallerSet = set1.size() <= set2.size() ? set1 : set2;

        // iterate over smaller set to compute intersection
        Iterator it = smallerSet.iterator();

        while(it.hasNext()) {
            Object obj = it.next();
            // != over contains check
            if(set1 != smallerSet && set1.contains(obj)) {
                intersection.add(obj);
            }
            else if(set2 != smallerSet && set2.contains(obj)) {
                intersection.add(obj);
            }
        }

        return intersection;
    }

    /**
     * @return the Total no of Result Records obtained by Query execution
     */
    public int resultSize() {
        usageCheck();
        if(result == null) result = diff();
        return result.size();
    }

    /**
     * If more matching Objects are available
     */
    public boolean available() {
        usageCheck();
        if(result == null) result = diff();
        return pos <= result.size();
    }

    /**
     * Positions by absolute pos
     */
    public void position(int pos) {
        usageCheck();

        if(pos < 1 || pos > result.size()) throw new IllegalArgumentException("invalid position");

        this.pos = pos;
    }

    /**
     * @return int the Current Absolute Pos
     */
    public int getPosition() {
        usageCheck();

        return pos;
    }

    /**
     * Converts the Search into Value Beans for Usage by UI  Layer
     */
    public Object[] fetch() {
        usageCheck();

        // on demand diff
        if(result == null) result = diff();

        Object[] ret = new Object[size];
        int index = 1;
        Iterator it = result.iterator();
        // move to current pos. Thats the only way:(
        while(index < pos){
           index++;
           it.next(); // index to pos
        }
        // move pos
        while(available() && pos < index + size) {
            ret[pos - index] = it.next();
            pos++;
        }
        return ret;
    }

    public void close() {
        if(super.closed) return;
        if(result != null) result.clear();
        result = null;
        if(result1 != null) result1.close();
        result1 = null;
        if(result2 != null) result2.close();
        result2 = null;
        super.close();
    }
}