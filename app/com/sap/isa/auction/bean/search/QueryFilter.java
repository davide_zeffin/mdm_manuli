package com.sap.isa.auction.bean.search;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;


/**
 * Title:        Internet Sales
 * Description:
 * Copyright:    Copyright (c) 2002
 * Company:      SAPMarkets
 * @author: e-auctions
 * @version 0.1
 * $revision $
 */

public abstract class QueryFilter implements Cloneable, Serializable {

	public static final int OR = 0x10;
	public static final int AND = 0x20;
	public static final int NOT = 0x01;
	public static final int NONE = -0x01;

	private String name;
	// this may contain Search Filter as Criterias
	// In this case there will be
	private ArrayList criterias;
	private String[] orderby;
	private boolean asc = true; // default asc
	private int op = NONE;
	private boolean parenthesized = false;

	public QueryFilter() {
		this("NoName");
	}

	public QueryFilter(String name) {
		this.name = name;
		criterias = new ArrayList();
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
    
	public void clear(){
		if (criterias != null && !criterias.isEmpty())
			criterias.clear();
	}

	/**
	 * Logical And Operator between this and supplied Filter
	 */
	void and(QueryFilter filter2) {
		filter2.op = AND;
		filter2.parenthesized = true;
		criterias.add(filter2);
	}

	/**
	 * Logical or Operator between this and supplied Filter
	 */
	void or(QueryFilter filter2) {
		filter2.op = OR;
		filter2.parenthesized = true;
		criterias.add(filter2);
	}

	/**
	 * Logical Not Operator on this Filter
	 */
	void not() {
		this.op = NOT;
		parenthesized = true;
	}

	/**
	 *returns the Operator associated in case of Composite Filter
	 *NONE in case of Simple filter
	 */
	public int getOperator() {
		return op;
	}

	public boolean isParenthesized () {
		return parenthesized;
	}

	public void setParenthesized(boolean value) {
		this.parenthesized = value;
	}

	/**
	 * ArrayList of Criterias
	 * Criteria are bu default
	 */
	public ArrayList getCriterias() {
		return criterias;
	}

	/**
	 * Like Criteria Name like 'SAP*', Wild Card Search
	 */
	public void addLikeCriteria(String name, Object value) {
		if(name == null || value == null) throw new IllegalArgumentException("name or value is null");

		validateSearchAttr(name);
		if(!checkSupported(name, LikeCriteria.OP)) {
			throw new UnsupportedOperationException("Like criteria is not supported for " + name);
		}
		
		Criteria crit = new LikeCriteria();
		crit.name = name;
		crit.rhs = value;

		criterias.add(crit);
	}

	/**
	 * Equal to Criteria Name = 'NPS'
	 */
	public void addEqualToCriteria(String name, Object value) {
		if(name == null || value == null) throw new IllegalArgumentException("name or value is null");

		validateSearchAttr(name);
		if(!checkSupported(name, EqualToCriteria.OP)) {
			throw new UnsupportedOperationException("Equal to criteria is not supported for " + name);
		}

		Criteria crit = new EqualToCriteria();
		crit.name = name;
		crit.rhs = value;

		criterias.add(crit);
	}

	/**
	 * Not Equal to Criteria, Name != 'NPS'
	 */
	public void addNotEqualToCriteria(String name, Object value) {
		if(name == null || value == null) throw new IllegalArgumentException("name or value is null");

		validateSearchAttr(name);
		if(!checkSupported(name, NotEqualToCriteria.OP)) {
			throw new UnsupportedOperationException("Not equal to criteria is not supported for " + name);
		}

		Criteria crit = new NotEqualToCriteria();
		crit.name = name;
		crit.rhs = value;

		criterias.add(crit);
	}

	/**
	 * Greater Than Criteria, Age > 35
	 */
	public void addGreaterThanCriteria(String name, Object value) {
		if(name == null || value == null) throw new IllegalArgumentException("name or value is null");

		validateSearchAttr(name);
		if(!checkSupported(name, GreaterThanCriteria.OP)) {
			throw new UnsupportedOperationException("Greater than criteria is not supported for " + name);
		}

		Criteria crit = new GreaterThanCriteria();
		crit.name = name;
		crit.rhs = value;

		criterias.add(crit);
	}

	/**
	 * >= Criteria
	 */
	public void addGTEqualToCriteria(String name, Object value) {
		if(name == null || value == null) throw new IllegalArgumentException("name or value is null");

		validateSearchAttr(name);
		if(!checkSupported(name, GreaterThanEqualToCriteria.OP)) {
			throw new UnsupportedOperationException("Greater than equal to criteria is not supported for " + name);
		}

		Criteria crit = new GreaterThanEqualToCriteria();
		crit.name = name;
		crit.rhs = value;

		criterias.add(crit);
	}

	/**
	 * < Criteria
	 */
	public void addLessThanCriteria(String name, Object value) {
		if(name == null || value == null) throw new IllegalArgumentException("name or value is null");

		validateSearchAttr(name);
		if(!checkSupported(name, LessThanCriteria.OP)) {
			throw new UnsupportedOperationException("Less than criteria is not supported for " + name);
		}

		Criteria crit = new LessThanCriteria();
		crit.name = name;
		crit.rhs = value;

		criterias.add(crit);
	}

	/**
	 * <= Criteria
	 */
	public void addLTEqualToCriteria(String name, Object value) {
		if(name == null || value == null) throw new IllegalArgumentException("name or value is null");

		validateSearchAttr(name);
		if(!checkSupported(name, LessThanEqualToCriteria.OP)) {
			throw new UnsupportedOperationException("Less than equal to criteria is not supported for " + name);
		}
		
		Criteria crit = new LessThanEqualToCriteria();
		crit.name = name;
		crit.rhs = value;

		criterias.add(crit);
	}

	/**
	 *
	 */
	public void addInCriteria(String name , Collection values) {
		if(name == null || values == null) throw new IllegalArgumentException("name or value is null");

		validateSearchAttr(name);
		if(!checkSupported(name, InCriteria.OP)) {
			throw new UnsupportedOperationException("In criteria is not supported for " + name);
		}
		
		Criteria crit = new InCriteria();
		crit.name = name;
		crit.rhs = values;

		criterias.add(crit);
	}

	/**
	 *
	 */
	public void addNullCriteria(String name) {
		if(name == null) throw new IllegalArgumentException("name or value is null");

		validateSearchAttr(name);
		if(!checkSupported(name, NullCriteria.OP)) {
			throw new UnsupportedOperationException("Null criteria is not supported for " + name);
		}
		
		Criteria crit = new NullCriteria();
		crit.name = name;
		criterias.add(crit);
	}

	/**
	 *
	 */
	public void addNotNullCriteria(String name) {
		if(name == null) throw new IllegalArgumentException("name or value is null");

		validateSearchAttr(name);
		if(!checkSupported(name, NotNullCriteria.OP)) {
			throw new UnsupportedOperationException("Not null criteria is not supported for " + name);
		}
		
		Criteria crit = new NotNullCriteria();
		crit.name = name;

		criterias.add(crit);
	}

	/**
	 * @param attrs name of Attributes in order for sorting
	 * Gets mapped to order by attrs[0],attrs[1]...
	 */
	public void orderBy(String[] attrs) {
		if(attrs == null || attrs.length == 0) {
			throw new IllegalArgumentException("arg attrs is invalid");
		}

		this.orderby = new String[attrs.length];
		System.arraycopy(attrs,0,orderby,0,attrs.length);
	}

	public String[] getOrderBy() {
		return orderby;
	}

	public void setAscending(boolean arg) {
		this.asc = arg;
	}

	public boolean isAscending() {
		return asc;
	}

	protected abstract void validateSearchAttr(String attrName);
	protected boolean checkSupported(String attrName, short op) {
		return true;
	}

	public String toString() {
		StringBuffer buffer = new StringBuffer();
		buffer.append("<");
		for(int i = 0 ; i < criterias.size(); i++) {
			Criteria crit = (Criteria)criterias.get(i);
			if(i != 0) buffer.append(",");
			buffer.append(crit.toString());
		}
		buffer.append(">");
		return buffer.toString();
	}

	///////////////////////////////////////////////////////////////
	// Criteria Sub classes
	public static class EqualToCriteria extends Criteria {
		public static final short OP = 1; 
		public boolean evaluate(Object lhs) {
			return rhs.equals(lhs);
		}

		public String toString() {
			return "[" + this.name + " == " + rhs + "]";
		}
	}

	public static class NotEqualToCriteria extends Criteria {
		public static final short OP = 2;    	
		public boolean evaluate(Object lhs) {
			if(lhs instanceof java.lang.Comparable) {
			}
			return !rhs.equals(lhs);
		}

		public String toString() {
			return "[" + this.name + " != " + rhs + "]";
		}
	}

	public static class GreaterThanEqualToCriteria extends Criteria {
		public static final short OP = 3;    	
		public boolean evaluate(Object lhs) {
			return false;
		}

		public String toString() {
			return "[" + this.name + " >= " + rhs + "]";
		}
	}

	public static class GreaterThanCriteria extends Criteria {
		public static final short OP = 4;    	
		public boolean evaluate(Object lhs) {
			return false;
		}

		public String toString() {
			return "[" + this.name + " > " + rhs + "]";
		}
	}

	public static class LessThanEqualToCriteria extends Criteria {
		public static final short OP = 5;    	
		public boolean evaluate(Object lhs) {
			return false;
		}

		public String toString() {
			return "[" + this.name + " <= " + rhs + "]";
		}
	}

	public static class LessThanCriteria extends Criteria {
		public static final short OP = 6;    	
		public boolean evaluate(Object lhs) {
			return false;
		}

		public String toString() {
			return "[" + this.name + " < " + rhs + "]";
		}
	}

	public static class LikeCriteria extends Criteria {
		public static final short OP = 7;    	
		public boolean evaluate(Object lhs) {
			return false;
		}

		public String toString() {
			return "[" + this.name + " like " + rhs + "]";
		}
	}

	public static class InCriteria extends Criteria {
		public static final short OP = 8;
		public boolean evaluate(Object lhs) {
			return false;
		}

		public String toString() {
			return "[" + this.name + " in (" + rhs + ")]";
		}
	}

	public static class NullCriteria extends Criteria {
		public static final short OP = 9;    	
		public boolean evaluate(Object lhs) {
			return lhs == null;
		}

		public String toString() {
			return "[" + this.name + " == null]";
		}
	}

	public static class NotNullCriteria extends Criteria {
		public static final short OP = 10;    	
		public boolean evaluate(Object lhs) {
			return lhs != null;
		}

		public String toString() {
			return "[" + this.name + " != null]";
		}
	}
}
