package com.sap.isa.auction.bean.search;

import java.util.HashMap;

/**
 * Title:        Dummy Implemenetation
 * Description:  Web Auctions
 * Copyright:    Copyright (c) 2002, All rights reserved
 * Company:      SAP Labs, PA
 * @author e-auctions
 * @version 0.1
 */

public class UnionQueryResult extends QueryResult {

    QueryResult result1;
    QueryResult result2;
    int pos = 1;
    HashMap cache = null;

    public UnionQueryResult(QueryResult result1, QueryResult result2) {
        this.result1 = result1;
        this.result2 = result2;
        cache = new HashMap((result1.resultSize() + result2.resultSize())/2);
    }

    /**
     * @return the Total no of Result Records obtained by Query execution
     */
    public int resultSize() {
        usageCheck();
        return result1.resultSize() + result2.resultSize();
    }

    /**
     * If more matching Objects are available
     */
    public boolean available() {
        usageCheck();
        return pos <= resultSize();
    }

    /**
     * Positions by absolute pos
     */
    public void position(int pos) {
        usageCheck();
        if(pos < 1 || pos > resultSize()) {
            throw new IllegalArgumentException("Position is invalid");
        }
        this.pos = pos;
    }

    /**
     * @return int the Current Absolute Pos
     */
    public int getPosition() {
        usageCheck();
        return pos;
    }

    /**
     * Sets the Fetch Size for retreival by Fetch {@link #fetch}
     */
    public void setFetchSize(int size) {
        super.setFetchSize(size);
    }

    /**
     * Converts the Search into Value Beans for Usage by UI  Layer
     */
    public Object[] fetch() {
        usageCheck();

        Object[] ret1 = null;
        Object[] ret2 = null;
        int index = 1;
        /*
        // requires only result2
        if(pos > result1.resultSize()) {
            if(pos - result1.resultSize() + size > result2.resultSize()) {
                ret2 = result2.fetch(pos - result1.resultSize(),result2.resultSize());
            }
            else {
                ret2 = result2.fetch(pos - result1.resultSize(),pos - result1.resultSize() + size);
            }
        }
        // requires both
        else if (pos + size > result1.resultSize()){
            ret1 = result1.fetch(pos,result1.resultSize());
            if(pos - result1.resultSize() + size > result2.resultSize()) {
                ret2 = result2.fetch(pos - result1.resultSize(),result2.resultSize());
            }
            else {
                ret2 = result2.fetch(pos - result1.resultSize(),pos - result1.resultSize() + size);
            }
        }
        else {
            ret1 = result1.fetch(pos,result1.resultSize());
        }
        */
        // merge ret1 & ret2
        Object[] ret = new Object[size];
        if(ret1 != null) System.arraycopy(ret1,0,ret,0,nonNullLength(ret1));
        if(ret2 != null) System.arraycopy(ret2,0,ret,nonNullLength(ret),ret2.length);
        return ret;
    }

    private int nonNullLength(Object[] arr) {
        int length = 0;
        for(int i = 0; i < arr.length; i++) {
            if(arr[i] != null) length++;
        }
        return length;
    }

    public void close() {
        if(super.closed) return;
        if(result1 != null) result1.close();
        if(result2 != null) result2.close();
        super.close();
    }
}