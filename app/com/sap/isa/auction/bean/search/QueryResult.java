package com.sap.isa.auction.bean.search;

/**
 * Title:        Internet Sales
 * Description:
 * <P>
 * Results from a search method returns QueryResult
 * This object is logical map to SQL Resultset.
 * QueryResult returns Java Objects matching the Query Criteria
 * For Value Queries corresponding Object Represenatations of Column Values
 * are returned. For Multiple Column Value Queries, Object[] is returned
 * </p>
 * <p>
 * QueryResult behaves like an Iterator with support for random positioning in the
 * result set. This class is not thread safe.
 * </p>
 *
 * Copyright:    Copyright (c) 2002
 * Company:      SAPMarkets
 * @author
 * @version 1.0
 */

public abstract class QueryResult {

	private static final int DEFAULT_FETCH_SIZE = 10;

	protected int size = DEFAULT_FETCH_SIZE;
	protected boolean closed = false;

	protected QueryResult() {
	}

	protected void usageCheck() {
		if(closed) throw new IllegalStateException("QueryResult is closed");
	}

	/**
	 * Sets the Fetch Size for retreival by Fetch {@link #fetch}
	 */
	public void setFetchSize(int size) {
		usageCheck();
		if(size <= 0) throw new IllegalArgumentException("Invalid fetch size: " + size);
		if(size > resultSize()) size = resultSize();
		this.size = size;
	}

	/**
	 *
	 */
	public int getFetchSize() {
		usageCheck();
		return size;
	}

	/**
	 * @return the Total no of Result Records obtained by Query execution
	 */
	public abstract int resultSize();

	/**
	 * If more matching Objects are available
	 */
	public boolean available() {
		usageCheck();
		return getPosition() <= resultSize();
	}

	/**
	 * QueryResult is placed at position 0 initially i.e. before the first search element
	 * Positions by absolute pos. For random positioning in resultset
	 */
	public abstract void position(int pos);

	/**
	 * @return int the current Absolute Pos
	 */
	public abstract int getPosition();
	
	/**
	 * Overriding point
	 */
	public void connect() {
	}
	
	/**
	 * Overriding point
	 */
	
	public void disconnect() {
	}
	
	/**
	 * Overriding point
	 */
	public boolean isConnected() {
		return true;
	}
	
	/*
	 * refreshes the search
	 */
	public void refresh() {
		// do nothing
	}
	
	/**
	 * Returns the next Object in the result or null if non is available
	 */
	public Object fetchNext() {
		return fetch(1)[0];
	}

	/**
	 * Retreives the objects based on set fetch size
	 */
	public abstract Object[] fetch();

	/**
	 * <p>
	 * Retreives the next <code>count</code> searched objects from result
	 * Converts the Search into Value Beans for Usage by UI  Layer
	 * </p>
	 *
	 * @param count number of next objects to retreive begining current position.
	 * @return Object[] fetched objects , Object[].length <= count
	 */
	public Object[] fetch(int count) {
		if(count < 1) throw new IllegalArgumentException("arg count in invalid, value : " + count);
		int oldFetch = getFetchSize();
		setFetchSize(count);
		Object[] result = fetch();
		if(oldFetch <= resultSize())
			setFetchSize(oldFetch);
		return result;
	}

	/**
	 * Closes an existing QueryResult and releases the underlying resources
	 * Close on an already closed result does nothing. QueryResult must be
	 * closed after usage, otherwise it will lead to a resource leakage
	 */
	public void close() {
		if(closed) return;
		closed = true;
	}
    
	public boolean isClosed()  {
		return closed;
	}
}