/*
 * Created on Jan 17, 2005
 *
 * To change the template for this generated file go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
package com.sap.isa.auction.bean;

import java.util.ArrayList;
import java.util.List;

/**
 * @author I802850
 *
 * To change the template for this generated type comment go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
public class EmailSentStatusEnum extends Enumeration{
	private static List EmailSentStatusEnums = new ArrayList();

	/**
	 * These int values should not be modified as they are mapped to database
	 */
	private static final int INT_NONE        = 0;
	private static final int INT_SENT        = 1;
	private static final int INT_NOTSENT = 	    2;

	private static final String STR_SENT         = "SENT";
	private static final String STR_NONE         = "NONE";
	private static final String STR_NOTSENT        = "NOTSENT";

	public static final EmailSentStatusEnum NONE =
		new EmailSentStatusEnum(INT_NONE,STR_NONE);

	public static final EmailSentStatusEnum SENT =
		new EmailSentStatusEnum(INT_SENT,STR_SENT);
	public static final EmailSentStatusEnum NOTSENT =
		new EmailSentStatusEnum(INT_NOTSENT,STR_NOTSENT);

	static {
		addEnum(EmailSentStatusEnums, NONE);
		addEnum(EmailSentStatusEnums, SENT);
		addEnum(EmailSentStatusEnums, NOTSENT);

	}
	
	private EmailSentStatusEnum(int value, String name) {
		super(name, value);
	}

	public static EmailSentStatusEnum[] enumerate() {
		return (EmailSentStatusEnum[])enumerate(EmailSentStatusEnums);
	}

	public static final EmailSentStatusEnum toEnum(int value) {
		return (EmailSentStatusEnum)toEnum(EmailSentStatusEnums, value);
	}
	
	public static final int toInt(EmailSentStatusEnum enum) {
		return enum.getValue();
	}

}
