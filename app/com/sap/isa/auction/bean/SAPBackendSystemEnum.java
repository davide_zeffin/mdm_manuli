/*
 * Created on Mar 3, 2004
 *
 * To change the template for this generated file go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
package com.sap.isa.auction.bean;

import java.io.Serializable;

/**
 * Title:			Asset Remarketing
 * Description:
 * @Copyright: 		Copyright (c) 2004
 * Company:			SAPLabs LLC
 * @author 
 *
 */
public class SAPBackendSystemEnum implements Serializable {
	private static final String STR_CRM = "CRM";
	private static final String STR_R3 = "R3";
	
	/** DONT CHANGE THESE VALUES*/
	private static final int INT_CRM = 0;
	private static final int INT_R3 = 1;
	
	public static final SAPBackendSystemEnum CRM = new SAPBackendSystemEnum(STR_CRM,INT_CRM);
	public static final SAPBackendSystemEnum R3 = new SAPBackendSystemEnum(STR_R3,INT_R3);
	
	private String name;
	private int value;

	private SAPBackendSystemEnum(String name, int value) {
		this.name = name;
		this.value = value;
	}
	
	public int getValue() {
		return value;
	}

	public String toString() {
		return "[" + name + " = " + value + "]";
	}

	public static final int toInt(SAPBackendSystemEnum enum) {
		return enum.getValue();
	}

	public static final SAPBackendSystemEnum toEnum(int value) {
		switch(value) {
			case INT_CRM: return CRM;
			case INT_R3: return R3;
			default: throw new IllegalArgumentException("Enum = " + value + " is invalid");
		}
	}

}
