/*
 * Created on Sep 10, 2003
 *
 */
package com.sap.isa.auction.bean;

import java.util.HashMap;
import java.io.Serializable;

/**
 * @author I802891
 *
 */
public class ChangeRecorder implements ChangeRecorderBean, Serializable {

	private static final String[] EMPTY_ARRAY = new String[0];
	protected HashMap changes = new HashMap();
	protected boolean recording = false;
	
	/* (non-Javadoc)
	 * @see com.sap.isa.auction.bean.ChangeRecorderBean#getModifiedAttributes()
	 */
	public String[] getModifiedAttributes() {
		if(recording) {
			String[] arr = new String[changes.size()];
			changes.keySet().toArray(arr);
			return arr;
		}
		else {
			return EMPTY_ARRAY;
		}
	}

	/* (non-Javadoc)
	 * @see com.sap.isa.auction.bean.ChangeRecorderBean#getNewAttributeValue(java.lang.String)
	 */
	public Object getNewAttributeValue(String attributeName) {
		if(recording) {
			return ((Object[])changes.get(attributeName))[1];
		}
		return null;
	}

	/* (non-Javadoc)
	 * @see com.sap.isa.auction.bean.ChangeRecorderBean#getOldAttributeValue(java.lang.String)
	 */
	public Object getOldAttributeValue(String attributeName) {
		if(recording && isModified(attributeName)) {
			return ((Object[])changes.get(attributeName))[0];					
		}
		
		return null;
	}

	/* (non-Javadoc)
	 * @see com.sap.isa.auction.bean.ChangeRecorderBean#isModified(java.lang.String)
	 */
	public boolean isModified(String attributeName) {
		if(recording) {
			return changes.containsKey(attributeName);
		}
		return false;
	}

	/* (non-Javadoc)
	 * @see com.sap.isa.auction.bean.ChangeRecorderBean#isRecording()
	 */
	public boolean isRecording() {
		return recording;
	}

	/* (non-Javadoc)
	 * @see com.sap.isa.auction.bean.ChangeRecorderBean#setAttributeValue(java.lang.String, java.lang.Object, java.lang.Object)
	 */
	public void setAttributeValue(
		String attributeName,
		Object oldValue,
		Object newValue) {
		
		if(recording) {
			if(isModified(attributeName)) {
				// only new value is modified
				((Object[])changes.get(attributeName))[1] = newValue;
			}
			else {
				changes.put(attributeName,new Object[]{oldValue,newValue});
			}
		}
	}

	/* (non-Javadoc)
	 * @see com.sap.isa.auction.bean.ChangeRecorderBean#startRecording()
	 */
	public void startRecording() {
		if(recording) endRecording();
		recording = true;
	}

	/* (non-Javadoc)
	 * @see com.sap.isa.auction.bean.ChangeRecorderBean#endRecording()
	 */
	public String[] endRecording() {
		if(recording) {
			String[] attrs = getModifiedAttributes();
			changes.clear();
			recording = false;
			return attrs;
		}
		return null;
	}

	/* 
	 * Semantics must be handled at the bean level
	 * @see com.sap.isa.auction.bean.ChangeRecorderBean#revertRecording()
	 */
	public void revertRecording() {
		endRecording();
	}
}
