/*
 * Created on Apr 21, 2004
 *
 * To change the template for this generated file go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
package com.sap.isa.auction.bean.b2x;

import java.util.*;

import com.sap.isa.auction.bean.Enumeration;

/**
 * @author I803746
 *
 * To change the template for this generated type comment go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
public class BidTypeEnum extends Enumeration {
	private static List BidTypeEnums = new ArrayList();
	
	private static final String STR_NONE = "NONE";
	private static final String STR_NORMAL = "NORMAL";
	private static final String STR_PRIVATE = "PRIVATE";
	private static final String STR_SEALED = "SEALED";    

	/** DONT CHANGE THESE VALUES AS THEY HAVE EBAY SEMANTICS*/
	public static final int INT_NONE = 100;
	public static final int INT_NORMAL = 101;
	public static final int INT_PRIVATE = 102;
	public static final int INT_SEALED = 103;    

	public static final BidTypeEnum NONE = new BidTypeEnum(STR_NONE,INT_NONE);
	public static final BidTypeEnum NORMAL = new BidTypeEnum(STR_NORMAL,INT_NORMAL);
	public static final BidTypeEnum PRIVATE = new BidTypeEnum(STR_PRIVATE,INT_PRIVATE);
	public static final BidTypeEnum SEALED = new BidTypeEnum(STR_SEALED,INT_SEALED);    

	static {
		addEnum(BidTypeEnums, NONE);
		addEnum(BidTypeEnums, NORMAL);
		addEnum(BidTypeEnums, PRIVATE);
		addEnum(BidTypeEnums, SEALED);
	}
	
	/**
	 * @param name
	 * @param value
	 */
	private BidTypeEnum(String name, int value) {
		super(name, value);
	}

	public static BidTypeEnum[] enumerate() {
		return (BidTypeEnum[])enumerate(BidTypeEnums);
	}

	public static final BidTypeEnum toEnum(int value) {
		return (BidTypeEnum)toEnum(BidTypeEnums, value);
	}
	
	public static void main(String[] args) {
		System.out.println(toEnum(BidTypeEnum.INT_NORMAL));
	}
}
