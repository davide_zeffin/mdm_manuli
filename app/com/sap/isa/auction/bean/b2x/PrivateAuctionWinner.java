/*
 * Created on Apr 21, 2004
 *
 * Title:        Internet Sales Asset Remarketing
 * Description:
 * Copyright:    Copyright (c) 2004
 * Company:      SAP Labs LLC
 * @author
 * @version 1.0
 */
package com.sap.isa.auction.bean.b2x;

import java.util.Date;

import com.sap.isa.auction.bean.Winner;

/**
 * @author I802850
 *
 * To change the template for this generated type comment go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
public class PrivateAuctionWinner extends Winner {
	
	private String webShopId;
	private boolean checkedOut = false;
	private String soldToParty;
	private double quantity;
	private Date createdDate;
	/**
	 * @return
	 */
	public String getWebShopId() {
		return webShopId;
	}

	/**
	 * @param string
	 */
	public void setWebShopId(String string) {
		webShopId = string;
	}

	/**
	 * @return
	 */
	public boolean isCheckedOut() {
		return checkedOut;
	}

	/**
	 * @param b
	 */
	public void setCheckedOut(boolean b) {
		checkedOut = b;
	}

	/**
	 * @return
	 */
	public String getSoldToParty() {
		return soldToParty;
	}

	/**
	 * @param string
	 */
	public void setSoldToParty(String string) {
		soldToParty = string;
	}


}
