/*
 * Created on Apr 21, 2004
 *
 * To change the template for this generated file go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
package com.sap.isa.auction.bean.b2x;

import java.util.*;

import com.sap.isa.auction.bean.Enumeration;

/**
 * @author I803746
 *
 * To change the template for this generated type comment go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
public class SealedBidTypeEnum extends Enumeration {
	private static List SealedBidTypeEnums = new ArrayList();
	
	private static final String STR_NONE = "NONE";

	public static final int INT_NONE = 200;

	public static final SealedBidTypeEnum NONE = new SealedBidTypeEnum(STR_NONE,INT_NONE);

	static {
		addEnum(SealedBidTypeEnums, NONE);
	}
	
	/**
	 * @param name
	 * @param value
	 */
	protected SealedBidTypeEnum(String name, int value) {
		super(name, value);
	}

	public static SealedBidTypeEnum[] enumerate() {
		return (SealedBidTypeEnum[])enumerate(SealedBidTypeEnums);
	}

	public static final SealedBidTypeEnum toEnum(int value) {
		return (SealedBidTypeEnum)toEnum(SealedBidTypeEnums, value);
	}
}
