/*
 * Created on Apr 21, 2004
 *
 * Title:        Internet Sales Asset Remarketing
 * Description:
 * Copyright:    Copyright (c) 2004
 * Company:      SAP Labs LLC
 * @author
 * @version 1.0
 */
package com.sap.isa.auction.bean.b2x;

import com.sap.isa.auction.bean.AuctionItem;
import com.sap.isa.auction.exception.i18n.ErrorMessageBundle;

/**
 * @author I802850
 *
 * To change the template for this generated type comment go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
public class PrivateAuctionItem extends AuctionItem{

	public static final String PRODUCTAREA_ID = "productAreaId";
	public static final String PRODUCTNAME = "productName";

	public static final int MAX_PRODUCTID_LENGTH = 255;
	public static final int MAX_PRODUCTAREAID_LENGTH = 255;
		
	private String productId;
	private String productAreaId;

	/**
	 * @return
	 */
	public String getProductAreaId() {
		return productAreaId;
	}

	/**
	 * @return
	 */
	public String getProductId() {
		return productId;
	}

	/**
	 * @param string
	 */
	public void setProductAreaId(String string) {
		productAreaId = string;
	}

	/**
	 * @param string
	 */
	public void setProductId(String string) {
		productId = string;
	}

	public boolean validate() {
		super.clearErrors();
		boolean bool = super.validate();
		ErrorMessageBundle bundle = ErrorMessageBundle.getInstance();

		if(super.getErrors().size() > 0) return false;
		else return true;
	}
}
