package com.sap.isa.auction.bean.b2x;

import java.io.ObjectInputStream;
import java.math.BigDecimal;
import java.util.ArrayList;

import com.sap.isa.auction.bean.Auction;
import com.sap.isa.auction.bean.AuctionItem;
import com.sap.isa.auction.bean.AuctionTypeEnum;
import com.sap.isa.auction.bean.ChangeRecorder;
import com.sap.isa.auction.bean.ValidatingBean;
import com.sap.isa.auction.businessobject.ValidationException;
import com.sap.isa.auction.exception.i18n.ErrorMessageBundle;
import com.sap.isa.auction.exception.i18n.I18nErrorMessage;
import com.sap.isa.auction.util.TrackedArrayList;

/**
 * Title:
 * Description:
 * Copyright:    Copyright (c) 2003
 * Company:
 * @author
 * @version 1.0
 */

public class PrivateAuction extends Auction {
	
	private BidTypeEnum bidType;
	private AuctionVisibilityEnum visibility;
	private WinnerDeterminationTypeEnum winnerTypeEnum = WinnerDeterminationTypeEnum.AUTOMATIC;
	
	private BigDecimal buyItNowPrice;
	private BigDecimal bidIncrement;
	
	private int quantityPerBundle;
	/**
	 * Simple Auction bean state validation checks
	 */
	public boolean validate(boolean deep) {
		super.clearErrors();
		super.clearWarnings();
		super.clearInvalidChildren();
		
		ErrorMessageBundle bundle = ErrorMessageBundle.getInstance();
		
		// First character must be a letter in a valid name
		if(name == null || name.length() == 0 || !Character.isLetter(name.charAt(0))) {
			super.addError(bundle.newI18nErrorMessage(ValidationException.INVALID_NAME));
		}

		// Max length allowed for name is 80
		if(name != null && name.length() > MAX_NAME_LENGTH) {
			I18nErrorMessage error = bundle.newI18nErrorMessage(ValidationException.INVALID_NAME_LENGTH);
			error.setArguments(new Object[]{Integer.toString(MAX_NAME_LENGTH)});
			super.addError(error);
		}

		// nps:: start date and end dates are not mandatory
		/*
		if(startDate == null) {
			super.addError(bundle.newI18nErrorMessage(ValidationException.INVALID_START_DATE));
		}

		if(endDate == null) {
			super.addError(bundle.newI18nErrorMessage(ValidationException.INVALID_END_DATE));
		}
		else {
			if(startDate != null && endDate.before(startDate)) {
				super.addError(bundle.newI18nErrorMessage(ValidationException.END_DATE_BEFORE_START_DATE));
			}

			if(endDate.before(new java.sql.Timestamp(System.currentTimeMillis()))) {
				super.addError(bundle.newI18nErrorMessage(ValidationException.END_DATE_BEFORE_NOW));
			}
		}*/
        
		if(currencyCode == null) {
			I18nErrorMessage error = bundle.newI18nErrorMessage(ValidationException.MISSING_CURRENCY);
			super.addError(error);
		}

		if(duration == null || (duration[2]==0 && duration[1]==0 && duration[0]==0)) {
			I18nErrorMessage error = bundle.newI18nErrorMessage(ValidationException.INVALID_DURATION);
			super.addError(error);
		}

		//Quotation expiration is no longer set from the user perspective.
		//Quotation is valid untill the buyer checkout
		/*if(quotDuration == null) {
			I18nErrorMessage error = bundle.newI18nErrorMessage(ValidationException.INVALID_DURATION);
			super.addError(error);
		}*/

		// check that the Quotation expiration millis are more than Auction Duration millis
		/*if(duration != null && quotDuration != null) {
			if(DateTimeUtil.toTimeInMillis(duration[0],duration[1],duration[2]) >=
			   DateTimeUtil.toTimeInMillis(quotDuration[0],quotDuration[1],quotDuration[2])) {
				super.addError(bundle.newI18nErrorMessage(ValidationException.QUOT_EXP_DATE_BEFORE_END_DATE));
			}
		}*/

		//Shop validation is not required, because we don't store it 
		if(webShopId == null) {
			super.addError(bundle.newI18nErrorMessage(ValidationException.INVALID_WEBSHOPID));
		}

		// Max length for web shop Id is 10
		if(webShopId != null && webShopId.length() > MAX_WEBSHOPID_LENGTH) {
			super.addError(bundle.newI18nErrorMessage(ValidationException.INVALID_WEBSHOPID));
		}

		// description must be provided
//		if(desc == null || desc.length() == 0) {
//			I18nErrorMessage error = bundle.newI18nErrorMessage(ValidationException.INVALID_DESCRIPTION);
//			super.addError(error);
//		}

		// Max of 10*1024 unicode chars are allowed
//		if(desc != null && desc.length() > MAX_DESC_DATA_LENTH) {
//			I18nErrorMessage error = bundle.newI18nErrorMessage(ValidationException.INVALID_DESCRIPTION_LENGTH);
//			error.setArguments(new Object[]{Integer.toString(MAX_DESC_DATA_LENTH)});
//			super.addError(error);
//		}

		if(startPrice == null || startPrice.doubleValue() <= 0) {
			I18nErrorMessage error = bundle.newI18nErrorMessage(ValidationException.INVALID_START_PRICE);
			super.addError(error);
		}
		
		if(reservePrice != null && reservePrice.compareTo(startPrice) <= 0) {
			I18nErrorMessage error = bundle.newI18nErrorMessage(ValidationException.INVALID_RESERVE_PRICE);
			super.addError(error);
		}

		if(type == AuctionTypeEnum.toInt(AuctionTypeEnum.BROKEN_LOT) && getQuantity() <= 0) {
			I18nErrorMessage error = bundle.newI18nErrorMessage(ValidationException.INVALID_AUCTION_QUANTITY);
			error.setArguments(new Object[]{Integer.toString(getQuantity())});
			super.addError(error);
		}		

		if( visibility == AuctionVisibilityEnum.PROTECTED ) {
			if((getTargetGroups() == null || getTargetGroups().size()<=0) && 
				(getBusinessPartners() == null || getBusinessPartners().size()<=0))
				super.addError(bundle.newI18nErrorMessage(ValidationException.NO_TARGETGRP_OR_BP));
		}
				
		if(items == null || items.size() == 0) {
			super.addError(bundle.newI18nErrorMessage(ValidationException.NO_AUCTION_ITEMS_ERROR));
		}

		boolean itemError = false;
		if(deep && items != null) {
			ValidatingBean[] invalidItems = new ValidatingBean[items.size()];
			for(int i = 0 ; i < items.size(); i++) {
				AuctionItem item = (AuctionItem)items.get(i);
				item.clearErrors();
				if(!item.validate()) {
					super.addInvalidChild(item);
					itemError = true;
				}
			}
		}

		if(deep) {
			return !(super.getErrors().size() > 0 || itemError);
		}
		else {
			return super.getErrors().size() == 0;
		}
	}
	
    public boolean equals(Object o) {
    	if (o == null) {
    		return false;
    	}
    	return super.equals(o);
    }


	public int hashCode() {
		if(auctionId != null)
			return auctionId.hashCode();
		else
			return super.hashCode();
	}

	public Object clone() {

		return super.clone();
	}
    
	// Deserialization
	private void readObject(ObjectInputStream in) 
		throws java.io.IOException, ClassNotFoundException {

		in.defaultReadObject();
		this.recorder = new ChangeRecorder();
	}
    
	////////////////////////////////////////////////////
	// recording specific, only relation specific attributs
	// are tracked
	
	/* (non-Javadoc)
	 * @see com.sapmarkets.isa.auction.bean.ChangeRecorderBean#getModifiedAttributes()
	 */
	public String[] getModifiedAttributes() {
		return recorder.getModifiedAttributes();
	}

	/* (non-Javadoc)
	 * @see com.sapmarkets.isa.auction.bean.ChangeRecorderBean#getNewAttributeValue(java.lang.String)
	 */
	public Object getNewAttributeValue(String attributeName) {
		return recorder.getNewAttributeValue(attributeName);
	}

	/* (non-Javadoc)
	 * @see com.sapmarkets.isa.auction.bean.ChangeRecorderBean#getOldAttributeValue(java.lang.String)
	 */
	public Object getOldAttributeValue(String attributeName) {
		return recorder.getOldAttributeValue(attributeName);
	}

	/* (non-Javadoc)
	 * @see com.sapmarkets.isa.auction.bean.ChangeRecorderBean#isModified(java.lang.String)
	 */
	public boolean isModified(String attributeName) {
		return recorder.isModified(attributeName);
	}

	/* (non-Javadoc)
	 * @see com.sapmarkets.isa.auction.bean.ChangeRecorderBean#isRecording()
	 */
	public boolean isRecording() {
		return recorder.isRecording();
	}

	/* 
	 * Privileged operation
	 * @see com.sapmarkets.isa.auction.bean.ChangeRecorderBean#startRecording()
	 */
	public void startRecording() {
		if(recorder.isModified(ATTR_ITEMS)) {
			((TrackedArrayList)items).commit();
		}		
		recorder.startRecording();
	}
	
	/* 
	 * Privileged operation
	 * @see com.sapmarkets.isa.auction.bean.ChangeRecorderBean#endRecording()
	 */
	public String[] endRecording() {
		if(recorder.isModified(ATTR_ITEMS)) {
			((TrackedArrayList)items).commit();
		}
		return recorder.endRecording();
	}
	
	/* (non-Javadoc)
	 * @see com.sapmarkets.isa.auction.bean.ChangeRecorderBean#revertRecording()
	 */
	public void revertRecording() {
		if(recorder.isRecording()) {
			if(recorder.isModified(ATTR_ITEMS)) {			
				((TrackedArrayList)items).revert();
			}			
			String[] attrs = recorder.getModifiedAttributes();
			for(int i = 0; i < attrs.length; i++) {
				String attr = attrs[i];
				setAttribute(attr,recorder.getOldAttributeValue(attr));
			}
			recorder.revertRecording();
		}
	}

	/**
	 * 
	 * @param attr
	 * @param value
	 */
	protected void setAttribute(String attr, Object value) {
		if(attr.equals(ATTR_ITEMS)) {
			setItems((ArrayList)value);
		}
	}

	public BidTypeEnum getBidType() {
		return bidType;
	}

	public void setBidType(BidTypeEnum arg0) {
		bidType = arg0;
	}
	
	public void setBidType(int arg0) throws IllegalArgumentException {
		bidType = BidTypeEnum.toEnum(arg0);
	}
	
	public AuctionVisibilityEnum getVisibilityType() {
		return visibility;
	}

	public void setVisibilityType(AuctionVisibilityEnum arg0) {
		visibility = arg0;
	}
	
	public void setVisibilityType(int arg0) throws IllegalArgumentException {
		visibility = AuctionVisibilityEnum.toEnum(arg0);
	}

	//Decimal get
	/**
	 * @return
	 */
	public BigDecimal getBidIncrement() {
		return bidIncrement;
	}

	/**
	 * @return
	 */
	public BigDecimal getBuyItNowPrice() {
		return buyItNowPrice;
	}

	/**
	 * @param decimal
	 */
	public void setBidIncrement(BigDecimal decimal) {
		bidIncrement = decimal;
	}

	/**
	 * @param decimal
	 */
	public void setBuyItNowPrice(BigDecimal decimal) {
		buyItNowPrice = decimal;
	}



	/**
	 * @return
	 */
	public WinnerDeterminationTypeEnum getWinnerTypeEnum() {
		return winnerTypeEnum;
	}

	/**
	 * @param enum
	 */
	public void setWinnerTypeEnum(WinnerDeterminationTypeEnum enum) {
		winnerTypeEnum = enum;
	}

	public int getTotalQuantityOfOneBundle()
	{
		return quantityPerBundle;
	}

	public void setTotalQuantityOfOneBundle(int q)
	{
		quantityPerBundle = q;
	}
}