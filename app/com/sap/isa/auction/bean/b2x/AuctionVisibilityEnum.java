/*
 * Created on Apr 21, 2004
 *
 * To change the template for this generated file go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
package com.sap.isa.auction.bean.b2x;

import java.util.*;

import com.sap.isa.auction.bean.Enumeration;

/**
 * @author I803746
 *
 * To change the template for this generated type comment go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
public class AuctionVisibilityEnum extends Enumeration {
	private static List AuctionVisibilityEnums = new ArrayList();
	
	private static final String STR_NONE = "NONE";
	private static final String STR_PROTECTED = "PROTECTED";
	private static final String STR_PUBLIC = "PUBLIC";

	public static final int INT_NONE = 300;
	public static final int INT_PROTECTED = 301;
	public static final int INT_PUBLIC = 302;

	public static final AuctionVisibilityEnum NONE = new AuctionVisibilityEnum(STR_NONE,INT_NONE);
	public static final AuctionVisibilityEnum PROTECTED = new AuctionVisibilityEnum(STR_PROTECTED,INT_PROTECTED);
	public static final AuctionVisibilityEnum PUBLIC = new AuctionVisibilityEnum(STR_PUBLIC,INT_PUBLIC);

	static {
		addEnum(AuctionVisibilityEnums, NONE);
		addEnum(AuctionVisibilityEnums, PROTECTED);
		addEnum(AuctionVisibilityEnums, PUBLIC);
	}

	/**
	 * @param name
	 * @param value
	 */
	public AuctionVisibilityEnum(String name, int value) {
		super(name, value);
	}

	public static AuctionVisibilityEnum[] enumerate() {
		return (AuctionVisibilityEnum[])enumerate(AuctionVisibilityEnums);
	}

	public static final AuctionVisibilityEnum toEnum(int value) {
		return (AuctionVisibilityEnum)toEnum(AuctionVisibilityEnums, value);
	}
}
