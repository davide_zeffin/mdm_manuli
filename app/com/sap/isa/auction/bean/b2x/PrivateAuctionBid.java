/*
 * Created on Apr 21, 2004
 *
 * Title:        Internet Sales Asset Remarketing
 * Description:
 * Copyright:    Copyright (c) 2004
 * Company:      SAP Labs LLC
 * @author
 * @version 1.0
 */
package com.sap.isa.auction.bean.b2x;

import com.sap.isa.auction.bean.Bid;

/**
 * @author I802850
 *
 * To change the template for this generated type comment go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
public class PrivateAuctionBid extends Bid{
	private String webShopId;
	private String soldToParty;
	/**
	 * @return
	 */
	public String getWebShopId() {
		return webShopId;
	}

	/**
	 * @param string
	 */
	public void setWebShopId(String string) {
		webShopId = string;
	}
	

	/**
	 * @return
	 */
	public String getSoldToParty() {
		return soldToParty;
	}

	/**
	 * @param string
	 */
	public void setSoldToParty(String string) {
		soldToParty = string;
	}

}