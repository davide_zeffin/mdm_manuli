/*
 * Created on Apr 21, 2004
 *
 * To change the template for this generated file go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
package com.sap.isa.auction.bean.b2x;

import java.util.ArrayList;
import com.sap .isa.auction.bean.Enumeration;

import java.util.List;

/**
 *
 * To change the template for this generated type comment go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
public class WinnerDeterminationTypeEnum extends Enumeration {
	private static List BidTypeEnums = new ArrayList();
	
	private static final String STR_NONE = "NONE";
	private static final String STR_AUTOMATIC = "AUTOMATIC";
	private static final String STR_MANUAL = "MANUAL";
   

	public static final int INT_NONE = 100;
	public static final int INT_AUTOMATIC = 101;
	public static final int INT_MANUAL = 102;
   

	public static final WinnerDeterminationTypeEnum NONE = new WinnerDeterminationTypeEnum(STR_NONE,INT_NONE);
	public static final WinnerDeterminationTypeEnum AUTOMATIC = new WinnerDeterminationTypeEnum(STR_AUTOMATIC,INT_AUTOMATIC);
	public static final WinnerDeterminationTypeEnum MANUAL = new WinnerDeterminationTypeEnum(STR_MANUAL,INT_MANUAL);
    

	static {
		addEnum(BidTypeEnums, NONE);
		addEnum(BidTypeEnums, AUTOMATIC);
		addEnum(BidTypeEnums, MANUAL);
	}
	
	/**
	 * @param name
	 * @param value
	 */
	private WinnerDeterminationTypeEnum(String name, int value) {
		super(name, value);
	}

	public static WinnerDeterminationTypeEnum[] enumerate() {
		return (WinnerDeterminationTypeEnum[])enumerate(BidTypeEnums);
	}

	public static final WinnerDeterminationTypeEnum toEnum(int value) {
		return (WinnerDeterminationTypeEnum)toEnum(BidTypeEnums, value);
	}
	
	public static void main(String[] args) {
		System.out.println(toEnum(WinnerDeterminationTypeEnum.INT_AUTOMATIC));
	}
}
