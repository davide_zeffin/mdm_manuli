/*
 * Created on Feb 19, 2004
 *
 */
package com.sap.isa.auction.bean;

/**
 * @author I802891
 *
 */
public class AppVersionInfo {
	private String appName;
	private String majorVer;
	private String minorVer;	
	private String[] dbVersions;
	
	public String getAppName() {
		return appName;
	}

	public void setAppName(String name) {
		this.appName = name;
	}

	public String getMajorVersion() {
		return majorVer;
	}
	
	public String getMinorVersion() {
		return minorVer;
	}
	
	public void setMajorVersion(String ver) {
		this.majorVer = ver;
	}
	
	public void setMinorVersion(String ver) {
		this.minorVer = ver;
	}
	
	public String[] getSupportedDBVersions() {
		return dbVersions;
	}
	
	public void setSupportedDBVersions(String[] vers) {
		this.dbVersions = vers;
	}
}
