package com.sap.isa.auction.bean;

/**
 * Title:        eBay Integration Project
 * Description:
 * Copyright:    Copyright (c) 2003
 * Company:      SAP Labs LLC
 * @author
 * @version 1.0
 */
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import com.sap.isa.core.Iterable;
import com.sap.isa.core.logging.IsaLocation;

/**
 * Represents a List of <code>Item</code> objects. This class can be used to
 * maintain a collection of such objects.
 * The internal storage is organized using a List, so duplicates of items
 * are allowd.
 *
 * @stereotype collection
 */

public class AuctionItemList implements Iterable, Cloneable {
    private List items;
    private IsaLocation log;

    /**
     * Creates a new <code>ItemList</code> object.
     */
    public AuctionItemList() {
        items = new ArrayList();
        // Remember to change the cast in the clone() method when you
        // switch to another implementor of List!!
        log = IsaLocation.getInstance(this.getClass().getName());
    }

    /**
     * Adds a new <code>Item</code> to the List.
     *
     * @param item Item to be stored in <code>ItemList</code>
     */
    public void add(AuctionItem item) {
        items.add(item);
    }

    /**
     * Returns the element at the specified position in this list.
     *
     * @param index index of element to return
     * @return the element at the specified position in this list
     */
    public AuctionItem get(int index) {
        return (AuctionItem) items.get(index);
    }
    /**
     * Removes all entries.
     */
    public void clear() {
        items.clear();
    }

    /**
     * Returns the number of elements in this list.
     *
     * @return the number of elements in this list.
     */
    public int size() {
        return items.size();
    }

    /**
     * Returns true if this list contains no data.
     *
     * @return <code>true</code> if list contains no data.
     */
    public boolean isEmpty() {
        return items.isEmpty();
    }

    /**
     * Returns true if this list contains the specified element.
     *
     * @param value value whose presence in this list is to be tested.
     * @return <code>true</code> if this list contains the specified value.
     */
    public boolean contains(AuctionItem value) {
        return items.contains(value);
    }


    /**
     * Returns an iterator over the elements contained in the
     * <code>SoldToList</code>.
     *
     * @return Iterator for this object
     */
    public Iterator iterator() {
        return items.iterator();
    }


    /**
     * Remove the element at the specified position in this list.
     *
     * @param index index of element to remove from the list
     */
    public void remove(int index) {
        items.remove(index);
    }

}