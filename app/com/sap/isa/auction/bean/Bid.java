package com.sap.isa.auction.bean;

import com.sap.isa.auction.exception.i18n.*;
import com.sap.isa.auction.businessobject.ValidationException;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.io.Serializable;

/**
 * Title:
 * Description:
 * Copyright:    Copyright (c) 2003
 * Company:
 * @author
 * @version 1.0
 */

public class Bid extends ValidatingBean implements Serializable {

    public Bid() {
    }

    private String auctionId;
    private String bidId;
    private String buyerId;
    private Timestamp bidDate;
    private BigDecimal bidAmount;
    private String currencyCode;
    private int quantity = 0;   // sales units
	private String client;
	private String systemId;
	private String userId;
	private Timestamp createdDate;
	
    public String getBidId() {
        return bidId;
    }

    public String getAuctionId() {
        return auctionId;
    }

    public String getBuyerId() {
        return buyerId;
    }

    public Timestamp getBidDate() {
        return bidDate;
    }

    /**
     * For full lot, Total Bid Amount for Full Lot is specified
     */
    public BigDecimal getBidAmount() {
        return this.bidAmount;
    }

    public int getQuantity() {
        return this.quantity;
    }

    public void setBidId(String bidId) {
        this.bidId = bidId;
    }

    public void setAuctionId(String auctionId) {
        this.auctionId = auctionId;
    }

    public void setBuyerId(String buyerId) {
        this.buyerId = buyerId;
    }

    public void setBidDate(Timestamp bidDate) {
        this.bidDate = bidDate;
    }

    /**
     * For Full Lot it is the Full Bid Amount
     */
    public void setBidAmount(BigDecimal bidAmount) {
        this.bidAmount = bidAmount;
    }

    public void setQuantity(int arg) {
        this.quantity = arg;
    }

    public void setCurrency(String code) {
        this.currencyCode = code;
    }

    public String getCurrency() {
        return this.currencyCode;
    }


    public boolean validate(boolean deep) {
        super.clearErrors();
		
		ErrorMessageBundle bundle = ErrorMessageBundle.getInstance();
		
        if(auctionId == null) {
            super.addError(bundle.newI18nErrorMessage(ValidationException.INVALID_AUCTION));
        }

        if(quantity < 0) {
            super.addError(ValidationException.INVALID_QUANTITY);
        }

        if(buyerId == null) {
            super.addError(bundle.newI18nErrorMessage(ValidationException.INVALID_WINNER_BUYERID));
        }

        if(bidAmount == null || bidAmount.doubleValue() <= 0.000000000) {
            super.addError(bundle.newI18nErrorMessage(ValidationException.INVALID_BIDAMOUNT));
        }

        if(super.getErrors().size() > 0) return false;
        else return true;
    }

    /**
     * @return
     */
    public String getClient() {
        return client;
    }


    /**
     * @return
     */
    public String getSystemId() {
        return systemId;
    }

    /**
     * @param string
     */
    public void setClient(String string) {
        client = string;
    }



    /**
     * @param string
     */
    public void setSystemId(String string) {
        systemId = string;
    }

	/**
	 * @return
	 */
	public String getUserId() {
		return userId;
	}

	/**
	 * @param string
	 */
	public void setUserId(String string) {
		userId = string;
	}

	/**
	 * @return
	 */
	public Timestamp getCreatedDate() {
		return createdDate;
	}

	/**
	 * @param timestamp
	 */
	public void setCreatedDate(Timestamp timestamp) {
		createdDate = timestamp;
	}

}