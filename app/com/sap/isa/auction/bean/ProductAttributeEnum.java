/*
 * Created on Oct 17, 2003
 *
 * To change the template for this generated file go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
package com.sap.isa.auction.bean;

/**
 * @author I803067
 *
 * To change the template for this generated type comment go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
public interface ProductAttributeEnum {

	public static final String NAME = "productName";
	public static final String DESCRIPTION = "productDescription";
	public static final String IMAGEURL = "imageURL";
	public static final String PARTNUMBER = "partNumber";
	public static final String BASEUOM = "baseUOM";
	public static final String MATGROUP = "matGroup";
	public static final String OLDMATNUMBER = "oldMatNumber";
	public static final String GROSSWEIGHT = "grossWeight";
	public static final String WEIGHTUNIT = "weightUnit";
	public static final String NETWEIGHT = "netWeight";
	public static final String VOLUME = "volume";
	public static final String VOLUMEUNIT = "volumeUnit";
	public static final String DIMENSIONS = "dimensions";
	public static final String EAN = "ean";
	public static final String EANCATEGORY = "eanCategory";
	public static final String BASICTEXT = "basicText";
	
	
}
