/*
 * Created on Feb 19, 2004
 *
 */
package com.sap.isa.auction.bean;

/**
 * @author I802891
 *
 */
public class DBVersionInfo {
	
	private String dbId;
	private String dbVersion;
	
	public String getDBIdentifier() {
		return dbId;
	}

	public void setDBIdentifier(String id) {
		this.dbId = id;
	}
	
	public String getDBSchemaVersion() {
		return dbVersion;
	}
	
	public void setDBSchemaVersion(String ver) {
		this.dbVersion = ver;
	}
}

