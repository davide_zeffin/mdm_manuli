package com.sap.isa.auction.bean;

/**
 * Title:        Internet Sales
 * Description:
 * Copyright:    Copyright (c) 2002
 * Company:      SAPMarkets
 * @author
 * @version 1.0
 */

public class RoleTypeEnum {

	/**
	 * These int values should not be modified as they are mapped to database
	 */

	private static final int INT_SELLER        = 0;
	private static final int INT_BUYER       = 1;
	private static final int INT_ADMIN        = 2;
	private static final int INT_ADMIN_SELLER        = 3;
	
	private static final String STR_SELLER         = "SELLER";
	private static final String STR_BUYER        = "BUYER";
	private static final String STR_ADMIN         = "ADMIN";
	private static final String STR_ADMIN_SELLER         = "ADMIN_SELLER";

	public static final RoleTypeEnum SELLER =
		new RoleTypeEnum(INT_SELLER,STR_SELLER);

	public static final RoleTypeEnum ADMIN =
		new RoleTypeEnum(INT_ADMIN,STR_ADMIN);

	public static final RoleTypeEnum ADMIN_SELLER =
		new RoleTypeEnum(INT_ADMIN_SELLER,STR_ADMIN_SELLER);

	public static final RoleTypeEnum BUYER =
		new RoleTypeEnum(INT_BUYER,STR_BUYER);


	private String name;
	private int value;

	private RoleTypeEnum(int value, String name) {
		this.name = name;
		this.value = value;
	}

	public int getValue() {
		return value;
	}

	public String toString() {
		return "[" + name + " = " + value + "]";
	}

	public static final int toInt(RoleTypeEnum enum) {
		return enum.getValue();
	}

	public static final RoleTypeEnum toEnum(int value) {
		switch(value) {
			case INT_SELLER: return SELLER;
			case INT_ADMIN: return ADMIN;
			case INT_BUYER: return BUYER;
			case INT_ADMIN_SELLER: return ADMIN_SELLER;
			default: throw new IllegalArgumentException("Enum = " + value + " is invalid");
		}
	}
}