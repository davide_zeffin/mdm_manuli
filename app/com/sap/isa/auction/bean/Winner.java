package com.sap.isa.auction.bean;

import java.io.Serializable;
import java.math.BigDecimal;
import java.sql.Timestamp;

import com.sap.isa.auction.backend.boi.WinnerData;
import com.sap.isa.auction.businessobject.ValidationException;
import com.sap.isa.auction.exception.i18n.ErrorMessageBundle;
import com.sap.isa.core.TechKey;

/**
 * Title:
 * Description:
 * Copyright:    Copyright (c) 2003
 * Company:
 * @author
 * @version 1.0
 */

public class Winner extends ValidatingBean implements WinnerData,Serializable {
 
    private String winnerId;
    private String auctionId;
    private String bidId;

    // CRM Business Partner
    private String buyerId;
    // CRM user Id
    private String userId;

    // For Full Lot, total winning Amount, For Broken Lot Price per Unit
    private BigDecimal bidAmount;

    // order Id in CRM system
    private String orderId;

	// order Id in CRM system
	private TechKey orderGuid;
    
    private String client;
    private String systemId;
    
	private BigDecimal quantity;
	
	private WinnerStatusEnum winnerStatusEnum = WinnerStatusEnum.NONE;
	private Timestamp createdDate;
	private Timestamp lastModifiedDate;

    public Winner() {
    }

    public String getWinnerId() {
        return winnerId;
    }

    public String getAuctionId() {
        return auctionId;
    }

    public String getBidId() {
        return bidId;
    }

    public String getBuyerId() {
        return buyerId;
    }

    public String getUserId() {
        return this.userId;
    }



    public BigDecimal getBidAmount() {
        return bidAmount;
    }

    public String getOrderId() {
        return orderId;
    }

    public void setWinnerId(String winnerId) {
        this.winnerId = winnerId;
    }

    public void setAuctionId(String auctionId) {
        this.auctionId = auctionId;
    }

    public void setBidId(String responseId) {
        this.bidId = responseId;
    }

    public void setBuyerId(String buyerId) {
        this.buyerId = buyerId;
    }

    public void setUserId(String crmUserId) {
        this.userId = crmUserId;
    }


    public void setBidAmount(BigDecimal bidAmount) {
        this.bidAmount = bidAmount;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    /**
     * Attribute level validation
     */
    public boolean validate(boolean deep) {
        super.clearErrors();
		
		ErrorMessageBundle bundle = ErrorMessageBundle.getInstance();
		
        if(auctionId == null) {
            super.addError(bundle.newI18nErrorMessage(ValidationException.INVALID_AUCTION));
        }



        if(userId == null) {
            super.addError(bundle.newI18nErrorMessage(ValidationException.INVALID_WINNER_USERID));
        }

        if(buyerId == null) {
            super.addError(bundle.newI18nErrorMessage(ValidationException.INVALID_WINNER_BUYERID));
        }

        if(bidAmount == null || bidAmount.doubleValue() <= 0.000000000) {
            super.addError(bundle.newI18nErrorMessage(ValidationException.INVALID_BIDAMOUNT));
        }
		if (quantity!=null && quantity.intValue() < 0) {
			super.addError(ValidationException.INVALID_QUANTITY);
		}
        if(super.getErrors().size() > 0) return false;
        else return true;
    }

    /**
     * @return
     */
    public String getClient() {
        return client;
    }

    /**
     * @return
     */
    public String getSystemId() {
        return systemId;
    }

    /**
     * @param string
     */
    public void setClient(String string) {
        client = string;
    }

    /**
     * @param string
     */
    public void setSystemId(String string) {
        systemId = string;
    }

	/**
	 * @return
	 */
	public TechKey getOrderGuid() {
		return orderGuid;
	}

	/**
	 * @param string
	 */
	public void setOrderGuid(TechKey guid) {
		orderGuid = guid;
	}

	/* (non-Javadoc)
	 * @see com.sap.isa.auction.backend.boi.WinnerData#getAuctionListingId()
	 */
	public String getAuctionListingId() {
		// TODO Auto-generated method stub
		return null;
	}

	/* (non-Javadoc)
	 * @see com.sap.isa.auction.backend.boi.WinnerData#getBidListingId()
	 */
	public String getBidListingId() {
		// TODO Auto-generated method stub
		return null;
	}

	/* (non-Javadoc)
	 * @see com.sap.isa.auction.backend.boi.WinnerData#getQuantity()
	 */
	public BigDecimal getQuantity() {
		// TODO Auto-generated method stub
		return quantity;
	}
	
	public void setQuantity(int i) {
		quantity = new BigDecimal(i);
	}
	
	public void setQuantity(BigDecimal qty) {
		quantity = qty;
	}

	/**
	 * @return
	 */
	public WinnerStatusEnum getWinnerStatusEnum() {
		return winnerStatusEnum;
	}

	/**
	 * @param enum
	 */
	public void setWinnerStatusEnum(WinnerStatusEnum enum) {
		winnerStatusEnum = enum;
	}

	/**
	 * @return
	 */
	public Timestamp getCreatedDate() {
		return createdDate;
	}

	/**
	 * @return
	 */
	public Timestamp getLastModifiedDate() {
		return lastModifiedDate;
	}

	/**
	 * @param timestamp
	 */
	public void setCreatedDate(Timestamp timestamp) {
		createdDate = timestamp;
	}

	/**
	 * @param timestamp
	 */
	public void setLastModifiedDate(Timestamp timestamp) {
		lastModifiedDate = timestamp;
	}

}