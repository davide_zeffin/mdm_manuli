package com.sap.isa.auction.bean;

import java.io.ObjectInputStream;
import java.io.Serializable;
import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

import com.sap.isa.auction.backend.boi.AuctionData;
import com.sap.isa.auction.backend.boi.SalesAreaData;
import com.sap.isa.auction.businessobject.ValidationException;
import com.sap.isa.auction.exception.i18n.ErrorMessageBundle;
import com.sap.isa.auction.exception.i18n.I18nErrorMessage;
import com.sap.isa.auction.util.DateTimeUtil;
import com.sap.isa.auction.util.TrackedArrayList;
import com.sap.isa.businessobject.Shop;

/**
 * Title:
 * Description:
 * Copyright:    Copyright (c) 2003
 * Company:
 * @author
 * @version 1.0
 */

public class Auction extends ValidatingBean implements AuctionData, ChangeRecorderBean, Cloneable, Serializable  {

	public static final String ATTR_ITEMS = "items";

	protected static final int MAX_NAME_LENGTH = 80;
    protected int MAX_DESC_DATA_LENTH = 500*1000; // ~500KB
	protected static final int MAX_WEBSHOPID_LENGTH = 80;

	public static final String ATTR_TGT_GRPS = "targetGrps";
	public static final String ATTR_BPS = "businessPartners";


	private ArrayList targetGroups = new ArrayList(); // Target Group
	private ArrayList businessPartners = new ArrayList(); // Business Partners
	
	private ExecutionStatusEnum execStatus; // 
	private transient Exception execException; // for asychronours/failsafe operations
	private String execRequestId; // internal
	private int quantity;
	private int quantityRemaining;
	private String imageURL;
	
	
    public Auction() {
    }

    protected String auctionId;
	protected String title;    
    protected boolean template = false;
    protected String name;
	protected String currencyCode;
	protected String countryCode;
    
    // Descriptions can be a huge data
    // They can contain HTML or Structured information
    // Description is a Unicode Text
    // MAX SIZE LIMT is 10*1024 Bytes
    protected String desc;
    protected String terms;
    protected Timestamp startDate = null;
    protected Timestamp endDate;
	protected Timestamp closeDate;

    protected Timestamp createDate;
    protected String createdBy;   // user Id
    protected Timestamp modifiedDate;
    protected String quotationId;
    protected int[] quotDuration; // quote validity duration days, hrs, mins in order
    protected Timestamp quotExpDate;

    protected int[] duration; // auction duration days, hrs, mins in order

    protected int type = AuctionTypeEnum.NORMAL.getValue();   // default
    protected int status = AuctionStatusEnum.OPEN.getValue(); // default state

    protected BigDecimal startPrice;
    protected BigDecimal reservePrice;

    protected String webShopId;
    private Shop webShop;
    protected SalesAreaData salesArea;
    protected String sellerId;    // CRM User Id
    
	/*
	 * 
	 * Schedule Date for publishing in the case of auction
	 * being scheduled 
	 */
	protected Timestamp publishDate;
	
    protected ArrayList items = new ArrayList();    // AuctionItem bean

    protected ArrayList bids;    // AuctionItem bean

	transient protected Object userObject; // transient and not cloned
	transient protected ChangeRecorder recorder = new ChangeRecorder();
	
	private String refAuctionId;
	private Collection refAuctions;
	private String client;
	private String systemId;
		
    public String getAuctionId() {
        return auctionId;
    }

    public String getAuctionName() {
        return name;
    }

	public void setAuctionTitle(String title) {
		this.title = title;
	}
	
	public String getAuctionTitle() {
		return title;
	}
	
    /**
     * e-auction specific Attribute.
     * Textual Description of Auction
     */
    public String getAuctionDescription() {
        return desc;
    }

    /**
     * e-auction specific Attribute.
     * Textual Description of Auction terms and conditions
     */
    public String getAuctionTerms() {
        return terms;
    }

    public Timestamp getStartDate() {
        return startDate;
    }

    public Timestamp getEndDate() {
        if(endDate != null) return endDate;
        // calculate end date
        if(startDate != null && duration != null) {
            long millis = DateTimeUtil.toTimeInMillis(duration[0],
                                                      duration[1],
                                                      duration[2]);
            endDate = (Timestamp)DateTimeUtil.adjustDate(startDate,millis);
        }
        return endDate;
    }

    public Timestamp getCreateDate() {
        return createDate;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public Timestamp getLastModifiedDate() {
        return modifiedDate;
    }

    /**
     * e-auction specific Attribute
     * Id of Quotation created in CRM system
     */
    public String getQuotationId() {
        return quotationId;
    }
    
	/**
	 * @return
	 */
	public Timestamp getPublishDate() {
		return publishDate;
	}

	public boolean validatePublishDate() {
		ErrorMessageBundle bundle = ErrorMessageBundle.getInstance();
//		Date date = new Date();
		Calendar cal = Calendar.getInstance();
		cal.set(Calendar.HOUR_OF_DAY,0);
		cal.set(Calendar.MINUTE,0);
		cal.set(Calendar.SECOND,0);
		cal.set(Calendar.MILLISECOND,0);
		if(publishDate ==null  ||  cal.getTime().after(publishDate)) {
			I18nErrorMessage error = bundle.newI18nErrorMessage(ValidationException.INVALID_SCHEDULE_DATE);
			super.addError(error);
			return false;
		}
		return true;
	}
	
	/**
	 * @param date
	 */
	public void setPublishDate(Timestamp date) {
		publishDate = date;
	}
	
    /**
     *
     */
    public void setAuctionDuration(int days, int hrs, int mins) {
        if(days < 0 || days > 365) throw new IllegalArgumentException("invalid arg: days:" + days);
        if(hrs < 0 || hrs > 23) throw new IllegalArgumentException("invalid arg: days:" + hrs);
        if(mins < 0 || mins > 59) throw new IllegalArgumentException("invalid arg: days:" + mins);

        this.duration = new int[]{days,hrs,mins};
        // clear end date, since it needs to recalculated
        setEndDate(null);
    }

    /**
     * @return int[] of size 3 duration [0] days
     *                                  [1] hrs
     *                                  [2] mins
     */
    public int[] getAuctionDuration() {
        return this.duration;
    }

    /**
     * For Internal Use only
     */
    public Timestamp getQuotationExpirationDate() {
        if(quotExpDate != null) return quotExpDate;
        if(startDate != null && quotDuration != null) {
            long millis = DateTimeUtil.toTimeInMillis(quotDuration[0],
                                      quotDuration[1],
                                      quotDuration[2]);
            quotExpDate = (Timestamp)DateTimeUtil.adjustDate(startDate,millis);
        }
        return quotExpDate;
    }

    /**
     * e-auction specific Attribute
     * Duration of Quotation created in CRM
     * This must be more than the duration of Auction
     *
     * @param days (0 <= days <= 365)
     * @param hrs  (0 <= hrs <= 24)
     * @param mins (0 <= mins <= 60)
     * Used only for templates
     */
    public void setQuotationDuration(int days, int hrs, int mins) {
        if(days < 0 || days > 365) throw new IllegalArgumentException("invalid arg: days:" + days);
        if(hrs < 0 || hrs > 23) throw new IllegalArgumentException("invalid arg: days:" + hrs);
        if(mins < 0 || mins > 59) throw new IllegalArgumentException("invalid arg: days:" + mins);

        // convert to millis
        this.quotDuration = new int[]{days,hrs,mins};

        this.quotExpDate = null; // reset
    }

    /**
     * @return int[] of size 3 duration [0] days
     *                                  [1] hrs
     *                                  [2] mins
     * Used only for templates
     */
    public int[] getQuotationDuration() {
        return this.quotDuration;
    }

    public String getSellerId() {
        return sellerId;
    }

	public void setCurrencyCode(String code) {
		this.currencyCode = code;
	}

	public String getCurrencyCode() {
		return this.currencyCode;
	}

	public String getCountryCode() {
		return this.countryCode;
	}

	public void setCountryCode(String val) {
		this.countryCode = val;
	}

    public int getStatus() {
        return status;
    }

    public int getType() {
        return type;
    }

    /*public boolean isFixedQuantity() {
        return fixedQuantity;
    }*/

    public boolean isTemplate() {
        return template;
    }

    public void setTemplate(boolean bool) {
        this.template = bool;
    }
    /**
     * e-auction specific Attribute
     * Id of CRM Web shop where Auction is created
     */
    public String getWebShopId() {
        return webShopId;
    }

    public BigDecimal getStartPrice() {
        return startPrice;
    }

    public BigDecimal getReservePrice() {
        return reservePrice;
    }

    public List getItems() {
    	if(items != null) {
	        return Collections.unmodifiableList(items);
    	}
        else {
        	return null;
        }
    }

    public List getBids() {
    	if(bids != null) {
        	return Collections.unmodifiableList(bids);
    	}
    	else {
    		return null;
    	}
    }

	/**
	 * TODO: Privileged operation
	 * @param list
	 */
    public void setBids(ArrayList list) {
        this.bids = list;
    }

    ///////////////////////////////
    // begin temp code
    /**
     * @@todo:: temp code for backend compilation
     */
    public List getListings() {
        return getItems();
    }

    public List getBusinessPartners() {
        return businessPartners;
    }

    public List getTargetGroups() {
        return targetGroups;
    }

    public boolean isBrokenLot() {
        return false;
    }

    // end temp code
    /////////////////////////////

	/**
	 * TODO: Privileged Operation
	 */
    public void setAuctionId(String auctionId) {
        this.auctionId = auctionId;
    }

    public void setAuctionName(String auctionName) {
        // no leading or trailing spaces allowed - no names made of spaces only
        this.name = auctionName.trim();
    }

    public void setAuctionDescription(String auctionDesc) {
        this.desc = auctionDesc;
    }

    public void setAuctionTerms(String auctionTerms) {
        this.terms = auctionTerms;
    }

    public void setStartDate(Timestamp startDate) {
        this.startDate = startDate;
        // clear end date & quotation expiration date, since it needs to recalculated
        setEndDate(null);
        setQuotationExpirationDate(null);
    }

    public void setEndDate(Timestamp endDate) {
        this.endDate = endDate;
    }

    public void setCreateDate(Timestamp crDate) {
        this.createDate = crDate;
    }

    public void setCreatedBy(String id) {
        this.createdBy = id;
    }

    public void setLastModifiedDate(Timestamp modDate) {
        this.modifiedDate = modDate;
    }

    public void setQuotationId(String quotationId) {
        this.quotationId = quotationId;
    }

    /**
     * For internal use only
     */
    public void setQuotationExpirationDate(Timestamp expDate) {
        this.quotExpDate = expDate;
    }

    public void setSellerId(String sellerId) {
        this.sellerId = sellerId;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public void setType(int type) {
        this.type = type;
    }

    public void setWebShopId(String webShopId) {
        this.webShopId = webShopId;
    }

    /**
     * Full Lot by Default is Fixed Quantity
     */
    /*public void setFixedQuantity(boolean value) {
        this.fixedQuantity = value;
    }*/

    public void setStartPrice(BigDecimal startPrice) {
        this.startPrice = startPrice;
    }

    public void setReservePrice(BigDecimal reservePrice) {
        this.reservePrice = reservePrice;
    }

	/**
	 * TODO: priviled operation
	 * @param items
	 */
    public void setItems(ArrayList items) {
        java.util.Iterator it = items.iterator();
        AuctionItem item = null;
        int i = 0;
        while(it.hasNext())  {
            item = (AuctionItem)it.next();
            item.setAuction(this);
        }

        this.items = items;
    }

    public void addItem(AuctionItem item) {
		if(recorder.isRecording() && !recorder.isModified(ATTR_ITEMS)) {
			ArrayList old = items;
			if(old != null)
				items = new TrackedArrayList(old);
			else 
				items = new TrackedArrayList();
			recorder.setAttributeValue(ATTR_ITEMS,old,items);				
        }
        
        if (!items.contains(item)) {
            item.setAuction(this);
            items.add(item);
        }
    }

    public void removeItem(AuctionItem item) {
		if(recorder.isRecording() && !recorder.isModified(ATTR_ITEMS)) {
			ArrayList old = items;			
			if(old != null)
				items = new TrackedArrayList(old);
			else 
				items = new TrackedArrayList();
			recorder.setAttributeValue(ATTR_ITEMS,old,items);				
		}
		    	
        if(this.items != null) {
            this.items.remove(item);
        }
    }
    
    /**
     * Simple Auction bean state validation checks
     */
    public boolean validate(boolean deep) {
        super.clearErrors();
		super.clearWarnings();
		super.clearInvalidChildren();
		
		ErrorMessageBundle bundle = ErrorMessageBundle.getInstance();
		
        if(name == null || name.length() == 0) {
			I18nErrorMessage error = bundle.newI18nErrorMessage(ValidationException.INVALID_NAME);
			error.setArguments(new Object[]{"'" + (name==null?"":name)+"'"});
            super.addError(error);
        }

        // Max length allowed for name is 80
        if(name != null && name.length() > MAX_NAME_LENGTH) {
            I18nErrorMessage error = bundle.newI18nErrorMessage(ValidationException.INVALID_NAME_LENGTH);
            error.setArguments(new Object[]{Integer.toString(MAX_NAME_LENGTH)});
            super.addError(error);
        }

        // nps:: start date and end dates are not mandatory
        /*
        if(startDate == null) {
            super.addError(bundle.newI18nErrorMessage(ValidationException.INVALID_START_DATE));
        }

        if(endDate == null) {
            super.addError(bundle.newI18nErrorMessage(ValidationException.INVALID_END_DATE));
        }
        else {
            if(startDate != null && endDate.before(startDate)) {
                super.addError(bundle.newI18nErrorMessage(ValidationException.END_DATE_BEFORE_START_DATE));
            }

            if(endDate.before(new java.sql.Timestamp(System.currentTimeMillis()))) {
                super.addError(bundle.newI18nErrorMessage(ValidationException.END_DATE_BEFORE_NOW));
            }
        }*/
        
		if(currencyCode == null) {
			I18nErrorMessage error = bundle.newI18nErrorMessage(ValidationException.MISSING_CURRENCY);
			super.addError(error);
		}

        if(duration == null) {
            I18nErrorMessage error = bundle.newI18nErrorMessage(ValidationException.INVALID_DURATION);
            super.addError(error);
        }

		//Quotation expiration is no longer set from the user perspective.
		//Quotation is valid untill the buyer checkout
        /*if(quotDuration == null) {
            I18nErrorMessage error = bundle.newI18nErrorMessage(ValidationException.INVALID_DURATION);
            super.addError(error);
        }*/

        // check that the Quotation expiration millis are more than Auction Duration millis
        /*if(duration != null && quotDuration != null) {
            if(DateTimeUtil.toTimeInMillis(duration[0],duration[1],duration[2]) >=
               DateTimeUtil.toTimeInMillis(quotDuration[0],quotDuration[1],quotDuration[2])) {
                super.addError(bundle.newI18nErrorMessage(ValidationException.QUOT_EXP_DATE_BEFORE_END_DATE));
            }
        }*/

		//Shop validation is not required, because we don't store it 
        /*if(webShopId == null) {
            super.addError(bundle.newI18nErrorMessage(ValidationException.INVALID_WEBSHOPID));
        }

        // Max length for web shop Id is 10
        if(webShopId != null && webShopId.length() > MAX_WEBSHOPID_LENGTH) {
            super.addError(bundle.newI18nErrorMessage(ValidationException.INVALID_WEBSHOPID));
        }*/


        // Max of 10*1024 unicode chars are allowed
        if(desc != null && desc.length() > MAX_DESC_DATA_LENTH) {
            I18nErrorMessage error = bundle.newI18nErrorMessage(ValidationException.INVALID_DESCRIPTION_LENGTH);
            error.setArguments(new Object[]{Integer.toString(MAX_DESC_DATA_LENTH)});
            super.addError(error);
        }

		if(startPrice == null || startPrice.doubleValue() <= 0) {
			I18nErrorMessage error = bundle.newI18nErrorMessage(ValidationException.INVALID_START_PRICE);
			super.addError(error);
		}
		
        if(reservePrice != null && reservePrice.compareTo(startPrice) <= 0) {
            I18nErrorMessage error = bundle.newI18nErrorMessage(ValidationException.INVALID_RESERVE_PRICE);
            super.addError(error);
        }
		
        if(items == null || items.size() == 0) {
            super.addError(bundle.newI18nErrorMessage(ValidationException.NO_AUCTION_ITEMS_ERROR));
        }

        boolean itemError = false;
        if(deep && items != null) {
        	ValidatingBean[] invalidItems = new ValidatingBean[items.size()];
            for(int i = 0 ; i < items.size(); i++) {
                AuctionItem item = (AuctionItem)items.get(i);
                item.clearErrors();
                if(!item.validate()) {
                	super.addInvalidChild(item);
					itemError = true;
                }
            }
        }

        if(deep) {
        	return !(super.getErrors().size() > 0 || itemError);
        }
        else {
            return super.getErrors().size() == 0;
        }
    }
	
    public boolean equals(Object other) {
        if(!(other instanceof Auction)) return false;

        Auction otherAuction = (Auction)other;
        if(auctionId != null && otherAuction.auctionId != null) {
            return auctionId.equals(otherAuction.auctionId);
        }
        else {
            return super.equals(other);
        }
    }

    public int hashCode() {
        if(auctionId != null)
            return auctionId.hashCode();
        else
            return super.hashCode();
    }

    public Object clone() {

        Auction auction = null;
        try {
            auction = (Auction)super.clone();
            auction.userObject = null;
        }
        catch(CloneNotSupportedException ex) {
            // should not occur
            return null;
        }
		
		auction.execRequestId = null;
		auction.execStatus = null;
		auction.execException = null;
		// dates are cleared to be reset
		auction.quotationId = null;
        auction.startDate  = null;
        auction.createDate  = null;
        auction.modifiedDate = null;
        auction.publishDate = null;
		auction.quotExpDate = null;
		auction.status = AuctionStatusEnum.OPEN.toInt();
        if(auction.quotDuration != null)  auction.quotDuration = new int[] {quotDuration[0],
                                                                            quotDuration[1],
                                                                            quotDuration[2]};
        if(auction.duration != null)  auction.duration = new int[] {duration[0],duration[1],duration[2]};

        if(auction.startPrice != null) auction.startPrice = new BigDecimal(startPrice.toString());
        if(auction.reservePrice != null) auction.reservePrice = new BigDecimal(reservePrice.toString());

        if(items != null) {
            auction.items = new ArrayList();
            for(int i = 0 ; i < items.size(); i++) {
                AuctionItem item = (AuctionItem)items.get(i);
                auction.items.add(item.clone());
            }
        }

        return auction;
    }
    
	// Deserialization
	private void readObject(ObjectInputStream in) 
		throws java.io.IOException, ClassNotFoundException {

		in.defaultReadObject();
		this.recorder = new ChangeRecorder();
	}
    
    /**
     * @return
     */
    public SalesAreaData getSalesArea() {
        return salesArea;
    }

    /**
     * @param string
     */
    public void setSalesArea(SalesAreaData area) {
        salesArea = area;
    }

	public void setUserObject(Object obj) {
		this.userObject = obj;
	}
    
	public Object getUserObject() {
		return userObject;
	}

	public String getReferencedAuctionId() {
		return refAuctionId;
	}
	
	public void setReferencedAuctionId(String id) {
		this.refAuctionId = id;
	}
	
	/**
	 * @return
	 */
	public Collection getReferencingAuctions() {
		if(refAuctions != null) {
			return Collections.unmodifiableCollection(refAuctions);
		}
		else {
			return null;
		}
	}

	/**
	 * @param collection
	 */
	public void setReferencingAuctions(Collection collection) {
		refAuctions = collection;
	}
	
	
	////////////////////////////////////////////////////
	// recording specific, only relation specific attributs
	// are tracked
	
	/* (non-Javadoc)
	 * @see com.sap.isa.auction.bean.ChangeRecorderBean#getModifiedAttributes()
	 */
	public String[] getModifiedAttributes() {
		return recorder.getModifiedAttributes();
	}

	/* (non-Javadoc)
	 * @see com.sap.isa.auction.bean.ChangeRecorderBean#getNewAttributeValue(java.lang.String)
	 */
	public Object getNewAttributeValue(String attributeName) {
		return recorder.getNewAttributeValue(attributeName);
	}

	/* (non-Javadoc)
	 * @see com.sap.isa.auction.bean.ChangeRecorderBean#getOldAttributeValue(java.lang.String)
	 */
	public Object getOldAttributeValue(String attributeName) {
		return recorder.getOldAttributeValue(attributeName);
	}

	/* (non-Javadoc)
	 * @see com.sap.isa.auction.bean.ChangeRecorderBean#isModified(java.lang.String)
	 */
	public boolean isModified(String attributeName) {
		return recorder.isModified(attributeName);
	}

	/* (non-Javadoc)
	 * @see com.sap.isa.auction.bean.ChangeRecorderBean#isRecording()
	 */
	public boolean isRecording() {
		return recorder.isRecording();
	}

	/* 
	 * Privileged operation
	 * @see com.sap.isa.auction.bean.ChangeRecorderBean#startRecording()
	 */
	public void startRecording() {
		if(recorder.isModified(ATTR_ITEMS)) {
			((TrackedArrayList)items).commit();
		}		
		recorder.startRecording();
	}
	
	/* 
	 * Privileged operation
	 * @see com.sapmarkets.isa.auction.bean.ChangeRecorderBean#endRecording()
	 */
	public String[] endRecording() {
		if(recorder.isModified(ATTR_ITEMS)) {
			((TrackedArrayList)items).commit();
		}
		if(recorder.isModified(ATTR_TGT_GRPS)) {
			((TrackedArrayList)targetGroups).commit();
		}		
		if(recorder.isModified(ATTR_BPS)) {
			((TrackedArrayList)businessPartners).commit();
		}		
		return recorder.endRecording();
	}

	
	/* (non-Javadoc)
	 * @see com.sapmarkets.isa.auction.bean.ChangeRecorderBean#revertRecording()
	 */
	public void revertRecording() {
		if(recorder.isRecording()) {
			if(recorder.isModified(ATTR_ITEMS)) {			
				((TrackedArrayList)items).revert();
			}
			if(recorder.isModified(ATTR_TGT_GRPS)) {
				((TrackedArrayList)targetGroups).revert();
			}		
			if(recorder.isModified(ATTR_BPS)) {
				((TrackedArrayList)businessPartners).revert();
			}							
			String[] attrs = recorder.getModifiedAttributes();
			for(int i = 0; i < attrs.length; i++) {
				String attr = attrs[i];
				setAttribute(attr,recorder.getOldAttributeValue(attr));
			}
			recorder.revertRecording();
		}
	}


	/**
	 * 
	 * @param attr
	 * @param value
	 */
	protected void setAttribute(String attr, Object value) {
		if(attr.equals(ATTR_ITEMS)) {
			setItems((ArrayList)value);
		}
		if(attr.equals(ATTR_TGT_GRPS)) {
			setTargetGroups((ArrayList)value);
		}		
		if(attr.equals(ATTR_BPS)) {
			setBusinessPartners((ArrayList)value);
		}		
	}

    /**
     * @return
     */
    public String getClient() {
        return client;
    }



    /**
     * @return
     */
    public String getSystemId() {
        return systemId;
    }

    /**
     * @param string
     */
    public void setClient(String string) {
        client = string;
    }



    /**
     * @param string
     */
    public void setSystemId(String string) {
        systemId = string;
    }
    
	/**
	 *
	 */
	public void setBusinessPartners(ArrayList bps) {
		this.businessPartners = bps;
	}


	public void addBusinessPartner(String bpid) {
		if(recorder.isRecording() && !recorder.isModified(ATTR_BPS)) {
			ArrayList old = items;
			if(old != null)
				items = new TrackedArrayList(old);
			else 
				items = new TrackedArrayList();
			recorder.setAttributeValue(ATTR_BPS,old,items);				
		}
        
		if (!businessPartners.contains(bpid)) {
			businessPartners.add(bpid);
		}
	}

	public void removeBusinessPartner(String bpid) {
		if(recorder.isRecording() && !recorder.isModified(ATTR_BPS)) {
			ArrayList old = items;			
			if(old != null)
				items = new TrackedArrayList(old);
			else 
				items = new TrackedArrayList();
			recorder.setAttributeValue(ATTR_BPS,old,items);				
		}
		    	
		if(this.businessPartners != null) {
			this.businessPartners.remove(bpid);
		}
	}

	public void setTargetGroups(ArrayList tgs) {
		this.targetGroups = tgs;
	}

	public void addTargetGroup(String tgid) {
		if(recorder.isRecording() && !recorder.isModified(ATTR_TGT_GRPS)) {
			ArrayList old = items;
			if(old != null)
				items = new TrackedArrayList(old);
			else 
				items = new TrackedArrayList();
			recorder.setAttributeValue(ATTR_TGT_GRPS,old,items);				
		}
        
		if (!targetGroups.contains(tgid)) {
			targetGroups.add(tgid);
		}
	}

	public void removeTargetGroup(String tgid) {
		if(recorder.isRecording() && !recorder.isModified(ATTR_TGT_GRPS)) {
			ArrayList old = items;			
			if(old != null)
				items = new TrackedArrayList(old);
			else 
				items = new TrackedArrayList();
			recorder.setAttributeValue(ATTR_TGT_GRPS,old,items);				
		}
		    	
		if(this.targetGroups != null) {
			this.targetGroups.remove(tgid);
		}
	}

	public void addTargetGroup(TargetGroupBP targetGroup)  {
		if(null == targetGroups)  {
			targetGroups = new ArrayList();
		}
		if (!targetGroups.contains(targetGroup.getGuid()))
			targetGroups.add(targetGroup.getGuid());
	} 
	  
	public ExecutionStatusEnum getExecutionStatus() {
		return execStatus;
	}
	
	public void setExecutionStatus(ExecutionStatusEnum status) {
		this.execStatus = status;
	}

	public String getExecutionRequestId() {
		return execRequestId;	
	}
	
	public void setExecutionRequestId(String id) {
		this.execRequestId = id;
	}
	
	public Exception getExecutionException() {
		return this.execException;
	}
	
	public void setExecutionException(Exception ex) {
		this.execException = ex;
	}
	/**
	 * @return
	 */
	public Timestamp getCloseDate() {
		return closeDate;
	}

	/**
	 * @param timestamp
	 */
	public void setCloseDate(Timestamp timestamp) {
		closeDate = timestamp;
	}
	

	/**
	 * @return
	 */
	public String getImageURL() {
		return imageURL;
	}

	/**
	 * @return
	 */
	public int getQuantity() {
		return quantity;
	}

	/**
	 * @param string
	 */
	public void setImageURL(String string) {
		imageURL = string;
	}

	/**
	 * @param i
	 */
	public void setQuantity(int i) {
		if (i > 1) { // PZ_TODO: remove after coding enhancement
			throw new IllegalArgumentException("Dutch Auctions Currently not Allowed");
		}
		quantity = i;
	}

	/* Returns the complete URL to view the item, given the B2B URL
	 * This method appends the auction id and shop id to the URL
	 * @see com.sapmarkets.isa.auction.backend.boi.AuctionData#getAuctionURL(java.lang.String)
	 */
	public String getAuctionURL(String b2bURL) {
		if (( b2bURL == null) || (auctionId == null) || ("".equals(b2bURL)))
			return b2bURL;
		if (b2bURL.indexOf('?') != -1){ //already has the first argument
			b2bURL += ( "&_auctionId=" + auctionId );	
		}else{//auctionid is the first argument
			b2bURL += ( "?_auctionId=" + auctionId );
		}
		if(webShopId != null)
			b2bURL += ("&shop=" + webShopId );
			
		return b2bURL;
	}
	/**
	 * @return
	 */
	public Shop getWebShop() {
		return webShop;
	}

	/**
	 * @param shop
	 */
	public void setWebShop(Shop shop) {
		this.webShop = shop;
	}

	/**
	 * @return
	 */
	public int getQuantityRemaining() {
		return quantityRemaining;
	}

	/**
	 * @param i
	 */
	public void setQuantityRemaining(int i) {
		quantityRemaining = i;
	}

}
