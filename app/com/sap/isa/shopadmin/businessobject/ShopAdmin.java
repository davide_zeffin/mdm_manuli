/*****************************************************************************
    Class:        ShopAdmin
    Copyright (c) 2001, SAPMarkets Europe GmbH, Germany, All rights reserved.
    Author:       Wolfgang Sattler
    Created:      Januray 2002
    Version:      1.0

    $Revision: #7 $
    $Date: 2001/07/25 $
*****************************************************************************/
package com.sap.isa.shopadmin.businessobject;

import java.sql.ResultSet;
import java.util.Iterator;

import com.sap.isa.businessobject.BusinessObjectBase;
import com.sap.isa.businessobject.BusinessObjectHelper;
import com.sap.isa.businessobject.CommunicationException;
import com.sap.isa.core.TechKey;
import com.sap.isa.core.businessobject.BackendAware;
import com.sap.isa.core.eai.BackendException;
import com.sap.isa.core.eai.BackendObjectManager;
import com.sap.isa.core.util.Message;
import com.sap.isa.core.util.table.ResultData;
import com.sap.isa.core.util.table.Table;
import com.sap.isa.core.util.table.TableRow;
import com.sap.isa.maintenanceobject.backend.boi.MaintenanceObjectBackend;
import com.sap.isa.maintenanceobject.backend.boi.PropertyData;
import com.sap.isa.shopadmin.backend.boi.ShopAdminBackend;
import com.sap.isa.user.businessobject.UserBase;


/**
 *
 * @author Wolfgang Sattler
 * @version 1.0
 */
public class ShopAdmin extends BusinessObjectBase implements BackendAware{

    private ShopAdminBackend backendService;
    
    protected BackendObjectManager bem;
    
    protected ResultData shopList;
    
    protected String searchExpression = "";
    
    protected String searchScenario = "";
    

    /**
     * Standard constructor
     *
     */
    public ShopAdmin() {
    }


    public Shop createShop() throws CommunicationException {

        try {
            Shop shop = new Shop((ShopAdminBackend)getBackendService());

            shop.initObject();

            return shop;
        }
        catch (BackendException ex) {
            BusinessObjectHelper.splitException(ex);
        }

        return null;
    }

    public void deleteShop(TechKey shopKey) throws CommunicationException {

        try {
            ((ShopAdminBackend)getBackendService()).deleteShop(shopKey);
            // Delete the deleted shop from the shoplist
            if (isValid() && shopList != null) {
            	
   	            shopList.first();

	            if (shopList.searchRowByColumn(Shop.ID, shopKey.getIdAsString())) {
	
	            	Table shopTable = shopList.getTable();
	
	            	int index = shopList.getRow();
	            	
	            	shopTable.removeRow(index);
	            	
	            	createShopList(shopTable);
	            }	
            }        
        }
        catch (BackendException ex) {
            BusinessObjectHelper.splitException(ex);
        }

    }

    
    /**
     * Read a list of shops in the backend.
     *
     */
    public ResultData readShopList(String searchExpression, String scenario)
            throws CommunicationException {

        try {
            shopList = ((ShopAdminBackend)getBackendService()).readShopList(searchExpression, scenario);
        }
        catch (BackendException ex) {
            BusinessObjectHelper.splitException(ex);
        }
        return shopList;
    }


	/**
	 * Sets the searchExpression.
	 * @param searchExpression The searchExpression to set
	 */
	public void setSearchExpression(String searchExpression) {
		if (searchExpression!= null) {
			this.searchExpression = searchExpression;
		}
	}


	/**
	 * Returns the searchExpression.
	 * @return String
	 */
	public String getSearchExpression() {
		return searchExpression;
	}


	/**
	 * Sets the searchScenario.
	 * @param searchScenario The searchScenario to set
	 */
	public void setSearchScenario(String searchScenario) {
		if (searchScenario!= null) {
			this.searchScenario = searchScenario;
		}	
	}


	/**
	 * Returns the searchScenario.
	 * @return String
	 */
	public String getSearchScenario() {
		return searchScenario;
	}

	/**
	 * Returns the shopList.
	 * @return ResultData
	 */
	public ResultData getShopList() {
		return shopList;
	}


	/**
     * Reset the ShopList. 
     * 
     */
    public void resetShopList() {
	    shopList = null;
	}


	/**
     * Update the shoplist with the description of the given shop.
     * @param shop shop which has changed
     * 
     */
    public void updateShopList(Shop shop) {
	    
	    if (shopList != null) {
        	shopList.first();

	        if (shopList.searchRowByColumn(Shop.ID, shop.getTechKey().getIdAsString())) {
		
	            Table shopTable = shopList.getTable();
		
	            int index = shopList.getRow();
		            	
	            TableRow row = shopTable.getRow(index);
	            
	            row.setValue(Shop.DESCRIPTION,shop.getDescription());            
		            	
	            createShopList(shopTable);
	        }	
        }
	}

	/**
	 * Creates the shopList from a given shop table.
	 * 
	 * @param shopTable shop table
	 */
	public void createShopList(Table shopTable) {
		shopList = new ResultData(shopTable);
	}


	/**
	 * Check if the given user is authorized to maintain shops. <br>
	 * 
	 * @param user user to check
	 * @return <code>true</code> if the user is authorized
	 * @throws CommunicationException
	 */
	public boolean isUserAuthorized(UserBase user)
			throws CommunicationException {

		try {
			return getMyBackendService().isUserAuthorized(user);
		}
		catch (BackendException ex) {
			BusinessObjectHelper.splitException(ex);
		}
		return false;
	}


	/**
	 * get the BackendService, if necessary
	 */
	protected ShopAdminBackend getMyBackendService()
			throws BackendException {
		return (ShopAdminBackend)getBackendService();		
	}


    /**
     * get the BackendService, if necessary
     */
    protected MaintenanceObjectBackend getBackendService()
            throws BackendException {

        if (backendService == null) {
            // get the Backend from the Backend Manager
            backendService = 
                    (ShopAdminBackend) bem.createBackendBusinessObject("ShopAdmin", null);
        }
        return backendService;
    }
    
    /**
     * Upload the data from <code>ShopAdminUpload</code> backend to the current
     * backend. <br>
     */
    public void uploadShopData() throws CommunicationException  {
    	
    	try  {
			ShopAdminBackend uploadService =  
				(ShopAdminBackend) bem.createBackendBusinessObject("ShopAdminUpload", null);

			ResultData shops = uploadService.readShopList("" , "");
			
			if (shops != null)  {
				ResultSetIterator iterate = new ResultSetIterator(shops);
				ResultData shopData = null;
				Shop shop = null;
				PropertyData property = null;
				while(iterate.hasNext())  {
					shopData = (ResultData)iterate.next();
					shop = createShop();
					shop.setTechKey(new TechKey(shopData.getString(Shop.ID)));
					uploadService.readData(shop);
					property = shop.getProperty("scenario");
					if(property != null)
						shop.setScenario(property.getString());
					shop.saveData();
					if(!shop.isValid() && shop.getMessageList() != null)  {
						getMessageList().add(shop.getMessageList());
					}
				}
			}
			addMessage(new Message(Message.INFO,"shpm.upload.success"));			
			
    	}  catch(BackendException exception) {
    		log.error("Upload of shop data failed" , exception);
    		String args[] = {exception.getMessage()};
			addMessage(new Message(Message.ERROR,"shpm.upload.error",args,null));
    	}
    }

    /**
     * Sets the BackendObjectManager for the basket object. This method is used
     * by the object manager to allow the user object interaction with
     * the backend logic. This method is normally not called
     * by classes other than BusinessObjectManager.
     *
     * @param bem BackendObjectManager to be used
     */
    public void setBackendObjectManager(BackendObjectManager bem) {
        this.bem = bem;
    }

	private class ResultSetIterator implements Iterator {

		private ResultSet rs;

		public ResultSetIterator(ResultSet rs) {
			this.rs = rs;
		}

		public boolean hasNext() {
			try {
				return rs.next();
			}
			catch (Exception e) {
				return false;
			}
		}

		public Object next() {
			return (Object) rs;
		}

		public void remove() {
		}
	}


}
