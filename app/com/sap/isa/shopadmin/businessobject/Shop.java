/*****************************************************************************
    Class:        Shop
    Copyright (c) 2001, SAPMarkets Europe GmbH, Germany, All rights reserved.
    Author:       Wolfgang Sattler
    Created:      Januray 2002
    Version:      1.0

    $Revision: #7 $
    $Date: 2001/07/25 $
*****************************************************************************/
package com.sap.isa.shopadmin.businessobject;

import com.sap.isa.businessobject.BusinessObjectHelper;
import com.sap.isa.businessobject.CommunicationException;
import com.sap.isa.core.eai.BackendException;
import com.sap.isa.maintenanceobject.backend.boi.MaintenanceObjectBackend;
import com.sap.isa.maintenanceobject.backend.boi.PropertyData;
import com.sap.isa.maintenanceobject.businessobject.MaintenanceObject;
import com.sap.isa.maintenanceobject.businessobject.PropertyGroup;
import com.sap.isa.maintenanceobject.businessobject.ScreenGroup;
import com.sap.isa.shopadmin.backend.boi.ShopAdminBackend;
import com.sap.isa.shopadmin.backend.boi.ShopData;


/**
 *
 * @author Wolfgang Sattler
 * @version 1.0
 */
public class Shop extends MaintenanceObject implements ShopData{

    private ShopAdminBackend backendService;

    /**
     * Standard constructor
     *
     *
     */
    public Shop(ShopAdminBackend backendService) {

        this.backendService = backendService;
    }


    /**
     * Read the values of the properties from the backend.
     *
     */
    public void readData() throws CommunicationException {

        try {
            // read properties from the backend.
            ((ShopAdminBackend)getBackendService()).readData(this);
            isNewObject = false;
        }
        catch (BackendException ex) {
            BusinessObjectHelper.splitException(ex);
        }
    }
    
 	/**
     * Check dependicies within the object
     *
     */
    public void checkDependencies() throws CommunicationException {

        try {
            // check dependencies
            ((ShopAdminBackend)getBackendService()).checkDependencies(this);
            determineVisibility();
        }
        catch (BackendException ex) {
            BusinessObjectHelper.splitException(ex);
        }
    }


    /**
     * Save the values of the properties to the backend.
     *
     */
    public void saveData() throws CommunicationException {

        try {
            // read properties from teh backend.
            ((ShopAdminBackend)getBackendService()).saveData(this);
            if (this.isValid()) {
                isNewObject = false;
            }
        }
        catch (BackendException ex) {
            BusinessObjectHelper.splitException(ex);
        }

    }


    //TODO docu
	/**
	 * Overwrites the method . <br>
	 * 
	 * @param screenGroup
	 * 
	 * 
	 * @see com.sap.isa.maintenanceobject.businessobject.MaintenanceObject#addScreenGroup(com.sap.isa.maintenanceobject.businessobject.ScreenGroup)
	 */
	public void addScreenGroup(ScreenGroup screenGroup) {
		super.addScreenGroup(screenGroup);
		screenGroup.setToggleAllowed(true);
	}
	
	


	//TODO docu
	/**
	 * Overwrites the method . <br>
	 * 
	 * @param propertyGroup
	 * 
	 * 
	 * @see com.sap.isa.maintenanceobject.businessobject.MaintenanceObject#addPropertyGroup(com.sap.isa.maintenanceobject.businessobject.PropertyGroup)
	 */
	public void addPropertyGroup(PropertyGroup propertyGroup) {
		super.addPropertyGroup(propertyGroup);
		propertyGroup.setToggleAllowed(true);
	}


	/**
     * Set the scenario of the shop and Adjust properties depending from the 
     * given scenario.
     *
	 * @param scenario scenario
     */
    public void setScenario(String scenario) 
    		throws CommunicationException {

        try {       	
        	PropertyData property = getProperty("scenario");

			if (property != null) {		
				property.setValue(scenario);
			}	
       	
            // adjust properties depending the scenario.
            ((ShopAdminBackend)getBackendService()).adjustToScenario(this,scenario);

        }
        catch (BackendException ex) {
            BusinessObjectHelper.splitException(ex);
        }
    }


    /**
     * get the BackendService, if necessary
     */
    protected MaintenanceObjectBackend getBackendService()
            throws BackendException {

        if (backendService == null) {
            throw new BackendException("no valid backend is set in Shop object");
        }
        return backendService;
    }
    
}
