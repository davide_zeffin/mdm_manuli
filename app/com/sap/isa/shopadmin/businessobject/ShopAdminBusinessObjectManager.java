/*****************************************************************************
    Class:        ShopAdminBusinessObjectManager
    Copyright (c) 2001, SAPMarkets Europe GmbH, Germany, All rights reserved.
    Author:       Thomas Smits, Wolfgang Sattler
    Created:      20.2.2001
    Version:      1.0

    $Revision: #8 $
    $Date: 2001/07/26 $
*****************************************************************************/
package com.sap.isa.shopadmin.businessobject;

import com.sap.isa.businessobject.CommunicationException;
import com.sap.isa.businessobject.GenericBusinessObjectManager;
import com.sap.isa.core.businessobject.BackendAware;
import com.sap.isa.helpvalues.businessobject.HelpValuesSearchHandler;
import com.sap.isa.user.businessobject.UserBase;
import com.sap.isa.user.businessobject.UserBaseAware;


/**
 * Central point to create and access Objects of the business logic. After
 * a request for a specific object, BusinessObjectManager checks if there is already
 * an object created. If so, a reference to this object is returned, otherwise
 * a new object is created and a reference to the new object is returned.
 * The reason for this class is, that the construction process of the businnes
 * logic objects may be quite complicated (because of some contextual
 * information) and most objects should only exist once per session.
 * To achieve this the <code>ShopAdminBusinessObjectManager</code>
 * is used. If you store a reference to this
 * object (for examle in a session context) you can get access to all other
 * business obejects using it.
 *
 * @author Thomas Smits
 * @version 1.0
 */
public class ShopAdminBusinessObjectManager extends GenericBusinessObjectManager
    implements BackendAware, UserBaseAware {

    protected ShopAdmin shopAdmin;
    protected Shop      shop;


    /**
     * Constant containing the name of this BOM
     */
    public static final String SHOPADMIN_BOM = "SHOPADMIN-BOM";


    /**
     * Create a new instance of the object.
     */
    public ShopAdminBusinessObjectManager() {
    }



    /**
     * Creates a new user object. If such an object was already created, a
     * reference to the old object is returned and no new object is created.
     *
     * @return referenc to a newly created or already existend user object
     */
    public synchronized ShopAdmin createShopAdmin() {
        shopAdmin = (ShopAdmin)createBusinessObject(ShopAdmin.class);
        return shopAdmin;
    }

    /**
     * Returns a reference to an existing user object.
     *
     * @return reference to user object or null if no object is present
     */
    public ShopAdmin getShopAdmin() {
      return (ShopAdmin) getBusinessObject(ShopAdmin.class);
    }


    /**
     * Release the references to created user object.
     */
    public synchronized void releaseShopAdmin() {
        releaseBusinessObject(ShopAdmin.class);
    }

    /**
     * Creates a shop and stores a reference
     * to this shop in the bom. This reference can be retrieved with
     * the <code>getShop</code> method.
     *
     *
     * @return The shopAdmin read
    */
    public synchronized Shop createShop()
            throws CommunicationException {

        if (shop == null) {
            if (shopAdmin == null) {
                createShopAdmin();
            }

            if (shopAdmin != null) {
                shop = shopAdmin.createShop();
            }
        }

        return shop;
    }

    /**
     * Returns a reference to an existing shop object.
     *
     * @return reference to shop object or null if no object is present
     */
    public Shop getShop() {
        return shop;
    }

    /**
     * Releases reference to the shop admin object.
     */
    public synchronized void releaseShop() {
        if (shop != null) {
            removalNotification(shop);
            callbackDestroy(shop);
            shop = null;
        }
    }


    /**
     * Creates a new user object. If such an object was already created, a
     * reference to the old object is returned and no new object is created.
     *
     * @return referenc to a newly created or already existend user object
     */
    public synchronized UserBase createUser() {
        return (UserBase) createBusinessObject(UserBase.class);
    }

    /**
     * Returns a reference to an existing user object.
     *
     * @return reference to user object or null if no object is present
     */
    public UserBase getUser() {
      return (UserBase) getBusinessObject(UserBase.class);
    }

    /**
     * Release the references to created user object.
     */
    public synchronized void releaseUser() {
        releaseBusinessObject(UserBase.class);
    }


	/**
	 * Returns a reference to an existing user object.
	 *
	 * @return reference to user object or null if no object is present
	 */
	public UserBase getUserBase() {
		return getUser();
	}


	/**
	 * Creates a new user object. If such an object was already created, a
	 * reference to the old object is returned and no new object is created.
	 *
	 * @return referenc to a newly created or already existend user object
	 */
	public UserBase createUserBase() {
		return createUser();
	}


	/**
	 * Creates a new helpValuesSearchHandler object. <br> 
	 * If such an object was already created, a reference to the old object is
	 * returned and no new object is created.
	 *
	 * @return referenc to a newly created or already existend 
	 *          HelpValuesSearchHandler object
	 */
	public synchronized HelpValuesSearchHandler createHelpValuesSearchHandler() {
		return(HelpValuesSearchHandler)createBusinessObject(HelpValuesSearchHandler.class);
	}


	/**
	 * Returns a reference to an existing HelpValuesSearchHandler object. <br>
	 *
	 * @return reference to HelpValuesSearchHandler object or null if no 
	 *          object is present
	 */
	public HelpValuesSearchHandler getHelpValuesSearchHandler() {
	  return createHelpValuesSearchHandler();
	}


	/**
	 * Release the references to created HelpValuesSearchHandler object.
	 */
	public synchronized void releaseHelpValuesSearchHandler() {
		releaseBusinessObject(HelpValuesSearchHandler.class);
	}


}