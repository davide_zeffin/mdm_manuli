/*****************************************************************************
    Class         ShopAdminSessionListner
    Copyright (c) 2001, SAPMarkets Europe GmbH, Germany, All rights reserved.
    Description:  Listner for Session
    Author:       SAP
    Created:      October 2002
    Version:      1.0

    $Revision: #3 $
    $Date: 2001/07/04 $
*****************************************************************************/

package com.sap.isa.shopadmin.action;

import javax.servlet.http.HttpSessionBindingEvent;
import javax.servlet.http.HttpSessionBindingListener;

import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.shopadmin.businessobject.Shop;
import com.sap.isa.shopadmin.businessobject.ShopAdminBusinessObjectManager;

/**
 * The Listner unlocks a shop if necessary.
 * The Listner prevent a shop after the session time out or is invalid.  
 * 
 * @author SAP
 * @version 1.0
 *
 */
public class ShopAdminSessionListner implements HttpSessionBindingListener {

	private ShopAdminBusinessObjectManager bom;
	
	/** 
	  *  The IsaLocation of the current Action.
	  *  This will be instantiated during perform and is always present.
	  */
	 protected IsaLocation log = IsaLocation.getInstance(ShopAdminSessionListner.class.getName());

	
	/**
	 * Constructor. 
	 * 
	 * @param bom shop admin business object manager
	 * 
	 */
	public ShopAdminSessionListner(ShopAdminBusinessObjectManager bom) {
		this.bom = bom;
	}	 

	/**
	 * @see javax.servlet.http.HttpSessionBindingListener#valueBound(HttpSessionBindingEvent)
	 */
	public void valueBound(HttpSessionBindingEvent arg0) {
	}

	/**
	 * @see javax.servlet.http.HttpSessionBindingListener#valueUnbound(HttpSessionBindingEvent)
	 */
	public void valueUnbound(HttpSessionBindingEvent arg0) {

        Shop shop = bom.getShop();

        if (shop != null) {
        	try {
	        	shop.unlock();
        	}
        	catch (Exception ex) {
        		log.debug("Exception occurred in method valueUnbound ", ex);
        	}	 	
        }

	}

}
