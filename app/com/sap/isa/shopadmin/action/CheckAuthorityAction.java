/*****************************************************************************
  Class:        CheckAuthorityAction
  Copyright (c) 2005, SAP AG, All rights reserved.
  Author:       SAP AG
  Created:      21.03.2005
  Version:      1.0

  $Revision: #5 $
  $Date: 2001/08/03 $
*****************************************************************************/
package com.sap.isa.shopadmin.action;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.sap.isa.businessobject.CommunicationException;
import com.sap.isa.core.UserSessionData;
import com.sap.isa.core.businessobject.management.MetaBusinessObjectManager;
import com.sap.isa.core.util.Message;
import com.sap.isa.core.util.RequestParser;
import com.sap.isa.isacore.MessageDisplayer;
import com.sap.isa.shopadmin.businessobject.ShopAdminBusinessObjectManager;
import com.sap.isa.user.action.EComExtendedBaseAction;
import com.sap.isa.user.businessobject.UserBase;

/**
 * The action checks if the user has the authority to manage shops. <br>
 * 
 * @author SAP AG
 * @version 1.0
 *
 */
public class CheckAuthorityAction extends EComExtendedBaseAction {


	/**
	 * You have to have implement here your action coding. <br>
	 * As an additional parameter you will get a flag indicating, whether or not
	 * a multiple invocation occured.
	 *
	 * @param mapping            The ActionMapping used to select this instance
	 * @param form               The <code>FormBean</code> specified in the
	 *                              config.xml file for this action
	 * @param request            The request object
	 * @param response           The response object
	 * @param userSessionData    Object wrapping the session
	 * @param requestParser      Parser to simple retrieve data from the request
	 * @param mbom               Reference to the MetaBusinessObjectManager
	 * @param multipleInvocation Flag indicating, that a multiple invocation occured
	 * @param browserBack        Flag indicating a browser back
	 * @return Forward to another action or page
	 */
	public ActionForward ecomPerform(ActionMapping mapping,
			                          ActionForm form,
			                          HttpServletRequest request,
			                          HttpServletResponse response,
			                          UserSessionData userSessionData,
			                          RequestParser requestParser,
			                          MetaBusinessObjectManager mbom,
			                          boolean multipleInvocation,
			                          boolean browserBack)
			                          
			throws IOException, ServletException,
				   CommunicationException {

        // first ask the mbom for the used bom
        ShopAdminBusinessObjectManager bom = (ShopAdminBusinessObjectManager)
                mbom.getBOMbyName(ShopAdminBusinessObjectManager.SHOPADMIN_BOM);

        UserBase user = bom.createUser();

	
		// check authority after user is logged in succesfully
			if (!bom.createShopAdmin().isUserAuthorized(user)) {
				MessageDisplayer messageDisplayer = new MessageDisplayer();
				messageDisplayer.setOnlyLogin();
				messageDisplayer.addMessage(new Message(Message.ERROR,"shopad.user.notAuthorized",null,null));
				messageDisplayer.addToRequest(request);

				bom.releaseUser();
				
				return mapping.findForward("message");
			}

                                        
		return  mapping.findForward("success");                                        
    }
}

