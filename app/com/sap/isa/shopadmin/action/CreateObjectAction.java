/*****************************************************************************
    Class         CreateObjectAction
    Copyright (c) 2001, SAPMarkets Europe GmbH, Germany, All rights reserved.
    Description:  Action to display an shop list
    Author:       Wolfgang Sattler
    Created:      February 2002
    Version:      1.0

    $Revision: #3 $
    $Date: 2001/07/04 $
*****************************************************************************/
package com.sap.isa.shopadmin.action;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.sap.isa.businessobject.CommunicationException;
import com.sap.isa.core.UserSessionData;
import com.sap.isa.core.util.RequestParser;
import com.sap.isa.shopadmin.businessobject.Shop;
import com.sap.isa.shopadmin.businessobject.ShopAdminBusinessObjectManager;

/**
 * Creates a shop object.<br>
 *
 * This action create a new shop or clears the shop data, if the shop already exist. 
 * </p>
 * <p>
  * <h4>Overview over request parameters and attributes</h4>
 * <table border="1" cellspacing="0" cellpadding="4">
 *   <tr>
 *     <th>name</th>
 *     <th>parameter</th>
 *     <th>attribute</th>
 *     <th>description</th>
 *   <tr>
 *   <tr>
 *      <td>scenario</td><td>X</td><td>&nbsp</td>
 *      <td>scenarrio to use within the new shop</td>
 *   </tr>
 * </table>
 *
 * <h4>The following forwards are used by this action</h4>
 * <table border="1" cellspacing="0" cellpadding="2">
 *   <tr>
 *     <th>forward</th>
 *     <th>description</th>
 *   <tr>
 *   <tr><td>showshop</td><td>standard forward</td></tr>
 *   <tr><td>error</td><td>an application error occurs</td></tr>
 * </table>
 * <h4>Overview over attributes which will be set in the request context</h4>
 * <table border="1" cellspacing="0" cellpadding="2">
 *   <tr>
 *     <th>attribute</th>
 *     <th>description</th>
 *   <tr>
 * <tr><td>none</td><td>none</td></tr>
 * </table>
 *
 *
 * @author SAP
 * @version 1.0
 *
 */
public class CreateObjectAction extends ShopAdminBaseAction {


    /**
     * Overriden <em>isaPerform</em> method of <em>IsaCoreBaseAction</em>.
     */
    public ActionForward performAction(ActionMapping mapping,
                ActionForm form,
                HttpServletRequest request,
                HttpServletResponse response,
                UserSessionData userSessionData,
                RequestParser requestParser,
                ShopAdminBusinessObjectManager bom)
                    throws CommunicationException {

        // Page that should be displayed next.
        String forwardTo = "showshop";

        Shop shop = bom.createShop();

        request.setAttribute(ActionConstants.RC_SHOP, shop);

        shop.clearData();
        
        shop.setScenario(requestParser.getParameter("scenario").getValue().getString());

        shop.clearMessages();

        return mapping.findForward(forwardTo);
    }


}
