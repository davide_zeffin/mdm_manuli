/*
 * Created on Dec 15, 2004
 *
 * To change the template for this generated file go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
package com.sap.isa.shopadmin.action;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.sap.isa.businessobject.CommunicationException;
import com.sap.isa.core.Constants;
import com.sap.isa.core.UserSessionData;
import com.sap.isa.core.businessobject.management.MetaBusinessObjectManager;
import com.sap.isa.core.config.AdminConfig;
import com.sap.isa.core.util.RequestParser;
import com.sap.isa.shopadmin.businessobject.ShopAdmin;
import com.sap.isa.shopadmin.businessobject.ShopAdminBusinessObjectManager;

/**
 * @author I803067
 *
 * To change the template for this generated type comment go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
public class UploadShopDataAction extends ShopAdminBaseAction {

	public static final String SUCCESS = "success";
	
	/* (non-Javadoc)
	 * @see com.sap.isa.isacore.action.IsaAppBaseAction#performAction(org.apache.struts.action.ActionMapping, org.apache.struts.action.ActionForm, javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse, com.sap.isa.core.UserSessionData, com.sap.isa.core.util.RequestParser, com.sap.isa.core.businessobject.management.MetaBusinessObjectManager)
	 */
	public ActionForward performAction(
		ActionMapping mapping,
		ActionForm form,
		HttpServletRequest request,
		HttpServletResponse response,
		UserSessionData userSessionData,
		RequestParser requestParser,
		MetaBusinessObjectManager mbom)
		throws CommunicationException {
		
		// check if the user has the rights to upload shop data.
		if(request.getParameter(Constants.XCM_SCENARIO_RP) == null ||
				request.getParameter(Constants.XCM_SCENARIO_RP).trim().equals("") ||
				!AdminConfig.isXCMAdmin(request)){
			return mapping.findForward(SUCCESS);
		}

		ShopAdminBusinessObjectManager bom = 
		(ShopAdminBusinessObjectManager)mbom.getBOMbyName(ShopAdminBusinessObjectManager.SHOPADMIN_BOM);
		ShopAdmin shopAdmin = bom.getShopAdmin();
		if(null == shopAdmin)
			shopAdmin = bom.createShopAdmin();
		shopAdmin.clearMessages();

		shopAdmin.uploadShopData();

		request.setAttribute(ActionConstants.RC_SHOP , shopAdmin);

		return mapping.findForward(SUCCESS);
	}

}
