/*****************************************************************************
    Class         ReadObjectAction
    Copyright (c) 2001, SAPMarkets Europe GmbH, Germany, All rights reserved.
    Description:  Action to display an shop list
    Author:       Wolfgang Sattler
    Created:      February 2002
    Version:      1.0

    $Revision: #3 $
    $Date: 2001/07/04 $
*****************************************************************************/
package com.sap.isa.shopadmin.action;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.sap.isa.businessobject.BusinessObjectBase;
import com.sap.isa.businessobject.CommunicationException;
import com.sap.isa.core.TechKey;
import com.sap.isa.core.UserSessionData;
import com.sap.isa.core.util.RequestParser;
import com.sap.isa.maintenanceobject.backend.boi.PropertyData;
import com.sap.isa.shopadmin.businessobject.Shop;
import com.sap.isa.shopadmin.businessobject.ShopAdminBusinessObjectManager;

/**
 * Display a object which should be maintained
 *
 * This action retrieves a list of the available shops from the system and
 * puts the data in the request context to allow a JSP the rendering of the
 * data. If only one shop is found, the control is directly forwarded to the
 * action reading the shop data.
 * </p>
 * <p>
 * <em>Input:</em>
 * <ul>
 *   <li><b><code>none</code></b>
 * </ul>
 * <br>
 * <em>Output:</em>
 * <ul>
 *   <li><b><code>Request-Attribute->RK_SHOP_LIST</code></b> - List of the
 *       shops found as a <code>ResultData</code> object.
 *   <li><b><code>Request-Attribute->PARAM_SHOPID</code></b> - Id of the shop
 *       if only one shop was found. In this case the control is directly
 *       forwarded to the action, which reads the shop.
 * </ul>
 *
 * @author Wolfgang Sattler
 * @version 1.0
 *
 */
public class ReadObjectAction extends ShopAdminBaseAction {


    /**
     * Overriden <em>isaPerform</em> method of <em>IsaCoreBaseAction</em>.
     */
    public ActionForward performAction(ActionMapping mapping,
                ActionForm form,
                HttpServletRequest request,
                HttpServletResponse response,
                UserSessionData userSessionData,
                RequestParser requestParser,
                ShopAdminBusinessObjectManager bom)
                    throws CommunicationException {

        // Page that should be displayed next.
        String forwardTo = "success";

        RequestParser.Parameter shopKey = requestParser.getParameter("shopKey");

        if (!shopKey.isSet()) {
            shopKey = requestParser.getAttribute("shopKey");
        }

        if (shopKey.isSet()) {

            Shop shop = bom.createShop();

            shop.setTechKey(new TechKey(shopKey.getValue().getString()));

            shop.readData();

            PropertyData property = shop.getProperty("scenario");
            
            if (property != null) {
	            String scenario = property.getString();	
	            // adjust shop properties from the scenario property
		        shop.setScenario(scenario);    
			}

            if (!shop.isValid()) {
                request.setAttribute(BusinessObjectBase.CONTEXT_NAME, shop);
                forwardTo = "error";
            }
        }
        else {
            forwardTo = "error";
        }

        return mapping.findForward(forwardTo);
    }
}
