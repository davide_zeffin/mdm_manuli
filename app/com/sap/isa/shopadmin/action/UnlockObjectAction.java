/*****************************************************************************
    Class         UnlockObjectAction
    Copyright (c) 2001, SAPMarkets Europe GmbH, Germany, All rights reserved.
    Description:  Action to display an shop list
    Author:       SAP
    Created:      October 2002
    Version:      1.0

    $Revision: #3 $
    $Date: 2001/07/04 $
*****************************************************************************/
package com.sap.isa.shopadmin.action;


import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.sap.isa.businessobject.CommunicationException;
import com.sap.isa.core.UserSessionData;
import com.sap.isa.core.util.RequestParser;
import com.sap.isa.shopadmin.businessobject.Shop;
import com.sap.isa.shopadmin.businessobject.ShopAdminBusinessObjectManager;

/**
 * Unlocks the shop, if necessary.
 *
 * This action retrieves a logical forward to navigate to the next action. <P>
 * <b>Note: <br>
 * This action is to be used for all actions which leave the maintenance screen.</b>
 * </p>
 *
 * <h4>Overview over request parameters and attributes</h4>
 * <table border="1" cellspacing="0" cellpadding="4">
 *   <tr>
 *     <th>name</th>
 *     <th>parameter</th>
 *     <th>attribute</th>
 *     <th>description</th>
 *   <tr>
 *   <tr>
 *      <td>nextAction</td><td>X</td><td>&nbsp</td>
 *      <td>logical forward, to use after the action</td>
 *   </tr>
 * </table>
 *
 * <h4>Overview over attributes which will be set in the request context</h4>
 * <table border="1" cellspacing="0" cellpadding="2">
 *   <tr>
 *     <th>attribute</th>
 *     <th>description</th>
 *   <tr>
 * <tr><td>none</td><td></td></tr>
 * </table>
 *
 *
 * @author SAP
 * @version 1.0
 *
 */
public class UnlockObjectAction extends ShopAdminBaseAction {


    /**
     * Overriden <em>isaPerform</em> method of <em>IsaCoreBaseAction</em>.
     */
    public ActionForward performAction(ActionMapping mapping,
                ActionForm form,
                HttpServletRequest request,
                HttpServletResponse response,
                UserSessionData userSessionData,
                RequestParser requestParser,
                ShopAdminBusinessObjectManager bom)
                    throws CommunicationException {

        RequestParser.Parameter forward = requestParser.getParameter("nextAction");
        
        String forwardTo = forward.getValue().getString();

        Shop shop = bom.getShop();

        if (shop != null) {
        	shop.unlock();
        }


        return mapping.findForward(forwardTo);
    }
}
