/*****************************************************************************
    Class         SelectHelpValueAction
    Copyright (c) 2001, SAPMarkets Europe GmbH, Germany, All rights reserved.
    Description:  Action to display an shop list
    Author:       Wolfgang Sattler
    Created:      February 2002
    Version:      1.0

    $Revision: #3 $
    $Date: 2001/07/04 $
*****************************************************************************/
package com.sap.isa.shopadmin.action;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.sap.isa.businessobject.CommunicationException;
import com.sap.isa.core.UserSessionData;
import com.sap.isa.core.businessobject.management.MetaBusinessObjectManager;
import com.sap.isa.core.util.RequestParser;
import com.sap.isa.core.util.table.ResultData;
import com.sap.isa.helpvalues.HelpValues;
import com.sap.isa.isacore.action.IsaAppBaseAction;
import com.sap.isa.maintenanceobject.action.MaintainObjectAction;
import com.sap.isa.shopadmin.businessobject.Shop;
import com.sap.isa.shopadmin.businessobject.ShopAdminBusinessObjectManager;

/**
 * Insert a selected help Value into a generic shop object. 
 *
 * @author SAP
 * @version 1.0
 *
 */
public class SelectHelpValueAction extends IsaAppBaseAction {

    /**
     * create the Request Parameter which must be given to this action
     *
     * @param  helpValues the help values object
     * @param  resultValue the list of help values
     *
     * @return parameter string
     *
     */
    public static String createParameter(HelpValues helpValues, ResultData resultData) {

		return MaintainObjectAction.createParameter(helpValues,resultData);

    }

    /**
     * Overriden <em>isaPerform</em> method of <em>IsaCoreBaseAction</em>.
     */
    public ActionForward performAction(ActionMapping mapping,
                ActionForm form,
                HttpServletRequest request,
                HttpServletResponse response,
                UserSessionData userSessionData,
                RequestParser requestParser,
                MetaBusinessObjectManager mbom)
                throws CommunicationException {

        // Page that should be displayed next.
        String forwardTo = "showshop";

        // first ask the mbom for the used bom
        ShopAdminBusinessObjectManager bom = (ShopAdminBusinessObjectManager)
                mbom.getBOMbyName(ShopAdminBusinessObjectManager.SHOPADMIN_BOM);

        Shop shop = bom.getShop();

		MaintainObjectAction.selectHelpValue(requestParser,shop);
		
		shop.synchronizeBeanProperties();
		shop.checkDependencies();

        return mapping.findForward(forwardTo);
    }


}
