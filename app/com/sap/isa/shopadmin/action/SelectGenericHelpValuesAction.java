/*****************************************************************************
    Class         SelectGenericHelpValuesAction
    Copyright (c) 2004, SAP AG
    Description:  Action to display an shop list
    Author:       SAP
    Created:      November 2004
    Version:      1.0

    $Revision: #3 $
    $Date: 2001/07/04 $
*****************************************************************************/
package com.sap.isa.shopadmin.action;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.sap.isa.businessobject.CommunicationException;
import com.sap.isa.core.UserSessionData;
import com.sap.isa.core.businessobject.management.MetaBusinessObjectManager;
import com.sap.isa.core.util.RequestParser;
import com.sap.isa.isacore.action.IsaAppBaseAction;
import com.sap.isa.maintenanceobject.action.MaintainObjectAction;
import com.sap.isa.shopadmin.businessobject.Shop;
import com.sap.isa.shopadmin.businessobject.ShopAdminBusinessObjectManager;

/**
 * Insert a selected help Value into a generic shop object. 
 *
 * @author SAP
 * @version 1.0
 *
 */
public class SelectGenericHelpValuesAction extends IsaAppBaseAction {


    /**
     * Overriden <em>isaPerform</em> method of <em>IsaCoreBaseAction</em>.
     */
    public ActionForward performAction(ActionMapping mapping,
                ActionForm form,
                HttpServletRequest request,
                HttpServletResponse response,
                UserSessionData userSessionData,
                RequestParser requestParser,
                MetaBusinessObjectManager mbom)
                throws CommunicationException {

        // Page that should be displayed next.
        String forwardTo = "showshop";

        // first ask the mbom for the used bom
        ShopAdminBusinessObjectManager bom = (ShopAdminBusinessObjectManager)
                mbom.getBOMbyName(ShopAdminBusinessObjectManager.SHOPADMIN_BOM);

        Shop shop = bom.getShop();

		MaintainObjectAction.getHelpValueResults(userSessionData, shop);
		
		shop.synchronizeBeanProperties();
		shop.checkDependencies();

        return mapping.findForward(forwardTo);
    }



}
