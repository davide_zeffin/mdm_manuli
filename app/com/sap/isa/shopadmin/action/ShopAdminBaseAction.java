/*****************************************************************************
    Class         ShopAdminBaseAction
    Copyright (c) 2001, SAPMarkets Europe GmbH, Germany, All rights reserved.
    Description:  Action to display an shop list
    Author:       Wolfgang Sattler
    Created:      February 2002
    Version:      1.0

    $Revision: #3 $
    $Date: 2001/07/04 $
*****************************************************************************/
package com.sap.isa.shopadmin.action;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.sap.isa.businessobject.CommunicationException;
import com.sap.isa.core.UserSessionData;
import com.sap.isa.core.businessobject.management.MetaBusinessObjectManager;
import com.sap.isa.core.util.Message;
import com.sap.isa.core.util.RequestParser;
import com.sap.isa.isacore.MessageDisplayer;
import com.sap.isa.isacore.action.IsaAppBaseAction;
import com.sap.isa.shopadmin.businessobject.ShopAdminBusinessObjectManager;
import com.sap.isa.user.businessobject.UserBase;


/**
 * Display a object which should be maintained
 *
 * This action retrieves a list of the available shops from the system and
 * puts the data in the request context to allow a JSP the rendering of the
 * data. If only one shop is found, the control is directly forwarded to the
 * action reading the shop data.
 * </p>
 * <p>
 * <em>Input:</em>
 * <ul>
 *   <li><b><code>none</code></b>
 * </ul>
 * <br>
 * <em>Output:</em>
 * <ul>
 *   <li><b><code>Request-Attribute->RK_SHOP_LIST</code></b> - List of the
 *       shops found as a <code>ResultData</code> object.
 *   <li><b><code>Request-Attribute->PARAM_SHOPID</code></b> - Id of the shop
 *       if only one shop was found. In this case the control is directly
 *       forwarded to the action, which reads the shop.
 * </ul>
 *
 * @author Wolfgang Sattler
 * @version 1.0
 *
 */
public class ShopAdminBaseAction extends IsaAppBaseAction {


    /**
     * Overriden <em>isaPerform</em> method of <em>IsaCoreBaseAction</em>.
     */
    public ActionForward performAction(ActionMapping mapping,
                ActionForm form,
                HttpServletRequest request,
                HttpServletResponse response,
                UserSessionData userSessionData,
                RequestParser requestParser,
                MetaBusinessObjectManager mbom)
                    throws CommunicationException {


        // first ask the mbom for the used bom
        ShopAdminBusinessObjectManager bom = (ShopAdminBusinessObjectManager)
                mbom.getBOMbyName(ShopAdminBusinessObjectManager.SHOPADMIN_BOM);

        UserBase user = bom.getUser();

        if (user == null || !user.isUserLogged() ) {
			MessageDisplayer messageDisplayer = new MessageDisplayer();
			messageDisplayer.setOnlyLogin();
			messageDisplayer.addMessage(new Message(Message.ERROR,"shopadmin.info.needLogin",null,null));
			messageDisplayer.addToRequest(request);

			return mapping.findForward("message");
        }

        request.setAttribute(ActionConstants.RC_USER, user);

        return performAction(mapping,
                             form,
                             request,
                             response,
                             userSessionData,
                             requestParser,
                             bom);
    }


    public ActionForward performAction(ActionMapping mapping,
                ActionForm form,
                HttpServletRequest request,
                HttpServletResponse response,
                UserSessionData userSessionData,
                RequestParser requestParser,
                ShopAdminBusinessObjectManager bom)
                    throws CommunicationException {

            return mapping.findForward("error");
        }

}
