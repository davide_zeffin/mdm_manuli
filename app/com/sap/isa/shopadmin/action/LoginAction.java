/*****************************************************************************
  Class:        LoginAction
  Copyright (c) 2001, SAPMarkets Europe GmbH, Germany, All rights reserved.
  Author:       SAP AG
  Created:      22.03.2001
  Version:      1.0

  $Revision: #5 $
  $Date: 2001/08/03 $
*****************************************************************************/
package com.sap.isa.shopadmin.action;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.sap.isa.businessobject.CommunicationException;
import com.sap.isa.core.SessionConst;
import com.sap.isa.core.UserSessionData;
import com.sap.isa.core.businessobject.management.MetaBusinessObjectManager;
import com.sap.isa.core.util.Message;
import com.sap.isa.core.util.RequestParser;
import com.sap.isa.core.util.StartupParameter;
import com.sap.isa.isacore.MessageDisplayer;
import com.sap.isa.isacore.action.IsaAppBaseAction;
import com.sap.isa.shopadmin.businessobject.ShopAdminBusinessObjectManager;
import com.sap.isa.user.action.UserActions;
import com.sap.isa.user.businessobject.UserBase;

/**
 * Logs user into the backend system. This is done using the appropiate
 * business object. If the login fails an error message is displayed and
 * the user has another chance to log in. After the login is successful,
 * a list of sold to parties is displayed, if more than one party is
 * present.<br>
 * Note that all Action classes have to provide a non argument constructor
 * because they were instantiated automatically by the action servlet.
 *
 * @author SAP AG
 * @version 1.0
 * @deprecated never used because introduction of the global login pages.
 */
public class LoginAction extends IsaAppBaseAction {


    /**
     * Implement this method to add functionality to your action.
     *
     * @param form              The <code>FormBean</code> specified in the
     *                          config.xml file for this action
     * @param request           The request object
     * @param response          The response object
     * @param userSessionData   Object wrapping the session
     * @param requestParser     Parser to simple retrieve data from the request
     * @param bom               Reference to the BusinessObjectManager
     * @return Forward to another action or page
     */
    public ActionForward performAction(ActionMapping mapping,
            ActionForm form,
            HttpServletRequest request,
            HttpServletResponse response,
            UserSessionData userSessionData,
            RequestParser requestParser,
            MetaBusinessObjectManager mbom)
                throws CommunicationException {

        // first ask the mbom for the used bom
        ShopAdminBusinessObjectManager bom = (ShopAdminBusinessObjectManager)
                mbom.getBOMbyName(ShopAdminBusinessObjectManager.SHOPADMIN_BOM);

        UserBase user = bom.createUser();

		ActionForward actionForward = UserActions.performLogin(mapping,
                                                               request,
                                                               userSessionData,
                                                               requestParser,
                                                               user);
                                                             
		// get the core Startup Parameter
		StartupParameter startupParameter =
						(StartupParameter)userSessionData.getAttribute(SessionConst.STARTUP_PARAMETER);
						
		startupParameter.addParameter("language",user.getLanguage(),true);
		
		
		// check authority after user is logged in succesfully
		if (user.isUserLogged()) {
			if (!bom.createShopAdmin().isUserAuthorized(user)) {
				MessageDisplayer messageDisplayer = new MessageDisplayer();
				messageDisplayer.setOnlyLogin();
				messageDisplayer.addMessage(new Message(Message.ERROR,"shopad.user.notAuthorized",null,null));
				messageDisplayer.addToRequest(request);

				return mapping.findForward("message");
			}
		}                                      
                                        
		return actionForward;                                        
    }
}

