/*****************************************************************************
  Class:        PWChangeAction
  Copyright (c) 2001, SAPMarkets Europe GmbH, Germany, All rights reserved.
  Author:       Martin Schley
  Created:      22.03.2001
  Version:      1.0

  $Revision: #5 $
  $Date: 2001/08/03 $
*****************************************************************************/

package com.sap.isa.shopadmin.action;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.sap.isa.businessobject.CommunicationException;
import com.sap.isa.core.UserSessionData;
import com.sap.isa.core.businessobject.management.MetaBusinessObjectManager;
import com.sap.isa.core.util.RequestParser;
import com.sap.isa.isacore.action.IsaAppBaseAction;
import com.sap.isa.shopadmin.businessobject.ShopAdminBusinessObjectManager;
import com.sap.isa.user.action.UserActions;
import com.sap.isa.user.businessobject.UserBase;

/**
 * This action changes a password, it can use either for password change on
 * the logon screen or for user switch.
 * Therefore the caller of the jsp must set the parameter ACTION_NAME to
 * the correct value.
 *
 * Note that all Action classes have to provide a non argument constructor
 * because they were instantiated automatically by the action servlet.
 *
 * @author Martin Schley
 * @version 1.0
 *
 */
public class PWChangeAction extends IsaAppBaseAction {



    /**
     * Implement this method to add functionality to your action.
     *
     * @param form              The <code>FormBean</code> specified in the
     *                          config.xml file for this action
     * @param request           The request object
     * @param response          The response object
     * @param userSessionData   Object wrapping the session
     * @param requestParser     Parser to simple retrieve data from the request
     * @param bom               Reference to the BusinessObjectManager
     * @param log               Reference to the IsaLocation, needed for logging
     * @return Forward to another action or page
     */
    public ActionForward performAction(ActionMapping mapping,
            ActionForm form,
            HttpServletRequest request,
            HttpServletResponse response,
            UserSessionData userSessionData,
            RequestParser requestParser,
            MetaBusinessObjectManager mbom)
                throws CommunicationException {

        
        // first ask the mbom for the used bom
        ShopAdminBusinessObjectManager bom = (ShopAdminBusinessObjectManager)
                mbom.getBOMbyName(ShopAdminBusinessObjectManager.SHOPADMIN_BOM);

        UserBase user = bom.getUser();

        return UserActions.performPwChange(mapping,
                                           request,
                                           requestParser,
                                           user);
    }
}

