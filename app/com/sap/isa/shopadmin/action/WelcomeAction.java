/*****************************************************************************
    Class         WelcomeAction
    Copyright (c) 2001, SAPMarkets Europe GmbH, Germany, All rights reserved.
    Description:  Action to display a welcome screen
    Author:       Wolfgang Sattler
    Created:      February 2002
    Version:      1.0

    $Revision: #3 $
    $Date: 2001/07/04 $
*****************************************************************************/
package com.sap.isa.shopadmin.action;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.sap.isa.businessobject.CommunicationException;
import com.sap.isa.core.UserSessionData;
import com.sap.isa.core.util.RequestParser;
import com.sap.isa.maintenanceobject.backend.boi.PropertyData;
import com.sap.isa.maintenanceobject.businessobject.Property;
import com.sap.isa.shopadmin.businessobject.Shop;
import com.sap.isa.shopadmin.businessobject.ShopAdminBusinessObjectManager;

/**
 * Action to display the welcome screen.<br>
 * 
 * This action is needed to display the user in the header and 
 * the allowed sceanrios for the shop creation.
 * 
 * <h4>Overview over request parameters and attributes</h4>
 *  None.
 *
 * <h4>The following forwards are used by this action</h4>
 * <table border="1" cellspacing="0" cellpadding="2">
 *   <tr>
 *     <th>forward</th>
 *     <th>description</th>
 *   <tr>
 *   <tr><td>success</td><td>standard forward</td></tr>
 * </table>
 * <h4>Overview over attributes which will be set in the request context</h4>
 * <table border="1" cellspacing="0" cellpadding="2">
 *   <tr>
 *     <th>attribute</th>
 *     <th>description</th>
 *   <tr>
 * <tr><td><code>ActionConstant.RC_SHOP</code></td><td>shop object to display messages</td></tr>
 * <tr><td><code>ActionConstant.RC_SCENARIO</code></td><td>scenario property to 
 * display the allowed values for scenario property</td></tr>
 * </table>
 *
 *  
 * @author SAP
 * @version 1.0
 *
 */
public class WelcomeAction extends ShopAdminBaseAction {

    /**
     * Overriden <em>isaPerform</em> method of <em>IsaCoreBaseAction</em>.
     */
    public ActionForward performAction(ActionMapping mapping,
                ActionForm form,
                HttpServletRequest request,
                HttpServletResponse response,
                UserSessionData userSessionData,
                RequestParser requestParser,
                ShopAdminBusinessObjectManager bom)
                    throws CommunicationException {

        // Page that should be displayed next.
        String forwardTo = "success";

		// Get the Shop, to get all allowed values for the scenrio 
		Shop shop = bom.createShop();

		PropertyData scenario = shop.getProperty("scenario");

		if (scenario== null) {		
			scenario = new Property("scenario");
		}	

        request.setAttribute(ActionConstants.RC_SHOP, shop);
		request.setAttribute(ActionConstants.RC_SCENARIO, scenario);

        return mapping.findForward(forwardTo);
    }
}
