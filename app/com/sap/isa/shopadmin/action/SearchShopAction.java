/*****************************************************************************
    Class         SearchShopAction
    Copyright (c) 2001, SAPMarkets Europe GmbH, Germany, All rights reserved.
    Description:  Action to display an shop list
    Author:       Wolfgang Sattler
    Created:      February 2002
    Version:      1.0

    $Revision: #3 $
    $Date: 2001/07/04 $
*****************************************************************************/
package com.sap.isa.shopadmin.action;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.sap.isa.businessobject.CommunicationException;
import com.sap.isa.core.UserSessionData;
import com.sap.isa.core.util.RequestParser;
import com.sap.isa.core.util.table.ResultData;
import com.sap.isa.shopadmin.businessobject.ShopAdmin;
import com.sap.isa.shopadmin.businessobject.ShopAdminBusinessObjectManager;

/**
 * Search an generic shop object
 *
 * <h4>Overview over request parameters and attributes</h4>
 * <table border="1" cellspacing="0" cellpadding="4">
 *   <tr>
 *     <th>name</th>
 *     <th>parameter</th>
 *     <th>attribute</th>
 *     <th>description</th>
 *   <tr>
 *   <tr>
 *      <td>CancelSearch</td><td>X</td><td>&nbsp</td>
 *      <td>user has pressed the <i>cancel</i> button</td>
 *   </tr>
 *   <tr>
 *      <td>SearchAll</td><td>X</td><td>&nbsp</td>
 *      <td>user has pressed the <i>search all</i> button</td>
 *   </tr>
 *   <tr>
 *      <td>Search</td><td>X</td><td>&nbsp</td>
 *      <td>user has pressed the <i>search</i> button</td>
 *   </tr>
 *   <tr>
 *      <td>backTo</td><td>X</td><td>&nbsp</td>
 *      <td>logical forward to go back</td>
 *   </tr>
 *   <tr>
 *      <td>forwardAfter</td><td>X</td><td>&nbsp</td>
 *      <td>logical forward to use after finding a shop</td>
 *   </tr>
 *   <tr>
 *      <td>shopId</td><td>X</td><td>&nbsp</td>
 *      <td>entered shop id</td>
 *   </tr>
 *   <tr>
 *      <td>scenario</td><td>X</td><td>&nbsp</td>
 *      <td>entered scenario</td>
 *   </tr>
 * </table>
 *
 * <h4>The following forwards are used by this action</h4>
 * <table border="1" cellspacing="0" cellpadding="2">
 *   <tr>
 *     <th>forward</th>
 *     <th>description</th>
 *   <tr>
 *   <tr><td>searchshop</td><td>standard forward</td></tr>
 *   <tr><td>error</td><td>an application error occurs</td></tr>
 *   <tr><td>values of <code>backTo</code></td><td>forward is given with backTo Parameter </td></tr>
 * </table>
 * <h4>Overview over attributes which will be set in the request context</h4>
 * <table border="1" cellspacing="0" cellpadding="2">
 *   <tr>
 *     <th>attribute</th>
 *     <th>description</th>
 *   <tr>
 * <tr><td><code>ActionConstant.RC_SCENARIO</code></td><td>scenario property to 
 * display the allowed values for scenario property</td></tr>
 * </table>
 *
 *
 * @author SAP
 * @version 1.0
 *
 */
public class SearchShopAction extends ShopAdminBaseAction {


    /**
     * Overriden <em>isaPerform</em> method of <em>IsaCoreBaseAction</em>.
     */
    public ActionForward performAction(ActionMapping mapping,
                ActionForm form,
                HttpServletRequest request,
                HttpServletResponse response,
                UserSessionData userSessionData,
                RequestParser requestParser,
                ShopAdminBusinessObjectManager bom)
                throws CommunicationException {

        // Page that should be displayed next.
        String forwardTo = "showsearch";
      
        String backTo=requestParser.getParameter(ActionConstants.RC_BACK).getValue().getString();

        RequestParser.Parameter shopIdParameter = requestParser.getParameter(ActionConstants.RC_SHOP_ID);
        RequestParser.Parameter scenarioParameter = requestParser.getParameter("scenario");
		String shopId = shopIdParameter.getValue().getString();
        String scenarioValue = scenarioParameter.getValue().getString();

        ShopAdmin shopAdmin = bom.createShopAdmin();

        if (requestParser.getParameter("ForwardShop").isSet()
        	&& requestParser.getParameter("ForwardShop").getValue().getString().equals("true")) {
            return mapping.findForward("getforward");
        }

        if (requestParser.getParameter("DeleteShop").isSet()
        	&& requestParser.getParameter("DeleteShop").getValue().getString().equals("true")) {
            return mapping.findForward("deleteshop");
        }

        if (requestParser.getParameter("ChangeShop").isSet()
        	&& requestParser.getParameter("ChangeShop").getValue().getString().equals("true")) {
            return mapping.findForward("changeshop");
        }


        if (requestParser.getParameter("CancelSearch").isSet()
            && requestParser.getParameter("CancelSearch").getValue().getString().length() > 0) {
            return mapping.findForward(backTo);
        }


        if (scenarioParameter.isSet()) {
            shopAdmin.setSearchScenario(scenarioValue);
        }

        if (requestParser.getParameter("SearchAll").isSet()
			&& requestParser.getParameter("SearchAll").getValue().getString().length() > 0) {

			shopAdmin.setSearchExpression("");

            ResultData shopList = shopAdmin.readShopList("",scenarioValue);

            if (shopList != null) {
                request.setAttribute(ActionConstants.RC_SHOP_LIST, shopList);
            }
            else {
                forwardTo = "error";
            }

            return mapping.findForward(forwardTo);
        }


        if (shopIdParameter.isSet()) {	
            shopAdmin.setSearchExpression(shopId);
        }
       	 	

        if (requestParser.getParameter("StartSearch").isSet()
				&& requestParser.getParameter("StartSearch").getValue().getString().length() > 0) {

            ResultData shopList = shopAdmin.readShopList(shopId,scenarioValue);

            if (shopList != null) {
                request.setAttribute(ActionConstants.RC_SHOP_LIST, shopList);
            }
            else {
                forwardTo = "error";
            }

        }

        return mapping.findForward(forwardTo);
    }


}
