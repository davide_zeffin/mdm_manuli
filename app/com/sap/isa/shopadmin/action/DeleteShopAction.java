/*****************************************************************************
    Class         DeleteShopAction
    Copyright (c) 2001, SAPMarkets Europe GmbH, Germany, All rights reserved.
    Description:  Action to display an shop list
    Author:       Wolfgang Sattler
    Created:      February 2002
    Version:      1.0

    $Revision: #3 $
    $Date: 2001/07/04 $
*****************************************************************************/
package com.sap.isa.shopadmin.action;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.sap.isa.businessobject.CommunicationException;
import com.sap.isa.core.TechKey;
import com.sap.isa.core.UserSessionData;
import com.sap.isa.core.util.Message;
import com.sap.isa.core.util.MessageList;
import com.sap.isa.core.util.RequestParser;
import com.sap.isa.shopadmin.businessobject.Shop;
import com.sap.isa.shopadmin.businessobject.ShopAdmin;
import com.sap.isa.shopadmin.businessobject.ShopAdminBusinessObjectManager;

/**
 * Maintain an generic object
 *
 * This action retrieves a list of the available shops from the system and
 * puts the data in the request context to allow a JSP the rendering of the
 * data. If only one shop is found, the control is directly forwarded to the
 * action reading the shop data.
 * </p>
 * <p>
 * <em>Input:</em>
 * <ul>
 *   <li><b><code>none</code></b>
 * </ul>
 * <br>
 * <em>Output:</em>
 * <ul>
 *   <li><b><code>Request-Attribute->RK_SHOP_LIST</code></b> - List of the
 *       shops found as a <code>ResultData</code> object.
 *   <li><b><code>Request-Attribute->PARAM_SHOPID</code></b> - Id of the shop
 *       if only one shop was found. In this case the control is directly
 *       forwarded to the action, which reads the shop.
 * </ul>
 *
 * @author Wolfgang Sattler
 * @version 1.0
 *
 */
public class DeleteShopAction extends ShopAdminBaseAction {


    /**
     * Overriden <em>isaPerform</em> method of <em>IsaCoreBaseAction</em>.
     */
    public ActionForward performAction(ActionMapping mapping,
                ActionForm form,
                HttpServletRequest request,
                HttpServletResponse response,
                UserSessionData userSessionData,
                RequestParser requestParser,
                ShopAdminBusinessObjectManager bom)
                throws CommunicationException {

        // Page that should be displayed next.
        String forwardTo = "welcome";

        String forwardAfter=requestParser.getParameter(ActionConstants.RC_FORWARD).getValue().getString();

        // forward and back are only for search
        if (forwardAfter.length() > 0) {
            forwardTo = "showsearch";
        }

        RequestParser.Parameter shopKey = requestParser.getParameter("shopKey");

        if (!shopKey.isSet()) {
            shopKey = requestParser.getAttribute("shopKey");
        }

        if (shopKey.isSet()) {

			MessageList messageList = null;

            ShopAdmin shopAdmin = bom.createShopAdmin();

			// first lock the shop            
            Shop shop = bom.createShop();

			if (shop.getTechKey().getIdAsString().equals(shopKey)) {
	            shop.setTechKey(new TechKey(shopKey.getValue().getString()));	            			
			}			
			
			shop.lock();
            if (shop.isValid()) {            				            	
				shopAdmin.deleteShop(new TechKey(shopKey.getValue().getString()));
				shop.unlock();
	            if (!shopAdmin.isValid()) {            				            	
	            	messageList = shopAdmin.getMessageList();
	            }	
            }
            else {
            	messageList = shop.getMessageList();           	
            }		
			
            if (shop.isValid() && shopAdmin.isValid()) {            				            	
                messageList = new MessageList();
                messageList.add(new Message(Message.INFO,"shopadmin.jsp.confirmDeletion",new String[]{shopKey.getValue().getString()},""));
            }
            
            request.setAttribute(ActionConstants.RC_MESSAGES,messageList);          
        }

        else {
            forwardTo = "error";
        }


        return mapping.findForward(forwardTo);
    }


}
