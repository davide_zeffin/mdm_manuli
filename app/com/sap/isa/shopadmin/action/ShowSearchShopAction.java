/*****************************************************************************
    Class         ShowSearchShopAction
    Copyright (c) 2001, SAPMarkets Europe GmbH, Germany, All rights reserved.
    Description:  Action to display an shop list
    Author:       Wolfgang Sattler
    Created:      February 2002
    Version:      1.0

    $Revision: #3 $
    $Date: 2001/07/04 $
*****************************************************************************/
package com.sap.isa.shopadmin.action;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.sap.isa.businessobject.CommunicationException;
import com.sap.isa.core.UserSessionData;
import com.sap.isa.core.util.RequestParser;
import com.sap.isa.core.util.table.ResultData;
import com.sap.isa.maintenanceobject.backend.boi.PropertyData;
import com.sap.isa.maintenanceobject.businessobject.Property;
import com.sap.isa.shopadmin.businessobject.Shop;
import com.sap.isa.shopadmin.businessobject.ShopAdmin;
import com.sap.isa.shopadmin.businessobject.ShopAdminBusinessObjectManager;

/**
 * Search an generic shop object
 *
 * <h4>Overview over request parameters and attributes</h4>
 * <table border="1" cellspacing="0" cellpadding="4">
 *   <tr>
 *     <th>name</th>
 *     <th>parameter</th>
 *     <th>attribute</th>
 *     <th>description</th>
 *   <tr>
 *   <tr>
 *      <td>CancelSearch</td><td>X</td><td>&nbsp</td>
 *      <td>user has pressed the <i>cancel</i> button</td>
 *   </tr>
 *   <tr>
 *      <td>SearchAll</td><td>X</td><td>&nbsp</td>
 *      <td>user has pressed the <i>search all</i> button</td>
 *   </tr>
 *   <tr>
 *      <td>Search</td><td>X</td><td>&nbsp</td>
 *      <td>user has pressed the <i>search</i> button</td>
 *   </tr>
 *   <tr>
 *      <td>backTo</td><td>X</td><td>&nbsp</td>
 *      <td>logical forward to go back</td>
 *   </tr>
 *   <tr>
 *      <td>forwardAfter</td><td>X</td><td>&nbsp</td>
 *      <td>logical forward to use after finding a shop</td>
 *   </tr>
 *   <tr>
 *      <td>shopId</td><td>X</td><td>&nbsp</td>
 *      <td>entered shop id</td>
 *   </tr>
 *   <tr>
 *      <td>scenario</td><td>X</td><td>&nbsp</td>
 *      <td>entered scenario</td>
 *   </tr>
 * </table>
 *
 * <h4>The following forwards are used by this action</h4>
 * <table border="1" cellspacing="0" cellpadding="2">
 *   <tr>
 *     <th>forward</th>
 *     <th>description</th>
 *   <tr>
 *   <tr><td>searchshop</td><td>standard forward</td></tr>
 *   <tr><td>error</td><td>an application error occurs</td></tr>
 *   <tr><td>values of <code>backTo</code></td><td>forward is given with backTo Parameter </td></tr>
 * </table>
 * <h4>Overview over attributes which will be set in the request context</h4>
 * <table border="1" cellspacing="0" cellpadding="2">
 *   <tr>
 *     <th>attribute</th>
 *     <th>description</th>
 *   <tr>
 * <tr><td><code>ActionConstant.RC_SCENARIO</code></td><td>scenario property to 
 * display the allowed values for scenario property</td></tr>
 * </table>
 *
 *
 * @author SAP
 * @version 1.0
 *
 */
public class ShowSearchShopAction extends ShopAdminBaseAction {


    /**
     * Overriden <em>isaPerform</em> method of <em>IsaCoreBaseAction</em>.
     */
    public ActionForward performAction(ActionMapping mapping,
                ActionForm form,
                HttpServletRequest request,
                HttpServletResponse response,
                UserSessionData userSessionData,
                RequestParser requestParser,
                ShopAdminBusinessObjectManager bom)
                throws CommunicationException {

        // Page that should be displayed next.
        String forwardTo = "showsearch";
      
        String backTo=requestParser.getParameter(ActionConstants.RC_BACK).getValue().getString();
        String forwardAfter=requestParser.getParameter(ActionConstants.RC_FORWARD).getValue().getString();

        request.setAttribute(ActionConstants.RC_BACK,backTo);
        request.setAttribute(ActionConstants.RC_FORWARD,forwardAfter);

		// Get the Shop, to get all allowed values for the scenrio 
		Shop shop = bom.createShop();

		PropertyData scenario = shop.getProperty("scenario");

		if (scenario== null) {		
			scenario = new Property("scenario");
		}	

		request.setAttribute(ActionConstants.RC_SCENARIO, scenario);

        // set shop into page context mayby it will be displayed in the next step
        ShopAdmin shopAdmin = bom.createShopAdmin();

        ResultData shopList = shopAdmin.getShopList();

        if (shopList != null) {
            request.setAttribute(ActionConstants.RC_SHOP_LIST, shopList);
        }
        
        request.setAttribute(ActionConstants.RC_SHOP_ADMIN,shopAdmin);          


        return mapping.findForward(forwardTo);
    }


}
