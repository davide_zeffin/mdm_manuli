/*****************************************************************************
    Class:        ActionConstants
    Copyright (c) 2001, SAPMarkets Europe GmbH, Germany, All rights reserved.
    Author:       Wolfgang Sattler
    Created:      February 2002
    Version:      1.0

    $Revision: #2 $
    $Date: 2001/07/03 $
*****************************************************************************/

package com.sap.isa.shopadmin.action;

/**
 * this class defines some constants for request attributes (RA) which are used
 * in actions to store and retrieve data from request context.
 */
public class ActionConstants {

  /**
   * key to store item key in request context
   */
  public final static String RA_FORWARD              = "forward";

  public final static String RC_FORWARD              = "forward";

  public final static String RC_READONLY             = "readonly";

  public final static String RC_BACK                 = "back";

  public final static String RC_SHOP                 = "shop";

  public final static String RC_SCENARIO             = "scenario";

  public final static String RC_SHOP_ID              = "shopId";

  public final static String RC_SHOP_KEY             = "shopKey";

  public final static String RC_SHOP_LIST            = "shoplist";

  public final static String RC_SHOP_ADMIN           = "shopadmin";

  public final static String RC_USER                 = "user";

  public final static String RC_MESSAGES             = "messagelist";

  public final static String RC_HELP_VALUES          = "helpvalues";

}