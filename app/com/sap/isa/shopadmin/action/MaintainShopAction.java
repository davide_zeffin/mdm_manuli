/*****************************************************************************
    Class         MaintainShopAction
    Copyright (c) 2001, SAPMarkets Europe GmbH, Germany, All rights reserved.
    Description:  Action to maintain a generic shop object
    Author:       SAP
    Created:      February 2002
    Version:      1.0

    $Revision: #3 $
    $Date: 2001/07/04 $
*****************************************************************************/
package com.sap.isa.shopadmin.action;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.sap.isa.businessobject.BusinessObjectBase;
import com.sap.isa.businessobject.CommunicationException;
import com.sap.isa.businessobject.GenericBusinessObjectManager;
import com.sap.isa.core.UserSessionData;
import com.sap.isa.core.util.Message;
import com.sap.isa.core.util.MessageList;
import com.sap.isa.core.util.RequestParser;
import com.sap.isa.maintenanceobject.action.MaintainObjectAction;
import com.sap.isa.shopadmin.businessobject.Shop;
import com.sap.isa.shopadmin.businessobject.ShopAdmin;
import com.sap.isa.shopadmin.businessobject.ShopAdminBusinessObjectManager;

/**
 * Maintain an generic shop object.
 *
 * <h4>Overview over request parameters and attributes</h4>
 * <table border="1" cellspacing="0" cellpadding="4">
 *   <tr>
 *     <th>name</th>
 *     <th>parameter</th>
 *     <th>attribute</th>
 *     <th>description</th>
 *   <tr>
 *   <tr>
 *      <td>CancelShop</td><td>X</td><td>&nbsp</td>
 *      <td>user has pressed the <i>cancel</i> button</td>
 *   </tr>
 *   <tr>
 *      <td>ChangeShop</td><td>X</td><td>&nbsp</td>
 *      <td>user has pressed the <i>change</i> button</td>
 *   </tr>
 *   <tr>
 *      <td>DeleteShop</td><td>X</td><td>&nbsp</td>
 *      <td>user has pressed the <i>delete</i> button</td>
 *   </tr>
 *   <tr>
 *      <td>CopyShop</td><td>X</td><td>&nbsp</td>
 *      <td>user has pressed the <i>copy</i> button</td>
 *   </tr>
 *   <tr>
 *      <td>SaveShop</td><td>X</td><td>&nbsp</td>
 *      <td>user has pressed the <i>save</i> button</td>
 *   </tr>
 *   <tr>
 *      <td>HelpValues</td><td>X</td><td>&nbsp</td>
 *      <td>user has pressed the help button. Contains the property name</td>
 *   </tr>
 *   <tr>
 *      <td><i>name of a property</i></td><td>X</td><td>&nbsp</td>
 *      <td>the values of the propteries are stored in request parameter with the properties names</td>
 *   </tr>
 * </table>
 *
 * <h4>The following forwards are used by this action</h4>
 * <table border="1" cellspacing="0" cellpadding="2">
 *   <tr>
 *     <th>forward</th>
 *     <th>description</th>
 *   <tr>
 *   <tr><td>showshop</td><td>standard forward</td></tr>
 *   <tr><td>canceled</td><td>user has pressed the <i>chancel</i> button</td></tr>
 *   <tr><td>changeshop</td><td>user has pressed the <i>change</i> button</td></tr>
 *   <tr><td>copyshop</td><td>user has pressed the <i>copy</i> button</td></tr>
 *   <tr><td>deleteshop</td><td>user has pressed the <i>delete</i> button</td> <</tr>
 *   <tr><td>helpvalues</td><td>help values should be displayed</td></tr>
 *   <tr><td>saved</td><td>the shop data was saved successfully </td></tr>
 *   <tr><td>error</td><td>an application error occurs</td></tr>
 * </table>
 * <h4>Overview over attributes which will be set in the request context</h4>
 * <table border="1" cellspacing="0" cellpadding="2">
 *   <tr>
 *     <th>attribute</th>
 *     <th>description</th>
 *   <tr>
 * <tr><td>ActionConstants.RC_HELP_VALUES</td><td>reference to the help values</td></tr>
 * </table>
 *
 *
 * @author SAP
 * @version 1.0
 *
 */
public class MaintainShopAction extends ShopAdminBaseAction {

    /**
     * user exit do Parse the request
     */
    protected void parseExit( HttpServletRequest request,
                              UserSessionData userSessionData,
                              RequestParser requestParser,
                              ShopAdminBusinessObjectManager bom,
                              Shop shop) {


    }


   /**
     * Generic user exit do Parse the request for the  business object.
     * overwrite this method to allow generic user exit.
     *
     * @param request           The request object
     * @param userSessionData   Object wrapping the session
     * @param requestParser     Parser to simple retrieve data from the request
     * @param bob               the business object
     */
    public void parseExit( HttpServletRequest request,
                           UserSessionData userSessionData,
                           RequestParser requestParser,
			               GenericBusinessObjectManager bom,
                           BusinessObjectBase bob) {
                              	
		parseExit(request,userSessionData, requestParser,(ShopAdminBusinessObjectManager)bom  ,(Shop)bob);
                              	

    }

    /**
     * Overriden <em>isaPerform</em> method of <em>IsaCoreBaseAction</em>.
     */
    public ActionForward performAction(ActionMapping mapping,
                ActionForm form,
                HttpServletRequest request,
                HttpServletResponse response,
                UserSessionData userSessionData,
                RequestParser requestParser,
                ShopAdminBusinessObjectManager bom)
                    throws CommunicationException {

        // Page that should be displayed next.
        String forwardTo = "showshop";

        Shop shop = bom.getShop();

        if (shop == null) {
                return mapping.findForward("error");
        }

        shop.clearMessages();

        request.setAttribute(ActionConstants.RC_SHOP, shop);

        // first dispatch
        if (requestParser.getParameter("CancelShop").isSet() 
				&& requestParser.getParameter("CancelShop").getValue().getString().length() > 0) {

            forwardTo = "canceled";

            return returnForward(mapping,forwardTo,shop);
        }


        if (requestParser.getParameter("ChangeShop").isSet()
				&& requestParser.getParameter("ChangeShop").getValue().getString().length() > 0) {

            // set the shop Id in request attribute
            request.setAttribute("shopKey", shop.getId());

            // changing from readonly to changing
            forwardTo = "changeshop";

            return returnForward(mapping,forwardTo,shop);
        }

        if (requestParser.getParameter("DeleteShop").isSet()
            && requestParser.getParameter("DeleteShop").getValue().getString().equals("true")) {

            // set the shop Id in request attribute
            request.setAttribute("shopKey", shop.getId());

            // changing from readonly to changing
            forwardTo = "deleteshop";

            return returnForward(mapping,forwardTo,shop);
        }


        if (requestParser.getParameter("CopyShop").isSet()
                && requestParser.getParameter("CopyShop").getValue().getString().equals("true")) {

            forwardTo = "copyshop";

            // parse shop id
            shop.setId(requestParser.getParameter("shopId").getValue().getString());

            return returnForward(mapping,forwardTo,shop);
        }

        // parse the request and read all property values
        MaintainObjectAction.parseRequest(request, userSessionData, requestParser, bom, shop, this);

        if (!shop.isReadOnly()&& !shop.isValid()) {
			return returnForward(mapping,"showshop",shop);
        }
        
        // check for help values
        if (requestParser.getParameter("HelpValues").isSet()
        	&& requestParser.getParameter("HelpValues").getValue().getString().length() > 0) {

			String propertyName = requestParser.getParameter("HelpValues").getValue().getString();
		    if (MaintainObjectAction.showHelpValues(request, shop, propertyName)) {
	              return returnForward(mapping,"helpvalues",shop);
		    }
        }        		


		// check for the generic help values
		if (requestParser.getParameter("helpValuesSearch").isSet()
			&& requestParser.getParameter("helpValuesSearch").getValue().getString().length() > 0) {

			return returnForward(mapping,"gethelpvalues",shop);
		}        		


        if (requestParser.getParameter("SaveShop").isSet()
				&& requestParser.getParameter("SaveShop").getValue().getString().length() > 0) {

			shop.checkDependencies();
            MaintainObjectAction.checkRequiredProperties(shop,"shopadmin.jsp.requiredField");

            if (!shop.isValid()) {
                return returnForward(mapping,"showshop",shop);
            }

			boolean isNew = shop.isNewObject();
			
            shop.saveData();

            if (shop.isValid()) {
                
                ShopAdmin shopAdmin = bom.createShopAdmin();

                if (isNew) {
                	shopAdmin.resetShopList();    
                } 
                else { 
                    shopAdmin.updateShopList(shop);                        
                }                
                
                forwardTo = "saved";
                MessageList messageList = new MessageList();
                messageList.add(new Message(Message.INFO,"shopadmin.jsp.confirmSave"));
                request.setAttribute(ActionConstants.RC_MESSAGES,messageList);
            }
            else {
                forwardTo = "showshop";
            }
        }

        return returnForward(mapping,forwardTo,shop);
    }



    private ActionForward returnForward(ActionMapping mapping,
                                        String forward,
                                        Shop shop)
            throws CommunicationException {

            shop.unlock();

            return mapping.findForward(forward);
    }


}
