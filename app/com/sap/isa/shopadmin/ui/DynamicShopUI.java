package com.sap.isa.shopadmin.ui;

import javax.servlet.jsp.PageContext;

import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.shopadmin.action.ActionConstants;
import com.sap.isa.shopadmin.businessobject.Shop;
import com.sap.isa.ui.uiclass.EnhancedMaintenanceObjectUI;

public class DynamicShopUI extends EnhancedMaintenanceObjectUI {

    protected Shop shop;

	final static private IsaLocation log = IsaLocation.getInstance(DynamicShopUI.class.getName());
	
	/**
	 * Constructor for ShopUI.
	 * 
	 * @param pageContext
	 */
	public DynamicShopUI(PageContext pageContext) {
		initContext(pageContext);
	}

	public void initContext(PageContext pageContext) {
		final String METHOD_NAME = "initContext(PageContext)";
		log.entering(METHOD_NAME);

		super.initContext(pageContext);
		
		log.exiting();
	}
	
	
	protected void determineDynamicUI() {
        shop = (Shop)request.getAttribute(ActionConstants.RC_SHOP);
        object = shop;
	}

	protected void determineReadOnly() {
        readOnly = (String)request.getAttribute(ActionConstants.RC_READONLY);        
	}

	protected void determineFormName() {
        formName = "shopform";
	}

	protected void determineHelpAction() {
        helpAction = "shopadmin/showhelp.do";
	}
	
	protected void determinKeyPressEventFunction() {
		keyPressEventFunction = "";
	}

    /**
     * Return the shop object. <br> 
     *
     * @return Returns the {@link #shop}.
     */
    public Shop getShop() {
        return shop;
    }

}
