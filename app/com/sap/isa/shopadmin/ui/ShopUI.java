/*****************************************************************************
    Class:        ShopUI
    Copyright (c) 2004, SAP AG, Germany, All rights reserved.
    Author:       SAP AG
    Created:      25.10.2004
    Version:      1.0

    $Revision: #13 $
    $Date: 2003/05/06 $ 
*****************************************************************************/

package com.sap.isa.shopadmin.ui;

import javax.servlet.jsp.PageContext;

import com.sap.isa.shopadmin.action.ActionConstants;
import com.sap.isa.shopadmin.businessobject.Shop;
import com.sap.isa.ui.uiclass.MaintenanceObjectUI;

//TODO docu
/**
 * The class ShopUI . <br>
 *
 * @author  SAP AG
 * @version 1.0
 */
public class ShopUI extends MaintenanceObjectUI {


    protected Shop shop;
    
    
	/**
	 * Constructor for ShopUI.
	 * 
	 * @param pageContext
	 */
	public ShopUI(PageContext pageContext) {
		initContext(pageContext);
	}
 
     
 
    /**
     * Determine the shop object from {@link ActionConstants#RC_SHOP}. <br>
     *  
     * @see com.sap.isa.ui.uiclass.MaintenanceObjectUI#determineMaintenanceObject()
     */
    protected void determineMaintenanceObject() {
        shop = (Shop)request.getAttribute(ActionConstants.RC_SHOP);
        object = shop;
    }

    
    /**
     * Determine the readOnly String from {@link ActionConstants#RC_READONLY}. <br>
     * 
     * @see com.sap.isa.ui.uiclass.MaintenanceObjectUI#determineReadOnly()
     */
    protected void determineReadOnly() {
        readOnly = (String)request.getAttribute(ActionConstants.RC_READONLY);        
    }

    
    /**
     * Sets the formname to <code>shopform</code>. <br>
     * 
     * @see com.sap.isa.ui.uiclass.MaintenanceObjectUI#determineFormName()
     */
    protected void determineFormName() {
        formName = "shopform";
    }
    

    /**
     * Sets the help action to <code>shopform</code> . <br>
     * 
     * @see com.sap.isa.ui.uiclass.MaintenanceObjectUI#determineHelpAction()
     */
    protected void determineHelpAction() {
        helpAction = "shopadmin/showhelp.do";
    }

    
    /**
     * Return the shop object. <br> 
     *
     * @return Returns the {@link #shop}.
     */
    public Shop getShop() {
        return shop;
    }
}
