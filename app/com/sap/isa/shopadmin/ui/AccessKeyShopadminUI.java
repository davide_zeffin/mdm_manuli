/*
 * Created on Dec 21, 2004
 *
 * To change the template for this generated file go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
package com.sap.isa.shopadmin.ui;

import com.sap.isa.ui.accessibility.AccessKeysForApplication;
import com.sap.isa.ui.uiclass.AccessKeyUI;

/**
 * @author i800855
 *
 * To change the template for this generated type comment go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
public class AccessKeyShopadminUI extends AccessKeyUI
{
	protected void initAccessKeys()
	{
		AccessKeysForApplication app = addApplication("app.shopadmin");
		app.addAccessKey("shopadmin.beginpage.access", "shopadmin.beginpage.access.inf");
		app.addAccessKey("shopadmin.endpage.access", "shopadmin.endpage.access.inf");
		app.addAccessKey("shopadmin.main.access", "shopadmin.main.access.inf");
		app.addAccessKey("shopadmin.buttonarea.access", "shopadmin.buttonarea.access.inf");
	}
}
