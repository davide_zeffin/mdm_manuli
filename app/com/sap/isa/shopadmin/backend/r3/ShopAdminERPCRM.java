/*
 * Created on 14.02.2008
 *
 * To change the template for this generated file go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
package com.sap.isa.shopadmin.backend.r3;




import com.sap.isa.core.eai.BackendException;
import com.sap.isa.maintenanceobject.backend.boi.MaintenanceObjectData;
import com.sap.isa.maintenanceobject.backend.boi.PropertyData;
import com.sap.isa.shopadmin.backend.boi.ShopData;



public class ShopAdminERPCRM extends ShopAdminR3 
		{

			private boolean isBestsellerAvailable;
			private String bestsellerTargetGroup;	
			private boolean isRecommendationAvailable;	
			private boolean isPersonalRecommendationAvailable;
			private boolean isCuaAvailable;				
			private boolean isCuaPersonalized;
			private String attributeSetId;
			private String methodeSchema;
			
			/**
			 * Set the property isBestsellerAvailable
			 *
			 * @param isBestsellerAvailable
			 */
			public void setBestsellerAvailable(boolean isBestsellerAvailable) {
				this.isBestsellerAvailable = isBestsellerAvailable;
			}

			/**
			 * Returns the property isBestsellerAvailable
			 *
			 * @return isBestsellerAvailable
			 */
			public boolean isBestsellerAvailable() {
				return isBestsellerAvailable;
			}
			
			
			/**
			  * Set the property bestsellerTargetGroup
			  *
			  * @param bestsellerTargetGroup
			  *
			  */
			public void setBestsellerTargetGroup(String bestsellerTargetGroup) {
				 this.bestsellerTargetGroup = bestsellerTargetGroup;
			}

			 /**
			  * Returns the property bestsellerTargetGroup
			  *
			  * @return bestsellerTargetGroup
			  *
			  */
			public String getBestsellerTargetGroup() {
				 return this.bestsellerTargetGroup;
			}

			/**
			 * Set the property isRecommendationAvailable
			 *
			 * @param isRecommendationAvailable
			 */
		  	public void setRecommendationAvailable(boolean isRecommendationAvailable) {
				this.isRecommendationAvailable = isRecommendationAvailable;
			}

			/**
			 * Returns the property isRecommendationAvailable
			 *
			 * @return isRecommendationAvailable
			 */
			public boolean isRecommendationAvailable() {
				return isRecommendationAvailable;
			}

			/**
			 * Set the property isCuaAvailable
			 *
			 * @param isCuaAvailable
			 */
			public void setCuaAvailable(boolean isCuaAvailable) {
				this.isCuaAvailable = isCuaAvailable;
			}

			/**
			 * Returns the property isCuaAvailable
			 *
			 * @return isCuaAvailable
			 */
			public boolean isCuaAvailable() {
				return isCuaAvailable;
			}

 
			/**
			 * Set the property personalRecommendationAvailable
			 *
			 * @param personalRecommendationAvailable
			 *
			 */
			public void setPersonalRecommendationAvailable(boolean personalRecommendationAvailable) {
				this.isPersonalRecommendationAvailable = personalRecommendationAvailable;
			}

			/**
			 * Returns the property personalRecommendationAvailable
			 *
			 * @return personalRecommendationAvailable
			 *
			 */
			public boolean isPersonalRecommendationAvailable() {
				return this.isPersonalRecommendationAvailable;
			}

			/**
			 * Set the property isCuaPersonalized
			 *
			 * @param isCuaPersonalized
			 *
			 */
			public void setCuaPersonalized(boolean cuaPersonalized) {
				this.isCuaPersonalized = cuaPersonalized;
			}

			/**
			 * Returns the property isCuaPersonalized
			 *
			 * @return isCuaPersonalized
			 *
			 */
			public boolean isCuaPersonalized() {
				return this.isCuaPersonalized;
			}
			
		    /**
		     * Returns the property attributeSetId
		     *
		     * @return attributeSetId
		     *
		     */
			public String getAttributeSetId() {
				return attributeSetId;
			}
			
		    /**
		     * Set the property attributeSetId
		     *
		     * @param attributeSetId
		     *
		     */
			public void setAttributeSetId(String attributeSetId) {
				this.attributeSetId = attributeSetId;
			}
			
		    /**
		     * Returns the property methodeSchema
		     *
		     * @return methodeSchema
		     *
		     */
			public String getMethodeSchema() {
				return methodeSchema;
			}
			
  		    /**
		     * Set the property methodeSchema
		     *
		     * @param methodeSchema
		     *
		     */
			public void setMethodeSchema(String methodeSchema) {
				this.methodeSchema = methodeSchema;
			}



     		protected void setPropertyFromFlag(MaintenanceObjectData object,
											  String groupName,
											  String propertyName,	 
											  boolean flag) {

				PropertyData property = object.getProperty(groupName, propertyName);
        
				if (property != null) {
					if (flag) {
						property.setHidden(false);
						property.setRequired(true);
					}
					else {
						property.setHidden(true);
						property.setRequired(false);        	
					}
				}
        
			}	



			public void checkDependencies(ShopData shopData) throws BackendException {
    	
				PropertyData property;
				
				super.checkDependencies(shopData);
				
	            property = shopData.getProperty("bestseller", "bestsellerAvailable");
	            property.setHidden(false);
	            property = shopData.getProperty("personalRecommendation", "personalRecommendationAvailable");
	            property.setHidden(false);
	            property = shopData.getProperty("cua", "cuaAvailable");
	            property.setHidden(false);	    
				
				isBestsellerAvailable = isBestsellerAvailable();
				isPersonalRecommendationAvailable = isPersonalRecommendationAvailable();
				isCuaAvailable = isCuaAvailable();
				isCuaPersonalized = isCuaPersonalized();
	
				// bestseller
				setPropertyFromFlag(shopData, "bestseller", 
									"bestsellerTargetGroup", isBestsellerAvailable );
				// personalRecommendation
				setPropertyFromFlag(shopData, "personalRecommendation", 
									"attributeSetId", isPersonalRecommendationAvailable );

				// cua
				setPropertyFromFlag(shopData, "cua", "methodeSchema", isCuaAvailable );
				setPropertyFromFlag(shopData, "cua", "globalCuaTargetGroup", isCuaAvailable && !isCuaPersonalized);
				setPropertyFromFlag(shopData, "cua", "cuaTargetGroup", isCuaAvailable && isCuaPersonalized);

				property = shopData.getProperty("cua", "cuaPersonalized");
		        if (isCuaAvailable) {
		            property.setHidden(false);
		        }
		        else {
		            property.setHidden(true);
		        }

			}
			
			
			
			/**
			 * Adjust properties depending from the scenario 
			 * 
			 * @param shopData shop
			 * @param scenario scenario
			 */
			public void adjustToScenario(ShopData shopData, String scenario)
								  throws BackendException {


				super.adjustToScenario(shopData, scenario);
				
//					// bestseller
//					PropertyData property = shopData.getProperty("bestseller", 
//																 "bestsellerTargetGroup");
//
//					boolean isHidden = property.isHidden();
//					String value = property.getString();
//
//					property = shopData.getProperty("bestseller", "bestsellerAvailable");
//					property.setHidden(isHidden);
//					property.setAllowed(!isHidden);
//					property.setValue(value.length() > 0);
//
//
//					// personalRecommendation
//					property = shopData.getProperty("personalRecommendation", 
//													"attributeSetId");
//					isHidden = property.isHidden();
//					value = property.getString();
//
//					property = shopData.getProperty("personalRecommendation", 
//													"personalRecommendationAvailable");
//					property.setHidden(isHidden);
//					property.setAllowed(!isHidden);
//					property.setValue(value.length() > 0);
//
//
//					// cua
//					property = shopData.getProperty("cua", "methodeSchema");
//					isHidden = property.isHidden();
//					value = property.getString();
//
//					property = shopData.getProperty("cua", "cuaAvailable");
//					property.setAllowed(!isHidden);
//					property.setHidden(isHidden);
//					property.setValue(value.length() > 0);
//
//					
//					// adjust the values of the backend object to the changes!
//					shopData.synchronizeBeanProperties(this);  					
			}		

			/*
			 * set internal properties depending from other properties
			 * 
			 * @param shopData 
			 * 
			 */
			 private final void adjustPrivateProperties(MaintenanceObjectData shopData) {
		    	
		        if (getAttributeSetId() != null && getAttributeSetId().length() > 0) {
		            setPersonalRecommendationAvailable(true);
		        }
		        else {
		            setPersonalRecommendationAvailable(false);
		        }

		        if (getBestsellerTargetGroup() != null && getBestsellerTargetGroup().length() > 0) {
		            setBestsellerAvailable(true);
		        }
		        else {
		            setBestsellerAvailable(false);
		        }

		        if (getMethodeSchema() != null && getMethodeSchema().length() > 0) {
		            setCuaAvailable(true);
		        }
		        else {
		            setCuaAvailable(false);
		        }			
			 }

			// adjust CRM shop data regarding the private shop properties.
			 private void adjustShopData(ShopData shopData) {
								 
				// delete marketing infos depending the check boxes			
				if (!isPersonalRecommendationAvailable()) {
					PropertyData attributeSetId = shopData.getProperty("attributeSetId");
					attributeSetId.clearValue();
					//"PROFILE_TEMPL_ID");
				}
			       
				if (!isBestsellerAvailable()) {
					PropertyData bestsellerTargetGroup = shopData.getProperty("bestsellerTargetGroup");
					bestsellerTargetGroup.clearValue();
					// "TARGET_GRP_DESCR");
				}
			       
				if (!isCuaAvailable()) {
					PropertyData methodeSchema = shopData.getProperty("methodeSchema");
					methodeSchema.clearValue();
					// "METHOD_SCHEME_ID");
					PropertyData globalTargetGroup = shopData.getProperty("globalCuaTargetGroup");
					globalTargetGroup.clearValue();
				}
				if (!isCuaPersonalized()) {
					PropertyData cuaTargetGroup = shopData.getProperty("cuaTargetGroup");
					cuaTargetGroup.clearValue();
				}
			 }
			 
			 /*
			  *  This method read the values of the maintenance object from the backend
			  *
			  *@param  shopData              Description of Parameter
			  *@exception  BackendException  Description of Exception
			  */
		     public void readData(ShopData shopData)
		                 throws BackendException {
		    	 super.readData(shopData);
		    	 adjustPrivateProperties(shopData);
		     }	 

		     /*
		      *  This method save the maintenance object in backend
		      *
		      *@param  shopObject            Description of Parameter
		      *@exception  BackendException  Description of Exception
		      */
		      public void saveData(ShopData shopObject)
		                  throws BackendException {

		    	  adjustShopData(shopObject);
		    	  super.saveData(shopObject);
		      }
		     
}