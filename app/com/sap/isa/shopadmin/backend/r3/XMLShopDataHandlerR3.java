/*****************************************************************************
    Class:        XMLShopDataHandler
    Copyright (c) 2001, SAPMarkets Europe GmbH, Germany, All rights reserved.
    Author:       Wolfgang Sattler/ Christoph Hinssen
    Created:      February 2002
    Version:      1.0

    $Revision: #3 $
    $Date: 2001/07/24 $
*****************************************************************************/
package com.sap.isa.shopadmin.backend.r3;

import java.util.Properties;

import com.sap.isa.core.eai.ConnectionFactory;
import com.sap.isa.maintenanceobject.backend.MaintenanceObjectHelperException;
import com.sap.isa.shopadmin.backend.XMLShopDataHandler;


/**
 *
 * @author Christoph Hinssen
 * @version 1.0
 *
 */
public class XMLShopDataHandlerR3 extends XMLShopDataHandler
{

    /**
     * This method initialize the maintenance object handler in the backend.
     *
     * @param props a set of properties which may be useful to initialize the object
     * @param params a object which wraps parameters
     */
    public void initHandler(ConnectionFactory connectionFactory, Properties props)
            throws MaintenanceObjectHelperException {

        super.initHandler(connectionFactory, props);
        additionalProperties.add("language");
    }

}
