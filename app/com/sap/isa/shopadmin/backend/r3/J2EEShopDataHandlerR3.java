/*
 * Created on Dec 20, 2004
 *
 * To change the template for this generated file go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
package com.sap.isa.shopadmin.backend.r3;

import java.util.Properties;

import com.sap.isa.core.eai.ConnectionFactory;
import com.sap.isa.maintenanceobject.backend.MaintenanceObjectHelperException;
import com.sap.isa.shopadmin.backend.ShopDBDataHandler;


/**
 * @author I803067
 *
 * To change the template for this generated type comment go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
public class J2EEShopDataHandlerR3 extends ShopDBDataHandler {

	/* (non-Javadoc)
	 * @see com.sap.isa.maintenanceobject.backend.MaintenanceObjectDataHandler#initHandler(com.sap.isa.core.eai.ConnectionFactory, java.util.Properties)
	 */
	public void initHandler(ConnectionFactory arg0, Properties arg1)
		throws MaintenanceObjectHelperException {
		super.initHandler(arg0, arg1);
		additionalProperties.add("scenario");
		additionalProperties.add("b2c");
		additionalProperties.add("b2b");
		additionalProperties.add("language");
	}

}
