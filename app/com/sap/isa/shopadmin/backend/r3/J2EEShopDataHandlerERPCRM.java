/*****************************************************************************
    Class:        J2EEShopDataHandlerERPCRM
    Copyright (c) 2008, SAP AG, Germany, All rights reserved.
    Author:       SAP AG
    Created:      03.03.2008
    Version:      1.0

    $Revision: #13 $
    $Date: 2003/05/06 $ 
*****************************************************************************/

package com.sap.isa.shopadmin.backend.r3;

/**
 * The class J2EEShopDataHandlerERPCRM overwrites the "HEADER" and "CHILDREN" attribute of the class
 * {@link com.sap.isa.shopadmin.backend.ShopDBDataHandler} to separate the shops between pure ERP and 
 * ERP/CRM scenario.  <br>
 *
 * @author  SAP AG
 * @version 1.0
 */
public class J2EEShopDataHandlerERPCRM extends J2EEShopDataHandlerR3 {
	
	/**
	 * Adjust the name of the structures. <br>
	 */
	protected void adjustConstants() {
		super.adjustConstants();
		HEADER = "ShopDataERPCRM-HEADER";
		CHILDREN = "ShopDataERPCRM-CHILDREN";
	}
	

}
