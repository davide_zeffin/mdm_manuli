/**
 *  Class: ShopAdminBackend Copyright (c) 2001, SAPMarkets Europe GmbH, Germany,
 *  All rights reserved. Author: Wolfgang Sattler/Christoph Hinssen Created:
 *  February 2002 Version: 1.0 $Revision: #3 $ $Date: 2001/07/24 $
 */

package com.sap.isa.shopadmin.backend.r3;

import java.util.ArrayList;
import java.util.List;
import java.util.Properties;
import java.util.StringTokenizer;

import com.sap.isa.backend.IsaBackendBusinessObjectBaseSAP;
import com.sap.isa.core.TechKey;
import com.sap.isa.core.eai.BackendBusinessObjectParams;
import com.sap.isa.core.eai.BackendException;
import com.sap.isa.core.eai.sp.jco.JCoConnection;
import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.core.logging.LogUtil;
import com.sap.isa.core.util.Message;
import com.sap.isa.core.util.table.ResultData;
import com.sap.isa.maintenanceobject.backend.EnqueueObjectHelper;
import com.sap.isa.maintenanceobject.backend.EnqueueObjectHelperException;
import com.sap.isa.maintenanceobject.backend.J2EEPersistencyDataHandler;
import com.sap.isa.maintenanceobject.backend.MaintenanceObjectHelper;
import com.sap.isa.maintenanceobject.backend.MaintenanceObjectHelperException;
import com.sap.isa.maintenanceobject.backend.boi.MaintenanceObjectData;
import com.sap.isa.maintenanceobject.backend.boi.PropertyData;
import com.sap.isa.maintenanceobject.util.HelpDescriptions;
import com.sap.isa.shopadmin.backend.boi.ShopAdminBackend;
import com.sap.isa.shopadmin.backend.boi.ShopData;
import com.sap.isa.user.backend.boi.UserBaseData;
import com.sap.mw.jco.JCO;

/**
 *  With the interface ShopAdminBackend the ShopAdmin can communicate with the
 *  backend.
 *
 *@author     Christoph Hinssen
 *@created    02/28/2002
 */
public class ShopAdminR3 extends IsaBackendBusinessObjectBaseSAP
                 implements ShopAdminBackend {

        /**
         *  Reference to a <code>MaintenanceObjectHelper</code> object, which will
         *  handle all backend independent stuff, like reading the configuration,
         *  generic reading and writing of properties.
         *
         *@see    com.sap.isa.shopadmin.backend.MaintenanceObjectHelper
         */
        protected MaintenanceObjectHelper objectHelper;

		protected static final String PROPERTY_AUTHORITY_OBJECT = "AuthObject";
		protected static final String PROPERTY_AUTHORITY_FIELDVALUE1 = "AuthFieldValue1";
		protected static final String PROPERTY_AUTHORITY_FIELDVALUE2 = "AuthFieldValue2";
		protected static final String PROPERTY_AUTHORITY_FIELDVALUE3 = "AuthFieldValue3";
		
        private Properties initProperties;
        private BackendBusinessObjectParams initParameters;
		protected String authorizationObject;
		protected String authFieldValue1;
		protected String authFieldValue2;
		protected String authFieldValue3;
        private String id;
        private String description;
        private String language;
//        protected String country;
//        protected String countryGroup;
//        protected String salesOrg;
//        protected String distrChan;
//        protected String division;
//        protected String scenario;
//        protected String catalog;
//        protected String pricingMode;
//        protected String priceProc;
//        protected String catalogVariant;
//        protected String currency;
//        protected String carrierType;
//        protected String simId;
//        protected String limId;
//        protected String rejectionCode;
//
//        protected String orderType;
//        protected String plant;
//        protected String refCustomer;
//        protected String codCustomer;
//        protected String refUser;
//        protected String addrForm;
//
//
//        private boolean isB2c;
//        private boolean isB2b;
//        private boolean isQuotationAllowed;
//
//        //payment
//        protected boolean invoiceEnabled;
//        protected boolean codEnabled;
//        protected boolean cCardEnabled;
//        protected String defaultPayment;
//
//        //are order templates enabled?
//        protected boolean orderTemplates;


// backend depending properties

        private boolean isEventCapturingActived;

        /**
         *  Description of the Field
         */
        protected final static String CRM_SHOP_LISTPRICES = "2";
        /**
         *  Description of the Field
         */
        protected static IsaLocation log = IsaLocation.getInstance(ShopAdminR3.class.getName());
        protected static boolean debug = log.isDebugEnabled();


	/**
	 * Adjust properties depending from the scenario 
	 * 
	 * @param shopData shop
	 * @param scenario scenario
	 */
	public void adjustToScenario(ShopData shopData, String scenario) 
			throws BackendException {
				
			PropertyData b2b = shopData.getProperty("b2b");
			PropertyData b2c = shopData.getProperty("b2c");
			b2b.setValue(false);
			b2c.setValue(false);
			
			if(log.isDebugEnabled()){ 
				log.debug("adjustToScenario, scenario is: "+ scenario); 
			}
				if (scenario.equals(ShopData.B2B)|| scenario.equals("B2BC")){
					if (log.isDebugEnabled()) log.debug("set b2b");
					b2b.setValue(true);
				}
				if (scenario.equals(ShopData.B2C)|| scenario.equals("B2BC")){
					if (log.isDebugEnabled()) log.debug("set b2c");
					b2c.setValue(true);
				}
			shopData.synchronizeBeanProperties(this);	
	}


	/**
	 * Set a lock to the maintenance object
	 * 
     * @param maintenanceObject
     */   
    public boolean lockObject(MaintenanceObjectData shop)
            throws BackendException {
   		
   		EnqueueObjectHelper enqueueObjectHelper	= EnqueueObjectHelper.getEnqueueObjectHelper();
   		try {
   			enqueueObjectHelper.lockObject("shop",shop.getId());
   		}
   		catch (EnqueueObjectHelperException ex) {	
   			shop.addMessage(new Message(Message.ERROR,"shop.locked"));
            return false;	
        }        	        
		return true;
    }      

          
	/**
	 * Delete a lock from the maintenance object
	 * 
     * @param maintenanceObject
     */   
    public void unlockObject(MaintenanceObjectData shop)
            throws BackendException {

   		EnqueueObjectHelper enqueueObjectHelper	= EnqueueObjectHelper.getEnqueueObjectHelper();
		enqueueObjectHelper.unLockObject("shop",shop.getId());
            	
    }        	

	/**
	 * Returns the help values given within the property
	 * @see com.sap.isa.maintenanceobject.backend.boi.MaintenanceObjectBackend#getHelpDescriptions(MaintenanceObjectData, PropertyData)
	 */
	public HelpDescriptions getHelpDescriptions(MaintenanceObjectData maintenanceObject,
       											PropertyData property)
				throws BackendException {
					
		if (property.isHelpAvailable()) {
			return new HelpDescriptions(HelpDescriptions.RESOURCE_KEY,
									   property.getHelpDescription());			
		}						
		return null;
	}


	/**
	 * Check if the given user is authorized to maintain shops. <br>
	 * <strong>Dummy implementation. Return always <code>true</code>.</strong>
	 * 
	 * @param user user to check
	 * @return <code>true</code> if the user is authorized
	 * @throws BackendException
	 */
	public boolean isUserAuthorized (UserBaseData user)
			throws BackendException {
	
				JCoConnection connection = getDefaultJCoConnection();
				int countFieldList = 1;
				if (authFieldValue2 != null) countFieldList = 2;
				if (authFieldValue3 != null) countFieldList = 3;
				
				List fieldValues = new ArrayList(countFieldList);
		
				fieldValues.add(parseFieldValueList(authFieldValue1));
				if (authFieldValue2 != null) fieldValues.add(parseFieldValueList(authFieldValue2));
				if (authFieldValue3 != null) fieldValues.add(parseFieldValueList(authFieldValue3));
				
				if (log.isDebugEnabled()){
					StringBuffer debugOutput = new StringBuffer("");
					debugOutput.append("\nauthorization object: "+authorizationObject);
					for (int i = 0;i<fieldValues.size();i++){
						debugOutput.append("\nfield values to check: "+((String[])fieldValues.get(i))[0]+", "+((String[])fieldValues.get(i))[1]);
					}
					debugOutput.append("\nuser id: "+user.getUserId());
					log.debug(debugOutput);
				}
				

				String returnCode = authorityCheck(user.getUserId().toUpperCase(),
												   authorizationObject, 
												   fieldValues,  
												   connection);				

				connection.close();
				
				return returnCode.length()>0?false:true;			
	}	
	
	/**
	 * Parses a string with separator "/" into a size 2 array.
	 * @param fieldValueString the argument to be parsed
	 * @return the array
	 */
	protected String[] parseFieldValueList(String fieldValueString){
		String[] result = new String[]{"",""};
		 
		if (log.isDebugEnabled())
			log.debug("parseFieldValueList for: " + fieldValueString);
			
		String separator = "/";
		StringTokenizer tokenizer = new StringTokenizer(fieldValueString,separator);
		
		if (tokenizer.hasMoreTokens()) result[0] = tokenizer.nextToken();
		if (tokenizer.hasMoreTokens()) result[1] = tokenizer.nextToken();
		
		return result;
	}



//        /**
//         *  Set the property b2c
//         *
//         *@param  b2c
//         */
//        public void setB2c(boolean b2c) {
//                this.isB2c = b2c;
//        }
//
//
//        /**
//         *  Set the property b2b
//         *
//         *@param  b2b
//         */
//        public void setB2b(boolean b2b) {
//                this.isB2b = b2b;
//        }


//        /**
//         *  Set the property catalog <b>Don't use this method, use readOrgData instead
//         *  <br>
//         *  This method is only needed in the backend!</b>
//         *
//         *@param  catalog
//         */
//        public void setCatalog(String catalog) {
//                this.catalog = catalog;
//        }


        /**
         *  Set the property description
         *
         *@param  description
         */
        public void setDescription(String description) {
                this.description = description;
        }





        /**
         *  Set the property eventCapturingActived
         *
         *@param  eventCapturingActived
         */
        public void setEventCapturingActived(boolean eventCapturingActived) {
                this.isEventCapturingActived = eventCapturingActived;
        }


        /**
         *  Set the property id
         *
         *@param  id
         */
        public void setId(String id) {
                this.id = id;
        }


//        /**
//         *  Set the property quotationAllowed
//         *
//         *@param  quotationAllowed
//         */
//        public void setQuotationAllowed(boolean quotationAllowed) {
//                this.isQuotationAllowed = quotationAllowed;
//        }
//
//
//        /**
//         *  Set the property pricingMode
//         *
//         *@param  pricingMode
//         */
//        public void setPricingMode(String pricingMode) {
//                this.pricingMode = pricingMode;
//        }



//        /**
//         *  Set the property scenario
//         *
//         *@param  scenario  use constants B2B and B2C
//         */
//        public void setScenario(String scenario) {
//                this.scenario = scenario;
//        }





//        /**
//         *  Set the property catalogVariant
//         *
//         *@param  catalogVariant
//         */
//        public void setCatalogVariant(String catalogVariant) {
//                this.catalogVariant = catalogVariant;
//        }
//
//
//
//
//        /**
//         *  Returns the property b2c
//         *
//         *@return    b2c
//         */
//        public boolean isB2c() {
//                return this.isB2c;
//        }

//
//        /**
//         *  Returns the property b2b
//         *
//         *@return    b2b
//         */
//        public boolean isB2b() {
//                return this.isB2b;
//        }
//
//
//
//
//
//        /**
//         *  Returns the property catalog
//         *
//         *@return    catalog
//         */
//        public String getCatalog() {
//                return catalog;
//        }



        /**
         *  Returns the property description
         *
         *@return    description
         */
        public String getDescription() {
                return description;
        }




        /**
         *  Returns the property eventCapturingActived
         *
         *@return    eventCapturingActived
         */
        public boolean isEventCapturingActived() {
                return isEventCapturingActived;
        }


        /**
         *  Returns the property id
         *
         *@return    id
         */
        public String getId() {
                return id;
        }



//        /**
//         *  Returns the property quotationAllowed
//         *
//         *@return    quotationAllowed
//         */
//        public boolean isQuotationAllowed() {
//                return isQuotationAllowed;
//        }
//
//
//        /**
//         *  Returns the property pricingMode
//         *
//         *@return    pricingMode
//         */
//        public String getPricingMode() {
//                return this.pricingMode;
//        }




//        /**
//         *  Returns the property szenario
//         *
//         *@return    scenario use constants B2B and B2C
//         */
//        public String getScenario() {
//                return scenario;
//        }
//
//
//
//
//        /**
//         *  Returns the property catalogVariant
//         *
//         *@return    catalogVariant
//         */
//        public String getCatalogVariant() {
//                return this.catalogVariant;
//        }


        /**
         *  Wrapper for CRM function module <code>CRM_ISA_SHOP_DATA_GET</code>
         *
         *@param  shopId                Description of Parameter
         *@return                       ReturnValue containing messages of call and (if
         *      present) the return code generated by the function module.
         *@exception  BackendException  Description of Exception
         */

        /**
         *  Wrapper for CRM function module <code>CRM_ISA_SHOP_DATA_GET</code>
         *
         *  Reads a list of all shops and returns this list using a tabluar data
         *  structure.
         *
         *@param  shopId                Description of Parameter
         *@param  sceanrio              Scenario to use

         *@return                       ReturnValue containing messages of call and (if
         *      present) the return code generated by the function module.
         *@return                       List of all available shops
         *@exception  BackendException  Description of Exception
         */
        public ResultData readShopList(String shopId, String scenario) throws BackendException {

            Properties props = new Properties();
			
			if (scenario.length() > 0) {
              props.setProperty("scenario", scenario);
			}  

            shopId = shopId.toUpperCase();
                       
            try {
            	return objectHelper.readObjectList(shopId, props);
            }
            catch (MaintenanceObjectHelperException ex) {
            	log.error("Unable to retrieve shop data" , ex);
                throw new BackendException("Could not read the object list: "  + ex.getMessage());
            }

        }

        /**
         *  This method deletes the shop with the key <code>shopKey</code> within the
         *  backend
         *
         *@param  shopKey               <code>TechKey</code> of the shop
         *@exception  BackendException  Description of Exception
         */
        public void deleteShop(TechKey shopKey)
                         throws BackendException {

            try {
            	objectHelper.deleteObject(shopKey);
            }
            catch (Exception ex) {
            	throw new BackendException("Could not read the object list");
            }

        }


        /**
         *  This method initialize the maintenance object in the backend.
         *
         *@param  maintenanceObject
         *@exception  BackendException  Description of Exception
         */
        public void initObject(MaintenanceObjectData maintenanceObject)
                         throws BackendException {

                try {
                        objectHelper.initObjectFromFile(maintenanceObject, initProperties);
                }
                catch (Exception ex) {
                        throw new BackendException("Could not initialize the maintenance object");
                }

        }



        /**
         *  This method read the values of the maintenance object from the backend
         *
         *@param  shopData              Description of Parameter
         *@exception  BackendException  Description of Exception
         */
        public void readData(ShopData shopData)
                         throws BackendException {

        	shopData.setId(shopData.getTechKey().getIdAsString());

            shopData.clearMessages();

            try {
            	objectHelper.readData(shopData);
           	}
            catch (Exception ex) {
            	throw new BackendException("Could not read the maintenance object");
            }
        }

		/**
		 * Converts filter relevant attributes to upper case.
		 * @param property the shop admin property to convert
		 */
		protected void convertToUpperCase(ShopData shopObject, String property){
			
			//convert property to upper case
		   PropertyData shopProperty =  shopObject.getProperty(property);
		   if (shopProperty != null){
			shopProperty.setValue(shopProperty.getString().toUpperCase());
			   if (log.isDebugEnabled())
				   log.debug("converted "+property+" to: " + shopProperty.getString());
		   }
		}


        /**
         *  This method save the maintenance object in backend
         *
         *@param  shopObject            Description of Parameter
         *@exception  BackendException  Description of Exception
         */
        public void saveData(ShopData shopObject)
                         throws BackendException {

                shopObject.clearMessages();
                shopObject.setId(shopObject.getId().toUpperCase());
                
				//convert attributes to upper case
				convertToUpperCase(shopObject,"language");
				convertToUpperCase(shopObject,"distrChan");
				convertToUpperCase(shopObject,"salesOrg");
				convertToUpperCase(shopObject,"division");
				

                try {
                        objectHelper.saveData(shopObject);
                }
                catch (Exception ex) {
                        throw new BackendException("Could not save the maintenance object");
                }

        }


        /**
         * Checks the constraints between different shop attributes. 
         * For scenario BOB (Business-on-behalf) or BOBA (Business-on-behalf,
         * agent scenario), shop attribute businessOnBehalf is set to true, otherwise
         * to false.
         * <br>
         * The partner function is editable only if scenario is BOB.
         *
         *@param  shopData              the shop representation
         *@exception  BackendException  if internally an exception is thrown
         */
		public void checkDependencies(ShopData shopData) throws BackendException {

			if (log.isDebugEnabled()) {
				log.debug("checkDependencies start");
			}
			try {
				// dependencies between scenario and BOB settings
				PropertyData propScenario = shopData.getProperty("Usages", "scenario");

				PropertyData propBOB = shopData.getProperty("customer", "businessOnBehalf");
				PropertyData propPartnerFunction = shopData.getProperty("customer", "bobPartnerFunction");

				//in general: always disable BOB because this depends on the scenario
				propBOB.setDisabled(true);
				String scenario = propScenario.getString();
				
				if (log.isDebugEnabled()){
					log.debug("scenario is: " + scenario);
				}
				
				//this covers the BOB reseller scenario
				if (scenario.equals("BOB")){
					propBOB.setValue(true);
					propPartnerFunction.setHidden(false);
				}
				else if (scenario.equals("BOBA")){
					propBOB.setValue(true);
					propPartnerFunction.setValue("");
					propPartnerFunction.setHidden(true);
				}
				else{
					propBOB.setValue(false);
					propPartnerFunction.setValue("");
					propPartnerFunction.setHidden(true);
				}

			} catch (Exception ex) {
				log.error(LogUtil.APPS_BUSINESS_LOGIC, "msg.error.trc.exception", new String[] { "SHOPERP" }, ex);
				log.throwing(ex);
				throw new BackendException(ex);

			}

		}


    /**
     *  Initializes Business Object.
     *
     *@param  props                 a set of properties which may be useful to
     *      initialize the object
     *@param  params                a object which wraps parameters
     *@exception  BackendException  Description of Exception
     */
    public void initBackendObject(Properties props, BackendBusinessObjectParams params)
                     throws BackendException {

        initProperties = (Properties)props.clone();
        initParameters = params;

        try {
			//now get auth object + fields and values
			authorizationObject = props.getProperty(PROPERTY_AUTHORITY_OBJECT);
			authFieldValue1 = props.getProperty(PROPERTY_AUTHORITY_FIELDVALUE1);                         
			authFieldValue2 = props.getProperty(PROPERTY_AUTHORITY_FIELDVALUE2);                         
			authFieldValue3 = props.getProperty(PROPERTY_AUTHORITY_FIELDVALUE3);                         
				
			JCoConnection connection = getDefaultJCoConnection();
			JCO.Attributes attributes = connection.getAttributes();

			initProperties.setProperty(J2EEPersistencyDataHandler.CLIENT , attributes.getClient());
			initProperties.setProperty(J2EEPersistencyDataHandler.SYSTEM , attributes.getSystemID());     

             // create the object helper
            objectHelper = new MaintenanceObjectHelper(getConnectionFactory(), initProperties);
            }
        catch (Exception ex) {
        		log.debug("exception catched", ex);
                throw new BackendException("Could not initialize the backend object");
        }
    }


}
