/*****************************************************************************
    Class:        ShopAdminBackend
    Copyright (c) 2001, SAPMarkets Europe GmbH, Germany, All rights reserved.
    Author:       Wolfgang Sattler
    Created:      February 2002
    Version:      1.0

    $Revision: #3 $
    $Date: 2001/07/24 $
*****************************************************************************/
package com.sap.isa.shopadmin.backend.crm;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import com.sap.isa.backend.IsaBackendBusinessObjectBaseSAP;
import com.sap.isa.backend.JCoHelper;
import com.sap.isa.backend.crm.MessageCRM;
import com.sap.isa.core.TechKey;
import com.sap.isa.core.eai.BackendBusinessObjectParams;
import com.sap.isa.core.eai.BackendException;
import com.sap.isa.core.eai.sp.jco.ExtensionSAP;
import com.sap.isa.core.eai.sp.jco.JCoConnection;
import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.core.util.table.ResultData;
import com.sap.isa.core.util.table.Table;
import com.sap.isa.core.util.table.TableRow;
import com.sap.isa.helpvalues.HelpValues;
import com.sap.isa.maintenanceobject.backend.J2EEPersistencyDataHandler;
import com.sap.isa.maintenanceobject.backend.MaintenanceObjectHelper;
import com.sap.isa.maintenanceobject.backend.MaintenanceObjectHelperException;
import com.sap.isa.maintenanceobject.backend.boi.AllowedValueData;
import com.sap.isa.maintenanceobject.backend.boi.BackendPropertiesData;
import com.sap.isa.maintenanceobject.backend.boi.MaintenanceObjectData;
import com.sap.isa.maintenanceobject.backend.boi.PropertyData;
import com.sap.isa.maintenanceobject.backend.boi.PropertyGroupData;
import com.sap.isa.maintenanceobject.businessobject.BackendProperties;
import com.sap.isa.maintenanceobject.util.HelpDescriptions;
import com.sap.isa.shopadmin.backend.boi.ShopAdminBackend;
import com.sap.isa.shopadmin.backend.boi.ShopData;
import com.sap.isa.user.backend.boi.UserBaseData;
import com.sap.mw.jco.JCO;


/**
 * With the interface ShopAdminBackend the ShopAdmin can communicate with
 * the backend. <br>
 *
 */
public class ShopAdminCRM extends IsaBackendBusinessObjectBaseSAP
    	implements ShopAdminBackend {
    		
    protected static final String AUTHORITY_OBJECT_SHOP_ADMIN = "CRM_ISA_SM";
    
    public static final int MAX_LENGTH_ID = 10;
    
    public static final int MAX_LENGTH_DESCRIPTION = 40;
    
    protected static final String CRM_SHOP_LISTPRICES = "2";
    
    protected static IsaLocation log = IsaLocation.getInstance(
                                               ShopAdminCRM.class.getName());


    /**
     * Reference to a <code>MaintenanceObjectHelper</code> object, which will
     * handle all backend independent stuff, like reading the configuration,
     * generic reading and writing of properties.
     *
     * @see com.sap.isa.shopadmin.backend.MaintenanceObjectHelper
     */
    protected MaintenanceObjectHelper objectHelper;
    private String[][] propertyMapping; 
    
    private boolean isShopStoredinBackend = true;
    private Properties initProperties;
    private BackendBusinessObjectParams initParameters;

    
	private String itemsThresholdMode = "";
    
    private boolean isBestsellerAvailable;
    private boolean isRecommendationAvailable;
    private boolean isPersonalRecommendationAvailable;
    private boolean isUserProfileAvailable;
    private boolean isCuaAvailable;

	// properties available in the shop crm backend  
	private boolean isB2c;
	private boolean isB2b;
	private String scenario;

	// Store Locator
	private boolean storeLocatorAvailable;
	private String businessPartnerId;
	private String relationshipGroup;
	private String searchCriteriaMode = "";
	
	// Catalog
	private boolean isCatalogDeterminationActived;
	private String catalog;
	private String catalogVariant;
	private String catalogView;
	private boolean isInternalCatalogNotAvailable;

	// Order
	private String orderGroupSelected;
	private String orderProcessTypeGroup;
	private int itemsThresholdValue;
	private boolean templateAllowed;

	
	// Qutation
	private String quotationMode = "";
	private String quotationGroupSelected;
	private String quotationProcessTypeGroup;

	// Contract
	private boolean isContractAllowed;
	private boolean contractNegotiationAllowed;
 
 	// Marketing
	private String attributeSetId;
	private String bestsellerTargetGroup;
	private String methodeSchema;
	private boolean isCuaPersonalized;
    private boolean isEnrollmentAllowed;
    private String textTypeForCampDescr;
    private String newsAttributeSetId;
    // loyalty program
    private boolean isEnableLoyaltyProgram;
    private String loyaltyProgramID;
    private String pointType;
    
    
    // CCH
	private boolean partnerBasket;
    
    // Provider Shop
    private boolean basketloadAllowed;
	
	
    /**
     * Fill Jco with property mapping table
     * 
     * @param shop shop object to use
     * @param record JCo record to fill
     * 
     */
    protected final static void fillRecordFromMapping(MaintenanceObjectData shop, 
                                                      JCO.Record record) {
        
        Iterator iter = shop.iterator();
        while (iter.hasNext()) {
            Iterator propertyIterator = ((PropertyGroupData)iter.next()).iterator();
            
            while (propertyIterator.hasNext()) {
				PropertyData property = (PropertyData) propertyIterator.next();
                BackendPropertiesData backendProperties = (BackendProperties) property.getBackendProperties();

				if (backendProperties != null) {
                
					String fieldName = backendProperties.getFieldName();
	
					if (fieldName.length() > 0) {
		                if (property.getType().equals(PropertyData.TYPE_BOOLEAN)) {
		                    JCoHelper.setValue(record, 
		                                       property.getBoolean(), 
		                                       fieldName);
		                }
		                else if (property.getType().equals(PropertyData.TYPE_INTEGER)) {
		                	JCoHelper.setValue(record,
											   property.getInteger(),
		                					   fieldName);
		                }
		                else {
		                    record.setValue(property.getString(), fieldName);
		                }
					}
				}	
			}
		}
    }



	/**
	 * Fill Jco with property mapping table
	 * 
	 * @param shop shop object to use
	 * @param record JCo record to fill
	 * 
	 */
	protected final static String[][] fillMapping(MaintenanceObjectData shop) {
        
        List mapping = new ArrayList();
        
		Iterator iter = shop.iterator();
		while (iter.hasNext()) {
			Iterator propertyIterator = ((PropertyGroupData)iter.next()).iterator();
            
			while (propertyIterator.hasNext()) {
				PropertyData property = (PropertyData) propertyIterator.next();
				BackendPropertiesData backendProperties = (BackendProperties) property.getBackendProperties();
                
                if (backendProperties != null) {
					String fieldName = backendProperties.getFieldName();
		            
					if (fieldName.length() > 0) {
						mapping.add(new String [] {fieldName,property.getName()});
					}
				}	

			}
            
		}
		
		return (String[][]) mapping.toArray(new String[][]{});
	}



    /**
     * Fill shop object from Jco using property mapping table
     * 
     * @param shop shop object to fill
     * @param record JCo record to use
     * 
     */
    protected final static void readRecordFromMapping(MaintenanceObjectData shop, 
                                                      JCO.Record record) {
                                                      	
		Iterator iter = shop.iterator();
		while (iter.hasNext()) {
			Iterator propertyIterator = ((PropertyGroupData)iter.next()).iterator();

			while (propertyIterator.hasNext()) {
				PropertyData property = (PropertyData) propertyIterator.next();
				BackendPropertiesData backendProperties = (BackendProperties) property.getBackendProperties();
                
				if (backendProperties != null) {
					String fieldName = backendProperties.getFieldName();
	
					if (fieldName.length() > 0) {
	
						if (property.getType().equals(PropertyData.TYPE_BOOLEAN)) {
							property.setValue(JCoHelper.getBoolean(record, fieldName));
						}
						else if (property.getType().equals(PropertyData.TYPE_INTEGER)) {
							property.setValue(record.getInt(fieldName));
						}
						else {
							property.setValue(record.getString(fieldName));
						}
					}
				}		
			}
        }
    }


    /**
     * set property description from the backend()
     * 
     * @param bean bean object to fill
     * @param record JCo record to use
     * 
     */
    protected final static void readPropertyDescription(MaintenanceObjectData shopData, 
                                                        JCO.Record record) {

		Iterator iter = shopData.iterator();
		while (iter.hasNext()) {
			Iterator propertyIterator = ((PropertyGroupData)iter.next()).iterator();
	
			while (propertyIterator.hasNext()) {
				PropertyData property = (PropertyData) propertyIterator.next();
				BackendPropertiesData backendProperties = (BackendProperties) property.getBackendProperties();
                
				if (backendProperties != null) {
					String fieldName = backendProperties.getDescriptionFieldName();
		
					if (fieldName.length() > 0) {
		                property.setValueDescription(record.getString(fieldName));
					}
				}	    
            }
        }
    }

    /**
     * Wrapper for CRM function module <code>CRM_ISA_SHOP_GETLIST</code>
     *
     * @param table  List of all available shops
     * @param scenario Scenario the shop list should be retrieved for
     * @param connection connection to use
     *
     * @return ReturnValue containing messages of call and (if present) the
     *                     return code generated by the function module.
     *
     */
    public static JCoHelper.ReturnValue crmIsaShopGetlist(Table shopTable, 
                                                          String scenario, 
                                                          JCoConnection connection)
                                                   throws BackendException {
        try {
            // now get repository infos about the function
            JCO.Function function = connection.getJCoFunction(
                                            "CRM_ISA_SHOP_GETLIST");

            // getting import parameter
            JCO.ParameterList importParams = function.getImportParameterList();

            importParams.setValue(" ", "B2B");
            importParams.setValue(" ", "B2C");

            // setting the scenario
            if (ShopData.B2B.equalsIgnoreCase(scenario)) {
                importParams.setValue("X", "B2B");
            }
            else if (ShopData.B2C.equalsIgnoreCase(scenario)) {
                importParams.setValue("X", "B2C");
            }
            else if (ShopData.B2X.equalsIgnoreCase(scenario)) {
                importParams.setValue("X", "B2B");
                importParams.setValue("X", "B2C");
            }

            if (log.isDebugEnabled()) {
                log.debug("scenario: " + scenario);
            }

            // call the function
            connection.execute(function);

            JCO.Table extensionTable = function.getTableParameterList()
                                               .getTable("EXTENSION_OUT");

            // preprocess extension for use in result data
            ExtensionSAP.TablePreprocessedExtensions tableExtensions = 
                    ExtensionSAP.createTablePreprocessedExtensions(
                            extensionTable);

            // get the output parameter
            JCO.Table shops = function.getTableParameterList()
                                      .getTable("SHOPS");

            shopTable.addColumn(Table.TYPE_STRING, ShopData.ID);
            shopTable.addColumn(Table.TYPE_STRING, ShopData.DESCRIPTION);


            // add column from the extensions
            ExtensionSAP.addTableColumnFromExtension(shopTable, tableExtensions);

            int numShops = shops.getNumRows();

            for (int i = 0; i < numShops; i++) {
                TableRow shopRow = shopTable.insertRow();

                shopRow.setRowKey(new TechKey(shops.getString("ID")));
                shopRow.getField(ShopData.ID).setValue(shops.getString("ID"));
                shopRow.getField(ShopData.DESCRIPTION)
                       .setValue(shops.getString("DESCRIPTION"));

                // fill extension column with the data from the extensions
                ExtensionSAP.fillTableRowFromExtension(shopRow, tableExtensions);

                shops.nextRow();
            }

            // write some useful logging information
            //if (log.isDebugEnabled()) {
            //    WrapperCrmIsa.logCall("CRM_ISA_SHOP_GETLIST", importParams, null,log);
            //    WrapperCrmIsa.logCall("CRM_ISA_SHOP_GETLIST", null, shops,log);
            //}
            return new JCoHelper.ReturnValue(null, "");
        }
        catch (JCO.Exception ex) {
            JCoHelper.splitException(ex);
        }

        return null;
    }

    /**
     * Wrapper for CRM function module <code>CRM_ISA_HELPVALUES_SHOP</code>
     *
     * @param table  List of all available shops
     * @param shopId
     * @param scenario
     * @param connection connection to use
     *
     * @return ReturnValue containing messages of call and (if present) the
     *                     return code generated by the function module.
     *
     */
    public static JCoHelper.ReturnValue crmIsaHelpvaluesShop(Table table, 
                                                             String shopId, 
                                                             String sceanrio, 
                                                             JCoConnection connection)
                                                      throws BackendException {
        try {
            // now get repository infos about the function
            JCO.Function function = connection.getJCoFunction(
                                            "CRM_ISA_HELPVALUES_SHOP");

            // getting import parameter
            JCO.ParameterList importParams = function.getImportParameterList();

            importParams.setValue(shopId, "ID");

            importParams.setValue(sceanrio, "SCENARIO");


            // call the function
            connection.execute(function);

            JCO.Table values = function.getTableParameterList()
                                       .getTable("SHOP");

            int numValues = values.getNumRows();

            for (int i = 0; i < numValues; i++) {
                String id = values.getString("ID");

                TableRow row = table.insertRow();

                row.setRowKey(new TechKey(id));

                row.getField(ShopData.ID).setValue(id);
                row.getField(ShopData.DESCRIPTION)
                   .setValue(values.getString("DESCRIPTION"));

                values.nextRow();
            }

            // write some useful logging information
            //if (log.isDebugEnabled()) {
            //    WrapperCrmIsa.logCall("CRM_ISA_SHOP_GETLIST", importParams, null,log);
            //    WrapperCrmIsa.logCall("CRM_ISA_SHOP_GETLIST", null, shops,log);
            //}
            return new JCoHelper.ReturnValue(null, "");
        }
        catch (JCO.Exception ex) {
            JCoHelper.splitException(ex);
        }

        return null;
    }

    /**
     * Wrapper for CRM function module <code>CRM_ISA_SHOPVIEW_GET</code>
     *
     * @param table  List of all available shops
     * @param shopData
     * @param scenario
     * @param connection connection to use
     *
     * @return ReturnValue containing messages of call and (if present) the
     *                     return code generated by the function module.
     *
     */
    public static JCoHelper.ReturnValue crmIsaShopViewGet(ShopData shopData, 
                                                          String sceanrio, 
                                                          JCoConnection connection)
                                                   throws BackendException {
        try {
            // now get repository infos about the function
            JCO.Function function = connection.getJCoFunction(
                                            "CRM_ISA_SHOPVIEW_GET");

            // getting import parameter
            JCO.ParameterList importParams = function.getImportParameterList();

            importParams.setValue(sceanrio, "SCENARIO");


            // call the function
            connection.execute(function);

            JCO.Table values = function.getTableParameterList()
                                       .getTable("SHOPVIEW_OUT");

            int numValues = values.getNumRows();

            Map fieldMap = new HashMap(numValues);

            // buffer results for faster access
            for (int i = 0; i < numValues; i++) {
                fieldMap.put(values.getString("CRM_FIELDNAME"), values.clone());

                values.nextRow();
            }

            // loop over all properties of the business object		
            Iterator groupIter = shopData.iterator();

            while (groupIter.hasNext()) {
                PropertyGroupData group = (PropertyGroupData) groupIter.next();

                Iterator iter = group.iterator();

                while (iter.hasNext()) {
					PropertyData property = (PropertyData) iter.next();
					BackendPropertiesData backendProperties = (BackendProperties) property.getBackendProperties();
                
					String fieldName = null;
					
					if (backendProperties != null) {
						fieldName = backendProperties.getFieldName();
					}	

                    // get the CRM fieldname which fits					
                    JCO.Record record = null;

                    if (fieldName != null && fieldName.length()>0) {
                        // note: scenario should not be overwriten			
                        // get the jco record, which fits											
                        record = (JCO.Record) fieldMap.get(fieldName);

                        if (record != null) {
                            char visibilityFlag = record.getChar(
                                                          "CRM_MAINTVALUE");

                            boolean changeable = false;

                            if (visibilityFlag == ' ') {
                                property.setHidden(true);
                                property.setAllowed(false);
                            }
                            else {
                                property.setAllowed(true);
                                property.setHidden(false);

                                if (visibilityFlag == 'V') {
                                    property.setDisabled(true);
                                }
                                else {
                                    property.setDisabled(false);
                                    changeable = true;
                                }
                            }

                            if (!property.getName().equals("scenario")) {
                                String value = record.getString("CRM_VALUE");

                                // The value of the property will be overwritten with the value if
                                //  a) the field isn't changeable 
                                // 	b) the shop is new											
                                if (!changeable || 
                                        (shopData.isNewObject() && 
                                            value.length() > 0)) {
                                    if (property.getType()
                                                .equals(PropertyData.TYPE_BOOLEAN)) {
                                        property.setValue(JCoHelper.getBoolean(
                                                                  value));
                                    }
                                    else if (property.getType()
                                    			.equals(PropertyData.TYPE_INTEGER)) {
                                    	if (value.trim().length() == 0) {
                                    		value = "0";
                                    	}
                                    	property.setValue(Integer.parseInt(value));			
                                    }
                                    else {
                                        property.setValue(value);
                                    }
                                }
                            }
                        }
                        else {
                            property.setAllowed(false);
                            property.setHidden(true);
                            property.clearValue();
                        }
                    }
                }
            }

            return new JCoHelper.ReturnValue(null, "");
        }
        catch (JCO.Exception ex) {
            JCoHelper.splitException(ex);
        }

        return null;
    }

    /**
     * Wrapper for CRM function module <code>CRM_ISA_SHOP_DATA_GET</code>
     *
     * @param shop shop object in the backend
     * @param shopData shop object business object layer
     * @param connection connection to use
     *
     * @return ReturnValue containing messages of call and (if present) the
     *                     return code generated by the function module.
     *
     */
    public static JCoHelper.ReturnValue crmIsaShopDataGet(ShopAdminCRM shop, 
                                                          ShopData shopData, 
                                                          JCoConnection connection)
                                                   throws BackendException {
        try {
            // now get repository infos about the function
            JCO.Function function = connection.getJCoFunction(
                                            "CRM_ISA_SHOP_UI_DATA_GET");

            // getting import parameter
            JCO.ParameterList importParams = function.getImportParameterList();


            // setting the id of the attribute set
            importParams.setValue(shopData.getId(), "SHOP");


            // call the function
            connection.execute(function);

            // get the output parameter
            JCO.ParameterList exportParams = function.getExportParameterList();

            // check the message table
            JCO.Table messages = function.getTableParameterList()
                                         .getTable("MESSAGES");

            String returnCode = exportParams.getString("RETURNCODE");

            // write some useful logging information
            if (log.isDebugEnabled()) {
                JCoHelper.logCall("CRM_ISA_SHOP_DATA_GET", importParams, exportParams,log);
            }
            if (returnCode.length() > 0) {
                return new JCoHelper.ReturnValue(messages, returnCode);
            }

            // analyse the shop details
            JCO.Record shopDetail = function.getExportParameterList()
                                            .getStructure("SHOP_DATA");

            shopData.setDescription(shopDetail.getString("DESCRIPTION"));
			shopData.setId(shopDetail.getString("ID"));
		
            readRecordFromMapping(shopData, shopDetail);

            readPropertyDescription(shopData, shopDetail);
            
			shopData.synchronizeBeanProperties(shop);
                        
			shop.adjustPrivateProperties(shopData);

            JCO.Table extensionTable = function.getTableParameterList()
                                               .getTable("EXTENSION_OUT");

            // add all extensions to the items
            ExtensionSAP.addToBusinessObject(shopData, extensionTable);

            return new JCoHelper.ReturnValue(messages, returnCode);
        }
        catch (JCO.Exception ex) {
            JCoHelper.splitException(ex);
        }

        return null;
    }

    /**
     * Wrapper for CRM function module <code>CRM_ISA_SHOP_ENQUEUE</code>
     *
     * @param shop_id shop id in the backend
     * @param connection connection to use
     *
     * @return ReturnValue containing messages of call and (if present) the
     *                     return code generated by the function module.
     *
     */
    public static JCoHelper.ReturnValue crmIsaShopEnqueue(String shop_id, 
                                                          JCoConnection connection)
                                                   throws BackendException {
        try {
            // now get repository infos about the function
            JCO.Function function = connection.getJCoFunction(
                                            "CRM_ISA_SHOP_ENQUEUE");

            // getting import parameter
            JCO.ParameterList importParams = function.getImportParameterList();


            // setting the id of the attribute set
            importParams.setValue(shop_id, "SHOP_ID");


            // call the function
            connection.execute(function);

            JCO.ParameterList exportParams = function.getExportParameterList();

            // check the message table
            JCO.Table messages = function.getTableParameterList()
                                         .getTable("MESSAGES");

            String returnCode = exportParams.getString("RETURNCODE");

            return new JCoHelper.ReturnValue(messages, returnCode);
        }
        catch (JCO.Exception ex) {
            JCoHelper.splitException(ex);
        }

        return null;
    }

    /**
     * Wrapper for CRM function module <code>CRM_ISA_SHOP_DEQUEUE</code>
     *
     * @param shop_id shop id in the backend
     * @param connection connection to use
     *
     * @return ReturnValue containing messages of call and (if present) the
     *                     return code generated by the function module.
     *
     */
    public static JCoHelper.ReturnValue crmIsaShopDequeue(String shop_id, 
                                                          JCoConnection connection)
                                                   throws BackendException {
        try {
            // now get repository infos about the function
            JCO.Function function = connection.getJCoFunction(
                                            "CRM_ISA_SHOP_DEQUEUE");

            // getting import parameter
            JCO.ParameterList importParams = function.getImportParameterList();


            // setting the id of the attribute set
            importParams.setValue(shop_id, "SHOP_ID");


            // call the function
            connection.execute(function);

            return new JCoHelper.ReturnValue(null, "");
        }
        catch (JCO.Exception ex) {
            JCoHelper.splitException(ex);
        }

        return null;
    }

    /**
     * Wrapper for CRM function module <code>CRM_ISA_SHOP_DELETE</code>
     *
     * @param shop_id shop id in the backend
     * @param connection connection to use
     *
     * @return ReturnValue containing messages of call and (if present) the
     *                     return code generated by the function module.
     *
     */
    public static JCoHelper.ReturnValue crmIsaShopDelete(String shop_id, 
                                                         JCoConnection connection)
                                                  throws BackendException {
        try {
            // now get repository infos about the function
            JCO.Function function = connection.getJCoFunction(
                                            "CRM_ISA_SHOP_DELETE");

            // getting import parameter
            JCO.ParameterList importParams = function.getImportParameterList();


            // setting the id of the attribute set
            importParams.setValue(shop_id, "SHOP");


            // call the function
            connection.execute(function);

            // check the message table
            JCO.Table messages = function.getTableParameterList()
                                         .getTable("MESSAGES");

            return new JCoHelper.ReturnValue(messages, "");
        }
        catch (JCO.Exception ex) {
            JCoHelper.splitException(ex);
        }

        return null;
    }

    /**
     * Wrapper for CRM function module <code>CRM_ISA_SHOP_UI_DATA_SAVE</code>
     *
     * @param shop shop object in the backend
     * @param shopData shop object business object layer
     *
     * @return ReturnValue containing messages of call and (if present) the
     *                     return code generated by the function module.
     *
     */
    public static JCoHelper.ReturnValue crmIsaShopDataSave(ShopAdminCRM shop, 
                                                           ShopData shopData, 
                                                           JCoConnection connection)
                                                    throws BackendException {
        try {
            // now get repository infos about the function
            JCO.Function function = connection.getJCoFunction(
                                            "CRM_ISA_SHOP_UI_DATA_SAVE");

            // getting import parameter
            JCO.ParameterList importParams = function.getImportParameterList();

            importParams.setValue(shopData.isNewObject() ? "X" : "", "IS_NEW");

            JCO.Record shopDetail = importParams.getStructure("SHOP_DATA");

			shopDetail.setValue(shopData.getId(), "ID");

            shopDetail.setValue(shopData.getDescription(), "DESCRIPTION");

            shop.adjustFromPrivateProperties(shopData);

            fillRecordFromMapping(shopData, shopDetail);

			shop.adjustShopData(shopData, shopDetail);

            JCO.Table extensionTable = function.getTableParameterList()
                                               .getTable("EXTENSION_IN");

		

            // add all extension from the items to the given JCo table.
            ExtensionSAP.fillExtensionTable(extensionTable, shopData);


            // call the function
            connection.execute(function);

            // get the output parameter
            JCO.ParameterList exportParams = function.getExportParameterList();

            // check the message table
            JCO.Table messages = function.getTableParameterList()
                                         .getTable("MESSAGES");

            String returnCode = exportParams.getString("RETURNCODE");

            // write some useful logging information
            if (log.isDebugEnabled()) {
                 JCoHelper.logCall("CRM_ISA_SHOP_UI_DATA_SAVE", importParams, exportParams,log);
            }
            if (returnCode.length() > 0) {
                return new JCoHelper.ReturnValue(messages, returnCode);
            }

            return new JCoHelper.ReturnValue(messages, returnCode);
        }
        catch (JCO.Exception ex) {
            JCoHelper.splitException(ex);
        }

        return null;
    }


	/**
	 * Wrapper for CRM function module <code>CRM_ISA_SHOP_UI_DESCRIPTIONS</code>
	 *
	 * @param shop shop object in the backend
	 * @param shopData shop object business object layer
	 *
	 * @return ReturnValue containing messages of call and (if present) the
	 *                     return code generated by the function module.
	 *
	 */
	public static JCoHelper.ReturnValue crmIsaShopDescriptionsGet(ShopAdminCRM shop, 
														          ShopData shopData, 
														          JCoConnection connection)
													              throws BackendException {
		try {
			// now get repository infos about the function
			JCO.Function function = connection.getJCoFunction(
											"CRM_ISA_SHOP_UI_DESCRIPTIONS");

			// getting import parameter
			JCO.ParameterList importParams = function.getImportParameterList();

			JCO.Record shopDetail = importParams.getStructure("SHOP_DATA");

			fillRecordFromMapping(shopData, shopDetail);

			JCO.Table extensionTable = function.getTableParameterList()
											   .getTable("EXTENSION_IN");

			// add all extension from the items to the given JCo table.
			ExtensionSAP.fillExtensionTable(extensionTable, shopData);


			// call the function
			connection.execute(function);

			// get the output parameter
			JCO.ParameterList exportParams = function.getExportParameterList();

			// check the message table
			JCO.Table messages = function.getTableParameterList()
										 .getTable("MESSAGES");

			String returnCode = exportParams.getString("RETURNCODE");

			// write some useful logging information
			if (log.isDebugEnabled()) {
			 	JCoHelper.logCall("crmIsaShopDescriptionsGet", importParams, exportParams,log);
			}
			if (returnCode.length() > 0) {
				return new JCoHelper.ReturnValue(messages, returnCode);
			}

			// analyse the shop details
			JCO.Record descriptions = function.getExportParameterList()
											.getStructure("DESCRIPTIONS");

			readPropertyDescription(shopData, descriptions);

			extensionTable = function.getTableParameterList()
											   .getTable("EXTENSION_OUT");

			// add all extensions to the items
			ExtensionSAP.addToBusinessObject(shopData, extensionTable);
			
			return new JCoHelper.ReturnValue(messages, returnCode);
		}
		catch (JCO.Exception ex) {
			JCoHelper.splitException(ex);
		}

		return null;
	}

    /**
     * Wrapper for CRM function module <code>CRM_ISA_HELPVALUES_CATALOG</code>
     *
     * @param table  List of all available catalogs
     * @param catalogId
     * @param connection connection to use
     *
     * @return ReturnValue containing messages of call and (if present) the
     *                     return code generated by the function module.
     *
     */
    public static JCoHelper.ReturnValue crmIsaHelpvaluesCatalog(Table table, 
                                                                String catalogId, 
                                                                String scenario,
                                                                JCoConnection connection)
        throws BackendException {
        try {
            // now get repository infos about the function
            JCO.Function function = connection.getJCoFunction(
                                            "CRM_ISA_HELPVALUES_CATALOG");

            // getting import parameter
            JCO.ParameterList importParams = function.getImportParameterList();

            importParams.setValue(catalogId, "ID");

            importParams.setValue(scenario.equals("CCH")?"X":"" , "CHANNEL_COMMERCE");

            // call the function
            connection.execute(function);

            JCO.Table values = function.getTableParameterList()
                                       .getTable("CATALOG");

            int numValues = values.getNumRows();

            for (int i = 0; i < numValues; i++) {
                String key = "" + i;

                TableRow row = table.insertRow();

                row.setRowKey(new TechKey(key));

                row.getField("Id").setValue(values.getString("ID"));
                row.getField("Description")
                   .setValue(values.getString("PCAT_DESCRIPTION"));

                values.nextRow();
            }

            // write some useful logging information
            //if (log.isDebugEnabled()) {
            //    WrapperCrmIsa.logCall("CRM_ISA_SHOP_GETLIST", importParams, null,log);
            //    WrapperCrmIsa.logCall("CRM_ISA_SHOP_GETLIST", null, shops,log);
            //}
            return new JCoHelper.ReturnValue(null, "");
        }
        catch (JCO.Exception ex) {
            JCoHelper.splitException(ex);
        }

        return null;
    }

    /**
     * Wrapper for CRM function module <code>CRM_ISA_HELPVALUES_VARIANT</code>
     *
     * @param table  List of all available variants
     * @param catalogId
     * @param variantId
     * @param connection connection to use
     *
     * @return ReturnValue containing messages of call and (if present) the
     *                     return code generated by the function module.
     *
     */
    public static JCoHelper.ReturnValue crmIsaHelpvaluesVariant(Table table, 
                                                                String catalogId, 
                                                                String variantId,
        														String scenario, 
                                                                JCoConnection connection)
        throws BackendException {
        try {
            // now get repository infos about the function
            JCO.Function function = connection.getJCoFunction(
                                            "CRM_ISA_HELPVALUES_VARIANT");

            // getting import parameter
            JCO.ParameterList importParams = function.getImportParameterList();

            importParams.setValue(catalogId, "CATALOG_ID");
            importParams.setValue(variantId, "ID");
            importParams.setValue(scenario.equals("CCH")?"X":"" , "CHANNEL_COMMERCE");


            // call the function
            connection.execute(function);

            JCO.Table values = function.getTableParameterList()
                                       .getTable("VARIANT");

            int numValues = values.getNumRows();

            for (int i = 0; i < numValues; i++) {
                String key = "" + i;

                TableRow row = table.insertRow();

                row.setRowKey(new TechKey(key));

                row.getField("Id").setValue(values.getString("VARIANT_ID"));
                row.getField("CatalogId")
                   .setValue(values.getString("CATALOG_ID"));
                row.getField("Description")
                   .setValue(values.getString("VRT_DESCRIPTION"));

                values.nextRow();
            }

            // write some useful logging information
            //if (log.isDebugEnabled()) {
            //    WrapperCrmIsa.logCall("CRM_ISA_SHOP_GETLIST", importParams, null,log);
            //    WrapperCrmIsa.logCall("CRM_ISA_SHOP_GETLIST", null, shops,log);
            //}
            return new JCoHelper.ReturnValue(null, "");
        }
        catch (JCO.Exception ex) {
            JCoHelper.splitException(ex);
        }

        return null;
    }

    /**
     * Wrapper for CRM function module <code>CRM_ISA_HELPVALUES_VIEW</code>
     *
     * @param table  List of all available variants
     * @param catalogId
     * @param viewId
     * @param connection connection to use
     *
     * @return ReturnValue containing messages of call and (if present) the
     *                     return code generated by the function module.
     *
     */
    public static JCoHelper.ReturnValue crmIsaHelpvaluesView(Table table, 
                                                             String catalogId, 
                                                             String viewId, 
														     String scenario,
                                                             JCoConnection connection)
                                                      throws BackendException {
        try {
            // now get repository infos about the function
            JCO.Function function = connection.getJCoFunction(
                                            "CRM_ISA_HELPVALUES_VIEW");

            // getting import parameter
            JCO.ParameterList importParams = function.getImportParameterList();

            importParams.setValue(catalogId, "CATALOG_ID");
            importParams.setValue(viewId, "ID");
            importParams.setValue(scenario.equals("CCH")?"X":"" , "CHANNEL_COMMERCE");

            // call the function
            connection.execute(function);

            JCO.Table values = function.getTableParameterList()
                                       .getTable("VIEW");

            int numValues = values.getNumRows();

            for (int i = 0; i < numValues; i++) {
                String key = "" + i;

                TableRow row = table.insertRow();

                row.setRowKey(new TechKey(key));

                row.getField("Id").setValue(values.getString("VIEW_ID"));
                row.getField("CatalogId")
                   .setValue(values.getString("CATALOG_ID"));
                row.getField("Description")
                   .setValue(values.getString("VIEW_DESCRIPTION"));

                values.nextRow();
            }

            // write some useful logging information
            //if (log.isDebugEnabled()) {
            //    WrapperCrmIsa.logCall("CRM_ISA_SHOP_GETLIST", importParams, null,log);
            //    WrapperCrmIsa.logCall("CRM_ISA_SHOP_GETLIST", null, shops,log);
            //}
            return new JCoHelper.ReturnValue(null, "");
        }
        catch (JCO.Exception ex) {
            JCoHelper.splitException(ex);
        }

        return null;
    }

    /**
      * Wrapper for CRM function module <code>CRM_ISA_HELPVALUES_ATTRIB_SET</code>
      *
      * @param table  List of all available attribute sets
      * @param id id of the attribute set
      * @param connection connection to use
      *
      * @return ReturnValue containing messages of call and (if present) the
      *                     return code generated by the function module.
      *
      */
    public static JCoHelper.ReturnValue crmIsaHelpvaluesAttributeSet(Table table, 
                                                                     String id, 
                                                                     JCoConnection connection)
        throws BackendException {
        try {
            // now get repository infos about the function
            JCO.Function function = connection.getJCoFunction(
                                            "CRM_ISA_HELPVALUES_ATTRIB_SET");

            // getting import parameter
            JCO.ParameterList importParams = function.getImportParameterList();

            importParams.setValue(id, "ID");


            // call the function
            connection.execute(function);

            JCO.Table values = function.getTableParameterList()
                                       .getTable("ATTRIBUTE_SET");

            int numValues = values.getNumRows();

            for (int i = 0; i < numValues; i++) {
                String key = "" + i;

                TableRow row = table.insertRow();

                row.setRowKey(new TechKey(key));

                row.getField("Id").setValue(values.getString("ID"));
                row.getField("Description")
                   .setValue(values.getString("DESCRIPTION"));

                values.nextRow();
            }

            // write some useful logging information
            //if (log.isDebugEnabled()) {
            //    WrapperCrmIsa.logCall("CRM_ISA_SHOP_GETLIST", importParams, null,log);
            //    WrapperCrmIsa.logCall("CRM_ISA_SHOP_GETLIST", null, shops,log);
            //}
            return new JCoHelper.ReturnValue(null, "");
        }
        catch (JCO.Exception ex) {
            JCoHelper.splitException(ex);
        }

        return null;
    }

    /**
     * Wrapper for CRM function module <code>CRM_ISA_HELPVALUES_TARGETGROUP</code>
     *
     * @param table  List of all available catalogs
     * @param description search expression
     * @param connection connection to use
     *
     * @return ReturnValue containing messages of call and (if present) the
     *                     return code generated by the function module.
     *
     */
    public static JCoHelper.ReturnValue crmIsaHelpvaluesTargetgroup(Table table, 
                                                                    String description, 
                                                                    JCoConnection connection)
        throws BackendException {
        try {
            // now get repository infos about the function
            JCO.Function function = connection.getJCoFunction(
                                            "CRM_ISA_HELPVALUES_TARGETGROUP");

            // getting import parameter
            JCO.ParameterList importParams = function.getImportParameterList();

            importParams.setValue(description, "DESCRIPTION");


            // call the function
            connection.execute(function);

            JCO.Table values = function.getTableParameterList()
                                       .getTable("TARGET_GROUP");

            int numValues = values.getNumRows();

            for (int i = 0; i < numValues; i++) {
                String key = "" + i;

                TableRow row = table.insertRow();

                row.setRowKey(new TechKey(key));

                row.getField("Description")
                   .setValue(values.getString("TG_DESCR"));

                values.nextRow();
            }

            // write some useful logging information
            //if (log.isDebugEnabled()) {
            //    WrapperCrmIsa.logCall("CRM_ISA_SHOP_GETLIST", importParams, null,log);
            //    WrapperCrmIsa.logCall("CRM_ISA_SHOP_GETLIST", null, shops,log);
            //}
            return new JCoHelper.ReturnValue(null, "");
        }
        catch (JCO.Exception ex) {
            JCoHelper.splitException(ex);
        }

        return null;
    }

    /**
     * Wrapper for CRM function module <code>CRM_ISA_HELPVALUES__FCT</code>
     *
     * @param table  List of all available catalogs
     * @param id search expression
     * @param FunctionType  function type to be used
     * @param connection connection to use
     *
     * @return ReturnValue containing messages of call and (if present) the
     *                     return code generated by the function module.
     *
     */
    public static JCoHelper.ReturnValue crmIsaHelpvaluesFct(Table table, 
                                                                   String id, 
                                                                   String partnerFunctionType, 
                                                                   JCoConnection connection)
        throws BackendException {
        try {
            // now get repository infos about the function
            JCO.Function function = connection.getJCoFunction(
                                            "CRM_ISA_HELPVALUES_PARTNER_FCT");

            // getting import parameter
            JCO.ParameterList importParams = function.getImportParameterList();

            importParams.setValue(id, "PARTNER_FCT");
            importParams.setValue(partnerFunctionType, "PARTNER_FUNCTION_TYPE");


            // call the function
            connection.execute(function);

            JCO.Table values = function.getTableParameterList()
                                       .getTable("PARTNER_FUNCTION");

            int numValues = values.getNumRows();

            for (int i = 0; i < numValues; i++) {
                String key = "" + i;

                TableRow row = table.insertRow();

                row.setRowKey(new TechKey(key));

                row.getField("Id")
                   .setValue(values.getString("PARTNER_FUNCTION"));
                row.getField("Description")
                   .setValue(values.getString("DESCRIPTION"));

                values.nextRow();
            }

            return new JCoHelper.ReturnValue(null, "");
        }
        catch (JCO.Exception ex) {
            JCoHelper.splitException(ex);
        }

        return null;
    }

    /**
     * Wrapper for CRM function module <code>CRM_ISA_SHOP_HELPVALUES</code>
     *
     * @param table  List of all available values
     * @param id search expression
     * @param fieldName name of the CRM field
     * @param descriptioName fieldname for description
     * @param connection connection to use
     *
     * @return ReturnValue containing messages of call and (if present) the
     *                     return code generated by the function module.
     *
     */
    public static JCoHelper.ReturnValue crmIsaShopHelpvalues(Table table, 
                                                             String id, 
                                                             String fieldName, 
                                                             String descriptionName, 
                                                             JCoConnection connection)
                                                      throws BackendException {
        try {
            // now get repository infos about the function
            JCO.Function function = connection.getJCoFunction(
                                            "CRM_ISA_SHOP_HELPVALUES");

            // getting import parameter
            JCO.ParameterList importParams = function.getImportParameterList();

            importParams.setValue(id, "ID");
            importParams.setValue(fieldName, "FIELDNAME");

            if (descriptionName != null) {
                importParams.setValue(descriptionName, "FIELDNAME_DESCRIPTION");
            }


            // call the function
            connection.execute(function);

            JCO.Table values = function.getTableParameterList()
                                       .getTable("VALUES");

            int numValues = values.getNumRows();

            for (int i = 0; i < numValues; i++) {
                String key = "" + i;

                TableRow row = table.insertRow();

                row.setRowKey(new TechKey(key));

                row.getField("Id").setValue(values.getString("ID"));
                row.getField("Description")
                   .setValue(values.getString("DESCRIPTION"));

                values.nextRow();
            }

            return new JCoHelper.ReturnValue(null, "");
        }
        catch (JCO.Exception ex) {
            JCoHelper.splitException(ex);
        }

        return null;
    }

    /**
     * Wrapper for CRM function module <code>CRM_ISA_FIELD_HELP_GET</code>
     *
     * @param fieldname
     * @param connection connection to use
     *
     * @return ReturnValue containing messages of call and (if present) the
     *                     return code generated by the function module.
     *
     */
    public static HelpDescriptions crmIsaFieldHelpGet(String fieldname, 
                                                      JCoConnection connection)
                                               throws BackendException {
        try {
            // now get repository infos about the function
            JCO.Function function = connection.getJCoFunction(
                                            "CRM_ISA_FIELD_HELP_GET");

            // getting import parameter
            JCO.ParameterList importParams = function.getImportParameterList();

            importParams.setValue("CRMT_ISA_SHOP_EXT", "TABNAME");
            importParams.setValue(fieldname, "FIELDNAME");


            // call the function
            connection.execute(function);

            JCO.Table values = function.getTableParameterList()
                                       .getTable("DOKU_LINES");

            int numValues = values.getNumRows();

			if (numValues == 0) {
				ArrayList list = new ArrayList(1);
				list.add("shpm.error.nohelptext"); 		
				return new HelpDescriptions(HelpDescriptions.RESOURCE_KEY, list);	
			}

            ArrayList list = new ArrayList(numValues);

            boolean useLine = false;
            boolean inLink = false;

            for (int i = 0; i < numValues; i++) {
                String line = values.getString("TDLINE");

                if (line.endsWith("<\\BODY>")) {
                    useLine = false;
                }

                if (useLine) {
                    StringBuffer lineBuffer = new StringBuffer(line);


                    // remove links from the html code								
                    inLink = true;

                    while (inLink) {
                        String tmpString = lineBuffer.toString();

                        int startpos = tmpString.indexOf("<A HREF");

                        if (startpos >= 0) {
                            int endpos = startpos + 1;

                            while (tmpString.charAt(endpos) != '>') {
                                endpos++;
                            }

                            lineBuffer.delete(startpos, endpos + 1);
                            tmpString = lineBuffer.toString();
                        }

                        int startpos2 = tmpString.indexOf("</A>");

                        if (startpos2 >= 0) {
                            int endpos = startpos2 + 1;

                            while (tmpString.charAt(endpos) != '>') {
                                endpos++;
                            }

                            lineBuffer.delete(startpos2, endpos + 1);
                        }

                        if ((startpos < 0) && (startpos2 < 0)) {
                            inLink = false;
                        }
                    }

                    list.add(lineBuffer.toString());
                }

                if (line.startsWith("<BODY>")) {
                    useLine = true;
                }

                values.nextRow();
            }

            return new HelpDescriptions(HelpDescriptions.HTML, list);
        }
		catch (JCO.AbapException ex) {
			ArrayList list = new ArrayList(1);
			list.add("shpm.error.nohelptext"); 		
			return new HelpDescriptions(HelpDescriptions.RESOURCE_KEY, list);	
		}
        catch (JCO.Exception ex) {
            JCoHelper.splitException(ex);
        }

        return null;
    }

    
    

	/*
	 * set internal properties depending from other properties
	 * 
	 * @param shopData 
	 * 
	 */
	 private final void adjustPrivateProperties(MaintenanceObjectData shopData) {
    	
        if (getAttributeSetId().length() > 0) {
            setPersonalRecommendationAvailable(true);
        }
        else {
            setPersonalRecommendationAvailable(false);
        }

        if (getBestsellerTargetGroup().length() > 0) {
            setBestsellerAvailable(true);
        }
        else {
            setBestsellerAvailable(false);
        }

        if (getMethodeSchema().length() > 0) {
            setCuaAvailable(true);
        }
        else {
            setCuaAvailable(false);
        }

        if (getItemsThresholdValue() == 0) {
            setItemsThresholdMode("");
        }
        else {
            setItemsThresholdMode("X");
        }

        if (isPartnerBasket()) {
            if (getBusinessPartnerId().length() > 0
                && getRelationshipGroup().length() > 0) {
                setSearchCriteriaMode("X");
            }
            else {
                setSearchCriteriaMode("");
            }
        }
               
        PropertyData property = shopData.getProperty("ordertemplate","templateMode");
        if (property.isAllowed()) {
                if (isTemplateAllowed()) {
                      property.setValue("templateAllowed");
                }
                else if(isBasketloadAllowed()) {
                    property.setValue("basketloadAllowed");
               }
               else {
                   property.setValue("");
               }
            }

   		
	 }

     // adjust CRM shop data regarding the private shop properties.
     private void adjustFromPrivateProperties(ShopData shopData) {

         PropertyData property = shopData.getProperty("ordertemplate","templateMode");
         if (property.isAllowed()) {
             PropertyData templateAllowed = shopData.getProperty("ordertemplate","templateAllowed");
             PropertyData basketloadAllowed = shopData.getProperty("ordertemplate","basketloadAllowed");
             String value = property.getString();
             if(value.equals("templateAllowed")) {
                 setTemplateAllowed(true);
             }
             else if(value.equals("basketloadAllowed")) {
                 setBasketloadAllowed(true);
            }
            else {
                setTemplateAllowed(false);
                setBasketloadAllowed(false);
            }
         }                
     }    


	// adjust CRM shop data regarding the private shop properties.
	private void adjustShopData(ShopData shopData, JCO.Record shopDetail) {
		
		// delete marketing infos depending the check boxes			
		if (!isPersonalRecommendationAvailable()) {
			shopDetail.setValue("", "PROFILE_TEMPL_ID");
		}
        
		if (!isBestsellerAvailable()) {
			shopDetail.setValue("", "TARGET_GRP_DESCR");
		}
        
		if (!isCuaAvailable()) {
			shopDetail.setValue("", "METHOD_SCHEME_ID");
		}
        
		// delete order infos depending the selected radio buttons
		if (isB2b() && !isB2c()) {
			if (getOrderGroupSelected().equals("X")) {
				shopDetail.setValue("", "PROCESS_TYPE");
			}
			else {
				shopDetail.setValue("","PROC_TYPE_GR_ORD");
			}	            
		}
        
		// set the ORD_GR_SELECTED info 
		if (isB2b() && isB2c()) {
			if (getOrderProcessTypeGroup().trim().length() > 0) {
				shopDetail.setValue("X", "ORD_GR_SELECTED");
			}
		}
        
		// delete quotation infos depending the selected radio buttons
		if (getQuotationMode().equals("B")) {
			if (getQuotationGroupSelected().equals("X")) {
				shopDetail.setValue("", "PROC_TYPE_QUOTE");
			}
			else {
				shopDetail.setValue("", "PROC_TYPE_GR_QUT");
			} 
		} 
		else {
			shopDetail.setValue("", "QUT_GR_SELECTED");  
			shopDetail.setValue("", "PROC_TYPE_QUOTE");
			shopDetail.setValue("", "PROC_TYPE_GR_QUT");
		} 
        
		// delete large document infos depending the selected radio buttons
		if (getItemsThresholdMode().equals("")) {
			shopDetail.setValue(0, "ITEMS_THRESHOLD");
		}
        
		// delete store locator info in CCH depending the selected radio buttons
		if (isPartnerBasket() && getSearchCriteriaMode().equals("")) {
			shopDetail.setValue("", "BUSINESS_PARTNER");
			shopDetail.setValue("", "REL_SHIP_GROUP");
		}
        
        // delete template infos depending the selected radio buttons in
        // standard B2C scenario
        PropertyData property = shopData.getProperty("ordertemplate","templateMode");
        if (isB2c() && !isB2b() && !scenario.equals("TCSS")) {
            if (property.getValue().equals("basketloadAllowed")) {
                shopDetail.setValue("X", "BASKLOAD_ENABLED");
                shopDetail.setValue("", "USE_TEMPLATES");
            }
            else if (property.getValue().equals("templateAllowed")) {    
                shopDetail.setValue("", "BASKLOAD_ENABLED");
                shopDetail.setValue("X", "USE_TEMPLATES");
            }
            else {
                shopDetail.setValue("", "BASKLOAD_ENABLED");
                shopDetail.setValue("", "USE_TEMPLATES");
            }
        }
                 
	}


    /**
     * Reads a list of all shops and returns this list
     * using a tabluar data structure.
     *
     *@param  shopId   Id of the shop
     * @param scenario Scenario the shop list should be retrieved for
     * @return List of all available shops
     */
    public ResultData readShopList(String shopId, String scenario)
                            throws BackendException {
                            	
        shopId = shopId.toUpperCase();

        // if (shopId.indexOf('*') < 0) {
        //    shopId = shopId + '*';
        //}

        if (isShopStoredinBackend) {
            JCoConnection connection = getDefaultJCoConnection();

            Table shopTable = new Table("Shops");

            shopTable.addColumn(Table.TYPE_STRING, ShopData.ID);
            shopTable.addColumn(Table.TYPE_STRING, ShopData.DESCRIPTION);

            crmIsaHelpvaluesShop(shopTable, 
                                 shopId, 
                                 scenario, 
                                 connection);
                                 
            connection.close();

            return new ResultData(shopTable);
        }
        else {
            try {
                Properties props = new Properties();

                if (scenario.length() > 0) {
                    props.setProperty("sceanrio", scenario);
                }

                return objectHelper.readObjectList(shopId, props);
            }
            catch (MaintenanceObjectHelperException ex) {
                return null;
            }
        }
    }

    /**
     * This method deletes the shop with the key <code>shopKey</code> within the backend
     *
     * @param shopKey <code>TechKey</code> of the shop
     */
    public void deleteShop(TechKey shopKey) throws BackendException {
        if (isShopStoredinBackend) {
            JCoConnection connection = getDefaultJCoConnection();

            //JCoHelper.ReturnValue retVal = 
            crmIsaShopDelete(shopKey.getIdAsString(), connection);

            //MessageCRM.addMessagesToBusinessObject(maintenanceObject,
            //                                   retVal.getMessages(),
            //                                   PROPERTY_MAPPING); 
            connection.close();
        }
        else {
            try {
                objectHelper.deleteObject(shopKey);
            }
            catch (Exception ex) {
                throw new BackendException("Could not read the object list");
            }
        }
    }

    /**
     * This method initialize the maintenance object in the backend.
     *
     * @param maintenanceObject
     */
    public void initObject(MaintenanceObjectData maintenanceObject)
                    throws BackendException {
        try {
            objectHelper.initObjectFromFile(maintenanceObject, initProperties);
			propertyMapping = fillMapping(maintenanceObject);
            maintenanceObject.setMaxLengthDescription(ShopAdminCRM.MAX_LENGTH_DESCRIPTION);
            maintenanceObject.setMaxLengthId(ShopAdminCRM.MAX_LENGTH_ID);
        }
        catch (Exception ex) {
            throw new BackendException(
                    "Could not initialize the maintenance object");
        }
    }

    /**
     * This method read the values of the maintenance object from the backend
     *
     * @param maintenanceObject
     */
    public void readData(ShopData shopData) throws BackendException {
        shopData.setId(shopData.getTechKey().getIdAsString());

        shopData.clearMessages();

        if (isShopStoredinBackend) {
            JCoConnection connection = getDefaultJCoConnection();

            JCoHelper.ReturnValue retVal = crmIsaShopDataGet(this, shopData, 
                                                             connection);

            if (retVal.getReturnCode().length() > 0) {
				MessageCRM.addMessagesToBusinessObject(shopData, 
													   retVal.getMessages(), 
													   propertyMapping);
                MessageCRM.logMessagesToBusinessObject(shopData, 
                                                       retVal.getMessages());
            }
            else {
                MessageCRM.addMessagesToBusinessObject(shopData, 
                                                       retVal.getMessages(), 
													   propertyMapping);
            }

            connection.close();

            // synchronize the properties
            shopData.synchronizeProperties();
        }
        else {
            try {
                objectHelper.readData(shopData);
                shopData.synchronizeBeanProperties(this);
            }
            catch (MaintenanceObjectHelperException ex) {
                shopData.addMessage(ex.getIsaMessage());
            }
        }
    }

    /**
     * This method save the maintenance object in backend
     *
     * @param maintenanceObject
     */
    public void saveData(ShopData shopData) throws BackendException {
        shopData.clearMessages();
        shopData.setId(shopData.getId().toUpperCase());

        if (isShopStoredinBackend) {
            JCoConnection connection = getDefaultJCoConnection();

            JCoHelper.ReturnValue retVal = crmIsaShopDataSave(this, shopData, 
                                                              connection);

            MessageCRM.addMessagesToBusinessObject(shopData, 
                                                   retVal.getMessages(), 
                                                   propertyMapping);

            connection.close();
        }
        else {
            try {
                objectHelper.saveData(shopData);
            }
            catch (MaintenanceObjectHelperException ex) {
                shopData.addMessage(ex.getIsaMessage());
            }
        }
    }


	/**
	 * This method save the maintenance object in backend
	 *
	 * @param maintenanceObject
	 */
	public void getDescriptions(ShopData shopData) throws BackendException {
		
//		shopData.clearMessages();
		
		Iterator groupIterator = shopData.iterator();
		
		while (groupIterator.hasNext()) {
			Iterator iter =  ((PropertyGroupData) groupIterator.next()).iterator();
            	
			// delete descriptions
			while (iter.hasNext()) {
	            ((PropertyData) iter.next()).setValueDescription("");            
	        }
		}

		if (isShopStoredinBackend) {
			JCoConnection connection = getDefaultJCoConnection();

			JCoHelper.ReturnValue retVal = 
					crmIsaShopDescriptionsGet(this,
											  shopData, 
											  connection);

			MessageCRM.addMessagesToBusinessObject(shopData, 
												   retVal.getMessages(), 
												   propertyMapping);

			connection.close();
		}
	}


    /**
     * Adjust properties depending from the scenario 
     * 
     * @param shopData shop
     * @param scenario scenario
     */
    public void adjustToScenario(ShopData shopData, String scenario)
                          throws BackendException {
        if (isShopStoredinBackend) {
            JCoConnection connection = getDefaultJCoConnection();

            JCoHelper.ReturnValue retVal = crmIsaShopViewGet(shopData, scenario, 
                                                             connection);

            connection.close();

            if (retVal.getReturnCode().length() > 0) {
                MessageCRM.logMessagesToBusinessObject(shopData, 
                                                       retVal.getMessages());
            }
            else {
                MessageCRM.addMessagesToBusinessObject(shopData, 
                                                       retVal.getMessages(), 
													   propertyMapping);
            }

            // bestseller
            PropertyData property = shopData.getProperty("bestseller", 
                                                         "bestsellerTargetGroup");

            boolean isHidden = property.isHidden();
            String value = property.getString();

            property = shopData.getProperty("bestseller", "bestsellerAvailable");
            property.setHidden(isHidden);
            property.setAllowed(!isHidden);
            property.setValue(value.length() > 0);


            // personalRecommendation
            property = shopData.getProperty("personalRecommendation", 
                                            "attributeSetId");
            isHidden = property.isHidden();
            value = property.getString();

            property = shopData.getProperty("personalRecommendation", 
                                            "personalRecommendationAvailable");
            property.setHidden(isHidden);
            property.setAllowed(!isHidden);
            property.setValue(value.length() > 0);


            // cua
            property = shopData.getProperty("cua", "methodeSchema");
            isHidden = property.isHidden();
            value = property.getString();

            property = shopData.getProperty("cua", "cuaAvailable");
            property.setAllowed(!isHidden);
            property.setHidden(isHidden);
            property.setValue(value.length() > 0);

            // allowed extented quotions, only if the process type could be set.
            property = shopData.getProperty("quotation","quotationProcessType");
            isHidden = property.isHidden();

            property = shopData.getProperty("quotation","quotationMode");
            Iterator iter = property.iterator();
            while (iter.hasNext()) {
                AllowedValueData allowedValue = (AllowedValueData) iter.next();
                
                if  (allowedValue.getValue().equals("B")) {
                    allowedValue.setHidden(isHidden);
                }
            }
            if (isHidden && property.getString().equals("B")) {
                property.setValue("A");
            }
            
            // large documents
            property = shopData.getProperty("transactions", "itemsThresholdValue");
            boolean isAllowed = property.isAllowed();
            property = shopData.getProperty("transactions", "itemsThresholdMode");
            property.setAllowed(isAllowed);
            property.setHidden(!isAllowed);

			property = shopData.getProperty("Catalog group","pricingMode");
			iter = property.iterator();
			
			// no support of two prices for collabrative shop room and order on behalf
			isHidden = scenario.equals("CCH") || scenario.equals("OOB"); 
			while (iter.hasNext()) {
				AllowedValueData allowedValue = (AllowedValueData) iter.next();
                
				if  (allowedValue.getValue().equals("3")) {
					allowedValue.setHidden(isHidden);
				}
			}

            // Use radio buttons only if template and basketload is allowed
            property = shopData.getProperty("ordertemplate","templateMode");          
            PropertyData templateAllowed = shopData.getProperty("ordertemplate","templateAllowed");
            PropertyData basketloadAllowed = shopData.getProperty("ordertemplate","basketloadAllowed");
            
            if (templateAllowed.isAllowed() && basketloadAllowed.isAllowed()) {
                property.setAllowed(true);
                property.setHidden(false);
                templateAllowed.setHidden(true);  
                basketloadAllowed.setHidden(true); 
                if (isTemplateAllowed()) {
                    property.setValue("templateAllowed");
                }
                else if(isBasketloadAllowed()) {
                    property.setValue("basketloadAllowed");
               }
               else {
                   if (!shopData.getTechKey().isInitial()) {
                       property.setValue("");   
                   }
               }                 
            } 
            else { 
                property.setValue("");
                property.setAllowed(false);
                property.setHidden(true);
            }
            
            // adjust the values of the backend object to the changes!
            shopData.synchronizeBeanProperties(this);            

            // adjustPrivateProperties(this);
        }
    }

    /**
     * Returns the help values given within the property
     * @see com.sap.isa.maintenanceobject.backend.boi.MaintenanceObjectBackend#getHelpDescriptions(MaintenanceObjectData, PropertyData)
     */
    public HelpDescriptions getHelpDescriptions(MaintenanceObjectData maintenanceObject, 
                                                PropertyData property)
                                         throws BackendException {
        
        
        if (property.isHelpAvailable()) {
			List helpDescription = property.getHelpDescription();
			if (helpDescription != null && helpDescription.size() > 0) {
				return new HelpDescriptions(HelpDescriptions.RESOURCE_KEY,
														   property.getHelpDescription());
			} else {
				JCoConnection connection = getDefaultJCoConnection();

				BackendPropertiesData backendProperties = (BackendPropertiesData) property.getBackendProperties();
				String fieldName = "";
				if ( backendProperties != null) {
					fieldName = backendProperties.getFieldName();
				}

				HelpDescriptions helpDescriptions = crmIsaFieldHelpGet(fieldName, 
																	   connection);

				connection.close();

				return helpDescriptions;				
			}
        }

        return null;
    }

    /**
     * Check dependencies between serval properties 
     * 
     * @param shopData shop
     */
    public void checkDependencies(ShopData shopData) throws BackendException {
    	
        PropertyData property;

        // check if catalog fields must be displayed
        PropertyData propCatalog = shopData.getProperty("Catalog group", 
                                                        "catalog");
        PropertyData propVariant = shopData.getProperty("Catalog group", 
                                                        "catalogVariant");
        PropertyData propView = shopData.getProperty("Catalog group", 
                                                     "catalogView");

        if (isB2c() || (isB2b() && !isCatalogDeterminationActived())) {
            propCatalog.setHidden(false);
            propCatalog.setRequired(true);
            propVariant.setHidden(false);
            propVariant.setRequired(true);
            propView.setHidden(false);
        }
        else {
            propCatalog.setHidden(true);
            propCatalog.setRequired(false);
            propVariant.setHidden(true);
            propVariant.setRequired(false);
            propView.setHidden(true);
        }


		if (isInternalCatalogNotAvailable()) {
		    // disable marketing function
		    setBestsellerAvailable(false);
		    property = shopData.getProperty("bestseller", "bestsellerAvailable");
            property.setValue(false);
            property.setHidden(true);
            
            setPersonalRecommendationAvailable(false);
            property = shopData.getProperty("personalRecommendation", "personalRecommendationAvailable");
            property.setValue(false);
            property.setHidden(true);
            
            setCuaAvailable(false);
            property = shopData.getProperty("cua", "cuaAvailable");
            property.setValue(false);
            property.setHidden(true);
		} 
		else {
            property = shopData.getProperty("bestseller", "bestsellerAvailable");
            property.setHidden(false);
            property = shopData.getProperty("personalRecommendation", "personalRecommendationAvailable");
            property.setHidden(false);
            property = shopData.getProperty("cua", "cuaAvailable");
            property.setHidden(false);	    
		}


        // catalog determination only for b2b
        property = shopData.getProperty("Catalog group", 
                                        "catalogDeterminationActived");

        if (isB2b()) {
            property.setHidden(false);
        }
        else {
            property.setHidden(true);
        }

        // bestseller
		setPropertyFromFlag(shopData, "bestseller", 
							"bestsellerTargetGroup", isBestsellerAvailable );

        // personalRecommendation
		setPropertyFromFlag(shopData, "personalRecommendation", 
                            "attributeSetId", isPersonalRecommendationAvailable );

        // cua
		setPropertyFromFlag(shopData, "cua", "methodeSchema", isCuaAvailable );
		setPropertyFromFlag(shopData, "cua", "globalCuaTargetGroup", isCuaAvailable && !isCuaPersonalized);
		setPropertyFromFlag(shopData, "cua", "cuaTargetGroup", isCuaAvailable && isCuaPersonalized);

        property = shopData.getProperty("cua", "cuaPersonalized");
        if (isCuaAvailable) {
            property.setHidden(false);
        }
        else {
            property.setHidden(true);
        }


        // cua for unknown user
        property = shopData.getProperty("cua", "marketingForUnknownUserAllowed");
        
        if (isCuaAvailable && isPersonalRecommendationAvailable) {
            property.setHidden(false);
        }
        else {
            property.setHidden(true);
        }
        
		// large documents
		property = shopData.getProperty("transactions", "itemsThresholdValue");
		if (getItemsThresholdMode().equals("")) {
			property.setHidden(true);
			property.setRequired(false);
		}
		else if (getItemsThresholdMode().equals("X")) { 
			property.setHidden(false);
			property.setRequired(true);
		}        

		// If B2B scenario we are able to maintain process type or process type group
		property = shopData.getProperty("order", "orderGroupSelected");
		PropertyData propType = shopData.getProperty("order", "processType");
		PropertyData propTypeGr = shopData.getProperty("order", "orderProcessTypeGroup");
		if (isB2b() && !isB2c()) {
			property.setHidden(false);
			
			if (getOrderGroupSelected().equals("")) {
				propType.setHidden(false);
				propType.setRequired(true);

				propTypeGr.setHidden(true);
				propTypeGr.setRequired(false);
			}
			else if (getOrderGroupSelected().equals("X")) {
				propType.setHidden(true);
				propType.setRequired(false);

				propTypeGr.setHidden(false);
				propTypeGr.setRequired(true);				
			}
		}
		else {
			property.setHidden(true);
		}
	
		
				
		if (isB2b() && isB2c()) {
			propType.setHidden(false);
			propType.setRequired(true);			

			propTypeGr.setHidden(false);
			propTypeGr.setRequired(false);			
			propTypeGr.setDescription("shopadmin.prop.processTypesB2B");
		}
		else {
			propType.setDescription("shopadmin.prop.processType");
			
			propTypeGr.setDescription("shopadmin.prop.processTypes");			
		}

		setPropertyFromFlag(shopData, "contract", "contractProfile", isContractAllowed());


		setPropertyFromFlag(shopData, "contractNegotiation",
		                     "contractProcessTypeGroup", isContractNegotiationAllowed());
       
		PropertyData soldtoSelctable = shopData.getProperty("HiddenProperties", "soldtoSelectable");
		PropertyData lateDecison = shopData.getProperty("transactions", "docTypeLateDecision");
		property = shopData.getProperty("quotation", "quotationMode");
		PropertyData  templateAllowed = shopData.getProperty("ordertemplate", "templateAllowed");       
		if (soldtoSelctable.getBoolean() && !lateDecison.getBoolean()) {
			property.setValue("");
			property.setHidden(true);
			templateAllowed.setValue(false);
			templateAllowed.setHidden(true);
		}
		else {
			property.setHidden(false);
			templateAllowed.setHidden(false);			
		}

        PropertyData templateMode = shopData.getProperty("ordertemplate","templateMode");
        PropertyData basketloadAllowed = shopData.getProperty("ordertemplate","basketloadAllowed");
        PropertyGroupData groupTemplate = shopData.getPropertyGroup("ordertemplate");   
                         
        if (templateAllowed.isAllowed() && basketloadAllowed.isAllowed()) {
            // Use radio buttons
            templateMode.setAllowed(true);
            templateMode.setHidden(false);
            templateAllowed.setHidden(true);  
            basketloadAllowed.setHidden(true);
            // set the correct Heading
            groupTemplate.setDescription("shopadmin.group.ordertemplate");
        } 
        else { 
            // Hide radio buttons instead use check boxes
            templateMode.setValue("");
            templateMode.setAllowed(false);
            templateMode.setHidden(true);
//          set the correct Heading
            if (isB2c()) {
                groupTemplate.setDescription("shopadmin.group.basketallow");
            }
            else {
                groupTemplate.setDescription("shopadmin.group.templateallow");
            }
        }

        // extended quoations
		// If B2B scenario we are able to maintain process type or process type group
		property = shopData.getProperty("quotation", "quotationGroupSelected");       
        propType = shopData.getProperty("quotation", "quotationProcessType");
		propTypeGr = shopData.getProperty("quotation", "quotationProcessTypeGroup");

        if (getQuotationMode().equals("B")) {
			if (isB2b()) {
				property.setHidden(false);
			}        	
			if (getQuotationGroupSelected().equals("")) {
				propType.setHidden(false);
				propType.setRequired(true);

				propTypeGr.setHidden(true);
				propTypeGr.setRequired(false);
			}
			else if (getQuotationGroupSelected().equals("X")) {
				propType.setHidden(true);
				propType.setRequired(false);

				propTypeGr.setHidden(false);
				propTypeGr.setRequired(true);				
			}
        }
        else {
			property.setHidden(true);
			
            propType.setHidden(true);
            propType.setRequired(false);
            
			propTypeGr.setHidden(true);
			propTypeGr.setRequired(false);            
        }

        property = shopData.getProperty("quotation", "quotationSearchAll");

        if (getQuotationMode().equals("B")) {
            property.setHidden(false);
        }
        else {
            property.setHidden(true);
        }

		// Store Locator
		setPropertyFromFlag(shopData, "storeLocator", "searchCriteriaMode", isStoreLocatorAvailable());
		setPropertyFromFlag(shopData, "storeLocator", "businessPartnerId", isStoreLocatorAvailable());
		setPropertyFromFlag(shopData, "storeLocator", "relationshipGroup", isStoreLocatorAvailable());
		if (isStoreLocatorAvailable()) {
			// Store Locator in Collaborative Showroom
			property = shopData.getProperty("storeLocator", "searchCriteriaMode");
			PropertyData propBP = shopData.getProperty("storeLocator", "businessPartnerId");
			PropertyData propRelShipGr = shopData.getProperty("storeLocator", "relationshipGroup");
			if (isPartnerBasket()) {
				property.setHidden(false);
				property.setRequired(false);
				if (searchCriteriaMode.equals("")) {
					propBP.setHidden(true);
					propBP.setRequired(false);
					propRelShipGr.setHidden(true);
					propRelShipGr.setRequired(false);
				} else {
					propBP.setHidden(false);
					propBP.setRequired(true);
					propRelShipGr.setHidden(false);
					propRelShipGr.setRequired(true);					
				}
			} else {
				property.setHidden(true);
			}			
		}

        
		setPropertyFromFlag(shopData, "ordertemplate", "templateProcessType", isTemplateAllowed());
        
        setPropertyFromProperty(shopData, "auctioning", "auctionOrderProcessType", "auctioning","auctionAllowed");

		setPropertyFromProperty(shopData, "auctioning", "auctionQuoteProcessType", "auctioning","auctionAllowed");

		setPropertyFromProperty(shopData, "auctioning", "auctionPriceConditionType", "auctioning","auctionAllowed");

		// get the changed shop descriptions
		getDescriptions(shopData);

        
    }

	/**
	 * Hide or show the property with the given name in the given group depending
	 * of the given property. <br>
	 * 
	 * @param object 
	 * @param groupName
	 * @param propertyName
	 * @param flag
	 */
	protected void setPropertyFromProperty(MaintenanceObjectData object,
									  String groupName,
									  String propertyName,	 
									  String compareGroupName,
									  String comparePropertyName) {

		PropertyData property = object.getProperty(compareGroupName, comparePropertyName);
		
		setPropertyFromFlag(object,groupName,propertyName,property.getBoolean());

	}									  	

	
    /**
     * Hide or show the property with the given name in the given group depending
     * of the given flag. <br>
     * 
     * @param object 
     * @param groupName
     * @param propertyName
     * @param flag
     */
    protected void setPropertyFromFlag(MaintenanceObjectData object,
    								  String groupName,
    								  String propertyName,	 
    								  boolean flag) {

        PropertyData property = object.getProperty(groupName, propertyName);
        
        if (property != null) {
			if (flag) {
				property.setHidden(false);
				property.setRequired(true);
			}
			else {
				property.setHidden(true);
				property.setRequired(false);        	
			}
        }
        
    }
    
    
	/**
	 * Check if the given user is authorized to maintain shops. <br>
	 * 
	 * @param user user to check
	 * @return <code>true</code> if the user is authorized
	 * @throws BackendException
	 */
	public boolean isUserAuthorized (UserBaseData user)
			throws BackendException {
				
		JCoConnection connection = getDefaultJCoConnection();

		List fieldValues = new ArrayList(1);
		
		fieldValues.add(new String[] {"ACTVT","01"} );

		String returnCode = authorityCheck(user.getTechKey().getIdAsString(),
		                                   AUTHORITY_OBJECT_SHOP_ADMIN, 
		                                   fieldValues,  
		                                   connection);				

		connection.close();
				
		return returnCode.length()>0?false:true;		
	}		  
    

    /* *******************************************************************************
    * HelpValues 
    ********************************************************************************/

    /**
     * Returns the catalog list for the given search expression which is given
     * within the property
     *
     * @param shop shop object
     * @param property the catalog property
     */
    public HelpValues getCatalogList(MaintenanceObjectData shop, 
                                     PropertyData property)
                              throws BackendException {
        Table catalogTable = new Table("CatalogList");

        catalogTable.addColumn(Table.TYPE_STRING, "Id", 
                               "shopadmin.resultdata.catalogId");

        catalogTable.addColumn(Table.TYPE_STRING, "Description", 
                               "shopadmin.rd.catalogDescription");

        JCoConnection connection = getDefaultJCoConnection();

        crmIsaHelpvaluesCatalog(catalogTable, catalog, scenario, connection);

        connection.close();

        HelpValues helpValues = new HelpValues(
                                        "shopadmin.resultdata.ctlgSearch", 
                                        new ResultData(catalogTable));

        helpValues.addPropertyLink("catalog", "Id");

        return helpValues;
    }

    /**
     * Returns the catalog list for the given search expression which is given
     * within the property
     *
     * @param shop shop object
     * @param property the catalog property
     */
    public HelpValues getCatalogVariantList(MaintenanceObjectData shop, 
                                            PropertyData property)
                                     throws BackendException {
        Table table = new Table("HelpValues");

        table.addColumn(Table.TYPE_STRING, "CatalogId", 
                        "shopadmin.resultdata.catalogId");

        table.addColumn(Table.TYPE_STRING, "Id", 
                        "shopadmin.resultdata.variantId");

        table.addColumn(Table.TYPE_STRING, "Description", 
                        "shopadmin.rd.variantDescription");

        JCoConnection connection = getDefaultJCoConnection();

        crmIsaHelpvaluesVariant(table, catalog, catalogVariant, scenario, connection);

        connection.close();

        HelpValues helpValues = new HelpValues("shopadmin.rd.variantSearch", 
                                               new ResultData(table));

        helpValues.addPropertyLink("catalogVariant", "Id");
        helpValues.addPropertyLink("catalog", "CatalogId");

        return helpValues;
    }

    /**
     * Returns the view list for the given search expression which is given
     * within the property
     *
     * @param shop shop object
     * @param property the view property
     */
    public HelpValues getCatalogViewList(MaintenanceObjectData shop, 
                                         PropertyData property)
                                  throws BackendException {
        Table table = new Table("HelpValues");

        table.addColumn(Table.TYPE_STRING, "CatalogId", 
                        "shopadmin.resultdata.catalogId");

        table.addColumn(Table.TYPE_STRING, "Id", 
                        "shopadmin.rd.viewId");

        table.addColumn(Table.TYPE_STRING, "Description", 
                        "shopadmin.rd.variantDescription");

        JCoConnection connection = getDefaultJCoConnection();

        crmIsaHelpvaluesView(table, catalog,
                                    catalogView,
                                    scenario,  
                                    connection);

        connection.close();

        HelpValues helpValues = new HelpValues(
                                        "shopadmin.rd.viewSearch", 
                                        new ResultData(table));

        helpValues.addPropertyLink("catalogView", "Id");
        helpValues.addPropertyLink("catalog", "CatalogId");

        return helpValues;
    }

    /**
     * Returns the catalog list for the given search expression which is given
     * within the property
     *
     * @param shop shop object
     * @param property the catalog property
     */
    public HelpValues getAttributeSetList(MaintenanceObjectData shop, 
                                          PropertyData property)
                                   throws BackendException {
        Table valueTable = new Table("ValueList");

        valueTable.addColumn(Table.TYPE_STRING, "Id", 
                             "shopadmin.resultdata.profileId");

        valueTable.addColumn(Table.TYPE_STRING, "Description", 
                             "shopadmin.resultdata.Description");

        JCoConnection connection = getDefaultJCoConnection();

        crmIsaHelpvaluesAttributeSet(valueTable, property.getString(), connection);

        connection.close();

        HelpValues helpValues = new HelpValues("shopadmin.rd.ProfileSearch", 
                                               new ResultData(valueTable));

        helpValues.addPropertyLink(property.getName(), "Id");

        return helpValues;
    }

    /**
     * Returns the target group list for the given search expression which is given
     * within the property
     *
     * @param shop shop object
     * @param property the catalog property
     */
    public HelpValues getTargetGroupList(MaintenanceObjectData shop, 
                                         PropertyData property)
                                  throws BackendException {
        Table table = new Table("TargetGroupList");

        table.addColumn(Table.TYPE_STRING, "Description", 
                        "shopadmin.resultdata.targetGroup");

        JCoConnection connection = getDefaultJCoConnection();

        crmIsaHelpvaluesTargetgroup(table, property.getString(), connection);

        connection.close();

        HelpValues helpValues = new HelpValues("shopadmin.rd.targetGroupSearch", 
                                               new ResultData(table));

        helpValues.addPropertyLink(property.getName(), "Description");

        return helpValues;
    }

    /**
     * Returns the partner function list for the given search expression which is given
     * within the property
     *
     * @param shop shop object
     * @param property property which contains a  function 
     */
    public HelpValues getPartnerFunctionContact(MaintenanceObjectData shop, 
                                                PropertyData property)
                                         throws BackendException {
        return getPartnerFunction(shop, property, "0007");
    }

    /**
     * Returns the partner function list for the given search expression which is given
     * within the property
     *
     * @param shop shop object
     * @param property property which contains a  function 
     * @param  function type to use 
     */
    public HelpValues getPartnerFunction(MaintenanceObjectData shop, 
                                         PropertyData property, 
                                         String partnerFunctionType)
                                  throws BackendException {
        Table table = new Table("Function");

        table.addColumn(Table.TYPE_STRING, "Id", "shopadmin.resultdata.fctId");

        table.addColumn(Table.TYPE_STRING, "Description", 
                        "shopadmin.resultdata.fct");

        JCoConnection connection = getDefaultJCoConnection();

        crmIsaHelpvaluesFct(table, 
                                   property.getString(), 
								   partnerFunctionType, 
                                   connection);

        connection.close();

        HelpValues helpValues = new HelpValues("shopadmin.resultdata.fctSearch", 
                                               new ResultData(table));

        helpValues.addPropertyLink(property.getName(), "Id");

        return helpValues;
    }

    /**
     * Returns the country group list for the given search expression which is given
     * within the property
     *
     * @param shop shop object
     * @param property the country group property
     */
    public HelpValues getCountryGroupList(MaintenanceObjectData shop, 
                                          PropertyData property)
                                   throws BackendException {
        String[] resourceKeys = new String[] {
            "shopadmin.rd.countryGroup", "shopadmin.resultdata.Description", 
            "shopadmin.rd.countryGrpSearch"
        };

        return getHelpValues(shop, property, "DESCR", resourceKeys);
    }

    /**
     * Returns the process type list for the given search expression which is given
     * within the property
     *
     * @param shop shop object
     * @param property process type
     */
    public HelpValues getProcessTypeList(MaintenanceObjectData shop, 
                                         PropertyData property)
                                  throws BackendException {
        String[] resourceKeys = new String[] {
            "shopadmin.rd.processType", "shopadmin.resultdata.Description", 
            "shopadmin.rd.processTypeSearch"
        };

        return getHelpValues(shop, property, "P_DESCRIPTION_20", resourceKeys);
    }

    /**
     * Returns the contract profile list for the given search expression which is given
     * within the property
     *
     * @param shop shop object
     * @param property contract profile
     */
    public HelpValues getContractProfileList(MaintenanceObjectData shop, 
                                             PropertyData property)
                                      throws BackendException {
        String[] resourceKeys = new String[] {
            "shopadmin.rd.cntProfil", "shopadmin.resultdata.Description", 
            "shopadmin.rd.cntProfilSearch"
        };

        return getHelpValues(shop, property, "DESCRIPTION", resourceKeys);
    }

    /**
     * Returns the contract process types group list for the given search expression which is given
     * within the property
     *
     * @param shop shop object
     * @param property contract process type group
     */
    public HelpValues getContractProcessTypes(MaintenanceObjectData shop, 
                                              PropertyData property)
                                       throws BackendException {
        String[] resourceKeys = new String[] {
            "shopadmin.rd.cntProcessTypes", "shopadmin.resultdata.Description", 
            "shopadmin.rd.cntProcessTypesSe"
        };

        return getHelpValues(shop, property, "DESCRIPTION", resourceKeys);
    }

	/**
	 * Returns the order or quotation process types group list for the given search expression 
	 * which is given within the property
	 *
	 * @param shop shop object
	 * @param property order or quotation process type group
	 */
	public HelpValues getOrdQutProcessTypes(MaintenanceObjectData shop, 
											  PropertyData property)
									   throws BackendException {
		String[] resourceKeys = new String[] {
			"shopadmin.rd.oqtProcessTypes", "shopadmin.resultdata.Description", 
			"shopadmin.rd.oqtProcessTypesSe"
		};

		return getHelpValues(shop, property, "DESCRIPTION", resourceKeys);
	}

    /**
     * Returns the method scheme list for the given search expression which is given
     * within the property
     *
     * @param shop shop object
     * @param property method scheme
     */
    public HelpValues getMethodSchemeList(MaintenanceObjectData shop, 
                                          PropertyData property)
                                   throws BackendException {
        String[] resourceKeys = new String[] {
            "shopadmin.rd.methodScheme", "shopadmin.resultdata.Description", 
            "shopadmin.rd.methodSchemeSearch"
        };

        return getHelpValues(shop, property, "DESCRIPTION", resourceKeys);
    }

    /**
     * Returns the user status procedure list for the given search expression which is given
     * within the property
     *
     * @param shop shop object
     * @param property user status procedure
     */
    public HelpValues getUserStatusProcedures(MaintenanceObjectData shop, 
                                              PropertyData property)
                                       throws BackendException {
        String[] resourceKeys = new String[] {
            "shopadmin.rd.userStatus", "shopadmin.resultdata.Description", 
            "shopadmin.rd.userStatusSearch"
        };

        return getHelpValues(shop, property, "TXT", resourceKeys);
    }

    /**
     * Returns the document pricing procedure list for the given search expression which is given
     * within the property
     *
     * @param shop shop object
     * @param property document pricing procedure
     */
    public HelpValues getDocumentPricing(MaintenanceObjectData shop, 
                                         PropertyData property)
                                  throws BackendException {
        String[] resourceKeys = new String[] {
            "shopadmin.rd.docPricing", "shopadmin.resultdata.Description", 
            "shopadmin.rd.docPricingSearch"
        };

        return getHelpValues(shop, property, "DESCRIPTION", resourceKeys);
    }

    
	/**
	 * Returns the condition group list for the given search expression which is
	 * given within the property
	 *
	 * @param shop shop object
	 * @param property the sales office property
	 */
	public HelpValues getExchangeConditionGroups(MaintenanceObjectData shop, 
												 PropertyData property)
			throws BackendException {
		String[] resourceKeys = new String[] {
			"shopadmin.rd.exchangeGroupId", "shopadmin.resultdata.Description", 
			"shopadmin.rd.exchangeGroup"
		};
 
		return getHelpValues(shop, property, "DESCRIPTION", resourceKeys);
	}


    /**
     * Returns the sales office list for the given search expression which is
     * given within the property
     *
     * @param shop shop object
     * @param property the sales office property
     */
    public HelpValues getSalesOfficeList(MaintenanceObjectData shop, 
                                         PropertyData property)
                                   throws BackendException {
        String[] resourceKeys = new String[] {
            "shopadmin.rd.salesOffice", "shopadmin.resultdata.Description", 
            "shopadmin.rd.salesOfficeSe"
        };

        return getHelpValues(shop, property, "TEXT", resourceKeys);
    }

	/**
	 * Returns the relationship group list for the given search expression which is given
	 * within the property
	 *
	 * @param shop shop object
	 * @param property process type
	 */
	public HelpValues getRelationGroupList(MaintenanceObjectData shop, 
										 PropertyData property)
								  throws BackendException {
		String[] resourceKeys = new String[] {
			"shopadmin.rd.relshipGr", "shopadmin.resultdata.Description", 
			"shopadmin.rd.relshipGrSearch"
		};

		return getHelpValues(shop, property, "DESCRIPTION", resourceKeys);
	}


    private HelpValues getHelpValues(MaintenanceObjectData shop, 
                                     PropertyData property, 
                                     String descriptionName, 
                                     String[] resourceKeys)
                              throws BackendException {
        Table valueTable = new Table("ValueList");

        valueTable.addColumn(Table.TYPE_STRING, "Id", 
                             resourceKeys[0]);

        valueTable.addColumn(Table.TYPE_STRING, "Description", 
                             resourceKeys[1]);

        JCoConnection connection = getDefaultJCoConnection();

		BackendPropertiesData backendProperties = (BackendPropertiesData) property.getBackendProperties();
		String fieldName = "";
		if ( backendProperties != null) {
			fieldName = backendProperties.getFieldName();
		}

        crmIsaShopHelpvalues(valueTable, property.getString(), fieldName, 
                             descriptionName, connection);

        connection.close();

        HelpValues helpValues = new HelpValues(
                                        resourceKeys[2], 
                                        new ResultData(valueTable));

        helpValues.addPropertyLink(property.getName(), "Id");

        return helpValues;
    }

    /**
     * Initializes Business Object.
     *
     * @param props a set of properties which may be useful to initialize the object
     * @param params a object which wraps parameters
     */
    public void initBackendObject(Properties props, 
                                  BackendBusinessObjectParams params)
                           throws BackendException {
        
        initProperties = (Properties)props.clone();
        initParameters = params;

        try {

			if (!isShopStoredinBackend) {
				JCoConnection connection = getDefaultJCoConnection();
				JCO.Attributes attributes = connection.getAttributes();
				initProperties.setProperty(J2EEPersistencyDataHandler.CLIENT , attributes.getClient());
				initProperties.setProperty(J2EEPersistencyDataHandler.SYSTEM , attributes.getSystemID());     

			}

            /* create the object helper */
            objectHelper = new  MaintenanceObjectHelper(getConnectionFactory(), 
														initProperties);

        }
        catch (Exception ex) {
            throw new BackendException(
                    "Could not initialize the backend object");
        }
    }

    /**
     * Delete a lock from the maintenance object
     *
     * @param maintenanceObject
     */
    public void unlockObject(MaintenanceObjectData maintenanceObject)
                      throws BackendException {
        if (isShopStoredinBackend) {
            JCoConnection connection = getDefaultJCoConnection();

            crmIsaShopDequeue(maintenanceObject.getTechKey().getIdAsString(), 
                              connection);

            connection.close();
        }
        else {
            /*
                        try {
                            objectHelper.saveData(shopData);
                        }
                        catch (MaintenanceObjectHelperException ex) {
                            shopData.addMessage(ex.getIsaMessage());
                        }
             */
        }
    }

    /**
     * Set a lock to the maintenance object
     *
     * @param maintenanceObject
     */
    public boolean lockObject(MaintenanceObjectData maintenanceObject)
                       throws BackendException {
        if (isShopStoredinBackend) {
            JCoConnection connection = getDefaultJCoConnection();

            JCoHelper.ReturnValue retVal = crmIsaShopEnqueue(
                                                   maintenanceObject.getTechKey()
                                                                    .getIdAsString(), 
                                                   connection);

            MessageCRM.addMessagesToBusinessObject(maintenanceObject, 
                                                   retVal.getMessages(), 
												   propertyMapping);
            connection.close();

            if (retVal.getReturnCode().length() > 0) {
                return false;
            }
        }
        else {
            /*            try {
            //                objectHelper.saveData(shopData);
                        }
                        catch (MaintenanceObjectHelperException ex) {
            //                shopData.addMessage(ex.getIsaMessage());
                        }
             */
        }

        return true;
    }


		
    /**
     * Set the property b2c
     *
     * @param b2c
     *
     */
    public void setB2c(boolean b2c) {
        this.isB2c = b2c;
    }

    /**
     * Returns the property b2c
     *
     * @return b2c
     *
     */
    public boolean isB2c() {
        return this.isB2c;
    }

    /**
     * Set the property b2b
     *
     * @param b2b
     *
     */
    public void setB2b(boolean b2b) {
        this.isB2b = b2b;
    }

    /**
     * Returns the property b2b
     *
     * @return b2b
     *
     */
    public boolean isB2b() {
        return this.isB2b;
    }

    /**
     * Set the property isBestsellerAvailable
     *
     * @param isBestsellerAvailable
     */
    public void setBestsellerAvailable(boolean isBestsellerAvailable) {
        this.isBestsellerAvailable = isBestsellerAvailable;
    }

    /**
     * Returns the property isBestsellerAvailable
     *
     * @return isBestsellerAvailable
     */
    public boolean isBestsellerAvailable() {
        return isBestsellerAvailable;
    }

    /**
     * Set the property catalog
     * <b>Don't use this method, use readOrgData instead <br>
     *    This method is only needed in the backend!</b>
     *
     * @param catalog
     */
    public void setCatalog(String catalog) {
        this.catalog = catalog.toUpperCase();
    }

    /**
     * Returns the property catalog
     *
     * @return catalog
     */
    public String getCatalog() {
        return catalog;
    }

    /**
     * Set the property catalogDeterminationActived
     *
     * @param catalogDeterminationActived
     */
    public void setCatalogDeterminationActived(boolean catalogDeterminationActived) {
        this.isCatalogDeterminationActived = catalogDeterminationActived;
    }

    /**
     * Returns the property catalogDeterminationActived
     *
     * @return catalogDeterminationActived
     */
    public boolean isCatalogDeterminationActived() {
        return isCatalogDeterminationActived;
    }

    /**
     * Set the property contractAllowed
     *
     * @param contractAllowed
     */
    public void setContractAllowed(boolean contractAllowed) {
        this.isContractAllowed = contractAllowed;
    }

    /**
     * Returns the property contractAllowed
     *
     * @return contractAllowed
     */
    public boolean isContractAllowed() {
        return isContractAllowed;
    }

    /**
     * Set the property isCuaAvailable
     *
     * @param isCuaAvailable
     */
    public void setCuaAvailable(boolean isCuaAvailable) {
        this.isCuaAvailable = isCuaAvailable;
    }

    /**
     * Returns the property isCuaAvailable
     *
     * @return isCuaAvailable
     */
    public boolean isCuaAvailable() {
        return isCuaAvailable;
    }

 
    /**
     * Set the property personalRecommendationAvailable
     *
     * @param personalRecommendationAvailable
     *
     */
    public void setPersonalRecommendationAvailable(boolean personalRecommendationAvailable) {
        this.isPersonalRecommendationAvailable = personalRecommendationAvailable;
    }

    /**
     * Returns the property personalRecommendationAvailable
     *
     * @return personalRecommendationAvailable
     *
     */
    public boolean isPersonalRecommendationAvailable() {
        return this.isPersonalRecommendationAvailable;
    }


    /**
     * Set the property isRecommendationAvailable
     *
     * @param isRecommendationAvailable
     */
    public void setRecommendationAvailable(boolean isRecommendationAvailable) {
        this.isRecommendationAvailable = isRecommendationAvailable;
    }

    /**
     * Returns the property isRecommendationAvailable
     *
     * @return isRecommendationAvailable
     */
    public boolean isRecommendationAvailable() {
        return isRecommendationAvailable;
    }

    /**
     * Set the property scenario
     *
     * @param scenario  use constants B2B and B2C
     */
    public void setScenario(String scenario) {
        this.scenario = scenario;
    }

    /**
     * Returns the property szenario
     *
     * @return scenario use constants B2B and B2C
     */
    public String getScenario() {
        return scenario;
    }

    /**
     * Set the property catalogView
     *
     * @param catalogView
     *
     */
    public void setCatalogView(String catalogView) {
        this.catalogView = catalogView.toUpperCase();
    }

    /**
     * Returns the property catalogView
     *
     * @return catalogView
     *
     */
    public String getCatalogView() {
        return this.catalogView;
    }

    /**
     * Set the property userProfileAvailable
     *
     * @param userProfileAvailable
     *
     */
    public void setUserProfileAvailable(boolean userProfileAvailable) {
        this.isUserProfileAvailable = userProfileAvailable;
    }

    /**
     * Returns the property userProfileAvailable
     *
     * @return userProfileAvailable
     *
     */
    public boolean isUserProfileAvailable() {
        return isUserProfileAvailable;
    }

    /**
     * Set the property attributeSetId
     *
     * @param attributeSetId
     *
     */
    public void setAttributeSetId(String attributeSetId) {
        this.attributeSetId = attributeSetId.toUpperCase();
    }

    /**
     * Returns the property attributeSetId
     *
     * @return attributeSetId
     *
     */
    public String getAttributeSetId() {
        return this.attributeSetId;
    }

    /**
     * Set the property isCuaPersonalized
     *
     * @param isCuaPersonalized
     *
     */
    public void setCuaPersonalized(boolean cuaPersonalized) {
        this.isCuaPersonalized = cuaPersonalized;
    }

    /**
     * Returns the property isCuaPersonalized
     *
     * @return isCuaPersonalized
     *
     */
    public boolean isCuaPersonalized() {
        return this.isCuaPersonalized;
    }


    /**
     * Set the property bestsellerTargetGroup
     *
     * @param bestsellerTargetGroup
     *
     */
    public void setBestsellerTargetGroup(String bestsellerTargetGroup) {
        this.bestsellerTargetGroup = bestsellerTargetGroup;
    }

    /**
     * Returns the property bestsellerTargetGroup
     *
     * @return bestsellerTargetGroup
     *
     */
    public String getBestsellerTargetGroup() {
        return this.bestsellerTargetGroup;
    }

    /**
     * Set the property methodeSchema
     *
     * @param methodeSchema
     *
     */
    public void setMethodeSchema(String methodeSchema) {
        this.methodeSchema = methodeSchema.toUpperCase();
    }

    /**
     * Returns the property methodeSchema
     *
     * @return methodeSchema
     *
     */
    public String getMethodeSchema() {
        return this.methodeSchema;
    }

    /**
     * Set the property catalogVariant
     *
     * @param catalogVariant
     *
     */
    public void setCatalogVariant(String catalogVariant) {
        this.catalogVariant = catalogVariant.toUpperCase();
    }

    /**
     * Returns the property catalogVariant
     *
     * @return catalogVariant
     *
     */
    public String getCatalogVariant() {
        return this.catalogVariant;
    }

    
    /**
     * Returns the contractNegotiationAllowed.
     * @return boolean
     */
    public boolean isContractNegotiationAllowed() {
        return contractNegotiationAllowed;
    }

    /**
     * Sets the contractNegotiationAllowed.
     * @param contractNegotiationAllowed The contractNegotiationAllowed to set
     */
    public void setContractNegotiationAllowed(boolean contractNegotiationAllowed) {
        this.contractNegotiationAllowed = contractNegotiationAllowed;
    }


    /**
     * Returns the quotationMode.
     * @return String
     */
    public String getQuotationMode() {
        return quotationMode;
    }


    /**
     * Sets the quotationMode.
     * @param quotationMode The quotationMode to set
     */
    public void setQuotationMode(String quotationMode) {
        this.quotationMode = quotationMode;
    }


    /**
     * Returns the partnerBasket.
     * @return boolean
     */
    public boolean isPartnerBasket() {
        return partnerBasket;
    }

    /**
     * Sets the partnerBasket.
     * @param partnerBasket The partnerBasket to set
     */
    public void setPartnerBasket(boolean Basket) {
        this.partnerBasket = Basket;
    }

    /**
     * Returns the templateAllowed.
     * @return boolean
     */
    public boolean isTemplateAllowed() {
        return templateAllowed;
    }

    /**
     * Sets the templateAllowed.
     * @param templateAllowed The templateAllowed to set
     */
    public void setTemplateAllowed(boolean templateAllowed) {
        this.templateAllowed = templateAllowed;
    }


	/**
	 * Returns the isInternalCatalogAvailable.
	 * @return boolean
	 */
	public boolean isInternalCatalogNotAvailable() {
		return isInternalCatalogNotAvailable;
	}

	/**
	 * Sets the isInternalCatalogAvailable.
	 * @param isInternalCatalogAvailable The isInternalCatalogAvailable to set
	 */
	public void setInternalCatalogNotAvailable(boolean internalCatalogNotAvailable) {
		this.isInternalCatalogNotAvailable = internalCatalogNotAvailable;
	}


	/**
	 * Returns the orderProcessTypeGroup.
	 * @return String
	 */
	public String getOrderProcessTypeGroup() {
		return orderProcessTypeGroup;
	}

	/**
	 * Sets the orderProcessTypeGroup.
	 * @param orderProcessTypeGroup The orderProcessTypeGroup to set
	 */
	public void setOrderProcessTypeGroup(String orderProcessTypeGroup) {
		this.orderProcessTypeGroup = orderProcessTypeGroup;
	}

	/**
	 * Returns the quotationProcessTypeGroup
	 * @return String
	 */
	public String getQuotationProcessTypeGroup() {
		return quotationProcessTypeGroup;
	}

	/**
	 * Sets the quotationProcessTypeGroup
	 * @param quotationProcessTypeGroup The quotationProcessTypeGroup to set
	 */
	public void setQuotationProcessTypeGroup(String quotationProcessTypeGroup) {
		this.quotationProcessTypeGroup = quotationProcessTypeGroup;
	}


	/**
	 * Returns the orderGroupSelected
	 * @return String
	 */
	public String getOrderGroupSelected() {
		return orderGroupSelected;
	}

	/**
	 * Sets the orderGroupSelected
	 * @param orderGroupSelected The orderGroupSelected to set
	 */
	public void setOrderGroupSelected(String orderGroupSelected) {
		this.orderGroupSelected = orderGroupSelected;
	}

	/**
	 * Returns the quotationGroupSelected
	 * @return String
	 */
	public String getQuotationGroupSelected() {
		return quotationGroupSelected;
	}

	/**
	 * Sets the quotationGroupSelected 
	 * @param quotationGroupSelected The quotationGroupSelected to set
	 */
	public void setQuotationGroupSelected(String quotationGroupSelected) {
		this.quotationGroupSelected = quotationGroupSelected;
	}
	
	/**
	 * Returns the itemsThresholdValue
	 * @return int
	 */
	public int getItemsThresholdValue() {
		return itemsThresholdValue;
	}
	
	/**
	 * Sets the itemsThresholdValue
	 * @param itemsThresholdValue The itemsThresholdValue to set
	 */
	public void setItemsThresholdValue(int itemsThresholdValue) {
		this.itemsThresholdValue = itemsThresholdValue;
	}
	
	/**
	 * Return the itemsThresholdMode
	 * @return String
	 */
	public String getItemsThresholdMode() {
		return itemsThresholdMode;
	}
	
	/**
	 * Sets the itemsThresholdMode 
	 * @param itemsThresholdMode The itemsThresholdMode to set
	 */
	public void setItemsThresholdMode(String itemsThresholdMode) {
		this.itemsThresholdMode = itemsThresholdMode;
	}	


	/**
	 * Returns the storeLocatorAvailable
	 * @return boolean
	 */
	public boolean isStoreLocatorAvailable() {
		return storeLocatorAvailable;
	}

	/**
	 * Sets the isStoreLocatorAvailable
	 * @param isStoreLocatorAvailable The isStoreLocatorAvailable to set
	 */
	public void setStoreLocatorAvailable(boolean isStoreLocatorAvailable) {
		this.storeLocatorAvailable = isStoreLocatorAvailable;
	}

	/**
	 * Returns the businessPartnerId
	 * @return String
	 */
	public String getBusinessPartnerId() {
		return businessPartnerId;
	}

	/**
	 * Sets the businessPartnerId
	 * @param businessPartnerId The businessPartnerId to set
	 */
	public void setBusinessPartnerId(String businessPartnerId) {
		this.businessPartnerId = businessPartnerId;
	}

	/**
	 * Return the relationshipGroup
	 * @return String
	 */
	public String getRelationshipGroup() {
		return relationshipGroup;
	}

	/**
	 * Sets the relationshipGroup
	 * @param relationshipGroup The relationshipGroup to set
	 */
	public void setRelationshipGroup(String relationshipGroup) {
		this.relationshipGroup = relationshipGroup;
	}

	/**
	 * Returns the searchCriteriaMode
	 * @return String
	 */
	public String getSearchCriteriaMode() {
		return searchCriteriaMode;
	}

	/**
	 * Sets the searchCriteriaMode
	 * @param searchCriteriaMode The searchCriteriaMode to set
	 */
	public void setSearchCriteriaMode(String searchCriteriaMode) {
		this.searchCriteriaMode = searchCriteriaMode;
	}

    /**
     * Returns the basketloadAllowed
     * @return boolean
     */
	public boolean isBasketloadAllowed() {
		return basketloadAllowed;
	}

    /**
     * Sets the isBasketloadAllowed
     * @param isBasketloadAllowed The isBasketloadAllowed to set
     */
	public void setBasketloadAllowed(boolean basketloadAllowed) {
		this.basketloadAllowed = basketloadAllowed;
	}

    /**
     * Sets the setIsEnrollmentAllowed
     * @param isEnrollmentAllowed The isEnrollmentAllowed to set
     */
    public void setIsEnrollmentAllowed(boolean isEnrollmentAllowed) {
        this.isEnrollmentAllowed = isEnrollmentAllowed;
    }

    /**
     * Returns the isEnrollmentAllowed()
     * @return boolean
     */
    public boolean isEnrollmentAllowed() {
        return this.isEnrollmentAllowed;
    }

    /**
     * Sets the text type for the campaign description
     * @param textId The text type for the campaigng description to set
     */
    public void setTextTypeForCampDescr(String textId) {
        this.textTypeForCampDescr = textId;
    }

    /**
     * Returns the text type for the campaign description
     * @return String
     */
    public String getTextTypeForCampDescr() {
        return this.textTypeForCampDescr;
    }

    /**
     * Returns the method scheme list for the given search expression which is given
     * within the property
     *
     * @param shop shop object
     * @param property text type
     */
    public HelpValues getTextTypeList(MaintenanceObjectData shop, 
                                      PropertyData property)
                      throws BackendException {
        String[] resourceKeys = new String[] {
            "shopadmin.rd.textType", "shopadmin.resultdata.Description", 
            "shopadmin.rd.textTypeSearch"
        };
    
        return getHelpValues(shop, property, "TDTEXT", resourceKeys);
    }


	/**
	 * Returns the property newsAttributeSetId
	 *
	 * @return newsAttributeSetId
	 *
	 */
	public String getNewsAttributeSetId() {
		return newsAttributeSetId;
	}

	/**
	 * Set the property newsAttributeSetId
	 *
	 * @param newsAttributeSetId
	 *
	 */
	public void setNewsAttributeSetId(String newsAttributeSetId) {
		this.newsAttributeSetId = newsAttributeSetId.toUpperCase();
	}

	
	/**
     * Returns a list of loyalty programs for given search expression which is given
     * within the property
     *
     * @param shop shop object
     * @param property the loyalty program ID property
     */
    public HelpValues getLoyaltyProgramList(MaintenanceObjectData shop, 
                                            PropertyData property)
                                     throws BackendException {
        Table table = new Table("HelpValues");

        table.addColumn(Table.TYPE_STRING, "LoyProgID", 
                        "shopadmin.rd.loyProgID");

        table.addColumn(Table.TYPE_STRING, "Description", 
                        "shopadmin.rd.loyProgDescription");

        JCoConnection connection = getDefaultJCoConnection();

        crmIsaHelpvaluesLoyProg(table, loyaltyProgramID, connection);

        connection.close();

        HelpValues helpValues = new HelpValues("shopadmin.rd.loyProgSearch", 
                                               new ResultData(table));

        helpValues.addPropertyLink(property.getName(), "LoyProgID");

        return helpValues;
    }

    
    /**
     * Returns a list of point codes given search expression which is given
     * within the property
     *
     * @param shop shop object
     * @param property the point code property
     */
    public HelpValues getLoyProgPointTypeList(MaintenanceObjectData shop, 
                                            PropertyData property)
                                     throws BackendException {
        Table table = new Table("HelpValues");

        table.addColumn(Table.TYPE_STRING, "PointType", 
                        "shopadmin.rd.pointType");

        table.addColumn(Table.TYPE_STRING, "Description", 
                        "shopadmin.rd.pointTypeDescription");

        JCoConnection connection = getDefaultJCoConnection();

        crmIsaHelpvaluesPointTypeLoyProg(table, loyaltyProgramID, pointType, connection);

        connection.close();

        HelpValues helpValues = new HelpValues("shopadmin.rd.pointTypeSearch", 
                                               new ResultData(table));

        helpValues.addPropertyLink(property.getName(), "PointType");

        return helpValues;
    }
    
    
    /**
     * Wrapper for CRM function module <code>CRM_ISA_HELPVALUES_LOYPROG</code>
     *
     * @param table  List of all available loyalty programs
     * @param catalogId
     * @param connection connection to use
     *
     * @return ReturnValue containing messages of call and (if present) the
     *                     return code generated by the function module.
     *
     */
    public static JCoHelper.ReturnValue crmIsaHelpvaluesLoyProg(Table table, 
                                                                String loyProgID, 
                                                                JCoConnection connection)
        throws BackendException {
    	
        try {
//        	if (!loyProgID.equals("")){
//                // now get repository infos about the function
//                JCO.Function function = connection.getJCoFunction(
//                                                "CRM_MKTPL_LOY_PROGRAM_READ_RFC");
//                
//                // getting import parameter
//                JCO.ParameterList importParams = function.getImportParameterList();
//        		
//                importParams.setValue(loyProgID, "CV_PROGRAM_ID");
//                
//                // call the function
//                connection.execute(function);
//
//        	}
//        	else {
        		// now get repository infos about the function
        		JCO.Function function = connection.getJCoFunction(
                                            "CRM_MKTPL_LOY_GET_ALL_PROG_RFC");

                // call the function
                connection.execute(function);
                
                JCO.Table values = function.getExportParameterList().getTable("ET_LOY_PROGRAM");
                
                int numValues = values.getNumRows();

                for (int i = 0; i < numValues; i++) {
                    String key = "" + i;

                    TableRow row = table.insertRow();

                    row.setRowKey(new TechKey(key));

                    row.getField("LoyProgID").setValue(values.getString("LOY_EXTERNAL_ID"));
                    row.getField("Description")
                       .setValue(values.getString("LOY_DESC"));

                    values.nextRow();
                }
                
                // write some useful logging information
                //if (log.isDebugEnabled()) {
                //    WrapperCrmIsa.logCall("CRM_ISA_SHOP_GETLIST", importParams, null,log);
                //    WrapperCrmIsa.logCall("CRM_ISA_SHOP_GETLIST", null, shops,log);
                //}
                return new JCoHelper.ReturnValue(null, "");
//        	}
        }
        catch (JCO.Exception ex) {
            JCoHelper.splitException(ex);
        }

        return null;
    }
    
   
    /**
     * Wrapper for CRM function module <code>CRM_ISA_HELPVALUES_POINTTYPE_LOYPROG</code>
     *
     * @param table  List of all available catalogs
     * @param catalogId
     * @param connection connection to use
     *
     * @return ReturnValue containing messages of call and (if present) the
     *                     return code generated by the function module.
     *
     */
    public static JCoHelper.ReturnValue crmIsaHelpvaluesPointTypeLoyProg(Table table, 
                                                                String loyProgID,
                                                                String pointType,
                                                                JCoConnection connection)
        throws BackendException {
    	
        try {
        	// get guid of loyalty program
            JCO.Function function = connection.getJCoFunction(
                                            "CRM_MKTPL_LOY_PROGRAM_READ_RFC");
       	
            // getting import parameter
            JCO.ParameterList importParams = function.getImportParameterList();
            
            importParams.setValue(loyProgID, "CV_PROGRAM_ID");
            
            // call the function
            connection.execute(function);

            
            // getting import parameter
            JCO.ParameterList exportParams = function.getExportParameterList();
                        
            String progIdGuid = exportParams.getField("CV_GUID").getString();
            
            
            // now get repository infos about the function
            function = connection.getJCoFunction(
                                            "CRM_MKTPL_LOY_POINT_IN_PRG_RFC");

            // getting import parameter
            importParams = function.getImportParameterList();

            importParams.setValue(progIdGuid, "IV_PROGRAM_GUID");

            // call the function
            connection.execute(function);

            JCO.Table values = function.getExportParameterList().getTable("ET_POINTTYPES");
            
            int numValues = values.getNumRows();

            for (int i = 0; i < numValues; i++) {
                String key = "" + i;

                TableRow row = table.insertRow();

                row.setRowKey(new TechKey(key));

                row.getField("PointType").setValue(values.getString("POINT_CODE"));
                row.getField("Description")
                   .setValue(values.getString("POINT_NAME"));

                values.nextRow();
            }

            // write some useful logging information
            //if (log.isDebugEnabled()) {
            //    WrapperCrmIsa.logCall("CRM_ISA_SHOP_GETLIST", importParams, null,log);
            //    WrapperCrmIsa.logCall("CRM_ISA_SHOP_GETLIST", null, shops,log);
            //}
            return new JCoHelper.ReturnValue(null, ""); 
        }
        catch (JCO.Exception ex) {
            JCoHelper.splitException(ex);
        }

        return null;
    }
    
    
	/**
	 * Set the property isEnableLoyaltyProgram
	 *
	 * @param isEnableLoyaltyProgram
	 *
	 */
	public boolean isEnableLoyaltyProgram() {
		return isEnableLoyaltyProgram;
	}


	/**
	 * Set the property isEnableLoyaltyProgramd
	 *
	 * @param isEnableLoyaltyProgram
	 *
	 */
	public void setEnableLoyaltyProgram(boolean isEnableLoyaltyProgram) {
		this.isEnableLoyaltyProgram = isEnableLoyaltyProgram;
	}



	public String getLoyaltyProgramID() {
		return loyaltyProgramID;
	}



	public void setLoyaltyProgramID(String loyaltyProgramID) {
		this.loyaltyProgramID = loyaltyProgramID;
	}



	public String getPointCode() {
		return this.pointType;
	}



	public void setPointCode(String pointType) {
		this.pointType = pointType;
	}

}