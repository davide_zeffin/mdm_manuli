/*****************************************************************************
    Class:        ShopAdminBackend
    Copyright (c) 2001, SAPMarkets Europe GmbH, Germany, All rights reserved.
    Author:       Wolfgang Sattler
    Created:      February 2002
    Version:      1.0

    $Revision: #3 $
    $Date: 2001/07/24 $
*****************************************************************************/

package com.sap.isa.shopadmin.backend.boi;

import com.sap.isa.core.TechKey;
import com.sap.isa.core.eai.BackendException;
import com.sap.isa.core.util.table.ResultData;
import com.sap.isa.maintenanceobject.backend.boi.MaintenanceObjectBackend;
import com.sap.isa.user.backend.boi.UserBaseData;

/**
 *
 * With the interface ShopAdminBackend the ShopAdmin can communicate with
 * the backend.
 *
 */
public interface ShopAdminBackend extends MaintenanceObjectBackend {


    /**
     * Reads a list of shops and returns this list
     * using a tabluar data structure.
     *
     * @param scenario Scenario the shop list should be retrieved for
     * @return List of all available shops
     */
    public ResultData readShopList(String shopId, String scenario) throws BackendException;

    /**
     * This method check dependencies of the maintenance object within the backend
     *
     * @param maintenanceObject
     */
    public void checkDependencies(ShopData shopData)
            throws BackendException;


	/**
	 * Adjust properties depending from the scenario 
	 * 
	 * @param shopData shop
	 * @param scenario scenario
	 */
	public void adjustToScenario(ShopData shopData, String scenario) throws BackendException;


    /**
     * This method deletes the shop with the key <code>shopKey</code> within the backend
     *
     * @param shopKey <code>TechKey</code> of the shop
     */
    public void deleteShop(TechKey shopKey)
            throws BackendException;



    /**
     * This method read the values of the maintenance object from the backend
     *
     * @param maintenanceObject
     */
    public void readData(ShopData shopData)
            throws BackendException;

    /**
     * This method save the maintenance object in backend
     *
     * @param maintenanceObject
     */
    public void saveData(ShopData shopData)
            throws BackendException;

	/**
	 * Check if the given user is authorized to maintain shops. <br>
	 * 
	 * @param user user to check
	 * @return <code>true</code> if the user is authorized
	 * @throws BackendException
	 */
	public boolean isUserAuthorized (UserBaseData user)
			throws BackendException;	

}