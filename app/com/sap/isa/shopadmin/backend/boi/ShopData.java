/*****************************************************************************
    Inteface:     ShopData
    Copyright (c) 2001, SAPMarkets Europe GmbH, Germany, All rights reserved.
    Author:       Wolfgang Sattler
    Created:      February 2002
    Version:      1.0

    $Revision: #3 $
    $Date: 2001/07/24 $
*****************************************************************************/
package com.sap.isa.shopadmin.backend.boi;

import com.sap.isa.core.eai.BackendException;
import com.sap.isa.maintenanceobject.backend.boi.MaintenanceObjectData;


/**
 * The ShopData interface handles all kind of settings for the application.<p>
 *
 * @author Wolfgang Sattler
 * @version 1.0
 */
public interface ShopData extends MaintenanceObjectData {


    /**
     * Constant to idendify the B2B scenario
     */
    public final static String B2B = "B2B";


    /**
     * Constant to idendify the B2C scenario
     */
    public final static String B2C = "B2C";


    /**
     * Constant to idendify independence from scenario
     */
    public final static String B2X = "B2X";

}
