/*
 * Created on Dec 2, 2004
 *
 * To change the template for this generated file go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
package com.sap.isa.shopadmin.backend;

import com.sap.isa.maintenanceobject.backend.J2EEPersistencyDataHandler;


/**
 * @author I803067
 *
 * To change the template for this generated type comment go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
public class ShopDBDataHandler extends J2EEPersistencyDataHandler {
	
	
	/**
	 * Adjust the name of the structures. <br>
	 */
	protected void adjustConstants() {
		HEADER = "ShopData-HEADER";
		CHILDREN = "ShopData-CHILDREN";
		ID = "ShopID";
	}
	
	

}
