package com.sap.isa.persistence.objects.eventmodel;

import java.util.EventObject;

/**
 * Title:
 * Description:
 * Copyright:    Copyright (c) 2001
 * Company:
 * @author
 * @version 1.0
 */

public class GenericEvent extends EventObject  {

    private byte eventType;

    public GenericEvent(byte eventType, Object src) {
        super(src);
        this.eventType = eventType;
    }

    /**
    *@return the type of event
    */
    public byte getType() {
        return eventType;
    }
}