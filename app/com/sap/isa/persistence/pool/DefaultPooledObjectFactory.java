package com.sap.isa.persistence.pool;

import com.sap.isa.core.logging.IsaLocation;

/**
 * Title:        Personalization Core Framework
 * Description: Default Object Pool Factory uses reflection to create
 *              new instances
 *              Pooled Object must provide Default ctor
 *              Pooled Object must implement the PooledObject interface
 * Copyright:    Copyright (c) 2001
 * Company:      SAP Markets Inc.
 * @author
 * @version 1.0
 */

public class DefaultPooledObjectFactory extends PooledObjectFactory {

	protected static IsaLocation log = IsaLocation.
		  getInstance(DefaultPooledObjectFactory.class.getName());
    /**
     *
     * @throws IllegalArgumentException if type does not implement PooledObject
     */
    public DefaultPooledObjectFactory(Class type) {
        super(type);
        if(!PooledObject.class.isAssignableFrom(type))
            throw new IllegalArgumentException(type.getClass().getName() +
                                               " class does not implement " +
                                               PooledObject.class.getName());

        try
        {
            type.getConstructor(new Class[0]);
        } catch(NoSuchMethodException e)
        {
			log.debug(e.getMessage());
            throw new IllegalArgumentException(type.getClass().getName() +
                                " class doesn't have no-arg ctor!");
        }
    }


    /**
     * @param params any initialization specific params
     * @return a new initialized instance to be put in Pool
     */
    public Object create(Object params) {
        Object obj = null;
        try {
            obj = getType().newInstance(); // no args ctor
        }
        catch(Exception ex) {
        	log.debug(ex.getMessage());
            // log the exception
        }
        return obj;
    }

    /**
     * When the available Pooled Object is picked for Usage Pool, Object is initialized
     * to proper state. Default no initialization required
     *
     * @param obj the Pooled Object
     * @params params any initialization specific parameters
     */
    public Object initialize(Object obj, Object params) {
        return ((PooledObject)obj).initialize(params);
    }

    /**
     * When the Pooled Object is returned to Pool, Object is closed to
     * release any resources held by the Object
     * Releases the Respources. If the Object was wrapped it must be unwrapped
     * here
     *
     * @param obj the pooled object
     */
    public void close(Object obj) {
        ((PooledObject)obj).close();
    }

    /**
     * Object is no longer pooled. Permanently delete the Object
     *
     * @param obj the pooled object
     */
    public void destroy(Object obj) {
        ((PooledObject)obj).destroy();
    }
}