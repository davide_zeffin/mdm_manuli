package com.sap.isa.persistence.pool;

/**
 * Title:        Personalization Core Framework
 * Description:  An Object to be pooled can optionally implment this interface
 * Copyright:    Copyright (c) 2002 - All rights reserved
 * Company:      SAP Labs, PA
 * @author: P13n Core
 * @version 0.1
 * $Creation Date:10.09.2002$
 * $revision : 0.1$
 */

public interface PooledObject {
    Object initialize(Object params);

    Object close();

    void destroy();

    void addPoolEventListener(PoolEventListener listener);

    void removePoolEventListener(PoolEventListener listener);
}