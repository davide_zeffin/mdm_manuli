package com.sap.isa.persistence.pool;

import java.util.Iterator;
import java.util.LinkedList;

import com.sap.isa.core.logging.IsaLocation;

/**
 * Title:        Personalization Core Framework
 * Description:  Garbage Collections is responsible for running GC cycles on Object Pools.
 *               This is a singleton which works over all the Object pools.
 *
 *               Garbage Collection is primarily responsible for 2 things
 *               : Reclaim Resources which have exceeded activeTTL
 *               : Delete Inactive resources which have exceeded inactiveTTL
 *
 *               It's a daemon thread. This thread is public for administrative
 *               purposes
 * Copyright:    Copyright (c) 2002
 * Company:      SAP Labs, PA
 * @author: NPS (narinder.singh@sap.com)
 * @version 0.1
 */

public final class PoolGarbageCollector {

    private GCThread worker = null;
    private LinkedList pools = new LinkedList();
    private boolean running = false;
    private boolean paused = false;

	/** 
	  *  The IsaLocation of the current Action.
	  *  This will be instantiated during perform and is always present.
	  */
	 protected IsaLocation log = IsaLocation.getInstance(PoolGarbageCollector.class.getName());

    private PoolGarbageCollector() {
    }

    /** Singleton per JVM*/
    public static final PoolGarbageCollector COLLECTOR = new PoolGarbageCollector();

    synchronized void addObjectPool(ObjectPool pool) {
        if(pool.getAllowGC()) {
            pools.add(pool);
            notifyAll();
        }
    }

    synchronized void removeObjectPool(ObjectPool pool) {
        pools.remove(pool);
    }

    private synchronized void waitForPools() {
        while(pools.size() == 0) {
            try {
                wait();
            } catch(InterruptedException e) {
				log.debug(e.getMessage());
            }
        }
    }

    private synchronized long getDelay() {
        long next = Long.MAX_VALUE;
        long now = System.currentTimeMillis();
        long current;
        for(Iterator it = pools.iterator(); it.hasNext();) {
            ObjectPool pool = (ObjectPool)it.next();
            current = pool.getNextGCTimeMillis(now);
            if(current < next) next = current;
        }
        return next >= 0l ? next : 0l;
    }

    /**
     * runs the GC Cycle on Pools GC
     */
    private synchronized void runGCCycle() {
        for(Iterator it = pools.iterator(); it.hasNext();) {
            ObjectPool pool = (ObjectPool)it.next();
            if(pool.isTimeToGC()) {
                pool.runGCAndShrink();
            }
        }
    }

    /**
     *
     */
    public void start() {
        if(paused) { resume(); return; } // behaves like resume if paused
        if(running) return;
        if(worker == null) worker = new GCThread();
        worker.start();
        running = true;
    }

    public synchronized void stop() {
        running = false;
        paused = false;
        worker = null;
        notifyAll();
    }

    public void pause() {
        paused = true;
    }

    public synchronized void resume() {
        paused = false;
        notifyAll();
    }

    /** Worker thread */
    private class GCThread extends Thread {
        GCThread() {
            setDaemon(true);
            //setPriority();
        }

        public void run() {
            Object lock = PoolGarbageCollector.this;

            while(running) {
                while(paused) {
                    // exit, if stop is called after pause
                    if(!running) break;
                    synchronized(lock) {
                        try {
                            lock.wait();
                        }
                        catch(InterruptedException ex) {
							log.debug(ex.getMessage());
                            // continue;
                        }
                    }
                }

                // if stopped, exit
                if(!running) break;

                waitForPools();

                long delay = getDelay(); // approx delay in millis to run next cycle

                if(delay > 0) {
                    try {
                        this.sleep(delay); // go to bed
                    }
                    catch(InterruptedException ex) {
                    	log.debug(ex.getMessage());
                    }
                }

                runGCCycle(); // run the cycle
            }
        }
    }
}