package com.sap.isa.persistence.pool;

import java.util.ConcurrentModificationException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;

import com.sap.isa.core.logging.IsaLocation;

/**
 * Title:        Personalization Core Framework
 * Description:  Timestamp based Object Pool. This Object Pools monitors the Pooled objects
 *               using the Timestamp.
 *
 *               It can function both as hard and soft Object Pool based on allowOverflow property
 *               specified explicitly or in PoolProperties.
 *
 *               If overflow is allowed, Reference type can be configured to be kept in the Pool.
 *               Choices are:
 *               Strong
 *               Soft
 *               Weak (Default)
 *
 *               If Overflow is allowed, a PoolReference is available only if
 *               <code>(!ref.isInUse() && ref.getObject() != null) == true</code>
 *               since a ref may have been cleared by JVM GC
 *
 *               The Pool Reference is simply removed in case
 *               <code>(!ref.isInUse() && ref.getObject() == null) == true</code>
 *
 *               If Pool GC is enabled,
 *               each Pool GC Cycle does the following:
 *               Object which are in use and have exceeded the activeTTL(runGCCycle())
 *               Object which are not in use and idle time has exceeded inactiveTTL are destroyed (shrink())
 *
 *
 * Copyright:    Copyright (c) 2002
 * Company:      SAP Labs, PA
 * @author: narinder.singh@sap.com
 * @version 0.1
 */

public class ObjectPool implements PoolEventListener {

    private String name;

    // All the Pooled Objects are indexed here
    private HashMap pooledObjects = null;

    private int size = 0; // In Use Pool Size

    private PooledObjectFactory factory;
    private PoolProperties props;

    private long lastGC = System.currentTimeMillis(); // set to start time

    private boolean initialized = false;
    private boolean closed = false;

    private Object mutex = new Object();    // my own, my precious :)
    
	/** 
	  *  The IsaLocation of the current Action.
	  *  This will be instantiated during perform and is always present.
	  */
	 protected IsaLocation log = IsaLocation.getInstance(ObjectPool.class.getName());
    

    private static PoolGarbageCollector collector = PoolGarbageCollector.COLLECTOR;

    static {
        collector.start();
    }

    /**
     * Use this ctor if Pool is to be created wout specific PoolObjectFactory
     * This defaults to DefaultObjectPoolFactory
     */
    public ObjectPool(String name, Class type) {
        this.name = name;
        factory = new DefaultPooledObjectFactory(type);
    }

    public ObjectPool(String name, PooledObjectFactory factory) {
        this.name = name;
        this.factory = factory;
    }

    private void propertiesCheck() {
        if(props == null) props = new PoolProperties();
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        initCheck();
        usageCheck();
        this.name = name;
    }

    private void initCheck() {
        if(initialized) throw new IllegalStateException("Pool " + name + " is already initialized");
    }

    /**
     * Pool must be initialized and not closed
     */
    private void usageCheck() {
        if(closed) throw new IllegalStateException("Pool is closed");
        if(!initialized) throw new IllegalStateException("Pool is not initialized");
    }

    public int getMaxSize() {
        propertiesCheck();

        return props.maxPoolSize;
    }

    public void setMaxSize(int size) {
        initCheck();
        propertiesCheck();

        if(size <= 0) throw new IllegalArgumentException("Invalid Max size : " + size);
        if(size < props.minPoolSize) {
            props.minPoolSize = size;
            // Log here
        }
        props.maxPoolSize = size;
    }

    public int getMinSize() {
        propertiesCheck();

        return props.minPoolSize;
    }

    public void setMinSize(int size) {
        initCheck();
        propertiesCheck();

        if(size < 0) throw new IllegalArgumentException("Invalid Min size : " + size);
        if(size > props.maxPoolSize) {
            props.maxPoolSize = size;
            // Log Here
        }
        props.minPoolSize = size;
    }

    public boolean getAllowGC() {
        propertiesCheck();

        return props.allowGC;
    }

    public void setAllowGC(boolean allow) {
        initCheck();
        propertiesCheck();

        props.allowGC = allow;
    }

    public long getGCInterval() {
        propertiesCheck();

        return props.gcTimeInterval;
    }

    public void setGCInterval(long millis) {
        initCheck();
        propertiesCheck();

        if(millis <= 0) throw new IllegalArgumentException("Invalid GC Internval:" + millis);
        props.gcTimeInterval = millis;
    }

    public long getIdleTimeoutInterval() {
        propertiesCheck();

        return props.inactiveTTL;
    }

    public void setIdleTimeoutInterval(long millis) {
        initCheck();
        propertiesCheck();

        if(millis <= 0) throw new IllegalArgumentException("Invalid Idle Timeout Interval:" + millis);
        props.inactiveTTL = millis;
    }

    public long getMaxUsedInterval() {
        propertiesCheck();

        return props.activeTTL;
    }

    public void setMaxUsedInterval(long millis) {
        initCheck();
        propertiesCheck();

        if(millis <= 0) throw new IllegalArgumentException("Invalid Max Used Interval:" + millis);
        props.activeTTL = millis;
    }

    /**
     * between 0 and 1
     */
    public float getShrinkFactor() {
        propertiesCheck();

        return props.shrinkFactor;
    }

    /**
     * This provides the factor by which to shrink the Pool. This avoids a sudden shrinking
     * of Pool.
     * Factor expresses the % of Pool size
     *
     * 0 => implies shrink by 1
     * 1 => shrink fully
     *
     * @param factor must be between 0 and 1
     */
    public void setShrinkFactor(float factor) {
        initCheck();
        propertiesCheck();

        if(!(0 <= factor && factor <= 1)) {
            throw new IllegalArgumentException("Invalid shrink factor ( 0 <= factor <= 1): " + factor);
        }

        props.shrinkFactor = factor;
    }

    public boolean isOverFlowAllowed() {
        propertiesCheck();

        return props.allowOverFlow;
    }

    public void allowOverFlow(boolean allow) {
        initCheck();
        propertiesCheck();

        props.allowOverFlow = allow;
    }

    /**
     *
     */
    public void setOverFlowReferenceType(int type) {
        initCheck();
        propertiesCheck();

        if(!(type == PoolReference.SOFT_REFERENCE ||
             type == PoolReference.WEAK_REFERENCE ||
             type == PoolReference.STRONG_REFERENCE
             )) {
            throw new IllegalArgumentException("Invalid Reference Type: " + type);
        }

        props.overFlowRefType = type;
    }

    /**
     *
     */
    public int getOverFlowReferenceType() {
        propertiesCheck();

        return props.overFlowRefType;
    }

    public boolean getRemoveOnError() {
        propertiesCheck();

        return props.removeOnError;
    }

    public void setRemoveOnError(boolean remove) {
        initCheck();
        propertiesCheck();

        props.removeOnError = remove;
    }

    /**
     * All the properties must be set using explicit set methods b4 calling
     * initialize. Once the Object pool is initialized, it cannot be configured
     * again
     */
    public void initialize() {
        propertiesCheck();
        initialize(null);
    }

    /**
     * Copies all the properties from supropslied PoolProperties.
     * Once the Object pool is initialized, it cannot be configured again
     */
    public void initialize(PoolProperties props) {
        if(initialized) throw new IllegalStateException("Pool " + name + " is already initialized");

        if(props != null) this.props = (PoolProperties)props.clone(); // copy

        factory.poolStarted(this);
        // no GC if props.activeTTL == Long.MAX_VALUE
        if(this.props.allowGC && this.props.activeTTL < Long.MAX_VALUE)
            collector.addObjectPool(this);

        pooledObjects = new HashMap((this.props.minPoolSize + this.props.minPoolSize)/2);
        initialized = true;
        // log here
    }

    /**
     * Returns an available Object from Pool wout Wait.
     * Creates a new Object if all Pooled Objects are in Use and Pool has not
     * exceeded the
     * If pool
     */
    public Object getObjectNoWait() {
        return getObject(null,-1);
    }

    /**
     *
     */
    public Object getObjectNoWait(Object params) {
        return getObject(params,-1);
    }

    /**
     * @param mls millis to wait, 0 => indefinite wait
     */
    public Object getObject(long mls) {
        return getObject(null,mls);
    }

    /**
     * @param mls millis to wait, > 0 => some definite wait
     *                            0 => indefinite wait
     *                            < 0 => no wait
     */
    public Object getObject(Object params, long mls) {
        usageCheck();

        Object result = null;
        // non zero value indicates blocking
        boolean shouldBlock = mls >= 0;
        while(true)
        {
            Iterator it = pooledObjects.values().iterator();
            while(it.hasNext())
            {
                PoolReference ref = (PoolReference)it.next();
                if(null != ref && !ref.isInUse() && factory.validate(ref.getObject(), params))
                {
                   try
                   {
                      ref.setInUse(true); // block for use
                      Object obj = ref.getObject();
                      result = factory.initialize(obj);
                      if(result != obj) ref.setWrapper(result);
                      if(obj instanceof PooledObject)
                         ((PooledObject)obj).addPoolEventListener(this);
                      // log here
                      //if( log.isTraceEnabled() )
                      //   log.trace("Pool "+this+" gave out pooled object: "+result);
                      return result;
                   } catch(ConcurrentModificationException e) {
                   	log.debug(e.getMessage());
                      // That's OK, just go on and try another object
                   }
                   catch(RuntimeException e) {
                      // Some problem in initialize
                      try
                      {
                         ref.setInUse(false);
                      } catch(ConcurrentModificationException e2)
                      {
						log.debug(e2.getMessage());
                      }
                      // If this is a problem with the current state,
                      // we don't want to block, as it will continue to
                      // be a problem as far as we know
                      throw e;
                   }
                }
            }

            // Nothing available in Pool
            result = newPooledObject(params,true);
            if(result != null) return result;

            if(shouldBlock) {
                //if( log.isTraceEnabled() )
                //   log.trace("Pool "+this+" waiting for a free object");
                synchronized(this) {
                   try {
                        wait(mls);
                        shouldBlock = false; //don't wait again
                   } catch(InterruptedException e) { 
					log.debug(e.getMessage());
                   	shouldBlock = false; 
                   }
                }
            }
            else {
                break; //exit
            }
        }

        // Log here
        //if(log.isTraceEnabled() )
        // log.trace("Pool "+this+" couldn't find an object to return!");
        return result;
    }

    /**
     * If the release referenced is found in OverFlow Map, and
     * Pool size if Max Pool Size, it is immediately destroyed.
     * This guarantees that all the references in OverFlow map are in Use.
     *
     * If the Pool size is less than Max then Reference is moved from OverFlow
     * to Pooled Object and is not lost
     *
     * This method allows In Use Objects to be returned to Pool to be destroyed properly
     * even after it has been closed
     */
    public void returnObject(Object obj) {
        if(obj == null) throw new IllegalArgumentException("obj is null");

        usageCheck();

        synchronized(obj) {

            PoolReference ref = (PoolReference)pooledObjects.get(obj);

            // check if the object is recognized by factory
            Object pooled = null;
            try {
                pooled = factory.unwrap(obj);
            }
            catch(Exception ex) {
				log.debug(ex.getMessage());
                return;
            }
            if(pooled == null) return;

            // object does not belong to this pool
            if(ref == null) {
                // log here
                throw new IllegalArgumentException("Object " + obj + " is not part of pool " + name);
            }

            if(!ref.isInUse()) return; // reclaimed by GC

            if(obj instanceof PooledObject) {
                ((PooledObject)obj).removePoolEventListener(this);
            }

            try {
                // close
                factory.close(obj);
            }
            catch(Exception ex) {
				log.debug(ex.getMessage());
                //log
            }

            // available for reuse
            ref.setInUse(false);
            // no usage context
            ref.setWrapper(null);
        }

        // log here, object returned to pool

        synchronized(this) {
            notify(); // notify the availability in the pool
        }
    }

    /**
     * Client can update the Used Time to renew the lease of Used Object with
     * Object Pool, otherwise if the Object usage time exceeds PoolProperties.activeTTL
     * it will be returned to Pool for reuse
     */
    public void updateUsedTime(Object obj) {
        if(obj == null) throw new IllegalArgumentException("obj is null");

        usageCheck();

        Object pooled = null;
        try
        {
            pooled = factory.unwrap(obj);
        }
        catch(Exception e) {
            throw new IllegalArgumentException("Pool "+ getName() + " does not recognize object for used time: "+obj);
        }

        PoolReference ref = (PoolReference) pooledObjects.get(obj);
        if(ref == null) {
            throw new IllegalArgumentException("Object " + obj + " does not belong to pool " + getName());
        }

        if(!ref.isInUse()) {
            throw new IllegalStateException("Cannot update used time, Object " + obj + " is not in use in pool " + getName());
        }

        ref.updateLastUsedTime();
    }

    /**
     *
     */
    public void removeObject(Object obj) {
        if(obj == null) throw new IllegalArgumentException("obj is null");

        usageCheck();

        synchronized(obj) {

            // check if the object is recognized by factory
            Object pooled = null;
            try {
                pooled = factory.unwrap(obj);
            }
            catch(Exception ex) {
				log.debug(ex.getMessage());
                return;
            }
            if(pooled == null) return;

            PoolReference ref = (PoolReference)pooledObjects.get(pooled);

            // object does not belong to this pool
            if(ref == null) {
                // log here
                throw new IllegalArgumentException("Object " + obj + " is not part of pool " + name);
            }

            if(ref.isInUse()) {
                // log warning  In use Object being removed from pool
            }

            removeInternal(pooled);

            ref.release(); // release the reference
        }

        // log here , object removed from pool
    }

    // Just removes from the pool
    private void removeInternal(Object obj) {
        try {
            factory.destroy(obj);
        }
        catch(Exception ex) {
			log.debug(ex.getMessage());
            // log here
        }

        if(obj instanceof PooledObject) {
            ((PooledObject)obj).removePoolEventListener(this);
        }

        synchronized(mutex) {
            pooledObjects.remove(obj);
        }
    }

    /**
     * @return the Current Pool Size (Ready + Active)
     */
    public int getSize() {
        int size = 0;
        synchronized(mutex) {
            size = pooledObjects.size();
        }
        return size;
    }

    /**
     * For Adminstrative Porposes
     * @return the Ready and Available Pooled Object size
     */
    public int getReadySize() {
        int count = 0;
        HashMap copy = null;
        synchronized(mutex) {
            copy = (HashMap)pooledObjects.clone();
        }
        Iterator it = copy.values().iterator();
        while(it.hasNext()) {
            PoolReference ref = (PoolReference)it.next();
            if(!ref.isInUse()) count++;
        }
        return count;
    }

    /**
     * Multiple calls to close are ignored
     * Object pool is closed
     * Object pool is no longer usable after this
     */
    public void close() {
        if(closed) return;
        // to stop pool usage by other threads
        closed = true;
        // now, close
        if(initialized) {
            if(props.allowGC) collector.removeObjectPool(this);
            factory.poolClosing(this);
            HashMap localObjects = pooledObjects;
            pooledObjects = null;

            // close all objects
            for(Iterator it = localObjects.values().iterator(); it.hasNext();)
            {
             PoolReference ref = (PoolReference)it.next();
             if (null != ref)
             {
                if(ref.isInUse()) {
                   // log here that a Used Object in Pool was closed
                }
                factory.close(ref.getObject());
                factory.destroy(ref.getObject());
                ref.release();
             }
            }

            localObjects.clear();
            factory = null;
            name = null;
        }
    }

    /**
     * @return Object newPooledObject
     */
    Object newPooledObject(Object params, boolean immediateUse) {
        Object obj = null;
        // avoid to concurrent calls to create
        synchronized(mutex) {
            if(getSize() < props.maxPoolSize || props.allowOverFlow) {
                obj = factory.create(params);
            }
            /*
            // overflow allowed
            else if(pp.allowOverFlow) {
                obj = factory.create(params);
                PoolReference ref;
                switch(props.overFlowRefType) {
                    case PoolReference.SOFT_REFERENCE :
                        ref = new SoftPoolReference(obj,immediateUse); break;
                    case PoolReference.STRONG_REFERENCE :
                        ref = new StrongPoolReference(obj,immediateUse); break;
                    default : // default
                        ref = new WeakPoolReference(obj,immediateUse);
                }
                pooledObjects.put(obj,ref);
            }*/
            else {
                // log pool has exceeded its limit and overflow is not allowed
            }
            if(obj != null) {
                PoolReference ref = null;
                ref = new StrongPoolReference(obj,immediateUse);
                pooledObjects.put(obj,ref);
                Object wrapper = null;
                if(immediateUse) {
                    try {
                        wrapper = factory.initialize(obj);
                    }
                    catch(Exception ex) {
						log.debug(ex.getMessage());
                        // log here
                    }
                }
                if(wrapper != null && obj != wrapper) {
                    ref.setWrapper(obj);
                }

                if(wrapper instanceof PooledObject) {
                    ((PooledObject)wrapper).addPoolEventListener(this);
                }
                obj = wrapper;
            }
        }
        return obj;
    }

    /**
     * Relative to specified time now
     * -ve if the Time has passed
     */
    long getNextGCTimeMillis(long now) {
        if(props.allowGC) {
            return lastGC + props.gcTimeInterval - now;
        }
        else {
            return Long.MAX_VALUE;
        }
    }

   // Allow GC if we're within 10% of the desired interval
    boolean isTimeToGC()
    {
        return System.currentTimeMillis() >=
        lastGC + Math.round((float)props.gcTimeInterval * 0.9f);
    }

    /**
     * Runs the GC Cycle on the invoked thread and cleans up
     * GC Cycle should be well spread over time for good performance
     *
     */
    void runGCAndShrink() {
        // reclaim used Resources
        if(props.allowGC) {

            LinkedList objectsCopy = null;
            synchronized(mutex) {
                // take a snap shot
                objectsCopy = new LinkedList(pooledObjects.values());
            }

            Iterator it = objectsCopy.iterator();
            while(it.hasNext()) {
                PoolReference ref = (PoolReference)it.next();

                // Usage exceeded activeTTL, reclaim
                if(ref.isInUse() && ref.getSinceLastUsedMillis() >= props.activeTTL) {
                    Object obj = ref.getObject();
                    if(obj != null)
                        this.returnObject(obj);
                    else {
                        // next lookup from pool will clean this reference
                        ref.setInUse(false);
                    }
                }
            }
        }
        // Shrink the Pool
        shrink();
        lastGC = System.currentTimeMillis(); // update the Last GC time
    }

    /**
     * Public: so that even the Shrink can be triggered externally if the
     * GC is not running.
     * Releases resources which have not been used too long
     */
    public void shrink() {
        LinkedList objectsCopy = null;
        synchronized(mutex) {
            // take a snap shot
            objectsCopy = new LinkedList(pooledObjects.values());
        }

        LinkedList oldies = new LinkedList();
        Iterator it = objectsCopy.iterator();
        while(it.hasNext()) {
            PoolReference ref = (PoolReference)it.next();
            if(!ref.isInUse() && ref.getSinceLastUsedMillis() >= props.inactiveTTL) {
                oldies.add(ref);
            }
        }

        // shrink the pool no more than shrink factor
        int max = Math.round(getSize()*props.shrinkFactor);
        max = max == 0 ? 1 : max;

        if(oldies.size() > 0) {
            Iterator listIt = oldies.iterator();
            int count = 0;
            while(listIt.hasNext()) {
                // no more than max or till min pool size is hit
                if(++count > max || getSize() <= props.minPoolSize) {
                    break;
                }

                PoolReference ref = (PoolReference)listIt.next();

                try {
                    // so nobody else can steal
                    ref.setInUse(true);
                    // obtain a hard reference
                    Object obj = ref.getObject();
                    // Remove from Pool
                    if(obj != null) {
                        try {
                            factory.destroy(obj);
                        }
                        catch(Exception ex) {
							log.debug(ex.getMessage());
                            // log here
                        }
                        this.removeInternal(obj);
                        ref.release(); // release
                    }
                    // it has been garbage Collected by JVM (remove the record from Pool)
                    else {
                        ref.setInUse(false);
                    }

                    // maintain min pool size
                    if(getSize() < props.minPoolSize) {
                        newPooledObject(null,false);
                    }
                }
                catch(ConcurrentModificationException ignore) {
					log.debug(ignore.getMessage());
                }
            }
        }

    }


    /////////////////////////////
    // Pool event Listener impl
    /**
     * To update the Timestamp
     */
    public void onUse(PoolEvent event) {
        if(event.getType() == PoolEvent.EVENT_USED) {
            updateUsedTime(event.getSource());
        }
    }

    /**
     * On Error, Policy needs to be defined
     */
    public void onError(PoolEvent event) {
        if(event.getType() == PoolEvent.EVENT_ERROR) {
            // remove from Reference Pool
            if(props.removeOnError) {
                Object src = event.getSource();
                this.removeObject(src);
            }
        }
    }

    /**
     * To return to pool
     */
    public void onClose(PoolEvent event) {
        if(event.getType() == PoolEvent.EVENT_CLOSE) {
            Object src = event.getSource();
            this.returnObject(src);
        }
    }
}