package com.sap.isa.persistence.pool;

/**
 * Title:        Personalization Core Framework
 * Description:
 * Copyright:    Copyright (c) 2002
 * Company:      SAP Labs, PA
 * @author: NPS
 * @version 0.1
 */

class StrongPoolReference extends PoolReference {

    private Object obj; // actual object reference

    StrongPoolReference(Object obj) {
        this(obj,true);
    }

    StrongPoolReference(Object obj, boolean inUse) {
        super(inUse);
        this.obj = obj;
    }

    /**
     *
     * @return Object the Object Reference
     */
    Object getObject() {
        return obj;
    }

    /**
     *
     */
    void release() {
        super.release();
        obj = null;
    }
}