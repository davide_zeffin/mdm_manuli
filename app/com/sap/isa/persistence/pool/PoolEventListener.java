package com.sap.isa.persistence.pool;

/**
 * Title:        Personalization Core Framework
 * Description:
 * Copyright:    Copyright (c) 2001
 * Company:      SAP Markets Inc.
 * @author: P13n Core
 * @version 0.1
 */

public interface PoolEventListener {

    /**
     * To update the Timestamp
     */
    public void onUse(PoolEvent event);

    /**
     * On Error, Policy needs to be defined
     */
    public void onError(PoolEvent event);

    /**
     * To return to pool
     */
    public void onClose(PoolEvent event);
}