package com.sap.isa.persistence.pool;

import com.sap.isa.core.logging.IsaLocation;

/**
 * Title:        Personalization Core Framework
 * Description:  Simple Lock Behavior. There is Thread affinity in Lock.
 *               Same Thread can acquire the Lock multiple times wout blocking
 *               Unlock has no Thread affinity.
 *               Any Thread can unlock the Resource
 *               Normally, the Lock is kept as a private resource for
 *               thread safe access akin to Mutex
 * Copyright:    Copyright (c) 2002
 * Company:      SAP Labs, PA
 * @author:
 * @version 0.1
 */

public class Lock
{
    private boolean m_isLocked;
    private Thread who;
	protected static IsaLocation log = IsaLocation.
		   getInstance(Lock.class.getName());

    public final void lock()
        throws InterruptedException {
        lockInternal(-1);
    }

    /**
     * 0 => indefinite wait
     * @return boolean true if Lock was acquired within specified mls
     */
    public final boolean lock(long mls) throws InterruptedException {
        return lockInternal(mls);
    }

    /**
     * @return true if Lock was granted
     */
    public final boolean tryLock() {
        boolean ret = false;
        try {
            ret = lockInternal(0);
        }
        catch(InterruptedException ignore) {
        	log.debug(ignore.getMessage());
        }
        return ret;
    }

    /**
     * Locks. Same Thread can lock multiple times
     *
     * @throw InterrruptedException breaks out of wait, if interrrupted
     */
    private boolean lockInternal(long mls)
        throws InterruptedException
    {
        Thread me = Thread.currentThread();
        synchronized( this )
        {
            while( m_isLocked ) {
                // its me who locked it, grant lock
                if(m_isLocked && who == me) return true; // Locked by same object
                if(mls >=0) wait(mls);
                else return false;
            }
            m_isLocked = true;
            who = Thread.currentThread();
        }
        return true;
    }

    /**
     * @return the Thread who locked
     *          null if not locked
     */
    public final Thread whoLocked() {
        return who;
    }

    /**
     * Unlocks by any Thread.
     */
    public final void unlock()
    {
        synchronized( this )
        {
            m_isLocked = false;
            who = null;
            notify();
        }
    }
}
