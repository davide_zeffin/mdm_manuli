package com.sap.isa.persistence.pool;

/**
 * Title:        Personalization Core Framework
 * Description: Each Pooled Object provides a ObjectPoolFactory to
 *              Create New
 *              Initialize
 *              Close
 *              Release
 *              Objects from the Pool
 * Copyright:    Copyright (c) 2001
 * Company:      SAP Markets Inc.
 * @author
 * @version 1.0
 */

public abstract class PooledObjectFactory {

    private Class type;

    public PooledObjectFactory(Class type) {
        this.type = type;
    }

    /**
     * @return Class the type of Objects pooled
     */
    public Class getType() {
        return type;
    }

    /**
     * @param params any initialization specific params
     * @return a new initialized instance to be put in Pool
     */
    public abstract Object create(Object params);

    /**
     * This is called if client requires an object created with specific state.
     * By default, all object are same.
     *
     *
     * @return boolean
     */
    public boolean validate(Object obj, Object state) {
        return true;
    }

    /**
     * When the available Pooled Object is picked for Usage Pool, Object is initialized
     * to proper state. Default no initialization required
     *
     * If the object to be returned outside if diff from the Pooled Object, it must
     * be wrapped here
     *
     * @param obj the Pooled Object
     * @params params any initialization specific parameters
     *
     * @return the Actual object or the wrapped object
     */
    public Object initialize(Object obj) {
        return obj;
    }

    /**
     * PoolFactory may maintain the Binding between Client Context and Pooled resource
     * so that multiple calls from the same context are provided with same Pooled resource
     *
     * Default null i.e. no specific binding, each calls returns a new Pooled Object
     * @return the Wrapped Pooled Object to return to client
     */
    public Object getRequestBinding() {
        return null;
    }

    /**
     * Informational Purposes only
     * If a binding is maintained between Client Context. This can be used to extract
     * some useful logging information
     *
     * Default null, no context information
     */
    public Object getRequestContext() {
        return null;
    }

    /**
     * If the Object pooled was wrapped before being present to client
     * this method is called.
     * Default returns the same as Actual object i.e. no wrapping was done.
     * @param wrapper wrapper which was given to client
     * @return the Pooled Object
     */
    public Object unwrap(Object wrapper) {
        return wrapper;
    }

    /**
     * When the Pooled Object is returned to Pool, Object is closed to
     * release any resources held by the Object
     * Releases the Resources.
     * If the Object was wrapped it must be unwrapped here to original object
     *
     * @param obj the pooled object
     */
    public abstract void close(Object obj);

    /**
     * Object is no longer pooled. Permanently destroy the Object
     *
     * @param obj the pooled object
     */
    public abstract void destroy(Object obj);


    /**
     * Pool life cycle method. This is called once before Object pool access
     * any method on Pool Factory. Pool Factory can initialize itself in this
     * method
     * Default is ignored
     */
    public void poolStarted(ObjectPool pool) {
        if(pool == null) throw new IllegalArgumentException("pool is null");
    }

    /**
     * Pool life cycle method. This is called after the Object Pool is closed.
     * Pool Factory can perform clean up in this method
     *
     * Default is ignored
     */
    public void poolClosing(ObjectPool pool) {
        if(pool == null) throw new IllegalArgumentException("pool is null");
    }
}