package com.sap.isa.persistence.pool;

import java.util.ConcurrentModificationException;

/**
 * Title:        Personalization Core Framework
 * Description:
 * Copyright:    Copyright (c) 2001
 * Company:      SAP Labs, PA
 * @author: NPS(narinder.singh@sap.com)
 * @version 1.0
 */

public abstract class PoolReference {

    public static final int STRONG_REFERENCE =  0x1;
    public static final int SOFT_REFERENCE =    0x2;
    public static final int WEAK_REFERENCE =    0x3;

    private long lastUsedTime; // to find inactivity peroid
    private long createTime;   // to find age
    private Object wrapper;    // Set to wrapper, if any
    private boolean isInUse = false;
    // only for Debug mode
    private int usageCount;    // number of times reused

    /**
     * Default set to used
     */
    PoolReference() {
        this(true);
    }

    /**
     *
     */
    PoolReference(boolean inUse) {
        createTime = lastUsedTime = System.currentTimeMillis();
        isInUse = inUse;
    }

    /**
     *
     * @return Object the Object Reference
     */
    abstract Object getObject();

    /**
     *
     */
    boolean isInUse() {
        return isInUse;
    }

    /**
     *
     */
    synchronized void setInUse(boolean val) {
        if(val && isInUse) {
            throw new ConcurrentModificationException("Object is already in Use");
        }
        if(isInUse) updateLastUsedTime();
        isInUse = val;
    }

    long getCreateMillis() {
        return createTime;
    }

    /**
     *
     */
    void updateLastUsedTime() {
        lastUsedTime = System.currentTimeMillis();
    }

    /**
     * If In Use, gives the time mls Object Reference has been in Use by Client
     * If Not In Use, it gives the Idle time for Object Reference
     * @return millis since last used
     */
    long getSinceLastUsedMillis() {
        return System.currentTimeMillis() - lastUsedTime;
    }

    /**
     * Indicator of the User/Usage Context of the Object
     */
    void setWrapper(Object wrapper) {
        this.wrapper = wrapper;
    }

    /**
     *
     */
    Object getWrapper() {
        return wrapper;
    }

    /**
     * Reference is no longer pooled
     */
    void release() {
        createTime = lastUsedTime = Long.MAX_VALUE; // close state
        wrapper = null;
        isInUse = true; // permanently set to used
    }
}