package com.sap.isa.persistence.pool;

import java.lang.ref.SoftReference;

/**
 * Title:        Personalization Core Framework
 * Description:
 * Copyright:    Copyright (c) 2002
 * Company:      SAP Labs Inc
 * @author: NPS
 * @version 0.1
 */

class SoftPoolReference extends PoolReference {

    private SoftReference ref; // actual object soft reference

    /**
     *
     */
    SoftPoolReference(Object obj) {
        this(obj,true);
    }

    SoftPoolReference(Object obj, boolean inUse) {
        super(inUse);
        this.ref = new SoftReference(obj);
    }

    /**
     * @return the contained Reference
     *         null, if the reference is garbage collected
     */
    Object getObject() {
        return ref.get();
    }

    /**
     *
     */
    void release() {
        ref.clear();
        ref = null; // clear the reference
        super.release();
    }
}