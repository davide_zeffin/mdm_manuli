package com.sap.isa.persistence.pool;

import com.sap.isa.core.logging.IsaLocation;

/**
 * Title:        Personalization Core Framework
 * Description:  This class provides the Configuring Properties for the Object Pool
 *              One of the ways to configure a Pool is to supply this Properties instance
 *              upon initialization
 *              Also, it provides reasonable defaults to Pool Properties
 * Copyright:    Copyright (c) 2001
 * Company:      SAP Markets Inc.
 * @author
 * @version 1.0
 */

public class PoolProperties implements Cloneable, java.io.Serializable {

    public int minPoolSize = 0;     // Min size of Pool
    public int maxPoolSize = 20;    // Max size of Pool

    public float shrinkFactor = 1;  // shrink not more than 30% of Pool Size

    public boolean removeOnError = true; // Remove Pooled Object if an Error occurs

    public boolean allowOverFlow = true; // Whether allow Pool to exceed Max size
    public int overFlowRefType = PoolReference.WEAK_REFERENCE;

    public long inactiveTTL = 8*60*60*1000;      // max time a resource can remain unused
    public long activeTTL = 1*60*60*1000;      // max time a resource can remain used by a User

    public boolean allowGC = false;     // Whether to run GC or not
    public long gcTimeInterval = Long.MAX_VALUE; // GC Thread cycle time

	protected static IsaLocation log = IsaLocation.
		   getInstance(PoolProperties.class.getName());
		   
    public PoolProperties() {
    }

    public Object clone() {
        Object clone = null;
        try {
            clone = super.clone();
        }
        catch(CloneNotSupportedException ignore) {
        	log.debug(ignore.getMessage());
        }
        return clone;
    }
}