package com.sap.isa.persistence.pool;

import com.sap.isa.persistence.objects.eventmodel.GenericEvent;
/**
 * Title:        Personalization Core Framework
 * Description:
 * Copyright:    Copyright (c) 2001
 * Company:      SAP Markets Inc.
 * @author
 * @version 1.0
 */

public class PoolEvent extends GenericEvent {

    public static final byte EVENT_ERROR    = 100;
    public static final byte EVENT_USED     = 101;
    public static final byte EVENT_CLOSE    = 102;

    public PoolEvent(byte type, Object src) {
        super(type,src);
    }
}