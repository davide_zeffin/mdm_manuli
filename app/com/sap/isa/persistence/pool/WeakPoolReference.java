package com.sap.isa.persistence.pool;

import java.lang.ref.WeakReference;

/**
 * Title:        Personalization Core Framework
 * Description: Weak Pool Reference allows cases where Reference is marked in use but
 *              getObject returns null e.g. if a client crashed wout returning the
 *              reference to Pool and JVM GC cycle cleared the underlying reference
 * Company:      SAP Labs, PA
 * @author: NPS
 * @version 0.1
 */

class WeakPoolReference extends PoolReference {

    private WeakReference ref; // actual object weak reference

    WeakPoolReference(Object obj) {
        this(obj,true);
    }

    WeakPoolReference(Object obj, boolean inUse) {
        super(inUse);
        this.ref = new WeakReference(obj);
    }

    /**
     * @return null, if the reference is grabage collected
     */
    Object getObject() {
        return ref.get();
    }

    /**
     *
     */
    void release() {
        ref.clear();
        ref = null; // clear the reference
        super.release();
    }
}