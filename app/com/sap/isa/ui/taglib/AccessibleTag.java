package com.sap.isa.ui.taglib;

import javax.servlet.jsp.JspException;

import com.sap.isa.auction.util.AccessibilityUtil;
import com.sapportals.htmlb.taglib.AbstractComponentTag;

/**
 * Title:			Internet Sales Selling Via eBay
 * Description:
 * @Copyright: 		Copyright (c) 2004
 * Company:			SAPLabs LLC
 * Created on:		Sep 15, 2004
 */
public class AccessibleTag extends AbstractComponentTag {

	
    /* (non-Javadoc)
     * @see javax.servlet.jsp.tagext.Tag#doStartTag()
     */
    public int doStartTag() throws JspException {
    	int returnVal = SKIP_BODY;

        if (AccessibilityUtil.isAccessibleContext(pageContext)){
            returnVal = EVAL_BODY_INCLUDE;
        }
		return returnVal;
    }
}
