/*
 * Created on Oct 20, 2004
 *
 * To change the template for this generated file go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
package com.sap.isa.ui.taglib;

import com.sap.isa.core.logging.IsaLocation;
import com.sapportals.htmlb.taglib.AbstractComponentTag;

/**
 * @author I803067
 *
 * To change the template for this generated type comment go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
public class ExtButtonBarTag extends AbstractComponentTag {

	private String title;
	private String id;
	private boolean underline = true;
	private boolean backgroundRequired = true;
	
	/** 
	  *  The IsaLocation of the current Action.
	  *  This will be instantiated during perform and is always present.
	  */
	 protected static IsaLocation log = IsaLocation.getInstance(ExtButtonBarTag.class.getName());
	
    public ExtButtonBarTag() {
    }

	public int doStartTag() throws javax.servlet.jsp.JspException {

		try  {
			pageContext.getOut().write(headerContent());
		}  catch(Exception exc)  {
			log.debug("Error ", exc);
		}
		return EVAL_BODY_INCLUDE;
	}

	public int doEndTag() throws javax.servlet.jsp.JspException {
		try  {
			pageContext.getOut().write(footerContent());
		}  catch(Exception exc)  {
			log.debug("Error ", exc);
		}
		return EVAL_PAGE;
	}

	private String headerContent()  {
		StringBuffer buff = new StringBuffer();
		if(title != null && !title.equals(""))
			buff.append(writeTitle());
		else  {
			buff.append("<table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"100%\">");
		}
		if(backgroundRequired)
			buff.append("<tr><td colSpan=\"2\"><div class=\"sapTlbLvl2\"><table vspace=10 class=\"sapextToolbar\" class1=\"sapTlb\" width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">");
		else
		buff.append("<tr><td colSpan=\"2\"><div class=\"sapTlbLvl2NoBackground\"><table vspace=10 class=\"sapextToolbarNoBackground\" class1=\"sapTlb\" width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">");
		buff.append("<tr>");
		return buff.toString();
	}
	
	private String writeTitle()  {
		StringBuffer buffer = new StringBuffer();
		buffer.append("<table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"100%\">");
		buffer.append("<tr>");
		buffer.append("<td class=\"sapAppBrandTopLvl2\"></td>");
		buffer.append("<td class=\"sapAppTitle\" title=\"\"><span id=\"");
		buffer.append(id);
		buffer.append("\" title=\"");
		buffer.append(getTitle());
		buffer.append("\">");
		buffer.append(this.getTitle());
		buffer.append("</span>");
		buffer.append("</td>");
		buffer.append("</tr>");
		if(underline)  {
			buffer.append("<tr><td class=\"sapAppBrandLeftLvl1\"></td>");
			buffer.append("<td class=\"sapAppBrandCenterLvl1\" width=\"100%\">&nbsp;</td>");
			buffer.append("</tr>");
		}
		return buffer.toString();
	}

	private String footerContent()  {
		StringBuffer buff = new StringBuffer();
		buff.append("</tr></table></div></td></tr></table>");
		return buff.toString();
	}
	/**
	 * @return
	 */
	public String getId() {
		return id;
	}

	/**
	 * @return
	 */
	public String getTitle() {
		return title;
	}

	/**
	 * @param string
	 */
	public void setId(String string) {
		id = string;
	}

	/**
	 * @param string
	 */
	public void setTitle(String string) {
		title = string;
	}

	/**
	 * @return
	 */
	public boolean isUnderline() {
		return underline;
	}

	/**
	 * @param b
	 */
	public void setUnderline(boolean b) {
		underline = b;
	}

	/**
	 * @return
	 */
	public boolean isBackgroundRequired() {
		return backgroundRequired;
	}

	/**
	 * @param b
	 */
	public void setBackgroundRequired(boolean b) {
		backgroundRequired = b;
	}

}