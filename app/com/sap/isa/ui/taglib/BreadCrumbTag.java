package com.sap.isa.ui.taglib;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspTagException;

import com.sap.isa.core.logging.IsaLocation;
import com.sapportals.htmlb.BreadCrumb;
import com.sapportals.htmlb.IListModel;
import com.sapportals.htmlb.enum.BreadCrumbBehavior;
import com.sapportals.htmlb.enum.BreadCrumbSize;
import com.sapportals.htmlb.rendering.IPageContext;
import com.sapportals.htmlb.taglib.AbstractComponentTag;
import com.sapportals.htmlb.taglib.RendererContextTag;

/**
 * Title:
 * Description:
 * Copyright:    Copyright (c) 2002
 * Company:
 * @author
 * @version 1.0
 */

public class BreadCrumbTag extends AbstractComponentTag {

  private BreadCrumb crumb;
  private IPageContext myContext;

  /** 
	*  The IsaLocation of the current Action.
	*  This will be instantiated during perform and is always present.
	*/
   protected IsaLocation log = IsaLocation.getInstance(BreadCrumbTag.class.getName());

  public BreadCrumbTag() {
  }

  public void setId(String id)  {
      try  {
          myContext = RendererContextTag.getTagRendererContext(this, pageContext);
      }  catch(JspTagException jsExc)  {
           log.debug("Error in setId() ", jsExc);
      }
      if(myContext != null)  {
          crumb = (BreadCrumb)myContext.getAttribute(id);
      }
      if(null == crumb)  {
          crumb = new BreadCrumb(id);
      }
      crumb.setId(id);
  }

    public void setModel (String modelDescriptor) {
      Object modelObj = null;
      if(myContext != null)  {
          modelObj = (IListModel)myContext.getAttribute(id);
      }
      if(null == modelObj)
          modelObj = getModel ( "breadCrumb", id);
      if ( ! ( modelObj instanceof IListModel ) ) {
                throw new IllegalArgumentException("Tag breadCrumb attribute model"
                                        + ": Cannot access bean property " + modelDescriptor + " in page context");
      }
      crumb.setModel( (IListModel) modelObj );
    }

    public void setTooltip (String tooltip) {
        crumb.setTooltip(tooltip);
    }

    public void setOnClick(String onClick) {
        crumb.setOnClick(onClick);
    }

    public void setNameOfKeyColumn(String nameOfKeyColumn) {
        crumb.setNameOfKeyColumn(nameOfKeyColumn);
    }

    public void setBehavior(String behavior) {
        crumb.setBehavior( (BreadCrumbBehavior) convertToEnum("breadCrumb", "behavior",
								behavior, "com.sapportals.htmlb.enum.BreadCrumbBehavior", this) );
    }

    public void setSize(String size) {
        crumb.setSize( (BreadCrumbSize) convertToEnum("breadCrumb", "size", size,
								    "com.sapportals.htmlb.enum.BreadCrumbSize", this) );
    }

    public BreadCrumb getBreadCrumb() {
        return this.crumb;
    }

    public int doStartTag () throws JspException {

      if ( (crumb.getId() != null) && (crumb.getId().length() > 0) ) {
                  pageContext.setAttribute(crumb.getId(), crumb);
        }

        return super.doStartTag(); // TagSupport: return EVAL_BODY_TAG;
    }

    public int doEndTag () throws JspException {

        myContext = RendererContextTag.getTagRendererContext(this, pageContext);

        if ( (crumb.getModel() == null) && (crumb.getKeys().hasNext() == false) ) {
            new JspException("The breadCrumb " + crumb.getId()
                                                    + " does have neither a model nor nested breadCrumbItems!");
        }

        myContext.render(crumb);

        return super.doEndTag(); // BodyTagSupport: return EVAL_PAGE;
    }
}