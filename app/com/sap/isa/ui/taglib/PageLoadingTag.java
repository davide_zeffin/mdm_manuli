package com.sap.isa.ui.taglib;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.jsp.JspTagException;
import javax.servlet.jsp.PageContext;

import com.sap.isa.auction.actionforms.BaseFormConstants;
import com.sap.isa.auction.util.AccessibilityUtil;
import com.sap.isa.auction.util.FormUtility;
import com.sap.isa.core.RequestProcessor;
import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.core.taglib.ContentTypeTag;
import com.sap.isa.core.util.HttpServletRequestFacade;
import com.sap.isa.core.util.WebUtil;
import com.sap.isa.ui.UIConstants;
import com.sap.isa.ui.controllers.ActionForm;
import com.sap.isa.ui.uiclass.BaseUI;
import com.sapportals.htmlb.rendering.IPageContext;
import com.sapportals.htmlb.taglib.AbstractComponentTag;
import com.sapportals.htmlb.taglib.RendererContextTag;
import com.sapportals.htmlb.util.HtmlbRuntimeException;

/**
 * Title:
 * Description:
 * Copyright:    Copyright (c) 2002
 * Company:
 * @author
 * @version 1.0
 */

public class PageLoadingTag extends AbstractComponentTag  {

	/** 
	  *  The IsaLocation of the current Action.
	  *  This will be instantiated during perform and is always present.
	  */
	 protected IsaLocation log = IsaLocation.getInstance(PageLoadingTag.class.getName());

    private String load;
    public static final String CANCELMSG = "Cancel.Message";
    public static final String BASEUI = "baseUI";
    public static final String BASEUI_FQCN = 
			"com.sap.isa.ui.uiclass.BaseUI";
	private boolean scriptLoading = true;

    public PageLoadingTag() {
    }

    public void setLoad(String ld)  {
        load = ld;
    }
	public void setScriptIncluded(String flag)  {
 		if(flag.equals("true"))  {
	  		scriptLoading = true;
		}  else  {
	  		scriptLoading = false;
		}
	}
    public int doStartTag() throws javax.servlet.jsp.JspException {

        try  {
            initializeAccessibilitySupport();
			if(scriptLoading)
	            pageContext.getOut().write(addJavaScriptFile());
			if(null != pageContext.getRequest().getAttribute(ActionForm.ACTIONFORMKEY))  {
	   			String keyName = (String)pageContext.getRequest().getAttribute(ActionForm.ACTIONFORMKEY);
	   			pageContext.setAttribute(ActionForm.ACTIONFORM , pageContext.getRequest().getAttribute(keyName) , PageContext.PAGE_SCOPE);
            }
			WebUtil.setContentType(pageContext, null);

			HttpServletRequest request = (HttpServletRequest)pageContext.getRequest();
			String check = (String)request.getAttribute(RequestProcessor.ENCODED_REQ);
			
			if (check != null) {
			   if (check.equals("ENCODE")) {
				  request = HttpServletRequestFacade.determinRequest(request);
			   }
			}
			pageContext.setAttribute(ContentTypeTag.VAR_NAME, request);
        			
            pageContext.getOut().write(addStyleSheetLink());
        }  catch(Exception exc)  {
              log.debug("Error ", exc);
        }
        return EVAL_BODY_INCLUDE;
    }

	/**
	 * Returns html 'link' element with sve stylesheet source.
	 * 
	 * @return String 
	 */
	private String addStyleSheetLink(){
		StringBuffer buffer = new StringBuffer();
		buffer.append("<link rel=\"stylesheet\" type=\"text/css\" href=\"");
		buffer.append(getContextPath());
		buffer.append("mimes/styles/sve.css\" />");
		return buffer.toString();			
	}
	
	/**
	 * Sets up base objects used for accessibility functionality and detect 
     * ACC switch for current page context.
	 *
	 */
	private void initializeAccessibilitySupport() 
        throws JspTagException {
		BaseUI baseUI = new BaseUI(pageContext);
		pageContext.setAttribute(BASEUI, baseUI, PageContext.PAGE_SCOPE);
		
        IPageContext htmlbPageContext = null;
        try {
            htmlbPageContext = RendererContextTag.getTagRendererContext
                    (this, pageContext);                
        } catch (HtmlbRuntimeException htmlbRuntimeException){
            htmlbPageContext = new com.sapportals.htmlb.rendering.PageContext
                (pageContext.getRequest(), pageContext.getResponse());
        }

        if ((baseUI != null) && baseUI.isAccessible){
            htmlbPageContext.setSection508Rendering(true);
        } else {
            htmlbPageContext.setSection508Rendering(false);                
        }			
	}
	
    public int doEndTag() throws javax.servlet.jsp.JspException {

        try  {

         }  catch(Exception exc)  {
			log.debug("Error ", exc);
        }
        return EVAL_PAGE;
    }

//	private String getLoadingScript()  {
//		StringBuffer buffer = new StringBuffer();
//		buffer.append("<div id='_our_loading'><img width='16' height='16' src='");
//		buffer.append(getContextPath());
//		buffer.append("mimes/images/clockon.gif");
//		buffer.append("'></img></div>");
//		return buffer.toString();
//	}

    private String addJavaScriptFile()  {
        StringBuffer buf = new StringBuffer();
		buf.append("<script type=\"text/javascript\">");
		buf.append("var cancelMsg=\"");
		buf.append(FormUtility.getResourceString(BaseFormConstants.BASEBUNDLE , CANCELMSG , pageContext.getSession()));
		buf.append("\";</script>");
        buf.append("<script type=\"text/javascript\" src=\"");
        buf.append(getContextPath());
        buf.append("jslib/auction.js");
        buf.append("\"></script>");
        buf.append(getLoadingWindow());
        return buf.toString();
    }

//	private String getLoadedScript()  {
//		StringBuffer buffer = new StringBuffer();
//		buffer.append("<script language='JavaScript'>eval(\"document.getElementById('_our_loading').style.display = 'none'\");</script>");
//		return buffer.toString();
//	}

    private String getLoadingWindow()  {
        BaseUI baseUI = AccessibilityUtil.getBaseUI(pageContext);
        StringBuffer buffer = new StringBuffer();
        buffer.append("<link type=\"text/css\" href=\"");
        buffer.append(getContextPath());
        buffer.append("mimes/styles/stylesheet.css\" rel=\"STYLESHEET\">");
        buffer.append("<div id=\"_auction_loading_section\" onClick=\"return false;\" style=\"position:absolute;left:0;top:0;z-index:9998;display:none;\">");
        buffer.append("<img src=\"");
        buffer.append(getContextPath());
        buffer.append("mimes/images/1x1.gif\" width=\"100%\" height=\"100%\"");
        
        buffer.append("/>");
        if ((baseUI != null) && baseUI.isAccessible){
            buffer.append("<a id=\"sapProtectDoubleSubmitIdLink\" tabindex=\"0\" name=\"sapProtectDoubleSubmitIdLink\"");
            buffer.append(" title=\"");
            buffer.append(FormUtility.getResourceString(BaseFormConstants.BASEBUNDLE , UIConstants.LOADINGBOXHEADER , pageContext.getSession()));           
            buffer.append(FormUtility.getResourceString(BaseFormConstants.BASEBUNDLE , UIConstants.LOADINGBOXBODY , pageContext.getSession()));
            buffer.append("\"");
            buffer.append("></a>");
        }
        buffer.append("<div class=\"sapTabDdWhl\" id=\"sapProtectDoubleSubmitId\" style=\"width:250;height:72;z-index:9999\"");
        buffer.append(">");
        buffer.append("<div class='sapTabDdItemH' style='font-size:0.9em'>");
		buffer.append("<span style=\"font-weight:800\"");
               
		if ((null != baseUI) && baseUI.isAccessible){
            buffer.append(" tabindex=\"0\"");
			buffer.append(" title=\"");
			buffer.append(FormUtility.getResourceString(BaseFormConstants.BASEBUNDLE , UIConstants.LOADINGBOXHEADER , pageContext.getSession()));        	
			buffer.append("\"");        	
		}
		buffer.append(">");
        
        buffer.append(FormUtility.getResourceString(BaseFormConstants.BASEBUNDLE , UIConstants.LOADINGBOXHEADER , pageContext.getSession()));
		buffer.append("</span>");
        buffer.append("</div>");
        buffer.append("<div class=\"sapTabDdItem\" style=\"text-align:center;font-size:0.9em\">");
        buffer.append("<br>");
		buffer.append("<span");

		if ((null != baseUI) && baseUI.isAccessible){
            buffer.append(" tabindex=\"0\"");
			buffer.append(" title=\"");
			buffer.append(FormUtility.getResourceString(BaseFormConstants.BASEBUNDLE , UIConstants.LOADINGBOXBODY , pageContext.getSession()));
			buffer.append("\"");
		}
		buffer.append(">");
        
        buffer.append(FormUtility.getResourceString(BaseFormConstants.BASEBUNDLE , UIConstants.LOADINGBOXBODY , pageContext.getSession()));
		buffer.append("</span>");
		buffer.append("<img width=\"16\" height=\"16\" ");
		buffer.append("alt=\"");
		buffer.append(FormUtility.getResourceString(BaseFormConstants.BASEBUNDLE , UIConstants.LOADINGBOXBODY , pageContext.getSession()));
		buffer.append("\" ");
		buffer.append("src=\"");        
        buffer.append(getContextPath());
		buffer.append("mimes/images/clockon.gif");
		buffer.append("\"></img>");
        buffer.append("</div></div>");
        buffer.append("</div>");
        
        return buffer.toString();
    }

    private String getContextPath()  {
        StringBuffer contextPath = new StringBuffer(((HttpServletRequest)pageContext.getRequest()).getContextPath());
        if (!contextPath.toString().endsWith("/"))
                 contextPath.append('/');
        return contextPath.toString();
    }

}
