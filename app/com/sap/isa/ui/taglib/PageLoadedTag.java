package com.sap.isa.ui.taglib;

import com.sap.isa.core.logging.IsaLocation;
import com.sapportals.htmlb.taglib.AbstractComponentTag;

/**
 * Title:
 * Description:
 * Copyright:    Copyright (c) 2002
 * Company:
 * @author
 * @version 1.0
 */

public class PageLoadedTag extends AbstractComponentTag  {

	/** 
	  *  The IsaLocation of the current Action.
	  *  This will be instantiated during perform and is always present.
	  */
	 protected IsaLocation log = IsaLocation.getInstance(PageLoadedTag.class.getName());

    public PageLoadedTag() {
    }


    public int doStartTag() throws javax.servlet.jsp.JspException {

        try  {
        }  catch(Exception exc)  {
			log.debug("Error in doStartTag() ", exc);
        }
        return EVAL_BODY_INCLUDE;
    }

    public int doEndTag() throws javax.servlet.jsp.JspException {

        try  {

            //pageContext.getOut().write(getLoadedScript());
        }  catch(Exception exc)  {
			log.debug("Error in doEndTag() ", exc);
        }
        return EVAL_PAGE;
    }

    private String getLoadedScript()  {
        StringBuffer buffer = new StringBuffer();
        buffer.append("<script language='JavaScript'>eval(\"document.getElementById('_our_loading').style.display = 'none'\");</script>");
        return buffer.toString();
    }
}

