package com.sap.isa.ui.taglib;

import java.io.IOException;

import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.tagext.TagSupport;

import com.sap.isa.core.logging.IsaLocation;

/**
 * Title:
 * Description:
 * Copyright:    Copyright (c) 2003
 * Company:
 * @author
 * @version 1.0
 */

public class OverwriteTitleHeaderTag extends TagSupport {

    private String title;
    private String id;
	/** 
	  *  The IsaLocation of the current Action.
	  *  This will be instantiated during perform and is always present.
	  */
	 protected IsaLocation log = IsaLocation.getInstance(OverwriteTitleHeaderTag.class.getName());

    public OverwriteTitleHeaderTag() {
    }

    public void setTitle(String title)  {
        this.title = title;
    }

    public void setId(String id)  {
        this.id = id;
    }

    public String getTitle()  {
        return title;
    }

    public String getId()  {
        return id;
    }

    public int doStartTag() throws javax.servlet.jsp.JspException {
        try  {
            writeTitle();
        }  catch(Exception exc)  {
             log.debug("Error ", exc);
        }
        return super.SKIP_BODY;
    }

    public int doEndTag() throws javax.servlet.jsp.JspException {
        return super.EVAL_PAGE;
    }

    private void writeTitle()  throws IOException {
        JspWriter out = pageContext.getOut();
        StringBuffer buffer = new StringBuffer();
        buffer.append("<script language=\"Javascript\">");
        buffer.append("if(document.getElementById(\"");
        buffer.append(id);
        buffer.append("\") != null)  {");
        buffer.append("document.getElementById(\"");
        buffer.append(id);
        buffer.append("\").innerText = \"");
        buffer.append(title);
        buffer.append("\";");
        buffer.append("}</script>");
        out.write(buffer.toString());
    }
}
