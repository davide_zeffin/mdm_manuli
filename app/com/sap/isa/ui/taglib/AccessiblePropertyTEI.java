package com.sap.isa.ui.taglib;

import javax.servlet.jsp.tagext.TagData;
import javax.servlet.jsp.tagext.TagExtraInfo;
import javax.servlet.jsp.tagext.VariableInfo;

/**
 * Title:			Internet Sales Selling Via eBay
 * Description:
 * @Copyright: 		Copyright (c) 2004
 * Company:			SAPLabs LLC
 * Created on:		Sep 16, 2004
 */
public class AccessiblePropertyTEI extends TagExtraInfo {

    /* (non-Javadoc)
     * @see javax.servlet.jsp.tagext.TagExtraInfo#getVariableInfo(javax.servlet.jsp.tagext.TagData)
     */
    public VariableInfo[] getVariableInfo(TagData tagData) {
    	String s = tagData.getAttributeString("id");
		VariableInfo info1 = new VariableInfo(
		   s,
			"java.lang.String",
			true,
			VariableInfo.AT_END);
     
		VariableInfo[] info = { info1} ;

		return info;
    }

}
