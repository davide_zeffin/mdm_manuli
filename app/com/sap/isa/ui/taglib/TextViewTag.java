package com.sap.isa.ui.taglib;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspTagException;
import javax.servlet.jsp.tagext.TagSupport;

import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.ui.controllers.ActionForm;
import com.sapportals.htmlb.TextView;
import com.sapportals.htmlb.enum.TextViewDesign;
import com.sapportals.htmlb.enum.TextViewLayout;
import com.sapportals.htmlb.rendering.IPageContext;
import com.sapportals.htmlb.taglib.AbstractComponentTag;
import com.sapportals.htmlb.taglib.RendererContextTag;

/**
 * Title:
 * Description:
 * Copyright:    Copyright (c) 2002
 * Company:
 * @author
 * @version 1.0
 */

public class TextViewTag extends AbstractComponentTag {

    private IPageContext myContext;
    private TextView myTextView;
    private String accText;

	/** 
	  *  The IsaLocation of the current Action.
	  *  This will be instantiated during perform and is always present.
	  */
	 protected IsaLocation log = IsaLocation.getInstance(TextViewTag.class.getName());


    public TextViewTag() {
    }

    public void setId (String id) {
        try  {
            myContext = RendererContextTag.getTagRendererContext(this, pageContext);
        }  catch(JspTagException jsExc)  {
              log.debug("Error in setId ", jsExc);
        }
        if(myContext != null)  {
            try{
            myTextView = (TextView)myContext.getAttribute(id);
            }catch(Exception ex) {
				log.debug("Error in setId ", ex);
            }
        }
        if(null == myTextView)  {
            myTextView = new TextView(id);
            myTextView.setText("");
        }
        myTextView.setId(id);
    }

    public void setText (String text) {
        myTextView.setText(text);
    }

//    public void setAcctext(String text){
//        this.accText = text;
//    }
    
    public void setTooltip (String tooltip) {
        myTextView.setTooltip(tooltip);
    }

    public void setLayout(String layout) {
		myTextView.setLayout( (TextViewLayout)convertToEnum("textView", "layout",
								  layout, "com.sapportals.htmlb.enum.TextViewLayout", this) );
    }

    public void setDesign(String design) {
            myTextView.setDesign( (TextViewDesign)convertToEnum("textView", "design",
                                                        design, "com.sapportals.htmlb.enum.TextViewDesign", this) );
    }

    public void setRequired(String required) {
            myTextView.setRequired( convertToBoolean("textView", "required", required) );
    }

    public void setEncode(String encode) {
            myTextView.setEncode( convertToBoolean("textView", "encode", encode) );
    }

    public void setWidth(String width) {
            myTextView.setWidth( width );
    }

    public void setWrapping(String wrapping) {
            myTextView.setWrapping( convertToBoolean("textView", "wrapping", wrapping) );
    }

    public int doStartTag () throws JspException {

        myContext = RendererContextTag.getTagRendererContext(this, pageContext);
        if ( (myTextView.getId() != null) && (myTextView.getId().length() > 0) ) {
                // Put the TextView instance into the page context
                // -> It is available as scripting variable (default scope: PageContext.PAGE_SCOPE)
                pageContext.setAttribute(myTextView.getId(), myTextView);
        }

        return TagSupport.EVAL_BODY_INCLUDE; // -> Scripting in the body

    }

    public int doEndTag () throws JspException {
		ActionForm actionFrm = (ActionForm)pageContext.getAttribute(ActionForm.ACTIONFORM);
		if((actionFrm == null) || (actionFrm != null && 
			  myTextView != null &&
			  actionFrm.isControlApplicable(myTextView.getId())))  {
//          if (AccessibilityUtil.isAccessibleContext(pageContext) && (null != accText)){
//              myTextView.setText(accText);
//          }
        	myContext.render(myTextView);
		}
        return super.doEndTag(); // BodyTagSupport: return EVAL_PAGE;
    }
}
