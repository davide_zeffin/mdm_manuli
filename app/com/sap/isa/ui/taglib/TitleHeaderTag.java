package com.sap.isa.ui.taglib;

import java.io.IOException;

import javax.servlet.jsp.JspException;

import com.sap.tc.logging.Category;
import com.sap.tc.logging.Location;
import com.sap.isa.auction.logging.CategoryProvider;
import com.sap.isa.auction.logging.LogHelper;
import com.sap.isa.auction.util.AccessibilityUtil;
import com.sap.isa.ui.components.TitleHeader;
import com.sapportals.htmlb.rendering.IPageContext;
import com.sapportals.htmlb.taglib.AbstractComponentTag;
import com.sapportals.htmlb.taglib.RendererContextTag;

/**
 * Title:
 * Description:
 * Copyright:    Copyright (c) 2003
 * Company:
 * @author
 * @version 1.0
 */

public class TitleHeaderTag extends AbstractComponentTag  {

    private String id;
    private String title = "";
    private boolean underline = true;
    private TitleHeader titleHeader = null;
    private String toolTip;
    private IPageContext myContext;

	private static Category logger = CategoryProvider.getUICategory();
	private static Location tracer = Location.getLocation(TitleHeaderTag.class);
	 
    public TitleHeaderTag() {
    }

    public void setTitle(String titl)  {
        this.title = titl;
    }

    public void setId(String ident)  {
		try  {
			myContext = RendererContextTag.getTagRendererContext(this, pageContext);
		}  catch(Exception jsExc)  {
			LogHelper.logError(logger, tracer, jsExc);
		}
		if(myContext != null)  {
			titleHeader = (TitleHeader)myContext.getAttribute(ident);
		}
		this.id = ident;
    }

    public String getTitle()  {
    	if(titleHeader != null)  {
    		return titleHeader.getText();	
    	}
        return this.title;
    }

    public String getId()  {
        return this.id;
    }
    
    public void setUnderline(boolean flg)  {
    	this.underline = flg;
    }

    public int doStartTag() throws javax.servlet.jsp.JspException {
            try {
                pageContext.getOut().write(writeTitle());
            } catch (IOException e) {
                LogHelper.logError(logger, tracer, e);
            }
        return super.SKIP_BODY;
    }

    public int doEndTag() throws javax.servlet.jsp.JspException {

        try  {
		
         }  catch(Exception exc)  {
			    LogHelper.logError(logger, tracer, exc);
        }
        return EVAL_PAGE;
    }

    private String writeTitle() throws JspException {
        StringBuffer buffer = new StringBuffer();
        buffer.append("<table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"100%\">");
        buffer.append("<tr>");
        buffer.append("<td class=\"sapAppBrandTopLvl2\"></td>");
        buffer.append("<td nowrap width=\"100%\" valign=\"top\" colspan=\"2\">");
        buffer.append("<table cellpadding=\"0\" border=\"0\" cellspacing=\"0\" width=\"100%\">");
        buffer.append("<tr>");
        buffer.append("<td class=\"sapAppTitle\" title=\"\"><span id=\"");
        buffer.append(id + "\" ");
        if (null != toolTip || 
            (AccessibilityUtil.isAccessibleContext(pageContext))){
            buffer.append(" tabindex=\"0\" ");
            // append if the acc switch is on even if tooltip is null
        	buffer.append(" title=\"");
            String tooltip = getTooltip();
            if (null == tooltip){
                tooltip = getTitle();
            }
			buffer.append(tooltip);
            buffer.append("\"");
//        } else if ){
//			buffer.append("\" title=\"");
//			buffer.append(getTitle());
		}

        buffer.append(">");
        buffer.append(this.getTitle());
        buffer.append("</span>");
        buffer.append("</td>");
        buffer.append("</tr>");
        buffer.append("</table>");
        buffer.append("</td>");
        buffer.append("</tr>");
        if(underline)  {
			buffer.append("<tr><td class=\"sapAppBrandLeftLvl1\"></td>");
			buffer.append("<td class=\"sapAppBrandCenterLvl1\" width=\"100%\">&nbsp;</td>");
			buffer.append("</tr>");
        }
        buffer.append("</table>");
        return buffer.toString();
    }

    /**
     * @return
     */
    public String getTooltip() {
        return toolTip;
    }

    /**
     * @param string
     */
    public void setTooltip(String string) {
        toolTip = string;
    }

}