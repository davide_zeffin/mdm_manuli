package com.sap.isa.ui.taglib;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspTagException;

import com.sap.isa.core.logging.IsaLocation;
import com.sapportals.htmlb.Tree;
import com.sapportals.htmlb.TreeNode;
import com.sapportals.htmlb.rendering.IPageContext;
import com.sapportals.htmlb.taglib.AbstractContainerTag;
import com.sapportals.htmlb.taglib.RendererContextTag;

/**
 * Title:
 * Description:
 * Copyright:    Copyright (c) 2002
 * Company:
 * @author
 * @version 1.0
 */

public class TreeTag extends AbstractContainerTag {

	/** 
	  *  The IsaLocation of the current Action.
	  *  This will be instantiated during perform and is always present.
	  */
	 protected IsaLocation log = IsaLocation.getInstance(TreeTag.class.getName());

    private String title;
    private boolean rootNodeIsVisible;
    private String rootNode;
    private IPageContext myContext;

    private Tree tree = new Tree("Sample");

    public TreeTag() {
    }

    public void setId(String id)  {
        try  {
            myContext = RendererContextTag.getTagRendererContext(this , pageContext);
        }  catch (Exception exc)  {
             log.debug("Error in setId() ", exc);
        }
        if(null != myContext)  {
            tree = (Tree)myContext.getAttribute(id);
        }
        if(null == tree)  {
            tree = new Tree(id);
        }
    }

    public void setTitle (String title)  {
        tree.setTitle (title);
    }

    public void setOffsetForTreeNode(String offset)  {
        tree.setOffsetForTreeNode(Integer.parseInt(offset));
    }

    public void setTooltip(String tooltip)  {
        tree.setTooltip(tooltip);
    }

    public void setRootNodeIsVisible (String rootNodeIsVisible)  {
        tree.setRootNodeIsVisible(rootNodeIsVisible.toLowerCase().equals("true"));
    }

    public void setRootNode (String rootNode)  {
        // Try to get the rootNode from the page context
        TreeNode root = (TreeNode) pageContext.getAttribute(rootNode);
        if (null == root && myContext != null) {
            root = (TreeNode)myContext.getAttribute(rootNode);
        }
        tree.setRootNode(root);
    }

    public Tree getTree() {
        return this.tree;
    }

    public int doStartTag() throws JspTagException  {
        if ( (this.getId() != null) && (this.getId().length() > 0) ) {
                tree.setId(this.getId());

                // Put the Grid instance into the page context
                // -> It is available as scripting variable (default scope: PageContext.PAGE_SCOPE)
                pageContext.setAttribute(tree.getId(), tree);
        }

        return EVAL_BODY_TAG;
    }

    public int doAfterBody() throws JspTagException  {
        return SKIP_BODY;
    }

    public int doEndTag() throws JspException  {
        IPageContext myContext
            = RendererContextTag.getTagRendererContext(this, pageContext);

        // Check if there is a root node defined
        if (tree.getRootNode() == null) {
            throw new JspTagException ("Tree tag defined without root node");
        }

        myContext.render(tree);

        return EVAL_PAGE;
    }
}

