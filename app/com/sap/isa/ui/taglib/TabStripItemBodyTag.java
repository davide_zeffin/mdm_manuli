package com.sap.isa.ui.taglib;

import javax.servlet.jsp.JspException;

import com.sapportals.htmlb.HTMLFragment;
import com.sapportals.htmlb.TabStripItem;
import com.sapportals.htmlb.taglib.AbstractContainerTag;

/**
 * Title:        Internet Sales
 * Description:
 * Copyright:    Copyright (c) 2002
 * Company:      SAPMarkets
 * @author
 * @version 1.0
 */

public final class TabStripItemBodyTag extends AbstractContainerTag
{
    private TabStripItem myItem;

    public TabStripItemBodyTag()
    {
        myItem = null;
    }

    public int doStartTag()
        throws JspException
    {
        javax.servlet.jsp.tagext.Tag parentTagHandler = getParent();
        if(parentTagHandler instanceof TabStripItemTag)
        {
            TabStripItemTag itemTagHandler = (TabStripItemTag)parentTagHandler;
            myItem = itemTagHandler.getTabStripItem();
        } else
        {
            throw new JspException("A tabStripHeader must be nested in an tabStrip !");
        }
        return super.doStartTag();
    }

    public int doEndTag()
        throws JspException
    {
        String htmlContent = getBodyContent().getString();
        if(htmlContent != null && htmlContent.length() > 0)
        {
            HTMLFragment component = new HTMLFragment(htmlContent);
            myItem.setBody(component);
        }
        return super.doEndTag();
    }

}
