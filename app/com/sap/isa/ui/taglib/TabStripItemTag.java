package com.sap.isa.ui.taglib;

import javax.servlet.jsp.JspException;

import com.sapportals.htmlb.TabStrip;
import com.sapportals.htmlb.TabStripItem;
import com.sapportals.htmlb.taglib.AbstractContainerTag;

/**
 * Title:        Internet Sales
 * Description:
 * Copyright:    Copyright (c) 2002
 * Company:      SAPMarkets
 * @author
 * @version 1.0
 */



// Referenced classes of package com.sapportals.htmlb.taglib:
//            AbstractContainerTag, TabStripTag, RendererContextTag

public final class TabStripItemTag extends AbstractContainerTag
{
    private TabStrip myTabStrip = null;
    private TabStripItem myItem = null;
    private String id;
    private String index;
    private String height;
    private String onSelect;
    private String title;
    private String tooltip;
    private String width;

    public TabStripItemTag()
    {
    }

    public void setId(String id)
    {
        this.id = id;
    }

    public void setIndex(String index)
    {
        this.index = index;
    }

    public void setHeight(String height)
    {
        this.height = height;
    }

    public void setOnSelect(String onSelect)
    {
        this.onSelect = onSelect;
    }

    public void setTitle(String title)
    {
        this.title = title;
    }

    public void setTooltip(String tooltip)
    {
        this.tooltip = tooltip;
    }

    public void setWidth(String width)
    {
        this.width = width;
    }

    public TabStripItem getTabStripItem()
    {
        return myItem;
    }

    public int doStartTag()
        throws JspException
    {

        javax.servlet.jsp.tagext.Tag parentTagHandler = getParent();
        if(parentTagHandler instanceof TabStripTag)
        {
            myTabStrip = ((TabStripTag)parentTagHandler).getTabStrip();
            if(index == null || index.length() < 1)
                throw new JspException("A tabStripItem must have an index !");
            myItem = myTabStrip.addTab(convertToInteger("tabStripItem", "index", index));
            if(id != null && id.length() > 0)
                myItem.setId(id);
            if(height != null && height.length() > 0)
                myItem.setHeight(height);
            if(width != null && width.length() > 0)
                myItem.setWidth(width);
            if(onSelect != null && onSelect.length() > 0)
                myItem.setOnSelect(onSelect);
            if(title != null && title.length() > 0)
                myItem.setTitle(title);
            if(tooltip != null && tooltip.length() > 0)
                myItem.setTooltip(tooltip);
        } else
        {
            throw new JspException("A tabStripItem must be nested in an tabStrip !");
        }
        if(myItem.getId() != null && myItem.getId().length() > 0)
            pageContext.setAttribute(myItem.getId(), myItem);
        return super.doStartTag();
    }

    public int doEndTag()
        throws JspException
    {
        if(myItem.getHeader() == null && (myItem.getTitle() == null || myItem.getTitle().length() < 1))
            throw new JspException("The tabStripItem with index " + index + " in tabStrip " + myTabStrip.getId() + " must have an inner tabStripHeader or an attribute title !");
        if(myItem.getBody() == null)
            throw new JspException("The tabStripItem with index " + index + " in tabStrip " + myTabStrip.getId() + " must have an inner tabStripBody !");
        else
            return super.doEndTag();
    }
}