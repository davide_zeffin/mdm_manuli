package com.sap.isa.ui.taglib;

import java.io.IOException;
import java.text.MessageFormat;

import javax.servlet.jsp.JspException;

import com.sap.isa.auction.logging.CategoryProvider;
import com.sap.isa.auction.logging.LogHelper;
import com.sap.isa.auction.util.AccessibilityUtil;
import com.sap.tc.logging.Category;
import com.sap.tc.logging.Location;
import com.sapportals.htmlb.taglib.AbstractComponentTag;

/**
 * Title:			Internet Sales Selling Via eBay
 * Description:
 * @Copyright: 		Copyright (c) 2004
 * Company:			SAPLabs LLC
 * Created on:		Sep 30, 2004
 */
public class AccessibleTitleTag extends AbstractComponentTag {

    private String value;
    
    private String[] args;    
    
    // Synchronizing object
    private Object syncObject = new Object();
    
    private static final Category logger = CategoryProvider.getUICategory();
    private static final Location tracer = Location.getLocation(
            AccessibleTitleTag.class);
    
    /* (non-Javadoc)
     * @see javax.servlet.jsp.tagext.Tag#doStartTag()
     */
    public int doStartTag() throws JspException {
        if (AccessibilityUtil.isAccessibleContext(pageContext)){
            try {                
                String translatedValue = null;
                synchronized(syncObject){
                    translatedValue = MessageFormat.format(getValue(), args);                    
                }
                pageContext.getOut().write("title=\"" + translatedValue + "\"");
            } catch (IOException e) {
                LogHelper.logInfo(logger, tracer, "Exception occurred while rendering " +
                    "title.", null);
            }
        }
        return EVAL_PAGE;
    }

    /**
     * @return
     */
    public String getValue() {
        return value;
    }

    /**
     * @param string
     */
    public void setValue(String string) {
        value = string;
    }
    
    /**
     * @return
     */
    public String getArg0() {
        return args[0];
    }

    /**
     * @return
     */
    public String getArg1() {
        return args[1];
    }

    /**
     * @return
     */
    public String getArg2() {
        return args[2];
    }

    /**
     * @return
     */
    public String getArg3() {
        return args[3];
    }

    /**
     * @return
     */
    public String getArg4() {
        return args[4];
    }

    /**
     * @param string
     */
    public void setArg0(String string) {
        setArgument(0, string);
    }

    /**
     * @param string
     */
    public void setArg1(String string) {
        setArgument(1, string);
    }

    /**
     * @param string
     */
    public void setArg2(String string) {
        setArgument(2, string);
    }

    /**
     * @param string
     */
    public void setArg3(String string) {
        setArgument(3, string);
    }

    /**
     * @param string
     */
    public void setArg4(String string) {
        setArgument(4, string);
    }
    
    /**
     * Sets the String value into the passed argument position array 
     * @param i
     * @param arg
     */
    private void setArgument(int i, String arg) {
        if (args == null)
            args = new String[5];
        args[i] = arg;
    }
    

}
