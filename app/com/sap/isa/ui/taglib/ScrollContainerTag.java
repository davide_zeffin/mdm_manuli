package com.sap.isa.ui.taglib;

import javax.servlet.jsp.JspException;

import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.ui.renderer.ExScrollContainerRenderer;
import com.sapportals.htmlb.HTMLFragment;
import com.sapportals.htmlb.ScrollContainer;
import com.sapportals.htmlb.enum.BrowserType;
import com.sapportals.htmlb.rendering.IPageContext;
import com.sapportals.htmlb.rendering.RendererManager;
import com.sapportals.htmlb.taglib.AbstractContainerTag;
import com.sapportals.htmlb.taglib.RendererContextTag;


/**
 * Title: ScrollContainerTag
 * Copyright (c) SAP AG 2002
 * @author	 ChME, Aug 06, 2002
 * @version $Revision: #1 $ <BR>
 */
public final class ScrollContainerTag extends AbstractContainerTag {

  IPageContext myContext = null;
  ScrollContainer scrollContainer = new ScrollContainer();

  /** 
	*  The IsaLocation of the current Action.
	*  This will be instantiated during perform and is always present.
	*/
   protected IsaLocation log = IsaLocation.getInstance(ScrollContainerTag.class.getName());

  public void setId(String id) {
	try  {
		myContext = RendererContextTag.getTagRendererContext(this, pageContext);
	} catch(Exception exc)  {
		log.debug("Error in setId() ", exc);
	}
	if(null != myContext)  {
		try  {
			scrollContainer = (ScrollContainer)myContext.getAttribute(id);
		}catch(Exception exc)  {
			//System.out.println("Error occured");
			log.debug("Error in setId() ", exc);
		}
	}
	if(null == scrollContainer)  {
		scrollContainer = new ScrollContainer(id);
	}	
	scrollContainer.setId(id);
  }

  /**
   * Sets the width of the scroll container
   *
   * @param width
   */
  public void setWidth(String width) {
	scrollContainer.setWidth(width);
  }

  /**
   * Sets the height of the scroll container
   *
   * @param width
   */
  public void setHeight(String height) {
	scrollContainer.setHeight(height);
  }

  /**
   * Redefined methods from TagSupport
   */
  public int doStartTag () throws JspException  {
	if ( (scrollContainer.getId() != null) && (scrollContainer.getId().length() > 0) ) {
	  pageContext.setAttribute(scrollContainer.getId(), scrollContainer);
	}
	RendererManager.addRenderer(ScrollContainer.UI_ID , BrowserType.DEFAULT , 
				ExScrollContainerRenderer.class);
	RendererManager.addRenderer(ScrollContainer.UI_ID , BrowserType.MSIE55, 
				ExScrollContainerRenderer.class);
	RendererManager.addRenderer(ScrollContainer.UI_ID , BrowserType.MSIE5, 
				ExScrollContainerRenderer.class);
	RendererManager.addRenderer(ScrollContainer.UI_ID , BrowserType.MSIE6, 
				ExScrollContainerRenderer.class);

	return super.doStartTag(); // BodyTagSupport: return EVAL_BODY_TAG;
  }

  public int doEndTag () throws JspException  {
	myContext = RendererContextTag.getTagRendererContext(this, pageContext);

	// Add the body content to the form and render the form
	String htmlContent = getBodyContent().getString();
	if ( htmlContent != null && htmlContent.length() > 0 ) {
	  scrollContainer.addComponent( new HTMLFragment(htmlContent) );
	}
	myContext.render(scrollContainer);

	return super.doEndTag(); // BodyTagSupport: return EVAL_PAGE;
  }

}
