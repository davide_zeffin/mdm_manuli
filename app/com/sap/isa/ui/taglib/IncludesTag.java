package com.sap.isa.ui.taglib;

import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.tagext.TagSupport;

import com.sap.isa.core.logging.IsaLocation;

/**
 * Title:
 * Description:
 * Copyright:    Copyright (c) 2002
 * Company:
 * @author
 * @version 1.0
 */

public class IncludesTag extends TagSupport  {

	/** 
	  *  The IsaLocation of the current Action.
	  *  This will be instantiated during perform and is always present.
	  */
	 protected IsaLocation log = IsaLocation.getInstance(IncludesTag.class.getName());

  public IncludesTag() {
  }

  public int doStartTag() throws javax.servlet.jsp.JspException {

    return TagSupport.SKIP_BODY;
  }
  public int doEndTag() throws javax.servlet.jsp.JspException {

    JspWriter out = pageContext.getOut();
    try  {

        //out.write(RendererManager.);
    }  catch (Exception exc)  {
		log.debug("Error in doRndTag() ", exc);
    }

    return TagSupport.EVAL_PAGE;
  }


}