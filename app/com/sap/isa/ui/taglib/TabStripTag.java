package com.sap.isa.ui.taglib;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspTagException;

import com.sap.isa.core.logging.IsaLocation;
import com.sapportals.htmlb.TabStrip;
import com.sapportals.htmlb.enum.CellHAlign;
import com.sapportals.htmlb.enum.CellVAlign;
import com.sapportals.htmlb.rendering.IPageContext;
import com.sapportals.htmlb.taglib.AbstractContainerTag;
import com.sapportals.htmlb.taglib.RendererContextTag;


/**
 * Title:        Internet Sales
 * Description:
 * Copyright:    Copyright (c) 2002
 * Company:      SAPMarkets
 * @author
 * @version 1.0
 */

public final class TabStripTag extends AbstractContainerTag
{
    TabStrip tabStrip;
    IPageContext myContext;

	/** 
	  *  The IsaLocation of the current Action.
	  *  This will be instantiated during perform and is always present.
	  */
	 protected IsaLocation log = IsaLocation.getInstance(TabStripTag.class.getName());

    public TabStripTag(){}

    public void setId(String id)
    {
        try {
            myContext = RendererContextTag.getTagRendererContext(this, pageContext);
        } catch (JspTagException ex)
        {
        	log.debug("Error ", ex);
        }
        if (null != myContext)
            //tabStrip = (TabStrip)myContext.getOldForm().getComponentById(id);
            tabStrip = (TabStrip)myContext.getAttribute(id);
        if (null == tabStrip)
            tabStrip = new TabStrip("tempName");
        tabStrip.setId(id);
    }

    public void setBodyHeight(String bodyHeight)
    {
        tabStrip.setBodyHeight(bodyHeight);
    }

    public void setWidth(String width)
    {
        tabStrip.setWidth(width);
    }

    public void setHorizontalAlignment(String horizontalAlignment)
    {
        tabStrip.setHAlign((CellHAlign)convertToEnum("tabStrip", "horizontalAlignment", horizontalAlignment, "com.sapportals.htmlb.enum.CellHAlign", this));
    }

    public void setVerticalAlignment(String verticalAlignment)
    {
        tabStrip.setVAlign((CellVAlign)convertToEnum("tabStrip", "verticalAlignment", verticalAlignment, "com.sapportals.htmlb.enum.CellVAlign", this));
    }

    public void setSelection(String selection)
    {
        tabStrip.setSelection(convertToInteger("tabStrip", "selection", selection));
    }

    public void setTooltip(String tooltip)
    {
        tabStrip.setTooltip(tooltip);
    }

    public TabStrip getTabStrip()
    {
        return tabStrip;
    }

    public int doStartTag()
        throws JspException
    {
        if ((tabStrip.getId() != null) && (tabStrip.getId().length() >0)) {
            pageContext.setAttribute(tabStrip.getId(), tabStrip);
        }
        return super.doStartTag();
    }

    public int doEndTag()
        throws JspException
    {
        //myContext = RendererContextTag.getTagRendererContext(this, pageContext);
        myContext.render(tabStrip);
        return super.doEndTag();
    }

}

