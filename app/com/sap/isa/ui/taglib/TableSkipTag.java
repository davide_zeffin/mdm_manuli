package com.sap.isa.ui.taglib;

import java.io.IOException;

import javax.servlet.jsp.JspException;

import com.sap.isa.auction.actionforms.BaseFormConstants;
import com.sap.isa.auction.logging.CategoryProvider;
import com.sap.isa.auction.util.AccessibilityUtil;
import com.sap.isa.auction.util.FormUtility;
import com.sap.tc.logging.Category;
import com.sap.tc.logging.Location;
import com.sapportals.htmlb.taglib.AbstractComponentTag;

/**
 * Title:			Internet Sales Selling Via eBay
 * Description:
 * @Copyright: 		Copyright (c) 2004
 * Company:			SAPLabs LLC
 * Created on:		Sep 16, 2004
 */
public class TableSkipTag extends AbstractComponentTag {

	private String next;
	private String previous;
	private static final String BEGINTABLE = "TableSkip.BeginTable";
	private static final String ENDTABLE = "TableSkip.EndTable";
    
    protected static Category logger = CategoryProvider.getUICategory();
    protected static Location tracer = Location.getLocation(TableSkipTag.class); 
	
	/* (non-Javadoc)
	 * @see javax.servlet.jsp.tagext.Tag#doStartTag()
	 */
	public int doStartTag() throws JspException {
        if (!AccessibilityUtil.isAccessibleContext(pageContext)){
            return SKIP_BODY;
        }
		checkConditions();
		try {
			if (null != getNext()){
				pageContext.getOut().write(composeNextHtmlString());
			} else if (null != getPrevious()){
				pageContext.getOut().write(composePreviousHtmlString());        		
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		return SKIP_BODY;
	}

	/**
	 * Checks tag properties are correctly set.
	 * 
	 * @throws JspException
	 */
	private void checkConditions() throws JspException {
		if ((null != getNext() && !"".equals(getNext())) && 
			(null != getPrevious() && !"".equals(getPrevious()))){
			throw new JspException("The next and previous property cannot be " +
					"set simultaneously.");
		}
		if ((null == getNext() || "".equals(getNext())) && 
			(null == getPrevious() || "".equals(getPrevious()))){
			throw new JspException("Either the next or previous property must " +
					"have a value");
		}
	}

	private String composeNextHtmlString(){
		StringBuffer sb = new StringBuffer();
		String beginGroup = FormUtility.getResourceString(
				BaseFormConstants.BASEBUNDLE, BEGINTABLE, 
				pageContext.getSession());
		sb.append("<a id=\"" + getId() + "\" " +
				"name=\"" + getId() + "\" " + 
				"tabindex=\"0\" " +
				"title=\"" + beginGroup +"\" " +
				"href=\"#" + getNext() + "\"></a>");
		return sb.toString();
	}

	private String composePreviousHtmlString(){
		StringBuffer sb = new StringBuffer();
		String endGroup = FormUtility.getResourceString(
			BaseFormConstants.BASEBUNDLE, ENDTABLE, 
				pageContext.getSession());
		sb.append("<a id=\"" + getId() + "\" "+
				"name=\"" + getId() + "\" " + 
				"tabindex=\"0\" " +
				"title=\"" + endGroup +"\" " +
				"href=\"#" + getPrevious() + "\"></a>");
		return sb.toString();
	}	
	
	/**
	 * @return
	 */
	public String getNext() {
		return next;
	}

	/**
	 * @return
	 */
	public String getPrevious() {
		return previous;
	}

	/**
	 * @param string
	 */
	public void setNext(String string) {
		next = string;
	}

	/**
	 * @param string
	 */
	public void setPrevious(String string) {
		previous = string;
	}

}
