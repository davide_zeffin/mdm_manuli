package com.sap.isa.ui.taglib;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspTagException;
import javax.servlet.jsp.tagext.TagSupport;

import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.ui.controllers.ActionForm;
import com.sapportals.htmlb.Label;
import com.sapportals.htmlb.enum.TextViewDesign;
import com.sapportals.htmlb.rendering.IPageContext;
import com.sapportals.htmlb.taglib.AbstractComponentTag;
import com.sapportals.htmlb.taglib.RendererContextTag;

/**
 * Title:			Internet Sales Selling Via eBay
 * Description:
 * @Copyright: 		Copyright (c) 2004
 * Company:			SAPLabs LLC
 * Created on:		Nov 15, 2004
 */
public class LabelTag extends AbstractComponentTag {
    private IPageContext myContext;
    private Label myLabel;
 
	/** 
	  *  The IsaLocation of the current Action.
	  *  This will be instantiated during perform and is always present.
	  */
	 protected IsaLocation log = IsaLocation.getInstance(LabelTag.class.getName());
    
    public LabelTag(){
    }
    
    public void setId(String id){
        try  {
            myContext = RendererContextTag.getTagRendererContext
                    (this, pageContext);
        } catch (JspTagException jsExc) {
        	log.debug("Error in setId() ", jsExc);
        }
        if(myContext != null)  {
            try {
                myLabel = (Label) myContext.getAttribute(id);
            } catch(Exception ex) {
				log.debug("Error in setId() ", ex);
            }
        }
        if(null == myLabel)  {
            myLabel = new Label(id);
        }
        myLabel.setId(id);        
    }
    
    public void setText (String text) {
        myLabel.setText(text);
    }
    public void setLabelFor (String forId) {
        myLabel.setLabeledComponentId(forId);
//        this.forId = forId;
    }
    public void setTooltip (String tooltip) {
        myLabel.setTooltip(tooltip);
    }
    public void setDesign(String design) {
        myLabel.setDesign( (TextViewDesign)convertToEnum("textView", "design",
                                design, "com.sapportals.htmlb.enum.TextViewDesign", this) );
    }
    public void setRequired(String required) {
        myLabel.setRequired( convertToBoolean("textView", "required", required) );
    }
    public void setEncode(String encode) {
        myLabel.setEncode( convertToBoolean("textView", "encode", encode) );
    }
    public void setWidth(String width) {
        myLabel.setWidth( width );
    }
    public void setWrapping(String wrapping) {
        myLabel.setWrapping( convertToBoolean("textView", "wrapping", wrapping) );
    }

    public void setLabeledComponentClass(String labeledComponentClass)
    {
        myLabel.setLabledComponentClassName(labeledComponentClass);
    }

    
    public void setJsObjectNeeded(String jsObjectNeeded) {
          myLabel.setJsObjectNeeded( convertToBoolean("textView", "jsObjectNeeded", jsObjectNeeded) );
        }

    // -------------------------------------------------------------------------
    // Redefined methods from TagSupport

    public int doStartTag () throws JspException {

        myContext = RendererContextTag.getTagRendererContext(this, pageContext);

        //add a virtual labeled component to the hashtabel
        // change the following line after implementation

//        LabeledComponent myLabeledComponent = (LabeledComponent) myContext.getComponentForId(this.forId);
//        myLabel.setLabelFor(myLabeledComponent);

        if ( (myLabel.getId() != null) && (myLabel.getId().length() > 0) ) {
            // Put the TextView instance into the page context
            // -> It is available as scripting variable (default scope: PageContext.PAGE_SCOPE)
            pageContext.setAttribute(myLabel.getId(), myLabel);
        }

        return TagSupport.EVAL_BODY_INCLUDE; // -> Scripting in the body

    }

    public int doEndTag () throws JspException {
        ActionForm actionFrm = (ActionForm)pageContext.getAttribute
                (ActionForm.ACTIONFORM);
        if((actionFrm == null) || (actionFrm != null && 
              myLabel != null &&
              actionFrm.isControlApplicable(myLabel.getId())))  {
            myContext.render(myLabel);
        }

//        // Any HTML or any controls in the body is ignored, only scripting changing
//        // the scripting variable for the TextView has an impact on the rendering
//        myContext.render(myLabel);

        return super.doEndTag(); // BodyTagSupport: return EVAL_PAGE;
    }        
}
