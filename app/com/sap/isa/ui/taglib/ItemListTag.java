package com.sap.isa.ui.taglib;

import java.io.IOException;
import java.text.MessageFormat;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspTagException;

import com.sap.tc.logging.Category;
import com.sap.tc.logging.Location;
import com.sap.isa.auction.actionforms.BaseFormConstants;
import com.sap.isa.auction.actionforms.Form;
import com.sap.isa.auction.logging.CategoryProvider;
import com.sap.isa.auction.logging.LogHelper;
import com.sap.isa.auction.util.AccessibilityUtil;
import com.sap.isa.auction.util.FormUtility;
import com.sap.isa.ui.uiclass.BaseUI;
import com.sap.isa.ui.controllers.ActionForm;
import com.sapportals.htmlb.Component;
import com.sapportals.htmlb.GridLayout;
import com.sapportals.htmlb.ItemList;
import com.sapportals.htmlb.rendering.IPageContext;
import com.sapportals.htmlb.taglib.AbstractContainerTag;
import com.sapportals.htmlb.taglib.RendererContextTag;

/**
 * Title:
 * Description:
 * Copyright:    Copyright (c) 2002
 * Company:
 * @author
 * @version 1.0
 */

public class ItemListTag extends AbstractContainerTag {

    ItemList     myItemList = new ItemList();
    IPageContext myContext;
    Component otherComponent;

    private static final Category logger = CategoryProvider.getUICategory();
    private static final Location tracer = Location.getLocation(ItemListTag.class);
    
    public ItemListTag() {
    }

    public void setId(String id) {
        try  {
            myContext = RendererContextTag.getTagRendererContext(this, pageContext);
        }  catch(JspTagException jsExc)  {
             LogHelper.logError(logger, tracer, jsExc);
        }
        myItemList = null;
        otherComponent = null;
        if(myContext != null)  {
            if(myContext.getAttribute(id) instanceof ItemList){
                myItemList = (ItemList)myContext.getAttribute(id);
                myItemList.setId(id);
            } else {
                otherComponent = (Component)myContext.getAttribute(id);
                if (null != otherComponent){    
                    otherComponent.setId(id);
                }
           }            
        }
        if(null == myItemList && null == otherComponent)  {
            myItemList = new ItemList();
            myItemList.setId(id);
        }
    }

    public void setOrdered (String ordered) {
        myItemList.setOrdered( convertToBoolean("ItemList", "ordered", ordered) );
    }

    public ItemList getItemList() {
        return this.myItemList;
    }

    public int doStartTag () throws JspException {

        if (myItemList != null && (myItemList.getId() != null) && (myItemList.getId().length() > 0) ) {
                pageContext.setAttribute(myItemList.getId(), myItemList);
        }

        return super.doStartTag();
    }

    public int doEndTag () throws JspException {
        StringBuffer sb = new StringBuffer();
        ActionForm actionFrm = (ActionForm)pageContext.getAttribute(ActionForm.ACTIONFORM);
        if((actionFrm == null && myItemList != null) || (actionFrm != null && 
                myItemList != null &&
                actionFrm.isControlApplicable(myItemList.getId())))  {
            myContext = RendererContextTag.getTagRendererContext(this, pageContext);
            myContext.render(myItemList);
        }
        if(otherComponent != null && null == myItemList){           
            boolean renderSkipEnd = false;
            if (AccessibilityUtil.isAccessibleContext(pageContext) && isErrorList()){
                /*
                 * Elements that will be layered
                 * -Begin Skip Element
                 * -Error List Accessible Description
                 * -Error List
                 * -End Skip Element
                 */
                int rowSize = ((GridLayout)otherComponent).getRowSize();
                if (rowSize > 1){
                    renderSkipEnd = true;

                    int numOfErrors = rowSize - 1; // Do not count the empty row
                    sb.append("<a id=\"" + otherComponent.getId() + "BeginMsgTable\"");
                    sb.append(" name=\"" + otherComponent.getId() + "BeginMsgTable\"");
                    sb.append(" href=\"#" + otherComponent.getId() + "EndMsgTable\"");
                    sb.append(" tabindex=\"0\"");                    sb.append(" title=\"");
                    String message = FormUtility.getResourceString(
                            BaseFormConstants.BASEBUNDLE, 
                            "Access.BeginMessageTable", 
                            pageContext.getSession());
                    Object[] args = {String.valueOf(numOfErrors)};                            
                    sb.append(MessageFormat.format(message, args));                            
                    sb.append("\"/>");                 
                    sb.append("<script defer=\"defer\" type=\"text/javascript\">");
                    sb.append(" window.onload = focusmessage;");
                    sb.append(" function focusmessage(){ ");
                    sb.append("document.getElementById(\"" + 
                            otherComponent.getId() + "BeginMsgTable\").focus();}</script>");
                    try {
                        pageContext.getOut().write(sb.toString());
                    } catch (IOException e) {
                        LogHelper.logInfo(logger, tracer, 
                                "Exception occurred while writing tag", null);
						LogHelper.logError(logger, tracer, e);
                    }                   
                }
            } else {
                LogHelper.logInfo(logger, tracer, "BaseUI was null. Accessibility " +
                    "features will be disabled", null);             
            }
            myContext.render(otherComponent);
            if (renderSkipEnd){
                sb.setLength(0);
                // Render the skip end if skip begin was also rendered
                sb.append("<a id=\"" + otherComponent.getId() + "EndMsgTable\"");
                sb.append(" name=\"" + otherComponent.getId() + "EndMsgTable\"");
                sb.append(" href=\"#" + otherComponent.getId() + "BeginMsgTable\"");
                sb.append(" tabindex=\"0\"");
                sb.append(" title=\"");
                String message = FormUtility.getResourceString(
                        BaseFormConstants.BASEBUNDLE, 
                        "Access.EndMessageTable", 
                        pageContext.getSession());
                String[] args = new String[]{};                       
                sb.append(MessageFormat.format(message, args));                            
                sb.append("\"></a>");
                try {
                    pageContext.getOut().write(sb.toString());
                } catch (IOException e) {
                    LogHelper.logInfo(logger, tracer, 
                            "Exception occurred while writing tag", null);
					LogHelper.logError(logger, tracer, e);
                }                   
            }
        }
        return super.doEndTag();
    }

    /**
     * Returns whether or not the component is the page error list
     * 
     * @return boolean
     */
    private boolean isErrorList() {
        return otherComponent instanceof GridLayout && 
                (Form.ERRORSEDIT.equals(otherComponent.getId()) || 
                "DetailsErrorEdit.Auction".equals(otherComponent.getId()));
    }
}