package com.sap.isa.ui.taglib;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspTagException;
import javax.servlet.jsp.tagext.TagSupport;

import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.ui.controllers.ActionForm;
import com.sapportals.htmlb.HTMLFragment;
import com.sapportals.htmlb.RadioButton;
import com.sapportals.htmlb.RadioButtonGroup;
import com.sapportals.htmlb.rendering.IPageContext;
import com.sapportals.htmlb.taglib.AbstractContainerTag;
import com.sapportals.htmlb.taglib.RendererContextTag;

/**
 * Title:
 * Description:
 * Copyright:    Copyright (c) 2002
 * Company:
 * @author
 * @version 1.0
 */


public final class RadioButtonTag extends AbstractContainerTag
{
    private IPageContext myContext;
    private RadioButton myRadioButton;
    private TagSupport parentTagHandler;
    private RadioButtonGroup myRadioButtonGroup;
    private String key;
    private String text;
    private String encode;
    private String tooltip;
    private String disabled;
    private String checked;
    private String id;

	/** 
	  *  The IsaLocation of the current Action.
	  *  This will be instantiated during perform and is always present.
	  */
	 protected IsaLocation log = IsaLocation.getInstance(RadioButtonTag.class.getName());

    public RadioButtonTag()
    {
    }

    public void setId(String id)
    {
        try {
            myContext = RendererContextTag.getTagRendererContext(this, pageContext);
        }catch (JspTagException ex) {
            log.debug("Error in setId() ", ex);
        }
        if (null != myContext) {
            Object obj =myContext.getAttribute(id);
            if (obj instanceof RadioButton) {
                myRadioButton = (RadioButton)obj;
                myRadioButton.setId(id);
            }
        }
        if(null == myRadioButton) {
            super.setId(id);
        }
    }
    public void setKey(String key)
    {
        this.key = key;
    }

    public void setText(String text)
    {
        this.text = text;
    }

    public void setEncode(String encode)
    {
        this.encode = encode;
    }

    public void setTooltip(String tooltip)
    {
        this.tooltip = tooltip;
    }

    public void setDisabled(String disabled)
    {
        this.disabled = disabled;
    }

    public int doStartTag()
        throws JspException
    {
        myContext = RendererContextTag.getTagRendererContext(this, pageContext);
        parentTagHandler = (TagSupport)getParent();
		ActionForm actionFrm = (ActionForm)pageContext.getAttribute(ActionForm.ACTIONFORM);
		if(actionFrm != null && 
			  myRadioButton != null &&
			  actionFrm.isControlApplicable(myRadioButton.getId()))  {

	        if(parentTagHandler instanceof RadioButtonGroupTag)
	        {
	            myRadioButtonGroup = ((RadioButtonGroupTag)parentTagHandler).getRadioButtonGroup();
	            if (myRadioButton == null){
	                myRadioButton = myRadioButtonGroup.addItem(key);
	            }
	            if(disabled != null)
	                //myRadioButton.setDisabled(convertToBoolean("radioButtonTag", "disabled", disabled));
	            if(encode != null)
	                myRadioButton.setEncode(convertToBoolean("radioButtonTag", "disabled", encode));
	            if(text != null)
	                myRadioButton.setText(text);
	            if(tooltip != null)
	                myRadioButton.setTooltip(tooltip);
	        } else
	        {
	            throw new JspException("A radioButton must be nested in a radioButtonGroup!");
	        }
			if(myRadioButton.getId() != null && myRadioButton.getId().length() > 0)
				pageContext.setAttribute(myRadioButton.getId(), myRadioButton);
		}
        return super.doStartTag();
    }

    public int doEndTag()
        throws JspException
    {
        myContext = RendererContextTag.getTagRendererContext(this, pageContext);
        String htmlContent = getBodyContent().getString();
        if(htmlContent != null && htmlContent.length() > 0)
        {
            HTMLFragment component = new HTMLFragment(htmlContent);
            myRadioButton.setElement(component);
        }
        return super.doEndTag();
    }

}
