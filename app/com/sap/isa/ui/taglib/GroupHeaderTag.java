package com.sap.isa.ui.taglib;

import java.util.Iterator;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspTagException;
import javax.servlet.jsp.tagext.Tag;

import com.sap.isa.core.logging.IsaLocation;
import com.sapportals.htmlb.Component;
import com.sapportals.htmlb.Group;
import com.sapportals.htmlb.HTMLFragment;
import com.sapportals.htmlb.rendering.IPageContext;
import com.sapportals.htmlb.taglib.AbstractContainerTag;
import com.sapportals.htmlb.taglib.RendererContextTag;

public class GroupHeaderTag extends AbstractContainerTag {

	private IPageContext  myContext = null;
	private Group 		 myGroup;
	private Component     component = null;

	/** 
	  *  The IsaLocation of the current Action.
	  *  This will be instantiated during perform and is always present.
	  */
	 protected static IsaLocation log = IsaLocation.getInstance(GroupHeaderTag.class.getName());

	public void setId(String id) {
		try  {
			myContext = RendererContextTag.getTagRendererContext(this, pageContext);
		}  catch(JspTagException jsExc)  {
			log.debug("Error ", jsExc);
		}
		if(myContext != null)  {
			component = (Component)myContext.getAttribute(id);
		}
	}

	public int doStartTag () throws JspException {

		myContext = RendererContextTag.getTagRendererContext(this, pageContext);
		Tag parentTagHandler = getParent();

		if ( parentTagHandler instanceof GroupTag ) {
			myGroup = ((GroupTag) parentTagHandler).getGroup();
		} else {
			throw new JspTagException ("A groupHeader must be nested in a group !");
		}

		if(component != null)  {
			return super.SKIP_BODY;
		}
		return super.doStartTag(); // BodyTagSupport: return EVAL_BODY_TAG;
	}

	public int doEndTag () throws JspException {

		// Add the body content (embedded in a HTML control) to the groupHeader
		// (it is rendered with the group).
		String htmlContent = getBodyContent() != null ? getBodyContent().getString() : null;
		if ( myGroup.getChildCount() > 0 ) {
			Iterator iterate = myGroup.iterator();
			while(iterate.hasNext())  {
				Object obj = iterate.next();
				iterate.remove();
			}
			//throw new JspTagException ("The group " + myGroup.getId()
			//			+ ": The groupHeader must be located before the groupBody !");
		}
		if(component != null)  {
			if(myGroup.getHeaderComponent() != null)  {
				myGroup.removeComponent(myGroup.getHeaderComponent());
			}
			myGroup.setHeaderComponent(component);
		}
		if ( htmlContent != null && htmlContent.length() > 0 ) {
			HTMLFragment htmlComponent = new HTMLFragment( htmlContent );
			if(null == myGroup.getHeaderComponent())  {
				myGroup.setHeaderComponent(htmlComponent);
			}
		}

		return super.doEndTag(); // BodyTagSupport: return EVAL_PAGE;
	}
}
