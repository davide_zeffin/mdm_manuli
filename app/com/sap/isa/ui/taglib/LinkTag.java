package com.sap.isa.ui.taglib;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspTagException;

import com.sap.isa.auction.util.AccessibilityUtil;
import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.ui.controllers.ActionForm;
import com.sapportals.htmlb.HTMLFragment;
import com.sapportals.htmlb.Link;
import com.sapportals.htmlb.enum.LinkFontSize;
import com.sapportals.htmlb.rendering.IPageContext;
import com.sapportals.htmlb.taglib.AbstractContainerTag;
import com.sapportals.htmlb.taglib.RendererContextTag;

/**
 * Title:
 * Description:
 * Copyright:    Copyright (c) 2002
 * Company:
 * @author
 * @version 1.0
 */

public class LinkTag extends AbstractContainerTag  {

	/** 
	  *  The IsaLocation of the current Action.
	  *  This will be instantiated during perform and is always present.
	  */
	 protected static IsaLocation log = IsaLocation.getInstance(LinkTag.class.getName());

    public LinkTag() {
    }

    IPageContext myContext = null;
    Link myLink = new Link("tempName", "");

    public void setId (String id) {
		try  {
			myContext = RendererContextTag.getTagRendererContext(this, pageContext);
		}  catch(JspTagException jsExc)  {
             log.debug("Error ", jsExc);
		}
		if(myContext != null)  {
			myLink = (Link)myContext.getAttribute(id);
		}
		if(null == myLink)  {
			myLink = new Link(id);
		}
		myLink.setId(id);
    }

    public void setText(String text) {
        myLink.addText(text);
        // Auto set tooltip if accessibility switch is on        
        if (AccessibilityUtil.isAccessibleContext(pageContext)){
            myLink.setTooltip(text);
        }
        
    }

    public void setOnClick(String onClick) {
        myLink.setOnClick(onClick);
    }

    public void setTarget(String target) {
        myLink.setTarget(target);
    }

    public void setTooltip(String tooltip) {
		myLink.setTooltip(tooltip);
    }

    public void setFontSize(String size)  {
        if(size.equalsIgnoreCase("SMALL"))
            myLink.setFontSize(LinkFontSize.SMALL);
        else
            myLink.setFontSize(LinkFontSize.STANDARD);
    }

    public void setReference(String reference) {
        myLink.setReference(reference);
    }

    public int doStartTag () throws JspException {

        myContext = RendererContextTag.getTagRendererContext(this, pageContext);

        if ( (myLink.getId() != null) && (myLink.getId().length() > 0) ) {
                pageContext.setAttribute(myLink.getId(), myLink);
        }

        return super.doStartTag(); // BodyTagSupport: return EVAL_BODY_TAG;
    }

    public int doEndTag () throws JspException {

		ActionForm actionFrm = (ActionForm)pageContext.getAttribute(ActionForm.ACTIONFORM);
		if((actionFrm == null) || (actionFrm != null && 
			  myLink != null &&
			  actionFrm.isControlApplicable(myLink.getId())))  {
	        myContext = RendererContextTag.getTagRendererContext(this, pageContext);
	
	        // Add the body content to the group and render the group
	        String htmlContent = getBodyContent().getString();
	        if ( htmlContent != null && htmlContent.length() > 0 ) {
	                myLink.addComponent( new HTMLFragment(htmlContent) );
	        }
	        myContext.render(myLink);
		}

        return super.doEndTag(); // BodyTagSupport: return EVAL_PAGE;
    }

}