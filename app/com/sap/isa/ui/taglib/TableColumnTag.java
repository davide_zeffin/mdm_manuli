package com.sap.isa.ui.taglib;

import com.sap.isa.ui.controllers.ActionForm;
import com.sapportals.htmlb.enum.CellHAlign;
import com.sapportals.htmlb.enum.CellVAlign;
import com.sapportals.htmlb.enum.TableColumnType;
import com.sapportals.htmlb.table.TableColumn;
import com.sapportals.htmlb.taglib.AbstractComponentTag;


/**
 * Title:
 * Description:
 * Copyright:    Copyright (c) 2002
 * Company:
 * @author
 * @version 1.0
 */

public class TableColumnTag extends AbstractComponentTag  {

  private TableColumn tableColumn;
  private String type;

  public TableColumnTag() {
  }

    public void setTooltip(String tooltipForColumnHeader){
        tableColumn.setTooltipForColumnHeader(tooltipForColumnHeader);
    }
    
  public void setId(String id)  {
      TableViewTag tableViewTag = (TableViewTag)this.getParent();
      tableColumn = tableViewTag.getColumn(id);
      if(tableColumn == null)  {
          tableColumn = new TableColumn(tableViewTag.getTableViewModel() , id);
		  tableColumn.setType(TableColumnType.USER);
      }
	  ActionForm actionFrm = (ActionForm)pageContext.getAttribute(ActionForm.ACTIONFORM);
	  if((actionFrm == null) || (actionFrm != null && 
		  tableColumn != null &&
		  actionFrm.isControlApplicable(tableColumn.getIdentifier())))  {
      
      		tableViewTag.addColumn(tableColumn);
	  }
      tableColumn.setIdentifier(id);
  }

  public void setTitle(String title)  {
      tableColumn.setTitle(title);
  }

  public void setWrapping(boolean wrap)  {
  	tableColumn.setWrapping(wrap);
  }
  
  public void setType(String typ)  {
      if(!typ.equals(""))
          tableColumn.setType((TableColumnType)convertToEnum("tableColumn", "type",
                              typ, "com.sapportals.htmlb.enum.TableColumnType", this));
      else
      	  tableColumn.setType(TableColumnType.TEXT);
  }

  public void setHalignment(String align)  {
      tableColumn.setCellHAlignment(-1 , (CellHAlign)convertToEnum("tableColumn", "cellHAlignment",
                              align, "com.sapportals.htmlb.enum.CellHAlign", this));
  }

  public void setValignment(String align)  {
      tableColumn.setCellVAlignment(-1 , (CellVAlign)convertToEnum("tableColumn", "cellVAlignment",
                              align, "com.sapportals.htmlb.enum.CellVAlign", this));
  }

  public void setWidth(String width)  {
      tableColumn.setWidth(width);
  }

  public int doStartTag() throws javax.servlet.jsp.JspException {
      TableViewTag tableTag = (TableViewTag)TableColumnTag.findAncestorWithClass(this , com.sap.isa.ui.taglib.TableViewTag.class);

      if(tableTag == null)  {
          throw new javax.servlet.jsp.JspException("TableColumn should be nested within TableView");
      }
      tableTag.setColumnsInJSP(true);
      return EVAL_BODY_INCLUDE;
  }

  public int doEndTag() throws javax.servlet.jsp.JspException {
      return EVAL_PAGE;
  }

}