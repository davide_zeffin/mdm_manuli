package com.sap.isa.ui.taglib;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspTagException;

import com.sap.isa.core.logging.IsaLocation;
import com.sapportals.htmlb.Component;
import com.sapportals.htmlb.rendering.IPageContext;
import com.sapportals.htmlb.taglib.AbstractContainerTag;
import com.sapportals.htmlb.taglib.RendererContextTag;

public class ComponentRowTag extends AbstractContainerTag {
	
	Component component;
	IPageContext myContext;
	
	/** 
	  *  The IsaLocation of the current Action.
	  *  This will be instantiated during perform and is always present.
	  */
	 protected static IsaLocation log = IsaLocation.getInstance(ComponentRowTag.class.getName());
	
	public void setId(String id)
	{
		try  {
			myContext = RendererContextTag.getTagRendererContext(this, pageContext);
		}  catch(JspTagException jsExc)  {
			log.debug("Error ", jsExc);
		}
		if(myContext != null)  {
			component = (Component)myContext.getAttribute(id);
			if(component != null)
				component.setId(id);
		}  else {
			component = null;
		}
	}
	
	public int doEndTag()
		throws JspException
	{
		if(component != null)
			myContext.render(component);
		return super.doEndTag();
	}
}
