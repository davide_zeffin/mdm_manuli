package com.sap.isa.ui.taglib;

import java.util.Iterator;
import java.util.Vector;

import javax.servlet.jsp.JspException;

import com.sap.isa.auction.logging.CategoryProvider;
import com.sap.isa.auction.logging.LogHelper;
import com.sap.isa.auction.util.AccessibilityUtil;
import com.sap.isa.ui.components.table.ExtTableViewModel;
import com.sap.isa.ui.controllers.ActionForm;
import com.sap.isa.ui.renderer.TableCellRenderer;
import com.sap.tc.logging.Category;
import com.sap.tc.logging.Location;
import com.sapportals.htmlb.TextView;
import com.sapportals.htmlb.enum.BrowserType;
import com.sapportals.htmlb.enum.TableNavigationMode;
import com.sapportals.htmlb.enum.TableSelectionMode;
import com.sapportals.htmlb.enum.TableViewDesign;
import com.sapportals.htmlb.enum.TextViewDesign;
import com.sapportals.htmlb.rendering.IPageContext;
import com.sapportals.htmlb.rendering.RendererManager;
import com.sapportals.htmlb.table.TableColumn;
import com.sapportals.htmlb.table.TableView;
import com.sapportals.htmlb.table.TableViewModel;
import com.sapportals.htmlb.taglib.AbstractComponentTag;
import com.sapportals.htmlb.taglib.RendererContextTag;

/**
 * Title:
 * Description:
 * Copyright:    Copyright (c) 2002
 * Company:
 * @author
 * @version 1.0
 */

public class TableViewTag extends AbstractComponentTag  {

    private IPageContext  myContext;
    private TableView     tableView;
    private boolean columnsInJsp = false;
    private Vector jspColumns = new Vector();
    private boolean showEmptyTable = false;
    private String emptyTableTxt = "";

    protected static Category logger = CategoryProvider.getUICategory();
    protected static Location tracer = Location.getLocation(TableViewTag.class);
    public TableViewTag() {
    }

    public void setId (String id) {
        try  {
            myContext = RendererContextTag.getTagRendererContext(this, pageContext);
        } catch(Exception exc)  {
            LogHelper.logError(logger, tracer, exc);
        }
        if(null != myContext)  {
            try  {
                tableView = (TableView)myContext.getAttribute(id);
            }catch(Exception exc)  {
				LogHelper.logError(logger, tracer, exc);
            }
        }
        if(null == tableView)  {
            tableView = new TableView(id);
        } else  {
            tableView.setId(id);
        }
    }

    public void setModel(String modelId)  {
        TableViewModel model = null;
        try  {
            model = (TableViewModel)getModel(tableView.getId() , modelId);
        }  catch(Exception exc)  {
			LogHelper.logError(logger, tracer, exc);

        }
        if(model == null)  {
            model = (TableViewModel)myContext.getAttribute(modelId);
        }
        if(null == model)  {
            throw new IllegalArgumentException("Tag tableView attribute model"
						+ ": Cannot access bean property " + modelId + " in page context");
        }
        tableView.setModel(model);
    }

    public void setColumnsInJSP(boolean flg)  {
        columnsInJsp = flg;
    }

    public TableViewModel getTableViewModel()  {
        return tableView.getModel();
    }

    public void setDesign (String design) {
        tableView.setDesign( (TableViewDesign) convertToEnum("tableView", "design",
                                                                              design, "com.sapportals.htmlb.enum.TableViewDesign", this) );
    }

    public void setHeaderVisible (String headerVisible) {
        tableView.setHeaderVisible( convertToBoolean("tableView", "headerVisible", headerVisible) );
    }

    public void setFooterVisible (String footerVisible) {
        tableView.setFooterVisible( convertToBoolean("tableView", "footerVisible", footerVisible) );
    }

    public void setFillUpEmptyRows (String fillUpEmptyRows) {
        tableView.setFillUpEmptyRows( convertToBoolean("tableView", "fillUpEmptyRows", fillUpEmptyRows) );
    }
    
    public void setShowAll(boolean bShowAll)  {
    	if(bShowAll)  {
    		tableView.setVisibleRowCount(tableView.getModel().getRowCount());
    		tableView.setRowCount(tableView.getModel().getRowCount());
    	} 
    }

    public void setNavigationMode (String navigationMode) {
        tableView.setNavigationMode( (TableNavigationMode) convertToEnum("tableView", "navigationMode", navigationMode,
                                                                                      "com.sapportals.htmlb.enum.TableNavigationMode", this) );
    }

    public void setSelectionMode (String selectionMode) {
        tableView.setSelectionMode( (TableSelectionMode) convertToEnum("tableView", "selectionMode", selectionMode,
                                                                                      "com.sapportals.htmlb.enum.TableSelectionMode", this) );
    }

    public void setHeaderText (String headerText) {
        tableView.setHeaderText(headerText);
    }

    public void setOnNavigate (String onNavigate) {
        tableView.setOnNavigate(onNavigate);
    }

    public void setVisibleFirstRow (String visibleFirstRow) {
        tableView.setVisibleFirstRow( convertToInteger("tableView", "visibleFirstRow", visibleFirstRow) );
    }

    public void setVisibleRowCount (String visibleRowCount) {
        tableView.setVisibleRowCount( convertToInteger("tableView", "visibleRowCount", visibleRowCount) );
        tableView.setRowCount(tableView.getVisibleRowCount());
    }

    public void setRowCount (String rowCount) {
        tableView.setRowCount( convertToInteger("tableView", "rowCount", rowCount) );
    }

    public void setColumnCount (String columnCount) {
        tableView.setColumnCount( convertToInteger("tableView", "columnCount", columnCount) );
    }

    public void setWidth(String width)  {
        tableView.setWidth(width);
    }

    public void setEmptyTableText(String txt)  {
        this.emptyTableTxt = txt;
    }

    public void setShowEmptyTable(boolean flag)  {
        this.showEmptyTable = flag;
    }

    public int doStartTag () throws JspException {
        if ( (tableView.getId() != null) && (tableView.getId().length() > 0) ) {
                pageContext.setAttribute(tableView.getId(), tableView);
        }
        tableView.setUserTypeCellRenderer(new TableCellRenderer(getTableViewModel()));
        return EVAL_BODY_INCLUDE;
    }

    public int doEndTag () throws JspException {
        

		ActionForm actionFrm = (ActionForm)pageContext.getAttribute(ActionForm.ACTIONFORM);
		if(actionFrm != null)  {
			if(actionFrm.getTableViewVisibleRowCount() > 0)  {
				tableView.setVisibleRowCount(actionFrm.getTableViewVisibleRowCount());
			}
		}
		updateInformationFromTableModel();
        if(showEmptyTable || tableView.getModel().getRowCount() > 0)  {
            // ACC Support
            // Apply custom renderer if acc switch is on. This is for group header
            // association
            if (AccessibilityUtil.isAccessibleContext(pageContext)){
                RendererManager.addRenderer
                        (TableView.UI_ID, 
                        BrowserType.DEFAULT, 
                        com.sap.isa.ui.renderer.AccessibleTableRenderer.class);
            }
            
            myContext.render(tableView);
        }  else  {
            TextView view = new TextView();
            view.setDesign(TextViewDesign.EMPHASIZED);
            view.setText(emptyTableTxt);
            myContext.render(view);
        }
        return EVAL_PAGE;
    }

    public TableColumn getColumn(String id)  {
        ExtTableViewModel model = (ExtTableViewModel)tableView.getModel();
        TableColumn column = model.getColumn(id);
        return column;
    }

    public void addColumn(TableColumn column)  {
        jspColumns.add(column);
    }

    private void updateInformationFromTableModel()  {
        setFirstRowVisible();
        updateColumnsToTableView();
        setRowSelection();
    }

    private void updateColumnsToTableView()  {
        if(columnsInJsp)  {
            ExtTableViewModel model = (ExtTableViewModel)tableView.getModel();
            model.removeAllColumns();
            Iterator iteratorColumns = jspColumns.iterator();
            TableColumn column = null;
            while(iteratorColumns.hasNext())  {
                column = (TableColumn)iteratorColumns.next();
                model.addColumn(column);
            }
        }
    }

    private void setRowSelection()  {
        ExtTableViewModel model = (ExtTableViewModel)getTableViewModel();
        int visibleRowCount = tableView.getVisibleFirstRow() > 0 ? tableView.getVisibleRowCount() : model.getVisibleRowCount();
        for(int i = model.getVisibleFirstRow() ; i <= visibleRowCount + model.getVisibleFirstRow(); i++)  {
            tableView.setRowSelectable(i , true);
            tableView.selectRow(i , model.isRowSelected(i));
            tableView.setRowSelectable(1 , model.isRowSelectable(i));
        }
    }

    private void setFirstRowVisible()  {
        ExtTableViewModel model = (ExtTableViewModel)getTableViewModel();
        tableView.setVisibleFirstRow(model.getVisibleFirstRow());
        if(null == tableView.getSelectionMode())
            tableView.setSelectionMode(model.getTableSelectionMode());
        if(tableView.getVisibleRowCount() == 0)  {
           tableView.setVisibleFirstRow(model.getVisibleRowCount());
        }  else  {
            model.setVisibleRowCount(tableView.getVisibleRowCount());
        }
    }    
}
