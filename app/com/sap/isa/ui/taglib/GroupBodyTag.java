/*
 * Created on Sep 29, 2003
 *
 * To change the template for this generated file go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
package com.sap.isa.ui.taglib;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.tagext.Tag;

import com.sapportals.htmlb.Group;
import com.sapportals.htmlb.HTMLFragment;
import com.sapportals.htmlb.rendering.IPageContext;
import com.sapportals.htmlb.taglib.AbstractContainerTag;
import com.sapportals.htmlb.taglib.RendererContextTag;


/**
 * @author I803067
 *
 * To change the template for this generated type comment go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
public class GroupBodyTag extends AbstractContainerTag {
	private IPageContext  myContext = null;

	private Group         myGroup    = null;

	public int doStartTag () throws JspException {

		myContext = RendererContextTag.getTagRendererContext(this, pageContext);
		Tag parentTagHandler = getParent();

		if ( parentTagHandler instanceof GroupTag ) {
			GroupTag groupTagHandler = (GroupTag) parentTagHandler;
			myGroup = groupTagHandler.getGroup();
		} else {
			throw new JspException ("A groupBody must be nested in an group !");
		}

		return super.doStartTag(); // BodyTagSupport: return EVAL_BODY_TAG;
	}

	public int doEndTag () throws JspException {

		// Add the body content (embedded in a HTML control) as body to the group
		// (it is rendered with the group).
		String htmlContent = getBodyContent().getString();
		if ( htmlContent != null && htmlContent.length() > 0 ) {
			HTMLFragment component = new HTMLFragment( htmlContent );
			myGroup.addComponent(component);
		}

		return super.doEndTag(); // BodyTagSupport: return EVAL_PAGE;
	}
}
