package com.sap.isa.ui.taglib;

import javax.servlet.jsp.tagext.TagData;
import javax.servlet.jsp.tagext.TagExtraInfo;
import javax.servlet.jsp.tagext.VariableInfo;


/**
 * Title:
 * Description:
 * Copyright:    Copyright (c) 2002
 * Company:
 * @author
 * @version 1.0
 */

public class PageLoadingTEI extends TagExtraInfo  {

  public PageLoadingTEI() {
  }
  public VariableInfo[] getVariableInfo(TagData data) {

    VariableInfo info1 = new VariableInfo(
       "actionForm",
        "com.sap.isa.ui.controllers.ActionForm",
        true,
        VariableInfo.AT_END);
     
	VariableInfo info2 = new VariableInfo(
		PageLoadingTag.BASEUI,
		PageLoadingTag.BASEUI_FQCN,
		true,
		VariableInfo.AT_END);    

    VariableInfo[] info = { info1, info2 } ;

    return info;
  }
}
