/*
 * Created on Oct 13, 2003
 *
 * To change the template for this generated file go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
package com.sap.isa.ui.taglib;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspTagException;
import javax.servlet.jsp.tagext.TagSupport;

import com.sap.isa.core.logging.IsaLocation;
import com.sapportals.htmlb.MessageBar;
import com.sapportals.htmlb.enum.MessageType;
import com.sapportals.htmlb.rendering.IPageContext;
import com.sapportals.htmlb.taglib.AbstractComponentTag;
import com.sapportals.htmlb.taglib.RendererContextTag;

/**
 * @author I803067
 *
 * To change the template for this generated type comment go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
public class MessageBarTag extends AbstractComponentTag  {

	private IPageContext myContext;
	private MessageBar messageBar;

	/** 
	  *  The IsaLocation of the current Action.
	  *  This will be instantiated during perform and is always present.
	  */
	 protected static IsaLocation log = IsaLocation.getInstance(MessageBarTag.class.getName());

	public void setId (String id) {
		try  {
			myContext = RendererContextTag.getTagRendererContext(this, pageContext);
		}  catch(JspTagException jsExc)  {
			log.debug("Error ", jsExc);
		}
		if(myContext != null)  {
			try{
			messageBar = (MessageBar)myContext.getAttribute(id);
			}catch(Exception ex) {
				log.debug("Error ", ex);
//				  throw ex;
			}
		}
		if(null == messageBar)  {
			messageBar = new MessageBar(id);
		}
		messageBar.setId(id);
	}
	
	public void setMessageText (String text)
	{
	  messageBar.setMessageText(text);
	}

	public void setMessageType (String messageType)
	{
	  messageBar.setMessageType((MessageType) convertToEnum("messageBar", "messageType", messageType,
								 "com.sapportals.htmlb.enum.MessageType", this));
	}

	public int doStartTag () throws JspException {

		myContext = RendererContextTag.getTagRendererContext(this, pageContext);
		if ( (messageBar.getId() != null) && (messageBar.getId().length() > 0) ) {
				// Put the TextView instance into the page context
				// -> It is available as scripting variable (default scope: PageContext.PAGE_SCOPE)
				pageContext.setAttribute(messageBar.getId(), messageBar);
		}

		return TagSupport.EVAL_BODY_INCLUDE; // -> Scripting in the body
	}

	public int doEndTag () throws JspException {
		myContext.render(messageBar);
		return super.doEndTag(); // BodyTagSupport: return EVAL_PAGE;
	}

}
