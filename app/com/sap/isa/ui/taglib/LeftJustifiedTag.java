/*
 * Created on Oct 20, 2004
 *
 * To change the template for this generated file go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
package com.sap.isa.ui.taglib;

import javax.servlet.jsp.JspException;

import com.sap.isa.core.logging.IsaLocation;
import com.sapportals.htmlb.taglib.AbstractComponentTag;

/**
 * @author I803067
 *
 * To change the template for this generated type comment go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
public class LeftJustifiedTag extends AbstractComponentTag {

	/** 
	  *  The IsaLocation of the current Action.
	  *  This will be instantiated during perform and is always present.
	  */
	 protected IsaLocation log = IsaLocation.getInstance(LeftJustifiedTag.class.getName());

	
	/* (non-Javadoc)
	 * @see javax.servlet.jsp.tagext.Tag#doEndTag()
	 */
	public int doEndTag() throws JspException {
		try  {
			pageContext.getOut().write("</td>");
		}  catch(Exception exc)  {
			log.debug("Error in doEndTag() ", exc);
		}
		return EVAL_PAGE;
	}

	/* (non-Javadoc)
	 * @see javax.servlet.jsp.tagext.Tag#doStartTag()
	 */
	public int doStartTag() throws JspException {
		StringBuffer buff = new StringBuffer();
		buff.append("<td class=\"sapTblBtnRow\" align=\"left\" valign=bottom>");
		try  {
			pageContext.getOut().write(buff.toString());
		}  catch(Exception exc)  {
			log.debug("Error in doStartTag() ", exc);
		}
		return EVAL_BODY_INCLUDE;
	}

}
