package com.sap.isa.ui.taglib;

import java.io.IOException;

import javax.servlet.jsp.JspException;

import com.sap.isa.auction.logging.CategoryProvider;
import com.sap.isa.auction.logging.LogHelper;
import com.sap.isa.auction.util.AccessibilityUtil;
import com.sap.isa.ui.uiclass.BaseUI;
import com.sap.tc.logging.Category;
import com.sap.tc.logging.Location;
import com.sapportals.htmlb.taglib.AbstractComponentTag;

/**
 * Title:			Internet Sales Selling Via eBay
 * Description: 	A convenience tag that checks the status of the accessibility switch 
 * 					and writes the value of the name and value property as an html
 * 					name value pair. If the accessibility switch is not on, the default is
 * 					an empty string. 
 * @Copyright: 		Copyright (c) 2004
 * Company:			SAPLabs LLC
 * Created on:		Sep 16, 2004
 */
public class AccessiblePropertyTag extends AbstractComponentTag {

	private String name;
	private String defaultValue = "";
	private String value;
	
    private static final Category logger = CategoryProvider.getUICategory();
    private static final Location tracer = 
            Location.getLocation(AccessiblePropertyTag.class);
            
    /* (non-Javadoc)
     * @see javax.servlet.jsp.tagext.Tag#doStartTag()
     */
    public int doStartTag() throws JspException {
    	if ((null == getName() || null == getValue()) || 
    			"".equals(getName()) || "".equals(getValue())){
    		throw new JspException("name and value property for " +    			"accessibleProperty tag cannot be empty");
    	}
        String s = AccessibilityUtil.isAccessibleContext(pageContext) ?
                composeHTML() : 
                composeDefaultValue() ;
            pageContext.setAttribute(getId(), s);
            try {
                pageContext.getOut().write(s);
            } catch (IOException e) {
                LogHelper.logInfo(logger, tracer, 
                    "Exception occurred during accessibility property output",
                     new Object[]{e});
            }
		return EVAL_BODY_INCLUDE;
    }

    /**
     * @return
     */
    private String composeHTML() {
        StringBuffer sb = new StringBuffer();
        sb.append(getName() + "=\"" + getValue() + "\"");
        return sb.toString();
    }
    
    /**
     * 
     * @return
     */
    private String composeDefaultValue(){
    	StringBuffer sb = new StringBuffer();
    	sb.append(getDefaultValue());
    	return sb.toString();
    }

    /**
     * @return
     */
    public String getName() {
        return name;
    }

    /**
     * @return
     */
    public String getValue() {
        return value;
    }

    /**
     * @param string
     */
    public void setName(String string) {
        name = string;
    }

    /**
     * @param string
     */
    public void setValue(String string) {
        value = string;
    }

    /**
     * @return
     */
    public String getDefaultValue() {
        return defaultValue;
    }

    /**
     * Sets default propertyValue if accessibility switch is false. Otherwise the default
     * value is always an empty string
     * @param string
     */
    public void setDefaultValue(String string) {
        defaultValue = string;
    }

}
