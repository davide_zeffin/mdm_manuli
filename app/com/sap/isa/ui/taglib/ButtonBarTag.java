package com.sap.isa.ui.taglib;

import com.sap.isa.core.logging.IsaLocation;
import com.sapportals.htmlb.taglib.AbstractComponentTag;

/**
 * Title:
 * Description:
 * Copyright:    Copyright (c) 2003
 * Company:
 * @author
 * @version 1.0
 */

public class ButtonBarTag extends AbstractComponentTag  {

	/** 
	  *  The IsaLocation of the current Action.
	  *  This will be instantiated during perform and is always present.
	  */
	 protected IsaLocation log = IsaLocation.getInstance(ButtonBarTag.class.getName());

  public ButtonBarTag() {
  }

    public int doStartTag() throws javax.servlet.jsp.JspException {

        try  {
            pageContext.getOut().write(headerContent());
        }  catch(Exception exc)  {
              log.debug("Error in doStartTag() ", exc);
        }
        return EVAL_BODY_INCLUDE;
    }

    public int doEndTag() throws javax.servlet.jsp.JspException {
        try  {
            pageContext.getOut().write(footerContent());
        }  catch(Exception exc)  {
			log.debug("Error in doEndTag() ", exc);
        }
        return EVAL_PAGE;
    }

    private String headerContent()  {
        StringBuffer buff = new StringBuffer();
        buff.append("<div><div class=\"sapTlbLvl2\"><table class=\"sapTlb\" width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">");
        buff.append("<tr valign=\"top\"><td class=\"sapTblBtnRow\" width=\"*\"  align=\"left\">");
        buff.append("<span id=\"DETL3_bgrp\"></span></td>");
        buff.append("<td class=\"sapTblBtnRow\"  align=\"right\" valign=\"middle\">");
        return buff.toString();
    }

    private String footerContent()  {
        StringBuffer buff = new StringBuffer();
        buff.append("&nbsp&nbsp;</td></tr></table></div></div>");
        return buff.toString();
    }
}