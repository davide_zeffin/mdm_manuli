package com.sap.isa.ui.taglib;

import javax.servlet.jsp.tagext.TagSupport;

import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.core.util.WebUtil;

/**
 * Title:
 * Description:
 * Copyright:    Copyright (c) 2002
 * Company:
 * @author
 * @version 1.0
 */

public class HTMLPageLoading extends TagSupport  {
	
	/** 
	  *  The IsaLocation of the current Action.
	  *  This will be instantiated during perform and is always present.
	  */
	 protected IsaLocation log = IsaLocation.getInstance(HTMLPageLoading.class.getName());

  public HTMLPageLoading() {
  }

    public int doStartTag() throws javax.servlet.jsp.JspException {

        try  {
            pageContext.getOut().write(getLoadingScript());
        }  catch(Exception exc)  {
             log.debug("Error in doStartTag() ", exc);
        }
        return EVAL_BODY_INCLUDE;
    }

    public int doEndTag() throws javax.servlet.jsp.JspException {

        try  {

         }  catch(Exception exc)  {
			log.debug("Error in doEndTag() ", exc);
        }
        return EVAL_PAGE;
    }

    private String getLoadingScript()  {
        StringBuffer buffer = new StringBuffer();
        buffer.append("<div id='_our_loading'>");
        //buffer.append(WebUtil.translate(pageContext , UIConstants.PAGELOADING , null));
        buffer.append("..........<img width='16' height='16' src='");
        buffer.append(WebUtil.getMimeURL(pageContext , "/auction/mimes/images/clockon.gif"));
        buffer.append("'></img></div>");
        return buffer.toString();
    }

    private String getLoadedScript()  {
        StringBuffer buffer = new StringBuffer();
        buffer.append("<script language='JavaScript'>eval(\"document.getElementById('_our_loading').style.display = 'none'\");</script>");
        return buffer.toString();
    }

}