package com.sap.isa.ui.taglib;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.tagext.TagSupport;

import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.core.util.table.TableColumn;
import com.sap.isa.ui.controllers.ActionForm;
import com.sapportals.htmlb.Checkbox;
import com.sapportals.htmlb.CheckboxGroup;
import com.sapportals.htmlb.HTMLFragment;
import com.sapportals.htmlb.rendering.IPageContext;
import com.sapportals.htmlb.taglib.AbstractContainerTag;
import com.sapportals.htmlb.taglib.CheckboxGroupTag;
import com.sapportals.htmlb.taglib.RendererContextTag;


/**
 * Title:
 * Description:
 * Copyright:    Copyright (c) 2002
 * Company:
 * @author
 * @version 1.0
 */

public class CheckboxTag extends AbstractContainerTag  {

  private Checkbox checkbox;
  private IPageContext myContext;
  private CheckboxGroup checkboxGroup;
  
  /** 
	*  The IsaLocation of the current Action.
	*  This will be instantiated during perform and is always present.
	*/
   protected IsaLocation log = IsaLocation.getInstance(CheckboxTag.class.getName());

  
  public CheckboxTag() {
  }

  public void setId(String id)  {
      try  {
          myContext = (IPageContext)RendererContextTag.getTagRendererContext(this , pageContext);
      }  catch(Exception exc)  {
            log.debug("Error in setId: ", exc);
      }
      if(null != myContext)  {
          checkbox = (Checkbox)myContext.getAttribute(id);
      }
      if(null == checkbox)  {
          checkbox = new Checkbox(id);
      }
      checkbox.setId(id);
  }

  public void setText (String text) {
      checkbox.setText(text);
  }

  public void setEncode (String encode) {
      checkbox.setEncode( convertToBoolean("checkbox", "encode", encode) );
  }

  public void setTooltip(String tooltip) {
      checkbox.setTooltip(tooltip);
  }
  
  public void setOnClick(String onClick)  {
  		checkbox.setOnClick(onClick);
  		TableColumn col;
  }

  public void setDisabled(String disabled) {
      //checkbox.setDisabled( convertToBoolean("checkbox", "disabled", disabled) );
  }

  public void setOnClientClick(String clientClickEvent) {
      checkbox.setOnClientClick(clientClickEvent);
  }

  public void setChecked(String checked) {
          checkbox.setChecked( convertToBoolean("checkbox", "checked", checked) );
  }

  public int doStartTag () throws JspException {

      myContext = RendererContextTag.getTagRendererContext(this, pageContext);

      TagSupport parentTagHandler =  (TagSupport) getParent();
      if ( parentTagHandler instanceof CheckboxGroupTag ) {
          // Add the checkbox to an enclosing checkboxGroup
              checkboxGroup = ((CheckboxGroupTag) parentTagHandler).getCheckboxGroup();
              checkboxGroup.addComponent(checkbox);
      }// else: a Checkbox which is not nested in a CheckboxGroup

      if ( (checkbox.getId() != null) && (checkbox.getId().length() > 0 ) ) {
              pageContext.setAttribute(checkbox.getId(), checkbox);
      }

      return super.doStartTag(); // BodyTagSupport: return EVAL_BODY_TAG;
  }

  public int doEndTag () throws JspException {

	  ActionForm actionFrm = (ActionForm)pageContext.getAttribute(ActionForm.ACTIONFORM);
	  if((actionFrm == null) || (actionFrm != null && 
			checkbox != null &&
			actionFrm.isControlApplicable(checkbox.getId())))  {

				myContext = RendererContextTag.getTagRendererContext(this, pageContext);

				// Add the body content to the checkbox
				String htmlContent = getBodyContent().getString().trim();
				if ( htmlContent != null && htmlContent.length() > 0 ) {
						checkbox.setElement( new HTMLFragment(htmlContent) );
				}
				if ( checkboxGroup == null ) {
						myContext.render(checkbox);
				}
	  }
      return super.doEndTag(); // BodyTagSupport: return EVAL_PAGE;
  }

}
