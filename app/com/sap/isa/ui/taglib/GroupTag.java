/*
 * Created on Sep 29, 2003
 *
 * To change the template for this generated file go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
package com.sap.isa.ui.taglib;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspTagException;

import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.ui.controllers.ActionForm;
import com.sapportals.htmlb.Group;
import com.sapportals.htmlb.HTMLFragment;
import com.sapportals.htmlb.enum.GroupDesign;
import com.sapportals.htmlb.rendering.IPageContext;
import com.sapportals.htmlb.taglib.AbstractContainerTag;
import com.sapportals.htmlb.taglib.RendererContextTag;


public class GroupTag extends AbstractContainerTag  {
	// -------------------------------------------------------------------------
	// Properties

	// Implicit properties: The context and the group instance which holds the properties
	Group myGroup = new Group();
	IPageContext myContext;
	/** 
	  *  The IsaLocation of the current Action.
	  *  This will be instantiated during perform and is always present.
	  */
	 protected static IsaLocation log = IsaLocation.getInstance(GroupTag.class.getName());

	// -------------------------------------------------------------------------
	// Set/get methods for the properties

	public void setId(String id) {
		try  {
			myContext = RendererContextTag.getTagRendererContext(this, pageContext);
		}  catch(JspTagException jsExc)  {
			log.debug("Error ", jsExc);
		}
		if(myContext != null)  {
			myGroup = (Group)myContext.getAttribute(id);
		}
		if(null == myGroup)  {
			myGroup = new Group();
		}
		myGroup.setId(id);
	}
	public void setDesign (String design) {
		myGroup.setDesign( (GroupDesign) convertToEnum("group", "design", design,
										  "com.sapportals.htmlb.enum.GroupDesign", this) );
	}
	public void setWidth (String width) {
		myGroup.setWidth(width);
	}
	public void setTitle (String title) {
		myGroup.setTitle(title);
	}
	public void setTooltip (String tooltip) {
		myGroup.setTooltip(tooltip);
	}

	public Group getGroup() {
		return this.myGroup;
	}

	public int doStartTag () throws JspException {

		myContext = RendererContextTag.getTagRendererContext(this, pageContext);

		if ( (myGroup.getId() != null) && (myGroup.getId().length() > 0) ) {
			pageContext.setAttribute(myGroup.getId(), myGroup);
		}

		return super.doStartTag(); // BodyTagSupport: return EVAL_BODY_TAG;
	}

	public int doEndTag () throws JspException {

		myContext = RendererContextTag.getTagRendererContext(this, pageContext);

		ActionForm actionFrm = (ActionForm)pageContext.getAttribute(ActionForm.ACTIONFORM);
		if(actionFrm != null && 
			  myGroup != null &&
			  actionFrm.isControlApplicable(myGroup.getId()))  {
			if ( myGroup.getChildCount() == 0 ) { // no groupBody
				if ( myGroup.getHeaderComponent() == null ) { // no groupHeader
					String htmlContent = getBodyContent().getString();
					if ( htmlContent != null && htmlContent.length() > 0 ) {
						HTMLFragment component = new HTMLFragment( htmlContent );
						myGroup.addComponent(component);
					} else {
						throw new JspTagException ("The group " + myGroup.getId()
													+ " must not be empty !");
					}
				} else {
					throw new JspTagException ("The group " + myGroup.getId()
						+ " requires a groupBody because you have used a groupHeader !");
				}
			}
	
			myContext.render(myGroup);
		}

		return super.doEndTag(); // BodyTagSupport: return EVAL_PAGE;
	}

}
