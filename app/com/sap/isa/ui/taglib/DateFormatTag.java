package com.sap.isa.ui.taglib;

import java.io.IOException;
import java.text.MessageFormat;

import javax.servlet.jsp.JspException;

import com.sap.isa.auction.actionforms.BaseFormConstants;
import com.sap.isa.auction.logging.CategoryProvider;
import com.sap.isa.auction.util.AccessibilityUtil;
import com.sap.isa.auction.util.FormUtility;
import com.sap.tc.logging.Category;
import com.sap.tc.logging.Location;
import com.sapportals.htmlb.taglib.AbstractComponentTag;

/**
 * Title:           
 * Description: Outputs localized date format
 * @Copyright:      Copyright (c) 2004
 * Company:         SAPLabs LLC
 * Created on:      Jan 11, 2005
 */

public class DateFormatTag extends AbstractComponentTag {
    
    private static Category tracer = CategoryProvider.getUICategory();
    private static Location logger = Location.getLocation(DateFormatTag.class);
    
    /* (non-Javadoc)
     * @see javax.servlet.jsp.tagext.Tag#doStartTag()
     */
    public int doStartTag() throws JspException {
        final String METHOD = "doStartTag";
        logger.entering(METHOD);
        
        java.text.SimpleDateFormat f = (java.text.SimpleDateFormat) 
                java.text.DateFormat.getDateInstance
                    (java.text.DateFormat.SHORT, 
                    pageContext.getRequest().getLocale());
        
        if (null != f){
            try {
                String df = FormUtility.getResourceString
                        (BaseFormConstants.BASEBUNDLE , 
                        "Access.DateFormat", 
                        pageContext.getSession());
                String[] args = {new String(f.toLocalizedPattern())};  
                StringBuffer sb = new StringBuffer();
                sb.append("<span tabindex=\"0\" class=\"urTxtStd\"");
                sb.append("style=\"white-space:nowrap\"");
                sb.append("title=\"");
                sb.append(MessageFormat.format(df, args));                sb.append("\">");
                sb.append(f.toLocalizedPattern());
                sb.append("</span>");
                pageContext.getOut().write(sb.toString());
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }

        logger.exiting(METHOD);
        return SKIP_BODY;
    }
    
}
