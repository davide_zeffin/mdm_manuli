package com.sap.isa.ui.taglib;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspTagException;

import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.core.util.JspUtil;
import com.sap.isa.ui.controllers.ActionForm;
import com.sapportals.htmlb.TextEdit;
import com.sapportals.htmlb.enum.TextWrapping;
import com.sapportals.htmlb.rendering.IPageContext;
import com.sapportals.htmlb.taglib.AbstractComponentTag;
import com.sapportals.htmlb.taglib.RendererContextTag;

/**
 * Title:
 * Description:
 * Copyright:    Copyright (c) 2002
 * Company:
 * @author
 * @version 1.0
 */

public final class TextEditTag extends AbstractComponentTag
{
    IPageContext myContext;
    TextEdit myTextEdit;
	/** 
	  *  The IsaLocation of the current Action.
	  *  This will be instantiated during perform and is always present.
	  */
	 protected IsaLocation log = IsaLocation.getInstance(TextEditTag.class.getName());

    public TextEditTag()
    {
        myContext = null;
    }

    public void setId(String id)
    {
        try  {
            myContext = RendererContextTag.getTagRendererContext(this, pageContext);
        }  catch(JspTagException jsExc)  {
        	log.debug("Error ", jsExc);

        }
        if(myContext != null)  {
            myTextEdit = (TextEdit)myContext.getAttribute(id);
        }
        if(null == myTextEdit)  {
            myTextEdit = new TextEdit(id);
        }
        myTextEdit.setId(id);
    }

    public void setText(String text)
    {
        myTextEdit.setText(JspUtil.encodeHtml(text));
    }

    public void setCols(String cols)
    {
        myTextEdit.setCols(convertToInteger("textEdit", "cols", cols));
    }

    public void setRows(String rows)
    {
        myTextEdit.setRows(convertToInteger("textEdit", "rows", rows));
    }

    public void setTooltip(String tooltip)
    {
        myTextEdit.setTooltip(tooltip);
    }

    public void setWrapping(String wrapping)
    {
        myTextEdit.setWrapping((TextWrapping)convertToEnum("textEdit", "wrapping", wrapping, "com.sapportals.htmlb.enum.TextWrapping", this));
    }

    public int doStartTag()
        throws JspException
    {
        myContext = RendererContextTag.getTagRendererContext(this, pageContext);
        if(myTextEdit.getId() != null && myTextEdit.getId().length() > 0)
            pageContext.setAttribute(myTextEdit.getId(), myTextEdit);
        return 1;
    }

    public int doEndTag()
        throws JspException
    {
		ActionForm actionFrm = (ActionForm)pageContext.getAttribute(ActionForm.ACTIONFORM);
		if((actionFrm == null)  || (actionFrm != null && 
				myTextEdit != null &&
			  actionFrm.isControlApplicable(myTextEdit.getId())))  {
        	myContext.render(myTextEdit);
		}
        return super.doEndTag();
    }
}
