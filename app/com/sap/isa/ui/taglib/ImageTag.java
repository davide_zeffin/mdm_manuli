package com.sap.isa.ui.taglib;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspTagException;
import javax.servlet.jsp.tagext.TagSupport;

import com.sap.isa.auction.logging.CategoryProvider;
import com.sap.isa.auction.logging.LogHelper;
import com.sap.isa.ui.controllers.ActionForm;
import com.sap.tc.logging.Category;
import com.sap.tc.logging.Location;
import com.sapportals.htmlb.Image;
import com.sapportals.htmlb.ImageMap;
import com.sapportals.htmlb.hovermenu.HoverMenu;
import com.sapportals.htmlb.rendering.IPageContext;
import com.sapportals.htmlb.taglib.AbstractComponentTag;
import com.sapportals.htmlb.taglib.RendererContextTag;
/**
 * @author I803067
 *
 * To change the template for this generated type comment go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
public class ImageTag extends AbstractComponentTag {
	protected static Category logger = CategoryProvider.getUICategory();
	protected static Location tracer = Location.getLocation(ImageTag.class);

	// -------------------------------------------------------------------------
	// Properties

	// Implicit properties: The context and the image instance which holds the properties
	IPageContext myContext = null;
	Image myImage = new Image("", "");
	String hoverMenuId;
	String imageMapId;
	// -------------------------------------------------------------------------
	// Set/get methods for the properties
	public void setId(String id) {
		try {
			myContext =
				RendererContextTag.getTagRendererContext(this, pageContext);
		} catch (JspTagException jsExc) {
             LogHelper.logError(logger, tracer, jsExc);
		}
		if (myContext != null) {
			try {
				myImage = (Image) myContext.getAttribute(id);
			} catch (Exception exc) {
				LogHelper.logError(logger, tracer, exc);
			}
		}
		if (null == myImage) {
			myImage = new Image(id, id);
		}
		myImage.setId(id);

	}
	public void setSrc(String src) {
		myImage.setSrc(src);
	}
	public void setAlt(String alt) {
		myImage.setAlt(alt);
	}
	public void setHeight(String height) {
		myImage.setHeight(height);
	}
	public void setWidth(String width) {
		myImage.setWidth(width);
	}
	public void setTooltip(String tooltip) {
		myImage.setTooltip(tooltip);
	}

	public String getHoverMenuId() {
		return hoverMenuId;
	}

	public void setHoverMenuId(String hoverMenuId) {
		this.hoverMenuId = hoverMenuId;
	}
	public void setImageMapId(String imageMapId) {
		this.imageMapId = imageMapId;
	}

	public int doStartTag() throws JspException {

		myContext = RendererContextTag.getTagRendererContext(this, pageContext);

		if ((myImage.getId() != null) && (myImage.getId().length() > 0)) {
			pageContext.setAttribute(myImage.getId(), myImage);
		}

		if (hoverMenuId != null) {
			HoverMenu menu = (HoverMenu) pageContext.getAttribute(hoverMenuId);
			if (menu == null)
				throw new JspException(
					"cannot find menu with the specified id " + hoverMenuId);
			else
				myImage.setHoverMenu(menu);
		}

		if (imageMapId != null) {
			ImageMap imageMap = (ImageMap) pageContext.getAttribute(imageMapId);
			if (imageMap == null)
				throw new JspException(
					"cannot find image map with the specified id "
						+ imageMapId);
			else
				myImage.setImageMap(imageMap);
		}

		return TagSupport.EVAL_BODY_INCLUDE; // -> Scripting in the body

	}

	public int doEndTag() throws JspException {

		ActionForm actionFrm =
			(ActionForm) pageContext.getAttribute(ActionForm.ACTIONFORM);
		if ((actionFrm == null)
			|| (actionFrm != null
				&& myImage != null
				&& actionFrm.isControlApplicable(myImage.getId()))) {
			myContext.render(myImage);
		}

		return super.doEndTag(); // BodyTagSupport: return EVAL_PAGE;
	}

}