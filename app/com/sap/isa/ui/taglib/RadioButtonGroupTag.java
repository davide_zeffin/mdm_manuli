package com.sap.isa.ui.taglib;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspTagException;

import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.ui.controllers.ActionForm;
import com.sapportals.htmlb.RadioButtonGroup;
import com.sapportals.htmlb.rendering.IPageContext;
import com.sapportals.htmlb.taglib.AbstractContainerTag;
import com.sapportals.htmlb.taglib.RendererContextTag;

/**
 * Title:
 * Description:
 * Copyright:    Copyright (c) 2002
 * Company:
 * @author
 * @version 1.0
 */

public final class RadioButtonGroupTag extends AbstractContainerTag
{
    IPageContext myContext;
    RadioButtonGroup myRadioButtonGroup;
	/** 
	  *  The IsaLocation of the current Action.
	  *  This will be instantiated during perform and is always present.
	  */
	 protected IsaLocation log = IsaLocation.getInstance(RadioButtonGroupTag.class.getName());

    public RadioButtonGroupTag()
    {
        myContext = null;

    }

    public void setId(String name)
    {
         try  {
            myContext = RendererContextTag.getTagRendererContext(this, pageContext);
        }  catch(JspTagException jsExc)  {
			log.debug("Error ", jsExc);
        }
        if(null != myContext)  {
            Object obj = myContext.getAttribute(name);
            if (null != obj)
                myRadioButtonGroup = (RadioButtonGroup)obj;
        }
        if (null == myRadioButtonGroup)
            myRadioButtonGroup = new RadioButtonGroup("tempName");
        myRadioButtonGroup.setId(name);
    }

    public void setSelection(String selection)
    {
        myRadioButtonGroup.setSelection(selection);
    }

    public void setColumnCount(String columnCount)
    {
        myRadioButtonGroup.setColumnCount(convertToInteger("radioButtonGroup", "columnCount", columnCount));
    }

    public RadioButtonGroup getRadioButtonGroup()
    {
        return myRadioButtonGroup;
    }

    public int doStartTag()
        throws JspException
    {
        if(myRadioButtonGroup.getId() != null && myRadioButtonGroup.getId().length() > 0)
            pageContext.setAttribute(myRadioButtonGroup.getId(), myRadioButtonGroup);

        return super.doStartTag();
    }

    public int doEndTag()
        throws JspException
    {

		ActionForm actionFrm = (ActionForm)pageContext.getAttribute(ActionForm.ACTIONFORM);
		if((actionFrm == null)  || (actionFrm != null && 
			  myRadioButtonGroup != null &&
			  actionFrm.isControlApplicable(myRadioButtonGroup.getId())))  {
	        if(myRadioButtonGroup.getChildCount() == 0)
	            new JspException("The radioButtonGroup " + myRadioButtonGroup.getId() + " does not have inner radioButtons!");
	        myContext.render(myRadioButtonGroup);
		}
        return super.doEndTag();
    }

}
