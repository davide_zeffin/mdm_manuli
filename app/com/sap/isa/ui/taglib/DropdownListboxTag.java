package com.sap.isa.ui.taglib;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspTagException;
import javax.servlet.jsp.tagext.TagSupport;

import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.ui.controllers.ActionForm;
import com.sapportals.htmlb.DropdownListBox;
import com.sapportals.htmlb.IListModel;
import com.sapportals.htmlb.enum.DropdownListBoxDesign;
import com.sapportals.htmlb.rendering.IPageContext;
import com.sapportals.htmlb.taglib.AbstractComponentTag;
import com.sapportals.htmlb.taglib.RendererContextTag;


/**
 * Title:
 * Description:
 * Copyright:    Copyright (c) 2002
 * Company:
 * @author
 * @version 1.0
 */

public class DropdownListboxTag extends AbstractComponentTag {

  private DropdownListBox dropDown;
  private IPageContext myContext;

  /** 
	*  The IsaLocation of the current Action.
	*  This will be instantiated during perform and is always present.
	*/
   protected static IsaLocation log = IsaLocation.getInstance(DropdownListboxTag.class.getName());

  public DropdownListboxTag() {
  }

  public void setId(String id)  {
      try  {
          myContext = RendererContextTag.getTagRendererContext(this, pageContext);
      }  catch(JspTagException jsExc)  {
               log.debug("Error ", jsExc);
      }
      if(myContext != null)  {
          dropDown = (DropdownListBox)myContext.getAttribute(id);
      }
      if(null == dropDown)  {
          dropDown = new DropdownListBox(id);
      }
      dropDown.setId(id);
  }

  public void setModel (String modelDescriptor) {
      Object modelObj = null;
      try  {
          modelObj = getModel ( "dropdownListBox", modelDescriptor );
      }  catch(Exception exc)  {
		log.debug("Error ", exc);
      }
      if(null == modelObj)  {
          if(myContext != null)  {
              modelObj = (IListModel)myContext.getAttribute(modelDescriptor);
          }
      }
      if ( ! ( modelObj instanceof IListModel ) ) {
              throw new IllegalArgumentException("Tag dropdownListBox attribute model"
                                      + ": Cannot access bean property " + modelDescriptor + " in page context");
      }
      dropDown.setModel( (IListModel) modelObj );
  }

  public void setDisabled (String disabled) {
      //dropDown.setDisabled( convertToBoolean("dropdownListBox", "disabled", disabled) );
  }

  public void setSelection (String selection) {
      //dropDown.setSelection(selection);
  }

  public void setNameOfKeyColumn (String nameOfKeyColumn) {
      dropDown.setNameOfKeyColumn(nameOfKeyColumn);
  }

  public void setNameOfValueColumn (String nameOfValueColumn) {
      dropDown.setNameOfValueColumn(nameOfValueColumn);
  }

  public void setOnSelect (String onSelect) {
      dropDown.setOnSelect(onSelect);
  }

  public void setOnClientSelect (String onClientSelect) {
      dropDown.setOnClientSelect(onClientSelect);
  }

  public void setTooltip (String tooltip) {
      dropDown.setTooltip(tooltip);
  }

  public DropdownListBox getListBox() {
      return this.dropDown;
  }

  public void setWidth(String width)  {
      dropDown.setWidth(width);
  }

  public void setDesign(String design)  {
      dropDown.setDesign( (DropdownListBoxDesign) convertToEnum ("dropdownListBox", "design", design,
                                                          "com.sapportals.htmlb.enum.DropdownListBoxDesign", this ) );
  }

  public int doStratTag() throws JspException  {
      if ( (dropDown.getId() != null) && (dropDown.getId().length() > 0) ){
          pageContext.setAttribute(dropDown.getId(), dropDown);
      }
      return TagSupport.EVAL_BODY_INCLUDE;
  }

  public int doEndTag () throws JspException {

	ActionForm actionFrm = (ActionForm)pageContext.getAttribute(ActionForm.ACTIONFORM);
	if((actionFrm == null) || (actionFrm != null && 
		  dropDown != null &&
		  actionFrm.isControlApplicable(dropDown.getId())))  {
	      myContext = RendererContextTag.getTagRendererContext(this, pageContext);
	
	      if ( (dropDown.getModel() == null) && (dropDown.getKeys().hasNext() == false) ) {
	          new JspException("The dropdownListBox " + dropDown.getId()
	                                                  + " does have neither a model nor anested listBoxItems!");
	      }
	
	      if(dropDown.getModel().getSingleSelection() != null)  {
	          dropDown.setSelection(dropDown.getModel().getSingleSelection());
	      }
		  myContext.render(dropDown);
	  }
      return EVAL_PAGE;
  }

}
