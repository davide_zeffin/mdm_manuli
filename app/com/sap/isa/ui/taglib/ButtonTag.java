package com.sap.isa.ui.taglib;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspTagException;
import javax.servlet.jsp.tagext.TagSupport;

import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.ui.controllers.ActionForm;
import com.sapportals.htmlb.Button;
import com.sapportals.htmlb.enum.ButtonDesign;
import com.sapportals.htmlb.rendering.IPageContext;
import com.sapportals.htmlb.taglib.AbstractComponentTag;
import com.sapportals.htmlb.taglib.RendererContextTag;

/**
 * Title:        Internet Sales
 * Description:
 * Copyright:    Copyright (c) 2002
 * Company:      SAPMarkets
 * @author
 * @version 1.0
 */

public final class ButtonTag extends AbstractComponentTag
{
    IPageContext myContext;
    Button button;

	/** 
	  *  The IsaLocation of the current Action.
	  *  This will be instantiated during perform and is always present.
	  */
	 protected static IsaLocation log = IsaLocation.getInstance(ButtonTag.class.getName());

    public ButtonTag(){}

    public void setId(String id)
    {
        try  {
            myContext = RendererContextTag.getTagRendererContext(this, pageContext);
        }  catch(JspTagException jsExc)  {
			log.debug("Error ", jsExc);
        }
        if(myContext != null)  {
            button = (Button)myContext.getAttribute(id);
        }
        if(null == button)  {
            button = new Button(id);
        }
        button.setId(id);
    }

    public void setText(String text)
    {
        button.setText(text);
    }

    public void setTooltip(String tooltip)
    {
        button.setTooltip(tooltip);
    }

    public void setOnClick(String onClick)
    {
        button.setOnClick(onClick);
    }

    public void setOnClientClick(String onClientClick)
    {
        button.setOnClientClick(onClientClick);
    }

    public void setDisabled(String disabled)  {
        //button.setDisabled(convertToBoolean("button", "disabled", disabled));
    }

    public void setWidth(String width)
    {
        button.setWidth(width);
    }

    public void setDesign(String design)
    {
        button.setDesign((ButtonDesign)convertToEnum("button", "design", design, "com.sapportals.htmlb.enum.ButtonDesign", this));
    }

    public void setEncode(String encode)
    {
        button.setEncode(convertToBoolean("button", "encode", encode));
    }

    public int doStartTag()
        throws JspException
    {
        pageContext.setAttribute(button.getId() , button);
        myContext = RendererContextTag.getTagRendererContext(this, pageContext);
        return TagSupport.EVAL_BODY_INCLUDE;
    }

    public int doEndTag()
        throws JspException
    {
		ActionForm actionFrm = (ActionForm)pageContext.getAttribute(ActionForm.ACTIONFORM);
		// Default the button width for consistency
		if (null == button.getWidth()){
			button.setWidth("50");
		}
		if((actionFrm == null)  || (actionFrm != null && 
			  button != null &&
			  actionFrm.isControlApplicable(button.getId())))  {
			myContext.render(button);
		}
        
        return super.doEndTag();
    }

}