package com.sap.isa.ui.taglib;

//import java.text.MessageFormat;

import java.text.MessageFormat;

import javax.servlet.jsp.JspTagException;
import javax.servlet.jsp.tagext.TagSupport;

import com.sap.isa.auction.actionforms.BaseFormConstants;
import com.sap.isa.auction.util.AccessibilityUtil;
import com.sap.isa.auction.util.FormUtility;
import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.ui.controllers.ActionForm;
import com.sapportals.htmlb.InputField;
import com.sapportals.htmlb.enum.DataType;
import com.sapportals.htmlb.enum.InputFieldDesign;
import com.sapportals.htmlb.rendering.IPageContext;
import com.sapportals.htmlb.taglib.AbstractComponentTag;
import com.sapportals.htmlb.taglib.RendererContextTag;

/**
 * Title:
 * Description:
 * Copyright:    Copyright (c) 2002
 * Company:
 * @author
 * @version 1.0
 */

public class InputFieldTag extends AbstractComponentTag  {

	/** 
	  *  The IsaLocation of the current Action.
	  *  This will be instantiated during perform and is always present.
	  */
	 protected static IsaLocation log = IsaLocation.getInstance(InputFieldTag.class.getName());

    private IPageContext myContext;
    private InputField inputField;

    public InputFieldTag() {
    }

    public void setId (String id) {
        try  {
            myContext = RendererContextTag.getTagRendererContext(this, pageContext);
        }  catch(JspTagException jsExc)  {
              log.debug("Error ", jsExc);
        }
        if(myContext != null)  {
            try  {
                inputField = (InputField)myContext.getAttribute(id);
            }  catch(Exception exc)  {
				log.debug("Error ", exc);
            }
        }
        if(null == inputField)  {
            inputField = new InputField(id);
        }
        inputField.setId(id);
    }

    public void setType (String type) {
            inputField.setType( (DataType) convertToEnum ("inputField", "type", type,
                                                                "com.sapportals.htmlb.enum.DataType", this ) );
    }

    public void setValue (String value) {
		inputField.setValue( value );
    }

    public void setDisabled (String disabled) {
        //inputField.setDisabled( convertToBoolean("inputField", "disabled", disabled) );
    }

    public void setVisible (String visible) {
		inputField.setVisible( convertToBoolean("inputField", "visible", visible) );
    }

    public void setPassword (String password) {
		inputField.setPassword( convertToBoolean("inputField", "password", password) );
    }

    public void setRequired (String required) {
		inputField.setRequired( convertToBoolean("inputField", "required", required) );
    }

    public void setShowHelp (String showHelp) {
		inputField.setShowHelp( convertToBoolean("inputField", "showDateHelp", showHelp) );
    }

    public void setSize (String size) {
		inputField.setSize( convertToInteger("inputField", "size", size) );
    }

    public void setMaxlength (String maxlength) {
		inputField.setMaxlength( convertToInteger("inputField", "maxlength", maxlength) );
    }

    public void setInvalid (String invalid) {
        //inputField.setInvalid( convertToBoolean("inputField", "invalid", invalid) );
    }

    public void setWidth (String width) {
		inputField.setWidth( width );
    }

    public void setTooltip(String tooltip) {
        inputField.setTooltip(tooltip);
    }

    public void setFirstDayOfWeek (String firstDayOfWeek) {
        inputField.setFirstDayOfWeek( convertToInteger("inputField", "firstDayOfWeek", firstDayOfWeek) );
    }

    public void setDesign(String design) {
        inputField.setDesign( (InputFieldDesign) convertToEnum ("inputField", "design", design,
                                                            "com.sapportals.htmlb.enum.InputFieldDesign", this ) );
    }

    public int doStartTag() throws javax.servlet.jsp.JspException {
      pageContext.setAttribute(inputField.getId() , inputField);
      myContext = RendererContextTag.getTagRendererContext(this, pageContext);
      return TagSupport.EVAL_BODY_INCLUDE;
    }

    public int doEndTag() throws javax.servlet.jsp.JspException {

		ActionForm actionFrm = (ActionForm)pageContext.getAttribute(ActionForm.ACTIONFORM);

        DataType dt = inputField.getType();         
        // isShowHelp is false by default in accessibility mode, so no need to
        // check for that
        if ((null != dt) && ("DATE".equals(dt.getStringValue())) &&
            (AccessibilityUtil.isAccessibleContext(pageContext))){ 
                java.text.SimpleDateFormat f = (java.text.SimpleDateFormat) 
                        java.text.DateFormat.getDateInstance
                            (java.text.DateFormat.SHORT, 
                            pageContext.getRequest().getLocale());
                if (null != f){
                    String df = FormUtility.getResourceString
                            (BaseFormConstants.BASEBUNDLE , 
                            "Access.DateFormatInputField", 
                            pageContext.getSession());
                    String[] args = {new String(f.toLocalizedPattern())};  
                    StringBuffer sb = new StringBuffer();
                    sb.append(MessageFormat.format(df, args));
                    inputField.setTooltip(sb.toString());
                }		           
        }

//        DataType dt = inputField.getType();         
//        if ("DATE".equals(dt.getStringValue()) && inputField.isShowHelp()){
//            if (AccessibilityUtil.isAccessibleContext(pageContext)){
//                java.text.SimpleDateFormat f = (java.text.SimpleDateFormat) 
//                        java.text.DateFormat.getDateInstance
//                            (java.text.DateFormat.SHORT, 
//                            pageContext.getRequest().getLocale());
//                String[] args = {new String(f.toLocalizedPattern())};  
//
//                String df = FormUtility.getResourceString
//                        (BaseFormConstants.BASEBUNDLE , 
//                        "Access.DateFormat", 
//                        pageContext.getSession());
//
//                inputField.setTooltip(inputField.getValue() + " " + 
//                        MessageFormat.format(df, args));                        
//            }
//        }


		if((actionFrm == null) || (actionFrm != null && 
			  inputField != null &&
			  actionFrm.isControlApplicable(inputField.getId())))  {
	  		myContext.render(inputField);
		}
      	return super.doEndTag();
    }
}
