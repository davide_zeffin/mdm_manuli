package com.sap.isa.ui.taglib;

import com.sapportals.htmlb.taglib.*;
import com.sapportals.htmlb.rendering.IPageContext;
import com.sapportals.htmlb.rendering.PageContextFactory;
import com.sapportals.htmlb.HTMLFragment;

import javax.servlet.jsp.PageContext;

import com.sap.isa.core.util.WebUtil;


/**
 * Title:
 * Description:
 * Copyright:    Copyright (c) 2002
 * Company:
 * @author
 * @version 1.0
 */

public class ExtendedRendererContextTag extends RendererContextTag {

  private boolean notifyLoading = false;

  public ExtendedRendererContextTag() {
      super();
  }

  public void setNotifyLoading(String notify)  {
      notifyLoading = convertToBoolean("context" , "notifyLoading" , notify);
  }

  public int doStartTag() throws javax.servlet.jsp.JspException {

      IPageContext myContext = null;

      Object myRequest  = pageContext.getAttribute(PageContext.REQUEST);
      Object myResponse = pageContext.getAttribute(PageContext.RESPONSE);

			// Create and initialize the renderer context
      myContext = PageContextFactory.createPageContext(myRequest, myResponse);
      setRendererContext(myContext);
      myContext.setWriter(pageContext.getOut());

      if ( (this.getId() != null) && (this.getId().length() > 0) ) {
          // Put the RendererContext instance into the page context
          // -> It is available as scripting variable (default scope: PageContext.PAGE_SCOPE)
          pageContext.setAttribute(this.getId(), myContext);
      }

      String   htmlContent = getLoadingScript();

      if ( (htmlContent != null) && (htmlContent.length() > 0) ) {
          HTMLFragment content = new HTMLFragment(htmlContent);
              myContext.render(content);
      }
      return EVAL_BODY_TAG;
  }

  public int doEndTag() throws javax.servlet.jsp.JspException {

      String   htmlContent = getBodyContent().getString();
      getRendererContext().setWriter(pageContext.getOut());

      if ( (htmlContent != null) && (htmlContent.length() > 0) ) {
          HTMLFragment content = new HTMLFragment(htmlContent);
              getRendererContext().render(content);
      }

      htmlContent = getLoadedScript();
      if ( (htmlContent != null) && (htmlContent.length() > 0) ) {
          HTMLFragment content = new HTMLFragment(htmlContent);
          getRendererContext().render(content);
      }

      return EVAL_PAGE;
  }

    private String getLoadingScript()  {
        StringBuffer buffer = new StringBuffer();
        buffer.append("<div id='_our_loading'><img width='16' height='16' src='");
        buffer.append(WebUtil.getMimeURL(pageContext , "/auction/mimes/images/clockon.gif"));
        buffer.append("'></img></div>");
        return buffer.toString();
    }

    private String getLoadedScript()  {
        StringBuffer buffer = new StringBuffer();
        buffer.append("<script language='JavaScript'>eval(\"document.getElementById('_our_loading').style.display = 'none'\");</script>");
        return buffer.toString();
    }
}
