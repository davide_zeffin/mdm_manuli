package com.sap.isa.ui.taglib;

import javax.servlet.jsp.JspException;

import com.sap.isa.core.util.WebUtil;
import com.sapportals.htmlb.Document;
import com.sapportals.htmlb.HTMLFragment;
import com.sapportals.htmlb.rendering.IPageContext;
import com.sapportals.htmlb.taglib.AbstractContainerTag;
import com.sapportals.htmlb.taglib.RendererContextTag;

/**
 * Title:
 * Description:
 * Copyright:    Copyright (c) 2002
 * Company:
 * @author
 * @version 1.0
 */

public class ExtendedPageTag extends AbstractContainerTag {

    private IPageContext  myContext;
    private Document      myPage;
    private boolean notifyLoading;

    // - Tag attribute:
    private String        title;

    public void setNotifyLoading(String notify)  {
        notifyLoading = convertToBoolean("page" , "notifyLoading" , notify);
    }

    public void setTitle( String title ) {
        this.title = title;
    }

    public ExtendedPageTag() {
    }

    public int doStartTag () throws JspException {

        myContext = RendererContextTag.getTagRendererContext(this, pageContext);
        myPage    = myContext.createDocument(this.title); // without form

        return super.doStartTag();
    }

    public int doEndTag () throws JspException {

            // Add the body content to the document and render the document
        myContext = RendererContextTag.getTagRendererContext(this, pageContext);
        String htmlContent = notifyLoading ? getLoadingScript() : null;
        if ( htmlContent != null && htmlContent.length() > 0 ) {
                myContext.render(new HTMLFragment(htmlContent) );
        }

        htmlContent = getBodyContent().getString();
        if ( htmlContent != null && htmlContent.length() > 0 ) {
                myPage.addComponent( new HTMLFragment(htmlContent) );
        }
        myContext.render(myPage);

        htmlContent = notifyLoading ? getLoadedScript() : null;
        if ( htmlContent != null && htmlContent.length() > 0 ) {
                myContext.render(new HTMLFragment(htmlContent) );
        }
        return super.doEndTag(); // BodyTagSupport: return EVAL_PAGE;
    }

    private String getLoadingScript()  {
        StringBuffer buffer = new StringBuffer();
        buffer.append("<div id='_our_loading'><img width='16' height='16' src='");
        buffer.append(WebUtil.getMimeURL(pageContext , "/auction/mimes/images/clockon.gif"));
        buffer.append("'></img></div>");
        return buffer.toString();
    }

    private String getLoadedScript()  {
        StringBuffer buffer = new StringBuffer();
        buffer.append("<script language='JavaScript'>eval(\"document.getElementById('_our_loading').style.display = 'none'\");</script>");
        return buffer.toString();
    }
}
