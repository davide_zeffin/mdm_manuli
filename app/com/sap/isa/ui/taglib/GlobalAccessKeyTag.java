package com.sap.isa.ui.taglib;

import java.io.IOException;

import javax.servlet.jsp.JspException;

import com.sap.tc.logging.Category;
import com.sap.tc.logging.Location;
import com.sap.isa.auction.actionforms.BaseFormConstants;
import com.sap.isa.auction.logging.CategoryProvider;
import com.sap.isa.auction.logging.LogHelper;
import com.sap.isa.auction.util.AccessibilityUtil;
import com.sap.isa.auction.util.FormUtility;
import com.sapportals.htmlb.taglib.AbstractComponentTag;

/**
 * Title:			Internet Sales Selling Via eBay
 * Description:		Prints out global accessibility navigation.
 * @Copyright: 		Copyright (c) 2004
 * Company:			SAPLabs LLC
 * Created on:		Sep 17, 2004
 */
public class GlobalAccessKeyTag extends AbstractComponentTag {

    private String key;
    private static final String TOPNAVIGATION = "Access.AccessKey.String.Top";
    private static final String MAINCONTENT = "Access.AccessKey.String.Main";
    private static final String DETAILS = "Access.AccessKey.String.Details";
    
    protected static Category logger = CategoryProvider.getUICategory();
    protected static Location tracer = Location.getLocation(GlobalAccessKeyTag.class);
    
    /* (non-Javadoc)
     * @see javax.servlet.jsp.tagext.Tag#doStartTag()
     */
    public int doStartTag() throws JspException {
        if (!AccessibilityUtil.isAccessibleContext(pageContext)){
            return SKIP_BODY;       
        }
		String accessKey = getKey();
		if (!isValidKeyValue(key)){
			throw new JspException("accessKey property may only have the values " +
				"1, 2 or 3");
		}
        try {
            pageContext.getOut().write(composeHTML(accessKey));    	
        } catch (IOException e) {
        	LogHelper.logError(logger, tracer, e);
        }
		return SKIP_BODY;
    }    

	/**
	 * 
	 * @param key
	 * @return
	 */
    private String composeHTML(String accessKey){
    	StringBuffer sb = new StringBuffer();
    	String title = "";
    	if ("1".equals(accessKey)){
    		title = FormUtility.getResourceString(BaseFormConstants.BASEBUNDLE, 
					TOPNAVIGATION, pageContext.getSession());
			sb.append("<a href=\"#TopNavigation\" id=\"TopNavigation\" " +					"title=\"" + title + "\" name=\"TopNavigation\" " +					"accesskey=\"" + accessKey +"\" tabindex=\"0\"></a>");    		
    	} else if ("2".equals(key)){
			title = FormUtility.getResourceString(BaseFormConstants.BASEBUNDLE, 
					MAINCONTENT, pageContext.getSession());
			sb.append("<a href=\"#MainContent\" id=\"MainContent\" " +
					"title=\"" + title + "\" name=\"MainContent\" " +
					"accesskey=\"" + accessKey +"\" tabindex=\"0\"></a>");    		
    	} else if ("3".equals(key)){
			title = FormUtility.getResourceString(BaseFormConstants.BASEBUNDLE, 
					DETAILS, pageContext.getSession());
			sb.append("<a href=\"#Details\" id=\"Details\" " +
					"title=\"" + title + "\" name=\"Details\" " +
					"accesskey=\"" + accessKey +"\" tabindex=\"0\"></a>");    		
    	}
    	return sb.toString();
    }

    private boolean isValidKeyValue(String key) {
        return "1".equals(key) || "2".equals(key) || "3".equals(key);
    }
    
    /**
     * @return
     */
    public String getKey() {
        return key;
    }

    /**
     * @param string
     */
    public void setKey(String string) {
        key = string;
    }

}
