package com.sap.isa.ui.taglib;

import javax.servlet.jsp.tagext.TagSupport;

import com.sap.isa.core.logging.IsaLocation;


/**
 * Title:
 * Description:
 * Copyright:    Copyright (c) 2002
 * Company:
 * @author
 * @version 1.0
 */

public class HTMLPageLoaded extends TagSupport  {

	/** 
	  *  The IsaLocation of the current Action.
	  *  This will be instantiated during perform and is always present.
	  */
	 protected static IsaLocation log = IsaLocation.getInstance(HTMLPageLoaded.class.getName());

  public HTMLPageLoaded() {
  }

    public int doStartTag() throws javax.servlet.jsp.JspException {

        try  {
        }  catch(Exception exc)  {
             log.debug("Error", exc );
        }
        return EVAL_BODY_INCLUDE;
    }

    public int doEndTag() throws javax.servlet.jsp.JspException {

        try  {

            pageContext.getOut().write(getLoadedScript());
        }  catch(Exception exc)  {
			log.debug("Error", exc );
        }
        return EVAL_PAGE;
    }

    private String getLoadedScript()  {
        StringBuffer buffer = new StringBuffer();
        buffer.append("<script language='JavaScript'>if(document.getElementById('_our_loading'))eval(\"document.getElementById('_our_loading').style.display = 'none'\");</script>");
        return buffer.toString();
    }

}