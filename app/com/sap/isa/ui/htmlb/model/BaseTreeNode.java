package com.sap.isa.ui.htmlb.model;

import com.sapportals.htmlb.TreeNode;
import java.util.Locale;

/**
 * Title:
 * Description:
 * Copyright:    Copyright (c) 2003
 * Company:
 * @author
 * @version 1.0
 */

public class BaseTreeNode extends TreeNode
{
  protected Object userObject = null;
  protected Locale locale = Locale.ENGLISH;

  public BaseTreeNode(Locale locale, String text)
  {
    super(text);
    this.locale = locale;
  }

  public void setUserObject(Object newVal)
  {
    userObject = newVal;
  }

  public Object getUserObject()
  {
    return userObject;
  }

  public Locale getLocale()
  {
    return locale;
  }

  public void refresh()
  {
  }
}
