package com.sap.isa.ui.htmlb.eventhandler;

import java.util.Hashtable;
import java.util.Enumeration;

import com.sapportals.htmlb.event.Event;


/**
 * Title:
 * Description:
 * Copyright:    Copyright (c) 2003
 * Company:
 * @author
 * @version 1.0
 */

public class EventHandlerMap
{
  protected Hashtable eventMap = new Hashtable();

  private static final EventHandlerMap evMap = new EventHandlerMap();

  private EventHandlerMap()
  {
  }

  public static EventHandlerMap getEventHandlerMap()
  {
    return evMap;
  }

  public EventHandler getEventHandler(Class formClass,
                                      String event)
  {
    EventHandler retval = null;
    Hashtable classHandlers = (Hashtable)eventMap.get(formClass);
    if (classHandlers != null)
    {
      retval = (EventHandler) classHandlers.get(event);
    }
    return retval;
  }

  public void addEventHandler(Class formClass,
                              String event,
                              EventHandler handler)
  {
    Hashtable classHandlers = (Hashtable)eventMap.get(formClass);
    if (classHandlers == null)
    {
      classHandlers = new Hashtable();
      eventMap.put(formClass, classHandlers);
    }
    classHandlers.put(event, handler);
  }

  public void removeEventHandler( Class formClass,
                                  String event)
  {
    Hashtable classHandlers = (Hashtable)eventMap.get(formClass);
    if (classHandlers != null)
    {
      classHandlers.remove(event);
    }
  }

  public boolean isMemberOfMap(Class formClass, Class possibleMapMember)
  {
    boolean retval = false;
    Hashtable classHandlers = (Hashtable)eventMap.get(formClass);
    if (classHandlers != null)
    {
      Enumeration en = classHandlers.elements();
      while(en.hasMoreElements())
      {
        EventHandler evH = (EventHandler)en.nextElement();
        if (evH.getClass() == possibleMapMember)
        {
         retval = true;
         break;
        }
      }
    }
    return retval;
  }
}
