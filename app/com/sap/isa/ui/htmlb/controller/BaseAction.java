package com.sap.isa.ui.htmlb.controller;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.sap.tc.logging.Category;
import com.sap.tc.logging.Location;
import com.sap.isa.services.schedulerservice.SchedulerLocation;
import com.sap.isa.services.schedulerservice.ui.actionform.JobDetailForm;
import com.sap.isa.services.schedulerservice.util.ExceptionLogger;
import com.sap.isa.ui.htmlb.eventhandler.EventHandler;
import com.sap.isa.ui.htmlb.eventhandler.EventHandlerMap;
import com.sapportals.htmlb.TabStrip;
import com.sapportals.htmlb.event.Event;
import com.sapportals.htmlb.rendering.IPageContext;
import com.sapportals.htmlb.rendering.PageContextFactory;


/**
 * Title:
 * Description:
 * Copyright:    Copyright (c) 2003
 * Company:
 * @author
 * @version 1.0
 */

public class BaseAction extends Action
{
	private static final String LOG_CAT_NAME = "/System/Scheduler/ui";
	private static Location tracer = SchedulerLocation.getLocation(BaseAction.class);	
	private static Category logger = Category.getCategory(LOG_CAT_NAME); 
  public BaseAction()
  {
  }

  public ActionForward perform( ActionMapping mapping,
                                ActionForm form,
                                HttpServletRequest request,
                                HttpServletResponse response)
  throws IOException, ServletException
  {
    try
    {
      IPageContext pageContext = PageContextFactory.createPageContext(request, response);
      if (pageContext != null)
      {
		TabStrip tabStrip = (TabStrip)pageContext.getComponentForId("jobDetailTabStrip");
		if(tabStrip!=null) {
			int i = tabStrip.getSelection();
			request.setAttribute(JobDetailForm.TabSelection,new Integer(i));
		}
        /**
         * The algorithm for event routing:
         * - If the event was called from another event, there should be a call stack
         * with the corresponding top of the stack being one of the items that belongs
         * to the form, in that scenario, reroute to that
         * - Find the event handler for the current event, if exists, reroute to it
         * - Call the default handler
         */
        HttpSession session = request.getSession();
        synchronized(session)
        {
          Event currentEvent = pageContext.getCurrentEvent();

          EventHandler evH = null;

          if (currentEvent != null)
          {
            if (currentEvent != null && form != null)
            {
              evH = EventHandlerMap.getEventHandlerMap().getEventHandler(form.getClass(), currentEvent.getAction());
              if (evH != null)
              {
                ActionForward forward = evH.perform(mapping, currentEvent, pageContext, session, request, response);
                return forward;
              }
            }
          }
        }
      }

      HttpSession session = request.getSession();
      synchronized (session)
      {
        ActionForward forward = doPerform(mapping, form, request, response);
        return forward;
      }
    }
    catch(Throwable th)
    {
      ExceptionLogger.logCatError(logger,tracer,th);
    }
    return mapping.findForward(BaseActionForwardConstants.Success);
  }


  public ActionForward doPerform( ActionMapping mapping,
                                  ActionForm form,
                                  HttpServletRequest request,
                                  HttpServletResponse response)
  throws IOException, ServletException
  {
    return mapping.findForward(BaseActionForwardConstants.Success);
  }

}
