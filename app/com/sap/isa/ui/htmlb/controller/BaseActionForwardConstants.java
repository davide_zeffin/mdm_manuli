package com.sap.isa.ui.htmlb.controller;

/**
 * Title:
 * Description:
 * Copyright:    Copyright (c) 2003
 * Company:
 * @author
 * @version 1.0
 */

public interface BaseActionForwardConstants
{
  public static final String Success = "success";
  public static final String Failure = "failure";
  public static final String Error = "error";
}
