package com.sap.isa.ui.samples.actionForms;

import com.sap.isa.ui.controllers.ActionForm;
import com.sapportals.htmlb.table.TableView;
import com.sap.isa.ui.samples.tableModel.SampleTableModel;
import com.sapportals.htmlb.DropdownListBox;

import com.sap.isa.ui.samples.listmodel.ListModel;


import com.sapportals.htmlb.InputField;

/**
 * Title:
 * Description:
 * Copyright:    Copyright (c) 2002
 * Company:
 * @author
 * @version 1.0
 */

public class SampleForm extends ActionForm {

  public final String INPUT = "input";
  private InputField inputField = new InputField(INPUT);
  public static final String TABLEVIEW = "tableView";
  public static final String TABLEVIEWMODEL = "tableViewMod";

  private TableView tableView = new TableView(TABLEVIEW);
  private SampleTableModel model = new SampleTableModel();

  private ListModel listmodel = new ListModel();
  private DropdownListBox listBox = new DropdownListBox("list1");

  public SampleForm() {
  }

  public void setData()  {
      this.setAttribute(INPUT , inputField);
      listmodel.setData();
      this.setAttribute("list1" , listBox);
      this.setAttribute("listmodel" , listmodel);
  }

  public void getData()  {
      InputField inputField = (InputField)this.getAttribute(INPUT);
      inputField.setString("NewValue");
      this.setAttribute(INPUT , inputField);
  }

  public void setTableDat()  {
      this.setAttribute(TABLEVIEW , tableView);
      this.setAttribute(TABLEVIEWMODEL , model);
  }
}
