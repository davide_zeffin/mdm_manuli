package com.sap.isa.ui.samples.tableModel;

import com.sap.isa.ui.components.DataComponent;
import com.sap.isa.ui.components.table.TableModel;
import com.sapportals.htmlb.Link;
import com.sapportals.htmlb.enum.TableColumnType;
import com.sapportals.htmlb.table.TableColumn;
import com.sapportals.htmlb.type.AbstractDataType;
import com.sapportals.htmlb.type.DataString;

/**
 * Title:
 * Description:
 * Copyright:    Copyright (c) 2002
 * Company:
 * @author
 * @version 1.0
 */

public class SampleTableModel extends TableModel {

  public SampleTableModel() {
  }

  public void initColumns()  {
      TableColumn column = new TableColumn(this , "COLUMN1" , "Col1" , TableColumnType.TEXT);
      addColumn(column);

      column = new TableColumn(this , "COLUMN2" , "Col2" , TableColumnType.USER);
      addColumn(column);

      column = new TableColumn(this , "COLUMN3" , "Col3" , TableColumnType.USER);
      addColumn(column);
  }

  public int getRowCount()  {
      return 9;
  }

  public AbstractDataType getValueAt(int row , int col)  {
      switch(col)  {
          case 1:
              return new DataString("hello1"+Integer.toString(row));

          case 2:
              Link lnk = new Link("Hello2");
              lnk.addText("hello2");
              return new DataComponent(lnk);

          case 3:
              Link lnk2 = new Link("Hello3");
              lnk2.addText("Hello3");
              lnk2.setOnClick(Integer.toString(row));
              return new DataComponent(lnk2);
      }
      return null;
  }


}
