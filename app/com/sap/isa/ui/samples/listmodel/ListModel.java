package com.sap.isa.ui.samples.listmodel;

import com.sapportals.htmlb.DefaultListModel;

/**
 * Title:
 * Description:
 * Copyright:    Copyright (c) 2002
 * Company:
 * @author
 * @version 1.0
 */

public class ListModel extends DefaultListModel {

  public ListModel() {
  }

  public void setData()  {
      this.addItem("Item1" , "Item1");
  }

  public boolean isSingleSelection()  {
      return true;
  }
}