package com.sap.isa.ui.samples.actions;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.sap.isa.ui.controllers.ActionForm;
import com.sap.isa.ui.eventhandlers.TableEventHandler;
import com.sapportals.htmlb.event.TableNavigationEvent;



/**
 * Title:
 * Description:
 * Copyright:    Copyright (c) 2002
 * Company:
 * @author
 * @version 1.0
 */

public class SampleTableEventHandler extends TableEventHandler {

  public SampleTableEventHandler() {
  }


  public ActionForward tableNavigationEvent(ActionMapping mapping,
                                              ActionForm form,
                                              TableNavigationEvent event,
                                              HttpServletRequest request,
                                              HttpServletResponse response)
                                              throws java.io.IOException, javax.servlet.ServletException {
      super.tableNavigationEvent(mapping , form , event , request , response);
      return mapping.findForward("table");
  }
}
