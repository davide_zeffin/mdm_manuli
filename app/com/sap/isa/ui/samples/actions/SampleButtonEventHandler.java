package com.sap.isa.ui.samples.actions;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.sap.isa.ui.controllers.ActionForm;
import com.sap.isa.ui.eventhandlers.ButtonClickedEventHandler;
import com.sap.isa.ui.samples.actionForms.SampleForm;
import com.sapportals.htmlb.event.Event;

/**
 * Title:
 * Description:
 * Copyright:    Copyright (c) 2002
 * Company:
 * @author
 * @version 1.0
 */

public class SampleButtonEventHandler extends ButtonClickedEventHandler {

  public SampleButtonEventHandler() {
  }

  public ActionForward buttonClickedEvent(ActionMapping mapping, ActionForm form, Event event, HttpServletRequest request, HttpServletResponse response) throws java.io.IOException, javax.servlet.ServletException {
      SampleForm actionForm = (SampleForm)form;
      actionForm.setTableDat();
      return mapping.findForward("table");
  }
}
