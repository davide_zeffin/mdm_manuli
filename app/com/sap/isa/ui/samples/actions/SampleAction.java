package com.sap.isa.ui.samples.actions;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.sap.isa.ui.controllers.Action;
import com.sap.isa.ui.controllers.ActionForm;
import com.sap.isa.ui.samples.actionForms.SampleForm;

/**
 * Title:
 * Description:
 * Copyright:    Copyright (c) 2002
 * Company:
 * @author
 * @version 1.0
 */

public class SampleAction extends Action {

  public SampleAction() {
  }

  protected void registerEventHandlers()  {
      registerEventHandler("BUTTON1" , new SampleButtonEventHandler());
      registerEventHandler("tableView" , new SampleTableEventHandler());
  }


  protected ActionForward doServiceRequest(ActionMapping mapping, ActionForm actionForm, HttpServletRequest request, HttpServletResponse response) throws javax.servlet.ServletException, java.io.IOException {

      SampleForm sample = (SampleForm)actionForm;
      sample.setData();
      sample.getData();
      return mapping.findForward("sample1");
  }
  public ActionForward perform(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws javax.servlet.ServletException, java.io.IOException {
    /**@todo: Override this com.sap.isa.ui.controllers.Action method*/
    return super.perform( mapping,  form,  request,  response);
  }

  protected void preProcessing(ActionForm form)  {

  }

  protected void postProcessing(ActionForm form)  {

  }
}
