package com.sap.isa.ui.controllers;

import com.sapportals.htmlb.rendering.RendererManager;

/**
 * Title:
 * Description:
 * Copyright:    Copyright (c) 2002
 * Company:
 * @author
 * @version 1.0
 */

public class ApplicationSettings {

  public ApplicationSettings() {
  }

  public static final void setDefaultSettings(String rootPath)  {
      RendererManager.setDefaultMimePath(rootPath+"/htmlb/mimes/");
      RendererManager.setDefaultJavascriptPath(rootPath+"/htmlb/jslib/");
  }
}