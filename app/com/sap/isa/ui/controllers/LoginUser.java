package com.sap.isa.ui.controllers;

import java.util.Locale;
import java.util.Collection;
import java.util.ArrayList;

/**
 * Title:
 * Description:
 * Copyright:    Copyright (c) 2003
 * Company:
 * @author
 * @version 1.0
 */

public class LoginUser {

  private String lastName;
  private String firstName;
  private String greetingMessage;
  private Locale locale;

  private Collection listofHiddenControls;

    public LoginUser() {
    }

    public LoginUser(String lastName , String firstName , String greetingMessage)  {
        this.lastName = lastName;
        this.firstName = firstName;
        this.greetingMessage = greetingMessage;
    }

    public void addControlIDToBeHidden(String id)  {
        if(null == listofHiddenControls)  {
            listofHiddenControls = new ArrayList();
        }
        listofHiddenControls.add(id);
    }

    public boolean isThisIdToBeHidden(String id)  {
        if(listofHiddenControls.contains(id))
            return true;
        return false;
    }

    public void setLastName(String name)  {
        this.lastName = name;
    }

    public void setFirstName(String name)  {
        this.firstName = name;
    }

    public void setLocale(Locale local)  {
        this.locale = local;
    }
}
