package com.sap.isa.ui.controllers;

import java.io.IOException;
import java.util.Hashtable;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.sap.isa.auction.helpers.LogSupport;
import com.sap.isa.auction.logging.CategoryProvider;
import com.sap.isa.core.ActionServlet;
import com.sap.isa.core.BaseAction;
import com.sap.isa.core.UserSessionData;
import com.sap.isa.core.util.CodePageUtils;
import com.sap.isa.ui.eventhandlers.ButtonClickedEventHandler;
import com.sap.isa.ui.eventhandlers.CheckboxClickedEventHandler;
import com.sap.isa.ui.eventhandlers.EventHandler;
import com.sap.isa.ui.eventhandlers.ExternalSubmitEventHandler;
import com.sap.isa.ui.eventhandlers.HoverMenuItemClickedEventHandler;
import com.sap.isa.ui.eventhandlers.LinkClickedEventHandler;
import com.sap.isa.ui.eventhandlers.ListSelectEventHandler;
import com.sap.isa.ui.eventhandlers.RadioButtonClickedEventHandler;
import com.sap.isa.ui.eventhandlers.TabSelectedEventHandler;
import com.sap.isa.ui.eventhandlers.TableEventHandler;
import com.sap.isa.ui.eventhandlers.TreeEventHandler;
import com.sap.tc.logging.Category;
import com.sap.tc.logging.Location;
import com.sapportals.htmlb.event.ButtonClickEvent;
import com.sapportals.htmlb.event.CheckboxClickEvent;
import com.sapportals.htmlb.event.Event;
import com.sapportals.htmlb.event.EventModifierData;
import com.sapportals.htmlb.event.ExternalSubmitEvent;
import com.sapportals.htmlb.event.HoverItemClickEvent;
import com.sapportals.htmlb.event.LinkClickEvent;
import com.sapportals.htmlb.event.ListSelectEvent;
import com.sapportals.htmlb.event.RadioButtonClickEvent;
import com.sapportals.htmlb.event.TabSelectEvent;
import com.sapportals.htmlb.event.TableCellClickEvent;
import com.sapportals.htmlb.event.TableHeaderClickEvent;
import com.sapportals.htmlb.event.TableNavigationEvent;
import com.sapportals.htmlb.event.TableRowSelectionEvent;
import com.sapportals.htmlb.event.TreeNodeExpandEvent;
import com.sapportals.htmlb.event.TreeNodeSelectEvent;
import com.sapportals.htmlb.rendering.PageContext;
import com.sapportals.htmlb.rendering.PageContextFactory;
import com.sapportals.htmlb.table.TableViewEventData;


/**
 * Title:
 * Description:
 * Copyright:    Copyright (c) 2002
 * Company:
 * @author
 * @version 1.0
 */

public abstract class Action extends BaseAction  {

  private static final String NOEVENTHANDLER = "No Event handler defined - ";
	private PageContext pageContext;
  private Hashtable eventHandlers = new Hashtable();
  protected HttpSession session;
  protected static Location location;
  protected static Category cat = CategoryProvider.getUICategory();

  public Action() {
      registerEventHandlers();
  }

  public final ActionForward doPerform(ActionMapping mapping,
                                  org.apache.struts.action.ActionForm form,
                                  HttpServletRequest request,
                                  HttpServletResponse response) throws java.io.IOException, javax.servlet.ServletException {

      com.sap.isa.ui.controllers.ActionForm actionForm = (com.sap.isa.ui.controllers.ActionForm)form;
      /*if(request.getAttribute(ActionForm.ACTIONREQUEST) == null)  {
          request.setAttribute(ActionForm.ACTIONREQUEST , mapping.getPath());
          String path = request.getContextPath();
          if(!path.endsWith("/"))  {
              path = path + "/";
          }
          servlet.getServletContext().getRequestDispatcher("/auction/container.jsp").forward(request , response);;
          return mapping.findForward("container");
      }*/
      
	  
	  /*String check = (String)request.getAttribute(RequestProcessor.ENCODED_REQ);
	  if (check != null) {
		 if (check.equals("ENCODE")) {
			String toEncoding = getEncoding(getSAPLang(request));
			if (toEncoding != null)
				request.setCharacterEncoding(toEncoding);
		 }
	  }*/
    
	  location = Location.getLocation(this.getClass());
	
      PageContext pageContext = (PageContext)PageContextFactory.createPageContext(request , response);
      actionForm.setSession(request.getSession());
      actionForm.setResponse(response);
      actionForm.setActionServlet((ActionServlet)servlet);
      actionForm.setAction(this);
      actionForm.setRequest(request);
      actionForm.setPageContext(pageContext);

      session = request.getSession();

	  
      preProcessing(actionForm);
      ActionForward forward = null;
      if(actionForm.getPageContext().getCurrentEvent() != null)  {
          forward = dispatchEventHandler(mapping , (ActionForm)form , request , response);
      }

      if(null == forward)  {
      	startOfNewProcessee();
		forward = doServiceRequest(mapping , (ActionForm)form , request , response);
      }
        

      postProcessing(actionForm);
      request.setAttribute(ActionForm.ACTIONFORMKEY , actionForm.getClass().getName());
      request.setAttribute(actionForm.getClass().getName() , actionForm);

      /*if(forward != null && forward instanceof com.sap.isa.ui.controllers.ActionForward)  {
          com.sap.isa.ui.controllers.ActionForward actionForward = (com.sap.isa.ui.controllers.ActionForward)forward;
          if(actionForward.getShowPageLoading() != null &&
                actionForward.getShowPageLoading().equals("true"))  {
              request.setAttribute(actionForward.FORWARD , actionForward.getPath());
              return mapping.findForward("container");
          }
      }*/
      return forward;
  }
  
  private static String getSAPLang(HttpServletRequest request) {
	  HttpSession session = request.getSession();
	  if (session == null)
		  return null; 
                 
	  UserSessionData userData = UserSessionData.getUserSessionData(session);
	  if (userData == null) 
		  return null;
            
	  return userData.getSAPLanguage();
  }

  private static String getEncoding(String sapLang) {
	  if (sapLang == null)
		 return null;

	  String sapCp = CodePageUtils.getSapCpForSapLanguage(sapLang);
	  if (sapCp == null)
		 return null;
           
	  return CodePageUtils.getHtmlCharsetForSapCp(sapCp);
  }


  protected abstract ActionForward doServiceRequest(ActionMapping mapping,
                                  ActionForm actionForm,
                                  HttpServletRequest request,
                                  HttpServletResponse response) throws IOException , ServletException;


  protected abstract void preProcessing(ActionForm form);
  protected abstract void postProcessing(ActionForm form);
  protected void startOfNewProcessee()  {
  	
  }

  protected void registerEventHandlers()  {

  }

  protected ActionForward dispatchEventHandler(ActionMapping mapping,
                                  ActionForm actionForm,
                                  HttpServletRequest request,
                                  HttpServletResponse response) throws IOException , ServletException  {


      Event event = actionForm.getCurrentEvent();
      if(null == event)
          return null;

      if(event instanceof ButtonClickEvent) {
          return doHandleButtonClickedEvent(mapping , actionForm , event , request , response);
      } else if (event instanceof RadioButtonClickEvent){
          return doHandleRadioButtonClickedEvent(mapping , actionForm , event , request , response);
      }  else if(event instanceof TableCellClickEvent || event instanceof TableHeaderClickEvent
                  || event instanceof TableNavigationEvent || event instanceof TableRowSelectionEvent)  {
          return doHandleTableEvents(mapping , actionForm , event , request , response);
      } else if(event instanceof TreeNodeExpandEvent || event instanceof TreeNodeSelectEvent)  {
          return doHandleTreeEvents(mapping , actionForm , event , request , response);
      }  else  if(event instanceof LinkClickEvent)  {
          return doHandleLinkEvents(mapping , actionForm , event , request , response);
      }  else  if(event instanceof TabSelectEvent)  {
          return doHandleTabSelectedEvent(mapping , actionForm , event , request , response);
      } else if(event instanceof ListSelectEvent)  {
          return doHandlerListSelectEvent(mapping , actionForm , event , request , response);
      }  else if(event instanceof ExternalSubmitEvent)  {
		return doHandlerExternalSubmitEvent(mapping , actionForm , event , request , response);
      }  else if(event instanceof HoverItemClickEvent)  {
		return doHandlerHoverItemClickedEvent(mapping , actionForm , event , request , response);
      }  else if(event instanceof CheckboxClickEvent)  {
		return doHandlerCheckboxClickEvent(mapping , actionForm , event , request , response);
      }
      return new ActionForward(mapping.getInput());
  }

    /**
 * @param mapping
 * @param actionForm
 * @param event
 * @param request
 * @param response
 * @return
 */
	private ActionForward doHandlerHoverItemClickedEvent(ActionMapping mapping, ActionForm actionForm, Event event, HttpServletRequest request, HttpServletResponse response) 
		throws IOException , ServletException  {
	
		String compid = event.getComponentName();
		EventModifierData modData = event.getModifierData();
		if(modData instanceof TableViewEventData) {
			compid = modData.getComponentName();
		}
		Object value = eventHandlers.get(compid);
		if(value != null) {
			LogSupport.logInfo(cat , location , value.toString());
			if(value instanceof HoverMenuItemClickedEventHandler) {
				// special handling for events generated from table in UI
				return ((HoverMenuItemClickedEventHandler)value).processEvent(mapping,
										actionForm,event,request,response);
			}
		}  else  {
			LogSupport.logInfo(cat , location , NOEVENTHANDLER + compid);
		}
		return null;
	
	}

/**
     * Default back to doPerform
     * Specific Actions can override to get the event directly
     */
    public final ActionForward doHandleButtonClickedEvent(ActionMapping mapping,
                                             ActionForm form,
                                             Event event,
                                             HttpServletRequest request,
                                             HttpServletResponse response)
          throws IOException, ServletException {
        String compid = event.getComponentName();
        EventModifierData modData = event.getModifierData();
        if(modData instanceof TableViewEventData) {
            //compid = modData.getComponentName();
        }
        Object value = eventHandlers.get(compid);
        if(value != null) {
        	LogSupport.logInfo(cat , location , value.toString());
            if(modData instanceof TableViewEventData && value instanceof TableEventHandler)  {
                return ((TableEventHandler)value).processEvent(mapping , form , event , request , response);
            }
            if(value instanceof ButtonClickedEventHandler) {
                return ((ButtonClickedEventHandler)value).processEvent(mapping,
                                        form,event,request,response);
            }
        }  else  {
        	LogSupport.logInfo(cat , location , NOEVENTHANDLER + compid);
        }
        return null;
    }

    public final ActionForward doHandleRadioButtonClickedEvent(ActionMapping mapping,
                                             ActionForm form,
                                             Event event,
                                             HttpServletRequest request,
                                             HttpServletResponse response)
          throws IOException, ServletException {
        String compid = event.getComponentName();
        String eventAction = event.getAction();
        EventModifierData modData = event.getModifierData();
        if(modData instanceof TableViewEventData) {
            //compid = modData.getComponentName();
        }
        // Use the action value which will be the radiogroup id. The event key can be
        // used to determine which button was selected
        Object value = eventHandlers.get(eventAction);
        if(value != null) {
            LogSupport.logInfo(cat , location , value.toString());
            if (modData instanceof TableViewEventData && 
                    value instanceof TableEventHandler)  {
                return ((TableEventHandler)value).processEvent(
                        mapping , form , event , request , response);
            }
            if(value instanceof RadioButtonClickedEventHandler) {
                return ((RadioButtonClickedEventHandler)value).processEvent(mapping,
                                        form,event,request,response);
            }
        }  else  {
            LogSupport.logInfo(cat , location , NOEVENTHANDLER + compid);
        }
        return null;
    }

    public final ActionForward doHandleTableEvents(ActionMapping mapping,
                                             ActionForm form,
                                             Event event,
                                             HttpServletRequest request,
                                             HttpServletResponse response)
          throws IOException, ServletException {
        String compid = event.getComponentName();
        EventModifierData modData = event.getModifierData();
        if(modData instanceof TableViewEventData) {
            compid = modData.getComponentName();
        }
        Object value = eventHandlers.get(compid);
        if(value != null) {
			LogSupport.logInfo(cat , location , value.toString());
            if(value instanceof TableEventHandler) {
                // special handling for events generated from table in UI
                return ((TableEventHandler)value).processEvent(mapping,
                                        form,event,request,response);
            }
        }  else  {
			LogSupport.logInfo(cat , location , NOEVENTHANDLER + compid);
        }
        return null;
    }

    public final ActionForward doHandleTreeEvents(ActionMapping mapping,
                                             ActionForm form,
                                             Event event,
                                             HttpServletRequest request,
                                             HttpServletResponse response)
          throws IOException, ServletException {
        String compid = event.getComponentName();
        EventModifierData modData = event.getModifierData();
        if(modData instanceof TableViewEventData) {
            compid = modData.getComponentName();
        }
        Object value = eventHandlers.get(compid);
        if(value != null) {
			LogSupport.logInfo(cat , location , value.toString());
            if(value instanceof TreeEventHandler) {
                // special handling for events generated from table in UI
                return ((TreeEventHandler)value).processEvent(mapping,
                                        form,event,request,response);
            }
        }  else  {
			LogSupport.logInfo(cat , location , NOEVENTHANDLER + compid);
        }
        return null;
    }

    public final ActionForward doHandleLinkEvents(ActionMapping mapping,
                                             ActionForm form,
                                             Event event,
                                             HttpServletRequest request,
                                             HttpServletResponse response)
          throws IOException, ServletException {
        String compid = event.getComponentName();
        EventModifierData modData = event.getModifierData();
        if(modData instanceof TableViewEventData) {
            //compid = modData.getComponentName();
        }
        Object value = eventHandlers.get(compid);
        if(value != null) {
			LogSupport.logInfo(cat , location , value.toString());
            if(value instanceof LinkClickedEventHandler) {
                // special handling for events generated from table in UI
                return ((LinkClickedEventHandler)value).processEvent(mapping,
                                        form,event,request,response);
            }
        }  else  {
			LogSupport.logInfo(cat , location , NOEVENTHANDLER + compid);
        }
        return null;
    }

        /**
     * Default back to doPerform
     * Specific Actions can override to get the event directly
     */
    public final ActionForward doHandleTabSelectedEvent(ActionMapping mapping,
                                             ActionForm form,
                                             Event event,
                                             HttpServletRequest request,
                                             HttpServletResponse response)
          throws IOException, ServletException {
        String compid = event.getComponentName();
        EventModifierData modData = event.getModifierData();
        if(modData instanceof TableViewEventData) {
            compid = modData.getComponentName();
        }
        Object value = eventHandlers.get(compid);
        if(value != null) {
			LogSupport.logInfo(cat , location , value.toString());
            if(value instanceof TabSelectedEventHandler) {
                // special handling for events generated from table in UI
                return ((TabSelectedEventHandler)value).processEvent(mapping,
                                        form,event,request,response);
            }
        }  else  {
			LogSupport.logInfo(cat , location , NOEVENTHANDLER + compid);
        }
        return null;
    }


        /**
     * Default back to doPerform
     * Specific Actions can override to get the event directly
     */
    public final ActionForward doHandlerListSelectEvent(ActionMapping mapping,
                                             ActionForm form,
                                             Event event,
                                             HttpServletRequest request,
                                             HttpServletResponse response)
          throws IOException, ServletException {
        String compid = event.getComponentName();
        EventModifierData modData = event.getModifierData();
//        if(modData instanceof TableViewEventData) {
//            compid = modData.getComponentName();
//        }
        Object value = eventHandlers.get(compid);
        if(value != null) {
			LogSupport.logInfo(cat , location , value.toString());
            if(value instanceof ListSelectEventHandler) {
                // special handling for events generated from table in UI
                return ((ListSelectEventHandler)value).processEvent(mapping,
                                        form,event,request,response);
            }
        }  else  {
			LogSupport.logInfo(cat , location , NOEVENTHANDLER + compid);
        }
        return null;
    }

	public final ActionForward doHandlerExternalSubmitEvent(ActionMapping mapping,
											 ActionForm form,
											 Event event,
											 HttpServletRequest request,
											 HttpServletResponse response)
		  throws IOException, ServletException {
		String compid = event.getComponentName();
		EventModifierData modData = event.getModifierData();
		if(modData instanceof TableViewEventData) {
			compid = modData.getComponentName();
		}
		Object value = eventHandlers.get(compid);
		if(value != null) {
			LogSupport.logInfo(cat , location , value.toString());
			if(value instanceof ExternalSubmitEventHandler) {
				// special handling for events generated from table in UI
				return ((ExternalSubmitEventHandler)value).processEvent(mapping,
										form,event,request,response);
			}
		}  else  {
			LogSupport.logInfo(cat , location , NOEVENTHANDLER + compid);
		}
		return null;
	}

	public final ActionForward doHandlerCheckboxClickEvent(ActionMapping mapping,
											 ActionForm form,
											 Event event,
											 HttpServletRequest request,
											 HttpServletResponse response)
		  throws IOException, ServletException {
		String compid = event.getComponentName();
		EventModifierData modData = event.getModifierData();
		if(modData instanceof TableViewEventData) {
			compid = modData.getComponentName();
		}
		Object value = eventHandlers.get(compid);
		if(value != null) {
			LogSupport.logInfo(cat , location , value.toString());
			if(value instanceof CheckboxClickedEventHandler) {
				// special handling for events generated from table in UI
				return ((CheckboxClickedEventHandler)value).processEvent(mapping,
										form,event,request,response);
			}
		}  else  {
			LogSupport.logInfo(cat , location , NOEVENTHANDLER + compid);
		}
		return null;
	}
    /**
    * Register a button event handler with the system
    * @param compid The source of the event, should be the same as used in the JSP page
    * @param bevhdlr The ButtonEventHandler: Handles events from buttons in a form
    */
    public void registerEventHandler(String compid,
                                     EventHandler bevhdlr)  {
        String key = compid;
        eventHandlers.put(key, bevhdlr);
    }

}
