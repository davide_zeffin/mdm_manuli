package com.sap.isa.ui.controllers;

/**
 * Title:
 * Description:
 * Copyright:    Copyright (c) 2002
 * Company:
 * @author
 * @version 1.0
 */

public class ActionForward extends org.apache.struts.action.ActionForward  {

  public static final String FORWARD = "_forward_page";
  public static final String LOADPAGE = "_load_Page";
  private String showPageLoad = "false";


  public ActionForward() {
      super();
  }

  public void setShowPageLoading(String flag)  {
      showPageLoad = flag;
  }

  public String getShowPageLoading()  {
      return showPageLoad;
  }
}