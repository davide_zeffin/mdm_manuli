package com.sap.isa.ui.controllers;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.Locale;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.sap.isa.auction.logging.CategoryProvider;
import com.sap.isa.auction.logging.LogHelper;
import com.sap.isa.core.ActionServlet;
import com.sap.tc.logging.Category;
import com.sap.tc.logging.Location;
import com.sapportals.htmlb.Button;
import com.sapportals.htmlb.ButtonRow;
import com.sapportals.htmlb.Checkbox;
import com.sapportals.htmlb.Component;
import com.sapportals.htmlb.Container;
import com.sapportals.htmlb.DropdownListBox;
import com.sapportals.htmlb.Group;
import com.sapportals.htmlb.InputField;
import com.sapportals.htmlb.ItemList;
import com.sapportals.htmlb.Link;
import com.sapportals.htmlb.ListBox;
import com.sapportals.htmlb.RadioButton;
import com.sapportals.htmlb.RadioButtonGroup;
import com.sapportals.htmlb.ScrollContainer;
import com.sapportals.htmlb.TabStrip;
import com.sapportals.htmlb.TextEdit;
import com.sapportals.htmlb.TextView;
import com.sapportals.htmlb.Tree;
import com.sapportals.htmlb.enum.DataType;
import com.sapportals.htmlb.event.Event;
import com.sapportals.htmlb.rendering.IPageContext;
import com.sapportals.htmlb.rendering.PageContext;
import com.sapportals.htmlb.table.TableView;
import com.sapportals.htmlb.type.DataInteger;

/**
 * Title:
 * Description:
 * Copyright:    Copyright (c) 2002
 * Company:
 * @author
 * @version 1.0
 */

public class ActionForm extends org.apache.struts.action.ActionForm {

	static {
		System.setProperty(
			"com.sap.mobile.clientinfo.ClientInfoImpl",
			"com.sap.isa.ui.controllers.ClientInfo");
	}

	public static final String ACTIONFORMKEY = "actionFormKey.ui.isa.sap.com";
	public static final String ACTIONREQUEST = "_action_request";
	public static final String ACTIONFORM = "actionForm";

	public static final Class HTMLB_INPUTFIELD = InputField.class;
	public static final Class HTMLB_DROPDOWN = DropdownListBox.class;
	public static final Class HTMLB_LISTBOX = ListBox.class;
	public static final Class HTMLB_ITEMLIST = ItemList.class;
	public static final Class HTMLB_TEXTVIEW = TextView.class;
	public static final Class HTMLB_CHECKBOX = Checkbox.class;
	public static final Class HTMLB_RADIOBUTTONGRP = RadioButtonGroup.class;
	public static final Class HTMLB_TEXTEDIT = TextEdit.class;
	public static final Class HTMLB_BUTTON = Button.class;
	public static final Class HTMLB_SCROLLCONTAINER = ScrollContainer.class;
	public static final Class HTMLB_LINK = Link.class;
	public static final Class HTMLB_TABSTRIP = TabStrip.class;
	public static final Class HTMLB_TREE = Tree.class;
	public static final Class HTMLB_GROUP = Group.class;
	public static final Class HTMLB_BUTTONROW = ButtonRow.class;
	public static final Class HTMLB_TABLEVIEW = TableView.class;

	private IPageContext pageContext = null;
	protected ActionServlet servlet = null;
	private LinkedList componentslist = new LinkedList();
	protected HttpSession session;
	protected Action action;
	protected HttpServletRequest request;
	protected HttpServletResponse response;
	protected boolean showError = false;
	public LoginUser user;

	protected static Location location;
	protected static Category cat = CategoryProvider.getUICategory();

	public ActionForm() {
		user = new LoginUser();
	}

	protected void initialize() {
	}

	public void setPageContext(IPageContext pageContex) {
		pageContext = pageContex;
		initialize();
	}

	public IPageContext getPageContext() {
		return pageContext;
	}

	public void setActionServlet(ActionServlet servlet) {
		this.servlet = servlet;
	}

	public void setAttribute(String id, Object obj) {
		pageContext.setAttribute(id, obj);
		componentslist.add(id);
	}

	public Object getAttribute(String id) {
		return pageContext.getAttribute(id);
	}

	public Object getComponentForId(String id) {
		return pageContext.getComponentForId(id);
	}

	public Event getCurrentEvent() {
		return pageContext.getCurrentEvent();
	}

	public void finalize() throws Throwable {
		Iterator iterate = componentslist.iterator();
		while (iterate.hasNext()) {
			pageContext.setAttribute((String) iterate.next(), null);
		}
		super.finalize();
	}

	public void setSession(HttpSession sesn) {
		this.session = sesn;
	}

	public HttpSession getSession() {
		return session;
	}

	public void setAction(Action action) {
		this.action = action;
	}

	public void setResponse(HttpServletResponse response) {
		this.response = response;
	}

	public void setRequest(HttpServletRequest request) {
		this.request = request;
	}

	public Action getAction() {
		return action;
	}

	public void setLocale(Locale locale) {
		((PageContext) pageContext).setLocale(locale);
		LogHelper.logInfo(
						cat,
						location,
						"Locale set on HTMLB PageContext: " + locale.toString(),  
						null);
	}

	public void hideThisControl(String id) {
		user.addControlIDToBeHidden(id);
	}
	public void removeAttributes() {
		Iterator iterate = componentslist.iterator();
		while (iterate.hasNext()) {
			pageContext.setAttribute((String) iterate.next(), null);
		}
	}
	protected Object addControl(Class control, String id) {
		try {
			Object object = this.getAttribute(id);
			if (this.getAttribute(id) != null
				&& this.getAttribute(id).getClass() == control)
				return object;
			Class parameterTypes[] = { String.class };
			Object parameterValues[] = { id };
			for (int i = 0; i < control.getConstructors().length; i++) {
				if (control.getConstructors()[i].getParameterTypes().length
					== 1
					&& control
						.getConstructors()[i]
						.getParameterTypes()[0]
						.getName()
						.equals(
						String.class.getName())) {
					object =
						control.getConstructor(parameterTypes).newInstance(
							parameterValues);
					break;
				} else if (
					control.getConstructors()[i].getParameterTypes().length
						== 0) {
					object = control.newInstance();
					//getConstructor(parameterTypes).newInstance(parameterValues);
					break;
				}
			}
			if (object instanceof Component) {
				((Component) object).setId(id);
			}
			this.setAttribute(id, object);
			return object;
		} catch (Exception exc) {
			//System.out.println(exc);
			LogHelper.logError(cat, location, exc);
		}
		return null;
	}

	public boolean isControlApplicable(String controlId) {
		return true;
	}

	public int getTableViewVisibleRowCount() {
		return 0;
	}

    /*
     * This can be used for Inputfields and Dropdown selection
     */
    protected String getValue(String id) {
        String value = null;
        try {
            Object obj = getComponentForId(id);
            if (obj instanceof InputField) {
                if (id.equals("INPUTID_DATE.SearchForm")) {
                    return ((InputField) obj).getDate().getValueAsString();
                }
                return ((InputField) obj).getString().getValueAsString();
            } else if (obj instanceof DropdownListBox) {
                return ((DropdownListBox) obj).getSelection();
            } else if (obj instanceof RadioButtonGroup) {
                return ((RadioButtonGroup) obj).getSelection();
            } else if (obj instanceof TextEdit) {
                return ((TextEdit) obj).getText();
            }
        } catch (Exception exc) {
            LogHelper.logWarning(
                cat,
                location,
                "Unable to get the component for id - " + id,
                exc);
        }
        return value;
    }

    /*
     * This can be used for Inputfields and Dropdown selection
     */
    protected int getIntValue(String id) {
        int value = 0;
        try {
            Object obj = getComponentForId(id);
            if (obj instanceof InputField) {
                if (((InputField) obj).getType().equals(DataType.INTEGER)) {
                    DataInteger val = (DataInteger)((InputField) obj).getValue();
                    value = val.getValue();
                }
                else {
                    throw  new Exception("Not an DataType.INTEGER");
                }
            }
        } catch (Exception exc) {
            LogHelper.logWarning(
                cat,
                location,
                "Unable to get the component for id - " + id,
                exc);
        }
        return value;
    }

	protected boolean isSelected(String id) {
		boolean value = false;
		try {
			Object obj = getComponentForId(id);
			if (obj instanceof RadioButton) {
				return ((RadioButton) obj).isSelected();
			} else if (obj instanceof Checkbox) {
				return ((Checkbox) obj).isChecked();
			}
		} catch (Exception exc) {
			LogHelper.logWarning(
				cat,
				location,
				"Unable to get the component for id - " + id,
				exc);
		}
		return value;
	}

	public void startOfNewProcessee() {

	}
	public String translate(String resourceId, String key) {
		return key;
	}

	public String translate(String key) {
		return key;
	}

	protected void clearContainer(Container container)  {
		Iterator iterator = container.iterator();
		ArrayList list = new ArrayList();
		while(iterator.hasNext())  {
			list.add(iterator.next());
		}
		iterator = list.iterator();
		while(iterator.hasNext())  {
			container.removeComponent((Component)iterator.next());
		}
	}
}
