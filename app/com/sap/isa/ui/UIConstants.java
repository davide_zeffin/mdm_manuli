package com.sap.isa.ui;

/**
 * Title:
 * Description:
 * Copyright:    Copyright (c) 2002
 * Company:
 * @author
 * @version 1.0
 */

public interface UIConstants {
    public static final String PAGELOADING = "Page.Loading";
    public static final String PAGEPROCESSING = "Page.Processing";

    public static final String LOADINGBOXHEADER = "Header.Loading";
    public static final String LOADINGBOXBODY = "Body.Loading";
}
