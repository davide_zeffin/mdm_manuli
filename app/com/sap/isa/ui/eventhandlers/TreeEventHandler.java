package com.sap.isa.ui.eventhandlers;

import com.sap.isa.ui.eventhandlers.EventHandler;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import com.sap.isa.ui.controllers.ActionForm;

import com.sapportals.htmlb.event.Event;
import com.sapportals.htmlb.event.TreeNodeExpandEvent;
import com.sapportals.htmlb.event.TreeNodeSelectEvent;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import javax.servlet.ServletException;
import java.io.IOException;

/**
 * Title:
 * Description:
 * Copyright:    Copyright (c) 2002
 * Company:
 * @author
 * @version 1.0
 */

public class TreeEventHandler extends BaseEventHandler  implements EventHandler {

  public TreeEventHandler() {
  	super();
  }

  public ActionForward doService(ActionMapping mapping,
                                ActionForm form,
                                Event event,
                                HttpServletRequest request,
                                HttpServletResponse response)
                                throws ServletException, IOException {
      if(event instanceof TreeNodeExpandEvent)  {
          return treeNodeExpandHandler(mapping , form , (TreeNodeExpandEvent)event , request , response);
      }  else if(event instanceof TreeNodeSelectEvent)  {
          return treeNodeSelectHandler(mapping , form , (TreeNodeSelectEvent)event , request , response);
      }
      return new ActionForward(mapping.getInput());
  }

  public ActionForward treeNodeExpandHandler(ActionMapping mapping,
                                ActionForm form,
                                TreeNodeExpandEvent event,
                                HttpServletRequest request,
                                HttpServletResponse response)
                                throws ServletException, IOException {
      return new ActionForward(mapping.getInput());
  }

  public ActionForward treeNodeSelectHandler(ActionMapping mapping,
                                ActionForm form,
                                TreeNodeSelectEvent event,
                                HttpServletRequest request,
                                HttpServletResponse response)
                                throws ServletException , IOException  {
      return new ActionForward(mapping.getInput());
  }



}