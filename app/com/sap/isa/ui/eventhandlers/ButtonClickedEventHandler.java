package com.sap.isa.ui.eventhandlers;

import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionForward;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.ServletException;

import com.sap.isa.ui.controllers.ActionForm;

import com.sapportals.htmlb.event.Event;
import com.sapportals.htmlb.event.ButtonClickEvent;

import java.io.IOException;

/**
 * Title:
 * Description:
 * Copyright:    Copyright (c) 2002
 * Company:
 * @author
 * @version 1.0
 */

public class ButtonClickedEventHandler extends BaseEventHandler  implements EventHandler {

  public ButtonClickedEventHandler() {
  	super();
  }

  public final ActionForward doService(ActionMapping mapping,
                                              ActionForm form,
                                              Event event,
                                              HttpServletRequest request,
                                              HttpServletResponse response)
                                              throws ServletException , IOException  {
      if(event instanceof ButtonClickEvent)  {
          return buttonClickedEvent(mapping , form , (ButtonClickEvent)event , request , response);
      }
      //If there are no listeners for this event, then just forward to the same page where the event
      //was triggered
      //Please maintain in the struts config xml, the input parameter for all actions with
      //relevant jsps
      return new ActionForward(mapping.getInput());
  }

  public ActionForward buttonClickedEvent(ActionMapping mapping,
                                              ActionForm form,
                                              ButtonClickEvent event,
                                              HttpServletRequest request,
                                              HttpServletResponse response)
                                              throws ServletException , IOException  {

        return new ActionForward(mapping.getInput());
  }

}