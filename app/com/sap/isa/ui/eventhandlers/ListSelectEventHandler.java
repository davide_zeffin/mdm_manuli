package com.sap.isa.ui.eventhandlers;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.sap.isa.ui.controllers.ActionForm;
import com.sapportals.htmlb.event.Event;
import com.sapportals.htmlb.event.ListSelectEvent;

/**
 * Title:
 * Description:
 * Copyright:    Copyright (c) 2003
 * Company:
 * @author
 * @version 1.0
 */

public class ListSelectEventHandler extends BaseEventHandler implements EventHandler {

    public ListSelectEventHandler() {
    	super();
    }

    public final ActionForward doService(ActionMapping mapping,
                                  ActionForm form,
                                  Event event,
                                  HttpServletRequest request,
                                  HttpServletResponse response)
                                  throws ServletException, IOException {
      if(event instanceof ListSelectEvent)  {
          return listSelectEvent(mapping , form , (ListSelectEvent) event , request , response);
      }
      return new ActionForward(mapping.getInput());
    }

    public ActionForward listSelectEvent(ActionMapping mapping,
                                          ActionForm form,
                                          ListSelectEvent event,
                                          HttpServletRequest request,
                                          HttpServletResponse response)
                                          throws IOException , ServletException  {

      return new ActionForward(mapping.getInput());
    }

}
