package com.sap.isa.ui.eventhandlers;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.sap.isa.ui.controllers.ActionForm;
import com.sapportals.htmlb.event.Event;
import com.sapportals.htmlb.event.RadioButtonClickEvent;

/**
 * Title:			Internet Sales Selling Via eBay
 * Description:
 * @Copyright: 		Copyright (c) 2004
 * Company:			SAPLabs LLC
 * Created on:		Oct 26, 2004
 */
public class RadioButtonClickedEventHandler extends BaseEventHandler implements EventHandler {

    public RadioButtonClickedEventHandler(){
        super();
    }

    /* (non-Javadoc)
     * @see com.sap.isa.ui.eventhandlers.EventHandler#doService(org.apache.struts.action.ActionMapping, com.sap.isa.ui.controllers.ActionForm, com.sapportals.htmlb.event.Event, javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse)
     */
    public ActionForward doService(
        ActionMapping mapping,
        ActionForm form,
        Event event,
        HttpServletRequest request,
        HttpServletResponse response)
        throws ServletException, IOException {
        if (event instanceof RadioButtonClickEvent)  {
            return radioButtonClickedEvent(mapping , 
                                                            form,
                                                            (RadioButtonClickEvent)event, 
                                                            request, 
                                                            response);
        }
        //If there are no listeners for this event, then just forward to the same page where the event
        //was triggered
        //Please maintain in the struts config xml, the input parameter for all actions with
        //relevant jsps
        return new ActionForward(mapping.getInput());
    }
    
    public ActionForward radioButtonClickedEvent(ActionMapping mapping,
            ActionForm form,RadioButtonClickEvent event, HttpServletRequest request,
            HttpServletResponse response)
            throws ServletException , IOException  {

          return new ActionForward(mapping.getInput());
    }

}
