package com.sap.isa.ui.eventhandlers;

import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import com.sap.isa.ui.controllers.ActionForm;

import com.sapportals.htmlb.event.Event;
import com.sapportals.htmlb.event.LinkClickEvent;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.ServletException;

import java.io.IOException;

/**
 * Title:
 * Description:
 * Copyright:    Copyright (c) 2002
 * Company:
 * @author
 * @version 1.0
 */

public class LinkClickedEventHandler extends BaseEventHandler  implements EventHandler {

  public LinkClickedEventHandler() {
  	super();
  }

  public final ActionForward doService(ActionMapping mapping,
                                  ActionForm form,
                                  Event event,
                                  HttpServletRequest request,
                                  HttpServletResponse response)
                                  throws ServletException, IOException {
      if(event instanceof LinkClickEvent)  {
          return linkClickedEvent(mapping , form , (LinkClickEvent) event , request , response);
      }
      return new ActionForward(mapping.getInput());
  }

  public ActionForward linkClickedEvent(ActionMapping mapping,
                                          ActionForm form,
                                          LinkClickEvent event,
                                          HttpServletRequest request,
                                          HttpServletResponse response)
                                          throws IOException , ServletException  {

      return new ActionForward(mapping.getInput());
  }
}