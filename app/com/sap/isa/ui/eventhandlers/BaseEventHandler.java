/*
 * Created on Mar 24, 2004
 *
 * To change the template for this generated file go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
package com.sap.isa.ui.eventhandlers;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.sap.isa.auction.helpers.LogSupport;
import com.sap.isa.ui.controllers.ActionForm;
import com.sapportals.htmlb.event.Event;

/**
 * @author I803067
 *
 * To change the template for this generated type comment go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
public class BaseEventHandler extends LogSupport implements EventHandler  {

	public BaseEventHandler()  {
		super();
	}
	
	public ActionForward processEvent(
			ActionMapping mapping,
			ActionForm form,
			Event event,
			HttpServletRequest request,
			HttpServletResponse response)
			throws ServletException, IOException {
		
			final String className = this.getClass().getName();
			entering(cat , location , className , null);
			ActionForward forward = doService(mapping , form , event ,
												request , response);
			exiting(cat , location , className , null);
			return forward;
	}
		
	public ActionForward doService(
		ActionMapping mapping,
		ActionForm form,
		Event event,
		HttpServletRequest request,
		HttpServletResponse response)
		throws ServletException, IOException {
		
		return null;
	}

}
