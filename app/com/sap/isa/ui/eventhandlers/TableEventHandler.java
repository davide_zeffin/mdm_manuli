package com.sap.isa.ui.eventhandlers;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.sap.isa.ui.components.table.TableModel;
import com.sap.isa.ui.controllers.ActionForm;
import com.sapportals.htmlb.event.ButtonClickEvent;
import com.sapportals.htmlb.event.Event;
import com.sapportals.htmlb.event.TableCellClickEvent;
import com.sapportals.htmlb.event.TableHeaderClickEvent;
import com.sapportals.htmlb.event.TableNavigationEvent;
import com.sapportals.htmlb.table.TableView;

/**
 * Title:
 * Description:
 * Copyright:    Copyright (c) 2002
 * Company:
 * @author
 * @version 1.0
 */

public class TableEventHandler extends BaseEventHandler  implements EventHandler {

  public TableEventHandler() {
  	super();
  }

  public final ActionForward doService(ActionMapping mapping,
                                              ActionForm form,
                                              Event event,
                                              HttpServletRequest request,
                                              HttpServletResponse response)
                                              throws ServletException , IOException  {
      if(event instanceof TableCellClickEvent)  {
          return tableCellClickedEvent(mapping , form , (TableCellClickEvent)event , request , response);
      }
      if(event instanceof TableHeaderClickEvent)  {
          return tableHeaderClickedEvent(mapping , form , (TableHeaderClickEvent)event , request , response);
      }
      if(event instanceof TableNavigationEvent)  {
          return tableNavigationEvent(mapping , form , (TableNavigationEvent)event , request , response);
      }
      if(event instanceof ButtonClickEvent)  {
          return tableCellButtonClickedEvent(mapping , form , (ButtonClickEvent)event , request , response);
      }
      //If there are no listeners for this event, then just forward to the same page where the event
      //was triggered
      //Please maintain in the struts config xml, the input parameter for all actions with
      //relevant jsps
      return new ActionForward(mapping.getInput());
  }

  /**
   * Override this method to handle the tablecell clicked event
   */

  public ActionForward tableCellClickedEvent(ActionMapping mapping,
                                              ActionForm form,
                                              TableCellClickEvent event,
                                              HttpServletRequest request,
                                              HttpServletResponse response)
                                              throws ServletException , IOException  {
      //if there are no listeners
      return new ActionForward(mapping.getInput());
  }

  /**
   * Override this method to handle table headerclicked event
   */

  public ActionForward tableHeaderClickedEvent(ActionMapping mapping,
                                              ActionForm form,
                                              TableHeaderClickEvent event,
                                              HttpServletRequest request,
                                              HttpServletResponse response)
                                              throws ServletException , IOException  {
      //if there are no listeners
      return new ActionForward(mapping.getInput());
  }

  public ActionForward tableCellButtonClickedEvent(ActionMapping mapping,
                                              ActionForm form,
                                              ButtonClickEvent event,
                                              HttpServletRequest request,
                                              HttpServletResponse response)
                                              throws ServletException , IOException  {
      //if there are no listeners
      return new ActionForward(mapping.getInput());
  }

  /**
   * Over ride this method to handle Navigation event
   * If this method is not override, default navigation action will be taken automatically
   */

  public ActionForward tableNavigationEvent(ActionMapping mapping,
                                              ActionForm form,
                                              TableNavigationEvent event,
                                              HttpServletRequest request,
                                              HttpServletResponse response)
                                              throws ServletException , IOException  {

      TableView oldView = (TableView)form.getComponentForId(event.getComponentName());
      TableView view = (TableView)form.getAttribute(event.getComponentName());
      TableModel tableModel = (TableModel)view.getModel();
      tableModel.updateSelection(oldView);
	  tableModel.setVisibleRowCount(view.getVisibleRowCount());
      tableModel.setVisibleFirstRow(event.getFirstVisibleRowAfter());
      return new ActionForward(mapping.getInput());
  }
}

