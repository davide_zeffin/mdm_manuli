/*
 * Created on Jan 29, 2004
 *
 * To change the template for this generated file go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
package com.sap.isa.ui.eventhandlers;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.sap.isa.ui.controllers.ActionForm;
import com.sapportals.htmlb.event.Event;
import com.sapportals.htmlb.event.ExternalSubmitEvent;

/**
 * @author I803067
 *
 * To change the template for this generated type comment go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
public class ExternalSubmitEventHandler extends BaseEventHandler  implements EventHandler {

	public ExternalSubmitEventHandler()  {
		super();
	}
	
	public ActionForward doService(
		ActionMapping mapping,
		ActionForm form,
		Event event,
		HttpServletRequest request,
		HttpServletResponse response)
		throws ServletException, IOException {
		if(event instanceof ExternalSubmitEvent)  {
			return externalSubmitEvent(mapping , form , (ExternalSubmitEvent) event , 
								request , response);
		}
		return null;
	}

	public ActionForward externalSubmitEvent(ActionMapping mapping,
												ActionForm form,
												ExternalSubmitEvent event,
												HttpServletRequest request,
												HttpServletResponse response)
												throws ServletException , IOException  {

		  return new ActionForward(mapping.getInput());
	}
}
