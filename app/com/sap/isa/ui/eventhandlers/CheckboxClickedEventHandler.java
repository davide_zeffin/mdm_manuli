/*
 * Created on Apr 19, 2005
 *
 * To change the template for this generated file go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
package com.sap.isa.ui.eventhandlers;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.sap.isa.ui.controllers.ActionForm;
import com.sapportals.htmlb.event.CheckboxClickEvent;
import com.sapportals.htmlb.event.Event;

/**
 * @author i803067
 *
 * To change the template for this generated type comment go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
public class CheckboxClickedEventHandler extends BaseEventHandler implements EventHandler  {
	

	/* (non-Javadoc)
	 * @see com.sap.isa.ui.eventhandlers.EventHandler#doService(org.apache.struts.action.ActionMapping, com.sap.isa.ui.controllers.ActionForm, com.sapportals.htmlb.event.Event, javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse)
	 */
	public ActionForward doService(
		ActionMapping mapping,
		ActionForm form,
		Event event,
		HttpServletRequest request,
		HttpServletResponse response)
		throws ServletException, IOException {
		if(event instanceof CheckboxClickEvent)  {
			return checkbxClickedEvent(mapping , form , (CheckboxClickEvent)event , request , response);
		}
		return new ActionForward(mapping.getInput());
	}

	public ActionForward checkbxClickedEvent(ActionMapping mapping,
												ActionForm form,
												CheckboxClickEvent event,
												HttpServletRequest request,
												HttpServletResponse response)
												throws ServletException , IOException  {

		  return new ActionForward(mapping.getInput());
	}

}
