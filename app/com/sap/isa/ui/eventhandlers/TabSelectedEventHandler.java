package com.sap.isa.ui.eventhandlers;

import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import com.sap.isa.ui.controllers.ActionForm;

import com.sapportals.htmlb.event.Event;
import com.sapportals.htmlb.event.TabSelectEvent;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.ServletException;

import java.io.IOException;

/**
 * Title:
 * Description:
 * Copyright:    Copyright (c) 2002
 * Company:
 * @author
 * @version 1.0
 */

public class TabSelectedEventHandler extends BaseEventHandler implements EventHandler {

    public TabSelectedEventHandler() {
    	super();
    }

    public final ActionForward doService(ActionMapping mapping,
                                  ActionForm form,
                                  Event event,
                                  HttpServletRequest request,
                                  HttpServletResponse response)
                                  throws ServletException, IOException {
        if(event instanceof TabSelectEvent)  {
            return tabSelectedEvent(mapping , form , (TabSelectEvent) event , request , response);
        }
        return new ActionForward(mapping.getInput());
    }

    public ActionForward tabSelectedEvent(ActionMapping mapping,
                                          ActionForm form,
                                          TabSelectEvent event,
                                          HttpServletRequest request,
                                          HttpServletResponse response)
                                          throws IOException , ServletException  {

        return new ActionForward(mapping.getInput());
    }
}