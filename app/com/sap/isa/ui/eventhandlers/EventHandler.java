package com.sap.isa.ui.eventhandlers;

import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.sap.isa.ui.controllers.ActionForm;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.ServletException;

import com.sapportals.htmlb.event.Event;

import java.io.IOException;

/**
 * Title:
 * Description:
 * Copyright:    Copyright (c) 2002
 * Company:
 * @author
 * @version 1.0
 */

public interface EventHandler {
    public ActionForward doService(ActionMapping mapping,
                                              ActionForm form,
                                              Event event,
                                              HttpServletRequest request,
                                              HttpServletResponse response)
                                              throws ServletException , IOException;
}