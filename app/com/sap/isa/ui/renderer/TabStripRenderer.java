package com.sap.isa.ui.renderer;

import com.sapportals.htmlb.rendering.DefaultTabStripRenderer;

import com.sapportals.htmlb.Component;
import com.sapportals.htmlb.TabStrip;
import com.sapportals.htmlb.TabStripItem;

import com.sapportals.htmlb.enum.CellHAlign;
import com.sapportals.htmlb.enum.CellVAlign;
import com.sapportals.htmlb.event.TabSelectEvent;

import com.sapportals.htmlb.rendering.IPageContext;

import com.sapportals.htmlb.rendering.RenderUtil;
import com.sapportals.htmlb.rendering.Section508Utils;

import java.util.Iterator;

/**
 * Title:
 * Description:
 * Copyright:    Copyright (c) 2002
 * Company:
 * @author
 * @version 1.0
 */

public class TabStripRenderer extends DefaultTabStripRenderer  {

  private static int idcounter;

  public TabStripRenderer() {
  }

    /**
     * Render the complete component
     * @param   component   the component to render
     * @param   renderContext   the context in which to render
     */
    public void render(Component component, IPageContext renderContext) {
        if (component instanceof TabStrip) {
            TabStrip myTabStrip = (TabStrip) component;
            TabStripItem myItem;
            int max = myTabStrip.getItemCount();

            if (myTabStrip.getIndexPosition(myTabStrip.getSelection()) < 0) {
                myTabStrip.setSelection(myTabStrip.getItems().getFirstIndex());
            }

            renderHeadBegin(myTabStrip, renderContext);
            Iterator items = myTabStrip.getItems().values();
            int i = 0;
            while (items.hasNext()) {
//            for (int i = 1; i <= max; i++) {
                myItem = (TabStripItem) items.next(); // myTabStrip.getItem(i);
                myTabStrip.setCurrentIndex(myItem.getIndex()); // Nasty, nasty,...
                if (myItem != null) {
                    renderHeadItemBegin(myTabStrip, renderContext);
                    Component header = myItem.getHeader();
                    if (header != null) {
                        header.render(renderContext);
                    } else {
                        String title = myItem.getTitle();
                        if (title != null) {
                            renderContext.writeEncoded(title);
                        }
                    }
                    renderHeadItemEnd(myTabStrip, renderContext);
                    renderHeadItemSeparator(myTabStrip, renderContext);
                }
//            }
            }
            renderHeadEnd(myTabStrip, renderContext);
            renderContentBegin(myTabStrip, renderContext);

            items = myTabStrip.getItems().values();
            i = 0;
            while (items.hasNext()) {

                //for (int i = 1; i <= max; i++) {
                myItem = (TabStripItem) items.next();//myTabStrip.getItem(i);
                myTabStrip.setCurrentIndex(myItem.getIndex()); // Nasty, nasty,...
                if (myItem != null) {
                    renderContentItemBegin(myTabStrip, renderContext);
                    RenderUtil.renderContainerContent(myItem, renderContext);
//                    Component content = myItem.getBody();
//                    if (content != null) {
//                        content.render(renderContext);
//                    }
                    renderContentItemEnd(myTabStrip, renderContext);
                }
            }
            renderContentEnd(myTabStrip, renderContext);
        }
    }


    /**
     * Render a specific aspect of the complete component
     *
     * @param   component   the component to render
     * @param   selector    the aspect to render
     * @param   renderContext   the context in which to render
     */
    public void render(Component component, int selector, IPageContext renderContext) {
        if (component instanceof TabStrip) {
            TabStrip myTabStrip = (TabStrip) component;
            switch (selector) {
                case TabStrip.VAR_HEADBEGIN:
                    renderHeadBegin(myTabStrip, renderContext);
                    break;
                case TabStrip.VAR_HEADITEM_BEGIN:
                    renderHeadItemBegin(myTabStrip, renderContext);
                    break;
                case TabStrip.VAR_HEADITEM_END:
                    renderHeadItemEnd(myTabStrip, renderContext);
                    break;
                case TabStrip.VAR_HEADITEM_SEP:
                    renderHeadItemSeparator(myTabStrip, renderContext);
                    break;
                case TabStrip.VAR_HEADEND:
                    renderHeadEnd(myTabStrip, renderContext);
                    break;
                case TabStrip.VAR_CONTENT_BEGIN:
                    renderContentBegin(myTabStrip, renderContext);
                    break;
                case TabStrip.VAR_ITEM_BEGIN:
                    renderContentItemBegin(myTabStrip, renderContext);
                    break;
                case TabStrip.VAR_ITEM_END:
                    renderContentItemEnd(myTabStrip, renderContext);
                    break;
                case TabStrip.VAR_CONTENT_END:
                    renderContentEnd(myTabStrip, renderContext);
                    break;
            }
        }
    }


    public void renderHeadBegin(TabStrip tabStrip, IPageContext rc) {
        String name = tabStrip.getId();
        String fullId = rc.getParamIdForComponent(tabStrip);

        rc.write("<table border=\"0\" cellspacing=\"5\" cellpadding=\"0\" class=\"sapGrcWhl");
        String tooltip = tabStrip.getTooltip();
        if (tooltip != null) {
            rc.write("\" title=\"");
            rc.writeEncoded(tooltip);
        }
        String width = tabStrip.getWidth();
        if (width != null) {
            rc.write("\" style=\"width:");
            rc.write(width);
            rc.write(";");
        }
        String height = tabStrip.getBodyHeight();
        if (height != null) {
            rc.write("height:");
            rc.write(height);
            rc.write(";");
        }

        rc.write("\"><tr><td>");

        rc.write("<input name=\"");
        rc.write(fullId);
        rc.write("_num\" id=\"");
        rc.write(fullId);
        rc.write("_num\" type=\"hidden\" value=\"");
        rc.write(tabStrip.getItemCount());
        rc.write("\">");

        rc.write("<input name=\"");
        rc.write(fullId);
        rc.write("_sel\" id=\"");
        rc.write(fullId);
        rc.write("_sel\" type=\"hidden\" value=\"");
        rc.write(tabStrip.getIndexPosition(tabStrip.getSelection()));
        rc.write("\">");

        rc.write("<input name=\"");
        rc.write(fullId);
        rc.write("_idx\" id=\"");
        rc.write(fullId);
        rc.write("_idx\" type=\"hidden\" value=\"");
        rc.write(tabStrip.getSelection());
        rc.write("\">");

        // Start table for header items
        rc.write("<table style=\"");

//        String width = tabStrip.getWidth();
        if ((width != null) && (!width.equals(""))) {
            rc.write("width:");
            rc.write(width);
            rc.write(";");
        }
        if (height != null) {
            rc.write("height:");
            rc.write(height);
            rc.write(";");
        }
        rc.write("\"><tr valign=\"top\"><td><table cellspacing=\"0\" cellpadding=\"0\" border=\"0\"><tr>");
    }


    public void renderHeadItemBegin(TabStrip tabStrip, IPageContext rc) {
        int selection = tabStrip.getSelection();

        int index = tabStrip.getCurrentIndex();
        int indexPos = tabStrip.getIndexPosition(index);
        //String name = tabStrip.getId();
        String name = rc.getParamIdForComponent(tabStrip);
        rc.write("<td id=\"");
        rc.write(name);
        rc.write("tab");
        rc.write(indexPos);
        rc.write("\" class=\"");
        rc.write((index == selection)?"sapTbsTabSel":"sapTbsTab");
        rc.write("\"><a href=\"#\" onkeypress=\"htmlbInvokeClick()\" onclick=\"");
        TabStripItem myItem = tabStrip.getItem(index);
        String onSelect = myItem.getOnSelect();
        String tooltip508 = "";
        if (onSelect == null) {
            // Use client side switching.
            // The java script will cause a server side event for NN4!
            rc.write("sapTbs_switch(");
            rc.write(indexPos);
            rc.write(",");
            rc.write(index);
            rc.write(",'");
            rc.write(name);
            rc.write("'); return false;");
            //System.out.println("selection="+selection );
        } else {
            rc.write(TabSelectEvent.renderEventCall(rc, tabStrip, myItem));
        }
        tooltip508 = Section508Utils.getTabStripItemTooltip(myItem, rc, selection);
        myItem = tabStrip.getItem(index);

        if (myItem != null) {
            String tooltip = myItem.getTooltip();
            if ((tooltip508 != null) || (tooltip != null)) {
                rc.write("\" title=\"");
                if (tooltip508 != null) {
                    rc.writeEncoded(tooltip508);
                    rc.write(" ");
                }
                if (tooltip != null) {
                    rc.writeEncoded(tooltip);
                }
            }
        }

        rc.write("\" style=\"text-decoration:none\"><span class=\"sapTxtStd");
        // tooltip?
        rc.write("\"><nobr>");
    }


    public void renderHeadItemEnd(TabStrip tabStrip, IPageContext rc) {
        rc.write("</nobr></span></a></td>");
    }


    public void renderHeadItemSeparator(TabStrip tabStrip, IPageContext rc) {

        int index = tabStrip.getCurrentIndex();
        int selection = tabStrip.getSelection();
//        String name = tabStrip.getId();
        String name = rc.getParamIdForComponent(tabStrip);

        rc.write("<td id=\"");
        rc.write(name);
        rc.write("tbsImg");
        rc.write(tabStrip.getIndexPosition(index));
        rc.write("\" class=\"sapTbsSpc");
        if (index == 1) {
            if (selection == index) {
                rc.write("FrOn");
            } else {
                rc.write("FrOff");
            }
        } else if (index == selection) {
            rc.write("OffOn");
        } else if (index == (selection + 1)) {
            rc.write("OnOff");
        } else {
            rc.write("OffOff");
        }
        rc.write("\"></td>");
    }


    public void renderHeadEnd(TabStrip tabStrip, IPageContext rc) {
        String name = rc.getParamIdForComponent(tabStrip);

        rc.write("<td nowrap id=\"");
        rc.write(name);
        rc.write("tbsImg");
        rc.write(tabStrip.getItemCount() + 1);
        rc.write("\" class=\"sapTbsSpcBkOff\"></td><td nowrap class=\"sapTbsTabBlk\">&nbsp;</td></tr></table>");
    }


    public void renderContentBegin(TabStrip tabStrip, IPageContext rc) {
        rc.write("<table class=\"sapTbsWhl\" width=\"100%\"><tr><td align=\"");
        CellHAlign hAlign = tabStrip.getHAlign();
        if (hAlign == CellHAlign.CENTER) {
            rc.write("center");
        } else if (hAlign == CellHAlign.LEFT) {
            rc.write("left");
        } else if (hAlign == CellHAlign.RIGHT) {
            rc.write("right");
        } else if (hAlign == CellHAlign.JUSTIFY) {
            rc.write("justify");
        } else if (hAlign == CellHAlign.CHAR) {
            rc.write("char");
        }
        rc.write("\" valign=\"");
        CellVAlign vAlign = tabStrip.getVAlign();
        if (vAlign == CellVAlign.BASELINE) {
            rc.write("baseline");
        } else if (vAlign == CellVAlign.BOTTOM) {
            rc.write("bottom");
        } else if (vAlign == CellVAlign.TOP) {
            rc.write("top");
        } else if (vAlign == CellVAlign.MIDDLE) {
            rc.write("middle");
        }
        rc.write("\">");
    }


    public void renderContentItemBegin(TabStrip tabStrip, IPageContext rc) {
        int selection = tabStrip.getSelection();
        int index = tabStrip.getCurrentIndex();
        String name = rc.getParamIdForComponent(tabStrip);

        rc.write("<div id=\"");
        rc.write(name);
        rc.write("content");
        rc.write(tabStrip.getIndexPosition(index));
        if (selection == index) {
          rc.write("\" class=\"sapTbsDspSel\"");
        } else {
          rc.write("\" class=\"sapTbsDsp\"");
        }
        rc.write(" style=\"");
        String width = tabStrip.getWidth();
        if (width != null) {
            rc.write("width:");
            rc.write(width);
            rc.write(";");
        }
        String height = tabStrip.getBodyHeight();
        if (height != null) {
            rc.write("height:");
            rc.write(height);
            rc.write(";");
        }
/*
        if (selection == index) {
            rc.write("display:block;");
        }
*/
        rc.write("\">");
    }


    public void renderContentItemEnd(TabStrip tabStrip, IPageContext rc) {
        rc.write("</div>");
    }


    public void renderContentEnd(TabStrip tabStrip, IPageContext rc) {
        rc.write("</td></tr></table></td></tr></table>");
        rc.write("</td></tr></table>");
    }
}