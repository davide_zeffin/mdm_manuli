package com.sap.isa.ui.renderer;

import java.io.PrintWriter;

import com.sapportals.htmlb.rendering.IPageContext;
import com.sapportals.htmlb.unifiedrendering.IUIElement;
import com.sapportals.htmlb.unifiedrendering.TagRenderer;
import com.sapportals.htmlb.unifiedrendering.enum.ScrollingMode;
import com.sapportals.htmlb.unifiedrendering.ie5.ScrollContainerRenderer;

/**
 */
public class ExScrollContainerRenderer extends ScrollContainerRenderer {

	/* (non-Javadoc)
	 * @see com.sapportals.htmlb.unifiedrendering.ie5.ScrollContainerRenderer#renderScrollContainerFragment(com.sapportals.htmlb.unifiedrendering.IUIElement, com.sapportals.htmlb.rendering.IPageContext, java.lang.String, java.lang.String, java.lang.String, com.sapportals.htmlb.unifiedrendering.enum.ScrollingMode, com.sapportals.htmlb.unifiedrendering.IUIElement)
	 */
	public void renderScrollContainerFragment(
		IUIElement pControl,
		IPageContext manager,
		String pId,
		String pWidth,
		String pHeight,
		ScrollingMode pScrollingMode,
		IUIElement pContent) {
		PrintWriter out = (PrintWriter) manager.getWriter();

		StringBuffer _style_collector = new StringBuffer();

		boolean encode = false;

		if (pScrollingMode != ScrollingMode.NONE) {
			TagRenderer.writeBeginTag(out, "div");

			_style_collector = new StringBuffer();

			{ //begin attribute

				String attribute = pId;

				if (!"".equals(attribute))
					TagRenderer.writeAttribute(out, "id", attribute, encode);
			} //end attribute

			{

				String attribute = String.valueOf("100%");

				if (pWidth != null
					&& !pWidth.equals("")
					&& !pWidth.equals("null")) {
					attribute = pWidth;
				}

				_style_collector.append("width:").append(attribute).append(";");
			}

			{

				String attribute = pHeight;

				if (!"".equals(attribute))
					_style_collector.append("height:").append(
						attribute).append(
						";");
			}

			{

				String attribute = "relative";

				_style_collector.append("position:").append(attribute).append(
					";");
			}

			if (pScrollingMode == ScrollingMode.AUTO) {
				{

					String attribute = "auto";

					_style_collector.append("overflow:").append(
						attribute).append(
						";");
				}
			}

			if (pScrollingMode == ScrollingMode.BOTH) {
				{

					String attribute = "scroll";

					_style_collector.append("overflow:").append(
						attribute).append(
						";");
				}
			}

			if (pScrollingMode == ScrollingMode.HIDE) {
				{

					String attribute = "hidden";

					_style_collector.append("overflow:").append(
						attribute).append(
						";");
				}
			}

			if (_style_collector.length() != 0)
				TagRenderer.writeAttribute(
					out,
					"style",
					_style_collector.toString(),
					encode);

			TagRenderer.writeTagRightChar(out);

			TagRenderer.writeBeginTag(out, "div");

			TagRenderer.writeTagRightChar(out);

			if (pContent != null) {
				com
					.sapportals
					.htmlb
					.rendering
					.RenderUtil
					.renderContainerContent(
					(com.sapportals.htmlb.Container) pContent.getHtmlBControl(),
					manager);
			}

			TagRenderer.writeEndTag(out, "div");

			TagRenderer.writeEndTag(out, "div");
		} else {
			TagRenderer.writeBeginTag(out, "div");

			_style_collector = new StringBuffer();

			{ //begin attribute

				String attribute = pId;

				if (!"".equals(attribute))
					TagRenderer.writeAttribute(out, "id", attribute, encode);
			} //end attribute

			{

				String attribute = String.valueOf("100%");

				if (pWidth != null
					&& !pWidth.equals("")
					&& !pWidth.equals("null")) {
					attribute = pWidth;
				}

				_style_collector.append("width:").append(attribute).append(";");
			}

			{

				String attribute = pHeight;

				if (!"".equals(attribute))
					_style_collector.append("height:").append(
						attribute).append(
						";");
			}

			if (_style_collector.length() != 0)
				TagRenderer.writeAttribute(
					out,
					"style",
					_style_collector.toString(),
					encode);

			TagRenderer.writeTagRightChar(out);

			if (pContent != null) {
				com
					.sapportals
					.htmlb
					.rendering
					.RenderUtil
					.renderContainerContent(
					(com.sapportals.htmlb.Container) pContent.getHtmlBControl(),
					manager);
			}

			TagRenderer.writeEndTag(out, "div");
		}

	}

}
