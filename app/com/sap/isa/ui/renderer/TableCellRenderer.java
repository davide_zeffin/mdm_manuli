package com.sap.isa.ui.renderer;

import com.sap.isa.ui.components.DataComponent;
import com.sapportals.htmlb.enum.TableColumnType;
import com.sapportals.htmlb.rendering.IPageContext;
import com.sapportals.htmlb.table.ICellRenderer;
import com.sapportals.htmlb.table.TableColumn;
import com.sapportals.htmlb.table.TableView;
import com.sapportals.htmlb.table.TableViewModel;
import com.sapportals.htmlb.type.AbstractDataType;


/**
 * Title:
 * Description:
 * Copyright:    Copyright (c) 2002
 * Company:
 * @author
 * @version 1.0
 */

public class TableCellRenderer implements ICellRenderer  {

  private TableViewModel tableViewModel;

  public TableCellRenderer(TableViewModel model) {
      tableViewModel = model;
  }

  public void renderCell(int row, int col, TableView tableView, IPageContext pageContext) {

      TableColumn column = tableViewModel.getColumnAt(col);
      if(column.getType().equals(TableColumnType.USER))  {
          AbstractDataType data = column.getValueAt(row);
          if (data instanceof com.sapportals.htmlb.type.DataString) {
              // Oops, what now?? ((com.sapportals.htmlb.type.DataString)data).getValue().
          }
          else {
              ((DataComponent)data).getValue().render(pageContext);
          }
          return;
      }
      //super.renderCell(row , col , tableView , pageContext);
  }
}
