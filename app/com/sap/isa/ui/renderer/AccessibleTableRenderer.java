package com.sap.isa.ui.renderer;

import com.sapportals.htmlb.rendering.IPageContext;

import com.sapportals.htmlb.unifiedrendering.*;
import com.sapportals.htmlb.unifiedrendering.enum.*;
import com.sapportals.htmlb.unifiedrendering.controlinterface.*;
import com.sapportals.htmlb.unifiedrendering.controls.TableViewCell;

import java.util.*;
import java.io.PrintWriter;

/**
 * Title:			Internet Sales Selling Via eBay
 * Description:     Extends the HTMLB TableViewRenderer by adding additional 
 *                  accessibility support for tables.
 * @Copyright: 		Copyright (c) 2004
 * Company:			SAPLabs LLC
 * Created on:		Nov 29, 2004
 */
public class AccessibleTableRenderer extends com.sapportals.htmlb.unifiedrendering.ie5.TableViewRenderer {

    public void renderTableViewHeaderCellFragment(
        IUIElement pControl,
        IPageContext manager,
        String pTableId,
        TableViewDesign pDesign,
        TableViewSelectionMode pSelectionMode,
        ITableViewHeaderCell pHeaderCell,
        int pCellIndex,
        int pLength,
        int pColCount,
        String pId,
        boolean pHasWidth) {
        PrintWriter out = (PrintWriter) manager.getWriter();

        {
            ((com.sapportals.htmlb.table.TableView) pControl.getHtmlBControl()).setCurrentColumnIndex(pCellIndex + 1);
        }

        StringBuffer _style_collector = new StringBuffer();

        boolean encode = false;

        if (pHeaderCell.getSortState() == SortState.DISABLED) {
            TagRenderer.writeBeginTag(out, "th");

            _style_collector = new StringBuffer();

            { //begin attribute

                String attribute = pHeaderCell.getId();

                if (!("").equals(attribute))
                    TagRenderer.writeAttribute(out, "id", attribute, encode);
            } //end attribute

            if (((ISystem) manager).getIs508()) {
                { //begin attribute

                    String attribute = String.valueOf(0);

                    TagRenderer.writeAttribute(out, "tabindex", attribute, encode);
                } //end attribute

                { //begin attribute

                    String attribute = String.valueOf(0);

                    TagRenderer.writeAttribute(out, "ti", attribute, encode);
                } //end attribute

                {
                    TagRenderer.writeAttribute(out, "scope", "col", encode);
                }
            }

            if (pCellIndex == pColCount - 1) {
                if (pHasWidth) {
                    { //begin attribute

                        String attribute = "*";

                        TagRenderer.writeAttribute(out, "width", attribute, encode);
                    } //end attribute
                } else {
                    { //begin attribute

                        String attribute = pHeaderCell.getWidth();

                        TagRenderer.writeAttribute(out, "width", attribute, encode);
                    } //end attribute
                }
            } else {
                {

                    String attribute = pHeaderCell.getWidth();

                    if (!("").equals(attribute))
                        _style_collector.append("width:").append(attribute).append(";");
                }
            }

            if (pHeaderCell.getWrapping() == false) {
                { //begin attribute

                    String attribute = "";
                    TagRenderer.writeAttribute(out, "nowrap", attribute, encode);
                } //end attribute
            }

            {

                String eventHandler = pControl.getJSEventFunction(ClientEvent.TABLEHEADERCELLCLICK, manager);

                if (eventHandler != null && eventHandler.length() > 0) {
                    manager.write(" onclick=\"");
                    manager.write(eventHandler);
                    manager.write("\" ");
                }

            }

            { //begin attribute

                String attribute = pHeaderCell.getTooltip();

                if (!("").equals(attribute))
                    TagRenderer.writeAttribute(out, "title", attribute, encode);
            } //end attribute

            { //begin attribute

                StringBuffer attribute = new StringBuffer();
                attribute.append("urSTbvColHdrLvl1");

                if (pDesign == TableViewDesign.TRANSPARENT) {
                    attribute.append("Trans");
                }

                /*
                if cellindex == 0 we are the first cell
                */

                if (pCellIndex == 0) {
                    if (pSelectionMode == TableViewSelectionMode.NONE) {
                        attribute.append("First");
                    } else {
                        attribute.append("Std");
                    }
                } else {
                    /*
                    if cellindex == length - 1 then it's the last cell
                    */

                    if (pCellIndex == pLength - 1) {
                        attribute.append("Last");
                    } else {
                        attribute.append("Std");
                    }
                }

                TagRenderer.writeAttribute(out, "class", attribute, encode);
            } //end attribute

            if (pHeaderCell.getIsClickable()) {
                {

                    String attribute = "hand";

                    _style_collector.append("cursor:").append(attribute).append(";");
                }
            }
            if (pHeaderCell.getHAlign() == CellHAlign.ENDOFLINE) {
                if (((ISystem) manager).getIsRTL()) {
                    {

                        String attribute = "left";

                        _style_collector.append("text-align:").append(attribute).append(";");
                    }
                } else {
                    {

                        String attribute = "right";

                        _style_collector.append("text-align:").append(attribute).append(";");
                    }
                }

            } else if (pHeaderCell.getHAlign() == CellHAlign.RIGHT) {
                if (((ISystem) manager).getIsRTL() == false) {
                    {

                        String attribute = "right";

                        _style_collector.append("text-align:").append(attribute).append(";");
                    }
                }

            } else if (pHeaderCell.getHAlign() == CellHAlign.CENTER) {
                {

                    String attribute = "center";

                    _style_collector.append("text-align:").append(attribute).append(";");
                }

            } else if (pHeaderCell.getHAlign() == CellHAlign.JUSTIFY) {
                {

                    String attribute = "justify";

                    _style_collector.append("text-align:").append(attribute).append(";");
                }

            } else if (pHeaderCell.getHAlign() == CellHAlign.FORCEDLEFT) {
                if (((ISystem) manager).getIsRTL()) {
                    {

                        String attribute = "left";

                        _style_collector.append("text-align:").append(attribute).append(";");
                    }
                }

            }

            if (_style_collector.length() != 0)
                TagRenderer.writeAttribute(out, "style", _style_collector.toString(), encode);

            TagRenderer.writeTagRightChar(out);

            if (pHeaderCell.getIsTextHeader()) {
                out.write(String.valueOf(pHeaderCell.getCaption()));
            } else {
                if (pHeaderCell.getHasHeaderCaption()) {
                    TagRenderer.writeBeginTag(out, "span");

                    _style_collector = new StringBuffer();

                    {

                        String attribute = "nowrap";

                        _style_collector.append("white-space:").append(attribute).append(";");
                    }

                    if (_style_collector.length() != 0)
                        TagRenderer.writeAttribute(out, "style", _style_collector.toString(), encode);

                    TagRenderer.writeTagRightChar(out);

                    TagRenderer.writeEndTag(out, "span");
                } else {
                    TagRenderer.writeBeginTag(out, "span");

                    TagRenderer.writeAttribute(out, "class", "urFontStd", encode);

                    TagRenderer.writeTagRightChar(out);

                    {
                        com.sapportals.htmlb.table.TableView tableView =
                            (com.sapportals.htmlb.table.TableView) pControl.getHtmlBControl();
                        tableView.getHeaderCellRenderer(pCellIndex + 1).renderHeaderCell(
                            pCellIndex + 1,
                            tableView,
                            manager);
                    }

                    TagRenderer.writeEndTag(out, "span");
                }
            }

            TagRenderer.writeEndTag(out, "th");
        } else {
            TagRenderer.writeBeginTag(out, "th");

            _style_collector = new StringBuffer();

            { //begin attribute

                String attribute = pHeaderCell.getId();

                if (!("").equals(attribute))
                    TagRenderer.writeAttribute(out, "id", attribute, encode);
            } //end attribute

            {

                String eventHandler = pControl.getJSEventFunction(ClientEvent.TABLEHEADERCELLCLICK, manager);

                if (eventHandler != null && eventHandler.length() > 0) {
                    manager.write(" onclick=\"");
                    manager.write(eventHandler);
                    manager.write("\" ");
                }

            }

            if (((ISystem) manager).getIs508()) {
                { //begin attribute

                    String attribute = String.valueOf(0);

                    TagRenderer.writeAttribute(out, "tabindex", attribute, encode);
                } //end attribute

                { //begin attribute

                    String attribute = String.valueOf(0);

                    TagRenderer.writeAttribute(out, "ti", attribute, encode);
                } //end attribute
            }

            if (pCellIndex == pColCount - 1) {
                { //begin attribute

                    String attribute = "*";

                    TagRenderer.writeAttribute(out, "width", attribute, encode);
                } //end attribute
            } else {
                {

                    String attribute = pHeaderCell.getWidth();

                    if (!("").equals(attribute))
                        _style_collector.append("width:").append(attribute).append(";");
                }
            }

            if (pHeaderCell.getWrapping() == false) {
                { //begin attribute

                    String attribute = "";
                    TagRenderer.writeAttribute(out, "nowrap", attribute, encode);
                } //end attribute
            }

            { //begin attribute

                String attribute = pHeaderCell.getTooltip();

                if (!("").equals(attribute))
                    TagRenderer.writeAttribute(out, "title", attribute, encode);
            } //end attribute

            { //begin attribute

                StringBuffer attribute = new StringBuffer();
                attribute.append("urSTbvColHdrLvl1");

                if (pDesign == TableViewDesign.TRANSPARENT) {
                    attribute.append("Trans");
                }

                /*
                if cellindex == 0 we are the first cell
                */

                if (pCellIndex == 0) {
                    if (pSelectionMode == TableViewSelectionMode.NONE) {
                        attribute.append("First");
                    } else {
                        attribute.append("Std");
                    }
                } else {
                    /*
                    if cellindex == length - 1 then it's the last cell
                    */

                    if (pCellIndex == pLength - 1) {
                        attribute.append("Last");
                    } else {
                        attribute.append("Std");
                    }
                }

                TagRenderer.writeAttribute(out, "class", attribute, encode);
            } //end attribute

            if (pHeaderCell.getIsClickable()) {
                {

                    String attribute = "hand";

                    _style_collector.append("cursor:").append(attribute).append(";");
                }
            }

            if (_style_collector.length() != 0)
                TagRenderer.writeAttribute(out, "style", _style_collector.toString(), encode);

            TagRenderer.writeTagRightChar(out);

            TagRenderer.writeBeginTag(out, "table");

            TagRenderer.writeAttribute(out, "cellpadding", "0", encode);

            TagRenderer.writeAttribute(out, "cellspacing", "0", encode);

            TagRenderer.writeAttribute(out, "width", "100%", encode);

            TagRenderer.writeTagRightChar(out);

            TagRenderer.writeBeginTag(out, "tr");

            TagRenderer.writeTagRightChar(out);

            TagRenderer.writeBeginTag(out, "td");

            TagRenderer.writeAttribute(out, "class", "urSTbvColHdrTtl", encode);

            TagRenderer.writeAttribute(out, "width", "100%", encode);

            _style_collector = new StringBuffer();

            { //begin attribute

                String attribute = "";
                TagRenderer.writeAttribute(out, "nowrap", attribute, encode);
            } //end attribute
            if (pHeaderCell.getHAlign() == CellHAlign.ENDOFLINE) {
                if (((ISystem) manager).getIsRTL()) {
                    {

                        String attribute = "left";

                        _style_collector.append("align:").append(attribute).append(";");
                    }
                } else {
                    {

                        String attribute = "right";

                        _style_collector.append("align:").append(attribute).append(";");
                    }
                }

            } else if (pHeaderCell.getHAlign() == CellHAlign.RIGHT) {
                if (((ISystem) manager).getIsRTL() == false) {
                    {

                        String attribute = "right";

                        _style_collector.append("align:").append(attribute).append(";");
                    }
                }

            } else if (pHeaderCell.getHAlign() == CellHAlign.CENTER) {
                {

                    String attribute = "center";

                    _style_collector.append("align:").append(attribute).append(";");
                }

            } else if (pHeaderCell.getHAlign() == CellHAlign.JUSTIFY) {
                {

                    String attribute = "justify";

                    _style_collector.append("align:").append(attribute).append(";");
                }

            } else if (pHeaderCell.getHAlign() == CellHAlign.FORCEDLEFT) {
                if (((ISystem) manager).getIsRTL()) {
                    {

                        String attribute = "left";

                        _style_collector.append("align:").append(attribute).append(";");
                    }
                }

            }

            if (_style_collector.length() != 0)
                TagRenderer.writeAttribute(out, "style", _style_collector.toString(), encode);

            TagRenderer.writeTagRightChar(out);

            if (pHeaderCell.getIsTextHeader()) {
                out.write(String.valueOf(pHeaderCell.getCaption()));
            } else {
                if (pHeaderCell.getHasHeaderCaption()) {
                    TagRenderer.writeBeginTag(out, "nobr");

                    TagRenderer.writeTagRightChar(out);

                    TagRenderer.writeEndTag(out, "nobr");
                } else {
                    TagRenderer.writeBeginTag(out, "span");

                    TagRenderer.writeAttribute(out, "class", "urFontStd", encode);

                    TagRenderer.writeTagRightChar(out);

                    {
                        com.sapportals.htmlb.table.TableView tableView =
                            (com.sapportals.htmlb.table.TableView) pControl.getHtmlBControl();
                        tableView.getHeaderCellRenderer(pCellIndex + 1).renderHeaderCell(
                            pCellIndex + 1,
                            tableView,
                            manager);
                    }

                    TagRenderer.writeEndTag(out, "span");
                }
            }

            TagRenderer.writeEndTag(out, "td");

            TagRenderer.writeBeginTag(out, "td");

            TagRenderer.writeAttribute(out, "class", "urSTbvSortIconCell", encode);

            TagRenderer.writeTagRightChar(out);

            TagRenderer.writeBeginTag(out, "button");

            TagRenderer.writeAttribute(out, "type", "button", encode);

            { //begin attribute

                StringBuffer attribute = new StringBuffer();
                if (pHeaderCell.getSortState() == SortState.ASCENDING) {
                    attribute.append("urSTbvIconSortAsc");

                } else if (pHeaderCell.getSortState() == SortState.DESCENDING) {
                    attribute.append("urSTbvIconSortDesc");

                } else {

                    attribute.append("urSTbvIconUnsorted");

                }

                TagRenderer.writeAttribute(out, "class", attribute, encode);
            } //end attribute

            {

                String eventHandler = pControl.getJSEventFunction(ClientEvent.TABLEHEADERCELLSORT, manager);

                if (eventHandler != null && eventHandler.length() > 0) {
                    manager.write(" onclick=\"");
                    manager.write(eventHandler);
                    manager.write("\" ");
                }

            }

            if (((ISystem) manager).getIs508()) {
                if (pHeaderCell.getSortState() == SortState.ASCENDING) {
                    { //begin attribute

                        StringBuffer attribute = new StringBuffer();

                        {
                            List paramList = new ArrayList();

                            paramList.add("#SAPUR_TBV_HEADER_SORT");

                            paramList.add(pHeaderCell.getCaption());

                            paramList.add("#SAPUR_TBV_HEADER_ASC");

                            paramList.add("#SAPUR_TBV_HEADER_CHANGE");

                            paramList.add(pHeaderCell.getTooltip());

                            attribute.append(TagRenderer.translate("#SAPUR_TBV_HEADER_SORT_WHL", paramList, manager));

                        }

                        TagRenderer.writeAttribute(out, "title", attribute, encode);
                    } //end attribute

                } else if (pHeaderCell.getSortState() == SortState.DESCENDING) {
                    { //begin attribute

                        StringBuffer attribute = new StringBuffer();

                        {
                            List paramList = new ArrayList();

                            paramList.add("#SAPUR_TBV_HEADER_SORT");

                            paramList.add(pHeaderCell.getCaption());

                            paramList.add("#SAPUR_TBV_HEADER_DESC");

                            paramList.add("#SAPUR_TBV_HEADER_CHANGE");

                            paramList.add(pHeaderCell.getTooltip());

                            attribute.append(TagRenderer.translate("#SAPUR_TBV_HEADER_SORT_WHL", paramList, manager));

                        }

                        TagRenderer.writeAttribute(out, "title", attribute, encode);
                    } //end attribute

                } else {

                    { //begin attribute

                        StringBuffer attribute = new StringBuffer();

                        {
                            List paramList = new ArrayList();

                            paramList.add("#SAPUR_TBV_HEADER_SORT");

                            paramList.add(pHeaderCell.getCaption());

                            paramList.add("#SAPUR_TBV_HEADER_UNSORTED");

                            paramList.add("#SAPUR_TBV_HEADER_CHANGE");

                            paramList.add(pHeaderCell.getTooltip());

                            attribute.append(TagRenderer.translate("#SAPUR_TBV_HEADER_SORT_WHL", paramList, manager));

                        }

                        TagRenderer.writeAttribute(out, "title", attribute, encode);
                    } //end attribute

                }
            }

            TagRenderer.writeTagRightChar(out);

            TagRenderer.writeEndTag(out, "button");

            TagRenderer.writeEndTag(out, "td");

            TagRenderer.writeEndTag(out, "tr");

            TagRenderer.writeEndTag(out, "table");

            TagRenderer.writeEndTag(out, "th");
        }

    }

    public void renderTableViewCellFragment(
        IUIElement pControl,
        IPageContext manager,
        String pTableId,
        ITableViewCell pCell,
        boolean pOddRow,
        TableViewSelectionMode pSelectionMode,
        TableViewNavigationMode pNavigationMode,
        TableViewDesign pDesign,
        int pIndex,
        int pLength,
        int pRowIndex,
        boolean pIsSelected,
        int pColCount) {
        PrintWriter out = (PrintWriter) manager.getWriter();

        {
            ((com.sapportals.htmlb.table.TableView) pControl.getHtmlBControl()).setCurrentColumnIndex(pIndex + 1);
        }

        StringBuffer _style_collector = new StringBuffer();

        boolean encode = false;

        /* ACC SUPPORT
         * Use a TH instead of a TR for data table headers and association. Only
         * apply to the first column.
         */
        if (((ISystem) manager).getIs508() && pIndex == 0) {
            TagRenderer.writeBeginTag(out, "th");
            if (pCell.getHAlign() == CellHAlign.LEFT) {
                TagRenderer.writeAttribute(out, "align", "left", encode);
            }
        } else {
            TagRenderer.writeBeginTag(out, "td");
        }

        _style_collector = new StringBuffer();

        { //begin attribute

            String attribute = pCell.getHeaderCellIds();

            TagRenderer.writeAttribute(out, "headers", attribute, encode);
        } //end attribute

        if (pIndex == pColCount - 1) {
            { //begin attribute

                String attribute = "*";

                TagRenderer.writeAttribute(out, "width", attribute, encode);
            } //end attribute
        } else {
            {

                String attribute = pCell.getWidth();

                if (!("").equals(attribute))
                    _style_collector.append("width:").append(attribute).append(";");
            }
        }

        if (pCell.getIsClickable()) {
            {

                String attribute = "hand";

                _style_collector.append("cursor:").append(attribute).append(";");
            }

            {

                String attribute = "pointer";

                _style_collector.append("cursor:").append(attribute).append(";");
            }
        }

        {

            String eventHandler = pControl.getJSEventFunction(ClientEvent.TABLECELLCLICK, manager);

            if (eventHandler != null && eventHandler.length() > 0) {
                manager.write(" onclick=\"");
                manager.write(eventHandler);
                manager.write("\" ");
            }

        }

        { //begin attribute

            String attribute = pCell.getTooltip();

            if (!("").equals(attribute))
                TagRenderer.writeAttribute(out, "title", attribute, encode);
        } //end attribute
        if (pCell.getHAlign() == CellHAlign.ENDOFLINE) {
            if (((ISystem) manager).getIsRTL()) {
                {

                    String attribute = "left";

                    _style_collector.append("align:").append(attribute).append(";");
                }
            } else {
                {

                    String attribute = "right";

                    _style_collector.append("align:").append(attribute).append(";");
                }
            }

        } else if (pCell.getHAlign() == CellHAlign.RIGHT) {
            if (((ISystem) manager).getIsRTL() == false) {
                {

                    String attribute = "right";

                    _style_collector.append("align:").append(attribute).append(";");
                }
            }

        } else if (pCell.getHAlign() == CellHAlign.CENTER) {
            {

                String attribute = "center";

                _style_collector.append("align:").append(attribute).append(";");
            }

        } else if (pCell.getHAlign() == CellHAlign.JUSTIFY) {
            {

                String attribute = "justify";

                _style_collector.append("align:").append(attribute).append(";");
            }

        } else if (pCell.getHAlign() == CellHAlign.FORCEDLEFT) {
            if (((ISystem) manager).getIsRTL()) {
                {

                    String attribute = "left";

                    _style_collector.append("align:").append(attribute).append(";");
                }
            }

        }

        { //begin attribute

            String attribute = String.valueOf(pCell.getVAlign());

            if (!String.valueOf(CellVAlign.MIDDLE).equals(attribute))
                TagRenderer.writeAttribute(out, "valign", attribute, encode);
        } //end attribute

        if (pCell.getColspan() != 1) {
            { //begin attribute

                String attribute = String.valueOf(pCell.getColspan());

                if (!String.valueOf(1).equals(attribute))
                    TagRenderer.writeAttribute(out, "colspan", attribute, encode);
            } //end attribute
        }

        if (pCell.getRowspan() != 1) {
            { //begin attribute

                String attribute = String.valueOf(pCell.getRowspan());

                if (!String.valueOf(1).equals(attribute))
                    TagRenderer.writeAttribute(out, "rowspan", attribute, encode);
            } //end attribute
        }

        if (((ISystem) manager).getIs508()) {
            { //begin attribute

                String attribute = String.valueOf(0);

                TagRenderer.writeAttribute(out, "tabindex", attribute, encode);
            } //end attribute

            { //begin attribute

                String attribute = String.valueOf(0);

                TagRenderer.writeAttribute(out, "ti", attribute, encode);
            } //end attribute

            { //begin attribute

                StringBuffer attribute = new StringBuffer();
                if (pCell.getSemanticColor() == TableViewCellSemanticColor.NEGATIVE) {

                    {
                        List paramList = new ArrayList();

                        attribute.append(TagRenderer.translate("#SAPUR_NEGATIVE", paramList, manager));

                    }

                } else if (pCell.getSemanticColor() == TableViewCellSemanticColor.POSITIVE) {

                    {
                        List paramList = new ArrayList();

                        attribute.append(TagRenderer.translate("#SAPUR_POSITIVE", paramList, manager));

                    }

                } else if (pCell.getSemanticColor() == TableViewCellSemanticColor.TOTAL) {

                    {
                        List paramList = new ArrayList();

                        attribute.append(TagRenderer.translate("#SAPUR_TOTAL", paramList, manager));

                    }

                } else if (pCell.getSemanticColor() == TableViewCellSemanticColor.SUBTOTAL) {

                    {
                        List paramList = new ArrayList();

                        attribute.append(TagRenderer.translate("#SAPUR_SUBTOTAL", paramList, manager));

                    }

                } else if (pCell.getSemanticColor() == TableViewCellSemanticColor.SUBTOTAL_LIGHT) {

                    {
                        List paramList = new ArrayList();

                        attribute.append(TagRenderer.translate("#SAPUR_SUBTOTAL", paramList, manager));

                    }

                } else if (pCell.getSemanticColor() == TableViewCellSemanticColor.BADVALUE_DARK) {

                    {
                        List paramList = new ArrayList();

                        attribute.append(TagRenderer.translate("#SAPUR_BADVALUE", paramList, manager));

                    }

                } else if (pCell.getSemanticColor() == TableViewCellSemanticColor.BADVALUE_MEDIUM) {

                    {
                        List paramList = new ArrayList();

                        attribute.append(TagRenderer.translate("#SAPUR_BADVALUE", paramList, manager));

                    }

                } else if (pCell.getSemanticColor() == TableViewCellSemanticColor.BADVALUE_LIGHT) {

                    {
                        List paramList = new ArrayList();

                        attribute.append(TagRenderer.translate("#SAPUR_BADVALUE", paramList, manager));

                    }

                } else if (pCell.getSemanticColor() == TableViewCellSemanticColor.CRITICALVALUE_DARK) {

                    {
                        List paramList = new ArrayList();

                        attribute.append(TagRenderer.translate("#SAPUR_CRITICALVALUE", paramList, manager));

                    }

                } else if (pCell.getSemanticColor() == TableViewCellSemanticColor.CRITICALVALUE_MEDIUM) {

                    {
                        List paramList = new ArrayList();

                        attribute.append(TagRenderer.translate("#SAPUR_CRITICALVALUE", paramList, manager));

                    }

                } else if (pCell.getSemanticColor() == TableViewCellSemanticColor.CRITICALVALUE_LIGHT) {

                    {
                        List paramList = new ArrayList();

                        attribute.append(TagRenderer.translate("#SAPUR_CRITICALVALUE", paramList, manager));

                    }

                } else if (pCell.getSemanticColor() == TableViewCellSemanticColor.GOODVALUE_DARK) {

                    {
                        List paramList = new ArrayList();

                        attribute.append(TagRenderer.translate("#SAPUR_GOODVALUE", paramList, manager));

                    }

                } else if (pCell.getSemanticColor() == TableViewCellSemanticColor.GOODVALUE_MEDIUM) {

                    {
                        List paramList = new ArrayList();

                        attribute.append(TagRenderer.translate("#SAPUR_GOODVALUE", paramList, manager));

                    }

                } else if (pCell.getSemanticColor() == TableViewCellSemanticColor.GOODVALUE_LIGHT) {

                    {
                        List paramList = new ArrayList();

                        attribute.append(TagRenderer.translate("#SAPUR_GOODVALUE", paramList, manager));

                    }

                } else if (pCell.getSemanticColor() == TableViewCellSemanticColor.GROUP_HIGHLIGHTED) {

                    {
                        List paramList = new ArrayList();

                        attribute.append(TagRenderer.translate("#SAPUR_STANDARD", paramList, manager));

                    }

                } else if (pCell.getSemanticColor() == TableViewCellSemanticColor.KEY_MEDIUM) {

                    {
                        List paramList = new ArrayList();

                        attribute.append(TagRenderer.translate("#SAPUR_KEY", paramList, manager));

                    }

                } else if (pCell.getSemanticColor() == TableViewCellSemanticColor.MARKED) {

                    {
                        List paramList = new ArrayList();

                        attribute.append(TagRenderer.translate("#SAPUR_STANDARD", paramList, manager));

                    }

                }

                TagRenderer.writeAttribute(out, "title", attribute, encode);
            } //end attribute

            /*
             * ACC Support
             * Add scope property if first column
             */
            {
                if (pIndex == 0) {
                    TagRenderer.writeAttribute(out, "scope", "row", encode);
                }
            }
        }

        { //begin attribute

            StringBuffer attribute = new StringBuffer();
            attribute.append("urSTbvCell");
            if (pDesign == TableViewDesign.TRANSPARENT) {
                attribute.append("Trans");

            } else if (pDesign == TableViewDesign.ALTERNATING) {
                if (pOddRow == true) {
                    attribute.append("Alt");
                }

            } else {

            }

            /*
            if cellindex == 0 we are the first cell
            */

            if (pIndex == 0) {
                if (pSelectionMode == TableViewSelectionMode.NONE) {
                    attribute.append("First");
                } else {
                    attribute.append("Std");
                }
            } else {
                /*
                if cellindex == length - 1 then it's the last cell
                */

                if (pIndex == pLength - 1) {
                    attribute.append("Last");
                } else {
                    attribute.append("Std");
                }
            }

            if (pCell.getSemanticColor() != TableViewCellSemanticColor.STANDARD) {
                attribute.append(" ");

                attribute.append("urSTbvCell");
                if (pCell.getSemanticColor() == TableViewCellSemanticColor.NEGATIVE) {
                    attribute.append("Neg");

                } else if (pCell.getSemanticColor() == TableViewCellSemanticColor.POSITIVE) {
                    attribute.append("Pos");

                } else if (pCell.getSemanticColor() == TableViewCellSemanticColor.TOTAL) {
                    attribute.append("Tot");

                } else if (pCell.getSemanticColor() == TableViewCellSemanticColor.SUBTOTAL) {
                    attribute.append("Subtot");

                } else if (pCell.getSemanticColor() == TableViewCellSemanticColor.SUBTOTAL_LIGHT) {
                    attribute.append("SubtotLight");

                } else if (pCell.getSemanticColor() == TableViewCellSemanticColor.BADVALUE_DARK) {
                    attribute.append("BadDark");

                } else if (pCell.getSemanticColor() == TableViewCellSemanticColor.BADVALUE_MEDIUM) {
                    attribute.append("BadMedium");

                } else if (pCell.getSemanticColor() == TableViewCellSemanticColor.BADVALUE_LIGHT) {
                    attribute.append("BadLight");

                } else if (pCell.getSemanticColor() == TableViewCellSemanticColor.CRITICALVALUE_DARK) {
                    attribute.append("CritDark");

                } else if (pCell.getSemanticColor() == TableViewCellSemanticColor.CRITICALVALUE_MEDIUM) {
                    attribute.append("CritMedium");

                } else if (pCell.getSemanticColor() == TableViewCellSemanticColor.CRITICALVALUE_LIGHT) {
                    attribute.append("CritLight");

                } else if (pCell.getSemanticColor() == TableViewCellSemanticColor.GOODVALUE_DARK) {
                    attribute.append("GoodDark");

                } else if (pCell.getSemanticColor() == TableViewCellSemanticColor.GOODVALUE_MEDIUM) {
                    attribute.append("GoodMedium");

                } else if (pCell.getSemanticColor() == TableViewCellSemanticColor.GOODVALUE_LIGHT) {
                    attribute.append("GoodLight");

                } else if (pCell.getSemanticColor() == TableViewCellSemanticColor.GROUP_HIGHLIGHTED) {
                    attribute.append("GrpHL");

                } else if (pCell.getSemanticColor() == TableViewCellSemanticColor.GROUP_HIGHLIGHTED_LIGHT) {
                    attribute.append("GrpHLLight");

                } else if (pCell.getSemanticColor() == TableViewCellSemanticColor.KEY_MEDIUM) {
                    attribute.append("KeyMedium");

                } else if (pCell.getSemanticColor() == TableViewCellSemanticColor.GROUP_LEVEL1) {
                    attribute.append("GrpLvl1");

                } else if (pCell.getSemanticColor() == TableViewCellSemanticColor.GROUP_LEVEL2) {
                    attribute.append("GrpLvl2");

                } else if (pCell.getSemanticColor() == TableViewCellSemanticColor.GROUP_LEVEL3) {
                    attribute.append("GrpLvl3");

                } else if (pCell.getSemanticColor() == TableViewCellSemanticColor.MARKED) {
                    attribute.append("Marked");

                }
            }

            /*
            if condition="$IsSelected"> <then><value value="' urSTbvCellSel'"></value></then> </if
            */

            if (pCell.getCellType() == TableViewCellType.EDIT) {
                attribute.append(" ");

                attribute.append("urSTbvCellEdit");
            }

            TagRenderer.writeAttribute(out, "class", attribute, encode);
        } //end attribute

        if ((pIndex == pLength - 1) && (pIndex == 0) && pDesign != TableViewDesign.TRANSPARENT) {
            if (((ISystem) manager).getIsRTL()) {
                {

                    String attribute = "solid 1px";

                    _style_collector.append("border-left:").append(attribute).append(";");
                }
            } else {
                {

                    String attribute = "solid 1px";

                    _style_collector.append("border-right:").append(attribute).append(";");
                }
            }
        }

        if (_style_collector.length() != 0)
            TagRenderer.writeAttribute(out, "style", _style_collector.toString(), encode);

        TagRenderer.writeTagRightChar(out);

        {
            com.sapportals.htmlb.table.TableView tableView =
                (com.sapportals.htmlb.table.TableView) pControl.getHtmlBControl();
            int row = ((TableViewCell) pCell).getCurrentRow() + tableView.getVisibleFirstRow() - 1;
            int col = ((TableViewCell) pCell).getCurrentColumn();
            manager.getCurrentForm().pushEventModifier(tableView);
            if (row <= tableView.getRowCount()) {
                com.sapportals.htmlb.table.ICellRenderer myCellRenderer = (tableView.getCellRenderer(row, col));
                myCellRenderer.renderCell(row, col, tableView, manager);

            } else {
                if (tableView.getRowCount() == 0 && row == 1 && col == 1) {
                    if (tableView.getEmptyTableText() != null) {
                        com.sapportals.htmlb.TextView emptyText =
                            new com.sapportals.htmlb.TextView(tableView.getEmptyTableText());
                        manager.render(emptyText);
                    } else {
                        TagRenderer.writeSpace(out);
                    }
                } else {
                    TagRenderer.writeSpace(out);
                }
            }
            manager.getCurrentForm().popEventModifier();
        }
        /*
         * ACC Support
         * Use TH instead of TD
         */
        if (((ISystem) manager).getIs508() && pIndex == 0) {
            TagRenderer.writeEndTag(out, "th");
        } else {
            TagRenderer.writeEndTag(out, "td");
        }
    }
}
