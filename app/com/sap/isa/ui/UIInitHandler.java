/*****************************************************************************
    Class:        UIInitHandler
    Copyright (c) 2004, SAP AG, Germany, All rights reserved.
    Author:       SAP AG
    Created:      25.10.2004
    Version:      1.0

    $Revision: #13 $
    $Date: 2003/05/06 $ 
*****************************************************************************/

package com.sap.isa.ui;

import com.sap.isa.core.init.GenericInitHandler;

/**
 * The class UIInitHandler reads UI relevant information from the init-config.xml
 * file. <br>
 *
 * @author  SAP AG
 * @version 1.0
 */
public class UIInitHandler extends GenericInitHandler {
	
	/**
	 * Name to store the name of an include for a simple header, which will be include
	 * on error pages, if no portal is available. 
	 */
	static protected String SIMPLE_HEADER_NAME = "simple-header";
	
	/**
	 * Return the name of a simple header include. <br>
	 * 
	 * @return name of file or an empty string.
	 */
	static public String getSimpleHeaderIncludeName() {
		String ret = getPropertyValue(SIMPLE_HEADER_NAME);
		if (ret == null) {
			ret = ""; 
		}
		return ret;
	}
	 

}
