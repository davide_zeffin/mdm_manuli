/*****************************************************************************
	Class:        UIControllerData
	Copyright (c) 2005, SAP AG, Germany, All rights reserved.
	Author:       SAP AG
	Created:      22.11.2005
	Version:      1.0

	$Revision:  $
	$Date:  $ 
*****************************************************************************/

package com.sap.isa.ui.uicontrol;

import java.util.Map;

import com.sap.isa.maintenanceobject.businessobject.UIElement;
import com.sap.isa.maintenanceobject.businessobject.UIElementGroup;
import com.sap.isa.maintenanceobject.businessobject.UIField;

/**
 * Interface for accessing UI Controler data
 * 
 * @author  SAP AG
 * @version 1.0
 */
public interface UIControllerData {


	/**
	 * Return the UIField with the given name. <br>
	 * 
	 * @param name of the UI field.
	 * @return the UI field object.
	 */
	public UIField getUIField(String name);

	/**
	 * Return the UIField with the given name. <br>
	 * 
	 * @param name of the UI field.
	 * @param key  (tech)key of the UI Field.
	 * @return the UI field object.
	 */
	public UIField getUIField(String name, String key);

	/**
	 * Returns the UIElement with the given name. <br>
	 * 
	 * @param name of the UI element.
	 * @return the UI element object.
	 */
	public UIElement getUIElement(String name);

	/**
	 * Returns the UIElement with the given name and key <br>
	 * 
	 * @param name name of the UI element.
	 * @param key  (tech)key of the UI element.
	 * @return the UI element object.
	 */
	public UIElement getUIElement(String name, String key);
	
	/**
	 * delete UIElement with the given name and key <br>
	 *  
	 * @param name name of the UI element.
	 * @param key  techkey of the UI element (Element is a list element).
	 */
	public void deleteUIElement(String name, String key);

	
	/**
	 * Delete all individual UIElement with the given name<br>
	 *  
	 * @param name name of the UI element.
	 */
	public void deleteIndividualUIElements(String name);
	
	/**
	 * Delete all individual elements for all UIElement with the given groupNamename<br>
	 *  
	 * @param name name of the UI element.
	 */
	public void deleteIndividualUIElementsInGroup(String name);

	/**
	 * Delete all individual elements for all UIElement with the given group name <code>name</code>
	 * and given key <code>key</code>.<br>
	 *  
	 * @param groupName name of the UI element.
	 */
	public void deleteUIElementsInGroup(String groupName, String key);
	
	/**
	 * Returns the UIElementGroup with the given name. <br>
	 * 
	 * @param name of the UI element group.
	 * @return the UI element group object.
	 */
	public UIElementGroup getUIElementGroup(String name);

	/**
	 * Returns the UIElementGroup with the given name. <br>
	 * 
	 * @param name of the UI element group.
	 * @param key  (tech)key of the UI element group.
	 * @return the UI element group object.
	 */
	public UIElementGroup getUIElementGroup(String name, String key);

	/**
	 * Adds a {@link #UIElement} to the controller. <br>
	 * This allows you to use dynamic elements which are not controlled
	 * via XCM. 
	 * 
	 * @param uiElement {@link #UIElement}
	 */
	public void addUIElement(UIElement uiElement);

	/**
	 * Add a {@link #UIElementGroup} to the controller. <br>
	 * This allows you to use dynamic elements which are not controlled
	 * via XCM. 
	 * 
	 * @param uiElement {@link #UIElementGroup}
	 */
	public void addUIElementGroup(UIElementGroup uiElement);

	/**
	 * Returns the name of the corresponding UIElement for a given backend 
	 * element name key <br>
	 * 
	 * @param key key for a Backend element name 
	 * @return the UIelement name
	 */
		
	public String getUIElementNameForBackendElement(Object key);
	
	/**
	  *  Return the string for a given translate key in the correct language. 
	  *
	  *@param  key     Resource key
	  *@param  args    Additional arguments
	  *@return         translated string
	  */
	 public String translate(String key, String[] args); 

}
