/*****************************************************************************
    Class:        UIElementFactory
    Copyright (c) 2004, SAP AG, Germany, All rights reserved.
    Author:       SAP AG
    Created:      March 2004
    Version:      1.0

    $Revision: #7 $
    $Date: 2001/07/25 $
*****************************************************************************/
package com.sap.isa.ui.uicontrol;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.commons.digester.Digester;

import com.sap.isa.businessobject.BusinessObjectException;
import com.sap.isa.core.PanicException;
import com.sap.isa.core.init.ParamDescription;
import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.core.logging.LogUtil;
import com.sap.isa.core.util.GroupAssignment;
import com.sap.isa.core.xcm.ConfigContainer;
import com.sap.isa.maintenanceobject.backend.boi.AllowedValueData;
import com.sap.isa.maintenanceobject.backend.boi.BackendElementKeyHolder;
import com.sap.isa.maintenanceobject.backend.boi.UIElementBaseData;
import com.sap.isa.maintenanceobject.backend.boi.VisibilityConditionData;
import com.sap.isa.maintenanceobject.businessobject.AllowedValue;
import com.sap.isa.maintenanceobject.businessobject.BackendProperties;
import com.sap.isa.maintenanceobject.businessobject.UIElement;
import com.sap.isa.maintenanceobject.businessobject.UIElementGroup;
import com.sap.isa.maintenanceobject.businessobject.UIField;
import com.sap.isa.maintenanceobject.businessobject.VisibilityCondition;

//TODO Docu
/**
 * The UI element factory read the UI element from the XCM and provides the
 * data to the UI controller.
 * The factory follows the singleton pattern. The only instance could be get with 
 * the {@link #getInstance()}.
 *
 * @author SAP AG
 * @version 1.0
 *
 * @see com.sapmarkets.isa.maintenanceobject.backend.MaintenanceObjectDataHandler
 *
 */
public class UIElementFactory {

	/*
	 * Reference to the IsaLocation. <br>
	 *
	 * @see com.sapmarkets.isa.core.logging.IsaLocation
	 */
	static final private IsaLocation log = IsaLocation.
		  getInstance(UIElementFactory.class.getName());


	/**
	 * tag name for UI elements. <br>
	 */
	static final public String TYPE_UIELEMENT = "UIElement";

	/**
	 * tag name for UI fields. <br>
	 */
	static final public String TYPE_UIFIELD = "UIField";


	/**
	 * Constant for parameter values in XCM. <br>
	 */
	static final protected String EDITABLE = "editable" ;


	/**
	 * Constant for parameter values in XCM. <br>
	 */
	static final protected String HIDDEN = "hidden" ;


	/**
	 * Constant for parameter values in XCM. <br>
	 */
	static protected final String PARAM_PREFIX = "ui.fields." ;


	/**
	 * used instance for Singeleton Pattern. <br>
	 */ 
	static protected UIElementFactory instance = createInstance();
	
	
	static private boolean isInitialized = false;


	/** 
	 * Name of the class to use for element-groups. <br>
	 * 
	 */
	protected String ELEMENT_GROUP_CLASS_NAME = UIElementGroup.class.getName();


	/** 
	 * Name of the class to use for allowed values. <br>
	 * 
	 */
	protected String ALLOWED_VALUE_CLASS_NAME = AllowedValue.class.getName();


	/** 
	 * Name of the class to use for visibility conditions
	 * 
	 */
	protected String VISIBILITY_CONDITION_CLASS_NAME = VisibilityCondition.class.getName();

	
	
	/** 
	 * Name of the class to use for backend properies.
	 * 
	 */
	protected String BACKEND_PROPERTIES_CLASS_NAME = BackendProperties.class.getName();

	
	/** 
	 * Map including Mapping between tag name and class. <br>
	 * See {@link FieldObjectMapping} 
	 */
	protected Map availableRootTags = new HashMap();
	

	/**
	 * The ui data for the current XCM configuration.
	 */
	protected UIControllerData currentControllerData;

	/**
	 * Map with all UI control data with XCM configuration as a key. <br> <br>
	 */
	protected Map uiControllerDataMap = new HashMap();
	
	/**
	 * List with element mappings for parsing process
	 */
	protected List tempElementList = new ArrayList();
	
	/**
	 * Alias for the filename in the <code>xcm-config.xml</code> <br>
	 */
	protected String FILE_ALIAS= "uicontrol-config";	
    
    
    private int debugLevel = 0;
    
    private final static String CRLF = System.getProperty("line.separator");

	/**
	 * Constructor for UIElementFactory. <br>
	 */
	protected UIElementFactory () {
		if (isInitialized) {
			throw new PanicException("Only one instance is allowed in the singelton pattern");
		}
		initInstance();
		isInitialized = true;
	}


	/**
	 * This static method creates the singelton. <br> 
	 * Overwrite this class to provide an other class.
	 * 
	 * @return the created instance.
	 */
	protected static UIElementFactory createInstance() {
		instance = new UIElementFactory();
		return instance;
	}


	/**
	 * This method initialise the singelton and defines the mapping. 
	 * Extends this method if you want change the mapping between tag names and 
	 * class. See {@link #availableRootTags}
	 */
	protected void initInstance() {
		final String METHOD_NAME = "initInstance()";
		log.entering(METHOD_NAME);
		
		List mappings = new ArrayList(1);
		mappings.add(new FieldObjectMapping("UIField",UIField.class.getName()));
		availableRootTags.put(TYPE_UIFIELD, mappings);
		mappings = new ArrayList(1);
		mappings.add(new FieldObjectMapping("UIElement",UIElement.class.getName()));
		availableRootTags.put(TYPE_UIELEMENT, mappings);
		
		log.exiting();
	}

	/**
	 * Return the only instance of the class (Singelton). <br>
	 * 
	 * @return singelton
	 */
	public static UIElementFactory getInstance() {
		return instance;
	}

	/**
	 * 
	 * Given a key for a backend element name and a configuration container 
	 * the method returns the name of the corresponding UIElement   
	 *   
	 * @param name key for an backend name
	 * @return UI element name or <code>null</code> if no config file is found 	 
	 */
	public String getUIElementNameForBackendElement(Object key, ConfigContainer configContainer)	{
		UIControllerData controllerData = getControllerData(configContainer);

		if (controllerData != null) {
			return controllerData.getUIElementNameForBackendElement(key);
		}		
		return null;
	}

	/**
	 * Return a ui element with the given name. <br>
	 * 
	 * @param name of the UI element
	 * @param configContainer config container
	 * @return UI element or <code>null</code> if the element could not be found.
	 */
	public UIElement getUIElement(String name, ConfigContainer configContainer)	{

		UIControllerData controllerData = getControllerData(configContainer);

		if (controllerData != null) {
			UIElement element = controllerData.getUIElement(name);
			if (element != null) {
				try {
	                return (UIElement) element.clone();
	            }
	            catch (CloneNotSupportedException exception) {
	            	log.error(LogUtil.APPS_USER_INTERFACE, "", exception);
	            }
			}
		}

		return  null;
	}

	/**
	 * Return a ui element group with the given name. <br>
	 * 
	 * @param name of the UI element group
	 * @param configContainer config container
	 * @return UI element group or <code>null</code> if the group could not be found.
	 */
	public UIElementGroup getUIElementGroup(String name, ConfigContainer configContainer)	{

		UIControllerData controllerData = getControllerData(configContainer);

		if (controllerData != null) {
			UIElementGroup elementGroup = controllerData.getUIElementGroup(name);
			if (elementGroup != null) {
				return (UIElementGroup) elementGroup.clone();
			}
		}

		return  null;
	}


	// TODO Docu	
	protected synchronized UIControllerData getControllerData(ConfigContainer configContainer)  {
		
        String configKey = configContainer.getConfigKey();
		
		currentControllerData = (UIControllerData)uiControllerDataMap.get(configKey);
		
		if (currentControllerData == null) {
		
			currentControllerData = new UIControllerData();		
			
			if (log.isDebugEnabled()) {
				String test = configContainer.getConfigUsingAliasAsString(FILE_ALIAS);
				log.debug(test);
		    }	
			
			InputStream inputStream = configContainer.getConfigUsingAliasAsStream(FILE_ALIAS);
			try {
                parseConfigFile(inputStream);
				uiControllerDataMap.put(configKey,currentControllerData);
                inputStream.close();
            }
            catch (UIControllerFactoryException e) {
            	log.debug(e.getMessage());
            } catch (IOException e) {
                log.error(LogUtil.APPS_USER_INTERFACE, "",e);
            }
		}
		
		return currentControllerData;
    }

	
    /**
     * Add a given element group to the cuurent ui controller. <br>
     * <strong>Should only used from the parser.</strong>
     *
     */
    public void addUIElementGroup(GroupAssignment elementGroup) {

		try {
	    	UIElementGroup uiElementGroup = (UIElementGroup)Class.forName(ELEMENT_GROUP_CLASS_NAME).newInstance();
	    	uiElementGroup.setName(elementGroup.getName());
	    	currentControllerData.addUIElementGroup(uiElementGroup);
	    	Iterator iter = elementGroup.getElements();
	    	while (iter.hasNext()) {
	    		ParamDescription element = (ParamDescription) iter.next();
	    		if (element.getName()!= null) {
	    			UIElement uiElement = currentControllerData.getUIElement(element.getName());
					if (uiElement != null) {
						uiElementGroup.addElement(uiElement);
					}
	    		}	
			}
		} catch (InstantiationException e) {
			log.error(e);
		} catch (IllegalAccessException e) {
			log.error(e);
		} catch (ClassNotFoundException e) {
			log.error(e);
		}   
    }

    
	/**
	  * Add a given element to the cuurent ui controller. <br>
     * <strong>Should only used from the parser.</strong>
	  *
	  */
	public void addUIElement(UIElement element) {

		currentControllerData.addUIElement(element);
		element.setAllowed(element.isAllowed(),UIElementBaseData.CONFIGURATION);
	}


	/**
	 * Adjust an element with the given ParamDescription. <br>
	 *
	 */
	public void setUIParam(ParamDescription param) {

		String name = param.getName();
		int index = name.indexOf(PARAM_PREFIX);
		
		if (index >= 0) {
			name = name.substring(PARAM_PREFIX.length());
		}
		
		UIElement element = currentControllerData.getUIElement(name);
		if (element != null) {
			String value = param.getValue();
			
			if (value.equals(HIDDEN)) {
				element.setAllowed(false,UIElementBaseData.CONFIGURATION);
			}
			else {
				element.setAllowed(true,UIElementBaseData.CONFIGURATION);
				element.setHidden(false);
			}			

			if (value.equals(EDITABLE)) {
				element.setDisabled(false);
			}
			else {
				element.setDisabled(true);
			}			
			
		}
	}



    /**
     * Set the debugLevel for the xml parse process. <br>
     *
     * @param debugLevel
     * @deprecated debuglevel in the digester is also deprecated.
     *
     */
    public void setDebugLevel(int debugLevel) {
        this.debugLevel = debugLevel;
    }


    /* private method to parse the config-file */
    private void parseConfigFile(InputStream inputStream)
    		throws UIControllerFactoryException {
		
        // new Struts digester
        Digester digester = new Digester();

        digester.push(this);

		Iterator rootTagIter = availableRootTags.values().iterator();
		
		String path = "";
		
		
		while (rootTagIter.hasNext()) {
			
			List mappings = (List)rootTagIter.next();
            
			Iterator iter = mappings.iterator();
	
			while (iter.hasNext()) {
	
	            FieldObjectMapping mapping = (FieldObjectMapping) iter.next();
				
				path = "UIControl/UIElements/" + mapping.getObjectName(); 
	
				// create a new property
				digester.addObjectCreate(path, mapping.getClassName());
	
				// set all properties for the property
				digester.addSetProperties(path);
	
				// add property to property group
				digester.addSetNext(path, "addUIElement", UIElement.class.getName());
	
				// get the help descriptions	
				digester.addCallMethod(path + "/helpDescription","addHelpDescription",0);
	
	
				// create a new allowed value
				digester.addObjectCreate(path + "/allowedValue", ALLOWED_VALUE_CLASS_NAME);
	
				// set all properties for the allowed value
				digester.addSetProperties(path + "/allowedValue");
	
				// add allowed value to the property
				digester.addSetNext(path + "/allowedValue",	"addAllowedValue", AllowedValueData.class.getName());
	
				// create a new backend properties object
				digester.addObjectCreate(path + "/backend",
										 BACKEND_PROPERTIES_CLASS_NAME);

				// set all properties for the backend properties
				digester.addSetProperties(path + "/backend");

				// add backend properties to the property
				digester.addSetNext(path + "/backend", "setBackendProperties", Object.class.getName());

	
				// create a new visibilty condition
				digester.addObjectCreate(path + "/depending", VISIBILITY_CONDITION_CLASS_NAME);
	
				// set all properties for the allowed value
				digester.addSetProperties(path + "/depending");
	
				// add allowed value to the property
				digester.addSetNext(path + "/depending", "addVisibilityCondition", VisibilityConditionData.class.getName());
	
			}
		}		
		
		path = "UIControl/UIElementGroups/UIElementGroup";

        // create a new property group
        digester.addObjectCreate(path, GroupAssignment.class.getName());

        // set all properties for the property group
        digester.addSetProperties(path);

        // add property-group to the maintenance object
        digester.addSetNext(path, "addUIElementGroup", GroupAssignment.class.getName());

        
		path = "UIControl/UIElementGroups/UIElementGroup/element";
	
		// create a new element 
		digester.addObjectCreate(path, ParamDescription.class.getName());

		// set all properties for the element (used for the name)
		digester.addSetProperties(path);

		// add elemet to the temporary group
		digester.addSetNext( path, "addElement", ParamDescription.class.getName());


		path = "UIControl/UIElementParams/param";
		
		// create a new property 
		digester.addObjectCreate(path, ParamDescription.class.getName());

		// set all properties for the property group
		digester.addSetProperties(path);

		// add property-group to the maintenance object
		digester.addSetNext( path, "setUIParam", ParamDescription.class.getName());


		try {
        	digester.parse(inputStream);

        }
        catch (Exception ex) {
            String errMsg = "Error reading configuration information" + CRLF + ex.toString();
            log.error(LogUtil.APPS_COMMON_CONFIGURATION, "system.eai.exception", new Object[] { errMsg}, null);
            	throw new UIControllerFactoryException(errMsg);
        }
    }

	/**
	 * The class FieldObjectMapping describes a mapping between a
	 * element name in the XML file and the corresponding class name. <br>
	 *
	 * @author  SAP AG
	 * @version 1.0
	 */
	public class FieldObjectMapping {

		/**
		 * Name of the XML element. <br>
		 */
		protected String objectName = ""; 
		
		/**
		 * Class name of the corresponding UI Element. <br>
		 */
		protected String className = "";
						

        /**
         * Standard constructor.
         *   
         * @param objectName name of xml object.
         * @param className class name of the corresponding UI element.
         */
        public FieldObjectMapping(String objectName, String className) {
            super();
            this.objectName = objectName;
            this.className = className;
        }
        
        
        /**
         * Return the property {@link #className}. <br>
         * 
         * @return {@link #className}
         */
        public String getClassName() {
            return className;
        }

        /**
         * Set the property {@link #className}. <br>
         * 
         * @param className {@link #className}
         */
        public void setClassName(String className) {
            this.className = className;
        }

        /**
         * Return the property {@link #objectName}. <br>
         * 
         * @return {@link #objectName}
         */
        public String getObjectName() {
            return objectName;
        }

        /**
         * Set the property {@link #objectName}. <br>
         * 
         * @param objectName {@link #objectName}
         */
        public void setObjectName(String objectName) {
            this.objectName = objectName;
        }

	}
	
	/**
	 * The class UIControllerData is the store for all UI data. <br>
	 *
	 * @author  SAP AG
	 * @version 1.0
	 */
	protected class UIControllerData {

		private Map elementMap = new HashMap();
		
		private Map backendIndex = new HashMap();
	
		private List elementGroupList = new ArrayList();
	
		private Map elementGroupMap = new HashMap();
	   
		/**
		 * Add a UIElement to controler data. If the UIElement has 
		 * backend properties, name mapping between UIElement name and Backend name 
		 * is also stored 
		 *   
		 * @param uiElement the UIElement
		 */
		public void addUIElement(UIElement uiElement) {
			elementMap.put(uiElement.getName(), uiElement);
			Object backendProperties = uiElement.getBackendProperties();
			if (backendProperties != null && backendProperties instanceof BackendElementKeyHolder) {
				backendIndex.put(((BackendElementKeyHolder)backendProperties).getKey(),uiElement.getName());
			}
		}

		/**
		 * Add a UIElementGroup to controler data. 
		 *   
		 * @param uiElementGroup the UIElementGroup to store
		 */
		public void addUIElementGroup(UIElementGroup uiElementGroup) {
			elementGroupMap.put(uiElementGroup.getName(), uiElementGroup);
			elementGroupList.add(uiElementGroup);
		}

		/**
		 * Returns the UIElement with a given name  
		 *   
		 * @param name the name of the searched UIElement
		 */
		public UIElement getUIElement(String name) {
			return (UIElement)elementMap.get(name);
		}

		/**
		 * 
		 * Returns the UIElementGroup for  a given name  
		 *   
		 * @param name the group name
		 */
		public UIElementGroup getUIElementGroup(String name) {
			return (UIElementGroup)elementGroupMap.get(name);
		}

		/**
		 * 
		 * Given a key for a backend element name the method returns the name of the corresponding 
		 * UIElement   
		 *   
		 * @param name key for an backend name
		 */
		public String getUIElementNameForBackendElement(Object key) {
			return (String)backendIndex.get(key);
		}


	}
	

	/**
	 * The class UIControllerFactoryException decribes exceptions of the factory. <br>
	 *
	 * @author  SAP AG
	 * @version 1.0
	 */	
	public class UIControllerFactoryException extends BusinessObjectException {
		
        /**
         * 
         */
        public UIControllerFactoryException() {
            super();
        }

        /**
         * @param msg
         */
        public UIControllerFactoryException(String msg) {
            super(msg);
        }


}

}
