/*****************************************************************************
    Class:        UIController
    Copyright (c) 2004, SAP AG, Germany, All rights reserved.
    Author:       SAP AG
    Created:      24.03.2004
    Version:      1.0

    $Revision: #13 $
    $Date: 2003/05/06 $ 
*****************************************************************************/

package com.sap.isa.ui.uicontrol;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import com.sap.isa.core.SessionConst;
import com.sap.isa.core.UserSessionData;
import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.core.util.GenericCacheKey;
import com.sap.isa.core.util.GenericFactory;
import com.sap.isa.core.util.WebUtil;
import com.sap.isa.core.xcm.ConfigContainer;
import com.sap.isa.maintenanceobject.businessobject.UIElement;
import com.sap.isa.maintenanceobject.businessobject.UIElementBase;
import com.sap.isa.maintenanceobject.businessobject.UIElementGroup;
import com.sap.isa.maintenanceobject.businessobject.UIField;


/**
 * The class UIController manages the ui elements for one session. <br>
 * Keep in mind to use always the {@link #getController(UserSessionData)} method to get the
 * correct instance of the controller.
 *
 * @author  SAP AG
 * @version 1.0
 */
public class UIController implements UIControllerData {

	/**
	 * reference to the IsaLocation
	 *
	 * @see com.sapmarkets.isa.core.logging.IsaLocation
	 */
	protected static IsaLocation log = IsaLocation.
		  getInstance(UIController.class.getName());


	/**
	 * Name of the current controller in the userSessionData.
	 */
	public static final String CONTROLLER = "com.sapmarkets.isa.uicontrol.UIController";


	private Map elementMap = new HashMap();

	// map with maps for individual elements
	private Map individualElementMaps = new HashMap();
	
	private List elementGroupList = new ArrayList();
	
	private Map elementGroupMap = new HashMap();
	
	private ConfigContainer configContainer;
	
	private Locale locale;


	/**
	 * Return the controller instance currently store in the user session data. <br>
	 * 
	 * If no instance exist, the instance will be created and stored in user session
	 * context. 
	 * 
	 * @param userData user session data.
	 * @return current controller instance.
	 */
	public static UIController getController(UserSessionData userData) {
		
		UIController controller = (UIController)userData.getAttribute(CONTROLLER);
		
		if (controller == null) {
			controller = new UIController();
			controller.setConfigContainer((ConfigContainer)(userData.getAttribute(SessionConst.XCM_CONFIG)));
			controller.setLocale(userData.getLocale());
			userData.setAttribute(CONTROLLER,controller);
		}

		return controller;		
	}


	/**
	 * private Constructore prevent creating wrong controller. <br> 
	 */
	private UIController() {
	    
	}

	/**
	 * Return the UIField with the given name. <br>
	 * 
	 * @param name of the UI field.
	 * @return the UI field object.
	 */
	public UIField getUIField(String name) {
		return getUIField(name,null);
	}
	
	/**
	 * Returns the UIField with the given name. <br>
	 * 
	 * @param name of the UI field.
	 * @param key  techkey of the UIField.
	 * @return the UI field object.
	 */
	public UIField getUIField(String name, String key) {
		
		
		UIElementBase element = getUIElement(name, key);
		
		if (element instanceof UIField) {
		    return  (UIField)element;
		}
		
		return null;
	}

	
	/**
	 * Returns the UIElement with the given name. <br>
	 * 
	 * @param name of the UI element.
	 * @return the UI element object.
	 */
	public UIElement getUIElement(String name) {
		
		UIElement element = (UIElement)elementMap.get(name);
		
		// no element found -> read element data from configuration
		if (element == null) {
			UIElementFactory factory = getUIControllerFactory();
			if (factory != null) {
				element = factory.getUIElement(name,configContainer);
				if (element != null) {
					elementMap.put(name, element);
				}
			}
		}
		return element;
	}

	/**
	 * finds the UIElement with the given name and key <br>
	 *  
	 * @param name name of the UI element.
	 * @param key  techkey of the UI element (Element is a list element).
	 * @return the UI element object.
	 */
	public UIElement getUIElement(String name, String key) {

		Map indiviualElementMap = getIndualivalElementMap(name);
		UIElement element = (UIElement)indiviualElementMap.get(key);

		// element with key not found, try to find element by name only
		if (element == null) {
			element = getUIElement(name);

			// Element has been found, copy it to the requested key
			if (element!= null) {
				try {
					element = (UIElement)element.clone();
					indiviualElementMap.put(key, element);				
				} catch (CloneNotSupportedException e) {
					log.debug(e.getMessage());
				}
			}
		}

		return element;
	}

	
	/**
	 * Return the map with all individual element with element with the given name. <br>
	 * 
	 * @param name name of ui element.
	 * @return
	 */
	protected Map getIndualivalElementMap(String name) {
		
		Map map = (Map)individualElementMaps.get(name);

		// element with key not found, try to find element by name only
		if (map == null) {
			map = new HashMap();
			individualElementMaps.put(name, map);
		}
		
		return map;
	}
	
	
	/**
	 * delete UIElement with the given name and key <br>
	 *  
	 * @param name name of the UI element.
	 * @param key  techkey of the UI element (Element is a list element).
	 */
	public void deleteUIElement(String name, String key) {

		Map indiviualElementMap = (Map)individualElementMaps.get(name);
		
		if (indiviualElementMap != null) {
			UIElement element = (UIElement)indiviualElementMap.get(key);

			if (element != null) {
				indiviualElementMap.remove(key);
			}
		}	
	}

	
	/**
	 * Delete all individual element for the element with the given name<br>
	 *  
	 * @param name name of the UI element.
	 */
	public void deleteIndividualUIElements(String name) {

		Map indiviualElementMap = (Map)individualElementMaps.get(name);
		
		if (indiviualElementMap != null) {
			individualElementMaps.remove(name);
		}	
	}
	
	/**
	 * Delete all individual elements for all UIElement with the given groupNamename<br>
	 *  
	 * @param name name of the UI element.
	 */
	public void deleteIndividualUIElementsInGroup(String name) {
		
		UIElementGroup group = getUIElementGroup(name);
		
		if (group != null) {
			Iterator iter = group.iterator();
			while (iter.hasNext()) {
				UIElement element = (UIElement)iter.next();
				deleteIndividualUIElements(element.getName());
			}
		}
		else {
			log.debug("Error in deleteIndividualUIElementsInGroup: group: "+ name + " doesn't exist");
		}
	}

	
	/**
	 * Delete all individual elements for all UIElement with the given group name <code>name</code>
	 * and given key <code>key</code>.<br>
	 *  
	 * @param groupName name of the UI element.
	 */
	public void deleteUIElementsInGroup(String groupName, String key) {
		
		UIElementGroup group = getUIElementGroup(groupName);
		
		if (group != null) {
			Iterator iter = group.iterator();
			while (iter.hasNext()) {
				UIElement element = (UIElement)iter.next();
				deleteUIElement(element.getName(),key);
			}
		}
		else {
			log.debug("Error in deleteIndividualUIElementsInGroup: group: "+ groupName + " doesn't exist");
		}
	}


	
	/**
	 * Returns the UIElementGroup for a given name. <br>
	 * 
	 * @param name of the UI element group.
	 * @return the UI element group object.
	 */
	public UIElementGroup getUIElementGroup(String name) {
		return getUIElementGroup(name,null);
	}
	
	
	/**
	 * Returns the UIElementGroup for a given name and key. <br>
	 * 
	 * @param name name of the UI element group.
	 * @param key  (tech)key of the UI element group.
	 * @return the UI element group object.
	 */
	public UIElementGroup getUIElementGroup(String name, String key) {

//TODO 	to clarify: UIElementGroup UIElement both extends UIElementbase. Why
//      are the different maps for each. Is a name collision possible?
// 		what do we need the elementGroupList for?? 	
		
		GenericCacheKey elementKey = new GenericCacheKey(newStringArray(name,key));
		UIElementGroup group = (UIElementGroup)elementGroupMap.get(elementKey);

		// element with key not found, try to find element by name only
		if (group == null) {
			group = (UIElementGroup)elementGroupMap.get(new GenericCacheKey(newStringArray(name,null)));
			
			// no element found -> read element data from configuration
			if (group == null) {
				UIElementFactory factory = getUIControllerFactory();
				if (factory != null) {
					group = factory.getUIElementGroup(name,configContainer);
					if (group != null) {
						addUIElementGroup(group);
					}
				}
			}
			// Group has been found, copy it to the requested key
			if (group!= null) {
				group = (UIElementGroup)group.clone();
				elementGroupMap.put(elementKey, group);				
//TODO 	???:	elementGroupList.add(group);
			}
		}		
		return group;
	}

	/**
	 * Returns the name of the corresponding {@link #UIElement} for a given backend 
	 * element name key <br>
	 * 
	 * @param key key for a Backend element name 
	 * @return the UIelement name
	 */
	public String getUIElementNameForBackendElement(Object key) {
		UIElementFactory factory = getUIControllerFactory();
		if (factory != null) {
			return factory.getUIElementNameForBackendElement(key,configContainer);
		}	
		return null;
	}
	

	/**
	 * Add a {@link #UIElement} to the controller. <br>
	 * This allows you to use dynamic elements which are not controlled
	 * via XCM. 
	 * 
	 * @param uiElement {@link #UIElement}
	 */
	public void addUIElement(UIElement uiElement) {
		elementMap.put(uiElement.getName(), uiElement);
	}


	/**
	 * Add a {@link #UIElementGroup} to the controller. <br>
	 * This allows you to use dynamic elements which are not controlled
	 * via XCM. 
	 * 
	 * @param uiElement {@link #UIElementGroup}
	 */
	public void addUIElementGroup(UIElementGroup uiElementGroup) {
		GenericCacheKey groupKey = new GenericCacheKey(newStringArray(uiElementGroup.getName(),null));
		elementGroupMap.put(groupKey, uiElementGroup);
		elementGroupList.add(uiElementGroup);
	}


    /**
     *  Return the string for a given translate key in the correct language. 
     *
     *@param  key     Resource key
     *@param  args    Additional arguments
     *@return         translated string
     */
    public String translate(String key, String[] args) {
    	return WebUtil.translate(locale,key,args); 
    }
	
	/**
	 * Return the ui controller factory. <br>
	 * The factory will taken from the Generic Factory. It is stored under
	 * teh value <code>uiControllerFactory</code>.
	 * 
	 * @return found factory or <code>UIElementFactory.getInstance()</code> as a fallback.
	 */
	protected UIElementFactory getUIControllerFactory() {

		UIElementFactory factory = (UIElementFactory)GenericFactory.getInstance("uiControllerFactory");

		if (factory == null) {
			log.debug("entry [uiControllerFactory] missing in factory-config.xml");
			factory = UIElementFactory.getInstance();
		}

	    return factory;		
	}

	
    /**
     * Set the property {@link #configContainer}. <br>
     * 
     * @param configContainer The {@link #configContainer} to set.
     */
    private void setConfigContainer(ConfigContainer configContainer) {
        this.configContainer = configContainer;
    }
	/**
	 * Returns the String Array with given name and key or with name only, 
	 * if key is null <br>
	 *  
	 * @return String array of both name and key
	 */
	private String[] newStringArray(String name, String key) {
		return key == null? new String[]{name} : new String[]{name, key}; 
	}


	/**
	 * Set the property {@link #locale}. <br>
	 * 
	 * @param locale The {@link #locale} to set.
	 */
	private void setLocale(Locale locale) {
		this.locale = locale;
	}

}
