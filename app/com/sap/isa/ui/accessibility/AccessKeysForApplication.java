/*
 * Created on Dec 20, 2004
 *
 * To change the template for this generated file go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
package com.sap.isa.ui.accessibility;

import java.util.ArrayList;
import java.util.Iterator;

/**
 * @author i800855
 *
 * To change the template for this generated type comment go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
public class AccessKeysForApplication
{
	protected String appname = "";
	protected ArrayList accesskeys = new ArrayList();


	/**
	 * Add an access key by a translation key and providing a translation key for
	 * the documentation. Non-null values expected.
	 * @param keyForAccessKey : The translation key that represents the access key in this application.
	 * @param keyForRepresentation : The translation key that represents the explanation
	 * of the access key as to what it is used for.
	 * @return The class that defines the access key definition
	 */
	public AccessKeyDefinition addAccessKey(String keyForAccessKey, String keyForRepresentation)
	{
		AccessKeyDefinition def = new AccessKeyDefinition();
		def.setAccesskey(keyForAccessKey);
		def.setAccesskeyrepresents(keyForRepresentation);
		accesskeys.add(def);
		return def;
	}
	
	/**
	 * Remove access keys added
	 * @param keyForAccessKey : The translation key for an access key that has been
	 * added and needs removal
	 */
	public void removeAccessKeys(String keyForAccessKey)
	{
		if (keyForAccessKey == null)
			return;
		Iterator it = getAccesskeys();
		while (it.hasNext())
		{
			AccessKeyDefinition def = (AccessKeyDefinition)it.next();
			if (keyForAccessKey.equalsIgnoreCase(def.getAccesskey()))
				accesskeys.remove(def);
		}
	}
	
	/**
	 * Clears all of the access keys added
	 */
	public void clearAllAccessKeys()
	{
		accesskeys.clear();
	}
	
	/**
	 * Gets all of the access keys
	 * @return
	 */
	public Iterator getAccesskeys()
	{
		return accesskeys.iterator();
	}

	/**
	 * Return the application component name for this structure
	 * @return
	 */
	public String getAppname()
	{
		return appname;
	}

	/**
	 * Sets the application component name for this structure
	 * @param string
	 */
	public void setAppname(String string)
	{
		appname = string;
	}

}
