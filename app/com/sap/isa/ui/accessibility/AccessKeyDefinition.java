/*
 * Created on Dec 17, 2004
 *
 * To change the template for this generated file go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
package com.sap.isa.ui.accessibility;

/**
 * @author i800855
 *
 * To change the template for this generated type comment go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
public class AccessKeyDefinition
{
	protected String accesskey = "";
	protected String accesskeyrepresents = "";
	/**
	 * Fetch the translation key for the access key
	 * @return
	 */
	public String getAccesskey()
	{
		return accesskey;
	}

	/**
	 * Fetch the translation key for the documentation for the access key
	 * @return
	 */
	public String getAccesskeyrepresents()
	{
		return accesskeyrepresents;
	}

	/**
	 * Set the access key
	 * @param string
	 */
	public void setAccesskey(String string)
	{
		accesskey = string;
	}

	/**
	 * Set the documentation for the access key
	 * @param string
	 */
	public void setAccesskeyrepresents(String string)
	{
		accesskeyrepresents = string;
	}

}
