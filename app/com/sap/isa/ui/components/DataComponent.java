package com.sap.isa.ui.components;

import com.sapportals.htmlb.type.*;
import com.sapportals.htmlb.enum.DataType;
import com.sapportals.htmlb.Component;

/**
 * Title:
 * Description:
 * Copyright:    Copyright (c) 2002
 * Company:
 * @author
 * @version 1.0
 */

public class DataComponent extends AbstractDataType {

  private Component compt;
  private DataType type;

  public DataComponent(Component component) {
      compt = component;
  }

  public void setValue(Object parm1) {
  }

  public String getValueAsString() {
      return compt.getUI();
  }

  public Component getValue()  {
      return compt;
  }

  public DataType getType() {
      return null;
  }

  public boolean isLocaleSpecific() {
      return false;
  }

}