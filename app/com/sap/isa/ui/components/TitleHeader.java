/*
 * Created on Sep 29, 2003
 *
 * To change the template for this generated file go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
package com.sap.isa.ui.components;

/**
 * @author I803067
 *
 * To change the template for this generated type comment go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
public class TitleHeader {
	private String text;
	
	public TitleHeader()  {
		
	}
	
	public TitleHeader(String text)  {
		this.text = text;
	}
	
	public void setText(String text)  {
		this.text = text;
	}
	
	public String getText()  {
		return this.text;
	}
}
