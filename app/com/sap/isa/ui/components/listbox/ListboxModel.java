package com.sap.isa.ui.components.listbox;

import java.util.ArrayList;
import java.util.Iterator;

import javax.servlet.http.HttpSession;

import com.sapportals.htmlb.IListModel;

/**
 * Title:
 * Description:
 * Copyright:    Copyright (c) 2002
 * Company:
 * @author
 * @version 1.0
 */

public class ListboxModel implements IListModel  {

    String nameOfKeyColumn;
    String nameOfValueColumn;
    ArrayList keys;
    ArrayList texts;
    ArrayList selection;
    boolean isSingleSelect = true;
    String singleSelect;
    protected HttpSession session;

    public ListboxModel() {
        keys = new ArrayList();
        texts = new ArrayList();
        selection = new ArrayList();
    }
    public ListboxModel(HttpSession session)  {
        this();
        this.session = session;
    }

    public void setData()  {

    }

    public void clearData()  {
        keys.clear();
        texts.clear();
        selection.clear();
    }

    /**
     * adds an item to the list of choices.
     *
     * @param   key     the key of the item
     * @param   text    the visible text of the item
     */
    public void addItem (String key, String text)  {
        keys.add(key);
        texts.add(text);
    }


    /**
     * gets the size of the list, i.e. the number
     * of items.
     *
     * @return  the current size of the list
     */
    public int size ()  {
        return keys.size();
    }


    /**
     * gets the key of an item for a specified index.
     *
     * @param   index   the index of the item in the list
     * @return  the key for that index
     * @throws  IndexOutOfBoundsException
     */
    public String getKeyByIndex (int index)  {
        if ((index < 0) || (index > size())) {
            throw new IndexOutOfBoundsException("Illegal index: " + index);
        }
        return (String) keys.get(index);
    }


    /**
     * gets the visible text of an item for a specified index.
     *
     * @param   index   the index of the item in the list
     * @return  the text for that index
     * @throws  IndexOutOfBoundsException
     */
    public String getTextByIndex (int index)  {
        if ((index < 0) || (index > size())) {
            throw new IndexOutOfBoundsException("Illegal index: " + index);
        }
        return (String) texts.get(index);
    }

    /**
     * gets the key of an item for a specified item.
     *
     * @param   index   the index of the item in the list
     * @return  the key for that text
     */
    public String getKeyByText(String text)  {
        int index = texts.indexOf(text);
        if (index >= 0) {
            return (String) keys.get(index);
        }
        return null;
    }

    /**
     * gets an Iterator for all keys currently in the list
     *
     * @return  an Iterator for the keys
     */
    public Iterator getKeys ()  {
        return keys.iterator();
    }


    /**
     * gets the text associated with the key
     *
     * @param   key     the key of an item
     * @return  the text of that item
     */
    public String getTextForKey (String key)  {
        int index = keys.indexOf(key);
        if (index >= 0) {
            return (String) texts.get(index);
        }
        return null;
    }


    /**
     * determines if the item at the given index is
     * currently selected. Checking for the selection
     * via the index is not supported for reconstructed
     * Lists. Use isSelected(String key) instead.
     *
     * @param   index   the index of the item
     * @return  true if that item is currently selected
     * @see isSelected(String)
     */
    public boolean isSelected (int index)  {
        return isSelected(getKeyByIndex(index));
    }


    /**
     * determines if the item with the given key is
     * currently selected.
     *
     * @param   key     the key of the item
     * @return  true if that item is currently selected
     */
    public boolean isSelected (String key)  {
        return selection.contains(key);
    }


    /**
     * gets the name of the column containing the key
     * values if the list model is associated with a
     * table.
     *
     * @return  the column identifier of the key column
     */
    public String getNameOfKeyColumn ()  {
        return nameOfKeyColumn;
    }


    /**
     * sets the name of the column containing the key
     * values if the list model is associated with a
     * table.
     *
     * @param   nameOfKeyColumn     the column identifier of the key column
     */
    public void setNameOfKeyColumn (String nameOfKeyColumn)  {
        this.nameOfKeyColumn = nameOfKeyColumn;
    }


    /**
     * gets the name of the column containing the visible
     * texts if the list model is associated with a
     * table.
     *
     * @return  the column identifier of the text column
     */
    public String getNameOfTextColumn ()  {
        return nameOfValueColumn;
    }


    /**
     * sets the name of the column containing the visible
     * texts if the list model is associated with a
     * table.
     *
     * @return  the column identifier of the text column
     */
    public void setNameOfTextColumn (String nameOfValueColumn)  {
        this.nameOfValueColumn = nameOfValueColumn;
    }


    /**
     * sets the current selection to the item with the
     * given key.
     *
     * @param   key     the key of the item to select
     */
    public void setSelection (String selection)  {
        this.selection.clear();
        this.selection.add(selection);
    }


    /**
     * determines if the model is a single selection model,
     * i.e. allows only a single item to be selected at any
     * given time.
     *
     * @return  true if the model only allows single selection
     */
    public boolean isSingleSelection ()  {
        return isSingleSelect;
    }


    public void setSingleSelection (boolean isSingleSelection)  {
        this.isSingleSelect = isSingleSelection;
    }


    /**
     * gets the key of the currently selected item if the model
     * is a single selection model.
     *
     * @return  the key of the selected item
     */
    public String getSingleSelection ()  {
        if (isSingleSelect) {
            if (selection.size() == 1) {
                return (String) selection.get(0);
            }
            return null;
        }
        throw new IllegalStateException("List uses multiselection");
    }


    /**
     * gets the keys of the currently selected items if the model
     * is a multi selection model.
     *
     * @return  the keys of the selected item
     */
    public String[] getMultiSelection ()  {
        String[] result = new String[selection.size()];
        for (int i = 0; i < selection.size(); i++)
            result[i] = (String) selection.get(i);
        return result;
        //        return new String[0];
    }


    /**
     * adds the item with the given key to the selection.
     * This should only work in single selection mode.
     *
     * @param   key     the key of the item to add to the selection
     */
    public void addSelection (String selection)  {
        if (!this.selection.contains(selection)) {
            this.selection.add(selection);
        }
    }


    /**
     * adds the item with the given key to the selection.
     * This should only work in single selection mode.
     *
     * @param   key     the key of the item to remove from
     *                  the selection
     */
    public void removeSelection (String selection)  {
        this.selection.remove(selection);
    }

    public void removeItem(String item)  {
        String text = getTextForKey(item);
        keys.remove(item);
        texts.remove(text);
    }
    
    public void clearSelection()  {
    	if(selection != null)  {
    		selection.clear();  
    	}
    }
}
