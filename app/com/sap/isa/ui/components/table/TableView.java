package com.sap.isa.ui.components.table;

/**
 * Title:			Internet Sales Selling Via eBay
 * Description:
 * @Copyright: 		Copyright (c) 2004
 * Company:			SAPLabs LLC
 * Created on:		Nov 29, 2004
 */
public class TableView extends com.sapportals.htmlb.table.TableView {

    public static final String UI_ID = "SVETableView";
    public TableView(String id){
        super(id);
    }
}
