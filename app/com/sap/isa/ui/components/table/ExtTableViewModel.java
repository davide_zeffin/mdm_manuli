package com.sap.isa.ui.components.table;

import java.util.Enumeration;

import com.sapportals.htmlb.enum.TableSelectionMode;
import com.sapportals.htmlb.table.TableColumn;
import com.sapportals.htmlb.table.TableViewModel;

/**
 * Title:
 * Description:
 * Copyright:    Copyright (c) 2002
 * Company:
 * @author
 * @version 1.0
 */

public interface ExtTableViewModel extends TableViewModel {
    public void removeAllColumns();
    public void addColumn(TableColumn column);
    public int getSelectedRow();
    public Enumeration getSelectedRows();
    public TableSelectionMode getTableSelectionMode();
    public int getVisibleFirstRow();
    public boolean isRowSelected(int row);
    public int getVisibleRowCount();
    public void setVisibleRowCount(int count);
	public TableColumn getColumn(String identifier);
	public boolean isRowSelectable(int row);
}
