package com.sap.isa.ui.components.table;

import java.util.ArrayList;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.List;
import java.util.Vector;

import javax.servlet.http.HttpSession;

import org.apache.struts.action.ActionServlet;

import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.ui.components.DataComponent;
import com.sapportals.htmlb.TextView;
import com.sapportals.htmlb.enum.TableColumnType;
import com.sapportals.htmlb.enum.TableSelectionMode;
import com.sapportals.htmlb.table.TableColumn;
import com.sapportals.htmlb.table.TableView;
import com.sapportals.htmlb.type.AbstractDataType;
import com.sapportals.htmlb.type.DataString;
import com.sapportals.htmlb.util.IndexedLinkedList;

/**
 * Title:
 * Description:
 * Copyright:    Copyright (c) 2002
 * Company:
 * @author
 * @version 1.0
 */

public class TableModel implements ExtTableViewModel {


  protected String searchString;
  protected String searchFilter;
  protected int selectedRow;
  protected Hashtable selectedRowSet;
  protected int firstVisibleRow;
  protected int visibleRowCount = 5;
  protected List rowList = new ArrayList();

  // component ids
  protected String tableViewId = null;
  protected String searchInputId = null;
  protected String dropDownId = null;

  protected Vector jspColumns = null;
  protected Vector actualColumns = null;
  private Hashtable jspColumnsIndex = new Hashtable();
  private Hashtable actualColumnsIndex = new Hashtable();
  protected IndexedLinkedList keyColumn = null;

  protected TableSelectionMode tableSelectionMode = TableSelectionMode.NONE;
  protected ActionServlet servlet;
  protected HttpSession session;
  
  /** 
	*  The IsaLocation of the current Action.
	*  This will be instantiated during perform and is always present.
	*/
   protected IsaLocation log = IsaLocation.getInstance(TableModel.class.getName());

  

  public TableModel() {
      jspColumns = new Vector();
      actualColumns = new Vector();
      selectedRowSet = new Hashtable();
      selectedRow = -1;
      initColumns();
  }

  public TableModel(ActionServlet servlet , HttpSession session)  {
      this();
      this.servlet = servlet;
      this.session = session;
  }

  public void initColumns()  {

  }

  public int getRowCount() {
      return getRowList().size();
  }

  public int getColumnCount() {
      return jspColumns.size();
  }

  public void setActionServlet(ActionServlet servlet)  {
      this.servlet = servlet;
  }

  public void setSession(HttpSession session)  {
      this.session = session;
  }

  public String getColumnName(int col) {
      return ((TableColumn) jspColumns.elementAt(col-1)).getTitle();
  }

  public void setColumnName(String parm1, int parm2) {
  }

  public AbstractDataType getValueAt(int row, int col) {
      //return getValueAt(row, ((TableColumn) columns.get(col - 1)).getIdentifier());
      return new DataString(" ");
  }

  public AbstractDataType getValueAt(int parm1, String parm2) {
      return getValueAt(parm1 , getColumnIndex(parm2));
  }


  public void setValueAt(AbstractDataType parm1, int parm2, int parm3) {
  }

  public void setKeyColumn(int columnIndex) {
      int rowCount = this.getRowCount();
      this.keyColumn = new IndexedLinkedList();
      for(int i=1; i<=rowCount; i++){
          this.keyColumn.put(i, this.getValueAt(i, columnIndex));
      }
  }

  public void addKeyColumn(int parm1) {

  }

  public IndexedLinkedList getKeyColumn() {
      return this.keyColumn;
  }

  public Vector getColumns() {
      return jspColumns;
  }

  public TableColumn getColumnAt(int col) {
      return (TableColumn) (jspColumns.elementAt(col-1));
  }

  public TableColumn getColumn(String identifier)  {
      TableColumn column = null;
      try  {
          column = (TableColumn)jspColumnsIndex.get(identifier);
      }  catch(Exception exc)  {
      	  log.debug("Error: ", exc);
          return null;
      }
      return column;
  }

  public TableColumn addColumn(String name) {
      TableColumn column = new TableColumn(this , name);
      jspColumns.add(column);
      jspColumnsIndex.put(column.getIdentifier() , column);
      return column;
  }

  public TableColumn getActualColumn(String identifier)  {
	  TableColumn column = null;
	  try  {
		  column = (TableColumn)actualColumnsIndex.get(identifier);
	  }  catch(Exception exc)  {
		log.debug("Error: ", exc);
		  return null;
	  }
	  return column;
  }

  public TableColumn addActualColumn(String name) {
	  TableColumn column = new TableColumn(this , name);
	  actualColumns.add(column);
	  actualColumnsIndex.put(column.getIdentifier() , column);
	  return column;
  }

  public void removeColumn(String parm1) {
  }

  public int getSelectedRow()  {
    return selectedRow;
  }

  public void setSelectedRow(int newSelectedRow)  {
    selectedRow = newSelectedRow;
  }

  public Enumeration getSelectedRows()  {
    return selectedRowSet.elements();
  }

  public void setSelectedRows(Hashtable newSelectedRowSet)  {
    selectedRowSet = newSelectedRowSet;
  }


  public int getVisibleFirstRow()  {
    return firstVisibleRow;
  }

  public void setVisibleFirstRow(int newFirstVisibleRow)  {
    firstVisibleRow = newFirstVisibleRow;
  }

  public int getVisibleRowCount()  {
    return visibleRowCount;
  }

  public void setVisibleRowCount(int newVisibleRowCount)  {
    visibleRowCount = newVisibleRowCount;
  }

  public List getRowList()  {
    return rowList;
  }

  public void refresh()  {
  }

  public String getIdForRow (int rowIndexByZero)  {
      return Integer.toString(rowIndexByZero+1);
  }

  public boolean isRowSelectable(int rowIndexByZero)  {
      return true;
  }


  public void setTableViewId(String newTableViewId)  {
    tableViewId = newTableViewId;
  }

  public String getTableViewId()  {
    return tableViewId;
  }

  public void setTableSelectionMode(TableSelectionMode newTableSelectionMode)  {
    tableSelectionMode = newTableSelectionMode;
  }

  public TableSelectionMode getTableSelectionMode()  {
    return tableSelectionMode;
  }

  public void removeAllColumns()  {
      jspColumns.removeAllElements();
	  jspColumnsIndex.clear();
  }

  public void addColumn(TableColumn column)  {
      jspColumns.add(column);
      jspColumnsIndex.put(column.getIdentifier() , column);
  }

  public void addActualColumn(TableColumn column)  {
	  actualColumns.add(column);
	  actualColumnsIndex.put(column.getIdentifier() , column);
  }

  public int getColumnIndex(String colId)  {
      int n = -1;
      try  {
          n = jspColumns.indexOf(jspColumnsIndex.get(colId));
      }  catch(Exception exc)  {
		log.debug("Error: ", exc);
      }
      return n+1;
  }

  public boolean isRowSelected(int row)  {
      String rowObject = (String)selectedRowSet.get(Integer.toString(row));
      if(null != rowObject)
          return true;
      else
          return false;
  }

  public void updateSelection(TableView view)  {
      for(int i = view.getVisibleFirstRow() ; i <= view.getVisibleLastRow(); i++)  {
          updateRowSelection(i , view.isRowSelected(i));
      }
  }

  private void updateRowSelection(int row , boolean selected)  {
      String rowObject = (String)selectedRowSet.get(Integer.toString(row));
      if(null == rowObject && selected)  {
          rowObject = Integer.toString(row);
          selectedRowSet.put(rowObject , rowObject);
      }
      if(rowObject != null && !selected)  {
          selectedRowSet.remove(rowObject);
      }
  }

  public void clearSelection()  {
      selectedRowSet.clear();
  }

  protected AbstractDataType getDummyData(String columnName) {
      try  {
          TableColumn column = getColumn(columnName);
          if(column != null && column.getCellType(-1).equals(TableColumnType.USER))  {
              DataComponent comp = new DataComponent(new TextView(""));
              return comp;
          }
      }  catch(Exception exc)  {
		log.debug("Error: ", exc);
      }
      return new DataString("");
  }
}

