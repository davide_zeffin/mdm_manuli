package com.sap.isa.ui.uiclass;

import javax.servlet.jsp.PageContext;

import com.sap.isa.maintenanceobject.action.ActionConstants;
import com.sap.isa.maintenanceobject.businessobject.MaintenanceObject;


abstract public class MaintenanceObjectDynamicUI extends DynamicUIObjectUI {


	public MaintenanceObjectDynamicUI() {
	}

	/**
	 * Constructor 
	 * 
	 * @param pageContext
	 */
	public MaintenanceObjectDynamicUI(PageContext pageContext) {
		initContext(pageContext);
	}
 
	public void determineReadOnly() {
		readOnly = "";
	}

	public void determineFormName() {
		 formName = "propertyForm";
	}
	
	public void exitBeforeMaintenance() {
		
	}

	public void exitAfterMaintenance() {
		
	}

	//TODO docu
	abstract public String getPageTitle();

	//TODO docu
	abstract public String getFormAction();

	//TODO docu
	public String getBodyClass() {
		return "maintainObject";
	}
	
	
	protected void determineHelpAction() {
		// TODO Auto-generated method stub

	}


	protected void determineDynamicUI() {
		object = (MaintenanceObject)request.getAttribute(ActionConstants.RC_PROPERTY);
	}
	
	protected void determinKeyPressEventFunction() {
		keyPressEventFunction = "";
	}

}
