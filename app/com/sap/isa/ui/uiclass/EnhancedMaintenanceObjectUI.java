/*****************************************************************************
    Class:        EnhancedMaintenanceObjectUI
    Copyright (c) 2004, SAP AG, Germany, All rights reserved.
    Author:       SAP AG
    Created:      22.10.2004
    Version:      1.0

    $Revision: #13 $
    $Date: 2003/05/06 $ 
*****************************************************************************/

package com.sap.isa.ui.uiclass;

import java.util.List;

import javax.servlet.jsp.PageContext;

import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.isacore.TabStripHelper;
import com.sap.isa.maintenanceobject.businessobject.MaintenanceObject;
import com.sap.isa.maintenanceobject.businessobject.ScreenGroup;

/**
 * The class EnhancedMaintenanceObjectUI managed the generation of a maintenance object. <br>
 * This is an abstract base class. 
 *
 * @author  SAP AG
 * @version 1.0
 */
abstract public class EnhancedMaintenanceObjectUI extends DynamicUIObjectUI {

    /**
     * Constant to store the property groups to iterate. <br>
     */
    public static final String RA_PROPERTY_GROUPS = "propertyGroups";

    /**
     * Constant to store the property groups to iterate. <br>
     */
    public static final String RA_MESSAGES = "messages";

    
    /**
     * Maintenance object which should be rendered. <br>
     */
    protected MaintenanceObject maintenanceObject;
    
    /**
     * Tab strip helper for menu. <br>
     */
    protected TabStripHelper tabStrip;

    final static private IsaLocation log = IsaLocation.getInstance(EnhancedMaintenanceObjectUI.class.getName());
    /**
     * Constructor for MaintenanceObjectUI. <br>
     * 
     * @param pageContext
     */
    public EnhancedMaintenanceObjectUI(PageContext pageContext) {
        initContext(pageContext);
    }

    /**
     * Constructor for MaintenanceObjectUI. <br>
     * 
     */
    public EnhancedMaintenanceObjectUI() {
    }

    /**
     * Overwrites the method initContext. <br>
     * Initialize the page from the page context.
     * 
     * @param pageContext
     * 
     * 
     * @see com.sap.isa.ui.uiclass.BaseUI#initContext(javax.servlet.jsp.PageContext)
     */
    public void initContext(PageContext pageContext) {
        final String METHOD_NAME = "initContext(PageContext)";
        log.entering(METHOD_NAME);

        super.initContext(pageContext);

        maintenanceObject = (MaintenanceObject)object;
        
        if (activePage.length() > 0 && maintenanceObject.getScreenGroup(activePage) != null) {
            request.setAttribute(RA_PROPERTY_GROUPS, maintenanceObject.getScreenGroup(activePage));
        }
        else {
            request.setAttribute(RA_PROPERTY_GROUPS, maintenanceObject);
        }

        request.setAttribute(RA_MESSAGES, maintenanceObject);

        log.exiting();
    }


    protected void initPages() {

    	super.initPages();
    	
    	maintenanceObject = (MaintenanceObject)object;
    	
        tabStrip = new TabStripHelper();

        TabStripHelper.TabButton tabButton;

        List screenGroups = maintenanceObject.getScreenGroupList();

        for (int i = 0; i < screenGroups.size(); i++) {

            ScreenGroup screenGroup = (ScreenGroup) screenGroups.get(i);
            /* first add all buttons, which will be used */

            if (!screenGroup.isHidden()) {
                tabButton =
                    tabStrip.createTabButton(
                        screenGroup.getName(),
                        screenGroup.getDescription(),
                        "javascript:SelectTabStrip(\'" + screenGroup.getName() + "\')");
                tabStrip.addTabButton(tabButton);
            }
        }

        tabStrip.setActiveButton(activePage);
    }


    /**
     * Return if the menu for different screen groups should be displayed. <br>
     * 
     * @return <code>true</code> if the menu should be displayed.
     */
    public boolean showMenu() {

        boolean ret = false;

        if (maintenanceObject.getScreenGroupList() != null && maintenanceObject.getScreenGroupList().size() > 0) {
            return true;
        }

        return ret;

    }

    /**
     * Return the property {@link #activeScreenGroup}. <br>
     * 
     * @return {@link #activeScreenGroup}
     */
    public String getActiveScreenGroup() {
        return activePage;
    }


    /**
     * Return the property {@link #object}. <br>
     * 
     * @return {@link #object}
     */
    public MaintenanceObject getMaintenanceObject() {
        return maintenanceObject;
    }


    /**
     * Return the property {@link #tabStrip}. <br>
     * 
     * @return {@link #tabStrip}
     */
    public TabStripHelper getTabStrip() {
        return tabStrip;
    }

}
