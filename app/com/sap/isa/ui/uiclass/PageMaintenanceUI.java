package com.sap.isa.ui.uiclass;

import javax.servlet.jsp.PageContext;

import com.sap.isa.core.util.WebUtil;
import com.sap.isa.maintenanceobject.action.ActionConstants;
import com.sap.isa.maintenanceobject.businessobject.MaintenanceObject;


public class PageMaintenanceUI extends MaintenanceObjectDynamicUI {

	
	/**
	 * Constructor for ShopUI.
	 * 
	 * @param pageContext
	 */
	public PageMaintenanceUI(PageContext pageContext) {
		initContext(pageContext);
	}
 
	public void determineFormName() {
		 formName = "pageForm";
	}

	protected void determineDynamicUI() {
		object = (MaintenanceObject)request.getAttribute(ActionConstants.RC_PAGE);
	}

	public String getFormAction() {
		return WebUtil.getAppsURL(pageContext,null,"dynamicUIMaintenance/maintainPage.do",null,null,false);
	}
	
	public void exitAfterMaintenance() {
		includePage("/appbase/dynamicUI/designer/maintainDescription.inc.jsp", this);		
	}
	

	public String getPageTitle() {
		return translate("fw.du.designer.page.title");
	}


}
