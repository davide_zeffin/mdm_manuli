/* @author SAP
 * Created on Sep 15, 2004
 * 
 */
package com.sap.isa.ui.uiclass.r3.genericsearch;

import com.sap.isa.core.util.table.ResultData;
import com.sap.isa.core.util.table.Table;
import com.sap.isa.core.util.table.TableRow;
import com.sap.isa.ui.uiclass.genericsearch.GenericSearchUIDynamicContent;
import com.sap.isa.ui.uiclass.genericsearch.GenericSearchUIFactory;
import com.sap.isa.businessobject.BusinessObjectManager;
import com.sap.isa.businessobject.Shop;
import com.sap.isa.businessobject.order.DocumentListFilter;
import com.sap.isa.backend.r3base.util.Conversion;

/*
 * This class is the extension to the GenericSearchUIDynamicContent
 * class, which is used in the CRM context.
 * Some methods have to be overwritten to have the behaviour suitable
 * to ERP backend
 */
public class GenericSearchUIDynamicContentERP
	extends GenericSearchUIDynamicContent {

	
	public GenericSearchUIDynamicContentERP() {
		super();
		// TODO Auto-generated constructor stub
	}
	/**
	 * Return Attributes to point to a Sales Document's detail. By using 
	 * Attribute ID <b>singlerow</b> the appropriated ResultData object 
	 * will be retrieved from pageContext. ID is defined in the genericsearch.jsp.
	 * @return String containing the attributes
	 */
	public String buildAttributesForSalesDocumentDetails() {
		ResultData rsData = (ResultData)pageContext.getAttribute("singlerow");
		String objecttypeIN = "";
		String objecttypeOUT = "";
		if (searchRequestMemory != null) {
			objecttypeIN = (String)searchRequestMemory.get("rc_documenttypes");        
		} else {
			objecttypeIN = (String)request.getParameter("rc_documenttypes");
		}
		if ("ORDER".equals(objecttypeIN)) {
			objecttypeOUT = DocumentListFilter.SALESDOCUMENT_TYPE_ORDER;
		} else if ("QUOTATION".equals(objecttypeIN) || "INQUIRY".equals(objecttypeIN)) {
			objecttypeOUT = DocumentListFilter.SALESDOCUMENT_TYPE_QUOTATION;
		} else if ("ORDERTMP".equals(objecttypeIN)) {
			// Ordertemplates
			objecttypeOUT = DocumentListFilter.SALESDOCUMENT_TYPE_ORDERTEMPLATE;
		} else if ("CONTRACT".equals(objecttypeIN)) {
		// Ordertemplates
		objecttypeOUT = DocumentListFilter.DOCUMENT_TYPE_CONTRACT;
	}
		StringBuffer strB = new StringBuffer("techkey=");
		strB.append(rsData.getString("GUID"));		
		strB.append("&object_id=");
		strB.append(rsData.getString("VBELN"));
		strB.append("&objecttype=");
		strB.append(objecttypeOUT);
		strB.append("&processtype=");
		strB.append(rsData.getString("AUART"));
		return strB.toString();        
	}
	
	/**
	 * Return Attributes to point to a <b>B2C</b> Sales Document's detail.
	 * @return String containing the attributes
	 */
	public String buildAttributesForSalesDocumentDetailsB2C() {
		ResultData rsData = (ResultData)pageContext.getAttribute("singlerow");
		StringBuffer strB = new StringBuffer("techkey=");
		strB.append(rsData.getRowKey().toString());
		strB.append("&object_id=");
		strB.append(rsData.getString("VBELN"));
		strB.append("&objecttype=");
		/* Directly enter the objecttype as order, as AUART doesn't work,
		 * As of now only kind of documents allowed in B2C are orders, so this is fine
		 */ 
		strB.append(DocumentListFilter.SALESDOCUMENT_TYPE_ORDER);		
		return strB.toString();        
	}
	/**
	 * Return Attributes to point to a Contracts's detail.
	 * @return String containing the attributes
	 */
	public String buildAttributesForContractDocumentDetails() {
		ResultData rsData = (ResultData)pageContext.getAttribute("singlerow");
		StringBuffer strB = new StringBuffer();
		strB.append("contractselect.do?contractSelected=");
		strB.append(rsData.getString("GUID"));
		return strB.toString();
	}
    
	public String buildShowCustomerDetails() {
		   ResultData rsData = (ResultData)pageContext.getAttribute("singlerow");
		   StringBuffer strB = new StringBuffer();
		   strB.append("javascript:showSoldTo('");
		   try {
			   strB.append(rsData.getString("GUID"));
		   } catch (ResultData.UnknownColumnNameException ucnEx) {
		   	if (log.isDebugEnabled()) {
				log.debug("Error ", ucnEx);
		   	}
		   }
//		   try {
//			   strB.append(rsData.getString("PAYERS_GUID"));
//		   } catch (ResultData.UnknownColumnNameException ucnEx) {
//		   }
		   strB.append("');");
		   return strB.toString();
	}
	/**
     * Return the Creation Date field in the User format. The date field returned from backend
     * is unformatted, therefore, Locales are used to format the date field(consistent).
     */
    public Table getAudatToUIDate() {
        // check for missing context
        Shop shop = null;;
        if (userSessionData != null) {
            // get BOM
            BusinessObjectManager bom = (BusinessObjectManager) userSessionData.getBOM(BusinessObjectManager.ISACORE_BOM);
            // now I can read the shop
            shop = bom.getShop();
        }
        ResultData rsData = (ResultData)pageContext.getAttribute("singlerow");
        Table fieldT = GenericSearchUIFactory.getUIReturnTable(GenericSearchUIFactory.TYPE_RETURNTABLE_OUTPUTFIELD);
         
        String valueAudat = rsData.getString("AUDAT");
        if ( valueAudat != null) {
        valueAudat = Conversion.erpDateStringToUIDateString( valueAudat, shop.getDateFormat());
        //Fill Table
        TableRow tabrow = fieldT.insertRow();
        tabrow.setValue("TYPE", "FIELD");
        tabrow.setValue("VALUE", valueAudat);
        }        
        return fieldT;
    }
    /**
     * Returns the Valid from Date field in the User format. The date field returned from backend
     * is unformatted, therefore, Locales are used to format the date field(consistent).
     */
    public Table getDatabToUIDate() {
        // check for missing context
        Shop shop = null;;
        if (userSessionData != null) {
            // get BOM
            BusinessObjectManager bom = (BusinessObjectManager) userSessionData.getBOM(BusinessObjectManager.ISACORE_BOM);
            // now I can read the shop
            shop = bom.getShop();
        }
        ResultData rsData = (ResultData)pageContext.getAttribute("singlerow");
        Table fieldT = GenericSearchUIFactory.getUIReturnTable(GenericSearchUIFactory.TYPE_RETURNTABLE_OUTPUTFIELD);
         
        String valueDatab = rsData.getString("DATAB");
        if ( valueDatab != null) {
        valueDatab = Conversion.erpDateStringToUIDateString( valueDatab, shop.getDateFormat());
        //Fill Table
        TableRow tabrow = fieldT.insertRow();
        tabrow.setValue("TYPE", "FIELD");
        tabrow.setValue("VALUE", valueDatab);
        }
        
        return fieldT;
    }
    /**
     * Returns the Valid to Date field in the User format. The date field returned from backend
     * is unformatted, therefore, Locales are used to format the date field(consistent).
     */
    public Table getDatbiToUIDate() {
        // check for missing context
        Shop shop = null;;
        if (userSessionData != null) {
            // get BOM
            BusinessObjectManager bom = (BusinessObjectManager) userSessionData.getBOM(BusinessObjectManager.ISACORE_BOM);
            // now I can read the shop
            shop = bom.getShop();
        }
        ResultData rsData = (ResultData)pageContext.getAttribute("singlerow");
        Table fieldT = GenericSearchUIFactory.getUIReturnTable(GenericSearchUIFactory.TYPE_RETURNTABLE_OUTPUTFIELD);
        
        String valueDatbi = rsData.getString("DATBI");
        if ( valueDatbi != null) {
        valueDatbi = Conversion.erpDateStringToUIDateString( valueDatbi, shop.getDateFormat());
        //Fill Table
        TableRow tabrow = fieldT.insertRow();
        tabrow.setValue("TYPE", "FIELD");
        tabrow.setValue("VALUE", valueDatbi);
        }
        return fieldT;
    }
	/**
	 * Returns the Valid to Date field in the User format. The date field returned from backend
	 * is unformatted, therefore, Locales are used to format the date field(consistent).
	 */
	public Table getBlineDateToUIDate() {
		// check for missing context
		Shop shop = null;;
		if (userSessionData != null) {
			// get BOM
			BusinessObjectManager bom = (BusinessObjectManager) userSessionData.getBOM(BusinessObjectManager.ISACORE_BOM);
			// now I can read the shop
			shop = bom.getShop();
		}
		ResultData rsData = (ResultData)pageContext.getAttribute("singlerow");
		Table fieldT = GenericSearchUIFactory.getUIReturnTable(GenericSearchUIFactory.TYPE_RETURNTABLE_OUTPUTFIELD);
        
		String valueDatbi = rsData.getString("BLINE_DATE");
		if ( valueDatbi != null) {
		valueDatbi = Conversion.erpDateStringToUIDateString( valueDatbi, shop.getDateFormat());
		//Fill Table
		TableRow tabrow = fieldT.insertRow();
		tabrow.setValue("TYPE", "FIELD");
		tabrow.setValue("VALUE", valueDatbi);
		}
		return fieldT;
	}
	/**
	 * Returns the Billing Date field in the User format. The date field returned from backend
	 * is unformatted, therefore, Locales are used to format the date field(consistent).
	 */
	public Table getBillDateToUIDate() {
		// check for missing context
		Shop shop = null;;
		if (userSessionData != null) {
			// get BOM
			BusinessObjectManager bom = (BusinessObjectManager) userSessionData.getBOM(BusinessObjectManager.ISACORE_BOM);
			// now I can read the shop
			shop = bom.getShop();
		}
		ResultData rsData = (ResultData)pageContext.getAttribute("singlerow");
		Table fieldT = GenericSearchUIFactory.getUIReturnTable(GenericSearchUIFactory.TYPE_RETURNTABLE_OUTPUTFIELD);
        
		String valueDatbi = rsData.getString("BILL_DATE");
		if ( valueDatbi != null) {
		valueDatbi = Conversion.erpDateStringToUIDateString( valueDatbi, shop.getDateFormat());
		//Fill Table
		TableRow tabrow = fieldT.insertRow();
		tabrow.setValue("TYPE", "FIELD");
		tabrow.setValue("VALUE", valueDatbi);
		}
		return fieldT;
	}
    /**
     * Formats the NetValue field according to currenc and the shop settings for decimal formatting.
     * Returns the NetValue and Currency as one field. Therefore the Result from Backend
     * must include the fields <code>NET_VALUE</code> and <code>DOC_CURRENCY</code>.
     */
    public Table buildBillingNetValue() {
        // check for missing context
        Shop shop = null;;
        if (userSessionData != null) {
            // get BOM
            BusinessObjectManager bom = (BusinessObjectManager) userSessionData.getBOM(BusinessObjectManager.ISACORE_BOM);
            // now I can read the shop
            shop = bom.getShop();
        }
        ResultData rsData = (ResultData)pageContext.getAttribute("singlerow");
        Table fieldT = GenericSearchUIFactory.getUIReturnTable(GenericSearchUIFactory.TYPE_RETURNTABLE_OUTPUTFIELD);
        // Fill Table        
        TableRow tabrow = fieldT.insertRow();
        tabrow.setValue("TYPE", "FIELD");
                
        String currency = ""; 
        if (shop.isBillingSelectionCRM()  ||  shop.isBillingSelectionR3CRM()) {
            currency = rsData.getString("DOC_CURRENCY");
        }
        if (shop.isBillingSelectionR3()) {
            currency = rsData.getString("CURRENCY");
        }
        String value = Conversion.erpCurrencyStringToUICurrencyString( rsData.getString("NET_VALUE"),
	               shop.getDecimalPointFormat(), shop.getDecimalSeparator(), 
				   shop.getGroupingSeparator(), currency );
        
        String ffield = ("<nobr>" + value + "&nbsp;" + currency + "</nobr>");
        tabrow.setValue("VALUE", ffield);
        
        return fieldT;
    }
    
	/**
	 * Return Attributes to point to a Billing Document's detail. By using 
	 * Attribute ID <b>singlerow</b> the appropriated ResultData object 
	 * will be retrieved from pageContext. ID is defined in the genericsearch.jsp.
	 * <br> This overrides the super implementation and adds the sold-to key.
	 * 
	 * @return String containing the attributes
	 */
	public String buildAttributesForBillingDocumentDetails() {
		String url = super.buildAttributesForBillingDocumentDetails();
		
		ResultData rsData = (ResultData)pageContext.getAttribute("singlerow");
		
		url = url + "&sold_to=" + rsData.getString("SOLD_TO");
		if (log.isDebugEnabled()){
			log.debug("buildAttributesForBillingDocumentDetails, appended: "+ rsData.getString("SOLD_TO"));
		}
		return url;
	}
    
}
