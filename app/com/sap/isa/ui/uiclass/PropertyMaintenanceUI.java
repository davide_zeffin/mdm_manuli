package com.sap.isa.ui.uiclass;

import javax.servlet.jsp.PageContext;

import com.sap.isa.core.util.WebUtil;
import com.sap.isa.maintenanceobject.action.ActionConstants;
import com.sap.isa.maintenanceobject.businessobject.MaintenanceObject;


public class PropertyMaintenanceUI extends MaintenanceObjectDynamicUI {

	
	/**
	 * Constructor for ShopUI.
	 * 
	 * @param pageContext
	 */
	public PropertyMaintenanceUI(PageContext pageContext) {
		initContext(pageContext);
	}
 
	public void determineFormName() {
		 formName = "propertyForm";
	}

	protected void determineDynamicUI() {
		object = (MaintenanceObject)request.getAttribute(ActionConstants.RC_PROPERTY);
	}

	public String getFormAction() {
		return WebUtil.getAppsURL(pageContext,null,"dynamicUIMaintenance/maintainProperty.do",null,null,false);
	}
	
	public void exitAfterMaintenance() {
		includePage("/ipc/dynamicUI/designer/values.inc.jsp", this);		
	}
	

	public String getPageTitle() {
		return translate("fw.du.designer.property.title");
	}


}
