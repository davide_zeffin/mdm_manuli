/*****************************************************************************
    Class:        MaintenanceObjectUI
    Copyright (c) 2004, SAP AG, Germany, All rights reserved.
    Author:       SAP AG
    Created:      22.10.2004
    Version:      1.0

    $Revision: #13 $
    $Date: 2003/05/06 $ 
*****************************************************************************/

package com.sap.isa.ui.uiclass;

import java.util.List;

import javax.servlet.jsp.PageContext;

import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.isacore.TabStripHelper;
import com.sap.isa.maintenanceobject.businessobject.MaintenanceObject;
import com.sap.isa.maintenanceobject.businessobject.Property;
import com.sap.isa.maintenanceobject.businessobject.ScreenGroup;

/**
 * The class MaintenanceObjectUI managed the generation of a maintenance object. <br>
 * This is an abstract base class. 
 *
 * @author  SAP AG
 * @version 1.0
 */
abstract public class MaintenanceObjectUI extends BaseUI {

    /**
     * Constant to store the property groups to iterate. <br>
     */
    public static final String RA_PROPERTY_GROUPS = "propertyGroups";

    /**
     * Constant to store the property groups to iterate. <br>
     */
    public static final String RA_MESSAGES = "messages";

    /**
     * Maintenance object which should be rendered. <br>
     */
    protected MaintenanceObject object;

    /**
     * Flag if the page is rendered editable or ReadOnly. <br>
     */
    protected String readOnly = "false";

    /**
     * The name of the HTML form which should be submitted. <br>
     */
    protected String formName = "";

    /**
     * Name of the action which displays the help description. <br>
     */
    protected String helpAction = "";

    /**
     * active screen group. <br>
     */
    protected String activeScreenGroup = "";

    /**
     * Tab strip helper for menu. <br>
     */
    protected TabStripHelper tabStrip;

    final static private IsaLocation log = IsaLocation.getInstance(MaintenanceObjectUI.class.getName());
    /**
     * Constructor for MaintenanceObjectUI. <br>
     * 
     * @param pageContext
     */
    public MaintenanceObjectUI(PageContext pageContext) {
        initContext(pageContext);
    }

    /**
     * Constructor for MaintenanceObjectUI. <br>
     * 
     */
    public MaintenanceObjectUI() {
    }

    /**
     * Overwrites the method initContext. <br>
     * Initialize the page from the page context.
     * 
     * @param pageContext
     * 
     * 
     * @see com.sap.isa.ui.uiclass.BaseUI#initContext(javax.servlet.jsp.PageContext)
     */
    public void initContext(PageContext pageContext) {
        final String METHOD_NAME = "initContext(PageContext)";
        log.entering(METHOD_NAME);

        super.initContext(pageContext);

        determineReadOnly();

        if (readOnly == null) {
            readOnly = "false";
        }

        determineMaintenanceObject();

        activeScreenGroup = object.getActiveScreenGroup();

        if (activeScreenGroup.length() > 0 && object.getScreenGroup(activeScreenGroup) != null) {
            request.setAttribute(RA_PROPERTY_GROUPS, object.getScreenGroup(activeScreenGroup));
        }
        else {
            request.setAttribute(RA_PROPERTY_GROUPS, object);
        }

        request.setAttribute(RA_MESSAGES, object);

        determineHelpAction();
        determineFormName();

        initScreenGroupTabs();
        log.exiting();
    }

    protected void initScreenGroupTabs() {

        tabStrip = new TabStripHelper();

        TabStripHelper.TabButton tabButton;

        List screenGroups = object.getScreenGroupList();

        for (int i = 0; i < screenGroups.size(); i++) {

            ScreenGroup screenGroup = (ScreenGroup) screenGroups.get(i);
            /* first add all buttons, which will be used */

            if (!screenGroup.isHidden()) {
                tabButton =
                    tabStrip.createTabButton(
                        screenGroup.getName(),
                        screenGroup.getDescription(),
                        "javascript:SelectTabStrip(\'" + screenGroup.getName() + "\')");
                tabStrip.addTabButton(tabButton);
            }
        }

        tabStrip.setActiveButton(activeScreenGroup);

    }

    /**
     * Determine the property {@link #object}. <br>
     * 
     */
    abstract protected void determineMaintenanceObject();

    /**
     * Determine the property {@link #readOnly}. <br>
     * 
     */
    abstract protected void determineReadOnly();

    /**
     * Determine the property {@link #formName}. <br>
     * 
     */
    abstract protected void determineFormName();

    /**
     * Determine the property {@link #helpValuesAction}. <br>
     * 
     */
    abstract protected void determineHelpAction();

    /**
     * Return if the menu for different screen groups should be displayed. <br>
     * 
     * @return <code>true</code> if the menu should be displayed.
     */
    public boolean showMenu() {

        boolean ret = false;

        if (object.getScreenGroupList() != null && object.getScreenGroupList().size() > 0) {
            return true;
        }

        return ret;

    }

    /**
     * Return the property {@link #activeScreenGroup}. <br>
     * 
     * @return {@link #activeScreenGroup}
     */
    public String getActiveScreenGroup() {
        return activeScreenGroup;
    }

    /**
     * Return the property {@link #formName}. <br>
     * 
     * @return
     */
    public String getFormName() {
        return formName;
    }

    /**
     * Return the property {@link #helpAction}. <br>
     * 
     * @return {@link #helpAction}
     */
    public String getHelpAction() {
        return helpAction;
    }

    /**
     * Return the property {@link #object}. <br>
     * 
     * @return {@link #object}
     */
    public MaintenanceObject getObject() {
        return object;
    }

    /**
     * Return the property {@link #readOnly}. <br>
     * 
     * @return {@link #readOnly}
     */
    public String getReadOnly() {
        return readOnly;
    }

    /**
     * Return the property {@link #tabStrip}. <br>
     * 
     * @return {@link #tabStrip}
     */
    public TabStripHelper getTabStrip() {
        return tabStrip;
    }

    /**
     * Generate the accessibilty messages from the maintenance object . <br>
     * 
     * @see com.sap.isa.ui.uiclass.BaseUI#generateAccMessages()
     */
    public void generateAccMessages() {
        if (object != null) {
            setAccessibilityMessages(object.getMessageList());
        }
    }

    /**
     * Sets the FormName.
     * @param string formName
     */
    public void setFormName(String formName) {
        this.formName = formName;

    }

    /**
     * Sets the readOnly flag.
     * @param string readOnly
     */
    public void setReadOnly(String readOnly) {
        this.readOnly = readOnly;
    }

    /**
     * returns the PropertyDescription depending on attribute isRessourceKeyUsed
     * @param property
     * @return String
     */
    public String getPropertyDescription(Property property) {

        if (!property.isRessourceKeyUsed()) {
            return property.getDescription();
        }
        return translate(property.getDescription());
    }

}
