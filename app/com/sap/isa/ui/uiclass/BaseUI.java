package com.sap.isa.ui.uiclass;

import java.util.Locale;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.jsp.JspException;
import javax.servlet.jsp.PageContext;

import com.sap.isa.businessobject.CommunicationException;
import com.sap.isa.core.Constants;
import com.sap.isa.core.ContextConst;
import com.sap.isa.core.SessionConst;
import com.sap.isa.core.UserSessionData;
import com.sap.isa.core.action.SwitchAccessibilityAction;
import com.sap.isa.core.interaction.InteractionConfigContainer;
import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.core.logging.LogUtil;
import com.sap.isa.core.taglib.MessageTag;
import com.sap.isa.core.ui.layout.AreaContext;
import com.sap.isa.core.ui.layout.UILayout;
import com.sap.isa.core.ui.layout.UILayoutManager;
import com.sap.isa.core.util.JspUtil;
import com.sap.isa.core.util.Message;
import com.sap.isa.core.util.MessageList;
import com.sap.isa.core.util.MessageListDisplayer;
import com.sap.isa.core.util.RequestParser;
import com.sap.isa.core.util.StartupParameter;
import com.sap.isa.core.util.WebUtil;
import com.sap.isa.core.xcm.ConfigContainer;
import com.sap.isa.core.xcm.xml.ParamsStrategy;
import com.sap.isa.helpvalues.HelpValues;
import com.sap.isa.helpvalues.HelpValuesSearch;
import com.sap.isa.helpvalues.HelpValuesSearchManager;
import com.sap.isa.helpvalues.uiclass.HelpValuesSearchUI;
import com.sap.isa.isacore.action.DetermineBrowserVersionAction;
import com.sap.isa.maintenanceobject.businessobject.UIElement;
import com.sap.isa.maintenanceobject.businessobject.UIElementBase;
import com.sap.isa.maintenanceobject.businessobject.UIElementGroup;
import com.sap.isa.ui.uicontrol.UIController;

// TODO browser type und version standard festlegen.

/**
 * The class BaseUI is a base for all UI classes. <br>
 * 
 * The UI classes are used to seperate the buisness logic from the coding
 * on the JSP. <br> 
 *
 * This base class provide you easy access to context data, like user session data, 
 * portal flag etc., which could be usefull. <br>   
 *
 * @author SAP
 * @version 1.0
 **/
public class BaseUI {

	/**
	 * Constant to store the current include context in the request context. <br>
	 */
	final static public String INCLUDE_CONTEXT =  ContextConst.UI_INCLUDE_CONTEXT;

	/**
	 * Reference to the IsaLocation. <br>
	 *
	 * @see com.sap.isa.core.logging.IsaLocation
	 */
	static public IsaLocation log = null;
	

	/**
	 * Flag, if the "toogle" feature is available for the browser version. <br>
	 */ 
	public boolean isToggleSupported = false;

	/**
	 * global property, if the application runs in the accessibility mode. <br>
	 */
	public boolean isAccessible = false;

	/**
	 * global property, if the application runs in the portal environment. <br>
	 */
	public boolean isPortal = false;
 
	/**
	 * Constant for a line separator. <br>
	 */
	protected final static String CRLF = System.getProperty("line.separator");

    /**
     * Reference to the http request. <br> 
     */
    protected HttpServletRequest request;

	/**
	 * Reference to the JSP page context. <br> 
	 */
    protected PageContext pageContext;

	/**
	 * Reference to the user session data. <br>
	 * <strong>Use always user session instead of the http session.</strong> 
	 */
    protected UserSessionData userSessionData;

	/**
	 * Reference to the use browser type (version). <br>
	 */    
    protected DetermineBrowserVersionAction.BrowserVersion browserType;
    
    /**
     * Reference to a request parser for the http {@link #request}. 
     */
    protected RequestParser requestParser;
    
	/**
	 * Reference to a request parser for the http {@link #request}. 
	 */
    protected StartupParameter startupParameter;

	/**
	 * Messages to display in accessibility mode. <br>
	 */
	protected MessageList accessibilityMessages;

	/**
	 * Reference to the ui controller. <br>
	 */
	protected UIController uiController;
	
	/**
	 * Constructor for UIBase.
	 */
	public BaseUI() {
	
		if (log== null) {
			log = IsaLocation. getInstance(this.getClass().getName());	
		}
	}
      	
	/**
     * Constructor for UIBase.
     * 
     * @param pageContext
     */
    public BaseUI(PageContext pageContext) {

		if (log== null) {
			log = IsaLocation. getInstance(this.getClass().getName());	
		}
		
        initContext(pageContext);
	}
 
 
 	/**
 	 * Initialize the the object from the page context. <br>
 	 * 
 	 * @param pageContext page context
 	 */
 	public void initContext(PageContext pageContext) {
 		final String METHOD_NAME = "initContext(PageContext)";
 		log.entering(METHOD_NAME);
		this.pageContext = pageContext;
	   
		// get the request
		request = (HttpServletRequest)pageContext.getRequest();

		// create rquest parser	   
		requestParser = new RequestParser(request);
	   
	   	if (pageContext.getSession() != null) {
			// get user session data object
			userSessionData =  UserSessionData.getUserSessionData(pageContext.getSession());
	   	}
       
     	if (userSessionData != null) {
			// get the browser information
			browserType = (DetermineBrowserVersionAction.BrowserVersion)
					  userSessionData.getAttribute(DetermineBrowserVersionAction.SC_BROWSER_VERSION);

			startupParameter = (StartupParameter)userSessionData.getAttribute(SessionConst.STARTUP_PARAMETER);

			isAccessible = SwitchAccessibilityAction.getAccessibilitySwitch(userSessionData);

			if (startupParameter.getParameterValue(Constants.PORTAL).equals("YES")) {
				isPortal = true;
              
			}
			
			uiController = UIController.getController(userSessionData);
       	}
		
		if (browserType == null) {
			browserType = DetermineBrowserVersionAction.UNKNOWN_BROWSER;
		}
       
		isToggleSupported = !browserType.isUnknown() && 
							 (!browserType.isOpera() || browserType.isOpera7up()) 
							 && !browserType.isNav4();
		log.exiting();
 	}
   
    /**
     * Returns the maximum password length, depending on the settings in XCM configuration. 
     * @param pageContext Reference to the JSP page context.
     * @return The maximum password length, depending on the settings in XCM configuration.
     */
	public static int getPasswordLength(PageContext pageContext) {
		int maxLength = 8;
		// do everything in a try catch to avoid errors
		try{
			// To get the needed Information is a complex call.
   
			// 1. Get the usersassion data.
			UserSessionData sessionData = UserSessionData.getUserSessionData(pageContext.getSession());
			
			// 2. Get the current Data
			ConfigContainer currentConfig = (ConfigContainer) sessionData.getAttribute(SessionConst.XCM_CONFIG); 
			
			// 3. Get the parameters from the config.
			ParamsStrategy.Params configParams = (ParamsStrategy.Params) (currentConfig).getConfigParams(); 
			
			// 4. Finally get the config value we need.
			String userType = (configParams).getParamValue("usertype").toUpperCase();
			
			// In CRM is the Maximum password length 40 chars in R/3 it is only 8 chars long.
			// if the usertype begins with CRM we Use the CRM backend, if it begins with R3 we use
			// the R3 Backend. Do not worry if you use UME, you won't even come to this page.
			if(userType.startsWith("CRM")) {
				maxLength = 40;
			} else {
				if(userType.indexOf("SU05") != -1 ) {
					maxLength = 16;
				} else {
					maxLength = 8;
				}
				
			}
		} catch(Exception ex) {
			log.debug(ex.getMessage());
			// This error should not happen, but does not matter, even if happens.
		}
		return maxLength;
	}
   
   /**
    * Returns the language dependent text for a given resource give. <br>
    * 
    * @param key resource key.
    * @return String language dependent text
    */
	protected String translate(String key) {
       return WebUtil.translate(pageContext, key, null, null);    
    }
    

	/**
	 * Return the property {@link #accessibilityMessages}. <br>
	 * 
	 * @return MessageList with all messages which should be display in
	 *          accessibility mode.
	 */
	public MessageList getAccessibilityMessages() {
		return accessibilityMessages;
	}


	/**
	 * Could be overwriten to fill the accessibility messages. <br>
	 * The function is called in the <code>accessibilitymessages.inc.jsp</code>.
	 */
	public void generateAccMessages() {
	}

	/**
	 * Set the property {@link #accessibilityMessages}. <br>
	 * 
	 * @param messageList list with all messages which should eb displayed in 
	 *         accessibility mode.
	 */
	public void setAccessibilityMessages(MessageList messageList) {
		accessibilityMessages = messageList;
	}


	/**
	 * Add messages to the {@link #accessibilityMessages}. <br>
	 * 
	 * @param messageList list with additional messages which should eb displayed in 
	 *         accessibility mode.
	 */
	public void addAccessibilityMessages(MessageList messageList) {
		if (accessibilityMessages!= null) {
			accessibilityMessages.add(messageList);
		}
		else {
			accessibilityMessages = messageList;	
		}
	}


	/**
	 * Add messages to the {@link #accessibilityMessages}. <br>
	 * 
     * The method searches for an instance of the object containing the messages
     * in page, request, session and application scope (exactly in this order).<br>
     * The name of the object searched for is given using the <code>name</code>
     * parameter. 
	 * 
	 * @param name name of object in the page context with a list of messages. 
	 */
	public void addAccessibilityMessages(String name) 
			throws JspException {
		
		MessageList messageList = MessageTag.getMessageListFromContext(pageContext, name);
		
		if (messageList != null) {
			accessibilityMessages.add(messageList);
		}
	}

    /**
     * Returns the browserType. <br>
     * 
     * @return DetermineBrowserVersionAction.BrowserVersion
     */
    public DetermineBrowserVersionAction.BrowserVersion getBrowserType() {
        return browserType;
    }


	/**
	 * Return the language taken from the locale. <br>
	 * 
	 * @return page language
	 */
	public String getLanguage() {

		Locale locale = null;
		
		if (userSessionData != null) {		
			locale = userSessionData.getLocale();
		} 
		else {
			locale = request.getLocale();
		}
				
		return locale.getLanguage();
	}
    
    /**
     * Return the locale <br>
     * 
     * @return <code>Locale</code>, the current locale
     */
    public Locale getLocale() {
       Locale locale = null;
        
       if (userSessionData != null) {      
          locale = userSessionData.getLocale();
       } 
       else {
          locale = request.getLocale();
       }
                
       return locale;
    }


	/**
	 * Return Set the property {@link #isAccessible}. <br>
	 * 
	 * @return {@link #isAccessible}
	 */
	public boolean isAccessible() {
		return isAccessible;
	}


	/**
	 * Return the property {@link #isToggleSupported}. <br>
	 * 
	 * @return {@link #isToggleSupported}
	 */
	public boolean isToggleSupported() {
		return isToggleSupported;
	}

	
	/**
     * Converts null strings to <code>""</code>. <br>
     * Call JspUtil.removeNull.	
     *
     * @param object The object to be converted
     * @return Returns "" if the input parameter is <code>null</code>
     *         or the value returned by the <code>toString()</code> method
     *         of the given object if the reference is not
     *         <code>null</code>
	 */
	public String removeNull(Object value) {
		return JspUtil.removeNull(value);
	}
	


	/**
	 * Return the help values search with the given name or <code>null</code> if
	 * the search is not available. <br>
	 * 
	 * @param searchName
	 * @return help values search with the given name or <code>null</code> if
	 * the search is not available.
	 */
	public HelpValuesSearch getHelpValuesSearch(String searchName) {
		
		HelpValuesSearch helpValuesSearch = null;
		try {
			helpValuesSearch = HelpValuesSearchManager.getHelpValuesSearchFromSession(searchName, userSessionData);
        }
        catch (CommunicationException exception) {
        	log.error(LogUtil.APPS_COMMON_INFRASTRUCTURE, "", exception);
        }	
		
		return helpValuesSearch;
	}

	
	/**
	 * Return the help values for the given search. <br>
	 * 
	 * @param helpValuesSearch
	 * @return
	 */
	public HelpValues getHelpValues(HelpValuesSearch helpValuesSearch) {
		
		return HelpValuesSearchManager.getHelpValues(helpValuesSearch, 
		                                             userSessionData, 
		                                             request);		
	} 
	

	/**
	 * Return the help values for the given search. <br>
	 * 
	 * @param helpValuesSearch
	 * @return
	 */
	public HelpValues getHelpValues(String searchName) {
		
		return HelpValuesSearchManager.getHelpValues(searchName, 
												     userSessionData, 
												     request);
	} 


	/**
	 * Return Set the property {@link #isPortal}. <br>
	 * 
	 * @return {@link #isPortal}
	 */
	public boolean isPortal() {
		return isPortal;
	}


	/**
	 * Returns the logical key for the context value {@link Constants#CV_UITARGET}. <br>
	 * 
	 * @return LOgical key for the area with the given name. 
	 */
	public String getUiAreaKey(String name) {
		
		UILayoutManager layoutManager = UILayoutManager.getManagerFromRequest(request);
        
		UILayout layout = layoutManager.getCurrentUILayout();
		
		if (layout != null) {
			return layout.getUIAreaPosition(name);
		}

		return "";
	}

	/**
	 * Return the context which is passed to an include page. <br> 
	 * Ssee {@link #includePage(String, Object)}. <br>
	 * 
	 * @return context object.
	 */
	public Object getIncludeContext(){
		return request.getAttribute(INCLUDE_CONTEXT);
	}

	/**
	 * Return the context which is passed to an include page. <br> 
	 * Ssee {@link #includePage(String, Object)}. <br>
	 * 
	 * @return context object.
	 */
	static public Object getIncludeContextFromRequest(HttpServletRequest request){
		return request.getAttribute(INCLUDE_CONTEXT);
	}

	/**
	 * Includes the given page to the current JSP. <br>
	 * Because it is an dynamic include the include must build a separate JSP unit.
	 * (not necessarily a complete HTML unit.
	 * 
	 * @param page page to include.
	 * @param contextName name of the context.
	 * @param context context.
	 */
	public void includePage(String page, Object context) {
		
		if (context != null) {
            Object requestContext = (Object)request.getAttribute(INCLUDE_CONTEXT);
            if (requestContext != null) {
                log.debug("Request contains already an include context: " + requestContext.toString());
            }
			request.setAttribute(INCLUDE_CONTEXT,context);
		}

		includePage(page);

		if (context != null) {
			request.removeAttribute(INCLUDE_CONTEXT);
		}	
	}

		  	
	/**
	 * Includes the given page to the current JSP. <br>
	 * Because it is an dynamic include the include must build a separate JSP unit.
	 * (not necessarily a complete HTML unit.
	 * 
	 * @param page page to include.
	 */
	public void includePage(String page) {

		boolean error = false;
		Message message = null; 
		
		if (page != null && page.trim().length() > 0) {
			
			try {
				if (pageContext == null) {
                    log.debug("pageContext is null!");
				}
				pageContext.include(page);
                
			} catch (Exception exception) {
				log.error(LogUtil.APPS_USER_INTERFACE, "", exception);
				error = true;
				request.setAttribute(ContextConst.EXCEPTION,exception);
				String args[]= {page, exception.getMessage()};
				message = new Message(Message.ERROR,"system.ui.includeError",args,null);
			}
		}
		else {
			error = true;			
			message = new Message(Message.ERROR,"system.ui.missingInclude",null,null);
		}

		if (error) {
			MessageListDisplayer messageDisplayer = new MessageListDisplayer();
			messageDisplayer.addToRequest(request);
			messageDisplayer.setOnlyDisplayMessages();
			messageDisplayer.addMessage(message);
			try {
				pageContext.include("/appbase/message.inc.jsp");
			} catch (Exception e) {
				log.error(LogUtil.APPS_USER_INTERFACE, "", e);
			}
		}
	}


	/**
	 * Return the cuurent area context. <br>
	 * <strong>Note:</strong> The function work also without using the layout framework. 
	 * 
	 * @return current available area context  
	 */
	public AreaContext getAreaContext() {
		
		return AreaContext.getContextFromRequest(request);
	}
	
	
	/**
	 * Return the value to be used in input field on the JSP. <br>
	 * The value could be either the result of a help value result or the 
	 * given value. <br>
	 * 
	 * @param parameterName name of the parameter in the HelpValueSearch.
	 * @param value value to be used if no help value result for this parameter 
	 * exits
	 * @return 
	 */
	public String getFieldValue(String parameterName, String value) {
		
		String newValue = HelpValuesSearchUI.getFieldValueFromHelpResult(parameterName, value, pageContext);
		
		return newValue;
	}


/* Dynamic Field Control */	

	/**
	 * Return if the UIElement with the given name is visible. <br>
	 * 
	 * @param fieldName name of the ui field
	 * @return <code>true</code> if the element exist and is visible. 
	 */
	public boolean isElementVisible(String fieldName) {
		
		UIElementBase uiField = uiController.getUIElement(fieldName);
		
		if (uiField != null && !uiField.isHidden()) {
			return true;
		}
		
		return false;
		
	}
	
	/**
	 * Return if the UIElement with the given name and key is visible. <br>
	 * 
	 * @param fieldName name of the ui field
	 * @return <code>true</code> if the element exist and is visible. 
	 */
	public boolean isElementVisible(String fieldName, String key) {
		
		UIElementBase uiField = uiController.getUIElement(fieldName, key);
		
		if (uiField != null && !uiField.isHidden()) {
			return true;
		}
		
		return false;
	}	


	/**
	 * Return if the UIElement with the given name is enabled. <br>
	 * 
	 * @param fieldName name of the ui field
	 * @return <code>true</code> if the element exist and is enabled. 
	 */
	public boolean isElementEnabled(String fieldName) {
		
		UIElement uiField = uiController.getUIElement(fieldName);
		
		if (uiField != null && !uiField.isDisabled()) {
			return true;
		}
		
		return false;
	}
	
	/**
	 * Return if the UIElement with the given name and key is enabled. <br>
	 * 
	 * @param fieldName name of the ui field
	 * @return <code>true</code> if the element exist and is enabled. 
	 */
	public boolean isElementEnabled(String fieldName, String key) {
		
		UIElement uiField = uiController.getUIElement(fieldName, key);
		
		if (uiField != null && !uiField.isDisabled()) {
			return true;
		}
		
		return false;
	}	
	

	/**
	 * Return if the UIElement with the given name is disabled. <br>
	 * If yes the function returns the string <code>disabled</code> else an 
	 * empty string
	 * 
	 * @param fieldName name of the ui field
	 * @return The  string <code>disabled</code> if the element doesn't exist or 
	 * is disabled. 
	 */
	public String getElementDisabled(String fieldName) {

		return isElementEnabled(fieldName)?"":"disabled";
	}
	
	/**
	 * Return if the UIElement with the given name and key is disabled. <br>
	 * If yes the function returns the string <code>disabled</code> else an 
	 * empty string
	 * 
	 * @param fieldName name of the ui field
	 * @return The  string <code>disabled</code> if the element doesn't exist or 
	 * is disabled. 
	 */
	public String getElementDisabled(String fieldName, String key) {

		return isElementEnabled(fieldName, key)?"":"disabled";
	}
	
	/**
     * Return the ui group with the given name. <br>
     * 
     * @param groupName name of the ui group 
     * @return {@link UIElementGroup} or <code>null</code> if the group can't be
     *         found.
     */
    public UIElementGroup getUIGroup(String groupName) {
		return uiController.getUIElementGroup(groupName);
	}
	
	/**
     * Return the number of visible elements in the ui group with the 
     * given name. <br>
     * 
     * @param groupName name of the ui group
     * @return 
     * @see #getUIGroup(String) 
     */
	public int getNumberOfVisibleElements(String groupName) {
		UIElementGroup group = uiController.getUIElementGroup(groupName);
		if (group != null) {
			return group.getNumberOfVisibleElements();
		}
		return 0;
	}
	
	
	/**
	 * Delete all individual elements for all UIElement with the given groupname<br>
	 *  
	 * @param name name of the UI group.
	 */
	public void deleteIndividualUIElementsInGroup(String groupName) {
	   uiController.deleteIndividualUIElementsInGroup(groupName);	
	}

	/**
	 * Return the table title and summary for the accessibility mode. <br>
	 * The method uses the generic resource key <code>access.table.begin</code>
	 * to generate the string.
	 *  
	 * @param titleKey resource key to describe the title 
	 * @param numRow  number of rows in the table
	 * @param numColumn number of columns in the table.
	 * 
	 * @return String with title and inforamtion of row and column number.
	 */
	public String getAccessTableSummary(String titleKey,
								 int numRow, 
								 int numColumn) {
								 	
		return getAccessTableTitle(titleKey,numRow,numColumn,-1,-1,false);								 	
								 	
	}
	

	/**
     * Return the table title and summary for the accessibility mode. <br>
     * The method uses the generic resource key <code>access.table.begin</code>
     * to generate the string.
     *  
     * @param titleKey resource key to describe the title 
     * @param numRow  number of rows in the table
     * @param numColumn number of columns in the table.
     * 
     * @return String with title and inforamtion of row and column number.
     */
    public String getAccessTableTitle(String titleKey,
								 int numRow, 
								 int numColumn,
								 int startRow,
								 int endRow,
								 boolean addSkip) {
								 	
		String ret;	
	
		String colCnt = Integer.toString(numColumn);
		String rowCnt = Integer.toString(numRow);
		String title = translate(titleKey);
	
		if (startRow >= 0) {
			ret = WebUtil.translate(pageContext, 
									"access.table.summary.simple", 
									 new String[] {title, rowCnt, colCnt});
		}
		else {
			String rowStart = Integer.toString(startRow);
			String rowEnd = Integer.toString(endRow);
			ret = WebUtil.translate(pageContext, 
									"access.table.summary", 
									 new String[] {title, rowCnt, colCnt, rowStart, rowEnd});
		}
	
		if (addSkip) {
			ret = ret = translate("access.table.skip.suffix");	
		}
	
		return ret;
	}
	
    /**
     * Returns configuration valid for the current session. This feature
     * is only available if Extended Configuration Management is turned on.
     * If no configuration data is available <code>null</code> is returned
     * @return configuration data
     */
    public InteractionConfigContainer getInteractionConfig() {
        UserSessionData userData =
                 UserSessionData.getUserSessionData(request.getSession());
        if (userData == null) {
            return null;
        }
   
        return (InteractionConfigContainer)(userData.getAttribute(SessionConst.INTERACTION_CONFIG));
     }
}
