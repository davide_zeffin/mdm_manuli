/*****************************************************************************
    Class:        LayoutUI
    Copyright (c) 2004, SAP AG, Germany, All rights reserved.
    Author:       SAP AG
    Created:      22.04.2004
    Version:      1.0

    $Revision: #13 $
    $Date: 2003/05/06 $
*****************************************************************************/

package com.sap.isa.ui.uiclass;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.PageContext;

import com.sap.isa.core.ContextConst;
import com.sap.isa.core.SharedConst;
import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.core.logging.LogUtil;
import com.sap.isa.core.ui.ExtendedStyleManager;
import com.sap.isa.core.ui.layout.GlobalUILayoutHandler;
import com.sap.isa.core.ui.layout.UIArea;
import com.sap.isa.core.ui.layout.UIBaseElement;
import com.sap.isa.core.ui.layout.UIComponent;
import com.sap.isa.core.ui.layout.UILayout;
import com.sap.isa.core.ui.layout.UILayoutManager;
import com.sap.isa.core.util.WebUtil;


/**
 * The class LayoutUI handles a jsp, which renders a layout object. <br>
 *
 * @author  SAP AG
 * @version 1.0
 */
public class LayoutUI extends BaseUI {

    /**
     * Constant to store the layout in the page context. <br>
     */
    final static public String LAYOUT =  "layout.uiclass.ui.isa.sapmarkets.com";

 

	/**
	 * Reference to layout manger. <br>
	 */
    protected UILayoutManager layoutManager;

    /**
     * Reference to the layout object. <br>
     */
    protected UILayout layout;

	final static private IsaLocation log = IsaLocation.
			  getInstance(LayoutUI.class.getName());

    /**
     * Standard constructor. <br>
     *
     */
    public LayoutUI() {
        super();
    }


    /**
     * Standard constructor. <br>
     *
     * @param pageContext
     */
    public LayoutUI(PageContext pageContext) {
        initContext(pageContext);
    }


    /**
     * Initializes the object with the information from the page context. <br>
     *
     * @param pageContext current page context.
     *
     * @see com.sapmarkets.isa.isacore.uiclass.BaseUI#initContext(javax.servlet.jsp.PageContext)
     */
    public void initContext(PageContext pageContext) {
    	
		final String METHOD_NAME = "initContext(PageContext)";
		log.entering(METHOD_NAME);
		
        super.initContext(pageContext);
        
        layoutManager = UILayoutManager.getManagerFromRequest(request);
        
        layout = layoutManager.getCurrentUILayout();
        
        if (layout != null) {
            pageContext.setAttribute(LayoutUI.LAYOUT, layout);
                        
        }
		log.exiting();
    }


    /**
     * Returns the name of the page from the current component in the 
     * area with the given area name. <br>
     * 
     * @param areaName name of an UI area.
     * @return the name of the page for the current component in the found
     *         area.
     */
    public String getAreaPage(String areaName) {

        UIArea area = layout.getUiArea(areaName);

        return area.getComponent().getPage();
    }

    
    /**
     * <p>Generates the sytle sheet include statement using the method {@link ExtendedStyleManager#includesStyles}.</p>
     */
    public void includeStylesheets() {
    	ExtendedStyleManager.includeStyles(pageContext,null,layout.getStyleGroupName());
    }

	/**
	 * Includes all jsIncludes, which are available for the current layout. <br>
	 * Generates also the java script function <code>genericLayoutPageOnLoad</code>,
	 * which includes calls of all jsOnload function calls defined in the componenets.
	 * 
	 */
	public void includeHeaderJavaScript() {

		List functionCalls = new ArrayList();
		String jsInclude = layout.getJsInclude();
	  	if (jsInclude != null && jsInclude.length()> 0) {
			includePage(jsInclude);
	  	}
		Iterator iter = getAreaIterator();
		while (iter.hasNext()) {
		  UIArea area = (UIArea)iter.next();
		  jsInclude = area.getComponent().getJsInclude();
		  if (jsInclude != null && jsInclude.length()> 0) {
			includePage(jsInclude, area);
		  }
		  String jsOnload = area.getComponent().getJsOnload();
		  if (jsOnload != null && jsOnload.length()>0) {
		  	functionCalls.add(jsOnload); 
		  }
		}
		
		JspWriter out = pageContext.getOut();
		try {
			out.println("<script type=\"text/javascript\">");
			out.println("<!--");
            out.println("function genericLayoutPageOnLoad() {");
			String jsOnload = layout.getJsOnload();
			if (jsOnload != null && jsOnload.length()>0) {
				out.println(jsOnload +"();");
			}
            iter = functionCalls.iterator();
            while (iter.hasNext()) {
                out.println((String) iter.next()+"();");
            } 
			out.println("}");
			out.println("-->");
			out.println("</script>");
        }
        catch (IOException e) {
            log.error(LogUtil.APPS_USER_INTERFACE, "", e);
        }
			
	}	

	/**
	 * Includes all external header data, which are available for the current layout. <br>
	 */
	public void includeExternalHeaderData() {

		JspWriter out = pageContext.getOut();
		try {
			Iterator iter = getAreaIterator();
			while (iter.hasNext()) {
			  UIArea area = (UIArea)iter.next();
			  UIComponent uiComponent = area.getComponent();
			  if (uiComponent.isExternal()) {
				  Iterator rowIter = uiComponent.getExternalHeader().iterator();
				  while (rowIter.hasNext()) {
					out.println((String) rowIter.next());
				}
			  }
			}
        }
        catch (IOException e) {
            log.error(LogUtil.APPS_USER_INTERFACE, "", e);
        }
			
	}	


	
	/**
	 * Includes the compoment page of the area with the given area name. <br>
     * 
	 * The method calls the {@link #getAreaPage(String)} method to get the 
	 * pagename and calls then the <code>include</code> method of the 
	 * <code>pageContext</code> object. <br>
     * 
	 * The area object will be stored temporary in the request context using the 
	 * key {@link #INCLUDE_CONTEXT}. <br>
     * 
	 * The method manges also all exception, which occur while the include.
	 * 	 
	 * @param areaName name of am UI area.
	 */
    public void includeArea(String areaName) {

		UIArea area = layout.getUiArea(areaName);
		
		if (!area.isHidden()) {
			log.debug("Include Area: " + areaName);
			UIComponent uiComponent = area.getComponent();
			String pageName = uiComponent.getPage();
			layout.setCurrentArea(area);
			if (uiComponent.isExternal()) {
				includeExternalBodyData(uiComponent);
			}
			else {
                UIArea requestArea = (UIArea)request.getAttribute(ContextConst.UI_CURRENT_AREA);
                if (requestArea != null) {
                    log.debug("Request contains already an UI area: " + requestArea.getName());
                }
                
				request.setAttribute(ContextConst.UI_CURRENT_AREA, area);
				includePage(pageName, area);
				request.removeAttribute(ContextConst.UI_CURRENT_AREA);
			}	
		}
    }

    
	/**
	 * Includes all external header data, which are available for the current layout. <br>
	 */
	public void includeExternalBodyData(UIComponent uiComponent) {

		JspWriter out = pageContext.getOut();
		try {
		  if (uiComponent.isExternal()) {
			  Iterator rowIter = uiComponent.getExternalBody().iterator();
			  while (rowIter.hasNext()) {
				out.println((String) rowIter.next());
			}
		  }
        }
        catch (IOException e) {
            log.error(LogUtil.APPS_USER_INTERFACE, "", e);
        }
	}	



	/**		  
	 * Returns an iterator over the visible layout areas. <br>
	 * 
	 * @return UI area iterator.
	 */
    public Iterator getAreaIterator() {

		List areas = new ArrayList(10);
		
		Iterator iter = layout.iterator();
		
		while (iter.hasNext()) {
            UIArea element = (UIArea) iter.next();
            if (!element.isHidden()) {
				areas.add(element);	
            }
        } 
    	
        return areas.iterator();
    }


	/**
	 * Return if information for a global html form is available. <br>
	 * 
	 * @return
	 */
	public boolean isGlobalHTMLFormAvailable() {
		return layout.getForm() != null;
	}


	/**
	 * Return if information for a global html form is available. <br>
	 * 
	 * @return
	 */
	public UILayout.Form getHTMLForm() {
		return layout.getForm();
	}


	/**
	 * Return if the global html form should support the global dispatcher. <br>
	 * 
	 * @return
	 */
	public boolean isGlobalHTMLFormDispatcherAvailable() {
		return (layout.getForm() != null && layout.getForm().isDispatcherAvailable());
	}

	
	//TODO Implementation
	/**
	 * <p> The method isXHTMLSupported returns if the page should be rendered XHTML conform. </p>
	 * <p> The information is provide from the method {@link UILayout#isXHTMLSupported}</p>
	 * 
	 * @return <code>true</code> if the page should be rendered XHTML conform.
	 */
	public boolean isXHTMLSupported() {
		return layout.isXHTMLSupported();
	}
    
    /**
     * Return if accessibility information is available on layout level. 
     * 
     * @return <code>true</code> if accessibility information should be shown.
     */
    public boolean isLayoutAccessibilityAvailable() {
        return (isAccessible && layout.getHotSpot().length() > 0);
    }
    
    
    /**
     * Return the translated navigation text for the accessibility mode. <br>
     * 
     * @return title text.
     */
    public String getLayoutAccessibilityTitle() {
        if (layout.getNavigationText().length() > 0) {
            return translate(layout.getNavigationText());
        }
        return "";
    }

    /**
     * Return the translated hot spot for the accessibility mode. <br>
     * 
     * @return access key.
     */
    public String getLayoutAccessibilityKey() {
        if (layout.getHotSpot().length() > 0) {
            return translate(layout.getHotSpot());
        }
        return "";
    }


	/**
	 * Return if accessibility information is available on layout level. 
	 * 
	 * @param area current ui area.
	 * @return <code>true</code> if accessibility information should be shown.
	 */
	public boolean isAreaAccessibilityAvailable(UIArea area) {
		return (isAccessible && area.getHotSpot().length() > 0);
	}
    
    
	/**
	 * Return the translated navigation text for the accessibility mode. <br>
	 * 
	 * @param area current ui area.
	 * @return title text.
	 */
	public String getAreaAccessibilityTitle(UIArea area) {
		if (area.getNavigationText().length() > 0) {
			return translate(area.getNavigationText());
		}
		return "";
	}

	/**
	 * Return the translated hot spot for the accessibility mode. <br>
	 * 
	 * @param area current ui area.
	 * @return access key.
	 */
	public String getAreaAccessibilityKey(UIArea area) {
		if (area.getHotSpot().length() > 0) {
			return translate(area.getHotSpot());
		}
		return "";
	}

	/**
	 * Return if accessibility information is available on layout level. 
	 * 
	 * @return <code>true</code> if accessibility information should be shown.
	 */
	public boolean isLayoutBottomAccessibilityAvailable() {
		return (isAccessible && layout.getHotSpotBottom().length() > 0);
	}
    
    
	/**
	 * Return the translated navigation text for the accessibility mode. <br>
	 * 
	 * @return title text.
	 */
	public String getLayoutBottomAccessibilityTitle() {
		if (layout.getNavigationTextBottom().length() > 0) {
			return translate(layout.getNavigationTextBottom());
		}
		return "";
	}

	/**
	 * Return the translated hot spot for the accessibility mode. <br>
	 * 
	 * @return access key.
	 */
	public String getLayoutBottomAccessibilityKey() {
		if (layout.getHotSpot().length() > 0) {
			return translate(layout.getHotSpotBottom());
		}
		return "";
	}


    /**
     * Return the title for the layout page. <br>
     * If the titleArea of the layout is set, the title of the current component 
     * in the "titleArea" is used. If this is empty the layout title is used. <br>
     * 
     * @return title to display on the HTML page.
     */
    public String getTitle() {

		UIBaseElement titleElement = layout;
		                       
		if (layout.getTitleArea().length() > 0) {
			UIArea uiArea =layout.getUiArea(layout.getTitleArea());
			if (uiArea != null && uiArea.getComponent() != null) {
				String title = uiArea.getComponent().getTitle();
				if (title != null && title.length() > 0) {
					titleElement = uiArea.getComponent();
				}
			}
		}							
        
        List arguments = new ArrayList();
        
        checkArgument(arguments, titleElement.getTitleArg0());
		checkArgument(arguments, titleElement.getTitleArg1());
		checkArgument(arguments, titleElement.getTitleArg2());
		checkArgument(arguments, titleElement.getTitleArg3());
		
		String[] args = null;
		if (arguments.size() > 0) {
			args = (String[])arguments.toArray(new String []{"",""});
		}
        
        StringBuffer title = new StringBuffer();
        String globalTitle = GlobalUILayoutHandler.getInstance().getConfiguration().getTitle();
        
        if (globalTitle.length() > 0) {
        	title.append(translate(globalTitle));
        	title.append("&nbsp;");
        }
        
        if (titleElement.getTitle().length() > 0) {
			title.append(WebUtil.translate(pageContext, titleElement.getTitle(),args,null));
        }
		return  title.toString();   
    }


    private void checkArgument(List arguments, String arg) {
    	
        if (arg != null && arg.length()>0 ) {
        	Object argValue = request.getAttribute(arg);
			if (argValue == null) {
				argValue = request.getSession().getAttribute(arg);
			}	
        	if (argValue != null) {
        		arguments.add(argValue.toString());
        	}
        	else {
        		arguments.add("");
        	}
        }
    }

    
    /**
     * Return the name of the css class to be used in body tag. <br>
     * 
     * @return name of the css class.
     */
    public String getBodyClass() {
    	return layout.getCssClassName();
    }


	/**
	 * Return an iterator over html attribues used in the body. <br>
	 * 
	 * @return
	 */    
    public Iterator getBodyAttributes() {
    	return layout.getBodyAttributes().iterator();
    }
    

	/**
	 * Return an iterator over html attribues used in the form. <br>
	 * 
	 * @return
	 */    
	public Iterator getFormAttributes() {
		
		List list = null;
		
		if (layout.getForm() != null) {
			list = layout.getForm().getAttributes();
		}
				
		// create a dummy list if necessary		
		if (list == null) {
			list = new ArrayList();
		}	
				
		return list.iterator();
	}
    
    
	/**
	 * Return a iterator over all css container which starts with the given area. <br>
	 * 
	 * @param areaName name of the area
	 * @return iterator over the css id names of the found container
	 */
	public Iterator getContainerStartsWithArea(String areaName) {
       
	   return layout.getContainerStartsWithArea(areaName);
	}


	/**
	 * Return a iterator over all css container which starts with the given area. <br>
	 * 
	 * @param areaName name of the area
	 * @return iterator over the css id names of the found container
	 */
	public Iterator getContainerEndsWithArea(String areaName) {
		return layout.getContainerEndsWithArea(areaName);
	 }
    
    
	/**
	 * Logs the given exception object. <br>
	 * 
	 * @param exception 
	 */		
    public void handleException(Exception exception) {
        log.error(LogUtil.APPS_USER_INTERFACE, "", exception);
    }

    /**
     * Includes some information about the current UI area. The data will only 
     * be included, if the startup parameter &quot;showmodulename&quot; (or the
     * corresponding XCM parameter) is set to <code>true</code>.
     * 
     * @param areaName name of am UI area.
     */
    public void includeAreaInfo(String areaName) {

        String parameter = (String)userSessionData.getAttribute(SharedConst.SHOW_MODULE_NAME);
        if (parameter == null || !parameter.equalsIgnoreCase("true")) {
            return;
        }
        
        UIArea area = layout.getUiArea(areaName);
        
        StringBuffer areaInfo = new StringBuffer();
        areaInfo.append("<div class=\"uiarea-info\">\n");
        areaInfo.append("UIArea: " + area.getName());
        if(area.getControlledArea() != null && !area.getControlledArea().equals("")) {
            areaInfo.append(" (Controlled Area: " + area.getControlledArea() + ")\n");
        }        
        areaInfo.append(", UIInclude: " + area.getComponent().getPage());
        if(area.getComponent().getAction() != null && !area.getComponent().getAction().equals("")) {
            areaInfo.append(" (Action: " + area.getComponent().getAction() + ")\n");
        }
        areaInfo.append("</div>\n");
        
        JspWriter out = pageContext.getOut();
        try {
            out.println(areaInfo.toString());
        }
        catch (IOException e) {
            log.error(LogUtil.APPS_USER_INTERFACE, "", e);
        }

    }

}
