/*****************************************************************************
    Class:        InvalidSessionUI
    Copyright (c) 2004, SAP AG, Germany, All rights reserved.
    Author:       SAP AG
    Created:      15.04.2004
    Version:      1.0

    $Revision: #13 $
    $Date: 2003/05/06 $ 
*****************************************************************************/

package com.sap.isa.ui.uiclass;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.jsp.PageContext;

import com.sap.isa.core.logging.IsaLocation;

/**
 * The class InvalidSessionUI handles the "invalid session page". <br>
 *
 * @author  SAP AG
 * @version 1.0
 */
public class InvalidSessionUI extends MessageUI {


	// public List parameterList;


	/**
	 * Flag if the relogin cookie could be found in the request. <br>
	 */
	public boolean isCookieFound = false;
	
	final static private IsaLocation log = IsaLocation.
			  getInstance(InvalidSessionUI.class.getName());
    /**
     * Standard constructor. <br>
     */
    public InvalidSessionUI() {
        super();
    }

    /**
     * @param pageContext
     */
    public InvalidSessionUI(PageContext pageContext) {
        initContext(pageContext);
    }


	/**
	 * Initializing from the context. <br>
	 * 
	 * @param pageContext
	 * 
	 * 
	 * @see com.sap.isa.ui.uiclass.BaseUI#initContext(javax.servlet.jsp.PageContext)
	 */
	public void initContext(PageContext pageContext) {
		final String METHOD_NAME = "initContext(PageContext)";
		log.entering(METHOD_NAME);
		
		super.initContext(pageContext);
		
		parameterList = (List)request.getAttribute("parameter");
		if (parameterList == null) {
		  parameterList = new ArrayList();
		}

		Boolean isPortal =  (Boolean)request.getAttribute("isPortal");
		if (isPortal == null) {
		  isPortal = new Boolean(true);
		}
		
		this.isPortal = isPortal.booleanValue();

		Boolean cookieFound =  (Boolean)request.getAttribute("cookieFound");
		if (cookieFound == null) {
		  cookieFound = new Boolean(false);
		}

		isCookieFound = cookieFound.booleanValue();
		log.exiting();
	}


}
