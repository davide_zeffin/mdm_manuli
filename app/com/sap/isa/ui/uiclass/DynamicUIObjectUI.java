/*****************************************************************************
    Class:        DynamicUIObjectUI
    Copyright (c) 2008, SAP AG, Germany, All rights reserved.
    Author:       SAP AG
    Created:      21.02.2008
    Version:      1.0
*****************************************************************************/
package com.sap.isa.ui.uiclass;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.jsp.PageContext;

import com.sap.isa.core.ContextConst;
import com.sap.isa.core.Iterable;
import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.maintenanceobject.backend.boi.UIElementBaseData;
import com.sap.isa.maintenanceobject.backend.boi.UIElementGroupData;
import com.sap.isa.maintenanceobject.businessobject.DynamicUI;
import com.sap.isa.maintenanceobject.businessobject.Property;
import com.sap.isa.maintenanceobject.ui.UITypeRendererData;
import com.sap.isa.maintenanceobject.ui.renderer.UIRendererManager;

// TODO docu
/**
 * <p>The class DynamicUIObjectUI managed the generation of a page with the help of a dynamic UI 
 * object. </p>
 * <p>This is an abstract base class.</p> 
 *
 * @see com.sap.isa.maintenanceobject.businessobject.DynamicUI
 * 
 * @author  SAP AG
 * @version 1.0
 */
public abstract class DynamicUIObjectUI extends BaseUI {

	/**
	 * Constant to store the property groups to iterate. <br>
	 */
	public static final String RA_UIELEMENT_GROUPS = "uiElementGroups";

	/**
	 * Constant to store the message to iterate. <br>
	 */
	public static final String RA_MESSAGES = "messages";

	/**
	 * DynamicUI object which should be rendered. <br>
	 */
	protected DynamicUI object;

	/**
	 * Current used renderer for current UIElement to be rendered. <br>
	 */
	protected UIRendererManager uiRendererManager;

	/**
	 * Flag if the page is rendered editable or ReadOnly. <br>
	 */
	protected String readOnly = "false";

	/**
	 * The name of the HTML form which should be submitted. <br>
	 */
	protected String formName = "";

	/**
	 * Name of the action which displays the help description. <br>
	 */
	protected String helpAction = "";

	/**
	 * active page. <br>
	 */
	protected String activePage = "";
	
	/**
	 * Active pages list. Includes all visible pages.
	 */
	protected List pageList = new ArrayList();
	
	/**
	 * Constant for main JSP-include. <br>
	 */
	public static final String MAIN_JSP_INCLUDE = "mainJspInclude";
	
	/**
	 * Constant for exit before JSP-include. <br>
	 */
	public static final String EXIT_BEFORE_JSP_INCLUDE = "exitBeforeJspInclude";

	/**
	 * Constant for exit after JSP-include. <br>
	 */
	public static final String EXIT_AFTER_JSP_INCLUDE = "exitAfterJspInclude";

	private List uiStates = new ArrayList();
	
	private Iterator currentIterator;
	
//TODO docu	
	public UIElementBaseData currentUIElement;

//	TODO docu	
	public UIElementBaseData parentUIElement;

//TODO docu	
	public UIElementBaseData currentIteratorElement;
	
	/**
	 * Name fo the JavaScript function which handle the onkeypress event. <br>
	 */
	protected String keyPressEventFunction = "";
	
	
	final static private IsaLocation log = IsaLocation.getInstance(DynamicUIObjectUI.class.getName());
	
	static public DynamicUIObjectUI getFromRequest(HttpServletRequest request) {
		DynamicUIObjectUI dynamicUI =(DynamicUIObjectUI)request.getAttribute(ContextConst.UI_INCLUDE_CONTEXT);
		return dynamicUI;
	}
	
	/**
	 * Constructor for MaintenanceObjectUI. <br>
	 * 
	 * @param pageContext
	 */
	public DynamicUIObjectUI(PageContext pageContext) {
		initContext(pageContext);
	}

	/**
	 * Constructor for MaintenanceObjectUI. <br>
	 * 
	 */
	public DynamicUIObjectUI() {
	}

	/**
	 * Overwrites the method initContext. <br>
	 * Initialize the page from the page context.
	 * 
	 * @param pageContext
	 * 
	 * 
	 * @see com.sap.isa.ui.uiclass.BaseUI#initContext(javax.servlet.jsp.PageContext)
	 */
	public void initContext(PageContext pageContext) {
		final String METHOD_NAME = "initContext(PageContext)";
		log.entering(METHOD_NAME);

		super.initContext(pageContext);

		determineReadOnly();

		if (readOnly == null) {
			readOnly = "false";
		}

		
		determineDynamicUI();

		activePage = object.getActivePageName();

		if (activePage.length()== 0 || object.getActivePage() == null || object.getActivePage().isHidden() ) {
			// search the first valid page to use this
			Iterator pageIterator = object.getPageGroup().iterator();
			while (pageIterator.hasNext()) {
				UIElementGroupData pageObject = (UIElementGroupData) pageIterator.next();
				if (pageObject != null && !pageObject.isHidden()) {
					activePage = pageObject.getName();
					object.setActivePageName(activePage);
				}
			}
		}

		if (object.getActivePage() != null) {
			addState(object.getActivePage());
		}	
		
		request.setAttribute(RA_MESSAGES, object);
		
		uiRendererManager = object.getUIRendererManager();

		determineHelpAction();
		determineFormName();
		determinKeyPressEventFunction();
		initPages();
		
		log.exiting();
	}


	/**
	 * <p>Fills the pageList to the currently used list.</p>
	 * <p>Only visible pages should be shown if the edit modus isn't active.</p>
	 */
	protected void initPages() {

		List pages = object.getPageList();

		if (object.isEditable()) {
			pageList = pages;
		}
		else {
			Iterator iter = pages.iterator();
			while (iter.hasNext()) {
				UIElementGroupData uiPage = (UIElementGroupData) iter.next();
				if (!uiPage.isHidden()) {
					pageList.add(uiPage);
				}
			}		
		}
	}

	
	/**
	 * Determine the property {@link #object}. <br>
	 */
	abstract protected void determineDynamicUI();

	/**
	 * Determine the property {@link #readOnly}. <br>
	 * 
	 */
	abstract protected void determineReadOnly();

	/**
	 * Determine the property {@link #formName}. <br>
	 * 
	 */
	abstract protected void determineFormName();

	/**
	 * Determine the property {@link #helpValuesAction}. <br>
	 * 
	 */
	abstract protected void determineHelpAction();
	
	/**
	 * Determine the property {@link #keyPressEventFunction}. <br>
	 */
	abstract protected void determinKeyPressEventFunction();

	/**
	 * Return the property {@link #activeScreenGroup}. <br>
	 * 
	 * @return {@link #activeScreenGroup}
	 */
	public String getActivePage() {
		return activePage;
	}
	
	
	/**
	 * <p>Return the property {@link #pageList}. </p> 
	 *
	 * @return Returns the {@link #pageList}.
	 */
	public List getPageList() {
		return pageList;
	}

	/**
	 * Return the property {@link #formName}. <br>
	 * 
	 * @return
	 */
	public String getFormName() {
		return formName;
	}

	/**
	 * Return the property {@link #helpAction}. <br>
	 * 
	 * @return {@link #helpAction}
	 */
	public String getHelpAction() {
		return helpAction;
	}

	/**
	 * Return the property {@link #object}. <br>
	 * 
	 * @return {@link #object}
	 */
	public DynamicUI getObject() {
		return object;
	}

	/**
	 * Return the property {@link #readOnly}. <br>
	 * 
	 * @return {@link #readOnly}
	 */
	public String getReadOnly() {
		return readOnly;
	}

	/**
	 * <p> The method getPageDescription returns the description for a page.</p>
	 * 
	 * @param page page
	 * @return description of the page-
	 */
	public String getPageDescription(UIElementGroupData page) {
		
		String ret = "";
		int index = pageList.indexOf(page);
		if (index>=0) {
			ret = getPageDescription(index);
		}
		return ret;
	}

	
	/**
	 * <p> The method getPageDescription returns the description for a page.</p>
	 * 
	 * @param pageIndex index of the page
	 * @return description of the page-
	 */
	public String getPageDescription(int pageIndex) {
		
		UIElementGroupData page = (UIElementGroupData)pageList.get(pageIndex);
		String description = "";
		if (page != null) {
			description = page.getDescription();
			if (description == null || description.length() == 0) {
				StringBuffer buffer = new StringBuffer(translate("fw.du.page"));
				buffer.append(' ').append(pageIndex+1);
				description = buffer.toString();
			} 
			else {
				if (page.isRessourceKeyUsed()) {
					description = translate(description);
				}
			}
		}	
		return description;
	}
	

	/**
	 * Generate the accessibilty messages from the DynamicUI object . <br>
	 * 
	 * @see com.sap.isa.ui.uiclass.BaseUI#generateAccMessages()
	 */
	public void generateAccMessages() {
		if (object != null) {
			setAccessibilityMessages(object.getMessageList());
		}
	}

	/**
	 * Sets the FormName.
	 * @param string formName
	 */
	public void setFormName(String formName) {
		this.formName = formName;

	}

	/**
	 * Sets the readOnly flag.
	 * @param string readOnly
	 */
	public void setReadOnly(String readOnly) {
		this.readOnly = readOnly;
	}
	
	/**
	 * <p>Returns <code>true</code>if the the inline edit modus is active.</p> 
	 * <p>See method {@link DynamicUI#isEditable()} for details.</p>
	 */
	public boolean isEditable() {
		return object.isEditable();
	}	
	
	/**
	 * Includes the current JSP-include for the given UI element. <br>
	 * 
	 * @param element
	 * @param position
	 * @return JSP-include 
	 */
	public void includeUIElement(UIElementBaseData element, String position) {
		String jspInclude = null;
//		UIRendererManager uiRendererManager = object.getRendererManager();
		UITypeRendererData renderer = uiRendererManager.getUIRenderer(element.getUiType());
		if (renderer != null) {
			if (position.equals(DynamicUIObjectUI.MAIN_JSP_INCLUDE)) {
				jspInclude = renderer.getJSPIncludeName();
				addState(element);
			}
			
		    if (jspInclude != null && jspInclude.length() > 0) {
		    	includePage(jspInclude, this);
		    	if (position.equals(DynamicUIObjectUI.MAIN_JSP_INCLUDE)) {
		    		removeState();
		    	}
		    }
		}    
	}

//	TODO docu	
	protected void addState(UIElementBaseData element) {
		
		DynamicUIState uiState = new DynamicUIState(element, currentUIElement);
		uiStates.add(uiState);
		parentUIElement = currentUIElement;
		currentUIElement = element;
		currentIterator = uiState.iterator;
	}

//TODO docu	
	protected void removeState() {
		
		uiStates.remove(uiStates.size()-1);
		if (uiStates.size() > 0) {
			DynamicUIState uiState = (DynamicUIState)uiStates.get(uiStates.size()-1);
			currentUIElement = uiState.uiElement;
			currentIterator = uiState.iterator;
			parentUIElement = uiState.parentUiElement;
			currentIteratorElement = uiState.uiIteratorElement; 
		}

	}

	public boolean hasNextElement() {
		if (currentIterator!= null) {
			return currentIterator.hasNext();
		}
		return false;
	}

	
	public void useNextElement() {
		if (currentIterator!= null) {
			DynamicUIState uiState = (DynamicUIState)uiStates.get(uiStates.size()-1);
			currentIteratorElement = (UIElementBaseData)currentIterator.next();
			uiState.uiIteratorElement = currentIteratorElement; 
		}		
	}
	
	
    /**
     * Returns true if the page navigation should be displayed
	 * @return boolean
	 */
	public boolean isPageNavigationSupported() {
    	return ((object.getPageList().size() > 1)? true: false);	
    }

	
	/**
	 * Return the key which is used in the message object for the property attribute . <br>
	 * 
	 * @param property property which should be analysed
	 * @return key for message property attribute.
	 */
	public String getElementMessageKey(Property property) {
		if (object.isPropertyKeyForMessagesUsed()) {
			return property.getTechKey().getIdAsString();
		}
		else {
			return property.getName();
		}
	}

    /**
     * returns the PropertyDescription depending on attribute isRessourceKeyUsed
     * @param property
     * @return String
     */
    public String getPropertyDescription(Property property) {

        if (!property.isRessourceKeyUsed()) {
            return property.getDescription();
        }
        return translate(property.getDescription());
    }
	
    /**
     * Returns the map with invisible UI parts.
     * 
     * @return Map invisibleParts
     */
    public Map getInvisibleParts() {
    	return uiRendererManager.getInvisibleParts();
    }
    
    
	private class DynamicUIState {
		
		protected Iterator iterator;
		
		protected UIElementBaseData uiElement;

		protected UIElementBaseData parentUiElement;

		protected UIElementBaseData uiIteratorElement;

		public DynamicUIState(UIElementBaseData uiElement,  UIElementBaseData parentUiElement) {
			this.uiElement = uiElement;
			this.parentUiElement = parentUiElement;
			if (uiElement instanceof Iterable) {
				iterator = ((Iterable)uiElement).iterator();
			}
		}

		
	}


	/**
	 * 
	 * Return the property {@link #keyPressEventFunction}. <br>
	 * 
	 * @return keyPressEventFunction
	 */
	public String getKeyPressEventFunction() {
		return keyPressEventFunction;
	}

	/**
	 *  Set the property {@link #keyPressEventFunction}. <br>
	 * 
	 * @param keyPressEventFunction The keyPressEventFunction to set.
	 */
	public void setKeyPressEventFunction(String keyPressEventFunction) {
		this.keyPressEventFunction = keyPressEventFunction;
	}
	
}
