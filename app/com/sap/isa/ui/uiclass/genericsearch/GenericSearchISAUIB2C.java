/*****************************************************************************
    Class:        GenericSearchISAUIB2C
    Copyright (c) 2004, SAP AG, Germany, All rights reserved.
*****************************************************************************/

package com.sap.isa.ui.uiclass.genericsearch;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.jsp.PageContext;

import com.sap.isa.backend.boi.isacore.order.DocumentListFilterData;
import com.sap.isa.businessobject.BusinessObjectManager;
import com.sap.isa.businessobject.CommunicationException;
import com.sap.isa.businessobject.GenericSearchInstance;
import com.sap.isa.businessobject.Shop;
import com.sap.isa.businessobject.User;
import com.sap.isa.core.SessionConst;
import com.sap.isa.isacore.DocumentHandler;
import com.sap.isa.user.permission.ECoActionPermission;

/**
 * The class GenericSearchISAUI.java extends class <code>GenericSearchBaseUI</code> and 
 * provides additional methods and variables to for ISales Application. 
 *
 * @author SAP
 * @version 1.0
 **/
public class GenericSearchISAUIB2C extends GenericSearchBaseUI { 
    // Map with excluded document types
    Map exDocTypes = new HashMap();
    //permission flags 
    private boolean hasReadOrderPermission = false;
    private boolean hasReadOrdTmpPermission = false;    
    private boolean hasReadQuotPermission = false;  
    private boolean hasReadContPermission = false;
    private boolean hasReadAuctionPermission = false;
    private boolean hasReadInvoicePermission = false;
    
    
    /**
     * Constructor for GenericSearchUI.
     * @param pageContext
     */ 
    public GenericSearchISAUIB2C() {

    }

    /***
     * Initialize class. Necessary since reflection api doesn't support 
     * instantiation of classes with arguments.
     * @param pageContext
     */
    public void init(PageContext pageContext) {
        super.init(pageContext);

        Shop shop = null;;
        User user = null;
        if (userSessionData != null) {
            // get BOM
            BusinessObjectManager bom = (BusinessObjectManager) userSessionData.getBOM(BusinessObjectManager.ISACORE_BOM);
            // now I can read the shop
            shop = bom.getShop();
            user = bom.getUser();
        }
        if (user != null){
            //check for all create permissions
            String[] documents = new String[]{DocumentListFilterData.SALESDOCUMENT_TYPE_ORDER,
                                              DocumentListFilterData.SALESDOCUMENT_TYPE_ORDERTEMPLATE,
                                              DocumentListFilterData.SALESDOCUMENT_TYPE_QUOTATION,
                                              DocumentListFilterData.DOCUMENT_TYPE_CONTRACT,
                                              DocumentListFilterData.DOCUMENT_TYPE_AUCTION,
                                              DocumentListFilterData.BILLINGDOCUMENT_TYPE_INVOICE};
//              try{    
//                Boolean[] checkResults = checkReadPermissions(user, documents);                                         
//                hasReadOrderPermission = checkResults[0].booleanValue();
//                hasReadOrdTmpPermission = checkResults[1].booleanValue();               
//                hasReadQuotPermission = checkResults[2].booleanValue();
//                hasReadContPermission = checkResults[3].booleanValue();
//                hasReadAuctionPermission = checkResults[4].booleanValue();
//                hasReadInvoicePermission = checkResults[5].booleanValue();        
//              }
//              catch(CommunicationException Ex) {
//                    //TODO report(Ex);
//                }       
        }
        // Set the date and number format 
        dateFormat = shop.getDateFormat().toLowerCase();
        numberFormat = shop.getDecimalPointFormat();
    }

    /**
     * Return the Screen name which describes the search criteria in the XML file
     * @return String screen name
     */
    public String getScreenName() {
        String screenName = request.getParameter(GenericSearchUIData.RC_SEARCHCRITERASCREENNAME);
        if (screenName == null  ||  screenName.length() == 0) {
            screenName = (String)request.getAttribute(GenericSearchUIData.RC_SEARCHCRITERASCREENNAME);
        }
        if (screenName == null  ||  screenName.length() == 0) {
            DocumentHandler documentHandler = (DocumentHandler)userSessionData.getAttribute(SessionConst.DOCUMENT_HANDLER);
            screenName = documentHandler.getOrganizerParameter(GenericSearchUIData.RC_SEARCHCRITERASCREENNAME);
        }
        return screenName;
    }
    /**
     * Check for the user Permission for the Creation of different Documents. <br>
     * @param User for whom the permissions are required 
     * @param String document for which permission is asked.
     * @return Boolean True/False.
     */
    protected Boolean[] checkReadPermissions(User user, String[] docs) throws CommunicationException  {
        ECoActionPermission[] docPermissions = new ECoActionPermission[docs.length];
        for( int cnt = 0; cnt<docs.length; cnt++){
        docPermissions[cnt] = new ECoActionPermission(docs[cnt], DocumentListFilterData.ACTION_READ);       
        }
        return user.hasPermissions(docPermissions);     
    }
    /**
     * 
     * @return Businessobjectmanager
     */
    public BusinessObjectManager getBOM() {
        // get BOM
        return (BusinessObjectManager) userSessionData.getBOM(BusinessObjectManager.ISACORE_BOM);
    }
    
    /**
     * Return the current search instance.
     * @return Returns the current search instance.
     */
    public GenericSearchInstance getSearchInstance() {
    	return searchInstance;
    }
}