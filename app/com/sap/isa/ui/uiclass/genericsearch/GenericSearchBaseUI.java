/*****************************************************************************
    Class:        GenericSearchBaseUI
    Copyright (c) 2003, SAP AG, Germany, All rights reserved.

    $Revision: #26 $
    $DateTime: 2004/08/09 15:59:48 $ (Last changed)
    $Change: 199293 $ (changelist)
    
*****************************************************************************/

package com.sap.isa.ui.uiclass.genericsearch;

import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.PageContext;

import com.sap.isa.businessobject.GenericSearchBuilder;
import com.sap.isa.businessobject.GenericSearchInstance;
import com.sap.isa.core.SessionConst;
import com.sap.isa.core.SharedConst;
import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.core.logging.LogUtil;
import com.sap.isa.core.util.JspUtil;
import com.sap.isa.core.util.WebUtil;
import com.sap.isa.core.util.table.ResultData;
import com.sap.isa.core.util.table.Table;
import com.sap.isa.core.util.table.TableRow;
import com.sap.isa.core.xcm.ConfigContainer;
import com.sap.isa.helpvalues.HelpValuesSearch;
import com.sap.isa.helpvalues.uiclass.HelpValuesSearchUI;
import com.sap.isa.isacore.DocumentHandler;
import com.sap.isa.isacore.GenericSearchComparator;
import com.sap.isa.isacore.action.GenericSearchBaseAction;
import com.sap.isa.maintenanceobject.backend.GenericSearchBuilderHelper;
import com.sap.isa.maintenanceobject.backend.MaintenanceObjectHelperException;
import com.sap.isa.maintenanceobject.businessobject.GSAllowedValue;
import com.sap.isa.maintenanceobject.businessobject.GSProperty;
import com.sap.isa.maintenanceobject.businessobject.GSPropertyGroup;
import com.sap.isa.maintenanceobject.businessobject.VisibilityCondition;
import com.sap.isa.ui.uiclass.BaseUI;

/**
 * The class GenericSearchUI.java provides methods and variables to avoid 
 * java programming on the <code>appbase/genericsearch.jap</code>.
 * It will be instantiated and initialized <b>per request</b>.<br>
 * Terminologie used in this class is UI orientated (Mapping the general names
 * like Property and Property Groups to Screen Element and Screen Elements.
 *
 * @author SAP
 * @version 1.0
 **/
public class GenericSearchBaseUI extends BaseUI 
             implements GenericSearchUIData {

    /**
     * reference to the IsaLocation
     * @see com.sap.isa.core.logging.IsaLocation
     */
    static final protected IsaLocation log = IsaLocation.
          getInstance(GenericSearchBaseUI.class.getName());

    // Screen Elements (Property-Groups in the xml-file)
    protected GSPropertyGroup scrElms = null;
    // Followup Screen elements
    protected List flwElms = new ArrayList();
    // Index - Screen Element list
    protected Map idxScrElm = new HashMap();
    // Properties describing the result list (Property-Groups in the xml-file)
    protected GSPropertyGroup resultListDescription = null;
    // Counted documents
    protected int countedDocs = -1;
    // Reference to the GenericSearchBuilder object
    protected GenericSearchBuilder genSrBuilder = null;
    // Date format
    protected String dateFormat = "yyyy.mm.dd";
    // Number format
    protected String numberFormat = "#.###.###,##";
    // Language
    protected String language = "en";
    // Document Handler
    protected DocumentHandler documentHandler;
    // Flag for layout control
    protected boolean selectOptionEnabled = false;
    protected boolean fullScreenEnabled = true;
    protected boolean resultListEnabled = true;
    // Tabindex for HTML elements
    protected int tabIndex = 0;
    
    protected GenericSearchInstance searchInstance;
    
    protected boolean useInstance = true;
    
    /**
     * Constructor for GenericSearchUI.
     * @param pageContext
     */ 
    public GenericSearchBaseUI() {

    }

    /***
     * Initialize class. Necessary since reflection api doesn't support 
     * instantiation of classes with arguments.
     * @param pageContext
     */
    public void init(PageContext pageContext) {
    	final String METHOD_NAME = "init(PageContext)";
    	log.entering(METHOD_NAME);
        super.initContext(pageContext);

    	searchInstance = (GenericSearchInstance)getAreaContext().getAttribute(GenericSearchBaseAction.SC_GENERICSEARCH_INSTANCE);
    	if (searchInstance == null) {
    		log.debug("search Instance is not available");
    		useInstance = false;
//    		 TODO remove this coding after consolidation
    		searchInstance = new GenericSearchInstance();
    	}
        
        String screenName = getScreenName();
        if (screenName == null  ||  screenName.length() == 0) {
            log.error(LogUtil.APPS_USER_INTERFACE, "No name of a search description !");
        }
        
        JspWriter writer = pageContext.getOut();
                
        // Initialize Helper Object
        GenericSearchBuilderHelper objectHelper = new GenericSearchBuilderHelper();
        ConfigContainer container = userSessionData.getCurrentConfigContainer();
        try {
            // Object is cached, so it doesn't hurt to "create" it each time  
            genSrBuilder = (GenericSearchBuilder)objectHelper.createObjectFromXCM(GenericSearchBuilder.class.getName(),container, "generic-search-config");
  
            // Get screen elements (Properties) SEARCH SCREEN
            scrElms = genSrBuilder.getSearchCriteriaDescription(screenName);

        } catch (MaintenanceObjectHelperException e) {
            log.error(LogUtil.APPS_USER_INTERFACE, "", e);
        }
        // Create the Search Request Memory if it is allowed and not already exist
        // (see also com.sap.isa.isacore.action.GenericSearchBaseAction.getSearchRequestMemory())
        if (scrElms != null) {
            if (scrElms.isUseSearchRequestMemory() 
             && userSessionData.getAttribute(GenericSearchUIData.SC_SEARCHREQUEST_MEMORY + screenName) == null) {
                    Map srRqMm = new HashMap();
                    userSessionData.setAttribute((GenericSearchUIData.SC_SEARCHREQUEST_MEMORY + screenName), srRqMm);
            }
        }
        // Screen layout
        evaluateScreenLayout();
        log.exiting();
    }

    /**
     * Store Screen elements in request context to be used by Iterate tag 
     */
    public void setScreenElements() {
        request.setAttribute(RC_SCREENELEMENTS, scrElms.iterator());        
    }
    /**
     * This method checks if either in the search request memory or the request the following 
     * parameters are set.<br>
     * - @see GenericSearchUIData.RC_UI_NOSELECTOPTIONS<br>
     * - @see GenericSearchUIData.RC_UI_NORESULTLIST<br>
     * - @see GenericSearchUIData.RC_UI_FULLSCREENMODE
     *
     */
    protected void evaluateScreenLayout() {
    	
        String fullScreenEnabled = searchInstance.getFullScreenMode();
        String selectOptionEnabled = searchInstance.getNoSelectOptionsAvailable();

    	// TODO only use search instance
        if (!useInstance) {
        	fullScreenEnabled = request.getParameter(GenericSearchUIData.RC_UI_NOFULLSCREENMODE);
        	selectOptionEnabled = request.getParameter(GenericSearchUIData.RC_UI_NOSELECTOPTIONS);
        }	
        
        this.fullScreenEnabled = ("true".equals(fullScreenEnabled) ? false : true);
        this.selectOptionEnabled = ("true".equals(selectOptionEnabled) ? false : true);
        this.resultListEnabled = (scrElms.getLinkTargetFrameName() != null && scrElms.getLinkTargetFrameName().length() > 0
                                  ? false : true);
        if (this.selectOptionEnabled == false) {
            // If there are no select options we must have a result list. Otherwise the screen would be empty!!
            this.resultListEnabled = true;                                 
        }
        
    }
    
    /**
     * Return the Screen name which describes the search criteria in the XML file
     * @return String screen name
     */
    public String getScreenName() {
        
    	String searchToDisplay = searchInstance.getScreenName();
    	
    	// TODO only use search instance
        if (!useInstance) {
        	searchToDisplay = request.getParameter(GenericSearchUIData.RC_SEARCHCRITERASCREENNAME);
        }	
        
        if (searchToDisplay == null  ||  searchToDisplay.length() <= 0) {
            searchToDisplay = (String)request.getAttribute(GenericSearchUIData.RC_SEARCHCRITERASCREENNAME);
        }
        if (searchToDisplay == null  ||  searchToDisplay.length() <= 0) {
            DocumentHandler documentHandler = (DocumentHandler)userSessionData.getAttribute(SessionConst.DOCUMENT_HANDLER);
            if (documentHandler != null) {
                searchToDisplay = documentHandler.getOrganizerParameter(GenericSearchUIData.RC_SEARCHCRITERASCREENNAME);
            }
        }
        return searchToDisplay;
    }
    
   /**
    * Return the result list description object as defined in the XML file
    * @return GSPropertyGroup result list description
    */
    public GSPropertyGroup getResultListDescription() {
        if (resultListDescription != null) {
            return resultListDescription;
        }
        if (useInstance) {
        	resultListDescription = searchInstance.getSearchGroup(); 
        }
        
        //TODO only use search instance.
        if (resultListDescription == null) {
        	resultListDescription = (GSPropertyGroup)request.getAttribute(GenericSearchUIData.RC_RESULTLIST_PTYGRP + getScreenName());
        }	
        if (resultListDescription == null) {
            // Check in Session
            resultListDescription = 
               (GSPropertyGroup)userSessionData.getAttribute(
                   (GenericSearchUIData.RC_RESULTLIST_PTYGRP + getScreenName()) );
        }
        return resultListDescription;
    }
    
    /**
     * Return the module's name (jsp name) including the used screen names
     * @return String module name
     */
    public String getModuleName() {
        String retVal = "";
        GSPropertyGroup resultListDescription = getResultListDescription();
        String resultListName = (resultListDescription != null ? (" / " + resultListDescription.getName()) : "");
        if ("true".equalsIgnoreCase((String)userSessionData.getAttribute(SharedConst.SHOW_MODULE_NAME))) {
            retVal = ("<br>appbase/genericsearch.jsp (" + getScreenName() + resultListName + ")");
        }
        return retVal;
    }
    /** 
     * Get the sort info object.
     * @param String screen name 
     * @return Map representing the Sort info object
     */
    public Map getSortInfoObject(String screenName) {
        return ((Map)userSessionData.getAttribute(GenericSearchUIData.RC_SORTINFO + screenName));
    }
    /**
     * Store Options in request context to be used by Iterate tag.
     * @param ScreenElement for which options should be set 
     */
    public boolean setOptions(GSProperty scrElm) {
        Iterator itAV = scrElm.iterator();
        Table options = GenericSearchUIFactory.getUIReturnTable(GenericSearchUIFactory.TYPE_RETURNTABLE_SELECTBOXOPTION);

        while (itAV.hasNext()) {
            GSAllowedValue av = (GSAllowedValue)itAV.next();
            if ( ! "dynamic".equals(av.getContent())) {
                // Content is defined in XML 
                TableRow optRow = options.insertRow();
                optRow.setValue("VALUE", av.getValue()); 
                String descript = (translate(av.getDescription()) != null ?
                                    translate(av.getDescription()) : "");         
                optRow.setValue("DESCRIPTION", descript);
                optRow.setValue("SELECTED", av.isDefault());
                optRow.setValue("HIDDEN", av.isHidden());
            } else {
                // Dynamic Content via method call
                log.debug("Dynamic Content for Screenelement " + scrElm.getName());
                options = (Table)performDynamicUIMethod(av.getContentCreateClass(),
                                             av.getContentCreateMethod());
            }
        }

        if (options == null) {
            // No options could be created => return false
            return false;
        }
        request.setAttribute(RC_OPTIONS, new ResultData(options));
        return (options.getNumRows() > 0);
    }
    /**
     * Write tool tip for the date input field.
     * @return String containing the text
     */
    public String writeDateFormatToolTip() {
        String retVal = "";
        String dateFormat = translate(("gs.search.dtfr." + getDateFormat()));
        retVal = "title=\"" + WebUtil.translate(pageContext, "gs.search.dtfr.tip", new String[] {dateFormat}) + "\"";
        return retVal;
    }
    
    /**
     * Use this method to perform a method which is defined in the XML file.
     * @param String name of the Class
     * @param String name of the method to perform
     * @return Object  containing the result of the method
     */
    protected Object performDynamicUIMethod(String className, String methodName) {
        Object newObj = null;
        Object retTab = null;
        StringBuffer creationStep = new StringBuffer();
        try { 
            newObj = (Class.forName(className)).newInstance(); 
        } catch (Exception e) {
            creationStep.append("+Instanciation");
        }
        if (newObj == null) {
            log.debug("Object could not be created" + creationStep);
            return retTab;
        }
        Class cl = newObj.getClass();
        // Find init Method and excecute
        try { 
            Method meth = cl.getMethod("init", new Class[] {javax.servlet.jsp.PageContext.class} );
            meth.invoke(newObj, new Object[] {pageContext}); 
        } catch (Exception e) {
            creationStep.append("+init");
        }
        // Find given Method and excecute (WITHOUT arguments !!)
        try { 
            Method meth = cl.getMethod(methodName, new Class[] {} )  ;
            retTab = (Object)meth.invoke(newObj, new Object[] {});
        } catch (Exception e) {
            creationStep.append("+execution " + methodName);
            log.debug("Method could not be executed: " + creationStep);
            log.debug(e);
            log.debug("Cause: " + e.getCause());
        }
        return retTab;
    }
    /**
     * Store all follow-up Elements for this Entry (Option) in the request
     * context to be used by Iterate tag 
     * @param  Property name for which follow-up Elements should be found
     * @param  Proertty value for which follow-up Elements should be found
     */
    public void setFollowupElements(String ptyName, String ptyValue) {
        flwElms.clear();
        // Get current Screen Elements
        Iterator itPTY = scrElms.iterator();
        // Loop over all Properties
        while (itPTY.hasNext()) {
            GSProperty pty = (GSProperty)itPTY.next();
            List visCndList = pty.getvisibilityConditions();
            for (int vi = 0; vi < visCndList.size(); vi++) {
                VisibilityCondition visCnd = (VisibilityCondition)visCndList.get(vi);
                if (ptyName.equals(visCnd.getPropertyName())  &&  ptyValue.equals(visCnd.getValue())) {
                    // HIT; this property is depending from the requested one! => add to hit-list
                    flwElms.add(pty);
                }
            }
        }
        request.setAttribute(RC_FOLLOWUPELEMENTS, flwElms.iterator());
    }

    /**
     * Set index to Screen Element. 
     * See also @link getScreenElementIndex
     * @param Screen element
     * @param Index to be assigned 
     */
    public void setIndexToScreenElement(GSProperty pty, int idx) {
        if (idxScrElm.get(Integer.toString(pty.hashCode())) == null) {
            // Only add if not yet included
            idxScrElm.put(Integer.toString(pty.hashCode()), (Integer.toString(idx)) );         
        }
    }
    /**
     * Get Index for a screen element
     * @param Screen Element for which index should be returned
     * @return Index of that Screen Element
     */
    public int getIndexOfScreenElement(GSProperty pty) {
        String idxS = (String)idxScrElm.get(Integer.toString(pty.hashCode()));
        if (idxS == null) 
        	log.error(LogUtil.APPS_USER_INTERFACE, "Index for '" + pty.getName() +"' not found!");
        return Integer.parseInt(idxS);
    }
    /**
     * Return true if the Property has dependencies (VisibilityConditions).
     * @return true if dependencies exists
     */
    public boolean hasDependencies(GSProperty pty) {
        if (pty.getvisibilityConditions() == null  ||  pty.getvisibilityConditions().size() < 1 ) {
            return false;
        } 
        return true;
    }
    
    // Returns the counted documents
    protected ResultData getCountedDocuments() {

    	ResultData tab = searchInstance.getCountedDocuments(); 
        
        //TODO only use search instance.
        if (tab == null) {
        	tab = ((ResultData)request.getAttribute(
           (GenericSearchBaseAction.SC_GENERICSEARCH_RET_COUNTED_DOCS + getScreenName())));
        }
        if (tab == null) {
            // Check in Session
            tab = ((ResultData)userSessionData.getAttribute(
                       (GenericSearchBaseAction.SC_GENERICSEARCH_RET_COUNTED_DOCS + getScreenName())));
        }

        return tab;
    }
    
    /**
     * Return additional info if the requested event occured. Returns null if event is not
     * occured or info <code>Blank</code>.
     * @param int   Handle 
     * @param GenericSearchEvent event to be checked
     */
    public String getEventInfo(int handle, GenericSearchEvent ev) {
    	
    	ResultData tab = getCountedDocuments(); 

        int compHandle = 0;
        tab.beforeFirst();
        while (tab.next()) {
            compHandle = tab.getInt("HANDLE");
            if (compHandle == handle) {
                if (ev.getEvent().equals((String)tab.getString("EVENT"))) {
                    if (! "".equals(tab.getString("INFO"))) {
                        return tab.getString("INFO");
                    }
                }
            }
        }
        return null;
    }
    
    
    /**
     * Return <code>true</code> if requested event occured
     * @param int   Handle 
     * @param GenericSearchEvent event to be checked
     */
    public boolean isEventOccured(int handle, GenericSearchEvent ev) {

    	ResultData tab = getCountedDocuments(); 

        int compHandle = 0;
        tab.beforeFirst();
        while (tab.next()) {
            compHandle = tab.getInt("HANDLE");
            if (compHandle == handle) {
                if (ev.getEvent().equals((String)tab.getString("EVENT"))) {
                    return true;
                }
            }
        }
        return false;
    }
    
    /**
     * Return the amount of found documents for a given <code>HANDLE</code>.
     * @param  int   Handle
     * @return int   Number of found documents
     */
    public int getNumberOfReturnedDocuments(int handle) {

    	if (countedDocs > -1) {
            return countedDocs;        // Already counted 
        }

    	ResultData tab = getCountedDocuments(); 

        if (tab == null) {
            return 0;
        }

        int compHandle = 0;
        tab.beforeFirst();
        while (tab.next()) {
            compHandle = tab.getInt("HANDLE");
            if (compHandle == handle) {
                if (tab.getInt("NUM_DOC_SEL") > 0) {
                    countedDocs = tab.getInt("NUM_DOC_SEL");
                }
            }
        }
        
        return countedDocs;
    }
    /**
     * Return the memory object containing the last search request. This can be used
     * to re-build the search screen the way it has been send before. 
     * @return Map Containing the last search request as key - value pairs
     */
    public Map getSearchRequestMemory() {
        return ( (Map)userSessionData.getAttribute((SC_SEARCHREQUEST_MEMORY + this.getScreenName())) );
    }
    /**
     * Store List element "Header" in request context to be used by Iterate tag 
     */
    public void setResultListHeader() {
        request.setAttribute(RC_RESULTLIST_HEADER, getResultListDescription().iterator());
    }
    /**
     * Returns the number of visible columns; columns which are not marked with 'type="hidden"'
     * @return Number of visible Columns 
     */
    public int getNumberOfVisibleColumns() {
        Iterator itRLD = getResultListDescription().iterator();
        int visColumns = 0;
        while(itRLD.hasNext()) {
            GSProperty pty = (GSProperty)itRLD.next();
            if ( ! "hidden".equals(pty.getType())) {
                visColumns++;
            }
        }
        return visColumns;
    }
    /**
     * Store List element "Fields" in request context to be used by Iterate tag 
     */
    public void setResultListFields() {
        request.setAttribute(RC_RESULTLIST_FIELDS, getResultListDescription().iterator());
    }
    
    /**
     * Store List element "Rows" in request context to be used by Iterate tag 
     */
    public void setResultListRows() {

    	Object documents = searchInstance.getDocuments();
    	
    	scrElms = genSrBuilder.getSearchCriteriaDescription(this.getScreenName());
    	if (documents == null) {
	        if (scrElms.isUseSearchRequestMemory()) {
	            documents = userSessionData.getAttribute(
	                          (GenericSearchBaseAction.SC_GENERICSEARCH_RET_DOCUMENTS + this.getScreenName()));
	        } else {
	        	documents = request.getAttribute(
	                          (GenericSearchBaseAction.SC_GENERICSEARCH_RET_DOCUMENTS + this.getScreenName()));
	        }
    	}     
    	
    	request.setAttribute(RC_RESULTLIST_ROWS,documents);
    	
    }
    /**
     * Return the date format for UI.<br>
     * <b>Make sure application set the date format correctly</>. By default the date format
     * is set to <b>yyyy.mm.dd</b>
     */
    public String getDateFormat() {
        return dateFormat;
    }
    /**
     * Return the date format for Calendar control. The control needs to have the month indicator formatted
     * in capital letters otherwise it would be interpreted as minutes!<br>
     * i.e. dd.MM.yyyy
     * @return String containing the date foramt
     */
    public String getDateFormatForCalendarControl() {
        String retVal = dateFormat.replaceAll("mm", "MM");
        return retVal;
    }
    /**
     * Return the number format for UI.<br>
     * <b>Make sure application set the number format correctly</>. By default the number format
     * is set to <b>#.###.###,##</b>
     */
    public String getNumberFormat() {
        return numberFormat;
    }
    /**
     * Return the language of the UI.<br>
     * <b>Make sure application set the language correctly</>. By default the language
     * is set to <b>en</b>
     */
    public String getLanguage() {
        return language;
    }
    /**
     * Returns the resultlist Value, translated or not, depending on the existens of a
     * translation key prefix.
     * @param String Prefix for the translation key
     * @param String Value to translate
     * @return String tranlated value (if requested else plain value)
     */
    protected String translateResultlistValue(String translationKeyPrefix, String value) {
        String fullTranlationKey = "";
        String retValue = value;
        if (translationKeyPrefix != null  && translationKeyPrefix.length() > 0) {
            if (value != null) {
                fullTranlationKey = translationKeyPrefix + value;
            }
            retValue = translate(fullTranlationKey);
        }
        return retValue;
    }
    /**
     * Write out the translated value of the field. If makes used of method @see translateResultlistValue
     * for the translation.  
     * @param GSProperty field which should be written
     * @param String value of the field
     * @return String containing the foramtted and tranlated value
     */
    public String writeResultlistValue(GSProperty field, String value) {
        StringBuffer retVal = new StringBuffer();
        Map sortInfo = getSortInfoObject(getScreenName());
        if (sortInfo.get(field.getName()) != null  &&  getSearchRequestMemory() != null) {
            retVal.append("<b>");
            retVal.append(translateResultlistValue(field.getTranslationPrefix(), value));
            retVal.append("</b>");            
        } else {
        	if (value != null && !value.equals("")) {
				retVal.append(translateResultlistValue(field.getTranslationPrefix(), value));
        	}
        }
        return retVal.toString();
    }
	/**
	 * Write out the defined input field (&lt;input&gt; tag with type 'text' or 'checkbox').
	 * @param GSProperty field which should be written
	 * @param String value of the field
	 * @param int current number of line
	 * @param boolean readonly true or false
	 * @return String containing the input field
	 */
	public String writeResultlistInputField(GSProperty field, String value, int lineNo, boolean readOnly) {
		StringBuffer retVal = new StringBuffer();
		// If the length of the value exceed the defined max length, the length of the value will be max length
		String maxLen = (value.length() > field.getMaxLength() 
		                  ? String.valueOf(value.length()) : String.valueOf(field.getMaxLength()));

		retVal.append("<input type=\""+ field.getType() + "\" size=\"" + field.getSize() + "\"");
		retVal.append(" maxlength=\"" + maxLen + "\"");
		retVal.append(" name=\"" + field.getRequestParameterName() + "["+lineNo+"]" + "\"" );
		if ( ("text".equals(field.getType())   || field.getType() == null ||           // null is handled as text
		      "number".equals(field.getType()) ||
		      "date".equals(field.getType()) ) &&
		     value != null  &&  value.length() > 0) {
			retVal.append(" value=\"" + value + "\"");
		}
		if (readOnly) {
			retVal.append(" disabled ");
		}
		if ("checkbox".equals(field.getType())  &&  ("X".equals(value) || "checked".equals(value))) {
			retVal.append(" checked=\"checked\"");
		}
		if (field.getLabel() != null  &&  field.getLabel().length() > 0) {
			retVal.append(" title=\"" + translate(field.getLabel()) + "\"");
		}
        if (field.getUIJScriptToCall() != null  &&  field.getUIJScriptToCall().length() > 0) {
        	retVal.append(" " + field.getUIJScriptToCall());
        }
		retVal.append(" />");
		return retVal.toString();
	}
    /**
     * Returns the resultlist Value. It could be manipulated by the definded output handler
     * or just the pure value.
     * @param ResultData Row in the resultlist
     * @param GSProperty field in the row
     * @return ResultData containing field value
     */
    public ResultData fieldOutputHandler(ResultData row, GSProperty pty) {
        Table fieldT;
        ResultData retObj = null;
        if (pty.getFieldOutputHandlerClass() != null  &&  pty.getFieldOutputHandlerMethod() != null) {
            // Call dynamic method
            fieldT = (Table)performDynamicUIMethod(pty.getFieldOutputHandlerClass(), pty.getFieldOutputHandlerMethod());
        } else {
            // Just return the attribute value
             fieldT = GenericSearchUIFactory.getUIReturnTable(GenericSearchUIFactory.TYPE_RETURNTABLE_OUTPUTFIELD);
            TableRow tabrow = fieldT.insertRow();
            tabrow.setValue("TYPE", "FIELD");
            tabrow.setValue("VALUE", row.getString(pty.getName()));
            tabrow.setValue("ICONPATH",  "");
            tabrow.setValue("ICONWIDTH", "");
            tabrow.setValue("ICONHIGHT", "");
        }
        if (fieldT != null) {
            retObj = new ResultData(fieldT);
        } 
        return retObj;        
    }
   /**
    * Returns the link parameter which should be added to the URL stored in XML property attribute
    * <code>hyperlink</code>.
    * @param ResultData Row in the resultlist
    * @param GSProperty field in the row
    * @return String containing parameters to add
    */
    public String getLinkParameter(ResultData row, GSProperty pty) {
        String retValue = "";
        if (pty.getLinkParamClass() != null  &&  pty.getLinkParamMethod() != null) {
            String parm = (String)performDynamicUIMethod(pty.getLinkParamClass(), pty.getLinkParamMethod());
            retValue = (parm != null ? parm.replaceAll("&", "&amp;") : "");
        }
        
        return retValue;
    }
    /**
     * Returns the name of JScript files which should be included.
     * @return ResultData containing file names
     */
    public ResultData getJScriptFilesTab() {
        Table fieldT = null;
        ResultData retObj = null;
        
        fieldT = GenericSearchUIFactory.getUIReturnTable(GenericSearchUIFactory.TYPE_RETURNTABLE_JSCRIPTFILES);
        if (fieldT != null) {
            retObj = new ResultData(fieldT);
        } 
        return retObj;        
    }
    /**
     * Returns the JScript functions which should be called in case of the 'onchange' Event.<br>
     * <strong>i.e.</strong> func_01(); func_02(this);
     * @param GSProperty representing the screen element (box, text, ...)
     * @param String Basic JScript function to be called
     * @return String final string 
     */
    public String getJScriptOnChangeFinal(GSProperty pty, String adJS) {
        String retVal = "";
        String objEvt = "onchange=\"";
        if (pty != null &&  pty.getType().startsWith("radio")) {
            // OnChange event doesn't work very well for radio buttons, use OnClick event.
            objEvt = "onclick=\"";
        }
        if (adJS != null) {
            // Should be any other JScript function called?
            retVal = objEvt + adJS + ";\"";
        }
        if (pty.getUIJScriptOnChange() == null) {
            // Nothing to do
            return retVal;
        }
        if (adJS == null) {
            // Just return property value
            retVal = objEvt + pty.getUIJScriptOnChange() + ";\"";
            return retVal;
        }
        // Concatenate standard JScript and property attribute
        retVal = objEvt 
               + pty.getUIJScriptOnChange() 
               + adJS + ";\"";
        
        return retVal;
    }
    /**
     * Return true if screen area definded in xml file belongs to the one just processed
     * in the JSP
     * @param String screen area just processed in JSP
     * @param GSProperty screen element
     * @return boolean true if equal
     */
    public boolean isBelongingToScreenArea(String screenArea, GSProperty pty) {
        String defScreenArea = ("filter-" + pty.getScreenArea());
        return (screenArea.equals(defScreenArea));
    }
    /**
     * Return the maximal number of screen areas which should be available. The screen area could be
     * used to place the search criterias in. Those areas could be controlled by the stylesheet where 
     * the name of the several &lt;div&gt; classes are <b>filter-&lt;Number&gt;</b> 
     */
    public int getMaxScreenAreas() {
        return scrElms.getMaxScreenAreas();
    }
    /**
     * Return the lable for the screen element
     * @return String label
     */
    protected String getLabelText(String labelKey) {
        String retVal = "";
        if (labelKey != null  &&  labelKey.length() > 0) {
            retVal = translate(labelKey);
        }
        return retVal;
    }
    /**
     * Return the CSS class name for the &lt;body&gt; tag.
     * @see com.sap.isa.maintenanceobject.businessobject.GSPropertyGroup.cssBodyClassName
     * @return String css class name
     */
    public String getCssBodyTagClassName() {
        return scrElms.getCssBodyClassName();
    }
    /**
     * Return the ID for the main &lt;div&gt; container to allow adding css formatting
     * information to it. The ID is build of a base value <b>gensearch-col-</b> add by
     * the number returned from getMaxScreenAreas().
     * @see getMaxScreenAreas()
     * @return String div id
     */
    public String getCssMainDivID() {
        return ("gensearch-col-" + String.valueOf(scrElms.getMaxScreenAreas()));
    }
    /**
     * Checks whether or not an &lt;option&gt; line from a select box will be marked as 
     * <b>selected</b> or not (same for radio buttons). If true the option will be marked as such.
     * @param Map search request memory
     * @param GSProperty screen element
     * @param ResultData select box options
     * @return String
     */
    public String writeObjSelectedInfo(Map srm, GSProperty screlm, ResultData option) {
        String retVal = "";
        String reqObjValue = null;
        String defaultSetting = (screlm != null && screlm.getType().startsWith("radio") ? "checked" : "selected=\"selected\"");
        if (screlm.getRequestParameterName() != null) {
            reqObjValue = (request.getParameter(screlm.getRequestParameterName()) != null 
                             ? (String)request.getParameter(screlm.getRequestParameterName()) : null);
        }
        if (reqObjValue != null) {
            retVal = ( reqObjValue.equals(option.getString("VALUE")) ? defaultSetting : ""  );
        } else {
            if (srm == null  || srm.get(screlm.getRequestParameterName()) == null) {
                // Mark default value from XML as selected
                retVal = (option.getBoolean("SELECTED") ? defaultSetting : "");
            } else {
                // Mark value from the last request as selected
                String srmValue = ( srm.get(screlm.getRequestParameterName()) != null ? ((String)srm.get(screlm.getRequestParameterName())) : ""); 
                retVal = ( srmValue.equals(option.getString("VALUE")) ? defaultSetting : ""  );
            }
        }       
        return retVal;
    }
    /**
     * Write the value of the input field depending on the search request memory. If nothing is found
     * in the memory, the default value will be determined (definded in XML file or via dynamic methods)
     * @param Map search request memory
     * @param GSProperty screen element
     * @param String request parameter extension (e.g. _low, _high)
     * @return String
     */
    public String writeInputValueInfo(Map srm, GSProperty scrElm, String reqExt) {
        String retVal = "";
        Table options = null;
        if (srm != null) {
             String reqParamName = scrElm.getRequestParameterName();
             if (reqExt != null) {
                 reqParamName = reqParamName.concat(reqExt);
             }
             String retValx = ( (srm.get(reqParamName) != null  &&  
                                 srm.get(reqParamName).toString().length() > 0 )
                             ? ("value=\"" + JspUtil.encodeHtml((String)srm.get(reqParamName)) + "\"") : "");
             retVal = retValx;       
        }
        if ("".equals(retVal)) {
            // Get default value
            Iterator itAV = scrElm.iterator();

            while (itAV.hasNext()) {
                GSAllowedValue av = (GSAllowedValue)itAV.next();
                if ( "dynamic".equals(av.getContent())) {
                    // Dynamic Content via method call
                    log.debug("Dynamic Content for Screenelement " + scrElm.getName());
                    options = (Table)performDynamicUIMethod(av.getContentCreateClass(),
                                                 av.getContentCreateMethod());
                    if (options != null  &&  options.getNumRows() == 1) {
                        TableRow row = options.getRow(1);
                        String rowVal = row.getField("VALUE").getString();
                        retVal =  (! "".equals(rowVal) ? ("value=\"" + JspUtil.encodeHtml(rowVal) + "\"") : "");
                    }
                }
            }

        }
        if ("".equals(retVal)) {
            String text = (scrElm.getType().startsWith("date") 
                          ? translate("gs.search.dtfr." + this.getDateFormat()) : "");
            retVal = (( scrElm.getValue() != null  && scrElm.getValue().toString().length() > 0)
                     ? ("value=\"" + scrElm.getValue() + "\"") : ("value=\"" + JspUtil.encodeHtml(text) + "\""));
        }
        return retVal;
    }
    /**
     * Return the path/name of the include which is placed on top of the &lt;body&gt; tag. 
     * @return String include name or null
     */
    public String getBodyIncludeTop() {
        return ((scrElms.getBodyInclTop() != null  &&  scrElms.getBodyInclTop().length() > 0)
               ? scrElms.getBodyInclTop() : null);
    }
    /**
     * Return the path/name of the include which is placed on bottom of the HTML page 
     * before the &lt;/body&gt; tag. 
     * @return String include name or null
     */
    public String getBodyIncludeBottom() {
        return ((scrElms.getBodyInclBottom() != null  &&  scrElms.getBodyInclBottom().length() > 0)
              ? scrElms.getBodyInclBottom() : null);  
    }
	/**
	 * Return the path/name of the include which is placed <b>before</b> the result list table 
	 * @return String include name or null
	 */
	public String getBodyIncludeBeforeResult() {
		return ((getResultListDescription() != null                                &&
				 getResultListDescription().getBodyInclBeforeResult() != null      &&  
				 getResultListDescription().getBodyInclBeforeResult().length() > 0)
			  ? getResultListDescription().getBodyInclBeforeResult() : null);  
	}
	/**
	 * Return the path/name of the include which is placed <b>after</b> the result list table 
	 * @return String include name or null
	 */
	public String getBodyIncludeAfterResult() {
		return ((getResultListDescription() != null                                &&
		         getResultListDescription().getBodyInclAfterResult() != null       &&  
		         getResultListDescription().getBodyInclAfterResult().length() > 0)
			  ? getResultListDescription().getBodyInclAfterResult() : null);  
	}
    /**
     * Return the name of the JScript function which will be called
     * at the 'onclick' event.
     * @return String name
     */
    public String getJScriptOnStartButton() {
        String retVal = "";
        // sendRequest(); is default JScript
        if ("sendRequest();".equals(scrElms.getUIJScriptOnStartButton())) {
            retVal = (scrElms.getLinkTargetFrameName() != null && scrElms.getLinkTargetFrameName().length() > 0
            ? ("sendRequest('" + scrElms.getLinkTargetFrameName() + "'); ") : scrElms.getUIJScriptOnStartButton()); 
        } else {
            retVal = scrElms.getUIJScriptOnStartButton();
        }
        return retVal;
    }
    /**
     * Write the 'Go!' button target info attribute depending on the GSPropertyGroup.getLinkTargetFrameName() 
     * and the SPropertyGroup.getUIJScriptOnStartButton() method.
     * @return String target info
     */
    public String writeStartButtonTargetInfo() {
        String retVal = "";
        // sendRequest(); is the default JScript
        if ( ! "sendRequest();".equals(scrElms.getUIJScriptOnStartButton())) {
            retVal = (scrElms.getLinkTargetFrameName() != null && scrElms.getLinkTargetFrameName().length() > 0
                     ? (" target=\"" + scrElms.getLinkTargetFrameName() + "\" ") : ""); 
        }
        return retVal;
    }
    /**
     * Has Request been started? The check will performed on the parameter  
     * GenericSearchUIData.RC_REQUESTSTARTED.
     * @return boolean true or false
     */
    public boolean isRequestStarted() {
        boolean retVal = false;
        retVal = ( (request.getParameter(GenericSearchUIData.RC_REQUESTSTARTED) == null) ? false : true );
        if (! retVal) {
            retVal = ( (userSessionData.getAttribute(GenericSearchUIData.RC_REQUESTSTARTED) == null) ? false : true );
            userSessionData.removeAttribute(GenericSearchUIData.RC_REQUESTSTARTED);
        }
        if (! retVal) {
            retVal = ( (request.getParameter("genericsearch.sort") == null) ? false : true );
        }
        
        return retVal;
    }
    /**
     * Write event messages
     * @param int handle
     * @return String containing the event messages
     */
    public String writeEventMessages(int handle) {
        String msgText = "";
        // Message prefix can also be defined at result list description 
		String msgPrefix = getResultListDescription().getMsgKeyObjectFound();
        if ("gs.result.msg.".equals(msgPrefix)) {  // Still default on result list description
        	msgPrefix = scrElms.getMsgKeyObjectFound(); // than take it (as default) from search description
        }
        // Check for max hits event
        if (isEventOccured(handle, new GenericSearchMaxHitsEvent())) {
            String evtInfo = getEventInfo(handle, new GenericSearchMaxHitsEvent());
            if (evtInfo == null) {
                // No additional infos are available => write simple message
                msgText = WebUtil.translate(pageContext, (msgPrefix + "evt.s"), 
                           new String[] {Integer.toString(getNumberOfReturnedDocuments(handle))});
            } else {
                // Write enhanced message
                String arg[] = new String[2];
                arg[0] = Integer.toString(getNumberOfReturnedDocuments(handle));
                arg[1] = evtInfo;
                msgText = WebUtil.translate(pageContext, (msgPrefix + "evt.e"), arg);
            }
        } else {
            // Write normal message about found objects
            if (getNumberOfReturnedDocuments(handle) <= 0) { 
                msgText = translate(msgPrefix + "0");
            } else if (getNumberOfReturnedDocuments(handle) == 1) { 
                msgText = translate(msgPrefix + "1");
            } else { 
                msgText = WebUtil.translate(pageContext, (msgPrefix + "n"), 
                           new String[] {Integer.toString(getNumberOfReturnedDocuments(handle))});
            }
        }
        return msgText;
    }
    /**
     * Write label for a search element (e.g. drop down box). Depending on the location, which will be
     * check against the screen element settings, the label will be written or not.  
     * @param String location where the label should be placed 
     * @param GSProperty screen element
     * @param String ID extension (e.g. _LOW, _HIGH)
     * @return
     */
    public String writeSearchElementLabel(String location, GSProperty pty, String ext) {
        if (ext == null  ||  ext.length() <= 0) {
            // Extension is not specified us other method
            return writeSearchElementLabel(location, pty);
        }
        if (pty.getLabel() == null  ||  pty.getLabel().length() <= 0) {
            // No label defined
            return "";
        }
        if ( ! location.equals(pty.getLabelAlignment())) {
            return "";
        }
        String lblTxtKey = null;
        if (pty.getLabel() != null && pty.getLabel().length() > 0) {
            lblTxtKey = pty.getLabel() + ext;
        }
        StringBuffer strB = new StringBuffer();
        strB.append("<label for=\"ID" + pty.hashCode() + ext + "\">" + getLabelText(lblTxtKey));
        strB.append("</label>");
        if ("top".equals(location)) {
            // if placed on top of the search element add an <br> to separate the elements
            strB.append("<br>");
        }
        return strB.toString();
    }
    /**
     * Write label for a search element (e.g. drop down box). Depending on the location, which will be
     * check against the screen element settings, the label will be written or not.  
     * @param String location where the label should be placed 
     * @param GSProperty screen element
     * @return
     */
    public String writeSearchElementLabel(String location, GSProperty pty) {
        if (pty.getLabel() == null  ||  pty.getLabel().length() <= 0) {
            // No label defined
            return "";
        }
        if ( ! location.equals(pty.getLabelAlignment())) {
            return "";
        }
        StringBuffer strB = new StringBuffer();
        strB.append("<label for=\"ID" + pty.hashCode() + "\">" + getLabelText(pty.getLabel()));
        strB.append("</label>");
        if ("top".equals(location)) {
            // if placed on top of the search element add an <br> to separate the elements
            strB.append("<br>");
        }
        return strB.toString();
    }
    /**
     * Write result list header including its links for sorting. The sorting sequence is dependend
     * of the search request memory.
     * @param GSProperty header which should be written
     * @return String including linked header description
     */
    public String writeResultListHeader(GSProperty header) {
        StringBuffer retVal = new StringBuffer();
        Map sortInfo = getSortInfoObject(getScreenName());
		String arg[] = new String[1];
		arg[0] = translate(header.getColumnTitle());
        String sortDir = null;
        String fieldText = (header.getColumnTitle() != null  &&  header.getColumnTitle().length() > 0
                            ? translate(header.getColumnTitle()) : "");
        if ("checkbox".equals(header.getType())  &&   ! header.isReadOnly()) {
        	// generate a checkbox allowing to select or deselect all checkboxes of the column
        	String text = WebUtil.translate(pageContext, "gs.result.checkbox.mark", arg);
        	retVal.append("<br /><input type=\"checkbox\" onclick=\"checkboxSelectAll(this, '" 
        	              + header.getRequestParameterName() + "');\" title=\""+text+ "\" />");
        }
        if ("".equals(fieldText)) {
            // Blank object will NOT be linked
            retVal.insert(0, fieldText);
            return retVal.toString();
        }
        // Property is defined to be not sortable
        if ("nosort".equals(header.getType())  ||  ! header.isReadOnly()) {
			retVal.insert(0, fieldText);
			return retVal.toString();
        }
        if (sortInfo != null) {
            // Is this header field currently sorted and what direction
            sortDir = (String)sortInfo.get(header.getName());
        }

        if (getSearchRequestMemory() != null) {
            String columnType = "";
            if (sortDir != null) {
                // This header field is currently sorted by the "sortDir" direction
                if (Integer.toString(GenericSearchComparator.DESCENDING).equals(sortDir)) {
                    String iconPath = WebUtil.getMimeURL(pageContext, "mimes/images/gs_arrow_down.gif");
                    String altTextIcon = WebUtil.translate(pageContext, "gs.result.d.sort", arg);
                    String titleHref = " title=\"" + WebUtil.translate(pageContext, "gs.result.a.sorting", arg) + "\" "; 
                    retVal.append("<img alt=\"" + altTextIcon + "\" src=\"" + iconPath + "\"/>" );
                    retVal.append("<a href=\"#\"" + titleHref + "  onclick=\"sendSortRequest('" + header.getName() + "','");
                    retVal.append(GenericSearchComparator.ASCENDING + "',");                        
                } else {
                    String iconPath = WebUtil.getMimeURL(pageContext, "mimes/images/gs_arrow_up.gif");
                    String altTextIcon = WebUtil.translate(pageContext, "gs.result.a.sort", arg); 
                    String titleHref = " title=\"" + WebUtil.translate(pageContext, "gs.result.d.sorting", arg) + "\" ";                
                    retVal.append("<img alt=\"" + altTextIcon + "\" src=\"" + iconPath + "\"/>" );
                    retVal.append("<a href=\"#\"" + titleHref + "  onclick=\"sendSortRequest('" + header.getName() + "','");
                    retVal.append(GenericSearchComparator.DESCENDING + "',");
                }
            } else {
                // Field is not sorted so generate link to sort DESENDING
                String titleHref = " title=\"" + WebUtil.translate(pageContext, "gs.result.d.sorting", arg) + "\" ";
                retVal.append("<a href=\"#\"" + titleHref + " onclick=\"sendSortRequest('" + header.getName() + "','");
                retVal.append(GenericSearchComparator.DESCENDING + "',");
            }
            if ( (header.getType() == null || header.getType().length() <= 0)  &&
                 (header.getTranslationPrefix() != null  && header.getTranslationPrefix().length() > 0) ) {
                columnType = "translate";
                sortInfo.put(GenericSearchComparator.TRANSLATEPREFIX, header.getTranslationPrefix());
            } else {
                columnType = header.getType();
            }
            retVal.append("'" + (columnType != null ? columnType : "") + "')\">");
            retVal.append(fieldText);
            retVal.append("</a>");
        } else {
            retVal.append(fieldText);
        }
        return retVal.toString();
    }
    /**
     * Returns true if page should be rendered with HTML tags like <code>&lt;head&gt;</code>
     *  and <code>&lt;body&gt;</code>. This could be used if the page should be included into 
     * another page.
     * @return boolean
     */
    public boolean isFullScreenEnabled() {
        return fullScreenEnabled;
    }
    /**
     * Returns true if the select option part of the screen should be displayed.
     * @return boolean 
     */
    public boolean isSelectOptionEnabled() {
        return selectOptionEnabled;
    }
    /**
     * Returns true if the result list part of the screen should be displayed.
     * @return boolean 
     */
    public boolean isResultListEnabled() {
        return resultListEnabled;
    }
    /**
     * Returns a String including the JScript coding to communicate with the HelpValuesSearch. 
     * @param GSProperty screen element for which a HelpValuesSearch should be realized
     * @return String 
     */
    public String getHelpValuesSearch(GSProperty pty) {
        String retVal = "";
        if (pty.getType() != null && (pty.getType().startsWith("input")  && pty.getHelpValuesMethod() != null  
              &&  pty.getHelpValuesMethod().length() > 0)) { 
            HelpValuesSearch helpValuesSearch = getHelpValuesSearch(pty.getHelpValuesMethod());
            Map parameterMapping = new HashMap();
            if (helpValuesSearch!=null) {
                parameterMapping.put(helpValuesSearch.getParameter().getName(),pty.getRequestParameterName());
            }
            retVal = HelpValuesSearchUI.getJSPopupCoding(pty.getHelpValuesMethod(),pageContext, parameterMapping);
        }
        return retVal;
    }
    /** 
     * If select options and result list are in different frames (&lt;propert-group linkTargetFrameName="..."&gt;)
     * then after the search has been performed the search criteria needs to be re-activated.
     * @return String including a jScript function
     */
    public String enableSearchOptions() {
        String retVal = "";
        String frameName = request.getParameter(GenericSearchUIData.RC_ENABLESELECTOPTIONS_FRAMENAME);
        if (frameName != null  &&  frameName.length() > 0) {
            retVal = ";enableSelectOptions('" + frameName + "');";
        }
        return retVal;
    }
    /**
     * Returns a string of format <code>"tabindex='&lt;no.&gt;' "</code>. Each time method is called
     * &lt;no.&gt; will be increaded by 1.<br>
     * @param boolean accessibility relevant or not
     * @return String containing the current tabindex
     */
    public String getTabIndex(boolean access) {
        String retVal = "";
        if ( access == false ||
             (access == true  && isAccessible == true) ) {
            // Set tabindex only depending on accessibility.
            retVal = "tabindex=" + "\"" + String.valueOf(tabIndex) + "\"";
            tabIndex++;
        }
        return retVal;
    }
    /**
     * In accessiblity mode return string (e.g. title="Sales orders") announcing a HTML td element 
     * of a result list 
     * @param GSProperty rowfield of a result list
     * @return
     */
    public String writeResultTitlePerTD(GSProperty pty) {
        String retVal = "";
        if (isAccessible) {
            String titleText = (pty.getColumnTitle() != null  &&  pty.getColumnTitle().length() > 0
                                ? translate(pty.getColumnTitle()) : null);
            retVal = (titleText != null ? "title=\"" + titleText + "\"" : "");
        }
        return retVal;
    }
    /**
     * Write jScript onLoad functions for the &lt;body ...&gt; tag.  
     * @return String containing the JScript functions to call at onLoad event
     */
    public String writeJScriptBodyOnload() {
        String retVal = "";
        if (isSelectOptionEnabled()) {
            retVal = "evaluateX();";
            retVal = (retVal + enableSearchOptions());
        }
        if (isRequestStarted() &&  isAccessible) {
            retVal = retVal + "accSetFocusonMsg();";
        }
        return retVal;
    }
	/**
	 * Gets the occured messages in the backend for a given <code>HANDLE</code>
	 * @param  int   Handle 
	 */
	public String getMessagesFromBackend(int handle) {
		String messages = null;
		ResultData tab = searchInstance.getBackendMessages();
		if(tab != null) {
		   StringBuffer sb = new StringBuffer();
		    int compHandle = 0;
			while(tab.next() ) {
                if (! "D".equals(tab.getString("ID"))) {
                    compHandle = tab.getInt("HANDLE");
                    if (compHandle == handle) {             
                        sb.append(tab.getString("MESSAGE"));
                    } 
                }
			}
			messages = sb.toString();
		}
         
		return messages;
	}
    
    /**
     * Returns usefull debugging infos. Debugging infos are marked with ID = 'D'.
     */
    public String getDebugInfo() {
        String messages = null;
        ResultData tab = searchInstance.getBackendMessages();
        if(tab != null) {
           StringBuffer sb = new StringBuffer();
            int compHandle = 0;
            while(tab.next() ) {
                if ("D".equals(tab.getString("ID"))) {
                    sb.append(tab.getString("MESSAGE"));
                }
            }
            messages = sb.toString();
        }
         
        return messages;
        
    }
    /**
     * Returns whether attribute "isExpandedResultlist" is set to true or false and methods
     * "getJScriptOnResultExpand" and "getJScriptOnResultCollapse" don't return null. 
     * Method "getNumberOfReturnedDocuments" must not return zero documents.
     * @return true or false
     * @see com.sap.isa.ui.uiclass.genericsearch.GenericSearchBaseUI#getJScriptOnResultExpand
     * @see com.sap.isa.ui.uiclass.genericsearch.GenericSearchBaseUI#getJScriptOnResultCollapse
     * @see com.sap.isa.ui.uiclass.genericsearch.GenericSearchBaseUI#getNumberOfReturnedDocuments
     */
    public boolean isExpandedResultlist() {
    	boolean retVal = false;
    	if (getResultListDescription() != null) {
    		if (getResultListDescription().isExpandedResultlist()  &&
			    getNumberOfReturnedDocuments(1) > 0             &&
    		    getJScriptOnResultExpand() != null              &&
    		    getJScriptOnResultCollapse() != null            &&
			    isAccessible == false) {
    		    	retVal = true;
    		}
    	}
    	return retVal;
    }
    /**
     * Returns the JScript function to be called for expanding the navigator part of the
     * frameset. Overwrite this method with the application specific UI class to provide
     * the appropriated JScript function.
     * @return Always null
     */
    public String getJScriptOnResultExpand() {
    	return null;
    }
	/**
	 * Returns the JScript function to be called for collapsing the navigator part of the
	 * frameset. Overwrite this method with the application specific UI class to provide
	 * the appropriated JScript function.
	 * @return Always null
	 */
	public String getJScriptOnResultCollapse() {
		return null;
	}
    /**
     * Encodes given string for HTML. Included formatting command "/n" for a new line will be
     * converted into &lt;br/&gt;.
     * @param input which could contain HTML commands
     * @return HTML encode ouput string
     */
    public String encodeHTML(String input) {   /* note 1340113*/
        StringBuffer retVal = new StringBuffer();
        
        if (input != null) {
            String[] splitResult = input.split("/n");
            for (int x=0; x<splitResult.length; x++) {
                retVal.append(JspUtil.encodeHtml(splitResult[x]));
                if (splitResult.length > 1  &&  x != (splitResult.length - 1)) { // more than one line and not after the last element
                    retVal.append("<br/>");
                }
            }
        }
        
        return retVal.toString();
    }
}