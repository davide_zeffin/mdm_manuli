/*****************************************************************************
    Class:        GenericSearchUIFactory
    Copyright (c) 2003, SAP AG, Germany, All rights reserved.

    $Revision: #6 $
    $DateTime: 2004/08/09 15:59:48 $ (Last changed)
    $Change: 199293 $ (changelist)
    
*****************************************************************************/

package com.sap.isa.ui.uiclass.genericsearch;

import javax.servlet.jsp.PageContext;

import com.sap.isa.businessobject.GenericSearchBuilder;
import com.sap.isa.core.UserSessionData;
import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.core.logging.LogUtil;
import com.sap.isa.core.util.table.Table;
import com.sap.isa.core.xcm.ConfigContainer;
import com.sap.isa.maintenanceobject.backend.GenericSearchBuilderHelper;
import com.sap.isa.maintenanceobject.backend.MaintenanceObjectHelperException;

/**
 * The class GenericSearchUIFactory.java returns instances of various objects
 * used for the generic search UI.
 *
 * @author SAP
 * @version 1.0
 **/
public class GenericSearchUIFactory {

    /**
     * Constant that specifies the data type of a return table. Using this will return
     * a table containing colums for select box options.<br>
     * <code>&lt;option value="x1" selected&gt;Description&lt;/option&gt;<br>
     * Containing following fields:<br>
     * 1. VALUE = value of the option (x1 in the example above)<br>
     * 2. SELECTED = true or false<br>
     * 3. HIDDEN = true or false<br>
     * 4. DESCRIPTION = visible description of the option<br>
     * 
     */
    public static final int TYPE_RETURNTABLE_SELECTBOXOPTION = 1;
    /**
     * Constant that specifies the data type of a return table. Using this will return
     * a table containing colums for a single field output.<br>
     * Containing following fields:<br>
     * 1. TYPE = FIELD or ICON<br> 
     * 2. VALUE = Value of the field<br>
     * 3. READONLY = true or false (default is false)<br>
     * 4. ICONPATH = Path to the Icon (only if TYPE = ICON)<br>
     * 5. ICONWIDTH = width of the icon (String)<br>
     * 6. ICONHIGHT = hight of the icon (String)<br>
     * 7. ICONBORDER = border in pixel (e.g. 0 = no border)<br>
     * 8. ICONTITLE = title of the icon (quick tip)
     */
    public static final int TYPE_RETURNTABLE_OUTPUTFIELD = 2;
    /**
     * Constant that specifies the data type of a return table. Using this will return
     * a table containing colums specifying a JScript files.<br>
     * Containing following fields:<br>
     * 1. FILENAME = name and path starting from <root> <br> 
     */
    public static final int TYPE_RETURNTABLE_JSCRIPTFILES = 3;
    /**
     * Constant that specifies the data type of a return table. Using this will return
     * a table containing colums for a input field value.<br>
     * <code>&lt;input <b>value="x1"</b>&gt;<br>
     * Containing following fields:<br>
     * 1. VALUE = value of the option (x1 in the example above)<br>
     */
    public static final int TYPE_RETURNTABLE_INPUTFIELD = 4;
    /**
     * reference to the IsaLocation
     * @see com.sap.isa.core.logging.IsaLocation
     */
    protected static IsaLocation log = IsaLocation.
          getInstance(GenericSearchBaseUI.class.getName());

    /**
     * Constructor for GenericSearchUIFactory.
     * @param pageContext
     */ 
    protected GenericSearchUIFactory() {
    }
    
    /**
     * This method returns the instance of the UI class.  
     * The class name can be defined in the <code>modification/generic-searchbackend-config.xml</code>
     * file on Screen Group level. Screen group must be of type 'search'.
     * <p>Example</p>
     * <p>&lt;screen-group name="search_criteria"<br>
     *                     type="search"
     *                     uiClassName="com.sapmarktes...GenericSearchISAUI"&gt;
     * </p>
     * @param Servlet PageContext
     * @return GenericSearchUI class casted to GenericSearchUIData
     */
    public static GenericSearchUIData getUIClassInstance(PageContext pageContext) {
        GenericSearchUIData retObj = null;
        
        // Initialize Helper Object
        GenericSearchBuilderHelper objectHelper = new GenericSearchBuilderHelper();
        ConfigContainer container = (UserSessionData.getUserSessionData(pageContext.getSession())).getCurrentConfigContainer();
        try {
            GenericSearchBuilder object = (GenericSearchBuilder)objectHelper.createObjectFromXCM(GenericSearchBuilder.class.getName(),container, "generic-search-config");

            String uiClassName = object.getUIClassName();
            retObj = (GenericSearchUIData)((Class.forName(uiClassName)).newInstance());
            // Init Object
            retObj.init(pageContext);

        } catch (MaintenanceObjectHelperException ex) {
            log.error(LogUtil.APPS_USER_INTERFACE, "", ex);;
        } catch (Exception ex) {
            log.error(LogUtil.APPS_USER_INTERFACE, "", ex);
        }
        return retObj;
    }
    /**
     * This method returns a Table which should be used to communicate from a dynamic content
     * method to the genericsearch.jsp.<br>
     * @param int @see TYPE_RETURNTABLE_SELECTBOXOPTION @see TYPE_RETURNTABLE_OUTPUTFIELD
     * @see TYPE_RETURNTABLE_JSCRIPTFILES @see TYPE_RETURNTABLE_INPUTFIELD
     */
    public static Table getUIReturnTable(int type) {
        Table tab = new Table("UIReturnTable");
        switch (type) {
            case TYPE_RETURNTABLE_SELECTBOXOPTION:
                tab.addColumn(Table.TYPE_STRING, "VALUE");
                tab.addColumn(Table.TYPE_BOOLEAN, "SELECTED");
                tab.addColumn(Table.TYPE_BOOLEAN, "HIDDEN");
                tab.addColumn(Table.TYPE_STRING, "DESCRIPTION");
                break;
            case TYPE_RETURNTABLE_INPUTFIELD:
                tab.addColumn(Table.TYPE_STRING, "VALUE");
                break;
            case TYPE_RETURNTABLE_OUTPUTFIELD:
                tab.addColumn(Table.TYPE_STRING, "TYPE");
                tab.addColumn(Table.TYPE_STRING, "VALUE");
                tab.addColumn(Table.TYPE_BOOLEAN,"READONLY");
                tab.addColumn(Table.TYPE_STRING, "ICONPATH");
                tab.addColumn(Table.TYPE_STRING, "ICONWIDTH");
                tab.addColumn(Table.TYPE_STRING, "ICONHIGHT");
                tab.addColumn(Table.TYPE_STRING, "ICONBORDER");
                tab.addColumn(Table.TYPE_STRING, "ICONTITLE");
                break;
            case TYPE_RETURNTABLE_JSCRIPTFILES:
                tab.addColumn(Table.TYPE_STRING, "FILENAME");
                break;
        }
        
        return tab;
    }
}