/*****************************************************************************
    Interface:        GenericSearchUIData
    Copyright (c) 2003, SAP AG, Germany, All rights reserved.

    $Revision: #21 $
    $DateTime: 2004/07/21 15:29:47 $ (Last changed)
    $Change: 196763 $ (changelist)
    
*****************************************************************************/

package com.sap.isa.ui.uiclass.genericsearch;

import java.util.Map;

import javax.servlet.jsp.PageContext;

import com.sap.isa.core.util.table.ResultData;
import com.sap.isa.maintenanceobject.businessobject.GSProperty;


/**
 * Represents all allowed contants and methods in the Generic Search UI environment 
 *
 * @author SAP
 * @version 1.0
 **/
public interface GenericSearchUIData {
    /**
     * Key to store and retrieve Screen Elemnts in the request context
     */
    public static String RC_SCREENELEMENTS = "GSscreenelements";
    /**
     * Key to store and retrieve Options in the request context
     */
    public static String RC_OPTIONS = "GSoptions";
    /**
     * Key to store and retrieve Follow-up Screen Elements in the request context
     */
    public static String RC_FOLLOWUPELEMENTS = "GSfollowups";
    
    /**
     * Key to store and retrieve the name of the search criteria screen
     * in the request context.<br>
     * <p>
     * &lt;screen-group name="search_criteria" ...<br>
     * __&lt;property-group&gt;<b>Name of the search criteria screen</b>&lt;/property-group&gt;
     * </p>
     */
    public static String RC_SEARCHCRITERASCREENNAME = "genericsearch.name";
    /**
     * Key to store and retrieve the name of the result list in the request context. Using this
     * parameter the result list which should be used can be specified in the URL. 
     */
    public static String RC_RESULTLISTNAME = "genericsearch.resultlist";
    /**
     * Key to store and retrieve the info if a search request has been started or not.
     */
    public static String RC_REQUESTSTARTED = "genericsearch.start";    
    /**
     * Key to store and retrieve List Elemnts (Header) in the request context
     */
    public static String RC_RESULTLIST_HEADER = "GSresultlistheader";
    /**
     * Key to store and retrieve List Elemnts (Rows) in the request context
     */
    public static String RC_RESULTLIST_ROWS = "GSresultlistrows";
    /**
     * Key to store and retrieve List Elemnts (Fields) in the request context
     */
    public static String RC_RESULTLIST_FIELDS = "GSresultlistfields";
    /**
     * Key to store and retrieve the result list Property Group in the request context 
     */
    public static String RC_RESULTLIST_PTYGRP = "GSresultlistpropertygroup";
    /**
     * Key to store and retrieve the date format in the request context 
     */
    public static String RC_DATEFORMAT = "GSdateformat";
    /**
     * Key to store and retrieve the number format in the request context 
     */
    public static String RC_NUMBERFORMAT = "GSnumberformat";
    /**
     * Key to store and retrieve the number format in the request context 
     */
    public static String RC_LANGUAGE = "GSlanguage";
    /**
     * Prefix-Key to store and retrieve the search request memory in the session context 
     */
    public static String SC_SEARCHREQUEST_MEMORY = "GSsrm";
    /**
     * Key to store and retrieve the last search request the session context 
     */
    public static String SC_LASTSELECTOPTIONS = "GSlastselectoptions";
    /**
     * Key to store and retrieve the sort info object (Map) in the request context
     */
    public static String RC_SORTINFO = "gssortinfo";
    /**
     * Key to store and retrieve the information if the used 'search description' should
     * be stored as <b>actual organizer</b> in the document handler or not.  
     */
    public static String RC_DOCUMENTHANDLERNOADD = "GSdocumenthandlernoadd";
    /**
     * Key to store and retrieve the information if the used 'search description' should
     * be stored as <b>business object</b> in the document handler or not. 
     */
    public static String RC_DOCUMENTHANDLERBUSOBJADD = "GSdocumenthandlerbusobjadd";

    /**
     * Key to store and retrieve the information if the select option part of 
     * the screen should be displayed.<br>Possible value <code>true</code>.
     */
    public static String RC_UI_NOSELECTOPTIONS = "GSnoselectoptions";
    /**
     * Key to store and retrieve the information if page should be rendered 
     *  with HTML tags like <code>&lt;head&gt;</code> and <code>&lt;body&gt;</code>.
     *  This could be used if the page should be included into another page.<br>
     * Possible value <code>true</code>.
     */
    public static String RC_UI_NOFULLSCREENMODE = "GSnofullscreenmode";
    /**
     * Key to store and retrieve the current ui class from the request.
     */
    public static String RC_UICLASSINSTANCE = "GSuiclassinstance";
    /**
     * Key to store and retrieve the information of the frame name where the 
     * select options should be reactivated.
     */
    public static String RC_ENABLESELECTOPTIONS_FRAMENAME = "GSenableselectoptionsframename";
    /**
     * Key to store and retrieve the information which can be used in an imported include. 
     */    
    public static String RC_UI_INCLUDESTOSHOW = "GSincludestoshow";
    /**
     * Key to store and retrieve the trigger doing the last search again<br>
     * Possible value <code>true</code>
     */
    public static String RC_REDOLASTSEARCH = "GSredolastsearch";
    /**
     * Store Screen elements in request context to be used by Iterate tag 
     */
    public void setScreenElements();
    /**
     * Store Options in request context to be used by Iterate tag. 
     * @return boolean true if options exists and false if not 
     * @param ScreenElement for which options should be set
     */
    public boolean setOptions(GSProperty scrElm);
    /**
     * Store all follow-up Elements for this Entry (Option) in the request
     * context to be used by Iterate tag 
     * @param  Property name for which follow-up Elements should be found
     * @param  Proertty value for which follow-up Elements should be found
     */
    public void setFollowupElements(String ptyName, String ptyValue);
    /**
     * Set index to Screen Element. 
     * See also @link getScreenElementIndex
     * @param Screen element
     * @param Index to be assigned 
     */
    public void setIndexToScreenElement(GSProperty pty, int idx);
    /**
     * Get Index for a screen element
     * @param Screen Element for which index should be returned
     * @return Index of that Screen Element
     */
    public int getIndexOfScreenElement(GSProperty pty);
    /**
     * Return true if the Property has dependencies (VisibilityConditions).
     * @return true if dependencies exists
     */
    public boolean hasDependencies(GSProperty pty);
    /**
     * Return the amount of found documents for a given <code>HANDLE</code>.
     * @param  int   Handle
     * @return int   Number of found documents
     */
    public int getNumberOfReturnedDocuments(int handle);
    /**
     * Store List element Header in request context to be used by Iterate tag 
     */
    public void setResultListHeader();
    /**
     * Store List element Rows in request context to be used by Iterate tag 
     */
    public void setResultListRows();
    /**
     * Store List element Header in request context to be used by Iterate tag 
     */
    public void setResultListFields();
    /**
     * Returns the number of visible columns; columns which are not marked with 'type="hidden"'
     * @return Number of visible columns
     */
    public int getNumberOfVisibleColumns();
    /**
     * Return the memory object containing the last search request. This can be used
     * to re-build the search screen the way it has been send before. 
     * @return Map Containing the last search request as key - value pairs
     */
    public Map getSearchRequestMemory();
     /**
     * Initialize UI Object. Necessary since reflection api doesn't support
     * instantiation of classes with arguments.
     */
    public void init(PageContext pageContext);
    /**
     * Return the date format for UI.<br>
     * <b>Make sure application set the date format correctly</>. By default the date format
     * is set to <b>YYYY.MM.DD</b>
     * @return String representing the date's format
     */
    public String getDateFormat();
    /**
     * Write out the translated value of the field. If makes used of method @see translateResultlistValue
     * for the translation.  
     * @param GSProperty field which should be written
     * @param String value of the field
     * @return String containing the foramtted and tranlated value
     */
    public String writeResultlistValue(GSProperty field, String value);
    /**
     * Returns the resultlist Value. It could be manipulated by the definded output handler
     * or just the pure value.
     * @param ResultData Row in the resultlist
     * @param GSProperty field in the row
     * @return Table containing field value
     */
    public ResultData fieldOutputHandler(ResultData row, GSProperty pty);
    /**
     * Returns the link parameter which should be added to the URL stored in XML property attribute
     * <code>hyperlink</code>
     * @param ResultData Row in the resultlist
     * @param GSProperty field in the row
     * @return String containing parameters to add
     */
     public String getLinkParameter(ResultData row, GSProperty pty);
    /**
     * Returns the name of JScript files which should be included.
     * @return ResultData containing file names
     */
    public ResultData getJScriptFilesTab();
    /**
     * Returns the JScript functions which should be called in case of the 'onchange' Event.<br>
     * <strong>i.e.</strong> func_01(); func_02(this);
     * @param GSProperty representing the screen element (box, text, ...)
     * @param String Basic JScript function to be called
     * @return String final string 
     */
    public String getJScriptOnChangeFinal(GSProperty pty, String adJS);
    /**
     * Return the Screen name which describes the search criteria in the XML file
     * @return String screen name
     */
    public String getScreenName();
    /**
     * Return the number format for UI.<br>
     * <b>Make sure application set the number format correctly</>. By default the number format
     * is set to <b>#.###.###,##</b>
     */
    public String getNumberFormat();
    /**
     * Return true if screen area definded in xml file belongs to the one just processed
     * in the JSP
     * @param String screen area just processed in JSP
     * @param GSProperty screen element
     * @return boolean true if equal
     */
    public boolean isBelongingToScreenArea(String screenArea, GSProperty pty);
    /**
     * Return the maximal number of screen areas which should be available. The screen area could be
     * used to place the search criterias in. Those areas could be controlled by the stylesheet where 
     * the name of the several &lt;DIV&gt; classes are <b>filter-area-&lt;Number&gt;</b> 
     */
    public int getMaxScreenAreas();
    /**
     * Return the language taken from the locale. <br>
     * 
     * @return page language
     */
    public String getLanguage();
    /**
     * Return the CSS class name for the &lt;body&gt; tag.
     * @see com.sap.isa.maintenanceobject.businessobject.GSPropertyGroup.cssBodyClassName
     * @return String css class name
     */
    public String getCssBodyTagClassName();
    /**
     * Return the ID for the main &lt;div&gt; container to allow adding css formatting
     * information to it.
     * @see com.sap.isa.maintenanceobject.businessobject.GSPropertyGroup.cssMainDivId
     * @return String div id
     */
    public String getCssMainDivID();
    /**
     * Checks whether or not an &lt;option&gt; line from a select box will be marked as 
     * <b>selected</b> or not (same for radio buttons).
     * @param Map search request memory
     * @param GSProperty screen element
     * @param ResultData select box options
     * @return String
     */
    public String writeObjSelectedInfo(Map srm, GSProperty screlm, ResultData option);
    /**
     * Write the value of the input field depending on the search request memory
     * @param Map search request memory
     * @param GSProperty screen element
     * @param String request parameter extension (e.g. _low, _high) 
     * @return String
     */
    public String writeInputValueInfo(Map srm, GSProperty screlm, String reqExt);
    /**
     * Return the path/name of the include which is placed on top of the &lt;body&gt; tag. 
     * @return String include name or null
     */
    public String getBodyIncludeTop();
    /**
     * Return the path/name of the include which is placed on bottom of the HTML page 
     * before the &lt;/body&gt; tag. 
     * @return String include name or null
     */
    public String getBodyIncludeBottom();
    /**
     * Return the name of the JScript function which will be called
     * at the 'onclick' event.
     * @return String name
     */
    public String getJScriptOnStartButton();
	/**
	 * Return the path/name of the include which is placed <b>before</b> the result list table 
	 * @return String include name or null
	 */
	public String getBodyIncludeBeforeResult();
	/**
	 * Return the path/name of the include which is placed <b>after</b> the result list table 
	 * @return String include name or null
	 */
	public String getBodyIncludeAfterResult();
    /**
     * Write the 'Go!' button target info attribute depending on the GSPropertyGroup.getLinkTargetFrameName()
     * method.
     * @return String target attribute
     */
    public String writeStartButtonTargetInfo();
    /**
     * Has Request been started? The check will performed on the parameter  
     * GenericSearchUIData.RC_REQUESTSTARTED.
     * @return boolean true or false
     */
    public boolean isRequestStarted();
    /**
     * Write result list header including its links for sorting. The sorting sequence is dependend
     * of the search request memory.
     * @param GSProperty header which should be written
     * @return String including linked header description
     */
    public String writeResultListHeader(GSProperty header);
    /**
     * Return <code>true</code> if requested event occured
     * @param int handle
     * @param GenericSearchEvent event to be checked
     * @return boolean true if occured
     */
    public boolean isEventOccured(int handle, GenericSearchEvent ev);
    public interface GenericSearchEvent{public String getEvent();}
    public static class GenericSearchMaxHitsEvent 
           implements GenericSearchEvent{public String getEvent() {return "MAXHITS";}}
    /**
     * Write event messages
     * @param int handle
     * @return String containing the event messages
     */
    public String writeEventMessages(int handle);
    /**
     * Write label for a search element (e.g. drop down box). Depending on the location, which will be
     * check against the screen element settings, the label will be written or not.  
     * @param String location where the label should be placed 
     * @param GSProperty screen element
     * @param String ID extension (e.g. _LOW, _HIGH)
     * @return
     */
    public String writeSearchElementLabel(String location, GSProperty pty, String ext);
    /**
     * Write label for a search element (e.g. drop down box). Depending on the location, which will be
     * check against the screen element settings, the label will be written or not.  
     * @param String location where the label should be placed 
     * @param GSProperty screen element
     * @return
     */
    public String writeSearchElementLabel(String location, GSProperty pty);
    /**
     * Returns true if page should be rendered with HTML tags like <code>&lt;head&gt;</code>
     *  and <code>&lt;body&gt;</code>. This could be used if the page should be included into 
     * another page.
     * @return boolean
     */
    public boolean isFullScreenEnabled();
    /**
     * Returns true if the select option part of the screen should be displayed.
     * @return boolean 
     */
    public boolean isSelectOptionEnabled();
    /**
     * Returns true if the result list part of the screen should be displayed.
     * @return boolean 
     */
    public boolean isResultListEnabled();
    /**
     * Returns a String including the JScript coding to communicate with the HelpValuesSearch. 
     * @param GSProperty screen element for which a HelpValuesSearch should be realized
     * @return String 
     */
    public String getHelpValuesSearch(GSProperty pty);
    /** 
     * If select options and result list are in different frames (&lt;propert-group linkTargetFrameName="..."&gt;)
     * then after the search has been performed the search criteria needs to be re-activated.
     * @return String including a jScript function
     */
    public String enableSearchOptions();
    /**
     * Return the module's name (jsp name) including the used screen name
     * @return String module name
     */
    public String getModuleName();
    /**
     * Return the date format for Calendar control. The control needs to have the month indicator formatted
     * in capital letters otherwise it would be interpreted as minutes!<br>
     * i.e. dd.MM.yyyy
     * @return String containing the date foramt
     */
    public String getDateFormatForCalendarControl();
    /**
     * Returns a string of format <code>"tabindex='&lt;no.&gt;' "</code>. Each time method is called
     * &lt;no.&gt; will be increaded by 1.<br>
     * @param boolean accessibility relevant or not
     * @return String containing the current tabindex
     */
    public String getTabIndex(boolean access);    
    /**
     * In accessiblity mode return string (e.g. title="Sales orders") announcing a HTML td element 
     * of a result list 
     * @param GSProperty rowfield of a result list
     * @return
     */
    public String writeResultTitlePerTD(GSProperty pty);
    /**
     * Write jScript onLoad functions for the &lt;body ...&gt; tag.  
     * @return String containing the JScript functions to call at onLoad event
     */
    public String writeJScriptBodyOnload();
    /**
     * Write tool tip for date input field.
     * @return String containing the text
     */
    public String writeDateFormatToolTip();    
	/**
	 * Write out the defined input field (&lt;input&gt; tag with type 'text' or 'checkbox').
	 * @param GSProperty field which should be written
	 * @param String value of the field
	 * @param int current number of line
	 * @param boolean readonly true or false
	 * @return String containing the input field
	 */
	public String writeResultlistInputField(GSProperty field, String value, int lineNo, boolean readOnly);
	/**
	 * Gets the occured messages in the backend for a given <code>HANDLE</code>
     * @param  int   Handle 
	 */
	public String getMessagesFromBackend(int handle);
	/**
	 * Returns whether attribute "isExpandedResultlist" is set to true or false and methods
	 * "getJScriptOnResultExpand" and "getJScriptOnResultCollapse" don't return null. 
	 * Method "getNumberOfReturnedDocuments" must not return zero documents.
	 * @return true or false
	 * @see getJScriptOnResultExpand
	 * @see getJScriptOnResultCollapse, getNumberOfReturnedDocuments
	 * 
	 */
	public boolean isExpandedResultlist();
	/**
	 * Returns the JScript function to be called for expanding the navigator part of the
	 * frameset. Overwrite this method with the application specific UI class to provide
	 * the appropriated JScript function.
	 * @return Always null
	 */
	public String getJScriptOnResultExpand();
	/**
	 * Returns the JScript function to be called for collapsing the navigator part of the
	 * frameset. Overwrite this method with the application specific UI class to provide
	 * the appropriated JScript function.
	 * @return Always null
	 */
	public String getJScriptOnResultCollapse();
    /**
     * Returns usefull debugging infos. Debugging infos are marked with ID = 'D'.
     */
    public String getDebugInfo();
    /**
     * Encodes given string for HTML. Included formatting command "/n" for a new line will be
     * converted into &lt;br/&gt;.
     * @param input which could contain HTML commands
     * @return HTML encode ouput string
     */
    public String encodeHTML(String input);
}