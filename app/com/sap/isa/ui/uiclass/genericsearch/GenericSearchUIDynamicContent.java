/*****************************************************************************
    Class:        GenericSearchUIDynamicContent
    Copyright (c) 2003, SAP AG, Germany, All rights reserved.

    $Revision: #23 $
    $DateTime: 2004/07/23 15:16:09 $ (Last changed)
    $Change: 197093 $ (changelist)
    
*****************************************************************************/

package com.sap.isa.ui.uiclass.genericsearch;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.PageContext;

import com.sap.isa.backend.boi.isacore.order.DocumentListFilterData;
import com.sap.isa.businessobject.BusinessObjectManager;
import com.sap.isa.businessobject.CommunicationException;
import com.sap.isa.businessobject.Search;
import com.sap.isa.businessobject.SearchResult;
import com.sap.isa.businessobject.Shop;
import com.sap.isa.businessobject.User;
import com.sap.isa.businessobject.UserStatusProfileSearchCommand;
import com.sap.isa.businessobject.order.Basket;
import com.sap.isa.businessobject.order.DocumentListFilter;
import com.sap.isa.businesspartner.backend.boi.PartnerFunctionData;
import com.sap.isa.businesspartner.businessobject.BusinessPartner;
import com.sap.isa.businesspartner.businessobject.BusinessPartnerManager;
import com.sap.isa.businesspartner.businessobject.PartnerFunctionBase;
import com.sap.isa.businesspartner.businessobject.HierarchyKey;
import com.sap.isa.core.SessionConst;
import com.sap.isa.core.UserSessionData;
import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.core.logging.LogUtil;
import com.sap.isa.core.util.JspUtil;
import com.sap.isa.core.util.WebUtil;
import com.sap.isa.core.util.table.ResultData;
import com.sap.isa.core.util.table.Table;
import com.sap.isa.core.util.table.TableRow;
import com.sap.isa.core.util.table.ResultData.UnknownColumnNameException;
import com.sap.isa.isacore.DocumentHandler;
import com.sap.isa.isacore.ManagedDocumentLargeDoc;
import com.sap.isa.isacore.action.ActionConstantsBase;
import com.sap.isa.isacore.action.order.SalesDocumentRemoveAction;
import com.sap.isa.maintenanceobject.businessobject.GSProperty;
import com.sap.isa.user.permission.ECoActionPermission;

/**
 * The class GenericSearchUI.java provides method and variables to avoid 
 * java programming on the <code>appbase/genericsearch.jap</code>.
 *
 * @author SAP
 * @version 1.0
 **/
public class GenericSearchUIDynamicContent  
             implements GenericSearchUIDynamicContentData {

    // Content handling 
    protected HttpServletRequest request;
    protected PageContext pageContext;
    protected UserSessionData userSessionData;
    protected JspWriter writer;
    protected Map searchRequestMemory;
    

    /**
     * reference to the IsaLocation
     * @see com.sap.isa.core.logging.IsaLocation
     */
    protected static IsaLocation log = IsaLocation.
          getInstance(GenericSearchBaseUI.class.getName());

    /**
     * Constructor for GenericSearchUI.
     * @param pageContext
     */ 
    public GenericSearchUIDynamicContent() {

    }

    /***
     * Initialize class. Necessary since reflection api doesn't support 
     * instantiation of classes with arguments.
     * @param pageContext
     */
    public void init(PageContext pageContext) {
        
        this.pageContext = pageContext;
       
        // get the request
        request = (HttpServletRequest)pageContext.getRequest();
        // get user session data object
        userSessionData =  UserSessionData.getUserSessionData(pageContext.getSession());
        // get JSP writer to allow output on JSP directly
        writer = pageContext.getOut();
        // get Search Request Memory from Session Context
        searchRequestMemory = getSearchRequestMemory(request, userSessionData);
    }
    /**
     * Check for the user Permission for the Creation of different Documents. <br>
     * @param String document type for which permission is asked.
     * @param String Permission which is to check (ie. DocumentListFilterData.ACTION_READ)
     * @return boolean True/False.
     */
    protected boolean checkPermissions(String docType, String permission) {
        boolean retVal = false;
        Boolean[] checkResults = null;
        try {
            User user = null;
            if (userSessionData != null) {
                BusinessObjectManager bom = (BusinessObjectManager) userSessionData.getBOM(BusinessObjectManager.ISACORE_BOM);
                user = bom.getUser();
            }
            ECoActionPermission[] docPermissions = new ECoActionPermission[] {new ECoActionPermission(docType, permission)};       
            checkResults = user.hasPermissions(docPermissions);
            if (checkResults != null  &&  checkResults[0] != null  &&  checkResults[0].booleanValue()) {
                retVal = true;
            }
        } catch (CommunicationException comEX) {
        	if (log.isDebugEnabled()) {
        		log.debug("Error ", comEX);
        	}
        }
        return retVal; 
    }
    
    /**
     * Return the Screen name which describes the search criteria in the XML file
     * @return String screen name
     */
    protected String getScreenName(HttpServletRequest request, UserSessionData userSessionData) {
        String searchToDisplay = request.getParameter(GenericSearchUIData.RC_SEARCHCRITERASCREENNAME);
        if (searchToDisplay == null  ||  searchToDisplay.length() <= 0) {
            searchToDisplay = (String)request.getAttribute(GenericSearchUIData.RC_SEARCHCRITERASCREENNAME);
        }
        if (searchToDisplay == null  ||  searchToDisplay.length() <= 0) {
            DocumentHandler documentHandler = (DocumentHandler)userSessionData.getAttribute(SessionConst.DOCUMENT_HANDLER);
            if (documentHandler != null) {
                searchToDisplay = documentHandler.getOrganizerParameter(GenericSearchUIData.RC_SEARCHCRITERASCREENNAME);
            }
        }
        return searchToDisplay;
    }
    /**
     * Return the memory object containing the last search request. This can be used
     * to re-build the search screen the way it has been send before. 
     * @return Map Containing the last search request as key - value pairs
     */
    protected Map getSearchRequestMemory(HttpServletRequest request, UserSessionData userSessionData) {
        return ( (Map)userSessionData.getAttribute((GenericSearchUIData.SC_SEARCHREQUEST_MEMORY + getScreenName(request, userSessionData))) );
    }
    /**
     * Return Order process types. By using 
     * Attribute ID <b>screlm</b> the appropriated Property 
     * will be retrieved from pageContext
     * @return Table including all order process types
     */
    public Table getOrderProcessTypes() {
        // check for missing context
        Shop shop = null;;
        if (userSessionData != null) {
            // get BOM
            BusinessObjectManager bom = (BusinessObjectManager) userSessionData.getBOM(BusinessObjectManager.ISACORE_BOM);
            // now I can read the shop
            shop = bom.getShop();
        }
        GSProperty pty = (GSProperty)pageContext.getAttribute("screlm");
        Table options = GenericSearchUIFactory.getUIReturnTable(GenericSearchUIFactory.TYPE_RETURNTABLE_SELECTBOXOPTION);
        if (shop != null) {
        	//Note 1435714: Check for display authorizations
        	ResultData prcTypes = shop.getProcessTypesForDisplay();
            if (prcTypes != null) {
                prcTypes.first();
                TableRow optRow = null;
                // Only create Table if more than 1 process type exists
                if (prcTypes.getNumRows() > 1) {
                    // Write out blank line to allow selection without a process type
                    optRow = options.insertRow();
                    optRow.setValue("VALUE", "");
                    optRow.setValue("DESCRIPTION", WebUtil.translate(pageContext, "gs.of.type.any", null));
                    while (! prcTypes.isAfterLast()) {
                        optRow = options.insertRow();
                        // Process type is always included in the first Column (1)
                        String prcType = prcTypes.getString(1);
                        // Process type Description is always included in the second Column (2)
                        String prcTypeDsc = prcTypes.getString(2);
                        optRow.setValue("VALUE", prcType);
                        optRow.setValue("DESCRIPTION", prcTypeDsc);
                        // next row
                        prcTypes.next();
                    }
                }                
            }
        }
        return options;
    }
    /**
     * Return Quotation process types. By using 
     * Attribute ID <b>screlm</b> the appropriated Property 
     * will be retrieved from pageContext
     * @return Table including all quotation process types
     */
    public Table getQuotationProcessTypes() {
        // check for missing context
        Shop shop = null;;
        if (userSessionData != null) {
            // get BOM
            BusinessObjectManager bom = (BusinessObjectManager) userSessionData.getBOM(BusinessObjectManager.ISACORE_BOM);
            // now I can read the shop
            shop = bom.getShop();
        }
        GSProperty pty = (GSProperty)pageContext.getAttribute("screlm");
        Table options = GenericSearchUIFactory.getUIReturnTable(GenericSearchUIFactory.TYPE_RETURNTABLE_SELECTBOXOPTION);
        if (shop != null) {
            //Note 1435714: Use display authorizations
        	ResultData prcTypes = shop.getQuotationProcessTypesForDisplay();
            if (prcTypes != null) {
                prcTypes.first();
                TableRow optRow = null;
                // Only create Table if more than 1 process type exists
                if (prcTypes.getNumRows() > 1) {
                    // Write out blank line to allow selection without a process type
                    optRow = options.insertRow();
                    optRow.setValue("VALUE", "");
                    optRow.setValue("DESCRIPTION", WebUtil.translate(pageContext, "gs.of.type.any", null));
                    while (! prcTypes.isAfterLast()) {
                        optRow = options.insertRow();
                        // Process type is always included in the first Column (1)
                        String prcType = prcTypes.getString(1);
                        // Process type Description is always included in the second Column (2)
                        String prcTypeDsc = prcTypes.getString(2);
                        optRow.setValue("VALUE", prcType);
                        optRow.setValue("DESCRIPTION", prcTypeDsc);
                        // next row
                        prcTypes.next();
                    }
                }
            }
        }
        return options;
    }
    /**
     * Return Quotation status. The status are depending
     * of the quotation type (LEAN / EXTENDED).By using 
     * Attribute ID <b>screlm</b> the appropriated Property 
     * will be retrieved from pageContext
     * @return Table including all quotation status
     */
    public Table getQuotationStatus() {
        // check for missing context
        Shop shop = null;;
        if (userSessionData != null) {
            // get BOM
            BusinessObjectManager bom = (BusinessObjectManager) userSessionData.getBOM(BusinessObjectManager.ISACORE_BOM);
            // now I can read the shop
            shop = bom.getShop();
        }
        GSProperty pty = (GSProperty)pageContext.getAttribute("screlm");
        Table options = GenericSearchUIFactory.getUIReturnTable(GenericSearchUIFactory.TYPE_RETURNTABLE_SELECTBOXOPTION);
        if (shop != null) {
            TableRow optRow = options.insertRow();
            if (shop.isQuotationExtended()) {
                optRow.setValue("VALUE", "");
                optRow.setValue("SELECTED", true);        // Show as default
                optRow.setValue("DESCRIPTION", WebUtil.translate(pageContext, "gs.att.sta.any", null));

                // Status for Quotation type EXTENDED
                optRow = options.insertRow();
                optRow.setValue("VALUE", "OPEN");
                optRow.setValue("DESCRIPTION", WebUtil.translate(pageContext, "b2b.status.shuffler.key2val4", null));

                optRow = options.insertRow();
                optRow.setValue("VALUE", "RELEASED");
                optRow.setValue("DESCRIPTION", WebUtil.translate(pageContext, "b2b.status.shuffler.key2val5", null));

                optRow = options.insertRow();
                optRow.setValue("VALUE", "ACCEPTED");
                optRow.setValue("DESCRIPTION", WebUtil.translate(pageContext, "b2b.status.shuffler.key2val6", null));

                optRow = options.insertRow();
                optRow.setValue("VALUE", "COMPLETED");
                optRow.setValue("DESCRIPTION", WebUtil.translate(pageContext, "b2b.status.shuffler.key2val7", null));
            } else {
                optRow.setValue("VALUE", "LEANALL");
                optRow.setValue("SELECTED", true);        // Show as default
                optRow.setValue("DESCRIPTION", WebUtil.translate(pageContext, "b2b.status.shuffler.key2val3", null));
                // Status for Quotation type LEAN
                optRow = options.insertRow();
                optRow.setValue("VALUE", "I1002");
                optRow.setValue("DESCRIPTION", WebUtil.translate(pageContext, "b2b.status.shuffler.key2val1", null));

                optRow = options.insertRow();
                optRow.setValue("VALUE", "I1005");
                optRow.setValue("DESCRIPTION", WebUtil.translate(pageContext, "b2b.status.shuffler.key2val2", null));
            }
        }
        return options;
    }
    
	public Table getCountryList() {

		// check for missing context
		Shop shop = null;;
		if (userSessionData != null) {
			// get BOM
			BusinessObjectManager bom = (BusinessObjectManager) userSessionData.getBOM(BusinessObjectManager.ISACORE_BOM);
			// now I can read the shop
			shop = bom.getShop();
		}
		
		GSProperty pty = (GSProperty)pageContext.getAttribute("screlm");
		String countryID = (String)request.getParameter("rc_custselectcountry");
								
		// if countryID == null, set countryID to default country
		if (countryID == null) {
			countryID = shop.getDefaultCountry();
		}
									
		// set country in searchrequest Memory, so that "selected" does not need to be coded
		// -> serchrequest memory not available on country change!

		if (searchRequestMemory != null) {
			searchRequestMemory.put("rc_custselectcountry", countryID);						         
	    }			
												
		Table options = GenericSearchUIFactory.getUIReturnTable(GenericSearchUIFactory.TYPE_RETURNTABLE_SELECTBOXOPTION);
		
		//String defaultCountryID = shop.getDefaultCountry();
		
		if (shop != null) {
			ResultData listOfCountries = shop.getCountryList();
			if (listOfCountries != null) {
				listOfCountries.first();
				TableRow optRow = null;
				// Only create Table if more than 1 process type exists
				
				int counter = 0;
				
				if (listOfCountries.getNumRows() > 1) {
					// Write out blank line to allow selection without a process type
					optRow = options.insertRow();
					optRow.setValue("VALUE", "");
					optRow.setValue("DESCRIPTION", WebUtil.translate(pageContext, "bupa.search.any.region", null));
					
					if (countryID == null || countryID.equalsIgnoreCase("")) {
						
						optRow.setValue("SELECTED", true);
						
					} else {
					
						optRow.setValue("SELECTED", false);	
						
					}
					
					while (! listOfCountries.isAfterLast()) {
						
						counter++;
						
						optRow = options.insertRow();
						// Process type is always included in the first Column (1)
						String country = listOfCountries.getString(1);
						// Process type Description is always included in the second Column (2)
						String countryDesc = listOfCountries.getString(2);
						optRow.setValue("VALUE", country);
						optRow.setValue("DESCRIPTION", countryDesc);
						
						// check only if country!=null
						if ((countryID != null) && (countryID.length() > 0)) { 
							
							if (country.equalsIgnoreCase(countryID)) {
							
							optRow.setValue("SELECTED", true);
							
						} else {
							
							optRow.setValue("SELECTED", false);
							
						}
						}
						
						if (counter > 15) break;
						
						// next row
						listOfCountries.next();
					}
				} 
				
			}
		}
		
		return options;
				
	}
    

	public Table getRegionList() throws CommunicationException {
				
		boolean isRegionRequired = false;
		ResultData listOfRegions = null;
		Table options = GenericSearchUIFactory.getUIReturnTable(GenericSearchUIFactory.TYPE_RETURNTABLE_SELECTBOXOPTION);
		
		// check for missing context
		Shop shop = null;;
		if (userSessionData != null) {
			// get BOM
			BusinessObjectManager bom = (BusinessObjectManager) userSessionData.getBOM(BusinessObjectManager.ISACORE_BOM);
			// now I can read the shop
			shop = bom.getShop();
		}
		
		// get current country!
		String countryID = (String)request.getParameter("rc_custselectcountry");
		
		if (searchRequestMemory != null) {
			String test = (String)searchRequestMemory.get("rc_custselectcountry");        
		}
		
		
		
		
		if (countryID == null) {
			
			countryID = shop.getDefaultCountry();
			
		}
		
		
		if (shop != null) {
					ResultData listOfCountries = shop.getCountryList();
					if (listOfCountries != null) {
						// used to find out if the default country needs regions to complete the address
						
						listOfCountries.beforeFirst();
						while (listOfCountries.next()){
							if (listOfCountries.getString("ID").equalsIgnoreCase(countryID)){
								isRegionRequired = listOfCountries.getBoolean("REGION_FLAG");
							}
						}

						// get the list of regions in case the region flag is set
						if (isRegionRequired){
							listOfRegions = shop.getRegionList(countryID);
						}

																
					}
		}
		
		//build output table
		if (listOfRegions != null) {
			listOfRegions.first();
			TableRow optRow = null;
			// Only create Table if more than 1 process type exists
			
			int counter = 0;
			
			if (listOfRegions.getNumRows() > 1) {
				
								
				// Write out blank line to allow selection without a process type
				optRow = options.insertRow();
				optRow.setValue("VALUE", "");
				optRow.setValue("DESCRIPTION", WebUtil.translate(pageContext, "bupa.search.any.region", null));
				while (! listOfRegions.isAfterLast()) {
					
					counter++;
					
					optRow = options.insertRow();
					// Process type is always included in the first Column (1)
					String region = listOfRegions.getString(1);
					// Process type Description is always included in the second Column (2)
					String regionDesc = listOfRegions.getString(2);
					optRow.setValue("VALUE", region);
					optRow.setValue("DESCRIPTION", regionDesc);
					
					//if (counter > 15) break;
					
					// next row
					listOfRegions.next();
				}
			} 
				
		}
		
		return options;
				
	}
    
    /**
     * Return Attributes to point to a Sales Document's detail. By using 
     * Attribute ID <b>singlerow</b> the appropriated ResultData object 
     * will be retrieved from pageContext. ID is defined in the genericsearch.jsp.
     * @return String containing the attributes
     */
    public String buildAttributesForSalesDocumentDetails() {
        // check for missing context
        Shop shop = null;;
        if (userSessionData != null) {
            // get BOM
            BusinessObjectManager bom = (BusinessObjectManager) userSessionData.getBOM(BusinessObjectManager.ISACORE_BOM);
            // now I can read the shop
            shop = bom.getShop();
        }
        ResultData rsData = (ResultData)pageContext.getAttribute("singlerow");
		String prodId = (String)request.getParameter("rc_product_id");  
		if (prodId == null || "".equals(prodId)) {
			// After sorting the request parameter is no longer available, check search request memory
			prodId = (searchRequestMemory != null ? (String)searchRequestMemory.get("rc_product_id") : null);      
		}        
        String objecttypeIN = "";
        String objecttypeOUT = "";
        if (searchRequestMemory != null) {
            objecttypeIN = (String)searchRequestMemory.get("rc_documenttypes");        
        } else {
            objecttypeIN = (String)request.getParameter("rc_documenttypes");
        }
        if ("ORDER".equals(objecttypeIN)  ||  "BACKORDER".equals(objecttypeIN)) {
            objecttypeOUT = DocumentListFilter.SALESDOCUMENT_TYPE_ORDER;
        } else if ("QUOTATION".equals(objecttypeIN)) {
            objecttypeOUT = DocumentListFilter.SALESDOCUMENT_TYPE_QUOTATION;
        } else {
            // Ordertemplates
            objecttypeOUT = DocumentListFilter.SALESDOCUMENT_TYPE_ORDERTEMPLATE;
        }
        StringBuffer strB = new StringBuffer("techkey=");
        strB.append(rsData.getRowKey().toString());
        strB.append("&object_id=");
        try {
            strB.append(rsData.getString("OBJECT_ID"));
        } catch (UnknownColumnNameException ucnEx) {
            try {
                // OBJECT_ID not found try OBJECT_ID_HDR which is used for items
                strB.append(rsData.getString("OBJECT_ID_HDR"));
            } catch (UnknownColumnNameException ucnEx2) {
				if (log.isDebugEnabled()) {
					log.debug("Error ", ucnEx2);
				}
            }
        }
        strB.append("&objecttype=");
        strB.append(objecttypeOUT);
        strB.append("&processtype=");
        try {
            strB.append(rsData.getString("PROCESS_TYPE"));
        } catch (UnknownColumnNameException ucnEx) {
			if (log.isDebugEnabled()) {
				log.debug("Error ", ucnEx);
			}
        }
        // In case of large document handling, restrict document details on the search products
        if (prodId != null  &&  ! prodId.equals("") &&  shop.getLargeDocNoOfItemsThreshold() > 0) {
            strB.append("&" + ActionConstantsBase.RK_SEARCH_ITEM_PROPERTY + "=" + ManagedDocumentLargeDoc.ITEM_PROPERTY_PRODUCT);
            strB.append("&" + ActionConstantsBase.RK_SEARCH_ITEM_PROPERTY_LOW_VALUE + "=" + prodId);
            strB.append("&fromDocumentSearch=true");
            strB.append("&"+GenericSearchUIData.RC_SEARCHCRITERASCREENNAME+"=SearchCriteria_B2B_Sales_OrderStatus_Items");
        }
        return strB.toString();        
    }
    
	/**
	 * Return Attributes to point to an items detail. By using 
	 * Attribute ID <b>singlerow</b> the appropriated ResultData object 
	 * will be retrieved from pageContext. ID is defined in the genericsearch.jsp.
	 * @return String containing the attributes
	 */
	public String buildAttributesForItemDetails() {
		// check for missing context
		Shop shop = null;;
		if (userSessionData != null) {
			// get BOM
			BusinessObjectManager bom = (BusinessObjectManager) userSessionData.getBOM(BusinessObjectManager.ISACORE_BOM);
			// now I can read the shop
			shop = bom.getShop();
		}
		ResultData rsData = (ResultData)pageContext.getAttribute("singlerow");
		String objecttypeIN = "";
		String objecttypeOUT = "";
		if (searchRequestMemory != null) {
			objecttypeIN = (String)searchRequestMemory.get("rc_documenttypes");        
		} else {
			objecttypeIN = (String)request.getParameter("rc_documenttypes");
		}
		if ("ORDERITM".equals(objecttypeIN)) {
			objecttypeOUT = DocumentListFilter.SALESDOCUMENT_TYPE_ORDER;
		} else if ("QUOTATIONITM".equals(objecttypeIN)) {
			objecttypeOUT = DocumentListFilter.SALESDOCUMENT_TYPE_QUOTATION;
		} else {
			// Ordertemplates
			objecttypeOUT = DocumentListFilter.SALESDOCUMENT_TYPE_ORDERTEMPLATE;
		}
		StringBuffer strB = new StringBuffer("techkey=");
		strB.append(rsData.getRowKey().toString());
		strB.append("&object_id=");
		try {
            // try OBJECT_ID_HDR which is used for items
			strB.append(rsData.getString("OBJECT_ID_HDR"));
		} 
		catch (UnknownColumnNameException ucnEx) {
			if (log.isDebugEnabled()) {
				log.debug("Error ", ucnEx);
			}
		}
		
		strB.append("&objecttype=");
		strB.append(objecttypeOUT);
		strB.append("&processtype=");
		try {
			strB.append(rsData.getString("PROCESS_TYPE"));
		} catch (UnknownColumnNameException ucnEx) {
			if (log.isDebugEnabled()) {
				log.debug("Error ", ucnEx);
			}
		}
        
        strB.append("&" + ActionConstantsBase.RK_SEARCH_ITEM_PROPERTY + "=" + ManagedDocumentLargeDoc.ITEM_PROPERTY_NUMBERINT);
        strB.append("&" + ActionConstantsBase.RK_SEARCH_ITEM_PROPERTY_LOW_VALUE + "=" + rsData.getString("NUMBER_INT"));
        strB.append("&fromDocumentSearch=true");
        strB.append("&"+GenericSearchUIData.RC_SEARCHCRITERASCREENNAME+"=SearchCriteria_B2B_Sales_OrderStatus_Items");

		return strB.toString();        
	}    
    
    /**
     * Return Attributes to point to a <b>B2C</b> Sales Document's detail.
     * @return String containing the attributes
     */
    public String buildAttributesForSalesDocumentDetailsB2C() {
        ResultData rsData = (ResultData)pageContext.getAttribute("singlerow");
        StringBuffer strB = new StringBuffer("techkey=");
        strB.append(rsData.getRowKey().toString());
        strB.append("&object_id=");
        strB.append(rsData.getString("OBJECT_ID"));
        strB.append("&objecttype=");
        try {
            String docType = rsData.getString("DOCUMENTTYPE");
            if (docType != null && docType.length() > 0) {
                strB.append(docType);
            } else { 
                strB.append(DocumentListFilter.SALESDOCUMENT_TYPE_ORDER);
            }
        } catch (UnknownColumnNameException ucnEx) {
            strB.append(DocumentListFilter.SALESDOCUMENT_TYPE_ORDER);
        }
        return strB.toString();        
    }
    /**
     * Return Attributes to remove a <b>B2C</b> Sales Document.
     * @return String containing the attributes
     */
    public String buildAttributesForSalesDocumentRemoveB2C() {
		BusinessObjectManager bom = (BusinessObjectManager) userSessionData.getBOM(BusinessObjectManager.ISACORE_BOM);
        ResultData rsData = (ResultData)pageContext.getAttribute("singlerow");
		StringBuffer strB = new StringBuffer();
		strB.append("javascript:removeSavedBasket('");
        strB.append(rsData.getRowKey().toString());
        strB.append("','");
        String objectId = rsData.getString("OBJECT_ID");
        strB.append(objectId);
        strB.append("','");
        try {
            String docType = rsData.getString("DOCUMENTTYPE");
            if (docType != null && docType.length() > 0) {
                strB.append(docType);
            } else { 
                strB.append(DocumentListFilter.SALESDOCUMENT_TYPE_ORDER);
            }
        } catch (UnknownColumnNameException ucnEx) {
            strB.append(DocumentListFilter.SALESDOCUMENT_TYPE_ORDER);
        }
        strB.append("','delete',");
		if ((bom.getBasket() != null) && (!bom.getBasket().getTechKey().isInitial()) 
		     && (bom.getBasket().getTechKey().toString().equals(objectId))) {
			strB.append("true);");
		} else {
			strB.append("false);");
		}        
        return strB.toString();
        
    }

    /**
     * Return Attributes to point to a Contracts's detail.
     * @return String containing the attributes
     */
    public String buildAttributesForContractDocumentDetails() {
        ResultData rsData = (ResultData)pageContext.getAttribute("singlerow");
        StringBuffer strB = new StringBuffer();
        String status = rsData.getString("STATUS_SYSTEM");
        if ("I1004".equals(status)) {
            // status RELEASED
            strB.append("contractselect.do?contractSelected=");
        } else {
            // any other status
            strB.append("preparenegotiatedcontract.do?contractSelected=");
        }
        strB.append(rsData.getString("GUID"));
        return strB.toString();
    }
    /**
     * Return Link to delete a order template
     * @return String representing a link
     */
    public String buildAttributesForOrderTemplateDeletion() {
        StringBuffer strB = new StringBuffer();
        if (checkPermissions(DocumentListFilterData.SALESDOCUMENT_TYPE_ORDERTEMPLATE, DocumentListFilterData.ACTION_CHANGE)) {
            ResultData rsData = (ResultData)pageContext.getAttribute("singlerow");

            strB.append("javascript:removeSalesDocument('");
            strB.append(rsData.getRowKey().toString());
            strB.append("','ordertemplate','");
            strB.append(SalesDocumentRemoveAction.RK_REMOVETYPE_ORDERTEMPLATE);
            strB.append("','");
            try {
                strB.append(rsData.getString("OBJECT_ID"));
            } catch (ResultData.UnknownColumnNameException ucnEx) {
				if (log.isDebugEnabled()) {
					log.debug("Error ", ucnEx);
				}
            }
            strB.append("');");
        }
        return strB.toString();
    }
    /**
     * Return Link to remove a quotation
     * @return String representing a link
     */
    public String buildAttributesForQuotationRemoving() {
        StringBuffer strB = new StringBuffer();
        ResultData rsData = (ResultData)pageContext.getAttribute("singlerow");
        String status = "";
        try {
            status = rsData.getString("STATUS_SYSTEM");
        } catch (ResultData.UnknownColumnNameException ucnEx) {
            status = rsData.getString("STATUS");
        }
        if (checkPermissions(DocumentListFilterData.SALESDOCUMENT_TYPE_QUOTATION, DocumentListFilterData.ACTION_CHANGE)
            &&  ( status.indexOf(DocumentListFilterData.SALESDOCUMENT_STATUS_OPEN) >= 0  || 
                  status.indexOf(DocumentListFilterData.SALESDOCUMENT_STATUS_RELEASED) >= 0) ) {
            strB.append("javascript:removeSalesDocument('");
            strB.append(rsData.getRowKey().toString());
            strB.append("','quotation','");
            strB.append(SalesDocumentRemoveAction.RK_REMOVETYPE_QUOTATION);
            strB.append("','");
            try {
                strB.append(rsData.getString("OBJECT_ID"));
            } catch (ResultData.UnknownColumnNameException ucnEx) {
				if (log.isDebugEnabled()) {
					log.debug("Error ", ucnEx);
				}
            }
            strB.append("');");
        }
        return strB.toString();
    }
    /**
     * Return a Trashcan icon
     */
    public Table getOrderTemplateDeleteIcon() {
        ResultData rsData = (ResultData)pageContext.getAttribute("singlerow");
        Table fieldT = GenericSearchUIFactory.getUIReturnTable(GenericSearchUIFactory.TYPE_RETURNTABLE_OUTPUTFIELD);
        // Fill Table        
        TableRow tabrow = fieldT.insertRow();
        if (checkPermissions(DocumentListFilterData.SALESDOCUMENT_TYPE_ORDERTEMPLATE, DocumentListFilterData.ACTION_CHANGE)) {
            tabrow.setValue("TYPE", "ICON");
            tabrow.setValue("ICONPATH",  "b2b/mimes/images/trash.gif");
            tabrow.setValue("ICONWIDTH", "11");
            tabrow.setValue("ICONHIGHT", "14");
            tabrow.setValue("ICONBORDER", "0");
            tabrow.setValue("ICONTITLE", WebUtil.translate(pageContext, "status.docmod.ordertemplate", null));
        } else {
            // User has not the permission to delete an order template, so just output a space
            tabrow.setValue("TYPE", "FIELD");
            tabrow.setValue("VALUE", "");
        }
        return fieldT;

    }
    /**
     * Return a Trashcan icon 
     */
    public Table getQuotationRemovingIcon() {
        ResultData rsData = (ResultData)pageContext.getAttribute("singlerow");
        Table fieldT = GenericSearchUIFactory.getUIReturnTable(GenericSearchUIFactory.TYPE_RETURNTABLE_OUTPUTFIELD);
        // Fill Table        
        TableRow tabrow = fieldT.insertRow();
        String status = "";
        try {
            status = rsData.getString("STATUS_SYSTEM");
        } catch (ResultData.UnknownColumnNameException ucnEx) {
            status = rsData.getString("STATUS");
        }
        if (checkPermissions(DocumentListFilterData.SALESDOCUMENT_TYPE_QUOTATION, DocumentListFilterData.ACTION_CHANGE)
            &&  ( status.indexOf(DocumentListFilterData.SALESDOCUMENT_STATUS_OPEN) >= 0  || 
                  status.indexOf(DocumentListFilterData.SALESDOCUMENT_STATUS_RELEASED) >= 0) ) {
            tabrow.setValue("TYPE", "ICON");
            tabrow.setValue("ICONPATH",  "b2b/mimes/images/trash.gif");
            tabrow.setValue("ICONWIDTH", "11");
            tabrow.setValue("ICONHIGHT", "14");
            tabrow.setValue("ICONBORDER", "0");
            tabrow.setValue("ICONTITLE", WebUtil.translate(pageContext, "status.docmod.quotation", null));
        } else {
            // User has not the permission to delete an order template, so just output a space
            tabrow.setValue("TYPE", "FIELD");
            tabrow.setValue("VALUE", "");
        }
        return fieldT;

    }
    /**
     * Return Link to point to a window show Partner Address details
     * @return String representing a link
     */
    public String buildShowPartnerLinkSales() {
           ResultData rsData = (ResultData)pageContext.getAttribute("singlerow");
           StringBuffer strB = new StringBuffer();
           strB.append("javascript:showSoldTo('");
           try {
               strB.append(rsData.getString("SOLD_TO_PARTY_GUID"));
           } catch (ResultData.UnknownColumnNameException ucnEx) {
			if (log.isDebugEnabled()) {
				log.debug("Error ", ucnEx);
			}
           }
           try {
               strB.append(rsData.getString("PAYERS_GUID"));
           } catch (ResultData.UnknownColumnNameException ucnEx) {
			if (log.isDebugEnabled()) {
				log.debug("Error ", ucnEx);
			}
           }
           strB.append("');");
           return strB.toString();
    }
    
	/**
	  * Return Link to point to a window confirming user that saved basket will be loaded
	  * @return String representing a link
	  */
	 public String buildAttributesForSalesDocumentDetailsMiniBasketB2C() {
		StringBuffer strB = new StringBuffer();
		if (userSessionData != null) {
            // get BOM
            BusinessObjectManager bom = (BusinessObjectManager) userSessionData.getBOM(BusinessObjectManager.ISACORE_BOM);


			strB.append("javascript:loadSavedBasket('");
			ResultData rsData = (ResultData)pageContext.getAttribute("singlerow");
			strB.append(rsData.getRowKey().toString());
			strB.append("','");
			strB.append(rsData.getString("OBJECT_ID"));
			strB.append("','");
			
			try {
				String docType = rsData.getString("DOCUMENTTYPE");
				if (docType != null && docType.length() > 0) {
					strB.append(docType);
				} else { 
					strB.append(DocumentListFilter.SALESDOCUMENT_TYPE_ORDER);
				}
			} catch (UnknownColumnNameException ucnEx) {
				strB.append(DocumentListFilter.SALESDOCUMENT_TYPE_ORDER);
			}
			strB.append("',");
			if ((bom.getBasket() == null) || bom.getBasket().getTechKey().isInitial() 
			     || bom.getBasket().getItems().size() == 0 ) 
			{
				strB.append("false);");
			} else {
				strB.append("true);");
			}
		}
		return strB.toString();	
	 }    
	public String buildShowCustomerDetails() {
		   ResultData rsData = (ResultData)pageContext.getAttribute("singlerow");
		   StringBuffer strB = new StringBuffer();
		   strB.append("javascript:showSoldTo('");
		   try {
			   strB.append(rsData.getString("BPARTNER_GUID"));
		   } catch (ResultData.UnknownColumnNameException ucnEx) {
			if (log.isDebugEnabled()) {
				log.debug("Error ", ucnEx);
			}
		   }
//		   try {
//			   strB.append(rsData.getString("PAYERS_GUID"));
//		   } catch (ResultData.UnknownColumnNameException ucnEx) {
//		   }
		   strB.append("');");
		   return strB.toString();
	}    
    
    
    /**
     * Return Attributes to point to a Billing Document's detail. By using 
     * Attribute ID <b>singlerow</b> the appropriated ResultData object 
     * will be retrieved from pageContext. ID is defined in the genericsearch.jsp.
     * @return String containing the attributes
     */
    public String buildAttributesForBillingDocumentDetails() {
        // check for missing context
        Shop shop = null;;
        if (userSessionData != null) {
            // get BOM
            BusinessObjectManager bom = (BusinessObjectManager) userSessionData.getBOM(BusinessObjectManager.ISACORE_BOM);
            // now I can read the shop
            shop = bom.getShop();
        }
        ResultData rsData = (ResultData)pageContext.getAttribute("singlerow");
        // Add Product ID to link if had been used for searching
        String prodId = (String)request.getParameter("rc_product_id");
		if (prodId == null || "".equals(prodId)) {
			// After sorting, the request parameter is no longer available, check search request memory
			prodId = (searchRequestMemory != null ? (String)searchRequestMemory.get("rc_product_id") : null);      
		}
        StringBuffer strB = new StringBuffer("techkey=");
//      GUID will not fit into import parameter of fm CRM_ISA_INVOICE_DETAIL_GET (BILLINGDOC) !!
        if (shop.isBillingSelectionCRM()  ||  shop.isBillingSelectionR3CRM()) {
            strB.append(rsData.getString("HEADNO_EXT"));
        }
        if (shop.isBillingSelectionR3()) {
            strB.append(rsData.getString("BILLINGDOC"));
        }
        strB.append("&object_id=");
        if (shop.isBillingSelectionCRM()) {    
            strB.append(rsData.getString("HEADNO_EXT"));
        }
        if (shop.isBillingSelectionR3()) {    
            strB.append(rsData.getString("BILLINGDOC"));
        }
        if ( ! "CRM".equals(rsData.getString("OBJECTS_ORIGIN"))) { 
            try {
                String payersGuid = rsData.getString("PAYERS_GUID");
                if ( ! shop.isBpHierarchyEnable()) {     // Note 1403135
                	BusinessObjectManager bom = (BusinessObjectManager) userSessionData.getBOM(BusinessObjectManager.ISACORE_BOM);
                	BusinessPartnerManager buPaMa = bom.createBUPAManager();
                	BusinessPartner buPa = buPaMa.getDefaultBusinessPartner(PartnerFunctionBase.SOLDTO);
                	payersGuid = buPa.getTechKey().toString();
                }
                strB.append("&sold_to="); // Adding sold-to is actually only necessary if partner hierarchies are in involved
                strB.append(payersGuid);
            } catch (ResultData.UnknownColumnNameException ucnEx) {
//				$JL-EXC$
            }
        }
        strB.append("&objects_origin=");
        strB.append(rsData.getString("OBJECTS_ORIGIN"));
        strB.append("&billing=X");
        if (prodId != null  &&  ! prodId.equals("")) {
            strB.append("&" + ActionConstantsBase.RK_SEARCH_ITEM_PROPERTY + "=" + ManagedDocumentLargeDoc.ITEM_PROPERTY_PRODUCT);
            strB.append("&" + ActionConstantsBase.RK_SEARCH_ITEM_PROPERTY_LOW_VALUE + "=" + prodId);
            strB.append("&fromDocumentSearch=true");
            strB.append("&"+GenericSearchUIData.RC_SEARCHCRITERASCREENNAME+"=SearchCriteria_B2B_Billing_BillingStatus_Items");
        }

        return strB.toString();        
    }
    
	/**
	 * Return Attributes to point to a Sales Document's detail. By using 
	 * Attribute ID <b>singlerow</b> the appropriated ResultData object 
	 * will be retrieved from pageContext. ID is defined in the genericsearch.jsp.
	 * @return String containing the attributes
	 */
	public String buildAttributesForCustomerTransfer() {
		ResultData rsData = (ResultData)pageContext.getAttribute("singlerow");
		String objecttypeIN = "";
		String objecttypeOUT = "";
//		if (searchRequestMemory != null) {
//			objecttypeIN = (String)searchRequestMemory.get("rc_documenttypes");        
//		} else {
//			objecttypeIN = (String)request.getParameter("rc_documenttypes");
//		}
//		if ("ORDER".equals(objecttypeIN)) {
//			objecttypeOUT = DocumentListFilter.SALESDOCUMENT_TYPE_ORDER;
//		} else if ("QUOTATION".equals(objecttypeIN)) {
//			objecttypeOUT = DocumentListFilter.SALESDOCUMENT_TYPE_QUOTATION;
//		} else {
//			// Ordertemplates
//			objecttypeOUT = DocumentListFilter.SALESDOCUMENT_TYPE_ORDERTEMPLATE;
//		}
		StringBuffer strB = new StringBuffer("TechKey=");
		strB.append(rsData.getRowKey().toString());
//		strB.append("&object_id=");
//		strB.append(rsData.getString("OBJECT_ID"));
//		strB.append("&objecttype=");
//		strB.append(objecttypeOUT);
//		strB.append("&processtype=");
//		strB.append(rsData.getString("PROCESS_TYPE"));
		return strB.toString();        
	}
    
    
    
    /**
     * Return Dealer Family List. First entry in the list will be an option allowing to
     * select for all dealers in the list.
     */
    public Table getDealerList() {
        // check for missing context
        Shop shop = null;
        BusinessObjectManager bom = null;
        if (userSessionData != null) {
            // get BOM
            bom = (BusinessObjectManager) userSessionData.getBOM(BusinessObjectManager.ISACORE_BOM);
            // now I can read the shop
            shop = bom.getShop();
        }
        GSProperty pty = (GSProperty)pageContext.getAttribute("screlm");
        Table options = GenericSearchUIFactory.getUIReturnTable(GenericSearchUIFactory.TYPE_RETURNTABLE_SELECTBOXOPTION);
        ResultData dealerFamilyList = null;
        BusinessPartnerManager buPaMa = bom.createBUPAManager();
        BusinessPartner buPa = buPaMa.getDefaultBusinessPartner(PartnerFunctionData.SOLDTO);
        if (buPa != null&& shop.isBpHierarchyEnable()) {
		try { 
			HierarchyKey hierarchy = buPaMa.getPartnerHierachy();
			dealerFamilyList = buPa.getPartnerFromHierachy(hierarchy);
          } catch (CommunicationException comEx){
              log.error(LogUtil.APPS_COMMON_INFRASTRUCTURE, "Failed to connect to the backend", comEx);
          }
           
        }
        if (dealerFamilyList != null) {
            // Add Entry which allows a selection for all dealers
            TableRow optRow = options.insertRow();
            optRow.setValue("VALUE", "ALL");
            optRow.setValue("DESCRIPTION", WebUtil.translate(pageContext, "gs.attr.all.dealers", null));
            // Add Entry which represents dealer itself
            optRow = options.insertRow();
            optRow.setValue("VALUE", buPa.getId());
            optRow.setValue("SELECTED", true);
            optRow.setValue("DESCRIPTION", WebUtil.translate(pageContext, "gs.attr.myself", null));
            // Add all dealers in the family list
            dealerFamilyList.beforeFirst();
            while(!dealerFamilyList.isLast()) {
                dealerFamilyList.next();
                optRow = options.insertRow();
                //TODO Switch to SHORT_ADDR necessary ?
                optRow.setValue("VALUE", dealerFamilyList.getString("SOLDTO"));
                optRow.setValue("DESCRIPTION", dealerFamilyList.getString("NAME1"));
                //optRow.setValue("DESCRIPTION", dealerFamilyList.getString("SHORT_ADDR"));
            }
        }
        
        return options;        
    }
    /**
     * Return the NetValue and Currency as one field. Therefore the Result from Backend
     * must include the fields <code>NET_VALUE</code> and <code>DOC_CURRENCY</code>.
     */
    public Table buildBillingNetValue() {
        // check for missing context
        Shop shop = null;;
        if (userSessionData != null) {
            // get BOM
            BusinessObjectManager bom = (BusinessObjectManager) userSessionData.getBOM(BusinessObjectManager.ISACORE_BOM);
            // now I can read the shop
            shop = bom.getShop();
        }
        ResultData rsData = (ResultData)pageContext.getAttribute("singlerow");
        Table fieldT = GenericSearchUIFactory.getUIReturnTable(GenericSearchUIFactory.TYPE_RETURNTABLE_OUTPUTFIELD);
        // Fill Table        
        TableRow tabrow = fieldT.insertRow();
        tabrow.setValue("TYPE", "FIELD");
        String value = rsData.getString("NET_VALUE");
        String currency = ""; 
        if (shop.isBillingSelectionCRM()  ||  shop.isBillingSelectionR3CRM()) {
            currency = rsData.getString("DOC_CURRENCY");
        }
        if (shop.isBillingSelectionR3()) {
            currency = rsData.getString("CURRENCY");
        }
        String ffield = (value + " " + currency);
        tabrow.setValue("VALUE", ffield);
        
        return fieldT;
    }
    
    /**
    * Return the NetValue and Currency for an Item as one field. Therefore the Result from Backend
    * must include the fields <code>NET_VALUE</code> and <code>CURRENCY</code>.
    */
    public Table getItemNetValue() {
    	// check for missing context
    	Shop shop = null;;
    	if (userSessionData != null) {
          // get BOM
    	    BusinessObjectManager bom = (BusinessObjectManager) userSessionData.getBOM(BusinessObjectManager.ISACORE_BOM);
    	    // now I can read the shop
    	    shop = bom.getShop();
    	}
    	ResultData rsData = (ResultData)pageContext.getAttribute("singlerow");
    	Table fieldT = GenericSearchUIFactory.getUIReturnTable(GenericSearchUIFactory.TYPE_RETURNTABLE_OUTPUTFIELD);
    	// Fill Table        
    	TableRow tabrow = fieldT.insertRow();
    	tabrow.setValue("TYPE", "FIELD");
    	String value = rsData.getString("NET_VALUE");
    	String currency = rsData.getString("CURRENCY");

    	String ffield = (value + " " + currency);
    	tabrow.setValue("VALUE", ffield);
    		
      return fieldT;
    }
    
    /**
     * Return the formatted NetValue and Currency for an Item as one field. Therefore the Result from Backend
     * must include the fields <code>FORMATTED_NET_VALUE</code> and <code>CURRENCY</code>.
     */
     public Table getItemFormattedNetValue() {
     	// check for missing context
     	Shop shop = null;;
     	if (userSessionData != null) {
           // get BOM
     	    BusinessObjectManager bom = (BusinessObjectManager) userSessionData.getBOM(BusinessObjectManager.ISACORE_BOM);
     	    // now I can read the shop
     	    shop = bom.getShop();
     	}
     	ResultData rsData = (ResultData)pageContext.getAttribute("singlerow");
     	Table fieldT = GenericSearchUIFactory.getUIReturnTable(GenericSearchUIFactory.TYPE_RETURNTABLE_OUTPUTFIELD);
     	// Fill Table        
     	TableRow tabrow = fieldT.insertRow();
     	tabrow.setValue("TYPE", "FIELD");
     	String value = rsData.getString("FORMATTED_NET_VALUE");
     	String currency = rsData.getString("CURRENCY");

     	String ffield = ("<nobr>" + value + "&nbsp;" + currency + "</nobr>");
     	tabrow.setValue("VALUE", ffield);
     		
       return fieldT;
     }

    /**
    * Return the quantity and UOM for an Item as one field. Therefore the Result from Backend
    * must include the fields <code>FORMATTED_QTY</code> and <code>PROCESS_QTY_UNIT</code>.
    */
    public Table getItemFormattedQuantityAndUOM() {
    	// check for missing context
    	Shop shop = null;;
    	if (userSessionData != null) {
          // get BOM
    	    BusinessObjectManager bom = (BusinessObjectManager) userSessionData.getBOM(BusinessObjectManager.ISACORE_BOM);
    	    // now I can read the shop
    	    shop = bom.getShop();
    	}
    	ResultData rsData = (ResultData)pageContext.getAttribute("singlerow");
    	Table fieldT = GenericSearchUIFactory.getUIReturnTable(GenericSearchUIFactory.TYPE_RETURNTABLE_OUTPUTFIELD);
    	// Fill Table        
    	TableRow tabrow = fieldT.insertRow();
    	tabrow.setValue("TYPE", "FIELD");
    	String quantity = rsData.getString("FORMATTED_QTY");
    	String uom = rsData.getString("PROCESS_QTY_UNIT");

    	String ffield = (quantity + " " + uom);
    	tabrow.setValue("VALUE", ffield);
    		
      return fieldT;
    }
    
    /**
    * Return the documents id and the item no separated by a /. Therefore the Result from Backend
    * must include the fields <code>OBJECT_ID_HDR</code> and <code>NUMBER_INT</code>.
    */
    public Table getObjectIdAndItemNo() {
        // check for missing context
        Shop shop = null;;
        if (userSessionData != null) {
          // get BOM
            BusinessObjectManager bom = (BusinessObjectManager) userSessionData.getBOM(BusinessObjectManager.ISACORE_BOM);
            // now I can read the shop
            shop = bom.getShop();
        }
        ResultData rsData = (ResultData)pageContext.getAttribute("singlerow");
        Table fieldT = GenericSearchUIFactory.getUIReturnTable(GenericSearchUIFactory.TYPE_RETURNTABLE_OUTPUTFIELD);
        // Fill Table        
        TableRow tabrow = fieldT.insertRow();
        tabrow.setValue("TYPE", "FIELD");
        String objectId = rsData.getString("OBJECT_ID_HDR");
        String numInt = rsData.getString("NUMBER_INT");

        String ffield = (objectId + "/" + numInt);
        tabrow.setValue("VALUE", ffield);
            
      return fieldT;
    }
    
    /**
     * Return contract status info as Icon.<br>Following status are known<br>
     * - INPROGRESS          (I1076)<br>
     * - SENT                (I1076)<br>
     * - QUOTATION           (I1055)<br>
     * - QUOTATION_ACCECPTED (I1079)<br>
     * - RELEASED            (I1004)<br>
     * - DENIED              (I1032)<br>
     * - COMPLETED           (I1005)<br>
     *  
     * Field <code>STATUS_SYSTEM</code> must exist in the result from the backend
     */
    public Table getContractStatusIcon() {
        ResultData rsData = (ResultData)pageContext.getAttribute("singlerow");
        Table fieldT = GenericSearchUIFactory.getUIReturnTable(GenericSearchUIFactory.TYPE_RETURNTABLE_OUTPUTFIELD);
        String iconPath = "";
        String iconTitle = "";
        boolean changeable = ("X".equals(rsData.getString("CHANGEABLE")) ? true : false);
        if ("I1004".equals(rsData.getString("STATUS_SYSTEM"))) {
            // RELEASED
            iconPath = "b2b/mimes/images/icon_led_green.gif";
            iconTitle = "b2b.contract.shuffler.key7val3";
        } else
            if ("I1032".equals(rsData.getString("STATUS_SYSTEM"))) {
                // REJECTED
                iconPath = "b2b/mimes/images/icon_led_red.gif";
                iconTitle = "b2b.contract.shuffler.key7val6";
            } else 
                if ("I1079".equals(rsData.getString("STATUS_SYSTEM"))) {
                    // ACCEPTED
                    iconPath = "b2b/mimes/images/icon_led_yellow.gif";
                    iconTitle = "b2b.contract.shuffler.key7val5";
                } else 
                    if ("I1055".equals(rsData.getString("STATUS_SYSTEM")) && changeable) {
                        // QUOTATION
                        iconPath = "b2b/mimes/images/icon_led_yellow.gif";
                        iconTitle = "b2b.contract.shuffler.key7val4";
                    } else 
                        if ("I1076".equals(rsData.getString("STATUS_SYSTEM")) &&  ! changeable) {
                            // INQUIRY SENT
                            iconPath = "b2b/mimes/images/icon_led_yellow.gif";
                            iconTitle = "b2b.contract.shuffler.key7val7";
                        } else 
                            if ("I1005".equals(rsData.getString("STATUS_SYSTEM"))) {
                                // COMPLETED
                                iconPath = "b2b/mimes/images/icon_led_red.gif";
                                iconTitle = "b2b.contract.shuffler.key7val8";
                            } else 
                                if (true) {
                                    // IN PROCESS
                                    iconPath = "b2b/mimes/images/icon_led_yellow.gif";
                                    iconTitle = "b2b.contract.shuffler.key7val2";  
                                } else {
                                    // otherwise INQUIRY SENT 
                                    iconPath = "b2b/mimes/images/icon_led_yellow.gif";
                                    iconTitle = "b2b.contract.shuffler.key7val7";  
                                }
        
        // Fill Table        
        TableRow tabrow = fieldT.insertRow();
        tabrow.setValue("TYPE", "ICON");
        tabrow.setValue("ICONPATH",  iconPath);
        tabrow.setValue("ICONWIDTH", "16");
        tabrow.setValue("ICONHIGHT", "15");
        tabrow.setValue("ICONBORDER", "0");
        tabrow.setValue("ICONTITLE", WebUtil.translate(pageContext, iconTitle, null));
        return fieldT;
    }
    /**
     * Return a Reverse Document (Storno) as ICON ( red 'x') 
     * Field <code>CANCEL_FLAG</code> must exist in the result from the backend
     */
    public Table getReversDocumentIcon() {
        ResultData rsData = (ResultData)pageContext.getAttribute("singlerow");
        Table fieldT = GenericSearchUIFactory.getUIReturnTable(GenericSearchUIFactory.TYPE_RETURNTABLE_OUTPUTFIELD);
        String iconPath = "";
        if ("A".equals(rsData.getString("CANCEL_FLAG"))) {
            iconPath = "b2b/mimes/images/BillingDocCancelled.gif";
        }
        // Fill Table        
        TableRow tabrow = fieldT.insertRow();
        tabrow.setValue("TYPE", "ICON");
        String value = rsData.getString("CANCEL_FLAG");
        tabrow.setValue("VALUE", "");            // Even field is empty, it must exist !!
        tabrow.setValue("ICONPATH",  iconPath);
        tabrow.setValue("ICONWIDTH", "8");
        tabrow.setValue("ICONHIGHT", "8");
        tabrow.setValue("ICONBORDER", "0");
        
        return fieldT;
    }
    /**
     * Delete VALID_TO value of quotations of status 'open' 
     * Field <code>VALID_TO</code> <code>STATUS_SYSTEM</code> and must exist in the result from the backend
     */
    public Table checkOnValidityDate() {
        ResultData rsData = (ResultData)pageContext.getAttribute("singlerow");
        Table fieldT = GenericSearchUIFactory.getUIReturnTable(GenericSearchUIFactory.TYPE_RETURNTABLE_OUTPUTFIELD);
        // Fill Table        
        TableRow tabrow = fieldT.insertRow();
        tabrow.setValue("TYPE", "FIELD");
        String value = rsData.getString("VALID_TO");
        if ("x.open".equals(rsData.getString("STATUS_SYSTEM"))) {
            value = "";
        }
        tabrow.setValue("VALUE", value);
        return fieldT;
    }
    /**
     * Return a trash can icon. 
     */
    public Table getBasketRemoveIconB2C() {
        ResultData rsData = (ResultData)pageContext.getAttribute("singlerow");
        Table fieldT = GenericSearchUIFactory.getUIReturnTable(GenericSearchUIFactory.TYPE_RETURNTABLE_OUTPUTFIELD);
        // Fill Table        
        TableRow tabrow = fieldT.insertRow();
        tabrow.setValue("TYPE", "ICON");
//        tabrow.setValue("VALUE", "");            // Even field is empty, it must exist !!
        tabrow.setValue("ICONPATH",  "b2c/mimes/images/icon_delete.gif");
        tabrow.setValue("ICONWIDTH", "16");
        tabrow.setValue("ICONHIGHT", "16");
        tabrow.setValue("ICONBORDER", "0");
        tabrow.setValue("ICONTITLE", WebUtil.translate(pageContext, "status.docmod.ordertemplate", null));
        return fieldT;
    }
    /**
     * Return a Reverse Document (Storno) as ICON ( red 'x') for a R3 billing document.
     * Fields <code>SD_DOC_CAT</code> must exists in the result from the backend
     */
    public Table getReversDocumentIconR3() {
        ResultData rsData = (ResultData)pageContext.getAttribute("singlerow");
        Table fieldT = GenericSearchUIFactory.getUIReturnTable(GenericSearchUIFactory.TYPE_RETURNTABLE_OUTPUTFIELD);
        String iconPath = "";
        if ("S".equals(rsData.getString("SD_DOC_CAT"))  ||  "N".equals(rsData.getString("SD_DOC_CAT"))) {
            iconPath = "b2b/mimes/images/BillingDocCancelled.gif";
        }
        // Fill Table        
        TableRow tabrow = fieldT.insertRow();
        tabrow.setValue("TYPE", "ICON");
        tabrow.setValue("VALUE", "");            // Even field is empty, it must exist !!
        tabrow.setValue("ICONPATH",  iconPath);
        tabrow.setValue("ICONWIDTH", "8");
        tabrow.setValue("ICONHIGHT", "8");
        tabrow.setValue("ICONBORDER", "0");
        
        return fieldT;
    }
    /**
     * Return (customized) status available for selection. 
     * @return Table including all status available for selection
     */
    public Table getSelectionStatusHOM() {
        Shop shop = null;
        BusinessObjectManager bom= (BusinessObjectManager) userSessionData.getBOM(BusinessObjectManager.ISACORE_BOM);
        shop = bom.getShop();

        GSProperty pty = (GSProperty)pageContext.getAttribute("screlm");
        Table options = GenericSearchUIFactory.getUIReturnTable(GenericSearchUIFactory.TYPE_RETURNTABLE_SELECTBOXOPTION);

        // Get User Status profile for given Procedure and add to request
        Search search = bom.createSearch();
        SearchResult resultStatusProfile = null;
        try {
            resultStatusProfile = search.performSearch(new UserStatusProfileSearchCommand(shop.getLanguage()));
        } catch (CommunicationException comEx) {
			if (log.isDebugEnabled()) {
				log.debug("Error ", comEx);
			}
            return options;
        }
        ResultData statusResult =  new ResultData(resultStatusProfile.getTable());

        if (shop != null) {
            if (statusResult != null) {
                statusResult.first();
                TableRow optRow = null;
                if (statusResult.getNumRows() > 1) {
                    // Write out blank line to allow selection without a process type
                    optRow = options.insertRow();
                    optRow.setValue("VALUE", "");
                    optRow.setValue("DESCRIPTION", WebUtil.translate(pageContext, "gs.att.sta.any", null));
                    // Write out fix line for OPEN status
                    optRow = options.insertRow();
                    optRow.setValue("VALUE", "I1002");
                    optRow.setValue("DESCRIPTION", WebUtil.translate(pageContext, "gs.att.stat.not.compl", null));
                    while (! statusResult.isAfterLast()) {
                        optRow = options.insertRow();
                        // Rowkey contains the status (e.g. I1002, ...)
                        optRow.setValue("VALUE", statusResult.getRowKey());
                        optRow.setValue("DESCRIPTION", statusResult.getString("DESCRIPTION_LONG"));
                        // next row
                        statusResult.next();
                    }
                }                
            }
        }
        return options;
    }


	/**
	 * Return a Reverse Document (Storno) as ICON ( red 'x') 
	 * Fields <code>CANCEL_FLAG</code> must exists in the result from the backend
	 */
	public Table getTransferCustomerIcon() {
		ResultData rsData = (ResultData)pageContext.getAttribute("singlerow");
		Table fieldT = GenericSearchUIFactory.getUIReturnTable(GenericSearchUIFactory.TYPE_RETURNTABLE_OUTPUTFIELD);
		String iconPath = "";
		
		iconPath = "b2b/mimes/images/icon_SelectCustomerForOrder.gif";
		
		// Fill Table        
		TableRow tabrow = fieldT.insertRow();
		tabrow.setValue("TYPE", "ICON");
		String value = rsData.getString("TRANSFERCUST");
		tabrow.setValue("VALUE", "");            // Even field is empty, it must exist !!
		tabrow.setValue("ICONPATH",  iconPath);
		tabrow.setValue("ICONWIDTH", "16");
		tabrow.setValue("ICONHIGHT", "16");
        tabrow.setValue("ICONBORDER", "0");
		tabrow.setValue("ICONTITLE", WebUtil.translate(pageContext, "bupa.search.transfertobasket", null));
        
		return fieldT;
	}
    /**
     * Return the Document download link as ICON.
     */
    public Table getDocumentDownloadIcon() {
        ResultData rsData = (ResultData)pageContext.getAttribute("singlerow");
        Table fieldT = GenericSearchUIFactory.getUIReturnTable(GenericSearchUIFactory.TYPE_RETURNTABLE_OUTPUTFIELD);
        String iconPath = "/b2b/mimes/images/download.gif";
        // Fill Table        
        TableRow tabrow = fieldT.insertRow();
        tabrow.setValue("TYPE", "ICON");
        tabrow.setValue("VALUE", "");            // Even field is empty, it must exist !!
        tabrow.setValue("ICONPATH",  iconPath);
        tabrow.setValue("ICONWIDTH", "16");
        tabrow.setValue("ICONHIGHT", "16");
        tabrow.setValue("ICONBORDER", "0");
        tabrow.setValue("ICONTITLE", WebUtil.translate(pageContext, "massdwnld.addorderimg.ttip", null));
        return fieldT;
    }

    /**
     * Return Attributes for the document download link.<br>
     * Fields <code>OBJECT_ID</code>, <code>DESCRIPTION</code>, <code>SYSTEM_STATUS</code>,
     *  <code>CREATED_AT_DATE</code> must exist in the result list from the backend
     * @return String containing the attributes
     */
    public String buildAttributesForSalesDocumentDownload() {
        ResultData rsData = (ResultData)pageContext.getAttribute("singlerow");
        StringBuffer strB = new StringBuffer("orderguid=");
        strB.append(rsData.getRowKey().toString());
        strB.append("&operation=add");                          // FIX
        strB.append("&orderid=");           
        strB.append(rsData.getString("OBJECT_ID"));
        try {
            rsData.getString("DESCRIPTION");
            if (rsData.getString("DESCRIPTION").length() >0) {    
                strB.append("&desc=");
                strB.append(rsData.getString("DESCRIPTION"));
            }
        } catch (UnknownColumnNameException ucnEx) {
			if (log.isDebugEnabled()) {
				log.debug("Error ", ucnEx);
			}
        }
        strB.append("&status=");
        strB.append("");
        strB.append("&orderdate=");
        strB.append(rsData.getString("CREATED_AT_DATE"));
        return strB.toString();        
    }
    /**
     * Return Attributes to create a new document from a predecessor document
     * @return String containing the attributes
     */
    public String buildAttributesForCreateFromPredecessor() {
        ResultData rsData = (ResultData)pageContext.getAttribute("singlerow");

        String docType = "";
        if ("ORDERTMP".equals(request.getParameter("rc_documenttypes")) ||
            "ORDERTMP".equals(searchRequestMemory.get("rc_documenttypes"))) {
            docType = DocumentListFilter.SALESDOCUMENT_TYPE_ORDERTEMPLATE;
        }
        if ("QUOTATION".equals(request.getParameter("rc_documenttypes")) ||
		    "QUOTATION".equals(searchRequestMemory.get("rc_documenttypes"))) {
            docType = DocumentListFilter.SALESDOCUMENT_TYPE_QUOTATION;
        }
		String targetDocType = "";
		if (request.getParameter("target") != null &&  !"".equals(request.getParameter("target"))) {
			searchRequestMemory.put("temp_target", request.getParameter("target"));
			targetDocType = request.getParameter("target");
		} else {
			targetDocType = (String)searchRequestMemory.get("temp_target");
		}
		
		String targetProcessType = "";
		if (request.getParameter("processtype") != null &&  !"".equals(request.getParameter("processtype"))) {
			searchRequestMemory.put("temp_processtype", request.getParameter("processtype"));
			targetProcessType = request.getParameter("processtype");
		} else {
			targetProcessType = (String)searchRequestMemory.get("temp_processtype");
		}
		
        StringBuffer strB = new StringBuffer("javascript:createFromPredecessor('");
        strB.append("source=" + docType);
        strB.append("&target=" + targetDocType);
        strB.append("&techkey=" + rsData.getRowKey().toString());
        strB.append("&processtype=" + targetProcessType);
        strB.append("');");
        return strB.toString();
    }
    /**
     * Return attribute Value where ID is 'OBJECT_ID'
     * @return Table containing the value from ID 'OBJECT_ID'
     */
    public Table getValueFromOBJECT_ID() {
		ResultData rsData = (ResultData)pageContext.getAttribute("singlerow");
		Table fieldT = GenericSearchUIFactory.getUIReturnTable(GenericSearchUIFactory.TYPE_RETURNTABLE_OUTPUTFIELD);
		String iconPath = "/b2b/mimes/images/download.gif";
		// Fill Table        
		TableRow tabrow = fieldT.insertRow();
		tabrow.setValue("TYPE", "FIELD");
		tabrow.setValue("VALUE", rsData.getString("OBJECT_ID"));
		return fieldT;
    }

	/**
	 * Returns created at date for selected saved basket. 
	 */
	public Table getDescriptionForSelectedSavedBasket() {
		Table fieldT = null;
		Basket basket = null;
		String objectId ="";
		boolean isBasketSelected = false;
		if (userSessionData != null) {
			BusinessObjectManager bom = (BusinessObjectManager) userSessionData.getBOM(BusinessObjectManager.ISACORE_BOM);
			basket = bom.getBasket();	
            ResultData rsData = (ResultData)pageContext.getAttribute("singlerow");
			if ( basket != null && basket.getTechKey() != null && basket.getTechKey().toString().equals(rsData.getString("GUID"))) {
				isBasketSelected = true;
			}
			fieldT = GenericSearchUIFactory.getUIReturnTable(GenericSearchUIFactory.TYPE_RETURNTABLE_OUTPUTFIELD);
			// Fill Table  
			if (isBasketSelected) {
				objectId = objectId.concat(">>>");
			}
			objectId = objectId.concat(JspUtil.encodeHtml(rsData.getString("DESCRIPTION")));   
			TableRow tabrow = fieldT.insertRow();
			tabrow.setValue("TYPE", "FIELD");
			tabrow.setValue("VALUE", objectId);			    
		}
		return fieldT;
	}
	/**
	 * Returns description for selected saved basket. 
	 */
	public Table getDateForSelectedSavedBasket() {
		Table fieldT = null;
		Basket basket = null;
		String objectId ="";
		boolean isBasketSelected = false;
		if (userSessionData != null) {
			BusinessObjectManager bom = (BusinessObjectManager) userSessionData.getBOM(BusinessObjectManager.ISACORE_BOM);
			basket = bom.getBasket();
			ResultData rsData = (ResultData)pageContext.getAttribute("singlerow");	
			if ( basket != null && basket.getTechKey() != null && basket.getTechKey().toString().equals(rsData.getString("GUID"))) {
						isBasketSelected = true;
			}	 	
			fieldT = GenericSearchUIFactory.getUIReturnTable(GenericSearchUIFactory.TYPE_RETURNTABLE_OUTPUTFIELD);
			// Fill Table  
			objectId = objectId.concat(rsData.getString("CREATED_AT_DATE"));   
			TableRow tabrow = fieldT.insertRow();
			tabrow.setValue("TYPE", "FIELD");
			tabrow.setValue("VALUE", objectId);			    
		}		
		return fieldT;		
	}

}