/*****************************************************************************
	Interface:        GenericSearchUIDynamicData
	Copyright (c) 2003, SAP AG, Germany, All rights reserved.

	$Revision:$
	$DateTime:$ (Last changed)
	$Change:$ (changelist)
    
*****************************************************************************/

package com.sap.isa.ui.uiclass.genericsearch;

import javax.servlet.jsp.PageContext;

/**
 * Represents all necessary methods for dynamic content creation in the Generic
 * Search UI environment 
 *
 * @author SAP
 * @version 1.0
 **/
public interface GenericSearchUIDynamicContentData {
    /**
     * Initialize UI Object. Necessary since reflection api doesn't support
     * instantiation of classes with arguments.
     */
    public void init(PageContext pageContext);
}
