/*****************************************************************************
    Class:        GenericSearchISAUI
    Copyright (c) 2003, SAP AG, Germany, All rights reserved.

    $Revision: #14 $
    $DateTime: 2004/07/26 16:19:59 $ (Last changed)
    $Change: 197323 $ (changelist)
    
*****************************************************************************/

package com.sap.isa.ui.uiclass.genericsearch;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import javax.servlet.jsp.PageContext;

import com.sap.isa.backend.boi.isacore.ShopData;
import com.sap.isa.backend.boi.isacore.order.DocumentListFilterData;
import com.sap.isa.businessobject.BusinessObjectManager;
import com.sap.isa.businessobject.CommunicationException;
import com.sap.isa.businessobject.DocumentState;
import com.sap.isa.businessobject.GenericSearch;
import com.sap.isa.businessobject.Shop;
import com.sap.isa.businessobject.User;
import com.sap.isa.businesspartner.backend.boi.PartnerFunctionData;
import com.sap.isa.businesspartner.businessobject.BusinessPartner;
import com.sap.isa.businesspartner.businessobject.BusinessPartnerManager;
import com.sap.isa.businesspartner.businessobject.HierarchyKey;
import com.sap.isa.core.SessionConst;
import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.core.logging.LogUtil;
import com.sap.isa.core.util.table.ResultData;
import com.sap.isa.core.util.table.Table;
import com.sap.isa.core.util.table.TableRow;
import com.sap.isa.isacore.DocumentHandler;
import com.sap.isa.isacore.ManagedDocument;
import com.sap.isa.isacore.action.IsaCoreInitAction;
import com.sap.isa.isacore.action.IsaCoreInitAction.StartupParameter;
import com.sap.isa.maintenanceobject.businessobject.AllowedValue;
import com.sap.isa.maintenanceobject.businessobject.GSAllowedValue;
import com.sap.isa.maintenanceobject.businessobject.GSProperty;
import com.sap.isa.user.permission.ECoActionPermission;

/**
 * The class GenericSearchISAUI.java extends class <code>GenericSearchBaseUI</code> and 
 * provides additional methods and variables to for ISales Application. 
 *
 * @author SAP
 * @version 1.0
 **/
public class GenericSearchISAUI extends GenericSearchBaseUI {
	/**
	 * Constants for the search types available in the Generic Search.
	 *  
	 * @author SAP
	 *
	 */
	public static class DocumentTypes
	{
		public static final String ORDER = "ORDER";
		public static final String ORDERITM = "ORDERITM";
		public static final String BACKORDER = "BACKORDER";
        public static final String QUOTATION = "QUOTATION";
        public static final String QUOTITM = "QUOTITM";    
        public static final String ORDERTMP = "ORDERTMP";
        public static final String ORDTMPITM = "ORDTMPITM";
        public static final String CONTRACT = "CONTRACT";
        public static final String INVOICE = "INVOICE";
        public static final String CREDITMEMO = "CREDITMEMO";  
        public static final String DOWNPAYMENT = "DOWNPAYMENT";
        public static final String AUCTION = "AUCTION";
        public static final String INQUIRY = "INQUIRY";
	}
    // Map with excluded document types
    Map exDocTypes = new HashMap();
    //permission flags 
    private boolean hasReadOrderPermission = false;
    private boolean hasReadOrdTmpPermission = false;    
    private boolean hasReadQuotPermission = false;  
    private boolean hasReadContPermission = false;
    private boolean hasReadAuctionPermission = false;
    private boolean hasReadInvoicePermission = false;
    
	private static final IsaLocation log =
			IsaLocation.getInstance(GenericSearchISAUI.class.getName());
    
    /**
     * Constructor for GenericSearchUI.
     * @param pageContext
     */ 
    public GenericSearchISAUI() {

    }

    /***
     * Initialize class. Necessary since reflection api doesn't support 
     * instantiation of classes with arguments.
     * @param pageContext
     */
    public void init(PageContext pageContext) {
		final String METHOD_NAME = "init()";
		log.entering(METHOD_NAME);
        super.init(pageContext);

        addToDocumentHandler();
        
        Shop shop = null;
        User user = null;
        if (userSessionData != null) {
            // get BOM
            BusinessObjectManager bom = (BusinessObjectManager) userSessionData.getBOM(BusinessObjectManager.ISACORE_BOM);
            // now I can read the shop
            shop = bom.getShop();
            user = bom.getUser();
        }
        if (user != null){
            //check for all create permissions
            String[] documents = new String[]{DocumentListFilterData.SALESDOCUMENT_TYPE_ORDER,
                                              DocumentListFilterData.SALESDOCUMENT_TYPE_ORDERTEMPLATE,
                                              DocumentListFilterData.SALESDOCUMENT_TYPE_QUOTATION,
                                              DocumentListFilterData.DOCUMENT_TYPE_CONTRACT,
                                              DocumentListFilterData.DOCUMENT_TYPE_AUCTION,
                                              DocumentListFilterData.BILLINGDOCUMENT_TYPE_INVOICE};
              try{    
                Boolean[] checkResults = checkReadPermissions(user, documents);                                         
                hasReadOrderPermission = checkResults[0].booleanValue();
                hasReadOrdTmpPermission = checkResults[1].booleanValue();               
                hasReadQuotPermission = checkResults[2].booleanValue();
                hasReadContPermission = checkResults[3].booleanValue();
                hasReadAuctionPermission = checkResults[4].booleanValue();
                hasReadInvoicePermission = checkResults[5].booleanValue();        
              }
              catch(CommunicationException Ex) {
              	    log.debug(Ex.getMessage());
                }       
        }
        if (shop != null) {
            buildDocumentTypeFilter(shop, getScreenName());
        } else {
            // Disable all document types
            exDocTypes.put(DocumentTypes.ORDER,DocumentTypes.ORDER);
			exDocTypes.put(DocumentTypes.ORDERITM,DocumentTypes.ORDERITM);
            exDocTypes.put(DocumentTypes.BACKORDER,DocumentTypes.BACKORDER);
            exDocTypes.put(DocumentTypes.ORDERTMP,DocumentTypes.ORDERTMP);
			exDocTypes.put(DocumentTypes.ORDTMPITM,DocumentTypes.ORDTMPITM);
            exDocTypes.put(DocumentTypes.QUOTATION,DocumentTypes.QUOTATION);
			exDocTypes.put(DocumentTypes.QUOTITM,DocumentTypes.QUOTITM);
            exDocTypes.put(DocumentTypes.INQUIRY,DocumentTypes.INQUIRY);    
            exDocTypes.put(DocumentTypes.CONTRACT,DocumentTypes.CONTRACT);
            exDocTypes.put(DocumentTypes.INVOICE,DocumentTypes.INVOICE);
            exDocTypes.put(DocumentTypes.CREDITMEMO,DocumentTypes.CREDITMEMO);
            exDocTypes.put(DocumentTypes.DOWNPAYMENT,DocumentTypes.DOWNPAYMENT);
            exDocTypes.put(DocumentTypes.AUCTION, DocumentTypes.AUCTION); 
        }
        
        if ("SearchCriteria_BOB_Customers".equals(getScreenName())) {
        	setCustSearchParams();
        }
        if ("SearchCriteria_B2B_Contracts".equals(getScreenName())  &&  shop != null) {
            setContractSearchParams(shop);
        }
        if ( ("SearchCriteria_B2B_Sales".equals(getScreenName()) ||
              "SearchCriteria_B2B_Items".equals(getScreenName()) ||
              "SearchCriteria_B2B_Sales_Backorder".equals(getScreenName())) &&
              shop != null ) {
            setSalesSearchParams(shop);
        }
        if ("SearchCriteria_B2B_Billing".equals(getScreenName())  &&  shop != null) {
            setBillingSearchParams(shop);
            setBillingDocResultlist(shop);
        }
        // Set the date and number format 
        if (shop != null) {
            dateFormat = shop.getDateFormat().toLowerCase();
            numberFormat = shop.getDecimalPointFormat();
            language = shop.getLanguageIso();
        }
        log.exiting();
    }

    /**
     * Handle settings to interact with the class DocumentHandler. Following request parameter can be used
     * to control this behavior.<br>
     * <ul>
     * <li>@see GenericSearchUIData.RC_DOCUMENTHANDLERNOADD: Setting this parameter to <code>yes</code>
     * will avoid that the used search description will be set as the current document organizer.<br>
     * Default: no
     * </li>
     * <li>@see GenericSearchUIData.RC_DOCUMENTHANDLERBUSOBJADD: Setting this parameter to <code>true</code>
     * will add the used search description via Business Object @see GenericSearch to the document handler. 
     * This can be used if the Generic Search Framework is not running as document search on the left side
     * of the application, but as search tool in general in the middle frame (workarea). In this case a
     * screen refresh will work properly, while otherwise a refresh would lead to the welcome page or the
     * last opened document.<br>
     * Default: false
     * </li>
     * </ul>
     */
    protected void addToDocumentHandler() {
        final String METHOD_NAME = "addToDocumentHandler()";
        log.entering(METHOD_NAME);
        // Inform DocumentHandler about current ScreenName, to be able to do a correct refresh
        documentHandler = (DocumentHandler)userSessionData.getAttribute(SessionConst.DOCUMENT_HANDLER);
        String searchToDisplay = request.getParameter(GenericSearchUIData.RC_SEARCHCRITERASCREENNAME);

        boolean docHandlerNoAdd = ("yes".equals(request.getParameter(GenericSearchUIData.RC_DOCUMENTHANDLERNOADD)) 
                                   ? true : false);  
        if (documentHandler != null) {
            if (searchToDisplay != null) {
                if ( ! docHandlerNoAdd) {
                    // No block of adding search description to document handler
                    documentHandler.setActualOrganizer("genericdocsearch");
                    documentHandler.setOrganizerParameter(GenericSearchUIData.RC_SEARCHCRITERASCREENNAME, searchToDisplay);
                    if ( ! "SearchCriteria_BOB_Customers".equals(searchToDisplay)   &&
                         ! "SearchCriteria_Agent_Customers".equals(searchToDisplay)) {
                        // Add only as document search if those searches should be shown
                        // under the Tab 'Transactions'
                        documentHandler.setOrganizerParameter("documentsearch.name", searchToDisplay);
                    }
                    else {
                    	// A.S. 04.01.2007, internal message 4289796 2006
						documentHandler.setActualOrganizer("customersearch");
                }
                }
            } else {
                if ( ! docHandlerNoAdd ) {
                    searchToDisplay = documentHandler.getOrganizerParameter(GenericSearchUIData.RC_SEARCHCRITERASCREENNAME);
                } else {
                    log.error(LogUtil.APPS_USER_INTERFACE, "Request parameter RC_DOCUMENTHANDLERNOADD found, but not RC_SEARCHCRITERIASCREENNAME");
                }
            }
        }
        boolean docHandlerBusinessObjectAdd = ("true".equals(request.getParameter(GenericSearchUIData.RC_DOCUMENTHANDLERBUSOBJADD)) 
        ? true : false);
        if (documentHandler != null &&  docHandlerBusinessObjectAdd) {
            GenericSearch gs = new GenericSearch();
            gs.setState(DocumentState.VIEW_DOCUMENT);
            //create managed document
            // Parameter itself (RC_DOCUMENTHANDLERBUSOBJADD) must not be added to the href again, since the business object 
            // needs only be added once.
            String hrefParameter = GenericSearchUIData.RC_SEARCHCRITERASCREENNAME+"="+ searchToDisplay + 
                    "&" + GenericSearchUIData.RC_DOCUMENTHANDLERNOADD + "=" + request.getParameter(GenericSearchUIData.RC_DOCUMENTHANDLERNOADD) +
                    "&" + GenericSearchUIData.RC_UI_NOFULLSCREENMODE + "=" + request.getParameter(GenericSearchUIData.RC_UI_NOFULLSCREENMODE) +
                    "&" + GenericSearchUIData.RC_UI_NOSELECTOPTIONS + "=" + request.getParameter(GenericSearchUIData.RC_UI_NOSELECTOPTIONS) +
                    "&" + GenericSearchUIData.RC_UI_INCLUDESTOSHOW + "=" + request.getParameter(GenericSearchUIData.RC_UI_INCLUDESTOSHOW);
            ManagedDocument mDoc = new ManagedDocument(gs, null, null, null, null, null, 
                                       "genericsearch", 
                                       null,           // NO href will avoid adding a link to the history
                                       hrefParameter);
            documentHandler.add(mDoc);
            documentHandler.setOnTop(gs);
        }
        log.exiting();
    }

    /**
     * Set parameters for contract search according to shop settings<br>
     * - set property STAT(1) to "unknown"
     * @param Shop object
     */
    protected void setContractSearchParams(Shop shop) {
        allowBPHierarchyCriteria("payerselection");
        GSProperty pty = (GSProperty)scrElms.getProperty("STAT(1)");
        if (pty != null  &&  ! shop.isContractNegotiationAllowed()) {
            pty.setType("unknown");
        }
    }
    /**
     * Set parameters for sales search according to shop settings<br>
     * - set property STAT(4) to "unknown" (auctions: shop.isEAuctionUsed())
     * - set property payerselection to "unknown" (partner hierarchy is not activated)
     * @param Shop object
     */
    protected void setSalesSearchParams(Shop shop) {
        GSProperty pty = (GSProperty)scrElms.getProperty("STAT(4)");
        if (pty != null  &&  ! shop.isEAuctionUsed()) {
            pty.setType("unknown");
        }
        allowBPHierarchyCriteria("payerselection");
    }
    /**
     * Set parameters for billing search according to shop settings<br>
     * - set property payerselection to "unknown" (partner hierarchy is not activated)<br>
     * - set allowedvalue "PROD_ID" ("attrib_sub_char_in") to "unkown" (R3 selection doesn't support this)
     * @param Shop object
     */
    protected void setBillingSearchParams(Shop shop) {
        allowBPHierarchyCriteria("payerselection");
        GSProperty pty = (GSProperty)scrElms.getProperty("attrib_sub_char_in");
        if (pty != null  &&  shop != null) {
            if (shop.isBillingSelectionR3()  ||  shop.isBillingSelectionR3CRM()) {
                Iterator avIT = pty.iterator();
                while (avIT.hasNext()) {
                    AllowedValue av = (AllowedValue)avIT.next();
                    if ("PROD_ID".equals(av.getValue())) {
                        av.setHidden(true);
                    }
                }
            }
        }
    }
    /**
     * Set given property (given by name) to type "unknown" if business partner is not
     * involved in a Business Partner Hierarchy. 
     * @param String ptyName which should be check
     */
    protected void allowBPHierarchyCriteria(String ptyName) {
        GSProperty pty = (GSProperty)scrElms.getProperty(ptyName);
        if (pty != null) {
            String type = pty.getType();
            pty.setType("unknown");
            if (userSessionData != null) {
                BusinessObjectManager bom = (BusinessObjectManager) userSessionData.getBOM(BusinessObjectManager.ISACORE_BOM);
                if (bom.getShop().isBpHierarchyEnable()) {
                    BusinessPartnerManager buPaMa = bom.createBUPAManager();
                    BusinessPartner buPa = buPaMa.getDefaultBusinessPartner(PartnerFunctionData.SOLDTO);
                    if (buPa != null) {
                        ResultData dealerFamilyList = null;
                        try { 
                            HierarchyKey hierarchy = buPaMa.getPartnerHierachy();
                            dealerFamilyList = buPa.getPartnerFromHierachy(hierarchy);
                            if (dealerFamilyList != null  &&  dealerFamilyList.getNumRows() > 0) {
                                pty.setType(type);
                            }
                          } catch (CommunicationException comEx){
                              log.error(comEx);
                          }
                    }
                }
            }
        }
    }
    /**
     * Return the Screen name which describes the search criteria in the XML file
     * @return String screen name
     */
    public String getScreenName() {
        String screenName = request.getParameter(GenericSearchUIData.RC_SEARCHCRITERASCREENNAME);
        if (screenName == null  ||  screenName.length() == 0) {
            screenName = (String)request.getAttribute(GenericSearchUIData.RC_SEARCHCRITERASCREENNAME);
        }
        if (screenName == null  ||  screenName.length() == 0) {
            DocumentHandler documentHandler = (DocumentHandler)userSessionData.getAttribute(SessionConst.DOCUMENT_HANDLER);
            screenName = documentHandler.getOrganizerParameter(GenericSearchUIData.RC_SEARCHCRITERASCREENNAME);
        }
        return screenName;
    }

    /**
     * Store Screen elements in request context to be used by Iterate tag 
     */
    public void setScreenElements() {
        // For the ISales GenericSearchISAUI.java override this method
        // and "add scrElm.applyDocumentTypeFilter(MAP)". In the init of that
        // class, get Shop from bom and create the MAP including the document
        // types which are switched of in the shop customizing 
        applyDocumentTypeFilter(exDocTypes);
        // determineDealerListUI();      NOT IN USE
        request.setAttribute(RC_SCREENELEMENTS, scrElms.iterator());        
    }
    /**
     * Filters available document types by Shop settings  
     */
    protected void applyDocumentTypeFilter(Map exDocTypes) {
        int visDocTypes = 0;
        GSProperty pty = (GSProperty)scrElms.getProperty("document_types");
        if (pty == null) {
            // For billing document types
            pty = (GSProperty)scrElms.getProperty("IRT_BDH_BILL_TYPE");
        }
        if (pty != null) {
            Iterator itAV = pty.iterator();
            while (itAV.hasNext()) {
                GSAllowedValue av = (GSAllowedValue)itAV.next();
                if (exDocTypes.get(av.getValue()) != null) {
                    // Found entry in exclusion list, so remove from allowed value list
                    av.setHidden(true);
                }
                if (av.isHidden() != true) {
                    // Count visible Document types
                    visDocTypes++;
                }
            }
        }
        // Auctions are not yet handled by the framework. Therefore the auction selection jsp needs to
        // be informed about what are all the document types available in the application.
        if (null != userSessionData.getCurrentConfigContainer().getConfigParams().getParamValue("auctiondata")
                 && hasReadAuctionPermission) {
            userSessionData.setAttribute("genericsearch.doclist", pty);
        }
        // Add the ONE visible document to the request
        if (visDocTypes == 1) {
            userSessionData.setAttribute("genericsearch.onevisibledoctype", ("ID" + pty.hashCode()) );
            userSessionData.setAttribute("genericsearch.functiontocall", pty.getUIJScriptOnChange());
        }
    }

    /**
     * Build up the document type filter depending on the used search screen
     */
    protected void buildDocumentTypeFilter(Shop shop, String screenName) {
        StartupParameter startupParameter = (StartupParameter)userSessionData.getAttribute(IsaCoreInitAction.SC_STARTUP_PARAMETER);
        if ("SearchCriteria_B2B_Sales".equals(screenName)           ||
		    "SearchCriteria_B2B_Items".equals(screenName)           ||
            "SearchCriteria_BOB_Sales".equals(screenName)           ||
            "SearchCriteria_Agent_Sales".equals(screenName)         ||
            "SearchCriteria_B2B_Ordertemplates".equals(screenName)  ||
            "SearchCriteria_BOB_Ordertemplates".equals(screenName)  ||
            "SearchCriteria_B2B_Sales_Backorder".equals(screenName) ||
            "SearchCriteria_B2B_Auction".equals(screenName)         ||
            "SearchCriteria_B2B_Contracts".equals(screenName)) {
            if (! shop.isBillingDocsEnabled()  || ! startupParameter.isBillingIncluded() 
                            || ! hasReadInvoicePermission) {
                exDocTypes.put(DocumentTypes.INVOICE,DocumentTypes.INVOICE);
                exDocTypes.put(DocumentTypes.CREDITMEMO,DocumentTypes.CREDITMEMO);
                exDocTypes.put(DocumentTypes.DOWNPAYMENT,DocumentTypes.DOWNPAYMENT);
            }
            if (! hasReadOrderPermission) {
                exDocTypes.put(DocumentTypes.ORDER,DocumentTypes.ORDER);
				exDocTypes.put(DocumentTypes.ORDERITM,DocumentTypes.ORDERITM);
                exDocTypes.put(DocumentTypes.BACKORDER,DocumentTypes.BACKORDER);
            }
            if ( (! shop.isQuotationAllowed() &&  ! shop.isQuotationOrderingAllowed() )  || ! hasReadQuotPermission) {
                exDocTypes.put(DocumentTypes.QUOTATION,DocumentTypes.QUOTATION);
				exDocTypes.put(DocumentTypes.QUOTITM,DocumentTypes.QUOTITM);
            }
            if ( (shop.getBackend() == ShopData.BACKEND_CRM  ||  ! shop.isQuotationAllowed()) ){
                exDocTypes.put(DocumentTypes.INQUIRY,DocumentTypes.INQUIRY);    
            }
            if ((! shop.isContractAllowed() && ! shop.isContractNegotiationAllowed()) || ! hasReadContPermission) {
                exDocTypes.put(DocumentTypes.CONTRACT,DocumentTypes.CONTRACT);
            }
            if (! shop.isTemplateAllowed() || ! hasReadOrdTmpPermission) {
                exDocTypes.put(DocumentTypes.ORDERTMP,DocumentTypes.ORDERTMP);
				exDocTypes.put(DocumentTypes.ORDTMPITM,DocumentTypes.ORDTMPITM);
            }
			if (! shop.isEAuctionUsed()
				|| null == userSessionData.getCurrentConfigContainer().getConfigParams().getParamValue("auctiondata")
				|| ! hasReadAuctionPermission) {
               exDocTypes.put(DocumentTypes.AUCTION, DocumentTypes.AUCTION); 
            }               
        }
        // Billing within ISales application framesets
        if ( ("SearchCriteria_B2B_Billing".equals(screenName) ||
        	  "SearchCriteria_BOB_Billing".equals(screenName) ||
			  "SearchCriteria_Agent_Billing".equals(screenName)
        		) &&  startupParameter.isBillingIncluded()) {
            if (! hasReadInvoicePermission) {
                exDocTypes.put(DocumentTypes.INVOICE,DocumentTypes.INVOICE);
                exDocTypes.put(DocumentTypes.CREDITMEMO,DocumentTypes.CREDITMEMO);
                exDocTypes.put(DocumentTypes.DOWNPAYMENT,DocumentTypes.DOWNPAYMENT);
            }
            if (! hasReadOrderPermission) {
                exDocTypes.put(DocumentTypes.ORDER,DocumentTypes.ORDER);
				exDocTypes.put(DocumentTypes.ORDERITM,DocumentTypes.ORDERITM);
                exDocTypes.put(DocumentTypes.BACKORDER,DocumentTypes.BACKORDER);
            }
            if ( (! shop.isQuotationAllowed() &&  ! shop.isQuotationOrderingAllowed() )  || ! hasReadQuotPermission) {
                exDocTypes.put(DocumentTypes.QUOTATION,DocumentTypes.QUOTATION);
                exDocTypes.put(DocumentTypes.QUOTITM,DocumentTypes.QUOTITM);
            }
            if ( (shop.getBackend() == ShopData.BACKEND_CRM  ||  ! shop.isQuotationAllowed()) ){
                exDocTypes.put(DocumentTypes.INQUIRY,DocumentTypes.INQUIRY);    
            }
            if (! shop.isContractAllowed() || ! hasReadContPermission) {
                exDocTypes.put(DocumentTypes.CONTRACT,DocumentTypes.CONTRACT);
            }
            if (! shop.isTemplateAllowed() || ! hasReadOrdTmpPermission) {
                exDocTypes.put(DocumentTypes.ORDERTMP,DocumentTypes.ORDERTMP);
				exDocTypes.put(DocumentTypes.ORDTMPITM,DocumentTypes.ORDTMPITM);
            }
            if (! shop.isEAuctionUsed()
				|| null == userSessionData.getCurrentConfigContainer().getConfigParams().getParamValue("auctiondata")
               	|| ! hasReadAuctionPermission) {
               exDocTypes.put(DocumentTypes.AUCTION, DocumentTypes.AUCTION); 
            }               
        }
        // Billing in a separate frameset (no other document types allowed besides the billing ones)
        if ("SearchCriteria_B2B_Billing".equals(screenName) &&  ! startupParameter.isBillingIncluded()) {
            if (! hasReadInvoicePermission) {
                exDocTypes.put(DocumentTypes.INVOICE,DocumentTypes.INVOICE);
                exDocTypes.put(DocumentTypes.CREDITMEMO,DocumentTypes.CREDITMEMO);
                exDocTypes.put(DocumentTypes.DOWNPAYMENT,DocumentTypes.DOWNPAYMENT);
            }
            exDocTypes.put(DocumentTypes.ORDER,DocumentTypes.ORDER);
			exDocTypes.put(DocumentTypes.ORDERITM,DocumentTypes.ORDERITM);
            exDocTypes.put(DocumentTypes.BACKORDER,DocumentTypes.BACKORDER);            
            exDocTypes.put(DocumentTypes.QUOTATION,DocumentTypes.QUOTATION);
			exDocTypes.put(DocumentTypes.QUOTITM,DocumentTypes.QUOTITM);
			if ( !(shop.getBackend() == ShopData.BACKEND_CRM) ){
					exDocTypes.put(DocumentTypes.INQUIRY,DocumentTypes.INQUIRY);	
				}
            exDocTypes.put(DocumentTypes.CONTRACT,DocumentTypes.CONTRACT);
            exDocTypes.put(DocumentTypes.ORDERTMP,DocumentTypes.ORDERTMP);
			exDocTypes.put(DocumentTypes.ORDTMPITM,DocumentTypes.ORDTMPITM);
            exDocTypes.put(DocumentTypes.AUCTION, DocumentTypes.AUCTION); 
        }
            
    }
    /**
     * Returns the name of JScript files which should be included.
     * @return ResultData containing file names
     */
    public ResultData getJScriptFilesTab() {
        ResultData retObj = super.getJScriptFilesTab();

        Table tab = retObj.getTable();
        TableRow tabRow = tab.insertRow();
        tabRow.setValue("FILENAME", "b2b/jscript/frames.js");
        
		tabRow = tab.insertRow();
		tabRow.setValue("FILENAME", "b2b/jscript/showSoldTo.jsp");

        tabRow = tab.insertRow();
        tabRow.setValue("FILENAME", "b2b/jscript/GSloadNewPage.jsp");

//if the auction should be displayed in the Shop, then insert the include file.
		if (userSessionData != null) {
			// get BOM
			BusinessObjectManager bom = (BusinessObjectManager) userSessionData.getBOM(BusinessObjectManager.ISACORE_BOM);
			Shop shop = bom.getShop();
			if ( shop != null 
			    && shop.isEAuctionUsed()
				&& null != userSessionData.getCurrentConfigContainer().getConfigParams().getParamValue("auctiondata")
				&& hasReadAuctionPermission) {
				   
                 tabRow = tab.insertRow();
                 tabRow.setValue("FILENAME", "b2b/jscript/GSsendRequestAuction.jsp");
		    }
		}

        return (retObj = new ResultData(tab));
    }
    /**
     * Dealerlists could have more than 150 single entries. It wouldn't make sence to
     * present a dropdown listbox containing that much options. Evenmore the options have
     * a compressed format and might not be distinguishable from each other.<br>
     * So this method checks for the properties <code>PARTNER_NO/1 or IRT_BDH_PAYER</code> with 
     * type <code>box</code> and <code>input</code>. If the dynamic method returns more the <b>30</b>
     * entries, the select box will be set invisible and the input field with a helpValue will be 
     * displayed and vice versa.<br>
     * By setting the type of the properties to <code>unknown</code> the property will not be rendered
     * on the HTML page at all.
     */
    protected void determineDealerListUI() {
        Iterator itPTY = scrElms.iterator();
        GSProperty ptyBox = null;
        GSProperty ptyInput = null;
        while (itPTY.hasNext()) {
            GSProperty pty = (GSProperty)itPTY.next();
            if ( ("PARTNER_NO/1".equals(pty.getName())  &&   "box".equals(pty.getType())) 
              || ("IRT_BDH_PAYER".equals(pty.getName()) &&   "box".equals(pty.getType())) ) {
                ptyBox = pty;
            }
            if ( ("PARTNER_NO/1".equals(pty.getName())  &&   "input".equals(pty.getType())) 
              || ("IRT_BDH_PAYER".equals(pty.getName()) &&   "input".equals(pty.getType())) ) {
                ptyInput = pty;
            }
        }
        if (ptyBox == null || ptyInput == null) {
            // Stop here since we have not the situation described above
            return; 
        }
        // Get dynamic method of the BOX property to check amount of entires
        Table dealerList = null;
        Iterator itAV = ptyBox.iterator();
        while (itAV.hasNext()) {
            GSAllowedValue av = (GSAllowedValue)itAV.next();
            if ("dynamic".equals(av.getContent())) {
                dealerList = (Table)performDynamicUIMethod(av.getContentCreateClass(), av.getContentCreateMethod());
                break;
            }
        }
        // Decide on the UI depending on the amount of entries
        if (dealerList != null  &&  dealerList.getNumRows() > 30) {
            // More than X entries => use input property
            ptyBox.setType("unknown");
        } else {
            // Less than X entries => use select box property
            ptyInput.setType("unknown");
        }
    }
    /**
     * Check for the user Permission for the Creation of different Documents. <br>
     * @param User for whom the permissions are required 
     * @param String document for which permission is asked.
     * @return Boolean True/False.
     */
    protected Boolean[] checkReadPermissions(User user, String[] docs) throws CommunicationException  {
        ECoActionPermission[] docPermissions = new ECoActionPermission[docs.length];
        for( int cnt = 0; cnt<docs.length; cnt++){
            docPermissions[cnt] = new ECoActionPermission(docs[cnt], DocumentListFilterData.ACTION_READ);       
        }
        return user.hasPermissions(docPermissions);     
    }
    /**
     * Since the implementation filter itself is not a visible select box for billing documents and
     * there are two possible backends for the documents, it must be decided what is correct 
     * implementation filter with the correct result list assigned.
     * @param Shop shop
     */
    protected void setBillingDocResultlist(Shop shop) {
        if (shop == null) {
            return;
        }
        if (shop.isBillingSelectionCRM() ||  shop.isBillingSelectionR3CRM()) {
            // CRM requested set R3 to unknown for the selection
            GSProperty pty = (GSProperty)scrElms.getProperty("implfilterR3");
            if (pty != null) {
                pty.setType("unknown");
            }
        }
        if (shop.isBillingSelectionR3()) {
            // R3 requested set CRM to unknown for the selection
            GSProperty pty = (GSProperty)scrElms.getProperty("implfilterCRM");
            if (pty != null) {
                pty.setType("unknown");
            }
        }
    }
	/** 
	 * Return the application specific JScript function name ( e.g doSomething('rigth') ).
	 * @return JScript function name
	 */
	public String getJScriptOnResultExpand() {
		return "isaTop().resize('max', null)";
	}
	
	/** 
	 * Return the application specific JScript function name ( e.g doSomething('left') ).
	 * @return JScript function name
	 */
	public String getJScriptOnResultCollapse() {
		return "isaTop().resize('min', null)";
	}
    protected void setCustSearchParams() {
        Map srm = getSearchRequestMemory();
        if (srm != null) {
            if ("true".equalsIgnoreCase(request.getParameter("countryChange"))) {
                                                
                srm.put("rc_customertypes", "ADDRESS");
                srm.put("rc_custselectcountry", request.getParameter("rc_custselectcountry"));
                                   
            	//check if list of regions is emty, if yes remove corresponding text element
				GSProperty pty = (GSProperty)scrElms.getProperty("REGION");           

				// Use setOptions method to find out if options for an select box exist
				if (pty != null) {
					boolean valuesExist = setOptions(pty);
					if (!valuesExist) {
						GSProperty pty1 = (GSProperty)scrElms.getProperty("cs_region");
						if (pty1 != null) {
							pty1.setType("unknown");
						}

					}
				}
										                				               
            }
        }
    }
}