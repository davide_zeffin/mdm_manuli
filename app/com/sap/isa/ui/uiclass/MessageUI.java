/*****************************************************************************
    Class:        MessageUI
    Copyright (c) 2004, SAP AG, Germany, All rights reserved.
    Author:       SAP AG
    Created:      14.04.2004
    Version:      1.0

    $Revision: #13 $
    $Date: 2003/05/06 $ 
*****************************************************************************/

package com.sap.isa.ui.uiclass;


import java.util.List;

import javax.servlet.jsp.PageContext;

import com.sap.isa.core.ContextConst;
import com.sap.isa.core.logging.IsaLocation;
import com.sap.isa.core.util.MessageListDisplayer;
import com.sap.isa.ui.UIInitHandler;

/**
 * The class MessageUI is base UI class for all message pages. <br>
 *
 * @author  SAP AG
 * @version 1.0
 */
public class MessageUI extends BaseUI {
	
	
	/**
	 * Reference to the message displayer object. <br>
	 */
	public MessageListDisplayer messageDisplayer = null;
	
	
	/**
	 * Flag, if the message displayer is available. <br>
	 */	
	public boolean isMessageDisplayerAvailable = false;
	
	
	/**
	 * Flag, if the message describes an application error. <br>
	 */	
	public boolean isApplicationError = true;
		
	/**
	 * List with request parameter, which will be added to the relogin url. <br>
	 */	
	protected List parameterList;
	
	/**
	 * Reference to the exception object.
	 */
	protected Exception exception = null;
	
	final static private IsaLocation log = IsaLocation.
			  getInstance(MessageUI.class.getName());

    /**
     * Overwrites the method initContext. <br>
     * 
     * @param pageContext
     * 
     * 
     * @see com.sap.isa.ui.uiclass.BaseUI#initContext(javax.servlet.jsp.PageContext)
     */
    public void initContext(PageContext pageContext) {
		final String METHOD_NAME = "initContext(PageContext)";
		log.entering(METHOD_NAME);
		
        super.initContext(pageContext);
        
		exception = (Exception)request.getAttribute(ContextConst.EXCEPTION);
	
		messageDisplayer = (MessageListDisplayer)request.getAttribute(MessageListDisplayer.CONTEXT_NAME);
	
		if (messageDisplayer!= null) {
			parameterList = messageDisplayer.getActionParameters(); 
			isMessageDisplayerAvailable = true;
			isApplicationError = messageDisplayer.isApplicationError();   
		}
		log.exiting();      
    }


    /**
     * Standard constructor. <br> 
     */
    public MessageUI() {
        super();
    }


    /**
     * Standard constructor 
     * 
     * @param pageContext
     */
    public MessageUI(PageContext pageContext) {
        initContext(pageContext);
    }
    
	
	/**
	 * Return a special message which should be displayed or null if
	 * the message is not available. <br>
	 * 
	 * @return String
	 */
	public String displaySpecialMessage() {
		
		if (request.getAttribute("nolog_shopinformation") != null) { 
		    return (String)request.getAttribute("nolog_shopinformation");
		}		
		return null;
	}


	/**
	 * Returns the name of the action to perform provided from the 
	 * {@link #messageDisplayer} or "". <br>
	 * 
	 * @return action to perform
	 */	
	public String getMessageAction() {
		
		String action = messageDisplayer.getAction();
		if (action == null) {
			return "";
		}
		
		return action;		
	}	

	/**
     * Returns the property {@link #parameterList}. <br>
     * 
     * @return
     */
	public List getParameterList() {
		return parameterList;
	}
    
    
    /**
     * Return if the page is an error page but no application error page. 
     * 
     * @return <code>true</code> if the error page header should be displayed.
     */
    public boolean showErrorPageHeader() {
    
    	boolean ret = false;
    	
    	if (isMessageDisplayerAvailable && messageDisplayer.isErrorPage() &&
    	 isApplicationError == false) {
    	 	ret = true;
    	 }

		return ret;				    		
    }
    
    
	/**
	 * Returns the property {@link #exception}. <br>
	 * 
	 * @return
	 */
    public Exception getException() {
    	return exception;
    }
    
    
    /**
     * Returns the exception message of the {@link #exception} object. <br>
     * 
     * @return message which describes the exception.
     */
    public String getExceptionMessage() {
		String msgLocalized;
		
    	if (exception!= null) {
			msgLocalized = translate(exception.getMessage());
	
			if (msgLocalized.startsWith("???")) {
				msgLocalized = exception.getMessage();
			}
    	}
    	else {
    		msgLocalized = "";
    	}
    	
    	return msgLocalized;
    	
    }

	/**
     * Returns the name of the action to start the application. <br>
     * 
     * @return The value stored under the web.xml parameter {@link ContextConst#APP_INIT_ACTION}
     *  or <code>init.do</code>
     */
	public String getInitAction() {
		String action =  pageContext.getServletContext().getInitParameter(ContextConst.APP_INIT_ACTION);
		if (action == null) {
			action = "init.do";
		}
		return action;
	}
    

	/**
	 * Includes a simple header page to the message page. <br>
	 * The header is only displayed if no portal is available.
	 * The page name is take form the {@link com.sap.isa.ui.UIInitHandler}. 
	 *
	 */
	public void includeSimpleHeader() {
		if (!isPortal) {
			String page = UIInitHandler.getSimpleHeaderIncludeName();
			if (page.length() > 0) {
				includePage(page);
			}
		}
	}


}
