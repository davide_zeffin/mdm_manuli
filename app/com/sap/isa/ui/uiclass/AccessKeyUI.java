/*
 * Created on Dec 20, 2004
 *
 * To change the template for this generated file go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
package com.sap.isa.ui.uiclass;

import java.util.ArrayList;
import java.util.Iterator;

import com.sap.isa.ui.accessibility.AccessKeysForApplication;

/**
 * @author i800855
 *
 * To change the template for this generated type comment go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
public class AccessKeyUI extends BaseUI
{
	protected ArrayList accessKeysForApplication = new ArrayList(); 
	
	public AccessKeyUI()
	{
		initAccessKeys();
	}
	
	protected void initAccessKeys()
	{
	}
	
	public Iterator getAccessKeysForApplication()
	{
		return accessKeysForApplication.iterator();
	}
	
	public AccessKeysForApplication addApplication(String keyForAppName)
	{
		AccessKeysForApplication app = new AccessKeysForApplication();
		app.setAppname(keyForAppName);
		accessKeysForApplication.add(app);
		return app;
	}
	
	public void removeApplication(String keyForAppName)
	{
		if (keyForAppName == null)
			return;
		Iterator it = getAccessKeysForApplication();
		while (it.hasNext())
		{
			AccessKeysForApplication def = (AccessKeysForApplication)it.next();
			if (keyForAppName.equalsIgnoreCase(def.getAppname()))
				accessKeysForApplication.remove(def);
		}
	}
	
	public AccessKeysForApplication getAccessKeysForApplication(String keyForAppName)
	{
		if (keyForAppName == null)
			return null;
		Iterator it = getAccessKeysForApplication();
		while (it.hasNext())
		{
			AccessKeysForApplication def = (AccessKeysForApplication)it.next();
			if (keyForAppName.equalsIgnoreCase(def.getAppname()))
				return def;
		}
		return null;
	}
	
	public void addAccessKeyToApplication(String keyForAppName, String keyForAccessKey, String keyForAccessKeyRepresents)
	{
		AccessKeysForApplication def = getAccessKeysForApplication(keyForAppName);
		if (def == null)
			return;
		def.addAccessKey(keyForAccessKey, keyForAccessKeyRepresents);
	}

	public void removeAccessKeyFromApplication(String keyForAppName, String keyForAccessKey)
	{
		AccessKeysForApplication def = getAccessKeysForApplication(keyForAppName);
		if (def == null)
			return;
		def.removeAccessKeys(keyForAccessKey);
	}
}
