package com.sap.sxe.dbmt.imp.xml.crm;

import java.lang.reflect.Method;

import com.sap.sxe.db.db;
import com.sap.vmc.runtime.RuntimeInformation;

/**
 * Is responsible for the serialization of actions to undertake for the import
 * of SCE Knowledge Base data from the Mobile Sales Automation (MSA) Distribution Server
 * into the local SCE database.
 */
public class c_import_manager {

	static String C_IMPORT_MANAGER_CLASS  = "com.sap.sce.kbmt.kbmtc_import_manager_crm";
	static String C_IMPORT_MANAGER_METHOD = "kb_import";

	public static String C_SCEINIT_TABLE_NAME = "SMOSCEKBI",
                       C_SCEDELT_TABLE_NAME = "SMOSCEKBD",
                       C_CLIENT = "MANDT",
                       C_LOGSYS = "LOGSYS",
                       C_KBNAME = "KBOBJNAME",
                       C_KBVERSION = "VERSION";

  public void kb_import(String documentPath) throws Exception {
    kb_import(new String[0],documentPath);
  }

  public void kb_import(String[] args,String documentPath) throws Exception {
// 20050120-kha: database handling not necessary because we read/write from/to the one db connection we have
//	DatabaseInfo inDatabaseInfo = ConfigParameters.getActiveDatabaseInfo();
//	if (inDatabaseInfo == null) {
//		log_api.log_write_msg("import manager", "dbmt", log_api.SEVERITY_ABEND,
//		"No active database has been maintained!\nYou may do this using the Administrator.");
//	}
//
//	String usr_out = "sa";
//	String pw_out  = "sa";
//
//	for (int i=0; i<args.length; i++) {
//		if  ((args[i].equalsIgnoreCase("-help")) ||
//			(args[i].equalsIgnoreCase("-h")) ||
//			(args[i].equals("-?")) ||
//			(args[i].equals("?"))) {
//			// Catch the help argument
//			System.out.println( "Usage: java com.sap.sxe.dbmt.xml.crm." +
//							    "c_import_manager [flags]");
//			System.out.println( "\tflags can be one or more of the " +
//							    "following:");
//			System.out.println( "\t-user_out:<input user>");
//			System.out.println( "\t-pwd_out:<input password>");
//			System.exit(1);
//		}else if (args[i].regionMatches(true, 0, "-pwd_out:", 0, 9)) {
//			//the password
//			pw_out = args[i].substring(9);
//		}else if (args[i].regionMatches(true, 0, "-user_out:", 0, 10)) {
//			//the user name
//			usr_out = args[i].substring(10);
//		}
//	}
//
//	String client    = inDatabaseInfo.getClient();
//
//	// IN DATABASE
//	String ds_in     = inDatabaseInfo.getURL();
//	String driver_in = inDatabaseInfo.getDriver();
//	String user_in   = inDatabaseInfo.getUser();
//	String pwd_in    = inDatabaseInfo.getPassword();
//
//	// OUT DATABASE
//	DatabaseInfo outDatabaseInfo = inDatabaseInfo.copy();
//
//    String ds_out     = outDatabaseInfo.getURL();
//    String driver_out = outDatabaseInfo.getDriver();
//    String user_out   = usr_out;
//    String pwd_out    = pw_out;
//
//	System.out.println("IN  DATABASE:"  + ds_in);
//	System.out.println("IN  USER:" + user_in);
//	System.out.println("OUT DATABASE:"  + ds_out);
//	System.out.println("OUT USER:" + user_out);
//
//
    //create database connections
	db db_in = db.getDb();
	db db_out = db.getDb();
	String client = RuntimeInformation.getInstance().getUserInfo().getClient();
    kb_update(db_in, db_out, client,documentPath);
    db_in.db_close();
    db_out.db_close();
  }

  public static void kb_update(db indb, db outdb, String client,String documentPath) throws Exception {

//    Location location = Location.getLocation(c_import_manager.class);

// 20050120-kha: no clue what this is supposed to do...
//    Properties serverProps = ConfigParameters.getServerParameters();
//
//    if(serverProps != null && serverProps.getProperty("IMPORT_MANAGER") != null && serverProps.getProperty("IMPORT_MANAGER").toUpperCase().equals("FALSE") ) {
//        category.logT(Severity.DEBUG, location, "Import Manager disabled");
//        return;
//    }
//
	Class  importManagerClass  = Class.forName(C_IMPORT_MANAGER_CLASS);
	Method importManagerMethod = importManagerClass.getDeclaredMethod(C_IMPORT_MANAGER_METHOD, new Class[] { db.class, db.class, String.class,String.class });

	importManagerMethod.invoke(null, new Object[] {indb, outdb, client,documentPath});
  }

}
