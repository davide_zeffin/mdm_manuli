package com.sap.tte.webui.analysis;

import javax.servlet.*;
import javax.servlet.http.*;
import java.net.URLEncoder;
import java.io.*;
import java.util.*;

/*import com.sap.spc.remote.client.ClientSupport;*/
/*import com.sap.spc.remote.client.object.IPCException;*/

/*import com.sap.sxe.socket.client.ClientException;
import com.sap.sxe.socket.client.ServerResponse;
import com.sap.sxe.socket.shared.ErrorCodes;

import com.sap.tte.extern.remote.shared.*; */

/**
 * Title:       AnalysisControlerServlet
 * Description: This servlet it the only interface to the TTE Analysis WebUI.
 *      It is called with a TTE document ID, reads the corresponding TTE XML
 *      documentes via remote command from the IPC, writes these documents as
 *      Strings into the Servlet context and calls the servlet
 *      com.sap.tte.webui.analysis.AnalysisServlet
 */
public class AnalysisControlerServlet extends HttpServlet
/*implements com.sap.tte.extern.remote.shared.CommandConstants*/ {

	public static String logFileName = "c:\\tteanalysis.log";

	/*   private ClientSupport m_client;*/
	private PrintWriter m_out;

	private boolean m_trace = false;
	private boolean m_forwarding = true;
	private boolean m_save = false;
	private boolean m_info = false;

	protected static OutputStreamWriter _msgWriter = null;

	/**
	 *  Initialize global variables
	 */
	public void init(ServletConfig config) throws ServletException {
		super.init(config);
	}

	/**
	 *  Process the HTTP Post request
	 */
	public void doPost(
		HttpServletRequest imRequest,
		HttpServletResponse imResponse)
		throws ServletException, IOException {

		HttpSession thisSession = getSession(imRequest, imResponse);
		imResponse.setContentType("text/html");
		imResponse.setHeader("Cache-Control", "no-store");
		imResponse.setHeader("Pragma", "no-cache");
		imResponse.setDateHeader("Expires", 0);

		// get parameter

		/*        String ipcServer      = imRequest.getParameter("server");
		        String ipcPort        = imRequest.getParameter("port");
		        String ipcSessionId   = imRequest.getParameter("sessionId");
		        String ipcDocumentId  = imRequest.getParameter("documentId");
		        String ipcItemId      = imRequest.getParameter("itemId");
		        String forwarding     = imRequest.getParameter("forwarding");
		        String application    = imRequest.getParameter("application");
		        String info           = imRequest.getParameter("info"); */
		String language = imRequest.getParameter("language");
		String trace = imRequest.getParameter("trace");
		/*       String save           = imRequest.getParameter("save");*/
		//    String fileName       = imRequest.getParameter("file");
		String ipcScenario = imRequest.getParameter("ipcScenario");
		String styleSheet = imRequest.getParameter("styleSheet");
		String returnAddress = imRequest.getParameter("returnAddress");
		//The documents are not passed as a url parameter
		//but passed alongwith the request
		/*String xmldocument1 =   imRequest.getParameter("inputxmldocument"); 
		String xmldocument2 =   imRequest.getParameter("outputxmldocument");*/
		String xmldocument1 =
			(String) imRequest.getAttribute("inputxmldocument");
		String xmldocument2 =
			(String) imRequest.getAttribute("outputxmldocument");

		String address = "";

		if (language == null || language.equals(""));
		language = "EN";

		//   m_save       = checkFlag(save);
		//   m_forwarding = checkFlag(forwarding);
		m_trace = checkFlag(trace);
		//   m_info       = checkFlag(info);

		this.setTraceWriter(imResponse); // get output writer

		// write parameters of servlet to output file
		writeTrace("<h2>Parameters</h2>");
		/*       writeTrace("server:         " + ipcServer);
		       writeTrace("port:           " + ipcPort);
		       writeTrace("sessionId:      " + ipcSessionId);
		       writeTrace("documentId:     " + ipcDocumentId);
		       writeTrace("itemId:         " + ipcItemId);*/
		writeTrace("language:       " + language);
		//    writeTrace("forwarding:     " + forwarding);
		//    writeTrace("application:    " + application);
		//    writeTrace("save:           " + save);
		//     writeTrace("fileName:       " + fileName);
		//     writeTrace("info:           " + info);
		writeTrace("ipcScenario:    " + ipcScenario);
		writeTrace("styleSheet:     " + styleSheet);
		writeTrace("");

		try {
			// create client for IPC
			/*           m_client = getClient(thisSession, ipcServer , ipcPort);
			           attachToSession(ipcSessionId);*/

			/*          String[] xmlDocuments = getXmlInputAndOutputDocs(ipcDocumentId);*/

			if (xmldocument1 != null && xmldocument2 != null) {
				if (m_forwarding) {

					//                    address = "/servlet/com.sap.tte.webui.analysis.AnalysisServlet?";
					//                    address = "com.sap.tte.webui.analysis.AnalysisServlet?";
					address =
						"com.sap.tte.webui.analysis.AnalysisServlet?";

                    String contextPath = imRequest.getContextPath();
                    // If the TTE-Analysis is running in the standalone pricing-app (= in the MSA-scenario)
                    // we have to use a different forward-address.
                    if (contextPath.indexOf("ipcpricing") > 0){
                         address = "servlet/com.sap.tte.webui.analysis.AnalysisServlet?";
                    }
                        

					//            if (ipcDocumentId != null && !ipcDocumentId.equals(""))
					//                address = address+"&documentId="+ipcDocumentId;

					//              if (ipcItemId != null && !ipcItemId.equals(""))
					//                  address = address+"&itemId="+ipcItemId;

					writeTrace("<h2>forward address</h2>");
					writeTrace("forwardAddress: " + address);

					if (thisSession != null) {
						try {
							clearAttribute(thisSession, "returnAddress");
							//            clearAttribute(thisSession, TteGetXmlTaxDocs.INPUT_DOCUMENT);
							//            clearAttribute(thisSession, TteGetXmlTaxDocs.OUTPUT_DOCUMENT);
							//            clearAttribute(thisSession, "xmldocument");
							//            clearAttribute(thisSession, "XmlTree");
							writeTrace("Attributes cleared");
						} catch (Exception ex) {
							writeTrace("Attributes not correctly cleared");
						}
					}

					if (returnAddress != null && !returnAddress.equals(""))
						thisSession.setAttribute(
							"returnAddress",
							returnAddress);
					thisSession.setAttribute("INPUT_DOCUMENT", xmldocument1);
					thisSession.setAttribute("OUTPUT_DOCUMENT", xmldocument2);
					writeTrace("Document attributes set");

					if (!this.isTracing()) {
						// only real forwarding if servlet 1 is not tracing
						//                        RequestDispatcher dispatcher = getServletContext().getRequestDispatcher(address);
						//                        if (dispatcher != null)
						//                            dispatcher.forward(imRequest, imResponse);
						imResponse.sendRedirect(address);
					}
				}
			} // if (xmlDocuments != null)
			else {
				ServletOutputStream out = imResponse.getOutputStream();
				out.println("There are no taxes in this document.");
                out.println("<br><a href='" + returnAddress + "'>Back</a>");
				/*             out.println("There is no TTE input document for ID " + ipcDocumentId);*/
			}
		} // try
		catch (Exception ex) {
			//       ServletOutputStream out = imResponse.getOutputStream();
			//       out.println("Exception: " + e.getMessage());
			//       msg("IPC Exception: " + e.getMessage());
			writeTrace("Errors while executing the Servlet");
		}
		/*   catch(NullPointerException e) {
		       ServletOutputStream out = imResponse.getOutputStream();
		       out.println("NullPointerException in post command GetXMLPricingTrace (probably a parameter problem): "+e.toString());
		       msg("NullPointerException in post command GetXMLPricingTrace (probably a parameter problem): "+e.toString());
		   }*/
	}

	private void clearAttribute(HttpSession imSession, String imAttribute) {
		Object dummyValue = imSession.getAttribute(imAttribute);
		if (dummyValue != null) {
			imSession.removeAttribute(imAttribute);
		}
	}

	/**
	 *  Process the HTTP Get request
	 */
	public void doGet(HttpServletRequest request, HttpServletResponse response)
		throws ServletException, IOException {
		doPost(request, response);
	}

	/**
	 * @return the client that this session uses to communicate with a server.
	 *         If their is no client the method will create one
	 */
	/*  private ClientSupport getClient(HttpSession httpSession,
	                                 String server,
	                                 String port)
	              throws IPCException {
	
	      writeTrace("try to create client");
	      String encoding = "UnicodeLittle";
	
	      ClientSupport client = (ClientSupport)httpSession.getAttribute("CLIENT");
	
	      if (client == null) {
	          if (server == null || port == null)
	              throw new IPCException(new ClientException(ErrorCodes.RET_INTERNAL_FATAL,
	                                        "no parameters for connecting to client: server = " + server + " port = " + port));
	
	          try {
	              client = new ClientSupport(server, Integer.parseInt(port), encoding, new Boolean("false"));
	          }
	          catch (ClientException e) {
	              throw new IPCException(e);
	          }
	      }
	
	      httpSession.setAttribute("CLIENT",client);
	      writeTrace("client successfully created");
	      return client;
	  }*/

	/**
	 *  Attaching to the session 'sessionId'
	 */
	/*  private void attachToSession(String sessionId) {
	      writeTrace("try to attach to session");
	      // attach to session
	      m_client.setSessionId(sessionId);
	      writeTrace("attached to session");
	  }*/

	/**
	 *  Read from the IPC the TTE XML input and output documents
	 */
	/*   private String[] getXmlInputAndOutputDocs(String imDocumentId)
	   throws IPCException {
	
	       String[] xmlDocuments = new String[2];
	       writeTrace("try to get XML trace");
	
	       try {
	           if (m_client != null) {
	               ServerResponse myServerResponse = m_client.cmd
	                   (TTE_GET_XML_TAX_DOCS,
	                    new String[] {TteGetXmlTaxDocs.DOCUMENT_ID, imDocumentId});
	               writeTrace("XML trace successfully got");
	               xmlDocuments[0] = myServerResponse.getParameterValue
	                   (TteGetXmlTaxDocs.INPUT_DOCUMENT);
	               xmlDocuments[1] = myServerResponse.getParameterValue
	                   (TteGetXmlTaxDocs.OUTPUT_DOCUMENT);
	               if (xmlDocuments[0] == null)
	                   return null;
	               else;
	                   return xmlDocuments;
	           }
	           else
	               return null;
	       }
	       catch (ClientException e) {
	           throw new IPCException(e);
	       }
	   } */

	/**
	 *
	 */
	public HttpSession getSession(
		HttpServletRequest req,
		HttpServletResponse res) {
		HttpSession result = req.getSession(true);
		if (result != null) {
			return result;
		} else {
			getServletContext().log("Servlet session not found");
			try {
				res.sendError(
					HttpServletResponse.SC_INTERNAL_SERVER_ERROR,
					"Servlet session not found. This is most probably caused by a timeout.");
			} catch (IOException e) {
				writeTrace(e.getMessage());
			}
			return null;
		}
	}

	/**
	 *  obsolet
	 */
	//    public String getServletUrl() {
	//            return "/servlet/"+getClass().getName();
	//    }

	/**
	 *
	 */
	private void generateEmptyTraceInfo(
		HttpServletResponse response,
		String result) {
		int pos1;
		int pos2;
		try {
			m_out = response.getWriter();
			response.setContentType("text/html");

			m_out.println(
				"<HTML><BODY BGCOLOR=\"#DED5C4\" LEFTMARGIN=\"10\" TOPMARGIN=\"0\" MARGINWIDTH=\"0\" MARGINHEIGHT=\"0\" text=\"black\" link=\"#000000\" vlink=\"#000000\" alink=\"#000000\" >");
			m_out.println("<center><p><hr><p><font size=\"+2\"><b> ");
			pos1 = result.indexOf("INFO1") + 7;
			pos2 = result.indexOf("INFO2") - 2;
			m_out.println(result.substring(pos1, pos2));
			m_out.println("</b></font></p></center>");
			m_out.println("<p><br>");

			pos1 = result.indexOf("INFO2") + 7;
			pos2 = result.indexOf("/>") - 1;
			m_out.println(result.substring(pos1, pos2));
			m_out.println("</br></p><hr></body></html>");
		} catch (IOException e) {
			writeTrace(e.getMessage());
		}
	}

	/**
	 * Writes a message to the servlet's own log file.
	 */
	public static void msg(String message) {
		if (logFileName.equals("none"))
			return;

		try {
			if (_msgWriter == null) {
				_msgWriter =
					new OutputStreamWriter(new FileOutputStream(logFileName));
			}
			_msgWriter.write(message);
			_msgWriter.write("\n");
			_msgWriter.flush();
		} catch (IOException e) {
			AnalysisControlerServlet.logFileName = "none";
		}
	}

	private boolean isTracing() {
		return m_trace;
	}

	private void setTraceWriter(HttpServletResponse response) {

		if (this.isTracing() && m_out == null)
		
			try {
				m_out = response.getWriter();
				response.setContentType("text/html");

			} catch (Exception e) {
				m_trace = false;
			}
	}

	private void writeTrace(String line) {
		if (this.isTracing())
			m_out.println(line + "<br>");
	}

	private boolean checkFlag(String switchString) {
		if (switchString == null
			|| switchString.equals("")
			|| switchString.equals(" ")
			|| switchString.equalsIgnoreCase("FALSE"))
			return false;
		else
			return true;
	}

	/**
	 * Clean up resources
	 */
	public void destroy() {
	}

}