package com.sap.tte.webui.analysis;

import  org.w3c.dom.*;  // Needed for classes Document/Node/NodeList



/** * Title:        XmlTree
 * Description:  A tree, representing an XML document. The nodes of the tree are
 *  objects of class XmlTreeNode.
 *  After an object of this class exists, it can only
 *   - Returning its Root '_root'
 *   - Returning the maximum Level of it leaves
 *  Why do we have this class?
 *      The only purpose is to create itself (and hence all corresponding
 *      nodes (objects of class 'XmlTreeNode') from a DOM tree.
 */
public class XmlTree {
    private static String[] PATH_TO_OUTPUT_DOC_ITEM = {"TteOutputDocument",
                                                       "Items",
                                                       "Item"};
    private XmlTreeNode _root;



/**
 *  Create this object from the DOM tree 'imDomTree'. The root of the new tree
 *  is stored in '_root'.
 *  The new tree has less nodes then the original DOM tree. There is one node
 *  per XML node. The value of a node is stored in the node, not as a son.
 */
    public XmlTree(Document imDomTree) {
        _root = new XmlTreeNode();
        setNodeTreeInformation(imDomTree.getDocumentElement(), _root);
        setNodeViewInformation(_root, null, 0);
        setHtmlTreeInformation();
    }



/**
 *  Toggle the node with ID 'imNodeId'
 */
    public void toggle(int imNodeId){
        XmlTreeNode selectedNode = getRoot().getNodeFromId(imNodeId);
        if (selectedNode != null){
            selectedNode.toggle();
        }
    }



/**
 *  Get the generic list for the node with the ID 'imNodeId'
 */
    public String getGenericList(int imNodeId){
        return getRoot().getNodeFromId(imNodeId).getHtmlList();
    }



/**
 *  @return description: The Root element (object of class 'XmlTreeNode') of this
 *      tree
 */
    public XmlTreeNode getRoot(){
        return _root;
    }



/**
 *  Print the HTML Version of this tree
 */
    public String printHtmlTree(String imCallerRef, String imPicturePath){
        HtmlBuffer htmlRepresentation = new HtmlBuffer(imCallerRef, imPicturePath);
        htmlRepresentation.openDoc();
        htmlRepresentation.openTable("TTE Documents");
        getRoot().printSubtree(htmlRepresentation, getRoot().getMaximumLevel());
        htmlRepresentation.treeInvisibleRow(getRoot().getMaximumLevel() + 1);
        htmlRepresentation.closeTable();
        htmlRepresentation.closeDoc();
        return htmlRepresentation.toString();
    }



/**
 *  Loop over this tree and add information which are needed for the HTML
 *  representation
 */
    private void setHtmlTreeInformation(){
        int dummy = getRoot().setHtmlTreeInformation(0, 1);
    }



/**
 *  Set for XmlTreeNode _root and for all children the attributes
 *      - isGenericViewNode   // A generic view exists for this note
 *      - genericViewFather   // The root for the existing generic view
 *  This method calls itself
 */
    public void setNodeViewInformation(XmlTreeNode imNode,
                                       XmlTreeNode imListFather,
                                       int         imPathPosition){
        XmlTreeNode listFatherForMyChildren = imListFather;
        XmlTreeNode myListFather = imListFather;
        int         newPathPosition = imPathPosition;

        if (imListFather != null & imNode.isWithLeaves()){
            imNode.setIsViewNode(true, imListFather);
        }

        if (imNode.isWithIdenticalSons()){
            listFatherForMyChildren = imNode;
        }

        if (imPathPosition < PATH_TO_OUTPUT_DOC_ITEM.length){
            if (imNode.getName().equals(PATH_TO_OUTPUT_DOC_ITEM[imPathPosition])){
                if (imNode.getName().equals
                    (PATH_TO_OUTPUT_DOC_ITEM[PATH_TO_OUTPUT_DOC_ITEM.length - 1])){
                    imNode.setIsWithPricingAnalysis(true);
                }
                newPathPosition++;
            }
        }

        for (int i = 0; i < imNode.getChildCount(); i++) {
            setNodeViewInformation
                (imNode.getChild(i), listFatherForMyChildren, newPathPosition);
        }

    }



/**
 *  Set the attributes
 *      - Name
 *      - Children
 *      - isLeave
 *      - Value
 *  of the XmlTreeNode object 'imTreeNode' from the corresponding
 *  DOM Node 'imDomNode'. This includes to create all sons for 'imTreeNode'
 *  and of course their sons and so on.
 *  The method calls itself to achieve this.
 */
    private void setNodeTreeInformation(Node imDomNode, XmlTreeNode imTreeNode){
        boolean     isLeave;
        NodeList    domNodeList;
        XmlTreeNode[]  children;
        String      value = null;

/*  Name */
        imTreeNode.setName(imDomNode.getNodeName());

        domNodeList = imDomNode.getChildNodes();
        if (domNodeList != null){
            if (domNodeList.getLength() > 0){
                int realChildrenCounter = 0;
                for (int i = 0; i < domNodeList.getLength(); i++) {
                    if (domNodeList.item(i).getNodeType() ==  Node.ELEMENT_NODE)
                        realChildrenCounter++;
                }
                if (realChildrenCounter == 0){
                    isLeave = true;
                    value = domNodeList.item(0).getNodeValue();
                }
                else{
                    isLeave = false;
                    children = new XmlTreeNode[realChildrenCounter];
                    realChildrenCounter = 0;
                    for (int i = 0; i < domNodeList.getLength(); i++) {
                        if (domNodeList.item(i).getNodeType() ==  Node.ELEMENT_NODE){
                            children[realChildrenCounter] = new XmlTreeNode();
                            setNodeTreeInformation
                                (domNodeList.item(i), children[realChildrenCounter]);
                            realChildrenCounter++;
                        }
                    }
/*  Children */
                    imTreeNode.setChildren(children);
                }   //  else
            }   //  (domNodeList.getLength() > 0)
            else{
                isLeave = true;
            }
        }   //  if (domNodeList != null)
        else{
            isLeave = true;
        }

/*  isLeave */
        imTreeNode.setIsLeave(isLeave);

/*  Value */
        if (isLeave){
            if (value == null)
                value = "";
            imTreeNode.setValue(value);
        }
    }


}