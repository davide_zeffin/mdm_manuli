package com.sap.tte.webui.analysis;

import java.io.*;
import javax.servlet.http.*;
import javax.servlet.*;
import org.w3c.dom.*;

//import com.sap.tte.analysis.HtmlBuffer;
//import com.sap.tte.analysis.XmlTree;
//import com.sap.tte.extern.remote.shared.*;


/**
 *  This servlet creates different views of the object stored in the
 *  session attribute "XmlDocument"
 *  The tree views contains navigation (e.g. all non-leave nodes can be toggled)
 *  Therefor each such node is represented by a link which refers again to this
 *  servlet.
 */
public class MainServlet extends HttpServlet {
    public static String PARAMETER_TYPE = "ForwardType";
    public static String PARAMETER_VALUE = "ForwardValue";
    public static String TYPE_BUTTON = "TypeButton";
    public static String TYPE_TREE = "TypeTree";
    public static String TYPE_GENERIC = "TypeGenericView";
    public static String BUTTON_GENERIC = "ButtonGenericView";
    public static String BUTTON_TREE = "ButtonTreeView";
    public static String BUTTON_OVERVIEW = "DocumentOverview";
    public static String BUTTON_TRACE = "TraceView";
	public static String BUTTON_ITEM = "ItemView";
	public static String BUTTON_PRODUCT = "ProductView";
    public static String BUTTON_PARTNER = "BusinessPartnerView";
    public static String BUTTON_BACK = "BacktoPricingPanel";

    private static String XML_DOCUMENT = "XmlDocument";
    private static String XML_TREE = "XmlTree";
    private static String INPUT_DOC_ID = "InputDoc";
    private static String OUTPUT_DOC_ID = "OutputDoc";

    private static String SERVLET_PATH = "com.sap.tte.webui.analysis.MainServlet";



/**
 *  Do Get Request:
 *  Present the correct view, corresponding to the values of the two request
 *  parameters
 *      PARAMETER_TYPE and PARAMETER_VALUE.
 *  If these values are not filled, per default the Document overview is choosen.
 */
    public void doGet(HttpServletRequest imRequest, HttpServletResponse imResponse)
    throws ServletException, IOException{

        String picturePath = imRequest.getContextPath() + "/mimes/images/";
        String xsltPath = super.getServletContext().getRealPath("properties");
        xsltPath = xsltPath + "\\";

        imResponse.setContentType("text/html");
        imResponse.setHeader("Cache-Control","no-store");
        imResponse.setHeader("Pragma","no-cache");
        imResponse.setDateHeader ("Expires", 0);

        HttpSession thisSession = imRequest.getSession(true);
        ServletOutputStream out = imResponse.getOutputStream();
        String returnaddress = (String)thisSession.getAttribute("returnAddress");
        
        if (returnaddress == null){
        	returnaddress = "http://localhost:50000/tteanalysis/ui/testanalysis.jsp";
        	     	        }
        XmlDocumentShell xmlDocument =
            (XmlDocumentShell)thisSession.getAttribute(XML_DOCUMENT);
        if (xmlDocument == null){
            xmlDocument = getXmlDocument(imRequest, imResponse);
        }

        XmlTree xmlTree =
            (XmlTree)thisSession.getAttribute(XML_TREE);
        if (xmlTree == null){
            xmlTree = getXmlTree(xmlDocument.getDomDocument(), imRequest);
        }

        String forwardType = imRequest.getParameter(PARAMETER_TYPE);
        String forwardValue = imRequest.getParameter(PARAMETER_VALUE);

        if (forwardType != null){
            if (forwardType.equals(TYPE_TREE)){
                if (forwardValue != null)
                    xmlTree.toggle(Integer.parseInt(forwardValue));
                out.println(xmlTree.printHtmlTree(SERVLET_PATH, picturePath));
            }
            if (forwardType.equals(TYPE_GENERIC)){
                if (forwardValue != null){
                    out.println
                        (xmlTree.getGenericList(Integer.parseInt(forwardValue)));
                    thisSession.setAttribute("GenericNode", forwardValue);
                }
            }
            if (forwardType.equals(TYPE_BUTTON)){
                if (forwardValue.equals(BUTTON_GENERIC)){
                    String lastSelectedNode = (String)thisSession.getAttribute("GenericNode");
                    if (lastSelectedNode != null)
                        out.println(xmlTree.getGenericList(Integer.parseInt(lastSelectedNode)));
                    else
                        out.println("No view has been selected");
                }
                else if (forwardValue.equals(BUTTON_TREE))
                    out.println(xmlTree.printHtmlTree(SERVLET_PATH, picturePath));
                else if (forwardValue.equals(BUTTON_OVERVIEW))
                    out.println(
                        getXsltResult(xsltPath + "DocumentOverview.xsl", xmlDocument));
                else if (forwardValue.equals(BUTTON_PRODUCT))
                    out.println(
                        getXsltResult(xsltPath + "ProductView.xsl", xmlDocument));
				else if (forwardValue.equals(BUTTON_ITEM))
					out.println(
						getXsltResult(xsltPath + "ItemView.xsl", xmlDocument));
				else if (forwardValue.equals(BUTTON_PARTNER))
					out.println(
						getXsltResult(xsltPath + "BusinessPartnerView.xsl", xmlDocument));
                else if (forwardValue.equals(BUTTON_TRACE))
                    out.println(
                        getXsltResult(xsltPath + "TraceView.xsl", xmlDocument));
                else if (forwardValue.equals(BUTTON_BACK))
                    {
                     /*   imResponse.setContentType("text/xml");
                        // Save outputstream 'out' to a file
                        imResponse.setHeader("Content-disposition",
                                      "attachment; filename=" +
                                      "TteDocuments.xml" );
                        out.println(xmlDocument.toString()); */
						out.println();
						imResponse.reset();
						imResponse.flushBuffer();
						imResponse.sendRedirect(returnaddress);
                    }
            }
        }
        else{
            out.println(
                getXsltResult(xsltPath + "DocumentOverview.xsl", xmlDocument));
        }
    }



/**
 *  Post Request: Just process the HTTP Get request
 */
	public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request,response);
	}



/**
 *  Transform the imported 'imXmlDoc' XML document with the XSLT file 'imXsltUrl'
 *  into a HTML String
 */
    private String getXsltResult(String imXsltUrl, XmlDocumentShell imXmlDoc){
        String errorMessage = new String ("No valid XSLT File found");
        try {
            return imXmlDoc.getHtmlFromXslt(imXsltUrl);
        }
        catch(Exception ex){
            return errorMessage;
        }
    }



/**
 *  Create and set the object of class com.sap.tte.analysis.XmlTree from the
 *  DOM XML document 'imDocument'
 */
    private XmlTree getXmlTree(Document imDocument,
                                HttpServletRequest imRequest){
        HttpSession thisSession = imRequest.getSession(true);
        XmlTree myXmlTree = new XmlTree(imDocument);
        thisSession.setAttribute(XML_TREE, myXmlTree);
        return myXmlTree;
    }



/**
 *  Create an object of class com.sap.tte.analysis.XmlDocumentShell from the
 *  XML Strings, which are stored in the session context as
 *      TteGetXmlTaxDocs.INPUT_DOCUMENT
 *      TteGetXmlTaxDocs.OUTPUT_DOCUMENT
 */
    private XmlDocumentShell getXmlDocument(HttpServletRequest imRequest,
                                            HttpServletResponse imResponse){

        HttpSession thisSession = imRequest.getSession(true);
        String inputDocument = (String)thisSession.getAttribute("INPUT_DOCUMENT");
        String outputDocument = (String)thisSession.getAttribute("OUTPUT_DOCUMENT");
        try {
            XmlDocumentShell myXmlDocument =
                new XmlDocumentShell(inputDocument, outputDocument);
            thisSession.setAttribute(XML_DOCUMENT, myXmlDocument);
            return myXmlDocument;
        }
        catch (Exception ex) {
            try{
                ServletOutputStream out = imResponse.getOutputStream();
                out.println("<HTML><BODY>");
                out.println(ex.getMessage());
                out.println("</BODY></HTML>");
            }
            catch(Exception exTwo){
            	return null;
            }
        }
        return null;
    }




/**
 *  Who am I?
 */
    public String getServletInfo() {
        return "Main Servlet of the TTE Analysis Web UI";
    }



/**
 *  Obsolet methods
 */
//    private String getXmlString(String imDocId){
//        try{
//            char buffer[] = new char[500000];
//            int byte_length = 0;
//            FileReader freader = new FileReader(imDocId);
//            byte_length = freader.read(buffer);
//            String xmlString = new String(buffer,0,byte_length);
//            return xmlString;
//        }
//        catch(Exception ex){}
//        return null;
//    }
//
//    public void init(ServletConfig config) throws ServletException {
//        super.init(config);
//    }


}