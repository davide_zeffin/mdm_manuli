package com.sap.tte.webui.analysis;

import java.io.*;
import javax.servlet.http.*;
import javax.servlet.*;

//import com.sap.tte.webui.analysis.HtmlBuffer*;



/**
 *  A list with buttons for navigation purpose
 */
public class ButtonServlet extends HttpServlet {

    private static final String SPACE     = "&nbsp;";



/**
 *  Do Get:
 *  Create a list of button (a table with one column, every button is
 *  in an extra row
 */
    public void doGet(HttpServletRequest imRequest, HttpServletResponse imResponse)
    throws ServletException, IOException{

        HttpSession thisSession = imRequest.getSession(false);
        String returnAddress = null;
        if (thisSession != null)
            returnAddress = (String)thisSession.getAttribute("returnAddress");

        HtmlBuffer myHtmlBuffer = new HtmlBuffer();
        String linkInfix = new String(
                           MainServlet.PARAMETER_TYPE
                           + "="
                           + MainServlet.TYPE_BUTTON
                           + "&"
                           + MainServlet.PARAMETER_VALUE
                           + "=");
        String servletPath = "com.sap.tte.webui.analysis.MainServlet?";
        myHtmlBuffer.openDoc();
        myHtmlBuffer.openButtonTable("Views");

        myHtmlBuffer.button(servletPath
                            + linkInfix
                            + MainServlet.BUTTON_OVERVIEW, "Overview");
        myHtmlBuffer.button(servletPath
                            + linkInfix
                            + MainServlet.BUTTON_TRACE, "Trace");
		myHtmlBuffer.button(servletPath
							+ linkInfix
							+ MainServlet.BUTTON_PARTNER, "Business Partners");
		myHtmlBuffer.button(servletPath
							+ linkInfix
							+ MainServlet.BUTTON_PRODUCT, "Products");        
		myHtmlBuffer.button(servletPath
							+ linkInfix
							+ MainServlet.BUTTON_ITEM, "Items");
        myHtmlBuffer.button(servletPath
                            + linkInfix
                            + MainServlet.BUTTON_TREE, "Tree View");
        myHtmlBuffer.button(servletPath
                            + linkInfix
                            + MainServlet.BUTTON_GENERIC, "Generic View");

        myHtmlBuffer.append("<tr><td height=20></td></tr>");
        if (returnAddress != null){
            myHtmlBuffer.append("<tr><td> </td></tr>");
            myHtmlBuffer.returnButton(returnAddress, "Back");
        }

        myHtmlBuffer.closeTable();
        myHtmlBuffer.closeDoc();

        imResponse.setContentType("text/html");
        imResponse.setHeader("Cache-Control","no-store");
        imResponse.setHeader("Pragma","no-cache");
        imResponse.setDateHeader ("Expires", 0);
        ServletOutputStream out = imResponse.getOutputStream();
        out.println(myHtmlBuffer.toString());
    }



/**
 *  Who am I?
 */
    public String getServletInfo() {
        return "TTE Analysis Button Frame: Creates a column with some buttons";
    }


//    public void init(ServletConfig config) throws ServletException {
//        super.init(config);
//    }

}