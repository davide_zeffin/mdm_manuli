package com.sap.tte.webui.analysis;

import java.io.*;
import javax.servlet.http.*;
import javax.servlet.*;

//import com.sap.tte.webui.analysis.HtmlBuffer;



public class AnalysisServlet extends HttpServlet {



/**
 *  Do Get Request:
 *      Create a fraim-set with two frames:
 *          - A list with buttons on the left side
 *          - the actual view of the TTE XML documents on the right side
 */
    public void doGet(HttpServletRequest imRequest, HttpServletResponse imResponse)
    throws ServletException, IOException{
        HtmlBuffer myHtmlBuffer = new HtmlBuffer();

        myHtmlBuffer.openFrameSet();
        myHtmlBuffer.frame("button", "com.sap.tte.webui.analysis.ButtonServlet", true);
        myHtmlBuffer.frame("main", "com.sap.tte.webui.analysis.MainServlet", true);
        myHtmlBuffer.closeFrameSet();

        imResponse.setContentType("text/html");
        imResponse.setHeader("Cache-Control","no-store");
        imResponse.setHeader("Pragma","no-cache");
        imResponse.setDateHeader ("Expires", 0);
        ServletOutputStream out = imResponse.getOutputStream();
        out.println(myHtmlBuffer.toString());
    }



/**
 *  Who am I?
 */
    public String getServletInfo() {
        return "TTE Analysis Servlet";
    }



//    public void init(ServletConfig config) throws ServletException {
//        super.init(config);
//    }
}