package com.sap.tte.webui.analysis;

import java.io.*;
import java.net.URL;

import org.w3c.dom.*;
import org.xml.sax.*;
import javax.xml.parsers.*;
import javax.xml.transform.*;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;
import javax.xml.transform.dom.DOMSource;

// import com.inqmy.lib.xml.dom.ElementImpl;

/**
 * Title:       XmlDocumentShell
 * Description: A representation of an XML document.
 *  This class offers different internal representations on the XML document
 *  The constructor of this class has the two import parameters:
 *      String imXmlStringOne and String imXmlStringTwo)
 *  which represent two XML documents - the TTE Input document and the TTE
 *  output document.
 *  One XmlDocument is created from these two Strings - and hence of DOM
 *  Document also, which is stored in the attribute '_domDocument'.
 *  This is class is slightly different from the corresponding class in package
 *  com.sap.tte.analysis as here is the XML parser and transformer from InQMy is
 *  used, but not the Sun XML parser package.
 */
public class XmlDocumentShell
{

	private Document _domDocument;

	/**
	 *  Create one DOM tree (type 'Document' from the two imported Strings, which
	 *  represent two XML documents and store the result in '_domDocument'
	 */
	public XmlDocumentShell(String imXmlStringOne, String imXmlStringTwo)
		throws
			IOException,
			ParserConfigurationException,
			SAXException,
			ClassNotFoundException,
			Exception
	{

		if ((imXmlStringOne != null) & (imXmlStringTwo != null))
		{
			//            String documentImplClass = "com.inqmy.lib.xml.dom.DocumentImpl";
			//            _domDocument =
			//                (Document)Class.forName(documentImplClass).newInstance();
			//            Element newRoot = _domDocument.createElement("TteDocuments");
			//            appendDomTree(newRoot, imXmlStringOne);
			//            appendDomTree(newRoot, imXmlStringTwo);
			//            _domDocument.appendChild(newRoot);
			_domDocument = getDomDocument(imXmlStringOne, imXmlStringTwo);
		}
		else
			_domDocument = getDomDocument(imXmlStringOne);

	}
	/**
	 *  Create a DOM Tree XML Document from the two imported XML String by merging
	 *  the Strings and than creating one DOM Tree from the new String.
	 */
	private Document getDomDocument(
		String imXmlDocStringOne,
		String imXmlDocStringTwo)
		throws Exception
	{
		//            ParserConfigurationException,
		//            IOException ,
		//            SAXException{
		String mergedXmlString =
			"<?xml version=\"1.0\" encoding=\"UTF-8\" ?> "
				+ "<TteDocuments>"
				+ shortenXmlString(imXmlDocStringOne, "<TteInputDocument")
				+ shortenXmlString(imXmlDocStringTwo, "<TteOutputDocument")
				+ "</TteDocuments>";
		return getDomDocument(mergedXmlString);
	}
	/**
	 *  Remove everything in front of the first tag 'imFirstTag'
	 */
	private String shortenXmlString(String imXmlString, String imFirstTag)
	{
		int index = imXmlString.indexOf(imFirstTag);
		String truncatedText =
			new String(imXmlString.substring(index, imXmlString.length()));
		return truncatedText;
	}
	/**
	 *  Return the DOM representation of this object.
	 *  (I.e. the attribute '_domDocument')
	 */
	public Document getDomDocument()
	{
		return _domDocument;
	}

	/**
	 *  Return the String representation of this object.
	 *  (I.e. a XML String)
	 */
	public String toString()
	{
		try
		{
			return getHtmlFromXslt(null);
		}
		catch (Exception ex)
		{
			return null;
		}
	}

	/**
	 *  Create a DOM tree (type 'Document') from the imported XML String imXmlString
	 *  and append this tree to the imported node imRoot (type 'Element')
	 */
	private void appendDomTree(Element imRoot, String imXmlString)
//throws IOException, ParserConfigurationException, SAXException
	{
//		Document domDocSubTree = getDomDocument(imXmlString);
//		ElementImpl subRoot = (ElementImpl) domDocSubTree.getDocumentElement();
//		subRoot.setOwner(imRoot.getOwnerDocument(), true);
//		imRoot.appendChild(subRoot);
	}

	/**
	 *  Create a DOM Tree XML Document from the imported XML String and return it.
	 */
	private Document getDomDocument(String imDocString)
		throws ParserConfigurationException, IOException, SAXException
	{

		//        System.setProperty("javax.xml.parsers.DocumentBuilderFactory",
		//        "com.inqmy.lib.jaxp.DocumentBuilderFactoryImpl");
		//
		///*      Step 1: create a DocumentBuilderFactory */
		//        DocumentBuilderFactory  docFactory = DocumentBuilderFactory.newInstance();
		//
		///*      Step 2: Set some configuration options */
		//        docFactory.setValidating(false);
		//        docFactory.setIgnoringComments(true);
		//        docFactory.setIgnoringElementContentWhitespace(true);
		//
		///*      Step 3: create a DocumentBuilder that satisfies the constraints */
		///*      specified by the DocumentBuilderFactory */
		//        DocumentBuilder docBuilder = docFactory.newDocumentBuilder();
		//
		///*      Step 4: parse the input String*/
		//        StringReader    myStringReader = new StringReader(imDocString);
		//        InputSource     myInputSource = new InputSource(myStringReader);
		//        Document        myDocument = docBuilder.parse(myInputSource);

		/*      Step 5: Return the result */
		//			return myDocument;

		byte[] byteArray = imDocString.getBytes();
		ByteArrayInputStream bais = new ByteArrayInputStream(byteArray);
		InputStream is = (InputStream) bais;

		DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
		factory.setValidating(false);
		DocumentBuilder builder = factory.newDocumentBuilder();
		Document xmldoc = builder.parse(is);
		is.close();

		return xmldoc;

	}

	/**
	 *  If the imported String 'imFileName' is 'null', create an XML String from
	 *  this object (the attribute '_domDocument') by applying the "identity
	 *  transformation" to the DOM document.  In this case, the "transformer" isn't
	 *  actually changing anything, which means that the "transformation" generates
	 *  a copy of the source - unchanged. We get an String, which contains the XML
	 *  file of this object (of the DOM Tree attribute '_domDocument')
	 *  Otherwise, create a HTML String from this object ('_domDocument') by
	 *  applying the XSL transformaton defined by file 'imFileName' to it and
	 *  return it.
	 */
	public String getHtmlFromXslt(String imFileName)
		throws TransformerConfigurationException, TransformerException
	{
		
//		System.setProperty(
//			"javax.xml.transform.TransformerFactory",
//			"com.inqmy.lib.jaxp.TransformerFactoryImpl");

		TransformerFactory transFactory = TransformerFactory.newInstance();
		Transformer docTransformer = null;
		if (imFileName != null)
		{
			Source xsltFilter = new StreamSource(new File(imFileName));
			docTransformer = transFactory.newTransformer(xsltFilter);
			docTransformer.setOutputProperty(OutputKeys.METHOD, "HTML");

		}
		else
		{
			docTransformer = transFactory.newTransformer(); //identity transform
		}
		docTransformer.setOutputProperty(OutputKeys.INDENT, "YES");

		StringWriter myWriter = new StringWriter();
		Result myResult = new StreamResult(myWriter);
		Source xmlSource = new DOMSource(_domDocument);

		docTransformer.transform(xmlSource, myResult);
		return myWriter.toString();
	}

}