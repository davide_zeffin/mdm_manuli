package com.sap.tte.webui.analysis;

import java.util.ArrayList;



/**
 * Title:        XmlTreeNode
 * Description:  An object of this class represents one node in a tree, which
 *  represents an XML tree.
 */
public class XmlTreeNode {



/*  Standard node information, needed for any kind of trees */
    private boolean     _isLeave;
    private String      _name;
    private String      _value;     // Value of the node (only if it is a leave)
    private XmlTreeNode[]  _children;  // There are no children if it is a leave
/*  Information about existing views for the note */
    private boolean     _isWithPricingAnalysis; // This is an <Item> node, hence a Pricing Analysis should exist for it
    private boolean     _isGenericViewNode;     // A generic view exists for this note
    private XmlTreeNode    _genericViewFather;     // Node, which is the root for the existing generic view
/*  Needed to create a HTML table which represents the XML data as a tree
    This is needed for a Servlet, but not if the tree is presented with an
    Swing Tree table */
    private boolean     _isOpen;    // Is the node expanded or not.
    private boolean     _isVisible; // Is the node visible or not.
    private int         _ID;
    private int         _level; // The level of the note: Every son has the level
                                // n+1, where n is the level of the father node.
                                // The level of the root is 0.


/**
 *  Constructor
 */
    public XmlTreeNode() {
    }



/**
 *  Self explaining Set methods
 */
    public void setIsLeave (boolean imIsLeave){
        _isLeave = imIsLeave;
    }
    public void setName (String imName){
        _name = imName;
    }
    public void setValue (String imValue){
        _value = imValue;
    }
    public void setChildren (XmlTreeNode[] imChildren){
        _children = imChildren;
    }
    public void setIsWithPricingAnalysis (boolean imIsWithPricingAnalysis){
        _isWithPricingAnalysis = imIsWithPricingAnalysis;
    }



/**
 *  Set method for
 *      - _isGenericViewNode and
 *      - _genericViewFather
 *  together. If a node is a "GenericViewNode" it needs information about the
 *  corresponding "GenericViewFather". This is, why this values can only be
 *  set together.
 */
    public void setIsViewNode (boolean imIsViewNode, XmlTreeNode imViewFather){
        _isGenericViewNode = imIsViewNode;
        _genericViewFather = imViewFather;
    }



/**
 *  Self explaining Get methods
 */
    public String getName(){
        return _name;
    }
    public String getValue(){
        return _value;
    }
    public boolean IsViewNode(){
        return _isGenericViewNode;
    }
    public boolean IsWithPricingAnalysis(){
        return _isWithPricingAnalysis;
    }
    private XmlTreeNode getViewFather(){
        return _genericViewFather;
    }



/**
 *  Write for this node and all his sons their HTML tree representation (HTML
 *  text) into the HtmlBuffer 'imHtmlBuffer'.
 *  This method calls itself
 */
    public void printSubtree(HtmlBuffer imHtmlBuffer, int imMaxLevel){
        if(_isVisible){
            imHtmlBuffer.treeOpenRow(_level);
            for (int i = 0; i < _level; i++) {
                imHtmlBuffer.treeCellEmpty();
            }
            if (_isLeave)
                imHtmlBuffer.treeFolderLeave();
            else{
                if (_isOpen)
                    imHtmlBuffer.treeFolderOpen(_ID);
                else
                    imHtmlBuffer.treeFolderClose(_ID);
            }
//            for (int i = _level; i < (imMaxLevel + 1); i++) {
//                imHtmlBuffer.treeCellEmpty();
//            }
//            if (_isLeave)
//                imHtmlBuffer.treeLeaveWithSpace(_name, _level);
//            else
//                imHtmlBuffer.treeCellWithSpace(_name, _level, _ID);
            int columnsToSpan = 1 + imMaxLevel - _level;
            if (_isLeave)
                imHtmlBuffer.treeLeaveWithColSpan(_name, columnsToSpan);
            else
                imHtmlBuffer.treeCellWithColSpan(_name, columnsToSpan, _ID);

            imHtmlBuffer.treeVerticalLine();
            if (_value != null)
                imHtmlBuffer.treeValueCell(_value);
            else
                imHtmlBuffer.treeCellEmpty();
            imHtmlBuffer.treeVerticalLine();
            if (_isGenericViewNode)
                imHtmlBuffer.treeGenericButton(_ID);
            else
                imHtmlBuffer.treeCellEmpty();
            imHtmlBuffer.treeCloseRow();
            for (int i = 0; i < getChildCount(); i++) {
                getChild(i).printSubtree(imHtmlBuffer, imMaxLevel);
            }
        }
    }



/**
 *  Returns the maximum level (value of the attribute '_level') of this node and
 *  all his sons. This method calls itself.
 */
    public int getMaximumLevel(){
        int maximum = this._level;
        for (int i = 0; i < getChildCount(); i++) {
            int childrenMaximum = getChild(i).getMaximumLevel();
            if (childrenMaximum > maximum)
                maximum = childrenMaximum;
        }
        return maximum;
    }



/**
 *  Add information which are needed for the HTML
 *  representation
 */
    public int setHtmlTreeInformation(int imLevel, int imID){
        this._level = imLevel;
        this._ID = imID;
        int sonID = imID + 1;

        if (imLevel == 0){
            this._isOpen = true;
            this._isVisible = true;
        }
        else if (imLevel == 1){
            this._isOpen = false;
            this._isVisible = true;
        }
        else{
            this._isOpen = false;
            this._isVisible = false;
        }
        for (int i = 0; i < getChildCount(); i++) {
            sonID = getChild(i).setHtmlTreeInformation(imLevel + 1, sonID);
        }
        return sonID;
    }



/**
 *  Returns this node, if it has the ID (parameter '_ID') 'imNodeId'
 *  Returns one of his sons (or one their sons or ...) if that node has the ID
 *    'imNodeId'
 *  Returns 'null' if no corresponding node is found
 */
    public XmlTreeNode getNodeFromId(int imNodeId){
        if (_ID == imNodeId)
            return this;
        else{
            for (int i = 0; i < getChildCount(); i++) {
                XmlTreeNode foundNode = getChild(i).getNodeFromId(imNodeId);
                if (foundNode != null)
                    return foundNode;
            }
        }
        return null;
    }



/**
 *  Close this node and hide all sons and their sons and ... if this node is open
 *  Open this node and show all direct sons, if this node is closed
 */
    public void toggle(){
        if (_isOpen){
            _isOpen = false;
            for (int i = 0; i < getChildCount(); i++) {
                getChild(i).close();
            }
        }
        else{
            _isOpen = true;
            for (int i = 0; i < getChildCount(); i++) {
                getChild(i)._isVisible = true;
            }
        }
    }



/**
 *  Close this node and all the sons and their sons and ...
 *  This method calls itself
 */
    private void close(){
        _isOpen = false;
        _isVisible = false;
        for (int i = 0; i < getChildCount(); i++) {
            getChild(i).close();
        }
    }


/**
 *  @return description:
 *      - 'Yes', if this node has at least two direct sons with the same name
 *      - 'No', otherwise
 */
    public boolean isWithIdenticalSons(){
        for (int i = 0; i < getChildCount(); i++) {
            for (int j = i+1; j < getChildCount(); j++) {
                if ( getChild(i).getName().equals( getChild(j).getName() ) ){
                    return true;
                }
            }
        }
        return false;
    }



/**
 *  @return description:
 *      - 'Yes', if this node has at least one direct son, which is a leave
 *      - 'No', otherwise
 */
    public boolean isWithLeaves(){
        for (int i = 0; i < getChildCount(); i++) {
            if (getChild(i).isLeaf()){
                return true;
            }
        }
        return false;
    }



/**
 *  @return description: If the actual node is a "Generic View Node", this
 *  method returns an HTML String which defines the corresponding
 *  "Generic View" for the node.
 */
    public String getHtmlList(){
        XmlTreeNode[] path = getViewFather().getPath(getName(), _level);
        XmlTreeNode[] header = getHeaderNodes(path);
        HtmlBuffer htmlList = new HtmlBuffer();

        htmlList.openDoc();
        htmlList.openTable(getViewFather().getName()
                                  + " / "
                                  + getName());
        htmlList.openRow();
        for (int i = 0; i < header.length; i++) {
            htmlList.headerCell(header[i].getName());
        }
        htmlList.closeRow();
        htmlList.append(getViewFather().getHtmlListPart(path, 0));
        htmlList.closeTable();
        htmlList.closeDoc();
        return htmlList.toString();
    }



/**
 *  @return description: HTML String (actually a 'HtmlBuffer' objects, which
 *  contains a 'StringBuffer' object),
 *   - which contains the values of every leave
 *   - which are direct suns of an subnode of the actual node
 *   - if the father of this subnode and the subnode itself are directly behind
 *     each other in the path
 *
 *  Example: We have the tree
 *      Contact
 *          ID = D241281
 *          Name
 *              FirstName = Star
 *              LastName = Light
 *          Address
 *              Street = Mission
 *              City = San Francisco
 *          Other
 *              email = ...
 *  and the path
 *      {Contact, Address}
 *  If this method is called for node Contact, we get as result the String
 *      (Mission + San Francisco)
 *
 *  Actually, this method does not return a String but a 'HtmlBuffer' object.
 *  Furthermore, the result does not only return the values of the specified
 *  leaves, but also HTML tags, which define every value to be in its own table
 *  cell.
 *  This method calls itself.
 */
    private HtmlBuffer getHtmlListPart(XmlTreeNode[] imPath, int imIndex){
    HtmlBuffer myListPart = new HtmlBuffer();

/*      Print leave values */
        if(imPath[1].getName().equals(this.getName())){
            myListPart.openRow();     // New line in the HTML table
            for (int i = 0; i < this.getLeaveCount(); i++) {
                myListPart.cell(getLeave(i).getValue());
            }
        }
        else{
            for (int i = 0; i < this.getLeaveCount(); i++) {
                myListPart.keyCell(getLeave(i).getValue());
            }
        }
/*      The end of the path is reached - no more recursion */
        if(imPath[imPath.length - 1].getName().equals(this.getName())){
            myListPart.closeRow(); // Close actual line in the HTML table
            return myListPart;
        }
/*      Call this method for all children which have the same name as the next
        position in the path */
        for (int i = 0; i < getChildCount(); i++) {
            if (imPath[imIndex + 1].getName().equals( getChild(i).getName()) ){
                myListPart.append(getChild(i).getHtmlListPart(imPath, imIndex+1));
            }
        }

        return myListPart;
    }



/**
 *  @return description: If this node is an 'Item' node (if it has the name
 *  'Item', this method returns the corresponding 'ItemId' (the value of the
 *  son with the name 'ItemId'
 */
    public String getItemIdFromItem(){
        if (getName().equals("Item")){
            int i = 0;
            for (i = 0; i < getChildCount(); i++) {
                if (getChild(i).getName().equals("ItemID")) break;
            }
            return getChild(i).getValue();
        }
        return null;
    }



/**
 *  @return description: Array of nodes, which describe the path from the
 *  actual path to the node with the name 'imName' and the level 'imLevel'
 *  This method calls itself.
 */
    private XmlTreeNode[] getPath (String imName, int imLevel){
        XmlTreeNode[] myPath;
        XmlTreeNode[] subPath;

        if ((getName().equals(imName)) && (_level == imLevel)){
            myPath = new XmlTreeNode[1];
            myPath[0] = this;
            return myPath;
        }

        if (_level < imLevel){
            for (int i = 0; i < getChildCount(); i++) {
                subPath = getChild(i).getPath(imName, imLevel);
                if (subPath != null){
                    myPath = new XmlTreeNode[subPath.length + 1];
                    myPath[0] = this;
                    for (int j = 0; j < subPath.length; j++) {
                        myPath[j + 1] = subPath[j];
                    }
                    return myPath;
                }
            }
        }

        return null;
    }



/**
 *  @return description: Array of nodes,
 *   - which are leaves and
 *   - which are direct sons of one of the nodes in the imported Array 'imPath'
 */
    private XmlTreeNode[] getHeaderNodes (XmlTreeNode[] imPath){
        ArrayList headerNodesList = new ArrayList();
        XmlTreeNode[] headerArray;

        for (int i = 0; i < imPath.length; i++) {
            for (int j = 0; j < imPath[i].getLeaveCount(); j++) {
                headerNodesList.add(imPath[i].getLeave(j));
            }
        }

        headerNodesList.trimToSize();
        headerArray = new XmlTreeNode[headerNodesList.size()];
        for (int i = 0; i < headerNodesList.size(); i++) {
            headerArray[i] = (XmlTreeNode)headerNodesList.get(i);
        }
        return headerArray;
    }



/**
 *  @return description: The number of children of the node, which are leaves.
 */
    private int getLeaveCount() {
        if (_children == null)
            return 0;
        else{
            int counter = 0;
            for (int i = 0; i < getChildCount(); i++) {
                if (getChild(i).isLeaf())
                    counter++;
            }
            return counter;
        }
    }



/**
 *  @return description: The leave number 'imLeaveNumber' (I.e. the
 *      'imLeaveNumber' leave in the array _children) If there is no leave
 *      number 'imLeaveNumber' the value 'null' is returned.
 */
    private XmlTreeNode getLeave(int imLeaveNumber) {
        if (_children == null)
            return null;
        else {
            int counter = 0;
            for (int i = 0; i < this.getChildCount(); i++) {
                if (getChild(i).isLeaf()){
                    if (counter == imLeaveNumber)
                        return getChild(i);
                    else
                        counter++;
                }
            }
            return null;
        }
    }



/**
 *  @return description: The number of children of the node. (I.e. the length
 *      of array _children. If _children is null, the value 0 is returned.)
 */
    public int getChildCount() {
        if (_children == null)
            return 0;
        else
            return _children.length;
    }



/**
 *  @return description: The child number 'imChildNumber' (I.e. element number
 *      'imChildNumber' of the array _children) If there is no child number
 *      'imChildNumber' the value 'null' is returned.
 */
    public XmlTreeNode getChild(int imChildNumber) {
        if (_children == null)
            return null;
        else if (_children.length <= imChildNumber)
            return null;
        else
            return _children[imChildNumber];
    }



/**
 *  @return description: 'Yes' if this node is a leave, 'No' otherwise.
 */
    public boolean isLeaf() {
        return _isLeave;
    }


/**
 *  @return description: String to be used to display this leaf in the JTree.
 */
    public String toString() {
    	return _name;
    }

}