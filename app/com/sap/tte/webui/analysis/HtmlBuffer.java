package com.sap.tte.webui.analysis;

import java.util.Calendar;

/**
 * Title:       HtmlBuffer
 * Description: This class should be an extension of class 'StringBuffer'. But
 *  as 'StringBuffer' is 'final', objects of this class have as an
 *  attribute the 'StringBuffer' object '_myBuffer'. All methods are public.
 *  This class provides some append methods to '_myBuffer'. Most of these
 *  methods do not only append the imported String, but also some HTML coding
 *  (tags), others do not have an import paramter at all and do just add some
 *  HTML to the '_myBuffer'.
 *  The methods are self-explaining.
 */
public class HtmlBuffer{

/*  Constants */
    private static String PARAMETER_TYPE = "ForwardType";
    private static String PARAMETER_VALUE = "ForwardValue";
    private static String TYPE_BUTTON = "TypeButton";
    private static String TYPE_TREE = "TypeTree";
    private static String TYPE_GENERIC = "TypeGenericView";

    private static final char EOL = '\n';
    private static final String SPACE     = "&nbsp;";
    private static final String LONGSPACE = new String(SPACE+SPACE+SPACE);
    private static final String LINE = "<td bgcolor=#FFFFFF></td>";
    private static final String[] TREE_ROW_COLOR = {"#9DBBD7",
                                                    "#C6CED6",
                                                    "#D1D8DF",
                                                    "#DDE2E7",
                                                    "#E9ECEF",
                                                    "#F4F6F8",
                                                    "#FFFFFF"};



/*  Attributes */
    private StringBuffer    _myBuffer;      // See Class description
    private String          _callerRef;     // String path for HTML links
    private String          _picturesPath;  // String path: Storage of the pictures

    private int _lastLevel = -1;



/**
 *  Constructor
 */
    public HtmlBuffer() {
        _myBuffer = new StringBuffer();
    }

/**
 *  Constructor
 *  @imCallerRef : The URL (path), which is used for all links
 *  @imPicturePath : The path, which contains all used pictures
 */
    public HtmlBuffer(String imCallerRef, String imPicturePath) {
        _myBuffer = new StringBuffer();
        _callerRef = imCallerRef;
        _picturesPath = imPicturePath;
    }


/**
 *  Methods for 'StringBuffer' operations
 */
    public void append(String imString){
        _myBuffer.append(imString);
    }

    public void append(HtmlBuffer imBuffer){
        _myBuffer.append(imBuffer.toString());
    }

    public void appendLn(String imString){
        _myBuffer.append(imString);
        _myBuffer.append(EOL);
    }

    public String toString(){
        return _myBuffer.toString();
    }



/**
 *  HTML creating Methods for the generic lists
 */
    public void openDoc(){
        this.appendLn("<html><body>");
    }

    public void closeDoc(){
        this.appendLn("</body></html>");
    }

    public void openTable(String imTitle){
        this.appendLn("<table cellpadding=0 cellspacing=0 border=0>");
        this.appendLn("<tr bgcolor=#336699>"
            + "<td colspan=99><h2 align=center><font color=#FFFFFF>"
            + imTitle
            + "</font></h2></td>"
            + "</tr>");
    }

    public void closeTable(){
        this.appendLn("</table>");
    }

    public void openRow(){
        this.appendLn("<tr><td height=1 bgcolor=#FFFFFF colspan=99></td></tr>");
        this.appendLn("<tr bgcolor=#D1D8DF>" + LINE);
    }

    public void closeRow(){
        this.appendLn("</tr>");
    }

    public void cell(String imString){
        if(imString == null)
            imString = "";
        this.appendLn("<td nowrap>" + imString + "</td>" + LINE);
    }

    public void keyCell(String imString){
        this.appendLn("<td bgcolor=#E9ECEF>" + imString + "</td>" + LINE);
    }

    public void headerCell(String imString){
        this.appendLn("<th bgcolor=#9DBBD7 nowrap>&nbsp;" + imString + "&nbsp;</th>" + LINE);
    }



/**
 *  HTML creating Methods for the Servlet
 */
    public void treeCellEmpty(){
        this.append("<td width=16></td>");
    }

    public void treeOpenTreeRef(int imID){
    Calendar now = Calendar.getInstance();
        this.appendLn("<a href="
                      + _callerRef
                      + "?"
                      + PARAMETER_TYPE
                      + "="
                      + TYPE_TREE
                      + "&timeStamp="
                      + now.getTime().getTime()
                      + "&"
                      + PARAMETER_VALUE
                      + "="
                      + imID
                      + "#" + imID + " name=" + imID
                      + ">");
    }

    public void treeOpenGenericRef(int imID){
        this.appendLn("<a href="
                      + _callerRef
                      + "?"
                      + PARAMETER_TYPE
                      + "="
                      + TYPE_GENERIC
                      + "&"
                      + PARAMETER_VALUE
                      + "="
                      + imID
                      + ">");
    }

    public void treeCloseRef(){
        this.appendLn("</a>");
    }

    public void treeVerticalLine(){
        this.treeCellEmpty();
        this.appendLn("<td width=1 bgcolor=#336699></td>");
    }

    public void treeCloseRow(){
        this.appendLn("</tr>");
    }

    public void treeIcon(String imIcon){
        this.append("<img src=\"" + _picturesPath + imIcon + "\" "
                    +"height=\"15\" width=\"16\" border=\"0\" ></img>");
    }

    public void treeFolderOpen(int imID){
        this.append("<td>");
        this.treeOpenTreeRef(imID);
        this.treeIcon("folder_open.gif");
        this.treeCloseRef();
        this.append("</td>");
    }

    public void treeFolderClose(int imID){
        this.append("<td>");
        this.treeOpenTreeRef(imID);
        this.treeIcon("folder_close.gif");
        this.treeCloseRef();
        this.append("</td>");
    }

    public void treeGenericButton(int imID){
        this.append("<td>");
        this.treeOpenGenericRef(imID);
        this.treeIcon("generic_list.gif");
        this.treeCloseRef();
        this.append("</td>");
    }

    public void treeFolderLeave(){
        this.append("<td>");
        this.treeIcon("doc.gif");
        this.append("</td>");
    }

    public void treeCell(String imLabel){
        this.append("<td>" + imLabel + "</td>");
    }

    public void treeValueCell(String imLabel){
        this.append("<td><b>" + imLabel + "</b></td>");
    }

    public void treeCellWithSpace(String imLabel, int imCount, int imID){
        this.append("<td>");
        for (int i=0; i<imCount; i++)
            this.append(LONGSPACE);
        this.treeOpenTreeRef(imID);
        this.append(imLabel);
        this.treeCloseRef();
        this.append("</td>");
    }

    public void treeLeaveWithSpace(String imLabel, int imCount){
        this.append("<td>");
        for (int i=0; i<imCount; i++)
            this.append(LONGSPACE);
        this.append(imLabel);
        this.append("</td>");
    }

    public void treeCellWithColSpan(String imLabel, int imColSpan, int imID){
        if (imColSpan > 1)
            this.append("<td  nowrap colspan=" + imColSpan + ">");
        else
            this.append("<td  nowrap>");
        this.append(SPACE);
        this.treeOpenTreeRef(imID);
        this.append(imLabel);
        this.treeCloseRef();
        this.append("</td>");
    }

    public void treeLeaveWithColSpan(String imLabel, int imColSpan){
        if (imColSpan > 1)
            this.append("<td nowrap colspan=" + imColSpan + ">");
        else
            this.append("<td  nowrap>");
        this.append(SPACE + " " + imLabel);
        this.append("</td>");
    }

    public void treeInvisibleRow(int imColumnCount){
        this.append("<tr>");

        // Folder Icons
        for (int i = 0; i < imColumnCount; i++) {
            this.append("<td width=16></td>");
        }
        // Label
        this.append("<td width=150></td>");
        // Vertical line
        for (int i = 0; i < 2; i++) {
            this.append("<td></td>");
        }
        // Value
        this.append("<td width=150></td>");
        // Vertical line
        for (int i = 0; i < 2; i++) {
            this.append("<td></td>");
        }
        // Generic List button
        this.append("<td></td>");

        this.append("</tr>");
    }

    public void openFrameSet(){
        this.appendLn("<html>");
        this.appendLn("<frameset framespacing=0 border=0 frameborder=0 cols=20,80>");
    }

    public void closeFrameSet(){
        this.appendLn("<noframes><body>");
        this.appendLn("<p>This page uses frames, but your browser doesn't support them.</p>");
        this.appendLn("</body></noframes>");
        this.appendLn("</frameset>");
        this.appendLn("</html> ");
    }

    public void frame(String imName, String imSource, boolean imIsSrolling){
        if (imIsSrolling)
            this.appendLn("<frame name="
                          + imName
                          + " src="
                          + imSource
                          + " scrolling=auto>");
        else
            this.appendLn("<frame name="
                          + imName
                          + " src="
                          + imSource
                          + " scrolling=no>");
    }

    public void button(String imTarget, String imLabel){
        this.appendLn("<tr bgcolor=#D1D8DF><td>");
        this.appendLn("<button type=button value=navigation name=myButton"
                      + " onClick=parent.main.location.href='"
                      + imTarget
                      + "'>"
                      + imLabel
                      + "</button></td>");
        this.appendLn("</td></tr>");
        closeRow();
    }

    public void returnButton(String imTarget, String imLabel){
        this.appendLn("<tr bgcolor=#D1D8DF><td>");
        this.appendLn("<button type=button value=navigation name=myButton"
                      + " onClick=parent.location.href='"
                      + imTarget
                      + "'>"
                      + imLabel
                      + "</button></td>");
        this.appendLn("</td></tr>");
        closeRow();
    }

    public void openButtonTable(String imTitle){
        this.appendLn("<table cellpadding=6 cellspacing=1 border=0>");
        this.appendLn("<tr bgcolor=#336699>"
            + "<td><h2 align=center><font color=#FFFFFF>"
            + imTitle
            + "</font></h2></td>"
            + "</tr>");
    }



/**
 *  This methods open a new table row for the tree representation. To make it
 *  look nicer, the background color is choosen depending on the level of the
 *  actual row within the tree. (All children of a certain node (row) have the
 *  level n+1, if there father has the level n. The root has the level 0)
 *  And to make even this look nicer: If the actual row has a different level
 *  than the last one, a horizontal line (a table row with hight 1 or 2) is
 *  printed in another background color.
 */
    public void treeOpenRow(int imLevel){

        if (_lastLevel >= 0){
            if (imLevel > _lastLevel){
                if (_lastLevel - 2 >= 0){
                    if (_lastLevel - 2 < TREE_ROW_COLOR.length)
                        this.appendLn("<tr><td height=1 bgcolor="
                        + TREE_ROW_COLOR[_lastLevel-2]
                        + " colspan=99></td></tr>");
                }
                else
                    this.appendLn("<tr><td height=1 bgcolor="
                    + TREE_ROW_COLOR[0]
                    + " colspan=99></td></tr>");
            }
            if (imLevel < _lastLevel){
                if (_lastLevel + 1 < TREE_ROW_COLOR.length)
                    this.appendLn("<tr><td height=2 bgcolor="
                    + TREE_ROW_COLOR[_lastLevel+1]
                    + " colspan=99></td></tr>");
            }
        }
        _lastLevel = imLevel;

        if (imLevel < TREE_ROW_COLOR.length)
            this.appendLn("<tr bgcolor="
                          + TREE_ROW_COLOR[imLevel]
                          + ">");
        else
            this.appendLn("<tr>");
    }
}









