/*
 * Created on Sep 15, 2005
 *
 * To change the template for this generated file go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
package com.sap.tte.remote.shared.command;

/**
 * Call to Get Tax Jurisdiction Code from External Interface <p>
 * 
 * <b>Import parameters</b>
 * <ul>
 *     <li> I_Country    (required):	Country name
 *     <li> I_State      (required):	State
 *     <li> I_County     (optional):	County name
 *     <li> I_City       (optional):	City name
 *     <li> I_Zipcode    (required):	Zipcode
 *     <li> I_TXJCDLV1   (optional):	Tax Jurcode Level1 
 *     <li> I_TXJCDLV2   (optional):	Tax Jurcode Level2
 *     <li> I_TXJCDLV3   (optional):	Tax Jurcode Level3
 *     <li> I_TXJCDLV4   (optional):	Tax Jurcode Level4 
 *
 * </ul><p>
 * 
 * <b>Export parameters</b>
 * <ul>
 *     <li> O_Country    :	Country name
 *     <li> O_State      :	State
 *     <li> O_County     :	County name
 *     <li> O_City       :	City name
 *     <li> O_Zipcode    :	Zipcode
 *     <li> O_TXJCDLV1   :	Tax Jurcode Level1 
 *     <li> O_TXJCDLV2   :	Tax Jurcode Level2
 *     <li> O_TXJCDLV3   :	Tax Jurcode Level3
 *     <li> O_TXJCDLV4   :	Tax Jurcode Level4 
 * 	   <li> O_TXJCD      :	Tax Jurcode 
 *     <li> O_OUTOFCITY  :	Out of city    
 * </ul>
 */

public interface DetermineTaxJurCode {
   //	input variables
   public static final String I_COUNTRY                 = "I_COUNTRY";
   public static final String I_STATE                   = "I_STATE";
   public static final String I_COUNTY                  = "I_COUNTY";
   public static final String I_CITY                    = "I_CITY";
   public static final String I_ZIPCODE                 = "I_ZIPCODE";
   public static final String I_TXJCDLV1                = "I_TXJCDLV1";
   public static final String I_TXJCDLV2                = "I_TXJCDLV2";
   public static final String I_TXJCDLV3                = "I_TXJCDLV3";
   public static final String I_TXJCDLV4                = "I_TXJCDLV4";
   
   // output variables
   public static final String O_COUNTRY                 = "O_COUNTRY";
   public static final String O_STATE                   = "O_STATE";
   public static final String O_COUNTY                  = "O_COUNTY";
   public static final String O_CITY                    = "O_CITY";
   public static final String O_ZIPCODE                 = "O_ZIPCODE";
   public static final String O_TXJCDLV1                = "O_TXJCDLV1";
   public static final String O_TXJCDLV2                = "O_TXJCDLV2";
   public static final String O_TXJCDLV3                = "O_TXJCDLV3";
   public static final String O_TXJCDLV4                = "O_TXJCDLV4";
   public static final String O_TXJCD                   = "O_TXJCD";
   public static final String O_OUTOFCITY               = "O_OUTOFCITY";
   
}
