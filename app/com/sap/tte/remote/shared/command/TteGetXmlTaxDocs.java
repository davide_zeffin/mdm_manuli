/*
 * Created on 28.09.2004
 *
 * To change this generated comment go to 
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
package com.sap.tte.remote.shared.command;

/**
 * Set the TTE Input Document xml<p>
 * 
 * <b>Export parameters</b>
 * <ul>
 *     <li>TTEXmlInputDocString :   the xml input document as String
 *     <li>TTEXmlOutputDocString :  the xml Output document as String
 *     <li>oReturnCode :     the return code sucess or failure
 *    
 * </ul>
 */
 
public interface TteGetXmlTaxDocs {
	
	// input variables
//	 input variables
    public static final String DOCUMENT_ID                 = "documentId";


    // output variables
    public static final String INPUT_DOCUMENT              = "inputDocument";
    public static final String OUTPUT_DOCUMENT             = "outputDocument";	 
		
}
