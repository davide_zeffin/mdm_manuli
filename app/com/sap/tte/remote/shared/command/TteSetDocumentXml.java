/*
 * Created on 28.09.2004
 *
 * To change this generated comment go to 
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
package com.sap.tte.remote.shared.command;

/**
 * Set the TTE Input Document xml<p>
 * 
 * <b>Import parameters</b>
 * <ul>
 *     <li>TTEXmlDocString :	the XML TTE Input Document as String
 * </ul><p>
 * 
 * <b>Export parameters</b>
 * <ul>
 *     <li>oReturnCode :     the return code sucess or failure
 *    
 * </ul>
 */
 
public interface TteSetDocumentXml {
	
	// input variables
	    public static final String TTEXMLDOCSTRING                = "TTEXmlDocString";
	    public static final String ORETURNCODE                    = "oReturnCode";  
		
}
