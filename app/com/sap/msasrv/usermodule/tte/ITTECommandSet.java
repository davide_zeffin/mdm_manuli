/*
 * Created on Dec 14, 2004
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package com.sap.msasrv.usermodule.tte;

import com.sap.msasrv.socket.server.module.ICommandSet;

/**
 * @author I026584
 *
 * TODO To change the template for this generated type comment go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
public interface ITTECommandSet extends ICommandSet {
	//CommandConstants.java is replaced by this interface.
    	/**
	    * public fields: Add a line here to have a command registered
	    * by the CommandMgr.
	    */
	public static final String TTE_GET_XML_TAX_DOCS    		= "TteGetXmlTaxDocs";
	public static final String TTE_SET_DOCUMENT_XML    		= "TteSetDocumentXml";
	public static final String TTE_GET_ANALYSIS_XML    		= "TteGetAnalysisXml";
	public static final String TTE_DETERMINE_TAX_JUR_CODE   = "DetermineTaxJurCode";
		
	
}
