/*
 * Created on Nov 30, 2004
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package com.sap.msasrv.usermodule.cfe;

import com.sap.msasrv.socket.server.module.IModuleMetaData;

/**
 * @author I026584
 *
 * TODO To change the template for this generated type comment go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
public class CFEModuleMetaData implements IModuleMetaData {

	/* (non-Javadoc)
	 * @see com.sap.msasrv.socket.server.module.IModuleMetaData#getModuleId()
	 */
	public String getModuleId() {
		return "CFE";
	}

	/* (non-Javadoc)
	 * @see com.sap.msasrv.socket.server.module.IModuleMetaData#getIdsExtendsList()
	 */
	public String[] getIdsExtendsList() {
		return null;
	}

	/* (non-Javadoc)
	 * @see com.sap.msasrv.socket.server.module.IModuleMetaData#getCommandSetInterface()
	 */
	public String getCommandSetInterface() {
		return "com.sap.msasrv.usermodule.cfe.ICFECommandSet";
	}

	/* (non-Javadoc)
	 * @see com.sap.msasrv.socket.server.module.IModuleMetaData#getImplPkgsList()
	 */
	public String[] getImplPkgsList() {
		return new String[] {"com.sap.spc.remote.server.command"};
	}

	/* (non-Javadoc)
	 * @see com.sap.msasrv.socket.server.module.IModuleMetaData#getSessionClass()
	 */
	public String getSessionClass() {
		return null;
	}

	/* (non-Javadoc)
	 * @see com.sap.msasrv.socket.server.module.IModuleMetaData#getIdsListForSessionReplacement()
	 */
	public String[] getIdsListForSessionReplacement() {
		return null;
	}

}
