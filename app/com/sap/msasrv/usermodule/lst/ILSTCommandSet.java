/*
 * Created on 08.03.2005
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package com.sap.msasrv.usermodule.lst;

import com.sap.msasrv.socket.server.module.ICommandSet;

/**
d027131 *
 */
public interface ILSTCommandSet extends ICommandSet {
	public static final String DETERMINE_LISTINGS = "DetermineListings";
	public static final String GET_RELEVANT_LI_ATTRIBUTES = "GetRelevantLiAttributes";
	public static final String PRODUCT_PROPOSAL = "ProductProposal";
	public static final String SELECT_LISTINGS = "SelectListings";
	public static final String CLEAR_BUFFER = "ClearListingBuffer";
}

