/*
 * Created on Oct 19, 2004
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package com.sap.msasrv.usermodule.spe;

import com.sap.msasrv.socket.server.module.IModuleMetaData;

/**
 * @author I026584
 *
 * TODO To change the template for this generated type comment go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
public class SPEModuleMetaData implements IModuleMetaData {

	/* (non-Javadoc)
	 * @see com.sap.msasrv.socket.server.module.IModuleMetaData#getModuleId()
	 */
	public String getModuleId() {
		return "SPE";
	}

	/* (non-Javadoc)
	 * @see com.sap.msasrv.socket.server.module.IModuleMetaData#getIdsExtendsList()
	 */
	public String[] getIdsExtendsList() {
		return new String[0];
	}

	/* (non-Javadoc)
	 * @see com.sap.msasrv.socket.server.module.IModuleMetaData#getCommandSetInterface()
	 */
	public String getCommandSetInterface() {
		return "com.sap.msasrv.usermodule.spe.ISPECommandSet";
	}

	/* (non-Javadoc)
	 * @see com.sap.msasrv.socket.server.module.IModuleMetaData#getImplPkgsList()
	 */
	public String[] getImplPkgsList() {
		//Yes.. not available! in 4.0..
		return new String[0];
	}

	/* (non-Javadoc)
	 * @see com.sap.msasrv.socket.server.module.IModuleMetaData#getSessionClass()
	 */
	public String getSessionClass() {
		return null;
	}

	/* (non-Javadoc)
	 * @see com.sap.msasrv.socket.server.module.IModuleMetaData#getIdsListForSessionReplacement()
	 */
	public String[] getIdsListForSessionReplacement() {
		return null;
	}

}
