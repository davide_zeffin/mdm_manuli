/*
 * Created on Nov 30, 2004
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package com.sap.msasrv.usermodule.fge;

import com.sap.msasrv.socket.server.module.ICommandSet;

/**
 * @author I026584
 *
 * TODO To change the template for this generated type comment go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
public interface IFGECommandSet extends ICommandSet {
    /**
	    * public fields: Add a line here to have a command registered
	    * by the CommandMgr.
	    */
	    public static final String DETERMINE_FREE_GOODS = "DetermineFreeGoods";
}
