/*
 * Created on Sep 27, 2004
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package com.sap.msasrv.usermodule;

/**
 * @author I026584
 *
 * TODO To change the template for this generated type comment go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
public interface ModuleMetaDatas {
	// Add your module meta data file as "public static final String" memober variable.
	
	//Order is very important If SPC is extends SCE, SPE then SPC module meta data class should come after SCE and SPC module data classes...
	// Or else you will get an error saying un known module.
	public static final String SCE_MODULE_METADATA_FILE 		= "com.sap.msasrv.usermodule.sce.SCEModuleMetaData";
	public static final String SPE_MODULE_METADATA_FILE 		= "com.sap.msasrv.usermodule.spe.SPEModuleMetaData";
	public static final String SPC_MODULE_METADATA_FILE 		= "com.sap.msasrv.usermodule.spc.SPCModuleMetaData"; // SPC = SCE + SPE
	public static final String TTE_MODULE_METADATA_FILE 		= "com.sap.msasrv.usermodule.tte.TTEModuleMetaData";
	public static final String CFE_MODULE_METADATA_FILE 		= "com.sap.msasrv.usermodule.cfe.CFEModuleMetaData";
	public static final String FGE_MODULE_METADATA_FILE 		= "com.sap.msasrv.usermodule.fge.FGEModuleMetaData"; // FGE = FGE + CFE
	public static final String LST_MODULE_METADATA_FILE 		= "com.sap.msasrv.usermodule.lst.LSTModuleMetaData"; // FGE = FGE + CFE
	
	//MSA Group
	public static final String MSA_MODULE_METADATA_FILE 		= "com.sap.msasrv.usermodule.msa.MSAModuleMetaData"; // MSA = SPC + TTE + FGE

}
