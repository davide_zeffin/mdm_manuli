/*
 * Created on Oct 19, 2004
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package com.sap.msasrv.usermodule.spc;

import com.sap.msasrv.socket.server.module.ICommandSet;

/**
 * @author I026584
 *
 * TODO To change the template for this generated type comment go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
public interface ISPCCommandSet extends ICommandSet {
	// SPC Commands 
	//public static final String CALCULATE_TIME_PERIODS		= "CalculateTimePeriods";
	public static final String CREATE_DOCUMENT				= "CreateDocument";
	public static final String CHANGE_DOCUMENT				= "ChangeDocument";
	public static final String REMOVE_DOCUMENT				= "RemoveDocument";
	//public static final String GET_CONDITION_RECORDS		= "GetConditionRecords";
	//public static final String GET_CONDITIONS		        = "GetConditions"; Do not register this abstract command.
	public static final String GET_DOCUMENTS				= "GetDocuments";
	public static final String GET_ITEMS					= "GetItems";
	//public static final String CREATE_ITEM					= "CreateItem";
	public static final String CREATE_ITEMS					= "CreateItems";
	//public static final String CHANGE_ITEM					= "ChangeItem";
	public static final String CHANGE_ITEM_CONFIG			= "ChangeItemConfig";
	public static final String CHANGE_ITEMS					= "ChangeItems";
	public static final String REMOVE_ITEMS					= "RemoveItems";
	public static final String COPY_ITEM					= "CopyItem";
	public static final String GET_ITEM_INFO				= "GetItemInfo";
	public static final String GET_DOCUMENT_AND_ITEM_INFO   = "GetDocumentAndItemInfo";
	//public static final String GET_ITEMS_OF_PRODUCT			= "GetItemsOfProduct";
	//public static final String GET_ITEMS_OF_PRODUCTS		= "GetItemsOfProducts";
	public static final String CHANGE_ITEM_PRODUCT			= "ChangeItemProduct";
//	public static final String REMOVE_PRODUCT				= "RemoveProduct";
//	public static final String CREATE_ADMIN_SESSION			= "CreateAdminSession";
	//public static final String GET_ADDITIONAL_DATA			= "GetAdditionalData";
	//public static final String GET_PROTOCOL                 = "GetProtocol";

	// SPE Commands
	public static final String GET_RELEVANT_ATTRIBUTES		= "GetRelevantAttributes";
	public static final String GET_PRICING_PROCEDURE_INFO	= "GetPricingProcedureInfo";
	//public static final String SET_ITEM_SALES_QUANTITY		= "SetItemSalesQuantity";
	//public static final String SET_ITEM_SALES_QUANTITIES	= "SetItemSalesQuantities";
	//public static final String PRICING_ITEM					= "PricingItem";
	public static final String GET_PRICING_ITEM_INFO		= "GetPricingItemInfo";
	//public static final String GET_PRODUCT					= "GetProducts";
	//public static final String GET_PRODUCT_PROPERTIES		= "GetProductProperties";
	public static final String GET_CONFIG_LOAD_STATUS       = "GetConfigLoadStatus";
	public static final String GET_CONFIG_LOAD_MESSAGES     = "GetConfigLoadMessages";
	public static final String SET_INITIAL_CONFIGURATION    = "SetInitialConfiguration";
	public static final String GET_INITIAL_CONFIG           = "GetInitialConfig";
	public static final String COMPARE_CONFIGURATIONS       = "CompareConfigurations";
	public static final String GET_PRICING_DOCUMENT_INFO	= "GetPricingDocumentInfo";
	public static final String PRICING_COMPLETE_DOCUMENT	= "PricingCompleteDocument";
	public static final String GET_ITEM_CONDITIONS			= "GetItemConditions";
	public static final String GET_ALL_ITEMS_CONDITIONS		= "GetAllItemsConditions";
	public static final String GET_ALL_CURRENCY_UNITS		= "GetAllCurrencyUnits";
	public static final String GET_ALL_PRICING_PROCEDURES	= "GetAllPricingProcedures";
	//public static final String ADD_PRICING_CONDITION		= "AddPricingCondition";
	//public static final String CHANGE_PRICING_CONDITION		= "ChangePricingCondition";
	public static final String GET_DOCUMENT_INFO			= "GetDocumentInfo";
	//public static final String REMOVE_PRICING_CONDITION		= "RemovePricingCondition";
	public static final String LOAD_PRICING_DOCUMENT		= "LoadPricingDocument";
	//public static final String GET_ALL_QUANTITY_UNITS		= "GetAllQuantityUnits";
	public static final String ADD_PRICING_CONDITIONS		= "AddPricingConditions";
	public static final String CHANGE_PRICING_CONDITIONS	= "ChangePricingConditions";
	public static final String REMOVE_PRICING_CONDITIONS	= "RemovePricingConditions";
	//public static final String GET_PRICING_USER_EXIT_INFO	= "GetPricingUserExitInfo";
	//public static final String PRINT_SUM_CONDITIONS         = "PrintSumConditions";
	//public static final String PRINT_ITEM_CONDITIONS        = "PrintItemConditions";
	public static final String CURR_CONVERT_TO_FOREIGN		= "CurrConvertToForeign";
	public static final String GET_EXCHANGE_RATE			= "GetExchangeRate";

	// SCE Commands
	//public static final String GET_KNOWLEDGEBASE          = "GetKnowledgeBase";
	public static final String GET_PROFILES_OF_KB         = "GetProfilesOfKB";
	public static final String GET_INSTANCES				= "GetInstances";
	//public static final String GET_DECOMPS					= "GetDecompositions";
	//public static final String GET_SPECS					= "GetSpecializations";
	public static final String GET_CSTIC_GROUPS				= "GetCsticGroups";
	public static final String GET_FILTERED_CSTICS_AND_VALUES= "GetFilteredCsticsAndValues";
	public static final String GET_CONFLICTS				= "GetConflicts";
	public static final String GET_CSTIC_CONFLICTS          = "GetCsticConflicts";
	public static final String GET_CONFIG_INFO				= "GetConfigInfo";
	public static final String GET_CONFIG_ITEM_INFO			= "GetConfigItemInfo";
	public static final String GET_CONFIG_ITEM_INFO_XML    	= "GetConfigItemInfoXml";
	//public static final String GET_CONFIG_ITEMS_INFO		= "GetConfigItemsInfo";
	//public static final String GET_DISPLAY_DATA			= "GetDisplayData";
	//public static final String GET_CSTICS_MASTER_DATA		= "GetCsticsMasterData";
	//public static final String GET_INSTANCE_FACTS			= "GetInstanceFacts";
	//public static final String GET_JUSTIFICATIONS			= "GetJustifications";
	//public static final String DELETE_FACT				= "DeleteFact";
	//public static final String SET_CONFIG_TRACE_PARAMS	= "SetConfigTraceParams";
	//public static final String GET_CONFIG_TRACE_PARAMS   	= "GetConfigTraceParams";
	//public static final String GET_CONFIG_TRACE   		= "GetConfigTrace";
	//public static final String DELETE_CONFIG_TRACE		= "DeleteConfigTrace";
	public static final String RESET_CONFIG                 = "ResetConfig";
    public static final String GET_CONFLICTING_ASSUMPTIONS   = "GetConflictingAssumptions";
	public static final String DELETE_CONFLICTING_ASSUMPTION = "DeleteConflictingAssumption";


	public static final String CREATE_INSTANCE				= "CreateInstance";
	public static final String DELETE_INSTANCE				= "DeleteInstance";
	//public static final String UNSPECIALIZE				= "Unspecialize";
	public static final String SET_CSTIC_VALUES				= "SetCsticValues";
	public static final String SET_CSTICS_VALUES			= "SetCsticsValues";

	public static final String GET_PRODUCT_VARIANTS         = "GetProductVariants";
	//public static final String GET_CONFIG			        = "GetConfig";
	//public static final String CHECK_AND_COMPLETE_CONFIG  = "CheckAndCompleteConfig";
	public static final String GET_CONFIG_HAS_CHANGED		= "GetConfigHasChanged";

    //public static final String FIND_KNOWLEDGEBASE     	= "FindKnowledgeBase";
    //public static final String GET_VARCOND_KEYS     		= "GetVarcondKeys";
    //public static final String GET_CLASS_MASTER_DATA   	= "GetClassMasterData";
	//public static final String MANUFACTURING_COMPLETION	= "ManufacturingCompletion";
	//public static final String MANUFACTURING_COMPLETION_MULTI = "ManufacturingCompletionMulti";
	//public static final String GET_KNOWLEDGEBASE_ID		= "GetKnowledgeBaseId";
	//public static final String GET_KNOWLEDGEBASE_ID_MULTI	= "GetKnowledgeBaseIdMulti";
	public static final String CREATE_CONFIG                = "CreateConfig";
	//public static final String REMOVE_CONFIG                = "RemoveConfig";
	public static final String FIND_KNOWLEDGEBASES   		= "FindKnowledgeBases";
	//public static final String CHANGE_TO_PRODUCT_VARIANT    = "ChangeToProductVariant";
	//public static final String CONVERT_IDOC_TO_XML        = "ConvertIdocToXml";
	public static final String GETVARTABLE					= "GetVarTable";
	public static final String GETALLVALIDGRIDVARIANTS		= "GetAllValidGridVariants";

	// Misc commands
	//public static final String GET_XML_PRICING_TRACE		= "GetXMLPricingTrace";
	//public static final String GET_PRICING_TRACE			= "GetPricingTrace";

	//public static final String GET_DATA_ELEMENT_TEXTS		= "GetDataElementTexts";

	//public static final String GET_DOMAIN_TEXTS			= "GetDomainTexts";
	//public static final String GET_MESSAGE				= "GetMessage";

	//public static final String GET_ALL_CONDITIONTYPES		= "GetAllConditionTypes";
	public static final String GET_ALL_HEADER_CONDITIONS	= "GetAllHeaderConditions";

	public static final String CONVERT_ISO_CODE_TO_UNIT_OF_MEASUREMENT = "ConvertIsoCodeToUnitOfMeasurement";
	public static final String CONVER_INTERNAL_PRODUCT_IDS_TO_PRODUCT_GUIDS = "ConvertInternalProductIdsToProductGUIDs";

    //public static final String DUMP_KB_BUFFER               = "DumpKnowledgeBaseBuffer";

    //There are new in 5.0 requested by MSA.
    public static final String LOAD_PRICING_DOCUMENT_FROM_DB_XML = "LoadPricingDocumentFromDbXML";
    public static final String GET_PRICING_CONDITION_AS_XML = "GetPricingConditionAsXML";
    public static final String GET_XML_PRICING_TRACE = "GetXMLPricingTrace";
    //public static final String GETALLMATRIXPRODUCTVARIANTS = "GetAllValidGridVariants";
    public static final String GET_CONFIG_IN_CSV_FORMAT     = "GetConfigInCSVFormat";
    public static final String IMPORT_MANAGER     			= "ImportManager";
    
    //TODO: Just for testing remove later..
    public static final String CONVERSION_TEST_COMMAND = "ConversionTestCommand";
    
	public static final String SET_EXTERNAL_TAX_PACKAGE  = "SetExternalTaxPackage";
    
    //Yet to analize whether required or not and yet to implement if required.
    //As JSP UIs COL is not getting compiled, just adding the command. TODO: 
	// 20050308-kha: removed Heartbeat and SetClientParameters which definitely have to be out of the MSA server
	// (and the normal IPC as well)
	public static final String GET_CONFIGS                  = "GetConfigs";
	public static final String GET_AVAILABLE_CONDITIONTYPES	= "GetAvailableConditionTypes";
	public static final String GET_ALL_PHYSICALUNITS		= "GetAllPhysicalUnits";
//	public static final String GET_AVAILABLE_PHYSICALUNITS		= "GetAvailablePhysicalUnits";
	public static final String PRICING_ITEMS				= "PricingItems";
//	public static final String PRICING_ALL_ITEMS			= "PricingAllItems";
	public static final String SPECIALIZE					= "Specialize";
	public static final String UNSPECIALIZE				= "Unspecialize";
	public static final String SET_ITEM_CONFIG_RESTRICTIONS = "SetItemConfigRestrictions";
	public static final String DELETE_CSTIC_VALUES		= "DeleteCsticValues";
	public static final String DELETE_CSTICS_VALUES		= "DeleteCsticsValues";
	public static final String GET_FACTS					= "GetFacts";
//   	public static final String GET_CSTICS_AND_VALUES		= "GetCsticsAndValues";
	public static final String GET_CSTIC					= "GetCstic";
	public static final String GET_CSTIC_VALUES				= "GetCsticValues";
	public static final String CONVERT_INTERNAL_TO_EXTERNAL	= "ConvertInternalToExternal";
	public static final String CONVERT_EXTERNAL_TO_INTERNAL	= "ConvertExternalToInternal";
	
	// 20050525-MMR : Add commands which will be used for the condition master data display
	public static final String GET_CONDITION_GROUP_ATTRIBUTES          = "GetConditionGroupAttributes";
	public static final String GET_CONDITION_RECORDS_FROM_DATABASE     = "GetConditionRecordsFromDatabase";
//	public static final String SELECT_CONDITION_RECORDS                = "SelectConditionRecords"; This is an abstract class and cannot be used as command directly
	public static final String GET_CONDITION_TABLE_FIELD_LABEL         = "GetConditionTableFieldLabel"; 
	public static final String CONVERT_FIELDNAMES_INTERNAL_TO_EXTERNAL = "ConvertFieldnameInternalToExternal";
	public static final String GET_CONDITION_RECORDS_FROM_REF_GUID     = "GetConditionRecordsFromRefGuid";
	public static final String GET_NUMBER_OF_CONDITION_RECORD_SELECTED = "GetNumberOfConditionRecordSelected";
		
}
