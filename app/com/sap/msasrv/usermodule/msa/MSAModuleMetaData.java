/*
 * Created on Dec 9, 2004
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package com.sap.msasrv.usermodule.msa;

import com.sap.msasrv.socket.server.module.IModuleMetaData;

/**
 * @author I026584
 *
 * TODO To change the template for this generated type comment go to
 * Window - Preferences - Java - Code Style - Code Templates
 */

// A super module for MSA Scenario. Depending on MSA requirements modify this module to load the corresponding modules.
// This module only extends modules but will not have any commands as such. (You can treat as Module Group)
public class MSAModuleMetaData implements IModuleMetaData {

	/* (non-Javadoc)
	 * @see com.sap.msasrv.socket.server.module.IModuleMetaData#getModuleId()
	 */
	public String getModuleId() {
		return "MSA";
	}

	/* (non-Javadoc)
	 * @see com.sap.msasrv.socket.server.module.IModuleMetaData#getIdsExtendsList()
	 */
	public String[] getIdsExtendsList() {
		//This Module Group containes the following Modules.
		// IPC = SPE + SCE
		// FGE = FGE + CFE
		// TTE = TTE
        // LST = LST
		return new String[] {"SPC","TTE","FGE","LST"};
	}

	/* (non-Javadoc)
	 * @see com.sap.msasrv.socket.server.module.IModuleMetaData#getCommandSetInterface()
	 */
	public String getCommandSetInterface() {
		//No commandSet available. This is just a module group.
		return null;
	}

	/* (non-Javadoc)
	 * @see com.sap.msasrv.socket.server.module.IModuleMetaData#getImplPkgsList()
	 */
	public String[] getImplPkgsList() {
		// No implementation available. This is just a module group.
		return null;
	}

	/* (non-Javadoc)
	 * @see com.sap.msasrv.socket.server.module.IModuleMetaData#getSessionClass()
	 */
	public String getSessionClass() {
		return "com.sap.spc.remote.server.MSASPCSession";
	}

	/* (non-Javadoc)
	 * @see com.sap.msasrv.socket.server.module.IModuleMetaData#getIdsListForSessionReplacement()
	 */
	public String[] getIdsListForSessionReplacement() {
		return new String[] {"SPC","TTE","FGE","LST"};
	}

}
