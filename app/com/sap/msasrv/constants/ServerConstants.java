/************************************************************************

	Copyright (c) 2000 by SAP AG

	All rights to both implementation and design are reserved.

	Use and copying of this software and preparation of derivative works
	based upon this software are not permitted.

	Distribution of this software is restricted. SAP does not make any
	warranty about the software, its performance or its conformity to any
	specification.

**************************************************************************/

package com.sap.msasrv.constants;

/**
 * SPC Constants used both by the server and by the client.
 */
public interface ServerConstants
{
	/**
	 * The separator string that separates different parameters in a request/response.
	 * Currently, only one-char separators are supported.
	 */
	public static final String SEPARATOR = "&";

	/**
	 * The error number. This number will be converted to a message with parameters.
	 */
	public static final String ERROR_NUMBER = "SPC_ERROR_NUMBER";

	/**
	 * The error message. If a command returns with an error,
	 * command.getParameter(ERROR_PARAMETER) will provide detail
	 * text for the error.
	 */
	public static final String ERROR_PARAMETER = "SPC_ERROR_MESSAGE";


	/**
	 * A special parameter name that contains how this response
	 * encodes null values in Strings.
	 */
	public static final String NULL_STRING_NAME = "SPC_NULL_STRING";

	/**
	 * Server return code: Stop. This return code is used by the server when
	 * doing a System.exit to signal the environment that the server wants to
	 * stop and shouldn't be restarted.
	 */
	public static final int SERVER_RC_STOP = 0;
	
	/**
	 * Server return code: Restart. This return code is used by the server when
	 * doing a System.exit to signal the environment that the server wants to
	 * be restarted.
	 */
	public static final int SERVER_RC_RESTART = 1;
	
	/**
	 * Server return code: Delayed Restart. This return code is used by the
	 * server when doing a System.exit to signal the environment that the server
	 * wants the environment to wait for some time and then to restart the
	 * server (for example, if a resource is not present, an endless restart
	 * loop will slow down all the other applications unnecessary so the
	 * environment should wait between restart attempts).
	 */
	public static final int SERVER_RC_DELAYED_RESTART = 2;
	
	/**
	 * Server return code: Stop with error. This return code is used by the
	 * server when doing a System.exit to signal the environment that the server
	 * has stopped with a fatal error (e.g. corrupt DB) and doesn't want to be
	 * restarted. The environment can react by performing actions like sending
	 * e-mail to administrators).
	 */
	public static final int SERVER_RC_STOP_WITH_ERROR = 3;


	public static final String FILE_SEPARATOR = System.getProperty("file.separator");
	
	public static final String CATALINA_HOME = "catalina.home"; // 5.0 tomcat is passing catalina.home evn variable to JVM using -d argument.
	public static final String CATALINA_BASE = "catalina.base"; // 5.0 tomcat is passing catalina.base evn variable to JVM using -d argument.
	public static final String VMC_CONFIG_FILE_KEY = "vmc.config.file";
	public static final String CATALINA_COMMON_DIR = "common";
	public static final String VMC_CONFIG_FILE = "config.properties";
	
	public static final String SERVER_ENCODING			= "ENCODING";
	public static final String SERVER_CONNECT_TIMEOUT	= "CONNECT_TIMEOUT";
	public static final String SERVER_TASK              = "TASK";

	//Logging Properties
	public static final String SERVER_LOG_FILE_LOCATION			= "LOG_FILE";
	public static final String SERVER_LOG_LEVEL					= "LOG_LEVEL";
	
	//Tracing Properties
	public static final String SERVER_TRACING_FLAG				= "TRACE_FLAG";
	public static final String SERVER_TRACE_FILE_LOCATION		= "TRACE_FILE";
	public static final String SERVER_TRACE_LEVEL				= "TRACE_LEVEL";
	public static final String SERVER_TRACE_FILE_COUNT			= "TRACE_FILE_COUNT";
	public static final String SERVER_TRACE_FILE_SIZE			= "TRACE_FILE_SIZE"; // In MBs
	
	public static final String SERVER_USER_LANGUAGE				= "LANGUAGE";
	public static final String SERVER_DEFAULT_USER_LANGUAGE		= "EN";
	
	public static final String SERVER_CITRIX_PORT_PROPERTY_FILE	= System.getProperty(CATALINA_BASE)+FILE_SEPARATOR+"ipc_citrix.properties";
	public static final String SERVER_CITRIX_PORT_KEY = "PORT";
	
	public static final String SERVER_PROPERTY_REFRESH  = "PROPERTY_REFRESH_PERIOD";
	
	public static final String SERVER_SCE_KB_UPDATE = "SCE_KB_UPDATE_INTERVAL";
	public static final String SERVER_SECURITY = "SECURITY";
	
	public static final String SERVER_DEFAULT_SCE_KB_UPDATE = "0"; //As per 50 MSA Server desing doc
	public static final String SERVER_INSTALLATION_TYPE = "INSTALLATION_TYPE";
	public static final String INSTALLATION_TYPE_MSA = "msa";

	public static final String SERVER_DEFAULT_PORT				= "4363";
	public static final String SERVER_DEFAULT_ENCODING			= "UnicodeLittle";

	public static final String SERVER_DEFAULT_INSTALLATION_TYPE = INSTALLATION_TYPE_MSA;

	//For the log message types.

	public static final String SERVER_DEFAULT_CONNECT_TIMEOUT	= "10000"; //in millis
	public static final String SERVER_DEFAULT_CLEANER_SLEEP_INTERVAL	= "900"; //Seconds (15 * 60 sec)
	public static final String SERVER_DEFAULT_CONNECTION_WAIT_TIME = "10";
	public static final String SERVER_DEFAULT_CUSTOMER_CMDMGR   = "";
	public static final String SERVER_DEFAULT_SECURITY = "0";
	
	
	//Logging Properties
	public static final String SERVER_DEFAULT_LOG_FILE_LOCATION			= "%t/ipclogs/server-startup_log.log";
	public static final String SERVER_DEFAULT_LOG_LEVEL					= "DEBUG";
	
	//Tracing Properties
	public static final String SERVER_DEFAULT_TRACING_FLAG				= "true";
	public static final String SERVER_DEFAULT_TRACE_FILE_LOCATION		= "%t/ipclogs/server-startup_trace.log";
	public static final String SERVER_DEFAULT_TRACE_LEVEL				= "WARNING";
	public static final String SERVER_DEFAULT_TRACE_FILE_COUNT			= "2";
	public static final String SERVER_DEFAULT_TRACE_FILE_SIZE			= "10"; //in MB
	
	public static final String SERVER_DEFAULT_PROPERTY_REFRESH  		= "10000";

	public static final String MSA_DATABASE_DEFAULT_CLIENT 				= "000";

	public static final String YES = "Y";
	public static final String NO  = "N";
	
	public static final String VALUE_TYPE_CRM							= "CRM";
	public static final String VALUE_TYPE_SYSTEM						= "SYSTEM";
	public static final String VALUE_APPLICATION_CRM					= "MSA";
	public static final String VALUE_APPLICATION_SYSTEM					= "SYSTEM";
	
	public static final String DEFAULT_APPLICATION  					= VALUE_APPLICATION_CRM; //Nothing but Module Id i.e., IModuleMetaData.getModuleId()
	public static final String DEFAULT_TYPE  							= VALUE_TYPE_CRM;
	public static final String SAP_MSA_IPC_SERVER  						= "SAP MSA IPC Server";
	public static final String SAP_MSA_IPC_SERVER_VERSION  				= "5.0";
	public static final String APD_SCE_C_PRODUCTVARIANTREADER_IMPL_KEY 	= "sce.ProductVariantReader"; //APD-SCE need this system property.
	public static final String APD_SCE_C_PRODUCTVARIANTREADER_IMPL_VALUE= "com.sap.sce.kbrt.imp.product_variant_reader_service_msa";
	public static final String APD_SCE_C_NUMERIC_ID_CONVERTION_SERVICE_IMPL_KEY 	= "sce.NumericIdConvertionServiceImpl"; //APD-SCE need this system property.
    public static final String APD_SCE_C_NUMERIC_ID_CONVERTION_SERVICE_IMPL_VALUE   = "com.sap.msasrv.db.MSANumericIdConvertor";    
	public static final String VMC_RUNTIME_INFORMATION_IMPL_KEY 		= "vmc.runtimeInformationImpl";
	public static final String VMC_RUNTIME_INFORMATION_IMPL_VALUE 		= "com.sap.msasrv.db.MSASRVRuntimeInformation";
	
}
