/************************************************************************

	Copyright (c) 2000 by SAP AG

	All rights to both implementation and design are reserved.

	Use and copying of this software and preparation of derivative works
	based upon this software are not permitted.

	Distribution of this software is restricted. SAP does not make any
	warranty about the software, its performance or its conformity to any
	specification.

**************************************************************************/

package com.sap.msasrv.module.system.command;

/**
 * Base interface for all server commands. Defines the password parameter.
 */
public interface ServerCommand extends com.sap.msasrv.constants.ServerCommand
{
}
