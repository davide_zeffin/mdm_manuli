/************************************************************************

	Copyright (c) 2001 by SAPMarkets Corp, SAP AG

	All rights to both implementation and design are reserved.

	Use and copying of this software and preparation of derivative works
	based upon this software are not permitted.

	Distribution of this software is restricted. SAP does not make any
	warranty about the software, its performance or its conformity to any
	specification.

**************************************************************************/
package com.sap.msasrv.module.system.command;

/**
 * Not used any more. Till JSP UI adopt to 50 server, JSP UI will be using this command, which will do nothing.
*/

public interface CreateSession
{
	// input parameters
	public static final String CLIENT									= "client";
	public static final String SYSTEM									= "system";
	public static final String TYPE										= "type";
	public static final String APPLICATION                              = "application";
	public static final String USER_LOCATION							= "userLocation";
	public static final String USER_NAME								= "userName";
	public static final String PARAMETER_NAMING 						= "parameterNaming";
	public static final String PARAMETER_NAMES  						= "parameterNames";
	public static final String ATTRIBUTE_NAMES                          = "attributeNames";
	public static final String ATTRIBUTE_VALUES                         = "attributeValues";
}
