/************************************************************************

	Copyright (c) 2000 by SAP AG

	All rights to both implementation and design are reserved.

	Use and copying of this software and preparation of derivative works
	based upon this software are not permitted.

	Distribution of this software is restricted. SAP does not make any
	warranty about the software, its performance or its conformity to any
	specification.

**************************************************************************/

package com.sap.msasrv.module.system.command;


/**
 * Returns server statistics about a command. <p>
 * <b>import parameters:</b>
 * <ul>
 * <li>commands (optional): include statistics about which commands were executed
 * how often with what response times. Y/N toggle, defaults to Y.
 * <li>sessions (optional): include statistics about every session (age and idle time).
 * <ul>
 * <b>export parameters:</b>
 * <p>Some of the export parameters of this command are dynamic; for example, information
 * about command X will be held by parameter "i. X".</p>
 * <ul>
 * <li>"active threads": how many worker threads are active right now.
 * <li>"active sessions": how many sessions are active right now.
 * <li>"idle time of "&lt;session id&gt;: for each session that is active, its idle time (seconds)
 * <li>"age of "&lt;session id &gt;: for each session, its age (seconds)
 * <li>"removed sessions": how many sessions are marked as timeouted/removed right now.
 * <li>"connect timeout": the connection timeout that this server uses (seconds).
 * <li>"session timeout": the session timeout that this server uses (seconds).
 * <li>"listening time": the listening time that this server uses (msec).
 * <li>"free memory": the free memory of this server (as the Java VM tells it)
 * <li>"total memory": the total memory of this server (as the Java VM tells it)
 * <li>"used memory": the used memory of this server (...)
 * <li>"last cleanup": how many seconds have passed since the last session cleanup.
 * <li>"next cleanup": in how many seconds will the next cleanup take place.
 * <li>"uptime": how many seconds ago has the server been started.
 * <li>"commands processed": how many commands have been processed by this server.
 * <li>"clients served": how many clients have connected to this server.
 * <li>"bytes sent": how many bytes have been sent over the socket connection.
 * <li>"bytes received": ... received ...
 * <li>"avg response time": what was the average response time of all commands since
 * the server started.
 * <li>"last command avg": ... of the last 50 commands that were executed.
 * <li>"i. "&lt;commandname&gt;: statistical information about number of invocations and
 * response times of a certain command. Only those commands that were executed once or more often
 * are returned.
 * </ul>
 * <p>The export parameters are not sorted in any way.</p>
 */
public interface GetServerStatistics extends ServerCommand
{
    public static final String SHOW_COMMANDS = "commands";
    public static final String SHOW_SESSIONS = "sessions";
}
