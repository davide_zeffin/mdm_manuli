/************************************************************************

	Copyright (c) 2000 by SAP AG

	All rights to both implementation and design are reserved.

	Use and copying of this software and preparation of derivative works
	based upon this software are not permitted.

	Distribution of this software is restricted. SAP does not make any
	warranty about the software, its performance or its conformity to any
	specification.

**************************************************************************/

package com.sap.msasrv.module.system.command;

/**Instantiate a class for a given name and returns
 * a parameter that indicates if this operation succeeded.
 * <p>
 *
 * <b>Import parameters</b><p>
 *
 * <ul>
 *	   <li>className:			the class to be instaniated
 *	   <li>instanceOf:			the class or interface the class above is derived from (optional)
 * </ul><p>
 *
 * <b>Export parameters</b><p>
 *
 * <ul>
 *	   <li>returnCode:	indicates if this command failed
 *			<ul>
 *				<li>returnCodeOK: no error
 *				<li>classNotFound: class could not be found
 *				<li>instanceOf: class is not instance of <instanceOf>
 *				<li>instantiation: instantiation exception encountered
 *				<li>illegal: illegal access exception encountered
 *			</ul><p>
 * </ul><p>
 *
 * Needs to be authorized.<p>
 */
public interface InstantiateClass extends ServerCommand
{
	//import
	public static final String CLASS_NAME = "className";
	public static final String INSTANCE_OF_CLASS_NAME = "instanceOf"; //optional

	//export
	public static final String RETURN_CODE = "returnCode";

	//values for export parameter
	public static final String RETURN_CODE_OK = "returnCodeOK";
	public static final String RETURN_CODE_ERROR_CLASS_NOT_FOUND = "classNotFound";
	public static final String RETURN_CODE_ERROR_INSTANCE_OF = "instanceOf";
	public static final String RETURN_CODE_ERROR_INSTANTIATION = "instantiation";
	public static final String RETURN_CODE_ERROR_ILLEGAL_ACCESS = "illegal";
}

