package com.sap.msasrv.module.system.command;

/**
 * Returns information about the IPC commands that are known to this server.
 * <p>
 *
 * Input parameters<p>
 * none
 * <p>
 * Export parameters<p>
 * <ul>
 *     <li>commands: the names of all commands that this server knows.
 * </ul>
 */
public interface GetCommands
{
	public static final String COMMANDS = "commands";
}
