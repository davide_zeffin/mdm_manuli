/*
 * Created on Nov 22, 2004
 *
 */
package com.sap.msasrv.module.system.command;

import com.sap.msasrv.constants.ServerCommand;

/**
 * 
 *  Configures IPC Server based on the imported parameters. Parameters are expected as an XML string.
 * 
 * Example XML String:
 * <Parameters>
 * 	 <Server>
 * 	    <Property Name="LOG_FILE" Value="%t/ipclogs/server_log.%g.log"/>
 * 	    <Property Name="LOG_LEVEL" Value="DEBUG"/>
 * 	    <Property Name="TRACE_FLAG" Value="true"/>
 * 	    <Property Name="TRACE_FILE" Value="%t/ipclogs/server_trc.%t.log"/>
 * 	    <Property Name="TRACE_LEVEL" Value="DEBUG"/>
 * 		. . .
 * 	 </Server>
 * </Parameters>
 *
 *    
 * <b>Import parameters</b>
 *
 * <ul>
 *      <li>xmlPropertyFileData	(required)	:	 Parameters in XML format.
 * </ul>
 * 
 * <b>Export parameters</b>
 * <ul>
 *      <li>acknowledgementStatus		 : acknowledgement status as 'COMMAND_EXECUTED' if command got executed.
 * 										   [With this export parameter client can check whether 
 * 											server received the command or not] 	
 *     <li>No result, check the STATUS code for success or failure.
 * </ul>
 */
public interface ConfigureIPC extends ServerCommand {
	//import
	public static final String XML_PROPERTY_FILE_DATA	= "xmlPropertyFileData"; 

	//output
	public static final String ACKNOWLEDGEMENT_STAUTS	= "acknowledgementStatus"; 
}
