/************************************************************************

	Copyright (c) 2000 by SAP AG

	All rights to both implementation and design are reserved.

	Use and copying of this software and preparation of derivative works
	based upon this software are not permitted.

	Distribution of this software is restricted. SAP does not make any
	warranty about the software, its performance or its conformity to any
	specification.

**************************************************************************/

package com.sap.msasrv.module.system.command;

/**
 * Administration command: returns the ids of all available sessions.
 * <p>
 *
 * <b>Import parameters</b><p>
 * none<p>
 *
 * <b>Export parameters</b><p>
 *
 * <ul>
 *		status[]:	description of each worker thread status
 * </ul><p>
 *
 * Needs to be authorized.<p>
 */
public interface GetWorkerThreadStatus extends ServerCommand {
	// export
	public static final String STATUS = "status";
}


