/************************************************************************

	Copyright (c) 2000 by SAP AG

	All rights to both implementation and design are reserved.

	Use and copying of this software and preparation of derivative works
	based upon this software are not permitted.

	Distribution of this software is restricted. SAP does not make any
	warranty about the software, its performance or its conformity to any
	specification.

**************************************************************************/

package com.sap.msasrv.module.system.command;

/**
 * Administration command: Stop or restart the IPC server. Please note that the
 * actual stop/restart handling is delegated to the server's environment. The
 * server will just terminate with different return codes.<p>
 *
 * <b>Import parameters</b><p>
 *
 * <ul>
 *     <li>restart:	"Y"/"N" toggle. Y=restart the server, N=stop the server
 * </ul><p>
 *
 * <b>Export parameters</b><p>
 * none<p>
 *
 * Needs to be authorized.<p>
 */
public interface StopServer extends ServerCommand
{
	public static final String RESTART = "restart";
}
