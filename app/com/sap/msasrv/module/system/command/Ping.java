/************************************************************************

	Copyright (c) 2000 by SAP AG

	All rights to both implementation and design are reserved.

	Use and copying of this software and preparation of derivative works
	based upon this software are not permitted.

	Distribution of this software is restricted. SAP does not make any
	warranty about the software, its performance or its conformity to any
	specification.

**************************************************************************/

package com.sap.msasrv.module.system.command;

/**
 * <p>Simple ping command for availability and response time tests.</p>
 *
 * <p>Apart from the basic "pinging" functionality, you can optionally send any data you want to the
 * server, which the server will send back unchanged (to test data transfer speeds).
 *
 * <b>Import parameters</b><p>
 *
 * <ul>
 *     <li>input (optional): any data you want to send to the server.
 * </ul><p>
 *
 * <b>Export parameters</b><p>
 *
 * <ul>
 *     <li>output (optional): if you sent input data, you'll receive them as output data. This parameter
 *         will only be != null if you have set input data.
 * </ul><p>
 */
public interface Ping extends ServerCommand
{
	public static final String INPUT  = "input";
	public static final String OUTPUT = "output";
}
