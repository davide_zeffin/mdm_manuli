/************************************************************************

	Copyright (c) 1999 by SAP AG

	All rights to both implementation and design are reserved.

	Use and copying of this software and preparation of derivative works
	based upon this software are not permitted.

	Distribution of this software is restricted. SAP does not make any
	warranty about the software, its performance or its conformity to any
	specification.

**************************************************************************/

package com.sap.msasrv.module.system.command;

/**
 * Returns the properties of sessions identified by their id (use GetSessions to get all session ids).
 * <p>
 * This API has changed in IPC 3.0A to support several sessions in one command. This was necessary
 * because calling the same command 50 times to retrieve info of 50 sessions took unbearably long.
 * <p>
 *
 *  <b>Import Parameters</b>
 *	<ul>
 *		none
 *   </ul><p>
 *  <b>Export Parameters</b>
 *  <ul>
 * 		<li>sessionId(required)	: session identification number identifying unique session.
 *		<li>sessionUserName		: name of user who started the session
 *		<li>sessionUserLocation	: location (machine id) of user who started the session
 *		<li>sessionClient		: R/3 client of the session
 *      <li>sessionApplication  : application of the session
 *      <li>sessionType         : type of the session
 *      <li>sessionLanguage     : language passed by MSA using ConfigureIPC command
 *      <li>sessionISOLanguage  : ISOLanguage passed by MSA using ConfigureIPC command
 *  </ul><p>
 */

public interface GetSessionInfo
{

    public static final String SESSION_ID				= "sessionId";
    public static final String SESSION_USER_NAME		= "userName";
    public static final String SESSION_USER_LOCATION	= "userLocation";
    public static final String SESSION_CLIENT			= "client";
	public static final String SESSION_TYPE				= "sessionType";
	public static final String SESSION_APPLICATION      = "sessionApplication";
	public static final String SESSION_LANGUAGE         = "sessionLanguage";
	public static final String SESSION_ISO_LANGUAGE     = "sessionISOLanguage";
}
