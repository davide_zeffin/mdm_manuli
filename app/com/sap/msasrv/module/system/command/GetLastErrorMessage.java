package com.sap.msasrv.module.system.command;


/**
 * Returns information about the last exception thrown in IPC in this session.
 *
 * <b>Import parameters</b><p>
 *
 * <ul>
 *     <li>substringSize (optional): return at most so many characters. Default: no limitation.
 *     <li>substringFromRight (optional): only used if substringSize is set to a value, and if
 *         the actual message is longer than this size. In this case, substringFromRight="Y" will
 *         return the rightmost portion of the message instead of the leftmost portion.
 * </ul><p>
 *
 * <b>Export parameters</b><p>
 *
 * <ul>
 *     <li>message: the message, or " " if no error was encountered recently.
 * </ul><p>
 */
public interface GetLastErrorMessage {
	// imports
	public static final String SUBSTRING_SIZE = "substringSize";
	public static final String SUBSTRING_FROM_RIGHT = "substringFromRight";

	// exports
	public static final String MESSAGE = "message";
}