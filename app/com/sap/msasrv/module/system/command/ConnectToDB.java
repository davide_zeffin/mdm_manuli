/*
 * Created on Nov 22, 2004
 *
 */
package com.sap.msasrv.module.system.command;

import com.sap.msasrv.constants.ServerCommand;

/**
 * Collectes the connection information to establishes a connection to the database (when required). Parameters are expected as an XML string
 *
 *Example:
 *<Parameters>
 *  <DatabaseList>
 *    <Database Server="dbHost" DatabaseName="dbName" Port="port" User="user" Password="password">
 *    </Database>
 *  </DatabaseList>
 *</Parameters>
 *
 * <b>Import parameters</b>
 *
 * <ul>
 *      <li>xmlPropertyFileData	(required)	:	 Parameters in XML format.
 * </ul>
 *
 * 
 * <b>Export parameters</b>
 *
 * <ul>
 *      <li>acknowledgementStatus		 : acknowledgement status as 'COMMAND_EXECUTED' if command got executed.
 * 										   [With this export parameter client can check whether 
 * 											server received the command or not] 	
 *     <li>No result, check the STATUS code for success or failure.
 * </ul>
 */
public interface ConnectToDB extends ServerCommand {
	//import
	public static final String XML_PROPERTY_FILE_DATA	= "xmlPropertyFileData";
	//output
	public static final String ACKNOWLEDGEMENT_STAUTS	= "acknowledgementStatus"; 
}
