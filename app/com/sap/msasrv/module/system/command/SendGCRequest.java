package com.sap.msasrv.module.system.command;

/**
 * @author i026584
 *
 * for Memory cleanup.
 * <p>command is used to call VM garbage collection thread.</p>
 *
 * <p> As VM can't guaranty the garbage collection with System.gc(),
 * this command also can't guaranty the garbage collection.
 *</p>
 */
public interface SendGCRequest {

}
