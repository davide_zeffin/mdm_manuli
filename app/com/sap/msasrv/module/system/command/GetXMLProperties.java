/************************************************************************

	Copyright (c) 2000 by SAP AG

	All rights to both implementation and design are reserved.

	Use and copying of this software and preparation of derivative works
	based upon this software are not permitted.

	Distribution of this software is restricted. SAP does not make any
	warranty about the software, its performance or its conformity to any
	specification.

**************************************************************************/

package com.sap.msasrv.module.system.command;

/**
 * Administration command: Returns the XML file with all the parameters
 * read from the property file spc/lib/properties/properties.xml
 * <p>
 *
 * <b>Import parameters</b><p>
 * none<p>
 *
 * <b>Export parameters</b><p>
 *
 * <ul>
 *	   <li>xmlPropertyFile : Properties in XML format.		
 * </ul><p>
 *
 * Needs to be authorized.<p>
 */

public interface GetXMLProperties extends ServerCommand {
	// export
	public static final String XML_PROPERTY_FILE	= "xmlPropertyFile";
}
