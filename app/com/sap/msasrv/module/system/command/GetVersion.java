package com.sap.msasrv.module.system.command;

/**
 * Returns the version string of this IPC server and the third-party components it includes.
 * The format of these version strings is subject to change without notice.
 * <p>
 * Export parameters:
 * <ul>
 *    <li>vmVersion: the version of the Java VM
 *	  <li>vmVendor: the vendor of the Java VM
 */
public interface GetVersion {
	public static final String VM_VERSION = "vmVersion";
	public static final String VM_VENDOR = "vmVendor";
}