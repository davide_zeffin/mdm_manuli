/*
 * Created on Nov 29, 2004
 *
 */
package com.sap.msasrv.module.system.command;

/**
 * Configures the session.
 *
 * <b>Import parameters</b>
 *
 * <ul>
 *		<li>type (optional):					the type of the session (possible values are: admin, crm, isa)
 *												<ul><li>ADMIN: administration
 *												<li>CRM: CRM (The CRM system is supposed to read the product master data)
 *												<li>ISA: internet sales (IPC reads the product master data)
 *												<li>SYSTEM: For system purposes, e.g. buffer refresh </ul>
 *      <li>appliation (optional):              the application of the session. The application identifies which commands
 *                                              are available in this session and which session object is being used.
 *                                              <br>Possible values are defined in the server 
 * 												com.sap.msasrv.usermodule.xxx.xxxModuleMetaData java file.
 *                                              Typical: SCE, SPE, SPC, TTE, CFE, FGE, MSA.
 *                                              The application may seem the same as the type. Actually, there is a major
 *                                              difference. For example, types CRM and ISA both use the MSA application;
 *                                              If no application is specified, "MSA" is assumed (for compatibility reasons).
 *      <li>attributeNames (optional):          a number of additional attribute keys. The meaning of the attributes are
 *                                              defined by the application. In addition to the attributes in here, all other
 *                                              parameters passed to this command are also included in the attributes.
 *      <li>attributeValues (optional):         the same number of attribute values.
 *
 * </ul>
 *
 * <b>Export parameters</b>
 * <ul>
 *      <li>acknowledgementStatus		 : acknowledgement status as 'COMMAND_EXECUTED' if command got executed.
 * 										   [With this export parameter client can check whether 
 * 											server received the command or not] 	
 *     <li>No result, check the STATUS code for success or failure.
 * </ul>
 */

public interface ConfigureSession {
	// input parameters
//	public static final String CLIENT									= "client";
	public static final String TYPE										= "type";
	public static final String APPLICATION                              = "application";
	public static final String ATTRIBUTE_NAMES                          = "attributeNames";
	public static final String ATTRIBUTE_VALUES                         = "attributeValues";

	//output
	public static final String ACKNOWLEDGEMENT_STAUTS	= "acknowledgementStatus"; 
}
