package com.sap.msasrv.module.system.command;

/**
 * Returns information about the input/output parameters an IPC command accepts/returns.
 * <p>
 *
 * Input parameters<p>
 * <ul>
 *     <li>commandName (optional): the name of the command to retrieve information about. If
 *         no command has been given, info about all commands will be returned.
 * </ul>
 * <p>
 * Export parameters<p>
 * <ul>
 *     <li>parameterCommands: name of the command the respective parameter belongs to
 *     <li>parameterNames: array of all parameters
 *     <lI>parameterInputs: "Y"/"N" toggles: are the parameters input parameters?
 *     <lI>parameterRequireds: "Y"/"N" toggles: are the parameters required?
 *     <lI>parameterArrays: "Y"/"N" toggles: are the parameters array parameters?
 *     <li>parameterTables: reference of the table each array belongs to (not yet implemented)
 */
public interface GetApi
{
	// import
	public static final String COMMAND_NAME = "commandName";

	// export
	public static final String PARAMETER_COMMANDS	= "parameterCommands";
	public static final String PARAMETER_NAMES		= "parameterNames";
	public static final String PARAMETER_INPUTS		= "parameterInputs";
	public static final String PARAMETER_REQUIREDS	= "parameterRequireds";
	public static final String PARAMETER_ARRAYS		= "parameterArrays";
	public static final String PARAMETER_TABLES		= "parameterTables";
}
