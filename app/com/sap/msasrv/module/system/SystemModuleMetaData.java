/*
 * Created on Sep 22, 2004
 *
 */
package com.sap.msasrv.module.system;

import com.sap.msasrv.socket.server.module.IModuleMetaData;


/**
 * @author I026584
 *
 * Metadata for the module SYSTEM, which is the core must to have module from SAP
 * 
 */
public class SystemModuleMetaData implements IModuleMetaData {

	/* (non-Javadoc)
	 * @see com.sap.msasrv.socket.server.module.IModuleMetaData#getModuleId()
	 */
	public String getModuleId() {
		
		return "SYSTEM";
	}

	/* (non-Javadoc)
	 * @see com.sap.msasrv.socket.server.module.IModuleMetaData#getIdsExtendsList()
	 */
	public String[] getIdsExtendsList() {
		return new String[0];
	}

	/* (non-Javadoc)
	 * @see com.sap.msasrv.socket.server.module.IModuleMetaData#getCommandSetInterface()
	 */
	public String getCommandSetInterface() {
		return "com.sap.msasrv.module.system.ISystemCommandSet";
	}

	/* (non-Javadoc)
	 * @see com.sap.msasrv.socket.server.module.IModuleMetaData#getImplPkgsList()
	 */
	public String[] getImplPkgsList() {
		return new String[] {"com.sap.msasrv.module.system.command.imp"};
	}

	/* (non-Javadoc)
	 * @see com.sap.msasrv.socket.server.module.IModuleMetaData#getSessionClass()
	 */
	public String getSessionClass() {
		return null;
	}

	/* (non-Javadoc)
	 * @see com.sap.msasrv.socket.server.module.IModuleMetaData#getIdsListForSessionReplacement()
	 */
	public String[] getIdsListForSessionReplacement() {
		return new String[0];
	}

	//Just for Testing if any
	public static void main(String[] args) {
	}
}
