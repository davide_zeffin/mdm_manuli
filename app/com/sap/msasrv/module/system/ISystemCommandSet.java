/*
 * Created on Sep 22, 2004
 *
 */
package com.sap.msasrv.module.system;

import com.sap.msasrv.socket.server.module.ICommandSet;

/**
 * @author I026584
 *
 * This interface associates symbolic constants with command classes.
 * It is used for two purposes:<br>
 * <ul>
 * <li>Command Registration: The value of every constant declared here
 * is a command to be registered by the CommandMgr.
 * <li>Command Execution: Use the symbolic constants declared here in
 * CommandProcessor.execute.
 * </ul>
 */

public interface ISystemCommandSet extends ICommandSet{
	
	/**
	 * public fields: Add a line here to have a socket command registered
	 * by the CommandMgr.
	 * Ex: if you want to register a command called ConfigureIPC then the like should look like
	 *  public static final String CONFIGURE_IPC				= "ConfigureIPC"; 
	 */
	public static final String CREATE_SESSION               = "CreateSession"; //Temp fix for JSP UI
	public static final String GET_SERVER_STATISTICS		= "GetServerStatistics";
	public static final String PING							= "Ping";
	public static final String STOP_SERVER					= "StopServer";
	public static final String INSTANTIATE_CLASS			= "InstantiateClass";
	public static final String GET_SESSION_INFO             = "GetSessionInfo";
	public static final String GET_API						= "GetApi";
	public static final String GET_COMMANDS					= "GetCommands";
	public static final String GET_WORKER_THREAD_STATUS		= "GetWorkerThreadStatus";
	public static final String GET_XML_PARAMETERS           = "GetXMLProperties";
	public static final String GET_LAST_ERROR_MESSAGE       = "GetLastErrorMessage";
	public static final String GET_VERSION                  = "GetVersion";
	public static final String SEND_GC_REQUEST				= "SendGCRequest";
	public static final String CONFIGURE_IPC				= "ConfigureIPC";
	public static final String CONFIGURE_SESSION			= "ConfigureSession";
	public static final String CONNECT_TO_DB				= "ConnectToDB";
}
