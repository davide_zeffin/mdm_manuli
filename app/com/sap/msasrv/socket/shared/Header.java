/************************************************************************

	Copyright (c) 2001 by SAPMarkets Corp, SAP AG

	All rights to both implementation and design are reserved.

	Use and copying of this software and preparation of derivative works
	based upon this software are not permitted.

	Distribution of this software is restricted. SAP or SAPMarkets
	do not make any warranty about the software, its performance or
	its conformity to any specification.

**************************************************************************/

package com.sap.msasrv.socket.shared;


/**
 * Header information of an IPC command. Headers are passed from the
 * client to the server and back.<p>
 *
 * The header the client provides contains a command, and optionally a session
 * id, a broadcast flag and a compress flag.<p>
 *
 * The header the server sends back contains the same data as well as a return
 * status. If the client didn't pass a session id or passed "" as session id to
 * the server, the server creates a new session and passes its id back to the
 * client in the header. The client pass this id to the server in later communication.<p>
 *
 * A small note about compression: The server never change the compression mode of
 * the header. Iff the client passes it a compressed request, it will return a
 * compressed response.
 */
public interface Header {

	/**
	 * Changes this header's broadcasting behavior.
	 * @param s the broadcast string ("1" or "0").
	 */
	public void setBroadcast(String s);

	/**
	 * Changes this header's broadcasting behavior (if this command should be
	 * passed on to other servers registered at the same dispatcher as the one
	 * that executes this command).
	 */
	public void setBroadcast(boolean useBroadcast);

	/**
	 * Returns true iff this command is being broadcasted to other servers.
	 */
	public boolean isBroadcast();

	/**
	 * Returns the name of the command of this header.
	 */
	public String getCommand();

	/**
	 * Sets the command name of this header.
	 */
	public void setCommand(String command);

	/**
	 * Returns the IPC session id of this header. Initially, you pass an empty
	 * session id to the server, the header t
	 */
	public String getId();

	/**
	 * Sets the session id of this header.
	 */
	public void setId(String id);

	/**
	 * Returns the status string of this header.
	 * @see com.sap.msasrv.socket.shared.ErrorCodes
	 */
	public String getStatus();

	/**
	 * Sets the status string of this header.
	 * @see com.sap.msasrv.socket.shared.ErrorCodes
	 */
	public void setStatus(String s);

	/**
	 * Returns the error of this header. Not supported by the IPC Protocol.
	 */
	public String getError();

	/**
	 * Sets the error of this header. Not supported by the IPC Protocol.
	 */
	public void setError(String s);

	/**
	 * Returns the length of the request data that belong to this header in
	 * characters, or 0, if the implementation doesn't need/support body
	 * strings.
	 */
	public int getLength();

	/**
	 * Sets the length of data that belong to this header, or does nothing
	 * if the implementation doesn't support body strings.
	 */
	public void setLength(int i);

	/**
	 * Sets the compression mode of this header. True means that the data
	 * string that belongs to this header has been compressed with SAPZip
	 * @see com.sap.msasrv.util.zip.SAPZip
	 */
	public void setCompressed(boolean compressed);

	/**
	 * Returns true if the string data for this header are compressed and need
	 * to be decompressed by the server.
	 */
	public boolean isCompressed();

	/**
	 * Returns a String representation of this Header in the format defined
	 * in the IPC protocol. This method <em>always</em> has to be implemented,
	 * even in implementations that don't use the IPC protocol. The reason is
	 * that broadcasted commands are always broadcasted using the socket protocol.
	 * If they were original eg. RFC requests, they need to be converted to a
	 * IPC socket request first.
	 */
	public String toIPCString();
}
