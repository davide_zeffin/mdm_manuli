/************************************************************************

	Copyright (c) 2000 by SAP AG

	All rights to both implementation and design are reserved.

	Use and copying of this software and preparation of derivative works
	based upon this software are not permitted.

	Distribution of this software is restricted. SAP does not make any
	warranty about the software, its performance or its conformity to any
	specification.

**************************************************************************/

package com.sap.msasrv.socket.shared;

import java.util.Enumeration;

/**
 * A SharedRequest is the most simple form of a Request, ie one that doesn't use
 * client or server specific notions and that thus can be used by the client and
 * the server.<p>
 */
public interface SharedRequest
{
	/**
	 * Returns a string representation of the query in the format defined by the IPC
	 * protocol. This method <em>always</em> has to be implemented,
	 * even in implementations that don't use the IPC protocol. The reason is
	 * that broadcasted commands are always broadcasted using the socket protocol.
	 * If they were original eg. RFC requests, they need to be converted to a
	 * IPC socket request first.
	 */
	public String getQueryString();

	/**
	 * Returns all values of a given parameter, or null if the parameter does not
	 * exist.
	 * @param	name	the name of the parameter
	 * @return	an array of the values
	 */
	public String[] getParameterValues(String name);

	/**
	 * Returns all values of a given parameter. If the parameter does not exist,
	 * this method returns a default value.
	 * @param	name	the name of the parameter
	 * @return	an array of the values
	 */
	public String[] getParameterValues(String name, String[] defaultValues);

	/**
	 * Returns the value of a parameter. If the named parameter does not exist,
	 * this method returns null, if the parameter has several values, one of them
	 * is chosen arbitrarily.
	 * @param	name	the name of the parameter
	 * @return	the/a value of the parameter
	 */
	public String getParameterValue(String name);

	/**
	 * Returns the value of a parameter. If the named parameter does not exist,
	 * the default value is returned, if the parameter has several values, one of them
	 * is chosen arbitrarily.
	 * @param	name	the name of the parameter
	 * @param	defaultValue	the default value of the parameter
	 * @return	the/a value of the parameter, never null.
	 */
	public String getParameterValue(String name, String defaultValue);

	/**
	 * @return an Enumeration of the names of all parameters.
	 */
	public Enumeration getParametersEnum();

	/**
	 * @return	an array containing the names of all parameters.
	 */
	public String[] getParameters();
}
