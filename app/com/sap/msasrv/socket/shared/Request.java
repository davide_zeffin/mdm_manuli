/************************************************************************

	Copyright (c) 2000 by SAP AG

	All rights to both implementation and design are reserved.

	Use and copying of this software and preparation of derivative works
	based upon this software are not permitted.

	Distribution of this software is restricted. SAP does not make any
	warranty about the software, its performance or its conformity to any
	specification.

**************************************************************************/

package com.sap.msasrv.socket.shared;

import com.sap.msasrv.socket.server.Command;
import com.sap.msasrv.socket.server.CommandException;

/**
 * A Request implements functions to retrieve names and values of
 * a set of parameters. A parameter is identified by a String. It
 * can have one or more String values.
 */
public interface Request extends SharedRequest
{
	/**
	 * Returns all values of a given parameter. If the parameter does not exist,
	 * this method throws a CommandException.
	 * @param	name	the name of the parameter
	 * @return	an array of the values
	 */
	public String[] getParameterValues(Command c, String name) throws CommandException;

	/**
	 * Returns the value of a parameter. If the named parameter does not exist,
	 * this method throws a CommandException, if the parameter has several values, one of them
	 * is chosen arbitrarily.
	 * @param	command	the command that called this method
	 * @param	name	the name of the parameter
	 * @return	the/a value of the parameter, never null (when no Exception has been thrown)
	 */
	public String getParameterValue(Command c, String name) throws CommandException;
}
