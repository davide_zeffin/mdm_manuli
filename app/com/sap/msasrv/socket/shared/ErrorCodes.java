/************************************************************************

	Copyright (c) 2000 by SAP AG

	All rights to both implementation and design are reserved.

	Use and copying of this software and preparation of derivative works
	based upon this software are not permitted.

	Distribution of this software is restricted. SAP does not make any
	warranty about the software, its performance or its conformity to any
	specification.

**************************************************************************/

package com.sap.msasrv.socket.shared;


/**
 * String constants for the error codes used by the SPC protocol.
 */
public interface ErrorCodes
{
	public  final String RET_OK						= "200";
	public  final String RET_UNKNOWN_ID				= "400";
	public  final String RET_TIMEDOUT_ID		    = "404";
	public  final String RET_UNKNOWN_CMD			= "410";
 	public  final String RET_COMPRESSION_FAILED		= "420";
	public  final String RET_INTERNAL_FATAL			= "500";
	public  final String RET_INTERNAL_ERROR			= "501";
}
