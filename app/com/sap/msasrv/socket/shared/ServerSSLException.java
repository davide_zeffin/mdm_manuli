/*
 * Created on Mar 18, 2004
 *
 * 
 */
package com.sap.msasrv.socket.shared;

import java.io.PrintStream;
import java.io.PrintWriter;

/**
 * @author I026584
 * An exception that signals that something went wrong in SSL mode.
 */
public class ServerSSLException extends Exception {

	private Exception _sub;

	/**
	 * @param msg Message related to Exception
	 */
	public ServerSSLException(String msg) {
		super(msg);
		_sub = null;
	}

	/**
	 * Constructs a ServerSSLException that wraps another exception. Stack trace and message
	 * of that exception will be used by this exception.
	 */
	public ServerSSLException(Exception sub) {
		super(sub.getMessage());
		_sub = sub;
	}

	/**
	 * Returns the exception that causes this CommandException to be thrown, or null,
	 * if this was not caused by an exception.
	 */
	public Exception getCauseException() {
		return _sub;
	}

	/**
	 * Prints the stack trace of this exception. If it was constructed
	 * from another exception e, e's stack trace is printed instead
	 * (which is usually what we're interested in).
	 */
	public void printStackTrace() {
		if (_sub != null)
			_sub.printStackTrace();
		else
			super.printStackTrace();
	}

	/**
	 * Prints the stack trace of this exception. If it was constructed
	 * from another exception e, e's stack trace is printed instead
	 * (which is usually what we're interested in).
	 */
	public void printStackTrace(PrintStream s) {
		if (_sub != null)
			_sub.printStackTrace(s);
		else
			super.printStackTrace(s);
	}

	/**
	 * Prints the stack trace of this exception. If it was constructed
	 * from another exception e, e's stack trace is printed instead
	 */
	public void printStackTrace(PrintWriter s) {
		if (_sub != null)
			_sub.printStackTrace(s);
		else
			super.printStackTrace(s);
	}

	/**
	 * Fills in the stack trace of this exception. If it was constructed
	 * from another exception e, e's stack trace is used instead
	 */
	public Throwable fillInStackTrace() {
		if (_sub != null)
			return _sub.fillInStackTrace();
		else
			return super.fillInStackTrace();
	}
}
