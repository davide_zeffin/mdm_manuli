/************************************************************************

	Copyright (c) 2000 by SAP AG

	All rights to both implementation and design are reserved.

	Use and copying of this software and preparation of derivative works
	based upon this software are not permitted.

	Distribution of this software is restricted. SAP does not make any
	warranty about the software, its performance or its conformity to any
	specification.

**************************************************************************/

package com.sap.msasrv.socket.shared;
//So that we maintain the exception types only at one place.
//import com.sap.msasrv.sys.exc.ExceptionTypes;

/**
 * SPC Constants used both by the server and by the client.
 */
public interface ServerConstants extends com.sap.msasrv.constants.ServerConstants
{

	
	
/*	TODO: These constants are being used by 
	com.sap.msasrv.socket.client.Client.getConnectAttributes() method and 
	are previously available in shared.DispatcherConstants file (yes! a bad place).
	So moved for the time being to ServerConstants, find a better place if you can.
*/	
	/**
	 * attribute key: connection type (eg. "crm", "ipc").
	 */
	public static final String KEY_TYPE			= "type";
	
	/**
	 * attribute key: R/3 client (string valued!, eg. "805", "100")
	 */
	public static final String KEY_CLIENT		= "client";
	
	/**
	 * attribute key: user name (eg. "tester")
	 */
	public static final String KEY_USERNAME		= "userName";
	
	public static final String KEY_USERLOCATION = "userLocation";
	
//	public static final String VALUE_TYPE_INTERNET_SALES	= "ISA";
//	public static final String VALUE_TYPE_PME				= "PME";
//	public static final String VALUE_TYPE_IPC				= "IPC";
//	public static final String VALUE_TYPE_ADMIN				= "ADM";
}
