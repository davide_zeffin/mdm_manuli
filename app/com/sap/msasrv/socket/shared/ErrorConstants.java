/************************************************************************

	Copyright (c) 2000 by SAP AG

	All rights to both implementation and design are reserved.

	Use and copying of this software and preparation of derivative works
	based upon this software are not permitted.

	Distribution of this software is restricted. SAP does not make any
	warranty about the software, its performance or its conformity to any
	specification.

**************************************************************************/

package com.sap.msasrv.socket.shared;

public interface ErrorConstants
{
	public static final int UNKNOWN_ERROR							= -1;
	public static final int UNKNOWN_COMMAND							= 0;
	public static final int ENGINE_EXCEPTION						= 1;
	public static final int REQUIRED_PARAMETER_NOT_FOUND			= 2;
	public static final int PARAMETER_ARRAY_LENGTH_MISMATCH			= 3;
	public static final int IDENTIFIED_DATA_NOT_FOUND				= 4;
	public static final int IDENTIFIED_DATA_NOT_FOUND_IN_CONTEXT	= 5;
	public static final int	ILLEGAL_BOOLEAN_VALUE					= 6;
	public static final int REQUIRED_PARAMETER_NOT_FOUND_IN_CONTEXT	= 7;
	public static final int MALFORMED_OBJECT						= 8;
	public static final int EMPTY_ARRAY								= 9;
	public static final int MALFORMED_PROPERTY_FILE					= 10;
	public static final int INTERNAL_SESSION_ERROR					= 11;
	public static final int PROPERTY_FILE_NOT_FOUND					= 12;
	public static final int REMOTE_ADMIN_PASSWORD_REQUIRED			= 13;
	public static final int REMOTE_ADMIN_DISABLED					= 14;
	public static final int FILE_WRITE_ERROR						= 15;
	public static final int MALFORMED_SESSION						= 16;
	public static final int UNKNOWN_CLIENT							= 17;
	public static final int BROADCAST_FAILURE						= 18;
	public static final int SECURITY_ERROR							= 19;

	public static final int DISPATCHER_NO_SERVERS_REGISTERED        = 100;
	public static final int DISPATCHER_UNREGISTER_FAILED            = 101;
}
