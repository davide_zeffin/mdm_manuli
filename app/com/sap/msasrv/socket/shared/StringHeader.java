/************************************************************************

	Copyright (c) 2001 by SAPMarkets Corp, SAP AG

	All rights to both implementation and design are reserved.

	Use and copying of this software and preparation of derivative works
	based upon this software are not permitted.

	Distribution of this software is restricted. SAP or SAPMarkets
	do not make any warranty about the software, its performance or
	its conformity to any specification.

**************************************************************************/

package com.sap.msasrv.socket.shared;


public final class StringHeader implements Header, ErrorCodes {
	public static final String HEAD_PRE  = "<HEAD ";
	public static final String HEAD_POST = "/>";

	public static final String ID        = "ID=\"";
	public static final String COMMAND   = "OP=\"";
	public static final String LENGTH    = "LEN=\"";
	public static final String STATUS    = "STATUS=\"";
	public static final String ERROR     = "ERROR=\"";
	public static final String BROADCAST = "BROADCAST=\"";
	public static final String COMPRESSED = "ZIP=\"";


	private String _id = null;
	private String _op = null;

	private String _status = RET_INTERNAL_ERROR;
	private String _error  = null;

	private boolean _broadcast = false;

	private int _len = 0;
	private boolean _compressed = false;


	public StringHeader() {
	}

	public StringHeader(String header) {
		this();

		setHeader(header);
	}

	public void setHeader(String header) {

		_op = _getSubstring(header, COMMAND);

		_id = _getSubstring(header, ID);

		setStatus(_getSubstring(header, STATUS));
		setError(_getSubstring(header, ERROR));

		setLength(_getSubstring(header, LENGTH));

 		String s = _getSubstring(header, COMPRESSED);
		if (s != null && s.equals("1"))
			_compressed = true;

		setBroadcast(_getSubstring(header, BROADCAST));

	}




	private void _output(String s) {
		System.err.println(s);
	}



	private static String _getSubstring(String s0, String sub) {
		int i = s0.indexOf(sub);

		if (i < 0)
			return null;

		i = i + sub.length();

		int j = s0.indexOf("\"", i);
		if (i==j)
			return null;

		return s0.substring(i, j);
	}

	public void setBroadcast(String s) {
		if (s == null || s.equals("0"))
			setBroadcast(false);
		else
			setBroadcast(true);
	}
	public void setBroadcast(boolean bFlag) {
		_broadcast = bFlag;
	}
	public boolean isBroadcast() {
		return _broadcast;
	}


	public static String getCommand(String header) {
		return _getSubstring(header, COMMAND);
	}

	public String getCommand() {
		return _op;
	}

	public void setCommand(String command) {
		_op = command;
	}

	public String getId() {
		return _id;
	}

	public void setId(String id) {
		_id = id;
	}

	public String getStatus() {
		return _status;
	}

	public void setStatus(String s) {
		_status = s;
	}

	public String getError() {
		return _error;
	}

	public void setError(String s) {
		_error = s;
	}

	public int getLength() {
		return _len;
	}

	public void setLength(int i) {
		_len = i;
	}

	void setLength(String s) {

		int i = 0;

		if (s != null) {

			try {
				i = Integer.parseInt(s);
			} catch (Exception ex) {
				_output("Conversionerror: " + s);
			}
		}

		setLength(i);
	}


	public void setCompressed(boolean compressed) {
		_compressed = compressed;
	}

	public boolean isCompressed() {
		return _compressed;
	}

	public String buildHeader(String id, String op, String status, int l) {
		return buildHeader(id,op,status,false,false,l);
	}

	public String buildHeader(String id, String op, String status, boolean compressed, boolean broadcast, int l) {
		_id = id;
		_op = op;
		_status = status;
		_compressed = compressed;
		_broadcast = broadcast;

		_error = null;

		_len = l;

		return (this.toString());
	}



	public String toString() {
		StringBuffer sb = new StringBuffer(HEAD_PRE);

		sb.append(COMMAND);
		sb.append(_op);
		sb.append("\" ");

		if (_id != null) {
			sb.append(ID);
			sb.append(_id);
			sb.append("\" ");
		}

		if (_status != null) {
			sb.append(STATUS);
			sb.append(_status);
			sb.append("\" ");

			if (_error != null) {
				sb.append(ERROR);
				sb.append(_error);
				sb.append("\" ");
			}
		}

		if (_compressed) {
			sb.append(COMPRESSED);
			sb.append("1\" ");
		}

		if (_len > 0) {
			sb.append(LENGTH);
			sb.append(_len);
			sb.append("\" ");
		}

		if (_broadcast) {
			sb.append(BROADCAST);
			sb.append("1\" ");
		}

		sb.append(HEAD_POST);

		return sb.toString();


		/*
		return HEAD_PRE + ID + (_id==null?"":_id) + "\" " +
			   COMMAND + (_op==null?"":_op) + "\" " +
			   STATUS + (_status==null?"":_status) + "\" " +
			   LENGTH + _len + "\"" +
			   HEAD_POST;
		*/
	}


	public String toIPCString() {
		return toString();
	}

	/*
	 * Methods that work directly on a String-encoded header
	 */

	/**
	 * Returns the length value from a String-encoded header
	 */
    public static int getHeaderLength(String sHeader)
    {
        int pos;
		if ((pos = sHeader.indexOf(StringHeader.LENGTH)) >= 0) {
			String s = sHeader.substring(pos + StringHeader.LENGTH.length());
			int pos2 = s.indexOf("\"");
			return Integer.parseInt(s.substring(0, pos2));
		}

        return 0;
    }


	/**
	 * Returns the status value from a String-encoded header
	 */
    public static int getHeaderStatus(String sHeader)
    {
        int pos;
		if ((pos = sHeader.indexOf(StringHeader.STATUS)) >= 0) {
			String s = sHeader.substring(pos + StringHeader.STATUS.length());
			int pos2 = s.indexOf("\"");
			return Integer.parseInt(s.substring(0, pos2));
		}

        return 0;
    }


	/**
	 * Returns the compress value from a String-encoded header
	 */
	public static boolean getHeaderCompress(String header) {
		// has the client sent compressed data
		boolean clientZipped = false;
		int pos;
		if ((pos=header.indexOf(StringHeader.COMPRESSED)) >= 0) {
			String s = header.substring(pos+StringHeader.COMPRESSED.length());
			int pos2 = s.indexOf("\"");

			if (s.substring(0, pos2).equals("1"))
				clientZipped = true;
		}

		return clientZipped;
	}


	/**
	 * Returns the broadcast value from a String-encoded header
	 */
	public static boolean getHeaderBroadcast(String header) {
		// has the client sent compressed data
		boolean broadcast = false;
		int pos;
		if ((pos=header.indexOf(StringHeader.BROADCAST)) >= 0) {
			String s = header.substring(pos+StringHeader.BROADCAST.length());
			int pos2 = s.indexOf("\"");

			if (s.substring(0, pos2).equals("1"))
				broadcast = true;
		}

		return broadcast;
	}


	/**
	 * Returns the new header that is equal to the given, String-encoded header
	 * but has no broadcast value.
	 */
	public static String disableHeaderBroadcast(String header) {
		return removePart(header,StringHeader.BROADCAST);
	}

	private static final String removePart(String header, String keyword) {
		int pos = header.indexOf(keyword);
		if (pos != -1) {
			int pos2 = header.indexOf("\"",pos);
			int pos3 = header.indexOf("\"",pos2+1);
			String result = header.substring(0,pos) + header.substring(pos3+1);
			return result;
		}

		return header;
	}

	/**
	 * Returns the new header that is equal to the given, String-encoded header
	 * but has no session id.
	 */
	public static String removeSessionId(String header) {
		return removePart(header,StringHeader.ID);
	}


	public static String getSessionId(String header) {
		return _getSubstring(header,StringHeader.ID);
	}
}
