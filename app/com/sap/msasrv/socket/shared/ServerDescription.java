package com.sap.msasrv.socket.shared;

/**
 * A very simple class that holds a description of a server
 */
public class ServerDescription
{
	private String host;
	private String port;
	private String programId;

	public ServerDescription(String host, String port, String programId) {
		this.host = host;
		this.port = port;
		this.programId = programId;
	}

	public String getHost() {
		return host;
	}

	public String getPort() {
		return port;
	}

	/**
	 * Returns the RFC program id of this server, or null, if this server doesn't
	 * act as a RFC server
	 */
	public String getProgramId() {
		return programId;
	}
}
