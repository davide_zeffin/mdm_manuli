/************************************************************************

	Copyright (c) 2000 by SAP AG

	All rights to both implementation and design are reserved.

	Use and copying of this software and preparation of derivative works
	based upon this software are not permitted.

	Distribution of this software is restricted. SAP does not make any
	warranty about the software, its performance or its conformity to any
	specification.

**************************************************************************/

package com.sap.msasrv.socket.shared;


import com.sap.msasrv.socket.server.Command;
import com.sap.msasrv.socket.server.CommandException;


/**
 * A request with more convenient getParameterValue variants. Implements all
 * parameter list accessors that have Command or default value parameters based
 * on the ones without a second parameter.
 */
public abstract class AbstractRequest implements Request
{
	/**
	 * Returns the value of a parameter. If the named parameter does not exist,
	 * this method throws a CommandException, if the parameter has several values, one of them
	 * is chosen arbitrarily.
	 * @param	command	the command that called this method
	 * @param	name	the name of the parameter
	 * @return	the/a value of the parameter, never null (when no Exception has been thrown)
	 */
	public String getParameterValue(Command c, String name) throws CommandException {
		String result = getParameterValue(name);
		if (result == null)
			throw new CommandException(c, ErrorConstants.REQUIRED_PARAMETER_NOT_FOUND,
				new String[] { name });
		else
			return result;
	}


	/**
	 * Returns all values of a given parameter. If the parameter does not exist,
	 * this method throws a CommandException.
	 * @param	name	the name of the parameter
	 * @return	an array of the values
	 */
	public String[] getParameterValues(Command c, String name) throws CommandException {
		String[] values = getParameterValues(name);
		if (values == null)
			throw new CommandException(c, ErrorConstants.REQUIRED_PARAMETER_NOT_FOUND,
				new String[] { name });
		else
			return values;
	}


	/**
	 * Returns the value of a parameter. If the named parameter does not exist,
	 * the default value is returned, if the parameter has several values, one of them
	 * is chosen arbitrarily.
	 * @param	name	the name of the parameter
	 * @param	defaultValue	the default value of the parameter
	 * @return	the/a value of the parameter, never null.
	 */
	public String getParameterValue(String name, String defaultValue) {
		String result = getParameterValue(name);
		if (result == null)
			return defaultValue;
		else
			return result;
	}


	/**
	 * Returns all values of a given parameter. If the parameter does not exist,
	 * this method returns a default value.
	 * @param	name	the name of the parameter
	 * @return	an array of the values
	 */
	public String[] getParameterValues(String name, String[] defaultValues) {
		String[] values = getParameterValues(name);
		if (values == null)
			return defaultValues;
		else
			return values;
	}


	/**
	 * Separates a String into two. This utility method always returns null or an array of length 2.
	 * The first array element will contain everything left of the first occurence of sep in s,
	 * the second element everything right of it (both excluding sep).
	 *
	 * @param   s   the String to separate
	 * @param   sep the separating character.
	 * @return  two-element array, or null, if sep does not occur in s.
	 */
	public static final String[] separateString(String s, char sep) {
		int index = s.indexOf(sep);
		if (index == -1)
			return null;
		else {
			String[] result = new String[2];
			result[0] = s.substring(0,index);
			result[1] = s.substring(index+1);
			return result;
		}
	}



}
