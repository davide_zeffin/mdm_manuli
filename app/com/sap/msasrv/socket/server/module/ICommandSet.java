/*
 * Created on Sep 22, 2004
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package com.sap.msasrv.socket.server.module;

/**
 * @author I026584
 *
 * The interface which extends this interface associates symbolic constants with 
 * command classes. It is used for two purposes:<br>
 * <ul>
 * <li>Command Registration: The value of every constant declared here
 * is a command to be registered by the CommandMgr.
 * <li>Command Execution: Use the symbolic constants declared here in
 * CommandProcessor.execute.
 * </ul>
 */
public interface ICommandSet {
	//No methods required.
}
