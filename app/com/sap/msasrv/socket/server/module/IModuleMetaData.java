/*
 * Created on Sep 22, 2004
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package com.sap.msasrv.socket.server.module;

/**
 * @author I026584
 *
 * TODO To change the template for this generated type comment go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
public interface IModuleMetaData {
	
	/**
	 * @return Unique identifier of the Module  (Should not be null)
	 * 
	 * Ex: "SPC"
	 */
	public String getModuleId();
	
	/**
	 * @return Array of Extending Modules if any or else return null
	 * 
	 * Ex: {"SPE", "SCE"}
	 */
	public String[] getIdsExtendsList();
	
	/**
	 * @return Name of the interface (including package name) 
	 * 		   where the list of commands are available if any
	 * 			or else return null.
	 * 
	 * Hint: Every User defined CommandSet interface should extend 
	 * 		 the basic com.sap.msasrv.socket.server.module.ICommandSet interface. 
	 * 
	 * Ex: "com.sap.msasrv.usermodule.spc.ISPCCommandSet" 
	 */
	public String getCommandSetInterface();
	
	/**
	 * @return Array of packages where the command 
	 * 		   implementation classes are available if any
	 * 			or else return null.
	 * 
	 * Ex: {"com.sap.msasrv.usermodule.spc.impl1", "com.sap.msasrv.usermodule.spc.impl2"} 
	 */
	public String[] getImplPkgsList();
	
	/**
	 * @return Name of the Session class (including package name) if any
	 * 			or else return null
	 * 
	 * Ex: "com.sap.msasrv.usermodule.spc.SPCSession" 
	 */
	public String getSessionClass();
	
	/**
	 * @return Array of Modules Ids whose corresponding Sessions 
	 * 		   needs to be replaced by this Modules session if any
	 * 			or else return null. 
	 *        (If this module is extending any module then only
	 * 			you can think of whether to replace the extending 
	 * 			modules session or not.
	 * if not extending any module return null
	 * if extending but don't want to replace that modules session then return null
	 * if extending but would like to replace that modules session then return an array with that/those module(s).
	 * 
	 * Ex: { "SPE" , "SCE" }
	 */
	public String[] getIdsListForSessionReplacement();
}
