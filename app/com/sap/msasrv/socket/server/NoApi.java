package com.sap.msasrv.socket.server;

/**
 * A class that implements the Api class for commands that don't
 * need to be known by non-Java clients
 */
public class NoApi implements Api
{
	private String[] s = new String[0];
	
	public NoApi() {}
	
	public String[] getInputParameters() {
		return s;
	}
	
	public String[] getOutputParameters() {
		return s;
	}
	
	public boolean isRequired(String inputParameterName) {
		return false;
	}
	
	public boolean isArray(String parameterName) {
		return false;
	}
}
