package com.sap.msasrv.socket.server;

import java.util.Hashtable;
import java.util.Vector;

/**
 * Default Api implementation. Offers several more or less convenient constructors to define the Api of a command.
 */
public final class DefaultApi implements Api
{
	public static final String INPUT_REQUIRED = "!ir";
	public static final String INPUT_OPTIONAL = "!io";
	public static final String OUTPUT = "!o";
	
	public Hashtable nameToData;
	
	public String[] inputNames;
	
	public String[] outputNames;
	
    /**
	 * straightforward but inconvenient constructor
	 */
	public DefaultApi(String[] paraNames, boolean[] inputs, boolean[] requireds, boolean[] arrays) {
		nameToData = new Hashtable();
		Vector iNames = new Vector();
		Vector oNames = new Vector();
		for (int i=0; i<paraNames.length; i++) {
			nameToData.put(paraNames[i],new _Data(inputs[i],requireds[i],arrays[i]));
			if (inputs[i])
				iNames.addElement(paraNames[i]);
			else
				oNames.addElement(paraNames[i]);
		}
		
		inputNames = new String[iNames.size()];
		iNames.copyInto(inputNames);
		
		outputNames = new String[oNames.size()];
		oNames.copyInto(outputNames);
	}
	
	/**
	 * Constructs this defaultAPI like this(true,params)
	 */
	public DefaultApi(String[] params) {
		this(true,params);
	}
	
	/**
	 * not-so-straightforward constructor that is actually much easier to use than
	 * the default one. Essentially mixes or prefixes parameter names with special
	 * strings that describe the parameter.<p>
	 * isSequence = false =>
	 * Construct via a String array of Strings like [ i { r  | o } | o ] [ a | s ] name
	 * examples: irsdocumentId, iosformatValue, ioaformatValues, ositemId, oaitemIds<p>
	 * isSequence = true =>
	 * Construct via a String array that holds parameter names, mixed with strings that tell
	 * what all the next parameters will be. Arrayness will be extracted from the parameter
	 * ending.
	 * example "!ir", "documentId", "itemId", "!io", "productIdType", "!o", "productId".
	 * implicitely starts with "!ir"
	 */
	public DefaultApi(boolean isSequence, String[] params) {
		nameToData = new Hashtable();
		Vector iNames = new Vector();
		Vector oNames = new Vector();
		if (!isSequence) {
			
			for (int i=0; i<params.length; i++) {
				int startChar;
				boolean isInput;
				boolean isRequired = false;
				if (params[i].charAt(0) == 'i') {
					isInput = true;
					startChar = 2;
					isRequired = (params[i].charAt(1) == 'r');
				}
				else {
					isInput = false;
					startChar = 1;
				}
				boolean isArray = (params[i].charAt(startChar) == 'a');
				String name = params[i].substring(startChar+1);
				
				nameToData.put(name, new _Data(isInput,isRequired,isArray));	
				
				if (isInput)
					iNames.addElement(name);
				else
					oNames.addElement(name);
			}
		}
		else {
			boolean isInput = true;
			boolean isRequired = true;
			for (int i=0; i<params.length; i++) {
				String name = params[i];
				if (!name.equals("")) {
					// strip out empty stuff
					if (name.charAt(0) == '!') {
						if (name.charAt(1) == 'i') {
							isInput = true;
							isRequired = (name.charAt(2) == 'r');
						}
						else {
							isInput = false;
							isRequired = false;
						}
					}
					else {
						boolean isArray = (name.charAt(name.length()-1) == 's');
						if (name.charAt(0) == '/') {
							isArray = !isArray;
							name = name.substring(1);
						}
						nameToData.put(name, new _Data(isInput,isRequired,isArray));	
						
						if (isInput)
							iNames.addElement(name);
						else
							oNames.addElement(name);					
					}
				}
			}
			
		}
		inputNames = new String[iNames.size()];
		iNames.copyInto(inputNames);
		
		outputNames = new String[oNames.size()];
		oNames.copyInto(outputNames);			
	}
	

    /**
	 * Constructs an Api from an Api of a "super command". Some commands extend other commands;
	 * so it is natural that they inherit their API (import/export parameters) and add other
	 * parameters.
	 */
	public DefaultApi(Api superApi, String[] params) {
		this(params);
		concat(superApi);
	}
	
	public void concat(Api api) {
		String[] inputs = api.getInputParameters();
		String[] outputs = api.getOutputParameters();
		
		// concat input parameter names
		String[] newInputs = new String[inputNames.length+inputs.length];
		for (int i=0; i<newInputs.length; i++) {
			newInputs[i] = (i<inputs.length ? inputs[i] : inputNames[i-inputs.length]);
		}
												  
		// concat output parameter names
		String[] newOutputs = new String[outputNames.length+outputs.length];
		for (int i=0; i<newOutputs.length; i++) {
			newOutputs[i] = (i<outputs.length ? outputs[i] : outputNames[i-outputs.length]);
		}
			
		inputNames = newInputs;
		outputNames = newOutputs;
		
		// concat hash table
		for (int i=0; i<inputs.length; i++) {
			boolean isRequired = api.isRequired(inputs[i]);
			boolean isArray    = api.isArray(inputs[i]);
			nameToData.put(inputs[i],new _Data(true,isRequired,isArray));
		}
		for (int i=0; i<outputs.length; i++) {
			boolean isRequired = api.isRequired(outputs[i]);
			boolean isArray    = api.isArray(outputs[i]);
			nameToData.put(outputs[i],new _Data(false,isRequired,isArray));
		}
	}
	
	
	private class _Data {
		boolean input;
		boolean required;
		boolean array;
		
		public _Data(boolean isInput, boolean isRequired, boolean isArray) {
			input = isInput;
			required = isRequired;
			array = isArray;
		}
	}

	
	public String[] getInputParameters() {
		return inputNames;
	}
	
	
	public String[] getOutputParameters() {
		return outputNames;
	}
	
	
	public boolean isRequired(String inputParameterName) {
		_Data d = (_Data)nameToData.get(inputParameterName);
		if (d == null)
			return false;
		else
			return d.required;
	}

	
	public boolean isArray(String parameterName) {
		_Data d = (_Data)nameToData.get(parameterName);
		if (d == null)
			return false;
		else
			return d.array;
	}
}
