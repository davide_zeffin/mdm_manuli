package com.sap.msasrv.socket.server;

/**
 * The API of a single IPC command. APIs are not necessary for the core IPC server functionality,
 * they just provide a simple way of "introspection" into the command - which parameters does it
 * know, are they input/output, arrays, required? Used to generate ipc.dat files and in the IPC monitor.<p>
 * Although they are not really critical, every IPC command should return a valid Api.
 */
public interface Api 
{
	public String[] getInputParameters();
	public String[] getOutputParameters();
	public boolean isRequired(String inputParameterName);
	public boolean isArray(String parameterName);
}
