/*
 * Created on Dec 7, 2004
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package com.sap.msasrv.socket.server;

import java.util.Vector;

import com.sap.msasrv.socket.shared.Request;

/**
 * 
 */
public interface ICommandProcessor {
	/**
	 * Executes a command of a given name and handles all Exceptions this command
	 * could throw. If the exception is really serious, this method will just re-throw it
	 * (and delegate handling it to the next level above it - the Connection implementation)
	 *
	 * @param	name	the name of the command. If no command with this name has been
	 * added, an error of type RET_UNKNOWN_CMD is returned.
	 * @param	request	the request parameter to be passed to the command.
	 * @param	response	the response the command should fill. Error messages will
	 * be added to this response.
	 */
	public void execute(String name, ISessionRemote session, Request request, Response response);
	
	
	/**
	 * Returns the API of a given command. This method will construct all Commands, retrieve
	 * their APIs, and store them when called the first time. Use with care.
	 */
	public Api getApi(Command c);
	
	/**
	 * Returns a named command, or null, if this command has not been registered.
	 */
	public Command getCommand(String name);
	
	/**
	 * Returns a Vector of all commands this command processor knows. Use with care
	 * since this method will construct a lot of commands, most of which will not be
	 * used in this session.
	 */
	public Vector getCommands();
	
	public void setModule(IModule module);
}
