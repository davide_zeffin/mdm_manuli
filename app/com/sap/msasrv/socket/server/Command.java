/************************************************************************

	Copyright (c) 2000 by SAP AG

	All rights to both implementation and design are reserved.

	Use and copying of this software and preparation of derivative works
	based upon this software are not permitted.

	Distribution of this software is restricted. SAP does not make any
	warranty about the software, its performance or its conformity to any
	specification.

**************************************************************************/
package com.sap.msasrv.socket.server;

import com.sap.msasrv.socket.shared.ErrorConstants;
import com.sap.msasrv.socket.shared.Request;
import com.sap.sxe.sys.language;

/**
 * A server command. Subclass this class to provide your own functionality
 * in isStartCommand and run.<p>
 * To register a command, simply add a constant that identifies it to
 * the CommandConstants class of the respective server module.<p>
 */
public abstract class Command
{
	public static final String LANGUAGE = "language";

	/**
	 * The session that is being used by this command. Initialized properly by the
	 * server prior to invocation of this.run(...).
	 * 
	 */
	protected ISessionRemote spcSessionRemote;

	/**
	 * The command processor that this command is attached to. Use this processor to
	 * access other commands.
	 */
	protected ICommandProcessor  _commandProcessor;

	/**
	 * The name of this command. The name is hardwired into the command (actually, it
	 * is the class name of this class. The name is stored the first time getName() is called.
	 */
	protected String            _name;

	public Command()
	{
		_name = null;
		_commandProcessor = null;
		spcSessionRemote = null;
	}

    /**
     * Used internally by the IPC server to initialize the command with the correct session.
	 * No need to override this method.
	 */
	public void setSession(ISessionRemote session) {
		spcSessionRemote = session;
	}

    /**
	 * Used internally by the IPC server to initialize the command with the correct command processor.
	 * No need to override this method.
	 */
	public final void setCommandProcessor(ICommandProcessor p) {
		_commandProcessor = p;
	}

	/**
	 * Returns the name of this command. This name is hardwired into the command.<p>
	 */
	public final String getName() {
		// thread safe without synchronization
		if (_name != null)
			return _name;
		else {
		    String fullyQualifiedClassName = this.getClass().getName();
			_name = fullyQualifiedClassName.substring(fullyQualifiedClassName.lastIndexOf(".") + 1);
			return _name;
		}
	}

	/**
	 * returns true iff several instances of this command may run
	 * concurrently in several threads. The default implementation is
	 * conservative and disables concurrent command dispatching. Override
	 * this method to return true if possible.<p>
	 * 
	 * If this method returns false, no other command that also returns false for this
	 * method will be run concurrently to this command's run.
	 *
	 * @return	the ability of this command to run in a multi-threaded environment
	 */
	public boolean isConcurrent() {
		return false;
	}

    /**
	 * Returns true if this command can be used without any previous other IPC command.
	 * typically you need to run CreateSession before you can run another command (like
	 * CreateDocument). If this command can work on an empty system session (like most
	 * administrative commands, return true, otherwise return false).
	 */
	public abstract boolean isStartCommand();


	/**
	 * Run this command. As of IPC 3.0, this method is called instead of run. The
	 * default implementation just calls run with the same parameters, dealing with
	 * command conversion first.<p>
	 *
	 * The reason for this indirection is that module implementations may want to
	 * subclass Command by their own abstract implementation. This class may want to perform
	 * some initialization/housekeeping whenever they are run before the whatever their subclass
	 * wants to do.<p>
	 *
	 * If you override this method, make sure to finish with super.doRun(request, response)
	 * if ParameterNameConverter stuff is an issue for you.<p>
	 *
	 * Since checking if parameter name conversion takes a tiny amount of time (two hashtable
	 * accesses), commands that want the utmost performance and that never want to use
	 * naming conversion can override this method by
	 * <pre>
	 * public void doRun(Request request, Response response) throws CommandException {
         *   run(request, response);
	 * }
	 * </pre><p>
	 */
	public void doRun(Request request, Response response) throws CommandException {
		ThreadState.setState(ThreadState.STATE_BUSY, "running command");
	    run(request, response);
		ThreadState.setState(ThreadState.STATE_BUSY, "after running command");
	}


    /**
	 * The run method contains the "core" of an IPC command. The method will be executed when
	 * the IPC server has processed the client's request, created an empty response, and is
	 * ready to process whatever functionality this command should have.<p>
	 * Your implementation should use the request to read parameters the client has passed
	 * to the command and to set parameters in the response object. Commands should convert
	 * all exceptions they throw into CommandExceptions which will be automatically converted
	 * into reasonable exceptions on the client side. If you throw any other exception, it
	 * will be converted into a less-readable exception.
	 */
	public abstract void run(Request request, Response response) throws CommandException;

	/**
	 * Converts a language id to an ISO 2 letter language code, if necessary
	 * @param	langId	a one-letter or two-letter language code
	 * @return	a two-letter language code
	 */
	public static final String getISO2Language(String langId)	{
		// kha: language should be in APD's code.
		language lang;
		if (langId.length() == 1) {
			lang = language.find_language_for_id(langId);
			return lang.get_id2();
		}
		else
			return langId;
	}

	/**
	 * Looks for the parameter LANGUAGE in r and returns it, converting it to a two-letter
	 * ISO code if necessary.
	 * @param	r	the request to use
	 * @param	isOptional	if parameter LANGUAGE is not found, isOptional=true will return null,
	 * isOptional=false will throw a CommandException
	 * @return	a two letter ISO language code
	 */
	protected final String getLanguageParameter(Request r, boolean isOptional) throws CommandException {
		String langVal = r.getParameterValue(LANGUAGE);
		if (langVal == null) {
			if (isOptional)
				return null;
			else
				throw new CommandException(this,ErrorConstants.REQUIRED_PARAMETER_NOT_FOUND,
					new String[] {"language"});
		}
		else
			return getISO2Language(langVal);
	}

	/**
	 * Ensure that all the integers in lengths are equal. In case of an error, use the
	 * corresponding ids to throw a CommandException. If all lengths elements are equal,
	 * this method is equivalent to no operation.
	 *
	 * @param	lengths		array of integers (typically array lengths)
	 * @param	ids			array of identifiers. The i-th id corresponds to the i-th length
	 */
	protected final void checkArrayLength(int[] lengths, String[] ids) throws CommandException {
		if (lengths.length < 2)
			return;
		int length = lengths[0];
		for (int i=1; i<lengths.length; i++)
			if (length != lengths[i])
				throw new CommandException(this, ErrorConstants.PARAMETER_ARRAY_LENGTH_MISMATCH,
					new String[] { ids[0], ids[i] },
					CommandException.SEVERITY_ERROR);
	}

	/**
	 * Returns a String array of a given length where every element has the same value.
	 */
	protected static final String[] constantStringArray(int length, String value) {
		String[] a = new String[length];
		for (int i=0; i<length; i++)
			a[i] = value;
		return a;
	}

    /**
	 * Returns the API of this IPC command. The APIs are not really necessary for the 
	 * command to work; they are merely there for e.g. the IPC monitor and similar tools
	 * that want some kind of introspection into the IPC command layer. Also the file
	 * "ipc.dat" for ITS is automatically generated from the APIs of all IPC commands.
	 */
	public abstract Api getApi();

}
