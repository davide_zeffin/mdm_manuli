package com.sap.msasrv.socket.server;
// kha: since this should be accessed from anywhere in IPC, it shouldn't be inside the socket package
// but in a central place.

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

/**
 * This class offers utility methods to set the state of every IPC thread,
 * which can e.g. be read using IPC monitoring commands (GetWorkerThreadState).
 */
public class ThreadState {

	/**
	 * Thread State: Busy, which means that the thread is actively processing something.
	 */
	public static final int STATE_BUSY      = 0;

	/**
	 * Thread State: Sleeping, which means that the thread is sleeping for some time using Thread.sleep.
	 * Should be set from outside whenever a server thread is set to sleep (which should never be the case!).
	 */
	public static final int STATE_SLEEP     = 1;

	/**
	 * Thread State: Waiting, which should be set from outside when the thread waits for data from
	 * a database, remote connection, ...
	 */
	public static final int STATE_WAITING   = 2;

	private static HashMap states = new HashMap();     // maps Thread objects to ThreadState objects

	private int state;
	private String description;


    private ThreadState(int state, String description) {
		this.state = state;
		this.description = description;
    }


	/**
	 * Updates the current thread's state.
	 */
	public static void setState(int state, String description) {
		ThreadState ts = new ThreadState(state, description);
		Thread t = Thread.currentThread();
		synchronized(ThreadState.class) {
			states.put(t, ts);
		}
	}


	public static void setState(int state) {
		setState(state, null);
	}

	/**
	 * Returns a new map of thread states which maps Thread objects to Strings. The strings
	 * are formatted like this:
	 * BUSY: <description>
	 * SLEEP: <description>
	 * WAIT: <description>
	 * UNKNOWN
	 */
	public static Map getStates() {
		HashMap result = new HashMap();
		synchronized(ThreadState.class) {
			for (Iterator iter=states.keySet().iterator(); iter.hasNext();) {
				Object o = iter.next();
				ThreadState data = (ThreadState)states.get(o);
				if (data == null)
					result.put(o, "UNKNOWN");
				else
				    result.put(o, data.toString());
			}
		}
		return result;
	}


	/**
	 * Returns the state of a given thread, formatted as a string (see getStates).
	 */
	public static String getState(Thread thread) {
		ThreadState state;
		synchronized(ThreadState.class) {
			state = (ThreadState)states.get(thread);
		}
		if (state == null)
			return "UNKNOWN";
		else
		return state.toString();
	}


	public String toString() {
		String stateString;
		switch(state) {
			case STATE_BUSY:
				stateString = "BUSY";
				break;
			case STATE_SLEEP:
				stateString = "SLEEP";
				break;
			case STATE_WAITING:
				stateString = "WAIT";
				break;
			default:
				stateString = "UNKNOWN";
				break;
		}
		if (description != null)
			stateString = stateString + ": "+description;

		return stateString;
	}
}
