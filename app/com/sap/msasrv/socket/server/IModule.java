/*
 * Created on Nov 2, 2004
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package com.sap.msasrv.socket.server;

import java.util.Set;


/**
 * @author I026584
 *
 * TODO To change the template for this generated type comment go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
public interface IModule {

	/**
	 * Returns the id of this module
	 */
	public String getId();

	/**
	 * Returns the class of the session of this module. New sessions for this module will
	 * be constructed via getSession().newInstance calls.
	 */
	public Class getSession();

	/**
	 * Returns references to all super modules of this module, or the empty array,
	 * if there is no super module. Never returns null.<p>
	 * For performance reasons, this method returns the array stored in this class
	 * rather than a reference to it. Users of this method should treat it as a
	 * constant (or the module will be messed up).
	 */
	public IModule[] getSuperModules();


	/**
	 * Returns the class of the command of name commandName, including inherited ones.
	 */
	public Class getCommand(String commandName);
	
	/**
	 * Returns all command names that this module knows, including the inherited ones.
	 */
	public String[] getAllCommandNames();
	

	/**
	 * sets all replaced Session Ids.
	 */
	public void setReplacedSessionIds(String[] ids);


	/**
	 * Returns all replaced Session Ids.
	 */
	public Set getReplacedSessionIds();

}
