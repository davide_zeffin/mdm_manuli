/************************************************************************

	Copyright (c) 2000 by SAP AG

	All rights to both implementation and design are reserved.

	Use and copying of this software and preparation of derivative works
	based upon this software are not permitted.

	Distribution of this software is restricted. SAP does not make any
	warranty about the software, its performance or its conformity to any
	specification.

**************************************************************************/

package com.sap.msasrv.socket.server;

import java.io.PrintStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Enumeration;

import com.sap.msasrv.socket.shared.ErrorConstants;
import com.sap.sxe.sys.seq.vector;

/**
 * An exception thrown by a Command. A CommandException may know any combination
 * of three parameters:<p>
 *
 * <ul>
 * <li><b>a command</b> that caused it (may be null for "command not found")
 * <li><b>a message</b> that describes it (may be null)
 * <li><b>an exception</b> in a layer below the command interface that lead to this exception (may be null)
 * </ul><p>
 *
 * <p>Additionally, a CommandException has either severity "error" or "fatal".
 * "error" signals that something has gone wrong but the layer below the
 * command API is still consistent (for example wrong parameters to a command).
 * "fatal" signals that it may be the case that the session is inconsistent.</p>
 */
public class CommandException extends Exception // the IPC Server is responsible for CommandException logging, so we don't subclass LoggedException here
{
	private Command   _command  = null;
	private int       _number   = ErrorConstants.UNKNOWN_ERROR;
	private Exception _sub	    = null;
	private int		  _severity = SEVERITY_ERROR;
	private String[]  _param	= null;

	/**
	 * Error severity
	 */
	public static final int SEVERITY_ERROR = 0;

	/**
	 * Fatal severity
	 */
	public static final int SEVERITY_FATAL = 1;

	/**
	 * Constructs a CommandException for a given command/message pair.<p>
	 * The Exception's message is constructed from the two parameters.
	 * The severity of this message is "error".
	 * @param	c		the command. Must not be null.
	 * @param	number	the error number;
	 * @param	params	array of parameters that describe the exception
	 */
	public CommandException(Command c, int number, String[] params) {
		super("Exception in Command "+c.getName()+": "+Integer.toString(number));
		_command = c;
		_number = number;
		_param = params;
	}

	/**
	 * Constructs a CommandException for a given command, number, parameter array and a severity.
	 */
	public CommandException(Command c, int number, String[] params, int severity) {
		this(c,number,params);
		_severity = severity;
	}

	/**
	 * Constructs a CommandException for an unknown command. The severity of this message is "error".
	 * @param	commandName	the name of the command.
	 */
	public CommandException(String commandName) {
		super("Command not found: "+commandName);
		_number = ErrorConstants.UNKNOWN_COMMAND;
		_param  = new String[] { commandName };
	}

    /**
	 * Constructs a CommandException for an unknown command and a given severity.
	 */
	public CommandException(String commandName, int severity) {
		this(commandName);
		_severity = severity;
	}

	/**
	 * Constructs a CommandException for an exception sub thrown by
	 * a command c and use its message and stack trace. The severity of this message is "fatal"
	 * (since Exceptions from the layer below usually show that something's gone really wrong).
	 *
	 * @param	c	the command that threw the exception. Must not be null.
	 * @param	sub	an exception
	 */
	public CommandException(Command c, Exception sub) {
		super("Exception thrown by command "+c.getName()+".\n\t"+sub.toString());
		_number = ErrorConstants.ENGINE_EXCEPTION;
		_param = getExceptionDetails(sub);

		_command = c;
		_sub = sub;
		_severity = SEVERITY_FATAL;
	}

    /**
	 * Constructs a CommandException for an exception sub thrown by
	 * a command c and use its message and stack trace. 
	 *
	 * @param	c	 the command that threw the exception. Must not be null.
	 * @param	sub	 an exception
	 * @param       severity the severity to use.
	 */
	public CommandException(Command c, Exception sub, int severity) {
		this(c,sub);
		_severity = severity;
	}


	/**
	 * Returns the command of this exception
	 */
	public Command getCommand() {
		return _command;
	}

	/**
	 * Returns the message of this exception.
	 * @deprecated	use the ErrorHandler to get a localized message.
	 */
	public String getDetailMessage() {
		return "CommandException #"+Integer.toString(_number);
	}


	/**
	 * Returns the exception that causes this CommandException to be thrown, or null,
	 * if this was not caused by an exception.
	 */
	public Exception getCauseException() {
		return _sub;
	}


	/**
	 * Returns this Exception's number
	 * @see	ErrorConstants
	 */
	public int getNumber() {
		return _number;
	}


	/**
	 * Returns the parameter String of this Exception.
	 */
	public String[] getParameters() {
		return _param;
	}


	// If _sub is available, delegate stack trace stuff to _sub, else use super's methods

	/**
	 * Prints the stack trace of this exception. If it was constructed
	 * from another exception e, e's stack trace is printed instead
	 * (which is usually what we're interested in).
	 */
	public void printStackTrace() {
		if (_sub != null)
			_sub.printStackTrace();
		else
			super.printStackTrace();
	}

	/**
	 * Prints the stack trace of this exception. If it was constructed
	 * from another exception e, e's stack trace is printed instead
	 * (which is usually what we're interested in).
	 */
	public void printStackTrace(PrintStream s) {
		if (_sub != null)
			_sub.printStackTrace(s);
		else
			super.printStackTrace(s);
	}

	/**
	 * Prints the stack trace of this exception. If it was constructed
	 * from another exception e, e's stack trace is printed instead
	 */
	public void printStackTrace(PrintWriter s) {
		if (_sub != null)
			_sub.printStackTrace(s);
		else
			super.printStackTrace(s);
	}

	/**
	 * Fills in the stack trace of this exception. If it was constructed
	 * from another exception e, e's stack trace is used instead
	 */
	public Throwable fillInStackTrace() {
		if (_sub != null)
			return _sub.fillInStackTrace();
		else
			return super.fillInStackTrace();
	}

	/**
	 * Returns the severity of this exception.
	 */
	public int getSeverity() {
		return _severity;
	}


	/**
	 * Returns an array of two strings containing details about an exception. The first String
	 * is the exception's toString, the second one the IPC method that threw the exception.
	 * (If the exception is in a method not starting with "com.sap.", the stack trace is
	 * read until the first method that is).
	 */
	public static String[] getExceptionDetails(Exception e) {
		String[] result = new String[2];
		result[0] = e.toString();
		StringWriter stringWriter = new StringWriter();
		PrintWriter wr = new PrintWriter(stringWriter);
		e.printStackTrace(wr);
		String data = stringWriter.toString();
		int index = data.indexOf("\tat com.sap.");
		if (index == -1) {
			result[1] = "unknown";
		}
		else {
			index = index + 12; // jump behind com.sap
			boolean found = false;
			int endindex = index;
			char[] chars = data.toCharArray();
			while (!found && endindex < chars.length) {
				if (chars[endindex] == '\n')
					found = true;
				else
					endindex++;
			}
			if (found)
				result[1] = data.substring(index,endindex);
			else
				result[1] = data.substring(index);
		}

		return result;
	}

	/**
	 * Returns the stack trace as an array of strings
	 */
	public static String[] getStackTrace(Exception e) {

                vector stack = new vector();

                StringWriter stringWriter = new StringWriter();
		PrintWriter wr = new PrintWriter(stringWriter);
		e.printStackTrace(wr);
		String data = stringWriter.toString();
		int index = data.indexOf("\tat com.sap.");
		index = index + 12; // jump behind com.sap
		char[] chars = data.toCharArray();

		int endindex = index;
		int i = 0;
		while ( endindex < chars.length) {
			if (chars[endindex] == '\n')
				{
					String line = data.substring(index, endindex);
					stack.pushz(line);
					index = endindex + 2;
					i++;
				}

			endindex++;
		}

		String[] result = new String[stack.length()];

		i = 0;
		for (Enumeration se = stack.elements();
			 se.hasMoreElements(); ) {

			result[i] = (String)se.nextElement();
			i++;
		}

		return result;
	}

	/**
	 * Returns a localized, one-line message for a CommandException
	 */
	public static String getMessage(CommandException e) {
		if (e.getCauseException() != null && e.getCauseException() instanceof CommandException) {
			return getMessage((CommandException)e.getCauseException());
		}
		int number = e.getNumber();
		String[] param = e.getParameters();
		return getFormattedString(Integer.toString(number),param);
	}

	// As we are not using any language specific message no need to read from resource bundle.
	private static String getFormattedString(String errNum, Object[]  args){
		StringBuffer msgBuf = new StringBuffer();
		msgBuf.append("CommandException: code=" + errNum);
		if (args != null && args.length > 0) {
			for (int i=0; i<args.length; i++) {
				msgBuf.append(", ");
				msgBuf.append(args[i].toString());
			}
		}
		return msgBuf.toString();
	}
	
	public String getMessage(){
		return CommandException.getMessage(this);
	}
}
