package com.sap.msasrv.socket.server;

import java.util.Hashtable;

/**
 * Rudimentary interface of a session. All sessions (eg. SPCSession) need to implement
 * this interface.<p>
 *
 * A method you can use has not been included in this interface. If you implement the
 * static method
 * <pre>
 * public static void initMasterSession()
 * </pre>
 * in your Session implementation, the IPC server will call this method upon startup.
 * <br>
 * Master sessions are important to eg. keep your database connection open or preload
 * often-used data. Since you can't have static methods declared in an interface,
 * the Java Reflection API is used when the server starts to call this method, if it
 * is there.
 */
public interface Session
{
	public static final String MASTER_SESSION_INITIALIZATION_METHOD_NAME = "initMasterSession";
	/**
	 * (re)initializes this session with the given attributes.
	 */
	public void setAttributes(Hashtable attributes);

	/**
	 * Closes this session (cleans up whatever is needed)
	 */
	public void close();
}