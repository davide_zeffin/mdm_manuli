/************************************************************************

	Copyright (c) 2000 by SAP AG

	All rights to both implementation and design are reserved.

	Use and copying of this software and preparation of derivative works
	based upon this software are not permitted.

	Distribution of this software is restricted. SAP does not make any
	warranty about the software, its performance or its conformity to any
	specification.

**************************************************************************/

package com.sap.msasrv.socket.server;

import com.sap.msasrv.socket.shared.Header;
//import com.sap.msasrv.util.zip.SAPZip;

/**
 * The response Object of the server. This class encapsulates the
 * server view of its response object. It offers methods to set
 * parameter values of a Response and to set its return code.<p>
 *
 * To get a client view of a Response object, ie. an object that
 * offers access methods to the parameters of a Response, use the
 * class ParaRequest or ClientSupport to get a Request implementation
 * of a Response.<p>
 *
 * @see com.sap.msasrv.socket.client.ParaRequest
 * @see com.sap.msasrv.socket.client.ClientSupport
 */
public interface Response {

	/**
	 * Returns the header of this response object.
	 */
	public Header getHeader();

	/**
	 * Sets the status code of this response to a given String.
	 * @see com.sap.msasrv.socket.ErrorCodes
	 */
	public void setStatus(String status);

	/**
	 * sets a parameter to a value.
	 * @param	para	the parameter name
	 * @param	val		the value (may be null)
	 */
	public void setParameterValue(String para, String val);

	/**
	 * Set an array parameter to a value. This method performs the same function
	 * as setParameterValue(para+"["+Integer.toString(index)+"]", val), but is
	 * typically slightly faster.
	 */
	public void setParameterValue(String para, int index, String val);

   	/**
	 * 
	 * Set the expected size of the response. In case of RFCReponse
	 * this corresponds to the minimum number of lines expected.
	 * 
	 * @param para
	 * @param size
	 */
	
	public void setExpectedArrayLength(String para, int size);

}
