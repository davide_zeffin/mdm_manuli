/************************************************************************

	Copyright (c) 2000 by SAP AG

	All rights to both implementation and design are reserved.

	Use and copying of this software and preparation of derivative works
	based upon this software are not permitted.

	Distribution of this software is restricted. SAP does not make any
	warranty about the software, its performance or its conformity to any
	specification.

**************************************************************************/
package com.sap.msasrv.socket.server;

import java.util.Hashtable;

/**
 * A remote session holds the state of a whole user session across all modules.
 * This includes all Session implementations, the parameter name converter to
 * be used, and a Hashtable into which commands may put any object in order to
 * remember the session state.
 */
public interface ISessionRemote
{
	/**
	 * Returns a session for an application (= module).
	 * @param   application the id of the application.
	 * @return  a session that has been constructed for this application, or null,
	 * if the application doesn't define/need a session.
	 */
	public Session getSession(String application);

	/**
	 * Changes this session's attributes. All Session instances that have been
	 * created for my application will have their method setAttributes called
	 * with this Hashtable in order to (re-)initialize themselves.
	 */
	public void setAttributes(Hashtable attributes);

	/**
	 * Returns this session's attributes.
	 */
	public Hashtable getAttributes();

	/**
	 * Closes my Sessions and their related objects.
	 */
	public void close();

	/**
	 * Returns <i>true</i> if the session is already closed (not being used anymore)
	 * <i>false</i> otherwise.
	 */
	public boolean isActivelyClosed();
	
	/**
	 * Sets a value of this session. These values are not used by default.
	 * Module commands may use it to remember the session state according
	 * to their wishes. The values of different applications are kept separate.
	 */
	public void putValue(String application, String key, Object value);

	/**
	 * Returns a value that has been put into this session by setValue, or
	 * null, if no value has been set (by putValue) for this combination
	 * of application and key.
	 */
	public Object getValue(String application, String key);

	/**
	 * Initializes this session for a new command. Implementations of this
	 * class that keep an internal status that they wish to reset before a
	 * command is executed should implement this method accordingly.
	 */
	public void initForNewCommand();


	/**
	 * Returns an error that has been detected during the last command,
	 * or null, if no error has been detected or this session doesn't
	 * support error detection. If all your errors throw Exceptions,
	 * use an empty implementation.
	 */
	public String getError();

    /**
	 * sets the SAP client (MANDT) to be used in this session.
	 */
	public void setSAPClient(String sapClient);

    /**
	 * returns the SAP lient (MANDT) used in this session.
	 */
	public String getSAPClient();

	/**
	 * Sets the system id of the system in this session (used for logging/tracing purposes, no
	 * special format).
	 */
	public void setSystem(String system);

	/**
	 * Returns the system id used by this session.
	 */
	public String getSystem();

	/**
	 * Sets the type to be used by this session. What types are valid and in what way they
	 * influence the application is defined by the respective IPC application / module.
	 */
	public void setType(String type);

	/**
	 * Returns the type used by this session.
	 */
	public String getType();

	/**
	 * Changes the application of this session. All former Session objects this session
	 * holds are deleted and replaced by fresh Sessions for the given application.
	 */
	public void setApplication(String application, Hashtable attributes);
	public String getApplication();

	/**
	 * Sets the user location of this session. The user location doesn't have a specific
	 * format. Typically it's a symbolic or numeric IP address or a SAP system id. It is
	 * used for logging/monitoring purposes only.
	 */
	public void setUserLocation(String userLocation);

	/**
	 * returns the user location of this session.
	 */
	public String getUserLocation();

	/**
	 * Sets the user name of this session. The user name doesn't have a specific format;
	 * it is only used for logging and monitoring.
	 */
	public void setUserName(String userName);

	/**
	 * Returns the user name of this session.
	 */
	public String getUserName();

	/**
	 * Returns the currently active module in this session. The module knows which IPC commands
	 * can work on this session.
	 */
	public IModule getModule();

	/**
	 * Sets the session-wide exception memory. Sometimes (e.g. in the SCE engine) the server gets
	 * informed about exceptions that are thrown and caught in other modules. These exceptions
	 * can be marked as "the last exception".
	 */
	public void setLastException(Exception e);

	/**
	 * Returns the last exception set by setLastException.
	 */
	public Exception getLastException();

	/**
	 * Sets a "last error message" string.
	 */
	public void setLastErrorMessage(String message);

	/**
	 * Returns the last error message set by setLastErrorMessage.
	 */
	public String getLastErrorMessage();
}
