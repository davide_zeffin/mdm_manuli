package com.sap.spc.remote.shared;

/**
 * The most basic request interface. Implement this with all kinds of requests
 * (eg. IPC request, HTTP request) to get methods like ExternalConfigConverter.getConfig to work.
 */
public interface SimpleRequest {
	public String getParameterValue(String name);
	public String getParameterValue(String name, String defaultValue);
	public String[] getParameterValues(String name);
	public String[] getParameterValues(String name, String[] defaultValues);
}