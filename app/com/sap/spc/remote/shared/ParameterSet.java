package com.sap.spc.remote.shared;


/**
 * Wrapper around things that can set a value. To be used to transparently set values of responses
 * in the server or requests in the client.
 */
public interface ParameterSet
{
	public void setValue(String key, String value);
	public void setValue(String key, int index, String value);
}