package com.sap.spc.remote.shared;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.Locale;
import java.util.Map;
import java.util.Vector;
import java.util.Enumeration;

import com.sap.sce.kbrt.*;
import com.sap.sce.kbrt.imp.*;

import com.sap.spc.remote.shared.command.ExtConfigConstants;
import com.sap.spc.remote.shared.command.SCECommand;
import com.sap.sxe.sys.exc.exc_internal_error;
import com.sap.msasrv.socket.shared.ErrorConstants;


/**
 * Provides methods (or rather: a method) that convert external configurations to IPC parameter name/value pairs
 * and backward.
 */
public class ExternalConfigConverter implements ExtConfigConstants, SCECommand
{
    /* a3-349tl:
     * Converts a double to a String that has at most
     * 15 characters as specified in the RFCCommands.xml. 
     * The double is rounded to fit into the 15 digits. 
     */
    private static DecimalFormatSymbols dfs = new DecimalFormatSymbols(Locale.US);
    //used positive numbers with a 2 digit exponent (e.g. E1)
    private static DecimalFormat C_DF_1 = new DecimalFormat("0.0##########E0", dfs);
    //used for negative numbers with a 2 digit-exponent (e.g. E1) and positive numbers
    //with a 3 digit exponent (E-1, E11)
    private static DecimalFormat C_DF_2 = new DecimalFormat("0.0#########E0", dfs);
    //used for negative numbers with a 3 digit-exponent (e.g. E-1) and positive numbers
    //with a 4 digit exponent (e.g. E-11, E111)
    private static DecimalFormat C_DF_3 = new DecimalFormat("0.0########E0", dfs);
    //used for negative numbers with a 4 digit-exponent (e.g. E-11, E111) and positive numbers
    //with a 5 digit exponent (e.g. E-111)
    private static DecimalFormat C_DF_4 = new DecimalFormat("0.0#######E0", dfs);
    //used for negative numbers with a 5 digit-exponent (e.g. E-111)
    private static DecimalFormat C_DF_5 = new DecimalFormat("0.0######E0", dfs);
    private static double        C_10_POW_PLUS_7 = Math.pow(10.0, 7.0); 
    private static double        C_10_POW_PLUS_10 = Math.pow(10.0, 10.0); 
    private static double        C_10_POW_PLUS_100 = Math.pow(10.0, 100.0); 
    private static double        C_10_POW_MINUS_3 = Math.pow(10.0, -3.0); 
    private static double        C_10_POW_MINUS_9 = Math.pow(10.0, -9.0); 
    private static double        C_10_POW_MINUS_99= Math.pow(10.0, -99.0); 
    private static String _convertDoubleTo15CharactersString(double priceFactor){
        double magnitude = Math.abs(priceFactor);
        String factor = null;
        //magnitude >= 1.0E100
        if(magnitude >= C_10_POW_PLUS_100){
            if(priceFactor >= 0)
                factor = C_DF_3.format(priceFactor);
            else
                factor = C_DF_4.format(priceFactor);
        }
        //1.0E100 > magnitude >= 1.0E10
        else if(magnitude >= C_10_POW_PLUS_10){
            if(priceFactor >= 0)
                factor = C_DF_2.format(priceFactor);
            else
                factor = C_DF_3.format(priceFactor);
        }
        //1.0E10 > magnitude >= 1.0E7
        else if(magnitude >= C_10_POW_PLUS_7){
            if(priceFactor >= 0)
                factor = C_DF_1.format(priceFactor);
            else
                factor = C_DF_2.format(priceFactor);
        }
        //if factor is just a number between 1.0E-3 and 1.0E7
        //the digits exceeding the index 15 are just cut of without rounding
        else if(C_10_POW_PLUS_7 > magnitude &&  magnitude >= C_10_POW_MINUS_3 ){
            factor = Double.toString(priceFactor);
            factor = factor.substring(0, Math.min(factor.length(), 15));
        }
        //1.0E-3 >= magnitude > 1.0E-10
        else if(C_10_POW_MINUS_3 > magnitude && magnitude > C_10_POW_MINUS_9){
            if(priceFactor >= 0)
                factor = C_DF_2.format(priceFactor);
            else
                factor = C_DF_3.format(priceFactor);
        }
        //1.0E-10 >= magnitude > 1.0E-100
        else if(C_10_POW_MINUS_9 >= magnitude && magnitude > C_10_POW_MINUS_99){
            if(priceFactor >= 0)
                factor = C_DF_3.format(priceFactor);
            else
                factor = C_DF_4.format(priceFactor);
        }
        //1.0E-100 >= magnitude > 0.0
        else if(C_10_POW_MINUS_99 >= magnitude ){
            if(priceFactor >= 0)
                factor = C_DF_4.format(priceFactor);
            else
                factor = C_DF_5.format(priceFactor);
        }
        else throw new exc_internal_error("Pricing factor could not be converted to String: " + priceFactor);
        
        if(factor.length() > 15) throw new exc_internal_error("Pricing factor must not exceed 15 digits! " + "\n" +
                                                              "double " + priceFactor + " is converted to " + factor);
        return factor;
    }
    /* a3-349tl:
     * Converts a double to a String that represents that has at most
     * 15 characters as specified in the RFCCommands.xml. 
     * The double is in contrary to _convertDoubleStringTo15Characters not rounded. 
     */
    private static String _convertDoubleTo15CharactersString2(double priceFactor){
        String factor = String.valueOf(priceFactor);
        int index = factor.indexOf('E');
        //we cut out the intermediate part of the string, this has to be done in
        //two steps: cut the front part, cut the back part and concatenate them
        if(index != -1 && factor.length() > 15){
            int length = factor.length();
            //this is the most important part of the double and must not be cut
            String exponent = factor.substring(index, length);
            //base will contain the relevant digits but one
            String base = factor.substring(0, 15 - exponent.length());
            //concatenation of front and back
            factor = base + exponent;
        }
        //if factor is just a number between 1.0E-3 and 1.0E10
        //the digits exceeding the index 15 are just cut of
        else if(factor.length() > 15){
            factor = factor.substring(0, 15);
        }
        return factor;
    }
    
    private static void _getConfigPrices(Vector priceSeqs, int cfgId, ParameterSet param, boolean useIndices) {

		int index = 0;
		for (Enumeration enum=priceSeqs.elements(); enum.hasMoreElements();) {
			cfg_ext_price_key_seq seq = (cfg_ext_price_key_seq)enum.nextElement();

			for (Enumeration e = seq.elements(); e.hasMoreElements();) {
				cfg_ext_price_key key = (cfg_ext_price_key)e.nextElement();
				index++;
                //a3-349tl: priceFactor is cut to have at most 15 digits
                double factor = key.get_factor();
                String priceFactor = _convertDoubleTo15CharactersString(factor);
				setParameterValue(param, EXT_PRICE_CONFIG_ID,   useIndices, index, Integer.toString(cfgId));
				setParameterValue(param, EXT_PRICE_INSTANCE_ID, useIndices, index, key.get_inst_id().toString());
				setParameterValue(param, EXT_PRICE_FACTOR,      useIndices, index, priceFactor);
				setParameterValue(param, EXT_PRICE_KEY,         useIndices, index, key.get_key());
			}
			cfgId++;
		}
	}


	private static void _getConfigCsticsVals(Vector csticVals, int cfgId, ParameterSet param, boolean useIndices) {

		int index = 0;

		for (Enumeration e=csticVals.elements(); e.hasMoreElements();) {
			cfg_ext_cstic_val_seq seq = (cfg_ext_cstic_val_seq)e.nextElement();
			for (Enumeration enum = seq.elements(); enum.hasMoreElements();) {
				index++;

				cfg_ext_cstic_val item = (cfg_ext_cstic_val)enum.nextElement();

				setParameterValue(param, EXT_VALUE_CONFIG_ID,   useIndices, index, Integer.toString(cfgId));
				setParameterValue(param, EXT_VALUE_INSTANCE_ID, useIndices, index, ""+item.get_inst_id());
				setParameterValue(param, EXT_VALUE_CSTIC_NAME,  useIndices, index, item.get_charc());
				setParameterValue(param, EXT_VALUE_CSTIC_LNAME, useIndices, index, item.get_charc_txt());
				setParameterValue(param, EXT_VALUE_NAME,        useIndices, index, item.get_value());
				setParameterValue(param, EXT_VALUE_LNAME,       useIndices, index, item.get_value_txt());
				setParameterValue(param, EXT_VALUE_AUTHOR,      useIndices, index, item.get_author());
			}
			cfgId++;
		}
	}


	private static void _getConfigInsts(Vector instances, int cfgId, ParameterSet param, boolean useIndices) {

		int index = 0;

		for (Enumeration e=instances.elements(); e.hasMoreElements();) {
			cfg_ext_inst_seq insts = (cfg_ext_inst_seq)e.nextElement();
			for (Enumeration enum = insts.elements(); enum.hasMoreElements();) {
				cfg_ext_inst inst = (cfg_ext_inst)enum.nextElement();
				index++;

				setParameterValue(param, EXT_INST_CONFIG_ID,        useIndices, index, Integer.toString(cfgId));
				setParameterValue(param, EXT_INST_ID,               useIndices, index, ""+inst.get_inst_id());
				setParameterValue(param, EXT_INST_OBJECT_TYPE,      useIndices, index, inst.get_obj_type());
				setParameterValue(param, EXT_INST_CLASS_TYPE,       useIndices, index, inst.get_class_type());
				setParameterValue(param, EXT_INST_OBJECT_KEY,       useIndices, index, inst.get_obj_key());
				setParameterValue(param, EXT_INST_OBJECT_TEXT,      useIndices, index, inst.get_obj_txt());
				setParameterValue(param, EXT_INST_QUANTITY,         useIndices, index, inst.get_quantity());
				setParameterValue(param, EXT_INST_QUANTITY_UNIT,    useIndices, index, inst.get_quantity_unit());
				setParameterValue(param, EXT_INST_AUTHOR,           useIndices, index, inst.get_author());
				setParameterValue(param, EXT_INST_COMPLETE,         useIndices, index, inst.is_complete_p() ? TRUE : FALSE);
				setParameterValue(param, EXT_INST_CONSISTENT,       useIndices, index, inst.is_consistent_p() ? TRUE : FALSE);
			}
			cfgId++;
		}
	}


	private static void _getConfigItems(Vector instances, Vector instIdToItemIdTables, int cfgId, ParameterSet param, boolean useIndices) {

		int index = 0;
		Enumeration tables = instIdToItemIdTables.elements();
		for (Enumeration e=instances.elements(); e.hasMoreElements();) {
			cfg_ext_inst_seq insts = (cfg_ext_inst_seq)e.nextElement();
			Hashtable instIdToItemIdTable = (Hashtable)tables.nextElement();
			for (Enumeration enum = insts.elements(); enum.hasMoreElements();) {
				cfg_ext_inst inst = (cfg_ext_inst)enum.nextElement();

				String instId = Integer.toString(inst.get_inst_id().intValue());
				String itemId = (String)instIdToItemIdTable.get(instId);

				index++;
				setParameterValue(param, EXT_POS_CONFIG_ID, useIndices, index, Integer.toString(cfgId));
				setParameterValue(param, EXT_POS_INST_ID,   useIndices, index, instId);
				setParameterValue(param, EXT_POS_ITEM_ID,   useIndices, index, itemId);
			}
			cfgId++;
		}
	}


	private static void _getConfigParts(Vector partsVec, int cfgId, ParameterSet param, boolean useIndices) {

		int index = 0;

		for (Enumeration e=partsVec.elements(); e.hasMoreElements();) {
			cfg_ext_part_seq parts = (cfg_ext_part_seq)e.nextElement();
			for (Enumeration enum = parts.elements(); enum.hasMoreElements();) {
				cfg_ext_part part = (cfg_ext_part)enum.nextElement();

				index++;
				setParameterValue(param, EXT_PART_CONFIG_ID,       useIndices, index, Integer.toString(cfgId));
				setParameterValue(param, EXT_PART_PARENT_ID,       useIndices, index, ""+part.get_parent_id());
				setParameterValue(param, EXT_PART_INST_ID,         useIndices, index, ""+part.get_inst_id());
				setParameterValue(param, EXT_PART_POS_NR,          useIndices, index, part.get_pos_nr());
				setParameterValue(param, EXT_PART_OBJECT_TYPE,     useIndices, index, part.get_obj_type());
				setParameterValue(param, EXT_PART_CLASS_TYPE,      useIndices, index, part.get_class_type());
				setParameterValue(param, EXT_PART_OBJECT_KEY,      useIndices, index, part.get_obj_key());
				setParameterValue(param, EXT_PART_AUTHOR,          useIndices, index, part.get_author());
				setParameterValue(param, EXT_PART_SALES_RELEVANT,  useIndices, index, part.is_sales_relevant_p() ? SALES_RELEVANT:"");
			}
			cfgId++;
		}
	}


	/**
	 * Updates param with all IPC parameter name/value pairs that are needed to represent all external configurations
	 * in extCfgs. Only to be used by server components!
	 * @param   extCfg  Vector of ext_configuration
	 * @param   instIdToItemIdTables Vector of (Hashtable mapping String instids to String itemIds). May be null if no item mapping is needed
	 * @param   posExInt initial config position number (eg. 1)
	 * @param   cfgId initial config id (eg. 1)
	 * @param   something implementing ParameterSet that will be updated by this method.
	 * @return  nothing - param will be updated.
	 */
	public static void getConfigs(Vector extCfgs, Vector instIdToItemIdTables, int posExInt, int cfgId, ParameterSet param) {
		getConfigs(extCfgs, instIdToItemIdTables, posExInt, cfgId, param, true);
	}

	/**
	 * Updates param with all IPC parameter name/value pairs that are needed to represent all external configurations
	 * in extCfgs.
	 * @param   extCfg  Vector of ext_configuration
	 * @param   instIdToItemIdTables Vector of (Hashtable mapping String instids to String itemIds). May be null if no item mapping is needed
	 * @param   posExInt initial config position number (eg. 1)
	 * @param   cfgId initial config id (eg. 1)
	 * @param   something implementing ParameterSet that will be updated by this method.
	 * @param   useIndices attach a numeric index [<number>] to each parameter name. If you want to use the result
	 * in a server response, use true here, if you want to use this in a request, use false.
	 * @return  nothing - param will be updated.
	 */
	public static void getConfigs(Vector extCfgs, Vector instIdToItemIdTables, int posExInt, int cfgId, ParameterSet param, boolean useIndices) {

		//BAPICUCFG
		int initialCfgId = cfgId;

		Vector parts = new Vector();
		Vector insts = new Vector();
		Vector csticVals = new Vector();
		Vector conditions = new Vector();

		for (Enumeration e = extCfgs.elements(); e.hasMoreElements();) {
			ext_configuration extCfg = (ext_configuration)e.nextElement();

			setParameterValue(param, EXT_CONFIG_POSITION,    useIndices, cfgId, Integer.toString(posExInt));
			posExInt++;
			setParameterValue(param, EXT_CONFIG_ID,          useIndices, cfgId, Integer.toString(cfgId));
			setParameterValue(param, EXT_CONFIG_ROOT_ID,     useIndices, cfgId, ""+extCfg.get_root_id());
			setParameterValue(param, EXT_CONFIG_SCE_MODE,    useIndices, cfgId, SCE_MODE);
			setParameterValue(param, EXT_CONFIG_KB_NAME,     useIndices, cfgId, extCfg.get_kb_name());
			setParameterValue(param, EXT_CONFIG_KB_VERSION,  useIndices, cfgId, extCfg.get_kb_version());
			setParameterValue(param, EXT_CONFIG_KB_PROFILE,  useIndices, cfgId, extCfg.get_kb_profile_name());
			setParameterValue(param, EXT_CONFIG_KB_BUILD,    useIndices, cfgId, ""+extCfg.get_kb_build());
			setParameterValue(param, EXT_CONFIG_INFO,        useIndices, cfgId, extCfg.get_cfg_info());

			setParameterValue(param, EXT_CONFIG_KB_LANGUAGE, useIndices, cfgId, extCfg.get_language());
			setParameterValue(param, EXT_CONFIG_NAME,        useIndices, cfgId, "");
			setParameterValue(param, EXT_CONFIG_COMPLETE,    useIndices, cfgId, extCfg.is_complete_p()? TRUE: FALSE);

			setParameterValue(param, EXT_CONFIG_CONSISTENT,  useIndices, cfgId, extCfg.is_consistent_p()? TRUE: FALSE);

			setParameterValue(param, EXT_CONFIG_SCE_VERSION, useIndices, cfgId, extCfg.get_sce_version());

			parts.addElement(extCfg.get_parts());
			insts.addElement(extCfg.get_insts());
			csticVals.addElement(extCfg.get_cstics_values());
			conditions.addElement(extCfg.get_price_keys());
			cfgId++;
		}

		_getConfigParts(parts, initialCfgId, param, useIndices);
		_getConfigInsts(insts, initialCfgId, param, useIndices);
		if (instIdToItemIdTables != null) {
		    _getConfigItems(insts, instIdToItemIdTables, initialCfgId, param, useIndices);
		}
		_getConfigCsticsVals(csticVals, initialCfgId, param, useIndices);
		_getConfigPrices(conditions, initialCfgId, param, useIndices);
	}




	public static ext_configuration createConfig(SimpleRequest request, String client,
										  String kbName, String kbVersion, String kbProfile,
										  String kbLanguage, String defaultSceVersion) throws ConfigException {

		if (kbName == null) {
			kbName = request.getParameterValue(EXT_CONFIG_KB_NAME);
		}
		if (kbVersion == null) {
			kbVersion = request.getParameterValue(EXT_CONFIG_KB_VERSION);
		}
		if (kbProfile == null) {
			kbProfile = request.getParameterValue(EXT_CONFIG_KB_PROFILE);
		}
		if (kbLanguage == null) {
			kbLanguage = request.getParameterValue(EXT_CONFIG_KB_LANGUAGE);
		}
		if (defaultSceVersion == null) {
		    defaultSceVersion = request.getParameterValue(EXT_CONFIG_SCE_VERSION);
		}
		c_ext_cfg_imp writable = new c_ext_cfg_imp(request.getParameterValue(EXT_CONFIG_NAME,kbName),
																			  request.getParameterValue(EXT_CONFIG_SCE_VERSION,defaultSceVersion),
																			  client,
																			  kbName,
																			  kbVersion,
																			  Integer.parseInt(request.getParameterValue(EXT_CONFIG_KB_BUILD,"1").trim()),
																			  kbProfile,
																			  // 20001127-kha: the KB language is now always ignored. Instead, the language of the document
																			  // is used. This makes sense: If I configure in English, leave the system, come back in German
																			  // to continue my configuration I don't want the saved language for the language dependent
																			  // texts but the current document language that is consistent with the UI texts.
																			  //request.getParameterValue(EXT_CONFIG_KB_LANGUAGE,kbLanguage),
																			  kbLanguage,
																			  new Integer(getRequiredParameter(request,EXT_CONFIG_ROOT_ID).trim()),
																			  request.getParameterValue(EXT_CONFIG_CONSISTENT,SCECommand.TRUE).equals(SCECommand.TRUE),
																			  request.getParameterValue(EXT_CONFIG_COMPLETE,SCECommand.TRUE).equals(SCECommand.TRUE)
																			  );
		String configInfo = request.getParameterValue(EXT_CONFIG_INFO);
		if (configInfo != null)
			writable.set_cfg_info(configInfo);

		// add instance info
		String[] instIds = getRequiredParameters(request, EXT_INST_ID);
		String[] instObjType = getRequiredParameters(request, EXT_INST_OBJECT_TYPE);
		String[] instClassType = getRequiredParameters(request, EXT_INST_CLASS_TYPE);
		String[] instObjKey = getRequiredParameters(request, EXT_INST_OBJECT_KEY);
		String[] instObjText = getRequiredParameters(request, EXT_INST_OBJECT_TEXT);
		String[] instAuthor = request.getParameterValues(EXT_INST_AUTHOR, constantStringArray(instIds.length, " "));
		String[] instQuantity = getRequiredParameters(request, EXT_INST_QUANTITY);
		String[] defaultConsistent = new String[instIds.length];
		String[] defaultComplete   = new String[instIds.length];
		String[] instConsistent = request.getParameterValues(EXT_INST_CONSISTENT, constantStringArray(instIds.length, SCECommand.TRUE));
		String[] instComplete = request.getParameterValues(EXT_INST_COMPLETE, constantStringArray(instIds.length, SCECommand.TRUE));

		// quantity units are optional with "ST" as default
		String[] instQuantityUnits = request.getParameterValues(EXT_INST_QUANTITY_UNIT, constantStringArray(instIds.length, "ST"));

		checkLength(new int[] { instIds.length, instObjType.length, instClassType.length, instObjKey.length,
					instObjText.length, instAuthor.length, instQuantity.length,
					instConsistent.length, instComplete.length, instQuantityUnits.length },
					new String[] { "inst id", "object type", "class type", "object key", "object text",
						"author", "quantity", "consistent", "complete", "quantity unit" } );

		for (int i=0; i<instIds.length; i++) {
			if (instAuthor[i] == null || instAuthor[i].length() == 0)
				instAuthor[i] = " ";

			writable.add_inst(new Integer(instIds[i].trim()),
							  instObjType[i], instClassType[i],
							  instObjKey[i], instObjText[i],
							  instAuthor[i], instQuantity[i],
							  instQuantityUnits[i],
							  instConsistent[i].equals(SCECommand.TRUE),
							  instComplete[i].equals(SCECommand.TRUE));
		}

		// add part info (if available)
		String[] partParentId = request.getParameterValues(EXT_PART_PARENT_ID);
		if (partParentId != null) {
			String[] partInstId = getRequiredParameters(request,EXT_PART_INST_ID);
			String[] partPosNr = getRequiredParameters(request,EXT_PART_POS_NR);
			String[] partObjType = getRequiredParameters(request,EXT_PART_OBJECT_TYPE);
			String[] partClassType = getRequiredParameters(request,EXT_PART_CLASS_TYPE);
			String[] partObjKey = getRequiredParameters(request,EXT_PART_OBJECT_KEY);
			String[] partAuthor = request.getParameterValues(EXT_PART_AUTHOR, constantStringArray(partParentId.length, " "));
			// the sales relevant array is not part of the BAPI interface, so we make it
			// optional. External configs and CRM provide it, BAPI will use the default
			String[] partSalesRelevant = request.getParameterValues(EXT_PART_SALES_RELEVANT,constantStringArray(partParentId.length, "X"));

			checkLength(new int[] { partParentId.length, partInstId.length, partPosNr.length,
						partObjType.length, partClassType.length, partObjKey.length,
						partAuthor.length, partSalesRelevant.length },
						new String[] { "part parent id", "part inst id", "part posnr", "part object type",
							"part class type", "part object key", "part author", "part sales relevant" } );

			for (int i=0; i<partParentId.length; i++) {
				if (partAuthor[i] == null || partAuthor[i].length() == 0)
					partAuthor[i] = " ";
				writable.add_part(new Integer(partParentId[i].trim()),
								  new Integer(partInstId[i].trim()),
								  partPosNr[i],
								  partObjType[i], partClassType[i],
								  partObjKey[i], partAuthor[i],
								  partSalesRelevant[i].equals(SCECommand.SALES_RELEVANT));
			}
		}

		// add cstic value info (if available)
		String[] valueInstId = request.getParameterValues(EXT_VALUE_INSTANCE_ID);
		if (valueInstId != null) {
			String[] valueCsticName = getRequiredParameters(request,EXT_VALUE_CSTIC_NAME);
			String[] valueCsticLName = getRequiredParameters(request,EXT_VALUE_CSTIC_LNAME);
			String[] valueName = getRequiredParameters(request,EXT_VALUE_NAME);
			String[] valueLName = getRequiredParameters(request,EXT_VALUE_LNAME);
			String[] valueAuthor = request.getParameterValues(EXT_VALUE_AUTHOR, constantStringArray(valueCsticName.length, " "));

			checkLength(new int[] { valueInstId.length, valueCsticName.length,
						valueCsticLName.length, valueName.length,
						valueLName.length, valueAuthor.length }, new String[] { "value inst id",
							"cstic name", "cstic lname", "value name", "value lname", "value author" } );

			for (int i=0; i<valueInstId.length; i++) {
				if (valueAuthor[i] == null || valueAuthor[i].length() == 0)
					valueAuthor[i] = " ";
				writable.add_cstic_value(new Integer(valueInstId[i].trim()),
										 valueCsticName[i], valueCsticLName[i],
										 valueName[i], valueLName[i],
										 valueAuthor[i]);
			}
		}

		// add price keys (if available)
		String[] priceInstId = request.getParameterValues(EXT_PRICE_INSTANCE_ID);
		if (priceInstId != null) {
			String[] priceKey = getRequiredParameters(request,EXT_PRICE_KEY);
			String[] priceFactor = getRequiredParameters(request,EXT_PRICE_FACTOR);

			checkLength(new int[] { priceInstId.length, priceKey.length, priceFactor.length },
						new String[] { "price inst id", "price key", "price factor" }
						);

			for (int i=0; i<priceInstId.length; i++) {
				writable.add_price_key(new Integer(priceInstId[i].trim()),
									   priceKey[i],
									   Double.valueOf(priceFactor[i].trim()).doubleValue());
			}
		}

		return writable;
	}
	private static Map _readConfigs(SimpleRequest request, String client,
	String defaultLanguage,
	String defaultSceVersion)
	{
		Map configs = new HashMap();
		String configName[] = request.getParameterValues(EXT_CONFIG_NAME);
		String configId[] = request.getParameterValues(EXT_CONFIG_ID);

		String configRootId[] = request.getParameterValues(EXT_CONFIG_ROOT_ID);
		String kbName[] = request.getParameterValues(EXT_CONFIG_KB_NAME);
		String kbVersion[] = request.getParameterValues(EXT_CONFIG_KB_VERSION);
		String kbProfile[] = request.getParameterValues(EXT_CONFIG_KB_PROFILE);
		String kbBuild[] =
			request.getParameterValues(
				EXT_CONFIG_KB_BUILD,
				constantStringArray(kbName.length, "1"));
		String kbLanguage[] =
			request.getParameterValues(EXT_CONFIG_KB_LANGUAGE);
		String sceVersion[] =
			request.getParameterValues(
				EXT_CONFIG_SCE_VERSION,
				constantStringArray(kbName.length, defaultSceVersion));
		String configConsistent[] =
			request.getParameterValues(
				EXT_CONFIG_CONSISTENT,
				constantStringArray(kbName.length, SCECommand.TRUE));
		String configComplete[] =
			request.getParameterValues(
				EXT_CONFIG_COMPLETE,
				constantStringArray(kbName.length, SCECommand.TRUE));
		String configInfo[] = request.getParameterValues(EXT_CONFIG_INFO);

		for (int k = 0; k < kbName.length; k++) {
			c_ext_cfg_imp writable =
				new c_ext_cfg_imp(
					configName[k],
					sceVersion[k],
					client,
					kbName[k],
					kbVersion[k],
					Integer.parseInt(kbBuild[k].trim()),
					kbProfile[k],
					kbLanguage[k],
					new Integer(configRootId[k].trim()),
					configConsistent[k].equals(SCECommand.TRUE),
					configComplete[k].equals(SCECommand.TRUE));
			if (configInfo != null)
				writable.set_cfg_info(configInfo[k]);
			configs.put(configId[k], writable);
		}
		return configs;
	}
	
	public static ext_configuration[] createConfigs(
		SimpleRequest request,
		String client,
		String defaultLanguage,
		String defaultSceVersion)
		throws ConfigException {
			
		Map configs = _readConfigs(request, client, defaultLanguage, defaultSceVersion);
		c_ext_cfg_imp writableArray[]= new c_ext_cfg_imp[configs.size()];
		String configId[] = request.getParameterValues(EXT_CONFIG_ID);
		for(int idx=0;idx < configId.length;idx++)
		{
			c_ext_cfg_imp writable = (c_ext_cfg_imp)configs.get(configId[idx]);
			writableArray[idx] = writable;
		}
		_addInstances(request, configs);
		_addParts(request, configs);
        _addValues(request, configs);
        _addPrices(request, configs);		
		return writableArray;
	}

	private static void _addPrices(SimpleRequest request, Map configs) throws ConfigException {
		// add price keys (if available)
		String[] priceInstId = request.getParameterValues(EXT_PRICE_INSTANCE_ID);
		if (priceInstId != null) {
			String[] priceConfigId = request.getParameterValues(EXT_PRICE_CONFIG_ID);
			String[] priceKey = getRequiredParameters(request,EXT_PRICE_KEY);
			String[] priceFactor = getRequiredParameters(request,EXT_PRICE_FACTOR);

			checkLength(new int[] { priceInstId.length, priceKey.length, priceFactor.length },
						new String[] { "price inst id", "price key", "price factor" }
						);

			for (int i=0; i<priceInstId.length; i++) {
				c_ext_cfg_imp writable = (c_ext_cfg_imp)configs.get(priceConfigId[i]);
				writable.add_price_key(new Integer(priceInstId[i].trim()),
									   priceKey[i],
									   Double.valueOf(priceFactor[i].trim()).doubleValue());
			}
		}
	}
	private static void _addValues(SimpleRequest request, Map configs) throws ConfigException {
		// add cstic value info (if available)
		String[] valueInstId = request.getParameterValues(EXT_VALUE_INSTANCE_ID);
		
		if (valueInstId != null) {
			String[] valueConfigIds = request.getParameterValues(EXT_VALUE_CONFIG_ID);
			String[] valueCsticName = getRequiredParameters(request,EXT_VALUE_CSTIC_NAME);
			String[] valueCsticLName = getRequiredParameters(request,EXT_VALUE_CSTIC_LNAME);
			String[] valueName = getRequiredParameters(request,EXT_VALUE_NAME);
			String[] valueLName = getRequiredParameters(request,EXT_VALUE_LNAME);
			String[] valueAuthor = request.getParameterValues(EXT_VALUE_AUTHOR, constantStringArray(valueCsticName.length, " "));

			checkLength(new int[] { valueConfigIds.length, valueInstId.length, valueCsticName.length,
						valueCsticLName.length, valueName.length,
						valueLName.length, valueAuthor.length }, new String[] { "value config id", "value inst id",
							"cstic name", "cstic lname", "value name", "value lname", "value author" } );

			for (int i=0; i<valueInstId.length; i++) {
				if (valueAuthor[i] == null || valueAuthor[i].length() == 0)
					valueAuthor[i] = " ";
				c_ext_cfg_imp writable = (c_ext_cfg_imp)configs.get(valueConfigIds[i]);
				writable.add_cstic_value(new Integer(valueInstId[i].trim()),
										 valueCsticName[i], valueCsticLName[i],
										 valueName[i], valueLName[i],
										 valueAuthor[i]);
			}
		}

		
	}
	private static void _addInstances(SimpleRequest request, Map configs) throws ConfigException {
		// add instance info
		String[] instConfigIds = getRequiredParameters(request, EXT_INST_CONFIG_ID);
		String[] instIds = getRequiredParameters(request, EXT_INST_ID);
		String[] instObjType = getRequiredParameters(request, EXT_INST_OBJECT_TYPE);
		String[] instClassType = getRequiredParameters(request, EXT_INST_CLASS_TYPE);
		String[] instObjKey = getRequiredParameters(request, EXT_INST_OBJECT_KEY);
		String[] instObjText = getRequiredParameters(request, EXT_INST_OBJECT_TEXT);
		String[] instAuthor = request.getParameterValues(EXT_INST_AUTHOR, constantStringArray(instIds.length, " "));
		String[] instQuantity = getRequiredParameters(request, EXT_INST_QUANTITY);
		String[] defaultConsistent = new String[instIds.length];
		String[] defaultComplete   = new String[instIds.length];
		String[] instConsistent = request.getParameterValues(EXT_INST_CONSISTENT, constantStringArray(instIds.length, SCECommand.TRUE));
		String[] instComplete = request.getParameterValues(EXT_INST_COMPLETE, constantStringArray(instIds.length, SCECommand.TRUE));
		String[] instQuantityUnits = request.getParameterValues(EXT_INST_QUANTITY_UNIT, constantStringArray(instIds.length, "ST"));

		checkLength(new int[] { instConfigIds.length, instIds.length, instObjType.length, instClassType.length, instObjKey.length,
					instObjText.length, instAuthor.length, instQuantity.length,
					instConsistent.length, instComplete.length, instQuantityUnits.length },
					new String[] { "inst config id", "inst id", "object type", "class type", "object key", "object text",
						"author", "quantity", "consistent", "complete", "quantity unit" } );

		for (int i=0; i<instIds.length; i++) {
			if (instAuthor[i] == null || instAuthor[i].length() == 0)
				instAuthor[i] = " ";
			c_ext_cfg_imp writable = (c_ext_cfg_imp)configs.get(instConfigIds[i]);
			writable.add_inst(new Integer(instIds[i].trim()),
							  instObjType[i], instClassType[i],
							  instObjKey[i], instObjText[i],
							  instAuthor[i], instQuantity[i],
							  instQuantityUnits[i],
							  instConsistent[i].equals(SCECommand.TRUE),
							  instComplete[i].equals(SCECommand.TRUE));
		}		
	}
	private static void _addParts(SimpleRequest request, Map configs) throws ConfigException
	{
		String[] partParentId = request.getParameterValues(EXT_PART_PARENT_ID );
		if (partParentId != null) {
			String[] partConfigId = getRequiredParameters(request,EXT_PART_CONFIG_ID);
			String[] partInstId = getRequiredParameters(request,EXT_PART_INST_ID);
			String[] partPosNr = getRequiredParameters(request,EXT_PART_POS_NR);
			String[] partObjType = getRequiredParameters(request,EXT_PART_OBJECT_TYPE);
			String[] partClassType = getRequiredParameters(request,EXT_PART_CLASS_TYPE);
			String[] partObjKey = getRequiredParameters(request,EXT_PART_OBJECT_KEY);
			String[] partAuthor = request.getParameterValues(EXT_PART_AUTHOR, constantStringArray(partParentId.length, " "));
			// the sales relevant array is not part of the BAPI interface, so we make it
			// optional. External configs and CRM provide it, BAPI will use the default
			String[] partSalesRelevant = request.getParameterValues(EXT_PART_SALES_RELEVANT ,constantStringArray(partParentId.length, "X"));

			checkLength(new int[] { partParentId.length, partInstId.length, partPosNr.length,
						partObjType.length, partClassType.length, partObjKey.length,
						partAuthor.length, partSalesRelevant.length },
						new String[] { "part parent id", "part inst id", "part posnr", "part object type",
							"part class type", "part object key", "part author", "part sales relevant" } );

			for (int i=0; i<partParentId.length; i++) {
				if (partAuthor[i] == null || partAuthor[i].length() == 0)
					partAuthor[i] = " ";
				c_ext_cfg_imp writable = (c_ext_cfg_imp)configs.get(partConfigId[i]);					
				writable.add_part(new Integer(partParentId[i].trim()),
								  new Integer(partInstId[i].trim()),
								  partPosNr[i],
								  partObjType[i], partClassType[i],
								  partObjKey[i], partAuthor[i],
								  partSalesRelevant[i].equals(SCECommand.SALES_RELEVANT));
			}
		}
		
	}
	 
	private static String[] getRequiredParameters(SimpleRequest request, String name) throws ConfigException {
		String[] value = request.getParameterValues(name);
		if (value == null) {
			throw new ConfigException(ErrorConstants.REQUIRED_PARAMETER_NOT_FOUND, new String[] { name });
		}
		else {
		    return value;
		}
	}


	private static String getRequiredParameter(SimpleRequest request, String name) throws ConfigException {
		String value = request.getParameterValue(name);
		if (value == null) {
			throw new ConfigException(ErrorConstants.REQUIRED_PARAMETER_NOT_FOUND, new String[] { name });
		}
		else {
		    return value;
		}
	}


	// HACK! This is mostly pasted from com.sap.sxe.socket.server.command. TODO: integrate the two calls
	private static void checkLength(int[] lengths, String[] ids) throws ConfigException {
		if (lengths.length < 2)
			return;
		int length = lengths[0];
		for (int i=1; i<lengths.length; i++)
			if (length != lengths[i])
				throw new ConfigException(ErrorConstants.PARAMETER_ARRAY_LENGTH_MISMATCH,
									      new String[] { ids[0], ids[i] });
	}

	// HACK! This is pasted from com.sap.sxe.socket.server.command. TODO: integrate the two calls
	private static String[] constantStringArray(int length, String value) {
		String[] a = new String[length];
		for (int i=0; i<length; i++)
			a[i] = value;
		return a;
	}


	private static void setParameterValue(ParameterSet set, String param, boolean useIndices, int index, String value) {
		if (useIndices) {
			set.setValue(param, index, value);
		}
		else {
			set.setValue(param, value);
		}
	}
}
