package com.sap.spc.remote.shared;

public class ConfigException extends Exception {

	private int id;
	private String[] params;

    public ConfigException(int id, String[] params) {
		super("Couldn't read external configuration");
		this.id = id;
		this.params = params;
    }
    
	public int getId() {
		return id;
	}

	public String[] getParams() {
		return params;
	}
}