package com.sap.spc.remote.shared;

/**
 * Manages configuration ids - knows how to name a config id for session-wide configs,
 * and the same for configs inside documents and items.
 *
 * A note about configuration ids: As of IPC 3.0A, configurations may always be
 * identified by a config id. Config ids are unique within a session. There are
 * two kinds of configurations: Those that are created by CreateConfig and those
 * that are created by CreateItem. The former are session wide. Their ID is an
 * "S" followed by an id that is unique within the session. The later belong to
 * an item. Their ID is a "I", followed by a document id, a "/" and an item id.
 * The actual id formats are subject to change.
 */
public class ConfigIdManager {

	public static String getConfigId(String documentId, String itemId) {
		return "I"+documentId+"/"+itemId;
	}

	public static String getConfigId(String sessionWideConfigId) {
		return "S"+sessionWideConfigId;
	}

	/**
	 * Returns the document id of a given config id, or null, if the config id is no legal document/item config id
	 */
	public static String getDocumentId(String configId) {
		if (configId.startsWith("I")) {
			int index = configId.indexOf("/");
			if (index == -1)
				return null; // no well-formed id => forget it
			else {
				String documentId = configId.substring(1,index); // remove first char and everything after "/"
				return documentId;
			}
		}
		else {
			// session wide config
			return null;
		}
	}


	public static String getItemId(String configId) {
		if (configId.startsWith("I")) {
			int index = configId.indexOf("/");
			if (index == -1)
				return null; // no well-formed id => forget it
			else {
				String itemId = configId.substring(index+1);
				return itemId;
			}
		}
		else
			return null; // session-wide config => no item
	}

}