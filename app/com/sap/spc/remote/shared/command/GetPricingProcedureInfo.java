/************************************************************************

	Copyright (c) 1999 by SAP AG

	All rights to both implementation and design are reserved.

	Use and copying of this software and preparation of derivative works
	based upon this software are not permitted.

	Distribution of this software is restricted. SAP does not make any
	warranty about the software, its performance or its conformity to any
	specification.

**************************************************************************/
package com.sap.spc.remote.shared.command;

/**
 * return basic information about a given pricing procedure
 *
 * <b>Import parameters</b>
 *
 * <ul>
 *		<li>procedureName (required):		the name of the pricing procedure
 *		<li>application (optional):			the name of the application. Default value is "CRM".
 *		<li>usagee (optional):				the name of the usage hosting the document (only usages derived from pricing are allowed)
 * </ul>
 *
 * <b>Export parameters</b>
 *
 * <ul>
 *     <li>conditionTypesWithDataSource:	an array of condition type names for which a dataSource not equal conditionTechnique is maintained.
 *     <li>dataSources:						the corresponding array of data source names
 *     <li>purposes:						the corresponding array of purposes
 *     <li>availableItemConditionTypeNames:	an array of the available item condition type names
 *     <li>availableHeaderConditionTypeNames:an array of the available header condition type names
 *     <li>headerAttributeNames:			an array of the header attribute names
 *     <li>itemAttributeNames:				an array of the item attribute names
 *     <li>conditionAccessTimestampNames:   an array of the condition access timestamp names
 * </ul>
 */

public interface GetPricingProcedureInfo {
	// input parameter
	public static final String PROCEDURE_NAME							= "procedureName";
	public static final String APPLICATION								= "application";
	public static final String USAGE									= "usage";

	// output parameter
	public static final String CONDITION_TYPE_NAMES_WITH_DATA_SOURCE	= "conditionTypesWithDataSource";
	public static final String DATA_SOURCES								= "dataSources";
	public static final String PURPOSES									= "purposes";
	public static final String AVAILABLE_ITEM_CONDITION_TYPE_NAMES		= "availableItemConditionTypeNames";
	public static final String AVAILABLE_HEADER_CONDITION_TYPE_NAMES	= "availableHeaderConditionTypeNames";
	public static final String HEADER_ATTRIBUTE_NAMES					= "headerAttributeNames";
	public static final String ITEM_ATTRIBUTE_NAMES						= "itemAttributeNames";
    public static final String CONDITION_ACCESS_TIMESTAMP_NAMES         = "conditionAccessTimestampNames";
}
