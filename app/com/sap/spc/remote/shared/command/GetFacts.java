/************************************************************************

	Copyright (c) 1999 by SAP AG

	All rights to both implementation and design are reserved.

	Use and copying of this software and preparation of derivative works
	based upon this software are not permitted.

	Distribution of this software is restricted. SAP does not make any
	warranty about the software, its performance or its conformity to any
	specification.

**************************************************************************/

package com.sap.spc.remote.shared.command;


/**
 * Returns all facts about an instance. <p>
 *
 * <b>Import parameters</b>
 *
 * <ul>
 *     <li>documentId	(required):	the document id of the item being configured
 *     <li>itemId		(required):	the item id of the item being configured
 *     <li>justId		(required):	the id of the justification we're interested in
 * </ul><p>
 *
 * <b>Export parameters</b>
 *
 * <ul>
 *     <li> factIds[]:				an array of ids for the facts that belong to the fact
 *     <li> factLNames[]:			an array of language dependent names of facts that belong to the fact
 *     <li> factIsUserCreateds[]:	is the fact owned by the uswer.
 * </ul>
 *
 * The command guarantees that all returned arrays are of equal length
 */
public interface GetFacts extends SCECommand
{
	public static final String JUST_ID		= "justId";

	public static final String FACT_ID				= "factIds";
	public static final String FACT_LNAME			= "factLNames";
	public static final String FACT_IS_USER_CREATED	= "factIsUserCreateds";

	public static final String FACT_KEY_BEGIN = "<FACT_KEY>";
	public static final String FACT_KEY_END = "</FACT_KEY>";
	public static final String FACT_KEY_DELIMITER = "|";
	public static final String FACT_KEY_PLACEHOLDER = "*";
	public static final int FACT_KEY_ID = 0;
	public static final int FACT_KEY_INST_ID = 1;
	public static final int FACT_KEY_OBSERV_TYPE = 2;
	public static final int FACT_KEY_OBSERVABLE = 3;
	public static final int FACT_KEY_BINDING = 4;
}