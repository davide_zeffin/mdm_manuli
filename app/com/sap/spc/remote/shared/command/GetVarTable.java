package com.sap.spc.remote.shared.command;

import com.sap.spc.remote.shared.command.SCECommand;
/**
 * Returns information about all the variant tables of a given item. Please note 
 * the presence of number of records field with the first Cstic of a table. This
 * attached info has to be detatched from the value on the client side, so as to 
 * read the table records.
 * <b>Import parameters</b>
 * <ul>
 *     <li>documentId (required):	the document id of the item being configured
 *     <li>itemId (required)	:	the item id of the item being configured
 *     <li>TabName (optional)	:	if specified, only the variant table with the given name, will be returned.
 *	   <li>NameCstics (optional):   if specified, only the table with composing of given cstics(i.e all or more) will be returned.
 *                                  If there are more than one such tables, all of them will be returned
 *									If both tbe TabName, and NameCstics are passed, then TabName takes the precedence and 
 * 									the NameCstics are ignored.
 * </ul><p>
 * <ul>
 *     <li>TabNames[]			:	the names of the tables belonging to the KB of the item 
 *     <li>CsticsNames[]		:	the names of the Cstics belonging to each of these tables, passed as name-value pairs
 * 									alongwith the table names.
 * 									Note: Always the first Cstic of each table will return the no. of records value attached
 * 									to it with a '.'. For e.g.a table has 3 Cstics, Cstic1,Cstic2,Cstic3. Then, the values returned 
 * 									would be "Cstic1.15,Cstic2,Cstic3". This would mean that table has 15 records and also, the
 * 									first value has to be decoded back into "Cstic1" as the value and 15 as no. of records.
 *     <li>CsticsValues[]		:	the values of the cstics in the table rows are returned here, in the same sequence
 * 									as the cstics themselves. To reconstruct the table, the values of Cstics have to read,
 * 									according to the no. of records value passed in above parameter, and then for each Cstic
 * 									that many values have to be picked up. So as in the example there are 3 Cstics, therefore the 
 * 									total no. of values is 15*3 = 45.
 * </ul>
 */
public interface GetVarTable extends SCECommand {
	public static final String DOCUMENTID = "documentId";
	public static final String ITEMID = "itemId";
	//public static final String INSTANCEID = "instanceId";	
	public static final String TABNAME = "TabName";
	public static final String NAMECSTICS = "NameCstics";
	//filter parameter for building a subset of values
	public static final String VARTABLEGROUP = "VarTableGroup";
	public static final String FILTERNAME = "FilterName";
	public static final String FILTERVALUE = "FilterValue";
	public static final String FILTERDEFAULT = "FilterDefault";	
	//exports, the table is passed in characteristics sequence as name value pairs.
	public static final String TABNAMES = "TabNames";
	public static final String CSTICSNAMES = "CsticsNames";
	public static final String CSTICSVALUES = "CsticsValues";
	
}


