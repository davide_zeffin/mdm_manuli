/************************************************************************

	Copyright (c) 2000 by SAP AG

	All rights to both implementation and design are reserved.

	Use and copying of this software and preparation of derivative works
	based upon this software are not permitted.

	Distribution of this software is restricted. SAP does not make any
	warranty about the software, its performance or its conformity to any
	specification.

**************************************************************************/
package com.sap.spc.remote.shared.command;

/**
 * Changes an existing (sales) document.  <p>
 *
 * <b>Import parameters</b>
 *
 * <ul>
 *      <li>documentId (required):				the id of the document
 *		<li>procedureName (optional):			the name of the pricing procedure
 *		<li>documentCurrencyUnit (optional):	the document currency unit
 *		<li>localCurrencyUnit (optional):		the local currency unit
 *      <li>salesOrganisation (optional):		the sales organization (for specifying the sales data of the products and for condition finding)
 *      <li>distributionChannel (optional):		the distribution channel (for specifying the sales data of the products and for condition finding)
 *		<li>editMode (optional):            	'A' for read-write, 'B' for read-only
 *		<li>headerAttributeNames (optional):	an array of header attribute names
 *		<li>headerAttributeValues (optional):	the corresponding array of header attribute values
 *		<li>expiringCheckDate (optional):		the date that will be used to check if a currency is expired
 *      <li>dataKeys (optional):				an array of keys for additional data
 *      <li>dataValues (optional):				the corresponding array of values for additional data
 *      <li>suppressPricing (optional):	        suppresses the pricing after the change of the document ("Y"/"N" toggle, default value is "N")
 *      <li>suppressRateDetermination (optional): suppresses the determination of the exchange rates ("Y"/"N" toggle, default value is "N")
 * </ul>
 */

public interface ChangeDocument {
	// input parameter
	public static final String DOCUMENT_ID								= "documentId";
	public static final String PROCEDURE_NAME							= "procedureName";
	public static final String DOCUMENT_CURRENCY_UNIT					= "documentCurrencyUnit";
	public static final String LOCAL_CURRENCY_UNIT						= "localCurrencyUnit";
	public static final String SALES_ORGANISATION						= "salesOrganisation";
	public static final String DISTRIBUTION_CHANNEL						= "distributionChannel";
	public static final String EDIT_MODE     						    = "editMode";
	public static final String HEADER_ATTRIBUTE_NAMES					= "headerAttributeNames";
	public static final String HEADER_ATTRIBUTE_VALUES					= "headerAttributeValues";
	public static final String EXPIRING_CHECK_DATE  					= "expiringCheckDate";
	public static final String ADDITIONAL_DATA_KEYS						= "dataKeys";
	public static final String ADDITIONAL_DATA_VALUES					= "dataValues";
	public static final String ITEM_IDS_FOR_REMOVAL						= "itemIdsForRemoval";
    public static final String SUPPRESS_PRICING                         = "suppressPricing";
    public static final String SUPPRESS_RATE_DETERMINATION              = "suppressRateDetermination";
    public static final String FREEGOODS_PROCEDURE                      = "freeGoodsProcedure";
	// output parameters
	public static final String OBJECT_IDS						    	= "objectIds";
	public static final String MESSAGES		    						= "messages";
	public static final String MESSAGE_TYPES		    				= "messageTypes";
}
