/************************************************************************

	Copyright (c) 1999 by SAP AG

	All rights to both implementation and design are reserved.

	Use and copying of this software and preparation of derivative works
	based upon this software are not permitted.

	Distribution of this software is restricted. SAP does not make any
	warranty about the software, its performance or its conformity to any
	specification.

**************************************************************************/
package com.sap.spc.remote.shared.command;

/**
 * Returns the pricing informations of a document without recomputing
 * the prices.  <p>
 *
 * <b>Import parameters</b>
 *
 * <ul>
 *     <li>documentId (required):	the id of the document we're interested in
 *     <li>formatValue (optional):	"Y"/"N" toggle: "Y": the values of the export parameters are converted to a string in the locale representation (default value is "Y")
 * </ul><p>
 *
 * <b>Export parameters</b>
 *
 * <ul>
 *     <li>grossValue:				the gross value of the document
 *	   <li>netValue:				the net value of the document
 *	   <li>taxValue:				the tax value of the document
 *     <li>subtotal1:				the subtotal1 value of the document
 *     <li>subtotal2:				the subtotal2 value of the document
 *     <li>subtotal3:				the subtotal3 value of the document
 *     <li>subtotal4:				the subtotal4 value of the document
 *     <li>subtotal5:				the subtotal5 value of the document
 *     <li>subtotal6:				the subtotal6 value of the document
 *     <li>freight: 				the freight value of the document
 *     <li>netValueWithoutFreight:	the net value without freight of the document
 *     <li>grossValueSubtotal:	    the gross values (derived from subtotal) of the document. Corresponds to the R/3 field BRTWR
 *     <li>rootItemIds[]:			the array of all root items in this document
 *     <li>itemIds[]:				the array of all items in this document (incl. subitems)
 *     <li>itemGrossValues[]:		the gross values of all items in this document
 *     <li>itemNetValues[]:			the net values of all items in this document
 *     <li>itemTaxValues[]:			the tax values of all items in this document
 *     <li>itemTotalGrossValues[]:	the total gross value of all items in this document
 *     <li>itemTotalNetValues[]:	the total net value of the all items in this document
 *     <li>itemTotalTaxValues[]:	the total tax value of the all items in this document
 *     <li>itemSubtotals1[]:		the subtotal1 values of all items in this document
 *     <li>itemSubtotals2[]:		the subtotal2 values of all items in this document
 *     <li>itemSubtotals3[]:		the subtotal3 values of all items in this document
 *     <li>itemSubtotals4[]:		the subtotal4 values of all items in this document
 *     <li>itemSubtotals5[]:		the subtotal5 values of all items in this document
 *     <li>itemSubtotals6[]:		the subtotal6 values of all items in this document
 *     <li>freights[]:				the accumulated freight values of all items in this document
 *     <li>netValuesWithoutFreight[]:	the accumulated net values without freight of all items in this document
 *     <li>grossValuesSubtotal[]:	the gross values (derived from subtotal) of all items in this document. Corresponds to the R/3 field BRTWR
 *     <li>currencyUnit:			the (document) currency unit
 *     <li>itemNetPrices[]:		    the array of accumulated net prices of all items in this document (NETPR)
 *     <li>itemNetPricePricingUnitValues[]: the array of values of the net price's pricing unit of all items in this document
 *     <li>itemNetPricePricingUnitNames[]:  the array of names of the net price's pricing unit of all items in this document
 *     <li>itemCashDiscounts[]:		the array of accumulated cash discounts of all items in this document (SKONTO)
 *     <li>itemCashDiscountBases[]:	the array of accumulated cash discount bases of all items in this document (SKFDF)
 *     <li>statisticalValues[]:		the statistical values for foreign trade of all items in this document
 *     <li>itemPurposeNames[]:		the array of purpose field names (coming from the customizing of the condition types) of all items in this document.
 *     <li>itemPurposeValues[]:		the corresponding array of accumulated values
 *     <li>itemPurposeRef[]:		the corresponding array of item references
 * 	<li>itemDynamicReturnNames[]:	the array of dynamic return field names of all items in this document
 * 	<li>itemDynamicReturnValues[]:	the corresponding array of dynamic return values
 * 	<li>itemDynamicReturnRefs[]:		the corresponding array of item references
 * </ul><p>
 *
 * The command guarantees that all arrays (except rootItemIds) are of equal length.
 */

public interface GetPricingDocumentInfo {

	// input parameter
	public static final String DOCUMENT_ID								= "documentId";
	public static final String FORMAT_VALUE								= "formatValue";

	// output parameter
	public static final String GROSS_VALUE								= "grossValue";
	public static final String NET_VALUE								= "netValue";
	public static final String TAX_VALUE								= "taxValue";
	public static final String SUBTOTAL_1                               = "subtotal1";
	public static final String SUBTOTAL_2                               = "subtotal2";
	public static final String SUBTOTAL_3                               = "subtotal3";
	public static final String SUBTOTAL_4                               = "subtotal4";
	public static final String SUBTOTAL_5                               = "subtotal5";
	public static final String SUBTOTAL_6                               = "subtotal6";
	public static final String FREIGHT									= "freight";
	public static final String NET_VALUE_WITHOUT_FREIGHT				= "netValueWithoutFreight";
	public static final String GROSS_VALUE_DERIVED_FROM_SUBTOTAL		= "grossValueSubtotal";
	public static final String ROOT_ITEM_IDS							= "rootItemIds";
	public static final String ITEM_IDS									= "itemIds";
	public static final String ITEM_GROSS_VALUES						= "itemGrossValues";
	public static final String ITEM_NET_VALUES							= "itemNetValues";
	public static final String ITEM_TAX_VALUES							= "itemTaxValues";
	public static final String ITEM_TOTAL_GROSS_VALUES					= "itemTotalGrossValues";
	public static final String ITEM_TOTAL_NET_VALUES					= "itemTotalNetValues";
	public static final String ITEM_TOTAL_TAX_VALUES					= "itemTotalTaxValues";
	public static final String ITEM_SUBTOTALS_1							= "itemSubtotals1";
	public static final String ITEM_SUBTOTALS_2							= "itemSubtotals2";
	public static final String ITEM_SUBTOTALS_3							= "itemSubtotals3";
	public static final String ITEM_SUBTOTALS_4							= "itemSubtotals4";
	public static final String ITEM_SUBTOTALS_5							= "itemSubtotals5";
	public static final String ITEM_SUBTOTALS_6							= "itemSubtotals6";
	public static final String FREIGHTS									= "freights";
	public static final String NET_VALUES_WITHOUT_FREIGHT				= "netValuesWithoutFreight";
	public static final String GROSS_VALUES_DERIVED_FROM_SUBTOTAL		= "grossValuesSubtotal";
	public static final String CURRENCY_UNIT							= "currencyUnit";
	public static final String ITEM_NET_PRICES                          = "itemNetPrices";
	public static final String ITEM_NET_PRICE_PRICING_UNIT_VALUES       = "itemNetPricePricingUnitValues";
	public static final String ITEM_NET_PRICE_PRICING_UNIT_NAMES        = "itemNetPricePricingUnitNames";
	public static final String ITEM_CASH_DISCOUNTS                      = "itemCashDiscounts";
	public static final String ITEM_CASH_DISCOUNT_BASES                 = "itemCashDiscountBases";
	public static final String STATISTICAL_VALUES						= "statisticalValues";
	public static final String ITEM_PURPOSE_NAMES                       = "itemPurposeNames";
	public static final String ITEM_PURPOSE_VALUES                      = "itemPurposeValues";
	public static final String ITEM_PURPOSE_REF                         = "itemPurposeRef";
	public static final String ITEM_DYNAMIC_RETURN_NAMES		= "itemDynamicReturnNames";
	public static final String ITEM_DYNAMIC_RETURN_VALUES		= "itemDynamicReturnValues";
	public static final String ITEM_DYNAMIC_RETURN_REFS			= "itemDynamicReturnRefs";

	//For swing GUI---------------------------start
	public static final String IS_SWINGGUI						        = "isSwingGUI";
	public static final String NET_UNIT_NAME							= "netUnitName";
	public static final String NET_UNIT_DESC							= "netUnitDesc";
	public static final String TAX_UNIT_NAME							= "taxUnitName";
	public static final String TAX_UNIT_DESC							= "taxUnitDesc";
	public static final String DOC_CURRENCY_UNIT_NAME					= "docCurrencyUnitName";
	public static final String DOC_CURRENCY_UNIT_DESC					= "docCurrencyUnitDesc";
	public static final String LOCAL_CURRENCY_UNIT_NAME					= "localCurrencyUnitName";
	public static final String LOCAL_CURRENCY_UNIT_DESC					= "localCurrencyUnitDesc";
	public static final String PRICING_PROCEDURE_NAME					= "pricingProcedureName";
	public static final String PRICING_PROCEDURE_DESC					= "pricingProcedureDesc";
	public static final String ALWAYS_PERFORM_GROUPCONDITION_PROCESSING = "alwaysPerformGroupConditionProcessing";
	//For swing GUI---------------------------end.


}
