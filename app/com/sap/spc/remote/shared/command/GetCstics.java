/************************************************************************

    Copyright (c) 1999 by SAP AG

    All rights to both implementation and design are reserved.

    Use and copying of this software and preparation of derivative works
    based upon this software are not permitted.

    Distribution of this software is restricted. SAP does not make any
    warranty about the software, its performance or its conformity to any
    specification.

**************************************************************************/

package com.sap.spc.remote.shared.command;


/**
 * Returns information about all characteristics of a given instance. Please note while most of
 * these data look like they are master data, most of them may actually change during configuration
 * via dependency knowledge (visibility, being read-only, is domain an interval).  <p>
 *
 * <b>Import parameters</b>
 *
 * <ul>
 *     <li>documentId (required):	the document id of the item being configured
 *     <li>itemId (required):		the item id of the item being configured
 *     <li>instId (required):		the instance id of the instance we're interested in
 *	   <li>filterGroups (optional): if specified, only the cstics of the given groups will be returned.
 *                                  If a group is not specified, one single dummy cstic will be returned
 *									in this group (to get group data at all). The dummy cstic has the
 *									following values (see below for order): DUMMY,DUMMY,N,N,N,<group>,<null>,Y,N,U,N,Y.
 *		<li>visibleCsticsOnly
 *						(optional):	if specified, only visible cstics and invisible cstics with
 *									with a conflict will be shown
 *		<li>withoutDescription
 *						(optional):	if specified, no description will given back
 *      <li>withoutGroupNo
 *                      (optional): if specified, no group number will be returned
 *                                  (Default is "Y" - which means no group numbers)
 *      <li>ungroupedCsticsOnly
 *                      (optional): if specified, only those cstics are returned that
 *                                  are not assigned to any group.
 *                                  (Default is "N")
 *      <li>includeUngroupedCstics
 *                      (optional): if specified, no ungrouped cstics will be returned
 *                                  (Default is "Y")
 * </ul><p>
 *
 * <b>Export parameters</b>
 *
 * <ul>
 *     <li>	csticNames[]:				the names of the cstics
 *     <li>	csticLNames[]:				the language independent names
 *	   <li> csticMultis[]:				"Y"/"N" toggle: is multivalued
 *     <li>	csticAddits[]:				"Y"/"N" toggle: additional values allowed
 *     <li>	csticDomainConstraineds[]:	"Y"/"N" toggle: is the domain of the cstic constrained
 *	   <li>	csticDomainIsAnIntervals[]:	"Y"/"N" toggle: if the domain is a Sequence of intervals
 *     <li>	csticGroups[]:				group identifiers for all cstics
 *     <li>	csticGroupNos[]:			group number of this cstic's group
 *     <li>	csticUnits[]:				units of measurement of the cstics
 *     <li>	csticReadOnlys[]:			"Y"/"N": is the cstic read only
 *     <li>	csticVisibles[]:			...: visible
 *     <li>	csticAuthors[]:				the author of the cstic "U" (User) / "S" (System)
 *     <li>	csticRequireds[]:			"Y"/"N": is the cstic required
 *     <li>	csticConsistents[]:			"Y"/"N": is the cstic consistent with the rest of the Config
 *     <li> csticEntryFieldMask[]:      cstic value's entry-field-mask
 *     <li> csticTypeLength[]:          length of cstic without separators or leading "-"
 *     <li> csticNumberScale[]:         number scale of the value
 *     <li> csticValueType[]:           basic type of cstic value

 *	   <li> csticDescriptions[]:		the long text for the cstics
 * </ul>
 *
 * The command guarantees that all returned arrays are of equal length
 */

public interface GetCstics extends SCECommand
{
    public static final String INST_ID					= "instId";
    public static final String FILTER_GROUPS		    = "filterGroups";
    public static final String VISIBLE_CSTICS_ONLY		= "visibleCsticsOnly";
    public static final String WITHOUT_DESCRIPTION     	= "withoutDescription";
    public static final String WITHOUT_GROUP_NO         = "withoutGroupNo";
    public static final String UNGROUPED_CSTICS_ONLY    = "ungroupedCsticsOnly";
    public static final String INCLUDE_UNGROUPED_CSTICS = "includeUngroupedCstics";
    public static final String CSTIC_NAMES				= "csticNames";
    public static final String CSTIC_LNAME				= "csticLNames";
    public static final String CSTIC_MULTI				= "csticMultis";
    public static final String CSTIC_ADDIT				= "csticAddits";
    public static final String CSTIC_GROUP				= "csticGroups";
    public static final String CSTIC_GROUP_NO			= "csticGroupNos";
    public static final String CSTIC_UNIT				= "csticUnits";
    public static final String CSTIC_READONLY			= "csticReadOnlys";
    public static final String CSTIC_VISIBLE			= "csticVisibles";
    public static final String CSTIC_AUTHOR				= "csticAuthors";
    public static final String CSTIC_REQUIRED			= "csticRequireds";
    public static final String CSTIC_CONSISTENT			= "csticConsistents";
    public static final String CSTIC_CONSTRAINED		= "csticDomainConstraineds";
    public static final String CSTIC_DOMAIN_IS_INTERVAL	= "csticDomainIsAnIntervals";
    public static final String CSTIC_DESCRIPTION		= "csticDescriptions";
    public static final String CSTIC_ENTRY_FIELD_MASK   = "csticEntryFieldMask";
    public static final String CSTIC_TYPELENGTH         = "csticTypeLength";
    public static final String CSTIC_NUMBERSCALE        = "csticNumberScale";
    public static final String CSTIC_VALUETYPE          = "csticValueType";
}