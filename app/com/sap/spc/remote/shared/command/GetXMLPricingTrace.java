/************************************************************************

	Copyright (c) 1999 by SAP AG

	All rights to both implementation and design are reserved.

	Use and copying of this software and preparation of derivative works
	based upon this software are not permitted.

	Distribution of this software is restricted. SAP does not make any
	warranty about the software, its performance or its conformity to any
	specification.

**************************************************************************/
package com.sap.spc.remote.shared.command;

/**
 * Returns an XML file that contains the pricing trace.  <p>
 *
 * <b>Import parameters</b>
 * 
 * <ul>
 *     <li>documentId (required):	the document id of the item for which the trace is required
 *     <li>itemId (required):		the item id of the item for which the trace is required
 *     <li>asTable (optional):		output is requested as table (rather than a string)				
 * </ul><p>
 *
 * <b>Export parameters</b>
 *
 * <ul>
 *     <li>xmlString:	the XML file.
 * </ul>
 *
 */
public interface GetXMLPricingTrace {
	// input parameter
	public static final String DOCUMENT_ID = "documentId";
	public static final String ITEM_ID = "itemId";
	public static final String AS_TABLE = "asTable";
	
	// output parameter
	public static final String XML_STRING  = "xmlString";
	public static final String XML_TRACE_LINE = "xmlTraceLine";
}
