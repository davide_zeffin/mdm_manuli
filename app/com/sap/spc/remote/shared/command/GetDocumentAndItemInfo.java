/************************************************************************

	Copyright (c) 1999 by SAP AG

	All rights to both implementation and design are reserved.

	Use and copying of this software and preparation of derivative works
	based upon this software are not permitted.

	Distribution of this software is restricted. SAP does not make any
	warranty about the software, its performance or its conformity to any
	specification.

**************************************************************************/
package com.sap.spc.remote.shared.command;

/**
 * Returns basic information about a given document and all of its items.
 *
 * <b>Import parameters</b>
 *
 * <ul>
 *     <li>documentId (required):	the document id to return info about
 *	   <li>filterItemId (optional): if you pass this id, and the id belongs to an item in this document, then only information about this item is returned.
 *     <li>formatValue (optional):	"Y"/"N" toggle: "Y": the values of the export parameters are converted to a string in the locale representation (default value is "Y")
 * </ul><p>
 *
 * <b>Export parameters</b>
 *
 * <ul>
 *     <li>grossValue:				the gross value of the document
 *     <li>netValue:				the net value of the document
 *     <li>taxValue:                the tax value of the document
 *     <li>currencyUnit:			the (document) currency unit
 *     <li>itemIds[]:				the item ids of all items in the document (or the one filtered) (incl. all subitems)
 *     <li>itemParentIds[]:			the item ids of all parents (null means root)
 *     <li>itemProductIds[]:		the product id of each (or the one filtered) item (external product id)
 *     <li>itemProductGuids[]:		the product guid of each (or the one filtered)  item (internal product id)
 *     <li>itemProductDescriptions[]:the product description of each (...) item (in the document language)
 *     <li>itemQuantityValues[]:    the quantity value of each (...) item
 *     <li>itemQuantityUnits[]:     the quantity unit of each item
 *     <li>itemGrossValues[]:		the gross value of each item
 *     <li>itemNetValues[]:			the net value of each item
 *     <li>itemTaxValues[]:			the tax value of each item
 *     <li>itemTotalGrossValues[]:  the total gross value of each item
 *     <li>itemTotalNetValues[]:    the total net value of each item
 *     <li>itemTotalTaxValues[]:	the total tax value of each item
 *     <li>itemIsConfigurables[]:	"Y"/"N" toggle: is the item configurable
 *     <li>itemIsProductVariants[]: "Y"/"N" toggle: is the item a product variant (implies isConfigurable)
 *     <li>itemConfigChangeds[]:	"Y"/"N" toggle: has the item's config been edited already
 *     <li>itemConfigCompletes[]:   "Y"/"N" toggle: is the item's config complete
 *     <li>itemConfigConsistents[]: "Y"/"N" toggle: is the item's config consistent
 * </ul><p>
 *
 * The command guarantees that all arrays are of equal length.<p>
 * 
 * Please note that the array parameters hold information about either all items, or, if filterItemId holds the id of a valid item in this document, only this one item. 
 *
 */

public interface GetDocumentAndItemInfo {
	// input parameters
	public static final String DOCUMENT_ID				= "documentId";

	public static final String FILTER_ITEM_ID			= "filterItemId";
	public static final String FORMAT_VALUE				= "formatValue";

	// output parameters
	public static final String GROSS_VALUE				= "grossValue";
	public static final String NET_VALUE				= "netValue";
	public static final String TAX_VALUE				= "taxValue";
    public static final String FREIGHT				= "freight";
	public static final String CURRENCY_UNIT			= "currencyUnit";
	public static final String ITEM_ID					= "itemIds";
	public static final String ITEM_PARENT_ID			= "itemParentIds";
	public static final String ITEM_PRODUCT_ID			= "itemProductIds";
	public static final String ITEM_PRODUCT_GUID		= "itemProductGuids";
	public static final String ITEM_PRODUCT_DESCRIPTION = "itemProductDescriptions";
	public static final String ITEM_QUANTITY_VALUE		= "itemQuantityValues";
	public static final String ITEM_QUANTITY_UNIT		= "itemQuantityUnits";
	public static final String ITEM_GROSS_VALUE			= "itemGrossValues";
	public static final String ITEM_NET_VALUE			= "itemNetValues";
	public static final String ITEM_TAX_VALUE			= "itemTaxValues";
	public static final String ITEM_TOTAL_GROSS_VALUE	= "itemTotalGrossValues";
	public static final String ITEM_TOTAL_NET_VALUE		= "itemTotalNetValues";
	public static final String ITEM_TOTAL_TAX_VALUE		= "itemTotalTaxValues";
	public static final String ITEM_IS_CONFIGURABLE		= "itemIsConfigurables";
	public static final String ITEM_IS_PRODUCT_VARIANT  = "itemIsProductVariants";
	public static final String ITEM_CONFIG_CHANGED		= "itemConfigChangeds";
	public static final String ITEM_CONFIG_COMPLETE		= "itemConfigCompletes";
	public static final String ITEM_CONFIG_CONSISTENT	= "itemConfigConsistents";
}
