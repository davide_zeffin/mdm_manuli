/************************************************************************

	Copyright (c) 1999 by SAP AG

	All rights to both implementation and design are reserved.

	Use and copying of this software and preparation of derivative works
	based upon this software are not permitted.

	Distribution of this software is restricted. SAP does not make any
	warranty about the software, its performance or its conformity to any
	specification.

**************************************************************************/

package com.sap.spc.remote.shared.command;

/**
 * Copies an item from one document to another (or create a copy of an
 * item in a document).  If the item is configurable, the configuration
 * is copied with the item. Optionally, the config of the original item
 * can be reverted to a new configuration.<p>
 * 
 * <b>Import parameters</b>
 * 
 * <ul>
 *     <li>sourceDocumentId (required):	the id of the document that holds the item to be copied
 *     <li>sourceItemId (required):		the id of the item to be copied
 *     <li>targetDocumentId (required):	the id of the document that will hold the copy of the item.
 *     <li>revertConfig (optional):     "Y"/"N" toggle: revert the configuration of the source item to an empty one.
 *                                      If this parameter is omitted, the default "N" is used.
 * </ul><p>
 * 
 * <b>Export parameters</b>
 * 
 * <ul>
 *     <li>targetItemId:				the item id of the new item.
 * </ul>
 */

public interface CopyItem {
	// input parameters
	public static final String SOURCE_DOCUMENT_ID = "sourceDocumentId";
	public static final String TARGET_DOCUMENT_ID = "targetDocumentId";
	public static final String SOURCE_ITEM_ID     = "sourceItemId";
	public static final String REVERT_CONFIG      = "revertConfig";
	
	// output parameters
	public static final String TARGET_ITEM_ID	  = "targetItemId";
}
