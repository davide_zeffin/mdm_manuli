/************************************************************************

	Copyright (c) 1999 by SAP AG

	All rights to both implementation and design are reserved.

	Use and copying of this software and preparation of derivative works
	based upon this software are not permitted.

	Distribution of this software is restricted. SAP does not make any
	warranty about the software, its performance or its conformity to any
	specification.

**************************************************************************/

package com.sap.spc.remote.shared.command;


/**
 * Returns the ids of all configurations that were directly added to the session with CreateConfig. <p>
 *
 * <b>Import parameters</b><p>
 * none.<p>
 *
 *
 * <b>Export parameters</b>
 * <ul>
 *      configIds
 * </ul><p>
 */

public interface GetConfigs extends SCECommand
{
	public static final String CONFIG_IDS = "configIds";
}