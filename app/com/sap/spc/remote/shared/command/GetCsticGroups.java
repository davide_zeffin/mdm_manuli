/************************************************************************

	Copyright (c) 1999 by SAP AG

	All rights to both implementation and design are reserved.

	Use and copying of this software and preparation of derivative works
	based upon this software are not permitted.

	Distribution of this software is restricted. SAP does not make any
	warranty about the software, its performance or its conformity to any
	specification.

**************************************************************************/
package com.sap.spc.remote.shared.command;

/**
 * Returns information about all characteristic groups of a given instance.
 * <p>
 * <b>Import parameters</b>
 *
 * <ul>
 *     <li>documentId (required):	the document id of the item being configured
 *     <li>itemId (required):		the item id of the item being configured
 *     <li>instId (required):		the instance id of the instance we're interested in
 * </ul>
 * </p>
 * </p>
 * <b>Export parameters</b>
 *
 * <ul>
 *     <li>	csticGroups[]:				the group names. Note: groups are returned in the correct order
 * </ul>
 *
 * The command guarantees that all returned arrays are of equal length
 */
public interface GetCsticGroups extends SCECommand
{
	static final String INST_ID          = "instId";
    static final String CSTIC_GROUP      = "csticGroup";
    static final String GROUP_CONSISTENT = "gc";
    static final String GROUP_REQUIRED   = "gr";
}
