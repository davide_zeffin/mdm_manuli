/************************************************************************

	Copyright (c) 1999 by SAP AG

	All rights to both implementation and design are reserved.

	Use and copying of this software and preparation of derivative works
	based upon this software are not permitted.

	Distribution of this software is restricted. SAP does not make any
	warranty about the software, its performance or its conformity to any
	specification.

**************************************************************************/
package com.sap.spc.remote.shared.command;

/**
 * Loads the pricing document of a given (sales) document from the database.<p>
 *
 * <b>Import parameters</b>
 *
 * <ul>
 *     <li>documentId (required):	the id of the (sales) document we're interested in
 * </ul>
 * <b>Export parameters</b>
 *
 * <ul>
 *     <li>objectIds:							an array of item ids (the initial guid 00000000000000000000000000000000 indicates protocol lines deriving from the document)
 *     <li>messages:							an array of message texts
 *     <li>messageTypes:						an array of message types (A: Abend, E: Error, I: Information, S: Success, W: Warning)
 * </ul>
 * 
 */
public interface LoadPricingDocument extends ImportPricingConditions
{
	// input parameter
	public static final String DOCUMENT_ID								= "documentId";
	// output parameters
	public static final String OBJECT_IDS						    	= "objectIds";
	public static final String MESSAGES		    						= "messages";
	public static final String MESSAGE_TYPES		    					= "messageTypes";	
}
