/*

Copyright (c) 2005 by SAP AG

All rights to both implementation and design are reserved.

Use and copying of this software and preparation of derivative works based
upon this software are not permitted.

Distribution of this software is restricted. SAP does not make any warranty
about the software, its performance or its conformity to any specification.

*/

package com.sap.spc.remote.shared.command;

/**
 * Updates the item structure of an item from a configuration and prices the
 * resulting subitem tree of this item.  <p>
 * 
 * <b>Import parameters</b>
 * 
 * <ul>
 *     <li>documentId (required): the document id of the item being priced
 *     <li>itemIds (required): the item ids of the items being priced
 *     <li>pricingAnalysis (optional): Y/N toggle: Y: pricing analysis is performed
 *     <li>pricingType (optional): pricing type according to which the conditions 
 *     will be redetermined; if not provided or space, then no redetermination, 
 *     just a new calculation will be performed
 *                                 
 * </ul><p>
 * 
 * <b>Export parameters</b>
 * 
 */
public interface PricingItems {
    // input parameters
    public static final String DOCUMENT_ID = "documentId";
    public static final String ITEM_IDS = "itemIds";
    public static final String PERFORM_PRICING_ANALYSIS = "pricingAnalysis";
    public static final String PRICING_TYPE = "pricingType";
}
