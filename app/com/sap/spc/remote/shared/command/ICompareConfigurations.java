package com.sap.spc.remote.shared.command;
/**
 * Calculates the difference between two configurations. <p>
 *
 * <b>Import parameters</b>
 *    <li> Array of external configurations containing the 
 *         two configurations to be compared.<p> The external
 *         configuration format is documented in the export 
 *         parameter for {@link GetConfigItemInfo}</li>
 * <ul>
 *     
 * </ul><p>
 * 
 * <b>Export parameters</b>
 * The export parameters are stored in 3 tables
 *  <ul>
 *    <li>The field delta table</li>
 *    <li>The instance delta table</li>
 *    <li>The value delta table</li>
 *  <ul>
 * The <b>field delta table</b> contains the header deltas and the instance field deltas
 * It has the following attributes:
 *  	<ul>
 *    	<li>INST_FLD_REF_INDEX: Foreign key to instance delta table(primary field DELTA_INST_IDX),
 *            If the value for this field contains the value "H", the row contains a header delta</li>
 *    	<li>FIELD_NAME: The changed fields name</li>
 *    	<li>VALUE_1: Original value</li>
 *    	<li>VALUE_2: Changed value</li>
 *		</ul><p>  
 * The <b>instance delta table</b> contains the instance deltas and refers to the <b>field delta table</b> 
 * and <b>value delta table</b><p>
 * It has the following attributes:<p>
 *  	<ul>
 *    	<li>INST_FLD_REF_INDEX: Foreign key to instance delta table(primary field DELTA_INST_IDX),
 *            If the value for this field contains the value "H", the row contains a header delta</li>
 *    	<li>INST_ID_1: The changed instance 1d</li>
 *    	<li>INST_ID_2: Original instance id</li>
 *      <li>INST_AUTHOR: Author of the instance delta</li>
 *    	<li>DECOMP_PATH: {@link com.sap.spc.remote.client.object.InstanceDelta#getDecompPath}</li>
 *    	<li>CHANGE_FLAG: {@link com.sap.spc.remote.client.object.InstanceDelta#getType}</li>
 *		</ul><p>  
 *
 */

public interface ICompareConfigurations extends ExtConfigConstants{
	
//	export
	
	 // COMT_CFGM_DELTA
	 public static final String FIELD_NAME   	= "FIELD_NAME";
	 public static final String VALUE_1 	        = "VALUE_1";
	 public static final String VALUE_2    	    = "VALUE_2";
	 public static final String INST_FLD_REF_INDEX   = "INST_FLD_REF_INDEX";
	public static final String INST_VAL_REF_INDEX   = "INST_VAL_REF_INDEX";
	
	 // COMT_CFGM_INSTANCE_DELTA
 	 public static final String DELTA_INST_IDX   	    = "DELTA_INST_IDX";
	 public static final String INST_ID_1   	    = "INST_ID_1";
	 public static final String INST_ID_2 	    = "INST_ID_2";
	 public static final String DECOMP_PATH 	    = "DECOMP_PATH";
	 public static final String INST_DELTAS 	    = "INST_DELTAS";
	 public static final String PART_OF_DELTAS   = "PART_OF_DELTAS";
	 public static final String VALUE_DELTAS     = "VALUE_DELTAS";
	 public static final String AUTHOR           = "AUTHOR";
	 public static final String INST_AUTHOR      = "INST_AUTHOR";
	public static final String VALUE_AUTHOR      = "VALUE_AUTHOR";
	 public static final String CHANGE_FLAG      = "CHANGE_FLAG";
	public static final String VALUE_CHANGE_FLAG      = "VALUE_CHANGE_FLAG";
	
	 // COMT_CFGM_VALUE_DELTA
	 public static final String CHARC         	= "CHARC";
	 public static final String VALUE 	        = "VALUE";
	 // + author, change_flag


	public static final String KB_LANGUAGE      = "language";
	public static final String KB_DATE          = "kbDate";

	public static final String CONFIG_ID        = "configId";

	public static final String EXT_CONFIG_INDEX       = "extConfigIndex";
	public static final String DELTA_HEADER_PREFIX = "H";
	

}
