/************************************************************************

	Copyright (c) 1999 by SAP AG

	All rights to both implementation and design are reserved.

	Use and copying of this software and preparation of derivative works
	based upon this software are not permitted.

	Distribution of this software is restricted. SAP does not make any
	warranty about the software, its performance or its conformity to any
	specification.

**************************************************************************/

package com.sap.spc.remote.shared.command;



/**
 * Creates a new configuration. The parameters kbDate, kbLogSys, kbName, kbVersion, kbProfile, productId are all
 * used to determine the correct knowledge base and profile. Either productId + kbDate or kbName + kbVersion
 * need to be specified.<p>
 *
 * <b>Import parameters</b>
 *
 * <ul>
 *      <li>productId (optional):    the configuration should configure the given product.
 *      <li>productType (optional):  the product type of the product.
 *      <li>language (optional):     the language in which language dependent data of this configuration should be
 *                                   displayed. The default language is English.
 *      <li>kbLogSys (optional):     the logical system in which the knowledge base has been maintained.
 *      <li>kbDate (optional):       target date. Find a configuration that is valid at that date.
 *      <li>kbName (optional):       the knowledge base name to use
 *      <li>kbVersion (optional):    the knowledge base version to use
 *      <li>kbProfile (optional):    the knowledge base profile to use
 *      <li>contextNames (optional): array of names for the context table (used by reference characteristics)
 *      <li>contextValues(optional): array of values for the context table. contextNames and values must both
 *          either exist or not. If they do, they must be equally long.
 *      <li>external configuration data (optional): an external configuration that initializes the config.
 * </ul>
 *
 * <b>Export parameters</b>
 *
 * <ul>
 *     <li>configId:                a string identifying the new configuration, or null, if the
 *                                  configuration could not be created.
 * </ul>
 * <p>
 * This command supports client-specific parameter naming for all input parameters.
 */
public interface CreateConfig extends ExtConfigConstants
{
	public static final String PRODUCT_ID       = "productId";
	public static final String PRODUCT_TYPE     = "productType";
	public static final String KB_LANGUAGE      = "language";
	public static final String KB_DATE          = "kbDate";
	public static final String KB_LOG_SYS       = "kbLogSys";
	public static final String KB_NAME          = "kbName";
	public static final String KB_VERSION       = "kbVersion";
	public static final String KB_PROFILE       = "kbProfile";
	public static final String KB_ID            = "kbId";
	public static final String CONTEXT_NAMES    = "contextNames";
	public static final String CONTEXT_VALUES   = "contextValues";

	public static final String CONFIG_ID        = "configId";
}
