/*
 * Created on Sep 22, 2004
 *
 * To change the template for this generated file go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
package com.sap.spc.remote.shared.command;

/**
* Loads the pricing document of a given (sales) document from the database xml string.<p>
*
* <b>Import parameters</b>
*
* <ul>
*     <li>documentId (required):	the id of the (sales) document we're interested in
* </ul>
* <b>Export parameters</b>
*
* 	<ul> No export parameters
* 	</ul>
*/

/* TODO: If required use later
*  <ul>
*     <li>objectIds:						an array of item ids (the initial guid 00000000000000000000000000000000 indicates protocol lines deriving from the document)
*     <li>messages:							an array of message texts
*     <li>messageTypes:						an array of message types (A: Abend, E: Error, I: Information, S: Success, W: Warning)
* </ul>
*/

public interface LoadPricingDocumentFromDbXML extends ImportPricingConditions
{
// input parameter
public static final String DOCUMENT_ID								= "documentId";
// output parameters
//public static final String OBJECT_IDS						    	= "objectIds";
//public static final String MESSAGES		    						= "messages";
//public static final String MESSAGE_TYPES		    				= "messageTypes";	
}
