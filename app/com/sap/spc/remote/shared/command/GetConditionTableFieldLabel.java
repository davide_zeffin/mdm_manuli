/*******************************************************************************

    Copyright (c) 2005 by SAP AG

    All rights to both implementation and design are reserved.

    Use and copying of this software and preparation of derivative works based
    upon this software are not permitted.

    Distribution of this software is restricted. SAP does not make any warranty
    about the software, its performance or its conformity to any specification.

*******************************************************************************/
package com.sap.spc.remote.shared.command;

/**
 * @author d043319
 */
/**
 * Returns the 4 text DDIC label for a given condition field catalogue
 * field or dataelement name.<p>
 * The fieldname is optional, if only usage & application is given
 * the command returns all fields from the fieldcatalogues for the this task.<p>
 * ( depending on the IsApplicationField flag, all fields will be returned on
 *   application ( flag is true ) or usage ( flag is flase ) base )
 * If fieldnames are given, only for thos fields the texts are returned
 * If dataelement names are given, only the texts for the data elements are returned
 * Fieldnames and datalement names can not be used together !!!
 * It's not supported to use both parameters together 
 * 
 * <b>Import parameters</b>
 * <ul>
 *     <li>usage               :	usage related to the condition technique
 *     <li>application         :	application related to the condition technique 
 *     <li>isAppplicationField :	Flag to switch between application or usage field   
 *     <li>fieldname(s)        :    fieldnames from the cond.tech. field catalogue ( optional )
 * <br>
 *     <li>dataelementname(s) : if this parameter is filled, all other input parameters are ignored !!!
 *                              Name(s) of dataelements for which text labels should be returned
 *  * </ul><p>
 * 
 * <b>Export parameters</b>
 * <ul>
 *     <li>name:					the names ( fieldname or dataelement name, depending on input parameter )
 *     <li>extra short text:		the DDIC extra short text
 *     <li>short text:				the DDIC short text
 *     <li>medium text:				the DDIC medium text
 *     <li>long text:				the DDIC long text   
 * </ul>
 */

public interface GetConditionTableFieldLabel {
	
	public static String USAGE            = "usage";
	public static String APPLICATION      = "application";
	public static String FIELDNAME        = "fieldname";	
	public static String DATAELEMENTNAME  = "dataelementname";
	public static String IS_APPLICATION_FIELD = "isApplicationField";
	public static String IS_APP_FIELD_TRUE = "Y";
	public static String IS_APP_FIELD_FALSE= "N";	

	public static String EXTRA_SHORT_TEXT = "xshorttext";
	public static String SHORT_TEXT       = "shorttext";
	public static String MEDIUM_TEXT      = "mediumtext";
	public static String LONG_TEXT        = "longtext";	

}
