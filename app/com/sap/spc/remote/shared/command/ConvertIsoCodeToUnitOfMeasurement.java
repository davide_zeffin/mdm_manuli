/*******************************************************************************

	Copyright (c) 2001 by SAP AG

	All rights to both implementation and design are reserved.

	Use and copying of this software and preparation of derivative works based
	upon this software are not permitted.

	Distribution of this software is restricted. SAP does not make any warranty
	about the software, its performance or its conformity to any specification.

*******************************************************************************/
package com.sap.spc.remote.shared.command;

/**
 * Returns the internal and the external units of measurement or currency units 
 * for the given ISO codes.<p>
 *
 * <b>Import parameters</b>
 *
 * <ul>
 *		<li>isoCodes (required):   				the array of ISO codes for the units of measurement or the currencies
 *		<li>values (optional):   				the array of values to be formatted
 *      (The representation of the values consists of an optional sign ('+' or '-') followed by a sequence of zero or more decimal digits ("the integer"),
 *       optionally followed by a fraction. The fraction consists of of a decimal point followed by zero or more decimal digits.
 *       The value must contain at least one digit in either the integer or the fraction.)
 *		<li>language (optional):   				the language (ISO code, two places)
 *		<li>country (optional):         		the country (ISO code, two places)
 *		<li>decimalSeparator (optional):		the character used for decimal sign (default: depends on language and country)
 *		<li>groupingSeparator (optional):		the character used for thousands separator (default: depends on language and country)
 *		<li>currencyConversion (optional):   	"Y"/"N" toggle: currency or unit of measurement conversion.
 *											    "Y": 'isoCodes' are iso codes for currencies
 *											    "N": 'isoCodes' are iso codes for units of measurement (default value)
 * </ul><p>
 *
 * <b>Export parameters</b>
 * <ul>
 *		<li>internalUnits:   				    the array of the internal (SAP) units of measurement
 *		<li>refs:              				    the corresponding array of ISO code references (conversion was possible). The ISO code reference is a counter that starts with 1 for the first item to be created.
 *		<li>externalUnits:   				    the array of the external and language dependent (SAP) units of measurement
 *		<li>formattedValues:   				    the array of the values in an external and locale dependent representation
 *      <li>errorMessages:						an array of error messages
 *		<li>errorMessageRefs:  				    the corresponding array of ISO code references (conversion was not possible). The ISO code reference is a counter that starts with 1 for the first item to be created.
 * </ul><p>
 *
 * The command guarantees that the arrays internalUnits, refs, externalUnits (if the input parameters language and country are set)
 * and formattedValues (if the input parameters values, language and country are set) are of the same length.
 * The arrays errorMessages and errorMessageRefs are of the same length.
 *
 */

public interface ConvertIsoCodeToUnitOfMeasurement {

	// input parameter
	public static final String ISO_CODES								= "isoCodes";
	public static final String VALUES   								= "values";
	public static final String LANGUAGE 								= "language";
	public static final String COUNTRY   								= "country";
	public static final String DECIMAL_SEPARATOR						= "decimalSeparator";
	public static final String GROUPING_SEPARATOR						= "groupingSeparator";
	public static final String CURRENCY_CONVERSION						= "currencyConversion";

	// output parameter
	public static final String INTERNAL_UNITS   						= "internalUnits";
	public static final String REFS                						= "refs";
	public static final String EXTERNAL_UNITS   						= "externalUnits";
	public static final String FORMATTED_VALUES   						= "formattedValues";
	public static final String ERROR_MESSAGES   						= "errorMessages";
	public static final String ERROR_MESSAGE_REFS   					= "errorMessageRefs";
}
