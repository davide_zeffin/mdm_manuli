/************************************************************************

	Copyright (c) 1999 by SAP AG

	All rights to both implementation and design are reserved.

	Use and copying of this software and preparation of derivative works
	based upon this software are not permitted.

	Distribution of this software is restricted. SAP does not make any
	warranty about the software, its performance or its conformity to any
	specification.

**************************************************************************/

package com.sap.spc.remote.shared.command;


/**
 * Sets values of more cstics.  <p>
 *
 * <b>Import parameters</b>
 *
 * <ul>
 *     <li>documentId (required):	the document id of the item being configured
 *     <li>itemId (required):		the item id of the item being configured
 *     <li>instId (required):		the instance id the cstic belongs to
 *     <li>csticNames[] (required):	the (language independent) names of the cstics
 *     <li>valueNames[] (optional): the values to set (language independent names).
 *         If a cstic is single valued, only valueNames[0] will be set. If the valueNames
 *		   are not present, no operation will be performed. If a single, empty value is
 *         passed to this command, all values of that characteristic are deleted.
 *     <li>formatValue (optional): "Y"/"N" toggle: the values in valueNames[] are formatted in the current locale or
 *         language dependent. Default: "N".
 *     <li>formatValues[] (optional): "Y"/"N" toggle: the same as formatValue, but it allows to
 *         specify this flag for each value. This makes sense especially to differenciate between
 *         additional values and other values. Default: "N".
 * </ul><p>
 *
 * <b>Export parameters</b>
 */

public interface SetCsticsValues extends SCECommand
{
	public static final String INST_ID = "instId";
	public static final String CSTIC_NAMES = "csticNames";
	public static final String VALUE_NAMES = "valueNames";
	public static final String FORMAT_VALUE = "formatValue";
	public static final String FORMAT_VALUES = "formatValues";
}