/************************************************************************

	Copyright (c) 1999 by SAP AG

	All rights to both implementation and design are reserved.

	Use and copying of this software and preparation of derivative works
	based upon this software are not permitted.

	Distribution of this software is restricted. SAP does not make any
	warranty about the software, its performance or its conformity to any
	specification.

**************************************************************************/
package com.sap.spc.remote.shared.command;

/**
 * Returns the pricing informations of an item without recomputing the
 * prices.  <p>
 *
 * <b>Import parameters</b>
 *
 * <ul>
 *     <li>documentId (required):	the document id of the item we're interested in
 *     <li>itemId (required):		the item id of the item we're interested in
 *     <li>formatValue (optional):	"Y"/"N" toggle: "Y": the values of the export parameters are converted to a string in the locale representation (default value is "Y")
 * </ul><p>
 *
 * <b>Export parameters</b>
 *
 * <ul>
 *     <li>grossValue:			the gross value of the item which is the sum of net value and tax value
 *	   <li>netValue:			the net value of the item
 *	   <li>taxValue:			the tax value of the item
 *     <li>totalGrossValue:		the total gross value of the item (this is the sum of the gross values of all subitems;
 *								if the item doesn't have subitems, return the gross value of the item.)
 *     <li>totalNetValue:		the total net value of the item (this is the sum of the net values of all subitems;
 *								if the item doesn't have subitems, return the net value of the item.)
 *     <li>totalTaxValue:		the total tax value of the item (this is the sum of the tax values of all subitems;
 *								if the item doesn't have subitems, return the tax value of the item.)
 *     <li>subtotal1:			the subtotal1 value of the item
 *     <li>subtotal2:			the subtotal2 value of the item
 *     <li>subtotal3:			the subtotal3 value of the item
 *     <li>subtotal4:			the subtotal4 value of the item
 *     <li>subtotal5:			the subtotal5 value of the item
 *     <li>subtotal6:			the subtotal6 value of the item
 *     <li>freight:				the accumulated freight value of the item
 *     <li>netValueWithoutFreight:	the accumulated net value without freight of the item
 *     <li>grossValueSubtotal:	the gross value (derived from subtotal) of the item. Corresponds to the R/3 field BRTWR
 *     <li>currencyUnit:		the (document) currency unit
 *     <li>netPrice:	        the accumulated net prices of all items in this document (NETPR)
 *     <li>netPricePricingUnitValue: the value of the net price's pricing unit
 *     <li>netPricePricingUnitUnit:  the name of the net price's pricing unit
 *     <li>cashDiscount:	    the accumulated cash discounts of all items in this document (SKONTO)
 *     <li>cashDiscountBasis:	the accumulated cash discount bases of all items in this document (SKFDF)
 *     <li>statisticalValuet:	the statistical value for foreign trade of the item
 *     <li>purposeNames[]:	    the array of purpose field names (coming from the customizing of the condition types) of all items in this document.
 *     <li>purposeValues[]:	    the corresponding array of accumulated values
 * 	<li>dynamicReturnNames[]:	the array of dynamic return field names
 * 	<li>dynamicReturnValues[]:	the corresponding array of dynamic return values
 * </ul><p>
 */

public interface GetPricingItemInfo {
	// input parameter
	public static final String DOCUMENT_ID								= "documentId";
	public static final String ITEM_ID									= "itemId";
	public static final String FORMAT_VALUE								= "formatValue";

	// output parameter
	public static final String GROSS_VALUE								= "grossValue";
	public static final String NET_VALUE								= "netValue";
	public static final String TAX_VALUE								= "taxValue";
	public static final String TOTAL_GROSS_VALUE						= "totalGrossValue";
	public static final String TOTAL_NET_VALUE							= "totalNetValue";
	public static final String TOTAL_TAX_VALUE							= "totalTaxValue";
	public static final String SUBTOTAL_1								= "subtotal1";
	public static final String SUBTOTAL_2								= "subtotal2";
	public static final String SUBTOTAL_3								= "subtotal3";
	public static final String SUBTOTAL_4								= "subtotal4";
	public static final String SUBTOTAL_5								= "subtotal5";
	public static final String SUBTOTAL_6								= "subtotal6";
	public static final String FREIGHT									= "freight";
	public static final String NET_VALUE_WITHOUT_FREIGHT				= "netValueWithoutFreight";
	public static final String GROSS_VALUE_DERIVED_FROM_SUBTOTAL		= "grossValueSubtotal";
	public static final String CURRENCY_UNIT							= "currencyUnit";
	public static final String ITEM_NET_PRICE                           = "netPrice";
	public static final String NET_PRICE_PRICING_UNIT_VALUE             = "netPricePricingUnitValue";
	public static final String NET_PRICE_PRICING_UNIT_NAME              = "netPricePricingUnitName";
	public static final String ITEM_CASH_DISCOUNT                       = "cashDiscount";
	public static final String ITEM_CASH_DISCOUNT_BASIS                 = "cashDiscountBasis";
	public static final String STATISTICAL_VALUE						= "statisticalValue";
	public static final String ITEM_PURPOSE_NAMES                       = "purposeNames";
	public static final String ITEM_PURPOSE_VALUES                      = "purposeValues";
	public static final String ITEM_DYNAMIC_RETURN_NAMES		= "dynamicReturnNames";
	public static final String ITEM_DYNAMIC_RETURN_VALUES		= "dynamicReturnValues";

	//For swing GUI---------------------------start
	public static final String IS_SWINGGUI						        = "isSwingGUI";
	public static final String GROSS_WEIGHT					        	= "grossWeight";
	public static final String NET_WEIGHT					        	= "netWeight";
	public static final String VOLUME					        	    = "volume";
	public static final String EXTERNAL_ITEM_ID                         = "externalItemId";
	public static final String IS_UNCHANGEABLE                          = "isUnchangeable";
	public static final String IS_TRACING                               = "isTracing";
	public static final String NET_UNIT_NAME							= "netUnitName";
	public static final String NET_UNIT_DESC							= "netUnitDesc";
	public static final String TAX_UNIT_NAME							= "taxUnitName";
	public static final String TAX_UNIT_DESC							= "taxUnitDesc";
	public static final String SALES_QUANTITY							= "salesQuantity";
	public static final String BASE_QUANTITY							= "baseQuantity";
	public static final String QUANTITY_UNIT_NAME						= "quantityUnitName";
	public static final String QUANTITY_UNIT_DESC						= "quantityUnitDesc";
	public static final String WEIGHT_UNIT_NAME					       	= "weightUnitName";
	public static final String WEIGHT_UNIT_DESC					       	= "weightUnitDesc";
	public static final String VOLUME_UNIT_NAME					       	= "volumeUnitName";
	public static final String VOLUME_UNIT_DESC					       	= "volumeUnitDesc";
	public static final String PRODUCT_ID						    	= "productId";
	public static final String PRODUCT_DESC							    = "productDesc";
	public static final String NO_VISIBLE_CONDITIONS				    = "numberOfVisibleConditions";
	public static final String EXCHANGE_RATE        				    = "exchangeRate";

	//For swing GUI---------------------------end.

}
