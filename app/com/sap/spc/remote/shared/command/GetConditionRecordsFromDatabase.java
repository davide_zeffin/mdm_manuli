/************************************************************************

Copyright (c) 1999 by SAP AG

All rights to both implementation and design are reserved.

Use and copying of this software and preparation of derivative works
based upon this software are not permitted.

Distribution of this software is restricted. SAP does not make any
warranty about the software, its performance or its conformity to any
specification.

**************************************************************************/
package com.sap.spc.remote.shared.command;

public interface GetConditionRecordsFromDatabase extends ConditionSelectionCommand {
	public static final String 	SEL_ATTRIBUTES_INCL_ARRAY	="selAttributesInclArray";
	public static final String 	SEL_ATTRIBUTES_INCL_VALUE_ARRAY	="selAttributesInclValueArray";
	public static final String 	SEL_ATTRIBUTES_EXCL_ARRAY	="selAttributesExclArray";
	public static final String 	SEL_ATTRIBUTES_EXCL_VALUE_ARRAY	="selAttributesExclValueArray";
	public static final String 	APPLICATION	="application";
	public static final String 	USAGE	="usage";
	public static final String 	VALIDITY_DATE_START	="validityDateStart";
	public static final String 	VALIDITY_DATE_END	="validityDateEnd";
	public static final String 	CONDITION_GROUP_ID	="conditionGroupId";
	public static final String 	NUMBER_OF_CONDITION_RECORDS	="numberOfConditionRecords";

}
