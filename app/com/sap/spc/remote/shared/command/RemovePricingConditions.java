/*******************************************************************************

	Copyright (c) 2000 by SAP AG

	All rights to both implementation and design are reserved.

	Use and copying of this software and preparation of derivative works based
	upon this software are not permitted.

	Distribution of this software is restricted. SAP does not make any warranty
	about the software, its performance or its conformity to any specification.

*******************************************************************************/
package com.sap.spc.remote.shared.command;

/**
 * Removes a couple of pricing conditions.
 *
 * <b>Import parameters</b>
 *
 * <ul>
 *     <li>documentId (required):			the document id of the item we're interested in
 *     <li>itemIds (optional):				the item id of the item we're interested in
 *                                          (if not given, the request is for header conditions)
 *     <li>stepNumbers (optional):			the step number of the pricing condition we're interested in
 *     <li>conditionCounters (optional):	the counter of the pricing or header condition we're interested in
 *     <li>sumCounters (optional):			the counter of the pricing condition on header level (ZAEHK)
 *     <li>suppressChecks (optional):		the process mode: "Y" (legacy mode), "N" (manual mode) or "A" (external determination mode).
 * </ul><p>
 *
 * The arrays have to be of the same length.
 * On item level:   step number & counter is mandatory
 * On header level: either the sumCounter or the step number & the counter must be supplied.
 * 
 * <b>Export parameters</b>
 * <ul>
 *     <li>objectIds:						an array of object ids the messages are related to
 *     <li>messages:						an array of error messages
 *     <li>messageTypes:					indicates the severity level of the corresponding error message
 * </ul>
 */
public interface RemovePricingConditions {

	// input parameter
	public static final String DOCUMENT_ID								= "documentId";
	public static final String ITEM_IDS									= "itemIds";
	public static final String STEP_NUMBERS								= "stepNumbers";
	public static final String CONDITION_COUNTERS						= "conditionCounters";
	public static final String SUM_COUNTERS								= "sumCounters";
	public static final String SUPPRESS_CHECKS  						= "suppressChecks";

	// output parameters
	public static final String OBJECT_IDS						    	= "objectIds";
	public static final String MESSAGES		    						= "messages";
	public static final String MESSAGE_TYPES		    				= "messageTypes";
}
