/*
 * Created on Nov 17, 2004
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package com.sap.spc.remote.shared.command;

/**
 * @author I026584
 *
 * TODO To change the template for this generated type comment go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
public interface ConversionTestCommand {
	//input
	public static final String FROM_CURRENCY		= "fromCurrency";
	public static final String TO_CURRENCY			= "toCurrency";
	public static final String CURRENCY_VALUE		= "currencyValue";
	//output
	public static final String RESULT				= "result";

}
