/************************************************************************

	Copyright (c) 1999 by SAP AG

	All rights to both implementation and design are reserved.

	Use and copying of this software and preparation of derivative works
	based upon this software are not permitted.

	Distribution of this software is restricted. SAP does not make any
	warranty about the software, its performance or its conformity to any
	specification.

**************************************************************************/

package com.sap.spc.remote.shared.command;


/**
 * Returns all values of a cstic.  <p>
 *
 * <b>Import parameters</b>
 *
 * <ul>
 *     <li>documentId (required):	the document id of the item being configured
 *     <li>itemId (required):		the item id of the item being configured
 *     <li>instId (required):		the instance id of the instance we're interested in
 *	   <li>csticName (required):	the name of the cstic
 *     <li>getMimeObjects (optional): Flag YES/NO - if this is YES, you will receive a list of mime objects
 *     <li>withoutDescription (optional): Flag YES/NO - if this is NO, you will receive the description (default=NO)
 * </ul><p>
 *
 * <b>Export parameters</b>
 *
 * <ul>
 *     <li> valueNames[]:		an array of value names	(language independent)
 *     <li> valueLNames[]:		an array of language dependent value names
 *     <li> valueSelectables[]:	"Y"/"N" toggle: is the value selectable by the user?
 *     <li> valueDefaults[]:	"Y"/"N" toggle: is the value a default value?
 *     <li> valueAssigneds[]:	"Y"/"N" toggle: has this value been assigned (set)?
 *     <li> valueAuthors[]:		Author of this value. "U" (user) or "S" (system)
 *     <li> valuePrices[]:		The prices associated with the values (some may be null).
 *     <li> valueLPrices[]:     The prices associated with the values, formatted for the current locale (some may be null).
 *     <li> valueImages[]:      The images associated with the values (some may be null).
 *	   <li> valueDescriptions[]:The long text for these values
 *     <li> mimeValueLNames[]:  an array of value language dependent names for matching the mime objects with the instances
 *     <li> mimeObjects[]:      an array of mime object url's
 *     <li> mimeObjectTypes[]:  an array of mime object types (e.g. "image,sound,...")
 * </ul>
 *
 * The command guarantees that all returned arrays are of equal length
 */
public interface GetCsticValues extends SCECommand, GetCstics
{
	public static final String CSTIC_NAME		  = "csticName";

	public static final String VALUE_NAME		  = "valueNames";
	public static final String VALUE_LNAME		  = "valueLNames";
	public static final String VALUE_SELECTABLE	  = "valueSelectables";
	public static final String VALUE_DEFAULT	  = "valueDefaults";
	public static final String VALUE_ASSIGNED	  = "valueAssigneds";
	public static final String VALUE_AUTHOR		  = "valueAuthors";
	public static final String VALUE_PRICE		  = "valuePrices";
 	public static final String VALUE_LPRICE       = "valueLPrices";
	public static final String VALUE_IMAGE        = "valueImages";
	public static final String VALUE_DESCRIPTION  = "valueDescriptions";
	public static final String GET_MIME_OBJECTS   = "valueGetMimeObjects";

	public static final String MIME_VALUE_LNAMES = "mimeValueLNames";
	public static final String MIME_OBJECT_TYPES = "mimeObjectTypes";
	public static final String MIME_OBJECTS      = "mimeObjects";

// 25.05.01 mk
	public static final String ASSIGNABLE_VALUES_ONLY = "assignableValuesOnly";
}
