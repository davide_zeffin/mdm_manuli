/************************************************************************

	Copyright (c) 2000 by SAP AG

	All rights to both implementation and design are reserved.

	Use and copying of this software and preparation of derivative works
	based upon this software are not permitted.

	Distribution of this software is restricted. SAP does not make any
	warranty about the software, its performance or its conformity to any
	specification.

**************************************************************************/

package com.sap.spc.remote.shared.command;

/**
 * Checks if the configuration has changed after the last call. To allow multiple
 * checks working in the same session, you have to provide a (unique) key that
 * identifies you. The output parameter "changed" will be set to "Y" if and only
 * if the configuration has been changed after the last call of this command with
 * the same key. If no key is specified, the default key "DEFAULT" is used.<p>
 * 
 * <b>Import parameters</b>
 * 
 * <ul>
 *	<li>documentId (required):		the document id of the document that holds the config
 *  <li>itemId     (required):		the item id of the item that holds the config
 *  <li>key        (optional):		the key for the check.
 * </ul>
 * 
 * <b>Export parameters</b>
 * 
 * <ul>
 *  <li>changed:					"Y"/"N" toggle: the config has changed since the last call
 * </ul>
 * <p>
 */
public interface GetConfigHasChanged extends SCECommand
{
	// import
	public static final String KEY		= "key";
	
	// export
	public static final String CHANGED  = "changed";
}
