/************************************************************************

	Copyright (c) 1999 by SAP AG

	All rights to both implementation and design are reserved.

	Use and copying of this software and preparation of derivative works
	based upon this software are not permitted.

	Distribution of this software is restricted. SAP does not make any
	warranty about the software, its performance or its conformity to any
	specification.

**************************************************************************/

package com.sap.spc.remote.shared.command;


/**
 * Returns information about (possibly filtered) knowledge bases. <b>Please note that in IPC 2.0c,
 * this command will fail if the session has not been initialized by CreateSession before.</b>  <p>
 *
 * <b>Import parameters</b>
 *
 * <ul>
 *     <li>objectName (optional):	        filters by object name
 *     <li>objectType (optional):	        filters by object type (kbrt.object_type string constants)
 *     <li>objectLogsys (optional):	        filters by object logical system
 *     <li>salesOrganisation (optional):	(deprecated and CRM only) filters by a sales organisation
 *     <li>distributionChannel (optional):	(deprecated and CRM only) filters by a distribution channel
 *     <li>validDate (optional):	        filters by object logical system
 *     <li>kbUsage (optional):
 *     <li>orgId (optional):
 *     <li>status (optional):
 *     <li>readKbProfiles (optional):
 * 	   <li>loadKbs (optional):			    ("Y"/"N" switch) does not only find the kbs, but also loads (buffers) 
 * 											every KB that has been found. Default is "N".
 * </ul><p>
 *
 * <b>Export parameters</b>
 *
 * <ul>
 *     <li>kbNames:			(deprecated) array of the knowledge base names that match the request
 *     <li>kbProfiles:		(deprecated) array of the kb profiles that match the request
 *     <li>kbVersions:		(deprecated) array of the kb versions that match the request
 *     <li>kbOrgid: 		(deprecated) array of the kb orgids that match the request
 *
 *     <li>kbsHierarchy:	array of
 *     <li>kbsLogsys:		array of
 *     <li>kbsNames:		array of the knowledge base names that match the request
 *     <li>kbsVersions:		array of the kb versions that match the request
 *     <li>kbsUsage:		array of
 *     <li>kbsOrgType:		array of
 *     <li>kbsOrgId:		array of
 *     <li>kbsActive:		array of
 *     <li>kbsFromDate:		array of
 *     <li>kbsToDate:		array of
 *     <li>kbsCreator:		array of
 *     <li>kbsChanger:		array of
 *     <li>kbsChangeDate:	array of
 *     <li>kbsBuild:		array of
 *     <li>kbsStatus:		array of
 *     <li>kbsId:			array of
 *     <li>kbsProfiles:		array of the kb profiles that match the request
 * </ul><p>
 *
 * The command guarantees that kbName.length == version.length == profile.length.
 */

public interface FindKnowledgeBases extends SCECommand
{
	public static final String OBJECT_NAME			= "objectName";
	public static final String OBJECT_TYPE			= "objectType";
	public static final String OBJECT_LOGSYS    	= "objectLogsys";
	public static final String VALID_DATE			= "validDate";
	public static final String USAGE			    = "usage";
	public static final String ORG_TYPE   			= "orgType";
	public static final String ORG_ID   			= "orgId";
	public static final String STATUS	    		= "status";
	public static final String WITH_KB_PROFILES		= "withKbProfiles";
	public static final String WITHOUT_TABLE_KBKEYS	= "withoutTableKbkeys";
	public static final String LOAD_KBS				= "loadKbs";

    // deprecated SALES_ORGANISATION and DISTRIBUTION_CHANNEL
	public static final String SALES_ORGANISATION	= "salesOrganisation";
	public static final String DISTRIBUTION_CHANNEL	= "distributionChannel";

    // deprecated
	public static final String KB_NAME	            = "kbNames";
	public static final String KB_VERSION		    = "kbVersions";
	public static final String KB_PROFILE     	    = "kbProfiles";
	public static final String KB_ORGID 	    	= "kbOrgid";

	public static final String KBS_HIERARCHY		= "kbsHierarchy";
	public static final String KBS_LOGSYS 			= "kbsLogsys";
	public static final String KBS_NAME				= "kbsName";
	public static final String KBS_VERSION			= "kbsVersion";
	public static final String KBS_USAGE			= "kbsUsage";
	public static final String KBS_ORGTYPE		    = "kbsOrgtype";
	public static final String KBS_ORGID			= "kbsOrgid";
	public static final String KBS_ACTIVE	    	= "kbsActive";
	public static final String KBS_FROMDATE			= "kbsFromdate";
	public static final String KBS_TODATE		    = "kbsTodate";
	public static final String KBS_CREATOR			= "kbsCreator";
	public static final String KBS_CHANGER			= "kbsChanger";
	public static final String KBS_CHANGEDATE		= "kbsChangedate";
	public static final String KBS_BUILD			= "kbsBuild";
	public static final String KBS_STATUS			= "kbsStatus";
	public static final String KBS_ID    			= "kbsId";
	public static final String KBS_PROFILE 			= "kbsProfile";
	public static final String KBS_EXT_REF 			= "kbsExtReference";
	public static final String KBS_HIER_VERSION		= "kbsHierarchyVersion";
}
