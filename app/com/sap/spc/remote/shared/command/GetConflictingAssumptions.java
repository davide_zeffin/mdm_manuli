package com.sap.spc.remote.shared.command;

/**
 * Returns an array of conflicting Statements<p>
 * <b>Import parameters</b>
 * <ul>
 *    <li>documentId (required):	the document id of the item being configured
 *    <li>itemId (required):		the item id of the item being configured
 *    or alternatively
 *    <li>configId (required):		the instance id of the instance
 * </ul><p>
 *
 * <b>Export parameters</b>
 *    <li>assumptionGroupId[]:      the id of the group to tell to which
 *                                  group the assumption belongs; is just a counting
 *                                  number beginning with 1.
 *    <li>assumptionId[]:           the id of the assumption
 *    <li>assumptionLName[]:        text description of each assumption
 *    <li>assumptionAuthor[]:       who made the statement returns
 *                                  ASSUMPTION_AUTHOR_USER for user
 *                                  ASSUMPTION_AUTHOR_DEFAULT for default
 * All export parameter arrays are of the same length
 */

public interface GetConflictingAssumptions extends SCECommand{
  public static String ASSUMPTION_GROUP_ID = "assumptionGroupId";
  public static String ASSUMPTION_ID = "assumptionId";
  public static String ASSUMPTION_LNAME = "assumptionLName";
  public static String ASSUMPTION_AUTHOR = "assumptionAuthor";
  public static String ASSUMPTION_AUTHOR_DEFAULT = "D";
  public static String ASSUMPTION_AUTHOR_USER = "U";
}