/************************************************************************

	Copyright (c) 1999 by SAP AG

	All rights to both implementation and design are reserved.

	Use and copying of this software and preparation of derivative works
	based upon this software are not permitted.

	Distribution of this software is restricted. SAP does not make any
	warranty about the software, its performance or its conformity to any
	specification.

**************************************************************************/
package com.sap.spc.remote.shared.command;

/**
 * Removes one or multiple items (and their subitems) from a document.  <p>
 *
 * <b>Import parameters</b>
 *
 * <ul>
 *     <li>documentId (required):	the document id of the items being removed
 *     <li>itemIds (required):		the array of item ids being removed
 * </ul><p>
 *
 * <b>Export parameters</b>
 *
 * <ul>
 *     <li>removedItemIds:     		the array of item ids being removed
 * </ul><p>
 * <b>Export parameters</b>
 */

public interface RemoveItems {
	// input parameters
	public static final String DOCUMENT_ID								= "documentId";
	public static final String ITEM_IDS									= "itemIds";

	// output parameter
	public static final String REMOVED_ITEM_IDS							= "removedItemIds";
}