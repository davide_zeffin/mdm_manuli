/************************************************************************

	Copyright (c) 1999 by SAP AG

	All rights to both implementation and design are reserved.

	Use and copying of this software and preparation of derivative works
	based upon this software are not permitted.

	Distribution of this software is restricted. SAP does not make any
	warranty about the software, its performance or its conformity to any
	specification.

**************************************************************************/

package com.sap.spc.remote.shared.command;


/**
 * Changes the product of an item. The command reinitializes the item to the
 * new product, ie. all subitems are deleted/reconstructed.  <p>
 *
 * <b>Import parameters</b>
 *
 * <ul>
 *     <li>documentId (required):			the document that holds the item.
 *	   <li>itemId (required):				the item to change.
 *     <li>productId (required):			the product id of the new product.
 * </ul><p>
 *
 * <b>Export parameters</b>
 *
 * <ul>
 *     <li>success:							"Y"/"N" toggle. If eg. quantity conversion failed,
 *											"N" is returned.
 * </ul>
 */
public interface ChangeItemProduct extends SCECommand
{
	// imports
	public static final String PRODUCT_ID			= "productId";
    public static final String DATA_MODEL           = "dataModel";
    public static final String DATA_MODEL_CRM       = "CRM";
	
    // exports
	public static final String SUCCESS				= "success";
}
