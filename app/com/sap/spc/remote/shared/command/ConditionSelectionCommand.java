/************************************************************************

Copyright (c) 1999 by SAP AG

All rights to both implementation and design are reserved.

Use and copying of this software and preparation of derivative works
based upon this software are not permitted.

Distribution of this software is restricted. SAP does not make any
warranty about the software, its performance or its conformity to any
specification.

**************************************************************************/
package com.sap.spc.remote.shared.command;

public interface ConditionSelectionCommand {
    
     
    //output 
    public static final String 	NUMBER_OF_RECORD_RETURNED 	="numberOfRecordReturned";
    public static final String 	CONDITION_RECORD_IDS	="ConditionRecordIds";
    public static final String 	VALIDITY_ENDS	="validityEnds";
    public static final String 	VALIDITY_STARTS	="validityStarts";
    public static final String 	OBJECT_IDS	="objectIds";
    public static final String 	RELEASE_STATUS	="releaseStatus";
    public static final String 	CONDITION_TYPES	="conditionTypes";
    public static final String 	SUPPLEMENTARY_CONDITION_TYPES	="supplementaryConditionTypes";
    public static final String 	GROUP_IDS	="groupIds";
    public static final String 	MAINTENANCE_STATUS	="maintenanceStatus";
    public static final String 	SUPPLEMENTARY_CONDITION_IDS	="supplementaryConditionIds";
    public static final String 	DIMENSION_NUMBERS	="dimensionNumbers";
    public static final String 	APPLICATION_FIELD_NUMBERS	="applicationFieldNumbers";
    public static final String 	APPLICATION_FIELD_OFFSETS	="applicationFieldOffsets";
    public static final String 	APPLICATION_FIELD_NAMES	="applicationFieldNames";
    public static final String 	APPLICATION_FIELD_VALUES	="applicationFieldValues";
    public static final String 	USAGE_FIELD_NUMBERS	="usageFieldNumbers";
    public static final String 	USAGE_FIELD_OFFSETS	="usageFieldOffsets";
    public static final String 	USAGE_FIELD_NAMES	="usageFieldNames";
    public static final String 	USAGE_FIELD_VALUES	="usageFieldValues";
    public static final String 	SCALE_BASE_TYPES	="scaleBaseTypes";
    public static final String 	SCALE_LINE_NUMBERS	="scaleLineNumbers";
    public static final String 	SCALE_LINE_OFFSETS	="scaleLineOffsets";
    public static final String 	PR_SCALE_LINE_REFS	="PRScaleLineRefs";
    public static final String 	PR_SCALE_AMOUNTS	="PRScaleAmounts";
    public static final String 	PR_SCALE_RATES	            ="PRScaleRates";
	public static final String 	PR_SCALE_RATES_CURR_UNIT	="PRScaleRatesCurrUnit";    
	public static final String 	PR_SCALE_PRICING_UNIT      	="PRScalePricingUnit";
	public static final String 	PR_SCALE_PRODUCT_UNIT	    ="PRScaleProductUnit";  	  	
    public static final String 	FG_SCALE_LINE_REFS	="FGScaleLineRefs";
    public static final String 	FG_MIN_QUANTITIES	="FGMinQuantities";
    public static final String 	FG_QUANTITY_UNITS	="FGQuantityUnits";
    public static final String 	FG_QUANTITIES	="FGQuantities";
    public static final String 	FG_ADD_QUANTITIES	="FGAddQuantities";
    public static final String 	FG_ADD_QUANTITY_UNITS	="FGAddQuantityUnits";
    public static final String 	FG_ADD_PRODUCTS	="FGAddProducts";
    public static final String 	FG_CALC_TYPES	="FGCalcTypes";
    public static final String 	FG_INDICATORS	="FGIndicators"; 
    



}
