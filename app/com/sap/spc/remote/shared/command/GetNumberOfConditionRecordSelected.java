/************************************************************************

Copyright (c) 1999 by SAP AG

All rights to both implementation and design are reserved.

Use and copying of this software and preparation of derivative works
based upon this software are not permitted.

Distribution of this software is restricted. SAP does not make any
warranty about the software, its performance or its conformity to any
specification.

**************************************************************************/
package com.sap.spc.remote.shared.command;

public interface GetNumberOfConditionRecordSelected {
    
    public static final String NUMBER_OF_CONDITION_RECORDS	="numberOfConditionRecords";    
    public static final String USAGE	="usage";    

}
