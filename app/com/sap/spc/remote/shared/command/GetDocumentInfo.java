package com.sap.spc.remote.shared.command;


/** Returns the information related to a document
 * <b>Import parameters</b>
 * <ul>
 *		<li>documentId(required)		: document id for identifying the document(for ex. 22344354534467804874768876354126)
 * </ul><p>
 *
 * <b>Export parameters</b>
 * <ul>
 *		<li>documentCurreny				: currency unit of the document
 *		<li>localCurreny				: local currency unit
 *		<li>pricingProcedure			: pricing procedure to be used by the document
 *		<li>country						:
 *		<li>departureRegion				: region from where document departs
 *		<li>salesOrganisation			: sales organisation of the document
 *		<li>distributionChannel			: distribution channel of the document
 *		<li>division					: division of the document
 *		<li>headerAttributeNames[]		: names of all the header attributes the document has (for ex. SALES_ORG)
 *		<li>headerAttributeValues[]		: values of all the header attributes corresponding to the header attribute names
 *		<li>externalId					: external id of the document (for ex. document00001)
 * </ul><p>
 */
public interface GetDocumentInfo
{
	// import parameters
    public static final String DOCUMENT_ID				= "documentId";

	// export parameters
    /* inherited
	public static final String LANGUAGE					= "language";
	*/
    public static final String DOCUMENT_CURRENCY		= "documentCurrency";
    public static final String LOCAL_CURRENCY			= "localCurrency";
    public static final String PRICING_PROCEDURE_NAME	= "pricingProcedureName";
    public static final String COUNTRY					= "country";
    public static final String DEPARTURE_REGION			= "departureRegion";
    public static final String SALES_ORGANISATION		= "salesOrganisation";
    public static final String DISTRIBUTION_CHANNEL		= "distributionChannel";
    public static final String DIVISION					= "division";
    public static final String EDIT_MODE        		= "editMode";
    public static final String DEPARTURE_COUNTRY		= "departureCountry";
	public static final String HEADER_ATTRIBUTES_NAMES	= "headerAttributeNames";
	public static final String HEADER_ATTRIBUTE_VALUES  = "headerAttributeValues";
	public static final String EXTERNAL_ID				= "externalId";
	public final String TAX_DEPART_CTY = "TAX_DEPART_CTY";
	public final String SALES_ORG = "SALES_ORG";
	public final String TAX_DEPART_REG = "TAX_DEPART_REG";
	public final String DIV = "DIVISION";
	public final String DIS_CHANNEL = "DIS_CHANNEL";
}
