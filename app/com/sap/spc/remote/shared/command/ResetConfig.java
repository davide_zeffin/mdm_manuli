package com.sap.spc.remote.shared.command;

/**
 * Resets an instance of a configuration. This method resets a given instance, ie. resets
 * all its characteristics to their default values and performs a completeness check.
 * If no instance is passed, the root instance is "resetted".
 * <p>
 * <b>Import parameters</b>
 *
 * <ul>
 *     <li>documentId (required):	the document id of the item being configured
 *     <li>itemId (required):		the item id of the item being configured
 *     <li>instId (optional):
 * </ul>
 * <p>As for every configuration command, instead of documentId+itemId, a configId can be used.<p>
 */
public interface ResetConfig extends SCECommand {
	/**
	 * optional import parameter: reset only this instance
	 */
	public static final String INST_ID = "instId";
}
