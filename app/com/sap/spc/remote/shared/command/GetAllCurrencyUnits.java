/************************************************************************

	Copyright (c) 1999 by SAP AG

	All rights to both implementation and design are reserved.

	Use and copying of this software and preparation of derivative works
	based upon this software are not permitted.

	Distribution of this software is restricted. SAP does not make any
	warranty about the software, its performance or its conformity to any
	specification.

**************************************************************************/
package com.sap.spc.remote.shared.command;

/**
 * Returns all currency units available.<p>
 * 
 * <b>Import parameters</b>
 * <ul>
 *     <li>language :	the language for locale
 *     <li>country :	the country for locale
 * </ul><p>
 * 
 * <b>Export parameters</b>
 * <ul>
 *     <li>name:					the names (keys) of the currency units
 *     <li>description:				the descriptions of the currency units
 *     <li>decimals:				the number of decimal digits
 * </ul>
 */
public interface GetAllCurrencyUnits
{
	//Input
	public static final String CURR_LANGUAGE = "language";
	public static final String COUNTRY = "country";
	
	//output
	public static final String NAME = "name";
	public static final String DESCRIPTION = "description";
	public static final String DECIMALS = "decimals";
	
}
