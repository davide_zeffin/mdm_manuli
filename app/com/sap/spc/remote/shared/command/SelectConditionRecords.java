/************************************************************************

Copyright (c) 1999 by SAP AG

All rights to both implementation and design are reserved.

Use and copying of this software and preparation of derivative works
based upon this software are not permitted.

Distribution of this software is restricted. SAP does not make any
warranty about the software, its performance or its conformity to any
specification.

**************************************************************************/
package com.sap.spc.remote.shared.command;

public interface SelectConditionRecords {
    
    // input parameters
    public static final String 	ATTRIBUTE_NAMES	="attributeNames";
    public static final String 	ATTRIBUTE_VALUES	="attributeValues";
    public static final String 	APPLICATION	="application";
    public static final String 	USAGE	="usage";
    public static final String 	VALIDITY_DATE_START	="validityDateStart";
    public static final String 	VALIDITY_DATE_END	="validityDateEnd";
    public static final String 	CONDITION_GROUP_ID	="conditionGroupId";
    public static final String 	NUMBER_OF_CONDITION_RECORDS	="numberOfConditionRecords";
    
  

}
