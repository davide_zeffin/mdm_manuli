/*
 * Created on Dec 28, 2004
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package com.sap.spc.remote.shared.command;

/**
 * @author I026584
 *
 * TODO To change the template for this generated type comment go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
public interface GetConfigInCSVFormat {
	public static final String EXPORT_INVISIBLE	= "1";
	public static final String DO_NOT_EXPORT_INVISIBLE = "0";

	/**
	 * Returns the configuration in CSV format  <p>
	 *
	 * <b>Import parameters</b>
	 *
	 * <ul>
	 *      <li>documentId (required):	the id of the document we're interested in
	 *      <li>itemId (required):	the id of the item we're interested in
	 *      <li>exportInvisibleAlso (optional):	Set value 1 if Invisible cstics also needs to be exported or else set 0 (default 0)
	 *	    <li>restrictToView (optional): pass a one-digit string here to get only information about those characteristics that are
	 *				in the application view defined by that characteristic. If the string is longer than one character, the first character
	 *			    will be used. An empty string does not filter.
	 * </ul>
	 *
	 * <b>Export parameters</b>
	 *
	 * <ul>
	 *      <li>configInCSVFormat :	Configuration in CSV format.
	 * </ul>
	 */

	//Import parameters
	public static final String DOCUMENT_ID				= "documentId";
	public static final String ITEM_ID					= "itemId";
	public static final String EXPORT_INVISIBLE_ALSO	= "exportInvisibleAlso";
	public static final String RESTRICT_TO_VIEW			= "restrictToView";

	//Output parameters
	public static final String CONFIG_IN_CSV_FORMAT		= "configInCSVFormat";

}
