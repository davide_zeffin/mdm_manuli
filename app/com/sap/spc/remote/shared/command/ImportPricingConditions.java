package com.sap.spc.remote.shared.command;

/**
 * An interface internally used by some SPE commands.
 */
public interface ImportPricingConditions
{
	// flag for importing external conditions (valid for the whole command call)
	public static final String IMPORT_PRICING_CONDITIONS    		    = "importPricingConditions";

	// flag for importing external conditions (valid for one item)
	public static final String IMPORT_ITEM_CONDITIONS               	= "importItemConditions";

	// control parameters for copying one item
	public static final String COPY_SOURCE_DOCUMENT_ID				= "copySourceDocumentId";
	public static final String COPY_SOURCE_ITEM_ID					= "copySourceItemId";
	public static final String COPY_PRICING_TYPE						= "copyPricingType";
	public static final String COPY_TYPE					        	= "copyType";
	public static final String COPY_SOURCE_SALES_QUANTITY_VALUE		= "copySourceSalesQuantityValue";
	public static final String COPY_SOURCE_SALES_QUANTITY_UNIT		= "copySourceSalesQuantityUnit";
	public static final String NETVALUE_ORIGS                       	= "netvalueOrigs";
	public static final String NETVALUE_NEWS                        	= "netvalueNews";

	// control parameters for copying multiple items
	public static final String COPY_SOURCE_DOCUMENT_IDS				= "copySourceDocumentIds";
	public static final String COPY_SOURCE_ITEM_IDS					= "copySourceItemIds";
	public static final String COPY_PRICING_TYPES					= "copyPricingTypes";
	public static final String COPY_TYPES			        		= "copyTypes";
	public static final String COPY_SOURCE_SALES_QUANTITY_VALUES		= "copySourceSalesQuantityValues";
	public static final String COPY_SOURCE_SALES_QUANTITY_UNITS		= "copySourceSalesQuantityUnits";
	public static final String NETVALUE_ORIG                      	= "netvalueOrig";
	public static final String NETVALUE_NEW                         	= "netvalueNew";

	// external item conditions to be imported
	public static final String IMPORT_CLIENTS								    = "importClients";
	public static final String IMPORT_DOCUMENT_IDS								= "importDocumentIds";
	public static final String IMPORT_ITEM_IDS									= "importItemIds";
	public static final String IMPORT_STEP_NUMBERS		            			= "importStepNumbers";
	public static final String IMPORT_CONDITION_COUNTERS						= "importConditionCounters";
	public static final String IMPORT_APPLICATIONS								= "importApplications";
	public static final String IMPORT_CONDITION_TYPES							= "importConditionTypes";
	public static final String IMPORT_PURPOSES									= "importPurposes";
	public static final String IMPORT_PRICING_DATES						    	= "importPricingDates";
	public static final String IMPORT_CALCULATION_TYPES					    	= "importCalculationTypes";
	public static final String IMPORT_CONDITION_BASE_VALUES					    = "importConditionBaseValues";
	public static final String IMPORT_CONDITION_RATES							= "importConditionRates";
	public static final String IMPORT_CONDITION_CURRENCIES						= "importConditionCurrencies";
	public static final String IMPORT_CONDITION_EXCHANGE_RATES					= "importConditionExchangeRates";
	public static final String IMPORT_CONDITION_PRICING_UNIT_VALUES		    	= "importConditionPricingUnitValues";
	public static final String IMPORT_CONDITION_PRICING_UNIT_UNITS				= "importConditionPricingUnitUnits";
	public static final String IMPORT_CONVERSION_NUMERATORS					    = "importConversionNumeratorsConditionUnitToBaseUnit";
	public static final String IMPORT_CONVERSION_DENOMINATORS					= "importConversionDenominatorsConditionUnitToBaseUnit";
	public static final String IMPORT_CONVERSION_EXPONENTS  					= "importConversionExponentsConditionUnitToBaseUnit";
	public static final String IMPORT_CONDITION_CATEGORIES						= "importConditionCategories";
	public static final String IMPORT_INDICATORS_STATISTICAL					= "importIndicatorsStatistical";
	public static final String IMPORT_SCALE_TYPES								= "importScaleTypes";
	public static final String IMPORT_INDICATORS_ACCRUALS						= "importIndicatorsAccruals";
	public static final String IMPORT_INDICATORS_INVOICE_LIST					= "importIndicatorsInvoiceList";
	public static final String IMPORT_CONDITION_ORIGINS						    = "importConditionOrigins";
	public static final String IMPORT_INDICATORS_GROUP_CONDITION				= "importIndicatorsGroupCondition";
	public static final String IMPORT_INDICATORS_CONDITION_UPDATE				= "importIndicatorsConditionUpdate";
	public static final String IMPORT_ACCESS_NUMBERS							= "importAccessNumbers";
	public static final String IMPORT_CONDITION_RECORD_NUMBERS					= "importConditionRecordNumbers";
	public static final String IMPORT_CONDITION_SEQUENTIAL_NUMBERS				= "importConditionSequentialNumbers";
	public static final String IMPORT_ACCOUNT_KEYS								= "importAccountKeys";
	public static final String IMPORT_ACCOUNT_KEYS_ACCRUALS_OR_PROVISIONS		= "importAccountKeysAccrualsOrProvisions";
	public static final String IMPORT_ROUNDING_OFF_DIFFERENCES					= "importRoundingOffDifferences";
	public static final String IMPORT_CONDITION_VALUES							= "importConditionValues";
	public static final String IMPORT_CONDITION_CONTROLS						= "importConditionControls";
	public static final String IMPORT_CONDITION_IS_INACTIVES					= "importConditionIsInactives";
	public static final String IMPORT_CONDITION_CLASSES						    = "importConditionClasses";
	public static final String IMPORT_HEADER_CONDITION_COUNTERS				    = "importHeaderConditionCounters";
	public static final String IMPORT_CONDITION_BASE_VALUE_FACTORS				= "importConditionBaseValueFactors";
	public static final String IMPORT_INDICATORS_SCALE_BASIS					= "importIndicatorsScaleBasis";
	public static final String IMPORT_SCALE_BASE_VALUES					    	= "importScaleBaseValues";
	public static final String IMPORT_SCALE_UNITS								= "importScaleUnits";
	public static final String IMPORT_SCALE_CURRENCIES							= "importScaleCurrencies";
	public static final String IMPORT_INDICATORS_CONDITION_INTER_COMPANY_BILLING= "importIndicatorsConditionInterCompanyBilling";
	public static final String IMPORT_INDICATORS_CONDITION_CONFIGURATION		= "importIndicatorsConditionConfiguration";
	public static final String IMPORT_INDICATORS_CONDITION_CHANGED_MANUALLY	    = "importIndicatorsConditionChangedManually";
	public static final String IMPORT_VARIANT_CONDITIONS						= "importVariantConditions";
	public static final String IMPORT_CONDITION_BASE_VALUE_FACTOR_PERIODS		= "importConditionBaseValueFactorPeriods";
	public static final String IMPORT_STRUCTURE_CONDITIONS	                	= "importIndicatorsStructureCondition";
	public static final String IMPORT_SALES_TAX_CODE   						    = "importSalesTaxCode";
	public static final String IMPORT_WITHHOLDING_TAX_CODE   					= "importWithholdingTaxCode";
	public static final String IMPORT_DATA_SOURCE              					= "importDataSources";
	public static final String IMPORT_CONDITION_TABLE_NAMES                       = "importConditionTableNames";
	public static final String IMPORT_ITEM_REF                                  = "importItemRefs";	
}
