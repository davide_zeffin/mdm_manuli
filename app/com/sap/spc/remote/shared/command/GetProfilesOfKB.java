package com.sap.spc.remote.shared.command;


/**
 * Returns information about (possibly filtered) knowledge bases. <b>Please note that in IPC 2.0c,
 * this command will fail if the session has not been initialized by CreateSession before.</b>  <p>
 *
 * <b>Import parameters</b>
 *
 * <ul>
 *     <li>objectName (optional):	filters by object name
 *     <li>objectType (optional):	filters by object type (kbrt.object_type string constants)
 * </ul><p>
 *
 * <b>Export parameters</b>
 *
 * <ul>
 *     <li>kbLogsys:			array of the logical systems that match the request
 *     <li>kbNames:				array of the knowledge base names that match the request
 *     <li>kbProfiles:			array of the kb profiles that match the request
 *     <li>kbVersions:			array of the kb versions that match the request
 *	   <li>rootInstanceNames:	array of the root instance names of each match
 * </ul><p>
 *
 * The command guarantees that kbName.length == version.length == profile.length.
 */

public interface GetProfilesOfKB extends SCECommand
{
	public static final String KB_LOGSYS			= "kbLogsys";
	public static final String KB_NAME				= "kbName";
	public static final String KB_VERSION			= "kbVersion";
	public static final String KB_PROFILE			= "kbProfile";
	public static final String ROOT_INSTANCE_NAME	= "rootInstanceName";
	public static final String UINAME				= "uiName";
	//output
	public static final String KB_PROFILES		= "kbProfiles";
	public static final String KB_ROOT_INSTANCE_NAMES = "rootInstanceNames";
	public static final String UINAMES				= "uiNames";
}