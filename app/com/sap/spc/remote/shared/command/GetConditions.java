/************************************************************************

	Copyright (c) 1999 by SAP AG

	All rights to both implementation and design are reserved.

	Use and copying of this software and preparation of derivative works
	based upon this software are not permitted.

	Distribution of this software is restricted. SAP does not make any
	warranty about the software, its performance or its conformity to any
	specification.

**************************************************************************/
package com.sap.spc.remote.shared.command;

public interface GetConditions {


	// output parameter
	public static final String CLIENTS									= "clients";
	public static final String DOCUMENT_IDS								= "documentIds";
	public static final String ITEM_IDS									= "itemIds";
	public static final String PROCEDURE_STEP_NUMBERS					= "procedureStepNumbers";
	public static final String CONDITION_COUNTERS						= "conditionCounters";
	public static final String APPLICATIONS								= "applications";
	public static final String USAGES									= "usages";
	public static final String CONDITION_TYPES							= "conditionTypes";
	public static final String PURPOSES									= "purposes";
	public static final String PRICING_DATES							= "pricingDates";
	public static final String CALCULATION_TYPES						= "calculationTypes";
	public static final String CONDITION_BASE_VALUES					= "conditionBaseValues";
	public static final String CONDITION_BASE_VALUES2					= "conditionBaseValues2";
	public static final String CONDITION_RATES							= "conditionRates";
	public static final String CONDITION_CURRENCIES						= "conditionCurrencies";
	public static final String CONDITION_CURRENCIES2					= "conditionCurrencies2";
	public static final String CONDITION_EXCHANGE_RATES					= "conditionExchangeRates";
	public static final String CONDITION_PRICING_UNIT_VALUES			= "conditionPricingUnitValues";
	public static final String CONDITION_PRICING_UNIT_UNITS				= "conditionPricingUnitUnits";
	public static final String CONVERSION_NUMERATORS					= "conversionNumeratorsConditionUnitToBaseUnit";
	public static final String CONVERSION_DENOMINATORS					= "conversionDenominatorsConditionUnitToBaseUnit";
	public static final String CONVERSION_EXPONENTS  					= "conversionExponentsConditionUnitToBaseUnit";
	public static final String CONDITION_CATEGORIES						= "conditionCategories";
	public static final String INDICATORS_STATISTICAL					= "indicatorsStatistical";
	public static final String SCALE_TYPES								= "scaleTypes";
	public static final String INDICATORS_ACCRUALS						= "indicatorsAccruals";
	public static final String INDICATORS_INVOICE_LIST					= "indicatorsInvoiceList";
	public static final String CONDITION_ORIGINS						= "conditionOrigins";
	public static final String INDICATORS_GROUP_CONDITION				= "indicatorsGroupCondition";
	public static final String INDICATORS_CONDITION_UPDATE				= "indicatorsConditionUpdate";
	public static final String ACCESS_NUMBERS							= "accessNumbers";
	public static final String CONDITION_RECORD_NUMBERS					= "conditionRecordNumbers";
	public static final String CONDITION_SEQUENTIAL_NUMBERS				= "conditionSequentialNumbers";
	public static final String ACCOUNT_KEYS								= "accountKeys";
	public static final String ACCOUNT_KEYS_ACCRUALS_OR_PROVISIONS		= "accountKeysAccrualsOrProvisions";
	public static final String ROUNDING_OFF_DIFFERENCES					= "roundingOffDifferences";
	public static final String CONDITION_VALUES							= "conditionValues";
	public static final String CONDITION_VALUES2						= "conditionValues2";
	public static final String CONDITION_CONTROLS						= "conditionControls";
	public static final String CONDITION_IS_INACTIVES					= "conditionIsInactives";
	public static final String CONDITION_CLASSES						= "conditionClasses";
	public static final String HEADER_CONDITION_COUNTERS				= "headerConditionCounters";
	public static final String CONDITION_BASE_VALUE_FACTORS				= "conditionBaseValueFactors";
	public static final String INDICATORS_SCALE_BASIS					= "indicatorsScaleBasis";
	public static final String SCALE_BASE_VALUES						= "scaleBaseValues";
	public static final String SCALE_UNITS								= "scaleUnits";
	public static final String SCALE_CURRENCIES							= "scaleCurrencies";
	public static final String INDICATORS_CONDITION_INTER_COMPANY_BILLING= "indicatorsConditionInterCompanyBilling";
	public static final String INDICATORS_CONDITION_CONFIGURATION		= "indicatorsConditionConfiguration";
	public static final String INDICATORS_CONDITION_CHANGED_MANUALLY	= "indicatorsConditionChangedManually";
	public static final String VARIANT_CONDITIONS						= "variantConditions";
	public static final String SALES_TAX_CODE   						= "salesTaxCode";
	public static final String WITHHOLDING_TAX_CODE   					= "withholdingTaxCode";
	public static final String INDICATORS_STRUCTURE_CONDITION			= "indicatorsStructureCondition";
	public static final String DATA_SOURCES                 			= "dataSources";
	public static final String CONDITION_BASE_VALUE_FACTOR_PERIODS		= "conditionBaseValueFactorPeriods";
	public static final String CONDITION_TABLE_NAMES						= "conditionTableNames";
        public static final String FIX_GROUP_FIX_RATES						= "fixGroupFixRates";

	// new export parameter for the ipc and online store
	public static final String DESCRIPTIONS								= "descriptions";
	public static final String DOCUMENT_CURRENCIES						= "documentCurrencies";

	public static final String GROUP_CONDITION_KEY_FORMULA_NOS          = "groupConditionKeyFormulaNos";
	public static final String MANUAL_ENTRY_FLAGS                       = "manualEntryFlags";
	public static final String CHANGE_OF_RATES_ALLOWED					= "changeOfRatesAllowed";
	public static final String CHANGE_OF_UNITS_ALLOWED					= "changeOfUnitsAllowed";
	public static final String CHANGE_OF_VALUES_ALLOWED					= "changeOfValuesAllowed";
	public static final String CHANGE_OF_CALCULATION_TYPES_ALLOWED		= "changeOfCalculationTypesAllowed";
	public static final String CHANGE_OF_CONVERSION_FACTORS_ALLOWED		= "changeOfConversionFactorsAllowed";

	public static final String IS_SWINGGUI						        = "isSwingGUI";
//	//For Swing GUI Only-------------------start
//	public static final String IS_SWINGGUI						        = "isSwingGUI";
//	public static final String IS_SUB_TOTAL_LINES                		= "isSubTotalLines";
//	public static final String COUNTERS                          		= "counters";
//	public static final String CONDITION_TYPE_NAMES                 	= "conditionTypeNames";
//	public static final String CONDITION_BASE_CURRENCY_NAMES			= "conditionBaseCurrencyNames";
//	public static final String CONDITION_BASE_CURRENCY_DESCS			= "conditionBaseCurrencyDescs";
//	public static final String CONDITION_RATES_CURRENCY_NAMES			= "conditionRatesCurrencyNames";
//	public static final String CONDITION_RATES_CURRENCY_DESCS			= "conditionRatesCurrencyDescs";
//	public static final String CONDITION_PRICING_UNIT_UNIT_NAMES		= "conditionPricingUnitUnitNames";
//	public static final String CONDITION_PRICING_UNIT_UNIT_DESCS        = "conditionPricingUnitUnitDescs";
//	public static final String CONDITION_CURRENCY_NAMES					= "conditionCurrencyNames";
//	public static final String CONDITION_CURRENCY_DESCS					= "conditionCurrencyDescs";
//    public static final String SCALE_CURRENCY_NAMES						= "scaleCurrencyNames";
//	public static final String SCALE_CURRENCY_DESCS						= "scaleCurrencyDescs";
//	public static final String CONDITION_VALUES_CURRENCY_NAMES			= "conditionValuesCurrencyNames";
//	public static final String CONDITION_VALUES_CURRENCY_DESCS			= "conditionValuesCurrencyDescs";
//	public static final String SCALE_BASE_FORMULA_NOS					= "scaleBaseFormulaNos";
//	public static final String CONDITION_BASE_FORMULA_NOS    			= "conditionBaseFormulaNos";
//	public static final String CONDITION_VALUE_FORMULA_NOS              = "conditionValueFormulaNo";
//	public static final String ROUNDING_DIFFERENCES                     = "roundingDifferences";
//	public static final String PRICING_CONDITION_TYPES                  = "pricingConditionTypes";
	public static final String PRICING_UNIT_DIMENSION_NAMES             = "pricingUnitDimensionNames";
//	public static final String INC_SCALES_BEGIN                         = "incScalesBegin";
//	public static final String INC_SCALES_END                           = "incScalesEnd";
//	public static final String VARIANT_CONDITION_FACTORS                = "variantConditionFactors";
	public static final String EXCHANGE_RATE_EXT_REPRESENTATIONS        = "exchangeRateExtRepresentations";
//	public static final String DIRECT_EXCHANGE_RATES                    = "directExchangeRates";
	public static final String DIRECT_EXCHANGE_RATE_EXT_REPRESENTATIONS = "directExchangeRateExtRepresentations";
	public static final String IS_DIRECT_EXCHANGE_RATES                 = "isDirectExchangeRates";
//	public static final String FACTORS                                  = "factors";
//	public static final String FRACTION_NUMERATORS                      = "fractionNumerators";
//	public static final String FRACTION_DENOMINATORS                    = "fractionDenominators";
//	public static final String CONDITION_FINDING_TIMESTAMPS             = "conditionFindingTimestamps";
//	public static final String HEADER_COUNTERS                          = "headerCounters";
//	public static final String TO_STRINGS                               = "toStrings";
//	public static final String STEP_NOS  								= "stepNos";
//	public static final String VARIANT_CONDITION_KEYS					= "variantConditionKeys";
//	public static final String VARIANT_CONDITION_DESCS					= "variantConditionDescs";
	public static final String DELETION_ALLOWED     					= "deletionAllowed";
//	//For Swing GUI-------------------end

	// flags for field filter (i.e. which fields are needed from condition)
	public static final String FIELD_FILTER_TYPE_DEFAULT    = "N"; // default: works as up to now
	public static final String FIELD_FILTER_TYPE_SWING_GUI  = "Y"; // fields needed for swing GUI
	public static final String FIELD_FILTER_TYPE_LEASING    = "A"; // fields needed for leasing

	// flag for suppressing invisible conditions (KINAK = 'Z')
	public static final String SUPPRESS_INVISIBLE           = "suppressInvisible";
}
