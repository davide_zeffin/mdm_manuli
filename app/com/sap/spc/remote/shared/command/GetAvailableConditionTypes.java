/************************************************************************

	Copyright (c) 1999 by SAP AG

	All rights to both implementation and design are reserved.

	Use and copying of this software and preparation of derivative works
	based upon this software are not permitted.

	Distribution of this software is restricted. SAP does not make any
	warranty about the software, its performance or its conformity to any
	specification.

**************************************************************************/
package com.sap.spc.remote.shared.command;

/**
 * Returns all condition types available for the document.<p>
 * 
 * <b>Import parameters</b>
 * 
 * <ul>
 *     <li>documentId (required):			
 *     <li>HeaderConditionsFlag (optional):		(default: false)
 * </ul><p>
 * 
 * <b>Export parameters</b>
 * <ul>
 *     <li>name:					the names (keys) of the Condition types 
 *     <li>description:				the descriptions of the Condition types
 * </ul>
 */
public interface GetAvailableConditionTypes
{
	//Input
	public static final String DOCUMENT_ID	= "documentId";
	public static final String HEADER_CONDITIONS_FLAG = "HeaderConditionsFlag";
	
	//output
	public static final String NAME = "name";
	public static final String DESCRIPTION = "description";	
	
	//constants
	public static final String HEADER_CONDITIONS_TRUE = "true";
	
}