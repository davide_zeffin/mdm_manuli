/*
 * Created on Sep 22, 2004
 *
 * To change the template for this generated file go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
package com.sap.spc.remote.shared.command;

/**
* Returns the Pricing Condition as an XML String
* 
*
* <b>Import parameters</b>
*
* <ul>
*     <li>documentId (required):			the document id of the item we're interested in
*     <li>includeSubtotals (optional):		Subtotals required or not: "Y" (Include subtotals) or "N" (Do not include subtotals) [Default "N"]
* </ul><p>
*
* <b>Export parameters</b>
* <ul>
*     <li>pricingConditionXMLString:	Pricing Condition as an XML string.
* </ul>
*/
public interface GetPricingConditionAsXML  {

// input parameter
public static final String DOCUMENT_ID								= "documentId";
public static final String INCLUDE_SUBTOTALS 						= "includeSubtotals";

// output parameters
public static final String PRICING_CONDITION_XML_STRING				= "pricingConditionXMLString";
}
