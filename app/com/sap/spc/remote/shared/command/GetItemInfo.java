/************************************************************************

	Copyright (c) 1999 by SAP AG

	All rights to both implementation and design are reserved.

	Use and copying of this software and preparation of derivative works
	based upon this software are not permitted.

	Distribution of this software is restricted. SAP does not make any
	warranty about the software, its performance or its conformity to any
	specification.

**************************************************************************/
package com.sap.spc.remote.shared.command;

/**
 * Returns general information about an item. For configuration info use GetConfigItemInfo,
 * for pricing info use GetPricingItemInfo.<p>
 *
 * <b>Import parameters</b>
 *
 * <ul>
 *     <li>documentId (required):	the document id of the item's document
 *     <li>itemId (required):		the item id of the item
 *     <li>formatValue (optional):	"Y"/"N" toggle: "Y": the values of the export parameters are converted to strings in an external and locale dependent representation (default value is "Y")
 * </ul><p>
 *
 * <b>Export parameters</b>
 * <ul>
 *     <li>productId:				the product id of the product that this item holds (external product id). If connected to a CRM system, numeric
 *                                  product ids will be returned as a 40 digit number (not as an 18 digit number).
 *     <li>productGuid:				the product guid of each item (internal product id)
 *     <li>productDescription:		descriptive text of the product (in the document's language)
 *     <li>salesQuantityValue:		value of the sales quantity (locale dependent representation if import parameter 'formatValue' is set)
 *     <li>salesQuantityUnit:		unit of the sales quantity (external and language dependent representation if import parameter 'formatValue' is set)
 *     <li>netWeightValue:			value of the product's net weight (locale dependent representation if import parameter 'formatValue' is set)
 *     <li>grossWeightValue:		value of the product's gross weight (locale dependent representation if import parameter 'formatValue' is set)
 *     <li>weightUnit:              unit of the product's net and gross weight (external and language dependent representation if import parameter 'formatValue' is set)
 *     <li>volumeValue:				value of the product's volume (locale dependent representation if import parameter 'formatValue' is set)
 *     <li>volumeUnit:				unit of the product's volume (external and language dependent representation if import parameter 'formatValue' is set)
 *     <li>baseQuantityValue:		value of the base quantity (locale dependent representation if import parameter 'formatValue' is set)
 *     <li>baseQuantityUnit:        unit of the base quantity (external and language dependent representation if import parameter 'formatValue' is set)
 *     <li>isConfigurable:			"Y"/"N" toggle: is the item configurable? An item is configurable if it is a KMAT
 *                                  or a product variant (which can be configured just like a KMAT, only that the first
 *                                  configuration-changing command will change it to its standard product)
 *     <li>isProductVariant:        "Y"/"N" toggle: is the item a product variant?
 *     <li>configHasChanged:		optional "Y"/"N" toggle: has the item's config changed. This value
 *                                  is only set when isConfigurable="Y".
 *     <li>subItemIds[]             array of the ids of all direct subitems of this item.
 *     <li>alternativeQuantityUnitUnits[]
 *									array of the units (unit names) of all alternative quantity units
 *									(external and language dependent representation if import parameter 'formatValue' is set)
 *     <li>alternativeQuantityUnitNumerators[]
 *									array of the numerators of all alternative quantity units
 *     <li>alternativeQuantityUnitDenominators[]
 *									array of the denominators of all alternative quantity units
 *     <li>alternativeQuantityUnitDexcriptions[]:
 *                                  the descriptions of the quantity units
 *	   <li>highLevelItemId			: returns the item id of this item's high level item
 *	   <li>isReturn					: returns the isReturn attribute of the item("Y" or "N")
 *     <li>isStatistical			: returns the isStatistical attribute of the item("Y" or "N")
 *     <li>performPricingAnalysis	: returns the performPricingAnalysis attribute of the item("Y" or "N")
 *	   <li>pricingItemIsUnchangable	: returns the pricingItemIsUnchangable attribute of the item("Y" or "N")
 *	   <li>serviceRendererTimestamp	: returns the service renderer timestamp of this item
 *	   <li>subItemRecursive[]		: returns the id's of item's subItems, id's of subItem's subItem and so on....
 *	   <li>bindingName[]			: returns the binding names of the item. Please note that a name can be returned multiple times for attributes that have multiple values.
 *	   <li>bindingValue[]			: returns the binding values corresponding to header binding names.
 *	   <li>itemVarcondName[]		: names of the item's variant conditions
 *     <li>itemVarcondDescription[]	: names of the item's variant condition description
 *     <li>itemVarcondFactor[]		: names of the item's variant condition factor
 *
 * </ul><p>
 *
 * The command guarantees that all alternativeQuantityUnit and itemBindingName and itemBindingValue arrays are of the same length.
 *
 * <p>Attribute bindings: as of IPC 5.0, there is no more distinction between header- and item-level attributes. Also, attributes
 * can now be multi valued. Therefore you can now get something like:
 * bindingName[1]=SALES_ORG &amp; bindingValue[1]=O 500001 &amp; bindingName[2]=SALES_ORG &amp; bindingValue[2]=O 500003.</p> 
 */

public interface GetItemInfo {
	// input parameter
	public static final String DOCUMENT_ID								= "documentId";
	public static final String ITEM_ID									= "itemId";
	public static final String FORMAT_VALUE								= "formatValue";

	// output parameter
	public static final String PRODUCT_ID   							= "productId";
	public static final String PRODUCT_GUID								= "productGuid";
	public static final String PRODUCT_DESCRIPTION						= "productDescription";
	public static final String SALES_QUANTITY_VALUE						= "salesQuantityValue";
	public static final String SALES_QUANTITY_UNIT						= "salesQuantityUnit";
	public static final String NET_WEIGHT_VALUE							= "netWeightValue";
	public static final String GROSS_WEIGHT_VALUE						= "grossWeightValue";
	public static final String WEIGHT_UNIT								= "weightUnit";
	public static final String VOLUME_VALUE								= "volumeValue";
	public static final String VOLUME_UNIT								= "volumeUnit";
	public static final String BASE_QUANTITY_VALUE						= "baseQuantityValue";
	public static final String BASE_QUANTITY_UNIT						= "baseQuantityUnit";
	public static final String ALT_QUANTITY_UNIT_UNITS					= "alternativeQuantityUnitUnits";
	public static final String ALT_QUANTITY_UNIT_NUMERATORS				= "alternativeQuantityUnitNumerators";
	public static final String ALT_QUANTITY_UNIT_DENOMINATORS			= "alternativeQuantityUnitDenominators";
	public static final String ALT_QUANTITY_UNIT_DESCRIPTIONS			= "alternativeQuantityUnitDescriptions";
	public static final String ALT_QUANTITY_UNIT_EXPONENTS				= "alternativeQuantityUnitExponents";
	public static final String ALT_QUANTITY_UNIT_GROSS_WEIGHTS			= "alternativeQuantityUnitGrossWeights";
	public static final String ALT_QUANTITY_UNIT_NET_WEIGHTS			= "alternativeQuantityUnitNetWeights";
	public static final String ALT_QUANTITY_UNIT_WEIGHT_UNITS			= "alternativeQuantityUnitWeightUnits";
	public static final String ALT_QUANTITY_UNIT_VOLUMES				= "alternativeQuantityUnitVolumes";
	public static final String ALT_QUANTITY_UNIT_VOLUME_UNITS			= "alternativeQuantityUnitVolumeUnits";
	public static final String ALT_QUANTITY_UNIT_IS_BASE_UNITS			= "alternativeQuantityUnitIsBaseUnits";
	public static final String IS_CONFIGURABLE							= "isConfigurable";
	public static final String IS_PRODUCT_VARIANT						= "isProductVariant";
	public static final String CONFIG_HAS_CHANGED						= "configHasChanged";
	public static final String SUB_ITEM_IDS								= "subItemIds";
	public static final String HIGH_LEVEL_ITEM_ID						= "highLevelItemId";
	public static final String IS_RETURN								= "isReturn";
	public static final String IS_STATISTICAL							= "isStatistical";
	public static final String PERFORM_PRICING_ANALYSIS					= "performPricingAnalysis";
	public static final String PRICING_ITEMS_IS_UNCHANGEABLE			= "pricingItemsIsUnchangable";
    public static final String CONDITION_ACCESS_TIMESTAMP_NAME          = "conditionAccessTimestampName";
    public static final String CONDITION_ACCESS_TIMESTAMP_VALUE         = "conditionAccessTimestampValue";
	public static final String SERVICE_RENDERER_TIMESTAMP				= "serviceRendererTimestamp";
	public static final String SUB_ITEM_RECURSIVE						= "subItemRecursive";

	public static final String BINDING_NAME = "bindingName";
    public static final String BINDING_VALUE = "bindingValue";
    public static final String ITEM_VARCOND_NAME = "itemVarcondName";
    public static final String ITEM_VARCOND_DESCRIPTION = "itemVarcondDescription";
    public static final String ITEM_VARCOND_FACTOR = "itemVarcondFactor";
    }
