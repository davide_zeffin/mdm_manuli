/************************************************************************

	Copyright (c) 1999 by SAP AG

	All rights to both implementation and design are reserved.

	Use and copying of this software and preparation of derivative works
	based upon this software are not permitted.

	Distribution of this software is restricted. SAP does not make any
	warranty about the software, its performance or its conformity to any
	specification.

**************************************************************************/

package com.sap.spc.remote.shared.command;


/**
 * This command issues a deletion request for an Assumption.
 * It is typically used in the context of conflict resolution to
 * delete one or several Assumptions in the sequence of triggering Assumptions
 * for a conflict.<p>
 *
 * <b>Import parameters</b>
 *
 * <ul>
 *     <li>documentId		(required):	the document id of the item being configured
 *     <li>itemId			(required):	the item id of the item being configured
 *     <li>or
 *     <li>configId         (required): the id of the configuration
 *
 *     <li>assumptionId			(required):	the id of the assumption we're interested in
 * </ul>
 *
 * <ul>
 *     <li>isAssumptionDeleted:	the result ("Y"/"N" toggle). "Y": the assumption was deleted.
 * </ul>
 */
public interface DeleteConflictingAssumption extends SCECommand
{
	public static final String ASSUMPTION_ID			= "assumptionId";
	public static final String IS_ASSUMPTION_DELETED    = "isAssumptionDeleted";
}