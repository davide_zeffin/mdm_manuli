/*******************************************************************************

    Copyright (c) 2005 by SAP AG

    All rights to both implementation and design are reserved.

    Use and copying of this software and preparation of derivative works based
    upon this software are not permitted.

    Distribution of this software is restricted. SAP does not make any warranty
    about the software, its performance or its conformity to any specification.

*******************************************************************************/
package com.sap.spc.remote.shared.command;

/**
 * Returns the external fieldnames from internal fieldnames which are given in array of fields 
 * (names, externalValues).<p>
 *
 * <b>Import parameters</b>
 *
 * <ul>
 *		<li>application        (required):	    application
 *		<li>usage              (required):   	usage 
 *		<li>isApplicationField (required):   	flag if the fields are application fields or not
 *		<li>intFieldname       (required):   	the array of external values (which needs to be converted)
 * </ul><p>
 *
 * <b>Export parameters</b>
 * <ul>
 *		<li>intFieldname:  				the array of internal fieldnames
 *      <li>extFieldname:				the array of external fieldnames which belongs to 
 * 										the internal names. if there are multiple external names
 * 										for the one internal name, the external names are concatenated
 * 										with the separator '@$#' between  
 * </ul><p>
 *
 */
public interface ConvertFieldnameInternalToExternal {

	public static String USAGE = "usage";
	public static String APPLICATION = "application";
	public static String IS_APPLICATION_FIELD = "isApplicationField";
	public static String IS_APP_FIELD_TRUE = "Y";
	public static String IS_APP_FIELD_FALSE = "N";		
	public static String INT_FIELDNAME = "intFieldname";
	public static String EXT_FIELDNAME = "extFieldname";
	public static String SEPARATOR_EXT_FIELD = "@$#";


}
