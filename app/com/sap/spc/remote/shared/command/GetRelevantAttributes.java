/************************************************************************

	Copyright (c) 1999 by SAP AG

	All rights to both implementation and design are reserved.

	Use and copying of this software and preparation of derivative works
	based upon this software are not permitted.

	Distribution of this software is restricted. SAP does not make any
	warranty about the software, its performance or its conformity to any
	specification.

**************************************************************************/
package com.sap.spc.remote.shared.command;

/**
 * return the header and item attribute names that are required for the search of condition
 * records. The result depends on the procedure (customizing data) employed or a list of condition types
 * provided by caller.
 *
 * <b>Import parameters</b>
 *
 * <ul>
 *		<li>procedureName (optional):	the name of the procedure
 *                                      - either procedure name or condition types have to be provided
 *		<li>conditionTypes (optional): an array of condition types
 *		<li>stepNumbers (optional):	step numbers of the provided condition types
 *		<li>counters (optional):	counters of the provided condition types
 *		<li>application (optional):		the name of the application. Default value is "CRM".
 *		<li>usage (optional):		the name of the usage. Default value is pricing "PR".
 * </ul>
 *
 * <b>Export parameters</b>
 *
 * <ul>
 *     <li>headerAttributeNames:		an array of the header attribute names
 *     <li>itemAttributeNames:			an array of the item attribute names
 *     <li>conditionAccessTimestampNames an array of the condition access timestamp names
 * </ul>
 */

public interface GetRelevantAttributes {
	// input parameter
	public static final String PROCEDURE_NAME							= "procedureName";
	public static final String CONDITION_TYPE_NAMES						= "conditionTypeNames";
	public static final String STEP_NUMBERS     						= "stepNumbers";
	public static final String COUNTERS     						    = "counters";
	public static final String APPLICATION								= "application";
	public static final String USAGE    								= "usage";

	// output parameter
	public static final String HEADER_ATTRIBUTE_NAMES					= "headerAttributeNames";
	public static final String ITEM_ATTRIBUTE_NAMES						= "itemAttributeNames";
    public static final String CONDITION_ACCESS_TIMESTAMP_NAMES         = "conditionAccessTimestampNames";
}
