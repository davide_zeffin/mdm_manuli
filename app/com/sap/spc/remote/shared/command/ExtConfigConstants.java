package com.sap.spc.remote.shared.command;

/**
 * This interface holds constants of the names used for external
 * configuration data.  The constants are used by the commands
 * GetConfigItemInfo and CreateItem. The parameter names defined
 * here can be changed for each client using a ParameterNameConverter.
 *
 * @see com.sap.spc.remote.ParameterNameConverter
 * @see CreateItem
 * @see GetConfigItemInfo
 */
public interface ExtConfigConstants
{
	public final static String EXT_CONFIG_POSITION		= "extConfigPositions";	
	public final static String EXT_CONFIG_ID			= "extConfigIds";
	public final static String EXT_CONFIG_ROOT_ID		= "extConfigRootIds";
	public final static String EXT_CONFIG_SCE_MODE		= "extConfigSceModes";
	public final static String EXT_CONFIG_KB_LOGSYS		= "extConfigKBLogsys";
	public final static String EXT_CONFIG_KB_NAME		= "extConfigKBNames";
	public final static String EXT_CONFIG_KB_VERSION	= "extConfigKBVersions";
	public final static String EXT_CONFIG_KB_PROFILE	= "extConfigKBProfiles";
	public final static String EXT_CONFIG_KB_BUILD		= "extConfigKBBuilds";
	public final static String EXT_CONFIG_KB_LANGUAGE	= "extConfigKBLanguages";
	public final static String EXT_CONFIG_NAME			= "extConfigNames";		
	public final static String EXT_CONFIG_INFO			= "extConfigInfos";		
	public final static String EXT_CONFIG_COMPLETE		= "extConfigCompletes";
	public final static String EXT_CONFIG_CONSISTENT	= "extConfigConsistents";		
	public final static String EXT_CONFIG_SCE_VERSION	= "extConfigSceVersions";			

	public final static String EXT_PART_CONFIG_ID		= "extPartConfigIds";	
	public final static String EXT_PART_PARENT_ID		= "extPartParentIds";
	public final static String EXT_PART_INST_ID			= "extPartInstIds";
	public final static String EXT_PART_POS_NR			= "extPartPosNrs";
	public final static String EXT_PART_OBJECT_TYPE		= "extPartObjectTypes";
	public final static String EXT_PART_CLASS_TYPE		= "extPartClassTypes";
	public final static String EXT_PART_OBJECT_KEY		= "extPartObjectKeys";
	public final static String EXT_PART_AUTHOR			= "extPartAuthors";	
	public final static String EXT_PART_SALES_RELEVANT  = "extPartSalesRelevants";
	
	public static final String EXT_INST_CONFIG_ID		= "extInstConfigIds";
	public static final String EXT_INST_ID				= "extInstIds";
	public static final String EXT_INST_OBJECT_TYPE		= "extInstObjectTypes";
	public static final String EXT_INST_CLASS_TYPE		= "extInstClassTypes";
	public static final String EXT_INST_OBJECT_KEY		= "extInstObjectKeys";
	public static final String EXT_INST_OBJECT_TEXT		= "extInstObjectTexts";
	public static final String EXT_INST_QUANTITY		= "extInstQuantities";	
	public static final String EXT_INST_QUANTITY_UNIT	= "extInstQuantityUnits";	
	public static final String EXT_INST_AUTHOR			= "extInstAuthors";
	public static final String EXT_INST_COMPLETE		= "extInstCompletes";
	public static final String EXT_INST_CONSISTENT		= "extInstConsistents";
	
	public final static String EXT_VALUE_CONFIG_ID		= "extValueConfigIds";	
	public final static String EXT_VALUE_INSTANCE_ID	= "extValueInstIds";
	public final static String EXT_VALUE_CSTIC_NAME		= "extValueCsticNames";
	public final static String EXT_VALUE_CSTIC_LNAME	= "extValueCsticLNames";
	public final static String EXT_VALUE_NAME			= "extValueNames";
	public final static String EXT_VALUE_LNAME			= "extValueLNames";
	public final static String EXT_VALUE_AUTHOR			= "extValueAuthors";	
	
	public final static String EXT_PRICE_CONFIG_ID		= "extPriceConfigIds";
	public final static String EXT_PRICE_INSTANCE_ID	= "extPriceInstanceIds";
	public final static String EXT_PRICE_FACTOR			= "extPriceFactors";
	public final static String EXT_PRICE_KEY			= "extPriceKeys";
	
	public final static String EXT_POS_CONFIG_ID		= "extPosConfigIds";
	public final static String EXT_POS_INST_ID			= "extPosInstIds";
	public final static String EXT_POS_ITEM_ID			= "extPosItemIds";
}
