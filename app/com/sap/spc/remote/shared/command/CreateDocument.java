/************************************************************************

	Copyright (c) 1999 by SAP AG

	All rights to both implementation and design are reserved.

	Use and copying of this software and preparation of derivative works
	based upon this software are not permitted.

	Distribution of this software is restricted. SAP does not make any
	warranty about the software, its performance or its conformity to any
	specification.

**************************************************************************/
package com.sap.spc.remote.shared.command;

/**
 * Creates a new (sales) document. <b>Please note that in IPC 2.0c, no document may be created if the
 * session has not initialized with CreateSession before.</b>  <p>
 *
 * <b>Import parameters</b>
 *
 * <ul>
 *		<li>businessObjectType (optional):		the name of the related business object type - refer to SAP Business Object Repository (BOR)
 *		<li>usageName (optional):       		the name of the usage hosting the document (only usages derived from pricing are allowed)
 *		<li>onlySpecifiedUsage (optional):		access for condition records should only take place for specified usage. Value = Y or N
 *		<li>applicationName (optional):			the name of the application hosting the document
 *		<li>authorityForDisplay (optional):		a string containing an authority level (integer number) for displaying conditions
 *		<li>authorityForEdit (optional):		a string containing an authority level (integer number) for editing conditions
 *		<li>procedureName:						the name of the pricing procedure
 *		<li>documentCurrencyUnit:				the document currency unit
 *		<li>localCurrencyUnit:					the local currency unit
 *		<li>language (optional):				the language of the document (ISO code, two places)
 *		<li>country (optional):					the country (ISO code, two places)
 *		<li>dateFormat (optional):				the format of the date
 *		<li>decimalSeparator (optional):		the character used for decimal sign (default: depends on language and country)
 *		<li>groupingSeparator (optional):		the character used for thousands separator (default: depends on language and country)
 *      <li>salesOrganisation (optional):		the sales organization (for specifying the sales data of the products and for condition finding)
 *      <li>distributionChannel (optional):		the distribution channel (for specifying the sales data of the products and for condition finding)
 *      <li>groupConditionProcessing (optional):"Y"/"N" toggle: if "Y", this document will always perform group condition processing. Defaults to "Y".
 *		<li>editMode (optional):            	'A' for read-write, 'B' for read-only
 *		<li>partialProcessing (optional):      	"Y"/"N" toggle: if "Y", this document is in partial processing mode. Defaults to "N".
 *		<li>keepZeroPrices (optional):			"Y"/"N" toggle: if "Y", zero price conditions are not set to inactive. Defaults to "N".
 *		<li>headerAttributeNames (optional):	an array of header attribute names
 *		<li>headerAttributeValues (optional):	the corresponding array of header attribute values
 *		<li>unitToBeRoundedTo (optional):		the unit to be rounded, e.g. 5 for the so-called 5-Rappen-rounding in swisse. Should be 0 if no rounding should be performed. Defaults to 0.
 *		<li>expiringCheckDate (optional):		the date that will be used to check if a currency is expired
 *      <li>dataKeys (optional):				an array of keys for additional data
 *      <li>dataValues (optional):				the corresponding array of values for additional data
 *      <li>readPricingDocumentFromDb (optional):"Y"/"N" toggle: "Y": Loads the pricing document for this document from the database. "N": creates a pricing document and performs pricing. Defaults to "N".
 *      <li>useDocumentId (optional):			the document id to be used for the new document. If a document with that id already exists, no operation will be performed.
 *                                              Particularly, the document will <em>not</em> change its properties to the values provided with this command. Use ChangeDocument for this purpose.
 * </ul>
 *
 * <b>Export parameters</b>
 *
 *
 * <ul>
 *     <li>documentId:							the document id of the new document
 * </ul>
 * <p>
 * This command supports client-specific parameter naming for all input parameters.
 */

public interface CreateDocument {
	// input parameter
	public static final String BUSINESS_OBJECT_TYPE						= "businessObjectType";
	public static final String USAGE_NAME								= "usage";
	public static final String ONLY_SPECIFIED_USAGE						= "onlySpecifiedUsage";
	public static final String APPLICATION_NAME							= "application";
	public static final String AUTHORITY_FOR_DISPLAY					= "authorityForDisplay";
	public static final String AUTHORITY_FOR_EDIT						= "authorityForEdit";
	public static final String PROCEDURE_NAME							= "procedureName";
	public static final String DOCUMENT_CURRENCY_UNIT					= "documentCurrencyUnit";
	public static final String LOCAL_CURRENCY_UNIT						= "localCurrencyUnit";
	public static final String LANGUAGE									= "language";
	public static final String COUNTRY									= "country";
	public static final String DECIMAL_SEPARATOR						= "decimalSeparator";
	public static final String GROUPING_SEPARATOR						= "groupingSeparator";
    public static final String DATE_FORMAT                              = "dateFormat";
	public static final String SALES_ORGANISATION						= "salesOrganisation";
	public static final String DISTRIBUTION_CHANNEL						= "distributionChannel";
	public static final String EDIT_MODE         						= "editMode";
	public static final String PARTIAL_PROCESSING  						= "partialProcessing";
	public static final String KEEP_ZERO_PRICES  						= "keepZeroPrices";
	public static final String HEADER_ATTRIBUTE_NAMES					= "headerAttributeNames";
	public static final String HEADER_ATTRIBUTE_VALUES					= "headerAttributeValues";
	public static final String GROUP_CONDITION_PROCESSING				= "groupConditionProcessing";
	public static final String UNIT_TO_BE_ROUNDED_TO					= "unitToBeRoundedTo";
	public static final String EXPIRING_CHECK_DATE  					= "expiringCheckDate";
	public static final String ADDITIONAL_DATA_KEYS						= "dataKeys";
	public static final String ADDITIONAL_DATA_VALUES					= "dataValues";
	public static final String READ_PRICING_DOCUMENT_FROM_DATABASE		= "readPricingDocumentFromDb";
	public static final String USE_DOCUMENT_ID                          = "useDocumentId";

	// output parameter
	public static final String DOCUMENT_ID								= "documentId";
}
