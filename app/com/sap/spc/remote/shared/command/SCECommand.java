package com.sap.spc.remote.shared.command;


/**
 * A base interface used by all SCE commands.
 */
public interface SCECommand
{
	//Misc constants
	public static final String WEBROOT			= "WEBROOT";
	public static final String CONFIG_ID        = "configId";

	public static final String DOCUMENT_ID		= "documentId";
	public static final String ITEM_ID			= "itemId";

	public static final String SCE_MODE			= "1";

	public static final String YES				= "Y";
	public static final String NO				= "N";

	public static final String TRUE				= "T";
	public static final String FALSE			= "F";

	public static final String USER				= "U";
	public static final String SYSTEM			= "S";

	public static final String SALES_RELEVANT   = "X";
	static final String IPC_DOCUMENT_ROOT           = "ipc/mimes/documents/";
}
