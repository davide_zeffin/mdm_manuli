/************************************************************************

	Copyright (c) 1999 by SAP AG

	All rights to both implementation and design are reserved.

	Use and copying of this software and preparation of derivative works
	based upon this software are not permitted.

	Distribution of this software is restricted. SAP does not make any
	warranty about the software, its performance or its conformity to any
	specification.

**************************************************************************/

package com.sap.spc.remote.shared.command;


/**
 * Delete values of several cstics. If the (one/some of the) values can not be deleted, the server
 * will just not delete them without informing the client about that problem.<p>
 *
 * If the instance of the given id is not found in the configuration, the command returns
 * a corresponding error message (identified data not found) and doesn't change the
 * configuration. The command checks and sets the values one by one, ie. if the n-th characteristic
 * in the list is not found, an error will will be returned, but the first n-1 characteristic
 * values have been set.<p>
 *
 * <b>Import parameters</b>
 *
 * <ul>
 *     <li>documentId (required):	the document id of the item being configured
 *     <li>itemId (required):		the item id of the item being configured
 *     <li>instId (required):		the instance id the cstic belongs to
 *     <li>csticNames[] (required):	the (language independent) names of the cstics
 *     <li>valueNames[] (optional): the values to delete.
 *	   <li>formatValue (optional):	"Y"/"N" toggle (default "N"). If "Y", the values are treated as
 *									language/locale dependent values.
 *     <li>formatValues[] (optional): "Y"/"N" toggle: the same as formatValue, but it allows to
 *         specify this flag for each value. This makes sense especially to differenciate between
 *         additional values and other values. Default: "N".
 * </ul><p>
 *
 * <b>Export parameters</b>
 * <p>
 * none.
 * <p>
 */

public interface DeleteCsticsValues extends SCECommand
{
	public static final String INST_ID = "instId";
	public static final String CSTIC_NAMES = "csticNames";
	public static final String VALUE_NAMES = "valueNames";
 	public static final String FORMAT_VALUE = "formatValue";
 	public static final String FORMAT_VALUES = "formatValues";
}
