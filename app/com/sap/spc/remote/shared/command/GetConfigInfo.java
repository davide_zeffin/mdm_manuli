/************************************************************************

	Copyright (c) 1999 by SAP AG

	All rights to both implementation and design are reserved.

	Use and copying of this software and preparation of derivative works
	based upon this software are not permitted.

	Distribution of this software is restricted. SAP does not make any
	warranty about the software, its performance or its conformity to any
	specification.

**************************************************************************/

package com.sap.spc.remote.shared.command;


/**
 * Returns header information about a configuration.  <p>
 *
 * <b>Import parameters</b>
 *
 * <ul>
 *     <li>documentId (required):	the document id of the item being configured
 *     <li>itemId (required):		the item id of the item being configured
 *     <li>getContext (optional):   "Y"/"N" toggle: return context data, too. Default: "N"
 * </ul><p>
 *
 * <b>Export parameters</b>
 *
 * <ul>
 *     <li>configurationId: the configuration id of this config. This id identifies this configuration
 *                          within its session and can be used by all SCECommand subclasses. It has
 *                          no relation with GetConfigItemInfo's config ids!
 *     <li>rootInstanceId:	the uid of the root instance of this config. This id can later be used to
 *                          retrieve characteristics or other information of the instance.
 *     <li>sceMode:			"1"/"" toggle: does this config use SCE mode (1) (or VC mode: "")
 *	   <li>isComplete:      "T"/"F" toggle: is the config complete?
 *     <li>isConsistent:    "T"/"F" toggle: is this config consistent?
 *     <li>isSingleLevel:	"T"/"F" toggle: is this config single level? (or multi level)
 *     <li>contextNames:    array of context names, will only be returned if getContext is "Y"
 *     <li>contextValues:   array of context values, will only be returned if getContext is "Y"
 * </ul><p>
 */

public interface GetConfigInfo extends SCECommand
{
// import
	public static final String GET_CONTEXT      = "getContext";

// export
	public static final String CONFIGURATION_ID = "configurationId";
	public static final String ROOT_INSTANCE_ID = "rootInstanceId";
	public static final String SCE_MODE         = "sceMode";
	public static final String IS_COMPLETE      = "isComplete";
	public static final String IS_CONSISTENT    = "isConsistent";
	public static final String IS_SINGLELEVEL   = "isSingleLevel";
	public static final String KB_LOGSYS        = "kbLogsys";
	public static final String KB_NAME	    = "kbName";
	public static final String KB_VERSION	    = "kbVersion";
	public static final String KB_PROFILE	    = "kbProfile";
	public static final String KB_BUILD_NUMBER  = "kbBuildNumber";
	public static final String CONTEXT_NAMES    = "contextNames";
	public static final String CONTEXT_VALUES   = "contextValues";
}