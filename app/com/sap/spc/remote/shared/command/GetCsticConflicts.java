package com.sap.spc.remote.shared.command;

/**
 * Returns an array of conflicts.  If the optional instId parameter is defined,
 * it returns only those conflicts that this instance is involved in, else all
 * existing conflicts are returned.<p>
 *
 * <b>Import parameters</b>
 *
 * <ul>
 *     <li>documentId (required):	the document id of the item being configured
 *     <li>itemId (required):		the item id of the item being configured
 *     <li>instId (optional):		the instance id of the instance
 *     <li>csticName (optional):    the name of the characteristic
 *     <li>level (optional):        the level of explanation search; can be the number
 *                                  0 or 1.
 *                                  0: only the description of the conflict type is delivered
 *                                  1: in addition the dependency explanation is delivered
 *                                  The default value is 1.
 *     <li>blockTag (optional):     which tag should be used to mark the start
 *                                  of a new block in the conflict text
 *                                  "ul" is the defauilt value
 *     <li>lineTag (optional):     which tag should be used to mark the start
 *                                  of a new line in a block of conflict text
 *                                  "li" is the defauilt value
 *
 * </ul><p>
 *
 * <b>Export parameters</b>
 *
 * <ul>
 *     <li>conflict_inst_ids[]:		    the instance ids for each conflict
 *     <li>conflict_cstic_names[]:       the characteristic names
 *     <li>conflict_cstic_lnames[]:     the language dependent name of the characteristic
 *     <li>conflict_long_texts[]:		the long text description of a conflict (for dependency)
 * </ul>
 *
 * The command guarantees that all returned arrays are of equal length
 */

public interface GetCsticConflicts extends SCECommand{
//  parameter
	public static final String INST_ID		= "instId";
	public static final String CSTIC_NAME   = "csticName";
	public static final String VIEW         = "view";
	public static final String LEVEL        = "level";
	public static final String BLOCK_TAG    = "blockTag";
	public static final String LINE_TAG     = "lineTag";
	public static final String VALUE_LAYOUT_TAG  = "valueLayoutTag";
	public static final String CONFLICT_INST_IDS    = "i";
	public static final String CONFLICT_CSTIC_NAMES = "c";
	public static final String CONFLICT_CSTIC_LNAMES = "cln";
	public static final String CONFLICT_TEXTS	    = "t";
	public static final String CONFLICT_LONG_TEXTS  = "lt"; // 20011121-ak
//  Text placeholders
	public static final String NO_CONFLICT_TEXT     = "&NO_CONFLICT_TEXT&";
	public static final String IS_DEFAULT           = "&IS_DEFAULT&";
	public static final String IS_USER_CREATED      =   "&IS_USER_CREATED&";
	public static final String INFERRED_BY_SYSTEM   =   "&INFERRED_BY_SYSTEM&";
	public static final String REQUIRED_BY_USER     =   "&REQUIRED_BY_USER&";
	public static final String EXCLUDED_BY_USER     =   "&EXCLUDED_BY_USER&";
	public static final String REQUIRED_STATICALLY  =   "&REQUIRED_STATICALLY&";
	//conflict types
	public static final String CONFLICT_CONSTRAINED = "conflict_constrained";
	public static final String CONFLICT_VALUE       = "conflict_value";
	public static final String CONFLICT_RESTRICTED_DOMAIN = "conflict_restricted_domain";
	public static final String CONFLICT_STATIC_DOMAIN = "conflict_static_domain";
	public static final String CONFLICT_EXCLUDED_VALUE = "conflict_excluded_value";
	public static final String CONFLICT_NIL_VALUE = "conflict_nil_value";
	public static final String CONFLICT_NIL_REQUIRED = "conflict_nil_required";
	public static final String CONFLICT_OTHER = "conflict_other";
	//functional characters
	public static final String CONFLICT_TYPE_BEGIN = "<CONFLICT_TYPE>";
	public static final String CONFLICT_TYPE_END = "</CONFLICT_TYPE>";
	public static final String CONFLICT_TEXT_BEGIN = "<CONFLICT_TEXT>";
	public static final String CONFLICT_TEXT_END = "</CONFLICT_TEXT>";
	//UI-tags
	public static final String UL_BEGIN      = "<ul>";
	public static final String UL_END        = "</ul>";
	public static final String LI_BEGIN       = "<li>";
	public static final String LI_END         = "</li>";
//	public static final String B_BEGIN      ="<b>";
//	public static final String B_END        ="</b>";

}