package com.sap.spc.remote.shared.command;
/**
 * Sets the initial item configuration. This can be used for 
 * subsequent comparison with the current item configuration  <p>
 *
 * <b>Import parameters</b>
 *
 * <ul>
 *     <li>DOCUMENT_ID (optional):	the document id 
 *     <li>ITEM_ID (optional): 	    the item id 
 *     <li>CONFIG_ID (optional):	the configuration id 
 *     
 * </ul><p>
 * 
 * <b>Export parameters</b>
 * 
 *  <ul>
 *    <li>EXT_CONFIG params:	             the initial configuration
 * </ul><p> 
 * 
 *
 */

public interface IGetInitialConfig {
//	import

// export
	public static final String EXT_CONFIG_P	= "EXT_CONFIG";

}
