/************************************************************************

	Copyright (c) 1999 by SAP AG

	All rights to both implementation and design are reserved.

	Use and copying of this software and preparation of derivative works
	based upon this software are not permitted.

	Distribution of this software is restricted. SAP does not make any
	warranty about the software, its performance or its conformity to any
	specification.

**************************************************************************/

package com.sap.spc.remote.shared.command;

/**
 * Creates one or more new items, adds them to the specified document and performs pricing for the items.
 * For details on identifying products please cf. the documentation of CreateItem.
 * <p>
 *
 * <b>Import parameters</b>
 *
 * <ul>
 *     <li>documentId (required):				the id of the document that will contain the items
 *     <li>suppressPricing (optional):	        suppresses the pricing after the creation of the items ("Y"/"N" toggle, default value is "N")
 *     <li>productGuids (optional):				an array of internal product ids identifying the products that are associated with the items
 *     <li>productIds (optional):				an array of external product ids identifying the products that are associated with the items.
 *												If productGuids are supplied, all productIds will be ignored.
 *     <li>productLogSys (optional):			an array of logical system ids.
 *     <li>productTypes (optional):				an array of product types.
 *     <li>departureCountries (optional):		an array of departure countries for specifying the tax classifications of the product (if departure country is not set: the tax classifications of the product won't be found)
 *     <li>departureRegions (optional):			an array of departure regions for specifying the tax classifications of the product (if departure country is not set: the tax classifications of the product won't be found)
 *     <li>exchangeRateTypes	(optional):		an array of types of the exchange rate to employed
 *     <li>exchangeRateDates (optional):		an array of dates the exchange rate employed must be valid (format: YYYYMMDD, default: pricing date)
 *     <li>externalExchangeRates (optional):	an array of external rates of the exchange rate to employed
 *     <li>pricingDates (optional):				an array of pricing dates of the items (default: current date)
 *     <li>conditionAccessTimestampNames;       names of dates for condition access
 *     <li>conditionAccessTimestampValues;      values of dates for condition access
 *	   <li>conditionAccessTimestampRefs (optional):	the corresponding array of conditionAccessTimestamp references
 *	   <li>serviceRenderDates (optional):		deprecated, use conditionAccessTimestampNames and conditionAccessTimestampValues instead
 *     <li>billingDates (optional):				deprecated, use conditionAccessTimestampNames and conditionAccessTimestampValues instead
 *     <li>creationDates (optional):			deprecated, use conditionAccessTimestampNames and conditionAccessTimestampValues instead
 *     <li>documentDates (optional):    		deprecated, use conditionAccessTimestampNames and conditionAccessTimestampValues instead
 *     <li>scaleBaseDates (optional):			an array of timestamps used as scale base (scale base type TC1)
 *     <li>scalePeriodBegins (optional):		an array of timestamps defining the begin of the period which is used to calculate scale base (scale base type M/N/O/P)
 *     <li>scalePeriodEnds (optional):			an array of timestamps defining the end of the period which is used to calculate scale base (scale base type M/N/O/P)
 *     <li>scaleNumbersOfDaysInAMonth (optional): deprecated, use numbersOfDaysInAMonth instead
 *     <li>scaleNumbersOfDaysInAYear (optional): deprecated, use numbersOfDaysInAYear instead
 *     <li>scaleBaseNumbersOfDays (optional):	an array of numbers of days used as scale base (scale base type O)
 *     <li>scaleBaseNumbersOfWeeks (optional):	an array of numbers of weeks used as scale base (scale base type P)
 *     <li>scaleBaseNumbersOfMonths (optional):	an array of numbers of months used as scale base (scale base type M)
 *     <li>scaleBaseNumbersOfYears (optional):	an array of numbers of years used as scale base (scale base type N)
 *     <li>conditionBasePeriodBegins (optional):an array of timestamps used to calculate condition base (calcluation type M/N/O/P)
 *     <li>conditionBasePeriodEnds (optional):	an array of timestamps used to calculate condition base (calcluation base type M/N/O/P)
 *     <li>numbersOfDaysInAMonth (optional):    an array of numbers of days in a month in the current period which is used to calculate condition and scale base (type M/N/O/P)
 *     <li>numbersOfDaysInAYear (optional):	    an array of numbers of days in a year in the current period which is used to calculate condition and scale base (type M/N/O/P)
 *     <li>conditionBaseNumbersOfDays (optional):an array of numbers of days used as condition base (calcluation base type O)
 *     <li>conditionBaseNumbersOfWeeks (optional):	an array of numbers of weeks used as condition base (calcluation base type P)
 *     <li>conditionBaseNumbersOfMonths (optional):	an array of numbers of months used as condition base (calcluation base type M)
 *     <li>conditionBaseNumbersOfYears (optional):	an array of numbers of years used as condition base (calcluation base type N)
 *     <li>salesQuantityValues (optional):		an array of sales quantity values of the items (default: value: 1, unit: sales unit of product). Internal presentation
 *     <li>salesQuantityUnits (optional):		an array of sales quantity units (name) of the items (default: value: 1, unit: sales unit of product). Internal presentation
 *     <li>quantityRatioNumerators(optional):	an array of numerators: defines the sales to base quantity ratio.
 *     <li>quantityRatioDenominators(optional):	an array of denominators: defines the sales to base quantity ratio.
 *     <li>grossWeightValues (optional):		an array of gross weight values of the items. see import parameter 'formatValues'
 *     <li>netWeightValues (optional):			an array of net weight values of the items. see import parameter 'formatValues'
 *     <li>weightUnits (optional):				an array of weight units (name) of the items. see import parameter 'formatValues'
 *     <li>volumeValues (optional):				an array of volume values of the items. see import parameter 'formatValues'
 *     <li>volumeUnits (optional):				an array of volume units (name) of the items. see import parameter 'formatValues'
 *     <li>salesOrganisations (optional):		an array of sales organizations for specifying the sales data of the product
 *     <li>distributionChannels (optional):		an array of distribution channels for specifying the sales data of the product
 *     <li>headerAttributeNames (optional):		an array of header attribute names for all items
 *	   <li>headerAttributeValues (optional):	the corresponding array of header attribute values
 *	   <li>headerAttributeItemRefs (optional):  the corresponding array of item references. The item reference is a counter that starts with 1 for the first new item
 *     <li>itemAttributeNames (optional):		an array of item attribute names for all items
 *	   <li>itemAttributeValues (optional):		the corresponding array of item attribute values
 *	   <li>itemAttributeItemRefs (optional):	the corresponding array of item references
 *     <li>kbLogsys (optional):					the logical systems where the knowledge bases have been maintained
 *     <li>kbNames (optional):					the knowledge base name of each item (default: the last name found); each may be the empty string
 *	   <li>kbVersions (optional):				the knowledge base version of each item (default: the last version found); each may be the empty string
 *     <li>kbProfiles (optional):				the knowledge base profile of each item (default: the last profile found); each may be the empty string
 *     <li>kbDates (optional):					the knowledge base dates of each item (format: YYYYMMDD, default: don't use date-based KB determination); each may be the empty string.
 *     <li>kbIds (optional):					the knowledge base identifiers
 *     <li>extConfigXmls (optional):			the external configuration of each item (format: XML String); each may be the empty string.
 *	   <li>sceContextNames (optional):			context names for the SCE context object (default: empty)
 *	   <li>sceContextValues (optional)			context values for the SCE context object (default: empty)
// *     <li>formatValue (optional):				"Y"/"N" toggle: "Y": the 'salesQuantityValue' and 'salesQuantityUnit' parameters are strings in a external and language dependent representation (default value is "Y")
// *												"N": internal and language independent representation
 *     <li>pricingAnalysis (optional):			"Y"/"N" toggle: "Y": pricing analysis is performed
 *     <li>isReturn (optional):					an array of flags: "Y"/"N" toggle: "Y": sets the new instance to return
 *     <li>isStatistical (optional):			an array of flags: "Y"/"N" toggle: "Y": sets the new instance to statistical
 *     <li>pricingItemIsUnchangeable(optional): an array of flags: "Y"/"N" toggle: "Y": should be set, if the item created is already billed
 *     <li>isRelevantForPricing(optional):		an array of flags: "Y"/"N" toggle: "N": deactivates the price determination for the new item
 *     <li>isNotDeletable(optional):			an array of flags: "Y"/"N" toggle: "N": determines whether the item may be removed by configuration
 *     <li>inputItemIds (optional):				an array of (internal) item ids of the new items. See CreateItem for documentation about dealing with existing ids.
 *     <li>highLevelItemId (optional):			an array of the parent items to which the new items created are added as sub-items
 *     <li>dataKeys (optional):					an array of keys for additional data
 *     <li>dataValues (optional):				the corresponding array of values for additional data
 *     <li>dataRefs (optional):					the corresponding array of item references
 *     <li>conditionTypes (optional):			an array of conditionTypes for external condition data
 *     <li>conditionRates (optional):			the corresponding array of rates for external condition data
 *     <li>conditionCurrencies (optional):		the corresponding array of currencies for external condition data
 *     <li>calculationTypes (optional):			the corresponding array of calculation types for external condition data
 *     <li>conditionPricingUnitValues (optional):	the corresponding array of pricing unit values for external condition data
 *     <li>conditionPricingUnitUnits (optional):	the corresponding array of pricing unit units for external condition data
 *     <li>conditionItemRefs (optional):			the corresponding array of item references
 *     <li>copySourceDocumentIds (optional):	an array of document ids of the items to copy
 *     <li>copySourceItemIds (optional):		an array of item ids of the items to copy
 *     <li>copyPricingTypes (optional):			an array of pricing types, specifies how to copy pricing data
 *     <li>copySourceSalesQuantityValues (optional):an array of sales quantity values of the items to copy
 *     <li>copySourceSalesQuantityUnits (optional): an array of sales quantity units of the items to copy
 *     <li>copyTypes (optional):	    		an array of copy types, specifies how to copy pricing data
 *     <li>variantConditionKeys (optional):		the array of variant condition keys
 *     <li>variantConditionFactors (optional):	the array of variant condition factors which are associated with the keys
 *     <li>variantConditionDescs (optional):	the array of variant condition descriptions which are associated with the keys
 *	   <li>variantConditionItemRefs (optional): the corresponding array of item references. The item reference is a counter that starts with 1 for the first item
 *     <li>subItemsAlloweds (optional):         "Y"/"N" toggle: are sub items allowed by this item? Defaults to "Y".
 *     <li>multiplicities (optional):	        (integer) factor of how many identical items are represented by this item
 *     <li>netvalueOrigs (optional):            when copying items: original net value in document currency
 *     <li>netvalueNews (optional):	            when copying items: desired new net value (milestone based billing) in document currency
 *     <li>importItemConditions (optional):	    "Y"/"N" toggle: should item conditions be imported? Defaults to "N".
 * </ul>
 *
 * The arrays have to be of the same length; the i-th elements of all arrays initialize the i-th item.
 *
 * <b>Export parameters</b>
 *
 * <ul>
 *     <li>itemIds:								an array of ids of the new items
 *     <li>itemRefs:							the corresponding array of item references. The item reference is a counter that starts with 1 for the first item to be created.
 *     <li>errorMessages:						an array of error messages of the faulty items
 *     <li>faultyItemIds:						the corresponding array of item references. The item reference is a counter that starts with 1 for the first item to be created.
 *     <li>availableProductIds:					the corresponding array of the ids of the available products (deprecated)
 * </ul>
 *
 * The command guarantees that the itemIds and availableProductIds arrays are of the same length.
 */

public interface CreateItems extends ImportPricingConditions
{
	// input parameters
	public static final String DOCUMENT_ID							= "documentId";
	public static final String INPUT_ITEM_IDS						= "inputItemIds";
	public static final String MULTIPLICITIES                       = "multiplicities";
	public static final String PRODUCT_GUIDS						= "productGuids";
//	public static final String PRODUCT_IDS							= "productIds";
//	public static final String PRODUCT_LOG_SYS						= "productLogSys";
//	public static final String PRODUCT_TYPES						= "productTypes";
	public static final String DEPARTURE_COUNTRIES					= "departureCountries";
	public static final String DEPARTURE_REGIONS					= "departureRegions";
	public static final String EXCHANGE_RATE_TYPES					= "exchangeRateTypes";
	public static final String EXCHANGE_RATE_DATES					= "exchangeRateDates";
	public static final String EXTERNAL_EXCHANGE_RATES				= "externalExchangeRates";
	public static final String PRICING_DATES						= "pricingDates";
    public static final String CONDITION_ACCESS_TIMESTAMP_NAMES     = "conditionAccessTimestampNames";
    public static final String CONDITION_ACCESS_TIMESTAMP_VALUES    = "conditionAccessTimestampValues";
    public static final String CONDITION_ACCESS_TIMESTAMP_REFS      = "conditionAccessTimestampRefs";

	public static final String ALT_QUANTITY_UNIT_UNITS				= "alternativeQuantityUnitUnits";
	public static final String ALT_QUANTITY_UNIT_NUMERATORS			= "alternativeQuantityUnitNumerators";
	public static final String ALT_QUANTITY_UNIT_DENOMINATORS		= "alternativeQuantityUnitDenominators";
	public static final String ALT_QUANTITY_UNIT_EXPONENTS			= "alternativeQuantityUnitExponents";
	public static final String ALT_QUANTITY_UNIT_GROSS_WEIGHTS		= "alternativeQuantityUnitGrossWeights";
	public static final String ALT_QUANTITY_UNIT_NET_WEIGHTS		= "alternativeQuantityUnitNetWeights";
	public static final String ALT_QUANTITY_UNIT_WEIGHT_UNITS		= "alternativeQuantityUnitWeightUnits";
	public static final String ALT_QUANTITY_UNIT_VOLUMES			= "alternativeQuantityUnitVolumes";
	public static final String ALT_QUANTITY_UNIT_VOLUME_UNITS		= "alternativeQuantityUnitVolumeUnits";
	public static final String ALT_QUANTITY_UNIT_IS_BASE_UNITS		= "alternativeQuantityUnitIsBaseUnits";
	public static final String ALT_QUANTITY_UNIT_ITEM_REFS			= "alternativeQuantityUnitItemRefs";

	public static final String SERVICE_RENDERER_DATES				= "serviceRenderDates";
	public static final String BILLING_DATES						= "billingDates";
	public static final String CREATION_DATES						= "creationDates";
	public static final String DOCUMENT_DATES						= "documentDates";
	public static final String SCALE_BASE_DATES						= "scaleBaseDates";
	public static final String SCALE_PERIOD_BEGINS					= "scalePeriodBegins";
	public static final String SCALE_PERIOD_ENDS					= "scalePeriodEnds";
	public static final String SCALE_NUMBERS_OF_DAYS_IN_A_MONTH		= "scaleNumbersOfDaysInAMonth";
	public static final String SCALE_NUMBERS_OF_DAYS_IN_A_YEAR		= "scaleNumbersOfDaysInAYear";
	public static final String SCALE_BASE_NUMBERS_OF_DAYS			= "scaleBaseNumbersOfDays";
	public static final String SCALE_BASE_NUMBERS_OF_WEEKS			= "scaleBaseNumbersOfWeeks";
	public static final String SCALE_BASE_NUMBERS_OF_MONTHS			= "scaleBaseNumbersOfMonths";
	public static final String SCALE_BASE_NUMBERS_OF_YEARS			= "scaleBaseNumbersOfYears";
	public static final String CONDITION_BASE_PERIOD_BEGINS			= "conditionBasePeriodBegins";
	public static final String CONDITION_BASE_PERIOD_ENDS			= "conditionBasePeriodEnds";
	public static final String NUMBERS_OF_DAYS_IN_A_MONTH	        = "numbersOfDaysInAMonth";
	public static final String NUMBERS_OF_DAYS_IN_A_YEAR	        = "numbersOfDaysInAYear";
	public static final String CONDITION_BASE_NUMBERS_OF_DAYS		= "conditionBaseNumbersOfDays";
	public static final String CONDITION_BASE_NUMBERS_OF_WEEKS		= "conditionBaseNumbersOfWeeks";
	public static final String CONDITION_BASE_NUMBERS_OF_MONTHS		= "conditionBaseNumbersOfMonths";
	public static final String CONDITION_BASE_NUMBERS_OF_YEARS		= "conditionBaseNumbersOfYears";
	public static final String SALES_QUANTITY_VALUES				= "salesQuantityValues"; // formatted
	public static final String SALES_QUANTITY_UNITS					= "salesQuantityUnits";
	public static final String QUANTITY_RATIO_NUMERATORS			= "quantityRatioNumerators";
	public static final String QUANTITY_RATIO_DENOMINATORS			= "quantityRatioDenominators";
	public static final String GROSS_WEIGHT_VALUES					= "grossWeightValues"; // formatted
	public static final String NET_WEIGHT_VALUES					= "netWeightValues"; // formatted
	public static final String WEIGHT_UNITS							= "weightUnits";
	public static final String VOLUME_VALUES						= "volumeValues"; // formatted
	public static final String VOLUME_UNITS							= "volumeUnits";
	public static final String POINTS_VALUES						= "pointsValues";
	public static final String POINTS_UNITS							= "pointsUnits";
	public static final String SALES_ORGANISATIONS					= "salesOrganisations";
	public static final String DISTRIBUTION_CHANNELS				= "distributionChannels";
	public static final String HEADER_ATTRIBUTE_NAMES				= "headerAttributeNames";
	public static final String HEADER_ATTRIBUTE_VALUES				= "headerAttributeValues";
	public static final String HEADER_ATTRIBUTE_ITEM_REFS			= "headerAttributeItemRefs";
	public static final String ITEM_ATTRIBUTE_NAMES					= "itemAttributeNames";
	public static final String ITEM_ATTRIBUTE_VALUES				= "itemAttributeValues";
	public static final String ITEM_ATTRIBUTE_ITEM_REFS				= "itemAttributeItemRefs";
	public static final String KNOWLEDGE_BASE_LOGSYS                = "kbLogsys";
	public static final String KNOWLEDGE_BASE_NAMES					= "kbNames";
	public static final String KNOWLEDGE_BASE_VERSIONS				= "kbVersions";
	public static final String KNOWLEDGE_BASE_PROFILES				= "kbProfiles";
	public static final String KNOWLEDGE_BASE_DATES					= "kbDates";
	public static final String KNOWLEDGE_BASE_IDS					= "kbIds";
	public static final String EXT_CONFIG_XML_STRINGS                 = "extConfigXmls";
	public static final String SCE_CONTEXT_NAMES					= "sceContextNames";
	public static final String SCE_CONTEXT_VALUES					= "sceContextValues";
	public static final String SCE_CONTEXT_ITEM_REFS				= "sceContextItemRefs";
//	public static final String FORMAT_VALUE							= "formatValue";
	public static final String PERFORM_PRICING_ANALYSIS				= "pricingAnalysis";
	public static final String IS_RETURNS							= "isReturns";
	public static final String IS_STATISTICALS						= "isStatisticals";
	public static final String PRICING_ITEM_IS_UNCHANGEABLES		= "pricingItemIsUnchangeables";
	public static final String IS_RELEVANT_FOR_PRICINGS				= "isRelevantForPricings";
	public static final String IS_NOT_DELETABLES					= "isNotDeletables";
	public static final String EXTERNAL_ITEM_IDS					= "externalItemIds";
	public static final String HIGH_LEVEL_ITEM_IDS					= "highLevelItemIds";
	public static final String ADDITIONAL_DATA_KEYS					= "dataKeys";
	public static final String ADDITIONAL_DATA_VALUES				= "dataValues";
	public static final String ADDITIONAL_DATA_ITEM_REFS			= "dataRefs";
	public static final String EXTSRC_CONDITION_TYPES				= "conditionTypes";
	public static final String EXTSRC_CONDITION_RATES				= "conditionRates";
	public static final String EXTSRC_CONDITION_CURRENCIES			= "conditionCurrencies";
	public static final String EXTSRC_CALCULATION_TYPES				= "calculationTypes";
	public static final String EXTSRC_CONDITION_PRICING_UNIT_VALUES	= "conditionPricingUnitValues";
	public static final String EXTSRC_CONDITION_PRICING_UNIT_UNITS	= "conditionPricingUnitUnits";
	public static final String EXTSRC_CONDITION_ITEM_REFS			= "extRefs";
	public static final String VARIANT_CONDITION_KEYS               = "variantConditionKeys";
	public static final String VARIANT_CONDITION_FACTORS            = "variantConditionFactors";
	public static final String VARIANT_CONDITION_DESCRIPTIONS       = "variantConditionDescs";
	public static final String VARIANT_CONDITION_ITEM_REFS          = "variantConditionItemRefs";
	public static final String SUB_ITEMS_ALLOWEDS                   = "subItemsAlloweds";
    public static final String SUPPRESS_PRICING                     = "suppressPricing";
    public static final String FIXATION_GROUPS  			        = "fixationGroups";
    public static final String EXT_POS_GUID                         = "extPosItemIds";
    public static final String EXT_POS_INST_ID                      = "extPosInstIds";
                                                                       
    
    
    //special input parameter to identify AFS products
    public static final String GRID_PRODUCTS                          = "gridProducts";

	// output parameters
 	public static final String ITEM_REFS							= "itemRefs";
	public static final String ITEM_IDS								= "itemIds";
 	public static final String FAULTY_ITEM_REFS						= "faultyItemRefs";
 	public static final String ERROR_MESSAGES						= "errorMessages";
	public static final String AVAILABLE_PRODUCT_IDS				= "availableProductIds";
	public static final String OBJECT_IDS							= "objectIds";
	public static final String MESSAGES								= "messages";
	public static final String MESSAGE_TYPES						= "messageTypes";
}
