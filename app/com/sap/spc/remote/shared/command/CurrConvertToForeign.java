/************************************************************************

Copyright (c) 2005 by SAP AG

All rights to both implementation and design are reserved.

Use and copying of this software and preparation of derivative works
based upon this software are not permitted.

Distribution of this software is restricted. SAP does not make any
warranty about the software, its performance or its conformity to any
specification.

**************************************************************************/
package com.sap.spc.remote.shared.command;

/**
 * Returns all currency units available.<p>
 * 
 * <b>Import parameters</b>
 * <ul>
 *      <li>localAmount :		the amount to convert in local currency
 * 	    <li>localCurrency:		the local currency
 * 		<li>foreignCurrency: 	the foreign currency
 * 		<li>exchangeRateDate:	date for the exchange rate, requested format = YYYYMMDD
 * 		<li>exchangeRateType (optional - defaulted to M)
 * 		<li>exchangeRate (optional)
 * 
 * </ul><p>
 * 
 * <b>Export parameters</b>
 * <ul>
 *     	<li>foreignAmount
 * 		<li>currencyConversionRate					
 * </ul>
 */
public interface CurrConvertToForeign {
	//Input
	public static final String LOCAL_AMOUNT = "localAmount";
	public static final String LOCAL_CURRENCY = "localCurrency";
	public static final String FOREIGN_CURRENCY = "foreignCurrency";
	public static final String EXCH_RATE_DATE = "exchangeRateDate";
	public static final String EXCH_RATE_TYPE = "exchangeRateType";
	public static final String EXCH_RATE = "exchangeRate";
	
	//output
	public static final String FOREIGN_AMOUNT = "foreignAmount";
	public static final String CURR_CONV_RATE = "currencyConversionRate";
}
