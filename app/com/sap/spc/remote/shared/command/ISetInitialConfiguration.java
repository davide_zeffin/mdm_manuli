package com.sap.spc.remote.shared.command;

/**
 * Sets the initial item configuration. This can be used for 
 * subsequent comparison with the current item configuration  <p>
 *
 * <b>Import parameters</b>
 *
 * <ul>
 *    <li>documentId (required):    the document id of the item being configured (either docId/itemId or configId is required)
 *    <li>itemId (required):        the item id of the item being configured (either docId/itemId or configId is required)
 *    <li>configId (required):	    the configuration id (either docId/itemId or configId is required)
 *    <li>extConfigXml (required):	the external configuration in xml representation
 * </ul><p>
 *
 */

public interface ISetInitialConfiguration extends ExtConfigConstants{
    
    public static final String EXT_CONFIG_XML_STRING    = "extConfigXml";    
}
