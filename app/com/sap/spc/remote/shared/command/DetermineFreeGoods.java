/*
 * Created on 19.01.2005
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Generation - Code and Comments
 */
package com.sap.spc.remote.shared.command;

	/**
	 * perform determination of free goods using condition technique and return resulting data.
	 *
	 * <b>Import parameters</b>
	 *
	 * <ul>
	 *                <li>procedureName (required):                    the name of the procedure.
	 *                <li>lineIds (required):                            an array of unique line ids, which identify the calling container.
	 *                <li>application (optional):                 the name of the application. Default value is "CRM".
	 *      <li>language(optional):                     the language for locale. Default value is "EN".
	 *      <li>country(optional):                      the country for locale. Default value is "US".
	 *                <li>productGuids (required):                an array of internal product ids identifying the products that are associated with the items
	 *      <li>salesQuantityValues (optional):                    an array of sales quantity values of the items (default: value: 1, unit: sales unit of product).
	 *      <li>salesQuantityUnits (optional):                    an array of sales quantity units (name) of the items (default: value: 1, unit: sales unit of product).
	 *                <li>defaultAccessTimestamps (optional):            an array of timestamps used for condition access. Default value is "now".
	 *      <li>AccessTimestampNames (optional):            an array of timestamp names for condition access.
	 *            <li>AccessTimestampValues (optional):            the corresponding array of timestamp values.
	 *            <li>AccessTimestampLineIdRefs (optional):   the corresponding array of line id references. The line id reference is a counter that starts with 1 for the first new line id.
	 *      <li>headerAttributeNames (optional):            an array of header attribute names for all line ids.
	 *            <li>headerAttributeValues (optional):            the corresponding array of header attribute values.
	 *            <li>headerAttributeLineIdRefs (optional):   the corresponding array of line id references. The line id reference is a counter that starts with 1 for the first new line id.
	 *      <li>itemAttributeNames (optional):                    an array of item attribute names for all line ids.
	 *            <li>itemAttributeValues (optional):                    the corresponding array of item attribute values.
	 *            <li>itemAttributeLineIdRefs (optional):            the corresponding array of line id references.
	 *      <li>performAnalysis (optional):                            "Y"/"N" toggle: "Y": analysis is performed
	 * </ul>
	 *
	 * <b>Export parameters</b>
	 * <ul>
	 *     <li>freeProductGuids:                an array of products to be given free.
	 *     <li>freeProductQuantityValues:       an array of quantity values of the product to be given free.
	 *     <li>freeProductQuantityUnits:        an array of quantity units of the product to be given free.
	 *     <li>freeProductTypes:                an array of the types of free. Namely exclusive, inclusive, or inclusive without item generation.
	 *     <li>conditionIsInactives:            an array of flags which specifies if a condition is inactive (eg. inactive due to an error in user exit).
	 *     <li>minimumQuantityValues:           an array of minimum quantity values required for the item to get the free good.
	 *     <li>freeGoodsQuantityValues:         an array of freegoods quantity values for which the additional quantity will be given.
	 *     <li>freeGoodsQuantityUnits:          an array of freegoods quantity units for which the additional quantity will be given.
	 *     <li>additionalQuantityValues:        an array of freegoods additional quantity values which will be given for every freegoods quantity.
	 *     <li>additionalQuantityUnits:         an array of freegoods additional quantity values which will be given for every freegoods quantity.
	 *     <li>additionalProductGuids:          an array of products to be given with the item as free good.
	 *     <li>calculationTypes                 an array of calculation rule numbers used to calculate the condition value.
	 *     <li>errorMessageTexts                an array of message texts. This explains the reason for the condition to be inactive or some other error in determination.
	 *     <li>reasonsForZeroQuantity           an array of reasons for the freegoods quantity to be zero.
	 *           <li>freeGoodsLineIdRefs:                    the corresponding array of line id references.
	 * </ul>
	 *
	 * The command guarantees that all arrays are of the same length.<p>
	 *
	 *
	 *
	 */
	public interface DetermineFreeGoods {
	    // input parameter
	    public static final String PROCEDURE_NAME = "procedureName";
	    public static final String LINE_IDS = "lineIds";
	    public static final String APPLICATION = "application";
	    public static final String COUNTRY = "country";
	    public static final String PRODUCT_GUIDS = "productGuids";
	    public static final String SALES_QUANTITY_VALUES = "salesQuantityValues";
	    public static final String SALES_QUANTITY_UNITS = "salesQuantityUnits";
	    public static final String ACCESS_TIMESTAMP_LINE_ID_REFS = "accessTimestampLineIdRefs";
	    public static final String ACCESS_TIMESTAMP_NAMES = "accessTimestampNames";
	    public static final String ACCESS_TIMESTAMP_VALUES = "accessTimestampValues";
	    public static final String HEADER_ATTRIBUTE_LINE_ID_REFS = "headerAttributeLineIdRefs";
	    public static final String HEADER_ATTRIBUTE_NAMES = "headerAttributeNames";
	    public static final String HEADER_ATTRIBUTE_VALUES = "headerAttributeValues";
	    public static final String ITEM_ATTRIBUTE_LINE_ID_REFS = "itemAttributeLineIdRefs";
	    public static final String ITEM_ATTRIBUTE_NAMES = "itemAttributeNames";
	    public static final String ITEM_ATTRIBUTE_VALUES = "itemAttributeValues";
	    public static final String PERFORM_ANALYSIS = "performAnalysis";

	    // output parameter
	    public static final String FREE_GOODS_LINE_ID_REFS = "freeGoodsLineIdRefs";
	    public static final String FREE_PRODUCT_GUIDS = "freeProductGuids";
	    public static final String FREE_PRODUCT_QUANTITY_VALUES = "freeProductQuantityValues";
	    public static final String FREE_PRODUCT_QUANTITY_UNITS = "freeProductQuantityUnits";
	    public static final String FREE_PRODUCT_TYPES = "freeProductTypes";
	    public static final String CONDITION_IS_INACTIVES = "conditionIsInactives";
	    public static final String MINIMUM_QUANTITY_VALUES = "minimumQuantityValues";
	    public static final String FREE_GOODS_QUANTITY_VALUES = "freeGoodsQuantityValues";
	    public static final String FREE_GOODS_QUANTITY_UNITS = "freeGoodsQuantityUnits";
	    public static final String ADDITIONAL_QUANTITY_VALUES = "additionalQuantityValues";
	    public static final String ADDITIONAL_QUANTITY_UNITS = "additionalQuantityUnits";
	    public static final String ADDITIONAL_PRODUCT_GUIDS = "additionalProductGuids";
	    public static final String CALCULATION_TYPES = "calculationTypes";
	    public static final String ERROR_MESSAGE_TEXTS = "errorMessageTexts";
	    public static final String REASONS_FOR_ZERO_QUANTITY = "reasonsForZeroQuantity";
	    public static final String VARIABLE_KEY_NAMES = "dynamicVariableKeyNames";
	    public static final String VARIABLE_KEY_VALUES = "dynamicVariableKeyValues";
	    public static final String VARIABLE_KEY_REFS = "dynamicVariableKeyRefs";
	    public static final String VARIABLE_DATA_NAMES = "dynamicVariableDataNames";
	    public static final String VARIABLE_DATA_VALUES = "dynamicVariableDataValues";
	    public static final String VARIABLE_DATA_REFS = "dynamicVariableDataRefs";
}
