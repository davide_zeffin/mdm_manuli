/************************************************************************

	Copyright (c) 1999 by SAP AG

	All rights to both implementation and design are reserved.

	Use and copying of this software and preparation of derivative works
	based upon this software are not permitted.

	Distribution of this software is restricted. SAP does not make any
	warranty about the software, its performance or its conformity to any
	specification.

**************************************************************************/
package com.sap.spc.remote.shared.command;

/**
 * Returns all pricing procedures available.<p>
 * 
 * <b>Import parameters</b>
 * 
 * <ul>
 *     <li>application (optional):	application (default: V)
 *     <li>usage (optional):		usage (default: A)
 *     <li>language (optional):		language (default: English)
 * </ul><p>
 * 
 * <b>Export parameters</b>
 * <ul>
 *     <li>name:					the names (keys) of the pricing procedures
 *     <li>description:				the descriptions of the pricing procedures
 * </ul>
 */
public interface GetAllPricingProcedures
{
	//input
	public static final String APPLICATION = "application"; //optional
	public static final String USAGE = "usage"; //optional
	public static final String LANGUAGE = "language"; //optional
	
	//output
	public static final String NAME = "name";
	public static final String DESCRIPTION = "description";	
}
