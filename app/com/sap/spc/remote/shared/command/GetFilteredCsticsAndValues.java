/************************************************************************

    Copyright (c) 1999 by SAP AG

    All rights to both implementation and design are reserved.

    Use and copying of this software and preparation of derivative works
    based upon this software are not permitted.

    Distribution of this software is restricted. SAP does not make any
    warranty about the software, its performance or its conformity to any
    specification.

**************************************************************************/
package com.sap.spc.remote.shared.command;

/**
 * This command returns characteristics and values. Interest groups can be
 * defined to tell the server explicitely which data you need. You can define
 * as many interest groups as you want, but group 0 is always used for those
 * characteristics that are not associated with another interest group.
 * The parameter names used for this command differ lightely from the way other
 * server commands handle parameters. An index of the interest group is added
 * to the returned parameter name.
 *
 * <p>
 *
 * <b>Import parameters</b>
 * <ul>
 *    <li>documentId (required):    the document id of the item being configured
 *    <li>itemId (required):        the item id of the item being configured
 *    <li>instId (required):        the instance id of the instance we're interested in
 *    <li>filterGroups (optional): if specified, only the cstics of the given groups will be returned.
 *      <li>visibleCsticsOnly
 *                      (optional): if specified, only visible cstics and invisible cstics with
 *                                  with a conflict will be shown
 *    <li>assignableValuesOnly (optional: if specified only assignable values will be shown 
 *    <li>UNGROUPED_CSTICS_ONLY (optional): if specified, only ungrouped cstics will be returned.
 *    <li>INCLUDE_UNGROUPED_CSTICS (optional): if specified, only ungrouped cstics will be returned.
 *    <li>MONITORED_CSTIC_NAMES (optional): these are the language independent cstic names of those cstics which you
 *                                          want to associate with one of the given interest groups.
 *    <li>MONITORED_CSTIC_INFO_GROUPS (optional): the interest group id of the interest group which you want to
 *                                                associate with the cstic in MONITORED_CSTIC_NAMES
 *    <li>CSTIC_INFO_GROUPS: the ids of all interest groups, starting from 0. 0 is used for all not monitored cstics.
 *    <li>CSTIC_INFO_GROUP_INTERESTS: A string containing the desired information. It can contain following states:
 *    <ul>
 *      <li>A : INTEREST_CSTIC_MIMES include mime objects for cstics
 *      <li>B : INTEREST_CSTIC_DOMAIN_IS_INTERVAL include domain is an interval
 *      <li>C : INTEREST_CSTIC_LNAMES include language dependent names
 *      <li>D : INTEREST_CSTIC_MULTIS include multiple values allowed
 *      <li>E : INTEREST_CSTIC_ADDITS include additional values allowed
 *      <li>F : INTEREST_CSTIC_CONSTRAINEDS include domain constrained
 *      <li>G : INTEREST_CSTIC_GROUPS include group association
 *      <li>H : INTEREST_CSTIC_UNITS include unites for cstic
 *      <li>I : INTEREST_CSTIC_READONLYS include if cstic is readonly
 *      <li>J : INTEREST_CSTIC_VISIBLES include cstic visible
 *      <li>K : INTEREST_CSTIC_AUTHORS include author of cstic
 *      <li>L : INTEREST_CSTIC_REQUIREDS include if cstic is required
 *      <li>M : INTEREST_CSTIC_CONSISTENTS include if cstic is consistent
 *      <li>X : INTEREST_CSTIC_ENTRY_FIELD_MASK include entry field mask for cstic
 *      <li>Y : INTEREST_CSTIC_TYPELENGTH include type length of cstic
 *      <li>Z : INTEREST_CSTIC_NUMBERSCALE include numeric scale of cstic
 *      <li>a : INTEREST_CSTIC_VALUETYPE include value type
 *      <li>b : INTEREST_CSTIC_GROUP_POSITION include cstic position in group
 *      <li>c : INTEREST_CSTIC_INST_POSITION include cstic position in instance
 *      <li>d : INTEREST_CSTIC_VIEWS include cstic application views
 *      <li>e : INTEREST_CSTIC_EXPANDEDS include flag whether cstic should be displayed expanded
 *      <li>N : INTEREST_VALUE_LNAMES include language dependent names
 *      <li>P : INTEREST_VALUE_DEFAULTS include default values
 *      <li>Q : INTEREST_VALUE_ASSIGNEDS include values assigned
 *      <li>R : INTEREST_VALUE_AUTHORS include authors of values
 *      <li>S : INTEREST_VALUE_PRICES include prices
 *      <li>T : INTEREST_VALUE_LPRICES include language dependent prices
 *      <li>V : INTEREST_VALUE_DESCRIPTIONS include descriptions
 *      <li>W : INTEREST_VALUE_MIMES include value mime objects
 *      <li>f : INTEREST_VALUE_IS_DOMAIN_VALUE include flag to determine whether the value is a domain value
 *    </ul>
 *
 * <b>Export parameters</b>
 *    <ul>
 *      <li>X_CSTIC_NAMES = "cn" : language independent cstic name
 *      <li>X_CSTIC_LNAMES = "cl" : language dependent cstic name
 *      <li>X_CSTIC_MULTIS = "cm" : cstic allows multiple values
 *      <li>X_CSTIC_ADDITS = "cad" : cstic allows additional values
 *      <li>X_CSTIC_CONSTRAINEDS = "cc" : cstic is domain constrained
 *      <li>X_CSTIC_GROUPS = "cg" : cstic group
 *      <li>X_CSTIC_UNITS = "cu" : cstic value's unit
 *      <li>X_CSTIC_READONLYS = "cr" : is cstic read-only? [Y/N]
 *      <li>X_CSTIC_VISIBLES = "cv" : is cstic visible? [Y/N]
 *      <li>X_CSTIC_AUTHORS = "ca" : author of cstic
 *      <li>X_CSTIC_REQUIREDS = "crq" : is cstic required?
 *      <li>X_CSTIC_CONSISTENTS = "ccn" : is cstic consistent?
 *      <li>X_CSTIC_DOMAIN_IS_INTERVALS = "ci" : is domain an interval?
 *      <li>X_CSTIC_ENTRY_FIELD_MASK = "cef" : cstic value's entry-field-mask
 *      <li>X_CSTIC_TYPELENGTH = "ct" : length of cstic without separators or leading "-"
 *      <li>X_CSTIC_NUMBERSCALE = "cns" : number scale of the value
 *      <li>X_CSTIC_VALUETYPE = "cvt" : basic type of cstic value
 *      <li>X_CSTIC_GROUP_POSITIONS = "cgp" : position of cstic in group
 *      <li>X_CSTIC_INST_POSITIONS = "cip" : position of cstic in instance
 *      <li>X_CSTIC_VIEWS = "cav" : application views that were maintained for this cstic
 *      <li>X_CSTICS_EXPANDEDS = "ces" : flag: display the cstic expanded?
 *      <li>X_CSTIC_MIME_LNAME = "cmn" : cstic name that mime belongs to
 *      <li>X_CSTIC_MIME_TYPE = "cmt" : mime object type of this cstic mime
 *      <li>X_CSTIC_MIME_OBJECT = "cmo" : mime object (URL) of this cstic mime
 *    </ul>
 *    <ul>
 *      <li>X_VALUE_NAMES = "vn" : language independent value name
 *      <li>X_VALUE_CSTIC_NAMES = "vc" : the cstic name, this value belongs to
 *      <li>X_VALUE_LNAMES = "vl" : language dependent value name
 *      <li>X_VALUE_DEFAULTS = "vd" : default value
 *      <li>X_VALUE_ASSIGNEDS = "va" : is value assigned? [Y/N]
 *      <li>X_VALUE_AUTHORS = "vat" : author of value
 *      <li>X_VALUE_PRICES = "vp" : value price
 *      <li>X_VALUE_LPRICES = "vpl" : value language dependent price
 *      <li>X_VALUE_DESCRIPTIONS = "vdc" : value description
 *      <li>X_VALUE_IS_DOMAIN_VALUE = "vid" : is value in the domain (part of the master-data)?
 *      <li>X_MIME_VALUE_LNAME = "mv" : value name, this mime object belongs to
 *      <li>X_MIME_OBJECT = "mo" : mime object (URL)
 *      <li>X_MIME_TYPE = "mt" : mime object type
 *    </ul>
 * </ul>
 */

public interface GetFilteredCsticsAndValues extends SCECommand {

    static final String INST_ID                     = "instId";

    // These constants allow to filter the characteristics that are to be shown
    static final String FILTER_GROUPS               = "fig";
    static final String UNGROUPED_CSTICS_ONLY       = "ugc";
    static final String INCLUDE_UNGROUPED_CSTICS    = "iuc";
    static final String VISIBLE_CSTICS_ONLY         = "vco";
    static final String ASSIGNABLE_VALUES_ONLY      = "avo";

    // These constants are for information groups
    static final String CSTIC_INFO_GROUPS           = "cig";
    static final String CSTIC_INFO_GROUP_INTERESTS  = "cii";
    static final String MONITORED_CSTIC_NAMES       = "mcn";
    static final String MONITORED_CSTIC_INFO_GROUPS = "mci";

    // Now the interest toggles
    static final char INTEREST_CSTIC_MIMES         = 'A';
    static final char INTEREST_CSTIC_DOMAIN_IS_INTERVAL = 'B';
    static final char INTEREST_CSTIC_LNAMES        = 'C';
    static final char INTEREST_CSTIC_MULTIS        = 'D';
    static final char INTEREST_CSTIC_ADDITS        = 'E';
    static final char INTEREST_CSTIC_CONSTRAINEDS  = 'F';
    static final char INTEREST_CSTIC_GROUPS        = 'G';
    static final char INTEREST_CSTIC_UNITS         = 'H';
    static final char INTEREST_CSTIC_READONLYS     = 'I';
    static final char INTEREST_CSTIC_VISIBLES      = 'J';
    static final char INTEREST_CSTIC_AUTHORS       = 'K';
    static final char INTEREST_CSTIC_REQUIREDS     = 'L';
    static final char INTEREST_CSTIC_CONSISTENTS   = 'M';
    static final char INTEREST_CSTIC_ENTRY_FIELD_MASK = 'X';
    static final char INTEREST_CSTIC_TYPELENGTH    = 'Y';
    static final char INTEREST_CSTIC_NUMBERSCALE   = 'Z';
    static final char INTEREST_CSTIC_VALUETYPE     = 'a';
    static final char INTEREST_CSTIC_GROUP_POSITION = 'b';
    static final char INTEREST_CSTIC_INST_POSITION = 'c';
    static final char INTEREST_CSTIC_VIEWS         = 'd';
    static final char INTEREST_CSTIC_EXPANDEDS     = 'e';
	static final char INTEREST_CSTIC_DESCRIPTIONS    ='g';

    // characteristic values
    static final char INTEREST_VALUE_LNAMES        = 'N';
    static final char INTEREST_VALUE_DEFAULTS      = 'P';
    static final char INTEREST_VALUE_ASSIGNEDS     = 'Q';
    static final char INTEREST_VALUE_AUTHORS       = 'R';
    static final char INTEREST_VALUE_PRICES        = 'S';
    static final char INTEREST_VALUE_LPRICES       = 'T';
    static final char INTEREST_VALUE_DESCRIPTIONS  = 'V';
    static final char INTEREST_VALUE_MIMES         = 'W';
    static final char INTEREST_VALUE_IS_DOMAIN_VALUE = 'f';

    // This command comes with new constants with a smaller string representation
    // This reduces the data transfer and improves the performance, but it
    // makes the server command almost unreadable.
    static final String X_CSTIC_NAMES              = "cn";
    static final String X_CSTIC_GROUP_POSITIONS    = "cgp";
    static final String X_CSTIC_INST_POSITIONS     = "cip";
    static final String X_CSTIC_LNAMES             = "cl";

    static final String X_CSTIC_MULTIS             = "cm";
    static final String X_CSTIC_ADDITS             = "cad";
    static final String X_CSTIC_CONSTRAINEDS       = "cc";
    static final String X_CSTIC_GROUPS             = "cg";
    static final String X_CSTIC_UNITS              = "cu";
    static final String X_CSTIC_READONLYS          = "cr";
    static final String X_CSTIC_VISIBLES           = "cv";
    static final String X_CSTIC_AUTHORS            = "ca";
    static final String X_CSTIC_REQUIREDS          = "crq";
    static final String X_CSTIC_CONSISTENTS        = "ccn";
    static final String X_CSTIC_DOMAIN_IS_INTERVALS= "ci";
    static final String X_CSTIC_ENTRY_FIELD_MASK   = "cef";
    static final String X_CSTIC_TYPELENGTH         = "ct";
    static final String X_CSTIC_NUMBERSCALE        = "cns";
    static final String X_CSTIC_VALUETYPE          = "cvt";
    static final String X_CSTIC_VIEWS              = "cav";
    static final String X_CSTIC_MIME_LNAME         = "cmn";
    static final String X_CSTIC_MIME_TYPE          = "cmt";
    static final String X_CSTIC_MIME_OBJECT        = "cmo";
    static final String X_CSTIC_EXPANDEDS          = "ces";
	static final String X_CSTIC_DESCRIPTIONS        = "cd";

    static final String X_VALUE_NAMES              = "vn";
    static final String X_VALUE_CSTIC_NAMES        = "vc";
    static final String X_VALUE_LNAMES             = "vl";
    static final String X_VALUE_DEFAULTS           = "vd";
    static final String X_VALUE_ASSIGNEDS          = "va";
    static final String X_VALUE_AUTHORS            = "vat";
    static final String X_VALUE_CONDITIONS         = "vco";
    static final String X_VALUE_PRICES             = "vp";
    static final String X_VALUE_LPRICES            = "vpl";
    static final String X_VALUE_DESCRIPTIONS       = "vdc";
    static final String X_VALUE_IS_DOMAIN_VALUE    = "vid";
    static final String X_MIME_VALUE_LNAME         = "mv";
    static final String X_MIME_OBJECT              = "mo";
    static final String X_MIME_TYPE                = "mt";

    static final String GROUP_CONSISTENT           = "gc";
    static final String GROUP_REQUIRED             = "gr";
    static final String GROUP_NAME                 = "gn";

}