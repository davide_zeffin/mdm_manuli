/************************************************************************

	Copyright (c) 1999 by SAP AG

	All rights to both implementation and design are reserved.

	Use and copying of this software and preparation of derivative works
	based upon this software are not permitted.

	Distribution of this software is restricted. SAP does not make any
	warranty about the software, its performance or its conformity to any
	specification.

**************************************************************************/
package com.sap.spc.remote.shared.command;

/**
 * Returns all physical units available.<p>
 *
* <b>Import parameters</b>
 * <ul>
 *     <li>dimensionName :	a filter value for the units
 * </ul><p>
 *
 * <b>Export parameters</b>
 * <ul>
 *     <li>name:					the names (keys) of the physical units (external and language dependent representation)
 *     <li>description:				the descriptions of the physical units
 *     <li>decimals:				the number of decimal digits
 * </ul>
 */
public interface GetAllPhysicalUnits
{
	//input
	public static final String DIMENSIONNAME = "dimensionName";
	//output
	public static final String NAME = "name";
	public static final String DESCRIPTION = "description";
	public static final String INTERNAL_NAME = "internalname";
	public static final String DECIMALS = "decimals";

}

