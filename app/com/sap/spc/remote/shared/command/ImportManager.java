/*
 * Created on Jan 18, 2005
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package com.sap.spc.remote.shared.command;

/**
 * Opens a connection to the current database, reads the import data from the known MSA
 * import tables, checks the consistency of the import data, writes data into the SCE database tables
 * and deletes the imported data from the import tables.  <p>
 * 
 * <b>Import parameters</b>
 * <br>
 * none: the MSA IPC server has to be initialized with a proper database connection for this command
 * to work.<p>
 * 
 * <b>Export parameters</b>
 * <ul>
 * 		No export parameters, check the STATUS code for success or failure. 
 * </ul>
 */
public interface ImportManager {
	public static final String DOCUMENT_PUBLISHER_PATH = "documentPath";	
}
