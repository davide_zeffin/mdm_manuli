package com.sap.spc.remote.shared.command;
/**
 * Returns load messages for a given configuration.  <p>
 *
 * <b>Import parameters</b>
 *
 * <ul>
 *     <li>CONFIG_ID (required):	the configuration id 
 *     
 * </ul><p>
 *
 * <b>Export parameters</b>
 *
 * Table LOAD_MESSAGES with the following line structure
 * <ul>
 *     <li>type:            message type (E, W, I) 
 *	   <li>id:              T100 message area (here: 34)
 *	   <li>number:          T100 message number
 *	   <li>message:         message Text (with variable substitutions).
 *                        	Uses the language of the current session
 *     <li>message_v1:      argument of message (for variable substitution)
 *     <li>message_v2:      argument of message (for variable substitution)
 *     <li>message_v3:      argument of message (for variable substitution)
 *     <li>message_v4:      argument of message (for variable substitution)
 * 
 *     <li>inst_id:         ID of instance where message occurred
 *     <li>charc:           name of cstic where message occurred
 * </ul><p>
 */


public interface IGetConfigLoadMessages {
//	export

	 public static final String TYPE                 = "TYPE";
	 public static final String ID                   = "ID";
	 public static final String NUMBER               = "NUMBER";
	 public static final String MESSAGE              = "MESSAGE";
	 public static final String MESSAGE_V1           = "MESSAGE_V1";
	 public static final String MESSAGE_V2           = "MESSAGE_V2";
	 public static final String MESSAGE_V3           = "MESSAGE_V3";
	 public static final String MESSAGE_V4           = "MESSAGE_V4";
	
	 public static final String INST_ID              = "PARAMETER";   // use generic BAPIRET2
	 public static final String CHARC                = "FIELD";       // use generic BAPIRET2
	

}
