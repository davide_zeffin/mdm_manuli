/************************************************************************

Copyright (c) 1999 by SAP AG

All rights to both implementation and design are reserved.

Use and copying of this software and preparation of derivative works
based upon this software are not permitted.

Distribution of this software is restricted. SAP does not make any
warranty about the software, its performance or its conformity to any
specification.

**************************************************************************/
package com.sap.spc.remote.shared.command;

public interface GetSupplementaryConditionRecords {

    //in/out
    public static final String 	CONDITION_RECORD_ID	="conditionRecordId";
    
    //out
    public static final String 	SCALE_TYPE	="scaleType";
    public static final String 	SCALE_BASE_TYPE	="scaleBaseType";
    public static final String 	COUNTER	="counter";
    public static final String 	DENOMINATOR_BASE_TEN_EXPONENT_SALE_TAX_CODE	="denominatorBaseTenExponentSaleTaxCode";
    public static final String 	WITHHOLDING_TAX_CODE	="withholdingTaxCode";
    public static final String 	CONDITION_UNIT_MEASURE	="conditionUnitMeasure";
    public static final String 	CURRENCY_KEY_SCALE_LIMIT	="currencyKeyScaleLimit";
    public static final String 	BASE_UNIT_MEASURE	="baseUnitMeasure";
    public static final String 	EXCLUSION_INDICATOR_LOWER_LIMIT_CONDITION_RATE	="exclusionIndicatorLowerLimitConditionRate";
    public static final String 	UPPER_LIMIT_CONDITION_RATE	="upperLimitConditionRate";
    public static final String 	TERMS_PAYMENT_KEY	="termsPaymentKey";
    public static final String 	FIXED_VALUE_DATE	="fixedValueDate";
    public static final String 	NB_VALUE_DAYS	="nbValueDays";
    
}
