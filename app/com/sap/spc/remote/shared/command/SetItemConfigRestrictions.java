package com.sap.spc.remote.shared.command;


/**
 * Internal command, not released for general usage.<p>
 *
 * This command is invoked on a configurable item with �live� configuration data
 * in the IPC in order to impose specific external restrictions. This command can
 * be invoked multiple times for different restrictions. In this case, the combination
 * of all restrictions is imposed on the current configuration.<p>
 *
 * The command will only perform the actual work if all optional parameters are
 * provided. If one ore more value is missing, the command will not do anything
 * (not even log an error).
 *
 * <b>Import parameters</b>
 *
 * <ul>
 *     <li>documentId (required):	    the document id of the item being configured
 *     <li>itemId (required):		    the item id of the item being configured
 *     <li>ownerType (optional):	    the type of the restricting entity (e.g. "CONTRACT")
 *     <li>ownerId   (optional):	    a unique identifier for the restricting entity (contract number)
 *     <li>repositoryType (optional):	a string identifying the repository (e.g. "IBASE")
 *     <li>restrictId (optional):	    the key identifying configuration data (restrictions) associated with the given owner
 * </ul><p>
 *
 * <b>Export parameters</b><p>
 * none.
*/
public interface SetItemConfigRestrictions extends SCECommand {
	public static final String OWNER_TYPE       = "ownerType";
	public static final String OWNER_ID         = "ownerId";
	public static final String REPOSITORY_TYPE  = "repositoryType";
	public static final String RESTRICT_ID      = "restrictId";
}
