/************************************************************************

	Copyright (c) 2002 by SAP AG

	All rights to both implementation and design are reserved.

	Use and copying of this software and preparation of derivative works
	based upon this software are not permitted.

	Distribution of this software is restricted. SAP does not make any
	warranty about the software, its performance or its conformity to any
	specification.

**************************************************************************/

package com.sap.spc.remote.shared.command;


/**
 * Requests the deletion of an instance and all of its part instances
 * if any.
 *
 * <b>Import parameters</b>
 *
 * <ul>
 *     <li>documentId (required):	the document id of the item being configured
 *     <li>itemId (required):		the item id of the item being configured
 *     <li>instId (required):       the id of the instance to be deleted
 * </ul><p>
 *
 * <b>Export parameters</b>
 *
 * <ul>
 *     <li>success:                 "Y"/"N" toggle: the instance was successfully deleted by the engine
 * </ul>
 */
public interface DeleteInstance extends SCECommand
{
	public static final String INST_ID			= "instId";
	public static final String SUCCESS      	= "success";
}
