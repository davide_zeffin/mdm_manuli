/*******************************************************************************

	Copyright (c) 2000 by SAP AG

	All rights to both implementation and design are reserved.

	Use and copying of this software and preparation of derivative works based
	upon this software are not permitted.

	Distribution of this software is restricted. SAP does not make any warranty
	about the software, its performance or its conformity to any specification.

*******************************************************************************/
package com.sap.spc.remote.shared.command;

/**
 * Adds a one or more new pricing conditions to a pricing item, if the corresponding condition type exists
 * in the pricing procedure and the customizing of the condition type allows manual entries.
 *
 * <b>Import parameters</b>
 *
 * <ul>
 *     <li>documentId (required):			the document id of the items we're interested in
 *     <li>itemIds (optional):				the array of item ids of the items we're interested in
 *                                          (if not given, the request is for header conditions)
 *     <li>conditionTypes (required):		the array of names of the conditon types
 *     <li>conditionRates (optional):		the array of condition rates (external and locale dependent representation if import parameter 'formatValue' is set)
 *     <li>conditionCurrencies (optional):	the array of names of the condition currency units
 *     <li>pricingUnitValues (optional):	the array of values of the pricing units (external and locale dependent representation if import parameter 'formatValue' is set)
 *     <li>pricingUnitUnits (optional):		the array of unit names of the pricing units (external and locale dependent representation if import parameter 'formatValue' is set)
 *     <li>conditionValues (optional):		the array of condition values (in an external (locale dependant) representation for 'formatValue' == "Y" or
 *											in an internal (locale independant) representation for 'formatValue' == "N")
 *     <li>formatValue (optional):			"Y"/"N" toggle: "Y": language dependent external representation, "N": internal and language independent representation (default value is "Y").
 *     <li>suppressChecks (optional):		the process mode: "Y" (legacy mode), "N" (manual mode) or "A" (external determination mode).
 * </ul><p>
 *
 * All arrays of the import parameters are of equal length.
 *
 * <b>Export parameters</b>
 * <ul>
 *     <li>itemIds:							the array of item ids
 *     <li>conditionTypes:					the array of names of the conditon types we're interested in
 *     <li>stepNumbers:						the array of step numbers of the new pricing conditions
 *     <li>conditionCounters:				the array of counters of the new pricing conditions
 *     <li>objectIds:						an array of object ids the messages are related to
 *     <li>messages:						an array of error messages
 *     <li>messageTypes:					indicates the severity level of the corresponding error message
 * </ul>
 *
 * The command guarantees that all returned arrays are of equal length.
 */
public interface AddPricingConditions {

	// input parameter
	public static final String DOCUMENT_ID								= "documentId";
	public static final String ITEM_IDS									= "itemIds";
	public static final String CONDITION_TYPES							= "conditionTypes";
	public static final String CONDITION_RATES							= "conditionRates";
	public static final String CONDITION_CURRENCIES						= "conditionCurrencies";
	public static final String PRICING_UNIT_VALUES						= "pricingUnitValues";
	public static final String PRICING_UNIT_UNITS						= "pricingUnitUnits";
	public static final String CONDITION_VALUES							= "conditionValues";
	public static final String FORMAT_VALUE								= "formatValue";
	public static final String SUPPRESS_CHECKS  						= "suppressChecks";

	// output parameter
	public static final String REF_ITEM_IDS								= "refItemIds";
	public static final String REF_CONDITION_TYPES						= "refConditionTypes";
	public static final String STEP_NUMBERS								= "stepNumbers";
	public static final String CONDITION_COUNTERS						= "conditionCounters";
	public static final String OBJECT_IDS						    	= "objectIds";
	public static final String MESSAGES		    						= "messages";
	public static final String MESSAGE_TYPES		    				= "messageTypes";

}
