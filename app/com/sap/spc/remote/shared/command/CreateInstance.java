/************************************************************************

	Copyright (c) 1999 by SAP AG

	All rights to both implementation and design are reserved.

	Use and copying of this software and preparation of derivative works
	based upon this software are not permitted.

	Distribution of this software is restricted. SAP does not make any
	warranty about the software, its performance or its conformity to any
	specification.

**************************************************************************/

package com.sap.spc.remote.shared.command;


/**
 * Creates a new instance as part of another instance. If the instance could not
 * be created, null is returned. (ie. ((null)) or whatever the value of SPC_NULL_STRING holds).<p>
 *
 * <b>Import parameters</b>
 *
 * <ul>
 *     <li>documentId (required):	the document id of the item being configured
 *     <li>itemId (required):		the item id of the item being configured
 *     <li>parentInstId (required):	the instance id of the parent instance
 *     <li>decompPosNr (required):	the decomp position number at which the instance will be created
 * </ul><p>
 *
 * <b>Export parameters</b>
 *
 * <ul>
 *     <li>instId:	the instance number of the new instance, or the spc null string.
 * </ul>
 */
public interface CreateInstance extends SCECommand
{
	public static final String PARENT_INST_ID   = "parentInstId";
	public static final String INST_ID			= "instId";
	public static final String DECOMP_POS_NR	= "decompPosNr";
}
