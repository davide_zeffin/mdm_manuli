/************************************************************************

	Copyright (c) 2000 by SAP AG

	All rights to both implementation and design are reserved.

	Use and copying of this software and preparation of derivative works
	based upon this software are not permitted.

	Distribution of this software is restricted. SAP does not make any
	warranty about the software, its performance or its conformity to any
	specification.

**************************************************************************/

package com.sap.spc.remote.shared.command;

/**
 * Changes the configuration of an item. If no configuration is provided, ie. if
 * extConfigIds is not available, the configuration is not updated, but the other
 * configuration properties are.<p>
 *
 * Please note that it is not possible to change the configuration of a product variant.
 * If you call ChangeItemConfig on a product variant item, no operation will be performed.<p>
 *
 * <b>Import parameters</b>
 *
 * <ul>
 *     <li>documentId (required):	the document that contains the item
 *	   <li>itemId (required):		the item to change
 *     <li>kbDate (optional):       the target date for the knowledge base determination. This is only used if
 *                                  the external configuration doesn't contain kbName or kbVersion. If it is
 *                                  not provided either the first kb found for the root material in the external
 *                                  configuration is used.
 *     <li>external configuration data as described in ExtConfigConstants (all optional but with
 *         a consistency restriction on the length of arrays that belong to the same set of data) (including position guid/instance id map)
 *     <li>sceContextNames, sceContextValues: holds context data for reference characteristics. If provided,
 *         the corresponding table will be changed in the configuration.
 *         <br>
 * 		   Please note that if you pass one or more context parameters to IPC, this will also trigger IPC's own
 *         "context defaulting" where reasonable context values will be determined for the context fields that
 *         IPC knows from its product master or from document / item attributes.
 *     <li>subItemsAllowed (optional): "Y"/"N" toggle: are sub items supported by this item?
 *     <li>onlyVarFind (optional):              "only product variant finding mode": A configuration of a KMAT is <em>always</em> incomplete. Only product
 *                                              variant configs are complete (required for generic articles).
 *     <li>resetConfig (optional): "Y"/"N" toggle: reset the configuration or not
 * </ul><p>
 *
 * <b>Export parameters</b>
 *
 * <ul>
 *     <li>none
 * </ul>
 *
 * The command guarantees that all returned arrays are of equal length.<p>
 *
 * This command supports client-specific parameter naming for all input parameters.<p>
 */
public interface ChangeItemConfig
{
    public static final String DOCUMENT_ID                       = "documentId";
    public static final String ITEM_ID                           = "itemId";
    public static final String KB_DATE                           = "kbDate";
    public static final String EXT_CONFIG_POSITIONS              = "extConfigPositions";
    public static final String EXT_CONFIG_IDS                    = "extConfigIds";
    public static final String EXT_CONFIG_ROOT_IDS               = "extConfigRootIds";
    public static final String EXT_CONFIG_SCE_MODES              = "extConfigSceModes";
    public static final String EXT_CONFIG_KB_LOGSYS              = "extConfigKBLogsys";
    public static final String EXT_CONFIG_KB_NAMES               = "extConfigKBNames";
    public static final String EXT_CONFIG_KB_VERSIONS            = "extConfigKBVersions";
    public static final String EXT_CONFIG_KB_PROFILES            = "extConfigKBProfiles";
    public static final String EXT_CONFIG_KB_BUILDS              = "extConfigKBBuilds";
    public static final String EXT_CONFIG_KB_LANGUAGES           = "extConfigKBLanguages";
    public static final String EXT_CONFIG_NAMES                  = "extConfigNames";
    public static final String EXT_CONFIG_INFOS                  = "extConfigInfos";
    public static final String EXT_CONFIG_COMPLETES              = "extConfigCompletes";
    public static final String EXT_CONFIG_CONSISTENTS            = "extConfigConsistents";
    public static final String EXT_CONFIG_SCE_VERSIONS           = "extConfigSceVersions";
    public static final String EXT_PART_CONFIG_IDS               = "extPartConfigIds";
    public static final String EXT_PART_PARENT_IDS               = "extPartParentIds";
    public static final String EXT_PART_INST_IDS                 = "extPartInstIds";
    public static final String EXT_PART_POS_NRS                  = "extPartPosNrs";
    public static final String EXT_PART_OBJECT_TYPES             = "extPartObjectTypes";
    public static final String EXT_PART_CLASS_TYPES              = "extPartClassTypes";
    public static final String EXT_PART_OBJECT_KEYS              = "extPartObjectKeys";
    public static final String EXT_PART_AUTHORS                  = "extPartAuthors";
    public static final String EXT_PART_SALES_RELEVANTS          = "extPartSalesRelevants";
    public static final String EXT_INST_CONFIG_IDS               = "extInstConfigIds";
    public static final String EXT_INST_IDS                      = "extInstIds";
    public static final String EXT_INST_OBJECT_TYPES             = "extInstObjectTypes";
    public static final String EXT_INST_CLASS_TYPES              = "extInstClassTypes";
    public static final String EXT_INST_OBJECT_KEYS              = "extInstObjectKeys";
    public static final String EXT_INST_OBJECT_TEXTS             = "extInstObjectTexts";
    public static final String EXT_INST_QUANTITIES               = "extInstQuantities";
    public static final String EXT_INST_QUANTITY_UNITS           = "extInstQuantityUnits";
    public static final String EXT_INST_AUTHORS                  = "extInstAuthors";
    public static final String EXT_INST_COMPLETES                = "extInstCompletes";
    public static final String EXT_INST_CONSISTENTS              = "extInstConsistents";
    public static final String EXT_VALUE_CONFIG_IDS              = "extValueConfigIds";
    public static final String EXT_VALUE_INST_IDS                = "extValueInstIds";
    public static final String EXT_VALUE_CSTIC_NAMES             = "extValueCsticNames";
    public static final String EXT_VALUE_CSTIC_LNAMES            = "extValueCsticLNames";
    public static final String EXT_VALUE_NAMES                   = "extValueNames";
    public static final String EXT_VALUE_LNAMES                  = "extValueLNames";
    public static final String EXT_VALUE_AUTHORS                 = "extValueAuthors";
    public static final String EXT_PRICE_CONFIG_IDS              = "extPriceConfigIds";
    public static final String EXT_PRICE_INSTANCE_IDS            = "extPriceInstanceIds";
    public static final String EXT_PRICE_FACTORS                 = "extPriceFactors";
    public static final String EXT_PRICE_KEYS                    = "extPriceKeys";
	public static final String SUB_ITEMS_ALLOWED                 = "subItemsAllowed";
	public static final String EXT_POS_GUID                      = "extPosItemIds";
	public static final String EXT_POS_CONFIG_ID                 = "extPosConfigIds";
	public static final String EXT_POS_INST_ID                   = "extPosInstIds";
	public static final String SCE_CONTEXT_NAME                  = "sceContextNames";
	public static final String SCE_CONTEXT_VALUE                 = "sceContextValues";
	public static final String ONLY_VAR_FIND                     = "onlyVarFind";
	public static final String RESET_CONFIG		                 = "resetConfig";
	public static final String CONFIG_ID                         = "configId";
}

