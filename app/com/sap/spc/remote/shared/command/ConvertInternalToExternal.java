/*
 * Created on May 4, 2005
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package com.sap.spc.remote.shared.command;

/**
 * Returns the external representation from internal ones for the given array of fields 
 * (names, internalValues).<p>
 *
 * <b>Import parameters</b>
 *
 * <ul>
 *		<li>names_input (required):   			the array of field names
 *		<li>internalValues (required):   		the array of the internal values (which needs to be converted)
 * </ul><p>
 *
 * <b>Export parameters</b>
 * <ul>
 *		<li>externalValues:   	the array of external values for the names
 *      <li>errorMessages:		an array of error messages of the faulty names (nulls if no error)
 * </ul><p>
 *
 */
public interface ConvertInternalToExternal {
	// input parameter
	public static final String NAMES 			= "names";
	public static final String INTERNAL_VALUES	= "internalValues";
	
	// output parameter
	public static final String EXTERNAL_VALUES 	= "externalValues";
	public static final String ERROR_MESSAGES 	= "errorMessages";

}
