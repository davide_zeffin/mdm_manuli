/************************************************************************

	Copyright (c) 1999 by SAP AG

	All rights to both implementation and design are reserved.

	Use and copying of this software and preparation of derivative works
	based upon this software are not permitted.

	Distribution of this software is restricted. SAP does not make any
	warranty about the software, its performance or its conformity to any
	specification.

**************************************************************************/
package com.sap.spc.remote.shared.command;

/**
 * Removes a document from the current session.  <p>
 * 
 * <b>Import parameters</b>
 * 
 * <ul>
 *     <li>documentId (required):	the id of the document being removed
 * </ul><p>
 * 
 * <b>Export parameters</b>
 */

public interface RemoveDocument {
	// input parameter
	public static final String DOCUMENT_ID								= "documentId";
}
