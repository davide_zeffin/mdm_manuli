package com.sap.spc.remote.shared.command;
/**
 * Returns header information about a configuration.  <p>
 *
 * <b>Import parameters</b>
 *
 * <ul>
 *     <li>CONFIG_ID (required):	the configuration id 
 *     
 * </ul><p>
 *
 * <b>Export parameters</b>
 *
 * <ul>
 *     <li>cfg_loaded_from_ext: "T"/"F" toggle: is the config loaded from external format?
 *	   <li>msg_lvl:         Maximum level of load messages (E > W > I) that occurred
 *                          during loading.
 *     <li>kb_version_changedt:    "T"/"F" toggle: is this config loaded with 
 *                          a different KB version than originally used for the 
 *                          input configuration.
 * </ul><p>
 */

public interface IGetConfigLoadStatus {
//	export

	 public static final String CFG_LOADED_FROM_EXT = "CFG_LOADED_FROM_EXT";
	 public static final String MSG_LVL             = "MSG_LEVEL";
	 public static final String KB_VERSION_CHANGED  = "KB_VERSION_CHANGED";

}
