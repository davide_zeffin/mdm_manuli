/************************************************************************

	Copyright (c) 1999 by SAP AG

	All rights to both implementation and design are reserved.

	Use and copying of this software and preparation of derivative works
	based upon this software are not permitted.

	Distribution of this software is restricted. SAP does not make any
	warranty about the software, its performance or its conformity to any
	specification.

**************************************************************************/
package com.sap.spc.remote.shared.command;

/**
 * Returns an array of pricing conditions for all items of a document.  <p>
 *
 * <b>Import parameters</b>
 *
 * <ul>
 *     <li>documentId (required):	the id of the document we are interested in
 *     <li>formatValue (optional):	"Y"/"N" toggle: "Y": the values of the export parameters are converted to strings in an external and locale dependent representation (default value is "Y")
 *     <li>r3Value (optional):		"Y"/"N" toggle: "Y": the values of the export parameters are converted to strings in the R/3 "database" format; the input parameter 'formatValue' have to set to "N" (default value is "N")
 *     <li>includeSubtotals (optional):         "Y"/"N" toggle: include/exclude subtotals, respectively
 *     <li>includeHeaderConditions (optional):  "Y"/"N" toggle: include/exclude header conditions
 *     <li>onlyHeaderConditions (optional):     "Y"/"N" toggle: "Y": returns only the header conditions, the parameter sourceItemIds and includeHeaderConditions are ignored (default value is "N")
 *     <li>fieldFilterType (optional):          a character that decides which fields are requested ('A' for leasing)
 *     <li>sourceItemIds (optional):            an array of (internal) item ids of the requested items.
 * </ul><p>
 *
 * <b>Export parameters</b>
 * <ul>
 *     <li>clients:											R/3-field: KONV-MANDT
 *     <li>documentIds:										R/3-field: KONV-KNUMV
 *     <li>itemIds:											R/3-field: KONV-KPOSN
 *     <li>procedureStepNumbers:							R/3-field: KONV-STUNR
 *     <li>conditionCounters:								R/3-field: KONV-ZAEHK
 *     <li>applications:									R/3-field: KONV-KAPPL
 *     <li>conditionTypes:									R/3-field: KONV-KSCHL, may be null
 *     <li>purposes											Purpose, may be null
 *     <li>pricingDates:									R/3-field: KONV-KDATU, may be null, formatValue='Y': external representation, 'N': YYYYMMDD format
 *     <li>calculationTypes:								R/3-field: KONV-KRECH
 *     <li>conditionBaseValues:								R/3-field: KONV-KAWRT, external and locale dependent representation if import parameter 'formatValue' is set
 *     <li>conditionRates:									R/3-field: KONV-KBETR, external and locale dependent representation if import parameter 'formatValue' is set
 *     <li>conditionCurrencies:								R/3-field: KONV-WAERS, external and locale dependent representation if import parameter 'formatValue' is set
 *     <li>conditionExchangeRates:							R/3-field: KONV-KKURS, may be null
 *     <li>conditionPricingUnitValues:						R/3-field: KONV-KPEIN, may be null, external and locale dependent representation if import parameter 'formatValue' is set
 *     <li>conditionPricingUnitUnits:						R/3-field: KONV-KMEIN, may be null, external and locale dependent representation if import parameter 'formatValue' is set
 *     <li>conversionNumeratorConditionUnit2BaseUnits:		R/3-field: KONV-KUMZA
 *     <li>conversionDenominatorConditionUnit2BaseUnits:	R/3-field: KONV-KUMNE
 *     <li>conversionExponentConditionUnitToBaseUnits:	    R/3-field: KONV-EXPNT
 *     <li>conditionCategories:								R/3-field: KONV-KNTYP
 *     <li>indicatorsStatistical:							R/3-field: KONV-KSTAT, "Y"/"N" toggle
 *     <li>scaleTypes:										R/3-field: KONV-KNPRS
 *     <li>indicatorsAccruals:								R/3-field: KONV-KRUEK, "Y"/"N" toggle
 *     <li>indicatorsInvoiceList:							R/3-field: KONV-KRELI, "Y"/"N" toggle
 *     <li>conditionOrigins:								R/3-field: KONV-KHERK
 *     <li>indicatorsGroupCondition:						R/3-field: KONV-KGRPE, "Y"/"N" toggle
 *     <li>indicatorsConditionUpdate:						R/3-field: KONV-KOUPD, "Y"/"N" toggle
 *     <li>accessNumbers:									R/3-field: KONV-KOLNR (deprecated)
 *     <li>conditionRecordNumbers:							R/3-field: KONV-KNUMH, may be null
 *     <li>conditionSequentialNumbers:						R/3-field: KONV-KOPOS
 *     <li>accountKeys:										R/3-field: KONV-KVSL1, may be null
 *     <li>accountKeysAccrualsOrProvisions:					R/3-field: KONV-KVSL2, may be null
 *     <li>roundingOffDifferences:							R/3-field: KONV-KDIFF, external and locale dependent representation if import parameter 'formatValue' is set
 *     <li>conditionValues:									R/3-field: KONV-KWERT, external and locale dependent representation if import parameter 'formatValue' is set
 *     <li>conditionControls:								R/3-field: KONV-KSTEU
 *     <li>conditionIsInactives:							R/3-field: KONV-KINAK
 *     <li>conditionClasses:								R/3-field: KONV-KOAID
 *     <li>headerConditionCounters:							R/3-field: KONV-ZAEKO
 *     <li>conditionBaseValueFactors:						R/3-field: KONV-KFAKTOR, may be null
 *     <li>indicatorsScaleBasis:							R/3-field: KONV-KZBZG
 *     <li>scaleBaseValues:									R/3-field: KONV-KSTBS, may be null, external and locale dependent representation if import parameter 'formatValue' is set
 *     <li>scaleUnits:										R/3-field: KONV-KONMS, may be null, external and locale dependent representation if import parameter 'formatValue' is set
 *     <li>scaleCurrencies:									R/3-field: KONV-KONWS, may be null, external and locale dependent representation if import parameter 'formatValue' is set
 *     <li>indicatorsConditionInterCompanyBilling:			R/3-field: KONV-KFKIV, "Y"/"N" toggle
 *     <li>indicatorsConditionConfiguration:				R/3-field: KONV-KVARC, "Y"/"N" toggle
 *     <li>indicatorsConditionChangedManually:				R/3-field: KONV-KMPRS, "Y"/"N" toggle
 *     <li>variantConditions:								R/3-field: KONV-VARCOND, may be null
 *     <li>salesTaxCode:    								R/3-field: KONV-MWSK1, may be null
 *     <li>withholdingTaxCode:   							R/3-field: KONV-MWSK2, may be null
 *     <li>indicatorsStructureCondition:    				R/3-field: KONV-KDUPL
 *     <li>dataSource:             							R/3-field: KONV-DTASRC, may be null
 *     <li>conditionBaseValueFactorPeriods:					R/3-field: KONV-KFAKTOR1, may be null
 *     <li>manualEntryFlags:								Manual entry of condition allowed, "true": "false"
 *     <li>changeOfRatesAllowed:							Manual change of condition rates allowed, "true": "false"
 *     <li>changeOfUnitsAllowed:							Manual change of condition units allowed, "true": "false"
 *     <li>changeOfValuesAllowed:							Manual change of condition values allowed, "true": "false"
 *     <li>changeOfCalculationTypesAllowed:					Manual change of condition calculation types allowed, "true": "false"
 *     <li>changeOfConversionFactorsAllowed:				Manual change of condition conversion factors allowed, "true": "false"
* </ul>
 *
 * The command guarantees that all arrays are of the same length.<p>
 *
 * This command supports client-specific parameter naming for all output parameters.<p>
 */

public interface GetAllItemsConditions extends GetConditions {
	// input parameter
	public static final String DOCUMENT_ID		            = "documentId";
	public static final String FORMAT_VALUE		            = "formatValue";
	public static final String R3_VALUE		                = "r3Value";
	public static final String INCLUDE_SUBTOTALS	        = "includeSubtotals";
	public static final String INCLUDE_HEADER_CONDITIONS	= "includeHeaderConditions";
	public static final String ONLY_HEADER_CONDITIONS	    = "onlyHeaderConditions";
	public static final String FIELD_FILTER_TYPE            = "fieldFilterType";
	public static final String SOURCE_ITEM_IDS              = "sourceItemIds";

	// output parameter
	// see GetConditions interface
}
