/*
 * Created on 11.06.2004
 *
 * To change the template for this generated file go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
package com.sap.spc.remote.shared.command;

import com.sap.spc.remote.shared.command.SCECommand;

/**
 * @author d031177
 *
 * To change the template for this generated type comment go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
public interface GetAllValidGridVariants extends SCECommand {
	
	//import parameters
	public static final String PRODUCTID = "productId";
	public static final String CSTICS_NAMES = "csticsNames";
	public static final String CSTICS_VALUES = "csticsValues";

}
