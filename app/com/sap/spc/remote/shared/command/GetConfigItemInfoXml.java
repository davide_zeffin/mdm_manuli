/************************************************************************

	Copyright (c) 2001 by SAP AG

	All rights to both implementation and design are reserved.

	Use and copying of this software and preparation of derivative works
	based upon this software are not permitted.

	Distribution of this software is restricted. SAP does not make any
	warranty about the software, its performance or its conformity to any
	specification.

**************************************************************************/

package com.sap.spc.remote.shared.command;


/**
 * Returns the external configuration of a given item as a XML table.<p>
 *
 * <b>Import parameters</b>
 *
 * <ul>
 *     <li>documentId (required):	the document id of the item being configured
 *     <li>itemId (required):		the item id of the item being configured
 * </ul><p>
 *
 * <b>Export parameters</b>
 *
 * <ul>
 *     <li>configuration data (XML): arrays with one entry for each configuration (currently always of length 1)
 *         <ul>
 *             <li>extConfigXml[]:	        the configuration in XML format
 *         </ul>
 * </ul><p>
 *
 * This command supports client-specific parameter naming for all output parameters.<p>
 */
public interface GetConfigItemInfoXml extends SCECommand, ExtConfigConstants
{
	public static final int    EXT_CONFIG_XML_ROWSIZE = 512;
	public static final String EXT_CONFIG_XML         = "extConfigXml";
}
