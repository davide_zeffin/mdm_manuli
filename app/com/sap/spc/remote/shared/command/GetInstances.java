/************************************************************************

	Copyright (c) 1999 by SAP AG

	All rights to both implementation and design are reserved.

	Use and copying of this software and preparation of derivative works
	based upon this software are not permitted.

	Distribution of this software is restricted. SAP does not make any
	warranty about the software, its performance or its conformity to any
	specification.

**************************************************************************/

package com.sap.spc.remote.shared.command;


/**
 * Returns all instances in a configuration. The instances of a configuration form a tree (by
 * their bills of material). This tree is returned by the command. The tree structure is stored
 * in the instPIds array which holds the id of the parent of an instance in the tree. <p>
 *
 * <b>Import parameters</b>
 *
 * <ul>
 *     <li>documentId (required):	the document id of the item being configured
 *     <li>itemId	 (required):	the item id of the item being configured
 *     <li>getMimeObjects (optional): Flag YES/NO - if this is YES, you will receive a list of mime objects
 * </ul><p>
 *
 * <b>Export parameters</b>
 *
 * <ul>
 *     <li>instNames:		an array of all instance names
 *	   <li>instLNames:		an array of all language dependent instance names
 *	   <li>instIds:			an array of all instance identifiers
 *     <li>instPIds:		an array of all parent instance identifiers. Special identifiers are
 *                          "0" for root instances and "-1" for non-part instances.
 *     <li>instUnspecables:	an array of "Y"/"N" entries that describe if the instances can be unspecified
 *     <li>instPrices:      an array of Strings containing pricing information about the instances
 *     <li>instAuthors:		an array of all instance authors: "U" for user, "S" for System
 *     <li>instConflicts:	an array of "Y"/"N" toggles for "is not consistent"
 *     <li>instCompletes:	an array of "Y"/"N" toggles for "is complete"
 *     <li>instImages:      an array of image URLs (null if no image is available)
 *	   <li>instDescriptions:an array of long texts of the instances
 *	   <li>instQuantities:	an array of quantities of the instances
 *	   <li>instPositions:	an array of positions of the instances
 *
 *     <li>mimeInstIds:     an array of instance ids for matching the mime objects with the instances
 *     <li>mimeObjects:     an array of mime object url's
 *     <li>mimeObjectTypes: an array of mime object types (e.g. "image,sound,...")
 * </ul><p>
 *
 * The command guarantees that all arrays are of equal length.
 */
public interface GetInstances extends SCECommand
{
	public static final String INST_NAME		    = "instNames";
	public static final String INST_LNAME		    = "instLNames";
	public static final String INST_ID			    = "instIds";
	public static final String INST_PID			    = "instPIds";
	public static final String INST_UNSPECALBE	    = "instUnspecables";
	public static final String INST_PRICE		    = "instPrices";
	public static final String INST_AUTHOR		    = "instAuthors";
	public static final String INST_CONFLICT	    = "instConflicts";
	public static final String INST_COMPLETE	    = "instCompletes";
	public static final String INST_IMAGE           = "instImages";
	public static final String INST_DESCRIPTION     = "instDescriptions";
	public static final String INST_QUANTITY	    = "instQuantities";
	public static final String INST_POSITION	    = "instPositions";
	public static final String GET_MIME_OBJECTS     = "instGetMimeObjects";

	public static final String MIME_INST_ID	        = "mimeInstIds";
	public static final String MIME_OBJECTS	        = "mimeObjects";
	public static final String MIME_OBJECT_TYPES	= "mimeObjectTypes";

	public static final String ROOT				    = "0";
	public static final String NONPART			    = "-1";
}