/************************************************************************

	Copyright (c) 1999 by SAP AG

	All rights to both implementation and design are reserved.

	Use and copying of this software and preparation of derivative works
	based upon this software are not permitted.

	Distribution of this software is restricted. SAP does not make any
	warranty about the software, its performance or its conformity to any
	specification.

**************************************************************************/
package com.sap.spc.remote.shared.command;

/**
 * Returns all items of a given (sales) document.  <p>
 * 
 * <b>Import parameters</b>
 * 
 * <ul>
 *     <li>documentId (required):	the id of the document we're interested in
 * </ul><p>
 * 
 * <b>Export parameters</b>
 * 
 * <ul>
 *     <li>itemIds[]:				the array of the (top-level) items in this document
 * </ul>
 */

public interface GetItems {
	// input parameter
	public static final String DOCUMENT_ID								= "documentId";
	
	// output parameter
	public static final String ITEM_IDS									= "itemIds";
}
