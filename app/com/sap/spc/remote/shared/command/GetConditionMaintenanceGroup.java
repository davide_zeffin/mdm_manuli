/************************************************************************

Copyright (c) 1999 by SAP AG

All rights to both implementation and design are reserved.

Use and copying of this software and preparation of derivative works
based upon this software are not permitted.

Distribution of this software is restricted. SAP does not make any
warranty about the software, its performance or its conformity to any
specification.

**************************************************************************/
package com.sap.spc.remote.shared.command;

public interface GetConditionMaintenanceGroup {
    
    public static final String USAGE = "usage";
    public static final String CONTEXT_NAME = "contextName";
    public static final String GROUP_NAMES = "groupNames";
    public static final String IS_DEFAULT = " isDefault";
}
