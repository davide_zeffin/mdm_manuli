/*
 * Created on May 4, 2005
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package com.sap.spc.remote.shared.command;

/**
 * Returns the internal representation from external ones for the given array of fields 
 * (names, externalValues).<p>
 *
 * <b>Import parameters</b>
 *
 * <ul>
 *		<li>names_input (required):   			the array of field names
 *		<li>externalValues (required):   	the array of external values (which needs to be converted)
 *
 * This cammand also converts ranges. If in the names_input at 2 position if the same fieldname appears, then that field is
 * converted to ranges and output gives back the ranges converted in individual values in the internal format.
 *
 * </ul><p>
 *
 * <b>Export parameters</b>
 * <ul>
 *		<li>internalValues:   				the array of the internal values for the names
 *      <li>errorMessages:					an array of error messages of the faulty names (nulls if no error)
 * 		<li>names_output:					an array of field names
 * </ul><p>
 *
 */

public interface ConvertExternalToInternal {
	// input parameter
	public static final String NAMES_INPUT		= "names_input";
	public static final String EXTERNAL_VALUES 	= "externalValues";
	
	// output parameter
	public static final String INTERNAL_VALUES	= "internalValues";
	public static final String ERROR_MESSAGES 	= "errorMessages";
	public static final String NAMES_OUTPUT 	= "names_output";
}
