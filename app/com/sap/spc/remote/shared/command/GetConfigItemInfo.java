/************************************************************************

	Copyright (c) 1999 by SAP AG

	All rights to both implementation and design are reserved.

	Use and copying of this software and preparation of derivative works
	based upon this software are not permitted.

	Distribution of this software is restricted. SAP does not make any
	warranty about the software, its performance or its conformity to any
	specification.

**************************************************************************/

package com.sap.spc.remote.shared.command;


/**
 * Returns the external configuration of a given item plus information to match the configuration
 * instances with the respective items. The format of the external configuration complies to the
 * common CU* table structure. For R/3-like parameter names (eg. CFG-KBVERSION instead of
 * extConfigKBVersions) switch to a different client parameter naming convention, for example
 * with command SetClientParameters, before calling this command).<p>
 *
 * <b>Import parameters</b>
 *
 * <ul>
 *     <li>documentId (required):	the document id of the item being configured
 *     <li>itemId (required):		the item id of the item being configured
 * </ul><p>
 *
 * <b>Export parameters</b>
 *
 * <ul>
 *     <li>configuration data (BAPICUCFG): arrays with one entry for each configuration (currently always of length 1)
 *         <ul>
 *             <li>extConfigPositions[]:	the position number of each configuration (currently always "0")
 *             <li>extConfigIds[]:			a unique id for each configuration (currently always "0")
 *             <li>extConfigRootIds[]:		the id of the root instance...
 *             <li>extConfigSceModes[]:		"1"/"0" toggle: the config uses SCE mode ("0": R/3 VC mode)
 *             <li>extConfigKBNames[]:		the knowledge base name of every config.
 *             <li>extConfigKBVersions[]:	the knowledge base version...
 *             <li>extConfigKBProfiles[]:	the knowledge base profile...
 *             <li>extConfigKBBuilds[]:		the knowledge base build number...
 *             <li>extConfigKBLanguages[]:	the language used in all language dependent fields in this external config
 *             <li>extConfigNames[]:		the name of the configuration (currently always empty)
 *             <li>extConfigCompletes[]:	"T"/"F" toggle: config is complete
 *             <li>extConfigConsistents[]:	"T"/"F" toggle: config is consistent (free of conflicts)
 *         </ul>
 *         The command guarantees that all configuration arrays are of equal length.
 *     <li>part-of data (BAPICUPRT): arrays with one entry per part-of relationship - one array for all Configs
 *         <ul>
 *             <li>extPartConfigIds[]:		the id of the configuration to which this part belongs
 *             <li>extPartInstanceIds[]:	the instance id of each part
 *             <li>extPartParentIds[]:		the instance id of the parent instance of each part
 *			   <li>extPartPosNrs[]:			the position number of the BOM (decomp) position that contains this part
 *             <li>extPartObjectTypes[]:	the object type of this part (kbrt.object_type string constants)
 *             <li>extPartClassTypes[]:		the class type of this part (eg. "300")
 *             <li>extPartObjectKeys[]:		the object key of this part (class name for a class, material number for a material)
 *             <li>extPartAuthors[]:		minimal author information
 *             <li>extPartIsSalesRelevant[]:"X"/" " toggle: is this part sales relevant?
 *         </ul>
 *		   The command guarantees that all part-of data arrays are of equal length
 *     <li>instance data (BAPICUINS): arrays with one entry per instance in each config.
 *         <ul>
 *             <li>extInstConfigIds[]:		the id of the configuration to which this instance belongs
 *             <li>extInstIds[]:			the instance id of the instance
 *			   <li>extInstObjectTypes[]:	the object type of this instance (kbrt.object_type string constants)
 *			   <li>extInstClassTypes[]:		the class type of this instance (eg. "300")
 *			   <li>extInstObjectKeys[]:		the object key of this instance (class name for a class, material number for a material)
 *             <li>extInstObjectTexts[]:	language dependent text of this instance (in extConfigKBLangage)
 *             <li>extInstQuantities[]:		the quantity of this instance
 *             <li>extInstQuantityUnits[]:  the quantity unit of this instance
 *	           <li>extInstAuthors[]:		minimal author information for the instance type
 *             <li>extInstCompletes[]:		"T"/"F" toggle: is this instance complete?
 *			   <li>extInstConsistents[]:	"T"/"F" toggle: is this instance consistent (conflict-free)
 *         </ul>
 *         The command guarantees that all instance data arrays are of equal length
 *     <li>cstic value data (BAPICUVAL): arrays with one entry per cstic value that has been set in any config.
 *         <ul>
 *             <li>extValueConfigIds[]:		the id of the configuration to which this value belongs
 *             <li>extValueInstanceIds[]:	the id of the instance to which this value belongs
 *			   <li>extValueCsticNames[]:	the name of the cstic to which this value belongs
 *             <li>extValueCsticLNames[]:	the language dependent name of the cstic ...
 *             <li>extValueNames[]:			the name of the value
 *             <li>extValueLNames[]:		the language dependent name of the value
 *             <li>extValueAuthors[]:		minimal author information
 *         </ul>
 *         The command guarantees that all value data arrays are of equal length
 *     <li>pricing data (?): arrays with one entry per price condition key that has been set in any config.
 *         <ul>
 *             <li>extPriceConfigIds[]:		the id of the configuration to which this condition key belongs
 *             <li>extPriceInstanceIds[]:   the id of the instance to which this condition key belongs
 *             <li>extPriceFactors[]:		the factor of this price condition
 *             <li>extPriceKeys[]:			the condition key
 *         </ul>
 *		   The command guarantees that all price data arrays are of equal length
 *		<li>item data: for each instance that corresponds to an item (ie. that holds a product that
 *          is sales relevant, the arrays hold information about that item.
 *          <ul>
 *				<li>extPosItemIds[]:		the item id of each item in the config
 *				<li>extPosConfigIds[]:		the config id
 *				<li>extPosInstIds[]:		the instance id of each item in the config
 *          </ul>
 *      <li>resultProductId:                the current product id held by this item. Since an item may
 *                                          change its product from product variants from/to KMATs, this
 *                                          id is necessary to determine if/which variant is currently
 *                                          used (or if a normal KMAT is used).
 *      <li>resultProductLogSys:            the logical system of the product held by this item.
 *      <li>resultProductType:              the product type of the product held by this item.
 *      </ul>
 *   <p>
 *
 * This command supports client-specific parameter naming for all output parameters.<p>
 */
public interface GetConfigItemInfo extends SCECommand, ExtConfigConstants
{
	public static final String INST_IDS = "instIds";
	public static final String INST_ITEM_IDS = "instItemIds";
	public static final String INST_ITEM_GUIDS = "instItemGuids";
	public static final String INST_PRODUCT_IDS = "instProductIds";
	public static final String INST_QUANTITY_VALUE = "instQuantityValue";
	public static final String INST_QUANTITY_UNIT = "instQuantityUnit";

	public static final String RESULT_PRODUCT_ID = "resultProductId";
	public static final String RESULT_PRODUCT_LOG_SYS = "resultProductLogSys";
	public static final String RESULT_PRODUCT_TYPE = "resultProductType";
}
