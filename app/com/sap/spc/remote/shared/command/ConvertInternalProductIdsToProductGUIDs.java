/*******************************************************************************

Copyright (c) 2005 by SAP AG

All rights to both implementation and design are reserved.

Use and copying of this software and preparation of derivative works based
upon this software are not permitted.

Distribution of this software is restricted. SAP does not make any warranty
about the software, its performance or its conformity to any specification.

*******************************************************************************/
package com.sap.spc.remote.shared.command;

/**
 * Returns the internal representation for products (GUID) from internal ones 
 * (internal id, logical system, product type).<p>
 *
 * <b>Import parameters</b>
 *
 * <ul>
 *		<li>productIds (required):   			the array of product ids
 *		<li>productLogSys (optional):   		the array of logical systems to each product id
 *		<li>productType (optional):   			the product type of each product id
 * </ul><p>
 *
 * <b>Export parameters</b>
 * <ul>
 *		<li>productGUIDs:   				    the array of the internal keys for the products
 *      <li>errorMessages:						an array of error messages of the faulty product ids
 * </ul><p>
 *
 */

public interface ConvertInternalProductIdsToProductGUIDs {

	// input parameter
	public static final String PRODUCT_IDS = "productIds";
	public static final String PRODUCT_LOGSYS = "productLogicalSystems";
	public static final String PRODUCT_TYPES = "productTypes";
	
	// output parameter
	public static final String PRODUCT_GUIDS = "productGUIDs";
	public static final String ERROR_MESSAGES = "errorMessages";
}
