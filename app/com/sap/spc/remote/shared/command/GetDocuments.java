/************************************************************************

	Copyright (c) 1999 by SAP AG

	All rights to both implementation and design are reserved.

	Use and copying of this software and preparation of derivative works
	based upon this software are not permitted.

	Distribution of this software is restricted. SAP does not make any
	warranty about the software, its performance or its conformity to any
	specification.

**************************************************************************/
package com.sap.spc.remote.shared.command;

/**
 * Returns all documents of the current session.  <p>
 * 
 * <b>Import parameters</b>
 * <p>
 * 
 * <b>Export parameters</b>
 * 
 * <ul>
 *     <li>documentIds:								an array of all document ids
 * </ul>
 */

public interface GetDocuments {
	// output parameter
	public static final String DOCUMENT_IDS								= "documentIds";
}
