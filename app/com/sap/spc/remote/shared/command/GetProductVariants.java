/************************************************************************

	Copyright (c) 1999 by SAP AG

	All rights to both implementation and design are reserved.

	Use and copying of this software and preparation of derivative works
	based upon this software are not permitted.

	Distribution of this software is restricted. SAP does not make any
	warranty about the software, its performance or its conformity to any
	specification.

**************************************************************************/

package com.sap.spc.remote.shared.command;


/**
 * Returns a number of matching product variants for a configurable or
 * product variant item. This process may be not supported by a product
 * variant. If this is the case, no record will be returned.  <p>
 *
 * <b>Import parameters</b>
 *
 * <ul>
 *     <li>documentId (required):	the document id of the item being requested
 *     <li>itemId (required):		the item id of the item being requested
 *     <li>limit (optional):		limit of how many matching product variants
 *                                  should be returned.
 *     <li>quality (optional):		describes a quality range in which the result
 *									set should be. If 1.0 is the quality of the
 *									best case, the parameter describes how good the
 *									"worst case we're interested in" should be.
 *									Specify 0.9 to return only those cases that
 *									are at least 90% as good as the best case found.
 *     <li>filterKeys (optional):	array of filter keys (e.g. "VKORG")
 *     <li>filterValues (optional): array of filter values (e.g. "0001")
 *     <li>targetDate (optional):	target availability date. This date will determine,
 *                                  which KB will be used as a reference for the search.
 *     <li>exactMode (optional):	should we search exact ("Y") or fuzzy ("N")? In
 *									IPC 2.0b, only limited support for fuzzy search is available.
 *     <li>maxMatchPoint (optional):how many points should a perfect match be? Think of
 *									points as the stars that show the quality in some
 *									search engines (5 stars being best, 0 stars worst).
 *                                  A logarithmic scale is being used, ie. *** is twice
 *                                  as good as **.
 * </ul><p>
 *
 * The command refuses to accept searches where neither a limit nor a quality
 * is provided (these searches would return all cases). Furthermore, the arrays
 * filterKeys and filterValues have to be equally long. If one of these conditions
 * is violated, the command will fail with an error.
 *
 * <b>Export parameters</b>
 *
 * <ul>
 *     <li>variantCount:			the number of product variants found. If a limit is
 *									specified, variantCount will not exceed limit.
 *     <li>variantProductIds (n):	array of product ids (n = variantCount).
 *     <li>distances (n):			distance of the variant (0..infinity)
 *     <li>matchPoints (n):			how good each match is (0..maxMatchPoint, derived from distance).
 *     <li>featureProductIds (n*m):	for each feature, back reference to the product,
 *									where m is the number of differing features of a match.
 *     <li>featureTypes (n*m):		what kind of feature, currently only: CSTIC.
 *	   <li>featureIds (n*m):		string identifying the feature (eg. "KBCA_A0001.KBCA_TECHNOLOGY").
 *	   <li>featureNames (n*m):		language dependent name of the feature (may be null)
 *     <li>featureLongtexts (n*m):  language dependent long text of the feature (may be null)
 *     <li>featureValues (n*m):		the value of the feature for that product variant
 *     <li>featureWeights (n*m):	doubles holding the feature weights
 *     <li>featureDistances (n*m):	doubles holding the contribution of the feature to the distance.
 * </ul>
 *
 * The command guarantees that all n are equal, and that the m of the i-th product variant
 * are equal. The language used to return language dependent texts is the language of the
 * document.<p>
 */

public interface GetProductVariants extends SCECommand
{
	// import parameters
	public static final String LIMIT				= "limit";
	public static final String QUALITY				= "quality";
	public static final String FILTER_KEYS			= "filterKeys";
	public static final String FILTER_VALUES		= "filterValues";
	public static final String TARGET_DATE			= "targetDate";
	public static final String EXACT_MODE			= "exactMode";
	public static final String MAX_MATCH_POINT		= "maxMatchPoint";
    public static final String SEARCH_MODE          = "searchMode";

	// export parameters
	public static final String VARIANT_COUNT		= "variantCount";
	public static final String VARIANT_PRODUCT_IDS	= "variantProductIds";
	public static final String DISTANCE				= "distances";
	public static final String MATCH_POINTS			= "matchPoints";
	public static final String FEATURE_PRODUCT_IDS	= "featureProductIds";
	public static final String FEATURE_TYPES		= "featureTypes";
	public static final String FEATURE_IDS			= "featureIds";
	public static final String FEATURE_NAMES		= "featureNames";
	public static final String FEATURE_LNAMES       = "featureLNames";
	public static final String FEATURE_LONGTEXTS	= "featureLongtexts";
	public static final String FEATURE_VALUES		= "featureValues";
	public static final String FEATURE_LVALUES      = "featureLValues";
	public static final String FEATURE_WEIGHTS		= "featureWeights";
	public static final String FEATURE_DISTANCES	= "featureDistances";

	public static final int MAX_MATCH_POINT_DEFAULT_VALUE = 5;
}
