/************************************************************************

	Copyright (c) 1999 by SAP AG

	All rights to both implementation and design are reserved.

	Use and copying of this software and preparation of derivative works
	based upon this software are not permitted.

	Distribution of this software is restricted. SAP does not make any
	warranty about the software, its performance or its conformity to any
	specification.

**************************************************************************/

package com.sap.spc.remote.shared.command;


/**
 * Unspecializes an instance that has previously been specialized.  <p>
 * 
 * <b>Import parameters</b>
 * 
 * <ul>
 *     <li>documentId (required):	the document id of the item being configured
 *     <li>itemId (required):		the item id of the item being configured
 *     <li>instId (required):		the instance id to specialize
 * </ul><p>
 * 
 * <b>Export parameters</b>
 */
public interface Unspecialize extends SCECommand
{
	public static final String INST_ID = "instId";
}