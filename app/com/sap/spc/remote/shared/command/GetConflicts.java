/************************************************************************

	Copyright (c) 1999 by SAP AG

	All rights to both implementation and design are reserved.

	Use and copying of this software and preparation of derivative works
	based upon this software are not permitted.

	Distribution of this software is restricted. SAP does not make any
	warranty about the software, its performance or its conformity to any
	specification.

**************************************************************************/
// change log:
// 20011121-ak: added long text description of conflict (OSS 454791)

package com.sap.spc.remote.shared.command;


/**
 * Returns an array of conflicts.  If the optional instId parameter is defined,
 * it returns only those conflicts that this instance is involved in, else all
 * existing conflicts are returned.<p>
 *
 * <b>Import parameters</b>
 *
 * <ul>
 *     <li>documentId (required):	the document id of the item being configured
 *     <li>itemId (required):		the item id of the item being configured
 *     <li>instId (optional):		the instance id of the instance
 * </ul><p>
 *
 * <b>Export parameters</b>
 *
 * <ul>
 *     <li>conflictInstIds[]:		the instance ids for each conflict
 *     <li>conflictInstLNames[]:	the language dependent names of the instances
 *     <li>conflictTexts[]:			text descriptions of each conflict
 *     <li>conflictAuthors[]:		who owns the conflicts. "S" (System) or "U" (User)
 *     <li>conflictIds[]:		the Id of a justification for the conflict
 *     <li>conflictLongTexts[]:		the long text description of a conflict (for dependency)
 * </ul>
 *
 * The command guarantees that all returned arrays are of equal length
 */
public interface GetConflicts extends SCECommand
{
	public static final String INST_ID				= "instId";
	public static final String CONFLICT_INST_ID		= "conflictInstIds";
	public static final String CONFLICT_INST_LNAME	= "conflictInstLNames";
	public static final String CONFLICT_TEXT		= "conflictTexts";
	public static final String CONFLICT_AUTHOR		= "conflictAuthors";
	public static final String CONFLICT_ID			= "conflictIds";
       	public static final String CONFLICT_LONG_TEXT   = "conflictLongTexts"; // 20011121-ak
}