package com.sap.spc.remote.shared.command;

/**
 * Serves for analysis of communication problems with the IPC.
 * Returns the message from the request.
 */

public interface Echo {
    public static final String MESSAGE = "message";
}
