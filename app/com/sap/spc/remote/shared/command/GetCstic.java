/************************************************************************

	Copyright (c) 2002 by SAP AG

	All rights to both implementation and design are reserved.

	Use and copying of this software and preparation of derivative works
	based upon this software are not permitted.

	Distribution of this software is restricted. SAP does not make any
	warranty about the software, its performance or its conformity to any
	specification.

**************************************************************************/

package com.sap.spc.remote.shared.command;


/**
 * Returns information about all characteristics of a given instance. Please note while most of
 * these data look like they are master data, most of them may actually change during configuration
 * via dependency knowledge (visibility, being read-only, is domain an interval).  <p>
 *
 * <b>Import parameters</b>
 *
 * <ul>
 *     <li>documentId (required):	the document id of the item being configured
 *     <li>itemId (required):		the item id of the item being configured
 *     <li>instId (required):		the instance id of the instance we're interested in
 *	   <li>csticName (required):	the name of the cstic
 *	   <li>withoutDescription
 *						(optional):	if specified, no description will given back
 * </ul><p>
 *
 * <b>Export parameters</b>
 *
 * <ul>
 *     <li>	csticName:				the names of the cstics
 *     <li>	csticLName:				the language independent names
 *	   <li> csticMulti:				"Y"/"N" toggle: is multivalued
 *     <li>	csticAddit:				"Y"/"N" toggle: additional values allowed
 *     <li>	csticDomainConstrained:	"Y"/"N" toggle: is the domain of the cstic constrained
 *	   <li>	csticDomainIsAnInterval:"Y"/"N" toggle: if the domain is a Sequence of intervals
 *     <li>	csticGroup:				group identifiers for all cstics
 *     <li>	csticUnit:				units of measurement of the cstics
 *     <li>	csticReadOnly:			"Y"/"N": is the cstic read only
 *     <li>	csticVisibles:			...: visible
 *     <li>	csticAuthor:			the author of the cstic "U" (User) / "S" (System)
 *     <li>	csticRequired:			"Y"/"N": is the cstic required
 *     <li>	csticConsistent:		"Y"/"N": is the cstic consistent with the rest of the Config
 *	   <li> csticDescription:		the long text for the cstics
 * </ul>
 *
 * The command guarantees that all returned arrays are of equal length
 */

public interface GetCstic extends SCECommand
{
	public static final String INST_ID					= "instId";
	public static final String WITHOUT_DESCRIPTION	    = "withoutDescription";
	public static final String CSTIC_NAME				= "csticName";
	public static final String CSTIC_LNAME				= "csticLName";
	public static final String CSTIC_MULTI				= "csticMulti";
	public static final String CSTIC_ADDIT				= "csticAddit";
	public static final String CSTIC_GROUP				= "csticGroup";
	public static final String CSTIC_UNIT				= "csticUnit";
	public static final String CSTIC_READONLY			= "csticReadOnly";
	public static final String CSTIC_VISIBLE			= "csticVisible";
	public static final String CSTIC_AUTHOR				= "csticAuthor";
	public static final String CSTIC_REQUIRED			= "csticRequired";
	public static final String CSTIC_CONSISTENT			= "csticConsistent";
	public static final String CSTIC_CONSTRAINED		= "csticDomainConstrained";
	public static final String CSTIC_DOMAIN_IS_INTERVAL	= "csticDomainIsAnInterval";
	public static final String CSTIC_DESCRIPTION		= "csticDescription";
}