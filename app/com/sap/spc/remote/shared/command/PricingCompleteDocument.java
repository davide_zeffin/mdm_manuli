/************************************************************************

	Copyright (c) 1999 by SAP AG

	All rights to both implementation and design are reserved.

	Use and copying of this software and preparation of derivative works
	based upon this software are not permitted.

	Distribution of this software is restricted. SAP does not make any
	warranty about the software, its performance or its conformity to any
	specification.

**************************************************************************/
package com.sap.spc.remote.shared.command;

/**
 * Perform pricing for groups of items (this corresponds to the R/3 function 'Pricing_Complete').
 * This method should not be called too often because it is very time consuming.
 * It should usually  be called before a document is saved or transmitted to the R/3 system or 
 * when the SPCDocument is complete.<p>
 * 
 * <b>Import parameters</b>
 * 
 * <ul>
 *     <li>documentId (required):	the id of the document being priced
 * </ul><p>
 * 
 * <b>Export parameters</b>
 */

public interface PricingCompleteDocument {
	// input parameters
	public static final String DOCUMENT_ID						= "documentId";
	// output parameters
	public static final String NET_VALUE						= "netValue"; 
	public static final String TAX_VALUE						= "taxValue";
	public static final String GROSS_VALUE						= "grossValue";
	public static final String OBJECT_IDS						= "objectIds";	
	public static final String MESSAGES							= "messages";
	public static final String MESSAGE_TYPES					= "messageTypes";
}
