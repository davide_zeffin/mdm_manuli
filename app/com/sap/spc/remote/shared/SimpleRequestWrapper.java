package com.sap.spc.remote.shared;

import com.sap.msasrv.socket.shared.*;


public class SimpleRequestWrapper implements SimpleRequest {
	private SharedRequest request;

	public SimpleRequestWrapper(SharedRequest request) {
		this.request = request;
	}

	public String getParameterValue(String name) {
		return request.getParameterValue(name);
	}

	public String getParameterValue(String name, String defaultValue) {
		return request.getParameterValue(name,defaultValue);
	}

	public String[] getParameterValues(String name) {
		return request.getParameterValues(name);
	}

	public String[] getParameterValues(String name, String[] defaultValues) {
		return request.getParameterValues(name,defaultValues);
	}
}