/*
 * Created on Nov 3, 2004
 *
 */
package com.sap.spc.remote.client;

import com.sap.tc.logging.Category;
import com.sap.tc.logging.Location;

/**
 */
public class ResourceAccessor {

	/**
	 * The location in which tracing information is stored.
	 */
	protected static final Location location =
	        ResourceAccessor.getLocation(ResourceAccessor.class);

	/**
	 * The category in which logging information is stored.
	 */
	public static final Category category =
	        Category.getCategory(Category.APPLICATIONS, IClientLogCategories.IPC_CLIENT_CATEGORY);

	/**
	 * Resource bundle name containing the messages for the
	 * conversion functionality.
	 */
	protected static final String RESOURCE_BUNDLE_NAME =
	    "com.sap.spc.remote.client.ResourceBundle";

	static {
	    category.setResourceBundleName(RESOURCE_BUNDLE_NAME);
	}

	public static Location getLocation(Class locClass){
		Location loc = Location.getLocation(locClass);
		loc.setResourceBundleName(RESOURCE_BUNDLE_NAME);
		return loc;
	}
}
