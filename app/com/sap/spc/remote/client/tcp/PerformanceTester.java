package com.sap.spc.remote.client.tcp;

import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Enumeration;
import java.util.Vector;

import com.sap.isa.core.logging.IsaLocation;
import com.sap.msasrv.usermodule.spc.ISPCCommandSet;
import com.sap.spc.remote.shared.command.SetCsticValues;
import com.sap.spc.remote.client.ClientException;

public class PerformanceTester
{
	protected static IsaLocation log = IsaLocation.getInstance(PerformanceTester.class.getName());

	private static final int THREAD_COUNT = 90;

	private static PrintWriter out;

	private Vector[] clients;  // one vector for each thread, each containing ClientSupport objects
	private static int commands;
	private Thread[] allthreads;
	private static Boolean isRFC = null;    // null for "not determined" or a correct boolean value
	private static com.sap.spc.remote.client.tcp.TcpClientSupport anyClient = null;
	private int count;

	public static void main(String[] args) {
		try {
			out = new PrintWriter(new FileWriter("C:\\TEMP\\IPCPERFORMANCE.LOG"));
		}
		catch(IOException e) {
			System.err.println("Couldn't open log file");
			System.exit(1);
		}

		out.println("Preparing to test...");
		new PerformanceTester(1).test();
		System.gc();
		for (int testSize=10; testSize < 641; testSize = testSize*2) {
		//for (int testSize=1; testSize < 20; testSize++) {
			out.println("\n\n\nTesting with "+testSize+" clients");
			out.flush();
			System.gc();
			long time = System.currentTimeMillis();
			new PerformanceTester(testSize).test();
			long deltatime = (System.currentTimeMillis()-time);
			out.println("Test finished");
			out.println(Integer.toString(commands)+" executed in "+deltatime+" msec");
			double cps = (double)commands * 1000 / (double)deltatime;
			out.println("Commands per second: "+cps);
			out.flush();
		}
	}


	public PerformanceTester(int clientCount) {
		// the problem here is that too many threads will kill Client perfomance and make
		// server testing pointless. Therefore we keep a fixed number of client threads
		// that each do their job
		count = clientCount;

		clients = new Vector[THREAD_COUNT];
		for (int i=0; i<THREAD_COUNT; i++) {
			clients[i] = new Vector();
		}

		int index = 0;
		try {
			for (int i=0; i<clientCount; i++) {
				clients[index].addElement(new com.sap.spc.remote.client.tcp.TcpClientSupport());
				index++;
				if (index == THREAD_COUNT)
					index = 0;
			}
		}
		catch(Exception ex)  {
			out.println("exception during construction "+ex.getMessage());
			out.flush();
		}
	}


	public void test() {
		allthreads = new Thread[THREAD_COUNT];
		commands = 0;
		anyClient = null;
		for (int i=0; i<THREAD_COUNT; i++) {
			allthreads[i] = new PerformanceThread(i,clients[i]);
			allthreads[i].start();
		}

		// wait until finished
		boolean finished = false;
		while (!finished) {
			Thread t = selectThread(allthreads);
			if (t == null)
				finished = true;
			else
				try {
					t.join();
				}
				catch(InterruptedException ie) {log.debug(ie.getMessage(),ie);}
		}/*
		if (anyClient != null && isRFC != null && isRFC.booleanValue()) {
			try {
				System.out.println("writing throughput");
		        anyClient.cmd("JCODumpThroughput", new String[] { "message", "Throughput after test with "+count+" clients" });
				System.out.println("done writing throughput");
			}
			catch(Exception e) {
				e.printStackTrace();
			}
		}*/
	}


	private Thread selectThread(Thread[] threads) {
		for (int i=0; i<threads.length; i++) {
			Thread t = threads[i];
			if (t != null)
				return t;
		}
		return null;
	}


	public class PerformanceThread extends Thread
	{
		private Vector c;
		private int    index;

		public PerformanceThread(int myIndex, Vector clients) {
			this.c       = clients;
			this.index   = myIndex;
		}

		public void run() {
			try {
				for (Enumeration e=c.elements(); e.hasMoreElements();) {
					TcpClientSupport next = (TcpClientSupport)e.nextElement();
					if (isRFC == null) {
						if (next.getPort() == -1)
							isRFC = Boolean.TRUE;
						else
							isRFC = Boolean.FALSE;
					}
					if (anyClient == null)
						anyClient = next;
					configure(next);
				}
			}
			catch(Exception e) {
				out.println("Exception "+e.getMessage());
				out.flush();
			}
			allthreads[index] = null;
		}

		public void configure(TcpClientSupport client) throws ClientException {
			client.cmd("CreateSession", "client", "700", "application", "IPC");
			ServerResponse r = client.cmd("CreateDocument",new String[0]);
			String docId = r.getParameterValue("documentId");
			r = client.cmd("CreateItem","documentId",docId,"productId","WP-940");
			String itemId = r.getParameterValue("itemId");
			r = client.itemCmd(ISPCCommandSet.SET_CSTIC_VALUES,docId, itemId,
						SetCsticValues.INST_ID,"1",
						SetCsticValues.CSTIC_NAME,"WP_SCREEN",
						SetCsticValues.VALUE_NAME,"001");

			r = client.itemCmd(ISPCCommandSet.SET_CSTIC_VALUES,docId, itemId,
						SetCsticValues.INST_ID,"1",
						SetCsticValues.CSTIC_NAME,"WP_CONNECTION",
						SetCsticValues.VALUE_NAME,"002");

			r = client.itemCmd(ISPCCommandSet.SET_CSTIC_VALUES,docId, itemId,
							   new String[] {
							   SetCsticValues.INST_ID,"1",
							   SetCsticValues.CSTIC_NAME,"WP_OPTIONS",
							   SetCsticValues.VALUE_NAME,"002",
							   SetCsticValues.VALUE_NAME,"003"});

			r = client.itemCmd(ISPCCommandSet.SET_CSTIC_VALUES,docId, itemId,
						SetCsticValues.INST_ID,"1",
						SetCsticValues.CSTIC_NAME,"WP_INSTALL",
						SetCsticValues.VALUE_NAME,"Y");

			r = client.itemCmd(ISPCCommandSet.GET_INSTANCES,docId, itemId, new String[0]);

//			r = client.itemCmd(ISPCCommandSet.GET_CSTICS_AND_VALUES,docId, itemId, "instId", "1");

			r = client.itemCmd(ISPCCommandSet.GET_CONFIG_ITEM_INFO,docId, itemId, new String[0]);
			commands = commands + 9;
		}
	}



}
