/************************************************************************

	Copyright (c) 2000 by SAP AG

	All rights to both implementation and design are reserved.

	Use and copying of this software and preparation of derivative works
	based upon this software are not permitted.

	Distribution of this software is restricted. SAP does not make any
	warranty about the software, its performance or its conformity to any
	specification.

**************************************************************************/

package com.sap.spc.remote.client.tcp;


import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.net.ConnectException;
import java.net.InetAddress;
import java.net.Socket;
import java.net.SocketException;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.Properties;

import com.sap.msasrv.socket.shared.ErrorCodes;
import com.sap.msasrv.socket.shared.Header;
import com.sap.msasrv.socket.shared.ErrorConstants;
import com.sap.msasrv.socket.shared.ServerConstants;
import com.sap.msasrv.socket.shared.StringHeader;
import com.sap.spc.remote.client.AbsClientSupport;
import com.sap.spc.remote.client.ClientException;
import com.sap.spc.remote.client.ClientExceptionLog;
import com.sap.spc.remote.client.ClientLogEvent;
import com.sap.spc.remote.client.ClientLogListener;
import com.sap.spc.remote.client.ResourceAccessor;
import com.sap.tc.logging.Category;
import com.sap.tc.logging.Location;
import com.sap.tc.logging.Severity;

/**
 * An extension of the abstract Client class that communicates with the server via sockets.
 */
public class Client extends AbsClientSupport implements ErrorConstants, ServerConstants {

	private static final Category category = ResourceAccessor.category;
		//Categories.getCategory(Categories.CLIENT_CATEGORY);
	private static final Location location = ResourceAccessor.getLocation(Client.class);

	//Moved from com.sap.msasrv.socket.shared.command.dispatcher.Connect.java
	//Find a right plance if its not.
	public static final String ATTRIB_NAMES = "attribNames";
	public static final String ATTRIB_VALUES = "attribValues";

	/**
	 * Info: an IPC Dispatcher
	 */
//	public static final int INFO_DISPATCHER = 0;

	/**
	 * Info: an IPC Server
	 */
	public static final int INFO_SERVER     = 1;

	/**
	 * Info: couldn't connect to this port, probably nothing there
	 */
	public static final int INFO_NOBODY		= 2;

	/**
	 * Info: unknown application uses this port
	 */
	public static final int INFO_FOREIGN	= 3;

	/**
	 * Info: dead application on this port
	 */
	public static final int INFO_DEAD		= 4;

	/**
	 * Info: don't know what is on this port. Certainly no running dispatcher or server.
	 */
	public static final int INFO_UNKNOWN	= 5;


	public static final String CLIENT_PROPERTY_FILE = "properties/client.properties";

	Socket _socket;
	BufferedReader  is;
	OutputStreamWriter os;

	String  _host;
	int     _port;
	String  _encoding;
	boolean _compress;
//	SAPZip  _zip;
	protected String _attributes; // the actual connect string

	protected static Properties _clientProperties;

	/**
	 * Which security level should be used. "0" disables the security mode.
	 */
	int _securityLevel;

	/**
	* sslClientSecurityProps Contains the security related key-value pairs.
	* 			keys = {
	* 				"security.ssl.keystoreType" ,
	*				"security.ssl.keystoreLocation" ,
	*				"security.ssl.keystorePassword" ,
	*				"security.ssl.keyAlias" ,
	*				"security.ssl.keyPassword"
	* 			}
	* 			If securityLevel <=0 then securityProps parameter will be ignored.
	*/
	protected static Properties sslClientSecurityProps = null;

	public static final String CLIENT_SECURITY = "security.level";
	public static final String CLIENT_SSL_KEYSTORE_TYPE = "security.ssl.keystoreType";
	public static final String CLIENT_SSL_KEYSTORE_LOC = "security.ssl.keystoreLocation";
	public static final String CLIENT_SSL_KEYSTORE_PWD = "security.ssl.keystorePassword";
	public static final String CLIENT_SSL_KEY_ALIAS = "security.ssl.keyAlias";
	public static final String CLIENT_SSL_KEY_PWD = "security.ssl.keyPassword";
	public static final String CLIENT_SSL_DEFAULT_KEYSTORE_TYPE = "JKS";
	//set to "0" for SP05; should be set back to "1" as of SP06!
	public static final int CLIENT_DEFAULT_SECURITY = 0;
	public static final String DUMMY_REQUEST = "DUMMY-REQUEST";

	/**
	 * Returns connection attributes for a client. These attributes can be fed into
	 * the constructor of a client. Fills in the two clients given as well as reasonable
	 * values for userName ("user.name" and userLocation (IP address of this machine)).
	 *
	 * @param	type	the type of this client. null == IPC.
	 * @param	client	the R/3 client of this IPC client. null == client-independent connection.
	 * @return	a Hashtable suitable for the Client constructor.
	 *
	 */
	public static Hashtable getConnectAttributes(String type, String client) {
		Hashtable attribs = new Hashtable();
		if (type == null)
			type = ServerConstants.DEFAULT_TYPE;
		

		attribs.put(ServerConstants.KEY_TYPE, type);
		if (client != null)
			attribs.put(ServerConstants.KEY_CLIENT,client);

		String locationl;
		try {
			locationl = java.net.InetAddress.getLocalHost().getHostName();
			attribs.put(ServerConstants.KEY_USERLOCATION,locationl);
		}
		catch(Exception e) {
			category.logThrowableT(Severity.DEBUG,location,e.getMessage(),e);
		}
		attribs.put(ServerConstants.KEY_USERNAME, System.getProperty("user.name"));
		return attribs;
	}


	/**
	 * Constructs a new client. host, port and encoding for the communication line
	 * are read from the property file.
	 */
	public Client() throws ClientException{
		this(null,-1,null,null,null);
	}


	/**
	 * Constructs a new Client for a given host, port and encoding. The client will consult the
	 * property "dispatcher" in client.properties to find out if host/port points to a server or
	 * to a dispatcher.
	 *
	 * @param	host	the server host. If host is null, the default host is read from client.properties
	 * @param	port	the server port. If port is -1, the defalt port is read from client.properties
	 * @param	encoding	the character encoding to use. If encoding is null, the default encoding is read from client.properties
	 */
	public Client(String host, int port, String encoding) throws ClientException {
		this(host,port,encoding,null,null);
	}

	/**
	 * Constructs a new Client for a given host, port and encoding.
	 *
	 * @param	host	the server host. If host is null, the default host is read from client.properties
	 * @param	port	the server port. If port is -1, the defalt port is read from ipcclient.properties
	 * @param	encoding	the character encoding to use. If encoding is null, the default encoding is read from client.properties
	 * @param	viaDispatcher should the client ask a dispatcher at host/port for a server's address (true), directly contact a
	 * server at host/port (false) or leave this decision to property "dispatcher" in client.properties (null)?
	 * @param	attributes	a Hashtable of attributes that specify what this client wants to do. Only needed
	 * to help the dispatcher determine a best-suited IPC server for the client.
	 */
	public Client(String host, int port, String encoding, Boolean viaDispatcher, Hashtable attributes) throws ClientException{
 		this(host,port,encoding,viaDispatcher,attributes,null);
 	}

 	/**
 	 * Constructs a new Client for a given host, port, encoding and SSL properties.
 	 *
 	 * @param	host	the server host. If host is null, the default host is read from client.properties
 	 * @param	port	the server port. If port is -1, the defalt port is read from ipcclient.properties
 	 * @param	encoding	the character encoding to use. If encoding is null, the default encoding is read from client.properties
 	 * @param	viaDispatcher should the client ask a dispatcher at host/port for a server's address (true), directly contact a
 	 * server at host/port (false) or leave this decision to property "dispatcher" in client.properties (null)?
 	 * @param	attributes	a Hashtable of attributes that specify what this client wants to do. Only needed
 	 * to help the dispatcher determine a best-suited IPC server for the client.
 	 * @param   sslProps A set of SSL properties to enable/disable the Security. (default security.level=0)
 	 */
 	public Client(String host, int port, String encoding, Boolean viaDispatcher, Hashtable attributes,Properties sslProps ) throws ClientException{

		// Matthias had changed this to read the properties whenever a client is constructed to
		// support changing the property file while the VM is running. I don't see much point
		// in this, it just costs performance. kha.
		if (_clientProperties == null) {
			// work on a copy to avoid synchronization
			Properties clientProperties = new Properties();
			try {
				clientProperties.load(getClass().getClassLoader().getResourceAsStream(CLIENT_PROPERTY_FILE));
			}
			catch(Exception e){			
				category.logThrowableT(Severity.DEBUG,location,e.getMessage(),e);
			}
			_clientProperties = clientProperties; // may be written too often during startup by several threads
		}

		if (host != null)
			_host = host;
		else {
			_host = _clientProperties.getProperty("host");
			if (_host == null) {
				category.warningT(location, "No host given and none found in the property file. Using localhost");
				_host = "localhost";
			}
			else {
				//System.out.println("   -> Host: " + _host);
			}
		}

		if (port != -1)
			_port = port;
		else {
			String portString = (String)_clientProperties.get("port");
			if (portString == null) {
				category.warningT(location, "No port given and none found in the property file. Using 4444");
				_port = 4444;
			}
			else {
				//System.out.println("   -> Port: " + portString);
				_port = Integer.parseInt(portString);
			}
		}

		if (encoding == null) {
			encoding = (String)_clientProperties.get("encoding");
			if (encoding == null) {
				category.warningT(location, "No encoding given and none found in the property file");
				encoding = "UnicodeLittle";
			}
			else if (encoding.equals("default"))
				encoding = "Cp1252";
		}
		// security level
 		String securityLevel = "-1";
 		// If application passes SSL properties use them or else use the values of _clientProperties.
 		if (sslProps == null){
 			securityLevel = _clientProperties.getProperty(Client.CLIENT_SECURITY, ""+Client.CLIENT_DEFAULT_SECURITY);
 		}else{
 			securityLevel = sslProps.getProperty(Client.CLIENT_SECURITY, ""+Client.CLIENT_DEFAULT_SECURITY);
 		}
		try {
			_securityLevel = Integer.parseInt(securityLevel);
			if (_securityLevel < 0) {
				if (category.beWarning()){
					category.logT(Severity.WARNING, location, "Security level should not be -ve (" +
										securityLevel+"). Changing to default value '"+Client.CLIENT_DEFAULT_SECURITY+"'");
				}
				_securityLevel = Client.CLIENT_DEFAULT_SECURITY;
			}
		}
		catch(NumberFormatException e) {
			if (category.beWarning()){
				category.logT(Severity.WARNING, location, "Security level should be numeric. Changing from '" +
									securityLevel+"' to default value '"+Client.CLIENT_DEFAULT_SECURITY+"'");
			}
			_securityLevel = Client.CLIENT_DEFAULT_SECURITY;
		}
		category.infoT(location, "IPC Client Security-Level: " + _securityLevel);
		//Set SSL security properties..
		if (getSecurityLevel() > 0 && sslClientSecurityProps == null){
 			if (sslProps != null){
 				setSSLClientSecurityProperties(sslProps);
 			}else{
 				setSSLClientSecurityProperties(_clientProperties);
 			}
		}
//		if (viaDispatcher == null) {
//			String viaDispatcherString = (String)_clientProperties.get("dispatcher");
//			viaDispatcher = new Boolean(viaDispatcherString == null || !viaDispatcherString.equals("false"));
//		}

		// kha: removed compression support
		_compress = false;

		initSocket(_host, _port, encoding);
		_encoding = encoding;

		if (attributes == null)
			_attributes = "";
		else {
			StringBuffer result = new StringBuffer();
			boolean isFirst = true;
			for (Enumeration e=attributes.keys(); e.hasMoreElements();) {
				String key = (String)e.nextElement();
				String value = (String)attributes.get(key);
				if (isFirst)
					isFirst = false;
				else
					result.append("&");
				result.append(Client.ATTRIB_NAMES);
				result.append("=");
				result.append(key);
				result.append("&");
				result.append(Client.ATTRIB_VALUES);
				result.append("=");
				result.append(value);
			}
			_attributes = result.toString();
		}

		if (viaDispatcher.booleanValue())
			// connect to the server via the dispatcher
			connectToServer(encoding,_attributes);

		addLogListener(new ClientLogListener() {
			public void activated(ClientLogEvent event) {
				log("request",event.getMessage(),null);
			}
			public void requestSent(ClientLogEvent event) {
			//	System.out.println("LOG: SENT");
			//	sendTime = event.getTime();
			}
			public void responseReceived(ClientLogEvent event) {
				log("response",event.getMessage(),null);
			}
			public void closed(ClientLogEvent event) {
				log("closed","",null);
			}
			public void exceptionThrown(ClientLogEvent event) {
				log("exception","",event.getException());
			}
		});
	}


	protected void log(String what, String message, Throwable e) {
		if (e != null) {
			if (message != null)
				message = ", "+message;
			ClientExceptionLog.getInstance().exception(category, Severity.ERROR, e, "in client "+what+
				(message != null ? message : ""));
		}
		else {
			if (category.beDebug()) {
				category.logT(Severity.DEBUG, location, "Client "+what+": "+message);
			}
		}
	}


	/**
	 * Constructs a client that reads all its master data (host,port,encoding,dispatcher)
	 * from a property file and that uses the given dispatcher attributes.
	 */
	public Client(Hashtable attributes) throws ClientException {
		this(null,-1,null,null,attributes);
	}

  protected void connectToServer(String encoding, String attributes)  throws ClientException {

		int tryCount = 3;

		int port=0;

		String host = null;
		String sPort= null;
		ClientException clientException = null;

		while(--tryCount >= 0) {

			String result = readWrite("Connect", attributes, false);
			if (result == null) {
				category.errorT(location, "Could not read connection information from dispatcher");
			}

			ParaRequest para = new ParaRequest(result);

			host  = para.getParameterValue("host");
			if (host == null) {
				category.errorT(location, "Did not receive host from dispatcher");
				continue;
			}

			try {
				sPort = para.getParameterValue("port");
				port = Integer.parseInt(sPort);
			} catch (NumberFormatException ex) {
				category.fatalT(location, "Port received from dispatcher is not numeric (" + sPort + ')');
			}

			try {
				initSocket(host, port, encoding);

				if (category.beDebug())
				    category.logT(Severity.DEBUG, location, "connection to " + host + ':' + port + " succeeded.");
				return;
			} catch (ClientException ex) {
				ClientExceptionLog.getInstance().exception(category, Severity.ERROR, ex, "Initializing client socket connection");
				clientException = ex;
			}
		}

		category.fatalT(location, "connection to " + host + ':' + port + " failed.");
		throw clientException;
	}


	public String getHost() {
		return _host;
	}


	public int getPort() {
		return _port;
	}

	public String getEncoding() {
		return _encoding;
	}


	public void close() {

		if (_socket != null) {
			try {
				_socket.close();
			} catch (IOException ex) {category.logThrowableT(Severity.DEBUG,location,ex.getMessage(),ex);
}

			_socket = null;
		}

		if (is != null) {
			try {
				is.close();
			} catch (IOException ex) {category.logThrowableT(Severity.DEBUG,location,ex.getMessage(),ex);}

			is = null;
		}

		if (os != null) {
			try {
				os.close();
			} catch (IOException ex) {category.logThrowableT(Severity.DEBUG,location,ex.getMessage(),ex);}

			os = null;
		}

		fireEvent(new ClientLogEvent(this, ClientLogEvent.TYPE_CLOSED, null, null));
	}


	protected void _initSocket(Socket sock, String encoding) throws ClientException {

		close();
		id = null;  // reset the session id (which may have been set by communicating to the dispatcher)

		_socket = sock;

		try {
			is = new BufferedReader(new InputStreamReader(_socket.getInputStream(), encoding));
			os = new OutputStreamWriter(_socket.getOutputStream(), encoding);
		} catch(UnsupportedEncodingException e) {
			category.fatalT(location, "Encoding "+encoding+" not supported!");
			ArrayList placeHolder = new ArrayList(1);
			placeHolder.add(encoding);
			throw new ClientException(ErrorCodes.RET_INTERNAL_FATAL,"socket.client._initSocket.UNSEx",placeHolder,e);
		} catch (IOException ex) {
			ClientExceptionLog.getInstance().exception(category, Severity.FATAL, ex);
			throw new ClientException(ErrorCodes.RET_INTERNAL_FATAL,"socket.client._initSocket.IOEx",null,ex);
		}
	}

	protected void initSocket(String host, int port, String encoding) throws ClientException{
		ArrayList placeHolder = null;
		try {
			InetAddress addr = InetAddress.getByName(host);
			Socket socket = ClientSSLSecurityMgr.getClientSocket(getSecurityLevel(),addr,port,getSSLClientSecurityProperties(),category,location);
			_initSocket(socket, encoding);

			_host = host;
			_port = port;
		} catch (UnknownHostException e) {
			placeHolder = new ArrayList(2);
			placeHolder.add(host);
			placeHolder.add(String.valueOf(port));
			ClientExceptionLog.getInstance().exception(category, Severity.FATAL, e, "Trying to connect to unknown host");
			throw new ClientException(ErrorCodes.RET_INTERNAL_FATAL,"socket.client.initSocket.UHEx",placeHolder,e);
		} catch (ConnectException ex) {
			placeHolder = new ArrayList(2);
			placeHolder.add(host);
			placeHolder.add(String.valueOf(port));
			ClientExceptionLog.getInstance().exception(category, Severity.FATAL, ex, "on "+host+Integer.toString(port));
			throw new ClientException(ErrorCodes.RET_INTERNAL_FATAL,"socket.client.initSocket.CEx",placeHolder,ex);
		} catch (IOException ex) {
			placeHolder = new ArrayList(2);
			placeHolder.add(host);
			placeHolder.add(String.valueOf(port));
			ClientExceptionLog.getInstance().exception(category, Severity.FATAL, ex, "IO Problem in client");
			throw new ClientException(ErrorCodes.RET_INTERNAL_FATAL,
									  "socket.client.initSocket.IOEx",placeHolder,ex);
		} catch (ClientSSLException ex){
			placeHolder = new ArrayList(2);
			placeHolder.add(host);
			placeHolder.add(String.valueOf(port));
			ClientExceptionLog.getInstance().exception(category, Severity.FATAL, ex, "related to SSL Security while connecting to server/dispatcher");
			throw new ClientException(ErrorCodes.RET_INTERNAL_FATAL,
									  "socket.client.initSocket.ClientSSLEx",placeHolder,ex);
		}
	}



	protected String readWrite(String cmd, String inMsg, boolean broadcast) throws ClientException {

		fireEvent(new ClientLogEvent(this, ClientLogEvent.TYPE_ACTIVATED, cmd+":"+inMsg, null));

		if (inMsg == null)
			throw new ClientException(ErrorCodes.RET_COMPRESSION_FAILED,"socket.client.readWrite.CR",null,null);

		String h = (new StringHeader()).buildHeader(id, cmd, "STATUS", _compress, broadcast, inMsg.length());

		return doReadWrite(h, inMsg, broadcast);
	}


	protected String doReadWrite(String h, String inMsg, boolean broadcast) throws ClientException {
		StringBuffer sb = new StringBuffer();

		category.logT(Severity.DEBUG, location, "Request form client: " + h + inMsg);

		try {
			os.write(h);
			os.write('\n');
			os.flush();
			os.write(inMsg);
			os.flush();

			fireEvent(new ClientLogEvent(this, ClientLogEvent.TYPE_REQUEST_SENT, null, null));

			StringBuffer head = new StringBuffer();
			int chr = 'X';

			while ((chr=is.read()) != -1) {

				head.setLength(0);

				// HEADER

				while(chr != -1 && chr != '\n') {

					head.append((char)chr);

					chr = is.read();
				}

				if (chr == -1)
					break;

				if (chr == '\n') {
					String sHead = head.toString();

					if (sHead.startsWith(StringHeader.HEAD_PRE) &&
						sHead.endsWith(StringHeader.HEAD_POST)       ) {// get the Header

						Header header = new StringHeader(sHead);

						int max = header.getLength();
						id = header.getId();

						boolean serverCompresses = header.isCompressed();

						if (max > 0) {
							char[] chrA = new char[max];
							// read from socket until max chars are read
							int todo = max;
							int index = 0;
							while (todo > 0) {
								int charRead = is.read(chrA,index,todo);
								todo = todo - charRead;
								index = index + charRead;
							}

							long time1 = System.currentTimeMillis();

							if (header.getStatus().equals(ErrorCodes.RET_OK)) {
								//String result = _uncompressString(serverCompresses,new String(chrA)); ***5.0 MSA will not support this.
								String result = new String(chrA);
								if (result == null) {
									category.logT(Severity.DEBUG, location, "Response form server: " + h);
									throw new ClientException(ErrorCodes.RET_COMPRESSION_FAILED,"socket.client.doReadWrite.DR",null,null);
								}
								else{
									category.logT(Severity.DEBUG, location, "Response form server: " + h + result);
									return result;
									
								}
							}
							else {
								// retrieve error number and message
								String result = null; // initialization to make the compiler happy.
								int resultNumber;

//								if (broadcast) {
//									// command was a broadcast. There is no message but stats about the servers
//									String chrA2 = _uncompressString(serverCompresses,new String(chrA));
//									if (chrA2 == null)
//										throw new ClientException(RET_COMPRESSION_FAILED, "socket.client.doReadWrite.DEM",null,null);
//									ParaRequest r = new ParaRequest(chrA2);
//									String[] hosts = r.getParameterValues(ServerConstants.BROADCAST_RESPONSE_HOST);
//									String[] ports = r.getParameterValues(ServerConstants.BROADCAST_RESPONSE_PORT);
//									String[] status = r.getParameterValues(ServerConstants.BROADCAST_RESPONSE_STATUS);
//									if (hosts != null && ports != null && status != null) {
//										StringBuffer b = new StringBuffer(hosts.length*10+40);
//										b.append("The following IPC servers couldn't execute the broadcasted command: ");
//										boolean first = true;
//										for (int i=0; i<hosts.length; i++) {
//											if (!status[i].equals(RET_OK)) {
//												if (first)
//													first = false;
//												else
//													b.append(',');
//												b.append(hosts[i]+":"+ports[i]);
//											}
//										}
//										result = b.toString();
//									}
//									else {
//										result = "unknown server";
//									}
//									resultNumber = ErrorConstants.BROADCAST_FAILURE;
//								}
//								else {
									try {
										String chrA2 = new String(chrA);
										ParaRequest r = new ParaRequest(chrA2);
										result = r.getParameterValue(ERROR_PARAMETER);
										String resultNumberString = r.getParameterValue(ERROR_NUMBER);
										resultNumber = Integer.parseInt(resultNumberString);
									}
									catch(NumberFormatException nfe) {
										// result is already assigned properly if this happens
										resultNumber = ErrorConstants.UNKNOWN_ERROR;
									}
									catch(Exception e) {
										result = new String(chrA);
										resultNumber = ErrorConstants.UNKNOWN_ERROR;
									}
//								}
								category.logT(Severity.DEBUG, location, "Response form server: " + h + new String(chrA));
								throw new ClientException(header.getStatus(), result, resultNumber);
							}
						}
						else {
							category.logT(Severity.DEBUG, location, "Response form server: " + h);
							return "";
						}
					}
				}

				// ignore all others
			}
 		}catch(ClientException cle2){
 			String code = cle2.getCode();
 			String message = cle2.getMessage();
 			if(code.equals("410") && message.indexOf("Command Connect unknown") != -1)
 				throw new ClientException(ErrorCodes.RET_INTERNAL_FATAL,"ipc.error.server.dispatcher",null,cle2);
 			if(code.equals("410") && (message.indexOf("Command CreateConfig unknown") != -1 || message.indexOf("Command CreateDocument unknown") != -1))
 				throw new ClientException(ErrorCodes.RET_INTERNAL_FATAL,"ipc.error.dispatcher.server",null,cle2);
 			throw cle2;
 		}catch(SocketException ex){
 			ClientExceptionLog.getInstance().exception(category, Severity.FATAL, ex, "Possible reason:Server is not able to register at dispatcher, check if the dispatcher is running.");
 			throw new ClientException(ErrorCodes.RET_INTERNAL_FATAL,"ipc.socketerror",null,ex);
		} catch (UnknownHostException e) {
			ClientExceptionLog.getInstance().exception(category, Severity.FATAL, e, "Trying to connect to unknown host");
			throw new ClientException(ErrorCodes.RET_INTERNAL_FATAL,"socket.client.doReadWrite.UHEx",null,e);
		} catch (IOException x) {
			throw new ClientException(ErrorCodes.RET_INTERNAL_FATAL,"socket.client.doReadWrite.IOEx",null,x);
		}

		return null;
	}


	public String communicate(String command, String parameters) throws ClientException {
		return communicate(command,parameters,false);
	}


	public String communicate(String command, String parameters, boolean broadcast) throws ClientException {
		synchronized(this) {
			try {
				String result = readWrite(command, parameters,broadcast);
				fireEvent(new ClientLogEvent(this, ClientLogEvent.TYPE_RESPONSE_RECEIVED, command+":"+result, null));
				return result;
			}
			catch(ClientException clientEx) {
				fireEvent(new ClientLogEvent(this, ClientLogEvent.TYPE_EXCEPTION, clientEx.getMessage(), clientEx));
				throw clientEx; // rethrow the exception after logging
			}
		}
	}


	/**
	 * Internal method that communicates with the IPC server for a given header.
	 */
	public String transmitData(String header, String data, boolean clientZipped) throws ClientException {
		//data = _compressString(data); ***5.0 MSA will not support this.
		if (data == null)
			throw new ClientException(ErrorCodes.RET_COMPRESSION_FAILED,"socket.client.readWrite.CR",null,null);

		return this.doReadWrite(header, data, false);
	}


	/**
	 * Sets the socket timeout (SO_TIMEOUT) of this client to a given time.
	 * @param	timeout	the time in msec.
	 */
	public void setTimeout(int timeout) {
		try {
			_socket.setSoTimeout(timeout);
		}
		catch(SocketException ex) {
			category.logThrowableT(Severity.DEBUG,location,ex.getMessage(),ex);
		}
	}

	public int getSecurityLevel() {
		return _securityLevel;
	}

	public Properties getSSLClientSecurityProperties(){
			return sslClientSecurityProps;
	}

	public void setSSLClientSecurityProperties(Properties cliProps){
		sslClientSecurityProps = Client.getSSLClientSecurityProperties(cliProps);
	}

	public static Properties getSSLClientSecurityProperties(Properties cliProps){
		Properties sslClientSecurityProps = new Properties();
		// SSL properties...

		// SSL keystore type.
		String _sslKeystoreType = cliProps.getProperty(Client.CLIENT_SSL_KEYSTORE_TYPE, Client.CLIENT_SSL_DEFAULT_KEYSTORE_TYPE);
		if (_sslKeystoreType.equals("")) {
			_sslKeystoreType = "<undefined>";
		}
		sslClientSecurityProps.setProperty(Client.CLIENT_SSL_KEYSTORE_TYPE,_sslKeystoreType);

		// SSL keystore type.
		String _sslKeystoreLoc = cliProps.getProperty(Client.CLIENT_SSL_KEYSTORE_LOC, "<undefined>");
		if (_sslKeystoreLoc.equals("")) {
			_sslKeystoreLoc = "<undefined>";
		}
		sslClientSecurityProps.setProperty(Client.CLIENT_SSL_KEYSTORE_LOC,_sslKeystoreLoc);

		// SSL keystore type.
		String _sslKeystorePwd = cliProps.getProperty(Client.CLIENT_SSL_KEYSTORE_PWD, "<undefined>");
		sslClientSecurityProps.setProperty(Client.CLIENT_SSL_KEYSTORE_PWD,_sslKeystorePwd);

		// SSL keystore type.
		String _sslKeyAlias = cliProps.getProperty(Client.CLIENT_SSL_KEY_ALIAS, "<undefined>");
		if (_sslKeyAlias.equals("")) {
			_sslKeyAlias = "<undefined>";
		}
		sslClientSecurityProps.setProperty(Client.CLIENT_SSL_KEY_ALIAS,_sslKeyAlias);

		// SSL keystore type.
		String _sslKeyPwd = cliProps.getProperty(Client.CLIENT_SSL_KEY_PWD, "<undefined>");
		sslClientSecurityProps.setProperty(Client.CLIENT_SSL_KEY_PWD,_sslKeyPwd);
		return sslClientSecurityProps;
	}

	public static void main(String[] args) {
//		int info = getDispatcherOrServerInfo(args[0], Integer.parseInt(args[1]), "UnicodeLittle");
//		System.out.println("ANALYSIS RESULT "+info);
	}

}
