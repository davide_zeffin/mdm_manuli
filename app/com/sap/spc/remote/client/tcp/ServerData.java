package com.sap.spc.remote.client.tcp;

import java.util.Hashtable;

import com.sap.msasrv.socket.shared.ErrorCodes;
/**
 * Data about a server that a HighAvailabilityClientDispatcher can know
 */

public class ServerData implements ErrorCodes {

	public String host;
	public int port;
	protected static Hashtable attributes = null;
	protected Client client = null;


	public ServerData(String host, int port, boolean isDispatcher) {
		this.host = host;
		this.port = port;
		if (attributes == null) {
			attributes = Client.getConnectAttributes("SYSTEM","000");
		}
	}
}