/*
 * Created on Nov 2, 2004
 *
 */
package com.sap.spc.remote.client.tcp;

import java.io.IOException;
import java.io.OutputStream;
import java.net.InetAddress;
import java.net.Socket;
import java.util.Properties;

import com.sap.spc.remote.client.ClientException;

/**
 */
public class ClientSSLSecurityMgr {
	// ******getClientSocket() has two flavors. don't forget to update both (if any)******
	public static Socket getClientSocket(int securityLevel, InetAddress host,int port, Properties clientProps,
			com.sap.tc.logging.Category category,com.sap.tc.logging.Location location)  throws ClientSSLException, IOException, ClientException{
		Socket socket = null;
		if (securityLevel > 0){
			throw new ClientSSLException("SSL Secutiry not support by this Client...");
			//socket = ClientIAIKLibMgr.getClientSocket(host,port,clientProps,category,location);

		}else{
			socket = new Socket(host, port);
			//Handshake with server to know whether server is in SSL Security mode or not.

			//Disabled for backward compatibility:
			// CCMS client is in 'C' language and we don't want to enhance such old clients with this sort of new protocol (dummy request).
			// If we avoid this handshake, IPC client will be in hang state if server is in security level 0 and client is in security level 1.
			// But this is acceptable in 4.0, so disabling handshake.
			// Communicating this to customers with an SAP NOTE (726230).
			//ipcHandshakeWithServer(socket,category,location);
		}


		return socket;
	}
	// ******getClientSocket() has two flavors. don't forget to update both (if any)******
	public static Socket getClientSocket(int securityLevel, String host,int port, Properties clientProps,
			com.sap.tc.logging.Category category,com.sap.tc.logging.Location location)  throws ClientSSLException, IOException, ClientException{
		Socket socket = null;
		if (securityLevel > 0){
			throw new ClientSSLException("SSL Secutiry not support by this Client...");
			//socket = ClientIAIKLibMgr.getClientSocket(host,port,clientProps,category,location); 

		}else{
			socket = new Socket(host, port);
			//Handshake with server to know whether server is in SSL Security mode or not.

			//Disabled for backward compatibility:
			// CCMS client is in 'C' language and we don't want to enhance such old clients with this sort of new protocol (dummy request).
			// If we avoid this handshake, IPC client will be in hang state if server is in security level 0 and client is in security level 1.
			// But this is acceptable in 4.0, so disabling handshake.
			// Communicating this to customers with an SAP NOTE (726230).
			//ipcHandshakeWithServer(socket,category,location);
		}
		return socket;
	}

	public static void setDebugStream(int securityLevel, Socket socket, OutputStream os){
		if (securityLevel > 0){
			//ClientIAIKLibMgr.setDebugStream(socket,os);
			//SSL Secutiry not support by this Client...

		}
	}

}
