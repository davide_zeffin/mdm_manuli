/************************************************************************

	Copyright (c) 2000 by SAP AG

	All rights to both implementation and design are reserved.

	Use and copying of this software and preparation of derivative works
	based upon this software are not permitted.

	Distribution of this software is restricted. SAP does not make any
	warranty about the software, its performance or its conformity to any
	specification.

**************************************************************************/

package com.sap.spc.remote.client.tcp;


public abstract class AbstractServerResponse implements ServerResponse
{
	/**
	 * Returns the value of a parameter. If the named parameter does not exist,
	 * the default value is returned, if the parameter has several values, one of them
	 * is chosen arbitrarily.
	 * @param	name	the name of the parameter
	 * @param	defaultValue	the default value of the parameter
	 * @return	the/a value of the parameter, never null.
	 */
	public String getParameterValue(String name, String defaultValue) {
		String result = getParameterValue(name);
		if (result == null)
			return defaultValue;
		else
			return result;
	}


	/**
	 * Returns all values of a given parameter. If the parameter does not exist,
	 * this method returns a default value.
	 * @param	name	the name of the parameter
	 * @return	an array of the values
	 */
	public String[] getParameterValues(String name, String[] defaultValues) {
		String[] values = getParameterValues(name);
		if (values == null)
			return defaultValues;
		else
			return values;
	}
}
