/*
 * Created on Mar 19, 2004
 *
 * To change the template for this generated file go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
package com.sap.spc.remote.client.tcp;

import java.util.ArrayList;

import com.sap.msasrv.socket.shared.ErrorCodes;
import com.sap.msasrv.socket.shared.ErrorConstants;

/**
 * @author I026584
 *
 * An exception thrown by a SSLClient when it failed to create SSLClientContext.
 */
public class ClientSSLException extends Exception implements ErrorCodes
{
	protected String _code;
	protected String _message;
	protected int	   _number;
	protected String key;
	protected ArrayList placeHolders;
	protected Exception orgEx;

	public ClientSSLException (String code, String message) {
		this(code,message,ErrorConstants.UNKNOWN_ERROR);
	}
	
	public ClientSSLException(String msg){
		super(msg);
		_message = msg;
	}

	public ClientSSLException (String code, String message, int number) {
		super("Server error: "+code+" ("+message+")");
		_code = code;
		_message = message;
		_number = number;
	}

	/*
	* This constructor takes key and original exception as parameter.
	*/
	public ClientSSLException (String code, String key, ArrayList placeHolders, Exception ex){
		this._code = code;
		this.key = key;
		this.orgEx = ex;
		this.placeHolders = placeHolders;
	}

	/**
	 * Returns the server's error code.<p>
	 *
	 * @see com.sap.msasrv.socket.shared.ErrorCodes
	 */
	public String getCode() {
		return _code;
	}

	public String getKey(){
		return key;
	}


	public ArrayList getPlaceHolders(){
		return placeHolders;
	}

	/**
	 * Returns the Server's error number.
	 * @see	com.sap.msasrv.socket.ErrorConstants
	 */
	public int getErrorNumber() {
		return _number;
	}


	/**
	 * Returns the server's message.<p>
	 *
	 * In error situations the server will return a response with a parameter
	 * SPC_ERROR_MESSAGE. The value of this parameter is retrieved with this method.
	 *
	 * @see com.sap.msasrv.socket.shared.ServerConstants
	 */
	public String getMessage() {
		if(key == null || key == ""){
			return _message;
		}else{
			return "ClientSSLException.getMessage() Not supported..";
		}
	}

	/**
	* Returns the original exception
	*/
	public Exception getOriginalException(){
		return this.orgEx;
	}

	/**
	 * Returns true if this Exception is fatal, ie if the client should
	 * terminate its session.<p>
	 *
	 * Fatal errors are those explicitely marked
	 * fatal by the server as well as references to unknown or "timeouted"
	 * session ids.<p>
	 *
	 * Recoverable errors are those marked "error" by the server and references
	 * to unknown server commands.<p>
	 */
	public boolean isRecoverable() {
		if (_code.equals(RET_INTERNAL_ERROR))
			return true;
		else if (_code.equals(RET_INTERNAL_FATAL))
			return false;
		else if (_code.equals(RET_OK))
			return true;
		else if (_code.equals(RET_TIMEDOUT_ID))
			return false;
		else if (_code.equals(RET_UNKNOWN_CMD))
			return true;
		else if (_code.equals(RET_UNKNOWN_ID))
			return false;
		else
			// default: not recoverable
			return false;
	}
}
