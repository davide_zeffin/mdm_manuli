/*
 * Created on Dec 30, 2004
 *
 */
package com.sap.spc.remote.client.tcp;


import java.util.Enumeration;
import java.util.Hashtable;
import java.util.Properties;

import com.sap.spc.remote.shared.command.CreateDocument;
import com.sap.spc.remote.shared.command.CreateItems;
import com.sap.spc.remote.client.tcp.Client;
import com.sap.spc.remote.client.ClientException;
import com.sap.spc.remote.client.ClientLogListener;
import com.sap.spc.remote.client.tcp.ServerResponse;


/**
 * A ClientSupport bases on a sxe ClientSupport implementation and extends it by
 * IPC convenience methods. It can only be constructed on top of another sxe ClientSupport
 * to which it delegates all its basic method calls, similar to java's FilterReaders.
 */
public class TcpClientSupport extends com.sap.spc.remote.client.AbsClientSupport
{
	com.sap.spc.remote.client.AbsClientSupport _base;

	public TcpClientSupport(com.sap.spc.remote.client.AbsClientSupport base) {
		_base = base;
	}


	/**
	 * Constructor that automatically constructs a new delegate client using
	 * its constructor with the same signature
	 * @throws com.sap.spc.remote.client.ClientException
	 */
	public TcpClientSupport() throws ClientException  {
		this(new Client());
	}


	/**
	 * Constructor that automatically constructs a new delegate client using
	 * its constructor with the same signature
	 */
	public TcpClientSupport(String host, int port, String encoding) throws ClientException {
		this(new Client(host,port,encoding));
	}


	/**
	 * Constructor that automatically constructs a new delegate client using
	 * its constructor with the same signature
	 */
	public TcpClientSupport(String host, int port, String encoding, Boolean viaDispatcher) throws ClientException {
		this(new Client(host,port,encoding,viaDispatcher,Client.getConnectAttributes(null,null)));
	}

	public TcpClientSupport(String host, int port, String encoding, Boolean viaDispatcher, Properties securityProperties) throws ClientException {
		this(new Client(host,port,encoding,viaDispatcher,Client.getConnectAttributes(null,null), securityProperties));
	}

	/**
	 * Constructs a ClientSupport for a specific purpose. attributes contains a number
	 * of key/value pairs that are communicated to the dispatcher. The dispatcher will
	 * then assign an appropriate IPC server to this client.
	 */
	public TcpClientSupport(String host, int port, String encoding, Hashtable attributes) throws ClientException {
		this(new Client(host,port,encoding,new Boolean(true), attributes));
	}


	public TcpClientSupport(String host, int port, String encoding, Boolean viaDispatcher, Hashtable attributes) throws ClientException {
		this(new Client(host,port,encoding, viaDispatcher, attributes));
	}

	public TcpClientSupport(String host, int port, String encoding, Boolean viaDispatcher, Hashtable attributes, Properties securityProperties) throws ClientException {
		this(new Client(host,port,encoding, viaDispatcher, attributes, securityProperties));

	}

	public TcpClientSupport(Hashtable attributes) throws ClientException {
		this(new Client(attributes));
	}

	public TcpClientSupport(Hashtable attributes, Properties securityProperties) throws ClientException {
		this(new Client(null, -1, null, null, attributes, securityProperties));
	}

	/**
	 * Adds a new client log listener to this client. The client will inform it about
	 * all relevant events.
	 */
	public void addLogListener(ClientLogListener l) {
		_base.addLogListener(l);
	}

	/**
	 * Removes a clinet log listener from this client. Doesn't perform any operation if l
	 * has not previously been added by addLogListener.
	 */
	public void removeLogListener(ClientLogListener l) {
		_base.addLogListener(l);
	}

	/**
	 * Command communication with an IPC server. Implement this class
	 * to gain a working ClientSupport implementation.
	 *
	 * @param	command	the name of a SPC command
	 * @param	parameters string-encoded parameters
	 * (NAME=VALUE&amp;OTHERNAME=OTHERVALUE&amp;...&amp;)
	 * @return	a string encoding the server's Response object.
	 */
	public String communicate(String command, String parameters) throws ClientException {
		return _base.communicate(command,parameters);
	}


	public String communicate(String command, String parameters, boolean broadcast) throws ClientException {
		return _base.communicate(command,parameters,broadcast);
	}


	/**
	 * Sets the session id of this client. The session id is passed to the server on every
	 * communication. Therefore you can change the id as often as you like.
	 */
	public void setSessionId(String id) {
		_base.setSessionId(id);
	}


	/**
	 * Returns the session id of this client, ie. the id of the server session to which this
	 * client is currently connected.
	 */
	public String getSessionId() {
		return _base.getSessionId();
	}


	public String getHost() {
		if (_base instanceof com.sap.spc.remote.client.tcp.Client)
			return ((com.sap.spc.remote.client.tcp.Client)_base).getHost();
		else
			return null;
	}


	public int getPort() {
		if (_base instanceof com.sap.spc.remote.client.tcp.Client)
	    	return ((com.sap.spc.remote.client.tcp.Client)_base).getPort();
		else
			return -1;
	}


	public String getEncoding() {
		if (_base instanceof com.sap.spc.remote.client.tcp.Client)
			return ((com.sap.spc.remote.client.tcp.Client)_base).getEncoding();
		else
			return null;
	}



	/**
	 * Sets the socket timeout of this client (SO_TIMEOUT) to a given time. Some clients
	 * may not support socket timeouts. They will always return false.
	 *
	 * @param	timeout	the timeout (in msec).
	 * @return	true if the timeout was set successfully.
	 */
	public boolean setTimeout(int timeout) {
		if (_base instanceof Client) {
			((Client)_base).setTimeout(timeout);
			return true;
		}
		else
			return false;
	}


	/**
	 * executes a given command with a parameter list given by a string array, parses the
	 * server's response and provides a ServerResponse object to get response values.
	 */
	public ServerResponse cmd(String command, String[] parameters) throws ClientException {
		return _base.cmd(command,parameters);
	}


	/**
	 * executes a given command with a parameter list given by a string array, parses the
	 * server's response and provides a ServerResponse object to get response values.
	 * If broadcast is set to true, the command will be broadcasted to all IPC servers registered
	 * at the same dispatcher as my server, the ServerResponse will <em>not</em> contain the return
	 * parameters but three arrays "host", "port" and "status" that hold the return status ("200", "500")
	 * returned from every IPC server.
	 */
	public ServerResponse cmd(String command, String[] parameters, boolean broadcast) throws ClientException {
		return _base.cmd(command,parameters,broadcast);
	}


	/**
	 * Convenience method for cmd with a single name/value pair
	 */
	public ServerResponse cmd(String command, String name, String value) throws ClientException {
		return _base.cmd(command,name,value);
	}

	public ServerResponse cmd(String command, String name1, String value1, String name2, String value2) throws ClientException {
		return _base.cmd(command,name1,value1,name2,value2);
	}

	public ServerResponse cmd(String command, String name1, String value1, String name2, String value2,
						   String name3, String value3) throws ClientException {
		return _base.cmd(command,name1,value1,name2,value2,name3,value3);
	}

	public ServerResponse cmd(String command, String name1, String value1, String name2, String value2,
						   String name3, String value3, String name4, String value4) throws ClientException {
		return _base.cmd(command,name1,value1,name2,value2,name3,value3,name4,value4);
	}

	public ServerResponse cmd(String command, String name1, String value1, String name2, String value2,
						   String name3, String value3, String name4, String value4, String name5, String value5) throws ClientException {
		return _base.cmd(command,name1,value1,name2,value2,name3,value3,name4,value4,name5,value5);
	}


	/**
	 * Convenience method for cmd that operates on a document.
	 */
	public ServerResponse documentCmd(String command, String docId, String[] parameters) throws ClientException {
		String[] theparameters = new String[parameters.length+2];
		theparameters[0] = CreateDocument.DOCUMENT_ID;
		theparameters[1] = docId;
		for (int i=0; i<parameters.length; i++)
			theparameters[i+2] = parameters[i];
		return cmd(command,theparameters);
	}

	public ServerResponse documentCmd(String command, String docId) throws ClientException {
		String[] parameters = new String[] { CreateDocument.DOCUMENT_ID, docId };
		return cmd(command, parameters);
	}

	public ServerResponse documentCmd(String command, String docId, String name, String value) throws ClientException {
		String[] parameters = new String[] { CreateDocument.DOCUMENT_ID, docId, name, value };
		return cmd(command, parameters);
	}

	public ServerResponse documentCmd(String command, String docId, String name1, String value1,
								   String name2, String value2) throws ClientException {
		String[] parameters = new String[] { CreateDocument.DOCUMENT_ID, docId, name1, value1, name2, value2 };
		return cmd(command, parameters);
	}

	public ServerResponse documentCmd(String command, String docId, String name1, String value1,
								   String name2, String value2, String name3, String value3) throws ClientException {
		String[] parameters = new String[] { CreateDocument.DOCUMENT_ID, docId, name1, value1, name2, value2,
								 name3, value3 };
		return cmd(command, parameters);
	}


	/**
	 * Convenience method for cmd that operates on an item
	 */
	public ServerResponse itemCmd(String command, String docId, String itemId, String[] parameters) throws ClientException {
		String[] theparameters = new String[parameters.length+4];
		theparameters[0] = CreateDocument.DOCUMENT_ID;
		theparameters[1] = docId;
		theparameters[2] = CreateItems.ITEM_IDS;
		theparameters[3] = itemId;
		for (int i=0; i<parameters.length; i++)
			theparameters[i+4] = parameters[i];
		return cmd(command,theparameters);
	}

	public ServerResponse itemCmd(String command, String docId, String itemId) throws ClientException {
		String[] parameters = new String[] { CreateDocument.DOCUMENT_ID, docId, CreateItems.ITEM_IDS, itemId };
		return cmd(command, parameters);
	}

	public ServerResponse itemCmd(String command, String docId, String itemId, String name, String value) throws ClientException {
		String[] parameters = new String[] { CreateDocument.DOCUMENT_ID, docId, CreateItems.ITEM_IDS, itemId, name, value };
		return cmd(command, parameters);
	}

	public ServerResponse itemCmd(String command, String docId, String itemId, String name1, String value1,
							   String name2, String value2) throws ClientException {
		String[] parameters = new String[] { CreateDocument.DOCUMENT_ID, docId, CreateItems.ITEM_IDS, itemId,
								 name1, value1, name2, value2 };
		return cmd(command, parameters);
	}

	public ServerResponse itemCmd(String command, String docId, String itemId, String name1, String value1,
							   String name2, String value2, String name3, String value3) throws ClientException {
		String[] parameters = new String[] { CreateDocument.DOCUMENT_ID, docId, CreateItems.ITEM_IDS, itemId,
								 name1, value1, name2, value2, name3, value3 };
		return cmd(command, parameters);
	}

	public ServerResponse itemCmd(String command, String docId, String itemId, String name1, String value1,
							   String name2, String value2, String name3, String value3,
							   String name4, String value4) throws ClientException {
		String[] parameters = new String[] { CreateDocument.DOCUMENT_ID, docId, CreateItems.ITEM_IDS, itemId,
								 name1, value1, name2, value2, name3, value3, name4, value4 };
		return cmd(command, parameters);
	}


	/**
	 * Closes this client.
	 */
	public void close() {
		_base.close();
	}


	/**
	 * ServerResponse array access convenince function.<p>
	 *
	 * Returns an Enumeration of arrays, filled with consecutive parameter values that decode
	 * the usual structure representation.<p>
	 *
	 * This method is useful in situations where a Response contains several arrays of the
	 * same length, where the i-th element of each array holds a different aspect of the
	 * same object. For example, the server might return a number of Rectangles by four
	 * arrays x[], y[], width[], height[]. The client is usually not interested in those
	 * arrays but in an Enumeration of (x,y,width, height) elements. The necessary conversion
	 * is provided by this method.<p>
	 *
	 * @param	request	the request that contains the arrays we're interested in
	 * @param	parameterNames an array of parameter names that are all associated with
	 * arrays of the same length.
	 * @return	an Enumeration of String arrays. Each String array has the same length
	 * as parameterNames and corresponds to the i-th array element of every array, in the
	 * same order as parameterNames. The number of elements in the enumeration is the
	 * minimal length of any parameter value array (usually all parameter value arrays
	 * are of the same length).<p>
	 * In the rectangle example, the first enumeration element would be a String[4] where
	 * s[0]=x0, s[1]=y0, s[2]=width0, s[3]=height0, the next element would be the same
	 * data for the second rectangle and so on.
	 */
	public static Enumeration getStructures(ServerResponse request, String[] parameterNames) {
		return com.sap.spc.remote.client.AbsClientSupport.getStructures(request,parameterNames);
	}


	/**
	 * ServerResponse array access convenience function.<p>
	 *
	 * This function acts similar as getStructures, but instead of returning an enumeration,
	 * it uses another string-valued parameter as a key of the enumerated data and puts
	 * them into a hashtable. This is useful when one parameter contains names or ids of
	 * structures (eg. cstics, items) and other parameters contain data to those structures
	 * in the same order (eg. cstic values, language dependent names,...)
	 *
	 * <pre>
	 * ServerResponse r = itemCmd(GET_DOCUMENT_AND_ITEM_INFO, docId);
	 * Hashtable h = ClientSupport.getStructureTable(r,GetDocumentAndItemInfo.ITEM_ID,
	 *     new String[] { GetDocumentAndItemInfo.ITEM_NET_VALUE, GetDocumentAndItemInfo.ITEM_TAX_VALUE });
	 * </pre>
	 * will return a Hashtable from ItemId to String[] where the value of an item id is a string with
	 * as many elements as given in parameterNames, containing the respective data in the respective order.
	 *
	 * @param	request	the request to use
	 * @param	indexParameter	a parameter to be used as the keys of the hashtable. All null values will be
	 * ignored, if a value occurs twice, the second will overwrite the first.
	 * @param	parameterNames	the names of te parameters that the caller is interested in.
	 */
	public static Hashtable getStructureTable(ServerResponse request, String indexParameter, String[] parameterNames) {
		return com.sap.spc.remote.client.AbsClientSupport.getStructureTable(request,indexParameter,parameterNames);
	}
}
