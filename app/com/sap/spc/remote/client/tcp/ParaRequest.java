/************************************************************************

	Copyright (c) 2000 by SAP AG

	All rights to both implementation and design are reserved.

	Use and copying of this software and preparation of derivative works
	based upon this software are not permitted.

	Distribution of this software is restricted. SAP does not make any
	warranty about the software, its performance or its conformity to any
	specification.

**************************************************************************/

package com.sap.spc.remote.client.tcp;


import java.util.ArrayList;
import java.util.Comparator;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.Map;
import java.util.TreeSet;

import com.sap.spc.remote.client.IClientLogCategories;
import com.sap.spc.remote.client.ResourceAccessor;
import com.sap.msasrv.socket.shared.ServerConstants;
import com.sap.tc.logging.Category;
import com.sap.tc.logging.Location;

/**
 * A ParaRequest is a Request constructed from the parameter list as returned
 * in the body of a Response object.<p>
 *
 * Since the Response String has a slighly more complicated syntax than the
 * Request String and is able to use null parameter values, a normal Request
 * object can not be used for this purpose.<p>
 */
public class ParaRequest extends AbstractServerResponse
{
/*
 * kha proudly presents: the happy parser :-)
 *
 * When parsing a request, the parser first assumes the "normal situation": all parameter[x]=value
 * assignments are in the correct order and there is no value missing between them. This will be
 * parsed rather fast in the parser's "happy state" with method addParameterHappy(). The result of
 * this parse is that the table _isStartingWithZero is not null, and table _nameToValues contains
 * valid values.
 *
 * When the parser finds out during addParameterHappy that a value assignment violates the "happy
 * condition", it changes to its "unhappy state": In this state, _isStartingWithZero is not needed,
 * and thus set to null, the table _nameToTable contains the more complicated "int index to value"
 * table-containing-table, _highIndex contains the highest index reached for a parameter so far,
 * _nameToValues contains those values who could be parsed happy.
 *
 * The reasoning behind all this is that the server (if not modified badly) always returns values
 * suitable for parseHappy, and parseHappy is about 34% faster than parseUnhappy is, and doing
 * a getParameterValues() on parseHappy's results is 35 times faster than on parseUnhappy's
 * (according to JProbe).
 *
 * Therefore the client should stay in its happy mode unless the command is implemented badly.
 *
 * All accesses to the data later need to first check if _isStartingWithZero is empty. If it is,
 * the unhappy result should be used (unless there's a partial happy result), if it is not, the
 * happy result can be used.
 */

 /*
  * 20020805-kha: The conversion from happy to unhappy format is complicated, takes a lot of time,
  * and has some bugs, obviously, because depending on the problem found, different adjustments
  * have to be made (it seems). Therefore the parser now tries to parse in "happy mode". If it
  * fails, it completely starts over in unhappy mode. This is safer, better maintainable and costs
  * at most twice as long as unhappy parsing. Given that unhappy parsing should never happen in
  * real life (haha) and takes 35 times longer than happy parsing, the one time doesn't matter so much. */

/*
 * 20020805-kha: Right now the happy and unhappy parser behave differently when confronted with
 * missing values, for example with a response data[1]=A&data[5]=B. Happy parsing will correctly
 * return { A, null, null, null, B }, while unhappy parsing will return { A, B }. Since unhappy parsing
 * shouldn't be used at all, I'll fix this later.
 */
	// _nameToTable is a HashMap from name to Hashtables.
	// the value Hashtables map indices to Values
	protected HashMap _nameToTable;

	// from name to ArrayList of values. This is created lazily, and may be null
	// for some names.
	protected HashMap _nameToValues;

	// _highIndex is a HashMap from name to Integers.
	// The value of a name contains the highest index present in its
	// HashMap in _nameToTable. This integer is required for dealing
	// with multiple assignments to the same key which are internally
	// mapped to consecutive array assignments.
	// a=one&a=two will be mapped to a[1]=one&a[2]=two.
	protected HashMap _highIndex;
	protected HashMap _isStartingWithZero; // temporary, required during parsing, later null

	protected String    _queryString;

	// since it is not possible to store (key,null) in a HashMap, we
	// use a special null object.
	protected Object    _nullObject;

	protected boolean   _happyData;   // are the data that are being/have been parsed "happy compliant", i.e. normalized
	protected boolean   _happyMode;   // are we parsing in happy mode?

	protected static final Category category = ResourceAccessor.category;
	protected static final Location location = ResourceAccessor.getLocation(ParaRequest.class);

	/**
	 * Constructs a new ResponseRequest object from a given parameter list.
	 */
	public ParaRequest(String paraList) {
	    _queryString  = paraList;
		init();
		_happyMode = true;
		_happyData = true;
		 _parseParaList(paraList);
		 if (!_happyData) {
			category.warningT(location, "Fast parsing failed: This will cost lots of performance. Please fix this IPC command");
			_happyMode = false;
			_parseParaList(paraList);
		 }
		 _convertNullStrings();
	}

	protected void init() {
		 _nameToTable = new HashMap();
		 _nameToValues = new HashMap();
		 _highIndex   = new HashMap();
		 _nullObject = new Boolean(true);
		 _isStartingWithZero = new HashMap();
	}

	/**
	 * Adds a parameter to this list.
	 *
	 * @param	name	the name of the parameter. If it ends with "[" natural "]", the
	 * natural number is taken as an index, the [..] removed from the name
	 * @param	value	the value of the parameter; may be null. Any encoding must be removed.
	 */
	protected void addParameter(String name, String value) {
		if (_isStartingWithZero != null) {
			if (_happyMode) {
				if (_happyData) {
			        if (!addParameterHappy(name, value)) {
					    _happyData = false;
	    		    }
				}
				else {
					// happy mode, no happy data => ignore, we're in a dead and and have to switch to
					// unappy parsing
				}
			}
			else {
				addParameterUnhappy(name, value);
			}
		}
	}


	/**
	 * Covers the happy 99% case that parameter order is correct for all parameters. Returns false if this
	 * is not the case. In this case, a real, slower, more memory consuming, second parse has to be applied
	 */
	protected boolean addParameterHappy(String name, String value) {
		int index = -1;
		String origName = name;
		if (name.endsWith("]")) {
			// array name - retrieve index; remove [...] from name.
			int startPos = name.lastIndexOf("[");
			if (startPos != -1) {
				String number = name.substring(startPos+1,name.length()-1);
				try {
					index = Integer.parseInt(number);
					if (index < 0)
						index = -1;
					else
						name = name.substring(0,startPos);
				}
				catch(NumberFormatException e) {
					index = -1;
				}
			}
		}

		if (index == -1) {
			ArrayList l = (ArrayList)_nameToValues.get(name);
			if (l != null) {
				category.warningT(location, "Performance problem: Parameter "+name+" is not first, but doesn't have an array index.");
				return false; // name=value => MUST BE the first element
			}

			_isStartingWithZero.put(name, Boolean.FALSE);
			ArrayList val = new ArrayList(1);
			val.add(value);
			_nameToValues.put(name,val);
			return true;
		}
		else if (index == 0) {
			// some <expletive> implemented a command wrongly, work around it
			ArrayList l = (ArrayList)_nameToValues.get(name);
			if (l != null) {
				category.warningT(location, "Performance problem: Parameter "+name+"[0] not returned first");
				return false; // name[0]=value => MUST BE the first element
			}

			_isStartingWithZero.put(name, Boolean.TRUE);
			ArrayList val = new ArrayList();
			val.add(value);
			_nameToValues.put(name,val);
			return true;
		}
		else {
			Boolean zeroOff = (Boolean)_isStartingWithZero.get(name);
			int addOff = 0;
			if (zeroOff == null) {
				// normal array param, index=1 => first element
				_isStartingWithZero.put(name, Boolean.FALSE);
			}
			else if (zeroOff.equals(Boolean.TRUE))
				addOff = -1;

			ArrayList currentVal = (ArrayList)_nameToValues.get(name);
			if (currentVal == null) {
				currentVal = new ArrayList();
				_nameToValues.put(name, currentVal);
			}

			int length = currentVal.size();

			int lastIndex = length+addOff+1;
			if (lastIndex != index) {
				if (lastIndex < index) {
					while (lastIndex < index) {
						currentVal.add(null);
						lastIndex++;
					}
				}
				else {
					category.warningT(location, "Performance problem: Parameter "+name+" is not correctly ordered at ["+index+"]");
				    return false;
				}
			}

			currentVal.add(value);
			return true;
		}
	}


	protected void addParameterUnhappy(String name, String value) {
		int index = -1;
		String origName = name;

		if (name.endsWith("]")) {
			// array name - retrieve index; remove [...] from name.
			int startPos = name.lastIndexOf("[");
			if (startPos != -1) {
				String number = name.substring(startPos+1,name.length()-1);
				try {
					index = Integer.parseInt(number);
					if (index < 0)
						index = -1;
					else
						name = name.substring(0,startPos);
				}
				catch(NumberFormatException e) {
					index = -1;
				}
			}
		}

		// get the hashtable of our name and the currently highest index in it
		HashMap indexToValue = (HashMap)_nameToTable.get(name);
		int highestIndex;
		if (indexToValue == null) {
			indexToValue = new HashMap();
			_highIndex.put(name,new Integer(0));
			highestIndex = 0; // the lowest index in our data is 1!
			_nameToTable.put(name,indexToValue);
			// pre-initialize _nameToValues
			_nameToValues.put(name, new ArrayList());
		}
		else {
			Integer high = (Integer)_highIndex.get(name);
			highestIndex = high.intValue();
		}

		// compute the target index, if necessary
		if (index == -1)
			index = highestIndex + 1;
		else if (index == 0) {
			// somebody didn't know that IPC commands return arrays 1..n, not 0..n-1. oh well
			highestIndex = index-1;
			_highIndex.put(name, new Integer(-1));
		}

		// enter the value
		Object valObj;
		if (value == null)
			valObj = _nullObject;
		else
			valObj = value;
		indexToValue.put(new Integer(index), valObj);

		// the server can return values in any order, but typically it doesn't. Cover the 99% case
		// by just adding the value at the end of _nameToValues' list, if this is still consistent
		if (index == highestIndex +1) {
			ArrayList l = (ArrayList)_nameToValues.get(name);
			if (l == null) {
				// we deleted this list before => there has been a problem (weird ordering)
			}
			else {
				l.add(valObj);
			}
		}
		else {
			category.warningT(location, "parameter "+origName+" is not legal, needs to start from 1");
			// different index than expected => reorder
			_nameToValues.remove(name);
		}

		// update the highest index, if necessary
		if (index > highestIndex) {
			_highIndex.put(name, new Integer(index));
		}
	}

	/**
	 * Parses a parameter list.
	 *
	 * The syntax of the Response String is as follows:<p>
	 *
	 * <pre>
	 * ParameterList = Parameter { Separator Parameter }.
	 * Parameter = ParameterName "=" ParameterValue.
	 * ParameterName = String [ "[" Natural "]" ].
	 * ParameterValue = String.
	 * </pre>
	 *
	 * where Separator is defined in IPCSessionRemote and String is any
	 * sequence of characters that doesn't contain single "=" or separators.
	 * Two consecutive "=" chars or separators are treated as one of these
	 * characters.<p>
	 */
	protected void _parseParaList(String paraList) {
		if (ServerConstants.SEPARATOR.length() > 1)
			throw new RuntimeException("Multi-char separator string not supported");
		_parseAssignments(paraList);
		if (_isStartingWithZero != null)
			_isStartingWithZero = new HashMap(1);
	}


	protected void _parseAssignments(String s) {
		if (!s.endsWith(ServerConstants.SEPARATOR))
			s = s + ServerConstants.SEPARATOR;
		int currstart = 0;
		boolean finished = false;
		while (!finished && (!_happyMode || _happyData)) {
			int next = _indexOfSingleSeparator(s,currstart);
			if (next == -1)
				finished = true;
			else {
				String assignment = s.substring(currstart,next);
				_addAssignment(assignment);
				currstart = next+1;
			}
		}
	}


	protected static int _indexOfSingleSeparator(String s, int start) {
		String separator = ServerConstants.SEPARATOR;
		char sepChar = separator.charAt(0);
		int currStart = start;
		int length = s.length();
		boolean eos = false;
		while (!eos) {
			int index = s.indexOf(sepChar,currStart);
			if (index == -1)
				eos = true;
			else {
				if (index == length-1 || s.charAt(index+1) != sepChar)
					return index;
				else
					currStart = index+2; // jump over double separator
			}
		}
		return -1;
	}


	protected void _addAssignment(String assignment) {
		int equalsIndex = assignment.indexOf("=");
		if (equalsIndex == -1)
			return;
		else {
			String name = assignment.substring(0,equalsIndex);
			String value = assignment.substring(equalsIndex+1);
			addParameter(name,_decodeValue(value));
		}
	}


	protected String _decodeValue(String value) {
		boolean finished = false;
		do {
			int index = value.indexOf("&&");
			if (index == -1)
				finished = true;
			else {
				value = value.substring(0,index)+
						value.substring(index+1);
			}
		}
		while (!finished);
		return value;
	}


	/**
	 * Converts every null string into _nullObject which allows faster null checking in
	 * the getter methods
	 */
	protected void _convertNullStrings() {
		String nullString = getParameterValue(ServerConstants.NULL_STRING_NAME);
		if (nullString != null) {
			for (Iterator eTables=_nameToTable.values().iterator(); eTables.hasNext();) {
				HashMap current = (HashMap)eTables.next();
				for (Iterator eKeys = current.keySet().iterator(); eKeys.hasNext();) {
					Object key = eKeys.next();
					Object value = current.get(key);
					if (value.equals(nullString)) {
						current.put(key,_nullObject);
					}
				}
			}
			for (Iterator iValues=_nameToValues.values().iterator(); iValues.hasNext();) {
				ArrayList current = (ArrayList)iValues.next();
				int size = current.size();
				for (int i=0; i<size; i++) {
					Object value = current.get(i);
					if (value != null && value.equals(nullString))
						current.set(i,null);
				}
			}
		}
	}



	public String getQueryString() {
		return _queryString;
	}


	public String[] getParameterValues(String name) {
		ArrayList l = (ArrayList)_nameToValues.get(name);
		if (l != null) {
			String[] result = new String[l.size()];
			l.toArray(result);
			return result;
		}
		else {
			HashMap table = (HashMap)_nameToTable.get(name);
			if (table == null)
				return null;
			else
				return mapToStringArray(table);
		}
		// kha: typically, clients retrieve a response object once and never call getParameterValues
		// for the same parameter more than once, therefore it just wastes space trying to add the
		// parameter list to _nameToValues. (does it?)
	}


	/**
	 * @deprecated use mapToStringArray instead
	 */
	public String[] tableToStringArray(Hashtable table) {
		TreeSet set = new TreeSet(new Comparator() {
			public int compare(Object o1, Object o2) {
				Integer i1 = (Integer)o1;
				Integer i2 = (Integer)o2;
				return i1.intValue()-i2.intValue();
			}});

		set.addAll(table.keySet());

		// now a contains the keys, sorted by number. We run through a and collect
		// the hashtable data

		String[] data = new String[set.size()];

		Iterator dataIter = set.iterator();
		for (int i=0; i<data.length; i++) {
			if (!dataIter.hasNext())
				throw new RuntimeException("TreeSet lost elements during sort");
			Object currData = table.get(dataIter.next());
			if (currData == _nullObject)
				data[i] = null;
			else
				data[i] = (String)currData;
		}

		return data;
	}


	public String[] mapToStringArray(Map table) {
		TreeSet set = new TreeSet(new Comparator() {
			public int compare(Object o1, Object o2) {
				Integer i1 = (Integer)o1;
				Integer i2 = (Integer)o2;
				return i1.intValue()-i2.intValue();
			}});

		set.addAll(table.keySet());

		// now a contains the keys, sorted by number. We run through a and collect
		// the hashtable data

		String[] data = new String[set.size()];

		Iterator dataIter = set.iterator();
		for (int i=0; i<data.length; i++) {
			if (!dataIter.hasNext())
				throw new RuntimeException("TreeSet lost elements during sort");
			Object currData = table.get(dataIter.next());
			if (currData == _nullObject)
				data[i] = null;
			else
				data[i] = (String)currData;
		}

		return data;
	}


	public String getParameterValue(String name) {
		ArrayList l = (ArrayList)_nameToValues.get(name);
		if (l != null)
			return (String)l.get(0);

		HashMap table = (HashMap)_nameToTable.get(name);
		if (table == null)
			return null;
		else {
			Iterator iter = table.values().iterator();
			if (iter.hasNext()) {
				Object o = iter.next();
				if (o == _nullObject)
					return null;
				else
					return (String)o;
			}
			else
				return null;
		}
	}

	/**
	 * @return an Enumeration of the names of all parameters.
	 */
	public Iterator getParametersIterator() {
		if (_isStartingWithZero != null)
			return _nameToValues.keySet().iterator();
		else
		    return _nameToTable.keySet().iterator();
	}

	/**
	 * @return an Enumeration of the names of all parameters.
	 */
	public Enumeration getParametersEnum() {
		return new IteratorEnumeration(getParametersIterator());
	}

	/**
	 * @return	an array containing the names of all parameters.
	 */
	public String[] getParameters() {
		String[] result = new String[_nameToTable.size()];
		Iterator iter;
		if (_isStartingWithZero != null)
			iter = _nameToValues.keySet().iterator();
		else
			iter = _nameToTable.keySet().iterator();

		for (int i=0; i<result.length; i++) {
			if (!iter.hasNext())
				throw new RuntimeException("Inconsistent HashMap size/keys");
			String key = (String)iter.next();
			result[i] = key;
		}

		return result;
	}


	/**
	 * returns true iff the parameter "name" is present, regardless if its
	 * value is null or not.
	 */
	public boolean findParameter(String name) {
		if (_isStartingWithZero != null)
			return _nameToValues.containsKey(name);
		else
		    return _nameToTable.containsKey(name);
	}
}


class IteratorEnumeration implements Enumeration {
	private Iterator iter;
	public IteratorEnumeration(Iterator iter) {
		this.iter = iter;
	}
	public boolean hasMoreElements() {
		return iter.hasNext();
	}
	public Object nextElement() {
		return iter.next();
	}
}
