/************************************************************************

	Copyright (c) 2000 by SAP AG

	All rights to both implementation and design are reserved.

	Use and copying of this software and preparation of derivative works
	based upon this software are not permitted.

	Distribution of this software is restricted. SAP does not make any
	warranty about the software, its performance or its conformity to any
	specification.

**************************************************************************/

package com.sap.spc.remote.client;


import java.util.ArrayList;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.NoSuchElementException;
import java.util.Properties;
import java.util.Vector;

import com.sap.spc.remote.client.tcp.Client;
import com.sap.spc.remote.client.tcp.ParaRequest;
import com.sap.spc.remote.client.tcp.ServerResponse;
import com.sap.isa.core.logging.IsaLocation;
import com.sap.msasrv.socket.shared.ServerConstants;
//<--


/**
 * This class provides convenience methods for the client in dealing
 * with single command communication.  Subclasses have to implement the
 * abstract function communicate that sends a string request to the
 * server and gets a string response from it. This class particularly
 * deals with
 * <ul>
 *    <li>calling commands without having to construct the command
 *    string
 *    <li>parsing command results into an object that implements the
 *    ServerResponse interface (ie. that can be asked for parameter names and
 *    values)
 *    <li>null values returned by the server in a special
 *    encoding
 *    <li>access of data of a single object encoded in several string
 *    arrays.
 * </ul>
 */
public abstract class AbsClientSupport implements ExceptionHandler, IClientSupport
{
	protected static IsaLocation log = IsaLocation.getInstance(AbsClientSupport.class.getName());
	public static final String GW_HOST_PROPERTY_KEY             = "gwHost";
	public static final String GW_SERVICE_PROPERTY_KEY          = "gwService";
	public static final String DISPATCHER_PROG_ID_PROPERTY_KEY  = "dispatcherProgId";
	public static final String USE_RFC                          = "useRFC";

	protected static Properties clientProperties = null;

	/**
	 * The IPC Session id used by this client.
	 */
	protected String id;

	protected ArrayList listeners = new ArrayList();


	public AbsClientSupport() {
		initProperties();
	}

	public static boolean useRFCClient() {
		initProperties();
		String val = clientProperties.getProperty(USE_RFC);
		return val != null && val.equals("true");
	}

	/**
	 * Command communication with an IPC server. Implement this class
	 * to gain a working AbsClientSupport implementation.
	 *
	 * @param	command	the name of a SPC command
	 * @param	parameters string-encoded parameters
	 * (NAME=VALUE&amp;OTHERNAME=OTHERVALUE&amp;...&amp;)
	 * @return	a string encoding the server's Response object.
	 */
	public abstract String communicate(String command, String parameters) throws ClientException;

	/**
	 * Command communication with an IPC server. Implement this class
	 * to gain a working AbsClientSupport implementation.
	 *
	 * @param	command	the name of a SPC command
	 * @param	parameters string-encoded parameters
	 * (NAME=VALUE&amp;OTHERNAME=OTHERVALUE&amp;...&amp;)
	 * @param	broadcast	true iff the IPC server should pass the command to all its neighbors (that are connected
	 * to the same dispatcher) and return a response status overview instead of the actual results.
	 * @return	a string encoding the server's Response object.
	 */
	public abstract String communicate(String command, String parameters, boolean broadcast) throws ClientException;


	protected static void initProperties() {
		clientProperties = new Properties();
		try {
			clientProperties.load((AbsClientSupport.class).getClassLoader().getResourceAsStream(Client.CLIENT_PROPERTY_FILE));
		}
		catch(Exception e){log.debug(e.getMessage(),e);}
	}


	/**
	 * Sets the session id of this client. The session id is passed to the server on every
	 * communication. Therefore you can change the id as often as you like.
	 */
	public void setSessionId(String id) {
		this.id = id;
	}

	/**
	 * Returns the session id of this client, ie. the id of the server session to which this
	 * client is currently connected.
	 */
	public String getSessionId() {
		return id;
	}


	/**
	 * executes a given command with a parameter list given by a string array, parses the
	 * server's response and provides a ServerResponse object to get response values.
	 */
	public ServerResponse cmd(String command, String[] parameters) throws ClientException {
		return cmd(command,parameters,false);
	}


	/**
	 * executes a given command with a parameter list given by a string array, parses the
	 * server's response and provides a ServerResponse object to get response values.
	 * If broadcast is set to true, the command will be broadcasted to all IPC servers registered
	 * at the same dispatcher as my server, the ServerResponse will <em>not</em> contain the return
	 * parameters but three arrays "host", "port" and "status" that hold the return status ("200", "500")
	 * returned from every IPC server.
	 */
	public ServerResponse cmd(String command, String[] parameters, boolean broadcast) throws ClientException {
		//BD 28052003: Changelist 42773 IPC 4.0 SP2 SAT
		//new-->
//		if(_monitor != null)
//			_monitor.startComponent(SAT_STRING+":cmd("+command+")");
		//<--

		StringBuffer paraBuffer = new StringBuffer(parameters.length * 2);
		boolean nullStringIntroducerWritten = false;
		for (int i=0; i<parameters.length-1; i=i+2) {
			if (i>0)
				paraBuffer.append("&");

			if (parameters[i+1] == null && !nullStringIntroducerWritten) {
				_appendNullStringIntroducer(paraBuffer);
				nullStringIntroducerWritten = true;
			}

			paraBuffer.append(parameters[i]);
			paraBuffer.append("=");
			paraBuffer.append(_encodeParameter(parameters[i+1]));
		}
		String result = communicate(command,paraBuffer.toString(),broadcast);
		//BD 28052003: Changelist 42773 IPC 4.0 SP2 SAT
		//new-->
//		if(_monitor != null)
//			_monitor.endComponent(SAT_STRING+":cmd("+command+")");
		//<--
		if (result == null)
			return null;
		else
			return new ParaRequest(result);
	}


	protected void _appendNullStringIntroducer(StringBuffer buffer) {
		buffer.append(com.sap.msasrv.socket.shared.ServerConstants.NULL_STRING_NAME);
		buffer.append("=");
		buffer.append("((NULL))"); 
		buffer.append("&");
	}


	protected String _encodeParameter(String s) {
		if (s == null) {
			return "((NULL))";
		}
		else {
			StringBuffer sb = new StringBuffer();
			int tokLen = ServerConstants.SEPARATOR.length();
			int i;
			int fromIdx = 0;

			while ((i=s.indexOf(ServerConstants.SEPARATOR, fromIdx)) >= 0) {

				if (i == 0) {
					sb.append(ServerConstants.SEPARATOR);
					sb.append(ServerConstants.SEPARATOR);
				} else {
					sb.append(s.substring(fromIdx, i));
					sb.append(ServerConstants.SEPARATOR);
					sb.append(ServerConstants.SEPARATOR);
				}

				fromIdx = i + tokLen;
			}
			sb.append(s.substring(fromIdx));

			return sb.toString();
		}
	}


	/**
	 * Convenience method for cmd with a single name/value pair
	 */
	public ServerResponse cmd(String command, String name, String value) throws ClientException {
		String[] parameters = new String[] { name, value };
		return cmd(command, parameters);
	}

	public ServerResponse cmd(String command, String name1, String value1, String name2, String value2) throws ClientException {
		String[] parameters = new String[] { name1, value1, name2, value2 };
		return cmd(command, parameters);
	}

	public ServerResponse cmd(String command, String name1, String value1, String name2, String value2,
						   String name3, String value3) throws ClientException {
		String[] parameters = new String[] { name1, value1, name2, value2, name3, value3 };
		return cmd(command, parameters);
	}

	public ServerResponse cmd(String command, String name1, String value1, String name2, String value2,
						   String name3, String value3, String name4, String value4) throws ClientException {
		String[] parameters = new String[] { name1, value1, name2, value2, name3, value3, name4, value4 };
		return cmd(command, parameters);
	}

	public ServerResponse cmd(String command, String name1, String value1, String name2, String value2,
						   String name3, String value3, String name4, String value4, String name5, String value5) throws ClientException {
		String[] parameters = new String[] { name1, value1, name2, value2, name3, value3, name4, value4, name5, value5 };
		return cmd(command, parameters);
	}


	public abstract void close();


	/**
	 * ServerResponse array access convenince function.<p>
	 *
	 * Returns an Enumeration of arrays, filled with consecutive parameter values that decode
	 * the usual structure representation.<p>
	 *
	 * This method is useful in situations where a Response contains several arrays of the
	 * same length, where the i-th element of each array holds a different aspect of the
	 * same object. For example, the server might return a number of Rectangles by four
	 * arrays x[], y[], width[], height[]. The client is usually not interested in those
	 * arrays but in an Enumeration of (x,y,width, height) elements. The necessary conversion
	 * is provided by this method.<p>
	 *
	 * @param	request	the request that contains the arrays we're interested in
	 * @param	parameterNames an array of parameter names that are all associated with
	 * arrays of the same length.
	 * @return	an Enumeration of String arrays. Each String array has the same length
	 * as parameterNames and corresponds to the i-th array element of every array, in the
	 * same order as parameterNames. The number of elements in the enumeration is the
	 * minimal length of any parameter value array (usually all parameter value arrays
	 * are of the same length).<p>
	 * In the rectangle example, the first enumeration element would be a String[4] where
	 * s[0]=x0, s[1]=y0, s[2]=width0, s[3]=height0, the next element would be the same
	 * data for the second rectangle and so on.
	 */
	public static Enumeration getStructures(ServerResponse request, String[] parameterNames) {
		Vector structureVector = new Vector();
		for (int i=0; i<parameterNames.length; i++) {
			String[] values = request.getParameterValues(parameterNames[i]);
			if (values == null)
				return new _EmptyEnumeration();
			structureVector.addElement(values);
		}

		return new _MultiArrayEnumeration(structureVector);
	}


	/**
	 * ServerResponse array access convenience function.<p>
	 *
	 * This function acts similar as getStructures, but instead of returning an enumeration,
	 * it uses another string-valued parameter as a key of the enumerated data and puts
	 * them into a hashtable. This is useful when one parameter contains names or ids of
	 * structures (eg. cstics, items) and other parameters contain data to those structures
	 * in the same order (eg. cstic values, language dependent names,...)
	 *
	 * <pre>
	 * ServerResponse r = itemCmd(GET_DOCUMENT_AND_ITEM_INFO, docId);
	 * Hashtable h = AbsClientSupport.getStructureTable(r,GetDocumentAndItemInfo.ITEM_ID,
	 *     new String[] { GetDocumentAndItemInfo.ITEM_NET_VALUE, GetDocumentAndItemInfo.ITEM_TAX_VALUE });
	 * </pre>
	 * will return a Hashtable from ItemId to String[] where the value of an item id is a string with
	 * as many elements as given in parameterNames, containing the respective data in the respective order.
	 *
	 * @param	request	the request to use
	 * @param	indexParameter	a parameter to be used as the keys of the hashtable. All null values will be
	 * ignored, if a value occurs twice, the second will overwrite the first.
	 * @param	parameterNames	the names of te parameters that the caller is interested in.
	 */
	public static Hashtable getStructureTable(ServerResponse request, String indexParameter, String[] parameterNames) {
		String[] indexValues = request.getParameterValues(indexParameter);
		int i=0;
		Hashtable result = new Hashtable();
		for (Enumeration e = getStructures(request,parameterNames); e.hasMoreElements();) {
			if (indexValues[i] != null) {
				result.put(indexValues[i],e.nextElement());
			}
			i++;
		}
		return result;
	}



	// listener stuff

	/**
	 * Adds a new client log listener to this client. The client will inform it about
	 * all relevant events.
	 */
	public void addLogListener(ClientLogListener l) {
		if (!listeners.contains(l))
		    listeners.add(l);
	}


	/**
	 * Removes a client log listener from this client. Doesn't perform any operation if l
	 * has not previously been added by addLogListener.
	 */
	public void removeLogListener(ClientLogListener l) {
		listeners.remove(l);
	}


	protected void fireEvent(ClientLogEvent event) {
		for (Iterator iter=listeners.iterator(); iter.hasNext();) {
			ClientLogListener l = (ClientLogListener)iter.next();
			int type = event.getType();
			switch (type) {
				case ClientLogEvent.TYPE_ACTIVATED:
					l.activated(event);
					break;
					case ClientLogEvent.TYPE_CLOSED:
					l.closed(event);
					break;
					case ClientLogEvent.TYPE_EXCEPTION:
					l.exceptionThrown(event);
					break;
					case ClientLogEvent.TYPE_REQUEST_SENT:
					l.requestSent(event);
					break;
					case ClientLogEvent.TYPE_RESPONSE_RECEIVED:
					l.responseReceived(event);
					break;
			}
		}
	}

	//BD 28052003: Changelist 42773 IPC 4.0 SP2 SAT
	//new-->
//	protected void setJARMMonitor(IMonitor monitor)
//	{
//		_monitor = monitor;
//	}
	//<--

	// ExceptionHandler implementation
	public void handleException(Object source, String message, Exception exception) {
		fireEvent(new ClientLogEvent(source, ClientLogEvent.TYPE_EXCEPTION, message, exception));
	}

}


class _EmptyEnumeration implements Enumeration
{
	public boolean hasMoreElements() { return false; }
	public Object  nextElement()     { throw new NoSuchElementException(); }
}

class _MultiArrayEnumeration implements Enumeration
{
	protected int _index;
	protected int _limit;
	protected int _size;
	protected Vector _v;

	public _MultiArrayEnumeration(Vector v) {
		_index=0;
		_v = v;
		_size = _v.size();

		_limit = Integer.MAX_VALUE;
		for (Enumeration e=v.elements(); e.hasMoreElements();) {
			String[] curr = (String[])e.nextElement();
			if (curr.length < _limit)
				_limit = curr.length;
		}
	}

	public boolean hasMoreElements() {
		return _limit > _index;
	}

	public Object nextElement() {
		if (!hasMoreElements())
			throw new NoSuchElementException();
		else {
			String[] result = new String[_size];
			int i=0;
			for (Enumeration e=_v.elements(); e.hasMoreElements();) {
				String[] current = (String[])e.nextElement();
				result[i] = current[_index];
				i++;
			}
			_index++;
			return result;
		}
	}
}

