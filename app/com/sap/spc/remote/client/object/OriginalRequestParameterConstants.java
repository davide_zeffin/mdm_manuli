package com.sap.spc.remote.client.object;

/**
 * Contains request parameter constants.
 * New parameter names (not the constants) should be added in Java notation.
 * (showOneToOneConditions instead of show_one_to_one_conditions)
 * NOTE: The declaration of these constants should be moved to the ipc webapp.
 * It was moved to this place for use in the DefaultUIContext, which should also
 * be in the ipc webapp. This will be done in the next release!
 */
public interface OriginalRequestParameterConstants {

	/**
	 * <p>Represents the value true for a http request parameter value</p>
	 */
	public String T = "T";
	/**
	 * <p>Represents the value false for a http request parameter value</p>
	 */
	public String F = "F";

	public static final String COMMAND_INIT = "~initialTemplate";
	public static final String COMMAND_EXECUTE = "~template";
	/**
	 * <p>The caller is the application which uses (calls) the IPC JSP UI.</p>
	 * <p>It has influence on the way the UI processes the http request parameters</p>
	 * <p>and distincts the set of default parameter values maintained in the</p>
	 * <p>configuration file web.xml e.g. crm.order.hidePrices = {@link #T}</p>
	 * Possible values:
	 * {@link #ONLY_CONFIG}
	 * {@link #ERP}
	 * {@link #VEHICLE_MANAGER}
	 * {@link #ONLINE_STORE}
	 * {@link #ORDER_MAINTAIN}
	 * {@link #PRODUCT_SIMULATION}
	 * {@link #THIRD_PARTY}
	 */
	public static final String CALLER = "caller";
	/**
	 * <p>The caller may pass a string message that the IPC UI displays in the</p>
	 * <p>header of the UI</p>
	 */
	public static final String CALLER_MESSAGE = "behavior.callermessage";//"caller_message"; //agreed upon with Stefan Hunsicker

	static final String ORDER_MAINTAIN = "crmordermaintain";
	static final String ORDER_MAINTAIN_ERP_LRD = "crmordermaintain_erp_lrd";
	static final String PRODUCT_SIMULATION = "crmproductsimulation";
	static final String CRM_PRODUCT_CATALOG = "crmproductcatalog";
	static final String MSA_ORDER = "msaordermaintain";
	static final String MSA_PRODUCT_SIMULATION = "msaproductsimulation";
	static final String MSAONLYCONFIG = "msaonlyconfig";
	static final String VEHICLE_MANAGER = "vehiclemanager";
	static final String ERP_ORDER = "erpordermaintain";
	static final String ERP_PRODUCT_SIMULATION = "erpproductsimulation";
	static final String ERP_BASE = "erpbase"; //other configurations in ERP (product variants, purchase order, equipment, procuction order, ...
	static final String THIRD_PARTY = "thirdparty";
	static final String ONLY_CONFIG = "onlyconfig";
	static final String CALLER_B2B_BASKET = "b2b_basket";
	static final String CALLER_B2C_BASKET = "b2c_basket";
	static final String CALLER_B2C_ORDERSTATUS = "b2c_orderstatus";
	static final String CALLER_B2B_ORDERSTATUS = "orderstatuscloseconfig";
	static final String CALLER_B2B_ORDER = "b2b_order";
	static final String CALLER_CLOSE_CONFIGURATION = "contractCloseConfiguration";
	public final static String CALLER_BACK_TO_CATALOG = "catalog";
    public final static String CALLER_B2C_CATALOG = "b2c_catalog";

	/**
	 * Calculate pricing after each evaluation ({@link #T}) or only if the customer press
	 * the "Price" Button ({@link #F})
	 */
	public static final String CHECK_PRICING = "behavior.checkpricing";//"check_pricing";
	/**
	 * display mode on ({@link #T}) or off ({@link #F}). In the display mode you can't modify the
	 * configuration. It is open for display only.
	 */
	public static final String DISPLAY_MODE = "behavior.display";//"display";
	/**
	 * Shows ({@link #T}) a "Settings" button for additional properties or trace functionality.
	 * ({@link #F}) suppresses the "Settings" button.
	 */
	public static final String ENABLE_SETTINGS = "behavior.enablesettings";//"enable_settings";
	/**
	 * Show ({@link #F}) or hide ({@link #T}) pricing information for
	 * the product.
	 * @see #SHOW_ONE_TO_ONE_CONDITIONS
	 */
	public static final String HIDE_PRICES = "behavior.hideprices";//"hidePrices";
	/**
	 * Specifies how the Browser communicates with the IPC server.
	 * If {@link #T}, the web browser will send a request for each changed value
	 * to the server and set this value in the configurator.
	 * If {@link #F}, the user may changes several values and then send a request
	 * upon click on a special button like "check".
	 * The latter setting is recommended for advanced users that know the
	 * configuration model and set several values by keeping the configuration
	 * consistent.
	 */
	public static final String ONLINE_EVALUATE = "behavior.onlineevaluate";//"online_evaluate";
    
     /**
     * Shows ({@link #T}) a "Help" button. This leads to a pop-up that shows additional help
     * for using the application (e.g. shortcuts, etc.)
     * ({@link #F}) suppresses the "Help" button.
     */
    public static final String ENABLE_HELP_POPUP = "behavior.enablehelppopup";
    
    /**
     * Values {@link #F}/{@link #ON_MESSAGES}/{@link #ALWAYS}<br>
     * Controls whether the UI should forward to the comparison screen. Only on the 
     * comparison screen the loading messages are displayed. 
     * If "{@link #F}" no forward, it only shows a static message in the messagearea with a 
     * link to the comaprison screen.
     * If "{@link #ON_MESSAGES}" it forwards automatically to the comparison screen if 
     * loading messages/new KB exist.
     * If "{@link #ALWAYS}" it always forwards to the comparison screen even if no messages exist.
     * This parameter is depenendent on parameter {@link #SHOW_LOADING_MESSAGES}. Only if loading 
     * messages are enabled it will forward on messages existance. That means if loading messages 
     * are disabled it would not check for existance and in case of "onmessages" it would not forward
     * to the comparison screen. But it would forward if the parameter is set to "always".
     */
    public static final String LOADING_MESSAGES_AUTOMATIC_FORWARD = "behavior.forwardtocomparison";     

    /**
     * Values {@link #F}/{@link #T}<br>
     * Controls whether the user can take snapshots during the configuration session.<br>
     * If this feature is enabled also the comparison feature should be enabled in order to
     * display the differences to the snapshot and to return to the configuration state of the 
     * snapshot. 
     * If "{@link #T}" the feature is enabled. 
     * If "{@link #F}" the feature is disabled.
     */    
    public static final String ENABLE_SNAPSHOT = "behavior.enablesnapshot";

    /**
     * Values {@link #F}/{@link #T}<br>
     * This parameter enables the configuration comparison feature. If it is enabled (&quot;T&quot;) the 
     * user can take compare the current configuration to a taken snapshot (parameter behavior.enablesnapshot
     * has to be set to &quot;T&quot;).<br/>
     * If an already stored configuration is displayed in the Configuration UI the user has the possibility
     * to compare the current configuration also to the stored configuration.
     * If "{@link #T}" the feature is enabled. 
     * If "{@link #F}" the feature is disabled.
     */    
    public static final String ENABLE_COMPARISON = "behavior.enablecomparison";
    
    /**
     * Represents the value onmessages for parameter {@link #LOADING_MESSAGES_AUTOMATIC_FORWARD}.
     */
    public static final String ON_MESSAGES = "onmessages";
    
    /**
     * Represents the value always for parameter {@link #LOADING_MESSAGES_AUTOMATIC_FORWARD}.
     */
    public static final String ALWAYS = "always";
    
	/**
	 * If {@link #T}, the UI shows the characteristic values in options or check
	 * boxes.
	 * If {@link #F}, the UI shows the characteristic values in combo or list boxes.
	 */
	public static final String SHOW_CSTIC_VALUES_EXPANDED = "values.expanded";//"show_cstic_values_expanded";
	/**
	 * Show product, characteristics and values pictures when {@link #T}.
	 */
	public static final String SHOW_PICTURE = "appearance.showpictures";//"show_pic";
   /**
    * Show the customization list, if {@link #T}.
    */
   public static final String SHOW_CUSTOMIZATION_LIST = "customizationlist.show";//showCustomizationList";
   /**
    * Display or Hide the price information in the customization list.
    */
   public static final String SHOW_PRICES_IN_CUSTOMIZATION_LIST = "customizationlist.prices.show";
   /**
    * Controls the display of price information for the root instance in the customization list.
    */
   public static final String CUSTOMIZATION_LIST_ROOT_INSTANCE_PRICES = "customizationlist.prices.rootinstance";
   /**
    * Controls the display of price information for the sub instances in the customization list.
    */
   public static final String CUSTOMIZATION_LIST_SUB_INSTANCES_PRICES = "customizationlist.prices.subinstances";
   /**
    * Controls the display of price information for the totals area in the customization list.
    */
   public static final String CUSTOMIZATION_LIST_TOTAL_PRICES = "customizationlist.prices.totals";
   /**
    * Show mime-objects enlarged in new window if {@link #T}.
    */
   public static final String SHOW_MIMES_IN_NEW_WINDOW = "appearance.showmimesinnewwindow";//"showMimesInNewWindow";
	/**
	 * Show dynamic product pictures {@link #T}.
	 */
	public static final String SHOW_DYNAMIC_PRODUCT_PICTURE = "appearance.dynamicproductpicture";//show_config_pictures";
	/**
	 * Show surcharges for characteristic values when {@link #T}.
	 * @see #HIDE_PRICES
	 */
	public static final String SHOW_ONE_TO_ONE_CONDITIONS = "values.showonetooneconditions";//"showOneToOneConditions";
	public static final String SHOW_CHARACTERISTIC_VALUE_3D_PICTURE = "values.show3dpicture";//"show_csticvalue_3d";
	/**
	 * Show invisible characteristics when {@link #T}.
	 */
	public static final String SHOW_INVISIBLE_CHARACTERISTICS = "characteristics.showinvisibles";//"show_inv_cstic";

	/**
	 * Show the tab of the general group as first (T) or last (F) tab.
	 */
	public static final String SHOW_GENERAL_TAB_FIRST = "groups.showgeneraltabfirst";//"showGeneralTabFirst";
	/**
	 * The customer buttons can be used for general purpose.
	 * The next three constants decide, which button is available in the ui
	 */
	static final String SHOW_CUSTOMER_BUTTON1   = "customerbutton1.show";//"showCustomerButton1";
	static final String SHOW_CUSTOMER_BUTTON2   = "customerbutton2.show";
	static final String SHOW_CUSTOMER_BUTTON3   = "customerbutton3.show";

	/**
	 * The customer buttons can be used for general purpose.
	 * The next three constants decide, which language dependent label to
	 * display on the customer buttons
	 */
	static final String CUSTOMER_BUTTON1_LABEL  = "customerbutton1.label";
	static final String CUSTOMER_BUTTON2_LABEL  = "customerbutton2.label";
	static final String CUSTOMER_BUTTON3_LABEL  = "customerbutton3.label";

	/**
	 * The customer buttons can be used for general purpose.
	 * The next three constants decide, if the button is available whenever
	 * the configuration is complete only.
	 */
	static final String CUSTOMER_BUTTON1_AVAILABLE_IF_COMPLETE_ONLY = "customerbutton1.ifcompleteonly";//"customerButton1AvailableIfCompleteOnly";
	static final String CUSTOMER_BUTTON2_AVAILABLE_IF_COMPLETE_ONLY = "customerbutton2.ifcompleteonly";//"customerButton2AvailableIfCompleteOnly";
	static final String CUSTOMER_BUTTON3_AVAILABLE_IF_COMPLETE_ONLY = "customerbutton3.ifcompleteonly";//"customerButton3AvailableIfCompleteOnly";

	/**
	 * The customer buttons can be used for general purpose.
	 * The next three constants decide, which action to call whenever the button
	 * is pressed.
	 */
	static final String CUSTOMER_BUTTON1_ACTION = "customerbutton1.action";
	static final String CUSTOMER_BUTTON2_ACTION = "customerbutton2.action";
	static final String CUSTOMER_BUTTON3_ACTION = "customerbutton3.action";

	/**
	 * The customer buttons can be used for general purpose.
	 * The next three constants decide, which forward destination to call
	 * whenever the button is pressed.
	 */
	static final String CUSTOMER_BUTTON1_FORWARD = "customerbutton1.forward";//"customerButton1Forward";
	static final String CUSTOMER_BUTTON2_FORWARD = "customerbutton2.forward";//"customerButton2Forward";
	static final String CUSTOMER_BUTTON3_FORWARD = "customerbutton3.forward";//"customerButton3Forward";

	/**
	 * The customer buttons can be used for general purpose.
	 * The next three constants decide, in which target frame the action should
	 * appear whenever the customer button is pressed.
	 */
	static final String CUSTOMER_BUTTON1_TARGET = "customerbutton1.target";//"customerButton1Target";
	static final String CUSTOMER_BUTTON2_TARGET = "customerbutton2.target";//"customerButton2Target";
	static final String CUSTOMER_BUTTON3_TARGET = "customerbutton3.target";//"customerButton3Target";

	/**
	 * If {@link #T}, the UI shows only characteristic values which the user
	 * may select.
	 * If {@link #F}, these values are hidden.
	 * To show non selectable values may make sense to show the user that the
	 * non selectable value might have been available, if he had selected a
	 * different value for a previous characteristic (upselling).
	 */
	public static final String ASSIGNABLE_VALUES_ONLY = "values.assignablesonly";//"assignableValuesOnly";
	/**
     * Show link to additional mimes of the values.
     */
    public static final String SHOW_LINK_TO_ADD_VALUE_MIMES = "values.showlinktomimes";

    /**
     * Threshold for multiple values in collapsed mode: when should a scroll bar be displayed.
     */
    public static final String MULITIPLE_VALUES_THRESHOLD = "values.multiple.threshold";

    /**
     * Selects which content for the name of a characteristics  value is to be displayed
     */
    public static final String CHARACTERISTICS_DESCRIPTION_TYPE = "characteristics.descriptiontype";
    
    /**
     * Selects which content for the name of a characteristics  value is to be displayed
     */
    public static final String VALUES_DESCRIPTION_TYPE = "values.descriptiontype";
    /**
     * Selects which content for the name of a characteristics  value is to be displayed
     */
    public static final String CHARACTERISTICS_MAX_DESCRIPTION_LENGTH = "characteristics.maxdescriptionlength";
    
    /**
     * Selects which content for the name of a characteristics  value is to be displayed
     */
    public static final String VALUES_MAX_DESCRIPTION_LENGTH = "values.maxdescriptionlength";

    /**
     * Default behavior. The description (30 characters) of the value is displayed in the work area.
     */
    public static final String shortDescriptionTextId="ShortDescription";
    /**
     * Instead of the description, the long description of the value is displayed up to the threshold (values.maxdescriptionlength). For collapsed values the long description is displayed up to the first line break or up to the threshold, dependent on whatever comes first.
     */
    public static final String longDescriptionTextId="LongDescription";
    /**
     * The description and the long description of the value are displayed in two columns. This setting works only with expanded values (checkboxes/radiobuttons). For collapsed values only the description is displayed.
     */
    public static final String shortAndLongDescriptionTextId="ShortAndLongDescription";
    /**
     * The ID and the description of the value are displayed.
     */
    public static final String iDAndShortDescriptionTextId ="IDAndShortDescription";
    /**
     * The ID and the long description of the value are displayed in two columns. For collapsed values the ID and the long description are displayed in one line up to the first line break or up to the threshold, dependent on whatever comes first.
     */
    public static final String iDAndLongDescriptionTextId ="IDAndLongDescription";
    /**
     * The ID and the description are displayed. The long description of the value is displayed in another column. This setting works only with expanded values (checkboxes/radiobuttons). For collapsed values only the description is displayed.
     */
    public static final String iDAndShortAndLongDescriptionTextId="IDAndShortAndLongDescription";
    /**
     * The ID is displayed.
     */
    public static final String iDTextId="ID";
        
    /**
	 * Show the language dependend {@link #T} or language neutral names {@link #F}
	 * for components, characteristics and values.
	 */
	public static final String SHOW_LANGUAGE_DEPENDENT_NAMES = "appearance.showlangdepnames";//"show_language_dependent_names";
	/**
	 * Allows to customize the way the status of the characteristics is displayed.
	 * {@link #T} means, classic "Traffic Light"
	 * {@link #F} or missing parameter value means "(*)" after the characteristic name
	 */   
	public static final String SHOW_STATUS_LIGHTS = "appearance.showstatuslights";//"showStatusLights";
    /**
     * Allows to use the explanation tool which consists of the components:
     * conflict solver (parameter: {@link #USE_CONFLICT_SOLVER}),
     * conflict explanation (parameter: {@link #USE_CONFLICT_EXPLANATION}).
     * {@link #T} means, the explanation tool is activated.
     * {@link #F} or missing parameter value means, no explanation tool
     * should be used.
     */    
    public static final String USE_EXPLANATION_TOOL = "conflicthandling.show";//"useExplanationTool";
    /**
     * {@link #T} means, when a conflict occurs the conflict solver will help to
     * solve the conflict.
     * {@link #F} or missing parameter value means, no conflict solver should be used.
     * Is only valid when the parameter {@link #USE_EXPLANATION_TOOL} is set to {@link #T}.
     */
    public static final String USE_CONFLICT_SOLVER = "conflictsolver.show";//"useConflictSolver";
    /**
     * Controls whether to show the solution rate of the conflict.
     * {@link #T} means Yes.
     * {@link #F} means No.
     * Is only valid when the parameter {@link #USE_CONFLICT_SOLVER} is set to {@link #T}.
     */
    public static final String CONFLICT_SOLVER_SHOW_CONFLICT_SOLUTION_RATE = "conflictsolver.solutionrate";//"conflictSolverShowConflictSolutionRate";
    /**
     * Indicates whether shortcuts should be used to solve conflicts.
     * {@link #T} means Yes.
     * {@link #F} means No.
     * Is only valid when the parameter {@link #USE_EXPLANATION_TOOL} is set to {@link #T}.
     * Is shown only if the parameter {@link #SHOW_MESSAGE_AREA} is set to {@link #T}.
     */
    public static final String USE_CONFLICT_HANDLING_SHORTCUTS = "conflicthandlingshortcuts.show";//"useConflictHandlingShortcuts";
    /**
     * Indicates whether the shortcut for the solution of value conflicts
     * should be used.
     * {@link #T} means Yes.
     * {@link #F} means No.
     * Is only valid when the parameters {@link #USE_EXPLANATION_TOOL}  and
     * {@link #USE_CONFLICT_HANDLING_SHORTCUTS} both are set to {@link #T}.
     */
    public static final String USE_SHORTCUT_VALUE_CONFLICT = "valueconflictshortcuts.show";//"useShortcutValueConflict";
    /**
     * Allows to display the explanation of conflicts.
     * {@link #T} means, the conflict explanation tool is activated and you get
     * explanation of conflicts.
     * {@link #F} or missing parameter value means, no conflict explanation tool
     * should be used.
     * Is only valid when the parameter {@link #USE_EXPLANATION_TOOL} is set to {@link #T}
     */
    public static final String USE_CONFLICT_EXPLANATION = "conflictexplanation.show";//"useConflictExplanation";
    /**
     * Defines the level on which the conflict explanation should be shown.
	 * values 0 or 1 are allowed.
	 * Default value is "1".
	 * with level 0 is only the text of the conflict type shown.
	 * with level 1 additionally the explation text assigned to the dependency is shown.
	 * Is only valid when the parameter {@link #USE_CONFLICT_EXPLANATION}
     * is set to {@link #T}
     */
    public static final String CONFLICT_EXPLANATION_LEVEL = "conflictexplanation.level";//"conflictExplanationLevel";
    /**
     * Defines the block tag to use in order to display the conflict explanation text.
	 * The default value is "UL". Alternatively the value "OL" or
	 * any other HTML layout tag is possible.
	 * Is only valid when the parameter {@link #USE_CONFLICT_EXPLANATION}
     * is set to {@link #T}.
     */
    public static final String CONFLICT_EXPLANATION_TEXT_BLOCK_TAG = "conflictexplanation.textblocktag";//"conflictExplanationTextBlockTag";
    /**
     * Defines the line tag to use in order to display the conflict explanation text.
     * The default value is "LI". Any other HTML layout tag is possible.
     * Is only valid when the parameter {@link #USE_CONFLICT_EXPLANATION}
     * is set to {@link #T}.
     */
    public static final String CONFLICT_EXPLANATION_TEXT_LINE_TAG = "conflictexplanation.textlinetag";
								    //"conflictExplanationTextLineTag";
	/**
	 * show additional infos like long text description {@link #T} or not {@link #F}
	 * (not active)
	 */
	public static final String SHOW_DETAILS = "characteristics.showdetails";//"showDetails";
	/**
	 * open the details in a new window {@link #T} or inside the
	 * characteristic frame {@link #F}
	 */
	public static final String SHOW_DETAILS_IN_NEW_WINDOW = "characteristics.detailsinwindow";//"showDetailsInNewWindow";
	/**
     * Indent characteristics as soon as another characteristic of the same group has
     * a mime-object or a value with a mime-object that is displayed in the work-area
     * {@link #T}.
     * The characteristics will be indeted by the default width of the mimes shown in 
     * the work-area (width=80).
     * If this is disabled {@link #F} the characteristics will be aligned left until they 
     * have mimes on their own.
     */    
    public static final String CSTIC_INDENTATION = "characteristics.indentation";
    
    /**
     * Show the link to additional mimes of the characteristics.
     */
    public static final String SHOW_LINK_TO_ADD_CSTIC_MIMES  = "characteristics.showlinktomimes";
    
    /**
     * Values T|F.
     * Enables/Disbles the display of cstic messages.
     */
    public static final String SHOW_MESSAGE_CSTICS = "characteristics.messagecstics.enable";
    
    /**
     * Identifying prefix which determines that this cstic is a message cstic
     */
    public static final String MESSAGE_CSTICS_PREFIX = "characteristics.messagecstics.prefix";    

    /**
     * Values T|F.
     * Enable/Disable the additional display of the message id (i.e. the value name).
     */
    public static final String MESSAGE_CSTICS_SHOW_ID = "characteristics.messagecstics.showid";
    
	/**
	 * open a seperate frameset for MultiFunctionalityArea {@link #T} or not {@link #F}
	 */
	public static final String SHOW_MULTI_FUNCTIONALITY_AREA = "multifunctionalarea.show";//"showMultiFunctionalityArea";
	/**
	 * Values T|F.
	 * Opens a separate frame for the message area in case messages exist.
	 * Enables the usage of following features:
	 * - standard messages
	 * - conflict handling shortcuts
	 */
	public static final String SHOW_MESSAGE_AREA = "messagearea.show";//"showMessageArea";
	/**
	 * Values T|F.
	 * Shows standard messages which are not created dynamically
	 * Is shown only if the parameter {@link #SHOW_MESSAGE_AREA} is set to {@link #T}.
	 */
	public static final String SHOW_STANDARD_MESSAGE_IN_MESSAGE_AREA = "messagearea.showstandardmessage";//"showStandardMessageInMessageArea";
    
    /**
     * Values T/F.
     * Enable/disable the display of loading messages. 
     * Loading messages are displayed on the comparison screen.
     */
    public static final String SHOW_LOADING_MESSAGES = "messagearea.loadingmessages.show";

    /**
	 * Shows the apply button, if the value is true. Otherwise the
	 * apply button is not available in the UI
	 */
	public static final String SHOW_APPLY_BUTTON = "applybutton.show"; //"showApplyButton";
	/**
	 * Action that the apply button will execute
	 */
	public static final String APPLY_BUTTON_ACTION = "applybutton.action";//"applyButtonAction";
	/**
	 * Shows the back button, if the value is true. Otherwise the
	 * back button is not available in the UI
	 */
	public static final String SHOW_BACK_BUTTON = "backbutton.show";//"showBackButton";
	/**
	 * Shows the back button, if the value is true. Otherwise the
	 * back button is not available in the UI
	 */
	public static final String BACK_BUTTON_ACTION = "backbutton.action";//"backButtonAction";
	/**
	 * Specifies the number of columns for instances
	 */
	static final String INSTANCE_NO_COLUMNS  = "instance.nocolumns";//INSTANCE + "." + "noColumns";
	/**
	 * Specifies the number of columns for characteristics
	 */
	static final String CHARACTERISTIC_NO_COLUMNS  = "characteristics.nocolumns";//CHARACTERISTIC + "." + "noColumns";
	/**
	 * Specifies the number of columns for characteristic values
	 */
	static final String CHARACTERISTIC_VALUE_NO_COLUMNS  = "values.nocolumns";//CHARACTERISTIC_VALUE + "." + "noColumns";
	/**
	 * If the configuration model contains informations how to group characteristics
	 * together, display them ({@link #T}).
	 * When {@link #F}, show the characteristic in a single list.
	 */
	public static final String USE_GROUP_INFORMATION = "groups.show";//"use_group_information";

	public static final String USE_GROUP_INFORMATION_MODIFIED = "groups.showModified";//"use_group_information";
	
    /**
     * Defines the number of groupTabs shown at one time.
     */
	public static final String GROUP_QUANTITY = "groups.quantity";//"group_quantity";
    
    /**
     * Defines whether the groups (and group tabs) without characteristics are shown
     * ({@link #T}) or not ({@link #F})(default)
     */    
	public static final String SHOW_EMPTY_GROUPS = "groups.showemptygroups";//"show_empty_groups";

    /**
     * Defines whether the group name is displayed in the work area ({@link #T}) or 
     * not ({@link #F})(default).
     */    	
    public static final String SHOW_GROUP_NAME_IN_WORK_AREA = "groups.shownameinworkarea";
    
    /**
     * Defines if the instances on the customizationlist should displayed expanded
     * ({@link #T}) or not ({@link #F}).
     */
	public static final String SHOW_CUSTOMIZATIONLIST_INSTANCES_EXPANDED = "customizationlist.instancesexpanded";//"showCustomizationListInstancesExpanded";
    /**
	 * Defines if the conflict tracer should be shown ({@link #T}) or not ({@link #F}).
	 */
	public static final String SHOW_CONFLICT_TRACE = "conflicttrace.show";//"showConflictTrace";
	/**
	 * Show a customer defined tab-rider on the multi functionality area if {@link #T}.
	 * Possible valus: {@link #T} | {@link #F}.
	 */
	public static final String SHOW_CUSTOMER_TAB = "customertab.show";//"showCustomerTab";
	/**
	 * Defines the order of the multi functionality area tab-riders.
	 */
	public static final String MFA_TAB_ORDER = "multifunctionalarea.taborder";//"mfaTabOrder";    
    /**
	 * The IPC server may handle several clients (MANDT). Analog to the login
	 * into a SAP system (R/3 or CRM), you need to specify the client here.
	 */
	public static final String CLIENT = "ipc.client";//"IPC_CLIENT";
 	/**
 	 * This parameter defines the email id of the admin to be displayed on the error screen
 	 * {@link #T} or not {@link #F}.
 	 */
 	public static final String ADMIN_EMAIL_ID = "errorcontrl.email";

 	/**
 	 * This parameter defines the type of the system i.e. production or test.
 	 * {@link #T} or not {@link #F}.
 	 */
 	public static final String SYSTEM_TYPE = "errorcontrl.type";
	public static final String TYPE = "TYPE";
	/**
	 * Allows customizing depending on the passed parameter.
	 * Possible values:
	 * {@link #ISA}
	 * {@link #CRM}
	 * {@link #THIRD_PARTY}
	 * {@link #VEHICLE_MANAGER}
	 */
	public static final String IPC_SCENARIO = "ipcScenario";
	public static final String ISA = "ISA";
	public static final String CRM = "CRM";
	/**
	 * Allows customizing depending on the passed design.
	 * Possible values:
	 */
	public static final String DESIGN = "design";
	/**
	 * Only used in R/3 scenarios.
	 * Simulates an R/3 event in order to tell the calling application that
	 * the configuration process is finished.
	 * The VehicleManager passes the value "SAPEVENT:CFG".
	 */
	public static final String HOOK_URL = "structure.hookurl";//"hookURL";
    
    /**
     * This URL describes the server directory, to which MIME documents of the masterdata 
     * (product, charaterstics, values) from the backend system are published. This allows 
     * you to place your MIME documents outside the web-application.<br/>
     * In the application, this URL will be concatenated with the name of each MIME 
     * document to build the full URL. The value of this parameter must correspond to the
     * settings that you have used in the document publisher 
     * (report CFG_DOCUMENT_PUBLISHER: FTP server host and document directory). <br/>
     * The root directory of the IPC web-application is used if this parameter is not set.<br/>
     */
    public static final String MIMES_MASTERDATA = "mimes.ipc.masterdata";
    
	/**
	 * This is the language used for language dependend texts in the configuration.
	 * In standalone scenarios, the IPC also uses this language to display UI texts
	 * like button labels.
	 * However in the Internet Sales scenario, this is the language of the shop
	 * and may differ from the language of the http session.
	 */
	 //this parameter is used in the isacore, therefore it remains like it is
	public static final String LANGUAGE = "language";
	/**
	 * Specifies the product to be configured.
	 * @see #PRODUCT_ID
	 */
	public static final String PRODUCT_ID = "productId";

    /**
     * Specifies the product to be configured.
     * @see #PRODUCT_ID_ERP
     */
    public static final String PRODUCT_ID_ERP = "productIdERP";

    /**
     * Specifies the product to be configured.
     * @see #PRODUCT_GUID
     */
    public static final String PRODUCT_GUID = "productGuid";

	/**
	 * Specifies the product type
	 * @see #PRODUCT_TYPE
	 */
	public static final String PRODUCT_TYPE = "productType";
	/**
	 * Additional identifier for the product to be configured.
	 * A fully specified key for a product consists of {@link #PRODUCT_ID} and
	 * {@link #PRODUCT_LOGSYS}.
	 * This parameter is required if the master data may contain the same
	 * product from different source systems (e.g.
	 * 1. two different (R/3 or other) backend systems where the product was downloaded from
	 * 2. the product was created locally and downloaded from a backend system.
	 */
	public static final String PRODUCT_LOGSYS = "product_logsys";
	/**
	 * In case the configuration model changes over time and you create
	 * several versions of the same model or even several different models,
	 * you may distinct these versions using this parameter.
	 * Example: you have created two versions of a configuration model
	 * (knowledge base) with "valid from" 1.1.2002 and 1.2.2002.
	 * Passing a kbDate before 1.2.2002 will load the first version, a later
	 * kbDate will load the second version of the knowledge base.
	 */
	public static final String KB_DATE = "kbDate";
	/**
	 * Additional identifier for the knowledge base.
	 * A fully specified identifier for a knowledge base consists of
	 * {@link #KB_LOGSYS}
	 * {@link #KB_NAME} and
	 * {@link #KB_VERSION}
	 * @see #PRODUCT_LOGSYS
	 * In general the KB_LOGSYS should be the same as {@link #PRODUCT_LOGSYS}.
	 */
	public static final String KB_LOGSYS = "kbLogsys";
	/**
	 * Identifier for the knowledge base used for configuration.
	 * @see #KB_LOGSYS
	 * @see #KB_VERSION
	 */
	public static final String KB_NAME = "kbName";
	/**
	 * Identifier for the knowledge base used for configuration.
	 * @see #KB_LOGSYS
	 * @see #KB_NAME
	 */
	public static final String KB_VERSION = "kbVersion";
	/**
	 * Knowledge base profile. A knowledge base may have several profiles.
	 * Each profile is associated with a product or class used es entry into
	 * the configuration.
	 */
	public static final String KB_PROFILE = "kbProfile";
	/**
	 * Change counter of the knowledge base. Each change to a knowledge base
	 * {@link #KB_LOGSYS}, {@link #KB_NAME}, {@link #KB_VERSION} increments
	 * the build number.
	 */
	public static final String KB_BUILD_NUMBER = "kbBuildNumber";
	/**
	 * Specifies the identifier of an existing configuration.
	 * @see #CONFIG_ID
	 */
	public static final String CONFIG_ID = "configId";
	/**
	 * Activate({@link #T}) finding product variants or not ({@link #F}).
	 */	
	public static final String ENABLE_VARIANT_SEARCH = "productvariants.enablesearch";//"enableVariantSearch";	
	
	/**
	 * Activate({@link #T}) mode: Root product is a changeable product variant.
	 */	
	public static final String CHANGEABLE_PRODUCT_VARIANT_MODE = "changeableProductVariantMode";
	/**
	 * Find product variants automatically ({@link #T})
	 * or with user manual user interaction ({@link #F}).
	 * @deprecated Only used in CRM ABAP layer
	 */
	public static final String ENABLE_AUTOMATIC_SEARCH = "enableAutomaticSearch";
	/**
	 * Ask the user ({@link #T}) or not ({@link #F}) before changing the original product.
	 * Only if {@link #ENABLE_AUTOMATIC_SEARCH}={@link #T}.
	 * @deprecated Only used in CRM ABAP layer
	 */
	public static final String ASK_BEFORE_AUTO_REPLACE = "askBeforeAutoReplace";
	/**
	 * ({@link #T}) or ({@link #F}). Should only product variants be treated as complete?
	 * Y: Configurations of KMATS are always incomplete.
	 * For special product types like service products.
	 */
	public static final String ONLY_VAR_FIND = "onlyVarFind";
	/**
	 * Depending on the number of characteristics, the search for product
	 * variants may return a large number of results.
	 * This parameter specifies the number of results after which the search
	 * is cancelled.
	 */
	public static final String LIMIT_OF_PRODUCT_VARIANTS = "productvariants.limitof";//"limitOfProductVariants";
	/**
	 * When using the fuzzy search for product variants (characteristic values
	 * of the current configuration may differ from the product variant), this
	 * parameter tells the search algorithm, how big this difference may be.
	 */
	public static final String QUALITY_OF_PRODUCT_VARIANTS = "productvariants.qualityof";//"qualityOfProductVariants";

	/**
	 * When using the fuzzy search for product variants, the product variants found
	 * may differ from the current configuration.
	 * When expressing how good the product variant with the
	 * current configuration matches, it is on a scale from 0...({@link #MATCH_POINTS_OF_PRODUCT_VARIANTS})
	 */
	public static final String MATCH_POINTS_OF_PRODUCT_VARIANTS = "productvariants.matchpointsof";//"matchPointsOfProductVariants";

/**
	 * This parameter defines whether the IPC will
	 * generate sub items {@link #Y} or not {@link #N}.
	 */
	public static final String SUB_ITEMS_ALLOWED = "behavior.subitemsallowed";//"subItemsAllowed";

	/**
	 * This parameter defines if spaces in language dependent names should be be shown in the web-browser
	 * {@link #T} or not {@link #F}.
	 */
	public static final String SHOW_INNER_SPACES_IN_LANGUAGE_DEPENDENT_NAMES = "behavior.langdepnames.spaces";//"showInnerSpacesInLanguageDependentNames";

	/**
	 * This parameter controls the expand/collapse behavior of components. 
	 * {@link #T} or not {@link #F}.
	 */
	public static final String AUTOMATIC_COLLAPSE_COMPONENTS = "behavior.components.automatic.collapse";

	/**
	 * This parameter controls the expand/collapse behavior of groups. 
	 * {@link #T} or not {@link #F}.
	 */
	public static final String AUTOMATIC_COLLAPSE_GROUPS = "groups.automatic.collapse";

	/**
	 * This parameter defines if spaces in language independent names should be be shown in the web-browser
	 * {@link #T} or not {@link #F}.
	 */
	public static final String SHOW_INNER_SPACES_IN_LANGUAGE_INDEPENDENT_NAMES = "behavior.langindepnames.spaces";//"showInnerSpacesInLanguageIndependentNames";
	
	/**
	 * This parameter specifies the mode of the import/export functionality
	 */
	public static final String IMPORT_EXPORT_MODE = "behavior.importexport.mode";
	
    /**
     * This parameter specifies the mode of the search/set functionality
     */
    public static final String SEARCH_SET_MODE = "behavior.searchset.mode";

    /**
     * This parameter specifies the delimiter of the search/set functionality
     */
    public static final String SEARCH_SET_DELIMITER = "behavior.searchset.delimiter";    
	/**
	 * The calling application may specify a stylesheet different from the
	 * default stylesheet.
	 */
	public static final String STYLESHEET = "appearance.stylesheet";//"stylesheet";
    /**
     * Show input field mask for numerical values (integer, float and date) when {@link #T}.
     */
    public static final String INPUTFIELD_MASK = "appearance.showinputfieldmask";
    /**
     * Enables the display link to the calendar control for date values when {@link #T}.
     */
    public static final String CALENDAR_CONTROL = "appearance.showcalendarcontrol";     
	/**
     * Displays the calendar control embedded (not as pop-up) when {@link #T}.
     */
    public static final String CALENDAR_CONTROL_EMBEDDED = "appearance.showcalendarcontrol.embedded";     

    /**
     * Displays an information text (like "Loading page...") when retrieving data.
     */
    public static final String SHOW_PROGRESS_INDICATOR = "appearance.showprogressindicator";     

	/**
	 * In thirdparty scenarios ({@link #CALLER} ,the result of the configuration is sent by the ui
	 * via a http request. This url is used as destination url for this request.
	 */
	public static final String TOPFRAME = "structure.topframe";//"topframe";
	// restrictions
	/**
	 * Used only for contract specific restrictions.
	 * Specified the type of owner, f.e. contract or order.
	 */
	public static final String OWNER_TYPE       = "ownerType";
	/**
	 * Used only for contract specific restrictions.
	 * Identifier (GUID) of the owner object, f.e. contract or order.
	 */
	public static final String OWNER_ID         = "ownerId";
	/**
	 * Used only for contract specific restrictions.
	 * Type of database repository. Default is 'IBASE'.
	 */
	public static final String REPOSITORY_TYPE  = "repositoryType";
	/**
	 * Parameter for contract specific configuration restrictions.
	 * The calling application determines the {@link #RESTRICT_ID}
	 * as function of application specific criteria like contract id,
	 * customer id, etc.
	 * The ipc reads the restrictions to apply according to the {@link #RESTRICT_ID}.
	 */
	public static final String RESTRICT_ID      = "restrictId";

	//parameters to attach to running IPC Session
	/**
	 * The calling application may have started an IPC session in a previous
	 * request or using a different communication channel.
	 * An IPC session is identified by:
	 * {@link #IPC_HOST}, {@link #IPC_PORT} and {@link #IPC_SESSION}
	 * The calling application may want to continue to work in this session.
	 * A typical scenario using this technique is {@link #CRM}
	 */
	public static final String IPC_HOST = "ipc.host"; //"IPC_HOST";
	public static final String IPC_PORT = "ipc.port"; //"IPC_PORT";
//	TODO This is now handled by the Logon Module (crm/tc/user)
//	public static final String IPC_SYSNR = "ipc.instanceNo"; //crm online uses connection to appserver in order to attache to existing session
    public static final String IPC_TOKENID = "ipc.tokenId"; //vmc session token
	public static final String DISPATCHER = "ipc.dispatcher"; //"DISPATCHER";
	public static final String ENCODING = "ipc.encoding"; //"ENCODING";
	/**
     * Specifies the security level of the web-application.
	 * Secure mode is switched off by default (security.level = 0).
	 */
	public static final String SECURITY_LEVEL = "security.level";
	/**
	 * Specifies the SSL keystore type.
	 * Default keystore type is JKS.
	 */
	public static final String SSL_KEYSTORE_TYPE = "security.ssl.keystoreType";
	/**
	 * Specifies the SSL keystore location.
	 */
	public static final String SSL_KEYSTORE_LOCATION = "security.ssl.keystoreLocation";
	/**
	 * Specifies the SSL keystore password.
	 */
	public static final String SSL_KEYSTORE_PASSWORD = "security.ssl.keystorePassword";
	/**
	 * Specifies the SSL key alias.
	 */
	public static final String SSL_KEY_ALIAS = "security.ssl.keyAlias";
	/**
	 * Specifies the SSL key password.
	 */
	public static final String SSL_KEY_PASSWORD = "security.ssl.keyPassword";
	/**
	 * The calling application may have created IPC objects in a previous request
	 * or using a different communication channel.
	 * The calling application may want to continue to work on the state of these
	 * objects.
	 * A document is equivalent to a shopping basket or order document.
	 * A document is identified by:
	 * @see #IPC_HOST
	 * @see #IPC_PORT
	 * @see #IPC_SESSION and
	 * {@link #IPC_DOCUMENTID}
	 * @see com.sap.spc.base.SPCDocument
	 */
	
	public static final String DESIGNER_MODE = "behavior.designer.enable";
	
	public static final String UIMODEL_GUID = "uimodel.guid";
	public static final String UIMODEL_GUID_INITIAL = "00000000000000000000000000000000";
	
	public static final String UIMODEL_SCENARIO = "uimodel.scenario";
	
	public static final String UIMODEL_SCENARIO_B2B = "B2B";
	
	public static final String UIMODEL_SCENARIO_B2C = "B2C";
	
	public static final String UIMODEL_ROLEKEY = "uimodel.rolekey";
	
	public static final String DOCUMENTID = "DOCUMENTID";
	/**
	 * An item is equivalent to an order position.
	 * @see #DOCUMENTID
	 * An item is identified by the identifiers of the {@link #DOCUMENTID} and
	 * the {@link ITEMID}
	 */
	public static final String ITEMID = "ITEMID";
	public static final String UNICODE_LITTLE = "UnicodeLittle";
	/**
	 * The calling application may pass the identifiers of an
	 * {@link #com.sap.spc.base.SPCItem} in form of an
	 * {@link #com.sap.spc.remote.client.object.IPCItemReference}.
	 */
	public static final String IPC_ITEM_REFERENCE = "ipcItemReference";
	public static final String SET_INITIAL_UI_CONTEXT_VALUES = "setInitialUIContextValues";
	public static final String IPC_CONFIG_REFERENCE = "ipcConfigReference";
	/**
	 * The calling application may pass an implementation of the interface
	 * {@link #com.sap.spc.remote.client.object.IPCItemFilter}.
	 * The filter may be passed to apis that return a set of
	 * {@link #com.sap.spc.remote.client.object.IPCItem}
	 * Currently this is the method
	 * {@link #com.sap.spc.remotr.client.object.IPCDocument.getItems(IPCItemFilter ipcItemFilter)}.
	 */
	public static final String IPC_ITEM_FILTER = "ipcItemFilter";

	// parameters for the command/action layer
	// and their mapping to the CRM or R3 systems
	public static final String CRM_START_CONFIGURATION = "ipc_createConfig";
	public static final String CRM_SHOW_CONFIGURATION = "ipc_websce";

	//pricing parameter (R3 model) -->
	public static final String DOCUMENT_CURRENCY_UNIT = "documentCurrencyUnit";
	public static final String LOCAL_CURRENCY_UNIT =  "localCurrencyUnit";
	public static final String COUNTRY =  "country";
	public static final String SALES_ORGANISATION =  "salesOrganisation";
	public static final String DISTRIBUTION_CHANNEL =  "distributionChannel";
	public static final String BASE_QUANTITY_UNIT =  "baseQuantityUnit";
	public static final String BASE_QUANTITY_VALUE =  "baseQuantityValue";
	public static final String SALES_QUANTITY_UNIT =  "salesQuantityUnit";
	public static final String SALES_QUANTITY_VALUE =  "salesQuantityValue";
	public static final String DEPARTURE_COUNTRY =  "departureCountry";
	public static final String PRICING_DATE =  "pricingDate";
	public static final String PRICING_RELEVANT = "pricingRelevant";
	public static final String PROCEDURE_NAME =  "procedureName";
	/**
	 * pricing issue: explicit header attributes
	 */
	public static final String PRC_HDR_ATTR = "PRC_HDR_ATTR-";
	/**
	 * pricing issue: explicit item attributes
	 */
	public static final String PRC_ITM_ATTR = "PRC_ITM_ATTR-";
	
	public static final String PRC_TIMESTAMP = "PRC_TIMESTAMP-";
	/**
	 * pricing issue: prefix to use for further pricing/tax header parameters
	 */
	 public static final String GENERIC_DOC_ATTR = "GENERIC_DOC_ATTR-";
	/**
	 * pricing issue: prefix to use for further pricing/tax item parameters
	 */
	 public static final String GENERIC_ITM_ATTR = "GENERIC_ITM_ATTR-";


	//external configuration params
	//the following parameter is passed in the request from the CRM
	//in the CRM online scenario
	public String EXTERNAL_CONFIGURATION_ID = "CFG-CONFIG_ID[1]";

	public static final String EXT_CONFIG_KB_PROFILE = "CFG-KBPROFILE";
	public static final String EXT_CONFIG_KB_LANGUAGE = "CFG-KBLANGUAGE";
    public static final String EXT_CONFIG_POSITION = "CFG-POSEX";
	public static final String EXT_CONFIG_NAME = "CFG-CONFIG_ID";
	public static final String EXT_CONFIG_ROOT_ID = "CFG-ROOT_ID";
	public static final String EXT_CONFIG_SCE_VERSION = "CFG-SCE";
	public static final String EXT_CONFIG_KB_NAME = "CFG-KBNAME";
	public static final String EXT_CONFIG_KB_VERSION = "CFG-KBVERSION";
	public static final String EXT_CONFIG_KB_BUILD = "CFG-KBBUILD";
	public static final String EXT_CONFIG_COMPLETE = "CFG-COMPLETE";
	public static final String EXT_CONFIG_CONSISTENT = "CFG-CONSISTENT";
	public static final String EXT_CONFIG_INFO = "CFG-CFGINFO";
	public static final String EXT_INST_ID= "INS-INST_ID";
    public static final String EXT_INST_CONFIG_ID = "INS-CONFIG_ID";
	public static final String EXT_INST_OBJECT_TYPE = "INS-OBJ_TYPE";
	public static final String EXT_INST_CLASS_TYPE = "INS-CLASS_TYPE";
	public static final String EXT_INST_OBJECT_KEY = "INS-OBJ_KEY";
	public static final String EXT_INST_OBJECT_TEXT = "INS-OBJ_TXT";
	public static final String EXT_INST_QUANTITY = "INS-QUANTITY";
	public static final String EXT_INST_QUANTITY_UNIT = "INS-QUANTITY_UNIT";
	public static final String EXT_INST_AUTHOR = "INS-AUTHOR";
	public static final String EXT_INST_CONSISTENT="INS-CONSISTENT";
	public static final String EXT_INST_COMPLETE = "INS-COMPLETE";
	//parts
	public static final String EXT_PART_CONFIG_ID = "PRT-CONFIG_ID";
	public static final String EXT_PART_PARENT_ID = "PRT-PARENT_ID";
	public static final String EXT_PART_INST_ID = "PRT-INST_ID";
	public static final String EXT_PART_POS_NR = "PRT-PART_OF_NO";
	public static final String EXT_PART_OBJECT_TYPE = "PRT-OBJ_TYPE";
	public static final String EXT_PART_CLASS_TYPE = "PRT-CLASS_TYPE";
	public static final String EXT_PART_OBJECT_KEY = "PRT-OBJ_KEY";
	public static final String EXT_PART_AUTHOR = "PRT-AUTHOR";
	public static final String EXT_PART_SALES_RELEVANT = "PRT-SALES_RELEVANT";
	//values
	public static final String EXT_VALUE_ = "VAL-CONFIG_ID";
	public static final String EXT_VALUE_INSTANCE_ID = "VAL-INST_ID";
	public static final String EXT_VALUE_CSTIC_NAME ="VAL-CHARC";
	public static final String EXT_VALUE_CSTIC_LNAME ="VAL-CHARC_TXT";
	public static final String EXT_VALUE_NAME ="VAL-VALUE";
	public static final String EXT_VALUE_LNAME ="VAL-VALUE_TXT";
	public static final String EXT_VALUE_AUTHOR ="VAL-AUTHOR";
	public static final String EXT_VALUE_CONFIG_ID ="VK-CONFIG_ID";
	public static final String EXT_VALUE_INST_ID ="VK-INST_ID";
	public static final String EXT_PRICE_INSTANCE_ID = "VK-INST_ID";
	public static final String EXT_PRICE_KEY ="VK-VKEY";
	public static final String EXT_PRICE_FACTOR ="VK-FACTOR";

	public static final String CONTEXT_NAME = "CONTEXT-NAME";
	public static final String CONTEXT_VALUE = "CONTEXT-VALUE";

	public static final String POS_CONFIG_ID = "POS-CONFIG_ID";
	public static final String POS_INST_ID = "POS-INST_ID";
	public static final String POS_POS_GUID = "POS-POS_GUID";

	public static final String EXT_ATTR_CHARC = "EXT_ATTR-CHARC";
	public static final String EXT_ATTR_VISIBLE = "EXT_ATTR-VISIBLE";
	public static final String EXT_ATTR_READONLY = "EXT_ATTR-READONLY";
	
	//final constants for the allowed values for characteristics.showExpandLink XCM parameter
	public static final  int CSTIC_SHOW_EXPAND_FALSE =0;
	public static final  int CSTIC_SHOW_EXPAND_TRUE =1;
	public static final  int CSTIC_SHOW_EXPAND_CSTIC_DEP =2;
	
	public static final String CSTIC_SHOW_EXPAND_LINK = "characteristics.showexpandlink";
	//XCM values for xcm parameter values.expanded
	public static final String CSTIC_VALUE_EXPAND_CSTIC_DEP = "csticdep";
    // Flag to indicate that "Display All Options" setting has been modified in this session
    public static final String CSTIC_VALUES_EXPANDED_MODIFIED = "valuesExpandedModified"; 

    public static final String PRICING_APPLICATION = "pricingApplication";
    public static final String USAGE = "pricingUsage";
    public static final String PERFORM_PRICING_ANALYSIS = "performPricingAnalysis";

    public static final String PRICING_STATISTICAL = "pricing.statistical";

	// flag to indicate that a "Reset Configuration" was triggered
	public static final String RESET_CONFIG_DONE = "config.reset";
}
