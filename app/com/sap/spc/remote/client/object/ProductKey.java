package com.sap.spc.remote.client.object;

public interface ProductKey {
    
    public String getProductId();
    
    public String getProductType();
    
    public String getProductLogSys();

}
