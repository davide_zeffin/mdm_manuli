/************************************************************************

	Copyright (c) 2001 by SAPMarkets Europe GmbH, Germany, SAP AG

	All rights to both implementation and design are reserved.

	Use and copying of this software and preparation of derivative works
	based upon this software are not permitted.

	Distribution of this software is restricted. SAP does not make any
	warranty about the software, its performance or its conformity to any
	specification.

**************************************************************************/

package com.sap.spc.remote.client.object;

import java.util.List;

/**
 * MimeObjectContainer
 * This container is able to filter mime objects by their type
 */
public interface MimeObjectContainer
{
  /**
   * Use this method to retreive all mime objects in this container
   */
  List getMimeObjects();

  /**
   * Use this method to retreive all mime objects of this type
   */
  List getMimeObjectsByType(String type);

  /**
   * Use this method to retreive one mime objects of this type and the
   * given index
   */
  MimeObject getMimeObjectByType(String type,int index);

  /**
   * This method enables you to add one MimeType
   */
  void add(MimeObject mimeObject);

  /**
   * @return true, if this container contains no mime objects
   */
  boolean isEmpty();
}
