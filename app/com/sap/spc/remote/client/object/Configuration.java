/************************************************************************

	Copyright (c) 2001 by SAPMarkets Europe GmbH, Germany, SAP AG

	All rights to both implementation and design are reserved.

	Use and copying of this software and preparation of derivative works
	based upon this software are not permitted.

	Distribution of this software is restricted. SAP does not make any
	warranty about the software, its performance or its conformity to any
	specification.

**************************************************************************/

package com.sap.spc.remote.client.object;

import java.util.HashMap;
import java.util.List;

import com.sap.spc.remote.client.util.ext_configuration;

/**
 * Client view of a configuration. The methods in this class mirror
 * these of com.sap.sce.front.base.Config (as far as possible with
 * the data the IPC commands provide). The documentation assumes
 * familarity with the front.base API.<p>
 */
public interface Configuration extends Closeable
{
	/**
	 * Returns the config id of this configuration.
	 */
	public String getId();

	/**
	 * Returns knowledge base information about this configuration.
	 */
	public KnowledgeBase getKnowledgeBase();
	
	
	/**
	 * Returns the knowledge base profile used for this configuration.
	 */
	public KBProfile getActiveKBProfile();

	/**
	 * Returns the names of the configuration context.
	 * like VBAK-KUNNR
	 */
	public String[] getContextNames();

	/**
	 * Returns the values of the configuration context.
	 */
	public String[] getContextValues();

	/**
	 * Returns true if this configuration is available on the server,
	 * and false if the configuration id is unknown or the configuration
	 * couldn't be loaded by the configuration engine.
	 */
	public boolean isAvailable();

	/**
	 * Returns true, if this config is complete.
	 */
	public boolean isComplete() throws IPCException;

	/**
	 * Returns true, if this config is consistent.
	 */
	public boolean isConsistent() throws IPCException;

	/**
	 * Returns true, if this config is a single level configuration.
	 */
	public boolean isSingleLevel() throws IPCException;

	/**
	 * Returns true if this configuration uses SCE advanced mode.
	 */
	public boolean isSceMode() throws IPCException;


    /**
     * Returns true, if the configuration is in initial state().
     * Returns false, if the configuration has changed during the current session.
     */
     public boolean isInitial() throws IPCException;

	/**
	 * Returns all part instances that are present in this configuration,
	 * ie. all instances in the tree below the root instance.
	 * @return an array of Instances, sorted in depth-first preorder (root,
	 * first child of root, first child of this child, ...)
	 */
	public List getInstances();

	/**
	 * Returns an instance by it's id
         * @param instId the id of the instance
	 * @return the instance object
	 */
	public Instance getInstance(String instId);

	/**
	 * Returns the root instance of this configuration. There is always
	 * a root instance - this method never returns null.
	 */
	public Instance getRootInstance();

	/**
	 * Returns all non-part instances of this configuration.
	 */
	public List getNonPartInstances();

	/**
	 * Returns the explanation tool
	 */
	 public ExplanationTool getExplanationTool();

	/**
	 * Returns an external representation of this config in the ext_configuration
	 * format.
	 */
	public ext_configuration toExternal();

	/**
	 * Returns an external representation of this config in a HashMap
	 */
	public HashMap getExternalConfigurationStandalone() throws IPCException;

	/**
	 * Returns an external representation of this config in a HashMap
	 */
	public HashMap getExternalConfiguration () throws IPCException;

	/**
	 * Performs a completeness check (incl. dependency knowledge, BOM explosion)
	 * on this configuration. Forces an update of the whole configuration.
	 */
	public void check();

	/**
	 * Returns a timestamp (System.currentTimeMillis) of when this configuration
	 * was last changed.
	 */
	public long getChangeTime();

	/**
	 * Updates the time at which this config was last changed (for internal use only)
	 */
	public void setChangeTime();

        public void decompose();

        /**
         * Resets the configuration
         */
         public void reset() throws IPCException;


	/**
	 * Requests deletion of an instance. Returns true only if the instance was successfully deleted.
	 */
	public boolean deleteInstance(Instance instance);

    /**
     * Call this method to send all changes of the configuration to the server
     */
    void flush();

	/**
	 * Returns explanations for the occured conflicts as Strings.
	 * If the configuration contains no conflicts null is returned.
	 * Since it makes no sense to cache conflicts this methode should
	 * not be executed too often because Server-Command is executed every time
	 * this method is invoked.
	 */
	public List getConflictInfos();
    
    /**
     * @return Array of instance names of this configuration
     */
    public String[] getInstanceNames();
    
    /**
     * @return Loading status of configuration.
     */
    public LoadingStatus getLoadingStatus();
       
    /**
     * Compares to external configurations. Uses AP-CFG FM to calculate the delta. Returns a
     * ComparisonResult object that encapsulates the result of the comparison.
     * @param extConfig1 external configuration 1
     * @param extConfig2 external configuration 2
     * @return ComparisonResult object encapsulating the result
     */
    public ComparisonResult compareConfigurations(ext_configuration extConfig1, ext_configuration extConfig2);
    
    public void setContextNames(String[] contextNames);
    
    public void setContextValues(String[] contextValues); 
    
    public void setCacheDirty(boolean b);
    
    public boolean isCacheDirty();
    
    public void setInstanceCacheDirty(boolean b);
    
    public boolean isInstanceCacheDirty();
}
