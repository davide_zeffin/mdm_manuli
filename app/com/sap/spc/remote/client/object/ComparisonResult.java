package com.sap.spc.remote.client.object;

import java.util.HashMap;
import java.util.List;

import com.sap.spc.remote.client.util.ext_configuration;

/**
 * The result of the configuration comparison.
 */
public interface ComparisonResult {

    /**
     * @return list of errors that occured during comparison
     */
    public List getComparisonErrors();

    /**
     * Returns the list of deleted instances.
     * These instance deltas are also part of the <code>instanceDeltas</code> list.
     * @return list of deleted instances
     */
    public List getDeletedInstances();

    /**
     * @return flag indicating whether configuration 1 and 2 are different
     */
    public boolean isDifferent();

    /**
     * @return flag indicating whether errors occured during comparison
     */
    public boolean hasErrorsDuringComparison();

    /**
     * @return list of header deltas
     */
    public List getHeaderDeltas();

    /**
     * 
     * @return list of instance delta objects
     */
    public List getInstanceDeltas();

    /**
     * A <code>HashMap</code> with the value deltas sorted by config object key.
     * The <code>HashMap</code> contains <code>List</code> objects that contains the 
     * <code>ValueDelta</code> objects.
     * @return a <code>HashMap</code> with the value deltas sorted by config object key.
     */
    public HashMap getValueDeltasByConfigObjectKey();

    /**
     * A <code>HashMap</code> with the instance deltas sorted by instId2.
     * The <code>HashMap</code> contains <code>InstanceDelta</code> objects.
     * @return a <code>HashMap</code> with the instance deltas sorted by instId2.
     */
    public HashMap getInstanceDeltasById2();

    /**
     * @return external configuration one that has been compared to external configuration two
     */
    public ext_configuration getConfigOne();

    /**
     * @return external configuration two that has been compared to external configuration one
     */
    public ext_configuration getConfigTwo();

    /**
     * If the list contains at least one entry the flag <code>errorsDuringComparison</code> 
     * is set to true.
     * @param list errors that occured during comparison
     */
    public void setComparisonErrors(List list);

}
