package com.sap.spc.remote.client.object;

/**
 * Title:
 * Description:
 * Copyright:    Copyright (c) 2000
 * Company:
 * @author
 * @version 1.0
 */

import java.util.List;
import java.util.Locale;

import org.apache.struts.util.MessageResources;

public interface DescriptiveConflict extends Closeable {
    public ExplanationTool getExplanationTool();
    public String getInstanceId();
    public String getCharacteristicName();
    public String getCharacteristicLangDepName();
    public String getLongText(
        boolean showConflictSolutionRate,
        Locale locale,
        MessageResources resources,
        String conflictExplanationTextLineTag,
        boolean useConflictHandlingShortcuts,
        boolean useShortcutValueConflict,
        boolean useExplanationTool);
    public List getConflictTypes(
        boolean showConflictSolutionRate,
        Locale locale,
        MessageResources resources,
        String conflictExplanationTextLineTag,
        boolean useConflictHandlingShortcuts,
        boolean useShortcutValueConflict,
        boolean useExplanationTool);
    public boolean hasShortcut();
    public void setShortcut(ConflictHandlingShortcut shortcut);
    public ConflictHandlingShortcut getShortcut();
}