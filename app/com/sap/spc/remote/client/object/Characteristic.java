/************************************************************************

    Copyright (c) 2001 by SAPMarkets Europe GmbH, Germany, SAP AG

    All rights to both implementation and design are reserved.

    Use and copying of this software and preparation of derivative works
    based upon this software are not permitted.

    Distribution of this software is restricted. SAP does not make any
    warranty about the software, its performance or its conformity to any
    specification.

**************************************************************************/

package com.sap.spc.remote.client.object;

import java.util.List;

/**
 * Client view of a characteristic. The methods in this class mirror
 * these of com.sap.sce.front.base.Cstic (as far as possible with
 * the data the IPC commands provide). The documentation assumes
 * familarity with the front.base API.<p>
 */
public interface Characteristic extends Closeable
{

    //
    // Cstic value type
    // ----------------
    /** Unknown or unset value type */
    public final int  TYPE_UNDEFINED = -1;
    /** Value type for String */
    public final int  TYPE_STRING    = 0;
    /** Value type for integer */
    public final int  TYPE_INTEGER   = 1;
    /** Value type for float */
    public final int  TYPE_FLOAT     = 2;
    /** Value type for boolean */
    public final int  TYPE_BOOLEAN   = 3;
    /** Value type for date */
    public final int  TYPE_DATE      = 4;
    /** Value type for time */
    public final int  TYPE_TIME      = 5;
    /** Value type for currency */
    public final int  TYPE_CURRENCY  = 6;
    /** Value type for object class (materials) */
    public final int  TYPE_CLASS     = 7;


   /**
    * @return the instance this characteristic belongs to
    */
    public Instance getInstance();

   /**
    * @return the characteristic group this characteristic belongs to
    */
    public CharacteristicGroup getGroup();

   /**
    * Assigns the characteristic to a characteristic group
    * @param group This is the characteristic group
    */
    public void setGroup(CharacteristicGroup group);


//   /**
//    * @return This is the name of the characteristic group
//    * this characteristic belongs to
//    * @deprecated Use getGroup().getName()
//    */
//    public String getGroupName();
//
//   /**
//    * Assigns the name of the characteristic group
//    * this characteristic belongs to
//    * @param groupName This is the name of the characteristic group
//    * @deprecated Use setGroup(CharacteristicGroup group)
//    */
//    public void setGroupName(String groupName);
   
    /**
     * Adds a new value to the set of values of the characteristic.
     * Empty values would not be set.
     * Make sure that the value that you want to add is not already in the list of values.
     * You can do this by checking before with the getValue(internalValueName) method.
     * If getValue() returns null the value is not in the list, then you can add it.
     *
     * @param value The value that you want to add.
     */
    public void addValue(String value);

    /**
     * Sets the characteristic's values
     * @param valueNames This is a list of value names
     * @return true, if a value has been changed
     */
    boolean setValues(String[] valueLNames);

    /**
     * Returns the characteristic values.
     * Depending on the request parameter assignableValuesOnly ("T"/"F")
     * you will get either the restricted domain ("T")
     * or all possible values ("F") of a characteristic.
     * If no characteristic values are available you will get
     * an empty list.
     * @return the list of the characteristic values
     */
     List getValues();

     /**
     * Returns the value with the given internal name that you pass.
     * Remark: this checks only for internal names not for language depenent ones.
     * @param valueName (this is the internal name not the language dependent one)
     * @return characteristic value object with the given internal name.
     */
     CharacteristicValue getValue(String valueName);
     
     /**
     * Returns the value with the given language-dependent name that you pass.
     * Remark: this checks only for language dependent names not for the independent ones.
     * @param valueName 
     * @return characteristic value object with the given language-dependent name.
     */
    CharacteristicValue getValueFromLanguageDependentName(String valueName);

    /**
     * @return the characteristic values that have been assigned
     * If no characterist values have been assigned you will
     * get an empty list
     */
     List getAssignedValues();
     
     /**
     * @return The characteristic values that can be assigned to
     * this characteristic.
     */
    List getAssignableValues();

    /**
     * Clears all values which were assigned by the user
     */
     void clearValues();

    /**
     * a list of mime objects (images, sounds, etc.) attached to the characteristic.
     * @return MimeObjectContainer containing the mime-objects for this characteristic
     */
    MimeObjectContainer getMimeObjects();

    /**
     * @return the language dependent name of the characteristic.
     * The default language is EN
     */
    String getLanguageDependentName();

    /**
     * @return the name of the characteristic
     */
    String getName();

    /**
     * @return the measurement unit of the characteristic if
     * one was assigned
     */
     String getUnit();

    /**
     * @return the language dependent measurement unit of the characteristic. 
     * If no language dependent unit can be found it returns the internal unit (same as getUnit())
     */
     String getLanguageDependentUnit();

    /**
     * @return the entry field mask for the characteristic if
     * one was assigned
     */
     String getEntryFieldMask();

    /**
     * @return the length (in characters) a value of this characteristic may not
     * exceed.
     * Please note that in case of numeric values, particularly floating point
     * values, the length only counts digits, not separators or leading "-".
     */
     int getTypeLength();

    /**
     * @return the numeric scale of this cstic or null if it doesn't hold
     * floating-point values.
     * The scale is the number of digits behind the ".".
     */
     int getNumberScale();

    /**
     * @return the basic type of the characteristic value as a number as declared
     * in com.sap.sce.front.base.SceConstants
     */
     int getValueType();

    /**
     * @return true if additional values are allowed;
     * false otherwise
     */
    boolean allowsAdditionalValues();

    /**
     * @return true if the characteristic is visible;
     * false otherwise
     */
    boolean isVisible();

    /**
     * set if characteristic will show or not.
     * @param visible true/false
     */
    void setVisible(boolean visible);

    /**
     * @return true if you can assign multiple values;
     * false otherwise
     */
    boolean allowsMultipleValues();

    /**
     * @returns true if the characteristic is pricing relevant;
     * false otherwise
     */
    boolean isPricingRelevant();

    /**
     * @return true if the characteristic must be evaluated;
     * false otherwise
     */
    boolean isRequired();

    /**
     * @return true if the characteristic is consistent;
     * false otherwise
     */
    boolean isConsistent();

    /**
     * Returns true only if the domain is constrained.
     * A domain is constrained if it has a fixed set of possible values
     * for its Characteristic. Only constrained domains will return a non empty
     * array when calling getStaticDomain().
     *
     * @see #getStaticDomain
     */
    public boolean isDomainConstrained();

    /**
     * @return true if the domain of the characteristic may contain an interval (1 - 10).
     * Since we do not have the knowledge about the single values, the method exists at the characteristic.
     * If the engine messages will deliver the more detailed information about values, we may deprecate this method.
     */
    boolean hasIntervalAsDomain();

    /**
     * @return true if the characteristic can only be read
     * but cannot be changed; false otherwise
     */
    boolean isReadOnly();

    /**
     * set if characteristic will readonly or writeable
     * @param readonly true/false
     */
    void setReadOnly(boolean readonly);

    /**
     * @return true if in the user interface the characteristic
     * has been selected; false otherwise
     */
     public boolean isSelected();

     /**
      * marks whether the characteristic was selected in the user interface
      * @param selected true/false
      */
     public void setSelected(boolean selected);

     /**
      * @return true if the characteristic has a least one value
      * assigned; false otherwise
      */
     public boolean hasAssignedValues();

     /**
      * @return true if the characteristic has a least one value
      * assigned by the user; false otherwise
      */
     public boolean hasAssignedValuesByUser();
    // to be continued

    /**
     * @return the description of a chartacteristic
     */
    public String getDescription();

    /**
     * Call this method to send all changes of this characteristic and all
     * subobjects to the server.
     */
    void flush();

    /**
     * Returns the application views maintained for this characteristic.
     * 
     * @return String representing application views.
     */
    public String getApplicationViews();
    
    /**
     * Returns true if the characteristic is part of the given application view.
     * 
     * @param applicationView application 
     * @return true if the characteristic is part of the given application view.
     */
    public boolean isPartOfApplicationView(char applicationView);
    
    /**
     * 
     * @return true if the characteristic is to be displayed in "expanded" mode,
     * that is with the allowed values variable.
     */
    public boolean isExpanded();

    /**
     * Assign a value object to the characteristic including the following actions:
     * - Set the value to "assigned" and "assignedByUser".
     * - Add it to the assignedValuesVec.
     * - Add it to the assignedValuesByUserVec.
     * Remark: 
     * 1. Check whether the value existing at the characteristic before.
     * 2. If the value is not a CharacteristicValue object use addValue(String valueName) method instead. 
     * This method then will create a CharacteristicValue object and assign it. 
     * @param value to be assigned
     * @return true if assignement was successful
     */
    public boolean assignValue(CharacteristicValue value);

	public int getCharacteristicGroupPosition();

	public int getCharacteristicInstancePosition();
    
}
