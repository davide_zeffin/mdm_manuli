/************************************************************************

	Copyright (c) 2001 by SAPMarkets Europe GmbH, Germany, SAP AG

	All rights to both implementation and design are reserved.

	Use and copying of this software and preparation of derivative works
	based upon this software are not permitted.

	Distribution of this software is restricted. SAP does not make any
	warranty about the software, its performance or its conformity to any
	specification.

**************************************************************************/

package com.sap.spc.remote.client.object;

import java.util.List;

/**
 * Client view of an instance. The methods in this class mirror
 * these of com.sap.sce.front.base.Instance (as far as possible with
 * the data the IPC commands provide). The documentation assumes
 * familarity with the front.base API.<p>
 */
public interface Instance extends Closeable
{

	public IPCClient getIpcClient();
	public IPCDocument getIpcDocument();
	public IPCItem getIpcItem();
 //   public void copyObject(Instance object);

	/**
	 * Returns the configuration to which this instance belongs.
	 */
	public Configuration getConfiguration();

	/**
	 * Returns the instance id of this instance.
	 */
	public String getId();

	/**
	 * Returns the children of this instance in the instance tree,
	 * or an empty array if this instance doesn't have children. Never
	 * returns null.
	 */
	public List getChildren();

	/**
	 * Adds an instance as child instance.
	 */
	 public void addChild(Instance child);

	/**
	 * Returns the parent instance of this instance, or null, if this
	 * instance is the root or a non-part instance.
	 */
	public Instance getParent();
	public String getParentId();

	/**
	 * Returns true if the instance is the rootInstance
	 * returns false otherwise
	 */
	 public boolean isRootInstance();


	public void setMimeObjects(MimeObjectContainer mimeObjects);

	/**
	 * Returns a list of mime objects
	 */
	public MimeObjectContainer getMimeObjects();

	public void setComplete(boolean complete);
	/**
	 * Returns true if this instance is complete.
	 */
	public boolean isComplete();

	public void setConsistent(boolean consistent);
	/**
	 * Returns true if this instance is consistent.
	 */
	public boolean isConsistent();

	public void setSpecializable(boolean isSpecializable);
	/**
	 * Returns true if this instance can be specialized (further)
	 */
	public boolean isSpecializable();

	public void setUnspecializable(boolean isUnspecializable);
	/**
	 * Returns true if this instance can be unspecialized
	 */
	public boolean isUnspecializable();

	public void setQuantity(String quantity);
	/**
	 * Returns the instance quantity of this instance (relative to
	 * its parent).
	 */
	public String getQuantity();

	public void setBOMPosition(String position);
	/**
	 * Returns the position of this instance in the Bill of Materials of
	 * its parent, or null, if this is the root instance or a non-part
	 * instance.
	 */
	public String getBOMPosition();

	/**
	 * Returns the characteristics of this instance (please cf. the
	 * documentation of com.sap.sce.front.base for the difference
	 * between this kind of characteristics and SCE engine characteristics).
	 */
	public List getCharacteristics();

	/**
	 * Returns the characteristics of this instance.If includeInvisible is
	 * true, the result set will also contain invisible characteristics. This
	 * is the same result set as method getCharacteristics() returns. If set to
	 * false, the visible characteristics are returned only, which might be
	 * an effort according to the perfromance - depending on the amount of
	 * invisible characteristics.
	 */
	public List getCharacteristics(boolean includeInvisible);

    /**
     * Returns the characteristics of this instance that belong to the given
     * application view. If includeInvisible is true, the result set will also
     * contain invisible characteristics. 
     */    
    public List getCharacteristics(boolean includeInvisible, char view);

	/**
	 * Returns the characteristics of this instance. If includeInvisible is
	 * true, the result set will also contain invisible characteristics. If 
	 * set to false, the visible characteristics are returned only.
	 * If assignableValuesOnly is true, the result set will only contain the 
	 * assignable values of each characteristic. If assignableValuesOnly is set
	 * to false all values of each characteristic will be returned. If the method
	 * getCharacteristics() is used invisible characteristics and all values of 
	 * each characteristic will be returned. 
	 * Setting includeInvisible and assignableValuesOnly to true might be an
	 * effort according to the perfromance - depending on the amount of invisible
	 * characteristics and not assignable values.
	 */
	public List getCharacteristics(boolean includeInvisible, boolean assignableValuesOnly);

	/**
	 * Returns a single characteristic - identified by the language
	 * independent characteristic name
	 */
	public Characteristic getCharacteristic(String csticName, boolean includeInvisible);

	/**
	 * Returns all characteristic groups (design groups) of this instance.
	 * If no design groups are available, an array with a single dummy group
	 * with null name will be returned.
	 */
	public List getCharacteristicGroups();

	/**
	 * Returns all characteristic groups (design groups) of this instance.
	 * If includeInvisible is set to true, the groups will contain invisible
	 * cstics as well!
	 */
	public List getCharacteristicGroups(boolean includeInvisible, boolean showGeneralTabFirst);

	/**
	 * Returns the characteristic group, specified by the groupName
	 */
	public CharacteristicGroup getCharacteristicGroup(String groupName);

	public void setIsUserOwned(boolean userOwned);
	/**
	 * Returns true, if the author of this instance is the user (which
	 * is the case if it has been created by createInstance).
	 */
	public boolean isUserOwned();

	public void setPrice(String price);
	/**
	 * Returns the price of this instance.
	 */
	public String getPrice();

	/**
	 * Specializes this instance to another type. Throws a IllegalConfigOperationException
	 * if this Instance cannot be specialized to targetType.
	 */
	public void specialize(String specTypeId);// throws IllegalConfigOperationException,IPCException;

	/**
	 * Unspecializes this instance. Calling this method will undo the last call of
	 * specialize on this instance. If the instance has never been specialized,
	 * a IllegalConfigOperationException will be thrown.
	 */
	public void unspecialize();// throws IllegalConfigOperationException, IPCException;

	/**
	 * Creates a new child instance of this instance at a given BOM position,
	 * or throws an IllegalConfigOperationException if the instance could not
	 * be created.<p>
	 *
	 * Please note that due to a lacking command in the IPC command set there is
	 * currently no way to remove an instance that has been created before.
	 */
	public void createChildInstance(String decompPosition);// throws IllegalConfigOperationException, IPCException;

	/**
	 * Returns true, if one or more childs are present.
	 */
	public boolean hasChildren();

	public void setName(String Name);

	/**
	 * Returns the language independent name of the instance.
	 */
	public String getName();

	public void setLanguageDependendName(String languageDependendName);

	/**
	 * Returns the language dependent name of the instance.
	 */
	public String getLanguageDependentName();

	public void setDescription(String description);

	/**
	 * Returns the long description of the instance.
	 */
	public String getDescription();

	/**
	 * Returns true if the instance is configurable
	 * returns false otherwise
	 */
	public boolean isConfigurable();

	/**
	 * Returns product variants of the root instance.
	 * The 5.0 release does not support fuzzy search, but incomplete search only.
	 * Therefore the parameter
	 * @param String maxMatchPoints
	 * @param String quality
	 * @param String maxMatchPoints
	 * have no affect at the result of the search.
	 */
	public List getProductVariants(String limit, String quality, String maxMatchPoints);

	/**
     * @param productVariantId
     * @return false, if productVariantId does not identify the product of the configurations
     * root instance
     * Throws an IPCException if the command that replaces the product of the configuration with the
     * product identified with productVariantId throws an Exception.
     */
    public boolean applyProductVariant(String productVariantId);

    /**
     * Call this method to send all changes of this instance and all subobjects
     * to the server.
     */
    void flush();
	 
	/**
	 * Resets the groupCache of the Instance.
	 */
    public void resetGroupCache();
	 
}
