package com.sap.spc.remote.client.object;

import java.util.List;

public interface KnowledgeBase extends Closeable
{
	
	/**
	 * Returns a reference to the associated IPCClient.
	 * This reference is needed for communication with the IPC server.
	 */
	IPCClient getIPCClient();

	/**
	 * Returns the logical system of the knowledge base.
	 */
	String getLogsys();
  /**
   * returns the name of the knowledgebase
   */
  String getName();

  /**
   * returns the version of the knowledgebase
   */
  String getVersion();

   /**
    * returns the profile of the knowledgebase
    */
  List getProfiles();
  
  /**
   * Creates and adds a knowledge base profile to the knowledge base.
   * @return The created knowledge base profile.
   */
  KBProfile createProfile(String profilename);

   /**
   * returns the build number of this knowledgebase
   */
  String getBuildNumber();
  
}