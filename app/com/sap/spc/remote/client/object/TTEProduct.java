package com.sap.spc.remote.client.object;

import java.util.ArrayList;
import java.util.HashMap;


public interface TTEProduct {


    /**
     * @return product id
     */
    public String getProductId(); 


    /**
     * @return taxability of product
     */
    public String getTaxability();

    /**
     * @return list of tax classifications
     */
    public ArrayList getTaxClassifications();
    
    /**
     * @param country
     * @param region
     * @param taxType
     * @return HashMap with tax classifications by key (country, region, taxType)
     */
    public HashMap getTaxClassificationsByCountry(String country, String region, String taxType);

    /**
     * @param country
     * @param region
     * @param taxType
     * @return true if the product contains the tax classification with the given key
     */
    public boolean containsTaxClassification(String country, String region, String taxType);


}
