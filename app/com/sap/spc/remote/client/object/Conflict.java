package com.sap.spc.remote.client.object;

/**
 * Title:
 * Description:
 * Copyright:    Copyright (c) 2000
 * Company:
 * @author
 * @version 1.0
 */

import java.util.List;

/**
 * Helper class for conflict handling in the ui.
 * @author mk
 *
 */
public interface Conflict extends Closeable{

  /**
   * 
   * @return ExplanationTool Proxy for conflict explanation on server side 
   */
  public ExplanationTool getExplanationTool();
  
  public List getParticipants();
  
  /**
   * Add a reason for a conflict.
   * @param participant
   */
  public void addParticipant(ConflictParticipant participant);
  
  /**
   * Returns the reason (participant) for a given key.
   * @param key a concatenation of GetFacts.FACT_KEY_PLACEHOLDER
   * + GetFacts.FACT_KEY_DELIMITER
   * + instId
   * + GetFacts.FACT_KEY_DELIMITER
   * + observType
   * + GetFacts.FACT_KEY_DELIMITER
   * + observable
   * + GetFacts.FACT_KEY_DELIMITER
   * + binding
   * @return The reason (participant) of the conflict.
   */
  public ConflictParticipant getParticipantByKey(String key);
  
  /**
   * ??? initialized with GetConflictingAssumptions.ASSUMPTION_GROUP_ID in the constructor
   * @return an id for this conflict
   */
  public String getId();
  
  /**
   * @param participantId
   * @return The reason for this conflict for a given id
   */
  public ConflictParticipant getParticipant(String participantId);
  
  /**
   * @return the number of reasons for this conflict
   */
  public int countParticipants();

}