package com.sap.spc.remote.client.object;

import java.util.List;


/**
 * Loading status of the configuration.
 * Provides information on the loading process of the configuration.
 */
public interface LoadingStatus {
    
    public static final int UNKNOWN    = 0; // status is unknown, i.e. it has not been retrieved from the engine
    public static final int OK         = 1; // no errors or warning messages during loading
    public static final int INFO       = 2; // ok, but with info messages
    public static final int WARNING    = 3; // ok, but with warning messages
    public static final int ERROR      = 9; // severe errors during loading of configuration

    /**
     * @return status of loading process.
     */
    public int getStatus();
    
    /**
     * @return true if a new KB has been used for loading.
     */
    public boolean hasNewKB();
    
    /**
     * @return true if messages occurred during the loading of the configuration.
     */
    public boolean hasLoadingMessages();
    
    /**
     * Returns the list of loading messages. If no messages have been created during the 
     * loading process this methods returns an empy list.
     * @return list of loading messages 
     */
    public List getLoadingMessages();
    
    /**
     * @return true if the engine has been called with an initial configuration
     */
    public boolean loadedFromInitialConfiguration();

    /**
     * Set the loadedFromInitialConfiguration flag.<br>
     * Usually the flag should be set during initialization process of this class.
     * Exception if the the initial configuration is set using setInitialConfiguration() method
     * of IPCItem the flag is set manually by this method.
     * @param flag whether the configuration has an initial configuration
     */    
    public void setLoadedFromInitialConfiguration(boolean b);    
    
}
