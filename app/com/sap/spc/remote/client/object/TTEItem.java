package com.sap.spc.remote.client.object;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Client view of a TTE item.
 */
public interface TTEItem {

   
    /**
     * @return business process
     */
    public String getBusinessProcess();

    /**
     * @return incoterms 1
     */
    public String getIncoterms1();

    /**
     * @return incoterms 2
     */
    public String getIncoterms2();

    /**
     * @return product id
     */
    public String getProductId(); 


    /**
     * @return list of business partners of this item
     */
    public ArrayList getItemBusinessPartners();

    /**
     * @return item id
     */
    public String getItemId();

    /**
     * @return reference item id
     */
    public String getRetItemId();

    /**
     * @return position number
     */
    public String getPositionNumber();

    /**
     * @return item quantity
     */
    public String getQuantity();

    /**
     * @return the TTEDocument the item belongs to
     */
    public TTEDocument getTteDocument();

    /**
     * @return unit of measurement
     */
    public String getUnit();

    /**
     * @return direction idicator
     */
    public String getDirectionIndicator();

    /**
     * Uses the given document-header attributes to change the business partners of this item.
     * It changes a BP only if appropriate data is part of the document-header-attributes. 
     * It changes only exisiting BPs. It does not create BPs.
     * If the BP for a role (e.g. "Ship-from") is not existing it is NOT created.
     * @param docHeaderAttributes document-header attributes
     */
    public void changeBusinessPartners(HashMap docHeaderAttributes);

}
