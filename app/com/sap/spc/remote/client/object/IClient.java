/*
 * Created on Mar 15, 2005
 *
 * To change the template for this generated file go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
package com.sap.spc.remote.client.object;

import com.sap.spc.remote.client.ClientLogListener;

/**
 * @author I026584
 *
 * To change the template for this generated type comment go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
public interface IClient {
	public IPCItemReference getItemReference();
	public IPCConfigReference getConfigReference();
	public void addLogListener(ClientLogListener l);
	public void removeLogListener(ClientLogListener l);
	
}
