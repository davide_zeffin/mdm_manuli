package com.sap.spc.remote.client.object;

import java.io.PrintStream;
import java.io.PrintWriter;
import java.util.ArrayList;

import com.sap.spc.remote.client.ClientException;

/**
 * All exceptions thrown in this API will be subclassed from this exception.
 * Technically it can be constructed from a ClientException (which it hides).
 */
public class IPCException extends RuntimeException
{

	protected Exception _sub;
	protected String key;
	protected ArrayList placeHolders;


    public static final String IPCCLIENTEX1 = "ipc.client.exception.1"; //unknownCsticValueType cannot be converted to an int
    public static final String IPCCLIENTEX2 = "ipc.client.exception.2"; //unknownCsticTypeLength cannot be converted to an int
    public static final String IPCCLIENTEX3 = "ipc.client.excetpion.3"; //unknownCsticNumberScale cannot be converted to an int
    public static final String UNIMPLEMENTED = "Unimplemented by design";

	public IPCException(Exception sub) {
		super("IPC Exception: "+sub.getMessage());
		_sub = sub;
		if(sub instanceof ClientException){
			key = ((ClientException)sub).getKey();
			placeHolders = ((ClientException)sub).getPlaceHolders();
		}
	}


	public IPCException(String message) {
		super(message);
		_sub = null;
	}

	public IPCException(String key, ArrayList placeHolders, Exception e){
		this.key = key;
		this.placeHolders = placeHolders;
		this._sub = e;
	}

	/**
	 * Prints the stack trace of this exception. If it was constructed
	 * from another exception e, e's stack trace is printed instead
	 * (which is usually what we're interested in).
	 */
	public void printStackTrace() {
		if (_sub != null){
			if(_sub instanceof ClientException){
				Exception exp = ((ClientException)_sub).getOriginalException();
				if(exp != null){
					exp.printStackTrace();
				}else{
					((ClientException)_sub).printStackTrace();
				}
			}else{
				_sub.printStackTrace();
			}
		}
		else
			super.printStackTrace();
	}

	/**
	 * Prints the stack trace of this exception. If it was constructed
	 * from another exception e, e's stack trace is printed instead
	 * (which is usually what we're interested in).
	 */
	public void printStackTrace(PrintStream s) {
		if (_sub != null){
			if(_sub instanceof ClientException){
				Exception exp = ((ClientException)_sub).getOriginalException();
				if(exp != null){
					exp.printStackTrace(s);
				}else{
					((ClientException)_sub).printStackTrace(s);
				}
			}else{
				_sub.printStackTrace(s);
			}
		}
		else
			super.printStackTrace(s);
	}

	/**
	 * Prints the stack trace of this exception. If it was constructed
	 * from another exception e, e's stack trace is printed instead
	 */
	public void printStackTrace(PrintWriter s) {
		if (_sub != null){
			if(_sub instanceof ClientException){
				Exception exp = ((ClientException)_sub).getOriginalException();
				if(exp != null){
					exp.printStackTrace(s);
				}else{
					((ClientException)_sub).printStackTrace(s);
				}
			}else{
				_sub.printStackTrace(s);
			}
		}
		else
			super.printStackTrace(s);
	}

	/**
	 * Fills in the stack trace of this exception. If it was constructed
	 * from another exception e, e's stack trace is used instead
	 */
	public Throwable fillInStackTrace() {
		if (_sub != null){
			if(_sub instanceof ClientException){
				Exception exp = ((ClientException)_sub).getOriginalException();
				if(exp != null){
					return exp.fillInStackTrace();
				}else{
					return ((ClientException)_sub).fillInStackTrace();
				}
			}else{
				return _sub.fillInStackTrace();
			}
		}
		else
			return super.fillInStackTrace();
	}

	/**
	 * Returns the server's error code.<p>
	 *
	 * @see com.sap.msasrv.socket.shared.ErrorCodes
	 */
	public String getCode() {
		if (_sub != null && _sub instanceof ClientException) {
			return ((ClientException)_sub).getCode();
		}
		else {
			return com.sap.msasrv.socket.shared.ErrorCodes.RET_INTERNAL_FATAL;
		}
	}

	public String getKey(){
		if (_sub != null && _sub instanceof ClientException) {
			return ((ClientException)_sub).getKey();
		}
		else {
			return key;
		}
	}

	public ArrayList getPlaceHolders(){
		if (_sub != null && _sub instanceof ClientException) {
			return ((ClientException)_sub).getPlaceHolders();
		}
		else {
			return placeHolders;
		}
	}

	/**
	 * Returns the Server's error number.
	 * @see	com.sap.sxe.socket.ErrorConstants
	 */
	public int getErrorNumber() {
		if (_sub != null && _sub instanceof ClientException) {
			return ((ClientException)_sub).getErrorNumber();
		}
		else {
			return com.sap.msasrv.socket.shared.ErrorConstants.UNKNOWN_ERROR;
		}
	}


	/**
	 * Returns the server's message.<p>
	 *
	 * In error situations the server will return a response with a parameter
	 * SPC_ERROR_MESSAGE. The value of this parameter is retrieved with this method.
	 *
	 * @see com.sap.sxe.socket.shared.ServerConstants
	 */
	public String getMessage() {
		if (_sub != null) {
			if(_sub instanceof ClientException){
				return ((ClientException)_sub).getMessage();
			}else{
				return _sub.getMessage();
			}
		}
		else {
			return super.getMessage();
		}
	}

	/**
	 * Returns true if this Exception is fatal, ie if the client should
	 * terminate its session.<p>
	 *
	 * Fatal errors are those explicitely marked
	 * fatal by the server as well as references to unknown or "timeouted"
	 * session ids.<p>
	 *
	 * Recoverable errors are those marked "error" by the server and references
	 * to unknown server commands.<p>
	 */
	public boolean isRecoverable() {
		String code = getCode();
		if (code.equals(com.sap.msasrv.socket.shared.ErrorCodes.RET_INTERNAL_ERROR))
			return true;
		else if (code.equals(com.sap.msasrv.socket.shared.ErrorCodes.RET_INTERNAL_FATAL))
			return false;
		else if (code.equals(com.sap.msasrv.socket.shared.ErrorCodes.RET_OK))
			return true;
		else if (code.equals(com.sap.msasrv.socket.shared.ErrorCodes.RET_TIMEDOUT_ID))
			return false;
		else if (code.equals(com.sap.msasrv.socket.shared.ErrorCodes.RET_UNKNOWN_CMD))
			return true;
		else if (code.equals(com.sap.msasrv.socket.shared.ErrorCodes.RET_UNKNOWN_ID))
			return false;
		else
			// default: not recoverable
			return false;
	}
}
