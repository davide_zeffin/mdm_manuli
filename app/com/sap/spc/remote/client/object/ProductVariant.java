package com.sap.spc.remote.client.object;

/**
 * Client view of a product variant
 */

import java.util.List;
import java.util.Map;

public interface ProductVariant extends Closeable{

/**
 * Set-Get-Methods for the product id of the variant
 */
 public void setId(String id);
 public String getId();

 /**
  * Set-Get Methods for the configuration attached to the product variant.
  */
 public void setConfiguration(Configuration configuration);
 public Configuration getConfiguration();

/**
 * Set-Get-Methods for the distance of the variant (0..infinity)
 */
 public void setDistance(double distance);
 public double getDistance();

/**
 * Set-Get-Methods for the match point:
 * how good each match is (0..maxMatchPoint, derived from distance).
 */
 public void setMatchPoints(String matchPoints);
 public String getMatchPoints();


 /**
  * Returns the scale end
  */
 public String getMaxMatchPoints();

 /**
  * Set-Get-Methods for the features of the product variant
  */
  public void setFeatures(List features);
  public List getFeatures();

  /**
   * Set-Get-Methods for the main difference
   */
   public void setMainDifference(ProductVariantFeature mainDifference);
   public ProductVariantFeature getMainDifference();

  /**
   * special methods for a single feature
   */
   public void addFeature(ProductVariantFeature feature);
   public ProductVariantFeature getFeature(String featureId);

    /**
     * Returns true if the productVariant was selected in the UI;
     * returns false otherwise
     */
     public boolean isSelected();
     public void setSelected(boolean selected);
	 
	 /**
	  * Associates a product variant with its item.
	  */
	  public void setItem(IPCItem ipcItem);
	  
	  /**
	   * Returns the associated Item for this product variant.
	   * Returns null if no item is associated.
	   */
	   public IPCItem getItem();
    
    /**
     * Set the product type of the variant.
     * @param string roduct type 
     */
    public void setProductType(String string);
    
    /** Set the logical system of the product.
     * @param string logsys
     */
    public void setProductLogSys(String string);
    
    /**
     * Retrieves the item attributes from product master.
     * @param application The application we are running in (e.g. CRM)
     * @return Map with the attributes.
     */
    public Map getAttributesFromProductMaster(String application);

}