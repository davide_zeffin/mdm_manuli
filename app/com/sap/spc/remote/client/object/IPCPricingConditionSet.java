/************************************************************************

	Copyright (c) 2001 by SAP AG

	All rights to both implementation and design are reserved.

	Use and copying of this software and preparation of derivative works
	based upon this software are not permitted.

	Distribution of this software is restricted. SAP does not make any
	warranty about the software, its performance or its conformity to any
	specification.

**************************************************************************/
package com.sap.spc.remote.client.object;

import java.util.HashMap;

public interface IPCPricingConditionSet /*extends Closeable*/ {

	/**
	 * @return all pricing conditions in this set as an array of IPCPricingConditions
	 * instances.
	 */
	public IPCPricingCondition[] getPricingConditions() throws IPCException;

	/**
	 * @return all pricing conditions in this set as an HashMap with
	 * BAPI conformed parameter names
	 */
	public HashMap getExternalPricingConditions() throws IPCException;

	/**
	 * Adds a new pricing condition to the set
	 */
	public void addPricingCondition(String conditionTypeName,
									String      conditionRate,
									String      conditionCurrency,
									String      conditionPricingUnitValue,
									String      conditionPricingUnitUnit,
									String      conditionValue,
									String      decimalSeparator,
									String      groupingSeparator);

	/**
	 * Removes a pricing condition from the set
	 */
	public void removePricingCondition(String stepNo, String counter);

	/**
	 * Commits changes made on this set.
	 * @return a set of newly created step identifiers.
	 */
	public StepId[] commit() throws IPCException;

	/**
	 * @return messages occurred since last call
	 */
	public IPCMessage[] getMessages();

	/**
	 * @return all names of condition types available in the current pricing procedure.
	 */
	public ValueDescriptionPairs getAvailableConditionTypeNames() throws IPCException;

	/**
	 * @return all names of physical units available.
	 */
	public ValueDescriptionPairs getAllPhysicalUnits() throws IPCException;

	/**
	 * @return all names of physical units available to a given dimension.
	 */
	public ValueDescriptionPairs getAvailablePhysicalUnits(String DimensionName) throws IPCException;

	/**
	 * @return all names of quantity units available.
	 */
	public ValueDescriptionPairs getAvailableQuantityUnits() throws IPCException;

	/**
	 * @return all names of currencies available.
	 */
	public ValueDescriptionPairs getAllCurrencyUnits() throws IPCException;


}