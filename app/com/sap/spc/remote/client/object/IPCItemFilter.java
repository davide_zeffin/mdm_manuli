package com.sap.spc.remote.client.object;

/**
 * Title:
 * Description:  Enables the client of the object layer to implement a filter
 *      object that reduces the set of Items returned by a method.
 * Copyright:    Copyright (c) 2000
 * Company:
 * @author
 * @version 1.0
 */

public interface IPCItemFilter {
	public IPCItem[] filterItems(IPCItem[] input);
}