package com.sap.spc.remote.client.object;

/**
 * Represents a message that occured during the loading process of a configuration.
 */
public interface LoadingMessage {

    static final int ERROR      = 0;
    static final int WARNING    = 1;
    static final int INFO       = 2;  

    /**
     * Returns the configuration object key this message belongs to. This is the concatenated
     * instId and csticName ("<instId>.<csticName>").
     * If this message belongs to an instance the configuration object key is just <instId> 
     * (no separator dot). 
     * @return The configuration object key this message belongs to.
     */
    public String getConfigObjKey();
    
    /**
     * @return Instance id associated with this message.
     */
    public String getInstId();
    
    /**
     * @return Characteristic name associated with this message.
     */
    public String getCsticName();
    
    /**
     * @return Message number.
     */
    public int getNumber();
    
    /**
     * @return Return class of message.
     */
    public String getMessageClass();
    
    /**
     * @return Severity of message.
     */
    public int getSeverity();
    
    public boolean isErrorMessage();
    
    public boolean isWarningMessage();
    
    public boolean isInfoMessage();
    
    /**
     * @return Language dependent message text.
     */
    public String getText();

    /**
     * @param text of the loading message with variable substitution
     */
    public void setText(String text);
    
    /**
     * @return Array of message arguments.
     */
    public String[] getArguments();
    
 
}
