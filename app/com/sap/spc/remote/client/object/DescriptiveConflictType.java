package com.sap.spc.remote.client.object;
import java.util.Vector;

/**
 * Title:
 * Description:
 * Copyright:    Copyright (c) 2000
 * Company:
 * @author
 * @version 1.0
 */

public interface DescriptiveConflictType extends Closeable{

/**
 *  returns the DescriptiveConflict the DescriptiveConflictType is assigned to
 */
	public DescriptiveConflict getDescriptiveConflict();
/**
 * returns the language independent name of the conflict type
 */
	public String getType();
/**
 *  returns the language dependent name of the conflict type
 */
	public String getName();
/**
 *  returns the language dependent text of the conflict type
 */
	public String getText();
/**
 * returns true if there is a conflict text available
 * returns false otherwise
 */
	public boolean hasText();
/**
 * returns true if there is a shortcut
 */
	public boolean hasShortcut();
/**
 * returns the number of members of the conflict type
 */
	public int getNumberOfMembers();
/**
 * returns the shortcut
 */
	public ConflictHandlingShortcut getShortcut();
/**
 * returns the keys of facts which are part of the conflict type
 */
	public Vector getFactKeys();
/**
 * returns the value selected by the user
 */
	public String getUserSelection();
/**
 * returns true if there exists a value selected by user
 */
    public boolean hasUserSelection();

}
