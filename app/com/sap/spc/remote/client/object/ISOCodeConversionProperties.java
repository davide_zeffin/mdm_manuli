package com.sap.spc.remote.client.object;

/**
 * Properties for iso code conversion. To be used in IPCSession. When you call
 * IPCSession.determineUnitsOfMeasurement, the properties are passed to the IPC server,
 * where some of their values are updated. If inconsistent data are passed in a properties
 * object, you can retrieve an error message from the object via getErrorMessage.
 */
public class ISOCodeConversionProperties {

    protected String isoCode;
    protected String value;
    protected String internalUnit;
    protected String externalUnit;
    protected String errorMessage;
    protected String formattedValue;

	/**
	 * Returns the iso code of this object. This code is always set by the user.
	 */
	public String getIsoCode() {
        return isoCode;
    }

	/**
	 * Sets the iso code of this object.
	 * This code must (!) be set for a successful
	 * call of IPCSession.determineUnitsOfMeasurement, either this way or in the constructor.
	 */
    public void setIsoCode(String newIsoCode) {
        isoCode = newIsoCode;
    }

	/**
	 * Sets the language independent value of this object. This value may be set by the user,
	 * it is not changed by the server. If a value is set and a locale is passed to
	 * IPCSession.determineUnitsOfMeasurement, the corresponding formattedValue is set by the server.
	 */
    public void setValue(String newValue) {
        value = newValue;
    }

	/**
	 * Returns the user-set value of this object.
	 */
    public String getValue() {
        return value;
    }

	/**
	 * Returns the internal unit name. This value is set by the server. Any user-set
	 * value will be overwritten by IPCSession.determineUnitsOfMeasurement.
	 */
    public String getInternalUnit() {
        return internalUnit;
    }

	/**
	 * Returns the external unit name. This value is set by the server. Any user-set
	 * value will be overwritten by IPCSession.determineUnitsOfMeasurement.
	 */
    public String getExternalUnit() {
        return externalUnit;
    }

	/**
	 * Returns an error message in the case that the iso code passed to IPCSession.determineUnitsOfMeasurement
	 * is not ok/ambiguous/... This value is set by the server. Any user-set
	 * value will be overwritten by IPCSession.determineUnitsOfMeasurement.
	 */
    public String getErrorMessage() {
        return errorMessage;
    }

	/**
	 * Returns the formatted version of value. If setValue() was not called before calling
	 * IPCSession.determineUnitsOfMeasurement, this method returns null. This value is set by the server. Any user-set
	 * value will be overwritten by IPCSession.determineUnitsOfMeasurement.
	 */
    public String getFormattedValue() {
        return formattedValue;
    }

	/**
	 * Internal. This method is used by the server to update this object. It should not
	 * be used by callers. If you set the error message, it may change at any time to
	 * reflect the server's state.
	 */
    public void setErrorMessage(String newErrorMessage) {
        errorMessage = newErrorMessage;
    }

	/**
	 * Internal. This method is used by the server to update this object. It should not
	 * be used by callers. If you set the external unit, it may change at any time to
	 * reflect the server's state.
	 */
    public void setExternalUnit(String newExternalUnit) {
        externalUnit = newExternalUnit;
    }

	/**
	 * Internal. This method is used by the server to update this object. It should not
	 * be used by callers. If you set the formatted value, it may change at any time to
	 * reflect the server's state.
	 */
    public void setFormattedValue(String newFormattedValue) {
        formattedValue = newFormattedValue;
    }

	/**
	 * Internal. This method is used by the server to update this object. It should not
	 * be used by callers. If you set the internal unit, it may change at any time to
	 * reflect the server's state.
	 */
    public void setInternalUnit(String newInternalUnit) {
        internalUnit = newInternalUnit;
    }

	/**
	 * Constructs a new ISOCodeConversionProperties object for a given iso code.
	 */
	public ISOCodeConversionProperties(String isoCode) {
		this.isoCode = isoCode;
	}
}
