package com.sap.spc.remote.client.object;

import java.util.HashMap;
import java.util.List;

/**
 * Represents a delta of an instance in the configuration comparison. 
 */
public interface InstanceDelta {

    public static final String DELETED = "D";
    public static final String INSERTED = "I";
    public static final String UPDATED = "U";

    /**
     * @return author of the instance delta
     */
    public String getAuthor();

    /**
     * @return decomposition path
     */
    public String getDecompPath();

    /**
     * @return list of deltas on instance level
     */
    public List getDeltas();

    /**
     * @return id of instance 1
     */
    public String getInstId1();

    /**
     * @return id of instance 2
     */
    public String getInstId2();

    /**
     * Returns the item id if available.
     * @return item id 
     */
    public String getItemId();

    /**
     * @return list of part of deltas
     */
    public List getPartOfDeltas();

    /**
     * Returns the type of the delta. This can be 
     * <ul>
     * <li>D - deleted
     * <li>I - inserted
     * <li>U - updated
     * </ul> 
     * @return type of delta
     */
    public String getType();

    /**
     * @return list of value deltas
     */
    public List getValueDeltas();
    
    /**
     * Returns a <code>HashMap</code> with an <code>ArrayList</code> for each configuration object 
     * that has value deltas.
     * For each configuration object that has value deltas an <code>ArrayList</code> has been added
     * to the  <code>HashMap</code> with the configObjectKey as key (pattern: "<instId>.<csticName>").
     * @return <code>HashMap</code> with an <code>ArrayList</code> for each configuration object that has value deltas
     */
    public HashMap getValueDeltasByConfigObjectKey();

}
