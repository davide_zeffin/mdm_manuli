package com.sap.spc.remote.client.object;

/**
 * Feature of a productVariant
 */

public interface ProductVariantFeature {

/**
 * Set-Get-Methods for the type of the feature:
 * what kind of feature, currently only: CSTIC.
 */
 public void setType(String type);
 public String getType();

 /**
  * Set-Get-Methods for the id:
  * string identifying the feature (eg. "KBCA_A0001.KBCA_TECHNOLOGY").
  */
  public void setId(String Id);
  public String getId();

  /**
   * Set-Get-Methods for the name:
   * language dependent name of the feature (may be null)
   */
   public void setName(String name);
   public String getName();

   /**
    * Returns the language dependent name of the product variant feature.
    */
   public String getLanguageDependentName();

   /**
    * Set-Get-Methods for the longtext:
    * language dependent long text of the feature (may be null)
    */
    public void setLongtext(String longtext);
    public String getLongtext();

    /**
     * Set-Get-Methods for the feature value:
     * the value of the feature for that product variant
     */
     public void setValue(String value);
     public String getValue();

	 public String getLanguageDependentValue();

    /**
     * Set-Get-Methods for the feature weight:
     * doubles holding the feature weights
     */
     public void setWeight(double weight);
     public double getWeight();

    /**
     * Set-Get-Methods for the feature distance:
     * doubles holding the contribution of the feature to the distance.
     */
     public void setDistance(double distance);
     public double getDistance();

}