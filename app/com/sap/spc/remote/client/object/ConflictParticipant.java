package com.sap.spc.remote.client.object;

import java.util.Locale;

import org.apache.struts.util.MessageResources;

/**
 * Title:
 * Description:
 * Copyright:    Copyright (c) 2000
 * Company:
 * @author
 * @version 1.0
 */



public interface ConflictParticipant extends Closeable{

  public String getText(Locale locale, MessageResources resources);
  public String getKey();
  public Conflict getConflict();
  public String getKeyWithId();
  public String getId();
  public boolean isCausedByUser();
  public boolean isCausedByDefault();
  public void setHasTheHighestSolutionRate(boolean value);
  public boolean hasTheHighestSolutionRate();
  public void setIsTheDefaultToDelete(boolean value);
  public boolean isTheDefaultToDelete();
  public boolean isSuggestedForDeletion();
  public int getSolutionRate();
  public void setSolutionRate(int solutionRate);
  public void remove();

}