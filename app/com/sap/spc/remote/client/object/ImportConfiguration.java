/************************************************************************

	Copyright (c) 2001 by SAPMarkets Europe GmbH, Germany, SAP AG

	All rights to both implementation and design are reserved.

	Use and copying of this software and preparation of derivative works
	based upon this software are not permitted.

	Distribution of this software is restricted. SAP does not make any
	warranty about the software, its performance or its conformity to any
	specification.

**************************************************************************/

package com.sap.spc.remote.client.object;

/**
 * Client view of a configuration. The methods in this class mirror
 * these of com.sap.sce.front.base.Config (as far as possible with
 * the data the IPC commands provide). The documentation assumes
 * familarity with the front.base API.<p>
 */
public interface ImportConfiguration extends Configuration
{
	/**
	 * Returns the warning protocol of the creation of the configuration.
     * This functions returns an empty string because function is not 
     * implemented yet
	 */
	public String getWarningProtocol();

    /**
     * set the warning protocol of the creation of the configuration.
     * @param protocol
     */
    public void setWarningProtocol(String protocol);

}
