/************************************************************************

	Copyright (c) 2001 by SAP AG

	All rights to both implementation and design are reserved.

	Use and copying of this software and preparation of derivative works
	based upon this software are not permitted.

	Distribution of this software is restricted. SAP does not make any
	warranty about the software, its performance or its conformity to any
	specification.

**************************************************************************/
package com.sap.spc.remote.client.object;

public class StepId {

	protected String _stepNo;
	protected String _counter;

    public StepId(String stepNo, String counter) {
		_stepNo = stepNo;
		_counter = counter;
    }

	public String getStepNo(){
		return _stepNo;
	}

	public String getCounter(){
		return _counter;
	}

}