/************************************************************************

	Copyright (c) 2001 by SAPMarkets Europe GmbH, Germany, SAP AG

	All rights to both implementation and design are reserved.

	Use and copying of this software and preparation of derivative works
	based upon this software are not permitted.

	Distribution of this software is restricted. SAP does not make any
	warranty about the software, its performance or its conformity to any
	specification.

**************************************************************************/

package com.sap.spc.remote.client.object;

/**
 * An IPCConfigReference knows everything needed to connect to a remote IPC server session,
 * and retrieve an Config from that session. This information is made up from the everything
 * needed to contact an IPC server (host, port, character encoding), and from everything
 * required to identify an Config on a server (session id, config id).
 * @todo Restructure between connection data and config ref data, constructor instead of set methods.
 */
public class IPCConfigReference
{
	protected String configId;
    public void setConfigId(String newConfigId) {
        configId = newConfigId;
    }
    public String getConfigId() {
        return configId;
    }
    
    public static boolean isConfigOnly(String configId) {
    	if (configId.length() == 66 && configId.indexOf('/') != -1) {
    		//a configid that references a document and an item has the following form:
    		//"ADF2345....EAD/443A32F...67EE"
    		return false;
    	} else {
    		//a configid that references a configuration only has the following form:
    		//"S1"
    		return true;
    	}
    }
}
