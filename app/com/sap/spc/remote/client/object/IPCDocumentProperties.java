/************************************************************************

	Copyright (c) 2001 by SAPMarkets Europe GmbH, Germany, SAP AG

	All rights to both implementation and design are reserved.

	Use and copying of this software and preparation of derivative works
	based upon this software are not permitted.

	Distribution of this software is restricted. SAP does not make any
	warranty about the software, its performance or its conformity to any
	specification.

**************************************************************************/

package com.sap.spc.remote.client.object;

import java.util.HashMap;
import java.util.Map;

/**
 * Properties to initialize a document with. Please note that the properties object
 * you pass to a document will be stored inside the document. So you can not reuse
 * a properties object to initialize several documents. Additionally, please note
 * that once you have called newDocument() with a properties object, the server takes
 * over "setter control", ie. any call to set... of this object will not cause
 * any effect. You may use this object for get... access at any time, though (although
 * it is recommended to access the document via the document's methods).
 * <p>
 *
 * As of IPC 5.0, the document properties have been split up into an interface and an
 * implementation. This way many methods in the imp package that needed to be public
 * only because they are used by this class can be made package private; and it
 * is consistent with the other interface/implementation classes.
 * 
 * Note that these methos are to set up the initial properties object but not to change an existing document properties.
 * For changing the existing document properties, use the IPCDocument interface methods but not properties interface methods.
 *  
 **/
public interface IPCDocumentProperties
{
    public void setLocalCurrency(String localCurrency);

    public String getLocalCurrency() throws IPCException;

    public String[] getAllHeaderAttributeValues() throws IPCException;

    public void setDocumentCurrency(String documentCurrency);

    public String getDocumentCurrency() throws IPCException;

    public String[] getAllHeaderAttributeNames();

	public void setAttribute(String attrName,String attrValue);

    public void setDivision(String division);

    public void setDepartureRegion(String departureRegion);

    public String getDivision() throws IPCException;

    public String getDepartureRegion() throws IPCException;

    public void setLanguage(String language);

    public String getLanguage() throws IPCException;

	public boolean isGroupConditionProcessingEnabled();

    public void setDistributionChannel(String distributionChannel);

    public String getDistributionChannel() throws IPCException;

    public void setPricingProcedure(String pricingProcedure);

    public String getPricingProcedure() throws IPCException;

    public void setDepartureCountry(String departureCountry);

    public void setCountry(String country);

    public String getDepartureCountry() throws IPCException;

    public String getCountry() throws IPCException;

    /**
     * @deprecated The sales organization now is passed as header or item attribute.
     * The sales organization set with this api was used to determine sales organization
     * dependent master data attributes. Those are now determined by the client of the IPC.
     * @param salesOrganisation
     */
    public void setSalesOrganisation(String salesOrganisation);

	/**
	 * @deprecated The sales organization now is passed as header or item attribute.
	 * The sales organization set with this api was used to determine sales organization
	 * dependent master data attributes. Those are now determined by the client of the IPC.
	 */
	public String getSalesOrganisation() throws IPCException;

	public void setGroupConditionProcessingEnabled(boolean enabled);

	public void setHeaderAttributes(Map headerAttributes);

	public HashMap getHeaderAttributes() throws IPCException;

	public String[] getHeaderAttributeEnvironment() throws IPCException;

	public String getHeaderAttributeBinding(String key) throws IPCException;

	public void setExternalId(String id);

	public String getExternalId() throws IPCException;

	public void setEditMode(char mode);

	public char getEditMode() throws IPCException;
    
    public String getApplication();

    public String getUsage();

    public void setApplication(String string);

    public void setUsage(String string);

	/**
	 * Sets the grouping separator to be used when displaying prices, quantities and
	 * related information of this document. Please note that the grouping separator can only be
	 * set before a Document is created and persists through the whole lifetime of the
	 * Document.
	 */
	public void setGroupingSeparator(String sep);

	/**
	 * Returns the grouping separator used by this document, or null, if this separator
	 * has not been set explicitely by setGroupingSeparator.
	 */
	public String getGroupingSeparator();

	/**
	 * Sets the decimal separator to be used when displaying prices, quantities and
	 * related information of this document. Please note that the decimal separator can only be
	 * set before a Document is created and persists through the whole lifetime of the
	 * Document.
	 */
	public void setDecimalSeparator(String sep);

	/**
	 * Returns the decimal separator used by this document, or null, if this separator
	 * has not been set explicitely by setDecimalSeparator.
	 */
	public String getDecimalSeparator();

	/**
	 * Adds a parameter=name setting to these properties that will be passed without any interpretation to
	 * the IPC command that creates the sales document. This method can be used to use sophisticated features
	 * of the pricing engine that are not included in this API because they would make it impossible to
	 * implement it differently.
	 */
	public void addCreationCommandParameter(String name, String value);

    public  boolean getPerformPricingAnalysis() throws IPCException;

    public  void setPerformPricingAnalysis(boolean flag);

	/**
	 * @param businessObjectType
	 */
	public void setBusinessObjectType(String businessObjectType);
	
	public String getBusinessObjectType();

    /**
     * Sets if this document should strip decimal places of values if the decimal
     * places are zero.<br>
     * Examples:<br>
     * Set to true: if the value is "3.00" it will be returned as "3", i.e. the decimal
     * places are stripped.<br>
     * Set to false: if the value is "3.00" it will be returned as "3.00", i.e.g the decimal
     * places are not stripped. The number of decimal places is dependent on the unit of 
     * measurement or the currency (e.g. EUR has 2 decimal places, KWD has 3).<br>
     * If not set the default is true.
     * @param stripDecimalPlaces boolean flag whether decimal places should be stripped 
     * if they are zero.  
     */
    public void setStripDecimalPlaces(boolean stripDecimalPlaces) throws IPCException;

    /**
     * Returns if this document strips decimal for values if they are zero.<br>
     * Example: If set to true a value "3.00" will be returned as "3".<br>
     * Default is true.
     */
    public boolean isStripDecimalPlaces();

}
