/************************************************************************

	Copyright (c) 2001 by SAPMarkets Europe GmbH, Germany, SAP AG

	All rights to both implementation and design are reserved.

	Use and copying of this software and preparation of derivative works
	based upon this software are not permitted.

	Distribution of this software is restricted. SAP does not make any
	warranty about the software, its performance or its conformity to any
	specification.

**************************************************************************/

package com.sap.spc.remote.client.object;

//import com.sap.spc.remote.client.object.imp.CachingClient;

/**
 * A DimensionalValue is any value that can be expressed by a (floating point)
 * number and a unit of some kind. This data type is used to represent sizes,
 * volumes, prices and so on. No conversion of units is supported in any way.
 */
public interface DimensionalValue
{

	/**
	 * Returns the numeric value of this value, if the value can be parsed as
	 * a number. Throws an IPCException if it can't.<p>
	 *
	 * @deprecated
	 * Please note: some DimensionalValues returned by the classes in this
	 * package contain values that are formatted in a way that can not be
	 * converted back to a number. In this case, calling getValue will
	 * throw an IPCException. Use getValueAsString instead.
	 */
	public double getValue() throws IPCException;

	/**
	 * Returns the value as a (formatted) string.
	 */
	public String getValueAsString() throws IPCException;

	/**
	 * Returns the unit of this value.
	 */
	public String getUnit() throws IPCException;


	/**
	 * Changes unit and value of this DimensionalValue.
	 * @deprecated: all values are language dependent, this method may fail badly
	 * if the document language is different than EN.
	 */
	public void setUnitAndValue(double value, String unit);


	/**
	 * Changes unit and value of this DimensionalValue.
	 */
	public void setUnitAndValue(String value, String unit);


	/**
	 * Returns a simple string representation of this dimensional value
	 * (value <space> unit), or null, if communication to the server
	 * was needed and failed.
	 */
	public String toString();

}