package com.sap.spc.remote.client.object;

/**
 * This interface is implemented by objects that can be closed. It is needed
 * by the caching client to determine if a command really needs to be executed.
 * Getter commands that stem from an object that has been closed on the client will
 * not be executed anymore.
 */
public interface Closeable
{
	/**
	 * Marks this object as closed
	 */
	public void close();

	/**
	 * Returns true if this object has been closed.
	 */
	public boolean isClosed();

	/**
	 * Checks whether calling command commandName is considered relevant by this object.
	 * Objects typically put a number of command calls into the Client's "waiting
	 * for execution" queue. If there are dependencies between these commands the object
	 * can not know if a certain command will be required or if it should not be called
	 * until the others have been executed. Using this method it is possible to have
	 * something like a "conditional execution" - when the Client executes its command
	 * queue, isRelevant is called immediately before the actual command execution to
	 * check if the object now has information that might cause it to avoid executing
	 * the command.<p>
	 *
	 * Note: You have to be very careful when using this method. For example, if you don't
	 * know the exact order in which commands are executed, you might access some data
	 * (ie. Value objects) that are not yet pulled from the server, causing a recursive
	 * call of the command loop, which leads to an infinite recursion (!)<p>
	 *
	 * If you don't know what to do, just have this method return true.
	 */
	public boolean isRelevant(String commandName);
}