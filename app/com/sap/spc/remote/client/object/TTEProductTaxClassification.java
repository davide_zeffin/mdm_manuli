
package com.sap.spc.remote.client.object;


public interface TTEProductTaxClassification {

    /**
     * @return country of the tax classification
     */
    public String getCountry();

    /**
     * @return region of the tax classification
     */
    public String getRegion();

    /**
     * @return tax group of the classification
     */
    public String getTaxGroup();

    /**
     * @return tax type of the classification
     */
    public String getTaxType();

    /**
     * @return tax tariff code of the classification
     */
    public String getTaxTariffCode(); 


}
