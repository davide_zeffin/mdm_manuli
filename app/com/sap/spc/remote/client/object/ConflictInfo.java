package com.sap.spc.remote.client.object;

public interface ConflictInfo {

	public static final String DOCU = "&DOCUMENTATION&";
	public static final String EXPL = "&EXPLANATION&";
	public static final String INSTANCE_$ = "$1-";

	public String getConflictId();
	public String getConflictingInstanceId();
	public String getConflictingInstanceName();
	public String getConflictText();
	public String getConflictExplanation();
	public String getConflictDocumentation();
}