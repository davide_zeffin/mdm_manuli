/*
 * Created on 29.06.2005
 *
 */
package com.sap.spc.remote.client.object;

/**
 * @author 
 *
 */
public interface KBProfile {

	/**
	* @see com.sap.spc.remote.client.object.KBProfile#getName()
	*/
	public String getName();

	/**
	 * @see com.sap.spc.remote.client.object.KBProfile#getUIName()
	 */
	public String getUIName();

	/**
	 * @see com.sap.spc.remote.client.object.KBProfile#getRootObjName()
	 */
	public String getRootObjName();
}
