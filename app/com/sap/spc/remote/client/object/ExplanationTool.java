package com.sap.spc.remote.client.object;

/**
 * Title:
 * Description:
 * Copyright:    Copyright (c) 2000
 * Company:
 * @author
 * @version 1.0
 */

import java.util.List;
import java.util.Locale;

import org.apache.struts.util.MessageResources;
public interface ExplanationTool extends Closeable {

    public static final String INSTANCE_$ = "$";
    public static final String VALUE_LAYOUT_TAG_BEGIN =
        "<span class=\"ipcConflictHandlingValueLayout\">";
    public static final String VALUE_LAYOUT_TAG_END = "</span>";
    public IPCClient getIPCClient();
    public Configuration getConfiguration();
    /**
     * Returns a list of conflicts used in the conflict solving process
     */
    public List getConflicts(
        boolean showConflictSolutionRate,
        Locale locale,
        MessageResources resources);
    /**
     * Returns the conflict of one characterisitc
     */
    public DescriptiveConflict getDescriptiveConflictOfCharacteristic(
        String instId,
        String csticName,
        String conflictExplanationLevel,
        String conflictExplanationTextBlockTag,
        String conflictExplanationTextLineTag,
        boolean showConflictSolutionRate,
        Locale locale,
        MessageResources resources,
        boolean useConflictHandlingShortcuts,
        boolean useShortcutValueConflict,
        boolean useExplanationTool);
    /**
     * Returns one single conflict which is used in the conflict solving process
     */
    public Conflict getConflict(
        String conflictId,
        boolean showConflictSolutionRate,
        Locale locale,
        MessageResources resources);
    /**
     * Returns descriptive conflicts (aquivalent to the R3 conflict explanation)
     */
    public List getDescriptiveConflicts(
        String conflictExplanationLevel,
        String conflictExplanationTextBlockTag,
        String conflictExplanationTextLineTag,
        boolean showConflictSolutionRate,
        Locale locale,
        MessageResources resources,
        boolean useConflictHandlingShortcuts,
        boolean useShortcutValueConflict,
        boolean useExplanationTool);
    /**
     * Returns one single descriptive conflict
     */
    //public DescriptiveConflict getDescriptiveConflict(String conflictId);
    /**
     * Tells the Explanation Tool to update the conflicts from the SCE engine
     */
    public void setUpdateConflicts(boolean value);
    /**
     * Returns true if the conflicts must be updated from the SCE engine,
     * returns false otherwise
     */
    public boolean getUpdateConflicts();
    /**
     * Tells the Explanation Tool to update the descriptive conflicts from the SCE engine
     */
    public void setUpdateDescriptiveConflicts(boolean value);
    /**
     * Returns true if the descriptive conflicts must be updated from the SCE engine,
     * returns false otherwise
     */
    public boolean getUpdateDescriptiveConflicts();

    /**
     * asks the ExplanationTool to update itself
     */
    public void update();

}