package com.sap.spc.remote.client.object;

/**
 * Title:
 * Description:
 * Copyright:    Copyright (c) 2000
 * Company:
 * @author
 * @version 1.0
 */

public interface ConflictHandlingShortcut extends Closeable{
	public static final int TYPE_DROP = 1;
	public static final int ENTRY_TO_KEEP_OBSERVABLE_INDEX = 0;
	public static final int ENTRY_TO_KEEP_BINDING_INDEX = 1;
	public boolean isOfTypeDrop();
	public String getValue();
	public String getConflictId();
	public String getConflictParticipantId();
	public String getKey();
	public int getType();
	public String getValueToKeep();
	public String getValueToDrop();
	public String getObservable();
	public String getExplanation();
}