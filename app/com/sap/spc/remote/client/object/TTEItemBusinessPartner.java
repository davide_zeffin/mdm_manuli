package com.sap.spc.remote.client.object;

/**
 * Client view of TTE item business partner.
 */
public interface TTEItemBusinessPartner {

    public static final String SHIP_FROM = "SF";
    public static final String SHIP_TO = "ST";

    /**
     * @return business partner id
     */
    public String getBusinessPartnerId();

    /**
     * @param id business partner id
     */
    public void setBusinessPartnerId(String id);

    /**
     * @return city
     */
    public String getCity();

    /**
     * @return country
     */
    public String getCountry();

    /**
     * @return county
     */
    public String getCounty();

    /**
     * @return region that is exempted
     */
    public String getExemptedRegion();

    /**
     * @return geo code
     */
    public String getGeoCode();

    /**
     * @return jurisdiction code
     */
    public String getJurisdictionCode();

    /**
     * @return zip code
     */
    public String getPostalCode();

    /**
     * @return region
     */
    public String getRegion(); 

    /**
     * @return role (ship-from (SF) or ship-to (ST)) 
     */
    public String getRole();

}
