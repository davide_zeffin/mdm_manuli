/************************************************************************

	Copyright (c) 2001 by SAPMarkets Europe GmbH, Germany, SAP AG

	All rights to both implementation and design are reserved.

	Use and copying of this software and preparation of derivative works
	based upon this software are not permitted.

	Distribution of this software is restricted. SAP does not make any
	warranty about the software, its performance or its conformity to any
	specification.

**************************************************************************/

package com.sap.spc.remote.client.object;


import java.util.HashMap;
import java.util.Hashtable;
import java.util.List;
import java.util.Map;

import com.sap.spc.remote.client.util.ext_configuration;

/**
 * An item. Each item always belongs to a document. Items may not move to other documents.
 * Items may have subitems.<p>
 *
 */
public interface IPCItem extends Closeable {

	// ----------------------- item id and structural methods

	/** Returns the item id (GUID).
	 * @return		the item's Id
	 */
	public  String getItemId();

    /**  Returns the external id of the item. The external id is any id that callers may use to
	 *   name the item. The only relevant id internally is its GUID.
	 * @return		external Id
	 */
	public  String getExternalId() throws IPCException;


	/**
	 * Returns the complete set of item properties.
	 */
	public IPCItemProperties getItemProperties();


    /** Returns the product id of this item.<p>
	 *  Please note that unlike in com.sap.spc.base, where the product id is actually
	 *  the product guid, the product id here is the normal product id (formerly matnr).
	 *  Use getProductGuid to get the GUID.
	 * @return		the product id of the product in this item
	 */
	public  String getProductId() throws IPCException;

	/**
	 * Returns the product type of the product in this item, or null, if this information is not
	 * available. Please note that due to technical restrictions the default implementation of the
	 * IPCItem only returns a product type for configurable products, and throws an IPCException
	 * for non-configurable products.
	 */
	public  String getProductType() throws IPCException;

	/**
	 * Returns the id of the logical system in which the product in this item was originally created,
	 * or null, if this information is not
	 * available. Please note that due to technical restrictions the default implementation of the
	 * IPCItem only returns a product type for configurable products, and throws an IPCException
	 * for non-configurable products.
	 */
	public  String getProductLogSys() throws IPCException;


	/** Returns the product GUID of this item. This GUID is the
	 *  same GUID as the one used when creating the product in a CRM system.
	 * @return		the product guid of the product in this item.
	 */
	public  String getProductGUID() throws IPCException;

	/**
	 * Returns an IPCItemReference to this item.
	 */
	public  IPCItemReference getItemReference();

    /** Returns the parent item of this item. Items are organized in a tree, so every
	 *  item has at most one parent item. If this item is a root item in the document,
	 *  the method returns null.
	 * @return		the parent item of this item.
	 */
	public  IPCItem getParent();

   /** Returns the direct subitems of this item.
	 * @return		all direct subitems of this item
	 */
	public  IPCItem[] getChildren() throws IPCException;

    /** Returns all subitems of this item. The items are ordered in "depth first search, parents first" ordering,
	 *  ie. getDescendants()[0] is the first subitem, getDescendants()[1] its first subitem, ...
	 * @return		all subitems
	 */
	public  IPCItem[] getDescendants() throws IPCException;

    /** Returns the product description of the product that this item holds, a language dependent name of the product,
	 *  in the language of the document of this item.
	 * @return		product description
	 */
	public  String getProductDescription() throws IPCException;



	// ----------------------- attribute access

	/**
	 * Returns the header attribute names. Use getHeaderBindingValue to get values
	 * bound to these names.
	 * @return		array of attribute names
	 */
	public  String[] getHeaderAttributeEnvironment() throws IPCException;

    /** Returns the header attribute binded to attribute name s.
	 * @param		s		        attributeName
	 * @return		headerAttributeValue
	 */
	public  String getHeaderBindingValue(String s) throws IPCException;

    /** Returns the item attribute Value binded to attribute name s.
	 * @param		s       		attributeName
	 * @return		itemAttributeValue
	 */
	public  String getItemBindingValue(String s) throws IPCException;

    /**
	 * to be documented
	 */
	public  void setHeaderAttributeBindings(String[] names, String[] values) throws IPCException;

    /**
     * to be documented
	 */
	public  void  setItemAttributeBindings(String[] names, String[] values) throws IPCException;

    /** Returns Item attribute names.
	 * @return		nameValue pair
	 */
	public  String[] getItemAttributeEnvironment() throws IPCException;



	// ----------------------- configuration properties

    /** Gets all the variants conditions of this item.
	 * @return		String[][]		collection of all variant conditions
	 */
	public  String[][] getVariantConditions() throws IPCException;

    /** Removes all the variant conditions attached to this item.
	 */
	public  void removeAllVariantConditions();

    /** Adds a variant condition to the item.
	 * @param		 name		name of the variant condition
	 * @param		 factor     the factor of the variant condition
	 * @param		 description		description of the variant condition
	 */
	public  void addVariantCondition(String name, Double factor, String description);

    /** Returns true if this item may be configured. This is the case if the item holds a
	 *  configurable material and if the configuration engine knows a valid knowledge base for it.
	 */
	public  boolean isConfigurable() throws IPCException;

    /**
     * Returns true if this item is a product variant.
     */
    public  boolean isProductVariant() throws IPCException;

	/**
	 * Returns true if this item is a changeable product variant.
	 */
	public  boolean isChangeableProductVariant() throws IPCException;

	
	/**
	 * Changes the configuration of this item, updates subitems and prices accordingly.
	 * Please note that this method will delete all existing subitems.
	 * Will enforce a complete recalculation of the item.
	 */
	public  void setConfig(ext_configuration config) throws IPCException;


	/**
	 * Changes the configuration of this item, updates subitems and prices accordingly.
	 * This method will try to connect existing subitems to the instances in the
	 * configuration via the posIdToInstId table in config.
	 * Will enforce a complete recalculation of the item.
	 */
	public  void setConfig(OrderConfiguration config) throws IPCException;
	
	/**
	 * Changes the configuration of this item, updates subitems and prices accordingly.
	 * Also takes in context as parameter.
	 */
	public void setConfig(ext_configuration config, Hashtable context) throws IPCException;


	/**
	 * Returns the configuration that belongs to this item, or null, if !isConfigurable.
	 */
	public  ext_configuration getConfig() throws IPCException;

	/**
	 * Returns the configuration stored in this item, or null, if there is no configuration.
	 */
	public  Configuration getConfiguration() throws IPCException;

	/**
	 * Returns the SCE context table that was last set.
	 */
	public  Map getContext();

	/**
	 * Changes the item's configuration context table.
	 */
	public  void setContext(Map context) throws IPCException;

	/**
	 * Changes customizing parameters on item's
	 */
	public  void setAdditionalParameter(String param, String value) throws IPCException;

    /**
     * Sets the subItemsAllowed attribute of the item.
     */
    public  void setSubItemsAllowed(boolean flag) throws IPCException;


	// ----------------------- pricing properties

    /** Method for pricing of the item.
	 */
	public  void pricing() throws IPCException;

    /** Method for pricing of the item.
	 * @param		redetermineAllConditions flag if all existing conditions are thrown away and a new search and calculation is performed
	 */
	public  void pricing(char mode) throws IPCException;

    /**  Returns whether the item is statistical or not
	 * @return		statistical or not
	 */
	public  boolean getStatistical() throws IPCException;

    /** Sets the statistical attribute of the item
	 * @param		flag		statistical determiner
	 */
	public  void setStatistical(boolean flag) throws IPCException;

    /** Returns the net value of this item, which is its net price excluding subitems.
	 * @return		netValue
	 */
	public  DimensionalValue getNetValue() throws IPCException;
	
 	/**
 	 * Returns the net value of this item without freight.<p>
 	 * Please note that the default implementation of this method calls the IPC server
 	 * once for each item. In a document with many items this will cause serious performance
 	 * degradation. Use carefully.
 	 */
 	public  DimensionalValue getNetValueWithoutFreight() throws IPCException;

    /** Returns the total net value of this item, which is its net price including all subitems.
	 * @return		netValue
	 */
	public  DimensionalValue getTotalNetValue() throws IPCException;

    /** Returns total gross value of item, which is its gross price including all subitems.
	 * @return		total gross value
	 */
	public  DimensionalValue getTotalGrossValue() throws IPCException;

    /**Returns the gross value of this item, which is its gross price excluding subitems.
	 * @return		grossValue
	 */
	public  DimensionalValue getGrossValue() throws IPCException;

    /** Returns the tax value of this item, which is its tax price excluding subitems.
	 * @return		taxValue
	 */
	public  DimensionalValue getTaxValue() throws IPCException;

	/** Returns the total tax value of this item, which is its tax price including all subitems.
	 * @return		netValue
	 */
	public  DimensionalValue getTotalTaxValue() throws IPCException;

    /** Returns the UI Document where this item has been created.
	 * @return		UI Document of this item.
	 */
	public  IPCDocument getDocument();

    /** Sets pricing Date of the item.
	 * @param		s		String representation of new pricing Date
	 * @exception	ParseException
	 */
	public  void setPricingDate(String s)
			throws IPCException;

    /** Returns the pricingDate of the item.
	 * @return		String representation of pricingDate
	 */
	public  String getPricingDate() throws IPCException;

    /** Returns the item's attribute 'pricingItemIsUnchangable'
	 * @return		pricingItemIsUnchangable indicator
	 */
	public  boolean getPricingItemIsUnchangeable() throws IPCException;

    /** Sets the item's attribute 'pricingItemIsUnchangable'
	 * @param		pricingItemIsUnchangable indicator
	 */
	public  void setPricingItemIsUnchangeable(boolean flag);

    /** Returns the item's attribute 'performPricingAnalysis'
	 * @return		performPricingAnalysis indicator
	 */
	public  boolean getPerformPricingAnalysis() throws IPCException;

    /** Sets the item's attribute 'performPricingAnalysis'
	 *  <b>Currently not supported</b>. Please note that right now you
	 *  can only set "perform pricing analysis" when you create an item.
	 *  Support of dynamically changing this is available on the IPC server
	 *  but currently not supported in this API.<p>
	 * @param		performPricingAnalysis	indicator
	 */
	public  void setPerformPricingAnalysis(boolean flag);

    /** Sets service rendering Date.
	 * @param		s	String representation of new service rendering Date
	 * @exception	IPCException
	 */
	public  void setServiceRendererDate(String s)
			throws IPCException;

    /** Gets the service rendering Date of the item.
	 * @return		string representation of service rendering Date
	 */
	public  String getServiceRenderDate() throws IPCException;

	/** Returns the item attribute 'isReturn'
	 * @return		'isReturn' indicator
	 */
	public  boolean isReturn() throws IPCException;

    /** Set item's attribute 'isReturn'
	 * @param		flag	'isReturn' indicator
	 * @exception	none
	 */
	public  void setReturn(boolean flag) throws IPCException;



	// ------------------- dimensional stuff from material master and order

    /** Returns net weight of the item
	 * @return		NetWeight
	 */
	public  DimensionalValue getNetWeight() throws IPCException;

    /** Sets the new net weight of item. (External representation / SaleValue)
	 */
	public  void setNetWeight(DimensionalValue weight);

	/** Sets the base (internal representation) quantity of item.
	 * Do not pass user entered value into this API. They might be formatted in a wrong way.
	 * Pass only values that you are getting from getBaseQuantity();
	 * @exception   IPCException
	 */
	public  void setBaseQuantity(DimensionalValue value) throws IPCException;
    /** Returns the item's base quantity
	 * @return		base Quantity
	 */
	public DimensionalValue getBaseQuantity() throws IPCException;

    /** Returns item's volume (External representation / SaleValue)
	 */
	public DimensionalValue getVolume() throws IPCException;

    /** Sets the items volume (External representation / SaleValue)
	 */
	public  void setVolume(DimensionalValue value) throws IPCException;

    /** Returns the sales quantity of this item.
	 */
	public DimensionalValue getSalesQuantity() throws IPCException;

    /** Sets the sales (external) quantity of item.
	 * @exception   IPCException
	 */
	public  void setSalesQuantity(DimensionalValue value) throws IPCException;

    /** Returns the gross weight of item
	 */
	public DimensionalValue getGrossWeight() throws IPCException;

    /** Sets the gross weight of this item.
	 * @param		weight		new weight
	 * @param		unit		new unit
	 * @param		locale	    locale of item
	 */
	public  void setGrossWeight(DimensionalValue weight) throws IPCException;

	/**
	 * Sets the type of the exchange rate to employed. Since this rate will never be
	 * changed by the server, there is not getExchangeRateType method.
	 */
	public  void setExchangeRateType(String exchangeRateType) throws IPCException;

	/**
	 * Sets the date the exchange rate employed must be valid (format: YYYYMMDD).
	 * Since this date will never be changed by the server, there is no corresponding
	 * getExchangeRateDate method.
	 */
	public  void setExchangeRateDate(String exchangeRateDate) throws IPCException;

	/** Copies existing item to a new item.
	 * @param       targetDocument  the document in which the copy will end up
	 * @return      the new item.
	 */
	public  IPCItem copyItem(IPCDocument targetDocument) throws IPCException;

	/** Returns the pricing conditions of this item.
	 * @return      set of pricing conditions.
	 */
	public  IPCPricingConditionSet getPricingConditions() throws IPCException;

	/** Returns the currency exchange rate
	 * @return      exchange rate
	 */
	public String getExchangeRate() throws IPCException;

	/** Returns the pricing conditions of this item in an external format with
	 * BAPI conformed parameter names. (use in standalone scenarios)
	 * @return      set of pricing conditions.
	 */
	public  HashMap getExternalPricingConditions() throws IPCException;

	/**
	 * @return external (BAPI) representation of the conditions
	 */

	public HashMap getExternalPricingInfo() throws IPCException;

    /**
	 * Adds child as a new child to this item, but doesn't perform any additional
	 * functionality (like retrieving child's state from the server, re-pricing, ...).
	 * If child is already child of this item, do not perform any operation.
     * Note: This method does not add new children on the server. It is only used to update 
     * the children on the client-side. 
     * Example: During the sync-process you have retrieved itemdIds that are not known 
     * on the client-side. Then you use this method to add the item on the client.
     * In this case you don't want to create a new child on the server because it is already
     * existing there.
     * @param child Child Item to be added to this item.
	 */
	public void addChild(IPCItem child);

	/**
	 * Initializes common properties of this item. The cacheKey passed to this method can be
	 * used to determine if these values are still valid on the server.<p>
	 *
	 * This method is called at certain times to optimize performance. Simple item implementations
	 * can just implement it by an empty method body.<p>
	 *
	 * This method is not supposed to be used from outside and will be removed in later releases.
	 */
	public void initCommonProperties(Object cacheKey,
	                                 String netValue, String taxValue, String grossValue,
									 String totalNetValue, String totalTaxValue, String totalGrossValue,
									 String currencyUnit,
									 String productGuid, String productId, String productDescription,
									 String baseQuantityValue, String baseQuantityUnit,
									 String salesQuantityValue, String salesQuantityUnit,
									 String configurable, String variant, String complete, String consistent, String configChanged,
									 List children);
									 
	/**
	 * This method gets the pricing analysis Data.
	 * @return the XML Document containing the Pricing Analysis Data
	 * @throws IPCException
	 */
	public String getPricingAnalysisData() throws IPCException;
    
//    public void syncWithServer() throws IPCException;	
//    
    /**
     * Set item attributes.
     * First set these attributes to the ItemProperties then send them to the server.
     * If you only want to set attributes on the client side set them directly on the ItemProperties.
     * @param attributes map of item attributes
     */    public void setItemAttributes(Map attributes);

	/**
	 * @return
	 */
	public IPCItemProperties[] getPropertiesFromServer(String[] itemIds);

    /**
     * Get the product key information from the backend (productId, productType, productLogSys).
     * @return ProductKey object
     */
    public ProductKey getProductKey();
    
    /**
     * Get the product GUID for the given parameters from the backend. The application parameter is used
     * to determine in which context we are running (CRM or ERP). In the ERP case the productId is the productGuid
     * and will be returned.
     *
     * @param productId 
     * @param productType
     * @param productLogSys
     * @return
     */
    public String getProductGuid(String productId, String productType, String productLogSys);
    
    /**
     * Retrieves the item attributes from product master.
     * It is possible to retrieve the attributes of other items if the appropriate productGuid is passed.
     * If null is passed as productGUID this method will return the attributes of its item. 
     * @param guid The productGUID for which the attribtues should be retreived.
     * @return Map with the attributes.
     */
    public Map getAttributesFromProductMaster(String guid);
    
    /**
     * @param productId The product id that should be switched to (e.g. "WP-940"). 
     */
    public boolean changeProduct(String productId);        
    
    /**
     * @return TTE item if existing
     */
    public TTEItem getTteItem();
    
    /**
     * Sets the initial configuration (from the database) of this item
     * @param config initial configuration   
     */
    public void setInitialConfiguration(ext_configuration config) throws IPCException;        

    public void setTteItem(TTEItem tteItem);    

    public Map getDynamicReturn();
    
    /**
     * This method returns the initial configuration snapshot. This has to be set 
     * via function module SPC_SET_INITIAL_CONFIGURATION or method setInitialConfiguration() 
     * before.
     * @return the inital configuration snapshot.
     */
    public ConfigurationSnapshot getInitialConfiguration();

    /** Changes the external id on the server.
     * @param value external id to be set
     * @throws IPCException
     */
    public void changeExternalId(String value) throws IPCException;
    
    public IPCItemProperties getItemPropertiesNoSync();    

	/**
	 * If the item is configurable and a root item of the configuration,
	 * then this method returns the rootitem or subitem belonging to the passed instance id.
	 * @param String The id of the configuration instance
	 * @return IPCItem The item belonging to that instance.
	 */
	public IPCItem getItemFromInstanceId(String instanceId);    
    								 
}
