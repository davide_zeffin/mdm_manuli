/************************************************************************

	Copyright (c) 2001 by SAPMarkets Europe GmbH, Germany, SAP AG

	All rights to both implementation and design are reserved.

	Use and copying of this software and preparation of derivative works
	based upon this software are not permitted.

	Distribution of this software is restricted. SAP does not make any
	warranty about the software, its performance or its conformity to any
	specification.

**************************************************************************/

package com.sap.spc.remote.client.object;

import java.util.List;


/**
 * A characteristic design group. In the SCE engine and EAL, design
 * groups are just treated as data that belong to a characteristic.
 * In user interfaces, though, characteristic groups are an important
 * means of separating the set of characteristics into smaller pieces.
 * Therefore a cstic group is a separate class in this client-side API
 * that is close to the UI.
 */
public interface CharacteristicGroup extends Closeable
{
	/**
	 * Returns the name of this characteristic group. Please note that
	 * there is no distinction between language dependent and independent
	 * names here, just a single name.<p>
	 *
	 * If no group has actually been maintained, this group is a dummy group.
	 * In this case (only), getName returns null.
	 */
	public String getName();
	
	
	public String getLanguageDependentName();

	/**
	 * Returns all characteristics in this group.
	 */
	public List getCharacteristics();

	/**
	 * Returns visible characteristics only if includeInvisible is set to false
	 * Otherwise this method returns visible and invisible characteristics (same
	 * as getCharacteristics().
	 */
	public List getCharacteristics(boolean includeInvisible);

    /**
     * Returns the characteristics of this group that belong to the given
     * application view. If includeInvisible is true, the result set will also
     * contain invisible characteristics. 
     */    
    public List getCharacteristics(boolean includeInvisible, char view);
	
	/**
	 * @param characteristicName
	 * @param includeInvisible
	 * @return Reference to the Characteristic identified by characteristicName.
	 * null, if the Characteristic can not be found.
	 */
	public Characteristic getCharacteristic(String characteristicName, boolean includeInvisible);

	/**
	 * Returns assignable values only if assignableValuesOnly is set to true.
	 * Otherwise assignable and not assignable values will be returned. 
	 */
	public List getCharacteristics(boolean includeInvisible, boolean assignableValuesOnly);

	/**
	 * Returns the instance to which the characteristic group belongs
	 */
	public Instance getInstance();

    /**
     * Returns the position of this characteristic group. 0 is the first
     * position.
     */
    public int getPosition();

	/**
	 * Returns true only for the base group. The base group is a virtually
	 * defined group that holds all characteristics that have not been assigned
	 * to any group.
	 */
	public boolean isBaseGroup();

    /**
     * Returns true if the characteristic group was selected in the UI;
     * returns false otherwise
     */
     public boolean isSelected();
     /**
      * Sets the selected flag for this object. The flag is only relevant for the UI.
      * It is neither transferred to nor delivered from the IPC server.
      * @param selected
      */
     public void setSelected(boolean selected);

	 /**
	  * Sets the consistent flag for the characteristic group. Since characteristic groups are pure
	  * ui objects, the value is never transferred to nor delivered from the IPC server.
	  * A consistent group is a group that contains only consistent characteristics.
	  * An inconsistent group is a group that contains at least one inconsistent characteristic.
	  * @param consistent
	  */
	 public void setConsistent(boolean consistent);

	 /**
	  * Sets the required flag for this group.
	  * Only client side storage for ui usage.
	  * Not transferred to nor read from the IPC server.
	  * @param hasRequiredCstics
	  */
	 public void setRequired(boolean hasRequiredCstics);

	 /**
	  * 
	  * @return true if the group contains required characteristics without value, false otherwise.
	  */
	 public boolean isRequired();

	 /**
	  * 
	  * @return true if all characteristics of this group are consistent, false otherwise.
	  */
	 public boolean isConsistent();

}
