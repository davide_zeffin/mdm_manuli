/************************************************************************

	Copyright (c) 2001 by SAP AG

	All rights to both implementation and design are reserved.

	Use and copying of this software and preparation of derivative works
	based upon this software are not permitted.

	Distribution of this software is restricted. SAP does not make any
	warranty about the software, its performance or its conformity to any
	specification.

**************************************************************************/
package com.sap.spc.remote.client.object;

/**
 * Collection of value description pairs. This pairs are generally used for
 * constructing input help.
 */
public interface ValueDescriptionPairs {

    public String[] getValues() throws IPCException;

    public String[] getDescriptions() throws IPCException;

}