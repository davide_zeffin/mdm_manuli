package com.sap.spc.remote.client.object.imp;

import java.util.Hashtable;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.StringTokenizer;
import java.util.Vector;

import org.apache.struts.util.MessageResources;

import com.sap.isa.businessobject.BusinessObjectBase;
import com.sap.isa.core.TechKey;
import com.sap.spc.remote.client.object.Configuration;
import com.sap.spc.remote.client.object.Conflict;
import com.sap.spc.remote.client.object.ConflictHandlingShortcut;
import com.sap.spc.remote.client.object.ConflictParticipant;
import com.sap.spc.remote.client.object.DescriptiveConflict;
import com.sap.spc.remote.client.object.DescriptiveConflictType;
import com.sap.spc.remote.client.object.ExplanationTool;
import com.sap.spc.remote.client.object.IClient;
import com.sap.spc.remote.client.object.IPCClient;
import com.sap.spc.remote.shared.command.GetCsticConflicts;
import com.sap.spc.remote.shared.command.GetFacts;
import com.sap.spc.remote.client.ResourceAccessor;
import com.sap.tc.logging.Category;
import com.sap.tc.logging.Location;

public abstract class DefaultExplanationTool
    extends BusinessObjectBase
    implements ExplanationTool {

    protected IPCClientObjectFactory factory =
        IPCClientObjectFactory.getInstance();
    protected IClient cachingClient;
    protected IPCClient ipcClient;
    protected Configuration configuration;
    protected boolean closed;
    protected Hashtable conflicts;
    protected Vector sortedConflicts;
    protected Vector descriptiveConflicts;
    protected Hashtable descriptiveConflIndex;
    protected boolean updateConflicts;
    protected boolean updateDescriptiveConflicts;
    protected long changeTime;
    protected String[] conflictingInstances;
    protected String[] conflictingCharacteristics;
    protected static final String ASSUMPTION_AUTHOR_DEFAULT = "D";
    protected static final String ASSUMPTION_AUTHOR_USER = "U";

    protected static final String DEL = " ";

    // logging
	private static final Category category =
	ResourceAccessor.category;
	private static final Location location =
		ResourceAccessor.getLocation(DefaultExplanationTool.class);

    public List getConflicts(
        boolean showConflictSolutionRate,
        Locale locale,
        MessageResources resources) {
        if (getUpdateConflicts()) {
            readInConflicts(showConflictSolutionRate, locale, resources);
        }
        return (List) sortedConflicts;
    }

    protected abstract void readInConflicts(
        boolean showConflictSolutionRate,
        Locale locale,
        MessageResources resources);
            
    public Conflict getConflict(
        String conflictId,
        boolean showConflictSolutionRate,
        Locale locale,
        MessageResources resources) {
        if (getUpdateConflicts()) {
            readInConflicts(showConflictSolutionRate, locale, resources);
        }
        return (Conflict) conflicts.get(conflictId);
    }

    public List getDescriptiveConflicts(
        String conflictExplanationLevel,
        String conflictExplanationTextBlockTag,
        String conflictExplanationTextLineTag,
        boolean showConflictSolutionRate,
        Locale locale,
        MessageResources resources,
        boolean useConflictHandlingShortcuts,
        boolean useShortcutValueConflict,
        boolean useExplanationTool) {
        if (getUpdateDescriptiveConflicts()) {
            readInDescriptiveConflicts(conflictExplanationLevel,
                conflictExplanationTextBlockTag,
                conflictExplanationTextLineTag,
                showConflictSolutionRate,
                locale,
                resources,
                useConflictHandlingShortcuts,
                useShortcutValueConflict,
                useExplanationTool);
        }                
        return (List) descriptiveConflicts;
    }

    public DescriptiveConflict getDescriptiveConflictOfCharacteristic(
        String instId,
        String csticName,
        String conflictExplanationLevel,
        String conflictExplanationTextBlockTag,
        String conflictExplanationTextLineTag,
        boolean showConflictSolutionRate,
        Locale locale,
        MessageResources resources,
        boolean useConflictHandlingShortcuts,
        boolean useShortcutValueConflict,
        boolean useExplanationTool) {
        if (getUpdateDescriptiveConflicts()) {
            readInDescriptiveConflicts(conflictExplanationLevel,
                conflictExplanationTextBlockTag,
                conflictExplanationTextLineTag,
                showConflictSolutionRate,
                locale,
                resources,
                useConflictHandlingShortcuts,
                useShortcutValueConflict,
                useExplanationTool);
        }
        DescriptiveConflict conflict =
            (DescriptiveConflict) descriptiveConflIndex.get(
                instId + DEL + csticName);
        return conflict;
    }

    protected abstract void readInDescriptiveConflicts(
        String conflictExplanationLevel,
        String conflictExplanationTextBlockTag,
        String conflictExplanationTextLineTag,
        boolean showConflictSolutionRate,
        Locale locale,
        MessageResources resources,
        boolean useConflictHandlingShortcuts,
        boolean useShortcutValueConflict,
        boolean useExplanationTool); 
        
    public IPCClient getIPCClient() {
        return this.ipcClient;
    }

    public Configuration getConfiguration() {
        return this.configuration;
    }

    public boolean isRelevant(String commandName) {
        // we don't use the cache
        return true;
    }
    
    public boolean isClosed() {
        return closed;
    }
    
    public void close() {
        this.closed = true;
    }
    
    public boolean getUpdateConflicts() {
        return this.updateConflicts;
    }
    
    public synchronized void setUpdateConflicts(boolean value) {
        this.updateConflicts = value;
    }
    
    public boolean getUpdateDescriptiveConflicts() {
        return this.updateDescriptiveConflicts;
    }
    
    public synchronized void setUpdateDescriptiveConflicts(boolean value) {
        this.updateDescriptiveConflicts = value;
    }
    
    public synchronized void update() {
        this.setUpdateConflicts(true);
        this.setUpdateDescriptiveConflicts(true);
    }

    public class DefaultDescriptiveConflict extends BusinessObjectBase
        implements com.sap.spc.remote.client.object.DescriptiveConflict {
			protected ExplanationTool explTool;
			protected String text;
			protected boolean userOwned;
			protected boolean hasShortcut;
			protected String id;
			protected String instId;
			protected String csticName;
			protected String csticLangDepName;
			protected Vector conflictTypes;
			protected String lText;
			protected String adjustedLongText;
			protected IClient cachingClient;
			protected boolean closed;
			protected boolean longTextAdjusted;
			protected ConflictHandlingShortcut shortcut;

        DefaultDescriptiveConflict() {
        }
        
        public DefaultDescriptiveConflict(
            ExplanationTool explTool,
            String instId,
            String csticName,
            String csticLangDepName,
            String lText,
            boolean showConflictSolutionRate,
            Locale locale,
            MessageResources resources,
            String conflictExplanationTextLineTag,
            boolean useConflictHandlingShortcuts,
            boolean useShortcutValueConflict,
            boolean useExplanationTool) {
            	super(new TechKey(instId + csticName));
            this.explTool = explTool;
            this.instId = instId;
            this.csticName = csticName;
            this.csticLangDepName = csticLangDepName;
            this.lText = lText;
            this.closed = false;
            this.closed = false;
            this.hasShortcut = false;
            this.shortcut = null;
            adjustLongText(
                lText,
                showConflictSolutionRate,
                locale,
                resources,
                conflictExplanationTextLineTag,
                useConflictHandlingShortcuts,
                useShortcutValueConflict,
                useExplanationTool);
            this.longTextAdjusted = true;
        }

        public ExplanationTool getExplanationTool() {
            return this.explTool;
        }

        public String getInstanceId() {
            return this.instId;
        }
        
        public String getCharacteristicName() {
            return this.csticName;
        }
        
        public String getCharacteristicLangDepName() {
            return this.csticLangDepName;
        }
        
        public String getLongText(
            boolean showConflictSolutionRate,
            Locale locale,
            MessageResources resources,
            String conflictExplanationTextLineTag,
            boolean useConflictHandlingShortcuts,
            boolean useShortcutValueConflict,
            boolean useExplanationTool) {
            if (longTextAdjusted) {
                return this.adjustedLongText;
            }
            return adjustLongText(
                this.lText,
                showConflictSolutionRate,
                locale,
                resources,
                conflictExplanationTextLineTag,
                useConflictHandlingShortcuts,
                useShortcutValueConflict,
                useExplanationTool);
        }
        
        public List getConflictTypes(
            boolean showConflictSolutionRate,
            Locale locale,
            MessageResources resources,
            String conflictExplanationTextLineTag,
            boolean useConflictHandlingShortcuts,
            boolean useShortcutValueConflict,
            boolean useExplanationTool) {
            if (!longTextAdjusted) {
                adjustLongText(
                    this.lText,
                    showConflictSolutionRate,
                    locale,
                    resources,
                    conflictExplanationTextLineTag,
                    useConflictHandlingShortcuts,
                    useShortcutValueConflict,
                    useExplanationTool);
            }
            return (List) this.conflictTypes;
        }
        
        public boolean isRelevant(String commandName) {
            // we don't use the cache
            return true;
        }
        
        public boolean isClosed() {
            return closed;
        }
        
        public void close() {
            this.closed = true;
        }

		protected String replacePlaceHolder(
            String text,
            String placeHolder,
            String resource) {
            int index = text.indexOf(placeHolder);
            while (index > -1) {
                int end = index + placeHolder.length();
                text = text.substring(0, index) + resource + text.substring(end);
                index = text.indexOf(placeHolder);
            }
            return text;
        }

		protected String adjustLongText(
            String text,
            boolean showConflictSolutionRate,
            Locale locale,
            MessageResources resources,
            String conflictExplanationTextLineTag,
            boolean useConflictHandlingShortcuts,
            boolean useShortcutValueConflict,
            boolean useExplanationTool) {
            int indexStart;
            int indexEnd;

            //  IS_USER_CREATED
            String userCreatedString =
                resources.getMessage(locale, "ipc.conflict.created.by.user");
            text =
                replacePlaceHolder(
                    text,
                    GetCsticConflicts.IS_USER_CREATED,
                    " " + userCreatedString);
            //  INFERRED_BY_SYSTEM
            text =
                replacePlaceHolder(
                    text,
                    GetCsticConflicts.INFERRED_BY_SYSTEM,
                    " " + resources.getMessage(locale, "ipc.conflict.inferred.by.system"));
            //  REQUIRED_STATICALLY
            text =
                replacePlaceHolder(
                    text,
                    GetCsticConflicts.REQUIRED_STATICALLY,
                    " " + resources.getMessage(locale, "ipc.conflict.required.static"));
            //  REQUIRED_BY_USER
            text =
                replacePlaceHolder(
                    text,
                    GetCsticConflicts.REQUIRED_BY_USER,
                    " " + resources.getMessage(locale, "ipc.conflict.required.by.user"));
            //  EXCLUDED_BY_USER
            text =
                replacePlaceHolder(
                    text,
                    GetCsticConflicts.EXCLUDED_BY_USER,
                    " " + resources.getMessage(locale, "ipc.conflict.excluded.by.user"));
            //  DEFAULT
            text =
                replacePlaceHolder(
                    text,
                    GetCsticConflicts.IS_DEFAULT,
                    " " + resources.getMessage(locale, "ipc.conflict.is.default"));
            //  Handle Conflict Type
            this.conflictTypes = new Vector();
            indexStart = text.indexOf(GetCsticConflicts.CONFLICT_TYPE_BEGIN);
            while (indexStart > -1) {
                indexStart = indexStart + GetCsticConflicts.CONFLICT_TYPE_BEGIN.length();
                indexEnd =
                    text.indexOf(
                        GetCsticConflicts.CONFLICT_TYPE_END,
                        indexStart);
                String conflictTypeKey = text.substring(indexStart, indexEnd);
                String conflictTypeName = conflictTypeKey;
                conflictTypeName = conflictTypeName.replace('_', '.');
                conflictTypeName = conflictTypeName.toLowerCase();
                conflictTypeName = "ipc." + conflictTypeName;
                conflictTypeName = resources.getMessage(locale, conflictTypeName);

                indexStart =
                    text.indexOf(
                        GetCsticConflicts.CONFLICT_TEXT_BEGIN,
                        indexEnd);
                indexStart = indexStart + GetCsticConflicts.CONFLICT_TEXT_BEGIN.length();
                indexEnd =
                    text.indexOf(
                        GetCsticConflicts.CONFLICT_TEXT_END,
                        indexStart);
                String conflictTypeText = text.substring(indexStart, indexEnd);
                if (conflictTypeText.indexOf(GetCsticConflicts.NO_CONFLICT_TEXT) > -1) {
                    conflictTypeText = "";
                }
                // Creation of ConflictTypes
                DescriptiveConflictType conflictType =
                    new DefaultDescriptiveConflictType(
                        this,
                        conflictTypeKey,
                        conflictTypeName,
                        conflictTypeText,
                        userCreatedString,
                        showConflictSolutionRate,
                        locale,
                        resources,
                        conflictExplanationTextLineTag,
                        useConflictHandlingShortcuts,
                        useShortcutValueConflict,
                        useExplanationTool);
                this.conflictTypes.add(conflictType);

                indexStart =
                    text.indexOf(
                        GetCsticConflicts.CONFLICT_TYPE_BEGIN,
                        indexEnd);
            }
            this.longTextAdjusted = true;
            this.adjustedLongText = text;
            return adjustedLongText;
        }

        public synchronized void setShortcut(ConflictHandlingShortcut shortcut) {
            this.shortcut = shortcut;
            this.hasShortcut = true;
        }
        
        public ConflictHandlingShortcut getShortcut() {
            return this.shortcut;
        }

        public boolean hasShortcut() {
            return this.hasShortcut;
        }
    }

    class DefaultDescriptiveConflictType
        extends BusinessObjectBase
        implements DescriptiveConflictType {

			protected DescriptiveConflict conflict;
			protected String type;
			protected String name;
			protected String text;
			protected String adjustedText;
			protected Vector factKeys;
			protected boolean closed;
			protected ConflictHandlingShortcut shortcut;
			protected boolean hasShortcut;
			protected int numberOfMembers;
			protected String userBinding;

        DefaultDescriptiveConflictType() {
        }

        DefaultDescriptiveConflictType(
            DescriptiveConflict conflict,
            String type,
            String name,
            String text,
            String userCreatedString,
            boolean showConflictSolutionRate,
            Locale locale,
            MessageResources resources,
            String conflictExplanationTextLineTag,
            boolean useConflictHandlingShortcuts,
            boolean useShortcutValueConflict,
            boolean useExplanationTool) {
            	super(new TechKey(((DefaultDescriptiveConflict)conflict).getTechKey() + type + name));
            this.conflict = conflict;
            this.type = type;
            this.name = name;
            this.text = text;
            this.factKeys = new Vector();
            this.hasShortcut = false;
            this.shortcut = null;
            adjustedText =
                adjustText(
                    text,
                    userCreatedString,
                    showConflictSolutionRate,
                    locale,
                    resources,
                    conflictExplanationTextLineTag,
                    useConflictHandlingShortcuts,
                    useShortcutValueConflict,
                    useExplanationTool);
            this.closed = false;
        }
        
        public DescriptiveConflict getDescriptiveConflict() {
            return this.conflict;
        }
        
        public String getType() {
            return this.type;
        }
        
        public String getName() {
            return this.name;
        }
        
        public String getText() {
            return this.adjustedText;
        }
        
        public boolean hasText() {
            if ((this.adjustedText == null) || this.adjustedText.equals("")) {
                return false;
            }
            return true;
        }
        
        public int getNumberOfMembers() {
            return numberOfMembers;
        }

		protected String getKey(String text, String beginTag, String endTag) {
            int indexStart;
            if (beginTag.equals("")) {
                indexStart = 0;
            }
            else {
                indexStart = text.indexOf(beginTag);
            }
            if (indexStart == -1) {
                return text;
            }
            indexStart = indexStart + beginTag.length();
            int indexEnd = text.indexOf(endTag);
            if (indexEnd == -1) {
                return text;
            }
            return text.substring(indexStart, indexEnd);
        }

		protected String replaceText(
            String text,
            String oldTerm,
            String newTerm) {
            int indexStart = text.indexOf(oldTerm);
            if (indexStart == -1) {
                return text;
            }
            int indexEnd = indexStart + oldTerm.length();
            String newText =
                text.substring(0, indexStart)
                    + newTerm
                    + text.substring(indexEnd);
            return newText;
        }

		protected String adjustText(
            String text,
            String userCreatedString,
            boolean showConflictSolutionRate,
            Locale locale,
            MessageResources resources,
            String conflictExplanationTextLineTag,
            boolean useConflictHandlingShortcuts,
            boolean useShortcutValueConflict,
            boolean useExplanationTool) {
            userBinding = "";
            numberOfMembers = 0;
            while (text.indexOf(GetFacts.FACT_KEY_BEGIN) > -1) {
                // exctract fact key
                String factKey =
                    getKey(
                        text,
                        GetFacts.FACT_KEY_BEGIN,
                        GetFacts.FACT_KEY_END);
                StringTokenizer factKeyTokenizer =
                    new StringTokenizer(
                        factKey,
                        GetFacts.FACT_KEY_DELIMITER,
                        false);
                String[] keys = new String[5];
                int i = 0;
                while (factKeyTokenizer.hasMoreTokens()) {
                    if (i == 5) {
                        break;
                    }
                    keys[i] = factKeyTokenizer.nextToken();
                    i++;
                }
                String instId = keys[GetFacts.FACT_KEY_INST_ID];
                String observType = keys[GetFacts.FACT_KEY_OBSERV_TYPE];
                String observable = keys[GetFacts.FACT_KEY_OBSERVABLE];
                String binding = keys[GetFacts.FACT_KEY_BINDING];
                // get user binding
                String searchString =
                        "<"
                        + conflictExplanationTextLineTag
                        + ">"
                        + GetFacts.FACT_KEY_BEGIN
                        + factKey
                        + GetFacts.FACT_KEY_END
                        + userCreatedString
                        + "</"
                        + conflictExplanationTextLineTag
                        + ">";
                int userBindingIndex = text.indexOf(searchString);
                if (userBindingIndex > -1) {
                    userBinding = binding;
                }
                text =
                    replaceText(
                        text,
                        GetFacts.FACT_KEY_BEGIN
                            + factKey
                            + GetFacts.FACT_KEY_END,
                        ExplanationTool.VALUE_LAYOUT_TAG_BEGIN
                            + binding
                            + ExplanationTool.VALUE_LAYOUT_TAG_END);
                factKey =
                    GetFacts.FACT_KEY_PLACEHOLDER
                        + GetFacts.FACT_KEY_DELIMITER
                        + instId
                        + GetFacts.FACT_KEY_DELIMITER
                        + observType
                        + GetFacts.FACT_KEY_DELIMITER
                        + observable
                        + GetFacts.FACT_KEY_DELIMITER
                        + binding;
                factKeys.add(factKey);
                // check for shortcuts
                if (!useConflictHandlingShortcuts
                    || !useShortcutValueConflict
                    || !useExplanationTool) {
                    continue;
                }
                List conflicts =
                    this.conflict.getExplanationTool().getConflicts(
                        showConflictSolutionRate,
                        locale,
                        resources);
                if (conflicts == null) {
                    continue;
                }
                Iterator it = conflicts.iterator();
                boolean isPartOfEveryConflict = true;
                ConflictParticipant participant = null;
                while (it.hasNext()) {
                    Conflict conflict = (Conflict) it.next();
                    participant = conflict.getParticipantByKey(factKey);
                    if (participant == null) {
                        isPartOfEveryConflict = false;
                        break;
                    }
                }
                if (isPartOfEveryConflict) {
                    this.shortcut = createShortCut(participant, observable, binding);
                    this.hasShortcut = true;
                    this.conflict.setShortcut(this.shortcut);
                }
                numberOfMembers++;
            }
            return text;
        }

		protected ConflictHandlingShortcut createShortCut(
            ConflictParticipant participant,
            String observable,
            String value) {
            ConflictHandlingShortcut shortcut =
                new DefaultConflictHandlingShortcut(
                    this,
                    participant,
                    ConflictHandlingShortcut.TYPE_DROP,
                    observable,
                    value);
            return shortcut;
        }

        public boolean hasShortcut() {
            return this.hasShortcut;
        }
        
        public ConflictHandlingShortcut getShortcut() {
            return this.shortcut;
        }
        
        public Vector getFactKeys() {
            return this.factKeys;
        }
        
        public String getUserSelection() {
            return this.userBinding;
        }
        
        public boolean hasUserSelection() {
            if (userBinding.equals("")) {
                return false;
            }
            return true;
        }

        //Methods implementation of Closeable
        public boolean isRelevant(String commandName) {
            // we don't use the cache
            return true;
        }
        
        public boolean isClosed() {
            return closed;
        }
        
        public void close() {
            this.closed = true;
        }

    }

    class DefaultConflictHandlingShortcut
        extends BusinessObjectBase
        implements ConflictHandlingShortcut {

			protected DescriptiveConflictType descriptiveConflictType;
			protected ConflictParticipant conflictParticipant;
			protected int type;
			protected String key;
			protected String value;
			protected String observable;
			protected String conflictId;
			protected String participantId;

			protected boolean closed;

        DefaultConflictHandlingShortcut() {
            type = TYPE_DROP;
            value = "";
            conflictId = "";
            participantId = "";
            this.closed = false;
        }
        
        DefaultConflictHandlingShortcut(
            DescriptiveConflictType descriptiveConflictType,
            ConflictParticipant conflictParticipant,
            int type,
            String observable,
            String value) {
            	super(new TechKey(descriptiveConflictType.getName() + conflictParticipant.getId() + value));
            this.descriptiveConflictType = descriptiveConflictType;
            this.conflictParticipant = conflictParticipant;
            this.observable = observable;
            this.type = type;
            this.key = conflictParticipant.getKey();
            this.conflictId = conflictParticipant.getConflict().getId();
            this.participantId = conflictParticipant.getId();
            this.value = value;
            this.closed = false;
        }
        
        public boolean isOfTypeDrop() {
            if (type == TYPE_DROP)
                return true;
            return false;
        }
        
        public String getValue() {
            return this.value;
        }
        
        public String getConflictId() {
            return this.conflictId;
        }
        
        public String getConflictParticipantId() {
            return this.participantId;
        }
        
        public String getKey() {
            return this.key;
        }
        
        public int getType() {
            return this.type;
        }

        public String getValueToKeep() {
            // conflict type shoul have exactly 2 members: one to keep and one to drop
            if (descriptiveConflictType.getNumberOfMembers() != 2) {
                return null;
            }
            Vector factKeys = descriptiveConflictType.getFactKeys();
            String factKey1 = (String) factKeys.get(0);
            String factKey2 = (String) factKeys.get(1);
            String factKey;
            if (factKey1.equals(conflictParticipant.getKey())) {
                factKey = factKey2;
            }
            else {
                factKey = factKey1;
            }
            StringTokenizer factKeyTokenizer =
                new StringTokenizer(
                    factKey,
                    GetFacts.FACT_KEY_DELIMITER,
                    false);
            String[] keys = new String[2];
            int i = 0;
            while (factKeyTokenizer.hasMoreTokens()) {
                String token = factKeyTokenizer.nextToken();
                if (i == GetFacts.FACT_KEY_OBSERVABLE) {
                    keys[ENTRY_TO_KEEP_OBSERVABLE_INDEX] = token;
                }
                if (i == GetFacts.FACT_KEY_BINDING) {
                    keys[ENTRY_TO_KEEP_BINDING_INDEX] = token;
                }
                if (i == 4) {
                    break;
                }
                i++;
            }
            return keys[ENTRY_TO_KEEP_BINDING_INDEX];
        }

        public String getValueToDrop() {
            String value = "";
            String factKey = conflictParticipant.getKey();
            StringTokenizer factKeyTokenizer =
                new StringTokenizer(
                    factKey,
                    GetFacts.FACT_KEY_DELIMITER,
                    false);
            int i = 0;
            while (factKeyTokenizer.hasMoreTokens()) {
                String token = factKeyTokenizer.nextToken();
                if (i == GetFacts.FACT_KEY_BINDING)
                    value = token;
                i++;
            }
            return value;
        }

        public String getObservable() {
            return this.observable;
        }

        public String getExplanation() {
            return this.descriptiveConflictType.getText();
        }

        //Methods implementation of Closeable
        public boolean isRelevant(String commandName) {
            // we don't use the cache
            return true;
        }
        public boolean isClosed() {
            return closed;
        }
        public void close() {
            this.closed = true;
        }
    }

}
