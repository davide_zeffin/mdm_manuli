/*
 * Created on 21.07.2004
 *
 * To change the template for this generated file go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
package com.sap.spc.remote.client.object.imp;

import java.util.HashMap;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Properties;

import org.apache.struts.util.MessageResources;

import com.sap.isa.core.eai.BackendBusinessObject;
import com.sap.isa.core.util.GenericFactory;
import com.sap.spc.remote.client.util.ext_configuration;
import com.sap.spc.remote.client.object.Characteristic;
import com.sap.spc.remote.client.object.Configuration;
import com.sap.spc.remote.client.object.Conflict;
import com.sap.spc.remote.client.object.ExplanationTool;
import com.sap.spc.remote.client.object.IClient;
import com.sap.spc.remote.client.object.IPCClient;
import com.sap.spc.remote.client.object.IPCConfigReference;
import com.sap.spc.remote.client.object.IPCDocument;
import com.sap.spc.remote.client.object.IPCDocumentProperties;
import com.sap.spc.remote.client.object.IPCException;
import com.sap.spc.remote.client.object.IPCItem;
import com.sap.spc.remote.client.object.IPCItemProperties;
import com.sap.spc.remote.client.object.IPCItemReference;
import com.sap.spc.remote.client.object.Instance;
import com.sap.spc.remote.client.object.KnowledgeBase;
import com.sap.spc.remote.client.object.MimeObjectContainer;
import com.sap.spc.remote.client.object.ProductVariant;
import com.sap.spc.remote.client.object.ProductVariantFeature;
import com.sap.spc.remote.client.object.TTEDocument;
import com.sap.spc.remote.client.object.imp.rfc.RfcDefaultIPCClient;
import com.sap.spc.remote.client.ResourceAccessor;
import com.sap.tc.logging.Category;
import com.sap.tc.logging.Location;
import com.sap.util.monitor.jarm.IMonitor;

/**
 * @author 
 *
 * To change the template for this generated type comment go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
public abstract class IPCClientObjectFactory {

    // logging
    private static final Category category = ResourceAccessor.category;
    private static final Location location = ResourceAccessor.getLocation(IPCClientObjectFactory.class);

    // used instance (Singeleton Pattern)
    protected static IPCClientObjectFactory instance = null /*new IPCClientObjectFactory()*/;

    protected static final String IPC_CLIENT_OBJECT_FACTORY_NAME =
        "IPCClientObjectFactory";

    protected String clientType = null;
	
    /**
     * Return the only instance of the class (Singleton). <br>
     * 
     * @return Factory object. Depending on the scenario either RfcIPCClientObjectFactory or TcpIPCClientObjectFactory.
     */
    public static IPCClientObjectFactory getInstance() {
    	
    	if (instance != null) return instance;
		instance =
			(IPCClientObjectFactory) GenericFactory.getInstance(
				IPC_CLIENT_OBJECT_FACTORY_NAME);
		if (instance == null) {
            location.errorT(category,
                "entry ["
                    + IPC_CLIENT_OBJECT_FACTORY_NAME
                    + "] missing in factory-config.xml");            
		}
		return instance;
    }

   
    /**
     * Creates a new BaseGroup.
     * The base group is the group, containing all the characteristics that have not
     * been assigned to other groups.
     * @param instance Current instance.
     * @param characteristics List of characteristics that should be included in this group.
     * @param position Position of the group.
     * @return BaseGroup object.
     */
    public BaseGroup newBaseGroup(
        Instance instance,
        List characteristics,
        int position) {
        return new BaseGroup(instance, characteristics, position);
    }

    /**
     * Creates a characteristic group.
     * @param instance Current instance.
     * @param name Internal name of the group.
     * @param languageDependentName Language dependent name of the group. 
     * @param characteristics List of characteristics that should be included in this group.
     * @param position Position of the group.
     * @return DefaultCharacteristicGroup object.
     */
    public DefaultCharacteristicGroup newCharacteristicGroup(
        Instance instance,
        String name,
        String languageDependentName,
        List characteristics,
        int position) {
        return new DefaultCharacteristicGroup(
            instance,
            name,
            languageDependentName,
            characteristics,
            position);
    }

    /**
     * Create a new characteristic.
     * @param configurationChangeStream Container for configuration changes.
     * @param ipcClient IPCClient (Facade to the set of IPC objects)
     * @param instance Instance of the characteristic to be created.
     * @param groupName Group that the characteristic belongs to.
     * @param languageDependentName Language dependent name of characteristic.
     * @param name Internal name of characteristic.
     * @param unit Unit of measurement (language independent).
     * @param entryFieldMask The template for entered values (if necessary), e.g. ___.__
     * @param typeLength Length (in characters) a value of this characteristic may not exceed.
     * @param numberScale Numeric scale of this cstic.
     * @param valueType Basic type of the characteristic value as a number as declared in com.sap.sce.front.base.SceConstants
     * @param allowsAdditionalValues Set to true if additional values are allowed.
     * @param visible Set to true if the characteristic is visible
     * @param allowsMultipleValues Set to true if the characteristic allows assignment of multiple values.
     * @param required Set to true if the characteristic must be evaluated.
     * @param expanded Set to true if the characteristic is to be displayed in "expanded" mode.
     * @param consistent Set to true if the characteristic is consistent.
     * @param isDomainConstrained Set to true if the domain is constrained.
     * @param hasIntervalAsDomain Set to true if the domain of the characteristic may contain an interval (1 - 10).
     * @param readonly Set to true if the characteristic can only be read but cannot be changed
     * @param pricingRelevant Set to true if the characteristic is pricing relevant.
     * @param description The description of a chartacteristic
     * @return characteristic object (depending on scenario: RfcDefaultCharacteristic or TcpDefaultCharacteristic) 
     */
    public abstract DefaultCharacteristic newCharacteristic(
        ConfigurationChangeStream configurationChangeStream,
        IPCClient ipcClient,
        Instance instance,
        String groupName,
        String languageDependentName,
        String name,
        String unit,
        String entryFieldMask,
        int typeLength,
        int numberScale,
        int valueType,
        boolean allowsAdditionalValues,
        boolean visible,
        boolean allowsMultipleValues,
        boolean required,
        boolean expanded,
        boolean consistent,
        boolean isDomainConstrained,
        boolean hasIntervalAsDomain,
        boolean readonly,
        boolean pricingRelevant,
        String description);


    /**
     * Create a new characteristic.
     * @param configurationChangeStream Container for configuration changes.
     * @param ipcClient IPCClient (Facade to the set of IPC objects)
     * @param instance Instance of the characteristic to be created.
     * @param groupName Group that the characteristic belongs to.
     * @param languageDependentName Language dependent name of characteristic.
     * @param name Internal name of characteristic.
     * @param unit Unit of measurement (language independent).
     * @param entryFieldMask The template for entered values (if necessary), e.g. ___.__
     * @param typeLength Length (in characters) a value of this characteristic may not exceed.
     * @param numberScale Numeric scale of this cstic.
     * @param valueType Basic type of the characteristic value as a number as declared in com.sap.sce.front.base.SceConstants
     * @param allowsAdditionalValues Set to true if additional values are allowed.
     * @param visible Set to true if the characteristic is visible
     * @param allowsMultipleValues Set to true if the characteristic allows assignment of multiple values.
     * @param required Set to true if the characteristic must be evaluated.
     * @param expanded Set to true if the characteristic is to be displayed in "expanded" mode.
     * @param consistent Set to true if the characteristic is consistent.
     * @param isDomainConstrained Set to true if the domain is constrained.
     * @param hasIntervalAsDomain Set to true if the domain of the characteristic may contain an interval (1 - 10).
     * @param readonly Set to true if the characteristic can only be read but cannot be changed
     * @param pricingRelevant Set to true if the characteristic is pricing relevant.
     * @param description The description of a chartacteristic
     * @param applicationViews The application views maintained for this characteristic concatenated as String (e.g. "BPT")
     * @param csticMimeLNames Array of all internal names of Mimes for characteristics (e.g. "WP_SCREEN"). The name determines to which characteristic the mime-object belongs. 
     * @param csticMimeObjectTypes Array of all Mime-types of Mimes for characteristics (e.g. "Image"). 
     * @param csticMimeObjects Array of all Mime-Objects (i.e. the file names like "myImage.gif") of Mimes for characteristics.
     * @param index Index for processing the arrays of Mime-Objects more efficiently.
     * @return characteristic object (depending on scenario: RfcDefaultCharacteristic or TcpDefaultCharacteristic)
     */
    public abstract DefaultCharacteristic newCharacteristic(
        ConfigurationChangeStream configurationChangeStream,
        IPCClient ipcClient,
        Instance instance,
        String groupName,
        String languageDependentName,
        String name,
        String unit,
        String entryFieldMask,
        int typeLength,
        int numberScale,
        int valueType,
        boolean allowsAdditionalValues,
        boolean visible,
        boolean allowsMultipleValues,
        boolean required,
        boolean expanded,
        boolean consistent,
        boolean isDomainConstrained,
        boolean hasIntervalAsDomain,
        boolean readonly,
        boolean pricingRelevant,
        String description,
        String applicationViews,
        String[] csticMimeLNames,
        String[] csticMimeObjectTypes,
        String[] csticMimeObjects,
        int[] index);

    /**
     * Creates a new characteristic value.
     * @param configurationChangeStream Container for configuration changes.
     * @param ipcClient IPCClient (Facade to the set of IPC objects)
     * @param characteristic Characteristic to which the value belongs to.
     * @param name Internal name of the value.
     * @param languageDependentName Language dependent name of the value.
     * @param description Description (long-text) of the value.
     * @param conditionKey  Condition Key of the value.
     * @param price Price of the value.
     * @param index Index for processing the arrays of Mime-Objects more efficiently.
     * @param mimeValueLNames Array of all internal names of Mimes for values. The name determines to which value the mime-object belongs. The name is build up of the characteristic and value name  (e.g. "WP_SCREEN.001")
     * @param mimeObjectTypes Array of all Mime-types of Mimes for values (e.g. "Image").
     * @param mimeObjects Array of all Mime-Objects (i.e. the file names like "myImage.gif") of Mimes for values.
     * @return value object (depending on scenario: RfcDefaultCharacteristicValue or TcpDefaultCharacteristicValue) 
     */
    public abstract DefaultCharacteristicValue newCharacteristicValue(
        ConfigurationChangeStream configurationChangeStream,
        IPCClient ipcClient,
        Characteristic characteristic,
        String name,
        String languageDependentName,
        String description,
        String conditionKey,
        String price,
        int[] index,
        String[] mimeValueLNames,
        String[] mimeObjectTypes,
        String[] mimeObjects); 

    /**
     * Creates a new characteristic value.
     * @param configurationChangeStream Container for configuration changes.
     * @param ipcClient IPCClient (Facade to the set of IPC objects)
     * @param characteristic Characteristic to which the value belongs to.
     * @param name Internal name of the value.
     * @param languageDependentName Language dependent name of the value.
     * @param description Description (long-text) of the value.
     * @param conditionKey  Condition Key of the value.
     * @param price Price of the value.
     * @param index Index for processing the arrays of Mime-Objects more efficiently.
     * @param mimeValueLNames Array of all internal names of Mimes for values. The name determines to which value the mime-object belongs. The name is build up of the characteristic and value name  (e.g. "WP_SCREEN.001")
     * @param mimeObjectTypes Array of all Mime-types of Mimes for values (e.g. "Image").
     * @param mimeObjects Array of all Mime-Objects (i.e. the file names like "myImage.gif") of Mimes for values.
     * @param defaultValue should be set to true if the value is the default value
     * @return value object (depending on scenario: RfcDefaultCharacteristicValue or TcpDefaultCharacteristicValue) 
     */
    public abstract DefaultCharacteristicValue newCharacteristicValue(
        ConfigurationChangeStream configurationChangeStream,
        IPCClient ipcClient,
        Characteristic characteristic,
        String name,
        String languageDependentName,
        String description,
        String conditionKey,
        String price,
        int[] index,
        String[] mimeValueLNames,
        String[] mimeObjectTypes,
        String[] mimeObjects,
        boolean defaultValue); 

    /**
     * Creates a new characteristic value.
     * @param configurationChangeStream Container for configuration changes.
     * @param ipcClient IPCClient (Facade to the set of IPC objects)
     * @param characteristic Characteristic to which the value belongs to.
     * @param name Internal name of the value.
     * @param languageDependentName Language dependent name of the value.
     * @param description Description (long-text) of the value.
     * @param conditionKey  Condition Key of the value.
     * @param price Price of the value.
     * @param isAdditionalValue Set to true if this value is an additional value (entered by the user).
     * @return value object (depending on scenario: RfcDefaultCharacteristicValue or TcpDefaultCharacteristicValue) 
     */
    public abstract DefaultCharacteristicValue newCharacteristicValue( 
        ConfigurationChangeStream configurationChangeStream,
        IPCClient ipcClient,
        Characteristic characteristic,
        String name,
        String languageDependentName,
        String description,
        String conditionKey,
        String price,
        boolean isAdditionalValue);
    
    /**
     * Creates a client-side configuration for an existing configuration on the server.
     * @param session  the IPCSession to create the configuration in.
     * @param configId the configuration id of the configuration to use. A configuration of this id has to be present in
     * at server in this session.
     * @return a new configuration object.
     */
    public abstract DefaultConfiguration newConfiguration(
        DefaultIPCSession session,
        String configId)
        throws IPCException;

    /**
     * Creates a client-side configuration for an existing configuration on the server.
     * @param ipcClient IPCClient (Facade to the set of IPC objects)
     * @param id The configuration id of the configuration to use. A configuration of this id has to be present in
     * at server in this session.
     * @return a new configuration object.
     */
    public abstract DefaultConfiguration newConfiguration(IPCClient ipcClient, String id);
    
    /**
     * Creates a client-side configuration for an imported configuration.
     * @param session the IPCSession to create the configuration in.
     * @param configId the configuration id of the configuration to use. A configuration of this id has to be present in
     * at server in this session.
     * @return a new ImportConfiguration object.
     * @throws IPCException
     */
    public DefaultImportConfiguration newImportConfiguration (DefaultIPCSession session,
        String configId)
        throws IPCException {
        return new DefaultImportConfiguration (session, configId);
    }
    
    /**
     * Creates a client-side configuration for an imported configuration.
     * @param ipcClient IPCClient (Facade to the set of IPC objects)
     * @param idd the configuration id of the configuration to use. A configuration of this id has to be present in
     * at server in this session.
     * @return a new ImportConfiguration object.
     */
    public DefaultImportConfiguration newImportConfiguration(IPCClient ipcClient, String id) {
        return new DefaultImportConfiguration (ipcClient,id);
    }

    /**
     * Creates a new conflict object. This object represents a conflict and stores information 
     * on the conflict like reasons, etc.
     * @param ipcClient IPCClient (Facade to the set of IPC objects)
     * @param explanationTool Object of current explanation tool (proxy for conflict explanation on
     * server side. 
     * @param id Conflict Id.
     * @return Conflict object.
     */
    public DefaultConflict newConflict(
        IPCClient ipcClient,
        ExplanationTool explanationTool,
        String id) {
        return new DefaultConflict(ipcClient, explanationTool, id);
    }

    /**
     * Create an instance of the Explanation Tool. The Explanation tool is a proxy for conflict 
     * explanation on server side.
     * @param ipcClient IPCClient (Facade to the set of IPC objects)
     * @param configuration Configuration object for which the explanation tool should be used.
     * @return An object of the explanation tool.
     */
    public abstract DefaultExplanationTool newExplanationTool(
        IPCClient ipcClient,
        Configuration configuration);

    /**
     * Creates an object of an instance.
     * An instance is the main object being created and customized in a configuration. It 
     * holds a set of characteristics (cstics) and possibly children instances (part instances).
     * @param configurationChangeStream Container for configuration changes.
     * @param ipcClient IPCClient (Facade to the set of IPC objects)
     * @param configuration Configuration object.
     * @param instanceId Id for the instance.
     * @param parentId Id of the parent instance.
     * @param complete Flag: Is instance complete?
     * @param consistent Falg: Is instance consistent?
     * @param quantity Quantity of the instance.
     * @param name Internal name of the instance.
     * @param languageDependentName Language dependent name of the instance.
     * @param description Description (long-text) of the instance.
     * @param bomPosition Position of this instance in the Bill of Materials of its parent
     * @param price Price of this instance.
     * @param userOwned Flag: is the author of this instance the user? 
     * @param configurable Flag: is this instance configurabel.
     * @param specializable Flag: is this instance specializable?
     * @param unspecializable Flag: is this instance unspecializable?
     * @param mimeObjects MimeObjectContainer object that contains all mime-objects for this instance.
     * @return A new instance object.
     */
    public abstract DefaultInstance newInstance(
        ConfigurationChangeStream configurationChangeStream,
        IPCClient ipcClient,
        DefaultConfiguration configuration,
        String instanceId,
        String parentId,
        boolean complete,
        boolean consistent,
        String quantity,
        String name,
        String languageDependentName,
        String description,
        String bomPosition,
        String price,
        boolean userOwned,
        boolean configurable,
        boolean specializable,
        boolean unspecializable,
        MimeObjectContainer mimeObjects);
        
    /**
     * Creates a new document on the server using the given properties and will return the same object.
     * @param   session the IPCSession to create the document in.
     * @param   attr    the document properties to use to initialize the document.
     * @return  a new document object.
     */
    public abstract DefaultIPCDocument newIPCDocument(
        DefaultIPCSession session,
        DefaultIPCDocumentProperties attr)
        throws IPCException;
	

    /**
     * Creates a client-side document for an existing document on the server.
     * @param session the IPCSession to create the document in.
     * @param attr    the document id of the document to use. A document of this id has to be present in
     * at server in this session.
     * @return a new document object.
     */
    public abstract DefaultIPCDocument newIPCDocument(
        DefaultIPCSession session,
        String documentId)
        throws IPCException;

	/**
	 * Creates a client-side document properties object.
	 * @return a new document properties object
	 */
    public abstract DefaultIPCDocumentProperties newIPCDocumentProperties();
    
	/**
	* Creates a new client-side IPCItemProperties object as clone of initialData.
	* @param initialData
	* @return a new document properties object
	* @throws IPCException
	*/
	public abstract DefaultIPCDocumentProperties newIPCDocumentProperties(IPCDocumentProperties initialData);

	/**
	 * Creates a client-side item for an existing item on the server. 
     * @param   client the IClient to create the item.
     * @param   document the document to create the item in.
	 * @param   itemId the item id of the item to use. An item of this id has to be present in
     * at server in this document.
	 * @return a new item object
	 * @throws IPCException
	 */
    public abstract DefaultIPCItem newIPCItem(IClient client, DefaultIPCDocument document, String itemId) throws IPCException;

	/**
	 * Creates a client-side subitem for an existing subitem on the server. 
     * @param   client the IClient to create the item.
     * @param   document the document to create the item in.
	 * @param   itemId the item id of the subitem to use. An item of this id has to be present in
     * at server in this document.
	 * @param parentItemId the paretnItemId to which it shold have a link as sub item. 
	 * This paretnItemd should be found on the server.
	 * @return a new item object
	 * @throws IPCException
	 */
    public abstract DefaultIPCItem newIPCItem(IClient client, DefaultIPCDocument document, String itemId, String parentItemId) throws IPCException;

    /**
     *  Creates a new item on the server in a given document.<p>
     *  @param  properties  the item properties to use to initialize the item.
     * The new Item holds a reference to the passed properties. Clients of this
     * method would need to create a new IPCItemProperties object, if each item
     * should have its own IPCItemProperties.
     *  @return the new item.
     */
    public abstract IPCItem newIPCItem(
        DefaultIPCDocument document,
        IPCItemProperties properties)
        throws IPCException;
    
	/**
	 * Creates a new client-side ItemFilter
	 * @return new item filter object
	 */
    public DefaultIPCItemFilter newIPCItemFilter() {
        return new DefaultIPCItemFilter();
    }
    
	/**
	 * Creates a client-side item properties object.
	 * @return a new item properties object.
	 */
    public abstract DefaultIPCItemProperties newIPCItemProperties();
    
    /**
     * Creates a new client-side IPCItemProperties object as clone of initialData.
     * @param initialData
     * @return a new item properties object
     * @throws IPCException
     */
    public abstract DefaultIPCItemProperties newIPCItemProperties(IPCItemProperties initialData) throws IPCException;

    /**
     * Creates several items on the server.
     * @return an array of new item objects.
     */
    public abstract IPCItem[] newIPCItems(
        IPCDocument document,
        IPCItemProperties[] properties)
        throws IPCException;

	/**
     * Creates a client-side session that attaches to the session that is specified by reference.
     * Only host, port, encoding and sessionId of the IPCConfigReference are used. All configs
     * in the server session are constructed.
     *  
	 * Note: Not supported by RFC-implementation.
	 */
    public abstract DefaultIPCSession newIPCSession(DefaultIPCClient ipcClient, IPCConfigReference reference) throws IPCException;


    /**
     * Creates a client-side session that attaches to the session that is specified by reference.
     * Only host, port, encoding and sessionId of the IPCItemReference are used. All configs
     * in the server session are constructed.
     * 
     * Note: Not supported by RFC-implementation.
     * 
     */
    public abstract DefaultIPCSession newIPCSession(
        DefaultIPCClient ipcClient,
        IPCConfigReference reference,
        boolean isExtendedData)
        throws IPCException;

    /**
     * Creates a client-side session that attaches to the session that is specified by reference.
     * Only host, port, encoding and sessionId of the IPCItemReference are used. All documents,
     * items and such in the server session are constructed.
     * 
     * Note: Not supported by RFC-implementation.
     * 
     */
    public abstract DefaultIPCSession newIPCSession(
        DefaultIPCClient ipcClient,
        IPCItemReference reference,
        boolean isExtendedData)
        throws IPCException;

    /**
	 * Creates a client-side session that attaches to the session that is specified by reference.
	 * Only host, port, encoding and sessionId of the IPCItemReference are used. All documents,
	 * items and such in the server session are constructed.
     * 
     * @param ipcClient   the IPCClient to which this session belongs.
     * @param reference The IPC item reference that keeps the connection parameters. null => read from property file
     * @return A new IPCSession object.
     * @throws IPCException
	 */
	public abstract DefaultIPCSession newIPCSession(
		DefaultIPCClient ipcClient,
		IPCItemReference reference)
		throws IPCException;


    /**
     * Convenience method:
     * Creates an IPC session for SAP client mandt. Will throw an exception if
     * initializing the session fails. The connection data of the server to connect to are
     * retrieved from the property file client.properties.
     * @param   ipcClient   the IPCClient to which this session belongs.
     * @param   mandt       the SAP client to use
     * @param   type        the session type (eg. "ISA" or "CRM")
     * <p>
     * Please note since all IPC server communication is encapsulated in objects, client-specific
     * parameter naming is not supported by this API.<p>
     *
     * This method just calls the extended version with null item reference and CachingClient. Typically, you don't
     * need to override it.
     * 
     * Note: Not supported by RFC-implementation.
     * 
     */
    public abstract DefaultIPCSession newIPCSession(
        DefaultIPCClient ipcClient,
        String mandt,
        String type)
        throws IPCException;

    /**
     * Creates a new, empty IPC session for SAP client mandt. Will throw an exception if
     * initializing the session fails.<p>
     *
     * If a CachingClient is passed to this session, this client will be used. Otherwise,
     * if a config reference is passed, it will be used to initialize a client. If both
     * client and reference are null, the client will be initialized from a property file.<p>
     *
     * @param   ipcClient   the IPCClient to which this session belongs.
     * @param   mandt       the SAP client to use
     * @param   type        the session type (eg. "ISA" or "CRM")
     * @param   client      the IPC client to use. null => construct client from reference or property file
     * @param   ref         the IPC config reference that keeps the connection parameters. null => read from property file
     * <p>
     * Please note since all IPC server communication is encapsulated in objects, client-specific
     * parameter naming is not supported by this API.<p>
     * 
     * Note: Not supported by RFC-implementation.
     * 
     */
    public abstract DefaultIPCSession newIPCSession(
        DefaultIPCClient ipcClient,
        String mandt,
        String type,
        IClient client,
        IPCConfigReference ref)
        throws IPCException;


    /**
     * Creates a new, empty IPC session for SAP client mandt. Will throw an exception if
     * initializing the session fails.<p>
     *
     * If a CachingClient is passed to this session, this client will be used. Otherwise,
     * if a config reference is passed, it will be used to initialize a client. If both
     * client and reference are null, the client will be initialized from a property file.<p>
     *
     * @param   ipcClient   the IPCClient to which this session belongs.
     * @param   mandt       the SAP client to use
     * @param   type        the session type (eg. "ISA" or "CRM")
     * @param   client      the IPC client to use. null => construct client from reference or property file
     * @param   ref         the IPC config reference that keeps the connection parameters. null => read from property file
     * @param   securityProperites      security properties
     * <p>
     * Please note since all IPC server communication is encapsulated in objects, client-specific
     * parameter naming is not supported by this API.<p>
     * 
     * Note: Not supported by RFC-implementation.
     * 
     */
    public abstract DefaultIPCSession newIPCSession(
        DefaultIPCClient ipcClient,
        String mandt,
        String type,
        IClient client,
        IPCConfigReference ref,
        Properties securityProperties)
        throws IPCException;
    
    /**
     * Creates a new, empty IPC session for SAP client mandt. Will throw an exception if
     * initializing the session fails.<p>
     *
     * If a CachingClient is passed to this session, this client will be used. Otherwise,
     * if an item reference is passed, it will be used to initialize a client. If both
     * client and reference are null, the client will be initialized from a property file.<p>
     *
     * @param   ipcClient   the IPCClient to which this session belongs.
     * @param   mandt       the SAP client to use
     * @param   type        the session type (eg. "ISA" or "CRM")
     * @param   client      the IPC client to use. null => construct client from reference or property file
     * @param   ref         the IPC item reference that keeps the connection parameters. null => read from property file
     * <p>
     * Please note since all IPC server communication is encapsulated in objects, client-specific
     * parameter naming is not supported by this API.<p>
     * 
     * Note: Not supported by RFC-implementation.
     * 
     */
    public abstract DefaultIPCSession newIPCSession(
        DefaultIPCClient ipcClient,
        String mandt,
        String type,
        IClient client,
        IPCItemReference ref)
        throws IPCException;

    /**
     * Creates a new, empty IPC session for SAP client mandt. Will throw an exception if
     * initializing the session fails.<p>
     *
     * If a CachingClient is passed to this session, this client will be used. Otherwise,
     * if an item reference is passed, it will be used to initialize a client. If both
     * client and reference are null, the client will be initialized from a property file.<p>
     *
     * @param   ipcClient   the IPCClient to which this session belongs.
     * @param   mandt       the SAP client to use
     * @param   type        the session type (eg. "ISA" or "CRM")
     * @param   client      the IPC client to use. null => construct client from reference or property file
     * @param   ref         the IPC item reference that keeps the connection parameters. null => read from property file
     * <p>
     * Please note since all IPC server communication is encapsulated in objects, client-specific
     * parameter naming is not supported by this API.<p>
     * 
     * Note: Not supported by RFC-implementation.
     * 
     */
    public abstract DefaultIPCSession newIPCSession(
        DefaultIPCClient ipcClient,
        String mandt,
        String type,
        IClient client,
        IPCItemReference ref,
        Properties securityProperties)
        throws IPCException;

    /**
     * Creates an IPC session for SAP client mandt. Will throw an exception if
     * initializing the session fails. The server/dispatcher that will be contacted by this
     * session is defined in the item reference. Only host, port, encoding, dispather of this reference will be used.
     * @param   ipcClient   the IPCClient to which this session belongs.
     * @param   mandt       the SAP client to use
     * @param   type        the session type (eg. "ISA" or "CRM")
     * @param   ref         the IPC config reference that keeps the connection parameters. null => read from property file
     * <p>
     * Please note since all IPC server communication is encapsulated in objects, client-specific
     * parameter naming is not supported by this API.<p>
     *
     * This method just calls the extended version with null CachingClient. Typically, you don't
     * need to override it.
     * 
     * Note: Not supported by RFC-implementation.
     * 
     */
    public abstract DefaultIPCSession newIPCSession(
        DefaultIPCClient ipcClient,
        String mandt,
        String type,
        IPCConfigReference ref)
        throws IPCException;

    /**
     * Creates an IPC session for SAP client mandt. Will throw an exception if
     * initializing the session fails. The server/dispatcher that will be contacted by this
     * session is defined in the item reference. Only host, port, encoding, dispather of this reference will be used.
     * @param   ipcClient   the IPCClient to which this session belongs.
     * @param   mandt       the SAP client to use
     * @param   type        the session type (eg. "ISA" or "CRM")
     * @param   ref         the IPC item reference that keeps the connection parameters. null => read from property file
     * <p>
     * Please note since all IPC server communication is encapsulated in objects, client-specific
     * parameter naming is not supported by this API.<p>
     *
     * This method just calls the extended version with null CachingClient. Typically, you don't
     * need to override it.
     * 
     * Note: Not supported by RFC-implementation.
     * 
     */
    public abstract DefaultIPCSession newIPCSession(
        DefaultIPCClient ipcClient,
        String mandt,
        String type,
        IPCItemReference ref)
        throws IPCException;
        	
    /**
     * Creates a new Knowledgebase object.
     * @param ipcClient The IPCClient to which this session belongs.
     * @param logsys The logical system.
     * @param name Name of the knowledgebase.
     * @param version Version of the knowledgebase.
     * @param buildNumber Build Number of the knowledgebase
     * @return A knowledgebase object.
     */
    public DefaultKnowledgeBase newKnowledgeBase(
        IPCClient ipcClient,
        String logsys,
        String name,
        String version,
        String buildNumber) {
        return new DefaultKnowledgeBase(
            ipcClient,
            logsys,
            name,
            version,
            buildNumber);
    }
    
    /**
     * Creates a new mime-object (representing an image or document).
     * Use this method to build a mime object from an URLEncoded String.
     * @param index The index of the Mime-object in the URLEncoded String for which an 
     * object should be created. 
     * @param queryStr The URLEncoded String.
     * @return A mime object.
     */
    public DefaultMimeObject newMimeObject(int index,String queryStr) {
        return new DefaultMimeObject(index, queryStr);
    }
    
    /**
     * Creates a new mime-object (representing an image or document).
     * Use this method if you already know the mime type.
     * @param url The URL.
     * @param type The mime-type.
     * @return A mime object.
     */
    public DefaultMimeObject newMimeObject(String url,String type) {
        return new DefaultMimeObject(url, type);
    }
    
    /**
     * Creates a new MimeObjectContainer.
     * This container is able to filter mime objects by their type.
     * @return A MimeObjectContainer.
     */
    public DefaultMimeObjectContainer newMimeObjectContainer() {
        return new DefaultMimeObjectContainer();
    }
    
    /**
     * Creates a new MimeObjectContainer.
     * This container is able to filter mime objects by their type.
     * Use this method, if you already know the numer of mime objects
     * @param initialSize Initial number of mime-objects.
     * @return A MimeObjectContainer.
     */
    public DefaultMimeObjectContainer newMimeObjectContainer(int initialSize) {
        return new DefaultMimeObjectContainer(initialSize);
    }
    
    /**
     * Creates a ProductVariant object.
     * @param ipcClient IPCClient (Facade to the set of IPC objects)
     * @param id Product Id of the variant.
     * @param distance Distance of the variant (0..infinity)
     * @param matchPoints How good each match is (0..maxMatchPoint, derived from distance).
     * @param maxMatchPoints The scale end.
     * @param features List of features.
     * @param mainDifference Main difference (product variant feature).
     * @return A ProductVariant object.
     */
    public abstract DefaultProductVariant newProductVariant(IPCClient ipcClient,
        String id,
        double distance,
        String matchPoints,
        String maxMatchPoints,
        List features,
        ProductVariantFeature mainDifference);

    
    /**
     * Creates a ProductKey object.
     * @return A ProductKey object.
     */
    public DefaultProductKey newProductKey(String productId,
        String productType,
        String productLogSys) {
        return new DefaultProductKey(productId, productType, productLogSys);
    }    
    
    /**
     * Creates a ProductVariantFeature object.
     * @param ipcClient IPCClient (Facade to the set of IPC objects)
     * @param productVariant The ProductVariant for this feature.
     * @param type Type of the feature: what kind of feature, currently only: CSTIC.
     * @param id String identifying the feature (eg. "KBCA_A0001.KBCA_TECHNOLOGY").
     * @param name Internal name of the feature.
     * @param lname Language dependent name of the feature.
     * @param longtext Long-text of the feature.
     * @param value The value of the feature for that product variant
     * @param lvalue The language-dependent value of the feature for that product variant.
     * @param weight Feature weight.
     * @param distance Feature distance.
     * @return A ProductVariantFeature object.
     */
    public DefaultProductVariantFeature newProductVariantFeature(
        IPCClient ipcClient,
        ProductVariant productVariant,
        String type,
        String id,
        String name,
        String lname,
        String longtext,
        String value,
        String lvalue,
        double weight,
        double distance) {
        return new DefaultProductVariantFeature(
            ipcClient,
            productVariant,
            type,
            id,
            name,
            lname,
            longtext,
            value,
            lvalue,
            weight,
            distance);
    }

    /**
     * Creates a new IPCClient.
	 * @param clientType Valid values are IPCClientObjectFactory.TCP_CLIENT or IPCClientObjectFactory.RFC_CLIENT
	 * @param   mandt       the SAP client to use
	 * @param   type        the session type (eg. "ISA" or "CRM")
     * @return IPCClient object.
     * @throws IPCException
     * 
     * Note: Not supported by RFC-implementation.
     * 
	 */
	public abstract IPCClient newIPCClient(String mandt, String type) throws IPCException;
    
	/**
     * Creates a new IPCClient.
     * @param sessionId Session Id
     * @return IPCClient object
     * @throws IPCException
     * 
     * Note: Not supported by RFC-implementation.
     * 
     */
	public abstract IPCClient newIPCClient(String sessionId) throws IPCException;


	/**
	 * Creates an IPC client without any connection to server.
     * @return IPCClient object
     * @throws IPCException
	 */
	public abstract IPCClient newIPCClient() throws IPCException;

	/**
     * Creates a new IPCClient.
	 * @param mandt the SAP client to use
	 * @param string the session type (eg. "ISA" or "CRM")
	 * @param cachingClient Caching Client object.
	 * @return IPCClient object.
     * 
     * Note: Not supported by RFC-implementation.
     * 
	 */
	public abstract DefaultIPCClient newIPCClient(String mandt, String string, IClient cachingClient)throws IPCException;

	/**
	 * Creates a new IPCClient.
     * @param mandt the SAP client to use
     * @param type the session type (eg. "ISA" or "CRM")
	 * @param reference IPCItemReference
	 * @param securityProperties Security Properties
	 * @return IPCClient object.
     * 
     * Note: Not supported by RFC-implementation.
     * 
	 */
	public abstract IPCClient newIPCClient(String mandt, String type, IPCItemReference reference, Properties securityProperties);

    /**
     * Creates a new IPCClient.
     * @param mandt the SAP client to use
     * @param type the session type (eg. "ISA" or "CRM")
     * @param reference IPCItemReference
     * @return IPCClient object.
     */
    public abstract IPCClient newIPCClient(String mandt, String type, IPCItemReference reference);

	/**
     * Creates a new IPCClient.
     * @param mandt the SAP client to use
     * @param type the session type (eg. "ISA" or "CRM")
     * @param reference IPCConfigReference
     * @return IPCClient object.
     * 
     * Note: Not supported by RFC-implementation.
     * 
	 */
	public abstract IPCClient newIPCClient(String mandt, String type, IPCConfigReference reference);

	/**
     * Creates a new IPCClient.
     * @param mandt the SAP client to use
     * @param type the session type (eg. "ISA" or "CRM")
     * @param reference IPCConfigReference
     * @param securityProperties Security Properties
     * @return IPCClient object.
     * 
     * Note: Not supported by RFC-implementation.
     * 
	 */
	public abstract IPCClient newIPCClient(String mandt, String type, IPCConfigReference reference, Properties securityProperties);


	/**
	 * Creates a new IPCClient.
	 * @param monitor IMonitor object.
     * @return IPCClient object.
     * 
     * Note: Not supported by RFC-implementation.
     * 
	 */
	public abstract IPCClient newIPCClient(IMonitor monitor);

	/**
     * Creates a new IPCClient.
     * @param mandt the SAP client to use
     * @param type the session type (eg. "ISA" or "CRM")
	 * @param monitor IMonitor object.
	 * @return IPCClient object.
     * 
     * Note: Not supported by RFC-implementation.
     * 
	 */
	public abstract IPCClient newIPCClient(String mandt, String type, IMonitor monitor);

	/**
     * Creates a new IPCClient.
	 * @param sessionId Session ID.
	 * @param monitor IMonitor object.
	 * @return IPCClient object.
     * 
     * Note: Not supported by RFC-implementation.
     * 
	 */
	public abstract IPCClient newIPCClient(String sessionId, IMonitor monitor);

	/**
     * Creates a new IPCClient.
     * @param mandt the SAP client to use
     * @param type the session type (eg. "ISA" or "CRM")
     * @param reference IPCItemReference.
     * @param monitor IMonitor object.
     * @return IPCClient object.
	 */
	public abstract IPCClient newIPCClient(String mandt, String type, IPCItemReference reference, IMonitor monitor);

	/**
     * Creates a new IPCClient.
     * @param mandt the SAP client to use
     * @param type the session type (eg. "ISA" or "CRM")
     * @param reference IPCConfigReference.
     * @param monitor IMonitor object.
     * @return IPCClient object.
	 */
	public abstract IPCClient newIPCClient(String mandt, String type, IPCConfigReference reference, IMonitor monitor);

    /**
     * Creates a ConfigValue. A ConfigValue is a value that encapsulates an external configuration.
     * @return A ConfigValue object.
     */
    public abstract ConfigValue newConfigValue();

    /**
     * Creates a ConfigValue. A ConfigValue is a value that encapsulates an external configuration.
     * Uses the passed external configuration.
     * @param config External configuration.
     * @return  A ConfigValue object for the given external configuration.
     */
    public abstract ConfigValue newConfigValue(ext_configuration config);

    /**
     * Create a configuration change stream (Container for configuration changes).
     * @param configuration The configuration.
     * @param cachingClient Caching client.
     * @return ConfigurationChangeStream
     */
    public abstract ConfigurationChangeStream newConfigurationChangeStream (Configuration configuration, 
        IClient cachingClient);
        
    /**
     * Creates a new conflict participant (reason) for a conflict.
     * @param ipcClient IPCClient (Facade to the set of IPC objects)
     * @param conflict The conflict to which the participant (reason) belongs to.
     * @param id Id of the participant.
     * @param factKey Fact key.
     * @param causedByUser Flag: is this participant (reason) caused by user?
     * @param causedByDefault Flag: is this participant (reason) caused by default?
     * @param locale Locale.
     * @param resources MessageRessources
     * @return DefaultConflictParticipant
     */
    public abstract DefaultConflictParticipant newConflictParticipant(IPCClient ipcClient,
        Conflict conflict,
        String id,
        String factKey,
        boolean causedByUser,
        boolean causedByDefault,
        Locale locale,
        MessageResources resources);
        
    /**
     * Creates a new knowledgebase profile.
     * @param name Name of the profile.
     * @param kb Knowledgebase.
     * @return A KBProfile object.
     */
    public abstract DefaultKBProfile newKBProfile(String name, KnowledgeBase kb);
    
    /**
     * Creates an IPCConfigReference.
     * An IPCConfigReference knows everything needed to connect to a remote IPC server session,
     * and retrieve an Config from that session.
     * @return An IPCConfigReference.
     */
    public abstract IPCConfigReference newIPCConfigReference();
    
    /**
     * Creates an IPCItemReference.
     * An IPCItemReference knows everything needed to connect to a remote IPC server session,
     * and retrieve an item from that session.
     * @return An IPCItemReference.
     */
    public abstract IPCItemReference newIPCItemReference();


	/**
     * Creates a new IPCSession using the given BackendBusinessObject.
	 * @param client IPCClient (Facade to the set of IPC objects)
	 * @param backendObject BackendBusinessObject
	 * @return A new IPCSession.
	 */
	public abstract DefaultIPCSession newIPCSession(IPCClient ipcClient, BackendBusinessObject backendObject);


	/**
     * Creates a new IPCSession using the given BackendBusinessObject.
     * @param client IPCClient (Facade to the set of IPC objects)
     * @param backendObject BackendBusinessObject
     * @param monitor IMonitor object.
     * @return A new IPCSession.
	 */
	public abstract DefaultIPCSession newIPCSession(IPCClient client, BackendBusinessObject backendObject, IMonitor monitor);

	/**
     * Creates a new IPCClient using the given BackendBusinessObject.
     * @param backendObject BackendBusinessObject
     * @param monitor IMonitor object.
     * @return A new IPCClient.
	 */
	public abstract IPCClient newIPCClient(BackendBusinessObject backendObject, IMonitor _monitor);


	/**
     * Creates a new IPCClient using the given BackendBusinessObject.
     * @param backendObject BackendBusinessObject
     * @return A new IPCClient.
	 */
	public abstract IPCClient newIPCClient(BackendBusinessObject backendObject);


	/**
     * Creates a new IPCClient using the given BackendBusinessObject and the tokenId.
     * @param rfcClientSupport BackendBusinessObject
     * @param _monitor IMonitor object.
     * @param tokenId
     * @return A new IPCClient.
	 */
	public abstract IPCClient newIPCClient(BackendBusinessObject rfcClientSupport, IMonitor monitor, String tokenId, String configId);

	/**
     * Creates a new IPCClient using the given BackendBusinessObject and the tokenId.
     * @param rfcClientSupport BackendBusinessObject
     * @param tokenId
     * @return A new IPCClient.
	 */
	public abstract IPCClient newIPCClient(BackendBusinessObject rfcClientSupport, String tokenId, String configId);


	/**
     * Creates a new IPCClient using the given BackendBusinessObject and the tokenId.
     * @param client IPCClient (Facade to the set of IPC objects)
     * @param rfcClientSupport BackendBusinessObject
     * @param tokenId
     * @return A new IPCClient.
	 */
	public abstract DefaultIPCSession newIPCSession(RfcDefaultIPCClient client, BackendBusinessObject rfcClientSupport, String tokenId, String configId);


	/**
     * Creates a new IPCClient using the given BackendBusinessObject and the tokenId.
     * @param client IPCClient (Facade to the set of IPC objects)
     * @param rfcClientSupport BackendBusinessObject
     * @param monitor IMonitor object.
     * @param tokenId
     * @return A new IPCClient.
	 */
	public abstract DefaultIPCSession newIPCSession(RfcDefaultIPCClient client, BackendBusinessObject rfcClientSupport, IMonitor monitor, String tokenId, String configId);
    
    /**
     * Creates a new LoadingMessage object that represents a message that occured during
     * the loading process of the configuration.
     * @param configObjKey configuration object key this message belongs to
     * @param number message number
     * @param severity messages severity
     * @param text message text
     * @param arguments array of message arguments
     * @return a new LoadingMessage object.
     */
    public DefaultLoadingMessage newLoadingMessage(String instId, String csticName, int number, String messageClass, int severity, String text, String[] arguments){
        return new DefaultLoadingMessage(instId, csticName, number, messageClass, severity, text, arguments);
    }

    /**
     * Creates a new TTEDocument using the IPCDocument.
     * @param session
     * @param document
     * @param createDocumentOnServer
     * @return A new TTEDocument
     */
    public abstract DefaultTTEDocument newTTEDocument(DefaultIPCSession session, IPCDocument document, boolean createDocumentOnServer);

    /**
     * Creates a new TTEItem.
     * @param itemId
     * @param itemProps
     * @param docProps
     * @param tteDoc
     * @param createItemOnServer
     * @return A new TTEItem.
     */
    public abstract DefaultTTEItem newTTEItem(  String itemId,  
                            IPCItemProperties itemProps, 
                            IPCDocumentProperties docProps, 
                            TTEDocument tteDoc, 
                            boolean createItemOnServer);
                                
    /**
     * Creates new LoadingStatus object.
     * @param client IPCClient 
     * @param config Configuration to which the <code>LoadingStatus</code> belongs to.
     * @return
     */
    public abstract DefaultLoadingStatus newLoadingStatus(IPCClient client, Configuration config);
    
    /**
     * Creates new Delta object.
     * @param fieldName name of the field, where the delta occurs
     * @param val1 the old value of the field
     * @param val2 the new value of the field
     */
    public DefaultDelta newDelta(String fieldName, String val1, String val2) {
        return new DefaultDelta(fieldName, val1, val2);
    }
    
    /**
     * Creates a new ValueDelta object.
     * @param configObjectKey unique key of the configuration object to this value delta belongs to
     * @param csticName name of the characteristic the value delta belongs to
     * @param value the value of the value delta
     * @param unit the unit of the value 
     * @param author author of the delta
     * @param type type of delta (D, I)
     */
    public DefaultValueDelta newValueDelta(String configObjectKey, String csticName, String value, String unit, String author, String type) {
        return new DefaultValueDelta(configObjectKey, csticName, value, unit, author, type);
    }

    /**
     * Creates a new InstanceDelta object.
     * @param id1 id of instance 1
     * @param id2 id of instance 2
     * @param itemId item id (if available)
     * @param decompPath decomposition path
     * @param deltas list of deltas on instance level
     * @param partOfDeltas list of part of deltas
     * @param valueDeltas list of value deltas
     * @param author author of the instance delta
     * @param type type of delta (D, I, U)
     */
    public DefaultInstanceDelta newInstanceDelta(String id1, 
                                String id2, 
                                String itemId, 
                                String decompPath, 
                                List deltas, 
                                List partOfDeltas,
                                List valueDeltas,
                                String author,
                                String type) {
        return new DefaultInstanceDelta(id1, 
                                id2, 
                                itemId, 
                                decompPath, 
                                deltas, 
                                partOfDeltas,
                                valueDeltas,
                                author,
                                type);
    }

    /**
     * Creates a new ComparisonResult object.
     * @param headerDeltas list of header deltas
     * @param instanceDeltas list of instance delta objects
     */
    public DefaultComparisonResult newComparisonResult( List headerDeltas,
                                    List instanceDeltas) {
        return new DefaultComparisonResult(headerDeltas, instanceDeltas);
    }

    /**
     * Creates a new ComparisonResult object.
     * @param configOne configuration one that has been compared to external configuration two
     * @param configTwo configuration two that has been compared to external configuration one
     * @param headerDeltas list of header deltas
     * @param instanceDeltas list of instance delta objects
     */
    public DefaultComparisonResult newComparisonResult( ext_configuration configOne, 
                                    ext_configuration configTwo,
                                    List headerDeltas,
                                    List instanceDeltas) {
        return new DefaultComparisonResult( configOne, 
                                    configTwo,
                                    headerDeltas,
                                    instanceDeltas);
    }

    /**
     * Creates a new ComparisonResult object.
     * @param configOne configuration one that has been compared to external configuration two
     * @param configTwo configuration two that has been compared to external configuration one
     * @param headerDeltas list of header deltas
     * @param instanceDeltas list of instance delta objects
     * @param valueDeltasByConfigObjectKey a <code>HashMap</code> with the value deltas sorted by config object key
     * @param deletedInstances list of deleted instances 
     * @param different flag indicating whether configuration 1 and 2 are different
     * @param comparisonErrors list of errors that occured during comparison
     */
    public DefaultComparisonResult newComparisonResult( ext_configuration configOne, 
                                    ext_configuration configTwo,
                                    List headerDeltas,
                                    List instanceDeltas,
                                    HashMap valueDeltasByConfigObjectKey,
                                    List deletedInstances,
                                    boolean different,
                                    List comparisonErrors) {

        return new DefaultComparisonResult(configOne, 
                                    configTwo,
                                    headerDeltas,
                                    instanceDeltas,
                                    valueDeltasByConfigObjectKey,
                                    deletedInstances,
                                    different,
                                    comparisonErrors); 
    }

    /**
     * Creates a new ConfigurationSnapshot object.
     * @param config Configuration object for which the snapshot should be created
     */
    public DefaultConfigurationSnapshot newConfigurationSnapshot(Configuration config) {
        return new DefaultConfigurationSnapshot(config);
    }        

    /**
     * @param config Configuration object for which the snapshot should be created
     * @param name name of the snapshot
     * @param price price of the configuration of the snapshot
     * @param isInitial flag whether snapshot is the initial configuration
     */
    public DefaultConfigurationSnapshot newConfigurationSnapshot(Configuration config, String name, String price, boolean isInitial) {
        return new DefaultConfigurationSnapshot(config, name, price, isInitial);
    }

    /**
     * @param config Configuration object for which the snapshot should be created
     * @param name name of the snapshot
     * @param description description of the snapshot
     * @param price price of the configuration of the snapshot
     * @param isInitial flag whether snapshot is the initial configuration
     */
    public DefaultConfigurationSnapshot newConfigurationSnapshot(Configuration config, String name, String description, String price, boolean isInitial) {
        return new DefaultConfigurationSnapshot(config, name, description, price, isInitial);
    }

    /**
     * @param extConfig external configuration for which the snapshot should be created
     * @param name name of the snapshot
     * @param description description of the snapshot
     * @param price price of the configuration of the snapshot
     * @param isInitial flag whether snapshot is the initial configuration
     */
    public DefaultConfigurationSnapshot newConfigurationSnapshot(ext_configuration extConfig, String name, String description, String price, boolean isInitial) {
        return new DefaultConfigurationSnapshot(extConfig, name, description, price, isInitial);
    }

    /**
     * Creatse a new TTEProduct
     * @param itemProps
     * @param docProps
     * @return A new TTEProduct 
     */
    public DefaultTTEProduct newTTEProduct(IPCItemProperties itemProps, IPCDocumentProperties docProps) {
        return new DefaultTTEProduct(itemProps, docProps);
    }

    /**
     * Creates a new TTEBusinessPartner
     * @param businessPartnerId
     * @param country
     * @param region
     * @param county
     * @param city
     * @param postalCode
     * @param geoCode
     * @param jurisidictionCode
     * @param exemptedRegion
     * @param belongsToLegEnt
     * @param taxability
     * @param taxTypes
     * @param taxGroups
     * @param taxNumbers
     * @param taxNumberTypes
     * @return A new TTEBusinessPartner
     */
    public DefaultTTEBusinessPartner newTTEBusinessPartner(
                                                String businessPartnerId,
                                                String country,
                                                String region,
                                                String county,
                                                String city,
                                                String postalCode,
                                                String geoCode,
                                                String jurisidictionCode,
                                                String exemptedRegion,
                                                String belongsToLegEnt,
                                                String taxability,
                                                ArrayList taxTypes,
                                                ArrayList taxGroups,
                                                ArrayList taxNumbers,
                                                ArrayList taxNumberTypes){
        return new DefaultTTEBusinessPartner(businessPartnerId, country, region, county,
            city, postalCode, geoCode, jurisidictionCode, exemptedRegion, belongsToLegEnt,
            taxability, taxTypes, taxGroups, taxNumbers, taxNumberTypes);
    }

    /**
     * Creates a new TTEItemBusinesspartner
     * @param role
     * @param businessPartnerId
     * @param country
     * @param region
     * @param county
     * @param city
     * @param postalCode
     * @param geoCode
     * @param exemptedRegion
     * @param jurisdictionCode
     * @return A new TTEItemBusinesspartner
     */
    public DefaultTTEItemBusinessPartner newTTEItemBusinessPartner(
                                    String role,
                                    String businessPartnerId,
                                    String country,
                                    String region,
                                    String county, 
                                    String city, 
                                    String postalCode,
                                    String geoCode, 
                                    String exemptedRegion, 
                                    String jurisdictionCode){
        return new DefaultTTEItemBusinessPartner(role, businessPartnerId, country, region, county, 
                                    city, postalCode, geoCode, exemptedRegion, jurisdictionCode);                                        
    }

    public DefaultTTEProductTaxClassification newTTEProductTaxClassification(
                                    String country,
                                    String region,
                                    String taxType,
                                    String taxGroup,
                                    String taxTariffCode){
        return new DefaultTTEProductTaxClassification(country, region, taxType, taxGroup, taxTariffCode);
    }

    /**
     * Creates a new PricingConverter
     * @param session
     * @return
     */
    public abstract DefaultPricingConverter newPricingConverter(DefaultIPCSession session);

}
