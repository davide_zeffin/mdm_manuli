/*
 * Created on 20.02.2006
 *
 */
package com.sap.spc.remote.client.object.imp;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import com.sap.isa.core.cache.Cache;
import com.sap.isa.core.cache.Cache.Exception;

/**
 * Stores meta data of characteristics session spreading. 
 * Currently it contains a characeristics description and characteristic value descriptions
 * 
 * It can be extended to store other meta data.
 */
public class KnowledgebaseCacheContainer {

    public class CsticContainer {
        Map valueDescriptions = new HashMap();
        String description;
        public String lname;
        public void addValue( String valueName, String valueDescription)
        {
            valueDescriptions.put(valueName,valueDescription);
        }
        public String getLName()
        {
            return lname;
        }
        public String toString()
        {
            Iterator iter= valueDescriptions.entrySet().iterator();
            StringBuffer buf= new StringBuffer("<csticContainer><description>" + description + "</description>\n");
            while(iter.hasNext())
            {
                String key=(String) iter.next();
                buf.append("<key>\n");
                buf.append(key);
                buf.append("</key>\n");
                buf.append("<value>\n");
                buf.append(valueDescriptions.get(key));
                buf.append("</value>\n");
            }
            return buf.toString();
        }
    }
    /**
     * Description of all characteristic values. The characteristics value name is use as key
     * The Map contains CsticContainer's 
     */
    Map cstics = new HashMap();
    
    /**
     * 
     * @param csticname
     * @param valueName
     * @return Description of Characteristic Value
     */
    public String getValueDescription( String csticname, String valueName)
    {
        CsticContainer container = (CsticContainer)cstics.get(csticname);
        if( container == null)
            return null;
        return (String) container.valueDescriptions.get(valueName);
    }

    /**
     * @param csticname
     * @param valueName
     * @param valueDescription
     * @throws Cache.Exception
     */
    public void setValue( String csticname, String valueName, String valueLName, String valueDescription) throws Exception
    {
        CsticContainer container = (CsticContainer)cstics.get(csticname);
        
        if( container == null)
        {
            throw new Cache.Exception("cstic not found in container: " + csticname);
        }
        container.addValue(valueName, valueDescription);
    }
  
    /**
     * @param csticname
     * @param description
     * @return
     * @throws Exception
     */
    public CsticContainer addCharacteristic(String csticname, String languageDependandName, String description) throws Exception
    {
        CsticContainer container = new CsticContainer();
        container.description = description;
        container.lname = languageDependandName;
        cstics.put( csticname, container);
        return container;
    }
    /**
     * @param csticname
     * @return Characterisitc description
     * @throws Exception
     */
    public String getCharacteristicDescription( String characteristicName) 
    {
        
        CsticContainer rc = (CsticContainer) cstics.get( characteristicName);
        if(rc!= null)
            return rc.description;
        return null;
    }
    public String getCharacteristicLName( String characteristicName) 
    {
        
        CsticContainer rc = (CsticContainer) cstics.get( characteristicName);
        if(rc!= null)
            return rc.lname;
        return null;
    }
    public CsticContainer getCharacteristic( String characteristicName)
    {
        return (CsticContainer) cstics.get( characteristicName);
    }
}
