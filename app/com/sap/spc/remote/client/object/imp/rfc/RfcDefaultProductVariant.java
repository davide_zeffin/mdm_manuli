package com.sap.spc.remote.client.object.imp.rfc;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.sap.spc.remote.client.object.IClient;
import com.sap.spc.remote.client.object.IPCClient;
import com.sap.spc.remote.client.object.ProductVariantFeature;
import com.sap.spc.remote.client.object.imp.DefaultIPCDocument;
import com.sap.spc.remote.client.object.imp.DefaultIPCSession;
import com.sap.spc.remote.client.object.imp.DefaultProductVariant;

public class RfcDefaultProductVariant extends DefaultProductVariant {
   
    protected RfcDefaultProductVariant(IPCClient ipcClient,
        String id,
        double distance,
        String matchPoints,
        String maxMatchPoints,
        List features,
        ProductVariantFeature mainDifference) {
            super(ipcClient, id, distance, matchPoints, maxMatchPoints, features, mainDifference);
    }    

    /* (non-Javadoc)
     * @see com.sap.spc.remote.client.object.ProductVariant#getProductGuid()
     */
	protected String getProductGuid(String application) {
        if (productGuid != null){
            return productGuid; 
        }
        String guid = id;
        if ((application != null) && (application.equals(DefaultIPCDocument.CRM))){
            DefaultIPCSession session = (DefaultIPCSession)ipcClient.getIPCSession();
            IClient client = session.getClient();
            productGuid = RfcDefaultIPCItem.getProductGuid(id, productType, productLogSys, client);
            guid = productGuid;
        }
        return guid;
    }

    /* (non-Javadoc)
     * @see com.sap.spc.remote.client.object.ProductVariant#getAttributesFromProductMaster(java.lang.String)
     */
    public Map getAttributesFromProductMaster(String application) {
        Map attribs = new HashMap();
        String variantGuid = getProductGuid(application);
        // Dummy implementation: at the moment only the attributes containing the guid are returned.
        // We have no chance to get the attributes from the server (no function modules available).
        // Should be implemented with next release (5.1).        
        if (application != null && application.equals(DefaultIPCDocument.ERP)){ // ERP
            attribs.put("PMATN", variantGuid);  
        }
        else { // CRM
            attribs.put("PRICE_PRODUCT", variantGuid);  
            attribs.put("PRODUCT", variantGuid);
        }
        return attribs;
    }

}
