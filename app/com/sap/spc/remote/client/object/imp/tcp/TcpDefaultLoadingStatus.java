package com.sap.spc.remote.client.object.imp.tcp;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import com.sap.msasrv.usermodule.spc.ISPCCommandSet;
import com.sap.spc.remote.client.ClientException;
import com.sap.spc.remote.client.ResourceAccessor;
import com.sap.spc.remote.client.object.Configuration;
import com.sap.spc.remote.client.object.IPCClient;
import com.sap.spc.remote.client.object.IPCException;
import com.sap.spc.remote.client.object.IPCSession;
import com.sap.spc.remote.client.object.LoadingMessage;
import com.sap.spc.remote.client.object.LoadingStatus;
import com.sap.spc.remote.client.object.imp.DefaultLoadingStatus;
import com.sap.spc.remote.client.object.imp.IllegalClassException;
import com.sap.spc.remote.client.tcp.ServerResponse;
import com.sap.spc.remote.shared.command.IGetConfigLoadMessages;
import com.sap.spc.remote.shared.command.IGetConfigLoadStatus;
import com.sap.tc.logging.Category;
import com.sap.tc.logging.Location;



public class TcpDefaultLoadingStatus extends DefaultLoadingStatus implements LoadingStatus {
	private static final Category category = ResourceAccessor.category;
	private static final Location location = ResourceAccessor.getLocation(TcpDefaultIPCItem.class);
	protected HashMap data = new HashMap(); //Pricing cached data. 

    /**
     * @param client IPCClient 
     * @param config Configuration to which the <code>LoadingStatus</code> belongs to.
     */
    TcpDefaultLoadingStatus(IPCClient client, Configuration config) {
        super(client, config);
    }

    /* (non-Javadoc)
     * @see com.sap.spc.remote.client.object.imp.DefaultLoadingStatus#init()
     */
    protected void init() {
        location.entering("init()");
        String configId = config.getId();
        IPCSession session = ipcClient.getIPCSession();
        if (!(session instanceof TcpDefaultIPCSession)){
            throw new IllegalClassException(session, TcpDefaultIPCSession.class);
        }
        this.cachingClient = ((TcpDefaultIPCSession) session).getClient();
 		ServerResponse r = null;
			try {
				r = ((TCPDefaultClient)cachingClient).doCmd(ISPCCommandSet.GET_CONFIG_LOAD_STATUS,
									new String[] {"configId", configId
									});
			} catch (ClientException e) {
				throw new IPCException(e);
			}
		String msgLevel = "";
		String cfgLoadedFromExt = "";
		String kbVersionChanged = "";
		
		cfgLoadedFromExt = r.getParameterValue(IGetConfigLoadStatus.CFG_LOADED_FROM_EXT);
		kbVersionChanged = r.getParameterValue(IGetConfigLoadStatus.KB_VERSION_CHANGED);
		
		// KB changed? 
		if (kbVersionChanged.equals(TRUE)) {
			newKB = true;
		}

		// status
		if (msgLevel.equals(EMPTY)) {
			status = OK;
		}
		else if (msgLevel.equals(I)) {
			status = INFO;
		}
		else if (msgLevel.equals(W)) {
			status = WARNING;
		}
		else {
			status = ERROR;
		}

		// has loading messages?
		if (status > OK) {
			loadingMessages= true;
		}

		// has initial configuration
		if (cfgLoadedFromExt.equals(TRUE)){
			initialConfiguration = true;
		}
			
		// r.getParameterValue(IGetConfigLoadStatus.MSG_LVL);
        location.debugT("LoadingStatus for configuration " + config.getId() + ":");
        location.debugT("Status: " + status + ", hasNewKB: " + newKB + 
                        ", hasLoadingMessages: " + loadingMessages + 
                        ", hasInitialConfiguration: " + initialConfiguration);
        location.exiting();
    }

    /* (non-Javadoc)
     * @see com.sap.spc.remote.client.object.LoadingStatus#getLoadingMessages()
     */
    public List getLoadingMessages() {
        location.entering("getLoadingMessages()");
        if (listOfLoadingMessages != null){
            location.debugT("listOfLoadingMessages is not null; no call of FM necessary.");
            location.exiting();
            return listOfLoadingMessages;
        }
        else {
            location.debugT("listOfLoadingMessages is null; creating new ArrayList and call FM");
            listOfLoadingMessages = new ArrayList();
            String configId = config.getId();
			IPCSession session = ipcClient.getIPCSession();
			if (!(session instanceof TcpDefaultIPCSession)){
				throw new IllegalClassException(session, TcpDefaultIPCSession.class);
			}
			ServerResponse r = null;
				try {
					r = ((TCPDefaultClient)cachingClient).doCmd(ISPCCommandSet.GET_CONFIG_LOAD_MESSAGES,
										new String[] {"configId", configId
										});
										
					String number[]       = r.getParameterValues(IGetConfigLoadMessages.NUMBER);
					String type[]         = r.getParameterValues(IGetConfigLoadMessages.TYPE);			
					String message[]      = r.getParameterValues(IGetConfigLoadMessages.MESSAGE);
					String messagesV1[]    = r.getParameterValues(IGetConfigLoadMessages.MESSAGE_V1);
					String messagesV2[]    = r.getParameterValues(IGetConfigLoadMessages.MESSAGE_V2);
					String messagesV3[]    = r.getParameterValues(IGetConfigLoadMessages.MESSAGE_V3);
					String messagesV4[]    = r.getParameterValues(IGetConfigLoadMessages.MESSAGE_V4);
					String instId[]       = r.getParameterValues(IGetConfigLoadMessages.INST_ID);
					String csticName[]    = r.getParameterValues(IGetConfigLoadMessages.CHARC);
					String messageClass[] = r.getParameterValues(IGetConfigLoadMessages.ID);
					
					if( message != null )
					{
						for (int k=0; k < message.length;k++){
							int msgNumber = Integer.parseInt(number[k]);
							// if instId is not specified we create a dummy instId "NO_ID"
							if (instId[k] == null || instId[k].equals("")){
								instId[k] = NO_ID; 
							}
						
											int severity = ERROR;
											if (type[k].equals(W)) {
												severity = WARNING;
											}
											else if (type[k].equals(I)) {
												severity= INFO;
											}
							String messageV1 = null;				                    
							String messageV2 = null;				                    
							String messageV3 = null;				                    
							String messageV4 = null;				                    
							if(messagesV1 != null && k < messagesV1.length )
								messageV1 = messagesV1[k];
							if(messagesV2 != null && k < messagesV2.length )
								messageV2 = messagesV2[k];
							if(messagesV3 != null && k < messagesV3.length )
								messageV3 = messagesV3[k];
							if(messagesV4 != null && k < messagesV4.length )
								messageV4 = messagesV4[k];
							
							LoadingMessage lm = factory.newLoadingMessage(instId[k], csticName[k], msgNumber, 
																		  messageClass[k], severity, message[k], 
																		  new String[] {messageV1, messageV2, messageV3, messageV4});
							location.debugT("Created new LoadingMessage(" + lm.getConfigObjKey()+ ", " + lm.getNumber() + ", " + lm.getSeverity() + ", " + lm.getText());
							listOfLoadingMessages.add(lm);
						}
					}
				} catch (ClientException e) {
					throw new IPCException(e);
				}
            
        }
        location.exiting();
        return listOfLoadingMessages;
    }

}
