package com.sap.spc.remote.client.object.imp.tcp;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.ParseException;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import com.sap.spc.remote.client.ClientException;
import com.sap.spc.remote.client.tcp.ServerResponse;
import com.sap.spc.remote.client.object.IPCException;
import com.sap.spc.remote.client.object.IPricingConverter;
import com.sap.spc.remote.client.object.imp.DefaultIPCSession;
import com.sap.spc.remote.client.object.imp.DefaultPricingConverter;
import com.sap.spc.remote.client.object.imp.tcp.TCPDefaultClient;

import com.sap.msasrv.module.system.ISystemCommandSet;
import com.sap.msasrv.module.system.command.GetSessionInfo;
import com.sap.msasrv.usermodule.spc.ISPCCommandSet;
import com.sap.spc.remote.shared.command.GetAllPhysicalUnits;
import com.sap.spc.remote.shared.command.GetAllCurrencyUnits;

import com.sap.spc.remote.client.ResourceAccessor;
import com.sap.tc.logging.Category;
import com.sap.tc.logging.Location;
import com.sap.tc.logging.Severity;

public class TcpDefaultPricingConverter
    extends DefaultPricingConverter
    implements IPricingConverter {

        // language independent cache for external currency units ( ISO Codes )
        protected Map externalCurrencyCodes = null;
    
        public static final String DEFAULT_SESSION_LANGUAGE = "EN";
        protected String sessionLanguage = null;
        protected String sessionISOLanguage = null;
      
        private static final Category category = ResourceAccessor.category;
        private static final Location location = ResourceAccessor.getLocation(TcpDefaultPricingConverter.class);

        public TcpDefaultPricingConverter(DefaultIPCSession session){
            internalToExternalUOM = new HashMap();
            externalToInternalUOM = new HashMap();
            numberOfDecimalsForUOMs = new HashMap();
            externalUOMNamesAndDescriptions = new HashMap();
            currencyDescriptions = new HashMap();
            numberOfDecimalsForCurrencies = new HashMap();
        
            _session = session;
        }
    
        /**
         * Converts a currency value to the internal representation. Uses the currency unit to determine the 
         * scale. The session is used for correct language-dependent parsing of the currency value.  
         * Grouping separator and decimal separator can also be passed separately. These will have higher priority than
         * the language of the session.
         * @param extValue external currency value
         * @param unit currency unit
         * @param decimalSeparator (pass null if you want to use the session-language for correct parsing)
         * @param groupingSeparator (pass null if you want to use the session-language for correct parsing)
         * @return currency value as String (internal representation)
         */
        public  String convertCurrencyValueExternalToInternal(String extValue, String unit, String decimalSeparator, String groupingSeparator) {
            if (extValue == null){
                category.logT(Severity.ERROR, location, "Currency value conversion: external value is null. Returning null.");
                return null;
            }
            if (extValue.equals("")){
                category.logT(Severity.ERROR, location, "Currency value conversion: external value is an empty string. Returning empty string.");
                return "";            
            }        
            int scale = 0;
            category.logT(Severity.DEBUG, location, "Converting external currency value (" + extValue + unit + ") to internal.");
            HashMap numberOfDecimals = getNumberOfDecimalsForCurrencies();
            if ((numberOfDecimals != null) && (unit != null)){
                if (numberOfDecimals.get(unit) != null){
                    scale = ((Integer) numberOfDecimals.get(unit)).intValue();
                }else{
                    category.logT(Severity.ERROR, location, "No demimals (scale) found for unit " + unit +". Setting to 0." );
                }
            }
            double internalCur = convertToDouble(extValue, scale, getLanguage(), decimalSeparator, groupingSeparator);
            return "" + internalCur;
        }
    
        /**
         * Converts a currency value as BigDecimal to the external representation. Uses the currency unit to determine the 
         * scale and the language of the session to determine grouping separator and decimal separator.
         * Grouping separator and decimal separator can also be passed. These will have higher priority than
         * the language of the session.
         * @param decimal internal currency value (BigDecimal
         * @param unit currency unit
         * @param decimalSeparator (pass null if you want to use the session-language)
         * @param groupingSeparator (pass null if you want to use the session-language)
         * @return currency value as String (external representation)
         */    
        public  String convertCurrencyValueInternalToExternal(String decimal, String unit, String decimalSeparator, String groupingSeparator) {
            if (decimal == null){
                category.logT(Severity.ERROR, location, "Currency value conversion: internal value (BigDecimal) is null. Returning null.");
                return null;
            }
            if (decimal.equals("")){
                category.logT(Severity.ERROR, location, "Currency value conversion: internal value is an empty string. Returning empty string.");
                return "";            
            }        
        
//          BigDecimal decimalScale0 = decimal.setScale(0, BigDecimal.ROUND_DOWN);
//          if (decimal.compareTo(decimalScale0) == 0) {
//              return convertToString(decimalScale0, 0, getLanguage(session), decimalSeparator, groupingSeparator);
//          }
            int scale = 0;
            category.logT(Severity.DEBUG, location, "Converting internal currency value (" + decimal.toString() + unit + " to external.");
            HashMap numberOfDecimals = getNumberOfDecimalsForCurrencies();
            if ((numberOfDecimals != null) && (unit != null)) {
                if (numberOfDecimals.get(unit) != null){
                    scale = ((Integer) numberOfDecimals.get(unit)).intValue();
                }else{
                    category.logT(Severity.ERROR, location, "No decimals (scale) found for unit " + unit +". Setting to 0." );
                }
            }        
            return convertToString(Double.parseDouble(decimal), scale, getLanguage(), decimalSeparator, groupingSeparator);
        }
    
        /**
         * Converts the language dependent unit of measurement to the internal represenation
         * @param extUnit external represenation (language dependent) of the unit of measurement
         * @return internal representation of unit of measurement
         */
        public  String convertUOMExternalToInternal(String extUnit) {
            if (extUnit == null) {
                category.logT(Severity.ERROR, location, "Unit conversion: external unit is null. Returning null.");
                return null;
            }
            String language = getLanguage();
            category.logT(Severity.DEBUG, location, "Converting external unit (" + extUnit + " to internal.");
            // mapping for this language already retrieved?
            HashMap data = (HashMap) externalToInternalUOM.get(language);
            if (data == null){
                initMappingForLanguage();
                data = (HashMap) externalToInternalUOM.get(language);
            }
            String internalUnit = "";
            if (data != null) {
                internalUnit = (String) data.get(extUnit);
                if (internalUnit == null){
                    category.logT(Severity.ERROR, location, "Unit conversion: Could not find appropriate mapping for external unit "+ extUnit + " and language " + language + ". Maybe unit and language does not fit. Returning null.");
                }
            }
            return internalUnit;
        }
    
        /**
         * Converts the language independent unit of measurement to the external represenation (language dependent)
         * @param intUnit internal represenation (language independent) of the unit of measurement
         * @return external representation of unit of measurement
         */    
        public  String convertUOMInternalToExternal(String intUnit) {
            if (intUnit == null){
                category.logT(Severity.ERROR, location, "Unit conversion: internal unit is null. Returning null.");
                return null;
            }
            String language = getLanguage();
            category.logT(Severity.DEBUG, location, "Converting internal unit (" + intUnit + " to external.");
            // mapping for this language already retrieved?
            HashMap data = (HashMap) internalToExternalUOM.get(language);
            if (data == null){
                initMappingForLanguage();
                data = (HashMap) internalToExternalUOM.get(language);
            }
            String externalUnit = "";
            if (data != null) {
                externalUnit = (String) data.get(intUnit);
            }
            if (externalUnit == null) {
                category.logT(Severity.ERROR, location, "Unit conversion: external unit for [" + intUnit + "] is null. Returning empty string. Please check your customizing!");
                externalUnit = "";
            }else if ("".equals(externalUnit)) {
                category.logT(Severity.ERROR, location, "Unit conversion: external unit for [" + intUnit + "] is empty string. Returning empty string. Please check your customizing!");
            }
            return externalUnit;
        }
    
        /**
         * Converts a value to the internal representation. Uses the unit of measurement to determine the 
         * scale. The session is used for correct language-dependent parsing of the value.
         * Grouping separator and decimal separator can also be passed separately. 
         * These will have higher priority than the language of the session.
         * @param extValue external value
         * @param extUnit external unit of measurement
         * @param decimalSeparator (pass null if you want to use the session-language for correct parsing)
         * @param groupingSeparator (pass null if you want to use the session-language for correct parsing)
         * @return value as String (internal representation)
         */    
        public  String convertValueExternalToInternal(String extValue, String extUnit, String decimalSeparator, String groupingSeparator) {
            if (extValue == null){
                category.logT(Severity.ERROR, location, "Value conversion: external value is null. Returning null.");
                return null;
            }
            if (extValue.equals("")){
                category.logT(Severity.ERROR, location, "Value conversion: external value is an empty string. Returning empty string.");
                return "";            
            }
            category.logT(Severity.DEBUG, location, "Converting external value (" + extValue + extUnit + " to internal.");
            String internalUnit = convertUOMExternalToInternal(extUnit);
//          String intDecimal = convertUOMToBigDecimal(extValue, internalUnit, session, decimalSeparator, groupingSeparator);
            double intDecimal = convertUOMTodouble(extValue, internalUnit, decimalSeparator, groupingSeparator);
            return ""+intDecimal;
        }



        /**
         * Converts a value as BigDecimal to the external representation. Uses the unit of measurement to determine the 
         * scale and the language of the session to determine grouping separator and decimal separator.
         * Grouping separator and decimal separator can also be passed. These will have higher priority than
         * the language of the session.
         * @param decimal internal  value (BigDecimal)
         * @param unit internal unit of measurement
         * @param decimalSeparator (pass null if you want to use the session-language)
         * @param groupingSeparator (pass null if you want to use the session-language)
         * @return value as String (external representation)
         */    
        public  String convertValueInternalToExternal(String intDecimal, String intUnit, String decimalSeparator, String groupingSeparator) {
            if (intDecimal == null){
                category.logT(Severity.ERROR, location, "Value conversion: internal value (BigDecimal) is null. Returning null.");
                return null;
            }
            if (intDecimal.equals("")){
                category.logT(Severity.ERROR, location, "Value conversion: internal value is an empty string. Returning empty string.");
                return "";            
            }        
            category.logT(Severity.DEBUG, location, "Converting internal value (" + intDecimal.toString() + intUnit + " to internal.");        
            String extValue = convertUOMToString(Double.parseDouble(intDecimal), intUnit, decimalSeparator, groupingSeparator);
            return extValue;
        }

        /**
         * Helper method. 
         * If the scale is 0 it uses this. Otherwise it requests the number of decimals for 
         * the unit of measurements.
         * Passes this information on to the real String-conversion method.
         * @param decimal
         * @param intUnit
         * @param decimalSeparator
         * @param groupingSeparator
         * @return String converted values
         */
        protected  String convertUOMToString(double decimal, String intUnit, String decimalSeparator, String groupingSeparator) {
//          double decimalScale0 = decimal.setScale(0, BigDecimal.ROUND_DOWN);
//          if (decimal.compareTo(decimalScale0) == 0) {
//              return convertToString(decimalScale0, 0, getLanguage(session), decimalSeparator, groupingSeparator);
//          }
            int scale = 0;
            HashMap numberOfDecimals = getNumberOfDecimalsForUOMs();
            if ((numberOfDecimals != null) && (intUnit != null)) {
                Object unitValObj = numberOfDecimals.get(intUnit);
                if (unitValObj != null){
                    scale = ((Integer) unitValObj).intValue();
                }else{
                    category.logT(Severity.ERROR, location, "Not able to find the scale of numberOfDecimalsForUOMs for this session. scalling to default (ZERO).");
                }
            }        
            return convertToString(decimal, scale, getLanguage(), decimalSeparator, groupingSeparator);
        }


        protected  double convertUOMTodouble(String value, String internalUnit, String decimalSeparator, String groupingSeparator) {
            int scale = 0;
            HashMap numberOfDecimals = getNumberOfDecimalsForUOMs();
            if ((numberOfDecimals != null) && (internalUnit != null)) {
                scale = ((Integer) numberOfDecimals.get(internalUnit)).intValue();
            }
            return convertToDouble(value, scale, getLanguage(), decimalSeparator, groupingSeparator);
        
        }

        /**
         * Returns the number of decimals for units of measurements. Tries to use the cache first.
         * If this was not successful it retrieves the data from the function module.
         * @return HashMap containting the number of decimals for units of measurements for the session-language
         */
        protected  HashMap getNumberOfDecimalsForUOMs() {
            String language = getLanguage();
            HashMap data = (HashMap) numberOfDecimalsForUOMs.get(language);
            if (data == null){
                initMappingForLanguage();
                data = (HashMap) numberOfDecimalsForUOMs.get(language);
            }
            return data;
        }

        /**
         * Returns a list of external names and descriptions for units of measurements. Tries to use the cache first.
         * If this was not successful it retrieves the data from the function module.
         * @return String[][] array containting the external names of UOMS and descriptions for the session-language
         */
        public  String[][] getExternalUOMNamesAndDescriptions() {
            String language = getLanguage();
            String[][] data = (String[][]) externalUOMNamesAndDescriptions.get(language);
            if (data == null){
                initMappingForLanguage();
                data = (String[][]) externalUOMNamesAndDescriptions.get(language);
            }
            return data;
        }

    
        /**
         * Converts the given BigDecimal using scale and language (and maybe separators) to a String. 
         * @param decimal
         * @param scale
         * @param language
         * @param decimalSeparator
         * @param groupingSeparator
         * @return Value as string
         */
        protected  String convertToString(double decimal, int scale, String language, String decimalSeparator, String groupingSeparator) {
            String pattern = "#,##0";
            if (scale > 0) {
                pattern += '.';
            }
            
            for (int i = 0; i < scale; i++) {
                pattern += "0";
            }
            DecimalFormatSymbols formatSymbols;
            if ((decimalSeparator != null) && (groupingSeparator != null) && !decimalSeparator.equals("") && !groupingSeparator.equals("")){
                formatSymbols = new DecimalFormatSymbols();
                formatSymbols.setDecimalSeparator(decimalSeparator.charAt(0));
                formatSymbols.setGroupingSeparator(groupingSeparator.charAt(0));
            }
            else {
                formatSymbols = new DecimalFormatSymbols(new Locale(language));
            }
            DecimalFormat df = new DecimalFormat(pattern, formatSymbols);
            String value = df.format(decimal);
            return value;
        }
    
        /**
         * Returns the number of decimals for currencies. Tries to use the cache first.
         * If this was not successful it retrieves the data from the function module. 
         * @return HashMap with the number of decimals for the currencies (using session language)
         */
        protected  HashMap getNumberOfDecimalsForCurrencies() {
            String language = getLanguage();
            HashMap data = (HashMap) numberOfDecimalsForCurrencies.get(language);
            if (data == null){
                initMappingForLanguage();
                data = (HashMap) numberOfDecimalsForCurrencies.get(language);
            }
            return data;
        }
    
        /**
         * Returns a list of descriptions for currencies. Tries to use the cache first.
         * If this was not successful it retrieves the data from the function module. 
         * @return String[][] Array with the descriptions of currencies (using session language)
         */
        public  String[][] getCurrencyDescriptions() {
            String language = getLanguage();
            String[][] data = (String[][])currencyDescriptions.get(language);
            if (data == null){
                initMappingForLanguage();
                data = (String[][])currencyDescriptions.get(language);
            }
            return data;
        }    
    
        /**
         * Converts the given String using scale and language (and maybe separators) to a BigDecimal.
         * @param value
         * @param scale
         * @param language
         * @param decimalSeparator
         * @param groupingSeparator
         * @return
         */
        protected  double convertToDouble(String value, int scale, String language, String decimalSeparator, String groupingSeparator) {
            DecimalFormatSymbols formatSymbols;
            if ((decimalSeparator != null) && (groupingSeparator != null) && !decimalSeparator.equals("") && !groupingSeparator.equals("")){
                formatSymbols = new DecimalFormatSymbols();
                formatSymbols.setDecimalSeparator(decimalSeparator.charAt(0));
                formatSymbols.setGroupingSeparator(groupingSeparator.charAt(0));
            }
            else {
                formatSymbols = new DecimalFormatSymbols(new Locale(language));
            }
            DecimalFormat df = new DecimalFormat("#,##0.000", formatSymbols);
            double decimal;
            try {
                decimal = df.parse(value).doubleValue();
            } catch (ParseException ex) {
                throw new IPCException(ex);
            }
            //decimal = decimal.setScale(scale, BigDecimal.ROUND_HALF_UP); // set to a
            return decimal;
        }
    
        /**
         * Returns the converted external currency name.
         * First a check is done if the currency does not exist in the way it is entered as parameter.
         * If the given currency unit  could not be found the string will be converted into upper case
         * @param  String   external currency unit
         * @return String   converted ( to uppercase if needed ) currency name
         */
        public  String checkExternalCurrencyUnit(String argExternalCurrencyUnit) {

            String data = null;
            if ( argExternalCurrencyUnit != null && 
                 _session != null ){         
                if (externalCurrencyCodes == null){
                    // first call, need to init the caches
                    initMappingForLanguage();
                }
                // try to get the currency unit from the cache
                data = (String) externalCurrencyCodes.get(argExternalCurrencyUnit);
                if ( data == null ){
                    // could not find the currency name as it was provided.
                    // now convert the value into upper case
                    data = argExternalCurrencyUnit.toUpperCase();
                }
                category.logT(Severity.DEBUG, 
                              location, 
                              "checkExternalCurrencyUnit, currency unit check: " +
                              "in currency unit :" + argExternalCurrencyUnit +
                              "out currency unit :" +data);
            
            }
            else {
                // some input parameters are missing, just return the currency name as it is
                data = argExternalCurrencyUnit;
                if (_session == null )       
                    category.logT(Severity.ERROR, location, "checkExternalCurrencyUnit, currency unit check: session is null. Returning curr. unit value without changes.");
                if (argExternalCurrencyUnit == null )       
                    category.logT(Severity.ERROR, location, "checkExternalCurrencyUnit, currency unit check: currency unit is null. Returning null.");
            
            }
            return data;

        }    
    
        /**
         * Initializes the mapping using session-language.
         */
        protected  void initMappingForLanguage() {
            try {
            
                String[] params = new String[0];
                ServerResponse r = ((TCPDefaultClient)_session.getClient()).doCmd(ISPCCommandSet.GET_ALL_PHYSICALUNITS,params);
                String[] internalNames = r.getParameterValues(GetAllPhysicalUnits.INTERNAL_NAME);
                String[] externalNames = r.getParameterValues(GetAllPhysicalUnits.NAME); //External Names
                String[] descriptions = r.getParameterValues(GetAllPhysicalUnits.DESCRIPTION);
                String[] decimals = r.getParameterValues(GetAllPhysicalUnits.DECIMALS);
                int numOfUnits = internalNames.length; 
            
                // cache the result
                HashMap internalToExternalUOMForLanguage = new HashMap(numOfUnits);
                HashMap externalToInternalUOMForLanguage = new HashMap(numOfUnits);
                HashMap numberOfDecimalsForUOMsForLanguage = new HashMap(numOfUnits);
                String[][] uOMNamesAndDescriptionsForLanguage = new String[numOfUnits][2]; // use array to keep order
            
                for (int i = 0; i < numOfUnits; i++) {
                    internalToExternalUOMForLanguage.put(internalNames[i], externalNames[i]);
                    externalToInternalUOMForLanguage.put(externalNames[i], internalNames[i]);
                    numberOfDecimalsForUOMsForLanguage.put(internalNames[i], new Integer(decimals[i]));
                    uOMNamesAndDescriptionsForLanguage[i][0] = externalNames[i];
                    uOMNamesAndDescriptionsForLanguage[i][1] = descriptions[i];                
                }
            
                // get number of decimals for currencies
                String[] curParams = new String[0];
                ServerResponse cr = ((TCPDefaultClient)_session.getClient()).doCmd(ISPCCommandSet.GET_ALL_CURRENCY_UNITS,curParams);
                String[] internalCurNames = cr.getParameterValues(GetAllCurrencyUnits.NAME);
                String[] curDescriptions = cr.getParameterValues(GetAllCurrencyUnits.DESCRIPTION);
                String[] curDecimals = cr.getParameterValues(GetAllCurrencyUnits.DECIMALS);
                String[] curLanguage = cr.getParameterValues(GetAllCurrencyUnits.CURR_LANGUAGE);
                String[] contoies = cr.getParameterValues(GetAllCurrencyUnits.COUNTRY);
                int numOfCurs = internalCurNames.length;

                // cache the result
                HashMap numberOfDecimalsForCurrenciesForLanguage = new HashMap(numOfCurs);
                String[][] listOfCurrencyDescriptionsForLanguage = new String[numOfCurs][2]; // use array to keep order
            
                for (int i = 0; i < numOfCurs; i++) {
                    numberOfDecimalsForCurrenciesForLanguage.put(internalCurNames[i], new Integer(curDecimals[i]));
                    listOfCurrencyDescriptionsForLanguage[i][0] = internalCurNames[i];
                    listOfCurrencyDescriptionsForLanguage[i][1] = curDescriptions[i];                
                }
            
                // cache the language independent currency units only once
                if ( externalCurrencyCodes == null ){
                    externalCurrencyCodes = new HashMap(numOfCurs);
                    for ( int loop = 0; loop < numOfCurs; loop ++){
                        // store external currency as key and value
                        externalCurrencyCodes.put(internalCurNames[loop], internalCurNames[loop]);
                    }
                }
            
                // save with language as key
                String currentLanguage = getLanguage();
                internalToExternalUOM.put(currentLanguage, internalToExternalUOMForLanguage);
                externalToInternalUOM.put(currentLanguage, externalToInternalUOMForLanguage);
                numberOfDecimalsForUOMs.put(currentLanguage, numberOfDecimalsForUOMsForLanguage);
                externalUOMNamesAndDescriptions.put(currentLanguage, uOMNamesAndDescriptionsForLanguage);
                numberOfDecimalsForCurrencies.put(currentLanguage, numberOfDecimalsForCurrenciesForLanguage);
                currencyDescriptions.put(currentLanguage, listOfCurrencyDescriptionsForLanguage);
            }
            catch (ClientException ce) {
                throw new IPCException(ce);
            }
        }
    
        protected  String getLanguage(){
            if (sessionLanguage == null){
                String[] params = new String[0];
                String lang = null;
                try{            
                    ServerResponse r = ((TCPDefaultClient)_session.getClient()).doCmd(ISystemCommandSet.GET_SESSION_INFO,params);
                    lang = r.getParameterValue(GetSessionInfo.SESSION_LANGUAGE);
                }catch (ClientException ce) {
                    throw new IPCException(ce);
                }
                if (lang == null){
                    category.logT(Severity.ERROR, location, "Server session do not returned a language. Set language to default \""+DEFAULT_SESSION_LANGUAGE+"\"");
                    sessionLanguage = DEFAULT_SESSION_LANGUAGE;
                }else{
                    sessionLanguage = lang;
                }
            }
            return sessionLanguage;
        }



}
