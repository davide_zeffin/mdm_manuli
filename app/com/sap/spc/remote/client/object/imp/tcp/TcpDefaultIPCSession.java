/*
 * Created on Dec 22, 2004
 *
 * To change the template for this generated file go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
package com.sap.spc.remote.client.object.imp.tcp;

import java.util.*;

import com.sap.msasrv.socket.shared.ServerConstants;
import com.sap.spc.remote.client.object.*;
import com.sap.spc.remote.client.object.imp.*;
import com.sap.msasrv.usermodule.spc.ISPCCommandSet;
import com.sap.spc.remote.shared.command.*;
import com.sap.spc.remote.client.*;
import com.sap.spc.remote.client.tcp.ServerResponse;
import com.sap.tc.logging.*;
import com.sap.msasrv.module.system.command.*;

/**
 * @author I026584
 *
 * To change the template for this generated type comment go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
public class TcpDefaultIPCSession
	extends DefaultIPCSession
	implements IPCSession {
//		protected CachingClient _client;
		protected TCPDefaultClient _client;
		
		private static final Category category = ResourceAccessor.category;
		private static final Location location = ResourceAccessor.getLocation(TcpDefaultIPCItem.class);

		// Do not use any primitive data types as parameters to constructors (Only using reflection calling the constructors.
		
		/**
		 * Creates an IPC session for SAP client mandt. Will throw an exception if
		 * initializing the session fails.<p>
		 *
		 * If a TCPDefaultClient is passed to this session, this client will be used. Otherwise,
		 * if an item reference is passed, it will be used to initialize a client. If both
		 * client and reference are null, the client will be initialized from a property file.<p>
		 *
		 * @param   ipcClient   the IPCClient to which this session belongs.
		 * @param   mandt       the SAP client to use
		 * @param   type        the session type (eg. "ISA" or "CRM")
		 * @param   client      the IPC client to use. null => construct client from reference or property file
		 * @param   ref         the IPC item reference that keeps the connection parameters. null => read from property file
		 * <p>
		 * Please note since all IPC server communication is encapsulated in objects, client-specific
		 * parameter naming is not supported by this API.
		 */
		protected TcpDefaultIPCSession(DefaultIPCClient ipcClient, String mandt, String type, TCPDefaultClient client,
						IPCItemReference genref) throws IPCException {
			TCPIPCItemReference ref = (TCPIPCItemReference) genref; 
			String host = null;
			String port = null;
			String encoding = null;
			boolean isDispatcher = true;

			if (ref != null) {
				host = ref.getHost();
				port = ref.getPort();
				encoding = ref.getEncoding();
				isDispatcher = ref.isDispatcher();
			}
            
            _pricingConverter = factory.newPricingConverter(this);
			_defaultIPCSession(ipcClient, mandt, type, client, host, port, encoding, new Boolean(isDispatcher));

		}

		/**
		 * Creates an IPC session for SAP client mandt. Will throw an exception if
		 * initializing the session fails.<p>
		 *
		 * If a TCPDefaultClient is passed to this session, this client will be used. Otherwise,
		 * if an item reference is passed, it will be used to initialize a client. If both
		 * client and reference are null, the client will be initialized from a property file.<p>
		 *
		 * @param   ipcClient   the IPCClient to which this session belongs.
		 * @param   mandt       the SAP client to use
		 * @param   type        the session type (eg. "ISA" or "CRM")
		 * @param   client      the IPC client to use. null => construct client from reference or property file
		 * @param   ref         the IPC item reference that keeps the connection parameters. null => read from property file
		 * @param   securityProperties		securityProperties
		 * <p>
		 * Please note since all IPC server communication is encapsulated in objects, client-specific
		 * parameter naming is not supported by this API.
		 */
		protected TcpDefaultIPCSession(DefaultIPCClient ipcClient, String mandt, String type, TCPDefaultClient client,
			TCPIPCItemReference ref, Properties securityProperties) throws IPCException {
			String host = null;
			String port = null;
			String encoding = null;
			boolean isDispatcher = true;

			if (ref != null) {
				host = ref.getHost();
				port = ref.getPort();
				encoding = ref.getEncoding();
				isDispatcher = ref.isDispatcher();
			}

            _pricingConverter = factory.newPricingConverter(this);
			_defaultIPCSession(ipcClient, mandt, type, client, host, port, encoding, isDispatcher, securityProperties);

		}

		/**
		 * Creates an IPC session for SAP client mandt. Will throw an exception if
		 * initializing the session fails.<p>
		 *
		 * If a TCPDefaultClient is passed to this session, this client will be used. Otherwise,
		 * if an item reference is passed, it will be used to initialize a client. If both
		 * client and reference are null, the client will be initialized from a property file.<p>
		 *
		 * @param   ipcClient   the IPCClient to which this session belongs.
		 * @param   mandt       the SAP client to use
		 * @param   type        the session type (eg. "ISA" or "CRM")
		 * @param   client      the IPC client to use. null => construct client from reference or property file
		 * @param   ref         the IPC item reference that keeps the connection parameters. null => read from property file
		 * <p>
		 * Please note since all IPC server communication is encapsulated in objects, client-specific
		 * parameter naming is not supported by this API.
		 */
		protected TcpDefaultIPCSession(
			DefaultIPCClient ipcClient,
			String mandt,
			String type,
			TCPDefaultClient client,
			TCPIPCConfigReference  ref)
			throws IPCException {
			String host = null;
			String port = null;
			String encoding = null;
			boolean isDispatcher = true;
			if (ref != null) {
				host = ref.getHost();
				port = ref.getPort();
				encoding = ref.getEncoding();
				isDispatcher = ref.isDispatcher();
			}

            _pricingConverter = factory.newPricingConverter(this);
			_defaultIPCSession(
				ipcClient,
				mandt,
				type,
				client,
				host,
				port,
				encoding,
				new Boolean(isDispatcher));
		}

		/**
		 * Creates an IPC session for SAP client mandt. Will throw an exception if
		 * initializing the session fails.<p>
		 *
		 * If a TCPDefaultClient is passed to this session, this client will be used. Otherwise,
		 * if an item reference is passed, it will be used to initialize a client. If both
		 * client and reference are null, the client will be initialized from a property file.<p>
		 *
		 * @param   ipcClient   the IPCClient to which this session belongs.
		 * @param   mandt       the SAP client to use
		 * @param   type        the session type (eg. "ISA" or "CRM")
		 * @param   client      the IPC client to use. null => construct client from reference or property file
		 * @param   ref         the IPC item reference that keeps the connection parameters. null => read from property file
		 * @param   securityProperties		security Properties
		 * <p>
		 * Please note since all IPC server communication is encapsulated in objects, client-specific
		 * parameter naming is not supported by this API.
		 */
		protected TcpDefaultIPCSession(DefaultIPCClient ipcClient, String mandt, String type, TCPDefaultClient client,
					IPCConfigReference  GenRef, Properties securityProperties) throws IPCException {
			TCPIPCConfigReference  ref = (TCPIPCConfigReference) GenRef;
			String host = null;
			String port = null;
			String encoding = null;
			boolean isDispatcher = true;
			if (ref != null) {
				host = ref.getHost();
				port = ref.getPort();
				encoding = ref.getEncoding();
				isDispatcher = ref.isDispatcher();
			}

            _pricingConverter = factory.newPricingConverter(this);
			_defaultIPCSession(ipcClient, mandt, type, client, host, port, encoding, isDispatcher, securityProperties);
		}

		/**
		 * Creates a client-side session that attaches to the session that is specified by reference.
		 * Only host, port, encoding and sessionId of the IPCItemReference are used. All documents,
		 * items and such in the server session are constructed.
		 */
		protected TcpDefaultIPCSession(DefaultIPCClient ipcClient, TCPIPCItemReference reference, Boolean isExtendedData) throws IPCException {
			try {
				_client = new TCPDefaultClient(reference.getHost(), Integer.parseInt(reference.getPort()), reference.getEncoding(), new Boolean(false));
			}
			catch(ClientException e) {
				throw new IPCException(e);
			}

//	   25.06.01 mk added the ipcClient
			_ipcClient = ipcClient;
            _pricingConverter = factory.newPricingConverter(this);

			_documentTable = new Hashtable();
			_documents = null;
			_descriptionForProcedure = new Hashtable();
			_descriptionForCurrency = new Hashtable();
			_mandt = "???";
			_userName = "unknown";
			_userLocation = "unknown";
			_isExtendedData = isExtendedData.booleanValue();

			initializeDocuments();
		}

		/**
		 * Creates a client-side session that attaches to the session that is specified by reference.
		 * Only host, port, encoding and sessionId of the IPCItemReference are used. All documents,
		 * items and such in the server session are constructed.
		 */
		protected TcpDefaultIPCSession(DefaultIPCClient ipcClient, IPCConfigReference  gemRef, Boolean isExtendedData) throws IPCException {
			TCPIPCConfigReference  reference = (TCPIPCConfigReference) gemRef;
			try {
				_client = new TCPDefaultClient(reference.getHost(), Integer.parseInt(reference.getPort()), reference.getEncoding(), new Boolean(false));
			}
			catch(ClientException e) {
				throw new IPCException(e);
			}

			_ipcClient = ipcClient;
            _pricingConverter = factory.newPricingConverter(this);

			_documentTable = new Hashtable();
			_configTable = new Hashtable();
			_documents = null;
			_descriptionForProcedure = new Hashtable();
			_descriptionForCurrency = new Hashtable();
			_mandt = "???";
			_userName = "unknown";
			_userLocation = "unknown";
			_isExtendedData = isExtendedData.booleanValue();

			initializeConfigs();
		}

		protected TcpDefaultIPCSession(DefaultIPCClient ipcClient, TCPIPCItemReference reference) throws IPCException {
			this(ipcClient, reference, new Boolean(false));
		}

		protected TcpDefaultIPCSession(DefaultIPCClient ipcClient, IPCConfigReference  reference) throws IPCException {
			this(ipcClient, reference, new Boolean(false));
		}

		protected TcpDefaultIPCSession(DefaultIPCClient ipcClient) {

//	   25.06.01 mk added the ipcClient
			_ipcClient = ipcClient;
            _pricingConverter = factory.newPricingConverter(this);

			_documentTable = new Hashtable();
			_documents = null;
			_descriptionForProcedure = new Hashtable();
			_descriptionForCurrency  = new Hashtable();
		}


		protected void _defaultIPCSession(DefaultIPCClient ipcClient, String mandt, String type, TCPDefaultClient client,
							   String host, String port, String encoding, Boolean isDispatcher) throws IPCException {

			_documentTable = new Hashtable();
			_documents = null;
			_descriptionForProcedure = new Hashtable();
			_descriptionForCurrency  = new Hashtable();
			_ipcClient = ipcClient;

			Hashtable attr = com.sap.spc.remote.client.tcp.Client.getConnectAttributes(type,mandt);

			if (client == null) {
				try {
					if (port == null || host == null ||
						port.equals("") && host.equals("") ) {
						//fault back solution if port and host are empty
						// f.e. (Vehicle Manager & old R3 scenarios)
						_client = new TCPDefaultClient(attr);
					}
					else {
						int _port;
						try {
							_port = Integer.parseInt(port);
						}
						catch(NumberFormatException e) {
							ArrayList placeholder = new ArrayList(1);
							placeholder.add(port);
							throw new IPCException(new ClientException("500", "spc.remote.ipcsession.ni",placeholder,null));
						}
						_client = new TCPDefaultClient(host, _port, encoding, isDispatcher, attr);
					}
				}
				catch(ClientException e) {
					throw new IPCException(e);
				}
			}
			else {
				_client = client;
			}

			// initialize session
			try{
				_userName = (String)attr.get(ServerConstants.KEY_USERNAME/*DispatcherConstants.KEY_USERNAME*/);
				_userLocation = (String)attr.get(ServerConstants.KEY_USERLOCATION);
				_mandt = (String)attr.get(ServerConstants.KEY_CLIENT);
				_client.doCmd(com.sap.msasrv.module.system.ISystemCommandSet.CREATE_SESSION,
							  new String[] {
								CreateSession.APPLICATION, "IPC_TTE",
								CreateSession.CLIENT, _mandt,
								CreateSession.USER_LOCATION, _userLocation,
								CreateSession.USER_NAME, _userName,
								CreateSession.TYPE, type
							}
				);
				//setCacheDirty();
			}catch(ClientException e){
				throw new IPCException(e);
			}
			
			//Default IPC Session should always sync with server.
			syncWithServer();
		}

		protected void _defaultIPCSession(DefaultIPCClient ipcClient, String mandt, String type, TCPDefaultClient client,
							   String host, String port, String encoding, boolean isDispatcher, Properties securityProperties) throws IPCException {

			_documentTable = new Hashtable();
			_documents = null;
			_descriptionForProcedure = new Hashtable();
			_descriptionForCurrency  = new Hashtable();
			_ipcClient = ipcClient;

			Hashtable attr = com.sap.spc.remote.client.tcp.Client.getConnectAttributes(type,mandt);

			if (client == null) {
				try {
					if (port == null || host == null ||
						port.equals("") && host.equals("") ) {
						//fault back solution if port and host are empty
						// f.e. (Vehicle Manager & old R3 scenarios)
						_client = new TCPDefaultClient(attr, securityProperties);
					}
					else {
						int _port;
						try {
							_port = Integer.parseInt(port);
						}
						catch(NumberFormatException e) {
							ArrayList placeholder = new ArrayList(1);
							placeholder.add(port);
							throw new IPCException(new ClientException("500", "spc.remote.ipcsession.ni",placeholder,null));
						}
						_client = new TCPDefaultClient(host, _port, encoding, new Boolean(isDispatcher), attr, securityProperties);
					}
				}
				catch(ClientException e) {
					throw new IPCException(e);
				}
			}
			else {
				_client = client;
			}

			// initialize session
			try{
				_userName = (String)attr.get(ServerConstants.KEY_USERNAME);
				_userLocation = (String)attr.get(ServerConstants.KEY_USERLOCATION);
				_mandt = (String)attr.get(ServerConstants.KEY_CLIENT);
				_client.doCmd(com.sap.msasrv.module.system.ISystemCommandSet.CREATE_SESSION,
							  new String[] {
								CreateSession.APPLICATION, "IPC_TTE",
								CreateSession.CLIENT, _mandt,
								CreateSession.USER_LOCATION, _userLocation,
								CreateSession.USER_NAME, _userName,
								CreateSession.TYPE, type
							}
				);
				//setCacheDirty();
			}catch(ClientException e){
				throw new IPCException(e);
			}
			
			//Default IPC Session should always sync with server.
			syncWithServer();
		}


		//********Implementation for Abstract methods starts from here!********

	/* (non-Javadoc)
	 * @see com.sap.spc.remote.client.object.imp.DefaultIPCSession#initializeDocuments()
	 */
	protected void initializeDocuments() throws IPCException 
	{
	   try{
		   ServerResponse r = _client.doCmd(ISPCCommandSet.GET_DOCUMENTS, new String[0]);
		   String[] docIds = r.getParameterValues(GetDocuments.DOCUMENT_IDS);
		   for (int i=0; i<docIds.length; i++) {
			   String documentId = docIds[i];
			   IPCDocument document = factory.newIPCDocument(this, documentId);
			   _documentTable.put(documentId, document);
		   }
		   _documents = null;  // requires recalc
	   }catch(ClientException e){
		   throw new IPCException(e);
	   }
 }

	/* (non-Javadoc)
	 * @see com.sap.spc.remote.client.object.imp.DefaultIPCSession#initializeConfigs()
	 */
	protected void initializeConfigs() throws IPCException 
	{
	   try{
		   ServerResponse r = _client.doCmd(ISPCCommandSet.GET_CONFIGS, new String[0]);
		   String[] configIds = r.getParameterValues(GetConfigs.CONFIG_IDS);
		   if (configIds != null) {
			   for (int i=0; i<configIds.length; i++) {
				   String configId = configIds[i];
				   Configuration config = factory.newConfiguration(this, configId);
				   _configTable.put(configId, config);
			   }
		   }
	   }catch(ClientException e){
		   throw new IPCException(e);
	   }
 }

	/* (non-Javadoc)
	 * @see com.sap.spc.remote.client.object.IPCSession#getAvailablePricingProcedureNames(java.lang.String)
	 */
	public String[] getAvailablePricingProcedureNames(String application)
		throws IPCException 
		{
           location.entering("getAvailablePricingProcedureNames(String application)");
           if (location.beDebug()){
                location.debugT("Parameters:");
                location.debugT("String application " + application);
           }
		   String msg;
		   try{
			   String inputParameters[] = {
				    GetAllPricingProcedures.APPLICATION, application, 
					GetAllPricingProcedures.USAGE, "PR", 
					GetAllPricingProcedures.LANGUAGE, "EN"
				   };
			   // no point in calling this delayed since we immediately pass the result outside
			   ServerResponse r = _client.doCmd(ISPCCommandSet.GET_ALL_PRICING_PROCEDURES, inputParameters);
			   String procedures[] = r.getParameterValues(GetAllPricingProcedures.NAME);
			   String description[] = r.getParameterValues(GetAllPricingProcedures.DESCRIPTION);
			   for(int i = 0; i < procedures.length; i++) {
				   if(description[i] == null)
					   description[i] = "No Descritption";
				   _descriptionForProcedure.put(procedures[i], description[i]);
				   }
               if (location.beDebug()){
                   if (procedures != null) {
                       location.debugT("return procedures");
                       for (int i = 0; i<procedures.length; i++){
                           location.debugT("  procedures["+i+"] "+procedures[i]);
                       }
                   }
               }
			   return procedures;
		   }catch(ClientException e){
			   throw new IPCException(e);
		   }

		 }

	/* (non-Javadoc)
	 * @see com.sap.spc.remote.client.object.IPCSession#getAvailableCurrencyUnits()
	 */
	public String[] getAvailableCurrencyUnits() throws IPCException 
	{
     location.entering("getAvailableCurrencyUnits()");
	 try{
		 ServerResponse r = _client.doCmd(ISPCCommandSet.GET_ALL_CURRENCY_UNITS,new String[0]);
		 String name[] = r.getParameterValues(GetAllCurrencyUnits.NAME);
		 String description[] = r.getParameterValues(GetAllCurrencyUnits.DESCRIPTION);
		 int len = name.length;
		 for (int i = 0; i < len; i++) {
			 _descriptionForCurrency.put(name[i], description[i]);
		 }
         if (location.beDebug()){
             if (name != null) {
                 location.debugT("return name");
                 for (int i = 0; i<name.length; i++){
                     location.debugT("  name["+i+"] "+name[i]);
                 }
             }
         }
		 return name;
	 }catch(ClientException e){
		 throw new IPCException(e);
	 }
 }

	/* (non-Javadoc)
	 * @see com.sap.spc.remote.client.object.imp.DefaultIPCSession#determineDimensionalUnits(com.sap.spc.remote.client.object.ISOCodeConversionProperties[], java.util.Locale, java.lang.String, java.lang.String, boolean)
	 */
	protected void determineDimensionalUnits(
		ISOCodeConversionProperties[] props,
		Locale locale,
		String groupSeparator,
		String decimalSeparator,
		boolean currencyUnit)
		throws IPCException 
		{

			HashMap serverIndexToClientIndex = new HashMap();
			Vector parameters = new Vector();
			int serverIndex = 1;
			for (int i=0; i<props.length; i++) {
				String isoCode = props[i].getIsoCode();
				if (isoCode != null) {
					serverIndexToClientIndex.put(new Integer(serverIndex), new Integer(i));
					serverIndex++;
					parameters.addElement(ConvertIsoCodeToUnitOfMeasurement.ISO_CODES);
					parameters.addElement(isoCode);
					String value = props[i].getValue();
					parameters.addElement(ConvertIsoCodeToUnitOfMeasurement.VALUES);
					parameters.addElement(value == null ? "0" : value); // result will be ignored anyway
				}
			}

			if (locale != null) {
				parameters.addElement(ConvertIsoCodeToUnitOfMeasurement.LANGUAGE);
				parameters.addElement(locale.getLanguage().toUpperCase());
				parameters.addElement(ConvertIsoCodeToUnitOfMeasurement.COUNTRY);
				parameters.addElement(locale.getCountry().toUpperCase());
			}

			if (decimalSeparator != null) {
				parameters.addElement(ConvertIsoCodeToUnitOfMeasurement.DECIMAL_SEPARATOR);
				parameters.addElement(decimalSeparator);
			}
			if (groupSeparator != null) {
				parameters.addElement(ConvertIsoCodeToUnitOfMeasurement.GROUPING_SEPARATOR);
				parameters.addElement(groupSeparator);
			}

			parameters.addElement(ConvertIsoCodeToUnitOfMeasurement.CURRENCY_CONVERSION);
			parameters.addElement(currencyUnit ? "Y" : "N");

			String[] params = new String[parameters.size()];
			parameters.copyInto(params);
			try{
				ServerResponse r = _client.doCmd(ISPCCommandSet.CONVERT_ISO_CODE_TO_UNIT_OF_MEASUREMENT,
												 params);
				// update successful values
				String[] internalUnits = r.getParameterValues(ConvertIsoCodeToUnitOfMeasurement.INTERNAL_UNITS);
				String[] externalUnits = r.getParameterValues(ConvertIsoCodeToUnitOfMeasurement.EXTERNAL_UNITS);
				String[] formattedValues = r.getParameterValues(ConvertIsoCodeToUnitOfMeasurement.FORMATTED_VALUES);
				String[] refs = r.getParameterValues(ConvertIsoCodeToUnitOfMeasurement.REFS);
				if (internalUnits != null) {
					for (int i=0; i<internalUnits.length; i++) {
						Integer ref = new Integer(refs[i]);
						Integer origRef = (Integer)serverIndexToClientIndex.get(ref);
						if (origRef == null) {
							throw new IllegalStateException("IPC Command ConvertIsoCodeToUnitOfMeasurement returned index that was not in the legal range");
						}
						int oRef = origRef.intValue();
						ISOCodeConversionProperties p = props[oRef];
						p.setInternalUnit(internalUnits[i]);
						if (externalUnits != null)
							p.setExternalUnit(externalUnits[i]);
						if (formattedValues != null)
							p.setFormattedValue(formattedValues[i]);
						p.setErrorMessage(null);
					}
				}

				// update unsuccessful values
				String[] errorMessages = r.getParameterValues(ConvertIsoCodeToUnitOfMeasurement.ERROR_MESSAGES);
				String[] errorMessageRefs = r.getParameterValues(ConvertIsoCodeToUnitOfMeasurement.ERROR_MESSAGE_REFS);
				if (errorMessages != null) {
					for (int i=0; i<errorMessages.length; i++) {
						Integer ref = new Integer(errorMessageRefs[i]);
						Integer origRef = (Integer)serverIndexToClientIndex.get(ref);
						if (origRef == null) {
							throw new IllegalStateException("IPC Command ConvertIsoCodeToUnitOfMeasurement returned index that was not in the legal range");
						}
						int oRef = origRef.intValue();
						ISOCodeConversionProperties p = props[oRef];
						p.setErrorMessage(errorMessages[i]);
					}
				}
			}catch(ClientException e){
				throw new IPCException(e);
			}
		}

	/* (non-Javadoc)
	 * @see com.sap.spc.remote.client.object.IPCSession#close()
	 */
	public void close() 
	{
       location.entering("close()");
//	   try {
		   if (_client != null) {
			   //_client.cmd(com.sap.sxe.socket.shared.command.CommandConstants.CLOSE_SESSION, new String[0]);
			   _client.close();
			   _client = null;
		   }else {
			   //the connection is already closed dont have to do anything
		   }
//	   }
//	   catch(ClientException e) {
//		   // when we try to close the session and don't have a connection, it's okay just to do nothing
//	   }
	   _documents = null;
       location.exiting();
 }

	//********Implementation for Abstract methods ends here!********

	public void removeDocument(IPCDocument document) throws IPCException {
         location.entering("removeDocument(IPCDocument document)");
         if (location.beDebug()){
            location.debugT("Parameters:");
            location.debugT("IPCDocument document " + document.getId());
         }
		 // before or after? In case of remove it seems to be reasonable to remove the document
		 // on the client even if the IPC command fails later.
		 String documentId = document.getId();
		//No need to call a sync, which was a performance hit. just remove from data strecture
		 _documentTable.remove(documentId);
		 _documents = null;
		 // kha: changed command to doCmd because after removing a document, obviously no getter on
		 // this document would be there, causing RemoveDocument to stay in the client cache, never
		 // to be executed...
		 try{
			 _client.doCmd(ISPCCommandSet.REMOVE_DOCUMENT,
						   new String[] { RemoveDocument.DOCUMENT_ID, documentId });
             location.exiting();
		 }catch(ClientException e){
			 throw new IPCException(e);
		 }

	 }
	 
	 public String getSessionId() {
         location.entering("getSessionId()");
         String sessionId = _client.getSessionId();
         if (location.beDebug()) {
             location.debugT("return sessionId " + sessionId);
         }
         location.exiting();
		 return sessionId;
	 }
	 
		//	 direct method from protocol layer
	 public IClient getClient() {
         location.entering("getClient()");
         if (location.beDebug()) {
             if (_client!=null){
                 location.debugT("return _client " + _client.toString());
             }
             else location.debugT("return _client null");
         }
         location.exiting();
		 return _client;
	 }
	 
	 /**
	  * Sets Cache as diety.
	  */
	 public void setCacheDirty() {
        location.entering("setCacheDirty()");
		if (!cacheIsDirty){
			category.log(Severity.DEBUG,location,"Setting session cache as Dirty.");
			_setSessionDocsFlagToDirty();
			_setSessionFlagToDirty();
		}
        location.exiting();
	 }
			
	 public void syncWithServer() throws IPCException{
        location.entering("syncWithServer()");
	 	//If cacheIs not dirty don't sync.
	 	if (!cacheIsDirty) return;
	 	
		category.log(Severity.DEBUG,location,"Client session cache is dirty. Syncing client session with Server....");
	 	
	 	//get Documents
		try{
			ServerResponse r = _client.doCmd(ISPCCommandSet.GET_DOCUMENTS, new String[0]);
			String[] docIds = r.getParameterValues(GetDocuments.DOCUMENT_IDS);
			if (docIds != null) {
				for (int i=0; i<docIds.length; i++) {
					String documentId = docIds[i];
					if (getDocument(documentId) == null){
						IPCDocument document = factory.newIPCDocument(this, documentId);
						_documentTable.put(documentId, document);
					}
				}
			}
			_documents = null;  // requires recalc
		}catch(ClientException e){
			throw new IPCException(e);
		}
		
		//Get Configs
		try{
			ServerResponse r = _client.doCmd(ISPCCommandSet.GET_CONFIGS, new String[0]);
			String[] configIds = r.getParameterValues(GetConfigs.CONFIG_IDS);
			if (configIds != null) {
				for (int i=0; i<configIds.length; i++) {
					String configId = configIds[i];
					if (getConfiguration(configId) == null){
						Configuration config = factory.newConfiguration(this, configId);
						_configTable.put(configId, config);
					}
				}
			}
		}catch(ClientException e){
			throw new IPCException(e);
		}
		
		 cacheIsDirty = false;
         location.entering();
	 }
	 
	public void lock() {
	 	//Don't throw any exception: Applications can try to lock the session, but in MSA scenario no lock is possible/required.
	 	//throw new IPCException(IPCException.UNIMPLEMENTED);
	 }
	 
	 public void publish() {
		//Don't throw any exception: Applications can try to lock the session, but in MSA scenario no lock is possible/required.
		//throw new IPCException(IPCException.UNIMPLEMENTED);	 	
	 }

	/* (non-Javadoc)
	 * @see com.sap.spc.remote.client.object.IPCSession#unlock()
	 */
	public void unlock() {
		//Don't throw any exception: Applications can try to lock the session, but in MSA scenario no lock is possible/required.
		//throw new IPCException(IPCException.UNIMPLEMENTED);		
	}

	/* (non-Javadoc)
	 * @see com.sap.spc.remote.client.object.IPCSession#isLocked()
	 * Clients should not try to lock the session, since there is no socket command available.
	 * The API is intended only for the RFC case.
	 */
	public boolean isLocked() {
		return true;
	}

	/* (non-Javadoc)
	 * @see com.sap.spc.remote.client.object.IPCSession#getLanguage()
	 * The language of the session is determined by the language of the
	 * RFC connection.
	 * In the TCP case the client session currently does not
	 * have a language. The method getLanguage is not supposed to be called.
	 */
	public String getLanguage() {
        location.entering("getLanguage()");
        if (location.beDebug()) {
            location.debugT("return \"\"");
        }
        location.exiting();
		return "";
	}

	 
}
