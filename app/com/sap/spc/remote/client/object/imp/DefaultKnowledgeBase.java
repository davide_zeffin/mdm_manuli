package com.sap.spc.remote.client.object.imp;

import java.util.List;
import java.util.Vector;

import com.sap.isa.businessobject.BusinessObjectBase;
import com.sap.isa.core.TechKey;
import com.sap.spc.remote.client.object.IPCClient;
import com.sap.spc.remote.client.object.KBProfile;
import com.sap.spc.remote.client.object.KnowledgeBase;

public class DefaultKnowledgeBase extends BusinessObjectBase implements KnowledgeBase {
    
    protected IPCClientObjectFactory factory =
            IPCClientObjectFactory.getInstance(); 
    protected IPCClient ipcClient;
    protected String m_Logsys;
    protected String m_Name;
    protected String m_Version;
    protected Vector profiles = new Vector();
    protected String m_BuildNumber;
    protected boolean m_Closed;

    DefaultKnowledgeBase(
        IPCClient ipcClient,
        String logsys,
        String name,
        String version,
        String buildNumber) {
        	super(new TechKey(logsys + name + version + buildNumber));
        this.ipcClient = ipcClient;
        m_Logsys = logsys;
        m_Name = name;
        m_Version = version;
        m_BuildNumber = buildNumber;
        m_Closed = false;
    }

    /**
     * Returns the logical system of the knowledge base
     */
    public String getLogsys() {
        return m_Logsys;
    }
    /**
     * returns the name of the knowledgebase
     */
    public String getName() {
        return (m_Name == null) ? "N/A" : m_Name;
    }

    /**
     * returns the version of the knowledgebase
     */
    public String getVersion() {
        return (m_Version == null) ? "N/A" : m_Version;
    }

    /**
     * returns the profile of the knowledgebase
     */
    public List getProfiles() {
        return profiles;
    }

    /**
     * returns the build number of this knowledgebase
     */
    public String getBuildNumber() {
        return (m_BuildNumber == null) ? "N/A" : m_BuildNumber;
    }

    public void close() {
        m_Closed = true;
    }

    public boolean isClosed() {
        return m_Closed;
    }

    public boolean isRelevant(String commandName) {
        // we don't use the cache
        return true;
    }

    /**
     * @see com.sap.spc.remote.client.object.KnowledgeBase#addProfile(KBProfile)
     */
    public KBProfile createProfile(String profilename) {
        DefaultKBProfile kbProfile = factory.newKBProfile(profilename, this);
        profiles.add(kbProfile);
        return kbProfile;
    }

    /**
     * @see com.sap.spc.remote.client.object.KnowledgeBase#getIPCClient()
     */
    public IPCClient getIPCClient() {
        return this.ipcClient;
    }

}
