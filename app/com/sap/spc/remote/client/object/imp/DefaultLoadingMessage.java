
package com.sap.spc.remote.client.object.imp;

import com.sap.isa.businessobject.BusinessObjectBase;
import com.sap.isa.core.TechKey;
import com.sap.spc.remote.client.object.LoadingMessage;

public class DefaultLoadingMessage extends BusinessObjectBase implements LoadingMessage {

    private String instId;
    private String csticName;
    private int number;
    private String messageClass;
    private int severity;
    private String text;
    private String[] arguments;

    /**
     * @param instId Instance id associated with this message.
     * @param csticName Characteristic name associated with this message.
     * @param number message number
     * @param messageClass message class
     * @param severity message severity
     * @param text message text
     * @param arguments array of message arguments
     */
    DefaultLoadingMessage(String instId, String csticName, int number, String messageClass, int severity, String text, String[] arguments) {
        super(new TechKey(instId + "." + csticName + "." + number));
        this.instId = instId;
        this.csticName = csticName;
        this.number = number;
        this.messageClass = messageClass;
        this.severity = severity;
        this.text = text;
        this.arguments = arguments; 
    }
    
    /* (non-Javadoc)
     * @see com.sap.spc.remote.client.object.LoadingMessage#getConfigObjKey()
     */
    public String getConfigObjKey() {
        String configObjKey = instId;
        if ((csticName != null) && !csticName.equals("")){
            configObjKey = configObjKey + "." + csticName;
        }
        return configObjKey;
    }

    /* (non-Javadoc)
     * @see com.sap.spc.remote.client.object.LoadingMessage#getNumber()
     */
    public int getNumber() {
        return number;
    }

    /* (non-Javadoc)
     * @see com.sap.spc.remote.client.object.LoadingMessage#getSeverity()
     */
    public int getSeverity() {
        return severity;
    }

    /* (non-Javadoc)
     * @see com.sap.spc.remote.client.object.LoadingMessage#isErrorMessage()
     */
    public boolean isErrorMessage() {
        return (severity == ERROR);
    }

    /* (non-Javadoc)
     * @see com.sap.spc.remote.client.object.LoadingMessage#isWarningMessage()
     */
    public boolean isWarningMessage() {
        return (severity == WARNING);
    }

    /* (non-Javadoc)
     * @see com.sap.spc.remote.client.object.LoadingMessage#isInfoMessage()
     */
    public boolean isInfoMessage() {
        return (severity == INFO);
    }

    /* (non-Javadoc)
     * @see com.sap.spc.remote.client.object.LoadingMessage#getText()
     */
    public String getText() {
        return text;
    }

    /* (non-Javadoc)
     * @see com.sap.spc.remote.client.object.LoadingMessage#getArguments()
     */
    public String[] getArguments() {
        return arguments;
    }

    /* (non-Javadoc)
     * @see com.sap.spc.remote.client.object.LoadingMessage#getCsticName()
     */
    public String getCsticName() {
        return csticName;
    }

    /* (non-Javadoc)
     * @see com.sap.spc.remote.client.object.LoadingMessage#getInstId()
     */
    public String getInstId() {
        return instId;
    }

    /* (non-Javadoc)
     * @see com.sap.spc.remote.client.object.LoadingMessage#getMessageClass()
     */
    public String getMessageClass() {
        return messageClass;
    }

    /* (non-Javadoc)
     * @see com.sap.spc.remote.client.object.LoadingMessage#setText(java.lang.String)
     */
    public void setText(String text) {
        this.text = text;
    }

}
