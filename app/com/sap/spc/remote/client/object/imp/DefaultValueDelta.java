package com.sap.spc.remote.client.object.imp;

import com.sap.isa.businessobject.BusinessObjectBase;
import com.sap.spc.remote.client.object.ValueDelta;

public class DefaultValueDelta extends BusinessObjectBase implements ValueDelta {

    private String configObjectKey;
    private String csticName;
    private String value;
    private String author;
    private String type;
    private String unit;

    /**
     * @param configObjectKey unique key of the configuration object to this value delta belongs to
     * @param csticName name of the characteristic the value delta belongs to
     * @param value the value of the value delta
     * @param unit the unit of the value 
     * @param author author of the delta
     * @param type type of delta (D, I)
     */
    public DefaultValueDelta(String configObjectKey, String csticName, String value, String unit, String author, String type) {
        super();
        this.configObjectKey = configObjectKey;
        this.csticName = csticName;
        this.value = value;
        this.author = author;
        this.type = type;
        this.unit = unit;
    }

    /* (non-Javadoc)
     * @see com.sap.spc.remote.client.object.ValueDelta#getAuthor()
     */
    public String getAuthor() {
        return author;
    }

    /* (non-Javadoc)
     * @see com.sap.spc.remote.client.object.ValueDelta#getType()
     */
    public String getType() {
        return type;
    }

    /* (non-Javadoc)
     * @see com.sap.spc.remote.client.object.ValueDelta#getConfigObjectKey()
     */
    public String getConfigObjectKey() {
        return configObjectKey;
    }

    /* (non-Javadoc)
     * @see com.sap.spc.remote.client.object.ValueDelta#getCsticName()
     */
    public String getCsticName() {
        return csticName;
    }

    /* (non-Javadoc)
     * @see com.sap.spc.remote.client.object.ValueDelta#getValue()
     */
    public String getValue() {
        return value;
    }

    /* (non-Javadoc)
     * @see com.sap.spc.remote.client.object.ValueDelta#getUnit()
     */
    public String getUnit() {
        return unit;
    }

}
