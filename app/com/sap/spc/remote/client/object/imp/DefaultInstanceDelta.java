package com.sap.spc.remote.client.object.imp;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import com.sap.isa.businessobject.BusinessObjectBase;
import com.sap.spc.remote.client.object.InstanceDelta;
import com.sap.spc.remote.client.object.ValueDelta;

public class DefaultInstanceDelta extends BusinessObjectBase implements InstanceDelta {

    public static final DefaultInstanceDelta C_NULL = new DefaultInstanceDelta("","","","",null,null,null,"","");
    private String instId1;
    private String instId2;
    private String itemId;
    private String decompPath;
    private List deltas;
    private List partOfDeltas;
    private List valueDeltas;
    private HashMap valueDeltasByConfigObjectKey;    
    private String author;
    private String type;


    /**
     * @param id1 id of instance 1
     * @param id2 id of instance 2
     * @param itemId item id (if available)
     * @param decompPath decomposition path
     * @param deltas list of deltas on instance level
     * @param partOfDeltas list of part of deltas
     * @param valueDeltas list of value deltas
     * @param author author of the instance delta
     * @param type type of delta (D, I, U)
     */
    protected DefaultInstanceDelta(String id1, 
                                String id2, 
                                String itemId, 
                                String decompPath, 
                                List deltas, 
                                List partOfDeltas,
                                List valueDeltas,
                                String author,
                                String type) {
        super();
        this.instId1 = id1;
        this.instId2 = id2;
        this.itemId = itemId;
        this.decompPath = decompPath;
        this.deltas = deltas;
        this.partOfDeltas = partOfDeltas;
        this.valueDeltas = valueDeltas;
        this.author = author;
        this.type = type;
        this.valueDeltasByConfigObjectKey = initValueDeltasByConfigObjectKey(valueDeltas);
    }

    /**
     * Sorts the valueDeltas by their configObject keys and puts them into a <code>HashMap</code>.
     * For each configuration object an <code>ArrayList</code> object is generated and 
     * added to the <code>HashMap</code> with the configObjectKey as key 
     * (pattern: "<instId>.<csticName>").
     * @param valueDeltas list of value delta objects
     * @return <code>HashMap</code> with an <code>ArrayList</code> for each configuration object that has value deltas
     */
    private HashMap initValueDeltasByConfigObjectKey(List valueDeltas) {
        HashMap vdMap = new HashMap();
        if (valueDeltas != null){
            String previousConfigObjectKey = "";
            for (int i=0; i<valueDeltas.size(); i++ ){
                ValueDelta vd = (ValueDelta)valueDeltas.get(i);
                if (vd.getConfigObjectKey().equals(previousConfigObjectKey)) {
                    // there is already a list for this configObject in the map
                    ArrayList deltaList = (ArrayList)vdMap.get(vd.getConfigObjectKey());
                    // add the delta to the list
                    deltaList.add(vd);
                }
                else {
                    // create a new list for this configObject
                    ArrayList deltaList = new ArrayList();
                    // put it into the map
                    vdMap.put(vd.getConfigObjectKey(), deltaList);
                    // add the delta to the list
                    deltaList.add(vd);
                    // store the configObject key
                    previousConfigObjectKey = vd.getConfigObjectKey();
                }
            }
        }
        return vdMap;
    }

    /* (non-Javadoc)
     * @see com.sap.spc.remote.client.object.InstanceDelta#getAuthor()
     */
    public String getAuthor() {
        return author;
    }

    /* (non-Javadoc)
     * @see com.sap.spc.remote.client.object.InstanceDelta#getDecompPath()
     */
    public String getDecompPath() {
        return decompPath;
    }

    /* (non-Javadoc)
     * @see com.sap.spc.remote.client.object.InstanceDelta#getDeltas()
     */
    public List getDeltas() {
        return deltas;
    }

    /* (non-Javadoc)
     * @see com.sap.spc.remote.client.object.InstanceDelta#getInstId1()
     */
    public String getInstId1() {
        return instId1;
    }

    /* (non-Javadoc)
     * @see com.sap.spc.remote.client.object.InstanceDelta#getInstId2()
     */
    public String getInstId2() {
        return instId2;
    }

    /* (non-Javadoc)
     * @see com.sap.spc.remote.client.object.InstanceDelta#getItemId()
     */
    public String getItemId() {
        return itemId;
    }

    /* (non-Javadoc)
     * @see com.sap.spc.remote.client.object.InstanceDelta#getPartOfDeltas()
     */
    public List getPartOfDeltas() {
        return partOfDeltas;
    }

    /* (non-Javadoc)
     * @see com.sap.spc.remote.client.object.InstanceDelta#getType()
     */
    public String getType() {
        return type;
    }

    /* (non-Javadoc)
     * @see com.sap.spc.remote.client.object.InstanceDelta#getValueDeltas()
     */
    public List getValueDeltas() {
        return valueDeltas;
    }

    /* (non-Javadoc)
     * @see com.sap.spc.remote.client.object.InstanceDelta#getValueDeltasByConfigObjectKey()
     */
    public HashMap getValueDeltasByConfigObjectKey() {
        return valueDeltasByConfigObjectKey;
    }

}
