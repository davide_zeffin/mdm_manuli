package com.sap.spc.remote.client.object.imp.tcp;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.TreeMap;
import java.util.Vector;

import com.sap.msasrv.usermodule.spc.ISPCCommandSet;
import com.sap.spc.remote.client.ClientException;
import com.sap.spc.remote.client.ResourceAccessor;
import com.sap.spc.remote.client.object.Characteristic;
import com.sap.spc.remote.client.object.IPCClient;
import com.sap.spc.remote.client.object.IPCException;
import com.sap.spc.remote.client.object.IPCItem;
import com.sap.spc.remote.client.object.IPCSession;
import com.sap.spc.remote.client.object.Instance;
import com.sap.spc.remote.client.object.MimeObjectContainer;
import com.sap.spc.remote.client.object.ProductVariant;
import com.sap.spc.remote.client.object.ProductVariantFeature;
import com.sap.spc.remote.client.object.imp.ConfigurationChangeStream;
import com.sap.spc.remote.client.object.imp.DefaultCharacteristic;
import com.sap.spc.remote.client.object.imp.DefaultCharacteristicGroup;
import com.sap.spc.remote.client.object.imp.DefaultConfiguration;
import com.sap.spc.remote.client.object.imp.DefaultIPCSession;
import com.sap.spc.remote.client.object.imp.DefaultInstance;
import com.sap.spc.remote.client.object.imp.GroupCache;
import com.sap.spc.remote.client.object.imp.IllegalClassException;
import com.sap.spc.remote.client.tcp.ServerResponse;
import com.sap.spc.remote.shared.command.ChangeItemProduct;
import com.sap.spc.remote.shared.command.CreateInstance;
import com.sap.spc.remote.shared.command.GetCsticGroups;
import com.sap.spc.remote.shared.command.GetFilteredCsticsAndValues;
import com.sap.spc.remote.shared.command.GetProductVariants;
import com.sap.spc.remote.shared.command.SCECommand;
import com.sap.spc.remote.shared.command.Specialize;
import com.sap.spc.remote.shared.command.Unspecialize;
import com.sap.tc.logging.Location;
import com.sap.tc.logging.Severity;

public class TcpDefaultInstance extends DefaultInstance implements Instance {

	protected static final Location location = ResourceAccessor.getLocation(TcpDefaultInstance.class);
    
    protected TcpDefaultInstance(
        ConfigurationChangeStream configurationChangeStream,
        IPCClient ipcClient,
        DefaultConfiguration configuration,
        String instanceId,
        String parentId,
        boolean complete,
        boolean consistent,
        String quantity,
        String name,
        String languageDependentName,
        String description,
        String bomPosition,
        String price,
        boolean userOwned,
        boolean configurable,
        boolean specializable,
        boolean unspecializable,
        MimeObjectContainer mimeObjects) {
        init(
            configurationChangeStream,
            ipcClient,
            configuration,
            instanceId,
            parentId,
            complete,
            consistent,
            quantity,
            name,
            languageDependentName,
            description,
            bomPosition,
            price,
            userOwned,
            configurable,
            specializable,
            unspecializable,
            mimeObjects);
    }

    /* (non-Javadoc)
     * @see com.sap.spc.remote.client.object.imp.DefaultInstance#init(com.sap.spc.remote.client.object.imp.ConfigurationChangeStream, com.sap.spc.remote.client.object.IPCClient, com.sap.spc.remote.client.object.imp.DefaultConfiguration, java.lang.String, java.lang.String, boolean, boolean, java.lang.String, java.lang.String, java.lang.String, java.lang.String, java.lang.String, java.lang.String, boolean, boolean, boolean, boolean, com.sap.spc.remote.client.object.MimeObjectContainer)
     */
    public void init(
        
        ConfigurationChangeStream configurationChangeStream,
        IPCClient ipcClient,
        DefaultConfiguration configuration,
        String instanceId,
        String parentId,
        boolean complete,
        boolean consistent,
        String quantity,
        String name,
        String languageDependentName,
        String description,
        String bomPosition,
        String price,
        boolean userOwned,
        boolean configurable,
        boolean specializable,
        boolean unspecializable,
        MimeObjectContainer mimeObjects) {
            
            location.entering("init(ConfigurationChangeStream configurationChangeStream, "
                +"IPCClient ipcClient, DefaultConfiguration configuration, String instanceId, "
                +"String parentId, boolean complete, boolean consistent, String quantity, String name, "
                +"String languageDependentName, String description, String bomPosition, String price, "
                +"boolean userOwned, boolean configurable, boolean specializable, "
                +"boolean unspecializable, MimeObjectContainer mimeObjects)");
            if (location.beDebug()) {
                location.debugT("Parameters:");
                if (configurationChangeStream!=null){
                    location.debugT("ConfigurationChangeStream configurationChangeStream " + configurationChangeStream.toString());
                }
                else location.debugT("ConfigurationChangeStream configurationChangeStream null");
                location.debugT("DefaultConfiguration configuration " + configuration.getId());
                location.debugT("String instanceId " + instanceId);
                location.debugT("String parentId " + parentId);
                location.debugT("boolean complete " + complete);
                location.debugT("boolean consistent " + consistent);
                location.debugT("String quantity " + quantity);
                location.debugT("String name " + name);
                location.debugT("String languageDependentName " + languageDependentName);
                location.debugT("String description " + description);
                location.debugT("String bomPosition " + bomPosition);
                location.debugT("String price " + price);
                location.debugT("boolean userOwnded " + userOwned);
                location.debugT("boolean configurable " + configurable);
                location.debugT("boolean specializable " + specializable);
                location.debugT("boolean unspecializable " + unspecializable);
                if (mimeObjects!=null){
                    location.debugT("MimeObjectContainer mimeObjects " + mimeObjects.toString());
                }
                else location.debugT("MimeObjectContainer mimeObjects null");
            }
            
            this.configurationChangeStream = configurationChangeStream;
            this.ipcClient = ipcClient;
            this.configuration = configuration;
            ipcDocument = null;
            ipcItem = null;
            this.instanceId = instanceId;
            this.parentId = parentId;
            this.complete = complete;
            this.consistent = consistent;
            this.quantity = quantity;
            this.name = name;
            this.languageDependentName = languageDependentName;
            this.description = description;
            this.position = bomPosition;
            this.price = price;
            this.userOwned = userOwned;
            this.configurable = configurable;
            this.specializable = specializable;
            this.unspecializable = unspecializable;
            this.closed = false;
            IPCSession session = ipcClient.getIPCSession();
            if (!(session instanceof DefaultIPCSession))
                throw new IllegalClassException(session, DefaultIPCSession.class);
            this.cachingClient = ((DefaultIPCSession) session).getClient();
            this.mimeObjects =
                (mimeObjects == null)
                    ? factory.newMimeObjectContainer()
                    : mimeObjects;
            this.cacheIsDirty = true;
            // remove all existing children
            this.childInstances = new ArrayList();
            location.exiting();
    }

    /* (non-Javadoc)
     * @see com.sap.spc.remote.client.object.imp.DefaultInstance#synchronize()
     */
    protected void synchronize() throws IPCException {
        location.entering("synchronize()");
        try {
            if (groupCaches == null) {
                location.debugT("groupCaches is null; retrieving info from AP-CFG");
                // Use optimized server command (requires extended server!)
                // generate group cache
                ServerResponse r =
                ((TCPDefaultClient)cachingClient).doCmd(
                        ISPCCommandSet.GET_CSTIC_GROUPS,
                        new String[] {
                            SCECommand.CONFIG_ID,
                            configuration.getId(),
                            GetCsticGroups.INST_ID,
                            getId()});
                String groupNames[] =
                    r.getParameterValues(GetCsticGroups.CSTIC_GROUP);
                String groupConsistent[] =
                    r.getParameterValues(GetCsticGroups.GROUP_CONSISTENT);
                String groupRequired[] =
                    r.getParameterValues(GetCsticGroups.GROUP_REQUIRED);

                // generate cache
                groups = new TreeMap();
                groupCaches = new HashMap();
                orderedGroups_GeneralGroupFirst =
                    new ArrayList(
                        (groupNames != null) ? groupNames.length + 1 : 1);
                orderedGroups_GeneralGroupLast =
                    new ArrayList(
                        (groupNames != null) ? groupNames.length + 1 : 1);
                csticsList = new ArrayList();  

                // Add the base group (used for all unassigned values.
                GroupCache gCache = new GroupCache();
                    
                    // no cstics, yet
                    gCache.setGroup(factory.newBaseGroup(this, new ArrayList(), 0));
                // Define the variable baseGroup to enable the setting of the
                // base group either at the beginning or at the end of the group list
                DefaultCharacteristicGroup baseGroup = gCache.getGroup();
                gCache.setInterest(GroupCache.NO_INTEREST);
                gCache.setRequiredRefresh(GroupCache.REFRESH_REQUIRED_FOR_ALL_CSTICS);
                groupCaches.put(baseGroup.getName(), gCache);
                orderedGroups_GeneralGroupFirst.add(baseGroup);
                groups.put(baseGroup.getName(), baseGroup);
                for (int i = 0;
                    groupNames != null && i < groupNames.length;
                    i++) {

                    gCache = new GroupCache();
                        gCache.setGroup(factory.newCharacteristicGroup(
                                            this,
                                            groupNames[i],
                                            groupNames[i], //Use langdep Names as soon as available
                                            new ArrayList(), // no cstics, yet
                                            i + 1));
                    gCache.getGroup().setConsistent(
                        groupConsistent[i].equals(GetCsticGroups.TRUE)
                            ? true
                            : false);
                    gCache.getGroup().setRequired(
                        groupRequired[i].equals(GetCsticGroups.TRUE)
                            ? true
                            : false);
                    gCache.setInterest(GroupCache.NO_INTEREST);
                    gCache.setRequiredRefresh(GroupCache.REFRESH_REQUIRED_FOR_ALL_CSTICS);
                    groupCaches.put(gCache.getGroup().getName(), gCache);
                    orderedGroups_GeneralGroupFirst.add(gCache.getGroup());
                    orderedGroups_GeneralGroupLast.add(gCache.getGroup());
                    groups.put(gCache.getGroup().getName(), gCache.getGroup());
                }
                orderedGroups_GeneralGroupLast.add(baseGroup);

            } else {
                // Update the cache
                location.debugT("groupCaches is not null; update the groupCache");
                Iterator cacheIt = groupCaches.values().iterator();
                while (cacheIt.hasNext()) {
                    GroupCache gCache = (GroupCache) cacheIt.next();

                    if (gCache.getInterest() != GroupCache.NO_INTEREST) {
                        updateGroupCache(gCache);
                    }
                }
            }
        } catch (ClientException e) {
            category.logT(Severity.ERROR, location, e.toString());
            throw new IPCException(e);
        }
        location.exiting();
    }

    /* (non-Javadoc)
     * @see com.sap.spc.remote.client.object.imp.DefaultInstance#updateGroupCache(com.sap.spc.remote.client.object.imp.DefaultInstance.GroupCache)
     */
    protected void updateGroupCache(GroupCache gCache) throws ClientException {
        location.entering("updateGroupCache(GroupCache gCache)");
        if (location.beDebug()) {
            location.debugT("Parameters:");
            location.debugT("GroupCache gCache: Group Name: " 
                                + gCache.getGroup().getName() + " Interest: " 
                                + gCache.getInterest() + " Required Refresh: " 
                                + gCache.getRequiredRefresh());
        }
        // verify, if an update is required!
        // first case:
        //  if interest is INTEREST_IN_ALL_CSTICS 
        //  and if requiredRefresh is REFRESH_REQUIRED_FOR_INVISIBLE_CSTICS,
        //                         or REFRESH_REQUIRED_FOR_VISIBLE_CSTICS
        //                         or REFRESH_REQUIRED_FOR_ALL_CSTICS
        // second case:
        //  if interst is INTEREST_IN_VISIBLE_CSTICS_ONLY
        //  and if requiredRefresh is REFRESH_REQUIRED_FOR_VISIBLE_CSTICS
        //                         or REFRESH_REQUIRED_FOR_ALL_CSTICS 
        if ((gCache.getInterest() == GroupCache.INTEREST_IN_ALL_CSTICS
            && gCache.getRequiredRefresh() != GroupCache.NO_REFRESH_REQUIRED)
            || (gCache.getInterest() == GroupCache.INTEREST_IN_VISIBLE_CSTICS_ONLY
                && (gCache.getRequiredRefresh()
                    & GroupCache.REFRESH_REQUIRED_FOR_VISIBLE_CSTICS)
                    != 0)) {
            int i;
            HashMap previouslyKnownCstics = new HashMap();
            HashMap csticsInGroup = new HashMap();
            DefaultCharacteristic cstic;

            StringBuffer unknownCsticsInterest = new StringBuffer();
            // unknownCsticsInterest for cstics
            unknownCsticsInterest.append(
                GetFilteredCsticsAndValues.INTEREST_CSTIC_LNAMES);
            unknownCsticsInterest.append(
                GetFilteredCsticsAndValues.INTEREST_CSTIC_DOMAIN_IS_INTERVAL);
            unknownCsticsInterest.append(
                GetFilteredCsticsAndValues.INTEREST_CSTIC_VISIBLES);
            unknownCsticsInterest.append(
                GetFilteredCsticsAndValues.INTEREST_CSTIC_ADDITS);
            unknownCsticsInterest.append(
                GetFilteredCsticsAndValues.INTEREST_CSTIC_CONSISTENTS);
            unknownCsticsInterest.append(
                GetFilteredCsticsAndValues.INTEREST_CSTIC_CONSTRAINEDS);
            unknownCsticsInterest.append(
                GetFilteredCsticsAndValues.INTEREST_CSTIC_GROUPS);
            unknownCsticsInterest.append(
                GetFilteredCsticsAndValues.INTEREST_CSTIC_MULTIS);
            unknownCsticsInterest.append(
                GetFilteredCsticsAndValues.INTEREST_CSTIC_READONLYS);
            unknownCsticsInterest.append(
                GetFilteredCsticsAndValues.INTEREST_CSTIC_REQUIREDS);
            unknownCsticsInterest.append(
                GetFilteredCsticsAndValues.INTEREST_CSTIC_UNITS);
            unknownCsticsInterest.append(
                GetFilteredCsticsAndValues.INTEREST_CSTIC_ENTRY_FIELD_MASK);
            unknownCsticsInterest.append(
                GetFilteredCsticsAndValues.INTEREST_CSTIC_TYPELENGTH);
            unknownCsticsInterest.append(
                GetFilteredCsticsAndValues.INTEREST_CSTIC_NUMBERSCALE);
            unknownCsticsInterest.append(
                GetFilteredCsticsAndValues.INTEREST_CSTIC_VALUETYPE);
            unknownCsticsInterest.append(
                GetFilteredCsticsAndValues.INTEREST_CSTIC_GROUP_POSITION);
            unknownCsticsInterest.append(
                GetFilteredCsticsAndValues.INTEREST_CSTIC_INST_POSITION);
            unknownCsticsInterest.append(
                GetFilteredCsticsAndValues.INTEREST_CSTIC_VIEWS);
            unknownCsticsInterest.append(
                GetFilteredCsticsAndValues.INTEREST_CSTIC_EXPANDEDS);
            unknownCsticsInterest.append(
                GetFilteredCsticsAndValues.INTEREST_CSTIC_MIMES);
            // unknownCsticsInterest for values
            unknownCsticsInterest.append(
                GetFilteredCsticsAndValues.INTEREST_VALUE_ASSIGNEDS);
            unknownCsticsInterest.append(
                GetFilteredCsticsAndValues.INTEREST_VALUE_AUTHORS);
            unknownCsticsInterest.append(
                GetFilteredCsticsAndValues.INTEREST_VALUE_DEFAULTS);
            unknownCsticsInterest.append(
                GetFilteredCsticsAndValues.INTEREST_VALUE_LNAMES);
            unknownCsticsInterest.append(
                GetFilteredCsticsAndValues.INTEREST_VALUE_LPRICES);
            unknownCsticsInterest.append(
                GetFilteredCsticsAndValues.INTEREST_VALUE_PRICES);
            unknownCsticsInterest.append(
                    GetFilteredCsticsAndValues.INTEREST_VALUE_IS_DOMAIN_VALUE);
            unknownCsticsInterest.append(
                GetFilteredCsticsAndValues.INTEREST_VALUE_MIMES);

            StringBuffer knownCsticsInterest = new StringBuffer();
            
            // knownCsticsInterest for cstics
            knownCsticsInterest.append(
                GetFilteredCsticsAndValues.INTEREST_CSTIC_CONSISTENTS);
            knownCsticsInterest.append(
                GetFilteredCsticsAndValues.INTEREST_CSTIC_REQUIREDS);
            knownCsticsInterest.append(
                GetFilteredCsticsAndValues.INTEREST_CSTIC_VISIBLES);
            knownCsticsInterest.append(
                GetFilteredCsticsAndValues.INTEREST_CSTIC_READONLYS);
            knownCsticsInterest.append(
                GetFilteredCsticsAndValues.INTEREST_CSTIC_DOMAIN_IS_INTERVAL); // after change on AP-side this became dynamic information (AP-side change done by TL on 2005-06-30)
            knownCsticsInterest.append(
                GetFilteredCsticsAndValues.INTEREST_CSTIC_CONSTRAINEDS); // after change on AP-side this became dynamic information (AP-side change done by TL on 2005-06-30)                
            
            // knownCsticsInterest for values
            knownCsticsInterest.append(
                GetFilteredCsticsAndValues.INTEREST_VALUE_ASSIGNEDS);
            knownCsticsInterest.append(
                    GetFilteredCsticsAndValues.INTEREST_VALUE_IS_DOMAIN_VALUE);
            knownCsticsInterest.append(
                GetFilteredCsticsAndValues.INTEREST_VALUE_AUTHORS);
            knownCsticsInterest.append(
                GetFilteredCsticsAndValues.INTEREST_VALUE_LNAMES);
            knownCsticsInterest.append(
                GetFilteredCsticsAndValues.INTEREST_VALUE_DEFAULTS);

            ArrayList serverCommand = new ArrayList();
            serverCommand.add(SCECommand.CONFIG_ID);
            serverCommand.add(configuration.getId());
            serverCommand.add(GetFilteredCsticsAndValues.INST_ID);
            serverCommand.add(this.getId());
            if (!gCache.getGroup().isBaseGroup()) {
                serverCommand.add(GetFilteredCsticsAndValues.INCLUDE_UNGROUPED_CSTICS);
                serverCommand.add(GetFilteredCsticsAndValues.NO);
                serverCommand.add(GetFilteredCsticsAndValues.FILTER_GROUPS);
                serverCommand.add(gCache.getGroup().getName());
            } else {
                serverCommand.add(GetFilteredCsticsAndValues.UNGROUPED_CSTICS_ONLY);
                serverCommand.add(GetFilteredCsticsAndValues.YES);
            }
            // If you are only interested in visible cstics then only visible cstics
            // were requested. If you are interested in all cstics the next time then
            // a refresh of the invisible cstics is required.
            if (gCache.getInterest() == GroupCache.INTEREST_IN_VISIBLE_CSTICS_ONLY) {
                serverCommand.add(GetFilteredCsticsAndValues.VISIBLE_CSTICS_ONLY);
                serverCommand.add(GetFilteredCsticsAndValues.YES);
                gCache.setRequiredRefresh(GroupCache.REFRESH_REQUIRED_FOR_INVISIBLE_CSTICS);
            // If you are interested in all cstics then no refresh is required next time.
            } else {
                gCache.setRequiredRefresh(GroupCache.NO_REFRESH_REQUIRED);
            }
            gCache.setInterest(GroupCache.NO_INTEREST);
            serverCommand.add(GetFilteredCsticsAndValues.CSTIC_INFO_GROUPS);
            serverCommand.add("0");
            serverCommand.add(GetFilteredCsticsAndValues.CSTIC_INFO_GROUPS);
            serverCommand.add("1");
            serverCommand.add(GetFilteredCsticsAndValues.CSTIC_INFO_GROUP_INTERESTS);
            serverCommand.add(unknownCsticsInterest.toString());
            serverCommand.add(GetFilteredCsticsAndValues.CSTIC_INFO_GROUP_INTERESTS);
            serverCommand.add(knownCsticsInterest.toString());

            if (isAssignableValuesOnly()) {
                serverCommand.add(GetFilteredCsticsAndValues.ASSIGNABLE_VALUES_ONLY);
                serverCommand.add(GetFilteredCsticsAndValues.TRUE);
            } else {
                serverCommand.add(GetFilteredCsticsAndValues.ASSIGNABLE_VALUES_ONLY);
                serverCommand.add(GetFilteredCsticsAndValues.FALSE);
            }

            //transfer the group name
            serverCommand.add(GetFilteredCsticsAndValues.GROUP_NAME);
            serverCommand.add(gCache.getGroup().getName());

            // Now add all known characteristics
            Iterator knownCsticIterator =
                gCache.getGroup().getListOfCachedCharacteristics().iterator();
            while (knownCsticIterator.hasNext()) {
                Characteristic characteristic = (Characteristic) knownCsticIterator.next();
                serverCommand.add(GetFilteredCsticsAndValues.MONITORED_CSTIC_INFO_GROUPS);
                serverCommand.add("1");
                serverCommand.add(GetFilteredCsticsAndValues.MONITORED_CSTIC_NAMES);
                serverCommand.add(characteristic.getName());
                previouslyKnownCstics.put(characteristic.getName(), characteristic);
            }

            ServerResponse r =
            ((TCPDefaultClient)cachingClient).doCmd(
                    ISPCCommandSet.GET_FILTERED_CSTICS_AND_VALUES,
                    convertListToStringArray(serverCommand));

            String unknownCsticNames[] =
                r.getParameterValues(
                    GetFilteredCsticsAndValues.X_CSTIC_NAMES + "0");
            String unknownCsticLNames[] =
                r.getParameterValues(
                    GetFilteredCsticsAndValues.X_CSTIC_LNAMES + "0");
            String unknownCsticDomainIsAnIntervals[] =
                r.getParameterValues(
                    GetFilteredCsticsAndValues.X_CSTIC_DOMAIN_IS_INTERVALS
                        + "0");
            String unknownCsticVisibles[] =
                r.getParameterValues(
                    GetFilteredCsticsAndValues.X_CSTIC_VISIBLES + "0");
            String unknownCsticAddits[] =
                r.getParameterValues(
                    GetFilteredCsticsAndValues.X_CSTIC_ADDITS + "0");
            String unknownCsticAuthors[] =
                r.getParameterValues(
                    GetFilteredCsticsAndValues.X_CSTIC_AUTHORS + "0");
            String unknownCsticConsistents[] =
                r.getParameterValues(
                    GetFilteredCsticsAndValues.X_CSTIC_CONSISTENTS + "0");
            String unknownCsticConstraineds[] =
                r.getParameterValues(
                    GetFilteredCsticsAndValues.X_CSTIC_CONSTRAINEDS + "0");
            String unknownCsticGroups[] =
                r.getParameterValues(
                    GetFilteredCsticsAndValues.X_CSTIC_GROUPS + "0");
            String unknownCsticMultis[] =
                r.getParameterValues(
                    GetFilteredCsticsAndValues.X_CSTIC_MULTIS + "0");
            String unknownCsticReadOnlys[] =
                r.getParameterValues(
                    GetFilteredCsticsAndValues.X_CSTIC_READONLYS + "0");
            String unknownCsticRequireds[] =
                r.getParameterValues(
                    GetFilteredCsticsAndValues.X_CSTIC_REQUIREDS + "0");
            String unknownCsticUnits[] =
                r.getParameterValues(
                    GetFilteredCsticsAndValues.X_CSTIC_UNITS + "0");
            String unknownCsticEntryFieldMask[] =
                r.getParameterValues(
                    GetFilteredCsticsAndValues.X_CSTIC_ENTRY_FIELD_MASK + "0");
            String unknownCsticTypeLength[] =
                r.getParameterValues(
                    GetFilteredCsticsAndValues.X_CSTIC_TYPELENGTH + "0");
            String unknownCsticNumberScale[] =
                r.getParameterValues(
                    GetFilteredCsticsAndValues.X_CSTIC_NUMBERSCALE + "0");
            String unknownCsticValueType[] =
                r.getParameterValues(
                    GetFilteredCsticsAndValues.X_CSTIC_VALUETYPE + "0");
            String unknownCsticGroupPositions[] =
                r.getParameterValues(
                    GetFilteredCsticsAndValues.X_CSTIC_GROUP_POSITIONS + "0");
            if (unknownCsticGroupPositions == null) {
                // backward compatibility: Server dont know constant GetFilteredCsticsAndValues.X_CSTIC_GROUP_POSITIONS
                // therefor old constant GetFilteredCsticsAndValues.X_CSTIC_POSITIONS is used, but this is not kown on
                // client. So hardcoded "cp" is used which should be GetFilteredCsticsAndValues.X_CSTIC_POSITIONS
                unknownCsticGroupPositions = r.getParameterValues("cp" + "0");
            }
            String unknownCsticInstPositions[] =
                r.getParameterValues(
                    GetFilteredCsticsAndValues.X_CSTIC_INST_POSITIONS + "0");
            String unknownCsticApplicationViews[] =
                r.getParameterValues(
                    GetFilteredCsticsAndValues.X_CSTIC_VIEWS + "0");
           
           String unknownCsticExpandeds[] = 
                  r.getParameterValues(
                      GetFilteredCsticsAndValues.X_CSTIC_EXPANDEDS+"0");
           
           String unknownCsticMimeLNames[] = r.getParameterValues(GetFilteredCsticsAndValues.X_CSTIC_MIME_LNAME + "0");
           String unknownCsticMimeType[] = r.getParameterValues(GetFilteredCsticsAndValues.X_CSTIC_MIME_TYPE + "0");
           String unknownCsticMimeObjects[] = r.getParameterValues(GetFilteredCsticsAndValues.X_CSTIC_MIME_OBJECT + "0");

           String unknownValueCsticNames[] =
                r.getParameterValues(
                    GetFilteredCsticsAndValues.X_VALUE_CSTIC_NAMES + "0");
            String unknownValueNames[] =
                r.getParameterValues(
                    GetFilteredCsticsAndValues.X_VALUE_NAMES + "0");
            String unknownValueAssigneds[] =
                r.getParameterValues(
                    GetFilteredCsticsAndValues.X_VALUE_ASSIGNEDS + "0");
            String unknownValueAuthors[] =
                r.getParameterValues(
                    GetFilteredCsticsAndValues.X_VALUE_AUTHORS + "0");
            String unknownValueDefaults[] =
                r.getParameterValues(
                    GetFilteredCsticsAndValues.X_VALUE_DEFAULTS + "0");
            String unknownValueLNames[] =
                r.getParameterValues(
                    GetFilteredCsticsAndValues.X_VALUE_LNAMES + "0");
            String unknownValueConditionKeys[] = null;
            String unknownValueLPrices[] =
                r.getParameterValues(
                    GetFilteredCsticsAndValues.X_VALUE_LPRICES + "0");
            String unknownValuePrices[] =
                r.getParameterValues(
                    GetFilteredCsticsAndValues.X_VALUE_PRICES + "0");
            String unknownValueIsInDomains[] =
                r.getParameterValues(
                    GetFilteredCsticsAndValues.X_VALUE_IS_DOMAIN_VALUE + "0");                   

            String unknownMimeValueLNames[] =
                r.getParameterValues(
                    GetFilteredCsticsAndValues.X_MIME_VALUE_LNAME + "0");
            String unknownMimeType[] =
                r.getParameterValues(
                    GetFilteredCsticsAndValues.X_MIME_TYPE + "0");
            String unknownMimeObject[] =
                r.getParameterValues(
                    GetFilteredCsticsAndValues.X_MIME_OBJECT + "0");

            String knownCsticNames[] =
                r.getParameterValues(
                    GetFilteredCsticsAndValues.X_CSTIC_NAMES + "1");
            String knownCsticDomainIsAnIntervals[] =
                r.getParameterValues(
                    GetFilteredCsticsAndValues.X_CSTIC_DOMAIN_IS_INTERVALS
                        + "1");
            String knownCsticVisibles[] =
                r.getParameterValues(
                    GetFilteredCsticsAndValues.X_CSTIC_VISIBLES + "1");
            String knownCsticConstraineds[] =
                r.getParameterValues(
                    GetFilteredCsticsAndValues.X_CSTIC_CONSTRAINEDS + "1");                    
            String knownCsticAuthors[] =
                r.getParameterValues(
                    GetFilteredCsticsAndValues.X_CSTIC_AUTHORS + "1");
            String knownCsticConsistents[] =
                r.getParameterValues(
                    GetFilteredCsticsAndValues.X_CSTIC_CONSISTENTS + "1");
            String knownCsticRequireds[] =
                r.getParameterValues(
                    GetFilteredCsticsAndValues.X_CSTIC_REQUIREDS + "1");
            String knownCsticReadonlys[] =
                r.getParameterValues(
                    GetFilteredCsticsAndValues.X_CSTIC_READONLYS + "1");

            String knownValueCsticNames[] =
                r.getParameterValues(
                    GetFilteredCsticsAndValues.X_VALUE_CSTIC_NAMES + "1");
            String knownValueNames[] =
                r.getParameterValues(
                    GetFilteredCsticsAndValues.X_VALUE_NAMES + "1");
            String knownValueAssigneds[] =
                r.getParameterValues(
                    GetFilteredCsticsAndValues.X_VALUE_ASSIGNEDS + "1");
            String knownValueAuthors[] =
                r.getParameterValues(
                    GetFilteredCsticsAndValues.X_VALUE_AUTHORS + "1");
                    
            String knownValueIsInDomains[] =    
                r.getParameterValues(
                    GetFilteredCsticsAndValues.X_VALUE_IS_DOMAIN_VALUE + "1"); 
            String knownValueDefaults[] =
                r.getParameterValues(
                    GetFilteredCsticsAndValues.X_VALUE_DEFAULTS + "0");
                               

            // is only used in case of numerical cstics; otherwise they would have a wrong format in display
            String knownValueLongNames[] =
                r.getParameterValues(
                    GetFilteredCsticsAndValues.X_VALUE_LNAMES + "1");
            String knownValueLNames[] = null;
            if (knownValueLongNames != null) {
                knownValueLNames = new String[knownValueNames.length];
                if (knownValueLongNames.length < knownValueNames.length) {
                    System.arraycopy(
                        knownValueLongNames,
                        0,
                        knownValueLNames,
                        0,
                        knownValueLongNames.length);
                } else {
                    knownValueLNames = knownValueLongNames;
                }
            }

            // index[0]: index for values
            // index[1]: index for value mimes
            // index[2]: index for cstic mimes
            int index[] = new int[] { 0, 0, 0 };
            // First update all known characteristics
            index[0] = 0;
            for (i = 0;
                knownCsticNames != null && i < knownCsticNames.length;
                i++) {
                //              cstic = (DefaultCharacteristic) previousCstics.get(knownCsticNames[i]);
                cstic = (DefaultCharacteristic) cstics.get(knownCsticNames[i]);
                cstic.update( 
                    knownCsticVisibles[i].equals(
                        GetFilteredCsticsAndValues.YES),
                    knownCsticConsistents[i].equals(
                        GetFilteredCsticsAndValues.YES),
                    knownCsticRequireds[i].equals(
                        GetFilteredCsticsAndValues.YES),
                    knownCsticReadonlys[i].equals(
                        GetFilteredCsticsAndValues.YES),
                    knownCsticConstraineds[i].equals(
                        GetFilteredCsticsAndValues.YES),
                    knownCsticDomainIsAnIntervals[i].equals(
                        GetFilteredCsticsAndValues.YES));
                /*
                            cstics.put(knownCsticNames[i],cstic);
                            csticsList.add(cstic);
                            gCache.group.addCharacteristic(cstic);
                */
                cstic
                    .updateCharacteristicValues(
                        index,
                        knownValueCsticNames,
                        knownValueNames,
                        knownValueLNames,
                        null, //descriptions do not change at runtime
                        null, //conditionKeys do not change at runtime
                        null, //valuePrices do not change at runtime
                    knownValueIsInDomains,
                    knownValueAssigneds,
                    knownValueAuthors,
                    null,
                    knownValueDefaults,
                null, 
                null, 
                null); 
                // Since the cstic was found in the server's response again,
                // remove it from the list of previously known cstics so that this
                // list contains only those previously known cstics that are missing
                // which means they became invisible and the server should return
                // visible cstics only
                previouslyKnownCstics.remove(cstic.getName());
                csticsInGroup.put(cstic.getName(), cstic);
            }
            // Second add the new Characteristics
            index[0] = 0;
            for (i = 0;
                unknownCsticNames != null && i < unknownCsticNames.length;
                i++) {
                int unknownCsticValueTypeInt;
                try {
                    unknownCsticValueTypeInt = Integer.parseInt(unknownCsticValueType[i]);
                } catch (NumberFormatException ne) {
                    throw new IPCException(IPCException.IPCCLIENTEX1, null, ne);
                }
                int unknownCsticTypeLengthInt;
                String unknownCsticTypeLengthStr = unknownCsticTypeLength[i];
                if (unknownCsticTypeLengthStr != null) {
                    try {
                        unknownCsticTypeLengthInt = Integer.parseInt(unknownCsticTypeLengthStr);
                    } catch (NumberFormatException ne) {
                        throw new IPCException(
                            IPCException.IPCCLIENTEX2,
                            null,
                            ne);
                    }
                } else {
                    unknownCsticTypeLengthInt = 0;
                }
                int unknownCsticNumberScaleInt;
                String unknownCsticNumberScaleStr = unknownCsticNumberScale[i];
                if (unknownCsticNumberScaleStr != null) {
                    try {
                        unknownCsticNumberScaleInt = Integer.parseInt(unknownCsticNumberScaleStr);
                    } catch (NumberFormatException ne) {
                        throw new IPCException(
                            IPCException.IPCCLIENTEX3,
                            null,
                            ne);
                    }
                } else {
                    unknownCsticNumberScaleInt = 0;
                }
                
                // for backward compatibility (in the case that the server doesn't return 
                // application views for cstics)
                String unknownCsticApplicationView = "";
                if (unknownCsticApplicationViews != null) {
                    unknownCsticApplicationView = unknownCsticApplicationViews[i];
                }
                
                //for backward compatibility in case the server does not return boolean expanded value
                //If no value is returned the value is defaulted to false
                boolean csticExpanded=false;
                if (unknownCsticExpandeds!=null && unknownCsticExpandeds.length > 0 && unknownCsticExpandeds[i] != null) {
                    csticExpanded=unknownCsticExpandeds[i].equals(GetFilteredCsticsAndValues.TRUE);
                }else {
                    location.warningT("Incompatible Server version. Parameter " + GetFilteredCsticsAndValues.TRUE + " not returned from server. Functionality of expand/collapse of single characteristics is not available!");
                }

                cstic =
                    factory
                        .newCharacteristic(
                            configurationChangeStream,
                            ipcClient,
                            this,
                            unknownCsticGroups[i],
                            unknownCsticLNames[i],
                            unknownCsticNames[i],
                            unknownCsticUnits[i],
                            unknownCsticEntryFieldMask[i],
                            unknownCsticTypeLengthInt,
                            unknownCsticNumberScaleInt,
                            unknownCsticValueTypeInt,
                            unknownCsticAddits[i].equals(
                                GetFilteredCsticsAndValues.YES),
                            unknownCsticVisibles[i].equals(
                                GetFilteredCsticsAndValues.YES),
                            unknownCsticMultis[i].equals(
                                GetFilteredCsticsAndValues.YES),
                            unknownCsticRequireds[i].equals(
                                GetFilteredCsticsAndValues.YES),
                                csticExpanded,    
                            unknownCsticConsistents[i].equals(
                                GetFilteredCsticsAndValues.YES),
                            unknownCsticConstraineds[i].equals(
                                GetFilteredCsticsAndValues.YES),
                            unknownCsticDomainIsAnIntervals[i].equals(
                                GetFilteredCsticsAndValues.YES),
                            unknownCsticReadOnlys[i].equals(
                                GetFilteredCsticsAndValues.YES),
                            false, 
                            null,  
                            unknownCsticApplicationView,
                            unknownCsticMimeLNames,
                            unknownCsticMimeType,
                            unknownCsticMimeObjects,
                            index);

                cstics.put(unknownCsticNames[i], cstic);
                csticsInGroup.put(cstic.getName(), cstic);
                //check the cstics order
                int newGroupPosition = new Integer(unknownCsticGroupPositions[i]).intValue();
                int newInstPosition = new Integer(unknownCsticInstPositions[i]).intValue() - 1;
				addCharacteristic(newInstPosition, cstic);
                gCache.getGroup().addCharacteristic(newGroupPosition, cstic);

                cstic
                    .updateCharacteristicValues(
                        index,
                        unknownValueCsticNames,
                        unknownValueNames,
                        unknownValueLNames,
                        null, //descriptions are read with a separate command, triggered from GetCharacteristicAction
                    unknownValueConditionKeys,
                    unknownValueLPrices,
                    unknownValueIsInDomains,
                    unknownValueAssigneds,
                    unknownValueAuthors,
                    null,
                    unknownValueDefaults,
                unknownMimeValueLNames, unknownMimeType, unknownMimeObject);
            }

            // Now set all remaining cstics of the previously known cstics list
            // to the state invisible. That should keep a copy and renew the
            // information only if invisible cstics should be displayed as well.
            Iterator previouslyKnownCsticsIt = previouslyKnownCstics.values().iterator();
            while (previouslyKnownCsticsIt.hasNext()) {
                Characteristic characteristic = (Characteristic) previouslyKnownCsticsIt.next();
                characteristic.setVisible(false);
            }
            boolean groupConsistent =
                r.getParameterValue(
                    GetFilteredCsticsAndValues.GROUP_CONSISTENT).equals(
                    GetFilteredCsticsAndValues.TRUE)
                    ? true
                    : false;
            boolean groupReq =
                r.getParameterValue(
                    GetFilteredCsticsAndValues.GROUP_REQUIRED).equals(
                    GetFilteredCsticsAndValues.TRUE)
                    ? true
                    : false;
            // in case of the basegroup no status is returned we have to calculate it by ourselves
            if (gCache.getGroup().isBaseGroup()){
                Iterator keys = csticsInGroup.keySet().iterator();
                groupConsistent = true;
                groupReq = false;
                while (keys.hasNext()){
                    Characteristic currentCstic = (Characteristic)csticsInGroup.get(keys.next());
                    // as soon as one cstic is not consistent the whole group is not consistent
                    if (!currentCstic.isConsistent()) {
                        groupConsistent = false;
                    }
                    // as soon as one required cstic has no assigned values the whole group is required
                    if (currentCstic.isRequired() && !currentCstic.hasAssignedValues()){
                        groupReq = true;
                    }
                    // break the loop as soon as both flags have been changed
                    if ((groupConsistent == false) && (groupReq == true)){
                        break;
                    }
                }
            }
            gCache.getGroup().setConsistent(groupConsistent);
            gCache.getGroup().setRequired(groupReq);
            
        }
        // Reset interest, because it has been satisfied!
        gCache.setInterest(GroupCache.NO_INTEREST);
        location.exiting();
    }
    
	/* (non-Javadoc)
     * @see com.sap.spc.remote.client.object.Instance#specialize(java.lang.String)
     */
    public void specialize(String specTypeId) {
        location.entering("specialize(String specTypeId)");
        if (location.beDebug()){
            location.debugT("Parameters:");
            location.debugT("String specTypeId " + specTypeId);
        }
        try {
            ServerResponse r =
            ((TCPDefaultClient)cachingClient).doCmd(
                    ISPCCommandSet.SPECIALIZE,
                    new String[] {
                        SCECommand.CONFIG_ID,
                        configuration.getId(),
                        Specialize.INST_ID,
                        this.getId(),
                        Specialize.SPEC_TYPE_ID,
                        specTypeId });
        } catch (ClientException e) {
            category.logT(Severity.ERROR, location, e.toString());
            throw new IPCException(e);
        }

        // update the config's change time
        configuration.setChangeTime();
        location.exiting();
    }

    /* (non-Javadoc)
     * @see com.sap.spc.remote.client.object.Instance#unspecialize()
     */
    public void unspecialize() {
        try {
            location.entering("unspecialize()");
            ServerResponse r =
            ((TCPDefaultClient)cachingClient).doCmd(
                    ISPCCommandSet.UNSPECIALIZE,
                    new String[] {
                        SCECommand.CONFIG_ID,
                        configuration.getId(),
                        Unspecialize.INST_ID,
                        this.getId()});
        } catch (ClientException e) {
            category.logT(Severity.ERROR, location, e.toString());
            throw new IPCException(e);
        }

        // update the config's change time
        configuration.setChangeTime();
        location.exiting();
    }

    /* (non-Javadoc)
     * @see com.sap.spc.remote.client.object.Instance#createChildInstance(java.lang.String)
     */
    public void createChildInstance(String decompPosition) {
        try {
            location.entering("createChildInstance(String decompPosition)");
            if (location.beDebug()){
                location.debugT("Parameters:");
                location.debugT("String decompPosition " + decompPosition);
            }    
            ServerResponse r =
            ((TCPDefaultClient)cachingClient).doCmd(
                    ISPCCommandSet.CREATE_INSTANCE,
                    new String[] {
                        SCECommand.CONFIG_ID,
                        configuration.getId(),
                        CreateInstance.PARENT_INST_ID,
                        this.getId(),
                        CreateInstance.DECOMP_POS_NR,
                        decompPosition });
        } catch (ClientException e) {
            category.logT(Severity.ERROR, location, e.toString());
            throw new IPCException(e);
        }

        // update the config's change time
        configuration.setChangeTime();
    }

    /* (non-Javadoc)
     * @see com.sap.spc.remote.client.object.Instance#getProductVariants(java.lang.String, java.lang.String, java.lang.String)
     */
    public List getProductVariants(
        String limit,
        String quality,
        String maxMatchPoints) {
        location.entering("List getProductVariants(String limit, String quality, String maxMatchPoints)");
        if (location.beDebug()) {
            if (location.beDebug()){
                location.debugT("Parameters:");
                location.debugT("String limit " + limit);
                location.debugT("String quality " + quality);
                location.debugT("String maxMatchPoints " + maxMatchPoints);
                }
        }
        
        Vector productVariants = new Vector();
        if (!this.isRootInstance()) {
            if (location.beDebug()){
                location.debugT("return productVariants (empty list)");
            }
            location.exiting();
            return productVariants;
        }
        try {
            ServerResponse r =
            ((TCPDefaultClient)cachingClient).doCmd(
                    ISPCCommandSet.GET_PRODUCT_VARIANTS,
                    new String[] {
                        SCECommand.CONFIG_ID,
                        configuration.getId(),
                        GetProductVariants.LIMIT,
                        limit,
                        GetProductVariants.QUALITY,
                        quality,
                        GetProductVariants.MATCH_POINTS,
                        maxMatchPoints });

            String variantCount =
                r.getParameterValue(GetProductVariants.VARIANT_COUNT);
            //return an empty list if nothing found
            if (variantCount == null) {
                if (location.beDebug()){
                    location.debugT("return productVariants (empty list)");
                }
                location.exiting();
                return productVariants;
            }
            if (variantCount.equalsIgnoreCase("0")) {
                if (location.beDebug()){
                    location.debugT("return productVariants (empty list)");
                }
                location.exiting();
                return productVariants;
            }

            String[] variantProductIds =
                r.getParameterValues(GetProductVariants.VARIANT_PRODUCT_IDS);
            String[] distances =
                r.getParameterValues(GetProductVariants.DISTANCE);
            String[] matchPoints =
                r.getParameterValues(GetProductVariants.MATCH_POINTS);
            String[] featureProductIds =
                r.getParameterValues(GetProductVariants.FEATURE_PRODUCT_IDS);
            String[] featureTypes =
                r.getParameterValues(GetProductVariants.FEATURE_TYPES);
            String[] featureIds =
                r.getParameterValues(GetProductVariants.FEATURE_IDS);
            String[] featureNames =
                r.getParameterValues(GetProductVariants.FEATURE_NAMES);
            String[] featureLNames =
                r.getParameterValues(GetProductVariants.FEATURE_LNAMES);
            String[] featureLongtexts =
                r.getParameterValues(GetProductVariants.FEATURE_LONGTEXTS);
            String[] featureValues =
                r.getParameterValues(GetProductVariants.FEATURE_VALUES);
            String[] featureLValues =
                r.getParameterValues(GetProductVariants.FEATURE_LVALUES);
            String[] featureWeights =
                r.getParameterValues(GetProductVariants.FEATURE_WEIGHTS);
            String[] featureDistances =
                r.getParameterValues(GetProductVariants.FEATURE_DISTANCES);

            for (int i = 0; i < variantProductIds.length; i++) {
                String matchPoint = ""; // to avoid NullPointerException if matchPoints is null
                if (matchPoints != null){
                    matchPoint = matchPoints[i];
                }
                ProductVariant productVariant =
                    factory.newProductVariant(
                        ipcClient,
                        variantProductIds[i],
                        new Double(distances[i]).doubleValue(),
                        matchPoint,
                        maxMatchPoints,
                        new Vector(),
                        null);
                if (featureProductIds == null){
                    featureProductIds = new String[0];
                }
                for (int j = 0; j < featureProductIds.length; j++) {
                    if (!featureProductIds[j].equals(variantProductIds[i])) {
                        continue;
                    }
                    ProductVariantFeature feature =
                        factory.newProductVariantFeature(
                            ipcClient,
                            productVariant,
                            featureTypes[j],
                            featureIds[j],
                            featureNames[j],
                            featureLNames[j],
                            featureLongtexts[j],
                            featureValues[j],
                            featureLValues[j],
                            new Double(featureWeights[j]).doubleValue(),
                            new Double(featureDistances[j]).doubleValue());
                    productVariant.addFeature(feature);
                    if (productVariant.getFeatures().size() == 1) {
                        productVariant.setMainDifference(feature);
                    }
                }
                productVariants.add(productVariant);
            }
        } catch (ClientException e) {
            category.logT(Severity.ERROR, location, e.toString());
            throw new IPCException(e);
        }
        if (location.beDebug()){
            if (productVariants!=null){
                location.debugT("return productVariants " + productVariants.toString());
            }
            else location.debugT("return productVariants null");
        }
        location.exiting();
        return productVariants;
    }

    /* (non-Javadoc)
     * @see com.sap.spc.remote.client.object.Instance#applyProductVariant(java.lang.String)
     */
    public boolean applyProductVariant(String productVariantId) {
        location.entering("applyProductVariant(String productVariantId)");
        if (location.beDebug()){
            location.debugT("Parameters:");
            location.debugT("String productVariantId " + productVariantId);
        }
        boolean success = false;
        if (!this.isRootInstance()) {
            return false;
        }
        try {
            IPCItem item = getIpcItem();
            if (item != null) {
                success = item.changeProduct(productVariantId);
                ((TcpDefaultIPCItem)item).setCacheDirty();  
            } else {
            ArrayList params = new ArrayList();
            params.add(ChangeItemProduct.PRODUCT_ID);
            params.add(productVariantId);
                params.add(SCECommand.CONFIG_ID);
                params.add(configuration.getId());
            String[] paraString = new String[params.size()];
            params.toArray(paraString);
            ServerResponse r =
            ((TCPDefaultClient)cachingClient).doCmd(
                    ISPCCommandSet.CHANGE_ITEM_PRODUCT,
                    paraString);

            String result = r.getParameterValue(ChangeItemProduct.SUCCESS);
                // HACK: right now, ChangeItemProduct doesn't return anything for its success, assume the best has happened :-)
            if (result == null || result.equals("Y")) {
                success = true;
            }
            }
			cacheIsDirty = true;
        } catch (Exception e) { 
            category.logT(Severity.ERROR, location, e.toString());
            throw new IPCException(e);
        }
        if (location.beDebug()){
            location.debugT("return success " + success);
        }
        location.exiting();
        return success;
    }

    /* (non-Javadoc)
     * @see com.sap.spc.remote.client.object.imp.DefaultInstance#updateGroupCaches(java.util.HashMap)
     */
    protected void updateGroupCaches(HashMap gCaches)
        throws ClientException {
		// This is currently only a dummy implementation using the old updateGroupCache() method. 
		// For performance improvements in MSA this needs to be implemented like in the rfc-class.
		Iterator cacheIt = gCaches.values().iterator();
		while (cacheIt.hasNext()) {
			GroupCache gCache = (GroupCache) cacheIt.next();

			if (gCache.getInterest() != GroupCache.NO_INTEREST) {
				updateGroupCache(gCache);
			}
		}
        

    }

}