package com.sap.spc.remote.client.object.imp.rfc;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.TreeMap;
import java.util.Vector;

import com.sap.mw.jco.IMetaData;
import com.sap.mw.jco.JCO;
import com.sap.spc.remote.client.ClientException;
import com.sap.spc.remote.client.ResourceAccessor;
import com.sap.spc.remote.client.object.ComparisonResult;
import com.sap.spc.remote.client.object.Configuration;
import com.sap.spc.remote.client.object.ConflictInfo;
import com.sap.spc.remote.client.object.Delta;
import com.sap.spc.remote.client.object.IPCClient;
import com.sap.spc.remote.client.object.IPCException;
import com.sap.spc.remote.client.object.IPCSession;
import com.sap.spc.remote.client.object.Instance;
import com.sap.spc.remote.client.object.InstanceDelta;
import com.sap.spc.remote.client.object.MimeObject;
import com.sap.spc.remote.client.object.MimeObjectContainer;
import com.sap.spc.remote.client.object.ValueDelta;
import com.sap.spc.remote.client.object.imp.DefaultConfiguration;
import com.sap.spc.remote.client.object.imp.DefaultConflictInfo;
import com.sap.spc.remote.client.object.imp.DefaultIPCItem;
import com.sap.spc.remote.client.object.imp.DefaultInstance;
import com.sap.spc.remote.client.object.imp.DefaultMimeObjectContainer;
import com.sap.spc.remote.client.object.imp.IllegalClassException;
import com.sap.spc.remote.client.rfc.ExternalConfigConverter;
import com.sap.spc.remote.client.rfc.function.CfgApiCompareConfigurations;
import com.sap.spc.remote.client.rfc.function.CfgApiDeleteInstance;
import com.sap.spc.remote.client.rfc.function.CfgApiGetConfigInfo;
import com.sap.spc.remote.client.rfc.function.CfgApiGetConflicts;
import com.sap.spc.remote.client.rfc.function.CfgApiGetInstances;
import com.sap.spc.remote.client.rfc.function.CfgApiResetConfig;
import com.sap.spc.remote.client.rfc.function.IFunctionGroup;
import com.sap.spc.remote.client.rfc.function.SpcGetConfigItemInfo;
import com.sap.spc.remote.client.util.ext_configuration;
import com.sap.tc.logging.Category;
import com.sap.tc.logging.Location;
import com.sap.tc.logging.Severity;

public class RfcDefaultConfiguration extends DefaultConfiguration implements Configuration {

	// logging
	private static final Category category =
				ResourceAccessor.category;
	private static final Location location =
		ResourceAccessor.getLocation(RfcDefaultConfiguration.class);

    protected RfcDefaultConfiguration(IPCClient ipcClient, String id) {
        this.ipcClient = ipcClient;
        this.id = id;
        
        IPCSession session = ipcClient.getIPCSession();
        if (!(session instanceof RfcDefaultIPCSession)) {
            throw new IllegalClassException(session, RfcDefaultIPCSession.class);
        }
        this.cachingClient = ((RfcDefaultIPCSession) session).getClient();

        closed = false;
        changetime = -1;
        configurationChangeStream = factory.newConfigurationChangeStream(this, cachingClient);
        initMembers();
    }

    protected RfcDefaultConfiguration(IPCSession session, String id) {
        this.id = id;
        this.ipcClient = session.getIPCClient();
        this.cachingClient = ((RfcDefaultIPCSession) session).getClient();

        closed = false;
        changetime = -1;
        configurationChangeStream = factory.newConfigurationChangeStream(this, cachingClient);
        initMembers();
    }


    /* (non-Javadoc)
     * @see com.sap.spc.remote.client.object.imp.DefaultConfiguration#initMembers()
     */
    protected void initMembers() {
        try {        
            String configId = this.id;
            String getContext = TRUE;

            JCO.Function functionCfgApiGetConfigInfo = ((RFCDefaultClient)cachingClient).getFunction(IFunctionGroup.CFG_API_GET_CONFIG_INFO);
            JCO.ParameterList im = functionCfgApiGetConfigInfo.getImportParameterList();
            JCO.ParameterList ex = functionCfgApiGetConfigInfo.getExportParameterList();
            JCO.ParameterList tbl = functionCfgApiGetConfigInfo.getTableParameterList();
            
            im.setValue(configId, CfgApiGetConfigInfo.CONFIG_ID);     // STRING, Configuration Identifier
            im.setValue(getContext, CfgApiGetConfigInfo.GET_CONTEXT);     // optional, CHAR, T=True, F=False, ''=False
            
            category.logT(Severity.PATH, location, "executing RFC CFG_API_GET_CONFIG_INFO");
            ((RFCDefaultClient)cachingClient).execute(functionCfgApiGetConfigInfo);
            category.logT(Severity.PATH, location, "done with RFC CFG_API_GET_CONFIG_INFO");
            
            // process structure exConfigInfo
            JCO.Structure exConfigInfo = ex.getStructure(CfgApiGetConfigInfo.CONFIG_INFO);
            //rootId = exConfigInfo.getString(CfgApiGetConfigInfo.ROOT_ID);
            String exConfigInfoSce = exConfigInfo.getString(CfgApiGetConfigInfo.SCE);            
            String exConfigInfoName = exConfigInfo.getString(CfgApiGetConfigInfo.NAME);
            String exConfigInfoVersion = exConfigInfo.getString(CfgApiGetConfigInfo.VERSION);
            String exConfigInfoLogsys = exConfigInfo.getString(CfgApiGetConfigInfo.LOGSYS);
            String exConfigInfoProfile = exConfigInfo.getString(CfgApiGetConfigInfo.PROFILE);
            String exConfigInfoComplete = exConfigInfo.getString(CfgApiGetConfigInfo.COMPLETE);
            String exConfigInfoConsistent = exConfigInfo.getString(CfgApiGetConfigInfo.CONSISTENT);
            String exConfigInfoBuild = ((Integer)exConfigInfo.getValue(CfgApiGetConfigInfo.BUILD)).toString();
            String exConfigInfoSingleLevel = exConfigInfo.getString(CfgApiGetConfigInfo.SINGLE_LEVEL);
            
            
            if (exConfigInfoSce.equals(EMPTY)) {
                sceMode = Boolean.FALSE;
            }
            else {
                sceMode = Boolean.TRUE;
            }

            if (exConfigInfoComplete.equals(TRUE)) {
                complete = Boolean.TRUE;
            }
            else {
                complete = Boolean.FALSE;
            }

            if (exConfigInfoConsistent.equals(TRUE)) {
                consistent = Boolean.TRUE;
            }
            else {
                consistent = Boolean.FALSE;
            }
                
            if (exConfigInfoSingleLevel.equals(TRUE)) {
                singleLevel = Boolean.TRUE;
            }
            else {
                singleLevel = Boolean.FALSE;
            }

            this.knowledgeBase =
                factory.newKnowledgeBase(
                    this.ipcClient,
                    exConfigInfoLogsys,
                    exConfigInfoName,
                    exConfigInfoVersion,
                    exConfigInfoBuild);

            this.activeKBProfile = knowledgeBase.createProfile(exConfigInfoProfile);
            
            // process table exContext
            // TABLE, CFG: table of context data
            JCO.Table exContext = ex.getTable(CfgApiGetConfigInfo.CONTEXT);
            int numRows = exContext.getNumRows();
            contextNames = new String[numRows];
            contextValues = new String[numRows];
            int exContextCounter;
            for (exContextCounter=0; exContextCounter<numRows; exContextCounter++) {
                exContext.setRow(exContextCounter);
                contextNames[exContextCounter] = exContext.getString(CfgApiGetConfigInfo.NAME);
                contextValues[exContextCounter] = exContext.getString(CfgApiGetConfigInfo.VALUE);
            }

            isAvailable = true;
        }
        catch(ClientException e) {
            isAvailable = false;
            category.logT(Severity.ERROR, location, e.toString());
            throw new IPCException(e);
        }
        

    }

    protected void synchronizeInstances() {
		if (instanceCacheIsDirty || !instanceDataCached) {
            try {
                String configId = this.id;
                String getMimeObjects = TRUE;
    
                JCO.Function functionCfgApiGetInstances = ((RFCDefaultClient)cachingClient).getFunction(IFunctionGroup.CFG_API_GET_INSTANCES);
                JCO.ParameterList im = functionCfgApiGetInstances.getImportParameterList();
                JCO.ParameterList ex = functionCfgApiGetInstances.getExportParameterList();
                JCO.ParameterList tbl = functionCfgApiGetInstances.getTableParameterList();
    
                im.setValue(configId, CfgApiGetInstances.CONFIG_ID);     // STRING, Configuration Identifier
                im.setValue(getMimeObjects, CfgApiGetInstances.GET_MIME_OBJECTS);        // optional, CHAR, Get Mime Objects (F=False, T=True, ""=False)
                
                category.logT(Severity.PATH, location, "executing RFC CFG_API_GET_INSTANCES");
                ((RFCDefaultClient)cachingClient).execute(functionCfgApiGetInstances);
                category.logT(Severity.PATH, location, "done with RFC CFG_API_GET_INSTANCES");
                
                // process table exInstances
                // TABLE, Table with instances
                JCO.Table exInstances = ex.getTable(CfgApiGetInstances.INSTANCES);
                int numRows = exInstances.getNumRows();
                String[] instName = new String[numRows];
                String[] instLName = new String[numRows];
                String[] instId = new String[numRows];
                String[] instPId = new String[numRows];
                String[] instUnspecable = new String[numRows];
                String[] instPrice = new String[numRows];
                String[] instAuthor = new String[numRows];
                String[] instConflict = new String[numRows];
                String[] instComplete = new String[numRows];
                String[] instImage = new String[numRows];
                String[] instDescription = new String[numRows];
                String[] instQuantity = new String[numRows];
                String[] instPosition = new String[numRows];
                
                // determine length of arrays for mimes
                int instanceMimesNumRows = 0;
                int exInstancesCounter;
                for (exInstancesCounter=0; exInstancesCounter<numRows; exInstancesCounter++) {
                    exInstances.setRow(exInstancesCounter);
                    JCO.Table exInstancesInstMimes = exInstances.getTable(CfgApiGetInstances.INST_MIMES);
                    instanceMimesNumRows = instanceMimesNumRows + exInstancesInstMimes.getNumRows();
                }
                
                // initialize the arrays with the correct size
                String[] mimeInstIds = new String[instanceMimesNumRows];           
                String[] mimeObjectTypes = new String[instanceMimesNumRows];
                String[] mimeObjects = new String[instanceMimesNumRows];
                
                // initialize counter for filling the mime arrays (different one than for the single table loops)
                int instanceMimesCounter = 0;
                
                for (exInstancesCounter=0; exInstancesCounter<numRows; exInstancesCounter++) {
                    exInstances.setRow(exInstancesCounter);
                    instName[exInstancesCounter] = exInstances.getString(CfgApiGetInstances.INST_NAME);
                    instLName[exInstancesCounter] = exInstances.getString(CfgApiGetInstances.INST_LNAME);
                    instId[exInstancesCounter] = exInstances.getString(CfgApiGetInstances.INST_ID);
                    instPId[exInstancesCounter] = exInstances.getString(CfgApiGetInstances.INST_PID);
                    instPrice[exInstancesCounter] = exInstances.getString(CfgApiGetInstances.INST_PRICE);
                    instUnspecable[exInstancesCounter] = exInstances.getString(CfgApiGetInstances.INST_UNSPECABLE);
                    instAuthor[exInstancesCounter] = exInstances.getString(CfgApiGetInstances.INST_AUTHOR);
                    instConflict[exInstancesCounter] = exInstances.getString(CfgApiGetInstances.INST_CONSISTENT);
                    instComplete[exInstancesCounter] = exInstances.getString(CfgApiGetInstances.INST_COMPLETE);
                    instQuantity[exInstancesCounter] = exInstances.getString(CfgApiGetInstances.INST_QUANTITY);
                    instPosition[exInstancesCounter] = exInstances.getString(CfgApiGetInstances.INST_POSITION);
                    
                    // process table exInstancesInstDescription
                    // TABLE
                    JCO.Table exInstancesInstDescription = exInstances.getTable(CfgApiGetInstances.INST_DESCRIPTION);
					instDescription[exInstancesCounter] = RFCKnowledgebaseCacheLoader.getLongDescription(exInstancesInstDescription);
                    
                    // process table exInstancesInstMimes
                    // TABLE
                    JCO.Table exInstancesInstMimes = exInstances.getTable(CfgApiGetInstances.INST_MIMES);
                    int exInstancesInstMimesCounter;
                    for (exInstancesInstMimesCounter=0; exInstancesInstMimesCounter<exInstancesInstMimes.getNumRows(); exInstancesInstMimesCounter++) {
                        exInstancesInstMimes.setRow(exInstancesInstMimesCounter);
                        mimeObjectTypes[instanceMimesCounter] =  exInstancesInstMimes.getString(CfgApiGetInstances.MIME_OBJECT_TYPE);
                        mimeObjects[instanceMimesCounter] = exInstancesInstMimes.getString(CfgApiGetInstances.MIME_OBJECT);
                        mimeInstIds[instanceMimesCounter] = instId[exInstancesCounter];
                        instanceMimesCounter++;
                    }
                }
    
                TreeMap previousInstances = m_Instances;
                m_Instances = new TreeMap();
                m_RootInstance = null; // No root instance found yet

                // Now generate the mime objects and place them into a hashtable
                String prevInstance = "";
                DefaultMimeObjectContainer mimes = null;
                HashMap instToMimeIndexHash = new HashMap();
                if (mimeInstIds != null) {
                    for (int i = 0; i < mimeInstIds.length; i++) {
                        if (!prevInstance.equals(mimeInstIds[i])) {
                            // Next Instance's mime objects?
                            mimes = factory.newMimeObjectContainer();
                            instToMimeIndexHash.put(mimeInstIds[i], mimes);
                            prevInstance = mimeInstIds[i];
                        }
                        MimeObject mimeObject =
                            factory.newMimeObject(
                                mimeObjects[i],
                                mimeObjectTypes[i]);
                        mimes.add(mimeObject);
                    }
                }
                instanceList = new Vector(instId.length);

                // Iterate over all instances and generate a business object for each of them
                for (int i = 0; i < instName.length; i++) {
                    DefaultInstance instance = null;
                    if (previousInstances != null) {
                        instance = (DefaultInstance) previousInstances.get(instId[i]);
                    }
                    if (instance == null) {
                        instance =
                            factory.newInstance(
                                configurationChangeStream,
                                ipcClient,
                                this,
                                instId[i],
                                instPId[i],
                                instComplete[i].equals(TRUE),
                                instConflict[i].equals(FALSE),
                                instQuantity[i],
                                instName[i],
                                instLName[i],
                                instDescription[i],
                                instPosition[i],
                                instPrice[i],
                                instAuthor[i].equals(USER),
                                true,
                                instUnspecable[i].equals(TRUE),
                                instUnspecable[i].equals(TRUE),
                                (MimeObjectContainer) instToMimeIndexHash.get(
                                    instId[i]));
                    } else {
                        instance.init(
                            configurationChangeStream,
                            ipcClient,
                            this,
                            instId[i],
                            instPId[i],
                            instComplete[i].equals(TRUE),
                            instConflict[i].equals(FALSE),
                            instQuantity[i],
                            instName[i],
                            instLName[i],
                            instDescription[i],
                            instPosition[i],
                            instPrice[i],
                            instAuthor[i].equals(USER),
                            true,
                            instUnspecable[i].equals(TRUE),
                            instUnspecable[i].equals(TRUE),
                            (MimeObjectContainer) instToMimeIndexHash.get(
                                instId[i]));
                    }
                    if (instPId[i].equals("0")) {
                        instanceList.add(instance);
                        m_Instances.put(instId[i], instance);
                        m_RootInstance = instance;
                    } else if (!instPId[i].equals("-1")) {
                        instanceList.add(instance);
                        m_Instances.put(instId[i], instance);
                        ((DefaultInstance) m_Instances.get(instPId[i])).addChild(instance);
                    } else {
                        //non part instances, currently ignored
                    }
                }
            } catch (ClientException e) {
                category.logT(Severity.ERROR, location, e.toString());
                throw new IPCException(e);
            }
			instanceCacheIsDirty = false;
			instanceDataCached = true;
		}        
    }
    
    
    /* (non-Javadoc)
     * @see com.sap.spc.remote.client.object.imp.DefaultConfiguration#synchronize()
     */
    protected synchronized void synchronize() {
        if (cacheIsDirty || !dataCached) {
            initMembers();
            cacheIsDirty = false;
            dataCached = true;
            location.exiting();
        }        
    }

    /* (non-Javadoc)
     * @see com.sap.spc.remote.client.object.Configuration#toExternal()
     */
    public ext_configuration toExternal() {
        try {        
            String configId = getId();
            JCO.Function functionSpcGetConfigItemInfo = ((RFCDefaultClient)cachingClient).getFunction(IFunctionGroup.SPC_GET_CONFIG_ITEM_INFO);
            JCO.ParameterList im = functionSpcGetConfigItemInfo.getImportParameterList();
            JCO.ParameterList ex = functionSpcGetConfigItemInfo.getExportParameterList();
            JCO.ParameterList tbl = functionSpcGetConfigItemInfo.getTableParameterList();

            im.setValue(configId, SpcGetConfigItemInfo.CONFIG_ID);     // optional, STRING, Configuration Identifier
            // im.setValue(documentId, SpcGetConfigItemInfo.DOCUMENT_ID");     // optional, BYTE, GUID-Schlüssel des Preisfindungsbeleges (KNUMV)
            // im.setValue(itemId, SpcGetConfigItemInfo.ITEM_ID");     // optional, BYTE, Positionsnummer
            
            category.logT(Severity.PATH, location, "executing RFC SPC_GET_CONFIG_ITEM_INFO");
            ((RFCDefaultClient)cachingClient).execute(functionSpcGetConfigItemInfo);
            category.logT(Severity.PATH, location, "done with RFC SPC_GET_CONFIG_ITEM_INFO");

            // process structure exExtConfigGuid
            JCO.Structure exExtConfigGuid = ex.getStructure(SpcGetConfigItemInfo.EXT_CONFIG_GUID);
            
            // process structure exExtConfigGuidExtConfig
            JCO.Structure exExtConfigGuidExtConfig = exExtConfigGuid.getStructure(SpcGetConfigItemInfo.EXT_CONFIG);
            
            ext_configuration ext_config = ExternalConfigConverter.createConfig(
                                                exExtConfigGuidExtConfig,
                                                null);
            if (ext_config != null) {
                return ext_config;
            } else {
                throw new java.lang.NullPointerException(
                    "Error while creating external configuration");
            }
        } catch(ClientException e) {
            category.logT(Severity.ERROR, location, "Error while creating external configuration" + e.toString());
            throw new IPCException(e);
        }
    }

    /* (non-Javadoc)
     * @see com.sap.spc.remote.client.object.Configuration#getExternalConfiguration()
     */
    public HashMap getExternalConfiguration() {
        HashMap data = new HashMap();
        try {        
            String configId = this.id;
            JCO.Function functionSpcGetConfigItemInfo = ((RFCDefaultClient)cachingClient).getFunction(IFunctionGroup.SPC_GET_CONFIG_ITEM_INFO);
            JCO.ParameterList im = functionSpcGetConfigItemInfo.getImportParameterList();
            JCO.ParameterList ex = functionSpcGetConfigItemInfo.getExportParameterList();
            JCO.ParameterList tbl = functionSpcGetConfigItemInfo.getTableParameterList();
            
            im.setValue(configId, SpcGetConfigItemInfo.CONFIG_ID);     // optional, STRING, Configuration Identifier
            //im.setValue(documentId, SpcGetConfigItemInfo.DOCUMENT_ID");     // optional, BYTE, GUID-Schlüssel des Preisfindungsbeleges (KNUMV)
            //im.setValue(itemId, SpcGetConfigItemInfo.ITEM_ID");     // optional, BYTE, Positionsnummer
            
            category.logT(Severity.PATH, location, "executing RFC SPC_GET_CONFIG_ITEM_INFO");
            ((RFCDefaultClient)cachingClient).execute(functionSpcGetConfigItemInfo);            
            category.logT(Severity.PATH, location, "done with RFC SPC_GET_CONFIG_ITEM_INFO");
            
            // process structure exExtConfigGuid
            JCO.Structure exExtConfigGuid = ex.getStructure(SpcGetConfigItemInfo.EXT_CONFIG_GUID);
            
            // process structure exExtConfigGuidExtConfig
            JCO.Structure exExtConfigGuidExtConfig = exExtConfigGuid.getStructure(SpcGetConfigItemInfo.EXT_CONFIG);            
                       
            // read parameters from EXT_CONFIG interface and put them into data-hashmap
            IMetaData extConfigMetaData = exExtConfigGuidExtConfig.getMetaData();
            int fieldCount = extConfigMetaData.getFieldCount();
            for (int i=0; i<fieldCount; i++){
                int fieldType = extConfigMetaData.getType(i);
                switch (fieldType) {
                    case IMetaData.TYPE_STRUCTURE:
                        JCO.Structure str = exExtConfigGuidExtConfig.getStructure(extConfigMetaData.getName(i));
                        processExtConfigStructure(data, str);
                        break;
                    case IMetaData.TYPE_TABLE:
                        JCO.Table table = exExtConfigGuidExtConfig.getTable(extConfigMetaData.getName(i));
                        processExtConfigTable(data, table);
                        break;
                    default:
                        JCO.Field fld = exExtConfigGuidExtConfig.getField(extConfigMetaData.getName(i));
                        processExtConfigField(data, fld);
                }
            }

            // process table exExtConfigGuidExtIds
            JCO.Table exExtConfigGuidExtIds = exExtConfigGuid.getTable(SpcGetConfigItemInfo.EXT_IDS);
            processExtConfigTable(data, exExtConfigGuidExtIds);

            // process single fields of export parameterlist
            processExtConfigField(data, ex.getField(SpcGetConfigItemInfo.PRODUCT_ID));
            processExtConfigField(data, ex.getField(SpcGetConfigItemInfo.PRODUCT_LOGSYS));
            processExtConfigField(data, ex.getField(SpcGetConfigItemInfo.PRODUCT_TYPE));
        } catch(ClientException e) {
            throw new IPCException(e);
        }
        return data;            
    }
    
    protected void processExtConfigField(HashMap data, JCO.Field field) {
        String key = field.getName() + "[1]";
        Object fieldValue = field.getValue();
        if (fieldValue == null) {
            fieldValue = new String("");
        }
        data.put(key, fieldValue);
    }
    
    protected void processExtConfigStructure(HashMap data, JCO.Structure structure) {
        int structureFieldCount = structure.getFieldCount();
        for (int i=0; i<structureFieldCount; i++){
            JCO.Field field = structure.getField(i);
            String key = structure.getName() + "-" + field.getName() + "[1]";
            Object fieldValue = field.getValue();
            if (fieldValue == null) {
                fieldValue = new String("");
            }
            data.put(key, fieldValue);
        }
    }
    
    protected void processExtConfigTable(HashMap data, JCO.Table table) {
        int tableCounter;
        for (tableCounter=0; tableCounter<table.getNumRows(); tableCounter++) {
            table.setRow(tableCounter);
            int tableFieldCount = table.getFieldCount();
            for (int i=0; i<tableFieldCount; i++){
                JCO.Field field = table.getField(i);
                String key = table.getName() + "-" + field.getName() + "[" + (tableCounter + 1) + "]";
                Object fieldValue = field.getValue();
                if (fieldValue == null) {
                    fieldValue = new String("");
                }
                data.put(key, fieldValue);
            }
        } 
    }

    /* (non-Javadoc)
     * @see com.sap.spc.remote.client.object.Configuration#reset()
     */
    public void reset() throws IPCException {
        cacheIsDirty = true;
		instanceCacheIsDirty = true;
        try {        
            String configId = this.id;
            JCO.Function functionCfgApiResetConfig = ((RFCDefaultClient)cachingClient).getFunction(IFunctionGroup.CFG_API_RESET_CONFIG);
            JCO.ParameterList im = functionCfgApiResetConfig.getImportParameterList();
            JCO.ParameterList ex = functionCfgApiResetConfig.getExportParameterList();
            JCO.ParameterList tbl = functionCfgApiResetConfig.getTableParameterList();
            
            im.setValue(configId, CfgApiResetConfig.CONFIG_ID);     // STRING, Configuration Identifier
            //im.setValue(instId, CfgApiResetConfig.INST_ID);     // optional, CHAR, Internal identifier of a Instance
            
            category.logT(Severity.PATH, location, "executing RFC CFG_API_RESET_CONFIG");
            ((RFCDefaultClient)cachingClient).execute(functionCfgApiResetConfig);
            category.logT(Severity.PATH, location, "done with RFC CFG_API_RESET_CONFIG");
             
			// set IPCItems dirty otherwise the reset configuration will not be loaded
			if (getInstances() != null) {
				Instance currentInstance = null; 
				for (Iterator instIter = getInstances().iterator(); instIter.hasNext();) {
					currentInstance = (Instance) instIter.next();
					DefaultIPCItem ipcItem = (DefaultIPCItem)currentInstance.getIpcItem();
					if (ipcItem != null) {
                		ipcItem.setCacheDirty(); //flush() sends some SET or DELETE commands, hence make Item cache also dirty.
            		}
				}
			}
            synchronize();
        } catch(ClientException e) {
            throw new IPCException(e);
        }
    }

    /* (non-Javadoc)
     * @see com.sap.spc.remote.client.object.Configuration#deleteInstance(com.sap.spc.remote.client.object.Instance)
     */
    public synchronized boolean deleteInstance(Instance instance) {
        cacheIsDirty = true;
        instanceCacheIsDirty = true;
        String instId = instance.getId();
        String configId = this.id;
        boolean success = false;
        try {
            JCO.Function functionCfgApiDeleteInstance = ((RFCDefaultClient)cachingClient).getFunction(IFunctionGroup.CFG_API_DELETE_INSTANCE);
            JCO.ParameterList im = functionCfgApiDeleteInstance.getImportParameterList();
            JCO.ParameterList ex = functionCfgApiDeleteInstance.getExportParameterList();
            JCO.ParameterList tbl = functionCfgApiDeleteInstance.getTableParameterList();

            im.setValue(configId, CfgApiDeleteInstance.CONFIG_ID);     // STRING, Configuration Identifier
            im.setValue(instId, CfgApiDeleteInstance.INST_ID);     // CHAR, Internal identifier of a Instance

            category.logT(Severity.PATH, location, "executing RFC CFG_API_DELETE_INSTANCE");
            ((RFCDefaultClient)cachingClient).execute(functionCfgApiDeleteInstance);
            category.logT(Severity.PATH, location, "done with RFC CFG_API_DELETE_INSTANCE");

            // update the config's change time
            setChangeTime();
            success = ex.getString(CfgApiDeleteInstance.SUCCESS).equals(TRUE);
            
        }catch(ClientException e) {
            category.logT(Severity.ERROR, location, e.toString());
            return false;
        }
        initMembers();
        return success;
    }

    /* (non-Javadoc)
     * @see com.sap.spc.remote.client.object.Configuration#getConflictInfos()
     */
    public List getConflictInfos() {
        List explanations = null;
        if (!isConsistent()) {
            try {        
                String configId = this.id;
                JCO.Function functionCfgApiGetConflicts = ((RFCDefaultClient)cachingClient).getFunction(IFunctionGroup.CFG_API_GET_CONFLICTS);
                JCO.ParameterList im = functionCfgApiGetConflicts.getImportParameterList();
                JCO.ParameterList ex = functionCfgApiGetConflicts.getExportParameterList();
                JCO.ParameterList tbl = functionCfgApiGetConflicts.getTableParameterList();
                
                im.setValue(configId, CfgApiGetConflicts.CONFIG_ID);     // STRING, Configuration Identifier
                // im.setValue(instId, CfgApiGetConflicts.INST_ID);     // optional, CHAR, Internal identifier of a Instance

                category.logT(Severity.PATH, location, "executing RFC CFG_API_GET_CONFLICTS");
                ((RFCDefaultClient)cachingClient).execute(functionCfgApiGetConflicts);
                category.logT(Severity.PATH, location, "done with RFC CFG_API_GET_CONFLICTS");

                // process table exConflicts
                // TABLE, Configuration Conflict Handling: Table of Conflicts
                JCO.Table exConflicts = ex.getTable(CfgApiGetConflicts.CONFLICTS);
                // initialize arrays with table length
                int numRows = exConflicts.getNumRows();
                String[] conflictIds = new String[numRows];
                String[] conflictInstanceIds = new String[numRows];
                String[] conflictInstanceNames = new String[numRows];
                String[] conflictAuthors = new String[numRows];
                String[] conflictTexts = new String[numRows];
                String[] conflictExplanations = new String[numRows];
                int exConflictsCounter;
                for (exConflictsCounter=0; exConflictsCounter<numRows; exConflictsCounter++) {
                    exConflicts.setRow(exConflictsCounter);
                    conflictIds[exConflictsCounter] = ((Integer)exConflicts.getValue(CfgApiGetConflicts.CONFLICT_ID)).toString();
                    conflictInstanceIds[exConflictsCounter] = exConflicts.getString(CfgApiGetConflicts.CONFLICT_INST_ID);
                    conflictInstanceNames[exConflictsCounter] = exConflicts.getString(CfgApiGetConflicts.CONFLICT_INST_LNAME);
                    conflictAuthors[exConflictsCounter] = exConflicts.getString(CfgApiGetConflicts.CONFLICT_AUTHOR);
                    conflictTexts[exConflictsCounter] = exConflicts.getString(CfgApiGetConflicts.CONFLICT_TEXT);
                    
                    // process table exConflictsConflictLongText
                    JCO.Table exConflictsConflictLongText = exConflicts.getTable(CfgApiGetConflicts.CONFLICT_LONG_TEXT);
                    int conflictNumRows = exConflictsConflictLongText.getNumRows();
                    String[] textFormat = new String[conflictNumRows]; // APD delivers only empty array
                    String textLine;
                    Integer textLineId;
                    StringBuffer conflictExplanationBuffer = new StringBuffer();
                    int exConflictsConflictLongTextCounter;
                    for (exConflictsConflictLongTextCounter=0; exConflictsConflictLongTextCounter<conflictNumRows; exConflictsConflictLongTextCounter++) {
                        exConflictsConflictLongText.setRow(exConflictsConflictLongTextCounter);
                        textFormat[exConflictsConflictLongTextCounter] = exConflictsConflictLongText.getString(CfgApiGetConflicts.TEXT_FORMAT);
                        conflictExplanationBuffer.append(exConflictsConflictLongText.getString(CfgApiGetConflicts.TEXT_LINE));
                        textLineId = (Integer)exConflictsConflictLongText.getValue(CfgApiGetConflicts.TEXT_LINE_ID);
                    }
                    conflictExplanations[exConflictsCounter] = conflictExplanationBuffer.toString();
                }
                explanations = new Vector(conflictIds.length);

                for (int i = 0; i < conflictIds.length; ++i) {
                    String explanation;
                    String documentation;
                    if (!conflictExplanations[i].equals("")
                        && (conflictExplanations[i].indexOf(ConflictInfo.EXPL) > -1)
                        && (conflictExplanations[i].indexOf(ConflictInfo.DOCU) > -1)) {
                        explanation =
                            conflictExplanations[i].substring(
                                conflictExplanations[i].indexOf(ConflictInfo.EXPL)
                                    + ConflictInfo.EXPL.length()
                                    + 1,
                                conflictExplanations[i].indexOf(ConflictInfo.DOCU)
                                    - 1);
                        documentation =
                            conflictExplanations[i].substring(
                                conflictExplanations[i].indexOf(ConflictInfo.DOCU)
                                    + ConflictInfo.DOCU.length()
                                    + 1,
                                conflictExplanations[i].length() - 1);
                    } else {
                        explanation = conflictTexts[i];
                        documentation = conflictTexts[i];
                    }
                    String instanceName =
                        conflictInstanceNames[i].substring(
                            conflictInstanceNames[i].indexOf(ConflictInfo.INSTANCE_$)
                                + ConflictInfo.INSTANCE_$.length());

                    DefaultConflictInfo conflictInfo =
                        new DefaultConflictInfo(
                            conflictIds[i],
                            conflictInstanceIds[i],
                            instanceName,
                            conflictTexts[i],
                            explanation,
                            documentation);
                    explanations.add(conflictInfo);
                }
            } catch(ClientException e) {
                throw new IPCException(e);
            }
            return explanations;
        }else {
        	return new Vector(); 
        }
    }
    
    public String[] getInstanceNames(){
        String[] instName;
        try {        
            String configId = this.id;
            String getMimeObjects = FALSE;
            
    
            JCO.Function functionCfgApiGetInstances = ((RFCDefaultClient)cachingClient).getFunction(IFunctionGroup.CFG_API_GET_INSTANCES);
            JCO.ParameterList im = functionCfgApiGetInstances.getImportParameterList();
            JCO.ParameterList ex = functionCfgApiGetInstances.getExportParameterList();
            JCO.ParameterList tbl = functionCfgApiGetInstances.getTableParameterList();
    
            im.setValue(configId, CfgApiGetInstances.CONFIG_ID);     // STRING, Configuration Identifier
            im.setValue(getMimeObjects, CfgApiGetInstances.GET_MIME_OBJECTS);        // optional, CHAR, Get Mime Objects (F=False, T=True, ""=False)
            
            category.logT(Severity.PATH, location, "executing RFC CFG_API_GET_INSTANCES");
            ((RFCDefaultClient)cachingClient).execute(functionCfgApiGetInstances);
            category.logT(Severity.PATH, location, "done with RFC CFG_API_GET_INSTANCES");
            
            JCO.Table exInstances = ex.getTable(CfgApiGetInstances.INSTANCES);
            int numRows = exInstances.getNumRows();
            // we are only interested in the instance names
            instName = new String[numRows];
            
            int exInstancesCounter;
            for (exInstancesCounter=0; exInstancesCounter<numRows; exInstancesCounter++) {
                exInstances.setRow(exInstancesCounter);
                instName[exInstancesCounter] = exInstances.getString(CfgApiGetInstances.INST_NAME);
            }
            
            
        } catch (ClientException e) {
            category.logT(Severity.ERROR, location, e.toString());
            throw new IPCException(e);
        }
        if (instName == null){
            instName = new String[0];
        }
        return instName;
    }

    /* (non-Javadoc)
     * @see com.sap.spc.remote.client.object.Configuration#compareConfigurations(com.sap.spc.remote.client.util.ext_configuration, com.sap.spc.remote.client.util.ext_configuration)
     */
    public ComparisonResult compareConfigurations(
        ext_configuration extConfig1,
        ext_configuration extConfig2) {
        
        location.entering("compareConfigurations(extConfig1, extConfig2)");        
        ComparisonResult result = factory.newComparisonResult(null, null);
        List errorList = new ArrayList();
        
        if (extConfig1 == null || extConfig2 == null){
            errorList.add(EXT_CONFIGS_NULL);
        }
        else {
            try {
                JCO.Function functionCfgApiCompareConfigurations = ((RFCDefaultClient)cachingClient).getFunction(IFunctionGroup.CFG_API_COMPARE_CONFIGURATIONS);
                JCO.ParameterList im = functionCfgApiCompareConfigurations.getImportParameterList();
                JCO.ParameterList ex = functionCfgApiCompareConfigurations.getExportParameterList();
                JCO.ParameterList tbl = functionCfgApiCompareConfigurations.getTableParameterList();
    
                // fill extConfigStructure1 with the data of extConfig1            
                JCO.Structure imCfgapicompareconfigurations_extConfig1 = im.getStructure(CfgApiCompareConfigurations.EXT_CONFIG_1);
                JCO.Structure extConfigStructure1 = imCfgapicompareconfigurations_extConfig1.getStructure(CfgApiCompareConfigurations.EXT_CONFIG);
                ExternalConfigConverter.getConfig(extConfig1, extConfigStructure1);
                
                // fill extConfigStructure2 with the data of extConfig2
                JCO.Structure imCfgapicompareconfigurations_extConfig2 = im.getStructure(CfgApiCompareConfigurations.EXT_CONFIG_2);
                JCO.Structure extConfigStructure2 = imCfgapicompareconfigurations_extConfig2.getStructure(CfgApiCompareConfigurations.EXT_CONFIG);
                ExternalConfigConverter.getConfig(extConfig2, extConfigStructure2);
                
                category.logT(Severity.PATH, location, "executing RFC CFG_API_COMPARE_CONFIGURATIONS");
                ((RFCDefaultClient)cachingClient).execute(functionCfgApiCompareConfigurations); // does not define ABAP Exceptions
                category.logT(Severity.PATH, location, "done with RFC CFG_API_COMPARE_CONFIGURATIONS");
                
                // process structure exCfgapicompareconfigurations_extConfigDelta
                JCO.Structure exCfgapicompareconfigurations_extConfigDelta = ex.getStructure(CfgApiCompareConfigurations.EXT_CONFIG_DELTA);
                JCO.Table exCfgapicompareconfigurations_extConfigDeltaCfgapicompareconfigurations_headerDelta = exCfgapicompareconfigurations_extConfigDelta.getTable(CfgApiCompareConfigurations.HEADER_DELTA);
                JCO.Table exCfgapicompareconfigurations_extConfigDeltaCfgapicompareconfigurations_instanceDeltas = exCfgapicompareconfigurations_extConfigDelta.getTable(CfgApiCompareConfigurations.INSTANCE_DELTAS);
                int headerDeltaNumRows = exCfgapicompareconfigurations_extConfigDeltaCfgapicompareconfigurations_headerDelta.getNumRows();
                int instanceDeltaNumRows = exCfgapicompareconfigurations_extConfigDeltaCfgapicompareconfigurations_instanceDeltas.getNumRows();
                
                if (headerDeltaNumRows>0 || instanceDeltaNumRows>0){
                    // there are differences
                    // header deltas
                    ArrayList headerDeltas = new ArrayList();
                    // process table exCfgapicompareconfigurations_extConfigDeltaCfgapicompareconfigurations_headerDelta
                    for (int exCfgapicompareconfigurations_extConfigDeltaCfgapicompareconfigurations_headerDeltaCounter=0; exCfgapicompareconfigurations_extConfigDeltaCfgapicompareconfigurations_headerDeltaCounter<exCfgapicompareconfigurations_extConfigDeltaCfgapicompareconfigurations_headerDelta.getNumRows(); exCfgapicompareconfigurations_extConfigDeltaCfgapicompareconfigurations_headerDeltaCounter++) {
                        exCfgapicompareconfigurations_extConfigDeltaCfgapicompareconfigurations_headerDelta.setRow(exCfgapicompareconfigurations_extConfigDeltaCfgapicompareconfigurations_headerDeltaCounter);
                        String fieldName = exCfgapicompareconfigurations_extConfigDeltaCfgapicompareconfigurations_headerDelta.getString(CfgApiCompareConfigurations.FIELD_NAME);
                        String value1 = exCfgapicompareconfigurations_extConfigDeltaCfgapicompareconfigurations_headerDelta.getString(CfgApiCompareConfigurations.VALUE_1);
                        String value2 = exCfgapicompareconfigurations_extConfigDeltaCfgapicompareconfigurations_headerDelta.getString(CfgApiCompareConfigurations.VALUE_2);
                        Delta headerDelta = factory.newDelta(fieldName, value1, value2);
                        headerDeltas.add(headerDelta);                    
                    }
    
                    // instance deltas
                    ArrayList instanceDeltas = new ArrayList();                
                    // process table exCfgapicompareconfigurations_extConfigDeltaCfgapicompareconfigurations_instanceDeltas            
                    for (int exCfgapicompareconfigurations_extConfigDeltaCfgapicompareconfigurations_instanceDeltasCounter=0; exCfgapicompareconfigurations_extConfigDeltaCfgapicompareconfigurations_instanceDeltasCounter<exCfgapicompareconfigurations_extConfigDeltaCfgapicompareconfigurations_instanceDeltas.getNumRows(); exCfgapicompareconfigurations_extConfigDeltaCfgapicompareconfigurations_instanceDeltasCounter++) {
                        exCfgapicompareconfigurations_extConfigDeltaCfgapicompareconfigurations_instanceDeltas.setRow(exCfgapicompareconfigurations_extConfigDeltaCfgapicompareconfigurations_instanceDeltasCounter);
                        String instId1 = exCfgapicompareconfigurations_extConfigDeltaCfgapicompareconfigurations_instanceDeltas.getString(CfgApiCompareConfigurations.INST_ID_1);
                        String instId2 = exCfgapicompareconfigurations_extConfigDeltaCfgapicompareconfigurations_instanceDeltas.getString(CfgApiCompareConfigurations.INST_ID_2);
                        String itemId = exCfgapicompareconfigurations_extConfigDeltaCfgapicompareconfigurations_instanceDeltas.getString(CfgApiCompareConfigurations.ITEM_ID);
                        String decompPath = exCfgapicompareconfigurations_extConfigDeltaCfgapicompareconfigurations_instanceDeltas.getString(CfgApiCompareConfigurations.DECOMP_PATH);
                        String author = exCfgapicompareconfigurations_extConfigDeltaCfgapicompareconfigurations_instanceDeltas.getString(CfgApiCompareConfigurations.AUTHOR);
                        String changeFlag = exCfgapicompareconfigurations_extConfigDeltaCfgapicompareconfigurations_instanceDeltas.getString(CfgApiCompareConfigurations.CHANGE_FLAG);
                        // check if it is a "delete" instance delta
                        if (changeFlag.equals(CfgApiCompareConfigurations.DELETED)){
                            InstanceDelta iDelta = factory.newInstanceDelta(instId1, instId2, itemId, decompPath, null, null, null, author, InstanceDelta.DELETED);
                            instanceDeltas.add(iDelta);
                        }   
                        // check if it is an "insert" instance delta
                        else if (changeFlag.equals(CfgApiCompareConfigurations.INSERTED)){
                            InstanceDelta iDelta = factory.newInstanceDelta(instId1, instId2, itemId, decompPath, null, null, null, author, InstanceDelta.INSERTED);
                            instanceDeltas.add(iDelta);
                        }   
                        // it is an "update" instance delta
                        else {                 
                            // process table exCfgapicompareconfigurations_extConfigDeltaCfgapicompareconfigurations_instanceDeltasCfgapicompareconfigurations_instDeltas
                            // deltas on instance itself
                            JCO.Table exCfgapicompareconfigurations_extConfigDeltaCfgapicompareconfigurations_instanceDeltasCfgapicompareconfigurations_instDeltas = exCfgapicompareconfigurations_extConfigDeltaCfgapicompareconfigurations_instanceDeltas.getTable(CfgApiCompareConfigurations.INST_DELTAS);
                            ArrayList instDeltas = new ArrayList();
                            for (int exCfgapicompareconfigurations_extConfigDeltaCfgapicompareconfigurations_instanceDeltasCfgapicompareconfigurations_instDeltasCounter=0; exCfgapicompareconfigurations_extConfigDeltaCfgapicompareconfigurations_instanceDeltasCfgapicompareconfigurations_instDeltasCounter<exCfgapicompareconfigurations_extConfigDeltaCfgapicompareconfigurations_instanceDeltasCfgapicompareconfigurations_instDeltas.getNumRows(); exCfgapicompareconfigurations_extConfigDeltaCfgapicompareconfigurations_instanceDeltasCfgapicompareconfigurations_instDeltasCounter++) {
                                exCfgapicompareconfigurations_extConfigDeltaCfgapicompareconfigurations_instanceDeltasCfgapicompareconfigurations_instDeltas.setRow(exCfgapicompareconfigurations_extConfigDeltaCfgapicompareconfigurations_instanceDeltasCfgapicompareconfigurations_instDeltasCounter);
                                String fieldName = exCfgapicompareconfigurations_extConfigDeltaCfgapicompareconfigurations_instanceDeltasCfgapicompareconfigurations_instDeltas.getString(CfgApiCompareConfigurations.FIELD_NAME);
                                String value1 = exCfgapicompareconfigurations_extConfigDeltaCfgapicompareconfigurations_instanceDeltasCfgapicompareconfigurations_instDeltas.getString(CfgApiCompareConfigurations.VALUE_1);
                                String value2 = exCfgapicompareconfigurations_extConfigDeltaCfgapicompareconfigurations_instanceDeltasCfgapicompareconfigurations_instDeltas.getString(CfgApiCompareConfigurations.VALUE_2);
                                Delta delta = factory.newDelta(fieldName, value1, value2);
                                instDeltas.add(delta);                            
                            }
                            // process table exCfgapicompareconfigurations_extConfigDeltaCfgapicompareconfigurations_instanceDeltasCfgapicompareconfigurations_partOfDeltas
                            // part of deltas
                            JCO.Table exCfgapicompareconfigurations_extConfigDeltaCfgapicompareconfigurations_instanceDeltasCfgapicompareconfigurations_partOfDeltas = exCfgapicompareconfigurations_extConfigDeltaCfgapicompareconfigurations_instanceDeltas.getTable(CfgApiCompareConfigurations.PART_OF_DELTAS);
                            ArrayList partOfDeltas = new ArrayList();
                            for (int exCfgapicompareconfigurations_extConfigDeltaCfgapicompareconfigurations_instanceDeltasCfgapicompareconfigurations_partOfDeltasCounter=0; exCfgapicompareconfigurations_extConfigDeltaCfgapicompareconfigurations_instanceDeltasCfgapicompareconfigurations_partOfDeltasCounter<exCfgapicompareconfigurations_extConfigDeltaCfgapicompareconfigurations_instanceDeltasCfgapicompareconfigurations_partOfDeltas.getNumRows(); exCfgapicompareconfigurations_extConfigDeltaCfgapicompareconfigurations_instanceDeltasCfgapicompareconfigurations_partOfDeltasCounter++) {
                                exCfgapicompareconfigurations_extConfigDeltaCfgapicompareconfigurations_instanceDeltasCfgapicompareconfigurations_partOfDeltas.setRow(exCfgapicompareconfigurations_extConfigDeltaCfgapicompareconfigurations_instanceDeltasCfgapicompareconfigurations_partOfDeltasCounter);
                                String fieldName = exCfgapicompareconfigurations_extConfigDeltaCfgapicompareconfigurations_instanceDeltasCfgapicompareconfigurations_partOfDeltas.getString(CfgApiCompareConfigurations.FIELD_NAME);
                                String value1 = exCfgapicompareconfigurations_extConfigDeltaCfgapicompareconfigurations_instanceDeltasCfgapicompareconfigurations_partOfDeltas.getString(CfgApiCompareConfigurations.VALUE_1);
                                String value2 = exCfgapicompareconfigurations_extConfigDeltaCfgapicompareconfigurations_instanceDeltasCfgapicompareconfigurations_partOfDeltas.getString(CfgApiCompareConfigurations.VALUE_2);
                                Delta delta = factory.newDelta(fieldName, value1, value2);
                                partOfDeltas.add(delta);                            
                            }
                            
                            // process table exCfgapicompareconfigurations_extConfigDeltaCfgapicompareconfigurations_instanceDeltasCfgapicompareconfigurations_valueDeltas
                            // value deltas
                            JCO.Table exCfgapicompareconfigurations_extConfigDeltaCfgapicompareconfigurations_instanceDeltasCfgapicompareconfigurations_valueDeltas = exCfgapicompareconfigurations_extConfigDeltaCfgapicompareconfigurations_instanceDeltas.getTable(CfgApiCompareConfigurations.VALUE_DELTAS);
                            ArrayList valueDeltas = new ArrayList();
                            for (int exCfgapicompareconfigurations_extConfigDeltaCfgapicompareconfigurations_instanceDeltasCfgapicompareconfigurations_valueDeltasCounter=0; exCfgapicompareconfigurations_extConfigDeltaCfgapicompareconfigurations_instanceDeltasCfgapicompareconfigurations_valueDeltasCounter<exCfgapicompareconfigurations_extConfigDeltaCfgapicompareconfigurations_instanceDeltasCfgapicompareconfigurations_valueDeltas.getNumRows(); exCfgapicompareconfigurations_extConfigDeltaCfgapicompareconfigurations_instanceDeltasCfgapicompareconfigurations_valueDeltasCounter++) {
                                exCfgapicompareconfigurations_extConfigDeltaCfgapicompareconfigurations_instanceDeltasCfgapicompareconfigurations_valueDeltas.setRow(exCfgapicompareconfigurations_extConfigDeltaCfgapicompareconfigurations_instanceDeltasCfgapicompareconfigurations_valueDeltasCounter);
                                String charc = exCfgapicompareconfigurations_extConfigDeltaCfgapicompareconfigurations_instanceDeltasCfgapicompareconfigurations_valueDeltas.getString(CfgApiCompareConfigurations.CHARC);
                                String value = exCfgapicompareconfigurations_extConfigDeltaCfgapicompareconfigurations_instanceDeltasCfgapicompareconfigurations_valueDeltas.getString(CfgApiCompareConfigurations.VALUE);
                                String valueUnit = exCfgapicompareconfigurations_extConfigDeltaCfgapicompareconfigurations_instanceDeltasCfgapicompareconfigurations_valueDeltas.getString(CfgApiCompareConfigurations.VALUE_UNIT);
                                String vdAuthor = exCfgapicompareconfigurations_extConfigDeltaCfgapicompareconfigurations_instanceDeltasCfgapicompareconfigurations_valueDeltas.getString(CfgApiCompareConfigurations.AUTHOR);
                                String vdChangeFlag = exCfgapicompareconfigurations_extConfigDeltaCfgapicompareconfigurations_instanceDeltasCfgapicompareconfigurations_valueDeltas.getString(CfgApiCompareConfigurations.CHANGE_FLAG);
                                StringBuffer configObjKey = new StringBuffer(instId1);
                                configObjKey.append(".");
                                configObjKey.append(charc);
                                ValueDelta vDelta = factory.newValueDelta(configObjKey.toString(), charc, value, valueUnit, vdAuthor, vdChangeFlag);
                                valueDeltas.add(vDelta);                            
                            }
                            InstanceDelta iDelta = factory.newInstanceDelta(instId1, instId2, itemId, decompPath, instDeltas, partOfDeltas, valueDeltas, author, InstanceDelta.UPDATED);
                            instanceDeltas.add(iDelta);
                        }
                    }
                    location.debugT("There are differences: create a new ComparisonResult object.");
                    result = factory.newComparisonResult(extConfig1, extConfig2, headerDeltas, instanceDeltas);
                }
                else {
                    // has no differences, create an empty result
                    location.debugT("There are no differences: create an empty ComparisonResult object.");
                    result = factory.newComparisonResult(null, null);
                }
            }
            catch(ClientException e) {
                location.debugT("Error in FM call CFG_API_COMPARE_CONFIGURATIONS");
                errorList.add(ERROR_IN_FM);
            }
        }

        if (errorList.size()>0){
            // errors during comparison occured, create message(s)
            location.debugT("There have been errors during configuration.");
            result.setComparisonErrors(errorList);
        }
        location.exiting();
        return result;
    }

}
