/*
 * Created on Dec 22, 2004
 *
 * To change the template for this generated file go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
package com.sap.spc.remote.client.object.imp.tcp;

import java.util.ArrayList;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Vector;

import com.sap.isa.core.TechKey;
import com.sap.msasrv.constants.ServerConstants;
import com.sap.msasrv.usermodule.spc.ISPCCommandSet;
import com.sap.spc.remote.client.ClientException;
import com.sap.spc.remote.client.ResourceAccessor;
import com.sap.spc.remote.client.object.ConfigurationSnapshot;
import com.sap.spc.remote.client.object.DimensionalValue;
import com.sap.spc.remote.client.object.IClient;
import com.sap.spc.remote.client.object.IPCDocument;
import com.sap.spc.remote.client.object.IPCException;
import com.sap.spc.remote.client.object.IPCItem;
import com.sap.spc.remote.client.object.IPCItemProperties;
import com.sap.spc.remote.client.object.IPCItemReference;
import com.sap.spc.remote.client.object.LoadingStatus;
import com.sap.spc.remote.client.object.OrderConfiguration;
import com.sap.spc.remote.client.object.ProductKey;
import com.sap.spc.remote.client.object.imp.ConfigValue;
import com.sap.spc.remote.client.object.imp.ConfigurationValue;
import com.sap.spc.remote.client.object.imp.DefaultConfigurationSnapshot;
import com.sap.spc.remote.client.object.imp.DefaultIPCDocument;
import com.sap.spc.remote.client.object.imp.DefaultIPCItem;
import com.sap.spc.remote.client.object.imp.DefaultIPCItemProperties;
import com.sap.spc.remote.client.object.imp.DefaultIPCSession;
import com.sap.spc.remote.client.object.imp.IPCClientObjectFactory;
import com.sap.spc.remote.client.object.imp.IllegalClassException;
import com.sap.spc.remote.client.object.imp.TableValue;
import com.sap.spc.remote.client.tcp.ExternalConfigConverter;
import com.sap.spc.remote.client.tcp.PricingConverter;
import com.sap.spc.remote.client.tcp.ServerResponse;
import com.sap.spc.remote.client.util.ext_configuration;
import com.sap.spc.remote.client.util.imp.c_ext_cfg_imp;
import com.sap.spc.remote.shared.ConfigException;
import com.sap.spc.remote.shared.SimpleRequestWrapper;
import com.sap.spc.remote.shared.command.ChangeItemConfig;
import com.sap.spc.remote.shared.command.ChangeItemProduct;
import com.sap.spc.remote.shared.command.ChangeItems;
import com.sap.spc.remote.shared.command.ConvertInternalProductIdsToProductGUIDs;
import com.sap.spc.remote.shared.command.CopyItem;
import com.sap.spc.remote.shared.command.CreateItems;
import com.sap.spc.remote.shared.command.ExtConfigConstants;
import com.sap.spc.remote.shared.command.GetConfigItemInfo;
import com.sap.spc.remote.shared.command.GetItemConditions;
import com.sap.spc.remote.shared.command.GetItemInfo;
import com.sap.spc.remote.shared.command.GetPricingItemInfo;
import com.sap.spc.remote.shared.command.GetXMLPricingTrace;
import com.sap.spc.remote.shared.command.SCECommand;
import com.sap.tc.logging.Category;
import com.sap.tc.logging.Location;
import com.sap.tc.logging.Severity;

/**
 * @author I026584
 *
 * To change the template for this generated type comment go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
public class TcpDefaultIPCItem extends DefaultIPCItem implements IPCItem {

	private static final Category category = ResourceAccessor.category;
	private static final Location location = ResourceAccessor.getLocation(TcpDefaultIPCItem.class);
	protected HashMap data = new HashMap(); //Pricing cached data. 

	// Constructors
	protected TcpDefaultIPCItem(DefaultIPCDocument document, IPCItemProperties props) throws IPCException {
		_document = document;
		_parent   = null;
		_children = null;
		_childrenCopy = null;
        _tteItem = null; // will be set separately

		if (props instanceof DefaultIPCItemProperties) {
			_props = (DefaultIPCItemProperties)props;
		}
		else {
			_props = new TcpDefaultIPCItemProperties(props);
		}
        TcpDefaultPricingConverter pc = (TcpDefaultPricingConverter)((TcpDefaultIPCSession)document.getSession()).getPricingConverter();
        
		String productGuid = _props.getProductGuid();
		String productId = _props.getProductId();
		String productType = _props.getProductType();
		String productLogSys = _props.getProductLogSys();
		DimensionalValue salesQuantity = _props.getSalesQuantity();
		DimensionalValue baseQuantity = _props.getBaseQuantity();
		String targetDate = _props.getDate();
		Map headerAttributes = _props.getHeaderAttributes();
		Map itemAttributes = _props.getItemAttributes();
		Map conditionTimestamps = _props.getConditionTimestamps();

		_closed = false;

		_client = ((TcpDefaultIPCSession)document.getSession()).getClient();

		// call immediately without delay
		Vector paraVec = new Vector();
		paraVec.addElement(CreateItems.DOCUMENT_ID);
		paraVec.addElement(document.getId());
		//pass itemid from extern
		if (props.getItemId() != null && !props.getItemId().equals("")) {
			paraVec.addElement(CreateItems.ITEM_IDS);
			paraVec.addElement(props.getItemId());
		}
		if (productGuid != null) {
			paraVec.addElement(CreateItems.PRODUCT_GUIDS);
			paraVec.addElement(productGuid);
		}
//		if (productId != null) {
//			paraVec.addElement("productIds");
//			paraVec.addElement(productId);
//		}
		if (productId != null && productGuid == null) {
			productGuid = getProductGuid(productId, productType, productLogSys, _client);
			paraVec.addElement(CreateItems.PRODUCT_GUIDS);
			paraVec.addElement(productGuid);
		}
//		if (salesQuantity != null) {
//			paraVec.addElement(CreateItems.SALES_QUANTITY_UNITS);
//			paraVec.addElement(salesQuantity.getUnit());
//			paraVec.addElement(CreateItems.SALES_QUANTITY_VALUES);
//			paraVec.addElement(salesQuantity.getValueAsString());
//		}
		if (baseQuantity != null && baseQuantity.getValueAsString() != null) {
			 // conversion needed because command accepts only baseQuantity (internal) as of 5.0
			 String unit = baseQuantity.getUnit();
			 String value = baseQuantity.getValueAsString();
	
			paraVec.addElement(CreateItems.SALES_QUANTITY_UNITS);
			paraVec.addElement(/*salesQuantity.getUnit()*/unit);
			paraVec.addElement(CreateItems.SALES_QUANTITY_VALUES);
			paraVec.addElement(/*salesQuantity.getValueAsString()*/value);
		}else if (salesQuantity != null && salesQuantity.getValueAsString() != null) {
			 // conversion needed because command accepts only baseQuantity (internal) as of 5.0
			 String unit = pc.convertUOMExternalToInternal(salesQuantity.getUnit());
			 String value = pc.convertValueExternalToInternal(salesQuantity.getValueAsString(), salesQuantity.getUnit(), document._getDocumentProperties(false).getDecimalSeparator(), document._getDocumentProperties(false).getGroupingSeparator());

			paraVec.addElement(CreateItems.SALES_QUANTITY_UNITS);
			paraVec.addElement(/*salesQuantity.getUnit()*/unit);
			paraVec.addElement(CreateItems.SALES_QUANTITY_VALUES);
			paraVec.addElement(/*salesQuantity.getValueAsString()*/value);
		}else{
			log.warn("Neiter baseQuantity nor salesQuantity specified for the creation of item for " + productId);
		}
		if (targetDate != null) {
			paraVec.addElement(CreateItems.KNOWLEDGE_BASE_DATES);
			paraVec.addElement(targetDate);
			paraVec.addElement(CreateItems.PRICING_DATES);
			paraVec.addElement(targetDate);
		}
        
        
		_addMapsParamsToVector(1, itemAttributes, paraVec, CreateItems.ITEM_ATTRIBUTE_NAMES, CreateItems.ITEM_ATTRIBUTE_VALUES, CreateItems.ITEM_ATTRIBUTE_ITEM_REFS);
		_addMapsParamsToVector(1, headerAttributes, paraVec, CreateItems.HEADER_ATTRIBUTE_NAMES, CreateItems.HEADER_ATTRIBUTE_VALUES, CreateItems.HEADER_ATTRIBUTE_ITEM_REFS);

		if (_props.getExchangeRateType() != null) {
			paraVec.addElement(CreateItems.EXCHANGE_RATE_TYPES);
			paraVec.addElement(_props.getExchangeRateType());
		}
		if (_props.getExchangeRateDate() != null) {
			paraVec.addElement(CreateItems.EXCHANGE_RATE_DATES);
			paraVec.addElement(_props.getExchangeRateDate());
		}

		// add knowledgebase information
		String kbLogSys = _props.getKbLogSys();
		if (kbLogSys != null) {
			paraVec.addElement(CreateItems.KNOWLEDGE_BASE_LOGSYS);
			paraVec.addElement(kbLogSys);
		}
		String kbName = _props.getKbName();
		if (kbName != null) {
			paraVec.addElement(CreateItems.KNOWLEDGE_BASE_NAMES);
			paraVec.addElement(kbName);
		}
		String kbVersion = _props.getKbVersion();
		if (kbVersion != null) {
			paraVec.addElement(CreateItems.KNOWLEDGE_BASE_VERSIONS);
			paraVec.addElement(kbVersion);
		}
		String kbProfile = _props.getKbProfile();
		if (kbProfile != null) {
			paraVec.addElement(CreateItems.KNOWLEDGE_BASE_PROFILES);
			paraVec.addElement(kbProfile);
		}

		// add configuration data
		ConfigValue cfgValue = _props.getConfigValue();
		if (cfgValue != null) {
			String[] cfgParams = cfgValue.getConfigIPCParameters();
			if (cfgParams != null) {
				for (int i=0; i<cfgParams.length; i++) {
					paraVec.addElement(cfgParams[i]);
				}
			}
		}

		Map context = _props.getContext();
		_addMapsParamsToVector(1, context, paraVec, CreateItems.SCE_CONTEXT_NAMES, CreateItems.SCE_CONTEXT_VALUES, CreateItems.SCE_CONTEXT_ITEM_REFS);

		boolean performPricingAnalysis = _props.getPerformPricingAnalysis();
		_addBooleanParamToVector(performPricingAnalysis, paraVec, CreateItems.PERFORM_PRICING_ANALYSIS);

        boolean decoupleSubitems = _props.isDecoupleSubitems();
        _addBooleanParamToVector(decoupleSubitems, paraVec, CreateItems.GRID_PRODUCTS);

		// add additional parameters set by the user
		List additionalParaNames = _props.getCreationCommandParameterNames();
		List additionalParaValues = _props.getCreationCommandParameterValues();
		if (additionalParaNames != null) {
			int parasize = additionalParaNames.size();
			for (int i=0; i<parasize; i++) {
				paraVec.addElement(additionalParaNames.get(i));
				paraVec.addElement(additionalParaValues.get(i));
			}
		}

		String[] params = new String[paraVec.size()];
		paraVec.copyInto(params);
		try{
			ServerResponse r = ((TCPDefaultClient)_client).doCmd(ISPCCommandSet.CREATE_ITEMS, params);
			_itemId = r.getParameterValue(CreateItems.ITEM_IDS);
            this.setTechKey(new TechKey(_itemId));
			_config = new ConfigurationValue(document.getSession().getIPCClient(),
										 com.sap.spc.remote.shared.ConfigIdManager.getConfigId(document.getId(),_itemId));
			_props.initializeValues((DefaultIPCSession)document.getSession(), document.getId(), _itemId, true/*default formatting*/, document.getSession().isExtendedDataEnabled());
			setCacheDirty();
            // removed the syncWithServer because this lead to adding multiple items to the document
			//syncWithServer(); //This call only can sync document, which sets the common properties of item
		}catch(ClientException e){
			throw new IPCException(e);
		}
		_pricingConditionSet = new TcpDefaultIPCPricingConditionSet(this);
	}


	//Don't try to call sycn process, as this constrectures are not internally calling any server command.
	protected TcpDefaultIPCItem(TCPDefaultClient client, DefaultIPCDocument document, String itemId) throws IPCException {
		_client = client;
		_document = document;
		_itemId = itemId;
        this.setTechKey(new TechKey(_itemId));
		_props = factory.newIPCItemProperties();
		_props.setItemId(itemId);
		_resetInternalConfiguration();
		_props.initializeValues((DefaultIPCSession)document.getSession(), document.getId(), _itemId, true/*default formatting*/, document.getSession().isExtendedDataEnabled());
		_pricingConditionSet = new TcpDefaultIPCPricingConditionSet(this);
        _tteItem = null; // will be set separately
	}


	//Don't try to call sycn process, as this constrectures are not internally calling any server command.
	protected TcpDefaultIPCItem(TCPDefaultClient client, DefaultIPCDocument document, String itemId, String parentItemId) throws IPCException {
		this(client, document, itemId);
		if (parentItemId == null || parentItemId.equals("")) {
			_parent = null;
			// document.addItem(this); // removed because this could lead to adding the same item twice. An item should not add itself to the document, that should be done where the item is created and then the item should be added.
		}
		else {
			_parent = ((DefaultIPCDocument)document)._getItemRecursive(parentItemId);
			if (_parent == null){
				throw new IPCException(
					new ClientException(ClientException.RET_INTERNAL_ERROR,"Without parent item, trying to create subitem on client for itemId " + itemId + " with parentId " +parentItemId +". Please make sure the parentItem is already avaialble on the client before creating subItems.")
					);

			}else{
				_parent.addChild(this);
			}
		}
	}




	/* (non-Javadoc)
	 * 
	 */
	public static IPCItem[] createItems(IPCDocument doc, IPCItemProperties[] props)
		throws IPCException 
	{
       location.entering("createItems(IPCDocument doc, IPCItemProperties[] props)");
       if (location.beDebug()){
              location.debugT("Parameters:");
              location.debugT("IPCDocument doc " + doc.getId());
              location.debugT("IPCItemProperties[] props " + props);
              if (props != null) {
                  for (int i = 0; i<props.length; i++){
                      location.debugT("  props["+i+"] "+props[i].toString());
                  }
              }
       }
	   if (!(doc instanceof DefaultIPCDocument))
		   throw new IllegalClassException(doc, DefaultIPCDocument.class);
	   DefaultIPCDocument document = (DefaultIPCDocument)doc;
       TcpDefaultPricingConverter pc = (TcpDefaultPricingConverter)((TcpDefaultIPCSession)document.getSession()).getPricingConverter();

	   IClient client = ((DefaultIPCSession)document.getSession()).getClient();

	   Vector paraVec = new Vector();
	   paraVec.addElement(CreateItems.DOCUMENT_ID);
	   paraVec.addElement(document.getId());

	   for (int i=0; i<props.length; i++) {
		   String productGuid = props[i].getProductGuid();
		   String productId = props[i].getProductId();
			String productType = props[i].getProductType();
			String productLogSys = props[i].getProductLogSys();
		   Boolean pricingRelevant = props[i].isPricingRelevant();                    
		   String highLevelItemId = props[i].getHighLevelItemId();
		   DimensionalValue salesQuantity = props[i].getSalesQuantity();
		   DimensionalValue baseQuantity = props[i].getBaseQuantity();
		   String targetDate = props[i].getDate();
		   Map headerAttributes = props[i].getHeaderAttributes();
		   Map itemAttributes = props[i].getItemAttributes();
		   Map conditionTimestamps = props[i].getConditionTimestamps();

		   // call immediately without delay
			if (props[i].getItemId() != null && !props[i].getItemId().equals("")) {
				paraVec.addElement(CreateItems.ITEM_IDS);
				paraVec.addElement(props[i].getItemId());
			}
		   if (productGuid != null) {
			   paraVec.addElement(CreateItems.PRODUCT_GUIDS);
			   paraVec.addElement(productGuid);
		   }
//		   if (productId != null) {
//			   paraVec.addElement("productIds");
//			   paraVec.addElement(productId);
//		   }
			if (productId != null && productGuid == null) {
				productGuid = getProductGuid(productId, productType, productLogSys, client);
				paraVec.addElement(CreateItems.PRODUCT_GUIDS);
				paraVec.addElement(productGuid);
			}
//		   if (salesQuantity != null) {
//			   paraVec.addElement(CreateItems.SALES_QUANTITY_UNITS);
//			   paraVec.addElement(salesQuantity.getUnit());
//			   paraVec.addElement(CreateItems.SALES_QUANTITY_VALUES);
//			   paraVec.addElement(salesQuantity.getValueAsString());
//		   }
			if (baseQuantity != null && baseQuantity.getValueAsString() != null) {
				 // conversion needed because command accepts only baseQuantity (internal) as of 5.0
				 String unit = baseQuantity.getUnit();
				 String value = baseQuantity.getValueAsString();
		
				paraVec.addElement(CreateItems.SALES_QUANTITY_UNITS);
				paraVec.addElement(/*salesQuantity.getUnit()*/unit);
				paraVec.addElement(CreateItems.SALES_QUANTITY_VALUES);
				paraVec.addElement(/*salesQuantity.getValueAsString()*/value);
			}else if (salesQuantity != null && salesQuantity.getValueAsString() != null) {
				 // conversion needed because command accepts only baseQuantity (internal) as of 5.0
				 String unit = pc.convertUOMExternalToInternal(salesQuantity.getUnit());
				 String value = pc.convertValueExternalToInternal(salesQuantity.getValueAsString(), salesQuantity.getUnit(), document._getDocumentProperties(false).getDecimalSeparator(), document._getDocumentProperties(false).getGroupingSeparator());
	
				paraVec.addElement(CreateItems.SALES_QUANTITY_UNITS);
				paraVec.addElement(/*salesQuantity.getUnit()*/unit);
				paraVec.addElement(CreateItems.SALES_QUANTITY_VALUES);
				paraVec.addElement(/*salesQuantity.getValueAsString()*/value);
			}else{
				log.warn("Neiter baseQuantity nor salesQuantity specified for the creation of item for " + productId);
			}
		   if (targetDate != null) {
			   paraVec.addElement(CreateItems.KNOWLEDGE_BASE_DATES);
			   paraVec.addElement(targetDate);
			   paraVec.addElement(CreateItems.PRICING_DATES);
			   paraVec.addElement(targetDate);
		   }
		   _addMapsParamsToVector(i+1,itemAttributes, paraVec, CreateItems.ITEM_ATTRIBUTE_NAMES, CreateItems.ITEM_ATTRIBUTE_VALUES, CreateItems.ITEM_ATTRIBUTE_ITEM_REFS);
		   _addMapsParamsToVector(i+1,headerAttributes, paraVec, CreateItems.HEADER_ATTRIBUTE_NAMES, CreateItems.HEADER_ATTRIBUTE_VALUES, CreateItems.HEADER_ATTRIBUTE_ITEM_REFS);
		   _addMapsParamsToVector(i+1,conditionTimestamps, paraVec, CreateItems.CONDITION_ACCESS_TIMESTAMP_NAMES, CreateItems.CONDITION_ACCESS_TIMESTAMP_VALUES, CreateItems.CONDITION_ACCESS_TIMESTAMP_REFS);

		   if (props[i].getExchangeRateType() != null) {
			   paraVec.addElement(CreateItems.EXCHANGE_RATE_TYPES);
			   paraVec.addElement(props[i].getExchangeRateType());
		   }
		   if (props[i].getExchangeRateDate() != null) {
			   paraVec.addElement(CreateItems.EXCHANGE_RATE_DATES);
			   paraVec.addElement(props[i].getExchangeRateDate());
		   }
		   if (highLevelItemId != null) {
			   paraVec.addElement(CreateItems.HIGH_LEVEL_ITEM_IDS);
			   paraVec.addElement(highLevelItemId);
		   }
		   
			if (pricingRelevant != null) {
				String tcpPricingRelevant = "";
				if (pricingRelevant.equals(Boolean.TRUE)) {
					tcpPricingRelevant = ServerConstants.YES;
				}else {
					tcpPricingRelevant = ServerConstants.NO;
				}
				paraVec.addElement(CreateItems.IS_RELEVANT_FOR_PRICINGS);
				paraVec.addElement(tcpPricingRelevant);
			}

//Start of  modifications for passing the config along with CreateItems call.
		   // add knowledgebase information
		   String kbLogSys = props[i].getKbLogSys();
		   if (kbLogSys != null) {
			   paraVec.addElement(CreateItems.KNOWLEDGE_BASE_LOGSYS);
			   paraVec.addElement(kbLogSys);
		   }
		   String kbName = props[i].getKbName();
		   if (kbName != null) {
			   paraVec.addElement(CreateItems.KNOWLEDGE_BASE_NAMES);
			   paraVec.addElement(kbName);
		   }
		   String kbVersion = props[i].getKbVersion();
		   if (kbVersion != null) {
			   paraVec.addElement(CreateItems.KNOWLEDGE_BASE_VERSIONS);
			   paraVec.addElement(kbVersion);
		   }
		   String kbProfile = props[i].getKbProfile();
		   if (kbProfile != null) {
			   paraVec.addElement(CreateItems.KNOWLEDGE_BASE_PROFILES);
			   paraVec.addElement(kbProfile);
		   }

		   // add configuration data
		   DefaultIPCItemProperties tprop = (DefaultIPCItemProperties)props[i];
		   ConfigValue cfgValue = tprop.getConfigValue();
		   if (cfgValue != null) {
			   ext_configuration extConfig = cfgValue.getConfig(client.toString());

			   c_ext_cfg_imp extConfigimp = (c_ext_cfg_imp)extConfig;
			   if (extConfigimp != null) {
				   String extConfigXml = extConfigimp.cfg_ext_to_xml_string();
				   paraVec.addElement(CreateItems.EXT_CONFIG_XML_STRINGS);
				   paraVec.addElement(extConfigXml);
			   }
		   }
//End of modificaitons
		   _addMapsParamsToVector(i+1, props[i].getContext(), paraVec, CreateItems.SCE_CONTEXT_NAMES,
								  CreateItems.SCE_CONTEXT_VALUES, CreateItems.SCE_CONTEXT_ITEM_REFS);

		   boolean performPricingAnalysis = props[i].getPerformPricingAnalysis();
		   _addBooleanParamToVector(performPricingAnalysis, paraVec, CreateItems.PERFORM_PRICING_ANALYSIS);
           
            boolean decoupleSubitems = props[i].isDecoupleSubitems();
            _addBooleanParamToVector(decoupleSubitems, paraVec, CreateItems.GRID_PRODUCTS);
           
		   // add additional parameters set by the user
		   if (props[i] instanceof DefaultIPCItemProperties) {
			   List additionalParaNames = ((DefaultIPCItemProperties)props[i]).getCreationCommandParameterNames();
			   List additionalParaValues = ((DefaultIPCItemProperties)props[i]).getCreationCommandParameterValues();
			   if (additionalParaNames != null) {
				   int parasize = additionalParaNames.size();
				   for (int j=0; j<parasize; j++) {
					   paraVec.addElement(additionalParaNames.get(j));
					   paraVec.addElement(additionalParaValues.get(j));
				   }
			   }
		   }
	   }

	   String[] params = new String[paraVec.size()];
	   paraVec.copyInto(params);
	   try{
		   ServerResponse r = ((TCPDefaultClient)client).doCmd(ISPCCommandSet.CREATE_ITEMS, params);
		   String[] itemIds = r.getParameterValues(CreateItems.ITEM_IDS);
		   DefaultIPCItem[] items = new DefaultIPCItem[itemIds.length];
		   for (int i=0; i<itemIds.length; i++) {
               // create the new item-object for the returned itemId               
			   items[i] = IPCClientObjectFactory.getInstance().newIPCItem(client, document, itemIds[i]);
               // the _client member of ItemProperties' configValue has to be set (done via the initializeValues method; only the session is used - the other paramters are ignored);
               ((DefaultIPCItemProperties)props[i]).initializeValues((DefaultIPCSession)document.getSession(), null, null, false, false);
               // use the passed itemProperties-object for the new generated item (in order to avoid the loss of data already set in the passed itemProperties)
               items[i].setItemProperties((DefaultIPCItemProperties)props[i]);
			   document.addItem(items[i]);
			   ((DefaultIPCItem)items[i]).setCacheDirty();
		   }
			//document.setCacheDirty();
           if (location.beDebug()){
               if (items != null) {
                   location.debugT("return items");
                   for (int i = 0; i<items.length; i++){
                       location.debugT("  items["+i+"] "+items[i].getItemId());
                   }
               }
           }
           location.exiting();
           return items;
	   }catch(ClientException ce){
		   throw new IPCException(ce);
	   }

	 }

	//******Implementation for Abstract methods starts from here!********

	/* (non-Javadoc)
	 * @see com.sap.spc.remote.client.object.IPCItem#getItemReference()
	 */
	public IPCItemReference getItemReference() 
	{
       location.entering("getItemReference()");
	   TCPDefaultClient _tcpClient = (TCPDefaultClient) _client;
	   TCPIPCItemReference ref = new TCPIPCItemReference();
	   ref.setDispatcher(false);
	   ref.setHost(_tcpClient.getHost());
	   ref.setPort(Integer.toString(_tcpClient.getPort()));
	   ref.setEncoding(_tcpClient.getEncoding());
	   ref.setDocumentId(_document.getId());
	   ref.setItemId(getItemId());
       if (location.beDebug()){
           location.debugT("return ref " + ref.getItemId());
       }
       location.exiting();
       return ref;
 }

	/* (non-Javadoc)
	 * @see com.sap.spc.remote.client.object.IPCItem#setHeaderAttributeBindings(java.lang.String[], java.lang.String[])
	 */
	public void setHeaderAttributeBindings(String[] names, String[] values)
		throws IPCException 
	{
        location.entering("setHeaderAttributeBindings(String[] names, String[] values)");
        if (location.beDebug()){
            location.debugT("Parameters:");
            location.debugT("String[] names ");
            if (names != null) {
                for (int i = 0; i<names.length; i++){
                    location.debugT("  names["+i+"] "+names[i]);
                }
            }
            location.debugT("String[] values ");
            if (values != null) {
                for (int i = 0; i<values.length; i++){
                    location.debugT("  values["+i+"] "+values[i]);
                }
            }
        }
	   Map attr = _props.getHeaderAttributes();
	   String[] param = new String[names.length*4];
	   for (int i=0; i<names.length; i++) {
		   param[i*4] = ChangeItems.HEADER_ATTRIBUTE_NAMES;
		   param[i*4+1] = names[i];
		   param[i*4+2] = ChangeItems.HEADER_ATTRIBUTE_VALUES;
		   param[i*4+3] = values[i];
		   attr.put(names[i],values[i]);
	   }
	   _props.setHeaderAttributes(attr);
	   _changeItem(null, param);
       location.exiting();
	 }

	/* (non-Javadoc)
	 * @see com.sap.spc.remote.client.object.IPCItem#setItemAttributeBindings(java.lang.String[], java.lang.String[])
	 */
	public void setItemAttributeBindings(String[] names, String[] values)
		throws IPCException 
	{
        location.entering("setItemAttributeBindings(String[] names, String[] values)");
        if (location.beDebug()){
            location.debugT("Parameters:");
            location.debugT("String[] names ");
            if (names != null) {
                for (int i = 0; i<names.length; i++){
                    location.debugT("  names["+i+"] "+names[i]);
                }
            }
            location.debugT("String[] values ");
            if (values != null) {
                for (int i = 0; i<values.length; i++){
                    location.debugT("  values["+i+"] "+values[i]);
                }
            }
        }
		Map attr = _props.getItemAttributes();
		 String[] param = new String[names.length*4];
		 for (int i=0; i<names.length; i++) {
			 param[i*4] = ChangeItems.ITEM_ATTRIBUTE_NAMES;
			 param[i*4+1] = names[i];
			 param[i*4+2] = ChangeItems.ITEM_ATTRIBUTE_VALUES;
			 param[i*4+3] = values[i];
			attr.put(names[i],values[i]);
		}            
		_props.setItemAttributes(attr);
		 _changeItem(null, param);
 	}

	/* (non-Javadoc)
	 * @see com.sap.spc.remote.client.object.IPCItem#setContext(java.util.Map)
	 */
	public void setContext(Map context) throws IPCException 
	{
       location.entering("setContext(Map context)");
       if (location.beDebug()){
            location.debugT("Parameters:");
            if (context!=null){
                location.debugT("Map context " + context.toString());
            }
            else location.debugT("Map context null");
       }
       Vector params = new Vector();
	   params.add(ChangeItemConfig.DOCUMENT_ID);
	   params.add(_document.getId());
	   params.add(ChangeItemConfig.ITEM_ID);
	   params.add(_itemId);

	   _addMapParamsToVector(context, params, ChangeItemConfig.SCE_CONTEXT_NAME, ChangeItemConfig.SCE_CONTEXT_VALUE);

	   String[] paraString = new String[params.size()];
	   params.copyInto(paraString);
		try{
			((TCPDefaultClient)_client).doCmd(this, ISPCCommandSet.CHANGE_ITEM_CONFIG, paraString);
			setCacheDirty();
	  	}catch(ClientException e){
			throw new IPCException(e);
	  	}
	   _resetInternalConfiguration();
       location.exiting();
 }

	/* (non-Javadoc)
	 * @see com.sap.spc.remote.client.object.IPCItem#setAdditionalParameter(java.lang.String, java.lang.String)
	 */
	public void setAdditionalParameter(String param, String value)
		throws IPCException 
	{
       location.entering("setAdditionalParameter(String param, String value)");
       if (location.beDebug()){
           location.debugT("Parameters:");
           location.debugT("String param " + param);
           location.debugT("String value " + value);
       }
	   Vector params = new Vector();
	   params.add(ChangeItemConfig.DOCUMENT_ID);
	   params.add(_document.getId());
	   params.add(ChangeItemConfig.ITEM_ID);
	   params.add(_itemId);

	   params.add(param);
	   params.add(value);

	   String[] paraString = new String[params.size()];
	   params.copyInto(paraString);
	   try{
		   //_client.command(this, ISPCCommandSet.CHANGE_ITEM_CONFIG, paraString);
		   ((TCPDefaultClient)_client).doCmd(this, ISPCCommandSet.CHANGE_ITEM_CONFIG, paraString);
			setCacheDirty();
	   }catch(ClientException e){
		   throw new IPCException(e);
	   }
	   _resetInternalConfiguration();
	   _children = null;
       location.exiting();
 	}

	/* (non-Javadoc)
	 * @see com.sap.spc.remote.client.object.IPCItem#setConfig(com.sap.sce.kbrt.ext_configuration)
	 */
	public void setConfig(ext_configuration config) throws IPCException 
	{
         location.entering("setConfig(ext_configuration config)");
         if (location.beDebug()){
            location.debugT("Parameters:");
            location.debugT("ext_configuration config " + config.get_name());
         }
	     //String[] configParams = ConfigValue.getConfigIPCParameters(config);
	     TcpConfigValue cValue = new TcpConfigValue();
	     String[] configParams = cValue.getConfigIPCParameters(config);
		 String[] params = new String[configParams.length+4];
		 for (int i=0; i<configParams.length; i++)
			 params[i+4] = configParams[i];
				   params[0] = ChangeItemConfig.DOCUMENT_ID;
		 params[1] = _document.getId();
		 params[2] = ChangeItemConfig.ITEM_ID;
		 params[3] = _itemId;
		 try{
			  //_client.command(this, ISPCCommandSet.CHANGE_ITEM_CONFIG, params);
			 ((TCPDefaultClient)_client).doCmd(this, ISPCCommandSet.CHANGE_ITEM_CONFIG, params);
			 setCacheDirty();
		 }catch(ClientException e){
			 throw new IPCException(e);
		 }
		 _resetInternalConfiguration();
         location.exiting();
	}
	
	public void setConfig(ext_configuration config, Hashtable context) throws IPCException 
	{
         location.entering("setConfig(ext_configuration config)");
         if (location.beDebug()){
            location.debugT("Parameters:");
            location.debugT("ext_configuration config " + config.get_name());
         }
	     //String[] configParams = ConfigValue.getConfigIPCParameters(config);
	     TcpConfigValue cValue = new TcpConfigValue();
	     String[] configParams = cValue.getConfigIPCParameters(config);
		 String[] params = new String[configParams.length+4];
		 for (int i=0; i<configParams.length; i++)
			 params[i+4] = configParams[i];
				   params[0] = ChangeItemConfig.DOCUMENT_ID;
		 params[1] = _document.getId();
		 params[2] = ChangeItemConfig.ITEM_ID;
		 params[3] = _itemId;
		 try{
			  //_client.command(this, ISPCCommandSet.CHANGE_ITEM_CONFIG, params);
			 ((TCPDefaultClient)_client).doCmd(this, ISPCCommandSet.CHANGE_ITEM_CONFIG, params);
			 setCacheDirty();
		 }catch(ClientException e){
			 throw new IPCException(e);
		 }
		 _resetInternalConfiguration();
         location.exiting();
	}

	/* (non-Javadoc)
	 * @deprecated
	 * @see com.sap.spc.remote.client.object.IPCItem#setConfig(com.sap.spc.remote.client.object.OrderConfiguration)
	 */
	public void setConfig(OrderConfiguration config) throws IPCException 
	{
            location.entering("setConfig(OrderConfiguration config)");
            if (location.beDebug()){
                location.debugT("Parameters:");
                location.debugT("OrderConfiguration config " + config.getConfiguration().get_name());
            }
		   //String[] configParams = ConfigValue.getConfigIPCParameters(config.getConfiguration());
	       TcpConfigValue cValue = new TcpConfigValue();
	       String[] configParams = cValue.getConfigIPCParameters(config.getConfiguration());
		   ArrayList parA = new ArrayList(configParams.length*2);
		   parA.add(ChangeItemConfig.DOCUMENT_ID);
		   parA.add(_document.getId());
		   parA.add(ChangeItemConfig.ITEM_ID);
		   parA.add(_itemId);
		   for (int i=0; i<configParams.length; i++) {
			   parA.add(configParams[i]);
		   }
	
		   Map m = config.getPosIdToInstIdMap();
		   if (m != null) {
			   for (Iterator iter=m.keySet().iterator(); iter.hasNext();) {
				   String posId = (String)iter.next();
				   String instId = (String)m.get(posId);
				   parA.add(ChangeItemConfig.EXT_POS_CONFIG_ID);
				   parA.add("1"); // will be ignored anyway
				   parA.add(ChangeItemConfig.EXT_POS_GUID);
				   parA.add(posId); // will be ignored anyway
				   parA.add(ChangeItemConfig.EXT_POS_INST_ID);
				   parA.add(instId); // will be ignored anyway
			   }
		   }
	
		   String kbDate = config.getDate();
		   if (kbDate != null && kbDate.length() > 0) {
			   parA.add(ChangeItemConfig.KB_DATE);
			   parA.add(kbDate);
		   }
	
		   // convert to string[]
		   String[] data = new String[parA.size()];
		   parA.toArray(data);
		   try{
				//_client.command(this, ISPCCommandSet.CHANGE_ITEM_CONFIG, data);
			   ((TCPDefaultClient)_client).doCmd(this, ISPCCommandSet.CHANGE_ITEM_CONFIG, data);
			setCacheDirty();
		   }catch(ClientException e){
			   throw new IPCException(e);
		   }
		   _resetInternalConfiguration();
           location.exiting();
 	}

	/* (non-Javadoc)
	 * @see com.sap.spc.remote.client.object.IPCItem#setStatistical(boolean)
	 */
	public void setStatistical(boolean flag) throws IPCException 
	{
     location.entering("setStatistical(boolean flag)");
     if (location.beDebug()){
        location.debugT("Parameters:");
        location.debugT("boolean flag " + flag);
     }  
	 String[] params = { ChangeItems.IS_STATISTICALS, flag ? "Y" : "N" };
	 _changeItem(null, params);
     location.exiting();
 }

	/* (non-Javadoc)
	 * @see com.sap.spc.remote.client.object.IPCItem#setPricingDate(java.lang.String)
	 */
	public void setPricingDate(String s) throws IPCException 
	{
     location.entering("setPricingDate(String s)");
     if (location.beDebug()){
        location.debugT("Parameters:");
        location.debugT("String s " + s);
     }
	 String[] params = new String[] {
		 ChangeItems.PRICING_DATES, s
	 };
	 _changeItem(null,params);
 }

	/* (non-Javadoc)
	 * @see com.sap.spc.remote.client.object.IPCItem#setNetWeight(com.sap.spc.remote.client.object.DimensionalValue)
	 */
	public void setNetWeight(DimensionalValue weight) 
	{
        location.entering("setNetWeight(DimensionalValue weight)");
        if (location.beDebug()){
            location.debugT("Parameters:");
            location.debugT("DimensionalValue weight " + weight.getValueAsString() + " " + weight.getUnit());
        }
        TcpDefaultPricingConverter pc = (TcpDefaultPricingConverter)((TcpDefaultIPCSession)_document.getSession()).getPricingConverter(); 
		String convertedUnit = pc.convertUOMExternalToInternal(weight.getUnit());
		String convertedValue = pc.convertValueExternalToInternal(weight.getValueAsString(), weight.getUnit(), _document._getDocumentProperties(false).getDecimalSeparator(), _document._getDocumentProperties(false).getGroupingSeparator());
	   try {
//		   String[] params = new String[] { ChangeItems.NET_WEIGHT_VALUES, weight.getValueAsString(),
//											ChangeItems.WEIGHT_UNITS, weight.getUnit() };
		String[] params = new String[] { ChangeItems.NET_WEIGHT_VALUES, /*weight.getValueAsString()*/convertedValue,
										 ChangeItems.WEIGHT_UNITS, /*weight.getUnit()*/convertedUnit };
		   _changeItem(null, params);
        location.exiting();
	   }
	   catch(IPCException e) {
		   throw new RuntimeException("DefaultIPCItem.setNetWeight is supposed to be called with a manually constructed weight");
	   }
 }

	/* (non-Javadoc)
	 * @see com.sap.spc.remote.client.object.IPCItem#setVolume(com.sap.spc.remote.client.object.DimensionalValue)
	 */
	public void setVolume(DimensionalValue value) throws IPCException 
	{
        location.entering("setVolume(DimensionalValue value)");
        if (location.beDebug()){
            location.debugT("Parameters:");
            location.debugT("DimensionalValue value " + value.getValueAsString() + " " + value.getUnit());
        }
        TcpDefaultPricingConverter pc = (TcpDefaultPricingConverter)((TcpDefaultIPCSession)_document.getSession()).getPricingConverter();
		String convertedUnit = pc.convertUOMExternalToInternal(value.getUnit());
		String convertedValue = pc.convertValueExternalToInternal(value.getValueAsString(), value.getUnit(), _document._getDocumentProperties(false).getDecimalSeparator(), _document._getDocumentProperties(false).getGroupingSeparator());
		 String[] params = new String[] { ChangeItems.VOLUME_UNITS, /*value.getUnit()*/convertedUnit,
										  ChangeItems.VOLUME_VALUES, /*value.getValueAsString()*/convertedValue };
//		 String[] params = new String[] { ChangeItems.VOLUME_UNITS, value.getUnit(),
//										  ChangeItems.VOLUME_VALUES, value.getValueAsString() };
		 _changeItem(null, params);
         location.exiting();
	 }

	/* (non-Javadoc)
	 * @see com.sap.spc.remote.client.object.IPCItem#setSalesQuantity(com.sap.spc.remote.client.object.DimensionalValue)
	 */
	public void setSalesQuantity(DimensionalValue saleValue) throws IPCException 
	{
        location.entering("setSalesQuantity(DimensionalValue saleValue)");
        if (location.beDebug()){
            location.debugT("Parameters:");
            location.debugT("DimensionalValue value " + saleValue.getValueAsString() + " " + saleValue.getUnit());
        }
		//_changeItems(...) expecting always internal representation...
        TcpDefaultPricingConverter pc = (TcpDefaultPricingConverter)((TcpDefaultIPCSession)_document.getSession()).getPricingConverter();
		String convertedUnit = pc.convertUOMExternalToInternal(saleValue.getUnit());
		String convertedValue = pc.convertValueExternalToInternal(saleValue.getValueAsString(), saleValue.getUnit(), _document._getDocumentProperties(false).getDecimalSeparator(), _document._getDocumentProperties(false).getGroupingSeparator());
		 String[] params = new String[] { ChangeItems.SALES_QUANTITY_UNITS, convertedUnit,
										  ChangeItems.SALES_QUANTITY_VALUES, convertedValue};
		 _changeItem(null, params);
         location.exiting();
	 }

	/* (non-Javadoc)
	 * @see com.sap.spc.remote.client.object.imp.DefaultIPCItem#setBaseQuantity(com.sap.spc.remote.client.object.DimensionalValue)
	 */
	public void setBaseQuantity(DimensionalValue baseValue) throws IPCException {
        location.entering("setBaseQuantity(DimensionalValue baseValue)");
        if (location.beDebug()){
            location.debugT("Parameters:");
            location.debugT("DimensionalValue value " + baseValue.getValueAsString() + " " + baseValue.getUnit());
        }
        //_changeItems(...) expects always internal representation
		 String[] params = new String[] { ChangeItems.SALES_QUANTITY_UNITS, baseValue.getUnit(),
										  ChangeItems.SALES_QUANTITY_VALUES, baseValue.getValueAsString() };
		 _changeItem(null, params);
         location.exiting();
	}

	/* (non-Javadoc)
	 * @see com.sap.spc.remote.client.object.IPCItem#setGrossWeight(com.sap.spc.remote.client.object.DimensionalValue)
	 */
	public void setGrossWeight(DimensionalValue weight) throws IPCException 
	{
        location.entering("setGrossWeight(DimensionalValue weight)");
        if (location.beDebug()){
            location.debugT("Parameters:");
            location.debugT("DimensionalValue weight " + weight.getValueAsString() + " " + weight.getUnit());
        }
        TcpDefaultPricingConverter pc = (TcpDefaultPricingConverter)((TcpDefaultIPCSession)_document.getSession()).getPricingConverter();
		String convertedUnit = pc.convertUOMExternalToInternal(weight.getUnit());
		String convertedValue = pc.convertValueExternalToInternal(weight.getValueAsString(), weight.getUnit(), _document._getDocumentProperties(false).getDecimalSeparator(), _document._getDocumentProperties(false).getGroupingSeparator());
		
		 String[] params = new String[] { ChangeItems.GROSS_WEIGHT_VALUES, /*weight.getValueAsString()*/convertedValue,
									  ChangeItems.WEIGHT_UNITS, /*weight.getUnit()*/convertedUnit };
//	 String[] params = new String[] { ChangeItems.GROSS_WEIGHT_VALUES, weight.getValueAsString(),
//									  ChangeItems.WEIGHT_UNITS, weight.getUnit() };
	 _changeItem(null, params);
     location.exiting();
 }

	/* (non-Javadoc)
	 * @see com.sap.spc.remote.client.object.IPCItem#setExchangeRateType(java.lang.String)
	 */
	public void setExchangeRateType(String exchangeRateType)
		throws IPCException 
	{
           location.entering("setExchangeRateType(String exchangeRateType)");
           if (location.beDebug()){
               location.debugT("Parameters:");
               location.debugT("String exchangeRateType " + exchangeRateType);
           }
		   String[] params = new String[] { ChangeItems.EXCHANGE_RATE_TYPES, exchangeRateType };
		   _changeItem(null, params);
		   // sync props
		   _props.setExchangeRateType(exchangeRateType);
           location.exiting();
	 }

	/* (non-Javadoc)
	 * @see com.sap.spc.remote.client.object.IPCItem#setExchangeRateDate(java.lang.String)
	 */
	public void setExchangeRateDate(String exchangeRateDate)
		throws IPCException 
	{
            location.entering("setExchangeRateDate(String exchangeRateDate)");
            if (location.beDebug()){
                location.debugT("Parameters:");
                location.debugT("String exchangeRateDate " + exchangeRateDate);
            }
		    String[] params = new String[] { ChangeItems.EXCHANGE_RATE_DATES, exchangeRateDate };
		    _changeItem(null, params);
		   // sync props
			_props.setExchangeRateDate(exchangeRateDate);
            location.exiting();
	 }

	/* (non-Javadoc)
	 * @see com.sap.spc.remote.client.object.IPCItem#copyItem(com.sap.spc.remote.client.object.IPCDocument)
	 */
	public IPCItem copyItem(IPCDocument targetDocument) throws IPCException 
	{
         location.entering("copyItem(IPCDocument targetDocument)");
         if (location.beDebug()){
            location.debugT("Parameters:");
            location.debugT("IPCDocument targetDocument " + targetDocument.getId());
         }
		 if (!(targetDocument instanceof DefaultIPCDocument))
			 throw new IllegalClassException(targetDocument, DefaultIPCDocument.class);
	
		 String[] params = new String[] {
			 CopyItem.SOURCE_DOCUMENT_ID, _document.getId(),
			 CopyItem.SOURCE_ITEM_ID, _itemId,
			 CopyItem.TARGET_DOCUMENT_ID, targetDocument.getId(),
			 CopyItem.REVERT_CONFIG, "N"
		 };
		 try{
			 ServerResponse response = ((TCPDefaultClient)_client).doCmd(ISPCCommandSet.COPY_ITEM, params);
			 String targetItemId = response.getParameterValue(CopyItem.TARGET_ITEM_ID);
			 DefaultIPCItem newItem = factory.newIPCItem(_client, (DefaultIPCDocument)targetDocument, targetItemId);
             
             // create a copy of the existing itemProps using the original ones
             DefaultIPCItemProperties newProps = factory.newIPCItemProperties(_props);
            
             // the _client member of ItemProperties' configValue has to be set (to avoid NullPointerExceptions during execution of initProperties()-method later)
             newProps.getConfigValue().setInitialValue(_client);
            
             // set the new props at the new item
             newItem.setItemProperties(newProps);
             
			 targetDocument.addItem(newItem);
			((DefaultIPCItem)newItem).setCacheDirty();
             if (location.beDebug()){
                location.debugT("return newItem" + newItem.getItemId());
             }
             location.exiting();
             return newItem;
		 }catch(ClientException e){
			 throw new IPCException(e);
		 }
	 }

	/* (non-Javadoc)
	 * @see com.sap.spc.remote.client.object.imp.DefaultIPCItem#_getExternalPricingInfo()
	 */
	protected HashMap _getExternalPricingInfo() throws IPCException 
	{
			
			
			
		   data = new HashMap();
		   try{
			   // get pricing item
			   ServerResponse r = ((TCPDefaultClient)_client).doCmd(com.sap.msasrv.usermodule.spc.ISPCCommandSet.GET_PRICING_ITEM_INFO,
				   new String[] {
					   GetPricingItemInfo.DOCUMENT_ID, _document.getId(),
					   GetPricingItemInfo.ITEM_ID, _itemId,
					   GetItemConditions.FORMAT_VALUE, "N",
					   GetPricingItemInfo.IS_SWINGGUI, ServerConstants.YES //damit auch base_ + sales_quantity zurï¿½ckgeliefert wird
				   });
	
			   String nextParam = null;
			   String value = null;
			   String key = null;
			   for (Enumeration e = r.getParametersEnum(); e.hasMoreElements();) {
				   nextParam = (String)e.nextElement();
				   if ( nextParam.startsWith("currency") || nextParam.startsWith("CURRENCY") ||
						nextParam.startsWith("net") || nextParam.startsWith("NET") ||
						nextParam.startsWith("tax") || nextParam.startsWith("TAX") ||
						nextParam.startsWith("gross") || nextParam.startsWith("GROSS") ||
						nextParam.startsWith("total") || nextParam.startsWith("TOTAL") ||
						nextParam.startsWith("subtotal") || nextParam.startsWith("SUBTOTAL") ||
						nextParam.startsWith("purpose") || nextParam.startsWith("PURPOSE") ) {
	
					   String values[] = r.getParameterValues(nextParam);
					   for (int i=0; i<values.length; i++) {
						   value = values[i];
						   if (value == null) value = "";
	
						   key = nextParam + "[" + (i+1) +"]";
						   data.put(key, value);
					   }
				   }
			   }
		   }catch(ClientException ce){
			   throw new IPCException(ce);
		   }
		   return data;
	 }

	/* (non-Javadoc)
	 * @see com.sap.spc.remote.client.object.imp.DefaultIPCItem#_changeItem(java.lang.String[], java.lang.String[])
	 */
	protected void _changeItem(String[] configParams, String[] pricingParams)
		throws IPCException 
	{
	   // config influences price => call ChangeItemConfig before ChangeItem
	   if (configParams != null) {
		   String[] realParams = new String[configParams.length+4];
		   realParams[0] = ChangeItemConfig.DOCUMENT_ID;
		   realParams[1] = _document.getId();
		   realParams[2] = ChangeItemConfig.ITEM_ID;
		   realParams[3] = _itemId;
		   for (int i=0; i<configParams.length; i++) {
			   realParams[i+4] = configParams[i];
		   }
		   try {
				((TCPDefaultClient)_client).doCmd(this, ISPCCommandSet.CHANGE_ITEM_CONFIG, realParams);
				setCacheDirty();
		   }catch(ClientException e){
			   throw new IPCException(e);
		   }
	   }

	   if (pricingParams != null) {
		   String[] realParams = new String[pricingParams.length+4];
		   realParams[0] = ChangeItems.DOCUMENT_ID;
		   realParams[1] = _document.getId();
		   realParams[2] = ChangeItems.ITEM_IDS;
		   realParams[3] = _itemId;
           // flag for grid products has to be set (see CreateItems or RFC implementation) (adapt lenght of array! (4 -> 6) and for-loop
//           realParams[4] = ChangeItems.GRID_PRODUCTS;
//           boolean decoupleSubitems = _props.isDecoupleSubitems();
//           String tcpDecoupleSubitems = decoupleSubitems ? SCECommand.YES : SCECommand.NO;
//           realParams[5] = tcpDecoupleSubitems;
		   //realParams[4] = ChangeItems.FORMAT_VALUE;
		   //realParams[5] = SCECommand.YES;
		   for (int i=0; i<pricingParams.length; i++) {
			   realParams[i+4] = pricingParams[i];
		   }
		   try {
				((TCPDefaultClient)_client).doCmd(this, ISPCCommandSet.CHANGE_ITEMS, realParams);
				setCacheDirty();
		   }catch(ClientException e){
			   throw new IPCException(e);
		   }
	   }
 	}

	/* (non-Javadoc)
	 * @see com.sap.spc.remote.client.object.imp.DefaultIPCItem#_resetInternalConfiguration()
	 */
	protected void _resetInternalConfiguration() 
	{
		 _config = new ConfigurationValue(this.getDocument().getSession().getIPCClient(),
										  com.sap.spc.remote.shared.ConfigIdManager.
										  getConfigId(getDocument().getId(), getItemId()));
	   //20010808-kha: please note: the context data are not changed by the server, therefore
	   //there is no need to ever retrieve/change them.
	}

	/**
	 * Sets Cache as diety.
	 */
	public void setCacheDirty() {
        location.entering("setCacheDirty()");
		//cacheIsDirty = true;
		//Caching is done by document, pass this information.
		_document.setCacheDirty();
        location.exiting();
	}
				
//	public void syncWithServer() throws IPCException{
//		//This item cache might be dirty but doc cache may not be dirty. In this case make the doc cache as dirty so that doc will be in sycn with server in the sync call. 
//		if (isCacheDirty() && !_document.isCacheDirty())
//			_document.setCacheDirty();
//        	
//		_document.syncWithServer();
//	}
	
	public String getPricingAnalysisData() throws IPCException {
        location.entering("getPricingAnalysisData()");
		String xmlDoc=null;
		try {
			ServerResponse r=((TCPDefaultClient)_client).doCmd(com.sap.msasrv.usermodule.spc.ISPCCommandSet.GET_XML_PRICING_TRACE,
				new String[] {
					GetXMLPricingTrace.DOCUMENT_ID, _document.getId(),
					GetXMLPricingTrace.ITEM_ID, _itemId,
				});
				
			xmlDoc=r.getParameterValue(GetXMLPricingTrace.XML_STRING);	
		} catch (ClientException e) {
			throw new IPCException(e);
		}
        if (location.beDebug()){
            location.debugT("return xmlDoc" + xmlDoc);
        }
        location.exiting();
		return xmlDoc;
	}
	//********Implementation for Abstract methods ends here!********

    /* (non-Javadoc)
     * @see com.sap.spc.remote.client.object.IPCItem#setSubItemsAllowed(boolean)
     */
    public void setSubItemsAllowed(boolean flag) throws IPCException {
       location.entering("setSubItemsAllowed(boolean flag)");
       if (location.beDebug()){
            location.debugT("Parameters:");
            location.debugT("boolean flag " + flag);
       }
       Vector params = new Vector();
       params.add(ChangeItemConfig.DOCUMENT_ID);
       params.add(_document.getId());
       params.add(ChangeItemConfig.ITEM_ID);
       params.add(_itemId);

       params.add(ChangeItemConfig.SUB_ITEMS_ALLOWED);
       params.add(flag ? "Y" : "N");

       String[] paraString = new String[params.size()];
       params.copyInto(paraString);
       try{
           //_client.command(this, ISPCCommandSet.CHANGE_ITEM_CONFIG, paraString);
           ((TCPDefaultClient)_client).doCmd(this, ISPCCommandSet.CHANGE_ITEM_CONFIG, paraString);
            setCacheDirty();
       }catch(ClientException e){
           throw new IPCException(e);
       }
       _resetInternalConfiguration();
       _children = null;
       location.exiting();

    }

    public void setReturn(boolean flag) throws IPCException {
        location.entering("setReturn(boolean flag)");
        if (location.beDebug()){
            location.debugT("Parameters:");
            location.debugT("boolean flag " + flag);
        }
        String[] params = new String[] { ChangeItems.IS_RETURNS, flag ? "Y" : "N" };
        _changeItem(null, params);
        location.exiting();
    }

    /* (non-Javadoc)
     * @see com.sap.spc.remote.client.object.imp.DefaultIPCItem#setItemAttributes(java.util.Map)
     */
    public void setItemAttributes(Map attributes) throws IPCException {
        // not implemented (was introduced for RFC side because this was needed in ISA)
    }



	public static String getProductGuid(String productId, String productType, String productLogSys, IClient client){
        location.entering("getProductGuid(String productId, String productType, String productLogSys, IClient client)");
        if (location.beDebug()){
            location.debugT("Parameters:");
            location.debugT("String productId " + productId);
            location.debugT("String productType " + productType);
            location.debugT("String productLogSys " + productLogSys);
            if (client!=null){
                location.debugT("IClient client " + client.toString());
            }
            else location.debugT("IClient client null");
        }
        if (productId == null ) return null;
		if (client == null || !(client instanceof TCPDefaultClient)) return null;
		String productGuid = null;
		String[] productGuids = null;
		Vector paraVec = new Vector();
		
		paraVec.addElement(ConvertInternalProductIdsToProductGUIDs.PRODUCT_IDS);
		paraVec.addElement(productId);
		
		if (productType != null && !productType.equals("")){
			paraVec.addElement(ConvertInternalProductIdsToProductGUIDs.PRODUCT_TYPES);
			paraVec.addElement(productType);
		}else{
			category.logT(Severity.WARNING, location, ConvertInternalProductIdsToProductGUIDs.PRODUCT_TYPES+
				" is null or empty string. Ignored this while converting from " + ConvertInternalProductIdsToProductGUIDs.PRODUCT_IDS + 
				" to " + ConvertInternalProductIdsToProductGUIDs.PRODUCT_GUIDS);
		}
			
		if (productLogSys != null && !productLogSys.equals("")){
			paraVec.addElement(ConvertInternalProductIdsToProductGUIDs.PRODUCT_LOGSYS);
			paraVec.addElement(productLogSys);
		}else{
			category.logT(Severity.WARNING, location, ConvertInternalProductIdsToProductGUIDs.PRODUCT_LOGSYS+
				" is null or empty string. Ignored this while converting from "+ ConvertInternalProductIdsToProductGUIDs.PRODUCT_IDS +
				" to " + ConvertInternalProductIdsToProductGUIDs.PRODUCT_GUIDS);
		}

		String[] params = new String[paraVec.size()];
		paraVec.copyInto(params);
		try{
			ServerResponse r = ((TCPDefaultClient)client).doCmd(ISPCCommandSet.CONVER_INTERNAL_PRODUCT_IDS_TO_PRODUCT_GUIDS, params);
			productGuids = r.getParameterValues(ConvertInternalProductIdsToProductGUIDs.PRODUCT_GUIDS);
			if (productGuids != null && productGuids.length > 0) 
				productGuid = productGuids[0]; 
		}catch(ClientException e){
			throw new IPCException(e);
		}
        if (location.beDebug()){
            location.debugT("return productGuid" + productGuid);
        }
        location.exiting();
		return productGuid;
	}


	/**
	*/
   void initItemDetails() {
	   ServerResponse r = null;
	   try {
		   r = ((TCPDefaultClient)_client).doCmd(ISPCCommandSet.GET_ITEM_INFO,
							   new String[] {
								   GetItemInfo.DOCUMENT_ID, _document.getId(),
								   GetItemInfo.ITEM_ID, _itemId,
								   GetItemInfo.FORMAT_VALUE, "N"
							   });
	   } catch (ClientException e) {
		   throw new IPCException(e);
	   }
		
	   String[] conditionTimestampNames = r.getParameterValues(GetItemInfo.CONDITION_ACCESS_TIMESTAMP_NAME);
	   String[] conditionTimestampValues = r.getParameterValues(GetItemInfo.CONDITION_ACCESS_TIMESTAMP_VALUE);
	   String[] itemAttributeNames = r.getParameterValues(GetItemInfo.BINDING_NAME);
	   String[] itemAttributeValues = r.getParameterValues(GetItemInfo.BINDING_VALUE);
		
	   // make sure that some command results are available
	   if ( conditionTimestampNames != null && conditionTimestampValues != null){
		   Map conditionTimestamps = new HashMap(conditionTimestampNames.length);
		   for (int i = 0; i < conditionTimestampNames.length; i++) {
		   	   if (conditionTimestampValues[i] != null) {
			       conditionTimestamps.put(conditionTimestampNames[i], conditionTimestampValues[i]);
			   }
		   }
		   _props.setConditionTimestamps(conditionTimestamps);
	   }
	   
	   if ( itemAttributeNames != null && itemAttributeValues != null) {
	       Map itemAttributes = new HashMap(itemAttributeNames.length);
	       for (int i = 0; i < itemAttributeNames.length; i++) {
	           if (itemAttributeValues[i] != null) {
	           	   itemAttributes.put(itemAttributeNames[i], itemAttributeValues[i]);
	           }
	       }
	       _props.setItemAttributes(itemAttributes);
	   }
   }


	/* (non-Javadoc)
	 * @see com.sap.spc.remote.client.object.IPCItem#getPropertiesFromServer(java.lang.String[])
	 */
	public IPCItemProperties[] getPropertiesFromServer(String[] itemIds) {
        location.entering("getPropertiesFromServer(String[] itemIds)");
        if (location.beDebug()){
            location.debugT("Parameters:");
            location.debugT("String[] itemIds");
            if (itemIds != null) {
                for (int i = 0; i<itemIds.length; i++){
                    location.debugT("  itemIds["+i+"] "+itemIds[i]);
                }
            }
         }
		IPCItemProperties[] itemProperties = new IPCItemProperties[1];
		itemProperties[0] = this._props;
        if (location.beDebug()){
            if (itemProperties != null) {
                location.debugT("return itemProperties");
                for (int j = 0; j<itemProperties.length; j++){
                    location.debugT("  itemProperties["+j+"] "+itemProperties[j].toString());
                }
            }
        }
        location.exiting();
		return itemProperties;
	}

    /* (non-Javadoc)
     * @see com.sap.spc.remote.client.object.IPCItem#getProductKey()
     */
    public ProductKey getProductKey() {
        location.entering("getProductKey()");
        ProductKey prodKey =  factory.newProductKey(_props.getProductId(), _props.getProductType(), _props.getProductLogSys());
        if (location.beDebug()){
              location.debugT("return prodKey " + "(prodKey: "+prodKey.getProductId()+")");
        }
        location.exiting();
        return prodKey;
    }

    /* (non-Javadoc)
     * @see com.sap.spc.remote.client.object.IPCItem#getProductGUID(java.lang.String, java.lang.String, java.lang.String, java.lang.String)
     */
    public String getProductGuid(
        String prodId,
        String prodType,
        String prodLogSys) {
        location.entering("getProductGuid(String prodId, String prodType, String prodLogSys)");
        if (location.beDebug()){
            location.debugT("Parameters:");
            location.debugT("String prodId " + prodId);
            location.debugT("String prodType " + prodType);
            location.debugT("String prodLogSys " + prodLogSys);
        }    
        String guid = TcpDefaultIPCItem.getProductGuid(prodId, prodType, prodLogSys, _client);
        if (location.beDebug()){
              location.debugT("return guid " + guid);
        }
        location.exiting();
        return guid;
    }

    /* (non-Javadoc)
     * @see com.sap.spc.remote.client.object.IPCItem#getAttributesFromProductMaster(java.lang.String)
     */
    public Map getAttributesFromProductMaster(String guid) {
        location.entering("getAttributesFromProductMaster(String guid)");
        if (location.beDebug()){
            location.debugT("Parameters:");
            location.debugT("String guid " + guid);
        }
        Map attribs = new HashMap();
        // if no guid is passed we take the item's own guid
        if (guid == null){
            guid = _props.getProductGuid();
        }
        // Dummy implementation: at the moment only the attributes containing the guid are returned.
        // We have no chance to get the attributes from the server (no function modules available).
        // Should be implemented with next release (5.1).        
        attribs.put("PRICE_PRODUCT", guid);  
        attribs.put("PRODUCT", guid);
        if (location.beDebug()){
            if (attribs!=null){
                location.debugT("return attribs " + attribs.toString());
            }
            else location.debugT("return attribs null");
        }
        location.exiting();
        return attribs;
    }

    /* (non-Javadoc)
     * @see com.sap.spc.remote.client.object.IPCItem#changeProduct(java.lang.String)
     */
    public boolean changeProduct(String productId) {
        location.entering("changeProduct(String productId)");
        if (location.beDebug()){
            location.debugT("Parameters:");
            location.debugT("String productId " + productId);
        }
        boolean success = false;
        try {            
            ArrayList params = new ArrayList();
            params.add(ChangeItemProduct.PRODUCT_ID);
            params.add(productId);
            params.add(ChangeItemProduct.DOCUMENT_ID);
            params.add(this.getDocument().getId());
            params.add(ChangeItemProduct.ITEM_ID);
            params.add(this.getItemId());
            String[] paraString = new String[params.size()];
            params.toArray(paraString);
            ServerResponse r =
            ((TCPDefaultClient)_client).doCmd(
                    ISPCCommandSet.CHANGE_ITEM_PRODUCT,
                    paraString);

            String result = r.getParameterValue(ChangeItemProduct.SUCCESS);
            // HACK: right now, ChangeItemProduct doesn't return anything for its success, assume the best has happened :-)
            if (result == null || result.equals("Y")) {
                success = true;
            }
            setCacheDirty();
        } catch (ClientException e) { 
            category.logT(Severity.ERROR, location, e.toString());
            throw new IPCException(e);
        }
        if (location.beDebug()){
              location.debugT("return success " + success);
        }
        location.exiting();
        return success;
    }

    /* (non-Javadoc)
     * @see com.sap.spc.remote.client.object.IPCItem#setInitialConfiguration(com.sap.spc.remote.client.util.ext_configuration)
     */
    public void setInitialConfiguration(ext_configuration config) throws IPCException {
		location.entering("setInitialConfiguration(ext_configuration config)");
		if (location.beDebug()){
		}
		boolean success = false;
			TcpConfigValue cfgValue = new TcpConfigValue();
			String cfgParam[] = cfgValue.getConfigIPCParameters(config);
			String param[] = new String[ cfgParam.length + 4];
			int k;
			for(  k = 0; k < cfgParam.length;k++)
				param[k]=cfgParam [k];
			param[k++] = SCECommand.DOCUMENT_ID;
			param[k++] = getDocument().getId();			
			param[k++] = SCECommand.ITEM_ID;
			param[k++] = getItemId();			
			try {
				ServerResponse r =
				((TCPDefaultClient)_client).doCmd(
						ISPCCommandSet.SET_INITIAL_CONFIGURATION,
						param);
			} catch (ClientException e) {
				throw new IPCException(e);
			}
		if (location.beDebug()){
			  location.debugT("terminate setInitialConfiguration" );
		}
		// we have to set the hasInitialConfig flag of loading status manually
		// first get the loading status
		LoadingStatus ls = this.getConfiguration().getLoadingStatus();
		// set the flag
		ls.setLoadedFromInitialConfiguration(true);
		location.exiting();
    }
    
    /* (non-Javadoc)
     * @see com.sap.spc.remote.client.object.IPCItem#changeExternalId(java.lang.String)
     */
    public void changeExternalId(String value) throws IPCException {
        // not implemented (was introduced for RFC side because this was needed for TTE in ISA)
    }
    
	/* (non-Javadoc)
	 * @see com.sap.spc.remote.client.object.IPCItem#getDynamicReturn()
	 */
	public Map getDynamicReturn() {
		return null;
    }

    /* (non-Javadoc)
     * @see com.sap.spc.remote.client.object.IPCItem#getInitialConfiguration()
     */    
    public ConfigurationSnapshot getInitialConfiguration() {
		location.entering("getInitialConfiguration()");
		DefaultConfigurationSnapshot snap=null;
		TcpConfigValue cfgValue = new TcpConfigValue();
			try {
				ServerResponse r =
				((TCPDefaultClient)_client).doCmd(
						ISPCCommandSet.GET_INITIAL_CONFIG,
						new String[]{
							SCECommand.DOCUMENT_ID,
							getDocument().getId(),
							SCECommand.ITEM_ID,
							getItemId(),
						});
				String kbName =		r.getParameterValue(ExtConfigConstants.EXT_CONFIG_KB_NAME);
				if( kbName == null)
					return null;		
				ext_configuration ext_config = ExternalConfigConverter.createConfig(
													new SimpleRequestWrapper(r),
													null,
	 												null,null,null,null,null);                       
				if (ext_config != null){
					snap = factory.newConfigurationSnapshot(ext_config, "initialConfiguration", "", null, true);
					if (location.beDebug()) {
						String xml = ((c_ext_cfg_imp)ext_config).cfg_ext_to_xml_string();
						location.debugT("Initial Configuration returned by AP:");
						location.debugT(xml);
					}
				}
				else {
					category.logT(Severity.DEBUG, location, "getInitialConfiguration(): It was not possible to generate an ext_configuration object.");
				}
			} catch (ClientException e) {
				throw new IPCException(e);
			} catch (ConfigException e) {
				throw new IPCException(e);
			}
		if (location.beDebug()){
			  location.debugT("terminate getInitialConfiguration" );
		}
		location.exiting();
		return snap;
    }
    
	/**
	 * Executes command GetConfigItemInfo and sets the result to the member _config of the item properties.
	 * @return A mapping between instance ids of the configuration and ipc items used for pricing.
	 */
	public Map readConfigItemInfoFromBackend() {
		String[] params = new String[] { GetConfigItemInfo.DOCUMENT_ID, _document.getId(),
										 GetConfigItemInfo.ITEM_ID, _itemId };
										 
	        Map instItemMapping = new HashMap();
                              
			try {
				ServerResponse r = ((TCPDefaultClient)_client).doCmd(this, ISPCCommandSet.GET_CONFIG_ITEM_INFO, params);
				Vector dataVec = new Vector();
				((TcpConfigValue)_props.getConfigValue()).copyTableDataFromResponse(r,_props.getConfigValue().get_cfg(),dataVec);
				_props.getConfigValue().set_cfg(dataVec);

				dataVec = new Vector();                                 
				((TcpConfigValue)_props.getConfigValue()).copyTableDataFromResponse(r,_props.getConfigValue().get_ins(),dataVec);
				_props.getConfigValue().set_ins(dataVec);

				dataVec = new Vector();                                 
				((TcpConfigValue)_props.getConfigValue()).copyTableDataFromResponse(r,_props.getConfigValue().get_prt(),dataVec);
				_props.getConfigValue().set_prt(dataVec);

				dataVec = new Vector();                                 
				((TcpConfigValue)_props.getConfigValue()).copyTableDataFromResponse(r,_props.getConfigValue().get_val(),dataVec);
				_props.getConfigValue().set_val(dataVec);

				dataVec = new Vector();                                 
				((TcpConfigValue)_props.getConfigValue()).copyTableDataFromResponse(r,_props.getConfigValue().get_prc(),dataVec);
				_props.getConfigValue().set_prc(dataVec);
				
				dataVec.clear();
				TableValue instItemMappingTable = new TableValue(new String[] { GetConfigItemInfo.EXT_POS_CONFIG_ID, GetConfigItemInfo.EXT_POS_INST_ID, GetConfigItemInfo.EXT_POS_ITEM_ID } );
				((TcpConfigValue)_props.getConfigValue()).copyTableDataFromResponse(r,instItemMappingTable, dataVec);
				for (int i = 0; i < instItemMappingTable.rows(); i++) {
					String instanceId = instItemMappingTable.getElement(GetConfigItemInfo.EXT_POS_INST_ID, i);
					String itemId = instItemMappingTable.getElement(GetConfigItemInfo.EXT_POS_ITEM_ID, i);
					instItemMapping.put(instanceId, _document.getItemFromCache(itemId)); //do not trigger syncWithServer
}
			}
			catch(ClientException e) {
				throw new IPCException(e);
			}
			return instItemMapping;

	}

}
