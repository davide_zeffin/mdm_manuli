package com.sap.spc.remote.client.object.imp;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;

import com.sap.isa.businessobject.BusinessObjectBase;
import com.sap.isa.core.TechKey;
import com.sap.spc.remote.client.object.IPCDocumentProperties;
import com.sap.spc.remote.client.object.IPCItemProperties;
import com.sap.spc.remote.client.object.TTEConstants;
import com.sap.spc.remote.client.object.TTEProduct;


public class DefaultTTEProduct extends BusinessObjectBase implements TTEProduct, TTEConstants {

    private String productId;
    private String taxability;
    private ArrayList taxClassifications;
    private HashMap taxClassificationsByCountry;
    
    // arrays of attribute names
    private String[] tax_dept_type_p; //taxTypeProductDepartCty;
    private String[] tax_dept_group_p; // taxGroupProductDepartCty;
    private String[] tax_type_p; //taxTypeProduct;
    private String[] tax_group_p; // taxGroupProduct;


    /**
     * 
     */
    DefaultTTEProduct(IPCItemProperties itemProps, IPCDocumentProperties docProps) {
        super(new TechKey(itemProps.getProductGuid()));
        initAttributeArrays();
        initAttributes(itemProps, docProps);
    }

    /**
     * Initializes the product related information.
     * @param itemProps
     * @param docProps
     */
    private void initAttributes(IPCItemProperties itemProps, IPCDocumentProperties docProps) {

        HashMap headerAttributes = (HashMap) itemProps.getHeaderAttributes();
        HashMap itemAttributes = (HashMap) itemProps.getItemAttributes();
        HashMap docHeaderAttributes = (HashMap) docProps.getHeaderAttributes();
        
       
        this.productId = itemProps.getProductGuid();
        this.taxability = "100";
        this.taxClassifications = new ArrayList();
        this.taxClassificationsByCountry = new HashMap();
        // first get the information of the destination 
        String countryDest = getAttributeValue(docHeaderAttributes, headerAttributes, itemAttributes, TAX_DEST_CTY_ISO); 
        String regionDest = getAttributeValue(docHeaderAttributes, headerAttributes, itemAttributes, TAX_DEST_REG);
        ArrayList taxGroupsDest = getAttributeValues(docHeaderAttributes, headerAttributes, itemAttributes, tax_group_p);
        ArrayList taxTypesDest = getAttributeValues(docHeaderAttributes, headerAttributes, itemAttributes, tax_type_p);
        
        // fill the productCountries and productRegions list with the appropriate number of entries
        Iterator groupsDestIter = taxGroupsDest.iterator();
        String taxType; 
        int i = 0;
        while (groupsDestIter.hasNext()){
            String taxGroup = groupsDestIter.next().toString();
            if (i >= taxTypesDest.size()){
                taxType = "";
            }
            else {
                taxType = taxTypesDest.get(i).toString();
            }
            // Add the classification only if it does not exist at this product already
            // and if taxType is not empty. 
            String key = countryDest + regionDest + taxType;
            if ((taxClassificationsByCountry.get(key) == null) && (!taxType.equals(""))){
                DefaultTTEProductTaxClassification taxClassif = 
                    IPCClientObjectFactory.getInstance().
                        newTTEProductTaxClassification(countryDest, regionDest, taxType, taxGroup, "");
                taxClassifications.add(taxClassif);
                taxClassificationsByCountry.put(key, taxClassif);                    
            }
            i++;
        }        
        
        // second get the information of the departure
        String countryDept = getAttributeValue(docHeaderAttributes, headerAttributes, itemAttributes, TAX_DEP_CTY_ISO);   
        String regionDept = getAttributeValue(docHeaderAttributes, headerAttributes, itemAttributes, TAX_DEPART_REG);
        ArrayList taxGroupsDept = getAttributeValues(docHeaderAttributes, headerAttributes, itemAttributes, tax_dept_group_p);
        ArrayList taxTypesDept = getAttributeValues(docHeaderAttributes, headerAttributes, itemAttributes, tax_dept_type_p);
        
        // fill the productCountries and productRegions list with the appropriate number of entries
        Iterator groupsDeptIter = taxGroupsDept.iterator();
        String taxTypeDept; 
        int j = 0;
        while (groupsDeptIter.hasNext()){
            String taxGroup = groupsDeptIter.next().toString();
            if (j >= taxTypesDept.size()){
                taxTypeDept = "";
            }
            else {
                taxTypeDept = taxTypesDept.get(j).toString();
            }
            // Add the classification only if it does not exist at this product already
            // and if taxType is not empty. 
            String key = countryDept + regionDept + taxTypeDept;
            if ((taxClassificationsByCountry.get(key) == null) && (!taxTypeDept.equals(""))){
                DefaultTTEProductTaxClassification taxClassif = 
                    IPCClientObjectFactory.getInstance().
                        newTTEProductTaxClassification(countryDept, regionDept, taxTypeDept, taxGroup, "");
                taxClassifications.add(taxClassif);
                taxClassificationsByCountry.put(key, taxClassif);                    
            }
            j++;
        }        
    }

    /* (non-Javadoc)
     * @see com.sap.spc.remote.client.object.TTEProduct#getProductId()
     */
    public String getProductId() {
        return productId;
    }


    /* (non-Javadoc)
     * @see com.sap.spc.remote.client.object.TTEProduct#getTaxability()
     */
    public String getTaxability() {
        return taxability;
    }

    /* (non-Javadoc)
     * @see com.sap.spc.remote.client.object.TTEProduct#containsTaxClassification(java.lang.String, java.lang.String, java.lang.String)
     */
    public boolean containsTaxClassification(String country, String region, String taxType) {
        String key = country + region + taxType;
        if (taxClassificationsByCountry.get(key) == null){
            return false;
        }
        return true;
    }

    /**
     * Fills the arrays with the attribute names
     */
    private void initAttributeArrays(){
        tax_dept_type_p = new String[9];
        tax_dept_type_p[0] = TAX_DEPT_TYPE_P_01;
        tax_dept_type_p[1] = TAX_DEPT_TYPE_P_02;
        tax_dept_type_p[2] = TAX_DEPT_TYPE_P_03;
        tax_dept_type_p[3] = TAX_DEPT_TYPE_P_04;
        tax_dept_type_p[4] = TAX_DEPT_TYPE_P_05;
        tax_dept_type_p[5] = TAX_DEPT_TYPE_P_06;
        tax_dept_type_p[6] = TAX_DEPT_TYPE_P_07;
        tax_dept_type_p[7] = TAX_DEPT_TYPE_P_08;
        tax_dept_type_p[8] = TAX_DEPT_TYPE_P_09;
        
        tax_dept_group_p = new String[9];
        tax_dept_group_p[0] = TAX_DEPT_GROUP_P_01;
        tax_dept_group_p[1] = TAX_DEPT_GROUP_P_02;
        tax_dept_group_p[2] = TAX_DEPT_GROUP_P_03;
        tax_dept_group_p[3] = TAX_DEPT_GROUP_P_04;
        tax_dept_group_p[4] = TAX_DEPT_GROUP_P_05;
        tax_dept_group_p[5] = TAX_DEPT_GROUP_P_06;
        tax_dept_group_p[6] = TAX_DEPT_GROUP_P_07;
        tax_dept_group_p[7] = TAX_DEPT_GROUP_P_08;
        tax_dept_group_p[8] = TAX_DEPT_GROUP_P_09;
        
        tax_type_p = new String[9];
        tax_type_p[0] = TAX_TYPE_P_01;
        tax_type_p[1] = TAX_TYPE_P_02;
        tax_type_p[2] = TAX_TYPE_P_03;
        tax_type_p[3] = TAX_TYPE_P_04;
        tax_type_p[4] = TAX_TYPE_P_05;
        tax_type_p[5] = TAX_TYPE_P_06;
        tax_type_p[6] = TAX_TYPE_P_07;
        tax_type_p[7] = TAX_TYPE_P_08;
        tax_type_p[8] = TAX_TYPE_P_09;
        
        tax_group_p = new String[10];
        tax_group_p[0] = TAX_GROUP_P; 
        tax_group_p[1] = TAX_GROUP_P_01;
        tax_group_p[2] = TAX_GROUP_P_02;
        tax_group_p[3] = TAX_GROUP_P_03;
        tax_group_p[4] = TAX_GROUP_P_04;
        tax_group_p[5] = TAX_GROUP_P_05;
        tax_group_p[6] = TAX_GROUP_P_06;
        tax_group_p[7] = TAX_GROUP_P_07;
        tax_group_p[8] = TAX_GROUP_P_08;
        tax_group_p[9] = TAX_GROUP_P_09;
    }

    /**
     * Searches for a list of attribute names. 
     * See also @see DefaultTTEProduct#getAttributeValue(HashMap, HashMap, HashMap, String).
     * @param docHeaderAttributes document-header attributes
     * @param headerAttributes item-header attributes
     * @param itemAttributes item attrbitues
     * @param attributeNames list of attribute names
     * @return a list of values
     */
    private ArrayList getAttributeValues(HashMap docHeaderAttributes, HashMap headerAttributes, HashMap itemAttributes, String[] attributeNames) {
        int max = attributeNames.length;
        ArrayList values = new ArrayList();
        for (int i = 0; i < max; i++){
            String value = getAttributeValue(docHeaderAttributes, headerAttributes, itemAttributes, attributeNames[i]);
            if (value != null && !value.equals("")) {
                values.add(value);
            }
        }
        return values;
    }
    
    /**
     * The attributes can be stored either in the item-header attributes, in the item attributes or 
     * in the document-header attributes.<br>
     * Attributes on item-level overwrite attributes on document level.<br> 
     * First we search in item attributes. If this was not successful we search
     * in the item-header attributes. If this was still not successfull we search in the document-header
     * attributes.
     * @param docHeaderAttributes document-header attributes
     * @param headerAttributes item-header attributes
     * @param itemAttributes item attrbitues
     * @param attributeName
     * @return the value if found. Otherwise an empty String ("").
     */
    private String getAttributeValue(HashMap docHeaderAttributes, HashMap headerAttributes, HashMap itemAttributes, String attributeName) {
        String attributeValue = (String) itemAttributes.get(attributeName);
        if (attributeValue == null) {
            attributeValue = (String) headerAttributes.get(attributeName);
            if (attributeValue == null){
                attributeValue = (String) docHeaderAttributes.get(attributeName);
                if (attributeValue == null){
                    attributeValue = "";
                }
            }
        }
        return attributeValue;
    }    


    /* (non-Javadoc)
     * @see com.sap.spc.remote.client.object.TTEProduct#getTaxClassifications()
     */
    public ArrayList getTaxClassifications() {
        return taxClassifications;
    }

    /* (non-Javadoc)
     * @see com.sap.spc.remote.client.object.TTEProduct#getTaxClassificationsByCountry(java.lang.String, java.lang.String, java.lang.String)
     */
    public HashMap getTaxClassificationsByCountry(String country, String region, String taxType) {
        return taxClassificationsByCountry;
    }

}
