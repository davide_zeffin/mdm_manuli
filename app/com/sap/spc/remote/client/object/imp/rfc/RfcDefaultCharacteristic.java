package com.sap.spc.remote.client.object.imp.rfc;

import com.sap.spc.remote.client.ResourceAccessor;
import com.sap.spc.remote.client.object.IPCClient;
import com.sap.spc.remote.client.object.Instance;
import com.sap.spc.remote.client.object.imp.ConfigurationChangeStream;
import com.sap.spc.remote.client.object.imp.DefaultCharacteristic;
import com.sap.tc.logging.Category;
import com.sap.tc.logging.Location;

public class RfcDefaultCharacteristic extends DefaultCharacteristic {

	// logging
	private static final Category category = ResourceAccessor.category;
	private static final Location location = ResourceAccessor.getLocation(RfcDefaultCharacteristic.class);

    protected RfcDefaultCharacteristic(
        ConfigurationChangeStream configurationChangeStream,
        IPCClient ipcClient,
        Instance instance,
        String groupName,
        String languageDependentName,
        String name,
        String unit,
        String entryFieldMask,
        int typeLength,
        int numberScale,
        int valueType,
        boolean allowsAdditionalValues,
        boolean visible,
        boolean allowsMultipleValues,
        boolean required,
        boolean expanded,
        boolean consistent,
        boolean isDomainConstrained,
        boolean hasIntervalAsDomain,
        boolean readonly,
        boolean pricingRelevant,
        String description,
        String applicationViews,
        String[] csticMimeLNames,
        String[] csticMimeObjectTypes,
        String[] csticMimeObjects,
        int[] index) {
        init(
            configurationChangeStream,
            ipcClient,
            instance,
            groupName,
            name,
            unit,
            entryFieldMask,
            typeLength,
            numberScale,
            valueType,
            allowsAdditionalValues,
            visible,
            allowsMultipleValues,
            required,
            expanded,
            consistent,
            isDomainConstrained,
            hasIntervalAsDomain,
            readonly,
            pricingRelevant,
            applicationViews,
            csticMimeLNames,
            csticMimeObjectTypes,
            csticMimeObjects,
            index);
    }

 	public static void main(String[] args) {
		DefaultCharacteristic cstic  = new RfcDefaultCharacteristic(null, null, null, "group1", "langDepName1",
		 "name1", "unit1", "entryFieldMask1", 1, 1, 1, false, true, true, true, true, true, true, true, true, true,
		 "description1", "views1", new String[]{"csticMimeLNames1"}, new String[]{"csticMimeObjectTypes1"},
		new String[]{"csticMimeObjects1"}, new int[]{1} );
		
		cstic.setGroupName("group2");
		System.out.println(cstic.getGroupName());
	}

    /* (non-Javadoc)
     * @see com.sap.spc.remote.client.object.Characteristic#getLanguageDependentUnit()
     */
    public String getLanguageDependentUnit() {
        String languageDependentUnit = "";
        RfcDefaultIPCSession session = (RfcDefaultIPCSession) ipcClient.getIPCSession();
        RfcDefaultPricingConverter pc = (RfcDefaultPricingConverter)((RfcDefaultIPCSession)session).getPricingConverter();
        // convert only if
        // - unit is not null
        // - unit is not an empty string ("")
        // - value type is not CURRENCY (for currency no conversion needed)
        
        if ((this.unit != null) && (!this.unit.equals("")) && (this.valueType != TYPE_CURRENCY)){
            languageDependentUnit = pc.convertUOMInternalToExternal(this.unit); 
        }
        if (languageDependentUnit.equals("")){
            languageDependentUnit = this.unit;
        }
        return languageDependentUnit;
    }

}
